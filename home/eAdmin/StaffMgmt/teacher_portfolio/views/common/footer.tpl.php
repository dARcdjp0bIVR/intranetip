<div class="modal fade" id="tpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel"></h4>
			</div>
			<div class="modal-body">
				<div style="padding: 25px; text-center"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i></div>
			</div>
		</div>
	</div>
</div>

<?php if (isset($data["debugMsg"])) echo $data["debugMsg"]; ?>
<script language="javascript" type="text/javascript">
var tp_key = "<?php echo $data["ajaxKey"]; ?>";
var assetPath = '<?php echo $data["AssetsPath"]; ?>';
var msze='<?php echo $data["max_upload"]; ?>';
var langtxt = [];
langtxt["mod_title"] = '<?php echo $data["lang"]["TeacherPortfolio"]["ModuleTitle"]; ?>';
langtxt["loading"] = '<?php echo $data["lang"]["TeacherPortfolio"]["loading"]; ?>...';
langtxt["pageNotFound"] = '<?php echo $data["lang"]["TeacherPortfolio"]["pageNotFound"]; ?>';
langtxt["processing"] = '<?php echo $data["lang"]["TeacherPortfolio"]["processing"]; ?>';
langtxt["updated"] = '<?php echo $data["lang"]["TeacherPortfolio"]["updated"]; ?>';
langtxt["UpdateFailed"] = '<?php echo $data["lang"]['TeacherPortfolio']["UpdateFailed"]; ?>';
langtxt["sortUpdateFailed"] = '<?php echo $data["lang"]['TeacherPortfolio']["sortUpdateFailed"]; ?>';
langtxt["allRequiredFields"] = '<?php echo $data["lang"]["TeacherPortfolio"]["allRequiredFields"]; ?>';
langtxt["dateRangeInvalid"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dateRange"]; ?>';
langtxt["action_add"] = '<?php echo $data["lang"]['TeacherPortfolio']["add"]; ?>';
langtxt["action_edit"] = '<?php echo $data["lang"]['TeacherPortfolio']["edit"]; ?>';
langtxt["action_back"] = '<?php echo $data["lang"]['TeacherPortfolio']["back"]; ?>';
langtxt["action_submit"] = '<?php echo $data["lang"]['TeacherPortfolio']["Submit"]; ?>';
langtxt["action_cancel"] = '<?php echo $data["lang"]['TeacherPortfolio']["Cancel"]; ?>';
langtxt["action_delete"] = '<?php echo $data["lang"]['TeacherPortfolio']["Delete"]; ?>';
langtxt["ConfirmMsg_delete"] = '<?php echo $data["lang"]['TeacherPortfolio']["ConfirmMsg_delete"]; ?>';
langtxt["dataTable_info"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_info"]; ?>';
langtxt["dataTable_emptyTable"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_infoEmpty"]; ?>';
langtxt["dataTable_infoEmpty"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_infoEmpty"]; ?>';
langtxt["dataTable_lengthMenu"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_lengthMenu"]; ?>';
langtxt["dataTable_paginate_first"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_paginate_first"]; ?>';
langtxt["dataTable_paginate_previous"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_paginate_previous"]; ?>';
langtxt["dataTable_paginate_next"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_paginate_next"]; ?>';
langtxt["dataTable_paginate_last"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_paginate_last"]; ?>';
langtxt["dataTable_paginate_page"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_paginate_page"]; ?>';
langtxt["dataTable_paginate_pageof"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_paginate_pageof"]; ?>';
langtxt["dataTable_delete_success"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_delete_success"]; ?>';
langtxt["dataTable_delete_fail"] = '<?php echo $data["lang"]["TeacherPortfolio"]["dataTable_delete_fail"]; ?>';
langtxt["dataTable_selectAtLeastOne"] = '<?php echo $data["lang"]['TeacherPortfolio']["dataTable_selectAtLeastOne"]; ?>';
langtxt["dataTable_search"] = '<?php echo $data["lang"]['TeacherPortfolio']["dataTable_search"]; ?>';
langtxt["dataTable_searchPH"] = '<?php echo $data["lang"]["TeacherPortfolio"]["TeacherName"]; ?>';
langtxt["Content"] = '<?php echo $data["lang"]["TeacherPortfolio"]["Content"]; ?>';
langtxt["Mode"] = '<?php echo $data["lang"]['TeacherPortfolio']["Mode"]; ?>';
langtxt["Hour"] = '<?php echo $data["lang"]['TeacherPortfolio']["Hour"]; ?>';
langtxt["Organizer"] = '<?php echo $data["lang"]['TeacherPortfolio']["Organizer"]; ?>';
langtxt["CertificateName"] = '<?php echo $data["lang"]['TeacherPortfolio']["CertificateName"]; ?>';
langtxt["AwardingInstitution"] = '<?php echo $data["lang"]['TeacherPortfolio']["AwardingInstitution"]; ?>';
langtxt["Category"] = '<?php echo $data["lang"]['TeacherPortfolio']["Category"]; ?>';
langtxt["err_unknown"] = '<?php echo $data["lang"]['TeacherPortfolio']["err_unknown"]; ?>';
langtxt["err_timeout"] = '<?php echo $data["lang"]['TeacherPortfolio']["err_timeout"]; ?>';
langtxt["fileupload_failed"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_failed"]; ?>';
langtxt["fileupload_file_failed"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_file_failed"]; ?>';
langtxt["fileupload_Success"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_Success"]; ?>';
langtxt["fileupload_file_extension_err"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_file_extension_err"]; ?>';
langtxt["fileupload_select_failed"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_select_failed"]; ?>';
langtxt["fileupload_outOfSize"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_outOfSize"]; ?>';
langtxt["fileupload_failedUploadingProcessing"] = '<?php echo $data["lang"]['TeacherPortfolio']["fileupload_failedUploadingProcessing"]; ?>';
langtxt["downloadFailed"] = '<?php echo $data["lang"]['TeacherPortfolio']["downloadFailed"]; ?>';
langtxt["downloadSuccess"] = '<?php echo $data["lang"]['TeacherPortfolio']["downloadSuccess"]; ?>';
langtxt["manage_selectAtLeastOne"] = '<?php echo $data["lang"]['TeacherPortfolio']["manage_selectAtLeastOne"]; ?>';
langtxt["manage_removeAllUser"] = '<?php echo $data["lang"]['TeacherPortfolio']["manage_removeAllUser"]; ?>';
langtxt["manage_updateSuccess"] = '<?php echo $data["lang"]['TeacherPortfolio']["manage_updateSuccess"]; ?>';
langtxt["manage_updateFail"] = '<?php echo $data["lang"]['TeacherPortfolio']["manage_updateFail"]; ?>';
</script>
</body>
</html>