<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $data["lang"]["TeacherPortfolio"]["title"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script src="<?php echo $data["AssetsPath"]; ?>/jquery/jquery-3.1.1.min.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootstrap-notify.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/bootstrap/js/bootbox.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/plupload/js/plupload.full.min.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo $data["AssetsPath"]; ?>/webfont/Arvo.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $data["AssetsPath"]; ?>/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/bootstrap-datepicker/bootstrap-datepicker3.css"/>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/css/datatables.min.css"/>
<link rel="stylesheet" href="<?php echo $data["AssetsPath"]; ?>/css/jquery.qtip.min.css"/>
<link href="<?php echo $data["AssetsPath"]; ?>/css/animate.css" rel="stylesheet" />
<script src="<?php echo $data["AssetsPath"]; ?>/js/datatables.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery.qtip.min.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery-sortable.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/jquery.fileDownload.js"></script>
<link href="<?php echo $data["AssetsPath"]; ?>/css/style.css" rel="stylesheet" />
<link href="<?php echo $data["AssetsPath"]; ?>/css/tp.css" rel="stylesheet" />
<link rel="apple-touch-icon-precomposed" sizes="57x57" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-114x114.png" />
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-144x144.png" />
<link rel="apple-touch-icon-precomposed" sizes="60x60" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-60x60.png" />
<link rel="apple-touch-icon-precomposed" sizes="120x120" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-120x120.png" />
<link rel="apple-touch-icon-precomposed" sizes="76x76" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-76x76.png" />
<link rel="apple-touch-icon-precomposed" sizes="152x152" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/apple-touch-icon-152x152.png" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-196x196.png" sizes="196x196" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-96x96.png" sizes="96x96" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-32x32.png" sizes="32x32" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-16x16.png" sizes="16x16" />
<link rel="icon" type="image/png" href="<?php echo $data["AssetsPath"]; ?>/images/favicon/favicon-128.png" sizes="128x128" />
<meta name="application-name" content="&nbsp;"/>
<meta name="msapplication-TileColor" content="#FFFFFF" />
<meta name="msapplication-TileImage" content="<?php echo $data["AssetsPath"]; ?>/images/favicon/mstile-144x144.png" />
<meta name="msapplication-square70x70logo" content="<?php echo $data["AssetsPath"]; ?>/images/favicon/mstile-70x70.png" />
<meta name="msapplication-square150x150logo" content="<?php echo $data["AssetsPath"]; ?>/images/favicon/mstile-150x150.png" />
<meta name="msapplication-wide310x150logo" content="<?php echo $data["AssetsPath"]; ?>/images/favicon/mstile-310x150.png" />
<meta name="msapplication-square310x310logo" content="<?php echo $data["AssetsPath"]; ?>/images/favicon/mstile-310x310.png" />
<script src="<?php echo $data["PATH_WRT_ROOT"]; ?>templates/script.js"></script>
<script src="<?php echo $data["AssetsPath"]; ?>/js/tbapp.js?t=<?php echo time(); ?>"></script>
</head>
<body>