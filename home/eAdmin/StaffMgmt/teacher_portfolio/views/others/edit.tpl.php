<!--CONTENT: DEVELOP - EDIT-->
<div id="others-edit" class="tab-pane fade in active">
	<div class="toolbar">                
		<ul class="actions left">
			<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
		</ul>
	</div>
<?php
	if ($data["currAction"] == "edit" && $data["infoRecord"] == "no record") {
?>
		<div class="content">
			<?php echo $data["lang"]["TeacherPortfolio"]["noThisRecord"]; ?>
		</div>
<?php
	} else {
?>
	<div class="content cert-form">
		<form id="editform" rel="<?php echo $data["section"]; ?>/process">
		<?php if ($data["currAction"] == "edit") { ?>
			<input type='hidden' name='action' value='updaterecord'>
			<input type='hidden' name='recid' value='<?php echo $data["infoRecord"]["InfoID"]; ?>'>
		<?php } else { ?>
			<input type='hidden' name='action' value='insertrecord'>
		<?php } ?>
		<div class="title">
			<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"][$data["currAction"]]; ?></span><span class="text2"><?php echo $data["lang"]['TeacherPortfolio']["sec_others"]; ?></span>
		</div>
		<div class="body">
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
			<div class="form-group">
				<label for="selectedUser" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Teacher"]; ?></label>
				<div class="col-field">
					<select class="form-control input-sm" name='selectedUser'>
<?php
			if (count($data["allTeachers"]) > 0) {
?>
				<option value='0'><?php echo $data["lang"]['TeacherPortfolio']["pleaseSelectTeacher"]; ?></option>
<?php
				foreach ($data["allTeachers"] as $tkk => $tvv) {
					echo "<option value='" . $tvv["UserID"] . "_" . $tvv["cs"] . "'";
					if ($data["currAction"] == "edit") {
						if ($data["infoRecord"]["UserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					} else {
						if ($data["userInfo"]["info"]["SelectedUserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					}
					echo ">" . Get_Lang_Selection($tvv["ChineseName"], $tvv["EnglishName"]) . " ( LoginID: " . $tvv["UserLogin"]. " )</option>";
				}
				} ?></select>
				</div>
			</div>		
<?php } ?>
			<div class="form-group">
				<label for="date" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Date"]; ?></label>
				<div class="col-field">
					<div class="input-group date inputdate" id="datetimepicker2">
						<input class="form-control date" id="date" name="date" placeholder="YYYY-MM-DD" value="<?php if (isset($data["infoRecord"]["InfoDate"])) echo $data["infoRecord"]["InfoDate"]; ?>" type="text" required />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>    
			<div class="form-group">
				<label for="engInfoName" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Name"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="engInfoName" value="<?php if (isset($data["infoRecord"]["InfoNameEn"])) echo $data["infoRecord"]["InfoNameEn"]; ?>" id="engName" required>
				</div>
			</div>
			<div class="form-group">
				<label for="chiInfoName" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Name"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="chiInfoName" value="<?php if (isset($data["infoRecord"]["InfoNameChi"])) echo $data["infoRecord"]["InfoNameChi"]; ?>" id="chiName" required>
				</div>
			</div>
			<div class="form-group">
				<label for="engInfoType" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Type"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="engInfoType" value="<?php if (isset($data["infoRecord"]["TypeNameEn"])) echo $data["infoRecord"]["TypeNameEn"]; ?>" id="engInfoType" required>
				</div>
			</div>
			<div class="form-group">
				<label for="chiInfoType" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Type"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="chiInfoType" value="<?php if (isset($data["infoRecord"]["TypeNameChi"])) echo $data["infoRecord"]["TypeNameChi"]; ?>" id="chiInfoType" required>
				</div>
			</div>
			<div class="form-group">
				<label for="engInfoRemark" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Remarks"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="engInfoRemark" value="<?php if (isset($data["infoRecord"]["InfoRemarkEn"])) echo $data["infoRecord"]["InfoRemarkEn"]; ?>" id="engInfoRemark">
				</div>
			</div>
			<div class="form-group">
				<label for="chiInfoRemark" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Remarks"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="chiInfoRemark" value="<?php if (isset($data["infoRecord"]["InfoRemarkChi"])) echo $data["infoRecord"]["InfoRemarkChi"]; ?>" id="chiInfoRemark">
				</div>
			</div>
			<div class="form-group uploadFileArea">
				<label for="" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["RelatedFiles"]; ?></label>
				<div class="col-field">
					<div id="existfilelist" rel='<?php echo $data["section"]; ?>.filelist'><?php
					if (isset($data["infoRecordFiles"]) && count($data["infoRecordFiles"]) > 0) {
						echo '<ul id="extFiles" class="uploaded">';
						foreach ($data["infoRecordFiles"] as $kk => $vv) {
							echo '<li><div class="btnDelfile" rel="' . $data["section"] . '.dfile&flid=' . $vv["FileID"] . '&d=' . $data["currDate"] . '&skey=' . md5($vv["FileID"] . '_' . $vv["FileSize"] . '_' .  $data["currDate"]) .'"> ' . $vv["OrgFileName"] . ' <i class="fa fa-trash" style="cursor: pointer;"></i>  <span></span></div>';
							echo '<input type="hidden" name=files[] value="' . $vv["FileID"] . '"></li>';
						}
						echo "</ul><br>";
					}
					?></div>
					<div id="drop-target"><?php echo $data["lang"]['TeacherPortfolio']["fileupload_dragupload"]; ?></div>
					<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
					<div id="file_container">
						<a id="pickfiles" href="#" class="btn-upload"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["fileupload_selectfiles"]; ?></a> 
						<!-- <a id="uploadfiles" href="#" class="btn-upload"><i class="fa fa-arrow-circle-o-up"></i> Upload files</a> -->
						<div class="console"></div>
					</div>
				</div>
			</div>
			<div class="form-group buttons">
				<div class="col-form-label"></div>
				<div class="col-field">
					<a href="#" class="btn btn-primary btnSubmit"><?php echo $data["lang"]['TeacherPortfolio']["Submit"]; ?></a>
					<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-secondary btnBack"><?php echo $data["lang"]['TeacherPortfolio']["Cancel"]; ?></a>
					<?php if ($data["currAction"]=='edit') { ?><a href="#" class="btn btnDel btn-danger" rel="<?php echo $data["section"]; ?>" ><?php echo $data["lang"]['TeacherPortfolio']["Delete"]; ?></a><?php } ?>
				</div>
			</div>
			</form>
		</div>
		<?php echo $data["defaultFooter"]; ?>
	</div>
	<?php } ?>
</div>