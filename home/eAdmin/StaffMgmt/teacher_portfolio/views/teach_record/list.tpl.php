<div class="toolbar">
	<div class="filter btn-group">
		<?php echo $data["html_YearPanel"]; ?>
	</div>
	<ul class="actions">
		<li><a href="#" rel="<?php echo str_replace("_", "-", $data["section"]); ?>" class="btnAjax label label-info"><?php echo $data["lang"]["TeacherPortfolio"]["btnClass"]; ?></a></li>
		
		<li><a href="#" rel="<?php echo str_replace("_", "-", $data["section"]); ?>/subjectlist" class="btnAjax label label-default"><?php echo $data["lang"]["TeacherPortfolio"]["btnSubject"]; ?></a></li>
	
		<li><a href="#" rel="<?php echo str_replace("_", "-", $data["section"]); ?>.exportrecord&sec=<?php echo $data["currTable"]; ?>" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content teach-record">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["btnClass"]; ?></span>
	</div><br><br>
	<table id="teach-record-bdTable" class="tp-table bdTable bdTable" cellspacing="0" width="100%">
		<thead>
			<tr>
			<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["_ROLE_"]["isViewerMode"] && $data["_ROLE_"]["hasViewerRight"]) { ?>
				<th class='tbheader_col' rel='TeacherName'><?php echo $data["lang"]["TeacherPortfolio"]["TeacherName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
			<?php } ?>
				<th class='tbheader_col' style='width:150px' rel='academicYear'><?php echo $data["lang"]["TeacherPortfolio"]["SchoolYear"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
				<th class='tbheader_col' style='width:150px' rel='course'><?php echo $data["lang"]["TeacherPortfolio"]["Course"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:70px' rel='func'>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br><br>
	<?php if (isset($data["debugMsg"])) echo $data["debugMsg"]; ?>
</div>