<div class="toolbar">
	<div class="filter btn-group">
		<?php echo $data["html_YearPanel"]; ?>
	</div>
	<ul class="actions">                      
		<li><a href="#" class="btnExport"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="#" class="btnPrint printAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content teaching-record">
	<table id="activities-bdTable" class="tp-table bdTable" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class='tbheader_col' style='width:15%' rel='academicYear'><?php echo $data["lang"]["TeacherPortfolio"]["SchoolYear"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
				<th class='tbheader_col' style='width:15%' rel='semester'><?php echo $data["lang"]["TeacherPortfolio"]["Semester"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:35%' rel='eventname'><?php echo $data["lang"]["TeacherPortfolio"]["EventName"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col' style='width:35%' rel='remarks'><?php echo $data["lang"]["TeacherPortfolio"]["Remarks"]; ?>  <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	<?php if (isset($data["debugMsg"])) echo $data["debugMsg"]; ?>
</div>