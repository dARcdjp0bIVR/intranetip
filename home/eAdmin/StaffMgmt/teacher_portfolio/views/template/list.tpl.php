<?
/*
 * 	Using:
 * 	2019-02-21 Paul
 * 		- Move "eclass logo" out of the content div
 * 	2017-11-09 Cameron
 * 		- revise access right for add and import button
 * 
 * 	2017-11-08 Cameron
 * 		- add Add button for cpd
 */
 
?>
<div class="toolbar">
	<div class="filter btn-group">
		<?php echo $data["html_YearPanel"]; ?>
	</div>
	<ul class="actions">
<?php
if (($data["section"]=="cpd") && (($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) || ($data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"] && $data["userInfo"]["tpConfigs"]["allowOwnerAddOrEditCpd"] == "Y"))) {
?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/addrecord" class="btnAjax"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnAddNew"]; ?></a></li>
<?php } ?>
<?php
if (($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) || (($data["section"]=="cpd") && $data["_ROLE_"]["isOwnerMode"] && $data["_ROLE_"]["hasOwnerRight"] && $data["userInfo"]["tpConfigs"]["allowOwnerAddOrEditCpd"] == "Y")) {
//if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["section"]=="cpd" && $data["_CUSTOMIZATION_"]["allowImportCPDByIndividualTeacher"] && $data["_ROLE_"]["isOwnerMode"]) { 
?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/settings" class="btnAjax"><i class="glyphicon glyphicon-cloud-upload"></i> <?php echo $data["lang"]["TeacherPortfolio"]["Import"]; ?></a></li>
<?php } ?>
		<li><a href="#" rel="<?php echo $data["section"]; ?>.exportrecord" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="?task=<?php echo $data["section"]; ?>.printall" class="printAllAction"><i class="glyphicon glyphicon-print"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnPrint"]; ?></a></li>
	</ul>
</div>
<div class="content ftpl">
	<form id='ftpl-form'>
	<table id="ftpl-bdTable-<?php echo $data["section"]; ?>" class="tp-table bdTable table-hover" cellspacing="0" width="100%" <?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { echo "data-searching='Y'"; } ?>>
		<thead>
			<tr valign="bottom">
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col ' width="10%" rel='TeacherName'><nobr><?php echo $data["lang"]["TeacherPortfolio"]["TeacherName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></nobr></th><!-- use "fa-sort-asc" for ascending order-->
<?php
				}
				if (isset($data["formTplInfo"]["childs"]) && count($data["formTplInfo"]) > 0) {
					foreach ($data["formTplInfo"]["childs"] as $kk => $val) {
						if ($val["showInGrid"] == "Y") {
							$headerStyle = "width='".(100/count($data["formTplInfo"]["childs"]))."%'";
?>
				<th class='tbheader_col<?php if ($val["FieldType"] == "INT" || $val["FieldType"] == "FLOAT") echo " text-center-ignore"; ?>' <?php echo $headerStyle;?> rel='<?php echo $val["FieldLabel"]; ?>'><nobr><?php echo Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]); ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></nobr></th><!-- use "fa-sort-asc" for ascending order-->
<?php
						}
					}
				} ?>
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
				<th class='tbheader_col' width="10%" rel='DateModified' data-id='ftpl-form'><nobr><?php echo $data["lang"]["TeacherPortfolio"]["LastUpdate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
<?php } ?>
<?php 
				if ($data["TemplateType"] == "GRID") {
?>
				<th class='tbheader_col text-center' width="10%" rel='func' data-id='ftpl-form'>&nbsp;</th>
<?php
				} else if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) {
?>
				<th class='tbheader_col text-center' width="10%" rel='func' data-id='ftpl-form'><a href="#" class="btn btn-xs btn-primary btnTableSelectAll"><?php echo $data["lang"]["TeacherPortfolio"]["SelectAll"]; ?></a> <a href="#" class="btn btn-xs btn-danger btnTableDelete" rel='ftpl-form'><?php echo $data["lang"]["TeacherPortfolio"]["Delete"]; ?></a></th>
<?php
				}
?>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	</form>
	<?php if (($data["section"]=="cpd") && ($data["_CUSTOMIZATION_"]["ltmpsStyle"]==true)){
	?>
	<b>註（Ⅰ）專業進修範疇</b><br>
	<ol>
	 	<li>教與學方面的進修： 學科內容知識 / 課程及教學內容知識  / 教學策略 / 評核及評估</li>
	    <li>學生發展方面的進修： 學生在校內的不同需要  / 與學生建立互信關係  / 學生關顧  / 學生的多元學習經歷</li>
	    <li>學校發展的進修： 學校願景，文化及校風 / 校政 / 家庭與學校協作 / 回應社會變革</li>
	    <li>專業群體關係及服務方面的進修： 校內協作關係  / 教師專業發展 / 教育政策的參與 / 與教育有關的社區服務及志願工作</li>
	    <li>教師成長及發展的進修： 非與教學直接相關，但有助教師作全人發展的活動 （e.g. 減壓工作坊，領導才能訓練等）</li>
	</ol>         
	<b>註（Ⅱ）配合學校關注事項</b>
	<?php	
	}?>
</div>
<?php echo $data["defaultFooter"]; ?>