<div class="toolbar">
	<ul class="actions left">
		<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
	</ul>
</div>
<div class="content attendance settings">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["Format_Settings"]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?> > <?php echo $data["lang"]["TeacherPortfolio"]["Settings"]; ?></span>
	</div><br>
	<div class="container-fluid <?php echo $data["currMethod"]; ?>">
		<div class="row">
			<div class="col-sm-12">
				<ul class='settingnav pull-right'>
				<?php
					if (count($data["settings_menu"]) > 1) {
						foreach ($data["settings_menu"] as $mkk => $kvv) {
				?>
							<li><a href="#" rel="<?php echo $data["section"]; ?>/<?php echo $mkk; ?>" class="btn btn-sm btnAjax btn-<?php echo ($data["currMethod"] == $mkk) ? 'primary': 'default'; ?>"><?php echo $data["lang"]["TeacherPortfolio"][$kvv]; ?></a></li>
				<?php
						}
					}
				?>
				</ul>
			</div>
		</div><br>
		<div class="row">
			<div class="col-sm-12">
				<div class='row'>
					<div class="col-md-6">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4><?php echo $data["lang"]["TeacherPortfolio"]["settings_DataColumnSorting"]; ?></h4>
							</div>
							<div class="panel-body">
								<ol class='cusSort listhdr'>
						<?php
							if (isset($data["formTplInfo"]["childs"]) && count($data["formTplInfo"]) > 0) {
								foreach ($data["formTplInfo"]["childs"] as $kk => $val) {
									if ($val["FixedItem"] == "Y") {
										echo "<li class='static' data-label='" . $val["FieldLabel"] . "' data-nameCN='" . $val["FieldNameChi"] . "' data-nameEN='" . $val["FieldNameEn"] . "' id='field_" . $val["RelationID"] . "'><span class='glyphicon glyphicon-pushpin'></span> " . Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]) . "</li>";
									} else {
										echo "<li data-label='" . $val["FieldLabel"] . "' data-nameCN='" . $val["FieldNameChi"] . "' data-nameEN='" . $val["FieldNameEn"] . "' id='field_" . $val["RelationID"] . "'><span class='glyphicon glyphicon-resize-vertical'></span> " . Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]) . "</li>";
									}
								}
							}
						?>
								</ol><br>
								<a href="#" class='btn btn-sm btn-primary btnFormFieldSubmit'><?php echo $data["lang"]["TeacherPortfolio"]["Submit"]; ?></a>
								<a href="#" class='btn btn-sm btn-default btnAjax' rel="<?php echo $data["section"]; ?>/<?php echo $data["currMethod"]; ?>"><?php echo $data["lang"]["TeacherPortfolio"]["Reset"]; ?></a>
								<br><small class='txt_remarks'><i><span class='glyphicon glyphicon-resize-vertical'></span> <?php echo $data["lang"]["TeacherPortfolio"]["settings_dragtosort"]; ?></i></small>
							</div>
						</div><br>
					</div>
					<div class="col-md-6">
						<div class="panel panel-info">
							<div class="panel-heading">
								<h4><?php echo $data["lang"]["TeacherPortfolio"]["settings_ExistingColumn"]; ?></h4>
							</div>
							<div class="panel-body">
								<ol class='cusSort editing'>
						<?php
							if (isset($data["formTplInfo"]["extChilds"]) && count($data["formTplInfo"]) > 0) {
								foreach ($data["formTplInfo"]["extChilds"] as $kk => $val) {
									echo "<li data-label='" . $val["FieldLabel"] . "' data-nameCN='" . $val["FieldNameChi"] . "' data-nameEN='" . $val["FieldNameEn"] . "' id='field_" . $val["RelationID"] . "'><span class='glyphicon glyphicon-resize-vertical'></span> " . Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]) . "</li>";
								}
							}
						?>
								</ol>
								<small class='txt_remarks'><i><span class="glyphicon glyphicon-asterisk"></span> <?php echo $data["lang"]["TeacherPortfolio"]["settings_dragtoedit"]; ?></i></small>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php echo $data["defaultFooter"]; ?>
</div>