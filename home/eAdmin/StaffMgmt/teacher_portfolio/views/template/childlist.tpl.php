<?php
if ($data["loadbydt"] == "Y") {
	$table_id = $data["section"];
} else {
	$table_id = time();
}
if ($data["loadbydt"] == "Y") { ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
<?php } else { ?>
<div class="childDetail" id='ftpl-childform-<?php echo $table_id; ?>'>
<?php } ?>
		<form id='ftpl-childform-<?php echo $table_id; ?>' rel="<?php echo $data["section"]; ?>">
<?php if ($data["loadbydt"] == "Y") { ?>
			<table id="ftpl-bdChildTable-<?php echo $data["section"]; ?>-<?php echo $data["getData"]["id"]; ?>" data-section="<?php echo $data["section"]; ?>" data-search="<?php echo $data["searchArr"]; ?>" class="tp-table bdTable table-hover" cellspacing="0" width="100%">
<?php } else {?>
			<table class="childtable table table-hover" cellspacing="0">
<?php } ?>
			<thead>
			<tr valign="bottom">
<?php
				if (isset($data["formTplInfo"]["childs"]) && count($data["formTplInfo"]) > 0) {
					$tdNum = 0;
					foreach ($data["formTplInfo"]["childs"] as $kk => $val) {
						if ($val["showInGrid"] == "Y") {
							$tdNum++;
							$headerStyle = "width='".(100/count($data["formTplInfo"]["childs"]))."%'";
?>
				<th class='tbheader_col<?php if ($val["FieldType"] == "INT" || $val["FieldType"] == "FLOAT") echo " text-center-ignore"; ?>' <?php echo $headerStyle;?> rel='<?php echo $val["FieldLabel"]; ?>'><nobr><?php echo Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]); ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></nobr></th><!-- use "fa-sort-asc" for ascending order-->
<?php
						}
					}
				}
				if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["section"]=="cpd" && $data["_CUSTOMIZATION_"]["allowImportCPDByIndividualTeacher"] && $data["_ROLE_"]["isOwnerMode"]) {
					$tdNum++;
					$tdNum++;
?>
				<th class='tbheader_col' width="10%" rel='DateModified'><nobr><?php echo $data["lang"]["TeacherPortfolio"]["LastUpdate"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th>
				<th class='tbheader_col text-center' width='10%' rel='func' data-id='ftpl-childform-<?php echo $table_id; ?>'><a href="#" class="btn btn-xs btn-primary btnTableSelectAll"><small><?php echo $data["lang"]["TeacherPortfolio"]["SelectAll"]; ?></small></a> <a href="#" class="btn btn-xs btn-danger btnTableDelete" rel='ftpl-childform-<?php echo $table_id; ?>'><small><?php echo $data["lang"]["TeacherPortfolio"]["Delete"]; ?></small></a></th>
<?php
				}
?>
			</tr>
			</thead>
<?php
if ($data["loadbydt"] == "Y") {
?>
			<tbody></tbody>
<?php
} else {
?>
			<tbody>
<?php
	if (isset($data["recData"]["data"]) && count($data["recData"]["data"]) > 0) {
		foreach ($data["recData"]["data"] as $recIndex => $ONE) {
			echo "<tr>";
			foreach ($data["formTplInfo"]["childs"] as $kk => $val) {
				if ($val["showInGrid"] == "Y") {
					if ($val["isMultiLang"] == "Y") {
						$tdVal = Get_Lang_Selection($ONE[$val["FieldLabel"] . "Chi"], $ONE[$val["FieldLabel"] . "En"]);
						if (empty($tdVal)) $tdVal = "-";
						echo "<td>" . $tdVal . "</td>";
					} else {
						$tdVal = $ONE[$val["FieldLabel"]];
						if (empty($tdVal)) $tdVal = "-";
						echo "<td>" . $tdVal . "</td>";
					}
		 		}
			}
			if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"] || $data["section"]=="cpd" && $data["_CUSTOMIZATION_"]["allowImportCPDByIndividualTeacher"] && $data["_ROLE_"]["isOwnerMode"]) {
				echo "<td>" . $ONE["DateModified"] . "</td>";
				echo "<td>" . $ONE["func"] . "</td>";
			}
			echo "</tr>";
		}
	} else {
?>
				<tr>
					<td colspan="<?php echo $tdNum; ?>"><?php echo $data["lang"]["TeacherPortfolio"]["dataTable_emptyTable"]; ?></td>
				</tr>
<?php } ?>
			</tbody>
<?php } ?>
			</table>
		</form>
<?php if ($data["loadbydt"] == "Y") { ?>
		</div>
	</div>
<?php } ?>
</div>