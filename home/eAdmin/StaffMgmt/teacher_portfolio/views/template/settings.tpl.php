<div class="toolbar">
	<ul class="actions left">
		<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
	</ul>
</div>
<div class="content attendance settings">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["Import"]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; /* ?> > <?php echo $data["lang"]["TeacherPortfolio"]["Settings"]; */ ?></span>
	</div><br>
	<div class="container-fluid <?php echo $data["currMethod"]; ?>">
		<div class="row">
			<div class="col-sm-12">
				<ul class='settingnav pull-right'>
				<?php
					if (count($data["settings_menu"]) > 1) {
						foreach ($data["settings_menu"] as $mkk => $kvv) {
				?>
							<li><a href="#" rel="<?php echo $data["section"]; ?>/<?php echo $mkk; ?>" class="btn btn-sm btnAjax btn-<?php echo ($data["currMethod"] == $mkk) ? 'primary': 'default'; ?>"><?php echo $data["lang"]["TeacherPortfolio"][$kvv]; ?></a></li>
				<?php
						}
					}
				?>
				</ul>
			</div>
		</div><br>
		<div class="row">
			<div class="col-sm-12">
				<div class="container-fluid">
					<div class="row bs-wizard">
						<div class="col-xs-4 bs-wizard-step active">
							<div class="text-center bs-wizard-stepnum"><?php echo $data["lang"]["TeacherPortfolio"]["settings_step1"]; ?></div>
							<div class="progress"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center"><?php echo $data["lang"]["TeacherPortfolio"]["settings_step1_msg"]; ?></div>
						</div>
						<div class="col-xs-4 bs-wizard-step disabled"><!-- complete -->
							<div class="text-center bs-wizard-stepnum"><?php echo $data["lang"]["TeacherPortfolio"]["settings_step2"]; ?></div>
							<div class="progress"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center"><?php echo $data["lang"]["TeacherPortfolio"]["settings_step2_msg"]; ?></div>
						</div>
						<div class="col-xs-4 bs-wizard-step disabled"><!-- complete -->
							<div class="text-center bs-wizard-stepnum"><?php echo $data["lang"]["TeacherPortfolio"]["settings_step3"]; ?></div>
							<div class="progress"><div class="progress-bar"></div></div>
							<a href="#" class="bs-wizard-dot"></a>
							<div class="bs-wizard-info text-center"><?php echo $data["lang"]["TeacherPortfolio"]["settings_step3_msg"]; ?></div>
						</div>
					</div>
					<div class="steppanel">
					<form id="editform">
						<?php
							if ($data["formTplInfo"]["childs"] != null && count($data["formTplInfo"]["childs"]) > 0) { ?>
							<div class="panel panel-info">
								<div class="panel-heading">
									<h4><?php echo $data["lang"]["TeacherPortfolio"]["settings_step1_msg"]; ?></h4>
								</div>
								<div class="panel-body uploadFileArea">
									<div class="form-group">
										<div for="uploadfile" class="col-sm-3 cust-label"><?php echo $data["lang"]["TeacherPortfolio"]["settings_selectedfile"]; ?></div>
										<div class="col-sm-9">
											<div id="drop-target"><?php echo $data["lang"]['TeacherPortfolio']["fileupload_dragupload"]; ?></div>
											<div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>
											<div id="file_container" rel='<?php if (isset($data["fileLimit"])) { echo $data["fileLimit"]; } else { echo 0; } ?>'>
												<a id="pickfiles" href="#" class="btn-upload"><i class="fa fa-plus"></i> <?php echo $data["lang"]["TeacherPortfolio"]["fileupload_selectfile"]; ?></a> 
												<!-- <a id="uploadfiles" href="#" class="btn-upload"><i class="fa fa-arrow-circle-o-up"></i> Upload files</a> -->
												<div class="console"></div>
											</div>
										</div>
									</div><br>
									<div class="form-group">
										<div class="col-sm-3 cust-label"><?php echo $data["lang"]["TeacherPortfolio"]["settings_samplefile"]; ?></div>
										<div class="col-sm-9">
											<a href='#' class='btnFDAjax btn btn-primary btn-sm'  rel="<?php echo $data["section"]; ?>.dfile&flid=sample"><?php echo $data["lang"]["TeacherPortfolio"]["settings_download"]; ?></a>
										</div>
									</div><br>
									<div class="form-group">
										<div class="col-sm-3 cust-label"><?php echo $data["lang"]["TeacherPortfolio"]["settings_DataColumn"]; ?></div>
										<div class="col-sm-9">
											<ul>
										<?php
											$i = 65;
											foreach ($data["formTplInfo"]["childs"] as $kk => $val) {
												$status_icon = "";
												if($data["section"]=="cpd" && $data["_CUSTOMIZATION_"]['ltmpsStyle']==true && $val["FieldLabel"]=="EventType"){
													$val["FieldStatus"] = "normal";
												}
												if ($val["FieldStatus"] == "required") {
													$status_icon = " <span class='text-danger'>*</span>";
												} else if ($val["FieldStatus"] == "reference") {
													$status_icon = " <span class='text-danger'>^</span>";
												}
												$legendGuide = "";
												if($data["_CUSTOMIZATION_"]["useEDBCPD"] && $data["formTplInfo"]["TemplateName"]=="CPD"){
													$searchParam = base64_encode($kk);
													if($kk=="CPDMode" || $kk=="CPDDomain" || $kk=="SubjectRelated"){														
														$legendGuide = "<a href=\"#\" class=\"tablelink btnModal\" title=\"".Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"])."\" rel=\"?task=".$data['section'].".codelist&s=".$searchParam."\">[".$data["lang"]["TeacherPortfolio"]["view_code_list"]."]</a><span id=\"".$kk."_click\">&nbsp;</span>";
													}													
												}
												if ($val["isMultiLang"] == "Y") {
													echo "<li>" . $data["lang"]["TeacherPortfolio"]["settings_Column"] . " " . chr($i) . $status_icon . " : " . Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]) . " (" . $data["lang"]["TeacherPortfolio"]["lang_eng"] . ")" . $legendGuide ."</li>";
													$i++;
													echo "<li>" . $data["lang"]["TeacherPortfolio"]["settings_Column"] . " " . chr($i) . $status_icon . " : " . Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]) . " (" . $data["lang"]["TeacherPortfolio"]["lang_chi"] . ")" . $legendGuide ."</li>";
													
												} else {
													echo "<li>" . $data["lang"]["TeacherPortfolio"]["settings_Column"] . " " . chr($i) . $status_icon . " : " . Get_Lang_Selection($val["FieldNameChi"], $val["FieldNameEn"]) . $legendGuide . "</li>";
												}
												$i++;
											}
										?>
											</ul>
											
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12"><small><?php echo $data["lang"]["TeacherPortfolio"]["settings_requiredfield"]; ?></small></div>
										<div class="col-sm-12"><small><?php echo $data["lang"]["TeacherPortfolio"]["settings_referencefield"]; ?></small></div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<br><a href="#" class="btn btn-danger btn-sm btnAjax" rel="<?php echo $data["section"]; ?>/settings"><?php echo $data["lang"]['TeacherPortfolio']["Reset"]; ?></a>
										</div>
									</div>
								</div>
							</div>
						<?php } else { ?>
							<?php echo $data["lang"]["TeacherPortfolio"]["settings_noColumnInfo"]; ?>
						<?php } ?>
					</form>
					</div>
					<div class="steppanel" rel='<?php echo $data['section']; ?>.checkcsv'>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4><kbd><?php echo $data["lang"]["TeacherPortfolio"]["settings_step1"]; ?></kbd> <?php echo $data["lang"]["TeacherPortfolio"]["settings_step1_msg"]; ?></h4>
							</div>
							<div class="panel-body">
								<h3 class='processing'><i class="fa fa-refresh fa-spin fa-fw"></i> <?php echo $data["lang"]["TeacherPortfolio"]["settings_DataProcessing"]; ?></h3>
							</div>
							<div class="panel-footer">
								<a href="#" class='btn btn-sm btn-success'><?php echo $data["lang"]["TeacherPortfolio"]["Continue"]; ?></a>
								<a href="#" class='btn btn-sm btn-danger btnAjax' rel='<?php echo $data["section"]; ?>/settings'><?php echo $data["lang"]["TeacherPortfolio"]["Cancel"]; ?></a>
							</div>
						</div>
					</div>
					<div class="steppanel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4><kbd><?php echo $data["lang"]["TeacherPortfolio"]["settings_step2"]; ?></kbd> <?php echo $data["lang"]["TeacherPortfolio"]["settings_step2_msg"]; ?></h4>
							</div>
							<div class="panel-body">
								<h3 class='processing'><i class="fa fa-refresh fa-spin fa-fw"></i> <?php echo $data["lang"]["TeacherPortfolio"]["settings_DataProcessing"]; ?></h3>
							</div>
							<div class="panel-footer">
								<a href="#" class='btn btn-sm btn-success'><?php echo $data["lang"]["TeacherPortfolio"]["Continue"]; ?></a>
								<a href="#" class='btn btn-sm btn-danger btnAjax' rel='<?php echo $data["section"]; ?>/settings'><?php echo $data["lang"]["TeacherPortfolio"]["Cancel"]; ?></a>
							</div>
						</div>
					</div>
					<div class="steppanel" rel='<?php echo $data['section']; ?>.importnow'>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4><kbd><?php echo $data["lang"]["TeacherPortfolio"]["settings_step3"]; ?></kbd> <?php echo $data["lang"]["TeacherPortfolio"]["settings_step3_msg"]; ?></h4>
							</div>
							<div class="panel-body">
								<h3 class='processing'><i class="fa fa-refresh fa-spin fa-fw"></i> <?php echo $data["lang"]["TeacherPortfolio"]["settings_DataProcessing"]; ?></h3>
							</div>
							<div class="panel-footer">
								<a href="#" class='btn btn-sm btn-danger btnAjax' rel='<?php echo $data["section"]; ?>/settings'><?php echo $data["lang"]["TeacherPortfolio"]["Cancel"]; ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div>
	<iframe id="dfileframe" style="display:none;"></iframe>
	<?php echo $data["defaultFooter"]; ?>
</div>