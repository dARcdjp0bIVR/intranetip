<div class="toolbar">
	<ul class="actions">
		<li><a href="#" rel="<?php echo $data["section"]; ?>/configs" class="btnAjax"><i class="fa fa-cogs"></i> <?php echo $data["lang"]["TeacherPortfolio"]["manage_configs"]; ?></a></li>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/addrecord" class="btnAjax"><i class="fa fa-user"></i> <?php echo $data["lang"]["TeacherPortfolio"]["manage_addUser"]; ?></a></li>
		<li class='hidden'><a href="#" rel="<?php echo $data["section"]; ?>.exportrecord" class="btnFDAjax"><i class="glyphicon glyphicon-save-file"></i> <?php echo $data["lang"]["TeacherPortfolio"]["btnExport"]; ?></a></li>
		<li><a href="#" rel="<?php echo $data["section"]; ?>/logrecord" class="btnAjax"><i class="fa fa-database"></i> <?php echo $data["lang"]["TeacherPortfolio"]["manage_logRecord"]; ?></a></li>
	</ul>
</div>
<div class="content manage">
	<div class="title">
		<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"]["sec_manage"]; ?></span> <span class="text2"><?php echo $data["lang"]["TeacherPortfolio"]["sec_" . $data["section"]]; ?></span>
	</div><br>
	<form id='ftpl-form'>
	<table id="manage-bdTable" class="tp-table bdTable" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class='tbheader_col' rel='TeacherName'><?php echo $data["lang"]["TeacherPortfolio"]["manage_userName"]; ?> <span class="tp-sort"><i class="fa fa-unsorted"></i><i class="fa fa-sort-asc"></i><i class="fa fa-sort-desc"></i></span></th><!-- use "fa-sort-asc" for ascending order-->
<?php 
			if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) {
?>
				<th class='tbheader_col text-center' style='width:20%;' rel='func' data-id='ftpl-form'><a href="#" class="btn btn-xs btn-primary btnTableSelectAll"><?php echo $data["lang"]["TeacherPortfolio"]["SelectAll"]; ?></a> <a href="#" class="btn btn-xs btn-danger btnTableDelete" rel='ftpl-form'><?php echo $data["lang"]["TeacherPortfolio"]["Delete"]; ?></a></th>
<?php
				}
?>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table><br>
	</form>
	<?php echo $data["defaultFooter"]; ?>
</div>