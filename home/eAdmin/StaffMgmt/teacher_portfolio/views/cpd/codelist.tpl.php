<div class="content cpd">
	<table class="tp-table bdTable" cellspacing="0" width="100%">
	<?php
	if($data["searchArr"]=="CPDMode"){
	?>
		<thead>
			<tr>
				<th class='tbheader_col' style='width:15%'><?php echo $data["lang"]['TeacherPortfolio']['code'] ; ?> </th>
				<th class='tbheader_col' style='width:15%'><?php echo $data["lang"]['TeacherPortfolio']['cpd_mode'] ; ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>0</td>
				<td><?php echo $data["lang"]['TeacherPortfolio']["edit_cpd_mode_systematic_scheme"];?></td>
			</tr>
			<tr>
				<td>1</td>
				<td><?php echo $data["lang"]['TeacherPortfolio']["edit_cpd_mode_others"];?></td>
			</tr>
		</tbody>
	<?php
	}
	?>
	<?php
	if($data["searchArr"]=="CPDDomain"){
	?>
		<thead>
			<tr>
				<th class='tbheader_col' style='width:15%'><?php echo $data["lang"]['TeacherPortfolio']['position'] ; ?></th>
				<th class='tbheader_col' style='width:15%'><?php echo $data["lang"]['TeacherPortfolio']['cpd_domain'] ; ?></th>
			</tr>
		</thead>
		<tbody>			
			<?php
				for($idx=0;$idx < count($data["lang"]['TeacherPortfolio']['edit_cpd_domain']); $idx++)
				{				
			?>
					<tr>
						<td><?php echo ($idx+1);?></td>
						<td><?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_domain'][$idx];?></td>
					</tr>
			<?php		
				}
			?>
				<tr><td colspan="2"><?php echo $data["lang"]['TeacherPortfolio']['example'];?>: 0,1,0,0,1,1 = <?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_domain'][1];?>, <?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_domain'][4];?>, <?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_domain'][5];?></td></tr>
		</tbody>	
	<?php
	}
	?>
	<?php
	if($data["searchArr"]=="SubjectRelated"){
	?>
		<thead>
			<tr>
				<th class='tbheader_col' style='width:15%'><?php echo $data["lang"]['TeacherPortfolio']['position'] ; ?></th>
				<th class='tbheader_col' style='width:15%'><?php echo $data["lang"]['TeacherPortfolio']['cpd_subject'] ; ?></th>
			</tr>
		</thead>
		<tbody>			
			<?php
				$example = "";
				for($idx=0;$idx < count($data["lang"]['TeacherPortfolio']['edit_cpd_subject']); $idx++)
				{
					if($idx==0){
						$example = "0";
					}elseif($idx==1 || $idx==4 || $idx==5){
						$example .= ",1";
					}else{
						$example .= ",0";
					}				
			?>
					<tr>
						<td><?php echo ($idx+1);?></td>
						<td><?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_subject'][$idx];?></td>
					</tr>
			<?php		
				}
			?>
				<tr><td colspan="2"><?php echo $data["lang"]['TeacherPortfolio']['example'];?>: <?php echo $example;?>= <?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_subject'][1];?>, <?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_subject'][4];?>, <?php echo $data["lang"]['TeacherPortfolio']['edit_cpd_subject'][5];?></td></tr>
		</tbody>	
<?php
}
?>
	</table><br>
	<?php if (isset($data["debugMsg"])) echo $data["debugMsg"]; ?>
</div>