<?
/*
 * 2017-11-16 Cameron
 * 		- NoOfSection should allow float number
 */
	$fieldTitle = $data["formTplInfo"]["childs"];
?>

<!--CONTENT: DEVELOP - EDIT-->
<div id="others-edit" class="tab-pane fade in active">
	<div class="toolbar">                
		<ul class="actions left">
			<li><a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax"><i class="fa fa-caret-left"></i> <?php echo $data["lang"]["TeacherPortfolio"]["back"]; ?></a></li>
		</ul>
	</div>
<?php
	if ($data["currAction"] == "edit" && $data["infoRecord"] == "no record") {
?>
		<div class="content">
			<?php echo $data["lang"]["TeacherPortfolio"]["noThisRecord"]; ?>
		</div>
<?php
	} else {
?>
	<div class="content cert-form">
		<form id="editform" rel="<?php echo $data["section"]; ?>/process">
		<?php if ($data["currAction"] == "edit") { ?>
			<input type='hidden' name='action' value='updaterecord'>
			<input type='hidden' name='recid' value='<?php echo $data["infoRecord"]["FTDataRowID"]; ?>'>
		<?php } else { ?>
			<input type='hidden' name='action' value='insertrecord'>
		<?php } ?>
		<div class="title">
			<span class="text1"><?php echo $data["lang"]["TeacherPortfolio"][$data["currAction"]]; ?></span><span class="text2"><?php echo $data["lang"]['TeacherPortfolio']["sec_cpd"]; ?></span>
		</div>
		<div class="body">
<?php if ($data["_ROLE_"]["isAdminMode"] && $data["_ROLE_"]["hasAdminRight"]) { ?>
			<div class="form-group">
				<label for="selectedUser" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Teacher"]; ?></label>
				<div class="col-field">
					<select class="form-control input-sm" name='SelectedUserID'>
<?php
			if (count($data["allTeachers"]) > 0) {
?>
				<option value='0'><?php echo $data["lang"]['TeacherPortfolio']["pleaseSelectTeacher"]; ?></option>
<?php
				foreach ($data["allTeachers"] as $tkk => $tvv) {
					echo "<option value='" . $tvv["UserID"] . "_" . $tvv["cs"] . "'";
					if ($data["currAction"] == "edit") {
						if ($data["infoRecord"]["UserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					} else {
						if ($data["userInfo"]["info"]["SelectedUserID"] == $tvv["UserID"]) {
							echo " selected";
						}
					}
					echo ">" . Get_Lang_Selection($tvv["ChineseName"], $tvv["EnglishName"]) . " ( LoginID: " . $tvv["UserLogin"]. " )</option>";
				}
				} ?></select>
				</div>
			</div>		
<?php } else {
		echo '<input type="hidden" name="SelectedUserID" value="'.$_SESSION['UserID'].'_'.time().'">';
	}
?>
			<div class="form-group">
				<label for="year" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["SchoolYear"]; ?></label>
				<div class="col-field">
					<select class="form-control inputstl" name="AcademicYear" id="year" required>
					<option value=""><?php echo $data["lang"]["TeacherPortfolio"]["pleaseSelect"]; ?></option>
<?php
					if (count($data["yearArr"]) > 0) {
						foreach ($data["yearArr"] as $yearID => $year) {
?>
					<option value="<?php echo $yearID; ?>"<?php if (isset($data["infoRecord"]["AcademicYear"]) && $data["infoRecord"]["AcademicYear"]==$year['YearNameEN']) { ?> selected<?php } ?>><?php echo Get_Lang_Selection($year["YearNameB5"], $year["YearNameEN"]); ?></option>
<?php
						}
					}
?>
					</select>
				</div>
			</div>
			
			<div class="form-group">
				<label for="date" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["StartDate"]["FieldNameChi"], $fieldTitle["StartDate"]["FieldNameEn"])?></label>
				<div class="col-field">
					<div class="input-group date" id="datetimepicker2">
						<input class="form-control date" id="StartDate" name="StartDate" placeholder="YYYY-MM-DD" value="<?php if (isset($data["infoRecord"]["StartDate"])) echo $data["infoRecord"]["StartDate"]; ?>" type="text" required />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div>    

			<div class="form-group">
				<label for="date" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EndDate"]["FieldNameChi"], $fieldTitle["EndDate"]["FieldNameEn"])?></label>
				<div class="col-field">
					<div class="input-group date" id="datetimepicker2">
						<input class="form-control date" id="EndDate" name="EndDate" placeholder="YYYY-MM-DD" value="<?php if (isset($data["infoRecord"]["EndDate"])) echo $data["infoRecord"]["EndDate"]; ?>" type="text" required />
						<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
			</div> 
			   
			<div class="form-group">
				<label for="NoOfSection" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["NoOfSection"]["FieldNameChi"], $fieldTitle["NoOfSection"]["FieldNameEn"])?></label>
				<div class="col-field">
					<input class="form-control" type="number" name="NoOfSection" value="<?php if (isset($data["infoRecord"]["NoOfSection"])) echo $data["infoRecord"]["NoOfSection"]; ?>" id="NoOfSection" required>
				</div>
			</div>
			
			<div class="form-group">
				<label for="EventNameEn" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventName"]["FieldNameChi"], $fieldTitle["EventName"]["FieldNameEn"]). " (" . $data["lang"]["TeacherPortfolio"]["lang_eng"] . ")"?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventNameEn" value="<?php if (isset($data["infoRecord"]["EventNameEn"])) echo $data["infoRecord"]["EventNameEn"]; ?>" id="EventNameEn" required>
				</div>
			</div>
			
			<div class="form-group">
				<label for="EventNameChi" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventName"]["FieldNameChi"], $fieldTitle["EventName"]["FieldNameEn"]). " (" . $data["lang"]["TeacherPortfolio"]["lang_chi"] . ")"?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventNameChi" value="<?php if (isset($data["infoRecord"]["EventNameChi"])) echo $data["infoRecord"]["EventNameChi"]; ?>" id="EventNameChi" required>
				</div>
			</div>
			
			<div class="form-group">
				<label for="EventTypeEn" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventType"]["FieldNameChi"], $fieldTitle["EventType"]["FieldNameEn"]). " (" . $data["lang"]["TeacherPortfolio"]["lang_eng"] . ")"?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventTypeEn" value="<?php if (isset($data["infoRecord"]["EventTypeEn"])) echo $data["infoRecord"]["EventTypeEn"]; ?>" id="EventTypeEn" required>
				</div>
			</div>

			<div class="form-group">
				<label for="EventTypeChi" class="col-form-label"><?=Get_Lang_Selection($fieldTitle["EventType"]["FieldNameChi"], $fieldTitle["EventType"]["FieldNameEn"]). " (" . $data["lang"]["TeacherPortfolio"]["lang_chi"] . ")"?></label>
				<div class="col-field">
					<input class="form-control" type="text" name="EventTypeChi" value="<?php if (isset($data["infoRecord"]["EventTypeChi"])) echo $data["infoRecord"]["EventTypeChi"]; ?>" id="EventTypeChi" required>
				</div>
			</div>

			<div class="form-group">
				<label for="RemarksEn" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Remarks"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_eng"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="RemarksEn" value="<?php if (isset($data["infoRecord"]["RemarksEn"])) echo $data["infoRecord"]["RemarksEn"]; ?>" id="RemarksEn">
				</div>
			</div>

			<div class="form-group">
				<label for="RemarksChi" class="col-form-label"><?php echo $data["lang"]['TeacherPortfolio']["Remarks"]; ?> (<?php echo $data["lang"]["TeacherPortfolio"]["lang_chi"]; ?>)</label>
				<div class="col-field">
					<input class="form-control" type="text" name="RemarksChi" value="<?php if (isset($data["infoRecord"]["RemarksChi"])) echo $data["infoRecord"]["RemarksChi"]; ?>" id="RemarksChi">
				</div>
			</div>

			<div class="form-group buttons">
				<div class="col-form-label"></div>
				<div class="col-field">
					<a href="#" class="btn btn-primary btnSubmit" rel="needfile"><?php echo $data["lang"]['TeacherPortfolio']["Submit"]; ?></a>
					<a href="#" rel="<?php echo $data["section"]; ?>" class="btnAjax btn btn-secondary btnBack"><?php echo $data["lang"]['TeacherPortfolio']["Cancel"]; ?></a>
					<?php if ($data["currAction"]=='edit') { ?><a href="#" class="btn btnDel btn-danger" rel="<?php echo $data["section"]; ?>" ><?php echo $data["lang"]['TeacherPortfolio']["Delete"]; ?></a><?php } ?>
				</div>
			</div>
			</form>
		</div>
		<?php echo $data["defaultFooter"]; ?>
	</div>
	<?php } ?>
</div>