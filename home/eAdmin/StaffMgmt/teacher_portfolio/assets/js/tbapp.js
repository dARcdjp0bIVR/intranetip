var dateJS = {
	vars: {
		clcolor: "#30da2d"
	},
	listeners: {},
	func: {
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dateJS.func.init", dateJS );
			var date_input = $('.inputdate'); //our date input has the name "date"
			var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
			if (typeof date_input != "undefined" && date_input.length > 0) {
				date_input.datepicker({
					format: 'yyyy-mm-dd',
					// container: container,
					todayHighlight: true,
					autoclose: true
				});
			}
		}
	}
}
/*********************************************************************/
var navJS = {
	vars: {
		defaultAnimate: "fadeIn",
		clcolor: "#f0de9e",
		xhr: "",
		getURL: "?task=",
		section: "",
		clearMsg: true
	},
	listeners: {
		navClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners.navClickHandler", navJS );
			e.preventDefault();
			var strHref = $(this).attr('href');
			var strRel = $(this).attr('rel');
			if (typeof strRel == "undefined") {
				strRel = "list";
			}
			strHref = strHref.replace(/\#/g, '');
			var Title = $(this).text();
			window.location.hash = strHref;
			
			$('html, body').css({scrollTop:0});
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners.navClickHandler >> set Hash", navJS );
			
			var data = { k: tp_key, tp: "ajax" };
			try {
				if (navJS.vars.section != "" && $('#' + navJS.vars.section).length > 0) {
					$('#' + navJS.vars.section).empty();
				}
			} catch(err) {
				console.log(err);
			}
			navJS.vars.section = strHref;
			var url = navJS.vars.getURL + strHref + "." + strRel + "";
			// $('#' + strHref).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> "' + Title + '" ' + langtxt["loading"] + "</div>");
			$('#' + strHref).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + "</div>");
			navJS.func.getContent(url, data, $('#' + strHref));
		},
		ajaxHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners.ajaxHandler", navJS );
			var strRel = $(this).attr('rel');
			if (typeof strRel != "undefined") {
				var hrefArr = strRel.split('/');
				var url = navJS.vars.getURL + hrefArr.join(".");
				var data = { k: tp_key, tp: "ajax" };
				var Title = $('.tp-menu li.active > a').eq(0).text();
				// $("#" + hrefArr[0]).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> "' + Title + '" ' + langtxt["loading"] + "</div>");
				$("#" + hrefArr[0]).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + "</div>");
				navJS.func.getContent(url, data, $("#" + hrefArr[0]));
			}
			e.preventDefault();
		},
		ajaxFDHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners.ajaxFDHandler", navJS );
			notifyMsgJS.func.showMsg('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"], 'warning');
			var rel = $(this).attr('rel');
			try {
				var url = navJS.vars.getURL + rel;
				$.fileDownload(url, {
					successCallback: function (url) {
						// notifyMsgJS.func.showMsg('Download Successful', 'info');
					},
					failCallback: function (html, url) {
						notifyMsgJS.func.showMsg(langtxt["downloadFailed"], 'danger');
					}
				});
			} catch(err) {
				notifyMsgJS.func.showMsg(langtxt["downloadFailed"], 'danger');
			}
			e.preventDefault();
		},
		popupClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners.popupClickHandler", navJS );
			var strHref = $(this).attr('rel');
			if (typeof strHref != "undefined") {
				newWindow(strHref, 9);
			}
			e.preventDefault();
		},
		modalClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners.modalClickHandler", navJS );
			var strHref = $(this).attr('rel');
			var strTitle = $(this).attr('title');
			$('#myModalLabel').html(langtxt["mod_title"] + " / " + strTitle);
			// $('#tpModal .modal-body').css({"padding": "0px"}).eq(0).html("<iframe id='tbiFrame' src='" + strHref + "' width='100%' style='border:0px; height:500px;'></iframe>")
			// $('#tpModal .modal-content').eq(0).append("<div class='modal-footer'><a href='#'>Add</a></div>");
			var targetHeight = Math.floor($("body").height() * 0.7);
			iframe = "<iframe id='tbiFrame' src='" + strHref + "' width='100%' style='border:0px; height:" + targetHeight + "px'></iframe>";
			$('#tpModal .modal-body').eq(0).empty().append(iframe);
			$('#tpModal').modal('show');
			$('#tpModal').on('hidden.bs.modal', function () {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " Close Modal", navJS );
				var table = dtableJS.vars.tableObj;
				if (typeof table != "undefined") {
					table.draw();
				}
			})
			e.preventDefault();
		},
		_successFunc: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners._successFunc", navJS );
			return false;
		},
		_failFunc: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.listeners._failFunc", navJS );
			return false;
		}
	},
	func: {
		getJSON: function(purl, pdata, successFunc, failFunc) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.getJSON >> AJAX get JSON", navJS );
			var xhr = appJS.vars.xhr;
			if (xhr != "") {
				xhr.abort();
				appJS.vars.xhr = "";
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.getJSON >> AJAX Abort", navJS );
			}
			if (typeof successFunc == "undefined") {
				successFunc = navJS.listeners._successFunc;
			}
			if (typeof failFunc == "undefined") {
				failFunc = navJS.listeners._failFunc;
			}
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.getJSON >> AJAX get JSON >> " + purl, navJS );
			appJS.vars.xhr = $.ajax({
				url: purl,
				data: pdata,
				cache: false,
				dataType: 'json',
				timeout: appJS.vars.timeout,
				error: failFunc,
				success: successFunc,
				type: 'POST'
			});
		},
		getContent: function(purl, pdata, resultObj) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.getContent >> AJAX get Content", navJS );
			// $.notifyClose();
			var xhr = appJS.vars.xhr;
			if (xhr != "") {
				xhr.abort();
				appJS.vars.xhr = "";
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.getContent >> AJAX Abort", navJS );
			}
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.getJSON >> AJAX get JSON >> " + purl, navJS );
			appJS.vars.xhr = $.ajax({
				url: purl,
				data: pdata,
				cache: false,
				timeout: appJS.vars.timeout,
				error: function(request, status, err) {
					var msg = langtxt["err_pagenotfound"];
					if (status == "timeout") {
						msg = langtxt["err_timeout"];
					} else {
					}
					resultObj.html("");
					notifyMsgJS.func.showMsg(msg, 'danger');
				},
				dataType: 'html',
				success: function(data) {
					// $.notifyClose();
					resultObj.animateCss(navJS.vars.defaultAnimate);
					resultObj.html(data);
					$('a.printAction').off('click', appJS.listeners.printHandler);
					$('a.printAction').on('click', appJS.listeners.printHandler);
					
					$('a.printAllAction').off('click', appJS.listeners.printAllHandler);
					$('a.printAllAction').on('click', appJS.listeners.printAllHandler);
					
					$('a.btnAjax').off('click', navJS.listeners.ajaxHandler);
					$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
					
					$('a.btnPopup').off('click', navJS.listeners.popupClickHandler);
					$('a.btnPopup').on('click', navJS.listeners.popupClickHandler);
					
					$('a.btnModal').off('click', navJS.listeners.modalClickHandler);
					$('a.btnModal').on('click', navJS.listeners.modalClickHandler);
					
					$('a.btnFDAjax').off('click', navJS.listeners.ajaxFDHandler);
					$('a.btnFDAjax').on('click', navJS.listeners.ajaxFDHandler);
					
					var timer = debugMsgJS.vars.timer;
					if (timer != "") {
						clearTimeout(timer);
						debugMsgJS.vars.timer = "";
					}
					debugMsgJS.vars.timer = setTimeout("appJS.func.hideDebug()", 3000);
					appJS.func.linkInit();
					dateJS.func.init();
					dtableJS.func.init();
					filesJS.vars.uploader = "";
					stepPanelJS.func.init();
					formJS.func.init();
					academicYearJS.func.linkInit();
					jqSortJS.func.init();
					manageUserJS.func.init();
					$('html, body').css({scrollTop:0});
					$(".wrapper").height("100%");
					/*if ($("ol." + jqSortJS.vars.container).length > 0) {
						requirejs(['jquery-sortable'], function(foo) {
						});
					}*/
				},
				type: 'POST'
			});
		},
		basename: function (path) {
			var bname = path.split('/').reverse()[0];
			bname = bname.split("#")[0];
			return bname; 
		},
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " navJS.func.init", navJS );
			$('.tp-menu a').off("click", navJS.listeners.navClickHandler);
			$('.tp-menu a').on("click", navJS.listeners.navClickHandler);

			var hash = window.location.hash;
			navJS.vars.hashArr = hash.split("/");
			if (typeof navJS.vars.hashArr[0] == "undefined" || hash == "") {
				hash = $('.tp-menu a').eq(0).attr('href');
			}
			if ($('.tp-menu a[href="' + hash + '"]').length > 0) {
				$('.tp-menu a[href="' + hash + '"]').trigger('click');
			} else {
				if (navJS.vars.section == "") {
					var section = $('.tp-menu a').eq(0).attr('href');
					if (typeof section != "undefined") {
						section = section.replace(/\#/g, '');
						navJS.vars.section = section;
					}
				}
				$('#norecord').addClass('active').html('<div style="padding:25px;"><i class="fa fa-warning"></i> ' + langtxt["pageNotFound"] + "</div>");
			}
			if ($(".thrList select option") > 0 || $(".thrList select option:selected").val() == "0") {
				$('.tab-content').hide();
				$('#selectUser').show();
			} else {
				$("body").height(parseInt($(document).height()));
				$('.tab-content').show();
				$('#selectUser').hide();
			}
			navJS.vars.getURL = navJS.func.basename(window.location.href) + "?task=";
			$('html, body').delay(1000).animate({scrollTop:0});
			// requirejs.config({ baseUrl: assetPath + '/js' });
		}
	}
};
/*********************************************************************/
var academicYearJS = {
	vars: {
		clcolor: "AQUA",
		selectedYear: "",
		selectedYearSection: ""
	},
	listeners: {
		clickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " academicYearJS.listeners.clickHandler", academicYearJS );
			if (!appJS.vars.disableAllAction) {
				var hashLink = $(this).attr('rel');
				var parentObj = $(this).parent().parent();
				parentObj.find('a').removeClass('active');
				$(this).addClass('active');
				var parentButton = $(".tab-pane.active button.selectYearElt").eq(0);
				var selectedYear = $(this).text();
				parentButton.html(selectedYear + '<span class="caret"></span>');
				$(".tab-pane.active .actions").eq(0).hide().removeClass('hidden').fadeIn(800);
				$(".tab-pane.active .content").eq(0).hide().removeClass('hidden').fadeIn(800);
				var hashParam = hashLink.split("/");
				var data = { k: tp_key, tp: "ajax", selectedYear: hashParam[1] };
				academicYearJS.vars.selectedYearSection = hashParam[0];
				academicYearJS.vars.selectedYear = hashParam[1];
				var strHref = hashParam[0];
				var Title = $("a[href='#" + strHref + "']").text();
				var url = navJS.vars.getURL + strHref + ".list";
				$('#' + strHref).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + "</div>");
				navJS.func.getContent(url, data, $('#' + strHref));
				e.preventDefault();
			}
		}
	},
	func: {
		yearFromJson: function(selectedYear) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " academicYearJS.func.yearFromJson", academicYearJS );
			selectedYear = selectedYear.replace(/\s/g,"-").replace(/\(/g,"\_").replace(/\)/g,"\_");
			var objname = ".AcademicYear.year_" + selectedYear;
			var strHtml = '<span class="caret"></span>';
			if ($(objname).length > 0) {
				$("button.selectYearElt").eq(0).html($(objname).eq(0).text() + strHtml);
			}
		},
		linkInit: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " academicYearJS.func.linkInit", academicYearJS );
			$("a.AcademicYear").off('click', academicYearJS.listeners.clickHandler);
			if ($("a.AcademicYear").length) {
				$("a.AcademicYear").on('click', academicYearJS.listeners.clickHandler);
			}
		}
	}
};
/*********************************************************************/
var notifyMsgJS = {
	vars: {
		clcolor: "TEAL",
	},
	listeners: {},
	func: {
		showPrompt: function(msg, confirmOK, remarks) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " notifyMsgJS.func.showPrompt", notifyMsgJS );
			var strRemarks = "";
			if (typeof remarks != "undefined" && remarks != "") {
				strRemarks = "<br><br><blockquote>";
				strRemarks += "<footer><cite>" + remarks + "</cite></footer>";
				strRemarks += "</blockquote>";
			}
			// $.notifyClose();
			bootbox.confirm({
				"message": msg + strRemarks,
				buttons: {
					'confirm': {
						label: langtxt["action_delete"],
						className: 'btn-danger btn-sm pull-right'
					},
					'cancel': {
						label: langtxt["action_cancel"],
						className: 'btn-default btn-sm pull-right'
					}
				},
				callback: confirmOK
			});
		},
		showMsg: function(strMsg, msgtype) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " notifyMsgJS.func.showMsg", notifyMsgJS );
			// $.notifyClose();
			if (typeof msgtype == "undefined") msgtype = 'info';
			if (msgtype == "success") {
				$.notifyClose();
			}
			if (typeof strMsg != "undefined" && typeof msgtype != "undefined") {
				var ntimer = 2000;
				switch (msgtype) {
					case "info":
						// ntimer = 0;
						break;
					case "warning":
						ntimer = 100;
						break;
				}
				if (formJS.vars.notify != "") {
					$.notifyClose();
				}
				formJS.vars.notify = strMsg;
				$.notify({
					message: strMsg
				},{
					hideDuration: 0,
					allow_dismiss: true,
					newest_on_top: true,
					placement: {
						from: "top",
						align: "right"
					},
					offset: {
						x: 20,
						y: 120
					},
					timer: ntimer,
					// settings
					type: msgtype,
					onClosed: function() {
						formJS.vars.notify = "";
					}
				});
			}
		}
	}
};
/*********************************************************************/
var formJS = {
	vars: {
		clcolor: "YELLOW",
		allowDirectCheck: true,
		container: "#editform",
		notify: "",
		clickObj: {}
	},
	listeners: {
		submitClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.submitClickHandler", formJS );
			$(formJS.vars.container).off('submit', formJS.listeners.submitClickHandler);
			$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.submitClickHandler);
			$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.deleteClickHandler);
			$('a.btnAjax').off('click', navJS.listeners.ajaxHandler);

			$(formJS.vars.container).on('submit', appJS.listeners.disableHandler);
			$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.disableHandler);
			$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.disableHandler);
			$('a.btnAjax').on('click', appJS.listeners.disableHandler);
			
			if (!appJS.vars.disableAllAction) {
				var value = "";
				var name = "";
				var type = "";
				var dateRangeError = false;
				$(".error").removeClass('error');
				if ($('#EndDate').length > 0) {
					if (formJS.func.checkEndDate()) {
						$('#EndDate').removeClass('error');
					} else {
						$('#EndDate').addClass('error');
						dateRangeError = true;
					}
				}

				$('input[required]').each(function(i, requiredField) {
					name = $.trim($(this).attr('name'));
					value = $.trim($(this).val());
					type = $.trim($(this).attr('type'));
					switch (type) {
						case "checkbox":
							if ($('input[required][name="' + name +'"]:checked').length == 0) {
								$('input[required][name="' + name +'"]').each(function() {
									$(this)
										.closest("label")
										.addClass('error');
								});
							} else {
								$('input[required][name="' + name +'"]').each(function() {
									$(this)
										.closest("label")
										.removeClass('error');
								});
							}
							break;
						default:
							if (value == "") {
								$(this).addClass('error');
							}
							break;
					}
				});
				$('textarea[required]').each(function(i, requiredField) {
					value = $.trim($(this).val());
					if (value == "") {
						$(this).addClass('error');
					} else {
						$(this).removeClass('error');
					}
				});
				$('select[required]').each(function(i, requiredField) {
					var value = $(this).val();
					if (typeof value == "undefined" || value == "") {
						$(this).addClass('error');
					} else {
						$(this).removeClass('error');
					}
				});
				
				if ($('.uploadFileArea').hasClass('required')) {
					if ($('input[name="files[]"]').length > 0) {
						$('#drop-target').removeClass('error');
					} else {
						$('#drop-target').addClass('error');
					}
				}

				if ($('.error').length == 0) {
					if (!filesJS.vars.uploading) {
						var formData = "k=" + tp_key + "&tp=ajax&" + $(formJS.vars.container).serialize();
						notifyMsgJS.func.showMsg(langtxt["processing"], 'info');
						formJS.func.formSubmit(formData);
					} else {
						$(formJS.vars.container).off('submit', appJS.listeners.disableHandler);
						$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.disableHandler);
						$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.disableHandler);
						$('a.btnAjax').off('click', appJS.listeners.disableHandler);
						
						$(formJS.vars.container).find('.btn-upload').show();
						$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
						$(formJS.vars.container).on('submit', formJS.listeners.submitClickHandler);
						$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.submitClickHandler);
						$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.deleteClickHandler);

						notifyMsgJS.func.showMsg(langtxt["fileupload_failedUploadingProcessing"], 'danger');
					}
				} else {
					$(formJS.vars.container).off('submit', appJS.listeners.disableHandler);
					$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.disableHandler);
					$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.disableHandler);
					$('a.btnAjax').off('click', appJS.listeners.disableHandler);
					
					$(formJS.vars.container).find('.btn-upload').show();
					$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
					$(formJS.vars.container).on('submit', formJS.listeners.submitClickHandler);
					$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.submitClickHandler);
					$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.deleteClickHandler);

					var fieldMsg = ''; 
					if (dateRangeError)
					{
						fieldMsg = langtxt["dateRangeInvalid"];
					} else {
						fieldMsg = langtxt["allRequiredFields"];
					}
					notifyMsgJS.func.showMsg(fieldMsg, 'danger');
				}
			}
			e.preventDefault();
		},
		delSuccessHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.delSuccessHandler", formJS );
			if (typeof e.result != "undefined" && e.result == "success") {
				notifyMsgJS.func.showMsg(langtxt["dataTable_delete_success"], 'success');
				$("#" + navJS.vars.section + " .content").empty();
			} else {
				notifyMsgJS.func.showMsg(langtxt["dataTable_delete_fail"], 'danger');
			}
		},
		delFailHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.delFailHandler", formJS );
			notifyMsgJS.func.showMsg(langtxt["dataTable_delete_fail"], 'danger');
		},
		deleteClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.deleteClickHandler", formJS );
			var clickObj = $(this);
			formJS.vars.clickObj = clickObj;
			var msg = langtxt["ConfirmMsg_delete"].replace("__TITLE__", "");
			notifyMsgJS.func.showPrompt(msg, formJS.func.removeConfirmHandler);
			e.preventDefault();
		},
		keyCheckHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.keyCheckHandler", formJS );
			if (!appJS.vars.disableAllAction) {
				var value = $.trim($(this).val());
				switch ($(this).attr('type')) {
					case "textarea": value = $.trim($(this).text());
					default:
						$(this).removeClass('error');
						if (!$(this).hasClass('date')) {
							if ($(this).prop('required')) {
								if (value != "") {
									$(this).removeClass('error');
								} else {
									$(this).addClass('error');
								}
							}
						}
						break;
				}
			}
		},
		keySelectCheckHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.keySelectCheckHandler", formJS );
			if (!appJS.vars.disableAllAction) {
				var value = $(this).val();
				if (typeof value == "undefined" || value == "") {
					$(this).addClass('error');
				} else {
					$(this).removeClass('error');
				}
			}
		},
		keyCheckBoxHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.keyCheckBoxHandler", formJS );
			if (!appJS.vars.disableAllAction) {
				var name = $(this).attr('name');
				if ($('input[required][name="' + name +'"]:checked').length == 0) {
					$('input[required][name="' + name +'"]').each(function() {
						$(this)
							.closest("label")
							.addClass('error');
					});
				} else {
					$('input[required][name="' + name +'"]').each(function() {
						$(this)
							.closest("label")
							.removeClass('error');
					});
				}
			}
		},
		formSuccess: function(responseObj) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.formSuccess", formJS );
			if (typeof responseObj.result == "undefined" || responseObj.result != "success") {
				var msg = "Error";
				if (typeof responseObj.data != "undefined") {
					msg = responseObj.data;
				}
				notifyMsgJS.func.showMsg(msg, 'danger');

				$(formJS.vars.container).off('submit', appJS.listeners.disableHandler);
				$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.disableHandler);
				$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.disableHandler);
				$('a.btnAjax').off('click', appJS.listeners.ajaxHandler);
				
				if ($(formJS.vars.container).find('.btn-upload').length > 0) {
					$(formJS.vars.container).find('.btn-upload').show();
					notifyMsgJS.func.showMsg(langtxt["fileupload_failed"], 'danger');
				}
				$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
				$(formJS.vars.container).on('submit', formJS.listeners.submitClickHandler);
				$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.submitClickHandler);
				$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.deleteClickHandler);
			} else {
				var msg = "Success";
				if (typeof responseObj.data.msg != "undefined") {
					msg = responseObj.data.msg;
				}
				notifyMsgJS.func.showMsg(msg, 'success');
				
				$(formJS.vars.container).off('submit', appJS.listeners.disableHandler);
				$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.disableHandler);
				$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.disableHandler);
				if (typeof responseObj.data.noaction != "undefined" && responseObj.data.noaction == "Y") {
					$(formJS.vars.container).on('submit', formJS.listeners.submitClickHandler);
					$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.submitClickHandler);
					$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.deleteClickHandler);
					$('a.btnAjax').off('click', appJS.listeners.ajaxHandler);
					$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
				} else {
					$(formJS.vars.container).find('.btnSubmit').hide();
					$('#drop-target').hide();
					$('#pickfiles').hide();
					$('.uploadFileArea .fa.fa-trash').hide();
					$(formJS.vars.container).find('.btnDel').hide();
					$('a.btnAjax').off('click', appJS.listeners.ajaxHandler);
					$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
					// $(formJS.vars.container).find('a.btnAjax.btnBack').html(langtxt["action_back"]);
					$(formJS.vars.container).find('a.btnAjax.btnBack').trigger('click');
				}
			}
		},
		formFail: function(data, status, err) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.listeners.formFail", formJS );
			
			$(formJS.vars.container).off('submit', appJS.listeners.disableHandler);
			$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.disableHandler);
			$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.disableHandler);
			$('a.btnAjax').off('click', appJS.listeners.ajaxHandler);

			$(formJS.vars.container).find('.btn-upload').show();
			$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
			$(formJS.vars.container).on('submit', formJS.listeners.submitClickHandler);
			$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.submitClickHandler);
			$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.deleteClickHandler);
			var msg = langtxt["err_unknown"];
			if (status == "timeout") {
				msg = langtxt["err_timeout"];
			} else if (typeof data.responseJSON != "undefined") {
				if (typeof data.responseJSON.msg != "undefined") {
					msg = data.responseJSON.msg;
				}
			}
			notifyMsgJS.func.showMsg(msg, 'danger');
		},
		fileDelClickHandler: function(e) {
			$(this).parent().remove();
			e.preventDefault();
		}
	},
	func: {
		formSubmit: function(formData) {
			if (!filesJS.vars.uploading) {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.func.formSubmit", formJS );
				var strHref = $(formJS.vars.container).attr('rel');
				strHref = strHref.replace("#", "");
				var hrefArr = strHref.split('/');
				var urlLink = navJS.vars.getURL + hrefArr.join(".");
				navJS.func.getJSON(urlLink, formData, formJS.listeners.formSuccess, formJS.listeners.formFail);
			} else {
				
			}
		},
		removeConfirmHandler: function(result) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.func.removeConfirmHandler", formJS );
			if (result) {
				clickObj = formJS.vars.clickObj;
				var strRel = clickObj.attr('rel');
				var id = $("input[name='recid']").val();
				var data = { recid: id, k: tp_key, tp: "ajax" };
				
				if (typeof strRel != "undefined") {
					var url = navJS.vars.getURL + strRel + ".deleterecord";
					navJS.func.getJSON(url, data, formJS.listeners.delSuccessHandler, formJS.listeners.delFailHandler);
				}
			}
		},
		checkEndDate: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.func.checkEndDate", formJS );
			var startDate = $("#StartDate").val();
			var endDate = $("#EndDate").val();
			if (startDate != "" && endDate) {
				if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
					return false;
				}
			}
			return true;
		},
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " formJS.func.init", formJS);
			if ($(formJS.vars.container).length > 0) {
				$('input[required], select[required], textarea[required]')
					.closest(".form-group")
					.children("label")
					.append(" <i class='fa fa-asterisk fa-fw'></i>")
					
				$('#filelist.required')
					.closest(".form-group")
					.children("label")
					.append(" <i class='fa fa-asterisk fa-fw'></i>")
					
				if (formJS.vars.allowDirectCheck) {
					$('input[type!=checkbox][required], textarea[required]').on('mouseup keyup', formJS.listeners.keyCheckHandler);
					$('input[type=checkbox][required]').on('click', formJS.listeners.keyCheckBoxHandler);
					$('select[required]').on('change', formJS.listeners.keySelectCheckHandler);
				}
				$(formJS.vars.container).off('submit', formJS.listeners.submitClickHandler);
				$(formJS.vars.container).on('submit', formJS.listeners.submitClickHandler);
				
				$(formJS.vars.container).find('.btnSubmit').off('click', formJS.listeners.submitClickHandler);
				$(formJS.vars.container).find('.btnSubmit').on('click', formJS.listeners.submitClickHandler);
				
				$(formJS.vars.container).find('.btnDel').off('click', formJS.listeners.deleteClickHandler);
				$(formJS.vars.container).find('.btnDel').on('click', formJS.listeners.deleteClickHandler);


				$(formJS.vars.container).find('.btnDelfile').off('click', formJS.listeners.fileDelClickHandler);
				$(formJS.vars.container).find('.btnDelfile').on('click', formJS.listeners.fileDelClickHandler);

				if ($("#"+ filesJS.vars.container).length > 0) {
					/********* Replace Listeners for submit form ********/
					// filesJS.listeners.customUploadComplete = formJS.listeners.customUploadComplete;
					filesJS.func.init();
				}
			}
		}
	}
};
/*********************************************************************/
var debugMsgJS = {
	vars: {
		allowConsole: false,
		clcolor: "FUCHSIA",
		container: "#devobj"
	},
	listeners: {},
	func: {
		showlog: function(msg, customObj) {
			if (window.console && window.console.log) {
				var txtColor = "SILVER";
				var txtBGColor = "BLACK";
				if (typeof customObj != "undefined") {
					if (typeof customObj.vars.clcolor != "undefined") {
						txtColor = customObj.vars.clcolor;
					}
				}
				var log_msg = "%c[-]%c";
				var log_color1 = "background:" + txtBGColor + ";color:WHITE;";
				var log_color2 = "background:" + txtBGColor + ";color:" + txtColor + ";";
				var log_type = "log";
				if (msg.indexOf(' AJAX ') > -1) {
					log_msg = "%c[A]%c";
					log_color1 = "background:#FFCE54;color:BLACK;";
				} else if (msg.indexOf('.listeners.') > -1) {
					msg = msg.replace(".listeners.", " >> Listener >> ");
					log_msg = "%c[L]%c";
					log_color1 = "background:WHITE;color:BLACK;";
					log_type = "info";
				} else if (msg.indexOf('.func.') > -1) {
					msg = msg.replace(".func.", " :: Function :: ");
					log_msg = "%c[F]%c";
					log_type = "log";
				}
				log_msg = log_msg + msg + " ";
				
				log_color2 = "background:" + txtBGColor + ";color:" + txtColor + ";";
				switch (log_type) {
					case "warn":
						console.warn(log_msg, log_color1, log_color2);
						break;
					case "info":
						console.info(log_msg, log_color1, log_color2);
						break;
					default:
						console.log(log_msg, log_color1, log_color2);
						break;
				}
			}
			return false;
		},
		showMsg: function(debugInfo) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " debugMsgJS.func.showMsg", debugMsgJS );
			if ($(debugMsgJS.vars.container).length > 0) {
				var second = (typeof debugInfo.second != "undefined") ? debugInfo.second : 0;
				var memory = (typeof debugInfo.memory != "undefined") ? debugInfo.memory : 0;
				var numOfQuery = (typeof debugInfo.numOfQuery != "undefined") ? debugInfo.numOfQuery : 0;
				var msgTemplate = '<div style="color:#fff; padding:5px; font-family:Arial; font-size:12px;">Page generated in <span style="color:#f00">' + second +
							'</span> seconds.Total memory used in this page: <span style="color:yellow">' + memory + '</span>. QUERIES : <span style="color:red">' + numOfQuery + '</span></div>';
				$(debugMsgJS.vars.container).hide().html(msgTemplate).fadeIn(400);

				var timer = debugMsgJS.vars.timer;
				if (timer != "") {
					clearTimeout(timer);
					debugMsgJS.vars.timer = "";
				}
				debugMsgJS.vars.timer = setTimeout("appJS.func.hideDebug()", 5000);
			}
		}
	}
}
/*********************************************************************/
var filesJS = {
	vars: {
		clcolor: "LIME",
		uploader: "",
		container: "file_container",
		existfilelist: "existfilelist",
		file_lists: "filelist",
		errors: false,
		uploading: false,
		xhr: "",
		maxFile: 0,
		fileObjs: []
	},
	listeners: {
		customUploadComplete: function (up, files) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.listeners.customUploadComplete [ Uploader > init ]", filesJS );
			filesJS.vars.uploading = false;
			$('#' + filesJS.vars.file_lists).find('small.percenttxt').remove();
			$('#' + filesJS.vars.file_lists).find('small.sizetxt').remove();
			$('#' + filesJS.vars.file_lists).find('.progress').remove();
			$('#' + filesJS.vars.file_lists).find('.upres').remove();
			$('#' + filesJS.vars.file_lists).find(".fa.fa-trash").show();
		},
		delFileHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.listeners.delFileHandler", filesJS );
			
			if (typeof $(this).parent().attr('id') != "undefined") {
				var fileid = $(this).parent().attr('id');
				var parentObj = $(this).parent().parent();
				parentObj.remove();
				var uploader = filesJS.vars.uploader;
				uploader.removeFile(fileid);
			}
			if ($("#extFiles li").length < 1) {
				$('#existfilelist').empty();
			}
			$('#drop-target').show();
			$('#pickfiles').show();
			e.preventDefault();
		}
	},
	func: {
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init", filesJS );
			filesJS.vars.uploader = "";
			if ($("#"+ filesJS.vars.container).length > 0) {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init -- RUN", filesJS );
				var fid = 0;
				if ($('input[name="recid"]').length > 0) {
					fid = $('input[name="recid"]').eq(0).val();
				}
				
				var maxfile = $("#"+ filesJS.vars.container).attr('rel');
				if (Math.floor(maxfile) == maxfile && $.isNumeric(maxfile)) {
					filesJS.vars.maxFile = Math.floor(maxfile);
				}
				var _dragdrop = true;
				var _drop_element = "drop-target";
				filesJS.vars.uploader = new plupload.Uploader({
								runtimes : 'html5,flash,silverlight,html4',
								browse_button : 'pickfiles', // you can pass an id...
								container: document.getElementById(filesJS.vars.container), // ... or DOM Element itself
								url : navJS.vars.getURL + navJS.vars.section + ".uploadfile",
								flash_swf_url : assetPath + '/js/plupload/js/Moxie.swf',
								silverlight_xap_url : assetPath + '/js/plupload/js/Moxie.xap',
								// chunk_size: "1024kb";
								/* chunk_size: "500kb", */
								dragdrop: _dragdrop,
								drop_element : 'drop-target',
								unique_names : false,
								filters : {
									max_file_size : msze,
									// Specify what files to browse for
									/*
									mime_types: [
										{title : "Image files", extensions : "jpg,gif,png"},
										{title : "Doc files", extensions : "doc,docx"},
										{title : "PDF files", extensions : "pdf"}
									] */
								},
								multipart_params : { recid: fid },
								init: {
									Init: function(up, params) {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > Init ", filesJS );
										if (uploader.features.dragdrop) {
											var target = $("#drop-target");
											target.ondragover = function(event) {
												event.dataTransfer.dropEffect = "copy";
											};

											target.ondragenter = function() {
												this.className = "dragover";
											};
											
											target.ondragleave = function() {
												this.className = "";
											};
											
											target.ondrop = function() {
												this.className = "";
											};
										}
										if ($("#extFiles li").length > 0) {
											$('#drop-target').hide();
											$('#pickfiles').hide();
											$("#extFiles i.fa-trash").on('click', filesJS.listeners.delFileHandler);
										}
									},
									PostInit: function() {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > PostInit ", filesJS );
										document.getElementById('filelist').innerHTML = '';
										if ($('#uploadfiles').length > 0) {
											document.getElementById('uploadfiles').onclick = function() {
												var uploader = filesJS.vars.uploader;
												uploader.start();
												return false;
											};
										}
									},
									FilesAdded: function(up, files) {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > FilesAdded ", filesJS );
										var fileHTML = '<ul class="uploaded">';
										var fileListHTML = "";
										var errFileHTML = "";
										var uploader = filesJS.vars.uploader;
										if (filesJS.vars.maxFile > 0) {
											if (up.files.length > filesJS.vars.maxFile || uploader.files.length > filesJS.vars.maxFile) {
												uploader.splice();
												notifyMsgJS.func.showMsg('Only ' + filesJS.vars.maxFile + ' file(s) per upload !', "danger");
												return false;
											} else if (up.files.length == filesJS.vars.maxFile) {
												$('#' + _drop_element).slideUp(1000);
												$('#pickfiles').fadeOut(1000);
											}
										}
										plupload.each(files, function(file) {
											if (typeof file != "undefined") {
												var filename = file.name;
												if (typeof filename != "undefined") {
													var extension = filename.substr( (filename.lastIndexOf('.') +1) );
													switch (extension.toLowerCase()) {
														case "exe":
														case "inf":
															errFileHTML += "<b>" + filename + "</b> " + langtxt["fileupload_file_extension_err"] + "<br>";
															uploader.removeFile(file.id);
															break;
														default:
															fileListHTML += '<li><div id="' + file.id + '"> ' + file.name + ' <i class="fa fa-trash"></i> <small class="sizetxt"> (' + plupload.formatSize(file.size) + ')</small> <span></span></div></li>';
															break;
													}
												}
											}
										});
										if (errFileHTML != "") {
											notifyMsgJS.func.showMsg( errFileHTML, "danger" );
										}
										if (fileListHTML != "") {
											fileHTML += fileListHTML + '</ul>';
											$("#" + filesJS.vars.file_lists).append(fileHTML);
											$("#" + filesJS.vars.file_lists).find('i.fa-trash').css({"cursor":"pointer"});
											$("#" + filesJS.vars.file_lists + " i.fa-trash").on('click', filesJS.listeners.delFileHandler);
										}
										/*****************************************************************/
										/* auto upload */
										/*****************************************************************/
										filesJS.vars.uploading = true;
										uploader.start();
									},
									ChunkUploaded: function(up, file, info) {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > ChunkUploaded ", filesJS );
										// Called when a file chunk has finished uploading
										// console.log('[ChunkUploaded] File:', file, "Info:", info);
									},
									FileUploaded: function(up, file, info) {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > FileUploaded ", filesJS );
										if (typeof info.response != "undefined") {
											var fileid = file.id;
											var responseObj = $.parseJSON(info.response);
											if (typeof responseObj.result != "undefined") {
												if (responseObj.id != "undefined") {
													$('#' + fileid).append("<small class='text-info upres'><i class='fa fa-check-square'></i> " + langtxt["fileupload_Success"] + "</small><input type='hidden' name='files[]' value='" + responseObj.id + "'>");
												}
												filesJS.vars.uploading = false;
											} else {
												filesJS.vars.errors = true;
												$('#' + fileid).find('.progress').eq(0).remove();
												$('#' + fileid).find('small.percenttxt').eq(0).remove();
												if (typeof responseObj.error.message != "undefined") {
													$('#' + fileid).append("<div class='text-danger upres'><small><i class='fa fa-warning'></i> " + responseObj.error.message + "</small></div>");
												} else {
													$('#' + fileid).append("<div class='text-danger upres'><small><i class='fa fa-warning'></i> " + langtxt["fileupload_file_failed"] + "</small></div>");
												}
											}
										} else {
											$('#' + fileid).find('.progress').eq(0).remove();
											filesJS.vars.errors = true;
											$('#' + fileid).append("<div class='text-danger upres'><small><i class='fa fa-warning'></i> " + langtxt["fileupload_file_failed"] + "</small></div>");
										}
									},
									UploadProgress: function(up, file) {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > UploadProgress ", filesJS );
										$("#" + filesJS.vars.file_lists).find(".fa.fa-trash").hide();
										var colorName = "danger active";
										if (file.percent > 97) {
											colorName = "info";
										} else if (file.percent > 60) {
											colorName = "success active";
										} else if (file.percent > 40) {
											colorName = "warning active";
										}
										var processHTML = '<small class="percenttxt">' + file.percent + '%</small><div class="progress" style="width:250px;">' + 
															'<div class="progress-bar progress-bar-' + colorName + ' progress-bar-striped" role="progressbar" aria-valuenow="' + file.percent + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + file.percent + '%">' + 
															'<span class="sr-only">' + file.percent + '% Complete (success)</span>' + 
															'</div>' +
															'</div>';
										
										if ($('#' + file.id).length > 0) {
											document.getElementById(file.id).getElementsByTagName('span')[0].innerHTML = processHTML;
										}
									},
									/********************************************/
									UploadComplete: filesJS.listeners.customUploadComplete,
									/********************************************/
									Error: function(up, args) {
										if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " filesJS.func.init >> uploader > init > Error ", filesJS );
										var msg = "";
										switch (args.message) {
											case "File size error.":
												msg = langtxt["fileupload_outOfSize"]
												break;
											default:
												msg = langtxt["fileupload_select_failed"]
												break;
										}
										notifyMsgJS.func.showMsg("<b>" + args.file.name + "</b> " + msg, "danger");
										// console.log(document.createTextNode("Error #" + err.code + ": " + err.message + "\n"));
										// document.getElementById('console').appendChild(document.createTextNode("\nError #" + err.code + ": " + err.message));
									}
								}
				});
				var uploader = filesJS.vars.uploader;
				uploader.init();
			}
		}
	}
};
/*********************************************************************/
var dtableJS = {
	vars: {
		clcolor: "YELLOW",
		container: ".bdTable",
		header_cols: "th.tbheader_col",
		tableObj: "",
		showAll: false,
		row_data: {},
		coll_data: "",
		childLinkObj: "",
		childTableID: ""
	},
	listeners: {
		handleAjaxError: function(xhr, error) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.handleAjaxError", dtableJS );
			var msg = langtxt["error_unknown"];
			if (error.status == "timeout") {
				msg = langtxt["error_timeout"];
			}
			notifyMsgJS.func.showMsg(msg, 'danger');
		},
		expandHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.expandHandler", dtableJS );
			var table = dtableJS.vars.tableObj;
//			var tr = $(this).parent().parent().parent();
			var tr = $(this);
			var row = table.row(tr);
			if (row.child.isShown()) {
				if (!dtableJS.vars.showAll) {
					var rel = $(this).attr('rel');
					if (typeof rel != "undefined" ) {
						row.child.remove();
						dtableJS.vars.childTableID = "";
					} else {
						row.child.hide();
					}
					tr.removeClass('shown');
					// tr.animateCss('shake');
				}
			} else {
				var rel = $(this).attr('rel');
				if (typeof rel != "undefined" ) {
					var rowData = row.data();
					var childID = rowData.UserID + "_" + rowData.AcademicYear.replace(/\s/g,"-").replace(/\(/g,"\_").replace(/\)/g,"\_");
					var childHTML = "<div id='" + childID + "'><center><i class='fa fa-spinner fa-spin fa-fw text-default'></i></center></div>";
					try {
						if (dtableJS.vars.childTableID != "") {
							var childLinkObj = dtableJS.vars.childLinkObj;
							var prev_tr = childLinkObj;
//							var prev_tr = childLinkObj.parent().parent().parent();
							var prev_row = table.row(prev_tr);
							prev_row.child.remove();
							prev_tr.removeClass('shown');
						}
						dtableJS.vars.childTableID = childID;
						dtableJS.vars.childLinkObj = $(this); 
						row.child(childHTML).show();
						$('#' + childID).load(rel, function() {
							$(this).find('div').eq(0).hide().slideDown(800);
							$('a.btnTableSelectAll').off('click', dtableJS.listeners.selectAllHandler);
							$('a.btnTableSelectAll').on('click', dtableJS.listeners.selectAllHandler);
							
							$('a.btnTableDelete').off('click', dtableJS.listeners.ajaxCollectionDelHandler);
							$('a.btnTableDelete').on('click', dtableJS.listeners.ajaxCollectionDelHandler);
							
							$('a.btnAjax').off('click', navJS.listeners.ajaxHandler);
							$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
						});
					} catch(e) {
						dtableJS.vars.childTableID = "";
						dtableJS.vars.childLinkObj = "";
						$('#' + childID).remove();
					}

				} else {
					row.child(dtableJS.func.rowExpandFormat(row.data)).show();	
				}
				tr.addClass('shown');
				// tr.animateCss('rubberBand');
			}
			e.preventDefault();
		},
		delSuccessHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.delSuccessHandler", dtableJS );
			if (typeof e.result != "undefined" && e.result == "success") {
				notifyMsgJS.func.showMsg(langtxt["dataTable_delete_success"], 'success');
				var table = dtableJS.vars.tableObj;
				if (typeof table != "undefined") {
					table.draw();
				}
			} else {
				notifyMsgJS.func.showMsg(langtxt["dataTable_delete_fail"], 'danger');
			}
		},
		delFailHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.delFailHandler", dtableJS );
			$('a.btnTableDelete').on('click', dtableJS.listeners.ajaxCollectionDelHandler);
			notifyMsgJS.func.showMsg(langtxt["dataTable_delete_fail"], 'danger');
		},
		delRecHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.delRecHandler", dtableJS );
			var table = dtableJS.vars.tableObj;
			var tr = $(this).parent().parent();
			var row = table.row(tr);
			var clickObj = $(this);
			var d = row.data();
			if (typeof row != "undefined" && typeof d != "undefined") {
				dtableJS.vars.row_data = d;
				var msg = langtxt["ConfirmMsg_delete"].replace("__TITLE__", d.listTitle);
				dtableJS.vars.clickObj = $(this);
				notifyMsgJS.func.showPrompt(msg, dtableJS.func.removeConfirmHandler);
			}
			e.preventDefault();
		},
		editRecHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.editRecHandler", dtableJS );
			var table = dtableJS.vars.tableObj;
			var tr = $(this).parent().parent();
			var row = table.row(tr);
			var clickObj = $(this);
			var d = row.data();
			if (typeof row != "undefined" && typeof d != "undefined") {
				var strRel = clickObj.attr('rel');
				var url = navJS.vars.getURL + strRel + ".editrecord";
				$('#' + strRel).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + '</div>');
				navJS.func.getContent(url, d, $('#' + strRel));
			}
			e.preventDefault();
		},
		ajaxCollectionDelHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.ajaxCollectionDelHandler", dtableJS );
			$('a.btnTableDelete').off('click', dtableJS.listeners.ajaxCollectionDelHandler);
			var strREL = $(this).attr('rel');
			if (typeof strREL != "undefined") {
				if ($('#' + strREL).length > 0) {
					var msg = langtxt["ConfirmMsg_delete"].replace("__TITLE__", "");
					dtableJS.vars.coll_data = strREL;
					if ($('#' + dtableJS.vars.coll_data).length > 0) {
						var collectionData = [];
						if ($('#' + dtableJS.vars.coll_data).find('input[type="checkbox"]').length > 0) {
							$('#' + dtableJS.vars.coll_data).find('input[type="checkbox"]').each(function() {
								if ($(this).prop('checked')) {
									collectionData.push($(this).val());
								}
							});
							if (collectionData.length > 0) {
								notifyMsgJS.func.showPrompt(msg, dtableJS.func.removeCollectionConfirmHandler);
								return false;
							}
						}
					}
				}
			}
			$('a.btnTableDelete').on('click', dtableJS.listeners.ajaxCollectionDelHandler);
			notifyMsgJS.func.showMsg(langtxt["dataTable_selectAtLeastOne"], "danger");
			e.preventDefault();
		},
		selectAllHandler: function(e) {
			var checkstatus = false;
			if ($(dtableJS.vars.container).find('input[type="checkbox"]').length == $(dtableJS.vars.container).find('input[type="checkbox"]:checked').length) {
				checkstatus = true;
			}
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.listeners.selectAllHandler", dtableJS );
			
			if ($(dtableJS.vars.container).find('input[type="checkbox"]').length > 0) {
				$(dtableJS.vars.container).find('input[type="checkbox"]').each(function() {
					if ($(this).prop('checked') == checkstatus) {
						$(this).trigger('click');
					}
				});
			}
			e.preventDefault();
		}
	},
	func: {
		removeConfirmHandler: function(result) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.func.removeConfirmHandler", dtableJS );
			if (result) {
				clickObj = dtableJS.vars.clickObj;
				var strRel = clickObj.attr('rel');
				var data = dtableJS.vars.row_data;
				if (typeof strRel != "undefined") {
					var url = navJS.vars.getURL + strRel + ".deleterecord";
					navJS.func.getJSON(url, data, dtableJS.listeners.delSuccessHandler, dtableJS.listeners.delFailHandler);
				}
			}
		},
		removeCollectionConfirmHandler: function($result) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.func.removeCollectionConfirmHandler", dtableJS );
			if ($result) {
				if (dtableJS.vars.coll_data != "") {
					if ($('#' + dtableJS.vars.coll_data).length > 0) {
						var collectionData = [];
						if ($('#' + dtableJS.vars.coll_data).find('input[type="checkbox"]').length > 0) {
							$('#' + dtableJS.vars.coll_data).find('input[type="checkbox"]').each(function() {
								if ($(this).prop('checked')) {
									collectionData.push($(this).val());
								}
							});
							if (collectionData.length > 0) {
								notifyMsgJS.func.showMsg(langtxt["processing"], "info");
								url = "?task=" + navJS.vars.section + ".deleterecord";
								navJS.func.getJSON(url, { "recid" : collectionData }, dtableJS.listeners.delSuccessHandler, dtableJS.listeners.delFailHandler);
								return true;
							}
						}
					}
				}
				$('a.btnTableDelete').on('click', dtableJS.listeners.ajaxCollectionDelHandler);
				notifyMsgJS.func.showMsg(langtxt["dataTable_selectAtLeastOne"], "danger");
			} else {
				$('a.btnTableDelete').on('click', dtableJS.listeners.ajaxCollectionDelHandler);
			}
		},
		rowExpandFormat: function ( d ) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.func.rowExpandFormat", dtableJS );
			// `d` is the original data object for the row
			
//			if(typeof d === 'undefined'){
//				return '';
//			}
			return	'<div class="container-fluid">'+
					'	<div class="row">'+
					'		<div class="col-xs-6">'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["Content"] + ':</div>'+
					'				<div class="col-xs-8">'+d.Content +'</div>'+
					'			</div>'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["Mode"] + ':</div>'+
					'				<div class="col-xs-8">' + d.CPDModeName +'</div>'+
					'			</div>'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["Hour"] + ':</div>'+
					'				<div class="col-xs-8">' + d.TotalHours +'</div>'+
					'			</div>'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["Organizer"] + ':</div>'+
					'				<div class="col-xs-8">' + d.OrganizerName +'</div>'+
					'			</div>'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["CertificateName"] + ':</div>'+
					'				<div class="col-xs-8">' + d.CertificationName +'</div>'+
					'			</div>'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["AwardingInstitution"] + ':</div>'+
					'				<div class="col-xs-8">' + d.AwardingInstitution +'</div>'+
					'			</div>'+
					'		</div>'+
					'		<div class="col-xs-6">'+
					'			<div class="row">'+
					'				<div class="col-xs-4">' + langtxt["Category"] + ':</div>'+
					'				<div class="col-xs-8">' + d.Category +'</div>'+
					'			</div>'+
					'		</div>'+
					'	</div>'+
					'</div>';
		},
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.func.init", dtableJS );
			if ($(dtableJS.vars.container).length > 0 && $(dtableJS.vars.header_cols).length > 0) {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.func.init -- RUN", dtableJS );
				var table = dtableJS.vars.tableObj;
				try {
					table.destroy();
				} catch(err) {
					// Handle error(s) here
				}
				dtableJS.vars.tableObj = "";
				var columns = [];
				$(dtableJS.vars.header_cols).each(function(index, data) {
					var rel = $(this).attr('rel');
					var allowSort = true;
					if (typeof $(this).attr('data-sort') != "undefined") {
						if ($(this).attr('data-sort') == "false") {
							allowSort = false;
						}
					}
					if (typeof rel != "undefined") {
						if (rel != "func") {
							columns[index] = { "data": rel, "orderable": allowSort };
						} else {
							columns[index] =	{
													"className":      'details-control',
													"orderable":      false,
													"data":           "func",
													"defaultContent": ''
												};
						}
					}
				});
				var strRel= $(dtableJS.vars.container).attr('rel');
				var jsonurl = "";
				var pagination = true;
				var fixedHeader = false;
				var customSearch = "";
				if (navJS.vars.section != "") {
					jsonurl = navJS.vars.getURL + navJS.vars.section + ".listjson";
				} else {
					pagination = false;
					fixedHeader = true;
					navJS.vars.section = $(dtableJS.vars.container).attr('data-section');
					jsonurl = navJS.vars.getURL + navJS.vars.section + ".childlistjson";
					customSearch = $(dtableJS.vars.container).attr('data-search');
				}
				if (typeof strRel != "undefined") {
					jsonurl = navJS.vars.getURL + strRel;
				}
				var searchFunction = false;
				var dataSearching = $(dtableJS.vars.container).attr('data-searching');
				if (typeof dataSearching != "undefined") {
					searchFunction = true;
				}
				
				var searchPlhdr = langtxt["dataTable_searchPH"];
				if ($(dtableJS.vars.container).attr("data-noholder") == "true") {
					searchPlhdr = "";
				}
				dtableJS.vars.tableObj = $(dtableJS.vars.container).DataTable({
					"stateSave": true,
					"processing": true,
					"serverSide": true,
					"paging": pagination,
					"fixedHeader": fixedHeader,
					"searching": searchFunction,
					"sPaginationType": "listbox",
					"columns": columns,
					"lengthMenu": [[10, 20, 50, 100], [10, 20, 50, 100]],
					"language": {
						"emptyTable": "<div class='text-center'>" + langtxt["dataTable_emptyTable"] + "</div>",
						"loadingRecords": '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"],
						"processing": '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"],
						"info": langtxt["dataTable_info"],
						"zeroRecords": "<div class='text-center'>" + langtxt["dataTable_emptyTable"] + "</div>",
						"infoEmpty": "<div class='text-center'>" + langtxt["dataTable_infoEmpty"] + "</div>",
						"lengthMenu": langtxt["dataTable_lengthMenu"],
						"search": langtxt["dataTable_search"],
						"searchPlaceholder": searchPlhdr,
						"paginate": {
							"first": langtxt["dataTable_paginate_first"],
							"previous": langtxt["dataTable_paginate_previous"],
							"next": langtxt["dataTable_paginate_next"],
							"last": langtxt["dataTable_paginate_last"],
							"page": langtxt["dataTable_paginate_page"],
							"pageof": langtxt["dataTable_paginate_pageof"]
						}
					},
					"dom": '<"top">ft<"bottom"ipl><"clear">r',
					"order": [[ 0, "desc" ]],
					"ajax":  {
						"url": jsonurl,
						"type": "POST",
						"data": function ( d ) {
							if (navJS.vars.section == academicYearJS.vars.selectedYearSection) {
								d.selyear = academicYearJS.vars.selectedYear;
							} else {
								d.selyear = 'all';
							}
							if (typeof customSearch != "undefined" && customSearch != "") {
								d.s = customSearch;
							}
						},
						"complete": function(response) {
							var rel = response.responseJSON.data;
							var index =0;
							$("tbody tr").each(function(){
							    $(this).attr("rel",rel[index].rel);
							    index++;

							});
							if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " dtableJS.func.init >> Load Data Complete ", dtableJS );
							var table = dtableJS.vars.tableObj;
							appJS.func.linkInit();
							var wrapperWidth = $(dtableJS.vars.container).parent().width();
							var tableWidth = $(dtableJS.vars.container).width();
							if(tableWidth > wrapperWidth){
								$(dtableJS.vars.container).parent().width(tableWidth);
								$('.footer').width(tableWidth);
							}
							$( window ).resize(function() {
								$(dtableJS.vars.container).parent().width($(dtableJS.vars.container).width());
								$('.footer').width($(dtableJS.vars.container).width());
							});
							$(dtableJS.vars.container + ' td.details-control a.btn-del').off('click', dtableJS.listeners.delRecHandler);
							$(dtableJS.vars.container + ' td.details-control a.btn-del').on('click', dtableJS.listeners.delRecHandler);
							
							$(dtableJS.vars.container + ' td.details-control a.btn-edit').off('click', dtableJS.listeners.editRecHandler);
							$(dtableJS.vars.container + ' td.details-control a.btn-edit').on('click', dtableJS.listeners.editRecHandler);
							if (typeof response.responseText != "undefined") {
								var respJSON = $.parseJSON(response.responseText);
								if (typeof respJSON.acdyear != "undefined") {
										academicYearJS.func.yearFromJson(respJSON.acdyear);
								}
								if (typeof respJSON.debugInfo != "undefined") {
									debugMsgJS.func.showMsg(respJSON.debugInfo);
								}
							}
							if (typeof respJSON.allowExpand != "undefined" && respJSON.allowExpand) {
								$(dtableJS.vars.container + ' tr').off('click', dtableJS.listeners.expandHandler);
								$(dtableJS.vars.container + ' tr').on('click', dtableJS.listeners.expandHandler);
								
//								$(' td.details-control a.btn-detail').hide();
//								$(dtableJS.vars.container + ' td.details-control a.btn-detail').off('click', dtableJS.listeners.expandHandler);
//								$(dtableJS.vars.container + ' td.details-control a.btn-detail').on('click', dtableJS.listeners.expandHandler);

							}
							
							$('a.btnAjax').off('click', navJS.listeners.ajaxHandler);
							$('a.btnAjax').on('click', navJS.listeners.ajaxHandler);
							
							$('a.btnFDAjax').off('click', navJS.listeners.ajaxFDHandler);
							$('a.btnFDAjax').on('click', navJS.listeners.ajaxFDHandler);
							
							$('a.btnTableDelete').off('click', dtableJS.listeners.ajaxCollectionDelHandler);
							$('a.btnTableDelete').on('click', dtableJS.listeners.ajaxCollectionDelHandler);
							
							$('a.btnTableSelectAll').off('click', dtableJS.listeners.selectAllHandler);
							$('a.btnTableSelectAll').on('click', dtableJS.listeners.selectAllHandler);
							
							$('a.btnModal').off('click', navJS.listeners.modalClickHandler);
							$('a.btnModal').on('click', navJS.listeners.modalClickHandler);
						}
					},
					"timeout": appJS.vars.timeout,
					"error": dtableJS.listeners.handleAjaxError
				});
			}
		}
	}
};
/*********************************************************************/
var appJS = {
	vars: {
		clcolor: "ORANGE",
		xhr: '',
		newWinParam: "location=no,menubar=no,titlebar=no,width=1000",
		disableAllAction: false,
		allowAnimate: true,
		timeout: 1000 * 60 * 10
	},
	listeners: {
		printHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.printHandler", appJS );
			e.preventDefault();
			if ($(dtableJS.vars.container).find('td.details-control a.btn-detail').length > 0) {
				dtableJS.vars.showAll = true;
				$(dtableJS.vars.container).find('td.details-control a.btn-detail').each(function() {
					$(this).trigger('click');
				});
			}
			if (!appJS.vars.disableAllAction) {
				appJS.func.hideDebug();
				var divToPrint = $(".DivIdToPrint.active .content").eq(0);
				var id = $(".DivIdToPrint.active").eq(0).attr('id');
				var headerToPrint = $(".tp-header").eq(0);
				var html = "<html style='overflow:auto!important'><header>";
				$('link').each(function() { // find all <link tags that have
					if ($(this).attr('rel').indexOf('stylesheet') !=-1) { // rel="stylesheet"
						html += '<link rel="stylesheet" href="'+$(this).attr("href")+'" />';
					}
				});
				html += '</header><body onload="window.print();" style="overflow-y:auto;">';
				html += "<div class='tp-header'>" + headerToPrint.html() + "</div>";
				html += '<div class="printWrap content ' + id + '" >';
				html += '<div style="clear:both;"><br>';
				html += $(".logo").text() + " &gt; " + $('.tp-menu li.active > a').eq(0).text();
				html += '</div>';
				html += divToPrint.html();
				html += '</div></body></html>';
				var w = window.open("","print", appJS.vars.newWinParam);
				if (w) { w.document.write(html); w.document.close() }
			}
			if ($(dtableJS.vars.container).find('td.details-control a.btn-detail').length > 0) {
				dtableJS.vars.showAll = false;
				$(dtableJS.vars.container).find('td.details-control a.btn-detail').each(function() {
					$(this).trigger('click');
				});
			}
		},
		printAllHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.printAllHandler", appJS );
			var url = $(this).attr('href');
			var w = window.open(url, "print", appJS.vars.newWinParam);
			e.preventDefault();
		},
		disableHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.disableHandler", appJS );
			e.preventDefault();
		},
		changeLangHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.changeLangHandler", appJS );
			$("#" + navJS.vars.section).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + "</div>");
			var strRel = $(this).attr('rel');
			if (typeof strRel != "undefined") {
				strRel += "&section=" + navJS.vars.section;
				var purl = navJS.vars.getURL + "nav.changelang&lg=" + strRel;
				window.location.href = purl;
				// window.location.href = "changelang.php?lg=" + strRel;
			}
			e.preventDefault();
		},
		changeModeHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.changeModeHandler", appJS );
			$("#" + navJS.vars.section).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + "</div>");
			var strRel = $(this).attr('rel');
			if (typeof strRel != "undefined") {
				strRel += "&section=" + navJS.vars.section;
				// window.location.href = "changemode.php?lg=" + strRel;
				var purl = navJS.vars.getURL + "nav.changemode&lg=" + strRel + "&t=" + $.now();
				window.location.href = purl;
			}
			e.preventDefault();
		},
		thrChangeHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.thrChangeHandler", appJS );
			$("#" + navJS.vars.section).html('<div style="padding: 25px;"><i class="fa fa-circle-o-notch fa-spin fa-fw"></i> ' + langtxt["loading"] + "</div>");
			var purl = navJS.vars.getURL + "nav.changethr&lg=" + $(this).val();
			navJS.func.getJSON(purl, { thr: $(this).val(), k: tp_key, tp: "ajax" }, appJS.listeners.thrHandler, appJS.listeners.thrHandler );
			e.preventDefault();
		},
		thrHandler: function(response) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.listeners.thrHandler", appJS );
			// var purl = navJS.vars.getURL + navJS.vars.section;
			// navJS.func.getContent(purl, [], $("#" + navJS.vars.section));
			window.location.reload();
		}
	},
	func: {
		linkInit: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.func.linkInit", appJS );
			$("a[href='#']").off('click', appJS.listeners.disableHandler);
			$("a[href='#']").on('click', appJS.listeners.disableHandler);
		},
		hideDebug: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.func.hideDebug", appJS );
			$('#devobj').fadeOut(400, function() {});
		},
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " appJS.func.init", appJS );
			// $('.wrapper').animateCss("fadeIn");
			$('.wrapper').show();
			$('.user-lang a').on('click', appJS.listeners.changeLangHandler);
			$('.user-mode a').on('click', appJS.listeners.changeModeHandler);
			$('.thrList select').on('change', appJS.listeners.thrChangeHandler);
			
			if ($("body").hasClass('childwindow')) {
				dtableJS.func.init();	
			}
			
			/*
			if (!debugMsgJS.vars.allowConsole) {
				$(function() {
					$(this).bind("contextmenu", function(e) {
						e.preventDefault();
					});
				});
			}
			*/
			navJS.func.init();
			appJS.func.linkInit();
			$("html, body").css({"scrollTop":0});
			$("html, body").on("scroll", function(e) {
				e.preventDefault();
			});
		}
	}
};
/*********************************************************************/
var jqSortJS = {
	vars: {
		clcolor: "RED",
		mainhdr: ".settings",
		container: "cusSort",
		status: "sort",
		collectionIDs: "",
		adjustment: {},
		sortObj: ""
	},
	listeners: {
		addColumnHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.listeners.addColumnHandler", jqSortJS );
			$(jqSortJS.vars.mainhdr + " .btnFormFieldCancel").show();
			$(jqSortJS.vars.mainhdr + " .formPanel")
				.removeClass('panel-warning')
				.removeClass('panel-info')
				.addClass('panel-warning')
				.removeClass('hidden');
			e.preventDefault();
		},
		formCancelHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.listeners.formCancelHandler", jqSortJS );
			$(jqSortJS.vars.mainhdr + " .btnFormFieldCancel").hide();
			$(jqSortJS.vars.mainhdr + " .formPanel")
				.removeClass('panel-info')
				.removeClass('panel-warning')
				.addClass('hidden');
			e.preventDefault();
		},
		getJSONSuccess: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.listeners.getJSONSuccess", jqSortJS );
			if (typeof e.result != "undefined" && e.result == "success") {
				var collectionIDs = "";
				$("ol." + jqSortJS.vars.container + ".listhdr li").each(function(index) {
					var fldsid = $(this).attr("id");
					if (typeof fldsid != "undefined") {
						if (collectionIDs != "") {
							collectionIDs += ","+ fldsid.replace("field_", "");
						} else {
							collectionIDs = fldsid.replace("field_", "");
						}
					}
				});
				jqSortJS.vars.collectionIDs = collectionIDs;
				notifyMsgJS.func.showMsg(langtxt["updated"], "success");
			} else {
				notifyMsgJS.func.showMsg(langtxt["sortUpdateFailed"], "danger");
			}
		},
		getJSONFailed: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.listeners.getJSONFailed", jqSortJS );
			notifyMsgJS.func.showMsg(langtxt["sortUpdateFailed"], "danger");
		},
		formSubmitHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.listeners.formSubmitHandler", jqSortJS );
			var collectionIDs = "";
			var customRel = $(this).attr('rel');
			$("ol." + jqSortJS.vars.container + ":not(.editing) li").each(function(index) {
				var fldsid = $(this).attr("id");
				if (typeof fldsid != "undefined") {
					if (collectionIDs != "") {
						collectionIDs += ","+ fldsid.replace("field_", "");
					} else {
						collectionIDs = fldsid.replace("field_", "");
					}
				}
			});
			if (jqSortJS.vars.collectionIDs != collectionIDs) {
				notifyMsgJS.func.showMsg(langtxt["processing"], "info");
				var uppdata = { recid: collectionIDs, k: tp_key, tp: "ajax" };
				var purl = navJS.vars.getURL + navJS.vars.section + ".updatesort";
				if (typeof customRel != "undefined") {
					purl += "&custom=" + customRel;
				}
				navJS.func.getJSON(purl, uppdata, jqSortJS.listeners.getJSONSuccess, jqSortJS.listeners.getJSONFailed);
			} else {
				notifyMsgJS.func.showMsg(langtxt["updated"], "success");
			}
			e.preventDefault();
		}
	},
	func: {
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.func.init", jqSortJS );
			if ($("ol." + jqSortJS.vars.container).length > 0) {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.func.init -- RUN", jqSortJS );
				var collectionIDs = "";
				$("ol." + jqSortJS.vars.container + ".listhdr li").each(function(index) {
					var fldsid = $(this).attr("id");
					if (typeof fldsid != "undefined") {
						if (collectionIDs != "") {
							collectionIDs += ","+ fldsid.replace("field_", "");
						} else {
							collectionIDs = fldsid.replace("field_", "");
						}
					}
				});
				jqSortJS.vars.collectionIDs = collectionIDs;
				jqSortJS.vars.sortObj = jqSortJS.vars.container + "_" + $.now();
				$("ol." + jqSortJS.vars.container).addClass(jqSortJS.vars.sortObj);
				$("ol." + jqSortJS.vars.sortObj).sortable({
					itemSelector: 'li:not(.static)',
					group: jqSortJS.vars.sortObj,
					dropOnEmpty: true,
					pullPlaceholder: true,
					// animation on drop
					onDrop: function  ($item, container, _super) {
						if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.func.init >> Sortable > onDrop", jqSortJS );
						var $clonedItem = $('<li/>').css({height: 0});
						$item.before($clonedItem);
						$clonedItem.animate({'height': $item.height()});
						$item.animate($clonedItem.position(), function  () {
							$clonedItem.detach();
							_super($item, container);
						});
						var itemParent = $item.parent();
						$(jqSortJS.vars.mainhdr + " .btnFormFieldCancel").hide();
						if (itemParent.hasClass('editing')) {
							jqSortJS.vars.status = "editing";
						}
					},
					// set $item relative to cursor position
					onDragStart: function ($item, container, _super) {
						if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.func.init >> Sortable > onDragStart", jqSortJS );
						var offset = $item.offset(),
						pointer = container.rootGroup.pointer;
						jqSortJS.vars.adjustment = {
							left: pointer.left - offset.left,
							top: pointer.top - offset.top
						};
						_super($item, container);
					},
					onDrag: function ($item, position) {
						if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.func.init >> Sortable > onDrag", jqSortJS );
						$item.css({
							left: position.left - jqSortJS.vars.adjustment.left,
							top: position.top - jqSortJS.vars.adjustment.top
						});
					}
				});
				$(jqSortJS.vars.mainhdr + " a.btnFormAdd").off('click', jqSortJS.listeners.addColumnHandler);
				$(jqSortJS.vars.mainhdr + " a.btnFormFieldSubmit").off('click', jqSortJS.listeners.formSubmitHandler);
				$(jqSortJS.vars.mainhdr + " a.btnFormFieldCancel").off('click', jqSortJS.listeners.formCancelHandler);
				
				$(jqSortJS.vars.mainhdr + " a.btnFormAdd").on('click', jqSortJS.listeners.addColumnHandler);
				$(jqSortJS.vars.mainhdr + " a.btnFormFieldSubmit").on('click', jqSortJS.listeners.formSubmitHandler);
				$(jqSortJS.vars.mainhdr + " a.btnFormFieldCancel").on('click', jqSortJS.listeners.formCancelHandler);
				
				if ($(jqSortJS.extra.optionForm.vars.container).length > 0) {
					jqSortJS.extra.optionForm.func.init();
				}
			}
		}
	},
	extra: {
		optionForm: {
			vars: {
				clcolor: "MAROON",
				container: ".mainPanel",
				jcontainer: ".jqbuttonPanel",
				itemButton: ".settings a.jqbutton",
				addButton: ".jqAddBtn",
				selectData: {}
			},
			listeners: {
				getJSONSuccess: function(response) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.listeners.getJSONSuccess", jqSortJS.extra.optionForm );
					if (typeof response.result != "undefined" && response.result == "success") {
						var purl = navJS.vars.getURL + navJS.vars.section + ".settings&otype=" + response.data.sec;
						navJS.func.getContent(purl, response.data, $("#" + navJS.vars.section));
						notifyMsgJS.func.showMsg(langtxt["updated"], "success");
						
					} else {
						notifyMsgJS.func.showMsg(langtxt["UpdateFailed"], "danger");
					}
				},
				getJSONFailed: function(data, status, err) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.listeners.getJSONFailed", jqSortJS.extra.optionForm );
					var errHtml = "<i class='fa fa-warning'></i> ERROR. Please Check network and try again.";
					notifyMsgJS.func.showMsg(errHtml, "danger");
				},
				panelResetHandler: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.listeners.panelResetHandler", jqSortJS.extra.optionForm );
					jqSortJS.extra.optionForm.func.hideSubPanel();
					e.preventDefault();
				},
				panelSubmitHandler: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.listeners.panelSubmitHandler", jqSortJS.extra.optionForm );
					// $.notifyClose();
					if (jqSortJS.extra.optionForm.func.checkInputData()) {
						notifyMsgJS.func.showMsg(langtxt["processing"], "info");
						var id = $("#recid").val();
						var formData = {
								recid: id.replace("field_", ""),
								nameCN: $("#nameCN").val(),
								nameEN: $("#nameEN").val(),
								otype: $("#otype").val(),
								k: tp_key,
								tp: "ajax"
							};
						var purl = navJS.vars.getURL + navJS.vars.section + ".optionsmanage";
						navJS.func.getJSON(purl, formData, jqSortJS.extra.optionForm.listeners.getJSONSuccess, jqSortJS.extra.optionForm.listeners.getJSONFailed);
					}
					e.preventDefault();
				},
				jqbuttonClickHandler: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.listeners.jqbuttonClickHandler", jqSortJS.extra.optionForm );
					var parentObj=  $(this).closest("li");
					var itemData = jqSortJS.extra.optionForm.func.getItemData($(this));
					jqSortJS.extra.optionForm.vars.selectData = itemData;
					if ($(this).hasClass('jqpencil')) {
						parentObj.animateCss('flash');
						jqSortJS.extra.optionForm.func.showSubPanel("edit");
					} else if ($(this).hasClass('jqtrash')) {
						parentObj.animateCss('shake');
						var title = "";
						title = parentObj.attr('title');
						if (typeof title == "undefined") {
							title = "";
						}
						var msg = langtxt["ConfirmMsg_delete"].replace("__TITLE__", title);
						notifyMsgJS.func.showPrompt(msg, jqSortJS.extra.optionForm.func.removeConfirmHandler);
					}
					e.preventDefault();
				},
				jqAddClickHandler: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.listeners.jqAddClickHandler", jqSortJS.extra.optionForm );
					$('#recid').val(0);
					$("#nameCN").val('');
					$("#nameEN").val('');
					jqSortJS.extra.optionForm.func.showSubPanel('add');
					e.preventDefault();
				}
			},
			func: {
				removeConfirmHandler: function(result) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.func.removeConfirmHandler", jqSortJS.extra.optionForm );
					if (result) {
						notifyMsgJS.func.showMsg(langtxt["processing"], "info");
						var selectedData = jqSortJS.extra.optionForm.vars.selectData;
						var id = $("#recid").val();
						var formData = {
								recid: selectedData.recid.replace("field_", ""),
								nameCN: selectedData.nameCN,
								nameEN: selectedData.nameEN,
								otype: $("#otype").val(),
								action: "delete",
								k: tp_key,
								tp: "ajax"
							};
						var purl = navJS.vars.getURL + navJS.vars.section + ".optionsmanage";
						navJS.func.getJSON(purl, formData, jqSortJS.extra.optionForm.listeners.getJSONSuccess, jqSortJS.extra.optionForm.listeners.getJSONFailed);
					}
				},
				showSubPanel: function(panelType) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.func.showSubPanel", jqSortJS.extra.optionForm );
					var typeLbl = ".type_add";
					if (panelType == "edit") {
						typeLbl = ".type_edit";
					}
					$(jqSortJS.extra.optionForm.vars.jcontainer)
						.removeClass('col-xs-12')
						.addClass('col-xs-6')
						.find('.formtype:not(' + typeLbl + ')')
						.hide();
					$(jqSortJS.extra.optionForm.vars.jcontainer)
						.removeClass('col-xs-12')
						.addClass('col-xs-6')
						.find(typeLbl).show();
					
					$(jqSortJS.extra.optionForm.vars.jcontainer)
						.removeClass('col-xs-12')
						.addClass('col-xs-6')
						.show()
						.animateCss("fadeInRight");
					$(jqSortJS.extra.optionForm.vars.container)
						.removeClass('col-xs-12')
						.addClass('col-xs-6');
					$(jqSortJS.extra.optionForm.vars.jcontainer).find('.tplReset').off('click', jqSortJS.extra.optionForm.listeners.panelResetHandler);
					$(jqSortJS.extra.optionForm.vars.jcontainer).find('.tplReset').on('click', jqSortJS.extra.optionForm.listeners.panelResetHandler);
					
					$(jqSortJS.extra.optionForm.vars.jcontainer).find('.tplSubmit').off('click', jqSortJS.extra.optionForm.listeners.panelSubmitHandler);
					$(jqSortJS.extra.optionForm.vars.jcontainer).find('.tplSubmit').on('click', jqSortJS.extra.optionForm.listeners.panelSubmitHandler);
				},
				hideSubPanel: function() {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.func.hideSubPanel", jqSortJS.extra.optionForm );
					$(jqSortJS.extra.optionForm.vars.jcontainer)
						.removeClass('col-xs-12')
						.addClass('col-xs-6')
						.hide();
					$(jqSortJS.extra.optionForm.vars.container)
						.delay(1000)
						.removeClass('col-xs-6')
						.addClass('col-xs-12');
				},
				getItemData: function(obj) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.func.getItemData", jqSortJS.extra.optionForm );
					// CPD Form Object
					var liObj = obj.closest( "li" );
					var data = { recid: liObj.attr('id'), nameCN: liObj.attr('data-nameCN'), nameEN: liObj.attr('data-nameEN') };
					if (data.recid != "undefined") {
						$('#recid').val(data.recid);
						if (data.nameCN != "undefined") $("#nameCN").val(data.nameCN);
						else $("#nameCN").val('');
						
						if (data.nameEN != "undefined") $("#nameEN").val(data.nameEN);
						else $("#nameEN").val('');
					} else {
						$('#recid').val(0);
						$("#nameCN").val('');
						$("#nameEN").val('');
					}
					return data;
				},
				checkInputData: function() {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.func.checkInputData", jqSortJS.extra.optionForm );
					$(jqSortJS.extra.optionForm.vars.jcontainer).find('input.error').removeClass('error');
					var nameCN = $("#nameCN").val();
					var nameEN = $("#nameEN").val();
					if (nameCN == "" || typeof nameCN == "undefined") {
						$("#nameCN").addClass('error');
					}
					if (nameEN == "") {
						$("#nameEN").addClass('error');
					}
					if ($(jqSortJS.extra.optionForm.vars.jcontainer).find('input.error').length > 0) {
						notifyMsgJS.func.showMsg(langtxt["allRequiredFields"], 'danger');
					} else {
						return true;
					}
					return false;
				},
				init: function() {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " jqSortJS.extra.optionForm.func.init", jqSortJS.extra.optionForm );
					
					$(jqSortJS.extra.optionForm.vars.itemButton).off('click', jqSortJS.extra.optionForm.listeners.jqbuttonClickHandler);
					$(jqSortJS.extra.optionForm.vars.itemButton).on('click', jqSortJS.extra.optionForm.listeners.jqbuttonClickHandler);
					
					$(jqSortJS.extra.optionForm.vars.addButton).off('click', jqSortJS.extra.optionForm.listeners.jqAddClickHandler);
					$(jqSortJS.extra.optionForm.vars.addButton).on('click', jqSortJS.extra.optionForm.listeners.jqAddClickHandler);
				}
			}
		}
	}
};
/*********************************************************************/
var stepPanelJS = {
	vars: {
		clcolor: "FUCHSIA",
		container: ".steppanel",
		stepProcess: ".bs-wizard-step",
		currStep: 1,
		step1data: {},
		xhr: ""
	},
	extra: {
		step1: {
			listeners: {
				customUploadComplete: function (up, files) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step1.listeners.customUploadComplete [ Uploader > init ]", stepPanelJS );
					if (!filesJS.vars.errors) {
						stepPanelJS.vars.currStep = 0;
						$(stepPanelJS.vars.container).eq(stepPanelJS.vars.currStep).slideUp(800);
						stepPanelJS.vars.currStep = 1;
						stepPanelJS.extra.step1.func.init();
					}
				},
				nextStep: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step1.listeners.nextStep" );
					stepPanelJS.vars.currStep = 2;
					var msg = langtxt["processing"];
					notifyMsgJS.func.showMsg(msg, "info");
					setTimeout("stepPanelJS.extra.step2.func.init()", 1000);
					e.preventDefault();
				},
				getJSONSuccess: function(response) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step1.listeners.getJSONSuccess" );
					var step1Obj = $(stepPanelJS.vars.container).eq(1);
					if (typeof response.errorCode != "undefined") {
						// $(stepPanelJS.vars.stepProcess).eq(0).find('.progress-bar').eq(0).css({ "width": "0%" });
						var errHtml = "<h3 class='text-danger'><i class='fa fa-warning'></i> " + response.Msg + "</h3>";
						step1Obj.find('.panel').eq(0).removeClass('panel-default').addClass('panel-danger');
						step1Obj.find('.panel-body').eq(0).find('.processing').remove();
						step1Obj.find('.panel-body').eq(0).append(errHtml);
						step1Obj.find('.panel-footer .btn-success').hide();
						step1Obj.find('.panel-footer .btn-danger').show();
					} else {
						var successHtml = "" + response.statusmsg + "";
						if (typeof response.result == "undefined" || response.result == "error") {
							step1Obj.find('.panel').eq(0).removeClass('panel-default').addClass('panel-danger');
							step1Obj.find('.panel-footer .btn-success').hide();
							step1Obj.find('.panel-footer .btn-danger').show();
						} else {
							stepPanelJS.vars.step1data = response;
							step1Obj.find('.panel').eq(0).removeClass('panel-default').addClass('panel-success');
							step1Obj.find('.panel-footer .btn-success').show();
							step1Obj.find('.panel-footer .btn-danger').show();
							step1Obj.find('.panel-footer .btn-success').off('click', stepPanelJS.extra.step1.listeners.nextStep);
							step1Obj.find('.panel-footer .btn-success').on('click', stepPanelJS.extra.step1.listeners.nextStep);
						}
						step1Obj.find('.panel-body').eq(0).find('.processing').remove();
						step1Obj.find('.panel-body').eq(0).append(successHtml);
					}
				},
				getJSONFailed: function(data, status, err) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step1.listeners.getJSONFailed" );
					// $(stepPanelJS.vars.stepProcess).eq(0).find('.progress-bar').eq(0).css({ "width": "0%" });
					var errHtml = "<h3 class='text-danger'><i class='fa fa-warning'></i> ERROR. Please Check network and try again.</h3>";
					var step1Obj = $(stepPanelJS.vars.container).eq(1);
					step1Obj.find('.panel-body').eq(0).find('.processing').remove();
					step1Obj.find('.panel-body').eq(0).append(errHtml);
				}
			},
			func: {
				init: function() {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step1.func.init" );
					if (stepPanelJS.vars.currStep == 1) {
						if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step1.func.init -- RUN" );

						$(stepPanelJS.vars.container).eq(0).slideUp(800);
						$(stepPanelJS.vars.stepProcess).eq(0).find('.progress-bar').eq(0).css({ "width": "100%" });
						$(stepPanelJS.vars.container).eq(1).find('.panel-footer .btn-success').hide();
						$(stepPanelJS.vars.container).eq(1).find('.panel-footer .btn-danger').show();
						$(stepPanelJS.vars.container).eq(1).slideDown(800);
						stepPanelJS.vars.step1data = {};
						var strRel = $(stepPanelJS.vars.container).eq(stepPanelJS.vars.currStep).attr('rel');
						if (typeof strRel != "undefined") {
							var purl = navJS.vars.getURL + strRel;
							var pdata = [];
							navJS.func.getJSON(purl, pdata, stepPanelJS.extra.step1.listeners.getJSONSuccess, stepPanelJS.extra.step1.listeners.getJSONFailed);
						}
					}
				}
			}
		},
		step2: {
			vars: {
				tableObj: ""
			},
			listeners: {
				deleteHandler: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step2.listeners.deleteHandler", stepPanelJS );
					var obj = $(this).parent().parent();
					if (typeof obj != "undefined") {
						var id = obj.attr("id");
						if (typeof id != "undefined") {
							id = id.replace("row_", "");
						}
						var data = stepPanelJS.vars.step1data;
						if (typeof data.csvData[id] != "undefined") {
							delete data.csvData[id];
							stepPanelJS.vars.step1data.csvData = data.csvData;
						}
						obj.remove();
						if ($('#csvData tr').length < 2) {
							var step2Obj = $(stepPanelJS.vars.container).eq(2);
							step2Obj.find('.panel-footer .btn-success').hide();
						}
					}
					e.preventDefault();
				},
				nextStep: function(e) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step2.listeners.nextStep", stepPanelJS );
					var step2Obj = $(stepPanelJS.vars.container).eq(2);
					stepPanelJS.extra.step2.func.removeDupfunc(step2Obj);
					stepPanelJS.vars.currStep = 3;
					stepPanelJS.extra.step3.func.init();
					e.preventDefault();
				}
			},
			func: {
				removeDupfunc: function(step2Obj) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step2.func.removeDupfunc", stepPanelJS );
				},
				init: function() {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step2.func.init", stepPanelJS );
					if (stepPanelJS.vars.currStep == 2) {
						// $.notifyClose();
						$(stepPanelJS.vars.stepProcess).eq(0).removeClass('disabled');
						$(stepPanelJS.vars.stepProcess).eq(0).removeClass('active');
						$(stepPanelJS.vars.stepProcess).eq(0).addClass('complete');
						$(stepPanelJS.vars.stepProcess).eq(1).removeClass('disabled');
						$(stepPanelJS.vars.stepProcess).eq(1).addClass('active');

						$(stepPanelJS.vars.container).eq(0).hide();
						$(stepPanelJS.vars.container).eq(1).slideUp(800);
						$(stepPanelJS.vars.container).eq(2).find('.panel-footer .btn-success').hide();
						$(stepPanelJS.vars.container).eq(2).find('.panel-footer .btn-danger').show();
						$(stepPanelJS.vars.container).eq(2).slideDown(800);
						
						var data = stepPanelJS.vars.step1data;
						var tableHTML = "No record";
						if (typeof data != "undefined") {
							tableHTML = data.tableHTML;
						}
						var step2Obj = $(stepPanelJS.vars.container).eq(2);
						step2Obj.find('.panel-body').eq(0).find('.processing').remove();
						step2Obj.find('.panel-body').eq(0).append(tableHTML);
						
						var totalimprt = 0;
						var totalvld = 0;
						var totaldup = 0;
						if (typeof $("#tpip_totalvalid") != "undefined") {
							totalvld = $("tr.valid").length;
							totalimprt += totalvld; 
							$("#tpip_totalvalid").text(totalvld);
						}
						if (typeof $("#tpip_totaldup") != "undefined") {
							totaldup = $("tr.duplicate").length;
							totalimprt += totaldup;
							$("#tpip_totaldup").text(totaldup);
						}
						$("#tpip_totalimport").text(totalimprt);
						var totalerr = 0;
						if (typeof $("#tpip_totalerror") != "undefined") {
							totalerr = $("tr.invalid").length;
							totalimprt += totalerr;
							$("#tpip_totalerror").text(totalerr);
						}
						if (typeof $("#tpip_totalrecord") != "undefined") {
							$("#tpip_totalrecord").text(totalimprt);
						}
						$('.btnShowVaild').show();
						$('.btnShowDup').show();
						$('.btnShowError').show();

						$('a.csvImpHS').off('click');
						$('a.csvImpHS').on('click', function() {
							if ($(this).hasClass('btnRemoveDup')) {
								var msg = "Are you sure to delete?";
								notifyMsgJS.func.showPrompt(msg, function(result) {
									if (result) {
										stepPanelJS.step2.func.removeDupfunc(step2Obj);
										var totalimpt = $("tr.duplicate").length;
										var totalvld = $("tr.valid").length;
										$("#tpip_totaldup").text(totalimpt);
										$("#tpip_totalimport").text(totalimpt + totalvld);
										
										step2Obj.find('.panel-body').eq(0).find('tr').hide();
										step2Obj.find('.panel-body').eq(0).find('tr').eq(0).show();
										step2Obj.find('.panel-body').eq(0).find('tr.valid').show();
									}
								});
							} else {
								if ($(this).hasClass('btnShowAll')) {
									step2Obj.find('.panel-body').eq(0).find('tr').show();
								} else {
									step2Obj.find('.panel-body').eq(0).find('tr').hide();
									step2Obj.find('.panel-body').eq(0).find('tr').eq(0).show();
								}
								if ($(this).hasClass('btnShowImpt')) {
									step2Obj.find('.panel-body').eq(0).find('tr.valid').show();
									step2Obj.find('.panel-body').eq(0).find('tr.duplicate').show();
								}
								if ($(this).hasClass('btnShowVaild')) {
									step2Obj.find('.panel-body').eq(0).find('tr.valid').show();
								}
								if ($(this).hasClass('btnShowDup')) {
									step2Obj.find('.panel-body').eq(0).find('tr.duplicate').show();
								}
								if ($(this).hasClass('btnShowError')) {
									step2Obj.find('.panel-body').eq(0).find('tr.invalid').show();
								}
							}
						});

						$('[data-tooltip!=""]').qtip({
							content: {
								attr: 'data-tooltip'
							},
							style: {
								classes: 'qtip-red'
							},
							position: {
								// target: 'mouse'
								at: 'bottom left'
							}
						});
						$("#csvData a.rowDel").on('click', stepPanelJS.extra.step2.listeners.deleteHandler);
						// stepPanelJS.extra.step2.vars.tableObj = table;
						if(totalvld > 0){
							step2Obj.find('.panel-footer .btn-success').show();
							step2Obj.find('.panel-footer .btn-success').off('click', stepPanelJS.extra.step2.listeners.nextStep);
							step2Obj.find('.panel-footer .btn-success').on('click', stepPanelJS.extra.step2.listeners.nextStep);
						}
						step2Obj.find('.panel-footer .btn-danger').show();
					}
				}
			}
		},
		step3: {
			vars: {},
			listeners: {
				getJSONSuccess: function(response) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step3.listeners.getJSONSuccess", stepPanelJS );
					var msg = "ERROR. Please Check network and try again.";
					var errHtml = "<h3 class='text-danger'><i class='fa fa-warning'></i> " + msg + "</h3>";
					var step3Obj = $(stepPanelJS.vars.container).eq(3);
					if (typeof response.errorCode != "undefined") {
						if (typeof response.Msg != "undefined") {
							msg = response.Msg;
							msgHtml = "<h3 class='text-danger'><i class='fa fa-warning'></i> " + msg + "</h3>";
							step3Obj.find('.panel').eq(0).removeClass('panel-default').removeClass('panel-success').addClass('panel-danger');
						}
					} else {
						if (typeof response.Msg != "undefined") {
							msg = response.Msg;
							step3Obj.find('.panel-footer').remove();
							msgHtml = "<h3 class='text-success'><span class='glyphicon glyphicon-ok'></span> " + msg + "</h3>";
							step3Obj.find('.panel').eq(0).removeClass('panel-default').removeClass('panel-success').addClass('panel-success');
						}
					}
					step3Obj.find('.panel-body').eq(0).find('.processing').remove();
					step3Obj.find('.panel-body').eq(0).append(msgHtml);
				},
				getJSONFailed: function(data, status, err) {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step3.listeners.getJSONFailed", stepPanelJS );
					// $(stepPanelJS.vars.stepProcess).eq(0).find('.progress-bar').eq(0).css({ "width": "0%" });
					var errHtml = "<h3 class='text-danger'><i class='fa fa-warning'></i> ERROR. Please Check network and try again.</h3>";
					var step3Obj = $(stepPanelJS.vars.container).eq(3);
					step3Obj.find('.panel-body').eq(0).find('.processing').remove();
					step3Obj.find('.panel-body').eq(0).append(errHtml);
					step3Obj.find('.panel').eq(0).removeClass('panel-default').removeClass('panel-success').addClass('panel-danger');
				}
			},
			func: {
				init: function() {
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step3.func.init", stepPanelJS );
					if (stepPanelJS.vars.currStep == 3) {
						if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.extra.step3.func.init -- RUN", stepPanelJS );

						$(stepPanelJS.vars.stepProcess).eq(0).removeClass('disabled');
						$(stepPanelJS.vars.stepProcess).eq(0).removeClass('active');
						$(stepPanelJS.vars.stepProcess).eq(0).addClass('complete');
						$(stepPanelJS.vars.stepProcess).eq(1).removeClass('disabled');
						$(stepPanelJS.vars.stepProcess).eq(1).removeClass('active');
						$(stepPanelJS.vars.stepProcess).eq(1).addClass('complete');
						$(stepPanelJS.vars.stepProcess).eq(2).removeClass('disabled');
						$(stepPanelJS.vars.stepProcess).eq(2).addClass('active');
						$(stepPanelJS.vars.container).eq(2).slideUp(800);
						// $(stepPanelJS.vars.stepProcess).eq(2).find('.progress-bar').eq(0).css({ "width": "100%" });
						$(stepPanelJS.vars.container).eq(3).slideDown(800);
						var strRel = $(stepPanelJS.vars.container).eq(stepPanelJS.vars.currStep).attr('rel');
						if (typeof strRel != "undefined") {
							var purl = navJS.vars.getURL + strRel;
							var pdata = stepPanelJS.vars.step1data.csvData;
							var uppdata = { csvData: JSON.stringify(pdata) };
							navJS.func.getJSON(purl, uppdata, stepPanelJS.extra.step3.listeners.getJSONSuccess, stepPanelJS.extra.step3.listeners.getJSONFailed);
						}
					}
				}
			}
		}
	},
	func: {
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.func.init", stepPanelJS );
			if ($(stepPanelJS.vars.container).length > 0) {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " stepPanelJS.func.init -- RUN", stepPanelJS  );
				filesJS.listeners.customUploadComplete = stepPanelJS.extra.step1.listeners.customUploadComplete;
				stepPanelJS.vars.currStep = 0;
				$(stepPanelJS.vars.container).eq(stepPanelJS.vars.currStep).show();
				switch (stepPanelJS.vars.currStep) {
					case 1: stepPanelJS.extra.step1.func.init(); break;
					case 2: stepPanelJS.extra.step2.func.init(); break;
				}
			}
		}
	}
};
/*********************************************************************/
var manageUserJS = {
	vars: {
		clcolor: "#87D37C",
		container: ".manageUserList",
		suggBox: "#suggesstion-box",
		inputObj: "input.ajaxAutoComplete",
		suggResult: {},
		timer: "",
		req: "",
		selectedObj: "SelectedUserIDArr[]"
	},
	listeners: {
		autoCHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.autoCHandler", manageUserJS );
			var val = $(manageUserJS.vars.inputObj).eq(0).val();
			if (val == "") {
				$(manageUserJS.vars.suggBox).hide();
			} else {
				if (manageUserJS.vars.req != val) {
					manageUserJS.vars.req = val;
					$(manageUserJS.vars.suggBox).html('<i class="fa fa-spinner fa-spin fa-fw"></i>').css({ "height": "auto" }).show();
					if (manageUserJS.vars.timer != "") {
						clearTimeout(manageUserJS.vars.timer);
						manageUserJS.vars.timer = "";
					}
					manageUserJS.vars.timer = setTimeout("manageUserJS.func.getSugg()", 500);
				} else {
					$(manageUserJS.vars.suggBox).show();
				}
			}
		},
		autoCSuccess: function(response) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.autoCSuccess", manageUserJS );
			if (typeof response.data != "undefined") {
				manageUserJS.vars.suggResult = response.data;
				if (Object.keys(response.data).length > 0) {
					var strHTML = '<ul class="list-unstyled">';
					var sub_strHTML = "";
					$.each(response.data, function(index, jdata) {
						var str = jdata.ul;
						if ($("select[name='" + manageUserJS.vars.selectedObj + "']").eq(0).find('option[value="U' + jdata.id + '"]').length == 0) {
							sub_strHTML += '<li><a href="#" class="btnSelectUser text-muted" rel="' + jdata.id + '"><i class="fa fa-angle-right"></i> ' + jdata.name + ' (Login ID: ' + str.replace(manageUserJS.vars.req, "<span class='text-primary'>" + manageUserJS.vars.req + "</span>") + ')</a>';
						}
					});
					if (sub_strHTML != "") {
						strHTML += sub_strHTML + '</ul>';
						$(manageUserJS.vars.suggBox).stop().html(strHTML).show();
						$('a.btnSelectUser').off("click", manageUserJS.listeners.suggLinkClickHandler);
						$('a.btnSelectUser').on("click", manageUserJS.listeners.suggLinkClickHandler);
					} else {
						$(manageUserJS.vars.suggBox).stop().empty().hide();
					}
				} else {
					$(manageUserJS.vars.suggBox).stop().empty().hide();
				}
			} else {
				$(manageUserJS.vars.suggBox).stop().empty().hide();
			}
		},
		autoCFail: function(data, status, err) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.autoCFail", manageUserJS );
			$(manageUserJS.vars.suggBox).hide();
		},
		autoCBlurHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.autoCBlurHandler", manageUserJS );
			// $(manageUserJS.vars.suggBox).hide();
		},
		closeSugguestBox: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.closeSugguestBox", manageUserJS );
			$(manageUserJS.vars.suggBox).hide();
		},
		suggLinkClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.suggLinkClickHandler", manageUserJS );
			$("#selectBox").append('<option value="option6">option6</option>');
			$(manageUserJS.vars.suggBox).stop().show();
			$(manageUserJS.vars.suggBox).off('mouseleave', manageUserJS.listeners.closeSugguestBox);
			$(manageUserJS.vars.suggBox).on('mouseleave', manageUserJS.listeners.closeSugguestBox);
			$("select[name='" + manageUserJS.vars.selectedObj + "']").eq(0).append('<option value="U' + $(this).attr('rel') + '">' + $(this).text() + '</option>');
			$(manageUserJS.vars.inputObj).eq(0).val('')
			$(this).remove();
			if ($('a.btnSelectUser').length == 0) {
				$(manageUserJS.vars.suggBox).hide();
			}
			e.preventDefault();
		},
		removeSelectedHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.removeSelectedHandler", manageUserJS );
			$("select[name='" + manageUserJS.vars.selectedObj + "']").eq(0).children('option:selected').remove();
			e.preventDefault();
		},
		selectedUserSubmitHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.selectedUserSubmitHandler", manageUserJS );
			var msg = "";
			if ($("select[name='" + manageUserJS.vars.selectedObj + "']").eq(0).children('option').length > 0) {
				manageUserJS.func.submitSelectedUser();
			} else {
				msg = langtxt["manage_selectAtLeastOne"];
				notifyMsgJS.func.showMsg(msg, "danger");
				// notifyMsgJS.func.showPrompt(msg, manageUserJS.func.submitSelectedUser);
			}
			e.preventDefault();
		},
		jsonSubmitSuccess: function(response) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.jsonSubmitSuccess", manageUserJS );
			msg = langtxt["manage_updateSuccess"];
			notifyMsgJS.func.showMsg(msg, "success");
			$("a.btnBack").trigger('click');
		},
		jsonSubmitFail: function(data, status, err) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.listeners.jsonSubmitFail", manageUserJS );
			msg = langtxt["manage_updateFail"];
			notifyMsgJS.func.showMsg(msg, "danger");
		}
	},
	func: {
		submitSelectedUser: function() {
			var selectedUser = [];
			if ($("select[name='" + manageUserJS.vars.selectedObj + "']").eq(0).children('option').length > 0) {
				$("select[name='" + manageUserJS.vars.selectedObj + "']").eq(0).children('option').each(function(index, data) {
					selectedUser.push($(this).val());
				});
				var strHref = "process";
				var purl = navJS.vars.getURL + navJS.vars.section + "." + strHref;
				
				msg = langtxt["processing"];
				notifyMsgJS.func.showMsg(msg, "info");
			
				navJS.func.getJSON(purl, { sUser: selectedUser, k: tp_key, tp: "ajax" }, manageUserJS.listeners.jsonSubmitSuccess, manageUserJS.listeners.jsonSubmitFail);
			}
		},
		getSugg: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.func.getSugg", manageUserJS );
			var strHref = $(manageUserJS.vars.inputObj).eq(0).attr('rel');
			var purl = navJS.vars.getURL + navJS.vars.section + "." + strHref;
			var val = $(manageUserJS.vars.inputObj).eq(0).val();
			navJS.func.getJSON(purl, { q: val, k: tp_key, tp: "ajax" }, manageUserJS.listeners.autoCSuccess, manageUserJS.listeners.autoCFail);
		},
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.func.init", manageUserJS );
			if ($(manageUserJS.vars.container).length > 0 && $(manageUserJS.vars.inputObj).length > 0) {
				if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " manageUserJS.func.init -- RUN", manageUserJS );
				
				$('input.ajaxAutoComplete').off('keyup', manageUserJS.listeners.autoCHandler)
				$('input.ajaxAutoComplete').on('keyup', manageUserJS.listeners.autoCHandler)
				
				$('input.ajaxAutoComplete').off('blur', manageUserJS.listeners.autoCBlurHandler)
				$('input.ajaxAutoComplete').on('blur', manageUserJS.listeners.autoCBlurHandler)
				
				$('a.btnManageSubmit').off('click', manageUserJS.listeners.selectedUserSubmitHandler)
				$('a.btnManageSubmit').on('click', manageUserJS.listeners.selectedUserSubmitHandler)
				
				$('a.btnRemoveSelected').off('click', manageUserJS.listeners.removeSelectedHandler)
				$('a.btnRemoveSelected').on('click', manageUserJS.listeners.removeSelectedHandler)
			}
		}
	}
}
/*********************************************************************/
if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	var msViewportStyle = document.createElement('style');
	msViewportStyle.appendChild(
		document.createTextNode(
			'@-ms-viewport{width:auto!important}'
		)
	);
	document.querySelector('head').appendChild(msViewportStyle);
}

$.fn.extend({
    animateCss: function (animationName) {
		if (appJS.vars.allowAnimate) {
			var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
			this.addClass('animated ' + animationName).one(animationEnd, function() {
				$(this).removeClass('animated ' + animationName);
			});
		}
    }
});
/*********************************************************************/
$(document).ready(function(){
	if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " DOCUMENT READY " );
	appJS.func.init();
	if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog( " DOCUMENT READY >> ScrollToTop" );
	$("html, body").css({"scrollTop":0});
});