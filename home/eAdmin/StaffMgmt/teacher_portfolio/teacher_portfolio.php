<?php
if (defined('TEACHER_PORTFOLIO')) {
	class TeacherPortfolio {
		public function __construct($parent = null) {
			$this->parent = $parent;
			
		}

		public function mapConvert($data, $map) {
			$result = array();
			$result["hasError"] = "N";
			$result["dataInfo"] = array();
			if (count($map) > 0) {
				foreach ($map as $kk => $vv) {
					$result["dataInfo"][$vv["field"]] = $vv;
					if (isset($data[$kk])) {
						$result["dataInfo"][$vv["field"]]["org_key"] = $kk;
						$result["dataInfo"][$vv["field"]]["data"] = $data[$kk];
						$result["dataInfo"][$vv["field"]]["error"] = "N";
						if ($vv["required"] && empty($data[$kk])) {
							$result["hasError"] = "Y";
							$result["dataInfo"][$vv["field"]]["error"] = "Y";
						} else {
							switch ($vv["type"]) {
								case "int":
									if (!isInteger($result["dataInfo"][$vv["field"]]["data"])) {
										$result["dataInfo"][$vv["field"]]["error"] = "Y";
										$result["hasError"] = "Y";
									}
									break;
								case "intArray":
									if (count($result["dataInfo"][$vv["field"]]["data"]) > 0) {
										foreach ($result["dataInfo"][$vv["field"]]["data"] as $intK => $intV) {
											if (!isInteger($intV)) {
												$result["dataInfo"][$vv["field"]]["error"] = "Y";
												$result["hasError"] = "Y";
											}
										}
									}
									break;
								case "float":
									if (!isFloat($result["dataInfo"][$vv["field"]]["data"])) {
										$result["dataInfo"][$vv["field"]]["error"] = "Y";
										$result["hasError"] = "Y";
									}
									break;
								case "floatArray":
									if (count($result["dataInfo"][$vv["field"]]["data"]) > 0) {
										foreach ($result["dataInfo"][$vv["field"]]["data"] as $intK => $intV) {
											if (!isFloat($intV)) {
												$result["dataInfo"][$vv["field"]]["error"] = "Y";
												$result["hasError"] = "Y";
											}
										}
									}
									break;
							}
						}
					} else {
						$result["dataInfo"][$vv["field"]]["data"] = "";
					}
				}
			}
			return $result;
		}
	}
}