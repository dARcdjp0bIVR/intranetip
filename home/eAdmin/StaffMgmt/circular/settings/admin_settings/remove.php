<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libadminjob.php");

intranet_opendb();

$ladminjob = new libadminjob();

if(!empty($AdminID))
{
	$AdminIDStr = implode(",",$AdminID);
	$ladminjob->RemoveAdminUser($AdminIDStr);
	$msg = "delete";
}
else
	$msg = "delete_failed";

intranet_closedb();
header("Location: index.php?msg=$msg");
?>