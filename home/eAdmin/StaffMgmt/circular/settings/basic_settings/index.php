<?php
# using: yat

################# Change Log [Start] ############
# 	Date:	2016-09-23	(Villa)
#			add to module setting - "default using eclass App to notify"
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
#	Date:	2010-11-30	YatWoon
#			change to IP25 standard
#
################# Change Log [End] ############
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircularSettings_BasicSettings";
$lcircular = new libcircular();

# Left menu 
$TAGS_OBJ[] = array($Lang['Circular']['BasicSettings']);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

# build select option object
for($i=1;$i<=30;$i++)
	$data[] = array($i, $i);
$defaultNumDays_selection = getSelectByArray($data, "name='defaultNumDays'", $lcircular->defaultNumDays , 0, 1);	

for($i=3;$i<=100;$i++)
	$data1[] = array($i, $i);
$MaxReplySlipOption_selection = getSelectByArray($data1, "name='MaxReplySlipOption'", $lcircular->MaxReplySlipOption , 0, 1);	

?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}
//-->
</script>


<form name="form1" method="post" action="edit_update.php">
<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$eDiscipline['SettingName']?> </span>-</em>
		<p class="spacer"></p>
	</div>

	<table class="form_table_v30">
	
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['DisableCircular']?></td>
				<td> 
				<span class="Edit_Hide"><?=$lcircular->disabled ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="disabled" value="1" <?=$lcircular->disabled ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="disabled" value="0" <?=$lcircular->disabled ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['AllowHelpSigning']?></td>
				<td> 
				<span class="Edit_Hide"><?=$lcircular->isHelpSignAllow ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="isHelpSignAllow" value="1" <?=$lcircular->isHelpSignAllow ? "checked":"" ?> id="isHelpSignAllow1"> <label for="isHelpSignAllow1"><?=$i_general_yes?></label> 
				<input type="radio" name="isHelpSignAllow" value="0" <?=$lcircular->isHelpSignAllow ? "":"checked" ?> id="isHelpSignAllow0"> <label for="isHelpSignAllow0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['AllowLateSign']?></td>
				<td> 
				<span class="Edit_Hide"><?=$lcircular->isLateSignAllow ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="isLateSignAllow" value="1" <?=$lcircular->isLateSignAllow ? "checked":"" ?> id="isLateSignAllow1"> <label for="isLateSignAllow1"><?=$i_general_yes?></label> 
				<input type="radio" name="isLateSignAllow" value="0" <?=$lcircular->isLateSignAllow ? "":"checked" ?> id="isLateSignAllow0"> <label for="isLateSignAllow0"><?=$i_general_no?></label>
				</span></td>
			</tr>

			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['AllowResign']?></td>
				<td> 
				<span class="Edit_Hide"><?=$lcircular->isResignAllow ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="isResignAllow" value="1" <?=$lcircular->isResignAllow ? "checked":"" ?> id="isResignAllow1"> <label for="isResignAllow1"><?=$i_general_yes?></label> 
				<input type="radio" name="isResignAllow" value="0" <?=$lcircular->isResignAllow ? "":"checked" ?> id="isResignAllow0"> <label for="isResignAllow0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['DefaultDays']?></td>
				<td>
				<span class="Edit_Hide"><?=$lcircular->defaultNumDays?></span>
				<span class="Edit_Show" style="display:none"><?=$defaultNumDays_selection?></span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['TeacherCanViewAll']?></td>
				<td> 
				<span class="Edit_Hide"><?=$lcircular->showAllEnabled ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="showAllEnabled" value="1" <?=$lcircular->showAllEnabled ? "checked":"" ?> id="showAllEnabled1"> <label for="showAllEnabled1"><?=$i_general_yes?></label> 
				<input type="radio" name="showAllEnabled" value="0" <?=$lcircular->showAllEnabled ? "":"checked" ?> id="showAllEnabled0"> <label for="showAllEnabled0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['MaxNumberReplySlipOption']?></td>
				<td>
				<span class="Edit_Hide"><?=$lcircular->MaxReplySlipOption?></span>
				<span class="Edit_Show" style="display:none"><?=$MaxReplySlipOption_selection?></span></td>
			</tr>
<!-- 			villa 23-09-2016  -->
			<?php if($plugin['eClassApp']){ ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eClassApp']['DefaultUserNotifyUsingApp']?></td>
				<td> 
				<span class="Edit_Hide"><?=$lcircular->sendPushMsg ? $Lang['General']['Yes']:$Lang['General']['No']?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" id= "sendPushMsg_yes" name="sendPushMsg" value="1" <?=$lcircular->sendPushMsg ? "checked":"" ?>> <label for="sendPushMsg_yes"><?=$Lang['General']['Yes']?></label> 
				<input type="radio" id= "sendPushMsg_no" name="sendPushMsg" value="0" <?=$lcircular->sendPushMsg ? "":"checked" ?>> <label for="sendPushMsg_no"><?=$Lang['General']['No']?></label>
				</span></td>
			</tr>
			<?php }?>
			
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>











<br />
<table width="96%" border="0" cellspacing="0" cellpadding="5">
<tr> 
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2"><?=$table?></td>
			</tr>
		</table>
	</td>
</tr>
</table>


</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
