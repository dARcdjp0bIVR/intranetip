<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!$special_feature['circular'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircularSettings_BasicSettings";
$lcircular = new libcircular();

$PAGE_NAVIGATION[] = array($button_edit);

# Left menu 
$TAGS_OBJ[] = array($Lang['Circular']['BasicSettings']);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# build select option object
for($i=1;$i<=30;$i++)
	$data[] = array($i, $i);
$defaultNumDays_selection = getSelectByArray($data, "name='defaultNumDays'", $lcircular->defaultNumDays , 0, 1);	
?>

<br />   
<form name="form1" method="get" action="edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['DisableCircular']?></td>
			<td class='tabletext'>
			<input type="radio" name="disabled" value="1" <?=$lcircular->disabled ? "checked":"" ?> id="disabled1"> <label for="disabled1"><?=$i_general_yes?></label> 
			<input type="radio" name="disabled" value="0" <?=$lcircular->disabled ? "":"checked" ?> id="disabled0"> <label for="disabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['AllowHelpSigning']?></td>
			<td class='tabletext'>
			<input type="radio" name="isHelpSignAllow" value="1" <?=$lcircular->isHelpSignAllow ? "checked":"" ?> id="isHelpSignAllow1"> <label for="isHelpSignAllow1"><?=$i_general_yes?></label> 
			<input type="radio" name="isHelpSignAllow" value="0" <?=$lcircular->isHelpSignAllow ? "":"checked" ?> id="isHelpSignAllow0"> <label for="isHelpSignAllow0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['AllowLateSign']?></td>
			<td class='tabletext'>
			<input type="radio" name="isLateSignAllow" value="1" <?=$lcircular->isLateSignAllow ? "checked":"" ?> id="isLateSignAllow1"> <label for="isLateSignAllow1"><?=$i_general_yes?></label> 
			<input type="radio" name="isLateSignAllow" value="0" <?=$lcircular->isLateSignAllow ? "":"checked" ?> id="isLateSignAllow0"> <label for="isLateSignAllow0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['AllowResign']?></td>
			<td class='tabletext'>
			<input type="radio" name="isResignAllow" value="1" <?=$lcircular->isResignAllow ? "checked":"" ?> id="isResignAllow1"> <label for="isResignAllow1"><?=$i_general_yes?></label> 
			<input type="radio" name="isResignAllow" value="0" <?=$lcircular->isResignAllow ? "":"checked" ?> id="isResignAllow0"> <label for="isResignAllow0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['DefaultDays']?></td>
			<td class='tabletext'><?=$defaultNumDays_selection?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['TeacherCanViewAll']?></td>
			<td class='tabletext'>
			<input type="radio" name="showAllEnabled" value="1" <?=$lcircular->showAllEnabled ? "checked":"" ?> id="showAllEnabled1"> <label for="showAllEnabled1"><?=$i_general_yes?></label> 
			<input type="radio" name="showAllEnabled" value="0" <?=$lcircular->showAllEnabled ? "":"checked" ?> id="showAllEnabled0"> <label for="showAllEnabled0"><?=$i_general_no?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_update, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>