<?php
// Editing by 
/*
 * 2019-01-22 (Anna): Added $sys_custom['DHL']
 * 2017-08-22 (Carlos): Created for searching staff users. 
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$search_text = rawurldecode($_POST['SearchText']);
$userlogins = explode("\n",$search_text);

$li = new libdb();
$jsonObj = new JSON_obj();

$userlogin_ary = array();
for($i=0;$i<count($userlogins);$i++){
	$user_login = trim($userlogins[$i]);
	if($user_login != '') $userlogin_ary[] = $li->Get_Safe_Sql_Query($user_login);
}

$conds = "";
if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    
    $PICInfo = $libdhl->getPICResponsInfo();
    $DepartmentIDAry = $PICInfo['Department'];
    $DepartmentUseInfoAry = $libdhl->getDepartmentUserRecords(array('DepartmentID'=>$DepartmentIDAry));
    $DepartmentUseIDAry = Get_Array_By_Key($DepartmentUseInfoAry,'UserID');
    
    $conds = " AND UserID IN ('".implode('\',\'',$DepartmentUseIDAry)."')";
    
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]){
        $conds = "";
    } 
}



$name_field = getNameFieldByLang2("u.");
$sql = "SELECT u.UserID,$name_field as UserName,u.UserLogin FROM INTRANET_USER as u WHERE u.RecordStatus='1' AND u.RecordType='1' AND u.UserLogin IN ('".implode("','",$userlogin_ary)."') $conds ORDER BY UserName ";
$records = $li->returnResultSet($sql);

$json_string = $jsonObj->encode($records); 

echo $json_string;

intranet_closedb();
?>