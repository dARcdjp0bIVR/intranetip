<?php
# using: anna

##########################################
#   Date:   2018-12-27 (Anna)  added access right for $sys_custom['DHL'] PIC
#	Date:	2017-08-22	(Carlos) added js onSearchAndInsertStaff() for searching staff users by userlogins.
#	Date:	2017-04-20	(Carlos) $sys_custom['DHL'] - hide teaching and non-teaching staff target selection types for DHL.
#	Date:	2017-04-07	(Carlos) Fixed send push message time checking.
#	Date:	2017-03-30	(Carlos) Fixed js related to attachments checking.
#	Date:	2017-03-13	(Carlos)
#			Fix the js at submit button.
#
#	Date:	2016-10-19	Bill	[DM#3064]
#			Fixed: cannot highlight left meun
#
#	Date:	2016-09-23	Villa 
#			UI improvement 
#			add block to push msg setting
#	
#	Date:	2015-10-16	Roy
#			modified reset_innerHtml(), checkform(), added clickedSendTimeRadio(), checkedSendPushMessage() for scheduled push message
#			add parameter to return_FormContent()
#
#	Date:	2015-01-22	Roy
#			fix: check $plugin['eClassTeacherApp'] instead of $plugin['eClassApp'] for push message option
#	Date:	2014-10-16	Roy
#			Improved: add notify by push message option
#	Date:	2013-09-10	YatWoon
#			Improved: update the upload attachment method
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the question with "enter" will become "<br>"
#	Date:	2012-06-20 YatWoon
#			Fixed: If edit the reply slip again, the 
#	Date:	2012-05-04 YatWoon
#			Improve: skip the start date checking [Case#2012-0504-1030-05066]
#	Date:	2011-08-04	YatWoon
#			HTML-editor issue (need update display description)
#	Date:	2011-02-23	YatWoon
#			Add option "Display question number"
#
##########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || 
    !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]|| ($isPIC && $sys_custom['DHL']))))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// intranet_auth();
// intranet_opendb();

$linterface = new interface_html();
//$CurrentPage = "PageCircular";
$CurrentPage = "PageCircular_Index";
$CurrentPageArr['eAdminCircular'] = 1;
$lcircular = new libcircular($CircularID);

$lform = new libform();

# Check editable
$hasRight = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($isPIC && $sys_custom['DHL']))
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  # 1 - circular type admin
    if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $adminlevel==1 || $lcircular->IssueUserID == $UserID)     # Allow if Full admin or issuer
    {
        if ($lcircular->isEditable())
            $hasRight = true;
    }
}

if (!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# check signed or not
$isIssuedCircular = 0;
$signed = $lcircular->countSignedNumber($CircularID);
$RecordStatus = $lcircular->RecordStatus;
//if ($start <= $now && $lnotice->RecordStatus==DISTRIBUTED_NOTICE_RECORD)
if($signed>0 && $RecordStatus==1)
{
	$isIssuedCircular = 1;
}

# Attachment Folder
/*
if(!session_is_registered("circularAttFolder") || $circularAttFolder==""){
     session_register("circularAttFolder");
}
*/
$circularAttFolder = $lcircular->Attachment;
$li = new libfilesystem();
$path = "$file_path/file/circular/$circularAttFolder";
$displayAttachment = $lcircular->displayAttachmentEdit("file2delete[]", $circularAttFolder);

/*
$li = new libfilesystem();
$path = "$file_path/file/circular/$circularAttFolder";
if(!empty($circularAttFolder))
{
	$tmp_path = $path."tmp";
	$li->folder_remove_recursive($tmp_path);
	$li->folder_new($tmp_path);
	$li->folder_content_copy($path, $tmp_path);
}

$lo = new libfiletable("", $tmp_path, 0, 0, "");
$files = $lo->files;
*/

// 2015-10-16
// 2016-10-18 Villa update the sql to get the Apptype setting in order to set
$sql = "Select NotifyDateTime, SendTimeMode, Apptype from INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION as ianmmr
		inner join INTRANET_APP_NOTIFY_MESSAGE as ianm On ianmmr.NotifyMessageID = ianm.NotifyMessageID
		Where ianmmr.ModuleName = 'eCircular' And ianmmr.ModuleRecordID = $CircularID And ianm.RecordStatus = '1' group by Apptype order by ianm.NotifyMessageID Desc";
		
$resultAry = $lcircular->returnResultSet($sql);
$appType = BuildMultiKeyAssoc($resultAry, "Apptype", array('Apptype'), 1);
if (count($resultAry) > 0) {
	$sendTimeMode = $resultAry[0]["SendTimeMode"];
	$sendTimeString = $resultAry[0]["NotifyDateTime"];
} else {
	$sendTimeMode = "";
	$sendTimeString = "";
}

### Title ###
$TAGS_OBJ[] = array($i_Circular_Circular,"");
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($i_Circular_Edit);

$linterface->LAYOUT_START();

/*
$type_selection = $recipientNames[0];
if ($recipientNames[1]=="")
{
    $nextSelect = "";
}
else
{
    switch($type)
    {
           case 4: $cellTitle = $i_Circular_RecipientTypeIndividual; break;
    }
    $nextSelect = "<tr><td valign='top' nowrap='nowrap' class='formfieldtitle tabletext'>$cellTitle</td><td class='tabletext' colspan='2'>".$recipientNames[1]."</td></tr>\n";
}
*/

$start = $lcircular->DateStart;
$end = $lcircular->DateEnd;
$Title = $lcircular->Title;
$CircularNumber = $lcircular->CircularNumber;
$Description = $lcircular->Description;
$Question = $lcircular->Question;
$Question = str_replace("<br>","\r\n",$Question);
$Question = intranet_htmlspecialchars($Question);
$type = $lcircular->RecordType;
$thisAudience = $lcircular->returnRecipientNames();
$RecipientID = $lcircular->RecipientID;
$AllFieldsReq = $lcircular->AllFieldsReq;
$RecordStatus = $lcircular->RecordStatus;
$DisplayQuestionNumber = $lcircular->DisplayQuestionNumber;

$type_selection = "<SELECT name='type' onChange='selectAudience()'>";
$type_selection.= "<OPTION value='' > -- $button_select -- </OPTION>";
$type_selection.= "<OPTION value='1' ". ($type==1? "selected":"").">$i_Circular_RecipientTypeAllStaff</OPTION>";
if(!$sys_custom['DHL'])
{
$type_selection.= "<OPTION value='2' ". ($type==2? "selected":"").">$i_Circular_RecipientTypeAllTeaching</OPTION>";
$type_selection.= "<OPTION value='3' ". ($type==3? "selected":"").">$i_Circular_RecipientTypeAllNonTeaching</OPTION>";
}
$type_selection.= "<OPTION value='4' ". ($type==4? "selected":"").">$i_Circular_RecipientTypeIndividual</OPTION>";
$type_selection.= "</SELECT>";

#check if there is a schedule will be sending later	26-9-2016 Villa
$sendTimeMode=$resultAry[0]['SendTimeMode'];
$passScheduled;
$nowTime = strtotime("now");
if ($sendTimeMode == "scheduled" && $sendTimeString != "") {
	$sendTime = strtotime($sendTimeString);
	if ($sendTime > $nowTime) {
		$passScheduled = true;
	} else {
		$sendTimeString = "";
		$passScheduled=false;
	}
}


echo $linterface->Include_AutoComplete_JS_CSS();
echo $linterface->Include_JS_CSS();
?>

<script language="javascript">
$(document).ready( function() {
	// 26-9-2016 Villa hide the setting div 
	if('<?=$plugin['eClassTeacherApp']?>'=='1'){
		$('input#sendNewSelect').hide();
		$('label#sendNewSelect').hide();
		if('<?php echo $passScheduled?>' != '1'){
			$('div#PushMessageSetting').hide();
		}else{
		
			if('<?=$appType['T']?>'=='T'){
				$('#pushmessagenotify').attr("checked","checked");
			}
			$('div#PushMessageSetting').show();
			$('#sendTimeSettingDiv').show();
		
		}
	}
});




function compareDate(s1,s2)
{
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}

function checkform_old(obj){
     checkOption(obj.elements['Attachment[]']);
     if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) {alert ("<?php echo $i_Circular_Alert_DateInvalid; ?>"); return false;}
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Circular_Title; ?>.")) return false;
     if(!check_text(obj.CircularNumber, "<?php echo $i_alert_pleasefillin.$i_Circular_CircularNumber; ?>.")) return false;
     if (!check_date(obj.DateStart,"<?php echo $i_invalid_date; ?>")) return false;
     if (!check_date(obj.DateEnd,"<?php echo $i_invalid_date; ?>")) return false;

     checkOptionAll(obj.elements["Attachment[]"]);
     
     obj.action = 'edit_update.php';
     obj.method = 'POST';
}

function js_show_email_notification(val)
{
	if(val == 1) {
		$('#pushmessagenotify').removeAttr('disabled');
		$('#emailnotify').removeAttr('disabled');
		
	}else{
		$('#pushmessagenotify').removeAttr('checked');
		$('#emailnotify').removeAttr('checked');
		$('#pushmessagenotify').attr('disabled','true');
		$('#emailnotify').attr('disabled','true');
		$('div#PushMessageSetting').hide();
	}
}

function selectAudience()
{
	var tid = document.form1.type.value;
	document.getElementById('div_audience_err_msg').innerHTML = "";
	
	$('#div_audience').load(
		'ajax_loadAudience.php', 
		{type: tid, RecipientID: '<?=$RecipientID?>', CircularID: '<?=$CircularID?>'}, 
		function (data){
			if(tid == 4){
				// initialize jQuery Auto Complete plugin
				Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
				$('input#UserClassNameClassNumberSearchTb').focus();
			}
		});
}

function reset_innerHtml()
{
	<? if(!$isIssuedCircular) {?>
		document.getElementById('div_audience_err_msg').innerHTML = "";
		document.getElementById('div_DateStart_err_msg').innerHTML = "";
	<? } ?>
	
 	document.getElementById('div_CircularNumber_err_msg').innerHTML = "";
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	<?php if($plugin['eClassTeacherApp']) { ?>
 		document.getElementById('div_push_message_date_err_msg').innerHTML = "";
 	<?php } ?>
}

function Big5FileUploadHandler() {
	 var new_attach = parseInt(document.form1.attachment_size.value);
	 for(var i=0;i<new_attach;i++){
	 	var objFile = eval('document.form1.filea'+i);
	 	var objUserFile = eval('document.form1.hidden_userfile_name'+i);
	 	if(typeof(objFile)!='undefined' && objFile.value!='' && typeof(objUserFile)!='undefined'){
		 	Ary = objFile.value.split('\\');
		 	if(Ary!=null){
			 	objUserFile.value = Ary[Ary.length-1];
			}
		}
	 }
	 return true;
}

function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	<? if(!$isIssuedCircular) {?>
	if(obj.type.value==4) 
    {
		checkOptionAll(obj.elements["target[]"]);
	    
		if(obj.elements["target[]"].length==0)
		{ 
			document.getElementById('div_audience_err_msg').innerHTML = '<font color="red"><?=$i_frontpage_campusmail_choose?></font>';
			error_no++;
			focus_field = "type";
		}
	}
	
	 if(!check_text_30(obj.CircularNumber, "<?php echo $i_alert_pleasefillin.$i_Circular_CircularNumber; ?>.", "div_CircularNumber_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "CircularNumber";
	}
	
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$i_Circular_Title; ?>.", "div_Title_err_msg"))
	 {
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	 }
	 
	 <? /* ?>
	  if (compareDate('<?=date('Y-m-d')?>', obj.DateStart.value) > 0)
		{
			document.getElementById('div_DateStart_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_startdate_wrong?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "DateStart";
		}
		<? */ ?>
		
		if (compareDate(obj.DateStart.value, obj.DateEnd.value) > 0) 
		{
			document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "DateEnd";
		}
		
     <? } else {?>
		if (compareDate("<?=$lcircular->DateStart?>", obj.DateEnd.value) > 0) 
		{
			document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_Homework_new_duedate_wrong?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "DateEnd";
		}
	<? } ?>
	if($('#status1').attr('checked') && $('#pushmessagenotify').attr('checked'))
	{
		var scheduleString = '';
	    if ($('input#sendTimeRadio_scheduled').attr('checked')) {
	    	scheduleString = $('input#sendTime_date').val() + ' ' + str_pad($('select#sendTime_hour').val(),2) + ':' + str_pad($('select#sendTime_minute').val(),2) + ':00';
	    }
	    $('input#sendTimeString').val(scheduleString);
	    
	    var minuteValid = false;
		if (str_pad($('select#sendTime_minute').val(),2) == '00' || str_pad($('select#sendTime_minute').val(),2) == '15' || str_pad($('select#sendTime_minute').val(),2) == '30' || str_pad($('select#sendTime_minute').val(),2) == '45') {
			minuteValid = true;
		}
		
		// if scheduled message => check time is past or not
		var dateNow = new Date();
		var dateNowString = dateNow.getFullYear() + '-' + str_pad((dateNow.getMonth()+1),2) + '-' + str_pad(dateNow.getDate(),2) + ' ' + str_pad(dateNow.getHours(),2) + ':' + str_pad(dateNow.getMinutes(),2) + ':' + str_pad(dateNow.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString <= dateNowString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?>');
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['SendTimeInvalid']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "sendTime_date";
		}
		
		// check time is within 30 days
		var maxDate = new Date();
		maxDate.setDate(dateNow.getDate() + 31);
		var maxDateString = maxDate.getFullYear() + '-' + str_pad((maxDate.getMonth()+1),2) + '-' + str_pad(maxDate.getDate(),2) + ' ' + str_pad(maxDate.getHours(),2) + ':' + str_pad(maxDate.getMinutes(),2) + ':' + str_pad(maxDate.getSeconds(),2);
		if ($('input#sendTimeRadio_scheduled').attr('checked') && minuteValid && scheduleString > maxDateString) {
			// alert('<?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?>');
			$('input#sendTime_date').focus();
			// return false;
			document.getElementById('div_push_message_date_err_msg').innerHTML = '<font color="red"><?=$Lang['AppNotifyMessage']['WarningMsg']['PleaseSelectDateWithinRange']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "sendTime_date";
		}
	}
	 
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		var go_on = 1;
		// check status changed and display warning (changed from Distrubute only)
		if(obj.original_status.value==1 && !obj.status[0].checked)
		{
			if(!confirm("<?=$Lang['Circular']['UpdateStatusWarning']?>"))
				go_on = 0;
		}
		
		if(go_on)
		{
			//checkOption(obj.elements['Attachment[]']);
			//checkOptionAll(obj.elements["Attachment[]"]);
			//obj.action = 'edit_update.php';
			//obj.method = 'POST';
			//return true;
			
			Big5FileUploadHandler();
			
			// create a list of files to be deleted
	        var objDelFiles = document.getElementsByName('file2delete[]');
	        var files = obj.deleted_files;
	        if(objDelFiles!=null)
	        {
		        var x="";
		        for(var i=0;i<objDelFiles.length;i++){
			        if(objDelFiles[i].checked==true){
				 		x+=objDelFiles[i].value+":";       
				    }
			    }
			    files.value = x.substr(0,x.length-1);
		    }
		    return true;
		}
		return false;
	}
     
}

var no_of_upload_file = <? echo $no_file==""?1:$no_file;?>;
function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	var cell = row.insertCell(0);
	x= '<input class="file" type="file" name="filea'+no_of_upload_file+'" size="40">';
	x+='<input type="hidden" name="hidden_userfile_name'+no_of_upload_file+'">';
	cell.innerHTML = x;
	no_of_upload_file++;
	document.form1.attachment_size.value = no_of_upload_file;
}

function setRemoveFile(index,v){
	/*
	if(index==undefined)
	{
		alert(no_of_upload_file);
	}
	*/
	
	obj = document.getElementById('a_'+index);
	obj.style.textDecoration = v?"line-through":"";
}
if('<?=$plugin['eClassTeacherApp']?>'=='1'){
	function clickedSendTimeRadio(targetType) {
		if (targetType == 'now') {
			$('div#specificSendTimeDiv').hide();
		}
		else {
			$('div#specificSendTimeDiv').show();
		}
	}
	
	function checkedSendPushMessage(checkBox) {
		if (checkBox.checked == true) {
			$('div#sendTimeSettingDiv').show();
			$('div#PushMessageSetting').show();
		} else {
			$('div#sendTimeSettingDiv').hide();
			$('div#PushMessageSetting').hide();
		}
	}
}
function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
	
	AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
		"ajax_search_user.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
			},
			formatItem: function(row) {
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName, InputID) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('target[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
	
	// reset and refocus the textbox
	$('input#' + InputID).val('').focus();
}

function Update_Auto_Complete_Extra_Para(){
	checkOptionAll(document.getElementById('form1').elements["target[]"]);
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
	AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Remove_Selected_Student(){
	
	checkOptionRemove(document.getElementById('target[]'));
	Update_Auto_Complete_Extra_Para();
}

function onSearchAndInsertStaff(btnObj,textObj,targetObj)
{
	var loading_image = '<?=$linterface->Get_Ajax_Loading_Image(1)?>';
	var error_layer = $('#'+textObj.id+'_Error');
	var search_text = $.trim(textObj.value);
	error_layer.html('');
	if(search_text == ''){
		error_layer.html('<?=$Lang['AppNotifyMessage']['WarningRequestInputSearchLoginID']?>');
		return false;
	}
	
	error_layer.html(loading_image);
	btnObj.disabled = true;
	$.post(
		'ajax_batch_search_staff.php',
		{
			"SearchText":encodeURIComponent(search_text)
		},
		function(returnJson){
			var ary = [];
			if( JSON && JSON.parse){
				ary = JSON.parse(returnJson);
			}else{
				eval('ary='+returnJson);
			}
			var UserSelected = targetObj;
			var inputted_target_values = search_text.split("\n");
			var selected_target_values = Get_Selection_Value(targetObj.id,'Array',true);
			var searched_userlogins = [];
			var not_found_values = [];
			for(var i=0;i<ary.length;i++){
				var id_value = 'U'+ary[i]['UserID'];
				searched_userlogins.push(ary[i]['UserLogin']);
				if(selected_target_values.indexOf(id_value)==-1)
				{
					UserSelected.options[UserSelected.length] = new Option(ary[i]['UserName'], id_value);
					selected_target_values.push(id_value);
				}
			}
			for(var i=0;i<inputted_target_values.length;i++){
				if($.trim(inputted_target_values[i])!='' && searched_userlogins.indexOf(inputted_target_values[i])==-1){
					not_found_values.push(inputted_target_values[i]);
				}
			}
			js_Select_All(targetObj.id);
			if(not_found_values.length>0){
				error_layer.html('<?=$Lang['AppNotifyMessage']['WarningFollowingUsersCannotBeFound']?><br />'+not_found_values.join("<br />"));
			}else{
				error_layer.html('');
			}
			btnObj.disabled = false;
		}
	);
}
</script>

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" id="form1" method="post" action="edit_update.php" enctype="multipart/form-data" >

<table class="form_table_v30">
<tr valign='top'>
	<td nowrap class='field_title'><? if(!$isIssuedNotice) {?><span class='tabletextrequire'>*</span><?}?><?=$i_Circular_RecipientType?></td>
	<td>
	<? if($isIssuedCircular) {?>
		<?=$thisAudience['0']?><br><?=Get_String_Display($thisAudience['1'],0,'')?>
	<? } else {?>
		<?=$type_selection?><br><span id="div_audience_err_msg"></span>
		<div id="div_audience"></div>
	<? } ?>
	</td>
</tr>

</table>


<div id="div_content">

	<?= $lcircular->return_FormContent($start, $end, $isIssuedCircular, $Title, $Description, $Question, $CircularNumber, $AllFieldsReq, $RecordStatus, $circularAttFolder, $DisplayQuestionNumber, ($plugin['eClassTeacherApp'] ? 1 : 0), $sendTimeMode, $sendTimeString);?>
	
</div>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
	<?
	$save_btn_name = $isIssuedCircular ? $button_save : "$button_save / $button_send";
	echo $linterface->GET_ACTION_BTN("$save_btn_name", "submit", "return checkform(document.form1);","submit2");
	?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn") ?>
</div>	


<input type="hidden" name="CircularID" value="<?=$CircularID?>">
<input type="hidden" name="original_status" value="<?=$lcircular->RecordStatus?>">

<input type="hidden" name="deleted_files" value='' />
<input type="hidden" name='attachment_size' value='<? echo $no_file==""?1:$no_file;?>'>

</form>


<script language="JavaScript1.2">

<? if(!$isIssuedCircular) {?>
selectAudience();
<? } ?>
<? /* ?>
obj = document.form1.elements["Attachment[]"];
checkOptionClear(obj);
<?php
while (list($key, $value) = each($files))
	echo "checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n";
?>
checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
<? */ ?>
</script>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>