<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$special_feature['circular'] || $_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"])
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
intranet_auth();
intranet_opendb();


$target_filename = stripslashes($target);
$lc = new libcircular($targetID);
$path = "$file_path/file/circular/".$lc->Attachment;
$a = new libfiletable("", $path, 0, 0, "");
$files = $a->files;
$valid = false;
while (list($key, $value) = each($files))
{
       if ($files[$key][0]==$target_filename)
       {
           $valid = true;
       }
}

if ($valid)
{
    $target_filepath = $path."/".$target_filename;
    $content = get_file_content($target_filepath);
    $encoded_name = urlencode($target_filename);
                $target_filename = utf8Encode2($target_filename); # convert octets to represented chars in UTF-8, see above utf8Encode2()
                 output2browser($content, $target_filename);

}
else
{
    echo "You don't have privileges or the file has been corrupted.";
}

intranet_closedb();


# functions for handling filename parameter for "Content-Disposition" in header() function.

# conver utf-8 octets (eg. &#12345) to the represented chars in UTF-8
function octets2char( $input ) {
        if(!function_exists("octets2char_callback")){
         function octets2char_callback($octets) {
                   $dec = $octets[1];
                           if ($dec < 128) {
                             $utf8Substring = chr($dec);
                           } else if ($dec < 2048) {
                             $utf8Substring = chr(192 + (($dec - ($dec % 64)) / 64));
                             $utf8Substring .= chr(128 + ($dec % 64));
                           } else {
                             $utf8Substring = chr(224 + (($dec - ($dec % 4096)) / 4096));
                             $utf8Substring .= chr(128 + ((($dec % 4096) - ($dec % 64)) / 64));
                             $utf8Substring .= chr(128 + ($dec % 64));
                           }
                           return $utf8Substring;
        }
    }
  return preg_replace_callback('|&#([0-9]{1,});|', 'octets2char_callback', $input );

}

# convert the string with octets to represted chars in UTF-8 form
function utf8Encode2($source) {
   $utf8Str = '';
   $entityArray = explode ("&#", $source);
   $size = count ($entityArray);
   for ($i = 0; $i < $size; $i++) {
       $subStr = $entityArray[$i];
       $nonEntity = strpos ($subStr, ';');
       if ($nonEntity !== false) {
                        $str = substr($subStr,0,$nonEntity);
                        $str = "&#".$str.";";
                        $str_end=substr($subStr,$nonEntity+1);

                        $convertedStr=rawurlencode(octets2char($str));
                        //$convertedStr=octets2char($str);

                        $utf8Str.=$convertedStr.$str_end;
       }
       else {
           $utf8Str .= $subStr;
       }
   }
   //return preg_replace('/[:\\x5c\\/*?"<>|]/', '_', $utf8Str);
   return $utf8Str;

}

# end functions

?>
