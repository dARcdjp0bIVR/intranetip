<?php
# using: 

############ Change Log Start ###############
#
#	Date:	 2017-09-26	Bill	[2017-0915-1733-43235]
#	Details: Support Issuer filter for admin
#
#	Date:	 2017-03-08	Bill	[2017-0209-1034-55236]
#	Details: Allow admin to view all result
#
#	Date:	 2017-02-10	Bill	[2017-0209-1034-55236]
#	Details: Only include distributed circular in report
#
#	Date:	 2011-08-23	Henry
#	Details: File Created
#
############ Change Log End ###############

### Set Cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_circular_admin_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_circular_admin_page_number", $pageNo, 0, "", "", 0);
	$ck_circular_admin_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_circular_admin_page_number!="")
{
	$pageNo = $ck_circular_admin_page_number;
}
if ($ck_circular_admin_page_order!=$order && $order!="")
{
	setcookie("ck_circular_admin_page_order", $order, 1, "", "", 1);
	$ck_circular_admin_page_order = $order;
}
else if (!isset($order) && $ck_circular_admin_page_order!="")
{
	$order = $ck_circular_admin_page_order;
}
if ($ck_circular_admin_page_field!=$field && $field!="")
{
	setcookie("ck_circular_admin_page_field", $field, 0, "", "", 0);
	$ck_circular_admin_page_field = $field;
}
else if (!isset($field) && $ck_circular_admin_page_field!="")
{
	$field = $ck_circular_admin_page_field;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");



include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}


if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]|| ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircular_UnsignedCircularList";

$lcircular = new libcircular();
$linterface = new interface_html();

// Get Admin Level
$adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] ? $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] : 0;
$adminlevel = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"]? 1 : $adminlevel;

$namefield = getNameFieldByLang("c.");

## Unsigned Staff SQL Cond
$cond = "";
if($StaffSelect != "") {
	//debug_pr($StaffSelect);
	//$query = explode( ',',urldecode($StaffSelect));
	//debug_pr($query);
	$cond = " AND b.UserID = '".$StaffSelect."' " ;
	//AND BINARY IF(c.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$namefield) = '".$query[1]."' 
}

## [2017-0915-1733-43235] Issuer SQL Cond
$cond2 = "";
if($adminlevel && $IssuerSelect != "") {
	$cond2 .= " AND a.IssueUserID = '".$IssuerSelect."' ";
}

//# staff filter
//$StaffSelectionBox = '';
//
//$sql1 = "SELECT
//			b.UserID as UserID,
//			IF(c.UserID IS NULL, CONCAT('<I>',b.UserName,'</I>'), $namefield) as UserName1
//		 FROM
//			INTRANET_CIRCULAR as a 
//			LEFT OUTER JOIN INTRANET_CIRCULAR_REPLY as b ON (a.CircularID = b.CircularID)
//			LEFT OUTER JOIN INTRANET_USER as c ON (b.UserID = c.UserID)
//		 WHERE
//			b.RecordStatus <> 2 AND c.UserID IS NOT NULL AND (a.IssueUserID = $UserID OR $adminlevel) AND a.RecordStatus = 1";
////debug_pr($sql1);
//$distributorInfo = $lcircular->returnArray($sql1);
//$numOfDistributorInfo = count($distributorInfo);
//
//$StaffSelectionOption = array();
//for($i=0;$i<$numOfDistributorInfo;$i++){
//	if($distributorInfo[$i]['UserName1'] != '')
//		$StaffSelectionOption[]= array($distributorInfo[$i]['UserID'], $distributorInfo[$i]['UserName1']);
//}
//$StaffSelectionBox = $linterface->GET_SELECTION_BOX($StaffSelectionOption, 'name="StaffSelect" id="StaffSelect" onchange="document.form1.submit()"', "-- {$Lang['StaffAttendance']['AllStaff']} --", $StaffSelect);
//$filterbar .= $StaffSelectionBox;

############################################################################################################

$iseCircularAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]==1) ? 1 : 0;
$tempField = "ORDER BY ";
if($field == 0) {
	$tempField .= "UserName1";
}
else if($field == 1) {
	$tempField .= "a.Title";
}
else if($field == 2) {
	$tempField .= "a.DateStart";
}
else if($field == 3) {
	$tempField .= "a.DateEnd";
}
else {
	$tempField = "";
}
$tempOrder = "";
if($order == 0 && $tempField != "") {
	$tempOrder = " DESC";
}

	$sql .= "SELECT
				IF(c.UserID IS NULL,CONCAT('<I>', b.UserName,'</I>'), $namefield) as UserName1,
				a.Title,
				a.DateStart,
				a.DateEnd 
			 FROM
				INTRANET_CIRCULAR as a 
				LEFT OUTER JOIN INTRANET_CIRCULAR_REPLY as b ON (a.CircularID = b.CircularID) 
				LEFT OUTER JOIN INTRANET_USER as c ON (b.UserID = c.UserID) 
			 WHERE 
				b.RecordStatus <> 2 AND c.UserID IS NOT NULL AND (a.IssueUserID = $UserID OR $adminlevel) AND a.RecordStatus = 1 {$cond} {$cond2}
				".$tempField.$tempOrder;
	
	$lb = new libdb();
	$temp = $lb->returnArray($sql,6);
    
    $display= "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" align=\"center\" class=\"eSporttableborder\">";
	$display.="<tr>";
	$display.= "<td width=\"1\" class=\"eSporttdborder eSportprinttabletitle\">#</td>\n";
	//$display.= "<td width=\"30%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_UserName."</td>\n";
	//$display.= "<td width=\"40%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Circular_Title."</td>\n";
	$display.= "<td width=\"30%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['Circular']['UnsignedCircularStaffName']."</td>\n";
	$display.= "<td width=\"40%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang['Circular']['UnsignedCircularTitle']."</td>\n";
	$display.= "<td width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Circular_DateStart."</td>\n";
	$display.= "<td width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\">".$i_Circular_DateEnd."</td>\n";

	for($i=0;$i<sizeof($temp);$i++) {
		list($username, $lesson, $classname, $classnum) = $temp[$i];
		$css = $i%2==0?"tableContent":"tableContent2";
		$row = "<tr>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">".($i+1)."</td>";		
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$username</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$lesson&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classname&nbsp;</td>";
		$row.= "<td class=\"eSporttdborder eSportprinttext\">$classnum&nbsp;</td>";		
		$row.="</tr>";
		$display.=$row;
	}
	if(sizeof($temp)<=0) {
		$row = "<tr><td class=\"eSporttdborder eSportprinttext\" height=\"40\" colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td></tr>";
		$display.=$row;
	}
	$display.="</table>";
	//debug_pr($sql);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
?>

<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttext"><b><?=$Lang['Circular']['UnsignedCircularList']?> ( <?=($StaffSelect == ''?$Lang['StaffAttendance']['AllStaff']:$temp[0]['UserName1'])?> )</b></td>
	</tr>
</table>

<?
echo $display;

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>