<?php
# using: 

#################################
#	Date:	2017-04-25	Carlos
#			$sys_custom['DHL'] - Hide Identity selection for DHL.
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]  || ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$MODULE_OBJ['title'] = $i_Circular_ViewStat;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lform = new libform();
$lcircular = new libcircular($CircularID);
if ($lcircular->disabled)
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$hasRight = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($isPIC && $sys_custom['DHL']))
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])     # Allow if Full admin or issuer
    {
        $hasRight = true;
    }
    
    if($isPIC && $sys_custom['DHL']){
        $hasRight = true;
    }
}

if (!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$data = $lcircular->returnAllResult($TeachingType);
if ($lcircular->Question == "")
{
    $x .= "No options to be filled in this circular";
    $viewNeeded = false;
}
else if (sizeof($data)==0)
{
    $x .= "$i_Circular_NoReplyAtThisMoment";
    $viewNeeded = false;
}
else
{
    $x = $lform->returnAnswerInJS($data);
    $viewNeeded = true;
}

# Staff Type 
$staffTypeMenu = "<select name=\"TeachingType\" id=\"TeachingType\" onChange=\"reloadForm()\">";
$staffTypeMenu .= "<option value=''".((!isset($TeachingType) || $TeachingType=="") ? " selected" : "").">{$i_Circular_RecipientTypeAllStaff}</option>";
$staffTypeMenu .= "<option value='1'".(($TeachingType=='1') ? " selected" : "").">{$Lang['Identity']['TeachingStaff']}</option>";
$staffTypeMenu .= "<option value='0'".(($TeachingType=='0') ? " selected" : "").">{$Lang['Identity']['NonTeachingStaff']}</option>";
$staffTypeMenu .= "</select>";
?>

<script language="javascript">
<!--
function reloadForm() 
{
	document.form1.action = "";
	document.form1.submit();
}
//-->
</script>

<form name="print_form" action="stat_print_preview.php" method="post" target="_blank">
	<input type="hidden" name="CircularID" value="<?=$CircularID?>">
	<input type="hidden" name="all" value="<?=$all?>">
</form>
<br />

<form name="form1" action="" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr> 
					<td align="right" colspan="2"> 
						<a href="tableview.php?CircularID=<?=$CircularID?>&TeachingType=<?=$TeachingType?>" class="tablelink"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/icon_viewreply.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Circular_TableViewReply?></a>
					</td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Circular_CircularNumber?></span></td>
					<td class='tabletext'><?=$lcircular->CircularNumber?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Circular_Title?></span></td>
					<td class='tabletext'><?=$lcircular->Title?></td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr valign='top'>
					<td class='tabletext' colspan="2">&nbsp;</td>
				</tr>
				<?php if(!$sys_custom['DHL']){ ?>
				<tr><td><?=$staffTypeMenu?></td></tr>
				<?php } ?>
				<tr valign='top'>
					<td colspan="2" align="center" class="tabletext">
					<? if ($viewNeeded) { ?>
						<script language="javascript" src="/templates/forms/form_view.js"></script>
						<form name="ansForm" method="post" action="update.php">
							<input type=hidden name="qStr" value="">
							<input type=hidden name="aStr" value="">
						</form>
						<script language="Javascript">
						<?=returnFormStatWords()?>
						var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
						w_total_no = "<?=$i_Circular_NumberOfSigned?>";
						var myQue = "<?=$lform->getConvertedString($lcircular->Question)?>";
						var myAns = new Array();
						w_show_details = "<?=$i_Form_ShowDetails?>";
						w_hide_details = "<?=$i_Form_HideDetails?>";
						w_view_details_img = "<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_view.gif' border='0'>";
					<? } ?>
					<?=$x?>
					<? if ($viewNeeded) { ?>
						var stats= new Statistics();
						stats.qString = myQue;
						stats.answer = myAns;
						stats.analysisData();
						document.write(stats.getStats());
					</script>
					<? } ?>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<? if($viewNeeded) { ?>
						<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "document.print_form.submit();") ?>
						<? } ?>
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="CircularID" value="<?=$CircularID?>">
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>