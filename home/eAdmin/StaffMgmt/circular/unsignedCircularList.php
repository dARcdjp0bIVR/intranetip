<?php
# using: 

############ Change Log Start ###############
#
#	Date:	 2017-09-26	Bill	[2017-0915-1733-43235]
#	Details: add Issuer filter for admin
#
#	Date:	 2017-05-31	Bill	[2017-0529-1652-27206]
#	Details: set cookies after include global.php, PHP 5.4 problem
#
#	Date:	 2017-03-08	Bill	[2017-0209-1034-55236]
#	Details: Allow admin to view all result
#
#	Date:	 2017-02-10	Bill	[2017-0209-1034-55236]
#	Details: Only include distributed circular in report
#
#	Date:	 2011-08-23	Henry
#	Details: File Created
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

### Set Cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_circular_admin_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_circular_admin_page_number", $pageNo, 0, "", "", 0);
	$ck_circular_admin_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_circular_admin_page_number!="")
{
	$pageNo = $ck_circular_admin_page_number;
}
if ($ck_circular_admin_page_order!=$order && $order!="")
{
	setcookie("ck_circular_admin_page_order", $order, 1, "", "", 1);
	$ck_circular_admin_page_order = $order;
}
else if (!isset($order) && $ck_circular_admin_page_order!="")
{
	$order = $ck_circular_admin_page_order;
}
if ($ck_circular_admin_page_field!=$field && $field!="")
{
	setcookie("ck_circular_admin_page_field", $field, 0, "", "", 0);
	$ck_circular_admin_page_field = $field;
}
else if (!isset($field) && $ck_circular_admin_page_field!="")
{
	$field = $ck_circular_admin_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}



if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]|| ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$CurrentPageArr['eAdminCircular'] = 1;
$CurrentPage = "PageCircular_UnsignedCircularList";

$lcircular = new libcircular();
$linterface = new interface_html();

// Get Admin Level
$adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] ? $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] : 0;
$adminlevel = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"]? 1 : $adminlevel;

$namefield = getNameFieldByLang("c.");

## Unsigned Staff SQL Cond
$cond = "";
if($StaffSelect != "") {
	//debug_pr($StaffSelect);
	//$query = explode( ',',urldecode($StaffSelect));
	//debug_pr($query);
	$cond .= " AND b.UserID = '".$StaffSelect."' " ;
	//AND BINARY IF(c.UserID IS NULL,CONCAT('<I>',b.UserName,'</I>'),$namefield) = '".$query[1]."' 
}

## [2017-0915-1733-43235] Issuer SQL Cond
$cond2 = "";
if($adminlevel && $IssuerSelect != "") {
	$cond2 .= " AND a.IssueUserID = '".$IssuerSelect."' ";
}

# Staff filter
// Get Staff with unsigned circular
$StaffSelectionBox = "";
$sql1 = "SELECT
			DISTINCT b.UserID as UserID,
			BINARY IF(c.UserID IS NULL, CONCAT('<I>',b.UserName,'</I>'), $namefield) as UserName1
		 FROM 
			INTRANET_CIRCULAR_REPLY as b 
			LEFT OUTER JOIN INTRANET_USER as c ON (b.UserID = c.UserID) 
			LEFT OUTER JOIN INTRANET_CIRCULAR as a ON (a.CircularID = b.CircularID)
		 WHERE 
			b.RecordStatus <> 2 AND c.UserID IS NOT NULL AND (a.IssueUserID = $UserID OR $adminlevel) AND a.RecordStatus = 1 {$cond2}
		 ORDER BY 
			c.EnglishName";
$distributorInfo = $lcircular->returnArray($sql1);
$numOfDistributorInfo = count($distributorInfo);

// Build Staff filter
$StaffSelectionOption = array();
for($i=0; $i<$numOfDistributorInfo; $i++){
	if($distributorInfo[$i]["UserName1"] != "") {
		$StaffSelectionOption[] = array($distributorInfo[$i]["UserID"], $distributorInfo[$i]["UserName1"]);
	}
}
$StaffSelectionBox = $linterface->GET_SELECTION_BOX($StaffSelectionOption, 'name="StaffSelect" id="StaffSelect" onchange="document.form1.submit()"', "-- {$Lang['Circular']['AllUnsignedStaff']} --", $StaffSelect);
$filterbar .= $StaffSelectionBox;

# [2017-0915-1733-43235] Issuer filter (Admin only)
$IssuerSelectionBox = "";
if($adminlevel)
{
	// Get Issuer with circular still unsigned
	$sql1 = "SELECT
				DISTINCT a.IssueUserID as UserID,
				BINARY IF(c.UserID IS NULL, CONCAT('<I>',a.IssueUserName,'</I>'), $namefield) as UserName1
			 FROM 
				INTRANET_CIRCULAR_REPLY as b 
				LEFT OUTER JOIN INTRANET_CIRCULAR as a ON (a.CircularID = b.CircularID)
				LEFT OUTER JOIN INTRANET_USER as c ON (a.IssueUserID = c.UserID) 
			 WHERE 
				b.RecordStatus <> 2 AND c.UserID IS NOT NULL AND a.RecordStatus = 1 {$cond}
			 ORDER BY 
				c.EnglishName";
	$IssuerInfo = $lcircular->returnArray($sql1);
	$numOfIssuer = count($IssuerInfo);
	
	// Build Issuer filter
	$IssuerSelectionOption = array();
	for($i=0; $i<$numOfIssuer; $i++) {
		if($IssuerInfo[$i]["UserName1"] != "") {
			$IssuerSelectionOption[] = array($IssuerInfo[$i]["UserID"], $IssuerInfo[$i]["UserName1"]);
		}
	}
	$IssuerSelectionBox = $linterface->GET_SELECTION_BOX($IssuerSelectionOption, 'name="IssuerSelect" id="IssuerSelect" onchange="document.form1.submit()"', "-- {$Lang['Circular']['AllIssuerStaff']} --", $IssuerSelect);
}
$filterbar = $IssuerSelectionBox.$filterbar;

############################################################################################################

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if (!isset($field)) $field = 0;
if (!isset($order)) $order = 0;

$li = new libdbtable2007($field, $order, $pageNo);

$iseCircularAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"]==1) ? 1 : 0;

	$sql .= "SELECT
				IF(c.UserID IS NULL, CONCAT('<I>',b.UserName,'</I>'), $namefield) as UserName1,
				CONCAT('<a href=javascript:viewCircular(', a.CircularID, ') class=\"tablelink\">', a.Title, '</a>'),
				a.DateStart,
				a.DateEnd 
			 FROM
				INTRANET_CIRCULAR_REPLY as b 
				LEFT OUTER JOIN INTRANET_USER as c ON (b.UserID = c.UserID) 
				LEFT OUTER JOIN INTRANET_CIRCULAR as a ON (a.CircularID = b.CircularID)
			 WHERE 
				b.RecordStatus <> 2 AND c.UserID IS NOT NULL AND (a.IssueUserID = $UserID OR $adminlevel) AND a.RecordStatus = 1 {$cond} {$cond2}";
	//debug_pr($sql);
	$li->sql = $sql;
	$li->field_array = array("UserName1", "a.Title", "a.DateStart", "a.DateEnd");
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = "";
	$li->column_array = array(0,1,0,0);
	$li->wrap_array = array(0,0,0,0);
	$li->IsColOff = "IP25_table";
	
	$pos = 0;
	$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";              
	//$li->column_list .= "<th width='20%'>".$li->column($pos++, $i_UserName)."</th>\n";
	//$li->column_list .= "<th width='60%'>".$li->column($pos++, $i_Circular_Title)."</th>\n";
	$li->column_list .= "<th width='20%'>".$li->column($pos++, $Lang['Circular']['UnsignedCircularStaffName'])."</th>\n";
	$li->column_list .= "<th width='60%'>".$li->column($pos++, $Lang['Circular']['UnsignedCircularTitle'])."</th>\n";
	$li->column_list .= "<th width='10%'>".$li->column($pos++, $i_Circular_DateStart)."</th>\n";
	$li->column_list .= "<th width='10%'>". $li->column($pos++, $i_Circular_DateEnd) ."</th>\n";

$TAGS_OBJ[] = array($Lang['Circular']['UnsignedCircularList'],"", 0);
$MODULE_OBJ = $lcircular->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$toolbar = $linterface->GET_LNK_PRINT("javascript:openPrintPage()","","","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:exportPage('unsignedCircularList_export.php')","","","","",0);
?>

<script language="Javascript">
function viewCircular(id)
{
     //newWindow('/home/eService/circular/view.php?CircularID='+id+'&field='+<?=$ck_circular_admin_page_field?>+'&order='+<?=$order?>,10);
     newWindow('/home/eService/circular/view.php?CircularID='+id+'&showStar='+0,10);
}

function openPrintPage(){
    newWindow("unsignedCircularList_print.php?StaffSelect=<?=$StaffSelect?>&IssuerSelect=<?=$IssuerSelect?>&field=<?=$ck_circular_admin_page_field?>&order=<?=$order?>", 12);
}

function exportPage(newurl){
	old_url = document.form1.action;
	document.form1.action = newurl;
	document.form1.submit();
	document.form1.action = old_url;
}
</script>

<form name="form1" method="get">

<div class="content_top_tool">
	<div class="Conntent_tool">
		<?=$toolbar?>
	</div>
<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom"><?= $filterbar; ?></td>
	<!--<td valign="bottom"><div class="common_table_tool"><a href="javascript:checkRemoveThis(document.form1,'CircularID[]','remove_update.php')" class="tool_delete"><?=$Lang['Btn']['Delete']?></a></div></td>-->
</tr>
</table>
</div>

<?php echo $li->display(); ?>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<br>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();

if($sys_custom['PopupCircularAfterCreated'] && !empty($CircularID))
{
?>
<SCRIPT LANGUAGE=Javascript>
newWindow('/home/eService/circular/view.php?CircularID=<?=$CircularID?>',10);
</script>
<?	
}
?>