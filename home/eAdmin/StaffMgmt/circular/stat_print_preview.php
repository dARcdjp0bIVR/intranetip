<?php

#################################
#	Date:	2011-02-24	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();


if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}


if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]  || ($isPIC && $sys_custom['DHL']))))
{
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
    {
        header("location: ./settings/basic_settings/");
        exit;
    }
    
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$MODULE_OBJ['title'] = $i_Circular_ViewStat;
$linterface = new interface_html();

$lform = new libform();
$lcircular = new libcircular($CircularID);
if ($lcircular->disabled)
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$ladminjob = new libadminjob($UserID);
$hasRight = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($isPIC && $sys_custom['DHL']))
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || ($isPIC && $sys_custom['DHL']))     # Allow if Full admin or issuer
    {
        $hasRight = true;
    }
}

if (!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$data = $lcircular->returnAllResult();
if ($lcircular->Question == "")
{
    $x .= "No options to be filled in this circular";
    $viewNeeded = false;
}
else if (sizeof($data)==0)
{
    $x .= "$i_Circular_NoReplyAtThisMoment";
    $viewNeeded = false;
}
else
{
    $x = $lform->returnAnswerInJS($data);
    $viewNeeded = true;
}
?>
<table width='100%' align='center' class='print_hide' border='0'>
	<tr>
		<td align='right'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<table width='100%' align='center' class='print_hide' border='0'>
	<tr>
		<td align='center' class='eSportprinttitle'><b><?=$i_Circular_Circular?><br /><?=$i_Circular_ViewStat?></b></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td class="eSportprinttitle"><?=$i_Circular_CircularNumber?> : <?=$lcircular->CircularNumber?></td>
	</tr>
	<tr>
		<td class="eSportprinttitle"><?=$i_Circular_Title?> : <?=$lcircular->Title?></td>
	</tr>
</table>
<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr valign='top'>
		<td colspan="2">
			<? if ($viewNeeded) { ?>
				<script language="javascript" src="/templates/forms/form_view.js"></script>
				<form name="ansForm" method="post" action="update.php">
					<input type=hidden name="qStr" value="">
					<input type=hidden name="aStr" value="">
				</form>
				<script language="Javascript">
				<?=returnFormStatWords()?>
				var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
				w_total_no = "<?=$i_Circular_NumberOfSigned?>";
				var myQue = "<?=$lform->getConvertedString($lcircular->Question)?>";
				var myAns = new Array();
			<? } ?>
			<?=$x?>
			<? if ($viewNeeded) { ?>
				var stats= new Statistics();
				stats.qString = myQue;
				stats.answer = myAns;
				stats.analysisData();
				document.write(stats.getStats());
			</script>
			<? } ?>
		</td>
	</tr>
</table>
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>