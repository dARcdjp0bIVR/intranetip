<?php
// using 
################## Change Log [Start] ##############
#   Date:   2019-05-30 Anna
#           DHL add userlogin after user name 
#
#   Date:   2018-12-17 Anna
#           Added $sys_custom['DHL'] cust
#
#	Date:	2016-08-15 	Omas
#			created this page for autocompleter
################## Change Log [End] ################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Get data
$YearID = $_REQUEST['YearID'];
//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);

$libdb = new libdb();
$conds = "";
if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    
    $PICInfo = $libdhl->getPICResponsInfo();
    $DepartmentIDAry = $PICInfo['Department'];
    $DepartmentUseInfoAry = $libdhl->getDepartmentUserRecords(array('DepartmentID'=>$DepartmentIDAry));
    $DepartmentUseIDAry = Get_Array_By_Key($DepartmentUseInfoAry,'UserID');
    
    $conds = " AND UserID IN ('".implode('\',\'',$DepartmentUseIDAry)."')";
    
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]){
        $conds = "";
    } 
}

$SearchValue = $libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q']))));
$sql = "SELECT  
			UserID, 
			".getNameFieldByLang()." as Name, 
			ClassName, 
			ClassNumber,
            UserLogin
		FROM 
			INTRANET_USER 
		WHERE 
			RecordStatus = 1
			AND RecordType IN ( 1 )
			AND USERID NOT IN ('".$SelectedUserIDsStr."') 
			AND ( ChineseName like'%".$SearchValue."%' 
					or EnglishName like '%".$SearchValue."%') 
            $conds
		Order by EnglishName asc
		Limit 5";
$result = $libdb->returnResultSet($sql);

$returnString = '';
foreach((array)$result as $_StudentInfoArr){
	$_UserID = $_StudentInfoArr['UserID'];
	$_Name = $_StudentInfoArr['Name'];
	$_ClassName = $_StudentInfoArr['ClassName'];
	$_ClassNumber = $_StudentInfoArr['ClassNumber'];
	if($_ClassName != '' && $_ClassNumber != ''){
		$displayClass = ' ('.$_ClassName.'-'.$_ClassNumber.')';
	}
	$UserLoginDisplay = '';
	if($sys_custom['DHL']){
	    $UserLoginDisplay =  " (".$_StudentInfoArr['UserLogin'].")";
	}
	$_thisUserNameWithClassAndClassNumber = $_Name.$UserLoginDisplay.$displayClass;

	$returnString .= $_thisUserNameWithClassAndClassNumber.'|'.'|'.'U'.$_UserID."\n";
}

intranet_closedb();
echo $returnString;
?>