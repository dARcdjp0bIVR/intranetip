<?
###################################
#
#	Date:	2015-01-22	Roy
#			fix: check $plugin['eClassTeacherApp'] instead of $plugin['eClassApp'] for push message option
#
#	Date:	2015-01-02	Bill [Case #P70566]
#			- get circularAttFolder from $_POST
#
#	Date:	2014-10-16	Roy
#			Improved: add notify by push message option
#
#	Date:	2013-09-10	YatWoon
#			add copy attachment logic
#
#	Date:	2013-07-24	YatWoon
#			stripslashes which create from templates [Case#2013-0708-0924-06167]
#
###################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lform = new libform();
$lcircular = new libcircular();

if ($templateID != -1)
{
    $lcircular->returnRecord($templateID);
    
    $Title = $lcircular->Title;
	$Description = $lcircular->Description;
	$Question = stripslashes($lform->getConvertedString($lcircular->Question));
	$DisplayQuestionNumber = $lcircular->DisplayQuestionNumber;
	
	$template_AttFolder = $lcircular->Attachment;
	
	# clear attachment
	$lf = new libfilesystem();
//	$path = "$file_path/file/circular/$circularAttFolder";
	$path = "$file_path/file/circular/".$_POST['circularAttFolder'];
	$templf = new libfiletable("", $path,0,0,"");
	$temp_files = $templf->files;
	if(sizeof($temp_files)>0)
	{
		for($i=0;$i<sizeof($temp_files);$i++)
		{
			$des = $path."/".$temp_files[$i][0];
			$lf->file_remove($des);
		}
	}
	
	# copy file 
	$template_path = "$file_path/file/circular/$template_AttFolder";
	$templf = new libfiletable("", $template_path,0,0,"");
	$template_files = $templf->files;
	if(sizeof($template_files)>0)
	{
		for($i=0;$i<sizeof($template_files);$i++)
		{
			$loc = $template_path."/".$template_files[$i][0];
			$des = $path."/".$template_files[$i][0];
			$lf->lfs_copy($loc, $des);
		}
	}
}
$now = time();
$defaultEnd = $now + ($lcircular->defaultNumDays*24*3600);
$start = date('Y-m-d',$now);
$end = date('Y-m-d',$defaultEnd);

intranet_closedb();

$x = $lcircular->return_FormContent($start, $end, $isIssuedCircular, $Title, $Description, $Question,'','','',$template_AttFolder,$DisplayQuestionNumber, ($plugin['eClassTeacherApp'] ? 1 : 0));

echo $x;
?>