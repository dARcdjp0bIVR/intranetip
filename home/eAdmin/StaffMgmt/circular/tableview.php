<?php
# using: 

############### Change Log ######
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
#	Date:	2017-04-25 (Carlos)
#			$sys_custom['DHL'] - Display Department, hide Identity type for DHL.
#
#	Date:	2016-11-04 [Villa]
#			Modified notifyParentByApp() - Fix the filter function for sending push msg
#	Date:	2016-10-28 [Villa]
#			hide the pushMsg Button if no $plugin['eClassTeacherApp']
#
#	Date: 	2016-09-20 [Villa] #E100627
#			add a popup thickbox for sending push up msg
#
#	Date:	2012-08-01 [YatWoon]
#			display circular content at the top
#
#	Date :	2010-10-29 [YatWoon]
#			add teaching staff / non-teaching staff filter
#
#################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if($sys_custom['DHL']){
    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
    $libdhl = new libdhl();
    $isPIC = $libdhl->isPIC();
}

if (!$special_feature['circular'] || ($_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] || !($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]  || ($isPIC && $sys_custom['DHL']))))
{
	if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
	{
		header("location: ./settings/basic_settings/");	
		exit;
	}
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

// intranet_auth();
// intranet_opendb();

// debug_pr($CircularID);
$MODULE_OBJ['title'] = $i_Circular_ViewResult;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
$lcircular = new libcircular($CircularID);
$Title = $lcircular->Title;
$hasRight = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];  # 1 - circular type admin
    if ($adminlevel==1 || $lcircular->IssueUserID == $UserID)     # Allow if Full admin or issuer
    {
        $hasRight = true;
    }
}

$questions = $lcircular->splitQuestion($lcircular->Question);
$signedCount = 0;
$totalCount = 0;
$singleTable = true;

$all = 1;
$data = $lcircular->returnTableViewAll($TeachingType);
$totalCount = sizeof($data);

if($sys_custom['DHL']){
	include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	$libdhl = new libdhl();
	
	$userIdAry = Get_Array_By_Key($data,0);
	$userRecords = $libdhl->getUserRecords(array('user_id'=>$userIdAry,'GetOrganizationInfo'=>1));
	$userIdToRecordMap = array();
	for($i=0;$i<count($userRecords);$i++){
		$userIdToRecordMap[$userRecords[$i]['UserID']] = $userRecords[$i];
	}
}

if (sizeof($data)!=0)
{
//        $toolbar = exportIcon2()."<a href=\"export.php?CircularID=$CircularID\">$button_export</a>";
       $x .= "<table width='90%' border='0' cellspacing='0' cellpadding='5' align='center'>";
       $x .= "<tr>";
       $x .= "<td class='tablebluetop tabletopnolink'>$i_UserName</td>";
       if($sys_custom['DHL']){
       		$x .= "<td class='tablebluetop tabletopnolink'>".$Lang['General']['UserLogin']."</td>";
       		$x .= "<td class='tablebluetop tabletopnolink'>".$Lang['DHL']['Company']."</td>";
       		$x .= "<td class='tablebluetop tabletopnolink'>".$Lang['DHL']['Division']."</td>";
       		$x .= "<td class='tablebluetop tabletopnolink'>".$Lang['DHL']['Department']."</td>";
       }else{
       		$x .= "<td class='tablebluetop tabletopnolink'>$i_identity</td>";
       }
        for ($i=0; $i<sizeof($questions); $i++)
        {
             if ($questions[$i][0]!=6)
             {
                 $tooltipQ = makeTooltip($questions[$i][1]);
                 $x .= "<td class='tablebluetop'><a href=\"#\" $tooltipQ class='tabletoplink'>$i_Circular_Option ".($i+1)."</a></td>";
             }
        }
        $x .= "<td class='tablebluetop tabletopnolink'>$i_Circular_Signer</td>";
        $x .= "<td class='tablebluetop tabletopnolink'>$i_Circular_SignedAt</td></tr>\n";
//         debug_pr($data);
        for ($i=0; $i<sizeof($data); $i++)
        {
             list ($targetID,$name,$type,$status,$answer,$signer,$last, $user_identity) = $data[$i];
             if ($status == 2)
             {
                 $css = ($type==1?"attendancepresent":"attendanceouting");
                 $signedCount++;
             }
             else
             {
                 $css = "attendanceabsent";
                 $last = "$i_Circular_Unsigned";
             }

             $x .= "<tr class=\"$css\"><td class=\"tabletext $css\"><a class=\"tablebluelink\" href=\"/home/eService/circular/sign.php?CircularID=$CircularID&targetUserID=$targetID\">$name</a></td>";
			 if($sys_custom['DHL']){
			 	$x .= "<td class=\"tabletext $css\">".($userIdToRecordMap[$targetID]['UserLogin'])."</td>";
			 	$x .= "<td class=\"tabletext $css\">".($userIdToRecordMap[$targetID]['CompanyCode'])."</td>";
			 	$x .= "<td class=\"tabletext $css\">".($userIdToRecordMap[$targetID]['DivisionCode'])."</td>";
			 	$x .= "<td class=\"tabletext $css\">".($userIdToRecordMap[$targetID]['DepartmentCode'])."</td>";
			 }else{
			 	$x .= "<td class=\"tabletext $css\">$user_identity</td>";
			 }
             for ($j=0; $j<sizeof($questions); $j++)
             {
                  if ($questions[$j][0]!=6)
                  $x .= "<td class=\"tabletext $css\">".$answer[$j]." &nbsp;</td>";
             }
             $x .= "<td class=\"tabletext $css\">$signer&nbsp;</td>
             <td class=\"tabletext $css\">$last</td>
             </tr>\n";
        }
        $x .= "</table>\n";
    }
    else
    {
        $x .= "<table width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"1\">\n";
        $x .= "<tr><td align=\"center\">$i_Circular_NoTarget</td></tr>\n";
        $x .= "</table>\n";
    }

    
    
# Staff Type 
$staffTypeMenu = "<select name=\"TeachingType\" id=\"TeachingType\" onChange=\"reloadForm()\">";
$staffTypeMenu .= "<option value=''".((!isset($TeachingType) || $TeachingType=="") ? " selected" : "").">{$Lang['StaffAttendance']['AllStaff']}</option>";
$staffTypeMenu .= "<option value='1'".(($TeachingType=='1') ? " selected" : "").">{$Lang['Identity']['TeachingStaff']}</option>";
$staffTypeMenu .= "<option value='0'".(($TeachingType=='0') ? " selected" : "").">{$Lang['Identity']['NonTeachingStaff']}</option>";
$staffTypeMenu .= "</select>";
    
$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);


//Villa
# build thickbox
$thickBoxHeight = 180;
$thickBoxWidth = 600;

$pushMessageButton = $linterface->Get_Thickbox_Link($thickBoxHeight, $thickBoxWidth, "",
		$Lang['AppNotifyMessage']['MessageToStaff'], "", 'divPushMessage', $Content='', 'pushMessageButton');
# thickbox layout
$thickboxContent = '';
$thickboxContent .= '<div id="divPushMessage" style="display:none">';
$thickboxContent .= '<form id="messageMethodForm" action="#">';
$thickboxContent .= '<table class="form_table_v30">';
$thickboxContent .= '<tr>';
$thickboxContent .= '<td class="field_title"><span>'.$Lang['AppNotifyMessage']['MessagingMethodToStaff'].'</span></td>';
$thickboxContent .= '<td>';
$smsChecked = 1;
if ($plugin['eClassTeacherApp']) {
	$smsChecked = 0;
	$thickboxContent .= $linterface->Get_Radio_Button('pushMessage', 'messageMethod', 'pushMessage', $isChecked=1, $Class='', $Lang['AppNotifyMessage']['PushMessage'], $Onclick='', $Disabled='');
	$thickboxContent .= '<br/>';
}

$thickboxContent .= '</tr>';
$thickboxContent .= '</table>';
$thickboxContent .= '<br/>';
$thickboxContent .= '<div align="center">';
if($plugin['eClassTeacherApp']){
	$thickboxContent .= '&nbsp;';
	$thickboxContent .= $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['MessageToStaff'], "button", "sendMessage()");
	$thickboxContent .= '<br /><br />';
}
$thickboxContent .= '</div>';
$thickboxContent .= '</form>';
$thickboxContent .= '</div>';


if ($plugin['eClassTeacherApp']) {
	$sendPushMessagePhp = 'notify_staff_via_eClassApp.php';
}
else {
	$sendPushMessagePhp = 'notify_staff_via_app.php';
}
      
?>
<script language="Javascript" src='/templates/brwsniff.js'></script>
<script language="Javascript" src='/templates/tooltip.js'></script>
<script language="JavaScript" type="text/javascript">
<!--
function click_print()
{
	with(document.form1)
        {
                submit();
	}
        
}

function reloadForm() {
	document.form1.action = "";
	document.form1.target = "_self";
	document.form1.submit();
}
//-->
</script>
<style type="text/css">
#ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
isToolTip = true;

function sendPushMessage(){
	$('a#pushMessageButton').click();
	
}

function sendMessage(){
	
	var selectedVal = "";
	selectedVal = $('input:radio[name=messageMethod]:checked').val();
	
	if (selectedVal == 'pushMessage') {
		notifyParentByApp();
		js_Hide_ThickBox();
	}
}

function notifyParentByApp()
{
	$('#PreviewStatus').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	var TeachingType = $('#TeachingType').val();
	$.ajax({
	    url: '<?=$sendPushMessagePhp?>?NoticeID=<?=$CircularID?>',
	    data: "CircularID="+<?= $CircularID?>+"&TeachingType="+TeachingType,
	    error: function(xhr) {
			alert('request error');
	    },
	    success: function(response) {
	      	$('#PreviewStatus').html(response);
	    }
	});
}


</script>
<div id="ToolTip"></div>
<form name="form1" action="tableview_print_preview.php" method="post" target = "_blank">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr>
				<td colspan="2" align="right">
				<? if($lcircular->Question!="") { ?>
					<a href="stat.php?CircularID=<?=$CircularID?>&all=<?=$all?>&TeachingType=<?=$TeachingType?>" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eOffice/icon_viewstat.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Circular_ViewStat?></a>
				<? } ?>
				</td>
			</tr>
				
			<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateStart?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateStart?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_DateEnd?></span>
					</td>
					<td class="tabletext"><?=$lcircular->DateEnd?></td>
				</tr>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'><?=$i_Circular_CircularNumber?></span></td>
					<td class='tabletext'><?=$lcircular->CircularNumber?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Title?></span>
					</td>
					<td class="tabletext"><?=$lcircular->Title?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Description?></span>
					</td>
					<td class="tabletext"><?=htmlspecialchars_decode($lcircular->Description)?></td>
				</tr>
				<? if ($attachment != "") { ?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Attachment?></span>
					</td>
					<td class="tabletext"><?=$attachment?></td>
				</tr>
				<? } ?>
				<?
					$issuer = $lcircular->returnIssuerName();
				?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_Issuer?></span>
					</td>
					<td class="tabletext"><?=$issuer?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Circular_RecipientType?></span>
					</td>
					<td class="tabletext"><?=$targetType[$lcircular->RecordType]?></td>
				</tr>
			<?php if(!$sys_custom['DHL']){ ?>
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class="formfieldtitle"><span class="tabletext"><?=$i_identity?></span></td>
					<td class='tabletext'><?=$staffTypeMenu?></td>
				</tr>
			<?php } ?>	
				<tr valign='top'>
					<td width='30%' valign='top' nowrap='nowrap' class="formfieldtitle"><span class="tabletext"><?="$i_Circular_Signed/$i_Circular_Total"?></span></td>
					<td class='tabletext'><?="$signedCount/$totalCount"?></td>
				</tr>
				<tr>
					<td colspan="2" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td colspan="2" align="left">
						<a href="export.php?CircularID=<?=$CircularID?>&all=<?=$all?>&TeachingType=<?=$TeachingType?>" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_export.gif" width="20" height="20" border="0" align="absmiddle"><?=$button_export?></a>
					</td>
				</tr>
			</table>
		<td>
	</tr>
	
	<tr>
		<td colspan="2"><?=$x?></td>
	</tr>
	<tr>
		<td>
			<table width="95%" border="0" cellspacing="0" cellpadding="2" align="center">
				<tr>
					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center">
					<?php
					if($plugin['eClassTeacherApp']){
						echo $linterface->GET_ACTION_BTN($Lang['AppNotifyMessage']['MessageToStaff'], "button", "sendPushMessage();");
					}
					echo $pushMessageButton;?>
					<?= $linterface->GET_ACTION_BTN($button_print . ($intranet_session_language=="en"?" ":"") . $button_preview, "button", "click_print()") ?>
					<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close()","submit3") ?>
				</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<p><center><span id='PreviewStatus'></span></center></p>
<input type="hidden" name="CircularID" value="<?=$CircularID?>">
</form>
 <?=$thickboxContent?>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>