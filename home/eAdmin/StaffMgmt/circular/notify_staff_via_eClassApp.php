<?php
# using: 
/*	
 * 2016-11-04	Villa
 *  Do not send to the user who signed the eCircular
 *  Fixing the filter for the Identity
 *  
 * 2016-09-20	Villa
 * 	Open the file - method of sending push up msg
 *	
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");;
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

$lcircular = new libcircular($CircularID);
$leClassApp = new libeClassApp();

$all = 1;
$data = $lcircular->returnTableViewAll($TeachingType);
$NoticeNumber = "".intranet_undo_htmlspecialchars($lcircular->CircularNumber);
$NoticeTitle = "".intranet_undo_htmlspecialchars($lcircular->Title);
$NoticeDeadline = "".$lcircular->DateEnd;

$MessageTitle = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eCircular']['Title']['App']);
$MessageContent = str_replace("[NoticeNumber]", $NoticeNumber, $Lang['AppNotifyMessage']['eCircular']['Alert']['App']);
$MessageContent = str_replace("[NoticeTitle]", $NoticeTitle, $MessageContent);
$MessageContent = str_replace("[NoticeDeadline]", $NoticeDeadline, $MessageContent);

### get parent student mapping
$messageInfoAry = array();
foreach((array)$data as $key => $teacherId){
	// check if the user signed or not
	if(empty($teacherId[5])){
		$_targetTeacherId = $leClassApp->getDemoSiteUserId($teacherId[0]);
		$parentStudentAssoAry[$teacherId[0]] = array($teacherId[0]);
	}
	
	
}
$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent,  $isPublic='', $recordStatus=1, $appType='T', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eCircular', $CircularID);
$statisticsAry = $leClassApp->getPushMessageStatistics($notifyMessageId);
$statisticsAry = $statisticsAry[$notifyMessageId];

intranet_closedb();

if ($notifyMessageId > 0) {
	if($statisticsAry['numOfSendSuccess']==''){
		$statisticsAry['numOfSendSuccess'] = 0;
	}	
	echo "<font color='#DD5555'>".str_replace("[SentTotal]", $statisticsAry['numOfSendSuccess'], $Lang['AppNotifyMessage']['eNotice']['send_result_staff']). "</font>";

} else
{
	$ErrorMsg = $Lang['AppNotifyMessage']['eNotice']['send_failed_staff'];
	if ($statisticsAry['numOfRecepient'] == 0) {
		$ErrorMsg .= $Lang['AppNotifyMessage']['eNotice']['send_result_no_staff'];
	}
	echo "<font color='red'>".$ErrorMsg."</font> <font color='grey'>(".$statisticsAry['numOfRecepient']."-".$statisticsAry['numOfSendSuccess'].")</font>";

}
?>