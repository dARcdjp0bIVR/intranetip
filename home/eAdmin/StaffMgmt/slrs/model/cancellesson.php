<?php
if (!defined("LIBSLRS_MODEL"))
{
    if (file_exists(dirname(__FILE__) . '/model.php'))
    {
        require_once(dirname(__FILE__) . '/model.php');
    }
}

if (defined("LIBSLRS_MODEL"))
{
    class CancelLessonMD extends SLRS_Model
    {
        public function __construct() {
            $this->SLRS_Model();
            $this->table = "INTRANET_SLRS_CANCEL_LESSON";
            $this->timeslot_table = "INTRANET_TIMETABLE_TIMESLOT";
            
            $this->pri_key = "CancelLessonID";
            $this->field = array (
                            "CancelLessonID",
                            "UserID",
                            "CancelDate",
                            "TimetableID",
                            "TimeSlotID",
                            "YearTermID",
                            "SubjectID",
                            "SubjectGroupID",
                            "LocationID",
                            "CancelRemark",
                            "InputBy_UserID",
                            "DateTimeInput",
                            "DeletedAt",
                            "DeletedBy"
                        );
            $this->date_field = array( "CancelDate" );
            $this->datetime_field = array( "DateTimeInput", "DeletedAt" );
            
            $this->unique_field = array(
                "UserID",
                "CancelDate",
                "TimeSlotID",
                "SubjectGroupID"
            );
        }
        
        public function getListingHeader()
        {
            return array("CancelDate", "TeacherName", "Timeslot", "LessonInfo", "CancelRemark", "DateTimeInput", "checkbox");
        }
         
        /*
         * $request = array(
         *  "user_table" => '',
         *  "name_field" => '',
         * );
         */
        public function getListingSQL($request = array())
        {
            $subjectGroupName = Get_Lang_Selection("ClassTitleB5", "ClassTitleEN");
            $strSQL = "SELECT ";
            $strSQL .= " CancelDate,
                        " . $request["name_field"] . " as TeacherName,
                        CONCAT(TimeSlotName, ' (', LEFT(StartTime, 5), '-', LEFT(EndTime, 5), ')') as Timeslot,
                        " . $subjectGroupName . " as LessonInfo,
                        CancelRemark,
                        iscl.DateTimeInput,
                        CONCAT('<input type=checkbox name=cancelIdAry[] id=cancelIdAry[] value=\"', " . $this->pri_key . ",'\">') as checkbox
            ";
            $strSQL .= " FROM " . $this->table . " AS iscl ";
            $strSQL .= " INNER JOIN SUBJECT_TERM_CLASS AS stc ON (stc.SubjectGroupID=iscl.SubjectGroupID)";
            $strSQL .= " JOIN " . $request["user_table"] . " AS iu ON (iu.UserID=iscl.UserID)";
            $strSQL .= " JOIN " . $this->timeslot_table. " AS itt ON (itt.TimeSlotID=iscl.TimeSlotID)";
            $strSQL .= " WHERE DeletedAt IS NULL";
            return $strSQL;
        }
        
        public function buildRetrieveSQLByParam($param, $withDeleted = false)
        {
            $strSQL = "SELECT 
                            " . $this->pri_key . ", DeletedAt
                        FROM 
                            " . $this->table . "
                        WHERE ";
            
            $strSQL_where = "";
            if (count($param) > 0)
            {
                foreach ($this->unique_field as $field)
                {
                    if (!empty($strSQL_where))
                    {
                        $strSQL_where .= " AND ";
                    }
                    $strSQL_where .= $field. "='" . (isset($param[$field]) ? $param[$field] : '') . "'";
                }
            }
            $strSQL .= $strSQL_where;
            if (!$withDeleted)
            {
                $strSQL .= " AND DeletedAt IS NULL";
            }
            $strSQL .= " LIMIT 1";
            return $strSQL;
        }
        
        public function buildInsertSQLByParam($param)
        {
            if (count($param) > 0)
            {
                $strSQL = "INSERT INTO " . $this->table . " (";
                $strSQL_column = "";
                $strSQL_value = "";
                foreach ($param as $key => $val)
                {
                    if (!empty($strSQL_column))
                    {
                        $strSQL_column .= ", ";
                    }
                    $strSQL_column .= $key;
                    
                    if (!empty($strSQL_value))
                    {
                        $strSQL_value.= ", ";
                    }
                    $strSQL_value .= "'" . $val . "'";
                }
                $strSQL .= $strSQL_column . ", InputBy_UserID, DateTimeInput";
                $strSQL .= ") VALUES (";
                $strSQL .= $strSQL_value . ", '" . $_SESSION["UserID"] . "', NOW()";
                $strSQL .= ");";
                return $strSQL;
            } else {
                return null;
            }
        }
        
        public function buildDeleteSQLByParam($cancelID)
        {
            if ($cancelID > 0)
            {
                $strSQL = "UPDATE " . $this->table . " SET DeletedAt=NOW(), DeletedBy='" . $_SESSION["UserID"] . "'";
                $strSQL .= " WHERE " . $this->pri_key . "='" . $cancelID . "'";
                return $strSQL;
            } else {
                return null;
            }
        }
        
        public function buildUpdateSQLByParam($param, $cancelID)
        {
            if (count($param) > 0 && $cancelID > 0)
            {
                $strSQL = "UPDATE " . $this->table . " SET ";
                
                $strSQL_value= "";
                foreach ($param as $key => $val)
                {
                    if (!empty($strSQL_value))
                    {
                        $strSQL_value .= ", ";
                    }
                    
                    $strSQL_value .= $key . "='" . $val . "'";
                }
                $strSQL .= $strSQL_value . ", InputBy_UserID='" . $_SESSION["UserID"] . "', DeletedAt=NULL, DeletedBy=NULL";
                $strSQL .= " WHERE " . $this->pri_key . "='" . $cancelID . "'";
                
                return $strSQL;
            } else {
                return null;
            }
        }
    }
}
?>