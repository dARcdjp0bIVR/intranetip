<?php

    if (!defined("LIBSLRS_MODEL")) {
        define("LIBSLRS_MODEL", true);
    }

    class SLRS_Model {
    
        public function __construct() {
            $this->SLRS_Model();
        }
        
        public function SLRS_Model() {
            // treated as constructor in PHP 5.3.0-5.3.2
            // treated as regular method as of PHP 5.3.3
            
        }
    }

?>