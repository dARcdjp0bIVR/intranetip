<?php
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================

### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libmessagecenter.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Transactional data ==============================
$libdb = new libdb();
$luser = new libuser();
$lmc = new libmessagecenter();

$libeClassApp = new libeClassApp();
$appType = $eclassAppConfig['appType']['Teacher'];

$connection = new libgeneralsettings();

$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
$date = $_POST["date"];
$absentee = $_POST["lthr"];
if ($absentee == "all") {
	$absentee = null;
} 
$dateArr[0] = $date;
/*
$sql = "SELECT RecordDate,CycleDay,LeaveID,isl.UserID,DateLeave,ReasonCode,Duration,".$name_field." as userName
		FROM(".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd
		LEFT JOIN(
			SELECT LeaveID,UserID,DateLeave,ReasonCode,Duration FROM INTRANET_SLRS_LEAVE
		) as isl ON icd.RecordDate=isl.DateLeave
		LEFT JOIN(
			SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM INTRANET_USER
		) as iu ON isl.UserID=iu.UserID	";
$a = $connection->returnResultSet($sql);
*/

$targetIsAssigned = false;
if (isset($_POST["reportTarget"]) && $_POST["reportTarget"]=='AssignedTeacher' && $sys_custom['SLRS']["dailyReportWithAssignedTeacherOption"])
{
    $a = $indexVar["libslrs"]->getAssignedUserByDate($date, $name_field, $absentee);
    $targetIsAssigned = true;
}
else
{
    $a = $indexVar["libslrs"]->getSubstituteUserByDate($date, $name_field, $absentee);
}
$locaitonList = $indexVar['libslrs']->getLocationList();
$locaitonList = $indexVar["libslrs"]->convertArrKeyToID($locaitonList, "LocationID");

$individualMessageInfoAry = array();
$dateWithFormat = date("d M", strtotime($date));
$allRecords = array();

for($i=0;$i<sizeof($a);$i++){
	$day = $a[$i]["CycleDay"];
	
	if ($targetIsAssigned)
	{
	    $userID = $a[$i]["leaveUserID"];
	}
	else
	{
	    $userID = $a[$i]["UserID"];
	}
	
	$academicTimeSlot = convertMultipleRowsIntoOneRow($indexVar['libslrs']->getAcademicTimeSlot($date),"TimeSlotID");
	$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($date);
	$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
	// $content = $indexVar['libslrs']->getSubstituteTimetableInformation($yearClass, $date, $day, IntegerSafe($userID), $academicTimeSlot, $name_field, false);
	
    $content = $indexVar['libslrs']->getSubstituteTimetableInformation($yearClass, $date, $day, IntegerSafe($userID), $academicTimeSlot, $name_field);
	if (count($content) > 0)
	{
		foreach ($content as $kk => $_RECORD)
		{
			if (!empty($_RECORD["ArrangedTo_UserID"]) && $_RECORD["ArrangedTo_UserID"] > 0)
			{
			    if (!$targetIsAssigned || $targetIsAssigned && $a[$i]["UserID"]==$_RECORD["ArrangedTo_UserID"])
			    {
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["leaveTeacherName"] = Get_Lang_Selection($_RECORD["ChineseName"], $_RECORD["EnglishName"]);
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["name"] = $_RECORD["ArrangedTo_UserName"];
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["date"] = $date;
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["CycleDate"] = $day;
			        
			        $eng_loc_name = $_RECORD["NameEng"];
			        if (!empty($_RECORD["LocationDesc"])) {
			            $eng_loc_name .= ' ('.$_RECORD["LocationDesc"].')';
			        }
			        if ($_RECORD["LocationID"] != $_RECORD["ArrangedTo_LocationID"] && !empty($_RECORD["ArrangedTo_LocationID"]) && isset($locaitonList[$_RECORD["ArrangedTo_LocationID"]])) {
			            $eng_loc_name = $locaitonList[$_RECORD["ArrangedTo_LocationID"]]["NameEng"];
			            if (!empty($locaitonList[$_RECORD["ArrangedTo_LocationID"]]['LocationDesc'])) {
			                $eng_loc_name .= ' ('.$locaitonList[$_RECORD["ArrangedTo_LocationID"]]['LocationDesc'].')';
			            }
			        }
			        if (isset($allRecords[$_RECORD["ArrangedTo_UserID"]]["data"])) {
			            $recordIndex = count($allRecords[$_RECORD["ArrangedTo_UserID"]]["data"]);
			        } else {
			            $recordIndex = 0;
			        }
			        
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["leaveTeacherID"][$userID] = $libdb->Get_Safe_Sql_Query($_RECORD["TimeSlotName"] . ": " . $_RECORD["ClassTitleEN"] . " @ " . $eng_loc_name);
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["data"][$recordIndex]["TimeSlotName"] = $_RECORD["TimeSlotName"];
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["data"][$recordIndex]["ClassTitleEN"] = $_RECORD["ClassTitleEN"];
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["data"][$recordIndex]["location"] = $eng_loc_name;
			        $allRecords[$_RECORD["ArrangedTo_UserID"]]["data"][$recordIndex]["info"] = $_RECORD;
			    }
			}
		}
	}
}

$messageTitle = $Lang['SLRS']['ReportGenerationSLRSNoticationEN'] . " (" . $dateWithFormat . ")";
if ($indexVar['libslrs']->isEJ()) {
	$messageTitle = convert2Big5($messageTitle);
}

$messageContent = 'MULTIPLE MESSAGES';
$index = 0;
$recordDetailArr = array();
if (count($allRecords) > 0)
{
	foreach($allRecords as $arrangedToUserID => $_RECORD)
	{
		if (count($_RECORD["data"]) > 0)
		{
			$_individualMessageInfoAry = array();
			$recordDetailArr[$arrangedToUserID] = $_RECORD["leaveTeacherID"];
			
			$_individualMessageInfoAry['relatedUserIdAssoAry'][$arrangedToUserID] = array($arrangedToUserID);
			$_individualMessageInfoAry['messageTitle'] = $messageTitle;
			
			if ($indexVar['libslrs']->isEJ()) {
			    // [2019-0523-1225-40170] no need to double conversion
				// $_RECORD["name"] = convert2Big5($_RECORD["name"]);
			}
			
// 			$_individualMessageInfoAry['messageContent'] = $Lang['SLRS']['ReportGenerationSLRSNoticationTo'] . $_RECORD["name"] . "\n\n";
// 			$_individualMessageInfoAry['messageContent'] .= $Lang['SLRS']['ReportGenerationSLRSNoticationDate'] . $_RECORD["date"] . " (" . date("l", strtotime($_RECORD["date"])) . ")\n\n";
// 			$_individualMessageInfoAry['messageContent'] .= $Lang['SLRS']['ReportGenerationSLRSNoticationText'] . "\n\n";
			$PushMessagemessageContent = str_replace("__Name__", $_RECORD["name"], $Lang['SLRS']['ReportGenerationSLRSNoticationText']);
			$PushMessagemessageContent = str_replace("__DateEng__", $_RECORD["date"] . " (" . date("l", strtotime($_RECORD["date"])) . ")", $PushMessagemessageContent);
			$PushMessagemessageContent = str_replace("__DateChi__", $_RECORD["date"] . " (" . get_chinese_weekday($_RECORD["date"]). ")", $PushMessagemessageContent);
			$_individualMessageInfoAry['messageContent'] = $PushMessagemessageContent;
			if ($indexVar['libslrs']->isEJ()) {
				$_individualMessageInfoAry['messageContent'] = convert2Big5($_individualMessageInfoAry['messageContent']);
			}
			
			if (count($_RECORD["data"]) > 0)
			{
				foreach ($_RECORD["data"] as $dataIndex => $_TimeSlot)
				{
					if ($indexVar['libslrs']->isEJ()) {
						$_TimeSlot["ClassTitleEN"] = convert2unicode($_TimeSlot["ClassTitleEN"], 1, 1);
						$_TimeSlot["TimeSlotName"] = convert2Big5($_TimeSlot["TimeSlotName"]);
						$_TimeSlot["ClassTitleEN"] = convert2Big5($_TimeSlot["ClassTitleEN"]);
					}
					$_individualMessageInfoAry['messageContent'] .= "\n - " . $_TimeSlot["TimeSlotName"] . ": " . $_TimeSlot["ClassTitleEN"] . " @ " . $_TimeSlot["location"] . "\n";
				}
			}
            // $_individualMessageInfoAry['messageContent'] .= $Lang['SLRS']['ReportGenerationSLRSNoticationThanks'] . "\n\n";
			$_individualMessageInfoAry['messageTitle'] = $_individualMessageInfoAry['messageTitle'];
			$_individualMessageInfoAry['messageContent'] = $_individualMessageInfoAry['messageContent'];
			$individualMessageInfoAry[$index] = $_individualMessageInfoAry;
			$index++;
		}
	}
}

$messageContent = 'MULTIPLE MESSAGES';
$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, "", "1", $appType);
$output["result"] = $notifyMessageId > 0 ? "Success" : "Failed";
if ($output["result"] == "Success")
{
	$output["logId"] = $indexVar['libslrs']->saveNotifyInfo($date, $day, IntegerSafe($_SESSION["UserID"]), IntegerSafe($notifyMessageId), $recordDetailArr, "StaffMgmtSLRSDailySubstitutionReportsNotify");
	$output["notifyMessageId"] = $notifyMessageId;
}

$jsonObj = new JSON_obj();
echo $jsonObj->encode($output);
exit;

function checkEmpty($value){
	return (($value==""||$value==null)?"-":$value);
}

function convertMultipleRowsIntoOneRow($arr,$fieldName){
	$x = "";
	for($i=0; $i<sizeof($arr);$i++){
		if($i==sizeof($arr)-1){
			$x .= $arr[$i][$fieldName];
		}
		else{
			$x .= $arr[$i][$fieldName].",";
		}
	}
	if(sizeof($arr)==0){
		$x = "''";
	}
	return $x;
}

function get_chinese_weekday($datetime)
{
	$weekday  = date('w', strtotime($datetime));
	$weeklist = array('日', '一', '二', '三', '四', '五', '六');
	return '星期' . $weeklist[$weekday];
}
?>