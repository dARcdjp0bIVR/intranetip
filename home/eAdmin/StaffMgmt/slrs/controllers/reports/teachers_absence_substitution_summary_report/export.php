<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();
$dataAry = array();
$connection = new libgeneralsettings();

$startDate = $_GET["startDate"];
$endDate = $_GET["endDate"];

$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");

$sql = "SELECT DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'";
$a = $connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	$dateArr[$i]=$a[$i]["DateLeave"];
}

// $sql ="
// 	SELECT isl.LeaveID,isl.UserID,DateLeave,ReasonCode,isla.SubjectGroupID,isla.SubjectID,isla.LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,isla.TimeSlotID,
// 	ClassCode,ClassTitleEN,ClassTitleB5,CycleDay,TimeSlotName,UserName,AssignedUserName,isVolunteer
// 	FROM(
// 		SELECT LeaveID,UserID,DateLeave,ReasonCode FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
// 	) as isl
// 	LEFT JOIN(
// 		SELECT UserID,LeaveID,SubjectGroupID,SubjectID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,TimeSlotID,isVolunteer
// 		FROM INTRANET_SLRS_LESSON_ARRANGEMENT
// 	) as isla ON isl.UserID=isla.UserID AND isl.LeaveID=isla.LeaveID
// 	LEFT JOIN (
// 		SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
// 	) as stc ON isla.SubjectGroupID=stc.SubjectGroupID
// 	LEFT JOIN  (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON isl.DateLeave=icd.RecordDate
// 	LEFT JOIN(
// 		SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT
// 	) as itt ON isla.TimeSlotID=itt.TimeSlotID
// 	LEFT JOIN(
// 		SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER 
// 	) as stcu ON isla.SubjectGroupID=stcu.SubjectGroupID AND isla.UserID=stcu.UserID
// 	LEFT JOIN(
// 		SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."		
// 	) as iu ON isl.UserID=iu.UserID
// 	LEFT JOIN(
// 		SELECT UserID,".$name_field." as AssignedUserName FROM ".$slrsConfig['INTRANET_USER']."		
// 	) as iua ON isla.ArrangedTo_UserID=iua.UserID
	
// 	ORDER BY DateLeave,UserName,TimeSlotName
// 	";
// //echo $sql;

// $info=$connection->returnResultSet($sql);

$info = $indexVar['libslrs']->getSubstitutionStatisticsRecordByDate($startDate, $endDate);


$dataAry[0][0]=$Lang['SLRS']['ReportTeachersAbsenceSubstitutionSummary'];
$dataAry[1][0]="";

$index = 2;
$header = true;
$reasonCode="";

$dataAry[$index][0]=$Lang['SLRS']['ReportOverall'];
$dataAry[$index+1][0]=$Lang['SLRS']['ReportTC'];
$dataAry[$index+1][1]=$Lang['SLRS']['ReportDate'];
$dataAry[$index+1][2]=$Lang['SLRS']['ReportDay'];
$dataAry[$index+1][3]=$Lang['SLRS']['ReportPeriod'];
$dataAry[$index+1][4]=$Lang['SLRS']['ReportSubject'];
$dataAry[$index+1][5]=$Lang['SLRS']['ReportClass'];
$dataAry[$index+1][6]=$Lang['SLRS']['ReportSubTC'];
$dataAry[$index+1][7]=$Lang['SLRS']['ReportVolunteered'];
$dataAry[$index+1][8]=$Lang['SLRS']['ReportExemption'];
$index = $index +1;

for($i=0; $i<sizeof($info);$i++){
	$dataAry[$index+1][0]=$info[$i]["UserName"];
	$dataAry[$index+1][1]=$info[$i]["DateLeave"];
	
	if (empty($info[$i]["LessonArrangementID"]) && (!isset($info[$i]["LessonExchangeID"]) || empty($info[$i]["LessonExchangeID"])))
	{
		if ($info[$i]["ArrangedTo_UserID"] == "NOT_AVAILABLE")
		{
			$dataAry[$index+1][2] = $Lang['SLRS']['NotAvailableTeacher'];
		} else {
			$dataAry[$index+1][2] = $Lang['SLRS']['SubstitutionArrangementDes']['notArranged'];
		}
	}
	else
	{
		$dataAry[$index+1][2]=$info[$i]["CycleDay"];
		$dataAry[$index+1][3]=$info[$i]["TimeSlotName"];
		$dataAry[$index+1][4]=$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupID"],"subjectName");
		$dataAry[$index+1][5]=$indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupID"]);
		if ($info[$i]["ArrangedTo_UserID"] < 0 || empty($info[$i]["ArrangedTo_UserID"]))
		{
			if (isset($info[$i]["LessonExchangeID"]) && !empty($info[$i]["LessonExchangeID"]))
			{
				$dataAry[$index+1][6] = $Lang['SLRS']['SubstitutionArrangementDes']['isExchangeRecord'];
			}
			else
			{
				$dataAry[$index+1][6] = $Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'];
			}
		} else {
			$dataAry[$index+1][6]=$info[$i]["AssignedUserName"];
		}
		$dataAry[$index+1][7]=(($info[$i]["isVolunteer"]==1)?"✔":"-");
		$dataAry[$index+1][8]="-";
	
		$fullRec[$info[$i]["ReasonCode"]][] = $info[$i];
	}
	$index=$index+1;
}

$index = $index +2;

if (count($fullRec) > 0)
{
	foreach ($fullRec as $kk => $info) {
		for($i=0; $i<sizeof($info);$i++){
			
			if($reasonCode != $info[$i]["ReasonCode"] && $reasonCode!=""){
				$header = true;
				$dataAry[$index+1][0]="";
				$index = $index +2;
			}
			if($header == true){
				$dataAry[$index][0]=$Lang['SLRS']['OfficialLeavel'.$info[$i]["ReasonCode"]];
				$dataAry[$index+1][0]=$Lang['SLRS']['ReportTC'];
				$dataAry[$index+1][1]=$Lang['SLRS']['ReportDate'];
				$dataAry[$index+1][2]=$Lang['SLRS']['ReportDay'];
				$dataAry[$index+1][3]=$Lang['SLRS']['ReportPeriod'];
				$dataAry[$index+1][4]=$Lang['SLRS']['ReportSubject'];
				$dataAry[$index+1][5]=$Lang['SLRS']['ReportClass'];
				$dataAry[$index+1][6]=$Lang['SLRS']['ReportSubTC'];
				$dataAry[$index+1][7]=$Lang['SLRS']['ReportVolunteered'];
				$dataAry[$index+1][8]=$Lang['SLRS']['ReportExemption'];
				$index = $index +1;
			}
			
			$dataAry[$index+1][0]=$info[$i]["UserName"];
			$dataAry[$index+1][1]=$info[$i]["DateLeave"];
			
			if ($info[$i]["ArrangedTo_UserID"] == "NOT_AVAILABLE")
			{
				$dataAry[$index+1][2] = nl2br($info[$i]["ReasonCode"]);
			}
			else if (empty($info[$i]["LessonArrangementID"]) && (!isset($info[$i]["LessonExchangeID"]) || empty($info[$i]["LessonExchangeID"])))
			{
				$dataAry[$index+1][2] = $Lang['SLRS']['SubstitutionArrangementDes']['notArranged'];
			}
			else
			{
				$dataAry[$index+1][2]=$info[$i]["CycleDay"];
				$dataAry[$index+1][3]=$info[$i]["TimeSlotName"];
				$dataAry[$index+1][4]=$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupID"],"subjectName");
				$dataAry[$index+1][5]=$indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupID"]);
				
				if ($info[$i]["ArrangedTo_UserID"] < 0 || empty($info[$i]["ArrangedTo_UserID"]))
				{
					if (isset($info[$i]["LessonExchangeID"]) && !empty($info[$i]["LessonExchangeID"]))
					{
						$dataAry[$index+1][6] = $Lang['SLRS']['SubstitutionArrangementDes']['isExchangeRecord'];
					}
					else
					{
						$dataAry[$index+1][6] = $Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'];
					}
				}
				else
				{
					$dataAry[$index+1][6] = $info[$i]["AssignedUserName"];
				}
				$dataAry[$index+1][7] = (($info[$i]["isVolunteer"]==1)?"✔":"-");
				$dataAry[$index+1][8]="-";
			}
			
			$header = false;
			$index=$index+1;
			$reasonCode = $info[$i]["ReasonCode"];
		}
	}
}

$notAvailableInfo = $indexVar['libslrs']->getNotAvailableTeacherRecordByDate($startDate, $endDate);
if (count($notAvailableInfo) > 0)
{
	$header = true;
	$dataAry[$index+1][0]="";
	$index = $index +2;
	foreach ($notAvailableInfo as $i => $info)
	{
		if($header == true){
			$dataAry[$index][0]= $Lang['SLRS']['NotAvailableTeachers'];
			$index++;
			
			$dataAry[$index][0]=$Lang['SLRS']['ReportTC'];
			$dataAry[$index][1]=$Lang['SLRS']['ReportDate'];
			$dataAry[$index][2]=$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"];
			$index++;
		}
		
		$dataAry[$index][0]=$info["UserName"];
		$dataAry[$index][1]=$info["DateLeave"];
		$dataAry[$index][2]=$info["ReasonCode"];
		$header = false;
		$index++;
	}
}
$dataAry[$index][0] = "";

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'teachers_absence_substitution_summary_report.csv';
// $lexport->EXPORT_FILE($fileName, $exportContent);
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');


?>