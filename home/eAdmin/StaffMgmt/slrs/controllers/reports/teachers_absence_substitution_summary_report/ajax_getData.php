<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();

$startDate = $_POST["startDate"];
$endDate = $_POST["endDate"];


$info = $indexVar['libslrs']->getSubstitutionStatisticsRecordByDate($startDate, $endDate);

$notAvailableInfo = $indexVar['libslrs']->getNotAvailableTeacherRecordByDate($startDate, $endDate);
if (count($notAvailableInfo) > 0) {
	if ($info == null)
	{
		$info = array();
	}
	$info = array_merge($info, $notAvailableInfo);
}
/*
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");

$sql = "SELECT DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'";
$a = $connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	$dateArr[$i]=$a[$i]["DateLeave"];
}

$sql ="
	SELECT isl.LeaveID,isl.UserID,DateLeave,ReasonCode,isla.SubjectGroupID,isla.SubjectID,isla.LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,isla.TimeSlotID,
	ClassCode,ClassTitleEN,ClassTitleB5,CycleDay,TimeSlotName,UserName,AssignedUserName,isVolunteer, isla.LessonArrangementID
	FROM(
		SELECT LeaveID,UserID,DateLeave,ReasonCode FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
	) as isl
	LEFT JOIN(
		SELECT UserID,LeaveID,SubjectGroupID,SubjectID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,TimeSlotID,isVolunteer, LessonArrangementID
		FROM INTRANET_SLRS_LESSON_ARRANGEMENT
	) as isla ON isl.UserID=isla.UserID AND isl.LeaveID=isla.LeaveID
	LEFT JOIN (
		SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS
	) as stc ON isla.SubjectGroupID=stc.SubjectGroupID
	LEFT JOIN  (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON isl.DateLeave=icd.RecordDate
	LEFT JOIN(
		SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT
	) as itt ON isla.TimeSlotID=itt.TimeSlotID
	LEFT JOIN(
		SELECT SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_USER 
	) as stcu ON isla.SubjectGroupID=stcu.SubjectGroupID AND isla.UserID=stcu.UserID
	LEFT JOIN(
		SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."		
	) as iu ON isl.UserID=iu.UserID
	LEFT JOIN(
		SELECT UserID,".$name_field." as AssignedUserName FROM ".$slrsConfig['INTRANET_USER']."		
	) as iua ON isla.ArrangedTo_UserID=iua.UserID
	
	ORDER BY DateLeave,UserName,TimeSlotName
	";
// echo $sql;

$info=$connection->returnResultSet($sql);
*/

$btnAry = array();
$btnAry[] = array('export', 'javascript: export_csv();');
$btnAry[] = array('print', 'javascript: print_timetable();');

$x = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$x .= "<br/><br/>";

$reasonCode = "";
$index = 0;

$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=3>".$Lang['SLRS']['ReportTeachersAbsenceSubstitutionSummary']."</td></tr>"."\r\n";
$x .= "</table><br/>"."\r\n";

$x .= "<span style='font-weight:bold;'>".$Lang['SLRS']['ReportOverall']."</span>";
$x .= "<table class=\"common_table_list\">"."\r\n";
$x .= "<tr><th style='width:12%'>".$Lang['SLRS']['ReportTC']."</th>";
$x .= "<th style='width:20%'>".$Lang['SLRS']['ReportDate']."</th>";
$x .= "<th style='width:12%'>".$Lang['SLRS']['ReportDay']."</th>";
$x .= "<th style='width:12%'>".$Lang['SLRS']['ReportPeriod']."</th>";
$x .= "<th style='width:12%'>".$Lang['SLRS']['ReportSubject']."</th>";
$x .= "<th style='width:12%'>".$Lang['SLRS']['ReportClass']."</th>";
$x .= "<th style='width:12%'>".$Lang['SLRS']['ReportSubTC']."</th>";
$x .= "<th style='width:12%'>".$Lang['SLRS']['ReportVolunteered']."</th></tr>";

for($i=0; $i<sizeof($info);$i++){
	if (!(empty($info[$i]["LessonArrangementID"]) && $info[$i]["ArrangedTo_UserID"] == "NOT_AVAILABLE" ))
	{
		$x .= "<tr>"."\r\n";
		$x .= "<td>".$info[$i]["UserName"]."</td>"."\r\n";
		$x .= "<td>".$info[$i]["DateLeave"]."</td>"."\r\n";
		
		if (empty($info[$i]["LessonArrangementID"]) && (!isset($info[$i]["LessonExchangeID"]) || empty($info[$i]["LessonExchangeID"])))
		{
			if ($info[$i]["ArrangedTo_UserID"] == "NOT_AVAILABLE")
			{
				$x .= "<td colspan=6 style='color:#f00;'>". $Lang['SLRS']['NotAvailableTeacher'] ."</td>"."\r\n";
			} else {
				$x .= "<td colspan=6 style='color:#f00;'>". $Lang['SLRS']['SubstitutionArrangementDes']['notArranged'] ."</td>"."\r\n";
			}
		}
		else
		{
			$x .= "<td>".$info[$i]["CycleDay"]."</td>"."\r\n";
			$x .= "<td>".$info[$i]["TimeSlotName"]."</td>"."\r\n";
			$x .= "<td>".$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupID"],"subjectName")."</td>"."\r\n";
			$x .= "<td>".$indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupID"])."</td>"."\r\n";
	
			if ($info[$i]["ArrangedTo_UserID"] < 0 || empty($info[$i]["ArrangedTo_UserID"]))
			{
				if (isset($info[$i]["LessonExchangeID"]) && !empty($info[$i]["LessonExchangeID"]))
				{
					$x .= "<td style='color:#f00;'>". $Lang['SLRS']['SubstitutionArrangementDes']['isExchangeRecord'] ."</td>"."\r\n";
				}
				else 
				{
					$x .= "<td style='color:#f00;'>". $Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'] ."</td>"."\r\n";
				}
			}
			else
			{
				$x .= "<td style='width:12%'>".$info[$i]["AssignedUserName"]."</td>"."\r\n";
			}
			$x .= "<td>".(($info[$i]["isVolunteer"]==1)?"✔":"-")."</td>"."\r\n";
		}
		$x .= "</tr>"."\r\n";
	}
	
	if ($info[$i]["ArrangedTo_UserID"] == "NOT_AVAILABLE" )
	{
		$fullReasonCode["NotAvailableTeacher"][] = $info[$i];
	}
	else
	{
		$fullReasonCode[$info[$i]["ReasonCode"]][] = $info[$i];
	}
}
$x .= "</table>";

$x .= "<br/>";
$not_header = false;
$is_AvailHeader = false;

if (count($fullReasonCode) > 0)
{
	foreach ($fullReasonCode as $kk => $info)
	{
		if ($kk == "NotAvailableTeacher")
		{
			$x .= "<span style='font-weight:bold;'>". $Lang['SLRS']['NotAvailableTeachers'] ."</span>";
		}
		else
		{
			$x .= "<span style='font-weight:bold;'>".$Lang['SLRS']['OfficialLeavel'.$kk]."</span>";
		}
		
		$x .= "<table class=\"common_table_list\">"."\r\n";
		$x .= "<tr><th style='width: 16%'>".$Lang['SLRS']['ReportTC']."</th>";
		$x .= "<th style='width: 14%'>".$Lang['SLRS']['ReportDate']."</th>";
		if ($kk == "NotAvailableTeacher")
		{
			$x .= "<th>".$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"]."</th></tr>";
		}
		else
		{
			$x .= "<th style='width: 9%'>".$Lang['SLRS']['ReportDay']."</th>";
			$x .= "<th style='width: 9%'>".$Lang['SLRS']['ReportPeriod']."</th>";
			$x .= "<th style='width: 12%'>".$Lang['SLRS']['ReportSubject']."</th>";
			$x .= "<th style='width: 9%'>".$Lang['SLRS']['ReportClass']."</th>";
			$x .= "<th style='width: 9%'>".$Lang['SLRS']['ReportSubTC']."</th>";
			$x .= "<th style='width: 4%'>".$Lang['SLRS']['ReportVolunteered']."</th>";
			$x .= "<th style='width: 20%'>".$Lang['SLRS']['ReportExemption']."</th></tr>";
		}
		
		for($i=0; $i<sizeof($info);$i++)
		{
// 			if($reasonCode != $info[$i]["ReasonCode"] && $reasonCode=="")
// 			{
// 				$x .= "</table>"."\r\n";
// 			}
// 			else if($reasonCode != $info[$i]["ReasonCode"] && $reasonCode!=""){
// 				$x .= "</table>"."\r\n";
// 				$x .= "<br/>";
// 				$index=0;
// 			}
			//else{
			$x .= "<tr>"."\r\n";
			$x .= "<td style=''>".$info[$i]["UserName"]."</td>"."\r\n";
			$x .= "<td style=''>".$info[$i]["DateLeave"]."</td>"."\r\n";
			if ($info[$i]["ArrangedTo_UserID"] == "NOT_AVAILABLE")
			{
				$x .= "<td style='' >". nl2br($info[$i]["ReasonCode"])."</td>"."\r\n";
			}
			else if (empty($info[$i]["LessonArrangementID"]) && (!isset($info[$i]["LessonExchangeID"]) || empty($info[$i]["LessonExchangeID"])))
			{
				## IGNORE this condition
				$x .= "<td colspan='7' style=' color:#f00' >". $Lang['SLRS']['SubstitutionArrangementDes']['notArranged'] ."</td>"."\r\n";
			}
			else
			{
				$x .= "<td>".$info[$i]["CycleDay"]."</td>"."\r\n";
				$x .= "<td>".$info[$i]["TimeSlotName"]."</td>"."\r\n";
				$x .= "<td>".$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupID"],"subjectName")."</td>"."\r\n";
				$x .= "<td>".$indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupID"])."</td>"."\r\n";

				if ($info[$i]["ArrangedTo_UserID"] < 0 || empty($info[$i]["ArrangedTo_UserID"]))
				{
					if (isset($info[$i]["LessonExchangeID"]) && !empty($info[$i]["LessonExchangeID"]))
					{
						$x .= "<td style=' color:#f00;'>". $Lang['SLRS']['SubstitutionArrangementDes']['isExchangeRecord'] ."</td>"."\r\n";
					}
					else
					{
						$x .= "<td style=' color:#f00;'>". $Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'] ."</td>"."\r\n";
					}
				}
				else
				{
					$x .= "<td style=''>".$info[$i]["AssignedUserName"]."</td>"."\r\n";
				}
				
				$x .= "<td>".(($info[$i]["isVolunteer"]==1)?"✔":"-")."</td>"."\r\n";
				$x .= "<td></td>"."\r\n";
			}
			
			$x .= "</tr>"."\r\n";
			//}
			$index++;
			$reasonCode=$info[$i]["ReasonCode"];
		}
		$x .= "</table>"."\r\n";
		$x .= "<br/>";
	}
}
$x .= "</table>"."\r\n";
$x .= "<br/>";



echo $x;












?>