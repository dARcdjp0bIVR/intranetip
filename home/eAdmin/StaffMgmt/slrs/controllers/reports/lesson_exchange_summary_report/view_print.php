<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

function checkEmpty($value){
    return (($value==""||$value==null)?"-":$value);
}

function convertMultipleRowsIntoOneRow($arr,$fieldName){
    $x = "";
    for($i=0; $i<sizeof($arr);$i++){
        if($i==sizeof($arr)-1){
            $x .= $arr[$i][$fieldName];
        }
        else{
            $x .= $arr[$i][$fieldName].",";
        }
    }
    if(sizeof($arr)==0){
        $x = "''";
    }
    return $x;
}

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
$startDate = $_GET["startDate"];
$endDate = $_GET["endDate"];

$sql = "SELECT TeacherA_Date
			FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE TeacherA_Date BETWEEN '".$startDate ."' AND '".$endDate ."'";
$a = $connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	$dateArr[$i]=$a[$i]["TeacherA_Date"];
}

$sql = "SELECT
            GroupID,SequenceNumber,LessonExchangeID,TeacherA_UserID,UserNameA,TeacherB_UserID,UserNameB,TeacherA_Date,TeacherB_Date,
		      itt.TimeSlotName,ClassTitleAEN,ClassTitleAB5,ClassTitleBEN,ClassTitleBB5,NameChiA,NameEngA,NameChiB,NameEngB,
		      stca.SubjectGroupID as SubjectGroupIDA,stcb.SubjectGroupID as SubjectGroupIDB,CodeA,CodeB,itt.TimeSlotID,CycleDay,
              TeacherB_TimeSlotID, TimeSlotNameB
		FROM (
			SELECT
                LessonExchangeID,TeacherA_UserID,TeacherA_Date,TeacherA_SubjectGroupID,TeacherA_SubjectID,TeacherA_LocationID,TeacherA_TimeSlotID,
			    TeacherB_UserID,TeacherB_Date,TeacherB_SubjectGroupID,TeacherB_SubjectID,TeacherB_LocationID,TeacherB_TimeSlotID,GroupID,SequenceNumber
			FROM
                INTRANET_SLRS_LESSON_EXCHANGE
            WHERE
                TeacherA_Date BETWEEN '".$startDate ."' AND '".$endDate ."'
		) as isle
		LEFT JOIN(
			SELECT UserID,".$name_field." as UserNameA FROM ".$slrsConfig['INTRANET_USER']."
		) as iua ON isle.TeacherA_UserID =iua.UserID
		LEFT JOIN(
			SELECT UserID,".$name_field." as UserNameB FROM ".$slrsConfig['INTRANET_USER']."
		) as iub ON isle.TeacherB_UserID =iub.UserID
		LEFT JOIN(
			SELECT TimeSlotID,TimeSlotName FROM INTRANET_TIMETABLE_TIMESLOT
		) as itt ON isle.TeacherA_TimeSlotID=itt.TimeSlotID
		LEFT JOIN(
			SELECT TimeSlotID as TimeSlotIDB,TimeSlotName as TimeSlotNameB FROM INTRANET_TIMETABLE_TIMESLOT
		) as ittB ON isle.TeacherB_TimeSlotID=TimeSlotIDB
		LEFT JOIN(
			SELECT SubjectGroupID,ClassTitleEN as ClassTitleAEN,ClassTitleB5 as ClassTitleAB5 FROM SUBJECT_TERM_CLASS
		) as stca ON isle.TeacherA_SubjectGroupID=stca.SubjectGroupID
		LEFT JOIN(
			SELECT SubjectGroupID,ClassTitleEN as ClassTitleBEN,ClassTitleB5 as ClassTitleBB5 FROM SUBJECT_TERM_CLASS
		) as stcb ON isle.TeacherB_SubjectGroupID=stcb.SubjectGroupID
		LEFT JOIN(
			SELECT LocationID,NameChi as NameChiA,NameEng as NameEngA,Code as CodeA FROM INVENTORY_LOCATION
		) as ila ON isle.TeacherA_LocationID = ila.LocationID
		LEFT JOIN(
			SELECT LocationID,NameChi as NameChiB,NameEng as NameEngB,Code as CodeB FROM INVENTORY_LOCATION
		) as ilb ON isle.TeacherB_LocationID = ilb.LocationID
		LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON isle.TeacherA_Date=icd.RecordDate
		ORDER BY TeacherA_Date DESC,GroupID,SequenceNumber;";
//echo $sql;
$info = $connection->returnResultSet($sql);

$x .= "<div align=\"right\">"."\r\n";
$x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['SLRS']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
$x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
	<img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['SLRS']['ReportProcessing']."</span></span></div>";
$x .= "</div>"."\r\n";


$x .= "<table>"."\r\n";
$x .= "<tr><td colspan=4><h3>".$Lang['SLRS']['ReportLessonExchangeSummary']."</h3></td></tr>"."\r\n";
// $x .= "<tr><td colspan=3>".$date."</td></tr></table><br/>"."\r\n";
$x .= "<tr>";
$x .= "<td>" . $Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date'] . ":</td>";
$x .= "<td>" . $startDate . "</td>";
$x .= '<td>-</td>';
$x .= "<td>" . $endDate . "</td>";
$x .= "</tr>";
$x .= "</table><br>"."\r\n";


$x .= "<table class=\"common_table_list\">"."\r\n";
$x .= "<thead>";
$x .= "<tr><th>".$Lang['SLRS']['ReportDate']."</th><th>".$Lang['SLRS']['ReportDay']."</th><th>".$Lang['SLRS']['ReportClass']."</th><th>".$Lang['SLRS']['ReportPeriod']."</th><th>".$Lang['SLRS']['ReportTeacher']."</th>"."\r\n";;
$x .= "<th>".$Lang['SLRS']['ReportSubject']."</th><th>".$Lang['SLRS']['ReportRoom']."</th></tr>"."\r\n";
$x .= "</thead>";
$x .= "<tbody>";
for($i=0;$i<sizeof($info);$i++){
	$x .= "<tr>"."\r\n";
	// $x .="<td>".(($info[$i]["TeacherA_Date"]==$info[$i]["TeacherB_Date"])?$info[$i]["TeacherA_Date"]:($info[$i]["TeacherA_Date"]." ".$Lang['SLRS']['ReportTo']." ".$info[$i]["TeacherB_Date"]))."</td>"."\r\n";
	$x .="<td>".(($info[$i]["TeacherA_Date"]==$info[$i]["TeacherB_Date"])?$info[$i]["TeacherA_Date"]:($info[$i]["TeacherA_Date"]." > ".$info[$i]["TeacherB_Date"]))."</td>"."\r\n";
	$x .="<td>".$info[$i]["CycleDay"]."</td>"."\r\n";
	// $x .="<td>".$indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupIDA"])."</td>"."\r\n";
	$yearClassIDSQL = $indexVar['libslrs']->getYearClassID($info[$i]["TeacherA_Date"]);
	//$yearClass = $yearClassIDSQL[0]["YearClassID"];
	$yearClass = convertMultipleRowsIntoOneRow($yearClassIDSQL,"YearClassID");
	
	$classNameInfo = $indexVar['libslrs']->getClassInfoBySubjectGroupID($info[$i]["SubjectGroupIDA"], $yearClass);
	
	if (empty($classNameInfo) || count($classNameInfo) == 0)
	{
	    $x .= "<td>".$indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupIDA"])."</td>"."\r\n";
	}
	else
	{
	    $str_class_name = "";
	    foreach ($classNameInfo as $_class_key => $_class_val)
	    {
	        if (!empty($str_class_name))
	        {
	            $str_class_name .= ", ";
	        }
	        $str_class_name .= Get_Lang_Selection(
	            (
	                $indexVar['libslrs']->isEJ() ? convert2unicode($_class_val["ClassTitleB5"],1,1) : $_class_val["ClassTitleB5"]
	                ),
	            $_class_val["ClassTitleEN"]
	            );
	    }
	    $x .= "<td>".checkEmpty($str_class_name) . "</td>"."\r\n";
	}
	// $x .="<td>".$info[$i]["TimeSlotName"]."</td>"."\r\n";
	$x .="<td>";
	if ($indexVar['libslrs']->isEJ()) {
	    $x .= convert2unicode($info[$i]["TimeSlotName"]);
    	if (!empty($info[$i]["TimeSlotNameB"]) && $info[$i]["TimeSlotNameB"] != $info[$i]["TimeSlotName"])
    	{
    	    $x .=  " > " . convert2unicode($info[$i]["TimeSlotNameB"]);
    	}
	} else {
	    $x .= $info[$i]["TimeSlotName"];
	    if (!empty($info[$i]["TimeSlotNameB"]) && $info[$i]["TimeSlotNameB"] != $info[$i]["TimeSlotName"])
	    {
	        $x .=  " > " . $info[$i]["TimeSlotNameB"];
	    }
	}
	
	$x .= "</td>"."\r\n";
	// $x .="<td>".$info[$i]["UserNameA"]." > ".$info[$i]["UserNameB"]."</td>"."\r\n";
	$x .="<td>".$info[$i]["UserNameB"]." > ".$info[$i]["UserNameA"]."</td>"."\r\n";
	
	$othersLocationA = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDA"],$info[$i]["TeacherA_Date"]);
	$othersLocationB = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDB"],$info[$i]["TeacherB_Date"]);
	
// 	$x .="<td>".$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"")
// 		." > ".
// 		$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDB"],"")."</td>"."\r\n";
// 	$x .="<td>".(Get_Lang_Selection($info[$i]["NameChiA"],$info[$i]["NameEngA"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeA"],$info[$i]["NameChiA"],$info[$i]["NameEngA"])) :$othersLocationA)
// 		." > ".
// 		(Get_Lang_Selection($info[$i]["NameChiB"],$info[$i]["NameEngB"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeB"],$info[$i]["NameChiB"],$info[$i]["NameEngB"])) :$othersLocationB)."</td>"."\r\n";
	$x .="<td>".$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDB"],"")
		." > ".
    	$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"")."</td>"."\r\n";
	$x .="<td>".(Get_Lang_Selection($info[$i]["NameChiB"],$info[$i]["NameEngB"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeB"],$info[$i]["NameChiB"],$info[$i]["NameEngB"])) :$othersLocationB)
		." > ".
		(Get_Lang_Selection($info[$i]["NameChiA"],$info[$i]["NameEngA"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeA"],$info[$i]["NameChiA"],$info[$i]["NameEngA"])) :$othersLocationA)."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
}
$x .= "</tbody>";
$htmlAry['contentTbl'] = $x;

?>