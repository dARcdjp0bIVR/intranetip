<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$x ="<table class=\"form_table slrs_form\">"."\r\n";
	$x .="<tr>"."\r\n";
		$x .= "<td>".$indexVar['libslrs_ui']->RequiredSymbol().$Lang['SLRS']['ReportAcademicYear']."</td><td>:</td>"."\r\n";
			$x .= "<td>"
			.getSelectByAssoArray(cnvAcademicYearArrToSelect($indexVar['libslrs']->getAcademicYear($academicID)), $selectionTags='id="academicYearID" name="academicYearID" onChange=""', $SelectedType="", $all=0, $noFirst=0)
			."</td>"."\r\n";
			$x .=  $indexVar['libslrs_ui']->Get_Form_Warning_Msg('datePickerTbEmptyWarnDiv', $Lang['SLRS']['ReportDateWarning'], $Class='warnMsgDiv')."\r\n";
	$x .="</tr>"."\r\n";
	$x .="<tr>"."\r\n";
		$x .= "<td>".$indexVar['libslrs_ui']->RequiredSymbol().$Lang['SLRS']['ReportSemester']."</td><td>:</td>"."\r\n";
		$x .= "<td>"
			."<div id='semesterDiv'></div>"
			."</td>"."\r\n";
			$x .=  $indexVar['libslrs_ui']->Get_Form_Warning_Msg('datePickerTbEmptyWarnDiv', $Lang['SLRS']['ReportDateWarning'], $Class='warnMsgDiv')."\r\n";
	$x .="</tr>"."\r\n";
$x .="</table>"."\r\n";
$x .="<span>".sprintf($Lang['SLRS']['ReportWarning'],$indexVar['libslrs_ui']->RequiredSymbol())."</span>";

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['SLRS']['ReportGeneration'], "submit", "goRptGen()", 'genRptBtn');
// ============================== Define Button ==============================
function cnvAcademicYearArrToSelect($arr){
	$oAry = array();
	for($_i=0; $_i<sizeof($arr); $_i++){
		$oAry[$arr[$_i]["AcademicYearID"]] = Get_Lang_Selection($arr[$_i]["YearNameB5"],$arr[$_i]["YearNameEN"]);
	}
	return $oAry;
}
?>