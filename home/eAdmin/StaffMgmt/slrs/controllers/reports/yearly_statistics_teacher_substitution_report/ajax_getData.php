<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();

$academicYearID = $_POST["academicYearID"];
$semester = $_POST["semester"];
$type = $_POST["type"];
$semsterArray = explode(',',$semester);

if($type=="academicYearTerm"){
	$sql="SELECT YearTermID,AcademicYearID,YearTermNameEN,YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=".IntegerSafe($academicYearID)." ORDER BY TermStart ASC;";
	$info=$connection->returnResultSet($sql);	
	$arr = array();
	$jsonObj = new JSON_obj();
	for ($i=0; $i<sizeof($info); $i++){
		$arr[] = array(
				'YearTermID' => $info[$i]["YearTermID"], 
				'Name' => Get_Lang_Selection( 
						(
							$indexVar['libslrs']->isEJ() ? 
							convert2unicode($info[$i]["YearTermNameB5"],1,1) : 
							$info[$i]["YearTermNameB5"]
						), 
						$info[$i]["YearTermNameEN"])
		);
	}
	echo $jsonObj->encode($arr);
}
else if($type=="info"){
	
	$reportData = $indexVar['libslrs']->getYearStatSubstitutionReportData($academicYearID, $semester, $semsterArray, $filterNoAffectLeaveReason=true);
	
	$btnAry = array();
	$btnAry[] = array('export', 'javascript: export_csv();');
	$btnAry[] = array('print', 'javascript: print_timetable();');
	
	$x = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	$x .= "<br/><br/>";
	
	$x .= "<table>"."\r\n";
	$x .= "<tr><td colspan=3>".$Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution']."</td></tr>"."\r\n";
	$x .= "</table><br/>"."\r\n";
	
	$x .= "<table class=\"common_table_list\">"."\r\n";
	$x .= "<tr><th style='width:20%'>".$Lang['SLRS']['ReportTC']."</th>";
	$x .= "<th style='width:20%'>".$Lang['SLRS']['ReportPeriodSickLeave']."</th>";
	$x .= "<th style='width:20%'>".$Lang['SLRS']['ReportPeriodSubstitition']."</th>";
	$x .= "<th style='width:20%'>".$Lang['SLRS']['ReportCarriedPreviousYear']."</th>";
	$x .= "<th style='width:20%'>".$Lang['SLRS']['ReportBalance']."</th>";
	// $x .= "<th>".$Lang['SLRS']['ReportBalance']."<br>** 不包括'臨時代課安排'及'假期分類及影響'</th>";
	$x .= "</tr>"."\r\n";
	
	// 	for($i=0;$i<sizeof($info);$i++){
	// 		$x .= "<tr>"."\r\n";
	// 		$x .="<td>".$info[$i]["UserName"]."</td>"."\r\n";
	// 		$x .="<td>".$info[$i]["CntLeave"]."</td>"."\r\n";
	// 		$x .="<td>".$info[$i]["CntSubstitution"]."</td>"."\r\n";
	// 		$x .="<td>".$info[$i]["CntPreYear"]."</td>"."\r\n";
	// 		$x .="<td>".$info[$i]["Balance"]."</td>"."\r\n";
	// 		$x .= "</tr>"."\r\n";
	// 	}
	// $x .= "</table>"."\r\n";
	
	
	if (count($reportData) > 0)
	{
		foreach ($reportData as $_UserID => $rData) {
			$x .= "<tr>"."\r\n";
			$x .="<td>".$rData["UserName"]."</td>"."\r\n";
			$x .="<td>". (empty($rData["CntLeave"]) ? '--' : $rData["CntLeave"]) ."</td>"."\r\n";
			$x .="<td>". (empty($rData["SubstituteUser"]) ? '--' : $rData["SubstituteUser"]) ."</td>"."\r\n";
			$x .="<td>". (empty($rData["BalanceAdjustTo"]) ? '0' : $rData["BalanceAdjustTo"]) ."</td>"."\r\n";
			$x .="<td>". $rData["balance"] ."</td>"."\r\n";
			$x .= "</tr>"."\r\n";
		}
	}
	
	
	echo $x;
}


?>