<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();
/*$headerAry[] = $Lang['SLRS']['ReportPeriod'];
$headerAry[] = $Lang['SLRS']['ReportAssignedTo'];
$headerAry[] = $Lang['SLRS']['ReportClass'];
$headerAry[] = $Lang['SLRS']['ReportSubject'];
$headerAry[] = $Lang['SLRS']['ReportRoom'];
*/

// build content data array
$dataAry = array();

$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
$academicYearID = $_GET["academicYearID"];
$semester = $_GET["semester"];
$semsterArray = explode(',',$semester);

$sql = "SELECT TermStart,TermEnd
			FROM(
				SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE AcademicYearID=".IntegerSafe($academicYearID)."
			)as ay
			INNER JOIN(
				SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$semester.")
			) as ayt ON ay.AcademicYearID=ayt.AcademicYearID
			ORDER BY TermStart
	";
//echo $sql;
$info=$connection->returnResultSet($sql);
$startDate=$info[0]["TermStart"];
$endDate=$info[sizeof($info)-1]["TermEnd"];

$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");

/*
$sql="
		SELECT UserID,UserName,CntLeave,CntSubstitution,CntPreYear,(CntSubstitution-CntLeave+CntPreYear) as Balance
			FROM(
			SELECT ist.UserID,UserName,COALESCE(CntLeave,'--') as CntLeave,COALESCE(CntSubstitution,'--') as CntSubstitution,COALESCE(BalanceAdjustTo,'--') as CntPreYear
			FROM(
				SELECT UserID FROM INTRANET_SLRS_TEACHER
			) as ist
			LEFT JOIN(
				SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
				GROUP BY UserID
			) as isl ON ist.UserID=isl.UserID
			LEFT JOIN(
				SELECT a0.UserID,SubstituteUser as CntSubstitution,CntLeave,BalanceAdjustTo
				FROM( 
					SELECT UserID FROM INTRANET_SLRS_LEAVE WHERE DateLeave BETWEEN '".$startDate."' AND '".$endDate."'
					GROUP BY UserID 
				) as a0 
				INNER JOIN( 
					SELECT ArrangedTo_UserID,COUNT(ArrangedTo_UserID) as SubstituteUser
					FROM INTRANET_SLRS_LESSON_ARRANGEMENT GROUP BY ArrangedTo_UserID 
				) as a1 ON a0.UserID=a1.ArrangedTo_UserID
				INNER JOIN(
					SELECT UserID,COUNT(TimeSlotID) as CntLeave
					FROM INTRANET_SLRS_LESSON_ARRANGEMENT GROUP BY UserID 		
				) as a2 ON a0.UserID=a2.UserID
				LEFT JOIN(
					SELECT MIN(DateTimeInput) as DateTimeInput,UserID,BalanceAdjustID,BalanceAdjustTo
					FROM(
						SELECT YearTermID,AcademicYearID,TermStart,TermEnd FROM ACADEMIC_YEAR_TERM WHERE YearTermID IN (".$semsterArray[0].") 
					) as ayt
					INNER JOIN(
						SELECT UserID,BalanceAdjustTo,DateTimeInput,BalanceAdjustID FROM INTRANET_SLRS_BALANCE_ADJUST
					) as isba ON isba.DateTimeInput BETWEEN TermStart AND TermEnd
					GROUP BY UserID
				) as a3 ON a0.UserID=a3.UserID					
			) as a ON ist.UserID=a.UserID
			LEFT JOIN(
				SELECT UserID,".$name_field." as UserName FROM ".$slrsConfig['INTRANET_USER']."
			) as iu ON ist.UserID =iu.UserID						
		) as a0
	";
//echo $sql;
$info=$connection->returnResultSet($sql);
*/
$reportData = $indexVar['libslrs']->getYearStatSubstitutionReportData($academicYearID, $semester, $semsterArray, $filterNoAffectLeaveReason=true);

$dataAry[0][0]=$Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution'];
$dataAry[1][0]="";

$dataAry[2][0]=$Lang['SLRS']['ReportTC'];
$dataAry[2][1]=$Lang['SLRS']['ReportPeriodSickLeave'];
$dataAry[2][2]=$Lang['SLRS']['ReportPeriodSubstitition'];
$dataAry[2][3]=$Lang['SLRS']['ReportCarriedPreviousYear'];
$dataAry[2][4]=$Lang['SLRS']['ReportBalance'];


$index = 3;
/*
for($i=0;$i<sizeof($info);$i++){
	$dataAry[$i+$index][0]=$info[$i]["UserName"];
	$dataAry[$i+$index][1]=$info[$i]["CntLeave"];
	$dataAry[$i+$index][2]=$info[$i]["CntSubstitution"];
	$dataAry[$i+$index][3]=$info[$i]["CntPreYear"];
	$dataAry[$i+$index][4]=$info[$i]["Balance"];
}
*/
if (count($reportData) > 0)
{
	foreach ($reportData as $_userID => $rData)
	{
		$dataAry[$index][0] = $rData["UserName"];
		$dataAry[$index][1] = $rData["CntLeave"];
		$dataAry[$index][2] = $rData["SubstituteUser"];
		$dataAry[$index][3] = $rData["BalanceAdjustTo"];
		$dataAry[$index][4] = $rData["balance"];
		$index++;
	}
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'yearly_statistics_teacher_substitution_report.csv';
// $lexport->EXPORT_FILE($fileName, $exportContent);
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');

?>