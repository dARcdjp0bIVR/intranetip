<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ReportSummaryofClassroomChangeforLessons']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$x ="<table class=\"form_table slrs_form\">"."\r\n";
	$x .="<tr>"."\r\n";
		$x .= "<td>".$indexVar['libslrs_ui']->RequiredSymbol().$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date']."</td><td>:</td>"."\r\n";
			$x .= "<td>"
			.$indexVar['libslrs_ui']->GET_DATE_PICKER('startDate', $date,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")
			."~"
			.$indexVar['libslrs_ui']->GET_DATE_PICKER('endDate', $date,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")
			."</td>"."\r\n";
			$x .=  $indexVar['libslrs_ui']->Get_Form_Warning_Msg('datePickerTbEmptyWarnDiv', $Lang['SLRS']['ReportDateWarning'], $Class='warnMsgDiv')."\r\n";
	$x .="</tr>"."\r\n";
$x .="</table>"."\r\n";
$x .="<span>".sprintf($Lang['SLRS']['ReportWarning'],$indexVar['libslrs_ui']->RequiredSymbol())."</span>";

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['SLRS']['ReportGeneration'], "submit", "goRptGen()", 'genRptBtn');
// ============================== Define Button ==============================

?>