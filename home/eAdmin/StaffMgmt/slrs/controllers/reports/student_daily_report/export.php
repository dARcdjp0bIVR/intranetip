<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build header array
$headerAry = array();
/*$headerAry[] = $Lang['SLRS']['ReportPeriod'];
$headerAry[] = $Lang['SLRS']['ReportAssignedTo'];
$headerAry[] = $Lang['SLRS']['ReportClass'];
$headerAry[] = $Lang['SLRS']['ReportSubject'];
$headerAry[] = $Lang['SLRS']['ReportRoom'];
*/

// build content data array
$dataAry = array();

$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".");
$startDate = $_GET["startDate"];
$endDate = $_GET["endDate"];

$sql = "SELECT TeacherA_Date
			FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE TeacherA_Date like '".$startDate ."'
		UNION
		SELECT DATE_FORMAT(TimeStart,'%Y-%m-%d') as TeacherA_Date
			FROM 
		INTRANET_SLRS_LESSON_ARRANGEMENT WHERE DATE_FORMAT(TimeStart,'%Y-%m-%d') like '".$startDate ."'";
$a = $connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	$dateArr[$i]=$a[$i]["TeacherA_Date"];
}

$sql = "SELECT
			GroupID, SequenceNumber, LessonRemarks, LessonExchangeID, LessonArrangementID, TeacherA_UserID, UserNameA, TeacherB_UserID, UserNameB, TeacherA_Date, TeacherA_LocationID,
			TeacherB_Date, TeacherB_LocationID, CycleDay, TimeSlotName, ClassTitleAEN, ClassTitleAB5, ClassTitleBEN, ClassTitleBB5,
			NameChiA, NameEngA, NameChiB, NameEngB, CycleDay, TeacherA_SubjectGroupID as SubjectGroupIDA,
			TeacherB_SubjectGroupID as SubjectGroupIDB, CodeA, CodeB, laID
		FROM (
			SELECT
				0 as LessonArrangementID, LessonExchangeID, TeacherA_UserID, TeacherA_Date, TeacherA_SubjectGroupID, TeacherA_SubjectID, TeacherA_LocationID, TeacherA_TimeSlotID,
				TeacherB_UserID, TeacherB_Date, TeacherB_SubjectGroupID, TeacherB_SubjectID, TeacherB_LocationID, TeacherB_TimeSlotID, GroupID,
				SequenceNumber, LessonRemarks, laID
			FROM
				INTRANET_SLRS_LESSON_EXCHANGE
			WHERE
				TeacherB_Date = '".$startDate ."' AND (laID IS NULL)
			UNION
				SELECT
					LessonArrangementID as LessonArrangementID, 0 as LessonExchangeID, UserID as TeacherA_UserID, DATE_FORMAT(TimeStart,'%Y-%m-%d') as TeacherA_Date,
					SubjectGroupID as TeacherA_SubjectGroupID, SubjectID as TeacherA_SubjectID, LocationID as TeacherA_LocationID,
					TimeSlotID as TeacherA_TimeSlotID,ArrangedTo_UserID as TeacherB_UserID, DATE_FORMAT(TimeStart,'%Y-%m-%d') as TeacherB_Date,
					SubjectGroupID as TeacherB_SubjectGroupID, SubjectID as TeacherB_SubjectID, ArrangedTo_LocationID as TeacherB_LocationID,
					TimeSlotID as TeacherB_TimeSlotID, NULL as GroupID, 1 as SequenceNUmber, LessonRemarks, Null as laID
				FROM
					INTRANET_SLRS_LESSON_ARRANGEMENT
				WHERE
					DATE_FORMAT(TimeStart,'%Y-%m-%d')='".$startDate ."'
		) as isle
		LEFT JOIN(
			SELECT
				UserID,".$name_field." as UserNameA
			FROM
				".$slrsConfig['INTRANET_USER']."
		) as iua ON isle.TeacherA_UserID =iua.UserID
		LEFT JOIN(
			SELECT
				UserID,".$name_field." as UserNameB
			FROM
				".$slrsConfig['INTRANET_USER']."
		) as iub ON isle.TeacherB_UserID =iub.UserID
		LEFT JOIN(
			SELECT
				TimeSlotID, TimeSlotName
			FROM
				INTRANET_TIMETABLE_TIMESLOT
		) as itt ON isle.TeacherA_TimeSlotID=itt.TimeSlotID
		LEFT JOIN(
			SELECT
				SubjectGroupID, ClassTitleEN as ClassTitleAEN, ClassTitleB5 as ClassTitleAB5
			FROM
				SUBJECT_TERM_CLASS
		) as stca ON isle.TeacherA_SubjectGroupID=stca.SubjectGroupID
		LEFT JOIN(
			SELECT
				SubjectGroupID, ClassTitleEN as ClassTitleBEN, ClassTitleB5 as ClassTitleBB5
			FROM
				SUBJECT_TERM_CLASS
		) as stcb ON isle.TeacherB_SubjectGroupID=stcb.SubjectGroupID
		LEFT JOIN(
			SELECT
				LocationID, NameChi as NameChiA, NameEng as NameEngA, Code as CodeA
			FROM
				INVENTORY_LOCATION
		) as ila ON isle.TeacherA_LocationID = ila.LocationID
		LEFT JOIN(
			SELECT
				LocationID, NameChi as NameChiB, NameEng as NameEngB, Code as CodeB
			FROM
				INVENTORY_LOCATION
		) as ilb ON isle.TeacherB_LocationID = ilb.LocationID
		LEFT JOIN (
			".$indexVar['libslrs']->getDaySQL($dateArr,"")."
		) as icd ON isle.TeacherA_Date=icd.RecordDate
		ORDER BY TeacherA_Date DESC,GroupID,SequenceNumber;";
//echo $sql;
$info = $connection->returnResultSet($sql);
$index = 0;
$dataAry[$index][0]=$Lang['SLRS']['StudentDailyReport'];
$index++;
$dataAry[$index][0]=$startDate;
$index++;
$dataAry[$index][0]="";
$index++;

$subIndex = 0;
$dataAry[$index][$subIndex]=$Lang['SLRS']['ReportClass'];
$subIndex++;
$dataAry[$index][$subIndex]=$Lang['SLRS']['ReportPeriod'];
$subIndex++;
$dataAry[$index][$subIndex]=$Lang['SLRS']['ReportTeacher'];
$subIndex++;
$dataAry[$index][$subIndex]=$Lang['SLRS']['ReportSubject'];
$subIndex++;
$dataAry[$index][$subIndex]=$Lang['SLRS']['ReportRoom'];
$subIndex++;
$dataAry[$index][$subIndex]=$Lang['SLRS']['LessonRemarks'];
$subIndex++;

$index++;

$locationSelection = $indexVar['libslrs']->getAllLocationSelection($date = '', $timeSlotID = '', $keepLocationID = '', $withNotAvailableLocation = true);

for($i=0;$i<sizeof($info);$i++){
	$subIndex = 0;
	/*$dataAry[$i+$index][0]=(($info[$i]["TeacherA_Date"]==$info[$i]["TeacherB_Date"])?$info[$i]["TeacherA_Date"]:($info[$i]["TeacherA_Date"]." ".$Lang['SLRS']['ReportTo']." ".$info[$i]["TeacherB_Date"]));
	$dataAry[$i+$index][1]=$info[$i]["CycleDay"];*/
	
	$dataAry[$i+$index][$subIndex] = $indexVar['libslrs']->getYearClass($info[$i]["SubjectGroupIDA"]);
	$subIndex++;
	
	$dataAry[$i+$index][$subIndex] = $info[$i]["TimeSlotName"];
	$subIndex++;
	
	// $dataAry[$i+$index][$subIndex]=$info[$i]["UserNameA"].">".$info[$i]["UserNameB"];
	$dataAry[$i+$index][$subIndex] = (!empty($info[$i]["UserNameB"])) ? $info[$i]["UserNameB"] : '-';
	$subIndex++;
	/*
	$dataAry[$i+$index][$subIndex] = $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"subjectName") 
		.">".
		$indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDB"],"subjectName");
	*/
	$dataAry[$i+$index][$subIndex] = $indexVar['libslrs']->getSubjectName($info[$i]["SubjectGroupIDA"],"subjectName");
	$subIndex++;
	$othersLocationA = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDA"],$info[$i]["TeacherA_Date"]);
	$othersLocationB = $indexVar['libslrs']->getOthersLocation($info[$i]["TimeSlotID"],$info[$i]["SubjectGroupIDB"],$info[$i]["TeacherB_Date"]);
	
	/*
	
	$dataAry[$i+$index][$subIndex]=(Get_Lang_Selection($info[$i]["NameChiA"],$info[$i]["NameEngA"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeA"],$info[$i]["NameChiA"],$info[$i]["NameEngA"])) :$othersLocationA)
			.">".
			(Get_Lang_Selection($info[$i]["NameChiB"],$info[$i]["NameEngB"])?($indexVar['libslrs']->getLocationName($info[$i]["CodeB"],$info[$i]["NameChiB"],$info[$i]["NameEngB"])) :$othersLocationB);
	*/
	$dataAry[$i+$index][$subIndex] = (!empty($locationSelection[$info[$i]["TeacherB_LocationID"]]) ? $locationSelection[$info[$i]["TeacherB_LocationID"]] : $othersLocationB);
	$subIndex++;
	
	$dataAry[$i+$index][$subIndex] = ($info[$i]["LessonRemarks"] ? $info[$i]["LessonRemarks"] : "-");
	$subIndex++;
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'student_daily_report_' . str_replace("-", "", $startDate) . '.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, 'UTF-8');


?>