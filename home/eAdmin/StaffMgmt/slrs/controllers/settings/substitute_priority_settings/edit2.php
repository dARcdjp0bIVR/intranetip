<?php 

// ============================== Includes files/libraries ==============================
### check access right
include_once('libdb.php');
include_once('lib.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");


# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['SubstitutePrioritySettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

echo $indexVar['libslrs_ui']->Include_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$x = Get_Substitute_Priority_Detail($Lang);
//$y = Get_Substitute_Prioiry_Data(1);
//echo $y[0]["ConditionID"];
$htmlAry['contentTbl'] = $x;

$x = '';
$x .= '<table id="dndTable" class="common_table_list">'."\r\n";
$x .= '<thead>'."\r\n";
$x .= '<tr>'."\r\n";
$x .= '<th>Title</th>'."\r\n";
$x .= '<th>&nbsp;</th>'."\r\n";
$x .= '</tr>'."\r\n";
$x .= '</thead>'."\r\n";
$x .= '<tbody>'."\r\n";
for ($i=0; $i<10; $i++) {
	$_rowNum = $i + 1;
	$x .= '<tr id="row_'.$_rowNum.'">'."\r\n";
	$x .= '<td>row '.$_rowNum.'</td>'."\r\n";
	$x .= '<td class="Dragable"><div class="table_row_tool"><a href="#" class="move_order_dim" title="'.$Lang['SysMgr']['FormClassMapping']['Move'].'"></a></div></td>'."\r\n";
	$x .= '</tr>'."\r\n";
}
$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['dndTable'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================


function Get_Substitute_Priority_Detail($Lang){
	$x = '<div id="DetailLayer"><span>';
	$x .= Get_Substitue_Priority_Table($Lang);	
	$x .= '</span></div>';	
	return $x;
}

function Get_Substitue_Priority_Table($Lang){
	$x = '<table id="dndTable" class="common_table_list ClassDragAndDrop">'."\r\n";
	$x .= '<tr>'."\r\n";
	$x .= '<th class="sub_row_top" style="width:20%;">'.$Lang['SLRS']['SubstitutePrioritySettings'].'</th>'."\r\n";
		//$x .= '<th class="sub_row_top" style="width:10%;">'.$Lang['SLRS']['SubstitutePrioritySettingsDes']['Adjustment'].'</th>';
		$x .= '<th class="sub_row_top" style="width:70%;">'.$Lang['SLRS']['SubstitutePrioritySettingsDes']['SecondPriority'].'</th>'."\r\n";
		//$x .= '<th class="sub_row_top" style="width:20%;">'.$Lang['SLRS']['SubstitutePrioritySettingsDes']['UpperLimitmation'].'</th>';
		$x .= '<th class="sub_row_top" style="width:10%;">'.$Lang['SLRS']['SubstitutePrioritySettingsDes']['Adjustment'].'</th>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= Get_Substitute_Prioity_Data($Lang);
	$x .= '</table>'."\r\n";
	return $x;
}

function Get_Substitute_Prioity_Data($Lang){
	$displayOrder = 0;
	$connection = new libgeneralsettings();
	// get column 1 data
	$sql = "SELECT ConditionID,RowID,ColumnID,DisplayOrder,UpperLimitation ";
	$sql .= "FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY ";
	$sql .= "WHERE ColumnID=1 ";
	$sql .= "ORDER BY RowID,ColumnID,DisplayOrder";
	$sql .= ";";		
	$a = $connection->returnArray($sql);	
	$sql = "SELECT ConditionID,RowID,ColumnID,DisplayOrder,UpperLimitation ";
	$sql .= "FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY ";
	$sql .= "WHERE ColumnID=2 ";
	$sql .= "ORDER BY RowID,ColumnID,DisplayOrder";
	$sql .= ";";
	$b = $connection->returnArray($sql);
	for($i=0; $i<sizeof($a); $i++){		
		$x .= '<tbody>'."\r\n";
		$x .= '<tr class="nodrag nodrop">'."\r\n";
		$x .= '<td rowspan='.Get_Substitute_Prioity_Row($connection,$a[$i]["RowID"], "2").'>'.Get_Substitute_Prioity_Description($Lang, $a[$i]['ConditionID']).'</td>'."\r\n";
		$x .= '<td style="display:none;"></td><td style="display:none;"></td>'."\r\n";			
		$x .= '</tr>'."\r\n";
		$_count = 0;
		for($j=0; $j<sizeof($b); $j++){
			if($b[$j]["RowID"] == $a[$i]["RowID"]){
				$_count ++;
				$x .= '<tr class="sub_row" id="'.$b[$j]['ConditionID'].'">'."\r\n";
				$x .= '<td>'.Get_Substitute_Prioity_Description($Lang, $b[$j]['ConditionID']).'</td>'."\r\n";
				$x .= '<td class="Dragable"><div class="table_row_tool"><a href="#" class="move_order_dim" title="'.$Lang['SysMgr']['FormClassMapping']['Move'].'"></a></div></td>'."\r\n";
				$x.= '</tr>'."\r\n";
				if($_count == Get_Substitute_Prioity_Maximun_Order($connection,$b[$j]["RowID"],$b[$j]["ColumnID"])) {
					$x .='<tr class="nodrag nodrop"><td></td><td></td><td></td></tr>'."\r\n";					
				}				
			}			
		}		
		$x .= '</tbody>'."\r\n";
	}
	return $x;
}

function Get_Substitute_Prioity_Description($Lang, $i){
	switch($i){
		case "1":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['ExternalTeachers'];
			break;
		case "2":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['InternalTeachers'];
			break;
		case "3":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['DailyTotalLessons'];
			break;
		case "4":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['DailySubstituteLessons'];
			break;
		case "5":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['SubstituteBalance'];
			break;
		case "6":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['DailySubstituteLessons'];
			break;
		case "7":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['DailyTotalLessons'];
			break;
		case "8":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['SubstituteFilterOrder'];
			break;
		case "9":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['ClassTeacher'];
			break;
		case "10":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['SubjectTeacher'];
			break;
		case "11":
			$x = $Lang['SLRS']['SubstitutePrioritySettingsDes']['SameSubjectTeacher'];
			break;
	}
	return $x;
}

function Get_Substitute_Prioity_Row($connection,$rowID,$columnID){
	$sql = "SELECT Count(DisplayOrder) as DisplayOrder ";
	$sql .= "FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE RowID=".$rowID.' AND ColumnID='.$columnID.' ';
	$sql .= ";";
	$a = $connection->returnArray($sql);
	return $a[0]["DisplayOrder"]+1;
}

function Get_Substitute_Prioity_Maximun_Order($connection,$rowID,$columnID){
	$sql = "SELECT Count(DisplayOrder) as DisplayOrder ";
	$sql .= "FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE RowID=".$rowID.' AND ColumnID='.$columnID.' ';
	$sql .= ";";
	$a = $connection->returnArray($sql);	
	return $a[0]["DisplayOrder"];
}

	


?>

