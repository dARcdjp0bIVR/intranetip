<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['BasicSettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

$x = "";
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['BasicSettings']);
$x .= '<table class="form_table_v30">'."\r\n";	
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['CycleWeekMaximunLessons'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['CycleWeekMaximunLessonsDes'],null), $selectionTags='id="CycleWeekMaximunLessons" name="CycleWeekMaximunLessons"', $SelectedType=$data["CycleWeekMaximunLessons"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['DailyMaximunLessons'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['DailyMaximunLessonsDes'],null), $selectionTags='id="DailyMaximunLessons" name="DailyMaximunLessons"', $SelectedType=$data["DailyMaximunLessons"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['DailyMaximunSubstitution'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['DailyMaximunSubstitutionDes'],null), $selectionTags='id="DailyMaximunSubstitution" name="DailyMaximunSubstitution"', $SelectedType=$data["DailyMaximunSubstitution"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	/*
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SpecialClassroomLocation'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['SpecialClassroomLocationDes'],null), $selectionTags='id="SpecialClassroomLocation" name="SpecialClassroomLocation"', $SelectedType=$data["SpecialClassroomLocation"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionAfterSickLeave'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['SubstitutionAfterSickLeaveDes'],null), $selectionTags='id="SubstitutionAfterSickLeave" name="SubstitutionAfterSickLeave"', $SelectedType=$data["SubstitutionAfterSickLeave"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['FirstLessonSubstituion'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['FirstLessonSubstituionDes'],null), $selectionTags='id="FirstLessonSubstituion" name="FirstLessonSubstituion"', $SelectedType=$data["FirstLessonSubstituion"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
// 	$x .= '<tr>'."\r\n";
// 		$x .= '<td class="field_title">'.$Lang['SLRS']['FormSixTeacherSubstition'].'</td>'."\r\n";
// 		$x .= '<td>'."\r\n";
// 		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['FormSixTeacherSubstitionDes'],null), $selectionTags='id="FormSixTeacherSubstition" name="FormSixTeacherSubstition"', $SelectedType=$data["FormSixTeacherSubstition"], $all=0, $noFirst=1);
// 		$x .= '</td>'."\r\n";
// 	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['VoluntarySubstitute'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['VoluntarySubstituteDes'],null), $selectionTags='id="VoluntarySubstitute" name="VoluntarySubstitute"', $SelectedType=$data["VoluntarySubstitute"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['SLRS']['NoSubstitutionCalBalance'].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['NoSubstitutionCalBalanceDes'],null), $selectionTags='id="NoSubstitutionCalBalance" name="NoSubstitutionCalBalance"', $SelectedType=$data["NoSubstitutionCalBalance"], $all=0, $noFirst=1);
	$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
$x .= '</table>';
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['Display']);
$x .= '<table class="form_table_v30">'."\r\n";
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubjectDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['SubjectDisplayDes'],null), $selectionTags='id="SubjectDisplay" name="SubjectDisplay"', $SelectedType=$data["SubjectDisplay"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['LocationNameDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['LocationNameDisplayDes'],null), $selectionTags='id="LocationNameDisplay" name="LocationNameDisplay"', $SelectedType=$data["LocationNameDisplay"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	/*$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubjectGroupDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['SubjectGroupDisplayDes'],null), $selectionTags='id="SubjectGroupDisplay" name="SubjectGroupDisplay"', $SelectedType=$data["SubjectGroupDisplay"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";*/
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionBalanceDisplay'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
		$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['SubstitutionBalanceDisplayDes'],null), $selectionTags='id="SubstitutionBalanceDisplay" name="SubstitutionBalanceDisplay"', $SelectedType=$data["SubstitutionBalanceDisplay"], $all=0, $noFirst=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>';

if ($sys_custom['SLRS']["defaultToPresetLocation"]) {
	$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['PresetSubstitutionLocation']);
	$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['SLRS']['PresetSubstitutionLocationTH']["SubstitutionLimitPerDay"].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	
	$data["PSSubstitutionLimitPerDay"] = (!empty($data["PSSubstitutionLimitPerDay"])) ? $data["PSSubstitutionLimitPerDay"] : 2;
	
	$x .= getSelectByAssoArray(cnvArrToSelect($Lang['SLRS']['DailyMaximunSubstitutionDes'],null), $selectionTags='id="PSSubstitutionLimitPerDay" name="PSSubstitutionLimitPerDay"', $SelectedType=$data["PSSubstitutionLimitPerDay"], $all=0, $noFirst=1);
	$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$Lang['SLRS']['PresetSubstitutionLocationTH']["PresetLocation"].'</td>'."\r\n";
	$x .= '<td>'."\r\n";
	
	$AllLocationSelection = array(0 => $Lang['SLRS']['ClosedLocationTH']["please-select"]) + $indexVar["libslrs"]->getAllLocationSelection();
	$x .= getSelectByAssoArray($AllLocationSelection, $selectionTags='id="PresetLocation" name="PresetLocation"', $SelectedType=$data["PresetLocation"], $all=0, $noFirst=1);
	
	$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '</table>';
}

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ============================== 
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================

// ============================== Custom Function ==============================

function cnvArrToSelect($arrDes,$arrVal){
	$oAry = array();	
	if($arrVal == null){
		for($_i=0; $_i<sizeof($arrDes); $_i++){
			$oAry[$_i] = $arrDes[$_i];		
		}
	}
	else{
		for($_i=0; $_i<sizeof($arrDes); $_i++){
			$oAry[$arrVal[$_i]] = $arrDes[$_i];
		}
	}
	return $oAry;
}
// ============================== Custom Function ==============================

?>