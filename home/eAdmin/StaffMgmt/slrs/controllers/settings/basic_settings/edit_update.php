<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/json.php");
$lgeneralsettings = new libgeneralsettings();
$liblog = new liblog();
$JSON_obj = new JSON_obj();

$saveSuccess = true;

$data = array();
$data['CycleWeekMaximunLessons'] = IntegerSafe($_POST["CycleWeekMaximunLessons"]);
$data['DailyMaximunLessons'] = IntegerSafe($_POST["DailyMaximunLessons"]);
$data['DailyMaximunSubstitution'] = IntegerSafe($_POST["DailyMaximunSubstitution"]);
$data['SpecialClassroomLocation'] = IntegerSafe($_POST["SpecialClassroomLocation"]);
$data['SubstitutionAfterSickLeave'] = IntegerSafe($_POST["SubstitutionAfterSickLeave"]);
$data['FirstLessonSubstituion'] = IntegerSafe($_POST["FirstLessonSubstituion"]);
// $data['FormSixTeacherSubstition'] = IntegerSafe($_POST["FormSixTeacherSubstition"]);
$data['VoluntarySubstitute'] = IntegerSafe($_POST["VoluntarySubstitute"]);
$data['NoSubstitutionCalBalance'] = IntegerSafe($_POST["NoSubstitutionCalBalance"]);
$data['SubjectDisplay'] = IntegerSafe($_POST["SubjectDisplay"]);
$data['LocationNameDisplay'] = IntegerSafe($_POST["LocationNameDisplay"]);
$data['SubjectGroupDisplay'] = IntegerSafe($_POST["SubjectGroupDisplay"]);
$data['SubstitutionBalanceDisplay'] = IntegerSafe($_POST["SubstitutionBalanceDisplay"]);
$data['PSSubstitutionLimitPerDay'] = IntegerSafe($_POST["PSSubstitutionLimitPerDay"]);
$data['PresetLocation'] = IntegerSafe($_POST["PresetLocation"]);

$lgeneralsettings->Save_General_Setting("SLRS", $data);

$liblog->INSERT_LOG($Module='SLRS', $Section='BASIC_SETTING_UPDATE', $RecordDetail=$JSON_obj->encode($data));


$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$slrsConfig['taskSeparator'].'basic_settings'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>