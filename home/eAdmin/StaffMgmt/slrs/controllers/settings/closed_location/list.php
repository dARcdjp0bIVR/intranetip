<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['ClosedLocation']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ==============================
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

// ============================== Transactional data ==============================

$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 5 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$args = array(
		'table' => 'INTRANET_SLRS_NOT_AVAILABLE_LOCATION',
		"page_size" => $page_size,
		"keyword" => $keyword
);
$li = new libdbtable2007($field, $order, $pageNo);

$gridInfo = $indexVar['libslrs']->getTableGridInfo($args);

if (isset($gridInfo["fields"]) && is_array($gridInfo["fields"])) $li->field_array = $gridInfo["fields"];
if (isset($gridInfo["sql"]) && !empty($gridInfo["sql"])) $li->sql = $gridInfo["sql"];
if (isset($gridInfo["order2"]) && !empty($gridInfo["order2"])) $li->fieldorder2 = $gridInfo["order2"];

$args = array(
		"sql" => $li->built_sql(),
		"recIDArr" => "LocationRecIdAry",
		"recID" => "NA_Location_ID",
		"girdInfo" => $gridInfo
);
$li->custDataset = true;
$li->custDataSetArr = $indexVar['libslrs']->getTableGridData($args);

$li->no_col = sizeof($li->field_array) + 2;
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$pos = 0;
if (count($li->field_array) > 0) {
	$colTotal = count($li->field_array);
	$eqWidth = floor(100 / $colTotal);
	$cust_str_eqWidth = "style='width:" . $eqWidth . "%'";
	foreach ($li->field_array as $kk => $vv) {
		$li->column_list .= "<th " . $cust_str_eqWidth . ">".$li->column($pos++, $Lang["SLRS"]['ClosedLocationTH']["th_" . $vv])."</th>\n";
	}
} else {
	$eqWidth = "";
}
$li->column_list .= "<th width='1'>".$li->check("LocationRecIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();
$htmlAry['maxRow'] = $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('rowGroupID', 'rowGroupID', $a[0]["rowGroupID"]);

$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;
// ============================== Transactional data ==============================
// ============================== Define Button ==============================*/
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goNewForm();', '', $subBtnAry);

$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
$btnAry[] = array('edit', 'javascript: goEdit();');
$btnAry[] = array('delete', 'javascript: goDelete();', $Lang['eForm']['Delete']);
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);
