<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (count($_POST) > 0) {
	
	if (isset($_POST["TeacherRecIdAry"]["0"]) || isset($_POST["rec_id"]) && $_POST["rec_id"] > 0) {
		$rec_id = (isset($_POST["TeacherRecIdAry"]["0"])) ? $_POST["TeacherRecIdAry"]["0"] : $_POST["rec_id"];
		$is_update = true;
	} else {
		$is_update = false;
	}
	if (isset($_POST["token"])) {
		$err = array();
		if (empty($_POST["teacher"])) $err["teacher"] = $Lang["SLRS"]["NotAvailableTeachersErr"]["teacher"];
		if (empty($_POST["StartDate"])) $err["StartDate"] = $Lang["SLRS"]["NotAvailableTeachersErr"]["StartDate"];
		if (empty($_POST["EndDate"])) $err["EndDate"] = $Lang["SLRS"]["NotAvailableTeachersErr"]["EndDate"];
		if (empty($_POST["Reasons"])) $err["Reasons"] = $Lang["SLRS"]["NotAvailableTeachersErr"]["Reasons"];
		
		if (count($err) == 0) {
			$param = array(
					"UserID" => $_POST["teacher"],
					"StartDate" => $indexVar['libslrs']->reformat_date($_POST["StartDate"]),
					"EndDate" => $indexVar['libslrs']->reformat_date($_POST["EndDate"]),
					"Reasons" => $_POST["Reasons"]
			);
			
			if ($is_update) {
				$strSQL = "UPDATE INTRANET_SLRS_NOT_AVAILABLE_TEACHERS set ";
				foreach ($param as $kk => $vv) {
					$strSQL .= " " . $kk . "='" . $vv . "', ";
				}
				$strSQL .= "ModifiedBy_UserID='" . $_SESSION["UserID"] . "', DateTimeModified=NOW() WHERE NA_Teacher_ID='" . $rec_id . "'";
				$indexVar['libslrs']->db_db_query($strSQL);
			} else {
				$strSQL = "INSERT INTO INTRANET_SLRS_NOT_AVAILABLE_TEACHERS (" . implode(", ", array_keys($param)) . ", InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)";
				$strSQL .= " VALUES ";
				$strSQL .= " ('" . implode("', '", array_values($param)) . "', '" . $_SESSION["UserID"] . "', NOW(), '" . $_SESSION["UserID"] . "', NOW())";
				$indexVar['libslrs']->db_db_query($strSQL);
			}
			$saveSuccess = true;
			$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
			header('Location: ?task=settings'.$slrsConfig['taskSeparator'].'not_available_teachers'.$slrsConfig['taskSeparator'].'list&returnMsgKey=' . $returnMsgKey);
			exit;
		}
		
	} else if ($is_update) {
		$data = $indexVar['libslrs']->getNotAvailableTeacherByID($rec_id);
		$_POST["teacher"] = $data[0]["UserID"];
		$_POST["StartDate"] = $data[0]["StartDate"];
		$_POST["EndDate"] = $data[0]["EndDate"];
		$_POST["Reasons"] = $data[0]["Reasons"];
	}
}

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['NotAvailableTeachers']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ============================== 
$lgeneralsettings = new libgeneralsettings();
$data = $lgeneralsettings->Get_General_Setting("SLRS");

// $allTeachers = $indexVar['libslrs']->getAllTeachers();
$allTeachers = $indexVar['libslrs']->getAllSLRSAvailableTeachers();

$sql = "SELECT Module,SettingName FROM GENERAL_SETTING WHERE SettingName like '%OfficialLeave%'; ";
$reasons = $indexVar['libslrs']->returnResultSet($sql);
if (count($reasons) > 0) {
	foreach ($reasons as $kk => $vv) {
		$allReasons[] = str_replace('OfficialLeavel', "", $vv["SettingName"]);
	}
}

// ============================== Transactional data ==============================
$x = "";
$x .= "<table class=\"form_table_v30\">"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td class='field_title'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['NotAvailableTeachersTH']["th_Teacher"].":</td>"."\r\n";
$x .= "<td><select class='js-basic-single' name='teacher' id='slrs-teacher' style='width:50%; min-width:300px;'>";
$x .= "<option></option>";
if (count($allTeachers) > 0) {
	foreach ($allTeachers as $kk=> $vv) {
		if ($intranet_session_language == "en" || empty($vv["ChineseName"])) {
			$x .= "<option value='" . $vv["UserID"] . "'";
			if (isset($_POST["teacher"]) && $_POST["teacher"] == $vv["UserID"]) $x .= " selected";
			$x .= ">" . $vv["EnglishName"] . " ( " . $vv["UserLogin"] . " )</option>";
		} else {
			$x .= "<option value='" . $vv["UserID"] . "'";
			if (isset($_POST["teacher"]) && $_POST["teacher"] == $vv["UserID"]) $x .= " selected";
			$x .= ">" . $vv["ChineseName"] . " ( " . $vv["UserLogin"] . " )</option>";
		}
	}
}
$x .= "</select>";
if (isset($err["teacher"])) {
	$x .= " <span class='error' style='color:#f00;'>" . $err["teacher"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td class='field_title'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['NotAvailableTeachersTH']["th_StartDate"].":</td>"."\r\n";
$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='StartDate' id='StartDate' value='' />";
if (isset($err["StartDate"])) {
	$x .= " <span class='error' style='color:#f00;'>" . $err["StartDate"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td class='field_title'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['NotAvailableTeachersTH']["th_EndDate"].":</td>"."\r\n";
$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='EndDate' id='EndDate' value='' />";
if (isset($err["EndDate"])) {
	$x .= " <span class='error' style='color:#f00;'>" . $err["EndDate"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "<tr valign='top'>"."\r\n";
$x .= "<td class='field_title' class='slrs-textarea'><span class='tabletextrequire'>*</span> ".$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"].":</td>\r\n";
$x .= "<td><textarea id='Reasons' class='slrs-textarea' name='Reasons' style='width:90%; height:70px; resize: vertical;'>" . (isset($_POST["Reasons"]) ? $_POST["Reasons"] : '') . "</textarea>";
if (isset($err["Reasons"])) {
	$x .= "<br><span class='error' style='color:#f00;'>" . $err["Reasons"] . "</span>";
}
$x .= "</td>"."\r\n";
$x .= "</tr>"."\r\n";
$x .= "</table>"."\r\n";

$htmlAry['contentTbl'] = $x;
if (!$indexVar['libslrs']->isEJ())
{
	$htmlAry['loadingTxt'] = $linterface->Get_Ajax_Loading_Image();
}

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
