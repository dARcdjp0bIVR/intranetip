<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_USER
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
echo $indexVar['libslrs_ui']->Include_JS_CSS();
//$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteBalance']);
//$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$x = Get_Substitute_Teacher_Priority_Detail($Lang);

$htmlAry['contentTbl'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['cancelBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
// ============================== Define Button ==============================


function Get_Substitute_Teacher_Priority_Detail($Lang){
	$x = '<div id="DetailLayer"><span>';
	$x .= Get_Substitue_Teacher_Priority_Table($Lang);
	$x .= '</span></div>';
	return $x;
}

function Get_Substitue_Teacher_Priority_Table($Lang){
	$x = '<table id="dndTable" class="common_table_list ClassDragAndDrop">'."\r\n";
	$x .= '<thead>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['SLRS']['SubstituteTeacherSettingsDes']['UserName'].'</th>'."\r\n";
		$x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['SLRS']['SubstituteTeacherSettingsDes']['Balance'].'</th>'."\r\n";
		$x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['SLRS']['SubstituteTeacherSettingsDes']['Priority'].'</th>'."\r\n";
		$x .= '<th class="sub_row_top" style="width:25%;">'.$Lang['SLRS']['SubstituteTeacherSettingsDes']['JoinDate'].'</th>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= Get_Substitute_Teacher_Prioity_Data($Lang);
	$x .= '</table>'."\r\n";
	return $x;
}


function Get_Substitute_Teacher_Prioity_Data($Lang){
	global $indexVar, $slrsConfig;
	$displayOrder = 0;
	$connection = new libgeneralsettings();

	$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");
	$sql = "SELECT ".$name_field." as UserName,Balance,Priority,DATE_FORMAT(DateTimeInput,'%Y-%m-%d') as JoinDate,a.UserID
		 From (SELECT UserID,Balance,Priority,DateTimeInput FROM
		 INTRANET_SLRS_TEACHER
		 ) as a
		 INNER JOIN(
		 SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
		 ".$slrsConfig['INTRANET_USER']."
		 ) as b ON a.UserID=b.UserID ORDER BY Priority";
	//echo $sql;
	$a = $connection->returnArray($sql);	
	for($i=0; $i<sizeof($a);$i++){
		$x .= '<tr class="sub_row" id="'.$a[$i]['UserID'].'">'."\r\n";
			$x .= '<td>'.$a[$i]['UserName'].'</td>'."\r\n";
			$x .= '<td>'.$a[$i]['Balance'].'</td>'."\r\n";
			//$x .= '<td>'.$a[$i]['Priority'].'</td>';
			$x .= '<td class="Dragable"><div class="table_row_tool"><a href="#" class="move_order_dim" title="'.$Lang['SysMgr']['FormClassMapping']['Move'].'"></a></div></td>'."\r\n";
			$x .= '<td>'.$a[$i]['JoinDate'].'</td>'."\r\n";
		$x .= '</tr>';
	}
	return $x;
}

?>