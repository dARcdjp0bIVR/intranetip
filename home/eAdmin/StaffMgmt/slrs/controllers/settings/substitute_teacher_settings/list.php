<?php
## Using By :

################ Change Log [Start] #####################
#
#	Date 	:	2017-01-13 [Frankie]
#	Details	:	Improved to sorting and Add Search box
#
################ Change Log [End] #####################
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
$linterface = new interface_html();

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteTeacherSettings']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 0 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();

//$name_field = getNameFieldByLang("USR.");
//echo $name_field;
$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("EnglishName","Balance","Priority","JoinDate");
$li->sql = "SELECT ".$name_field." as UserName, Balance*".$balanceDisplay." as Balance, Priority, DATE_FORMAT(DateTimeInput,'%Y-%m-%d') as JoinDate,
	 CONCAT('<input type=checkbox name=userIdAry[] id=userIdAry[] value=\"',a.UserID,'\">') as checkbox";

$li->sql .= " From (";
$li->sql .= " SELECT UserID,Balance,Priority,DateTimeInput FROM INTRANET_SLRS_TEACHER WHERE IsActive=1 ";
$li->sql .= " ) as a ";
$li->sql .= " INNER JOIN( SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM ".$slrsConfig['INTRANET_USER']." WHERE Teaching <> 'S' AND RecordStatus='1' AND RecordType='1' ) as b ON a.UserID=b.UserID";

if (isset($_POST["searchtext"]) && !empty($_POST["searchtext"])) {
	$seachTxt = $_POST["searchtext"];
	$li->sql .= " WHERE EnglishName like '%" . $seachTxt . "%' or ChineseName like '%" . $seachTxt . "%' or DateTimeInput like '%" . $seachTxt . "%'"; 
}

$li->no_col = sizeof($li->field_array) + 2;
// $li->fieldorder2 = ", UserName";
$li->fieldorder2 = ", EnglishName";
$li->IsColOff = "IP25_table";
$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['SubstituteTeacherSettingsDes']['UserName'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['SubstituteTeacherSettingsDes']['Balance'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['SubstituteTeacherSettingsDes']['Priority'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['SubstituteTeacherSettingsDes']['JoinDate'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("userIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();

### hidden fields for db table
$x = '';
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;
// ============================== Transactional data ==============================
// ============================== Define Button ==============================
$btnAry = array();
$btnAry[] = array('new', 'javascript: goNew();');
$htmlAry['contentTool'] = $linterface->Get_Search_Box_Div('searchtext', stripslashes(stripslashes($seachTxt)));
$htmlAry['contentTool'] .= $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$btnAry = array();
$btnAry[] = array('edit', 'javascript: goEditSelectionOrder();', $Lang['SLRS']['SubstituteTeacherSettingsDes']['EditSelectionOrder']);
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// ============================== Define Button ==============================



?>