<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

$connection = new libgeneralsettings();

$saveSuccess = true;

$data = array();
$teacher= $_POST["teacher"];
for($i=0; $i<sizeof($teacher); $i++){
	$tempUserAry[] = (is_numeric($teacher[$i])) ? $teacher[$i] : substr($teacher[$i],1);
	$sql="SELECT UserID FROM INTRANET_SLRS_TEACHER WHERE UserID=".$tempUserAry[$i].";";
	$checkExistence=$connection->returnResultSet($sql);
	
	if(sizeof($checkExistence) > 0){
		$sql = "UPDATE INTRANET_SLRS_TEACHER SET IsActive=1,DateTimeInput=NOW(),DateTimeModified=NOW(),ModifiedBy_UserID=".$_SESSION['UserID']." WHERE UserID=".$tempUserAry[$i]."; ";
		$connection->db_db_query($sql);
		
	}else{	
		$sql = "INSERT IGNORE INTO INTRANET_SLRS_TEACHER(UserID,Balance,Priority,IsActive,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID) ";
		$sql .= "SELECT ".$tempUserAry[$i].","."0,0,1,".$_SESSION['UserID'].",NOW(),NOW(),".$_SESSION['UserID'];	
		$connection->db_db_query($sql);
	}
	//echo $sql;
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$slrsConfig['taskSeparator'].'substitute_teacher_settings'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>