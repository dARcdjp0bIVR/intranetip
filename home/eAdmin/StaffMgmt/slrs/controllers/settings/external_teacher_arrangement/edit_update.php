<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$connection = new libgeneralsettings();

$supplyID = $_POST['supplyID'];
$supplyTeacher = $_POST['supplyTeacher'];
$startDate = $_POST['startDate'];
$endDate = $_POST['endDate'];
$teacher = $_POST['teacher'];
$remark = $_POST['remark'];

$sql = "SELECT SupplyPeriodID FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE SupplyPeriodID=".IntegerSafe($supplyID).";";
//echo $sql;
$checkExistence = $connection->returnResultSet($sql);

if(sizeof($checkExistence) > 0){
	$sql = "UPDATE INTRANET_SLRS_SUPPLY_PERIOD SET Supply_UserID=".IntegerSafe($supplyTeacher).",TeacherID=".IntegerSafe($teacher).",DateStart='".$startDate."',";
	$sql .="DateEnd='".$endDate."',Remarks='".$remark."',DateTimeModified=NOW(),ModifiedBy_UserID=".$_SESSION['UserID']." WHERE SupplyPeriodID=".IntegerSafe($supplyID);	
	$sql .=";";
	$a = $connection->db_db_query($sql);
	//echo $sql;
}
else{
	$sql = "INSERT INTO INTRANET_SLRS_SUPPLY_PERIOD(Supply_UserID,TeacherID,DateStart,DateEnd,Remarks,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)";
	$sql .= "SELECT ".IntegerSafe($supplyTeacher).",".IntegerSafe($teacher).",'".$startDate."','".$endDate."','".$remark."',".$_SESSION['UserID'].",NOW(),NOW(),".$_SESSION['UserID'].";";
	$a = $connection->db_db_query($sql);	
	//echo $sql;
	
}

$saveSuccess = true;
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$slrsConfig['taskSeparator'].'external_teacher_arrangement'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);


?>