<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['CancelLesson']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
if (file_exists($PATH_WRT_ROOT."includes/slrs/slrs_helper.php"))
{
    include_once($PATH_WRT_ROOT."includes/slrs/slrs_helper.php");
}
// ============================= Transactional data ==============================
$linterface = new interface_html();
$connection = new libgeneralsettings();
$sql = "SELECT Module,SettingName FROM GENERAL_SETTING WHERE SettingName like '%OfficialLeave%'; ";
$a = $connection->returnResultSet($sql);

$TermInfoArr = getCurrentAcademicYearAndYearTerm();
$academicYearID = $TermInfoArr['AcademicYearID'];
$Year_StartDate = getStartDateOfAcademicYear($academicYearID);
$Year_EndDate = getEndDateOfAcademicYear($academicYearID);

$dateLimitation = array();
$dateLimitation['min']["Y"] = date("Y", strtotime($Year_StartDate));
$dateLimitation['min']["M"] = date("m", strtotime($Year_StartDate)) - 1;
$dateLimitation['min']["D"] = date("d", strtotime($Year_StartDate));
$dateLimitation['max']["Y"] = date("Y", strtotime($Year_EndDate));
$dateLimitation['max']["M"] = date("m", strtotime($Year_EndDate)) - 1;
$dateLimitation['max']["D"] = date("d", strtotime($Year_EndDate));

if (isset($_GET["selected_date"]))
{
	$date = $_GET["selected_date"];
}
else 
{
	$date = date("Y-m-d");
}

$x = "";
$x .= "<table class=\"form_table_v30\">"."\r\n";
	$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['SubstitutionArrangementDes']['Date']."</td><td>:</td>"."\r\n";
		// $x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $date,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
		$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='datePicker' id='datePicker' value='" . $date . "' />";
		if (isset($err["StartDate"])) {
			$x .= " <span class='error' style='color:#f00;'>" . $err["StartDate"] . "</span>";
		}
		$x .= "</td>"."\r\n";
		
		// $x .= "<td><input type='text' class='slrs-datepicker' id='datePicker' name='datePicker' value='" . $date . "'>"."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	$x .= "<tr>"."\r\n";
	$x .= "<td>".$Lang['SLRS']['CancelLessonDes']['Target'].$indexVar['libslrs_ui']->RequiredSymbol()."</td><td>:</td>"."\r\n";
	
		$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($date, $fromExchange=true);
		
		$finalTeacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
		$finalTeacherList = array_values($finalTeacherList);
		
		$x .= "<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="cancelTeacher" class="js-select2 js-basic-single" name="cancelTeacher"', $SelectedType="", $all=0, $noFirst=0)."</td>"."\r\n";
	$x .= "</tr>"."\r\n";
	
$x .= "</table><div class='loadingbox'></div><div class='form_table_v30' id=cancelLessonBody></div>";

$htmlAry['contentTbl'] = $x;
$htmlAry['loadingTxt'] = $linterface->Get_Ajax_Loading_Image();

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================

// ============================== Custom Function ==============================
// ============================== Custom Function ==============================
?>