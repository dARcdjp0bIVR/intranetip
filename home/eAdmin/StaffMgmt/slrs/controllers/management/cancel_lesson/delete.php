<?php
if (file_exists($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php"))
{
    include_once($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php");
    $CancelLessonModel = new CancelLessonMD();
}

if (count($_POST["cancelIdAry"]) > 0)
{
    foreach ($_POST["cancelIdAry"] as $key => $cancelLessonID)
    {
        $strSQL = $CancelLessonModel->buildDeleteSQLByParam($cancelLessonID);
        if ($strSQL != null)
        {
            $result = $indexVar["libslrs"]->db_db_query($strSQL);
        }
    }
    $saveSuccess = true;
} else {
    $saveSuccess = false;
}
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'cancel_lesson'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);
