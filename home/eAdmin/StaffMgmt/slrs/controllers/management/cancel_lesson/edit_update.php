<?php 
if (file_exists($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php"))
{
    include_once($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php");
    $CancelLessonModel = new CancelLessonMD();
}

$now = date("Y-m-d H:i:s");
if (count($_POST["clTimeSlotID"]) > 0)
{
    foreach ($_POST["clTimeSlotID"] as $key => $timeslotID)
    {
        $param = array (
            "UserID" => $_POST["cancelTeacher"],
            "CancelDate" => $_POST["datePicker"],
            "TimetableID" => $_POST["clTimetableID"][$key],
            "TimeSlotID" => $_POST["clTimeSlotID"][$key],
            "YearTermID" => $_POST["clYearTermID"][$key],
            "SubjectID" => $_POST["clSubjectID"][$key],
            "SubjectGroupID" => $_POST["clSubjectGroupID"][$key],
            "LocationID" => $_POST["clLocationID"][$key],
            "CancelRemark" => str_replace("\n", "<br>", $_POST["cancelLessonRemarks"][$key])
        );
        $strSQL = $CancelLessonModel->buildRetrieveSQLByParam($param, true);
        $result = $indexVar["libslrs"]->returnResultSet($strSQL);
        if ($_POST["clCancel"][$key])
        {
            if (count($result) > 0)
            {
                $cancelLessonID = $result[0]["CancelLessonID"];
                $strSQL = $CancelLessonModel->buildUpdateSQLByParam($param, $cancelLessonID);
            } else {
                $strSQL = $CancelLessonModel->buildInsertSQLByParam($param);
            }
            if ($strSQL != null)
            {
                $result = $indexVar["libslrs"]->db_db_query($strSQL);
            }
        } else {
            if (count($result) > 0)
            {
                $cancelLessonID = $result[0]["CancelLessonID"];
                $strSQL = $CancelLessonModel->buildDeleteSQLByParam($cancelLessonID);
                if ($strSQL != null)
                {
                    $result = $indexVar["libslrs"]->db_db_query($strSQL);
                }
            }
        }
    }
    $saveSuccess = true;
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'cancel_lesson'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);
exit;