<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['LessonExchangeTitle']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================= Transactional data ==============================
$linterface = new interface_html();

//$groupID = $_POST["rowGroupID"];

$groupIDInput= $_POST["groupIdAry"];

$groupIDArray[] = (is_numeric($groupIDInput[0])) ? $groupIDInput[0] : substr($groupIDInput[0],1);
$groupID=$groupIDArray[0];
//echo $groupID;
//echo $room;

if ($groupID == "")
{
	if (isset($_GET["exchangetoken"]) && $sys_custom['SLRS']["lessonExchangePromptAnotherLession"])
	{
		$thisToken = md5($_GET["sgid"] . "_" . $_GET["slotid"] . "_" . $_GET["date"] . "_SLRS_EXC");
		if ($_GET["exchangetoken"] == $thisToken)
		{
			$anotherInfo = array(
				"SubjectGroupID" => $_GET["sgid"],
				"TimeSlotID" => $_GET["slotid"],
				"ExchangeDate" => $_GET["date"]
			);
			$datePicker = $anotherInfo["ExchangeDate"];
			$date = $_GET["date"];
		}
	}
	$x = "<table id=\"exchangeTable\" width=\"99%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"."\r\n";
		// new row
		$x .= "<tr>"."\r\n";
			$x .="<td class=\"main_content_slrs\">"."\r\n";
			// Left Teacher
			$x .= "<div id=\"div_1\" class=\"slrs_exchange left\">"."\r\n";
					$x .= "<div class=\"table_board\">";
						$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['OriginalLesson']."</span><br/>"."\r\n";
							$x .= "<table style='width:90%;'>"."\r\n";
								$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
								$x .= "<tbody>"."\r\n";
									$x .="<tr>"."\r\n";
									$x .="<td style='width:13%;'>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td>";
									$x .= "<td style='width:2%;'>:</td>"."\r\n";
									//$x .="<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_1', $datePicker,$OnDatePickSelectedFunction='temp()')."</td>"."\r\n";
									if (isset($anotherInfo))
									{
										$x .="<td style='width:85%'>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_1', $datePicker, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_1')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=true, $cssClass="textboxnum")."</td>"."\r\n";
									}
									else
									{
										$x .="<td style='width:85%'>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_1', $datePicker, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_1')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
									}
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";

									// $indexVar['libslrs']->getTeachingTeacher()
									$finalTeacherList = array();
									if (isset($anotherInfo))
									{
										$teacherList = $indexVar['libslrs']->getLessonTeacherByAnotherInfo($anotherInfo);
										$finalTeacherList = array_values($teacherList);
									}
									else
									{
										$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($date, $fromExchange = true, $ignoreExchangeGroupID=$groupIDArray);
										if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"])
										{
											$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
											$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($date, TRUE);
											$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
											if (count($teacherList) > 0 && count($ignore_teacherList))
											{
												$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
												if (count($finalTeacherList) > 0)
												{
													$finalTeacherList = array_values($finalTeacherList);
												}
											}
											else
											{
												$finalTeacherList = array_values($teacherList);
											}
										}
										else
										{
											$finalTeacherList = array_values($teacherList);
										}
									}
									$selectedTeacher = ($anotherInfo) ? $finalTeacherList[0]["UserID"] : "";
									if (isset($anotherInfo))
									{
										$x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="teachingTeacher_1" style="color:#000;background-color:#ebebe4;" disabled name="teachingTeacher_1"', $SelectedType=$selectedTeacher, $all=0, $noFirst=0)."</td>"."\r\n";
									}
									else
									{
										$x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="teachingTeacher_1" name="teachingTeacher_1"', $SelectedType=$selectedTeacher, $all=0, $noFirst=0)."</td>"."\r\n";
									}
									
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
									
									
									$lessonSelection = "";
									if (isset($anotherInfo))
									{
										$lessonInfo = $indexVar['libslrs']->getRoom($selectedTeacher, $anotherInfo["ExchangeDate"], $checkWithArrangement = true, $ignoreExchangeGroup = $groupIDArr);
										if (count($lessonInfo) > 0)
										{
											if (isset($anotherInfo))
											{
												$lessonSelection = '<select id="timeSlot_1" name="timeSlot_1" disabled style="color:#000; background-color:#ebebe4;">';
											}
											else
											{
												$lessonSelection = '<select id="timeSlot_1" name="timeSlot_1">';
											}
											
											$lessonSelection .= "<option value=''>" . $button_select . " --</option>";
											foreach ($lessonInfo as $less_index => $less_data)
											{
												$subjectGroupName = Get_Lang_Selection(
																		($indexVar['libslrs']->isEJ() ?
																				convert2unicode($less_data["ClassTitleB5"], 1, 1) : 
																				$less_data["ClassTitleB5"]
																		),
																		$less_data["ClassTitleEN"]
																	) . " (".$less_data["TimeSlotName"].")";
												
												if (isset($less_data["isArranged"]) && $less_data["isArranged"])
												{
													$subjectGroupName .= " " . $Lang['SLRS']['SubstitutionArrangementHasRecord'];
													$lessonSelection .= "<option value='" .  $less_data["TimeSlotID"] . "' disabled rel='" . $less_data["SubjectGroupID"] . "'>" . $subjectGroupName . "</option>";
												}
												else 
												{
													if ($anotherInfo["TimeSlotID"] == $less_data["TimeSlotID"])
													{
														$lessonSelection .= "<option value='" .  $less_data["TimeSlotID"] . "' selected rel='" . $less_data["SubjectGroupID"] . "'>" . $subjectGroupName . "</option>";
													}
													else
													{
														$lessonSelection .= "<option value='" .  $less_data["TimeSlotID"] . "' rel='" . $less_data["SubjectGroupID"] . "'>" . $subjectGroupName . "</option>";
													}
												}
											}
											$lessonSelection .= "</select>";
										}
										else
										{
											$lessonSelection = $Lang['SLRS']['SubstitutionArrangementHasRecordDisableExchange'];
										}
									}
									else
									{
										$lessonSelection = getSelectByAssoArray("", $selectionTags='id="timeSlot_1" name="timeSlot_1"', $SelectedType="", $all=0, $noFirst=0);
									}
									$x .="<td>". $lessonSelection ."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									
									if ($sys_custom['SLRS']["allowSubstitutionRemarks"])
									{
										$x .="<tr valign=top>"."\r\n";
										$x .="<td>". $Lang['SLRS']['LessonRemarksForOriginalLesson']."</td><td>:</td>"."\r\n";
										$x .="<td><textarea name='LessonRemarks_1' style='width:100%; resize:vertical; height:40px;'></textarea></td>"."\r\n";
										$x .="</tr>"."\r\n";
									}
									
								$x .= "</tbody>"."\r\n";
							$x .= "</table>"."\r\n";									
					$x .= "</div>";
					$x .= "<div id='detailsLesson_1' class=\"table_board\"></div>"."\r\n";			
				$x .= "</div>";
				// End of Left Teacher
				// Center Separator
				$x .= "<div id=\"divCenter_1\" class=\"slrs_exchange_separator\" style=\"display:none\"><span></span></div>"."\r\n";
				// End of Center Separator
				// Right Teacher
				$x .= "<div id=\"div_2\" class=\"slrs_exchange\" style=\"display:none\">"."\r\n";
					$x .= "<div class=\"table_board\">";
						$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['ExchangedLesson']."</span><br/>"."\r\n";
						$x .= "<table>"."\r\n";
							$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
								$x .= "<tbody>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td><td>:</td>"."\r\n";
										$x .="<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_2', $datePicker,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_2')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
										$x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($indexVar['libslrs']->getTeachingTeacher()), $selectionTags='id="teachingTeacher_2" name="teachingTeacher_2"', $SelectedType="", $all=0, $noFirst=0)."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
										$x .="<td>".getSelectByAssoArray("", $selectionTags='id="timeSlot_2" name="timeSlot_2"', $SelectedType="", $all=0, $noFirst=0)."</td>"."\r\n";
									$x .="</tr>"."\r\n";
								$x .= "</tbody>"."\r\n";
						$x .= "</table>"."\r\n";
					$x .= "</div>";
					$x .= "<div id='detailsLesson_2' class=\"table_board\"></div>"."\r\n";
				$x .= "</div>";
				// End of Right Teacher
			$x .="</td>"."\r\n";
		$x .= "</tr>"."\r\n";
		// End of new row
		
	$x .= "</table>"."\r\n";
		
	$htmlAry['contentTbl'] = $x;
	$htmlAry['startIndex'] = "1";
	$htmlAry['endIndex'] = "2";

}
else
{
	$connection = new libgeneralsettings();
	
	$sql = "SELECT TeacherA_Date,TeacherB_Date FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE GroupID=".$groupID."";
	$a = $connection->returnResultSet($sql);
	$teacherA_DateArr=array();
	$teacherB_DateArr=array();
	for($i=0;$i<sizeof($a);$i++)
	{
		$teacherA_DateArr[$i]=$a[$i]["TeacherA_Date"];
		$teacherB_DateArr[$i]=$a[$i]["TeacherB_Date"];
	}
	
	$sql = "SELECT
				LessonExchangeID, TeacherA_UserID, TeacherA_Date, TeacherA_TimeSlotID, TeacherA_TimeStart, TeacherA_TimeEnd, TeacherA_SubjectGroupID,
				TeacherA_YearClassID, TeacherA_SubjectID, TeacherA_LocationID,
				TeacherB_UserID, TeacherB_Date, TeacherB_TimeSlotID, TeacherB_TimeStart, TeacherB_TimeEnd, TeacherB_SubjectGroupID, TeacherB_YearClassID,
				TeacherB_SubjectID, TeacherB_LocationID, Remarks,GroupID, SequenceNumber, LessonRemarks,
				EnglishName, ChineseName, TimeSlotID, TimeSlotName, StartTime, EndTime, ClassCode, ClassTitleEN, ClassTitleB5,
				LocationID, Code, BarCode, NameChi, NameEng, DisplayOrder,
				RecordDateA, CycleDayA, RecordDateB, CycleDayB,
				TimeSlotIDA, SubjectGroupIDA, LocationIDA, DayA, OtherLocationA,
				TimeSlotIDB, SubjectGroupIDB, SubjectGroupIDB, DayB, LocationIDB, OtherLocationB,
				CASE WHEN OriginalTeacherA IS NULL THEN TeacherA_UserID ELSE OriginalTeacherA END as OriginalTeacherA,
				CASE WHEN OriginalTeacherB IS NULL THEN TeacherB_UserID ELSE OriginalTeacherB END as OriginalTeacherB			
			FROM (
				SELECT
					LessonExchangeID,
					
					TeacherA_UserID, TeacherA_Date, TeacherA_TimeSlotID, TeacherA_TimeStart, TeacherA_TimeEnd, TeacherA_SubjectGroupID,
					TeacherA_YearClassID, TeacherA_SubjectID, TeacherA_LocationID,
					
					TeacherB_UserID, TeacherB_Date, TeacherB_TimeSlotID, TeacherB_TimeStart, TeacherB_TimeEnd, TeacherB_SubjectGroupID,
					TeacherB_YearClassID, TeacherB_SubjectID, TeacherB_LocationID,

					Remarks, GroupID, SequenceNumber, LessonRemarks
				FROM		
					INTRANET_SLRS_LESSON_EXCHANGE
			) as isle
			INNER JOIN (
				SELECT 
					UserID, EnglishName, ChineseName
				FROM 
					".$slrsConfig['INTRANET_USER']."
			) as iu ON (isle.TeacherA_UserID = iu.UserID)
			INNER JOIN (
				SELECT
					TimeSlotID, TimeSlotName, StartTime, EndTime
				FROM 
					INTRANET_TIMETABLE_TIMESLOT
			) as itt ON (isle.TeacherA_TimeSlotID = itt.TimeSlotID)
			INNER JOIN (
				SELECT
					SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5
				FROM
					SUBJECT_TERM_CLASS
			) as stc ON (isle.TeacherA_SubjectGroupID = stc.SubjectGroupID)
			LEFT JOIN (
				SELECT
					LocationID, Code, BarCode, NameChi, NameEng, DisplayOrder
				FROM
					INVENTORY_LOCATION
			) as il ON (isle.TeacherA_LocationID = il.LocationID)
			INNER JOIN (
				".$indexVar['libslrs']->getDaySQL($teacherA_DateArr,"A") . "
			) as icdA ON (isle.TeacherA_Date = icdA.RecordDateA)
			INNER JOIN (
				".$indexVar['libslrs']->getDaySQL($teacherB_DateArr,"B")."
			) as icdB ON (isle.TeacherB_Date = icdB.RecordDateB)
			LEFT JOIN (
				SELECT
					RoomAllocationID, TimeSlotID as TimeSlotIDA, SubjectGroupID as SubjectGroupIDA, LocationID as LocationIDA, Day as DayA, OthersLocation as OtherLocationA
				FROM
					INTRANET_TIMETABLE_ROOM_ALLOCATION
			) as itraA ON (isle.TeacherA_SubjectGroupID = itraA.SubjectGroupIDA AND icdA.CycleDayA = itraA.DayA AND isle.TeacherA_TimeSlotID = itraA.TimeSlotIDA)
			LEFT JOIN (
				SELECT 
					RoomAllocationID, TimeSlotID as TimeSlotIDB, SubjectGroupID as SubjectGroupIDB, LocationID as LocationIDB, Day as DayB, OthersLocation as OtherLocationB
				FROM
					INTRANET_TIMETABLE_ROOM_ALLOCATION
			) as itraB ON (isle.TeacherB_SubjectGroupID = itraB.SubjectGroupIDB AND icdB.CycleDayB = itraB.DayB AND isle.TeacherB_TimeSlotID = itraB.TimeSlotIDB)			
			LEFT JOIN(
				SELECT
					SupplyPeriodID, Supply_UserID as Supply_UserIDA, TeacherID as OriginalTeacherA, DateStart as DateStartA, DateEnd as DateEndA
				FROM
					INTRANET_SLRS_SUPPLY_PERIOD
			) as isspA ON (isle.TeacherA_UserID = Supply_UserIDA AND TeacherA_Date BETWEEN DateStartA AND DateEndA)
			LEFT JOIN(
				SELECT
					SupplyPeriodID, Supply_UserID as Supply_UserIDB, TeacherID as OriginalTeacherB, DateStart as DateStartB, DateEnd as DateEndB
				FROM
					INTRANET_SLRS_SUPPLY_PERIOD
			) as isspB ON (isle.TeacherB_UserID = Supply_UserIDB AND TeacherB_Date BETWEEN DateStartB AND DateEndB)
			WHERE GroupID=".$groupID."
			ORDER BY SequenceNumber;";

	$a= $connection->returnResultSet($sql);
	$maxSequenceNumber = $a[sizeof($a)-1]["SequenceNumber"] * 2;
	
	$groupIDArr = explode(',',$groupID);
	
	//$sql = "SELECT LocationID,NameChi,NameEng FROM INVENTORY_LOCATION";
	$locaitonList = $indexVar['libslrs']->getLocationList();
	
	$x = "<table id=\"exchangeTable\" width=\"99%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">"."\r\n";
	for($i=0; $i<sizeof($groupIDArr); $i++ )
	{
		for($j=0; $j<sizeof($a); $j++)
		{
			if($groupIDArr[$i] == $a[$j]["GroupID"])
			{
				if($j==0)
				{
					// new row
					$x .= "<tr>"."\r\n";
						$x .="<td class=\"main_content_slrs\">"."\r\n";
							// Left Teacher
							$x .= "<div id=\"div_1\" class=\"slrs_exchange left\">"."\r\n";
							$x .= "<div class=\"table_board\">";
							$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['OriginalLesson']."</span><br/>"."\r\n";
								$x .= "<table>"."\r\n";
								$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
								$x .= "<tbody>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td><td>:</td>"."\r\n";
										$x .="<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_1', $a[$j]["TeacherA_Date"], $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_1')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
										
										$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($a[$j]["TeacherA_Date"], $fromExchange = true, $ignoreExchangeGroupID=$groupIDArr);
										$finalTeacherList = array();
										if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"])
										{
											$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
											$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($a[$j]["TeacherA_Date"], TRUE);
											$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
											if (isset($ignore_teacherList[$a[$j]["TeacherA_UserID"]])) unset($ignore_teacherList[$a[$j]["TeacherA_UserID"]]);
											
											if (count($teacherList) > 0 && count($ignore_teacherList))
											{
												$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
												if (count($finalTeacherList) > 0)
												{
													$finalTeacherList = array_values($finalTeacherList);
												}
											}
											else
											{
												$finalTeacherList = array_values($teacherList);
											}
										}
										else
										{
											$finalTeacherList = array_values($teacherList);
										}
										$x .="<td>". getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="teachingTeacher_1" name="teachingTeacher_1"', $SelectedType=$a[$j]["TeacherA_UserID"], $all=0, $noFirst=0)."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
										
										$lessonRoom = $indexVar['libslrs']->getRoom($a[$j]["OriginalTeacherA"], $a[$j]["TeacherA_Date"], $checkWithArrangement = true, $ignoreExchangeGroup = $groupIDArr);
										$lessonSelection = '<select id="timeSlot_1" name="timeSlot_1">';
										$lessonSelection .= "<option value=''>-- " . $button_select . " --</option>";
										foreach ($lessonRoom as $less_index => $less_data)
										{
											$subjectGroupName = Get_Lang_Selection($less_data["ClassTitleB5"], $less_data["ClassTitleEN"]) . " (".$less_data["TimeSlotName"].")";
											$lessonSelection .= "<option value='" .  $less_data["TimeSlotID"] . "' rel='" . $less_data["SubjectGroupID"] . "'";
											if ($a[$j]["TeacherA_TimeSlotID"]== $less_data["TimeSlotID"]) $lessonSelection .= " selected";
											$lessonSelection .= ">" . $subjectGroupName . "</option>";
										}
										$lessonSelection .= "</select>";
										
										// $x .="<td>".getSelectByAssoArray(cnvRoomArrToSelect($indexVar['libslrs']->getRoom($a[$j]["OriginalTeacherA"],$a[$j]["TeacherA_Date"]),"TimeSlotID","ClassTitleB5","ClassTitleEN"), $selectionTags='' id="1" name="1"', $SelectedType=$a[$j]["TeacherA_TimeSlotID"], $all=0, $noFirst=0)."</td>"."\r\n";
										$x .= "<td>" . $lessonSelection . "</td>";
									$x .="</tr>"."\r\n";
									
									if ($sys_custom['SLRS']["allowSubstitutionRemarks"])
									{
										$x .="<tr valign=top>"."\r\n";
										$x .="<td>". sprintf($Lang['SLRS']['LessonRemarksForOriginalLessonMarks'], Get_Lang_Selection($a[$j]["ClassTitleB5"],$a[$j]["ClassTitleEN"]) . "(" . $a[$j]["TimeSlotName"] . ")")."</td><td>:</td>"."\r\n";
										$x .="<td><textarea name='LessonRemarks_1' style='width:100%; resize:vertical; height:40px;'>" . $a[$j]["LessonRemarks"] . "</textarea></td>"."\r\n";
										$x .="</tr>"."\r\n";
									}
									
								$x .= "</tbody>"."\r\n";
								$x .= "</table>"."\r\n";
							$x .= "</div>";
							$b = $indexVar['libslrs']->getRoomDetails($a[$j]["OriginalTeacherA"],$a[$j]["TeacherA_Date"],$a[$j]["TeacherA_TimeSlotID"]);
							$x .= "<div id='detailsLesson_1' class=\"table_board\">";
								$x .= "<table class=\"common_table_list slrs_list\">"."\r\n";
									$x .= "<tr>";
									$x .= "<th style='width:30%;'></th>";
									$x .= "<th style='width:30%;'>".$a[$j]["TeacherA_Date"]."<br/>".$b[0]["Day"]."</th>";
									$x .= "<th style='width:40%;'>".$Lang['SLRS']['LessonExchangeDes']['Location']."</th>";
									$x .= "</tr>";
									$x .= "<tr><td>".$b[0]["StartTime"]." - ".$b[0]["EndTime"]."<br/>".$b[0]["TimeSlotName"]."</td>";
									$x .= "<td class=\"date class_fail\">".$indexVar['libslrs']->getSubjectName($b[0]["SubjectGroupID"],"linebreak")."</td>";
									$x .= "<td class=\"date\">".
									(Get_Lang_Selection($b[0]["NameChi"],$b[0]["NameEng"]) ? ($indexVar['libslrs']->getLocationName($b[0]["Code"], $b[0]["NameChi"], $b[0]["NameEng"])) : $b[0]["OthersLocation"]);
								$x .= "</td>";
								$x .="</tr>"."\r\n";
								$x .= "</table>";	
								$x .= "<input type=\"hidden\" id=\"StartTime_1"."\" value=\"".$b[0]["StartTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"EndTime_1"."\" value=\"".$b[0]["EndTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotID_1"."\" value=\"".$b[0]["TimeSlotID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupID_1"."\" value=\"".$b[0]["SubjectGroupID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"YearClassID_1"."\" value=\"".$b[0]["YearClassID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectID_1"."\" value=\"".$b[0]["SubjectID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationID_1"."\" value=\"".$b[0]["LocationID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"Day_1"."\" value=\"".$b[0]["Day"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotName_1"."\" value=\"".$b[0]["TimeSlotName"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupName_1"."\" value=\"".Get_Lang_Selection($b[0]["ClassTitleB5"],$b[0]["ClassTitleEN"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationName_1"."\" value=\"".(Get_Lang_Selection($b[0]["NameChi"],$b[0]["NameEng"])?($indexVar['libslrs']->getLocationName($b[0]["Code"],$b[0]["NameChi"],$b[0]["NameEng"])) :$b[0]["OthersLocation"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"DatePicker_1"."\" value=\"".$a[$j]["TeacherA_Date"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"OriginalTeacher_1"."\" value=\"".$a[$j]["OriginalTeacherA"]."\"/>";
							$x .= "</div>"."\r\n";
							$x .= "</div>";
							// End of Left Teacher
							// Center Separator
							$x .= "<div id=\"divCenter_1\" class=\"slrs_exchange_separator\"><span></span></div>"."\r\n";
							// End of Center Separator
							// Right Teacher
							$x .= "<div id=\"div_2\" class=\"slrs_exchange\">"."\r\n";
							$x .= "<div class=\"table_board\">";
							$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['ExchangedLesson']."</span><br/>"."\r\n";
								$x .= "<table style='width:90%'>"."\r\n";
								$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
								$x .= "<tbody>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td style='width:13%'>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td><td style='width:2%'>:</td>"."\r\n";
										$x .="<td style='width:85%'>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_2', $a[$j]["TeacherB_Date"],  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_2')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
										
										$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($a[$j]["TeacherB_Date"], $fromExchange = true, $ignoreExchangeGroupID=$groupIDArray);
										$finalTeacherList = array();
										if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"])
										{
											$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
											$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($a[$j]["TeacherB_Date"], TRUE);
											$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
											if (isset($ignore_teacherList[$a[$j]["TeacherB_UserID"]])) unset($ignore_teacherList[$a[$j]["TeacherB_UserID"]]);
											if (count($teacherList) > 0 && count($ignore_teacherList))
											{
												$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
												if (count($finalTeacherList) > 0)
												{
													$finalTeacherList = array_values($finalTeacherList);
												}
											}
											else
											{
												$finalTeacherList = array_values($teacherList);
											}
										}
										else
										{
											$finalTeacherList = array_values($teacherList);
										}
										
										$x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="teachingTeacher_2" name="teachingTeacher_2"', $SelectedType=$a[$j]["TeacherB_UserID"], $all=0, $noFirst=0)."</td>"."\r\n";
									$x .="</tr>"."\r\n";
									$x .="<tr>"."\r\n";
										$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
										$lessonRoom = $indexVar['libslrs']->getRoom($a[$j]["OriginalTeacherB"], $a[$j]["TeacherB_Date"], $checkWithArrangement = true, $ignoreExchangeGroup = $groupIDArr);
										$lessonSelection = '<select id="timeSlot_2" name="timeSlot_2">';
										$lessonSelection .= "<option value=''>-- " . $button_select . " --</option>";
										foreach ($lessonRoom as $less_index => $less_data)
										{
											$subjectGroupName = Get_Lang_Selection( $less_data["ClassTitleB5"], $less_data["ClassTitleEN"]) . " (".$less_data["TimeSlotName"].")";
											$lessonSelection .= "<option value='" .  $less_data["TimeSlotID"] . "' rel='" . $less_data["SubjectGroupID"] . "'";
											if ($a[$j]["TeacherB_TimeSlotID"]== $less_data["TimeSlotID"]) $lessonSelection .= " selected";
											$lessonSelection .= ">" . $subjectGroupName . "</option>";
										}
										$lessonSelection .= "</select>";
										// $x .="<td>".getSelectByAssoArray(cnvRoomArrToSelect($lessonRoom,"TimeSlotID","ClassTitleB5","ClassTitleEN"), $selectionTags='id="2" name="2"', $SelectedType=$a[$j]["TeacherB_TimeSlotID"], $all=0, $noFirst=0)."</td>"."\r\n";
										$x .= "<td>" . $lessonSelection . "</td>";
										
										
									$x .="</tr>"."\r\n";
								$x .= "</tbody>"."\r\n";
								$x .= "</table>"."\r\n";
							$x .= "</div>";
							$c = $indexVar['libslrs']->getRoomDetails($a[$j]["OriginalTeacherB"],$a[$j]["TeacherB_Date"],$a[$j]["TeacherB_TimeSlotID"]);
							$x .= "<div id='detailsLesson_2' class=\"table_board\">"."\r\n";
								$x .= "<table class=\"common_table_list slrs_list\">"."\r\n";
								$x .= "<tr>";
									$x .= "<th style='width:30%;'></th>";
									$x .= "<th style='width:30%;' class=\"date\">".$a[$j]["TeacherB_Date"]."<br/>".$c[0]["Day"]."</th>";
									$x .= "<th style='width:40%;' class=\"date\">".$Lang['SLRS']['LessonExchangeDes']['Location']."</th>";
									$x .= "</tr>";
								$x .= "<tr><td>".$c[0]["StartTime"]." - ".$c[0]["EndTime"]."<br/>".$c[0]["TimeSlotName"]."</td>";
									// $x .= "<td class=\"date class_success\">".$indexVar['libslrs']->getSubjectName($c[0]["SubjectGroupID"],"linebreak")."</td>";
									$x .= "<td class=\"date class_success\">".$indexVar['libslrs']->getSubjectName($a[$j]["TeacherA_SubjectGroupID"],"linebreak")."</td>";

									$x .= "<td class=\"date\">". 
											(Get_Lang_Selection($c[0]["NameChi"],$c[0]["NameEng"]) ?
													($indexVar['libslrs']->getLocationName($c[0]["Code"],$c[0]["NameChi"],$c[0]["NameEng"])) :
													$c[0]["OthersLocation"]);
									
									if($a[$j]["TeacherB_LocationID"] == $a[$j]["LocationIDB"])
									{
										$x .= "<div>"
												."<a id=\"locationText_2\">".$Lang['SLRS']['LessonExchangeDes']['ToBeChanged']."</a><br/>"
														."<div id=\"locationDiv_2\" style=\"display:none\">". getSelectByAssoArray(cnvGeneralArrToSelect($locaitonList, "LocationID","NameChi","NameEng"), $selectionTags='id="locationID_2" name="locationID_2"', $SelectedType=$a[0]["LocationIDA"], $all=0, $noFirst=0)."</div>"
																."</div>";
									}
									else if ($a[$j]["TeacherB_LocationID"] != $a[$j]["LocationIDB"])
									{
										$x .= "<div>"
												."<a id=\"locationText_2\">".$Lang['SLRS']['LessonExchangeDes']['Cancel']."</a><br/>"
														."<div id=\"locationDiv_2\">". getSelectByAssoArray(cnvGeneralArrToSelect($locaitonList, "LocationID","NameChi","NameEng"), $selectionTags='id="locationID_2" name="locationID_2"', $SelectedType=$a[0]["TeacherB_LocationID"], $all=0, $noFirst=0)."</div>"
																."</div>";
									}									
									$x .= "</td>";
									$x .="</tr>"."\r\n";
								$x .= "</table>";
								$x .= "<input type=\"hidden\" id=\"StartTime_2"."\" value=\"".$c[0]["StartTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"EndTime_2"."\" value=\"".$c[0]["EndTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotID_2"."\" value=\"".$c[0]["TimeSlotID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupID_2"."\" value=\"".$c[0]["SubjectGroupID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"YearClassID_2"."\" value=\"".$c[0]["YearClassID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectID_2"."\" value=\"".$c[0]["SubjectID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationID_2"."\" value=\"".$a[0]["TeacherB_LocationID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"Day_2"."\" value=\"".$c[0]["Day"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotName_2"."\" value=\"".$c[0]["TimeSlotName"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupName_2"."\" value=\"".Get_Lang_Selection($c[0]["ClassTitleB5"],$c[0]["ClassTitleEN"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationName_2"."\" value=\"".(Get_Lang_Selection($c[0]["NameChi"],$c[0]["NameEng"])?($indexVar['libslrs']->getLocationName($c[0]["Code"],$c[0]["NameChi"],$c[0]["NameEng"])) :$c[0]["OthersLocation"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"DatePicker_2"."\" value=\"".$a[$j]["TeacherB_Date"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"OriginalTeacher_2"."\" value=\"".$a[$j]["OriginalTeacherB"]."\"/>";
							$x .= "</div>"."\r\n";
							$x .= "</div>";
							// End of Right Teacher
						$x .="</td>"."\r\n";
					$x .= "</tr>"."\r\n";
					// End of new row
				}
				else
				{
					$subStartIndex = $a[$j]["SequenceNumber"] * 2 -1;
					$subEndIndex = $a[$j]["SequenceNumber"] * 2;
					$x .= "<tr>"."\r\n";
						$x .="<td class=\"main_content_slrs\">"."\r\n";
						// Left Teacher
							$x .= "<div id=\"div_".$subStartIndex."\" class=\"slrs_exchange left\">"."\r\n";
							$x .= "<div class=\"table_board\">";
							$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['OriginalLesson']."</span><br/>"."\r\n";
								$x .= "<table style='width:90%'>"."\r\n";
								$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
								$x .= "<tbody>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td style='width:13%'>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td><td style='width:2%'>:</td>"."\r\n";
									$x .="<td style='width:85%'>".$a[$j]["TeacherA_Date"]."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
									if ($indexVar['libslrs']->isEJ()) {
										// $a[$j]["ChineseName"] = convert2unicode($a[$j]["ChineseName"]);
									}
									$x .="<td>".Get_Lang_Selection($a[$j]["ChineseName"],$a[$j]["EnglishName"])."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
									
								if ($indexVar['libslrs']->isEJ()) {
									$a[$j]["ClassTitleB5"] = convert2unicode($a[$j]["ClassTitleB5"]);
								}
									$x .="<td class=\"date\">".Get_Lang_Selection($a[$j]["ClassTitleB5"],$a[$j]["ClassTitleEN"]). " (" . $a[$j]["TimeSlotName"] . ")</td>"."\r\n";
								$x .="</tr>"."\r\n";
								if ($sys_custom['SLRS']["allowSubstitutionRemarks"]) {
									$x .="<tr valign=top>"."\r\n";
									$x .="<td>". sprintf($Lang['SLRS']['LessonRemarksForOriginalLessonMarks'], Get_Lang_Selection($a[$j]["ClassTitleB5"], $a[$j]["ClassTitleEN"]) . "(" . $a[$j]["TimeSlotName"] . ")")."</td><td>:</td>"."\r\n";
									$x .="<td><textarea name='LessonRemarks_".$subStartIndex."' style='width:100%; resize:vertical; height:40px;'>" . $a[$j]["LessonRemarks"] . "</textarea></td>"."\r\n";
									$x .="</tr>"."\r\n";
								}
								
								$x .= "</tbody>"."\r\n";
								$x .= "</table>"."\r\n";
							$x .= "</div>";
							$b = $indexVar['libslrs']->getRoomDetails($a[$j]["TeacherA_UserID"], $a[$j]["TeacherA_Date"], $a[$j]["TeacherA_TimeSlotID"]);
							$x .= "<div id=\"detailsLesson_".$subStartIndex."\" class=\"table_board\">";
								$x .= "<table class=\"common_table_list slrs_list\">"."\r\n";
								$x .= "<tr>";
								$x .= "<th style='width:30%;'></th>";
								$x .= "<th style='width:30%;' class=\"date\">".$a[$j]["TeacherA_Date"]."<br/>".$b[0]["Day"]."</th>";
								$x .= "<th style='width:40%;' class=\"date\">".$Lang['SLRS']['LessonExchangeDes']['Location']."</th>";
								$x .= "</tr>";
								$x .= "<tr><td>".$b[0]["StartTime"]." - ".$b[0]["EndTime"]."<br/>".$b[0]["TimeSlotName"]."</td>";
								// $x .= "<td class=\"date class_fail\">".$indexVar['libslrs']->getSubjectName($b[0]["SubjectGroupID"],"linebreak")."</td>";
								$x .= "<td class=\"date class_fail\">".$indexVar['libslrs']->getSubjectName($a[$j]["TeacherA_SubjectGroupID"],"linebreak")."</td>";
								$x .= "<td class=\"date\">".
									(Get_Lang_Selection($b[0]["NameChi"],$b[0]["NameEng"])?($indexVar['libslrs']->getLocationName($b[0]["Code"],$b[0]["NameChi"],$b[0]["NameEng"])) :$b[0]["OthersLocation"]);

								$x .= "</td>";
								$x .="</tr>"."\r\n";
								$x .= "</table>";
								$x .= "<input type=\"hidden\" id=\"StartTime_".$subStartIndex."\" value=\"".$b[0]["StartTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"EndTime_".$subStartIndex."\" value=\"".$b[0]["EndTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotID_".$subStartIndex."\" value=\"".$b[0]["TimeSlotID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupID_".$subStartIndex."\" value=\"".$b[0]["SubjectGroupID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"YearClassID_".$subStartIndex."\" value=\"".$b[0]["YearClassID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectID_".$subStartIndex."\" value=\"".$b[0]["SubjectID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationID_".$subStartIndex."\" value=\"".$b[0]["LocationID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"DatePicker_".$subStartIndex."\" value=\"".$a[$j]["TeacherA_Date"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TeachingTeacher_".$subStartIndex."\" value=\"".$a[$j]["TeacherA_UserID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"Day_".$subStartIndex."\" value=\"".$b[0]["Day"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotName_".$subStartIndex."\" value=\"".$b[0]["TimeSlotName"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupName_".$subStartIndex."\" value=\"".Get_Lang_Selection($b[0]["ClassTitleB5"],$b[0]["ClassTitleEN"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationName_".$subStartIndex."\" value=\"".(Get_Lang_Selection($b[0]["NameChi"],$b[0]["NameEng"])?($indexVar['libslrs']->getLocationName($b[0]["Code"],$b[0]["NameChi"],$b[0]["NameEng"])) :$b[0]["OthersLocation"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"OriginalTeacher_".$subStartIndex."\" value=\"".$a[$j]["OriginalTeacherA"]."\"/>";
							$x .= "</div>"."\r\n";
							$x .= "</div>";
							// End of Left Teacher
							// Center Separator
							$x .= "<div id=\"divCenter_".$subStartIndex."\" class=\"slrs_exchange_separator\"><span></span></div>"."\r\n";
							// End of Center Separator
							// Right Teacher
							$x .= "<div id=\"div_".$subEndIndex."\" class=\"slrs_exchange\">"."\r\n";
							$x .= "<div class=\"table_board\">";
							$x .= "<span class=\"slrs_heading\">".$Lang['SLRS']['LessonExchangeDes']['ExchangedLesson']."</span><br/>"."\r\n";
								$x .= "<table style='width:90%'>"."\r\n";
								$x .= "<colgroup><col width=\"25%\"><col class=\"field_c\"></colgroup>"."\r\n";
								$x .= "<tbody>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td style='width:13%'>".$Lang['SLRS']['LessonExchangeDes']['Date']."</td><td style='width:2%'>:</td>"."\r\n";
									$x .="<td style='width:85%'>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker_'.$subEndIndex, $a[$j]["TeacherB_Date"],  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker_".$subEndIndex."')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Teacher']."</td><td>:</td>"."\r\n";
									
									// $x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($indexVar['libslrs']->getTeachingTeacherDate($a[$j]["TeacherB_Date"])), $selectionTags='id="teachingTeacher_'.$subEndIndex.'" name="teachingTeacher_'.$subEndIndex.'"', $SelectedType=$a[$j]["TeacherB_UserID"], $all=0, $noFirst=0)."</td>"."\r\n";
									$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($a[$j]["TeacherB_Date"], $fromExchange = true, $ignoreExchangeGroupID=$groupIDArray);
									$finalTeacherList = array();
									if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"])
									{
										$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
										$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($a[$j]["TeacherB_Date"], TRUE);
										$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
										if (isset($ignore_teacherList[$a[$j]["TeacherB_UserID"]])) unset($ignore_teacherList[$a[$j]["TeacherB_UserID"]]);
										if (count($teacherList) > 0 && count($ignore_teacherList))
										{
											$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
											if (count($finalTeacherList) > 0)
											{
												$finalTeacherList = array_values($finalTeacherList);
											}
										}
										else
										{
											$finalTeacherList = array_values($teacherList);
										}
									}
									else
									{
										$finalTeacherList = array_values($teacherList);
									}
									$x .="<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="teachingTeacher_'.$subEndIndex.'" name="teachingTeacher_'.$subEndIndex.'"', $SelectedType=$a[$j]["TeacherB_UserID"], $all=0, $noFirst=0)."</td>"."\r\n";
									
								$x .="</tr>"."\r\n";
								$x .="<tr>"."\r\n";
									$x .="<td>".$Lang['SLRS']['LessonExchangeDes']['Classroom']."</td><td>:</td>"."\r\n";
									
									$lessonRoom = $indexVar['libslrs']->getRoom($a[$j]["OriginalTeacherB"],$a[$j]["TeacherB_Date"], $checkWithArrangement = true, $ignoreExchangeGroup = $groupIDArr);
									$lessonSelection = '<select id="timeSlot_'.$subEndIndex.'" name="timeSlot_'.$subEndIndex.'">';
									$lessonSelection .= "<option value=''>-- " . $button_select . " --</option>";
									foreach ($lessonRoom as $less_index => $less_data)
									{
										$subjectGroupName = Get_Lang_Selection($less_data["ClassTitleB5"],$less_data["ClassTitleEN"]) . " (".$less_data["TimeSlotName"].")";
										$lessonSelection .= "<option value='" .  $less_data["TimeSlotID"] . "' rel='" . $less_data["SubjectGroupID"] . "'";
										if ($a[$j]["TeacherB_TimeSlotID"]== $less_data["TimeSlotID"]) $lessonSelection .= " selected";
										$lessonSelection .= ">" . $subjectGroupName . "</option>";
									}
									$lessonSelection .= "</select>";
									// $x .="<td>".getSelectByAssoArray(cnvRoomArrToSelect($indexVar['libslrs']->getRoom($a[$j]["OriginalTeacherB"],$a[$j]["TeacherB_Date"]),"TimeSlotID","ClassTitleB5","ClassTitleEN"), $selectionTags='id="'.$subEndIndex.'" name="'.$subEndIndex.'"', $SelectedType=$a[$j]["TeacherB_TimeSlotID"], $all=0, $noFirst=0)."</td>"."\r\n";
									$x .= "<td>" . $lessonSelection . "</td>";
									
								$x .="</tr>"."\r\n";
								$x .= "</tbody>"."\r\n";
								$x .= "</table>"."\r\n";
							$x .= "</div>";
							$c = $indexVar['libslrs']->getRoomDetails($a[$j]["TeacherB_UserID"], $a[$j]["TeacherB_Date"], $a[$j]["TeacherB_TimeSlotID"]);

							$x .= "<div id=\"detailsLesson_".$subEndIndex."\" class=\"table_board\">"."\r\n";
								$x .= "<table class=\"common_table_list slrs_list\">"."\r\n";
									$x .= "<tr>";
									$x .= "<th style='width:30%;'></th>";
									$x .= "<th style='width:30%;' class=\"date\">".$a[$j]["TeacherB_Date"]."<br/>".$c[0]["Day"]."</th>";
									$x .= "<th style='width:40%;' class=\"date\">".$Lang['SLRS']['LessonExchangeDes']['Location']."</th>";
									$x .= "</tr>";
									$x .= "<tr><td>".$c[0]["StartTime"]." - ".$c[0]["EndTime"]."<br/>".$c[0]["TimeSlotName"]."</td>";
									// $x .= "<td class=\"date class_success\">".$indexVar['libslrs']->getSubjectName($c[0]["SubjectGroupID"],"linebreak")."</td>";
									$x .= "<td class=\"date class_success\">".$indexVar['libslrs']->getSubjectName($a[$j]["TeacherA_SubjectGroupID"],"linebreak")."</td>";
									$x .= "<td class=\"date\">".
									(Get_Lang_Selection($c[0]["NameChi"],$c[0]["NameEng"])?($indexVar['libslrs']->getLocationName($c[0]["Code"],$c[0]["NameChi"],$c[0]["NameEng"])) :$c[0]["OthersLocation"]);;
									if($a[$j]["TeacherB_LocationID"] == $a[$j]["LocationIDB"])
									{
										$x .= "<div>"
												."<a id=\"locationText_".$subEndIndex."\">".$Lang['SLRS']['LessonExchangeDes']['ToBeChanged']."</a><br/>"
														."<div id=\"locationDiv_".$subEndIndex."\" style=\"display:none\">". getSelectByAssoArray(cnvGeneralArrToSelect($locaitonList, "LocationID","NameChi","NameEng"), $selectionTags='id="locationID_'.$subEndIndex.'" name="locationID_'.$subEndIndex.'"', $SelectedType=$a[$j]["LocationIDA"], $all=0, $noFirst=0)."</div>"
																."</div>";
									}
									else if ($a[$j]["TeacherB_LocationID"] != $a[$j]["LocationIDB"])
									{
										$x .= "<div>"
												."<a id=\"locationText_".$subEndIndex."\">".$Lang['SLRS']['LessonExchangeDes']['Cancel']."</a><br/>"
														."<div id=\"locationDiv_".$subEndIndex."\">". getSelectByAssoArray(cnvGeneralArrToSelect($locaitonList, "LocationID","NameChi","NameEng"), $selectionTags='id="locationID_'.$subEndIndex.'" name="locationID_'.$subEndIndex.'"', $SelectedType=$a[$j]["TeacherB_LocationID"], $all=0, $noFirst=0)."</div>"
																."</div>";
									}							
									$x .= "</td>";
									$x .="</tr>"."\r\n";
								$x .= "</table>";
								$x .= "<input type=\"hidden\" id=\"StartTime_".$subEndIndex."\" value=\"".$c[0]["StartTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"EndTime_".$subEndIndex."\" value=\"".$c[0]["EndTime"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotID_".$subEndIndex."\" value=\"".$c[0]["TimeSlotID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupID_".$subEndIndex."\" value=\"".$c[0]["SubjectGroupID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"YearClassID_".$subEndIndex."\" value=\"".$c[0]["YearClassID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectID_".$subEndIndex."\" value=\"".$c[0]["SubjectID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationID_".$subEndIndex."\" value=\"".$a[$j]["TeacherB_LocationID"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"Day_".$subEndIndex."\" value=\"".$c[0]["Day"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"TimeSlotName_".$subEndIndex."\" value=\"".$c[0]["TimeSlotName"]."\"/>";
								$x .= "<input type=\"hidden\" id=\"SubjectGroupName_".$subEndIndex."\" value=\"".Get_Lang_Selection($c[0]["ClassTitleB5"],$c[0]["ClassTitleEN"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"LocationName_".$subEndIndex."\" value=\"".(Get_Lang_Selection($c[0]["NameChi"],$c[0]["NameEng"])?($indexVar['libslrs']->getLocationName($c[0]["Code"],$c[0]["NameChi"],$c[0]["NameEng"])) :$c[0]["OthersLocation"])."\"/>";
								$x .= "<input type=\"hidden\" id=\"OriginalTeacher_".$subEndIndex."\" value=\"".$a[$j]["OriginalTeacherB"]."\"/>";
								$x .= "</div>"."\r\n";
							$x .= "</div>";
						// End of Right Teacher
						$x .="</td>"."\r\n";
					$x .= "</tr>"."\r\n";
				}
			}
		}
	}
	$x .= "</table>"."\r\n";
	
	$htmlAry['rowGroupID'] = $groupID;
	$htmlAry['contentTbl'] = $x;
	$htmlAry['startIndex'] = $a[0]["SequenceNumber"];
	$htmlAry['endIndex'] = $a[sizeof($a)-1]["SequenceNumber"] * 2;
}
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
/*$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goExchangeLesson();', '', $subBtnAry);
$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
$subBtnAry = array();
$btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
$btnAry[] = array('edit', 'javascript: goEdit();');
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);
*/
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "submit", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

$htmlAry['loadingTxt'] = $linterface->Get_Ajax_Loading_Image();

// ============================== Define Button ==============================

// ============================== Custom Function ==============================

function cnvUserArrToSelect($arr)
{
    $oAry = array();
    for($_i=0; $_i<sizeof($arr); $_i++){
        $oAry[$arr[$_i]["UserID"]] = Get_Lang_Selection($arr[$_i]["ChineseName"],$arr[$_i]["EnglishName"]);
    }
    return $oAry;;
}

function cnvRoomArrToSelect($arr,$type,$ChineseName,$EnglishName)
{
    $oAry = array();
    for($_i=0; $_i<sizeof($arr); $_i++){
        $oAry[$arr[$_i][$type]] = Get_Lang_Selection($arr[$_i][$ChineseName],$arr[$_i][$EnglishName])."(".$arr[$_i]["TimeSlotName"].")";
    }
    return $oAry;;
}

function cnvGeneralArrToSelect($arr,$type,$ChineseName,$EnglishName)
{
    $oAry = array();
    for($_i=0; $_i<sizeof($arr); $_i++){
        $oAry[$arr[$_i][$type]] = Get_Lang_Selection($arr[$_i][$ChineseName],$arr[$_i][$EnglishName]);
    }
    return $oAry;;
}
// ============================== Custom Function ==============================
?>