<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
if (file_exists($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php"))
{
    include_once($PATH_WRT_ROOT."home/eAdmin/StaffMgmt/slrs/model/cancellesson.php");
    $CancelLessonModel = new CancelLessonMD();
}
// ============================== Transactional data ==============================
$type = $_POST['type'];
$teacher = $_POST['teacher'];
$room = $_POST['room'];
$date = $_POST['date'];
$groupID = $_POST['groupID'];
$SubjectGroupID = $_POST['sg'];
$timeSlotID = $_POST['timeSlotID'];

### Handle SQL Injection + XSS [START]
$type = cleanCrossSiteScriptingCode($type);
if($type == "teacher" || $type == "room" || $type == "roomDetails")
{
    if(!intranet_validateDate($date))
    {
        $arr = array('result' => false);
        $jsonObj = new JSON_obj();
        echo $jsonObj->encode($arr);
        exit;
    }
}

$teacher = IntegerSafe($teacher);
$groupID = IntegerSafe($groupID);
$SubjectGroupID = IntegerSafe($SubjectGroupID);
$timeSlotID = IntegerSafe($timeSlotID);
### Handle SQL Injection + XSS [END]

$connection = new libgeneralsettings();

if($type == "teacher"){
	// $a= $indexVar['libslrs']->getTeachingTeacherDate($date);
	
	// $indexVar['libslrs']->getTeachingTeacher()
	if ($groupID != "")
	{
		$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($date, $fromExchange=true, $ignoreExchangeGroupID = array($groupID));
	}
	else
	{
		$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($date, $fromExchange=true);
	}
	
	$finalTeacherList = array();
	if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"]) {
		$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
		
		/*$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($date, TRUE);
		$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
		if (count($teacherList) > 0 && count($ignore_teacherList)) {
			$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
			if (count($finalTeacherList) > 0) {
				$finalTeacherList = array_values($finalTeacherList);
			}
		} else {
			$finalTeacherList = array_values($teacherList);
		}*/
		$finalTeacherList = array_values($teacherList);
	} else {
		$finalTeacherList = array_values($teacherList);
	}
	$arr = array();
	$jsonObj = new JSON_obj();
	for ($i=0; $i<sizeof($finalTeacherList); $i++){
        $arr[] = array(
            'UserID' => $finalTeacherList[$i]["UserID"],
            'Name' => Get_Lang_Selection($finalTeacherList[$i]["ChineseName"], $finalTeacherList[$i]["EnglishName"])
        );
	}
	echo $jsonObj->encode($arr);
}
else if($type == "room"){
	if ($groupID != "")
	{
		$a = $indexVar['libslrs']->getRoom($teacher,$date, $checkWithArrangement = true, $ignoreExchangeGroup = array($groupID));
	}
	else
	{
		$a = $indexVar['libslrs']->getRoom($teacher,$date, $checkWithArrangement = true);
	}
	$arr = array();
	$jsonObj = new JSON_obj();	
	for ($i=0; $i<sizeof($a); $i++){
	    
	    $cancel_param = array(
	        "UserID" => $teacher,
	        "CancelDate" =>  $date,
	        "TimeSlotID" => $a[$i]["TimeSlotID"],
	        "SubjectGroupID" => $a[$i]["SubjectGroupID"]
	    );
	    $strSQL = $CancelLessonModel->buildRetrieveSQLByParam($cancel_param);
	    $tmp = $connection->returnResultSet($strSQL);
	    if (count($tmp) > 0)
	    {
	        
	    } else {
    		$subjectGroupName = Get_Lang_Selection($a[$i]["ClassTitleB5"], $a[$i]["ClassTitleEN"])." (".$a[$i]["TimeSlotName"].")";
    		$arr[] = array(
    				'SubjectGroupID' => $a[$i]["SubjectGroupID"],
    				'Name' => $subjectGroupName,
    				'TimeSlotID'=>$a[$i]["TimeSlotID"]		
    		);
	    }
	}
	echo $jsonObj->encode($arr);
}
else if($type == "roomDetails"){
	$a = $indexVar['libslrs']->getRoomDetails($teacher,$date,$timeSlotID,$SubjectGroupID);
	/*
	$b = $indexVar['libslrs']->getLocationList();
	$locationName = "";
	$locationValue = "";	
	for($j=0; $j<sizeof($b); $j++){
		if($j==(sizeof($b)-1)){
			$locationName .= Get_Lang_Selection($b[$j]["NameChi"],$b[$j]["NameEng"]);
			$locationValue .= $b[$j]["LocationID"];
		}
		else{
			$locationName .= Get_Lang_Selection($b[$j]["NameChi"],$b[$j]["NameEng"]).",";
			$locationValue .= $b[$j]["LocationID"].",";
		}
	}
	*/
	$locaitonSelection = $indexVar['libslrs']->getAllLocationSelection($date, $timeSlotID);
	if (count($locaitonSelection) > 0) {
		foreach ($locaitonSelection as $loc_id => $loc_vaL) {
		    // [DM#1270] fix location name display problem in EJ
            $loc_vaL = $indexVar['libslrs']->isEJ() ? convert2unicode($loc_vaL,1,1) : $loc_vaL;

			$locationName .= (empty($locationName)) ? $loc_vaL : "," . $loc_vaL;
			$locationValue .= (empty($locationValue)) ? $loc_id: "," . $loc_id;
		}
	}
	
	$arr = array();
	$jsonObj = new JSON_obj();

	for ($i=0; $i<sizeof($a); $i++){
		
		// $display_locationName = (Get_Lang_Selection($a[$i]["NameChi"],$a[$i]["NameEng"])?($indexVar['libslrs']->getLocationName($a[$i]["Code"],$a[$i]["NameChi"],$a[$i]["NameEng"])) :$a[$i]["OthersLocaiton"]);
		$display_locationName = (isset($locaitonSelection[$a[$i]["LocationID"]])) ?
									$locaitonSelection[$a[$i]["LocationID"]] :
									((Get_Lang_Selection($a[$i]["NameChi"],$a[$i]["NameEng"]) != "") ?
											($indexVar['libslrs']->getLocationName($a[$i]["Code"],$a[$i]["NameChi"],$a[$i]["NameEng"])) :
											($a[$i]["OthersLocation"]));
		
		$arr[] = array(
					'LocationID' => $a[$i]["LocationID"],
					'Name' => $display_locationName,
					'TimeSlotName' => $a[$i]["TimeSlotName"],
					'StartTime' => $a[$i]["StartTime"],
					'EndTime' => $a[$i]["EndTime"],
					'Day' => $a[$i]["Day"],
					'SubjectGroupName' => $indexVar['libslrs']->getSubjectName($a[$i]["SubjectGroupID"],"linebreak"),
					'YearTermID' => $a[$i]["YearTermID"],
					'AcademicYearID' => $a[$i]["AcademicYearID"],
					'YearID' => $a[$i]["YearID"],
					'YearClassID' => $a[$i]["YearClassID"],
					'SubjectID' => $a[$i]["SubjectID"],
					'TimeSlotID' => $a[$i]["TimeSlotID"],
					'SubjectGroupID' => $a[$i]["SubjectGroupID"],
					'Date'=>$date,
					'LocationName'=>$locationName,
					'LocationValue'=>$locationValue
		);
	}
	echo $jsonObj->encode($arr);
}
else if($type == "list" ){
	/*
	$sql = "SELECT
				GroupID,
				SequenceNumber,
				TeacherA_UserID,
				TeacherA_TimeSlotID,
				TeacherA_SubjectGroupID,
				TeacherA_Date,
				TeacherB_Date,
				TeacherA_LocationID,
				TeacherB_LocationID,
				isle.LessonExchangeID,
				isle.DateTimeModified,
				EnglishName, ChineseName, TitleChinese, TimeSlotName,
				itt.StartTime, itt.EndTime, ClassTitleB5, ClassTitleEN, SessionType,
				isla.LessonArrangementID, 
				isla.LeaveID, Date(MAX(isla.TimeStart)) as DateLeave,
				isla.ArrangedTo_LocationID,
				isl.LeaveID as substitutionLeaveID,
				isl.DateLeave as substitutionDateLeave
			FROM
				INTRANET_SLRS_LESSON_EXCHANGE as isle
			INNER JOIN
				 ".$slrsConfig['INTRANET_USER']." AS iu ON (isle.TeacherA_UserID =iu.UserID)
			INNER JOIN 
				INTRANET_TIMETABLE_TIMESLOT AS itt ON (isle.TeacherA_TimeSlotID=itt.TimeSlotID)
			INNER JOIN
				SUBJECT_TERM_CLASS AS stc ON (isle.TeacherA_SubjectGroupID=stc.SubjectGroupID)
			LEFT JOIN 
				INTRANET_SLRS_LESSON_ARRANGEMENT as isla ON (
					isla.UserID=TeacherA_UserID AND isla.TimeSlotID=isle.TeacherB_TimeSlotID AND TeacherB_Date=DATE(TimeStart)
				)
			LEFT JOIN INTRANET_SLRS_LEAVE AS isl ON (
				(Duration=SessionType OR Duration='wd') AND isl.UserID=TeacherA_UserID AND 
				( DateLeave=TeacherA_Date OR DateLeave=TeacherB_Date) 
			)
			WHERE
				GroupID IN (".$groupID.")
			GROUP BY isle.LessonExchangeID 
			ORDER BY TeacherA_Date, SequenceNumber;"
	;
	// echo $sql; exit;
	$a= $connection->returnResultSet($sql);
	
	for($i=0; $i<sizeof($a); $i++){
		$orgTeacherName = Get_Lang_Selection(($indexVar['libslrs']->isEJ() ? convert2unicode($a[$i]["ChineseName"],1,1) : $a[$i]["ChineseName"]), $a[$i]["EnglishName"]);
		$TeacherName = $orgTeacherName;
		$hasSubstitution = false;
		if (empty($a[$i]["DateLeave"]))
		{
			$a[$i]["DateLeave"] = $a[$i]["substitutionDateLeave"];
		}
		if($a[$i]["LeaveID"] > 0 && ($a[$i]["DateLeave"] == $a[$i]["TeacherB_Date"]))
		{
			$hasSubstitution = true;
			$TeacherName .= " <span style='font-style:italic; color:#afafaf'>(** " . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . " - " . $a[$i]["DateLeave"] . ")</span>";
		}
		else if ($a[$i]["substitutionLeaveID"] > 0 || $a[$i]["substitutionDateLeave"] == $a[$i]["TeacherB_Date"])
		{
			$hasSubstitution = true;
			$TeacherName .= " <span style='font-style:italic; color:#afafaf'>(** " . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . " - " . $a[$i]["TeacherB_Date"] . ")</span>";
		}
		if (is_array($startTime = explode(":", $a[$i]["StartTime"])))
		{
			array_pop($startTime);
			$startTime = implode(":", $startTime);
		}
		else
		{
			$startTime = $a[$i]["StartTime"];
		}
		if (is_array($endTime = explode(":", $a[$i]["EndTime"])))
		{
			array_pop($endTime);
			$endTime = implode(":", $endTime);
		}
		else
		{
			$endTime = $a[$i]["EndTime"];
		}
		$TimeSlotName = ($indexVar['libslrs']->isEJ() ? convert2unicode($a[$i]["TimeSlotName"],1,1) : $a[$i]["TimeSlotName"]);
		$ClassName = Get_Lang_Selection( ($indexVar['libslrs']->isEJ() ? convert2unicode($a[$i]["ClassTitleB5"],1,1) : $a[$i]["ClassTitleB5"]), $a[$i]["ClassTitleEN"]);
		
		if (!isset($arr[$a[$i]["LessonExchangeID"]]))
		{
			$arr[$a[$i]["LessonExchangeID"]] = array(
													'GroupID'=>$a[$i]["GroupID"],
													'Date' => $a[$i]["TeacherA_Date"],
													'DateTimeModified'=>$a[$i]["DateTimeModified"],
													'orgName' => $orgTeacherName,
													'Name' => $TeacherName,
													'TimeSlotName'=> $TimeSlotName,
													'SlotTime' => $startTime . '~' . $endTime,
													'ClassName' => $ClassName,
													'hasSubstitution' => ($hasSubstitution) ? 1 : 0
												);
		}
		else
		{
			if ($arr[$a[$i]["LessonExchangeID"]]["hasSubstitution"] === 0)
			{
				$arr[$a[$i]["LessonExchangeID"]]["Name"] = $TeacherName;
				$arr[$a[$i]["LessonExchangeID"]]["hasSubstitution"] = ($hasSubstitution) ? 1 : 0;
			}
		}
	}
	*/
	// echo "<pre>"; print_r($arr); exit;
	$sql = "SELECT
				GroupID,
				SequenceNumber,
				TeacherA_UserID,
				TeacherA_TimeSlotID,
				TeacherA_SubjectGroupID,
				TeacherA_Date,
				TeacherA_LocationID,

				TeacherB_UserID,
				TeacherB_TimeSlotID,
				TeacherB_Date,
				TeacherB_LocationID,
				TeacherB_SubjectGroupID,

				isle.LessonExchangeID,
				combined_type,
                LessonRemarks
			FROM
				INTRANET_SLRS_LESSON_EXCHANGE as isle
			INNER JOIN
				 ".$slrsConfig['INTRANET_USER']." AS iu ON (isle.TeacherA_UserID =iu.UserID)
			WHERE
				GroupID IN (".$groupID.")
			GROUP BY isle.LessonExchangeID
			ORDER BY DateTimeModified DESC, GroupID, SequenceNumber;"
	;
	// echo $sql; exit;

	$a= $connection->returnResultSet($sql);
	if (count($a) > 0)
	{
		foreach ($a as $key => $val)
		{
// 			$orgTeacherName = Get_Lang_Selection(($indexVar['libslrs']->isEJ() ? convert2unicode($a[$i]["ChineseName"],1,1) : $a[$i]["ChineseName"]), $a[$i]["EnglishName"]);
// 			$TeacherName = $orgTeacherName;
// 			
// 			$ClassName = Get_Lang_Selection( ($indexVar['libslrs']->isEJ() ? convert2unicode($a[$i]["ClassTitleB5"],1,1) : $a[$i]["ClassTitleB5"]), $a[$i]["ClassTitleEN"]);
			$groupInfo[$val["GroupID"]][$val["LessonExchangeID"]] = $val;
			$groupInfo[$val["GroupID"]][$val["LessonExchangeID"]]["is_location_exchange"] = false;
			
			if ($val["TeacherA_Date"] == $val["TeacherB_Date"] && 
			    $val["TeacherA_TimeSlotID"] == $val["TeacherB_TimeSlotID"] &&
			    $val["TeacherA_SubjectGroupID"] == $val["TeacherB_SubjectGroupID"] &&
			    $val["TeacherA_UserID"] == $val["TeacherB_UserID"]) {
			        $groupInfo[$val["GroupID"]][$val["LessonExchangeID"]]["is_location_exchange"] = true;
		    }
			$userIDs[] = $val["TeacherA_UserID"];
			$userIDs[] = $val["TeacherB_UserID"];
			
			$timeSlotIDs[] = $val["TeacherA_TimeSlotID"];
			$timeSlotIDs[] = $val["TeacherB_TimeSlotID"];
			
			$subjectGroupIDs[] = $val["TeacherA_SubjectGroupID"];
			$subjectGroupIDs[] = $val["TeacherB_SubjectGroupID"];
			
			if ($val["TeacherA_LocationID"] > 0)
			{
				$locationIDs[] = $val["TeacherA_LocationID"];
			}
			else
			{
				$otherLocations[] = array(
										"GroupID" => $val["GroupID"],
										"LessonExchangeID" => $val["LessonExchangeID"],
										"Target" => "A",
										"Date" => $val["TeacherA_Date"],
										"TimeSlotID" => $val["TeacherA_TimeSlotID"],
										"LocationID" => $val["TeacherA_LocationID"],
										"SubjectGroupID" => $val["TeacherA_SubjectGroupID"]
									);
			}
			if ($val["TeacherB_LocationID"] > 0)
			{
				$locationIDs[] = $val["TeacherB_LocationID"];
			}
			else
			{
				$otherLocations[] = array(
										"GroupID" => $val["GroupID"],
										"LessonExchangeID" => $val["LessonExchangeID"],
										"Target" => "B",
										"Date" => $val["TeacherB_Date"],
										"TimeSlotID" => $val["TeacherB_TimeSlotID"],
										"LocationID" => $val["TeacherB_LocationID"],
										"SubjectGroupID" => $val["TeacherB_SubjectGroupID"]
									);
			}
			
			$cycleDays[] = $val["TeacherA_Date"];
			$cycleDays[] = $val["TeacherB_Date"];
			
			$checkSubstitution[$val["TeacherA_UserID"]][] = $val["TeacherA_Date"];
			$checkSubstitution[$val["TeacherA_UserID"]][] = $val["TeacherB_Date"];
			
			$checkSubstitution[$val["TeacherB_UserID"]][] = $val["TeacherA_Date"];
			$checkSubstitution[$val["TeacherB_UserID"]][] = $val["TeacherB_Date"];
		}

		if (count($checkSubstitution) > 0)
		{
			#find all Substitution record's date
			foreach ($checkSubstitution as $_userID => $substitutionDate)
			{
				$leaveDateInfo[$_userID] = array();
				if (count($substitutionDate) > 0)
				{
					$substitutionDate = array_unique($substitutionDate);
				}

				/*
				$strSQL = "SELECT DATE(TimeStart) as DateLeave FROM INTRANET_SLRS_LESSON_ARRANGEMENT
							WHERE 
							(TimeStart LIKE '" . implode("%' OR TimeStart LIKE '", array_unique($substitutionDate)) . "%') AND (UserID='" . $_userID . "' OR ArrangedTo_UserID='" . $_userID . "')";
				*/
				$strSQL = "SELECT DATE(TimeStart) as DateLeave FROM INTRANET_SLRS_LESSON_ARRANGEMENT
							WHERE
							(TimeStart LIKE '" . implode("%' OR TimeStart LIKE '", array_unique($substitutionDate)) . "%') AND (UserID='" . $_userID . "')";
				$result = $connection->returnResultSet($strSQL);
				if (count($result) > 0)
				{
					foreach ($result as $key => $val)
					{
						$leaveDateInfo[$_userID][] = $val["DateLeave"];
					}
				}
				$strSQL = "SELECT DateLeave FROM INTRANET_SLRS_LEAVE 
							WHERE
							DateLeave IN ('" . implode("', '", array_unique($substitutionDate)) . "') AND (UserID='" . $_userID . "')";
				$result = $connection->returnResultSet($strSQL);
				if (count($result) > 0)
				{
					foreach ($result as $key => $val)
					{
						$leaveDateInfo[$_userID][] = $val["DateLeave"];
					}
				}
				if (count($leaveDateInfo[$_userID]) > 0)
				{
					$leaveDateInfo[$_userID] = array_unique($leaveDateInfo[$_userID]);
				}
			}
		}
		
		if (count($userIDs) > 0)
		{
			## get All User  Info
			$strSQL = "SELECT UserID, EnglishName, ChineseName FROM ".$slrsConfig['INTRANET_USER']." WHERE UserID IN ('" . implode("', '", $userIDs) . "')";
			$result = $connection->returnResultSet($strSQL);
			if (count($result) > 0)
			{
				foreach ($result as $key => $val)
				{
					$userInfo[$val['UserID']] = $val;
					$userName = Get_Lang_Selection($val["ChineseName"], $val["EnglishName"]);
					$userInfo[$val['UserID']]["UserName"] = $userName;
				}
			}
		}
		
		if (count($cycleDays) > 0)
		{
			## get All Cycle Day Info
			$strSQL = "SELECT CycleDay, RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate IN ('" . implode("', '", $cycleDays) . "')";
			$result = $connection->returnResultSet($strSQL);
			if (count($result) > 0)
			{
				foreach ($result as $key => $val)
				{
					$cycleDayInfo[$val['RecordDate']] = $val["CycleDay"];
				}
			}
		}
		
		if (count($subjectGroupIDs) > 0)
		{
			## get All Class Info
			$strSQL = "SELECT SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5 FROM SUBJECT_TERM_CLASS WHERE SubjectGroupID IN ('" . implode("', '", $subjectGroupIDs) . "')";
			$result = $connection->returnResultSet($strSQL);
			if (count($result) > 0)
			{
				foreach ($result as $key => $val)
				{
					$ClassName = Get_Lang_Selection( ($indexVar['libslrs']->isEJ() ? convert2unicode($val["ClassTitleB5"],1,1) : $val["ClassTitleB5"]), $val["ClassTitleEN"]);
					$subjectGroupInfo[$val['SubjectGroupID']] = $val;
					$subjectGroupInfo[$val['SubjectGroupID']]["ClassName"] = $ClassName;
				}
			}
		}
		
		if (count($locationIDs) > 0)
		{
			## get All Location Info
			$locationInfo = $indexVar['libslrs']->getLocationInfoByIDs($locationIDs);
		}
		
		if (count($otherLocations) > 0)
		{
			## get All Others site
			$otherLocationsInfo = array();
			foreach ($otherLocations as $ol_key => $ol_val)
			{
				$strSQL = "SELECT 
								OthersLocation
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION 
							WHERE
								TimeSlotID='" . $ol_val["TimeSlotID"] . "'
								AND LocationID='" . $ol_val["LocationID"] . "'
								AND SubjectGroupID='" . $ol_val["SubjectGroupID"] . "'";
				if (isset($cycleDayInfo[$ol_val["Date"]]))
				{
					$strSQL .= " AND Day='" . $cycleDayInfo[$ol_val["Date"]] . "'";
				}
				$strSQL .= " LIMIT 1";
				$result = $connection->returnResultSet($strSQL);
				if (count($result) > 0)
				{
					foreach ($result as $key => $val)
					{
						if (!empty($val["TimeSlotID"]))
						{
							$otherLocationsInfo[$ol_val["TimeSlotID"]][$ol_val["SubjectGroupID"]][$ol_val["Date"]][$ol_val["LocationID"]] = $indexVar['libslrs']->isEJ() ? convert2unicode($val["OthersLocation"], 1,1) : $val["OthersLocation"] ; 
						}
					}
				}
			}
		}

		if (count($timeSlotIDs) > 0)
		{
			## get All Timeslot Info
			$strSQL = "SELECT TimeSlotID, TimeSlotName, StartTime, EndTime, DisplayOrder, RecordType, RecordStatus FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID IN ('". implode("', '", $timeSlotIDs). "')";
			$result = $connection->returnResultSet($strSQL);
			if (count($result) > 0)
			{
				foreach ($result as $key => $val)
				{
					$TimeSlotName = ($indexVar['libslrs']->isEJ() ? convert2unicode($val["TimeSlotName"],1,1) : $val["TimeSlotName"]);
					if (is_array($startTime = explode(":", $val["StartTime"])))
					{
						array_pop($startTime);
						$startTime = implode(":", $startTime);
					}
					else
					{
						$startTime = $val["StartTime"];
					}
					if (is_array($endTime = explode(":", $val["EndTime"])))
					{
						array_pop($endTime);
						$endTime = implode(":", $endTime);
					}
					else
					{
						$endTime = $val["EndTime"];
					}
					$timeSlotInfo[$val["TimeSlotID"]] = array(
															"TimeSlotName" => $TimeSlotName,
															"StartTime" => $startTime,
															"EndTime" => $endTime
														);
				}
			}
		}
	}
	$arr = array();
	if (count($groupInfo) > 0)
	{
		## finalize output data
		foreach ($groupInfo as $_groupID => $lesson)
		{
			$hasSubstitution = false;
			$teacherInfoArr = array();
			$arr[$_groupID]["teacherInfo"] = "";
			$arr[$_groupID]["exchangeDetail"] = "";
			if (count($lesson) > 0)
			{
			    /*** remove same lesson */
			    if (count($lesson) > 1) {
			        foreach ($lesson as $key => $val) {
			            if ($val["is_location_exchange"]) {
			                $lesson[$key]["is_location_exchange"] = false;
			            }
			        }
			    }
			    /* remove same lesson ***/
				foreach ($lesson as $key => $val)
				{
				    
					if (!isset($arr[$_groupID]["combined_type"]) || $arr[$_groupID]["combined_type"] != "MASTER")
					{
						$arr[$_groupID]["combined_type"] = $val["combined_type"];
						$arr[$_groupID]["is_location_exchange"] = $val["is_location_exchange"];
						
						if ($sys_custom['SLRS']["lessonExchangePromptAnotherLession"] && $sys_custom['SLRS']["lessonExchangePromptAnotherLessionForced"] && $arr[$_groupID]["combined_type"] == "MASTER")
						{
							$strSQL = "SELECT GroupID From INTRANET_SLRS_LESSON_EXCHANGE
										WHERE combined_id in (
											SELECT LessonExchangeID FROM INTRANET_SLRS_LESSON_EXCHANGE
												WHERE GroupID='" . $_groupID. "' AND combined_type='MASTER'
										)";
							$result = $indexVar["libslrs"]->returnResultSet($strSQL);
							if (count($result) == 0)
							{
								$arr[$_groupID]["combined_type"] = "NORMAL";
							}
						}
						
					}
					if (isset($locationInfo[$val["TeacherA_LocationID"]]))
					{
						$levelA = $locationInfo[$val["TeacherA_LocationID"]]["BuildingName"] . " > " . $locationInfo[$val["TeacherA_LocationID"]]["LevelName"];
						if (!empty($levelA))
						{
							$locationA = $levelA;
						}
						else
						{
							$locationA = "";
						}
						if ($locationInfo[$val["TeacherA_LocationID"]]["LocationName"])
						{
							if (!empty($locationA)) $locationA .= " > ";
							$locationA .= $locationInfo[$val["TeacherA_LocationID"]]["LocationName"];
						}
					}
					else 
					{
						if (isset($otherLocationsInfo[$val["TeacherA_TimeSlotID"]][$val["TeacherA_SubjectGroupID"]][$val["TeacherA_LocationID"]][$val["TeacherA_Date"]]))
						{
							
							$locationA = $otherLocationsInfo[$val["TeacherA_TimeSlotID"]][$val["TeacherA_SubjectGroupID"]][$val["TeacherA_LocationID"]][$val["TeacherA_Date"]];
						}
					}
					if (isset($locationInfo[$val["TeacherB_LocationID"]]))
					{
						$levelB = $locationInfo[$val["TeacherA_LocationID"]]["BuildingName"] . " > " . $locationInfo[$val["TeacherB_LocationID"]]["LevelName"];
						if (!empty($levelB))
						{
							$locationB = $levelB;
						}
						else 
						{
							$locationB = "";
						}
						if (isset($locationInfo[$val["TeacherB_LocationID"]]["LocationName"]))
						{
							if (!empty($locationB)) $locationB .= " > ";
							$locationB .= $locationInfo[$val["TeacherB_LocationID"]]["LocationName"];
						}
					}
					else
					{
						if (isset($otherLocationsInfo[$val["TeacherB_TimeSlotID"]][$val["TeacherB_SubjectGroupID"]][$val["TeacherB_LocationID"]][$val["TeacherB_Date"]]))
						{
							
							$locationB = $otherLocationsInfo[$val["TeacherB_TimeSlotID"]][$val["TeacherB_SubjectGroupID"]][$val["TeacherB_LocationID"]][$val["TeacherB_Date"]];
						}
					}
					// $arr[$_groupID]["teacherInfo"] .= (isset($userInfo[$val["TeacherA_UserID"]]["UserName"]) ? $userInfo[$val["TeacherA_UserID"]]["UserName"]: "-") . "<br>";
					if (isset($userInfo[$val["TeacherA_UserID"]]["UserName"]))
					{
					    $teacherInfoArr[] = $userInfo[$val["TeacherA_UserID"]]["UserName"];
					}
					
					
					
					/*****************************************************************/
					
					if ($val["is_location_exchange"]) {
					    $exchangeDetail_lesson = "<div class='slrs-card-time slrs-exchange-location'";
					} else {
					    $exchangeDetail_lesson = "<div class='slrs-card-time'";
					}
					$exchangeDetail_lesson .= " id='la_". $val["LessonExchangeID"];
					$exchangeDetail_lesson .= "'>";
					if (is_array($leaveDateInfo[$val["TeacherB_UserID"]]) && in_array($val["TeacherA_Date"], $leaveDateInfo[$val["TeacherB_UserID"]]))
					{
						$exchangeDetail = "<div class='slrs-card slrs-card-blue'>";
						// $hasSubstitution = true;
					}
					else 
					{
						$exchangeDetail = "<div class='slrs-card'>";
					}
					// $exchangeDetail_lesson .= '<div class="slrs-title">' . $val["TeacherB_Date"] . " " . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["EndTime"] . ")</div>";
					
					// $exchangeDetail .= $val["TeacherA_Date"] . " " . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["EndTime"] . ")";

					$timeslotA = "<div class='slrs-card-body'>";
					$timeslotA .= $val["TeacherA_Date"] . " " . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["EndTime"] . ") <br>";
					$timeslotA .= (isset($userInfo[$val["TeacherA_UserID"]]["UserName"]) ? $userInfo[$val["TeacherA_UserID"]]["UserName"]: "-") . "<br>";
					$timeslotA .= $subjectGroupInfo[$val['TeacherA_SubjectGroupID']]["ClassName"] . "<br>";
					$timeslotA .= $locationA . "<br>";
					$timeslotA .= "</div>";
					
					$timeslotA_lesson = '<div class="slrs-title">' . $val["TeacherB_Date"] . " " . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["EndTime"] . ")";
					if ($val["is_location_exchange"]) {
					    $timeslotA_lesson .= ' -- [' . $Lang['SLRS']['ExchangeLocation'] . ']';
					}
					$timeslotA_lesson .= '</div>';
					/*
					if (is_array($leaveDateInfo[$val["TeacherB_UserID"]]) && in_array($val["TeacherA_Date"], $leaveDateInfo[$val["TeacherB_UserID"]]))
					{
						$hasSubstitution = true;
						$timeslotA_lesson .= "<div class='slrs-card-body slrs-card-body-blue'>";
					}
					else
					{
						$timeslotA_lesson .= "<div class='slrs-card-body'>";
					}
					*/
					$timeslotA_lesson .= "<div class='slrs-card-body'>";
					$timeslotA_lesson .= "<div class='slrs-card-body-content'>";
					$timeslotA_lesson .= "<span class='slrs-oldtimeslot'>". (isset($userInfo[$val["TeacherB_UserID"]]["UserName"]) ? $userInfo[$val["TeacherB_UserID"]]["UserName"]: "-") . "</span><br>";
					$timeslotA_lesson .= "<span class='slrs-oldtimeslot'>". $subjectGroupInfo[$val['TeacherB_SubjectGroupID']]["ClassName"] . "</span><br>";
					// $timeslotA_lesson .= "<span class='slrs-oldtimeslot'>". $locationA. "</span><br>";
					// $timeslotA_lesson .= "<div class='slrs-oldtimeslot'>" . $val["TeacherB_Date"] . " " . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherA_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["EndTime"] . ")</div>";
					
// 					if (is_array($leaveDateInfo[$val["TeacherB_UserID"]]) && in_array($val["TeacherA_Date"], $leaveDateInfo[$val["TeacherB_UserID"]]))
// 					{
// 						$timeslotA_lesson .= "<div class='slrs-oldtimeslot'>( " . $Lang['SLRS']['SubstitutionArrangementHasLeaveRecord'] . " )</div>";
// 					}
 
					$timeslotA_lesson.= "</div></div>";

					$exchangeDetail .= $timeslotA;
					$exchangeDetail_lesson .= $timeslotA_lesson;
					
					if (is_array($leaveDateInfo[$val["TeacherA_UserID"]]) && in_array($val["TeacherA_Date"], $leaveDateInfo[$val["TeacherA_UserID"]]))
					{
						$exchangeDetail .= "<div style='text-align:right;color:#000'>(** " . $Lang['SLRS']['SubstitutionArrangementHasRecord'] . ")</div>";
					}
					$exchangeDetail .= "</div>";
					
					$arrow = "<div class='slrs-card-spa'>";
					// $arrow .= "<img src='/images/2009a/icon_SLRS_exchange.png'>";
					$arrow .= $Lang["SLRS"]["changeTo"];
					$arrow.= "</div>";
					
					$exchangeDetail .= $arrow;
 					$exchangeDetail_lesson .= $arrow;
					
					if (is_array($leaveDateInfo[$val["TeacherA_UserID"]]) && in_array($val["TeacherB_Date"], $leaveDateInfo[$val["TeacherA_UserID"]]))
					{
						$exchangeDetail .= "<div class='slrs-card slrs-card-red slrs-card-ex'>";
						// $hasSubstitution = true;
					}
					else
					{
						$exchangeDetail .= "<div class='slrs-card slrs-card-ex'>";
					}
					$exchangeDetail .= $val["TeacherB_Date"] . " " . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["EndTime"] . ")";
					
					$timeslotB = "<div class='slrs-card-body'>";
					$timeslotB .= (isset($userInfo[$val["TeacherA_UserID"]]["UserName"]) ? $userInfo[$val["TeacherA_UserID"]]["UserName"]: "-") . "<br>";
					$timeslotB .= $subjectGroupInfo[$val['TeacherA_SubjectGroupID']]["ClassName"] . "<br>";
					$timeslotB .= $locationB. "<br>";
					$timeslotB .= "</div>";
					$exchangeDetail .= $timeslotB;
					
					if (is_array($leaveDateInfo[$val["TeacherA_UserID"]]) && in_array($val["TeacherB_Date"], $leaveDateInfo[$val["TeacherA_UserID"]]))
					{
						$hasSubstitution = true;
						$timeslotB_lesson= "<div class='slrs-card-body slrs-card-body-yellow lessonB'>";
					}
					else
					{
						$timeslotB_lesson= "<div class='slrs-card-body lessonB'>";
					}
					$timeslotB_lesson .= "<div class='slrs-card-body-content'>";
					$timeslotB_lesson .= "<span class='slrs-lesson'>" . (isset($userInfo[$val["TeacherA_UserID"]]["UserName"]) ? $userInfo[$val["TeacherA_UserID"]]["UserName"]: "-") . "</span><br>";
					$timeslotB_lesson .= "<span class='slrs-lesson'>" . $subjectGroupInfo[$val['TeacherA_SubjectGroupID']]["ClassName"] . "</span><br>";
// 					if ($val["TeacherA_LocationID"] != $val["TeacherB_LocationID"])
// 					{
// 						$timeslotB_lesson.= "<span class='slrs-chg-location'>" . $locationB. "</span><br>";
// 					}
// 					else
// 					{
// 						$timeslotB_lesson .= $locationB. "<br>";
// 					}
					$timeslotB_lesson .= "<span class='slrs-lesson lessonB_location'>" . $locationB. "</span><br>";
					if ($sys_custom['SLRS']["allowSubstitutionRemarks"] && !empty($val['LessonRemarks']))
					{
					    $timeslotB_lesson .= "<span class='slrs-lesson lessonB_location'>" . $Lang["SLRS"]["LessonRemarks"] . ": " . nl2br($val['LessonRemarks']) . "</span><br>";
					}
					
					// $timeslotB_lesson .= "<div class='slrs-oldtimeslot'>" . $val["TeacherB_Date"] . " " . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["TimeSlotName"] . " (" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["StartTime"] . "-" . $timeSlotInfo[$val["TeacherB_TimeSlotID"]]["EndTime"] . ")</div>";
					
					if (is_array($leaveDateInfo[$val["TeacherA_UserID"]]) && in_array($val["TeacherB_Date"], $leaveDateInfo[$val["TeacherA_UserID"]]))
					{
						$timeslotB_lesson .= "<div class='slrs-leave'>( <i class='fa fa-info-circle' aria-hidden='true'></i> " . $Lang['SLRS']['SubstitutionArrangementHasAlreadySRRecord'] . " )</div>";
					}
					
					$timeslotB_lesson .= "</div></div>";
					$exchangeDetail_lesson .= $timeslotB_lesson;
					if ($sys_custom['SLRS']["lessonExchangePromptAnotherLession"] && $sys_custom['SLRS']["lessonExchangePromptAnotherLessionForced"])
					{
						/* Another Subject Group for Forced exchange */
						$another_param = array( "LessonExchangeID" => $val["LessonExchangeID"] );
						$anotherInfo = $indexVar['libslrs']->getAnotherSubjectGroupByExchangeRecord($another_param, $getTeacherInfo = true);

						if (count($anotherInfo) > 0)
						{
							if (count($anotherInfo) > 0)
							{
								$anotherSubjectButton = "";
								foreach ($anotherInfo as $resIndex => $resVal)
								{
									$token = md5($resVal["GroupID"] . "_" . $resVal["LessonExchangeID"] . "_" . $resVal["TeacherA_SubjectGroupID"] . "_" . $resVal["SubjectGroupID"] . "_SLRS_FORCE_EXC");
									$anotherLink = "?task=management.lesson_exchange.combined_exchange&gpid=" . $resVal["GroupID"] . "&laid=" . $resVal["LessonExchangeID"] . "&osg=" . $resVal["TeacherA_SubjectGroupID"] . "&nsg=" . $resVal["SubjectGroupID"] . "&exchangetoken=" . $token;
									
									$anotherSubjectButton .= "<a href='#' rel='" . $anotherLink . "' class='anotherForcedExchangeBtn' data-id='la_" . $resVal["LessonExchangeID"] . "'>";
									$anotherSubjectButton .= ($intranet_session_language == "en" ? $resVal["EnglishName"] : $resVal["ChineseName"]) . "<br>";
									$anotherSubjectButton .= ($intranet_session_language == "en" ? $resVal["ClassTitleEN"] : $resVal["ClassTitleB5"]) . "<br>";
									if (isset($resVal["location"]))
									{
										
										$_location = $resVal["location"]["BuildingName"] . " > " . $resVal["location"]["LevelName"];
										if (!empty($_location))
										{
											$location_another = $_location;
										}
										else
										{
											$location_another = "";
										}
										if ($resVal["location"]["LocationName"])
										{
											if (!empty($location_another)) $location_another .= " > ";
											$location_another .= $resVal["location"]["LocationName"];
										}
										$anotherSubjectButton .= "<div class='location'>" . $location_another . "</div>";
									}
									$anotherSubjectButton .= "</a>";
								}
								$exchangeDetail_lesson.= "<div class='anotherinfo'><br>" . $Lang['SLRS']['ForcedExchangeMsg'] . "<br>" . $anotherSubjectButton . "</div>";
							}
						}
						else
						{
							$exchangeDetail_lesson .= "<div class='slrs-clear'>&nbsp;</div>";
						}
						/* END Another Subject Group for Forced exchange */
					}
					else
					{
						$exchangeDetail_lesson .= "<div class='slrs-clear'>&nbsp;</div>";
					}
					$exchangeDetail_lesson .= "</div>";
					
// 					$exchangeDetail_lesson .= $timeslotB . "<br>";
					
					if (is_array($leaveDateInfo[$val["TeacherA_UserID"]]) && in_array($val["TeacherB_Date"], $leaveDateInfo[$val["TeacherA_UserID"]]))
					{
						$exchangeDetail .= "<div style='text-align:right;color:#000'>(** " . $Lang['SLRS']['SubstitutionArrangementHasAlreadySRRecord'] . ")</div>";
					}
					$exchangeDetail .= "</div>";
					$exchangeDetail .= "<div class='slrs-clear'>&nbsp;</div>";
					
					// $arr[$_groupID]["exchangeDetail"] .= $exchangeDetail;
					$arr[$_groupID]["exchangeDetail"] .= $exchangeDetail_lesson;
					// $arr[$_groupID]["exchangeDetail"] .= $exchangeDetail . $exchangeDetail_lesson;
				}
				if (count($teacherInfoArr) > 0)
				{
				    $arr[$_groupID]["teacherInfo"] = implode("<br>", array_unique($teacherInfoArr)); 
				}
				else
				{
				    $arr[$_groupID]["teacherInfo"] = '-';
				}
			}
			$arr[$_groupID]["hasSubstitution"] = $hasSubstitution;
		}
	}

	$jsonObj = new JSON_obj();
	echo $jsonObj->encode($arr);
} else if ($_POST["type"] == "checklesson")
{
	if (isset($_POST["startIndex"]) && $_POST["startIndex"] > 0 && isset($_POST["endIndex"]) && $_POST["endIndex"] > 0 && $_POST["startIndex"] < $_POST["endIndex"])
	{
		$records = array();
		$status = true;
		for($i=$_POST["startIndex"]; $i<=$_POST["endIndex"]; $i++)
		{
			$session = "session_" . ceil($i / 2);
			$lesson = (($i % 2) == 1) ? $type = "1" : $type = "2";
			$records[$session][$lesson]["group"] = ceil($i / 2);
			$records[$session][$lesson]["i"] = $i;
			$records[$session][$lesson]["date"] = $_POST["datePicker_". $i]; 
			$records[$session][$lesson]["teacher"] = $_POST["teachingTeacher_". $i];
			if (isset($_POST["timeSlot_". $i]))
			{
				$records[$session][$lesson]["timeslot"] = $_POST["timeSlot_". $i];
			}
			else 
			{
				$records[$session][$lesson]["timeslot"] = $_POST["timeSlotID_". $i];
			}
			$records[$session][$lesson]["subject_group"] = $_POST["subjectGroupID_". $i];
			$records[$session][$lesson]["year_class"] = $_POST["yearClassID_". $i];
			$records[$session][$lesson]["subject"] = $_POST["subjectID_". $i];
			$records[$session][$lesson]["location"] = $_POST["locationID_". $i];
			$records[$session][$lesson]["startTime"] = $_POST["startTime_". $i];
			$records[$session][$lesson]["endTime"] = $_POST["endTime_". $i];
			$records[$session][$lesson]["LessonRemarks"] = $_POST["LessonRemarks_". $i];
			
			if ($lesson == 2)
			{
				$cycleDays[] = $records[$session][$lesson]["date"];
			}
		}
		if (count($records) > 0) {
			if (count($cycleDays) > 0)
			{
				## get All Cycle Day Info
				$strSQL = "SELECT CycleDay, RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate IN ('" . implode("', '", $cycleDays) . "')";
				$result = $connection->returnResultSet($strSQL);
				if (count($result) > 0)
				{
					foreach ($result as $key => $val)
					{
						$cycleDayInfo[$val['RecordDate']] = $val["CycleDay"];
					}
				}
			}
			
			$allowChangeLocationOnly = false;
			foreach ($records as $key => $val) {
				if ($val[1]['teacher'] == $val[2]['teacher'] && 
					$val[1]['date'] == $val[2]['date'] &&
				    $val[1]['timeslot'] == $val[2]['timeslot']  &&
				    $val[1]['subjectgroup'] == $val[2]['subjectgroup'] &&
				    ( $val[1]['location'] == $val[2]['location'] && count($records) == 1 || count($records) > 1 ))
				{
					$status = false;
					$records[$key][2]["errorMsg"] = $Lang['SLRS']['LessonExchangeDes']['CannotExchangeMyLesson'];
				}
				else if ($val[1]["teacher"] == $val[2]["teacher"] && ($val[1]["timeslot"] != $val[2]["timeslot"] || $val[1]["date"] != $val[2]["date"]))
				{
					$records[$key][2]["hasLesson"] = false;
				}
				else
				{
					
					if ($val[1]['teacher'] == $val[2]['teacher'] &&
							$val[1]['date'] == $val[2]['date'] &&
							$val[1]['timeslot'] == $val[2]['timeslot'] &&
							$val[1]['subjectgroup'] == $val[2]['subjectgroup'] && $allowChangeLocationOnly)
					{
						$records[$key][2]["hasLesson"] = false;
					}
					else 
					{
						$strSQL = "SELECT
								RoomAllocationID, EnglishName, ChineseName
							FROM
								INTRANET_TIMETABLE_ROOM_ALLOCATION as itra
							INNER JOIN
								SUBJECT_TERM_CLASS_TEACHER as stct ON (itra.SubjectGroupID = stct.SubjectGroupID)
							INNER JOIN
								".$slrsConfig['INTRANET_USER']." AS iu ON (stct.UserID =iu.UserID)
							WHERE Day='" . $cycleDayInfo[$val[2]["date"]] . "' AND TimeSlotID='" . $val[2]["timeslot"] . "' AND stct.UserID='" . $val[1]["teacher"] . "'";
						
						$result = $connection->returnResultSet($strSQL);
						if (count($result) > 0)
						{
							## check Exchanged lesson
							$strSQL = "SELECT
										LessonExchangeID FROM INTRANET_SLRS_LESSON_EXCHANGE
									WHERE
										TeacherA_UserID='" . $val[1]["teacher"] . "' AND 
                                        TeacherA_Date='" . $val[2]["date"] . "' AND
                                        TeacherA_TimeSlotID='" . $val[2]["timeslot"] . "'";
							$result = $connection->returnResultSet($strSQL);
							if (count($result) > 0)
							{
								$records[$key][2]["hasLesson"] = false;
							}
							else
							{
								if ($sys_custom['SLRS']["enableCancelLesson"])
								{
								    $strSQL = "SELECT 
                                                    CancelLessonID
                                                FROM
                                                    INTRANET_SLRS_CANCEL_LESSON 
                                                WHERE
                                                    UserID='" . $val[1]["teacher"] . "' AND
                                                    CancelDate='" . $val[2]["date"] . "' AND
                                                    TimeSlotID='" . $val[2]["timeslot"] . "' limit 1";
								    $result = $connection->returnResultSet($strSQL);
								    if (count($result) > 0)
								    {
								        $records[$key][2]["hasLesson"] = false;
								    } else {
								        $records[$key][2]["hasLesson"] = true;
								        $status = false;
								        $records[$key][2]["errorMsg"] = Get_Lang_Selection($result[0]["ChineseName"], $result[0]["EnglishName"]) . " " . $Lang['SLRS']['LessonExchangeDes']['TimeslotIsNotEmpty'];
								    }
								} else {
								    $records[$key][2]["hasLesson"] = true;
								    $status = false;
								    $records[$key][2]["errorMsg"] = Get_Lang_Selection($result[0]["ChineseName"], $result[0]["EnglishName"]) . " " . $Lang['SLRS']['LessonExchangeDes']['TimeslotIsNotEmpty'];
								}
							}
						}
						else
						{
							$records[$key][2]["hasLesson"] = false;
						}
					}
 				}
			}
		}
		$arr = array( 'result' => $status, 'data' => $records);
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($arr);
	}
	else 
	{
		$arr = array( 'result' => false );
		$jsonObj = new JSON_obj();
		echo $jsonObj->encode($arr);
	}
}

?>