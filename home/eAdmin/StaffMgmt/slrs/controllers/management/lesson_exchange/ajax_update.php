<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

$connection = new libgeneralsettings();

$startIndex = $_POST["startIndex"];
$endIndex = $_POST["endIndex"];
$groupID = $_POST["rowGroupID"];

$i = $startIndex;
$sequence = $i;

// echo "<pre>";
// print_r($_POST);
// echo "<pre>";

$saveSuccess = false;

function TextSafe($val, $rev = false) {
	$replaceArr = array(
			'"' => "&#34;",
			"'" => "&#39;",
	);
	return str_replace(array_keys($replaceArr), array_values($replaceArr), $val);
}


if (!isset($_POST["endIndex"])) {
	$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
	header('Location: ?task=management'.$slrsConfig['taskSeparator'].'lesson_exchange'.$slrsConfig['taskSeparator'].'edit&returnMsgKey='.$returnMsgKey);
	exit;
}

if($groupID == "" || $groupID == "NaN" || empty($groupID)) {
	$sql = "SELECT MAX(GroupID) as GroupID FROM INTRANET_SLRS_LESSON_EXCHANGE; ";
	$a = $connection->returnArray($sql);
	$groupID = ($a[0]["GroupID"]=="")?"1":$a[0]["GroupID"]+1;
}

//echo $groupID;
//for($i=$startIndex; $i<= $endIndex; $i++){
while ($i<= $endIndex){	
	$j = $i+1;
	
	$sql = "SELECT IF(EXISTS(SELECT LessonExchangeID FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE GroupID='".$groupID."' AND SequenceNumber='".$sequence."'),";
	$sql .= "( SELECT 'UPDATE'";
	$sql .="),( SELECT 'INSERT'";
	$sql .=")) as result;";
	$a = $connection->returnResultSet($sql);

	if($a[0]["result"]=="INSERT"){
		$sql = "INSERT INTO INTRANET_SLRS_LESSON_EXCHANGE(TeacherA_UserID,TeacherA_Date,TeacherA_TimeSlotID,TeacherA_TimeStart,TeacherA_TimeEnd,";
		$sql .= "TeacherA_SubjectGroupID,TeacherA_YearClassID,TeacherA_SubjectID,TeacherA_LocationID,";
		$sql .= "TeacherB_UserID,TeacherB_Date,TeacherB_TimeSlotID,TeacherB_TimeStart,TeacherB_TimeEnd,TeacherB_SubjectGroupID,TeacherB_YearClassID,TeacherB_SubjectID,";
		$sql .= "TeacherB_LocationID,GroupID,SequenceNumber,LessonRemarks,";
		$sql .= "InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID)";
		
		$sql .= "SELECT ".IntegerSafe($_POST["teachingTeacher_".$i]).",'".$_POST["datePicker_".$i]."','".IntegerSafe($_POST["timeSlotID_".$i])."','".$_POST["datePicker_".$i]." ".$_POST["startTime_".$i]."',";
		$sql .= "'".$_POST["datePicker_".$i]." ".$_POST["endTime_".$i]."','".IntegerSafe($_POST["subjectGroupID_".$i])."','".IntegerSafe($_POST["yearClassID_".$i])."','".$_POST["subjectID_".$i]."',";
		$sql .= "'".$_POST["locationID_".$i]."',";
		
		$sql .= "'".$_POST["teachingTeacher_".$j]."','".$_POST["datePicker_".$j]."','".IntegerSafe($_POST["timeSlotID_".$j])."','".$_POST["datePicker_".$j]." ".$_POST["startTime_".$j]."',";
		$sql .= "'".$_POST["datePicker_".$j]." ".$_POST["endTime_".$j]."','".IntegerSafe($_POST["subjectGroupID_".$j])."','".$_POST["yearClassID_".$j]."',";
		$sql .= "'".$_POST["subjectID_".$j]."','".$_POST["locationID_".$j]."','".IntegerSafe($groupID)."','".IntegerSafe($sequence)."', '" . TextSafe($_POST["LessonRemarks_".$i]) . "','".$_SESSION['UserID']."',NOW()".",NOW(),'".$_SESSION['UserID']."';";
	}
	else if($a[0]["result"]=="UPDATE"){
		$sql = "UPDATE INTRANET_SLRS_LESSON_EXCHANGE SET TeacherA_UserID=".IntegerSafe($_POST["teachingTeacher_".$i]).",TeacherA_Date='".$_POST["datePicker_".$i]."',TeacherA_TimeSlotID=".IntegerSafe($_POST["timeSlotID_".$i]).",";
		$sql .= "TeacherA_TimeStart='".$_POST["datePicker_".$i]." ".$_POST["startTime_".$i]."',TeacherA_TimeEnd='".$_POST["datePicker_".$i]." ".$_POST["endTime_".$i]."',TeacherA_SubjectGroupID='".IntegerSafe($_POST["subjectGroupID_".$i])."',";
		$sql .= "TeacherA_YearClassID='".IntegerSafe($_POST["yearClassID_".$i])."',TeacherA_SubjectID='".$_POST["subjectID_".$i]."',TeacherA_LocationID='".$_POST["locationID_".$i]."',";
		
		$sql .= "TeacherB_UserID='".$_POST["teachingTeacher_".$j]."',TeacherB_Date='".$_POST["datePicker_".$j]."',TeacherB_TimeSlotID='".IntegerSafe($_POST["timeSlotID_".$j])."',";
		$sql .= "TeacherB_TimeStart='".$_POST["datePicker_".$j]." ".$_POST["startTime_".$j]."',TeacherB_TimeEnd='".$_POST["datePicker_".$j]." ".$_POST["endTime_".$j]."',TeacherB_SubjectGroupID='".IntegerSafe($_POST["subjectGroupID_".$j])."',";
		$sql .= "TeacherB_YearClassID='".$_POST["yearClassID_".$j]."',TeacherB_SubjectID='".$_POST["subjectID_".$j]."',TeacherB_LocationID='".$_POST["locationID_".$j]."', LessonRemarks='".TextSafe($_POST["LessonRemarks_". $i])."', DateTimeModified=NOW(),ModifiedBy_UserID='".$_SESSION['UserID']."'";
		$sql .= "WHERE GroupID=".IntegerSafe($groupID)." AND SequenceNumber=".IntegerSafe($sequence).";";
	}
	$a = $connection->db_db_query($sql);
	// echo $sql . "<br><br>";	
	$saveSuccess = true;
	$sequence = $sequence +1 ;
	$i = $i+2;
}
// exit;
// delete the remained records

$sequenceNumber = $endIndex /2;
$sql = "DELETE FROM INTRANET_SLRS_LESSON_EXCHANGE WHERE GroupID=".$groupID." AND SequenceNumber >".$sequenceNumber;
$a = $connection->db_db_query($sql);
//echo $sql;

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'lesson_exchange'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>