<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$connection = new libgeneralsettings();

$date = $_POST['datePicker'];
$substitution = $_POST['substitution'];
$userID = $_POST['leaveTeacher'];
$reason = $_POST['reason'];
$session = $_POST['session'];

$sql = "INSERT INTO INTRANET_SLRS_LEAVE(UserID,DateLeave,ReasonCode,Duration,SubstitutionArrangement ,InputBy_UserID,DateTimeInput,DateTimeModified,ModifiedBy_UserID) ";
$sql .= "SELECT '".IntegerSafe($userID)."','".$date."','".$reason."','".$session."','LATER','".$_SESSION['UserID']."',NOW()".",NOW(),'".$_SESSION['UserID']."';";

$a = $connection->db_db_query($sql);

$sql = "SELECT LeaveID FROM INTRANET_SLRS_LEAVE WHERE UserID='".IntegerSafe($userID)."' AND DateLeave='".$date."' AND ReasonCode='".$reason."' AND Duration='".$session."';";

$a = $connection->returnResultSet($sql);

$leaveID = $a[0]["LeaveID"];

$saveSuccess = true;
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';

if($substitution == "NOW"){
	header('Location: ?task=management'.$slrsConfig['taskSeparator'].'substitute_arrangement'.$slrsConfig['taskSeparator'].'edit_details&leaveID='.$leaveID);	
}
else{
	header('Location: ?task=management'.$slrsConfig['taskSeparator'].'substitute_arrangement'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);
}
