<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$type = $_POST['type'];
$date = $_POST['date'];
$session = $_POST['session'];
$userID = $_POST['userID'];

$connection = new libgeneralsettings();
$arr = array();
$jsonObj = new JSON_obj();

// get the original userID if this user is supply teacher; else the original userID is that teacher's ID
$userID = $indexVar['libslrs']->getOriginalTeacherPeriod($userID, $date);

if ($type == "checkSettings")
{
	$result = $indexVar["libslrs"]->checkSettingsLimitation($_POST);
	echo $jsonObj->encode($result);
	exit;
}
else if($type == "checkSession" && $session=="am")
{
	$sql = "SELECT LeaveID, Duration FROM INTRANET_SLRS_LEAVE WHERE UserID=".IntegerSafe($userID)." AND DateLeave='".$date."' AND Duration IN ('am','wd');";
	$leaveRecords = $connection->returnResultSet($sql);
	if(sizeof($leaveRecords)>0){
	    $is_whole_day = false;
	    $is_am = false;
	    $is_pm = false;
	    foreach ($leaveRecords as $leaveRecord)
	    {
	        if ($leaveRecord["Duration"] == "wd")
	        {
	            $is_whole_day = true;
	        }
	        if ($leaveRecord["Duration"] == "am")
	        {
	            $is_am = true;
	        }
	        if ($leaveRecord["Duration"] == "pm")
	        {
	            $is_pm = true;
	        }
	        if ($is_am && $is_pm)
	        {
	            $is_whole_day = true;
	        }
	        if ($is_whole_day) break;
	    }
	    if ($is_whole_day)
	    {
	        $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['WholeDayError']);
	    }
	    else if ($is_am)
	    {
	        $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['AMExistError']);
	    }
	    else if ($is_pm)
	    {
	        $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['PMExistError']);
	    }
	}
	else if(sizeof($leaveRecords)==0){
		$b = checkLesson($connection,$date,$userID,$session);
		$c = checkSupplyPeriod($connection, $date, $userID);
		if ($b == -199)
		{
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitution']);
		}
		else if(sizeof($b)==0)
		{
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['NoLesson']);
		}
		/*else if(sizeof($c)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['ExistSupplyTeacherSubstitution']);
		}*/
		else{
			$arr[] = array('result' => "0", 'msg' => "");
		}
	}
	echo $jsonObj->encode($arr);
	
}
else if($type == "checkSession" && $session=="pm")
{
	$sql = "SELECT LeaveID, Duration FROM INTRANET_SLRS_LEAVE WHERE UserID=".IntegerSafe($userID)." AND DateLeave='".$date."' AND Duration IN ('pm','wd');";
	$leaveRecords = $connection->returnResultSet($sql);
	if(sizeof($leaveRecords)>0){
	    $is_whole_day = false;
	    $is_am = false;
	    $is_pm = false;
	    foreach ($leaveRecords as $leaveRecord)
	    {
	        if ($leaveRecord["Duration"] == "wd")
	        {
	            $is_whole_day = true;
	        }
	        if ($leaveRecord["Duration"] == "am")
	        {
	            $is_am = true;
	        }
	        if ($leaveRecord["Duration"] == "pm")
	        {
	            $is_pm = true;
	        }
	        if ($is_am && $is_pm)
	        {
	            $is_whole_day = true;
	        }
	        if ($is_whole_day) break;
	    }
	    if ($is_whole_day)
	    {
	        $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['WholeDayError']);
	    }
	    else if ($is_am)
	    {
	        $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['AMExistError']);
	    }
	    else if ($is_pm)
	    {
	        $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['PMExistError']);
	    }
	}
	else if(sizeof($leaveRecords)==0){
		$b = checkLesson($connection,$date,$userID,$session);
		$c = checkSupplyPeriod($connection, $date, $userID);
		if ($b == -199)
		{
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitution']);
		}
		else if(sizeof($b)==0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['NoLesson']);
		}
		/*else if(sizeof($c)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['ExistSupplyTeacherSubstitution']);
		}*/
		else{
			$arr[] = array('result' => "0", 'msg' => "");
		}
	}
	echo $jsonObj->encode($arr);
}
else if($type == "checkSession" && $session=="wd"){
	$sql = "SELECT LeaveID FROM INTRANET_SLRS_LEAVE WHERE UserID=".IntegerSafe($userID)." AND DateLeave='".$date."';";
	$a = $connection->returnResultSet($sql);
	if(sizeof($a)>0){
	    $arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['WholeDayError']);
	}
	else if(sizeof($a)==0){
		$b = checkLesson($connection,$date,$userID,$session);
		$c = checkSupplyPeriod($connection, $date, $userID);
		if ($b == -199)
		{
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitution']);
		}
		else if(sizeof($b)==0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['NoLesson']);
		}
		/*else if(sizeof($c)>0){
			$arr[] = array('result' => "1", 'msg' => $Lang['SLRS']['SubstitutionArrangementDes']['ExistSupplyTeacherSubstitution']);
		}*/
		else{
			$arr[] = array('result' => "0", 'msg' => "");
		}
	}
	echo $jsonObj->encode($arr);
}

function checkLesson($connection,$date,$userID,$session){
	/*
	global $PATH_WRT_ROOT;
	
	include_once($PATH_WRT_ROOT."includes/libtimetable.php");
	
	$indexVar['libslrs'] = new libslrs();
	$ltimetable = new Timetable();
	$curTimetableId = $ltimetable->Get_Current_Timetable();
	
	$session = ($session!= "am" && $session!= "pm")?"%":$session;
	
	//$sql = "SELECT RecordDate,CycleDay FROM INTRANET_CYCLE_DAYS WHERE RecordDate='".$date."'";
	$dateArr[0]=$date;
	$sql=$indexVar['libslrs']->getDaySQL($dateArr,"");
	$a = $connection->returnResultSet($sql);
	$day = $a[0]["CycleDay"];	
	
	$sql = "SELECT *
		FROM (
			SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE SessionType like '".$session."'
		) as itt
		LEFT JOIN(
			SELECT RoomAllocationID,Day,itra.TimeSlotID,itra.LocationID,itra.SubjectGroupID,OthersLocation,RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,
			NameChi,NameEng,DisplayOrder,Code,BarCode,SubjectClassTeacherID,stct.UserID,EnglishName,ChineseName,
			SubjectTermID,SubjectID,YearTermID,isl.LeaveID,islUserID,DateLeave,LessonArrangementID,ArrangedTo_UserID,ArrangedTo_LocationID
			FROM(
				SELECT RoomAllocationID,Day,TimeSlotID,LocationID,SubjectGroupID,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." and TimetableID='".$curTimetableId."') as itra
				LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON itra.Day=icd.CycleDay
				LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
				LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) as il ON itra.LocationID = il.LocationID
				LEFT JOIN (SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".IntegerSafe($userID).") as stct ON itra.SubjectGroupID=stct.SubjectGroupID
				LEFT JOIN (SELECT UserID,EnglishName,ChineseName FROM INTRANET_USER) as iu ON stct.UserID=iu.UserID
				LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) as st ON itra.SubjectGroupID = st.SubjectGroupID
				LEFT JOIN (SELECT LeaveID,UserID as islUserID,DateLeave FROM INTRANET_SLRS_LEAVE WHERE DateLeave='".$date."') as isl ON stct.UserID=isl.islUserID
				LEFT JOIN (SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID
						FROM INTRANET_SLRS_LESSON_ARRANGEMENT) as isla ON isl.islUserID=isla.UserID AND isl.LeaveID=isla.LeaveID
						AND itra.TimeSlotID=isla.TimeSlotID
			WHERE stct.UserID IS NOT NULL
		) as a ON itt.TimeSlotID=a.TimeSlotID
		WHERE SubjectGroupID IS NOT NULL;
	";
	$a = $connection->returnResultSet($sql);
	return $a;	
	//echo $sql;
	 * 
	 */
	$indexVar['libslrs'] = new libslrs();
	return $indexVar['libslrs']->checkLesson($date,$userID,$session);
}	

function checkSupplyPeriod($connection,$date,$userID){
	$sql = "SELECT *
			FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE '".$date."' BETWEEN DateStart AND DateEnd AND TeacherID=".IntegerSafe($userID)."		
		";
	$a = $connection->returnResultSet($sql);
	return $a;
}
	



?>