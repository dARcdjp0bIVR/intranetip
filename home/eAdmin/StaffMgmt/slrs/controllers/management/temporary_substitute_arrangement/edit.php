<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
if($indexVar['libslrs']->isEJ())
{
	include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
}
else
{
	include_once($PATH_WRT_ROOT."includes/libtimetable.php");
}
$ltimetable = new Timetable();

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['TemporarySubstitutionArrangement']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$arrangementID = $_GET["arrangementID"];

if (empty($arrangementID))
{
	if (isset($_GET["selected_date"]))
	{
		$date = $_GET["selected_date"];
	}
	else
	{
		$date = date("Y-m-d");
	}
	
}
else 
{
	$sql = "
		SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,DATE_FORMAT(TimeStart,'%Y-%m-%d') as date FROM INTRANET_SLRS_LESSON_ARRANGEMENT
		WHERE LessonArrangementID=".IntegerSafe($arrangementID)."
		";
	//echo $sql;
	$basicInformation = $connection->returnResultSet($sql);
	$date = $basicInformation[0]["date"];
}

$academicTimeSlot = convertMultipleRowsIntoOneRow($indexVar['libslrs']->getAcademicTimeSlot($date),"TimeSlotID");
$curTimetableId = $ltimetable->Get_Current_Timetable($date);
//echo $date;

//$a = $indexVar['libslrs']->getLessonLimitation($arrangementID,$date);

$currentDate = $date;


$TermInfoArr = getCurrentAcademicYearAndYearTerm();
$academicYearID = $TermInfoArr['AcademicYearID'];
$Year_StartDate = getStartDateOfAcademicYear($academicYearID);
$Year_EndDate = getEndDateOfAcademicYear($academicYearID);

$dateLimitation = array();
$dateLimitation['min']["Y"] = date("Y", strtotime($Year_StartDate));
$dateLimitation['min']["M"] = date("m", strtotime($Year_StartDate)) - 1;
$dateLimitation['min']["D"] = date("d", strtotime($Year_StartDate));
$dateLimitation['max']["Y"] = date("Y", strtotime($Year_EndDate));
$dateLimitation['max']["M"] = date("m", strtotime($Year_EndDate)) - 1;
$dateLimitation['max']["D"] = date("d", strtotime($Year_EndDate));

$PresetLocationInfo = $indexVar['libslrs']->getPresetLocationInfo($arrangementID, $date, TRUE);
$expiredUserIDs = $indexVar['libslrs']->getExpiredUserIDs();

if($arrangementID == ""){
	$x = "<table class=\"form_table_v30 slrs_form\" style='max-width:500px;'>"."\r\n";
		$x .= "<tr>"."\r\n";
			$x .= "<td style='width:200px;'>".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date']."</td><td>:</td>"."\r\n";
			// $x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $currentDate,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
			$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='datePicker' id='datePicker' value='" . $currentDate . "' />";
			if (isset($err["datePicker"])) {
				$x .= " <span class='error' style='color:#f00;'>" . $err["datePicker"] . "</span>";
			}
			$x .= "</td>";
		$x .= "</tr>"."\r\n";
		$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['LeaveTeacher'].$indexVar['libslrs_ui']->RequiredSymbol()."</td><td>:</td>"."\r\n";
			/***
			// $indexVar['libslrs']->getTeachingTeacherDate($currentDate)
			$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($currentDate);
			$finalTeacherList = array();
			if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"]) {
				$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
				$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($currentDate, TRUE);
				$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
				
				if (count($teacherList) > 0 && count($ignore_teacherList)) {
					$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
					if (count($finalTeacherList) > 0) {
						$finalTeacherList = array_values($finalTeacherList);
					}
				} else {
					$finalTeacherList = array_values($teacherList);
				}
			} else {
				$finalTeacherList = array_values($teacherList);
			}
			**/
			/***********************************************************************/
			// $teacherList = $indexVar['libslrs']->getTeachingTeacherDate($date);
			$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($currentDate, $fromExchange=true);
			/***********************************************************************/
			if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"]) {
				$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
				$teacherList = $indexVar['libslrs']->getExchangedTeachersByDate($currentDate, $teacherList);
				
				$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($currentDate, TRUE);
				$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
				
				if (count($teacherList) > 0 && count($ignore_teacherList)) {
					$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
					if (count($finalTeacherList) > 0) {
						$finalTeacherList = array_values($finalTeacherList);
					}
				} else {
					$finalTeacherList = array_values($teacherList);
				}
			} else {
				$teacherList = $indexVar['libslrs']->getExchangedTeachersByDate($date, $teacherList);
				$finalTeacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
				$finalTeacherList = array_values($finalTeacherList);
			}
			
			$x .= "<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="leaveTeacher" class="js-select2 js-basic-single" name="leaveTeacher"', $SelectedType="", $all=0, $noFirst=0)."</td>"."\r\n";
		$x .= "</tr>"."\r\n";
		$x .= "<tr>"."\r\n";
		$x .= "<td>".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Room'].$indexVar['libslrs_ui']->RequiredSymbol()."</td><td>:</td>"."\r\n";
			$x .= "<td>".getSelectByAssoArray("", $selectionTags='id="lesson" class="js-select2 js-basic-single" name="lesson"', $SelectedType="", $all=0, $noFirst=0);
			$x .= $indexVar['libslrs_ui']->Get_Form_Warning_Msg('lessonEmptyWarnDiv', '', $Class='warnMsgDiv');
			$x .= "</td>"."\r\n";
		$x .= "</tr>"."\r\n";
	$x .= "</table>";
	$x .= "<div class=\"slrs_legend\"><strong>".$Lang['SLRS']['TempSubstitutionArrangement']['StrongSubstitutedTeacher']."</strong>";
	$x .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>".$Lang['SLRS']['TempSubstitutionArrangement']['EmClassTeacher']."</em></div>";	
	$x .= "<br/><br/>";	
	$x .= "<table id=\"lessonDetails\" class=\"common_table_list slrs_list\">";				
	$x .="</table>";
}
else{
	$x = "<table class=\"form_table_v30 slrs_form\" style='max-width:500px;'>"."\r\n";
		$x .= "<tr>"."\r\n";
			$x .= "<td style='width:200px;'>".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date']."</td><td>:</td>"."\r\n";
			// $x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $basicInformation[0]["date"],  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
			
			$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='datePicker' id='datePicker' value='" . $basicInformation[0]["date"] . "' />";
			if (isset($err["datePicker"])) {
				$x .= " <span class='error' style='color:#f00;'>" . $err["datePicker"] . "</span>";
			}
			$x .= "</td>";
			
		$x .= "</tr>"."\r\n";
		$x .= "<tr>"."\r\n";
			$x .= "<td>".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['LeaveTeacher']."</td><td>:</td>"."\r\n";
			
// 			// $indexVar['libslrs']->getTeachingTeacherDate($basicInformation[0]["date"])
// 			$teacherList = $indexVar['libslrs']->getLessonTeacherByDate($basicInformation[0]["date"], true);
// 			$finalTeacherList = array();
// 			if ($sys_custom['SLRS']["disallowSubstitutionAtOneDate"]) {
// 				$teacherList = BuildMultiKeyAssoc($teacherList, 'UserID');
// 				$ignore_teacherList = $indexVar['libslrs']->getTeachingTeacherDate($basicInformation[0]["date"], TRUE);
// 				$ignore_teacherList = BuildMultiKeyAssoc($ignore_teacherList, 'UserID');
				
// 				if (isset($ignore_teacherList[$basicInformation[0]["UserID"]])) {
// 					unset($ignore_teacherList[$basicInformation[0]["UserID"]]);
// 				}
// 				if (count($teacherList) > 0 && count($ignore_teacherList)) {
// 					$finalTeacherList = array_diff_key($teacherList, $ignore_teacherList);
// 					if (count($finalTeacherList) > 0) {
// 						$finalTeacherList = array_values($finalTeacherList);
// 					}
// 				} else {
// 					$finalTeacherList = array_values($teacherList);
// 				}
// 			} else {
				
// 			}
			$strSQL = "SELECT UserID, EnglishName, ChineseName FROM " . $slrsConfig['INTRANET_USER'] . " WHERE UserID = '" . $basicInformation[0]["UserID"] . "'";
			$selectedTeacherInfo = $connection->returnResultSet($strSQL);
			$finalTeacherList = array_values($selectedTeacherInfo);
			
			$x .= "<td>".getSelectByAssoArray(cnvUserArrToSelect($finalTeacherList), $selectionTags='id="leaveTeacher" class="js-select2 js-basic-single" name="leaveTeacher"', $SelectedType=$basicInformation[0]["UserID"], $all=0, $noFirst=0)."</td>"."\r\n";
		$x .= "</tr>"."\r\n";
		$x .= "<tr>"."\r\n";
			$x .= "<td>".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Room']."</td><td>:</td>"."\r\n";
			
			/*************************************************************/
			$roomInfo = $indexVar['libslrs']->getRoom($basicInformation[0]["UserID"],$basicInformation[0]["date"], false);
			if (count($roomInfo) > 0)
			{
				foreach ($roomInfo as $key => $val)
				{
					$strSQL = "SELECT
						LessonExchangeID
					FROM
						INTRANET_SLRS_LESSON_EXCHANGE
					 WHERE
						(TeacherA_UserID='" . $val["UserID"] . "' AND TeacherA_TimeSlotID='" . $val["TimeSlotID"] . "' and TeacherA_Date='" . $basicInformation[0]["date"]. "') OR
						(TeacherB_UserID='" . $val["UserID"] . "' AND TeacherB_TimeSlotID='" . $val["TimeSlotID"] . "' and TeacherB_Date='" . $basicInformation[0]["date"]. "')";
					$tmp = $connection->returnResultSet($strSQL);
					if (count($tmp) > 0)
					{
						unset($roomInfo[$key]);
					}
				}
			}
			
			$exchangeRecord = $indexVar['libslrs']->getExchangedLessonByDateTeacher($basicInformation[0]["date"], $basicInformation[0]["UserID"]);
			if (count($exchangeRecord) > 0)
			{
				foreach ($exchangeRecord as $kk => $vv)
				{
					$roomInfo[] = array(
							"LocationID" => $vv["LocationID"],
							"NameChi" => $vv["NameChi"],
							"NameEng" => $vv["NameEng"],
							"ClassTitleB5" => $vv["ClassTitleB5"],
							"ClassTitleEN" => $vv["ClassTitleEN"],
							"TimeSlotName" => $vv["TimeSlotName"],
							"TimeSlotID" => $vv["TimeSlotID"],
							"SubjectGroupID" => $vv["SubjectGroupID"],
							"StartTime" => $vv["StartTime"],
							"UserID" => $basicInformation[0]["UserID"]
					);
				}
			}
			/*************************************************************/
			// $x .= "<td>".getSelectByAssoArray(cnvRoomArrToSelect($indexVar['libslrs']->getRoom($basicInformation[0]["UserID"],$basicInformation[0]["date"],false),"LocationID","ClassTitleB5","ClassTitleEN"), $selectionTags='id="lesson" class="js-select2 js-basic-single" name="lesson"', $SelectedType=$basicInformation[0]["LocationID"], $all=0, $noFirst=0)."</td>"."\r\n";
			$x .= "<td>".getSelectByAssoArray(cnvRoomArrToSelect($roomInfo,"LocationID","ClassTitleB5","ClassTitleEN"), $selectionTags='id="lesson" class="js-select2 js-basic-single" name="lesson"', $SelectedType=$basicInformation[0]["LocationID"], $all=0, $noFirst=0)."</td>"."\r\n";
		$x .= "</tr>"."\r\n";
	$x .= "</table>";
	$x .= "<div class=\"slrs_legend\"><strong>".$Lang['SLRS']['TempSubstitutionArrangement']['StrongSubstitutedTeacher']."</strong>";
		$x .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<em>".$Lang['SLRS']['TempSubstitutionArrangement']['EmClassTeacher']."</em></div>";
	$x .= "<br/><br/>";
	$x .= "<table id=\"lessonDetails\" class=\"common_table_list slrs_list\">";
	
	$date = $basicInformation[0]["date"];
	$timeSlotID = $basicInformation[0]["TimeSlotID"];
	$userID = $basicInformation[0]["UserID"];	
	
	// get the original userID if this user is supply teacher; else the original userID is that teacher's ID
	$userID = $indexVar['libslrs']->getOriginalTeacherPeriod($userID,$date);
		
	$index = 0;
	$leaveID = -1;
	
	//$sql = "SELECT CycleDay FROM INTRANET_CYCLE_DAYS WHERE RecordDate='".$date."';";
	$dateArr[0]=$date;
	$sql = $indexVar['libslrs']->getDaySQL($dateArr,"");
	$getDay = $connection->returnResultSet($sql);
	$day = $getDay[0]["CycleDay"];
	
	$x .= "<thead>"."\r\n";
	$x .= "<tr>"."\r\n";
	$x .="<th style='width:10%;'>&nbsp;</th>";
	$x .= "<th style='width:10%;' class=\"date\">".$date."<br/>Day ".$day."</th>";
	$x .= "<th style='width:25%;' class=\"location\">".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Location']."</th>";
	$x .="<th style='width:35%;' class=\"row_right_none\">&nbsp;</th>";
	$x .= "<th class=\"row_right_none\">".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Balance']."</th>";
	$x .="<th class=\"row_right_none\">".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['DailyLesson']."</th>";
	$x .= "<th class=\"row_right_none\">".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['TimeTable']."</th>";
	$x .= "</tr>"."\r\n";
	
	$sql = "SELECT
				itt.TimeSlotID, TimeSlotName, LEFT(StartTime,5) as StartTime, LEFT(EndTime,5) as EndTime, itt.DisplayOrder, RoomAllocationID,
				Day,a.LocationID, a.SubjectGroupID,OthersLocation, RecordDate, CycleDay, ClassCode, ClassTitleEN, ClassTitleB5,
				NameChi, NameEng, Code, BarCode, UserID, EnglishName, ChineseName, SubjectTermID, SubjectID, LeaveID, LessonArrangementID,
				ArrangedTo_UserID,COALESCE(ArrangedTo_LocationID,LocationID) as ArrangedTo_LocationID,
				COALESCE(isVolunteer,0) as isVolunteer, LessonRemarks
		FROM (
			SELECT
				TimeSlotID, TimeSlotName, StartTime, EndTime, DisplayOrder
			FROM
				INTRANET_TIMETABLE_TIMESLOT
		) as itt
		LEFT JOIN(
			SELECT
				RoomAllocationID, Day, itra.TimeSlotID, itra.LocationID, itra.SubjectGroupID, OthersLocation, RecordDate, CycleDay,
				ClassCode, ClassTitleEN, ClassTitleB5, NameChi, NameEng, DisplayOrder, Code, BarCode, SubjectClassTeacherID,
				stct.UserID, EnglishName, ChineseName, SubjectTermID, SubjectID, YearTermID, isla.LeaveID, LessonArrangementID,
				ArrangedTo_UserID, ArrangedTo_LocationID, isVolunteer, LessonRemarks
			FROM(			
				SELECT
					RoomAllocationID, Day, TimeSlotID, LocationID, SubjectGroupID, OthersLocation
				FROM
					INTRANET_TIMETABLE_ROOM_ALLOCATION
				WHERE
					Day=".$day." and TimetableID = '".$curTimetableId."'
			) as itra
			LEFT JOIN (
				".$indexVar['libslrs']->getDaySQL($dateArr,"")."
			) as icd ON itra.Day=icd.CycleDay
			LEFT JOIN (
				SELECT
					SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5
				FROM
					SUBJECT_TERM_CLASS
			) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
			LEFT JOIN (
				SELECT
					LocationID, NameChi, NameEng, DisplayOrder, Code, BarCode
				FROM
					INVENTORY_LOCATION
			) as il ON itra.LocationID = il.LocationID
			LEFT JOIN (
				SELECT
					SubjectClassTeacherID, SubjectGroupID, UserID
				FROM
					SUBJECT_TERM_CLASS_TEACHER
				WHERE
					UserID=".IntegerSafe($userID)."
			) as stct ON itra.SubjectGroupID=stct.SubjectGroupID
			LEFT JOIN (
				SELECT
					UserID, EnglishName, ChineseName
				FROM
					".$slrsConfig['INTRANET_USER']."
			) as iu ON stct.UserID=iu.UserID
			LEFT JOIN (
				SELECT 
					SubjectTermID, SubjectGroupID, SubjectID, YearTermID
				FROM
					SUBJECT_TERM
			) as st ON itra.SubjectGroupID = st.SubjectGroupID
			LEFT JOIN (
				SELECT
					LessonArrangementID, UserID,".$userID." as OriUserID, LeaveID, TimeSlotID, LocationID, ArrangedTo_UserID,
					ArrangedTo_LocationID, isVolunteer, LessonRemarks
				FROM
					INTRANET_SLRS_LESSON_ARRANGEMENT
				WHERE
					LeaveID=-1 AND LessonArrangementID='".IntegerSafe($arrangementID)."'
			) as isla ON stct.UserID=isla.OriUserID 
						AND itra.TimeSlotID=isla.TimeSlotID
			WHERE
				stct.UserID IS NOT NULL
		) as a ON itt.TimeSlotID=a.TimeSlotID
		WHERE itt.TimeSlotID=".$timeSlotID ."
		ORDER BY DisplayOrder LIMIT 1";	
	$b = $connection->returnResultSet($sql);
	
	## check with Exchanged Course
	$strSQL = "SELECT
			isle.*, isla.*, itt.*, stc.*, isle.LessonExchangeID
		FROM
			INTRANET_SLRS_LESSON_EXCHANGE as isle
		INNER JOIN
			INTRANET_TIMETABLE_TIMESLOT AS itt ON (itt.TimeSlotID IN (".$timeSlotID.") AND itt.TimeSlotID=isle.TeacherB_TimeSlotID)
		LEFT JOIN
			INTRANET_SLRS_LESSON_ARRANGEMENT AS isla ON (isle.TeacherA_UserID=isla.UserID AND DATE(TimeStart)='" . $date . "' AND isla.TimeSlotID = isle.TeacherB_TimeSlotID)
		LEFT JOIN (
			SELECT
				LocationID, NameChi, NameEng, DisplayOrder, Code, BarCode
			FROM
				INVENTORY_LOCATION
		) as il ON isle.TeacherB_LocationID = il.LocationID
		LEFT JOIN (
			SELECT
				SubjectGroupID, ClassCode, ClassTitleEN, ClassTitleB5
			FROM
				SUBJECT_TERM_CLASS
		) as stc ON isle.TeacherA_SubjectGroupID=stc.SubjectGroupID
		LEFT JOIN (
			SELECT
				SubjectTermID, SubjectGroupID, SubjectID, YearTermID
			FROM
				SUBJECT_TERM
		) as st ON isle.TeacherA_SubjectGroupID = st.SubjectGroupID
		WHERE
			TeacherB_Date='" . $date . "' AND TeacherA_UserID='" . $userID . "' LIMIT 1";
	$exchangedRecords = $connection->returnResultSet($strSQL);

	if (count($exchangedRecords) > 0)
	{
		foreach ($exchangedRecords as $kk => $vv)
		{
			$exchangedRecord = array(
					"TimeSlotID" => $vv["TeacherB_TimeSlotID"],
					"TimeSlotName" => $vv["TimeSlotName"] . "<br><span style='color:#f00'>(**" . $Lang['SLRS']['SubstitutionArrangementFromExchangeRecord'] . ")</span>",
					"StartTime" => date("H:i", strtotime($vv["TeacherB_TimeStart"])),
					"EndTime" => date("H:i", strtotime($vv["TeacherB_TimeEnd"])),
					"DisplayOrder" => $vv["DisplayOrder"],
					"RoomAllocationID" => "",
					"Day" => $day,
					"LocationID" => $vv["TeacherB_LocationID"],
					"SubjectGroupID" => $vv["TeacherA_SubjectGroupID"],
					"OthersLocation" => "",
					"RecordDate" => $vv["TeacherB_Date"],
					"CycleDay" => $day,
					"ClassCode" => $vv["ClassCode"],
					"ClassTitleEN" => $vv["ClassTitleEN"],
					"ClassTitleB5" => $indexVar['libslrs']->displayChinese($vv["ClassTitleB5"]),
					"NameChi" => $indexVar['libslrs']->displayChinese($vv["NameChi"]),
					"NameEng" => $vv["NameEng"],
					"Code" => "",
					"BarCode" => "",
					"UserID" => $userID,
					"EnglishName" => $userInfo["0"]["EnglishName"],
					"ChineseName" => $userInfo["0"]["ChineseName"],
					"SubjectTermID" => $vv["SubjectTermID"],
					"SubjectID" => $vv["TeacherB_SubjectID"],
					"islUserID" => $userID,
					"LeaveID" => $leaveID,
					"laID" => $vv["LessonExchangeID"],
					"LessonArrangementID" => $vv["LessonArrangementID"],
					"ArrangedTo_UserID" => $vv["ArrangedTo_UserID"],
					"ArrangedTo_LocationID" => $vv["ArrangedTo_LocationID"],
					"isVolunteer" => $vv["isVolunteer"],
					"LessonRemarks" => $vv["LessonRemarks"]
			);
			$tmp = $exchangedRecord + $b[0];
			$b[0] = $tmp;
		}
	}
	
	$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
	
	$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();
	
	
	// Supply Teacher (in period)
	$sql = "SELECT Supply_UserID as UserID, DateStart, DateEnd FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE DateStart <= '" . $date . "' AND DateEnd >='" . $date . "'";
	$st = $connection->returnResultSet($sql);
	if (count($st) > 0)
	{
		$supplyTeacherInfo = BuildMultiKeyAssoc($st, 'UserID');
		$availSuppleTeacher = implode(", ", array_keys($supplyTeacherInfo));
	} else {
		$supplyTeacherInfo = array();
		$availSuppleTeacher = "''";
	}
	/**
	$strSQL = "SELECT UI.UserID FROM ".$slrsConfig['INTRANET_USER']." as UI
				INNER JOIN INTRANET_SLRS_TEACHER as IST on (UI.UserID=IST.UserID and UI.Teaching='S')
				WHERE RecordStatus='1' AND RecordType='1'";
	$st = $connection->returnResultSet($strSQL);
	if (count($st) > 0)
	{
		if ($availSuppleTeacher == "''")
		{
			$availSuppleTeacher = implode(", ", array_keys(BuildMultiKeyAssoc($st, 'UserID')));
		}
		else
		{
			$availSuppleTeacher .= ", " . implode(", ", array_keys(BuildMultiKeyAssoc($st, 'UserID')));
		}
	}
	**/
	
	for($i=0; $i<sizeof($b);$i++){
		$hideMore = false;
		// get order list
		$supplyTeacherorderList = getTeacherOrderList($connection,1);
		$internalTeacherorderList = getTeacherOrderList($connection,2);
		
		$arrangedToUserID = ($b[$i]["ArrangedTo_UserID"]=="")?"''":$b[$i]["ArrangedTo_UserID"];

		$selectedNotAvailableTeacher = "";
		// $lessonTeacher = $indexVar['libslrs']->getLessonTeacher($date,$b[$i]["TimeSlotID"]);
		$lessonTeacher = $indexVar['libslrs']->getLessonTeacher($date, $b[$i]["TimeSlotID"], $b[$i]["LessonArrangementID"], $withNotAvailTeacher = true);
		
		if ($lessonTeacher != "''") {
			$lessonTeacherArr = explode(",", $lessonTeacher);
			if (($key = array_search($b[$i]["ArrangedTo_UserID"], $lessonTeacherArr)) !== false) {
				unset($lessonTeacherArr[$key]);
				if (count($lessonTeacherArr) > 0) {
					$lessonTeacher = implode(",", $lessonTeacherArr);
					$selectedNotAvailableTeacher = $b[$i]["ArrangedTo_UserID"];
				} else {
					$lessonTeacher = "''";
				}
			}
		}
		
		// get teachers for each subject
		$rowspan = 1;
		if($b[$i]["UserID"] != ''){
			$sql = "
					SELECT userName,UserID,OriUserID,UpdatedBalance,ModifiedUpdatedBalance,CountSubjectGroupID,ClassTeacher,SameSubjectTeacher,SameClassTeacher,SelectionOrder,
					ClassCode,ClassTitleEN,ClassTitleB5,TeacherType,SelectedTeacher,CountDailySubstitution,SubjectGroupID,Supply_UserID,SupplyUserName
					FROM(
						SELECT userName,b0.UserID,b0.UserID as OriUserID,UpdatedBalance,UpdatedBalance *".$balanceDisplay." as ModifiedUpdatedBalance,CountSubjectGroupID,ClassTeacher,SameSubjectTeacher,SameClassTeacher,SelectionOrder,
						ClassCode,ClassTitleEN,ClassTitleB5,TeacherType,SelectedTeacher,COALESCE(CountDailySubstitution,0) as CountDailySubstitution,SubjectGroupID,NULL as Supply_UserID,NULL as SupplyUserName,NULL as isActive
						FROM(
							SELECT userName,UserID,UpdatedBalance,CountSubjectGroupID,
							CASE WHEN SUM(ClassTeacher)>0 THEN 0 ELSE 1 END as ClassTeacher,
							CASE WHEN SUM(SameSubjectTeacher)>0 THEN 0 ELSE 1 END as SameSubjectTeacher,
							CASE WHEN SUM(SameClassTeacher)>0 THEN 0 ELSE 1 END as SameClassTeacher,
							CountDailySubstitution,Priority as SelectionOrder,ClassCode,ClassTitleEN,ClassTitleB5,TeacherType,
							CASE WHEN UserID=".$arrangedToUserID." THEN 0 ELSE 1 END as SelectedTeacher,0 as SubjectGroupID
							FROM(
								SELECT userName,UserID,UpdatedBalance,COALESCE(SUM(NumberOfLessons),0) as CountSubjectGroupID,0 as ClassTeacher,0 as SameSubjectTeacher,0 as SameClassTeacher,Priority,NULL as ClassCode,NULL as ClassTitleEN,NULL as ClassTitleB5,0 as TeacherType
								FROM( 	
									SELECT ".$name_field." as userName,iu.UserID as UserID,NULL as UpdatedBalance,NULL as SubjectGroupID,NULL as Priority,
									NULL as NumberOfLessons
									FROM(
										SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
										".$slrsConfig['INTRANET_USER']." WHERE Teaching='S' AND UserID IN (" . $availSuppleTeacher . ")										
									) as iu							
									LEFT JOIN(
									   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
									) as stct ON iu.UserID=stct.UserID
									LEFT JOIN(
									   	SELECT SubjectGroupID,1 as NumberOfLessons FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." AND TimeSlotID IN (".$academicTimeSlot.")
									) as itra ON itra.SubjectGroupID=stct.SubjectGroupID
								) as a
								GROUP BY username,UserID,UpdatedBalance,Priority									
							) as a0
							LEFT JOIN(
								SELECT ArrangedTo_UserID,COUNT(ArrangedTo_UserID) as CountDailySubstitution
								FROM INTRANET_SLRS_LESSON_ARRANGEMENT
								WHERE UserID=".$userID." AND LeaveID=-1 AND LessonArrangementID=".IntegerSafe($arrangementID)."
								GROUP BY ArrangedTo_UserID
							) as isla ON a0.UserID=isla.ArrangedTo_UserID
							WHERE UserID <> ".$userID."
							GROUP BY userName,UserID,UpdatedBalance,CountSubjectGroupID,Priority ".$internalTeacherorderList.") 						
						as b0 
						UNION					
						SELECT userName,b0.UserID,b0.UserID as OriUserID,
						UpdatedBalance,UpdatedBalance *".$balanceDisplay." as ModifiedUpdatedBalance,CountSubjectGroupID,ClassTeacher,SameSubjectTeacher,SameClassTeacher,SelectionOrder,
						ClassCode,ClassTitleEN,ClassTitleB5,TeacherType,
						SelectedTeacher,
						COALESCE(CountDailySubstitution,0) as CountDailySubstitution,SubjectGroupID,NULL as Supply_UserID,NULL as SupplyUserName,isActive
						FROM(
							SELECT userName,UserID,UpdatedBalance,CountSubjectGroupID,
							CASE WHEN SUM(ClassTeacher)>0 THEN 0 ELSE 1 END as ClassTeacher,
							CASE WHEN SUM(SameSubjectTeacher)>0 THEN 0 ELSE 1 END as SameSubjectTeacher,
							CASE WHEN SUM(SameClassTeacher)>0 THEN 0 ELSE 1 END as SameClassTeacher,
							CountDailySubstitution,Priority as SelectionOrder,a0.ClassCode,a0.ClassTitleEN,a0.ClassTitleB5,TeacherType,
							CASE WHEN UserID=".$arrangedToUserID." THEN 0 ELSE 1 END as SelectedTeacher,
							CASE WHEN SUM(a0.SubjectGroupID)>0 THEN SUM(a0.SubjectGroupID) ELSE 0 END as SubjectGroupID,isActive
							FROM(
								SELECT userName,UserID,UpdatedBalance,SUM(NumberOfLessons) as CountSubjectGroupID,1 as ClassTeacher,0 as SameSubjectTeacher,0 as SameClassTeacher,Priority,
									NULL as ClassCode,NULL as ClassTitleEN,NULL as ClassTitleB5,1 as TeacherType,0 as SubjectGroupID,isActive
								FROM(
									SELECT ".$name_field." as userName,ist.UserID,UpdatedBalance,stct.SubjectGroupID,Priority,
									CASE WHEN NumberOfLessons IS NOT NULL THEN NumberOfLessons ELSE 0 END as NumberOfLessons,isActive
									FROM (
										SELECT UserID,Balance as UpdatedBalance,Priority,isActive FROM INTRANET_SLRS_TEACHER WHERE UserID NOT IN (" . $availSuppleTeacher . ") ) as ist
									INNER JOIN(
										SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
									   	".$slrsConfig['INTRANET_USER']." WHERE UserID IN (".convertMultipleRowsIntoOneRow($indexVar['libslrs']->getClassTeacherBySubjectGroup($b[$i]["SubjectGroupID"]),"UserID").")
									) as iu ON ist.UserID=iu.UserID
								   	LEFT JOIN(
									   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
									) as stct ON ist.UserID=stct.UserID
									LEFT JOIN(
									   	SELECT SubjectGroupID,1 as NumberOfLessons FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." AND TimeSlotID IN (".$academicTimeSlot.")
									) as itra ON itra.SubjectGroupID=stct.SubjectGroupID						
								) as a
								GROUP BY username,UserID,UpdatedBalance,Priority,isActive
								UNION
								SELECT userName,UserID,UpdatedBalance,SUM(NumberOfLessons) as CountSubjectGroupID,0 as ClassTeacher,1 as SameSubjectTeacher,0 as SameClassTeacher,Priority,
									  NULL as ClassCode,NULL as ClassTitleEN,NULL as ClassTitleB5,1 as TeacherType,0 as SubjectGroupID,isActive
								FROM(
									SELECT ".$name_field." as userName,ist.UserID,UpdatedBalance,stct.SubjectGroupID,Priority,
									CASE WHEN NumberOfLessons IS NOT NULL THEN NumberOfLessons ELSE 0 END as NumberOfLessons,
											NULL as ClassCode,NULL as ClassTitleEN,NULL as ClassTitleB5,isActive
									FROM (
										SELECT UserID,Balance as UpdatedBalance,Priority,isActive FROM INTRANET_SLRS_TEACHER WHERE UserID NOT IN (" . $availSuppleTeacher . ") ) as ist
									INNER JOIN(
										SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
									   	".$slrsConfig['INTRANET_USER']." WHERE UserID IN (".convertMultipleRowsIntoOneRow($indexVar['libslrs']->getSubjectTeacherTeachingSameSubject($b[$i]["SubjectGroupID"]),"UserID").")
									) as iu ON ist.UserID=iu.UserID
									LEFT JOIN(
									   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
									) as stct ON ist.UserID=stct.UserID							
								   	LEFT JOIN(
									   	SELECT SubjectGroupID,1 as NumberOfLessons FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." AND TimeSlotID IN (".$academicTimeSlot.")
									) as itra ON itra.SubjectGroupID=stct.SubjectGroupID									
								) as a
								GROUP BY username,UserID,UpdatedBalance,Priority,isActive
								UNION
								SELECT userName,UserID,UpdatedBalance,SUM(NumberOfLessons) as CountSubjectGroupID,0 as ClassTeacher,0 as SameSubjectTeacher,1 as SameClassTeacher,Priority,
									 ClassCode,ClassTitleEN,ClassTitleB5,1 as TeacherType,SubjectGroupID,isActive
								FROM(
									SELECT ".$name_field." as userName,ist.UserID,UpdatedBalance,stct.SubjectGroupID,Priority,
									CASE WHEN NumberOfLessons IS NOT NULL THEN NumberOfLessons ELSE 0 END as NumberOfLessons,
											NULL as ClassCode,NULL as ClassTitleEN,NULL as ClassTitleB5,isActive
									FROM (
										SELECT UserID,Balance as UpdatedBalance,Priority,isActive FROM INTRANET_SLRS_TEACHER WHERE UserID NOT IN (" . $availSuppleTeacher . ") ) as ist
									INNER JOIN(
										SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
									   	".$slrsConfig['INTRANET_USER']." WHERE UserID IN (".convertMultipleRowsIntoOneRow($indexVar['libslrs']->getSubjectTeacherTeachingSameClass($b[$i]["SubjectGroupID"]),"UserID").")
									) as iu ON ist.UserID=iu.UserID
								   	LEFT JOIN(
									   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
									) as stct ON ist.UserID=stct.UserID
									LEFT JOIN(
									   	SELECT SubjectGroupID,1 as NumberOfLessons FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." AND TimeSlotID IN (".$academicTimeSlot.")
									) as itra ON itra.SubjectGroupID=stct.SubjectGroupID						
								) as a
								GROUP BY username,UserID,UpdatedBalance,Priority,isActive
								UNION
								SELECT userName,UserID,UpdatedBalance,SUM(NumberOfLessons) as CountSubjectGroupID,0 as ClassTeacher,0 as SameSubjectTeacher,0 as SameClassTeacher,Priority,
									 ClassCode,ClassTitleEN,ClassTitleB5,1 as TeacherType,SubjectGroupID,isActive
								FROM(
									SELECT ".$name_field." as userName,ist.UserID,UpdatedBalance,stct.SubjectGroupID,Priority,
									CASE WHEN NumberOfLessons IS NOT NULL THEN NumberOfLessons ELSE 0 END as NumberOfLessons,
											NULL as ClassCode,NULL as ClassTitleEN,NULL as ClassTitleB5,isActive
									FROM (
										SELECT UserID,Balance as UpdatedBalance,Priority,isActive FROM INTRANET_SLRS_TEACHER WHERE UserID NOT IN (" . $availSuppleTeacher . ") ) as ist
									INNER JOIN(
										SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
									   	".$slrsConfig['INTRANET_USER']."
									) as iu ON ist.UserID=iu.UserID
								   	LEFT JOIN(
									   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
									) as stct ON ist.UserID=stct.UserID
									LEFT JOIN(
									   	SELECT SubjectGroupID,1 as NumberOfLessons FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." AND TimeSlotID IN (".$academicTimeSlot.")
									) as itra ON itra.SubjectGroupID=stct.SubjectGroupID						
								) as a
								GROUP BY username,UserID,UpdatedBalance,Priority,isActive
							) as a0
							LEFT JOIN(
								SELECT ArrangedTo_UserID,COUNT(ArrangedTo_UserID) as CountDailySubstitution
								FROM INTRANET_SLRS_LESSON_ARRANGEMENT
								WHERE UserID=".$userID."  AND LeaveID=-1 AND LessonArrangementID=".IntegerSafe($arrangementID)."
								GROUP BY ArrangedTo_UserID
							) as isla ON a0.UserID=isla.ArrangedTo_UserID
							LEFT JOIN(
								SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS 
							) as stc ON a0.SubjectGroupID=stc.SubjectGroupID										
						WHERE UserID <> ".$userID." AND (isActive=1 OR ".$arrangedToUserID."=UserID)
						GROUP BY userName,UserID,UpdatedBalance,CountSubjectGroupID,Priority ".$internalTeacherorderList.") 
						as b0
						LEFT JOIN(
							SELECT Supply_UserID,TeacherID FROM INTRANET_SLRS_SUPPLY_PERIOD WHERE '".$date."' BETWEEN DateStart AND DateEnd		
						) as issp ON b0.UserID=issp.TeacherID
						LEFT JOIN(
							SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish,".$indexVar['libslrs']->getNameFieldByLang($slrsConfig['INTRANET_USER'].".")." as SupplyUserName
							FROM ".$slrsConfig['INTRANET_USER']." WHERE Teaching='S' AND UserID IN (" . $availSuppleTeacher . ")
						) as iu ON issp.Supply_UserID=iu.UserID	
						WHERE issp.TeacherID IS NULL
					) as a0
					WHERE UserID NOT IN (".$indexVar['libslrs']->getLessonLimitation("",$arrangementID,$date).") AND UserID NOT IN (". $lessonTeacher .")

					AND ( UserID IN (SELECT UserID FROM INTRANET_SLRS_TEACHER)
						 OR UserID IN (" . $availSuppleTeacher . ")
						OR UserID IN (" . $arrangedToUserID . ") )

				".$internalTeacherorderList.";";
			// echo $sql."<br/><br/>";
			$c = $connection->returnResultSet($sql);
			//$rowspan=sizeof($c);
			$rowspan= (sizeof($c)<=5)?sizeof($c) : 6;
			$rowspan= ($rowspan!=0)?$rowspan:1;
		}
	
		// get available timeslot
		$isEmptyTimeSlot = $indexVar['libslrs']->isTimeslotWithoutLesson($b[$i]["TimeSlotID"]);
	
		$x .= ($isEmptyTimeSlot != 1)?"<tr id=\"row_details_".$i."_0"."\">"."\r\n":"<tr class=\"row_recess\">"."\r\n";
			
		$x .= "<td id=\"tdTime_".$i."\" rowspan=\"".$rowspan."\">".$b[$i]["StartTime"]." - ".$b[$i]["EndTime"]."<br/>".$b[$i]["TimeSlotName"]."</td>"."\r\n";
		if($b[$i]["UserID"] != ""){
			$index ++ ;
			//$x .= "<td id=\"tdDate_".$i."\" class=\"date class_success\" rowspan=\"".$rowspan."\">".Get_Lang_Selection($b[$i]["ClassTitleB5"],$b[$i]["ClassTitleEN"])."\r\n";
			$x .= "<td id=\"tdDate_".$i."\" class=\"date class_success\" rowspan=\"".$rowspan."\">".$indexVar['libslrs']->getSubjectName($b[$i]["SubjectGroupID"],"linebreak")."\r\n";
			$x .= "<input type=\"hidden\" name=\"timeSlotID_".$index."\" id=\"timeSlotID_".$index."\" value=\"".$b[$i]["TimeSlotID"]."\">";
			$x .= "<input type=\"hidden\" name=\"startTime_".$index."\" id=\"startTime_".$index."\" value=\"".$a[0]["DateLeave"]." ".$b[$i]["StartTime"]."\">";
			$x .= "<input type=\"hidden\" name=\"endTime_".$index."\" id=\"endTime_".$index."\" value=\"".$a[0]["DateLeave"]." ".$b[$i]["EndTime"]."\">";
			$x .= "<input type=\"hidden\" name=\"subjectGroupID_".$index."\" id=\"subjectGroupID_".$index."\" value=\"".$b[$i]["SubjectGroupID"]."\">";
			$x .= "<input type=\"hidden\" name=\"subjectID_".$index."\" id=\"subjectID_".$index."\" value=\"".$b[$i]["SubjectID"]."\">";
			$x .= "<input type=\"hidden\" name=\"lessonArrangementID_".$index."\" id=\"lessonArrangementID_".$index."\" value=\"".$b[$i]["LessonArrangementID"]."\">";
			$x .= "<input type=\"hidden\" name=\"originalLocationID_".$index."\" id=\"originalLocation_".$index."\" value=\"".$b[$i]["LocationID"]."\">";
			$x .= "<input type=\"hidden\" name=\"rowspan_".$i."\" id=\"rowspan_".$i."\" value=\"".(sizeof($c))."\">";
			$x .= "</td>"."\r\n";
		}
		else if ($b[$i]["UserID"] == "" && $isEmptyTimeSlot != 1){
			$x .= "<td id=\"tdDate_".$i."\" class=\"date\" rowspan=\"".$rowspan."\">(".$Lang['SLRS']['SubstitutionArrangementDes']['Reserve'].")</td>"."\r\n";
		}
		else if($isEmptyTimeSlot == 1){
			$x .= "<td id=\"tdDate_".$i."\" class=\"date\" rowspan=\"".$rowspan."\"></td>"."\r\n";
		}
	
		// get location
		//$sql = "SELECT LocationID,NameChi,NameEng FROM INVENTORY_LOCATION";
		// $locaitonList = $indexVar['libslrs']->getLocationList();
		/*
		 * $x .= "<td id=\"tdLocation_".$i."\" class=\"location\" rowspan=\"".$rowspan."\">".((Get_Lang_Selection($b[$i]["NameChi"],$b[$i]["NameEng"]) != "" && $b[$i]["UserID"] != "") ?
				//(Get_Lang_Selection($b[$i]["NameChi"],$b[$i]["NameEng"])) : ($b[$i]["OthersLocation"]));
				($indexVar['libslrs']->getLocationName($b[$i]["Code"],$b[$i]["NameChi"],$b[$i]["NameEng"])) : ($b[$i]["OthersLocation"]));
		 */
		
		$locaitonList = $indexVar['libslrs']->getAllLocationSelection($date, $b[$i]["TimeSlotID"], $b[$i]["LocationID"]);
		
		$x .= "<td id=\"tdLocation_".$i."\" class=\"location\" width='25%' rowspan=\"".$rowspan."\">";
		$x .= ($b[$i]["UserID"] != "" && isset($locaitonList[$b[$i]["LocationID"]])) ?
		$locaitonList[$b[$i]["LocationID"]] :
		((Get_Lang_Selection($b[$i]["NameChi"],$b[$i]["NameEng"]) != "" && $b[$i]["UserID"] != "") ?
				($indexVar['libslrs']->getLocationName($b[$i]["Code"],$b[$i]["NameChi"],$b[$i]["NameEng"])) :
				($b[$i]["OthersLocation"]));
	
		//if ($b[$i]["UserID"] != "" && $b[$i]["ArrangedTo_LocationID"] == $b[$i]["LocationID"]){
		if ($b[$i]["UserID"] != "" &&
				$b[$i]["ArrangedTo_LocationID"] == $b[$i]["LocationID"] &&
				(!$sys_custom['SLRS']["defaultToPresetLocation"] ||
						$sys_custom['SLRS']["defaultToPresetLocation"] &&
						( empty($b[$i]["LessonArrangementID"]) && $PresetLocationInfo["ThisCount"] == 0)
						)
				)
		{
			$x .= "<div>";
				$x .= "<a id=\"locationText_".$i."\" >".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToBeChanged']."</a><br/>";
				$x .= "<div id=\"locationDiv_".$i."\" style=\"display:none\">". getSelectByAssoArray($locaitonList, $selectionTags='id="locationID_'.$index.'" class="js-select2 js-basic-single" name="locationID_'.$index.'"', $SelectedType=$b[$i]["LocationID"], $all=0, $noFirst=0)."</div>";
				$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='VoluntarySubstitute'";
				$z = $connection->returnResultSet($sql);
				if($z[0]["SettingValue"]==1)
				{
					$x .= "<div style=\"visibility:hidden\">";
				}
				else
				{
					$x .= "<div style=\"visibility:visible\">";
				}
				$x.="<br/>".$indexVar['libslrs_ui']->Get_Checkbox("isVolunteer_".$index, "isVolunteer_".$index, $Value=1, $isChecked=$b[$i]["isVolunteer"], $Class='', $Display=$Lang['SLRS']['SubstitutionArrangementDes']['isVolunteer'], $Onclick='', $Disabled='');
				$x.="</div>";
				
				if ($arrangedToUserID == -999 || (empty($b[$i]["LessonArrangementID"]) && ($isDefaultNoSubstitution || $TotalArranged > 0))) {
					$ignoreThisRec = true;
				} else {
					$ignoreThisRec = false;
				}
				$x.="<br/>". $indexVar['libslrs_ui']->Get_Checkbox("ignoreThisRec_".$index, "ignoreThisRec_".$index, $Value=1, $isChecked=$ignoreThisRec, $Class='ignoreAction', $Display=$Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'], $Onclick='', $Disabled='');
				
				if ($sys_custom['SLRS']["allowSubstitutionRemarks"]) {
					$x .= "<br><br><div style='text-align:left;'>";
					$x .= $Lang["SLRS"]["LessonRemarks"] . ":";
					$x .= "<textarea style='width:100%;height:60px;resize:vertical;' name='LessonRemarks_".$index."'>" . $b[$i]["LessonRemarks"] . "</textarea>";
				}
				
			$x .="</div>";
		}
		// else if ($b[$i]["UserID"] != "" && $b[$i]["ArrangedTo_LocationID"] != $b[$i]["LocationID"]){
		else if ($b[$i]["UserID"] != "" && $b[$i]["ArrangedTo_LocationID"] != $b[$i]["LocationID"] || $sys_custom['SLRS']["defaultToPresetLocation"]){
			$x .= "<div>";
				$x .= "<a id=\"locationText_".$i."\" >".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Cancel']."</a><br/>";
				
				$selectedLocationID = $b[$i]["ArrangedTo_LocationID"];
				if ($sys_custom['SLRS']["defaultToPresetLocation"] && $PresetLocationInfo["LimitLocation"]) {
					if (empty($b[$i]["ArrangedTo_LocationID"]) || empty($b[$i]["LessonArrangementID"])) {
						$selectedLocationID = $PresetLocationInfo["LocationID"];
						$PresetLocationInfo["NumOfPresentLocation"]++;
					}
				}
				
				$x .= "<div id=\"locationDiv_".$i."\">". getSelectByAssoArray($locaitonList, $selectionTags='id="locationID_'.$index.'" class="js-select2 js-basic-single" name="locationID_'.$index.'"', $SelectedType=$selectedLocationID, $all=0, $noFirst=0)."</div>";
				$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='SLRS' AND SettingName='VoluntarySubstitute'";
				$z = $connection->returnResultSet($sql);
				if($z[0]["SettingValue"]==1){
					$x .= "<div style=\"visibility:hidden\">";
				}
				else{
					$x .= "<div style=\"visibility:visible\">";
				}
				$x .="<br/>".$indexVar['libslrs_ui']->Get_Checkbox("isVolunteer_".$index, "isVolunteer_".$index, $Value=1, $isChecked=$b[$i]["isVolunteer"], $Class='', $Display=$Lang['SLRS']['SubstitutionArrangementDes']['isVolunteer'], $Onclick='', $Disabled='');
				$x .="</div>";
				
				if ($arrangedToUserID == -999 || (empty($b[$i]["LessonArrangementID"]) && ($isDefaultNoSubstitution || $TotalArranged > 0))) {
					$ignoreThisRec = true;
				} else {
					if ($arrangedToUserID == -999)
					{
						if ($selectedLocationID == $PresetLocationInfo["LocationID"] && $sys_custom['SLRS']["defaultToPresetLocation"]) {
							$ignoreThisRec = true;
						} else {
							$ignoreThisRec = false;
						}
					}
					else 
					{
						$ignoreThisRec = false;
					}
					
				}
				$x.="<br/>". $indexVar['libslrs_ui']->Get_Checkbox("ignoreThisRec_".$index, "ignoreThisRec_".$index, $Value=1, $isChecked=$ignoreThisRec, $Class='ignoreAction', $Display=$Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'], $Onclick='', $Disabled='');

				if ($sys_custom['SLRS']["allowSubstitutionRemarks"]) {
					$x .= "<br><br><div style='text-align:left;'>";
					$x .= $Lang["SLRS"]["LessonRemarks"] . ":";
					$x .= "<textarea style='width:100%;height:60px;resize:vertical;' name='LessonRemarks_".$index."'>" . $b[$i]["LessonRemarks"] . "</textarea>";
				}
				
			$x .= "</div>";
		}
		$x .= "<input type=\"hidden\" id=\"oriLocationID_".$i."\" name=\"oriLocationID_".$i."\" value=\"".$b[$i]["LocationID"]."\">";
		$x .= "</td>"."\r\n";
		
		if (count($c) > 0)
		{
			foreach ($c as $c_key => $c_val)
			{
				if (in_array($c_val["UserID"], $expiredUserIDs) && $c_val["UserID"] != $b[$i]["ArrangedTo_UserID"])
				{
					unset($c[$c_key]);
				}
			}
			$c = array_values($c);
		}
		
		if(sizeof($c) == 0){
			$x .="<td rowspan=\"".$rowspan."\" class=\"row_right_none\"></td><td rowspan=\"".$rowspan."\" class=\"row_right_none\"></td>";
			$x .="<td rowspan=\"".$rowspan."\" class=\"row_right_none\"></td><td rowspan=\"".$rowspan."\" class=\"row_right_none\"></td>";
			$x .="</tr>"."\r\n";
		}
		else{
			for($_j=0;$_j<sizeof($c);$_j++){
				if($c[$_j]["ClassTeacher"]=="0" && $c[$_j]["SameClassTeacher"]=="0"){
					$teacherName = "<em>".$c[$_j]["userName"] ."(".$indexVar['libslrs']->getSubjectSameClass($b[$i]["SubjectGroupID"],$date,$c[$_j]["OriUserID"]).")"."</em>";
				}
				else if($c[$_j]["ClassTeacher"]=="0"){
					$teacherName = "<em>".$c[$_j]["userName"]."</em>";
				}
				else if($c[$_j]["SameClassTeacher"]=="0"){
					$teacherName = $c[$_j]["userName"]."(".$indexVar['libslrs']->getSubjectSameClass($b[$i]["SubjectGroupID"],$date,$c[$_j]["OriUserID"]).")";
				}
				else if($c[$_j]["TeacherType"]=="0"){
					$teacherName = "<strong>".$c[$_j]["userName"]."</strong>";
					
					if (isset($supplyTeacherInfo[$c[$_j]["UserID"]]))
					{
						$teacherName .= " [" . $Lang["SLRS"]["AvailablePeriods"] . " : " . $supplyTeacherInfo[$c[$_j]["UserID"]]["DateStart"] . $Lang["SLRS"]["AvailablePeriodsTo"]. $supplyTeacherInfo[$c[$_j]["UserID"]]["DateEnd"] . "]";
					}
					
				}
				else{
					$teacherName=$c[$_j]["userName"];
				}
				if($_j== 0){
					$checked = ($b[$i]["ArrangedTo_UserID"]==$c[$_j]["UserID"])?1:0;
					if($checked == 1){
						$class = "class=\"row_suggest row_right_none\"";
					}
					else{
						$class = "class=\"row_right_none\"";
					}
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$indexVar['libslrs_ui']->Get_Radio_Button('teacher_'.$i.'_'.($c[$_j]["UserID"]), 'teacher_'.$index, $Value=$c[$_j]["UserID"], $isChecked=($b[$i]["ArrangedTo_UserID"]==$c[$_j]["UserID"])?1:0, $Class="", $Display=$teacherName, $Onclick="",$isDisabled=0);
					if ($c[$_j]["UserID"] == $selectedNotAvailableTeacher)
					{
						$x .= " <div style='color:#f00'>** " . $Lang['SLRS']['NotAvailableTeachersErr']["NotAvailable"]. "</div>";
					}
					$x .="</td>";
					
					if (in_array($c[$_j]["UserID"], $expiredUserIDs))
					{
						$x .="<td colspan='3' id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">" . $Lang["SLRS"]["User_Suspended"] . "</td>";
					}
					else
					{
						$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$c[$_j]["ModifiedUpdatedBalance"]."</td>";
						$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$c[$_j]["CountSubjectGroupID"]."</td>";
						$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class."><a id=\"viewTimeTable_".$i."\" href=\"javascript:goView('".$c[$_j]["UserID"]."')\">".$Lang['SLRS']['SubstitutionArrangementDes']['View']."</a>";
						//$x .="<input type=\"hidden\" id=\"assigedToUserID_".$_j."\" value=\"".$c[$_j]["UserID"]."\">";
						$x .="</td>";
					}
				}
				else if ($_j >= 1 && $_j <5){
					$checked = ($b[$i]["ArrangedTo_UserID"]==$c[$_j]["UserID"])?1:0;
					if($checked == 1){
						$class = "class=\"row_suggest row_right_none\"";
					}
					else{
						$class = "class=\"row_right_none\"";
					}
					$x .="<tr id=\"row_".$i."_".($_j)."\">";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$indexVar['libslrs_ui']->Get_Radio_Button('teacher_'.$i.'_'.($c[$_j]["UserID"]), 'teacher_'.$index, $Value=$c[$_j]["UserID"], $isChecked=($b[$i]["ArrangedTo_UserID"]==$c[$_j]["UserID"])?1:0, $Class="", $Display="$teacherName", $Onclick="",$isDisabled=0)."</label>";
					if ($c[$_j]["UserID"] == $selectedNotAvailableTeacher)
					{
						$x .= " <div style='color:#f00'>** " . $Lang['SLRS']['NotAvailableTeachersErr']["NotAvailable"]. "</div>";
					}
					$x .="</td>";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$c[$_j]["ModifiedUpdatedBalance"]."</td>";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$c[$_j]["CountSubjectGroupID"]."</td>";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class."><a id=\"viewTimeTable_".$i."\" href=\"javascript:goView('".$c[$_j]["UserID"]."')\">".$Lang['SLRS']['SubstitutionArrangementDes']['View']."</a>";
					//$x .="<input type=\"hidden\" id=\"assigedToUserID_".$_j."\" value=\"".$c[$_j]["UserID"]."\">";
					$x .="</td>";
					$x .="</tr>";
				}
				else if($_j >=5){
					if($hideMore == false){
						$x .="<tr id=\"row_more_".$i."\" class=\"row_more\">";
						$x .="<td colspan=\"4\">";
						$x .= "<div>"
								."<a id=\"more_".$i."\" href=\"javascript:goMore('".$i."')\">".$Lang['SLRS']['SubstitutionArrangementDes']['More']."</a>"
										."</div>";
										$x .="</td>";
										$x .="</tr>";
										$hideMore = true;
					}
					$checked = ($b[$i]["ArrangedTo_UserID"]==$c[$_j]["UserID"])?1:0;
					if($checked == 1){
						$class = "class=\"row_suggest row_right_none\"";
					}
					else{
						$class = "class=\"row_right_none\"";
					}
					$x .="<tr id=\"row_".$i."_".($_j)."\" style=\"display:none;\">";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$indexVar['libslrs_ui']->Get_Radio_Button('teacher_'.$i.'_'.($c[$_j]["UserID"]), 'teacher_'.$index, $Value=$c[$_j]["UserID"], $isChecked=($b[$i]["ArrangedTo_UserID"]==$c[$_j]["UserID"])?1:0, $Class="", $Display="$teacherName", $Onclick="",$isDisabled=0);
					if ($c[$_j]["UserID"] == $selectedNotAvailableTeacher)
					{
						$x .= " <div style='color:#f00'>** " . $Lang['SLRS']['NotAvailableTeachersErr']["NotAvailable"]. "</div>";
					}
					$x .="</td>";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$c[$_j]["ModifiedUpdatedBalance"]."</td>";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class.">".$c[$_j]["CountSubjectGroupID"]."</td>";
					$x .="<td id=\"details_".$i."_".($c[$_j]["UserID"])."\" ".$class."><a id=\"viewTimeTable_".$i."\" href=\"javascript:goView('".$c[$_j]["UserID"]."')\">".$Lang['SLRS']['SubstitutionArrangementDes']['View']."</a>";
					//$x .="<input type=\"hidden\" id=\"assigedToUserID_".$_j."\" value=\"".$c[$_j]["UserID"]."\">";
					$x .="</td>";
					$x .="</tr>";
				}
			}
		}
		$c = null;
	}
	$x .="</thead>";
	$x .="<input type=\"hidden\" id=\"endIndex\" value=".$index.">";
	$x .="<input type=\"hidden\" id=\"day\" value=".$day.">";
	
	
	$x .="</table>";
	
}

$htmlAry['contentTbl'] = $x;
$htmlAry['startIndex'] = 1;
//$htmlAry['endIndex'] = $index;
$htmlAry['leaveID'] = "-1";
//$htmlAry['day'] = $day;

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "submit", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================

function cnvUserArrToSelect($arr){
    $oAry = array();
    for($_i=0; $_i<sizeof($arr); $_i++){
        $oAry[$arr[$_i]["UserID"]] = Get_Lang_Selection($arr[$_i]["ChineseName"],$arr[$_i]["EnglishName"]);
    }
    return $oAry;;
}


function cnvGeneralArrToSelect($arr,$type,$ChineseName,$EnglishName){
    $oAry = array();
    for($_i=0; $_i<sizeof($arr); $_i++){
        $oAry[$arr[$_i][$type]] = Get_Lang_Selection($arr[$_i][$ChineseName],$arr[$_i][$EnglishName]);
    }
    return $oAry;;
}

function cnvRoomArrToSelect($arr,$type,$ChineseName,$EnglishName){
    global $indexVar;
    $oAry = array();
    for($_i=0; $_i<sizeof($arr); $_i++){
        $oAry[$arr[$_i][$type]] = Get_Lang_Selection($indexVar['libslrs']->displayChinese($arr[$_i][$ChineseName]), $arr[$_i][$EnglishName])."(".$arr[$_i]["TimeSlotName"].")";
    }
    return $oAry;;
}

function getTeacherOrderList($connection, $teacherType){
    //$teacherType=1->外來代課老師; $teacherType=2->校內老師
    $sql = "SELECT ConditionID,DisplayOrder FROM INTRANET_SLRS_SUBSTITUTE_PRIORITY WHERE RowID=".$teacherType." ORDER BY DisplayOrder";
    $orderList = $connection->returnResultSet($sql);
    
    $returnOrderList = " ORDER BY SelectedTeacher,TeacherType, ";
    if($teacherType==1){
        for($i=0; $i<sizeof($orderList);$i++){
            if($orderList[$i]["ConditionID"]=="3"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList." DailyLesson,": $returnOrderList." DailyLesson";
            }
            else if($orderList[$i]["ConditionID"]=="4"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList." CountSubjectGroupID,": $returnOrderList." CountSubjectGroupID";
            }
        }
    }
    else if ($teacherType==2){
        for($i=0; $i<sizeof($orderList);$i++){
            if($orderList[$i]["ConditionID"]=="5"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."UpdatedBalance,": $returnOrderList." UpdatedBalance";
            }
            else if($orderList[$i]["ConditionID"]=="6"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."CountDailySubstitution,": $returnOrderList." CountDailySubstitution";
            }
            else if($orderList[$i]["ConditionID"]=="7"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."CountSubjectGroupID,": $returnOrderList." CountSubjectGroupID";
            }
            else if($orderList[$i]["ConditionID"]=="8"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."SelectionOrder,": $returnOrderList." SelectionOrder";
            }
            else if($orderList[$i]["ConditionID"]=="9"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."ClassTeacher,": $returnOrderList." ClassTeacher";
            }
            else if($orderList[$i]["ConditionID"]=="10"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."SameClassTeacher,": $returnOrderList." SameClassTeacher";
            }
            else if($orderList[$i]["ConditionID"]=="11"){
                $returnOrderList = ($i!=sizeof($orderList)-1)? $returnOrderList."SameSubjectTeacher,": $returnOrderList." SameSubjectTeacher";
            }
        }
    }
    return $returnOrderList;
}

function convertMultipleRowsIntoOneRow($arr,$fieldName){
    $x = "";
    for($i=0; $i<sizeof($arr);$i++){
        if($i==sizeof($arr)-1){
            $x .= $arr[$i][$fieldName];
        }
        else{
            $x .= $arr[$i][$fieldName].",";
        }
    }
    if(sizeof($arr)==0){
        $x = "''";
    }
    return $x;
}

?>