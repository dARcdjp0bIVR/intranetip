<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_GET["selected_date"]))
{
	$currentDate = $_GET["selected_date"];
}
else
{
	$currentDate = date("Y-m-d");
}
$date = $currentDate;
$dateArr[0]=$currentDate;
$datePicker = $currentDate;
//$date =(isset($_POST['datePicker']))?$_POST['datePicker']:"$currentDate";

if (isset($_POST['datePicker'])&& $_POST['datePicker']!="") {
	$keyword = $_POST['keyword'];
	$date = $_POST['datePicker'];
	$dateArr[0]=$date;
}
else if (isset($_POST['datePicker'])&& $_POST['datePicker']=="") {	
	$keyword = $_POST['keyword'];
	$date = "%";
	
	$sql = "SELECT DATE_FORMAT(TimeStart,'%Y-%m-%d') as TimeStart
		FROM INTRANET_SLRS_LESSON_ARRANGEMENT
		WHERE DATE_FORMAT(TimeStart,'%Y-%m-%d') like '".$date."'";
	$connection = new libgeneralsettings();
	$a = $connection->returnResultSet($sql);
	for($i=0;$i<sizeof($a);$i++){
		$dateArr[$i]=$a[$i]["TimeStart"];
	}
}
else{
	
// 	$sql = "SELECT DATE(TimeStart) as DateLeave FROM INTRANET_SLRS_LESSON_ARRANGEMENT ORDER BY DateTimeModified DESC, TimeStart DESC LIMIT 1";
// 	$connection = new libgeneralsettings();
// 	$lastDateRec = $connection->returnResultSet($sql);
// 	if (count($lastDateRec) > 0) {
// 		$date = $lastDateRec[0]["DateLeave"];
// 		$dateArr[0] = $lastDateRec[0]["DateLeave"];
// 		$datePicker = $lastDateRec[0]["DateLeave"];
// 	} else {
// 		$date = $currentDate;
// 		$dateArr[0]=$currentDate;
// 	}
}
$datePicker = $date;

$arrCookies = array();
$arrCookies[] = array("substitute_arrangement_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['TemporarySubstitutionArrangement']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 2 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;



//echo $name_field;

$name_field = $indexVar['libslrs']->getNameFieldByLang("iu.");
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("TeacherName","Lesson","DateTimeModified","Action");	

if ($keyword !== '' && $keyword !== null) {
	// use Get_Safe_Sql_Like_Query for " LIKE "
	// use Get_Safe_Sql_Query for " = "
	$condsKeyword = " WHERE (
							TeacherName LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR Lesson LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
							OR DateTimeModified LIKE '%".$indexVar['libslrs']->Get_Safe_Sql_Like_Query($keyword)."%'
						  )
					";
}

$li -> sql ="
		SELECT TeacherName,Lesson,DateTimeModified,Action,checkbox,LessonArrangementID
		FROM(
			SELECT userName as TeacherName,1 as Lesson,DateTimeModified,
				CONCAT('<a id=\"lessonArrangementID_',a.LessonArrangementID,'\" class=\"tablelink\" href=\"javascript:goArrange(',a.LessonArrangementID,')\">',Action,'</a>') as Action,
					CONCAT('<input type=checkbox name=lessonArrangementIdAry[] id=lessonArrangementIdAry[] value=\"', LessonArrangementID,'\">') as checkbox, LessonArrangementID
				FROM(
					SELECT LessonArrangementID,".$name_field." as userName,isla.UserID,DateTimeModified,NumberOfLessons,'".$Lang['SLRS']['TemporarySubstitutionArrangementDes']['View']."' as Action
					FROM(
						SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,DATE_FORMAT(TimeStart,'%Y-%m-%d') as TimeStart,DATE_FORMAT(TimeEnd,'%Y-%m-%d') as TimeEnd,
							ArrangedTo_UserID,ArrangedTo_LocationID,DateTimeModified,SubjectGroupID
						FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LeaveID=-1 AND DATE_FORMAT(TimeStart,'%Y-%m-%d') like '".$date."'
					) as isla
					LEFT JOIN(
						 SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
						 ".$slrsConfig['INTRANET_USER']."
				 	) as iu ON isla.UserID=iu.UserID
					LEFT JOIN(
					   	SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER		
					) as stct ON isla.UserID=stct.UserID AND isla.SubjectGroupID=stct.SubjectGroupID	
					LEFT JOIN(
					   	SELECT SubjectGroupID,1 as NumberOfLessons,TimeSlotID FROM INTRANET_TIMETABLE_ROOM_ALLOCATION
					) as itra ON itra.SubjectGroupID=stct.SubjectGroupID AND isla.TimeSlotID=itra.TimeSlotID	
				) as a
				GROUP BY username,UserID,DateTimeModified
		) as a0	".$condsKeyword."	
		";

//echo $li->sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->fieldorder2 = ", DateTimeModified";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$showSADetails = true;
if ($showSADetails)
{
	$args["table"] = "custom_sql";
	$args["sql"] = $li->sql;
	$args["fieldorder2"] = $li->fieldorder2;
	
	$gridInfo = $indexVar['libslrs']->getTableGridInfo($args);
	// $gridInfo["fields"] = array("TeacherName", "Lesson", "Duration", "DateTimeModified", "Action", "checkbox", "LeaveID");
	$gridInfo["fields"] = $li->field_array;
	$gridInfo["fields"][] = "checkbox";
	$gridInfo["fields"][] = "LessonArrangementID";
	$gridInfo["sql"] = $li->sql;
	
	$args = array(
			"sql" => $li->built_sql(),
			"girdInfo" => $gridInfo
	);

	$li->custDataset = true;
	$listData = $indexVar['libslrs']->getTableGridData($args);
	$li->custDataSetArr = $indexVar['libslrs']->getArrangeDetailsForListing($listData, $gridInfo, false);
}

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['TemporarySubstitutionArrangementDes']['Teacher'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['TemporarySubstitutionArrangementDes']['Lesson'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['TemporarySubstitutionArrangementDes']['DateTimeModified'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['SLRS']['TemporarySubstitutionArrangementDes']['Action'])."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("lessonArrangementIdAry[]")."</th>\n";
$htmlAry['dataTable'] = $li->display();


$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libslrs_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$htmlAry['hiddenField'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goAddLeave();', $Lang['SLRS']['TemporarySubstitutionArrangementDes']['AddArrangment'], $subBtnAry);
/*$btnAry[] = array('import', 'javascript: void(0);', '', $subBtnAry);
$subBtnAry = array();
$btnAry[] = array('export', 'javascript: void(0);', '', $subBtnAry);*/
$htmlAry['contentTool'] = $indexVar['libslrs_ui']->Get_Content_Tool_By_Array_v30($btnAry);

$htmlAry['searchBox'] = $indexVar['libslrs_ui']->Get_Search_Box_Div('keyword', $keyword);

$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['SLRS']['SubstitutionArrangementDes']['DeleteSubstition']);
$htmlAry['dbTableActionBtn'] = $indexVar['libslrs_ui']->Get_DBTable_Action_Button_IP25($btnAry);

$htmlAry['dataPickerTool']=$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $datePicker, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$htmlAry['viewBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['View'], "button", "goView()", 'viewBtn');
// ============================== Define Button ==============================

?>