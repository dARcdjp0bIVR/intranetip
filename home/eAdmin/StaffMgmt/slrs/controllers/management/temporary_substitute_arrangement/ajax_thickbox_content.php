<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
echo $indexVar['libslrs_ui']->Include_JS_CSS();
//$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteBalance']);
//$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//echo $indexVar['libslrs_ui']->Include_Thickbox_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$connection = new libgeneralsettings();

$date = $_POST["date"];
$userID = $_POST["userID"];
$day = $_POST["day"];
$leaveID = $_POST["leaveID"];

//echo $date. " ".$userID." ".$day;


$name_field = $indexVar['libslrs']->getNameFieldByLang("a.");
$sql = "SELECT".$name_field." as UserName
		FROM (SELECT UserID,EnglishName,ChineseName,TitleChinese FROM ".$slrsConfig['INTRANET_USER']." WHERE UserID=".$userID.") as a		
		";
$userInfo = $connection->returnResultSet($sql);

$academicTimeSlot = convertMultipleRowsIntoOneRow($indexVar['libslrs']->getAcademicTimeSlot($date),"TimeSlotID");
// get the original userID if this user is supply teacher; else the original userID is that teacher's ID
$userID = $indexVar['libslrs']->getOriginalTeacherPeriod($userID,$date);
$dateArr[0]=$date;
$sql = "	
		SELECT itt.TimeSlotID,TimeSlotName,LEFT(StartTime,5) as StartTime,LEFT(EndTime,5) as EndTime,itt.DisplayOrder,UserID,EnglishName,ChineseName,
		SubjectClassTeacherID,SubjectGroupID,RoomAllocationID,Day,RefTimeSlotID,LocationID,OthersLocation,RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,
			NameChi,NameEng,Code,BarCode,SubjectTermID,SubjectID,YearTermID,Substitution
		FROM (
			SELECT TimeSlotID,TimeSlotName,StartTime,EndTime,DisplayOrder FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID IN (".$academicTimeSlot.")
		) as itt
		LEFT JOIN(
			SELECT iu.UserID,EnglishName,ChineseName,SubjectClassTeacherID,stct.SubjectGroupID,RoomAllocationID,Day,TimeSlotID as RefTimeSlotID,itra.LocationID,OthersLocation,RecordDate,CycleDay,ClassCode,ClassTitleEN,ClassTitleB5,
			NameChi,NameEng,DisplayOrder,Code,BarCode,SubjectTermID,SubjectID,YearTermID,0 as Substitution
			FROM(SELECT UserID,EnglishName,ChineseName FROM ".$slrsConfig['INTRANET_USER']." WHERE UserID=".IntegerSafe($userID).") as iu
			LEFT JOIN(SELECT SubjectClassTeacherID,SubjectGroupID,UserID FROM SUBJECT_TERM_CLASS_TEACHER WHERE UserID=".IntegerSafe($userID).") as stct ON iu.UserID=stct.UserID
			LEFT JOIN (SELECT RoomAllocationID,Day,TimeSlotID,LocationID,SubjectGroupID,OthersLocation FROM INTRANET_TIMETABLE_ROOM_ALLOCATION WHERE Day=".$day." AND TimeSlotID IN (".$academicTimeSlot.") ) as itra ON stct.SubjectGroupID=itra.SubjectGroupID
			LEFT JOIN (".$indexVar['libslrs']->getDaySQL($dateArr,"").") as icd ON itra.Day=icd.CycleDay
			LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS) as stc ON itra.SubjectGroupID=stc.SubjectGroupID
			LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) as il ON itra.LocationID = il.LocationID
			LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) as st ON itra.SubjectGroupID = st.SubjectGroupID		
			WHERE TimeSlotID IS NOT NULL
			UNION
			SELECT iu.UserID,EnglishName,ChineseName,NULL as SubjectClassTeacherID,isla.SubjectGroupID,NULL as RoomAllocationID,NULL as Day,isla.TimeSlotID as RefTimeSlotID,isla.LocationID,NULL as OthersLocation,NULL as RecordDate,NULL as CycleDay,
					ClassCode,ClassTitleEN,ClassTitleB5, NameChi,NameEng,DisplayOrder,Code,BarCode,SubjectTermID,isla.SubjectID,YearTermID,1 as Substitution
			FROM(SELECT LessonArrangementID,UserID,LeaveID,TimeSlotID,LocationID,ArrangedTo_UserID,ArrangedTo_LocationID,SubjectID,SubjectGroupID
				FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LeaveID=".IntegerSafe($leaveID)." AND ArrangedTo_UserID=".IntegerSafe($userID).") as isla
			LEFT JOIN(
				SELECT UserID,EnglishName,ChineseName FROM ".$slrsConfig['INTRANET_USER']." WHERE UserID=".IntegerSafe($userID).") as iu ON isla.ArrangedTo_UserID=iu.UserID	
			LEFT JOIN (SELECT SubjectGroupID,ClassCode,ClassTitleEN,ClassTitleB5 FROM SUBJECT_TERM_CLASS) as stc ON isla.SubjectGroupID=stc.SubjectGroupID
			LEFT JOIN (SELECT LocationID,NameChi,NameEng,DisplayOrder,Code,BarCode FROM INVENTORY_LOCATION) as il ON isla.LocationID = il.LocationID
			LEFT JOIN (SELECT SubjectTermID,SubjectGroupID,SubjectID,YearTermID FROM SUBJECT_TERM) as st ON isla.SubjectGroupID = st.SubjectGroupID		
		) as a ON itt.TimeSlotID=a.RefTimeSlotID
		ORDER BY DisplayOrder
		";

$index = 0;
$lesson = $connection->returnResultSet($sql);

$x = "<div class=\"edit_pop_board slrs\">"."\r\n";
$x .= "<h1>".$userInfo[0]["UserName"]." (".$date.", Day".$day.")</h1><br/>"."\r\n";
$x .= "<table class=\"common_table_list slrs_list\">"."\r\n";
$x .= "<tr><th></th><th class=\"date\">".$Lang['SLRS']['SubstitutionArrangementDes']['Room']."</th><th class=\"date\">".$Lang['SLRS']['SubstitutionArrangementDes']['Location']."</th></tr>"."\r\n";
for ($i=0; $i<sizeof($lesson); $i++){
	$isEmptyTimeSlot = $indexVar['libslrs']->isTimeslotWithoutLesson($lesson[$i]["TimeSlotID"]);
	$x .= ($isEmptyTimeSlot != 1)?"<tr>"."\r\n":"<tr class=\"row_recess\">"."\r\n";
		$x .= "<td>".$lesson[$i]["StartTime"]." - ".$lesson[$i]["EndTime"]."<br/>".$indexVar['libslrs']->displayChinese($lesson[$i]["TimeSlotName"])."</td>";
		if($lesson[$i]["UserID"]!="" && $lesson[$i]["Substitution"]==0){
			$x .= "<td class=\"date\">".Get_Lang_Selection($indexVar['libslrs']->displayChinese($lesson[$i]["ClassTitleB5"]),$lesson[$i]["ClassTitleEN"]);
			$x ."</td>";
			$x .= "<td>";
			$x .= ((Get_Lang_Selection($indexVar['libslrs']->displayChinese($lesson[$i]["NameChi"]),$lesson[$i]["NameEng"]) != "" && $lesson[$i]["UserID"] != "") ?
					(Get_Lang_Selection($indexVar['libslrs']->displayChinese($lesson[$i]["NameChi"]),$lesson[$i]["NameEng"])) : ($lesson[$i]["OthersLocation"]));
			$x .= "</td>";
		}
		else if($lesson[$i]["UserID"]!="" && $lesson[$i]["Substitution"]==1){
			$x .= "<td class=\"date class_success\">".Get_Lang_Selection($indexVar['libslrs']->displayChinese($lesson[$i]["ClassTitleB5"]),$lesson[$i]["ClassTitleEN"])."<br/>(".$Lang['SLRS']['SubstitutionArrangementDes']['SubstitutedLesson'].")";
			$x ."</td>";
			$x .= "<td>";
			$x .= ((Get_Lang_Selection($indexVar['libslrs']->displayChinese($lesson[$i]["NameChi"]),$lesson[$i]["NameEng"]) != "" && $lesson[$i]["UserID"] != "") ?
					(Get_Lang_Selection($indexVar['libslrs']->displayChinese($lesson[$i]["NameChi"]),$lesson[$i]["NameEng"])) : ($lesson[$i]["OthersLocation"]));
			$x .= "</td>";
		}
		else if($lesson[$i]["UserID"] == "" && $isEmptyTimeSlot != 1){
			$x .= "<td class=\"date\">(".$Lang['SLRS']['SubstitutionArrangementDes']['Reserve'].")</td><td></td>";
		}
		else if($isEmptyTimeSlot == 1){
			$x .= "<td class=\"date\"></td><td></td>"."\r\n";
		}	
	$x .="</tr>";
}
$x .= "</table></div>";
$htmlAry['contentTbl'] = $x;




// ============================= Transactional data ==============================
// ============================== Define Button ==============================
$htmlAry['closeBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
// ============================== Define Button ==============================

// ============================== Custom Function ==============================

function convertMultipleRowsIntoOneRow($arr,$fieldName){
	$x = "";
	for($i=0; $i<sizeof($arr);$i++){
		if($i==sizeof($arr)-1){
			$x .= $arr[$i][$fieldName];
		}
		else{
			$x .= $arr[$i][$fieldName].",";
		}
	}
	if(sizeof($arr)==0){
		$x = "''";
	}
	return $x;
}
// ============================== Custom Function ==============================

?>
