<?php 

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$connection = new libgeneralsettings();
$general_settings_SLRS = $connection->Get_General_Setting("SLRS");

$lessonArrangementID= $_POST["lessonArrangementIdAry"];

//echo $leaveID;
for($i=0; $i<sizeof($lessonArrangementID); $i++){
	
	$tempLessonArrangementIDAry[] = (is_numeric($lessonArrangementID[$i])) ? $lessonArrangementID[$i] : substr($lessonArrangementID[$i],1);
	
	
	$sql = "SELECT * FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID=".$tempLessonArrangementIDAry[$i];
	$result = $connection->returnResultSet($sql);
	

	$slrsLessonArrangementInfo = $result[0];
	$DateLeave = date("Y-m-d", strtotime($slrsLessonArrangementInfo['TimeStart']));
	$academicYearInfo = $indexVar['libslrs']->getAcademicYearTerm($DateLeave);
	$academicID = $academicYearInfo[0]["AcademicYearID"];
	
	$userID = $slrsLessonArrangementInfo["UserID"];
	$arrangedTo_userID = $slrsLessonArrangementInfo["ArrangedTo_UserID"];
	
	
	if (isset($general_settings_SLRS['NoSubstitutionCalBalance']) && $general_settings_SLRS['NoSubstitutionCalBalance'] === '1' && $arrangedTo_userID == '-999')
	{
		## ignore 'No substitution'
	}
	else
	{
		// update Balance Adjustment
		$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($userID), $academicID, "REDUCE", $DateLeave . " __TempSubstitutionArrangementDelete__", IntegerSafe($_SESSION['UserID']));
		// Request
		$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance+1, DateTimeModified=NOW(), ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."' WHERE UserID=".IntegerSafe($userID).";";
		//echo $sql;
		$a = $connection->db_db_query($sql);
	}
	
	if ($arrangedTo_userID > 0) {
		
		$indexVar['libslrs']->updateSLRSBalance(IntegerSafe($arrangedTo_userID), $academicID, "REDUCE", $DateLeave . " __TempSubstitutionArrangementDelete__" . "__SubstitutionArrangementSubTC__", IntegerSafe($_SESSION['UserID']));
		// Substition Teacher
		$sql = "UPDATE INTRANET_SLRS_TEACHER SET Balance=Balance-1,DateTimeModified=NOW(),ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID'])." WHERE UserID=".IntegerSafe($arrangedTo_userID).";";
		// echo $sql . "<br>";
		$a = $connection->db_db_query($sql);
	}
	
	$sql = "DELETE FROM INTRANET_SLRS_LESSON_ARRANGEMENT WHERE LessonArrangementID=".$tempLessonArrangementIDAry[$i];
	$connection->db_db_query($sql);
}

$saveSuccess = true;
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=management'.$slrsConfig['taskSeparator'].'temporary_substitute_arrangement'.$slrsConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);

?>