<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

// ============================== Transactional data ==============================
$userID = $_POST["userIdAry"];
if (count($userID) > 0) $sql_userid = implode(',',$userID);
else $sql_userid = '';
if ($sql_userid == "")
{
	header("Location: /home/eAdmin/StaffMgmt/slrs/index.php?task=management.substitute_balance.list");
	exit;
}
//echo $userID;

# Page Title
$TAGS_OBJ[] = array($Lang['SLRS']['SubstituteBalance']);
$indexVar['libslrs_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
// ============================== Includes files/libraries ==============================


$balanceDisplay=$indexVar['libslrs']->getBalanceDisplay();


$TermInfoArr = getCurrentAcademicYearAndYearTerm();
$academicYearID = $TermInfoArr['AcademicYearID'];
$Year_StartDate = getStartDateOfAcademicYear($academicYearID);
$Year_EndDate = getEndDateOfAcademicYear($academicYearID);

$dateLimitation = array();
$dateLimitation['min']["Y"] = date("Y", strtotime($Year_StartDate));
$dateLimitation['min']["M"] = date("m", strtotime($Year_StartDate)) - 1;
$dateLimitation['min']["D"] = date("d", strtotime($Year_StartDate));
$dateLimitation['max']["Y"] = date("Y", strtotime($Year_EndDate));
$dateLimitation['max']["M"] = date("m", strtotime($Year_EndDate)) - 1;
$dateLimitation['max']["D"] = date("d", strtotime($Year_EndDate));
$dateLimitation["initialDate"] = date("Y-m-d", strtotime($Year_StartDate));

$connection = new libgeneralsettings();
$name_field = $indexVar['libslrs']->getNameFieldByLang("b.");
$sql = "SELECT ".$name_field." as userName,a.UserID,previousBalance * ".$balanceDisplay." as previousBalance,updatedBalance * ".$balanceDisplay." as updatedBalance "."\r\n";
$sql .= "FROM (SELECT UserID,Balance as previousBalance,Balance as updatedBalance FROM INTRANET_SLRS_TEACHER WHERE UserID IN (".$sql_userid.")) as a ";
$sql .= "INNER JOIN(
	 SELECT UserID,EnglishName,ChineseName,TitleChinese,TitleEnglish FROM
	 ".$slrsConfig['INTRANET_USER']."
	 ) as b ON a.UserID=b.UserID
	ORDER BY a.UserID;"
;

$a = $connection->returnArray($sql);
$previousBalanceArray = array();
$updateBalanceArray = array();
$userIDArr = array();
$x = "";
$x .= $indexVar['libslrs_ui']->Get_Form_Sub_Title_v30($Lang['SLRS']['SubstitutionBalance']);
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$indexVar['libslrs_ui']->RequiredSymbol().$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedType'].'</td>'."\r\n";		
		$x .= '<td>'."\r\n";
			$x .= $indexVar['libslrs_ui']->Get_Radio_Button('balanceRadio_add', 'balanceSelection', $Value="ADD", $isChecked=0, $Class="requiredField", $Display=$Lang['SLRS']['SubstitutionBalanceAdjTyp']['AddBalance'], $Onclick="",$isDisabled=0);
			$x .= '&nbsp;';
			$x .= $indexVar['libslrs_ui']->Get_Radio_Button('balanceRadio_reduce', 'balanceSelection', $Value="REDUCE", $isChecked=0, $Class="requiredField", $Display=$Lang['SLRS']['SubstitutionBalanceAdjTyp']['ReduceBalance'], $Onclick="",$isDisabled=0);
			$x .= '&nbsp;';
			$x .= $indexVar['libslrs_ui']->Get_Radio_Button('balanceRadio_set', 'balanceSelection', $Value="SET", $isChecked=0, $Class="requiredField", $Display=$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetBalance'], $Onclick="",$isDisabled=0);
			$x .= '<br><br>';
			$x .= $indexVar['libslrs_ui']->Get_Radio_Button('balanceRadio_set_year', 'balanceSelection', $Value="SET_YEAR", $isChecked=0, $Class="requiredField", $Display=$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalance'] . "<br>" . $Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'], $Onclick="",$isDisabled=0);
			$x .= '<span>'.$indexVar['libslrs_ui']->Get_Form_Warning_Msg('balanceSelectionEmptyWarnDiv', $Lang['SLRS']['InputValidation']["AdjustedType"], $Class='warnMsgDiv')."\n".'</span>'."\r\n";
			$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= "<tr style='display:none' id='initialDate'>"."\r\n";
		$x .= "<td class='field_title'><span class='tabletextrequire'>*</span>".$Lang['SLRS']['SubstitutionArrangementDes']['InitialDate'].":</td>"."\r\n";
	// 	$x .= "<td>".$indexVar['libslrs_ui']->GET_DATE_PICKER('datePicker', $date,  $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="changeDatePicker('datePicker')",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum")."</td>"."\r\n";
		$x .= "<td><input type='text' class='slrs-datepicker' data-position='right top' name='datePicker' id='datePicker' value='" . $dateLimitation["initialDate"] . "' /></td>"."\r\n";
	$x .= "</tr>"."\r\n";
	
	$x .= '<tr>'."\r\n";
	$x .= '<td class="field_title">'.$indexVar['libslrs_ui']->RequiredSymbol()."<span id='balanceValueText'>" . $Lang['SLRS']['SubstitutionBalanceDes']['AdjustedValue'].'</span></td>'."\r\n";
	$x .= '<td>'."\r\n";
	$x .= $indexVar['libslrs_ui']->GET_TEXTBOX_NUMBER('adjustedValue', 'number', $number, $OtherClass='requiredField slrs-input', $OtherPar=array('maxlength'=>255));
	$x .= $indexVar['libslrs_ui']->Get_Form_Warning_Msg('adjustedValueEmptyWarnDiv', $Lang['SLRS']['InputValidation']["AdjustedValue"] , $Class='warnMsgDiv')."\n";
	$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionBalanceDes']['Reason'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $indexVar['libslrs_ui']->GET_TEXTAREA('reason', $taContents='', $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='requiredField slrs-textarea', $taID='', $CommentMaxLength='');
			$x .= $indexVar['libslrs_ui']->Get_Form_Warning_Msg('reasonEmptyWarnDiv', 'Please input textarea', $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		// $x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionBalanceDes']['BalanceReview'].'</td>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['SLRS']['SubstitutionBalanceDes']['BalanceTeacher'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			for ($i=0; $i<sizeof($a); $i++){
				// $x .= "<div>".$a[$i]["userName"].": <span id='balance_".$i."'>".$a[$i]["previousBalance"]."->".$a[$i]["updatedBalance"]."</span></div>";
				$x .= "<div>".$a[$i]["userName"]."</div>";
				$previousBalanceArray[$i] = $a[$i]["previousBalance"];
				$updateBalanceArray[$i] = $a[$i]["updatedBalance"];
			}
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['contentTbl'] = $x;
$htmlAry['previousBalanceArr'] = implode(',',$previousBalanceArray);
$htmlAry['updatedBalanceArr'] = implode(',',$updateBalanceArray);
$htmlAry['userIDArr'] = $sql_userid;
$htmlAry['academicYearID'] = Get_Current_Academic_Year_ID();

/*$sql = "SELECT MAX(BalanceAdjustID) as MaxBalanceAdjustID FROM INTRANET_SLRS_BALANCE_ADJUST; ";
$a = $connection->returnArray($sql);*/

// ============================== Transactional data ==============================


// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libslrs_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "back()", 'backBtn');
// ============================== Define Button ==============================
// ============================== Define Functions ==============================

function getBalanceArrayValue($field, $index){
    return $a[$index][$field];
}

// ============================== Define Functions ==============================
?>