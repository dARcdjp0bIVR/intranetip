<form name="form1" id="form1" method="POST" action="?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<br style="clear:both;"><br>
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>	
	</div>
	
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
});

function goSearch() {
	$('form#form1').submit();
}

function goNew(){
	window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>add';
}

function goDelete(){
	checkRemove(document.form1,'userIdAry[]','?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>delete');
}

function goEditSelectionOrder(){
	load_dyn_size_thickbox_ip('<?=$Lang['SLRS']['SubstituteTeacherSettingsDes']['Priority']?>', 'onloadThickBox();');
}

function onloadThickBox() {
	$('div#TB_ajaxContent').load(
		'?task=settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content', 
		{ 
			task: 'settings<?=$slrsConfig['taskSeparator']?>substitute_teacher_settings<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content'
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}


</script>