<form name="form1" id="form1" method="POST" action="?task=settings<?=$slrsConfig['taskSeparator']?>closed_location<?=$slrsConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dataPickerTool']?>
		<?=$htmlAry['viewBtn']?>
		<?=$htmlAry['dbTableActionBtn']?>
		<br style="clear:both;">		
		
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>	
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
});
function goSearch() {
	$('form#form1').submit();
}

function goEdit(){
	checkEdit(document.form1,'LocationRecIdAry[]','?task=settings<?=$slrsConfig['taskSeparator']?>closed_location<?=$slrsConfig['taskSeparator']?>edit');
}

function goDelete(){
	checkRemove(document.form1,'LocationRecIdAry[]','?task=settings<?=$slrsConfig['taskSeparator']?>closed_location<?=$slrsConfig['taskSeparator']?>delete');
}

function goNewForm(){
	window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>closed_location<?=$slrsConfig['taskSeparator']?>edit';
}
</script>