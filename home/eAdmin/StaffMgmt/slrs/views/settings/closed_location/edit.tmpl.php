<link rel="stylesheet" href="/templates/slrs/css/select2.css">
<link rel="stylesheet" href="/templates/slrs/css/datepicker.min.css">
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js"></script>
<script language="JavaScript" src="/templates/slrs/js/select2.full.min.js"></script>
<script language="JavaScript" src="/templates/slrs/js/datepicker.min.js"></script>
<?php if ($intranet_session_language == "en") {?>
<script src="/templates/slrs/js/i18n/en.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.en.js"></script>
<?php } else { ?>
<script src="/templates/slrs/js/i18n/zh-TW.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.zh.js"></script>
<?php } ?>

<form id="form1" name="form1" method="post" action="index.php?task=settings.closed_location.edit">
<input type='hidden' name="token" value='<?php base64_encode($_SESSION["UserID"] . " " . $rec_id); ?>'>
<input type='hidden' name="rec_id" value='<?php echo $rec_id; ?>'>
	<div class="table_board">
		<div>
			<?php echo $htmlAry['contentTbl']; ?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?php echo $htmlAry['submitBtn']; ?>
			<?php echo $htmlAry['backBtn']; ?>
			<p class="spacer"></p>
		</div>
	</div>
</form>

<script language="javascript" type='text/javascript'>

function goSubmit() {
	document.form1.submit();
}

function goBack() {
	window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>closed_location<?=$slrsConfig['taskSeparator']?>list';
}

var slrsJS = {
	vrs: {
		ready: false
	},
	ltr: {
	},
	cfn: {
		setStartDateTime: function(dateVal) {
			if (dateVal == "") return '';
			else {
				var DateArr = dateVal.split(" ");
				var data = {};
				
				data.year = DateArr[0].split("-")[0]; 
				data.month = DateArr[0].split("-")[1] - 1;
				data.day = DateArr[0].split("-")[2];

				data.hour = DateArr[1].split(":")[0];
				data.min = DateArr[1].split(":")[1];
				
				return new Date(data.year, data.month, data.day, data.hour, data.min, '00')
			}
				
		},
		init: function() {
			if ($(".js-basic-single").length > 0) {
			 	$(".js-basic-single").select2({
				 	language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
				 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
				});
			}

			var _startdate = '<?php echo (isset($_POST["StartDate"]) ? $_POST["StartDate"] : ''); ?>';
			var _enddate = '<?php echo (isset($_POST["EndDate"]) ? $_POST["EndDate"] : ''); ?>';
			
			var options = {
					language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh"); ?>",
				    dateFormat: 'yyyy-mm-dd',
				    timeFormat: 'hh:ii',
				    minHours: 5,
			        maxHours: 22,
			        autoClose: true,
				    toggleSelected: true,
				    range: false,
				    timepicker: true,
				    todayButton: true,
				    onSelect: function(data, date, inst) {
					    if (slrsJS.vrs.ready) {
						    switch (inst.el.id) {
						    	case "StartDate":
						    		end_datepicker.update({
							    		minDate: date
						    		});
							    	break;
						    	case "EndDate":
						    		start_datepicker.update({
							    		maxDate: date
						    		});
						    }
					    }
				    }
				};

			var selectedDate = slrsJS.cfn.setStartDateTime(_startdate);
			
			$('#StartDate').datepicker(options);
			var start_datepicker = $('#StartDate').datepicker({startDate: selectedDate}).data('datepicker');
			start_datepicker.selectDate(selectedDate);			
			
			selectedDate = slrsJS.cfn.setStartDateTime(_enddate);
			
			$('#EndDate').datepicker(options);
			var end_datepicker = $('#EndDate').datepicker({startDate: selectedDate}).data('datepicker');
			end_datepicker.selectDate(selectedDate);
			slrsJS.vrs.ready = true;
		}
	}	
};
$(document).ready(function() {
	slrsJS.cfn.init();
});
</script>