<form name="form1" id="form1" method="POST" action="?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>list">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<div class="content_top_tool">
			<?=$htmlAry['dataPickerTool']?>
			<?=$htmlAry['viewBtn']?>
			<?=$htmlAry['dbTableActionBtn']?>
			<br style="clear:both;">
		</div>
		
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>	
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
});
function goSearch() {
	$('form#form1').submit();
}

function goEditSelectionOrder(){
	//window.location = '?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>edit';
	checkEdit(document.form1,'supplyIdAry[]','?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>edit');
}

function goDelete(){
	checkRemove(document.form1,'supplyIdAry[]','?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>delete');
	//window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>delete';
}

function goNew(){
	window.location = '?task=settings<?=$slrsConfig['taskSeparator']?>external_teacher_arrangement<?=$slrsConfig['taskSeparator']?>edit';
}


</script>