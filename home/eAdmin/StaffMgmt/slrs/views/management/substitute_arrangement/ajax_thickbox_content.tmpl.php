<div id="thickboxContainerDiv" class="edit_pop_board">
	<form id="thickboxForm" name="thickboxForm">
		<div id="thickboxContentDiv" class="edit_pop_board_write">
			<?=$htmlAry['contentTbl']?>			
		</div>
		<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['closeBtn']?>
			<p class="spacer"></p>
		</div>
	</form>
</div>
<script language='javascript' type='text/javascript'>
var xhr = "";
var loadingImg = '<?php echo $indexVar['libslrs_ui']->Get_Ajax_Loading_Image(); ?>';
function reloadForm() {
	var year_ID = $("select[name='yearID']").val();
	var user_ID = '<?php echo $userID; ?>';
	if (xhr != "") {
		xhr.abort();
	} 
	$('#tableContainer').html(loadingImg);
	xhr = $.ajax({
		url: "?task=management.substitute_balance.ajax_thickbox_content",
		type: 'POST',
		data: { userID: user_ID, yearID: year_ID },
		success: function(data) {
	    	$('#TB_ajaxContent').html(data);
	    	$('#thickboxContentDiv').height(parseInt($('#TB_ajaxContent').height()) - 70);      
		}
	});
}
</script>