<?php  
include_once("../../../../includes/libinterface.php");
$linterface = new interface_html();

## print btn
$toolbar = $linterface->GET_LNK_PRINT("javascript:void(0);", $Lang["SLRS"]["BtnPrint"], "js_print_substitution_balance_table_list();", "", "", 0);
## export btn
$toolbar .= " ";
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:void(0);", $Lang["SLRS"]["BtnExport"], "js_substitution_balance_table_list_export();", "", "", 0);
$htmlAry['contentTool'] = $toolbar;
?>
<form name="form1" id="form1" method="POST" action="?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>list">
	<div class="table_board">
		<div class="content_top_tool"><br>
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>	
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
});

function thickBox(id){
	load_dyn_size_thickbox_ip('<?=$Lang['SLRS']['SubstitutionBalanceDes']['AdjustmentRecord']?>', 'onloadThickBox('+id+');');
}

function onloadThickBox(id) {
	$('div#TB_ajaxContent').load(
		'?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content', 
		{ 
			userID: id,
			task: 'management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content'
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function goSearch() {
	$('form#form1').submit();
}

function goEdit(){
	//window.location = '?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>edit';
	checkEditMultiple(document.form1,'userIdAry[]','?task=management<?=$slrsConfig['taskSeparator']?>substitute_balance<?=$slrsConfig['taskSeparator']?>edit');
}

function js_print_substitution_balance_table_list(){
	var param = "?task=management.substitute_balance.view_print";
	var SubmitPage = "index.php";
	var url = SubmitPage + param;
// 	newWindow(url, 10);
	document.form1.action = url;
	document.form1.target = '_blank';
	document.form1.submit();
	document.form1.action = '';
	document.form1.target = '';
}

function js_substitution_balance_table_list_export(){
	var param = "?task=management.substitute_balance.export";
	var SubmitPage = "index.php";
	var url = SubmitPage + param;
	document.form1.action = url;
	document.form1.target = '_blank';
	document.form1.submit();
	document.form1.action = '';
	document.form1.target = '';
}


</script>