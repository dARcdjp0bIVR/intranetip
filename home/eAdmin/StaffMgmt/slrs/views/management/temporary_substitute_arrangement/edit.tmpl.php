<link rel="stylesheet" href="/templates/slrs/css/select2.css?25911a">
<link rel="stylesheet" href="/templates/slrs/css/datepicker.min.css?25911a">
<script language="JavaScript" src="/templates/jquery/jquery-1.11.1.min.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/deprecated.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/select2.full.min.js?25911a"></script>
<script language="JavaScript" src="/templates/slrs/js/datepicker.min.js?25911a"></script>
<?php if ($intranet_session_language == "en") {?>
<script src="/templates/slrs/js/i18n/en.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.en.js"></script>
<?php } else { ?>
<script src="/templates/slrs/js/i18n/zh-TW.js"></script>
<script src="/templates/slrs/js/i18n/datepicker.zh.js"></script>
<?php } ?>
  <form id="form1" name="form1" method="post" action="index.php">
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
	</div>
	
	<input type="hidden" id="startIndex" value="<?=$htmlAry['startIndex']?>">
	<input type="hidden" id="endIndex" value="<?=$htmlAry['endIndex']?>">
	<input type="hidden" id="leaveID" value="<?=$htmlAry['leaveID']?>">
	<!-- <input type="hidden" id="day" value="<?=$htmlAry['day']?>"> -->
	
</form>

<script type="text/javascript">
var isLoad = false;

$(document).ready( function() {
	$('#submitBtn').hide();
	$('#backBtn').hide();
	var arrangmentID = getURLParameter("arrangementID");
	if(arrangmentID != "null"){
		$('#submitBtn').show();
		$('#backBtn').show();		
		$("#datePicker").attr("disabled", true);
		$(".datepick-trigger").remove();
		$("#leaveTeacher, #lesson").attr("disabled", true);
		$(':radio').each(function(){				 
			 var name = this.name;
			 var ignoreSub = "ignoreThisRec" + name.replace("teacher", "");
			 /*
			 if($('input[name='+name+']:checked').val() == undefined){					 
				 $('input[name='+name+'][disabled=false]:first').attr("checked", true);
				 $('input[name='+name+']:first').trigger("click");					 
			 }
			 */
			 if($('input[name="'+name+'"]:checked').val() == undefined && $('input[name="'+ignoreSub+'"]:checked').val() == undefined){
				 $('input[name='+name+'][disabled=false]:first').attr("checked", true);
				 $('input[name='+name+']:first').trigger("click");
			 }
		});
	}

	$(".ignoreAction").bind('click', ignoreAction);
	
<?php if ($PresetLocationInfo["LimitLocation"]) { ?>
	presetLocationJS.cfn.init();
<?php } ?>
	/*
	if ($(".js-select2").length > 0) {
		$(".js-select2").css({'width':'100%'});
	 	$(".js-select2").select2({
		 	language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
		 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
		});
	 	$(".js-select2").css({'max-width':'320px'});
	}
	*/
	slrsJS.cfn.init();
});

$("#datePicker, #leaveTeacher, #lesson").change(function(){
	$("#lessonDetails").empty();
	$("#lessonEmptyWarnDiv span").empty();	
});

$("#lesson").change(function(){
	$("#lessonDetails").html('<div style="padding:10px;"><?php echo $indexVar['libslrs_ui']->Get_Ajax_Loading_Image(); ?></div>');
	$('#submitBtn').hide();
	$('#backBtn').hide();
	$("#lessonEmptyWarnDiv span").empty();
	
	// check whether this teacher has been substituted by the external teacher
	var data = "type=checkExistence&userID="+$("#leaveTeacher option:selected").val()+"&timeSlotID="+$("#lesson option:selected").val()+"&date="+$("#datePicker").val();
	$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_checkData', 
        data: data,async:true,timeout:1000,
        success: function (data, textStatus, jqXHR) {
           	var result = eval('(' + data + ')');
           	if(result[0]["result"]==0){
               	isLoad = true;
               	loadLessonDetails();
           	}
           	else{
               	isLoad = false;
               	$("#lessonDetails").empty();
               	$("#lessonEmptyWarnDiv").show();
               	$("#lessonEmptyWarnDiv span").empty();
               	$("#lessonEmptyWarnDiv span").text("*"+result[0]["msg"]);
           	}
           	
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });	
});


function goSubmit() {
	$("#datePicker").attr("disabled", false);
	$("#leaveTeacher, #lesson").attr("disabled", false);
	var startIndexVal = parseInt($("#startIndex").val());
	var endIndexVal = parseInt($("#endIndex").val());		
	var leaveIDVal = parseInt($("#leaveID").val());
	var startIndex = $("<input/>").attr("type", "hidden").attr("name", "startIndex").val(startIndexVal);
	var endIndex = $("<input/>").attr("type", "hidden").attr("name", "endIndex").val(endIndexVal);
	var leaveID = $("<input/>").attr("type", "hidden").attr("name", "leaveID").val(leaveIDVal);
	$('#form1').append($(startIndex)).append($(endIndex)).append($(leaveID));
	$('form#form1').attr('action', 'index.php?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>edit_update').submit();
}

function goBack() {
	window.location = '?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>list';
}

function changeDatePicker(id){			
	var data = "type=teacher"+"&date="+ $("#"+id).val();		
	$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {				
           	var result = eval('(' + data + ')');
           	// get room information
           	var firstOptionValue = $("#leaveTeacher" + " option:first").val();
           	var firstOptionText = $("#leaveTeacher" + " option:first").text();
			$("#leaveTeacher").empty();
			$("#leaveTeacher").append($('<option></option>').val(firstOptionValue).html(firstOptionText));
            for (i=0; i<result.length; i++){
            	$("#leaveTeacher").append($('<option></option>').val(result[i].UserID).html(result[i].Name));
            }                 
            $("#lesson").empty();
            $("#lesson").append($('<option></option>').val(firstOptionValue).html(firstOptionText));
            $("#lessonDetails").empty();
            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}

$("#leaveTeacher").change(function(){
	var leaveTeacherID = $("#leaveTeacher option:selected").val();
	getRoomDetails(leaveTeacherID);
});

function getRoomDetails(id){
	var inputID = id;
	var index = inputID.substr(inputID.indexOf("_")+1,1);
	var val = $("#"+inputID +" option:selected").val();
	var data = "type=room&"+"teacher="+id+"&date="+$("#datePicker").val();
	$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {
           	var result = eval('(' + data + ')');
           	// get room information
           	var firstOptionValue = $("#lesson" + " option:first").val();
           	var firstOptionText = $("#lesson" + " option:first").text();
			$("#lesson").empty();
			$("#lesson").append($('<option></option>').val(firstOptionValue).html(firstOptionText))
            for (i=0; i<result.length; i++){
            	$("#lesson").append($('<option></option>').val(result[i].TimeSlotID).html(result[i].Name).attr('rel', result[i].SubjectGroupID));
            }                
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}
/*
$("#lesson").change(function(){
	var leaveTeacherID = $("#leaveTeacher option:selected").val();
	var lessonID = $("#lesson option:selected").val();
	//alert(isLoad);
	if (isLoad == true){
		loadLessonDetails();
	}
});*/

var jsSelectOBJ = [];
var presetLocationJS = {
		vrs: {
			limitCount: <?php echo (isset($PresetLocationInfo["ThisCount"])) ? $PresetLocationInfo["ThisCount"] : 0; ?>,
			currentUsed: 0,
			LimitLocation: '<?php echo ($PresetLocationInfo["LimitLocation"]) ? 'true' : 'false'; ?>',
			presetID: <?php echo ($PresetLocationInfo["LocationID"] > 0) ? $PresetLocationInfo["LocationID"] : 0; ?>,
			type: '<?php echo $PresetLocationInfo["type"]; ?>',
			defVal: []
		},
		ltr: {
			locationChangeHandle: function(e) {
				var prevVal = presetLocationJS.vrs.defVal[$(this).attr('id')];
				if (typeof prevVal != "undefined") {
					presetLocationJS.vrs.defVal[$(this).attr('id')] = $(this).val();
					if (prevVal == presetLocationJS.vrs.presetID && $(this).val() != presetLocationJS.vrs.presetID) {
						presetLocationJS.vrs.currentUsed--;
					} else if (prevVal != presetLocationJS.vrs.presetID && $(this).val() == presetLocationJS.vrs.presetID) {
						presetLocationJS.vrs.currentUsed++;
						var name = $(this).attr('name');
						var ignoreAction = "#ignoreThisRec_" + name.replace("locationID_", "");
						if ($(ignoreAction).size() > 0) {
							if ($(ignoreAction).is(":checked") == false) {
								$(ignoreAction).trigger('click');
							}
						}
					}
					presetLocationJS.cfn.initSelection();
					try {
						$(".js-select2").select2({
								allowClear: true,
	 							language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
	 							placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
						});
					} catch(err) {
						// console.clear();
					}
				} 
		
			}
		},
		cfn: {
			initSelection: function() {
				$(".js-select2").each(function(index) {
					$(this).find("option[value='" + presetLocationJS.vrs.presetID + "']").eq(0).removeAttr('disabled');
				});
				if (presetLocationJS.vrs.LimitLocation) {
					$(".js-select2").bind('change', presetLocationJS.ltr.locationChangeHandle);
					if (presetLocationJS.vrs.currentUsed >= presetLocationJS.vrs.limitCount) {
						$(".js-select2").each(function(index) {
							if ($(this).val() != presetLocationJS.vrs.presetID) {
								$(this).find("option[value='" + presetLocationJS.vrs.presetID + "']").eq(0).attr('disabled', 'disabled');
							}
						});
					}
				}
			},
			init: function() {
				if ($(".js-select2").size() > 0) {
					$(".js-select2").each(function(index) {
						presetLocationJS.vrs.defVal[$(this).attr('id')] = $(this).val();
						if ($(this).val() == presetLocationJS.vrs.presetID) {
							presetLocationJS.vrs.currentUsed++;
							if (presetLocationJS.vrs.type == "edit") {
								presetLocationJS.vrs.limitCount++;
							}
						}
					});
					presetLocationJS.cfn.initSelection();
				}
			}
		}
	};

function ignoreAction(e) {
	var name = $(this).attr('name');
	var teacher_name = "teacher_" + name.substring((name.indexOf('ignoreThisRec_')+"ignoreThisRec_".length), name.length);
	if ($(this).is(':checked')) {
		if ($("input[name='" + teacher_name + "']:checked").length > 0) {
			var parentDetail = $("input[name='" + teacher_name + "']:checked").parent().parent();
			if (parentDetail.find('.row_right_none').size() > 0) {
				parentDetail.find('.row_right_none').each(function() {
					$(this).removeClass('row_suggest');
				});
			}
			$("input[name='" + teacher_name + "']:checked").attr("checked", false);
		}
	} else {
		if ($("input[name='" + teacher_name + "']:checked").length > 0) {
		} else {
			$('input[name='+teacher_name+'][disabled=false]:first').attr("checked", true);
			$('input[name='+teacher_name+']:first').trigger("click");
//			e.preventDefault();
		}
	}
}

function loadLessonDetails(){
	var leaveTeacherID = $("#leaveTeacher option:selected").val();
	var lessonID = $("#lesson option:selected").val();
	var subjectgrp = $("#lesson option:selected").attr('rel');
	
	var data = "type=lessonDetails&"+"teacher="+leaveTeacherID+"&date="+$("#datePicker").val()+"&lesson="+lessonID+"&subjectgrp="+subjectgrp;
	$.ajax({ type: "POST", url: '?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,timeout:0,
        success: function (data, textStatus, jqXHR) {  
        	$('#submitBtn').show();
        	$('#backBtn').show();         
            $("#lessonDetails tr").remove();
           	$("#lessonDetails").html(data);
           	var preInfo = $('#presetItemJS').html();

           	if (typeof preInfo != "undefined") {
               	var jsonData = JSON.parse(preInfo);
               	if (jsonData.LimitLocation) {

               		presetLocationJS.vrs.LimitLocation = jsonData.LimitLocation;
               		presetLocationJS.vrs.limitCount = jsonData.ThisCount;
               		presetLocationJS.vrs.presetID = jsonData.LocationID;
               		presetLocationJS.vrs.type = jsonData.type;
               		presetLocationJS.cfn.init();
               	}
           	} 
           	$(".ignoreAction").bind('click', ignoreAction);
           	$(':radio').each(function(){				 
	   			 var name = this.name;
	   			 var ignoreSub = "ignoreThisRec" + name.replace("teacher", "");
	   			 /*
	   			 if($('input[name='+name+']:checked').val() == undefined){					 
	   				 $('input[name='+name+'][disabled=false]:first').attr("checked", true);
	   				 $('input[name='+name+']:first').trigger("click");					 
	   			 }
	   			 */
	   			 if($('input[name="'+name+'"]:checked').val() == undefined && $('input[name="'+ignoreSub+'"]:checked').val() == undefined){
	   				 $('input[name='+name+'][disabled=false]:first').attr("checked", true);
	   				 $('input[name='+name+']:first').trigger("click");
	   				 console.log(name);
	   			 }
	   				
	   		});
			if ($(".js-select2").length > 0) {
    			$(".js-select2").css({'width':'100%'});
    		 	$(".js-select2").select2({
    			 	language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
    			 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
    			});
    		 	$(".js-select2").css({'max-width':'320px'});
    		}
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}

function goView(teacherID){
	var date = $("#datePicker").val();
	var day = $("#day").val();
	var leaveID = $("#leaveID").val();
	load_dyn_size_thickbox_ip('<?=$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Timetable']?>', 'onloadThickBox("'+teacherID+'","'+date+'","'+day+'","'+leaveID+'");', inlineID='', defaultHeight=550, defaultWidth=550);
	
}

function onloadThickBox(id,date,day,leaveID) {
	$('div#TB_ajaxContent').load(
		'?task=management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content', 
	{ 
		userID: id,
		date: date,
		day: day,
		leaveID: leaveID,
		task: 'management<?=$slrsConfig['taskSeparator']?>temporary_substitute_arrangement<?=$slrsConfig['taskSeparator']?>ajax_thickbox_content'
		},
		function(ReturnData) {
			//adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

$("[id^='locationText']").click(function(e){
	var id = e.target.id;
	var index = id.split("_").pop();
	goChangeLocation(index);
});

function goChangeLocation(index){
	var text = $("#locationText_"+index).text();
	if(text == "<?= $Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToBeChanged'] ?>"){
		$("#locationText_"+index).text("<?= $Lang['SLRS']['TemporarySubstitutionArrangementDes']['Cancel'] ?>");
		var tgtID = "#locationDiv_"+index;
		$("#locationDiv_"+index).show();
	}
	else{
		$("#locationText_"+index).text("<?= $Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToBeChanged'] ?>");
		var hideLocationID = $("#oriLocationID_"+index).val();
		var tgtID = "#locationDiv_"+index;
		$("#locationDiv_"+index +" option[value="+hideLocationID+"]").attr("selected","selected");
		$("#locationDiv_"+index).hide();
	}
}


function goMore(id){
	//alert(id);
	var index = id;
	var rowspan = $("#rowspan_"+index).val();
	$("#tdTime_"+index).attr("rowspan",rowspan);
	$("#tdDate_"+index).attr("rowspan",rowspan);
	$("#tdLocation_"+index).attr("rowspan",rowspan);
	$("[id^='row_more_"+index+"']").remove();
	$("[id^='row_" + "']").show();
}

$("[id^='details_']").click(function(e){
	var id = e.target.id;	
	hlRow(id);

});

function getURLParameter(name) {
    return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]);
}
/*
function hlRow(id){
	var id = id;
	var index = "";
	if(id.indexOf("teacher") != -1){
		//alert(id.indexOf('teacher')+ " " + "teacher".length);
		index = id.substring((id.indexOf('teacher')+"teacher".length), id.length);
	}
	else if(id.indexOf("details") != -1){
		index = id.substring((id.indexOf('details')+"details".length), id.length);
	}
	if(index != ""){
		$("[id^='details_']").removeClass("row_suggest");
		$("[id='details" + index + "']").addClass("row_suggest");
		$("#teacher"+index).attr('checked', true);
	}
}*/
function hlRow(id){
	var id = id;
	var index = "";
	var indexArr = "";
	if(id.indexOf("teacher") != -1){
		index = id.substring((id.indexOf('teacher')+"teacher".length), id.length);
		indexArr = id.split('_');
	}
	else if(id.indexOf("details") != -1){
		index = id.substring((id.indexOf('details')+"details".length), id.length);
		indexArr = id.split('_');
	}
	if(index != ""){
		$("[id^='details_"+indexArr[1]+"']").removeClass("row_suggest");
		$("[id='details" + index + "']").addClass("row_suggest");
		$("#teacher"+index).attr('checked', true);

		var ignoreIndex = parseInt(indexArr[1]) + 1;
		$("#ignoreThisRec_"+ignoreIndex).attr('checked', false);
	}

}
var slrsJS = {
		vrs: {
			ready: false
		},
		ltr: {
		},
		cfn: {
			init: function() {
				if ($(".js-basic-single").length > 0) {
				 	$(".js-basic-single").select2({
				 		language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh-TW"); ?>",
					 	placeholder: '<?php echo $Lang['SLRS']['NotAvailableTeachersForm']["please-select"]; ?>'
					});
				}
				var options = {
						language: "<?php echo (($intranet_session_language == "en") ? "en" : "zh"); ?>",
					    dateFormat: 'yyyy-mm-dd',
				        autoClose: true,
				        immediateUpdates: true,
				        showOnFocus:true,
					    toggleSelected: false,
					    range: false,
					    timepicker: false,
					    todayButton: true,
					    minDate: new Date(<?php echo $dateLimitation['min']["Y"]; ?>, <?php echo $dateLimitation['min']["M"]; ?>, <?php echo $dateLimitation['min']["D"]; ?>),
					    maxDate: new Date(<?php echo $dateLimitation['max']["Y"]; ?>, <?php echo $dateLimitation['max']["M"]; ?>, <?php echo $dateLimitation['max']["D"]; ?>),
					    onSelect: function(data, date, inst) {
					    	changeDatePicker('datePicker');
					    }
					};
				$('#datePicker').datepicker(options);
				$('#datePicker').bind("change", function (e) {
					changeDatePicker('datePicker');
				});;
				slrsJS.vrs.ready = true;
			}
		}	
	};
</script>
