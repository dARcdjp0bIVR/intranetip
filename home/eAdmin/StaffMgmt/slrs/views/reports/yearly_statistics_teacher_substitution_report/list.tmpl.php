<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {
});

$("#academicYearID").change(function(){
	var data="academicYearID="+$("#academicYearID").val()+"&type=academicYearTerm";
	$.ajax({ type: "POST", url: '?task=reports<?=$slrsConfig['taskSeparator']?>yearly_statistics_teacher_substitution_report<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {
        	var result = eval('(' + data + ')');
            $("#semesterDiv").empty();
            for(i=0;i<result.length;i++){
            	var checkbox = "<input type='checkbox' value='"+result[i]["YearTermID"]+"' id='semester_"+result[i]["YearTermID"]+"' name='semester'/><label for='semester_"+result[i]["YearTermID"]+"'>"+result[i]["Name"]+"</label>";
                $('#semesterDiv').append(checkbox);
            }                     	            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
});

function goRptGen(){
	var semester = "";
    $('input:checkbox[id^="semester"]:checked').each(function (i,e) {
    	var comma = (semester.length==0)?"":",";
    	semester+=comma+e.value;
    });
	var data="academicYearID="+$("#academicYearID").val()+"&semester="+semester+"&type=info";
	$.ajax({ type: "POST", url: '?task=reports<?=$slrsConfig['taskSeparator']?>yearly_statistics_teacher_substitution_report<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {            
            $("#report").empty();
            $("#report").append(data); 
            reportContent = data;          	            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}

function print_timetable(){
	var semester = "";
    $('input:checkbox[id^="semester"]:checked').each(function (i,e) {
    	var comma = (semester.length==0)?"":",";
    	semester+=comma+e.value;
    });
	var data="academicYearID="+$("#academicYearID").val()+"&semester="+semester;
	var url = '?task=reports<?=$slrsConfig['taskSeparator']?>yearly_statistics_teacher_substitution_report<?=$slrsConfig['taskSeparator']?>view_print&'+data;
	newWindow(url,35);
	
}

function export_csv(){
	var semester = "";
    $('input:checkbox[id^="semester"]:checked').each(function (i,e) {
    	var comma = (semester.length==0)?"":",";
    	semester+=comma+e.value;
    });
	var data="academicYearID="+$("#academicYearID").val()+"&semester="+semester;
	var url = '?task=reports<?=$slrsConfig['taskSeparator']?>yearly_statistics_teacher_substitution_report<?=$slrsConfig['taskSeparator']?>export&'+data;
	//newWindow(url,35);
	window.location.href = url;
}



</script>