<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {
});

function goRptGen(){
	var startDate=$("#startDate").val();
	var endDate = $("#endDate").val();
	var data = "startDate="+startDate+"&endDate="+endDate;
	$.ajax({ type: "POST", url: '?task=reports<?=$slrsConfig['taskSeparator']?>summary_classroom_change_lesson_report<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {
            $("#report").empty();
            $("#report").append(data); 
            reportContent = data;          	            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}

function print_timetable()
{
	var url = '?task=reports<?=$slrsConfig['taskSeparator']?>summary_classroom_change_lesson_report<?=$slrsConfig['taskSeparator']?>view_print&startDate='+$("#startDate").val()+"&endDate="+$("#endDate").val();
	newWindow(url,35);
	//window.open('?task=reports<?=$slrsConfig['taskSeparator']?>daily_teacher_substitution_report<?=$slrsConfig['taskSeparator']?>view_print','xxx','height=500,width=500').document.write(reportContent);
}

function export_csv(){
	var url = '?task=reports<?=$slrsConfig['taskSeparator']?>summary_classroom_change_lesson_report<?=$slrsConfig['taskSeparator']?>export&startDate='+$("#startDate").val()+"&endDate="+$("#endDate").val();
	//newWindow(url,35);
	window.location.href = url;
}



</script>