<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {
});

function goRptGen(){
    var data="startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val();
    
    $.ajax({ type: "POST", url: '?task=reports<?=$slrsConfig['taskSeparator']?>teachers_absence_substitution_summary_report<?=$slrsConfig['taskSeparator']?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {            
            $("#report").empty();
            $("#report").append(data); 
            reportContent = data;          	            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}

function print_timetable(){
	var data="startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val();
	var url = '?task=reports<?=$slrsConfig['taskSeparator']?>teachers_absence_substitution_summary_report<?=$slrsConfig['taskSeparator']?>view_print&'+data;
	newWindow(url,35);
	
}

function export_csv(){
	var data="startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val();
	var url = '?task=reports<?=$slrsConfig['taskSeparator']?>teachers_absence_substitution_summary_report<?=$slrsConfig['taskSeparator']?>export&'+data;
	//newWindow(url,35);
	window.location.href = url;
}



</script>