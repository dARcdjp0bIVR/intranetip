<div class="table_board">
	<div>
		<?php echo $htmlAry['contentTbl']; ?>
	</div>
	<?php echo $htmlAry['detailedOptionDiv'];?>	
	<br style="clear:both;" />
	<div class="edit_bottom_v30" id="generateArea">
		<p class="spacer"></p>
		<?php echo $htmlAry['viewBtn']; ?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>

<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {
	slrsReportJS.func.init();
});

function goDateChange() {
	slrsReportJS.listeners.dateChangeHandler();
}

function goRptGen(){
	var date=$("#datePicker").val();
	var selteacher = slrsReportJS.func.getTeacherToStr();

	var reportTarget = 'LeaveTeacher';
	if ($(slrsReportJS.vars.reportTargetOptElm).length > 0)
	{
		reportTarget = $(slrsReportJS.vars.reportTargetOptElm).val(); 
	}
	var data = "date="+date+"&lthr="+selteacher+ "&reportTarget=" + reportTarget;
	
	$.ajax({ type: "POST", url: '?task=reports<?php echo $slrsConfig['taskSeparator']; ?>daily_teacher_substitution_report<?php echo $slrsConfig['taskSeparator']; ?>ajax_getData', 
        data: data,
        success: function (data, textStatus, jqXHR) {
            $("#report").empty();
            $("#report").append(data); 
            reportContent = data;          	            
        }, error: function (jqXHR, textStatus, errorThrown) {
           
        }
    });
}

function print_timetable()
{
	var selteacher = slrsReportJS.func.getTeacherToStr();
	var display_signature = $('#display_signature').attr('checked') ? 1 : 0;
	var display_remark = $('#display_remark').attr('checked') ? 1 : 0;

	var url = '?task=reports<?php echo $slrsConfig['taskSeparator']; ?>daily_teacher_substitution_report<?php echo $slrsConfig['taskSeparator']; ?>view_print&date='+$("#datePicker").val() + '&lthr=' + selteacher + '&display_signature=' + display_signature + '&display_remark=' + display_remark;
	var reportTarget = 'LeaveTeacher';
	if ($(slrsReportJS.vars.reportTargetOptElm).length > 0)
	{
		reportTarget = $(slrsReportJS.vars.reportTargetOptElm).val(); 
	}
	url += "&reportTarget=" + reportTarget;
	
	newWindow(url,35);
	//window.open('?task=reports<?php echo $slrsConfig['taskSeparator']; ?>daily_teacher_substitution_report<?php echo $slrsConfig['taskSeparator']; ?>view_print','xxx','height=500,width=500').document.write(reportContent);
}

function export_csv(){
	var selteacher = slrsReportJS.func.getTeacherToStr();
	var url = '?task=reports<?php echo $slrsConfig['taskSeparator']; ?>daily_teacher_substitution_report<?php echo $slrsConfig['taskSeparator']; ?>export&date='+$("#datePicker").val() + '&lthr=' + selteacher;
	var reportTarget = 'LeaveTeacher';
	if ($(slrsReportJS.vars.reportTargetOptElm).length > 0)
	{
		reportTarget = $(slrsReportJS.vars.reportTargetOptElm).val(); 
	}
	url += "&reportTarget=" + reportTarget;
	//newWindow(url,35);
	window.location.href = url;
}

function goRptSnd() {
	slrsReportJS.listeners.SendBtnHandler();
	return false;
}

var slrsReportJS = {
	vars: {
		reportDataObj: "#report",
		reportArea: "#report .edit_bottom_v30",
		datePickerContainer: "#datePicker",
		sendBtnObj: "sendingMsg",
		sendRptBtnId: "#sendRptBtn",
		sendingTxt: "<?php echo $Lang['SLRS']['ReportGenerationSending']; ?>",
		successTxt: "<?php echo $Lang['SLRS']['ReportGenerationSuccess']; ?>",
		failedTxt: "<?php echo $Lang['SLRS']['ReportGenerationFailed']; ?>",
		noRecordTxt: "<?php echo $Lang['SLRS']['ReportGenerationNoRecord']; ?>",
		sendMsgURL: '?task=reports<?php echo $slrsConfig['taskSeparator']; ?>daily_teacher_substitution_report<?php echo $slrsConfig['taskSeparator']; ?>ajax_sendMsgForDSReport',
		successMsg: "Success",
		teacherXHR: "",
		selectTeacherObj: "leaveTeacherIdIdSel",
		defaultKeyupFunction:"",
		generateArea: "#generateArea",
		getTeacherJSonURL: '?task=reports<?php echo $slrsConfig['taskSeparator']; ?>daily_teacher_substitution_report<?php echo $slrsConfig['taskSeparator']; ?>ajax_getLeaveTeacherData',
		reportTargetOptElm: 'input#report_target:checked'
	},
	listeners: {
		SendBtnHandler: function() {
			var date=$(slrsReportJS.vars.datePickerContainer).val();
			var selteacher = slrsReportJS.func.getTeacherToStr();

			var reportTarget = 'LeaveTeacher';
			if ($(slrsReportJS.vars.reportTargetOptElm).length > 0)
			{
				reportTarget = $(slrsReportJS.vars.reportTargetOptElm).val(); 
			}
			var data = "date="+date+"&lthr="+selteacher+ "&reportTarget=" + reportTarget;
			
			var msgAreaObj = $(slrsReportJS.vars.reportArea + " #" + slrsReportJS.vars.sendBtnObj);
			$(slrsReportJS.vars.sendRptBtnId).hide();
			if (msgAreaObj.size() > 0) {
				msgAreaObj.css({"color":"#f00"}).html(slrsReportJS.vars.sendingTxt);
			} else {
				$(slrsReportJS.vars.reportArea).eq(0).append("<div id='" + slrsReportJS.vars.sendBtnObj + "' style='color:#f00; padding-top:3px; padding-bottom:3px;'>" + slrsReportJS.vars.sendingTxt + "</div>");
			}
			$.ajax({
				type: "POST",
				url: slrsReportJS.vars.sendMsgURL, 
	        	data: data,
	        	dataType: "json",
	        	success: function (data) {
	        		var msgAreaObj = $(slrsReportJS.vars.reportArea + " #" + slrsReportJS.vars.sendBtnObj);
		        	if (typeof data.result != "undefined" && data.result == slrsReportJS.vars.successMsg) {
		        		msgAreaObj.css({"color":"#00f"}).html(slrsReportJS.vars.successTxt);
		        		setTimeout("goRptGen()", 1000);
		        	} else {
		        		$(slrsReportJS.vars.sendRptBtnId).show();
		        		msgAreaObj.html(slrsReportJS.vars.failedTxt);
		        	}
		        }, error: function (jqXHR, textStatus, errorThrown) {
		        	var msgAreaObj = $(slrsReportJS.vars.reportArea + " #" + slrsReportJS.vars.sendBtnObj);
		        	$(slrsReportJS.vars.sendRptBtnId).show();
		        	msgAreaObj.html(slrsReportJS.vars.failedTxt);
				}
	    	});
		},
		dateChangeHandler: function() {
			var selteacher = slrsReportJS.func.getTeacherToStr();
			$("#" + slrsReportJS.vars.selectTeacherObj + " option").remove();
			$(generateArea).hide();
			var xhr = slrsReportJS.vars.teacherXHR;
			$("#report").empty();
			if (xhr != "") {
				xhr.abort();
				slrsReportJS.vars.teacherXHR = "";
			}
			var date=$(slrsReportJS.vars.datePickerContainer).val();

			var reportTarget = 'LeaveTeacher';
			if ($(slrsReportJS.vars.reportTargetOptElm).length > 0)
			{
				reportTarget = $(slrsReportJS.vars.reportTargetOptElm).val(); 
			}
			var data = "date="+date+"&lthr="+selteacher+ "&reportTarget=" + reportTarget;
			
			slrsReportJS.vars.teacherXHR = $.ajax({
				type: "POST",
				url: slrsReportJS.vars.getTeacherJSonURL, 
	        	data: data,
	        	dataType: "json",
	        	success: function (data) {
	        		if (typeof data.result != "undefined" && typeof data.teacherObj != "undefined" && data.teacherObj != "" && data.teacherObj != null) {
	        			$.each(data.teacherObj, function(index, data) {
	        				$("#" + slrsReportJS.vars.selectTeacherObj).append('<option value="' + data.UserID + '">' + data.userName + '</option>');
	        			});
	        			// js_Select_All(slrsReportJS.vars.selectTeacherObj, 1);
	        			$("#" + slrsReportJS.vars.selectTeacherObj).parent().parent().find('input.formsmallbutton').show();
	        			$(generateArea).show();
	        		} else {
	        			$("#" + slrsReportJS.vars.selectTeacherObj).append('<option value="0">' + slrsReportJS.vars.noRecordTxt + '</option>');
	        			$("#" + slrsReportJS.vars.selectTeacherObj).parent().parent().find('input.formsmallbutton').hide();			        		
	        		}
		        }, error: function (jqXHR, textStatus, errorThrown) {
		        	// console.log(jqXHR, textStatus, errorThrown);
		        	$("#" + slrsReportJS.vars.selectTeacherObj).append('<option value="0">' + slrsReportJS.vars.noRecordTxt + '</option>');
		        	$("#" + slrsReportJS.vars.selectTeacherObj).parent().parent().find('input.formsmallbutton').hide();
				}
			});
		}
	},
	func: {
		getTeacherToStr: function() {
			var leaveTeacher =$("#" + slrsReportJS.vars.selectTeacherObj).val();
			var selteacher = "all";
			if (leaveTeacher == null) {
				js_Select_All(slrsReportJS.vars.selectTeacherObj, 1);
			}
			leaveTeacher =$("#" + slrsReportJS.vars.selectTeacherObj).val();
			if (leaveTeacher != null) {
				selteacher = "";
				$.each(leaveTeacher, function(index, seldata) {
					if (selteacher != "") {
						selteacher += ",";
					}
					selteacher += seldata;
				});
			}
			return selteacher;
		},
		init: function() {
			js_Select_All(slrsReportJS.vars.selectTeacherObj, 1);
			slrsReportJS.listeners.dateChangeHandler();
		}
	}
};
function showDetailedOptionDiv(btnId) {
	var optionDivId = 'detailedOptionDiv';
	var leftAdjustment = $('a.print').width();
	topAdjustment = 0;
	
	changeLayerPosition(btnId, optionDivId, leftAdjustment, topAdjustment);
	MM_showHideLayers(optionDivId, '', 'show');
}

function hideDetailedOptionDiv() {
	MM_showHideLayers('detailedOptionDiv', '', 'hide');
}

function hideAllOptionLayer() {
	hideSimpleOptionDiv();
	hideDetailedOptionDiv();
}
</script>