<?php
// using 
################## Change Log [Start] ##############
#	Date:	2016-08-15 	Omas
#			created this page for autocompleter
################## Change Log [End] ################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Get data
$YearID = $_REQUEST['YearID'];
//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);

$libdb = new libdb();
$SearchValue = $libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q']))));
$sql = "SELECT  
			UserID, 
			".getNameFieldByLang()." as Name, 
			ClassName, 
			ClassNumber 
		FROM 
			INTRANET_USER 
		WHERE 
			RecordStatus = 1
			AND RecordType IN ( 1 )
			AND USERID NOT IN ('".$SelectedUserIDsStr."') 
			AND ( ChineseName like'%".$SearchValue."%' 
					or EnglishName like '%".$SearchValue."%') 
		Order by EnglishName asc
		Limit 5";
$result = $libdb->returnResultSet($sql);

$returnString = '';
foreach((array)$result as $_StudentInfoArr){
	$_UserID = $_StudentInfoArr['UserID'];
	$_Name = $_StudentInfoArr['Name'];
	$_ClassName = $_StudentInfoArr['ClassName'];
	$_ClassNumber = $_StudentInfoArr['ClassNumber'];
	if($_ClassName != '' && $_ClassNumber != ''){
		$displayClass = ' ('.$_ClassName.'-'.$_ClassNumber.')';
	}
	$_thisUserNameWithClassAndClassNumber = $_Name.$displayClass;

	$returnString .= $_thisUserNameWithClassAndClassNumber.'|'.'|'.'U'.$_UserID."\n";
}

intranet_closedb();
echo $returnString;
?>