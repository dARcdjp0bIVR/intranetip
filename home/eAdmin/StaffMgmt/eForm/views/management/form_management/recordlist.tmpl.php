<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css" type="text/css" />
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<div class='folder_breadcrumb'>
<?php echo $htmlBreadcrumb; ?>
</div>
<div class='contenttitle'><?php echo $formInfo["FormTitle"];?></div>
<table class='form_table_v30'>
<tr>
	<td class='field_title'><?php echo $Lang["eForm"]["StartDate"]; ?></td>
	<td><?php echo $formInfo["StartDate"]; ?></td>
</tr>
<tr>
	<td class='field_title'><?php echo $Lang["eForm"]["DueDate"]; ?></td>
	<td><?php echo $formInfo["DueDate"]; ?></td>
</tr>
</table><br>
<form name="form1" id="form1" method="POST" action="?task=management<?=$eFormConfig['taskSeparator']?>form_management<?=$eFormConfig['taskSeparator']?>recordlist&folder=<?php echo $folderID; ?>&fid=<?php echo $_GET["fid"]; ?>&ctk=<?php echo $_GET["ctk"]; ?>">
	<div class="table_board">
	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>
			<?=$htmlAry['searchBox']?>
			<br style="clear:both;">
		</div>
		
		<p class="spacer"></p>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
		
		<?=$htmlAry['hiddenField']?>
		<?=$htmlAry['maxRow']?>
		
	</div>
</form>
<div style='text-align:center; margin-top:20px; margin-bottom:20px;'>
<?php echo $htmlAry['backBtn']; ?>
</div>
<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});	
	eClassFormJS.cfn.init();
});

function goBack() {
	window.location.href = "?task=management/form_management/list&folder=<?php echo $folderID; ?>";
}

function goSearch() {
	$('form#form1').submit();
}

function goEdit(){
	checkEdit(document.form1,'groupIdAry[]','?task=management<?=$eFormConfig['taskSeparator']?>form_management<?=$eFormConfig['taskSeparator']?>edit');
}
function goDelete(){
	checkRemove(document.form1,'groupIdAry[]','?task=management<?=$eFormConfig['taskSeparator']?>form_management<?=$eFormConfig['taskSeparator']?>userdelete&fid=<?php echo $_GET["fid"]; ?>&ctk=<?php echo $_GET["ctk"]; ?>');
}

function goNewForm() {
	window.location.href = "?task=management<?=$eFormConfig['taskSeparator']?>form_management<?=$eFormConfig['taskSeparator']?>edit";
}

var eClassFormJS = {
	vrs: {
		formBlrContainer: ".thickboxBTN",
	},
	ltr: {
		disableHandler: function(e) {
			e.preventDefault();
		},
		formBlrClickHandler: function(e) {
			e.preventDefault();
			var url = $(this).attr('rel');
			var title = $(this).attr('title');
			var extraparams = title;
			eClassFormJS.cfn.openFormBuilder(url, extraparams);
		},
	},
	cfn: {
		init: function() {
			if ($(eClassFormJS.vrs.formBlrContainer).length > 0) {
				$(eClassFormJS.vrs.formBlrContainer).bind('click', eClassFormJS.ltr.formBlrClickHandler);
			}
			$('a.linkdisable').bind('click', eClassFormJS.ltr.disableHandler);
			$(window).bind('hashchange', eClassFormJS.ltr.disableHandler);
			var strHash = window.location.hash;
			if (typeof strHash != "undefined" && strHash != "") {
				strHash = strHash.replace("#", "");
				if (strHash == "UpdateSuccess") {
					alert("<?php echo $Lang["eForm"]["DataRemoveSuccess"]; ?>");
				} else {
					alert("<?php echo $Lang["eForm"]["DataRemoveUnsuccess"]; ?>");
				}
				window.location.hash = "";
			}
		},
		openFormBuilder: function(url, extraparams) {
			var isUse = 1;
			var defaultHeight = "";
			var defaultWidth = "";
			var tbType = 1;
			var callBackFunc = "updateFromThickbox";
			var inlineID = "";
			show_dyn_thickbox_ip(extraparams, url, extraparams, isUse, defaultHeight, defaultWidth, tbType);
		}
	}	
};

</script>