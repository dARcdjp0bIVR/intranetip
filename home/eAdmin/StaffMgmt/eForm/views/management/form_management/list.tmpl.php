<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="/templates/eForm/assets/fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css" type="text/css" />
<link rel="stylesheet" href="/templates/eForm/assets/css/tip.css" type="text/css" />

<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery-migrate-1.4.1.js"></script>
<script src="/templates/eForm/assets/fancybox/jquery.fancybox.min.js"></script>

<div class='eform_holder list'>
	<div class='eform_content_area'>
    	<form name="form1" id="form1" method="POST" action="?task=management<?php echo $eFormConfig['taskSeparator']?>form_management<?=$eFormConfig['taskSeparator']?>list&folder=<?php echo $folderID; ?>&slug=<?php echo $folderInfo["FolderSlug"]; ?>">
    	<div class="table_board">
    		<div id="selectedhdr">
    			<div class='card card-warnings'>
    				<div class='card-title'>
    					<div class='panel_action_title'></div>
    					<b><?php echo $Lang["eForm"]["Folders"]["selectedFolder"]; ?>:</b><br><br>
    					<div id="item_title">-</div>
    				</div>
    				<div id='folder_action'>
    					<div class='actionbox set blue'>
							<a href='#' data-fancybox data-src="#boxAddNewFolder" class='btn pri eform_act act_add_folder'><i class="fa fa-plus"></i> <?php echo $Lang["eForm"]["Folders"]["addNewFolder"]; ?></a>
							<a href='#' data-fancybox data-src="#boxEditFolder" class='btn pri eform_act act_edit_folder'><i class="fa fa-pencil"></i> <?php echo $Lang["eForm"]["Folders"]["changeFolderName"]; ?></a>
<?php if ($folderID != "0") {?>
							<a href='#' class='btn sec moveFolderStart'><i class="fa fa-window-restore"></i> <?php echo $Lang["eForm"]["Folders"]["moveFolderButton"]; ?></a>
<?php } ?>
							<a href='#' data-fancybox data-src="#boxMoveFolder" class='btn pri eform_act act_folder_move tip-bottom  tip-always' aria-label="<?php echo $Lang["eForm"]["Folders"]["tip_clickMoveAction"]; ?>"><i class="fa fa-window-restore"></i> <?php echo $Lang["eForm"]["Folders"]["moveFolderToThisFolder"]; ?></a>
<?php if ($folderInfo["total_forms"] > 0) { ?>
    						<a href='#' class='btn sec moveFileStart'><i class="fa fa-window-restore"></i> <?php echo $Lang["eForm"]["Folders"]["moveFileButton"]; ?></a>
        					<a href='#' data-fancybox data-src="#boxMoveFile" class='btn pri eform_act act_file_move tip-bottom  tip-always' aria-label="<?php echo $Lang["eForm"]["Folders"]["tip_clickMoveAction"]; ?>"><i class="fa fa-sign-in"></i> <?php echo $Lang["eForm"]["Folders"]["moveFileToThisFolder"]; ?></a>
<?php } ?>
						</div>
    					<div class='actionbox set red'>
	        				<a href='#' data-fancybox data-src="#boxDeleteFolder" class='btn pri eform_act act_folder_delete'><i class="fa fa-trash-o"></i> <?php echo $Lang["eForm"]["Folders"]["deleteThisFolder"]; ?></a>
    					</div>
    					<div class='actionbox set black'>
    						<a href='#' class='btn sec eform_act selectAllFiles'><i class="fa fa-check-square-o"></i> <?php echo $Lang["eForm"]["Folders"]["SelectAllFormItem"]; ?></a>
    						<a href='#' class='btn pri eform_act unselectAllFiles'><i class="fa fa-square-o"></i> <?php echo $Lang["eForm"]["Folders"]["UnselectAllFormItem"]; ?></a>
    					</div>
    					<div class='clearboth'></div>
    					<div id='action_remarks'></div>
    					<div id='closeOptions'>
    						<div class='actionbox set red'>
    							<a href='#' class='btn pri moveFolderEnd' aria-label="<?php echo $Lang["eForm"]["Folders"]["tip_closepanel"]; ?>"><i class="fa fa-window-close"></i> <?php echo $Lang["eForm"]["Folders"]["closeButton"]; ?></a>
    						</div>
						</div>
						<div class='clearboth'></div>
    				</div>
				</div>
			</div>
			<div class='folder_breadcrumb'>
    			<?php echo $htmlBreadcrumb; ?>
    		</div>
    		<div class="content_top_tool">
    			<?php echo $htmlAry['contentTool']; ?>
    			<?php echo $htmlAry['searchBox']; ?>
    			<br style="clear:both;">
    		</div>
    		
    		<p class="spacer"></p>
    		<?php echo $htmlAry['dbTableActionBtn']?>
    		<?php echo $htmlAry['dataTable']?>
    		<p class="spacer"></p>
    		
    		<?php echo $htmlAry['hiddenField']?>
    		<?php echo $htmlAry['maxRow']?>
    		
    	</div>
    	</form>
        <script type="text/javascript">
        
        function goSearch() {
        	$('form#form1').submit();
        }
        
        function goEdit(){
        	checkEdit(document.form1,'groupIdAry[]','?task=management<?php echo $eFormConfig['taskSeparator'];?>form_management<?php echo $eFormConfig['taskSeparator'];?>edit&slug=<?php echo $folderInfo["FolderSlug"];?>');
        }
        function goDelete(){
        	checkRemove(document.form1,'groupIdAry[]','?task=management<?php echo $eFormConfig['taskSeparator'];?>form_management<?php echo $eFormConfig['taskSeparator'];?>delete&folder=<?php echo $folderID; ?>&slug=<?php echo $folderInfo["FolderSlug"];?>');
        }
        
        function goNewForm() {
        	window.location.href = "?task=management<?php echo $eFormConfig['taskSeparator']; ?>form_management<?php echo $eFormConfig['taskSeparator'];?>edit&folder=<?php echo $folderID; ?>&slug=<?php echo $folderInfo["FolderSlug"];?>";
        }

        function updateFromThickbox() {
        }

        var eClassFormJS = {
        	vrs: {
        		formBlrContainer: ".thickboxBTN",
        	},
        	ltr: {
        		disableHandler: function(e) {
        			e.preventDefault();
        		},
        		formBlrClickHandler: function(e) {
        			e.preventDefault();
        			var url = $(this).attr('rel');
        			var title = $(this).attr('title');
        			var extraparams = title;
        			eClassFormJS.cfn.openFormBuilder(url, extraparams);
        		},
        	},
        	cfn: {
        		init: function() {
        			if ($(eClassFormJS.vrs.formBlrContainer).length > 0) {
        				$(eClassFormJS.vrs.formBlrContainer).bind('click', eClassFormJS.ltr.formBlrClickHandler);
        			}
        			$('a.linkdisable').bind('click', eClassFormJS.ltr.disableHandler);
        			$(window).bind('hashchange', eClassFormJS.ltr.disableHandler);
        			var strHash = window.location.hash;
        			if (typeof strHash != "undefined" && strHash != "") {
        				strHash = strHash.replace("#", "");
        				alert(strHash);
        				window.location.hash = "";
        			}
        		},
        		openFormBuilder: function(url, extraparams) {
        			var isUse = 1;
        			var defaultHeight = "";
        			var defaultWidth = "";
        			var tbType = 1;
        			var callBackFunc = "updateFromThickbox";
        			var inlineID = "";
        			show_dyn_thickbox_ip(extraparams, url, extraparams, isUse, defaultHeight, defaultWidth, tbType);
        		}
        	}	
        };
        
        var eFolderJS = {
            vrs: {
                treeElm : '',
                xhr: '',
                movingAction: '',
                totalFiles: 0,
                limitation: 5,
                selectedElm: '',
                selected_folder: "<?php echo $folderID; ?>",
                selected_folder_total_child : <?php echo (!empty($folderInfo["child_folders"]) ? $folderInfo["child_folders"] : '0'); ?>,
                selected_folder_ds: [<?php echo (!empty($folderInfo["IdTrees"]) ? $folderInfo["IdTrees"] : '0'); ?>],
                selected_folder_parent: "<?php echo isset($folderInfo["ParentID"]) ? $folderInfo["ParentID"] : 0; ?>",
                selected_folder_title: "<?php $folderObj = end($folderBreadcrumb); echo $folderObj["title"]; ?>",
                page_url: "?task=management/form_management/list",
                url: "?task=management/form_management/folder",
                debug: false,
                animatedClass: ''
            },
            ltr: {
            	selectedFileHandler: function(e) {
                	if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > selectedFileHandler");
                	var node = $("#tree").fancytree("getTree").getSelectedNodes();  
                	eFolderJS.cfn.checkSelectedFiles();
                	eFolderJS.cfn.actionPanelInfo(node);
                	eFolderJS.cfn.checkSelectedFiles();
            	},
                expandAllHandler: function(e) {
                	if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > expandAllHandler");
                	$("#tree").fancytree("getTree").visit(function(node){
                    	node.setExpanded();
                    	if (eFolderJS.vrs.movingAction != "") {
                    		eFolderJS.cfn.controlTreeOption();
                    	}
    				});
                	e.preventDefault();
                },
            	hiddleAllHandler: function(e) {
            		if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > hiddleAllHandler");
            		$("#tree").fancytree("getTree").visit(function(node){
                    	node.setExpanded(false);
                    	eFolderJS.cfn.controlTreeOption();
    				});
                	e.preventDefault();
            	},
            	buttonClickHandler: function (e) {
            		if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > buttonClickHandler");
		            var hasError = false;
		            switch ($(this).attr('id')) {
		            	case "btnSubmitAddFolder":
		            		if ($("input[name='title_en']").eq(0).val() == "") {
		            			hasError = true;
		            			$("input[name='title_en']").eq(0).addClass('err');
		            		} else {
		            			$("input[name='title_en']").eq(0).removeClass('err');
		            		}

		            		if (!hasError) {
			            		var xhr = eFolderJS.vrs.xhr;
			            		if (xhr != "") {
				            		xhr.abort();
			            		}
			            		var post_url = "?task=management/form_management/add_folder";
			            		var postdata = { title_en: $("input[name='title_en']").eq(0).val(), folder: eFolderJS.vrs.selected_folder };

			            		$('#boxAddNewFolder .fancybox-close-small').hide();
			            		$('#boxAddNewFolder .boxFooter').hide();
			            		$('#boxAddNewFolder .boxBody').hide();
			            		$('#boxAddNewFolder .boxMsg').html("<i class='fa fa-save'></i> <?php echo $Lang["eForm"]["Folders"]["please_wait"]; ?>").removeClass('warning').show();

			            		eFolderJS.vrs.xhr = $.ajax({
			            					type: "POST",
			            					url: post_url,
			            					data: postdata,
			            					dataType: 'json',
			            					success: function(data) {
			            						if (data != null && eFolderJS.vrs.debug) console.log(data);
												if (typeof data.result != "undefined" && data.result == "success") {
    			            						var successHTML = "<i class='fa fa-check-square-o'></i> <?php echo $Lang["eForm"]["Folders"]["success_addfolder"]; ?><br><br>";
    			            						successHTML += "<div class='set blue'><a href='" + eFolderJS.vrs.page_url + "&folder=" + data.ParentID + "' class='btn pri'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a></div>";
    				            					
    			            						$("input[name='title_en']").eq(0).val('');

    			            						$('#boxAddNewFolder .boxMsg').addClass('info').html(successHTML).removeClass('warning').show();
												} else {
													$('#boxAddNewFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_addfolder"]; ?>").removeClass('warning').show();
				            						$('#boxAddNewFolder .boxMsg').addClass('warning');
				            						$('#boxAddNewFolder .boxBody').show();
				            						$('#boxAddNewFolder .fancybox-close-small').show();
				            						$('#boxAddNewFolder .boxFooter').show();
												}
			            					},
			            					error: function(XMLHttpRequest, textStatus, errorThrown) {

			            						$('#boxAddNewFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_addfolder"]; ?>").removeClass('warning').show();
			            						$('#boxAddNewFolder .boxMsg').addClass('warning');
			            						$('#boxAddNewFolder .boxBody').show();
			            						$('#boxAddNewFolder .fancybox-close-small').show();
			            						$('#boxAddNewFolder .boxFooter').show();
			            					}
			            			});
		            		}
		            		break;
		            	case "btnSubmitMoveFolder":
		            		var node = $("#tree").fancytree("getTree").getSelectedNodes();
		            		var xhr = eFolderJS.vrs.xhr;
		            		if (xhr != "") {
			            		xhr.abort();
		            		}
		            		var post_url = "?task=management/form_management/move_folder";
		            		if (typeof node[0].key != "undefined" && eFolderJS.vrs.selected_folder_parent != node[0].key) {

    		            		var postdata = { folder: eFolderJS.vrs.selected_folder, newParent: node[0].key };
    
    		            		$('#boxMoveFolder .fancybox-close-small').hide();
    		            		$('#boxMoveFolder .boxFooter').hide();
    		            		$('#boxMoveFolder .boxBody').hide();
    		            		$('#boxMoveFolder .boxMsg').html("<i class='fa fa-save'></i> <?php echo $Lang["eForm"]["Folders"]["please_wait"]; ?>").removeClass('warning').show();
    		            		
    		            		eFolderJS.vrs.xhr = $.ajax({
                					type: "POST",
                					url: post_url,
                					data: postdata,
                					dataType: 'json',
                					success: function(data) {
                						if (eFolderJS.vrs.debug) console.log(data);
    									if (data != null && typeof data.result != "undefined" && data.result == "success") {
    	            						var successHTML = "<i class='fa fa-check-square-o'></i> <?php echo $Lang["eForm"]["Folders"]["success_movefolder"]; ?><br><br>";
    	            						successHTML += "<div class='set blue'><a href='" + eFolderJS.vrs.page_url + "&folder=" + eFolderJS.vrs.selected_folder + "' class='btn pri'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a></div>";
    	            						$('#boxMoveFolder .boxMsg').addClass('info').html(successHTML).removeClass('warning').show();
    									} else {
    										$('#boxMoveFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_movefolder"]; ?>").removeClass('warning').show();
    	            						$('#boxMoveFolder .boxMsg').addClass('warning');
    	            						$('#boxMoveFolder .boxBody').show();
    	            						$('#boxMoveFolder .fancybox-close-small').show();
    	            						$('#boxMoveFolder .boxFooter').show();
    									}
                					},
                					error: function(XMLHttpRequest, textStatus, errorThrown) {
    
                						$('#boxMoveFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_movefolder"]; ?>").removeClass('warning').show();
                						$('#boxMoveFolder .boxMsg').addClass('warning');
                						$('#boxMoveFolder .boxBody').show();
                						$('#boxMoveFolder .fancybox-close-small').show();
                						$('#boxMoveFolder .boxFooter').show();
                					}
                				});
		            		}
			            	break;
		            	case "btnSubmitEditFolder":
			            		if ($("input[name='edit_title_en']").eq(0).val() == "") {
			            			hasError = true;
			            			$("input[name='edit_title_en']").eq(0).addClass('err');
			            		} else {
			            			$("input[name='edit_title_en']").eq(0).removeClass('err');
			            		}
			            		if (!hasError) {
				            		var xhr = eFolderJS.vrs.xhr;
				            		if (xhr != "") {
					            		xhr.abort();
				            		}
				            		var post_url = "?task=management/form_management/edit_folder";
				            		var postdata = { title_en: $("input[name='edit_title_en']").eq(0).val(), folder: eFolderJS.vrs.selected_folder };
				            		$('#boxEditFolder .fancybox-close-small').hide();
				            		$('#boxEditFolder .boxFooter').hide();
				            		$('#boxEditFolder .boxBody').hide();
				            		$('#boxEditFolder .boxMsg').html("<i class='fa fa-save'></i> <?php echo $Lang["eForm"]["Folders"]["please_wait"]; ?>").removeClass('warning').show();

				            		eFolderJS.vrs.xhr = $.ajax({
				            					type: "POST",
				            					url: post_url,
				            					data: postdata,
				            					dataType: 'json',
				            					success: function(data) {
				            						if (eFolderJS.vrs.debug) console.log(data);
													if (data != null && typeof data.result != "undefined" && data.result == "success") {
	    			            						var successHTML = "<i class='fa fa-check-square-o'></i> <?php echo $Lang["eForm"]["Folders"]["success_editfolder"]; ?><br><br>";
	    			            						successHTML += "<div class='set blue'><a href='" + eFolderJS.vrs.page_url + "&folder=" + data.ParentID + "' class='btn pri'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a></div>";
	    				            					
	    			            						$("input[name='edit_title_en']").eq(0).val('');
	    			    			            		
	    			            						$('#boxEditFolder .boxMsg').addClass('info').html(successHTML).removeClass('warning').show();
													} else {
														$('#boxEditFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_editfolder"]; ?>").removeClass('warning').show();
					            						$('#boxEditFolder .boxMsg').addClass('warning');
					            						$('#boxEditFolder .boxBody').show();
					            						$('#boxEditFolder .fancybox-close-small').show();
					            						$('#boxEditFolder .boxFooter').show();
													}
				            					},
				            					error: function(XMLHttpRequest, textStatus, errorThrown) {

				            						$('#boxEditFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_editfolder"]; ?>").removeClass('warning').show();
				            						$('#boxEditFolder .boxMsg').addClass('warning');
				            						$('#boxEditFolder .boxBody').show();
				            						$('#boxEditFolder .fancybox-close-small').show();
				            						$('#boxEditFolder .boxFooter').show();
				            					}
				            			});
			            		}
			            	break;
		            	case "btnDeleteFolder":
		            		var node = $("#tree").fancytree("getTree").getSelectedNodes();
		            		var xhr = eFolderJS.vrs.xhr;
		            		if (xhr != "") {
			            		xhr.abort();
		            		}
		            		var post_url = "?task=management/form_management/delete_folder";
		            		if (eFolderJS.vrs.selected_folder != '0') {

    		            		var postdata = { folder: eFolderJS.vrs.selected_folder };
    
    		            		$('#boxDeleteFolder .fancybox-close-small').hide();
    		            		$('#boxDeleteFolder .boxFooter').hide();
    		            		$('#boxDeleteFolder .boxBody').hide();
    		            		$('#boxDeleteFolder .boxMsg').html("<i class='fa fa-save'></i> <?php echo $Lang["eForm"]["Folders"]["please_wait"]; ?>").removeClass('warning').show();
    		            		
    		            		eFolderJS.vrs.xhr = $.ajax({
                					type: "POST",
                					url: post_url,
                					data: postdata,
                					dataType: 'json',
                					success: function(data) {
                						if (eFolderJS.vrs.debug) console.log(data);
    									if (data != null && typeof data.result != "undefined" && data.result == "success") {
    	            						var successHTML = "<i class='fa fa-check-square-o'></i> <?php echo $Lang["eForm"]["Folders"]["success_deletefolder"]; ?><br><br>";
    	            						successHTML += "<div class='set blue'><a href='" + eFolderJS.vrs.page_url + "&folder=" + data.ParentID + "' class='btn pri'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a></div>";
    	            						$('#boxDeleteFolder .boxMsg').addClass('info').html(successHTML).removeClass('warning').show();
    									} else {
    										$('#boxDeleteFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_deletefolder"]; ?>").removeClass('warning').show();
    	            						$('#boxDeleteFolder .boxMsg').addClass('warning');
    	            						$('#boxDeleteFolder .boxBody').show();
    	            						$('#boxDeleteFolder .fancybox-close-small').show();
    	            						$('#boxDeleteFolder .boxFooter').show();
    									}
                					},
                					error: function(XMLHttpRequest, textStatus, errorThrown) {
    
                						$('#boxDeleteFolder .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_deletefolder"]; ?>").removeClass('warning').show();
                						$('#boxDeleteFolder .boxMsg').addClass('warning');
                						$('#boxDeleteFolder .boxBody').show();
                						$('#boxDeleteFolder .fancybox-close-small').show();
                						$('#boxDeleteFolder .boxFooter').show();
                					}
                				});
		            		}
			            	break;
	            	
		            	case "btnSubmitMoveFile":
		            		var node = $("#tree").fancytree("getTree").getSelectedNodes();
		            		var xhr = eFolderJS.vrs.xhr;
		            		if (xhr != "") {
			            		xhr.abort();
		            		}
		            		var post_url = "?task=management/form_management/move_file";
		            		var selectedForms = [];

		            		if ($('.efolder_files').length > 0) {
			            		var i = 0;
		            			$('.efolder_files').each(function(index, data) {
			            			if ($(this).prop('checked') == true) {
		            					selectedForms[i] = $(this).val();
		            					i++;
			            			}
		            			});
		            		}
		            		if (typeof node[0].key != "undefined" && eFolderJS.vrs.selected_folder != node[0].key && selectedForms.length > 0) {

    		            		var postdata = { folder: eFolderJS.vrs.selected_folder, newParent: node[0].key, formItems: selectedForms };
   		            		
    		            		$('#boxMoveFile .fancybox-close-small').hide();
    		            		$('#boxMoveFile .boxFooter').hide();
    		            		$('#boxMoveFile .boxBody').hide();
    		            		$('#boxMoveFile .boxMsg').html("<i class='fa fa-save'></i> <?php echo $Lang["eForm"]["Folders"]["please_wait"]; ?>").removeClass('warning').show();
    		            		
    		            		eFolderJS.vrs.xhr = $.ajax({
                					type: "POST",
                					url: post_url,
                					data: postdata,
                					dataType: 'json',
                					success: function(data) {

                						if (eFolderJS.vrs.debug) console.log(data);
    									if (data != null && typeof data.result != "undefined" && data.result == "success") {
    	            						var successHTML = "<i class='fa fa-check-square-o'></i> <?php echo $Lang["eForm"]["Folders"]["success_moveFile"]; ?><br><br>";
    	            						successHTML += "<div class='set blue'><a href='" + eFolderJS.vrs.page_url + "&folder=" + eFolderJS.vrs.selected_folder + "' class='btn pri'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a></div>";
    	            						$('#boxMoveFile .boxMsg').addClass('info').html(successHTML).removeClass('warning').show();
    									} else {
    										$('#boxMoveFile .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_moveFile"]; ?>").removeClass('warning').show();
    	            						$('#boxMoveFile .boxMsg').addClass('warning');
    	            						$('#boxMoveFile .boxBody').show();
    	            						$('#boxMoveFile .fancybox-close-small').show();
    	            						$('#boxMoveFile .boxFooter').show();
    									}
                					},
                					error: function(XMLHttpRequest, textStatus, errorThrown) {
    
                						$('#boxMoveFile .boxMsg').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["failed_moveFile"]; ?>").removeClass('warning').show();
                						$('#boxMoveFile .boxMsg').addClass('warning');
                						$('#boxMoveFile .boxBody').show();
                						$('#boxMoveFile .fancybox-close-small').show();
                						$('#boxMoveFile .boxFooter').show();
                					}
                				});
		            		}
			            	break;
		            } 
		            e.preventDefault();
	            },
	            moveFileHandler: function(e) {
	            	if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > moveFileHandler");
	            	eFolderJS.cfn.startOption('move_file');
	            	e.preventDefault();
	            },
	            moveFolderHandler: function(e) {
	            	if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > moveFolderHandler");
	            	eFolderJS.cfn.startOption('move_folder');
	            	e.preventDefault();
	            },
	            closeOptionsHandler: function(e) {
	            	if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > closeOptionsHandler");
		            eFolderJS.cfn.closeOption();
					e.preventDefault();
				},
				selectAllFileHandler: function(e) {
					e.preventDefault();
					if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > selectAllFileHandler");
					$('.efolder_files').each(function() {
						$(this).prop("checked", true);
					});
						
					eFolderJS.cfn.checkSelectedFiles();
					var node = $("#tree").fancytree("getTree").getSelectedNodes();
			        eFolderJS.cfn.actionPanelInfo(node);
			        eFolderJS.cfn.checkSelectedFiles();
				},
				unselectAllFileHandler: function(e) {
					e.preventDefault();
					if (eFolderJS.vrs.debug) console.log("eFolderJS > ltr > unselectAllFileHandler");
					$('.efolder_files').each(function() {
						$(this).prop("checked", false);
					});
					eFolderJS.cfn.checkSelectedFiles();
					var node = $("#tree").fancytree("getTree").getSelectedNodes();
			        eFolderJS.cfn.actionPanelInfo(node);
			        eFolderJS.cfn.checkSelectedFiles();
				}
            },
			cfn: {
				controlTreeOption: function(ig) {
					if (eFolderJS.vrs.debug) console.log("eFolderJS > cfn > controlTreeOption");
					if (eFolderJS.vrs.movingAction == "move_folder") {

						$("#tree").fancytree("option", "checkbox", true);
	            		$("#tree").fancytree("option", "clickFolderMode", 2);
	            		$("#tree").fancytree("option", "checkbox", true);
	            		
 	            		$('.fancytree-unselectable').parent().find('.fancytree-checkbox').hide();
 	            		$('.fancytree-unselectable').parent().find('.fancytree-title').css({ 'color': '#afafaf'});
	            		
	            	} else if (eFolderJS.vrs.movingAction == "move_file") {

	            		$("#tree").fancytree("option", "checkbox", true);
	            		$("#tree").fancytree("option", "clickFolderMode", 2);
	            		$("#tree").fancytree("option", "checkbox", true);

// 	            		$('.fancytree-selected').parent().find('.fancytree-checkbox').show();
// 	            		$('.fancytree-selected').parent().find('.fancytree-title').css({ 'color': '#000'});

// 	            		$('.fancytree-selected').parent().find('.fancytree-checkbox').eq(0).hide();
// 	            		$('.fancytree-selected').parent().find('.fancytree-title').eq(0).css({ 'color': '#afafaf'});

	            	} else if (typeof ig == "undefined") {

	            		$("#tree").fancytree("option", "checkbox", false);
 	            		$("#tree").fancytree("option", "clickFolderMode", 1);

 	            		$("#tree").fancytree("option", "source", {
 	            			url: eFolderJS.vrs.url + "&type=init&folder=" + eFolderJS.vrs.selected_folder,
        			        dataType: "json",
        			        cache: false});
	            	}
				},
				startOption: function(action) {
					if (eFolderJS.vrs.debug) console.log("eFolderJS > cfn > startOption");
					eFolderJS.vrs.movingAction = action;

					// close all action button
					$('.act_add_folder').hide();
					$('.act_folder_delete').hide();
					$('.act_edit_folder').hide();
					
					$('.moveFolderStart').hide();
            		$('.moveFileStart').hide();
            		
					$('#closeOptions').show();
					
	            	$('.card .card-title').slideDown('slow', function() {
		            	$('#closeOptions a.moveFolderEnd').bind('click', eFolderJS.ltr.closeOptionsHandler);
		            	eFolderJS.cfn.controlTreeOption();

		            	$('#tree').addClass('tip-top-right tip-always tip-red');
		            	$('#closeOptions a.moveFolderEnd').addClass('tip-top-left tip-always tip-red');
		            	
	            	});
	            	$('.card').addClass('selected');
	            	$('#tree').delay(2000).addClass(eFolderJS.vrs.animatedClass);
	            	if (action == "move_folder") {
		            	$('.panel_action_title').html("<h3><span><?php echo $Lang["eForm"]["Folders"]["moveFolder"]; ?></span></h3>");
	            		$('#action_remarks').removeClass('warning').html("<i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["promptMsgForSelectParentFolder"]; ?>").show();
    	            	$('.common_table_list_v30').slideUp('slow');
					} else {
						$('.panel_action_title').html("<h3><span><?php echo $Lang["eForm"]["Folders"]["moveFile"]; ?></span></h3>");
						$('#action_remarks').removeClass('warning').html("<i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["promptMsgForSelectFolderAndFile"]; ?>").show();

						$('.common_table_list_v30').eq(0).addClass('tip-left tip-always');
						
						// find Table Header and hide Latest th
						$('.common_table_list_v30').find('thead tr').each(function(index, data) {
							var trObj = $(this).find('th');
							trObj.last().hide();
						});
						// find Table Body and hide Latest td and change Index to checkbox
						$('.common_table_list_v30').find('tbody tr').each(function(index, data) {
							var trObj = $(this).find('td');

							var itemVal = trObj.find('a.preview').eq(0).attr('rel');
							trObj.last().hide();

							var indexElm = trObj.first(); 
							indexElm.attr('rel', indexElm.text());
							indexElm.html('<input type="checkbox" class="efolder_files" value="' + itemVal + '" class="selected_movefile">');
							indexElm.addClass('move_file');
							indexElm.delay(2000).addClass(eFolderJS.vrs.animatedClass);
						});
						if ($('.efolder_files').length > 0) {
							$('.efolder_files').bind('click', eFolderJS.ltr.selectedFileHandler);
		            		$('.selectAllFiles').show();
	            		}
					}
	            	$('.content_top_tool').hide();
	            	$('.common_table_tool').hide();
	            	$('.common_table_bottom').hide();
				},
				closeOption: function() {
					if (eFolderJS.vrs.debug) console.log("eFolderJS > cfn > closeOption");

					$('#tree').removeClass('tip-top-right tip-always tip-warning');
					$('#folder_action').removeClass('tip-top-right tip-always tip-warning');
					$('.common_table_list_v30').eq(0).removeClass('tip-left tip-always tip-warning');
					
					$('#closeOptions a.moveFolderEnd').unbind('click', eFolderJS.ltr.closeOptionsHandler);
					$('.selectAllFiles').hide();
					$('.unselectAllFiles').hide();
					$('.moveFolderStart').show();
					$('.moveFileStart').show();
					
					$('#action_remarks').removeClass('warning').hide().html('');
					$('#closeOptions').hide();

					$('.content_top_tool').show();
	            	$('.common_table_tool').show();
	            	$('.common_table_bottom').show();
	            	$('.card').removeClass('selected');
	            	$('#tree').removeClass(eFolderJS.vrs.animatedClass);
	            	if (eFolderJS.vrs.movingAction == "move_file") {
    	            	$('.common_table_list_v30').find('thead tr').each(function(index, data) {
    	            		var trObj = $(this).find('th');
    	            		trObj.last().show();
    					});
    					$('.common_table_list_v30').find('tbody tr').each(function(index, data) {
    						var trObj = $(this).find('td');
    						trObj.last().show();
    						var indexElm = trObj.first(); 
    						indexElm.removeClass('move_file');
    						indexElm.removeClass(eFolderJS.vrs.animatedClass);
    						indexElm.text(trObj.first().attr('rel'));
    					});

    					
	            	} else if (eFolderJS.vrs.movingAction == "move_folder") {
	            		$('.common_table_list_v30').slideDown('slow');
	            	}
					eFolderJS.vrs.movingAction = "";
					$('.card .card-title').slideUp('slow', function() {
						eFolderJS.cfn.controlTreeOption();
	            	});
					// close all action button
					$('.act_add_folder').fadeIn('slow');
					if (eFolderJS.vrs.selected_folder != '0') {
						$('.act_folder_delete').fadeIn('slow');
						$('.act_edit_folder').fadeIn('slow');
					}
				},
				checkSelectedFiles: function() {
					if (eFolderJS.vrs.debug) console.log("eFolderJS > cfn > checkSelectedFiles");
					eFolderJS.vrs.totalFiles = 0;
					if ($('.efolder_files').length > 0) {
						$('.efolder_files').each(function() {
							if ($(this).prop('checked') == true) {
								eFolderJS.vrs.totalFiles++;
							}
						});
					}
					if (eFolderJS.vrs.debug) console.log(eFolderJS.vrs.totalFiles);
					var node = $("#tree").fancytree("getTree").getSelectedNodes();
			        // eFolderJS.cfn.actionPanelInfo(node);
			        
			        if (eFolderJS.vrs.totalFiles == $('.efolder_files').length) {
						$('.selectAllFiles').hide();
					} else {
						$('.selectAllFiles').show();
					}
			        if (eFolderJS.vrs.totalFiles > 0 && $('.efolder_files').length > 0) {
						$('.unselectAllFiles').show();
					} else {
						$('.unselectAllFiles').hide();
					}
				},
				actionPanelInfo: function(node) {
					if (eFolderJS.vrs.debug) console.log("eFolderJS > cfn > actionPanelInfo");
		            $('.eform_act').hide();

		            if (eFolderJS.vrs.movingAction == "move_folder") {
	            		$('#action_remarks').removeClass('warning').html("<i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["promptMsgForSelectParentFolder"]; ?>").show();
					} else if (eFolderJS.vrs.movingAction == "move_file") {
						$('#action_remarks').removeClass('warning').html("<i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["promptMsgForSelectFolderAndFile"]; ?>").show();
					}
					if (node.length > 0) {
		            	$('#item_title').html('<i class="fa fa-folder" aria-hidden="true"></i> ' + node[0].title);
		            	$('#selectedhdr').show();

		            	if (eFolderJS.vrs.movingAction == "" && (typeof eFolderJS.vrs.selected_folder_ds.length == "undefined" || eFolderJS.vrs.selected_folder_ds.length < eFolderJS.vrs.limitation)) {
		            		$('.act_add_folder').show();
		            	}
	            		if (eFolderJS.vrs.movingAction == "" && eFolderJS.vrs.selected_folder != "0") {
	            			$('.act_folder_delete').show();
	            			$('.act_edit_folder').show();
	            		}
		            	if (node[0].key != eFolderJS.vrs.selected_folder &&
				            	( eFolderJS.vrs.movingAction == "move_folder" && node[0].key != eFolderJS.vrs.selected_folder_parent || 
						            	eFolderJS.vrs.movingAction == "move_file" ) ) {
			            	if (eFolderJS.vrs.selected_folder != "0" && 
					            	node[0].key != eFolderJS.vrs.selected_folder_parent )
		            		{
			            		
			            		var parentIds = (typeof node[0].data.parentids != "undefined") ? node[0].data.parentids : [];
			            		var afterTotals = parentIds.length + parseInt(eFolderJS.vrs.selected_folder_total_child);   
			            		if (afterTotals < eFolderJS.vrs.limitation) {
    			            		var isChildFolder = false;
    			            		if (parentIds.length > 0) {
    			            			$.each(parentIds, function(index, data) {
    				            			if (parseInt(data) === parseInt(eFolderJS.vrs.selected_folder)) {
    				            				isChildFolder = true;
    				            			}
    			            			});
    			            		}
    			            		if (!isChildFolder) {
    			            			if (eFolderJS.vrs.movingAction == "move_folder") {
    			            				$('.act_folder_move').show();
    			            			}
    			            			$('#action_remarks').removeClass('warning').empty();
    			            		} else {
    			            			$('#action_remarks').addClass('warning').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["cannotMoveFolder"]; ?>");
    			            		}
			            		} else {
			            			$('#action_remarks').addClass('warning').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["overLimited"]; ?>");
			            		}
			            	}
			            	if (eFolderJS.vrs.totalFiles > 0) {
			            		$('.act_file_move').show();
			            		$('#action_remarks').removeClass('warning').empty();
			            	} else {
			            		if (eFolderJS.vrs.movingAction == "move_folder") {
			            			$('#action_remarks').removeClass('warning').html("<i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["promptMsgForSelectParentFolder"]; ?>").show();
			            		} else {
			            			$('#action_remarks').removeClass('warning').html("<i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["promptMsgForSelectFolderAndFile"]; ?>").show();
			            		}
			            	}
		            	} else {
		            		if (eFolderJS.vrs.movingAction == "move_folder") {
		            			$('#action_remarks').addClass('warning').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["cannotMoveFolder"]; ?>");
		            		} else if (eFolderJS.vrs.movingAction == "move_file") {
		            			$('#action_remarks').addClass('warning').html("<i class='fa fa-warning'></i> <?php echo $Lang["eForm"]["Folders"]["saveLocation"]; ?>");
		            		}
		            	}
	
			            $('.eform_current_folder').each(function() {
			            	$(this).text(eFolderJS.vrs.selected_folder_title);
			            });
			            
			            $('.eform_folder').each(function() {
    			            $(this).text(node[0].title);
			            });

			            $('.eform_file_totals').each(function() {
			            	$(this).text(eFolderJS.vrs.totalFiles);
			            });
		            } else {
		            	// $('#selectedhdr').hide();
		            }
				},
				init: function() {
					if (eFolderJS.vrs.debug) console.log("eFolderJS > cfn > init");

					$('.common_table_list_v30').eq(0).attr('aria-label', '<?php echo $Lang["eForm"]["Folders"]["tip_select_file"]; ?>');

					$("#tree").fancytree({
        			    activeVisible: true,
        			    aria: true,
        			    autoActivate: false,
        			    autoCollapse: false,
        			    autoScroll: false,
        			    clickFolderMode: 1,
        			    checkbox: false,
        			    disabled: false,
        			    focusOnSelect: false,
        			    escapeTitles: false,
        			    generateIds: false,
        			    idPrefix: "ft_",
        			    icon: true,
        			    keyboard: false,
        			    keyPathSeparator: "/",
        			    minExpandLevel: 2,
        			    quicksearch: false,
        			    rtl: false,
        			    selectMode: 1,
        			    unselectable: false,
        			    tabindex: "0",
        			    titlesTabbable: false,
        			    tooltip: false,
        			    source: $.ajax({
        			        url: eFolderJS.vrs.url + "&type=init&folder=" + eFolderJS.vrs.selected_folder,
        			        dataType: "json",
        			        cache: false
    			      	}),
    			      	lazyLoad: function(event, data){
    			            // Simulate a slow Ajax request
    			            var node = data.node;

			            	$("#tree").fancytree("option", "checkbox", false);
    			            
    			            data.result = {
    	    			            url: eFolderJS.vrs.url + "&type=expand&folder=" + node.key
    			            };
    			            var dfd = new $.Deferred();
    			            // data.result = dfd.promise();
    			            window.setTimeout(function(){  // Simulate a slow Ajax request
    			            	eFolderJS.cfn.controlTreeOption('ignore');
    			            }, 1000);
    			            
    			        },
    			        loadChildren: function(event, data) {
    			        	$('#boxAddNewFolder .boxMsg').empty();
    					},
    					postProcess: function(event, data) {
    						
    					},
    			      	renderColumns: function(event, data) {
    			            var node = data.node,               
    			                $tdList = $(node.tr).find(">td");
    			            //console.log(node);    
    			            $tdList.eq(1).text(node.data.childcount);
    			        },
        			    init: function(event, data, flag) {
        			        var node = $("#tree").fancytree("getTree").getSelectedNodes();
        			        node[0].unselectable = true;
        			        node[0].selected = false;
        			        node[0].statusNodeType = 'error';
							eFolderJS.cfn.actionPanelInfo(node);
        			    },
        			    focusTree: function(event, data) {
        			    	event.preventDefault();
        			    },
        			    activate: function(event, data) {
            			    var node = data.node;
            			    // acces node attributes
            			    var slug = node.data.slug;
            			    if (typeof node.data.slug == "undefined") {
            			    	slug = "main";
            			    }
            			    location.href = eFolderJS.vrs.page_url + "&folder=" + node.key + "&slug=" + slug;
//             			    $("#echoActive").text(node.title);
//             			    console.log("custom node data: ", node);
//             			    if( !$.isEmptyObject(node.data) ){
// //        			          alert("custom node data: " + JSON.stringify(node.data));
// 							}
        			    },
        			    blur: function(event, data) {
            			    // $("#echoFocused").text("-");
        			    },
        			    deactivate: function(event, data) {
        			        // $("#echoActive").text("-");
    			      	},
    			      	beforeSelect: function(event, data){
        			      	
    			      	},
    			      	select: function(event, data) {
    			            var node = data.tree.getSelectedNodes();
    			            eFolderJS.cfn.checkSelectedFiles();
    			            eFolderJS.cfn.actionPanelInfo(node);
    			            eFolderJS.cfn.checkSelectedFiles();
			            }
        			});
					$('.eform_expandAll').bind('click', eFolderJS.ltr.expandAllHandler);
					$('.eform_hiddenAll').bind('click', eFolderJS.ltr.hiddleAllHandler);

					$('input[name="checkmaster"]').bind('click', eFolderJS.ltr.selectedFileHandler);
					$('.btnSubmit').bind('click', eFolderJS.ltr.buttonClickHandler);

					$('.moveFileStart').bind('click', eFolderJS.ltr.moveFileHandler);
					$('.moveFolderStart').bind('click', eFolderJS.ltr.moveFolderHandler);

					$('.selectAllFiles').bind('click', eFolderJS.ltr.selectAllFileHandler)
					$('.unselectAllFiles').bind('click', eFolderJS.ltr.unselectAllFileHandler)
				}
			}
        }

        $(document).ready( function() {	
        	$('input#keyword').keydown( function(evt) {
        		if (Check_Pressed_Enter(evt)) {
        			// pressed enter
        			goSearch();
        		}
        	});	
        	eClassFormJS.cfn.init();
        	eFolderJS.cfn.init();
        });
        
        
        </script>
	</div>
	<div class='eform_folder_area'>
		<div class='folder_holder'>
			<div class='card'>
				<div class='card-body'>
<?php
        $current_folder_slug = isset($_GET["folder"]) ? $_GET["folder_id"] : null;
        echo $indexVar['libeform_ui']->treeMenu($current_folder_slug);
?>
					<div class='info_remark'>
    					<b><i class='fa fa-info-circle'></i> <?php echo $Lang["eForm"]["Folders"]["remarks"]; ?>:</b> 
    					<ul>
    						<li><?php echo $Lang["eForm"]["Folders"]["maxLevelsMsg1"]; ?></li>
    						<li><?php echo $Lang["eForm"]["Folders"]["maxLevelsMsg2"]; ?></li>
    						<li><?php echo $Lang["eForm"]["Folders"]["maxLevelsMsg3"]; ?></li>
    					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class='clearboth'></div>
</div>
<div id='boxAddNewFolder' class='promptbox promptbox-blue'>
	<div class='boxTitle'><i class="fa fa-folder"></i> <?php echo $Lang["eForm"]["Folders"]["addFolder"]; ?></div>
	<div class='boxBody'>
		<div class='row'>
			<div class='label'>
				<?php echo $Lang["eForm"]["Folders"]["parentFolder"]; ?>:
			</div>
			<div class='inputField'>
				<span class="eform_current_folder">:current_folder</span>
			</div>
			<div class='clearboth'></div>
		</div>
		<div class='row'>
			<div class='label'>
				<?php echo $Lang["eForm"]["Folders"]["TitleEN"]; ?>:
			</div>
			<div class='inputField'>
				<input type='text' name='title_en' value='' class='input-field'>
			</div>
			<div class='clearboth'></div>
		</div>
	</div>
	<div class='boxMsg'></div>
	<div class='boxFooter'>
		<div class='set blue'>
			<a href='#' class='btn pri btnSubmit' id='btnSubmitAddFolder'><?php echo $Lang["eForm"]["Folders"]["submit"]; ?></a>
			<a href='#' data-fancybox-close class='btn sec'><?php echo $Lang["eForm"]["Folders"]["cancel"]; ?></a>
		</div>
	</div>
</div>

<div id='boxEditFolder' class='promptbox promptbox-blue'>
	<div class='boxTitle'><i class="fa fa-pencil"></i> <?php echo $Lang["eForm"]["Folders"]["editFolder"]; ?></div>
	<div class='boxBody'>
		<div class='row'>
			<div class='label'>
				<?php echo $Lang["eForm"]["Folders"]["TitleEN"]; ?>:
			</div>
			<div class='inputField'>
				<input type='text' name='edit_title_en' value='<?php echo $folderInfo["FolderTitleEN"]; ?>' class='input-field'>
			</div>
			<div class='clearboth'></div>
		</div>
	</div>
	<div class='boxMsg'></div>
	<div class='boxFooter'>
		<div class='set blue'>
			<a href='#' class='btn pri btnSubmit' id='btnSubmitEditFolder'><?php echo $Lang["eForm"]["Folders"]["submit"]; ?></a>
			<a href='#' data-fancybox-close class='btn sec'><?php echo $Lang["eForm"]["Folders"]["cancel"]; ?></a>
		</div>
	</div>
</div>

<div id='boxMoveFolder' class='promptbox promptbox-blue'>
	<div class='boxTitle'><i class="fa fa-window-restore"></i> <?php echo $Lang["eForm"]["Folders"]["moveFolder"]; ?></div>
	<div class='boxBody'>
		<div class='row'>
			<?php echo $Lang["eForm"]["Folders"]["moveFolderToThisFolderMsg"]; ?>
		</div>
	</div>
	<div class='boxMsg'></div>
	<div class='boxFooter'>
		<div class='set blue'>
			<a href='#' class='btn pri btnSubmit' id='btnSubmitMoveFolder'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a>
			<a href='#' data-fancybox-close class='btn sec'><?php echo $Lang["eForm"]["Folders"]["cancel"]; ?></a>
		</div>
	</div>
</div>

<div id='boxMoveFile' class='promptbox promptbox-blue'>
	<div class='boxTitle'><i class="fa fa-sign-in"></i> <?php echo $Lang["eForm"]["Folders"]["moveFile"]; ?></div>
	<div class='boxBody'>
		<div class='row'>
			<?php echo $Lang["eForm"]["Folders"]["moveFileToThisFolderMsg"]; ?>
		</div>
	</div>
	<div class='boxMsg'></div>
	<div class='boxFooter'>
		<div class='set blue'>
			<a href='#' class='btn pri btnSubmit' id='btnSubmitMoveFile'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a>
			<a href='#' data-fancybox-close class='btn sec'><?php echo $Lang["eForm"]["Folders"]["cancel"]; ?></a>
		</div>
	</div>
</div>

<div id='boxDeleteFolder' class='promptbox promptbox-red'>
	<div class='boxTitle'><i class="fa fa-trash-o"></i> <?php echo $Lang["eForm"]["Folders"]["deleteFolder"]; ?></div>
	<div class='boxBody'>
		<div class="row">
<?php
if ($folderID != 0 && $folderInfo["total_folders"] == 0 && $folderInfo["total_forms"] == 0) {
    # not root and no children
    echo "<div class='text-warning'>" . $Lang["eForm"]["Folders"]["confirmDeleteFolder"] . "</div>";
} else {
    echo "<div class='text-warning'><i class='fa fa-warning'></i> " . $Lang["eForm"]["Folders"]["FolderIsNotEmpty"] . "</div>";
}
?>
		</div>
	</div>
	<div class='boxMsg'></div>
	<div class='boxFooter'>
		<div class='set red'>
			<?php if ($folderID != 0 && $folderInfo["total_folders"] == 0 && $folderInfo["total_forms"] == 0) { ?>
			<a href='#' data-fancybox data-src="#boxAddNewFolder" class='btn pri btnSubmit' id='btnDeleteFolder'><?php echo $Lang["eForm"]["Folders"]["confirm"]; ?></a>
			<?php }?>
			<a href='#' data-fancybox-close class='btn sec'><?php echo $Lang["eForm"]["Folders"]["cancel"]; ?></a>
		</div>
	</div>
</div>
