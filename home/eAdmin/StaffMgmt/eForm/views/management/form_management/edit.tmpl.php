<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css" type="text/css" />
<link rel="stylesheet" href="/templates/slrs/css/font-awesome.min.css" type="text/css" />
<div class='folder_breadcrumb'>
<?php echo $htmlBreadcrumb; ?>
</div>
<?php echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION); ?>
<form id="form1" name="form1" method="post" rel="?task=management/form_management/edit_update">
	<input type='hidden' name='folder' value='<?php echo (isset($folderID) ? $folderID : '0'); ?>'>
	<div class="table_board">
		<div>
			<table class="form_table_v30">
			<tr valign='top'>
				<td nowrap class='field_title'><span class='tabletextrequire'>*</span> <?php echo $Lang["eForm"]["TargetUser"]; ?></td>
				<td>
					<?php echo $type_selection; ?><br><span id="div_audience_err_msg"></span>
					<div id="div_audience"></div>
				</td>
			</tr>
			
			</table>
			<div id="div_content">
				<?php echo $htmlAry["contentBody"]; ?>
			</div>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<div id='eForm_errorMsg'></div>
			<p class="spacer"></p>
			<?php echo $htmlAry['submitBtn']; ?>
			<?php echo $htmlAry['backBtn']; ?>
			<p class="spacer"></p>
		</div>
	</div>
</form>
<link rel="stylesheet" href="/templates/eForm/assets/css/custom.css" type="text/css" />
<?php
$fixOpt = $indexVar["FormTemplate"]->jsonToStr($templateOptions["FixedTable"]);
$dynOpt = $indexVar["FormTemplate"]->jsonToStr($templateOptions["DynamicTable"]);
if (empty($fixOpt)) $fixOpt = "{}";
if (empty($dynOpt)) $dynOpt= "{}";
?>
<script type="text/javascript">
var initFixedTableIDs = <?php echo $fixOpt; ?>;
var initDynamicTableIDs = <?php echo $dynOpt; ?>;
</script>
<script type="text/javascript">

	var backlink = "?task=management/form_management/list&folder=<?php echo $folderID; ?>";

	function selectAudience()
	{
		var tid = document.form1.type.value;
		document.getElementById('div_audience_err_msg').innerHTML = "";
		$('#div_audience').load(
			'ajax_loadAudience.php', 
			{type: tid, RecipientID: '', FormID: '<?php echo ($eform_action != 'copy') ? $FormID : ''; ?>'}, 
			function (data){
				if(tid == 4){
					// initialize jQuery Auto Complete plugin
					Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');
					$('input#UserClassNameClassNumberSearchTb').focus();
				}
			});
	}

	function goBack() {
		window.location.href = backlink;
	}

	function getTableID() {
		var tableIDs = [];
		tableIDs["fixedTable"] = initFixedTableIDs;
		tableIDs["dynamicTable"] = initDynamicTableIDs;
		return tableIDs; 
	}
	function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
		AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
			"ajax_search_user.php",
			{  			
				onItemSelect: function(li) {
					Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);
		Update_Auto_Complete_Extra_Para();
	}

	function Add_Selected_User(UserID, UserName, InputID) {
		var UserID = UserID || "";
		var UserName = UserName || "";
		var UserSelected = document.getElementById('target[]');

		UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
		
		Update_Auto_Complete_Extra_Para();
		
		// reset and refocus the textbox
		$('input#' + InputID).val('').focus();
	}

	function Update_Auto_Complete_Extra_Para(){
		checkOptionAll(document.getElementById('form1').elements["target[]"]);
		ExtraPara = new Array();
		ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
		AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
	}

	function js_Remove_Selected(){
		checkOptionRemove(document.getElementById('target[]'));
		Update_Auto_Complete_Extra_Para();
	}

	function goSubmit() {
		$(eformJS.vrs.container).trigger('submit');
	}

	function updateFromThickbox(templateData) {
		if (templateData == "[]") {
			formJSONinfo = null;
			$('#FormSchemeJSON').val('');
		} else {
			formJSONinfo = JSON.parse(templateData);
			$('#FormSchemeJSON').val(templateData);
		}
		
		tb_remove();
	}
	
	$(document).ready( function() {
		selectAudience();
		eformJS.cfn.init();
	});
	var formJSONinfo = null;
	var eformJS = {
		vrs: {
			container: "#form1",
			xhr: "",
			formBlrContainer: ".thickboxBTN",
			jsonData: {}
		},
		ltr: {
			formBlrClickHandler: function(e) {
				e.preventDefault();
				var url = $(this).attr('rel');
				var extraparams = eformJS.vrs.jsonData;
				eformJS.cfn.openFormBuilder(url, extraparams);
			},
			submitHandler: function(e) {
				e.preventDefault();
				var allowSubmitForm = true;
				/************************************************************************/
				/* valid Form input
				/************************************************************************/
				var typeval = $(eformJS.vrs.container).find('select[name="type"]').val();
				if (typeof typeval != "undefined" && typeval != "") {
					if ($(eformJS.vrs.container).find('select[name="type"]').val() == "4") {
						if ($(".select_target_user").eq(0).find('option:select').length == 0) {
							allowSubmitForm = false;
							$('#div_audience_err_msg').css({"color":"red"}).html("<?php echo $Lang["eForm"]["TargetUser_err"]; ?>").show();
						} else {
							$('#div_audience_err_msg').empty().hide();
						}
					} else {
						$('#div_audience_err_msg').empty().hide();
					}
				} else {
					allowSubmitForm = false;
					$('#div_audience_err_msg').css({"color":"red"}).html("<?php echo $Lang["eForm"]["TargetUserChoose_err"]; ?>").show();
				}
				var errLangtxt = [];
<?php
if (count($Lang["eForm"]["FormErr"]) > 0) {
	foreach ($Lang["eForm"]["FormErr"] as $kk => $vv) {
		echo 'errLangtxt["' . $kk . '"] = "' . $vv . '";' . "\n";
	}
}
?>
				if ($(eformJS.vrs.container).find(".required input[type='text']").length > 0) {
					$(eformJS.vrs.container).find(".required input[type='text']").each(function() {
						var inputVal = $.trim($(this).val());
						var objName = $(this).attr('name');
						var errorObj = $(this).parent().find('#div_' + objName + '_err_msg');
						if (typeof errorObj != "undefined") {
							if (inputVal == "") {
								if (typeof errorObj != "undefined") {
									allowSubmitForm = false;
									errorObj.css({"color":"red"}).html(errLangtxt[objName]).show();
								}
							} else {
								if (typeof errorObj != "undefined") {
									errorObj.empty().hide();
								}
							}
						}
					});
				}
				var currDate = <?php echo date("Ymd"); ?>;  
				
				var StartDate = $('input[name="StartDate"]').eq(0).val();
				var DueDate = $('input[name="DueDate"]').eq(0).val();
				var intStartDate = "";
				var intDueDate = "";
				if (typeof StartDate != "undefined" && StartDate != "") {
					intStartDate = parseInt(StartDate.replace(/\-/g, ''));
					if ((isNaN(intStartDate) || intStartDate < currDate) && $('input[name="StartDate"]').eq(0).attr('type') != "hidden") {
						$('#DPWL-StartDate').html("<?php echo $Lang["eForm"]["jsMsg"]["ChoosePastDate"]; ?>");
						allowSubmitForm = false;
					} else {
						$('#DPWL-StartDate').empty();
					}
				} else {
					allowSubmitForm = false;
				}
				if (typeof DueDate != "undefined" && DueDate != "") {
					intDueDate = parseInt(DueDate.replace(/\-/g, ''));
					if ((isNaN(intDueDate) || intDueDate < currDate) && $('input[name="DueDate"]').eq(0).attr('type') != "hidden") {
						allowSubmitForm = false;
						$('#DPWL-DueDate').html("<?php echo $Lang["eForm"]["jsMsg"]["ChoosePastDate"]; ?>");
					} else {
						$('#DPWL-DueDate').empty();
					}
				} else {
					allowSubmitForm = false;
				}

				if (formJSONinfo != null || parseInt($('input[name="TemplateID"]').eq(0).val()) > 0) {
					$('#div_FormTemplateJSON_err_msg').empty().hide();
				} else {
					allowSubmitForm = false;
					$('#div_FormTemplateJSON_err_msg').css({"color":"red"}).html("<?php echo $Lang["eForm"]["FormErr"]["FormScheme"]; ?>").show();
				}
				/************************************************************************/
				var targetType = $('select[name="type"]').eq(0).val();
				if (targetType == "4") {
					$(".select_target_user").eq(0).find("option").attr('selected', true);
				}
				if (allowSubmitForm) {
					$("#submitBtn").attr('disable', 'disable');
					$("#backBtn").attr('disable', 'disable');
					$(eformJS.vrs.container).unbind('submit', eformJS.ltr.submitHandler);
					var URL = $(eformJS.vrs.container).attr('rel');
					var xhr = eformJS.vrs.xhr;
					if (xhr != "") {
						xhr.abort();
					}
					$('#eForm_errorMsg').empty();
					$.ajax({
						url: URL,
						data: $(eformJS.vrs.container).serializeArray(),
						timeout: 3000,
						cache: false,
						type: "post",
						success: function(respdata, status, jqXHR) {
							var resultTxt = respdata.split("||");
							if (resultTxt[0] == "SUCCESS") {
								$("#submitBtn").hide();
								// alert("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateSuccess"]; ?>");
								$('#eForm_errorMsg').html("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateSuccess"]; ?>");
								if (typeof resultTxt[1] != "undefined") {
									backlink = resultTxt[1];
								}
								// $(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
							} else {
								$("#submitBtn").removeAttr('disable');
								$(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
								$('#eForm_errorMsg').html("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"]; ?>");
							}
							// $("#backBtn").show();
							$("#backBtn").removeAttr('disable');
						},
						error: function(jqXHR, status, err) {
							// $("#submitBtn").show();
							// $("#backBtn").show();
							$("#submitBtn").removeAttr('disable');
							$("#backBtn").removeAttr('disable');
							$('#eForm_errorMsg').html("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"]; ?>");
							// alert("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"]; ?>");
							$(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);							
						}
					});
				}
			}
		},
		cfn: {
			init: function() {
				
				if ($(eformJS.vrs.formBlrContainer).length > 0) {
					$(eformJS.vrs.formBlrContainer).bind('click', eformJS.ltr.formBlrClickHandler);
				}
				if ($(eformJS.vrs.container).length > 0) {
					$(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
				}
				var temp = $('#initJSON').text();
				if (typeof temp != 'undefined' && temp != "") {
					formJSONinfo = JSON.parse(temp);
				}
				if ($('input[name="TemplateID"]').eq(0).val() == "") {
					// $("#submitBtn").hide();
				}
				$('#FormSchemeJSON').val($('#initJSON').text());
			},
			openFormBuilder: function(url, extraparams) {
				var isUse = 1;
				var defaultHeight = "";
				var defaultWidth = "";
				var tbType = 1;
				var callBackFunc = "updateFromThickbox";
				var inlineID = "";
				show_dyn_thickbox_ip("<?php echo $Lang["FormBuilder"]["Title"]; ?>", url, extraparams, isUse, defaultHeight, defaultWidth, tbType);
			}
		}
	};
</script>