<?php echo $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION); ?>
<form id="form1" name="form1" method="post" rel="?task=settings/table_template/edit_update">
	<div class="table_board">
		<div>
			<div id="div_content">
				<?php echo $htmlAry["contentBody"]; ?>
			</div>
		</div>
		<div class="edit_bottom_v30">
			<div id='eForm_errorMsg'></div>
			<p class="spacer"></p>
<?php if (!$templateInfo["isUsed"]) { ?>
			<?php echo $htmlAry['submitBtn']; ?>
<?php } ?>
			<?php echo $htmlAry['backBtn']; ?>
			<p class="spacer"></p>
		</div>

	</div>
</form>
<script src="/templates/eForm/assets/libs/jquery.3.1.1.js"></script>
<script src="/templates/eForm/assets/libs/jquery-migrate-1.4.1.js"></script>
<script src="/templates/eForm/assets/js/table/jquery.jexcel.js?v=2"></script>
<script src="/templates/eForm/assets/js/table/jquery.jcalendar.js"></script>
<script src="/templates/eForm/assets/libs/jquery.mask.js"></script>
<link rel="stylesheet" href="/templates/eForm/assets/css/table/jquery.jexcel.css" type="text/css" />
<link rel="stylesheet" href="/templates/eForm/assets/css/table/jquery.jcalendar.css" type="text/css" />
<link rel="stylesheet" href="/templates/eForm/assets/css/table/custom.css" type="text/css" />
<script type="text/javascript">
	function goBack() {
		window.location.href = "?task=settings/table_template/list";
	}

	function goSubmit() {
		$(eformJS.vrs.container).trigger('submit');
	}

	function updateFromThickbox(templateData) {
		if (templateData == "[]") {
			formJSONinfo = null;
			$('#FormSchemeJSON').val('');
		} else {
			formJSONinfo = JSON.parse(templateData);
			$('#FormSchemeJSON').val(templateData);
			eformJS.cfn.buildTable(formJSONinfo, true);
			$('#submitBtn').show();
		}
		tb_remove();
	}
	
	$(document).ready( function() {
		eformJS.cfn.init();
	});
	var formJSONinfo = null;
	var fixedTableData = {};
	var eformJS = {
		vrs: {
			container: "#form1",
			xhr: "",
			formBlrContainer: ".thickboxBTN",
			jsonData: {}
		},
		ltr: {
			formBlrClickHandler: function(e) {
				e.preventDefault();
				var url = $(this).attr('rel');
				var extraparams = eformJS.vrs.jsonData;
				eformJS.cfn.openFormBuilder(url, extraparams);
			},
			submitHandler: function(e) {
				e.preventDefault();
				var allowSubmitForm = true;

				/************************************************************************/
				/* valid Form input
				/************************************************************************/
				var errLangtxt = [];
<?php
if (count($Lang["eForm"]["FormErr"]) > 0) {
	foreach ($Lang["eForm"]["FormErr"] as $kk => $vv) {
		echo 'errLangtxt["' . $kk . '"] = "' . $vv . '";' . "\n";
	}
}
?>
				if ($(eformJS.vrs.container).find(".required input[type='text']").length > 0) {
					$(eformJS.vrs.container).find(".required input[type='text']").each(function() {
						var inputVal = $.trim($(this).val());
						var objName = $(this).attr('name');
						var errorObj = $(this).parent().find('#div_' + objName + '_err_msg');
						if (typeof errorObj != "undefined") {
							if (inputVal == "") {
								if (typeof errorObj != "undefined") {
									allowSubmitForm = false;
									errorObj.css({"color":"red"}).html(errLangtxt[objName]).show();
								}
							} else {
								if (typeof errorObj != "undefined") {
									errorObj.empty().hide();
								}
							}
						}
					});
				}
				if (formJSONinfo != null || parseInt($('input[name="TemplateID"]').eq(0).val()) > 0) {
					$('#div_TemplateJSON_err_msg').empty().hide();
				} else {
					allowSubmitForm = false;
					$('#div_TemplateJSON_err_msg').css({"color":"red"}).html("<?php echo $Lang["eForm"]["FormErr"]["TableScheme"]; ?>").show();
				}
				/************************************************************************/
				if (allowSubmitForm) {
					$("#submitBtn").hide();
					$("#backBtn").hide();
					$(eformJS.vrs.container).unbind('submit', eformJS.ltr.submitHandler);
					var URL = $(eformJS.vrs.container).attr('rel');
					var xhr = eformJS.vrs.xhr;
					if (xhr != "") {
						xhr.abort();
					}
					$("#SettingsJSON").val(JSON.stringify(fixedTableData));
					$("#SchemeJSON").val(JSON.stringify(fixedTableData));
					$.ajax({
						url: URL,
						data: $(eformJS.vrs.container).serializeArray(),
						timeout: 3000,
						cache: false,
						type: "post",
						success: function(respdata, status, jqXHR) {
							if (respdata == "SUCCESS") {
								// $("#submitBtn").show();
								// $(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
								// alert("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateSuccess"]; ?>");
								$('#eForm_errorMsg').html("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateSuccess"]; ?>");
							} else {
								$("#submitBtn").show();
								$(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
								// alert("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"]; ?>");
								$('#eForm_errorMsg').html("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"]; ?>");
							}
							$("#backBtn").show();
						},
						error: function(jqXHR, status, err) {
							$("#submitBtn").show();
							$("#backBtn").show();
							alert("Failed.");
							$(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
							alert("<?php echo $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"]; ?>");
						}
					});
				}
			},
			typeChangeHandler: function(e) {
				if ($('select[name="TemplateType"]').val() == 'FixedTable') {
					$('#fixTbl').show();
					$('#dynTblMin').hide();
					$('#dynTblMax').hide();
				} else {
					$('#fixTbl').hide();
					$('#dynTblMin').show();
					$('#dynTblMax').show();
				}
				eformJS.cfn.buildTable(formJSONinfo, false);
			},
			noOfRowsChangeHandler: function(e) {
				if (parseInt($(this).val()) <= 0) {
					$(this).val('1');
				}
				if ($(this).val() > 100) {
					$(this).val('100');
				}
				eformJS.cfn.buildTable(formJSONinfo, false);
			}
		},
		cfn: {
			buildTable: function(formJSON, isSchemeTrigger) {
				if (formJSON != null) { 
					var a_colHeaders = [];
					var a_tbdata = [];
					var a_colWidths = [];
					var a_columns = [];
					var $i = 0;
					var $j = parseInt($("input[name='settings_noofrows']").eq(0).val()) - 1;
					if (typeof $j == "undefined") {
						$j = 0;
					} else if ($j < 1) {
						$("input[name='settings_noofrows']").eq(0).val('1');
						$j = 0;
					}
					if ($("select[name='TemplateType']").val() != "FixedTable") {
						var max = parseInt($("input[name='settings_maxrows']").eq(0).val());
						if (max < $j + 1 && max > 0) {
							$j = max - 1;
							$("input[name='settings_noofrows']").eq(0).val(max);
						}
					}
					
					if (isSchemeTrigger || fixedTableData == "") {
						$.each(formJSON, function(index, data) {
							a_colHeaders[$i] = data.label;
							a_colWidths[$i] = 150;
							if ($('select[name="TemplateType"]').val() == "FixedTable") {
								switch (data.type) {
									case "dateFieldElm":
										a_columns[$i] = { type: 'calendar', options: { format:'YYYY/MM/DD', readonly:0, clear:1 } };
										break;
									case "datetimeFieldElm":
										a_columns[$i] = { type: 'calendar', options: { format:'YYYY/MM/DD HH24:MI', time:1 } };
										break;
									case "timeFieldElm":
										a_columns[$i] = { type: 'text', mask:'00:00', options:{ reverse: true } };
										break;
									case "number":
										a_columns[$i] = { type: "text" };
										break;
									case "radio-group":
									case "select":
									case "checkbox-group":
										var checkboxGroupArr = [];
										var ch = 0;
										$.each(data.values, function(ind, val) {
											checkboxGroupArr[ch] = { 'id': val.value, 'name': val.label  };
											ch++;
										});
										a_columns[$i] = { type: "dropdown", source: checkboxGroupArr };
										break;
									case "studentElm":
									case "teacherElm":
									case "subjectGroupElm":
										a_columns[$i] = { type: "text", readOnly:true };
										break;
									default:
										a_columns[$i] = { type: "text" };
										break; 
								}
							} else {
								a_columns[$i] = { type: "text", readOnly:true };
							}
							$i++;
						});
						fixedTableData.colHeaders = a_colHeaders;
						fixedTableData.colWidths = a_colWidths;
						fixedTableData.columns = a_columns;
						
					} else {
						a_colHeaders = fixedTableData.colHeaders;
						a_colWidths = fixedTableData.colWidths;
						a_columns = fixedTableData.columns;
					}
					for(var $x=0; $x <= $j; $x++) {
						$z = 0;
						a_tbdata[$x] = [];
						if ($("select[name='TemplateType']").val() == "FixedTable") {
							$.each(formJSON, function(index, data) {
								try {
									if (typeof fixedTableData.tddata[$x] != "undefined" && typeof fixedTableData.tddata[$x][$z] != "undefined") {
										a_tbdata[$x][$z] = fixedTableData.tddata[$x][$z];
									} else {
										a_tbdata[$x][$z] = "";
									}
								} catch(err) {
									a_tbdata[$x][$z] = "";
								}
								$z++;
							});
						}
					}
					fixedTableData.tddata = a_tbdata;
					
					var editable = 1;
					if ($("select[name='TemplateType']").val() != "FixedTable") {
						editable = 0;
						$('#tablesetting').addClass('isdyn');
						$('.fixedtable_msg').hide();
					} else {
						$('#tablesetting').removeClass('isdyn');
						$('.fixedtable_msg').show();
						if (typeof $('#submitBtn') == "undefined" || $('#submitBtn').length == 0) {
							editable = 0;
						}
					}
					$('#tablesetting').empty();
					$('#tablesetting').jexcel({
						data:a_tbdata,
						editable: editable,
						csvHeaders: true,
						colWidths: a_colWidths,
						colHeaders: a_colHeaders,
						columns: a_columns
					});
					if ($('#editlocked').length > 0) {
						$('.jexcel_arrow').hide();
					}
					$('#tablesetting').show();
				}
			},
			init: function() {
				if (typeof console.clear == "function") {
					console.clear();
				}
				if ($(eformJS.vrs.formBlrContainer).length > 0) {
					$(eformJS.vrs.formBlrContainer).bind('click', eformJS.ltr.formBlrClickHandler);
				}
				if ($(eformJS.vrs.container).length > 0) {
					$(eformJS.vrs.container).bind('submit', eformJS.ltr.submitHandler);
				}
				var temp = $('#initJSON').text();
				if (typeof temp != 'undefined' && temp != "") {
					formJSONinfo = JSON.parse(temp);
					var temp = $('#initTableJSON').html();
					if (typeof temp != 'undefined' && temp != "") {
						fixedTableData = JSON.parse(temp);
						eformJS.cfn.buildTable(formJSONinfo, false);
					}
				}
<?php if ($eform_action != 'copy') { ?>
				if ($('input[name="TemplateID"]').eq(0).val() == "") {
					$("#submitBtn").hide();
				}
<?php } ?>
				if ($('select[name="TemplateType"]').val() == 'FixedTable') {
					$('#fixTbl').show();
				} else {
					$('#dynTblMin').show();
					$('#dynTblMax').show();
				}
				$('select[name="TemplateType"]').bind('change', eformJS.ltr.typeChangeHandler);
				$('input[name="settings_maxrows"]').bind('keyup', eformJS.ltr.noOfRowsChangeHandler);
				$('input[name="settings_noofrows"]').bind('keyup', eformJS.ltr.noOfRowsChangeHandler);
				$('#FormSchemeJSON').val($('#initJSON').text());
			},
			openFormBuilder: function(url, extraparams) {
				var isUse = 1;
				var defaultHeight = "";
				var defaultWidth = "";
				var tbType = 1;
				var callBackFunc = "updateFromThickbox";
				var inlineID = "";
				show_dyn_thickbox_ip("<?php echo $Lang["FormBuilder"]["Title"]; ?>", url, extraparams, isUse, defaultHeight, defaultWidth, tbType);
			}
		}
	};
</script>