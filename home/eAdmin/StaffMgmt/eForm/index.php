<?php
// using : 
/*
 * Change Log:
 * 2018-03-12 Frankie Add Folder Structure to eForm
 * 2017-02-03 Omas Add $junior_mck for ej 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
if (file_exists($PATH_WRT_ROOT."lang/eForm/eform.lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT."lang/eForm/eform.lang.$intranet_session_language.php");

if (!$plugin['eForm']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/eForm/eformConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/eForm/libeform.php");
include_once($PATH_WRT_ROOT."includes/eForm/libeform_ui.php");

if (!class_exists("FormCommon")) include_once($PATH_WRT_ROOT."includes/eForm/class.formcommon.php");
if (!class_exists("FormInfo")) include_once($PATH_WRT_ROOT."includes/eForm/class.forminfo.php");
if (!class_exists("FormGrid")) include_once($PATH_WRT_ROOT."includes/eForm/class.formgrid.php");
if (!class_exists("FormTemplate")) include_once($PATH_WRT_ROOT."includes/eForm/class.formtemplate.php");

$home_header_no_EmulateIE7 = true;

if(isset($junior_mck)){
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.utf8.php");
	include_once($PATH_WRT_ROOT."includes/libschooltimetable.php");
	$g_encoding_unicode = true; // Change encoding from Big5 to UTF-8
	$module_custom['isUTF8'] = true;
}

$indexVar['libinterface'] = new interface_html();
$indexVar['libdb'] = new libdb();
$indexVar['libeform'] = new libeform();
$indexVar['libeform_ui'] = new libeform_ui();
$indexVar['curUserId'] = $_SESSION['UserID'];

$ModuleTitle = "eForm";
$indexVar['FormInfo'] = new FormInfo($ModuleTitle);
$indexVar['FormGrid'] = new FormGrid($ModuleTitle);
$indexVar['FormTemplate'] = new FormTemplate($ModuleTitle);

$indexVar['libeform_ui']->setElmTypeArr($indexVar['FormInfo']->getElmTypeArr());

intranet_auth();
intranet_opendb();
$indexVar['libeform']->checkAccessRight(true);


### handle all http post/get value by urldeocde, stripslashes, trim
array_walk_recursive($_POST, 'handleFormPost');
array_walk_recursive($_GET, 'handleFormPost'); 

### get system message
$returnMsgKey = $_GET['returnMsgKey'];
$indexVar['returnMsgLang'] = $Lang['General']['ReturnMessage'][$returnMsgKey];


### include corresponding php files
$indexVar['task'] = $_POST['task']? $_POST['task'] : $_GET['task'];


// load normal pages
if ($indexVar['task']=='') {
	// go to module index page if not defined
	$indexVar['task'] = 'management'.$eFormConfig['taskSeparator'].'form_management'.$eFormConfig['taskSeparator'].'list';
}

$indexVar['controllerScript'] = 'controllers/'.str_replace($eformConfig['taskSeparator'], '/', $indexVar['task']).'.php';
if ($plugin['eForm'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eForm"]) {
	if (file_exists($indexVar['controllerScript'])){
		include_once($indexVar['controllerScript']);
	
		$indexVar['viewScript'] = 'views/'.str_replace($eFormConfig['taskSeparator'], '/', $indexVar['task']).'.tmpl.php';
		if (file_exists($indexVar['viewScript'])){
			// normal script => with template script
			include_once($indexVar['viewScript']);
			if( strpos($indexVar['task'], 'print') !== false || strpos($indexVar['task'], 'thickbox') !== false || strpos($indexVar['task'], 'panel') !== false) {
				// print page and thickbox no footer
			}
			else {
				$indexVar['libeform_ui']->echoModuleLayoutStop();
			}
		}
		else {
			// update or ajax script => without template script
		}
	}
	else {
		No_Access_Right_Pop_Up();
	}
} else {
	No_Access_Right_Pop_Up();
}

// initial SLRS module default setting
$indexVar['libeform']->setDefaultSettings();

intranet_closedb();
?>