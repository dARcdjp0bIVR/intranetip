<?
# using: 

################################
# 2017-04-27 (Carlos): $sys_custom['DHL'] Use thickbox for choosing target for DHL.
################################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/eForm/eform.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eForm/libeform.php");

if (!class_exists("FormCommon")) include_once($PATH_WRT_ROOT."includes/eForm/class.formcommon.php");
if (!class_exists("FormInfo")) include_once($PATH_WRT_ROOT."includes/eForm/class.forminfo.php");

$libFormInfo = new FormInfo();

intranet_auth();
intranet_opendb();

$lclass = new libclass();

$nextSelect = "";

if ($type==1)       # All Staff
{
}
else if ($type==2)	# All teaching staff
{
}
else if ($type==3)	# All Non-teaching staff
{
}
else if ($type==4)	## Individual Recipients / Groups 
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	
	$linterface 	= new interface_html();
	$lu = new libuser();
			
	$option = "";
	
	$TargetUsers= $libFormInfo->getTargetUsers($FormID);
	
	if(count($TargetUsers) > 0)
	{
		$staff_ary = array_keys($TargetUsers);
		foreach ($TargetUsers as $targetUserID => $userInfo) {
			$name = Get_Lang_Selection($userInfo["ChineseName"], $userInfo["EnglishName"]);
			if (empty($name)) $name = $userInfo["EnglishName"];
			if (empty($name)) $name = $userInfo["UserLogin"];
			$target_name_ary[] = array( $targetUserID, $name );
		}
	}
	### Choose Member Btn
	$choose_btn_js = "newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=1&excluded_type=4&DisplayGroupCategory=1',16)";
	if($sys_custom['DHL']){
		$choose_btn_js = "show_dyn_thickbox_ip('','/home/common_choose/index.php?fieldname=target[]&from_thickbox=1&page_title=SelectAudience&permitted_type=1&DisplayGroupCategory=1', '', 0, 640, 640, 1, 'fakeDiv', null);";
	}
	$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", $choose_btn_js);
	
	### Auto-complete ClassName ClassNumber StudentName search
	$UserClassNameClassNumberInput = '';
	$UserClassNameClassNumberInput .= '<div style="float:left;">';
	$UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
	$UserClassNameClassNumberInput .= '</div>';
	
	### Auto-complete login search
	$UserLoginInput = '';
	$UserLoginInput .= '<div style="float:left;">';
	$UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
	$UserLoginInput .= '</div>';
	
	### Student Selection Box & Remove all Btn
	$MemberSelectionBox = $linterface->GET_SELECTION_BOX($target_name_ary, "name='target[]' id='target[]' class='select_target_user' size='15' multiple='multiple'", "");
	$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "js_Remove_Selected();");
	
	$nextSelect .= "<table>";
	$nextSelect .= '<tr>
									<td class="tablerow2" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td class="tabletext">'.$Lang['eForm']['FromGroup'].'</td>
										</tr>
										<tr>
											<td class="tabletext">'.$btn_ChooseMember.'</td>
										</tr>
										<tr>
											<td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												'.$Lang['eForm']['SearchStaffName'].'
												<br />
												'.$UserClassNameClassNumberInput.'
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td align="left" valign="top">
										<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td align="left">
												'. $MemberSelectionBox .'
												'.$btn_RemoveSelected.'
											</td>
										</tr>
										<tr>
											<td>
												'.$linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eForm']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
											</td>
										</tr>
										</table>
									</td>
								</tr>';
	$nexSelect .= "</table>";
	
// 	$nextSelect .= "<table border='0' cellspacing='1' cellpadding='1' class='inside_form_table'>";
// 	$nextSelect .= "<tr>";
// 	$nextSelect .= "<td>";
	
// 	$nextSelect .= "<select name='target[]' size='4' multiple>$option</select>";
// 	$nextSelect .= "</td>";
// 	$nextSelect .= "<td>";
// 	$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_choose, "button","newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=1&DisplayGroupCategory=1',16)") . "<br>";
// 	$nextSelect .= $linterface->GET_BTN($i_frontpage_campusmail_remove, "button","checkOptionRemove(document.form1.elements['target[]'])") . "<br>";
// 	$nextSelect .= "</td>";
// 	$nextSelect .= "</tr>";
// 	$nextSelect .= "</table>";
}

intranet_closedb();

echo $nextSelect;
	
?>