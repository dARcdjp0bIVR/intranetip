<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
/*
$arrCookies = array();
$arrCookies[] = array("eform_form_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
*/
# Page Title
$TAGS_OBJ[] = array($Lang['eForm']['FormManagement']);
$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libinterface']->Include_AutoComplete_JS_CSS();
echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();
echo $indexVar['libinterface']->Include_JS_CSS();

$eform_action = "new";
$formData = array();
if (isset($_POST["groupIdAry"][0]) && $_POST["groupIdAry"][0] > 0) {
	$TemplateID = $_POST["groupIdAry"][0];
} else {
	if (isset($_GET['cid']))
	{
		$thisCTK =  $indexVar["FormTemplate"]->getToken($_GET["cid"] . "-copy");
		if (isset($_GET["cid"]) && !empty($_GET["cid"]) && ($_GET["ctk"] == $thisCTK)) {
			$TemplateID = $_GET["cid"];
		}
		$eform_action= "copy";
	}
	else 
	{
		$eform_action = "edit";
		$thisCTK =  $indexVar["FormTemplate"]->getToken($_GET["tplid"]);
		if (isset($_GET["tplid"]) && !empty($_GET["tplid"]) && ($_GET["ctk"] == $thisCTK)) {
			$TemplateID = $_GET["tplid"];
		}
	}
}
$templateInfo = array( 'isUsed' => false );
if (isset($TemplateID)) {
	$templateInfo = $indexVar["FormTemplate"]->getFormTemplateInfoById($TemplateID);
	if (is_string($templateInfo["TemplateSettingsJSON"])) {
		$templateInfo["TemplateSettingsJSON"] = $indexVar["FormTemplate"]->strToJSON($templateInfo["TemplateSettingsJSON"]);
	}
	if ($eform_action == 'copy')
	{
		$PAGE_NAVIGATION[] = array($Lang["eForm"]["CopyTableTemplate"]);
	}
	else
	{
		$PAGE_NAVIGATION[] = array($Lang["eForm"]["EditTableTemplate"]);
	}
} else {
	$PAGE_NAVIGATION[] = array($Lang["eForm"]["NewTableTemplate"]);
}

$htmlAry["contentBody"] = $indexVar['libeform_ui']->return_FormTemplateContent($templateInfo, $isView=false, $eform_action);
// ============================== Define Button ==============================

$htmlAry['submitBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
?>