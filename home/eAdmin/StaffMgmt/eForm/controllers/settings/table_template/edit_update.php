<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$thisToken=  $indexVar["FormTemplate"]->getToken($_POST["TemplateID"]);
if ($thisToken == $_POST["token"]) {
	$templateResult = $indexVar['FormTemplate']->editFormTemplate("", $_POST, $_POST["TemplateType"]);
	if ($templateResult["result"]) {
		echo "SUCCESS";
		exit;
	}
}
echo "FAIL";