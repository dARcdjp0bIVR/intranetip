<?php 
$groupID = $_POST["groupIdAry"];;
$isUsedTemplate = $indexVar["FormTemplate"]->getIsUsedTableTemplateID($groupID);
$saveSuccess = true;
$result = array();
if (count($isUsedTemplate) > 0) {
	$saveSuccess = false;
} else {
	$result = $indexVar["FormTemplate"]->removeTableTemplate($groupID);;
	if (count($result) > 0) {
		$saveSuccess = true;
	} else {
		$saveSuccess = false;
	}
}
$returnMsgKey = ($saveSuccess)? $Lang["eForm"]["ReturnMessage"]["UpdateSuccess"] : $Lang["eForm"]["ReturnMessage"]["UpdateUnsuccess"];
header('Location: ?task=settings/'.$eFormConfig['taskSeparator'].'table_template'.$eFormConfig['taskSeparator'].'list#'.$returnMsgKey);
exit;
?>