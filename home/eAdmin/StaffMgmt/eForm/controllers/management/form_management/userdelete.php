<?php 
$TargetIDs = $_POST["groupIdAry"];
$thisToken = $indexVar['FormGrid']->getToken($_GET["fid"]);
if (!empty($_GET["fid"]) && $thisToken != $_GET["ctk"]) {
	No_Access_Right_Pop_Up();
}
$indexVar["FormInfo"]->removeTargetUser($_GET["fid"], $TargetIDs);
$indexVar["FormGrid"]->removeUserData($_GET["fid"], $TargetIDs);
$saveSuccess = true;
$returnMsgKey = ($saveSuccess)? "UpdateSuccess" : "UpdateUnsuccess";
header('Location: ?task=management'.$eFormConfig['taskSeparator'].'form_management'.$eFormConfig['taskSeparator'].'recordlist&fid='.$_GET["fid"].'&ctk='.$_GET["ctk"].'#'.$returnMsgKey);
exit;
?>