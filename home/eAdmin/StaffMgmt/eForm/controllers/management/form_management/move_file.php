<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/json.php");
$json = new JSON_obj();
$result = $indexVar['libeform']->moveItemToNode($_POST);
echo $json->encode($result);
?>