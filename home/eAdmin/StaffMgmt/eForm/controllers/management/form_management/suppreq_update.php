<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
# Page Title
$formData = array();
// if (isset($_POST["groupIdAry"][0]) && $_POST["groupIdAry"][0] > 0) {
$thisCTK =  $indexVar["FormInfo"]->getToken($_POST["fid"] . "_" . $_POST["gid"]);
if (!empty($_POST["fid"]) && !empty($_POST["gid"]) && ($_POST["ctk"] == $thisCTK)) { 
	// $FormID = $_POST["groupIdAry"][0];
	$FormID= $_POST["fid"];
	$formInfo = $indexVar["FormInfo"]->getFormInfoById($FormID);
	
	$res = array( "result" => "FAILED" );
	
	if (isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
		$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], $getFullDetail = true);
		$formInfo["GridInfo"] = $indexVar["FormGrid"]->getGridInfoByID($_POST["gid"], $formInfo);
		if (count($formInfo["GridInfo"]) > 0) {
			$SuppRemarks = $indexVar["FormGrid"]->retrivDBValue($_POST["SuppRemarks"]);
			$requestData = array(
				"GridID" => $formInfo["GridInfo"]["GridID"],
				"FormID" => $FormID,
				"UserID" => $formInfo["GridInfo"]["UserID"],
				"SuppStatus" => "unread",
				"SuppMsg" => $SuppRemarks,
				"SuppReadDate" => "",
				"InputBy" => $_SESSION["UserID"],
				"DateInput" => date("Y-m-d H:i:s")
			);
			$res = $indexVar["FormGrid"]->addSuppRequest($requestData);
			if ($res["result"] == "SUCCESS") {
				$res["resultHTML"] = $indexVar["libeform_ui"]->getSuppHTMLBody($requestData);
			}
			sleep(1);
		}
	}
	echo $indexVar["FormInfo"]->jsonToStr($res);
} else {
	No_Access_Right_Pop_Up();
}
?>