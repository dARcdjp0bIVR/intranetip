<?php
// editing by
/********************
 * Date : 2018-03-12 Frankie
 * Description :    Add Folder Structure to eForm
 ********************/
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($_POST['keyword'])) {
	$keyword = $_POST['keyword'];
}
/*
$arrCookies = array();
$arrCookies[] = array("eform_form_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
*/

# Page Title
$TAGS_OBJ[] = array($Lang['eForm']['FormManagement']);
$indexVar['libeform_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libinterface']->Include_AutoComplete_JS_CSS();
echo $indexVar['libeform_ui']->Include_Thickbox_JS_CSS();
echo $indexVar['libinterface']->Include_JS_CSS();

$formData = array();
$eform_action = "new";
// if (isset($_POST["groupIdAry"][0]) && $_POST["groupIdAry"][0] > 0) {
if (isset($_GET["cid"]) && !empty($_GET["cid"]))
{
	$thisCTK =  $indexVar["FormInfo"]->getToken($_GET["cid"] . "-copy");
}
else 
{
	$thisCTK =  $indexVar["FormInfo"]->getToken($_GET["fid"]);
}

$isUsed = false;

if ((isset($_GET["fid"]) && !empty($_GET["fid"]) || isset($_GET["cid"]) && !empty($_GET["cid"])) && ($_GET["ctk"] == $thisCTK)) { 
	// $FormID = $_POST["groupIdAry"][0];
	$eform_action = "edit";
	if (isset($_GET["cid"]) && !empty($_GET["cid"]))
	{
		$eform_action = "copy";
		$FormID = $_GET["cid"];
		$usedTotal = 0;
	}
	else 
	{
		$FormID = $_GET["fid"];
		$usedTotal = $indexVar["FormInfo"]->getTotalSubmitted(array($FormID), TRUE);
	}
	$formInfo = $indexVar["FormInfo"]->getFormInfoById($FormID);
	
	$folderID = (isset($formInfo["FolderID"]) && !empty($formInfo["FolderID"])) ? $formInfo["FolderID"]: '0';
	$folderBreadcrumb = $indexVar['libeform']->getBreadcrumb($folderID);
	$htmlBreadcrumb = $indexVar['libeform_ui']->getBreadcrumbHTML($folderBreadcrumb);
	
	if ($eform_action == "copy")
	{
		unset($formInfo["TotalTargetUser"]);
		unset($formInfo["FormRecordType"]);
	}

	if (isset($formInfo["TemplateID"]) && $formInfo["TemplateID"] > 0) {
		$formInfo["TemplateInfo"] = $indexVar["FormTemplate"]->getFormTemplateInfoById($formInfo["TemplateID"], false, true);
	}
	
	if (isset($usedTotal[$FormID]["Total"]) && $usedTotal[$FormID]["Total"] > 0) {
		$isUsed = true;
	}
	if ($eform_action == 'copy')
	{
		$PAGE_NAVIGATION[] = array($Lang["eForm"]["CopyForm"]);
	}
	else
	{
		$PAGE_NAVIGATION[] = array($Lang["eForm"]["EditForm"]);
	}
} else {
	$_GET["fid"] = "";
	$PAGE_NAVIGATION[] = array($Lang["eForm"]["NewForm"]);
	
	$folderID = (isset($_GET["folder"]) && !empty($_GET["folder"])) ? $_GET["folder"] : '0';
	$folderBreadcrumb = $indexVar['libeform']->getBreadcrumb($folderID);
	$htmlBreadcrumb = $indexVar['libeform_ui']->getBreadcrumbHTML($folderBreadcrumb);
}
$templateOptions = $indexVar["FormTemplate"]->getTableTemplateOption();
if ($eform_action != "copy")
{
	$thisAudience = $indexVar['libeform']->returnTargetUserName($formInfo);
}

$type_selection = "<SELECT name='type' onChange='selectAudience()'>";
$type_selection.= "<OPTION value='' > -- $button_select -- </OPTION>";
$type_selection.= "<OPTION value='1' ". ($formInfo["FormRecordType"]==1 && $formInfo["TotalTargetUser"]==0 ? "selected":"").">" . $Lang["eForm"]["TargetUserAllStaff"] . "</OPTION>";
if(!$sys_custom['DHL'])
{
	$type_selection.= "<OPTION value='2' ". ($formInfo["FormRecordType"]==2 && $formInfo["TotalTargetUser"]==0? "selected":"").">" . $Lang["eForm"]["TargetUserAllTeaching"] . "</OPTION>";
	$type_selection.= "<OPTION value='3' ". ($formInfo["FormRecordType"]==3 && $formInfo["TotalTargetUser"]==0? "selected":"").">" . $Lang["eForm"]["TargetUserAllNonTeaching"] . "</OPTION>";
}
$type_selection.= "<OPTION value='4' ". ($formInfo["FormRecordType"]==4 || $formInfo["TotalTargetUser"] > 0? "selected":"").">" . $Lang["eForm"]["TargetUserIndividual"] . "</OPTION>";
$type_selection.= "</SELECT>";

$htmlAry["contentBody"] = $indexVar['libeform_ui']->return_FormContent($formInfo, $isUsed, $eform_action);

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libeform_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
?>