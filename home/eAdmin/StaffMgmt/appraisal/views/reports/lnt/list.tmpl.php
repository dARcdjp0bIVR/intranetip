<script src="/templates/jquery/jquery-1.12.4.min.js"></script>
<script src="/templates/jquery/jquery-migrate-1.4.1.min.js"></script>
<script src="/templates/jquery/jquery.floatheader.min.js"></script>
<script src="/templates/jquery/jquery.PrintArea.js"></script>
<script src="/templates/highchart/highcharts.js"></script>

<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
</div>



<script type="text/javascript">
var reportContent = "";
$(document).ready( function() {

	//// UI START ////
	function updateTeacherList(){
		var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>lnt<?=$appraisalConfig['taskSeparator']?>ajaxGetTeacherList';
		$('#teacherSelectDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(url, $('#form1').serialize());
	}
	
	$('#AcademicYearID').change(function(){
		var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>lnt<?=$appraisalConfig['taskSeparator']?>ajaxGetSubjectList';
		$('#FromSubjectIdDiv, #teacherSelectDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$('#FromSubjectIdDiv').load(url, $('#form1').serialize(), function(){
        	$('#FromSubjectID').change(updateTeacherList).change();
		});
	}).change();
	//// UI END ////


	//// Report START ////
	$('#Btn_View').click(function(){
		if($('#TeacherID').length == 0 || $('#TeacherID').val() == ''){
			alert('<?=$Lang['Appraisal']['LNT']['ReportMsg']['SelectTeacher']?>');
			return false;
		}
		if($('[name="reportType"]:checked').length == 0){
			alert('<?=$Lang['Appraisal']['LNT']['ReportMsg']['SelectReportType']?>');
			return false;
		}
		
		$("#PrintBtn").hide();

		var reportType = $('[name="reportType"]:checked').val();
		var url;
		if(reportType == 1){
			url = '?task=reports<?=$appraisalConfig['taskSeparator']?>lnt<?=$appraisalConfig['taskSeparator']?>ajaxGetReport1';
		}else{
			url = '?task=reports<?=$appraisalConfig['taskSeparator']?>lnt<?=$appraisalConfig['taskSeparator']?>ajaxGetReport2';
		}
		$('#DBTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(url, $('#form1').serialize(), function(){
			$("#PrintBtn").show();
			$('.resultTable').floatHeader();
		});
	});
	//// Report END ////
		
});

function Print(){
	var print = "";
	var options = { mode : "popup", popClose : false};
	print = "#PrintArea";
	$( print ).printArea(options);
}
</script>