<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>
<?=$htmlAry['contentTool']?>
<div id="PrintArea">
	<div id="report">
	</div>
</div>


<script type="text/javascript">
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$("input[id^='LoginID']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>task_assignment<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					var idArr=elemId.split('_');
					var idIdx=idArr[1];					
					$("#UserName").text(li.extra[1]);
					$("#UserID").val(li.extra[0]);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	$('#CycleID').change(function(){
		$('#staff_selection').html(ajaxImage);
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>task_assignment<?=$appraisalConfig['taskSeparator']?>ajax_getData',
			{
				"Type":"GetLoginUserSelection",
				"CycleID": $('#CycleID').val()
			},
			function(res){
				if(res==""){
					$('#genRptBtn').attr("disabled",'').addClass("formbutton_disable").removeClass("formbutton");
				}else{
					if($('#staff_selection').length > 0){
						$('#staff_selection').html(res);
						$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
					}
				}
			}
		);
	});
});


function getStaffSelection(){
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>task_assignment<?=$appraisalConfig['taskSeparator']?>ajax_getData',
		{
			"Type":"GetLoginUserSelection",
			"CycleID": $('#CycleID').val()
		},
		function(res){
			if($('#staff_selection').length > 0){
				$('#staff_selection').html(res);
			}else{
				$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
			}
		}
	);
}
function goRptGen(){
	var cycleID=$("#CycleID :selected").val();
	var login = $("#LoginID").val();
	var mode = $("input[name='mode']:checked").val();
	$('#report').html(ajaxImage);
	$("#PrintBtn").hide();
	$("#cycleIDEmptyWarnDiv").hide();
    $("#userNameEmptyWarnDiv").hide();
	if(cycleID == ""){
    	$("#cycleIDEmptyWarnDiv").show();
    }
    else if(mode==1 && login == ""){
    	$("#userNameEmptyWarnDiv").show();
    }else{ 
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>task_assignment<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetReport',
			{
				"cycle": cycleID,
				"login": login,
				"mode": mode
			},
			function(callback){
				$('#report').empty();
				$('#report').html(callback);
				$("#PrintBtn").show();
			}
		);
    }
}

function activateUserSelection(mode)
{
	if(mode==0){
		$('.stdSelection').hide();
	}else{
		$('.stdSelection').show();
	}
}

function Print(){
	var cycleID=$("#CycleID :selected").val();
	var login = $("#LoginID").val();
	var mode = $("input[name='mode']:checked").val();
	newWindow("/home/eAdmin/StaffMgmt/appraisal/controllers/reports/task_assignment/view_print.php?cycleID="+cycleID+"&mode="+mode+"&login="+login,12);		
};
</script>