<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>
<?=$htmlAry['contentTool']?>
<div id="PrintArea">
	<div id="report" style="text-align:left;">
	</div>
</div>


<script type="text/javascript">
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$("input[id^='LoginID']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_average<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					var idArr=elemId.split('_');
					var idIdx=idArr[1];					
					$("#UserName").text(li.extra[1]);
					$("#UserID").val(li.extra[0]);
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	
	$('#CycleID').change(function(){
		$('#staff_selection').html(ajaxImage);
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_average<?=$appraisalConfig['taskSeparator']?>ajax_getData',
			{
				"Type":"GetLoginUserSelection",
				"CycleID": $('#CycleID').val()
			},
			function(res){
				if(res==""){
					$('#genRptBtn').attr("disabled",'').addClass("formbutton_disable").removeClass("formbutton");
				}else{
					if($('#staff_selection').length > 0){
						$('#staff_selection').html(res);
						$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
					}
				}
			}
		);
	});
	
	$('#templateShowAll').click(function(){
		if($('#templateShowAll').is(':checked')){
			$('#templateSelectionRow').hide();
		}else{
			$('#templateSelectionRow').show();
		}
	});
	$('textarea.tabletext').each(function(){
		$(this).height(this.scrollHeight);
	});
});

function goRptGen(){
	var cycleID = $('#CycleID').val();
	if($('#templateShowAll').is(':checked')==false){
		var templateID = $('#TemplateID').val();
	}else{
		var templateID = -1;
	}
	var appraiseeID = $('#appraiseeID').val();
	$("#PrintBtn").hide();
	$('#report').html("");
	$('#msg_no').remove();
	if(cycleID != '' && templateID != '' && appraiseeID != ''){
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_average<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetFormDetailsList',
			{
				'cycleID':cycleID,
				'templateID':templateID,
				'appraiseeID':appraiseeID
			},
			function(data){
				if(data.split('|')[0] == "0"){					
					$('.edit_bottom_v30').append("<p id='msg_no'>"+data.split('|')[1]+"</p>");
				}else{
					$('#report').html(data);
					$("#PrintBtn").show();
				}
				$('textarea.tabletext').each(function(){
					$(this).height(this.scrollHeight);
				});
			}
		);
	}else{
		alert('error!');
	}
}

function reloadTemplateSel(){
	var selectedTemplate = $('#TemplateID').val();
	var cycleID = $('#CycleID').val();
	$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_average<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetCycleActiveTemplate',
			{
				'cycleID':$('#CycleID').val(),
				'templateID':$('#TemplateID').val()
			},
			function(data){
				$('#templateSelection').html(data);
			}
		);
}

function Print(){
	var cycleID = $('#CycleID').val();
	if($('#templateShowAll').is(':checked')==false){
		var templateID = $('#TemplateID').val();
	}else{
		var templateID = -1;
	}
	var userID = $('#appraiseeID').val();
	newWindow("/home/eAdmin/StaffMgmt/appraisal/controllers/reports/personal_average/view_print.php?cycleID="+cycleID+"&templateID="+templateID+"&userID="+userID,12);		
};

function getStaffSelection(){
	$.post(
		'?task=reports<?=$appraisalConfig['taskSeparator']?>personal_average<?=$appraisalConfig['taskSeparator']?>ajax_getData',
		{
			"Type":"GetLoginUserSelection",
			"CycleID": $('#CycleID').val()
		},
		function(res){
			if($('#staff_selection').length > 0){
				$('#staff_selection').html(res);
			}else{
				$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
			}
		}
	);
}
</script>