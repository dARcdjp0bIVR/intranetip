<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">
<style>
    #progress {
      display: none;	
      border: 1px solid #aaa;
      height: 20px;
    }
    #progress .bar {
      background-color: #ccc;
      height: 20px;
    }
    #progress .label {
      position: absolute;
	  left: 49%;
    }
</style>
<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
		<div id="progress">0%</div>
	</div> 
</div>
<?=$htmlAry['contentTool']?>
<div id="PrintArea">
	<div id="report" style="text-align:left;">
	</div>
</div>


<script type="text/javascript">
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	$('#CycleID').change(function(){
		$('#appraiseelist_selection').html(ajaxImage);
		$.post(
			'?task=reports<?=$appraisalConfig['taskSeparator']?>export_appraisal_report<?=$appraisalConfig['taskSeparator']?>ajax_getData',
			{
				"Type":"GetLoginUserSelection",
				"CycleID": $('#CycleID').val()
			},
			function(res){
				if(res==""){
					$('#genRptBtn').attr("disabled",'').addClass("formbutton_disable").removeClass("formbutton");
				}else{
					if($('#appraiseelist_selection').length > 0){
						$('#appraiseelist_selection').html(res);
						$('#genRptBtn').removeAttr("disabled").removeClass("formbutton_disable").addClass("formbutton");
					}
				}
			}
		);
	});
	$('textarea.tabletext').each(function(){
		$(this).height(this.scrollHeight);
	});
});

function goRptGen(){
	var cycleID=$("#CycleID :selected").val();	
	var data = "CycleID="+cycleID;
	var selectedAppraisee = $("#appraiseelist_content").val();
	if(	typeof selectedAppraisee != "undefined"){
    	if(selectedAppraisee.length != 0 && selectedAppraisee[0] == ""){
    		selectedAppraisee.shift();
    		}
	} 
	$("#cycleIDEmptyWarnDiv").hide();
	$("#appraiseeListEmptyWarnDiv").hide();
// 	console.log("cycleID : " + cycleID);
// 	console.log("data : " + data);
// 	console.log("selectedAppraisee : " + selectedAppraisee);
	if(cycleID == ""){
		$("#cycleIDEmptyWarnDiv").show();
		var warning = true;
	}
	if(selectedAppraisee == "" || typeof selectedAppraisee == "undefined"){
		$("#appraiseeListEmptyWarnDiv").show();
		var warning = true;
	}
	if (warning != true){
		var confirmMessage = "<?=$Lang['Appraisal']['BtbConExportAppraisalList']?>";
		var r = confirm(confirmMessage);
		if(r == true){
    		$.ajax({
				type: "POST",
				url: 'index.php?task=reports<?=$appraisalConfig['taskSeparator']?>export_appraisal_report<?=$appraisalConfig['taskSeparator']?>zip_getfile',
				data: {
					'cycleID': $("#CycleID").val(),
					'selectedAppraisee': selectedAppraisee.toString()
				},
				success: function(return_path){
					window.open(return_path);
				}
			});
    		timer = window.setInterval(refreshProgress, 1000);
    		$.blockUI({ message: $("#progress")});
		}
	}

}

function refreshProgress() {
	  $.ajax({
		    url: "controllers/reports/export_appraisal_report/progress_control.php?file=<?php echo "export_appraisal_report".session_id() ?>",
		    success:function(result){
		      var data = JSON.parse(result);	
		      $("#progress").html('<div class="label">'+ data.message +'</div>'+'<div class="bar" style="width:' + data.percent + '%"></div>');
		      if (data.percent == 100) {
		        window.clearInterval(timer);
		        timer = null;
		        timerAlert = window.setTimeout(refreshProgressCompleted, 1000);		         
		      }
		    }
		  });
}
function refreshProgressCompleted(){
    window.clearTimeout(timer);
    $('#progress').css("display","none");
    $.unblockUI();
    $("#progress").remove();

}
</script>