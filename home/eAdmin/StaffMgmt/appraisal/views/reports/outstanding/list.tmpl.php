<div class="table_board">
	<div>
		<?=$htmlAry['contentTbl']?>
	</div>
	<br style="clear:both;" />
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['viewBtn']?>
		<p class="spacer"></p>
	</div>	
</div>

<div id="report"></div>


<script type="text/javascript">
var reportContent = "";
<?php
if($sys_custom['eAppraisal']['ShowOutstandingByRole']){?>
	var destination = 'view_print_role';
<?php
}else{?>
	var destination = 'view_print';
<?php	
}	
?>
var appRole = "";
$(document).ready( function() {
});

function goRptGen(){
	var cycleID=$("#CycleID :selected").val();	
	var data = "CycleID="+cycleID;
	<?php
	if($sys_custom['eAppraisal']['ShowOutstandingByRole']){?>
	var appRole=$("input[name='RoleType']:checked").val();
	data = data+"&appRole="+appRole;
	<?php	
	}	
	?>
	$("#cycleIDEmptyWarnDiv").hide();
	if(cycleID == ""){
		$("#cycleIDEmptyWarnDiv").show();
	}
	else{
		/*
		$.ajax({ type: "POST", url: '?task=reports<?=$appraisalConfig['taskSeparator']?>outstanding<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
	        data: data,
	        success: function (data, textStatus, jqXHR) {
	            $("#report").empty();
	            $("#report").append(data); 
	            reportContent = data;          	            
	        }, error: function (jqXHR, textStatus, errorThrown) {
	           
	        }
	    });*/
		var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>outstanding<?=$appraisalConfig['taskSeparator']?>'+destination+'&cycleID='+$("#CycleID").val()+'&appRole='+appRole;
		newWindow(url,35);
	}
}

function print_timetable()
{
	var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>outstanding<?=$appraisalConfig['taskSeparator']?>'+destination+'&cycleID='+$("#CycleID").val()+'&appRole='+appRole;
	newWindow(url,35);	
}

function export_csv(){
	var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>outstanding<?=$appraisalConfig['taskSeparator']?>export&cycleID='+$("#CycleID").val()+'&appRole='+appRole;	
	window.location.href = url;
}
</script>