<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
	
	
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
		<div>
			<?=$htmlAry['contentTbl2']?>
		</div>
		<?=$htmlAry['previewBtn']?>
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {
	initDndTable();
});

function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var templateID = $("#TemplateID").val();
			  var secID = $("#SecID").val();
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&SecID="+secID+"&Type=QCat";				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var templateID=$("#TemplateID").val();
		var parentSecID=$("#ParentSecID").val();
		var secID=($("#SecID").val()=="NULL")?"%s":$("#SecID").val();
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_subSec&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID;	
		
		var typeVal="SDFSubSecSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	return true;
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
}
function goDelete(qCatID){
	var typeVal="SDFDelete";
	var type = $("<input/>").attr("type", "hidden").attr("name", "Type").val(typeVal);
	var secID= $("<input/>").attr("type", "hidden").attr("name", "Status").val(secID);
	$('#form1').append($(type)).append($(secID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
function goBackTemplate(templateID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;
}
function goBackParSec(templateID,parSecID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+"&secID="+parSecID;
}
function goEditQCat(templateID,parSecID,secID,qCatID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID;
}
function goAddQCat(templateID,parSecID,secID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID=";
}
function goDelete(templateIDVal,qCatIDVal){
	var templateID=$("#TemplateID").val();
	var parentSecID=$("#ParentSecID").val();
	var secID=$("#SecID").val();
	var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_subSec&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID;
	var typeVal="SDFQCatDelete";		
	var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
	var qCatID = $("<input/>").attr("type", "hidden").attr("name", "qCatID").val(qCatIDVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(templateID)).append($(qCatID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
</script>