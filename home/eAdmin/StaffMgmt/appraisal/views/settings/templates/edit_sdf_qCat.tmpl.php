<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
	
	
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
		<div>
			<?=$htmlAry['contentTbl2']?>
		</div>
		<div>
			<?=$htmlAry['contentTbl3']?>
		</div>		
		<?=$htmlAry['previewBtn']?>
	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {
	initDndTable();
	initDndTable2();
	var qCatDisp = $("input[name='QCatDisplayMode']:checked").val();
	if(qCatDisp==1){
		$("#trAvgMark,#trAvgDec,#trVerAvgMark,#trVerAvgDec").show();
	}
	else{
		$("#trAvgMark,#trAvgDec,#trVerAvgMark,#trVerAvgDec").hide();
	}
});

function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var templateID = $("#TemplateID").val();
			  var secID = $("#SecID").val();
			  var qCatID = $("#QCatID").val();
			  var inputType = $("#DisplayMode").val();			 
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&SecID="+secID+"&QCatID="+qCatID+"&Type="+inputType;				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
function initDndTable2() {
	$("table#dndTable2").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var templateID = $("#TemplateID").val();
			  var secID = $("#SecID").val();
			  var qCatID = $("#QCatID").val();
			  var inputType = "Ques";			 
			  var ElementObj = $(this);
			  var url = 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_edit_update';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&TemplateID="+templateID+"&SecID="+secID+"&QCatID="+qCatID+"&Type="+inputType;				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var templateID=$("#TemplateID").val();
		var parentSecID=$("#ParentSecID").val();
		var secID=$("#SecID").val();
		var qCatID=($("#QCatID").val()=="NULL")?"%s":$("#QCatID").val();
		var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID;

		var typeVal="SDFQCatSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	 avgDec = $("#QCatAvgDec").val();
	$("#QCatAvgDecEmptyWarnDiv").hide();
	if(isNumeric(avgDec) == false){
		$("QCatAvgDecEmptyWarnDiv").val('<?=$Lang['Appraisal']['TemplateSample']['Number']?>');
		$("#QCatAvgDecEmptyWarnDiv").show();
		status = false;
	}
	else if(avgDec<0 || avgDec > 3){
		$("QCatAvgDecEmptyWarnDiv").val('<?=$Lang['Appraisal']['TemplateSample']['DecRng03']?>');
		$("#QCatAvgDecEmptyWarnDiv").show();
		status = false;
	}
	/*
	if($('#dndTable2 tbody tr').length==0 && $("input[name='QCatDisplayMode']:checked").val()==2){
		if(confirm("<?php echo $Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']; ?>")){
			status = status;
		}else{
			status = false;
		}
	}*/		
	return status;
}
function isNumeric(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}
function goReset(){
	$("#form1")[0].reset();
}
function goBack(){
	if($('#dndTable2 tbody tr').length==0){
		if(confirm("<?php echo $Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']; ?>")){
			window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
		}	
	}else{
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list';
	}	
}
function goDelete(qCatID){
	var typeVal="SDFAnsDelete";
	var type = $("<input/>").attr("type", "hidden").attr("name", "Type").val(typeVal);	
	$('#form1').append($(type)).append($(secID));s
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
function goBackTemplate(templateID){
	if($('#dndTable2 tbody tr').length==0){
		if(confirm("<?php echo $Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']; ?>")){
			window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;
		}
	}else{		
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;
	}
}
function goBackParSec(templateID,parSecID){
	if($('#dndTable2 tbody tr').length==0){
		if(confirm("<?php echo $Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']; ?>")){
			window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+"&secID="+parSecID;
		}
	}else{		
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_sec&templateID='+templateID+"&secID="+parSecID;
	}
}
function goBackSubSec(templateID,parSecID,secID){
	if($('#dndTable2 tbody tr').length==0){
		if(confirm("<?php echo $Lang['Appraisal']['TemplateSampleQCat']['ComfirmNoQuesLeave']; ?>")){
			window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_subSec&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID;
		}
	}else{		
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_subSec&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID;
	}
}
function goEditDispMode(templateID,parSecID,secID,qCatID,dispModeID,dispID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_dispMode&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&dispModeID="+dispModeID+"&dispID="+dispID;
}
function goEditQues(templateID,parSecID,secID,qCatID,qID){
    window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_ques&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID="+qID;
}
function goAddLikert(templateID,parSecID,secID,qCatID,dispModeID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_dispMode&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&dispModeID="+dispModeID+"&dispID=";
}
function goAddMatrix(templateID,parSecID,secID,qCatID,dispModeID){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_dispMode&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&dispModeID="+dispModeID+"&dispID=";
}
function goAddQues(templateID,parSecID,secID,qCatID){
    window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_ques&templateID='+templateID+"&parSecID="+parSecID+"&secID="+secID+"&qCatID="+qCatID+"&qID=";
}
function goDeleteQues(templateIDVal,qIDVal){
	var templateID=$("#TemplateID").val();
	var parentSecID=$("#ParentSecID").val();
	var secID=$("#SecID").val();
	var qCatID=$("#QCatID").val();	
	var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID;
	var typeVal="SDFQuesDelete";		
	var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
	var qID = $("<input/>").attr("type", "hidden").attr("name", "qID").val(qIDVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(templateID)).append($(qID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
function goDeleteMatrix(templateIDVal,mtxFldIDVal){
	var templateID=$("#TemplateID").val();
	var parentSecID=$("#ParentSecID").val();
	var secID=$("#SecID").val();
	var qCatID=$("#QCatID").val();	
	var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID;
	var typeVal="SDFMatrixDelete";		
	var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
	var mtxFldID = $("<input/>").attr("type", "hidden").attr("name", "mtxFldID").val(mtxFldIDVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(templateID)).append($(mtxFldID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
function goDeleteLikert(templateIDVal,markIDVal){
	var templateID=$("#TemplateID").val();
	var parentSecID=$("#ParentSecID").val();
	var secID=$("#SecID").val();
	var qCatID=$("#QCatID").val();	
	var returnPathVal='Location: ?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf_qCat&templateID='+templateID+"&parSecID="+parentSecID+"&secID="+secID+"&qCatID="+qCatID;
	var typeVal="SDFLikertDelete";		
	var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
	var markID = $("<input/>").attr("type", "hidden").attr("name", "markID").val(markIDVal);
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(templateID)).append($(markID));
	$('form#form1').attr('action', 'index.php?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
$("input[name='QCatDisplayMode']").change(function(e){
	var id = e.target.id;
	if(id=="QCatMode_1"){
		$("#trAvgMark,#trAvgDec,#trVerAvgMark,#trVerAvgDec").show();
	}
	else{
		$('#QCatAvgMark').attr('checked', false);
		$("#QCatAvgDec").val(0);
		$('#QCatVerAvgMark').attr('checked', false);
		$("#QCatVerAvgDec").val(0);
		$("#trAvgMark,#trAvgDec,#trVerAvgMark,#trVerAvgDec").hide();
	}
});

</script>