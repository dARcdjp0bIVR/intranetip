<form name="form1" id="form1" method="POST" action="?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>list">
	
	<div class="table_board">
	
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['filter1']?>
		<br style="clear:both;">
		
	</div>
	<div>
		<?=$htmlAry['contentTbl1']?>
	</div>	
	<div>
		<?=$htmlAry['dataTable1']?>
	</div>
		

	</div>
</form>

<script type="text/javascript">
$(document).ready( function() {	
	$('input#keyword').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter			
		}
	});	
});

function goArrange(templateID,type){
	if(type=="PDF"){
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit&templateID='+templateID;	
	}
	else if(type=="SDF"){
		window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID='+templateID;	
	}	
}
function goAddTemplate(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_sdf&templateID=';
}
function goChangeOrder(){
	window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>edit_order';
}
function goDelete(){	
	var confirmMessage = "<?=$Lang['Appraisal']['BtbDelItem']?>";
	var r = confirm(confirmMessage);
	if(r == true){
		//get the related template form id
		var obj = document.form1;
		var element_name = 'templateIDAry[]';
		var len=obj.elements.length;
        var i=0;
        var j=0;
        var result = "";
        for( i=0 ; i<len ; i++) {
           if (obj.elements[i].name==element_name && obj.elements[i].checked){
	          j=j+1;
	          result+=obj.elements[i].value+",";
           }
        }
        result=result.substring(0, result.length-1); 
       	var data = 'templateIDAry='+result+"&type=checkTemplate";
       	$("#msgDiv").hide();
       	$("#msg").text("");
		$.ajax({ type: "POST", url: '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>ajax_getData', 			
	        data: data,
	        success: function (data, textStatus, jqXHR) {
	            var result = eval('(' + data + ')');	    
	            if(result[0]["result"] != 0){	
		           	$("#msgDiv").show();
		            $("#msg").text(result[0]["msg"]);
	            }
	            else{		            
	           		checkRemove(document.form1,'templateIDAry[]','?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete&type=list');
	            }
	        }, error: function (jqXHR, textStatus, errorThrown) {
	           
	        }
	    });		
	}
}
function goCopy(){	
	checkEdit(document.form1,'templateIDAry[]','?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>copy');
}
function goPreview(templateID){
	window.open('?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID,'_blank');
	//window.location = '?task=settings<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>gen_form&templateID='+templateID;
}
</script>