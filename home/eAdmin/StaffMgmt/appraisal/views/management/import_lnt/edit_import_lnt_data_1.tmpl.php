<form id="form1" name="form1" method="post" action="index.php" enctype="multipart/form-data">
	<?=$htmlAry['msg']?>
	<br/>
	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>
<script type="text/javascript">
var questionCount = <?= $objJson->encode($countQuestion); ?>

$(document).ready( function() {
	$('#ImportTypeQuestion').click(function(){
		$('.typeQuestion').show();
		$('.typeAnswer').hide();
	});
	$('#ImportTypeAnswer').click(function(){
		$('.typeQuestion').hide();
		$('.typeAnswer').show();
	});
	
	$('#ImportTypeQuestion').click();
});

function goSubmit(){
	var status = formValidation();
	if (status == true){
		var page = '';
		if($('[name="ImportType"]:checked').val() == '<?=libappraisal_lnt::LNT_IMPORT_TYPE_QUESTION?>'){
			page = 'edit_import_lnt_question_2';
		}else{
			page = 'edit_import_lnt_answer_2';
		}
		
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>import_lnt<?=$appraisalConfig['taskSeparator']?>' + page).submit();		
	}
}

function formValidation(){
	$("#msgTable, #errMsg, #ImportTypeEmptyWarnDiv, #errMsgNoQuestion").hide();
	var count = questionCount[$('#AcademicYearID').val()] || 0;
	
	if($('[name="ImportType"]:checked').length == 0){
		$("#ImportTypeEmptyWarnDiv").show();
		return false;
	}else if($("#ImportData").val() == "" || !$("#ImportData").val().match(/.(csv|txt)$/i)){
		$("#msgTable, #errMsg").show();
		return false;
	}else if(count == 0 && $('[name="ImportType"]:checked').val() == '<?=libappraisal_lnt::LNT_IMPORT_TYPE_ANSWER?>'){
		$("#msgTable, #errMsgNoQuestion").show();
		return false;
	}
	
	return true; 
}

function goLeave(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_leave_1';
}
function goAbsLateSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_1';
}
function goSLLateSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_sl_late_1';
}
function goAttendance(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import<?=$appraisalConfig['taskSeparator']?>edit_import_attendance_1';
}
function goLntSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import_lnt<?=$appraisalConfig['taskSeparator']?>edit_import_lnt_data_1';
}

function generateAnswerTemplate(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>import_lnt<?=$appraisalConfig['taskSeparator']?>generate_answer_sample&AcademicYearID=' + $('#AcademicYearID').val() + '&SubjectID=' + $('#SubjectID').val();
}

</script>