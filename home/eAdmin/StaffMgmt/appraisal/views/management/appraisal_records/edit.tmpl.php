<script type="text/javascript" src="../../../../templates/jquery/jquery.jeditable.js"></script>
<link href='../../../../templates/2009a/css/appraisal/content.css' rel='stylesheet'>
<style>
.content_top_tool .Conntent_tool a{
	background: url('../../../../images/2009a/content_icon.gif') no-repeat 0px -80px;
}
</style>
<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<div class="content_top_tool" style="float:right">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
	</div>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
		<div style="border-top: 1px dashed #CCCCCC;">
			<?=$htmlAry['contentTbl2']?>
		</div>	
	</div>
	

</form>

<script type="text/javascript">
	var jsShowHideSpeed = "fast";
	var AcademicYearID = "<?=$academicYearID?>";
	$(document).ready(function(){		
		//$("signauture").attr("type","password");
		$('input[type="password"]').val("");
		$('.CPDNoOfSection').change(function(){
			calCPDHours();
		});
		$('.toAttendanceSum').change(function(){
			calAttendanceSummary();
		});
		$('form#form1').submit(function(){
			var noOfExtCur = 0;
			$('input[id^="othExtCurDesc_"]').each(function(){
				if($(this).val()!=""){
					noOfExtCur = noOfExtCur + 1;
				}
			});
			$('#othExtCurLength').val(noOfExtCur);
		});
		if("<?=$sys_custom['eAppraisal']['AttendanceAutoSync']?>"==true){
			importAttendance('<?=$UserID?>','<?=$rlsNo?>','<?=$recordID?>','<?=$batchID?>');
		}
		js_reload_form_lesson_hours_counter();
		js_reload_class_lesson_hours_counter();
	});

	function changeDatePicker(){
	};
	function addQualification(recordID,batchID){
		var currentRow = parseInt($("#currentQualificationRow").val());		
		currentRow = currentRow+1;
		$("#qualificationRow_"+currentRow).show();
		$("#currentQualificationRow").val(currentRow);	
	}
	function deleteQualification(recordID,batchID,rowID){
		$("#hiddenQualification_"+rowID).val("INACTIVE");
		$("#qualificationRow_"+rowID).hide();
	}
	function addClass(recordID,batchID){
		var currentRow = parseInt($("#currentClassRow").val());
		currentRow = currentRow+1;
		$("#teachClassRow_"+currentRow).show();
		$("#currentClassRow").val(currentRow);
		js_reload_class_lesson_hours_counter();
		
	}
	function deleteClass(recordID,batchID,rowID){
		$("#hiddenClass_"+rowID).val("INACTIVE");
		$("#teachClassRow_"+rowID).hide();
	}
	function addForm(recordID,batchID){
		var currentRow = parseInt($("#currentFormRow").val());
		currentRow = currentRow+1;
		$("#teachFormRow_"+currentRow).show();
		$("#currentFormRow").val(currentRow);
		js_reload_form_lesson_hours_counter();
	}
	function deleteForm(recordID,batchID,rowID){
		$("#hiddenForm_"+rowID).val("INACTIVE");
		$("#teachFormRow_"+rowID).hide();
	}
	function addCrsSemWs(recordID,batchID){
		var currentRow = parseInt($("#currentCrsSemWsRow").val());		
		currentRow = currentRow+1;
		$("#crsSemWsRow_"+currentRow).show();
		$("#currentCrsSemWsRow").val(currentRow);	
	}
	function deleteCrsSemWs(recordID,batchID,rowID){
		$("#hiddenCrsSemWsSts_"+rowID).val("INACTIVE");
		$("#crsSemWsRow_"+rowID).hide();
	}
	function addCrsSemWs(recordID,batchID){
		var currentRow = parseInt($("#currentCrsSemWsRow").val());		
		currentRow = currentRow+1;
		$("#crsSemWsRow_"+currentRow).show();
		$("#currentCrsSemWsRow").val(currentRow);	
	}
	function deleteCrsSemWs(recordID,batchID,rowID){
		$("#hiddenCrsSemWsSts_"+rowID).val("INACTIVE");
		$("#crsSemWsRow_"+rowID).hide();
	}
	function addTaskAssessment(recordID,batchID){
		var currentRow = parseInt($("#currentTaskAssessmentRow").val());		
		currentRow = currentRow+1;
		$("#taskAssessmentRow_"+currentRow).show();
		$("#currentTaskAssessmentRow").val(currentRow);	
	}
	function deleteTaskAssessment(recordID,batchID,rowID){
		$("#hiddenTaskAssessmentSts_"+rowID).val("INACTIVE");
		$("#taskAssessmentRow_"+rowID).hide();
	}
	function addCatTasks(recordID,batchID,catID){
		var currentRow = parseInt($("#currentTaskAssessmentRow_"+catID).val());
		currentRow = currentRow+1;
		$("#taskAssessmentRow_"+catID+"_"+currentRow).show();
		$("#currentTaskAssessmentRow_"+catID).val(currentRow);
	}
	function deleteCatTasks(recordID,batchID,catID,rowID){
		$("#hiddenTaskAssessmentSts_"+catID+"_"+rowID).val("INACTIVE");
		$("#taskAssessmentRow_"+catID+"_"+rowID).hide();
	}
	function addStudentTraining(recordID,batchID){
		var currentRow = parseInt($("#currentStudentTrainingRow").val());		
		currentRow = currentRow+1;
		$("#studentTrainingRow_"+currentRow).show();
		$("#currentStudentTrainingRow").val(currentRow);	
	}
	function deleteStudentTraining(recordID,batchID,rowID){
		$("#hiddenStudentTrainingSts_"+rowID).val("INACTIVE");
		$("#studentTrainingRow_"+rowID).hide();
	}
	function addTeachingExp(recordID,batchID){
		var currentRow = parseInt($("#currentTeachingExpRow").val());		
		currentRow = currentRow+1;
		$("#teachingExpRow_"+currentRow).show();
		$("#currentTeachingExpRow").val(currentRow);	
	}
	function deleteTeachingExp(recordID,batchID,rowID){
		$("#hiddenTeachingExpSts_"+rowID).val("INACTIVE");
		$("#teachingExpRow_"+rowID).hide();
	}
	function addTraining(recordID,batchID){
		var currentRow = parseInt($("#currentTrainingRow").val());		
		currentRow = currentRow+1;
		$("#trainingRow_"+currentRow).show();
		$("#currentTrainingRow").val(currentRow);	
	}
	function deleteTraining(recordID,batchID,rowID){
		$("#hiddenTrainingSts_"+rowID).val("INACTIVE");
		$("#trainingRow_"+rowID).hide();
	}
	function addCPD(recordID,batchID){
		var currentRow = parseInt($("#currentCPDRow").val());		
		currentRow = currentRow+1;
		$("#CPDRow_"+currentRow).show();
		$("#currentCPDRow").val(currentRow);	
	}
	function deleteCPD(recordID,batchID,rowID){
		$("#hiddenCPDSts_"+rowID).val("INACTIVE");
		$("#CPDRow_"+rowID).hide();
		calCPDHours();
	}
	function calCPDHours(){
		var cpdHours1=0;
		var cpdHours2=0;
		var total = 0;
		var separateMonth = <?php echo $cpdHoursCountingSeparateMonth?>;
		$('.CPDNoOfSection').each(function(index){
			if($(this).parent().parent().css('display')!="none"){
				var startDate = new Date($('#CPDStartDate_'+(index+1)).val());
				if (typeof startDate != 'undefined' && $(this).val()!=""){
					if(separateMonth >= 8){
						if(startDate.getMonth() >= 8 && startDate.getMonth() < separateMonth){
							cpdHours1 = parseFloat(cpdHours1) + parseFloat($(this).val());
						}else{
							cpdHours2 = parseFloat(cpdHours2) + parseFloat($(this).val());
						}
					}else{
						if(startDate.getMonth() >= <?php echo $cpdHoursCountingSeparateMonth?> && startDate.getMonth() <= 7){
							cpdHours2 = parseFloat(cpdHours2) + parseFloat($(this).val());
						}else{
							cpdHours1 = parseFloat(cpdHours1) + parseFloat($(this).val());
						}	
					}
				}
			}
		});
		$('#cpdHourA').html(parseFloat(cpdHours1));
		$('#cpdHourB').html(parseFloat(cpdHours2));
		var totalHour = parseFloat(cpdHours1) + parseFloat(cpdHours2);
		$('#cpdHourTotal').html(totalHour);
		$('#yearHour_'+AcademicYearID+'_span').html(totalHour);
		$('#yearHour_'+AcademicYearID).val(totalHour);
	}
	function importCPD(UserID,RlsNo,RecordID,BatchID){
		$('#import').css('display','none');
		$('#loading_cpd').css('display','inline-block');
		var SeqID = 0;
		var rowInDB = 0;
		$('.hiddenCPDSeqID').each(function(index){
			if($(this).val()!=""){
				SeqID = $(this).val();
				rowInDB++;
			}
		});
		var displayedRow = 1;
		var lastExtraRow = 0;
		$('.CPDRow').each(function(index){
			if($(this).css('display')!="none"){
				displayedRow++;
				if(displayedRow > SeqID){
					SeqID++;
				}
				lastExtraRow = index+1;
			}	
		});
		var data = "type=importCPDFromTeacherPortfolio&userID="+UserID+"&rlsNo="+RlsNo+"&recordID="+RecordID+"&batchID="+BatchID+"&lastRow="+lastExtraRow;
		$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
	        data: data,async:true,timeout:1000,
	        success: function (data, textStatus, jqXHR) {		
	         	var result = eval('(' + data + ')');
	           	var newRowInDB = parseInt(result['newRowInDB']);
	           	var rows = result['RowsToBeAppended'];
	           	var currHourA = $('#cpdHourA').html();
	           	var currHourB = $('#cpdHourA').html();
	           	$('#cpdHourA').html(parseInt(currHourA)+parseInt(result['CPDHours1']));
	           	$('#cpdHourB').html(parseInt(currHourB)+parseInt(result['CPDHours2']));
	           	for(var k=lastExtraRow+1; k < lastExtraRow+1+newRowInDB; k++){
		        	if($('#CPDRow_'+k).length==1){
		        		$('#CPDRow_'+k).remove();
		        	}
		        }
	           	if(lastExtraRow==0){
	           		$('#CPDTable > tbody > tr:first').before(rows);
	           	}else{
	           		$('#CPDRow_'+lastExtraRow).after(rows);
	           	}
	           	<?php
	           	if($sys_custom['eAppraisal']['3YearsCPD']==true){
	           	?>
		           	var pastYearCPDHours = result['PastYearCPDHours'];
		           	for(var yearID in pastYearCPDHours){
		           		if($('#yearHour_'+yearID+'_span').length > 0){
		           			$('#yearHour_'+yearID+'_span').html(pastYearCPDHours[yearID]);
		           			$('#yearHour_'+yearID).val(pastYearCPDHours[yearID]);
		           		}else{
		           			$('#yearHour_'+yearID).val(pastYearCPDHours[yearID]);
		           		}
		           	}
	           	<?php		
	           	}
	           	?>	
	           	calCPDHours();
				$('#import').css('display','inline-block');
				$('#loading_cpd').css('display','none');
	           	alert(result['Reply']);	         	
	        }, 
	        error: function (jqXHR, textStatus, errorThrown) {	            
	        }
	    });
	}
	
	function calAttendanceSummary(){
		if($('.toAttendanceSum').length > 0){
			$('.attendance_total_line span').each(function(){
				$(this).html(0);
			})
			$('.toAttendanceSum').each(function(idx){
				var colID = $(this).attr('column');
				var hourValue = $(this).val();
				if(hourValue==""){
					hourValue = 0;
				}
				var previousValue = $('#attendanceField_total_'+colID+' span').html();
				var newValue = parseFloat(previousValue) + parseFloat(hourValue);
				$('#attendanceField_total_'+colID+' span').html(newValue);
				$('input[name="attendanceData_'+reasonID+'_'+columnID+'"]').val(newValue);
			});
		}
	}
	
	function importAttendance(UserID,RlsNo,RecordID,BatchID){
		if("<?=$sys_custom['eAppraisal']['AttendanceAutoSync']?>"!=true){
			$('#staffAttendanceImport').css('display','none');
			$('#loading_attendance_import').css('display','inline-block');	
		}	
		var post_data = "type=transferAttendnanceDataToForm&userID="+UserID+"&rlsNo="+RlsNo+"&recordID="+RecordID+"&batchID="+BatchID;
		$.post(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>ajax_getData',
			{
				'type':'transferAttendnanceDataToForm',
				'userID':UserID,
				'rlsNo':RlsNo,
				'recordID':RecordID,
				'batchID':BatchID
				
			},
			function(returnData){
				var dataObj = JSON.parse(returnData);
	        	for (var reasonID in dataObj){
				    var column = dataObj[reasonID];
				    for(var columnID in column){
				    	$('#attendanceField_'+reasonID+'_'+columnID+' span').html(column[columnID]);
						$('input[name="attendanceData_'+reasonID+'_'+columnID+'"]').val(column[columnID]);
				    }
				}
				if("<?=$sys_custom['eAppraisal']['AttendanceAutoSync']?>"!=true){
					$('#staffAttendanceImport').css('display','inline-block');
					$('#loading_attendance_import').css('display','none');	
					alert('<?=$Lang['Appraisal']['Message']['SynchronizationCompleted']?>');
				}	
			}
		);
	}
	
	function goSignatureSubmit(){
		$("#passwordErrorWarnDiv").hide();
		var data = "type=getSignatureValidation&userID="+$("#fillerID").val()+"&signauture="+$("#signauture").val();
		$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
	        data: data,async:true,timeout:1000,
	        success: function (data, textStatus, jqXHR) {		
	           		var result = eval('(' + data + ')');
	           		if(result[0]["result"]==0){
						var rlsNo = $("#rlsNo").val();
						var recordID = $("#recordID").val();
						var batchID = $("#batchID").val();
						var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>view&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
						var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
						var typeVal="formSignature";
						var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
						$('#form1').append($(type)).append($(returnPath));;
						$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
	           		}
	           		else{
	           			$("#passwordErrorWarnDiv").show();
	           		}	         	
	        	}, error: function (jqXHR, textStatus, errorThrown) {	            
	        	}
	        }); 
	}
	function goModSignatureSubmit(){		
		//var status=signFormValidation();
		$("#passwordErrorWarnDiv").hide();
		var data = "type=getModSignatureValidation&userID="+$("#fillerID").val()+"&signauture="+$("#signauture").val();
		$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
	        data: data,async:true,timeout:1000,
	        success: function (data, textStatus, jqXHR) {		
	           		var result = eval('(' + data + ')');
	           		if(result[0]["result"]==0){
	           			var rlsNo = $("#rlsNo").val();
	           			var recordID = $("#recordID").val();
	           			var batchID = $("#batchID").val();
	           			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>view&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
	           			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	           			var typeVal="modFormSignature";
	           			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	           			$('#form1').append($(type)).append($(returnPath));
	           			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_update').submit();        			
	           		}
	           		else{
	           			$("#passwordErrorWarnDiv").show();
	           		}	         	
	        	}, error: function (jqXHR, textStatus, errorThrown) {	            
	        	}
	        }); 
	}
	function goSubmit(){
		var status=formValidation();
		if (status == true){
			var rlsNo = $("#rlsNo").val();
			var recordID = $("#recordID").val();
			var batchID = $("#batchID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			var typeVal="formSave";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
		}		
	}
	function goBack(){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>list';
	}
	function formValidation(){
		return true;
	}
	function goViewRelated(templateID){
		var url = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>view_related_forms&rlsNo='+$("#rlsNo").val()+'&recordID='+$("#recordID").val()+'&batchID='+$("#batchID").val()+'&viewTemplateID='+templateID;
		newWindow(url,35);	
	}
	
	$("#totClsCnt,#totFormCnt").change(function(e){
		var id = e.target.id;
		if(id=="totFormCnt"){
			var clsCnt = $("#totFormCnt").val();
			$("#totClsCnt").val(clsCnt);
		}
		else if(id=="totClsCnt"){
			var clsCnt = $("#totClsCnt").val();
			$("#totFormCnt").val(clsCnt);
		}
	});
	//-------------------------temp-------------------------//
	function js_Init_JEdit()
	{
		/************ Add/Edit Building Info ************/
		$('div.jEditQualiDescr').editable( 
			function(updateValue, settings) 
			{
				var thisDivID = $(this).attr("id");
				var idArr = thisDivID.split('_');
				var divID = idArr[0];
				var targetID = idArr[1];				
				updateValue = Trim(updateValue);	
				//alert(updateValue);
				if ($('#QualiDescr_Warning_Div_' + targetID).is(':visible') || updateValue=='' || $(this)[0].revert == updateValue)
				{
					// not vaild
					$('#QualiDescr_Warning_Div_' + targetID).hide(jsShowHideSpeed);
					$(this)[0].reset();
				}
				else
				{
					js_Reload_Learning_Category_Add_Edit_Table();
					//alert(updateValue);
					
					//$("#"+thisDivID).text(updateValue);
					//return updateValue;
					//js_Reload_Learning_Category_Add_Edit_Table();
					//js_Update_Learning_Category(thisDivID, "NameEng", updateValue);
				}			
			},
			{
				tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
				event : "click",
				onblur : "submit",
				type : "text",     
				style  : "display:inline",
				height: "20px"
			}
		);
	}	
	$('div.jEditQualiDescr').click( function() {
		var inputValue = $('form input').val();
		//var inputValue = $(this).attr('id').val();	
		var id = $(this).attr('id');
		var inputValue = $("#"+id).text();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
		//alert(inputValue);
		js_LC_Code_Validation("LC_QualiDescr", inputValue, selfID, 'QualiDescr_Warning_Div_'+selfID);
	});
	function js_LC_Code_Validation(RecordType, InputValue, SelfID, WarningDivID)
	{
		//alert(InputValue);
		if (Trim(InputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			/*$.post(
				"ajax_validate_learning_category_code.php", 
				{
					InputValue: InputValue,
					SelfID: SelfID
				},
				function(ReturnData)
				{
					if (ReturnData == "")
					{
						// valid code
						$('#' + WarningDivID).hide(jsShowHideSpeed);
					}
					else
					{
						// invalid code => show warning
						$('#' + WarningDivID).html(ReturnData);
						$('#' + WarningDivID).show(jsShowHideSpeed);
						
						//$('#debugArea').html('aaa ' + ReturnData);
					}
				}
			);*/
		}
	}
	function js_Reload_Learning_Category_Add_Edit_Table() {
		/*$.post(
			"ajax_reload_content.php", 
			{ RecordType: "LearningCategoryTable" },
			function(ReturnData)
			{
				$('#LearningCategoryInfoSettingLayer').html(ReturnData);
				js_Init_JEdit();
				js_Init_DND_Table();
				
				//$('#debugArea').html(ReturnData);
			}
		);*/
		$('div.jEditQualiDescr').editable('disable');
	}
	function js_reload_form_lesson_hours_counter(){
		$('input[name^="formPerCyc_"]').keyup(function(){calculateFormCounter();});
	}
	function js_reload_class_lesson_hours_counter(){
		$('input[name^="clsPerCyc_"]').keyup(function(){calculateClassCounter();});
	}
	function calculateFormCounter(){
		var hours = 0;
		$('input[name^="formPerCyc_"]').each(function(){
			var sectionHour = parseInt($(this).val());
			if(!isNaN(sectionHour)){
				hours = hours + sectionHour;
			}
		});
		$('#totFormCnt').val(hours);
		$('#totClsCnt').val(hours);
	}
	function calculateClassCounter(){
		var hours = 0;
		$('input[name^="clsPerCyc_"]').each(function(){
			var sectionHour = parseInt($(this).val());
			if(!isNaN(sectionHour)){
				hours = hours + sectionHour;
			}
		});
		$('#totClsCnt').val(hours);
		$('#totFormCnt').val(hours);
	}
</script>