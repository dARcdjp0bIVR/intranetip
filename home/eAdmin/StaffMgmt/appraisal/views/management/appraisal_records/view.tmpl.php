<script type="text/javascript" src="../../../../templates/jquery/jquery.jeditable.js"></script>
<link href='../../../../templates/2009a/css/appraisal/content.css' rel='stylesheet'>
<style>
.content_top_tool .Conntent_tool a{
	background: url('../../../../images/2009a/content_icon.gif') no-repeat 0px -80px;
}
</style>

<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<div class="content_top_tool" style="float:right">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
	</div>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
		<div>						
			<?=$htmlAry['contentTbl3']?>
		</div>
		<div class="edit_bottom_v30">
			<?=$htmlAry['modBtn']?>
		</div>
		<div style="border-top: 1px dashed #CCCCCC;">
			<?=$htmlAry['contentTbl2']?>
		</div>	
	</div>
	

</form>

<script type="text/javascript">
	var jsShowHideSpeed = "fast";
	$(document).ready(function(){		
		$("signauture").attr("type","password");
	});

	function changeDatePicker(){
	};
	function addQualification(recordID,batchID){
		var currentRow = parseInt($("#currentQualificationRow").val());		
		currentRow = currentRow+1;
		$("#qualificationRow_"+currentRow).show();
		$("#currentQualificationRow").val(currentRow);	
	}
	function deleteQualification(recordID,batchID,rowID){
		$("#hiddenQualification_"+rowID).val("INACTIVE");
		$("#qualificationRow_"+rowID).hide();
	}
	function addClass(recordID,batchID){
		var currentRow = parseInt($("#currentClassRow").val());
		currentRow = currentRow+1;
		$("#teachClassRow_"+currentRow).show();
		$("#currentClassRow").val(currentRow);
	}
	function deleteClass(recordID,batchID,rowID){
		$("#hiddenClass_"+rowID).val("INACTIVE");
		$("#teachClassRow_"+rowID).hide();
	}
	function addForm(recordID,batchID){
		var currentRow = parseInt($("#currentFormRow").val());
		currentRow = currentRow+1;
		$("#teachFormRow_"+currentRow).show();
		$("#currentFormRow").val(currentRow);
	}
	function deleteForm(recordID,batchID,rowID){
		$("#hiddenForm_"+rowID).val("INACTIVE");
		$("#teachFormRow_"+rowID).hide();
	}
	function addCrsSemWs(recordID,batchID){
		var currentRow = parseInt($("#currentCrsSemWsRow").val());		
		currentRow = currentRow+1;
		$("#crsSemWsRow_"+currentRow).show();
		$("#currentCrsSemWsRow").val(currentRow);	
	}
	function deleteCrsSemWs(recordID,batchID,rowID){
		$("#hiddenCrsSemWsSts_"+rowID).val("INACTIVE");
		$("#crsSemWsRow_"+rowID).hide();
	}
	function goSignatureSubmit(){
		var rlsNo = $("#rlsNo").val();
		var recordID = $("#recordID").val();
		var batchID = $("#batchID").val();
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		var typeVal="formSignature";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		$('#form1').append($(type)).append($(returnPath));;
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
	}
	function goSubmit(){
		var status=formValidation();
		if (status== true){
			var rlsNo = $("#rlsNo").val();
			var recordID = $("#recordID").val();
			var batchID = $("#batchID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			var typeVal="formSave";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
		}		
	}
	function goBack(){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>view_list';
	}
	function formValidation(){
		return true;
	}
	$("#totClsCnt,#totFormCnt").change(function(e){
		var id = e.target.id;
		if(id=="totFormCnt"){
			var clsCnt = $("#totFormCnt").val();
			$("#totClsCnt").val(clsCnt);
		}
		else if(id=="totClsCnt"){
			var clsCnt = $("#totClsCnt").val();
			$("#totFormCnt").val(clsCnt);
		}
	});
	function goModify(){
		var cycleStart=$("#cycleStart").val();
		var cycleClose=$("#cycleClose").val();
		var editPrdFr=$("#editPrdFr").val();
		var editPrdTo=$("#editPrdTo").val();
		var modDateFr=$("#modDateFr").val();
		var modDateTo=$("#modDateTo").val();
		var modRemark=$("#modRemark").val();
		var status = true;
		$("#ModDateOutCycleWarnDiv").hide();
		$("#ModDateInvalidRangeWarnDiv").hide();
		if(modDateFr<cycleStart || modDateTo>cycleClose){
			$("#ModDateOutCycleWarnDiv").show();
			status = false;
		}
		else if(modDateTo<modDateFr){
			$("#ModDateInvalidRangeWarnDiv").show();
			status = false;
		}
		else if(modDateFr<editPrdFr || modDateTo>editPrdTo){
			$("#ModDateOutEditWarnDiv").show();
			status = false;
		}
		if (status== true){
			var rlsNo = $("#rlsNo").val();
			var recordID = $("#recordID").val();
			var batchID = $("#batchID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>list';	
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			var typeVal="modifySave";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
		}	
	}
	//-------------------------temp-------------------------//
	function js_Init_JEdit()
	{
		/************ Add/Edit Building Info ************/
		$('div.jEditQualiDescr').editable( 
			function(updateValue, settings) 
			{
				var thisDivID = $(this).attr("id");
				var idArr = thisDivID.split('_');
				var divID = idArr[0];
				var targetID = idArr[1];				
				updateValue = Trim(updateValue);	
				//alert(updateValue);
				if ($('#QualiDescr_Warning_Div_' + targetID).is(':visible') || updateValue=='' || $(this)[0].revert == updateValue)
				{
					// not vaild
					$('#QualiDescr_Warning_Div_' + targetID).hide(jsShowHideSpeed);
					$(this)[0].reset();
				}
				else
				{
					js_Reload_Learning_Category_Add_Edit_Table();
					//alert(updateValue);
					
					//$("#"+thisDivID).text(updateValue);
					//return updateValue;
					//js_Reload_Learning_Category_Add_Edit_Table();
					//js_Update_Learning_Category(thisDivID, "NameEng", updateValue);
				}			
			},
			{
				tooltip   : "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
				event : "click",
				onblur : "submit",
				type : "text",     
				style  : "display:inline",
				height: "20px"
			}
		);
	}	
	$('div.jEditQualiDescr').click( function() {
		var inputValue = $('form input').val();
		//var inputValue = $(this).attr('id').val();	
		var id = $(this).attr('id');
		var inputValue = $("#"+id).text();
		var idArr = $(this).attr('id').split('_');
		var divID = idArr[0];
		var selfID = idArr[1];
		var recordType = '';
		//alert(inputValue);
		js_LC_Code_Validation("LC_QualiDescr", inputValue, selfID, 'QualiDescr_Warning_Div_'+selfID);
	});
	function js_LC_Code_Validation(RecordType, InputValue, SelfID, WarningDivID)
	{
		//alert(InputValue);
		if (Trim(InputValue) == '')
		{
			$('#' + WarningDivID).html('<?=$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning']?>');
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			/*$.post(
				"ajax_validate_learning_category_code.php", 
				{
					InputValue: InputValue,
					SelfID: SelfID
				},
				function(ReturnData)
				{
					if (ReturnData == "")
					{
						// valid code
						$('#' + WarningDivID).hide(jsShowHideSpeed);
					}
					else
					{
						// invalid code => show warning
						$('#' + WarningDivID).html(ReturnData);
						$('#' + WarningDivID).show(jsShowHideSpeed);
						
						//$('#debugArea').html('aaa ' + ReturnData);
					}
				}
			);*/
		}
	}
	function js_Reload_Learning_Category_Add_Edit_Table() {
		/*$.post(
			"ajax_reload_content.php", 
			{ RecordType: "LearningCategoryTable" },
			function(ReturnData)
			{
				$('#LearningCategoryInfoSettingLayer').html(ReturnData);
				js_Init_JEdit();
				js_Init_DND_Table();
				
				//$('#debugArea').html(ReturnData);
			}
		);*/
		$('div.jEditQualiDescr').editable('disable');
	}
	function goPrint(){
		var url = '?task=reports<?=$appraisalConfig['taskSeparator']?>personal_result<?=$appraisalConfig['taskSeparator']?>print_from_view&rlsNo='+$("#rlsNo").val()+'&recordID='+$("#recordID").val()+'&batchID='+$("#batchID").val();
		newWindow(url,35);	
	}
	function goViewRelated(templateID){
		var url = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_records<?=$appraisalConfig['taskSeparator']?>view_related_forms&rlsNo='+$("#rlsNo").val()+'&recordID='+$("#recordID").val()+'&batchID='+$("#batchID").val()+'&viewTemplateID='+templateID;
		newWindow(url,35);	
	}
</script>