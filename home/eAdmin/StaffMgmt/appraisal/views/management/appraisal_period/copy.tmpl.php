<form name="form1" id="form1" method="POST" onsubmit="return validateForm()" action="?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>copy_update">
	<div>
		<?=$htmlAry['navigation']?>
	</div>
	<div>
		<?=$htmlAry['contentTbl1']?>
	</div>	
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$Btn?>
		<p class="spacer"></p>
	</div>
</form>


<script>
function validateForm(){
	if($('#CycleID').val()==""){
		alert('<?php echo $Lang['Appraisal']['Message']['SelectAppraisalScheme'];?>');
		return false;
	}
	if($('#AcademicYearID').val()==""){
		alert('<?php echo $Lang['Appraisal']['Message']['SelectAcademicYear'];?>');
		return false;
	}
	var cycleStart = $("#CycleStart").val();
	var cycleClose = $("#CycleClose").val();
	if(cycleStart>cycleClose){
		alert('<?php echo $Lang['Appraisal']['Cycle']['DateCheckOverPeriod'];?>');
		return false;
	}
	return true;
}
function goBack(){
	document.form1.action = "?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>list";
	document.form1.method = "POST";
	document.form1.submit();
}
</script>