<style>
    #progress {
      display: none;	
      border: 1px solid #aaa;
      height: 20px;
    }
    #progress .bar {
      background-color: #ccc;
      height: 20px;
    }
    #progress .label {
      position: absolute;
	  left: 49%;
    }
</style>
<form id="form1" name="form1" method="post" action="index.php" enctype="multipart/form-data">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['rlsAppFormBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['backBtn']?>
			<div id="progress">0%</div> 			
			<p class="spacer"></p>
		</div>	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>			
			<br style="clear:both;">
		</div>	
		<div>
			<?=$htmlAry['contentTbl2']?>
		</div>
		<div class="edit_bottom_v30">
			<?=$htmlAry['rlsObsFormBtn']?>
		</div>
	</div>
</form>

<script type="text/javascript">
var timer;
var timerAlert;
$(document).ready( function() {
	initDndTable();
});
function initDndTable() {
	$("table#dndTable").tableDnD({
		onDrop: function(table, row) {
	        var rows = table.tBodies[0].rows;
	        var recordOrder = "";
	        var displayOrder = "";
	        for (var i=0; i<rows.length; i++) {
		        var j=i+1;
	        	if (rows[i].id != ""){
	        		recordOrder += rows[i].id+",";
        			displayOrder += j+",";
	        	}
	        }	        
	        //alert(recordOrder+" "+displayOrder);
	        reorderAjax = GetXmlHttpObject();
			  if (reorderAjax == null)
			  {
			    alert (errAjax);
			    return;
			  } 
			  var CycleID = $("#CycleID").val();
			  var ElementObj = $(this);
			  var url = 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajaxgetData';
			  var postContent = "recordOrder="+encodeURIComponent(recordOrder)+"&displayOrder="+encodeURIComponent(displayOrder)+"&cycleID="+CycleID+"&Type=Templates";				
				reorderAjax.onreadystatechange = function() {
					if (reorderAjax.readyState == 4) {
						//Get_Form_Class_List();
					}
				};
				reorderAjax.open("POST", url, true);
				reorderAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
				reorderAjax.send(postContent);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
function changeDatePicker(id){	
}
function goSubmit(){	
	var status=formValidation();
	if (status== true){
		var academicYearID = $("#AcademicYearID :selected").val();
		var cycleID = $("#CycleID").val();
		var cycleStart = $("#CycleStart").val();
		var cycleClose = $("#CycleClose").val();
		var data = "Type=CheckAcacdemic&academicID="+academicYearID+"&cycleID="+cycleID+"&cycleStart="+cycleStart+"&cycleClose="+cycleClose;		
		$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_checkData', 
	        data: data,
            success: function (data, textStatus, jqXHR) {			
            	var result = eval('(' + data + ')');
            	if(result[0]["result"] == 1 && result[0]["cycleID"] == 1){
            		$("#AcademicYearIDEmptyWarnDiv").hide();
					var msg = "* "+"<?=$Lang['Appraisal']['Cycle']['ExistAcademic']?>";
					$("#AcademicYearIDEmptyWarnDiv span").text(msg);
					$("#AcademicYearIDEmptyWarnDiv").show();
            	} else if(result[0]["result"] == 2){
            		$("#DateWarnDiv2").show();
            	}
            	else{
					var cycleID=($("#CycleID").val()=="")?"%s":$("#CycleID").val();
					var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;		
					var typeVal="InfoSave";
					var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
					var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
					$('#form1').append($(type)).append($(returnPath));
					$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
            	}

            }, error: function (jqXHR, textStatus, errorThrown) {
	               
            }
		});
	}
}
function formValidation(){
	var status = true;
	$("#AcademicYearIDEmptyWarnDiv").hide();
	var cycleStart = $("#CycleStart").val();
	var cycleClose = $("#CycleClose").val();
	$("#DateWarnDiv").hide();
	$("#DateWarnDiv2").hide();
	if($("#AcademicYearID :selected").val()==""){
		var emptyMsg = "* "+"<?=$Lang['Appraisal']['TemplateSample']['Mandatory']?>";
		$("#AcademicYearIDEmptyWarnDiv span").text(emptyMsg);
		$("#AcademicYearIDEmptyWarnDiv").show();
		status = false;
	}
	else if(cycleStart>cycleClose){
		$("#DateWarnDiv").show();
		status = false;
	}
	return status;
}
function goReset(){
	$("#form1")[0].reset();
}
function goEditArr(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_arr&cycleID='+cycleID;
}
function goBack(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>list';
}
function goReleaseReport(){
	var cycleID = $("#CycleID").val();
	var typeVal="BtnReportRelease";
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;	
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(returnPath)).append($(type));
	var confirmMessage = "<?=$Lang['Appraisal']['Message']['ConfirmReleaseReport']?>";
	var r = confirm(confirmMessage);
	if(r == true){
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
	}
}
function goDelete(secID){
	var typeVal="SDFDelete";
	var type = $("<input/>").attr("type", "hidden").attr("name", "Type").val(typeVal);
	var secID= $("<input/>").attr("type", "hidden").attr("name", "Status").val(secID);
	$('#form1').append($(type)).append($(secID));
	$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>templates<?=$appraisalConfig['taskSeparator']?>delete').submit();	
}
function goEditAcceptTemplate(cycleID,templateID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_arr&cycleID='+cycleID+"&templateID="+templateID;
}
function goAddStfGrp(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_grp_arr&cycleID='+cycleID+"&grpID=";
}
function goEditStfGrp(cycleID,groupID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_grp_arr&cycleID='+cycleID+"&grpID="+groupID;
}
function goAddStfIdv(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_idv_arr&cycleID='+cycleID+"&idvID=";
}
function goEditStfIdv(cycleID,idvID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_idv_arr&cycleID='+cycleID+"&idvID="+idvID;
}
function goImportLeave(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_import_leave_1&cycleID='+cycleID;
}
function goImportAbsLateSub(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_1&cycleID='+cycleID;
}
function goAddTemplate(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_arr&cycleID='+cycleID+"&templateID=";
}
function goDownloadLeave(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>export_leave_record&cycleID='+cycleID;
}
function goDownloadAbsence(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>export_absLateSub_record&cycleID='+cycleID; 
}
function goDownloadAttachment(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>view_attachment&cycleID='+cycleID; 
}
function goRlsAppForm(){
	var cycleID = $("#CycleID").val();
	var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;	
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(returnPath));
	var confirmMessage = "<?=$Lang['Appraisal']['BtbConRlsCycle']?>";
	var r = confirm(confirmMessage);
	if(r == true){
		$.ajax({
			type: "POST",
			url: 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>release_appraisal_form',
			data: $('form#form1').serialize()
		});
		timer = window.setInterval(refreshProgress, 1000);
		$.blockUI({ message: $("#progress")});  
		//$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>release_appraisal_form').submit();
	}
}
function goDeleteIdv(cycleID,idvIDVal){
	var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
	var typeVal="Idv";	
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var idvID = $("<input/>").attr("type", "hidden").attr("name", "idvID").val(idvIDVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(idvID));
	$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>delete').submit();			
}
function goDeleteGrp(cycleID,grpIDVal){
	var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
	var typeVal="Grp";	
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var grpID = $("<input/>").attr("type", "hidden").attr("name", "grpID").val(grpIDVal);
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(type)).append($(returnPath)).append($(grpID));
	$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>delete').submit();			
}
function goDeleteArr(cycleID,templateIDVal){
	if(confirm("<?=$Lang['Appraisal']['Message']['ConfirmRemoveFormFromCycle']?>")){
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
		var typeVal="Arr";	
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath)).append($(templateID));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>delete').submit();			
	}
}
function goDeleteObsArr(cycleID,templateIDVal){
	if(confirm("<?=$Lang['Appraisal']['Message']['ConfirmRemoveFormFromCycle']?>")){
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
		var typeVal="ObsArr";	
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var templateID = $("<input/>").attr("type", "hidden").attr("name", "templateID").val(templateIDVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath)).append($(templateID));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>delete').submit();			
	}
}
function goAddObs(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_obs_arr&cycleID='+cycleID+"&obsID=";
}
function goRlsObsAppForm(){
	var cycleID = $("#CycleID").val();
	var typeVal="RlsObsFrm";	
	var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
	var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;	
	var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
	$('#form1').append($(returnPath)).append($(type));
	var confirmMessage = "<?=$Lang['Appraisal']['BtbConRlsCycleObs']?>";
	var r = confirm(confirmMessage);
	if(r == true){
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
	}
}
/*
function goDownloadAttachment(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>view_attachment&cycleID='+cycleID; 
}*/
function refreshProgress() {
  $.ajax({
    url: "controllers/management/appraisal_period/progress_control.php?file=<?php echo "teacherAppraisal".session_id() ?>",
    success:function(result){
      var data = JSON.parse(result);	
      $("#progress").html('<div class="label">'+ data.message +'</div>'+'<div class="bar" style="width:' + data.percent + '%"></div>');
      if (data.percent == 100) {
        window.clearInterval(timer);
        timer = null;
        timerAlert = window.setTimeout(refreshProgressCompleted, 1000);		         
      }
    }
  });
}
function refreshProgressCompleted(){
	window.clearTimeout(timer);
	$('#progress').css("display","none");
    $.unblockUI();
    $("#progress").remove();    
 	if(!alert("<?=$Lang['Appraisal']['Message']['Released']?>")){	
		window.location.reload();
 	} 
}
</script>