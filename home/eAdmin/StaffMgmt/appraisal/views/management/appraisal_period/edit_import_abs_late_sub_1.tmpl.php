<form id="form1" name="form1" method="post" action="index.php" enctype="multipart/form-data">
	<?=$htmlAry['msg']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>
<script type="text/javascript">
$(document).ready( function() {
});

function goSubmit(){	
	$("#errMsg").hide();
	var status=formValidation();
	if (status== true){
		var cycleID=$("#CycleID").val();
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_import_abs_late_sub_2&cycleID='+cycleID).submit();
	}
	else{
		$("#errMsg").show();
	}
}

function formValidation(){
	var status = true;
	var fileVal = $("#ImportAbsLateSub").val();
	if(fileVal==""){
		status = false;
	}
	return status; 
}

function goExportAbsLateSub(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>export_absLateSub';
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}
function goBack(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}

</script>