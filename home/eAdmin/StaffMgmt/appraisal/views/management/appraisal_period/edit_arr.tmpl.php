<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<form id="form1" name="form1" method="post" action="index.php">

	<?=$htmlAry['navigation']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<?=$htmlAry['reOpenBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>	
		<div class="content_top_tool">
			<?=$htmlAry['contentTool']?>			
			<br style="clear:both;">
		</div>	
		<div>
			<?=$htmlAry['contentTbl2']?>
		</div>
	</div>
</form>

<script type="text/javascript">
var autoCompleteObj;
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	// clone initial Special set to Sample //
	$( ".initialSpecialSet" ).children().clone().appendTo( ".sampleSpecialSet" );
	$( ".sampleSpecialSet" ).find("input[type='hidden']").remove();
	$( ".sampleSpecialSet" ).find("span[id^='UserName_1_']").text("");
	$( ".sampleSpecialSet" ).find("div[id^='delete_1_']").hide();
	$( ".sampleSpecialSet" ).find("tr[id^='SecRow_1_']").hide();
	$( ".sampleSpecialSet" ).find("tr[id ='SecRow_1_0']").show();
	$( '<input type=\"hidden\" id=\"MaxSecID_0\" name=\"MaxSecID_0\" value="0">' ).appendTo('.sampleSpecialSet');
	$( '<input type=\"hidden\" id=\"RowIDStr_1\" name=\"RowIDStr_1\" value="0">' ).appendTo('.sampleSpecialSet');
	$( '<input type=\"hidden\" id=\"specialSetNo_1\" name=\"specialSetNo_1\" value="">' ).appendTo('.sampleSpecialSet');

});

function changeDatePicker(id){
	
}

function goSubmit(){	
	var status=formValidation();
	if (status== true){		
		setSpecialSetUserIDStr();
		var totalSpecialSetName = "";
		for(i=1 ;i <= parseInt($("#TotalSpecialSet").val())+1 ; i++){
			totalSpecialSetName += $( "#newSpecialSetTitle" + i ).val() + ",";
		}
		totalSpecialSetName = totalSpecialSetName.slice(0,-1);
		$("#SpecialSetName").val(totalSpecialSetName);
		var cycleID=$("#CycleID").val();
		var templateID=($("#TemplateID").val()=="")?"%s":$("#TemplateID").val();
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_arr&cycleID='+cycleID+"&templateID="+templateID;		
		var typeVal="ArrSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		$( ".sampleSpecialSet" ).html('');
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
}
function formValidation(){
	var status = true;
	var userList = [];
	//var dateRowIDArr=$("#SecIDStr").val().split(",");
	for(var setNo=1 ;setNo <= parseInt($("#TotalSpecialSet").val())+1 ; setNo++){
		if($("#SecIDStr_"+setNo).length > 0){
			var secIDArr=$("#SecIDStr_"+setNo).val().split(",");
		}else{
			var secIDArr = [];
		}
		var cycleStart=$("#CycleStart").val();
		var cycleClose=$("#CycleClose").val();
		var secID="";
		$("#EditDuplicatedUserDiv_"+setNo).hide();		
		if($("#RowIDStr_"+setNo).length > 0){
			var dataRowIDArr=$("#RowIDStr_"+setNo).val().split(",");
			for(i=0;i<dataRowIDArr.length;i++){
				$("#EditPrdEmptyWarnDiv_"+setNo+"_"+i).hide();
				$("#EditPrdOutCycleWarnDiv_"+setNo+"_"+i).hide();
				$("#NoRoleWarnDiv_"+setNo+"_"+i).hide();
			}
			for(i=0;i<dataRowIDArr.length;i++){
				appRole=$("#SecAppRoleID_"+setNo+"_"+dataRowIDArr[i]).val();
				strDat=$("#EditPrdFr_"+setNo+"_"+dataRowIDArr[i]).val();
				endDat=$("#EditPrdTo_"+setNo+"_"+dataRowIDArr[i]).val();
				if(appRole=="" || typeof appRole == "undefined"){
					if(secIDArr[i] != "NULL" && $("#SecRow_"+setNo+"_"+dataRowIDArr[i]).is(":visible")==true){
						$("#NoRoleWarnDiv_"+setNo+"_"+dataRowIDArr[i]).show();
						status = false;
					}
				}
				if(secIDArr[i] != "NULL" && (strDat<cycleStart || endDat>cycleClose)){
					$("#EditPrdOutCycleWarnDiv_"+setNo+"_"+dataRowIDArr[i]).show();
					status = false;
				}
				else if(secIDArr[i] != "NULL" && (endDat < strDat)){
					$("#EditPrdInvalidRangeWarnDiv_"+setNo+"_"+dataRowIDArr[i]).show();
					status = false;
				}
				else{
					for(j=i+1;j<dataRowIDArr.length;j++){
						if(secIDArr[i] != "NULL"){
							strDatCp=$("#EditPrdFr_"+setNo+"_"+dataRowIDArr[j]).val();
							endDatCp=$("#EditPrdTo_"+setNo+"_"+dataRowIDArr[j]).val();				
							if(endDat>=strDatCp && strDat<=endDatCp && secIDArr[j]!="NULL" && secIDArr[i]==secIDArr[j]){
								$("#EditPrdEmptyWarnDiv_"+setNo+"_"+dataRowIDArr[i]).show();
								$("#EditPrdEmptyWarnDiv_"+setNo+"_"+dataRowIDArr[j]).show();
								status = false;
							}
						}
					}
				}
			}
		}
		if($("#special_set_users_"+setNo+" option").length > 0){
			for(j=0;j<$("#special_set_users_"+setNo+" option").length;j++){
				memberID = $("#special_set_users_"+setNo+" option").eq(j).val();
				memberID = memberID.replace("U","").replace("G","");
				var inOtherList = jQuery.inArray(memberID, userList);
				if(inOtherList > -1 && memberID != ""){
					$("#EditDuplicatedUserDiv_"+setNo).show();
					status = false;
				}else{
					userList.push(memberID);
				}
			}
		}
	}	
	return status;
}
function goReset(){
	$("#form1")[0].reset();
}
function goEditArr(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_arr&cycleID='+cycleID;
}
function goBack(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>list';
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}
function goDelete(userID){
	var idArr=userID.split('_');
	var specialSetNo=idArr[1];	
	var idIdx=idArr[2];	
	$("#UserName_" + specialSetNo + "_" + idIdx).text("");
	$("#UserID_" + specialSetNo + "_" + idIdx).val("");
	$("#delete_" + specialSetNo + "_" + idIdx).hide();
}
$("#DDLTemplateID").change(function(){
	var ddlTemplateID=$("#DDLTemplateID :selected").val();
	$("#TemplateID").val(ddlTemplateID);
});

function goAddBatch(specialSetNo){
	var maxSecID=$("#MaxSecID_"+ specialSetNo).val();
	if(maxSecID==""){
		maxSecID = 0; 
	}
	else{
		maxSecID=parseInt(maxSecID)+1;
	}
	$("#MaxSecID_"+ specialSetNo).val(maxSecID);
	$("#SecRow_" + specialSetNo + "_" + maxSecID).show();

	var secIDStr="";
	var rowIDStr="";
	for(i=0;i<=maxSecID;i++){
		var secID=$("#SecTitleID_"+specialSetNo+"_"+i+" :selected").val();		
		if($("#SecRow_"+specialSetNo+"_"+i).is(":visible")==true){
			if(secIDStr==""){
				secIDStr=secID;
				rowIDStr=i;
			}
			else{
				secIDStr=secIDStr+","+secID;
				rowIDStr=rowIDStr+","+i;
			}
		}
		else{	
			if(secIDStr==""){
				secIDStr="NULL";
				rowIDStr=i;
			}
			else{
				secIDStr=secIDStr+","+"NULL";
				rowIDStr=rowIDStr+","+i;
			}
		}	
	}
	$("#RowIDStr_"+specialSetNo).val(rowIDStr);
	$("#SecIDStr_"+specialSetNo).val(secIDStr);
	bindBatchChange(specialSetNo);
}

function goDeleteBatch(rowID,batchID,specialSetNo){
	var delBatchID=$("#DelBatchIDStr").val();
	if(delBatchID==""){
		$("#DelBatchIDStr").val(batchID);
	}
	else{
		delBatchID=delBatchID+","+batchID;
		$("#DelBatchIDStr").val(delBatchID);
	}
	$("#SecRow_" + specialSetNo + "_" +rowID).hide();

	var secIDStr="";
	var rowIDStr="";
	var maxSecID=$("#MaxSecID_"+specialSetNo).val();
	$("#SecIDStr_" + specialSetNo).val("");
	$("#RowIDStr_"+specialSetNo).val("");
	for(i=0;i<=maxSecID;i++){
		var secID=$("#SecTitleID_"+specialSetNo+"_"+i+" :selected").val();		
		if($("#SecRow_"+specialSetNo+"_"+i).is(":visible")==true){
			if(secIDStr==""){
				secIDStr=secID;
				rowIDStr=i;
			}
			else{
				secIDStr=secIDStr+","+secID;
				rowIDStr=rowIDStr+","+i;
			}
		}
		else{	
			if(secIDStr==""){
				secIDStr="NULL";
				rowIDStr=i;
			}
			else{
				secIDStr=secIDStr+","+"NULL";
				rowIDStr=rowIDStr+","+i;
			}
		}
	}
	$("#SecIDStr_" + specialSetNo).val(secIDStr);
	$("#RowIDStr_"+ specialSetNo).val(rowIDStr);
}

function goReOpen(){
	if(confirm('<?=$Lang['Appraisal']['Message']['ConfirmReOpenThisForm']?>\n<?=$Lang['Appraisal']['Message']['ConfirmReOpenThisFormCaution']?>')){	
		var status=formValidation();
		if (status== true){
			var cycleID=$("#CycleID").val();
			var templateID=($("#TemplateID").val()=="")?"%s":$("#TemplateID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_arr&cycleID='+cycleID+"&templateID="+templateID;		
			var typeVal="FormReOpen";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
		}
	}
}

var specialSetNo = parseInt($("#TotalSpecialSet").val())+1;

function clonefunction(specialSetNo){
	$("input[id^='LoginID_" + specialSetNo + "_']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					var idArr=elemId.split('_');
					var idIdx=idArr[2];					
					$("#UserName_"  + specialSetNo + "_" + idIdx).text(li.extra[1]);
					$("#UserID_" + specialSetNo + "_" + idIdx).val(li.extra[0]);
					$("#"+elemId).val("");
					$("#delete_" + specialSetNo + "_" + idIdx).show();
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});
	var existRecords = $("#ExistRecords").val();
	var sectionSize = $("#SectionSize").val();
	if(existRecords == 0){
		//for(index=0;index<sectionSize;index++){
		//	goAddBatch(specialSetNo);
		//}
	}	
	bindBatchChange(specialSetNo);	
}

function bindBatchChange(specialSetNo){	
	$("select[id^='UserSel_" + specialSetNo + "_']").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).change(function(){
			var obj = $(this);
			var value = $(this).val();
			var text = $("option:selected",this).text();
			var idArr = elemId.split('_');
			var idIdx = idArr[2];	
			$(this).hide();
			$('#LoadingPlace_' + specialSetNo + '_' + idIdx).html(ajaxImage);
			setTimeout(function(){
				$("#UserName_" + specialSetNo + "_"+idIdx).text(text);
				$("#UserID_" + specialSetNo + "_" + idIdx).val(value);
				$("#"+elemId).val("");
				$("#delete_" + specialSetNo + "_" + idIdx).show();
				$('#LoadingPlace_'  + specialSetNo + '_' + idIdx).html('');
				obj.show();
			},750);
		});		
	});
	$("select[id^='SecTitleID_" + specialSetNo + "_']").change(function(e){
		var id=e.target.id;
		var idArr=id.split('_');
		var thisSecID=$("#"+id+" :selected").val();
		var secIDStr="";
		$("#SecIDStr_" + idArr[1]).val("");
		var maxSecID=$("#MaxSecID_" + idArr[1]).val();
		for(i=0;i<=maxSecID;i++){
			var secID=$("#SecTitleID_" + idArr[1] + "_" + i + " :selected").val();		
			if($("#SecRow_" + idArr[1] + "_"+i).is(":visible")==true){
				if(secIDStr==""){
					secIDStr=secID;
				}
				else{
					secIDStr=secIDStr+","+secID;
				}
			}
			else{
				if(secIDStr==""){
					secIDStr="NULL";
				}
				else{
					secIDStr=secIDStr+","+"NULL";
				}
			}
		}
		$("#SecIDStr_" + idArr[1]).val(secIDStr);
		/*
		$("#NoRecordKeepWarnDiv_" + idArr[1] + "_" + idArr[2]).hide();
		$("#NoRecordKeepWarnDiv_" + idArr[1] + "_" + idArr[2]).show();
		*/
		
		/* To control the choices of the Filler*/
		$.post(
			"?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData",
			{
				"Type":"GetSectionAvailableEditRole",
				"SpecialSetNo":idArr[1],
				"SectionID":thisSecID,
				"Index": idArr[2]
			},
			function(response){
				$("#SecAppRoleID_" + idArr[1] + "_"+idArr[2]).parent().html(response);
			}	
		)
		
	});
	$("select[id^='SecAppRoleID_"+specialSetNo+"_']").change(function(e){
		var id=e.target.id;
		var idArr=id.split('_');
		$("#NoRecordKeepWarnDiv_" + idArr[1] + "_" +idArr[2]).hide();
		$("#NoRecordKeepWarnDiv_" + idArr[1] + "_" +idArr[2]).show();
	});
}

for(var m=1; m<=specialSetNo; m++){
	clonefunction(m);
}

function goAddSpecialSet(){
	/***** Clone Sample *****/
	specialSetNo = specialSetNo + 1;
	$( ".newSpecialSetList" ).css( "display", "block" );
	$( ".sampleSpecialSet" ).clone()
		.appendTo( ".newSpecialSetList" )
		.attr("id", "newSpecialSet" + specialSetNo)
		.attr("class", "newSpecialSet")
		.css("display", "block" );

	/***** Add new div for New Special Form Order *****/
	$('<input type="hidden" id="set_order" name="set_order" value="' + specialSetNo + '">').appendTo('#newSpecialSet' + specialSetNo);
	
	/***** New Special Form title input *****/
	$( "#newSpecialSet"+specialSetNo ).find(".title")
									  .html( "<p>#" + (specialSetNo-1) + ": " + "<?= $Lang['Appraisal']['CycleTemplate']['SpecialFormSettingName'] ?>" + ": <input id=\"newSpecialSetTitle" + specialSetNo +  "\"type=\"text\" required></p>");

	/***** Special Role (Choose & selected) *****/
	var j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("select[id^='UserSel_1_']").each(function(){
		$(this).attr("id", "UserSel_" + specialSetNo + "_" + j)
			   .attr("name", "UserSel_" + specialSetNo + "_" + j)
		j++;
	});

	j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("span[id^='UserName_1_']").each(function(){
		$(this).parent().parent().append( '<input type=\"hidden\" id=\"UserID_' + specialSetNo + '_' + j + '\" name=\"UserID_' + specialSetNo + '_' + j + '\" value="">' );
		$(this).attr("id", "UserName_" + specialSetNo + "_" + j)
			   .attr("name", "UserName_" + specialSetNo + "_" + j);
		j++;
	});

	j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("span[id^='LoadingPlace_1_']").each(function(){
		$(this).attr("id", "LoadingPlace_" + specialSetNo + "_" + j)
			   .attr("name", "LoadingPlace_" + specialSetNo + "_" + j)
		j++;
	});

	j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("div[id^='delete_1_']").each(function(){
		$(this).attr("id", "delete_" + specialSetNo + "_" + j)
			   .attr("name", "delete_" + specialSetNo + "_" + j)
			   .children().attr("onClick", "javascript:goDelete('UserName_" + specialSetNo + "_" + j + "')")
		j++;
	});

	j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("input[id^='UserID_1_']").each(function(){
		$(this).attr("id", "UserID_" + specialSetNo + "_" + j)
			   .attr("name", "UserID_" + specialSetNo + "_" + j)
		j++;
	});

	$( "#newSpecialSet"+specialSetNo ).find("div[id='special_set_select_users_div_1']").each(function(){
		$(this).attr("id", "special_set_select_users_div_" + specialSetNo);
		$(this).css('display','block');
		$('.select_studentlist',this).attr("id", "special_set_users_"+specialSetNo).attr("name", "special_set_users_"+specialSetNo);
	});
	$( "#newSpecialSet"+specialSetNo ).find("input[id='selectBtn_1']").each(function(){
		$(this).attr("id", "selectBtn_" + specialSetNo);
		$(this).attr("name", "selectBtn_" + specialSetNo);
		$(this).removeAttr("onclick");
		$(this).attr('onClick', 'goAddSpecialSetUsers('+specialSetNo+');');
	});
	$( "#newSpecialSet"+specialSetNo ).find("input[id='submitBtn_1']").each(function(){
		$(this).attr("id", "submitBtn_" + specialSetNo);
		$(this).attr("name", "submitBtn_" + specialSetNo);
		$(this).removeAttr("onclick");
		$(this).attr('onClick', 'goDeleteSpecialSetUsers('+specialSetNo+');');
	});
	$( "#newSpecialSet"+specialSetNo ).find("div[id='EditDuplicatedUserDiv_1']").each(function(){
		$(this).attr("id", "EditDuplicatedUserDiv_" + specialSetNo);
	});

	/***** Access Right Section(s) *****/
	/***** 21 hidden Batch Rows *****/
	$(".textboxnum").removeClass('hasDatepick')
	    .removeData('datepicker')
	    .unbind();
	$(".datepick-trigger").remove();
	for(i=0;i<=21;i++){
		$( "#newSpecialSet"+specialSetNo  ).find("#SecRow_1_" + i).attr("id", "SecRow_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#delete_dim_1_" + i).attr("id", "delete_dim_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#delete_dim_" + specialSetNo + "_" + i).attr("onClick", "goDeleteBatch('" + i + "','','" + specialSetNo + "')");
		$( "#newSpecialSet"+specialSetNo  ).find("#EditPrdFr_1_" + i).attr("id", "EditPrdFr_" + specialSetNo + "_" + i)
															  		 .attr("name", "EditPrdFr_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#EditPrdTo_1_" + i).attr("id", "EditPrdTo_" + specialSetNo + "_" + i)
															  		 .attr("name", "EditPrdTo_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#NoRoleWarnDiv_1_" + i).attr("id", "NoRoleWarnDiv_" + specialSetNo + "_" + i)
															  		 .attr("name", "NoRoleWarnDiv_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#EditPrdEmptyWarnDiv_1_" + i).attr("id", "EditPrdEmptyWarnDiv_" + specialSetNo + "_" + i)
															  		 .attr("name", "EditPrdEmptyWarnDiv_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#EditPrdOutCycleWarnDiv_1_" + i).attr("id", "EditPrdOutCycleWarnDiv_" + specialSetNo + "_" + i)
															  		 .attr("name", "EditPrdOutCycleWarnDiv_" + specialSetNo + "_" + i);
		$( "#newSpecialSet"+specialSetNo  ).find("#EditPrdInvalidRangeWarnDiv_1_" + i).attr("id", "EditPrdInvalidRangeWarnDiv_" + specialSetNo + "_" + i)
															  		 .attr("name", "EditPrdInvalidRangeWarnDiv_" + specialSetNo + "_" + i);
		$("#SecRow_" + specialSetNo + "_" + i).append('<input type="hidden" id="BatchID_'+ specialSetNo + '_' + i + '" name="BatchID_'+ specialSetNo + '_' + i + '" value="">');
		
		}
	
	/***** GoAddBatch button *****/
	$( "#newSpecialSet"+specialSetNo  ).find("[specialSetNo=1]").attr("specialSetNo", specialSetNo);
	$( "#newSpecialSet"+specialSetNo  ).find("[specialSetNo=" + specialSetNo + "]").attr("href", "javascript: goAddBatch(" + specialSetNo + ");");

	/***** Max Number of Batch *****/
	$( "#newSpecialSet"+specialSetNo  ).find("#MaxSecID_0").attr("name", "MaxSecID_" + specialSetNo )
											 			   .attr("id", "MaxSecID_" + specialSetNo );
	/***** Records of Row *****/
	$( "#newSpecialSet"+specialSetNo  ).find("#RowIDStr_1").attr("name", "RowIDStr_" + specialSetNo )
											 			   .attr("id", "RowIDStr_" + specialSetNo );
		
	/***** Period (datepick) *****/
	$(".textboxnum").datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0,
			onSelect: function(dateText, inst) {
			Date_Picker_Check_Date_Format_CanEmpty($(this)[0],'DPWL-' + $(this).attr('id'),'');changeDatePicker('EditPrdFr')
				}
	});

	/*****	Access Right (Section and Fill in by) *****/
	j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("select[id^='SecAppRoleID_1_']").each(function(){
		$(this).attr("id", "SecAppRoleID_" + specialSetNo + "_" + j)
			   .attr("name", "SecAppRoleID_" + specialSetNo + "_" + j)
		j++;
	});

	j = 0;
	$( "#newSpecialSet"+specialSetNo ).find("select[id^='SecTitleID_1_']").each(function(){
		$(this).attr("id", "SecTitleID_" + specialSetNo + "_" + j)
			   .attr("name", "SecTitleID_" + specialSetNo + "_" + j).val('');
		j++;
	});
	
	clonefunction(specialSetNo);
	/***** Hidden Value *****/
	$("#TotalSpecialSet").val(specialSetNo);
	$("#newSpecialSet"+specialSetNo).append( "<input type=\"hidden\" id=\"SecIDStr_" + specialSetNo + "\" name=\"SecIDStr_" + specialSetNo + "\" value=\"\">" );
	$("select[id^='SecTitleID_" + specialSetNo + "_']").change();
}

function goAddSpecialSetUsers(specialSetNo){
	newWindow('/home/common_choose/index.php?fieldname=special_set_users_'+specialSetNo+'&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4&exclude_user_id_list='+$('#ExcludeTeacherID').val());
}
function goDeleteSpecialSetUsers(specialSetNo){
	// remove selected members
	$("#special_set_users_"+specialSetNo+" :selected").each(function(i, selected){
		  $("#special_set_users_"+specialSetNo+" option[value='"+$(selected).val()+"']").remove();
	});
}
function setSpecialSetUserIDStr(){
	for(i=1 ;i <= parseInt($("#TotalSpecialSet").val())+1 ; i++){
		var memberID = "";		
		var memberIDStr="";
		if($("#special_set_users_"+i+" option").length > 0){
			for(j=0;j<$("#special_set_users_"+i+" option").length;j++){
				memberID = $("#special_set_users_"+i+" option").eq(j).val();
				memberID = memberID.replace("U","").replace("G","");
				$("#special_set_users_"+i+" option").eq(j).val(memberID);
				if(j==($("#special_set_users_"+i+" option").length)-1){
					memberIDStr+=$("#special_set_users_"+i+" option").eq(j).val();
				}
				else{
					memberIDStr+=$("#special_set_users_"+i+" option").eq(j).val()+",";
				}		
			}
			var specialSetUserStrInput = $("<input/>").attr("type", "hidden").attr("name", "SpecialSetUsersStr_"+i).val(memberIDStr);
			$('#form1').append($(specialSetUserStrInput));
		}
	}
}
</script>