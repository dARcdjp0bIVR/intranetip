<form id="form1" name="form1" method="post" action="index.php" enctype="multipart/form-data">
	<?=$htmlAry['msg']?>
	<br/>
	<?=$htmlAry['customizedImportStepTbl']?>
		
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>			
	</div>
</form>
<script type="text/javascript">
$(document).ready( function() {
});

function goSubmit(){	
	$("#errMsg").hide();
	var status=formValidation();
	if (status== true){
		var cycleID=$("#CycleID").val();
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_import_leave_2&cycleID='+cycleID).submit();
	}
	else {
		$("#errMsg").show();
	}
}

function formValidation(file){
	var status = true;
	var fileName = $("#ImportLeave").val();
	if(fileName == ""){
		status = false;
	}
	return status; 
}

function goExportLeave(){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>export_leave';
}
function goBackCycle(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}
function goBack(cycleID){
	window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_info&cycleID='+cycleID;
}

</script>