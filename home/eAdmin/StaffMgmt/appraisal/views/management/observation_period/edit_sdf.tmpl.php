<script type="text/javascript" src="../../../../templates/jquery/jquery.jeditable.js"></script>
<link href='../../../../templates/2009a/css/appraisal/content.css' rel='stylesheet'>

<form id="form1" name="form1" method="post" action="index.php" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
		<?=$htmlAry['contentTbl2']?>
		
		<?=$htmlAry['specialAction']?>
	</div>
	

</form>


<script type="text/javascript">
	var needSubjCheck = <?=($needSubjCheck=="")?'""':$needSubjCheck?>;
	var needClassCheck = <?=($needClassCheck=="")?'""':$needClassCheck?>;
	$(document).ready(function(){
		var nonEditDivID=$("#nonEditDivID").val();
		//alert($nonEditDivID);
		//$("<div></div>").css({
		//    position: "absolute",width: "100%",height: "100%",top: 0,left: 0
		//}).appendTo($(nonEditDivID).css("position", "relative").fadeTo("slow",0.7));
		$('input[type="password"]').val("");		
	});
	function changeDatePicker(){
	};
	function goSignatureSubmit(){
		var status=signFormValidation();
		$("#passwordErrorWarnDiv").hide();
		var isTeacherFeedback = $("#isTeacherFeedback").val();
		if(isTeacherFeedback == 1){
			var fillerID = $('#fillerID').val();
			var pwd = $("#signauture_0").val();
		}else{
			var fillerID = $("#obsID").val();
			var fillerIDArr = fillerID.split(",");
			var pwd = "";
			for(i=0;i<fillerIDArr.length;i++){
				if(i==fillerIDArr.length-1){
					pwd = pwd + $("#signauture_"+i).val();
				}
				else{
					pwd = pwd + $("#signauture_"+i).val()+",";
				}
			}
		}		
		if(status==true){
			var data = "type=getSignatureValidation&userID="+fillerID+"&signauture="+pwd;
			$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
		        data: data,async:true,timeout:1000,
		        success: function (data, textStatus, jqXHR) {		
		           		var result = eval('(' + data + ')');
		           		if(result[0]["result"]==0){
		           			var rlsNo = $("#rlsNo").val();
		           			var recordID = $("#recordID").val();
		           			var batchID = $("#batchID").val();
		           			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>view_sdf&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
		           			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		           			var typeVal="formSignature";
		           			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		           			$('#form1').append($(type)).append($(returnPath));
		           			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_sdf_update').submit();        			
		           		}
		           		else{
			           		if(result[0]["msg"] == "<?= $Lang['Appraisal']['ARFormInvalidSignature'] ?>"){
			           			$("#passwordErrorWarnDiv_"+result[0]["obsIdx"]).show();
			           		}
			           		else{
		           				$("[id=atLeastOneErrorWarnDiv]").show();
			           		}
		           		}	         	
		        	}, error: function (jqXHR, textStatus, errorThrown) {	            
		        	}
		        }); 
		}    
	}
	function goModSignatureSubmit(){		
		var status=signFormValidation();
		$("#passwordErrorWarnDiv").hide();
		var isTeacherFeedback = $("#isTeacherFeedback").val();
		if(isTeacherFeedback == 1){
			var fillerID = $('#fillerID').val();
			var pwd = $("#signauture_0").val();
		}else{
			var fillerID = $("#obsID").val();
			var fillerIDArr = fillerID.split(",");
			var pwd = "";
			for(i=0;i<fillerIDArr.length;i++){
				if(i==fillerIDArr.length-1){
					pwd = pwd + $("#signauture_"+i).val();
				}
				else{
					pwd = pwd + $("#signauture_"+i).val()+",";
				}
			}
		}
		if (status== true){
			var data = "type=getSignatureValidation&userID="+fillerID+"&signauture="+pwd;
			$.ajax({ type: "POST", url: '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>ajax_getData', 
		        data: data,async:true,timeout:1000,
		        success: function (data, textStatus, jqXHR) {		
		           		var result = eval('(' + data + ')');
		           		if(result[0]["result"]==0){
		           			var rlsNo = $("#rlsNo").val();
		           			var recordID = $("#recordID").val();
		           			var batchID = $("#batchID").val();
		           			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>view_sdf&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
		           			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		           			var typeVal="modFormSignature";
		           			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		           			$('#form1').append($(type)).append($(returnPath));
		           			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_sdf_update').submit();        			
		           		}
		           		else{
		           			$("#passwordErrorWarnDiv").show();
		           		}	         	
		        	}, error: function (jqXHR, textStatus, errorThrown) {	            
		        	}
		        });
		}      
	}
	function goSubmit(){
		var rlsNo = $("#rlsNo").val();
		var recordID = $("#recordID").val();
		var batchID = $("#batchID").val();
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_sdf&rlsNo='+rlsNo+'&recordID='+recordID+'&batchID='+batchID;	
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		var typeVal="formSave";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		$('#form1').append($(type)).append($(returnPath));
		$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_sdf_update').submit();	
	}
	function goBack(){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';
	}
	function formValidation(){
		return true; 
	}
	function signFormValidation(){
		var formStatus = true;
		$('.requiredField').each( function() {
			var name = $(this).attr("name");
			var type = $(this).attr("type");			
			var isDisabled = $(this).attr('disabled');
			var selector = "";			
			if(type=="radio" || type=="checkbox"){
				selector = "input[name='"+name+"']:checked";
			}
			else{				
				if ($(this).is("input")) {
			        selector = "input[name='"+name+"']";
			    } else if ($(this).is("select")) {
			        selector = "select[name='"+name+"']";
			    } else if ($(this).is("textarea")) {
			        selector = "textarea[name='"+name+"']";
			    }
			}
			var value = $(selector).val();
			var nameMod = name.replace("[]","");
			$("#"+nameMod+"EmptyWarnDiv").hide();
			if((value==undefined || value=="")&& isDisabled==false){
				$("#"+nameMod+"EmptyWarnDiv").show();
				formStatus = false;
			}			
		});
		$("#classEmptyWarnDiv").hide();
		if(needClassCheck && $('#yearClassID').val()==""){
			$("#classEmptyWarnDiv").show();
			$('html, body').animate({scrollTop: $("#yearClassID").offset().top}, 100);
			formStatus = false;
		}
		$("#subjectEmptyWarnDiv").hide();
		if(needSubjCheck && $('#subjectID').val()==""){
			$("#subjectEmptyWarnDiv").show();
			$('html, body').animate({scrollTop: $("#subjectID").offset().top}, 100);
			formStatus = false;
		}
		
		return formStatus;
	}
	
	function goDelete(){
		if(confirm('<?php echo $Lang['Appraisal']['Message']['DeleteAcademicAffairs'];?>')){
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);		
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val('deleteObsFrm');
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
		}	
	}
	function goCopy(){
		if(confirm('<?php echo $Lang['Appraisal']['Message']['CopyAcademicAffairs'];?>')){
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';	
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val('copyObsFrm');
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();
		}	
	}
</script>