<link href='../../../../templates/2009a/css/appraisal/content.css' rel='stylesheet'>
<script type="text/javascript" src="../../../../templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="../../../../templates/jquery/jquery.autocomplete.css" type="text/css">

<form id="form1" name="form1" method="post" action="index.php">
	<?=$htmlAry['navigation']?>
	<br/>
	<div class="table_board">
		<div>
			<?=$htmlAry['contentTbl']?>
		</div>
		<br style="clear:both;" />		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['saveAsBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	

</form>


<script type="text/javascript">
	var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
	$(document).ready(function(){	
	
	});
	$("#appraiseeUserID").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).autocomplete(
			'?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>ajax_getData&addType=Appraisee&Type=GetLoginUser&',
			{  			
				onItemSelect: function(li,engine) {
					$("#appraiseeUserName").text(li.extra[1]);
					$("#appraiseeIDStr").val(li.extra[0]);
					$("#deleteAppraisee").show();
				},
				formatItem: function(row) {
					return row[0];
				},
				maxItemsToShow: 100,
				minChars: 1,
				delay: 0,
				width: 200
			}
		);		
	});	
	$("#AppraiseeSel").each(function(){
		var elem = $(this);
        var elemId=elem.attr("id");
		$(this).change(function(){
			var obj = $(this);
			var value = $(this).val();
			var text = $("option:selected",this).text();
			$(this).hide();
			$('#LoadingPlace').html(ajaxImage);
			setTimeout(function(){				
				$("#appraiseeUserName").text(text);
				$("#appraiseeIDStr").val(value);
				$("#"+elemId).val("");
				$("#deleteAppraisee").show();
				$('#LoadingPlace').html('');
				obj.show();
			},750);
		});		
	});
	function goReset(){
		$("#form1")[0].reset();
	}
	function goBack(){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';
	}
	function goBackCycle(cycleID){
		window.location = '?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';
	}
	function goDeleteAppraisee(){
		$("#appraiseeUserName").text("");
		$("#appraiseeIDStr").val("");
		$("#deleteAppraisee").hide();
	}
	function goDeleteAppraiser(){
		// remove selected Appraisers
		$("#appraiserID :selected").each(function(i, selected){
			  $("#appraiserID option[value='"+$(selected).val()+"']").remove();
		});
		// reconstruct Appraisers list
		var appraiserIDStr="";
		for(i=0;i<$("#appraiserID option").length;i++){
			if(i==($("#appraiserID option").length)-1){
				appraiserIDStr+=$("#appraiserID option").eq(i).val();
			}
			else{
				appraiserIDStr+=$("#appraiserID option").eq(i).val()+",";
			}		
		}
		$("#appraiserIDStr").val(appraiserIDStr);
	}
	function clearAppraiserIDPrefix(){
		var appraiserID = "";
		for(i=0;i<$("#appraiserID option").length;i++){
			appraiserID = $("#appraiserID option").eq(i).val();
			appraiserID = appraiserID.replace("U","").replace("G","");
			$("#appraiserID option").eq(i).val(appraiserID);
		}
	}
	function setAppraiserIDStr(){
		// reconstruct Appraisers list
		var appraiserIDStr="";
		for(i=0;i<$("#appraiserID option").length;i++){
			if(i==($("#appraiserID option").length)-1){
				appraiserIDStr+=$("#appraiserID option").eq(i).val();
			}
			else{
				appraiserIDStr+=$("#appraiserID option").eq(i).val()+",";
			}		
		}
		$("#appraiserIDStr").val(appraiserIDStr);
	}
	function goSelect(){
		newWindow('/home/common_choose/index.php?fieldname=appraiserID&page_title=SelectMembers&permitted_type=1&excludeSupplyTeacher=1&excluded_type=4');
	}
	function formValidation(){
		var status = true;		
		return status;
	}
	function goSubmit(){	
		var status=formValidation();
		if (status== true){
			clearAppraiserIDPrefix();
			setAppraiserIDStr();
			var cycleID=$("#CycleID").val();
			var templateID=$("#DDLTemplateID").val();
			var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>list';		
			var typeVal="AddObsFrm";
			var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
			var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
			$('#form1').append($(type)).append($(returnPath));
			$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>observation_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
		}
	}
	function goSaveAs(){
		var cycleID=$("#CycleID").val();
		var groupID=($("#GrpID").val()=="")?"%s":$("#GrpID").val();
		var returnPathVal='Location: ?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_grp_arr&cycleID='+cycleID+"&grpID=%s";		
		var typeVal="GrpSaveAs";
		var type = $("<input/>").attr("type", "hidden").attr("name", "type").val(typeVal);
		var returnPath = $("<input/>").attr("type", "hidden").attr("name", "returnPath").val(returnPathVal);
		$('#form1').append($(type)).append($(returnPath));
		//$('form#form1').attr('action', 'index.php?task=management<?=$appraisalConfig['taskSeparator']?>appraisal_period<?=$appraisalConfig['taskSeparator']?>edit_update').submit();	
	}
</script>