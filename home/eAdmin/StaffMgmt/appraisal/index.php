<?php
// using : 
/*
 * Change Log:
 */
//ini_set('display_errors',1);
//error_reporting(E_ALL ^ E_NOTICE); 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

if(isset($junior_mck)){
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.utf8.php");
	$g_encoding_unicode = true; // Change encoding from Big5 to UTF-8
	$module_custom['isUTF8'] = true;
	
}

if (isset($_GET["init"])) {
	if ($_GET["init"] != "eadmin") {
		$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] = true;
	} else {
		$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] = false;
	}
	header("Location: ?task=management.appraisal_records.list");
	exit;
}
if (!isset($_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"])) $_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"] = true;


//echo '<link href="'.$PATH_WRT_ROOT.'templates/2009a/css/appraisal/content.css" rel="stylesheet">';

$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();
$indexVar['curUserId'] = $_SESSION['UserID'];

intranet_auth();
intranet_opendb();

$indexVar['libappraisal']->checkAccessRight();


### handle all http post/get value by urldeocde, stripslashes, trim
array_walk_recursive($_POST, 'handleFormPost');
array_walk_recursive($_GET, 'handleFormPost'); 


### get system message
$returnMsgKey = $_GET['returnMsgKey'];
$indexVar['returnMsgLang'] = $Lang['General']['ReturnMessage'][$returnMsgKey];

### include corresponding php files
$indexVar['task'] = $_POST['task']? $_POST['task'] : $_GET['task'];

// load normal pages
if ($indexVar['task']=='') {
	// go to module index page if not defined
	$indexVar['task'] = 'management'.$appraisalConfig['taskSeparator'].'appraisal_records'.$appraisalConfig['taskSeparator'].'list';
}

$indexVar['controllerScript'] = 'controllers/'.str_replace($appraisalConfig['taskSeparator'], '/', $indexVar['task']).'.php';

if (file_exists($indexVar['controllerScript'])){
	include_once($indexVar['controllerScript']);
	
	$indexVar['viewScript'] = 'views/'.str_replace($appraisalConfig['taskSeparator'], '/', $indexVar['task']).'.tmpl.php';
	if (file_exists($indexVar['viewScript'])){
		// normal script => with template script
		
		include_once($indexVar['viewScript']);
		
		if( strpos($indexVar['task'], 'print') !== false || strpos($indexVar['task'], 'thickbox') !== false) {
			// print page and thickbox no footer
		}
		else {
			$indexVar['libappraisal_ui']->echoModuleLayoutStop();
		}
		
	}
	else {
		// update or ajax script => without template script
	}
}
else {
	No_Access_Right_Pop_Up();
}

// initial PA module default setting
$indexVar['libappraisal']->setDefaultSettings();


intranet_closedb();
?>