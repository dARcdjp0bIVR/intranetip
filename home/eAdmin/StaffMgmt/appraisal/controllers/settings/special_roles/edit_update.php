<?php 
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$connection = new libgeneralsettings();

$nameEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["NameEng"])."'";
$nameChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["NameChi"])."'";
$appRoleStatus=$_POST["AppRoleStatus"];
//$sysUse=$_POST["SysUse"];
$sysUse=0;
$appRoleID=$_POST["AppRoleID"];
$type=$_POST["type"];

if($type=="formSave"){	
	$sql="SELECT AppRoleID FROM INTRANET_PA_S_APPROL WHERE AppRoleID=".IntegerSafe($appRoleID).";";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	if(sizeof($a)>0){		
		$sql="SELECT AppRoleID FROM INTRANET_PA_S_APPROL WHERE NameChi=".$nameChi." AND AppRoleID<>".IntegerSafe($appRoleID).";";
		$x=$connection->returnResultSet($sql);
		if(sizeof($x)==0){
			$sql="UPDATE INTRANET_PA_S_APPROL SET NameChi=".$nameChi.",NameEng=".$nameEng.",IsActive=".$appRoleStatus.", ";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE AppRoleID=".IntegerSafe($appRoleID).";";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
			$saveSuccess = true;			
		}
		else{
			$saveSuccess = false;
		}		
	}
	else{
		$sql="SELECT AppRoleID FROM INTRANET_PA_S_APPROL WHERE NameChi=".$nameChi.";";
		$x=$connection->returnResultSet($sql);
		if(sizeof($x)==0){		
			$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_APPROL;";
			$a=$connection->returnResultSet($sql);
			$maxDisplayOrder=$a[0]["DisplayOrder"]+1;	
			$sql="INSERT INTO INTRANET_PA_S_APPROL(NameChi,NameEng,DisplayOrder,IsActive,SysUse,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".$nameChi." as NameChi,".$nameEng." as NameEng,".$maxDisplayOrder." as DisplayOrder,".$appRoleStatus." as IsActive,".$sysUse." as SysUse,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
			$saveSuccess = true;
		}
		else{
			$saveSuccess = false;
		}
	}
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$appraisalConfig['taskSeparator'].'special_roles'.$appraisalConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);



?>