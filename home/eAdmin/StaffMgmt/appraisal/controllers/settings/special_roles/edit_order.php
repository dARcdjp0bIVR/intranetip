<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['SettingsSpecialRole']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$connection = new libgeneralsettings();
$sql="SELECT AppRoleID,NameChi,NameEng,DisplayOrder,
		CASE WHEN IsActive=0 THEN '".$Lang['Appraisal']['SpecialRolesIsActiveDes']['0']."' WHEN IsActive=1 THEN '".$Lang['Appraisal']['SpecialRolesIsActiveDes']['1']."' END as IsActive,
		CASE WHEN SysUse=0 THEN '".$Lang['Appraisal']['SpecialRolesSysUseDes']['0']."' WHEN SysUse=1 THEN '".$Lang['Appraisal']['SpecialRolesSysUseDes']['1']."' END as SysUse
	FROM(
		SELECT AppRoleID,NameChi,NameEng,DisplayOrder,IsActive,SysUse FROM	
		INTRANET_PA_S_APPROL
	) as ipsa
	ORDER BY DisplayOrder";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
		$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang["Appraisal"]["SpecialRolesEngDes"]."</th>";
		$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang["Appraisal"]["SpecialRolesChiDes"]."</th>";
		$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang["Appraisal"]["SpecialRolesIsActive"]."</th>";
		$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang["Appraisal"]["SpecialRolesSysUse"]."</th>";
		$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang["Appraisal"]["SpecialRolesDisplayOrder"]."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0; $i<sizeof($a);$i++){
		$x .= "<tr id=\"".$a[$i]['AppRoleID']."\">"."\r\n";
			$x .= "<td>".$a[$i]['NameEng']."</td>"."\r\n";
			$x .= "<td>".$a[$i]['NameChi']."</td>"."\r\n";
			$x .= "<td>".$a[$i]['IsActive']."</td>"."\r\n";
			$x .= "<td>".$a[$i]['SysUse']."</td>"."\r\n";
			$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";		
		$x .= "</tr>";
	}
	$x .= "</table>";
$x .= "</span></div>";
$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================





?>