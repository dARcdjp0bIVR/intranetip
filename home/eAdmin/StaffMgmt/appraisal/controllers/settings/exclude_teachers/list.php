<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['SettingsExcludeTeachers']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 6 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$connection = new libgeneralsettings();

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("NameEng","NameChi","IsActive","SysUse","OrderNum","Action","DisplayOrder");
if ($keyword !== '' && $keyword !== null) {
	$condsKeyword = " WHERE 1=1";
}
$li -> sql ="
		SELECT NameEng,CONVERT(NameChi USING UTF8),IsActive,SysUse,OrderNum,checkbox,DisplayOrder
		FROM(
			SELECT CONCAT('<a id=',a.AppRoleID,' class=\"tablelink\" href=\"javascript:goArrange(',a.AppRoleID,')\">',NameEng,'</a>') as NameEng,
			CONCAT('<a id=',a.AppRoleID,' class=\"tablelink\" href=\"javascript:goArrange(',a.AppRoleID,')\">',NameChi,'</a>') as NameChi,
			CASE WHEN IsActive=0 THEN '".$Lang['Appraisal']['SpecialRolesIsActiveDes']['0']."' WHEN IsActive=1 THEN '".$Lang['Appraisal']['SpecialRolesIsActiveDes']['1']."' END as IsActive,
			CASE WHEN SysUse=0 THEN '".$Lang['Appraisal']['SpecialRolesSysUseDes']['0']."' 
					WHEN SysUse=1 AND AppRoleID=1 THEN CONCAT('".$Lang['Appraisal']['SpecialRolesSysUseDes']['1']."',' ','".$Lang['Appraisal']['SpecialRolesDefRolDes']['1']."')
					WHEN SysUse=1 AND AppRoleID=2 THEN CONCAT('".$Lang['Appraisal']['SpecialRolesSysUseDes']['1']."',' ','".$Lang['Appraisal']['SpecialRolesDefRolDes']['2']."')	
			END as SysUse,
			OrderNum,
			CASE WHEN SysUse = 0 THEN CONCAT('<input type=checkbox name=AppRoleIDAry[] id=AppRoleIDAry[] value=\"', AppRoleID,'\">') ELSE NULL END as checkbox,DisplayOrder
			FROM(
				SELECT AppRoleID,NameChi,NameEng,DisplayOrder as OrderNum,DisplayOrder,IsActive,SysUse FROM INTRANET_PA_S_APPROL
			) as a
		) as a0".$condsKeyword;
//echo $li->sql."<br/><br/>";

$li->no_col = sizeof($li->field_array);
$li->fieldorder2 = ", DisplayOrder";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['SpecialRolesEngDes'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['SpecialRolesChiDes'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['SpecialRolesIsActive'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['SpecialRolesSysUse'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['SpecialRolesDisplayOrder'])."</th>\n";
$li->column_list .= "<th width='1%'>".$li->check("AppRoleIDAry[]")."</th>\n";

$htmlAry['dataTable1'] = $li->display();


$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);

$htmlAry['contentTbl1'] = $x;


// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goAddRole();', $Lang['Appraisal']['SpecialRoles']['AddRoles'], $subBtnAry);
$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry)."<br/>";

$btnAry = array();
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['Appraisal']['SpecialRoles']['DeleteRoles']);
$htmlAry['dbTableActionBtn'] = $indexVar['libappraisal_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>
