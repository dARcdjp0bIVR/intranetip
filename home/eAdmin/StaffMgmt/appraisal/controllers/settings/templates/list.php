<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right

$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

/*
$ltimetable = new Timetable();
$curTimetableId = $ltimetable->Get_Current_Timetable();
*/

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 0 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 4 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;
$status = (isset($status))?$status:1;

$connection = new libgeneralsettings();

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("FrmEngTitle","FrmChiTitle","Status","Preview","DateTimeModified","Action","DisplayOrder");

if ($keyword !== '' && $keyword !== null) {
	$condsKeyword = " WHERE 1=1";
}
$li -> sql ="
		SELECT CONVERT(EngTitle USING UTF8) as EngTitle,CONVERT(ChiTitle USING UTF8) as ChiTitle,Status,Preview,DateTimeModified,checkbox,DisplayOrder,FrmEngTitle,FrmChiTitle
		FROM(
			SELECT
		CONCAT('<a id=',TemplateID,' class=\"tablelink\" href=\"javascript:goArrange(',TemplateID,',\'',Type,'\')\">',FrmEngTitle,'</a>') as EngTitle,
		CONCAT('<a id=',TemplateID,' class=\"tablelink\" href=\"javascript:goArrange(',TemplateID,',\'',Type,'\')\">',FrmChiTitle,'</a>') as ChiTitle,
		CONCAT('<a href=javascript:goPreview(', TemplateID, ')>', 
	       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>') as Preview,			
			CASE WHEN IsActive=1 THEN '".$Lang['Appraisal']['TemplateSampleIsActive']["1"]."'
				WHEN IsActive=0 THEN '".$Lang['Appraisal']['TemplateSampleIsActive']["0"]."' END as Status,DateTimeModified,OrderNum,
			CONCAT('<input type=checkbox name=templateIDAry[] id=templateIDAry[] value=\"', TemplateID,'\">') as checkbox,DisplayOrder,
			TemplateID,TemplateType,FrmEngTitle,FrmChiTitle
			FROM(
				SELECT TemplateID,TemplateType,FrmTitleChi,FrmCodChi,ObjChi,FrmTitleEng,FrmCodEng,ObjEng,IsActive,DisplayOrder as OrderNum,DisplayOrder,DateTimeModified,
				CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmEngTitle,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmChiTitle,
				CASE WHEN TemplateType=0 THEN 'PDF' WHEN TemplateType=1 THEN 'SDF' END as Type
				FROM INTRANET_PA_S_FRMTPL WHERE IsActive='".$status."'
			) as ipsf
		) as a
		";
// echo $li->sql;
$li->no_col = sizeof($li->field_array);
$li->fieldorder2 = ", DisplayOrder";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$pos = 0;
$li->column_list .= "<th width='1%' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='34%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']["FormEngName"])."</th>\n";
$li->column_list .= "<th width='34%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']["FormChiName"])."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']["FormStatus"])."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['Appraisal']['BtnPreview'])."</th>\n";
$li->column_list .= "<th width='17%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']["ModifiedDate"])."</th>\n";
$li->column_list .= "<th width='1%'>".$li->check("templateIDAry[]")."</th>\n";
$htmlAry['dataTable1'] = $li->display();

$filter = "";
$filter .= "<select name='status' onchange='document.form1.submit()'>";
$filter .= "	<option value='1' ".(($status==1)?"selected":"").">".$Lang['Appraisal']['TemplateSampleIsActive'][1]."</option>";
$filter .= "	<option value='0' ".(($status==0)?"selected":"").">".$Lang['Appraisal']['TemplateSampleIsActive'][0]."</option>";
$filter .= "</select>";
$htmlAry['filter1'] = $filter;

$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);

$htmlAry['contentTbl1'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$subBtnAry = array();
$btnAry[] = array('new', 'javascript: goAddTemplate();', $Lang['Appraisal']['BtnAddTemplate'], $subBtnAry);
$btnAry[] = array('edit', 'javascript: goChangeOrder();', $Lang['Appraisal']['BtnChangeTemplateOrderInReport'], $subBtnAry);
$htmlAry['contentTool'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"display:none;float:right;\" width=\"20%\"><span id=\"msg\"></span></div></div>";
//



$btnAry = array();
$btnAry[] = array('copy', 'javascript: goCopy();',$Lang['Appraisal']['TemplateSample']['CopyTemplate']);
$btnAry[] = array('delete', 'javascript: goDelete();',$Lang['Appraisal']['TemplateSample']['DeleteTemplate']);
$htmlAry['dbTableActionBtn'] = $indexVar['libappraisal_ui']->Get_DBTable_Action_Button_IP25($btnAry);

// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>