<?php
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

echo $indexVar['libappraisal_ui']->Include_JS_CSS();
$connection = new libgeneralsettings();
$sql="SELECT TemplateID,FrmEngTitle,FrmChiTitle,Status,DateTimeModified,DisplayOrder
		FROM(
			SELECT
		FrmEngTitle,
		FrmChiTitle,			
			CASE WHEN IsActive=1 THEN '".$Lang['Appraisal']['TemplateSampleIsActive']["1"]."'
				WHEN IsActive=0 THEN '".$Lang['Appraisal']['TemplateSampleIsActive']["0"]."' END as Status,DateTimeModified,OrderNum,DisplayOrder,
			TemplateID,TemplateType
			FROM(
				SELECT TemplateID,TemplateType,FrmTitleChi,FrmCodChi,ObjChi,FrmTitleEng,FrmCodEng,ObjEng,IsActive,DisplayOrder as OrderNum,DisplayOrder,DateTimeModified,
				CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmEngTitle,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmChiTitle,
				CASE WHEN TemplateType=0 THEN 'PDF' WHEN TemplateType=1 THEN 'SDF' END as Type
				FROM INTRANET_PA_S_FRMTPL 
			) as ipsf
		) as a
		ORDER BY DisplayOrder";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$x .= "<div id=\"DetailLayer\"><span>";
$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
$x .= "<thead>"."\r\n";
$x .= "<tr>";
$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSample']["FormEngName"]."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSample']["FormChiName"]."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSample']["FormStatus"]."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"]."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSample']["DisplayOrder"]."</th>";
$x .= "</tr>";
$x .= "</thead>";
$x .= "<tbody>"."\r\n";
for($i=0; $i<sizeof($a);$i++){
	$x .= "<tr id=\"".$a[$i]['TemplateID']."\">"."\r\n";
	$x .= "<td>".($i+1)."</td>"."\r\n";
	$x .= "<td>".$a[$i]['FrmEngTitle']."</td>"."\r\n";
	$x .= "<td>".$a[$i]['FrmChiTitle']."</td>"."\r\n";
	$x .= "<td>".$a[$i]['Status']."</td>"."\r\n";
	$x .= "<td>".$a[$i]['DateTimeModified']."</td>"."\r\n";
	$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
	$x .= "</tr>";
}
$x .= "</table>";
$x .= "</span></div>";
$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================



























?>