<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$parentSecID=$_GET["parSecID"];
$secID=($_GET["secID"]!=null)?$_GET["secID"]:"NULL";
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=3, $stepAry);
*/
//======================================================================== Content ========================================================================//
$sql="SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
$a0=$connection->returnResultSet($sql);
$sql="SELECT ipsfsec.TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,DateTimeModified
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DateTimeModified
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf ON ipsfsec.TemplateID=ipsf.TemplateID;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dateTimeModified = $a[0]["DateTimeModified"];
$sql="SELECT TemplateID,SecID,SecTitleChi as ParSecTitleChi,SecTitleEng as ParSecTitleEng,SecCodChi as ParSecCodChi,SecCodEng as ParSecCodEng,SecDescrChi as ParSecDescrChi,SecDescrEng as ParSecDescrEng
	  FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($parentSecID).";";
$b0=$connection->returnResultSet($sql);
$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID,ParSecTitleChi,ParSecTitleEng,ParSecCodChi,ParSecCodEng,ParSecDescrChi,ParSecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,SecID,SecTitleChi as ParSecTitleChi,SecTitleEng as ParSecTitleEng,SecCodChi as ParSecCodChi,SecCodEng as ParSecCodEng,SecDescrChi as ParSecDescrChi,SecDescrEng as ParSecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsfparsec ON ipsfsec.ParentSecID=ipsfparsec.SecID;";
//echo $sql."<br/><br/>";
$b=$connection->returnResultSet($sql);

$templateTitle = ($_SESSION['intranet_session_language']=="en")?($a0[0]["FrmTitleEng"]."-".$a0[0]["FrmCodEng"]."-".$a0[0]["ObjEng"]):($a0[0]["FrmTitleChi"]."-".$a0[0]["FrmCodChi"]."-".$a0[0]["ObjChi"]);
$parentSecTitle = ($_SESSION['intranet_session_language']=="en")?($b0[0]["ParSecCodEng"]."-".$b0[0]["ParSecTitleEng"]):($b0[0]["ParSecCodChi"]."-".$b0[0]["ParSecTitleChi"]);
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentTemplate']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackTemplate(".$templateID.")\">".$templateTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentParentSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackParSec(".$templateID.",".$parentSecID.")\">".$parentSecTitle."</a></td></tr>";
$x .= "</table>";
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFSubSecBasicInformation'])."\r\n";
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSubSecCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SubSecCodEng', 'SubSecCodEng', $a[0]["SecCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSubSecCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SubSecCodChi', 'SubSecCodChi', $a[0]["SecCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSubSecTitleEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SubSecTitleEng', 'SubSecTitleEng', $a[0]["SecTitleEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSubSecTitleChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('SubSecTitleChi', 'SubSecTitleChi', $a[0]["SecTitleChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("SubSecDescrEng", $taContents=strip_tags($a[0]["SecDescrEng"]), $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFSubSecDescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("SubSecDescrChi", $taContents=strip_tags($a[0]["SecDescrChi"]), $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .="</table><br/>";
$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">";
$x .= "<input type=\"hidden\" id=\"SecID\" name=\"SecID\" value=".$secID.">";
$x .= "<input type=\"hidden\" id=\"ParentSecID\" name=\"ParentSecID\" value=".$parentSecID.">";
$x .= "<input type=\"hidden\" id=\"AppRoleIDStr\" name=\"AppRoleIDStr\" value=".$appRoleIDStr.">";
$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
if($secID != "NULL" && $secID!="undefined"){
	$htmlAry['previewBtn'] = "<div class=\"edit_bottom_v30\"><p class=\"spacer\"></p>".$indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn')."</div>";;
}
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackParSec(".$templateID.",".$parentSecID.")", 'backBtn');
// ============================== Define Button ==============================

//======================================================================== Question Category ========================================================================//
if($secID != "NULL" && $secID!="undefined"){
	$x = $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFQstCat'])."\r\n";
	$x .= "<a name=\"anchQCat\"></a>";
	
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddQCat('.$templateID.','.$parentSecID.','.$secID.');', $Lang['Appraisal']['BtnAddQCat'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
		
	$sql="SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder
		FROM(
			SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder
			FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secID)."
		) as ipsq ORDER BY DisplayOrder;";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleQCat']['SDFDescrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleQCat']['SDFDescrChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleQCat']['SDFDisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleQCat']['SDFDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['QCatID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$qCatEng=($a[$i]["QCatCodEng"]==""&&$a[$i]["DescrEng"]=="")?$Lang['Appraisal']['TemplateSampleQCat']['NoTitleAndCod']:($a[$i]["QCatCodEng"]."-".$a[$i]["DescrEng"]);
		$x .= "<td><a id='QCatEngID_'".$a[$i]['QCatID']." class=\"tablelink\" href=\"javascript:goEditQCat(".$templateID.",".$parentSecID.",".$secID.",".$a[$i]['QCatID'].")\">".$qCatEng."</a></td>"."\r\n";
		$qCatChi=($a[$i]["QCatCodChi"]==""&&$a[$i]["DescrChi"]=="")?$Lang['Appraisal']['TemplateSampleQCat']['NoTitleAndCod']:($a[$i]["QCatCodChi"]."-".$a[$i]["DescrChi"]);
		$x .= "<td><a id='QCatChiID_'".$a[$i]['QCatID']." class=\"tablelink\" href=\"javascript:goEditQCat(".$templateID.",".$parentSecID.",".$secID.",".$a[$i]['QCatID'].")\">".$qCatChi."</a></td>"."\r\n";
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['QCatID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDelete(".$templateID.",".$a[$i]['QCatID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"QCatID_".$a[$i]["QCatID"]."\" name=\"SecID_".$a[$i]["QCatID"]."\" value=".$a[$i]["QCatID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	
	$x .= "</table>";
	$x .= "</div>";
	$htmlAry['contentTbl2'] = $x;
}





?>