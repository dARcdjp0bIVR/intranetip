<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=($_GET["templateID"]!=null)?$_GET["templateID"]:"NULL";
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=1, $stepAry);
*/
//======================================================================== Content ========================================================================//
$sql="SELECT TemplateID,TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,NeedClass,NeedSubj,NeedSubjLang,";
$sql.="AppTgtChi,AppTgtEng,IsActive,IsObs,DateTimeModified,IsObs FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
// echo $sql."<br/><br/>"; exit;
$a=$connection->returnResultSet($sql);
$isObs=$a[0]["IsObs"];

$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFBasicInformation'])."\r\n";
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['FrmTitleEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmTitleEng', 'FrmTitleEng', $a[0]["FrmTitleEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('FrmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['FrmTitleChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmTitleChi', 'FrmTitleChi', $a[0]["FrmTitleChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('FrmTitleChiEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['FrmCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmCodEng', 'FrmCodEng', $a[0]["FrmCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['FrmCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmCodChi', 'FrmCodChi', $a[0]["FrmCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['HdrRefEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('HdrRefEng', 'HdrRefEng', $a[0]["HdrRefEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['HdrRefChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('HdrRefChi', 'HdrRefChi', $a[0]["HdrRefChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['ObjEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('ObjEng', 'ObjEng', $a[0]["ObjEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['ObjChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('ObjChi', 'ObjChi', $a[0]["ObjChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['DescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrEng", $taContents=$a[0]["DescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['DescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrChi", $taContents=$a[0]["DescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['AppTgtEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('AppTgtEng', 'AppTgtEng', $a[0]["AppTgtEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['AppTgtChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('AppTgtChi', 'AppTgtChi', $a[0]["AppTgtChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['NeedClass']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("NeedClass", "NeedClass", $Value=1, $isChecked=($a[0]["NeedClass"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='');
//$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NeedClassEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>\n";"</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['NeedSubj']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("NeedSubject", "NeedSubject", $Value=1, $isChecked=($a[0]["NeedSubj"]=="1")?1:0, $Class='', $Display='', $Onclick='javascript:execLang()', $Disabled='');
//$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NeedSubjectEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>\n";"</td></tr>";
$x .= "<tr id=\"NeedSubjectLangRow\" style=\"".($a[0]["NeedSubj"]=="1"?"":"display:none;")."\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['NeedSubjLang']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("NeedSubjLang", "NeedSubjLang", $Value=1, $isChecked=($a[0]["NeedSubjLang"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='');
//$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('NeedSubjectLangEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>\n";"</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['IsObs']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("IsObs", "IsObs", $Value=1, $isChecked=($a[0]["IsObs"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='');

$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['Status']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Radio_Button('active', 'Status', $Value="1", $isChecked=($a[0]["IsActive"]=="1")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['TemplateSampleStatusVal']['1'], $Onclick="",$isDisabled=0);
$x .= "&nbsp;";
$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('inactive', 'Status', $Value="0", $isChecked=($a[0]["IsActive"]=="0")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['TemplateSampleStatusVal']['0'], $Onclick="",$isDisabled=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('StatusEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>\n";
$x .="</table>";
$x .= "<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['TemplateSample']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/>";
$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($a[0]["DateTimeModified"]!="")?$a[0]["DateTimeModified"]:" - ")."</span>";
$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">";

$hasSelected = 0;
$sql = "SELECT CycleID FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$templateID."'";
$checkingD = $connection->returnResultSet($sql);
if(empty($checkingD)){
	$sql = "SELECT CycleID FROM INTRANET_PA_C_OBSSEL WHERE TemplateID='".$templateID."'";
	$checkingD = $connection->returnResultSet($sql);
	if(!empty($checkingD)){
		$hasSelected = 1;
	}
}else{
	$hasSelected = 1;
}
$x .= "<input type=\"hidden\" id=\"HasSelected\" name=\"HasSelected\" value=".$hasSelected.">";
$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================
if($templateID != "NULL" && $templateID!="undefined"){
	$htmlAry['previewBtn'] = "<div class=\"edit_bottom_v30\"><p class=\"spacer\"></p>".$indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn')."</div>";
}
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================

//======================================================================== Section ========================================================================//
if($templateID != "NULL" && $templateID!="undefined"){
	$x = $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFSecInclude'])."\r\n";
	$x .= "<a name=\"anchParSec\"></a>";	
	
	
	$sql="SELECT SecID,TemplateID,SecCodChi,SecTitleChi,SecCodEng,SecTitleEng,ParentSecID,PageBreakAfter
		FROM(
			SELECT SecID,TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,DisplayOrder,ParentSecID,PageBreakAfter
			FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL
		) as ipsf
		ORDER BY DisplayOrder";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	if(sizeof($a)==0 && $isObs==1){
		$subBtnAry = array();
		$btnAry[] = array('new', 'javascript: goAddParSec('.$templateID.');', $Lang['Appraisal']['BtnAddParSec'], $subBtnAry);
		$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	}
	else{
		$subBtnAry = array();
		$btnAry[] = array('new', 'javascript: goAddParSec('.$templateID.');', $Lang['Appraisal']['BtnAddParSec'], $subBtnAry);
		$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	}

	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleSection']['CodDescrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleSection']['CodDescrChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleSection']['DisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['PageBreakWhenPrint']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleSection']['Delete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0; $i<sizeof($a);$i++){
		if($i==sizeof($a)-1){
			$dataTypeIDStr.=$a[$i]["SecID"];
		}
		else{
			$dataTypeIDStr.=$a[$i]["SecID"].",";
		}
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['SecID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$secTitleEng=($a[$i]["SecTitleEng"]==""&&$a[$i]["SecCodEng"]=="")?$Lang['Appraisal']['TemplateSampleSection']['NoTitleAndCod']:($a[$i]["SecCodEng"]."-".$a[$i]["SecTitleEng"]);
		$x .= "<td><a id='SectionEng_'".$a[$i]['SecID']." class=\"tablelink\" href=\"javascript:goEditParSec(".$templateID.",".$a[$i]['SecID'].")\">".$secTitleEng."</a></td>"."\r\n";
		$secTitleChi=($a[$i]["SecTitleChi"]==""&&$a[$i]["SecCodChi"]=="")?$Lang['Appraisal']['TemplateSampleSection']['NoTitleAndCod']:($a[$i]["SecCodChi"]."-".$a[$i]["SecTitleChi"]);
		$x .= "<td><a id='SectionChi_'".$a[$i]['SecID']." class=\"tablelink\" href=\"javascript:goEditParSec(".$templateID.",".$a[$i]['SecID'].")\">".$secTitleChi."</a></td>"."\r\n";
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox('PageBreak_'.$a[$i]["SecID"], 'PageBreak_'.$a[$i]["SecID"], $Value=$a[$i]["PageBreakAfter"], $isChecked=($a[$i]["PageBreakAfter"]==1)?1:0, $Class='', $Display='', $Onclick='javascript:addPageBreak('.$templateID.','.$a[$i]["SecID"].')', $Disabled='');
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['SecID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDelete(".$templateID.",".$a[$i]['SecID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"SecID_".$a[$i]["SecID"]."\" name=\"SecID_".$a[$i]["SecID"]."\" value=".$a[$i]["SecID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div>";
	$htmlAry['contentTbl2'] = $x;
}

?>