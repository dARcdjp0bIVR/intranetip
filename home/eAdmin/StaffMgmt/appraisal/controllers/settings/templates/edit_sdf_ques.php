<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$parentSecID=$_GET["parSecID"];
$secID=$_GET["secID"];
$qCatID=$_GET["qCatID"];
$qID=($_GET["qID"]!=null)?$_GET["qID"]:"NULL";
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=5, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT ipsfsec.TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf ON ipsfsec.TemplateID=ipsf.TemplateID;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID,ParSecTitleChi,ParSecTitleEng,ParSecCodChi,ParSecCodEng,ParSecDescrChi,ParSecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,SecID,SecTitleChi as ParSecTitleChi,SecTitleEng as ParSecTitleEng,SecCodChi as ParSecCodChi,SecCodEng as ParSecCodEng,SecDescrChi as ParSecDescrChi,SecDescrEng as ParSecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsfparsec ON ipsfsec.ParentSecID=ipsfparsec.SecID;";
//echo $sql."<br/><br/>";
$b=$connection->returnResultSet($sql);

$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec;";
//echo $sql."<br/><br/>";
$c=$connection->returnResultSet($sql);

$sql="SELECT ipsq.QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,QuesDispMode 
	FROM(
		SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,QuesDispMode 
		FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secID)." AND QCatID=".IntegerSafe($qCatID)."
	) as ipsq";
//echo $sql."<br/><br/>";
$d=$connection->returnResultSet($sql);
$QuesDispMode=$d[0]["QuesDispMode"];
//$secID=$c[0]["SecID"];

$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$templateTitle = ($_SESSION['intranet_session_language']=="en")?($a[0]["FrmTitleEng"]."-".$a[0]["FrmCodEng"]."-".$a[0]["ObjEng"]):($a[0]["FrmTitleChi"]."-".$a[0]["FrmCodChi"]."-".$a[0]["ObjChi"]);
$parentSecTitle = ($_SESSION['intranet_session_language']=="en")?($b[0]["ParSecCodEng"]."-".$b[0]["ParSecTitleEng"]):($b[0]["ParSecCodChi"]."-".$b[0]["ParSecTitleChi"]);
$secTitle = ($_SESSION['intranet_session_language']=="en")?($c[0]["SecCodEng"]."-".$c[0]["SecTitleEng"]):($c[0]["SecCodChi"]."-".$c[0]["SecTitleChi"]);
$catTitle = ($_SESSION['intranet_session_language']=="en")?($d[0]["QCatCodEng"]."-".$d[0]["DescrChi"]):($d[0]["QCatCodChi"]."-".$d[0]["DescrChi"]);

$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentTemplate']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackTemplate(".$templateID.")\">".$templateTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentParentSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackParSec(".$templateID.",".$parentSecID.")\">".$parentSecTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentSubSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackSubSec(".$templateID.",".$parentSecID.",".$secID.")\">".$secTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentQCatSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackQCat(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.")\">".$catTitle."</a></td></tr>";
$x .= "</table>";

$sql="SELECT QID,QCatID,QCodChi,QCodEng,DescrEng,DescrChi,DisplayOrder,InputType,IsMand,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng,IsAvgMark,AvgDec,DateTimeModified
	FROM(
		SELECT QID,QCatID,QCodChi,QCodEng,DescrEng,DescrChi,DisplayOrder,InputType,IsMand,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng,IsAvgMark,AvgDec,DateTimeModified
		FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($qCatID)." AND QID=".IntegerSafe($qID)."
	) as ipsq;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dateTimeModified = $a[0]["DateTimeModified"];

$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateQues']['SDFQuesBasicInformation'])."\r\n";
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFQCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('QCodEng', 'QCodEng', $a[0]["QCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFQCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('QCodChi', 'QCodChi', $a[0]["QCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFDescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrEng", $taContents=strip_tags($a[0]["DescrEng"]), $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFDescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrChi", $taContents=strip_tags($a[0]["DescrChi"]), $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFMandatory']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox('QMandatory', 'QMandatory', 1, $isChecked=$a[0]["IsMand"], $Class="", $Display="", $Onclick="",$isDisabled=0)."</td></tr>";
//$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class="", $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=0);

if($QuesDispMode==1 || $QuesDispMode==2){
	$displayTag="style=\"display:none;\"";
}
else{
	$displayTag="";
}
$x .= "<tr ".$displayTag."><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFInputType']."</td>";
$x .= "<td>";
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeEmpty", "InputType", $Value=0, $isChecked=($a[0]["InputType"]==0)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeEmpty'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeMC", "InputType", $Value=1, $isChecked=($a[0]["InputType"]==1)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeMC'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeMCM", "InputType", $Value=6, $isChecked=($a[0]["InputType"]==6)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeMCM'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeText", "InputType", $Value=2, $isChecked=($a[0]["InputType"]==2)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeText'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeScore", "InputType", $Value=3, $isChecked=($a[0]["InputType"]==3)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeScore'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeDate", "InputType", $Value=4, $isChecked=($a[0]["InputType"]==4)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeDate'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeMultiText", "InputType", $Value=5, $isChecked=($a[0]["InputType"]==5)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeMultiText'], $Onclick="",$isDisabled=0);
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeUpload", "InputType", $Value=7, $isChecked=($a[0]["InputType"]==7)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateQues']['SDFInputTypeUpload'], $Onclick="",$isDisabled=0);
$x .= "</td></tr>";

$x .= "<tr id=\"TxtboxNoteTr\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFTxtBoxNote']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('TxtBoxNote', 'TxtBoxNote', $a[0]["TxtBoxNote"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";

$x .= "<tr id=\"trAvgMark\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFAvgMark']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("AvgMark", "AvgMark", $Value=1, $isChecked=($a[0]["IsAvgMark"]==1)?1:0, $Class="", $Display="", $Onclick="",$isDisabled=0)."</td></tr>";
$x .= "<tr id=\"trAvgDec\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFAvgDec']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("AvgDec", "AvgDec", $a[0]["AvgDec"], $OtherClass="", $OtherPar=array('maxlength'=>255));
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("AvgDecEmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Number'], $Class='warnMsgDiv');
$x .= "</td></tr>";

$x .= "<tr id=\"RmkDescCbTr\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFHasRmk']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("HasRmk", "HasRmk", $Value=1, $isChecked=($a[0]["HasRmk"]=="1")?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td></tr>";

$x .= "<tr id=\"RmkDescrEngTr\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFRmkLabelEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("RmkLabelEng", $taContents=$a[0]["RmkLabelEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr id=\"RmkDescrChiTr\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateQues']['SDFRmkLabelChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("RmkLabelChi", $taContents=$a[0]["RmkLabelChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";



$x .= "</table><br/>";
$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value='".$templateID."'>";
$x .= "<input type=\"hidden\" id=\"ParentSecID\" name=\"ParentSecID\" value='".$parentSecID."'>";
$x .= "<input type=\"hidden\" id=\"SecID\" name=\"SecID\" value='".$secID."'>";
$x .= "<input type=\"hidden\" id=\"QCatID\" name=\"QCatID\" value='".$qCatID."'>";
$x .= "<input type=\"hidden\" id=\"QID\" name=\"QID\" value='".$qID."'>";
$x .= "<input type=\"hidden\" id=\"QuesDispMode\" name=\"QuesDispMode\" value='".$QuesDispMode."'>";

$htmlAry['contentTbl'] = $x;

//======================================================================== Answer Category ========================================================================//
if(($qID != "NULL" && $qID!="undefined")){
	$x = "<div id=\"divMCAns\">";
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFQuestion'])."\r\n";
	$x .= "<a name=\"anchAns\"></a>";
	
	$subBtnAry = array();
	$btnAry = array();
	$btnAry[] = array('new', 'javascript: goAddQAns('.$templateID.','.$parentSecID.','.$secID.','.$qCatID.','.$qID.');', $Lang['Appraisal']['BtnAddAns'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$sql="SELECT QAnsID,QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DisplayOrder
			FROM(
				SELECT QAnsID,QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DisplayOrder
				FROM INTRANET_PA_S_QANS  WHERE QID=".IntegerSafe($qID)."
			) as ipsq ORDER BY DisplayOrder";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateAnswer']['SDFCodEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateAnswer']['SDFCodChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateAnswer']['SDFDescrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateAnswer']['SDFDescrChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateAnswer']['SDFDisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateAnswer']['SDFDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['QAnsID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$qAnsEng=($a[$i]["QAnsEng"]=="")?$Lang['Appraisal']['TemplateAnswer']['NoAns']:($a[$i]["QAnsEng"]);
		$x .= "<td><a id='AnsID_'".$a[$i]['QAnsID']." class=\"tablelink\" href=\"javascript:goEditQAns(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$qID.",".$a[$i]['QAnsID'].")\">".$qAnsEng."</a></td>"."\r\n";
		$qAnsChi=($a[$i]["QAnsChi"]=="")?$Lang['Appraisal']['TemplateAnswer']['NoAns']:($a[$i]["QAnsChi"]);
		$x .= "<td><a id='AnsID_'".$a[$i]['QAnsID']." class=\"tablelink\" href=\"javascript:goEditQAns(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$qID.",".$a[$i]['QAnsID'].")\">".$qAnsChi."</a></td>"."\r\n";		
		$ansDescrEng=($a[$i]["DescrEng"]=="")?$Lang['Appraisal']['TemplateAnswer']['NoAnsDescr']:($a[$i]["DescrEng"]);
		$x .= "<td><a id='AnsID_'".$a[$i]['QAnsID']." class=\"tablelink\" href=\"javascript:goEditQAns(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$qID.",".$a[$i]['QAnsID'].")\">".$ansDescrEng."</a></td>"."\r\n";
		$ansDescrChi=($a[$i]["DescrChi"]=="")?$Lang['Appraisal']['TemplateAnswer']['NoAnsDescr']:($a[$i]["DescrChi"]);
		$x .= "<td><a id='AnsID_'".$a[$i]['QAnsID']." class=\"tablelink\" href=\"javascript:goEditQAns(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$qID.",".$a[$i]['QAnsID'].")\">".$ansDescrChi."</a></td>"."\r\n";
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['QAnsID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDelete(".$templateID.",".$a[$i]['QAnsID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"QAnsID_".$a[$i]["QAnsID"]."\" name=\"QAnsID_".$a[$i]["QAnsID"]."\" value=".$a[$i]["QAnsID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= "</div>";
	$htmlAry['contentTbl2'] = $x;
}



// ============================== Define Button ==============================
if(($qID != "NULL" && $qID!="undefined") && ($QuesDispMode==1 || $QuesDispMode==2)){
	$htmlAry['previewBtn1'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn');
}
if(($qID != "NULL" && $qID!="undefined") && $QuesDispMode==0){
	$htmlAry['previewBtn2'] = "<div class=\"edit_bottom_v30\"><p class=\"spacer\"></p>".$indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn')."</div>";;
}
$htmlAry['submitNewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSaveNew'], "button", "goSubmitNew()", 'submitBtn');
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackQCat(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.")", 'backBtn');
// ============================== Define Button ==============================





?>