<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$parentSecID=$_GET["parSecID"];
$secID=$_GET["secID"];
$qCatID=$_GET["qCatID"];
$dispModeID=$_GET["dispModeID"];
$dispID=($_GET["dispID"]!=null)?$_GET["dispID"]:"NULL";;
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=4, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT ipsfsec.TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf ON ipsfsec.TemplateID=ipsf.TemplateID;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID,ParSecTitleChi,ParSecTitleEng,ParSecCodChi,ParSecCodEng,ParSecDescrChi,ParSecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,SecID,SecTitleChi as ParSecTitleChi,SecTitleEng as ParSecTitleEng,SecCodChi as ParSecCodChi,SecCodEng as ParSecCodEng,SecDescrChi as ParSecDescrChi,SecDescrEng as ParSecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsfparsec ON ipsfsec.ParentSecID=ipsfparsec.SecID;";
//echo $sql."<br/><br/>";
$b=$connection->returnResultSet($sql);

$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec;";
//echo $sql."<br/><br/>";
$c=$connection->returnResultSet($sql);

$sql="SELECT ipsq.QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng
	FROM(
		SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng
		FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secID)." AND QCatID=".IntegerSafe($qCatID)."
	) as ipsq";
//echo $sql."<br/><br/>";
$d=$connection->returnResultSet($sql);

$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$templateTitle = ($_SESSION['intranet_session_language']=="en")?($a[0]["FrmTitleEng"]."-".$a[0]["FrmCodEng"]."-".$a[0]["ObjEng"]):($a[0]["FrmTitleChi"]."-".$a[0]["FrmCodChi"]."-".$a[0]["ObjChi"]);
$parentSecTitle = ($_SESSION['intranet_session_language']=="en")?($b[0]["ParSecCodEng"]."-".$b[0]["ParSecTitleEng"]):($b[0]["ParSecCodChi"]."-".$b[0]["ParSecTitleChi"]);
$secTitle = ($_SESSION['intranet_session_language']=="en")?($c[0]["SecCodEng"]."-".$c[0]["SecTitleEng"]):($c[0]["SecCodChi"]."-".$c[0]["SecTitleChi"]);
$catTitle = ($_SESSION['intranet_session_language']=="en")?($d[0]["QCatCodEng"]."-".$d[0]["DescrChi"]):($d[0]["QCatCodChi"]."-".$d[0]["DescrChi"]);

$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentTemplate']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackTemplate('".$templateID."')\">".$templateTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentParentSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackParSec('".$templateID."','".$parentSecID."')\">".$parentSecTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentSubSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackSubSec(".$templateID.",".$parentSecID.",".$secID.")\">".$secTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentQCatSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackQCat(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.")\">".$catTitle."</a></td></tr>";
$x .= "</table><br/>\r\n";

if($dispModeID==1){
	$sql="SELECT MarkID,QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,Score,DateTimeModified
		FROM(
			SELECT MarkID,QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,Score,DisplayOrder,DateTimeModified
			FROM INTRANET_PA_S_QLKS WHERE QCatID=".IntegerSafe($qCatID)." AND MarkID=".IntegerSafe($dispID)."
		) as ipsq";	
	$a=$connection->returnResultSet($sql);
	$dateTimeModified = $a[0]["DateTimeModified"];
	
	$inputID=$a[0]["MarkID"];
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFLikertInputInfo'])."\r\n";
	$x .="<table class=\"form_table_v30\">";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodEng']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('MarkCodEng', 'MarkCodEng', $a[0]["MarkCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodChi']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('MarkCodChi', 'MarkCodChi', $a[0]["MarkCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrEng']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('MarkDescrEng', 'MarkDescrEng', $a[0]["DescrEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrChi']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('MarkDescrChi', 'MarkDescrChi', $a[0]["DescrChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFScore']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("Score", "Score", $a[0]["Score"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
	$x .= "</table><br/>";
	$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";
}
else if($dispModeID==2){	
	$sql="SELECT MtxFldID,QCatID,FldHdrChi,FldHdrEng,InputType,DisplayOrder,DateTimeModified
		FROM(
			SELECT MtxFldID,QCatID,FldHdrChi,FldHdrEng,InputType,DisplayOrder,DateTimeModified
			FROM INTRANET_PA_S_QMTX WHERE QCatID=".IntegerSafe($qCatID)." AND MtxFldID=".IntegerSafe($dispID)."
		) as ipsq";
	$a=$connection->returnResultSet($sql);
	$dateTimeModified = $a[0]["DateTimeModified"];
	
	$inputID=$a[0]["MtxFldID"];
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFMatrixInputInfo'])."\r\n";
	$x .="<table class=\"form_table_v30\">";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrEng']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FldHdrEng', 'FldHdrEng', $a[0]["FldHdrEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrChi']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FldHdrChi', 'FldHdrChi', $a[0]["FldHdrChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateMatrix']['SDFInputType']."</td>";
	$x .= "<td>";
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeIDText", "InputType", $Value=0, $isChecked=($a[0]["InputType"]==0)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateMatrix']['SDFInputText'], $Onclick="",$isDisabled=0);
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeIDMultipleText", "InputType", $Value=3, $isChecked=($a[0]["InputType"]==3)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateMatrix']['SDFInputMultipleText'], $Onclick="",$isDisabled=0);
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeIDNumeric", "InputType", $Value=1, $isChecked=($a[0]["InputType"]==1)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateMatrix']['SDFInputNumeric'], $Onclick="",$isDisabled=0);
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("InputTypeIDDate", "InputType", $Value=2, $isChecked=($a[0]["InputType"]==2)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateMatrix']['SDFInputDate'], $Onclick="",$isDisabled=0);
	$x .= "</td></tr>";
	$x .= "</table><br/>";
	$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";
}

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value='".$templateID."'>";
$x .= "<input type=\"hidden\" id=\"ParentSecID\" name=\"ParentSecID\" value='".$parentSecID."'>";
$x .= "<input type=\"hidden\" id=\"SecID\" name=\"SecID\" value='".$secID."'>";
$x .= "<input type=\"hidden\" id=\"QCatID\" name=\"QCatID\" value='".$qCatID."'>";
$x .= "<input type=\"hidden\" id=\"DispModeID\" name=\"DispModeID\" value='".$dispModeID."'>";
$x .= "<input type=\"hidden\" id=\"InputID\" name=\"InputID\" value='".$dispID."'>";

$htmlAry['contentTbl'] = $x;

// ============================== Define Button ==============================
if(($dispID != "NULL" && $dispID!="undefined")){
	$htmlAry['previewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn');
}
$htmlAry['submitNewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSaveNew'], "button", "goSubmitNew()", 'submitBtn');
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackQCat(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.")", 'backBtn');
// ============================== Define Button ==============================





?>