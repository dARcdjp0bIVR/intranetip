<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['PDFTitle']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);

$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['BasicInformation'])."\r\n";

$sql="SELECT TemplateID,TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,NeedClass,NeedSubj,";
$sql.="AppTgtChi,AppTgtEng,IsActive,IsDataPreload FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['FrmTitleEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmTitleEng', 'FrmTitleEng', $a[0]["FrmTitleEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['FrmTitleChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmTitleChi', 'FrmTitleChi', $a[0]["FrmTitleChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['FrmCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmCodEng', 'FrmCodEng', $a[0]["FrmCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['FrmCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('FrmCodChi', 'FrmCodChi', $a[0]["FrmCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['HdrRefEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('HdrRefEng', 'HdrRefEng', $a[0]["HdrRefEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['HdrRefChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('HdrRefChi', 'HdrRefChi', $a[0]["HdrRefChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['ObjEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('ObjEng', 'ObjEng', $a[0]["ObjEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['ObjChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('ObjChi', 'ObjChi', $a[0]["ObjChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['DescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrEng", $taContents=$a[0]["DescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['DescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrChi", $taContents=$a[0]["DescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['AppTgtEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('AppTgtEng', 'AppTgtEng', $a[0]["AppTgtEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['AppTgtChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('AppTgtChi', 'AppTgtChi', $a[0]["AppTgtChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";

$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['DataPreload']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Radio_Button('yes', 'DataPreload', $Value="1", $isChecked=($a[0]["IsDataPreload"]=="1")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['TemplateSampleDataPreloadVal']['1'], $Onclick="",$isDisabled=0);
$x .= "&nbsp;";
$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('no', 'DataPreload', $Value="0", $isChecked=($a[0]["IsDataPreload"]=="0")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['TemplateSampleDataPreloadVal']['0'], $Onclick="",$isDisabled=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('statusEmptyWarnDiv', $Lang['Appraisal']['SpecialRoles']['Mandatory'], $Class='warnMsgDiv')."</td></tr>\n";
$x .= "<tr><td class=\"field_title\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['Status']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Radio_Button('active', 'Status', $Value="1", $isChecked=($a[0]["IsActive"]=="1")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['TemplateSampleStatusVal']['1'], $Onclick="",$isDisabled=0);
$x .= "&nbsp;";
$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('inactive', 'Status', $Value="0", $isChecked=($a[0]["IsActive"]=="0")?1:0, $Class="requiredField", $Display=$Lang['Appraisal']['TemplateSampleStatusVal']['0'], $Onclick="",$isDisabled=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('statusEmptyWarnDiv', $Lang['Appraisal']['SpecialRoles']['Mandatory'], $Class='warnMsgDiv')."</td></tr>\n";
$x .="</table>";
$x .="<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['TemplateSample']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/><br/>";

$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SelectedSection'])."<br/>\r\n";

$sql="SELECT TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,PageBreakAfter
	FROM(
		SELECT TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,PageBreakAfter FROM INTRANET_PA_S_FRMPRE WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf
	ORDER BY DisplayOrder";
// echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$x .= "<div id=\"DetailLayer\"><span>";
$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
$x .= "<thead>"."\r\n";
$x .= "<tr>";
$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleDataSelection']['DescrEng']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateSampleDataSelection']['DescrChi']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:6%;\">".$Lang['Appraisal']['ARFormEdit']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:6%;\">".$Lang["Appraisal"]["TemplateSampleDataSelection"]['DisplayOrder']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:14%;\">".$Lang['Appraisal']['PageBreakWhenPrint']."</th>";
$x .= "<th class=\"sub_row_top\" style=\"width:14%;\">".$indexVar['libappraisal_ui']->Get_Checkbox('includeAll', 'includeAll', $Value=1, $isChecked=0, $Class='', $Display=$Lang['Appraisal']['TemplateSampleDataSelection']['Display'], $Onclick='', $Disabled='')."</th>";
$x .= "</tr>";
$x .= "</thead>";
$x .= "<tbody>"."\r\n";
for($i=0; $i<sizeof($a);$i++){
	if($i==sizeof($a)-1){
		$dataTypeIDStr.=$a[$i]["DataTypeID"];
	}
	else{
		$dataTypeIDStr.=$a[$i]["DataTypeID"].",";
	}
	
	$sql="SELECT TemplateID,DataTypeID,DataSubTypeID,DescrChi,DescrEng,DisplayOrder,Incl
	FROM(
		SELECT TemplateID,DataTypeID,DataSubTypeID,DescrChi,DescrEng,DisplayOrder,Incl FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID=".IntegerSafe($templateID)." AND DataTypeID=".$a[$i]["DataTypeID"]."
	) as ipsf
	ORDER BY DisplayOrder";
	$subData=$connection->returnResultSet($sql);
	$x .= "<tr id=\"".$a[$i]['DataTypeID']."\">"."\r\n";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('DSDescrEng_'.$a[$i]["DataTypeID"], 'DSDescrEng_'.$a[$i]["DataTypeID"], $a[$i]["DescrEng"], $OtherClass='', $OtherPar=array('maxlength'=>255));
	$x .= "<br/>"."<i><span style=\"color:#999999\">".$a[$i]['DescrEng']."</span></i>"."</td>"."\r\n";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('DSDescrChi_'.$a[$i]["DataTypeID"], 'DSDescrChi_'.$a[$i]["DataTypeID"], $a[$i]["DescrChi"], $OtherClass='', $OtherPar=array('maxlength'=>255));
	$x .= "<br/>"."<i><span style=\"color:#999999\">".$a[$i]['DescrChi']."</span></i>"."</td>"."\r\n";
	$x .= "<td><div class=\"table_row_tool\">";
	if(!empty($subData)){
		$x .= "<a title=\"".$Lang['Appraisal']['ARFormEdit']."\" href=\"#TB_inline?height=550&amp;width=900&amp;inlineId=FakeLayer\" onclick=\"javascript:getEditContent(".$a[$i]["DataTypeID"].");\" class=\"edit_dim thickbox\"></a>";
	}elseif($a[$i]["DataTypeID"]=='17'){
		$x .= "<a title=\"".$Lang['Appraisal']['ARFormEdit']."\" href=\"#TB_inline?height=550&amp;width=900&amp;inlineId=FakeLayer\" onclick=\"javascript:getOrientation(".$a[$i]["DataTypeID"].");\" class=\"edit_dim thickbox\"></a>";
	}
	$x .= "</div></td>"."\r\n";
	$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
	$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox('PageBreak_'.$a[$i]["DataTypeID"], 'PageBreak_'.$a[$i]["DataTypeID"], $Value=1, $isChecked=($a[$i]["PageBreakAfter"]==1)?1:0, $Class='', $Display='', $Onclick='', $Disabled='')."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox('Include_'.$a[$i]["DataTypeID"], 'Include_'.$a[$i]["DataTypeID"], $Value=1, $isChecked=($a[$i]["Incl"]==1)?1:0, $Class='Include', $Display='', $Onclick='', $Disabled='');
	$x .= "<input type=\"hidden\" id=\"DataTypeID_".$a[$i]["DataTypeID"]."\" name=\"DataTypeID_".$a[$i]["DataTypeID"]."\" value=".$a[$i]["DataTypeID"]."></td>"."\r\n";
	$x .= "</tr>";	
}
$x .= "</table>";
$x .= "</span></div>";

$hasSelected = 0;
$sql = "SELECT CycleID FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$templateID."'";
$checkingD = $connection->returnResultSet($sql);
if(empty($checkingD)){
	$sql = "SELECT CycleID FROM INTRANET_PA_C_OBSSEL WHERE TemplateID='".$templateID."'";
	$checkingD = $connection->returnResultSet($sql);
	if(!empty($checkingD)){
		$hasSelected = 1;
	}
}else{
	$hasSelected = 1;
}

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">";
$x .= "<input type=\"hidden\" id=\"DataTypeStr\" name=\"DataTypeStr\" value=".$dataTypeIDStr.">";
$x .= "<input type=\"hidden\" id=\"HasSelected\" name=\"HasSelected\" value=".$hasSelected.">";
$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['previewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn');
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================






?>