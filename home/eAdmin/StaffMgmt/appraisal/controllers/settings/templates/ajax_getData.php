<?php
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$connection = new libgeneralsettings();
$arr = array();
$jsonObj = new JSON_obj();
$type = $_POST['type'];
if($type == "checkTemplate"){
	$status = true;
	$templateIDAry= explode(",",$_POST["templateIDAry"]);
	for($i=0; $i<sizeof($templateIDAry); $i++){
		$sql="SELECT TemplateID FROM INTRANET_PA_T_FRMSUM WHERE TemplateID=".IntegerSafe($templateIDAry[$i]).";";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$status = false;
		}		
	}
	if($status == false){
		$arr[] = array('result' => "1", 'msg' => $Lang['Appraisal']['Template']['UsedNoDeletion']);
	}
	else{
		$arr[] = array('result' => "0", 'msg' => "");
	}
	echo $jsonObj->encode($arr);
	
}elseif($type == "getSubContentList"){
	$x = '<div id="thickboxContainerDiv" class="edit_pop_board" style="height: 100%;"><form id="subSectionForm" name="subSectionForm">';
	$sql="SELECT TemplateID,DataTypeID,DataSubTypeID,DescrChi,DescrEng,DisplayOrder,Incl
	FROM(
		SELECT TemplateID,DataTypeID,DataSubTypeID,DescrChi,DescrEng,DisplayOrder,Incl FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID=".IntegerSafe($_POST['templateID'])." AND DataTypeID=".$_POST['dataType']."
	) as ipsf
	ORDER BY DisplayOrder";
	$subData=$connection->returnResultSet($sql);
	$x .= "<table id=\"subTable_".$_POST['dataType']."\" class=\"common_table_list subTables\" dataType=\"".$_POST['dataType']."\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateQues']['SDFDescrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateQues']['SDFDescrChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:8%;\">".$Lang["Appraisal"]["TemplateSampleDataSelection"]['DisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:17%;\">".$indexVar['libappraisal_ui']->Get_Checkbox('subSectionIncludeAll', 'subSectionIncludeAll', $Value=1, $isChecked=0, $Class='', $Display=$Lang['Appraisal']['TemplateSampleDataSelection']['Display'], $Onclick='subSectionSelectAll()', $Disabled='')."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>";
	foreach($subData as $j=>$sd){		
		$x .= "<tr id=\"".$sd['DataTypeID']."_".$sd['DataSubTypeID']."\" class=\"subsection_".$sd['DataTypeID']."\">"."\r\n";
		$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('DSDescrEng_'.$sd["DataTypeID"]."_".$sd["DataSubTypeID"], 'DSDescrEng[]', $sd["DescrEng"], $OtherClass='', $OtherPar=array('maxlength'=>255));
		$x .= "<br/>"."<i><span style=\"color:#999999\">".$sd['DescrEng']."</span></i>"."</td>"."\r\n";
		$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('DSDescrChi_'.$sd["DataTypeID"]."_".$sd["DataSubTypeID"], 'DSDescrChi[]', $sd["DescrChi"], $OtherClass='', $OtherPar=array('maxlength'=>255));
		$x .= "<br/>"."<i><span style=\"color:#999999\">".$sd['DescrChi']."</span></i>"."</td>"."\r\n";
		$x .= "<td class=\"SubDraggable_".$sd['DataTypeID']."\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		//$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox('PageBreak_'.$a[$i]["DataTypeID"], 'PageBreak_'.$a[$i]["DataTypeID"], $Value=$a[$i]["PageBreakAfter"], $isChecked=($a[$i]["PageBreakAfter"]==1)?1:0, $Class='', $Display='', $Onclick='', $Disabled='');		
		$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox('Include_'.$sd["DataTypeID"]."_".$sd["DataSubTypeID"], 'Include_'.$sd["DataTypeID"]."_".$sd["DataSubTypeID"], $Value=1, $isChecked=($sd["Incl"]==1)?1:0, $Class='subSectionInclude', $Display='', $Onclick='', $Disabled='');
		$x .= "<input type=\"hidden\" id=\"DataTypeID_".$sd["DataTypeID"]."_".$sd["DataSubTypeID"]."\" name=\"DataTypeID[]\" value=".$sd["DataTypeID"]."></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"DataSubTypeID_".$sd["DataTypeID"]."_".$sd["DataSubTypeID"]."\" name=\"DataSubTypeID[]\" value=".$sd["DataSubTypeID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= '<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<input type="button" name="submitBtn_tb" id="submitBtn_tb" onclick="javascript:submitSubSection();" class="formbutton_v30 print_hide actionBtn" value="'.$Lang['Btn']['Submit'].'">
			<input type="button" name="cancelBtn_tb" id="cancelBtn_tb" onclick="js_Hide_ThickBox();" class="formbutton_v30 print_hide actionBtn" value="'.$Lang['Btn']['Cancel'] .'">
			<p class="spacer"></p>';
	$x .= "</form></div>";
	echo $x;
}elseif($type == "getOrientaiton"){
	$x = '<div id="thickboxContainerDiv" class="edit_pop_board" style="height: 100%;"><form id="subSectionForm" name="subSectionForm">';
	$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='PreDefinedFormAttendanceTableOrientation'";
	$settingValue = current($connection->returnVector($sql));
	if(empty($settingValue)){
		$orientation = 0;
	}else{
		$settingValueArr = unserialize($settingValue);
		$orientation = $settingValueArr[$_POST['templateID']];
	}
	$x .= "<p>".$Lang['Appraisal']['SettingsColumnOrientation']."</p>";
	$x .= "<table id=\"subTable_".$_POST['dataType']."\" class=\"common_table_list subTables\" dataType=\"".$_POST['dataType']."\">";
	$x .= "<tbody>";
	$x .= "	<tr><td>";
	$x .= "	<input type='radio' name='orientation' value='0' ".(($orientation=="0")?"checked":"").">";
	$x .= "	<table>";
	$x .= "	<tr><th></th><th>".$Lang['Appraisal']['ARFormAttendanceColumn']."</th></tr><tr><td>".$Lang['Appraisal']['ARFormLeaCat']."</td><td></td></tr>";
	$x .= "	</table>";
	$x .= "	</td>";
	$x .= "	<td>";
	$x .= "	<input type='radio' name='orientation' value='1' ".(($orientation=="1")?"checked":"").">";
	$x .= "	<table>";
	$x .= "	<tr><th></th><th>".$Lang['Appraisal']['ARFormLeaCat']."</th></tr><tr><td>".$Lang['Appraisal']['ARFormAttendanceColumn']."</td><td></td></tr>";
	$x .= "	</table>";
	$x .= "	</td></tr>";
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= '<div id="editBottomDiv" class="edit_bottom_v30">
			<p class="spacer"></p>
			<input type="button" name="submitBtn_tb" id="submitBtn_tb" onclick="javascript:submitAttendanceOrientation();" class="formbutton_v30 print_hide actionBtn" value="'.$Lang['Btn']['Submit'].'">
			<input type="button" name="cancelBtn_tb" id="cancelBtn_tb" onclick="js_Hide_ThickBox();" class="formbutton_v30 print_hide actionBtn" value="'.$Lang['Btn']['Cancel'] .'">
			<p class="spacer"></p>';
	$x .= "</form></div>";
	echo $x;
}

// ============================== Transactional data ==============================




?>