<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$parentSecID=$_GET["parSecID"];
$secID=$_GET["secID"];
$qCatID=$_GET["qCatID"];
$qID=$_GET["qID"];
$qAnsID=($_GET["qAnsID"]!=null)?$_GET["qAnsID"]:"NULL";
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=5, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT ipsfsec.TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf ON ipsfsec.TemplateID=ipsf.TemplateID;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID,ParSecTitleChi,ParSecTitleEng,ParSecCodChi,ParSecCodEng,ParSecDescrChi,ParSecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,SecID,SecTitleChi as ParSecTitleChi,SecTitleEng as ParSecTitleEng,SecCodChi as ParSecCodChi,SecCodEng as ParSecCodEng,SecDescrChi as ParSecDescrChi,SecDescrEng as ParSecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsfparsec ON ipsfsec.ParentSecID=ipsfparsec.SecID;";
//echo $sql."<br/><br/>";
$b=$connection->returnResultSet($sql);

$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec;";
//echo $sql."<br/><br/>";
$c=$connection->returnResultSet($sql);

$sql="SELECT ipsq.QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,QuesDispMode
	FROM(
		SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,QuesDispMode
		FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secID)." AND QCatID=".IntegerSafe($qCatID)."
	) as ipsq";
//echo $sql."<br/><br/>";
$d=$connection->returnResultSet($sql);
$QuesDispMode=$d[0]["QuesDispMode"];

$sql="SELECT QID,QCatID,QCodChi,QCodEng,DescrEng,DescrChi,DisplayOrder,InputType,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng
	FROM(
		SELECT QID,QCatID,QCodChi,QCodEng,DescrEng,DescrChi,DisplayOrder,InputType,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng
		FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($qCatID)." AND QID=".IntegerSafe($qID)."
	) as ipsq;";
//echo $sql."<br/><br/>";
$e=$connection->returnResultSet($sql);

$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$templateTitle = ($_SESSION['intranet_session_language']=="en")?($a[0]["FrmTitleEng"]."-".$a[0]["FrmCodEng"]."-".$a[0]["ObjEng"]):($a[0]["FrmTitleChi"]."-".$a[0]["FrmCodChi"]."-".$a[0]["ObjChi"]);
$parentSecTitle = ($_SESSION['intranet_session_language']=="en")?($b[0]["ParSecCodEng"]."-".$b[0]["ParSecTitleEng"]):($b[0]["ParSecCodChi"]."-".$b[0]["ParSecTitleChi"]);
$secTitle = ($_SESSION['intranet_session_language']=="en")?($c[0]["SecCodEng"]."-".$c[0]["SecTitleEng"]):($c[0]["SecCodChi"]."-".$c[0]["SecTitleChi"]);
$catTitle = ($_SESSION['intranet_session_language']=="en")?($d[0]["QCatCodEng"]."-".$d[0]["DescrChi"]):($d[0]["QCatCodChi"]."-".$d[0]["DescrChi"]);
$qesTitle = ($_SESSION['intranet_session_language']=="en")?($e[0]["QCodEng"]."-".$e[0]["DescrChi"]):($e[0]["QCodChi"]."-".$e[0]["DescrChi"]);

$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentTemplate']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackTemplate(".$templateID.")\">".$templateTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentParentSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackParSec(".$templateID.",".$parentSecID.")\">".$parentSecTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentSubSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackSubSec(".$templateID.",".$parentSecID.",".$secID.")\">".$secTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentQCatSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackQCat(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.")\">".$catTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentQuesSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackQues(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$qID.")\">".$qesTitle."</a></td></tr>";
$x .= "</table>";

$sql="SELECT QAnsID,QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DateTimeModified
	FROM(
		SELECT QAnsID,QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DateTimeModified
		FROM INTRANET_PA_S_QANS WHERE QID=".IntegerSafe($qID)." AND QAnsID=".IntegerSafe($qAnsID)."
	) as ipsq;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dateTimeModified = $a[0]["DateTimeModified"];

$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFAnswer'])."\r\n";
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateAnswer']['SDFCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('QAnsEng', 'QAnsEng', $a[0]["QAnsEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateAnswer']['SDFCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('QAnsChi', 'QAnsChi', $a[0]["QAnsChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateAnswer']['SDFDescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrEng", $taContents=$a[0]["DescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateAnswer']['SDFDescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("DescrChi", $taContents=$a[0]["DescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "</table>";
$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value='".$templateID."'>";
$x .= "<input type=\"hidden\" id=\"ParentSecID\" name=\"ParentSecID\" value='".$parentSecID."'>";
$x .= "<input type=\"hidden\" id=\"SecID\" name=\"SecID\" value='".$secID."'>";
$x .= "<input type=\"hidden\" id=\"QCatID\" name=\"QCatID\" value='".$qCatID."'>";
$x .= "<input type=\"hidden\" id=\"QID\" name=\"QID\" value='".$qID."'>";
$x .= "<input type=\"hidden\" id=\"QAnsID\" name=\"QAnsID\" value='".$qAnsID."'>";

$htmlAry['contentTbl'] = $x;

// ============================== Define Button ==============================
if(($qAnsID != "NULL" && $qAnsID!="undefined")){
	$htmlAry['previewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn');
}
$htmlAry['submitNewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSaveNew'], "button", "goSubmitNew()", 'submitBtn');
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackQues(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$qID.")", 'backBtn');
// ============================== Define Button ==============================



?>