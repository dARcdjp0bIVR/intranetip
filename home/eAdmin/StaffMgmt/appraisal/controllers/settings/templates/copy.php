<?php 

// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
$indexVar['libappraisal'] = new libappraisal();

# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$templateIDAry=$_POST["templateIDAry"];
$type=$_POST["type"];

for($i=0; $i<sizeof($templateIDAry); $i++){
	$templateID=$templateIDAry[$i];
	$sql="SELECT TemplateID,TemplateType FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
	$a=$connection->returnResultSet($sql);
	$templateType=$a[0]["TemplateType"];
	
	$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_FRMTPL;";
	$a=$connection->returnResultSet($sql);
	$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
	
	if($templateType==0){
		//======================================================================== INTRANET_PA_S_FRMTPL ========================================================================//
		$sql="INSERT INTO INTRANET_PA_S_FRMTPL(TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,DisplayOrder,IsActive,NeedClass,NeedSubj,AppTgtChi,AppTgtEng,IsObs,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT 0 as TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,".$MaxDisplayOrder." as DisplayOrder,2 as IsActive,NeedClass,NeedSubj,";
		$sql.="AppTgtChi,AppTgtEng,IsObs,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
		$sql.="FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
		//echo  $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMTPL WHERE IsActive=2";
		$a=$connection->returnResultSet($sql);
		$nxtTemplateID=$a[0]["TemplateID"];
		$sql="UPDATE INTRANET_PA_S_FRMTPL SET IsActive=0 WHERE TemplateID=".$nxtTemplateID.";";
		$a=$connection->db_db_query($sql);
		//echo $sql."<br/><br/>";
		//======================================================================== INTRANET_PA_S_FRMSEC ========================================================================//
		$sql="INSERT INTO INTRANET_PA_S_FRMSEC(TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DisplayOrder,PageBreakAfter,ParentSecID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT ".$nxtTemplateID.",SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DisplayOrder,PageBreakAfter,ParentSecID,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID).";";
		$a=$connection->db_db_query($sql);
		//echo $sql."<br/><br/>";
		//======================================================================== INTRANET_PA_S_FRMPRE ========================================================================//
		$sql="INSERT INTO INTRANET_PA_S_FRMPRE(TemplateID,DataTypeID,DescrChi,DescrEng,DisplayOrder,PageBreakAfter,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT ".$nxtTemplateID.",DataTypeID,DescrChi,DescrEng,DisplayOrder,PageBreakAfter,Incl,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_S_FRMPRE WHERE TemplateID=".IntegerSafe($templateID).";";
		$a=$connection->db_db_query($sql);
		//echo $sql."<br/><br/>";
		//======================================================================== INTRANET_PA_S_FRMPRE_SUB ========================================================================//
		$sql="INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT ".$nxtTemplateID.",DataTypeID,DescrChi,DescrEng,DisplayOrder,Incl,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID=".IntegerSafe($templateID).";";
		$a=$connection->db_db_query($sql);
		//echo $sql."<br/><br/>";
	}
	else if($templateType==1){
		//======================================================================== INTRANET_PA_S_FRMTPL ========================================================================//		
		$sql="INSERT INTO INTRANET_PA_S_FRMTPL(TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,DisplayOrder,IsActive,NeedClass,NeedSubj,AppTgtChi,AppTgtEng,IsObs,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT 1 as TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,".$MaxDisplayOrder." as DisplayOrder,2 as IsActive,NeedClass,NeedSubj,";
		$sql.="AppTgtChi,AppTgtEng,IsObs,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
		$sql.="FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID).";";
		//echo  $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMTPL WHERE IsActive=2";
		$a=$connection->returnResultSet($sql);
		$nxtTemplateID=$a[0]["TemplateID"];
		$sql="UPDATE INTRANET_PA_S_FRMTPL SET IsActive=0 WHERE TemplateID=".$nxtTemplateID.";";
		$a=$connection->db_db_query($sql);	
		
		//======================================================================== INTRANET_PA_S_FRMSEC ========================================================================//
		$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL ORDER BY SecID;";
		//echo  $sql."<br/><br/>";
		$parentSecSQL=$connection->returnResultSet($sql);
		for($j=0; $j<sizeof($parentSecSQL); $j++){		
			$sql="INSERT INTO INTRANET_PA_S_FRMSEC(TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DisplayOrder,PageBreakAfter,ParentSecID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT ".$nxtTemplateID." as TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,";
			$sql.="SecDescrChi,SecDescrEng,DisplayOrder,PageBreakAfter,NULL as ParentSecID,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified  ";
			$sql.="FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".$parentSecSQL[$j]["SecID"].";";
			//echo  $sql."<br/><br/>";
			$x=$connection->db_db_query($sql); 		
		}	
		$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($nxtTemplateID)." AND ParentSecID IS NULL ORDER BY SecID;";
		//echo  $sql."<br/><br/>";
		$nxtParentSecIDSQL=$connection->returnResultSet($sql);
		for($j=0; $j<sizeof($parentSecSQL); $j++){
			$sql="INSERT INTO INTRANET_PA_S_FRMSEC(TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DisplayOrder,PageBreakAfter,ParentSecID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT ".$nxtTemplateID." as TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,";
			$sql.="SecDescrChi,SecDescrEng,DisplayOrder,PageBreakAfter,".$nxtParentSecIDSQL[$j]["SecID"]." as ParentSecID,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified  ";
			$sql.="FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID=".$parentSecSQL[$j]["SecID"].";";
			//echo  $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		//======================================================================== INTRANET_PA_S_SECROL ========================================================================//	
		for($j=0;$j<sizeof($parentSecSQL);$j++){
			$sql="INSERT INTO INTRANET_PA_S_SECROL(SecID,AppRoleID,CanBrowse,CanFill,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT ".$nxtParentSecIDSQL[$j]["SecID"]." as SecID,AppRoleID,CanBrowse,CanFill,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
			$sql.="FROM INTRANET_PA_S_SECROL WHERE SecID=".$parentSecSQL[$j]["SecID"].";";
			//echo  $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		//======================================================================== INTRANET_PA_S_QCAT ========================================================================//
		$parentSecSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($parentSecSQL,"SecID");
		$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IN (".$parentSecSQLStr.") ORDER BY ParentSecID;";
		//echo  $sql."<br/><br/>";
		$secSQL=$connection->returnResultSet($sql);
		$secSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($secSQL,"SecID");
		
		$nxtParentSecSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($nxtParentSecIDSQL,"SecID");
		//echo  "parentSecSQLStr: ".$parentSecSQLStr." && nxtParentSecSQLStr: ".$nxtParentSecSQLStr."<br/><br/>";
		$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($nxtTemplateID)." AND ParentSecID IN (".$nxtParentSecSQLStr.") ORDER BY ParentSecID;";
		//echo  $sql."<br/><br/>";
		$nxtSecSQL=$connection->returnResultSet($sql);
		$nxtSecSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($nxtSecSQL,"SecID");
		//echo  "secSQLStr: ".$secSQLStr." && nxtSecSQLStr: ".$nxtSecSQLStr."<br/><br/>";
		$secMapping = array();
		$qCatMapping = array();
		
		for($j=0;$j<sizeof($secSQL);$j++){
			$sql ="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secSQL[$j]["SecID"]).";";
			$qCatIDs=$connection->returnVector($sql);
			foreach($qCatIDs as $qcid){
				$sql="INSERT INTO INTRANET_PA_S_QCAT(SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder,QuesDispMode,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$nxtSecSQL[$j]["SecID"]." as SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,";
				$sql.="DisplayOrder,QuesDispMode,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_S_QCAT WHERE QCatID=".IntegerSafe($qcid).";";
				//echo  $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$nextQCatID = $connection->db_insert_id();
				array_push($qCatMapping, array("qCatID"=>$qcid, "nextQCatID"=>$nextQCatID));
			}
		}
		
		$sql="SELECT QCatID, SecID, DisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$secSQLStr.") ORDER BY SecID,QCatID,DisplayOrder;";
		//echo $sql."<br/><br/>";
		$qCatSQL=$connection->returnResultSet($sql);
		$qCatSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($qCatSQL,"QCatID");
		$qCatMapKey = array();
		$sql="SELECT QCatID, SecID, DisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$nxtSecSQLStr.") ORDER BY SecID,QCatID,DisplayOrder;";
		//echo $sql."<br/><br/>";
		$nxtQCatSQL=$connection->returnResultSet($sql);
		$nxtQCatSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($nxtQCatSQL,"QCatID");
		$nxtQCatMapKey = array();
		for($j=0; $j<sizeof($nxtQCatSQL);$j++){
			$nxtQCatMapKey[$nxtQCatSQL[$j]['SecID']] = $nxtQCatSQL[$j]['QCatID'];
			$qCatMapKey[$qCatSQL[$j]['SecID']] = $qCatSQL[$j]['QCatID'];
		}
		
		$reqCatOrder = array();
		for($j=0;$j<sizeof($secSQL);$j++){
			array_push($reqCatOrder, array("qCatID"=>$qCatMapKey[$secSQL[$j]['SecID']], "nextQCatID"=>$nxtQCatMapKey[$secMapping[$secSQL[$j]['SecID']]]));
		}	
		//echo  "qCatSQLStr: ".$qCatSQLStr." && nxtQCatSQLStr: ".$nxtQCatSQLStr."<br/><br/>";
		//======================================================================== INTRANET_PA_S_QLKS ========================================================================//
		for($j=0;$j<sizeof($qCatMapping);$j++){
			$sql="INSERT INTO INTRANET_PA_S_QLKS(QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,Score,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT ".$qCatMapping[$j]["nextQCatID"]." as QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,";
			$sql.="Score,DisplayOrder,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
			$sql.="FROM INTRANET_PA_S_QLKS WHERE QCatID=".IntegerSafe($qCatMapping[$j]["qCatID"]).";";
			//echo  $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		//======================================================================== INTRANET_PA_S_QMTX ========================================================================//
		for($j=0;$j<sizeof($qCatMapping);$j++){
			$sql="INSERT INTO INTRANET_PA_S_QMTX(QCatID,FldHdrChi,FldHdrEng,InputType,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT ".$qCatMapping[$j]["nextQCatID"]." as QCatID,FldHdrChi,FldHdrEng,";
			$sql.="InputType,DisplayOrder,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
			$sql.="FROM INTRANET_PA_S_QMTX WHERE QCatID=".IntegerSafe($qCatMapping[$j]["qCatID"]).";";
			//echo  $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		//======================================================================== INTRANET_PA_S_QUES ========================================================================//
		$qIDSQL = array();
		$nxtQIDSQL = array();
		for($j=0;$j<sizeof($qCatMapping);$j++){
			$sql ="SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($qCatMapping[$j]["qCatID"]).";";
			$qIDs=$connection->returnVector($sql);
			foreach($qIDs as $qid){
				$sql="INSERT INTO INTRANET_PA_S_QUES(QCatID,QCodChi,QCodEng,DescrChi,DescrEng,DisplayOrder,InputType,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng,IsMand,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$qCatMapping[$j]["nextQCatID"]." as QCatID,QCodChi,QCodEng,DescrChi,DescrEng,DisplayOrder,InputType,";
				$sql.="TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng,IsMand,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_S_QUES WHERE QID=".IntegerSafe($qid).";";
				//echo  $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				array_push($qIDSQL, array("QID"=>$qid));
				array_push($nxtQIDSQL, array("QID"=>$connection->db_insert_id()));
			}
		}
		
		//$sql="SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatSQLStr.") ORDER BY QCatID,QID";
		//echo  $sql."<br/><br/>";
		//$qIDSQL=$connection->returnResultSet($sql);
		$qIDSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($qIDSQL,"QID");
		
		//$sql="SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$nxtQCatSQLStr.")  ORDER BY QCatID,QID";
		//echo  $sql."<br/><br/>";
		//$nxtQIDSQL=$connection->returnResultSet($sql);
		$nxtQIDSQLStr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($nxtQIDSQL,"QID");
		
		//echo  "qIDSQLStr: ".$qIDSQLStr." && nxtQIDSQLStr: ".$nxtQIDSQLStr."<br/><br/>";
		//======================================================================== INTRANET_PA_S_QANS ========================================================================//
		for($j=0;$j<sizeof($qIDSQL);$j++){
			$sql="INSERT INTO INTRANET_PA_S_QANS(QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT ".$nxtQIDSQL[$j]["QID"]." as QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DisplayOrder,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
			$sql.="FROM INTRANET_PA_S_QANS WHERE QID = ".IntegerSafe($qIDSQL[$j]["QID"]).";";
			//echo  $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
	}
}


$saveSuccess=true;
$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header('Location: ?task=settings'.$appraisalConfig['taskSeparator'].'templates'.$appraisalConfig['taskSeparator'].'list&returnMsgKey='.$returnMsgKey);
















?>