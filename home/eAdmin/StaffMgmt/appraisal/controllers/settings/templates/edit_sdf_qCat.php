<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['TemplateSampleForm']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_JS_CSS();

$templateID=$_GET["templateID"];
$parentSecID=$_GET["parSecID"];
$secID=$_GET["secID"];
$qCatID=($_GET["qCatID"]!=null)?$_GET["qCatID"]:"NULL";
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['SettingsTemplate'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['TemplateSample']['Title']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps1'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps2'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps3'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps4'];
$stepAry[] = $Lang['Appraisal']['TemplateSample']['Steps5'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=4, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT ipsfsec.TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,DateTimeModified
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DateTimeModified
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsf ON ipsfsec.TemplateID=ipsf.TemplateID;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID,ParSecTitleChi,ParSecTitleEng,ParSecCodChi,ParSecCodEng,ParSecDescrChi,ParSecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec
	INNER JOIN(
		SELECT TemplateID,SecID,SecTitleChi as ParSecTitleChi,SecTitleEng as ParSecTitleEng,SecCodChi as ParSecCodChi,SecCodEng as ParSecCodEng,SecDescrChi as ParSecDescrChi,SecDescrEng as ParSecDescrEng
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)."
	) as ipsfparsec ON ipsfsec.ParentSecID=ipsfparsec.SecID;";
//echo $sql."<br/><br/>";
$b=$connection->returnResultSet($sql);

$sql="SELECT ipsfsec.TemplateID,ipsfsec.SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng
	FROM(
		SELECT TemplateID,SecID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,ParentSecID
		FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND SecID=".IntegerSafe($secID)." AND ParentSecID IS NOT NULL
	) as ipsfsec;";
//echo $sql."<br/><br/>";
$c=$connection->returnResultSet($sql);
//$secID=$c[0]["SecID"];

$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$a[0]["AppTgtEng"]:$a[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$templateTitle = ($_SESSION['intranet_session_language']=="en")?($a[0]["FrmTitleEng"]."-".$a[0]["FrmCodEng"]."-".$a[0]["ObjEng"]):($a[0]["FrmTitleChi"]."-".$a[0]["FrmCodChi"]."-".$a[0]["ObjChi"]);
$parentSecTitle = ($_SESSION['intranet_session_language']=="en")?($b[0]["ParSecCodEng"]."-".$b[0]["ParSecTitleEng"]):($b[0]["ParSecCodChi"]."-".$b[0]["ParSecTitleChi"]);
$secTitle = ($_SESSION['intranet_session_language']=="en")?($c[0]["SecCodEng"]."-".$c[0]["SecTitleEng"]):($c[0]["SecCodChi"]."-".$c[0]["SecTitleChi"]);

$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentTemplate']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackTemplate(".$templateID.")\">".$templateTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentParentSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackParSec(".$templateID.",".$parentSecID.")\">".$parentSecTitle."</a></td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['CurrentSubSection']."</td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackSubSec(".$templateID.",".$parentSecID.",".$secID.")\">".$secTitle."</a></td></tr>";
$x .= "</table>";

$sql="SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,DateTimeModified
	FROM(
		SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,DateTimeModified
		FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($secID)." AND QCatID=".IntegerSafe($qCatID)."
	) as ipsq;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$dateTimeModified = $a[0]["DateTimeModified"];

$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFQCatBasicInformation'])."\r\n";
$x .="<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFQCatCodEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('QCatCodEng', 'QCatCodEng', $a[0]["QCatCodEng"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFQCatCodChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('QCatCodChi', 'QCatCodChi', $a[0]["QCatCodChi"], $OtherClass='', $OtherPar=array('maxlength'=>255)).$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('frmTitleEngEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFQCatDescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("QCatDescrEng", $taContents=$a[0]["DescrEng"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFQCatDescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("QCatDescrChi", $taContents=$a[0]["DescrChi"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFQCatDisplayMode']."</td>";
$x .= "<td>";
$x .= "<table style=\"width:100%\">";
$x .= "<tr><td style=\"border:0;width:30%\">".$indexVar['libappraisal_ui']->Get_Radio_Button("QCatMode_0","QCatDisplayMode", $Value=0, $isChecked=($a[0]["QuesDispMode"]==0&&sizeof($a)>0)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['0'], $Onclick="",$isDisabled=0)."</td><td style=\"border:0\"><span style=\"color:#999999\"><i>".$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['0']."</i></span></td></tr>";
$x .= "<tr><td style=\"border:0;width:30%\">".$indexVar['libappraisal_ui']->Get_Radio_Button("QCatMode_1","QCatDisplayMode", $Value=1, $isChecked=($a[0]["QuesDispMode"]==1&&sizeof($a)>0)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['1'], $Onclick="",$isDisabled=0)."</td><td style=\"border:0\"><span style=\"color:#999999\"><i>".$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['1']."</i></span></td></tr>";
$x .= "<tr><td style=\"border:0;width:30%\">".$indexVar['libappraisal_ui']->Get_Radio_Button("QCatMode_2","QCatDisplayMode", $Value=2, $isChecked=($a[0]["QuesDispMode"]==2&&sizeof($a)>0)?1:0, $Class="", $Display=$Lang['Appraisal']['TemplateSampleQCatDisplayMode']['2'], $Onclick="",$isDisabled=0)."</td><td style=\"border:0\"><span style=\"color:#999999\"><i>".$Lang['Appraisal']['TemplateSampleQCatDisplayModeDes']['2']."</i></span></td></tr>";
$x .= "</table>";
$x .= "</td></tr>";

$x .= "<tr id=\"trAvgMark\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFAvgMark']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("QCatAvgMark", "QCatAvgMark", $Value=1, $isChecked=($a[0]["IsAvgMark"]==1)?1:0, $Class="", $Display="", $Onclick="",$isDisabled=0)."</td></tr>";
$x .= "<tr id=\"trAvgDec\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFAvgDec']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("QCatAvgDec", "QCatAvgDec", $a[0]["AvgDec"], $OtherClass="", $OtherPar=array('maxlength'=>255));
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("QCatAvgDecEmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Number'], $Class='warnMsgDiv');
$x .= "</td></tr>";

$x .= "<tr id=\"trVerAvgMark\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFVerAvgMark']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->Get_Checkbox("QCatVerAvgMark", "QCatVerAvgMark", $Value=1, $isChecked=($a[0]["IsVerAvgMark"]==1)?1:0, $Class="", $Display="", $Onclick="",$isDisabled=0)."</td></tr>";
$x .= "<tr id=\"trVerAvgDec\"><td class=\"field_title\">".$Lang['Appraisal']['TemplateSample']['SDFVerAvgDec']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER("QCatVerAvgDec", "QCatVerAvgDec", $a[0]["VerAvgDec"], $OtherClass="", $OtherPar=array('maxlength'=>255));
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg("QCatVerAvgDecEmptyWarnDiv", $Lang['Appraisal']['TemplateSample']['Number'], $Class='warnMsgDiv');
$x .= "</td></tr>";


$x .="</table><br/>";
$x .= "<span class=\"row_content tabletextremark\">".$Lang['Appraisal']['TemplateSample']["ModifiedDate"].": ".(($dateTimeModified!="")?$dateTimeModified:" - ")."</span>";

$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">";
$x .= "<input type=\"hidden\" id=\"SecID\" name=\"SecID\" value=".$secID.">";
$x .= "<input type=\"hidden\" id=\"ParentSecID\" name=\"ParentSecID\" value=".$parentSecID.">";
$x .= "<input type=\"hidden\" id=\"QCatID\" name=\"QCatID\" value=".$qCatID.">";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
if($qCatID != "NULL" && $qCatID!="undefined"){
	$htmlAry['previewBtn'] = "<div class=\"edit_bottom_v30\"><p class=\"spacer\"></p>".$indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnPreview'], "button", "goPreview(".$templateID.")", 'previewBtn')."</div>";;
}
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackSubSec(".$templateID.",".$parentSecID.",".$secID.")", 'backBtn');
// ============================== Define Button ==============================

//======================================================================== Category Input ========================================================================//
$dispMode="";
$x="";
if($a[0]["QuesDispMode"]==1){
	$x = $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFLikertInput'])."\r\n";
	$x .= "<a name=\"anchLikert\"></a>";
	
	//templateID,parSecID,secID,qCatID,dispModeID
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddLikert('.$templateID.','.$parentSecID.','.$secID.','.$qCatID.','.$a[0]["QuesDispMode"].');', $Lang['Appraisal']['BtnAddLikert'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$dispMode=$a[0]["QuesDispMode"];
	$sql="SELECT MarkID,QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,Score,DisplayOrder
		FROM(
			SELECT MarkID,QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,Score,DisplayOrder
			FROM INTRANET_PA_S_QLKS WHERE QCatID=".IntegerSafe($qCatID)."				
		) as ipsq
		ORDER BY DisplayOrder;";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:15%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:15%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFMarkCodChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDescrChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFScore']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['MarkID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$markCodEng=($a[$i]["MarkCodEng"]=="")?$Lang['Appraisal']['TemplateSampleLikert']['NoTitleAndCod']:($a[$i]["MarkCodEng"]);
		$x .= "<td><a id='MarkCodEngID_'".$a[$i]['MarkID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MarkID'].")\">".$markCodEng."</a></td>";
		$markCodChi=($a[$i]["MarkCodChi"]=="")?$Lang['Appraisal']['TemplateSampleLikert']['NoTitleAndCod']:($a[$i]["MarkCodChi"]);
		$x .= "<td><a id='MarkCodChiID_'".$a[$i]['MarkID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MarkID'].")\">".$markCodChi."</a></td>";
		$markDescrEng=($a[$i]["MarkDescEng"]==""&&$a[$i]["DescrEng"]=="")?$Lang['Appraisal']['TemplateSampleLikert']['NoTitleAndCod']:($a[$i]["DescrEng"]);
		$x .= "<td><a id='MarkIDEngID_'".$a[$i]['MarkID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MarkID'].")\">".$markDescrEng."</a></td>"."\r\n";
		$markDescrChi=($a[$i]["MarkDescChi"]==""&&$a[$i]["DescrChi"]=="")?$Lang['Appraisal']['TemplateSampleLikert']['NoTitleAndCod']:($a[$i]["DescrChi"]);
		$x .= "<td><a id='MarkChiID_'".$a[$i]['MarkID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MarkID'].")\">".$markDescrChi."</a></td>"."\r\n";
		$x .= "<td><a id='MarkScoreID_'".$a[$i]['MarkID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MarkID'].")\">".$a[$i]["Score"]."</a></td>"."\r\n";		
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['MarkID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLikert(".$templateID.",".$a[$i]['MarkID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"MarkID_".$a[$i]["MarkID"]."\" name=\"MarkID_".$a[$i]["MarkID"]."\" value=".$a[$i]["MarkID"]."></td>"."\r\n";
		$x .= "</tr>";
	}	
	$x .= "</tbody>"."\r\n";
	$x .= "</table>";
	$x .= "</div>";
	$x .= "<input type=\"hidden\" id=\"DisplayMode\" name=\"DisplayMode\" value='Likert'>";
}
else if($a[0]["QuesDispMode"]==2){
	$x = $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFMatrixInput'])."\r\n";
	$x .= "<a name=\"anchMatrix\"></a>";
	
	$dispMode=$a[0]["QuesDispMode"];	
	$subBtnAry = array();
	$btnAry[] = array('new', 'javascript: goAddMatrix('.$templateID.','.$parentSecID.','.$secID.','.$qCatID.','.$a[0]["QuesDispMode"].');', $Lang['Appraisal']['BtnAddMatrix'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
	
	$sql="SELECT MtxFldID,QCatID,FldHdrChi,FldHdrEng,
		CASE WHEN InputType=0 THEN '".$Lang['Appraisal']['TemplateMatrix']['SDFInputText']."' WHEN InputType=1 THEN '".$Lang['Appraisal']['TemplateMatrix']['SDFInputNumeric']."'
			 WHEN InputType=2 THEN '".$Lang['Appraisal']['TemplateMatrix']['SDFInputDate']."' WHEN InputType=3 THEN '".$Lang['Appraisal']['TemplateMatrix']['SDFInputMultipleText']."' END as InputType,DisplayOrder
		FROM(
			SELECT MtxFldID,QCatID,FldHdrChi,FldHdrEng,InputType,DisplayOrder
			FROM INTRANET_PA_S_QMTX  WHERE QCatID=".IntegerSafe($qCatID)."
		) as ipsq
		ORDER BY DisplayOrder;";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateMatrix']['SDFFldHdrEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:20%;\">".$Lang['Appraisal']['TemplateMatrix']['SDFInputType']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateSampleLikert']['SDFDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['MtxFldID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
		$mtxFldEng=($a[$i]["FldHdrEng"]=="")?$Lang['Appraisal']['TemplateMatrix']['NoTitleAndCod']:($a[$i]["FldHdrEng"]);
		$x .= "<td><a id='MtxHdrID_'".$a[$i]['MtxFldID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MtxFldID'].")\">".$mtxFldEng."</a></td>"."\r\n";
		$mtxFldChi=($a[$i]["FldHdrChi"]=="")?$Lang['Appraisal']['TemplateMatrix']['NoTitleAndCod']:($a[$i]["FldHdrChi"]);
		$x .= "<td><a id='MtxHdrChiID_'".$a[$i]['MtxFldID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MtxFldID'].")\">".$mtxFldChi."</a></td>"."\r\n";
		$x .= "<td><a id='MtxInputID_'".$a[$i]['MtxFldID']." class=\"tablelink\" href=\"javascript:goEditDispMode(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$dispMode.",".$a[$i]['MtxFldID'].")\">".$a[$i]["InputType"]."</a></td>"."\r\n";
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['MtxFldID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteMatrix(".$templateID.",".$a[$i]['MtxFldID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"MarkID_".$a[$i]["MtxFldID"]."\" name=\"MarkID_".$a[$i]["MtxFldID"]."\" value=".$a[$i]["MtxFldID"]."></td>"."\r\n";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div>";
	$x .= "<input type=\"hidden\" id=\"DisplayMode\" name=\"DisplayMode\" value='Matrix'>";
}
$x.= "<br>\r\n";
$htmlAry['contentTbl2'] = $x;
//======================================================================== Questions Category ========================================================================//
if($qCatID != "NULL" && $qCatID!="undefined"){	
	$x = $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['TemplateSample']['SDFQuestion'])."\r\n";
	$x .= "<a name=\"anchQuestion\"></a>";
	
	$subBtnAry = array();
	$btnAry = array();
	$btnAry[] = array('new', 'javascript: goAddQues('.$templateID.','.$parentSecID.','.$secID.','.$qCatID.');', $Lang['Appraisal']['BtnAddQues'], $subBtnAry);
	$x .= $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
		
	$sql="SELECT QID,QCatID,QCodChi,QCodEng,DescrChi,DescrEng,DisplayOrder,InputType,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng
		FROM(
			SELECT QID,QCatID,QCodChi,QCodEng,DescrChi,DescrEng,DisplayOrder,InputType,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng
			FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($qCatID)."
		) as ipsq ORDER BY DisplayOrder";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	
	$x .= "<div id=\"DetailLayer\"><span>";
	$x .= "<table id=\"dndTable2\" class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateQues']['SDFQCodEng']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:30%;\">".$Lang['Appraisal']['TemplateQues']['SDFQCodChi']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateQuestion']['SDFDisplayOrder']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:10%;\">".$Lang['Appraisal']['TemplateQuestion']['SDFDelete']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		$x .= "<tr id=\"".$a[$i]['QID']."\">"."\r\n";
		$x .= "<td>".$j."</td>"."\r\n";
//		$quesEng=($a[$i]["QCodEng"]==""&&$a[$i]["DescrEng"]=="")?$Lang['Appraisal']['TemplateQuestion']['NoTitleAndCod']:($a[$i]["QCodEng"]."-".$a[$i]["DescrEng"]);
		$quesEng=($a[$i]["QCodEng"]=="")?$Lang['Appraisal']['TemplateQuestion']['NoTitleAndCod']:($a[$i]["QCodEng"]);
		$x .= "<td><a id='QuesID_'".$a[$i]['QID']." class=\"tablelink\" href=\"javascript:goEditQues(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$a[$i]['QID'].")\">".$quesEng."</a></td>"."\r\n";
//		$quesChi=($a[$i]["QCodChi"]==""&&$a[$i]["DescrChi"]=="")?$Lang['Appraisal']['TemplateQuestion']['NoTitleAndCod']:($a[$i]["QCodChi"]."-".$a[$i]["DescrChi"]);
		$quesChi=($a[$i]["QCodChi"]=="")?$Lang['Appraisal']['TemplateQuestion']['NoTitleAndCod']:($a[$i]["QCodChi"]);
		$x .= "<td><a id='QuesID_'".$a[$i]['QID']." class=\"tablelink\" href=\"javascript:goEditQues(".$templateID.",".$parentSecID.",".$secID.",".$qCatID.",".$a[$i]['QID'].")\">".$quesChi."</a></td>"."\r\n";
		$x .= "<td class=\"Dragable\"><div class=\"table_row_tool\"><a href=\"#\" class=\"move_order_dim\" title=\"".$Lang['SysMgr']['FormClassMapping']['Move']."\"></a></div></td>"."\r\n";
		$x .= "<td><div class=\"table_row_tool\" id='delete_".$a[$i]['MtxFldID']."'><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteQues(".$templateID.",".$a[$i]['QID'].") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div></td>"."\r\n";
		$x .= "<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=".$a[$i]["QID"]."></td>"."\r\n";
		$x .= "</tr>";	
	}
	$x .= "</table>";	
	$htmlAry['contentTbl3'] = $x;
}

?>