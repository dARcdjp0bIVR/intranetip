<?php 
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$connection = new libgeneralsettings();

$returnPath=$_POST["returnPath"];
$formType=$_POST["type"];
$saveSuccess=true;

if($formType=="PDFSave"){
	$FrmTitleChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmTitleChi"])."'";
	$FrmTitleEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmTitleEng"])."'";
	$FrmCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmCodChi"])."'";
	$FrmCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmCodEng"])."'";
	$HdrRefChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["HdrRefChi"])."'";
	$HdrRefEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["HdrRefEng"])."'";
	$ObjChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["ObjChi"])."'";
	$ObjEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["ObjEng"])."'";
	$DescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrChi"])."'";
	$DescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrEng"])."'";
	$IsActive=$_POST["Status"];
	$IsDataPreload=$_POST["DataPreload"];
	$AppTgtChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AppTgtChi"])."'";
	$AppTgtEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AppTgtEng"])."'";
	$TemplateID=$_POST["TemplateID"];	
	$DataTypeArr=explode(",",$_POST["DataTypeStr"]);	
	$checkSql = "SELECT CycleID FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$TemplateID."'";
	$data = $connection->returnVector($checkSql);
	$checking=empty($data);
	if($checking==false && $IsActive==false){
		$returnMsgKey = 'UpdateUnsuccess';
		//echo $returnPath."<br/>";
		header($returnPath."&returnMsgKey=".$returnMsgKey);
		die();
	}
	//======================================================================== INTRANET_PA_S_FRMTPL ========================================================================//
	$sql="UPDATE INTRANET_PA_S_FRMTPL SET FrmTitleChi=".$FrmTitleChi.",FrmTitleEng=".$FrmTitleEng.",FrmCodChi=".$FrmCodChi.",FrmCodEng=".$FrmCodEng.",";
	$sql.="HdrRefChi=".$HdrRefChi.",HdrRefEng=".$HdrRefEng.",ObjChi=".$ObjChi.",ObjEng=".$ObjEng.",DescrChi=".$DescrChi.",DescrEng=".$DescrEng.",IsActive=".$IsActive.",";
	$sql.="AppTgtChi=".$AppTgtChi.",AppTgtEng=".$AppTgtEng.",IsDataPreload=".$IsDataPreload.",";
	$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
	$sql.="WHERE TemplateID=".IntegerSafe($TemplateID).";";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);	
	//======================================================================== INTRANET_PA_S_FRMPRE  ========================================================================//
	for($i=0;$i<sizeof($DataTypeArr);$i++){
		$dataType=$DataTypeArr[$i];
		$DSDescriChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DSDescrChi_".$dataType])."'";
		$DSDescriEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DSDescrEng_".$dataType])."'";
		$DSIncl=($_POST["Include_".$dataType]==null)?"0":"1";
		$DSPageBreak=($_POST["PageBreak_".$dataType]==null)?"0":"1";
		$DataTypeID=$_POST["DataTypeID_".$dataType];
		$sql="UPDATE INTRANET_PA_S_FRMPRE SET DescrChi=".$DSDescriChi.",DescrEng=".$DSDescriEng.",Incl=".IntegerSafe($DSIncl).",PageBreakAfter=".IntegerSafe($DSPageBreak).",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE TemplateID=".IntegerSafe($TemplateID)." AND DataTypeID=".$DataTypeID.";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);		
	}
}
else if($formType=="SDFSave"){
	$FrmTitleChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmTitleChi"])."'";
	$FrmTitleEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmTitleEng"])."'";
	$FrmCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmCodChi"])."'";
	$FrmCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FrmCodEng"])."'";
	$HdrRefChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["HdrRefChi"])."'";
	$HdrRefEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["HdrRefEng"])."'";
	$ObjChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["ObjChi"])."'";
	$ObjEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["ObjEng"])."'";
	$DescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrChi"])."'";
	$DescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrEng"])."'";
	$IsActive=$_POST["Status"];
	$AppTgtChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AppTgtChi"])."'";
	$AppTgtEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AppTgtEng"])."'";
	$NeedClass=($_POST["NeedClass"]!=null)?1:0;
	$NeedSubject=($_POST["NeedSubject"]!=null)?1:0;
	$NeedSubjLang = ($_POST["NeedSubjLang"]!=null)?1:0;
	$IsObs=($_POST["IsObs"]!=null)?1:0;
	$TemplateID=$_POST["TemplateID"];
	if($IsObs==1){
		$checkSql = "SELECT CycleID FROM INTRANET_PA_C_OBSSEL WHERE TemplateID='".$TemplateID."'";	
	}else{
		$checkSql = "SELECT CycleID FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$TemplateID."'";		
	}
	$data = $connection->returnVector($checkSql);
	$checking = empty($data);		
	if($checking==false && $IsActive==false){
		$returnMsgKey = 'UpdateUnsuccess';
		//echo $returnPath."<br/>";
		header($returnPath."&returnMsgKey=".$returnMsgKey);
		die();
	}
	//======================================================================== INTRANET_PA_S_FRMTPL ========================================================================//
	$sql="SELECT TemplateID FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=".IntegerSafe($TemplateID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_S_FRMTPL SET FrmTitleChi=".$FrmTitleChi.",FrmTitleEng=".$FrmTitleEng.",FrmCodChi=".$FrmCodChi.",FrmCodEng=".$FrmCodEng.",";
		$sql.="HdrRefChi=".$HdrRefChi.",HdrRefEng=".$HdrRefEng.",ObjChi=".$ObjChi.",ObjEng=".$ObjEng.",DescrChi=".$DescrChi.",DescrEng=".$DescrEng.",IsActive=".$IsActive.",";
		$sql.="AppTgtChi=".$AppTgtChi.",AppTgtEng=".$AppTgtEng.",NeedClass=".IntegerSafe($NeedClass).",NeedSubj=".$NeedSubject.",IsObs=".$IsObs.",NeedSubjLang=".$NeedSubjLang.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE TemplateID=".IntegerSafe($TemplateID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_FRMTPL;";
		$a=$connection->returnResultSet($sql);
		$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
		$sql="INSERT INTO INTRANET_PA_S_FRMTPL(TemplateType,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,HdrRefChi,HdrRefEng,ObjChi,ObjEng,DescrChi,DescrEng,DisplayOrder,IsActive,NeedClass,NeedSubj,NeedSubjLang,AppTgtChi,AppTgtEng,IsObs,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
 		$sql.="SELECT * FROM (SELECT 1 as TemplateType,".$FrmTitleChi." as FrmTitleChi,".$FrmTitleEng." as FrmTitleEng,".$FrmCodChi." as FrmCodChi,".$FrmCodEng." as FrmCodEng,".$HdrRefChi." as HdrRefChi,".$HdrRefEng." as HdrRefEng,";
 		$sql.=$ObjChi." as ObjChi,".$ObjEng." as ObjEng,".$DescrChi." as DescrChi,".$DescrEng." as DescrEng,".$MaxDisplayOrder." as DisplayOrder,".$IsActive." as IsActive,".$NeedClass." as NeedClass,".$NeedSubject." as NeedSubj,".$NeedSubjLang." as NeedSubjLang,";
 		$sql.=$AppTgtChi." as AppTgtChi,".$AppTgtEng." as AppTgtEng,".$IsObs." as IsObs,";
 		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
 		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
 		//echo $sql."<br/><br/>";
 		$a=$connection->db_db_query($sql);
 		$sql="SELECT TemplateID FROM INTRANET_PA_S_FRMTPL WHERE TemplateType=1 AND FrmTitleChi=".$FrmTitleChi." AND FrmTitleEng=".$FrmTitleEng." ORDER BY DateTimeInput DESC;";
 		//echo $sql."<br/><br/>";
 		$a=$connection->returnResultSet($sql);
 		$getTemplateID=$a[0]["TemplateID"];
 		$returnPath=sprintf($returnPath,$getTemplateID);
 		$returnPath=$returnPath."#anchParSec";
	}
}
else if($formType=="SDFSecSave"){
	$TemplateID=$_POST["TemplateID"];
	$SecID=$_POST["SecID"];
	$AppRoleIDArr=explode(",",$_POST["AppRoleIDStr"]);	
	$SecCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecCodEng"])."'";
	$SecCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecCodChi"])."'";
	$SecTitleEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecTitleEng"])."'";
	$SecTitleChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecTitleChi"])."'";
	$SecDescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecDescrEng"])."'";
	$SecDescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SecDescrChi"])."'";
	//======================================================================== INTRANET_PA_S_FRMSEC ========================================================================//
	$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($TemplateID)." AND SecID=".IntegerSafe($SecID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_S_FRMSEC SET SecTitleChi=".$SecTitleChi.",SecTitleEng=".$SecTitleEng.",SecCodChi=".$SecCodChi.",SecCodEng=".$SecCodEng.",";
		$sql.="SecDescrChi=".$SecDescrChi.",SecDescrEng=".$SecDescrEng.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE TemplateID=".IntegerSafe($TemplateID)." AND SecID=".IntegerSafe($SecID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($TemplateID).";";//." AND SecID=".IntegerSafe($SecID).";";
		$a=$connection->returnResultSet($sql);
		$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;		
		$sql="INSERT INTO INTRANET_PA_S_FRMSEC(TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$TemplateID." as TemplateID,".$SecTitleChi." as SecTitleChi,".$SecTitleEng." as SecTitleEng,".$SecCodChi." as SecCodChi,".$SecCodEng." as SecCodEng,";
		$sql.=$SecDescrChi." as SecDescrChi,".$SecDescrEng." as SecDescrEng,".$MaxDisplayOrder." as DisplayOrder,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".$TemplateID." AND DisplayOrder=".$MaxDisplayOrder.";";
		$a=$connection->returnResultSet($sql);
		$getSecID=$a[0]["SecID"];
		$returnPath=sprintf($returnPath,$getSecID);
		$returnPath=$returnPath."#anchSubSec";
	}	
	//======================================================================== INTRANET_PA_S_SECROL ========================================================================//
	if($SecID==""||$SecID=="NULL"){
		$SecID=$getSecID;
	}
	for($i=0;$i<sizeof($AppRoleIDArr);$i++){
		$sql="SELECT SecID FROM INTRANET_PA_S_SECROL WHERE SecID=".IntegerSafe($SecID)." AND AppRoleID=".IntegerSafe($AppRoleIDArr[$i]).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		$CanBrowse=($_POST["CanBrowse_".$AppRoleIDArr[$i]]!=null)?1:0;
		$CanFill=($_POST["CanFill_".$AppRoleIDArr[$i]]!=null)?1:0;
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_S_SECROL SET CanBrowse=".IntegerSafe($CanBrowse).",CanFill=".IntegerSafe($CanFill).",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE SecID=".IntegerSafe($SecID)." AND AppRoleID=".IntegerSafe($AppRoleIDArr[$i]).";";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
		else{
			$sql="INSERT INTO INTRANET_PA_S_SECROL(SecID,AppRoleID,CanBrowse,CanFill,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM(SELECT ".$SecID." as SecID,".$AppRoleIDArr[$i]." as AppRoleID,".IntegerSafe($CanBrowse)." as CanBrowse,".IntegerSafe($CanFill)." as CanFill,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
	}
}
else if($formType=="SDFSubSecSave"){
	$TemplateID=$_POST["TemplateID"];
	$SecID=$_POST["SecID"];
	$ParentSecID=$_POST["ParentSecID"];
	$SubSecCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SubSecCodEng"])."'";
	$SubSecCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SubSecCodChi"])."'";
	$SubSecTitleEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SubSecTitleEng"])."'";
	$SubSecTitleChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["SubSecTitleChi"])."'";
	$SubSecDescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query(nl2br($_POST["SubSecDescrEng"]))."'";
	$SubSecDescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query(nl2br($_POST["SubSecDescrChi"]))."'";
	//======================================================================== INTRANET_PA_S_FRMSEC ========================================================================//
	$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($TemplateID)." AND SecID=".IntegerSafe($SecID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_S_FRMSEC SET SecTitleChi=".$SubSecTitleChi.",SecTitleEng=".$SubSecTitleEng.",SecCodChi=".$SubSecCodChi.",SecCodEng=".$SubSecCodEng.",";
		$sql.="SecDescrChi=".$SubSecDescrChi.",SecDescrEng=".$SubSecDescrEng.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE TemplateID=".IntegerSafe($TemplateID)." AND SecID=".IntegerSafe($SecID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($TemplateID)." AND ParentSecID=".IntegerSafe($ParentSecID).";";
		$a=$connection->returnResultSet($sql);
		$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
		$sql="INSERT INTO INTRANET_PA_S_FRMSEC(TemplateID,SecTitleChi,SecTitleEng,SecCodChi,SecCodEng,SecDescrChi,SecDescrEng,DisplayOrder,ParentSecID,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$TemplateID." as TemplateID,".$SubSecTitleChi." as SecTitleChi,".$SubSecTitleEng." as SecTitleEng,".$SubSecCodChi." as SecCodChi,".$SubSecCodEng." as SecCodEng,";
		$sql.=$SubSecDescrChi." as SecDescrChi,".$SubSecDescrEng." as SecDescrEng,".$MaxDisplayOrder." as DisplayOrder,".IntegerSafe($ParentSecID)." as ParentSecID,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".$TemplateID." AND DisplayOrder=".$MaxDisplayOrder." AND ParentSecID=".IntegerSafe($ParentSecID).";";
		$a=$connection->returnResultSet($sql);
		$getSecID=$a[0]["SecID"];
		$returnPath=sprintf($returnPath,$getSecID);
		$returnPath=$returnPath."#anchQCat";
	}	
}
else if($formType=="SDFQCatSave"){
	$TemplateID=$_POST["TemplateID"];
	$SecID=$_POST["SecID"];
	$ParentSecID=$_POST["ParentSecID"];
	$QCatID=$_POST["QCatID"];
	$QCatCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCatCodEng"])."'";
	$QCatCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCatCodChi"])."'";
	$QCatDescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCatDescrEng"])."'";
	$QCatDescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCatDescrChi"])."'";
	$QCatDisplayMode=$_POST["QCatDisplayMode"];
	$QCatAvgMark=($_POST["QCatAvgMark"]=="1")?1:0;
	$QCatAvgDec="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCatAvgDec"])."'";
	$QCatVerAvgMark=($_POST["QCatVerAvgMark"]=="1")?1:0;
	$QCatVerAvgDec="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCatVerAvgDec"])."'";
	
	$sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE QCatID=".IntegerSafe($QCatID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_S_QCAT SET QCatCodChi=".$QCatCodChi.",QCatCodEng=".$QCatCodEng.",";
		$sql.="DescrChi=".$QCatDescrChi.",DescrEng=".$QCatDescrEng.",QuesDispMode=".IntegerSafe($QCatDisplayMode).",";
		$sql.="IsAvgMark=".$QCatAvgMark.",AvgDec=".$QCatAvgDec.",IsVerAvgMark=".$QCatVerAvgMark.",VerAvgDec=".$QCatVerAvgDec.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE SecID=".IntegerSafe($SecID)." AND QCatID=".IntegerSafe($QCatID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID=".IntegerSafe($SecID).";";
		$a=$connection->returnResultSet($sql);
		$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
		
		$sql="INSERT INTO INTRANET_PA_S_QCAT(SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$SecID." as SecID,".$QCatCodChi." as QCatCodChi,".$QCatCodEng." as QCatCodEng,".$QCatDescrChi." as DescrChi,".$QCatDescrEng." as DescrEng,";
		$sql.=$MaxDisplayOrder." as DisplayOrder,".$QCatDisplayMode." as QuesDispMode,".$QCatAvgMark." as IsAvgMark,".$QCatAvgDec." as AvgDec,".$QCatVerAvgMark." as IsVerAvgMark,".$QCatVerAvgDec." as VerAvgDec,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID=".$SecID." AND DisplayOrder=".$MaxDisplayOrder." AND QuesDispMode=".IntegerSafe($QCatDisplayMode).";";
		$a=$connection->returnResultSet($sql);
		$getQCatID=$a[0]["QCatID"];
		$returnPath=sprintf($returnPath,$getQCatID);
		if($QCatDisplayMode==1){
			$returnPath=$returnPath."#anchLikert";
		}
		else if($QCatDisplayMode==2){
			$returnPath=$returnPath."#anchMatrix";
		}
		else{
			$returnPath=$returnPath."#anchQuestion";
		}
		$QCatID = $getQCatID;
	}
//	if($QCatDisplayMode==2){
//		$sql = "SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID='".$QCatID."'";
//		$a=$connection->returnResultSet($sql);
//		if(sizeof($a)==0){
//			$sql="INSERT INTO INTRANET_PA_S_QUES(QCatID,DisplayOrder,InputType,HasRmk,IsMand,IsAvgMark,AvgDec,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
//			$sql.="SELECT * FROM(SELECT ".$QCatID." as QCatID, 1 as DisplayOrder,0 as InputType,";
//			$sql.= "0 as HasRmk,0 as IsMand,0 as IsAvgMark,0 as AvgDec,";
//			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
//			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";		
//			//echo $sql."<br/><br/>";
//			$a=$connection->db_db_query($sql);
//		}
//	}
}
else if($formType=="SDFDispModeSave"){
	$TemplateID=$_POST["TemplateID"];
	$SecID=$_POST["SecID"];
	$QCatID=$_POST["QCatID"];
	$dispModeID=$_POST["DispModeID"];
	
	if($dispModeID==1){
		$InputID=$_POST["InputID"];
		$MarkCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["MarkCodEng"])."'";
		$MarkCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["MarkCodChi"])."'";
		$MarkDescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["MarkDescrEng"])."'";
		$MarkDescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["MarkDescrChi"])."'";
		$Score=$_POST["Score"];
		
		$sql="SELECT MarkID FROM INTRANET_PA_S_QLKS WHERE MarkID=".IntegerSafe($InputID).";";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_S_QLKS SET MarkCodChi=".$MarkCodChi.",MarkCodEng=".$MarkCodEng.",DescrChi=".$MarkDescrChi.",DescrEng=".$MarkDescrEng.",";
			$sql.="Score=".IntegerSafe($Score).",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE MarkID=".IntegerSafe($InputID)." AND QCatID=".IntegerSafe($QCatID).";";
			// echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
		else{
			$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_QLKS WHERE QCatID=".IntegerSafe($QCatID).";";
			$a=$connection->returnResultSet($sql);
			$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;			
			$sql="INSERT INTO INTRANET_PA_S_QLKS(QCatID,MarkCodChi,MarkCodEng,DescrChi,DescrEng,Score,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM (SELECT ".$QCatID." as QCatID,".$MarkCodChi." as MarkCodChi,".$MarkCodEng." as MarkCodEng,".$MarkDescrChi." as DescrChi,".$MarkDescrEng." as DescrEng,";
			$sql.=IntegerSafe($Score)." as Score,".$MaxDisplayOrder." as DisplayOrder,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
			$sql="SELECT MarkID FROM INTRANET_PA_S_QLKS WHERE QCatID=".$QCatID." AND DisplayOrder=".$MaxDisplayOrder." AND MarkCodChi=".$MarkCodChi." AND MarkCodEng=".$MarkCodEng.";";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			$getMarkID=$a[0]["MarkID"];
			$returnPath=sprintf($returnPath,$getMarkID);
		}		
	}
	else if($dispModeID==2){
		$InputID=$_POST["InputID"];
		$FldHdrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FldHdrEng"])."'";
		$FldHdrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["FldHdrChi"])."'";
		$InputType=$_POST["InputType"];
		
		$sql="SELECT MtxFldID FROM INTRANET_PA_S_QMTX WHERE MtxFldID=".IntegerSafe($InputID).";";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_S_QMTX SET FldHdrChi=".$FldHdrChi.",FldHdrEng=".$FldHdrEng.",";
			$sql.="InputType=".IntegerSafe($InputType).",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE MtxFldID=".IntegerSafe($InputID)." AND QCatID=".IntegerSafe($QCatID).";";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
		}
		else {
			$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_QMTX WHERE QCatID=".IntegerSafe($QCatID).";";
			$a=$connection->returnResultSet($sql);
			$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
			$sql="INSERT INTO INTRANET_PA_S_QMTX(QCatID,FldHdrChi,FldHdrEng,InputType,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM (SELECT ".$QCatID." as QCatID,".$FldHdrChi." as FldHdrChi,".$FldHdrEng." as FldHdrEng,";
			$sql.=IntegerSafe($InputType)." as InputType,".$MaxDisplayOrder." as DisplayOrder,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);
			$sql="SELECT MtxFldID FROM INTRANET_PA_S_QMTX WHERE QCatID=".$QCatID." AND DisplayOrder=".$MaxDisplayOrder." AND FldHdrChi=".$FldHdrChi." AND FldHdrEng=".$FldHdrEng.";";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			$getMtxFldID=$a[0]["MtxFldID"];
			$returnPath=sprintf($returnPath,$getMtxFldID);
		}
	}	
}
else if($formType=="SDFQuesSave"){
	$QCodEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCodEng"])."'";;
	$QCodChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QCodChi"])."'";;
	$DescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query(nl2br($_POST["DescrEng"]))."'";;
	$DescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query(nl2br($_POST["DescrChi"]))."'";;
	$InputType=$_POST["InputType"];
	$HasRmk=($_POST["HasRmk"]!=null)?1:0;
	$RmkLabelEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["RmkLabelEng"])."'";
	$RmkLabelChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["RmkLabelChi"])."'";
	$TxtBoxNote="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["TxtBoxNote"])."'";;
	$Mand="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QMandatory"])."'";;
	$AvgMark=($_POST["AvgMark"]=="1")?1:0;
	$AvgDec="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["AvgDec"])."'";
	
	$QID=$_POST["QID"];
	$QCatID=$_POST["QCatID"];
	$sql="SELECT QID FROM INTRANET_PA_S_QUES WHERE QID=".IntegerSafe($QID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_S_QUES SET QCodEng=".$QCodEng.",QCodChi=".$QCodChi.",DescrChi=".$DescrChi.",DescrEng=".$DescrEng.",InputType=".IntegerSafe($InputType).",TxtBoxNote=".$TxtBoxNote.",";
		$sql.="HasRmk=".IntegerSafe($HasRmk).",RmkLabelEng=".$RmkLabelEng.",RmkLabelChi=".$RmkLabelChi.",IsMand=".$Mand.",IsAvgMark=".$AvgMark.",AvgDec=".$AvgDec.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE QID=".IntegerSafe($QID)." AND QCatID=".IntegerSafe($QCatID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_QUES WHERE QCatID=".IntegerSafe($QCatID).";";
		$a=$connection->returnResultSet($sql);
		$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
		
		$sql="INSERT INTO INTRANET_PA_S_QUES(QCatID,QCodChi,QCodEng,DescrChi,DescrEng,DisplayOrder,InputType,TxtBoxNote,HasRmk,RmkLabelChi,RmkLabelEng,IsMand,IsAvgMark,AvgDec,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$QCatID." as QCatID,".$QCodChi." as QCodChi,".$QCodEng." as QCodEng,".$DescrChi." as DescrChi,".$DescrEng." as DescrEng,".$MaxDisplayOrder." as DisplayOrder,".IntegerSafe($InputType)." as InputType,";
		$sql.=$TxtBoxNote." as TxtBoxNote,".$HasRmk." as HasRmk,".$RmkLabelChi." as RmkLabelChi,".$RmkLabelEng." as RmkLabelEng,".$Mand." as IsMand,".$AvgMark." as IsAvgMark,".$AvgDec." as AvgDec,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";		
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql="SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID=".$QCatID." AND DisplayOrder=".$MaxDisplayOrder." AND QCodChi=".$QCodChi." AND QCodEng=".$QCodEng.";";
		$a=$connection->returnResultSet($sql);
		$getQID=$a[0]["QID"];
		$returnPath=sprintf($returnPath,$getQID);
		$returnPath=$returnPath."#anchAns";
	}	
}
else if($formType=="SDFQAnsSave"){
	$QAnsEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QAnsEng"])."'";
	$QAnsChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["QAnsChi"])."'";
	$DescrEng="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrEng"])."'";
	$DescrChi="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["DescrChi"])."'";
	$QID=$_POST["QID"];
	$QAnsID=$_POST["QAnsID"];
	
	$sql="SELECT QAnsID FROM INTRANET_PA_S_QANS WHERE QID=".IntegerSafe($QID)." AND QAnsID=".IntegerSafe($QAnsID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_S_QANS SET QAnsEng=".$QAnsEng.",QAnsChi=".$QAnsChi.",DescrEng=".$DescrEng.",DescrChi=".$DescrChi.",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE QID=".IntegerSafe($QID)." AND QAnsID=".IntegerSafe($QAnsID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
	}
	else{
		$sql="SELECT MAX(DisplayOrder) as DisplayOrder FROM INTRANET_PA_S_QANS WHERE QID=".IntegerSafe($QID).";";
		$a=$connection->returnResultSet($sql);
		$MaxDisplayOrder=$a[0]["DisplayOrder"]+1;
		
		$sql="INSERT INTO INTRANET_PA_S_QANS(QID,QAnsChi,QAnsEng,DescrChi,DescrEng,DisplayOrder,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT * FROM(SELECT ".$QID." as QID,".$QAnsChi." as QAnsChi,".$QAnsEng." as QAnsEng,".$DescrChi." as DescrChi,".$DescrEng." as DescrEng,".$MaxDisplayOrder." as DisplayOrder,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
		//echo $sql."<br/><br/>";
		$a=$connection->db_db_query($sql);
		$sql="SELECT QAnsID FROM INTRANET_PA_S_QANS WHERE QID=".$QID." AND DisplayOrder=".$MaxDisplayOrder." AND QAnsChi=".$QAnsChi." AND QAnsEng=".$QAnsEng.";";
		$a=$connection->returnResultSet($sql);
		$getQAnsID=$a[0]["QAnsID"];
		$returnPath=sprintf($returnPath,$getQAnsID);
	}
	
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
//echo $returnPath."<br/>";
header($returnPath."&returnMsgKey=".$returnMsgKey);


?>