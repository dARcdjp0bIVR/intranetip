<?php
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['SettingsBasicSettings']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

// ============================== Includes files/libraries ==============================

// ============================== Transactional data ==============================
$lgeneralsettings = new libgeneralsettings();

$x = "";
if($_GET['editPart']=='1'){
    $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['SettingsBasicSettings']);
    $x .= '<table class="form_table_v30">'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsExcludeTeachers'];
    $x .= '</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= '<table class="inside_form_table">';
    $x .= '<tr>';
    $x .= '<td style="border-bottom:0px">'.$indexVar['libappraisal']->getExemptTeacherDisplay("nameSelectionList").'</td>';
    $x .= '<td style="border-bottom:0px">';
    $x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelect()", 'selectBtn')."<br>\r\n";
    $x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "goDeleteMember()", 'submitBtn');
    $x .= '<td>';
    $x .= '</tr>';
    $x .= '<tr>';
    $x .= '<td style="border-bottom:0px">';
    $x .= $Lang['Appraisal']['ReportPersonResult']['LoginID'];
    $x .= '</td>';
    $x .= '<td style="border-bottom:0px">';
    $x .= '<input type="text" id="exemptTeacher" name="exemptTeacher" value="" autocomplete="off" class="ac_input">';
    $x .= '</td>';
    $x .= '</tr>';
    $x .= '</table>';
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsPrintMcDisplayMode'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getPrintMcDisplayModeDisplay("edit");
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    if($sys_custom['eAppraisal']['AverageReport']){
        $x .= '<tr>'."\r\n";
        $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsReportDoubleCountPerson'];
        $x .= '</td>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= '<table class="inside_form_table">';
        $x .= '<tr>';
        $x .= '<td style="border-bottom:0px">'.$indexVar['libappraisal']->getReportDoubleCountPerson("nameSelectionList").'</td>';
        $x .= '<td style="border-bottom:0px">';
        $x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelectDoubleCountPerson()", 'selectBtn')."<br>\r\n";
        $x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "goDeleteDoubleCountPerson()", 'submitBtn');
        $x .= '<td>';
        $x .= '</tr>';
        $x .= '<tr>';
        $x .= '<td style="border-bottom:0px">';
        $x .= $Lang['Appraisal']['ReportPersonResult']['LoginID'];
        $x .= '</td>';
        $x .= '<td style="border-bottom:0px">';
        $x .= '<input type="text" id="reportDoubleCountPerson" name="reportDoubleCountPerson" value="" autocomplete="off" class="ac_input">';
        $x .= '</td>';
        $x .= '</tr>';
        $x .= '</table>';
        $x .= '</td>'."\r\n";
        $x .= '</tr>'."\r\n";
    }
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsReportDisplayPerson'].'<br><br>'.$Lang['Appraisal']['SettingsReportDisplayPersonRemarks'];
    $x .= '</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= '<table class="inside_form_table">';
    $x .= '<tr>';
    $x .= '<td style="border-bottom:0px">'.$indexVar['libappraisal']->getReportDisplayPerson("nameSelectionList").'</td>';
    $x .= '<td style="border-bottom:0px">';
    $x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelectReportDisplayPerson()", 'selectBtn')."<br>\r\n";
    $x .= $indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "goDeleteReportDisplayPerson()", 'submitBtn');
    $x .= '<td>';
    $x .= '</tr>';
    $x .= '<tr>';
    $x .= '<td style="border-bottom:0px">';
    $x .= $Lang['Appraisal']['ReportPersonResult']['LoginID'];
    $x .= '</td>';
    $x .= '<td style="border-bottom:0px">';
    $x .= '<input type="text" id="reportDisplayPerson" name="reportDisplayPerson" value="" autocomplete="off" class="ac_input">';
    $x .= '</td>';
    $x .= '</tr>';
    $x .= '</table>';
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    
    if($sys_custom['eAppraisal']['3YearsCPD']==true){
        $x .= '<tr>'."\r\n";
        $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsCPDRecords'].'</td>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= $indexVar['libappraisal']->getCPDSetting("edit");
        $x .= '</td>'."\r\n";
        $x .= '</tr>'."\r\n";
    }
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsCPDDuration'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getCPDDurationSetting("edit");
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsCPDShowingTotalHours'] .'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getCPDShowingTotalHoursSetting("edit");
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsAttendanceReasons'].'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getAttendanceReasons("edit");
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '<tr>'."\r\n";
    $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsColumnReasons'] .'</td>'."\r\n";
    $x .= '<td>'."\r\n";
    $x .= $indexVar['libappraisal']->getAttendanceTableColumn("edit");
    $x .= '</td>'."\r\n";
    $x .= '</tr>'."\r\n";
    $x .= '</table>';
}elseif($_GET['editPart']=='2'){
    if($plugin['attendancestaff'] == true && ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm'] == true || $sys_custom['eAppraisal']['generalTransferAttendnanceDataToForm'] == true)){
        $x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['SettingsAttendanceImport']);
        $x .= '<table class="form_table_v30">'."\r\n";
        $x .= '<tr>'."\r\n";
        $x .= '<td class="field_title">'.$Lang['Appraisal']['SettingsAttendanceImport'].'</td>'."\r\n";
        $x .= '<td>'."\r\n";
        $x .= $indexVar['libappraisal']->getAttendanceReasonMapping("edit");
        $x .= '</td>'."\r\n";
        $x .= '</tr>'."\r\n";
        $x .= '</table>';
    }
}
$x .= "<input type=\"hidden\" id=\"editPart\" name=\"editPart\" value=".$_GET['editPart'].">"."\r\n";
$x .= "<input type=\"hidden\" id=\"ExcludeAssessmentTeacherIDStr\" name=\"ExcludeAssessmentTeacherIDStr\" value=".$indexVar['libappraisal']->getExemptTeacherList(false).">"."\r\n";
$x .= "<input type=\"hidden\" id=\"ReportDisplayPersonIDStr\" name=\"ReportDisplayPersonIDStr\" value=".$indexVar['libappraisal']->getReportDisplayPersonList(false).">"."\r\n";
$x .= "<input type=\"hidden\" id=\"ReportDoubleCountPersonIDStr\" name=\"ReportDoubleCountPersonIDStr\" value=".$indexVar['libappraisal']->getReportDoubleCountPersonList(false).">"."\r\n";
$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================

// ============================== Custom Function ==============================

function cnvArrToSelect($arrDes,$arrVal){
    $oAry = array();
    if($arrVal == null){
        for($_i=0; $_i<sizeof($arrDes); $_i++){
            $oAry[$_i] = $arrDes[$_i];
        }
    }
    else{
        for($_i=0; $_i<sizeof($arrDes); $_i++){
            $oAry[$arrVal[$_i]] = $arrDes[$_i];
        }
    }
    return $oAry;
}
// ============================== Custom Function ==============================

?>