﻿<?php 
// ============================== Related Tables ==============================
// GENERAL_SETTING
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");

# Page Title
$connection = new libgeneralsettings();
$JSON_obj = new JSON_obj();

// ============================== Transactional data ==============================

$x = "";
$x .= '<table class="main">';
$x .= '	<tr class="box-upper">';
$x .= '		<td>';
$x .= '			<div class="org-name">博文教育學校團體</div>';
$x .= '			<div class="sch-name">博文亞洲中學</div>';
$x .= '		</td>';
$x .= '	</tr>';
$x .= '	<tr class="box-logo">';
$x .= '		<td>';
$x .= '			<div class="sch-logo"><img src="../../../..//file/appraisal/cover_image/56-512.png"></div>';
$x .= '		</td>';
$x .= '	</tr>';
$x .= '	<tr class="box-middle">';
$x .= '	<td>';
$x .= '	<div class="sch-year">2018 - 2019</div>';
$x .= '	<div class="report-name">教師考績報告</div>';
$x .= '	<div class="tea-name">陳大文<br>CHAN, Tai Man</div>';
$x .= '	</td>';
$x .= '	</tr>';
$x .= '	<tr class="box-lower">';
$x .= '	<td>';
$x .= '	<div class="sch-motto">「敬畏耶和華是智慧的開端，認識至聖者便是聰明。」<br>(聖經：箴言9章10節)</div>';
$x .= '	<table class="roles">';
$x .= '	<tr>';
$x .= '	<td>評估：黃志遠主任</td>';
$x .= '	<td>覆該：郭淑珍主任</td>';
$x .= '	</tr>';
$x .= '	<tr>';
$x .= '	<td>審閱：陳國強校長</td>';
$x .= '	<td>觀課：王宜君老師</td>';
$x .= '	</tr>';
$x .= '	<tr>';
$x .= '	<td>審閱：陳國強校長</td>';
$x .= '	<td>觀課：王宜君老師</td>';
$x .= '	</tr>';
$x .= '	<tr>';
$x .= '	<td>審閱：陳國強校長</td>';
$x .= '	<td>觀課：王宜君老師</td>';
$x .= '	</tr>';
$x .= '	</table>';
$x .= '	</div>';
$x .= '	</div>';
$x .= '	<div class="page-break"></div>';

$htmlAry['contentTbl'] = $x;

?>