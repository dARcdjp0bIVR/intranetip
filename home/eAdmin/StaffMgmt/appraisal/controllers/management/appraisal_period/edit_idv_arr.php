<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];
$idvID = $_GET["idvID"];
//$templateID = $_GET["templateID"];

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting'], 'javascript: goBackCycle('.$cycleID.');');
$navigationAry[] = array($Lang['Appraisal']['CycleTemplate']['StaffIdvAndArr']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps1'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps2'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps3'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps4'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT CycleID,AppDescr
	FROM(
		SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as AppDescr
		FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipsf";
$a=$connection->returnResultSet($sql);
$cycleDescr=$a[0]["AppDescr"];
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\"><b>".$Lang['Appraisal']['CycleTemplate']['CurrTemplate']."</b></td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackCycle(".$cycleID.")\">".$cycleDescr."</a></td></tr>";
$x .= "</table>";
//======================================================================== 適用於是次安排的評審表格 ========================================================================//
$sql="SELECT IdvID,CycleID,TemplateID,UserID,Remark
		FROM INTRANET_PA_C_IDVFRM WHERE IdvID like '%".$idvID."%';";
//echo $sql."<br/><br/>";
$z=$connection->returnResultSet($sql);
$templateID=$z[0]["TemplateID"];

$sql="SELECT ipsf.TemplateID,TemplateType,FrmTitleChi,FrmCodChi,ObjChi,FrmTitleEng,FrmCodEng,ObjEng,IsActive,OrderNum,DisplayOrder,DateTimeModified,
	FrmEngTitle,FrmChiTitle,".$indexVar['libappraisal']->getLangSQL("FrmChiTitle","FrmEngTitle")." as FrmTitle 
	FROM(
		SELECT TemplateID,TemplateType,FrmTitleChi,FrmCodChi,ObjChi,FrmTitleEng,FrmCodEng,ObjEng,IsActive,DisplayOrder as OrderNum,DisplayOrder,DateTimeModified,
		CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmEngTitle,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmChiTitle
		FROM INTRANET_PA_S_FRMTPL
	) as ipsf
	INNER JOIN(
		SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipcf ON ipsf.TemplateID=ipcf.TemplateID
		";
//echo $sql."<br/><br/>";
$templateList=$connection->returnResultSet($sql);
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['PossibleTemplateForm'])."\r\n";
$x .= "<div>";
$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
$x .= "<tr>";
$x .= "<td style=\"width:50%;\">";
$x .= $Lang['Appraisal']['CycleTemplate']['ApprsialForm'];
$x .= "</td>";
$x .= "<td>";
if($idvID!=null){
	for($i=0;$i<sizeof($templateList);$i++){
		if($templateList[$i]["TemplateID"]==$templateID){
			$idx = $i;
			break;			
		}
	}
	$x .= "<span>".$templateList[$idx]["FrmTitle"]."</span>";
}
else{	
	$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($templateList, "TemplateID","FrmChiTitle","FrmEngTitle"), $selectionTags='id="DDLTemplateID" name="DDLTemplateID"', $SelectedType=0, $all=0, $noFirst=0);
}
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "</div><br/>";
//======================================================================== 編配被評者 ========================================================================//
if($idvID!="" && $idvID != "undefined"){
	$sql="SELECT IdvID,CycleID,TemplateID,UserID,Remark
			FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".IntegerSafe($cycleID)." AND IdvID=".$idvID.";";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AssignedAppraisee'])."\r\n";
	$x .= "<div>";
	$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<tr>";
	$x .= "<td style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['SelectedAppraiseeRmk']."<br/>";
	//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"AppraiseeID\" name=\"AppraiseeID\" value=\"\">";
	$x .= $indexVar['libappraisal']->Get_Staff_Selection("AppraiseeSel", "AppraiseeSel", $ParIndividual=true,$ParIsMultiple=false,$ParSize=20,$ParSelectedValue="",$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true,$LoginIDAsValue=false,$Currentonly=true,$ParEmptyFirst=true);	
	$x .= "<span id=\"LoadingPlace\"></span>";
	$x .= "</td>";
	$x .= "<td style=\"width:50%;\">";
	$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['SelectedAppraisee']."<br/></span>";
	$x .= "<span id=\"AppraiseeUserName\" name=\"AppraiseeUserName\">";
	$x .=$indexVar['libappraisal']->getUserNameID($a[0]["UserID"],"name")."</span>";
	$x .= "<input type=\"hidden\" id=\"AppraiseeIDStr\" name=\"AppraiseeIDStr\" value=".$a[0]["UserID"].">"."\r\n";
	$divDisplay=($indexVar['libappraisal']->getUserNameID($a[0]["UserID"],"login")!="")?"block":"none";
	$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteAppraisee' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteAppraisee() title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
	$x .= "</td>";
	$x .= "</tr>";
	$x .= "</table>";
	$x .= "</div><br/>";
	//======================================================================== 編配評核者 ========================================================================//
	$sql="SELECT ipci.IdvID,AprID,UserID,EnglishName,ChineseName,UserLogin
		FROM(
			SELECT CycleID,IdvID FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".IntegerSafe($cycleID)." AND IdvID=".$idvID."
		) as ipci 
		INNER JOIN(
			SELECT IdvID,AprID FROM INTRANET_PA_C_IDVAPR 
			WHERE IdvID=".$idvID."
		) as ipcidvapr ON ipci.IdvID=ipcidvapr.IdvID 
		INNER JOIN(
			SELECT UserID,EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']."
		) as iu ON ipcidvapr.AprID=iu.UserID";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AssignedAppraiser'])."\r\n";
	$x .= "<div>";
	$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<tr>";
	$x .= "<td style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['SelectedAppraiserRmk']."<br/>";
	//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"AppraiserID\" name=\"AppraiserID\" value=\"\">";
	$x .= $Lang['Appraisal']['SearchByLoginID']." ".$indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelect()", 'selectBtn');;
	$x .= "</td>";
	$x .= "<td style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['SelectedAppraiser']."<br/>";
	$x .= "<select name=\"appraiser\" id=\"appraiser\" class=\"select_studentlist\" size=\"15\" multiple=\"multiple\">";
	for($i=0;$i<sizeof($a);$i++){
		if($i==sizeof($a)-1){
			$appraiserIDStr.=$a[$i]["UserID"];
		}
		else{
			$appraiserIDStr.=$a[$i]["UserID"].",";
		}
		$x .= "<option value=\"".$a[$i]["UserID"]."\">".$indexVar['libappraisal']->getUserNameID($a[$i]["UserID"], 'name')."</option>";
	}
	$x .= "</select>";
	$x .= "&nbsp;";
	$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['RemoveSelected'], "button", "goDeleteAppraiser()", 'submitBtn');
	$x .= "</td>";
	$x .= "</tr>";
	$x .= "</table>";
	$x .= "</div><br/>";
	//======================================================================== 個別編配備註========================================================================//
	$sql="SELECT IdvID,CycleID,TemplateID,UserID,Remark
			FROM INTRANET_PA_C_IDVFRM WHERE CycleID=".IntegerSafe($cycleID)." AND IdvID =".$idvID.";";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['IdvAssRmk'])."\r\n";
	$x .= "<table class=\"form_table_v30\">";
	$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['Rmk']."</td>";
	$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTAREA("Rmk", $taContents=$a[0]["Remark"], $taCols=70, $taRows=5, $OnFocus="", $readonly="", $other='', $class='', $taID='', $CommentMaxLength='')."</td></tr>";
	$x .= "</table><br/>";
}

$x .= "<input type=\"hidden\" id=\"AppraiserIDStr\" name=\"AppraiserIDStr\" value=".$appraiserIDStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"IdvID\" name=\"IdvID\" value=".$idvID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"TemplateID\" name=\"TemplateID\" value=".$templateID.">"."\r\n";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackCycle(".$cycleID.")", 'backBtn');
// ============================== Define Button ==============================






?>