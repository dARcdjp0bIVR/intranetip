<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting'], 'javascript: goBackCycle('.$cycleID.');');
$navigationAry[] = array($Lang['Appraisal']['BtnImportAbsLateSub']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);
//======================================================================== Content ========================================================================//
 ### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['ImportAbsLateSub']['name'];
$tmpName = $_FILES['ImportAbsLateSub']['tmp_name'];
$ext = strtoupper($lo->file_ext($name));

//echo $name." ".$ext." ".$tmpName." ".$cycleID."<br/><br/>";

if(!($ext == ".CSV" || $ext == ".TXT"))
{	
	/*$returnPath = 'Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'edit_import_leave_1&returnMsgKey=WrongFileFormat';
	echo $returnPath;
	header($returnPath);
	exit();*/
}
### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/appraisal/absence";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);

$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($ImportAbsLateSub, $TargetFilePath);
//$SuccessArr['MoveCsvFileToTempFolder'] = "1";
/*
 * error: 20161211_151316_6266.CSV
 * correct: 20161211_151655_6266.CSV
 */ 

//$TargetFilePath = "/home/eclass/intranetIP/file/import_temp/appraisal/absence/20161211_151655_6266.CSV";

### Get Data from the csv file
//$ObjTimetable = new Timetable("8");

//$ColumnTitleArr = $ObjTimetable->Get_Import_Lesson_Column_Title($ParLang='En', $TargetPropertyArr=array(1));	// Required Column Only
//$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();

$ColumnTitleArr = array(4);
$ColumnTitleArr[0]=$Lang['Appraisal']['CycleExportAbsLateSub']['TeacherID'];
$ColumnTitleArr[1]=$Lang['Appraisal']['CycleExportAbsLateSub']['NumAbsence'];
$ColumnTitleArr[2]=$Lang['Appraisal']['CycleExportAbsLateSub']['NumSubstitution'];
$ColumnTitleArr[3]=$Lang['Appraisal']['CycleExportAbsLateSub']['NumLateness'];

$ColumnPropertyArr = array(4);
$ColumnPropertyArr[0]="1";
$ColumnPropertyArr[1]="1";
$ColumnPropertyArr[2]="1";
$ColumnPropertyArr[3]="1";

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
$col_name = array_shift($data);
$numRcd = count($data);


$x .= "<table class=\"form_table_v30\">";
$x .= "<tr>";
$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleExportAbsLateSub']['SuccessfulRecord']."</td>";
$x .= "<td><div><span id=\"SuccessfulRecord\"></span></div></td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleExportAbsLateSub']['FailureRecord']."</td>";
$x .= "<td><div><span id=\"FailureRecord\"></span></div></td>";
$x .= "</tr>";
$x .= "</table>";

$x .= "<div id=\"ErrorTableDiv\">";
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tbody>";
$showHeader = true;
$count = 1;
$trueRecord=0;
$falseRecord=0;
for($i=0;$i<$numRcd;$i++){
	$err = false;
	$errMsg = "";
	$errLogin = false;
	$errAbsenceLessons = false;
	$errSubstitutedLessons = false;
	$errLateness = false;
	// check login name
	$sql="SELECT EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$data[$i][0]."';";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)==0){
		$errMsg .= $Lang['Appraisal']['CycleExportAbsLateSubError']['NoLoginUser']."<br/><br/>";
		$errLogin = true;
		//echo "ErrLogin: ".$errLogin." ".$data[$i][0]."<br/><br/>";
	}
	// check Absence Lessons
	if(is_numeric($data[$i][1])==false){
		$errMsg .= $Lang['Appraisal']['CycleExportAbsLateSubError']['NeedNum']."<br/><br/>";
		$errAbsenceLessons = true;
		//echo "ErrDaysWithPay: ".$errDaysWithPay."<br/><br/>";
	}
	// check Substituted Lessons
	if(is_numeric($data[$i][2])==false){
		$errMsg .= $Lang['Appraisal']['CycleExportAbsLateSubError']['NeedNum']."<br/><br/>";
		$errSubstitutedLessons = true;
		//echo "ErrDaysWithPay: ".$errDaysWithPay."<br/><br/>";
	}
	// check Lateness
	if(is_numeric($data[$i][3])==false){
		$errMsg .= $Lang['Appraisal']['CycleExportAbsLateSubError']['NeedNum']."<br/><br/>";
		$errLateness = true;
		//echo "ErrDaysWithPay: ".$errDaysWithPay."<br/><br/>";
	}	
	$err=$errLogin|$errAbsenceLessons|$errSubstitutedLessons|$errLateness;
		
	if($err==false){
		$trueRecord++;
	}
	else{
		$falseRecord++;
	}
	
	if($showHeader==true && $err==true){
		$x .= "<tr>";
		$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportAbsLateSub']['Row']."</td>";
		$x .= "<td width=\"15%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportAbsLateSub']['TeacherID']."</td>";
		$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportAbsLateSub']['NumAbsence']."</td>";
		$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportAbsLateSub']['NumSubstitution']."</td>";
		$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportAbsLateSub']['NumLateness']."</td>";
		$x .= "<td width=\"25%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportAbsLateSub']['SysRmk']."</td>";
		$x .= "</tr>";
		$showHeader = false;
	}
	
	if($err==true && $count%2==1){
		$x .= "<tr style=\"vertical-align:top\">";
		$x .= "<td class=\"tablebluerow\">".($i+1)."</td>";
		$x .= "<td class=\"tablebluerow\">".(($errLogin==true)?"<font color=\"red\">".$data[$i][0]."</font>":$data[$i][0])."</td>";
		$x .= "<td class=\"tablebluerow\">".(($errAbsenceLessons==true)?"<font color=\"red\">".$data[$i][1]."</font>":$data[$i][1])."</td>";
		$x .= "<td class=\"tablebluerow\">".(($errSubstitutedLessons==true)?"<font color=\"red\">".$data[$i][2]."</font>":$data[$i][2])."</td>";
		$x .= "<td class=\"tablebluerow\">".(($errLateness==true)?"<font color=\"red\">".$data[$i][3]."</font>":$data[$i][3])."</td>";
		$x .= "<td class=\"tablebluerow\">".$errMsg."</td>";
		$x .= "</tr>";
		$count ++;
	}
	else if($err==true && $count%2==0){
		$x .= "<tr style=\"vertical-align:top\">";
		$x .= "<td class=\"tablebluerow2\">".($i+1)."</td>";
		$x .= "<td class=\"tablebluerow2\">".(($errLogin==true)?"<font color=\"red\">".$data[$i][0]."</font>":$data[$i][0])."</td>";
		$x .= "<td class=\"tablebluerow2\">".(($errAbsenceLessons==true)?"<font color=\"red\">".$data[$i][1]."</font>":$data[$i][1])."</td>";
		$x .= "<td class=\"tablebluerow2\">".(($errSubstitutedLessons==true)?"<font color=\"red\">".$data[$i][2]."</font>":$data[$i][2])."</td>";
		$x .= "<td class=\"tablebluerow2\">".((errLateness==true)?"<font color=\"red\">".$data[$i][3]."</font>":$data[$i][3])."</td>";
		$x .= "<td class=\"tablebluerow2\">".$errMsg."</td>";
		$x .= "</tr>";
		$count ++;
	}	
}
$x .= "</tbody>";
$x .= "</table>";
$x .= "</div>";

$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value='".$cycleID."'>\r\n";
$x .= "<input type=\"hidden\" id=\"FilePath\" name=\"FilePath\" value='".$TargetFilePath."'>\r\n";
$x .= "<input type=\"hidden\" id=\"HiddenSuccessfulRecord\" name=\"HiddenSuccessfulRecord\" value=".$trueRecord.">\r\n";
$x .= "<input type=\"hidden\" id=\"HiddenFailureRecord\" name=\"HiddenFailureRecord\" value=".$falseRecord.">\r\n";
$htmlAry['contentTbl'] = $x;

// ============================== Define Button ==============================
$isDisabled=($err==false)?0:1;
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnImport'], "button", "goImport()", 'submitBtn', '', $Disabled=$isDisabled);
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
$htmlAry['cancelBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel('".$cycleID."')", 'cancelBtn');
// ============================== Define Button ==============================

?>