<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];
$grpID = $_GET["grpID"];

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting'], 'javascript: goBackCycle('.$cycleID.');');
$navigationAry[] = array($Lang['Appraisal']['CycleTemplate']['StfGrpAndArr']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
/*$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps1'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps2'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps3'];
$stepAry[] = $Lang['Appraisal']['CycleHeader']['Steps4'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);*/
//======================================================================== Content ========================================================================//
$sql="SELECT CycleID,AppDescr
	FROM(
		SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as AppDescr
		FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipsf";
$a=$connection->returnResultSet($sql);
$cycleDescr=$a[0]["AppDescr"];
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\"><b>".$Lang['Appraisal']['CycleTemplate']['CurrTemplate']."</b></td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackCycle(".$cycleID.")\">".$cycleDescr."</a></td></tr>";
$x .= "</table>";
//======================================================================== 職員分組內容 ========================================================================//
$sql="SELECT GrpID,CycleID,DescrChi,DescrEng
	FROM(
		SELECT GrpID,CycleID,DescrChi,DescrEng
		FROM INTRANET_PA_C_GRPSUM WHERE CycleID=".IntegerSafe($cycleID)." AND GrpID=".IntegerSafe($grpID)."
	) as ipcg";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['StfGrpContent'])."\r\n";
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['StfGrpDescrEng']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('DescrEng', 'DescrEng', $a[0]["DescrEng"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['StfGrpDescrChi']."</td>";
$x .= "<td>".$indexVar['libappraisal_ui']->GET_TEXTBOX('DescrChi', 'DescrChi', $a[0]["DescrChi"], $OtherClass='', $OtherPar=array('maxlength'=>255))."</td></tr>";
$x .= "</table><br/>";
//======================================================================== 編配表格及評核模式 ========================================================================//
$sql="SELECT ipcf.CycleID,ipcf.TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmChiTitle","FrmEngTitle")." as FrmTitle,GrpID,AppMode,
	 Incl as IncludeTemplate
	FROM(
		SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipcf
	INNER JOIN(
		SELECT TemplateID,TemplateType,FrmTitleChi,FrmCodChi,ObjChi,FrmTitleEng,FrmCodEng,ObjEng,IsActive,DisplayOrder as OrderNum,DisplayOrder,DateTimeModified,
		CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmEngTitle,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmChiTitle
		FROM INTRANET_PA_S_FRMTPL 
	) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
	LEFT JOIN(
		SELECT GrpID,TemplateID,AppMode,Incl FROM INTRANET_PA_C_GRPFRM WHERE GrpID=".IntegerSafe($grpID)."
	) as ipcg ON ipcf.TemplateID=ipcg.TemplateID";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
if($grpID != "" && $grpID != "undefined"){
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['StfGrpEditFormMode'])."\r\n";
	$x .= "<div>";
	$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<thead>"."\r\n";
	$x .= "<tr>";
	$x .= "<th class=\"sub_row_top\" style=\"width:2%;\">#</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:33%;\">".$Lang['Appraisal']['CycleTemplate']['PossibleTemplate']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:15%;\">".$Lang['Appraisal']['CycleTemplate']['IncludeTemplate']."</th>";
	$x .= "<th class=\"sub_row_top\" style=\"width:50%;\">".$Lang['Appraisal']['CycleTemplate']['ApprsialMode']."</th>";
	$x .= "</tr>";
	$x .= "</thead>";
	$x .= "<tbody>"."\r\n";
	for($i=0;$i<sizeof($a);$i++){
		$j=$i+1;
		if($i==sizeof($a)-1){
			$templateIDStr.=$a[$i]["TemplateID"];
		}
		else{
			$templateIDStr.=$a[$i]["TemplateID"].",";
		}
		$isDisabled = ($a[$i]["IncludeTemplate"]=="1")?0:1;
		$x .= "<tr id=\"".$a[$i]['TemplateID']."\">"."\r\n";
		$x .= "<td>".$j."</td>";
		$x .= "<td>".$a[$i]["FrmTitle"]."</td>";
		$x .= "<td>";
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("IncludeTemplate", "IncludeTemplate", $Value=$i, $isChecked=($a[$i]["IncludeTemplate"]=="1")?1:0, $Class='', $Display='', $Onclick='resetMode('.$i.')', $Disabled='');
		$x .= "</td>";
		$x .= "<td>";
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('SelfAppr_'.$i, 'AppMode_'.$i, $Value="1", $isChecked=($a[$i]["AppMode"]=="1")?1:0, $Class="requiredField appraisalMode", $Display=$Lang['Appraisal']['CycleTemplate']['SelfApprsialMode'], $Onclick="enableRoleSelection(0);enableLeaderFillSelection(1)",$isDisabled);
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('UTDAppr_'.$i, 'AppMode_'.$i, $Value="2", $isChecked=($a[$i]["AppMode"]=="2")?1:0, $Class="requiredField appraisalMode", $Display=$Lang['Appraisal']['CycleTemplate']['UpToDownApprsialMode'], $Onclick="enableRoleSelection(1);enableLeaderFillSelection(0)",$isDisabled);
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('DTUAppr_'.$i, 'AppMode_'.$i, $Value="3", $isChecked=($a[$i]["AppMode"]=="3")?1:0, $Class="requiredField appraisalMode", $Display=$Lang['Appraisal']['CycleTemplate']['DownToUpApprsialMode'], $Onclick="enableRoleSelection(0);enableLeaderFillSelection(0)",$isDisabled);
		$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('GrpAppr_'.$i, 'AppMode_'.$i, $Value="4", $isChecked=($a[$i]["AppMode"]=="4")?1:0, $Class="requiredField appraisalMode", $Display=$Lang['Appraisal']['CycleTemplate']['GroupApprsialMode'], $Onclick="enableRoleSelection(0);enableLeaderFillSelection(0)",$isDisabled);
		$x .= "</td>";
		$x .= "</tr>";
	}
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= "</div><br/>";
	//======================================================================== 編配組長 ========================================================================//
	$sql="SELECT GrpID,UserID,IsLeader,LeaderRole,CanCallModify,IsLeaderFill  FROM INTRANET_PA_C_GRPMEM WHERE GrpID=".IntegerSafe($grpID)." AND IsLeader=1;";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$sql ="SELECT AppMode FROM INTRANET_PA_C_GRPFRM WHERE GrpID=".IntegerSafe($grpID)." AND Incl=1";
	$appMode = current($connection->returnResultSet($sql));
	if($appMode['AppMode']!=2){
		$hideLeaderUserRole = "style=\"display:none;\"";
	}
	if($appMode['AppMode']!=1){
		$hideLeaderFillForm = "style=\"display:none;\"";
	}
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AssignLeader'])."\r\n";
	$x .= "<div>";
	$x .= "<table id=\"tbLeader\" class=\"common_table_list ClassDragAndDrop\">";
	for($i=0; $i<sizeof($a);$i++){
		$x .= "<tr id=\"row_".$i."\">";
			$x .= "<td style=\"width:40%;\">";
			//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"LeaderUserID_".$i."\" name=\"LeaderUserID_".$i."\" value=\"\">";
			$x .= $indexVar['libappraisal']->Get_Staff_Selection("LeaderUserNameSel_".$i, "LeaderUserNameSel_".$i, $ParIndividual=true,$ParIsMultiple=false,$ParSize=20,$ParSelectedValue="",$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true,$LoginIDAsValue=false,$Currentonly=true,$ParEmptyFirst=true);	
			$x .= "<span id=\"LoadingPlace_".$i."\"></span>";
			$x .= "</td>";
			$x .= "<td style=\"width:15%;\">";
			$x .= "<span id=\"LeaderUserName_".$i."\" name=\"LeaderUserName_".$i."\">".$indexVar['libappraisal']->getUserNameID($a[$i]["UserID"],"name")."</span>";			
			$x .= "<input type=\"hidden\" id=\"LeaderIDStr_".$i."\" name=\"LeaderIDStr_".$i."\" value=".$a[$i]["UserID"].">"."\r\n";
			//$divDisplay=($indexVar['libappraisal']->getUserNameID($a[$i]["UserID"],"login")!="")?"block":"none";
			//$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteLeader_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLeader(".$i.") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
			$x .= "</td>";
			$x .= "<td style=\"width:20%;\">";
			$x .= "<span id=\"LeaderUserRole_".$i."\" name=\"LeaderUserRole_".$i."\" class=\"LeaderRole\" $hideLeaderUserRole>";
			$x .= "<input type=\"radio\" name=\"LeaderRole_".$i."\" value=\"0\" ".(($a[$i]["LeaderRole"]==0)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsView']."\t";
			$x .= "<input type=\"radio\" name=\"LeaderRole_".$i."\" value=\"1\" ".(($a[$i]["LeaderRole"]==1)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsEdit']."";
			$x .= "</span>";			
			$x .= "<span id=\"LeaderFillForm_".$i."\" name=\"LeaderFillForm_".$i."\" class=\"LeaderFillForm\" $hideLeaderFillForm>";
			$x .= "<input type=\"radio\" name=\"LeaderFill_".$i."\" value=\"0\" ".(($a[$i]["IsLeaderFill"]==0)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsNeedNotFill']."\t";
			$x .= "<input type=\"radio\" name=\"LeaderFill_".$i."\" value=\"1\" ".(($a[$i]["IsLeaderFill"]==1)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsNeedFill'] ."";
			$x .= "</span>";
			//$x .= "<input type=\"hidden\" id=\"LeaderRole_".$i."\" name=\"LeaderRole_".$i."\" value=".$a[$i]["LeaderRole"].">"."\r\n";
			$divDisplay=($indexVar['libappraisal']->getUserNameID($a[$i]["UserID"],"login")!="")?"block":"none";
			//$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteLeader_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLeader(".$i.") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
			$x .= "</td>";
			$x .= "<td style=\"width:25%;\">";
			$x .= "<span id=\"LeaderCanEdit_".$i."\" name=\"LeaderCanEdit_".$i."\" class=\"LeaderEdit\">";
			$x .= "<input type=\"checkbox\" name=\"LeaderEdit_".$i."\" value=\"1\" ".(($a[$i]["CanCallModify"]==1)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsModify'];
			$x .= "</span>";
			$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteLeader_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLeader(".$i.") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
			$x .= "</td>";
		$x .= "</tr>";
		if($i==sizeof($a)-1){
			$leaderIDStr.=$a[$i]["UserID"];
			$LeaderIDRoleStr .= $a[$i]["LeaderRole"];
			$LeaderModifyIDStr .= $a[$i]["CanCallModify"];
			$LeaderFillIDStr .= $a[$i]["IsLeaderFill"];
		}
		else{
			$leaderIDStr.=$a[$i]["UserID"].",";
			$LeaderIDRoleStr .= $a[$i]["LeaderRole"].",";
			$LeaderModifyIDStr .= $a[$i]["CanCallModify"].",";
			$LeaderFillIDStr .= $a[$i]["IsLeaderFill"].",";
		}
	}
	
	$strIdx = sizeof($a);
	for($i=$strIdx; $i<($strIdx+4);$i++){
		if($i==$strIdx){
			$x .= "<tr id=\"row_".$i."\">";
		}
		else{
			$x .= "<tr id=\"row_".$i."\" style=\"display:none\">";
		}	
		$x .= "<td style=\"width:40%;\">";
		//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"LeaderUserID_".$i."\" name=\"LeaderUserID_".$i."\" value=\"\">";
		$x .= $indexVar['libappraisal']->Get_Staff_Selection("LeaderUserNameSel_".$i, "LeaderUserNameSel_".$i, $ParIndividual=true,$ParIsMultiple=false,$ParSize=20,$ParSelectedValue="",$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true,$LoginIDAsValue=false,$Currentonly=true,$ParEmptyFirst=true);	
		$x .= "<span id=\"LoadingPlace_".$i."\"></span>";
		$x .= "</td>";
		$x .= "<td style=\"width:15%;\">";
		$x .= "<span id=\"LeaderUserName_".$i."\" name=\"LeaderUserName_".$i."\">"."</span>";
		$x .= "<input type=\"hidden\" id=\"LeaderIDStr_".$i."\" name=\"LeaderIDStr_".$i."\" value=>"."\r\n";
		//$divDisplay="none";
		//$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteLeader_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLeader(".$i.") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
		$x .= "</td>";
		$x .= "<td style=\"width:20%;\">";
		$x .= "<span id=\"LeaderUserRole_".$i."\" name=\"LeaderUserRole_".$i."\" class=\"LeaderRole\" style=\"display:none;\">";
		$x .= "<input type=\"radio\" name=\"LeaderRole_".$i."\" value=\"0\" >".$Lang['Appraisal']['AppraisalRecordsView']."\t";
		$x .= "<input type=\"radio\" name=\"LeaderRole_".$i."\" value=\"1\" >".$Lang['Appraisal']['AppraisalRecordsEdit']."";
		$x .= "</span>";					
		$x .= "<span id=\"LeaderFillForm_".$i."\" name=\"LeaderFillForm_".$i."\" class=\"LeaderFillForm\" style=\"display:none;\">";
		$x .= "<input type=\"radio\" name=\"LeaderFill_".$i."\" value=\"0\" ".(($a[$i]["IsLeaderFill"]==0)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsNeedNotFill']."\t";
		$x .= "<input type=\"radio\" name=\"LeaderFill_".$i."\" value=\"1\" ".(($a[$i]["IsLeaderFill"]==1)?"checked":"").">".$Lang['Appraisal']['AppraisalRecordsNeedFill'] ."";
		$x .= "</span>";
		$divDisplay="none";
		//$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteLeader_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLeader(".$i.") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
		$x .= "</td>";
		$x .= "<td style=\"width:25%;\">";
		$x .= "<span id=\"LeaderCanEdit_".$i."\" name=\"LeaderCanEdit_".$i."\" class=\"LeaderEdit\" style=\"display:none;\">";
		$x .= "<input type=\"checkbox\" name=\"LeaderEdit_".$i."\" value=\"1\" >".$Lang['Appraisal']['AppraisalRecordsModify'];
		$x .= "</span>";
		$divDisplay="none";
		$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteLeader_".$i."' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteLeader(".$i.") title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
		$x .= "</td>";
		$x .= "</tr>";
	}
	$x .= "</table>";
	$x .= "</div><br/>";
	$x .= "<input type=\"hidden\" id=\"NumLeader\" name=\"NumLeader\" value=".sizeof($a).">"."\r\n";
	//======================================================================== 編配組員 ========================================================================//
	$sql="SELECT GrpID,ipcg.UserID,IsLeader,UserLogin,Name
		FROM(
			SELECT GrpID,UserID,IsLeader FROM INTRANET_PA_C_GRPMEM 
			WHERE GrpID=".IntegerSafe($grpID)." AND IsLeader=0
		) as ipcg
		INNER JOIN(
			SELECT UserID,EnglishName,ChineseName,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as Name,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." 		
		) as iu ON ipcg.UserID=iu.UserID";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AssignMembers'])."\r\n";
	$x .= "<div>";
	$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
	$x .= "<tr>";
	$x .= "<td style=\"width:50%;\">";
	//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"MemberUserID\" name=\"MemberUserID\" value=\"\">";
	$x .= $Lang['Appraisal']['SearchByLoginID']." ".$indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelect()", 'selectBtn');;
	$x .= "</td>";
	$x .= "<td style=\"width:50%;\">";
	$x .= "<select name=\"member\" id=\"member\" class=\"select_studentlist\" size=\"15\" multiple=\"multiple\">";
	for($i=0;$i<sizeof($a);$i++){
		if($i==sizeof($a)-1){
			$memberIDStr.=$a[$i]["UserID"];
		}
		else{
			$memberIDStr.=$a[$i]["UserID"].",";
		}
		$x .= "<option value=\"".$a[$i]["UserID"]."\">".$indexVar['libappraisal']->getUserNameID($a[$i]["UserID"], 'name')."</option>";  
	}
	$x .= "</select>";
	$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['RemoveSelected'], "button", "goDeleteMember()", 'submitBtn');
	$x .= "</td>";
	$x .= "</tr>";
	$x .= "</table>";
	$x .= "</div><br/>";
}
$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='ExcludeAssessmentTeacher'";
$excludeTeacherID = current($connection->returnVector($sql));
$x .= "<input type=\"hidden\" id=\"ExcludeTeacherID\" name=\"ExcludeTeacherID\" value=".$excludeTeacherID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"LeaderIDStr\" name=\"LeaderIDStr\" value=".$leaderIDStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"LeaderIDRoleStr\" name=\"LeaderIDRoleStr\" value=".$LeaderIDRoleStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"LeaderModifyIDStr\" name=\"LeaderModifyIDStr\" value=".$LeaderModifyIDStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"LeaderFillIDStr\" name=\"LeaderFillIDStr\" value=".$LeaderFillIDStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"MemberIDStr\" name=\"MemberIDStr\" value=".$memberIDStr.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"GrpID\" name=\"GrpID\" value=".$grpID.">"."\r\n";
$x .= "<input type=\"hidden\" id=\"TemplateIDStr\" name=\"TemplateIDStr\" value=".$templateIDStr.">"."\r\n";


$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
if($grpID!=""){
	$htmlAry['saveAsBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtbSaveAsGrp'], "button", "goSaveAs()", 'saveAsBtn');
}
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackCycle(".$cycleID.")", 'backBtn');
// ============================== Define Button ==============================




?>