<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalPeriod']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];


$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriod'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalPeriodSetting'], 'javascript: goBackCycle('.$cycleID.');');
$navigationAry[] = array($Lang['Appraisal']['BtnImportLeave']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=1, $stepAry);
//======================================================================== Content ========================================================================//
$x = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">";
$x .= "<tr>";
$x .= "<td align=\"right\" colspan=\"2\">";
$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class=\"systemmsg\">";
$x .= "<tbody>";
$x .= "<tr>";
$x .= "<td nowrap=\"nowrap\">";
$x .= "<span id=\"errMsg\" style=\"display:none\"><font color=\"red\">".$Lang['Appraisal']['Report']['InvalidFormat']."</font></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</tbody>";
$x .= "</table>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$htmlAry['msg'] = $x;

$sql="SELECT CycleID,AppDescr
	FROM(
		SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as AppDescr
		FROM INTRANET_PA_C_CYCLE WHERE CycleID=".IntegerSafe($cycleID)."
	) as ipsf";
$a=$connection->returnResultSet($sql);
$cycleDescr=$a[0]["AppDescr"];
$x = "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\"><b>".$Lang['Appraisal']['CycleTemplate']['CurrTemplate']."</b></td>";
$x .= "<td><a id='templateTitleID' class=\"tablelink\" href=\"javascript:goBackCycle(".$cycleID.")\">".$cycleDescr."</a></td></tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\">"."<span>".$Lang['Appraisal']['CycleTemplate']['InfoFile']."</span>"."<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['CSVFile']."</span>";
$x .= "<span style=\"color:#FF0000\">*</span></td>";
$x .= "<td><input type=\"file\" name=\"ImportLeave\" id=\"ImportLeave\" class=\"file\"></td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\"><span>".$Lang['Appraisal']['CycleTemplate']['TemplateFile']."</span></td>";
$x .= "<td><a id='exportTemplate' class=\"tablelink\" href=\"javascript:goExportLeave()\">".$Lang['Appraisal']['CycleExportTemplate']."</a></td>";
$x .= "</tr>";
$x .= "<tr>";
$x .= "<td class=\"field_title\"><span>".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow']."</span></td>";
$x .= "<td>";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow1'],"<span style=\"color:#FF0000\">*</span>")."<br/></span>";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow2'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk1']."</span>")."<br/></span>";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow3'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']."</span>")."<br/></span>";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow4'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']."</span>")."<br/></span>";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleTemplate']['LeaveInfoRow5'],"<span style=\"color:#999999\">".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRmk2']."</span>")."<br/></span>";
$x .= "<span>".$Lang['Appraisal']['CycleTemplate']['LeaveInfoRow6']."<br/></span>";		
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .="<span style=\"color:#999999\">".sprintf($Lang['Appraisal']['CycleLeave']['Remark'],"<span style=\"color:#FF0000\">*</span>")."</span><br/><br/>";
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnImportLeave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
// ============================== Define Button ==============================










?>