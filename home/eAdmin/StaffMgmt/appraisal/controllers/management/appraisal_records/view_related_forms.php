<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
include_once($PATH_WRT_ROOT."includes/appraisal/gen_form_class.php");
$genform = new genform();
$ColForPreSetField = 10;
$navigationAry[] = array($Lang['Appraisal']['AppraisalRecords'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['AppraisalRecordsView']);
//$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);


# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalRecords']);
//$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$rlsNo = $_GET["rlsNo"];
$recordID = $_GET["recordID"];
$batchID = $_GET["batchID"];
$viewTemplateID = $_GET["viewTemplateID"];

$currentDate = date("Y-m-d");

$connection = new libgeneralsettings();
$sql = "SELECT CycleID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$recordID."'";
$formData = current($connection->returnArray($sql));
$appraiseeID = $formData['UserID'];
$sql = "SELECT MAX(RlsNo) as RlsNo, bb.RecordID, MAX(BatchID) as BatchID,CycleID,TemplateID,UserID,bb.ObsID FROM INTRANET_PA_T_FRMSUB bb 
		INNER JOIN INTRANET_PA_T_FRMSUM mm ON bb.RecordID= mm.RecordID 
		WHERE mm.UserID='".IntegerSafe($appraiseeID)."' AND mm.TemplateID='".IntegerSafe($viewTemplateID)."' AND mm.CycleID='".IntegerSafe($formData['CycleID'])."' GROUP BY RecordID ORDER BY BatchID DESC";
$a = $connection->returnArray($sql);
for($i=0; $i < count($a); $i++){
	$nameField = $indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as UserName";
	$dateField = "sub.SubDate";
	$fieldArr = array_filter(array($nameField,$dateField));
	if($a[$i]['ObsID']==""){
		$aprInfoSql = "SELECT ".implode(",",$fieldArr)." FROM INTRANET_PA_T_FRMSUB sub 
						INNER JOIN INTRANET_PA_T_FRMSUM sum ON sub.RecordID=sum.RecordID 
						INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON sum.AprID=u.UserID
						WHERE sub.RecordID=".$a[$i]["RecordID"]." ORDER BY SubDate DESC LIMIT 1";
	}else{
		$aprInfoSql = "SELECT ".implode(",",$fieldArr)." FROM INTRANET_PA_T_FRMSUB sub 
						INNER JOIN INTRANET_PA_T_FRMSUM sum ON sub.RecordID=sum.RecordID 
						INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON u.UserID=".$a[$i]['ObsID']."
						WHERE sub.RecordID=".$a[$i]["RecordID"]." ORDER BY SubDate DESC LIMIT 1";
	}				
	$aprInfo = current($connection->returnArray($aprInfoSql));
	if($aprInfo['SubDate']==""){
		continue;
	}
	$x .= "<div>";
	$x .= "<span style=\"float:left;\"><b>".$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser']."</b>: ".$aprInfo['UserName']."</span>  ";
	$x .= "<span style=\"float:right;\"><b>".$Lang['Appraisal']['ARFormDate']."</b>: ".$aprInfo['SubDate']."</span>";
	$x .= "</div><br/><br/>";		
	$x .= $genform->exeForm($connection,$a[$i]["TemplateID"],$a[$i]["UserID"],$a[$i]["RecordID"],$a[$i]["CycleID"],$a[$i]["RlsNo"],$a[$i]["BatchID"], $isPrint=true);
	$x .= "<p class=\"spacer\"></p>";
}
if(count($a)==0){
	$x = "<div style='text-align: center;'>".$Lang['Appraisal']['Message']['NoRecord']."</div>";
}
$htmlAry['contentTbl'] = $x;

?>