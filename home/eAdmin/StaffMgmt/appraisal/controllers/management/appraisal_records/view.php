<?php
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
$ColForPreSetField = 10;
$navigationAry[] = array(
    $Lang['Appraisal']['AppraisalRecords'],
    'javascript: goBack();'
);
$navigationAry[] = array(
    $Lang['Appraisal']['AppraisalRecordsView']
);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);

// Page Title
$TAGS_OBJ[] = array(
    $Lang['Appraisal']['AppraisalRecords']
);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
// $btnAry[] = array('print', 'javascript:goPrint()');
// $htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$rlsNo = $_GET["rlsNo"];
$recordID = $_GET["recordID"];
$batchID = $_GET["batchID"];

$currentDate = date("Y-m-d");

$connection = new libgeneralsettings();
$json = new JSON_obj();
// ======================================================================== Header ========================================================================//
$sql = "
		SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
		EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,RlsNo,CycleStart,CycleClose,ModDateFr,ModDateTo
		FROM(
			SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='" . IntegerSafe($recordID) . "'
		) as iptf
		INNER JOIN(
			SELECT TemplateID," . $indexVar['libappraisal']->getLangSQL("FrmTitleChi", "FrmTitleEng") . " as FormTitle," . $indexVar['libappraisal']->getLangSQL("FrmCodChi", "FrmCodEng") . " as FormCode," . $indexVar['libappraisal']->getLangSQL("HdrRefChi", "HdrRefEng") . " as HdrRef," . $indexVar['libappraisal']->getLangSQL("ObjChi", "ObjEng") . " as Obj," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,
			AppTgtChi,AppTgtEng
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON iptf.TemplateID=ipsf.TemplateID
		INNER JOIN(
			SELECT CycleID," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as CycleDescr,AcademicYearID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
			FROM INTRANET_PA_C_CYCLE
		) as ipcc ON iptf.CycleID=ipcc.CycleID
		INNER JOIN(
			SELECT BatchID,CycleID,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo FROM INTRANET_PA_C_BATCH
		) as ipcb ON ipcc.CycleID=ipcb.CycleID
		INNER JOIN(
			SELECT RlsNo,RecordID,BatchID,SubDate,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as  ModDateTo FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
		) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
		";
// echo $sql."<br/>";
$a = $connection->returnResultSet($sql);
$appRole = Get_Lang_Selection($a[0]["AppTgtChi"], $a[0]["AppTgtEng"]);
$academicYearID = $a[0]["AcademicYearID"];
$canEdit = ($a[0]["OverdueDate"] == "0" && $a[0]["SubDate"] == "") ? "1" : "0";
$templateID = $a[0]["TemplateID"];
$cycleStart = $a[0]["CycleStart"];
$cycleClose = $a[0]["CycleClose"];
$modDateFr = $a[0]["ModDateFr"];
$modDateTo = $a[0]["ModDateTo"];
$editPrdFr = $a[0]["EditPrdFr"];
$editPrdTo = $a[0]["EditPrdTo"];

$activeForm = false;
if (strtotime($cycleStart) <= strtotime($currentDate) && strtotime($cycleClose) >= strtotime($currentDate)) {
    $activeForm = true;
}

// ======================================================================== View Related Form ========================================================================//
$sql = "SELECT RelatedTemplates FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='" . $templateID . "' AND CycleID=" . IntegerSafe($cycleID);
$relatedTemplateList = current($connection->returnVector($sql));
if (! empty($relatedTemplateList)) {
    $sql = "SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1 AND TemplateID IN (" . $relatedTemplateList . ")";
    $relatedTemplateData = $connection->returnArray($sql);
    foreach ($relatedTemplateData as $rtd) {
        $btnAry[] = array(
            'view_stat',
            'javascript:goViewRelated(' . $rtd['TemplateID'] . ')',
            Get_Lang_Selection($rtd["FrmInfoChi"], $rtd["FrmInfoEng"])
        );
    }
    $htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
}
// ======================================================================== Pre-defined form header ========================================================================//
$sql = "SELECT TemplateID,DataTypeID," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,DisplayOrder,Incl
			FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='" . IntegerSafe($templateID) . "'
			ORDER BY DataTypeID,DisplayOrder
			";
// echo $sql."<br/><br/>";
$preDefHeader = $connection->returnResultSet($sql);
$cycleID = $a[0]["CycleID"];
$userID = $a[0]["UserID"];

$x .= "<div class=\"formTitle\">";
$x .= $a[0]["FormTitle"];
if ($a[0]["CycleDescr"] != "") {
    $x .= "(" . $a[0]["CycleDescr"] . ")";
}
$x .= "</div>" . "\r\n";
$x .= "<div>" . "<span class=\"formObjective\" style=\"float: left;\">" . $a[0]["FormCode"] . " - " . $a[0]["Obj"] . "</span>" . "<span class=\"formRefHeader\" style=\"float: right;\">" . $a[0]["HdrRef"] . "</span>" . "</div><br/><br/>" . "\r\n";
$x .= "<div>" . nl2br($a[0]["Descr"]) . "</div><br/>" . "\r\n";
// ======================================================================== Personal Master ========================================================================//
$sql = "SELECT UserID,EnglishName,ChineseName,Gender
			FROM(
				SELECT UserID,EnglishName,ChineseName,Gender FROM " . $appraisalConfig['INTRANET_USER'] . " WHERE UserID='" . IntegerSafe($a[0]["UserID"]) . "'
			) as iu";
$iu = $connection->returnResultSet($sql);
$tgtName = ($_SESSION['intranet_session_language'] == "en") ? $Lang['Appraisal']['ARFormTgtName'] . $a[0]["AppTgtEng"] : $a[0]["AppTgtChi"] . $Lang['Appraisal']['ARFormTgtName'];
$x .= "<table class=\"form_table_v30\">";
if ($iu[0]["ChineseName"] == "") {
    $x .= "<tr><td class=\"field_title\">" . $tgtName . "</td><td>" . $iu[0]["EnglishName"] . "</td></tr>" . "\r\n";
} else {
    $x .= "<tr><td class=\"field_title\">" . $tgtName . "</td><td>" . $iu[0]["EnglishName"] . "(" . $iu[0]["ChineseName"] . ")" . "</td></tr>" . "\r\n";
}
$x .= "</table><br/>" . "\r\n";
;
// ======================================================================== Personal Pariculars ========================================================================//
if ($preDefHeader[0]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,NameChi,NameEng,Gender,DATE_FORMAT(DateOfBirth, '%Y-%m-%d') as DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,Publication,ExtServ,TotClsCnt
			FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
			";
    $perPar = $connection->returnResultSet($sql);
    $sql = "SELECT " . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr, Incl, DataSubTypeID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='" . IntegerSafe($templateID) . "' AND DataTypeID=1 ORDER BY DisplayOrder";
    $subSectionPar = $connection->returnResultSet($sql);
    $gender = ($perPar[0]["Gender"] == "M") ? $Lang['Appraisal']['ARFormMale'] : $Lang['Appraisal']['ARFormFemale'];
    $x0 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[0]["Descr"]) . "<br/>\r\n";
    $x0 .= "<table class=\"form_table_v30\">";
    foreach ($subSectionPar as $subsection) {
        if ($subsection['Incl'] == false)
            continue;
        switch ($subsection['DataSubTypeID']) {
            case 1:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["NameEng"] . "</td></tr>" . "\r\n";
                break;
            case 2:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["NameChi"] . "</td></tr>" . "\r\n";
                break;
            case 3:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $gender . "</td></tr>" . "\r\n";
                break;
            case 4:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["DateOfBirth"] . "</td></tr>" . "\r\n";
                break;
            case 5:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["Religion"] . "</td></tr>" . "\r\n";
                break;
            case 6:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["MaritalStatus"] . "</td></tr>" . "\r\n";
                break;
            case 7:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["AddressChi"] . "</td></tr>" . "\r\n";
                break;
            case 8:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["AddressEng"] . "</td></tr>" . "\r\n";
                break;
            case 9:
                $x0 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $perPar[0]["PhoneNo"] . "</td></tr>" . "\r\n";
                break;
        }
    }
    $x0 .= "</table><br/>" . "\r\n";
}
// ======================================================================== Present Job Information ========================================================================//
if ($preDefHeader[1]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,JobGrade,DATE_FORMAT(GradeEntDate,'%Y-%m-%d') as GradeEntDate,DATE_FORMAT(HireDateSch,'%Y-%m-%d') as HireDateSch,DATE_FORMAT(HireDateOrgn,'%Y-%m-%d') as HireDateOrgn,SenGov,SenPri,SenOrgn,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry
			FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);

    $sql = "SELECT " . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr, Incl, DataSubTypeID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='" . IntegerSafe($templateID) . "' AND DataTypeID=2 ORDER BY DisplayOrder";
    $subSectionPar = $connection->returnResultSet($sql);

    $x1 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[1]["Descr"]) . "<br/>\r\n";
    $x1 .= "<table class=\"form_table_v30\">";
    foreach ($subSectionPar as $subsection) {
        if ($subsection['Incl'] == false)
            continue;
        switch ($subsection['DataSubTypeID']) {
            case 1:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["JobGrade"] . "</td></tr>" . "\r\n";
                break;
            case 2:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["GradeEntDate"] . "</td></tr>" . "\r\n";
                break;
            case 3:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["HireDateSch"] . "</td></tr>" . "\r\n";
                break;
            case 4:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["HireDateOrgn"] . "</td></tr>" . "\r\n";
                break;
            case 5:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["SenGov"] . "</td></tr>" . "\r\n";
                break;
            case 6:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["SenPri"] . "</td></tr>" . "\r\n";
                break;
            case 7:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["SenOrgn"] . "</td></tr>" . "\r\n";
                break;
            case 8:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["RegNo"] . "</td></tr>" . "\r\n";
                break;
            case 9:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["SalaryPoint"] . "</td></tr>" . "\r\n";
                break;
            case 10:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["ActingPosition"] . "</td></tr>" . "\r\n";
                break;
            case 11:
                if ($a[0]["LongHoliday"] == "") {
                    $holidayStart = "";
                    $holidayEnd = "";
                    $holidayType = "";
                    $holidayDuration = "";
                } else {
                    $longHolidayArr = unserialize($a[0]["LongHoliday"]);
                    $holidayStart = $longHolidayArr['holidayStart'];
                    $holidayEnd = $longHolidayArr['holidayEnd'];
                    $holidayType = $longHolidayArr['holidayType'];
                    $holidayDuration = $longHolidayArr['holidayDuration'];
                }
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td>";
                $x1 .= "<td>" . $Lang['General']['From'] . " " . $holidayStart . " ";
                $x1 .= $Lang['General']['To'] . " " . $holidayEnd . "<br>\n\r";
                $x1 .= $Lang['Appraisal']['ARFormNature'] . ":" . $holidayType . "\t\t";
                $x1 .= $Lang['Appraisal']['CycleExportLeaveChi']['Days'] . ":" . $holidayDuration;
                $x1 .= "</td>";
                break;
            case 12:
                $x1 .= "<tr><td class=\"field_title\">" . $subsection['Descr'] . "</td><td>" . $a[0]["PositionOnEntry"] . "</td></tr>" . "\r\n";
                break;
        }
    }
    $x1 .= "</table><br/>" . "\r\n";
}
// ======================================================================== Qualification Acquired/ Study-in-progress during appraising period ========================================================================//
if ($preDefHeader[2]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,SeqID,IsCompleted,DATE_FORMAT(IssueDate,'%Y-%m-%d') as IssueDate,Issuer,QualiDescr,Programme
			FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "';";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x2 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[2]["Descr"]) . "<br/>\r\n";

    $x2 .= "<div id=\"quaAcqInfoLayer\">";
    $x2 .= "<table id=\"quaAcqInfoTable\" class=\"common_table_list\">";
    $x2 .= "<thead>";
    $x2 .= "<tr><th style=\"width:2%\">" . $Lang['Appraisal']['ARFormSeq'] . "</th><th style=\"width:20%\">" . $Lang['Appraisal']['ARFormQuaAcqQuaDesc'] . "</th>";
    $x2 .= "<th style=\"width:16%\">" . $Lang['Appraisal']['ARFormQuaAcqSts'] . "</th><th style=\"width:17%\">" . $Lang['Appraisal']['ARFormQuaAcqCmpDat'] . "</th>";
    $x2 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormQuaAcqAccBody'] . "</th><th style=\"width:20%\">" . $Lang['Appraisal']['ARFormQuaAcqPrgDtl'] . "</th>";
    $x2 .= "<th style=\"width:10%\"></th>";
    $x2 .= "</tr></thead>";
    $x2 .= "<tbody>";
    $runningIndex = 0;
    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x2 .= "<tr id=\"qualificationRow_" . $j . "\">";
        $x2 .= "<td>";
        $x2 .= "<div id=\"seqID_" . $a[$i]["SeqID"] . "\" name=\"seqID_" . $a[$i]["SeqID"] . "\">";
        $x2 .= $a[$i]["SeqID"];
        $x2 .= "</div>";
        $x2 .= "</td>";
        $x2 .= "<td>";
        $x2 .= $a[$i]["QualiDescr"];
        $x2 .= "</td>";
        $x2 .= "<td>";
        $x2 .= $Lang['Appraisal']['ARFormQuaAcqStsDes'][$a[$i]["IsCompleted"]];
        $x2 .= "</td>";
        $x2 .= "<td>";
        $x2 .= $a[$i]["IssueDate"];
        $x2 .= "</td>";
        $x2 .= "<td>";
        $x2 .= $a[$i]["Issuer"];
        $x2 .= "</td>";
        $x2 .= "<td>";
        $x2 .= $a[$i]["Programme"];
        $x2 .= "</td>";
        $x2 .= "<td>";
        $x2 .= "<input type=\"hidden\" id=\"hiddenQualification_" . $j . "\" name=\"hiddenQualification_" . $j . "\" VALUE=\"ACTIVE\">";
        $x2 .= "<input type=\"hidden\" id=\"hiddenSeqID_" . $j . "\" name=\"hiddenSeqID_" . $j . "\" VALUE=\"" . $a[$i]["SeqID"] . "\">";
        $x2 .= "</td>";
        $x2 .= "</tr>";
        $runningIndex ++;
    }
    $x2 .= "</tbody>";
    $x2 .= "</table>";
    $x2 .= "</div><br/>";
    if ($sys_custom['eAppraisal']['HidePublishingThisYear'] != true || $sys_custom['eAppraisal']['HideServiceOutsideSchool'] != true) {
        $x2 .= "<table class=\"form_table_v30\">";
        if ($sys_custom['eAppraisal']['HidePublishingThisYear'] != true) {
            $x2 .= "<tr><td class=\"field_title\">" . $Lang['Appraisal']['ARFormPubYear'] . "</td><td>" . $perPar[0]["Publication"] . "</td></tr>" . "\r\n";
        }
        if ($sys_custom['eAppraisal']['HideServiceOutsideSchool'] != true) {
            $x2 .= "<tr><td class=\"field_title\">" . $Lang['Appraisal']['ARFormExtServ'] . "</td><td>" . $perPar[0]["ExtServ"] . "</td></tr>" . "\r\n";
        }
        $x2 .= "</table><br/>";
    }
}
// ======================================================================== Present Teaching Duties per Class ========================================================================//
if ($preDefHeader[3]["Incl"] == 1) {
    $x3 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[3]["Descr"]) . "<br/>\r\n";
    $x3 .= "<table class=\"form_table_v30\">";
    $x3 .= "<tr><td class=\"field_title\">" . $Lang['Appraisal']['ARFormTotClsCnt'] . "</td><td>" . $perPar[0]["TotClsCnt"] . "</td></tr>" . "\r\n";
    $x3 .= "</table><br/>";
    if ($indexVar['libappraisal']->isEJ()) {
        $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearClassID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,ClassTitle
				FROM(
					SELECT RecordID,BatchID,YearClassID,SubjectID,Period,DateTimeInput
					FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
				) as iptpt
				LEFT JOIN(
					SELECT SubjectID as RecordID, SubjectName as SName
					FROM INTRANET_SUBJECT WHERE RecordStatus = 1
				) as assub ON iptpt.SubjectID=assub.RecordID
				LEFT JOIN(
					SELECT ClassID as YearClassID, ClassName as ClassTitle
					FROM INTRANET_CLASS
				) as yc ON iptpt.YearClassID=yc.YearClassID
				ORDER BY DateTimeInput";
    } else {
        $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearClassID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,ClassTitle
				FROM(
					SELECT RecordID,BatchID,YearClassID,SubjectID,Period,DateTimeInput
					FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
				) as iptpt
				LEFT JOIN(
					SELECT RecordID,CODEID," . $indexVar['libappraisal']->getLangSQL("CH_SNAME", "EN_SNAME") . " as SName
					FROM ASSESSMENT_SUBJECT
				) as assub ON iptpt.SubjectID=assub.RecordID
				LEFT JOIN(
					SELECT YearClassID,AcademicYearID," . $indexVar['libappraisal']->getLangSQL("ClassTitleB5", "ClassTitleEN") . " as ClassTitle
					FROM YEAR_CLASS
				) as yc ON iptpt.YearClassID=yc.YearClassID
				ORDER BY DateTimeInput";
    }
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $runningIndex = 0;

    // $x3 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
    $x3 .= "<div id=\"teachingClassLayer\">";
    $x3 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
    $x3 .= "<thead>";
    $x3 .= "<tr><th style=\"width:30%\">" . $Lang['Appraisal']['ARFormCls'] . "</th><th style=\"width:30%\">" . $Lang['Appraisal']['ARFormTeachSubj'] . "</th>";
    $x3 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormClsPerCyc'] . "</th>";
    $x3 .= "<th style=\"width:10%\"></th>";
    $x3 .= "</tr></thead>";
    $x3 .= "<tbody>";

    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x3 .= "<tr id=\"teachClassRow_" . $j . "\">";
        $x3 .= "<td>" . $indexVar['libappraisal']->displayChinese($a[$i]["ClassTitle"]) . "</td>";
        $x3 .= "<td >" . $indexVar['libappraisal']->displayChinese($a[$i]["SName"]) . "</td>";
        $x3 .= "<td>";
        $x3 .= $a[$i]["Period"];
        $x3 .= "</td>";
        $x3 .= "<td>";
        $x3 .= "</td>";
        $x3 .= "<input type=\"hidden\" id=\"hiddenYearClass_" . $j . "\" name=\"hiddenYearClass_" . $j . "\" VALUE=\"" . $a[$i]["YearClassID"] . "\">";
        $x3 .= "<input type=\"hidden\" id=\"hiddenSubject_" . $j . "\" name=\"hiddenSubject_" . $j . "\" VALUE=\"" . $a[$i]["SubjectID"] . "\">";
        $x3 .= "<input type=\"hidden\" id=\"hiddenClass_" . $j . "\" name=\"hiddenClass_" . $j . "\" VALUE=\"ACTIVE\">";
        $x3 .= "</tr>";
        $runningIndex ++;
    }
    $x3 .= "</tbody>";
    $x3 .= "</table>";
    $x3 .= "</div><br/>";
}
// ======================================================================== Present Teaching Duties per Form ========================================================================//
if ($preDefHeader[4]["Incl"] == 1) {
    $x4 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[4]["Descr"]) . "<br/>\r\n";
    $x4 .= "<table class=\"form_table_v30\">";
    $x4 .= "<tr><td class=\"field_title\">" . $Lang['Appraisal']['ARFormTotClsCnt'] . "</td><td>" . $perPar[0]["TotClsCnt"] . "</td></tr>" . "\r\n";
    $x4 .= "</table><br/>";
    if ($indexVar['libappraisal']->isEJ()) {
        $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,YearName
				FROM(
					SELECT RecordID,BatchID,YearID,SubjectID,Period,DateTimeInput
					FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
				) as iptpt
				LEFT JOIN(
					SELECT SubjectID as RecordID, SubjectName as SName
					FROM INTRANET_SUBJECT  WHERE RecordStatus = 1
				) as assub ON iptpt.SubjectID=assub.RecordID
				LEFT JOIN(
					SELECT ClassLevelID as YearID, LevelName as YearName FROM INTRANET_CLASSLEVEL
				) as year ON iptpt.YearID=year.YearID
				ORDER BY DateTimeInput";
    } else {
        $sql = "SELECT iptpt.RecordID,BatchID,iptpt.YearID,SubjectID,Period,DateTimeInput,IFNULL(SName,SubjectID) as SName,YearName
				FROM(
					SELECT RecordID,BatchID,YearID,SubjectID,Period,DateTimeInput
					FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
				) as iptpt
				LEFT JOIN(
					SELECT RecordID,CODEID," . $indexVar['libappraisal']->getLangSQL("CH_SNAME", "EN_SNAME") . " as SName
					FROM ASSESSMENT_SUBJECT
				) as assub ON iptpt.SubjectID=assub.RecordID
				LEFT JOIN(
					SELECT YearID,YearName,Sequence FROM YEAR
				) as year ON iptpt.YearID=year.YearID
				ORDER BY DateTimeInput";
    }
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $runningIndex = 0;

    // $x4 .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($preDefHeader[4]["Descr"])."<br/>\r\n";
    $x4 .= "<div id=\"teachingClassLayer\">";
    $x4 .= "<table id=\"teachingClassTable\" class=\"common_table_list\">";
    $x4 .= "<thead>";
    $x4 .= "<tr><th style=\"width:30%\">" . $Lang['Appraisal']['ARFormCls'] . "</th><th style=\"width:30%\">" . $Lang['Appraisal']['ARFormTeachSubj'] . "</th>";
    $x4 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormClsPerCyc'] . "</th>";
    $x4 .= "<th style=\"width:10%\"></th>";
    $x4 .= "</tr></thead>";
    $x4 .= "<tbody>";

    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x4 .= "<tr id=\"teachFormRow_" . $j . "\">";
        $x4 .= "<td>" . $indexVar['libappraisal']->displayChinese($a[$i]["YearName"]) . "</td>";
        $x4 .= "<td >" . $indexVar['libappraisal']->displayChinese($a[$i]["SName"]) . "</td>";
        $x4 .= "<td>";
        $x4 .= $a[$i]["Period"];
        $x4 .= "</td>";
        $x4 .= "<td>";
        $x4 .= "</td>";
        $x4 .= "<input type=\"hidden\" id=\"hiddenYear_" . $j . "\" name=\"hiddenYear_" . $j . "\" VALUE=\"" . $a[$i]["YearID"] . "\">";
        $x4 .= "<input type=\"hidden\" id=\"hiddenFormSubject_" . $j . "\" name=\"hiddenFormSubject_" . $j . "\" VALUE=\"" . $a[$i]["SubjectID"] . "\">";
        $x4 .= "<input type=\"hidden\" id=\"hiddenForm_" . $j . "\" name=\"hiddenForm_" . $j . "\" VALUE=\"ACTIVE\">";
        $x4 .= "</tr>";
        $runningIndex ++;
    }
    /*
     * $x4 .= "<tr height=\"34px\"><td></td><td></td><td></td><td>";
     * $x4 .= "<div class=\"table_row_tool row_content_tool\">";
     * $x4 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addForm('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
     * $x4 .= "<input type=\"hidden\" id=\"currentFormRow\" name=\"currentFormRow\" VALUE=\"".sizeof($a)."\">";
     * $x4 .= "<input type=\"hidden\" id=\"totalFormRow\" name=\"totalFormRow\" VALUE=\"".$runningIndex."\">";
     * $x4 .= "</td></tr>";
     */
    $x4 .= "</tbody>";
    $x4 .= "</table>";
    $x4 .= "</div><br/>";
}
// ======================================================================== Present Administrative Duties ========================================================================//
if ($preDefHeader[5]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
			AND DutyType='A'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x5 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[5]["Descr"]) . "<br/>\r\n";
    $x5 .= "<div id=\"othAdmInfoLayer\">";
    $x5 .= "<table id=\"othAdmInfoTable\" class=\"common_table_list\">";
    $x5 .= "<thead>";
    $x5 .= "<tr><th style=\"width:5%\">" . $Lang['Appraisal']['ARFormSeq'] . "</th><th style=\"width:95%\">" . $Lang['Appraisal']['ARFormOthAdmDesc'] . "</th>";
    $x5 .= "</tr></thead>";
    $x5 .= "<tbody>";
    for ($i = 0; $i < $ColForPreSetField; $i ++) {
        $j = $i + 1;
        if ($a[$i]["Descr"] == "")
            continue;
        $x5 .= "<tr id=\"othAdmRow_" . $j . "\">";
        $x5 .= "<td >" . $a[$i]["SeqID"] . "</td>";
        $x5 .= "<td>";
        $x5 .= $a[$i]["Descr"];
        $x5 .= "</td>";
        $x5 .= "</tr>";
    }
    $x5 .= "</tbody>";
    $x5 .= "</table></div><br/>";
}
// ======================================================================== Present Extra-Curriculum Activity Duties ========================================================================//
if ($preDefHeader[6]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
			AND DutyType='E'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x6 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[6]["Descr"]) . "<br/>\r\n";
    $x6 .= "<div id=\"othExtCurInfoLayer\">";
    $x6 .= "<table id=\"othExtCurInfoTable\" class=\"common_table_list\">";
    $x6 .= "<thead>";
    $x6 .= "<tr><th style=\"width:5%\">" . $Lang['Appraisal']['ARFormSeq'] . "</th><th style=\"width:95%\">" . $Lang['Appraisal']['ARFormOthExtCurDesc'] . "</th>";
    $x6 .= "</tr></thead>";
    $x6 .= "<tbody>";
    for ($i = 0; $i < $ColForPreSetField; $i ++) {
        $j = $i + 1;
        if ($a[$i]["Descr"] == "")
            continue;
        $x6 .= "<tr id=\"othExtCurRow_" . $j . "\">";
        $x6 .= "<td >" . $a[$i]["SeqID"] . "</td>";
        $x6 .= "<td>";
        $x6 .= $a[$i]["Descr"];
        $x6 .= "</td>";
        $x6 .= "</tr>";
    }
    $x6 .= "</tbody>";
    $x6 .= "</table></div><br/>";
}
// ======================================================================== Present Other Duties ========================================================================//
if ($preDefHeader[7]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
			AND DutyType='O'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x7 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[7]["Descr"]) . "<br/>\r\n";
    $x7 .= "<div id=\"othDutInfoLayer\">";
    $x7 .= "<table id=\"othDutInfoTable\" class=\"common_table_list\">";
    $x7 .= "<thead>";
    $x7 .= "<tr><th style=\"width:5%\">" . $Lang['Appraisal']['ARFormSeq'] . "</th><th style=\"width:95%\">" . $Lang['Appraisal']['ARFormOthDutDesc'] . "</th>";
    $x7 .= "</tr></thead>";
    $x7 .= "<tbody>";
    for ($i = 0; $i < $ColForPreSetField; $i ++) {
        $j = $i + 1;
        if ($a[$i]["Descr"] == "")
            continue;
        $x7 .= "<tr id=\"othDutRow_" . $j . "\">";
        $x7 .= "<td >" . $a[$i]["SeqID"] . "</td>";
        $x7 .= "<td>";
        $x7 .= $a[$i]["Descr"];
        $x7 .= "</td>";
        $x7 .= "</tr>";
    }
    $x7 .= "</tbody>";
    $x7 .= "</table></div><br/>";
}
// ======================================================================== Absence & Lateness ========================================================================//
if ($preDefHeader[8]["Incl"] == 1) {
    $sql = "SELECT '1' as SeqID,AbsLsnCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT '2' as SeqID,SubLsnCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT '3' as SeqID,LateCnt as Value FROM INTRANET_PA_C_ABSENCE WHERE CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x8 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[8]["Descr"]) . "\r\n";
    $x8 .= "<div id=\"asbLatInfoLayer\">";
    $x8 .= "<table id=\"asbLatInfoTable\" class=\"form_table_v30\">";
    $x8 .= "<thead>";
    $x8 .= "<tr>";
    $x8 .= "</tr></thead>";
    $x8 .= "<tbody>";
    $x8 .= "<tr>";
    $x8 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormNoLesAsb'] . "</td>";
    $x8 .= "<td>";
    $x8 .= $a[0]["Value"];
    $x8 .= "</td>";
    $x8 .= "</tr>";
    $x8 .= "<tr>";
    $x8 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormNoSubLes'] . "</td>";
    $x8 .= "<td>";
    $x8 .= $a[1]["Value"];
    $x8 .= "</td>";
    $x8 .= "</tr>";
    $x8 .= "<tr>";
    $x8 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormNoTimLat'] . "</td>";
    $x8 .= "<td>";
    $x8 .= $a[2]["Value"];
    $x8 .= "</td>";
    $x8 .= "</tr>";
    $x8 .= "</tbody>";
    $x8 .= "</table></div><br/>";
}
// ======================================================================== Leaves Records (Pay & Non-Pay) ========================================================================//
if ($preDefHeader[9]["Incl"] == 1) {
    $sql = "SELECT CycleID,1 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Sick Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT CycleID,2 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Official Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT CycleID,3 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Study Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT CycleID,4 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Maternity Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x9 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[9]["Descr"]) . "<br/>\r\n";
    $x9 .= "<div id=\"leavePaidInfoLayer\">";
    $x9 .= "<table id=\"leavePaidInfoTable\" class=\"common_table_list\">";
    $x9 .= "<thead>";
    $x9 .= "<tr>";
    $x9 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormLeaCat'] . "</th><th style=\"width:25%\">" . $Lang['Appraisal']['ARFormPayLea'] . "</th>";
    $x9 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormNoPayLea'] . "</th><th style=\"width:25%\">" . $Lang['Appraisal']['ARFormRemarks'] . "</th>";
    $x9 .= "</tr></thead>";
    $x9 .= "<tbody>";
    $x9 .= "<tr>";
    $x9 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormSickLeave'] . "</td>";
    $x9 .= "<td>";
    $x9 .= $a[0]["DaysPaid"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[0]["DaysNoPay"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[0]["Remark"];
    $x9 .= "</td>";
    $x9 .= "</tr>";
    $x9 .= "<tr>";
    $x9 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormOfficialLeave'] . "</td>";
    $x9 .= "<td>";
    $x9 .= $a[1]["DaysPaid"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[1]["DaysNoPay"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[1]["Remark"];
    $x9 .= "</td>";
    $x9 .= "</tr>";
    $x9 .= "<tr>";
    $x9 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormStudyLeave'] . "</td>";
    $x9 .= "<td>";
    $x9 .= $a[2]["DaysPaid"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[2]["DaysNoPay"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[2]["Remark"];
    $x9 .= "</td>";
    $x9 .= "</tr>";
    $x9 .= "<tr>";
    $x9 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormMaternityLeave'] . "</td>";
    $x9 .= "<td>";
    $x9 .= $a[3]["DaysPaid"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[3]["DaysNoPay"];
    $x9 .= "</td>";
    $x9 .= "<td>";
    $x9 .= $a[3]["Remark"];
    $x9 .= "</td>";
    $x9 .= "</tr>";
    $x9 .= "</tbody>";
    $x9 .= "</table></div><br/>";
}
// ======================================================================== Leave Records ========================================================================//
if ($preDefHeader[10]["Incl"] == 1) {
    $sql = "SELECT CycleID,1 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Sick Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT CycleID,2 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Official Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT CycleID,3 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Study Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			UNION
			SELECT CycleID,4 as SeqID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark FROM INTRANET_PA_C_LEAVE
			WHERE LeaveType='Maternity Leave' AND CycleID='" . $cycleID . "' AND UserID='" . IntegerSafe($userID) . "'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x10 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[10]["Descr"]) . "<br/>\r\n";
    $x10 .= "<div id=\"leavePaidTotalInfoLayer\">";
    $x10 .= "<table id=\"leavePaidTotalInfoTable\" class=\"common_table_list\">";
    $x10 .= "<thead>";
    $x10 .= "<tr>";
    $x10 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormLeaCat'] . "</th><th style=\"width:25%\">" . $Lang['Appraisal']['ARFormPayTot'] . "</th>";
    $x10 .= "<th style=\"width:50%\">" . $Lang['Appraisal']['ARFormRemarks'] . "</th>";
    $x10 .= "</tr></thead>";
    $x10 .= "<tbody>";
    $x10 .= "<tr>";
    $x10 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormSickLeave'] . "</td>";
    $x10 .= "<td>";
    $x10 .= (empty($a[0]["DaysTot"])) ? "0.00" : $a[0]["DaysTot"];
    $x10 .= "</td>";
    $x10 .= "<td>";
    $x10 .= $a[0]["Remark"];
    $x10 .= "</td>";
    $x10 .= "</tr>";
    $x10 .= "<tr>";
    $x10 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormOfficialLeave'] . "</td>";
    $x10 .= "<td>";
    $x10 .= (empty($a[1]["DaysTot"])) ? "0.00" : $a[1]["DaysTot"];
    $x10 .= "</td>";
    $x10 .= "<td>";
    $x10 .= $a[1]["Remark"];
    $x10 .= "</td>";
    $x10 .= "</tr>";
    $x10 .= "<tr>";
    $x10 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormStudyLeave'] . "</td>";
    $x10 .= "<td>";
    $x10 .= (empty($a[2]["DaysTot"])) ? "0.00" : $a[2]["DaysTot"];
    $x10 .= "</td>";
    $x10 .= "<td>";
    $x10 .= $a[2]["Remark"];
    $x10 .= "</td>";
    $x10 .= "</tr>";
    $x10 .= "<tr>";
    $x10 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormMaternityLeave'] . "</td>";
    $x10 .= "<td>";
    $x10 .= (empty($a[3]["DaysTot"])) ? "0.00" : $a[3]["DaysTot"];
    $x10 .= "</td>";
    $x10 .= "<td>";
    $x10 .= $a[3]["Remark"];
    $x10 .= "</td>";
    $x10 .= "</tr>";
    $x10 .= "</tbody>";
    $x10 .= "</table></div><br/>";
}
// ======================================================================== Attended Course/Seminar/Workshop ========================================================================//
if ($preDefHeader[11]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,SeqID,DATE_FORMAT(EventDate,'%Y-%m-%d') as EventDate,Orgn,CrsSemWs,Hours,Certification
			FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "';";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x11 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[11]["Descr"]) . "<br/>\r\n";

    $x11 .= "<div id=\"crsSemWsInfoLayer\">";
    $x11 .= "<table id=\"crsSemWsInfoTable\" class=\"common_table_list\">";
    $x11 .= "<thead>";
    $x11 .= "<tr><th style=\"width:2%\">" . $Lang['Appraisal']['ARFormSeq'] . "</th><th style=\"width:10%\">" . $Lang['Appraisal']['ARFormDate'] . "</th>";
    $x11 .= "<th style=\"width:28%\">" . $Lang['Appraisal']['ARFormOrganization'] . "</th><th style=\"width:20%\">" . $Lang['Appraisal']['ARFormCourseSeminarWorkshop'] . "</th>";
    $x11 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormHours'] . "</th><th style=\"width:20%\">" . $Lang['Appraisal']['ARFormCertification'] . "</th>";
    $x11 .= "<th style=\"width:10%\"></th>";
    $x11 .= "</tr></thead>";
    $x11 .= "<tbody>";
    $runningIndex = 0;
    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x11 .= "<tr id=\"crsSemWsRow_" . $j . "\">";
        $x11 .= "<td>";
        $x11 .= "<div id=\"crsSemWsSeqID_" . $a[$i]["SeqID"] . "\" name=\"crsSemWsSeqID_" . $a[$i]["SeqID"] . "\">";
        $x11 .= $a[$i]["SeqID"];
        $x11 .= "</div>";
        $x11 .= "</td>";
        $x11 .= "<td>";
        $x11 .= $a[$i]["EventDate"];
        $x11 .= "</td>";
        $x11 .= "<td>";
        $x11 .= $a[$i]["Orgn"];
        $x11 .= "</td>";
        $x11 .= "<td>";
        $x11 .= $a[$i]["CrsSemWs"];
        $x11 .= "</td>";
        $x11 .= "<td>";
        $x11 .= $a[$i]["Hours"];
        $x11 .= "</td>";
        $x11 .= "<td>";
        $x11 .= $a[$i]["Certification"];
        $x11 .= "</td>";
        $x11 .= "<td>";
        $x11 .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
        $x11 .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"" . $Lang['Appraisal']['ARFormDelete'] . "\" onclick=\"deleteCrsSemWs('" . $recordID . "','" . $batchID . "','" . $j . "');\"></a></span>";
        $x11 .= "</span>";
        $x11 .= "<input type=\"hidden\" id=\"hiddenCrsSemWsSts_" . $j . "\" name=\"hiddenCrsSemWsSts_" . $j . "\" VALUE=\"ACTIVE\">";
        $x11 .= "<input type=\"hidden\" id=\"hiddenCrsSemWsSeqID_" . $j . "\" name=\"hiddenCrsSemWsSeqID_" . $j . "\" VALUE=\"" . $a[$i]["SeqID"] . "\">";
        $x11 .= "</td>";
        $x11 .= "</tr>";
        $runningIndex ++;
    }
    /*
     * $x11 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td>";
     * $x11 .= "<div class=\"table_row_tool row_content_tool\">";
     * $x11 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addCrsSemWs('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
     * $x11 .= "<input type=\"hidden\" id=\"currentCrsSemWsRow\" name=\"currentCrsSemWsRow\" VALUE=\"".sizeof($a)."\">";
     * $x11 .= "<input type=\"hidden\" id=\"totalCrsSemWsRow\" name=\"totalCrsSemWsRow\" VALUE=\"".$runningIndex."\">";
     * $x11 .= "</td></tr>";
     */
    $x11 .= "</tbody>";
    $x11 .= "</table>";
    $x11 .= "</div><br/>";
}
// ======================================================================== Teaching / Administrative Duties Objectives and Assessment ========================================================================//
if ($preDefHeader[12]["Incl"] == 1) {
    $x12 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[12]["Descr"]) . "<br/>\r\n";
    $x12 .= "<div id=\"taskAssessmentLayer\">";
    if ($Lang['Appraisal']['ARFormTaskDesc'] != "") {
        $x12 .= "<p>";
        $x12 .= $Lang['Appraisal']['ARFormTaskDesc'];
        $x12 .= "</p>";
    }
    $sql = "SELECT RecordID,BatchID,SeqID,Objective,Plan,Assessment,Standard,CatID
			FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "';";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    if ($sys_custom['eAppraisal']['DutyObjectivesAssessmentIn4Parts'] == true) {
        $taskListByCatArr = array();
        foreach ($a as $taskRecord) {
            if (! isset($taskListByCatArr[$taskRecord['CatID']])) {
                $taskListByCatArr[$taskRecord['CatID']] = array();
            }
            array_push($taskListByCatArr[$taskRecord['CatID']], $taskRecord);
        }
        for ($index = 1; $index <= 4; $index ++) {
            $runningIndex = 0;

            $x12 .= "<p>" . $Lang['Appraisal']['ARFormTaskParts'][$index] . "</p>";
            $x12 .= "<table id=\"taskAssessmentTable\" class=\"common_table_list\">";
            $x12 .= "<thead>";
            $x12 .= "<tr>";
            $x12 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormTaskObjective'] . "</th>";
            $x12 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormTaskStandard'] . "</th>";
            $x12 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormTaskAction'] . "</th>";
            $x12 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormTaskAchievement'] . "</th>";
            $x12 .= "</tr></thead>";
            $x12 .= "<tbody>";

            for ($i = 0; $i < sizeof($taskListByCatArr[$index]); $i ++) {
                $j = $i + 1;
                $x12 .= "<tr id=\"taskAssessmentRow_" . $j . "\">";
                $x12 .= "<td>";
                $x12 .= nl2br($taskListByCatArr[$index][$i]["Objective"]);
                $x12 .= "</td>";
                $x12 .= "<td>";
                $x12 .= nl2br($taskListByCatArr[$index][$i]["Standard"]);
                $x12 .= "</td>";
                $x12 .= "<td>";
                $x12 .= nl2br($taskListByCatArr[$index][$i]["Plan"]);
                $x12 .= "</td>";
                $x12 .= "<td>";
                $x12 .= nl2br($taskListByCatArr[$index][$i]["Assessment"]);
                $x12 .= "</td>";
                $x12 .= "</tr>";
                $runningIndex ++;
            }
            $x12 .= "</tbody>";
            $x12 .= "</table><br>";
        }
    } else {
        $runningIndex = 0;

        $x12 .= "<table id=\"taskAssessmentTable\" class=\"common_table_list\">";
        $x12 .= "<thead>";
        $x12 .= "<tr>";
        $x12 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormTaskObjective'] . "</th>";
        $x12 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormTaskAction'] . "</th>";
        $x12 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormTaskAssessment'] . "</th>";
        $x12 .= "</tr></thead>";
        $x12 .= "<tbody>";

        for ($i = 0; $i < sizeof($a); $i ++) {
            $j = $i + 1;
            $x12 .= "<tr id=\"taskAssessmentRow_" . $j . "\">";
            $x12 .= "<td>";
            $x12 .= nl2br($a[$i]["Objective"]);
            $x12 .= "</td>";
            $x12 .= "<td>";
            $x12 .= nl2br($a[$i]["Plan"]);
            $x12 .= "</td>";
            $x12 .= "<td>";
            $x12 .= nl2br($a[$i]["Assessment"]);
            $x12 .= "</td>";
            $x12 .= "</tr>";
            $runningIndex ++;
        }
        $x12 .= "</tbody>";
        $x12 .= "</table>";
    }
    $x12 .= "</div><br/>";
}
// ======================================================================== Present Academic Duties ========================================================================//
if ($preDefHeader[13]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,DutyType,SeqID,Descr
			FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "'
			AND DutyType='S'
			";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x13 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[13]["Descr"]) . "<br/>\r\n";
    $x13 .= "<div id=\"othAdmInfoLayer\">";
    $x13 .= "<table id=\"othAdmInfoTable\" class=\"common_table_list\">";
    $x13 .= "<thead>";
    $x13 .= "<tr><th style=\"width:5%\">" . $Lang['Appraisal']['ARFormSeq'] . "</th><th style=\"width:95%\">" . $Lang['Appraisal']['ARFormOthAcaDesc'] . "</th>";
    $x13 .= "</tr></thead>";
    $x13 .= "<tbody>";
    for ($i = 0; $i < $ColForPreSetField; $i ++) {
        $j = $i + 1;
        if ($a[$i]["Descr"] == "")
            continue;
        $x13 .= "<tr id=\"othAdmRow_" . $j . "\">";
        $x13 .= "<td >" . $a[$i]["SeqID"] . "</td>";
        $x13 .= "<td>";
        $x13 .= $a[$i]["Descr"];
        $x13 .= "</td>";
        $x13 .= "</tr>";
    }
    $x13 .= "</tbody>";
    $x13 .= "</table></div><br/>";
}
// ======================================================================== Student Training for Competition ========================================================================//
if ($preDefHeader[14]["Incl"] == 1) {
    $x14 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[14]["Descr"]) . "<br/>\r\n";

    $sql = "SELECT RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark
			FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "';";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $runningIndex = 0;

    $x14 .= "<div id=\"studentTrainingLayer\">";
    $x14 .= "<table id=\"studentTrainingTable\" class=\"common_table_list\">";
    $x14 .= "<thead>";
    $x14 .= "<tr><th style=\"width:5%\">#</th>";
    $x14 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormStdTrainComp'] . "</th>";
    $x14 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormStdTrainNo'] . "</th>";
    $x14 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormStdTrainDur'] . "</th>";
    $x14 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormStdTrainResult'] . "</th>";
    $x14 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormRemarks'] . "</th>";
    $x14 .= "</tr></thead>";
    $x14 .= "<tbody>";

    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x14 .= "<tr id=\"studentTrainingRow_" . $j . "\">";
        $x14 .= "<td>";
        $x14 .= "<div id=\"StudentTrainingSeqID_" . $a[$i]["SeqID"] . "\" name=\"StudentTrainingSeqID_" . $a[$i]["SeqID"] . "\">";
        $x14 .= $j;
        $x14 .= "</div>";
        $x14 .= "</td>";
        $x14 .= "<td>";
        $x14 .= $a[$i]["Competition"];
        $x14 .= "</td>";
        $x14 .= "<td>";
        $x14 .= $a[$i]["Trainee"];
        $x14 .= "</td>";
        $x14 .= "<td>";
        $x14 .= $a[$i]["Duration"];
        $x14 .= "</td>";
        $x14 .= "<td>";
        $x14 .= $a[$i]["Result"];
        $x14 .= "</td>";
        $x14 .= "<td>";
        $x14 .= $a[$i]["Remark"];
        $x14 .= "</td>";
        $x14 .= "</tr>";
        $runningIndex ++;
    }

    $x14 .= "</tbody>";
    $x14 .= "</table>";
    $x14 .= "</div><br/>";
}
// ======================================================================== CPD ========================================================================//
if ($preDefHeader[15]["Incl"] == 1) {
    $x15 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[15]["Descr"]) . "<br/>\r\n";
    $academicYear = $indexVar['libappraisal']->getAcademicYearTermOfCurrentCycle(date('Y-m-d h:i:s'));
    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDDurationSetting'";
    $cpdHoursCountingSeparateMonth = current($connection->returnVector($sql));
    if (empty($cpdHoursCountingSeparateMonth)) {
        $cpdHoursCountingSeparateMonth = 4;
    } else {
        $cpdHoursCountingSeparateMonth = intval($cpdHoursCountingSeparateMonth);
    }
    $nextCpdHoursCountingSeparateMonth = $cpdHoursCountingSeparateMonth + 1;
    if ($cpdHoursCountingSeparateMonth == 12) {
        $nextCpdHoursCountingSeparateMonth = 1;
    }

    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDShowingTotalHourSetting'";
    $cpdShowingTotalHour = current($connection->returnVector($sql));
    if (empty($cpdShowingTotalHour)) {
        $cpdShowingTotalHour = false;
    }

    if ($sys_custom['eAppraisal']['CPDAutoSync'] == true) {
        // Rebuild the CPD record list for auto-sync with Teacher Portfolio
        // 1st: collect CPD record of the corresponding year
        $sql = "SELECT AcademicYearID FROM INTRANET_PA_T_FRMSUM fs INNER JOIN INTRANET_PA_C_CYCLE cc ON fs.CycleID=cc.CycleID WHERE fs.RecordID='$recordID'";
        $academicYearIDs = $connection->returnArray($sql);
        $academicYear = $indexVar['libappraisal']->getAllAcademicYear($academicYearIDs[0]['AcademicYearID']);
        $sql = "SELECT FTDataRowID,StartDate,EndDate,NoOfSection,EventNameEn,EventNameChi,EventTypeEn,EventTypeChi,RemarksEn,RemarksChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated FROM INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD WHERE UserID='" . $userID . "' AND AcademicYear='" . $academicYear[0]['YearNameEN'] . "' ORDER BY StartDate";
        $cpdRecords = $connection->returnArray($sql);
        $newCPDList = array();
        // 2nd: move teacher portfolio CPD records to TA CPD record list
        if (! empty($cpdRecords)) {
            foreach ($cpdRecords as $idx => $cpd) {
                $sql = "SELECT CpdID,SeqID FROM INTRANET_PA_S_T_PRE_CPD WHERE FTDataRowID='" . $cpd['FTDataRowID'] . "' AND RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='" . $userID . "'";
                $existingRecord = current($connection->returnArray($sql));
                if (empty($existingRecord)) {
                    // insert newly added records
                    $sql = "INSERT INTO INTRANET_PA_S_T_PRE_CPD
								(RlsNo, RecordID, BatchID, SeqID, UserID, AcademicYearID, StartDate, EndDate, NoOfSection, EventNameEng, EventNameChi, EventTypeEng, EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,FTDataRowID,DateInput,InputBy,DateModified,ModifyBy,SubjectRelated)
								VALUES
								('$rlsNo', '$recordID', '$batchID', '" . ($idx + 1) . "', '" . $userID . "','" . $academicYearIDs[0]['AcademicYearID'] . "', '" . $cpd['StartDate'] . "', '" . $cpd['EndDate'] . "', '" . $cpd['NoOfSection'] . "', '" . $cpd['EventNameEn'] . "', '" . $cpd['EventNameChi'] . "', '" . $cpd['EventTypeEn'] . "', '" . $cpd['EventTypeChi'] . "','" . $cpd['Content'] . "','" . $cpd['Organization'] . "','" . $cpd['CPDMode'] . "','" . $cpd['CPDDomain'] . "','" . $cpd['BasicLawRelated'] . "', '" . $cpd['FTDataRowID'] . "',NOW(),'$userID',NOW(),'$userID','" . $cpd['SubjectRelated'] . "')
							";
                    $connection->db_db_query($sql);
                    $newCPDList[] = $connection->db_insert_id();
                } else {
                    // update sequence for existing records
                    if (($idx + 1) != $existingRecord['SeqID']) {
                        $sql = "UPDATE INTRANET_PA_S_T_PRE_CPD SET SeqID='" . ($idx + 1) . "' WHERE CpdID='" . $existingRecord['CpdID'] . "'";
                        $connection->db_db_query($sql);
                    }
                }
            }
        }

        // log cpd sync
        $logData = array();
        $recordDetail = array();
        $logData['RlsNo'] = $rlsNo;
        $logData['RecordID'] = $recordID;
        $logData['BatchID'] = $batchID;
        $logData['Function'] = 'Data Sync';
        $logData['RecordType'] = 'CPD Import';
        if (empty($newCPDList)) {
            $recordDetail[] = array(
                "DisplayName" => 'New CPD Records',
                "Message" => $Lang['Appraisal']['Message']['NoNewCPDRecord']
            );
        } else {
            $recordDetail[] = array(
                "Parameter" => "CpdID",
                "DisplayName" => 'New CPD Records',
                "Original" => $newCPDList,
                "MapWithTable" => 'INTRANET_PA_S_T_PRE_CPD',
                "MapDisplay" => "EventNameEng"
            );
        }
        $logData['RecordDetail'] = $json->encode($recordDetail);
        $result = $indexVar['libappraisal']->addFormFillingLog($logData);

        // 3rd: if using Lam Tin 3 years approach
        if ($sys_custom['eAppraisal']['3YearsCPD'] == true) {
            $pastYearCPDHours = array();
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
            $cpdSettingList = unserialize(current($connection->returnVector($sql)));
            $cpdSettingList['Period'] = ($cpdSettingList['Period']) ? $cpdSettingList['Period'] : 1;
            $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear']) ? $cpdSettingList['SelectedYear'] : $_SESSION['CurrentSchoolYearID'];
            $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID='" . $cpdSettingList["SelectedYear"] . "'";
            $startYearSeq = current($connection->returnVector($sql));
            $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID='" . $academicYearIDs[0]['AcademicYearID'] . "'";
            $currSeq = current($connection->returnVector($sql));
            $yearNo = (abs(($startYearSeq - $currSeq)) % $cpdSettingList['Period']);
            for ($y = $yearNo; $y >= 0; $y --) {
                $sql = "SELECT AcademicYearID, YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE Sequence='" . ($currSeq + $y) . "'";
                $pastYearData = $connection->returnArray($sql);
                $sql = "SELECT SUM(NoOfSection) as totalCPDHours FROM INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD WHERE UserID='" . $userID . "' AND AcademicYear='" . $pastYearData[0]['YearNameEN'] . "'";
                $totalCPDHours = current($connection->returnVector($sql));
                $pastYearCPDHours[$pastYearData[0]['AcademicYearID']] = $totalCPDHours;
                $sql = "INSERT INTO INTRANET_PA_T_PRE_CPD_HOURS VALUES ('" . IntegerSafe($rlsNo) . "', '" . IntegerSafe($recordID) . "', '" . IntegerSafe($batchID) . "', '" . IntegerSafe($userID) . "', '" . IntegerSafe($pastYearData[0]['AcademicYearID']) . "', '" . $totalCPDHours . "', '" . IntegerSafe($_SESSION['UserID']) . "',NOW(),'" . IntegerSafe($_SESSION['UserID']) . "',NOW())
							 ON DUPLICATE KEY UPDATE Hours='" . $totalCPDHours . "', ModifiedBy_UserID='" . IntegerSafe($_SESSION['UserID']) . "', DateTimeModified=NOW()";
                $connection->db_db_query($sql);
            }
        }
    }

    $sql = "SELECT RecordID,BatchID,SeqID," . $indexVar['libappraisal']->getLangSQL("EventNameEng", "EventNameChi") . " as CourseTitle," . $indexVar['libappraisal']->getLangSQL("EventTypeEng", "EventTypeChi") . " as Nature,
			Content,Organization,NoOfSection,CPDMode,CPDDomain,BasicLawRelated,StartDate,EndDate,SubjectRelated
			FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "' AND AcademicYearID='" . IntegerSafe($academicYear[0]['AcademicYearID']) . "' AND UserID='" . $userID . "' ORDER BY StartDate ASC";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $runningIndex = 0;

    $CPDHours = array(
        0,
        0,
        0
    );
    for ($i = 0; $i < sizeof($a); $i ++) {
        $month = date('n', strtotime($a[$i]['StartDate']));
        if ($cpdHoursCountingSeparateMonth >= 9) {
            if ($month >= 9 && $month <= $cpdHoursCountingSeparateMonth) {
                $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
            } else {
                $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
            }
        } else {
            if ($month >= $nextCpdHoursCountingSeparateMonth && $month <= 8) {
                $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
            } else {
                $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
            }
        }
        $CPDHours[2] = $CPDHours[2] + $a[$i]['NoOfSection'];
        $domain = explode(",", $a[$i]['CPDDomain']);
        foreach ($domain as $idx => $dm) {
            $a[$i]['CPDDomain' . $idx] = $dm;
        }
        $subjectRelated = explode(",", $a[$i]['SubjectRelated']);
        foreach ($subjectRelated as $idx => $dm) {
            $a[$i]['SubjectRelated' . $idx] = $dm;
        }
    }
    $x15 .= "<div id=\"cpdHourLayer\">";
    $x15 .= "<table id=\"cpdHourTable\" class=\"common_table_list\">";
    $x15 .= "<thead>";
    $x15 .= "<tr>";
    if ($cpdShowingTotalHour == true) {
        $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDHourCategory'] . "</th>";
        $x15 .= "<th style=\"width:40%\">" . str_replace(array(
            '{{START_MONTH}}',
            '{{END_MONTH}}'
        ), array(
            $Lang['Appraisal']['Month'][9],
            $Lang['Appraisal']['Month'][$cpdHoursCountingSeparateMonth]
        ), $Lang['Appraisal']['ARFormCPDHourUndefined']) . "</th>";
        $x15 .= "<th style=\"width:40%\">" . str_replace(array(
            '{{START_MONTH}}',
            '{{END_MONTH}}'
        ), array(
            $Lang['Appraisal']['Month'][$nextCpdHoursCountingSeparateMonth],
            $Lang['Appraisal']['Month'][8]
        ), $Lang['Appraisal']['ARFormCPDHourUndefined']) . "</th>";
        $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDHourTotal'] . "</th>";
    } else {
        $x15 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormCPDHourCategory'] . "</th>";
        $x15 .= "<th style=\"width:40%\">" . str_replace(array(
            '{{START_MONTH}}',
            '{{END_MONTH}}'
        ), array(
            $Lang['Appraisal']['Month'][9],
            $Lang['Appraisal']['Month'][$cpdHoursCountingSeparateMonth]
        ), $Lang['Appraisal']['ARFormCPDHourUndefined']) . "</th>";
        $x15 .= "<th style=\"width:40%\">" . str_replace(array(
            '{{START_MONTH}}',
            '{{END_MONTH}}'
        ), array(
            $Lang['Appraisal']['Month'][$nextCpdHoursCountingSeparateMonth],
            $Lang['Appraisal']['Month'][8]
        ), $Lang['Appraisal']['ARFormCPDHourUndefined']) . "</th>";
    }
    $x15 .= "</tr></thead>";
    $x15 .= "<tbody>";
    $x15 .= "<tr height=\"34px\"><td>" . $Lang['Appraisal']['ARFormCPDHourCategoryHour'] . "</td><td><span id=\"cpdHourA\">" . $CPDHours[0] . "</span>" . $Lang['Appraisal']['ARFormCPDHourHour'] . "</td><td><span id=\"cpdHourB\">" . $CPDHours[1] . "</span>" . $Lang['Appraisal']['ARFormCPDHourHour'] . "</td><td style=\"" . (($cpdShowingTotalHour == true) ? "" : "display:none") . "\"><span id=\"cpdHourTotal\">" . $CPDHours[2] . "</span>" . $Lang['Appraisal']['ARFormCPDHourHour'] . "</td></tr>";
    $x15 .= "</tbody>";
    $x15 .= "</table>";
    $x15 .= "</div><br/>";
    if ($sys_custom['eAppraisal']['3YearsCPD'] == true) {
        $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
        $cpdSettingList = unserialize(current($connection->returnVector($sql)));
        $displayHTML = "";
        $cpdSettingList['Period'] = ($cpdSettingList['Period']) ? $cpdSettingList['Period'] : 1;
        $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear']) ? $cpdSettingList['SelectedYear'] : $_SESSION['CurrentSchoolYearID'];
        $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID='" . $cpdSettingList["SelectedYear"] . "'";
        $startYearSeq = current($connection->returnVector($sql));
        $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID='" . $academicYearID . "'";
        $currSeq = current($connection->returnVector($sql));
        $yearNo = (($startYearSeq - $currSeq) % $cpdSettingList['Period']);
        $x15 .= "<div id=\"CPDPastYears\">";
        $x15 .= $Lang['Appraisal']['ARFormCPDPastYearHead'] . "<br>";
        $x15 .= $Lang['Appraisal']['ARFormCPDPastYearRemark'] . "<br>";
        $x15 .= "<div id=\"CPDPastYearsHoursList\">";
        $sql = "SELECT AcademicYearID, Hours FROM INTRANET_PA_T_PRE_CPD_HOURS WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "' AND UserID='" . $userID . "'";
        $yearHourData = $connection->returnResultSet($sql);
        $yearCPDHour = array();
        foreach ($yearHourData as $yhd) {
            $yearCPDHour[$yhd['AcademicYearID']] = $yhd['Hours'];
        }
        if ($yearNo < 0) {
            for ($y = $yearNo; $y <= 0; $y ++) {
                $sql = "SELECT AcademicYearID, " . $indexVar['libappraisal']->getLangSQL("YearNameB5", "YearNameEN") . " as YearName FROM ACADEMIC_YEAR WHERE Sequence='" . ($currSeq + $y) . "'";
                $pastYearData = current($connection->returnArray($sql));
                if (! isset($yearCPDHour[$pastYearData['AcademicYearID']])) {
                    $yearCPDHour[$pastYearData['AcademicYearID']] = 0;
                }
                $x15 .= $pastYearData['YearName'] . " " . $Lang['Appraisal']['ARFormCPDHourSchoolYear'] . "( <span id='yearHour_" . $pastYearData['AcademicYearID'] . "_span'>" . $yearCPDHour[$pastYearData['AcademicYearID']] . "</span>" . $Lang['Appraisal']['ARFormCPDHourHour'] . ")   ";
            }
        } else {
            for ($y = $yearNo; $y >= 0; $y --) {
                $sql = "SELECT AcademicYearID, " . $indexVar['libappraisal']->getLangSQL("YearNameB5", "YearNameEN") . " as YearName FROM ACADEMIC_YEAR WHERE Sequence='" . ($currSeq + $y) . "'";
                $pastYearData = current($connection->returnArray($sql));
                if (! isset($yearCPDHour[$pastYearData['AcademicYearID']])) {
                    $yearCPDHour[$pastYearData['AcademicYearID']] = 0;
                }
                $x15 .= $pastYearData['YearName'] . " " . $Lang['Appraisal']['ARFormCPDHourSchoolYear'] . "( <span id='yearHour_" . $pastYearData['AcademicYearID'] . "_span'>" . $yearCPDHour[$pastYearData['AcademicYearID']] . "</span>" . $Lang['Appraisal']['ARFormCPDHourHour'] . ")   ";
            }
        }
        $x15 .= "</div>";
        $x15 .= "</div><br/>";
    }
    $x15 .= "<div id=\"CPDLayer\">";
    $x15 .= "<table id=\"CPDTable\" class=\"common_table_list\">";
    $x15 .= "<thead>";
    $x15 .= "<tr>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormCourseTitle'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormCourseNature'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormCourseContent'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormOrgnBody'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormStartDate'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormEndDate'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormCPDMode'] . "</th>";
    $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormCPDDomain'] . "</th>";
    if ($sys_custom['TeacherPortfolio']["cpdSubjectTag"]) {
        $x15 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['ARFormCPDFormSubjectRelated'] . "</th>";
    }
    $x15 .= "<th style=\"width:8%\">" . $Lang['Appraisal']['ARFormCPDFormCPDHour'] . "</th>";
    // $x15 .= "<th style=\"width:8%\">".$Lang['Appraisal']['ARFormCPDFormBasicLawHour']."</th>";
    // $x15 .= "<th style=\"width:4%\"></th>";
    $x15 .= "</tr></thead>";
    $x15 .= "<tbody>";
    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x15 .= "<tr id=\"CPDRow_" . $j . "\" class=\"CPDRow\">";
        $x15 .= "<td>";
        $x15 .= $a[$i]["CourseTitle"];
        $x15 .= "</td>";
        $x15 .= "<td>";
        $x15 .= $a[$i]["Nature"];
        $x15 .= "</td>";
        $x15 .= "<td>";
        $x15 .= $a[$i]["Content"];
        $x15 .= "</td>";
        $x15 .= "<td>";
        $x15 .= $a[$i]["Organization"];
        $x15 .= "</td>";
        $x15 .= "<td>";
        $x15 .= $a[$i]["StartDate"];
        $x15 .= "</td>";
        $x15 .= "<td>";
        $x15 .= $a[$i]["EndDate"];
        $x15 .= "</td>";
        $x15 .= "<td>";
        $x15 .= $Lang['Appraisal']['ARFormCPDMode'][$a[$i]['CPDMode']];
        $x15 .= "</td>";
        $x15 .= "<td>";
        for ($idx = 0; $idx < 6; $idx ++) {
            $x15 .= $indexVar['libappraisal_ui']->Get_Checkbox("CPDDomain_" . $j . "_" . $idx, "CPDDomain_" . $j . "_" . $idx, 1, $isChecked = $a[$i]['CPDDomain' . $idx], $Class = '', $Display = $Lang['Appraisal']['ARFormCPDDomain'][$idx], $Onclick = '', $Disabled = 'true') . "<br>";
        }
        $x15 .= "</td>";
        if ($sys_custom['TeacherPortfolio']["cpdSubjectTag"]) {
            $x15 .= "<td>";
            for ($idx = 0; $idx < count($Lang['Appraisal']['ARFormCPDSubject']); $idx ++) {
                $x15 .= $indexVar['libappraisal_ui']->Get_Checkbox("SubjectRelated_" . $j . "_" . $idx, "SubjectRelated_" . $j . "_" . $idx, 1, $isChecked = $a[$i]['SubjectRelated' . $idx], $Class = '', $Display = $Lang['Appraisal']['ARFormCPDSubject'][$idx], $Onclick = '', $Disabled = 'true') . "<br>";
            }
            $x15 .= "</td>";
        }
        $x15 .= "<td>";
        $x15 .= $a[$i]['NoOfSection'];
        // $x15 .= "</td>";
        // $x15 .= "<td>";
        // $x15 .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDBasicLaw_'.$j, 'CPDBasicLaw_'.$j, $a[$i]['BasicLawRelated']);
        // $x15 .= "</td>";
        // $x15 .= "<td>";
        $x15 .= "<input type=\"hidden\" id=\"hiddenCPDSts_" . $j . "\" name=\"hiddenCPDSts_" . $j . "\" VALUE=\"ACTIVE\">";
        $x15 .= "<input type=\"hidden\" id=\"hiddenCPDSeqID_" . $j . "\" class=\"hiddenCPDSeqID\" name=\"hiddenCPDSeqID_" . $j . "\" VALUE=\"" . $a[$i]["SeqID"] . "\">";
        $x15 .= "<input type=\"hidden\" id=\"hiddenUserID_" . $j . "\" class=\"hiddenUserID\" name=\"hiddenUserID_" . $j . "\" VALUE=\"" . $userID . "\">";
        $x15 .= "<input type=\"hidden\" id=\"hiddenAcademicYearID_" . $j . "\" class=\"hiddenAcademicYearID\" name=\"hiddenAcademicYearID_" . $j . "\" VALUE=\"" . $academicYear[0]['AcademicYearID'] . "\">";
        $x15 .= "</td>";
        $x15 .= "</tr>";
        $runningIndex ++;
    }
    // if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
    // $x15 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>";
    // }else{
    // $x15 .= "<tr height=\"34px\"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td>";
    // }
    // $x15 .= "<div class=\"table_row_tool row_content_tool\">";
    // $x15 .= "<a href=\"javascript:void(0);\" class=\"add_dim thickbox\" title=\"".$Lang['Appraisal']['ARFormAdd']."\" onclick=\"addCPD('".$recordID."','".$batchID."'); return false;\" id=\"\"></a></div>";
    // $x15 .= "<input type=\"hidden\" id=\"currentCPDRow\" name=\"currentCPDRow\" VALUE=\"".sizeof($a)."\">";
    // $x15 .= "<input type=\"hidden\" id=\"totalCPDRow\" name=\"totalCPDRow\" VALUE=\"".$runningIndex."\">";
    // $x15 .= "</td></tr>";
    $x15 .= "</tbody>";
    $x15 .= "</table>";
    if ($Lang['Appraisal']['ARFormCPDRmk'] != "") {
        $x15 .= "<p>";
        $x15 .= $Lang['Appraisal']['ARFormCPDRmk'];
        $x15 .= "</p>";
    }
    $x15 .= "</div><br/>";
}
// ======================================================================== Attendnace Information ========================================================================//
if ($preDefHeader[16]["Incl"] == 1) {
    if ($sys_custom['eAppraisal']['AttendanceAutoSync'] == true && ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm'] || $activeForm == true)) {
        include_once ($PATH_WRT_ROOT . "includes/libstaffattend3.php");
        $StaffAttend3 = new libstaffattend3();
        $staffs = $StaffAttend3->Get_Staff_Info($userID);
        $Params = array();
        $Params["StatusAbsent"] = 1;
        $Params["StatusLate"] = 1;
        $Params["StatusEarlyLeave"] = 1;
        $Params["StatusOuting"] = 1;
        $Params["StatusHoliday"] = 1;
        $Params["HaveRecord"] = 1;
        $Params["SelectStaff"] = Get_Array_By_Key($staffs, 'UserID');

        $sql = "SELECT AcademicYearID FROM INTRANET_PA_T_FRMSUM fs INNER JOIN INTRANET_PA_C_CYCLE cc ON fs.CycleID=cc.CycleID WHERE fs.RecordID='$recordID'";
        $academicYearID = current($connection->returnVector($sql));
        $sql = "SELECT MIN(TermStart) as YearStart, MAX(TermEnd) as YearEnd FROM ACADEMIC_YEAR_TERM ayt INNER JOIN ACADEMIC_YEAR ay ON ayt.AcademicYearID = ay.AcademicYearID WHERE ay.AcademicYearID='" . $academicYearID . "'";
        $academicYear = current($connection->returnArray($sql));

        $dataTable = array();
        $sql = "Select ReasonID,TAReasonID,Days,TypeID,MapID From INTRANET_PA_S_ATTEDNACE_REASON_MAPPING";
        $mappingTable = $connection->returnArray($sql);
        $mappingArray = array();
        foreach ($mappingTable as $mt) {
            $mappingArray[$mt['TypeID'] . '_' . $mt['ReasonID']] = array(
                "ReasonID" => $mt['ReasonID'],
                "TAReasonID" => $mt['TAReasonID'],
                "Days" => $mt['Days'],
                "TypeID" => $mt['TypeID']
            );
        }
        $sql = "SELECT TAReasonID,CountTotalBeforeThisReason FROM INTRANET_PA_S_ATTEDNACE_REASON";
        $reasonTable = $connection->returnArray($sql);
        $sql = "SELECT DATE_FORMAT(PeriodStart, '%Y-%m-%d') as PeriodStart, DATE_FORMAT(PeriodEnd, '%Y-%m-%d') as PeriodEnd, TAColumnID FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
        $columnTable = $connection->returnArray($sql);
        foreach ($reasonTable as $reason) {
            $dataTable[$reason['TAReasonID']] = array();
            foreach ($columnTable as $col) {
                $dataTable[$reason['TAReasonID']][$col['TAColumnID']] = 0;
                $Params["TargetStartDate"] = $col['PeriodStart'];
                $Params["TargetEndDate"] = $col['PeriodEnd'];

                $StartDate = explode('-', $Params["TargetStartDate"]);
                $StartYear = $StartDate[0];
                $StartMonth = $StartDate[1];
                $StartDay = $StartDate[2];
                if ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm']) {
                    if ((strtotime($academicYear['YearStart']) > strtotime($Params["TargetStartDate"])) || (strtotime($academicYear['YearEnd']) < strtotime($Params["TargetStartDate"]))) {
                        if ($StartMonth >= 1 && $StartMonth <= 8) {
                            $StartYear = date('Y', strtotime($academicYear['YearEnd']));
                        } else {
                            $StartYear = date('Y', strtotime($academicYear['YearStart']));
                        }
                        $Params["TargetStartDate"] = $StartYear . "-" . $StartMonth . "-" . $StartDay;
                    }
                }
                $EndDate = explode('-', $Params["TargetEndDate"]);
                $EndYear = $EndDate[0];
                $EndMonth = $EndDate[1];
                $EndDay = $EndDate[2];
                if ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm']) {
                    if ((strtotime($academicYear['YearStart']) > strtotime($Params["TargetEndDate"])) || (strtotime($academicYear['YearEnd']) < strtotime($Params["TargetEndDate"]))) {
                        if ($EndMonth >= 1 && $EndMonth <= 8) {
                            $EndYear = date('Y', strtotime($academicYear['YearEnd']));
                        } else {
                            $EndYear = date('Y', strtotime($academicYear['YearStart']));
                        }
                        $Params["TargetEndDate"] = $EndYear . "-" . $EndMonth . "-" . $EndDay;
                    }
                }
                $YearArr = array(); // start year to end year
                $MonthArr = array(); // months of start year to months of end year [assoc array using numeric year as key]
                for ($y = intval($StartYear); $y <= intval($EndYear); $y ++) {
                    $YearArr[] = $y;
                    $MonthArr[$y] = array();
                }
                for ($i = 0; $i < sizeof($YearArr); $i ++) {
                    if (sizeof($YearArr) == 1) // only 1 year
                    {
                        for ($m = intval($StartMonth); $m <= intval($EndMonth); $m ++) {
                            $MonthArr[$YearArr[$i]][] = ($m < 10) ? '0' . $m : '' . $m;
                        }
                    } else if ($i == 0) // first year
                    {
                        for ($m = intval($StartMonth); $m <= 12; $m ++) {
                            $MonthArr[$YearArr[$i]][] = ($m < 10) ? '0' . $m : '' . $m;
                        }
                    } else if ($i == (sizeof($YearArr) - 1)) // last year
                    {
                        for ($m = 1; $m <= $EndMonth; $m ++) {
                            $MonthArr[$YearArr[$i]][] = ($m < 10) ? '0' . $m : '' . $m;
                        }
                    } else // middle years
                    {
                        for ($m = 1; $m <= 12; $m ++) {
                            $MonthArr[$YearArr[$i]][] = ($m < 10) ? '0' . $m : '' . $m;
                        }
                    }
                }
                $AssocDataList = array();
                for ($i = 0; $i < sizeof($YearArr); $i ++) {
                    $CurYear = $YearArr[$i]; // int type e.g. 2009
                    for ($j = 0; $j < sizeof($MonthArr[$CurYear]); $j ++) {
                        $CurMonth = $MonthArr[$CurYear][$j]; // string type e.g. 01 which is January

                        $tempArr = $StaffAttend3->Get_Report_Customize_Monthly_Detail_Report_Data($Params, $CurYear, $CurMonth, 2);
                        if (sizeof($tempArr) > 0)
                            foreach ($tempArr as $theUser => $Record) {
                                if (sizeof($Record) > 0)
                                    foreach ($Record as $Datekey => $Value) {
                                        $AssocDataList[$theUser][$Datekey] = $Value;
                                    }
                            }
                    }
                }

                $attendanceData = $AssocDataList[$userID];
                $startTimeTS = strtotime($Params["TargetStartDate"]);
                $endTimeTS = strtotime($Params["TargetEndDate"]);
                for ($currTime = $startTimeTS; $currTime <= $endTimeTS; $currTime += (24 * 60 * 60)) {
                    $currTimeYMD = date('Y-m-d', $currTime);
                    if (empty($attendanceData[$currTimeYMD])) {
                        continue;
                    } else {
                        foreach ($attendanceData[$currTimeYMD] as $attData) {
                            $realReasonID = $attData['ReasonID'];
                            if (empty($realReasonID)) {
                                $realReasonID = $attData['ELReasonID'];
                            }
                            $statusNo = $attData['Status'];
                            if ($statusNo != CARD_STATUS_NORMAL) {
                                $isTreated = false;
                                if ($attData['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE) {
                                    if (! is_null($realReasonID)) {
                                        if ($mappingArray[CARD_STATUS_EARLYLEAVE . "_" . $realReasonID]['TAReasonID'] == $reason['TAReasonID']) {
                                            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_EARLYLEAVE . "_" . $realReasonID]['Days'];
                                        }
                                    } else {
                                        if (CARD_STATUS_EARLYLEAVE == $mappingArray[CARD_STATUS_EARLYLEAVE . "_0"]['TypeID'] && $mappingArray[CARD_STATUS_EARLYLEAVE . "_0"]['TAReasonID'] == $reason['TAReasonID']) {
                                            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_EARLYLEAVE . "_0"]['Days'];
                                        }
                                    }
                                    $isTreated = true;
                                }
                                if ($attData['InSchoolStatus'] == CARD_STATUS_LATE) {
                                    if (! is_null($realReasonID) && $realReasonID != $attData['ELReasonID']) {
                                        if ($mappingArray[CARD_STATUS_LATE . "_" . $realReasonID]['TAReasonID'] == $reason['TAReasonID']) {
                                            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_LATE . "_" . $realReasonID]['Days'];
                                        }
                                    } else {
                                        if (CARD_STATUS_LATE == $mappingArray[CARD_STATUS_LATE . "_0"]['TypeID'] && $mappingArray[CARD_STATUS_LATE . "_0"]['TAReasonID'] == $reason['TAReasonID']) {
                                            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_LATE . "_0"]['Days'];
                                        }
                                    }
                                    $isTreated = true;
                                }
                                if (! $isTreated) {
                                    if (! is_null($realReasonID)) {
                                        if ($mappingArray[$statusNo . "_" . $realReasonID]['TAReasonID'] == $reason['TAReasonID']) {
                                            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[$statusNo . "_" . $realReasonID]['Days'];
                                        }
                                    } else {
                                        if ($statusNo == $mappingArray[$statusNo . "_0"]['TypeID'] && $mappingArray[$statusNo . "_0"]['TAReasonID'] == $reason['TAReasonID']) {
                                            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[$statusNo . "_0"]['Days'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if ($reason['CountTotalBeforeThisReason'] == 1) {
                $totalData = array();
                foreach ($dataTable as $reasonID => $reasonData) {
                    foreach ($reasonData as $colID => $colData) {
                        if (isset($totalData[$colID])) {
                            $totalData[$colID] += $dataTable[$reasonID][$colID];
                        } else {
                            $totalData[$colID] = $dataTable[$reasonID][$colID];
                        }
                    }
                }
                $dataTable['total'] = $totalData;
            }
        }

        foreach ($dataTable as $reasonID => $reasonData) {
            if ($reasonID == "total")
                continue;
            foreach ($reasonData as $colID => $colData) {
                $sql = "SELECT AttID FROM INTRANET_PA_S_ATTEDNACE_DATA WHERE RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='" . $userID . "' AND TAReasonID='" . $reasonID . "' AND TAColumnID='" . $colID . "'";
                $attID = $connection->returnVector($sql);
                if (empty($attID)) {
                    $sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_DATA (RlsNo, RecordID, BatchID, UserID, TAReasonID, TAColumnID, Days, DateInput, InputBy, DateModified, ModifyBy)
							VALUES ('$rlsNo', '$recordID', '$batchID', '" . $userID . "', '" . $reasonID . "', '" . $colID . "', '" . $colData . "', NOW(), '" . $_SESSION['UserID'] . "', NOW(), '" . $_SESSION['UserID'] . "')";
                } else {
                    $sql = "UPDATE INTRANET_PA_S_ATTEDNACE_DATA SET Days='" . $colData . "', DateModified=NOW(), ModifyBy='" . $_SESSION['UserID'] . "' WHERE RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='" . $userID . "' AND TAReasonID='" . $reasonID . "' AND TAColumnID='" . $colID . "'";
                }
                // echo $sql;
                $connection->db_db_query($sql);
            }
        }
        // log attendance sync
        $logData = array();
        $recordDetail = array();
        $logData['RlsNo'] = $rlsNo;
        $logData['RecordID'] = $recordID;
        $logData['BatchID'] = $batchID;
        $logData['Function'] = 'Data Sync';
        $logData['RecordType'] = 'Attedance Import';
        $recordDetail[] = array(
            "Parameter" => "UserID",
            "DisplayName" => $Lang['Appraisal']['CycleTemplate']['StfIdvName'],
            "New" => $userID,
            "MapWithTable" => $appraisalConfig['INTRANET_USER'],
            "MapDisplay" => "EnglishName"
        );
        $logData['RecordDetail'] = $json->encode($recordDetail);
        $result = $indexVar['libappraisal']->addFormFillingLog($logData);
    }

    $sql = "SELECT data.TAReasonID, data.TAColumnID, data.Days, data.Remarks FROM  INTRANET_PA_S_ATTEDNACE_DATA data
			INNER JOIN INTRANET_PA_S_ATTEDNACE_REASON reason ON data.TAReasonID=reason.TAReasonID
			INNER JOIN INTRANET_PA_S_ATTEDNACE_COLUMN col ON data.TAColumnID=col.TAColumnID
			WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "' AND UserID='" . $userID . "'
			ORDER BY data.TAReasonID ASC, col.DisplayOrder ASC";
    $dataRaw = $connection->returnArray($sql);
    $dataTable = array();
    $reasonDataTable = array();
    $hasReasonDataContent = false;
    foreach ($dataRaw as $d) {
        $dataTable[$d['TAReasonID']][$d['TAColumnID']] = $d['Days'];
        if (! isset($reasonDataTable[$d['TAReasonID']])) {
            $reasonDataTable[$d['TAReasonID']] = $d['Remarks'];
            if ($d['Remarks'] != "") {
                $hasReasonDataContent = true;
            }
        }
    }
    if (! $hasReasonDataContent) {
        $reasonDataTable = array();
    }
    $sql = "SELECT " . $indexVar['libappraisal']->getLangSQL("TAReasonNameChi", "TAReasonNameEng") . " as ReasonTitle, ReasonUnit, CountTotalBeforeThisReason, TAReasonID FROM INTRANET_PA_S_ATTEDNACE_REASON";
    $reasonTable = $connection->returnArray($sql);
    $sql = "SELECT " . $indexVar['libappraisal']->getLangSQL("TAColumnNameChi", "TAColumnNameEng") . " as ColumnTitle, PeriodStart, PeriodEnd, TAColumnID FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
    $columnTable = $connection->returnArray($sql);
    $generalSetting = $connection->Get_General_Setting("TeacherApprisal");
    if (empty($generalSetting['PreDefinedFormAttendanceTableOrientation'])) {
        $orientation = 0;
    } else {
        $settingValueArr = unserialize($generalSetting['PreDefinedFormAttendanceTableOrientation']);
        $orientation = isset($settingValueArr[$templateID]) ? $settingValueArr[$templateID] : 0;
    }
    $totalData = array();
    if (empty($columnTable) || empty($reasonTable)) {
        $x16 .= "";
    } else {
        $x16 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[16]["Descr"]) . "<br/>\r\n";
        $x16 .= "<style>";
        $x16 .= ".common_table_list .tableTotal td{border-top: 2px solid ;border-bottom: 2px solid ;}";
        $x16 .= "</style>";
        $x16 .= "<div id=\"AttendanceRecordInfoLayer\">";
        $x16 .= "<table id=\"AttendanceRecordInfoTable\" class=\"common_table_list\">";
        if ($orientation == 0) {
            $x16 .= "<thead>";
            $x16 .= "<tr>";
            $x16 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormLeaCat'] . "</th>";
            $colWidth = round(50 / count($columnTable), 0);
            foreach ($columnTable as $col) {
                $x16 .= "<th style=\"width:$colWidth%\">" . $col['ColumnTitle'] . "</th>";
            }
            if (! empty($reasonDataTable)) {
                $x16 .= "<th style=\"width:40%\">" . $Lang['Appraisal']['ARFormOptionalRemarks'] . "</th>";
            }
            $x16 .= "</tr></thead>";
            $x16 .= "<tbody>";
            foreach ($reasonTable as $reason) {
                $x16 .= "<tr>";
                $x16 .= "	<td>" . $reason['ReasonTitle'] . "</td>";
                foreach ($columnTable as $col) {
                    $x16 .= "	<td  id='attendanceField_" . $reason['TAReasonID'] . "_" . $col['TAColumnID'] . "' style='text-align: right'><span>" . $dataTable[$reason['TAReasonID']][$col['TAColumnID']] . "</span>" . $Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']] . "</td>";
                    if (isset($totalData[$col['TAColumnID']])) {
                        $totalData[$col['TAColumnID']] += $dataTable[$reason['TAReasonID']][$col['TAColumnID']];
                    } else {
                        $totalData[$col['TAColumnID']] = $dataTable[$reason['TAReasonID']][$col['TAColumnID']];
                    }
                }
                if (! empty($reasonDataTable)) {
                    $x16 .= "	<td style='text-align: right'>" . $reasonDataTable[$reason['TAReasonID']] . "</td>";
                }
                $x16 .= "</tr>";
                if ($reason['CountTotalBeforeThisReason']) {
                    $x16 .= "<tr class='tableTotal'>";
                    $x16 .= "	<td>" . $Lang['Appraisal']['ARFormAttendanceLeaveTotal'] . "</td>";
                    foreach ($columnTable as $col) {
                        $x16 .= "	<td id='attendanceField_total_" . $col['TAColumnID'] . "' style='text-align: right'><span>" . $totalData[$col['TAColumnID']] . "</span>" . $Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']] . "</td>";
                    }
                    if (! empty($reasonDataTable)) {
                        $x16 .= "	<td></td>";
                    }
                    $x16 .= "</tr>";
                }
            }
            $x16 .= "</tbody>";
        } elseif ($orientation == 1) {
            $x16 .= "<thead>";
            $x16 .= "<tr>";
            $x16 .= "<th></th>";
            foreach ($reasonTable as $reason) {
                $x16 .= "<th width='" . round(100 / (count($reasonTable) + 1), 0) . "%'>" . $reason['ReasonTitle'] . "</th>";
            }
            $x16 .= "</tr></thead>";
            $x16 .= "<tbody>";
            foreach ($columnTable as $col) {
                $x16 .= "<tr>";
                $x16 .= "<td class=\"field_title\">" . $col['ColumnTitle'] . "</td>";
                foreach ($reasonTable as $reason) {
                    $x16 .= "	<td id='attendanceField_" . $reason['TAReasonID'] . "_" . $col['TAColumnID'] . "' style='text-align: right'><span>" . $dataTable[$reason['TAReasonID']][$col['TAColumnID']] . "</span>" . $Lang['Appraisal']['ARFormAttendanceUnitList'][$reason['ReasonUnit']] . "</td>";
                }
                $x16 .= "</tr>";
            }
            if (! empty($reasonDataTable)) {
                $x16 .= "<tr>";
                $x16 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormOptionalRemarks'] . "</td>";
                foreach ($reasonTable as $reason) {
                    $x16 .= "	<td style='text-align: right'>" . $reasonDataTable[$reason['TAReasonID']] . "</td>";
                }
                $x16 .= "</tr>";
            }
            $x16 .= "</tbody>";
        }
        $x16 .= "</table></div><br/>";
    }
}
// ======================================================================== Teaching Experience ========================================================================//
if ($preDefHeader[17]["Incl"] == 1) {
    $x17 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[17]["Descr"]) . "<br/>\r\n";

    $sql = "SELECT RecordID,BatchID,SeqID,SchoolName,StartDate,EndDate,Post
			FROM INTRANET_PA_T_PRE_TEACHEREXP WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "';";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $runningIndex = 0;

    $x17 .= "<div id=\"studentTrainingLayer\">";
    $x17 .= "<div id=\"teachingExpLayer\">";
    $x17 .= "<table id=\"teachingExpTable\" class=\"common_table_list\">";
    $x17 .= "<thead>";
    $x17 .= "<tr>";
    $x17 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormExpSchoolName'] . "</th>";
    $x17 .= "<th style=\"width:40%\">" . $Lang['Appraisal']['ARFormExpDate'] . "</th>";
    $x17 .= "<th style=\"width:25%\">" . $Lang['Appraisal']['ARFormExpPost'] . "</th>";
    $x17 .= "</tr></thead>";
    $x17 .= "<tbody>";

    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x17 .= "<tr id=\"teachingExpRow_" . $j . "\">";
        $x17 .= "<td>";
        $x17 .= $a[$i]["SchoolName"];
        $x17 .= "</td>";
        $x17 .= "<td>";
        $x17 .= $Lang['General']['From'] . date('Y-m-d', strtotime($a[$i]["StartDate"])) . " " . $Lang['General']['To'] . " " . date('Y-m-d', strtotime($a[$i]["EndDate"]));
        $x17 .= "</td>";
        $x17 .= "<td>";
        $x17 .= $a[$i]["Post"];
        $x17 .= "</td>";
        $x17 .= "</tr>";
        $runningIndex ++;
    }

    $x17 .= "</tbody>";
    $x17 .= "</table></div><br/>";
}
// ======================================================================== Teaching Experience ========================================================================//
if ($preDefHeader[18]["Incl"] == 1) {
    $sql = "SELECT RecordID,BatchID,SeqID,DATE_FORMAT(StartDate,'%Y-%m-%d') as StartDate,DATE_FORMAT(EndDate,'%Y-%m-%d') as EndDate,Issuer,QualiDescr,Programme
	FROM INTRANET_PA_T_PRE_TRAINING  WHERE RlsNo='" . IntegerSafe($rlsNo) . "' AND RecordID='" . IntegerSafe($recordID) . "' AND BatchID='" . IntegerSafe($batchID) . "';";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x18 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[18]["Descr"]) . "<br/>\r\n";
    $runningIndex = 0;

    $x18 .= "<div id=\"trainingInfoLayer\">";
    $x18 .= "<table id=\"trainingInfoTable\" class=\"common_table_list\">";
    $x18 .= "<thead><tr>";
    $x18 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormTrainingUni'] . "</th>";
    $x18 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormTrainingDate'] . "</th>";
    $x18 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['ARFormTrainingAward'] . "</th>";
    $x18 .= "<th style=\"width:30%\">" . $Lang['Appraisal']['ARFormTrainingSbj'] . "</th>";
    $x18 .= "</tr></thead>";
    $x18 .= "<tbody>";
    $runningIndex = 0;

    for ($i = 0; $i < sizeof($a); $i ++) {
        $j = $i + 1;
        $x18 .= "<tr id=\"trainingRow_" . $j . "\">";
        $x18 .= "<td>";
        $x18 .= $a[$i]["Issuer"];
        $x18 .= "</td>";
        $x18 .= "<td>";
        $x18 .= $Lang['General']['From'] . date('Y-m-d', strtotime($a[$i]["StartDate"])) . " " . $Lang['General']['To'] . " " . date('Y-m-d', strtotime($a[$i]["EndDate"]));
        $x18 .= "</td>";
        $x18 .= "<td>";
        $x18 .= $a[$i]["QualiDescr"];
        $x18 .= "</td>";
        $x18 .= "<td>";
        $x18 .= $a[$i]["Programme"];
        $x18 .= "</td>";
        $x18 .= "</tr>";
        $runningIndex ++;
    }

    $x18 .= "</tbody>";
    $x18 .= "</table>";
    $x18 .= "</div><br/>";
}
// ======================================================================== Sick Leave & lateness records ========================================================================//
if ($preDefHeader[19]["Incl"] == 1) {
    $sql = "SELECT SLLastYear,SLThisYear,LateLastYear,LateThisYear,IronMan,EarlyBird  FROM INTRANET_PA_C_ATTENDANCE WHERE CycleID='" . $cycleID . "' AND UserID='" . $userID . "'";
    // echo $sql;
    $a = $connection->returnResultSet($sql);
    $x19 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($preDefHeader[19]["Descr"]) . "<br/>\r\n";

    $x19 .= "<div id=\"trainingInfoLayer\">";
    $x19 .= "<table id=\"trainingInfoTable\" class=\"common_table_list\">";
    $x19 .= "<thead><tr>";
    $x19 .= "<th style=\"width:20%;text-align: center;\" colspan=2>" . $Lang['Appraisal']['SickLeaveRecord'] . "</th>";
    $x19 .= "<th style=\"width:20%;text-align: center;\" colspan=2>" . $Lang['Appraisal']['LatenessRecord'] . "</th>";
    $x19 .= "<th style=\"width:10%;text-align: center;\">" . $Lang['Appraisal']['IronMan'] . "</th>";
    $x19 .= "<th style=\"width:10%;text-align: center;\">" . $Lang['Appraisal']['EarlyBird'] . "</th>";
    $x19 .= "</tr><tr>";
    $x19 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['LastAcademicYear'] . "</th>";
    $x19 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['CurrentAcademicYearTilMar'] . "</th>";
    $x19 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['LastAcademicYear'] . "</th>";
    $x19 .= "<th style=\"width:20%\">" . $Lang['Appraisal']['CurrentAcademicYearTilMar'] . "</th>";
    $x19 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['LastAcademicYear'] . "</th>";
    $x19 .= "<th style=\"width:10%\">" . $Lang['Appraisal']['LastAcademicYear'] . "</th>";
    $x19 .= "</tr></thead>";
    $x19 .= "<tbody>";
    $x19 .= "<tr height=\"34px\">";
    $x19 .= "<td>" . $a[0]['SLLastYear'] . "</td>";
    $x19 .= "<td>" . $a[0]['SLThisYear'] . "</td>";
    $x19 .= "<td>" . $a[0]['LateLastYear'] . "</td>";
    $x19 .= "<td>" . $a[0]['LateThisYear'] . "</td>";
    $x19 .= "<td>" . $a[0]['IronMan'] . "</td>";
    $x19 .= "<td>" . $a[0]['EarlyBird'] . "</td>";
    $x19 .= "</tr>";
    $x19 .= "</tbody>";
    $x19 .= "</table>";
    $x19 .= "</div><br/>";
}
$content .= $x;

function headerSortByDisplayOrder($a, $b)
{
    if ($a['DisplayOrder'] == $b['DisplayOrder'])
        return 0;
    return ($a['DisplayOrder'] < $b['DisplayOrder']) ? - 1 : 1;
}
usort($preDefHeader, 'headerSortByDisplayOrder');
foreach ($preDefHeader as $pdh) {
    switch ($pdh["DataTypeID"]) {
        case "1":
            $content .= $x0;
            break;
        case "2":
            $content .= $x1;
            break;
        case "3":
            $content .= $x2;
            break;
        case "4":
            $content .= $x3;
            break;
        case "5":
            $content .= $x4;
            break;
        case "6":
            $content .= $x5;
            break;
        case "7":
            $content .= $x6;
            break;
        case "8":
            $content .= $x7;
            break;
        case "9":
            $content .= $x8;
            break;
        case "10":
            $content .= $x9;
            break;
        case "11":
            $content .= $x10;
            break;
        case "12":
            $content .= $x11;
            break;
        case "13":
            $content .= $x12;
            break;
        case "14":
            $content .= $x13;
            break;
        case "15":
            $content .= $x14;
            break;
        case "16":
            $content .= $x15;
            break;
        case "17":
            $content .= $x16;
            break;
        case "18":
            $content .= $x17;
            break;
        case "19":
            $content .= $x18;
            break;
        case "20":
            $content .= $x19;
            break;
    }
}
// echo $content;
$htmlAry['contentTbl'] = $content;
// ============================== Define Button ==============================
if ($canEdit == "1") {
    $htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnTempSave'], "button", "goSubmit()", 'submitBtn');
}
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');
// ============================== Define Button ==============================
// ======================================================================== Form Modification ========================================================================//
// only the user who has the right of the administrator can access the below content
if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eAppraisal"] == "1" && ($cycleStart <= $currentDate && $currentDate <= $cycleClose)) {
    $htmlAry['modBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnModify'], "button", "goModify()", 'modifyBtn');
    $z0 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormModification']) . "<br/><br/>\r\n";
    $z0 .= "<div id=\"formModification\">";
    $z0 .= "<table class=\"form_table_v30\">";
    $z0 .= "<tbody>";
    $z0 .= "<tr>";
    $z0 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormModDate'] . "</td>";
    $z0 .= "<td>";
    $z0 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER("modDateFr", $modDateFr, $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1, $Disable = false, $cssClass = "textboxnum");
    $z0 .= " - ";
    $z0 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER("modDateTo", $modDateTo, $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1, $Disable = false, $cssClass = "textboxnum");
    $z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateOutCycleWarnDiv', $Lang['Appraisal']['TemplateSample']['DateOutCycle'], $Class = 'warnMsgDiv') . "\r\n";
    $z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateInvalidRangeWarnDiv', $Lang['Appraisal']['TemplateSample']['ModDateInvalidRange'], $Class = 'warnMsgDiv') . "\r\n";
    $z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateOutEditWarnDiv', $Lang['Appraisal']['TemplateSample']['DateOutEdit'], $Class = 'warnMsgDiv') . "\r\n";
    $z0 .= "</td>";
    $z0 .= "</tr>";
    $z0 .= "<tr>";
    $z0 .= "<td class=\"field_title\">" . $Lang['Appraisal']['ARFormRemarks'] . "</td>";
    $z0 .= "<td>";
    $z0 .= $indexVar['libappraisal_ui']->GET_TEXTAREA("modRemark", $modRemark, $taCols = 70, $taRows = 5, $OnFocus = "", $readonly = "", $other = '', $class = '', $taID = '', $CommentMaxLength = '');
    $z0 .= "</td>";
    $z0 .= "</tr>";
    $z0 .= "</div>";
    $z0 .= "</table>";
    $htmlAry['contentTbl3'] = $z0;
} else {}
// ======================================================================== Form Modification ========================================================================//
// ======================================================================== Form Submission ========================================================================//
if ($canEdit == "1") {
    // $z .= $indexVar['libappraisal']->getFormSubmission($Lang,$iu[0]["UserID"],Get_Lang_Selection($iu[0]["ChineseName"],$iu[0]["EnglishName"]),$appRole);
}
$z .= "<input type=\"hidden\" id=\"rlsNo\" name=\"rlsNo\" value='" . IntegerSafe($rlsNo) . "'/>";
$z .= "<input type=\"hidden\" id=\"recordID\" name=\"recordID\" value='" . IntegerSafe($recordID) . "'/>";
$z .= "<input type=\"hidden\" id=\"batchID\" name=\"batchID\" value='" . IntegerSafe($batchID) . "'/>";
$z .= "<input type=\"hidden\" id=\"cycleID\" name=\"cycleID\" value='" . IntegerSafe($cycleID) . "'/>";
$z .= "<input type=\"hidden\" id=\"cycleStart\" name=\"cycleStart\" value='" . $cycleStart . "'/>";
$z .= "<input type=\"hidden\" id=\"cycleClose\" name=\"cycleClose\" value='" . $cycleClose . "'/>";
$z .= "<input type=\"hidden\" id=\"editPrdFr\" name=\"editPrdFr\" value='" . $editPrdFr . "'/>";
$z .= "<input type=\"hidden\" id=\"editPrdTo\" name=\"editPrdTo\" value='" . $editPrdTo . "'/>";
$z .= "<input type=\"hidden\" id=\"userID\" name=\"userID\" value='" . IntegerSafe($userID) . "'/>";
$z .= "<input type=\"hidden\" id=\"templateID\" name=\"templateID\" value='" . IntegerSafe($templateID) . "'/>";
$htmlAry['contentTbl2'] = $z;
// ======================================================================== Form Submission ========================================================================//

?>