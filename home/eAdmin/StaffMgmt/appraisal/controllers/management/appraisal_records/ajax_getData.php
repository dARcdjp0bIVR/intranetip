<?php

// ============================== Transactional data ==============================

$connection = new libgeneralsettings();
$json = new JSON_obj();
$type = $_POST['type'];
foreach($_POST as $key=>$value){
    ${$key} = $value;
}

if($type == "qualiAcqSts"){
    $stsDes=$indexVar['libappraisal']->cnvArrToSelect($Lang['Appraisal']['ARFormQuaAcqStsDes'],null);
    $stsVal=$indexVar['libappraisal']->cnvArrToSelect($Lang['Appraisal']['ARFormQuaAcqStsVal'],null);
    $arr = array();
    $jsonObj = new JSON_obj();
    for ($i=0; $i<sizeof($stsDes); $i++){
        $arr[] = array('Value' => $stsVal[$i], 'Descr' => $stsDes[$i]);
    }
    echo $jsonObj->encode($arr);
}
else if($type == "getSignatureValidation" || $type=="getModSignatureValidation"){
    $jsonObj = new JSON_obj();
    $userID = $_POST["userID"];
    $password = $_POST["signauture"];
    $status = false;
    $status = $indexVar['libappraisal']->signatureValidation($userID,$password);
    if($status == true){
        $arr[] = array('result' => 0, 'msg' => "");
    }
    else{
        $arr[] = array('result' => 1, 'msg' => $Lang['Appraisal']['ARFormInvalidSignature']);
    }
    echo $jsonObj->encode($arr);
}

else if($type == "getSubjectByClass"){
    
}
else if($type == "getSubjectByForm"){
    
}
else if($type == "importCPDFromTeacherPortfolio"){
    $reply = "";
    // import Teacher Portfolio data to Teacher Appraisal
    $sql = "SELECT AcademicYearID FROM INTRANET_PA_T_FRMSUM fs INNER JOIN INTRANET_PA_C_CYCLE cc ON fs.CycleID=cc.CycleID WHERE fs.RecordID=$recordID";
    //	$academicYearIDs = $indexVar['libappraisal']->getAcademicYearTerm(date('Y-m-d'));
    $academicYearIDs = $connection->returnArray($sql);
    $academicYear = $indexVar['libappraisal']->getAllAcademicYear($academicYearIDs[0]['AcademicYearID']);
    $sql = "SELECT FTDataRowID,StartDate,EndDate,NoOfSection,EventNameEn,EventNameChi,EventTypeEn,EventTypeChi,RemarksEn,RemarksChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated FROM INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD WHERE UserID='".$userID."' AND AcademicYear='".$academicYear[0]['YearNameEN']."'";
    $cpdRecords = $connection->returnArray($sql);
    if(empty($cpdRecords)){
        $reply = $Lang['Appraisal']['Message']['NoCPDRecord'];
    }
    $sql = "SELECT SeqID FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='".$userID."' AND AcademicYearID='".$academicYearIDs[0]['AcademicYearID']."' ORDER BY SeqID DESC LIMIT 1";
    $lastSeqID = current($connection->returnVector($sql));
    $newCPDRecordToAdd = array();
    foreach($cpdRecords as $idx=>$cpd){
        $sql = "SELECT CpdID FROM INTRANET_PA_S_T_PRE_CPD WHERE FTDataRowID='".$cpd['FTDataRowID']."' AND RlsNo='$rlsNo' AND RecordID='$recordID' AND BatchID='$batchID' AND UserID='".$userID."'";
        $isExists = current($connection->returnVector($sql));
        if(empty($isExists)){
            $sql = "INSERT INTO INTRANET_PA_S_T_PRE_CPD
						(RlsNo, RecordID, BatchID, SeqID, UserID, AcademicYearID, StartDate, EndDate, NoOfSection, EventNameEng, EventNameChi, EventTypeEng, EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,FTDataRowID,DateInput,InputBy,DateModified,ModifyBy,SubjectRelated)
						VALUES
						($rlsNo, $recordID, $batchID, '".($lastSeqID+$idx+1)."', '".$userID."','".$academicYearIDs[0]['AcademicYearID']."', '".$cpd['StartDate']."', '".$cpd['EndDate']."', '".$cpd['NoOfSection']."', '".$cpd['EventNameEn']."', '".$cpd['EventNameChi']."', '".$cpd['EventTypeEn']."', '".$cpd['EventTypeChi']."','".$cpd['Content']."','".$cpd['Organization']."','".$cpd['CPDMode']."','".$cpd['CPDDomain']."','".$cpd['BasicLawRelated']."', '".$cpd['FTDataRowID']."',NOW(),$userID,NOW(),$userID,'".$cpd['SubjectRelated']."')
					";
            $connection->db_db_query($sql);
            array_push($newCPDRecordToAdd, $connection->db_insert_id());
        }
    }
    if(empty($newCPDRecordToAdd) && $reply==""){
        $reply = $Lang['Appraisal']['Message']['NoNewCPDRecord'];
    }
    
    // log cpd sync
    $logData = array();
    $recordDetail = array();
    $logData['RlsNo'] = $rlsNo;
    $logData['RecordID'] = $recordID;
    $logData['BatchID'] = $batchID;
    $logData['Function'] = 'Data Sync';
    $logData['RecordType'] = 'CPD Import';
    if(empty($newCPDRecordToAdd)){
        $recordDetail[] = array(
            "DisplayName"=>'New CPD Records',
            "Message"=>$reply
        );
    }else{
        $recordDetail[] = array(
            "Parameter"=>"CpdID",
            "DisplayName"=>'New CPD Records',
            "Original"=>$newCPDRecordToAdd,
            "MapWithTable"=>'INTRANET_PA_S_T_PRE_CPD',
            "MapDisplay"=>"EventNameEng"
        );
    }
    $logData['RecordDetail'] = $json->encode($recordDetail);
    $result = $indexVar['libappraisal']->addFormFillingLog($logData);
    
    // rebuild the CPD table in Appraisal Form
    $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("EventNameEng","EventNameChi")." as CourseTitle,"
        .$indexVar['libappraisal']->getLangSQL("EventTypeEng","EventTypeChi")." as Nature,
			Content,Organization,NoOfSection,CPDMode,CPDDomain,BasicLawRelated,StartDate,EndDate,SubjectRelated
			FROM INTRANET_PA_S_T_PRE_CPD WHERE CpdID IN (".implode(",",$newCPDRecordToAdd).") ORDER BY StartDate ASC";
        //echo $sql;
        $a=$connection->returnArray($sql);
        $runningIndex = 0;
        
        $CPDHours = array(0,0);
        for($i=0;$i<sizeof($a);$i++){
            $month = date('n',strtotime($a[$i]['StartDate']));
            if($month >= 5 && $month <= 8){
                $CPDHours[1] = $CPDHours[1] + $a[$i]['NoOfSection'];
            }else{
                $CPDHours[0] = $CPDHours[0] + $a[$i]['NoOfSection'];
            }
            $domain = explode(",",$a[$i]['CPDDomain']);
            foreach($domain as $idx=>$dm){
                $a[$i]['CPDDomain'.$idx] = $dm;
            }
            $subjectRelated = explode(",",$a[$i]['SubjectRelated']);
            foreach($subjectRelated as $idx=>$dm){
                $a[$i]['SubjectRelated'.$idx] = $dm;
            }
        }
        $row = "";
        for($i=0;$i<sizeof($a);$i++){
            $j=$lastRow+$i+1;
            $row .= "<tr id=\"CPDRow_".$j."\" class=\"CPDRow\">";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDTitle_'.$j,'CPDTitle_'.$j,$a[$i]["CourseTitle"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $row .= "</td>";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDNature_'.$j,'CPDNature_'.$j,$a[$i]["Nature"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $row .= "</td>";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDContent_'.$j,'CPDContent_'.$j,$a[$i]["Content"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $row .= "</td>";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_TEXTBOX('CPDOrganization_'.$j,'CPDOrganization_'.$j,$a[$i]["Organization"], $OtherClass='', $OtherPar=array('maxlength'=>255));
            $row .= "</td>";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('CPDStartDate_'.$j,$a[$i]["StartDate"], $OtherMember='',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="calCPDHours()",$ID='CPDStartDate_'.$j);
            $row .= "</td>";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_DATE_PICKER('CPDEndDate_'.$j,$a[$i]["EndDate"], $OtherMember='',$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="calCPDHours()",$ID='CPDEndDate_'.$j);
            $row .= "</td>";
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_SELECTION_BOX($ParData=array(array(0,$Lang['Appraisal']['ARFormCPDMode'][0]),array(1,$Lang['Appraisal']['ARFormCPDMode'][1])), $ParTags="name='CPDMode_$j' id='CPDMode_$j'", $ParDefault="", $ParSelected=$a[$i]['CPDMode'], $CheckType=false);
            $row .= "</td>";
            $row .= "<td>";
            for($idx=0; $idx < 6; $idx++){
                $row .= $indexVar['libappraisal_ui']->Get_Checkbox("CPDDomain_".$j."_".$idx, "CPDDomain_".$j."_".$idx, 1, $isChecked=$a[$i]['CPDDomain'.$idx], $Class='', $Display=$Lang['Appraisal']['ARFormCPDDomain'][$idx], $Onclick='', $Disabled='')."<br>";
            }
            $row .= "</td>";
            if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
                $row .= "<td>";
                for($idx=0; $idx < 9; $idx++){
                    $row .= $indexVar['libappraisal_ui']->Get_Checkbox("SubjectRelated_".$j."_".$idx, "SubjectRelated_".$j."_".$idx, 1, $isChecked=$a[$i]['SubjectRelated'.$idx], $Class='', $Display=$Lang['Appraisal']['ARFormCPDSubject'][$idx], $Onclick='', $Disabled='')."<br>";
                }
                $row .= "</td>";
            }
            $row .= "<td>";
            $row .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDNoOfSection_'.$j, 'CPDNoOfSection_'.$j, $a[$i]['NoOfSection'], 'CPDNoOfSection');
            $row .= "</td>";
            //$row .= "<td>";
            //$row .= $indexVar['libappraisal_ui']->GET_TEXTBOX_NUMBER('CPDBasicLaw_'.$j, 'CPDBasicLaw_'.$j, $a[$i]['BasicLawRelated']);
            //$row .= "</td>";
            $row .= "<td>";
            $row .= "<span class=\"table_row_tool row_content_tool\" style=\"left;\">";
            $row .= "<span style=\"float:right;\"><a href=\"javascript:void(0);\" class=\"delete_dim\" title=\"".$Lang['Appraisal']['ARFormDelete']."\" onclick=\"deleteCPD('".$recordID."','".$batchID."','".$j."');\"></a></span>";
            $row .= "</span>";
            $row .= "<input type=\"hidden\" id=\"hiddenCPDSts_".$j."\" name=\"hiddenCPDSts_".$j."\" VALUE=\"ACTIVE\">";
            $row .= "<input type=\"hidden\" id=\"hiddenCPDSeqID_".$j."\" class=\"hiddenCPDSeqID\" name=\"hiddenCPDSeqID_".$j."\" VALUE=\"".$a[$i]["SeqID"]."\">";
            $x15 .= "<input type=\"hidden\" id=\"hiddenUserID_".$j."\" class=\"hiddenUserID\" name=\"hiddenUserID_".$j."\" VALUE=\"".$userID."\">";
            $x15 .= "<input type=\"hidden\" id=\"hiddenAcademicYearID_".$j."\" class=\"hiddenAcademicYearID\" name=\"hiddenAcademicYearID_".$j."\" VALUE=\"".$academicYearIDs[0]['AcademicYearID']."\">";
            $row .= "</td>";
            $row .= "</tr>";
            $runningIndex++;
        }
        if($reply==""){
            $reply = $Lang['Appraisal']['Message']['CPDImportComplete'];
        }
        if($sys_custom['eAppraisal']['3YearsCPD']==true){
            $pastYearCPDHours = array();
            $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='TeacherApprisal' AND SettingName='CPDSetting'";
            $cpdSettingList = unserialize(current($connection->returnVector($sql)));
            $cpdSettingList['Period'] = ($cpdSettingList['Period'])?$cpdSettingList['Period']:1;
            $cpdSettingList["SelectedYear"] = ($cpdSettingList['SelectedYear'])?$cpdSettingList['SelectedYear']:$_SESSION['CurrentSchoolYearID'];
            $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID=".$cpdSettingList["SelectedYear"];
            $startYearSeq = current($connection->returnVector($sql));
            $sql = "SELECT Sequence FROM ACADEMIC_YEAR WHERE AcademicYearID=".$academicYearIDs[0]['AcademicYearID'];
            $currSeq = current($connection->returnVector($sql));
            $yearNo = (($startYearSeq - $currSeq)%$cpdSettingList['Period']);
            for($y=$yearNo; $y >= 0; $y--){
                $sql = "SELECT AcademicYearID, YearNameB5, YearNameEN FROM ACADEMIC_YEAR WHERE Sequence='".($currSeq+$y)."'";
                $pastYearData = $connection->returnArray($sql);
                $sql = "SELECT SUM(NoOfSection) as totalCPDHours FROM INTRANET_TEACHER_PORTFOLIO_FORM_DATA_GRID_CPD WHERE UserID='".$userID."' AND AcademicYear='".$pastYearData[0]['YearNameEN']."'";
                $totalCPDHours = current($connection->returnVector($sql));
                //			$totalCPDHours = 0;
                //			foreach($cpdRecords as $rec){
                //				$totalCPDHours = $totalCPDHours + floatval($rec['NoOfSection']);
                //			}
                    $pastYearCPDHours[$pastYearData[0]['AcademicYearID']] = $totalCPDHours;
                    $sql = "INSERT INTO INTRANET_PA_T_PRE_CPD_HOURS VALUES (".IntegerSafe($rlsNo).", ".IntegerSafe($recordID).", ".IntegerSafe($batchID).", ".IntegerSafe($userID).", ".IntegerSafe($pastYearData[0]['AcademicYearID']).", '".$totalCPDHours."', ".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW())
						 ON DUPLICATE KEY UPDATE Hours='".$totalCPDHours."', ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."', DateTimeModified=NOW()";
                    $connection->db_db_query($sql);
            }
            
            $arr = array(
                "Reply"=>$reply,
                "newRowInDB"=>$runningIndex,
                "RowsToBeAppended"=>$row,
                "CPDHours1"=>$CPDHours[0],
                "CPDHours2"=>$CPDHours[1],
                "PastYearCPDHours"=>$pastYearCPDHours
            );
        }else{
            $arr = array(
                "Reply"=>$reply,
                "newRowInDB"=>$runningIndex,
                "RowsToBeAppended"=>$row,
                "CPDHours1"=>$CPDHours[0],
                "CPDHours2"=>$CPDHours[1]
            );
        }
        $jsonObj = new JSON_obj();
        echo $jsonObj->encode($arr);
}
else if($type == "transferAttendnanceDataToForm"){
    include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
    $StaffAttend3 = new libstaffattend3();
    $userID = $_POST["userID"];
    $staffs = $StaffAttend3->Get_Staff_Info($userID);
    $Params = array();
    $Params["StatusAbsent"] = 1;
    $Params["StatusLate"] = 1;
    $Params["StatusEarlyLeave"] = 1;
    $Params["StatusOuting"] = 1;
    $Params["StatusHoliday"] = 1;
    $Params["HaveRecord"] = 1;
    $Params["SelectStaff"] = Get_Array_By_Key($staffs,'UserID');
    
    $dataTable = array();
    $sql = "Select ReasonID,TAReasonID,Days,TypeID,MapID From INTRANET_PA_S_ATTEDNACE_REASON_MAPPING";
    $mappingTable = $connection->returnArray($sql);
    $mappingArray = array();
    foreach($mappingTable as $mt){
        $mappingArray[$mt['TypeID'].'_'.$mt['ReasonID']] = array("ReasonID"=>$mt['ReasonID'], "TAReasonID"=>$mt['TAReasonID'], "Days"=>$mt['Days'], "TypeID"=>$mt['TypeID']);
    }
    $sql = "SELECT TAReasonID,CountTotalBeforeThisReason FROM INTRANET_PA_S_ATTEDNACE_REASON";
    $reasonTable = $connection->returnArray($sql);
    $sql = "SELECT DATE_FORMAT(PeriodStart, '%Y-%m-%d') as PeriodStart, DATE_FORMAT(PeriodEnd, '%Y-%m-%d') as PeriodEnd, TAColumnID FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder ASC";
    $columnTable = $connection->returnArray($sql);
    foreach($reasonTable as $reason){
        $dataTable[$reason['TAReasonID']] = array();
        foreach($columnTable as $col){
            $dataTable[$reason['TAReasonID']][$col['TAColumnID']] = 0;
            $Params["TargetStartDate"] = $col['PeriodStart'];
            $Params["TargetEndDate"] = $col['PeriodEnd'];
            $StartDate = explode('-',$Params["TargetStartDate"]);
            $StartYear = $StartDate[0];
            $StartMonth = $StartDate[1];
            $StartDay = $StartDate[2];
            $EndDate = explode('-',$Params["TargetEndDate"]);
            $EndYear = $EndDate[0];
            $EndMonth = $EndDate[1];
            $EndDay = $EndDate[2];
            $YearArr = array();// start year to end year
            $MonthArr = array();// months of start year to months of end year [assoc array using numeric year as key]
            for($y = intval($StartYear);$y<=intval($EndYear);$y++)
            {
                $YearArr[] = $y;
                $MonthArr[$y] = array();
            }
            for($i=0;$i<sizeof($YearArr);$i++)
            {
                if(sizeof($YearArr)==1)//only 1 year
                {
                    for($m=intval($StartMonth);$m<=intval($EndMonth);$m++)
                    {
                        $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                    }
                }else if($i==0)//first year
                {
                    for($m=intval($StartMonth);$m<=12;$m++)
                    {
                        $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                    }
                }else if($i==(sizeof($YearArr)-1))//last year
                {
                    for($m=1;$m<=$EndMonth;$m++)
                    {
                        $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                    }
                }else// middle years
                {
                    for($m=1;$m<=12;$m++)
                    {
                        $MonthArr[$YearArr[$i]][] = ($m<10)?'0'.$m:''.$m;
                    }
                }
            }
            $AssocDataList = array();
            for($i=0;$i<sizeof($YearArr);$i++)
            {
                $CurYear = $YearArr[$i];// int type e.g. 2009
                for($j=0;$j<sizeof($MonthArr[$CurYear]);$j++)
                {
                    $CurMonth = $MonthArr[$CurYear][$j]; //string type e.g. 01 which is January
                    
                    $tempArr = $StaffAttend3->Get_Report_Customize_Monthly_Detail_Report_Data($Params, $CurYear, $CurMonth, 2);
                    if(sizeof($tempArr)>0)
                        foreach($tempArr as $theUser => $Record)
                        {
                            if(sizeof($Record)>0)
                                foreach($Record as $Datekey => $Value)
                                {
                                    $AssocDataList[$theUser][$Datekey]=$Value;
                                }
                        }
                }
            }
            
            $attendanceData = $AssocDataList[$userID];
            $startTimeTS = strtotime($Params["TargetStartDate"]);
            $endTimeTS = strtotime($Params["TargetEndDate"]);
            for($currTime = $startTimeTS; $currTime <= $endTimeTS; $currTime += (24*60*60)){
                $currTimeYMD = date('Y-m-d',$currTime);
                if(empty($attendanceData[$currTimeYMD])){
                    continue;
                }else{
                    foreach($attendanceData[$currTimeYMD] as $attData){
                        $realReasonID = $attData['ReasonID'];
                        if(empty($realReasonID)){
                            $realReasonID = $attData['ELReasonID'];
                        }
                        $statusNo = $attData['Status'];
                        if($statusNo != CARD_STATUS_NORMAL){
                            $isTreated = false;
                            if($attData['OutSchoolStatus'] == CARD_STATUS_EARLYLEAVE){
                                if(!is_null($realReasonID)){
                                    if($mappingArray[CARD_STATUS_EARLYLEAVE."_".$realReasonID]['TAReasonID']==$reason['TAReasonID']){
                                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_EARLYLEAVE."_".$realReasonID]['Days'];
                                    }
                                }else{
                                    if(CARD_STATUS_EARLYLEAVE == $mappingArray[CARD_STATUS_EARLYLEAVE."_0"]['TypeID'] && $mappingArray[CARD_STATUS_EARLYLEAVE."_0"]['TAReasonID']==$reason['TAReasonID']){
                                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_EARLYLEAVE."_0"]['Days'];
                                    }
                                }
                                $isTreated = true;
                            }
                            if($attData['InSchoolStatus'] == CARD_STATUS_LATE){
                                if(!is_null($realReasonID) && $realReasonID != $attData['ELReasonID']){
                                    if($mappingArray[CARD_STATUS_LATE."_".$realReasonID]['TAReasonID']==$reason['TAReasonID']){
                                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_LATE."_".$realReasonID]['Days'];
                                    }
                                }else{
                                    if(CARD_STATUS_LATE == $mappingArray[CARD_STATUS_LATE."_0"]['TypeID'] && $mappingArray[CARD_STATUS_LATE."_0"]['TAReasonID']==$reason['TAReasonID']){
                                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[CARD_STATUS_LATE."_0"]['Days'];
                                    }
                                }
                                $isTreated = true;
                            }
                            if(!$isTreated){
                                if(!is_null($realReasonID)){
                                    if($mappingArray[$statusNo."_".$realReasonID]['TAReasonID']==$reason['TAReasonID']){
                                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[$statusNo."_".$realReasonID]['Days'];
                                    }
                                }else{
                                    if($statusNo == $mappingArray[$statusNo."_0"]['TypeID'] && $mappingArray[$statusNo."_0"]['TAReasonID']==$reason['TAReasonID']){
                                        $dataTable[$reason['TAReasonID']][$col['TAColumnID']] += $mappingArray[$statusNo."_0"]['Days'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if($reason['CountTotalBeforeThisReason'] == 1){
            $totalData = array();
            foreach($dataTable as $reasonID=>$reasonData){
                foreach($reasonData as $colID=>$colData){
                    if(isset($totalData[$colID])){
                        $totalData[$colID] += $dataTable[$reasonID][$colID];
                    }else{
                        $totalData[$colID] = $dataTable[$reasonID][$colID];
                    }
                }
            }
            $dataTable['total'] = $totalData;
        }
    }
    
    foreach($dataTable as $reasonID=>$reasonData){
        if($reasonID=="total")continue;
        foreach($reasonData as $colID=>$colData){
            $sql = "SELECT AttID FROM INTRANET_PA_S_ATTEDNACE_DATA WHERE RlsNo=$rlsNo AND RecordID=$recordID AND BatchID=$batchID AND UserID='".$userID."' AND TAReasonID='".$reasonID."' AND TAColumnID='".$colID."'";
            $attID = $connection->returnVector($sql);
            if(empty($attID)){
                $sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_DATA (RlsNo, RecordID, BatchID, UserID, TAReasonID, TAColumnID, Days, DateInput, InputBy, DateModified, ModifyBy)
						VALUES ($rlsNo, $recordID, $batchID, '".$userID."', '".$reasonID."', '".$colID."', '".$colData."', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
            }else{
                $sql = "UPDATE INTRANET_PA_S_ATTEDNACE_DATA SET Days='".$colData."', DateModified=NOW(), ModifyBy='".$_SESSION['UserID']."' WHERE RlsNo=$rlsNo AND RecordID=$recordID AND BatchID=$batchID AND UserID='".$userID."' AND TAReasonID='".$reasonID."' AND TAColumnID='".$colID."'";
            }
            //echo $sql;
            $connection->db_db_query($sql);
        }
    }
    
    // log attendance sync
    $logData = array();
    $recordDetail = array();
    $logData['RlsNo'] = $rlsNo;
    $logData['RecordID'] = $recordID;
    $logData['BatchID'] = $batchID;
    $logData['Function'] = 'Data Sync';
    $logData['RecordType'] = 'Attedance Import';
    $recordDetail[] = array(
        "Parameter"=>"UserID",
        "DisplayName"=>$Lang['Appraisal']['CycleTemplate']['StfIdvName'],
        "New"=>$userID,
        "MapWithTable"=>$appraisalConfig['INTRANET_USER'],
        "MapDisplay"=>"EnglishName"
    );
    $logData['RecordDetail'] = $json->encode($recordDetail);
    $result = $indexVar['libappraisal']->addFormFillingLog($logData);
    
    $jsonObj = new JSON_obj();
    echo $jsonObj->encode($dataTable);
}
// ============================== Transactional data ==============================

?>