<?php 
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
$ColForPreSetField = 10;

$connection = new libgeneralsettings();
$json = new JSON_obj();
$rlsNo=$_POST["rlsNo"];
$recordID=$_POST["recordID"];
$batchID=$_POST["batchID"];
$cycleID=$_POST["cycleID"];
$userID=$_POST["userID"];
$type=$_POST["type"];
$returnPath=$_POST["returnPath"];
$templateID=$_POST["templateID"];

// log submission
switch($type){
	case "formSave":
		$logType = "save as draft";
		break;
	case "formSignature":
		$logType = "sign & submitted";
		break;
	case "modFormSignature":
		$logType = "Modification sign & submitted";
		break;
	case "modifySave":
		$logType = "Open to modify";
		break;
}		
$logData = array();
$recordDetail = array();
$logData['RlsNo'] = $rlsNo;
$logData['RecordID'] = $recordID;
$logData['BatchID'] = $batchID;
$logData['Function'] = 'Default Form Submission';
$logData['RecordType'] = $logType;						  
$recordDetail[] = array(
						"Parameter"=>"TemplateID",
					  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
					  	"Original"=>$templateID,
					  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
					  	"MapDisplay"=>"FrmTitleEng"
					  );						  					
$logData['RecordDetail'] = $json->encode($recordDetail);
$result = $indexVar['libappraisal']->addFormFillingLog($logData);

if($type=="formSave" || $type=="formSignature" || $type=="modFormSignature"){	
	//======================================================================== INTRANET_PA_T_PRE_GEN ========================================================================//
	$jobGrade=$_POST["jobGrade"];
	$gradeEntranceDate=$_POST["gradeEntranceDate"];
	$hireDateSch=$_POST["hireDateSch"];
	$hireDateOrgn=$_POST["hireDateOrgn"];
	$senGov=$_POST["senGov"];
	$senPri=$_POST["senPri"];
	$senOrgn=$_POST["senOrgn"];
	$pubYear=$_POST["pubYear"];
	$extServ=$_POST["extServ"];
	$totClsCnt=$_POST["totClsCnt"];
	$totFormCnt=$_POST["totFormCnt"];
	$dateOfBirth = $_POST["DateOfBirth"];
	$religion = $_POST["Religion"];
	$maritalStatus = $_POST["MaritalStatus"];
	$addressChi = $_POST["AddressChi"];
	$addressEng = $_POST["AddressEng"];
	$phoneNo = $_POST["PhoneNo"];
	$regNo = $_POST["RegNo"];
	$salaryPoint = $_POST["SalaryPoint"];
	$actingPosition = $_POST["ActingPosition"];
	$positionOnEntry = $_POST["PositionOnEntry"];
	$holidayStart = $_POST["holidayStart"];
	$holidayEnd = $_POST["holidayEnd"];
	$holidayType = $_POST["holidayType"];
	$holidayDuration = $_POST["holidayDuration"];
	$longHoliday = serialize(array(
						"holidayStart"=>$holidayStart,
						"holidayEnd"=>$holidayEnd,
						"holidayType"=>$holidayType,
						"holidayDuration"=>$holidayDuration
					));
	
	$gradeEntranceDate=($gradeEntranceDate=="")?"NULL":"'".$gradeEntranceDate."'";
	$hireDateSch=($hireDateSch=="")?"NULL":"'".$hireDateSch."'";
	$hireDateOrgn=($hireDateOrgn=="")?"NULL":"'".$hireDateOrgn."'";
	$dateOfBirth=($dateOfBirth=="")?"NULL":"'".$dateOfBirth."'";
	$totClsCnt=($totClsCnt==""&&$totFormCnt!="")?$totFormCnt:$totClsCnt;
	$sql="UPDATE INTRANET_PA_T_PRE_GEN SET JobGrade='".$jobGrade."',GradeEntDate=".$gradeEntranceDate.",HireDateSch=".$hireDateSch.",HireDateOrgn=".$hireDateOrgn.",";
	$sql.="SenGov='".$senGov."',SenPri='".$senPri."', SenOrgn='".$senOrgn."',Publication='".$pubYear."',ExtServ='".$extServ."',TotClsCnt='".$totClsCnt."',";
	$sql.="DateOfBirth=".$dateOfBirth.",Religion='".$religion."',MaritalStatus='".$maritalStatus."',AddressChi='".$addressChi."',AddressEng='".$addressEng."',PhoneNo='".$phoneNo."',";
	$sql .= "RegNo='".$regNo."',SalaryPoint='".$salaryPoint."',ActingPosition='".$actingPosition."',LongHoliday ='".$longHoliday."',PositionOnEntry='".$positionOnEntry."',";
	$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
	$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
	//echo $sql."<br/>";
	$x = $connection->db_db_query($sql);
	//======================================================================== INTRANET_PA_T_PRE_QUALI ========================================================================//
	//delete the unwanted qualification
	$currentQualificationRow=$_POST["currentQualificationRow"];
	for($i=0;$i<$currentQualificationRow;$i++){		
		$j=$i+1;
		$qualificationStatus=$_POST["hiddenQualification_".($j)];
		if($qualificationStatus=="INACTIVE"){
			$seqID=$_POST["hiddenSeqID_".$j];
			$sql="DELETE FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// prepare the data to insert/update for INTRANET_PA_T_PRE_QUALI
	$j=0;	
	for($i=0;$i<$currentQualificationRow;$i++){
		$j=$i+1;		
		$qualiDescr=$_POST["qualiDescr_".$j];
		$quaAcqSts=($_POST["quaAcqSts_".$j]==null)?"0":$_POST["quaAcqSts_".$j];
		$issuer=$_POST["issuer_".$j];
		$issueDate=$_POST["issueDate_".$j];
		$issueDate=($issueDate=="")?"NULL":("'".$issueDate."'");		
		$programme=$_POST["programme_".$j];
		$qualificationStatus=$_POST["hiddenQualification_".$j];
		$seqID=$_POST["hiddenSeqID_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
		$a=$connection->returnResultSet($sql);	
		if(sizeof($a)>0 AND $qualificationStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_QUALI SET IsCompleted=".IntegerSafe($quaAcqSts).",Issuer='".$issuer."',IssueDate=".$issueDate.",QualiDescr='".$qualiDescr."',";
			$sql.="Programme='".$programme."',ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $qualificationStatus=="ACTIVE"){			
			$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			$a=$connection->returnResultSet($sql);
			$nextSeqID=$a[0]["SeqID"]+1;
			
			$sql="INSERT INTO INTRANET_PA_T_PRE_QUALI(RlsNo,RecordID,BatchID,SeqID,IsCompleted,Issuer,IssueDate,QualiDescr,Programme,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,".IntegerSafe($quaAcqSts)." as IsCompleted,'";
			$sql.=$issuer."' as Issuer,".$issueDate." as IssueDate,";
			$sql.="'".$qualiDescr."' as QualiDescr,'".$programme."' as Programme,".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}		
	}
	//======================================================================== INTRANET_PA_T_PRE_TEACHCLASS ========================================================================//
	//delete the unwanted subjects for class
	$currentClassRow=$_POST["currentClassRow"];	
	$j=0;
	for($i=0;$i<$currentClassRow;$i++){
		$j=$i+1;
		$yearClassID=$_POST["hiddenYearClass_".$j];
		$subjectID=$_POST["hiddenSubject_".$j];
		$subjectID = (is_numeric($subjectID))?IntegerSafe($subjectID):$subjectID;
		$classStatus=$_POST["hiddenClass_".($j)];		
		if($classStatus=="INACTIVE"){
			$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHCLASS 
					WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND YearClassID=".IntegerSafe($yearClassID).
				" AND SubjectID='".$subjectID."';";			
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	$j=0;
	for($i=0;$i<$currentClassRow;$i++){
		$j=$i+1;
		$yearClassID=($_POST["yearClass_".$j]==null)?$_POST["hiddenYearClass_".$j]:$_POST["yearClass_".$j];
		$subjectID=($_POST["subjectClass_".$j]==null)?$_POST["hiddenSubject_".$j]:$_POST["subjectClass_".$j];
		$classStatus=$_POST["hiddenClass_".$j];
		$period=$_POST["clsPerCyc_".$j];		
		$subjectID = (is_numeric($subjectID))?IntegerSafe($subjectID):$subjectID;
		$sql="SELECT * FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND YearClassID=".IntegerSafe($yearClassID).
				" AND SubjectID='".$subjectID."';";
		$a=$connection->returnResultSet($sql);		
		if(sizeof($a)>0 AND $classStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_TEACHCLASS SET Period=".$period.",ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND YearClassID=".IntegerSafe($yearClassID)." AND SubjectID='".$subjectID."';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $classStatus=="ACTIVE"){
			$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHCLASS(RlsNo,RecordID,BatchID,YearClassID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($yearClassID)." as YearClassID,'".$subjectID."' as SubjectID,";
			$sql.=$period." as Period,";			
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_T_PRE_TEACHYEAR ========================================================================//
	//delete the unwanted subjects for form
	$currentFormRow=$_POST["currentFormRow"];
	$j=0;
	for($i=0;$i<$currentFormRow;$i++){
		$j=$i+1;
		$yearID=$_POST["hiddenYear_".$j];
		$subjectID=$_POST["hiddenFormSubject_".$j];
		$subjectID = (is_numeric($subjectID))?IntegerSafe($subjectID):$subjectID;
		$formStatus=$_POST["hiddenForm_".($j)];
		if($formStatus=="INACTIVE"){
			$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHYEAR
					WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND YearID=".IntegerSafe($yearID).
						" AND SubjectID='".$subjectID."';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	$j=0;
	for($i=0;$i<$currentFormRow;$i++){
		$j=$i+1;
		$yearID=($_POST["form_".$j]==null)?$_POST["hiddenYear_".$j]:$_POST["form_".$j];
		$subjectID=($_POST["subjectFormClass_".$j]==null)?$_POST["hiddenFormSubject_".$j]:$_POST["subjectFormClass_".$j];
		$subjectID = (is_numeric($subjectID))?IntegerSafe($subjectID):$subjectID;	
		$period=$_POST["formPerCyc_".$j];
		$formStatus=$_POST["hiddenForm_".($j)];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND YearID=".IntegerSafe($yearID).
		" AND SubjectID='".$subjectID."';";		
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0 AND $formStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_TEACHYEAR SET Period=".$period.",ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND YearID=".IntegerSafe($yearID)." AND SubjectID='".$subjectID."';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $formStatus=="ACTIVE"){
			$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHYEAR(RlsNo,RecordID,BatchID,YearID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($yearID)." as YearID,'".$subjectID."' as SubjectID,";
			$sql.=$period." as Period,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_T_PRE_OTH ========================================================================//
	// for the Academic Duties
	$j=0;
	for($i=0;$i<$ColForPreSetField;$i++){
		$j=$i+1;
		$descr=$_POST["othAcaDesc_".$j];		
		$sql="SELECT * FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='S';";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_T_PRE_OTH SET Descr='".$descr."',";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='S';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else{
			$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,'S' as DutyType,".IntegerSafe($j)." as SeqID,'".$descr."' as Descr,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}		
	}
	// for the Administrative Duties
	$j=0;
	for($i=0;$i<$ColForPreSetField;$i++){
		$j=$i+1;
		$descr=$_POST["othAdmDesc_".$j];		
		$sql="SELECT * FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='A';";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_T_PRE_OTH SET Descr='".$descr."',";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='A';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else{
			$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,'A' as DutyType,".IntegerSafe($j)." as SeqID,'".$descr."' as Descr,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}		
	}
	// for the Extra-Curriculum Duties
	$j=0;
	$rowOthExtCur = $ColForPreSetField;
	if($_POST['othExtCurLength'] > $ColForPreSetField){
		$rowOthExtCur = intval($_POST['othExtCurLength']);
	}
	for($i=0;$i<$rowOthExtCur;$i++){
		$j=$i+1;
		$descr=$_POST["othExtCurDesc_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='E';";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_T_PRE_OTH SET Descr='".$descr."',";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='E';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else{
			$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,'E' as DutyType,".IntegerSafe($j)." as SeqID,'".$descr."' as Descr,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// for the Other Duties
	$j=0;
	for($i=0;$i<$ColForPreSetField;$i++){
		$j=$i+1;
		$descr=$_POST["othDutDesc_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='O';";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0){
			$sql="UPDATE INTRANET_PA_T_PRE_OTH SET Descr='".$descr."',";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($j)." AND DutyType='O';";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else{
			$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,'O' as DutyType,".IntegerSafe($j)." as SeqID,'".$descr."' as Descr,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_C_ABSENCE ========================================================================//
	/*$sql = "SELECT * FROM INTRANET_PA_C_ABSENCE WHERE CycleID=".IntegerSafe($cycleID)." AND UserID=".IntegerSafe($userID).";";
	$absLsnCnt=$_POST["lesAsb"];
	$subLsnCnt=$_POST["subLes"];
	$lateCnt=$_POST["timLat"];	
	$a=$connection->returnResultSet($sql);	
	if(sizeof($a)>0){
		$sql="UPDATE INTRANET_PA_C_ABSENCE SET AbsLsnCnt=".IntegerSafe($absLsnCnt).",SubLsnCnt=".IntegerSafe($subLsnCnt).",LateCnt=".IntegerSafe($lateCnt).",";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
		$sql.="WHERE CycleID=".IntegerSafe($cycleID)." AND UserID=".IntegerSafe($userID);
		//echo $sql."<br/>";
		$x = $connection->db_db_query($sql);
	}
	else{
		$sql="INSERT INTO INTRANET_PA_C_ABSENCE(CycleID,UserID,AbsLsnCnt,SubLsnCnt,LateCnt,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
		$sql.="SELECT ".IntegerSafe($cycleID)." as CycleID,".IntegerSafe($userID)." as UserID,".IntegerSafe($absLsnCnt)." as AbsLsnCnt,".IntegerSafe($subLsnCnt)." as SubLsnCnt,";
		$sql.=IntegerSafe($lateCnt)." as LateCnt,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified;";
		//echo $sql."<br/>";
		$x = $connection->db_db_query($sql);
	}*/
	//======================================================================== INTRANET_PA_T_PRE_CRS ========================================================================//
	//delete the unwanted Course/Seminar/Workshop
	$j=0;
	$currentCrsSemWsRow=$_POST["currentCrsSemWsRow"];
	for($i=0;$i<$currentCrsSemWsRow;$i++){
		$j=$i+1;
		$crsSemWsStatus=$_POST["hiddenCrsSemWsSts_".($j)];
		if($crsSemWsStatus=="INACTIVE"){
			$seqID=$_POST["hiddenCrsSemWsSeqID_".$j];
			$sql="DELETE FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// prepare the data to insert/update for INTRANET_PA_T_PRE_CRS
	$j=0;
	for($i=0;$i<$currentCrsSemWsRow;$i++){
		$j=$i+1;
		$eventDate=($_POST["eventDate_".$j]==null)?"NULL":"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["eventDate_".$j])."'";
		$orgn="'".$_POST["orgn_".$j]."'";
		$crssemws="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["crssemws_".$j])."'";
		$hours=($_POST["hours_".$j]==null)?"0":$_POST["hours_".$j];
		$certification="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["certification_".$j])."'";
		$crsSemWsStatus=$_POST["hiddenCrsSemWsSts_".$j];

		$seqID=$_POST["hiddenCrsSemWsSeqID_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_CRS WHERE RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0 AND $crsSemWsStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_CRS SET EventDate=".$eventDate.",Orgn=".$orgn.",CrsSemWs=".$crssemws.",";
			$sql.="Hours=".$hours.",Certification=".$certification.",ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $crsSemWsStatus=="ACTIVE"){
			$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			$a=$connection->returnResultSet($sql);
			$nextSeqID=$a[0]["SeqID"]+1;
				
			$sql="INSERT INTO INTRANET_PA_T_PRE_CRS(RlsNo,RecordID,BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,".$eventDate." as EventDate,";
			$sql.=$orgn." as Orgn,".$crssemws." as CrsSemWs,";
			$sql.=$hours." as Hours,".$certification." as Certification,".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_T_PRE_TASKASSESSMENT ========================================================================//
	if($sys_custom['eAppraisal']['DutyObjectivesAssessmentIn4Parts']==true){
		$taskAssesmentCategory = $_POST['taskAssesmentCategory'];
		foreach($taskAssesmentCategory as $index=>$catID){
			//delete the unwanted task objective/assessment
			$j=0;
			$currentTaskAssessmentRow=$_POST["currentTaskAssessmentRow"][$index];
			for($i=0;$i<$currentTaskAssessmentRow;$i++){
				$j=$i+1;
				$taskAssessmentStatus=$_POST["hiddenTaskAssessmentSts_".$catID."_".($j)];
				if($taskAssessmentStatus=="INACTIVE"){
					$seqID=$_POST["hiddenTaskAssessmentSeqID_".$catID."_".$j];
					$sql="DELETE FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND CatID=".IntegerSafe($catID)." AND SeqID=".IntegerSafe($seqID).";";
					//echo $sql."<br/>";
					$x = $connection->db_db_query($sql);
				}
			}
			// prepare the data to insert/update for INTRANET_PA_T_PRE_TASKASSESSMENT
			$j=0;
			for($i=0;$i<$currentTaskAssessmentRow;$i++){
				$j=$i+1;
				$objectvies="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskObjective_".$catID."_".$j])."'";
				$plan="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskPlan_".$catID."_".$j])."'";
				$assessment="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskAssessment_".$catID."_".$j])."'";
				$standard="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskStandard_".$catID."_".$j])."'";
				$taskAssessmentStatus=$_POST["hiddenTaskAssessmentSts_".$catID."_".$j];
		
				$seqID=$_POST["hiddenTaskAssessmentSeqID_".$catID."_".$j];
				$sql="SELECT * FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND CatID=".IntegerSafe($catID)." AND SeqID=".IntegerSafe($seqID).";";
				//echo $sql."<br/><br/>";
				$a=$connection->returnResultSet($sql);
				if(sizeof($a)>0 AND $taskAssessmentStatus=="ACTIVE"){
					$sql="UPDATE INTRANET_PA_T_PRE_TASKASSESSMENT SET Objective=".$objectvies.",Plan=".$plan.",Assessment=".$assessment.",Standard=".$standard.",";
					$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
					$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID)." AND CatID=".IntegerSafe($catID).";";
					//echo $sql."<br/>";
					$x = $connection->db_db_query($sql);
				}
				else if(sizeof($a)==0 AND $taskAssessmentStatus=="ACTIVE"){
					$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND CatID=".IntegerSafe($catID).";";
					$a=$connection->returnResultSet($sql);
					$nextSeqID=$a[0]["SeqID"]+1;
						
					$sql="INSERT INTO INTRANET_PA_T_PRE_TASKASSESSMENT(RlsNo,RecordID,BatchID,SeqID,CatID,Objective,Plan,Assessment,Standard,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
					$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID, ".IntegerSafe($catID)." as CatID,";
					$sql.=$objectvies." as Objective,".$plan." as Plan,".$assessment." as Assessment,".$standard." as Standard,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
					//echo $sql."<br/>";
					$x = $connection->db_db_query($sql);
				}
			}
		}
	}else{	
		//delete the unwanted task objective/assessment
		$j=0;
		$currentTaskAssessmentRow=$_POST["currentTaskAssessmentRow"];
		for($i=0;$i<$currentTaskAssessmentRow;$i++){
			$j=$i+1;
			$taskAssessmentStatus=$_POST["hiddenTaskAssessmentSts_".($j)];
			if($taskAssessmentStatus=="INACTIVE"){
				$seqID=$_POST["hiddenTaskAssessmentSeqID_".$j];
				$sql="DELETE FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
				//echo $sql."<br/>";
				$x = $connection->db_db_query($sql);
			}
		}
		// prepare the data to insert/update for INTRANET_PA_T_PRE_TASKASSESSMENT
		$j=0;
		for($i=0;$i<$currentTaskAssessmentRow;$i++){
			$j=$i+1;
			$objectvies="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskObjective_".$j])."'";
			$plan="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskPlan_".$j])."'";
			$assessment="'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["taskAssessment_".$j])."'";
			$taskAssessmentStatus=$_POST["hiddenTaskAssessmentSts_".$j];
	
			$seqID=$_POST["hiddenTaskAssessmentSeqID_".$j];
			$sql="SELECT * FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			if(sizeof($a)>0 AND $taskAssessmentStatus=="ACTIVE"){
				$sql="UPDATE INTRANET_PA_T_PRE_TASKASSESSMENT SET Objective=".$objectvies.",Plan=".$plan.",Assessment=".$assessment.",";
				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
				$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
				//echo $sql."<br/>";
				$x = $connection->db_db_query($sql);
			}
			else if(sizeof($a)==0 AND $taskAssessmentStatus=="ACTIVE"){
				$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				$a=$connection->returnResultSet($sql);
				$nextSeqID=$a[0]["SeqID"]+1;
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TASKASSESSMENT(RlsNo,RecordID,BatchID,SeqID,Objective,Plan,Assessment,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
				$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,";
				$sql.=$objectvies." as Objective,".$plan." as Plan,".$assessment." as Assessment,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
				//echo $sql."<br/>";
				$x = $connection->db_db_query($sql);
			}
		}
	}
	//======================================================================== INTRANET_PA_T_PRE_STUDENTTRAINING ========================================================================//
	//delete the unwanted training details
	$j=0;
	$currentStudentTrainingRow=$_POST["currentStudentTrainingRow"];
	for($i=0;$i<$currentStudentTrainingRow;$i++){
		$j=$i+1;
		$studentTrainingStatus=$_POST["hiddenStudentTrainingSts_".($j)];
		if($studentTrainingStatus=="INACTIVE"){
			$seqID=$_POST["hiddenStudentTrainingSeqID_".$j];
			$sql="DELETE FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// prepare the data to insert/update for INTRANET_PA_T_PRE_STUDENTTRAINING
	$j=0;
	for($i=0;$i<$currentStudentTrainingRow;$i++){
		$j=$i+1;
		$Competition = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["stdTrainComp_".$j])."'";
		$Trainee = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["stdTrainNo_".$j])."'";
		$Duration = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["stdTrainDur_".$j])."'";
		$Result = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["stdTrainResult_".$j])."'";
		$Remark = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["stdTrainRmk_".$j])."'";
		$studentTrainingStatus=$_POST["hiddenStudentTrainingSts_".$j];

		$seqID=$_POST["hiddenStudentTrainingSeqID_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0 AND $studentTrainingStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_STUDENTTRAINING SET Competition=".$Competition.",Trainee=".$Trainee.",Duration=".$Duration.",Result=".$Result.",Remark=".$Remark.",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $studentTrainingStatus=="ACTIVE"){
			$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			$a=$connection->returnResultSet($sql);
			$nextSeqID=$a[0]["SeqID"]+1;
				
			$sql="INSERT INTO INTRANET_PA_T_PRE_STUDENTTRAINING(RlsNo,RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,";
			$sql.=$Competition." as Competition,".$Trainee." as Trainee,".$Duration." as Duration,".$Result." as Result,".$Remark." as Remark,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_S_T_PRE_CPD ========================================================================//
	//delete the unwanted CPD details
	$j=0;
	$currentCPDRow=$_POST["currentCPDRow"];
	for($i=0;$i<$currentCPDRow;$i++){
		$j=$i+1;
		$studentTrainingStatus=$_POST["hiddenCPDSts_".($j)];
		if($studentTrainingStatus=="INACTIVE"){
			$seqID=$_POST["hiddenCPDSeqID_".$j];
			$sql="DELETE FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// prepare the data to insert/update for INTRANET_PA_S_T_PRE_CPD
	$j=0;
	for($i=0;$i<$currentCPDRow;$i++){
		$j=$i+1;
		$CPDUserID = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["hiddenUserID_".$j])."'";
		$AcademicYearID = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["hiddenAcademicYearID_".$j])."'";
		$Title = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDTitle_".$j])."'";
		$Nature = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDNature_".$j])."'";
		$Content = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDContent_".$j])."'";
		$Organization = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDOrganization_".$j])."'";
		$StartDate = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDStartDate_".$j])."'";
		$EndDate = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDEndDate_".$j])."'";
		$Mode = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDMode_".$j])."'";
		$Domain = ($_POST["CPDDomain_".$j."_0"]=="")?0:$_POST["CPDDomain_".$j."_0"];
		for($idx=1; $idx < 6; $idx++){
		$Domain .= ",".(($_POST["CPDDomain_".$j."_".$idx]=="")?0:$_POST["CPDDomain_".$j."_".$idx]);
		}
		$SubjectRelated = "";
		if($sys_custom['TeacherPortfolio']["cpdSubjectTag"]){
			$SubjectRelated = ($_POST["SubjectRelated_".$j."_0"]=="")?0:$_POST["SubjectRelated_".$j."_0"];			
			for($idx=1; $idx < count($Lang['Appraisal']['ARFormCPDSubject']); $idx++){
				$SubjectRelated .= ",".(($_POST["SubjectRelated_".$j."_".$idx]=="")?0:$_POST["SubjectRelated_".$j."_".$idx]);	
			}
		}
		$Domain = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($Domain)."'";
		$NoOfSection = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDNoOfSection_".$j])."'";
		$BasicLaw = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["CPDBasicLaw_".$j])."'";
		$SubjectRelated = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($SubjectRelated)."'";
		$CPDStatus=$_POST["hiddenCPDSts_".$j];

		$seqID=$_POST["hiddenCPDSeqID_".$j];
		$sql="SELECT * FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0 AND $CPDStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_S_T_PRE_CPD SET UserID=".$CPDUserID.", AcademicYearID=".$AcademicYearID.", StartDate=".$StartDate.", EndDate=".$EndDate.", NoOfSection=".$NoOfSection.", EventNameEng=".$Title.", EventNameChi=".$Title.", EventTypeEng=".$Nature.", EventTypeChi=".$Nature.",";
			$sql.="Content=".$Content.",Organization=".$Organization.",CPDMode=".$Mode.",CPDDomain=".$Domain.",BasicLawRelated=".$BasicLaw.", SubjectRelated=".$SubjectRelated.", ";
			$sql.="ModifyBy=".IntegerSafe($_SESSION['UserID']).",DateModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $CPDStatus=="ACTIVE"){
			$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			$a=$connection->returnResultSet($sql);
			$nextSeqID=$a[0]["SeqID"]+1;
				
			$sql="INSERT INTO INTRANET_PA_S_T_PRE_CPD(RlsNo,RecordID,BatchID,SeqID,UserID,AcademicYearID,StartDate,EndDate,NoOfSection,EventNameEng,EventNameChi,EventTypeEng,EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated,InputBy,DateInput,ModifyBy,DateModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,";
			$sql.=$CPDUserID." as UserID,".$AcademicYearID." as AcademicYearID,".$StartDate." as StartDate,".$EndDate." as EndDate,";
			$sql.=$NoOfSection." as NoOfSection,".$Title." as EventNameEng,".$Title." as EventNameChi,".$Nature." as EventTypeEng,".$Nature." as EventTypeChi,";
			$sql.=$Content." as Content,".$Organization." as Organization,".$Mode." as CPDMode,".$Domain." as CPDDomain,".$BasicLaw." as BasicLawRelated, ".$SubjectRelated." as SubjectRelated, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy,NOW() as DateInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifyBy,NOW() as DateModified ;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_T_PRE_CPD_HOURS ========================================================================//
	// Sync Academic Year with the CPD hours
	if($sys_custom['eAppraisal']['3YearsCPD']==true){
		$CPDPastAcademicYearList = $_POST['CPDPastAcademicYear'];
		foreach($CPDPastAcademicYearList as $pay){
			$academicYear = $pay;
			$pastHour = $_POST['yearHour_'.$academicYear];
//				$sql = "UPDATE INTRANET_PA_T_PRE_CPD_HOURS SET Hours='".$pastHour."', ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."', DateTimeModified=NOW()
//						WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID)." AND UserID=".IntegerSafe($_POST['fillerID'])." ";
			$sql = "INSERT INTO INTRANET_PA_T_PRE_CPD_HOURS VALUES (".IntegerSafe($rlsNo).", ".IntegerSafe($recordID).", ".IntegerSafe($batchID).", ".IntegerSafe($_POST['fillerID']).", ".IntegerSafe($academicYear).", '".$pastHour."', ".IntegerSafe($_SESSION['UserID']).",NOW(),".IntegerSafe($_SESSION['UserID']).",NOW())
					 ON DUPLICATE KEY UPDATE Hours='".$pastHour."', ModifiedBy_UserID='".IntegerSafe($_SESSION['UserID'])."', DateTimeModified=NOW()";
//				echo $sql;
			$connection->db_db_query($sql);		 		
		}		
	}
	//======================================================================== INTRANET_PA_S_ATTEDNACE_DATA ========================================================================//
	// Manually control of attendance Data on self-defined Attendance table	
	$attendanceDataIdList = $_POST['attendanceDataIds'];
	if(!empty($attendanceDataIdList)){
		foreach($attendanceDataIdList as $Ids){
			$IdArray = explode("_",$Ids);
			$reasonID = $IdArray[0];
			$colID = $IdArray[1];
			$colData = $_POST['attendanceData_'.$Ids];
			$sql = "SELECT AttID FROM INTRANET_PA_S_ATTEDNACE_DATA WHERE RlsNo=$rlsNo AND RecordID=$recordID AND BatchID=$batchID AND UserID='".$userID."' AND TAReasonID='".$reasonID."' AND TAColumnID='".$colID."'";
			$attID = $connection->returnVector($sql);
			if($plugin['attendancestaff'] != true || ($sys_custom['eAppraisal']['ltmpsTransferAttendnanceDataToForm']!=true && $sys_custom['eAppraisal']['generalTransferAttendnanceDataToForm']!=true) || $sys_custom['eAppraisal']['AttendanceAutoSync']!=true){
				if(empty($attID)){
					$sql = "INSERT INTO INTRANET_PA_S_ATTEDNACE_DATA (RlsNo, RecordID, BatchID, UserID, TAReasonID, TAColumnID, Days, DateInput, InputBy, DateModified, ModifyBy)
							VALUES ($rlsNo, $recordID, $batchID, '".$userID."', '".$reasonID."', '".$colID."', '".$colData."', NOW(), '".$_SESSION['UserID']."', NOW(), '".$_SESSION['UserID']."')";
				}else{
					$sql = "UPDATE INTRANET_PA_S_ATTEDNACE_DATA SET Days='".$colData."', DateModified=NOW(), ModifyBy='".$_SESSION['UserID']."' WHERE RlsNo=$rlsNo AND RecordID=$recordID AND BatchID=$batchID AND UserID='".$userID."' AND TAReasonID='".$reasonID."' AND TAColumnID='".$colID."'";
				}
				//echo $sql;
				$connection->db_db_query($sql);
				
				// Update Remark
				if(empty($attID)){
					$attID = $connection->db_insert_id();
				}
			}	
			$attendanceRemarkData = $_POST['TAReasonRemarks_'.$reasonID];
			$sql = "UPDATE INTRANET_PA_S_ATTEDNACE_DATA SET Remarks='".$attendanceRemarkData."', DateModified=NOW(), ModifyBy='".$_SESSION['UserID']."' WHERE RlsNo=$rlsNo AND RecordID=$recordID AND BatchID=$batchID AND UserID='".$userID."' AND TAReasonID='".$reasonID."'";
			$connection->db_db_query($sql);
		}
	}
	
	//======================================================================== INTRANET_PA_T_PRE_TEACHEREXP ========================================================================//
	//delete the unwanted teaching experience records
	$j=0;
	$currentTeachingExpRow=$_POST["currentTeachingExpRow"];
	for($i=0;$i<$currentTeachingExpRow;$i++){
		$j=$i+1;
		$teachingExpStatus=$_POST["hiddenTeachingExpSts_".($j)];
		if($teachingExpStatus=="INACTIVE"){
			$seqID=$_POST["hiddenTeachingExpSeqID_".$j];
			$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHEREXP WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// prepare the data to insert/update for INTRANET_PA_T_PRE_TEACHEREXP
	$j=0;
	for($i=0;$i<$currentTeachingExpRow;$i++){
		$j=$i+1;
		$SchoolName = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["expSchoolName_".$j])."'";
		$StartDate = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["expStartDate_".$j])."'";
		$EndDate = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["expEndDate_".$j])."'";
		$Post= "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["expPost_".$j])."'";
		$teachingExpStatus=$_POST["hiddenTeachingExpSts_".$j];
		$seqID=$_POST["hiddenTeachingExpSeqID_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_TEACHEREXP WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0 AND $teachingExpStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_TEACHEREXP SET SchoolName=".$SchoolName.",StartDate=".$StartDate.",EndDate=".$EndDate.",Post=".$Post.",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $teachingExpStatus=="ACTIVE"){
			$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_TEACHEREXP WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			$a=$connection->returnResultSet($sql);
			$nextSeqID=$a[0]["SeqID"]+1;
				
			$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHEREXP(RlsNo,RecordID,BatchID,SeqID,SchoolName,StartDate,EndDate,Post,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,";
			$sql.=$SchoolName." as SchoolName,".$StartDate." as StartDate,".$EndDate." as EndDate,".$Post." as Post,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	//======================================================================== INTRANET_PA_T_PRE_TRAINING  ========================================================================//
	//delete the unwanted training records
	$j=0;
	$currentTrainingRow=$_POST["currentTrainingRow"];
	for($i=0;$i<$currentTrainingRow;$i++){
		$j=$i+1;
		$trainingStatus=$_POST["hiddenTrainingSts_".($j)];
		if($trainingStatus=="INACTIVE"){
			$seqID=$_POST["hiddenTrainingSeqID_".$j];
			$sql="DELETE FROM INTRANET_PA_T_PRE_TRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
	// prepare the data to insert/update for INTRANET_PA_T_PRE_TRAINING
	$j=0;
	for($i=0;$i<$currentTrainingRow;$i++){
		$j=$i+1;
		$Issuer = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["trainingIssuer_".$j])."'";
		$StartDate = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["trainingStartDate_".$j])."'";
		$EndDate = "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["trainingEndDate_".$j])."'";
		$Descr= "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["trainingDescr_".$j])."'";
		$Programme= "'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["trainingProgramme_".$j])."'";
		$trainingStatus=$_POST["hiddenTrainingSts_".$j];
		$seqID=$_POST["hiddenTrainingSeqID_".$j];
		$sql="SELECT * FROM INTRANET_PA_T_PRE_TRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)>0 AND $trainingStatus=="ACTIVE"){
			$sql="UPDATE INTRANET_PA_T_PRE_TRAINING SET Issuer=".$Issuer.",StartDate=".$StartDate.",EndDate=".$EndDate.",QualiDescr=".$Descr.",Programme=".$Programme.",";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND SeqID=".IntegerSafe($seqID).";";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
		else if(sizeof($a)==0 AND $trainingStatus=="ACTIVE"){
			$sql = "SELECT MAX(SeqID) as SeqID FROM INTRANET_PA_T_PRE_TRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			$a=$connection->returnResultSet($sql);
			$nextSeqID=$a[0]["SeqID"]+1;
				
			$sql="INSERT INTO INTRANET_PA_T_PRE_TRAINING(RlsNo,RecordID,BatchID,SeqID,Issuer,StartDate,EndDate,QualiDescr,Programme,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)";
			$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($nextSeqID)." as SeqID,";
			$sql.=$Issuer." as Issuer,".$StartDate." as StartDate,".$EndDate." as EndDate,".$Descr." as QualiDescr,".$Programme." as Programme,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
			//echo $sql."<br/>";
			$x = $connection->db_db_query($sql);
		}
	}
}
/*else if($type=="formSignature"){*/	
if($type=="formSignature"){
	$password=$_POST["signauture"];
	$fillerID=$_POST["fillerID"];
	$status = $indexVar['libappraisal']->signatureValidation($fillerID,$password);
	if($status=="true"){
		$sql ="UPDATE INTRANET_PA_T_FRMSUB SET SubDate=NOW(),IsLastSub=1,";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
		$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID);
		$x = $connection->db_db_query($sql);		
		
		// update IsLastSub for other documents with same rlsNo, recordID to be 0
//		$sql ="UPDATE INTRANET_PA_T_FRMSUB SET IsLastSub=0,";
//		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
//		$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID<>".IntegerSafe($batchID).";";
//		//echo $sql."<br/><br/>";
//		$x = $connection->db_db_query($sql);	
		
		// get those document w/o signing the signature
		$sql="SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND SubDate IS NULL;";
		//echo $sql."<br/><br/>";
		$y=$connection->returnResultSet($sql);
		
		if(sizeof($y)>0){
			for($i=0;$i<sizeof($y);$i++){
				// Delete the INTRANET_PA_T_PRE_GEN,INTRANET_PA_T_PRE_QUALI,INTRANET_PA_T_PRE_TEACHCLASS,INTRANET_PA_T_PRE_TEACHYEAR,INTRANET_PA_T_PRE_OTH,INTRANET_PA_T_PRE_CRS
				$sql="DELETE FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
									
				$sql="INSERT INTO INTRANET_PA_T_PRE_GEN(RlsNo,RecordID,BatchID,NameChi,NameEng,Gender,LateCnt,JobGrade,GradeEntDate,HireDateSch,HireDateOrgn,SenGov,SenPri,Publication,ExtServ,TotClsCnt,DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,NameChi,NameEng,Gender,LateCnt,JobGrade,GradeEntDate,HireDateSch,HireDateOrgn,SenGov,SenPri,Publication,ExtServ,TotClsCnt,DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_QUALI(RlsNo,RecordID,BatchID,SeqID,IsCompleted,Issuer,IssueDate,QualiDescr,Programme,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,IsCompleted,";
				$sql.="Issuer,IssueDate,QualiDescr,Programme,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHCLASS(RlsNo,RecordID,BatchID,YearClassID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearClassID,SubjectID,Period,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHYEAR(RlsNo,RecordID,BatchID,YearID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearID,SubjectID,Period,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,DutyType,SeqID,Descr,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_CRS(RlsNo,RecordID,BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_T_PRE_TASKASSESSMENT(RlsNo,RecordID,BatchID,SeqID,Objective,Plan,Assessment,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,Objective,Plan,Assessment,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_T_PRE_STUDENTTRAINING (RlsNo,RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_S_T_PRE_CPD(RlsNo,RecordID,BatchID,SeqID,UserID,Trainee,AcademicYearID,StartDate,EndDate,NoOfSection,EventNameEng,EventNameChi,EventTypeEng,EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated,InputBy,DateInput,ModifyBy,DateModified)";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,";
				$sql.="UserID, AcademicYearID, StartDate, EndDate, NoOfSection, EventNameEng, EventNameChi, EventTypeEng, EventTypeChi, Content, Organization, CPDMode, CPDDomain, BasicLawRelated, SubjectRelated,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy,NOW() as DateInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifyBy,NOW() as DateModified ";
				$sql.="FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/>";
				$x = $connection->db_db_query($sql);
			}
		}
		else{
			$sql="UPDATE INTRANET_PA_T_FRMSUM SET IsComp=1,";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RecordID=".IntegerSafe($recordID)." AND CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
			//echo $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
	}
}
else if($type=="modFormSignature"){
	$password=$_POST["signauture"];
	$fillerID=$_POST["fillerID"];
	$status = $indexVar['libappraisal']->signatureValidation($fillerID,$password);
	if($status=="true"){
		$sql ="UPDATE INTRANET_PA_T_FRMSUB SET SubDate=NOW(),ModDateFr=NULL,ModDateTo=NULL,ModRemark=NULL,";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
		$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID);
		$x = $connection->db_db_query($sql);
		
		// get those document w/o signing the signature
		$sql="SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND SubDate IS NULL;";
		//echo $sql."<br/><br/>";
		$y=$connection->returnResultSet($sql);
				
		if(sizeof($y)>0){
			for($i=0;$i<sizeof($y);$i++){
				// Delete the INTRANET_PA_T_PRE_GEN,INTRANET_PA_T_PRE_QUALI,INTRANET_PA_T_PRE_TEACHCLASS,INTRANET_PA_T_PRE_TEACHYEAR,INTRANET_PA_T_PRE_OTH,INTRANET_PA_T_PRE_CRS
				$sql="DELETE FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
		
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_GEN(RlsNo,RecordID,BatchID,NameChi,NameEng,Gender,LateCnt,JobGrade,GradeEntDate,HireDateSch,HireDateOrgn,SenGov,SenPri,Publication,ExtServ,TotClsCnt,DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,NameChi,NameEng,Gender,LateCnt,JobGrade,GradeEntDate,HireDateSch,HireDateOrgn,SenGov,SenPri,Publication,ExtServ,TotClsCnt,DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_QUALI(RlsNo,RecordID,BatchID,SeqID,IsCompleted,Issuer,IssueDate,QualiDescr,Programme,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,IsCompleted,";
				$sql.="Issuer,IssueDate,QualiDescr,Programme,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHCLASS(RlsNo,RecordID,BatchID,YearClassID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearClassID,SubjectID,Period,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHYEAR(RlsNo,RecordID,BatchID,YearID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearID,SubjectID,Period,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,DutyType,SeqID,Descr,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_CRS(RlsNo,RecordID,BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_T_PRE_TASKASSESSMENT(RlsNo,RecordID,BatchID,SeqID,Objective,Plan,Assessment,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,Objective,Plan,Assessment,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_T_PRE_STUDENTTRAINING (RlsNo,RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_S_T_PRE_CPD(RlsNo,RecordID,BatchID,SeqID,UserID,Trainee,AcademicYearID,StartDate,EndDate,NoOfSection,EventNameEng,EventNameChi,EventTypeEng,EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated,InputBy,DateInput,ModifyBy,DateModified)";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,";
				$sql.="UserID, AcademicYearID, StartDate, EndDate, NoOfSection, EventNameEng, EventNameChi, EventTypeEng, EventTypeChi, Content, Organization, CPDMode, CPDDomain, BasicLawRelated,SubjectRelated,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy,NOW() as DateInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifyBy,NOW() as DateModified ";
				$sql.="FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/>";
				$x = $connection->db_db_query($sql);
			}
		}
		else{
			$sql="UPDATE INTRANET_PA_T_FRMSUM SET IsComp=1,";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RecordID=".IntegerSafe($recordID)." AND CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
			//echo $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		
		// get those documents which is the last submission
		$sql="SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID<>".IntegerSafe($batchID)." AND IsLastSub=1;";
		//echo $sql."<br/><br/>";
		$y=$connection->returnResultSet($sql);
		
		if(sizeof($y)>0){
			for($i=0;$i<sizeof($y);$i++){
				// Delete the INTRANET_PA_T_PRE_GEN,INTRANET_PA_T_PRE_QUALI,INTRANET_PA_T_PRE_TEACHCLASS,INTRANET_PA_T_PRE_TEACHYEAR,INTRANET_PA_T_PRE_OTH,INTRANET_PA_T_PRE_CRS
				$sql="DELETE FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				$sql="DELETE FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
		
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_GEN(RlsNo,RecordID,BatchID,NameChi,NameEng,Gender,LateCnt,JobGrade,GradeEntDate,HireDateSch,HireDateOrgn,SenGov,SenPri,Publication,ExtServ,TotClsCnt,DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,NameChi,NameEng,Gender,LateCnt,JobGrade,GradeEntDate,HireDateSch,HireDateOrgn,SenGov,SenPri,Publication,ExtServ,TotClsCnt,DateOfBirth,Religion,MaritalStatus,AddressChi,AddressEng,PhoneNo,RegNo,SalaryPoint,ActingPosition,LongHoliday,PositionOnEntry,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_GEN WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_QUALI(RlsNo,RecordID,BatchID,SeqID,IsCompleted,Issuer,IssueDate,QualiDescr,Programme,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,IsCompleted,";
				$sql.="Issuer,IssueDate,QualiDescr,Programme,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_QUALI WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHCLASS(RlsNo,RecordID,BatchID,YearClassID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearClassID,SubjectID,Period,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TEACHCLASS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_TEACHYEAR(RlsNo,RecordID,BatchID,YearID,SubjectID,Period,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearID,SubjectID,Period,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TEACHYEAR WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_OTH(RlsNo,RecordID,BatchID,DutyType,SeqID,Descr,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,DutyType,SeqID,Descr,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_OTH WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
					
				$sql="INSERT INTO INTRANET_PA_T_PRE_CRS(RlsNo,RecordID,BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,EventDate,Orgn,CrsSemWs,Hours,Certification,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_CRS WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_T_PRE_TASKASSESSMENT(RlsNo,RecordID,BatchID,SeqID,Objective,Plan,Assessment,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,Objective,Plan,Assessment,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_TASKASSESSMENT WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_T_PRE_STUDENTTRAINING (RlsNo,RecordID,BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,Competition,Trainee,Duration,Result,Remark,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ";
				$sql.="FROM INTRANET_PA_T_PRE_STUDENTTRAINING WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
				
				$sql="INSERT INTO INTRANET_PA_S_T_PRE_CPD(RlsNo,RecordID,BatchID,SeqID,UserID,Trainee,AcademicYearID,StartDate,EndDate,NoOfSection,EventNameEng,EventNameChi,EventTypeEng,EventTypeChi,Content,Organization,CPDMode,CPDDomain,BasicLawRelated,SubjectRelated,InputBy,DateInput,ModifyBy,DateModified)";
				$sql.="SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,SeqID,";
				$sql.="UserID, AcademicYearID, StartDate, EndDate, NoOfSection, EventNameEng, EventNameChi, EventTypeEng, EventTypeChi, Content, Organization, CPDMode, CPDDomain, BasicLawRelated, SubjectRelated,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy,NOW() as DateInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifyBy,NOW() as DateModified ";
				$sql.="FROM INTRANET_PA_S_T_PRE_CPD WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
				//echo $sql."<br/>";
				$x = $connection->db_db_query($sql);
			}
		}
	}
	
}


else if($type=="modifySave"){

	$modDateFr=($_POST["modDateFr"]!=null)?"'".$_POST["modDateFr"]." 00:00:00'":"NULL";
	$modDateTo=($_POST["modDateTo"]!=null)?"'".$_POST["modDateTo"]." 23:59:59'":"NULL";
	$modRemark=($_POST["modRemark"]!="")?"'".$connection->Get_Safe_Sql_Query($_POST["modRemark"])."'":"''";
	// update the subdate and IsLastSub for current document
	$sql ="UPDATE INTRANET_PA_T_FRMSUB SET SubDate=NULL,ModDateFr=".$modDateFr.",ModDateTo=".$modDateTo.",ModRemark=".$modRemark.",";
	$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW(), ";
	$sql.="ReturnBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeReturn=NOW() ";
	$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
	//echo $sql."<br/><br/>";
	$x=$connection->db_db_query($sql);
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header($returnPath);



?>