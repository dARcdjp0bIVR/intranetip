<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$connection = new libgeneralsettings();
$lexport = new libexporttext();
$indexVar['libappraisal'] = new libappraisal();

// build content data array

$sql = "SELECT TAColumnNameChi,TAColumnNameEng FROM INTRANET_PA_S_ATTEDNACE_COLUMN";
$colNameList = $connection->returnArray($sql);

$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("TAReasonNameChi", "TAReasonNameEng")." as reason FROM INTRANET_PA_S_ATTEDNACE_REASON";
$reasonList = $connection->returnVector($sql);

$sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE RecordType=1";
$staffFullList = $connection->returnVector($sql);

$sampleList = array();
$randNo = rand(0, count($staffFullList));
for($k=0; $k < count($reasonList); $k++){
	$sampleList[] = $staffFullList[$randNo];
}


$dataAry = array();

$idx = 2;
$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['CycleExportLeaveEng']['LeaveType'];
foreach($colNameList as $col){
	$dataAry[0][$idx]=$col['TAColumnNameEng'];
	$idx++;
}
$dataAry[0][$idx]=$Lang['Appraisal']['CycleExportLeaveEng']['Rmk'];

$idx = 2;
$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType'];
foreach($colNameList as $col){
	$dataAry[1][$idx]=$col['TAColumnNameChi'];
	$idx++;
}
$dataAry[1][$idx]=$Lang['Appraisal']['CycleExportLeaveChi']['Rmk'];

$dataIndex = 2;
for($k=0; $k < count($reasonList); $k++){
	$idx = 2;
	$dataAry[$dataIndex][0]='T1';
	$dataAry[$dataIndex][1]=$reasonList[$k];
	foreach($colNameList as $col){
		$leaveDate = rand(0,10);
		$dataAry[$dataIndex][$idx]=$leaveDate;
		$idx++;
	}
	$dataAry[$dataIndex][$idx]='';
	$dataIndex++;
}	

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'attendance.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");





?>