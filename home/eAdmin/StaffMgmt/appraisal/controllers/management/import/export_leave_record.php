<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$cycleID=$_GET["cycleID"];
$lexport = new libexporttext();
// build content data array
$dataAry = array();
$connection = new libgeneralsettings();

$sql="SELECT CycleID,ipcl.UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark,EnglishName,ChineseName,UserLogin
	FROM(
		SELECT CycleID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark 
		FROM INTRANET_PA_C_LEAVE WHERE CycleID=".$cycleID."
	) as ipcl
	INNER JOIN(
		SELECT EnglishName,ChineseName,UserLogin,UserID
		FROM ".$appraisalConfig['INTRANET_USER']."
	) as iu ON ipcl.UserID=iu.UserID
	;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['CycleExportLeaveEng']['LeaveType'];
$dataAry[0][2]=$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithPay'];
$dataAry[0][3]=$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithoutPay'];
$dataAry[0][4]=$Lang['Appraisal']['CycleExportLeaveEng']['Days'];
$dataAry[0][5]=$Lang['Appraisal']['CycleExportLeaveEng']['Rmk'];

$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType'];
$dataAry[1][2]=$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithPay'];
$dataAry[1][3]=$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithoutPay'];
$dataAry[1][4]=$Lang['Appraisal']['CycleExportLeaveChi']['Days'];
$dataAry[1][5]=$Lang['Appraisal']['CycleExportLeaveChi']['Rmk'];

for($i=0;$i<sizeof($a);$i++){
	$dataAry[$i+2][0]=$a[$i]["UserLogin"];
	$dataAry[$i+2][1]=$a[$i]["LeaveType"];
	$dataAry[$i+2][2]=$a[$i]["DaysPaid"];
	$dataAry[$i+2][3]=$a[$i]["DaysNoPay"];
	$dataAry[$i+2][4]=$a[$i]["DaysTot"];
	$dataAry[$i+2][5]=$a[$i]["Remark"];
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'export_leave_record.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");