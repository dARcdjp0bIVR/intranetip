<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$curTab="leave";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
	$TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
if($plugin['eAppraisal_settings']['LnTReport']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
}

$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
if(file_exists($PATH_WRT_ROOT."includes/libtimetable_ui.php")){
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
}

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['BtnImportLeave']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);
//======================================================================== Content ========================================================================//
 ### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['ImportLeave']['name'];
$tmpName = $_FILES['ImportLeave']['tmp_name'];
$ext = strtoupper($lo->file_ext($name));
$err = false;
//echo $name." ".$ext." ".$tmpName." ".$cycleID."<br/><br/>";

if(!($ext == ".CSV" || $ext == ".TXT"))
{	
	/*$returnPath = 'Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'edit_import_leave_1&returnMsgKey=WrongFileFormat';
	echo $returnPath;
	header($returnPath);
	exit();*/
	$err = true;
	$htmlAry['navigation'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"float:right;\" width=\"20%\"><span id=\"msg\">".$Lang['Appraisal']['Report']['InvalidFormat']."</span></div></div><br/>";;
	
}
### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/appraisal/leave";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);

$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($ImportLeave, $TargetFilePath);
$SuccessArr['MoveCsvFileToTempFolder'] = "1";
/*
 * error: 20161210_162604_6266.CSV
 * correct: 20161210_174123_6266.CSV
 */ 

//$TargetFilePath = "/home/eclass/intranetIP/file/import_temp/appraisal/leave/20161210_174123_6266.CSV";

### Get Data from the csv file
//$ObjTimetable = new Timetable("8");

//$ColumnTitleArr = $ObjTimetable->Get_Import_Lesson_Column_Title($ParLang='En', $TargetPropertyArr=array(1));	// Required Column Only
//$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();

$ColumnTitleArr = array(6);
$ColumnTitleArr[0]=$Lang['Appraisal']['CycleExportLeave']['TeacherID'];
$ColumnTitleArr[1]=$Lang['Appraisal']['CycleExportLeave']['LeaveType'];
$ColumnTitleArr[2]=$Lang['Appraisal']['CycleExportLeave']['DaysWithPay'];
$ColumnTitleArr[3]=$Lang['Appraisal']['CycleExportLeave']['DaysWithoutPay'];
$ColumnTitleArr[4]=$Lang['Appraisal']['CycleExportLeave']['Days'];
$ColumnTitleArr[5]=$Lang['Appraisal']['CycleExportLeave']['Rmk'];

$ColumnPropertyArr = array(6);
$ColumnPropertyArr[0]="1";
$ColumnPropertyArr[1]="1";
$ColumnPropertyArr[2]="1";
$ColumnPropertyArr[3]="1";
$ColumnPropertyArr[4]="1";
$ColumnPropertyArr[5]="1";

if($err==false){
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
	$col_name = array_shift($data);
	$numRcd = count($data);
	
	$x .= "<table class=\"form_table_v30\">";
	$x .= "<tr>";
	$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleExportLeave']['SuccessfulRecord']."</td>";
	$x .= "<td><div><span id=\"SuccessfulRecord\"></span></div></td>";
	$x .= "</tr>";
	$x .= "<tr>";
	$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleExportLeave']['FailureRecord']."</td>";
	$x .= "<td><div><span id=\"FailureRecord\"></span></div></td>";
	$x .= "</tr>";
	$x .= "</table>";
	
	$x .= "<div id=\"ErrorTableDiv\">";
	$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	$x .= "<tbody>";
	$showHeader = true;
	$count = 1;
	$trueRecord=0;
	$falseRecord=0;
	
	for($i=0;$i<$numRcd;$i++){
		$err = false;
		$errMsg = "";
		$errLogin = false;
		$errLeaveType = false;
		$errDaysWithPay = false;
		$errDaysWithoutPay = false;
		$errDays = false;
		// check login name
		$sql="SELECT EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$data[$i][0]."';";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)==0){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NoLoginUser']."<br/><br/>";
			$errLogin = true;
			//echo "ErrLogin: ".$errLogin."<br/><br/>";
		}
		// check leave type
		if(($data[$i][1]!="Official Leave" && $data[$i][1]!="Sick Leave" && $data[$i][1]!="Maternity Leave" && $data[$i][1]!="Study Leave")){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NoLeaveType']."<br/><br/>";
			$errLeaveType = true;
			//echo "ErrLeaveType: ".$errLeaveType."<br/><br/>";
		}
		// check Days with pay
		if(is_numeric($data[$i][2])==false){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errDaysWithPay = true;
			//echo "ErrDaysWithPay: ".$errDaysWithPay."<br/><br/>";
		}
		// check Days without pay
		if(is_numeric($data[$i][3])==false){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errDaysWithoutPay = true;
			//echo "ErrDaysWithoutPay: ".$errDaysWithoutPay."<br/><br/>";
		}
		// check Days
		if(is_numeric($data[$i][4])==false){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errDays = true;
			//echo "ErrDays: ".$errDays."<br/><br/>";
		}
		$err=$errLogin|$errLeaveType|$errDaysWithPay|$errDaysWithoutPay|$errDays;
		
		if($err==false){
			$trueRecord++;
		}
		else{
			$falseRecord++;
		}
		
		if($showHeader==true && $err==true){
			$x .= "<tr>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['Row']."</td>";
			$x .= "<td width=\"15%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['TeacherID']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['LeaveType']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['DaysWithPay']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['DaysWithoutPay']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['Days']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['Rmk']."</td>";
			$x .= "<td width=\"25%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['SysRmk']."</td>";
			$x .= "</tr>";
			$showHeader = false;
		}
		if($err==true && $count%2==1){
			$x .= "<tr style=\"vertical-align:top\">";
			$x .= "<td class=\"tablebluerow\">".($i+1)."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errLogin==true)?"<font color=\"red\">".$data[$i][0]."</font>":$data[$i][0])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errLeaveType==true)?"<font color=\"red\">".$data[$i][1]."</font>":$data[$i][1])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errDaysWithPay==true)?"<font color=\"red\">".$data[$i][2]."</font>":$data[$i][2])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errDaysWithoutPay==true)?"<font color=\"red\">".$data[$i][3]."</font>":$data[$i][3])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errDays==true)?"<font color=\"red\">".$data[$i][4]."</font>":$data[$i][4])."</td>";
			$x .= "<td class=\"tablebluerow\">".$data[$i][5]."</td>";
			$x .= "<td class=\"tablebluerow\">".$errMsg."</td>";
			$x .= "</tr>";
			$count ++;
		}
		else if($err==true && $count%2==0){
			$x .= "<tr style=\"vertical-align:top\">";
			$x .= "<td class=\"tablebluerow2\">".($i+1)."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errLogin==true)?"<font color=\"red\">".$data[$i][0]."</font>":$data[$i][0])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errLeaveType==true)?"<font color=\"red\">".$data[$i][1]."</font>":$data[$i][1])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errDaysWithPay==true)?"<font color=\"red\">".$data[$i][2]."</font>":$data[$i][2])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errDaysWithoutPay==true)?"<font color=\"red\">".$data[$i][3]."</font>":$data[$i][3])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errDays==true)?"<font color=\"red\">".$data[$i][4]."</font>":$data[$i][4])."</td>";
			$x .= "<td class=\"tablebluerow2\">".$data[$i][5]."</td>";
			$x .= "<td class=\"tablebluerow2\">".$errMsg."</td>";
			$x .= "</tr>";
			$count ++;
		}
	}
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= "</div>";
}
else{
	$finalErr=$err;
}
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value='".$cycleID."'>\r\n";
$x .= "<input type=\"hidden\" id=\"FilePath\" name=\"FilePath\" value='".$TargetFilePath."'>\r\n";
$x .= "<input type=\"hidden\" id=\"HiddenSuccessfulRecord\" name=\"HiddenSuccessfulRecord\" value=".$trueRecord.">\r\n";
$x .= "<input type=\"hidden\" id=\"HiddenFailureRecord\" name=\"HiddenFailureRecord\" value=".$falseRecord.">\r\n";

$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================

$isDisabled=($err==false)?0:1;
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnImport'], "button", "goImport()", 'submitBtn', '', $Disabled=$isDisabled);
//$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
$htmlAry['cancelBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel('".$cycleID."')", 'cancelBtn');
// ============================== Define Button ==============================

?>