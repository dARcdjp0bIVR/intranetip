<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$curTab="slLateSub";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
$TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
if($plugin['eAppraisal_settings']['LnTReport']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
}

$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_GET["cycleID"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
if(file_exists($PATH_WRT_ROOT."includes/libtimetable_ui.php")){
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
}

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['BtnImportSLLateSub']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=2, $stepAry);
//======================================================================== Content ========================================================================//
 ### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['ImportLeave']['name'];
$tmpName = $_FILES['ImportLeave']['tmp_name'];
$ext = strtoupper($lo->file_ext($name));
$err = false;
//echo $name." ".$ext." ".$tmpName." ".$cycleID."<br/><br/>";

if(!($ext == ".CSV" || $ext == ".TXT"))
{	
	/*$returnPath = 'Location: ?task=management'.$appraisalConfig['taskSeparator'].'appraisal_period'.$appraisalConfig['taskSeparator'].'edit_import_leave_1&returnMsgKey=WrongFileFormat';
	echo $returnPath;
	header($returnPath);
	exit();*/
	$err = true;
	$htmlAry['navigation'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"float:right;\" width=\"20%\"><span id=\"msg\">".$Lang['Appraisal']['Report']['InvalidFormat']."</span></div></div><br/>";;
	
}
### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/appraisal/leave";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);

$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($ImportLeave, $TargetFilePath);
$SuccessArr['MoveCsvFileToTempFolder'] = "1";
/*
 * error: 20161210_162604_6266.CSV
 * correct: 20161210_174123_6266.CSV
 */ 

//$TargetFilePath = "/home/eclass/intranetIP/file/import_temp/appraisal/leave/20161210_174123_6266.CSV";

### Get Data from the csv file
//$ObjTimetable = new Timetable("8");

//$ColumnTitleArr = $ObjTimetable->Get_Import_Lesson_Column_Title($ParLang='En', $TargetPropertyArr=array(1));	// Required Column Only
//$ColumnPropertyArr = $ObjTimetable->Get_Import_Lesson_Column_Property();

$ColumnTitleArr = array(7);
$ColumnTitleArr[0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$ColumnTitleArr[1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$ColumnTitleArr[2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$ColumnTitleArr[3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$ColumnTitleArr[4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$ColumnTitleArr[5]=$Lang['Appraisal']['IronMan'];
$ColumnTitleArr[6]=$Lang['Appraisal']['EarlyBird'];

$ColumnPropertyArr = array(7);
$ColumnPropertyArr[0]="1";
$ColumnPropertyArr[1]="1";
$ColumnPropertyArr[2]="1";
$ColumnPropertyArr[3]="1";
$ColumnPropertyArr[4]="1";
$ColumnPropertyArr[5]="1";
$ColumnPropertyArr[6]="1";

if($err==false){
	$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
	$col_name = array_shift($data);
	$numRcd = count($data);
	
	$x .= "<table class=\"form_table_v30\">";
	$x .= "<tr>";
	$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleExportLeave']['SuccessfulRecord']."</td>";
	$x .= "<td><div><span id=\"SuccessfulRecord\"></span></div></td>";
	$x .= "</tr>";
	$x .= "<tr>";
	$x .= "<td class=\"field_title\">".$Lang['Appraisal']['CycleExportLeave']['FailureRecord']."</td>";
	$x .= "<td><div><span id=\"FailureRecord\"></span></div></td>";
	$x .= "</tr>";
	$x .= "</table>";
	
	$x .= "<div id=\"ErrorTableDiv\">";
	$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
	$x .= "<tbody>";
	$showHeader = true;
	$count = 1;
	$trueRecord=0;
	$falseRecord=0;
	
	for($i=0;$i<$numRcd;$i++){
		$err = false;
		$errMsg = "";
		$errLogin = false;
		$errLastSL = false;
		$errThisSL = false;
		$errLastLate = false;
		$errThisLate = false;
		$errIron = false;
		$errEarly = false;
		// check login name
		$sql="SELECT EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$data[$i][0]."';";
		//echo $sql."<br/><br/>";
		$a=$connection->returnResultSet($sql);
		if(sizeof($a)==0){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NoLoginUser']."<br/><br/>";
			$errLogin = true;
			//echo "ErrLogin: ".$errLogin."<br/><br/>";
		}
		// check Last year sick leave record
		if(is_numeric($data[$i][1])==false && $data[$i][1] != "N.A."){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errLastSL = true;
			//echo "ErrLeaveType: ".$errLeaveType."<br/><br/>";
		}
		// check This year sick leave record
		if(is_numeric($data[$i][2])==false){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errThisSL = true;
			//echo "ErrDaysWithPay: ".$errDaysWithPay."<br/><br/>";
		}
		// check Last year Lateness record
		if(is_numeric($data[$i][3])==false && $data[$i][1] != "N.A."){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errLastLate = true;
			//echo "ErrDaysWithoutPay: ".$errDaysWithoutPay."<br/><br/>";
		}
		// check This year Lateness record
		if(is_numeric($data[$i][4])==false){
			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
			$errThisLate = true;
			//echo "ErrDays: ".$errDays."<br/><br/>";
		}
//		// check Iron Man/Lady
//		if(is_numeric($data[$i][5])==false){
//			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
//			$errIron = true;
//			//echo "ErrDays: ".$errDays."<br/><br/>";
//		}
//		// check Early Bird
//		if(is_numeric($data[$i][6])==false){
//			$errMsg .= $Lang['Appraisal']['CycleExportLeaveError']['NeedNum']."<br/><br/>";
//			$errEarly = true;
//			//echo "ErrDays: ".$errDays."<br/><br/>";
//		}
		$err=$errLogin|$errLastSL|$errThisSL|$errLastLate|$errThisLate|$errIron|$errEarly;
		
		if($err==false){
			$trueRecord++;
		}
		else{
			$falseRecord++;
		}
		
		if($showHeader==true && $err==true){
			$x .= "<tr>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['Row']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['TeacherID']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['IronMan']."</td>";
			$x .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['EarlyBird']."</td>";
			$x .= "<td width=\"20%\" class=\"tablebluetop tabletopnolink\" >".$Lang['Appraisal']['CycleExportLeave']['SysRmk']."</td>";
			$x .= "</tr>";
			$showHeader = false;
		}
		if($err==true && $count%2==1){
			$x .= "<tr style=\"vertical-align:top\">";
			$x .= "<td class=\"tablebluerow\">".($i+1)."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errLogin==true)?"<font color=\"red\">".$data[$i][0]."</font>":$data[$i][0])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errLastSL==true)?"<font color=\"red\">".$data[$i][1]."</font>":$data[$i][1])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errThisSL==true)?"<font color=\"red\">".$data[$i][2]."</font>":$data[$i][2])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errLastLate==true)?"<font color=\"red\">".$data[$i][3]."</font>":$data[$i][3])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errThisLate==true)?"<font color=\"red\">".$data[$i][4]."</font>":$data[$i][4])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errIron==true)?"<font color=\"red\">".$data[$i][5]."</font>":$data[$i][5])."</td>";
			$x .= "<td class=\"tablebluerow\">".(($errEarly==true)?"<font color=\"red\">".$data[$i][6]."</font>":$data[$i][6])."</td>";
			$x .= "<td class=\"tablebluerow\">".$errMsg."</td>";
			$x .= "</tr>";
			$count ++;
		}
		else if($err==true && $count%2==0){
			$x .= "<tr style=\"vertical-align:top\">";
			$x .= "<td class=\"tablebluerow2\">".($i+1)."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errLogin==true)?"<font color=\"red\">".$data[$i][0]."</font>":$data[$i][0])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errLastSL==true)?"<font color=\"red\">".$data[$i][1]."</font>":$data[$i][1])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errThisSL==true)?"<font color=\"red\">".$data[$i][2]."</font>":$data[$i][2])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errLastLate==true)?"<font color=\"red\">".$data[$i][3]."</font>":$data[$i][3])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errThisLate==true)?"<font color=\"red\">".$data[$i][4]."</font>":$data[$i][4])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errIron==true)?"<font color=\"red\">".$data[$i][5]."</font>":$data[$i][5])."</td>";
			$x .= "<td class=\"tablebluerow2\">".(($errEarly==true)?"<font color=\"red\">".$data[$i][6]."</font>":$data[$i][6])."</td>";
			$x .= "<td class=\"tablebluerow2\">".$errMsg."</td>";
			$x .= "</tr>";
			$count ++;
		}
	}
	$x .= "</tbody>";
	$x .= "</table>";
	$x .= "</div>";
}
else{
	$finalErr=$err;
}
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value='".$cycleID."'>\r\n";
$x .= "<input type=\"hidden\" id=\"FilePath\" name=\"FilePath\" value='".$TargetFilePath."'>\r\n";
$x .= "<input type=\"hidden\" id=\"HiddenSuccessfulRecord\" name=\"HiddenSuccessfulRecord\" value=".$trueRecord.">\r\n";
$x .= "<input type=\"hidden\" id=\"HiddenFailureRecord\" name=\"HiddenFailureRecord\" value=".$falseRecord.">\r\n";

$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================

$isDisabled=($err==false)?0:1;
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnImport'], "button", "goImport()", 'submitBtn', '', $Disabled=$isDisabled);
//$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
$htmlAry['cancelBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel('".$cycleID."')", 'cancelBtn');
// ============================== Define Button ==============================

?>