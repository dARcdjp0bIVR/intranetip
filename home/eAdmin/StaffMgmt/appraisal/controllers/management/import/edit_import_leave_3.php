<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$curTab="leave";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
	$TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
if($plugin['eAppraisal_settings']['LnTReport']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
}

$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_POST["CycleID"];
$filePath = $_POST["FilePath"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
if(file_exists($PATH_WRT_ROOT."includes/libtimetable_ui.php")){
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
}

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['BtnImportLeave']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=3, $stepAry);
//======================================================================== Content ========================================================================//

### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();

### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/appraisal/leave";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFilePath = $filePath;

$ColumnTitleArr = array(6);
$ColumnTitleArr[0]=$Lang['Appraisal']['CycleExportLeave']['TeacherID'];
$ColumnTitleArr[1]=$Lang['Appraisal']['CycleExportLeave']['LeaveType'];
$ColumnTitleArr[2]=$Lang['Appraisal']['CycleExportLeave']['DaysWithPay'];
$ColumnTitleArr[3]=$Lang['Appraisal']['CycleExportLeave']['DaysWithoutPay'];
$ColumnTitleArr[4]=$Lang['Appraisal']['CycleExportLeave']['Days'];
$ColumnTitleArr[5]=$Lang['Appraisal']['CycleExportLeave']['Rmk'];

$ColumnPropertyArr = array(6);
$ColumnPropertyArr[0]="1";
$ColumnPropertyArr[1]="1";
$ColumnPropertyArr[2]="1";
$ColumnPropertyArr[3]="1";
$ColumnPropertyArr[4]="1";
$ColumnPropertyArr[5]="1";

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
$col_name = array_shift($data);
$numRcd = count($data);

// delete the existing leave records
$sql = "DELETE FROM INTRANET_PA_C_LEAVE WHERE CycleID=".IntegerSafe($cycleID).";";
//echo $sql."<br/><br/>";
$a=$connection->db_db_query($sql);

$record=0;

for($i=0;$i<$numRcd;$i++){
	$sql="SELECT UserID,EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$data[$i][0]."';";
	$userSql=$connection->returnResultSet($sql);
	$userID=$userSql[0]["UserID"];
	
	$sql="INSERT INTO INTRANET_PA_C_LEAVE(CycleID,UserID,LeaveType,DaysTot,DaysPaid,DaysNoPay,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	$sql.="SELECT * FROM (SELECT ".$cycleID." as CycleID,".$userID." as UserID,'".$data[$i][1]."' as LeaveType,".$data[$i][4]." as DaysTot,".$data[$i][2]." as DaysPaid,";
	$sql.=$data[$i][3]." as DaysNoPay,'".$indexVar['libappraisal']->Get_Safe_Sql_Query($data[$i][5])."' as Remark,";
	$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
	//echo $sql."<br/><br/>";
	$a=$connection->db_db_query($sql);
	$record++;
}

$x .= "<div align=\"center\">";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleExportLeave']['SuccessRecord'],$record++)."</span>";
$x .= "</div>";

$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================
$isDisabled=($err==false)?0:1;
$htmlAry['finishBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Done'], "button", "goBack('".$cycleID."')", 'finishBtn');
// ============================== Define Button ==============================




?>