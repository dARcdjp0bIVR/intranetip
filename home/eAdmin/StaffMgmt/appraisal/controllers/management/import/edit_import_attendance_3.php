<?php 
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
# Page Title
$curTab="attendance";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
	$TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
if($plugin['eAppraisal_settings']['LnTReport']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
}

$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$cycleID = $_POST["CycleID"];
$filePath = $_POST["FilePath"];


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
if(file_exists($PATH_WRT_ROOT."includes/libtimetable_ui.php")){
	include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
}

$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['BtnImportLeave']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step1'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step2'];
$stepAry[] = $Lang['Appraisal']['CycleLeave']['Step3'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=3, $stepAry);
//======================================================================== Content ========================================================================//

### Check csv file format
$limport = new libimporttext();
$lo = new libfilesystem();

### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/appraisal/attendance";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFilePath = $filePath;

$sql = "SELECT TAColumnID,".$indexVar['libappraisal']->getLangSQL("TAColumnNameChi","TAColumnNameEng")." as ColName FROM INTRANET_PA_S_ATTEDNACE_COLUMN ORDER BY DisplayOrder";
$colList = $connection->returnArray($sql);

$colNameList = array();
foreach($colList as $cd){
	if(!in_array($cd['ColName'],$colNameList)){
		$colNameList[] = $cd['ColName'];
	}
}

$idx=2;
$ColumnTitleArr = array(count($colNameList)+3);
$ColumnTitleArr[0]=$Lang['Appraisal']['CycleExportLeave']['TeacherID'];
$ColumnTitleArr[1]=$Lang['Appraisal']['CycleExportLeave']['LeaveType'];
foreach($colNameList as $col){
	$ColumnTitleArr[$idx]=$col;
	$idx++;	
}
$ColumnTitleArr[$idx]=$Lang['Appraisal']['CycleExportLeave']['Rmk'];

$idx=2;
$ColumnPropertyArr = array(count($colNameList)+3);
$ColumnPropertyArr[0]="1";
$ColumnPropertyArr[1]="1";
foreach($colNameList as $col){
	$ColumnPropertyArr[$idx]="1";
	$idx++;	
}
$ColumnPropertyArr[$idx]="1";

$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $ColumnTitleArr, $ColumnPropertyArr);
$col_name = array_shift($data);
$numRcd = count($data);

$record=0;

for($i=0;$i<$numRcd;$i++){
	$sql="SELECT UserID,EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$data[$i][0]."';";
	$userSql=$connection->returnResultSet($sql);
	$userID=$userSql[0]["UserID"];
	
	$sql = "SELECT TAReasonID FROM INTRANET_PA_S_ATTEDNACE_REASON WHERE TAReasonNameChi='".$data[$i][1]."' OR TAReasonNameEng='".$data[$i][1]."'";
	$TAReasonID = current($connection->returnVector($sql));
	
	$sql = "SELECT RlsNo, frmsum.RecordID,BatchID FROM INTRANET_PA_T_FRMSUM frmsum
			INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
			INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
			WHERE CycleID='".$cycleID."' AND UserID='".$userID."' AND TemplateType=0 ";
	$formARecordList = $connection->returnArray($sql);
	
	$columnData = array();
	$idx=2;
	foreach($colList as $col){
		$columnData[] = array(
			'TAColumnID'=>$col['TAColumnID'],
			'Days'=>$data[$i][$idx]
		);
		$idx++;	
	}	
	$remarks = $data[$i][$idx];
	
	foreach($formARecordList as $formA){
		foreach($columnData as $cdata){
			
			// delete the existing leave records
			$sql = "DELETE FROM INTRANET_PA_S_ATTEDNACE_DATA WHERE TAColumnID='".$cdata['TAColumnID']."' AND TAReasonID='".$TAReasonID."' AND RlsNo='".$formA['RlsNo']."' AND RecordID='".$formA['RecordID']."' AND RlsNo='".$formA['BatchID']."'";
			//echo $sql."<br/><br/>";
			$a=$connection->db_db_query($sql);			
			
			$insertSQL = "INSERT INTO INTRANET_PA_S_ATTEDNACE_DATA(";
			$insertSQL .= "RlsNo,RecordID,BatchID,UserID,TAReasonID,TAColumnID,Days,Remarks,DateInput,InputBy,DateModified,ModifyBy ";
			$insertSQL .= " ) VALUES ('".$formA['RlsNo']."','".$formA['RecordID']."','".$formA['BatchID']."'";
			$insertSQL .= " ,'".$userID."','".$TAReasonID."','".$cdata['TAColumnID']."','".$cdata['Days']."','".$remarks."'";
			$insertSQL .= " ,NOW(),'".$_SESSION['UserID']."',NOW(),'".$_SESSION['UserID']."')";
			$a=$connection->db_db_query($insertSQL);
			$record++;
		}
	}
}

$x .= "<div align=\"center\">";
$x .= "<span>".sprintf($Lang['Appraisal']['CycleExportLeave']['SuccessRecord'],$record++)."</span>";
$x .= "</div>";

$htmlAry['contentTbl'] = $x;


// ============================== Define Button ==============================
$isDisabled=($err==false)?0:1;
$htmlAry['finishBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Done'], "button", "goBack('".$cycleID."')", 'finishBtn');
// ============================== Define Button ==============================




?>