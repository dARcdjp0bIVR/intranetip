<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$cycleID=$_GET["cycleID"];
$lexport = new libexporttext();
// build content data array
$dataAry = array();
$connection = new libgeneralsettings();

$sql="SELECT CycleID,ipcl.UserID,SLLastYear,SLThisYear,LateLastYear,LateThisYear,IronMan,EarlyBird,EnglishName,ChineseName,UserLogin
	FROM(
		SELECT CycleID,UserID,SLLastYear,SLThisYear,LateLastYear,LateThisYear,IronMan,EarlyBird 
		FROM INTRANET_PA_C_ATTENDANCE WHERE CycleID=".$cycleID."
	) as ipcl
	INNER JOIN(
		SELECT EnglishName,ChineseName,UserLogin,UserID
		FROM ".$appraisalConfig['INTRANET_USER']."
	) as iu ON ipcl.UserID=iu.UserID
	;";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);

$currentLang = $_SESSION['intranet_session_language'];
include_once($PATH_WRT_ROOT."/lang/appraisal_lang.en.php");
$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[0][2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[0][3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[0][4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[0][5]=$Lang['Appraisal']['IronMan'];
$dataAry[0][6]=$Lang['Appraisal']['EarlyBird'];
include_once($PATH_WRT_ROOT."/lang/appraisal_lang.b5.php");
$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[1][2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[1][3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[1][4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[1][5]=$Lang['Appraisal']['IronMan'];
$dataAry[1][6]=$Lang['Appraisal']['EarlyBird'];
if($currentLang!='b5'){
	include_once($PATH_WRT_ROOT."/lang/appraisal_lang.".$currentLang.".php");
}

for($i=0;$i<sizeof($a);$i++){
	$dataAry[$i+2][0]=$a[$i]["UserLogin"];
	$dataAry[$i+2][1]=$a[$i]["SLLastYear"];
	$dataAry[$i+2][2]=$a[$i]["SLThisYear"];
	$dataAry[$i+2][3]=$a[$i]["LateLastYear"];
	$dataAry[$i+2][4]=$a[$i]["LateThisYear"];
	$dataAry[$i+2][5]=$a[$i]["IronMan"];
	$dataAry[$i+2][6]=$a[$i]["EarlyBird"];
}

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'export_sl_late_record.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");