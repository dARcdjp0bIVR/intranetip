<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build content data array
$dataAry = array();


$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['CycleExportLeaveEng']['LeaveType'];
$dataAry[0][2]=$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithPay'];
$dataAry[0][3]=$Lang['Appraisal']['CycleExportLeaveEng']['DaysWithoutPay'];
$dataAry[0][4]=$Lang['Appraisal']['CycleExportLeaveEng']['Days'];
$dataAry[0][5]=$Lang['Appraisal']['CycleExportLeaveEng']['Rmk'];

$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['CycleExportLeaveChi']['LeaveType'];
$dataAry[1][2]=$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithPay'];
$dataAry[1][3]=$Lang['Appraisal']['CycleExportLeaveChi']['DaysWithoutPay'];
$dataAry[1][4]=$Lang['Appraisal']['CycleExportLeaveChi']['Days'];
$dataAry[1][5]=$Lang['Appraisal']['CycleExportLeaveChi']['Rmk'];

$dataAry[2][0]="T1";
$dataAry[2][1]="Sick Leave";
$dataAry[2][2]="1";
$dataAry[2][3]="0";
$dataAry[2][4]="1";
$dataAry[2][5]="";

$dataAry[3][0]="T1";
$dataAry[3][1]="Official Leave";
$dataAry[3][2]="1";
$dataAry[3][3]="3";
$dataAry[3][4]="4";
$dataAry[3][5]="yyy";

$dataAry[4][0]="T2";
$dataAry[4][1]="Sick Leave";
$dataAry[4][2]="2";
$dataAry[4][3]="1";
$dataAry[4][4]="3";
$dataAry[4][5]="xxx";

$dataAry[5][0]="T2";
$dataAry[5][1]="Official Leave";
$dataAry[5][2]="2";
$dataAry[5][3]="0";
$dataAry[5][4]="2";
$dataAry[5][5]="";

$dataAry[6][0]="T3";
$dataAry[6][1]="Sick Leave";
$dataAry[6][2]="1";
$dataAry[6][3]="0";
$dataAry[6][4]="1";
$dataAry[6][5]="";

$dataAry[5][0]="T3";
$dataAry[5][1]="Official Leave";
$dataAry[5][2]="6";
$dataAry[5][3]="1";
$dataAry[5][4]="7";
$dataAry[5][5]="ZZ";

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'leave.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");





?>