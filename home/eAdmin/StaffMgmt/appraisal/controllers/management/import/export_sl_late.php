<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build content data array
$dataAry = array();

$currentLang = $_SESSION['intranet_session_language'];
include_once($PATH_WRT_ROOT."/lang/appraisal_lang.en.php");
$dataAry[0][0]=$Lang['Appraisal']['CycleExportLeaveEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[0][2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[0][3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[0][4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[0][5]=$Lang['Appraisal']['IronMan'];
$dataAry[0][6]=$Lang['Appraisal']['EarlyBird'];
include_once($PATH_WRT_ROOT."/lang/appraisal_lang.b5.php");
$dataAry[1][0]=$Lang['Appraisal']['CycleExportLeaveChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[1][2]=$Lang['Appraisal']['SickLeaveRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[1][3]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['LastAcademicYear'];
$dataAry[1][4]=$Lang['Appraisal']['LatenessRecord']." - ".$Lang['Appraisal']['CurrentAcademicYearTilMar'];
$dataAry[1][5]=$Lang['Appraisal']['IronMan'];
$dataAry[1][6]=$Lang['Appraisal']['EarlyBird'];
if($currentLang!='b5'){
	include_once($PATH_WRT_ROOT."/lang/appraisal_lang.".$currentLang.".php");
}
$dataAry[2][0]="T1";
$dataAry[2][1]="2";
$dataAry[2][2]="1";
$dataAry[2][3]="0";
$dataAry[2][4]="1";
$dataAry[2][5]="1";
$dataAry[2][6]="1";

$dataAry[3][0]="T1";
$dataAry[3][1]="1";
$dataAry[3][2]="2";
$dataAry[3][3]="1";
$dataAry[3][4]="2";
$dataAry[3][5]="1";
$dataAry[3][6]="1";

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'sample.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");





?>