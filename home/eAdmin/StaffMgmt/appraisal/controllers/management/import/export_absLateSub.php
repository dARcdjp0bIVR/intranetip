<?php

// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================


$lexport = new libexporttext();

// build content data array
$dataAry = array();


$dataAry[0][0]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['TeacherID'];
$dataAry[0][1]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumAbsence'];
$dataAry[0][2]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumSubstitution'];
$dataAry[0][3]=$Lang['Appraisal']['CycleExportAbsLateSubEng']['NumLateness'];

$dataAry[1][0]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['TeacherID'];
$dataAry[1][1]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumAbsence'];
$dataAry[1][2]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumSubstitution'];
$dataAry[1][3]=$Lang['Appraisal']['CycleExportAbsLateSubChi']['NumLateness'];

$dataAry[2][0]="T1";
$dataAry[2][1]="2";
$dataAry[2][2]="2";
$dataAry[2][3]="0";

$dataAry[3][0]="T1";
$dataAry[3][1]="4";
$dataAry[3][2]="4";
$dataAry[3][3]="2";

$dataAry[4][0]="T2";
$dataAry[4][1]="1";
$dataAry[4][2]="1";
$dataAry[4][3]="3";

$dataAry[5][0]="T2";
$dataAry[5][1]="2";
$dataAry[5][2]="2";
$dataAry[5][3]="1";

$dataAry[6][0]="T3";
$dataAry[6][1]="3";
$dataAry[6][2]="2";
$dataAry[6][3]="0";

$dataAry[5][0]="T3";
$dataAry[5][1]="1";
$dataAry[5][2]="0";
$dataAry[5][3]="0";

$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'absence.csv';
$lexport->EXPORT_FILE($fileName, $exportContent, false, false, "UTF-8");





?>