<?php

// ============================== Related Tables ==============================
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");
# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================

$libLnt = new libappraisal_lnt();
$lexport = new libexporttext();


######## Get all question START ########
$rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);

$questions = array();
foreach($rs as $r){
    if($r['SubjectID'] != $SubjectID){
        continue;
    }
    $questions[] = $r;
}
######## Get all question END ########

######## Get Subject START ########
$subjectInfo = array(
    'WebSAMSCode' => '080',
    'Title' => 'Chinese'
);
if($SubjectID){
    $sql = "SELECT CH_DES, EN_DES, CODEID FROM ASSESSMENT_SUBJECT WHERE RecordID = '{$SubjectID}' ";
    $rs = current($libLnt->returnResultSet($sql));
    $subjectInfo = array(
        'WebSAMSCode' => $rs['CODEID'],
        'Title' => Get_Lang_Selection($rs['CH_DES'],$rs['EN_DES'])
    );
}
######## Get Subject END ########


// build content data array
$dataAry = array();


$dataAry[0][0] = 'Teacher User Login';
$dataAry[0][1] = 'Subject WebSAMSCode';
$dataAry[0][2] = 'Subject Name (Ref.)';
$dataAry[0][3] = 'ClassName';

$dataAry[1][0] = '內聯網帳號';
$dataAry[1][1] = '學科 WebSAMSCode';
$dataAry[1][2] = '學科名稱 [參考用途]';
$dataAry[1][3] = '班別';

$dataAry[2][0] = 't01';
$dataAry[2][1] = $subjectInfo['WebSAMSCode'];
$dataAry[2][2] = $subjectInfo['Title'];
$dataAry[2][3] = '1A';

foreach($questions as $question){
    $title = "Part{$question['Part']}_Q{$question['QuestionNumber']}";
    $dataAry[0][] = $title;
    $title = "分類{$question['Part']}_題目{$question['QuestionNumber']}";
    $dataAry[1][] = $title;
    
    $type = $question['QuestionType'];
    if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
        $dataAry[2][] = $question['MaxScore'];
    }else{
        $dataAry[2][] = 'Very Good';
    }
}


$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'appraisal_lnt_answer_sample.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);




