<?php
/**
 * Change Log:
 * 2020-01-07 Paul
 *  - Add back missing page switching tabs
 */
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

# Page Title
$curTab="lnt";
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Leave'], 'javascript: goLeave(0);', $curTab=='leave');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['AbsLateSub'], 'javascript: goAbsLateSub(0);', $curTab=='absLateSub');
$TAGS_OBJ[] = array($Lang['Appraisal']['CycleTemplate']['Attendance'], 'javascript: goAttendance(0);', $curTab=='attendance');
if($sys_custom['eAppraisal']['SLLateRecords']){
    $TAGS_OBJ[] = array($Lang['Appraisal']['SickLeaveAndLateRecords'], 'javascript: goSLLateSub(0);', $curTab=='slLateSub');
}
$TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], 'javascript: goLntSub(0)', $curTab=='lnt');
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);

$objJson = new JSON_obj();
$connection = new libgeneralsettings();
//======================================================================== Header ========================================================================//
### navigation
$navigationAry[] = array($Lang['Appraisal']['ImportInformation']);
$navigationAry[] = array($Lang['Appraisal']['LNT']['ImportLnt']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);
### step box
$stepAry = array();
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'];
$stepAry[] = $Lang['General']['ImportArr']['ImportStepArr']['ImportResult'];
$htmlAry['customizedImportStepTbl'] = $indexVar['libappraisal_ui']->GET_IMPORT_STEPS($CurrStep=1, $stepAry);
//======================================================================== Content ========================================================================//
$x = "<table id=\"msgTable\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" style=\"display:none\">";
$x .= "<tr>";
$x .= "<td align=\"right\" colspan=\"2\">";
$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class=\"systemmsg\">";
$x .= "<tbody>";
$x .= "<tr>";
$x .= "<td nowrap=\"nowrap\">";
$x .= "<span id=\"errMsg\" style=\"display:none\"><font color=\"red\">".$Lang['Appraisal']['Report']['InvalidFormat']."</font></span>";
$x .= "<span id=\"errMsgNoQuestion\" style=\"display:none\"><font color=\"red\">".$Lang['Appraisal']['LNT']['ImportMsg']['EmptyQuestion']."</font></span>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</tbody>";
$x .= "</table>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$htmlAry['msg'] = $x;

#### Get all QuestionID START ####
$libLnt = new libappraisal_lnt();
$countQuestion = $libLnt->getAllQuestionCount();
#### Get all QuestionID END ####


ob_start();
?>
<table class="form_table_v30">
<tr>
	<td class="field_title">
		<?=$indexVar['libappraisal_ui']->RequiredSymbol() ?>
		<span><?=$Lang['General']['AcademicYear'] ?></span>
	</td>
	<td>
		<?=getSelectAcademicYear('AcademicYearID') ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$indexVar['libappraisal_ui']->RequiredSymbol() ?>
		<span><?=$Lang['Appraisal']['LNT']['ImportType'] ?></span>
	</td>
	<td>
		<input type="radio" name="ImportType" id="ImportTypeQuestion" value="<?=libappraisal_lnt::LNT_IMPORT_TYPE_QUESTION ?>">
		<label for="ImportTypeQuestion"><?=$Lang['Appraisal']['LNT']['Question']?></label>
		
		<input type="radio" name="ImportType" id="ImportTypeAnswer" value="<?=libappraisal_lnt::LNT_IMPORT_TYPE_ANSWER ?>">
		<label for="ImportTypeAnswer"><?=$Lang['Appraisal']['LNT']['Answer']?></label>
		
		<?=$indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ImportTypeEmptyWarnDiv', $Lang['Appraisal']['TemplateSample']['Mandatory'], $Class='warnMsgDiv') ?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$indexVar['libappraisal_ui']->RequiredSymbol() ?>
		<span><?=$Lang['General']['SourceFile'] ?></span>
		<span style="color:#999999"><?=$Lang['General']['CSVFileFormat'] ?></span>
	</td>
	<td>
		<input type="file" name="ImportData" id="ImportData" class="file">
	</td>
</tr>

<tr class="typeQuestion">
	<td class="field_title">
		<span><?=$Lang['General']['CSVSample'] ?></span>
	</td>
	<td>
		<a id="downloadQuestionTemplate" class="tablelink" href="<?=GET_CSV($ParCsvName='appraisal_lnt_question_sample.csv', $ParSpecificPath=dirname(__FILE__).'/', $AppendUnicodeSuffix=0) ?>" target="_blank">
			<?=$Lang['General']['ClickHereToDownloadSample'] ?>
		</a>
	</td>
</tr>

<tr class="typeAnswer">
	<td class="field_title">
		<?=$indexVar['libappraisal_ui']->RequiredSymbol() ?>
		<span><?=$Lang['SysMgr']['SubjectClassMapping']['Subject'] ?></span>
	</td>
	<td>
		<div id="subjectContainer"></div>
	</td>
</tr>

<tr class="typeAnswer">
	<td class="field_title">
		<span><?=$Lang['General']['CSVSample'] ?></span>
	</td>
	<td>
		<a id="downloadAnswerTemplate" class="tablelink" href="javascript:generateAnswerTemplate()" target="_blank">
			<?=$Lang['Appraisal']['LNT']['GenerateCsvTemplate'] ?>
		</a>
	</td>
</tr>

<tr class="typeQuestion">
	<td class="field_title">
		<span><?=$Lang['General']['ImportArr']['DataColumn'] ?></span>
	</td>
	<td>
		<?php
		foreach($Lang['Appraisal']['LNT']['ImportRawQuestion'] as $i => $column){
		    $index = $i+1;
		    echo "{$Lang['General']['ImportArr']['Column']} {$index}: {$column}<br />";
		}
		?>
	</td>
</tr>

<tr class="typeAnswer">
	<td class="field_title">
		<span><?=$Lang['General']['ImportArr']['DataColumn'] ?></span>
	</td>
	<td>
		<?php
		foreach($Lang['Appraisal']['LNT']['ImportRawAnswer'] as $i => $column){
		    $index = $i+1;
		    echo "{$Lang['General']['ImportArr']['Column']} {$index}: {$column}<br />";
		}
		?>
	</td>
</tr>

</table>
<span style="color:#999999">
	<?=$Lang['General']['RequiredField'] ?>
	<br />
	<?=$Lang['General']['ReferenceField'] ?>
</span>

<script>
$(function(){
	$('#AcademicYearID').change(function(){
		$('#subjectContainer').html('<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image() ?>');
		if($(this).val() == ''){
			return;
		}
		
		$.get('index.php?task=management.import_lnt.ajaxGetQuestionSubjects', {
			'AcademicYearID': $(this).val()
		}, function(res){
			$('#subjectContainer').html(res);
		});
	}).change();
});
</script>

<?php
$x = ob_get_clean();

$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Import'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
//$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack('".$cycleID."')", 'backBtn');
// ============================== Define Button ==============================










?>