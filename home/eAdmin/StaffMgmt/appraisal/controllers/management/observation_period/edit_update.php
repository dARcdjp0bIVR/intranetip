<?php 
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$connection = new libgeneralsettings();
$cycleID = $_POST["CycleID"];

$formType=$_POST["type"];
$returnPath=$_POST["returnPath"];
$saveSuccess = true;
if($formType=="AddObsFrm"){
	$templateID=$_POST["DDLTemplateID"];
	$userID=$_POST["appraiseeIDStr"];
	$obsID=rtrim($_POST["appraiserIDStr"],',');
	$obsIDArr=explode(",",$obsID);
	$sql="INSERT INTO INTRANET_PA_T_FRMSUM(CycleID,TemplateID,UserID,AprID,IsActive,IsComp,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".$userID." as UserID,".$userID." as AprID,1 as IsActive,0 as IsComp,";
	$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
	//echo $sql."<br/><br/>";
	$x=$connection->db_db_query($sql);
	$sql="SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE CycleID=".$cycleID." AND TemplateID=".$templateID." ORDER BY DateTimeInput DESC;";
	//echo $sql."<br/><br/>";
	$c=$connection->returnResultSet($sql);
	//echo "RecordID: ".$c[0]["RecordID"]."<br/>";
	
	$sql="SELECT CycleID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
			FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID.";";
	$a=$connection->returnResultSet($sql);
	$sql="SELECT fs.TemplateID, fs.SecID, sr.AppRoleID, sr.CanFill, fs.DisplayOrder FROM INTRANET_PA_S_FRMSEC fs
			INNER JOIN INTRANET_PA_S_SECROL sr ON fs.SecID=sr.SecID
			WHERE fs.TemplateID=".$templateID." AND sr.CanFill=1 AND fs.ParentSecID IS NULL";
	$b=$connection->returnResultSet($sql);
	
	foreach($b as $idx=>$val){
		$IsActive = 1;	
		if($val['AppRoleID']==1){
			$obsIDArr=explode(",",$obsID);
			foreach($obsIDArr as $oID){
				$filler = $oID;
				$sql="INSERT INTO INTRANET_PA_T_FRMSUB(RlsNo,RecordID,BatchID,AppRoleID,EditPrdFr,EditPrdTo,SecID,IsActive,BatchOrder,FillerID,ObsID,IsLastSub,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT * FROM(SELECT 1 as  RlsNo,".$c[0]["RecordID"]." as RecordID,".$val['DisplayOrder']." as BatchID,".$val['AppRoleID']." as AppRoleID,'".$a[0]["CycleStart"]."' as EditPrdFr,'".$a[0]["CycleClose"]."' as EditPrdTo,".$b[$idx]["SecID"].",".$IsActive." as IsActive,";
				$sql.="".$val['DisplayOrder']." as BatchOrder,".$filler." as FillerID,'".$obsID."' as ObsID,0 as IsLastSub,";	
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
			}
		}else{
			$filler = $userID;
			if($val['AppRoleID']==2 && $idx != 0){
				$IsActive=0;
			}
			$sql="INSERT INTO INTRANET_PA_T_FRMSUB(RlsNo,RecordID,BatchID,AppRoleID,EditPrdFr,EditPrdTo,SecID,IsActive,BatchOrder,FillerID,ObsID,IsLastSub,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM(SELECT 1 as  RlsNo,".$c[0]["RecordID"]." as RecordID,".$val['DisplayOrder']." as BatchID,".$val['AppRoleID']." as AppRoleID,'".$a[0]["CycleStart"]."' as EditPrdFr,'".$a[0]["CycleClose"]."' as EditPrdTo,".$b[$idx]["SecID"].",".$IsActive." as IsActive,";
			$sql.="".$val['DisplayOrder']." as BatchOrder,".$filler." as FillerID,'".$obsID."' as ObsID,0 as IsLastSub,";	
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		
	}
}elseif($formType=="copyObsFrm"){
	$cycleID = ($cycleID=="")?$_POST['cycleID']:$cycleID;
	$sql = "SELECT TemplateID, AprID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$_POST['recordID']."'";
	$templateInfo = $connection->returnArray($sql);
	$templateID=$templateInfo[0]['TemplateID'];
	$userID=$templateInfo[0]['AprID'];
	$sql = "SELECT obsID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$_POST['rlsNo']."' AND RecordID='".$_POST['recordID']."' AND BatchID='".$_POST['batchID']."'";
	$obsID = current($connection->returnVector($sql));
	$obsIDArr=explode(",",$obsID);
	$sql="INSERT INTO INTRANET_PA_T_FRMSUM(CycleID,TemplateID,UserID,AprID,IsActive,IsComp,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
	$sql.="SELECT * FROM(SELECT ".$cycleID." as CycleID,".$templateID." as TemplateID,".$userID." as UserID,".$userID." as AprID,1 as IsActive,0 as IsComp,";
	$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
	$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
	//echo $sql."<br/><br/>";
	$x=$connection->db_db_query($sql);
	$sql="SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE CycleID=".$cycleID." AND TemplateID=".$templateID." ORDER BY DateTimeInput DESC;";
	//echo $sql."<br/><br/>";
	$c=$connection->returnResultSet($sql);
	//echo "RecordID: ".$c[0]["RecordID"]."<br/>";
	
	$sql="SELECT CycleID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
			FROM INTRANET_PA_C_CYCLE WHERE CycleID=".$cycleID.";";
	$a=$connection->returnResultSet($sql);
	$sql="SELECT fs.TemplateID, fs.SecID, sr.AppRoleID, sr.CanFill, fs.DisplayOrder FROM INTRANET_PA_S_FRMSEC fs
			INNER JOIN INTRANET_PA_S_SECROL sr ON fs.SecID=sr.SecID
			WHERE fs.TemplateID=".$templateID." AND sr.CanFill=1 AND fs.ParentSecID IS NULL";
	$b=$connection->returnResultSet($sql);
	
	foreach($b as $idx=>$val){
		$IsActive = 1;	
		if($val['AppRoleID']==1){
			$obsIDArr=explode(",",$obsID);
			foreach($obsIDArr as $oID){
				$filler = $oID;
				$sql="INSERT INTO INTRANET_PA_T_FRMSUB(RlsNo,RecordID,BatchID,AppRoleID,EditPrdFr,EditPrdTo,SecID,IsActive,BatchOrder,FillerID,ObsID,IsLastSub,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT * FROM(SELECT 1 as  RlsNo,".$c[0]["RecordID"]." as RecordID,".$val['DisplayOrder']." as BatchID,".$val['AppRoleID']." as AppRoleID,'".$a[0]["CycleStart"]."' as EditPrdFr,'".$a[0]["CycleClose"]."' as EditPrdTo,".$b[$idx]["SecID"].",".$IsActive." as IsActive,";
				$sql.="".$val['DisplayOrder']." as BatchOrder,".$filler." as FillerID,'".$obsID."' as ObsID,0 as IsLastSub,";	
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
			}
		}else{
			$filler = $userID;
			if($val['AppRoleID']==2 && $idx != 0){
				$IsActive=0;
			}
			$sql="INSERT INTO INTRANET_PA_T_FRMSUB(RlsNo,RecordID,BatchID,AppRoleID,EditPrdFr,EditPrdTo,SecID,IsActive,BatchOrder,FillerID,ObsID,IsLastSub,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
			$sql.="SELECT * FROM(SELECT 1 as  RlsNo,".$c[0]["RecordID"]." as RecordID,".$val['DisplayOrder']." as BatchID,".$val['AppRoleID']." as AppRoleID,'".$a[0]["CycleStart"]."' as EditPrdFr,'".$a[0]["CycleClose"]."' as EditPrdTo,".$b[$idx]["SecID"].",".$IsActive." as IsActive,";
			$sql.="".$val['DisplayOrder']." as BatchOrder,".$filler." as FillerID,'".$obsID."' as ObsID,0 as IsLastSub,";	
			$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput, ";
			$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified) as a ";
			//echo $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
		}
		
	}	
}elseif($formType=="deleteObsFrm"){
	$cycleID = ($cycleID=="")?$_POST['cycleID']:$cycleID;
	$sql = "SELECT TemplateID, AprID FROM INTRANET_PA_T_FRMSUM WHERE RecordID='".$_POST['recordID']."'";
	$templateInfo = $connection->returnArray($sql);
	$templateID=$templateInfo[0]['TemplateID'];
	$userID=$templateInfo[0]['AprID'];
	$sql = "SELECT obsID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo='".$_POST['rlsNo']."' AND RecordID='".$_POST['recordID']."' AND BatchID='".$_POST['batchID']."'";
	$obsID = current($connection->returnVector($sql));
	$obsIDArr=explode(",",$obsID);
	
	if(count($obsIDArr) > 1){
		for($idx=0; $idx < count($obsIDArr); $idx++){
			if($obsIDArr[$idx] == $_SESSION['UserID']){
				unset($obsIDArr[$idx]);
			}
		}
		$obsID = implode(",",$obsIDArr);
		$sql = "UPDATE INTRANET_PA_T_FRMSUB SET ObsID='".$obsID."' WHERE RlsNo='".$_POST['rlsNo']."' AND RecordID='".$_POST['recordID']."'";
		$saveSuccess=$connection->db_db_query($sql);
	}else{
		$sql = "UPDATE INTRANET_PA_T_FRMSUB SET IsActive=0 WHERE RlsNo='".$_POST['rlsNo']."' AND RecordID='".$_POST['recordID']."'";
		$x=$connection->db_db_query($sql);
		if($x==false){
			$saveSuccess = false;
		}
		$sql = "UPDATE INTRANET_PA_T_FRMSUM SET IsActive=0 WHERE RecordID='".$_POST['recordID']."'";
		$x=$connection->db_db_query($sql);
		if($x==false){
			$saveSuccess = false;
		}
	}	
	
}



$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
//echo $returnPath;
header($returnPath."&returnMsgKey=".$returnMsgKey);




?>