<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

# Page Title

$curTab="viewList";
$TAGS_OBJ[] = array($Lang['Appraisal']['TitleBar']['FormToBeEdited'], 'javascript: goEditList(0);', $curTab=='editList');
$TAGS_OBJ[] = array($Lang['Appraisal']['TitleBar']['FormCompleted'], 'javascript: goViewList(0);', $curTab=='viewList');

$connection = new libgeneralsettings();
$currentDate = date("Y-m-d");
$a = $indexVar['libappraisal']->getAcademicYearTerm($currentDate);

if($indexVar['libappraisal']->isEJ()){
	$intranetUserTable = "INTRANET_USER_UTF8";
}else{
	$intranetUserTable = "INTRANET_USER";
}

// get Current Cycle Description
//$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".IntegerSafe($a[0]["AcademicYearID"]).";";
$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE CycleStart <= '".$currentDate."' AND CycleClose >= '".$currentDate."';";
$a = $connection->returnResultSet($sql);
$cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
$cycleDesc = Get_Lang_Selection($a[0]["DescrChi"],$a[0]["DescrEng"]);


# Page Title
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['ObservationPeriod']."</span></div>";;
//$TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();
echo $titleBar;

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$x = "";
$cycleMsg = ($cycleDesc!="")?sprintf($Lang['Appraisal']['ARObsCycleStart'] ,$cycleDesc):"";
//$x .= $cycleMsg."<br/><br/>"."\r\n";
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['ARMyObsFrm'])."<br/>\r\n";

$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("FormName","MyRole","FormFillAndOrder","SubDate","Action","DisplayOrder");
$li->field_array = array("ClassName","SubjectName","FormName","ObsName","LastEdit","Action","DisplayOrder");

$yearClassSql = ($indexVar['libappraisal']->isEJ())?"SELECT ClassID as YearClassID, ClassName as ClassTitleEN, ClassName as ClassTitleB5 FROM INTRANET_CLASS":"SELECT YearClassID,ClassTitleEN,ClassTitleB5 FROM YEAR_CLASS";
$subjectSql = ($indexVar['libappraisal']->isEJ())?"SELECT SubjectID, SubjectName as SubjectTitleB5,SubjectName as SubjectTitleEN FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ORDER BY SubjectID":"SELECT RecordID as SubjectID,CH_DES as SubjectTitleB5,EN_DES as SubjectTitleEN FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 and (CMP_CODEID is null || CMP_CODEID = '')";

$yearClassSqlJoin = ($indexVar['libappraisal']->isEJ())?" LEFT JOIN INTRANET_CLASS yc ON yc.ClassID=ihdr.YearClassID ":" LEFT JOIN YEAR_CLASS yc ON yc.YearClassID=ihdr.YearClassID ";
$yearClassSqlField = ($indexVar['libappraisal']->isEJ())?" ,yc.ClassName as ClassTitleB5,yc.ClassName as ClassTitleEN ":" ,yc.ClassTitleB5,yc.ClassTitleEN ";
$subjectSqlJoin = ($indexVar['libappraisal']->isEJ())?" LEFT JOIN INTRANET_SUBJECT asbj ON asbj.RecordStatus = 1 AND asbj.SubjectID=ihdr.SubjectID ":" LEFT JOIN ASSESSMENT_SUBJECT asbj ON asbj.RecordStatus = 1 AND (asbj.CMP_CODEID is null OR asbj.CMP_CODEID = '') AND asbj.RecordID=ihdr.SubjectID ";
$subjectSqlField = ($indexVar['libappraisal']->isEJ())?" ,asbj.SubjectName as SubjectTitleB5, asbj.SubjectName as SubjectTitleEN ":" ,asbj.CH_DES as SubjectTitleB5, asbj.EN_DES as SubjectTitleEN ";

$modifiedFrmSql = ($indexVar['libappraisal']->isEJ())?"cast(convert(".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." using  latin1) as binary)":$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng");

$sql = "
			SELECT ClassName,SubjectName,ObsID,FormTitle,LastEdit,Action,DisplayOrder
			FROM(
					SELECT ".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassName,".$indexVar['libappraisal']->getLangSQL("SubjectTitleB5","SubjectTitleEN")." as SubjectName,
					".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,	".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
					CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
					SubDate,
					CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) AND FillerID='".IntegerSafe($_SESSION['UserID'])."' THEN
						CONCAT('<a id=\"',summary.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',summary.RlsNo,',',summary.RecordID,',',summary.BatchID,',\'',summary.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a>')
					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL) AND FillerID='".IntegerSafe($_SESSION['UserID'])."'  THEN
						CONCAT('<a id=\"',summary.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',summary.RlsNo,',',summary.RecordID,',',summary.BatchID,',\'',summary.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a><br/>
							<span style=\"color:#FF0000\">(',summary.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',summary.ModDateTo,')</span>
							')
					ELSE
						CONCAT('<a id=\"',summary.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',summary.RlsNo,',',summary.RecordID,',',summary.BatchID,',\'',summary.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
					END
					as Action,DateTimeModified,RecordID,BatchID,DisplayOrder,TemplateID,LastEdit, $modifiedFrmSql as FormTitle,ObsID
					FROM(
						SELECT  frmsum.RecordID,CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi) as FrmTitleChi,CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng) as FrmTitleEng,
								frmsub.AppRoleID,NameChi,NameEng,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,
								DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,ihdr.DateTimeModified,MAX(frmsub.BatchID) as BatchID,frmsub.RlsNo,frmsub.FillerID,frmsub.ObsID,
								'SDF' as TemplateIDType,frmtpl.DisplayOrder,frmtpl.TemplateID,frmsub.SecID,ModDateFr,ModDateTo,ihdr.DateTimeModified as LastEdit $yearClassSqlField $subjectSqlField
						FROM INTRANET_PA_T_FRMSUM frmsum
						INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
						INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
						INNER JOIN INTRANET_PA_C_OBSSEL obssel ON frmsum.CycleID = obssel.CycleID AND frmsum.TemplateID = obssel.TemplateID AND obssel.IsActive = 1
						INNER JOIN INTRANET_PA_S_FRMSEC frmsec ON frmsum.TemplateID = frmsec.TemplateID AND frmsub.SecID=frmsec.SecID
						INNER JOIN INTRANET_PA_S_APPROL approl ON frmsub.AppRoleID = approl.AppRoleID
						LEFT JOIN INTRANET_PA_T_SLF_HDR ihdr ON ihdr.RecordID=frmsub.RecordID AND frmsub.RlsNo=ihdr.RlsNo AND ihdr.BatchID=frmsub.BatchID
						$yearClassSqlJoin
						$subjectSqlJoin
						WHERE frmtpl.IsObs=1
						AND frmsum.UserID='".IntegerSafe($_SESSION['UserID'])."'
						AND obssel.CycleID = '".$cycleID."' AND frmsub.IsActive=1 AND (frmsub.SubDate IS NOT NULL OR (EditPrdFr > NOW() OR EditPrdTo < NOW())) GROUP BY frmsub.RecordID
					) summary
				) summary_appraisee";
$ObsRecords = $li->returnArray($sql);
$sql = "DROP TEMPORARY TABLE IF EXISTS ObsRecordToSelf";
$li->db_db_query($sql);
$sql = "CREATE TEMPORARY TABLE ObsRecordToSelf(
			ObsRecordID int(8) NOT NULL AUTO_INCREMENT,
			ClassName TEXT,
			SubjectName TEXT,
			FormName TEXT,
			ObsName TEXT,
			LastEdit datetime,
			Action TEXT,
			DisplayOrder int(11) NOT NULL default '1',
			PRIMARY KEY(ObsRecordID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";	
$li->db_db_query($sql);
foreach($ObsRecords as $obs){
	$obsNameSql = $indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName");
	$sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field("",true)." as UserName FROM $intranetUserTable WHERE UserID IN (".$obs['ObsID'].")";
	$obsNames = implode("</br>",$li->returnVector($sql));
	$sql = "INSERT INTO ObsRecordToSelf (ClassName,SubjectName,FormName,LastEdit,Action,DisplayOrder,ObsName)
			VALUES ('".$obs['ClassName']."','".$obs['SubjectName']."','".$obs['FormTitle']."','".$obs['LastEdit']."','".$li->Get_Safe_Sql_Query($obs['Action'])."','".$obs['DisplayOrder']."','".$li->Get_Safe_Sql_Query(htmlspecialchars_decode($obsNames))."')";	
	$li->db_db_query($sql);
}
$li->sql  = "SELECT ClassName,SubjectName,FormName,ObsName,IF(LastEdit='00-00-00 0000:00:00', '--', LastEdit) as LastEdit,Action,DisplayOrder FROM ObsRecordToSelf ";										
/*
$li -> sql ="
		SELECT ClassName,SubjectName,Action,LastEdit,DisplayOrder
		FROM(
				SELECT ".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassName,".$indexVar['libappraisal']->getLangSQL("SubjectTitleB5","SubjectTitleEN")." as SubjectName,
				".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,	".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
				CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
				SubDate,
				CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">',$modifiedFrmSql,'</a>')
					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL) THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">',$modifiedFrmSql,'</a><br/>
							<span style=\"color:#FF0000\">(',a0.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',a0.ModDateTo,')</span>
							')
					ELSE
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">',$modifiedFrmSql,'</a>')
					END
				as Action,DateTimeModified,RecordID,BatchID,DisplayOrder,TemplateID,LastEdit
				FROM(
					SELECT yc.ClassTitleB5,yc.ClassTitleEN, asbj.SubjectTitleB5, asbj.SubjectTitleEN,iptf.RecordID,CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi,' - ',SecTitleChi) as FrmTitleChi,CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng,' - ',SecTitleEng) as FrmTitleEng,
					iptfFrmSub.AppRoleID,NameChi,NameEng,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,
					DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,DateTimeModified,iptfFrmSub.BatchID,iptfFrmSub.RlsNo,
					'SDF' as TemplateIDType,DisplayOrder,ipsf.TemplateID,iptfFrmSub.SecID,ModDateFr,ModDateTo,ihdr.LastEdit
					FROM(
						SELECT RecordID,CycleID,TemplateID,UserID,DateTimeModified,IsComp FROM INTRANET_PA_T_FRMSUM WHERE IsActive=1 AND CycleID=".$cycleID." AND UserID=".IntegerSafe($_SESSION['UserID'])."
					) as iptf
					INNER JOIN(
						SELECT RlsNo,RecordID,BatchID,FillerID,SubDate,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as ModDateTo,2 as AppRoleID,SecID,EditPrdFr,EditPrdTo
								FROM INTRANET_PA_T_FRMSUB WHERE IsActive=1 AND (SubDate IS NOT NULL OR (SubDate IS NULL AND AppRoleID=2))
					) as iptfFrmSub ON iptf.RecordID=iptfFrmSub.RecordID
					INNER JOIN(
						SELECT CycleID,TemplateID,IsActive FROM INTRANET_PA_C_OBSSEL WHERE IsActive=1 AND CycleID=".$cycleID."
					) as ipcobs ON iptf.CycleID=ipcobs.CycleID AND iptf.TemplateID=ipcobs.TemplateID
					LEFT JOIN(
						SELECT TemplateID,TemplateType,COALESCE(FrmTitleChi,'') as FrmTitleChi,COALESCE(FrmTitleEng,'') as FrmTitleEng,
						COALESCE(FrmCodChi,'') as FrmCodChi,COALESCE(FrmCodEng,'') as FrmCodEng,COALESCE(ObjChi,'') as ObjChi,COALESCE(ObjEng,'') as ObjEng,DisplayOrder FROM INTRANET_PA_S_FRMTPL
					) as ipsf ON iptf.TemplateID=ipsf.TemplateID
					LEFT JOIN(
						SELECT AppRoleID,NameChi,NameEng FROM INTRANET_PA_S_APPROL
					) as ipsa ON iptfFrmSub.AppRoleID=ipsa.AppRoleID
					LEFT JOIN(
						SELECT TemplateID as FrmSecTemplateID,SecID,COALESCE(SecTitleChi,'') as SecTitleChi,COALESCE(SecTitleEng,'') as SecTitleEng FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID IS NULL
					) as ipsFrmsec ON iptf.TemplateID=ipsFrmsec.FrmSecTemplateID AND iptfFrmSub.SecID=ipsFrmsec.SecID
					LEFT  JOIN (
						SELECT YearClassID, SubjectID,RecordID,RlsNo,BatchID, DateTimeModified as LastEdit FROM INTRANET_PA_T_SLF_HDR
					) ihdr ON  ihdr.RecordID=iptf.RecordID AND iptfFrmSub.RlsNo=ihdr.RlsNo AND ihdr.BatchID=iptfFrmSub.BatchID
					LEFT JOIN (
						$yearClassSql
					) yc ON  yc.YearClassID=ihdr.YearClassID
					LEFT JOIN (
						$subjectSql
					) asbj ON  asbj.SubjectID=ihdr.SubjectID
				) as a0
		) as a";*/
//echo htmlspecialchars($li->sql)."<br/><br/>";

$li->no_col = sizeof($li->field_array);
//$li->fieldorder2 = ", FormFillAndOrder";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
$li->column_array = array(23,23,0,0,0,0);
$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
/*$li->column_list .= "<th width='50%' >".$li->column($pos++, $Lang['Appraisal']['ARForm'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormMyRole'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['ARFormFormFillAndOrder'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ObsFormSubDate'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormAction'])."</th>\n";
*/
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormYearClass'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormSubject'])."</th>\n";
$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']['FrmTitle'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ObsFormFiller'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']["ModifiedDate"])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['ARFormAction'])."</th>\n";
$htmlAry['dataTable1'] = $li->display();

$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);

$o_order = ($o_order == '') ? 1 : $o_order;	// 1 => asc, 0 => desc
$o_field = ($o_field == '') ? 7 : $o_field;
$o_pageNo = ($o_pageNo == '') ? 1 : $o_pageNo;
$o_page_size = ($o_numPerPage == '') ? 10 : $o_numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);

$li->pageNo = $o_pageNo;
$li->order = $o_order;
$li->field = $o_field;
$li->page_size = $o_page_size;
$li->pageNo_name = 'o_pageNo';
$li->numPerPage_name = 'o_numPerPage';

$y .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['AROthObsFrm'])."<br/>\r\n";

if ($searchType == 'byID'){
    $default_search_value = $search_stfname;
    $filter_search = "WHERE UserID = ". $search_stfid ."";
} else if ($searchType == 'byInputName') {
    $filter_search = "WHERE UserName LIKE '%". $searchInput ."%'";
    $default_search_value = $searchInput;
} else {
}

$userNameSQL = $indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName");
$filter_deleted = "";
if(!$sys_custom['eAppraisal']['showArchieveTeacher']){
	$filter_deleted = " AND auser.UserLogin IS NULL ";
}
if($indexVar['libappraisal']->isEJ()){
	$coreSQL = "	SELECT
                        iu.UserID as UserID,
						iu.ChineseName, iu.EnglishName, iu.RecordStatus, iu.TitleChinese, iu.TitleEnglish,iu.Teaching,iu.RecordType,iu.Title,
						yc.ClassName as ClassTitleB5,yc.ClassName as ClassTitleEN, 
						asbj.SubjectName as SubjectTitleB5, asbj.SubjectName as SubjectTitleEN,
						iptf.RecordID,
						CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi,' - ',SecTitleChi) as FrmTitleChi,
						CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng,' - ',SecTitleEng) as FrmTitleEng,
						iptfFrmSub.AppRoleID,NameChi,NameEng,
						DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,
						DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,
						iptf.DateTimeModified,iptfFrmSub.BatchID,iptfFrmSub.RlsNo,iptfFrmSub.FillerID,
						'SDF' as TemplateIDType,ipsf.DisplayOrder,ipsf.TemplateID,
						iptfFrmSub.SecID,ModDateFr,ModDateTo,ihdr.DateTimeModified as LastEdit,iptfFrmSub.ObsID
					FROM INTRANET_PA_T_FRMSUB iptfFrmSub
					INNER JOIN INTRANET_PA_T_FRMSUM iptf ON iptf.RecordID=iptfFrmSub.RecordID 
					INNER JOIN INTRANET_PA_S_APPROL ipsa ON iptfFrmSub.AppRoleID=ipsa.AppRoleID 
					INNER JOIN INTRANET_PA_S_FRMTPL ipsf ON iptf.TemplateID=ipsf.TemplateID 
					INNER JOIN INTRANET_PA_S_FRMSEC ipsFrmsec ON iptf.TemplateID=ipsFrmsec.TemplateID AND iptfFrmSub.SecID=ipsFrmsec.SecID 
					INNER JOIN INTRANET_PA_C_OBSSEL ipcobs ON iptf.CycleID=ipcobs.CycleID AND ipsf.TemplateID=ipcobs.TemplateID AND ipcobs.IsActive=1
					INNER JOIN ".$appraisalConfig['INTRANET_USER']." iu ON  iu.UserID=iptf.UserID
					LEFT JOIN INTRANET_PA_T_SLF_HDR ihdr ON  ihdr.RecordID=iptf.RecordID AND iptfFrmSub.RlsNo=ihdr.RlsNo AND ihdr.BatchID=iptfFrmSub.BatchID
					LEFT JOIN INTRANET_CLASS yc ON yc.ClassID=ihdr.YearClassID 
					LEFT JOIN INTRANET_SUBJECT asbj ON asbj.SubjectID=ihdr.SubjectID AND asbj.RecordStatus=1
					LEFT JOIN INTRANET_ARCHIVE_USER auser ON iu.UserID = auser.UserID
					WHERE iptfFrmSub.IsActive=1
					AND (iptf.UserID <> '".IntegerSafe($_SESSION['UserID'])."' AND iptfFrmSub.AppRoleID <> '2')
					AND (ObsID like '".IntegerSafe($_SESSION['UserID']).",%' OR ObsID LIKE '%,".IntegerSafe($_SESSION['UserID']).",%' OR ObsID LIKE '%,".IntegerSafe($_SESSION['UserID'])."' OR ObsID='".IntegerSafe($_SESSION['UserID'])."')
					AND iptf.CycleID='".IntegerSafe($cycleID)."' $filter_deleted ";
}else{
	$coreSQL = "SELECT
                    iu.UserID as UserID,
					iu.ChineseName, iu.EnglishName, iu.RecordStatus, iu.TitleChinese, iu.TitleEnglish, iu.Teaching,iu.RecordType,iu.Title,
					yc.YearClassID,yc.ClassTitleEN,yc.ClassTitleB5,
					asbj.RecordID as SubjectID,asbj.CH_DES as SubjectTitleB5,asbj.EN_DES as SubjectTitleEN,
					iptf.RecordID, 
					CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi,' - ',SecTitleChi) as FrmTitleChi,
					CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng,' - ',SecTitleEng) as FrmTitleEng,
					iptfFrmSub.AppRoleID,NameChi,NameEng,
					DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,
					DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,
					iptf.DateTimeModified,iptfFrmSub.BatchID,iptfFrmSub.RlsNo,iptfFrmSub.FillerID,
					'SDF' as TemplateIDType,ipsf.DisplayOrder,ipsf.TemplateID,
					iptfFrmSub.SecID,ModDateFr,ModDateTo,ihdr.DateTimeModified as LastEdit,iptfFrmSub.ObsID
				FROM INTRANET_PA_T_FRMSUB iptfFrmSub
				INNER JOIN INTRANET_PA_T_FRMSUM iptf ON iptf.RecordID=iptfFrmSub.RecordID 
				INNER JOIN INTRANET_PA_S_APPROL ipsa ON iptfFrmSub.AppRoleID=ipsa.AppRoleID 
				INNER JOIN INTRANET_PA_S_FRMTPL ipsf ON iptf.TemplateID=ipsf.TemplateID 
				INNER JOIN INTRANET_PA_S_FRMSEC ipsFrmsec ON iptf.TemplateID=ipsFrmsec.TemplateID AND iptfFrmSub.SecID=ipsFrmsec.SecID 
				INNER JOIN INTRANET_PA_C_OBSSEL ipcobs ON iptf.CycleID=ipcobs.CycleID AND ipsf.TemplateID=ipcobs.TemplateID AND ipcobs.IsActive=1
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." iu ON  iu.UserID=iptf.UserID
				LEFT JOIN INTRANET_PA_T_SLF_HDR ihdr ON  ihdr.RecordID=iptf.RecordID AND iptfFrmSub.RlsNo=ihdr.RlsNo AND ihdr.BatchID=iptfFrmSub.BatchID
				LEFT JOIN YEAR_CLASS yc ON yc.YearClassID=ihdr.YearClassID 
				LEFT JOIN ASSESSMENT_SUBJECT asbj ON asbj.RecordID=ihdr.SubjectID AND asbj.RecordStatus = 1 and (asbj.CMP_CODEID is null || asbj.CMP_CODEID = '')
				LEFT JOIN INTRANET_ARCHIVE_USER auser ON iu.UserID = auser.UserID
				WHERE iptfFrmSub.IsActive=1
				AND (iptf.UserID <> '".IntegerSafe($_SESSION['UserID'])."' AND iptfFrmSub.AppRoleID <> '2')
				AND (ObsID like '".IntegerSafe($_SESSION['UserID']).",%' OR ObsID LIKE '%,".IntegerSafe($_SESSION['UserID']).",%' OR ObsID LIKE '%,".IntegerSafe($_SESSION['UserID'])."' OR ObsID='".IntegerSafe($_SESSION['UserID'])."')
				AND iptf.CycleID='".IntegerSafe($cycleID)."' $filter_deleted ";
}

$sql ="
		SELECT UserName,UserID,ClassName,SubjectName,FormName,LastEdit,Action,DisplayOrder,ObsID,FormFillAndOrder
		FROM(		
				SELECT ".$indexVar['libappraisal']->Get_Name_Field()." as UserName,UserID,".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassName,".$indexVar['libappraisal']->getLangSQL("SubjectTitleB5","SubjectTitleEN")." as SubjectName,
				".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
				CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
				SubDate,CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a>')
					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL)  THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a><br/>
							<span style=\"color:#FF0000\">(',a0.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',a0.ModDateTo,')</span>
							')
					ELSE
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
					END
				as Action,DateTimeModified,RecordID,BatchID,DisplayOrder,TemplateID,LastEdit,ObsID
				FROM(
					$coreSQL
				) as a0 WHERE SubDate IS NOT NULL OR (DATE_FORMAT(NOW(),'%Y-%m-%d') < EditPrdFr OR DATE_FORMAT(NOW(),'%Y-%m-%d') > EditPrdTo)
		) as a
        $filter_search";
		
/*
$sql ="
		SELECT UserName,ClassName,SubjectName,FormName,LastEdit,Action,DisplayOrder,ObsID,FormFillAndOrder
		FROM(		
				SELECT ".$indexVar['libappraisal']->Get_Name_Field()." as UserName,".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassName,".$indexVar['libappraisal']->getLangSQL("SubjectTitleB5","SubjectTitleEN")." as SubjectName,
				".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
				CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
				SubDate,CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a>')
					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL) THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a><br/>
							<span style=\"color:#FF0000\">(',a0.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',a0.ModDateTo,')</span>
							')
					ELSE
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
					END
				as Action,DateTimeModified,RecordID,BatchID,DisplayOrder,TemplateID,LastEdit,ObsID
				FROM(
					SELECT 
					iu.ChineseName, iu.EnglishName, iu.RecordStatus, iu.TitleChinese, iu.TitleEnglish,
					yc.ClassTitleB5,yc.ClassTitleEN, asbj.SubjectTitleB5, asbj.SubjectTitleEN, iptf.RecordID,CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi,' - ',SecTitleChi) as FrmTitleChi,CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng,' - ',SecTitleEng) as FrmTitleEng,
					iptfFrmSub.AppRoleID,NameChi,NameEng,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,
					DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,DateTimeModified,iptfFrmSub.BatchID,iptfFrmSub.RlsNo,
					'SDF' as TemplateIDType,DisplayOrder,ipsf.TemplateID,iptfFrmSub.SecID,ModDateFr,ModDateTo,ihdr.LastEdit,iptfFrmSub.ObsID
					FROM(
						SELECT RecordID,CycleID,TemplateID,UserID,DateTimeModified FROM INTRANET_PA_T_FRMSUM WHERE IsActive=1 AND CycleID=".$cycleID."
					) as iptf
					INNER JOIN(
						SELECT RlsNo,RecordID,BatchID,FillerID,SubDate,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as ModDateTo,AppRoleID,SecID,EditPrdFr,EditPrdTo,ObsID
								FROM INTRANET_PA_T_FRMSUB WHERE (ObsID like '".IntegerSafe($_SESSION['UserID']).",%' OR ObsID LIKE '%,".IntegerSafe($_SESSION['UserID']).",%' OR ObsID LIKE '%,".IntegerSafe($_SESSION['UserID'])."' OR ObsID='".IntegerSafe($_SESSION['UserID'])."') AND IsActive=1
					) as iptfFrmSub ON iptf.RecordID=iptfFrmSub.RecordID
					INNER JOIN(
						SELECT CycleID,TemplateID,IsActive FROM INTRANET_PA_C_OBSSEL WHERE IsActive=1 AND CycleID=".$cycleID."
					) as ipcobs ON iptf.CycleID=ipcobs.CycleID AND iptf.TemplateID=ipcobs.TemplateID
					INNER JOIN (
						SELECT UserID, EnglishName, ChineseName, RecordStatus, TitleChinese, TitleEnglish FROM $intranetUserTable
					) iu ON  iu.UserID=iptf.UserID	
					LEFT JOIN(
						SELECT TemplateID,TemplateType,COALESCE(FrmTitleChi,'') as FrmTitleChi,COALESCE(FrmTitleEng,'') as FrmTitleEng,
						COALESCE(FrmCodChi,'') as FrmCodChi,COALESCE(FrmCodEng,'') as FrmCodEng,COALESCE(ObjChi,'') as ObjChi,COALESCE(ObjEng,'') as ObjEng,DisplayOrder FROM INTRANET_PA_S_FRMTPL
					) as ipsf ON iptf.TemplateID=ipsf.TemplateID
					LEFT JOIN(
						SELECT AppRoleID,NameChi,NameEng FROM INTRANET_PA_S_APPROL
					) as ipsa ON iptfFrmSub.AppRoleID=ipsa.AppRoleID
					LEFT JOIN(
						SELECT TemplateID as FrmSecTemplateID,SecID,COALESCE(SecTitleChi,'') as SecTitleChi,COALESCE(SecTitleEng,'') as SecTitleEng FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID IS NULL
					) as ipsFrmsec ON iptf.TemplateID=ipsFrmsec.FrmSecTemplateID AND iptfFrmSub.SecID=ipsFrmsec.SecID
					LEFT  JOIN (
						SELECT YearClassID, SubjectID,RecordID,RlsNo,BatchID, DateTimeModified as LastEdit FROM INTRANET_PA_T_SLF_HDR
					) ihdr ON  ihdr.RecordID=iptf.RecordID AND iptfFrmSub.RlsNo=ihdr.RlsNo AND ihdr.BatchID=iptfFrmSub.BatchID				
					LEFT JOIN (
						$yearClassSql
					) yc ON  yc.YearClassID=ihdr.YearClassID
					LEFT JOIN (
						$subjectSql
					) asbj ON  asbj.SubjectID=ihdr.SubjectID
				) as a0
		) as a";		*/
$ObsRecords = $li->returnArray($sql);

$sql = "DROP TEMPORARY TABLE IF EXISTS ObsRecordToOthers";
$li->db_db_query($sql);
$sql = "CREATE TEMPORARY TABLE ObsRecordToOthers(
			ObsRecordID int(8) NOT NULL AUTO_INCREMENT,
			UserName TEXT,
            UserID TEXT,
			ClassName TEXT,
			SubjectName TEXT,
			FormName TEXT,
			ObsName TEXT,
			LastEdit datetime,
			Action TEXT,
			DisplayOrder int(11) NOT NULL default '1',
			FormFillAndOrder TEXT,
			PRIMARY KEY(ObsRecordID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";	
$li->db_db_query($sql);
foreach($ObsRecords as $obs){
	$obsNameSql = $indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName");
	$sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field("",true)." as UserName FROM $intranetUserTable WHERE UserID IN (".$obs['ObsID'].")";
	$obsNames = implode("</br>",$li->returnVector($sql));
	$sql = "INSERT INTO ObsRecordToOthers (UserName,UserID,ClassName,SubjectName,FormName,LastEdit,Action,DisplayOrder,ObsName,FormFillAndOrder)
			VALUES ('".$obs['UserName']."','".$obs['UserID']."','".$obs['ClassName']."','".$obs['SubjectName']."','".$obs['FormName']."','".$obs['LastEdit']."','".$li->Get_Safe_Sql_Query($obs['Action'])."','".$obs['DisplayOrder']."','".$li->Get_Safe_Sql_Query(htmlspecialchars_decode($obsNames))."','".$obs['FormFillAndOrder']."')";	
	$li->db_db_query($sql);
}
$li->sql = "SELECT UserName,ClassName,SubjectName,FormName,ObsName,IF(LastEdit='00-00-00 0000:00:00', '--', LastEdit) as LastEdit,Action,DisplayOrder,UserID FROM ObsRecordToOthers";
//echo htmlspecialchars($li->sql)."<br/><br/>";
//$li->field_array = array("FormName","MyRole","FormFillAndOrder","SubDate","Action","DisplayOrder");
$li->field_array = array("UserName","ClassName","SubjectName","FormName","ObsName","LastEdit","Action","DisplayOrder");
$li->no_col = sizeof($li->field_array);
$li->fieldorder2 = ", FormFillAndOrder";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
if($indexVar['libappraisal']->isEJ()){
	$li->column_array = array(0,23,23,0,5,0,0);
}else{
	$li->column_array = array(0,23,23,0,0,0,0);
}

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";/*
$li->column_list .= "<th width='50%' >".$li->column($pos++, $Lang['Appraisal']['ARForm'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormMyRole'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['ARFormFormFillAndOrder'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ObsFormSubDate'])."</th>\n";
*/
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormName'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormYearClass'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormSubject'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']['FrmTitle'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ObsFormFiller'])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['TemplateSample']["ModifiedDate"])."</th>\n";
$li->column_list .= "<th width='20%' >".$li->column($pos++, $Lang['Appraisal']['ARFormAction'])."</th>\n";

$htmlAry['dataTable2'] = $li->display();

$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('o_pageNo', 'o_pageNo', $li->pageNo);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('o_order', 'o_order', $li->order);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('o_field', 'o_field', $li->field);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('o_numPerPage', 'o_numPerPage', $li->page_size);

//***** Build search suggestion bar Start *****//
//Get UserName and UserID
$sql2 ="
		SELECT UserName,UserID
		FROM(
				SELECT ".$indexVar['libappraisal']->Get_Name_Field()." as UserName,UserID,".$indexVar['libappraisal']->getLangSQL("ClassTitleB5","ClassTitleEN")." as ClassName,".$indexVar['libappraisal']->getLangSQL("SubjectTitleB5","SubjectTitleEN")." as SubjectName,
				".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
				CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
				SubDate,CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a>')
					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL)  THEN
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a><br/>
							<span style=\"color:#FF0000\">(',a0.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',a0.ModDateTo,')</span>
							')
					ELSE
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
					END
				as Action,DateTimeModified,RecordID,BatchID,DisplayOrder,TemplateID,LastEdit,ObsID
				FROM(
					$coreSQL
				) as a0 WHERE SubDate IS NOT NULL OR (DATE_FORMAT(NOW(),'%Y-%m-%d') < EditPrdFr OR DATE_FORMAT(NOW(),'%Y-%m-%d') > EditPrdTo)
		) as a";


$sql_ARFormStfOwnerName=$connection->returnResultSet($sql2);
$ARFormStfOwnerNameList = array(array());

//Insert both Staff Name and Staff ID into 2D array $ARFormStfOwnerNameList
for($i=0; $i<sizeof($sql_ARFormStfOwnerName); $i++){
    $ARFormStfOwnerNameList[$i]['UserName'] = $sql_ARFormStfOwnerName[$i]['UserName'];
    $ARFormStfOwnerNameList[$i]['UserID'] = $sql_ARFormStfOwnerName[$i]['UserID'];
}

//Remove the values in array $ARFormStfOwnerNameList with same Staff ID
$ARFormStfOwnerNameList = array_values(array_intersect_key( $ARFormStfOwnerNameList , array_unique( array_map('serialize' , $ARFormStfOwnerNameList ) ) ));

//***** Build search suggestion bar END *****//

$htmlAry['contentTbl1'] = $x;
$htmlAry['contentTbl2'] = $y;

//$subBtnAry = array();
//$btnAry[] = array('new', 'javascript: goAdd();', $Lang['Appraisal']['BtbAddObsFrm'], $subBtnAry);
//$htmlAry['contentTool'] = "<div width=\"100%\"><div width=\"80%\" style=\"display:inline-block\">".$indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry)."</div><div id=\"msgDiv\" class=\"systemmsg\" style=\"display:none;float:right;\" width=\"20%\"><span id=\"msg\"></span></div></div>";


?>