<?php
// ============================== Related Tables ==============================
//
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================

$navigationAry[] = array($Lang['Appraisal']['ObservationRecord'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Appraisal']['ObservationRecordAdd']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ObservationRecord']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
$connection = new libgeneralsettings();
//======================================================================== 選擇觀課報表 ========================================================================//
$currentDate = date("Y-m-d 00:00:00");
//$a = $indexVar['libappraisal']->getAcademicYearTerm($currentDate);

$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE CycleStart <= '".$currentDate."' AND CycleClose >= '".$currentDate."' ";
$a = $connection->returnResultSet($sql);
$cycleID=$a[0]["CycleID"];

$sql="SELECT ipsf.TemplateID,FrmInfoChi,FrmInfoEng
	FROM(
		SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1
	) as ipsf
	INNER JOIN(	
		SELECT TemplateID,CycleID,IsActive FROM INTRANET_PA_C_OBSSEL WHERE IsActive=1 AND CycleID=".$cycleID." 	
	) as ipcobs ON ipsf.TemplateID=ipcobs.TemplateID;";
//echo $sql."<br/>";
$a=$connection->returnResultSet($sql);
for($i=0;$i<sizeof($a);$i++){
	if($a[$i]["TemplateID"]==$templateID){
		$idx = $i;
		$templateDescr = Get_Lang_Selection($a[$i]["FrmInfoChi"],$a[$i]["FrmInfoEng"]);
	}
}
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['CycleTemplate']['AcceptApprsialForm'])."\r\n";
$x .= "<table class=\"form_table_v30\">";
$x .= "<tr><td class=\"field_title\">".$Lang['Appraisal']['CycleTemplate']['ApprsialForm']."</td>";
$x .= "<td>";
if($templateID=="" ||$templateID=="undefined"){
	$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($a, "TemplateID","FrmInfoChi","FrmInfoEng"), $selectionTags='id="DDLTemplateID" name="DDLTemplateID"', $SelectedType=$a[$idx]["TemplateID"], $all=0, $noFirst=0);
}
else{
	$x .= "<span>".$templateDescr."</span>";
}
$x .= "</td>";
$x .= "</tr></table><br/>";

//======================================================================== 編輯被評者 ========================================================================//
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['ObsRcd']['AddAppraisee'])."\r\n";
$x .= "<div>";
$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
$x .= "<tr>";
$x .= "<td style=\"width:50%;\">";
//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"appraiseeUserID\" name=\"appraiseeUserID\" value=\"\">";
$x .= $indexVar['libappraisal']->Get_Staff_Selection("AppraiseeSel", "AppraiseeSel", $ParIndividual=true,$ParIsMultiple=false,$ParSize=20,$ParSelectedValue="",$ParOthers="",$ParAllStaffOption=false,$ShowOptGroupLabel=true,$LoginIDAsValue=false,$Currentonly=true,$ParEmptyFirst=true);	
$x .= "<span id=\"LoadingPlace\"></span>";
$x .= "</td>";
$x .= "<td style=\"width:50%;\">";
$x .= "<span id=\"appraiseeUserName\" name=\"appraiseeUserName\">".$indexVar['libappraisal']->getUserNameID($a[0]["UserID"],"name")."</span>";
$x .= "<input type=\"hidden\" id=\"appraiseeIDStr\" name=\"appraiseeIDStr\" value=".$a[0]["UserID"].">"."\r\n";
$divDisplay=($indexVar['libappraisal']->getUserNameID($a[0]["UserID"],"login")!="")?"block":"none";
$x .= "<div style=\"float:right;display:".$divDisplay."\" class=\"table_row_tool\" id='deleteAppraisee' ><a href=\"javascript:void(0)\" class=\"delete_dim\" onclick=javascript:goDeleteAppraisee() title=\"".$Lang['SysMgr']['FormClassMapping']['Delete']."\"></a></div>";
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "</div><br/>";

//======================================================================== 編輯考績者 ========================================================================//
$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['ObsRcd']['AddAppraiser'])."\r\n";
$x .= "<div>";
$x .= "<table class=\"common_table_list ClassDragAndDrop\">";
$x .= "<tr>";
$x .= "<td style=\"width:50%;\">";
//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"MemberUserID\" name=\"MemberUserID\" value=\"\">";
$x .= $Lang['Appraisal']['SearchByLoginID']." ".$indexVar['libappraisal_ui']->GET_BTN($Lang['Btn']['Select'], "button", "goSelect()", 'selectBtn');;
$x .= "</td>";
$x .= "<td style=\"width:50%;\">";
$x .= "<select name=\"appraiserID\" id=\"appraiserID\" class=\"select_studentlist\" size=\"15\" multiple=\"multiple\">";
$x .= "<option value='U".$_SESSION['UserID']."'>";
if($indexVar['libappraisal']->isEJ()){
	$sql = "SELECT ChineseName, EnglishName FROM INTRANET_USER_UTF8 WHERE UserID='".$_SESSION['UserID']."'";
	$userName = current($indexVar['libappraisal']->returnArray($sql));
	$x .= Get_Lang_Selection($userName['ChineseName'],$userName['EnglishName']);
}else{
	$x .= Get_Lang_Selection($_SESSION['ChineseName'],$_SESSION['EnglishName']);
}
$x .= "</option>";
$x .= "</select>";
$x .= $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['RemoveSelected'], "button", "goDeleteAppraiser()", 'submitBtn');
$x .= "</td>";
$x .= "</tr>";
$x .= "</table>";
$x .= "</div><br/>";


$x .= "<input type=\"hidden\" id=\"appraiserIDStr\" name=\"appraiserIDStr\" value='".$_SESSION['UserID'].",'>"."\r\n";
$x .= "<input type=\"hidden\" id=\"CycleID\" name=\"CycleID\" value=".$cycleID.">"."\r\n";

$htmlAry['contentTbl'] = $x;

// ============================== Define Button ==============================
$htmlAry['saveAsBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtbSaveAsObs'], "button", "goSaveAs()", 'saveAsBtn');
$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnSave'], "button", "goSubmit()", 'submitBtn');
//$htmlAry['resetBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnReset'], "button", "goReset()", 'resetBtn');
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBackCycle(".$cycleID.")", 'backBtn');
// ============================== Define Button ==============================



?>