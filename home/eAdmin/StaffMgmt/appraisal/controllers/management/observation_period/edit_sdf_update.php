<?php 
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$connection = new libgeneralsettings();
$json = new JSON_obj();

$rlsNo=$_POST["rlsNo"];
$recordID=$_POST["recordID"];
$batchID=$_POST["batchID"];
$templateID=$_POST["templateID"];
$qIDStr=$_POST["qID"];
$qIDs=explode(",",$qIDStr);
$type=$_POST["type"];
$returnPath=$_POST["returnPath"];
$userID=$_POST["userID"];

//echo $qID;

// log submission	
switch($type){
	case "formSave":
		$logType = "save as draft";
		break;
	case "formSignature":
		$logType = "sign & submitted";
		break;
	case "modFormSignature":
		$logType = "Modification sign & submitted";
		break;
	case "modifySave":
		$logType = "Open to modify";
		break;
}		
$logData = array();
$recordDetail = array();
$logData['RlsNo'] = $rlsNo;
$logData['RecordID'] = $recordID;
$logData['BatchID'] = $batchID;
$logData['Function'] = 'Academic Affairs Form Submission';
$logData['RecordType'] = $logType;						  
$recordDetail[] = array(
						"Parameter"=>"TemplateID",
					  	"DisplayName"=>$Lang['Appraisal']['CycleTemplate']['ApprsialForm'],
					  	"Original"=>$templateID,
					  	"MapWithTable"=>"INTRANET_PA_S_FRMTPL",
					  	"MapDisplay"=>"FrmTitleEng"
					  );					  					
$logData['RecordDetail'] = $json->encode($recordDetail);
$result = $indexVar['libappraisal']->addFormFillingLog($logData);

if($type=="formSave" || $type=="formSignature" || $type=="modFormSignature"){
// no matter this is temp save or sign the signature, it will run the Save function first
	//======================================================================== INTRANET_PA_T_SLF_HDR ========================================================================//
	$sql="SELECT * FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
	$a=$connection->returnResultSet($sql);
	if(sizeof($a)>0){
		$yearClassID=($_POST["yearClassID"]!=null)?$_POST["yearClassID"]:"NULL";
		$subjectID=($_POST["subjectID"]!=null)?$_POST["subjectID"]:"NULL";
		$teachLang=($_POST["teachLang"]!=null)?$_POST["teachLang"]:"";
		$sql="UPDATE INTRANET_PA_T_SLF_HDR SET YearClassID=".$yearClassID.",SubjectID=".$subjectID.",TeachLang='".$teachLang."',";
		$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
		$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
		//echo $sql."<br/><br/>";
		$x = $connection->db_db_query($sql);
	}
	else{
		$yearClassID=($_POST["yearClassID"]!=null)?$_POST["yearClassID"]:"NULL";
		$subjectID=($_POST["subjectID"]!=null)?$_POST["subjectID"]:"NULL";
		$teachLang=($_POST["teachLang"]!=null)?$_POST["teachLang"]:"";
		$sql="INSERT INTO INTRANET_PA_T_SLF_HDR(RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
		$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,";
		$sql.=$yearClassID." as YearClassID,".$subjectID." as SubjectID,'".$teachLang."' as TeachLang,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
		$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
		//echo $sql."<br/><br/>";
		$x = $connection->db_db_query($sql);		
	}
	//======================================================================== INTRANET_PA_T_SLF_DTL ========================================================================//
	for($i=0;$i<sizeof($qIDs);$i++){		
		$sql="SELECT QID,IsMC,HasRmk,QCatID,IsScore,IsTxtAns,InputType FROM INTRANET_PA_S_QUES WHERE QID=".IntegerSafe($qIDs[$i]).";";
		//echo $sql."<br/><br/>";
		$x=$connection->returnResultSet($sql);
		//echo $sql."<br/><br/>";
		$sql="SELECT QCatID,QuesDispMode FROM INTRANET_PA_S_QCAT WHERE QCatID=".IntegerSafe($x[0]["QCatID"]).";";
		$y=$connection->returnResultSet($sql);
		//echo $sql."<br/><br/>";
		if($y[0]["QuesDispMode"]==0 || $y[0]["QuesDispMode"]==1){
			$sql="SELECT * FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i]).";";
			$a=$connection->returnResultSet($sql);
			$isQuestionRecordExists = (sizeof($a)>0)?true:false;
			if($y[0]["QuesDispMode"]==1){
				$MarkID=($_POST["qID_".$qIDs[$i]]!=null)?$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qID_".$qIDs[$i]]):"NULL";
				$AnsID="NULL";
				$TxtAnsID=($_POST["qIDTxtAns_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDTxtAns_".$qIDs[$i]])."'":"NULL";
				$Score=($_POST["qIDScore_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDScore_".$qIDs[$i]])."'":"NULL";
				$DateAns=($_POST["qIDDate_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDDate_".$qIDs[$i]])."'":"NULL";
			}
			else if($y[0]["QuesDispMode"]==0){
				$MarkID="NULL";
				$AnsID=($x[0]["InputType"]==1 && $_POST["qID_".$qIDs[$i]]!=null)?$_POST["qID_".$qIDs[$i]]:"NULL";
				$TxtAnsID=($x[0]["InputType"]==2 && $_POST["qIDTxtAns_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDTxtAns_".$qIDs[$i]])."'":"NULL";
				$Score=($x[0]["InputType"]==3 && $_POST["qIDScore_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDScore_".$qIDs[$i]])."'":"NULL";
				$DateAns=($x[0]["InputType"]==4 && $_POST["qIDDate_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDDate_".$qIDs[$i]])."'":"NULL";				
				if($x[0]["InputType"]==6){
					$mcmAns = "";
					for($qq=0;$qq<sizeof($_POST["qID_".$qIDs[$i]]);$qq++){
						if($qq == sizeof($_POST["qID_".$qIDs[$i]])-1){
							$mcmAns .= $_POST["qID_".$qIDs[$i]][$qq];
						}
						else{
							$mcmAns .= $_POST["qID_".$qIDs[$i]][$qq].",";
						}
					}
					$TxtAnsID = ($mcmAns!="")?"'".$mcmAns."'":"NULL";
				}
				else if($x[0]["InputType"]==7){
					$file_name = $_FILES["qID_".$qIDs[$i]]["name"];					
					$tmp_fileName = $_FILES["qID_".$qIDs[$i]]["tmp_name"];
					$limport = new libimporttext();
					$lo = new libfilesystem();
					
					### move to temp folder first for others validation
					$folder_prefix = $appraisalConfig['filePath']."/sdf_file/".$batchID;
					if (!file_exists($folder_prefix))
						$lo->folder_new($folder_prefix);
					$TargetFileName = "qID_".$qIDs[$i]."|".$_SESSION['UserID']."|".$file_name;
					$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);
					$SuccessArr['MoveFileToTempFolder'] = $lo->lfs_move($tmp_fileName, $TargetFilePath);
					
					//$TxtAnsID = ($file_name!=NULL)?"'".$TargetFileName."'":$_POST["hdQID_".$qIDs[$i]];			
					if($file_name!=NULL){
						$TxtAnsID="'".addslashes($TargetFileName)."'";
					}
					else if($file_name==NULL && $_POST["hdQID_".$qIDs[$i]]!=""){
						$TxtAnsID="'".addslashes($_POST["hdQID_".$qIDs[$i]])."'";
					}
					else{
						$TxtAnsID="NULL";
					}
				}
			}
			$AnsIDRmk=($x[0]["HasRmk"]==1 && $_POST["qIDRmk_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDRmk_".$qIDs[$i]])."'":"NULL";
			
			if($isQuestionRecordExists){
				$sql="UPDATE INTRANET_PA_T_SLF_DTL SET QAnsID=".$AnsID.",MarkID=".$MarkID.",Remark=".$AnsIDRmk.",Score=".$Score.",TxtAns=".$TxtAnsID.",DateAns=".$DateAns.",";
				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
				$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i]).";";
				//echo $sql."<br/><br/>";
				$x = $connection->db_db_query($sql);
					
				$sql = "SELECT BatchID FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID<>".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i]).";";
				$otherBatches = $connection->returnVector($sql);
				foreach($otherBatches as $oBatchID){
					$sql="UPDATE INTRANET_PA_T_SLF_DTL SET QAnsID=".$AnsID.",MarkID=".$MarkID.",Remark=".$AnsIDRmk.",Score=".$Score.",TxtAns=".$TxtAnsID.",DateAns=".$DateAns.",";
					$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
					$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($oBatchID)." AND QID=".IntegerSafe($qIDs[$i]).";";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
				}
			}
			else{
				$sql="INSERT INTO INTRANET_PA_T_SLF_DTL(RlsNo,RecordID,BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($qIDs[$i])." as QID,";
				$sql.=$AnsID." as QAnsID,".$MarkID." as MarkID,".$Score." as Score,".$TxtAnsID." as TxtAns,".$DateAns." as DateAns,".$AnsIDRmk." as Remark,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
				//echo $sql."<br/><br/>";
				$x = $connection->db_db_query($sql);
			}
		}
		else if($y[0]["QuesDispMode"]==2){
			/*
			$mtxFldID=$_POST["MtxFldID_".$qIDs[$i]];
			$MarkID=($_POST["qID_".$qIDs[$i]]!=null)?$_POST["qID_".$qIDs[$i]]:"NULL";
			$TxtAnsID=($_POST["qIDTxtAns_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDTxtAns_".$qIDs[$i]])."'":"NULL";
			$Score=($_POST["qIDScore_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDScore_".$qIDs[$i]])."'":"NULL";
			$DateAns=($_POST["qIDDate_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDDate_".$qIDs[$i]])."'":"NULL";
			
			$sql="SELECT * FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i]).";";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			if(sizeof($a)>0){
				$sql="UPDATE INTRANET_PA_T_SLF_MTX SET Score=".$Score.",TxtAns=".$TxtAnsID.",DateAns=".$DateAns.",";
				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
				$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i])." AND MtxFldID=".$mtxFldID.";";
				//echo $sql."<br/><br/>";
				$x = $connection->db_db_query($sql);
			}
			else{
				$sql="INSERT INTO INTRANET_PA_T_SLF_MTX(RlsNo,RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($qIDs[$i])." as QID,".$mtxFldID." as MtxFldID,";
				$sql.=$Score." as Score,".$TxtAnsID." as TxtAns,".$DateAns." as DateAns,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
				//echo $sql."<br/><br/>";
				$x = $connection->db_db_query($sql);
			}
			*/
						
			$mtxFldID=$_POST["MtxFldID_".$qIDs[$i]];
			$sql="SELECT MtxFldID FROM INTRANET_PA_S_QMTX WHERE QCatID IN (SELECT QCatID FROM INTRANET_PA_S_QMTX WHERE MtxFldID=".$mtxFldID.");";
			$z=$connection->returnResultSet($sql);
			$MarkID=($_POST["qID_".$qIDs[$i]]!=null)?$_POST["qID_".$qIDs[$i]]:"NULL";
			$TxtNull = 0;
			$DatNull = 0;
			$ScoreNull = 0;
			$TxtAnsID = "";
			$DateAns = "";
			$Score = "";
			$remarks = ($_POST["qIDRmk_".$qIDs[$i]]!=null)?$_POST["qIDRmk_".$qIDs[$i]]:"";
			for($j=0;$j<sizeof($z);$j++){
				$TxtNull = ($_POST["qIDTxtAns_".$qIDs[$i]."_".$j]!=null)? ($TxtNull|1):($TxtNull|0);
				$DatNull = ($_POST["qIDDate_".$qIDs[$i]."_".$j]!=null)? ($DatNull|1):($DatNull|0);
				$ScoreNull = ($_POST["qIDScore_".$qIDs[$i]."_".$j]!=null)? ($ScoreNull|1):($ScoreNull|0);
				if($j==(sizeof($z)-1)){
					$TxtAnsID = $TxtAnsID.$_POST["qIDTxtAns_".$qIDs[$i]."_".$j];
					$DateAns = $DateAns.$_POST["qIDDate_".$qIDs[$i]."_".$j];
					$Score = $Score.$_POST["qIDScore_".$qIDs[$i]."_".$j];
				}
				else{					
					$TxtAnsID = $TxtAnsID.$_POST["qIDTxtAns_".$qIDs[$i]."_".$j]."#$^";
					$DateAns = $DateAns.$_POST["qIDDate_".$qIDs[$i]."_".$j]."#$^";
					//echo $_POST["qIDDate_".$qIDs[$i]."_".$j]."<br/>";
					$Score = $Score.$_POST["qIDScore_".$qIDs[$i]."_".$j]."#$^";
				}				
				/*$Score=($_POST["qIDScore_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDScore_".$qIDs[$i]])."'":"NULL";
				$DateAns=($_POST["qIDDate_".$qIDs[$i]]!=null)?"'".$indexVar['libappraisal']->Get_Safe_Sql_Query($_POST["qIDDate_".$qIDs[$i]])."'":"NULL";*/
			}
			$TxtAnsID = ($TxtNull!=0)? ("'".$indexVar['libappraisal']->Get_Safe_Sql_Query($TxtAnsID)."'"):"NULL";
			$DateAns = ($DatNull!=0)? ("'".$indexVar['libappraisal']->Get_Safe_Sql_Query($DateAns)."'"):"NULL";
			$Score = ($ScoreNull!=0)? ("'".$indexVar['libappraisal']->Get_Safe_Sql_Query($Score)."'"):"NULL";
								
			$sql="SELECT * FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i]).";";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			if(sizeof($a)>0){
				$sql="UPDATE INTRANET_PA_T_SLF_MTX SET Score=".$Score.",TxtAns=".$TxtAnsID.",DateAns=".$DateAns.",Remark='".$remarks."',";
				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
				$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)." AND QID=".IntegerSafe($qIDs[$i])." AND MtxFldID=".$mtxFldID.";";
				//echo $sql."<br/><br/>";
				$x = $connection->db_db_query($sql);
			}
			else{
				$sql="INSERT INTO INTRANET_PA_T_SLF_MTX (RlsNo,RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
				$sql.="SELECT ".IntegerSafe($rlsNo)." as RlsNo,".IntegerSafe($recordID)." as RecordID,".IntegerSafe($batchID)." as BatchID,".IntegerSafe($qIDs[$i])." as QID,".$mtxFldID." as MtxFldID,";
				$sql.=$Score." as Score,".$TxtAnsID." as TxtAns,".$DateAns." as DateAns,'".$remarks."' as Remark,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
				$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified ;";
				//echo $sql."<br/><br/>";
				$x = $connection->db_db_query($sql);
			}
		}
	}
}
//else if($type=="formSignature"){
if($type=="formSignature"){
	//echo $type."<br/><br/>";
	$fillerID="";
	$password=$_POST["signauture"];
	//$fillerID=$_POST["fillerID"];
	$obsID=$_POST["obsID"];
	$obsIDArr=explode(",",$obsID);
	
	for($i=0;$i<sizeof($obsIDArr);$i++){
		$fillerID=$_POST["fillerID_".$i];
		$password=$_POST["signauture_".$i];
		$status = $indexVar['libappraisal']->signatureValidation($fillerID,$password);
		
		if($status=="true"){
			// update the subdate and IsLastSub for current document 
			$sql ="UPDATE INTRANET_PA_T_FRMSUB SET SubDate=NOW(),IsLastSub=1,";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			//echo $sql."<br/><br/>";
			$x = $connection->db_db_query($sql);
			
			// update IsLastSub for other documents with same rlsNo, recordID to be 0
//			$sql ="UPDATE INTRANET_PA_T_FRMSUB SET IsLastSub=0,";
//			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
//			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID<>".IntegerSafe($batchID).";";
//			//echo $sql."<br/><br/>";
//			$x = $connection->db_db_query($sql);
			
			// get the current secID
			$sql="SELECT RlsNo,RecordID,BatchID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID.";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$currParSecID=$x[0]["SecID"];
			
			$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID=".IntegerSafe($currParSecID).";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);		
			$secIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"SecID");
			//echo $secIDArr."<br/><br/>";
			
			$sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$secIDArr.");";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$qCatIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"QCatID");
			
			$sql="SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatIDArr.");";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$qIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"QID");
			//echo $qIDArr."<br/><br/>";
			
			// get those documents w/o signing the signature
			/*$sql="SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND SubDate IS NULL;";
			echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);*/		
			
			$sql="UPDATE INTRANET_PA_T_FRMSUM SET IsComp=1,";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
			$sql.="WHERE RecordID=".IntegerSafe($recordID)." AND CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
			//echo $sql."<br/><br/>";
			$x=$connection->db_db_query($sql);
			
			// get and open the next secID
			$sql="SELECT RlsNo,RecordID,BatchID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND AppRoleID=2 AND IsActive=0";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			foreach($x as $fbkSec){
				$sql = "UPDATE INTRANET_PA_T_FRMSUB SET IsActive=1 WHERE RlsNo=".$fbkSec['RlsNo']." AND RecordID=".$fbkSec['RecordID']." AND BatchID=".$fbkSec['BatchID']."";
				$x=$connection->db_db_query($sql);
				
				if($fbkSec["BatchID"] != $batchID){
					$sql="INSERT INTO INTRANET_PA_T_SLF_HDR(RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$fbkSec["RlsNo"]." as RlsNo,".$fbkSec["RecordID"]." as RecordID,".$fbkSec["BatchID"]." as BatchID,YearClassID,SubjectID,TeachLang,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID.") as a;";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
				}
			}
			
			/*if(sizeof($y)>0){
				for($i=0;$i<sizeof($y);$i++){
					// Delete the INTRANET_PA_T_SLF_HDR & INTRANET_PA_T_SLF_DTL
					$sql="DELETE FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
					//echo $sql."<br/><br/>";
					$x=$connection->db_db_query($sql);
					$sql="DELETE FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"]." AND QID IN (".$qIDArr.");";
					//echo $sql."<br/><br/>";
					$x=$connection->db_db_query($sql);
					
					// Add the signed Answer to the unsigned  
					$sql="INSERT INTO INTRANET_PA_T_SLF_HDR(RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearClassID,SubjectID,TeachLang,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID.") as a;";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
						
					$sql="INSERT INTO INTRANET_PA_T_SLF_DTL(RlsNo,RecordID,BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID." AND QID IN (".$qIDArr.")";
					$sql.=") as a";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
				}
			}
			else{
				$sql="UPDATE INTRANET_PA_T_FRMSUM SET IsComp=1,";
				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
				$sql.="WHERE RecordID=".IntegerSafe($recordID)." AND CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
			}*/
		}
	}
}
else if($type=="modFormSignature"){
	/*$password=$_POST["signauture"];
	$fillerID=$_POST["fillerID"];*/
	//$fillerID=$_POST["fillerID"];
	$obsID=$_POST["obsID"];
	$obsIDArr=explode(",",$obsID);
	
	for($i=0;$i<sizeof($obsIDArr);$i++){
		$fillerID=$_POST["fillerID_".$i];
		$password=$_POST["signauture_".$i];
		$status = $indexVar['libappraisal']->signatureValidation($fillerID,$password);
		
		if($status=="true"){
			// update the subdate and IsLastSub for current document
			$sql ="UPDATE INTRANET_PA_T_FRMSUB SET SubDate=NOW(),ModDateFr=NULL,ModDateTo=NULL,ModRemark=NULL,";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
			//echo $sql."<br/><br/>";
			$x = $connection->db_db_query($sql);
			
			// update IsLastSub for other documents with same rlsNo, recordID to be 0
			/*$sql ="UPDATE INTRANET_PA_T_FRMSUB SET IsLastSub=0,";
			$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW() ";
			$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID<>".IntegerSafe($batchID).";";
			//echo $sql."<br/><br/>";
			$x = $connection->db_db_query($sql);*/
			
			// get the current secID
			$sql="SELECT RlsNo,RecordID,BatchID,SecID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID.";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$currParSecID=$x[0]["SecID"];
			
			$sql="SELECT SecID FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID=".IntegerSafe($currParSecID).";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$secIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"SecID");
			//echo $secIDArr."<br/><br/>";
			
			$sql="SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$secIDArr.");";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$qCatIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"QCatID");
			
			$sql="SELECT QID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatIDArr.");";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$qIDArr=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x,"QID");
			//echo $qIDArr."<br/><br/>";
			
			// get those documents which have SubDate
			$sql="SELECT RlsNo,RecordID,BatchID,SubDate FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID<>".IntegerSafe($batchID)." AND SubDate IS NOT NULL;";
			//echo $sql."<br/><br/>";
			$z0=$connection->returnResultSet($sql);
					
			// get those documents w/o signing the signature
			$sql="SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND SubDate IS NULL;";
			//echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);
			
			if(sizeof($y)>0){
				for($i=0;$i<sizeof($y);$i++){
					// Delete the INTRANET_PA_T_SLF_HDR & INTRANET_PA_T_SLF_DTL
					$sql="DELETE FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
					//echo $sql."<br/><br/>";
					$x=$connection->db_db_query($sql);
					$sql="DELETE FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"]." AND QID IN (".$qIDArr.");";
					//echo $sql."<br/><br/>";
					$x=$connection->db_db_query($sql);
			
					// Add the signed Answer to the unsigned
					$sql="INSERT INTO INTRANET_PA_T_SLF_HDR(RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearClassID,SubjectID,TeachLang,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID.") as a;";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
						
					$sql="INSERT INTO INTRANET_PA_T_SLF_DTL(RlsNo,RecordID,BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID." AND QID IN (".$qIDArr.")";
					$sql.=") as a";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
				}
			}
			else{
				$sql="UPDATE INTRANET_PA_T_FRMSUM SET IsComp=1,";
				$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW()";
				$sql.="WHERE RecordID=".IntegerSafe($recordID)." AND CycleID=".IntegerSafe($cycleID)." AND TemplateID=".IntegerSafe($templateID).";";
				//echo $sql."<br/><br/>";
				$x=$connection->db_db_query($sql);
			}		
			
			// get those documents which is the last submission
			$sql="SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID<>".IntegerSafe($batchID)." AND IsLastSub=1;";
			//echo $sql."<br/><br/>";
			$y=$connection->returnResultSet($sql);		
			
			if(sizeof($y)>0){
				for($i=0;$i<sizeof($y);$i++){
					// Delete the INTRANET_PA_T_SLF_HDR & INTRANET_PA_T_SLF_DTL
					$sql="DELETE FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"].";";
					//echo $sql."<br/><br/>";
					$x=$connection->db_db_query($sql);
					$sql="DELETE FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$y[$i]["RlsNo"]." AND RecordID=".$y[$i]["RecordID"]." AND BatchID=".$y[$i]["BatchID"]." AND QID IN (".$qIDArr.");";
					//echo $sql."<br/><br/>";
					$x=$connection->db_db_query($sql);
			
					// Add the signed Answer to the unsigned
					$sql="INSERT INTO INTRANET_PA_T_SLF_HDR(RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,YearClassID,SubjectID,TeachLang,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_HDR WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID.") as a;";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
						
					$sql="INSERT INTO INTRANET_PA_T_SLF_DTL(RlsNo,RecordID,BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified) ";
					$sql.="SELECT * FROM(SELECT ".$y[$i]["RlsNo"]." as RlsNo,".$y[$i]["RecordID"]." as RecordID,".$y[$i]["BatchID"]." as BatchID,QID,QAnsID,MarkID,Score,TxtAns,DateAns,Remark,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,";
					$sql.=IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID=".$batchID." AND QID IN (".$qIDArr.")";
					$sql.=") as a";
					//echo $sql."<br/><br/>";
					$x = $connection->db_db_query($sql);
				}
			}
		}
	}
}
else if($type=="modifySave"){
	
	$modDateFr=($_POST["modDateFr"]!=null)?"'".$_POST["modDateFr"]." 00:00:00'":"NULL";
	$modDateTo=($_POST["modDateTo"]!=null)?"'".$_POST["modDateTo"]." 23:59:59'":"NULL";
	$modRemark=($_POST["modRemark"]!="")?"'".$connection->Get_Safe_Sql_Query($_POST["modRemark"])."'":"''";
	// update the subdate and IsLastSub for current document
	$sql ="UPDATE INTRANET_PA_T_FRMSUB SET SubDate=NULL,ModDateFr=".$modDateFr.",ModDateTo=".$modDateTo.",ModRemark=".$modRemark.",";
	$sql.="ModifiedBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeModified=NOW(), ";
	$sql.="ReturnBy_UserID=".IntegerSafe($_SESSION['UserID']).",DateTimeReturn=NOW() ";
	$sql.="WHERE RlsNo=".IntegerSafe($rlsNo)." AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
	//echo $sql."<br/><br/>";
	$x=$connection->db_db_query($sql);
}

$returnMsgKey = ($saveSuccess)? 'UpdateSuccess' : 'UpdateUnsuccess';
header($returnPath);

?>