<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
# Page Title
$curTab="returnList";
$TAGS_OBJ[] = array($Lang['Appraisal']['Modification']['modification'], 'javascript: goModificationList(0);', $curTab=='modificationList');
$TAGS_OBJ[] = array($Lang['Appraisal']['Modification']['return'], 'javascript: goReturnList(0);', $curTab=='returnList');
/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */
$connection = new libgeneralsettings();
$currentDate = date("Y-m-d");
$a = $indexVar['libappraisal']->getAcademicYearTermOfCurrentCycle($currentDate);

// get Current Cycle Description
//$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".IntegerSafe($a[0]["AcademicYearID"]).";";
$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE CycleStart <= '".$currentDate."' AND CycleClose >= '".$currentDate."';";
$a = $connection->returnResultSet($sql);
$cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
$cycleDesc = Get_Lang_Selection($a[0]["DescrChi"],$a[0]["DescrEng"]);


# Page Title
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['ModificationList']."</span></div>";;
if($cycleID!=0){
// 	$titleBar .= "<div style=\"display:inline;float:right;\" class=\"online_help\"><a href=\"javascript:goDownloadAttachment('".$cycleID."')\" title=\"".$Lang['Appraisal']['TitleBar']['DownloadDocument']."\" onclick=\"goDownload(".$cycleID.")\" class=\"help_icon\" id=\"downloadIcon\">";
// 	$titleBar .= "<span style=\"font-size:10px\">".$Lang['Appraisal']['TitleBar']['DownloadDocument']."</span></a></div>";
}
// $TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();
echo $titleBar;
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$x = '';
// $sql = "SELECT * FROM INTRANET_PA_T_FRMS";
// $cycleMsg = ($cycleDesc!="")?sprintf($Lang['Appraisal']['ARCycleStart'],$cycleDesc):"";
// $x .= $cycleMsg."<br/><br/>"."\r\n";
//$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['ARMyAppraisal'])."<br/>\r\n";


$strSQL = "SELECT distinct TemplateID FROM INTRANET_PA_T_FRMSUM WHERE UserID=".IntegerSafe($_SESSION['UserID'])." AND IsActive=1 AND CycleID=".$cycleID."";
// hdebug_r(array($strSQL));
$result = $indexVar['libappraisal']->returnVector($strSQL);
if (count($result) > 0) {
	$AppRecInfo["own"]["TemplateIDs"] = $result;
}

$strSQL = "SELECT distinct UserID as Total FROM INTRANET_PA_T_FRMSUM WHERE UserID=".IntegerSafe($_SESSION['UserID'])." AND IsActive=1 AND CycleID=".$cycleID."";
// hdebug_r(array($strSQL));
$result = $indexVar['libappraisal']->returnVector($strSQL);
if (count($result) > 0) {
	$AppRecInfo["own"]["UserIDs"] = $result;
}
 
$strSQL = "SELECT distinct TemplateID FROM INTRANET_PA_T_FRMSUM WHERE UserID<>".IntegerSafe($_SESSION['UserID'])." AND IsActive=1 AND CycleID=".$cycleID."";
// hdebug_r(array($strSQL));
$result = $indexVar['libappraisal']->returnVector($strSQL);
if (count($result) > 0) {
 	$AppRecInfo["other"]["TemplateIDs"] = $result;
 
 	$strSQL = "SELECT BatchID FROM INTRANET_PA_C_BATCH WHERE TemplateID IN ('" . implode("', '", (array)$AppRecInfo["other"]["TemplateIDs"]) . "') ";
 	//hdebug_r(array($strSQL));
 	$result = $indexVar['libappraisal']->returnVector($strSQL);
 	if (count($result) > 0) {
 		$AppRecInfo["other"]["BatchIDs"] = $result;
 	
 	}

	$strSQL = "SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE TemplateID IN ('" . implode("', '", (array)$AppRecInfo["other"]["TemplateIDs"]) . "') ";
	// hdebug_r(array($strSQL));
	$result = $indexVar['libappraisal']->returnVector($strSQL);
	if (count($result) > 0) {
		$AppRecInfo["other"]["RecordIDs"] = $result;
	}
	
	$strSQL = "SELECT RlsNo FROM INTRANET_PA_C_CYCLE WHERE CycleID='".$cycleID."'";
	$result = $indexVar['libappraisal']->returnVector($strSQL);
	if (count($result) > 0) {
		$AppRecInfo["other"]["RlsNo"] = current($result);
	}
  }
$strSQL = "SELECT distinct UserID as Total FROM INTRANET_PA_T_FRMSUM WHERE UserID<>".IntegerSafe($_SESSION['UserID'])." AND IsActive=1 AND CycleID=".$cycleID."";
// hdebug_r(array($strSQL));
$result = $indexVar['libappraisal']->returnVector($strSQL);
if (count($result) > 0) {
	$AppRecInfo["other"]["UserIDs"] = $result;
}

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("FormName","FillerName","ModPeriod","Status","Action","MyRole","DisplayOrder");

if ($keyword !== '' && $keyword !== null) {
	$condsKeyword = " WHERE 1=1";
}

$li -> sql ="
		SELECT FormName,FillerName,ModPeriod,Status,Action,DisplayOrder
		FROM(
			SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,
						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\',',a0.isObs,')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
                as Action,
                DateTimeModified,RecordID,BatchID,DisplayOrder,TemplateID,DateTimeReturn,

				CASE WHEN ModDateTo < NOW() THEN
					CONCAT('<span class=\"red\">',ModDateFr, ' - ', DATE_FORMAT(ModDateTo,'%Y-%m-%d'),'</span>')
				ELSE
					CONCAT(ModDateFr, ' - ', DATE_FORMAT(ModDateTo,'%Y-%m-%d'))
				END as ModPeriod,

                CASE WHEN SubDate IS NOT NULL THEN
                    CONCAT('','".$Lang['Appraisal']['Completed']."','')
				    WHEN SubDate IS NULL AND DateTimeReturn = DateTimeModified THEN
				    CONCAT('','". $Lang['Appraisal']['Pending'] ."','')
                    WHEN SubDate IS NULL AND DateTimeReturn != DateTimeModified THEN
				    CONCAT('','" . $Lang['Appraisal']['Drafted'] . "','')
				END
                as Status,FillerName
				FROM(
					SELECT ".$indexVar['libappraisal']->Get_Name_Field("filleruser.")." as FillerName, frmsub.DateTimeReturn,frmsub.RecordID,CONCAT(COALESCE(FrmTitleChi,''),' - ',COALESCE('',FrmCodChi),' - ',COALESCE(ObjChi,''),' - ',COALESCE(SecTitleChi,'')) as FrmTitleChi,CONCAT(COALESCE(FrmTitleEng,''),' - ',COALESCE(FrmCodEng,''),' - ',COALESCE(ObjEng,''),' - ',COALESCE(SecTitleEng,'')) as FrmTitleEng,
					DescrChi,DescrEng,frmsub.AppRoleID,iuser.ChineseName as NameChi,iuser.EnglishName as NameEng,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,CONCAT(DATE_FORMAT(ModDateTo,'%Y-%m-%d'),' 23:59:59') as ModDateTo,
					DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,frmsub.DateTimeModified,frmsub.BatchID,frmsub.RlsNo,
					CASE WHEN sftpl.TemplateType = '0' THEN 'PDF' WHEN sftpl.TemplateType = '1' THEN 'SDF' END as TemplateIDType,
					sftpl.DisplayOrder,sftpl.TemplateID,sfsec.SecID,frmsub.EditPrdFr,frmsub.EditPrdTo,frmsub.FillerID,sftpl.isObs
					FROM INTRANET_PA_T_FRMSUB frmsub 
					INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID
					INNER JOIN INTRANET_PA_S_FRMTPL sftpl ON frmsum.TemplateID = sftpl.TemplateID
					INNER JOIN INTRANET_PA_C_BATCH cbatch ON frmsub .BatchID=cbatch.BatchID
					INNER JOIN INTRANET_PA_S_FRMSEC sfsec ON cbatch.SecID = sfsec.SecID
					INNER JOIN ".$appraisalConfig['INTRANET_USER']." iuser ON frmsum.UserID=iuser.UserID
                    INNER JOIN ".$appraisalConfig['INTRANET_USER']." filleruser ON frmsub.FillerID=filleruser.UserID
					WHERE frmsub.ReturnBy_UserID = " . $_SESSION['UserID'] . "
					ORDER BY frmsub.ModDateFr ASC, frmsub.RecordID ASC, frmsub.BatchID ASC
				) as a0
		) as a";
//echo $li->sql."<br/><br/>";

$li->no_col = sizeof($li->field_array)-1;
//$li->fieldorder2 = ", FormFillAndOrder";
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='30%' >".$li->column($pos++, $Lang['Appraisal']['ARForm'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['Modification']['returnTo'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['Appraisal']['ARFormModDate'])."</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['Appraisal']['ARFormQuaAcqSts'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, "")."</th>\n";
$htmlAry['dataTable'] = $li->display();
//
//$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
//$field = ($field == '') ? 8 : $field;
//$pageNo = ($pageNo == '') ? 1 : $pageNo;
//$page_size = ($numPerPage == '') ? 10 : $numPerPage;
//
//$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("FormName","StfName","StfGrpDescr","IdvArrRmk","MyRole","FormFillAndOrder","SubDate","Action","DisplayOrder");
//
//if ($keyword !== '' && $keyword !== null) {
//	$condsKeyword = " WHERE 1=1";
//}
//$filter_deleted = "";
//if(!$sys_custom['eAppraisal']['showArchieveTeacher']){
//	$filter_deleted = " AND auser.UserLogin IS NULL ";
//}
///*
//$li -> sql ="
//		SELECT FormName,StfName,StfGrpDescr,IdvArrRmk,MyRole,FormFillAndOrder,SubDate,Action,DisplayOrder
//		FROM(
//			SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as StfName,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as StfGrpDescr,
//				Remark as IdvArrRmk,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
//				CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
//				SubDate,CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) THEN
//						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a>')
//					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL) THEN
//						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a><br/>
//							<span style=\"color:#FF0000\">(',a0.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',a0.ModDateTo,')</span>	
//							')
//					ELSE
//						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
//					END
//				as Action,DateTimeModified,RecordID,BatchID,DisplayOrder
//				FROM(
//					SELECT iptf.RecordID,CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi,' - ',SecTitleChi) as FrmTitleChi,CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng,' - ',SecTitleEng) as FrmTitleEng,
//					DescrChi,DescrEng,Remark,ipcb.AppRoleID,NameChi,NameEng,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,CONCAT(DATE_FORMAT(EditPrdTo,'%Y-%m-%d'),' 23:59:59') as EditPrdTo,
//					DATE_FORMAT(SubDate,'%Y-%m-%d') as SubDate,DateTimeModified,iptfFrmSub.BatchID,iptfFrmSub.RlsNo,
//					CASE WHEN FrmPreTemplateID IS NOT NULL THEN 'PDF' WHEN FrmSecTemplateID IS NOT NULL THEN 'SDF' END as TemplateIDType,DisplayOrder,ipsf.TemplateID,ChineseName,EnglishName,ModDateFr,ModDateTo
//					FROM(
//						SELECT RecordID,CycleID,TemplateID,UserID,DateTimeModified,GrpID,IdvID FROM INTRANET_PA_T_FRMSUM WHERE UserID<>".IntegerSafe($_SESSION['UserID'])." AND IsActive=1 AND CycleID=".$cycleID."
//					) as iptf
//					INNER JOIN(
//						SELECT UserID,EnglishName,ChineseName,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE RecordStatus='1' AND RecordType='1'
//					) as iu ON iptf.UserID=iu.UserID
//					INNER JOIN(
//						SELECT RlsNo,RecordID,BatchID,FillerID,SubDate,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as ModDateTo 
//								FROM INTRANET_PA_T_FRMSUB WHERE FillerID=".IntegerSafe($_SESSION['UserID'])." AND IsActive=1 AND (SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo))
//					) as iptfFrmSub ON iptf.RecordID=iptfFrmSub.RecordID
//					INNER JOIN(
//						SELECT TemplateID,TemplateType,COALESCE(FrmTitleChi,'') as FrmTitleChi,COALESCE(FrmTitleEng,'') as FrmTitleEng,
//						COALESCE(FrmCodChi,'') as FrmCodChi,COALESCE(FrmCodEng,'') as FrmCodEng,COALESCE(ObjChi,'') as ObjChi,COALESCE(ObjEng,'') as ObjEng,DisplayOrder FROM INTRANET_PA_S_FRMTPL 
//										WHERE TemplateID IN ('" . implode("','", (array)$AppRecInfo["other"]["TemplateIDs"]) . "') AND IsObs=0
//					) as ipsf ON iptf.TemplateID=ipsf.TemplateID
//					LEFT JOIN(
//						SELECT GrpID,CycleID,DescrChi,DescrEng FROM INTRANET_PA_C_GRPSUM
//					) as ipcg ON iptf.CycleID=ipcg.CycleID AND iptf.GrpID=ipcg.GrpID
//					LEFT JOIN(
//						SELECT IdvID,CycleID,Remark,UserID,TemplateID FROM INTRANET_PA_C_IDVFRM WHERE TemplateID IN ('" . implode("','", (array)$AppRecInfo["other"]["TemplateIDs"]) . "')
//					) as ipci ON iptf.CycleID=ipci.CycleID  AND iptf.IdvID=ipci.IdvID
//					LEFT JOIN(
//						SELECT BatchID,CycleID,AppRoleID,EditPrdFr,EditPrdTo,TemplateID,SecID FROM INTRANET_PA_C_BATCH WHERE TemplateID IN ('" . implode("','", (array)$AppRecInfo["other"]["TemplateIDs"]) . "')
//					) as ipcb ON iptf.CycleID=ipcb.CycleID AND iptf.TemplateID=ipcb.TemplateID AND ipcb.BatchID=iptfFrmSub.BatchID
//					LEFT JOIN(
//						SELECT AppRoleID,NameChi,NameEng FROM INTRANET_PA_S_APPROL
//					) as ipsa ON ipcb.AppRoleID=ipsa.AppRoleID
//					LEFT JOIN(
//						SELECT TemplateID as FrmPreTemplateID FROM INTRANET_PA_S_FRMPRE WHERE TemplateID IN ('" . implode("','", (array)$AppRecInfo["other"]["TemplateIDs"]) . "') GROUP BY TemplateID
//					) as ipsFrmpre ON iptf.TemplateID=ipsFrmpre.FrmPreTemplateID
//					LEFT JOIN(
//						SELECT TemplateID as FrmSecTemplateID,SecID,COALESCE(SecTitleChi,'') as SecTitleChi,COALESCE(SecTitleEng,'') as SecTitleEng FROM INTRANET_PA_S_FRMSEC WHERE ParentSecID IS NULL AND TemplateID IN ('" . implode("','", (array)$AppRecInfo["other"]["TemplateIDs"]) . "')
//					) as ipsFrmsec ON iptf.TemplateID=ipsFrmsec.FrmSecTemplateID AND ipcb.SecID=ipsFrmsec.SecID
//				) as a0
//		) as a";
//*/
//$li->sql = "
//		SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormName,StfName,StfGrpDescr,IdvArrRmk,MyRole,FormFillAndOrder,SubDate,Action,DisplayOrder
//		FROM(
//			SELECT CONCAT(FrmTitleChi,' - ',FrmCodChi,' - ',ObjChi,' - ',SecTitleChi) as FrmTitleChi,CONCAT(FrmTitleEng,' - ',FrmCodEng,' - ',ObjEng,' - ',SecTitleEng) as FrmTitleEng,".$indexVar['libappraisal']->Get_Name_Field("",true)." as StfName,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as StfGrpDescr,
//				Remark as IdvArrRmk,".$indexVar['libappraisal']->getLangSQL("NameChi","NameEng")." as MyRole,
//				CONCAT('".$Lang['Appraisal']['ARFormFrm']."',' ',EditPrdFr,' ','".$Lang['Appraisal']['ARFormTo']."',' ',DATE_FORMAT(EditPrdTo,'%Y-%m-%d')) as FormFillAndOrder,
//				SubDate,CASE WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NULL AND ModDateTo IS NULL) THEN
//						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a>')
//					WHEN SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= EditPrdTo) AND (ModDateFr IS NOT NULL AND ModDateTo IS NOT NULL) THEN
//						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goEdit(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormEdit']."','</a><br/>
//							<span style=\"color:#FF0000\">(',a0.ModDateFr,' ".$Lang['Appraisal']['ARFormTo']."<br/>',a0.ModDateTo,')</span>	
//							')
//					ELSE
//						CONCAT('<a id=\"',a0.RecordID,'\" class=\"tablelink\" href=\"javascript:goView(',a0.RlsNo,',',a0.RecordID,',',a0.BatchID,',\'',a0.TemplateIDType,'\')\">','".$Lang['Appraisal']['ARFormView']."','</a>')
//					END
//				as Action,DateTimeModified,RecordID,BatchID,DisplayOrder
//				FROM(
//					SELECT 
//						'SDF' as TemplateIDType,tfsub.RlsNo,tfsum.RecordID,tfsub.BatchID,
//						COALESCE(FrmTitleChi,'') as FrmTitleChi,COALESCE(FrmTitleEng,'') as FrmTitleEng,
//						COALESCE(FrmCodChi,'') as FrmCodChi,COALESCE(FrmCodEng,'') as FrmCodEng,
//						COALESCE(ObjChi,'') as ObjChi,COALESCE(ObjEng,'') as ObjEng,
//						COALESCE(SecTitleChi,'') as SecTitleChi,COALESCE(SecTitleEng,'') as SecTitleEng, 
//						tfsum.CycleID,tfsum.TemplateID,tfsum.UserID,tfsum.DateTimeModified, cgsum.DescrChi,cgsum.DescrEng,
//						cifrm.Remark,tfsum.IdvID, saprol.NameChi, saprol.NameEng,
//						DATE_FORMAT(tfsub.EditPrdFr,'%Y-%m-%d') as EditPrdFr, DATE_FORMAT(tfsub.EditPrdTo,'%Y-%m-%d') as EditPrdTo,
//						DATE_FORMAT(tfsub.ModDateFr,'%Y-%m-%d') as ModDateFr, DATE_FORMAT(tfsub.ModDateTo,'%Y-%m-%d') as ModDateTo,
//						tfsub.GrpID, DATE_FORMAT(tfsub.SubDate,'%Y-%m-%d') as SubDate , sftpl.DisplayOrder,iuser.ChineseName,iuser.EnglishName,iuser.RecordStatus
//					FROM INTRANET_PA_T_FRMSUB tfsub
//					INNER JOIN INTRANET_PA_T_FRMSUM tfsum ON tfsub.RecordID = tfsum.RecordID  AND tfsum.UserID <> '".IntegerSafe($_SESSION['UserID'])."'
//					INNER JOIN INTRANET_PA_S_FRMTPL sftpl ON tfsum.TemplateID = sftpl.TemplateID
//					INNER JOIN INTRANET_PA_C_BATCH cbatch ON tfsub.BatchID=cbatch.BatchID
//					INNER JOIN INTRANET_PA_S_FRMSEC sfsec ON cbatch.SecID = sfsec.SecID
//					INNER JOIN ".$appraisalConfig['INTRANET_USER']." iuser ON tfsum.UserID=iuser.UserID
//					INNER JOIN INTRANET_PA_S_APPROL saprol ON saprol.AppRoleID = tfsub.AppRoleID
//					LEFT JOIN INTRANET_PA_C_GRPSUM cgsum ON tfsum.GrpID = cgsum.GrpID
//					LEFT JOIN INTRANET_PA_C_IDVFRM cifrm ON tfsum.IdvID= cifrm.IdvID
//					LEFT JOIN INTRANET_ARCHIVE_USER auser ON iuser.UserID= auser.UserID
//					WHERE (SubDate IS NULL AND (DATE_FORMAT(NOW(),'%Y-%m-%d')>=tfsub.EditPrdFr AND DATE_FORMAT(NOW(),'%Y-%m-%d') <= tfsub.EditPrdTo)) 
//					AND tfsub.FillerID='".IntegerSafe($_SESSION['UserID'])."'
//					AND tfsub.RlsNo='".$AppRecInfo["other"]["RlsNo"]."' AND tfsub.BatchID IN ('".implode("','", (array)$AppRecInfo["other"]["BatchIDs"])."') 
//					AND tfsum.RecordID IN ('".implode("','", (array)$AppRecInfo["other"]["RecordIDs"])."') $filter_deleted
//				) as a0
//		) as a
//		";
///*
// echo "<div style='display:none;'>";
// echo $li->sql."<br/><br/>";
// echo "</div>";
// */
//// hdebug_r(array($li->sql));
//$li->no_col = sizeof($li->field_array);
//$li->fieldorder2 = ", DisplayOrder";
//$li->IsColOff = "IP25_table";
//$li->count_mode = 1;
//
//$pos = 0;
//$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
//$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['Appraisal']['ARForm'])."</th>\n";
//$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormStfOwnerName'])."</th>\n";
//$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormStfGrp'])."</th>\n";
//$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['ARFormIdvArrRmk'])."</th>\n";
//$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormMyRole'])."</th>\n";
//$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormFormFillAndOrder'])."</th>\n";
//$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormSubDate'])."</th>\n";
//$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['ARFormAction'])."</th>\n";
//$htmlAry['dataTable2'] = $li->display();
//
//$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
//$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
//$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
//$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
//$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
//
//$y .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['AROtherStaffAppraisal'])."<br/>\r\n";

$htmlAry['contentTbl'] = $x;
//$htmlAry['contentTbl2'] = $y;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================

// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>