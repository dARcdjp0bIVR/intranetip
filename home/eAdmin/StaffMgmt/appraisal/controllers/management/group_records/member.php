<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */
$connection = new libgeneralsettings();
$currentDate = date("Y-m-d");
$a = $indexVar['libappraisal']->getAcademicYearTerm($currentDate);

// get Current Cycle Description
$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".IntegerSafe($a[0]["AcademicYearID"]).";";
$a = $connection->returnResultSet($sql);
$cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
$cycleDesc = Get_Lang_Selection($a[0]["DescrChi"],$a[0]["DescrEng"]);


# Page Title
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['GroupRecord']."</span></div>";;
$TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;
$grpID = ($grpID!="")?$grpID:0;
$hasRead = ($hasRead == '')?'':$hasRead;

$btnAry = array();
$btnAry[] = array('approve', 'javascript:changeStatus('.$grpID.',1);', $Lang['Appraisal']['GroupRecordRead']);
$btnAry[] = array('reject', 'javascript:changeStatus('.$grpID.',0);', $Lang['Appraisal']['GroupRecordUnread']);
$htmlAry['dbTableActionBtn'] = $indexVar['libappraisal_ui']->Get_DBTable_Action_Button_IP25($btnAry);

$x = "";
//$cycleMsg = ($cycleDesc!="")?sprintf($Lang['Appraisal']['ARCycleStart'],$cycleDesc):"";
//$x .= $cycleMsg."<br/><br/>"."\r\n";
//$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['ARMyAppraisal'])."<br/>\r\n";

// ============================== Filter for Group Member ===================================
$memberName = ($intranet_session_language=='en')?"u.EnglishName":"IF(u.ChineseName='', u.EnglishName, u.ChineseName)";
if(!$sys_custom['eAppraisal']['showArchieveTeacher']){
	$filter_deleted = " AND auser.UserLogin IS NULL ";
}
$sql = "SELECT gm.UserID, ".$indexVar['libappraisal']->Get_Name_Field('u.', true)." as MemberName FROM INTRANET_PA_C_GRPMEM gm
		INNER JOIN INTRANET_PA_C_GRPFRM gf ON gm.GrpID=gf.GrpID AND gf.Incl=1
		INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON gm.UserID=u.UserID
		LEFT JOIN INTRANET_ARCHIVE_USER auser ON u.UserID = auser.UserID
		WHERE gm.GrpID=$grpID AND ((gm.IsLeader=1 AND gm.IsLeaderFill=1 AND gf.AppMode=1) OR gm.IsLeader=0) $filter_deleted ";
$userFilterContent = $connection->returnResultSet($sql);
$filter = "";
$filter .= "<select name='member' onchange='document.form1.submit()'>";
$filter .= "	<option value='' ".(($member=="")?"selected":"").">".$Lang['Appraisal']['AllMember']."</option>";
foreach($userFilterContent as $ufc){		
	$filter .= "	<option value='".$ufc['UserID']."' ".(($member==$ufc['UserID'])?"selected":"").">".$indexVar['libappraisal']->displayChinese($ufc['MemberName'])."</option>";
}
$filter .= "</select>";
$x .= $filter;

// ============================== Filter for Form ===================================
$formName = ($intranet_session_language=='en')?"ft.FrmTitleEng":"IF(ft.FrmTitleChi='', ft.FrmTitleEng, ft.FrmTitleChi)";
$sql = "SELECT gf.TemplateID, $formName as FormName FROM INTRANET_PA_C_GRPFRM gf 
		INNER JOIN INTRANET_PA_S_FRMTPL ft ON gf.TemplateID=ft.TemplateID
		WHERE gf.GrpID=$grpID AND gf.Incl=1 AND ft.IsActive=1";
$formInfo = $connection->returnResultSet($sql);	
$filter2 = "";
$filter2 .= "<select name='form' onchange='document.form1.submit()'>";
$filter2 .= "	<option value='' ".(($form=="")?"selected":"").">".$Lang['Appraisal']['AllForm']."</option>";
foreach($formInfo as $fi){
	$filter2 .= "	<option value='".$fi['TemplateID']."' ".(($form==$fi['TemplateID'])?"selected":"").">".$fi['FormName']."</option>";
}
$filter2 .= "</select>";
$x .= $filter2;
// ============================== Filter for Read Status ===================================
$readFilter = "";
$readFilter .= "<select name='hasRead' onchange='document.form1.submit()'>";
$readFilter .= "	<option value='' ".(($hasRead=="")?"selected":"").">".$Lang['Appraisal']['GroupRecordRead']."/".$Lang['Appraisal']['GroupRecordUnread']."</option>";
$readFilter .= "	<option value='0' ".(($hasRead=="0")?"selected":"").">".$Lang['Appraisal']['GroupRecordUnread']."</option>";
$readFilter .= "	<option value='1' ".(($hasRead=="1")?"selected":"").">".$Lang['Appraisal']['GroupRecordRead']."</option>";
$readFilter .= "</select>";
$x .= $readFilter;

// ============================== Navigation ===================================
$GrpName = ($intranet_session_language=='en')?"DescrEng":"DescrChi";
$sql = "SELECT $GrpName as GroupName FROM INTRANET_PA_C_GRPSUM WHERE GrpID='".$grpID."'";
$a = $connection->returnResultSet($sql);
$navigationAry[] = array($Lang['Appraisal']['GroupRecord'], 'javascript: goBack();');
$navigationAry[] = array($a[0]['GroupName']);
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("MemberName","TemplateName", "FillerName", "View", "ReadStatus","StatusChange");

//$GrpName = ($intranet_session_language=='en')?"DescrEng":"DescrChi";

$memberName = ($intranet_session_language=='en')?"u.EnglishName":"IF(u.ChineseName='', u.EnglishName, u.ChineseName)";
$tmptName = ($intranet_session_language=='en')?"CONCAT(psft.FrmTitleEng,' - ',COALESCE(psft.FrmCodEng,''),' - ',COALESCE(psft.ObjEng,''),' - ',COALESCE(psfs.SecTitleEng,''))":"CONCAT(psft.FrmTitleChi,' - ',COALESCE(psft.FrmCodChi,''),' - ',COALESCE(psft.ObjChi,''),' - ',COALESCE(psfs.SecTitleChi,''))";
$fillerName = ($intranet_session_language=='en')?"fu.EnglishName":"IF(fu.ChineseName='', fu.EnglishName, fu.ChineseName)";
$filter_deleted = "";
if(!$sys_custom['eAppraisal']['showArchieveTeacher']){
	$filter_deleted = " AND auser.UserLogin IS NULL AND afuser.UserLogin IS NULL ";
}
$li->sql = "SELECT 
					".$indexVar['libappraisal']->Get_Name_Field("u.", true)." as MemberName,
					$tmptName as TemplateName,
					".$indexVar['libappraisal']->Get_Name_Field("fu.", true)." as FillerName,
					IF(ptgr.ReadRecordID IS NULL, '".$Lang['Appraisal']['GroupRecordUnread']."', '".$Lang['Appraisal']['GroupRecordRead']."') as ReadStatus,
					IF(ptfsb.SubDate IS NULL, '".$Lang['Appraisal']['Report']['NotSubmitted']."', CONCAT('<a id=\"',u.UserID,'\" class=\"tablelink\" href=\"javascript:goView(',ptfsb.RlsNo,',',ptfsb.RecordID,',', ptfsb.BatchID,',',pcgf.GrpID,',', psft.TemplateType,')\">','".$Lang['Appraisal']['ARFormView']."','</a>')) as View,
					CONCAT('<input class=\"statusChangeCheckbox\" name=\"statusChange[]\" type=\"checkbox\" value=\"',ptfsb.RecordID,'_',ptfsb.BatchID,'\">') as StatusChange
			FROM INTRANET_PA_T_FRMSUB  ptfsb
			INNER JOIN INTRANET_PA_S_FRMSEC psfs ON ptfsb.SecID=psfs.SecID  
			INNER JOIN INTRANET_PA_T_FRMSUM ptfsm ON ptfsb.RecordID=ptfsm.RecordID
			INNER JOIN INTRANET_PA_C_GRPFRM pcgf ON ptfsm.GrpID=pcgf.GrpID AND pcgf.Incl=1
			INNER JOIN INTRANET_PA_S_FRMTPL psft ON pcgf.TemplateID=psft.TemplateID AND psft.IsActive=1
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON ptfsm.UserID=u.UserID
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." fu ON ptfsb.FillerID=fu.UserID
			LEFT JOIN INTRANET_PA_T_GRPREAD ptgr ON ptfsb.RecordID=ptgr.RecordID AND ptfsb.BatchID = ptgr.BatchID AND ptgr.ReaderID='".$_SESSION['UserID']."' 
			LEFT JOIN INTRANET_ARCHIVE_USER afuser ON fu.UserID = afuser.UserID
			LEFT JOIN INTRANET_ARCHIVE_USER auser ON u.UserID = auser.UserID
			WHERE pcgf.GrpID='".$grpID."' AND ptfsb.IsActive=1 $filter_deleted ";
$li->sql .=	($member!="")?" AND u.UserID='".$member."'":"";
$li->sql .=	($form!="")?" AND psft.TemplateID='".$form."'":"";
$li->sql .= ($hasRead=='0')?" AND ptgr.ReadRecordID IS NULL ":(($hasRead=='1')?" AND ptgr.ReadRecordID IS NOT NULL ":"");				
/*
$li -> sql ="SELECT $GrpName as GrpName, CONCAT('<a id=\"',gs.GrpID,'\" class=\"tablelink\" href=\"javascript:goView(',gs.GrpID,')\">','".$Lang['Appraisal']['ARFormView']."','</a>') as GrpLink From INTRANET_PA_C_GRPSUM gs
			 INNER JOIN INTRANET_PA_C_GRPMEM gm ON gs.GrpID=gm.GrpID AND gm.IsLeader=1
			 WHERE gm.UserID='".$_SESSION['UserID']."' AND gs.CycleID='".$cycleID."'";*/
//echo $li->sql."<br/><br/>";

$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = "IP25_table";
$li->count_mode = 1;
$li->column_array = array(0,0,0,0,0,0);
$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['GroupMember'])."</th>\n";
$li->column_list .= "<th width='45%' >".$li->column($pos++, $Lang['Appraisal']['ARForm'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Appraisal']['ReportOutstanding']['Filler'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['Appraisal']['SpecialRolesIsActive'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, "")."</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, "").$indexVar['libappraisal_ui']->Get_Checkbox("statusAll", "", $value='', $defaultCheck, $thisClass='statusChangeCheckbox', "", "Check_All_Options_By_Class('statusChangeCheckbox', this.checked);")."</th>\n";
$htmlAry['dataTable1'] = $li->display();

$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('grpID', 'grpID', $grpID);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('recordIDList', 'recordIDList', '');
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('batchIDList', 'batchIDList', '');
$htmlAry['contentTbl1'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================

// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>