<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */
$connection = new libgeneralsettings();
$currentDate = date("Y-m-d");
$a = $indexVar['libappraisal']->getAcademicYearTerm($currentDate);

// get Current Cycle Description
//$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".IntegerSafe($a[0]["AcademicYearID"]).";";
$sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE CycleStart <= '".$currentDate."' AND CycleClose >= '".$currentDate."';";
$a = $connection->returnResultSet($sql);
$cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
$cycleDesc = Get_Lang_Selection($a[0]["DescrChi"],$a[0]["DescrEng"]);


# Page Title
$titleBar = "<div style=\"display:inline;float:left\"><span class=\"contenttitle\" style=\"vertical-align:bottom\">".$Lang['Appraisal']['GroupRecord']."</span></div>";;
$TAGS_OBJ[] = array($titleBar);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$x = "";
//$cycleMsg = ($cycleDesc!="")?sprintf($Lang['Appraisal']['ARCycleStart'],$cycleDesc):"";
//$x .= $cycleMsg."<br/><br/>"."\r\n";
//$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($Lang['Appraisal']['ARMyAppraisal'])."<br/>\r\n";


$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("GrpName","GrpLink");

$GrpName = ($intranet_session_language=='en')?"DescrEng":"DescrChi";
$li -> sql ="SELECT $GrpName as GrpName, CONCAT('<a id=\"',gs.GrpID,'\" class=\"tablelink\" href=\"javascript:goView(',gs.GrpID,')\">','".$Lang['Appraisal']['ARFormViewMember']."','</a>') as GrpLink From INTRANET_PA_C_GRPSUM gs
			 INNER JOIN INTRANET_PA_C_GRPMEM gm ON gs.GrpID=gm.GrpID AND gm.IsLeader=1
			 WHERE gm.UserID='".$_SESSION['UserID']."' AND gs.CycleID='".$cycleID."'";
//echo $li->sql."<br/><br/>";

$li->no_col = sizeof($li->field_array)+1;
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='50%' >".$li->column($pos++, $Lang['Appraisal']['ARFormStfGrp'])."</th>\n";
$li->column_list .= "<th width='50%' >".$li->column($pos++, "")."</th>\n";
$htmlAry['dataTable1'] = $li->display();

$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $li->pageNo);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('order', 'order', $li->order);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('field', 'field', $li->field);
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$x .= $indexVar['libappraisal_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $li->page_size);

$htmlAry['contentTbl1'] = $x;

// ============================== Transactional data ==============================

// ============================== Define Button ==============================

// ============================== Define Button ==============================
// ============================== Custom Function ==============================

// ============================== Custom Function ==============================
?>