<?php
// ============================== Related Tables ==============================
// 
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
include_once($PATH_WRT_ROOT."includes/appraisal/gen_form_class.php");

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['AppraisalRecords']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
//$btnAry[] = array('print', 'javascript:goPrint()');
//$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$rlsNo = $_GET["rlsNo"];
$recordID = $_GET["recordID"];
$batchID = $_GET["batchID"];
$grpID = $_GET["grpID"];

$currentDate = date("Y-m-d");

$connection = new libgeneralsettings();

$sql = "Select CanCallModify From INTRANET_PA_C_GRPMEM WHERE UserID='".$_SESSION['UserID']."' AND GrpID='".$grpID."' AND IsLeader=1";
$isLeader = current($connection->returnVector($sql));

$GrpName = ($intranet_session_language=='en')?"DescrEng":"DescrChi";
$memberName = ($intranet_session_language=='en')?"u.EnglishName":"IF(u.ChineseName='', u.EnglishName, u.ChineseName)";
$sql = "SELECT $GrpName as GroupName, $memberName as MemberName 
		FROM INTRANET_PA_T_FRMSUB  ptfsb
		INNER JOIN INTRANET_PA_T_FRMSUM ptfsm ON ptfsb.RecordID=ptfsm.RecordID
		INNER JOIN INTRANET_PA_C_GRPSUM pcgs ON pcgs.GrpID=ptfsm.GrpID
		INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON ptfsm.UserID=u.UserID
		WHERE ptfsm.GrpID='".$grpID."' AND ptfsb.RecordID='".$recordID."' AND ptfsb.RlsNo='".$rlsNo."' AND ptfsb.BatchID='".$batchID."'";
$a = $connection->returnResultSet($sql);
$navigationAry[] = array($Lang['Appraisal']['GroupRecord'], 'javascript: goBackList();');
$navigationAry[] = array($a[0]['GroupName'], 'javascript: goBack('.$grpID.');');
$navigationAry[] = array($indexVar['libappraisal']->displayChinese($a[0]['MemberName']));
$htmlAry['navigation'] = $indexVar['libappraisal_ui']->GET_NAVIGATION_IP25($navigationAry);

$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();

$sql = "SELECT iptf.RecordID,UserID,AprID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppRoleID,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
		EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,YearClassID,SubjectID,TeachLang,NeedSubj,NeedClass,FillerID,CycleStart,CycleClose,NeedSubjLang,ModDateFr,ModDateTo,ModRemark
		FROM(
			SELECT RecordID,CycleID,TemplateID,UserID,AprID FROM INTRANET_PA_T_FRMSUM WHERE RecordID=".IntegerSafe($recordID)."
		) as iptf
		INNER JOIN(
			SELECT TemplateID,".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FormTitle,"
			.$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FormCode,"
			.$indexVar['libappraisal']->getLangSQL("HdrRefChi","HdrRefEng")." as HdrRef,"
			.$indexVar['libappraisal']->getLangSQL("ObjChi","ObjEng")." as Obj,"
			.$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,
			AppTgtChi,AppTgtEng,NeedSubj,NeedClass,NeedSubjLang
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON iptf.TemplateID=ipsf.TemplateID
		INNER JOIN(
			SELECT CycleID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as CycleDescr,AcademicYearID,DATE_FORMAT(CycleStart,'%Y-%m-%d') as CycleStart,DATE_FORMAT(CycleClose,'%Y-%m-%d') as CycleClose
			FROM INTRANET_PA_C_CYCLE
		) as ipcc ON iptf.CycleID=ipcc.CycleID
		INNER JOIN(
			SELECT BatchID,CycleID,DATE_FORMAT(EditPrdFr,'%Y-%m-%d') as EditPrdFr,DATE_FORMAT(EditPrdTo,'%Y-%m-%d') as EditPrdTo,AppRoleID FROM INTRANET_PA_C_BATCH WHERE BatchID=".IntegerSafe($batchID)."
		) as ipcb ON ipcc.CycleID=ipcb.CycleID
		INNER JOIN(
			SELECT RlsNo,RecordID,BatchID,SubDate,FillerID,DATE_FORMAT(ModDateFr,'%Y-%m-%d') as ModDateFr,DATE_FORMAT(ModDateTo,'%Y-%m-%d') as  ModDateTo,ModRemark FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".IntegerSafe($rlsNo). " AND RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)."
		) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
		LEFT JOIN(
			SELECT RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang FROM INTRANET_PA_T_SLF_HDR WHERE RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID)."
		) as iptsh ON iptf.RecordID=iptsh.RecordID AND iptfrmSub.RlsNo=iptsh.RlsNo AND iptfrmSub.BatchID=iptsh.BatchID;";
//echo $sql."<br/>";
$header=$connection->returnResultSet($sql);
$templateID=$header[0]["TemplateID"];
$cycleID=$header[0]["CycleID"];
$cycleStart=$header[0]["CycleStart"];
$cycleClose=$header[0]["CycleClose"];
$userID = $header[0]["UserID"];
$fillerID = $header[0]["FillerID"];
$aprID = $header[0]["AprID"];
$canEdit=($header[0]["OverdueDate"]=="0"&&$header[0]["SubDate"]=="")?"1":"0";
$academicYearID=$header[0]["AcademicYearID"];
$appRole=Get_Lang_Selection($header[0]["AppTgtChi"],$header[0]["AppTgtEng"]);
$modDateFr=$header[0]["ModDateFr"];
$modDateTo=$header[0]["ModDateTo"];
$modRemark=$header[0]["ModRemark"];
$editPrdFr=$header[0]["EditPrdFr"];
$editPrdTo=$header[0]["EditPrdTo"];
$appRoleID=$header[0]["AppRoleID"];
/*
$sql="SELECT RecordID,BatchID,SubDate,CASE WHEN SubDate IS NULL OR (NOW()<=SubDate) THEN 1 ELSE 0 END as CanEdit FROM INTRANET_PA_T_FRMSUB WHERE RecordID=".IntegerSafe($recordID)." AND BatchID=".IntegerSafe($batchID).";";
$a=$connection->returnResultSet($sql);
*/
//======================================================================== View Related Form ========================================================================//
$sql = "SELECT RelatedTemplates FROM INTRANET_PA_C_FRMSEL WHERE TemplateID='".$templateID."' AND CycleID=".IntegerSafe($cycleID);
$relatedTemplateList = current($connection->returnVector($sql));
if(!empty($relatedTemplateList)){
	$sql = "SELECT TemplateID,CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
		FROM INTRANET_PA_S_FRMTPL WHERE IsActive=1 AND TemplateID IN (".$relatedTemplateList.")";
	$relatedTemplateData = $connection->returnArray($sql);
	foreach($relatedTemplateData as $rtd){	
		$btnAry[] = array('view_stat', 'javascript:goViewRelated('.$rtd['TemplateID'].')',  Get_Lang_Selection($rtd["FrmInfoChi"],$rtd["FrmInfoEng"]));
	}
	$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
}
//======================================================================== Section ========================================================================//
$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NULL
		) as ipsf ORDER BY DisplayOrder;
		";
//echo $sql."<br/><br/>";
$sectionHeader=$connection->returnResultSet($sql);

// prepare the QID for that form
$parSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($sectionHeader,"SecID");
$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
		FROM(
			SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
			$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NOT NULL
			AND ParentSecID IN (".$parSecID.")
		) as ipsf";
//echo $sql."<br/><br/>";
$y=$connection->returnResultSet($sql);
$subSecID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"SecID");
$sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,".
			"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (".$subSecID.")";
$y=$connection->returnResultSet($sql);
$qCatID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QCatID");
$sql="SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QCatID IN (".$qCatID.")";
$y=$connection->returnResultSet($sql);
$qID=$indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y,"QID");
// prepare the QID for that form


//======================================================================== Personal Master ========================================================================//

$x .= "<div class=\"formTitle\">";
$x .= $header[0]["FormTitle"];
if($header[0]["CycleDescr"]!=""){
$x .= "(".$header[0]["CycleDescr"].")";
}
$x .= "</div>"."\r\n";
$x .= "<div>";
	if($header[0]["FormCode"]!=""&&$header[0]["Obj"]!=""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]." - ".$header[0]["Obj"]."</span>";
	}
	else if($header[0]["FormCode"]!=""&&$header[0]["Obj"]==""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["FormCode"]."</span>";
	}
	else if($header[0]["FormCode"]==""&&$header[0]["Obj"]!=""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\">".$header[0]["Obj"]."</span>";
	}
	else if($header[0]["FormCode"]==""&&$header[0]["Obj"]==""){
		$x .= "<span class=\"formObjective\" style=\"float: left;\"></span>";
	}
	if($header[0]["Descr"]!=""){
		$x .= "<br/><br/><span>".$header[0]["Descr"]."</span>";
	}
	$x .= "<span class=\"formRefHeader\" style=\"float: right;\">".$header[0]["HdrRef"]."</span>";
	$x .="</div>"."\r\n";
$x .= "<div>".$header[0]["Descr"]."</div>".($header[0]["Descr"]!="")?"<br/>":""."\r\n";

$sql = "SELECT UserID,EnglishName,ChineseName,Gender
		FROM(
			SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID=".IntegerSafe($userID)."
		) as iu
		";
$iu=$connection->returnResultSet($sql);
$tgtName = ($_SESSION['intranet_session_language']=="en")?$Lang['Appraisal']['ARFormTgtName'].$header[0]["AppTgtEng"]:$header[0]["AppTgtChi"].$Lang['Appraisal']['ARFormTgtName'];
$x .= "<table class=\"form_table_v30\">";
if($iu[0]["ChineseName"]==""){
	$x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\">".$iu[0]["EnglishName"]."</td></tr>"."\r\n";
}else{
	$x .= "<tr><td class=\"field_title\">".$tgtName."</td><td colspan=\"3\">".$iu[0]["EnglishName"]."(".$iu[0]["ChineseName"].")"."</td></tr>"."\r\n";
}
if($header[0]["NeedSubj"]=="1"){
	$subjectSQL=$indexVar['libappraisal']->getAllSubject();	
	$x .="<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormSubject']."</td><td>";
	for($i=0;$i<sizeof($subjectSQL);$i++){
		if($header[0]["SubjectID"] == $subjectSQL[$i]["SubjectID"]){
			$subjName = Get_Lang_Selection($subjectSQL[$i]["SubjectTitleB5"],$subjectSQL[$i]["SubjectTitleEN"]);
		}
	}
	$subjName=($subjName=="")?"-":$subjName;
	$x .= $subjName;
	$x .= "</td>";
	if($header[0]["NeedSubjLang"]=="1"){
		$x .="<td class=\"field_title\">".$Lang['Appraisal']['ARFormSubjectMedium']."</td><td>";
		$x .= $header[0]["TeachLang"];
		$x .="</td>";
	}
	$x .= "</tr>";
}
if($header[0]["NeedClass"]=="1"){
	$yearClassSQL=$indexVar['libappraisal']->getAllYearClass($academicYearID);
	$x .="<tr><td class=\"field_title\">".$Lang['Appraisal']['ARFormYearClass']."</td><td colspan=\"3\">";
	for($i=0;$i<sizeof($yearClassSQL);$i++){
		if($header[0]["YearClassID"] == $yearClassSQL[$i]["YearClassID"]){
			$yearClass = Get_Lang_Selection($yearClassSQL[$i]["ClassTitleB5"],$yearClassSQL[$i]["ClassTitleEN"]);
		}
	}
	$yearClass=($yearClass=="")?"-":$yearClass;
	$x .= $yearClass;
	$x .= "</td></tr>";;
}
$x .= "</table>".($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0")?"<br/>":"\r\n";;
if($header[0]["NeedClass"]!="0" || $header[0]["NeedClass"]!="0"){
	$appTag = "<br/>";
}
else{
	$appTag = "\r\n";
}
$x .= "</table>".$appTag;
//echo sizeof($sectionHeader);
//======================================================================== Personal Master ========================================================================//
for($h=0;$h<sizeof($sectionHeader);$h++){
	// to check the section security
	$sectionSecurity=$indexVar['libappraisal']->getSectionUserRole($userID,$recordID,$batchID,$sectionHeader[$h]["SecID"],$rlsNo);
	if($sectionSecurity[0]["CanFill"]==0 && $h<sizeof($sectionHeader)-1){
		$nonEditDivID .= "#SecID_".$sectionHeader[$h]["SecID"].",";
	}
	else if($sectionSecurity[0]["CanFill"]==0 && $h==sizeof($sectionHeader)-1){
		$nonEditDivID .= "#SecID_".$sectionHeader[$h]["SecID"];
	}
	if($sectionSecurity[0]["CanBrowse"]==1){
		if(sizeof($sectionSecurity)>0){	
			$x .= "<div id=\"SecID_".$sectionHeader[$h]["SecID"]."\" name=\"SecID_".$sectionHeader[$h]["SecID"]."\">";		
			if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]!=""){
				$x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"]."-".$sectionHeader[$h]["SecTitle"])."<br/>\r\n";
			}
			else if($sectionHeader[$h]["SecCod"]!="" && $sectionHeader[$h]["SecTitle"]==""){
				$x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecCod"])."<br/>\r\n";
			}
			else if($sectionHeader[$h]["SecCod"]=="" && $sectionHeader[$h]["SecTitle"]!=""){
				$x .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($sectionHeader[$h]["SecTitle"])."<br/>\r\n";
			}
			$x .= "</div><br/>\r\n";
			$x .= "<table>";
			$x .= "<tr><td>".nl2br($sectionHeader[$h]["SecDescr"])."</td></tr></table>".nl2br(($sectionHeader[$h]["SecDescr"]!="")?"<br/>":""."\r\n");
			//======================================================================== Sub Section ========================================================================//
			$sql="SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
					FROM(
						SELECT SecID,TemplateID,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCod,".
						$indexVar['libappraisal']->getLangSQL("SecDescrChi","SecDescrEng")." as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=".IntegerSafe($templateID)." AND ParentSecID IS NOT NULL
						AND ParentSecID=".IntegerSafe($sectionHeader[$h]["SecID"])."
					) as ipsf 		
					ORDER BY DisplayOrder;
					";
			//echo $sql."<br/><br/>";
			$a=$connection->returnResultSet($sql);
			
			for($i=0;$i<sizeof($a);$i++){	
				if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]!=""){
					$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"]."-".$a[$i]["SecTitle"])."\r\n";
				}
				else if($a[$i]["SecCod"]!="" && $a[$i]["SecTitle"]==""){
					$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecCod"])."\r\n";
				}
				else if($a[$i]["SecCod"]=="" && $a[$i]["SecTitle"]!=""){
					$x .= $indexVar['libappraisal_ui']->Get_Form_Sub_Title_v30($a[$i]["SecTitle"])."\r\n";
				}
				$x .= "<table>";
				$x .= "<tr><td>".(($a[$i]["SecDescr"]!="")? $a[$i]["SecDescr"]:""."\r\n")."</td></tr></table>".(($a[$i]["SecDescr"]!="")?"<br/>":""."\r\n");
				
				$sql="SELECT QCatID,SecID,".$indexVar['libappraisal']->getLangSQL("QCatCodChi","QCatCodEng")." as QCatCod,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as QCatDescr,QuesDispMode,".
					"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID=".$a[$i]["SecID"]." ORDER BY DisplayOrder;";
				//echo $sql."<br/><br/>";
				$b=$connection->returnResultSet($sql);
				//echo sizeof($b);
				for($j=0;$j<sizeof($b);$j++){
					$x .= "<table width=\"100%\"><tr><td valign=\"top\" style=\"width: 5px;\">".$b[$j]["QCatCod"]."</td><td>".nl2br($b[$j]["QCatDescr"])."</td></tr></table>";
					//======================================================================== Question Category ========================================================================//
					if($b[$j]["QuesDispMode"]=="1"||$b[$j]["QuesDispMode"]=="2"){						
						$x .= "<div style=\"padding-left:20px;\"><table class=\"common_table_list\" width=\"100%\">";
					}
					else{
						$x .= "<div style=\"padding-left:20px;\"><table width=\"100%\">";
					}
					$x .= getSDFSectionContentPrint($connection,"",$a[$i]["SecID"],$b[$j]["QCatID"],$recordID,$rlsNo,$batchID);
					$x .= "</table></div><br/>\r\n";					
				}
			}
			//$x .="</div>";
		}
	}
}
$htmlAry['contentTbl'] = $x;
// ============================== Define Button ==============================
//if($canEdit=="1"){
	//$htmlAry['submitBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnTempSave'], "button", "goSubmit()", 'submitBtn');
//}
//else{
	$sql = "SELECT NameChi,NameEng FROM INTRANET_PA_S_APPROL WHERE AppRoleID=".$appRoleID.";";
	$w = $connection->returnResultSet($sql);
	$sql = "SELECT UserID,EnglishName,ChineseName,Gender
		FROM(
			SELECT UserID,EnglishName,ChineseName,Gender FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID=".IntegerSafe($fillerID)."
		) as iu
		";
	$q=$connection->returnResultSet($sql);
	
	$z .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormSubmission'])."<br/>\r\n";
	$z .= "<div id=\"formSubmissionInfoLayer\">";
	$z .= "<table id=\"formSubmissionInfoTable\" class=\"form_table_v30\">";
	$z .= "<thead>";
	$z .= "<tr>";
	$z .= "</tr></thead>";
	$z .= "<tbody>";
	$z .= "<tr>";
	$z .= "<td class=\"field_title\">".Get_Lang_Selection($w[0]["NameChi"],$w[0]["NameEng"])."</td>";
	$z .= "<td>";
	$z .= Get_Lang_Selection($q[0]["ChineseName"],$q[0]["EnglishName"]);
	$z .= "</td>";
	$z .= "</tr>";
	$z .= "</thead>";
	$z .= "</table>";
	$z .= "</div>";
//}
$htmlAry['backBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack(".$grpID.")", 'backBtn');

// ============================== Define Button ==============================
//======================================================================== Form Modification ========================================================================//

// only the user who has the right of the administrator can access the below content
if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-eAppraisal"] == "1" || $isLeader) && ($cycleStart<=$currentDate && $currentDate<=$cycleClose)){
	if($modDateFr!="" && $modDateTo != ""){
		$z0 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormModification'])."<br/><br/>\r\n";
		$z0 .= "<div id=\"formModification\">";
		$z0 .= "<table class=\"form_table_v30\">";
		$z0 .= "<tbody>";		
//		$z0 .= "<tr>";
//		$z0 .= "<td></td>";
//		$z0 .= "<td>".$Lang['Appraisal']['GroupModifying']."</td>";			
//		$z0 .= "</tr>";
		$z0 .= "<tr>";
		$z0 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormModDate']."</td>";
		$z0 .= "<td>";
		$z0 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER("modDateFr", $modDateFr,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=true, $cssClass="textboxnum");
		$z0 .= " - ";
		$z0 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER("modDateTo", $modDateTo,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=true, $cssClass="textboxnum");
		$z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateOutCycleWarnDiv', $Lang['Appraisal']['TemplateSample']['DateOutCycle'], $Class='warnMsgDiv')."\r\n";
		$z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateInvalidRangeWarnDiv', $Lang['Appraisal']['TemplateSample']['ModDateInvalidRange'], $Class='warnMsgDiv')."\r\n";
		$z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateOutEditWarnDiv', $Lang['Appraisal']['TemplateSample']['DateOutEdit'], $Class='warnMsgDiv')."\r\n";
		$z0 .= "</td>";
		$z0 .= "</tr>";
		$z0 .= "<tr>";
		$z0 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormRemarks']."</td>";
		$z0 .= "<td>";
		$z0 .= $indexVar['libappraisal_ui']->GET_TEXTAREA("modRemark", $modRemark, $taCols=70, $taRows=5, $OnFocus = "", $readonly = true, $other='', $class='', $taID='', $CommentMaxLength='');
		$z0 .= "</td>";
		$z0 .= "</tr>";
		$z0 .= "</div>";
		$z0 .= "</table>";
	}else{	
		$htmlAry['modBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnModify'], "button", "goModify()", 'modifyBtn');
		$z0 .= $indexVar['libappraisal_ui']->GET_NAVIGATION2_IP25($Lang['Appraisal']['ARFormModification'])."<br/><br/>\r\n";
		$z0 .= "<div id=\"formModification\">";
		$z0 .= "<table class=\"form_table_v30\">";
		$z0 .= "<tbody>";
		$z0 .= "<tr>";
		$z0 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormModDate']."</td>";
		$z0 .= "<td>";
		$z0 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER("modDateFr", $modDateFr,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
		$z0 .= " - ";
		$z0 .= $indexVar['libappraisal_ui']->GET_DATE_PICKER("modDateTo", $modDateTo,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="textboxnum");
		$z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateOutCycleWarnDiv', $Lang['Appraisal']['TemplateSample']['DateOutCycle'], $Class='warnMsgDiv')."\r\n";
		$z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateInvalidRangeWarnDiv', $Lang['Appraisal']['TemplateSample']['ModDateInvalidRange'], $Class='warnMsgDiv')."\r\n";
		$z0 .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('ModDateOutEditWarnDiv', $Lang['Appraisal']['TemplateSample']['DateOutEdit'], $Class='warnMsgDiv')."\r\n";
		$z0 .= "</td>";
		$z0 .= "</tr>";
		$z0 .= "<tr>";
		$z0 .= "<td class=\"field_title\">".$Lang['Appraisal']['ARFormRemarks']."</td>";
		$z0 .= "<td>";
		$z0 .= $indexVar['libappraisal_ui']->GET_TEXTAREA("modRemark", $modRemark, $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='', $class='', $taID='', $CommentMaxLength='');
		$z0 .= "</td>";
		$z0 .= "</tr>";
		$z0 .= "</div>";
		$z0 .= "</table>";
	}
	$htmlAry['contentTbl3'] = $z0;
}
else{

}
//======================================================================== Form Modification ========================================================================//
//======================================================================== Form Submission ========================================================================//
/*if($canEdit=="1"){
	$z .= "<div style=\"border-top: 1px dashed #CCCCCC;\">";
	$z .= $indexVar['libappraisal']->getFormSubmission($Lang,Get_Lang_Selection($iu[0]["ChineseName"],$iu[0]["EnglishName"]),$appRole);
	$z .= "</div>";
}*/
$z .= "<input type=\"hidden\" id=\"rlsNo\" name=\"rlsNo\" value='".IntegerSafe($rlsNo)."'/>";
$z .= "<input type=\"hidden\" id=\"recordID\" name=\"recordID\" value='".IntegerSafe($recordID)."'/>";
$z .= "<input type=\"hidden\" id=\"batchID\" name=\"batchID\" value='".IntegerSafe($batchID)."'/>";
$z .= "<input type=\"hidden\" id=\"grpID\" name=\"grpID\" value='".IntegerSafe($grpID)."'/>";
$z .= "<input type=\"hidden\" id=\"cycleID\" name=\"cycleID\" value='".IntegerSafe($cycleID)."'/>";
$z .= "<input type=\"hidden\" id=\"cycleStart\" name=\"cycleStart\" value='".$cycleStart."'/>";
$z .= "<input type=\"hidden\" id=\"cycleClose\" name=\"cycleClose\" value='".$cycleClose."'/>";
$z .= "<input type=\"hidden\" id=\"editPrdFr\" name=\"editPrdFr\" value='".$editPrdFr."'/>";
$z .= "<input type=\"hidden\" id=\"editPrdT\" name=\"editPrdT\" value='".$editPrdTo."'/>";
$z .= "<input type=\"hidden\" id=\"userID\" name=\"userID\" value='".IntegerSafe($userID)."'/>";
$z .= "<input type=\"hidden\" id=\"templateID\" name=\"templateID\" value='".$templateID."'/>";
$z .= "<input type=\"hidden\" id=\"qID\" name=\"qID\" value='".$qID."'/>";
$z .= "<input type=\"hidden\" id=\"nonEditDivID\" name=\"nonEditDivID\" value='".$nonEditDivID."'/>";
$htmlAry['contentTbl2'] = $z;
//======================================================================== Form Submission ========================================================================//


function getSDFSectionContentPrint($connection,$prefix,$secID,$qCatID,$recordID,$rlsNo,$batchID){
	global $intranet_root, $Lang;
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();

	$sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode FROM INTRANET_PA_S_QCAT WHERE SecID=".$secID." AND QCatID=".$qCatID."
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID,".$indexVar['libappraisal']->getLangSQL("QCodChi","QCodEng")." as QCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,DisplayOrder,InputType,IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk,".$indexVar['libappraisal']->getLangSQL("RmkLabelChi","RmkLabelEng")." as Rmk,
						DisplayOrder as QuesDisplayOrder,".$indexVar['libappraisal']->getLangSQL("RmkLabelChi","RmkLabelEng")." as RmkLabel
						FROM INTRANET_PA_S_QUES WHERE QCatID=".$qCatID."
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
	$a=$connection->returnResultSet($sql);
	//echo ($sql)."<br/><br/>";
	$quesDispMode=$a[0]["QuesDispMode"];
	
	// check any content in the first content
	$hasContent = false;
	$sql="SELECT SUM(LENGTH(".$indexVar['libappraisal']->getLangSQL("QCodChi","QCodEng").")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID=".$qCatID.";";
	$x=$connection->returnResultSet($sql);
	if($x[0]["content"] > 0){
		$hasContent = true;
	}

	if($quesDispMode==1){
		$sql = "SELECT MarkID,QCatID,".$indexVar['libappraisal']->getLangSQL("MarkCodChi","MarkCodEng")." as MarkCodDes,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,Score,DisplayOrder,".
				$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr
						FROM INTRANET_PA_S_QLKS WHERE QCatID=".$qCatID."
						ORDER BY DisplayOrder";
				//echo $sql."<br/><br/>";
				$b=$connection->returnResultSet($sql);
				$descWidth ="20%";
				
				$content.="<tr>";
				if($hasContent == true){
					$content .="<th style=\"width:5%\"></th>";
				}
				$content .= "<th style=\"width:".$descWidth."\"></th>";
				$dnmtr = (sizeof($b)==0)?1:sizeof($b);
				$thWidth=60/$dnmtr;
				for($j=0;$j<sizeof($b);$j++){
					$content.="<th style=\"width:".floor($thWidth)."%\">".nl2br($b[$j]["Descr"])."</th>";
				}
				$content.="</tr>";
				for($i=0;$i<sizeof($a);$i++){
					$intSecID=$a[$i]["SecID"];
					$intQusCodID=$i+1;
					$qID=$a[$i]["QID"];
					//$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND QID=".$qID.";";
					$sql="SELECT dtl.RecordID,dtl.BatchID,dtl.QID,dtl.QAnsID,dtl.MarkID,dtl.Remark,dtl.Score,DATE_FORMAT(dtl.DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL dtl 
						INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON dtl.RlsNo = frmsub.RlsNo AND dtl.RecordID = frmsub.RecordID AND dtl.BatchID = frmsub.BatchID
						WHERE dtl.RlsNo=".$rlsNo." AND dtl.RecordID=".$recordID." AND dtl.QID=".$qID." AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW());";
					//echo $sql."<br/><br/>";;
					$c=$connection->returnResultSet($sql);
					$c = $indexVar['libappraisal']->getQuestionDataByCmpArr($c);
					
					$hasRemark=$a[$i]["HasRmk"];
					$rmkLabel=$a[$i]["RmkLabel"];
					$rowSpan=($hasRemark==1 && $c[0]["Remark"]!="")?"2":"1";
					
					$content.="<tr>";
					if($hasContent == true){
						$content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
					}
					if(strstr($a[$i]["Descr"], '<br')){
						$a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
					}else{
						$a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
					}
					$content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."</td>";
					for($j=0;$j<sizeof($b);$j++){
						$content.="<td>";
						$content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"],"qID_".$a[$i]["QID"], $Value=$b[$j]["MarkID"], $isChecked=($c[0]["MarkID"]==$b[$j]["MarkID"])?1:0, $Class="", $Display=$b[$j]["MarkCodDes"], $Onclick="",$isDisabled=1);
						$content.="</td>";
					}
					//$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$c[0]["MarkID"]."\">";
					$content.="</tr>";
					if($hasRemark==1 && $c[0]["Remark"]!=""){
						$content.="<tr>";
						$content.="<td colspan=\"".sizeof($b)."\">";
						if($rmkLabel!=""){
							$content.= $rmkLabel."<br/>";
						}
						$content.=nl2br($c[0]["Remark"])."</td>";
						$content.="</tr>";
					}
				}
	}
	else if($quesDispMode==2){
		$sql = "SELECT MtxFldID,QCatID,".$indexVar['libappraisal']->getLangSQL("FldHdrChi","FldHdrEng")." as FldHdrDes,InputType,DisplayOrder
					FROM INTRANET_PA_S_QMTX  WHERE QCatID=".$qCatID."
					ORDER BY DisplayOrder";
		//echo $sql."<br/><br/>";
		$b=$connection->returnResultSet($sql);
		for($i=0;$i<sizeof($a);$i++){
			if($a[$i]["QCodDes"]!=""){
				$hasQuestionDisplay = true;
			}
		}
		$descWidth =100;
		$content.="<tr>";
		if($hasContent == true){
			$content.="<th style=\"width:5%\"></th>";
			$descWidth = 95;
		}	
		$dnmtr = (sizeof($b)==0)?1:sizeof($b);
		if($hasQuestionDisplay){
			$thWidth=$descWidth/($dnmtr+1);
			$content.="<th style=\"width:".floor($thWidth)."%\"></th>";
		}else{
			$thWidth=$descWidth/$dnmtr;		
			$content.="<th style=\"width:1%\"></th>";
		}
		for($j=0;$j<sizeof($b);$j++){
			$content.="<th style=\"width:".floor($thWidth)."%\">".$b[$j]["FldHdrDes"]."</th>";
		}
		$content .= "</tr>";
		for($i=0;$i<sizeof($a);$i++){
			$intSecID=$a[$i]["SecID"];
			$intQusCodID=$i+1;
			$qID=$a[$i]["QID"];
			//$sql="SELECT RecordID,BatchID,QID,MtxFldID,Score,TxtAns,DateAns,Remark FROM INTRANET_PA_T_SLF_MTX WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND QID=".$qID.";";
			$sql="SELECT mtx.RecordID,mtx.BatchID,mtx.QID,mtx.MtxFldID,mtx.Score,mtx.TxtAns,mtx.DateAns,mtx.Remark FROM INTRANET_PA_T_SLF_MTX mtx
				INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON mtx.RlsNo = frmsub.RlsNo AND mtx.RecordID = frmsub.RecordID AND mtx.BatchID = frmsub.BatchID
				WHERE mtx.RlsNo=".$rlsNo." AND mtx.RecordID=".$recordID." AND mtx.QID=".$qID." AND (frmsub.SubDate IS NOT NULL OR frmsub.EditPrdTo < NOW())";
			//echo $sql."<br/><br/>";;
			$c=$connection->returnResultSet($sql);
			$c = $indexVar['libappraisal']->getQuestionDataByCmpArr($c);
			$hasRemark=$a[$i]["HasRmk"];
			$rmkLabel=$a[$i]["RmkLabel"];
			$rowSpan=($hasRemark==1 && $c[0]["Remark"]!="")?"2":"1";
			$content.="<tr>";
			if($hasContent == true){
				$content.="<td valign=\"top\" rowspan=\"".$rowSpan."\">".$a[$i]["QCodDes"]."</td>";
			}
			if(strstr($a[$i]["Descr"], '<br')){
				$a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
			}else{
				$a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
			}
			$content.="<td rowspan=\"".$rowSpan."\">".nl2br($a[$i]["Descr"])."</td>";
			for($j=0;$j<sizeof($b);$j++){
				$content.="<td>";
				if($b[$j]["InputType"]==0){
					$unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
					$txtAns = $c[0]["TxtAns"];
					$txtAnsArr = explode("#$^",$txtAns);
					$content.=$txtAnsArr[$j].$unit;
				}
				else if($b[$j]["InputType"]==1){
					$unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
					$scoreAns = $c[0]["Score"];
					$scoreAnsArr = explode("#$^",$scoreAns);
					$content.=$scoreAnsArr[$j].$unit;
				}
				else if($b[$j]["InputType"]==2){
					$datAns = $c[0]["DateAns"];
					$datAnsArr = explode("#$^",$datAns);
					$content.=nl2br($datAnsArr[$j]);
				}
				else if($b[$j]["InputType"]==3){
					$unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
					$txtAns = $c[0]["TxtAns"];
					$txtAnsArr = explode("#$^",$txtAns);
					$content.=nl2br($txtAnsArr[$j]).$unit;
				}
				$content.="<input type=\"hidden\" id=\"MarkID_".$a[$i]["QID"]."\" name=\"MarkID_".$a[$i]["QID"]."\" value=\"".$b[$j]["MtxFldID"]."\">";
				$content.="</td>";
			}
			$content.="</tr>";
			if($hasRemark==1 && $c[0]["Remark"]!=""){
				$content.="<tr>";
				$content.="<td colspan=\"".sizeof($b)."\">";
				if($rmkLabel!=""){
					$content.= $rmkLabel."<br/>";
				}
				$content.=nl2br($c[0]["Remark"])."</td>";
				$content.="</tr>";
			}
		}
	}
	else if($quesDispMode==0){
		for($i=0;$i<sizeof($a);$i++){
			$intSecID=$a[$i]["SecID"];
			$intQusCodID=$i+1;
			$qID=$a[$i]["QID"];
			$sql = "SELECT IF(ParentSecID IS NULL, SecID,ParentSecID) as CurrentSection FROM INTRANET_PA_S_FRMSEC WHERE SecID='".$intSecID."'";
			$currentSection = current($connection->returnVector($sql));
			$sql = "SELECT AppRoleID FROM INTRANET_PA_S_SECROL WHERE SecID='".$currentSection."' AND CanFill=1";
			$appRoleID = $connection->returnVector($sql);
			$sql = "SELECT BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND AppRoleID IN (".implode(",",$appRoleID).") AND SecID='".$currentSection."' AND SubDate IS NOT NULL";
			$secBatchID = $connection->returnVector($sql);
			$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns
				FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND BatchID IN (".implode(",",$secBatchID).") AND QID=".$qID.";";
//			$sql="SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,TxtAns,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns
//						FROM INTRANET_PA_T_SLF_DTL WHERE RlsNo=".$rlsNo." AND RecordID=".$recordID." AND QID=".$qID." ORDER BY BatchID;";
			//echo $sql."<br/><br/>";;
			$c=$connection->returnResultSet($sql);
			$c = $indexVar['libappraisal']->getQuestionDataByCmpArr($c);
			if(strstr($a[$i]["Descr"], '<br')){
				$a[$i]["Descr"] = preg_replace( "/\r|\n/", "", $a[$i]["Descr"]);
			}else{
				$a[$i]["Descr"] = preg_replace( "/\r|\n/", "", nl2br($a[$i]["Descr"]));
			}
			$a[$i]["Descr"] = "<span style='line-height:200%;font-weight:bold;'>".$a[$i]["Descr"]."</span>";
			if($a[$i]["InputType"]==0){
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==1){
				$sql="SELECT QAnsID,QID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,".$indexVar['libappraisal']->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID=".$a[$i]["QID"]." ORDER BY DisplayOrder;";
				//echo $sql."<br/><br/>";
				$b=$connection->returnResultSet($sql);
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				for($j=0;$j<sizeof($b);$j++){
					$content.="<tr><td style=\"border:none\"></td>";
					$content.="<td style=\"border:none\">";
					$content.=$indexVar['libappraisal_ui']->Get_Radio_Button("qID_".$a[$i]["QID"], "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=($c[0]["QAnsID"]==$b[$j]["QAnsID"])?1:0, $Class="", $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1,$specialLabel=$b[$j]["QAns"]);
					$content.="</td>";
					$content.="</tr>";
				}
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==2){
				$unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["TxtAns"].$unit."</td>";;
				$content.="</tr>";
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==3){
				$unit=($a[$i]["TxtBoxNote"]!="")?$a[$i]["TxtBoxNote"]:"";
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["Score"].$unit."</td></tr>";
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==4){
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".$c[0]["DateAns"]."</td></tr>";
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==5){
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				if($c[0]["Remark"]!=""){
					$content.="<tr><td style=\"border:none\"></td><td style=\"border:none;width:10%\">".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==6){
				$sql="SELECT QAnsID,QID,".$indexVar['libappraisal']->getLangSQL("DescrChi","DescrEng")." as Descr,".$indexVar['libappraisal']->getLangSQL("QAnsChi","QAnsEng")." as QAns,DisplayOrder
						FROM INTRANET_PA_S_QANS WHERE QID=".$a[$i]["QID"]." ORDER BY DisplayOrder;";
				//echo $sql."<br/><br/>";
				$b=$connection->returnResultSet($sql);
				$content.="<tr>";
				$content.="<td valign=\"top\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td>".nl2br($a[$i]["Descr"])."</td>";
				$content.="</tr>";
				$mcmAnsID = explode(",",$c[0]["TxtAns"]);
				for($j=0;$j<sizeof($b);$j++){
					$content.="<tr><td></td>";
					$content.="<td>";
					$chkBoxChecked = 0;
					for($k=0;$k<sizeof($mcmAnsID);$k++){
						if($b[$j]["QAnsID"] == $mcmAnsID[$k]){
							$chkBoxChecked = 1;
							break;
						}
					}
					$content.=$indexVar['libappraisal_ui']->Get_Checkbox("qID_".$a[$i]["QID"]."_".$j, "qID_".$a[$i]["QID"], $Value=$b[$j]["QAnsID"], $isChecked=$chkBoxChecked, $Class="", $Display=$b[$j]["Descr"], $Onclick="",$isDisabled=1,$isIndented=1,$specialLabel=$b[$j]["QAns"]);
					$content.="</td>";
					$content.="</tr>";
				}
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			else if($a[$i]["InputType"]==7){
				$tmpFilePath = $intranet_root.'/file/appraisal/sdf_file/'.$c[0]["BatchID"].'/'.$c[0]["TxtAns"];
				$tmpFilePathAbs = downloadAttachmentRelativePathToAbsolutePath($tmpFilePath);
					
				$content.="<tr>";
				$content.="<td valign=\"top\" style=\"border:none;width:5%\">".$a[$i]["QCodDes"]."</td>";
				$content.="<td style=\"border:none;width:95%\">";
				$content.=nl2br($a[$i]["Descr"]);
				$content.="</td>";
				$content.="</tr>";
				$content.="<tr><td></td>";
				$content.="<td>";
				
				$content.="<tr><td></td>";
				$content.="<td>";
				//$content.=$c[0]["TxtAns"]."<br/>";
				$fileDataArr = explode("|",$c[0]["TxtAns"]);
				$content.=$Lang['Appraisal']['CycleTemplate']['DownloadFiles'].": <a id='download_"."qID_".$a[$i]["QID"]."' class=\"tablelink\" href=\"/home/download_attachment.php?target_e=".getEncryptedText($tmpFilePathAbs)."&filename_e=".getEncryptedText($fileDataArr[2])."\">".$fileDataArr[2]."</a><br/>";
				$content.="</td>";
				
				if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]!=""){
					$content.="<tr><td></td><td>".nl2br($a[$i]["Rmk"])."</td></tr>";
					if($c[0]["Remark"]!=""){
						$content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
					}
				}
				else if($a[$i]["HasRmk"]==1 && $a[$i]["Rmk"]=="" && $c[0]["Remark"]!=""){
					$content.="<tr><td></td><td>".nl2br($c[0]["Remark"])."</td></tr>";
				}
			}
			$content .= "<tr><td colspan='2' style='height:10px;'></td></tr>";
		}
	}
	return $content;
}




?>