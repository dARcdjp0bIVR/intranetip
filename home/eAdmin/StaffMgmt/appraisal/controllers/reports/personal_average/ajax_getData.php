<?php 
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$markerList = array();
$teacherPerformance = array();
$teacherQuestionList = array();
$questionData = array();
$type=($_GET["Type"]=="")?$_POST["Type"]:$_GET["Type"];
$q=$_GET["?q"];
$cycleID = $_POST['cycleID'];
$templateID = $_POST['templateID'];
$appraiseeID = $_POST['appraiseeID'];

// ============================== get Login User ==============================
if($type=="GetLoginUser"){
	$typeList = "1";		# 1-teacher only
	$name_field = getNameFieldByLang("USR.");
//	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
//		FROM(			
//			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin 
//			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $conds 
//			GROUP BY USR.UserID ORDER BY UserLogin
//		) as a
//		WHERE UserLogin like '%".$q."%'";
	if($junior_mck){
		$name_field_deleted = getNameFieldByLang2("AUR.");
	}else{
		$name_field_deleted = getNameFieldByLang("AUR.");
	}
	
	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
		FROM(			
			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin 
			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)  
			GROUP BY USR.UserID
			UNION
			SELECT AUR.UserID, $name_field_deleted as Name, ycu2.ClassNumber, AUR.UserLogin 
			FROM INTRANET_ARCHIVE_USER AUR LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=AUR.UserID) WHERE AUR.RecordType IN ($typeList)  
			GROUP BY AUR.UserID
		) as a
		WHERE UserLogin like '%".$q."%' ORDER BY UserLogin;
	";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	for ($i=0; $i<sizeof($a); $i++){
		$x .= $a[$i]["UserLogin"]."|".$a[$i]["UserID"]."|".$a[$i]["UserLogin"]."\n";
	}
	echo $x;
}else if($type=="GetLoginUserSelection"){
	$cycleID = $_POST['CycleID'];
	if($cycleID==""){
		echo '';
		die();
	}else{
		$name_field = getNameFieldByLang("USR.");
		$sql = "SELECT Distinct USR.userLogin, USR.UserID,
				CASE 
					WHEN au.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")') 
					ELSE {$name_field} 
				END  as name FROM INTRANET_PA_T_FRMSUM frmsum 
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." USR ON frmsum.UserID=USR.UserID				
				INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
				LEFT JOIN INTRANET_ARCHIVE_USER au ON USR.UserID = au.UserID	
				WHERE frmsum.CycleID='".$cycleID."' AND frmsub.IsLastSub=1";
		$resultList=$connection->returnResultSet($sql);
		$selList = array();
		foreach($resultList as $r){
			array_push($selList, array("id"=>$r['UserID'], "name"=>$r['name']));
		}		
		echo  getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($selList, "id","name","name"), $selectionTags='id="appraiseeID" name="appraiseeID"', $SelectedType="", $all=0, $noFirst=0);	
	}
}else if($type=="GetCycleActiveTemplate"){
	$sql="SELECT ipcf.TemplateID,ipsf.FrmInfoChi,ipsf.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipcf
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
		UNION
		SELECT ipof.TemplateID,ipsf2.FrmInfoChi,ipsf2.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipof
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf2 ON ipof.TemplateID=ipsf2.TemplateID";
	$formList = $connection->returnArray($sql);
	echo getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($formList, "TemplateID","FrmInfoChi","FrmInfoEng"), $selectionTags='id="TemplateID" name="templateID"', $SelectedType=$templateID, $all=0, $noFirst=0);	
}else if($type=="GetFormAppraiseeSel"){
	$sql = "SELECT DISTINCT u.UserID, ".Get_Lang_Selection("u.ChineseName","u.EnglishName")." as UserName FROM INTRANET_PA_T_FRMSUM ptfs
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON ptfs.UserID=u.UserID WHERE CycleID='".$_POST['cycleID']."' AND TemplateID='".$_POST['templateID']."'";
	$userList = $connection->returnArray($sql);
	$filter = "";
	$filter .= "<select name='appraiseeID' id='appraiseeID'>";
	foreach($userList as $ufc){		
		$filter .= "	<option value='".$ufc['UserID']."' ".(($cycleID==$ufc['UserID'])?"selected":"").">".$indexVar['libappraisal']->displayChinese($ufc['UserName'])."</option>";
	}
	$filter .= "</select>";
	echo $filter;		
}else if($type=="GetFormDetailsList"){
	$conds = "";
	if($templateID > "-1"){
		$conds = " AND frmtpl.TemplateID='".$templateID."' ";
	}
	$sql = "SELECT MAX(RlsNo) as RlsNo,ptfsub.RecordID,ptfsub.BatchID,ptfsum.UserID,ptfsum.TemplateID FROM INTRANET_PA_T_FRMSUB ptfsub
				INNER JOIN INTRANET_PA_T_FRMSUM ptfsum ON ptfsub.RecordID=ptfsum.RecordID
				INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON ptfsum.TemplateID=frmtpl.TemplateID
				WHERE frmtpl.TemplateType=1 AND CycleID='$cycleID' AND ptfsum.UserID='$appraiseeID' AND ptfsub.SubDate IS NOT NULL $conds GROUP BY ptfsub.RecordID";				
	$basicInfo = $connection->returnArray($sql);
	$formStatInfo = array();	
	foreach($basicInfo as $bi){
		getSdfStat($connection, $bi['TemplateID'], $bi["UserID"], $bi["RecordID"], $cycleID, $bi["RlsNo"], $bi["BatchID"]);
	}
	//$thisStdStat = current($teacherPerformance);
	$totalFormNo = count($basicInfo);
	if($totalFormNo==0){
		echo "0|".$Lang['General']['NoRecordAtThisMoment'];
	}else{
		$firstRow = current($teacherPerformance[$appraiseeID]);
		echo "<table id='nameLine' class='form_table_v30'><tbody><tr><td class='field_title'>".$Lang['Appraisal']['ARFormTgtName'] ."</td><td>".$firstRow['Name']."</td></tr></tbody></table>";
		foreach($teacherPerformance[$appraiseeID] as $formTemplateID=>$thisStdStat){
			$sql = "SELECT SUM(value) as submitted, COUNT(*) as total FROM (
						SELECT IF(frmsub.SubDate IS NOT NULL, 1, 0) as value, frmsum.RecordID FROM INTRANET_PA_T_FRMSUM frmsum
						INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
						 WHERE frmsum.TemplateID='".$formTemplateID."' AND frmsum.UserID='".$appraiseeID."' AND frmsum.CycleID='".$cycleID."'
						GROUP BY frmsub.RecordID						
					) a;";
			$templateStatistics = current($connection->returnArray($sql));				
			$output = "<br><h1>".$thisStdStat['FormName']." &nbsp;&nbsp;<span style='font-size:12px;'>(已遞交人數/需填寫人數: ".$templateStatistics['submitted']."/".$templateStatistics['total'].")</span></h1>";
			if(count($thisStdStat['Section']) > 0){
				foreach($thisStdStat['Section'] as $section){
					$output .= "<h2>".$section['SectionName']."</h2>";
					$subSectionContent = "";
					$subSectionComment = "";
					if(count($section['SubSection']) > 0){
						$hasQData = false;
						$subSectionHasContent = true;
						$subSectionHasComment = true;
						foreach($section['SubSection'] as $subSection){
							$subSectionBody = "";
//							$subSectionContent .= "<table id='StatTable' class='common_table_list_v30 tablesorter'>";
//							$subSectionContent .= "<tr><th colspan='3'>".$subSection['SubSectionName']."</th></tr>";
//							$subSectionContent .= "</table>";
							$subSectionName = ($subSection['SubSectionName']=="")?"":"<h4>".$subSection['SubSectionName']."</h4>";
							$subSectionContent .= $subSectionName;
							$subSectionID = $subSection['SubSectionID'];	
							if(!empty($subSection['QuestionData']))$hasQData=true;
							if(empty($subSection['QuestionData'])){
								$subSectionContent .= "";
							}else{
								$sql = "SELECT QCatID FROM INTRANET_PA_S_QCAT WHERE SecID='".$subSectionID."' AND QuesDispMode=1";
								$likertQCatList = $connection->returnVector($sql);
								foreach($likertQCatList as $currQCat){
									$hasProp = false;
									$hasPoor = false;
									$isShowingAllHeaders = false;
									$hideExtraBar = false;
									foreach($subSection['QuestionData'] as $question){			
										if($question['SecID']!=$subSectionID)continue;			
										if($question['QCatID']!=$currQCat)continue;
										$codeArr = explode(".",$question['Code']);
										$codeOrder = array();
										$codeAfter = array();
										$codePart = true;
										foreach($codeArr as $co){
											if(!is_numeric($co)){
												$codePart = false;
											}
											if($codePart == true){
												$codeOrder[] = $co;
											}else{
												$codeAfter[] = $co;
											}
										}
										$propertyStr = implode(".",$codeAfter);
										if($propertyStr != ""){
											$hasProp = true;
										}
										
										$descrPoorArr = explode("：",$question['DescrPoor']);
										if(count($descrPoorArr)==1){
											$descrPoor = $descrPoorArr[0];
										}else{
											unset($descrPoorArr[0]);
											$descrPoor = implode("：",$descrPoorArr);
										}
										$descrPoor = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $descrPoor));
										$descrPoor = trim(preg_replace('/\s\s+/', ' ', $descrPoor));
										
										if($descrPoor != ""){
											$hasPoor = true;
										}	
									}		
									$subSectionBody = "<table class='StatSubTable common_table_list_v30 tablesorter' style=\"page-break-inside:auto\">";
									$header1Colspan = ($hasProp==true)?3:2;
									$header1_sub = "";
									$header2 = "<thead>";
									$header2 .= "	<th>序</th>";
									if($hasProp==true){
										$header2 .= "	<th>特質</th>";
									}
									$header2_sub = "";
									$content = "";
									$headerFilled = false;
									foreach($subSection['QuestionData'] as $question){			
										if($question['SecID']!=$subSectionID)continue;		
										if($question['QCatID']!=$currQCat)continue;
										$codeArr = explode(".",$question['Code']);
										$codeOrder = array();
										$codeAfter = array();
										$codePart = true;
										foreach($codeArr as $co){
											if(!is_numeric($co)){
												$codePart = false;
											}
											if($codePart == true){
												$codeOrder[] = $co;
											}else{
												$codeAfter[] = $co;
											}
										}
										$descrExcellentArr = explode("：",$question['DescrExcellent']);
										if(count($descrExcellentArr)==1){
											$descrExcellent = $descrExcellentArr[0];
										}else{
											unset($descrExcellentArr[0]);
											$descrExcellent = implode("：",$descrExcellentArr);
										}
										$descrExcellent = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $descrExcellent));
										$descrPoorArr = explode("：",$question['DescrPoor']);
										if(count($descrPoorArr)==1){
											$descrPoor = $descrPoorArr[0];
										}else{
											unset($descrPoorArr[0]);
											$descrPoor = implode("：",$descrPoorArr);
										}
										$descrPoor = preg_replace('/[ \t]+/', ' ', preg_replace('/\s*$^\s*/m', "\n", $descrPoor));
										$descrPoor = trim(preg_replace('/\s\s+/', ' ', $descrPoor));
										$content .= "<tr style=\"page-break-inside:avoid; page-break-after:auto\">";
										$content .= "	<td>".implode(".",$codeOrder)."</td>";
										if($hasProp==true){
											$content .= "	<td style='white-space: nowrap'>".implode(".",$codeAfter)."</td>";
										}
										$content .= "	<td>".$descrExcellent."</td>";
										$idx=0;
										$sumOfReply = 0;
										if($question['DescrExcellent']===$descrExcellent && $descrPoor==""){
											$isShowingAllHeaders = true;
										}									
										$hasNotApplicable = false;										
										foreach($question['Scale'] as $mid=>$scaleStat){
											if($scaleStat['score']==='0' || $mid==""){
												$hasNotApplicable = true;
												break;
											}
										}
										$position = 2;
										if(!isset($question['Scale'][''])){
											$position = 1;
										}
										foreach($question['Scale'] as $mid=>$scaleStat){
											if($scaleStat['markName']=="")$scaleStat['markName'] = $Lang['Appraisal']['Report']['NotSubmitted'];
											if($scaleStat['markCode']=="")$scaleStat['markCode'] = $Lang['Appraisal']['Report']['NotSubmitted'];
											if($scaleStat['score']=="")$scaleStat['score'] = $Lang['Appraisal']['Report']['NotSubmitted'];
											if($isShowingAllHeaders && $headerFilled==false){
												if ($_SESSION['intranet_session_language'] == 'en') {
													$header1_sub .= "<th>".(($scaleStat['descrEng']=='')?$scaleStat['descrChi']:$scaleStat['descrEng'])."</th>";
												}else{	
													$header1_sub .= "<th>".(($scaleStat['descrChi']=='')?$scaleStat['descrEng']:$scaleStat['descrChi'])."</th>";
												}
											}else{
												if(($idx==0 || ($idx==count($question['Scale'])-$position) && $hasNotApplicable==false) && $headerFilled==false){		
													if($scaleStat['descrEng']=="" && $scaleStat['descrChi']!=""){
														$header1_sub .= "<th>".$scaleStat['descrChi']."</th>";
														if($idx==0){
															$header1_sub .= "<th></th>";
														}
													}else if($scaleStat['descrEng']!="" && $scaleStat['descrChi']==""){
														$header1_sub .= "<th>".$scaleStat['descrEng']."</th>";
														if($idx==0){
															$header1_sub .= "<th></th>";
														}
													}else{						
														$header1_sub .= "<th>".$scaleStat['descrEng']."</th>";
														$header1_sub .= "<th>".$scaleStat['descrChi']."</th>";
														$hideExtraBar = true;
													}
												}else if((($idx==count($question['Scale'])-($position+1)) && $hasNotApplicable==true) && $headerFilled==false){
													if($scaleStat['descrEng']=="" && $scaleStat['descrChi']!=""){
														$header1_sub .= "<th>".$scaleStat['descrChi']."</th>";
														$header1_sub .= "<th></th>";
													}else if($scaleStat['descrEng']!="" && $scaleStat['descrChi']==""){
														$header1_sub .= "<th>".$scaleStat['descrEng']."</th>";
														$header1_sub .= "<th></th>";
													}else{						
														$header1_sub .= "<th>".$scaleStat['descrEng']."</th>";
														$header1_sub .= "<th>".$scaleStat['descrChi']."</th>";
													}
												}else if(($idx!=1 && $idx!=count($question['Scale'])-1) && $headerFilled==false){
													$header1_sub .= "<th></th>";
												}
											}
											if($headerFilled==false){
												if($scaleStat['score']==='0'){
													if ($_SESSION['intranet_session_language'] == 'en') {
														if($scaleStat['descrEng']==""){
															$header2_sub .= "<th>".$scaleStat['descrChi']."</th>";
														}else{													
															$header2_sub .= "<th>".$scaleStat['descrEng']."</th>";
														}
													}else{
														if($scaleStat['descrChi']==""){
															$header2_sub .= "<th>".$scaleStat['descrEng']."</th>";
														}else{	
															$header2_sub .= "<th>".$scaleStat['descrChi']."</th>";
														}
													}
												}else{
													$header2_sub .= "<th>".$scaleStat['score']."</th>";
												}
											}
											//$content .= "	<td>".number_format(round(100*$scaleStat['vote']/$totalFormNo, 1),1)."%</td>";
											if($mid!=""){
											$sumOfReply += $scaleStat['vote'];
											}
											$idx++;
										}
										foreach($question['Scale'] as $mid=>$scaleStat){
											if($mid=="")continue;
											if($sumOfReply==0){
												$content .= "	<td>0.0%</td>";
											}else{
												$content .= "	<td>".number_format(round(100*$scaleStat['vote']/$sumOfReply, 1),1)."%</td>";
											}
										}
										if($hasPoor == true){
											$content .= "	<td>".$descrPoor."</td>";
										}
										$content .= "</tr>";
										$headerFilled = true;
									}									
									$header1 = "<thead>";									
									if($isShowingAllHeaders==false){
										$header1 .= "	<th colspan='".$header1Colspan."' style=\"width:40%\"></th>";
									}else{
										$header1 .= "	<th colspan='".$header1Colspan."'></th>";	
									}
									$header1 .= $header1_sub;
									if(($hasPoor && !$hideExtraBar) || ($hasNotApplicable==true && $hasPoor==true)){					
										$header1 .= "	<th style=\"width:20%\"></th>";
									}
									$header1 .= "</thead>";
									if($isShowingAllHeaders==false){
										$header2 .= "	<th>優異表現指標</th>";
									}else{
										$header2 .= "<th></th>";
									}
									$header2 .= $header2_sub;
									if($hasPoor == true){					
										$header2 .= "	<th>欠佳表現指標</th>";
									}
									$header2 .= "</thead>";
									if($content == ""){
										$subSectionHasContent = false;
									}else{	
										//$header = $header1.$header2;
										//$subSectionContent .= $header;
									}
									$header = $header1.$header2;
									$subSectionBody .= $header;
									$subSectionBody .= $content;
									$subSectionBody .= "</table>";
									if($content==""){
										$subSectionBody = "";
									}
									$subSectionContent .= $subSectionBody.'<br>';
								}
							}
							
							$commentContent = "";							
							if(isset($subSection['QuestionComment'])){
								foreach($subSection['QuestionComment'] as $qcomm){
									if($qcomm['name']==""){
										$qcomm['name'] = $Lang['Appraisal']['ARFormIdvArrRmk'];
									}
									if(count($qcomm["data"]) > 0){
										$commentContentBody = "";
										foreach($qcomm["data"] as $uid=>$reply){		
											if($reply != ""){									
												$commentContentBody .= "<table class='common_table_list_v30' style=\"border:none;\">";
												$commentContentBody .= "<tbody>";
												$commentContentBody .= "<tr>";
												//$commentContent .= "<td style=\"border:none; width:10%;\">".$indexVar['libappraisal']->getUserNameID($uid,"name").":</td>";
												$commentContentBody .= "<td style=\"border:none; width:90%;\">".$indexVar['libappraisal_ui']->GET_TEXTAREA($uid."_".$subSection['SubSectionID'], $taContents=$reply, $taCols=70, $taRows=5, $OnFocus="", $readonly=true, $other='style="width:100%;"', $class='', $taID='', $CommentMaxLength='')."</td>";
												$commentContentBody .= "</tr>";											
												$commentContentBody .= "</tbody></table><br/>";
											}
										}
										if($commentContentBody != ""){										
											$commentContent .= "<h4>".$qcomm['name']."</h4>";
											$commentContent .= $commentContentBody;
										}
									}
								}
								if($commentContent == ""){
									$subSectionHasComment = false;
								}
								$subSectionComment .= $commentContent;
							}
							if($subSectionBody=="" && $commentContent==""){
								$subSectionContent .= $Lang['Appraisal']['Message']['NoRecord'];
							}
							if($subSectionComment!=""){
								$subSectionContent .= $subSectionComment;
								unset($subSectionComment);
							}
							$subSectionContent .= "</table>";
							$subSectionContent .= "<br/>";
						}
					}else{
						$subSectionHasContent = false;
						$subSectionHasComment = false;
					}
					$output .= $subSectionContent;
					unset($subSectionContent);
				}
			}else{
				$output .= $Lang['Appraisal']['Message']['NoRecord'];
			}
			echo "<style>";
			echo ".simpleSignature td { font-size: 16px; height: 40px; }";
			echo "  table.simpleSignature, table.simpleSignature tr, table.simpleSignature tr td { border-style: none; }";
			echo "  table { page-break-inside:auto }";
			echo "  tr    { page-break-inside:avoid; page-break-after:auto }";
			echo "  thead { display:table-header-group }";
			echo "</style>";				
			echo $output;
			echo "<br/>";
			echo "<table id=\"signature\" class=\"common_table_list simpleSignature\">";
			$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("CONCAT(ChineseName,TitleChinese)", "CONCAT(TitleEnglish,'',EnglishName)")." as Name FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$appraiseeID."'";
			$appraiseeName = current($connection->returnVector($sql));
			if($appraiseeName==""){
				$appraiseeName = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			echo "	<tr>";
			echo "		<td style='text-align: right'>".$Lang['Appraisal']['ARFormAppraiseeSignature'].":</td><td>________________(".$appraiseeName.")</td>";
			echo "		<td style='text-align: right'>".$Lang['Appraisal']['ARFormDate'].":</td><td>_____________________</td>";
			echo "	</tr>";
// 			echo "	<tr>";
// 			echo "		<td style='text-align: right'>".$Lang['Appraisal']['ARFormAppraiserSignature'].":</td><td>________________(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)</td>";
// 			echo "		<td style='text-align: right'>".$Lang['Appraisal']['ARFormDate'].":</td><td>_____________________</td>";
// 			echo "	</tr>";
			$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("CONCAT(ChineseName,TitleChinese)", "CONCAT(TitleEnglish,'',EnglishName)")." as Name FROM ".$appraisalConfig['INTRANET_USER']." WHERE Teaching=1 AND Title=4;";
			$principalName = current($connection->returnVector($sql));
			if($principalName==""){
				$principalName = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			}
			echo "	<tr>";
			echo "		<td style='text-align: right'>".$Lang['Appraisal']['ARFormPrincipalSignature'].":</td><td>________________(".$principalName.")</td>";
			echo "		<td style='text-align: right'>".$Lang['Appraisal']['ARFormDate'].":</td><td>_____________________</td>";
			echo "	</tr>";
//			echo "	<tr>";
//			echo "		<td>Signature:</td>";
//			echo "		<td>________________</td>";
//			echo "		<td>(               )</td>";
//			echo "		<td></td>";
//			echo "		<td>Date:</td>";
//			echo "		<td>_________________</td>";
//			echo "	</tr>";
			echo "</table>";		
		}
	}
}



function getSdfStat($connection, $templateID, $userID, $recordID, $cycleID, $rlsNo, $batchID) {
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $teacherPerformance, $questionData, $appraisalConfig;

	if ($userID == "" && $recordID == "" && $cycleID == "" && $rlsNo == "" && $batchID == "") {
		$sql = "SELECT TemplateID," . $indexVar['libappraisal']->getLangSQL("FrmTitleChi", "FrmTitleEng") . " as FormTitle," .
		$indexVar['libappraisal']->getLangSQL("FrmCodChi", "FrmCodEng") . " as FormCode," .
		$indexVar['libappraisal']->getLangSQL("HdrRefChi", "HdrRefEng") . " as HdrRef," .
		$indexVar['libappraisal']->getLangSQL("ObjChi", "ObjEng") . " as Obj," .
		$indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,
					AppTgtChi,AppTgtEng,NeedSubj,NeedClass
					FROM INTRANET_PA_S_FRMTPL WHERE TemplateID=" . IntegerSafe($templateID) . ";";
	} else {
		$sql = "SELECT iptf.RecordID,UserID,iptf.CycleID,CycleDescr,iptf.TemplateID,FormTitle,FormCode,HdrRef,Obj,Descr,AppRoleID,AppTgtChi,AppTgtEng,AcademicYearID,COALESCE(SubDate,'') as SubDate,
				EditPrdFr,EditPrdTo,CASE WHEN NOW()>EditPrdTo THEN '1' ELSE '0' END as OverdueDate,YearClassID,SubjectID,TeachLang,NeedSubj,NeedClass
				FROM(
					SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE RecordID=" . IntegerSafe($recordID) . "
				) as iptf
				INNER JOIN(
					SELECT TemplateID," . $indexVar['libappraisal']->getLangSQL("FrmTitleChi", "FrmTitleEng") . " as FormTitle," .
		$indexVar['libappraisal']->getLangSQL("FrmCodChi", "FrmCodEng") . " as FormCode," .
		$indexVar['libappraisal']->getLangSQL("HdrRefChi", "HdrRefEng") . " as HdrRef," .
		$indexVar['libappraisal']->getLangSQL("ObjChi", "ObjEng") . " as Obj," .
		$indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,
					AppTgtChi,AppTgtEng,NeedSubj,NeedClass
					FROM INTRANET_PA_S_FRMTPL
				) as ipsf ON iptf.TemplateID=ipsf.TemplateID
				LEFT JOIN(
					SELECT CycleID," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as CycleDescr,AcademicYearID
					FROM INTRANET_PA_C_CYCLE
				) as ipcc ON iptf.CycleID=ipcc.CycleID
				LEFT JOIN(
					SELECT BatchID,CycleID,EditPrdFr,EditPrdTo,AppRoleID FROM INTRANET_PA_C_BATCH WHERE BatchID=" . IntegerSafe($batchID) . "
				) as ipcb ON ipcc.CycleID=ipcb.CycleID
				LEFT JOIN(
					SELECT RlsNo,RecordID,BatchID,SubDate FROM INTRANET_PA_T_FRMSUB
				) as iptfrmSub ON iptf.RecordID=iptfrmSub.RecordID AND ipcb.BatchID=iptfrmSub.BatchID
				LEFT JOIN(
					SELECT RlsNo,RecordID,BatchID,YearClassID,SubjectID,TeachLang FROM INTRANET_PA_T_SLF_HDR WHERE RecordID=" . IntegerSafe($recordID) . " AND BatchID=" . IntegerSafe($batchID) . "
				) as iptsh ON iptf.RecordID=iptsh.RecordID AND iptfrmSub.RlsNo=iptsh.RlsNo AND iptfrmSub.BatchID=iptsh.BatchID;";
		//echo $sql."<br/>";
	}

	$header = $connection->returnResultSet($sql);
	$templateID = $header[0]["TemplateID"];
	$academicYearID = $header[0]["AcademicYearID"];
	$formName = $header[0]["FormTitle"];

	$tchNameSql = "SELECT EnglishName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='" . $userID . "'";
	$tchName = current($connection->returnVector($tchNameSql));
	$isSetBefore = false;
	if(!isset($teacherPerformance[$userID])){
		$teacherPerformance[$userID] = array();
	}
	if (isset ($teacherPerformance[$userID][$templateID])) {
		$isSetBefore = true;
	} else {
		$teacherPerformance[$userID][$templateID] = array (
			"Name" => $tchName,
			"FormName" => $formName,
			"ID"=>$rlsNo."-".$recordID."-".$batchID,
			"Section" => array ()
		);
	}
	//======================================================================== Section ========================================================================//
	$sql = "SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,PageBreakAfter
		FROM(
			SELECT SecID,TemplateID," . $indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng") . " as SecTitle," . $indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng") . " as SecCod," .
	$indexVar['libappraisal']->getLangSQL("SecDescrChi", "SecDescrEng") . " as SecDescr,DisplayOrder,PageBreakAfter FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=" . IntegerSafe($templateID) . " AND ParentSecID IS NULL
		) as ipsf ORDER BY DisplayOrder;
		";
	//echo $sql."<br/><br/>";
	$sectionHeader = $connection->returnResultSet($sql);

	// prepare the QID for that form
	$parSecID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($sectionHeader, "SecID");
	$sql = "SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
		FROM(
			SELECT SecID,TemplateID," . $indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng") . " as SecTitle," . $indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng") . " as SecCod," .
	$indexVar['libappraisal']->getLangSQL("SecDescrChi", "SecDescrEng") . " as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=" . IntegerSafe($templateID) . " AND ParentSecID IS NOT NULL
			AND ParentSecID IN (" . $parSecID . ")
		) as ipsf";
	//echo $sql."<br/><br/>";
	$y = $connection->returnResultSet($sql);
	$subSecID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y, "SecID");
	$sql = "SELECT QCatID,SecID," . $indexVar['libappraisal']->getLangSQL("QCatCodChi", "QCatCodEng") . " as QCatCod," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as QCatDescr," .
	"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID IN (" . $subSecID . ")";
	//echo $sql."<br/><br/>";
	$y = $connection->returnResultSet($sql);
	$qCatID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y, "QCatID");
	$sql = "SELECT QID,QCatID FROM INTRANET_PA_S_QUES WHERE QCatID IN (" . $qCatID . ")";
	$y = $connection->returnResultSet($sql);
	$qID = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($y, "QID");
	// prepare the QID for that form

	for ($h = 0; $h < sizeof($sectionHeader); $h++) {
		$sectionHeaderTitle = "";
		if ($sectionHeader[$h]["SecCod"] != "" && $sectionHeader[$h]["SecTitle"] != "") {
			$sectionHeaderTitle = $sectionHeader[$h]["SecCod"] . "-" . $sectionHeader[$h]["SecTitle"];
		} else
			if ($sectionHeader[$h]["SecCod"] != "" && $sectionHeader[$h]["SecTitle"] == "") {
				$sectionHeaderTitle = $sectionHeader[$h]["SecCod"];
			} else
				if ($sectionHeader[$h]["SecCod"] == "" && $sectionHeader[$h]["SecTitle"] != "") {
					$sectionHeaderTitle = $sectionHeader[$h]["SecTitle"];
				}	
		if (!isset ($teacherPerformance[$userID][$templateID]['Section']['Section' . ($h +1)])) {
			$teacherPerformance[$userID][$templateID]['Section']['Section' . ($h +1)] = array (
				"SectionName" => $sectionHeaderTitle,
				"SubSection" => array ()
			);
		}
		//======================================================================== Sub Section ========================================================================//
		$sql = "SELECT SecID,TemplateID,SecTitle,SecCod,SecDescr,DisplayOrder,ParentSecID
							FROM(
								SELECT SecID,TemplateID," . $indexVar['libappraisal']->getLangSQL("SecTitleChi", "SecTitleEng") . " as SecTitle," . $indexVar['libappraisal']->getLangSQL("SecCodChi", "SecCodEng") . " as SecCod," .
		$indexVar['libappraisal']->getLangSQL("SecDescrChi", "SecDescrEng") . " as SecDescr,DisplayOrder,ParentSecID FROM INTRANET_PA_S_FRMSEC WHERE TemplateID=" . IntegerSafe($templateID) . " AND ParentSecID IS NOT NULL
								AND ParentSecID=" . IntegerSafe($sectionHeader[$h]["SecID"]) . "
							) as ipsf
							ORDER BY DisplayOrder;
							";
		//echo $sql."<br/><br/>";
		$a = $connection->returnResultSet($sql);
		for ($i = 0; $i < sizeof($a); $i++) {
			$subSectionName = "";
			if ($a[$i]["SecCod"] != "" && $a[$i]["SecTitle"] != "") {
				$subSectionName = $a[$i]["SecCod"] . " " . $a[$i]["SecTitle"];
			} else
				if ($a[$i]["SecCod"] != "" && $a[$i]["SecTitle"] == "") {
					$subSectionName = $a[$i]["SecCod"];
				} else
					if ($a[$i]["SecCod"] == "" && $a[$i]["SecTitle"] != "") {
						$subSectionName = $a[$i]["SecTitle"];
					}

			if (!isset ($teacherPerformance[$userID][$templateID]['Section']['Section' . ($h +1)]['SubSection']['SubSection' . ($i +1)])) {
				$teacherPerformance[$userID][$templateID]['Section']['Section' . ($h +1)]['SubSection']['SubSection' . ($i +1)] = array (
					"SubSectionName" => $subSectionName,
					"SubSectionID"=>$a[$i]["SecID"],
					"QuestionData" => array ()
				);
			}

			$sql = "SELECT QCatID,SecID," . $indexVar['libappraisal']->getLangSQL("QCatCodChi", "QCatCodEng") . " as QCatCod," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as QCatDescr,QuesDispMode," .
			"DisplayOrder as QCatDisplayOrder FROM INTRANET_PA_S_QCAT WHERE SecID=" . $a[$i]["SecID"] . " ORDER BY DisplayOrder;";
			//echo $sql."<br/><br/>";
			$b = $connection->returnResultSet($sql);
			for ($j = 0; $j < sizeof($b); $j++) {
				if ($b[$j]['QuesDispMode'] == 1 && empty($teacherPerformance[$userID][$templateID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionData"][$j])) {
					//======================================================================== Question Category ========================================================================//
					getSDFSectionContent("", $a[$i]["SecID"], $b[$j]["QCatID"], $recordID, $rlsNo, $batchID, $connection);
				}
				if($b[$j]['QuesDispMode'] == 0){
					$returnQueData = getSDFCommentList("", $a[$i]["SecID"], $b[$j]["QCatID"], $recordID, $rlsNo, $batchID, $connection);
					if ($returnQueData) {
						if(!isset($teacherPerformance[$userID][$templateID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionComment"])){
							$teacherPerformance[$userID][$templateID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionComment"] = array();
						}
						foreach($returnQueData as $queID=>$queContent){
							$teacherPerformance[$userID][$templateID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionComment"][$queID] = $queContent;
						}
					}
				}				
			}
			$teacherPerformance[$userID][$templateID]['Section']['Section'.($h+1)]['SubSection']['SubSection'.($i+1)]["QuestionData"] = $questionData;
		}
	}
}

function getSDFSectionContent($prefix, $secID, $qCatID, $recordID, $rlsNo, $batchID, $connection) {
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $sys_custom,$questionData,$markerList;
	
	$sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,ScoreIsAvgMark,ScoreAvgDec
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec FROM INTRANET_PA_S_QCAT WHERE SecID=" . $secID . " AND QCatID=" . $qCatID . "
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID," . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . " as QCodDes," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,DisplayOrder,InputType,
								IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as Rmk,IsAvgMark as ScoreIsAvgMark,AvgDec as ScoreAvgDec,
						DisplayOrder as QuesDisplayOrder," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as RmkLabel
						FROM INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . "
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
	$a = $connection->returnResultSet($sql);
	//echo ($sql)."<br/><br/>";
	$quesDispMode = $a[0]["QuesDispMode"];
	
	// check any content in the first content
	$hasContent = false;
	$sql = "SELECT SUM(LENGTH(" . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . ")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . ";";
	$x = $connection->returnResultSet($sql);
	if ($x[0]["content"] > 0) {
		$hasContent = true;
	}
	if ($quesDispMode == 1) {
		for ($i = 0; $i < sizeof($a); $i++) {
			if(!isset($questionData[$a[$i]["QID"]])){
				$sql = "SELECT " . $indexVar['libappraisal']->getLangSQL("CONCAT(MarkCodChi,' ',DescrChi)", "CONCAT(MarkCodEng,' ',DescrEng)") . " as MarkName, MarkID, Score, " . $indexVar['libappraisal']->getLangSQL("MarkCodChi", "MarkCodEng") . " as MarkCode, DescrChi, DescrEng FROM INTRANET_PA_S_QLKS WHERE QCatID=" . $qCatID . " ORDER BY DisplayOrder ASC";
				$lksScale = $connection->returnResultSet($sql);
				$descrArr = explode("\n",$a[$i]['Descr']);
				$descrExcellent = array();
				$descrPoor = array();
				if(strstr($a[$i]['Descr'],'優異') || strstr($a[$i]['Descr'],'欠佳')){					
					$isPoor = false;
					foreach($descrArr as $descWord){
						if(trim($descWord)==""){
							continue;
						}
						if(strstr($descWord,'欠佳')){
							$isPoor = true;
						}
						if($isPoor==true){
							$descrPoor[] = $descWord;
						}else{
							$descrExcellent[] = $descWord;
						}
					}
				}else{
					if(substr($a[$i]['Descr'], -3)=="："){
						$descrExcellent[] = substr($a[$i]['Descr'], 0, -3);
					}else{
						$descrExcellent[] = $a[$i]['Descr'];
					}
				}
				$questionData[$a[$i]["QID"]] = array(
					"Code"=>$a[$i]['QCodDes'],
					"DescrExcellent"=>implode("\n",$descrExcellent),
					"DescrPoor"=>implode("\n",$descrPoor),
					"Question"=>$a[$i]['QCodDes']."-".$a[$i]['Descr'],
					"SecID"=>$secID,
					"QCatID"=>$qCatID,
					"Scale"=>array()
				);
				foreach($lksScale as $lkss){
					$questionData[$a[$i]["QID"]]["Scale"][$lkss['MarkID']] = array("markName"=>$lkss['MarkName'],"vote"=>0,"score"=>$lkss['Score'],"markCode"=>$lkss['MarkCode'],"descrChi"=>$lkss['DescrChi'],"descrEng"=>$lkss['DescrEng'],"respondent"=>0);
				}
			}
			
			$sql = "SELECT ipstdtl.RecordID,ipstdtl.BatchID,QID,QAnsID,ipstdtl.MarkID,Remark,DateAns,ipsqlks.Score,ipsqlks.MarkCodChi,ipsqlks.MarkCodEng,frmsub.FillerID
								FROM(
									SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL 
									WHERE RlsNo=" . $rlsNo . " AND BatchID=" . $batchID . " AND QID=" . $a[$i]["QID"] . " AND RecordID=". $recordID . "
								) as ipstdtl
								INNER JOIN(
									SELECT MarkID,Score,MarkCodChi,MarkCodEng FROM INTRANET_PA_S_QLKS 
								) as ipsqlks ON ipstdtl.MarkID=ipsqlks.MarkID
								INNER JOIN (
									SELECT RlsNo,RecordID,BatchID,FillerID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND BatchID=" . $batchID . " AND RecordID=". $recordID . "
								) frmsub ON ipstdtl.RecordID=frmsub.RecordID";
			//echo $sql."<br/><br/>";;
			$quesResult = $connection->returnResultSet($sql);
			$ques = current($quesResult);
			$questionData[$a[$i]["QID"]]["Scale"][$ques['MarkID']]["vote"]++;
			$questionData[$a[$i]["QID"]]["Scale"][$ques['MarkID']]["respondent"]++;
			foreach($quesResult as $q){
				if(!in_array($q['FillerID'],$markerList)){
					array_push($markerList,$q['FillerID']);
				}
			}
		}
	}
}

function getSDFCommentList($prefix, $secID, $qCatID, $recordID, $rlsNo, $batchID, $connection){
	$indexVar['libappraisal'] = new libappraisal();
	$indexVar['libappraisal_ui'] = new libappraisal_ui();
	global $intranet_session_language, $Lang, $sys_custom,$teacherQuestionList,$markerList;
	$questionScore = array();
	$teacherCommentList = array();
	$sql = "SELECT SecID,ipsqcat.QCatID,QID,QuesDispMode,QCodDes,Descr,QuesDisplayOrder,IsMC,IsScore,HasRmk,Rmk,IsTxtAns,TxtBoxNote,InputType,RmkLabel,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec,ScoreIsAvgMark,ScoreAvgDec
					FROM(
						SELECT QCatID,SecID,QCatCodChi,QCatCodEng,DescrChi,DescrEng,DisplayOrder as CatDisplayOrder,QuesDispMode,IsAvgMark,AvgDec,IsVerAvgMark,VerAvgDec FROM INTRANET_PA_S_QCAT WHERE SecID=" . $secID . " AND QCatID=" . $qCatID . "
					) as ipsqcat
					LEFT JOIN(
						SELECT QID,QCatID," . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . " as QCodDes," . $indexVar['libappraisal']->getLangSQL("DescrChi", "DescrEng") . " as Descr,DisplayOrder,InputType,
								IsMC,IsScore,IsTxtAns,TxtBoxNote,HasRmk," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as Rmk,IsAvgMark as ScoreIsAvgMark,AvgDec as ScoreAvgDec,
						DisplayOrder as QuesDisplayOrder," . $indexVar['libappraisal']->getLangSQL("RmkLabelChi", "RmkLabelEng") . " as RmkLabel
						FROM INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . "
					) as ipsques ON ipsqcat.QCatID=ipsques.QCatID
					ORDER BY QuesDisplayOrder;";
	$a = $connection->returnResultSet($sql);
	//echo ($sql)."<br/><br/>";
	$quesDispMode = $a[0]["QuesDispMode"];

	// get Double Count Filler
	$doubleCountFillerList = $indexVar['libappraisal']->getReportDoubleCountPersonList(false);
	
	// check any content in the first content
	$hasContent = false;
	$sql = "SELECT SUM(LENGTH(" . $indexVar['libappraisal']->getLangSQL("QCodChi", "QCodEng") . ")) as content FROM  INTRANET_PA_S_QUES WHERE QCatID=" . $qCatID . ";";
	$x = $connection->returnResultSet($sql);
	if ($x[0]["content"] > 0) {
		$hasContent = true;
	}
	for ($i = 0; $i < sizeof($a); $i++) {
		if($a[$i]["InputType"]==5 || $a[$i]["InputType"]==0){
			$intSecID = $a[$i]["SecID"];
			$intQusCodID = $i +1;
			$qID = $a[$i]["QID"];

			$sql = "SELECT UserID,TemplateID  FROM(
								SELECT RlsNo,RecordID,BatchID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND BatchID=" . $batchID . " AND RecordID=" . $recordID . "
							) as iptfsub
							INNER JOIN(
								SELECT RecordID,UserID,TemplateID FROM INTRANET_PA_T_FRMSUM 
							) as iptf ON iptfsub.RecordID=iptf.RecordID;";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$userID = $x[0]["UserID"];
			$templateID = $x[0]["TemplateID"];
			$sql = "SELECT RecordID FROM INTRANET_PA_T_FRMSUM WHERE UserID=" . $userID . " AND TemplateID=" . $templateID . ";";
			//echo $sql."<br/><br/>";
			$x = $connection->returnResultSet($sql);
			$recordIDArr = $indexVar['libappraisal']->convertMultipleRowsIntoOneRow($x, "RecordID");
			//echo $recordIDArr."<br/><br/>";				
			$sql = "SELECT ipstdtl.RecordID,MAX(ipstdtl.BatchID),QID,QAnsID,ipstdtl.MarkID,MAX(Remark) as remark_ans,DateAns,frmsub.FillerID
								FROM(
									SELECT RecordID,BatchID,QID,QAnsID,MarkID,Remark,Score,DATE_FORMAT(DateAns,'%Y-%m-%d') as DateAns FROM INTRANET_PA_T_SLF_DTL 
									WHERE RlsNo=" . $rlsNo . " AND QID=" . $qID . " AND RecordID IN (" . $recordIDArr . ")
								) as ipstdtl
								INNER JOIN (
									SELECT RlsNo,RecordID,BatchID,FillerID FROM INTRANET_PA_T_FRMSUB WHERE RlsNo=" . $rlsNo . " AND RecordID IN (" . $recordIDArr . ")
								) frmsub ON ipstdtl.RecordID=frmsub.RecordID GROUP BY ipstdtl.RecordID";
			//echo $sql."<br/><br/>";;
			$b = $connection->returnResultSet($sql);
			if(!isset($teacherCommentList[$qID])){
				$teacherCommentList[$qID] = array("name"=>$a[$i]['Descr'],"data"=>array());
			}
			for ($j = 0; $j < sizeof($b); $j++) {
				$teacherCommentList[$qID]["data"][$b[$j]['FillerID']] = $b[$j]['remark_ans'];
			}	
		}
	}
	return $teacherCommentList;
}
?>