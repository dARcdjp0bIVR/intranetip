<?
//editing by 
####################################### Change Log #################################################
# 2012-12-28 by Carlos: Added continuous absent day option
####################################################################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");

intranet_opendb();
$g_encoding_unicode = true;

include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();
$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();

?>
<meta http-equiv='content-type' content='text/html; charset=utf8'>
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script>
$(document).ready(function(){
	$('textarea.tabletext').each(function(){
		$(this).height(this.scrollHeight);
	});
});
</script>
<style type="text/css">
@media print{
	#StatTable {
		border:1px solid grey;
		page-break-inside:auto;
	}
	#StatTable tr{
		page-break-inside:avoid;
		page-break-after:auto;
	}
	#StatTable th{
		color:grey;
		border-bottom: 2px solid #AAAAAA;
		border-top: 1px solid #CCCCCC;
	}
	table.StatSubTable {
		page-break-inside:avoid;
	}	
	table.StatSubTable tr{
		page-break-inside:avoid;
		page-break-after:auto;
	}
	table.StatSubTable tr th{
		color: #000000;
	}
	table#signature {
		page-break-after: always;
	}
}
#nameLine{
	/*display: none;*/
}
</style>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='left'><?=$indexVar['libappraisal_ui']->GET_BTN((($g_encoding_unicode) ?  convert2unicode($button_print, 1) :  $button_print), "button", "javascript:window.print();","submit2")?></td>
		<td align='left'><input type="hidden" id="CycleID" value="<?=$cycleID?>"></td>
		<td align='left'><input type="hidden" id="TemplateID" value="<?=$templateID?>"></td>
		<td align='left'><input type="hidden" id="UserID" value="<?=$userID?>"></td>
	</tr>
</table>
<div id="PrintArea">
</div>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
<script>
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	document.title = "<?=$Lang['Appraisal']['ReportPersonalAverageList']?>";
	goRptGen();	
});

function goRptGen(){
	var cycleID = $('#CycleID').val();
	var templateID = $('#TemplateID').val();
	$('#PrintArea').html(ajaxImage);
	if(cycleID != '' && templateID != ''){
		$.post(
			'/home/eAdmin/StaffMgmt/appraisal/index.php?task=reports<?=$appraisalConfig['taskSeparator']?>personal_average<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetFormDetailsList',
			{
				'cycleID':$('#CycleID').val(),
				'templateID':$('#TemplateID').val(),
				'appraiseeID':$('#UserID').val()
			},
			function(data){
				$('#PrintArea').html(data);
				$('textarea.tabletext').each(function(){
					$(this).height(this.scrollHeight+10);
				});
				$('#submit2').click();
			}
		);
	}else{
		alert('error!');
	}
}
</script>