<?
//editing by 
####################################### Change Log #################################################
# 2012-12-28 by Carlos: Added continuous absent day option
####################################################################################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");

intranet_opendb();

include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();
$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();

?>
<style type="text/css">
#AverageTable {
	border:1px solid black;
}
#AverageTable th{
	border:1px solid black;
	color:black;
}
#AverageTable td{
	border:1px solid black;
}
</style>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='left'><?=$indexVar['libappraisal_ui']->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
		<td align='left'><input type="hidden" id="CycleID" value="<?=$cycleID?>"></td>
		<td align='left'><input type="hidden" id="TemplateID" value="<?=$templateID?>"></td>
		<td align='left'><input type="hidden" id="UserID" value="<?=$userID?>"></td>
		<td align='left'><input type="hidden" id="LoginID" value="<?=$loginID?>"></td>
		<td align='left'><input type="hidden" id="mode" value="<?=$mode?>"></td>
	</tr>
</table>
<div id="PrintArea">
</div>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
<script>
var reportContent = "";
var ajaxImage = '<?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?>';
$(document).ready( function() {
	goRptGen();	
});

function goRptGen(){
	var cycleID = $('#CycleID').val();
	var templateID = $('#TemplateID').val();
	var mode = $('#mode').val();
	$('#PrintArea').html(ajaxImage);
	if(mode==0){
		$('#PrintArea').html(window.opener.$('#PrintArea').html());
		$('#submit2').click();
	}else if(mode==1){
		if(cycleID != '' && templateID != ''){
			if(typeof templateID == 'undefined' || templateID == 'undefined'){
				$.post(
					'/home/eAdmin/StaffMgmt/appraisal/index.php?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetUserDetailsList',
					{
						'cycleID':$('#CycleID').val(),
						'loginID':$('#LoginID').val(),
						'userID':$('#UserID').val()
					},
					function(data){
						$('#PrintArea').html(data);
						$('#submit2').click();
					}
				);
			}else{
				$.post(
					'/home/eAdmin/StaffMgmt/appraisal/index.php?task=reports<?=$appraisalConfig['taskSeparator']?>school_result<?=$appraisalConfig['taskSeparator']?>ajax_getData&Type=GetFormDetailsList',
					{
						'cycleID':$('#CycleID').val(),
						'templateID':$('#TemplateID').val(),
						'loginID':$('#LoginID').val()
					},
					function(data){
						$('#PrintArea').html(data);
						$('#submit2').click();
					}
				);
			}
		}else{
			alert('error!');
		}
	}
}
</script>