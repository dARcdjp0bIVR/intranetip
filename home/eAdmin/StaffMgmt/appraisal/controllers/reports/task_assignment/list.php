<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$linterface = new interface_html();
$connection = new libgeneralsettings();

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ReportTaskAssignmentChecklist']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
/*
$sql="SELECT CycleID,CONCAT(DescrChi,' (', YearNameB5,')') as DescrChi,CONCAT(DescrEng,' (', YearNameEN,')') as DescrEng
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
	$cycleList=$connection->returnResultSet($sql);
*/
$sql="SELECT CycleID, DescrChi, YearNameB5, DescrEng, YearNameEN
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
$tmp_cycleList=$connection->returnResultSet($sql);
$cycleList = array();
if (count($tmp_cycleList) > 0) {
	foreach ($tmp_cycleList as $kk => $vv) {
		$cycleList[$kk] = array();
		$cycleList[$kk]["CycleID"] = $vv["CycleID"];
		if ($indexVar['libappraisal']->isEJ()) {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $indexVar['libappraisal']->displayChinese($vv["YearNameB5"]) . ")";
		} else {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $vv["YearNameB5"] . ")";
		}
		$cycleList[$kk]["DescrEng"] = $vv["DescrEng"] . " (" . $vv["YearNameEN"] . ")";
	}
}



$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .="<tr>"."\r\n";
$x .= "<td width=\"30%\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['AppraisalPeriod']."</td><td>:</td>"."\r\n";
$x .= "<td width=\"70%\">".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="CycleID" name="CycleID" onChange="javascript:getStaffSelection()"', $SelectedType=$a[$i]["CycleID"], $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('cycleIDEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
$x .= "<td width=\"30%\">".$Lang['Appraisal']['Report']['ModeSelection']."</td><td>:</td>"."\r\n";
$x .= "<td width=\"70%\">";
$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("selectAll", "mode", "0", true, "", $Lang['Appraisal']['Report']['ModeShowAll'], "activateUserSelection(0)");
$x .= $indexVar['libappraisal_ui']->Get_Radio_Button("selectOne", "mode", "1", false, "", $Lang['Appraisal']['Report']['ModeShowOne'], "activateUserSelection(1)");
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .= "<tr class='stdSelection' style='display:none;'>"."\r\n";
$x .= "<td>"."\r\n";
$x .= "<span>".$Lang['Appraisal']['ReportPersonResult']['SelectAppraisee']."</span><br/>";
$x .= "</td>"."\r\n";
$x .= "<td>:</td>";
//$x .= "<td>";
//$x .= $Lang['Appraisal']['SearchByLoginID']." "."<input type=\"text\" id=\"LoginID\" name=\"LoginID\" value=\"\">";
//$x .= "</td>";
$x .= "<td>";
$x .= "<span id=\"staff_selection\">";
$x .= $indexVar['libappraisal']->Get_Staff_Selection('LoginID','LoginID',$__ParIndividual=true,$__ParIsMultiple=false,$__ParSize=20,$__ParSelectedValue=$StaffID,$__ParOthers="",$__ParAllStaffOption=false,$__ShowOptGroupLabel=true,$LoginIDAsValue=true);
$x .= "</span>";
$x .= "</td>";
$x .= "</tr>";
//$x .= "<tr class='stdSelection' style='display:none;'>";
//$x .= "<td>";
//$x .= $indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['ReportPersonResult']['SelectedAppraisee']."<br/>";
//$x .= "<td>:</td>";
//$x .= "<td>";
//$x .= "<span id=\"UserName\" name=\"UserName\"></span>";
//$x .= "<input type=\"hidden\" name=\"UserID\" id=\"UserID\" value/>";
//$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('userNameEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
//$x .= "</td>";
//$x .= "</tr>"."\r\n";

$x .= "</table>";
		

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptGen'], "submit", "goRptGen()", 'genRptBtn');
$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// ============================== Define Button ==============================








?>