<?php
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$type=($_GET["Type"]=="")?$_POST["Type"]:$_GET["Type"];
$q=$_GET["?q"];
$cycleID=$_POST["cycle"];
$userLogin=$_POST["login"];
$reportMode=$_POST["mode"];
// ============================== get Login User ==============================
if($type=="GetLoginUser"){
    $typeList = "1";		# 1-teacher only
    $name_field = getNameFieldByLang("USR.");
    //	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
    //		FROM(
    //			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin
    //			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $conds
    //			GROUP BY USR.UserID ORDER BY UserLogin
    //		) as a
    //		WHERE UserLogin like '%".$q."%'";
    
    $name_field_deleted = getNameFieldByLang("AUR.");
    $sql = "SELECT UserID,Name,ClassNumber,UserLogin
		FROM(
			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin
			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)
			GROUP BY USR.UserID
		) as a
		WHERE UserLogin like '%".$q."%' ORDER BY UserLogin;
	";
    //echo $sql."<br/><br/>";
    $a=$connection->returnResultSet($sql);
    for ($i=0; $i<sizeof($a); $i++){
        $x .= $a[$i]["UserLogin"]."|".$a[$i]["UserID"]."|".$a[$i]["UserLogin"]."\n";
    }
    echo $x;
}elseif($type=="GetReport"){
    $userInTemplateArr = array();
    // Get all the forms used at that cycle first
    $sql = "SELECT ipcf.TemplateID,".$indexVar['libappraisal']->getLangSQL("CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi)","CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng)")." as FrmInfo,'STD' as TemplateUsage
			FROM INTRANET_PA_C_FRMSEL ipcf
			INNER JOIN INTRANET_PA_S_FRMTPL ipsf ON ipcf.TemplateID=ipsf.TemplateID
			WHERE ipcf.CycleID='".IntegerSafe($cycleID)."'";
    $templateList = $connection->returnResultSet($sql);
    $templateIDList = array();
    foreach($templateList as $temp){
        $templateIDList[] = $temp['TemplateID'];
    }
    $sql = "SELECT ipco.TemplateID,".$indexVar['libappraisal']->getLangSQL("CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi)","CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng)")." as FrmInfo,'OBS' as TemplateUsage
			FROM INTRANET_PA_C_OBSSEL ipco
			INNER JOIN INTRANET_PA_S_FRMTPL ipsf ON ipco.TemplateID=ipsf.TemplateID
			WHERE ipco.CycleID='".IntegerSafe($cycleID)."'";
    $obsList = 	$connection->returnResultSet($sql);
    foreach($obsList as $obs){
        if(!in_array($obs['TemplateID'],$templateIDList)){
            array_push($templateList, $obs);
        }
    }
    //debug_r($templateList);
    // Get all the appraisee list actively using
    $cond = "";
    if($userLogin!="" && $mode==1){
        $cond = " AND u.UserLogin='".$userLogin."'";
    }
    $sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("ChineseName","EnglishName")." as Name, u.UserID FROM
			(
			SELECT DISTINCT IF(frmsum.GrpID IS NOT NULL, ipcg.UserID, frmsum.UserID) as User FROM INTRANET_PA_T_FRMSUM frmsum
			LEFT JOIN INTRANET_PA_C_GRPMEM ipcg ON frmsum.GrpID=ipcg.GrpID AND ipcg.IsLeader=0
			WHERE frmsum.CycleID='".IntegerSafe($cycleID)."'
			) udata
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON udata.User = u.UserID WHERE 1 $cond
			ORDER BY User";
    $userList = $connection->returnResultSet($sql);
    //debug_r($userList);
    // Combine the result together and search for the appraiser on each form
    if(empty($userList) || empty($templateList)){
        echo "<table><tr><td align=center style='border:none;'><br>".$i_no_record_exists_msg."<br><br></td></tr></table>\n";
    }else{
        foreach($userList as $user){
            if(!isset($userInTemplateArr[$user['UserID']])){
                $userInTemplateArr[$user['UserID']] = array(
                    "Name"=>$user['Name'],
                    "TemplatesInfo"=>array()
                );
                foreach($templateList as $tmpl){
                    $appraiserList = array();
                    if($tmpl['TemplateUsage']=='OBS'){
                        $sql = "SELECT DISTINCT ObsID FROM INTRANET_PA_T_FRMSUB frmsub
								INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID=frmsum.RecordID
								WHERE frmsum.IsActive=1 AND frmsum.TemplateID='".$tmpl['TemplateID']."' AND frmsum.CycleID='".$cycleID."' AND frmsum.UserID='".$user['UserID']."'";
                        $obsList = current($connection->returnVector($sql));
                        $obsArr = explode(",",$obsList);
                        foreach($obsArr as $o){
                            $sql = "SELECT u.UserID as AprID, ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as AprName
									FROM ".$appraisalConfig['INTRANET_USER']." u WHERE UserID=".$o."";
                            $aprName = current($connection->returnResultSet($sql));
                            if(!isset($appraiserList[$aprName['AprID']])){
                                $appraiserList[$aprName['AprID']] = $aprName['AprName'];
                            }
                        }
                    }else{
                        $sql = "SELECT DISTINCT frmsum.AprID, frmsum.GrpID, ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as AprName FROM INTRANET_PA_T_FRMSUM frmsum
								INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON frmsum.AprID=u.UserID
								WHERE IsActive=1 AND frmsum.UserID='".$user['UserID']."' AND TemplateID='".$tmpl['TemplateID']."'";
                        $records = $connection->returnResultSet($sql);
                        foreach($records as $r){
                            if($r['GrpID']==""){
                                if(!isset($appraiserList[$r['AprID']])){
                                    $appraiserList[$r['AprID']] = $r['AprName'];
                                }
                            }else{
                                $sql = "SELECT AppMode FROM INTRANET_PA_C_GRPFRM gpfrm WHERE GrpID='".$r['GrpID']."' AND Incl=1";
                                $appMode = current($connection->returnVector($sql));
                                switch($appMode){
                                    case "1":
                                    case "2":
                                        $sql = "SELECT u.UserID as AprID, ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as AprName
												FROM INTRANET_PA_C_GRPMEM gpmem
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON gpmem.UserID=u.UserID
												WHERE gpmem.IsLeader=1 AND GrpID='".$r['GrpID']."'";
                                        break;
                                    case "3":
                                        $sql = "SELECT u.UserID as AprID, ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as AprName
												FROM INTRANET_PA_C_GRPMEM gpmem
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON gpmem.UserID=u.UserID
												WHERE gpmem.IsLeader<>1 AND GrpID='".$r['GrpID']."'";
                                        break;
                                    case "4":
                                        $sql = "SELECT u.UserID as AprID, ".$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as AprName
												FROM INTRANET_PA_C_GRPMEM gpmem
												INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON gpmem.UserID=u.UserID
												WHERE gpmem.UserID<>'".$user['UserID']."' AND GrpID='".$r['GrpID']."'";
                                        break;
                                }
                                $resultSet = $connection->returnResultSet($sql);
                                foreach($resultSet as $rs){
                                    if(!isset($appraiserList[$rs['AprID']])){
                                        $appraiserList[$rs['AprID']] = $rs['AprName'];
                                    }
                                }
                            }
                        }
                    }
                    $userInTemplateArr[$user['UserID']]["TemplatesInfo"][$tmpl['TemplateID']]= array(
                        "templateName"=>$tmpl['FrmInfo'],
                        "Appraiser"=>$appraiserList
                    );
                }
            }
        }
        
        $output = "<table class='common_table_list_v30'>";
        
        //template display
        $cellWidth = 95.0/count($templateList);
        $output .= "<tr>";
        $output .= "	<thead>";
        $output .= "		<th style='width:5%;'></th>";
        foreach($templateList as $tmpl){
            $output .= "		<th style='width:".$cellWidth."%;'>".$tmpl['FrmInfo']."</th>";
        }
        $output .= "	</thead>";
        $output .= "</tr>";
        
        //each appraisee display
        foreach($userInTemplateArr as $uid=>$uit){
            //template display
            $output .= "<tr>";
            $output .= "	<td>".$indexVar['libappraisal']->getUserNameID($uid,'name')."</td>";
            foreach($uit['TemplatesInfo'] as $tmplID=>$tmplData){
                if(empty($tmplData['Appraiser']) || implode("<br/>", $tmplData['Appraiser'])==""){
                    $output .= "	<td>";
                    $output .= "	</td>";
                }else{
                    $output .= "	<td>";
                    $output .= $Lang['Appraisal']['ReportOutstanding']['Filler'].":<br/><br/>";
                    $output .= implode("<br/>", $tmplData['Appraiser']);
                    $output .= "	</td>";
                }
            }
            $output .= "</tr>";
        }
        
        $output .= "</table>";
        
        echo $output;
    }
}else if($type=="GetLoginUserSelection"){
    $cycleID = $_POST['CycleID'];
    if($cycleID==""){
        echo '';
        die();
    }else{
        $name_field = getNameFieldByLang("USR.");
        $sql = "SELECT Distinct USR.userLogin,
				CASE
					WHEN au.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")')
					ELSE {$name_field}
				END  as name FROM INTRANET_PA_T_FRMSUM frmsum
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." USR ON frmsum.UserID=USR.UserID
				LEFT JOIN INTRANET_ARCHIVE_USER au ON USR.UserID = au.UserID
				WHERE frmsum.CycleID='".$cycleID."'";
        $resultList=$connection->returnResultSet($sql);
        $selList = array();
        foreach($resultList as $r){
            array_push($selList, array("id"=>$r['userLogin'], "name"=>$r['name']));
        }
        echo  getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($selList, "id","name","name"), $selectionTags='id="LoginID" name="LoginID"', $SelectedType="", $all=0, $noFirst=0);
    }
}






?>