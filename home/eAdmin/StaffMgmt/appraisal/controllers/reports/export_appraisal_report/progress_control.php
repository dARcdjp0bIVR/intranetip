<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT.'includes/json.php');
$json = new JSON_obj();
// The file has JSON type.
header('Content-Type: application/json');
$file = str_replace(".", "", $_GET['file']);
$file = $PATH_WRT_ROOT."/file/temp/" . $file . ".txt";
// Make sure the file is exist.
if (file_exists($file)) {
  // Get the content and echo it.
  $text = file_get_contents($file);
  echo $text;  

// Convert to JSON to read the status.
  $obj = $json->decode($text);
  // If the process is finished, delete the file.
  if ($obj->percent == 100) {
    unlink($file);
  }
}   
else {
  echo $json->encode(array("percent" => 0, "message" => 0));
}
?>