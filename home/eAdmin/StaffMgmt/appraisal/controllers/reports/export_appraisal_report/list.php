<?php
// ============================== Related Tables ==============================
// INTRANET_PA_T_FRMSUM,INTRANET_PA_S_FRMTPL,INTRANET_PA_C_GRPSUM,INTRANET_PA_C_IDVFRM,INTRANET_PA_C_BATCH,INTRANET_PA_T_FRMSUB,INTRANET_PA_S_APPROL
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ==============================
### check access right


$currentDate = date("Y-m-d");

$arrCookies = array();
$arrCookies[] = array("records_keyword", "keyword");
if(isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
}
else {
    updateGetCookies($arrCookies);
}

/*
 $ltimetable = new Timetable();
 $curTimetableId = $ltimetable->Get_Current_Timetable();
 */
$connection = new libgeneralsettings();
$currentDate = date("Y-m-d");
$a = $indexVar['libappraisal']->getAcademicYearTerm($currentDate);

if(!isset($cycleID)){
    // get Current Cycle Description
    $sql = "SELECT CycleID,AcademicYearID,DescrChi,DescrEng FROM INTRANET_PA_C_CYCLE WHERE AcademicYearID=".IntegerSafe($a[0]["AcademicYearID"]).";";
    $a = $connection->returnResultSet($sql);
    $cycleID = ($a[0]["CycleID"]!="")?$a[0]["CycleID"]:0;
    $cycleDesc = Get_Lang_Selection($a[0]["DescrChi"],$a[0]["DescrEng"]);
}

// This report is for 伍時暢 Form F4 only
//$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMTPL WHERE IsObs=1 AND (FrmTitleChi LIKE '%F4%' OR FrmTitleEng LIKE '%F4%')";
//$f4FormTplIDList = $connection->returnVector($sql);

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ExportAppraisalReports']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 7 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 10 : $numPerPage;

$x = "<br/>";

// ============================== Filter for Cycle (If not set, use current Cycle) ===================================
$conds = "";
if(!in_array($_SESSION['UserID'], $indexVar['libappraisal']->getReportDisplayPersonList(false))){
    $conds = " WHERE ReportReleaseDate IS NOT NULL ";
}
$sql="SELECT CycleID, DescrChi, YearNameB5, DescrEng, YearNameEN
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE $conds
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
$tmp_cycleList=$connection->returnResultSet($sql);
$cycleList = array();
if (count($tmp_cycleList) > 0) {
    foreach ($tmp_cycleList as $kk => $vv) {
        $cycleList[$kk] = array();
        $cycleList[$kk]["CycleID"] = $vv["CycleID"];
        if ($indexVar['libappraisal']->isEJ()) {
            $cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $indexVar['libappraisal']->displayChinese($vv["YearNameB5"]) . ")";
        } else {
            $cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $vv["YearNameB5"] . ")";
        }
        $cycleList[$kk]["DescrEng"] = $vv["DescrEng"] . " (" . $vv["YearNameEN"] . ")";
    }
}

$sql="SELECT ipcf.TemplateID,ipsf.FrmInfoChi,ipsf.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipcf
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
		UNION
		SELECT ipof.TemplateID,ipsf2.FrmInfoChi,ipsf2.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID=".IntegerSafe($cycleID)."
		) as ipof
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf2 ON ipof.TemplateID=ipsf2.TemplateID";


$FormList = $connection->returnArray($sql);

$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .="<tr>"."\r\n";
$x .= "<td width=\"30%\">".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['AppraisalPeriod']."</td><td>:</td>"."\r\n";
$x .= "<td width=\"70%\">".getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="CycleID" name="CycleID"', $SelectedType=$a[$i]["CycleID"], $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('cycleIDEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
$x .= "</td>";
$x .= "</tr>"."\r\n";
$x .= "<tr>"."\r\n";
if (!$_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]) {
    $x .= "<td>"."\r\n";
    $x .= $indexVar['libappraisal_ui']->RequiredSymbol()."<span>".$Lang['Appraisal']['ReportPersonResult']['SelectAppraisee']."</span><br/>";
    $x .= "</td>"."\r\n";
    $x .= "<td>:</td>";
    $x .= "<td width=\"70%\">";
    $x .= "<span id=\"appraiseelist_selection\">";
    $x .= "<select size=\"10\" multiple=\"multiple\">";
    $x .= "<option disabled> -- 選擇 -- </option>";
    $x .= "</select>";
    $x .= "</span>";
    $x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('appraiseeListEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
    $x .= "</td>";
    $x .= "<td>";
    $x .= "</td>";
}else{
    $sql = "SELECT UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$_SESSION['UserID']."'";
    $LoginID = current($connection->returnVector($sql));
    $x .= "<td><input type=\"hidden\" id=\"LoginID\" name=\"LoginID\" value=\"".$LoginID."\"><input type=\"hidden\" name=\"UserID\" id=\"UserID\" value=\"".$_SESSION['UserID']."\"/></td>";
}
$x .= "</tr>"."\r\n";

$x .= "</table>";

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptGen'], "submit", "goRptGen()", 'genRptBtn');

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// ============================== Define Button ==============================

?>