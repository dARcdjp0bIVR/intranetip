<?php
@ini_set("memory_limit","1024M");
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST
// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
if(!isset($PATH_WRT_ROOT)){
    $PATH_WRT_ROOT = '../../../../../../../';
    include_once($PATH_WRT_ROOT."includes/global.php");
    include_once($PATH_WRT_ROOT."includes/libdb.php");
    include_once($PATH_WRT_ROOT."includes/libinterface.php");
    include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");
    include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
    include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
    include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
    $indexVar['curUserId'] = $_SESSION['UserID'];
    intranet_auth();
    intranet_opendb();
}
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/appraisal/gen_form_class.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");


# Page Title
// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
$connection = new libgeneralsettings();
$json = new JSON_obj();
$cycleID=$_GET["cycleID"];
$inclObs=$_GET["inclObs"];
$displayName = $_GET["displayName"];
$displayDate = $_GET["displayDate"];
$outputDOC = $_GET["outputDOC"];
$displayCover = $_GET["displayCover"];
$loginID=$_GET["loginID"];
$outputAsZip = $_GET['outputAsZip'];
$outputAsZipPath = $_GET['outputAsZipPath'];

$sysDate=date("Y-m-d");
/*
 $x .= "<div align=\"right\">"."\r\n";
 $x .= "<input type=\"button\" class=\"formsubbutton\" onclick=\"javascript:window.print();\" name=\"PrintBtn\" id=\"PrintBtn\" value=".$Lang['Appraisal']['ReportPrint']." onmouseover=\"this.className='formsubbuttonon'\" onmouseout=\"this.className='formsubbutton'\">";
 $x .= "<br/><div id=\"resizeFontLoadingDiv\" style=\"display:none; float:right;\">
 <img src=\"/images/2009a/indicator.gif\"><span class=\"tabletextremark\"><span style=\"color:red;\">".$Lang['Appraisal']['ReportProcessing'] ."</span></span></div>";
 $x .= "</div>"."\r\n";
 */

$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();
global $intranet_session_language, $Lang;

$connection = new libgeneralsettings();
$genform = new genform();
$archived = false;

if($indexVar['libappraisal']->isEJ()){
    $sql="SELECT u8.UserID,u8.UserLogin FROM ".$appraisalConfig['INTRANET_USER']." u8
		INNER JOIN INTRANET_USER u ON u8.UserID=u.UserID
		WHERE u8.UserLogin='".$loginID."' AND u.RecordType=1 AND u.RecordStatus=1";
    $a=$connection->returnResultSet($sql);
}else{
    $sql="SELECT UserID,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$loginID."' AND RecordType=1 AND RecordStatus=1";
    $a=$connection->returnResultSet($sql);
}
if(empty($a)){
    if($indexVar['libappraisal']->isEJ()){
        $sql="SELECT UserID,UserLogin FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserLogin='".$loginID."' AND RecordType=1 AND RecordStatus=1";
        $a=$connection->returnResultSet($sql);
    }else{
        $sql="SELECT UserID,UserLogin FROM INTRANET_ARCHIVE_USER WHERE UserLogin='".$loginID."';";
        $a=$connection->returnResultSet($sql);
        if(!empty($a)){
            $archived = true;
            $userTable = $appraisalConfig['INTRANET_USER'];
            $appraisalConfig['INTRANET_USER'] = "INTRANET_ARCHIVE_USER";
        }
    }
}
$userID=$a[0]["UserID"];
//echo $sql."<br/><br/>";

if(isset($outputAsZip) && $outputAsZip==true){
    if($outputAsZipPath == $intranet_root."/file/temp/export_appraisal_report"){
        $outputAsZipPath = $outputAsZipPath."/".$loginID.".pdf";
    }else{
        $fileName = str_replace($intranet_root."/file/temp/export_appraisal_report/", "", $outputAsZipPath);
        $outputAsZipPath = $outputAsZipPath."/".$fileName.".pdf";
    }
}

$cycleToSchoolYearSql = "SELECT AcademicYearID FROM INTRANET_PA_C_CYCLE WHERE CycleID='".$cycleID."'";
$correspondingYearID = current($indexVar['libappraisal']->returnVector($cycleToSchoolYearSql));

if($displayCover=="1"){
    $cover = '<div id="content" class="cover_content" style="left:0; right: 0;">';
    
    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE SettingName = 'ReportCoverPageSetting'";
    $settings = current($connection->returnVector($sql));
    $coverPageSettingValues = $json->decode($settings);
    $logoLink = "#";
    if(isset($coverPageSettingValues['SchoolEmblemPhoto']) && $coverPageSettingValues['SchoolEmblemPhoto']!=""){
        $logoLink = '/file/appraisal/cover_image/'.$coverPageSettingValues['SchoolEmblemPhoto'];
        if($outputDOC=="1"){
            $http_protocol = checkHttpsWebProtocol()?'https://':'http://';
            $logoLink = $http_protocol.$_SERVER['HTTP_HOST'].$logoLink;
        }
    }
    $school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
    $school_name = $school_data[0];
    $school_org = $school_data[1];
    
    $sql = "SELECT YearNameEN, YearNameB5 FROM ACADEMIC_YEAR WHERE AcademicYearID='".$correspondingYearID."'";
    $yearNameResult = current($indexVar['libappraisal']->returnArray($sql));
    if($coverPageSettingValues['Language']=='Zh'){
        $schoolYear = $yearNameResult['YearNameB5'];
    }else{
        $schoolYear = $yearNameResult['YearNameEN'];
    }
    
    $sql = "SELECT EnglishName, ChineseName FROM ".$appraisalConfig['INTRANET_USER']." WHERE UserID='".$userID."'";
    $userNameResult = current($indexVar['libappraisal']->returnArray($sql));
    
    $nameField = ($coverPageSettingValues['Language']=='Zh')?"u.ChineseName":"u.EnglishName";
    $roleField = ($coverPageSettingValues['Language']=='Zh')?"approl.NameChi":"approl.NameEng";
    $sql = "SELECT $nameField as name, $roleField as role From INTRANET_PA_T_FRMSUB frmsub
			INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID=frmsum.RecordID
			INNER JOIN INTRANET_PA_S_APPROL approl ON frmsub.AppRoleID = approl.AppRoleID
			INNER JOIN INTRANET_USER u ON frmsub.FillerID = u.UserID
			WHERE frmsum.CycleID='".$cycleID."' AND frmsum.UserID='".$userID."'
			GROUP BY frmsub.FillerID, approl.AppRoleID";
    $approleList = $indexVar['libappraisal']->returnArray($sql);
    $cover .= '
			<div class="page-wrapper" style="width:19.6cm; height:28cm; margin:0 auto; border:1px solid #fff;">
				<div class="page-content" style="margin:.5cm 2cm 1cm 2cm; height:27.7cm;">
					<table class="main" style="table-layout: fixed;height:100%; width:100%;">
						<tr class="box-upper" style="height:4.5cm; max-height:4.5cm;">
							<td>';
    if($coverPageSettingValues["GroupName"] == 1){
        $cover .= '<div class="org-name" style="font-size:2rem; margin-bottom:1rem;">'.$indexVar['libappraisal']->displayChinese($school_org).'</div>';
    }
    if($coverPageSettingValues["SchoolName"] == 1){
        $cover .= '<div class="sch-name" style="font-size:3.5rem;">'.$indexVar['libappraisal']->displayChinese($school_name).'</div>';
    }
    $cover .= '
							</td>
						</tr>
						<tr class="box-logo" style="height:9cm;">
							<td>';
    if($coverPageSettingValues["SchoolEmblem"] == 1){
        $cover .= '<div class="sch-logo"><img style="max-width:100%; height:7cm; max-height:8cm;" id="school_emblem" src="'.$logoLink.'"></div>';
    }
    $cover .= '			</td>
						</tr>
						<tr class="box-middle" style="max-height:12cm;">
							<td>';
    if($coverPageSettingValues["SchoolYear"] == 1){
        $cover .= '<div class="sch-year" style="font-size:3rem; margin-bottom:1rem;">'.$indexVar['libappraisal']->displayChinese($schoolYear).'</div>';
    }
    if($coverPageSettingValues["ReportName"] == 1){
        $cover .= '<div class="report-name" style="font-size:4rem; margin-bottom:3rem;">'.$indexVar['libappraisal']->displayChinese($coverPageSettingValues["ReportNameText"]).'</div>';
    }
    if($coverPageSettingValues["TeacherChineseName"] == 1){
        $cover .= '<div class="tea-name" style="font-size:3rem;">'.$userNameResult['ChineseName'];
    }
    if($coverPageSettingValues["TeacherEnglishName"] == 1){
        $cover .= '<br>'.$userNameResult['EnglishName'].'</div>';
    }
    $cover .= '			</td>
						</tr>
						<tr class="box-lower">
							<td>';
    if($coverPageSettingValues["SchoolMotto"] == 1){
        $cover .= '<div class="sch-motto" style="font-size:1.8rem; margin-top:1rem;">'.$indexVar['libappraisal']->displayChinese($coverPageSettingValues["SchoolMottoText"]).'</div>';
    }
    if($coverPageSettingValues["AllAppRole"] == 1){
        $cover .= '<table class="roles" style="font-size:1.3rem; width:100%; margin-top:2rem;">';
        $idx=0;
        foreach($approleList as $nameRole){
            if($idx==0){
                $cover .= '<tr>';
            }
            $cover .= '<td>'.$indexVar['libappraisal']->displayChinese($nameRole['role']).': '.$indexVar['libappraisal']->displayChinese($nameRole['name']).'</td>';
            if($idx==count($approleList)-1){
                $cover .= '</tr>';
            }elseif($idx % 2 == 1){
                $cover .= '</tr><tr>';
            }
            $idx++;
        }
        $cover .= '		</table>';
    }
    $cover .= '		</td>
						</tr>
					</table>
				</div>
			</div>
			';
    $cover .= '</div>';
}

$sql="SELECT RlsNo,iptf.RecordID,iptf.CycleID,iptf.TemplateID,UserID,BatchID
	FROM(
		SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE UserID=".IntegerSafe($userID)." AND CycleID=".$cycleID."
	) as iptf
	INNER JOIN(
		SELECT MAX(RlsNo) as RlsNo,RecordID FROM INTRANET_PA_T_FRMSUB WHERE IsLastSub=1 AND ObsID IS NULL GROUP BY RecordID
	) as iptfrmsub ON iptf.RecordID=iptfrmsub.RecordID
	INNER JOIN(
		SELECT TemplateID,IsObs,DisplayOrder FROM INTRANET_PA_S_FRMTPL
	) as ipsfrmtpl ON iptf.TemplateID=ipsfrmtpl.TemplateID
	INNER JOIN (
		SELECT RlsNo as RlsNo2, RecordID as RecordID2,MAX(BatchID) as BatchID FROM INTRANET_PA_T_FRMSUB WHERE IsLastSub=1 GROUP BY RlsNo, RecordID
	) iptfrmsub2 ON iptfrmsub.RecordID = iptfrmsub2.RecordID2 AND iptfrmsub.RlsNo = iptfrmsub2.RlsNo2 ORDER BY ipsfrmtpl.DisplayOrder ASC, UserID ASC
";
//echo $sql."<br/><br/>";
$a=$connection->returnResultSet($sql);
$formContent = array();
for($i=0; $i<sizeof($a);$i++){
    if($outputDOC=="1"){
        if($i > 0){
            $x .= "<p style=\"page-break-after:always;\"></p>";
        }
    }else{
        $x = "";
    }
    if($displayName || $displayDate){
        $nameField = ($displayName)?$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as UserName":"";
        $dateField = ($displayDate)?"sub.SubDate":"";
        $fieldArr = array_filter(array($nameField,$dateField));
        $aprInfoSql = "SELECT ".implode(",",$fieldArr)." FROM INTRANET_PA_T_FRMSUB sub
					INNER JOIN INTRANET_PA_T_FRMSUM sum ON sub.RecordID=sum.RecordID
					INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON sum.AprID=u.UserID
					WHERE sub.RecordID=".$a[$i]["RecordID"]." ORDER BY SubDate DESC LIMIT 1";
        $aprInfo = current($connection->returnArray($aprInfoSql));
        $x = "<div>";
        if($displayName){
            $x.="<span style=\"float:left;\"><b>".$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser']."</b>: ".$aprInfo['UserName']."</span>  ";
        }
        if($displayDate){
            $x.= "<span style=\"float:right;\"><b>".$Lang['Appraisal']['ARFormDate']."</b>: ".$aprInfo['SubDate']."</span>";
        }
        $x.= "</div><br/><br/>";
    }
    $x .= $genform->exeForm($connection,$a[$i]["TemplateID"],$a[$i]["UserID"],$a[$i]["RecordID"],$a[$i]["CycleID"],$a[$i]["RlsNo"],$a[$i]["BatchID"], $isPrint=true);
    if($outputDOC=="0"){
        if($i < sizeof($a)-1){
            $x .= "<p style=\"page-break-after:always;\"></p>";
        }
        array_push($formContent, $x);
    }
}

if($inclObs==1){
    $sql="SELECT RlsNo,iptf.RecordID,iptf.CycleID,iptf.TemplateID,UserID,BatchID,ObsID
	FROM(
		SELECT RecordID,CycleID,TemplateID,UserID FROM INTRANET_PA_T_FRMSUM WHERE UserID=".IntegerSafe($userID)." AND CycleID=".$cycleID."
	) as iptf
	INNER JOIN(
		SELECT MAX(RlsNo) as RlsNo,RecordID,ObsID FROM INTRANET_PA_T_FRMSUB WHERE IsLastSub=1 AND ObsID IS NOT NULL GROUP BY RecordID
	) as iptfrmsub ON iptf.RecordID=iptfrmsub.RecordID
	INNER JOIN(
		SELECT TemplateID,IsObs,DisplayOrder FROM INTRANET_PA_S_FRMTPL WHERE IsObs=1
	) as ipsfrmtpl ON iptf.TemplateID=ipsfrmtpl.TemplateID
	INNER JOIN (
		SELECT RlsNo as RlsNo2, RecordID as RecordID2,MAX(BatchID) as BatchID FROM INTRANET_PA_T_FRMSUB WHERE IsLastSub=1 GROUP BY RlsNo, RecordID
	) iptfrmsub2 ON iptfrmsub.RecordID = iptfrmsub2.RecordID2 AND iptfrmsub.RlsNo = iptfrmsub2.RlsNo2 ORDER BY ipsfrmtpl.DisplayOrder ASC, UserID ASC
";
    // echo $sql."<br/><br/>";
    $a=$connection->returnResultSet($sql);
    if($outputDOC=="0"){
        if(sizeof($a) > 0){
            $x = "";
            $x .= "<p style=\"page-break-after:always;\"></p>";
            array_push($formContent, $x);
        }
    }
    for($i=0; $i<sizeof($a);$i++){
        if($outputDOC=="1"){
            if($i > 0 || empty($formContent)){
                $x .= "<p style=\"page-break-after:always;\"></p>";
            }
        }else{
            $x = "";
        }
        if($displayName || $displayDate){
            $nameField = ($displayName)?$indexVar['libappraisal']->getLangSQL("u.ChineseName","u.EnglishName")." as UserName":"";
            $dateField = ($displayDate)?"sub.SubDate":"";
            $fieldArr = array_filter(array($nameField,$dateField));
            $aprInfoSql = "SELECT ".implode(",",$fieldArr)." FROM INTRANET_PA_T_FRMSUB sub
						INNER JOIN INTRANET_PA_T_FRMSUM sum ON sub.RecordID=sum.RecordID
						INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON sub.ObsID=u.UserID
						WHERE sub.RecordID=".$a[$i]["RecordID"]." ORDER BY SubDate DESC LIMIT 1";
            $aprInfo = current($connection->returnArray($aprInfoSql));
            $x = "<div>";
            if($displayName){
                $x.="<span style=\"float:left;\"><b>".$Lang['Appraisal']['CycleTemplate']['AssignedAppraiser']."</b>: ".$aprInfo['UserName']."</span>  ";
            }
            if($displayDate){
                $x.= "<span style=\"float:right;\"><b>".$Lang['Appraisal']['ARFormDate']."</b>: ".$aprInfo['SubDate']."</span>";
            }
            $x.= "</div><br/><br/>";
        }
        $x .= $genform->exeForm($connection,$a[$i]["TemplateID"],$a[$i]["UserID"],$a[$i]["RecordID"],$a[$i]["CycleID"],$a[$i]["RlsNo"],$a[$i]["BatchID"], $isPrint=true);
        if($outputDOC=="0"){
            if($i < sizeof($a)-1){
                $x .= "<p style=\"page-break-after:always;\"></p>";
            }
            array_push($formContent, $x);
        }
    }
}

if($archived==true){
    $appraisalConfig['INTRANET_USER'] = $userTable;
}

//echo $x;

//$html = "";
//$html.= $x;
###############################################################################
###############################################################################
//$allStyle = "<style></style>";

$allStyle.="<style>";
if($displayCover=="1"){
    $allStyle.=" table.main td {vertical-align:middle; text-align:center;}";
    $allStyle.=" table.main .box-lower td {vertical-align:bottom;}";
}
$allStyle.=".common_table_list{
	width: 100%;border: 1px solid #CCCCCC;margin:0 auto;border-collapse:separate;border-spacing: 0px;*border-collapse: expression('separate', cellSpacing = '0px');
}
.common_table_list tr th{
	margin:0px;padding:3px;padding-top:5px;padding-bottom:5px;background-color: #A6A6A6;font-weight: normal;color: #FFFFFF;text-align:left;border-bottom: 1px solid #CCCCCC;
	border-right: 1px solid #CCCCCC	;
}
.common_table_list tr th a{	color: #FFFFFF;}";
if($outputDOC=="1"){
    $allStyle .=
    ".common_table_list tr th a.sort_asc {	font-weight: bold;display:block; float:left; background-repeat:no-repeat; background-position:0px 0px; padding-left:15px; }
				.common_table_list tr th a.sort_asc:hover {	 background-position:0px -80px; }
				.common_table_list tr th a.sort_dec {	font-weight: bold;display:block; float:left;
				background-repeat:no-repeat; background-position:0px -160px; padding-left:15px; }
				.common_table_list tr th a.sort_dec:hover {	 background-position:0px -240px; }
				.common_table_list  tr th a:hover{	color: #FF0000;}
				.common_table_list tr td{
					margin:0px;padding:3px;background-color: #FFFFFF;border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;
				}";
}else{
    $allStyle.="
		.common_table_list tr th a.sort_asc {	font-weight: bold;display:block; float:left;
		background-image:url(../../../images/2009a/icon_table_sort_tool.gif); background-repeat:no-repeat; background-position:0px 0px; padding-left:15px; }
		.common_table_list tr th a.sort_asc:hover {	 background-position:0px -80px; }
		.common_table_list tr th a.sort_dec {	font-weight: bold;display:block; float:left;
		background-image:url(../../../images/2009a/icon_table_sort_tool.gif); background-repeat:no-repeat; background-position:0px -160px; padding-left:15px; }
		.common_table_list tr th a.sort_dec:hover {	 background-position:0px -240px; }
		.common_table_list  tr th a:hover{	color: #FF0000;}
		.common_table_list tr td{
			margin:0px;padding:3px;background-color: #FFFFFF;border-bottom: 1px solid #CCCCCC;border-right: 1px solid #f1f1f1;vertical-align:top;padding-top:5px;padding-bottom:5px;
		}";
}
$allStyle.="
.common_table_list a {	color: #2286C5;}
.common_table_list a:hover {	color: #FF0000;}
.common_table_list  .num_check{	width:20px;}
.common_table_list  .num_class{	width:40px;}
.common_table_list  .record_code{	width:150px;}
.common_table_list  .record_code_long{	width:250px;}
.common_table_list  .record_tool{	width:100px;}
.common_table_list  .record_title{	width:200px;}
.common_table_list tr.sub_header th{	background-color:#B3B3B3}
.common_table_list tr.sub_row td{ background-color:#f4f4f4;  border-right: 1px solid #cfcfcf}
.common_table_list tr.sub_row td .form_table td { border-right: none}
.common_table_list tr.selected_row td{ background-color: #EFFDDB}
.common_table_list tr td.sub_function_row{ padding-left : 15px;}
.common_table_list tr td.rights_title_row{ background-color:#CCCCCC; width:120px; text-align:left}
.common_table_list tr td.rights_selected{ background-color: #EFFDDB}
.common_table_list tr td.rights_not_select_sub{ background-color: #eeeeee}
.common_table_list tr td.rights_not_select{ color:#999999}
.common_table_list tr th.dummy_row { background-color:#CCCCCC; width:1px; padding:0px;}
.common_table_list tr td.dummy_row { background-color:#A6A6A6; width:1px; padding:0px; border-bottom-color:#A6A6A6}
.common_table_list tr.move_selected td{ background-color:#fbf786; border-top: 2px dashed #d3981a; border-bottom: 2px dashed #d3981a}
.common_table_list tr.sub_sub_row td{ background-color:#eeeeee}
.common_table_list tr.seperate_row td{ background-color:#B3B3B3; text-align:left; color:#FFFFFF; font-style:italic}
.common_table_list tr.total_row td{ border-top:2px solid #666666}
.common_table_list tr td.total_col ,th.total_col { border-left:2px solid #999999}
.form_table_v30{ width:100%; margin:0 0;	border-collapse:separate;	border-spacing: 5px; 	*border-collapse: expression('separate', cellSpacing = '5px');}
.form_table_thickbox{ width:90%;}
.form_table_v30 tr td{	vertical-align:top;	text-align:left; line-height:18px; padding-top:5px; padding-bottom:5px;}
.form_table_v30 tr td.field_title{	border-bottom:1px solid #FFFFFF ; width:30%; background:#f3f3f3; padding-left:2px; padding-right:2px}
.form_table_v30 tr td.field_title_short{	border-bottom:1px solid #FFFFFF ; width:14%; background:#f3f3f3/*f4f4f4*/; padding-left:2px; padding-right:2px}
.form_table_v30 tr td{	border-bottom:1px solid #EFEFEF; padding-left:2px; padding-right:2px}";
if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
    $allStyle .= ".common_table_list{ font-size: 10px; }";
    $allStyle .= ".form_table_v30{ line-height:50%;font-size: 10px; }";
    $allStyle .= ".form_sub_title_v30 {line-height:1px;}";
    $allStyle .= ".form_ques_title td{ font-weight: bold; }";
    $allStyle .= " table.simpleSignature, table.simpleSignature tr, table.simpleSignature tr td { border-style: none; }";
    $allStyle .= " table.form_ques_gp_title td { font-size: 10px; }";
    $allStyle .= " p, div { font-size: 10px; }";
    $allStyle .= " div.formTitle, span.formObjective {font-size: 16px;} ";
    $allStyle .= " td{ padding-top:1px;padding-bottom:1px; }";
    $allStyle .= " div.questionBundle table.common_table_list th {font-size: 10px;} ";
    $allStyle .= " div.questionBundle table.common_table_list td {font-size: 10px;} ";
    $allStyle .= " table.SecDescr { padding-top: 0px;}";
    $allStyle .= " table.SecDescr td { font-size: 10px; }";
    $allStyle .= " td.field_title{ padding-left:0px; padding-right:0px }";
    $allStyle .= " .form_table_v30 tr td.field_title{width:25%;}";
    $allStyle .= " .form_sub_title_v30 .field_title, .sectiontitle_v30 {font-size: 16px;} ";
    $allStyle .= " .personal_details_form tr td { width:25%; }";
    $allStyle .= " .form_ques_title, .form_ques_content {line-height: 20px;}";
    $allStyle .= " .form_ques_title td, .form_ques_content td {line-height: 20px;}";
    $allStyle .= " .simpleSignature td { font-size: 16px; } ";
    $allStyle .= " .signature td { font-size: 12px; } ";
    $allStyle .= " .common_table_list tr th { background-color:#ffffff; color:#000000; font-weight:bold;  }";
}
$allStyle .= "</style>";

//echo $allStyle.$html;
//echo $allStyle;

if($outputDOC=="1"){
    $html = "";
    if($displayCover=="1"){
        $html .= $cover;
        $html .= '<br clear="all" style="page-break-before: always">';
    }
    $html.= str_replace('<p style="page-break-after:always;"></p>','<br clear="all" style="page-break-before: always">',$x);
    header("Content-type: application/vnd.ms-word; charset=UTF-8");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Disposition: attachment;Filename=personal_result.doc");
    echo "\xEF\xBB\xBF";
    echo '<html><head>'.$allStyle.'</head><body>'.$html.'</body></html>';
    
}else{
    if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
        $pdf = new mPDF('','A4',0,'',15, 15, 8, 8, 9, 9);
    }else{
        $pdf = new mPDF('','A4',0,'',15, 15, 16, 16, 9, 9);
    }
    $pdf->backupSubsFont = array('mingliu');
    $pdf->useSubstitutions = true;
    $pdf->allow_charset_conversion=true;
    $pdf->charset_in='UTF-8';
    $pdf->shrink_tables_to_fit = 0;
    if($sys_custom['eAppraisal']['LTMPSPrintingStyle']){
        $pdf->defaultfooterline=0;
        $pdf->SetFooter('P.{PAGENO}');
    }
    $pdf->WriteHTML($allStyle);
    if($displayCover=="1"){
        $pdf->WriteHTML($cover);
        $pdf->AddPage();
        //		echo $cover;
    }
    foreach($formContent as $html){
        $htmlContent = explode("<p style=\"page-break-after:always;\"></p>", $html);
        if(count($htmlContent)==1){
            $pdf->WriteHTML($html);
        }else{
            $htmlContent[0] = preg_replace('/(<br\/>)+$/', '', $htmlContent[0]);
            $pdf->WriteHTML($htmlContent[0]);
            for($idx=1; $idx < count($htmlContent); $idx++){
                $pdf->AddPage();
                if($htmlContent[$idx]!=""){
                    if($idx < count($htmlContent)){
                        $htmlContent[$idx] = preg_replace('/(<br\/>)+$/', '', $htmlContent[$idx]);
                    }
                    $pdf->WriteHTML($htmlContent[$idx]);
                }
            }
        }
        //		echo $html;
    }
    if(isset($outputAsZipPath) && $outputAsZipPath!=""){
        $pdf->Output($outputAsZipPath, 'F');
    }else{
        $pdf->Output('personal_result.pdf', 'I');
    }
}

?>