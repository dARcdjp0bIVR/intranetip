<?php 
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();

$type= ($_GET["Type"]=="")?$_POST['Type']:$_GET['Type'];
$q=$_GET["?q"];

// ============================== get Login User ==============================
if($type=="GetLoginUser"){
	$typeList = "1";		# 1-teacher only
	$name_field = getNameFieldByLang("USR.");
//	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
//		FROM(			
//			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin 
//			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $conds 
//			GROUP BY USR.UserID ORDER BY UserLogin
//		) as a
//		WHERE UserLogin like '%".$q."%'";
	if($junior_mck){
		$name_field_deleted = getNameFieldByLang2("AUR.");
	}else{
		$name_field_deleted = getNameFieldByLang("AUR.");
	}
	
	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
		FROM(			
			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin 
			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)  
			GROUP BY USR.UserID
			UNION
			SELECT AUR.UserID, $name_field_deleted as Name, ycu2.ClassNumber, AUR.UserLogin 
			FROM INTRANET_ARCHIVE_USER AUR LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=AUR.UserID) WHERE AUR.RecordType IN ($typeList)  
			GROUP BY AUR.UserID
		) as a
		WHERE UserLogin like '%".$q."%' ORDER BY UserLogin;
	";
	//echo $sql."<br/><br/>";
	$a=$connection->returnResultSet($sql);
	for ($i=0; $i<sizeof($a); $i++){
		$x .= $a[$i]["UserLogin"]."|".$a[$i]["UserID"]."|".$a[$i]["UserLogin"]."\n";
	}
	echo $x;
}else if($type=="GetLoginUserSelection"){
	$cycleID = $_POST['CycleID'];
	if($cycleID==""){
		echo '';
		die();
	}else{
		$name_field = getNameFieldByLang("USR.");
		$sql = "SELECT Distinct USR.userLogin, 
				CASE 
					WHEN au.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")') 
					ELSE {$name_field} 
				END  as name FROM INTRANET_PA_T_FRMSUM frmsum 
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." USR ON frmsum.UserID=USR.UserID				
				INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
				LEFT JOIN INTRANET_ARCHIVE_USER au ON USR.UserID = au.UserID
				WHERE frmsum.CycleID='".$cycleID."'AND frmsub.IsLastSub=1 ORDER BY USR.EnglishName ASC";
		$resultList=$connection->returnResultSet($sql);
		$selList = array();
		foreach($resultList as $r){
			array_push($selList, array("id"=>$r['userLogin'], "name"=>$r['name']));
		}		
		echo  getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($selList, "id","name","name"), $selectionTags='id="LoginID" name="LoginID"', $SelectedType="", $all=0, $noFirst=0);	
	}
}






?>