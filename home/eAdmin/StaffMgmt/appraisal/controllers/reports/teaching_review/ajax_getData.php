<?php
// ============================== Related Tables ==============================

// ============================== Related Tables ==============================
// ============================== Includes files/libraries ==============================
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

# Page Title
// ============================== Includes files/libraries ==============================

// ============================== Includes files/libraries ==============================
$connection = new libgeneralsettings();
$markerList = array();
$teacherPerformance = array();
$teacherQuestionList = array();
$questionData = array();
$type=($_GET["Type"]=="")?$_POST["Type"]:$_GET["Type"];
$q=$_GET["?q"];
$cycleID = $_POST['cycleID'];
$templateID = $_POST['templateID'];
$appraiseeID = $_POST['appraiseeID'];

// ============================== get Login User ==============================
if($type=="GetLoginUser"){
    $typeList = "1";		# 1-teacher only
    $name_field = getNameFieldByLang("USR.");
    //	$sql = "SELECT UserID,Name,ClassNumber,UserLogin
    //		FROM(
    //			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin
    //			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) $conds
    //			GROUP BY USR.UserID ORDER BY UserLogin
    //		) as a
    //		WHERE UserLogin like '%".$q."%'";
    if($junior_mck){
        $name_field_deleted = getNameFieldByLang2("AUR.");
    }else{
        $name_field_deleted = getNameFieldByLang("AUR.");
    }
    
    $sql = "SELECT UserID,Name,ClassNumber,UserLogin
		FROM(
			SELECT USR.UserID, $name_field as Name, ycu.ClassNumber, USR.UserLogin
			FROM ".$appraisalConfig['INTRANET_USER']." USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList)
			GROUP BY USR.UserID
			UNION
			SELECT AUR.UserID, $name_field_deleted as Name, ycu2.ClassNumber, AUR.UserLogin
			FROM INTRANET_ARCHIVE_USER AUR LEFT OUTER JOIN YEAR_CLASS_USER ycu2 ON (ycu2.UserID=AUR.UserID) WHERE AUR.RecordType IN ($typeList)
			GROUP BY AUR.UserID
		) as a
		WHERE UserLogin like '%".$q."%' ORDER BY UserLogin;
	";
    //echo $sql."<br/><br/>";
    $a=$connection->returnResultSet($sql);
    for ($i=0; $i<sizeof($a); $i++){
        $x .= $a[$i]["UserLogin"]."|".$a[$i]["UserID"]."|".$a[$i]["UserLogin"]."\n";
    }
    echo $x;
}else if($type=="GetLoginUserSelection"){
    $cycleID = $_POST['CycleID'];
    if($cycleID==""){
        echo '';
        die();
    }else{
        $name_field = getNameFieldByLang("USR.");
        $sql = "SELECT Distinct USR.userLogin, USR.UserID,
				CASE
					WHEN au.UserLogin IS NOT NULL then CONCAT({$name_field}, ' (".$Lang['Appraisal']['Deleted'].")')
					ELSE {$name_field}
				END  as name FROM INTRANET_PA_T_FRMSUM frmsum
				INNER JOIN ".$appraisalConfig['INTRANET_USER']." USR ON frmsum.UserID=USR.UserID
				INNER JOIN INTRANET_PA_T_FRMSUB frmsub ON frmsum.RecordID = frmsub.RecordID
				LEFT JOIN INTRANET_ARCHIVE_USER au ON USR.UserID = au.UserID
				WHERE frmsum.CycleID='".$cycleID."' AND frmsub.IsLastSub=1";
        $resultList=$connection->returnResultSet($sql);
        $selList = array();
        foreach($resultList as $r){
            array_push($selList, array("id"=>$r['UserID'], "name"=>$r['name']));
        }
        echo  getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($selList, "id","name","name"), $selectionTags='id="appraiseeID" name="appraiseeID"', $SelectedType="", $all=0, $noFirst=0);
    }
}else if($type=="GetCycleActiveTemplate"){
    $sql="SELECT ipcf.TemplateID,ipsf.FrmInfoChi,ipsf.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_FRMSEL WHERE CycleID='".IntegerSafe($cycleID)."'
		) as ipcf
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf ON ipcf.TemplateID=ipsf.TemplateID
		UNION
		SELECT ipof.TemplateID,ipsf2.FrmInfoChi,ipsf2.FrmInfoEng
		FROM(
			SELECT CycleID,TemplateID FROM INTRANET_PA_C_OBSSEL WHERE CycleID='".IntegerSafe($cycleID)."'
		) as ipof
		INNER JOIN(
			SELECT TemplateID,FrmTitleChi,FrmTitleEng,FrmCodChi,FrmCodEng,ObjChi,ObjEng,
			CONCAT(FrmTitleChi,'-',FrmCodChi,'-',ObjChi) as FrmInfoChi,CONCAT(FrmTitleEng,'-',FrmCodEng,'-',ObjEng) as FrmInfoEng,TemplateType
			FROM INTRANET_PA_S_FRMTPL
		) as ipsf2 ON ipof.TemplateID=ipsf2.TemplateID";
    $formList = $connection->returnArray($sql);
    echo getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($formList, "TemplateID","FrmInfoChi","FrmInfoEng"), $selectionTags='id="TemplateID" name="templateID"', $SelectedType=$templateID, $all=0, $noFirst=0);
}else if($type=="GetFormAppraiseeSel"){
    $sql = "SELECT DISTINCT u.UserID, ".Get_Lang_Selection("u.ChineseName","u.EnglishName")." as UserName FROM INTRANET_PA_T_FRMSUM ptfs
			INNER JOIN ".$appraisalConfig['INTRANET_USER']." u ON ptfs.UserID=u.UserID WHERE CycleID='".$_POST['cycleID']."' AND TemplateID='".$_POST['templateID']."'";
    $userList = $connection->returnArray($sql);
    $filter = "";
    $filter .= "<select name='appraiseeID' id='appraiseeID'>";
    foreach($userList as $ufc){
        $filter .= "	<option value='".$ufc['UserID']."' ".(($cycleID==$ufc['UserID'])?"selected":"").">".$indexVar['libappraisal']->displayChinese($ufc['UserName'])."</option>";
    }
    $filter .= "</select>";
    echo $filter;
}else if($type=="GetFormDetailsList"){
    $conds = "";
    
    $sql = "SELECT ".Get_Lang_Selection("YearNameB5","YearNameEN")." as YearTitle, ay.AcademicYearID FROM ACADEMIC_YEAR ay INNER JOIN INTRANET_PA_C_CYCLE cycle ON ay.AcademicYearID = cycle.AcademicYearID WHERE cycle.CycleID='".$cycleID."'";
    $yearData = current($connection->returnArray($sql));
    $yearTitle = $yearData['YearTitle'];
    $yearID = $yearData['AcademicYearID'];
    
    $sql = "SELECT RlsNo, frmsub.RecordID, BatchID, frmsub.FillerID,frmtpl.TemplateID,".Get_Lang_Selection("FrmTitleChi","FrmTitleEng")." as TemplateTitle FROM INTRANET_PA_T_FRMSUB frmsub
			INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID = frmsum.RecordID
			INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
			WHERE frmsum.CycleID='".$cycleID."' AND frmsum.UserID='".$appraiseeID."' AND frmtpl.IsObs=1 AND (frmtpl.FrmTitleChi LIKE '%F4%' OR frmtpl.FrmTitleEng LIKE '%F4%') AND frmsub.SubDate IS NOT NULL";
    $basicInfo = $connection->returnArray($sql);
    $formStatInfo = array();
    
    $totalFormNo = count($basicInfo);
    if($totalFormNo==0){
        echo "0|".$Lang['General']['NoRecordAtThisMoment'];
    }else{
        include_once($PATH_WRT_ROOT.'includes/libaccountmgmt.php');
        $libacm = new libaccountmgmt();
        $special_user_list = $libacm->getSpecialIdentityMapping($yearID,true);
        
        $sql = "SELECT ".$indexVar['libappraisal']->Get_Name_Field('u.')." as UserName FROM ".$appraisalConfig['INTRANET_USER']." u WHERE u.UserID='".$appraiseeID."'";
        $userName = current($connection->returnVector($sql));
        if(empty($userName)){
            $sql = "SELECT ".$indexVar['libappraisal']->Get_Archive_Name_Field('u.')." as UserName FROM INTRANET_ARCHIVE_USER u WHERE u.UserID='".$appraiseeID."'";
            $userName = current($connection->returnVector($sql));
        }
        $templateList = array();
        $yearClassSbjList = array();
        foreach($basicInfo as $binfo){
            if(!isset($templateList[$binfo['TemplateID']])){
                $templateList[$binfo['TemplateID']] = array();
            }
            
            if ($indexVar['libappraisal']->isEJ()) {
                $sql = 	" SELECT hdr.SubjectID, hdr.YearClassID, sbj.SubjectName as SbjTitle, yc.ClassName as ClassName FROM INTRANET_PA_T_SLF_HDR hdr ";
                $sql .= " INNER JOIN INTRANET_SUBJECT sbj ON hdr.SubjectID = sbj.SubjectID ";
                $sql .= " INNER JOIN INTRANET_CLASS yc ON hdr.YearClassID = yc.ClassID ";
                $sql .= " WHERE hdr.RlsNo='".$binfo['RlsNo']."' AND hdr.RecordID='".$binfo['RecordID']."' AND hdr.BatchID='".$binfo['BatchID']."'";
            }else{
                $sql = 	" SELECT hdr.SubjectID, hdr.YearClassID, ".Get_Lang_Selection("CH_DES","EN_DES")." as SbjTitle, ".Get_Lang_Selection("ClassTitleB5","ClassTitleEN")." as ClassName FROM INTRANET_PA_T_SLF_HDR hdr ";
                $sql .= " INNER JOIN ASSESSMENT_SUBJECT sbj ON hdr.SubjectID = sbj.RecordID ";
                $sql .= " INNER JOIN YEAR_CLASS yc ON hdr.YearClassID = yc.YearClassID ";
                $sql .= " WHERE hdr.RlsNo='".$binfo['RlsNo']."' AND hdr.RecordID='".$binfo['RecordID']."' AND hdr.BatchID='".$binfo['BatchID']."'";
            }
            $yearClassSbj = current($connection->returnArray($sql));
            if(!isset($yearClassSbjList[$yearClassSbj['SubjectID']."_".$yearClassSbj['YearClassID']])){
                $yearClassSbjList[$yearClassSbj['SubjectID']."_".$yearClassSbj['YearClassID']] = array(
                    'Subject'=>$yearClassSbj['SbjTitle'],
                    'Class'=>$yearClassSbj['ClassName']
                );
            }
            $templateList[$binfo['TemplateID']][] = array(
                'RlsNo'=>$binfo['RlsNo'],
                'RecordID'=>$binfo['RecordID'],
                'BatchID'=>$binfo['BatchID'],
                'TemplateTitle'=>$binfo['TemplateTitle'],
                'SubjectID'=>$yearClassSbj['SubjectID'],
                'YearClassID'=>$yearClassSbj['YearClassID'],
                'FillerID'=>$binfo['FillerID']
            );
            
        }
        foreach($templateList as $templateID=>$submission){
            echo "<br>";
            echo "<h2 style='text-align: center;'>僑港伍氏宗親會伍時暢紀念學校</h2>";
            echo "<h2 style='text-align: center;'>".$submission[0]['TemplateTitle']."(".$yearTitle.")</h2>";
            echo "<h3>".$Lang['Appraisal']['Report']['TeacherName'].": <u>".$userName."</u></h3>";
            
            $sql = "SELECT SecID, ".Get_Lang_Selection("SecTitleChi","SecTitleEng")." as SecTitle ";
            $sql .= " ,".Get_Lang_Selection("SecCodChi","SecCodEng")." as SecCode ";
            $sql .= " FROM INTRANET_PA_S_FRMSEC WHERE TemplateID='".$templateID."' AND ParentSecID IS NOT NULL ORDER BY DisplayOrder";
            $subSectionList = $connection->returnArray($sql);
            
            $subSectionContent = array();
            foreach($subSectionList as $subSection){
                if(!isset($subSectionContent[$subSection['SecID']])){
                    $subSectionContent[$subSection['SecID']] = array(
                        'Code'=>$subSection['SecCode'],
                        'Title'=>$subSection['SecTitle'],
                        'QuestionList'=>array()
                    );
                }
                $sql = "SELECT ques.QID, ".Get_Lang_Selection("ques.QCodChi","ques.QCodEng")." as QCode ";
                $sql .= " ,".Get_Lang_Selection("ques.DescrChi","ques.DescrEng")." as QTitle ";
                $sql .= "FROM INTRANET_PA_S_QUES ques INNER JOIN INTRANET_PA_S_QCAT qcat ON ques.QCatID=qcat.QCatID WHERE qcat.SecID='".$subSection['SecID']."' ORDER BY ques.DisplayOrder ASC";
                $qList = $connection->returnArray($sql);
                foreach($qList as $q){
                    if(!isset($subSectionContent[$subSection['SecID']]['QuestionList'][$q['QID']])){
                        $subSectionContent[$subSection['SecID']]['QuestionList'][$q['QID']] = array(
                            'Code'=>$q['QCode'],
                            'Title'=>$q['QTitle'],
                            'Responses'=>array()
                        );
                    }
                    
                    foreach($submission as $batch){
                        $sql = "SELECT Remark FROM INTRANET_PA_T_SLF_DTL dtl WHERE dtl.RlsNo='".$batch['RlsNo']."' AND dtl.RecordID='".$batch['RecordID']."' AND dtl.BatchID='".$batch['BatchID']."' AND QID='".$q['QID']."'";
                        $remarkContent = current($connection->returnVector($sql));
                        if(!isset($subSectionContent[$subSection['SecID']]['QuestionList'][$q['QID']]['Responses'][$batch['SubjectID']."_".$batch['YearClassID']])){
                            $subSectionContent[$subSection['SecID']]['QuestionList'][$q['QID']]['Responses'][$batch['SubjectID']."_".$batch['YearClassID']] = array();
                        }
                        $subSectionContent[$subSection['SecID']]['QuestionList'][$q['QID']]['Responses'][$batch['SubjectID']."_".$batch['YearClassID']][$batch['FillerID']] = $remarkContent;
                    }
                }
            }
            
            foreach($subSectionContent as $content){
                $subSectionTitle = ($content['Code']=="")?$content['Title']:$content['Code'].". ".$content['Title'];
                echo '<br>';
                echo '<h3>'.$subSectionTitle.'</h3>';
                echo '<table class="common_table_list_v30">';
                //header part
                echo '<tr>';
                if(count($content['QuestionList'])==1 && $content['QuestionList'][0]['Title']==""){
                    echo '<th rowspan="2"><div style="text-align:right">學科（班）</div><div style="text-align:left">評核員</div></th>';
                }else{
                    echo '<th rowspan="2"><div style="text-align:right">學科（班）</div><div style="text-align:left">評核準則</div></th>';
                }
                $idx=1;
                foreach($yearClassSbjList as $ycs){
                    echo '<th style="text-align:center">('.$idx.')</th>';
                    $idx++;
                }
                echo '</tr>';
                echo '<tr>';
                $idx=1;
                foreach($yearClassSbjList as $ycs){
                    echo '<th style="text-align:center">'.convert2unicode($ycs['Subject']).'('.convert2unicode($ycs['Class']).')'.'</th>';
                    $idx++;
                }
                echo '</tr>';
                if(count($content['QuestionList'])==1 && $content['QuestionList'][key($content['QuestionList'])]['Title']==""){
                    $question = $content['QuestionList'][key($content['QuestionList'])];
                    echo '<tr><td>科統籌/科組長</td>';
                    foreach($yearClassSbjList as $ycsIdx=>$ycs){
                        echo '<td><ul>';
                        if(isset($question['Responses'][$ycsIdx])){
                            foreach($question['Responses'][$ycsIdx] as $fillerID=>$response){
                                if(!in_array($fillerID,(array)$special_user_list[1]) && !in_array($fillerID,(array)$special_user_list[2])){
                                    $responseLines = explode("\n",$response);
                                    foreach($responseLines as $line){
                                        if($line!=""){
                                            echo '<li>'.$line.'</li>';
                                        }
                                    }
                                }
                            }
                        }
                        echo '</ul></td>';
                    }
                    echo '<tr>';
                    echo '<tr><td>校長</td>';
                    foreach($yearClassSbjList as $ycsIdx=>$ycs){
                        echo '<td><ul>';
                        if(isset($question['Responses'][$ycsIdx])){
                            foreach($question['Responses'][$ycsIdx] as $fillerID=>$response){
                                if(in_array($fillerID,(array)$special_user_list[1]) || in_array($fillerID,(array)$special_user_list[2])){
                                    $responseLines = explode("\n",$response);
                                    foreach($responseLines as $line){
                                        if($line!=""){
                                            echo '<li>'.$line.'</li>';
                                        }
                                    }
                                }
                            }
                        }
                        echo '</ul></td>';
                    }
                    echo '<tr>';
                }else{
                    foreach($content['QuestionList'] as $question){
                        $questionTitle = ($question['Code']=="")?$question['Title']:$question['Code'].". ".$question['Title'];
                        echo '<tr><td>'.$questionTitle.'</td>';
                        foreach($yearClassSbjList as $ycsIdx=>$ycs){
                            echo '<td><ul>';
                            if(isset($question['Responses'][$ycsIdx])){
                                foreach($question['Responses'][$ycsIdx] as $response){
                                    $responseLines = explode("\n",$response);
                                    foreach($responseLines as $line){
                                        if($line!=""){
                                            echo '<li>'.$line.'</li>';
                                        }
                                    }
                                }
                            }
                            echo '</ul></td>';
                        }
                        echo '</tr>';
                    }
                }
                echo '</table><br>';
            }
            echo '<p style="text-align:right">'.$Lang['Appraisal']['ARFormPrintDate'].': <u>'.date('Y-m-d').'</u></p>';
        }
    }
}




?>