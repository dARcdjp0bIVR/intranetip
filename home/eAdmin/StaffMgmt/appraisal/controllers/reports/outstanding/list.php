<?php
// ============================== Related Tables ==============================
// INTRANET_SLRS_TEACHER, INTRANET_SLRS_BALANCE_ADJUST, INTRANET_SLRS_LESSON_ARRANGEMENT
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$linterface = new interface_html();
$connection = new libgeneralsettings();

# Page Title
$TAGS_OBJ[] = array($Lang['Appraisal']['ReportOutstandingAppraisalList']);
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
/*
$sql="SELECT CycleID,CONCAT(DescrChi,' (', YearNameB5,')') as DescrChi,CONCAT(DescrEng,' (', YearNameEN,')') as DescrEng
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
	$cycleList=$connection->returnResultSet($sql);
*/
$sql="SELECT CycleID, DescrChi, YearNameB5, DescrEng, YearNameEN
	FROM(
		SELECT CycleID,DescrChi,DescrEng,AcademicYearID
		FROM INTRANET_PA_C_CYCLE
	) as ipsf
	INNER JOIN(
		SELECT AcademicYearID,YearNameEN,YearNameB5 FROM ACADEMIC_YEAR
	) as ay ON ipsf.AcademicYearID=ay.AcademicYearID";
$tmp_cycleList=$connection->returnResultSet($sql);
$cycleList = array();
if (count($tmp_cycleList) > 0) {
	foreach ($tmp_cycleList as $kk => $vv) {
		$cycleList[$kk] = array();
		$cycleList[$kk]["CycleID"] = $vv["CycleID"];
		if ($indexVar['libappraisal']->isEJ()) {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $indexVar['libappraisal']->displayChinese($vv["YearNameB5"]) . ")";
		} else {
			$cycleList[$kk]["DescrChi"] = $vv["DescrChi"] . " (" . $vv["YearNameB5"] . ")";
		}
		$cycleList[$kk]["DescrEng"] = $vv["DescrEng"] . " (" . $vv["YearNameEN"] . ")";
	}
}



$x ="<table class=\"form_table slrs_form\">"."\r\n";
$x .="<tr>"."\r\n";
$x .= "<td>".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['AppraisalPeriod']."</td><td>:</td>"."\r\n";
$x .= "<td>";
$x .= getSelectByAssoArray($indexVar['libappraisal']->cnvGeneralArrToSelect($cycleList, "CycleID","DescrChi","DescrEng"), $selectionTags='id="CycleID" name="CycleID"', $SelectedType=$a[$i]["CycleID"], $all=0, $noFirst=0);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('cycleIDEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
$x .= "</td>";
$x .= "</tr>"."\r\n";
if($sys_custom['eAppraisal']['ShowOutstandingByRole']){
$x .="<tr>"."\r\n";
$x .= "<td>".$indexVar['libappraisal_ui']->RequiredSymbol().$Lang['Appraisal']['TemplateSample']['SDFSecAppRoleName']."</td><td>:</td>"."\r\n";
$x .= "<td>";
$roles = $indexVar['libappraisal']->getAppRoleList();
$isChecked = 1;
foreach($roles as $idx=>$r){
	$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('RoleType_'.$idx, 'RoleType', $Value=$r['AppRoleID'], $isChecked=$isChecked, $Class="requiredField appraisalMode", $Display=$r['RoleName'], $Onclick="",$isDisabled);
	$isChecked = 0;	
}
//$x .= $indexVar['libappraisal_ui']->Get_Radio_Button('RoleTypeAppraisee', 'RoleType'.$i, $Value="2", $isChecked=0, $Class="requiredField appraisalMode", $Display=$Lang['Appraisal']['CycleTemplate']['StfIdvName'], $Onclick="",$isDisabled);
$x .= $indexVar['libappraisal_ui']->Get_Form_Warning_Msg('roleIDEmptyWarnDiv', $Lang['Appraisal']['Report']['MandatorySelection'], $Class='warnMsgDiv')."\r\n";
$x .= "</td>";
$x .= "</tr>"."\r\n";
}
$x .= "</table>";
		

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
$htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptGen'], "submit", "goRptGen()", 'genRptBtn');
// ============================== Define Button ==============================
















?>