<?php
/**
 * Change Log:
 * 2018-01-05 Pun
 *  - Added Non-admin case
 */
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();

$AcademicYearID = IntegerSafe($AcademicYearID);
$FromSubjectID = IntegerSafe($FromSubjectID);

#### Get teacher list START ####
$AllTeachers = $libLnt->getTeacherListByAcademicYearIdSubjectId($AcademicYearID, $FromSubjectID);
$allTeacherNameArr = BuildMultiKeyAssoc($AllTeachers, array('TeacherID') , array('TeacherName'), $SingleValue=1);
#### Get teacher list END ####

if($_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]){
    if($allTeacherNameArr[$_SESSION['UserID']]){
        echo $allTeacherNameArr[$_SESSION['UserID']].'<input type="hidden" id="TeacherID" name="TeacherID" value="'.$_SESSION['UserID'].'" />';
    }else{
        echo $Lang['General']['NoRecordAtThisMoment'];
    }
}else if($AllTeachers){
    #### Get teacher select box START ####
    $TeacherSelection = $indexVar['libappraisal_ui']->GET_SELECTION_BOX($AllTeachers, "name='TeacherID' id='TeacherID'", "", $ParSelected="");
    #### Get teacher select box END ####
    
    echo $TeacherSelection;
}else{
    echo $Lang['General']['NoRecordAtThisMoment'];
}