<?php
// ============================== Related Tables ==============================
// ============================== Related Tables ==============================

// ============================== Includes files/libraries ============================== 
### check access right
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");

$linterface = new interface_html();
$connection = new libgeneralsettings();
$libSCM_ui = new subject_class_mapping_ui();

# Page Title
$curTab="statSummary";
$TAGS_OBJ[] = array($Lang['Appraisal']['LNT']['LearningAndTeaching'], '#', $curTab=='statSummary');
$indexVar['libappraisal_ui']->echoModuleLayoutStart($indexVar['task'], $indexVar['returnMsgLang']);
echo $indexVar['libappraisal_ui']->Include_Thickbox_JS_CSS();

// ============================== Includes files/libraries ==============================


// ============================== UI START ==============================
$AcademicYearSelection = getSelectAcademicYear('AcademicYearID');
//$FromSubjectSelection = $libSCM_ui->Get_Subject_Selection('FromSubjectID', $FromSubjectID, $OnChange='', $noFirst=0, $firstTitle=$Lang['Appraisal']['LNT']['AllSubject'], '', $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $IncludeSubjectIDArr='');

$btnAry[] = array('print', 'javascript:Print()','',array(),' id="PrintBtn" style="display:none;"');
$htmlAry['contentTool'] = $indexVar['libappraisal_ui']->Get_Content_Tool_By_Array_v30($btnAry);
// ============================== UI END ==============================

ob_start();
?>


<form id="form1" name="form1">
	<table border="0" cellspacing="0" cellpadding="5" class="form_table_v30" style="max-width: 1024px;">
		<tr>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['SchoolYear']?></span></td>
			<td valign="top" id="year_single" ><?=$AcademicYearSelection ?></td>
		</tr>
		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['SysMgr']['SubjectClassMapping']['Subject']?></span></td>
			<td valign="top"><div id="FromSubjectIdDiv"><?=$indexVar['libappraisal_ui']->Get_Ajax_Loading_Image()?></div></td>
		</tr>
		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['General']['Teacher']?></span></td>
			<td valign="top">
				<div id="teacherSelectDiv"></div>
			</td>
		</tr>
		<tr id='classRow'>
			<td class="field_title"><span class="tabletext"><?=$Lang['Appraisal']['LNT']['Report']['ReportType']?></span></td>
			<td valign="top">
				<input type="radio" id="reportType1" name="reportType" value="1" checked/>
				<label for="reportType1"><?=$Lang['Appraisal']['LNT']['Report']['StatSummary'] ?></label>
				
				<input type="radio" id="reportType2" name="reportType" value="2" />
				<label for="reportType2"><?=$Lang['Appraisal']['LNT']['Report']['StatDetails'] ?></label>
			</td>
		</tr>
	</table>

	<span class="tabletextremark"></span>
	<p class="spacer"></p>
			
	<div class="edit_bottom_v30" style="max-width: 1024px;">
		<input type="button" id="Btn_View" value="<?=$Lang['Btn']['View']?>" class='formbutton' onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" />
	</div>
	
	<div id="PrintButton">
		<?php echo $htmlAry['contentTool']?>
	</div>
	

	<div id="PrintArea"><div id="DBTableDiv"></div></div>

	
</form>

<?php
$x = ob_get_clean();

$htmlAry['contentTbl'] = $x;
// ============================== Transactional data ==============================

// ============================== Define Button ==============================
// $htmlAry['viewBtn'] = $indexVar['libappraisal_ui']->Get_Action_Btn($Lang['Appraisal']['BtnRptGen'], "submit", "goRptGen()", 'genRptBtn');
// ============================== Define Button ==============================







