<?php
/**
 * Change Log:
 * 2018-01-05 Pun
 *  - Added access right checking
 *  - Added seperate YearClass result
 */
include_once($PATH_WRT_ROOT."includes/appraisal/lnt/libappraisal_lnt.php");

$libLnt = new libappraisal_lnt();
$objJson = new JSON_obj();

define('DECIMAL_PLACE_PERCENT', 1);
define('DECIMAL_PLACE_NORMAL', 2);
$AcademicYearID = IntegerSafe($AcademicYearID);
$FromSubjectID = IntegerSafe($FromSubjectID);
$TeacherID = IntegerSafe($TeacherID);
//$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];


#### Access Right START ####
if($_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["isService"]){
    $TeacherID = $_SESSION['UserID'];
}
#### Access Right END ####


#### Get YearClass Name ####
$sql = "SELECT
    YC.YearID,
    YC.YearClassID,
    YC.ClassTitleEN,
    YC.ClassTitleB5,
    Y.YearName
FROM
    YEAR_CLASS YC
INNER JOIN
    YEAR Y
USING 
    (YearID)
WHERE
    YC.AcademicYearID = '{$AcademicYearID}'
ORDER BY
    Y.Sequence, YC.Sequence";
$rs = $libLnt->returnResultSet($sql);

$yearNameMapping = array();
$allYearClass = array();
foreach($rs as $r){
    $r['ClassTitle'] = Get_Lang_Selection($r['ClassTitleB5'], $r['ClassTitleEN']);
    $allYearClass[$r['YearClassID']] = $r;
    $yearNameMapping["Y{$r['YearID']}"] = $r['YearName'];
}
#### Get YearClass END ####


#### Get questions START ####
$rs = $libLnt->getAllQuestionByAcademicYearID($AcademicYearID);
$questionSubjectId = array_filter(array_unique(Get_Array_By_Key($rs, 'SubjectID')));
$filterSubjectId = (in_array($FromSubjectID, $questionSubjectId))? $FromSubjectID : '';

$questions = array();
foreach($rs as $r){
    if($r['SubjectID'] != $filterSubjectId){
        continue;
    }
    $questions[] = $r;
}

$minScore = NULL;
$maxScore = NULL;
foreach($questions as $question){
    if($question['QuestionType'] == libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
        if($minScore === NULL || $question['MinScore'] < $minScore){
            $minScore = $question['MinScore'];
        }
        if($maxScore === NULL ||$question['MaxScore'] > $maxScore){
            $maxScore = $question['MaxScore'];
        }
    }
}

$countPartQuestion = array();
foreach($questions as $question){
    if($question['QuestionType'] != libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
        continue;
    }
    
    $countPartQuestion[$question['Part']]++;
}
#### Get questions END ####

#### Get answer START ####
$rs = $libLnt->getAnswerByAcademicYearIdSubjectIdTeacherId($AcademicYearID, $FromSubjectID, $TeacherID);

$subjectAnswerArr = array();
if($FromSubjectID){
    $subjectAnswerArr = $rs;
}else{
    foreach($rs as $r){
        if(in_array($r['SubjectID'], $questionSubjectId)){
            continue;
        }
        $subjectAnswerArr[] = $r;
    }
}

$allYearClassAnswers = BuildMultiKeyAssoc($subjectAnswerArr, array('YearClassID', 'QuestionID', 'RecordID') , array('Answer'), $SingleValue=1);
#### Get answer END ####

#### Calculate answer student START ####
$totalAnswerStudent = array();
foreach($subjectAnswerArr as $r){
    $totalAnswerStudent[$r['YearClassID']]++;
}
$totalQuestions = count($questions);

if($totalQuestions){
    foreach($totalAnswerStudent as $YearClassID => $totalAnswer){
        $totalAnswerStudent[$YearClassID] = $totalAnswer / $totalQuestions;
        if(!is_int($totalAnswerStudent[$YearClassID])){
            echo '<!--Total number of answer does not match question-->';
            exit;
        }
    }
}else{
    echo '<!--No question-->';
    exit;
}
#### Calculate answer student END ####

#### Group data (if result has only one student) START ####
$groupYearIdArr = array();
foreach($allYearClassAnswers as $YearClassID => $d){
    if($totalAnswerStudent[$YearClassID] == 1){
        $groupYearIdArr[] = $allYearClass[$YearClassID]['YearID'];
    }
}

$groupAnswers = array();
foreach($allYearClassAnswers as $YearClassID => $d){
    $yearClass = $allYearClass[$YearClassID];
    
    if(!isset($groupAnswers[$yearClass['YearID']])){
        $groupAnswers[$yearClass['YearID']] = array();
    }
    if(in_array($yearClass['YearID'], $groupYearIdArr)){
        foreach($d as $questionId => $d2){
            foreach($d2 as $recordId => $answer){                    
                $groupAnswers["Y{$yearClass['YearID']}"][$questionId][$recordId] = $answer;
            }
        }
    }else{
        $groupAnswers[$YearClassID] = $d;
    }
}
$groupAnswers = array_filter($groupAnswers);
#### Group data (if result has only one student) END ####


$statResultPartArr = array();
foreach($groupAnswers as $YearClassID => $allAnswers){
    if(strpos($YearClassID, 'Y') === 0){
        $classTitle = $yearNameMapping[$YearClassID];
    }else{
        $classTitle = $allYearClass[$YearClassID]['ClassTitle'];
    }
    
    #### Calculate data START ####
    ## Count the answer START ##
    $answerArr = array();
    foreach($questions as $question){
        if($question['QuestionType'] != libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
            continue;
        }
        
        $questionId = $question['QuestionID'];
        foreach($allAnswers[$questionId] as $answer){
            $answerArr[$questionId][$answer]++;
        }
    }
    // debug_r($answerArr);
    // debug_r($countAnswerArr);
    ## Count the answer END ##
    
    ## Calculate the average (by answer) START ##
    $answerAverageArr = array();
    foreach($answerArr as $questionId => $answerStat){
        $countAnswer = array_sum($answerStat);
        
        foreach($answerStat as $answer => $count){
            $answerAverageArr[$questionId][$answer] = $count / $countAnswer;
        }
    }
    // debug_r($answerAverageArr);
    ## Calculate the average (by answer) END ##
    
    ## Calculate the summary (by question) START ##
    $statResultQuestionArr = array();
    
    foreach($questions as $question){
        if($question['QuestionType'] != libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
            continue;
        }
    
        $questionId = $question['QuestionID'];
        $answers = $allAnswers[$questionId];
        $sum = array_sum($answers);
        $countAns = count($answers);
        $average = $sum / $countAns;
        
        $statResultQuestionArr['questionAverage'][$questionId] = $average;
    //     $statResultQuestionArr['SD'][$questionId] = $libLnt->sd($answers);
        if($sum == 0){
            $statResultQuestionArr['SD'][$questionId] = 0;
        }else{
            $statResultQuestionArr['SD'][$questionId] = $libLnt->sd($answers);
        }
    }
    
    
    ## Calculate the summary (by question) END ##
    
    ## Calculate the summary (by part) START ##
    $partAnswer = array();
    foreach($questions as $question){
        if($question['QuestionType'] != libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
            continue;
        }
    
        $questionId = $question['QuestionID'];
        $partTitle = $question['Part'];
        $answers = $allAnswers[$questionId];
        $partAnswer[$partTitle] = array_merge((array)$partAnswer[$partTitle], $answers);
    }
    
    foreach($partAnswer as $partTitle => $answers){
        $statResultPartArr[$YearClassID]['partAverage'][$partTitle] = array_sum($answers) / count($answers);
    }
    ## Calculate the summary (by part) END ##
    #### Calculate data END ####
    
    /* * /
    debug_r($countPartQuestion);
    debug_r($answerAverageArr);
    debug_r($statResultQuestionArr);
    debug_r($statResultPartArr);
    /* */
    
    ?>
    
    <style>
    .realNum{
        display:none;
    }
    </style>
    
    
    <h1 style="margin: 50px; margin-bottom: 0;"><?=$classTitle ?></h1>
    
    <div class="chart_<?=$YearClassID ?>" style="height: 500px"></div>
    
    <div class="chart_tables" style="width: 250px">
    	<table class="common_table_list_v30 view_table_list_v30 resultTable">
    		<thead>
    			<tr>
    				<th><?=$Lang['Appraisal']['LNT']['Part'] ?></th>
    				<th><?=$Lang['Appraisal']['LNT']['Report']['Average'] ?></th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php foreach($statResultPartArr[$YearClassID]['partAverage'] as $partTitle => $average){ ?>
    				<tr>
    					<td><?=$partTitle ?></td>
    					<td>
    						<?=number_format($average, DECIMAL_PLACE_NORMAL) ?>
    						<span class="realNum"><?=$average ?></span>
    					</td>
    				</tr>
    			<?php } ?>
    		</tbody>
    	</table>
    </div>
    
    
    <div class="chart_tables">
    	<table class="common_table_list_v30 view_table_list_v30 resultTable">
    		<thead>
    			<tr>
    				<th><?=$Lang['Appraisal']['LNT']['Part'] ?></th>
    				<th colspan="2"><?=$Lang['Appraisal']['LNT']['Question'] ?></th>
    				<?php for($i=$maxScore;$i>=$minScore;$i--){ ?>
    					<th><?=$i ?></th>
    				<?php } ?>
    				<th><?=$Lang['Appraisal']['LNT']['Report']['Average'] ?></th>
    				<th><?=$Lang['Appraisal']['LNT']['Report']['SD'] ?></th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php 
                $partIndex = 0;
                $questionIndex = 0;
    			$lastPart = '';
                foreach($questions as $question){
                    if($question['QuestionType'] != libappraisal_lnt::LNT_QUESTION_TYPE_SCALE){
                        continue;
                    }
                    
                    $questionId = $question['QuestionID'];
                    $part = $question['Part'];
                    $partTitle = $question['PartTitle'];
                    $partQuestionCount = $countPartQuestion[$part];
                    $questionTitle = $question['QuestionTitle'];
    			?>
            		<tr>
            			<?php 
        			    if($part != $lastPart){
    			            $questionIndex = 0;
    			            $partIndex++;
    			        ?>
    			        	<td rowspan="<?=$partQuestionCount ?>">
    			        		<span><?=$part ?></span>
    			        		<br />
    			        		<span><?=$partTitle ?></span>
    			        	</td>
    			        <?php
        			    }
            			?>
            			<td style="font-weight: normal;">#<?=($questionIndex + 1) ?></td>
            			<td><?=$questionTitle ?></td>
            			
            			
        				<?php for($i=$maxScore;$i>=$minScore;$i--){ ?>
        					<td>
        						<?=number_format($answerAverageArr[$questionId][$i] * 100, DECIMAL_PLACE_PERCENT) ?>%
        						<span class="realNum"><?=$answerAverageArr[$questionId][$i] ?></span>
        					</td>
        				<?php } ?>
        				
        				<td>
        					<?=number_format($statResultQuestionArr['questionAverage'][$questionId], DECIMAL_PLACE_NORMAL) ?>
    						<span class="realNum"><?=$statResultQuestionArr['questionAverage'][$questionId] ?></span>
        				</td>
        				<td>
        					<?=number_format($statResultQuestionArr['SD'][$questionId], DECIMAL_PLACE_NORMAL) ?>
    						<span class="realNum"><?=$statResultQuestionArr['SD'][$questionId] ?></span>
        				</td>
            			
        			</tr>
    			<?php 
    			    $lastPart = $part;
    			    $questionIndex++;
                }
    			?>
    		</tbody>
    	</table>
    </div>

<?php
} // End foreach($allYearClassAnswers as $YearClassID => $allAnswers)

?>

<script>
$(function () {
	Highcharts.createElement('link', {
	   href: 'https://fonts.googleapis.com/css?family=Dosis:400,600',
	   rel: 'stylesheet',
	   type: 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
	   colors: ["#7cb5ec", "#f7a35c", "#90ee7e", "#7798BF", "#aaeeee", "#ff0066", "#eeaaee",
	      "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"],
	   chart: {
	      backgroundColor: null,
	      style: {
	         fontFamily: "Dosis, sans-serif"
	      }
	   },
	   title: {
	      style: {
	         fontSize: '16px',
	         fontWeight: 'bold',
	         textTransform: 'uppercase'
	      }
	   },
	   tooltip: {
	      borderWidth: 0,
	      backgroundColor: 'rgba(219,219,216,0.8)',
	      shadow: false
	   },
	   legend: {
	      itemStyle: {
	         fontWeight: 'bold',
	         fontSize: '13px'
	      }
	   },
	   xAxis: {
	      gridLineWidth: 1,
	      labels: {
	         style: {
	            fontSize: '12px'
	         }
	      }
	   },
	   yAxis: {
	      minorTickInterval: 'auto',
	      title: {
	         style: {
	            textTransform: 'uppercase'
	         }
	      },
	      labels: {
	         style: {
	            fontSize: '12px'
	         }
	      }
	   },
	   plotOptions: {
	      candlestick: {
	         lineColor: '#404048'
	      }
	   },


	   // General
	   background2: '#F0F0EA'

	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);


// chart randering
	var yearClassData = <?=$objJson->encode($statResultPartArr); ?>;
// 	console.log(data);

	$.each(yearClassData, function(YearClassID, data){
		data = data['partAverage'];

    	var categories = [];
    	var series = [];
    	$.each(data, function( key, value ) {
    		categories.push('<?=$Lang['Appraisal']['LNT']['Part'] ?> ' + key);
    		series.push(Math.round(value*100)/100);
    	});
//     	console.log(categories);
//     	console.log(series);
    	
    	$('.chart_'+YearClassID).highcharts({
    	    chart: {
    	        type: 'column'
    	    },
    	    title: {
    	        text: ''
    	    },
    	    xAxis: {
    	        categories: categories
    	    },
    	    yAxis: {
    	    	title: {
    	    		text: '<?=$Lang['Appraisal']['LNT']['Report']['Average'] ?>'
    	    	},
    	        max: <?=$maxScore ?>,
    	        min: <?=$minScore ?>,
    	    },
    	    tooltip: {
    	    },
    	    plotOptions: {
    	        column: {
    	            pointPadding: 0.2,
    	            borderWidth: 0
    	        }
    	    },
    	    series: [{ 
    		    name: '<?=$Lang['Appraisal']['LNT']['Report']['Average'] ?>',
    		    data: series 
    	    }],
    	    credits : {enabled: false,},
    	});
	});
});
</script>