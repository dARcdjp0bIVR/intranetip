<?php
// Using: 

/** 
 * Log:
 * 2019-06-06 Cameron 
 * - apply IntegerSafe to $ct to avoid sql injection
 * 2018-08-08 Bill [2018-0807-1648-12073]
 * - fixed sql injection 
 * 2015-12-11 Paul [ip.2.5.7.1.1]
 * -Powerspeech modification on expiration policy
 * 2010-08-19 Sandy
 * - Plugin Powerspeech modification
 */

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libannounce.php");

intranet_auth();
intranet_opendb();

$ct = IntegerSafe($ct);     // color type
$AnnouncementID = IntegerSafe($AnnouncementID);
$lo = new libannounce($AnnouncementID);

$MODULE_OBJ['title'] = $ip20_what_is_new;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();
?>

<script language="JavaScript" src="../templates/jquery/jquery-1.3.2.min.js"></script>
<!--<script language='JavaScript' type='text/javascript' src='http://192.168.0.146:31001/eclass40/src/tool/powervoice/js/swfobject.js'></script>-->
<script language="JavaScript" type="text/javascript" src="../templates/swf_object/swfobject.js"></script>
<script language="Javascript" type="text/javascript" src="../templates/text_to_speech/text_to_speech.js"></script>
<script>
<?php
if ($plugin['power_voice'])
{
?>
	function listenPVoice(fileName)
	{
		newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet.php?from_intranet=1&location="+encodeURIComponent(fileName), 18);
	}
	
	/*$(function (){
		$(".TTS_PlayButton").each(function(){
			$(this).append(TTS_PlayButton($(this).attr("ttsTarget"), $(this).attr("ttsPlayer"), $(this).attr("ttsLang")));
		});
	});*/
<?php
}

## POWER SPEECH plugin check & Generate
if($ss_intranet_plugin['power_speech'] && check_powerspeech_is_expired()!=2){
?>
	var IMAGE_PREFIX = "<?=$image_path?>";
	$(document).ready(function(){
  		init_textToSpeach();      //in script.js
	});
<?php
} 
?>
</script>

<?php
	if ($lo->hasRightToView()) 
	{
		echo $lo->display2($ct);
		
		if ($UserID != '')
		{
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET ReadFlag = CONCAT(ifnull(ReadFlag,''),';$UserID;') WHERE AnnouncementID = '$AnnouncementID' AND (INSTR(ReadFlag,';$UserID;') = 0 OR ReadFlag IS NULL)";
			$lo->db_db_query($sql);
		}
	}
	else
	{
		echo "You don't have privileges to view.";
	}
?>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>