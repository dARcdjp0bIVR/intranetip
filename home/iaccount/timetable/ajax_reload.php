<?php
# using: ivan
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");

intranet_auth();
intranet_opendb();

$libtimetable_ui = new libtimetable_ui();

if ($_SESSION['UserType'] == USERTYPE_PARENT)
{
	$hasRight = true;
	$lfamily = new libfamily();
	$studentid = $_REQUEST['TargetUserID'];
    if ($studentid == "")
    {
        $studentid = $lfamily->returnFirstChild($UserID);
    }
    $children_list = $lfamily->returnChildrens($UserID);
    if (!in_array($studentid,$children_list))
    {
        $hasRight = false;
    }
    if ($studentid == "")
    {
        $hasRight = false;
    }
    if(!$hasRight)
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}
$Action = $_REQUEST['Action'];
if ($Action == 'Personal_Timetable')
{
	$TargetUserID = stripslashes($_REQUEST['TargetUserID']);
	$SubjectGroupIDList = trim(stripslashes($_REQUEST['SubjectGroupIDList']));
	if ($SubjectGroupIDList == '')
		$SubjectGroupIDArr = array(0);
	else
		$SubjectGroupIDArr = explode(',', $SubjectGroupIDList);
	
	echo $libtimetable_ui->Get_Personal_Timetable($TargetUserID, $TimetableID='', $YearTermID='', $IsViewMode=1, $Day='', $isSmallFont=0, $SubjectGroupIDArr);
}
else if ($Action == 'Filter_Table')
{
	$TargetUserID = stripslashes($_REQUEST['TargetUserID']);
	echo $libtimetable_ui->Get_View_Selection_Div_Table_For_Non_Admin_User($TargetUserID);
}
else if ($Action == 'Class_Timetable')
{
	$YearClassID = stripslashes($_REQUEST['YearClassID']);
	echo $libtimetable_ui->Get_Class_Timetable($YearClassID, $TimetableID='', $YearTermID='', $IsViewMode=1, $Day='', $isSmallFont=0);
}

intranet_closedb();
?>