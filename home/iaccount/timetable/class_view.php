<?php
# using: ivan
/* 
 * Change Log:
 * 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_auth();
intranet_opendb();


$CurrentPage = "PageTimetable";
$lprofile = new libprofile();
$linterface = new interface_html();
$libFCM_ui = new form_class_manage_ui();
$ltimetable_ui = new libtimetable_ui();


### Top Menu ###
$TAGS_OBJ = $lprofile->Get_Timetable_Top_Tabs_Menu('Class');

### Left Menu ###
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

### Navigation ###
$PAGE_NAVIGATION[] = array($Lang['iAccount']['Timetable']['ClassTimetable']);
$linterface->LAYOUT_START($xmsg);


### Get Class Selection
$ClassSelection = $libFCM_ui->Get_Class_Selection(Get_Current_Academic_Year_ID(), $YearID='', $ID_Name='YearClassID', $SelectedYearClassID='', $Onchange='js_Changed_Class_Selection(this.value);', $noFirst=0, $isMultiple=0, $isAll=0, $TeachingOnly=1);

### Display Option button
$thisOnchange = "js_Show_Hide_Display_Option_Div(); return false;";
$DisplayOptionBtn = '<a href="#" class="tablelink" onclick="'.$thisOnchange.'"> '.$Lang['SysMgr']['Timetable']['DisplayOption'].'</a>'."\n";


$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post">'."\n";
	$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	$x .= '<br style="clear:both;" />'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div style="float:left;">'.$ClassSelection.'</div>'."\n";
					$x .= '<div id="FilterButtonDiv" style="float:right;display:none;">'."\n";
						$x .= $DisplayOptionBtn;
					$x .= '</div>'."\n";
					$x .= '<div style="clear:both" />'."\n";
					$x .= '<div style="float:right;position:relative;left:-200px;z-index:99;">'."\n";
						$x .= '<table>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DisplayOptionDiv" class="selectbox_layer" style="width:200px;">'.$ltimetable_ui->Get_Display_Option_Div_Table().'</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr><td><div id="TimetableDiv"></div></td></tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";
echo $x;

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/timetable.js"></script>
<script language="javascript">
var jsCurYearClassID = '';

$(document).ready(function () {
	jsCurYearClassID = $('select#YearClassID option:nth-child(2)').val();
	$('select#YearClassID').val(jsCurYearClassID);
	js_Reload_Timetable();
});

function js_Reload_Timetable()
{
	if (jsCurYearClassID == '')
	{
		$('div#TimetableDiv').html('');
		$('div#FilterButtonDiv').hide();
	}
	else
	{
		$('div#TimetableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				Action: "Class_Timetable",
				YearClassID: jsCurYearClassID
			},
			function(ReturnData)
			{
				js_Update_Timetable_Display();
				$('div#FilterButtonDiv').show();
			}
		);
	}
}

function js_Changed_Class_Selection(jsYearClassID)
{
	jsCurYearClassID = jsYearClassID;
	js_Reload_Timetable();
}

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>