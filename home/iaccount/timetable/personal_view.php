<?php
# using: ivan
/* 
 * Change Log:
 * 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT."includes/libtimetable_ui.php");

intranet_auth();
intranet_opendb();


$CurrentPage = "PageTimetable";
$lprofile = new libprofile();
$linterface = new interface_html();
$ltimetable_ui = new libtimetable_ui();


### Top Menu ###
$TAGS_OBJ = $lprofile->Get_Timetable_Top_Tabs_Menu('Personal');

### Left Menu ###
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

### Navigation ###
$PAGE_NAVIGATION[] = array($Lang['iAccount']['Timetable']['IndividualTimetable']);
$linterface->LAYOUT_START($xmsg);



### Set parameter for viewing Timetable
# If Parent, show children selection and children timetable
# If Teacher / Student, show own timetable
$TargetUserID = '';
$ChildrenSelection = '';
if ($_SESSION['UserType'] == USERTYPE_STAFF)
{
	$TargetUserID = $_SESSION['UserID'];
}
else if ($_SESSION['UserType'] == USERTYPE_STUDENT)
{
	$TargetUserID = $_SESSION['UserID'];
}
else if ($_SESSION['UserType'] == USERTYPE_PARENT)
{
	$libuser = new libuser($_SESSION['UserID']);
	$ChildrenSelection = $libuser->getChildrenSelection('ChildrenID', 'ChildrenID', 'js_Changed_Children_Selection(this.value);');
}


### Display Option button
$thisOnchange = "js_Show_Hide_Display_Option_Div(); return false;";
$DisplayOptionBtn = '<a href="#" class="tablelink" onclick="'.$thisOnchange.'"> '.$Lang['SysMgr']['Timetable']['DisplayOption'].'</a>'."\n";

### Filter Button
$thisOnchange = "js_Show_Hide_Filter_Div(); return false;";
$FilterBtn = '<a href="#" class="tablelink" onclick="'.$thisOnchange.'"> '.$Lang['SysMgr']['Timetable']['DataFiltering'].'</a>'."\n";


$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post">'."\n";
	$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	$x .= '<br style="clear:both;" />'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div style="float:left;">'.$ChildrenSelection.'</div>'."\n";
					$x .= '<div id="FilterButtonDiv" style="float:right;display:none;">'."\n";
						$x .= $DisplayOptionBtn;
						$x .= ' | ';
						$x .= $FilterBtn;
					$x .= '</div>'."\n";
					$x .= '<div style="clear:both" />'."\n";
					$x .= '<div style="float:right;position:relative;left:-200px;z-index:99;">'."\n";
						$x .= '<table>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="DisplayOptionDiv" class="selectbox_layer" style="width:200px;">'.$ltimetable_ui->Get_Display_Option_Div_Table().'</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
					
					$x .= '<div style="float:right;position:relative;left:-450px;z-index:99;">'."\n";
						$x .= '<table>'."\n";
							$x .= '<tr>'."\n";
								$x .= '<td>'."\n";
									$x .= '<div id="FilterSelectionDiv" class="selectbox_layer" style="width:450px;overflow:auto;">'."\n";
									$x .= '</div>'."\n";
								$x .= '</td>'."\n";
							$x .= '</tr>'."\n";
						$x .= '</table>'."\n";
					$x .= '</div>'."\n";
					
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr><td><div id="TimetableDiv"></div></td></tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";
echo $x;

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/timetable.js"></script>
<script language="javascript">
var jsTargetUserID = '<?=$TargetUserID?>';

$(document).ready(function () {
	if (jsTargetUserID != '')
		js_Reload_Filtering();
});

function js_Reload_Timetable()
{
	if (jsTargetUserID == '')
	{
		$('div#TimetableDiv').html('');
		$('div#FilterButtonDiv').hide();
	}
	else
	{
		var jsSubjectGroupIDValue = $('select#SubjectGroupIDArr\\[\\]').val();
		if (jsSubjectGroupIDValue == null)
			jsSubjectGroupIDValue = '';
		else
			jsSubjectGroupIDValue = jsSubjectGroupIDValue.join(',');
		
		$('div#TimetableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				Action: "Personal_Timetable",
				TargetUserID: jsTargetUserID,
				SubjectGroupIDList: jsSubjectGroupIDValue
			},
			function(ReturnData)
			{
				js_Update_Timetable_Display();
				$('div#FilterButtonDiv').show();
			}
		);
	}
}

function js_Changed_Children_Selection(jsChildrenID)
{
	jsTargetUserID = jsChildrenID;
	js_Reload_Filtering();
}

function js_Reload_Filtering()
{
	$('div#FilterSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: "Filter_Table",
			TargetUserID: jsTargetUserID
		},
		function(ReturnData)
		{
			js_Reload_Timetable();
		}
	);
}

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>