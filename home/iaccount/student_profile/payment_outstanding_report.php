<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_opendb();
$linterface = new interface_html();
$lpayment = new libpayment();
$lexport = new libexporttext();


$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType != 1) 
{
	$hasRight = false;
	$studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$delimiter = ($g_encoding_unicode) ? "\t" : ",";
$valueQuote = ($g_encoding_unicode) ? "" : "\"";

$ExcludeEmpty = (isset($_REQUEST['ExcludeEmpty']) && $_REQUEST['ExcludeEmpty'] == 1) ? 1 : 0;
$page_breaker = "<P CLASS='breakhere'>";
$namefield = getNameFieldWithClassNumberByLang("c.");

# Get Account Balance
$student_balance = $lpayment->getBalanceInAssoc();

if($IncludeNonOverDue!=1){
	$cond = " AND b.EndDate < CURDATE() ";
}else{
	$cond = " AND b.StartDate <=CURDATE()";
}

$sql = "SELECT
                                $namefield,
                                b.Name,
                                a.Amount,
                                c.ClassName,
                                a.StudentID,
                                d.PPSAccountNo,
                                DATE_FORMAT(b.EndDate,'%Y-%m-%d')
                        FROM
                                PAYMENT_PAYMENT_ITEMSTUDENT AS a
                                LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM AS b ON a.ItemID = b.ItemID
                                LEFT OUTER JOIN INTRANET_USER AS c ON a.StudentID = c.UserID
                                LEFT OUTER JOIN PAYMENT_ACCOUNT as d ON c.UserID = d.StudentID
                        WHERE
                                a.RecordStatus <> 1
                                $cond ";
if($StudentID != "")
	$sql .= (!empty($_REQUEST['StudentID'])) ? " AND c.UserID = '".$_REQUEST['StudentID']."'" : "";
else
	$sql .= (!empty($_REQUEST['ClassName'])) ? " AND c.ClassName = '".$_REQUEST['ClassName']."'" : "";
	
$sql .= " ORDER BY c.ClassName, c.ClassNumber";

$data = $lpayment->returnArray($sql, 7);
for($i=0; $i<sizeof($data); $i++)
{
        list($sname, $itemname, $amount, $classname, $sid, $s_pps_no,$due_date) = $data[$i];
        $s_pps_no = trim($s_pps_no);
        $s_pps_no = (($s_pps_no=="")?"--":$s_pps_no);
        $TotalRecord[$classname] += 1;
        $TotalAmount[$sid] += $amount;
        $Payment[$classname][] = array($sname, $itemname, $amount, $sid, $s_pps_no,$due_date);
}

if (empty($_REQUEST['ClassName']))
{
        $sql = "SELECT ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
        $classes = $lpayment->returnVector($sql);
}
else
{
        $classes = array($_REQUEST['ClassName']);
}

$display = "";

for ($i=0; $i<sizeof($classes); $i++)
{
        $classname = $classes[$i];
        $x = "";
        $rows = array();
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
                $x .= "<p align='center'><b><font size=+1>".$classname."</font></b></p>\n";
                $x .= "<table width='90%' border='0' cellspacing='0' cellpadding='4' class='eSporttableborder' align='center'>\n";
                $x .= "<tr class='tabletext'>\n";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_UserStudentName</td>";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PaymentItem</td>";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PrintPage_Outstanding_DueDate</td>";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PrintPage_Outstanding_Total</td>";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_PrintPage_Outstanding_LeftAmount</td>";
                        $x .= "<td class='eSporttdborder eSportprinttabletitle'>$i_Payment_Field_PPSAccountNo</td>";
                $x .= "</tr>\n";

                $rows[] = array($classname);
                $rows[] = array($i_UserStudentName, $i_Payment_Field_PaymentItem, $i_Payment_PrintPage_Outstanding_DueDate, $i_Payment_PrintPage_Outstanding_Total, $i_Payment_PrintPage_Outstanding_AccountBalance, $i_Payment_PrintPage_Outstanding_LeftAmount, $i_Payment_Field_PPSAccountNo);
        }

        if ($TotalRecord[$classname] == 0 && $ExcludeEmpty == 0)
        {
                $x .= "<tr class='tabletext'><td colspan=7 class='eSporttdborder eSportprinttext' align='center'>$i_no_record_exists_msg</td></tr>\n";
				$rows[] = array($i_no_record_exists_msg);
        }
        else
        {
                $payment_record = $Payment[$classname];
                $curr_student = "";
                for($j=0; $j<sizeof($payment_record); $j++)
                {
                        list($StudentName, $ItemName, $Amount, $StudentID, $s_pps_no,$due_date) = $payment_record[$j];

                        $this_row = array();
                        $x .= "<tr>\n";
                                $x .= "<td class='eSporttdborder eSportprinttext'>";
                                if($curr_student == $StudentID) {
                                        $x.="&nbsp;</td>";
                                        $this_row[] = "";
                                }
                                else{
                                         $x.=$StudentName."</td>";
                                         $this_row[] = $StudentName;
                                }
                                       
                                $x .= "<td class='eSporttdborder eSportprinttext'>".$ItemName."($".number_format($Amount, 1).")</td>";
                                $x .= "<td nowrap class='eSporttdborder eSportprinttext'>".$due_date."</td>";
                                
                                $this_row[] = $ItemName."(".round($Amount, 1).")";
                                $this_row[] = $due_date;
                                
                                if ($curr_student == $StudentID)
                                {
                                    $x .= "<td class='eSporttdborder eSportprinttext' colspan='4'>&nbsp;</td>";
                                    $this_row[] = "";
                                }
                                else
                                {
                                    $t_balance = $student_balance[$StudentID] + 0;
                                    $t_diff = $TotalAmount[$StudentID] - $t_balance;
                                    if (abs($t_diff) < 0.001)    # Smaller than 0.1 cent
                                    {
                                        $t_diff = 0;
                                    }
                                    $str_diff = ($t_diff > 0)? "<font color=red>-$".number_format($t_diff,1)."</font>":"--";
                                    $csv_str_diff = ($t_diff > 0)? "-".round($t_diff,1):"--";

                                    $x .= "<td class='eSporttdborder eSportprinttext'>$".number_format($TotalAmount[$StudentID], 1)."</td>";
                                    $x .= "<td class='eSporttdborder eSportprinttext'>$".number_format($t_balance, 1)."</td>";
                                    $x .= "<td class='eSporttdborder eSportprinttext'>$str_diff</td>";
                                    $x .= "<td class='eSporttdborder eSportprinttext'>$s_pps_no</td>";

                                    $this_row[] = round($TotalAmount[$StudentID], 1);
                                    $this_row[] = round($t_balance, 1);
                                    $this_row[] = $csv_str_diff;
                                    $this_row[] = $s_pps_no;
                                }

                        $x .= "</tr>\n";
                        $rows[] = $this_row;
                        $curr_student = $StudentID;
                }
        }
        if ($TotalRecord[$classname] > 0 || $ExcludeEmpty == 0)
        {
                $x .= "</table>\n";
                $x .= $page_breaker;
        }
}
if($format!=1) {## Print Page
	$display .= $x;
?>
	<? include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php"); ?>
	<link href="../../templates/2007a/css/content.css" rel="stylesheet" type="text/css" />
	<STYLE TYPE="text/css">
	     P.breakhere {page-break-before: always}
	</STYLE>
	<?php
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
	
	$print_btn_table ="<table width=\"90%\" align=\"center\" class=\"print_hide\" border=\"0\"><tr><td align=\"right\">".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td></tr></table><BR>";
	echo $print_btn_table;
        echo $display;
} else { ## Export
	
	$exportColumn = array();
	for($i=1;$i<=7;$i++)
	{
		$exportColumn[] = "";
	}

	$filename = "outstanding".date('Y-m-d').".csv";
	$export_content .= $lexport->GET_EXPORT_TXT($rows,$exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
}

intranet_closedb();
?>
