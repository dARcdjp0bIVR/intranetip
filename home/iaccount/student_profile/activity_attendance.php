<?php
# Using By:

############## Change Log [start] ####
#	Date:	2015-09-24	Omas
#			Copy from enrolment "eAdmin Enrol Summary Report"
#
############# Change Log [End] #######
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$libenroll = new libclubsenrol('', $sel_category);
$libenroll_ui = new libclubsenrol_ui();
$libfcm = new form_class_manage();
$lprofile = new libprofile();

$withinEnrolStage = $libenroll->WITHIN_ENROLMENT_STAGE("club");
$withinEnrolStageText = ($withinEnrolStage)? '1' : '0';
$withinProcessStage = $libenroll->WITHIN_ENROLMENT_PROCESS_STAGE();



####################################################
############# Check Access Right [End] #############
####################################################
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

	$lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    $lclass = new libclass();
    $classname = $class[0][1];
    if ($classname == "")
    {
        $hasRight = false;
    }
    else
    {
	    # class selection
	    $classid = $classid ? $classid : $class[0][0];
	    //$class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    $class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    
	    # re-get classname
	    $classname = $lclass->getClassName($classid);
	    
        $lu_student = new libuser($studentid);
        if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
        {
            $studentid = "";
        }
        if ($studentid == "")
        {
            $namelist = $lclass->returnStudentListByClass($classname);
            $studentid = $namelist[0][0];
            if ($studentid == "") $studentid = $UserID;
        }
        $student_list = $lclass->getStudentNameListWClassNumberByClassName($classname);
		$student_selection = getSelectByArray($student_list, "name=TargetStudent",$TargetStudent,1,0);
    }


####################################################
############# Page Proporties [Start] ##############
####################################################
$CurrentPage = "PageEnrolmentAttendanceView";
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['iAccount']['EnrolmentAttendanceOverview'], "", 1);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
####################################################
############## Page Proporties [End] ###############
####################################################

####################################################
############## Warning Msg [Start] #################
####################################################
$warningMsgAry = array();
$warningMsgAry[] = $eEnrollment['front']['time_conflict_warning'];
if ($warn_no_more_enrolment != '') {
	$warningMsgAry[] = $warn_no_more_enrolment;
}

$h_warningMsg = '';
if ($libenroll->oneEnrol == 1) {
	$h_warningMsg .= $linterface->Get_Warning_Message_Box('', $warningMsgAry);
	$h_warningMsg .= '<br style="clear:both;" />';
}
####################################################
############### Warning Msg [End] ##################
####################################################

		  	//get the class name
	       	if(empty($_POST['classid'])){
	       		$targetClassID = $class[0][0];
	       	}
	       	else{
	       		$targetClassID = $_POST['classid'];
	       	}
	       	
		  	$ObjClass = new year_class($targetClassID);
		   	$ObjFcm = new form_class_manage();
		   	$classSelection = $ObjClass->Get_Class_Name();
		       	
		    $classSelection = $lclass->getSelectClassID(" id=\"targetClassID\" name=\"targetClassID[]\" multiple size=\"10\"", $targetClassID,2, Get_Current_Academic_Year_ID(), "", "", 1);
		    $classSelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetClassID',1)");
			$classSelection .= $linterface->spacer();  	 
			$classSelection .= $linterface->MultiSelectionRemark();  
		       	
		    $targetClass = $ObjClass->Get_Class_Name(); 
		    //$hiddenFieldDisplay = "<input type='hidden' id='targetClassID' name='targetClassID' value='".$targetClassID."' />";
	       	
	       	
	       	$tempClub = $libenroll->Get_All_Club_Info();
	       	$ClubInfoAssoc = BuildMultiKeyAssoc($tempClub,"EnrolGroupID");
	       	$clubInfoArray = array();
	       	$p = 0;
	       	for($i=0; $i<sizeof($tempClub); $i++) {
		     	$clubInfoArray[$p][] = $tempClub[$i]['EnrolGroupID']; 	
		     	$clubInfoArray[$p][] = $intranet_session_language=="en" ? $tempClub[$i]['TitleWithSemester'] : $tempClub[$i]['TitleChineseWithSemester'];
		     	$p++;
	       	}
	       	
	       	$clubSelection = getSelectByArray($clubInfoArray," id=\"targetClub\" name=\"targetClub[]\" multiple size=10 ",$targetClub,0,1,"- ".$i_general_please_select." -");
	       	$clubSelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetClub',1)");
	       	$clubSelection .= $linterface->spacer();  	 
			$clubSelection .= $linterface->MultiSelectionRemark(); 
	       	
	       	# Date selection
	       	$FromDateField = $linterface->GET_DATE_PICKER("textFromDate", $textFromDate);
	       	$ToDateField = $linterface->GET_DATE_PICKER("textToDate", $textToDate);
	       	
	       	$DateSelection = "";
	       	$DateSelection .= $i_From." ".$FromDateField." ".$i_To." ".$ToDateField;
	       	
	       	if ($usePeriod)
	       	{
				$usePeriodChecked = "checked";
				$periodDisplay = "block";
	       	}
	       	else
	       	{
				$usePeriodChecked = "";
				$periodDisplay = "none";
	       	}
	       	
			$periodRowHTML = "";
			$periodRowHTML .= "<tr id=\"periodTR\" {$hidePeriodTR}>";
				$periodRowHTML .= "<td class=\"field_title\">";
					$periodRowHTML .= $eEnrollment['Period'];
				$periodRowHTML .= "</td>";
				$periodRowHTML .= "<td valign=\"top\" class=\"tabletext\">";
					$periodRowHTML .= "<input type=\"checkbox\" id=\"usePeriod\" name=\"usePeriod\" onclick=\"jsShowHideDiv('usePeriod','date_selection')\" ".$usePeriodChecked."><label for='usePeriod'>";
					$periodRowHTML .= $eEnrollment['Specify'];
					$periodRowHTML .= "<label><div id=\"date_selection\" style=\"display:".$periodDisplay."\">";
						$periodRowHTML .= $DateSelection;
					$periodRowHTML .= "</div>";
				$periodRowHTML .= "</td>";
			$periodRowHTML .= "</tr>";
			
			
	       	//show all student if a target class is set
	       	$showTarget = "class";
	       	if(($showTarget=="class" && isset($targetClassID) && $targetClassID!=""))
	       	{
		       
		       	if($showTarget=="class" && isset($targetClassID) && $targetClassID!="") {
			       	
					$studentNameArr = array();
					$ClassStudentAssoc = array();
					foreach((array)$targetClassID as $thistargetClassID)
					{
						$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
			        	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
			        	$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
			        	$GroupStudentAssoc[$thistargetClassID] = Get_Array_By_Key($YearClassObj->ClassStudentList, 'UserID');
			        }

					$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
					
					if (!isset($useCategory)){
						unset($targetCategory);
					}
					
			     	$studentClubArr = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIDArr,$targetCategory);
			     	$studentActivityArr = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($studentIDArr,'',$targetCategory);
			     	$activityIdArr = array_unique(Get_Array_By_Key($studentActivityArr,'EnrolEventID'));
					$clubIdArr = array_unique(Get_Array_By_Key($studentClubArr,'EnrolGroupID'));
			     	//reassign array key from 0 to n
			     	sort($activityIdArr);
			     	sort($clubIdArr);
					$clubAttendanceArr = $libenroll->Get_Student_Club_Attendance_Info($studentIDArr,$clubIdArr,$DoNotShowRate=1);
					//debug_pr($clubAttendanceArr);
					$activityAttendanceArr = $libenroll->Get_Student_Activity_Attendance_Info($studentIDArr,$activityIdArr,$DoNotShowRate=1);
		     	} 

			     	if ($usePeriod)
			     	{
				     	$fromDateTime = $textFromDate." 00:00:00";
				     	$toDateTime = $textToDate." 23:59:59";
				     	$clubWithinPeriodArr = $libenroll->Get_Club_Within_Period($fromDateTime, $toDateTime);
				     	$activityWithinPeriodArr = $libenroll->Get_Event_Within_Period($fromDateTime, $toDateTime);
			     	}
			     	
			     	if(isset($Semester))
			     	{
				     	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', array($Semester));
				     	$term_group_list = array();
				     	if(!empty($term_group_list_temp))
				     	{
					     	foreach($term_group_list_temp as $k=>$d)
					     		$term_group_list[] = $d['EnrolGroupID'];
				     	}
			     	}
			     	
		     		# build associative array
			     	$studentInfoArr = array();
			     	foreach ($studentNameArr as $key => $studentDataArr)
			     	{
			     		$thisUserID = $studentDataArr['UserID'];
						$studentInfoArr[$thisUserID]['StudentName'] = $studentDataArr['StudentName'];
						$studentInfoArr[$thisUserID]['ClassName'] = $studentDataArr['ClassName'];
						$studentInfoArr[$thisUserID]['ClassNumber'] = $studentDataArr['ClassNumber'];
			     	}
			     	foreach ($studentClubArr as $key => $clubDataArr)
			     	{
						list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
						$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
						$thisTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
						
						# filter the group if the group do not have gathering within the selected period
						if ($usePeriod && !in_array($thisEnrolGroupID, $clubWithinPeriodArr))
							continue;
						
						if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
						{
							continue;
						}
						
						if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
						{
							$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
						}
						
						$studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle);
			     	}
			     	foreach ($studentActivityArr as $key => $activityDataArr)
			     	{
						list($thisEnrolEventID, $thisEventTitle, $thisUserID, $roleTitle) = $activityDataArr;
						
						$thisEventTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
						
						# filter the group if the group do not have gathering within the selected period
						if ($usePeriod && !in_array($thisEnrolEventID, $activityWithinPeriodArr))
							continue;
						
						if (!isset($studentInfoArr[$thisUserID]['ActivityInfoArr']))
						{
							$studentInfoArr[$thisUserID]['ActivityInfoArr'] = array();
						}
						
						$studentInfoArr[$thisUserID]['ActivityInfoArr'][] = array($thisEnrolEventID, $thisEventTitle);
			     	}
			     	# end of building associative array

		     	//construct table title
		     	foreach((array)$GroupStudentAssoc as $ClassClubID => $StudentIDArr)
		     	{
		     		if($showTarget=="class")
		     		{
		     			$ObjClass = new year_class($ClassClubID);
	       				$GroupName = $ObjClass->Get_Class_Name();
		     		}
		     		else
		     		{
		     			$GroupName = $ClubInfoAssoc[$ClassClubID]['TitleWithSemester'];
		     		}
		     		$StudentIDArr = array_values((array)$StudentIDArr);
		     		
		     		$display .= "<br>".$linterface->GET_NAVIGATION2($GroupName);
		     	 	$display .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
					$display .= "<thead><tr>";
					$display .= "
								<th>".$i_UserClassName."</td>
								<th>".$i_UserClassNumber."</td>
								<th>".$i_UserStudentName."</td>
								<th width='30%'>".$eEnrollmentMenu['club']."</td>
								<th width='30%'>".$eEnrollmentMenu['activity']."</td>
								";
					if($special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student'])
					{
						$display .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>"; 
					}         
				    $display .= "</tr></thead>";
				    
				    $total_col = 5;
				   	$StudentSize = sizeof($StudentIDArr);
				   	
				   	//construct table content
				   	$display .= ($StudentSize==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
		        	    
				    for ($i=0; $i<$StudentSize; $i++)
				    {
					    $thisStudentID = $StudentIDArr[$i];
					    $thisStudentName = $studentInfoArr[$thisStudentID]['StudentName'];
					    $thisClassName = $studentInfoArr[$thisStudentID]['ClassName'];
					    $thisClassNumber = $studentInfoArr[$thisStudentID]['ClassNumber'];
					    $thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
					    $thisActivityArr = $studentInfoArr[$thisStudentID]['ActivityInfoArr'];
					    
					    # build name link
					    $thisNameLink = "";
					    $thisNameLink .= "$thisStudentName";
					    //$thisNameLink .= "<a class='tablelink' href='javascript:goStudentDetails(".$thisStudentID.")' >".$thisStudentName."</a>";
					    
					    # build clubs link
					    $sizeClubArr = count($thisClubArr);
					    $thisClubLink = "";
					    for ($j=0; $j<$sizeClubArr; $j++)
					    {
						    list($thisEnrolGroupID, $thisTitle) = $thisClubArr[$j];
						    
					    	$thisAttendance = $clubAttendanceArr[$thisStudentID][$thisEnrolGroupID]['AttendancePercentageRounded'];
					    	if(!empty($thisAttendance)){
					    		$displayAttendance = ' - '.$thisAttendance.'%';
					    	}
					    	else{
					    		$displayAttendance = '';
					    	}
						    $thisClubLink .= "<a class='tablelink' href=\"javascript: jsGoEventDetails('".$thisStudentID."', '".$thisEnrolGroupID."', 'club')\" >\n";
						    $thisClubLink .= $thisTitle.$displayAttendance."\n";
						    $thisClubLink .= "</a>\n";
						    
						    if ($j < $sizeClubArr-1 )
						    {
							    $thisClubLink .= "<br />\n";
						    }
						    
					    }
					    # build activities link
					    $sizeActivityArr = count($thisActivityArr);
					    $thisActivityLink = "";
					    for ($j=0; $j<$sizeActivityArr; $j++)
					    {
						    list($thisEnrolEventID, $thisTitle) = $thisActivityArr[$j];
						    
						    $thisAttendance = $activityAttendanceArr[$thisStudentID][$thisEnrolEventID]['AttendancePercentageRounded'];
					    	if(!empty($thisAttendance)){
					    		$displayAttendance = ' - '.$thisAttendance.'%';
					    	}
					    	else{
					    		$displayAttendance = '';	
					    	}
						    $thisActivityLink .= "<a class='tablelink' href=\"javascript: jsGoEventDetails('".$thisStudentID."', '".$thisEnrolEventID."', 'activity')\" >\n";
						    $thisActivityLink .= $thisTitle.$displayAttendance."\n";
						    $thisActivityLink .= "</a>\n";
						    
						    if ($j < $sizeActivityArr-1 )
						    {
							    $thisActivityLink .= "<br />\n";
						    }
					    }			    
				 				
						$display .= '<tr>';
						
						$display .= '
									<td class="tabletext" valign="top">'.$thisClassName.'</td>
									<td class="tabletext" valign="top">'.$thisClassNumber.'</td>
									<td valign="top">'.$thisNameLink.'</td>
					          	  	<td class="tabletext" align="left" valign="top">'.$thisClubLink.'</td>
					          	  	<td class="tabletext" align="left" valign="top">'.$thisActivityLink.'</td>
					          	  	';
					    if($special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student'])
					    {
					    	$display .= '<td class="tabletext" align="left" valign="top"><input type="checkbox" name="selected_student[]" value="'. $thisStudentID.'"></td>';
				    	}
				    	$display .= '</tr>';
					}
				    $display .= '</table>';
		     	}
	       	}
?>
<script language="javascript">
function jsGoEventDetails(studentID, eventID, actionType){
	if (actionType == "club")
	{
		document.getElementById("studentID").value = studentID;
		document.getElementById("EnrolGroupID").value = eventID;
		document.form1.action = "group_student_comment.php";
	}
	else
	{
		document.getElementById("studentID").value = studentID;
		document.getElementById("EnrolEventID").value = eventID;
		document.form1.action = "event_student_comment.php";
	}
	document.form1.target = "_self";
	document.form1.method = "post";
	document.form1.submit();
}

function goStudentDetails(studentID) {
	document.getElementById("studentID").value = studentID;
	
	document.form1.action = "index.php";
	document.form1.target = "_self";
	document.form1.method = "post";
	document.form1.submit();
}
</script>
<br />
<form id="form1" name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
        	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_class?></span></td>
					<td class="tabletext"><?=$class_selection?></td>
				</tr>
				</table>
			</td>
		</tr>
        </table>
	</td>
</tr>
<tr>
	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");?>
	</td>
</tr>
<input type="hidden" id="studentID" name="studentID"/>
<input type="hidden" id="EnrolEventID" name="EnrolEventID" />
<input type="hidden" id="EnrolGroupID" name="EnrolGroupID" />
</form>
<tr>
	<td>
		<?=$display?>
	</td>
</tr>	
</table>
<br />
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>