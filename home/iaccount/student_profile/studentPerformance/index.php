<?php
$NoLangWordings = true;

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();
$li_teaching = new libteaching();
		    	
$isClassTeacher = $li_teaching->Is_Class_Teacher($_SESSION['UserID']);
$isSubjectTeacher = $li_teaching->Is_Subject_Teacher($_SESSION['UserID']);
$isTeacherAppAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"];

intranet_closedb();

if ($isClassTeacher || $isTeacherAppAdmin) {
	header("Location: class_view.php");
}
else if ($isSubjectTeacher) {
	header("Location: subject_view.php");
}
?>