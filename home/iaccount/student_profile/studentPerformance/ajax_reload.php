<?php
// using:

/******************************************************
 * Modification log:
 *  20190808 Bill   [2019-0806-1405-49206]
 *      - fixed subject group selection
 *      - selection id + group list for admin
 * ****************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$actionType = standardizeFormPostValue($_POST['actionType']);
if ($actionType == 'reloadSubjectGroupSelection')
{
	//$selectionId = standardizeFormPostValue($_POST['selectionId']);
    $selectionId = standardizeFormPostValue($_POST['selectionIdName']);
	$selectionName = standardizeFormPostValue($_POST['selectionName']);
	$startDate = standardizeFormPostValue($_POST['startDate']);
	$endDate = standardizeFormPostValue($_POST['endDate']);
    $teachingOnly = standardizeFormPostValue($_POST['teachingOnly']);
	
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
	$fcm = new form_class_manage();
	$lsubject = new subject();
	
	// Get Terms
	$termIdAry = Get_Array_By_Key($fcm->Get_Term_By_Date_Range($startDate, $endDate), 'YearTermID');
	
	// Get Subject Groups
    $teacherId = ($teachingOnly? $_SESSION['UserID'] : '');
	$subjectGroupAssoAry = BuildMultiKeyAssoc($lsubject->Get_Subject_Group_List($termIdAry, $ClassLevelID='', $SubjectGroupID='', $teacherId, $returnAsso=0), 'SubjectGroupID', Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN'), $SingleValue=1);

	$selectionAttr = ' id="'.$selectionId.'" name="'.$selectionName.'" multiple size="10" ';
	echo getSelectByAssoArray($subjectGroupAssoAry, $selectionAttr, '', $isAll=0, $noFirst=1);
}

intranet_closedb();
?>