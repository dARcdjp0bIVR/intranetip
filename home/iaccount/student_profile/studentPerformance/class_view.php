<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_stuPerformance.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lprofile = new libprofile();
$stuPerformance = new libeClassApp_stuPerformance();
$linterface = new interface_html();


$CurrentPage = "PageStudentPerformanceView";
$TAGS_OBJ = $stuPerformance->getReportTopTabMenuAry('class');
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$isTeacherAppAdmin = $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"];


### edit table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// Report Type
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['iAccount']['StudentPerformance_viewType'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $linterface->Get_Radio_Button('viewTypeRadio_raw', 'viewType', 'raw', $isChecked=1, $Class="", $Lang['iAccount']['StudentPerformance_rawData'], '', $isDisabled=0);
			$x .= '&nbsp;';
			$x .= $linterface->Get_Radio_Button('viewTypeRadio_statistics', 'viewType', 'statistics', $isChecked=0, $Class="", $Lang['iAccount']['StudentPerformance_statistics'], '', $isDisabled=0);
			if ($sys_custom['eClassApp']['studentPerformance_SubjectTeacherSummaryReport']) {
			    $x .= '&nbsp;';
			    $x .= $linterface->Get_Radio_Button('viewTypeRadio_STSummary', 'viewType', 'STSummary', $isChecked=0, $Class="", $Lang['iAccount']['StudentPerformance_STSummary'], '', $isDisabled=0);
			}
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Date range
	$curTermId = getCurrentSemesterID();
	$termObj = new academic_year_term($curTermId, $GetAcademicYearInfo=false);
	$startDate = date('Y-m-d', strtotime($termObj->TermStart));
	$endDate = date('Y-m-d', strtotime($termObj->TermEnd));
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $Lang['General']['From'].' '.$linterface->GET_DATE_PICKER('startDate', $startDate).' '.$Lang['General']['To'].' '.$linterface->GET_DATE_PICKER('endDate', $endDate);
			$x .= $linterface->Get_Form_Warning_Msg('dateRangeWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Class
	$TeachingOnly = ($isTeacherAppAdmin)? 0 : 1;
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['General']['Class'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $lclass->getSelectClassID('id="classSel" name="classId[]" multiple=multiple style="height: 150px;"', $selected="", $DisplaySelect=2, $AcademicYearID='',$optionFirst=0, $DisplayClassIDArr='', $TeachingOnly);
			$x .= "<div style='display:inline-block; position: relative; bottom: 10px; left: 5px;'>";
			 $x .= $linterface->Get_Action_Btn($Lang['Btn']['SelectAll'], "button", "selectAllClass()");
			 $x .= "</div>";
			$x .= '<div><span class="tabletextremark">' . $Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'] . '</span></div>';
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTbl'] = $x;


### action buttons
$htmlAry['printBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Print'], "button", "goSubmit('print')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['exportBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Export'], "button", "goSubmit('export')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


### hidden fields
$htmlAry['outputModeHidden'] = $linterface->GET_HIDDEN_INPUT('outputMode', 'outputMode', '');
?>
<script type="text/javascript">
$(document).ready( function() {
	selectAllClass();
});

function goSubmit(parOutputMode) {
	var canSubmit = true;
	$('div.warnMsgDiv').hide();
	
	var startDate = $('input#startDate').val();
	var endDate = $('input#endDate').val();
	if (compareDate(startDate, endDate) == 1) {
		canSubmit = false;
		$('div#dateRangeWarnDiv').show();
	}
	
	if (canSubmit) {
		$('input#outputMode').val(parOutputMode);
		$('form#form1').submit();
	}
}

function selectAllClass(){
	$('#classSel option').attr('selected', 'selected');
}

</script>
<form name="form1" id="form1" method="POST" target="_blank" action="class_view_report.php">
	<div class="table_board">
		<?=$htmlAry['formTbl']?>
		<br style="clear:both;" />
		
		<?=$linterface->MandatoryField()?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			
			<?=$htmlAry['printBtn']?>
			<?=$htmlAry['exportBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<?=$htmlAry['outputModeHidden']?>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>