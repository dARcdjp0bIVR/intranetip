<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libmerit.php");

intranet_auth();
intranet_opendb();

if ($StudentID=="")
{
     include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['iAccount']['InvalidStudentData']);
	exit;
}


$lu_viewer = new libuser($UserID);
if ($lu_viewer->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    
    # check the student is in user's class 
    $lu_student = new libuser($StudentID);
    $pass = false;
    for($i=0;$i<sizeof($class);$i++)
    {
	    if($class[$i]['ClassName'] == $lu_student->ClassName)	$pass = true;
    }	
    
    if ($lu_student->UserID=="" || !$pass)
    {
         include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['iAccount']['InvalidStudentData']);
		exit;
    }
}
else if ($lu_viewer->RecordType == 3)  # Parent
{
     $lfamily = new libfamily();
     $children_list = $lfamily->returnChildrens($UserID);
     if (!in_array($StudentID,$children_list))
     {
         include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['iAccount']['InvalidStudentData']);
		exit;
     }
}
else # Student
{
       $StudentID = $UserID;
}

$lmerit = new libmerit();
$lu = new libuser($StudentID);

$now = time();
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));
if ($year == "")
{
    $x = $lmerit->displayStudentRecord($StudentID,$yearstart,$yearend);
}
else
{
    $x = $lmerit->displayDetailedRecordByYear($StudentID,$year, $sys_custom['display_warning_iaccount'], $yearid);
}

$MODULE_OBJ['title'] = $i_Merit_DetailedMeritRecord;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>

<table width="90%" border="0" cellpadding="5" cellspacing="0">
<tr>
          <td><table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
<tr>
<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
<td width="80%" valign="top" class="tabletext"><?=$lu->UserNameClassNumber()?></td>
</tr>
</table></td>
</tr>
<tr>
	<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
	<td><br /><?=$x?></td>
</td>
</table>

	<table width="95%" border="0" cellpadding="5" cellspacing="0">
                        <tr>
                        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                        </tr>
                        <tr>
        			<td align="center">
                                        <?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
        			</td>
        		</tr>
        </table>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>