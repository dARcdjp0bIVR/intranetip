<?php
## Modifying By: 
##### Change Log [Start] #####
#	Date	: 	2016-05-04	Carlos
#				Change hidden element "action" as "targetAction", to avoid naming conflict with form['action'].
#
#	Date	:	2015-04-23	Omas
# 				Create this file
#
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lprofile = new libprofile();
$linterface = new interface_html();
$lpayment = new libpayment();
$lclass = new libclass();
$lexport = new libexporttext();
$CurrentPage = "PageAccountOverView";
$TAGS_OBJ[] = array($Lang['iAccount']['AccountOverview']);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();



$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    $lclass = new libclass();
    $classname = $class[0][1];
    if ($classname == "")
    {
        $hasRight = false;
    }
    else
    {
	    # class selection
	    $classid = $classid ? $classid : $class[0][0];
	    //$class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    $class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    
	    # re-get classname
	    $classname = $lclass->getClassName($classid);
	    
        $lu_student = new libuser($studentid);
        if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
        {
            $studentid = "";
        }
        if ($studentid == "")
        {
            $namelist = $lclass->returnStudentListByClass($classname);
            $studentid = $namelist[0][0];
            if ($studentid == "") $studentid = $UserID;
        }
        $student_list = $lclass->getStudentNameListWClassNumberByClassName($classname);
		$student_selection = getSelectByArray($student_list, "name=TargetStudent",$TargetStudent,1,0);
    }
}
else # Student & Parent
{
	$hasRight = false;
	$studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
else{
	
	$ClassName = ($ClassName == '')? $classid: $ClassName;
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$currentSemesterID = getCurrentSemesterID();
	$AcademicYearStartDateTimestamp = strtotime(getStartDateOfAcademicYear($AcademicYearID,$currentSemesterID));
	$AcademicYearEndDateTimestamp = strtotime(getEndDateOfAcademicYear($AcademicYearID,$currentSemesterID));
	$FromDate = date('Y-m-d',$AcademicYearStartDateTimestamp);
	$ToDate = date('Y-m-d',$AcademicYearEndDateTimestamp);
	
	if($format == 1){//csv
		
		$x = $lpayment->iAccountAccountOverview($ClassName, $FromDate, $ToDate,1);
		exit;
	}
	else if($format == 2){

		$x = $lpayment->iAccountAccountOverview($ClassName, $FromDate, $ToDate);

	}
	else{
		$linterface->LAYOUT_START();

		$x = $lpayment->iAccountAccountOverview($ClassName, $FromDate, $ToDate);
	}
	
}

$contentTools= "<p><a class=\"contenttool\" href=\"javascript:goPrint();\"><img src=\"$PATH_WRT_ROOT/images/2009a/icon_print.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$i_PrinterFriendlyPage."</a>&nbsp;&nbsp;&nbsp;<a class=\"contenttool\" href=\"payment_info.php?format=1&ClassName=$classid\"><img src=\"$PATH_WRT_ROOT/images/2009a/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$button_export."</a></p>";
?>
<script>
function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
	$('form#form1').attr('target', '').attr('action', '');
}
</script>
<form id="form1" name="form1" action="" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
        	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_class?></span></td>
					<td class="tabletext"><?=$class_selection?></td>
				</tr>
				</table>
			</td>
		</tr>
        </table>
	</td>
</tr>
<tr>
	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");?>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<?=$contentTools?>
					<?=$x?>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="className" value="<?=$ClassName?>" />
<input type="hidden" name="fromDate" value="<?=$FromDate?>" />
<input type="hidden" name="toDate" value="<?=$ToDate?>" />
<input type="hidden" name="targetAction" value="AccountOverview" />
</form>
<?
intranet_closedb();
if($format==0){
	$linterface->LAYOUT_STOP();
}
?>