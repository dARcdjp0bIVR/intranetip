<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libmerit.php");
include_once($PATH_WRT_ROOT."includes/libactivity.php");
include_once($PATH_WRT_ROOT."includes/libservice.php");
include_once($PATH_WRT_ROOT."includes/libaward.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lprofile = new libprofile();
$linterface = new interface_html();
$lpayment = new libpayment();
$lclass = new libclass();
$CurrentPage = "PageTransactionLog";

$TAGS_OBJ[] = array($i_Payment_DetailTransactionLog);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$lu = new libuser($UserID);

$hasRight = true;
if ($lu->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    $lclass = new libclass();
    $classname = $class[0][1];
    if ($classname == "")
    {
        $hasRight = false;
    }
    else
    {
	    # class selection
	    $classid = $classid ? $classid : $class[0][0];
	    $class_selection = getSelectByArray($class,"name='classid' onChange='change_class()'",$classid, 0, 1);
	    
	    # re-get classname
	    $classname = $lclass->getClassName($classid);
	    
        $lu_student = new libuser($studentid);
        if ($lu_student->UserID=="" || $lu_student->ClassName != $classname)
        {
            $studentid = "";
        }
        if ($studentid == "")
        {
            $namelist = $lclass->returnStudentListByClass($classname);
            $studentid = $namelist[0][0];
            if ($studentid == "") $studentid = $UserID;
        }
        $student_list = $lclass->getStudentNameListWClassNumberByClassName($classname);
		$student_selection = getSelectByArray($student_list, "name=TargetStudent",$TargetStudent,1,0);
    }
}
else # Student & Parent
{
	$hasRight = false;
	$studentid = $UserID;
}

if(!$hasRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if($hasRight)
{
$student_balance = $lpayment->getBalanceInAssoc();

if($TargetStudent=="")
{
	$cond = " a.ClassName = '$classname' ";
}
else
{
	$cond = " a.UserID = '$TargetStudent' ";
}


$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT 
				a.UserID, $namefield, IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), CONCAT('$',IF(b.Balance>=0,FORMAT(b.Balance,1),'0.0') ), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
		FROM 
				INTRANET_USER AS a LEFT OUTER JOIN 
				PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
		WHERE
				$cond
				AND RecordType = 2
				AND RecordStatus = 1
		ORDER BY
				a.ClassName, a.ClassNumber, a.EnglishName";
				
$payment_account_result = $lpayment->returnArray($sql, 5);
	
if(sizeof($payment_account_result)>0)
{	
	$table_content .= "<p><a class=\"contenttool\" href=\"javascript:newWindow('payment_transaction_log_web.php?ClassName=$classname&StudentID=$TargetStudent',10)\"><img src=\"$PATH_WRT_ROOT/images/2009a/icon_print.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$i_PrinterFriendlyPage."</a>&nbsp;&nbsp;&nbsp;<a class=\"contenttool\" href=\"payment_transaction_log_csv.php?ClassName=$classname&StudentID=$TargetStudent\"><img src=\"$PATH_WRT_ROOT/images/2009a/icon_export.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\">".$button_export."</a></p>";
	
	for($i=0; $i<sizeof($payment_account_result); $i++)
	{
		list($u_id, $name, $pps, $bal, $last_modified) = $payment_account_result[$i];

		$sql2  = "SELECT
		               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
		               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
		               CASE a.TransactionType
		                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
		                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
		                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
		                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
		                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
		                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
		                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
		                    WHEN 8 THEN '$i_Payment_PPS_Charge'
		                    ELSE '$i_Payment_TransactionType_Other' END,
		               IF(a.TransactionType IN (1,5,6),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
		               IF(a.TransactionType IN (2,3,4,7,8),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
		               a.Details, CONCAT('$',IF(a.BalanceAfter>=0,FORMAT(a.BalanceAfter,1),'0.0') ),
		               IF(a.RefCode IS NULL, ' -- ', a.RefCode)
		         FROM
		               PAYMENT_OVERALL_TRANSACTION_LOG as a
		               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
		         WHERE
		               a.StudentID =$u_id";
    
        $table_content .= "<table width=100% border=0 cellpadding=5 cellspacing=0 align=center>";
		$table_content .= "<tr>
							<td nowrap='nowrap' width='30%' class='formfieldtitle tabletext'>$i_identity_student</td>
							<td class='tabletext' valign='top' width='70%'>$name</td>
							</tr>";
		$table_content .= "<tr>
							<td nowrap='nowrap' width='30%' class='formfieldtitle tabletext'>$i_Payment_Field_Balance</td>
							<td class='tabletext' valign='top' width='70%'>$bal</td>
							</tr>";							
		$table_content .= "<tr>
							<td nowrap='nowrap' width='30%' class='formfieldtitle tabletext'>$i_Payment_Field_LastUpdated</td>
							<td class='tabletext' valign='top' width='70%'>$last_modified</td>
							</tr>";		
		$table_content .= "<tr>
							<td nowrap='nowrap' width='30%' class='formfieldtitle tabletext'>$i_Payment_Field_PPSAccountNo</td>
							<td class='tabletext' valign='top' width='70%'>$pps</td>
							</tr>";		
							
		
		$table_content .= "<tr><td colspan='2'><table width='100%' border='0' cellspacing='0' cellpadding='4'>";
		$table_content .= "<tr><td width=15% class='tablebluetop tabletopnolink'>$i_Payment_Field_TransactionTime</td>";
		$table_content .= "<td width=15% class='tablebluetop tabletopnolink'>$i_Payment_Field_TransactionFileTime</td>";
		$table_content .= "<td width=10% class='tablebluetop tabletopnolink'>$i_Payment_Field_TransactionType</td>";
		$table_content .= "<td width=10% class='tablebluetop tabletopnolink'>$i_Payment_TransactionType_Credit</td>";
		$table_content .= "<td width=10% class='tablebluetop tabletopnolink'>$i_Payment_TransactionType_Debit</td>";
		$table_content .= "<td width=20% class='tablebluetop tabletopnolink'>$i_Payment_Field_TransactionDetail</td>";
		$table_content .= "<td width=10% class='tablebluetop tabletopnolink'>$i_Payment_Field_BalanceAfterTransaction</td>";
		$table_content .= "<td width=10% class='tablebluetop tabletopnolink'>$i_Payment_Field_RefCode</td></tr>";
		       
		$payment_detail_result = $lpayment->returnArray($sql2,7);
		
		if(sizeof($payment_detail_result)>0)
		{
			for($j=0; $j<sizeof($payment_detail_result); $j++)
			{
				list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$j];
				
				$table_content .= "<tr class='tablebluerow". ( $j%2 + 1 )."'>";
				$table_content .= "<td class='tabletext'>$transaction_time</td>";
				$table_content .= "<td class='tabletext'>$credit_transaction_time</td>";
				$table_content .= "<td class='tabletext'>$transaction_type</td>";
				$table_content .= "<td class='tabletext'>$credit_amount</td>";
				$table_content .= "<td class='tabletext'>$debit_amount</td>";
				$table_content .= "<td class='tabletext'>$transaction_detail</td>";
				$table_content .= "<td class='tabletext'>$balance_after</td>";
				$table_content .= "<td class='tabletext'>$ref_no</td></tr>";
			}
		}
		if(sizeof($payment_detail_result)==0)
		{
			$table_content .= "<tr><td class='tablebluerow2 tabletext' colspan=8 align=center><br />$i_no_record_exists_msg<br /><br /></td></tr>";
		}
		$table_content .= "</table></td></tr>";
		
		$table_content .= "<tr>
                			<td class='dotline' colspan='2'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1' /></td>
                			</tr>";		
							
		$table_content .= "</table>";
		$table_content .= "<table border=0>";
		$table_content .= "<tr><td height=20px></td></tr>";
		$table_content .= "</table>";
	}
}

?>

<SCRIPT LANGUAGE=Javascript>
function change_class()
{
	window.location = "payment_transaction_log.php?classid=" + document.form1.classid.value;
}
</SCRIPT>

<form name=form1 action="" method="POST">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
        <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr> 
        	<td><br />
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_class?></span></td>
					<td class="tabletext"><?=$class_selection?></td>
				</tr>
				
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_identity_student?></span></td>
					<td class="tabletext"><?=$student_selection?></td>
				</tr>
				</table>
			</td>
		</tr>
        </table>
	</td>
</tr>
<tr>
	<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_ACTION_BTN($button_view, "submit", "","submit2");?>
	</td>
</tr>
</table>

<table width="90%" border="0" cellspacing="0" cellpadding="5" align=center>
	<tr valign="top">
		<td>
			<?=$table_content?>
		</td>
	</tr>
</table>
</form>

<?
}
intranet_closedb();
$linterface->LAYOUT_STOP();
?>