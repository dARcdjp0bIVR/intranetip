<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libstudentphoto.php");


intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lprofile = new libprofile();
$stuPerformance = new libeClassApp_stuPhoto();
$linterface = new interface_html();


$CurrentPage = "PageStudentOfficialPhotoView";
$TAGS_OBJ = $stuPerformance->getReportTopTabMenuAry('class');
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


### edit table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
// 	// Report Type
// // 	$x .= '<tr>'."\r\n";
// // 		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['iAccount']['StudentPerformance_viewType'].'</td>'."\r\n";
// // 		$x .= '<td>'."\r\n";
// // 			$x .= $linterface->Get_Radio_Button('viewTypeRadio_raw', 'viewType', 'raw', $isChecked=1, $Class="", $Lang['iAccount']['StudentPerformance_rawData'], '', $isDisabled=0);
// // 			$x .= '&nbsp;';
// // 			$x .= $linterface->Get_Radio_Button('viewTypeRadio_statistics', 'viewType', 'statistics', $isChecked=0, $Class="", $Lang['iAccount']['StudentPerformance_statistics'], '', $isDisabled=0);
// // 		$x .= '</td>'."\r\n";
// // 	$x .= '</tr>'."\r\n";
	
	// Date range
	$curTermId = getCurrentSemesterID();
	$termObj = new academic_year_term($curTermId, $GetAcademicYearInfo=false);
	$startDate = date('Y-m-d', strtotime($termObj->TermStart));
	$endDate = date('Y-m-d', strtotime($termObj->TermEnd));
	$x .= '<tr style = "display:none;">'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $Lang['General']['From'].' '.$linterface->GET_DATE_PICKER('startDate', $startDate).' '.$Lang['General']['To'].' '.$linterface->GET_DATE_PICKER('endDate', $endDate);
			$x .= $linterface->Get_Form_Warning_Msg('dateRangeWarnDiv', $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	// Class
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['General']['Class'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $lclass->getSelectClassID('id="classSel" name="classId"', $selected="", $DisplaySelect=2, $AcademicYearID='',$optionFirst=0, $DisplayClassIDArr='', $TeachingOnly=1);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTbl'] = $x;


### action buttons
$htmlAry['printBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Print'], "button", "goSubmit('print_student_record.php')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
// $htmlAry['exportBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Export'], "button", "goSubmit('print_student_record.php?YearClassID')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


### hidden fields
$htmlAry['outputModeHidden'] = $linterface->GET_HIDDEN_INPUT('outputMode', 'outputMode', '');
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goSubmit(webpage) {
	var canSubmit = true;
	$('div.warnMsgDiv').hide();
	if (canSubmit) {
// 		var selectedYearClassID = document.getElementById("classSel").selectedIndex;
// 		var yearClassID = document.getElementsByTagName("option")[selectedYearClassID].value;
		document.form1.action= webpage;
		$('form#form1').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST" target="_blank">
	<div class="table_board">
		<?=$htmlAry['formTbl']?>
		<br style="clear:both;" />
		
		<?=$linterface->MandatoryField()?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			
			<?=$htmlAry['printBtn']?>
			<?=$htmlAry['exportBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<?=$htmlAry['outputModeHidden']?>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>