<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libstudentphoto.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lprofile = new libprofile();
$stuPerformance = new libeClassApp_stuPhoto();
$linterface = new interface_html();


$CurrentPage = "PageStudentOfficialPhotoView";
$TAGS_OBJ = $stuPerformance->getReportTopTabMenuAry('subject');
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


### edit table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	
	//Date range
	$curTermId = getCurrentSemesterID();
	$termObj = new academic_year_term($curTermId, $GetAcademicYearInfo=false);
	$startDate = date('Y-m-d', strtotime($termObj->TermStart));
	$endDate = date('Y-m-d', strtotime($termObj->TermEnd));
// 			$x .= $Lang['General']['From'].' '.$linterface->GET_DATE_PICKER('startDate', $startDate, '',$DateFormat="yy-mm-dd", '', '', '', "reloadSubjectGroupSelection()").' '.$Lang['General']['To'].' '.$linterface->GET_DATE_PICKER('endDate', $endDate, '',$DateFormat="yy-mm-dd", '', '', '', "reloadSubjectGroupSelection()");
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
			$fcm = new form_class_manage();
			$lsubject = new subject();
			
			// get terms
			$termIdAry = Get_Array_By_Key($fcm->Get_Term_By_Date_Range($startDate, $endDate), 'YearTermID');
			
			// get subject groups
			$subjectGroupAssoAry = BuildMultiKeyAssoc($lsubject->Get_Subject_Group_List($termIdAry, $ClassLevelID='', $SubjectGroupID='', $_SESSION['UserID'], $returnAsso=0), 'SubjectGroupID', Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN'), $SingleValue=1);
			
			$selectionId = 'subjectGroupSel';
			$selectionName = 'subjectGroupIdAry[]';
			
			$selectionAttr = ' id="'.$selectionId.'" name="'.$selectionName.'" multiple size="10" ';
	
	// Subject Group
	$selectAllBtn = $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('$selectionId', 1);");
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$linterface->RequiredSymbol().' '.$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
// 		$x .= ;
// 		$x .= '<td>'."\r\n";
		$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div(getSelectByAssoArray($subjectGroupAssoAry, $selectionAttr, '', $isAll=0, $noFirst=1), $selectAllBtn, $SpanID='subjectGroupSelSpan');
			$x .= $linterface->Get_Form_Warning_Msg('subjectGroupWarnDiv', $Lang['General']['WarningArr']['PleaseSelectAtLeastOneSubjectGroup'], $Class='warnMsgDiv')."\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTbl'] = $x;


### action buttons
$htmlAry['printBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Print'], "button", "goSubmit('print_student_record.php')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
// $htmlAry['exportBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Export'], "button", "goSubmit('export')", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");


### hidden fields
$htmlAry['outputModeHidden'] = $linterface->GET_HIDDEN_INPUT('outputMode', 'outputMode', '');
?>
<script type="text/javascript">

function goSubmit(webpage) {
	var canSubmit = true;
	$('div.warnMsgDiv').hide();
	
	if (canSubmit) {
		document.form1.action = webpage;
		$('form#form1').submit();
	}
}
</script>
<form name="form1" id="form1" method="POST" target="_blank">
	<div class="table_board">
		<?=$htmlAry['formTbl']?>
		<br style="clear:both;" />
		
		<?=$linterface->MandatoryField()?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			
			<?=$htmlAry['printBtn']?>
			<?=$htmlAry['exportBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	
	<?=$htmlAry['outputModeHidden']?>
</form>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>