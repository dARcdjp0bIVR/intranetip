<?php
// using kenneth chung
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libfamily.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$pageNo = $_POST["pageNo"];
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size_change = $_POST["page_size_change"];	//	To refresh the page when change the $numPerPage, (without it cannot change the page size)
$numPerPage = !empty($_POST["numPerPage"])?$_POST["numPerPage"]:$page_size;	//	The parameter $page_size seems to be set in global.php, pls use $numPerPage to avoid modified the $page_size

$GeneralSetting = new libgeneralsettings();
$SettingList[] = "'TeacheriAccountProfileAllowEdit'";
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);

$setting = $Settings['TeacheriAccountProfileAllowEdit'];

if ($StudentID=="")
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['iAccount']['InvalidStudentData']);
	exit;
}
$lu_viewer = new libuser($UserID);
$lattend = new libattendance();

if ($lu_viewer->RecordType == 1)  # Teacher
{
    $lteaching = new libteaching();
    $class = $lteaching->returnTeacherClass($UserID);
    
    # check the student is in user's class 
    $lu_student = new libuser($StudentID);
    $pass = false;
    for($i=0;$i<sizeof($class);$i++)
    {
	    if($class[$i]['ClassName'] == $lu_student->ClassName)	$pass = true;
    }	
    
    if ($lu_student->UserID=="" || !$pass)
    {
        include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['iAccount']['InvalidStudentData']);
		exit;
    }
}
else if ($lu_viewer->RecordType == 3)  # Parent
{
	$lfamily = new libfamily();
	$children_list = $lfamily->returnChildrens($UserID);
	if (!in_array($StudentID,$children_list))
	{
		 include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT($Lang['iAccount']['InvalidStudentData']);
		exit;
	}
}
else # Student
{
	$StudentID = $UserID;
}

$MODULE_OBJ['title'] = $i_Attendance_DetailedAttendanceRecord;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();


$now = time();
if(($start_date!="")&&($end_date!=""))
{
	if($setting==1 && $_SESSION['UserType']==USERTYPE_STAFF)
	{
		$sql = $lattend->returnDetailedRecordByDate1($StudentID,$start_date,$end_date, 1);
		
		# TABLE INFO
		if($field=="") $field = 0;
		$li = new libdbtable2007(0, 0, 1);
		$li->field_array = array("AttendanceDate");
		$li->sql = $sql;
		$li->no_col = 5;
		$li->IsColOff = "DetailedRecordByYearWithReasonEditable";

		$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>\n";
		$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Type</td>\n";
		$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_DayType</td>\n";
		$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Reason</td>\n";
		$li->column_list .= "<td class='tablegreentop tabletopnolink'>$button_edit</td>\n";
		
		$x = $li->display($StudentID, 1);	
	}
	else
	{
		$sql = $lattend->returnDetailedRecordByDate1($StudentID,$start_date,$end_date, 1);
		
		# TABLE INFO
			if($field=="") $field = 0;
			$li = new libdbtable2007(0, 0, 1);
			$li->field_array = array("AttendanceDate");
			$li->sql = $sql;
			$li->no_col = 5;
			$li->IsColOff = "DetailedRecordByYearWithReasonEditable";

			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Type</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_DayType</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Reason</td>\n";
			
			$x = $li->display($StudentID, 0);	
	}
}
else
{
	if ($year == "")
	{
		$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
		$yearend = date('Y-m-d',getEndOfAcademicYear($now));
		if($setting==1 && $_SESSION['UserType']==USERTYPE_STAFF)
		{
		  	$sql = $lattend->returnDetailedRecordByYear2($StudentID, $year,1);

			# TABLE INFO
			if($field=="") $field = 0;
			$li = new libdbtable2007(0, 0, 1);
			$li->field_array = array("AttendanceDate");
			$li->sql = $sql;
			$li->no_col = 5;
			$li->IsColOff = "DetailedRecordByYearWithReasonEditable";

			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Type</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_DayType</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Reason</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$button_edit</td>\n";
			
			$x = $li->display($StudentID, 1);	
	  	}
		else
		{
			$sql = $lattend->returnStudentRecord($StudentID,$yearstart,$yearend, 2);
		  	
		  	# TABLE INFO
			if($field=="") $field = 0;
			$li = new libdbtable2007(0, 0, 1);
			$li->field_array = array("AttendanceDate");
			$li->sql = $sql;
			$li->no_col = 5;
			$li->IsColOff = "DetailedRecordByYear";

			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Type</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_DayType</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Reason</td>\n";
			
			$x = $li->display($StudentID);	
			
			
		}
	}
	else
	{
		if($setting==1 && $_SESSION['UserType']==USERTYPE_STAFF)
		{
			$sql = $lattend->returnDetailedRecordByYear2($StudentID, $year,1);

			# TABLE INFO
			if($field=="") $field = 0;
			$li = new libdbtable2007(0, 0, $pageNo);
			$li->field_array = array("AttendanceDate");
			$li->sql = $sql;
			$li->no_col = 5;
			$li->IsColOff = "DetailedRecordByYearWithReasonEditable";
			$li->page_size = $numPerPage;

			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Type</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_DayType</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Reason</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$button_edit</td>\n";
			
			$x = $li->display($StudentID, 1);	
		}
		else
		{
			$sql = $lattend->returnDetailedRecordByYear($StudentID, $year,1);

			# TABLE INFO
			if($field=="") $field = 0;
			$li = new libdbtable2007(0, 0, $pageNo);
			$li->field_array = array("AttendanceDate");
			$li->sql = $sql;
			$li->no_col = 5;
			$li->IsColOff = "DetailedRecordByYear";
			$li->page_size = $numPerPage;

			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Date</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Type</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_DayType</td>\n";
			$li->column_list .= "<td class='tablegreentop tabletopnolink'>$i_Attendance_Reason</td>\n";
			
			$x = $li->display($StudentID);	
		}
	}
}

?>

<script language="JavaScript" src=<?=$intranet_httppath?>/templates/ajax_yahoo.js></script>
<script language="JavaScript" src=<?=$intranet_httppath?>/templates/ajax_connection.js></script>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>

<script language="JavaScript">
isMenu = true;
</script>

<div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>

<SCRIPT Language="JavaScript">
<!--
function putBack(target, value)
{
	var temp = eval("document.form1.reason_"+target);
    temp.value = value;
	hideMenu('ToolMenu');
}
function moveToolTip2(lay, FromTop, FromLeft)
{
	if (tooltip_ns6)
	{
		var myElement = document.getElementById(lay);
		myElement.style.left = (FromLeft + 10) + "px";
		myElement.style.top = (FromTop + document.body.scrollTop) + "px";
	}
	
	else if(tooltip_ie4)
	{
		eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
	}
	else if(tooltip_ns4)
	{
		eval(doc + lay + sty + ".top = "  +  FromTop)
	}
	
	if (!tooltip_ns6) 
		eval(doc + lay + sty + ".left = " + (FromLeft + 10));
}
// AJAX follow-up
var callback = {
    success: function ( o )
    {
            jChangeContent( "ToolMenu", o.responseText );
    }
}
// start AJAX
function retrieveReason(type,record_id)
{
    //FormObject.testing.value = 1;
    obj = document.form1;
    var myElement = document.getElementById("ToolMenu");
    
    showMenu("ToolMenu","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
    YAHOO.util.Connect.setForm(obj);

    // page for processing and feedback
    var path = "getReason.php?RecordType=" + type + "&RecordID=" + record_id;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
-->
</SCRIPT>


<form name="form1" method="POST">

<br />
<table width="100%" border="00" cellspacing="0" cellpadding="0">

  <tr>
    <td><?=$x?></td>
  </tr>
	
    <tr> 
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
					<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","closebtn") ?></td>
			</tr>
                </table>     		
		</td>
	</tr>
</table>

<input type="hidden" name="type" value="<?=$type?>">
<input type="hidden" name="StudentID" value="<?=$StudentID?>">
<input type="hidden" name="Year" value="<?=$year?>">
<input type="hidden" name="start_date" value="<?$start_date?>">
<input type="hidden" name="end_date" value="<?$end_date?>">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>