<?php
# using: 

### 		!!!!! Note: Required to deploy with libuser.php if upload this file to client BEFORE ip.2.5.9.1.1	!!!!!

######### Change Log
#
#   Date:   2019-06-04 Henry
#           added CSRF protection 
#
#   Date:   2018-11-12 Cameron
#           change $sys_custom['project'] to $sys_custom['project']['CourseTraining'] for Amway, Oaks, CEM 
#
#   Date:   2018-09-06 Anna
#           Double confirm user type and whether UserID changed 
#
#	Date:	2018-05-03 Carlos
#			Added the return message in session $_SESSION['iAccountChangePasswordReturnMsg'] from change password page.
#
#	Date:	2017-12-22 Cameron
#			redirect to index_oaks.php if $sys_custom['project']['Oaks'] is set
#
#	Date:	2017-10-19 Ivan [ip.2.5.9.1.1]
#			Changed default personal photo path to call function by libuser.php
#
#	Date:	2016-02-17 Carlos
#			Do not display gender if it is null. 
#
#	Date:	2015-12-11 Cameron
#			redirect to index_amway.php if $sys_custom['project']['Amway'] is set
#
#	Date:	2015-10-08	Bill	[DM#2916]
#			added js function checkPhotoExt(), to display warning if upload photo with incorrect format
#
#	Date:	2013-01-22	YatWoon
#			Display child or children [Case#2013-0122-1406-31073]
#
#	Date:	2011-12-16	Yuen
#			list children for parent view
#
#	Date:	2011-08-25	Yuen
#			generate eClass user code for students for eClass parent apps use
#
#	Date:	2010-09-10	yatwoon
#			add personal photo (file/photo/personal/p[UserID].JPG)
#
#	Date:	2010-08-11	yatWoon
#			hide "Title" setting
#
##########################

//$NoLangOld20 = true;

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if ($sys_custom['project']['CourseTraining']['Amway']) {
	include_once($PATH_WRT_ROOT."home/iaccount/account/index_amway.php");
	exit();
}

if ($sys_custom['project']['CourseTraining']['Oaks']) {
	include_once($PATH_WRT_ROOT."home/iaccount/account/index_oaks.php");
	exit();
}

// also use amway file
if ($sys_custom['project']['CourseTraining']['CEM']) {
    include_once($PATH_WRT_ROOT."home/iaccount/account/index_amway.php");
    exit();
}

if($sys_custom['DHL']){
	include_once($PATH_WRT_ROOT."home/iaccount/account/index_dhl.php");
	exit();
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PagePersonalInfo";
$lprofile = new libprofile();

$li = new libuser($UserID);
// $isStudent = $li->isStudent();

/*
$lf = new libfilesystem();

$new_user_info_setting = false;
if(file_exists($intranet_root."/file/user_info_settings_".$li->RecordType.".txt")){
  $new_user_info_setting = true;
  $misc = $lf->file_read($intranet_root."/file/user_info_settings_".$li->RecordType.".txt");
  $line = explode("\n",$misc);
  
  $misca = explode(",",$line[0]);
  array_shift($misca);
}
*/

if($_SESSION['UserType'] == USERTYPE_STUDENT && $_GET['UserID'] != $_SESSION['UserID'] && $_GET['UserID']!=''){
    No_Access_Right_Pop_Up();
}
########## Official Photo
//$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
$photo_link = $li->returnDefaultOfficialPhotoPath();
$photo_delete = "";
if ($li->PhotoLink !="" && $_SESSION['UserType']==USERTYPE_STUDENT)
{
    if (is_file($intranet_root.$li->PhotoLink) && $photo_link != $li->PhotoLink)
    {
        $photo_link = $li->PhotoLink;
        
        if($li->RetrieveUserInfoSetting("CanUpdate", "Photo")) 
          $photo_delete = "<a href=\"javascript:deletePhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeleteOfficialPhoto'] ." </a>";
    }
}
########## Personal Photo
//$personal_photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
$personal_photo_link = $li->returnDefaultPersonalPhotoPath();
$personal_photo_delete = "";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $personal_photo_link = $li->PersonalPhotoLink;
        
        if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")) 
          $personal_photo_delete = "<a href=\"javascript:deletePersonalPhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeletePersonalPhoto'] ." </a>";
    }
}
$update_no = 0;
// $Title0 = ($li->Title==0) ? "CHECKED" : "";
// $Title1 = ($li->Title==1) ? "CHECKED" : "";
// $Title2 = ($li->Title==2) ? "CHECKED" : "";
// $Title3 = ($li->Title==3) ? "CHECKED" : "";
// $Title4 = ($li->Title==4) ? "CHECKED" : "";
// $Title5 = ($li->Title==5) ? "CHECKED" : "";
$Gender0 = ($li->Gender=="M") ? "CHECKED" : "";
$Gender1 = ($li->Gender=="F") ? "CHECKED" : "";

// if($msg)
// 	$xmsg = "abc";

if(isset($_SESSION['iAccountChangePasswordReturnMsg'])){
	$xmsg = $_SESSION['iAccountChangePasswordReturnMsg'];
	unset($_SESSION['iAccountChangePasswordReturnMsg']);
}else if(isset($Lang['General']['ReturnMessage'][$msg])){
	$xmsg = $Lang['General']['ReturnMessage'][$msg];
}

/*
switch($msg){
     case 1: $xmsg = $i_con_msg_add; break;
     case 2: $xmsg = $i_con_msg_update; break;
     case 3: $xmsg = $i_con_msg_delete; break;
     case 4: $xmsg = $i_con_msg_email; break;
     case 5: $xmsg = $i_con_msg_password1; break;
     case 6: $xmsg = $i_con_msg_password2; break;
     case 7: $xmsg = $i_con_msg_email1; break;
     case 8: $xmsg = $i_con_msg_email2; break;
     case 9: $xmsg = $i_con_msg_photo_delete; break;
}
*/

### Title ###
$TAGS_OBJ[] = array($i_UserProfilePersonal);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($i_UserProfilePersonal);

$linterface->LAYOUT_START($xmsg);

if ($li->isParent())
{
	$myChildren = $li->getChildrenList("", false);
	
	# show name, class & class no, user code
	$children_list_with_photos = "";
	
	for ($i=0; $i<sizeof($myChildren); $i++)
	{
		$childobj = $myChildren[$i];
		if ($childobj["ClassName"]!="")
		{
			$ClassAndNo = $childobj["ClassName"];
		}
		if ($childobj["ClassNumber"]!="")
		{
			$ClassAndNo .= (($ClassAndNo!="") ? " - " : "" ) .$childobj["ClassNumber"];
		}
		$userPhoto = $li->GET_OFFICIAL_PHOTO_BY_USER_ID($childobj["StudentID"]);
		if (strstr($userPhoto[1], "images/myaccount_personalinfo/samplephoto.gif"))
		{
			$lui = new libuser($childobj["StudentID"]);
			if ($lui->PersonalPhotoLink !="" && is_file($intranet_root.$lui->PersonalPhotoLink))
			{
				$userPhoto[1] = $lui->PersonalPhotoLink;
			}
		}
		$eClassUserCode = "0001".$config_school_code . str_pad($childobj["StudentID"], 10, "0", STR_PAD_LEFT);
		$children_list_with_photos .= "<td nowrap='nowrap' align='center'><img src=\"".$userPhoto[1]."\" border='0' width='100' /><br />".$childobj["StudentName"]."<br />".$ClassAndNo."<br /><img src='/images/2009a/iPortfolio/icon_active_ipf.gif' border='0' title=\"".$Lang['General']['eClassUserCode']."\" /><font size='-1' title=\"".$Lang['General']['eClassUserCode']."\">".$eClassUserCode."</font></td>";
	}
	if ($children_list_with_photos!="")
	{
		$children_list_with_photos = "<p><table border='0' cellpadding='15' cellspacing='10'><tr>".$children_list_with_photos."</tr></table></p>";
	}
}

if ($plugin['ASLParentApp'] && isset($config_school_code) && $config_school_code!="" && $_SESSION['UserType']==USERTYPE_STUDENT)
{
	$eClassUserCode = "0001".$config_school_code . str_pad($UserID, 10, "0", STR_PAD_LEFT);
}
?>

<script language="javascript">
function checkPhotoExt(){
	<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Photo") && $_SESSION['UserType']==USERTYPE_STUDENT){ ?>
		if(document.form1.photo && document.form1.photo.value!="" && getFileExtension(document.form1.photo).toLowerCase()!=".jpg"){
			alert("<?=$i_UserPhotoGuide?>");
			return false;
		}
	<?php } ?>
	
	<?php if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")){ ?>
		if(document.form1.personal_photo && document.form1.personal_photo.value!="" && getFileExtension(document.form1.personal_photo).toLowerCase()!=".jpg"){
			alert("<?=$i_UserPhotoGuide?>");
			return false;
		}
	<?php } ?>
	
	document.form1.submit();
}
function deletePhoto() {
	if(confirm("<?=$Lang['Personal']['DeleteOfficialPhoto']?>?")){
		window.location = "photo_delete.php?PhotoType=official&UserID=<?=$UserID?>"
	}
}

function deletePersonalPhoto() {
	if(confirm("<?=$Lang['Personal']['DeletePersonalPhoto']?>?")){
		window.location = "photo_delete.php?PhotoType=personal&UserID=<?=$UserID?>"
	}
}
</script>
<form name="form1" enctype="multipart/form-data" method="post" action="personal_info_update.php">
<table class="form_table_v30">
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title" width="30%">
	    				<?=$i_UserLogin?>
	    			</td>
	    			<td class="tabletext" valign="top" width="70%"><?=$li->UserLogin?></td>
	    			<td class="tabletext" rowspan="8" valign="top" align="left" nowrap>
	    				<? if($_SESSION['UserType']==USERTYPE_STUDENT) {?>
	    				<?=$Lang['Personal']['OfficialPhoto']?><br />
	    				<img src="<?=$photo_link?>" width="100"><br><?=$photo_delete?><br>
	    				<? } ?>
	    				<?=$Lang['Personal']['PersonalPhoto']?><br />
	    				<img src="<?=$personal_photo_link?>" width="100"><br><?=$personal_photo_delete?>
	    			</td>
    			</tr>
<?php
if ($eClassUserCode!="")
{
?>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$Lang['General']['eClassUserCode']?>
	    			</td>
	    			<td class="tabletext" valign="top"><?=$eClassUserCode?></td>
    			</tr>
<?php
}
?>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserEnglishName?>
	    			</td>
	    			<td class="tabletext" valign="top"><?=intranet_wordwrap($li->EnglishName,20," ",1)?></td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserChineseName?>
	    			</td>
	    			<td class="tabletext" valign="top"><?=intranet_wordwrap($li->ChineseName,20," ",1)?></td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserNickName?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php if($li->RetrieveUserInfoSetting("CanUpdate", "NickName")){ $update_no++;?>
						<input class="textboxnum" type="text" name="NickName" size="20" maxlength="100" value="<?=$li->NickName?>">
					<?php } else { ?>
						<? echo $li->NickName; ?>
					<?php }?>
	    			</td>
    			</tr>
<?php if ($_SESSION['UserType']==USERTYPE_STUDENT) { ?>

    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$Lang['SysMgr']['FormClassMapping']['Class']?>
	    			</td>
	    			<td class="tabletext" valign="top">
						<? echo $li->ClassName; ?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$Lang['ModuleLicense']['ClassNumber']?>
	    			</td>
	    			<td class="tabletext" valign="top">
						<? echo $li->ClassNumber; ?>
	    			</td>
    			</tr>

<?php } ?>
    			<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Photo") && $_SESSION['UserType']==USERTYPE_STUDENT){ $update_no++;?>
	    			<tr>
		    			<td valign="top" nowrap="nowrap" class="field_title">
		    				<?=$Lang['Personal']['OfficialPhoto']?>
		    			</td>
		    			<td class="tabletextremark" valign="top">
						
		    				<input type="file" name="photo" class="file"><br /><?=$i_UserPhotoGuide?>
		    			</td>
	    			</tr>
    			<?php } else { ?>
              		<input type="hidden" name="photo" value=""><br />
				<?php } ?>
				
				<?php if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")){ $update_no++;?>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title">
						<?=$Lang['Personal']['PersonalPhoto']?>
					</td>
					<td class="tabletextremark" valign="top">
						<input type="file" name="personal_photo" class="file"><br /><?=$i_UserPhotoGuide?>
					</td>
				</tr>
	    		<?php } else { ?>
              		<input type="hidden" name="personal_photo" value=""><br />
				<?php } ?>	
    			
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserGender?>
	    			</td>
	    			<td class="tabletext" valign="top">
						<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Gender")){ $update_no++;?>
	    				<input type="radio" name="Gender" id="GenderM" value="M" <?php echo $Gender0; ?>><label for="GenderM"><?php echo $i_gender_male; ?></label>&nbsp;&nbsp;
						<input type="radio" name="Gender" id="GenderF" value="F" <?php echo $Gender1; ?>><label for="GenderF"><?php echo $i_gender_female; ?></label>
						<?php }
						      else{
						        //echo $Gender0!=""?$i_gender_male:$i_gender_female;
						        // gender maybe null
						        if($Gender0)
						        	echo $i_gender_male;
						        else if($Gender1) 
						        	echo $i_gender_female;
						      }
						?>
	    			</td>
    			</tr>
<? if($_SESSION['UserType']==USERTYPE_STUDENT) {?>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserDateOfBirth?>
	    			</td>
	    			<td class="tabletext" valign="top">
						<?php if($li->RetrieveUserInfoSetting("CanUpdate", "DOB")){ $update_no++;?>
	    					<input class="textboxnum" type="text" name="DateOfBirth" size="10" maxlength="10" value="<?php echo $li->DateOfBirth;?>">
	    					<span class="tabletextremark">(yyyy-mm-dd)</span>
						<?php }
						      else
						        echo $li->DateOfBirth ? $li->DateOfBirth : "--";
						?>
	    			</td>
    			</tr>
<?php } ?>
    		</table>

<? if($update_no) {?>
<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_save, "button", "checkPhotoExt();");?> 
<?=$linterface->GET_ACTION_BTN($button_reset, "reset");?>
<p class="spacer"></p>
</div>
<? } ?>

<?php if ($li->isParent()) { ?>
<div class="table_board"><span class="sectiontitle_v30"><?=sizeof($myChildren) > 1? $Lang['SysMgr']['RoleManagement']['MyChildren'] : $Lang['SysMgr']['RoleManagement']['MyChild']?></span><br />
<?=$children_list_with_photos ?>
<?php } ?>
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>

<?php
intranet_closedb();
//print $linterface->FOCUS_ON_LOAD("form1.Title[0]");
$linterface->LAYOUT_STOP();
?>