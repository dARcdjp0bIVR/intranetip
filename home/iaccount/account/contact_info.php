<?php
# using: 

##################################################
#   Date:   2019-06-04 Henry
#           - added CSRF protection
#
#   Date:   2018-09-06 Anna
#           Double confirm user type and whether UserID changed
#
#	Date:	2017-04-24	Carlos
#			$sys_custom['DHL'] - hide unnecessary fields for DHL.
#
#	Date:	2014-10-24	Omas
#			Add warning box for remind change email in first login
#
#	Date:	2012-02-08	YatWoon
#			change the email remark [Case#2012-0206-1200-56071]
#
##################################################

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageContactInfo";
$lprofile = new libprofile();

$li = new libuser($UserID);

if($_SESSION['UserType'] == USERTYPE_STUDENT && $_GET['UserID'] != $_SESSION['UserID'] && $_GET['UserID']!=''){
    No_Access_Right_Pop_Up();
}

// $isStudent = $li->isStudent();

/*
$lf = new libfilesystem();

if(file_exists($intranet_root."/file/user_info_settings_".$li->RecordType.".txt"))
{
  $new_user_info_setting = true;
  $misc = $lf->file_read($intranet_root."/file/user_info_settings_".$li->RecordType.".txt");
  $line = explode("\n",$misc);

  $misca = explode(",",$line[1]);
  array_shift($misca);
}
else
{
  $new_user_info_setting = false;
  $misc = $lf->file_read($intranet_root."/file/basic_misc.txt");
  $temp = explode("\n",$misc);

  $misca = array_fill(0,9,'');
  $misca[8] = $temp[0];
  $misca[0] = $temp[2];
}

$ableChangeEmail = ($misca[8]!=1);
*/

### Title ###
$TAGS_OBJ[] = array($i_UserProfileContact);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
//$PAGE_NAVIGATION[] = array($i_UserProfileContact);
$xmsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($xmsg);

$update_no = 0;


$warningString = $Lang['iAccount']['ContactInfo']['AlertChangeEmail'];
$warningBox = $linterface->Get_Warning_Message_Box($title='', $warningString);
?>
<script language="javascript">
function checkform(obj){
     <?php
       if ($li->RetrieveUserInfoSetting("CanUpdate", "Email"))
       {
       ?>
     	if(!check_text(obj.UserEmail, "<?php echo $i_alert_pleasefillin.$i_UserEmail; ?>.")) return false;
     	if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
     <?php
     }
     ?>
}
</script>

<? if ($user=="newuser"){ ?>
<?=$warningBox?>
<?}?>
<form name="form1" method="post" action="contact_info_update.php" onsubmit="return checkform(this)">
    		<table class="form_table_v30">
    		<?php if(!$sys_custom['DHL']){ ?>	
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserHomeTelNo?>
	    			</td>
	    			<td class="tabletext" valign="top" width="70%">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "HomeTel"))
					  {
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"HomeTelNo\" maxlength=\"20\" value=\"$li->HomeTelNo\">";
					    $update_no++;
				    	}
					  else{
					    echo intranet_wordwrap($li->HomeTelNo,20," ",1);
					  }
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserOfficeTelNo?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "OfficeTel"))
					  {
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"OfficeTelNo\" maxlength=\"20\" value=\"$li->OfficeTelNo\">";
					    $update_no++;
				    }
					  else{
					    echo intranet_wordwrap($li->OfficeTelNo,20," ",1);
					  }
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserMobileTelNo?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php
					        if($li->RetrieveUserInfoSetting("CanUpdate", "Mobile"))
					        {
					          echo "<input class=\"textboxnum\" type=\"text\" name=\"MobileTelNo\" maxlength=\"20\" value=\"$li->MobileTelNo\">";
					          $update_no++;
				          }
					        else
					          echo intranet_wordwrap($li->MobileTelNo,20," ",1);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserFaxNo?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "Fax"))
					  {
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"FaxNo\" maxlength=\"20\" value=\"$li->FaxNo\">";
					    $update_no++;
				    }
					  else{
					    echo intranet_wordwrap($li->FaxNo,20," ",1);
					  }
					?>
	    			</td>
    			</tr>
    			
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserAddress?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Address")){
						$update_no++;
						?>
							<?=$linterface->GET_TEXTAREA("Address", $li->Address);?>
					<?php }
					      else
					        echo nl2br($li->Address);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserCountry?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "Country"))
					  {
					    echo "<input class=\"textboxnum\" type=\"text\" name=\"Country\" maxlength=\"50\" value=\"$li->Country\">";
					    $update_no++;
				    }
					  else
					    echo intranet_wordwrap($li->Country,20," ",1);
					?>
	    			</td>
    			</tr>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserURL?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php
					  if($li->RetrieveUserInfoSetting("CanUpdate", "URL"))
					  {
					    echo "<input class=\"textboxtext\" type=\"text\" name=\"URL\" value=\"$li->URL\">";
					    $update_no++;
				    }
					  else
					    echo $li->URL;
					?>
	    			</td>
    			</tr>
    		<?php } ?>
    			<tr>
	    			<td valign="top" nowrap="nowrap" class="field_title">
	    				<?=$i_UserEmail?> <? if($li->RetrieveUserInfoSetting("CanUpdate", "Email")) {?><span class='tabletextrequire'>*</span><? } ?>
	    			</td>
	    			<td class="tabletext" valign="top">
					<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Email")){ 
						$update_no++;
						?>
					 	<input class="textboxtext" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$li->UserEmail?>">
					 	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
					<?php
					      }
					      else
					        echo $li->UserEmail;
					?>
	    			</td>
    			</tr>
    		</table>

<? if($update_no) {?>
<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?=$linterface->GET_ACTION_BTN($button_save, "submit");?> 
	<?=$linterface->GET_ACTION_BTN($button_reset, "reset");?>
<p class="spacer"></p>
</div>
<? } ?>
<br />
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.HomeTelNo");
$linterface->LAYOUT_STOP();
?>
