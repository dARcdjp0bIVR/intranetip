<?php
// Editing by 
/*
 *  2019-06-04 Henry
 * 		- added CSRF protection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!$sys_custom['DHL']){
	header("Location:/");
	exit;
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PagePersonalInfo";
$lprofile = new libprofile();
$libdhl = new libdhl();

$records = $libdhl->getUserRecords(array('user_id'=>$_SESSION['UserID'],'GetOrganizationInfo'=>1));
$user = $records[0];

$company_name = Get_Lang_Selection($user['CompanyNameChi'],$user['CompanyNameEng']);
$division_name = Get_Lang_Selection($user['DivisionNameChi'],$user['DivisionNameEng']);
$department_name = Get_Lang_Selection($user['DepartmentNameChi'],$user['DepartmentNameEng']);

$department = '';
if($department_name != ''){
	$department = $libdhl->formatOrganizationDisplayName($company_name,$division_name,$department_name);
}

### Title ###
$TAGS_OBJ[] = array($i_UserProfilePersonal);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($i_UserProfilePersonal);

$linterface->LAYOUT_START();
?>
<form name="form1" method="post" action="">
<table class="form_table_v30">
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title" width="30%">
			<?=$i_UserLogin?>
		</td>
		<td class="tabletext" valign="top" width="70%"><?=Get_String_Display($user['UserLogin'])?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['DHL']['StaffName']?>
		</td>
		<td class="tabletext" valign="top"><?=Get_String_Display($user['EnglishName'])?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['DHL']['Department']?>
		</td>
		<td class="tabletext" valign="top"><?=Get_String_Display($department)?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['DHL']['JoinDate']?>
		</td>
		<td class="tabletext" valign="top"><?=Get_String_Display($user['JoinDate'])?></td>
	</tr>
	<!--
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['DHL']['EffectiveDate']?>
		</td>
		<td class="tabletext" valign="top"><?=Get_String_Display($user['EffectiveDate'])?></td>
	</tr>
	-->
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['DHL']['TerminatedDate']?>
		</td>
		<td class="tabletext" valign="top"><?=Get_String_Display($user['TerminatedDate'])?></td>
	</tr>
</table>
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>