<?php
// editing by 

/*********************************************** Change log *********************************************
 * 2019-06-04 (Henry): added CSRF protection
 * 2019-02-22 (Bill): fixed: not update modified user in INTRANET_USER  [2019-0221-0952-39207]
 * 2018-11-12 (Cameron): consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
 * 2018-06-05 (Carlos): Strange case [2018-0604-0946-25066] fail to carry return msg in session, fall back to append msg to redirect url.
 * 2018-05-03 (Carlos): If change password success, redirect to iAccount page.
 * 2018-01-04 (Carlos): Clear session variable $_SESSION['FORCE_CHANGE_PASSWORD'] which is set at /login.php to force user to change password.
 * 2017-12-22 (Cameron): Configure for Oaks to show Portal linking in "login_password" page 
 * 2015-12-15 (Cameron): Add $extraPara (FromLogin=1) for showing Portal linking in "login_password" page for Amway
 * 2015-04-24 (Carlos): Added return case for Document Routing if see flag $FromDR=1
 * 2014-05-29 (Carlos): only send notification email if recipient email is valid
 * 2013-05-10 (Carlos): skip connection to IMAP account when change iMail plus password
 * 2013-04-03 (YatWoon): add returnpath "OnlineReg"
 * 2012-10-05 (YatWoon): fixed hash password no need strtolower user login [Case#2012-1004-1002-39066]
 * 2012-02-13 (Marcus): Added Reset password acknowledgement
 * 2011-11-18 (Carlos): Added checking on webmail change password
 ********************************************************************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

//Move this code to method ChangePassword in libauth;
//$li = new libdb();
//$lc = new libeclass();
$lauth = new libauth();

# for password reset case
if ($FromResetPassword)
{
	$UserID = "";
	$InfoArr = $lauth->GetInfoFromResetKey($Key);
	if(!is_array($InfoArr))
	{
		$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['InvalidKey'];
	}
	else
	{
		list($RequestTime, $ParUserLogin) = $InfoArr;
	    
		$ExpireTime = strtotime("+7 day", $RequestTime);
		if(time()>$ExpireTime)
		{
			$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['Expired'];
		}
	}
}
if ($warning_msg!="")
{
	die($warning_msg);	
}

$OldPassword = intranet_htmlspecialchars(trim($OldPassword));
$NewPassword = intranet_htmlspecialchars(trim($NewPassword));

$signal = $lauth->ChangePassword($UserID,$ParUserLogin,$OldPassword,$NewPassword,$ReNewPassword,$FromResetPassword);

if($signal == "UpdateSuccess")
{
    // Update Modified Info [2019-0221-0952-39207]
    $sql = "UPDATE INTRANET_USER SET DateModified = now(), ModifyBy = '".$_SESSION['UserID']."' WHERE UserID = '$UserID'";
    $result = $lauth->db_db_query($sql);
    
	// clear the session variable after changed password to avoid keep redirecting to /home/iaccount/account/login_password.php
	if(isset($_SESSION['FORCE_CHANGE_PASSWORD'])){
		unset($_SESSION['FORCE_CHANGE_PASSWORD']);
	}
    
	$_SESSION['iAccountChangePasswordReturnMsg'] = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	$redirect_to_iaccount = true;
}

//Move the  code below to method ChangePassword in libauth;
//$lu = new libuser($UserID, $ParUserLogin);
//
//$failed = !$lu->RetrieveUserInfoSetting("CanUpdate", "Password", $lu->RecordType);
//
# 2. Old Password is correct or not change password
//if (!$failed)
//{
//     if ($OldPassword != "" || $FromResetPassword)
//     {
////         if ($OldPassword != $lu->UserPassword && !$FromResetPassword)
//
//
//		 if(!$lauth->check_password($lu->UserLogin, $OldPassword) && !$FromResetPassword)
//         {
//             $failed = true;
//             $signal = "InvalidPassword";
//         } 
//         else
//         {
//             # 3. New Password and password confirm is the same
//             if ($NewPassword != $ReNewPassword)
//             {
//                 $failed = true;
//                 $signal = "InvalidPassword";
//             }
//         }
//     }
//}else{
//	$signal = "UpdateUnsuccess";
//}
//
//$fieldname = "";
//$result = array();
//if (!$failed)
//{
//     $fieldname .= " LastModifiedPwd = NOW() ";
//
//     if ($OldPassword != "" || $FromResetPassword)
//     {
//         $TargetPassword = $NewPassword;
//         if ($intranet_authentication_method == "HASH")
//         {
//             //$fieldname .= ", HashedPass = '".MD5(strtolower($lu->UserLogin).$TargetPassword.$intranet_password_salt)."'";
//             $fieldname .= ", HashedPass = '".MD5($lu->UserLogin.$TargetPassword.$intranet_password_salt)."'";
//             $fieldname .= ", UserPassword = ''";
//         }
//         else
//         {
//             $fieldname .= ", UserPassword = '$TargetPassword'";
//         }
//         $_SESSION['eclass_session_password'] = $TargetPassword;
//		
//     	 $lauth->UpdateEncryptedPassword($lu->UserID, $TargetPassword);
//		
//         # Webmail
//         $lwebmail = new libwebmail();
//         if($lwebmail->has_webmail && !$plugin['imail_gamma']){
//         	$result['webmail_changepassword'] = $lwebmail->change_password($lu->UserLogin,$TargetPassword,"iMail");
//         }
//        # iMail Gamma 
//        if($plugin['imail_gamma'] && $lu->ImapUserEmail != "" /* && ($_SESSION['SSV_EMAIL_LOGIN']!=''&&$_SESSION['SSV_EMAIL_PASSWORD']!=''&&$_SESSION['SSV_LOGIN_EMAIL']!='')*/)
//		{
//			$IMap = new imap_gamma(true);
//         	$imap_result = $IMap->change_password($lu->ImapUserEmail,$TargetPassword);
//         	if($imap_result)
//         		$_SESSION['SSV_EMAIL_PASSWORD'] = $TargetPassword;
//         	$result['gamma_changepassword'] = $imap_result;
//        }
//        
//         # FTP management
//         if ($plugin['personalfile'])
//         {
//             if ($personalfile_type == 'FTP' || $personalfile_type == 'LOCAL_FTP' || $personalfile_type == 'REMOTE_FTP')
//             {
//                 include_once($PATH_WRT_ROOT."includes/libftp.php");
//                 $lftp = new libftp();
//                 $result['ftp_changepassword'] = $lftp->changePassword($lu->UserLogin,$NewPassword,"iFolder");
//             }
//         }
//
//         if ($intranet_authentication_method == 'LDAP')
//         {
//             include_once($PATH_WRT_ROOT."includes/libldap.php");
//             $lldap = new libldap();
//             if ($lldap->isPasswordChangeNeeded())
//             {
////                 $result['ldap_changepassword'] = $lldap->changePassword($lu->UserLogin,$OldPassword,$NewPassword);
//					$result['ldap_changepassword'] = $lldap->changePassword($lu->UserLogin,$NewPassword);
//             }
//         }
//     }
//     else
//     {
//         $TargetPassword = "";
//     }
//
//     $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = ".$lu->UserID;
//     $result['sql_update'] = $li->db_db_query($sql) or die ($sql.mysql_error());
//     
//     $newmail = "";
//     
//     if (!in_array(false,$result))
//     {
//         $lc->eClassUserUpdateInfoIP($lu->UserEmail, $lu->Title, $lu->EnglishName,$lu->ChineseName,$lu->FirstName,$lu->LastName, $lu->NickName, $TargetPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$lu->DateOfBirth,$lu->Address,$lu->Country,$lu->URL,$lu->Info);
//         $signal = "UpdateSuccess";
//         
//         # Send Ack to admin
//         if($FromResetPassword)
//         {
//         	include_once($PATH_WRT_ROOT."includes/libemail.php");
//			include_once($PATH_WRT_ROOT."includes/libsendmail.php");
//		    $lsendmail = new libsendmail();
//         	
//		    $webmaster = get_webmaster();
//
//		    # Send ACK Email
//			list($ackMailSubject,$ackMailBody) = $lu->returnEmailNotificationData_HashedPw_ACK($lu->UserEmail, $lu->UserLogin);
//			if($webmaster != "" && intranet_validateEmail($webmaster,true)){
//				$result2 = $lsendmail->send_email($webmaster, $ackMailSubject,$ackMailBody,"");
//			}
//         }
//     }
//     else
//     {
//         $signal = "UpdateUnsuccess";
//     }
//}
//Move the  code above to method ChangePassword in libauth;
intranet_closedb();

if($FromDR=="1") {
	$x = '<script type="text/javascript" language="JavaScript">';
	$x.= 'if(window.parent.jsFinishChangePassword){window.parent.jsFinishChangePassword("'.$signal.'");}';
	$x.= '</script>';
	echo $x;
} else if($FromLogin=="OnlineReg") {
	header("Location: /home/eService/StudentRegistry/online_reg/login_password.php?msg=$signal&FromLogin=$FromLogin");
} else if($FromResetPassword) {
	header("Location: reset_password_confirm.php?msg=$signal");
} else {
    if ($FromLogin && ($sys_custom['project']['CourseTraining']['IsEnable'])) {
		$extraPara = "&FromLogin=1";
	}
	else {
		$extraPara = "";
	}
	
	if($extraPara == '' && $redirect_to_iaccount) {
		header("Location: index.php?msg=".$signal);
	} else {
		header("Location: login_password.php?msg=$signal".$extraPara);
	}
}
?>