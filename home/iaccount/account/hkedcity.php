<?php
// using : 
/*
 * Modification Log:
 *  2019-06-04 Henry
 * 		- added CSRF protection
 * 	Date : 2015-10-30	(Bill)
 * 		 - support API server for HKEdCity EdConnect
 * 	Date : 2015-10-02	(Bill)
 * 		 - support edit linked HKEdCity account
 */
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libhkedcity.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageHKEdCityLogin";
$lprofile = new libprofile();
$userObj = new libuser($UserID);

$lhkedcity = new libhkedcity();

$TAGS_OBJ[] = array($Lang['HKEdCity']['HKEdCityLogin']);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$xmsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($xmsg);

### get user data
$hkedcityUserLogin = ($userObj->HKEdCityUserLogin)? $userObj->HKEdCityUserLogin : $Lang['General']['EmptySymbol'];

### buttons
if($userObj->HKEdCityUserLogin)
	$htmlAry['actionBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Clear'], "button", "goDelete()", "deleteBtn", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
else
	$htmlAry['actionBtn'] = $linterface->Get_Action_Btn($Lang['HKEdCity']['LinkToHKEdCityAcct'], "button", "goHkEdCity()", "editBtn", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
	
### Form table
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	// HKEdCity userlogin
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['HKEdCity']['HKEdCityUserLogin'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span>'."\r\n";
				$x .= $hkedcityUserLogin;
			$x .= '</span>'."\r\n";				
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTbl'] = $x;
?>
<script language="javascript">
function goDelete() {
	window.location  = 'hkedcity_edit_update.php';
}
function goHkEdCity(){
	document.form1.submit();
}
</script>
<form name="form1" id="form1" method="POST" action="<?=$lhkedcity->getCentralServerPath()?>">
	<div class="table_board">
		<?=$htmlAry['formTbl']?>
		<br style="clear:both;" />
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['actionBtn']?>
			<p class="spacer"></p>
		</div>
	</div>
	<input type="hidden" name="HKEdCity" value="<?=$lhkedcity->getSendHKEdCityData("check")?>">
	<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>
<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>