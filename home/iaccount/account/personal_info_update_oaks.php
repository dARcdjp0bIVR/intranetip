<?php
/*
 * 	Using:
 * 
 * 	Purpose: update user profile info for Oaks	
 * 
 * 	Log
 * 
 *  2019-06-04 [Henry] added CSRF protection
 *  2019-02-22 [Bill] fixed: not update modified user in INTRANET_USER      [2019-0221-0952-39207]
 * 	2017-12-22 [Cameron] copy from personal_info_update.php, then modify
 * 
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$li = new libdb();
$lc = new libeclass();
$lu = new libuser($UserID);

$UserEmail = intranet_htmlspecialchars(trim($UserEmail));
$MobileTelNo = intranet_htmlspecialchars(trim($MobileTelNo));
$Address = intranet_htmlspecialchars(trim($Address));


# Check whether can change the info
$failed = false;

if ($UserEmail != "" && $lu->UserEmail != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail))
    {
        $failed = true;
        $signal = "EmailUsed";
    }
    else
    {
        if ($lc->isEmailExistInSystem($UserEmail))
        {
            $failed = true;
            $signal = "EmailUsed";
        }
    }
}

$fieldname = "";

if (!$failed)
{
     # Update photo
     ################################################
     # Personal Photo
     ################################################
     $re_path2 = "/file/photo/personal";
     $path2 = "$intranet_root$re_path2";
     // prevent cannot update official photo url in db 
     //$target = "";
     $target2 = "";
     if($personal_photo=="none" || $personal_photo == ""){
     }
     else
     {
         $lf = new libfilesystem();
         if (!is_dir($path2))
         {
              $lf->folder_new($path2);
         }
         $ext = strtolower($lf->file_ext($personal_photo_name));
         
         if ($ext == ".jpg")
         {
             $target2 = $path2 ."/p". $lu->UserID . $ext;
             $re_path2 .= "/p". $lu->UserID . $ext;
             
            $lf->lfs_copy($personal_photo, $target2);
			# check need to resize or not			
			include_once($PATH_WRT_ROOT."includes/SimpleImage.php");
			$im = new SimpleImage();
			if($im->GDLib_ava)
			{
				$im->load($target2);
				$w = $im->getWidth();
				$h = $im->getHeight();
				$default_photo_width = $default_photo_width ? $default_photo_width : 100;
				$default_photo_height = $default_photo_height ? $default_photo_height : 130;
				if($w > $default_photo_width || $h > $default_photo_height)	
				{
					$im->resize($default_photo_width,$default_photo_height);
					$im->save($target2);	
				}
			}
         }
     }
     ################################################
     # Personal Photo [End]
     ################################################
     
     $fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";

     if ($target2 != "")
     {
         $fieldname .= ",PersonalPhotoLink = '$re_path2'";
     }

     if ($lu->RetrieveUserInfoSetting("CanUpdate", "Mobile"))
     {
      	 $fieldname .= ",MobileTelNo = '$MobileTelNo'";
     }
     
     if($lu->RetrieveUserInfoSetting("CanUpdate", "Address"))
     {
     	 $fieldname .= ",Address = '$Address'";	
     }
      
     if ($UserEmail != "")
     {
      	 $fieldname .= ",UserEmail = '$UserEmail'";
         $newmail = $UserEmail;
     }
     else
     {
     	 $newmail = "";	
     }
     
     $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
     $result = $li->db_db_query($sql);
     
     if ($result)
     {
         $lc->eClassUserUpdateInfoIP($lu->UserEmail, $lu->Title, $lu->EnglishName,addslashes($lu->ChineseName),$lu->FirstName,$lu->LastName, $lu->NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $lu->ICQNo,$lu->HomeTelNo,$lu->FaxNo,$lu->DateOfBirth,$Address,$lu->Country,$lu->URL,$lu->Info);
         $signal = "UpdateSuccess";
         
         $lu->synUserDataToModules($lu->UserID);
     }
     else
     {
         $signal = "UpdateUnsuccess";
     }
}
else
{
}
intranet_closedb();

header("Location: index.php?msg=$signal");
?>
