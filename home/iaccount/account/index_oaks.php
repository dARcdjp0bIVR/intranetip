<?php
/*
 * 	Using: 
 * 
 * 	Purpose: user profile page for Oaks	
 * 
 * 	Log
 * 
 * 	2019-06-04 [Henry] added CSRF protection
 * 	2017-12-22 [Cameron] copy from index.php, then modify
 * 
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PagePersonalInfo";
$lprofile = new libprofile();

$li = new libuser($UserID);

########## Personal Photo
$personal_photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
$personal_photo_delete = "";
if ($li->PersonalPhotoLink !="")
{
    if (is_file($intranet_root.$li->PersonalPhotoLink))
    {
        $personal_photo_link = $li->PersonalPhotoLink;
        
        if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")) 
          $personal_photo_delete = "<a href=\"javascript:deletePersonalPhoto()\" class=\"tablelink\"> ". $Lang['Personal']['DeletePersonalPhoto'] ." </a>";
    }
}
$update_no = 0;
$Gender0 = ($li->Gender=="M") ? "CHECKED" : "";
$Gender1 = ($li->Gender=="F") ? "CHECKED" : "";

$xmsg = $Lang['General']['ReturnMessage'][$msg];

### Title ###
$TAGS_OBJ[] = array($i_UserProfilePersonal);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($i_UserProfilePersonal);

$linterface->LAYOUT_START($xmsg);

?>

<script language="javascript">
function checkPhotoExt(){
	<?php if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")){ ?>
		if(document.form1.personal_photo && document.form1.personal_photo.value!="" && getFileExtension(document.form1.personal_photo).toLowerCase()!=".jpg"){
			alert("<?=$i_UserPhotoGuide?>");
			return false;
		}
	<?php } ?>
	
    <?php
	    if ($li->RetrieveUserInfoSetting("CanUpdate", "Email"))
	    {
    ?>
     	if(!check_text(form1.UserEmail, "<?php echo $i_alert_pleasefillin.$i_UserEmail; ?>.")) return false;
     	if(!validateEmail(form1.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
    <?php
     	}
    ?>
     
     
    <?php
       	if($li->RetrieveUserInfoSetting("CanUpdate", "Mobile"))
       	{
    ?>
     	if(!check_text(form1.MobileTelNo, "<?php echo $i_alert_pleasefillin.$i_UserMobileTelNo; ?>.")) return false;
    <?php
     	}
    ?>

    <?php
    	if($li->RetrieveUserInfoSetting("CanUpdate", "Address"))
       	{
    ?>
     	if(!check_text(form1.Address, "<?php echo $i_alert_pleasefillin.$i_UserAddress; ?>.")) return false;
    <?php
     	}
    ?>
	
	document.form1.submit();
}

function deletePersonalPhoto() {
	if(confirm("<?=$Lang['Personal']['DeletePersonalPhoto']?>?")){
		window.location = "photo_delete.php?PhotoType=personal&UserID=<?=$UserID?>";
	}
}
</script>

<form name="form1" enctype="multipart/form-data" method="post" action="personal_info_update_amway.php">
	<table class="form_table_v30">
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%">
				<?=$i_UserLogin?>
			</td>
			<td class="tabletext" valign="top" width="70%"><?=$li->UserLogin?></td>
			<td class="tabletext" rowspan="4" valign="top" align="left" nowrap>
				<?=$Lang['Personal']['PersonalPhoto']?><br />
				<img src="<?=$personal_photo_link?>" width="100" height="130"><br><?=$personal_photo_delete?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_UserEnglishName?>
			</td>
			<td class="tabletext" valign="top"><?=intranet_wordwrap($li->EnglishName,20," ",1)?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_UserChineseName?>
			</td>
			<td class="tabletext" valign="top"><?=intranet_wordwrap($li->ChineseName,20," ",1)?></td>
		</tr>
   		<input type="hidden" name="photo" value=""><br />
		
		<?php if($li->RetrieveUserInfoSetting("CanUpdate", "PersonalPhoto")){ $update_no++;?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['Personal']['PersonalPhoto']?>
			</td>
			<td class="tabletextremark" valign="top">
				<input type="file" name="personal_photo" class="file"><br /><?=$i_UserPhotoGuide?>
			</td>
		</tr>
		<?php } else { ?>
      		<input type="hidden" name="personal_photo" value=""><br />
		<?php } ?>	
		
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_UserGender?>
			</td>
			<td class="tabletext" valign="top">
			<?php
				echo $Gender0!=""?$i_gender_male:$i_gender_female;
			?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$Lang['iAccount']['ADACardNo']?>
			</td>
			<td class="tabletext" valign="top"><?=$li->CardID?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_UserMobileTelNo?><? if($li->RetrieveUserInfoSetting("CanUpdate", "Mobile")) {?><span class='tabletextrequire'>*</span><? } ?>
			</td>
			<td class="tabletext" valign="top">
			<?php
			if($li->RetrieveUserInfoSetting("CanUpdate", "Mobile"))
			{
				echo "<input class=\"textboxnum\" type=\"text\" name=\"MobileTelNo\" maxlength=\"20\" value=\"".($li->MobileTelNo)."\">";
			    $update_no++;
		    }
			else
			    echo intranet_wordwrap($li->MobileTelNo,20," ",1);
			?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_UserAddress?><? if($li->RetrieveUserInfoSetting("CanUpdate", "Address")) {?><span class='tabletextrequire'>*</span><? } ?>
			</td>
			<td class="tabletext" valign="top">
			<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Address")){
				$update_no++;
				?>
					<?=$linterface->GET_TEXTAREA("Address", $li->Address);?>
			<?php }
			      else
			        echo nl2br($li->Address);
			?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_UserEmail?> <? if($li->RetrieveUserInfoSetting("CanUpdate", "Email")) {?><span class='tabletextrequire'>*</span><? } ?>
			</td>
			<td class="tabletext" valign="top">
			<?php if($li->RetrieveUserInfoSetting("CanUpdate", "Email")){ 
				$update_no++;
				?>
			 	<input class="textboxtext" type="text" name="UserEmail" size="30" maxlength="50" value="<?=$li->UserEmail?>">
			 	<span class="tabletextremark"><?=$Lang['AccountMgmt']['EmailFieldReminder']?></span>
			<?php
			      }
			      else
			        echo $li->UserEmail;
			?>
			</td>
		</tr>
		
	</table>

<? if($update_no) {?>
<?=$linterface->MandatoryField();?>
<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_save, "button", "checkPhotoExt();");?> 
<?=$linterface->GET_ACTION_BTN($button_reset, "reset");?>
<p class="spacer"></p>
</div>
<? } ?>
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>