<?php
// using : 
/*
 * Modification Log:
 *  2019-06-04 Henry
 * 		- added CSRF protection
 *  Date : 2019-02-22   (Bill)	[2019-0221-0952-39207]
 *       - fixed: not update modified user in INTRANET_USER
 * 	Date : 2015-10-30	(Bill)
 * 		 - support API server for HKEdCity EdConnect
 * 	Date : 2015-10-02 Bill
 * 		 - create file
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libhkedcity.php");
include_once($PATH_WRT_ROOT."includes/libupdatelog.php");

intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$lu = new libuser();
$lhkedcity = new libhkedcity();
$liblog = new libupdatelog();

if($_GET["HKEdCity"] != "")
{
	# Get passed data from $_GET
	$HKEdCityData = getDecryptedText($_GET["HKEdCity"], $lhkedcity->getHKEdCityKey());
	list($code1, $code2, $code3, $receive_userid, $receive_action, $storedUserIP) = explode("||", $HKEdCityData);
	
	# Verification
	// Gen code for Verification
	$curTimestamp = time();
	$prekey = $lhkedcity->getHKEdCityKey("check");
	$curHourCode = $prekey.'_'.date('YmdH', $curTimestamp);
	$curHourCode = getEncryptedText(md5($curHourCode), $prekey);
	
	// Code, IP and Action checking for Security
	if(($curHourCode != $code1 && $curHourCode != $code2 && $curHourCode != $code3) || $storedUserIP != $_SERVER["REMOTE_ADDR"] || $receive_action != "check") {
		header("Location: /");
		exit();
	}
}

/*	
 * Delete HKEdCityUserLogin of current user
 * 	1. Empty $receive_userid - Remove linkage
 * Update HKEdCityUserLogin of current user
 * 	1. Valid $receive_userid
 * 	2. Correct Status
 * 	3. Not duplicate $receive_userid stored in DB
 */
$fieldname = '';
if($receive_userid == "" || ($receive_userid != "" && $lu->Get_User_By_HKEdCityUserLogin($receive_userid) == "" && $receive_action == "check")) {
    $fieldname .= "HKEdCityUserLogin = '$receive_userid', ";
    $fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
    
	$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID='$UserID'";
	$result = $lu->db_db_query($sql);
}

if ($result) {
	$signal = "UpdateSuccess";
	// insert update log
	$RecordDetail = $receive_userid==""? "Remove HKEdCityUserLogin" : "Set HKEdCityUserLogin - '$receive_userid'";
	$liblog->INSERT_UPDATE_LOG("AccountMgmt", "HKEdCity", $RecordDetail, "INTRANET_USER");
}
else {
	$signal = "UpdateUnsuccess";
}
intranet_closedb();

header("Location: hkedcity.php?msg=$signal");
?>