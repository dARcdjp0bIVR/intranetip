<?php

// using : 
/*
 * 2019-06-04 (Henry) added CSRF protection
 * 2018-01-03 (Carlos) Improved password checking.
 * 2015-11-24 (Carlos): 2015-1124-1033-45206 UCCKE would not auto decode percent characters for the Key parameter.
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$linterface = new interface_html("imail_archive.html");
$CurrentPage = "PageResetPassword";

$lauth = new libauth();
$li = new libuser();

/* 
 * some sites would not auto decode percent characters in hyperlink, 
 * and may need to decode twice because first time decode %25 into % itself, second time decode %2F into / and %3D into =.
 */
if(strpos($Key,'%')!==false){
	$Key = urldecode($Key);
	if(strpos($Key,'%')!==false){
		$Key = urldecode($Key);
	}
}

$InfoArr = $lauth->GetInfoFromResetKey($Key);

if(!is_array($InfoArr))
{
	$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['InvalidKey'];
}
else
{
	list($RequestTime, $ParUserLogin) = $InfoArr;

	$ExpireTime = strtotime("+7 day", $RequestTime);
	if(time()>$ExpireTime)
	{
		$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['Expired'];
	}
	
	$li = new libuser("", $ParUserLogin);

	if(!empty($li->UserID))
	{
		$password_change_disabled = !$li->RetrieveUserInfoSetting("CanUpdate", "Password", $li->RecordType);
		if($password_change_disabled)
		{
			$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['CannotUpdatePassword'];
		}
		else if(strtotime($li->LastModifiedPwd) > $RequestTime)
		{
			$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['HasBeenReset'];
		}
		
	}
	else 
	{
		$warning_msg = $Lang['ForgotHashedPassword']['WarnMsgArr']['InvalidKey'];
	}
}

$thisUserType = $li->RecordType;
        		
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;


$SettingArr = $li->RetrieveUserInfoSettingInArrary($SettingNameArr);

$password_change_disabled = !$SettingArr['CanUpdatePassword_'.$thisUserType];
$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6 && $PasswordLength>0)
{
	# at least 6 by default
	$PasswordLength = 6;
}
if($sys_custom['UseStrongPassword'] && ($PasswordLength == '' || $PasswordLength<6)){
	$PasswordLength = 6;
}

### Title ###
$TAGS_OBJ[] = array($Lang['ForgotHashedPassword']['ResetPassword']);

$MODULE_OBJ['title'] = $iAccount;

$xmsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($xmsg);

?>
<script language="javascript">
function checkform(obj){
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>	
      if(!check_text(obj.NewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) return false;
<?php if ($PasswordLength>=6 && !$sys_custom['UseStrongPassword']) { ?>
	
      if (obj.NewPassword.value.length<<?=$PasswordLength?>)
		{
			alert("<?=str_replace("[6]", $PasswordLength, $Lang['PasswordNotSafe'][-2])?>");
			obj.NewPassword.focus();
			return false;
		}
		var re = /[a-zA-Z]/;
		if (!re.test(obj.NewPassword.value))
		{
			alert("<?=$Lang['PasswordNotSafe'][-3]?>");
			obj.NewPassword.focus();
			return false;
		}
		re = /[0-9]/;
		if (!re.test(obj.NewPassword.value))
		{
			alert("<?=$Lang['PasswordNotSafe'][-4]?>");
			obj.NewPassword.focus();
			return false;
		}

<?php } ?>

      if(!check_text(obj.ReNewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_retype; ?>")) return false;
      
      if (obj.NewPassword.value != "" || obj.ReNewPassword.value != "")
      {
      	/*
          if(!checkRegEx(obj.NewPassword.value,"<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) {
              obj.NewPassword.value = "";
              obj.ReNewPassword.value="";
              obj.NewPassword.focus();
              return false;
          }
        */
          if(obj.NewPassword.value!=obj.ReNewPassword.value){
              alert("<?php echo $i_frontpage_myinfo_password_mismatch; ?>");
              obj.NewPassword.value="";
              obj.ReNewPassword.value="";
              obj.NewPassword.focus();
              return false;
          }
      }
<?php if($sys_custom['UseStrongPassword']){ ?>       
    var check_password_result = CheckPasswordCriteria(obj.NewPassword.value,'<?=$li->UserLogin?>',<?=$PasswordLength?>);
	if(check_password_result.indexOf(1) == -1){
		var password_warning_msg = '';
		for(var i=0;i<check_password_result.length;i++){
			password_warning_msg += password_warnings[check_password_result[i]] + "\n";
		}
		alert(password_warning_msg);
		obj.NewPassword.focus();
		return false;
	}
<?php } ?>
}
</script>
<? if (!$warning_msg){ ?>
<table width="100%">
<tr>
	<td class="main_content">
	<form name="form1" method="post" action="login_password_update.php" onsubmit="return checkform(this)">
	<div class="table_board">
		<table class="form_table_v30">
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_frontpage_myinfo_password_new?> <span class='tabletextrequire'>*</span>
			</td>
			<td class="tabletextremark" valign="top">
				<input class="textboxnum" type="password" name="NewPassword" maxlength="20">
				<?php if ($li->RetrieveUserInfoSetting("Enable", "PasswordPolicy")) { ?>
				 (<?=$Lang['PasswordNotSafe'][-2]?>)
				 <? } ?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title">
				<?=$i_frontpage_myinfo_password_retype?> <span class='tabletextrequire'>*</span>
			</td>
			<td class="tabletext" valign="top">
				<input class="textboxnum" type="password" name="ReNewPassword" maxlength="20">
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap">&nbsp;</td>
			<td class="tabletextremark" valign="top"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?></td>
		</tr>
		</table>
		<?=$linterface->MandatoryField();?>
	</div>
	
	<div class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "","submit2") ?>
		<? $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "window.close()") ?>
	<p class="spacer"></p>
	</div>		
	<input type="hidden" name="FromResetPassword" value=1>
	<input type="hidden" name="Key" value="<?=addslashes($Key)?>"> 
	<?php echo csrfTokenHtml(generateCsrfToken());?>
	</form>
	</td>
</tr>
</table>

<? } else {?>
<table wiidth="100%" cellpadding=50>
<tr>
	<td class="tabletext" valign="top" align="center"><?=$warning_msg?></td>
</tr>
</table>
<div class="edit_bottom_v30">&nbsp;</div>
<? } ?>


<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
