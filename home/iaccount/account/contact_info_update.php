<?php
// Using:

/*
 * Modification Log:
 *  2019-06-04 Henry
 * 		- added CSRF protection
 *  2019-02-22  Bill	[2019-0221-0952-39207]
 *   - fixed: not update modified user in INTRANET_USER
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_opendb();

if(!verifyCsrfToken($_POST['csrf_token'],$_POST['csrf_token_key'])){
	No_Access_Right_Pop_Up();
	exit;
}

$li = new libdb();
$lc = new libeclass();
$lu = new libuser($UserID);
// $isStudent=$lu->isStudent();

/*
$lf = new libfilesystem();
if(file_exists($intranet_root."/file/user_info_settings_".$lu->RecordType.".txt"))
{
  $misc = $lf->file_read($intranet_root."/file/user_info_settings_".$lu->RecordType.".txt");
  $line = explode("\n",$misc);
  
  $misca = explode(",",$line[1]);
  array_shift($misca);
}
else
{
  $misc = $lf->file_read($intranet_root."/file/basic_misc.txt");
  $temp = explode("\n",$misc);
  
  $misca = array_fill(0,9,'');
  $misca[8] = $temp[0];
  $misca[0] = $temp[2];
}
*/

$UserEmail = intranet_htmlspecialchars(trim($UserEmail));
$HomeTelNo = intranet_htmlspecialchars(trim($HomeTelNo));
$OfficeTelNo = intranet_htmlspecialchars(trim($OfficeTelNo));
$MobileTelNo = intranet_htmlspecialchars(trim($MobileTelNo));
$FaxNo = intranet_htmlspecialchars(trim($FaxNo));
$ICQNo = intranet_htmlspecialchars(trim($ICQNo));
$Address = intranet_htmlspecialchars(trim($Address));
$Country = intranet_htmlspecialchars(trim($Country));
$URL = intranet_htmlspecialchars(trim($URL));

# Check whether can change the info
# 1. New email does not exists
$failed = false;
if ($UserEmail != "" && $lu->UserEmail != $UserEmail)
{
    if ($lu->isEmailExists($UserEmail))
    {
        $failed = true;
        $signal = "EmailUsed";
    }
    else
    {
        if ($lc->isEmailExistInSystem($UserEmail))
        {
            $failed = true;
            $signal = "EmailUsed";
        }
    }
}

// # retrieve original email
// $sql = "select UserEmail from INTRANET_USER where UserID=$uid";
// $result = $lu->returnVector($sql);
// $ori_UserEmail = $result[0];

$fieldname = "";
if (!$failed)
{
    if ($lu->RetrieveUserInfoSetting("CanUpdate", "HomeTel")) {
        $fieldname .= "HomeTelNo = '$HomeTelNo', ";
    }
    if ($lu->RetrieveUserInfoSetting("CanUpdate", "OfficeTel")) {
        $fieldname .= "OfficeTelNo = '$OfficeTelNo', ";
    }
    if ($lu->RetrieveUserInfoSetting("CanUpdate", "Mobile")) {
        $fieldname .= "MobileTelNo = '$MobileTelNo', ";
    }
    if ($lu->RetrieveUserInfoSetting("CanUpdate", "Fax")) {
        $fieldname .= "FaxNo = '$FaxNo', ";
    }
    if($lu->RetrieveUserInfoSetting("CanUpdate", "Address")) {
        $fieldname .= "Address = '$Address', ";
    }
    if ($lu->RetrieveUserInfoSetting("CanUpdate", "Country")) {
        $fieldname .= "Country = '$Country', ";
    }
    if ($lu->RetrieveUserInfoSetting("CanUpdate", "URL")) {
        $fieldname .= "URL = '$URL',";
    }
    $fieldname .= "DateModified = now(), ModifyBy = '".$_SESSION['UserID']."'";
    
    if ($UserEmail != "") {
        $fieldname .= ", UserEmail = '$UserEmail'";
        $newmail = $UserEmail;
    }
    else {
        $newmail = "";
    }
    
    $sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = '$UserID'";
    $result = $li->db_db_query($sql);
    
    if ($result)
    {
        $lc->eClassUserUpdateInfoIP($lu->UserEmail, $lu->Title, $lu->EnglishName,$lu->ChineseName,$lu->FirstName,$lu->LastName, $lu->NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $ICQNo,$HomeTelNo,$FaxNo,$lu->DateOfBirth,$Address,$Country,$URL,$lu->Info);
        //$lc->eClassUserUpdateInfoIP($UserEmail, $lu->Title, $lu->EnglishName,$lu->ChineseName,$lu->FirstName,$lu->LastName, $lu->NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $ICQNo,$HomeTelNo,$FaxNo,$lu->DateOfBirth,$Address,$Country,$URL,$lu->Info);
        //$lc->eClassUserUpdateInfoIP($ori_UserEmail, $lu->Title, $lu->EnglishName,$lu->ChineseName,$lu->FirstName,$lu->LastName, $lu->NickName, $lu->UserPassword, $lu->ClassName,$lu->ClassNumber, $newmail,$lu->Gender, $ICQNo,$HomeTelNo,$FaxNo,$lu->DateOfBirth,$Address,$Country,$URL,$lu->Info);
        
        $signal = "UpdateSuccess";
    }
    else
    {
        $signal = "UpdateUnsuccess";
    }
}
else
{
    // do nothing
}

intranet_closedb();

header("Location: contact_info.php?msg=$signal");
?>