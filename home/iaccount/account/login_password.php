<?php

// using : 

/************************ Change Log ****************************/
#	Date	:	2019-06-04 (Henry) added CSRF protection
#	Date	:	2018-01-03 (Carlos) Improved password checking.
#	Date	: 	2016-01-06 (Carlos) If current password is same as userlogin, let user change the password even if security policy does not allow to do so.
#	Date	:	2015-12-22 (Bill) Correct password remark	[2015-1222-0959-43206]
#	Date	:	2014-03-04 (Yuen) Enhanced password policy
#	Date	:	2012-06-08 (Carlos) Added password remark
#	Date	:	2011-12-14 [Yuen]
#				introduced password policy 
/********************** end of Change Log ***********************/



$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageLoginPassword";
$lprofile = new libprofile();

$li = new libuser($UserID);

$thisUserType = $li->RecordType;
        		
$SettingNameArr[] = 'CanUpdatePassword_'.$thisUserType;
$SettingNameArr[] = 'EnablePasswordPolicy_'.$thisUserType;
$SettingNameArr[] = 'RequirePasswordPeriod_'.$thisUserType;

//$sys_custom["UpdatePasswordOutSideLink"] = "https://password.lasalle.edu.hk/password.htm";	# customization for a school only (case:L65249)

$SettingArr = $li->RetrieveUserInfoSettingInArrary($SettingNameArr);

$password_change_disabled = !$SettingArr['CanUpdatePassword_'.$thisUserType];
if($li->UserLogin == $li->UserPassword){ # let user change password if current password is same as userlogin as it is unsecure and can easily being hacked
	$password_change_disabled = false;
}

$PasswordLength = $SettingArr['EnablePasswordPolicy_'.$thisUserType];
if ($PasswordLength<6 && $PasswordLength>0)
{
	# at least 6 by default
	$PasswordLength = 6;
}

if($sys_custom['UseStrongPassword'] && ($PasswordLength == '' || $PasswordLength<6)){
	$PasswordLength = 6;
}

$PasswordSafety = $li->CheckPasswordSafe();

$password_warning = $Lang['PasswordNotSafe'][$PasswordSafety];
if ($PasswordSafety==-2)
{
	$password_warning = str_replace("[6]", $PasswordLength, $password_warning);
} elseif ($PasswordSafety==-5)
{
	$password_warning = str_replace("[PERIOD]", $SettingArr['RequirePasswordPeriod_'.$thisUserType], $password_warning);
}

### Title ###
$TAGS_OBJ[] = array($i_UserProfileLogin);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();
if(isset($_SESSION['iAccountChangePasswordReturnMsg'])){
	$xmsg = $_SESSION['iAccountChangePasswordReturnMsg'];
	unset($_SESSION['iAccountChangePasswordReturnMsg']);
}else{
	$xmsg = $Lang['General']['ReturnMessage'][$msg];
}
$linterface->LAYOUT_START($xmsg);

if ($li->LastModifiedPwd!="")
{
	$LastChangeTime = $Lang['General']['LastModified'] . " : ". $li->LastModifiedPwd;
}
?>




<script language="javascript">
function checkform(obj){
	var password_warnings = {};
<?php
	foreach($Lang['AccountMgmt']['PasswordCheckingWarnings'] as $key => $val){
		echo "	password_warnings['".$key."'] = '".addslashes($val)."';\n";
	}
?>
      if(!check_text(obj.OldPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_old; ?>")) return false;
      if(!check_text(obj.NewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) return false;
<?php if ($PasswordLength>=6 && !$sys_custom['UseStrongPassword']) { ?>	
      if (obj.NewPassword.value.length<<?=$PasswordLength?>)
		{
			alert("<?=str_replace("[6]", $PasswordLength, $Lang['PasswordNotSafe'][-2])?>");
			obj.NewPassword.focus();
			return false;
		}
		var re = /[a-zA-Z]/;
		if (!re.test(obj.NewPassword.value))
		{
			alert("<?=$Lang['PasswordNotSafe'][-3]?>");
			obj.NewPassword.focus();
			return false;
		}
		re = /[0-9]/;
		if (!re.test(obj.NewPassword.value))
		{
			alert("<?=$Lang['PasswordNotSafe'][-4]?>");
			obj.NewPassword.focus();
			return false;
		}
<?php } ?>

      if(!check_text(obj.ReNewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_retype; ?>")) return false;
      
      if (obj.NewPassword.value != "" || obj.ReNewPassword.value != "")
      {
      	/*
          if(!checkRegEx(obj.NewPassword.value,"<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) {
              obj.NewPassword.value = "";
              obj.ReNewPassword.value="";
              obj.NewPassword.focus();
              return false;
          }
        */
          if(obj.NewPassword.value!=obj.ReNewPassword.value){
              alert("<?php echo $i_frontpage_myinfo_password_mismatch; ?>");
              obj.NewPassword.value="";
              obj.ReNewPassword.value="";
              obj.NewPassword.focus();
              return false;
          }
      }
<?php if($sys_custom['UseStrongPassword']){ ?>      
    var check_password_result = CheckPasswordCriteria(obj.NewPassword.value,'<?=$li->UserLogin?>',<?=$PasswordLength?>);
	if(check_password_result.indexOf(1) == -1){
		var password_warning_msg = '';
		for(var i=0;i<check_password_result.length;i++){
			password_warning_msg += password_warnings[check_password_result[i]] + "\n";
		}
		alert(password_warning_msg);
		obj.NewPassword.focus();
		return false;
	}
<?php } ?>
}
</script>


<? if (!(isset($password_change_disabled) && $password_change_disabled) || isset($_SESSION['FORCE_CHANGE_PASSWORD'])){ ?>
	
<? if ($sys_custom["UpdatePasswordOutSideLink"]!=""){ ?>
	<br />
	<br />&nbsp; &nbsp; <a href='<?=$sys_custom["UpdatePasswordOutSideLink"]?>' target='PasswordChangeOutside'><?=$Lang['UpdatePasswordOutSide'] ?></a> 
<? } else { ?>
<form name="form1" method="post" action="login_password_update.php" onsubmit="return checkform(this)">
<table class="form_table_v30">
<?php if ($password_warning!="" || isset($_SESSION['FORCE_CHANGE_PASSWORD'])) { ?>
<tr>
	<td valign="top" colspan="2" class="field_title">
		<font color="red"><?=$Lang['PasswordWarning']?><?=$password_warning!=''?'<br />* '.$password_warning:''?></font>
</tr>
	
<?php } ?>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?=$i_frontpage_myinfo_password_old?> <span class='tabletextrequire'>*</span>
	</td>
	<td class="tabletextremark" valign="top" width="70%">
		<input class="textboxnum" type="password" name="OldPassword" maxlength="20"> (<?=$i_UserProfilePwdUse?>)
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?=$i_frontpage_myinfo_password_new?> <span class='tabletextrequire'>*</span>
	</td>
	<td class="tabletextremark" valign="top">
		<input class="textboxnum" type="password" name="NewPassword" maxlength="20"> 
		<?php if ($PasswordLength>=6) { ?>
			<!--(<?=str_replace("[6]", $PasswordLength, $Lang['PasswordNotSafe'][-2])?>)-->
		<? } ?>
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?=$i_frontpage_myinfo_password_retype?> <span class='tabletextrequire'>*</span>
	</td>
	<td class="tabletext" valign="top">
		<input class="textboxnum" type="password" name="ReNewPassword" maxlength="20">
	</td>
</tr>
<tr>
	<td><?=$linterface->MandatoryField()?></td>
	<td class="tabletextremark"><?=$sys_custom['UseStrongPassword']?str_replace("<!--PASSWORD_LENGTH-->",$PasswordLength,implode("<br />\n",$Lang['AccountMgmt']['PasswordRequirements'])):$Lang['AccountMgmt']['PasswordRemark']?> <p><?=$LastChangeTime?></p></td>
</tr>
</table>
    	
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_save, "submit", "","submit2") ?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
<p class="spacer"></p>
</div>		

<input type="hidden" name="FromLogin" value="<?=$FromLogin?>" />
<?php echo csrfTokenHtml(generateCsrfToken());?>
</form>
<? 
	} 

} else {?>
<table class="form_table_v30">
<tr>
	<td class="tabletext" valign="top" align="center"><?=$i_UserProfile_Change_Notice?> </td>
</tr>
</table>
<? } ?>


<?php
intranet_closedb();
if (!(isset($password_change_disabled) && $password_change_disabled) || isset($_SESSION['FORCE_CHANGE_PASSWORD']))
	print $linterface->FOCUS_ON_LOAD("form1.OldPassword");	
$linterface->LAYOUT_STOP();
?>
