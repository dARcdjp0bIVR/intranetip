<?php
/*
 *  2019-06-04 Henry
 * 		- added CSRF protection
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPage = "PageMessage";
$lprofile = new libprofile();

$li = new libuser($UserID);

/*
$isStudent = $li->isStudent();

$lf = new libfilesystem();
if(file_exists($intranet_root."/file/user_info_settings_".$li->RecordType.".txt")){
  $misc = $lf->file_read($intranet_root."/file/user_info_settings_".$li->RecordType.".txt");
  $line = explode("\n",$misc);
  
  $misca = explode(",",$line[2]);
  array_shift($misca);
}
*/
/*
$xmsg = "";
switch($msg){
     case 1: $xmsg = $i_con_msg_add; break;
     case 2: $xmsg = $i_con_msg_update; break;
     case 3: $xmsg = $i_con_msg_delete; break;
     case 4: $xmsg = $i_con_msg_email; break;
     case 5: $xmsg = $i_con_msg_password1; break;
     case 6: $xmsg = $i_con_msg_password2; break;
     case 7: $xmsg = $i_con_msg_email1; break;
     case 8: $xmsg = $i_con_msg_email2; break;
     case 9: $xmsg = $i_con_msg_photo_delete; break;
}
*/

### Title ###
$TAGS_OBJ[] = array($i_UserProfileMessage);
$MODULE_OBJ = $lprofile->GET_MODULE_OBJ_ARR();

$xmsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($xmsg);
?>

<form name="form1" method="post" action="message_update.php">

<table class="form_table_v30">	<tr>
		<td valign="top" nowrap="nowrap" class="field_title">
			<?=$i_UserProfileMessage?>
		</td>
		<td class="tabletext" valign="top" width="70%">
		<?php
		      if($li->RetrieveUserInfoSetting("CanUpdate", "Message"))
		        echo $linterface->GET_TEXTAREA("Info", $li->Info);
		      else
		        echo nl2br($li->Info);
		?>		
		</td>
	</tr>
</table>
    		
<? if($li->RetrieveUserInfoSetting("CanUpdate", "Message")) {?>    	
<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_save, "submit") ?> 
<?= $linterface->GET_ACTION_BTN($button_reset, "reset") ?>
<p class="spacer"></p>
</div>
<? } ?>    		
<?php echo csrfTokenHtml(generateCsrfToken());?>	
</form>

<?php
intranet_closedb();
print $linterface->FOCUS_ON_LOAD("form1.Info");
$linterface->LAYOUT_STOP();
?>
