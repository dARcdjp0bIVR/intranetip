<?php 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/json.php");
$jsonObj = new JSON_obj();

$data = $jsonObj->decode(file_get_contents('php://input'));

if(!isset($data['t']))
{
	header("HTTP/1.1 403");
	exit();
}
include_once("../includes/libdb.php");
include_once("../includes/global.php");
include_once("../includes/hkssf/libhkssf.php");


intranet_opendb();

$libhkssf = new libhkssf();


if(!$libhkssf->isAvaliable())
{
	header("HTTP/1.1 403");
	exit();
}
switch($libhkssf->get($data['t']))
{
	case 'reg': // Create Request map 
		$libhkssf->checkParams($data, array('request'));
		
		$r = $libhkssf->get($data['request']);
		$rs = $libhkssf->saveRequest($r);
		
		$rs = array('result' => $rs);
		break;
		
	case 'check' : // Test eClass Connection 
		$libhkssf->checkParams($data, array('answer','match'));
		
		$s = $libhkssf->get($data['answer']);
		
		$result = $libhkssf->signRequest($s,$data['match']);
		
		$rs = array('response'=> $result);
		break;
		
	case 'fetchCL' :  // get Class List
		$libhkssf->checkParams($data, array('no','diff'));
		
		$s = $libhkssf->get($data['no']);
		$rs = $libhkssf->get($data['diff']);
		$request_id = $libhkssf->matchRequest($s,$data['diff']);
		include_once("../includes/form_class_manage.php");
		
		$year = ($year=="") ? Get_Current_Academic_Year_ID() : $year;
		$fcm = new form_class_manage();
		
		
		$raw = $jsonObj->encode($fcm->Get_Class_List_By_Academic_Year($year));
		$filepath = $libhkssf->CreateDataInput($raw);
		$ClassList = $libhkssf->putData($filepath, $s-$rs);
		
		
		$libhkssf->DeleteDataInput($filepath);
		
		$result = $libhkssf->closeRequest($request_id);
		$rs = array('response'=> $ClassList);
		
		
		break;
	case 'fetchSL' :  // get Class List
		$libhkssf->checkParams($data, array('no','diff','target','l'));
	
		$s = $libhkssf->get($data['no']);
		$rs = $libhkssf->get($data['diff']);
		$locale = $libhkssf->get($data['l']);
		$YearClassID = $libhkssf->get($data['target']);
		$request_id = $libhkssf->matchRequest($s,$data['diff']);
		$intranet_session_language = $locale;
		include_once("../includes/hkssf/form_class_manage_hkssf.php");
	
		$fcm = new form_class_manage_hkssf();
		
		$raw = $jsonObj->encode($fcm->Get_Student_By_Class($YearClassID));
		$filepath = $libhkssf->CreateDataInput($raw);
		$StudentList = $libhkssf->putData($filepath, $s-$rs);
	
		$libhkssf->DeleteDataInput($filepath);
		$result = $libhkssf->closeRequest($request_id);
		$rs = array('response'=> $StudentList);
	
		break;
	case 'fetchIU' : // get selected users info 
	
		$libhkssf->checkParams($data, array('no','diff','IU'));
		
		$s = $libhkssf->get($data['no']);
		$rs = $libhkssf->get($data['diff']);
// 		$locale = $libhkssf->get($data['l']);
		
		$filepath = $libhkssf->CreateDataInput($data['IU']."\n");
		$UserIDs = $libhkssf->getData($filepath);
		$libhkssf->DeleteDataInput($filepath);
		
		if(!is_array($UserIDs))
		{
			$UserIDs = array($UserIDs);
		}
		
		$request_id = $libhkssf->matchRequest($s,$data['diff']);
		
		include_once("../includes/hkssf/form_class_manage_hkssf.php");
		$fcm = new form_class_manage_hkssf();
		
		$raw = $jsonObj->encode($fcm->HKSSF_fetchStudentsInfo($UserIDs));
		$filepath = $libhkssf->CreateDataInput($raw);
		
		$StudentList = $libhkssf->putData($filepath, $s-$rs);
		
		$libhkssf->DeleteDataInput($filepath);
		
		$result = $libhkssf->closeRequest($request_id);
		$rs = array('response'=> $StudentList);
		
		break;
		
	default :
		header("HTTP/1.1 403");
		exit();
		break;
}


intranet_closedb();
header('Content-Type: text/json; charset=utf-8');
echo $jsonObj->encode($rs);
exit();
?>
