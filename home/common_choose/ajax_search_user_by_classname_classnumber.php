<?php
// using 
################## Change Log [Start] ##############
#	Date:	2018-02-23 	Isaac
#           Added $excludeUserIDsStr to filter excluded users from the result list
#
#	Date:	2018-02-23 	Isaac
#			Create this File with the reference of eNotice
################## Change Log [End] ################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Get data
$YearID = $_REQUEST['YearID'];
//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);
if($_GET['excludeUserIDAry']) {
    $excludeUserIDsStr  = str_replace(",","','",$_GET['excludeUserIDAry']);
}
//$excludeCurrentYearSearch = 0;

$libdb = new libdb();

$excludeCurrentYearSearch = (isset($_REQUEST['excludeCurrentYearSearch']))? $_REQUEST['excludeCurrentYearSearch'] : 0;

$ActiveYearID = $_REQUEST['ActiveYearID'];
if (isset($ActiveYearID) && !empty($ActiveYearID)) {
	$currAcademicYearID = $ActiveYearID;
}
else {
	$currAcademicYearID = Get_Current_Academic_Year_ID();
}
	
	$sql = "SELECT
				iu.UserID,
				".getNameFieldByLang()." as Name,
				IF (yc.YearClassID = '' OR yc.YearClassID IS NULL, iu.ClassName, yc.ClassTitleEN) as ClassName,
				IF (yc.YearClassID = '' OR yc.YearClassID IS NULL, iu.ClassNumber, ycu.ClassNumber) as ClassNumber
			FROM
				INTRANET_USER AS iu
                LEFT JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID = iu.UserID)
                LEFT JOIN YEAR_CLASS AS yc ON (ycu.YearClassID = yc.YearClassID AND AcademicYearID = '" . $currAcademicYearID . "')";
	$sql .= " WHERE iu.RecordStatus = 1 AND iu.RecordType = 2 AND iu.USERID NOT IN ('".$SelectedUserIDsStr."','".$excludeUserIDsStr."') AND (";
	
	$pos = strpos($_REQUEST['q'], " ");
	if ($pos !== false) {
		$tmp = explode(" ", $libdb->Get_Safe_Sql_Like_Query((urldecode($_REQUEST['q']))));
		$SearchValues[0] = strtolower(trim($tmp[0]));
		$SearchValues[1] = trim(str_replace($tmp[0], "", $libdb->Get_Safe_Sql_Like_Query((urldecode($_REQUEST['q'])))));
	}
	else {
		$SearchValue = trim($libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q'])))));
	}
	if ($SearchValues > 1)
	{
		if ($excludeCurrentYearSearch) {
			$currentYearSearchSql = '';
		}
		else {
			$currentYearSearchSql = " OR CONCAT(LOWER(iu.ClassName), iu.classNumber) LIKE '%".$SearchValues[0]."%'";
		}
					
		$sql .= " (CONCAT(LOWER(yc.ClassTitleEN), ycu.classNumber) LIKE '%".$SearchValues[0]."%'
			OR CONCAT(LOWER(yc.ClassTitleB5), ycu.classNumber) LIKE '%".$SearchValues[0]."%'
			$currentYearSearchSql
		)
		AND (iu.ChineseName LIKE'%".$SearchValues[1]."%'
		OR iu.EnglishName LIKE '%".$SearchValues[1]."%')";
	}
	else
    {
		if ($excludeCurrentYearSearch) {
			$currentYearSearchSql = '';
		}
		else {
			$currentYearSearchSql = " OR CONCAT(LOWER(iu.ClassName), iu.classNumber) LIKE '%".$SearchValue."%'";
		}
		
		$sql .= " CONCAT(LOWER(yc.ClassTitleEN), ycu.classNumber) LIKE '%".$SearchValue."%'
				OR CONCAT(LOWER(yc.ClassTitleB5), ycu.classNumber) LIKE '%".$SearchValue."%'
			  	OR iu.ChineseName LIKE '%".$SearchValue."%'
			  	OR iu.EnglishName LIKE '%".$SearchValue."%'
				$currentYearSearchSql";
	}
	$sql .= " )";
    $sql .= "GROUP BY iu.UserID ORDER BY iu.ClassName ASC, iu.ClassNumber ASC LIMIT 10";
	$result = $libdb->returnResultSet($sql);

	$returnString = '';
	foreach((array)$result as $_StudentInfoArr)
	{
		$_UserID = $_StudentInfoArr['UserID'];
		$_Name = $_StudentInfoArr['Name'];
		$_ClassName = $_StudentInfoArr['ClassName'];
		$_ClassNumber = $_StudentInfoArr['ClassNumber'];
		if (empty($_ClassName)) {
			$_thisUserNameWithClassAndClassNumber = $_Name.'';
		}
		else {
			$_thisUserNameWithClassAndClassNumber = $_Name.' ('.$_ClassName.'-'.$_ClassNumber.')';
		}
// 		$_thisUserNameWithClassAndClassNumber = convert2Big5($_thisUserNameWithClassAndClassNumber);

		$returnString .= $_thisUserNameWithClassAndClassNumber.'|'.'|'.$_UserID."\n";
// 		$returnString = convert2Big5($returnString);
	}

intranet_closedb();
echo $returnString;
?>