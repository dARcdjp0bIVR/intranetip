<?php
# modifying by : 

################################################
# used in: 
#	- [Student Management > Medical > Settings > new] /home/web/eclass40/intranetIP25/home/eAdmin/StudentMgmt/medical/settings/accessRight.php
#	- [Student Attendance > Other feature > outing record > new] /home/eAdmin/StudentMgmt/attendance/other_features/outing/new.php
#	- [eReportCard (Rubrics) > Admin Group] /home/eAdmin/StudentMgmt/reportcard_rubrics/settings/admin_group/new_step2.php
#	- [Repair System > Management Group] /home/eAdmin/ResourcesMgmt/RepairSystem/settings/mgmt_group/member_new.php
#	- [eNotice > Parent-signed] /home/eAdmin/StudentMgmt/notice/ajax_loadAudience.php
#	- [eNotice > Student-signed] /home/eAdmin/StudentMgmt/notice/student_notice/ajax_loadAudience.php
#	- [eNotice > Payment Notice] /home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice_new.php
#	- [Message Center > Parent Notification(iPhone Apps)] /home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/new.php
#	- [Student Management > Notice > Write message]/home/web/eclass40/intranetIP25/home/eAdmin/StudentMgmt/medical/message/writeMessage.php (By thickbox iframe)
# 	- [Resources Management > Document Routing]/home/eAdmin/ResourcesMgmt/DocRouting/
############ Change Log [start] #################
#
#   Date:   2020-05-04  Bill    [2020-0407-1445-44292]
#           - support class teacher audience limit checking + UI update ($sys_custom['eNotice']['ClassTeacherSelectOwnClassStudentOnly'])
#
#   Date:   2019-07-08 Cameron
#           filter medical group category if $sys_custom['medical']['Handover'] && $filterMedicalGroup
#
#   Date:   2019-01-02 Anna
#           $sys_custom['DHL'] added $_GET['DHLSelectGroup'] 
#
#	Date:	2017-07-03	Carlos
#			$sys_custom['DHL'] added $_GET['SelectGroupOnly'] for allow select and add departments(actually it is groups with same department name) only.
#
#	Date:	2017-06-20	Icarus	
#			Changed the list user id of $exclude_user_id_list (param from edit.php) to $exclude_user_id_ary
#
#	Date:	2017-04-20	Carlos $sys_custom['DHL'] Added DHL organizations selection target.
#
#	Date:	2017-01-11	Ivan [ip.2.5.8.1.1] deployment testing defect 3141
#			Fixed: failed to view class students for class teacher mode 
#
#	Date:	2016-03-11	Ivan [ip.2.5.7.4.1]
#			Added param $filterAppStudentByStudentApp to handle message center student app logic
# 
#	Date:	2016-01-26	Kenneth 
#			Added parameter '$custFollowUpJs', which call js: commonChooseFollowUpAction() in outer window
#
#	Date:	2015-09-17	Ivan [G85449] [ip.2.5.6.10.1.0]
#			Fixed: fail to initial libeClassApp object if the client has purchased Teacher App only
#
#	Date:	2015-08-03 	Ivan
#			Added logic to support send push message to logout-ed teacher function. Can search code around function "getTeacherWithTeacherUsingTeacherApp()"
#
#	Date:	2015-05-04	Carlos
#			Added $FromDR=1 with flag $sys_custom['DocRouting']['SpecialGroup'] for Document Routing to enable selecting users in special groups regardless the user account status.
#
#	Date:	2015-01-23	Ivan
#			fix: cannot select specific user from group in eNotice and eCircular
#
#	Date:	2014-12-10	Roy
#			fix: wrong filtering result for class teacher and club/event PIC
#
#	Date:	2014-10-27	Roy
#			Added $filterECAActivity to show ECA activity filtering radio button
#
#	Date:	2014-10-27	Roy
#			Fix: when student is selected, cannot select classes
#
#	Date:	2014-10-24	Roy
#			Added $filterClubAndEventInCharge to show teacher with logged in Teacher App's teachers only
#
#	Date:	2014-07-10	Ivan
#			Added $filterAppTeacher to show teacher with logged in Teacher App's teachers only
#
#	Date:	2014-06-30	Pun
#			Added from_medical_write_message for medical send message
#
#	Date:	2014-06-30	Pun
#			Added $filterUser(array) for showing the users only in this array
#
#	Date:	2014-03-28	Pun
#			Added $from_thickbox for showing this page from an thickbox iframe 
#
#	Date:	2014-02-11	Henry
#			Added $DisplayInternalRecipientGroup to show iMail Internal Recipient Group selection, the first character of index value is "i". eg, i21 
#
#	Date:	2013-12-18	Ivan
#			Added $filterAppStudent to show student with parent's logged in the eClassApp only
#
#	Date:	2013-10-09	Roy
#			Add $UserRecordStatus parameter to getParent()
#
#	Date:	2013-10-02	Henry
#			Added $Disable_AddGroup_Button to disable/hide the group add button
#
#	Date:	2013-09-12	Roy
#			check $plugin['ASLParentApp'] to switch app parents filter checkbox
#
#	Date:	2013-09-12	Roy
#			added parameter $filterNotAppParentOption to add not app parents filter checkbox in select parents
#
#	Date:	2013-06-14	Ivan
#			added parameter $exclude_user_id_ary to exclude certain users in the selection
#
#	Date:	2013-05-21	Ivan
#			added parameter $excluded_type to exclude "Alumni" selection for Document Routing 
#
#	Date:	2011-12-28	Carlos
#			add filterAppParent	parameter for libuser->getParent() to filter parents that have purchased parent app 
#
#	Date:	2011-12-14	YatWoon
#			add UserRecordStatus parameter [Case#2011-1202-1604-57066]
#
#	Date:	2011-10-06 Yuen
#			support different form name in opener if query string OpenerFormName is given
#
#	Date:	2011-09-02 Yuen
#			auto select the first category to save user's time when document is ready using JS
#	
#	Date:	2011-02-10 Kenneth Chung
#			1. Modify JS function AddOptions, exclude the duplicate user id before add back the option to parent window
#			2. Modify JS function AddOptions, run parent window function Update_Auto_Complete_Extra_Para if it exists for auto complete paramenter
#	
#	Date:	2010-12-20 YatWoon
#			update getStudentNameListWClassNumberByClassName(),getClassStudentList() , just retrieve "Approved Student"
#
############ Change Log [end] #################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
if($sys_custom['DHL']){
	include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
}
					
intranet_auth();
intranet_opendb();


############################
#	variables description
############################
#
#	$page_title: array val or $Lang['CommonChoose']['PageTitle'][$page_title], display top-left title
#	$permitted_type: which identity can be selected, e.g. "1,2,3" //accept all type of user
#	$DisplayGroupCategory:  e.g. 1 - display, 0 - not display	(default off)
#	$UserRecordStatus: e.g. "1,2" (default 1)
#	$FromDR : 1 to mark this page is opened from Document Routing to have enable customized functions
#   $Disable_AddGroup_Button: 1 - disable/hide add button in group selection, 0 - enable (default)
#	
############################
#
#	Since user may add a Group directly from the selection, a common function is written to retrieve the UserID List of all selected items.
#	You may call Get_User_Array_From_Common_Choose() in lib.php. 
#
###########################

$UserRecordStatus = $UserRecordStatus ? $UserRecordStatus : 1;
$filterAppStudent = ($filterAppStudent)? $filterAppStudent : 0;
$filterAppTeacher = ($filterAppTeacher)? $filterAppTeacher : 0;
$filterAppParent = ($filterAppParent)? $filterAppParent : 0;
$filterAppStudentByStudentApp = ($filterAppStudentByStudentApp)? $filterAppStudentByStudentApp : 0;
$includeNoPushMessageUser = ($includeNoPushMessageUser)? $includeNoPushMessageUser : 0;
$clubOrEvent = ($clubOrEvent) ? $clubOrEvent : "C";
$custFollowUpJs = ($_GET['custFollowUpJs'])? 1 : 0;
$logToDB = ($_GET['logToDB'])? 1 : 0;

if ($plugin['ASLParentApp'] || $plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
	$filterNotAppParentOption = $_GET['filterNotAppParentOption'] ? $_GET['filterNotAppParentOption'] : '';
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	$libeClassApp = new libeClassApp();
	
} else {
	$filterNotAppParentOption = '';
}

if (!isset($_GET['exclude_user_id_ary'])) {
	################## get the list of existing user id of staff and change to array
	$exclude_user_id_ary = explode(",", $exclude_user_id_list);
	$exclude_user_id_ary = array_remove_empty($exclude_user_id_ary);
}
else {
	$exclude_user_id_ary = $_GET['exclude_user_id_ary'] ? $_GET['exclude_user_id_ary'] : '';
	if (is_array($exclude_user_id_ary)) {
		$numOfUser = count($exclude_user_id_ary);
		for ($i=0; $i<$numOfUser; $i++) {
			$exclude_user_id_ary[$i] = str_replace('U', '', $exclude_user_id_ary[$i]);
		}
	}
}

$MODULE_OBJ['title'] = $Lang['CommonChoose']['PageTitle'][$page_title];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
$permitted = explode(",",$permitted_type);
$excluded_type_arr = explode(",",$excluded_type);
$luser = new libuser();

if($sys_custom['DHL']){
	$libdhl = new libdhl();
	$reset_selection_js = "ResetSelections();";
	$select_group_only = $_GET['SelectGroupOnly'] == 1; // only allow to add [Group]/[Department] selections, hide the add button at category(i.e. Division) level 
	$DHLSelectGroup = $_GET['DHLSelectGroup'] == 1;
	$isAdmin = $_GET['isAdmin'] == 1; // allow admin select all group
	$select_staff_only= $_GET['SelectStaffOnly'] == 1;
// 	$DepartmentID = $_GET['DepartmentID'];
// 	$DivisionID = $_GET['DivisionID'];
// 	$CompanyID = $_GET['CompanyID'];
}

$li = new libgrouping();
$lclass = new libclass();
$teaching=0;
if(($CatID < 0 || $CatID[0] == 'i') && !$sys_custom['DHL'])
{
	 unset($ChooseGroupID);
     switch($CatID)
     {
	     case -2 : $ChooseGroupID[0]=2; $teaching=0;break;
	     case -3 : $ChooseGroupID[0]=3; $teaching=0;break;
	     
	     case -99 : $ChooseGroupID[0]=1; $teaching=1;break;
	     case -100: $ChooseGroupID[0]=1; $teaching=0;break;
	     default:$ChooseGroupID[0]=1; $teaching=1;
	 }
}



$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=\"CatID\" id=\"CatID\" onChange=\"".$reset_selection_js."checkOptionNone(this.form.elements['ChooseGroupID[]']);this.form.submit();\">\n" : "<select name=\"CatID\" id=\"CatID\" onChange=\"".$reset_selection_js."this.form.submit();\">\n";
$x1 .= "<option value=\"0\"> </option>";

##### Identity selection
if (isset($permitted) && !empty($permitted) && isset($permitted_type) && $permitted_type!='')
{
    if($sys_custom['DHL'] && !in_array('COM',$excluded_type_arr)){
        $x1 .= "";
    }else{
        $x1 .= "<optgroup label='". $Lang['CommonChoose']['IdentitySelectOptionLabel'] ."'>";
        
    }
// 	$x1 .= "<optgroup label='". $Lang['CommonChoose']['IdentitySelectOptionLabel'] ."'>"; 

	if (in_array(2,$permitted) && !in_array(2, (array)$excluded_type_arr) && !$sys_custom['DHL'])
	$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
	
	if (in_array(3,$permitted) && !in_array(3, (array)$excluded_type_arr) && !$sys_custom['DHL'])
	$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
	    
	if (in_array(1,$permitted) && !in_array(1, (array)$excluded_type_arr))
	{
	    if($sys_custom['DHL']){ 
	        if(in_array('COM',$excluded_type_arr)){
	            $x1 .="<option value=-1 ".(($CatID==-1)?"SELECTED":"").">".$Lang['DHL']['Staff']."</option>\n";
	        }
		}else{
// 		if(!$sys_custom['DHL']){
			$x1 .="<option value=-99 ".(($CatID==-99)?"SELECTED":"").">$i_teachingStaff</option>\n";
			$x1 .="<option value=-100 ".(($CatID==-100)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
		}
	}
	
	if ($special_feature['alumni'] && !in_array(4, (array)$excluded_type_arr) && !$sys_custom['DHL'])
	{
		$x1 .= "<option value=-4 ".(($CatID==-4)?"SELECTED":"").">".$Lang['iMail']['FieldTitle']['Alumni']."</option>\n";
	}
}


##### Group Category selection
if($DisplayGroupCategory && !$sys_custom['DHL'])
{
    if ($sys_custom['medical']['Handover'] && $filterMedicalGroup) {
        include_once ($PATH_WRT_ROOT . "includes/cust/medical/libMedical.php");
        $objMedical = new libMedical();
        $medicalGroupCategories = $objMedical->getMedicalGroupCategory();
        $medicalGroupCategoryIDAry = Get_Array_By_Key($medicalGroupCategories,'GroupCategoryID');
     }
     else {
         $filterMedicalGroup = false;
     }
     
	$filterCatIDAry = array();
	if ($filterClubAndEventInCharge) {
		$filterCatIDAry[] = 5;
	}
	if ($filterClassTeacherClass) {
		$filterCatIDAry[] = 3;
	}
	$x1 .= "<optgroup label='". $Lang['CommonChoose']['GroupCategorySelectOptionLabel'] ."'>";
	$lgroupcat = new libgroupcategory();
	$cats = $lgroupcat->returnAllCat();
	for ($i=0; $i<sizeof($cats); $i++)
	{
	     list($id,$name) = $cats[$i];
	     
	     if (($filterClubAndEventInCharge || $filterClassTeacherClass) && !in_array($id, $filterCatIDAry)) {
	     	// skip other group catergory except ECA
	     	continue;
	     }
	     if ($filterMedicalGroup && !in_array($id, (array)$medicalGroupCategoryIDAry)) {
	        // skip non-medical group    
	        continue;
	     }
	     if ($id!=0)
	     {
	         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
	     }
	}
	$x1 .= "</optgroup>";
	
}

##### Internal Recipient Group selection
if($DisplayInternalRecipientGroup && !$sys_custom['DHL']){
	//get the Internal Recipient Group
	$sql = "	SELECT 
				AliasID, AliasName 
			FROM 
				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL 
			WHERE 
				OwnerID = $UserID 
			ORDER BY 
				AliasName
		";
	$alias_array2 = $li->returnArray($sql,2);
	if($alias_array2){
		$x1 .= "<optgroup label='". $Lang['CommonChoose']['InternalRecipientGroupSelectOptionLabel'] ."'>";
		for ($i=0; $i<count($alias_array2); $i++)
		{
			list ($id, $name) = $alias_array2[$i];
			if ($CatID == 'i'.$id)
			{
				$sel_str = " selected='selected' ";	
			} else {
				$sel_str = " ";	
			}	
			$x1 .= "<option value='i".$id."' $sel_str>$name</option>\n";
		}
		$x1 .= "</optgroup>";
	}
}

if($sys_custom['DHL'] && !in_array('COM',$excluded_type_arr)){
    // get default department/divison/company info
//     $fiterAry['DepartmentID'] = $DepartmentID==''? '':$DepartmentID;
//     $fiterAry['DivisionID'] = $DivisionID==''? '':$DivisionID;
//     $fiterAry['CompanyID'] = $CompanyID==''? '':$CompanyID;
//     $fiterAry = array_filter($fiterAry);
//     $DeafultInfo = $libdhl->getDepartmentRecords($fiterAry);
//     $AssocDeafultInfoAry = BuildMultiKeyAssoc($DeafultInfo,array('CompanyID','DivisionID','DepartmentID'),'','1','1');
//     foreach ($AssocDeafultInfoAry as $defaultCompanyID=>$DivisionInfoAry){
        
//         foreach ($DivisionInfoAry as $defaultDivisionID => $DepartmentInfoAry){
//             foreach($DepartmentInfoAry as $defaultDepartmentID => $InfoAry){
                
//             }
            
//         }
//     }
    
    $PICInfo = $libdhl->getPICResponsInfo();
    $displayCompanyIDAry = $PICInfo['Company'];
    $displayDivisionIDAry = $PICInfo['Division'];
    $displayDepartmentIDAry = $PICInfo['Department'];
//     array('picView'=>1)
	$company_records = $libdhl->getCompanyRecords();
	$company_name_field = Get_Lang_Selection("CompanyNameChi","CompanyNameEng");
	$x1 .= "<optgroup label=\"".str_pad(' ',10,'-',STR_PAD_LEFT).$Lang['DHL']['Company'].str_pad(' ',10,'-',STR_PAD_RIGHT)."\">";
		for($i=0;$i<count($company_records);$i++){
		    $company_val = $company_records[$i]['CompanyID'];
		    $company_text = $company_records[$i][$company_name_field];
		    
		    if($isAdmin || in_array($company_val,$displayCompanyIDAry)){
		        $selected = $CatID == 'COM'.$company_val || $company_val==$defaultCompanyID;
		        $x1 .= '<option value="COM'.$company_val.'"'.($selected?' selected="selected"':'').' '.($company_records[$i]['Description']!=''?' title="'.intranet_htmlspecialchars($company_records[$i]['Description']).'" ':'').'>'.intranet_htmlspecialchars($company_text).'</option>';		        
		    }			
		}

	$x1 .= "</optgroup>";
}

$x1 .= "</select>";

$filerGroupAry = array();
$enrolGroupStudentIdAry = array();
$eventParticipantIDAry = array();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();
if ($filterClubAndEventInCharge) {
	$managedEnrolGroupIDs = $libenroll->Get_PIC_Club($UserID);
	$managedEventIDs = $libenroll->Get_PIC_Activity($UserID);
//	$managedEventIDs = $libenroll->Get_PIC_Event($UserID);

	// student list that in user in-charged clubs
	$enrolGroupStudentIdAry = $libenroll->Get_Club_Student_List($managedEnrolGroupIDs, 2);

	foreach ((array)$managedEnrolGroupIDs as $managedEnrolGroupID) {
		$tempGroupID = $libenroll->GET_GROUPID($managedEnrolGroupID);
		if (!in_array($tempGroupID, $filerGroupAry)) {
			$filerGroupAry[] = $tempGroupID;
		}
	}
	
} else {
	// get all events for not filtering event PIC case
	$events = $libenroll->GET_ADMIN_EVENTINFO_LIST();
	$tempAry = BuildMultiKeyAssoc($events, '', 'EnrolEventID', $SingleValue = 1, $BuildNumericArray = 1);
	$managedEventIDs = $tempAry[''];
}

// get event paticipants
foreach ((array)$managedEventIDs as $managedEventID) {
	$eventParticipantInfoAry = $libenroll->Get_Activity_Participant_Info($managedEventID);
	
	foreach ((array)$eventParticipantInfoAry as $eventParticipantInfo) {
		$eventParticipantIDAry[] = $eventParticipantInfo['UserID'];
	}
}

$classGroupStudentIdAry = array();
$yearClassIdAry = array();
if ($filterClassTeacherClass) {
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	include_once($PATH_WRT_ROOT."includes/libgrouping.php");
	$form_class_manage = new form_class_manage();
	$libgrouping = new libgrouping();
	$classAry = $form_class_manage->Get_Class_Teacher_Class($UserID);
	foreach ((array)$classAry as $classInfo) {
		//$filerGroupAry = $classInfo['GroupID'];
		$filerGroupAry[] = $classInfo['GroupID'];
        $yearClassIdAry[] = $classInfo['ClassID'];  // [2020-0407-1445-44292]
	}
	
	$classGroupStudentAry = $libgrouping->returnGroupUsers($filerGroupAry);
	foreach ((array)$classGroupStudentAry as $classGroupStudent) {
		$classGroupStudentIdAry[] = $classGroupStudent["UserID"];
	}
}

if($sys_custom['DHL'] && preg_match('/^COM\d+$/',$CatID))
{	
  
    $selected_company_id = $libdhl->cleanTargetSelectionData($CatID, 'COM');
	$selected_division_id_ary = isset($ChooseDivisionID)? $libdhl->cleanTargetSelectionData($ChooseDivisionID,'DIV') : array();	
	
	$filterPIC = 0;
	if(!$isAdmin){	   
	    $selected_division_id_ary = $displayDivisionIDAry;
	    $filterPIC = 1;
	}

	$x2 = $libdhl->getDivisionSelection("ChooseDivisionID[]", "ChooseDivisionID[]", $selected_division_id_ary, $___isMultiple=true, $___hasFirst=false, $___firstText='', $___tags=' size="10" ', $___noOptgroupLabel=true, $selected_company_id, 'DIV',$filterPIC);

}else if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=10 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
        $GroupCatID = $row[$i][0];
        $GroupCatName = $intranet_session_language =="en" ? $row[$i][1] : $row[$i]['TitleChinese'];
        
        if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
        	if (!in_array($GroupCatID, $filerGroupAry)) {
        		continue;
        	}
        }
        
        $x2 .= "<option value=$GroupCatID";
        for($j=0; $j<sizeof($ChooseGroupID); $j++){
        	$x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
        }
        $x2 .= ">$GroupCatName</option>\n";
     }
     
     # choose enrol event
     if ($CatID == 5 && $clubOrEvent == 'E') {
     	$x4  = "<select name=ChooseEventID[] size=10 multiple>\n";
     	for ($i=0; $i<sizeof($managedEventIDs); $i++) {
     		$thisEventID = $managedEventIDs[$i];
			$thisEventInfo = $libenroll->GET_EVENTINFO($thisEventID);
			
			$x4 .= "<option value=$thisEventID";
	        for($j=0; $j<sizeof($ChooseEventID); $j++){
	        	$x4 .= ($thisEventID == $ChooseEventID[$j]) ? " SELECTED" : "";
	        }
			$x4 .= ">".$thisEventInfo["EventTitle"]."</option>\n";
     	}
     }
     
}

if($sys_custom['DHL'] && preg_match('/^COM\d+$/',$CatID) && isset($ChooseDivisionID)){
	$selected_division_id = $libdhl->cleanTargetSelectionData($ChooseDivisionID, 'DIV');
	$selected_department_id_ary = isset($ChooseDepartmentID)? $libdhl->cleanTargetSelectionData($ChooseDepartmentID,'DEP') :array();
	
	$depVal = $select_group_only? '':'DEP';
	$filterPIC = 0;
	if(!$isAdmin){
	    $filterPIC = 1;	   
	    $selected_department_id_ary = $displayDepartmentIDAry;
// 	    $depVal = $DHLSelectGroup? 'DEP':$depVal;
	}
	$depVal = $DHLSelectGroup? 'DEP':$depVal;

	$x3 = $libdhl->getDepartmentSelection("ChooseDepartmentID[]", "ChooseDepartmentID[]", $selected_department_id_ary, $___isMultiple=true, $___hasFirst=false, $___firstText='', $___tags=' size="10" ', $___noOptgroupLabel=false, $selected_division_id,$depVal, '',$filterPIC);
}

if($sys_custom['DHL'] && preg_match('/^COM\d+$/',$CatID) && isset($ChooseDepartmentID)){
	$selected_department_id = $libdhl->cleanTargetSelectionData($ChooseDepartmentID, 'DEP');
	$selected_user_id_ary = isset($ChooseUserID)? $ChooseUserID : array();
	$x4 = $libdhl->getDepartmentUserSelection($selected_department_id, "ChooseUserID[]", "ChooseUserID[]", $selected_user_id_ary, $___isMultiple=true, $___hasFirst=false, $___firstText='', $___tags=' size="10" ', $___valuePrefix='', $___hasOptgroupLabel=true);
}

if($sys_custom['DHL'] && $CatID == -1){
    $staffs = $libdhl->getUserRecords(array('RecordStatus'=>1, 'ExcludeUserIdAry'=>$exclude_user_id_ary));
	$staff_name_field = Get_Lang_Selection("ChineseName","EnglishName");
	$x4 = "<select name=\"ChooseUserID[]\" id=\"ChooseUserID[]\" multiple=\"multiple\" size=\"15\" >\n";
		for($i=0;$i<count($staffs);$i++){
		    $x4 .= "<option value=\"".$staffs[$i]['UserID']."\">".($staffs[$i][$staff_name_field]==''? $staffs[$i]['UserLogin'] : Get_String_Display($staffs[$i][$staff_name_field]).' ('.$staffs[$i]['UserLogin'].')')."</option>";
		}
	$x4.= "</select>\n";
}

if(isset($ChooseGroupID)) {
	if($CatID<0)
	{ 
 	    if($CatID==-4)
	    {
			$row_user = $luser->returnUsersByIdentity(4,1,"",$UserRecordStatus, $alumniyear);
	    }
 	    else if($CatID==-3)
	    {
	    	$sortByClass = 1;
		    # retrieve student list first
		    $StudentList = $lclass->getClassStudentList($classname, "1");
		    $StudentIDStr = implode(",",$StudentList);
			
			// $row = $luser->getParent($StudentIDStr, $sortByClass, $filterAppParent);
			$ParentArr = $luser->getParent($StudentIDStr, $sortByClass, 0, $UserRecordStatus);
			
			if ($filterNotAppParent) {
				$AppParentArr = $luser->getParent($StudentIDStr, $sortByClass, 1, $UserRecordStatus);
				
				foreach ((array)$AppParentArr as $AppParent) {
					$AppParentIDArr[] = $AppParent['ParentID'];
				}
				
				foreach ((array)$ParentArr as $index=>$Parent) {
					if (in_array($Parent['ParentID'], (array)$AppParentIDArr)) {
						unset($ParentArr[$index]);
					}
				}

				# reorder array
				$ParentArr = array_values($ParentArr);
				
			} else if ($filterAppParent) {
				$ParentArr = $luser->getParent($StudentIDStr, $sortByClass, 1, $UserRecordStatus);
			} else {
			}
			$row_user = $ParentArr;
			// debug_pr($ParentArr);
	    }
	    else if($CatID==-2 && $classname)
	    {
	    	$sortByClass = 1;
	    	
	    	$filterUserIdAry = '';
	    	if ($filterAppStudent) {
	    		$filterUserIdAry = $luser->getStudentWithParentUsingParentApp($includeNoPushMessageUser);
	    	}
	    	if ($filterAppStudentByStudentApp) {
	    		$filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser))));
	    	}
    		if($filterUser != ''){
	    		$filterUserIdAry = $filterUser;
    		}
//	    	if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
//	    		$filterUserIdAry = array_intersect((array)$filterUserIdAry, array_merge((array)$enrolGroupStudentIdAry, array_merge((array)$classGroupStudentIdAry, (array)$eventParticipantIDAry)));
//	    	}
			if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
				$UserInClassAndClubAndEvent = array();
				if ($filterClubAndEventInCharge) {
		    		$UserInClassAndClubAndEvent = array_merge($UserInClassAndClubAndEvent, array_merge((array)$enrolGroupStudentIdAry, (array)$eventParticipantIDAry));
		    	}
		    	if ($filterClassTeacherClass) {
		    		$UserInClassAndClubAndEvent = array_merge($UserInClassAndClubAndEvent, (array)$classGroupStudentIdAry);
		    	}
		    	$filterUserIdAry = array_intersect((array)$filterUserIdAry, (array)$UserInClassAndClubAndEvent);

				// [2020-0407-1445-44292] ensure own class students can be selected
				if($filterClassTeacherClass && $filterOwnClassesOnly && !empty($classGroupStudentIdAry)) {
                    $filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, (array)$classGroupStudentIdAry)));
                }
			}

		    $row_user = $lclass->getStudentNameListWClassNumberByClassName($classname, $UserRecordStatus, $filterUserIdAry);
	    }
	    else
	    {
	    	$filterUserIdAry = '';
	    	if ($filterAppStudent) {
	    		$filterUserIdAry = $luser->getStudentWithParentUsingParentApp($includeNoPushMessageUser);
	    	}
	    	if ($filterAppStudentByStudentApp) {
	    		$filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser))));
	    	}
	    	if ($filterAppTeacher) {
	    		$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
				$loggedOutTeacherCanReceivePushMessage= $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
	    		$filterUserIdAry = $luser->getTeacherWithTeacherUsingTeacherApp($includeNoPushMessageUser, $loggedOutTeacherCanReceivePushMessage);
	    	}
    		if($filterUser != ''){
	    		$filterUserIdAry = $filterUser;
    		}
//	    	if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
//	    		$filterUserIdAry = array_intersect((array)$filterUserIdAry, array_merge((array)$enrolGroupStudentIdAry, array_merge((array)$classGroupStudentIdAry, (array)$eventParticipantIDAry)));
//	    	}
			if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
				$UserInClassAndClubAndEvent = array();
				if ($filterClubAndEventInCharge) {
		    		$UserInClassAndClubAndEvent = array_merge($UserInClassAndClubAndEvent, array_merge((array)$enrolGroupStudentIdAry, (array)$eventParticipantIDAry));
		    	}
		    	if ($filterClassTeacherClass) {
		    		$UserInClassAndClubAndEvent = array_merge($UserInClassAndClubAndEvent, (array)$classGroupStudentIdAry);
		    	}
		    	
		    	// ip.2.5.8.1.1 deployment testing defect 3141, failed to select students for class teacher
		    	$filterUserIdAry = array_intersect((array)$filterUserIdAry, (array)$UserInClassAndClubAndEvent);
		    	//$filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, (array)$UserInClassAndClubAndEvent)));
			}
			
			if (!$filterAppStudent && !$filterAppStudentByStudentApp && !$filterAppTeacher && !$filterAppParent && !$filterUser) {
				$filterUserIdAry = '';
			}

            // [2020-0407-1445-44292] ensure own class students can be selected
            if($filterClassTeacherClass && $filterOwnClassesOnly && !empty($classGroupStudentIdAry)) {
                $filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, (array)$classGroupStudentIdAry)));
            }

	    	$row_user = $luser->returnUsersByIdentity($ChooseGroupID[0],$teaching,"",$UserRecordStatus, "", $filterUserIdAry);
		}
	}
	else if($CatID[0]=='i') { //for the Internal Recipient Group
		$aliasId = substr($CatID, 1);
		$recordType = implode("','",$permitted);
		$namefield = getNameFieldWithClassNumberByLang("b.");
   		$sql = "	SELECT 
    				DISTINCT a.TargetID,$namefield 
    			FROM 
    				INTRANET_CAMPUSMAIL_GROUPALIAS_INTERNAL_ENTRY as a
				INNER JOIN 
					INTRANET_USER as b 
				ON 
					a.TargetID = b.UserID
				WHERE 
					a.AliasID = '$aliasId'
				AND
					b.RecordType IN ('".$recordType."')
				ORDER BY 
					b.EnglishName ";
    	$row_user = $li->returnArray($sql,2);
	}
	else
	{
		$filterUserIdAry = '';
    	if ($filterAppStudent) {
    		$filterStudentUserIdAry = $luser->getStudentWithParentUsingParentApp($includeNoPushMessageUser);
    	}
    	if ($filterAppStudentByStudentApp) {
    		$filterStudentUserIdAry = array_values(array_unique(array_merge((array)$filterStudentUserIdAry, $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser))));
    	}
    	if ($filterAppTeacher) {
    		$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
			$loggedOutTeacherCanReceivePushMessage = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
				
    		$filterTeacherUserIdAry = $luser->getTeacherWithTeacherUsingTeacherApp($includeNoPushMessageUser, $loggedOutTeacherCanReceivePushMessage);
    	}
    	if ($filterAppParent) {
    		$filterParentUserIdAry = $luser->getParentUsingParentApp('', $includeNoPushMessageUser);
    	}
		
		if($filterUser != ''){
    		$filterUserIdAry = $filterUser;
		}
		else {
			$filterUserIdAry = array_values(array_unique(array_remove_empty(array_merge((array)$filterStudentUserIdAry, (array)$filterTeacherUserIdAry, (array)$filterParentUserIdAry))));
		}
		
//    	if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
//	    		$filterUserIdAry = array_intersect((array)$filterUserIdAry, array_merge((array)$enrolGroupStudentIdAry, array_merge((array)$classGroupStudentIdAry, (array)$eventParticipantIDAry)));
//    	}
		if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
			$UserInClassAndClubAndEvent = array();
			if ($filterClubAndEventInCharge) {
	    		$UserInClassAndClubAndEvent = array_merge($UserInClassAndClubAndEvent, array_merge((array)$enrolGroupStudentIdAry, (array)$eventParticipantIDAry));
	    	}
	    	if ($filterClassTeacherClass) {
	    		$UserInClassAndClubAndEvent = array_merge($UserInClassAndClubAndEvent, (array)$classGroupStudentIdAry);
	    	}
	    	$filterUserIdAry = array_intersect((array)$filterUserIdAry, (array)$UserInClassAndClubAndEvent);
		}
		
		if (!$filterAppStudent && !$filterAppStudentByStudentApp && !$filterAppTeacher && !$filterAppParent && !$filterUser) {
			$filterUserIdAry = '';
		}
		
		if($FromDR == 1 && isset($sys_custom['DocRouting']['SpecialGroup']) && is_array($sys_custom['DocRouting']['SpecialGroup']) && count($sys_custom['DocRouting']['SpecialGroup'])>0)
		{
			$row_user = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted, "", "",$UserRecordStatus, $filterUserIdAry, $sys_custom['DocRouting']['SpecialGroup']);
		}else{
			$row_user = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted, "", "",$UserRecordStatus, $filterUserIdAry);
		}
	}
}

if (isset($ChooseEventID)) {
	if ($CatID == 5 && $clubOrEvent == "E") {
		$filterUserIdAry = '';
		foreach ((array)$ChooseEventID as $thisChooseEventID) {
			$chooseEventParticipantInfoAry = $libenroll->Get_Activity_Participant_Info($thisChooseEventID);
			foreach ((array)$chooseEventParticipantInfoAry as $chooseEventParticipantInfo) {
			$chooseEventUserIDAry[] = $chooseEventParticipantInfo['UserID'];
			}
		}
		
		if ($filterAppStudent) {
    		$filterUserIdAry = $luser->getStudentWithParentUsingParentApp($includeNoPushMessageUser);
    	}
    	if ($filterAppStudentByStudentApp) {
    		$filterUserIdAry = array_values(array_unique(array_merge((array)$filterUserIdAry, $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser))));
    	}
    	if ($filterAppTeacher) {
    		$eClassAppSettingsObj = $libeClassApp->getAppSettingsObj();
			$loggedOutTeacherCanReceivePushMessage = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
    		
    		$filterUserIdAry = $luser->getTeacherWithTeacherUsingTeacherApp($includeNoPushMessageUser, $loggedOutTeacherCanReceivePushMessage);
    	}
    	
		if($filterUser != ''){
			$filterUserIdAry = $filterUser;
		}
//    	if ($filterClubAndEventInCharge || $filterClassTeacherClass) {
//	    	$filterUserIdAry = array_intersect((array)$filterUserIdAry, array_merge((array)$enrolGroupStudentIdAry, array_merge((array)$classGroupStudentIdAry), (array)$eventParticipantIDAry));
//    	}

	    $filterUserIdAry = array_intersect((array)$filterUserIdAry, (array)$chooseEventUserIDAry);
    	
		$row_user = $luser->returnUsersByIdentity(2,$teaching,"",$UserRecordStatus, $CheckiMailQuota=false, $filterUserIdAry);
		
	}
}

if (isset($ChooseGroupID) || isset($ChooseEventID)) {
	$x3  = "<select name=ChooseUserID[] size=15 multiple>\n";
     for($i=0; $i<sizeof($row_user); $i++)
     {
     		if($CatID==-3) {
				$tmp = "";
     			list($parentID, $parentName, $studentName, $clsName, $clsNo) = $row_user[$i];
     			
     			if (is_array($exclude_user_id_ary) && in_array($parentID, (array)$exclude_user_id_ary)) {
     				continue;
     			}
     			
	     		$x3 .= "<option value=".$parentID.">";
				$tmp = $clsName.($clsNo!=""?"-".$clsNo:"");
				$x3 .= ($tmp=="") ? "" : "($tmp) ";
				$x3 .= $studentName." ".$Lang['General']['s']."(".$parentName.")</option>\n";
     		}
	     	else {
	     		
	     		if (is_array($exclude_user_id_ary) && in_array($row_user[$i][0], (array)$exclude_user_id_ary)) {
     				continue;
     			}
	     		
	     		$x3 .= "<option value=".$row_user[$i][0].">".$row_user[$i][1]."</option>\n";
	     	}
     }
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
$OpenerFormName = ($OpenerFormName=="") ? "form1" : $OpenerFormName;
?>

<script language="javascript">
<!--
function AddOptions(obj){
	<?php if($from_thickbox){ ?>
	 var par = parent.window;
	 var parObj = parent.window.document.<?=$OpenerFormName?>.elements["<?=$fieldname?>"];
	<?php }else{ ?>
	 var par = opener.window;
	 var parObj = opener.window.document.<?=$OpenerFormName?>.elements["<?=$fieldname?>"];
	<?php } ?>
     var parSelectedOptions = par.Get_Selection_Value("<?=$fieldname?>","Array",true);
	 var x = (obj.name == "ChooseGroupID[]") ? "G" : (obj.name == "ChooseUserID[]"? "U" : "");
     checkOption(obj);
     par.checkOption(parObj);
     var i = obj.selectedIndex;
	 
	<?php if($sys_custom['DHL']){ ?>
	for(i=0;i<obj.options.length;i++){
		if(obj.options[i].selected){
			if(!jIN_ARRAY(parSelectedOptions,obj.options[i].value) && !jIN_ARRAY(parSelectedOptions,x + obj.options[i].value)){
				addtext = obj.options[i].text;
		    	par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
			}
		}
	}
	<?php }else{ ?>
     while(i!=-1){
	     if (x == "G" || (x == "U" && !jIN_ARRAY(parSelectedOptions,obj.options[i].value) && !jIN_ARRAY(parSelectedOptions,x + obj.options[i].value))) {
	     	addtext = obj.options[i].text;
		    par.checkOptionAdd(parObj, addtext, x + obj.options[i].value);
		 }
		 obj.options[i] = null;
		 i = obj.selectedIndex;
     }
     <?php } ?>
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     
     // update parent window's auto complete parameter
     if(typeof par.Update_Auto_Complete_Extra_Para == 'function') 
     	par.Update_Auto_Complete_Extra_Para();
     	
     ///// For Repair System variable [Start]
     if(par.form1 && par.form1.flag)
     {
	     par.form1.flag.value = 0;
     }
     ///// For Repair System variable [End]

     //par.generalFormSubmitCheck(par.form1);
     <?php if($custFollowUpJs){?>
     par.commonChooseFollowUpAction();
     <?php } ?>
     
     <?php if($logToDB){?>
     par.getSeclectedClassLogInfo($('select[name="classname"]').val());
     par.getSeclectedGroupLogInfo($('select[name="ChooseGroupID[]"]').val());
     par.getSeclectedTargetLogInfo($('select[name="CatID"]').val());
     <?php } ?>
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

<?php if($sys_custom['DHL']){ ?>
function ResetSelections()
{
	if(document.form1.elements['ChooseDivisionID[]']){
		checkOptionNone(document.form1.elements['ChooseDivisionID[]']);
	}
	if(document.form1.elements['ChooseDepartmentID[]']){
		checkOptionNone(document.form1.elements['ChooseDepartmentID[]']);
	}
}
<?php } ?>

<?php if (!isset($CatID)) { ?>


$(document).ready(function ()
{
	var obj = document.form1.CatID;
	var counting = 0;
	for (var i=0; i<obj.length; i++)
     {
          if (obj.options[i].value!=0 && obj.options[i].value!="")
          {
          	 counting ++;
          }
     }
     
     if (counting==1)
     {
     	// auto select the first category to save user's time
     	for (var i=0; i<obj.length; i++)
	     {
	          if (obj.options[i].value!=0 && obj.options[i].value!="")
	          {
	          	 obj.options[i].selected = true;
	          	 document.form1.submit();
	          }
	     }
     }

});
<?php } ?>

//-->
</script>

<form name="form1" action="" method="post">

<div class="form_table">
<table class="form_table_v30">
<?php if(!$sys_custom['DHL']){ ?>
<tr>
	<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
	<td ><?=getCurrentAcademicYear();?></td>
</tr>
<?php } ?>
<tr>
	<td class="field_title"><?=$Lang['CommonChoose']['SelectTarget']?></td>
	<td ><?=$x1?></td>
</tr>

<?php if($sys_custom['DHL'] && preg_match('/^COM\d+$/',$CatID)){ ?>
<tr>
	<td class="field_title"><?=$Lang['DHL']['ChooseDivisions']?></td>
	<td>
		<table border='0' class='inside_form_table'>
			<tr>
				<td><?=$x2?></td>
				<td>
					<?= $select_group_only || $select_staff_only? "":$linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseDivisionID[]']);AddOptions(this.form.elements['ChooseDivisionID[]'])") . "<br>" ; ?>
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseDivisionID[]'])") . "<br>"; ?>					
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseDivisionID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
<?php }else if($CatID!=0 && $CatID > 0) { ?>
<tr>
	<td class="field_title"><?=$i_frontpage_campusmail_select_group?></td>
	<td>
		<table border='0' class='inside_form_table'>
			<? if ($CatID == 5 && $filterECAActivity) { ?>
			<tr>
				<td>
				<?=$linterface->Get_Radio_Button("clubOrEvent", "clubOrEvent", "C", ($clubOrEvent == "C") ? 1:0, $Class="", $eEnrollmentMenu['club'], $Onclick="this.form.submit();",$isDisabled=0);?>
				<?=$linterface->Get_Radio_Button("clubOrEvent", "clubOrEvent", "E", ($clubOrEvent == "E") ? 1:0, $Class="", $eEnrollmentMenu['activity'], $Onclick="this.form.submit();",$isDisabled=0);?>
				</td>
			</tr>
			<? } ?>
			
			<tr <?= ($CatID == 5 && $clubOrEvent == "E") ? "style='display:none;'" : "" ?>>
				<td><?=$x2?></td>
				<td>
					<?if(!$Disable_AddGroup_Button){?>
					<?= $linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseGroupID[]']);AddOptions(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>
					<?}?>
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseGroupID[]'])") . "<br>"; ?>					
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseGroupID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
			
			<tr <?= ($filterECAActivity!=1 || $CatID != 5 || $clubOrEvent == "C") ? "style='display:none;'" : "" ?>>
				<td><?=$x4?></td>
				<td>
					<?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseEventID[]'])") . "<br>"; ?>
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseEventID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
<? } ?>

<?php if($sys_custom['DHL'] && preg_match('/^COM\d+$/',$CatID) && isset($ChooseDivisionID)){ ?>
<tr>
	<td class="field_title"><?=$Lang['DHL']['ChooseDepartments']?></td>
	<td>
		<table border='0' class='inside_form_table'>
			<tr>
				<td><?=$x3?></td>
				<td>
					<?= $select_staff_only? "":$linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseDepartmentID[]']);AddOptions(this.form.elements['ChooseDepartmentID[]'])") . "<br>"; ?>
					<?= $select_group_only? "": $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit","checkOption(this.form.elements['ChooseDepartmentID[]'])") . "<br>" ; ?>					
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseDepartmentID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
<?php } ?>

<?php if($sys_custom['DHL'] && ( (preg_match('/^COM\d+$/',$CatID) && isset($ChooseDepartmentID) && !$select_group_only) || $CatID==-1) ){ ?>
<tr>
	<td class="field_title"><?=$i_frontpage_campusmail_select_user?></td>
	<td>
		<table border='0' class='inside_form_table'>
			<tr>
				<td>
					<?=$x4?>
				</td>
				<td>
					<?= $linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
<?php } ?>

<? 
if($CatID==-3 || $CatID==-2) 
{
    $filterYearClassIDArr = '';
    if($filterClassTeacherClass && $filterOwnClassesOnly) {
        // [2020-0407-1445-44292] only teaching classes can be selected
        $filterYearClassIDArr = $yearClassIdAry;
    }
	$ClassSelection = $lclass->getSelectClass("name='classname' onChange='this.form.submit();'", $classname, "", $i_general_all_classes, "", "", $filterYearClassIDArr);
	?>
<tr>
	<td class="field_title"><?=$i_UserParentLink_SelectClass?></td>
	<td><?=$ClassSelection?></td>
</tr>
<? } ?>

<?
### App parent filter checkbox
if($CatID==-3 && $filterNotAppParentOption==1) {
	$filterAppParentCheckbox = '<tr><td>';
	$filterAppParentCheckbox .= $linterface->Get_Checkbox('filterNotAppParent', 'filterNotAppParent', 1, $filterNotAppParent, $Class='', $Lang['CommonChoose']['FilterAppParent'], 'this.form.submit()', $Disabled='');
	$filterAppParentCheckbox .= '</tr></td>';
	} else {
	$filterAppParentCheckbox = '';
} ?>

<? 
if($CatID==-4) 
{ 
	$AlumniYears = $lclass->getSelectAlumniYears();
	
	$YearSelection = $linterface->GET_SELECTION_BOX($AlumniYears, "name='alumniyear' onChange='this.form.submit();'", $Lang['SysMgr']['RoleManagement']['AllAlumni'], $alumniyear);
	?>
<tr>
	<td class="field_title"><?=$Lang['iMail']['FieldTitle']['Alumni']?></td>
	<td><?=$YearSelection?></td>
</tr>
<? } ?>

<? if(isset($ChooseGroupID) || isset($ChooseEventID)) { ?>
<tr>
	<td class="field_title"><?=$i_frontpage_campusmail_select_user?></td>
	<td>
		<table border='0' class='inside_form_table'>
			<?=$filterAppParentCheckbox?>
			<tr>
				<td>
					<?=$x3?>
				</td>
				<td>
					<?= $linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
					<?= $linterface->GET_BTN($button_select_all, "submit","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
				</td>
			</tr>
		</table>
	</td>
</tr>
<?php 
	if($from_medical_write_message != ''){
		include_once($PATH_WRT_ROOT."lang/cust/medical_lang.$intranet_session_language.php");
		echo '<tr><td colspan="2">'.$Lang['medical']['message']['thickbox']['showOnlyCanReceiveUser'].'</td></tr>';
	}
?>
<? } ?>
</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?php
	if($from_thickbox){
		echo $linterface->GET_ACTION_BTN($button_close, "button", "parent.js_Hide_ThickBox();");
	}else{
		echo $linterface->GET_ACTION_BTN($button_close, "button", "self.close();");
	} 
?>
<p class="spacer"></p>
</div>

</div>

<input type=hidden name=fieldname value="<?=$fieldname?>">

</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>