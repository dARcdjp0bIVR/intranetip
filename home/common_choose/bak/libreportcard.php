<?php
# modifying by : Bill

if ($intranet_session_language == "b5") {
	include_once($intranet_root."/lang/reportcard_lang.en.php");
	$EngReportCard = $eReportCard;
	include_once($intranet_root."/lang/reportcard_lang.$intranet_session_language.php");
	$ChiReportCard = $eReportCard;
} else {
	include_once($intranet_root."/lang/reportcard_lang.b5.php");
	$ChiReportCard = $eReportCard;
	include_once($intranet_root."/lang/reportcard_lang.$intranet_session_language.php");
	$EngReportCard = $eReportCard;
}

//include_once($intranet_root."/lang/reportcard_lang.$intranet_session_language.php");
include_once($intranet_root."/includes/libfilesystem.php");
include_once($intranet_root."/includes/form_class_manage.php");

class libreportcard extends libdb {

	var $SpecialMarkArray = array("-1"=>"abs", "-2"=>"/");
	var $IsClassTeacher = null;
	var $TeachingClassID = null;
	var $TeachingClassName = null;
	var $HighlightStyleArray = null;

	function libreportcard()
	{
		$this->libdb();
		$this->uid = $_SESSION['UserID'];

		list($this->StartDate, $this->EndDate, $this->Year, $this->Semester, $this->AllowClassTeacherComment, $this->AllowSubjectTeacherComment, $this->AllowClassTeacherUploadCSV) = $this->GET_MARKSHEET_COLLECTION();
		
		if ($this->Year != "")
		{
			$AcademicYearObj = new academic_year($this->Year);
			$this->YearName = $AcademicYearObj->YearNameEN;
		}
		
		if ($this->Semester != "FULL" && $this->Semester != "")
		{
			$TermObj = new academic_year_term($this->Semester,$GetAcademicYearInfo=false);
			$this->SemesterName = $TermObj->YearTermNameEN;
		}

		//$this->StartPrevDate = date('Y-m-d', strtotime($this->StartDate) - 24 * 60 * 60);
		$this->StartPrevDate = $this->StartDate;
		$this->EndNextDate = date('Y-m-d', strtotime($this->EndDate) + 24 * 60 * 60);
		// CalculationMethodRule : 1 (ratio) : 0 (weighted)
		list($this->CalculationMethodRule, $this->CalculationOrder) = $this->GET_CALCULATION_METHOD();
		if ($this->CalculationMethodRule == "") $this->CalculationMethodRule = 1;
		if ($this->CalculationOrder == "") $this->CalculationOrder = "Horizontal";
	}

	function GET_ADMIN_USER()
	{
		global $intranet_root;

		$lf = new libfilesystem();
		$AdminUser = trim($lf->file_read($intranet_root."/file/reportcard/admin_user.txt"));

		return $AdminUser;
	}

	function IS_ADMIN_USER($ParUserID)
	{
		global $intranet_root;

		$AdminUser = $this->GET_ADMIN_USER();
		$IsAdmin = 0;
		if(!empty($AdminUser))
		{
			$AdminArray = explode(",", $AdminUser);
			$IsAdmin = (in_array($ParUserID, $AdminArray)) ? 1 : 0;
		}

		return $IsAdmin;
	}

	function hasAccessRight()
	{
		global $PageRight, $intranet_reportcard_admin, $intranet_reportcard_usertype, $ck_ReportCard_UserType;
		
		if($PageRight=="ADMIN")
		{
			$HaveRight = ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"]==1) ? true : false;
		}
		else
		{
			$HaveRight = ($PageRight==$ck_ReportCard_UserType || ($PageRight=="TEACHER" && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"]==1)) ? true : false;
		}

		return $HaveRight;
	}

	/*
	* Get MODULE_OBJ array
	*/
	function GET_MODULE_OBJ_ARR(){

		global $PATH_WRT_ROOT, $i_ReportCard_System, $CurrentPage, $eReportCard, $ck_ReportCard_UserType, $LAYOUT_SKIN, $top_menu_mode, $CurrentPageArr;
		global $ReportCardTemplate;
		global $plugin, $ReportCardShowImprovementHonour;
		global $ReportCardShowCalMethod;

		if($top_menu_mode==0)
		{
			$CurrentPageArr['eReportCard1.0'] = 0;
			$CurrentPageArr['eServiceeReportCard1.0'] = 1;
		}
		else
		{
			$CurrentPageArr['eReportCard1.0'] = 1;
			$CurrentPageArr['eServiceeReportCard1.0'] = 0;
		}

		if($ck_ReportCard_UserType=="ADMIN")
		{
			# Current Page Information init
			$PageSchemeSettings = 0;
			$PageSchemeSettings_GradingSchemes = 0;
			$PageSchemeSettings_SubjectGradings = 0;
			$PageSchemeSettings_Highlights = 0;
			$PageReportBuilder = 0;
			$PageReportBuilder_Template = 0;
			$PageReportBuilder_TitlesCustomization = 0;
			$PageMarksheetCollection = 0;
			$PageMarksheetCollection_PeriodSetting = 0;
			$PageMarksheetCollection_VerificationPeriodSetting = 0;
			$PageMarksheetCollection_SubmissionProgress = 0;
			$PageMarksheetCollection_VerificationProgress = 0;
			$PageMarksheetRevision = 0;
			$PageInfoUpload = 0;
			$PageReportGeneration = 0;
			$PageTransitData = 0;
			$PageDataDeletion = 0;

			switch ($CurrentPage) {
				case "SchemeSettings_GradingSchemes":
					$PageSchemeSettings = 1;
					$PageSchemeSettings_GradingSchemes = 1;
					break;
				case "SchemeSettings_SubjectGradings":
					$PageSchemeSettings = 1;
					$PageSchemeSettings_SubjectGradings = 1;
					break;
				case "SchemeSettings_CalMethod":
					$PageSchemeSettings = 1;
					$PageSchemeSettings_CalMethod = 1;
					break;
				case "SchemeSettings_Highlights":
					$PageSchemeSettings = 1;
					$PageSchemeSettings_Highlights = 1;
					break;
				case "ReportBuilder_Template":
					$PageReportBuilder = 1;
					$PageReportBuilder_Template = 1;
					break;
				case "ReportBuilder_TitlesCustomization":
					$PageSchemeSettings = 1;
					$PageReportBuilder_TitlesCustomization = 1;
					break;
				case "MarksheetCollection_PeriodSetting":
					$PageMarksheetCollection = 1;
					$PageMarksheetCollection_PeriodSetting = 1;
					break;
				case "MarksheetCollection_VerificationPeriodSetting":
					$PageMarksheetCollection = 1;
					$PageMarksheetCollection_VerificationPeriodSetting = 1;
					break;
				case "MarksheetCollection_SubmissionProgress":
					$PageMarksheetCollection = 1;
					$PageMarksheetCollection_SubmissionProgress = 1;
					break;
				case "MarksheetCollection_VerificationProgress":
					$PageMarksheetCollection = 1;
					$PageMarksheetCollection_VerificationProgress = 1;
					break;
				case "MarksheetRevision":
					$PageMarksheetRevision = 1;
					$PageMarksheetRevisionTitle = 1;
					break;
				case "PageMarksheetRevisionPresetComment":
					$PageMarksheetRevisionPresetComment = 1;
					$PageSchemeSettings = 1;
					break;
				case "InfoUpload":
					$PageInfoUpload = 1;
					break;
				case "ReportGeneration":
					$PageReportGeneration = 1;
					$PageReportGenerationTitle = 1;
					break;
				case "PageReportGenerationPositionReport":
					$PageReportGenerationTitle = 1;
					$PageReportGenerationPositionReport = 1;
					break;
				case "PageReportGenerationImprovementReport":
					$PageReportGenerationTitle = 1;
					$PageReportGenerationImprovementReport = 1;
					break;
				case "PageReportGenerationHonourStudent":
					$PageReportGenerationTitle = 1;
					$PageReportGenerationHonourStudent = 1;
					break;
				case "PageReportGenerationStatistic":
					$PageReportGenerationTitle = 1;
					$PageReportGenerationStatistic = 1;
					break;
				case "TransitData":
					$PageTransitData = 1;
					break;
				case "DataDeletion":
					$PageDataDeletion = 1;
					break;
			}

			# Menu information
			$MenuArr["SchemeSettings"] = array($eReportCard['SchemeSettings'], "", $PageSchemeSettings);
			$MenuArr["SchemeSettings"]["Child"]["GradingSchemes"] = array($eReportCard['SchemeSettings_GradingSchemes'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/grading_schemes/index.php", $PageSchemeSettings_GradingSchemes);
			$MenuArr["SchemeSettings"]["Child"]["SubjectGradings"] = array($eReportCard['SchemeSettings_SubjectGradings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/subject_gradings/index.php", $PageSchemeSettings_SubjectGradings);
			if ($ReportCardShowCalMethod)
			{
				$MenuArr["SchemeSettings"]["Child"]["CalMethod"] = array($eReportCard['SchemeSettings_CalMethod'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/calmethod_setting.php", $PageSchemeSettings_CalMethod);
			}
			$MenuArr["SchemeSettings"]["Child"]["Highlights"] = array($eReportCard['SchemeSettings_Highlights'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/highlights_setting.php", $PageSchemeSettings_Highlights);
			$MenuArr["SchemeSettings"]["Child"]["TitlesCustomization"] = array($eReportCard['ReportBuilder_TitlesCustomization'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_builder/titles_custom.php", $PageReportBuilder_TitlesCustomization);
			$MenuArr["SchemeSettings"]["Child"]["PresetSetting"] = array($eReportCard['PresetComment'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/wordtemplate/index.php", $PageMarksheetRevisionPresetComment);

			//$MenuArr["ReportBuilder"] = array($eReportCard['ReportBuilder'], "", $PageReportBuilder);
			//$MenuArr["ReportBuilder"]["Child"]["Template"] = array($eReportCard['ReportBuilder_Template'], $PATH_WRT_ROOT."home/admin/reportcard/report_builder/index.php", $PageReportBuilder_Template);
			$MenuArr["ReportBuilder"] = array($eReportCard['ReportBuilder'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_builder/index.php", $PageReportBuilder_Template);

			$MenuArr["MarksheetCollection"] = array($eReportCard['MarksheetCollection'], "", $PageMarksheetCollection);
			$MenuArr["MarksheetCollection"]["Child"]["PeriodSetting"] = array($eReportCard['MarksheetCollectionPeriodSetting'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_collections/settings.php", $PageMarksheetCollection_PeriodSetting);
			$MenuArr["MarksheetCollection"]["Child"]["VerificationPeriodSetting"] = array($eReportCard['ResultVerificationPeriodSetting'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_collections/verification_settings.php", $PageMarksheetCollection_VerificationPeriodSetting);
			$MenuArr["MarksheetCollection"]["Child"]["SubmissionProgress"] = array($eReportCard['MonitorMarksheetSubmissionProgress'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_collections/marksheet_submission_progress.php", $PageMarksheetCollection_SubmissionProgress);
			$MenuArr["MarksheetCollection"]["Child"]["VerificationProgress"] = array($eReportCard['MonitorResultVerificationProgress'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_collections/result_verification_progress.php", $PageMarksheetCollection_VerificationProgress);

			//if ($ReportCardTemplate == 2) {
			if (true) {
				//$MenuArr["MarksheetRevision"] = array($eReportCard['MarksheetRevision'], "", $PageMarksheetRevisionTitle);
				//$MenuArr["MarksheetRevision"]["Child"]["MarksheetRevision"] = array($eReportCard['MarksheetRevision'], $PATH_WRT_ROOT."home/admin/reportcard/marksheet_submission/admin_index.php", $PageMarksheetRevision);
				$MenuArr["MarksheetRevision"] = array($eReportCard['MarksheetRevision'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_submission/admin_index.php", $PageMarksheetRevision);
				//$MenuArr["MarksheetRevision"]["Child"]["PresetSetting"] = array($eReportCard['PresetComment'], $PATH_WRT_ROOT."home/admin/reportcard/wordtemplate/index.php", $PageMarksheetRevisionPresetComment);
			} else {
				$MenuArr["MarksheetRevision"] = array($eReportCard['MarksheetRevision'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_submission/admin_index.php", $PageMarksheetRevision);
			}

			$MenuArr["InfoUpload"] = array($eReportCard['InfoUpload'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/info_upload/index.php", $PageInfoUpload);

			//if ($ReportCardTemplate == 2) {
			if (true) {
				$MenuArr["ReportGeneration"] = array($eReportCard['ReportGeneration'], "", $PageReportGenerationTitle);
				$MenuArr["ReportGeneration"]["Child"]["ReportCard"] = array($eReportCard['ReportCard'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/index.php", $PageReportGeneration);
				$MenuArr["ReportGeneration"]["Child"]["PositionReport"] = array($eReportCard['PositionReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/position_report/index.php", $PageReportGenerationPositionReport);
				if ($ReportCardShowImprovementHonour) {
					$MenuArr["ReportGeneration"]["Child"]["ImprovementReport"] = array($eReportCard['ImprovementReport'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/improvement_report/index.php", $PageReportGenerationImprovementReport);
					$MenuArr["ReportGeneration"]["Child"]["HonourStudent"] = array($eReportCard['HonourStudent'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/honour_student/index.php", $PageReportGenerationHonourStudent);
				}
				$MenuArr["ReportGeneration"]["Child"]["Statistic"] = array($eReportCard['Statistic'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/statistic/index.php", $PageReportGenerationStatistic);
			} else {
				$MenuArr["ReportGeneration"] = array($eReportCard['ReportGeneration'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/report_generation/index.php", $PageReportGeneration);
			}

			if ($plugin['iPortfolio'])
				$MenuArr["TransitData"] = array($eReportCard['TransitData'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/transit.php", $PageTransitData);

			$MenuArr["DataDeletion"] = array($eReportCard['DataDeletion'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/delete.php", $PageDataDeletion);
		}
		else if($ck_ReportCard_UserType=="TEACHER")
		{
			# Current Page Information init
			$PageMarksheetSubmission = 0;

			switch ($CurrentPage) {
				case "MarksheetSubmission":
					$PageMarksheetSubmission = 1;
					break;
			}

			# Menu information
			$MenuArr["MarksheetSubmission"] = array($eReportCard['MarksheetSubmission'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/marksheet_submission/index.php", $PageMarksheetSubmission);
		}
		else if($ck_ReportCard_UserType=="STUDENT")
		{
			# Current Page Information init
			$PageVerifyAssessmentResult = 0;

			switch ($CurrentPage) {
				case "VerifyAssessmentResult":
					$PageVerifyAssessmentResult = 1;
					break;
			}

			# Menu information
			$MenuArr["VerifyAssessmentResult"] = array($eReportCard['VerifyAssessmentResult'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/reportcard/verify_results/index.php", $PageVerifyAssessmentResult);
		}

		# module information
		$MODULE_OBJ['title'] = $i_ReportCard_System;
		$MODULE_OBJ['title_css'] = "menu_opened";
		$MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_reportcardsystem.gif";
		$MODULE_OBJ['menu'] = $MenuArr;

		return $MODULE_OBJ;
	}

	#########################################################################################################
	################################### Grading Scheme Functions Start ######################################
	/*
	* Get Scheme Data
	*/
	function GET_SCHEME_INFO($ParSchemeID)
	{
		# Get Scheme Info
		$sql = "SELECT
					s.SchemeTitle,
					s.Description,
					s.SchemeType,
					s.FullMark,
					s.Pass,
					s.Fail,
					s.Weighting
				FROM
					RC_GRADING_SCHEME as s
				WHERE
					s.SchemeID = '$ParSchemeID'
				";
		$row = $this->returnArray($sql, 7);
		$MainInfo = $row[0];

		# Get Scheme GradeMark Info
		$sql = "SELECT
					g.GradeMarkID,
					g.Nature,
					g.LowerLimit,
					g.Grade,
					g.GradePoint
				FROM
					RC_GRADING_SCHEME_GRADEMARK as g
				WHERE
					g.SchemeID = '$ParSchemeID'
				ORDER BY
					g.Nature, g.LowerLimit DESC
				";
		$GradeMarkInfo = $this->returnArray($sql, 5);

		for($i=0; $i<sizeof($GradeMarkInfo); $i++)
		{
			list($GradeMarkID, $Nature, $LowerLimit, $Grade, $GradePoint) = $GradeMarkInfo[$i];
			$Nature = trim($Nature);
			if($Nature=="P" || $Nature=="F")
			{
				$GradeMarkArr[$Nature][] = array($LowerLimit, $Grade, $GradePoint);
			}
			else
			{
				$GradeMarkArr[$Nature] = array($LowerLimit, $Grade, $GradePoint);
			}
		}

		return array($MainInfo, $GradeMarkArr);
	}

	/*
	* Generate Pass/Fail grading field for scheme form
	*/
	function GENERATE_GRADING_FORM_CELL($ParIsForm, $ParPrefix, $ParGradeMarkArr="")
	{
		if ($ParGradeMarkArr == '')
			$ParGradeMarkArr = array();
			
		$RowSize = (!empty($ParGradeMarkArr)) ? $RowSize = sizeof($ParGradeMarkArr) : 1;

		for($i=0; $i<$RowSize; $i++)
		{
			if($ParIsForm)
			{
				$rx .= "<tr>";
				$rx .= "<td class='tabletext'><input class='textboxnum' maxlength='10' type='text' name='".$ParPrefix."LowerLimit[]' id='".$ParPrefix."LowerLimit[]' value='".$ParGradeMarkArr[$i][0]."' /></td>";
				$rx .= "<td class='tabletext'><input class='textboxnum' maxlength='10' type='text' name='".$ParPrefix."Grade[]' id='".$ParPrefix."Grade[]' value='".$ParGradeMarkArr[$i][1]."' /></td>";
				$rx .= "<td class='tabletext'><input class='textboxnum' maxlength='10' type='text' name='".$ParPrefix."GradePoint[]' id='".$ParPrefix."GradePoint[]' value='".$ParGradeMarkArr[$i][2]."' /></td>";
				$rx .= "</tr>";
			}
			else
			{
				$rx .= "<tr>";
				$rx .= "<td class='formtextbox' width='100'><span class='tabletext'>".$ParGradeMarkArr[$i][0]."</span></td>";
				$rx .= "<td class='formtextbox' width='100'><span class='tabletext'>".$ParGradeMarkArr[$i][1]."</span></td>";
				$rx .= "<td class='formtextbox' width='100'><span class='tabletext'>".$ParGradeMarkArr[$i][2]."</span></td>";
				$rx .= "</tr>";
			}
		}

		return $rx;
	}

	#################################### Grading Scheme Functions End #######################################
	#########################################################################################################

	################################### Subject Gradings Functions Start ####################################
	/*
	* Generate Schemes Selection
	*/
	function GET_ALL_SCHEMES()
	{
		# Get Scheme Info
		$sql = "SELECT
					CONCAT(s.SchemeID, '_', s.SchemeType),
					s.SchemeTitle
				FROM
					RC_GRADING_SCHEME as s
				ORDER BY
					s.SchemeTitle
				";
		$SchemeArr = $this->returnArray($sql, 2);

		return $SchemeArr;
	}

	/*
	* Get All Subjects
	*/
	function GET_ALL_SUBJECTS($ParForSelection=0) {
		
		if ($ParForSelection==1)
			$SubjectField = "IF(a.CH_DES='' OR a.CH_DES IS NULL, a.EN_DES, CONCAT(a.EN_DES, ' (', a.CH_DES, ')'))";
		else
			$SubjectField = "IF(a.CH_DES='' OR a.CH_DES IS NULL, a.EN_DES, CONCAT(a.EN_DES, '<br />', a.CH_DES))";
		
		if ($ParForSelection==1)
			$CmpSubjectField = "IF(b.CH_DES='' OR b.CH_DES IS NULL, b.EN_DES, CONCAT(b.EN_DES, ' (', b.CH_DES, ')'))";
		else
			$CmpSubjectField = "IF(b.CH_DES='' OR b.CH_DES IS NULL, b.EN_DES, CONCAT(b.EN_DES, '<br />', b.CH_DES))";
		#$SubjectField = ($ParForSelection==1) ? "CONCAT(a.EN_DES, ' (', a.CH_DES, ')')" : "CONCAT(a.EN_DES, '<br />', a.CH_DES)";
		#$CmpSubjectField = ($ParForSelection==1) ? "CONCAT(b.EN_DES, ' (', b.CH_DES, ')')" : "CONCAT(b.EN_DES, '<br />', b.CH_DES)";
		$sql = "SELECT DISTINCT
						a.RecordID,
						$SubjectField,
						b.RecordID,
						$CmpSubjectField
					FROM
						ASSESSMENT_SUBJECT AS a
						LEFT JOIN ASSESSMENT_SUBJECT AS b ON a.CODEID = b.CODEID
					WHERE
						a.EN_SNAME IS NOT NULL
						AND (a.CMP_CODEID IS NULL Or a.CMP_CODEID='')
						AND a.RecordStatus = 1
						AND b.RecordStatus = 1
					ORDER BY
						a.DisplayOrder,
						b.DisplayOrder
				";
		$SubjectArr = $this->returnArray($sql, 4);
		
		for($i=0; $i<sizeof($SubjectArr); $i++)
		{
			list($SubjectID, $SubjectName, $CmpSubjectID, $CmpSubjectName) = $SubjectArr[$i];
			if($SubjectID==$CmpSubjectID)
			{
				$CmpSubjectID = 0;
			}
			else
			{
				$SubjectName = $CmpSubjectName;
			}
			$ReturnArr[$SubjectID][$CmpSubjectID] = $SubjectName;
			ksort($ReturnArr[$SubjectID]);
		}
		
		return $ReturnArr;
	}

	/*
	* Get Subjects Grading Records
	*/
	function GET_SUBJECT_GRADINGS_RECORDS()
	{
		$sql = "SELECT
					a.SubjectID,
					CONCAT(a.SchemeID, '_', b.SchemeType),
					a.Scale
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT JOIN RC_GRADING_SCHEME b ON a.SchemeID = b.SchemeID
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as c ON a.SubjectGradingID = c.SubjectGradingID
				WHERE
					c.SubjectGradingID IS NULL
				";
		$RecordArr = $this->returnArray($sql);

		for($i=0; $i<sizeof($RecordArr); $i++)
		{
			list($SubjectID, $Scheme, $Scale) = $RecordArr[$i];
			$ReturnArr[trim($SubjectID)] = array($Scheme, $Scale);
		}

		return $ReturnArr;
	}

	/*
	* Get Special Subjects Grading Records
	*/
	function GET_SPECIAL_SUBJECT_GRADINGS_RECORDS()
	{
		global $eclass_db;

		$sql = "SELECT
					a.SubjectGradingID,
					if(a.ParentSubjectGradingID IS NULL, 0, a.ParentSubjectGradingID),
					a.SubjectID,
					CONCAT(EN_DES, '<br />', CH_DES),
					CONCAT(a.SchemeID, '_', b.SchemeType),
					a.Scale
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT JOIN RC_GRADING_SCHEME b ON a.SchemeID = b.SchemeID
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as c ON a.SubjectGradingID = c.SubjectGradingID
					LEFT JOIN ASSESSMENT_SUBJECT as d ON a.SubjectID = d.RecordID
				WHERE
					c.SubjectGradingID IS NOT NULL
					And
					d.RecordStatus = 1
				GROUP BY
					c.SubjectGradingID
				";
		$row = $this->returnArray($sql, 6);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectGradingID, $ParentSubjectGradingID, $SubjectID, $SubjectName, $Scheme, $Scale) = $row[$i];
			if($ParentSubjectGradingID==0)
				$ReturnArr[$SubjectGradingID][$SubjectGradingID] = array($SubjectID, $SubjectName, $Scheme, $Scale);
			else
				$ReturnArr[$ParentSubjectGradingID][$SubjectGradingID] = array($SubjectID, $SubjectName, $Scheme, $Scale);
		}

		return $ReturnArr;
	}

	/*
	* Get Special Subjects Form(s)
	*/
	function GET_SUBJECT_GRADING_FORM($ParDisplayType=0, $ParSubjectGradingID="")
	{
		$cond = ($ParSubjectGradingID=="") ? "" : " WHERE a.SubjectGradingID = '$ParSubjectGradingID'";
		$sql = "SELECT
					a.SubjectGradingID,
					b.YearName as LevelName,
					b.YearID as ClassLevelID
				FROM
					RC_SUBJECT_GRADING_SCHEME_FORM as a
					LEFT JOIN YEAR as b ON a.ClassLevelID = b.YearID
				$cond
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectGradingID, $LevelName, $ClassLevelID) = $row[$i];
			$ReturnArray[trim($SubjectGradingID)][] = ($ParDisplayType==1) ? $LevelName : $ClassLevelID;
		}

		if($ParSubjectGradingID!="")
		{
			$ReturnArray = $ReturnArray[$ParSubjectGradingID];
		}
		return $ReturnArray;
	}

	/*
	* Get Subjects Selection
	*/
	function GET_SUBJECT_SELECTION()
	{
		global $eclass_db;

		$SubjectArray = $this->GET_ALL_SUBJECTS(1);
		if(is_array($SubjectArray))
		{
			$SubjectSelect = "<SELECT name='SubjectID'>";
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpSubjectID => $SubjectName)
					{
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
						$SubjectName = ($CmpSubjectID>0) ? "&nbsp;&nbsp;&nbsp;&nbsp;".$SubjectName : $SubjectName;
						$SubjectSelect .= "<OPTION value='".$SubjectID."'>".$SubjectName."</OPTION>";
					}
				}
			}
			$SubjectSelect .= "</SELECT>";
		}

		return $SubjectSelect;
	}

	/*
	* Get Class Levels
	*/
	function GET_FORMS()
	{
		$sql = "SELECT
					YearID as ClassLevelID,
					YearName as LevelName
				FROM
					YEAR
				ORDER BY
					Sequence
				";
		$row = $this->returnArray($sql, 2);

		return $row;
	}

	// get class level name by class level id
	function GET_CLASSLEVEL_NAME($ParClassLevelID)
	{
		$sql = "SELECT
					YearName as LevelName
				FROM
					YEAR
				WHERE
					YearID = '$ParClassLevelID'
				";
		$row = $this->returnVector($sql, 1);

		return $row[0];
	}

	function GET_CLASSLEVELID_BY_CLASSID($ParClassID)
	{
		$sql = "SELECT
					ClassLevelID
				FROM
					INTRANET_CLASS
				WHERE
					ClassID = '$ParClassID'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}


	/*
	* Get related subject(s)
	*/
	function GET_RELATED_SUBJECT($ParSubjectID)
	{
		global $eclass_db;

		$sql = "SELECT
					a.CODEID,
					a.CMP_CODEID
				FROM
					ASSESSMENT_SUBJECT as a
				WHERE
					a.RecordID = '".$ParSubjectID."'
					And
					a.RecordStatus = 1
				";
		$row = $this->returnArray($sql, 2);

		list($CODEID, $CMP_CODEID) = $row[0];
		if(empty($CMP_CODEID))
		{
			$sql = "SELECT RecordID FROM ASSESSMENT_SUBJECT WHERE CODEID = '$CODEID' AND RecordStatus = 1 And (CMP_CODEID IS NOT NULL Or CMP_CODEID != '')";
			$row = $this->returnVector($sql);
			$ReturnArr = array_merge(array($ParSubjectID), $row);

		}
		else
		{
			$sql = "SELECT RecordID FROM ASSESSMENT_SUBJECT WHERE CODEID = '$CODEID' AND RecordStatus = 1 And (CMP_CODEID IS NULL Or CMP_CODEID = '') LIMIT 1";
			$row = $this->returnVector($sql);
			$ReturnArr = array($row[0], $ParSubjectID);
		}

		return $ReturnArr;
	}

	/*
	* Insert records to RC_SUBJECT_GRADING_SCHEME_FORM
	*/
	function INSERT_SUBJECT_GRADING_FORM($ParSubjectGradingID, $ParFormArr)
	{
		for($i=0; $i<sizeof($ParFormArr); $i++)
		{
			$ClassLevelID = $ParFormArr[$i];

			# Step 2: Insert reocrd in RC_SUBJECT_GRADING_SCHEME_FORM
			$sql = "INSERT INTO RC_SUBJECT_GRADING_SCHEME_FORM (SubjectGradingID, ClassLevelID, DateInput, DateModified) VALUES ('$ParSubjectGradingID', '$ClassLevelID', now(), now())";
			$success = $this->db_db_query($sql);
		}

		return $success;
	}

#################################### Subject Gradings Function sEnd  #######################################
#############################################################################################################

################################### Report Builder Functions Start ######################################

	/*
	* Get All Templates
	*/
	function GET_ALL_TEMPLATES()
	{
		$sql = "SELECT
					ReportID,
					REPLACE(ReportTitle, '::', '&nbsp;')
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					RecordStatus = 1
				ORDER BY ReportID
				";
		$row = $this->returnArray($sql, 2);
		
		return $row;
	}

	/*
	* Get Report Template basic info
	*/
	function GET_REPORT_TEMPLATE($ParReportID)
	{
		$sql = "SELECT
					ReportTitle,
					ReportType,
					Description,
					ShowFullMark,
					DisplaySettings,
					RecordStatus,
					ShowPosition,
					PositionRange,
					HideNotEnrolled,
					Footer,
					NoHeader,
					LineHeight,
					SignatureWidth,
					AcademicYear,
					DecimalPoint
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
				";
		$row = $this->returnArray($sql, 11);

		return $row[0];
	}

	function GET_DECIMAL_POINT($ParReportID)
	{
		$Sql = "
				SELECT
					DecimalPoint
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
			 	";

		$row = $this->returnVector($Sql, 1);

		return $row[0];
	}


	/*
	* Get Report Template basic info
	*/
	function GET_ISSUE_DATE($ParReportID)
	{
		$sql = "SELECT
					IssueDate
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get Report Template basic info
	*/
	function GET_ACADEMIC_YEAR($ParReportID)
	{
		$sql = "SELECT
					AcademicYear
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}


	/*
	* Get Report Template basic info
	*/
	function GET_REPORT_TITLE($ParReportID)
	{
		$sql = "SELECT
					REPLACE(ReportTitle, '::', '&nbsp;')
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get Report Template basic info
	*/
	function GET_REPORT_TEMPLATE_SETTINGS($ParReportID)
	{
		$sql = "SELECT
					DisplaySettings
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get Report Type
	*/
	function GET_REPORT_TYPE($ParReportID)
	{
		$sql = "SELECT
					ReportType
				FROM
					RC_REPORT_TEMPLATE
				WHERE
					ReportID = '$ParReportID'
				";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get Report Template Form(s)
	*/
	function GET_REPORT_TEMPLATE_FORM($ParReportID)
	{
		$sql = "SELECT
					a.ClassLevelID
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN YEAR as b ON a.ClassLevelID = b.YearID
				WHERE
					a.ReportID = '$ParReportID'
					AND b.RecordStatus = 1
				";
		$row = $this->returnVector($sql);

		return $row;
	}



	/*
	* Get Report Subject
	*/
	function GET_REPORT_SUBJECT_GRADING($ParReportID, $ParFormArr="")
	{
		global $eclass_db;

		# Select Subject and Check as is set as special
		if($ParFormArr!="")
		{
			$FormList = implode(",", $ParFormArr);
			$cond = " AND (c.ClassLevelID IS NULL OR c.ClassLevelID IN ($FormList))";
		}
		$sql = "SELECT
					a.SubjectID,
					b.SubjectGradingID,
					c.ClassLevelID
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME as b ON a.SubjectID = b.SubjectID
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as c ON b.SubjectGradingID = c.SubjectGradingID
				WHERE
					a.ReportID = '$ParReportID'
					 $cond
				ORDER BY
					a.DisplayOrder
				";
		$row = $this->returnArray($sql, 3);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $SubjectGradingID, $Form) = $row[$i];
			if($Form=="")
				$SubjectGradingArr[$SubjectID]["default"] = $SubjectGradingID;
			else
				$SubjectGradingArr[$SubjectID]["special"] = $SubjectGradingID;
		}

		if(!empty($SubjectGradingArr))
		{
			foreach($SubjectGradingArr as $SubjectID => $Data)
			{
				$ReturnArr[$SubjectID] = (empty($SubjectGradingArr[$SubjectID]["special"])) ? $SubjectGradingArr[$SubjectID]["default"] : $SubjectGradingArr[$SubjectID]["special"];
			}
		}

		return $ReturnArr;
	}


	function GET_WEIGHTING($ParSubjectGradingID) {

		$Sql = "
					SELECT
							SchemeID
					FROM
							RC_SUBJECT_GRADING_SCHEME
					WHERE
							SubjectGradingID = '$ParSubjectGradingID'
				";
		$row = $this->returnVector($Sql);
		$SchemeID = $row[0];

		$Sql = "
					SELECT
							Weighting
					FROM
							RC_GRADING_SCHEME
					WHERE
							SchemeID = '$SchemeID'
				";
		$row = $this->returnVector($Sql);
		$temp = $row[0];

		return $temp;
	}

	function DISPLAY_WEIGHTING($ParSubjectGradingID, $ParTotalWeighting = 0, $ParIsParent = 0) {

		if ($ParIsParent) return "100%";

		$Sql = "
					SELECT
							SchemeID
					FROM
							RC_SUBJECT_GRADING_SCHEME
					WHERE
							SubjectGradingID = '$ParSubjectGradingID'
				";
		$row = $this->returnVector($Sql);
		$SchemeID = $row[0];

		$Sql = "
					SELECT
							Weighting
					FROM
							RC_GRADING_SCHEME
					WHERE
							SchemeID = '$SchemeID'
				";
		$row = $this->returnVector($Sql);
		$temp = $row[0];

		if ($ParTotalWeighting != 0) {
			$temp = round($temp / $ParTotalWeighting * 100);
		} else {
			$temp = "100";
		}

		return $temp."%";
	}



	/*
	* Get Report Scheme ID
	*/
	function GET_REPORT_SCHEME_ID($ParReportID, $ParFormArr="")
	{
		global $eclass_db;

		# Select Subject and Check as is set as special
		if($ParFormArr!="")
		{
			$FormList = implode(",", $ParFormArr);
			$cond = " AND (c.ClassLevelID IS NULL OR c.ClassLevelID IN ($FormList))";
		}
		$sql = "SELECT
					a.SubjectID,
					b.SchemeID,
					c.ClassLevelID
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME as b ON a.SubjectID = b.SubjectID
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as c ON b.SubjectGradingID = c.SubjectGradingID
				WHERE
					a.ReportID = '$ParReportID'
					 $cond
				ORDER BY
					a.DisplayOrder
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $SchemeID, $Form) = $row[$i];
			if($Form=="")
				$SchemeIDArr[$SubjectID]["default"] = $SchemeID;
			else
				$SchemeIDArr[$SubjectID]["special"] = $SchemeID;
		}

		if(!empty($SchemeIDArr))
		{
			foreach($SchemeIDArr as $SubjectID => $Data)
			{
				$ReturnArr[$SubjectID] = (empty($SchemeIDArr[$SubjectID]["special"])) ? $SchemeIDArr[$SubjectID]["default"] : $SchemeIDArr[$SubjectID]["special"];
			}
		}

		return $ReturnArr;
	}

	/*
	* Get Report Template Subject
	*/
	function GET_REPORT_TEMPLATE_SUBJECT($ParReportID, $ParSubjectGradingArr="")
	{
		global $eclass_db;

		if(!empty($ParSubjectGradingArr))
		{
			$SubjectGradingList = implode(",", $ParSubjectGradingArr);
			$cond = " AND c.SubjectGradingID IN ($SubjectGradingList)";
		}
		$sql = "SELECT
					a.ReportSubjectID,
					a.SubjectID,
					b.CODEID,
					if(b.CMP_CODEID IS NULL || b.CMP_CODEID = '', 0, b.CMP_CODEID),
					b.EN_DES,
					b.CH_DES,
					if(d.FullMark IS NULL, '--', d.FullMark),
					c.Scale,
					a.HidePosition
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME as c ON a.SubjectID = c.SubjectID
					LEFT JOIN RC_GRADING_SCHEME as d ON c.SchemeID = d.SchemeID
				WHERE
					a.ReportID = '$ParReportID'
					And
					b.RecordStatus = 1
					$cond
				ORDER BY
					a.DisplayOrder
				";
		$row = $this->returnArray($sql, 9);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($ReportSubjectID, $SubjectID, $CODEID, $CMP_CODEID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition) = $row[$i];
			$ReturnArr[$CODEID][$CMP_CODEID] = array($ReportSubjectID, $SubjectID, $EngSubjectName, $ChiSubjectName, $FullMark, $Scale, $HidePosition);
		}

		return $ReturnArr;
	}

	/*
	* Get report cell setting
	*/
	function GET_REPORT_CELL($ParReportID)
	{
		$sql = "SELECT
					a.ReportColumnID,
					a.ReportSubjectID,
					a.Setting
				FROM
					RC_REPORT_TEMPLATE_CELL as a
					LEFT JOIN RC_REPORT_TEMPLATE_COLUMN as b ON a.ReportColumnID = b.ReportColumnID
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as c ON a.ReportSubjectID = c.ReportSubjectID
					LEFT JOIN RC_REPORT_TEMPLATE as d ON b.ReportID = d.ReportID AND c.ReportID = d.ReportID
				WHERE
					d.ReportID = '$ParReportID'
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($rc_id, $rs_id, $setting) = $row[$i];
			$ReturnArr[$rc_id][$rs_id] = $setting;
		}

		Return $ReturnArr;
	}

	/*
	* Get report cell setting by ReportID and SubjectID
	*/
	function GET_REPORT_CELL_BY_SUBJECT($ParReportID, $ParSubjectID)
	{
		$sql = "SELECT
					a.ReportColumnID,
					a.Setting
				FROM
					RC_REPORT_TEMPLATE_CELL as a
					LEFT JOIN RC_REPORT_TEMPLATE_COLUMN as b ON a.ReportColumnID = b.ReportColumnID
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as c ON a.ReportSubjectID = c.ReportSubjectID
				WHERE
					b.ReportID = '$ParReportID'
					AND c.ReportID = '$ParReportID'
					AND c.SubjectID = '$ParSubjectID'
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($rc_id, $setting) = $row[$i];
			$ReturnArr[$rc_id] = $setting;
		}

		Return $ReturnArr;
	}


	/*
	* Get report subject ID
	*/
	function GET_REPORT_COLUMN_DETAILS($ParReportColumnID)
	{
		$sql = "SELECT
					ColumnTitle,
					Weight,
					DisplayOrder,
					ShowPosition,
					PositionRange,
					MarkSummaryWeight,
					IsColumnSum,
					NotForInput
				FROM
					RC_REPORT_TEMPLATE_COLUMN
				WHERE
					ReportColumnID = '".$ParReportColumnID."'
				";
		$row = $this->returnArray($sql, 8);

		return $row[0];
	}

	/*
	* Get Report Columns Title for Insert Position Selection
	*/
	function GET_REPORT_COLUMN_INSERT_SELECTION($ParReportID, $ParReportColumnID="")
	{
		global $eReportCard;

		$sql = "SELECT
					ReportColumnID,
					ColumnTitle
				FROM
					RC_REPORT_TEMPLATE_COLUMN
				WHERE
					ReportID = '".$ParReportID."'
				ORDER BY
					DisplayOrder
				";
		$row = $this->returnArray($sql, 2);

		$ColumnSelect = "<SELECT name='InsertAfterID'>";
		$ColumnSelect .= "<OPTION value=''>(".$eReportCard['Beginning'].")</OPTION>";
		$RowSize = sizeof($row);
		for($i=0; $i<$RowSize; $i++)
		{
			list($ReportColumnID, $ColumnTitle) = $row[$i];
			if($row[$i][0]!=$ParReportColumnID)
			{
				$Selected = (($ParReportColumnID!="" && $row[$i+1][0]==$ParReportColumnID) || ($ParReportColumnID=="" && ($i+1)==$RowSize)) ? "SELECTED='SELECTED'" : "";
				$ColumnSelect .= "<OPTION value='".$ReportColumnID."' $Selected>".$ColumnTitle."</OPTION>";
			}
		}
		$ColumnSelect .= "</SELECT>";

		return $ColumnSelect;
	}

	/*
	* Get Report Selected Subjects
	*/
	function GET_REPORT_SELECTED_SUBJECTS($ParReportID)
	{
		global $eclass_db;

		$sql = "SELECT
					a.ReportSubjectID,
					if(b.CMP_CODEID IS NULL || b.CMP_CODEID = '', 0, 1) as IsComponent,
					CONCAT(b.EN_DES, ' (', b.CH_DES, ')'),
					a.SubjectID
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT jOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
				WHERE
					a.ReportID = '$ParReportID'
					And
					b.RecordStatus = 1
				ORDER BY
					a.DisplayOrder
				";
		$ReturnArr = $this->returnArray($sql, 4);

		return $ReturnArr;
	}

	/*
	* Get Report Subjects for Insert Position Selection
	*/
	function GET_REPORT_SUBJECT_INSERT_SELECTION($ParReportID, $ParExclusiveReportSubjectID="")
	{
		global $eclass_db, $eReportCard;

		$SubjectArray = $this->GET_REPORT_SELECTED_SUBJECTS($ParReportID);

		$SubjectSelect = "<SELECT name='InsertAfterID'>";
		$SubjectSelect .= "<OPTION value=''>(".$eReportCard['Beginning'].")</OPTION>";
		for($i=0; $i<sizeof($SubjectArray); $i++)
		{
			# skip unwanted subject(s)
			if($SubjectArray[$i][0]==$ParExclusiveReportSubjectID)
			{
				if($SubjectArray[$i][1]==0)
				{
					$i++;
					while($SubjectArray[$i][1]==1)
					{
						$i++;
					}
				}
				else
					$i++;
			}
			list($ReportSubjectID, $IsComponent, $SubjectName) = $SubjectArray[$i];
			if($ReportSubjectID>0)
			{
				$Selected = ($SubjectArray[$i+1][0]==$ParExclusiveReportSubjectID) ? "SELECTED='SELECTED'" : "";
				$SubjectName = ($IsComponent==1) ? "&nbsp;&nbsp;&nbsp;".$SubjectName : $SubjectName;
				$SubjectSelect .= "<OPTION value='".$ReportSubjectID."' $Selected>".$SubjectName."</OPTION>";
			}
		}

		$SubjectSelect .= "</SELECT>";

		return $SubjectSelect;
	}

	/*
	* Get Report Selected SubjectID only
	*/
	function GET_REPORT_SELECTED_SUBJECT_ID($ParReportID)
	{
		$sql = "SELECT
					SubjectID
				FROM
					RC_REPORT_TEMPLATE_SUBJECT
				WHERE
					ReportID = '$ParReportID'
				ORDER BY
					DisplayOrder
				";
		$row = $this->returnVector($sql);

		return $row;
	}


	/*
	* Get Report Selected SubjectID only
	*/
	function GET_SUBJECT_ID_WITHOUT_REPORTID()
	{
		$sql = "SELECT
					Distinct(SubjectID)
				FROM
					RC_REPORT_TEMPLATE_SUBJECT
				";
		$row = $this->returnVector($sql);

		return $row;
	}

	/*
	* Get Subjects Selection for report builder
	*/
	function GET_REPORT_SUBJECT_SELECTION($ParReportID, $ParCheck = 0)
	{
		$SubjectArray = $this->GET_ALL_SUBJECTS(1);
		$SelectedSubjectIDArray = $this->GET_REPORT_SELECTED_SUBJECT_ID($ParReportID);

		if(is_array($SubjectArray))
		{
			$SubjectSelect = "<SELECT name='SubjectID'>";
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpSubjectID => $SubjectName)
					{
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
						$Discard = 0;
						if(is_array($SelectedSubjectIDArray) && in_array($SubjectID, $SelectedSubjectIDArray))
						{
							$Discard = 1;
							if($CmpSubjectID==0)
							{
								# check whether it has component subjects
								$RelatedSubjectArray = array_keys($SubjectArray[$SubjectID]);
								array_shift($RelatedSubjectArray);
								if(!empty($RelatedSubjectArray))
								{
									$Result = array_diff($RelatedSubjectArray, $SelectedSubjectIDArray);
									$Discard = (empty($Result)) ? 1 : 0;
								}
							}
						}

						if($Discard==0)
						{
							$SubjectName = ($CmpSubjectID>0) ? "&nbsp;&nbsp;&nbsp;&nbsp;".$SubjectName : $SubjectName;

							if ($ParCheck) {
								if ($this->HAVE_GRADING_SCHEME($SubjectID)) {
									$TempPass = true;
								} else
									$TempPass = false;
							} else {
								$TempPass = true;
							}

							if ($TempPass) $SubjectSelect .= "<OPTION value='".$SubjectID."'>".$SubjectName."</OPTION>";
						}
					}
				}
			}
			$SubjectSelect .= "</SELECT>";
		}

		return $SubjectSelect;
	}


	/*
	* Get Subjects Selection for report builder
	*/
	function GET_REPORT_SELECTED_SUBJECT_SELECTION($ParReportID, $ParCheck = 0)
	{
		$SubjectArray = $this->GET_ALL_SUBJECTS(1);
		$SelectedSubjectIDArray = $this->GET_REPORT_SELECTED_SUBJECT_ID($ParReportID);

		if(is_array($SubjectArray))
		{
			$SubjectSelect = "<SELECT name='SubjectID'>";
			foreach($SubjectArray as $SubjectID => $Data)
			{
				if(is_array($Data))
				{
					foreach($Data as $CmpSubjectID => $SubjectName)
					{
						$SubjectID = ($CmpSubjectID==0) ? $SubjectID : $CmpSubjectID;
						$Discard = 0;
						if(is_array($SelectedSubjectIDArray) && in_array($SubjectID, $SelectedSubjectIDArray))
						{
							//$Discard = 1;
							if($CmpSubjectID==0)
							{
								# check whether it has component subjects
								$RelatedSubjectArray = array_keys($SubjectArray[$SubjectID]);
								array_shift($RelatedSubjectArray);
								if(!empty($RelatedSubjectArray))
								{
									$Result = array_diff($RelatedSubjectArray, $SelectedSubjectIDArray);
									$Discard = (empty($Result)) ? 0 : 1;
								}
							}
						} else {
							$Discard = 1;
						}


						if($Discard==0)
						{
							$SubjectName = ($CmpSubjectID>0) ? "&nbsp;&nbsp;&nbsp;&nbsp;".$SubjectName : $SubjectName;

							if ($ParCheck) {
								if ($this->HAVE_GRADING_SCHEME($SubjectID)) {
									$TempPass = true;
								} else
									$TempPass = false;
							} else {
								$TempPass = true;
							}

							if ($TempPass) $SubjectSelect .= "<OPTION value='".$SubjectID."'>".$SubjectName."</OPTION>";
						}
					}
				}
			}
			$SubjectSelect .= "</SELECT>";
		}

		return $SubjectSelect;
	}

###################################### Report Builder Functions End ########################################
#############################################################################################################

################################## Marksheet Collection Functions Start #####################################

	/*
	* Get marksheet collection period
	*/
	function GET_MARKSHEET_COLLECTION()
	{
		$sql = "SELECT StartDate, EndDate, Year, Semester, AllowClassTeacherComment, AllowSubjectTeacherComment, AllowClassTeacherUploadCSV FROM RC_MARKSHEET_COLLECTION";
		$row = $this->returnArray($sql, 6);

		return $row[0];
	}

	/*
	* Get marksheet collection period
	*/
	function GET_MARKSHEET_COLLECTION_YEAR()
	{
		$sql = "SELECT Year FROM RC_MARKSHEET_COLLECTION";
		$row = $this->returnVector($sql);

		return $row[0];
	}


	/*
	* Get marksheet collection period
	*/
	function GET_MARKSHEET_VERIFICATION()
	{
		$sql = "SELECT StartDate, EndDate FROM RC_MARKSHEET_VERIFICATION";
		$row = $this->returnArray($sql, 2);

		return $row;
	}

	/*
	* Get duplicated classes in reports
	*/
	function GET_DUPLICATED_CLASS_REPORT_TABLE()
	{
		global $eReportCard;

		$sql = "SELECT
					a.ReportID,
					REPLACE(b.ReportTitle, '::', ' '),
					a.ClassLevelID,
					c.YearName as LevelName
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN RC_REPORT_TEMPLATE as b ON a.ReportID = b.ReportID
					LEFT JOIN YEAR as c ON a.ClassLevelID = c.YearID
				WHERE
					b.RecordStatus = 1
				ORDER BY
					a.ClassLevelID,
					a.ReportID
				";
		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($report_id, $report_title, $level_id, $level_name) = $row[$i];
			$ReportLevelArray[$level_name][] = $report_title;
		}
		if(is_array($ReportLevelArray))
		{
			foreach($ReportLevelArray as $LevelName => $ReportArray)
			{
				if(sizeof($ReportArray)>1)
				{
					$DuplicatedArray[] = array($LevelName, $ReportArray);
				}
			}
		}

		if(!empty($DuplicatedArray))
		{
			$ReturnContent = "<table width='90%' align=\"center\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td>";
			$ReturnContent .= "<tr><td class=\"tabletext\" colspan=2>".$eReportCard['DuplicatedClassReportRemind']."</td></tr>";
			$ReturnContent .= "<tr><td>";
			$ReturnContent .= "<table width='100%' border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td>";
			$ReturnContent .= "<tr>
								<td class='tabletoplink tablebluetop'>".$eReportCard['Form']."</td>
								<td class='tabletoplink tablebluetop'>".$eReportCard['Reports']."</td>
							</tr>
							";
			for($i=0; $i<sizeof($DuplicatedArray); $i++)
			{
				$css = ($i%2==0) ? 1 : 2;
				list($LevelName, $ReportArray) = $DuplicatedArray[$i];

				$ReturnContent .= "<tr>
									<td class='tablebluerow$css tabletext' valign='top'>".$LevelName."</td>
									<td class='tablebluerow$css tabletext'>";
				for($j=0; $j<sizeof($ReportArray); $j++)
				{
					$ReturnContent .= "<li>".$ReportArray[$j]."</li>";
				}
				$ReturnContent .= "</td>";
				$ReturnContent .= "</tr>";
			}
			$ReturnContent .= "</table>";
			$ReturnContent .= "</td></tr>";
			$ReturnContent .= "</table>";
		}

		return $ReturnContent;
	}

################################### Marksheet Collection Functions End #####################################
#############################################################################################################

################################## Marksheet Submission Functions Start #####################################

	/*
	* check whether the time is in marksheet submission period
	*/
	function CHECK_MARKSHEET_SUBMISSION_PERIOD()
	{
		$ValidPeriod = (empty($this->StartPrevDate) || empty($this->EndNextDate)) ? 0 : validatePeriod($this->StartPrevDate, $this->EndNextDate);

		return $ValidPeriod;
	}

	/*
	* check whether the time is in marksheet verification period
	*/
	function CHECK_MARKSHEET_VERIFICATION_PERIOD()
	{
		$VerificationPeriod = $this->GET_MARKSHEET_VERIFICATION();
		$StartDate = $VerificationPeriod[0][0];
		$EndDate = $VerificationPeriod[0][1];

		$ValidPeriod = (empty($StartDate) || empty($EndDate)) ? 0 : validatePeriod($StartDate, $EndDate);

		return $ValidPeriod;
	}

	/*
	* Get Teaching Subject(s) and Class(s)
	*/
	function GET_TEACHER_SUBJECT_CLASS()
	{
		global $eclass_db;
		
		if ($this->Semester == 'FULL' || $this->Semester == 0)
		{
			$AcademicYearObj = new academic_year($this->Year);
			$YearTermIDArr = $AcademicYearObj->Get_Term_List($returnAsso=0);
			$YearTermIDArr = Get_Array_By_Key($YearTermIDArr, 'YearTermID');
		}
		else
		{
			$YearTermIDArr = array($this->Semester);
		}
		

		$sql = "SELECT DISTINCT
					st.SubjectID,
					CONCAT(b.EN_DES, '<br />', b.CH_DES),
					yc.YearClassID as ClassID,
					yc.ClassTitleEN as ClassName
				FROM
					SUBJECT_TERM_CLASS_TEACHER as stct
					Left Join SUBJECT_TERM as st On (stct.SubjectGroupID = st.SubjectGroupID)
					LEFT JOIN ASSESSMENT_SUBJECT as b ON st.SubjectID = b.RecordID
					Left Join SUBJECT_TERM_CLASS_USER as stcu On (stct.SubjectGroupID = stcu.SubjectGroupID)
					Left Join YEAR_CLASS_USER as ycu On (stcu.UserID = ycu.UserID)
					Left Join YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as d ON st.SubjectID = d.SubjectID
					LEFT JOIN RC_REPORT_TEMPLATE as e ON d.ReportID = e.ReportID
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as f ON e.ReportID = f.ReportID
				WHERE
					stct.UserID = '".$this->uid."'
					and yc.AcademicYearID = '".$this->Year."'
					and st.YearTermID in (".implode(',', $YearTermIDArr).")
					AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
					AND e.RecordStatus = 1
					AND yc.YearID = f.ClassLevelID
					And b.RecordStatus = 1
				Group By
					st.SubjectID, yc.YearClassID
				ORDER BY
					b.DisplayOrder,
					yc.Sequence
				";
		$row = $this->returnArray($sql, 4);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($sid, $sname, $cid, $cname) = $row[$i];
			$ReturnArray[$sid]["name"] = $sname;
			$ReturnArray[$sid]["class"][$cid] = $cname;
		}

		return $ReturnArray;
	}

	/*
	* Get NotificationID
	*/
	function GET_ALL_NOTIFICATIONS($ParReportID)
	{
		$sql = "SELECT
					a.ClassID,
					a.SubjectID
				FROM
					RC_MARKSHEET_NOTIFICATION as a
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as b ON a.SubjectID = b.SubjectID
					LEFT JOIN INTRANET_CLASS as c ON a.ClassID = c.ClassID
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as d ON c.ClassLevelID = d.ClassLevelID
				WHERE
					b.ReportID = '$ParReportID'
					AND d.ReportID = '$ParReportID'
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($ClassID, $SubjectID) = $row[$i];
			$ReturnArray[$ClassID][$SubjectID] = 1;
		}

		return $ReturnArray;
	}

	/*
	* Get NotificationID
	*/
	function GET_NOTIFICATION($ParSubjectID, $ParClassID)
	{
		$sql = "SELECT
					NotificationID,
					ShowMainMarksheet,
					ShowSubMarksheet
				FROM
					RC_MARKSHEET_NOTIFICATION
				WHERE
					SubjectID = '$ParSubjectID'
					AND ClassID = '$ParClassID'
				";
		$row = $this->returnArray($sql, 3);

		return $row;
	}

	/*
	* Get Last Modified of Marksheet Submission
	*/
	function GET_MARKSHEET_LAST_MODIFIED($ParSubjectList, $ParClassID)
	{
		global $eclass_db;

		if($ParSubjectList!="")
		{
			$sql = "SELECT
						MAX(a.DateModified)
					FROM
						RC_MARKSHEET_SCORE as a
						LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
						LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
					WHERE
						a.SubjectID IN ($ParSubjectList)
						AND c.ClassID = '$ParClassID'
						AND a.Year = '".$this->Year."'
						AND a.Semester = '".$this->Semester."'
						AND b.RecordType = 2
						AND b.RecordStatus = 1
					";
			$row = $this->returnVector($sql);
		}

		return $row[0];
	}

	/*
	* Get ReportID by SubjectID and ClassID (edit by aki 20070928)
	*/
	function GET_REPORTID_BY_CLASS($ParClassID)
	{
		$sql = "SELECT
					a.ReportID
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN YEAR_CLASS as b ON a.ClassLevelID = b.YearID
					LEFT JOIN RC_REPORT_TEMPLATE as c ON a.ReportID = c.ReportID
				WHERE
					b.YearClassID = '$ParClassID'
					And
					b.AcademicYearID = '".$this->Year."'
					AND c.RecordStatus = 1
				LIMIT 1
				";
				//AND c.ReportType LIKE '".$this->Semester."'
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get ReportID by SubjectID and ClassID (edit by aki 20070928)
	*/
	function GET_REPORTID_BY_CLASSLEVEL($ParClassLevelID)
	{
		$sql = "SELECT
					a.ReportID
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN INTRANET_CLASS as b ON a.ClassLevelID = b.ClassLevelID
					LEFT JOIN RC_REPORT_TEMPLATE as c ON a.ReportID = c.ReportID
				WHERE
					b.ClassLevelID = '$ParClassLevelID'
					AND c.RecordStatus = 1
				LIMIT 1
				";
				//AND c.ReportType LIKE '".$this->Semester."'
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get Subject Name
	*/
	function GET_SUBJECT_NAME($ParSubjectID)
	{
		global $eclass_db;

		$sql = "SELECT
					a.EN_DES,
					a.CH_DES,
					b.EN_DES,
					b.CH_DES
				FROM
					ASSESSMENT_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.CODEID = b.CODEID AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
				WHERE
					a.RecordID = '$ParSubjectID'
					And
					a.RecordStatus = 1
					And
					b.RecordStatus = 1
				";
		$row = $this->returnArray($sql, 4);

		list($SubjectEngName, $SubjectChiName, $ParentSubjectEngName, $ParentSubjectChiName) = $row[0];
		$SubjectName = ($SubjectEngName==$ParentSubjectEngName) ? $SubjectEngName."&nbsp;".$SubjectChiName : $ParentSubjectEngName." - ".$SubjectEngName."&nbsp;".$ParentSubjectChiName." - ".$SubjectChiName;

		return $SubjectName;
	}

	/*
	* Check whether cell that is not null exist.
	*/
	function CHECK_REPORT_CELL_NULL($ParReportID, $ParSubjectID)
	{

		$sql = "SELECT
					COUNT(a.Setting)
				FROM
					RC_REPORT_TEMPLATE_CELL as a
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as b ON a.ReportSubjectID = b.ReportSubjectID
				WHERE
					b.SubjectID = '$ParSubjectID'
					AND b.ReportID = '$ParReportID'
					AND a.Setting <> 'N/A'
				";
		$row = $this->returnVector($sql);

		Return $row[0];
	}

	/*
	* Get grading scheme by SubjectID and ClassID
	*/
	function GET_GRADING_SCHEME_BY_SUBJECT_CLASS($ParSubjectID, $ParClassID)
	{
		$sql = "SELECT
					a.SchemeID,
					yc.YearClassID,
					a.Scale,
					d.FullMark
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID
					LEFT JOIN YEAR_CLASS as yc On (b.ClassLevelID = yc.YearID And yc.AcademicYearID = '".$this->Year."')
					LEFT JOIN RC_GRADING_SCHEME as d ON a.SchemeID = d.SchemeID
				WHERE
					a.SubjectID = '$ParSubjectID'
				";
		$row = $this->returnArray($sql,4);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($SchemeID, $ClassID, $Scale, $FullMark) = $row[$i];
			if($ParClassID==$ClassID)
			{
				$ReturnValue = array($SchemeID, $Scale, $FullMark);
				break;
			}
		}
		if(empty($ReturnValue))
		{
			for($i=0; $i<sizeof($row); $i++)
			{
				list($SchemeID, $ClassID, $Scale, $FullMark) = $row[$i];
				if($ClassID=="")
				{
					$ReturnValue = array($SchemeID, $Scale, $FullMark);
					break;
				}
			}
			//$ReturnValue = array($row[0][0], $row[0][2], $row[0][3]);
		}

		return $ReturnValue;
	}


	/*
	* Check if the Subject have grading scheme
	*/
	function HAVE_GRADING_SCHEME($ParSubjectID)
	{
		$sql = "SELECT
					a.SchemeID,
					c.ClassID,
					a.Scale,
					d.FullMark
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID
					LEFT JOIN INTRANET_CLASS as c ON b.ClassLevelID = c.ClassLevelID AND c.RecordStatus = 1
					LEFT JOIN RC_GRADING_SCHEME as d ON a.SchemeID = d.SchemeID
				WHERE
					a.SubjectID = '$ParSubjectID'
				GROUP BY
					a.SchemeID
				";
		$row = $this->returnArray($sql,4);

		return (sizeof($row) > 0);
	}



	/*
	* Get grading scheme by SubjectID and ClassID
	*/
	function GET_SUBJECT_GRADEMARK($ParClassID, $ParSubjectIDList)
	{
		//2012-0719-1026-32096
//		$sql = "SELECT
//					a.SubjectID,
//					a.SubjectGradingID,
//					c.ClassID,
//					a.Scale
//				FROM
//					RC_SUBJECT_GRADING_SCHEME as a
//					LEFT jOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID
//					LEFT jOIN INTRANET_CLASS as c ON b.ClassLevelID = c.ClassLevelID AND c.ClassID = '$ParClassID' AND c.RecordStatus = 1
//				WHERE
//					a.SubjectID IN ($ParSubjectIDList)
//				";
				
		$sql = "SELECT
					a.SubjectID,
					a.SubjectGradingID,
					c.YearClassID as ClassID,
					a.Scale
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT jOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID
					LEFT jOIN YEAR_CLASS as c ON b.ClassLevelID = c.YearID AND c.YearClassID = '$ParClassID'
				WHERE
					a.SubjectID IN ($ParSubjectIDList)
				Order By
					a.DateModified desc
				";
		$row = $this->returnArray($sql, 4);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $SubjectGradingID, $ClassID, $Scale) = $row[$i];
			if(empty($SubjectGradingArray[$SubjectID]) || $ParClassID==$ClassID)
			{
				$SubjectGradingArray[$SubjectID] = $SubjectGradingID;
				$SubjectScaleArray[$SubjectID] = $Scale;
			}
		}

		if(is_array($SubjectGradingArray))
		{
			$SubjectGradingIDArray = array_unique(array_values($SubjectGradingArray));
			$SubjectGradingIDList = implode(",", $SubjectGradingIDArray);
			list($FailedArray, $DistinctionArray, $PFArray) = $this->GET_REPORT_GRADEMARK($SubjectGradingIDList);
		}

		return array($FailedArray, $DistinctionArray, $PFArray, $SubjectScaleArray);
	}

	/*
	* Get Scheme GRADING
	*/
	function GET_SCHEME_GRADE($ParSchemeID)
	{
		global $button_select;

		$sql = "SELECT DISTINCT
					Grade
				FROM
					RC_GRADING_SCHEME_GRADEMARK
				WHERE
					SchemeID = '$ParSchemeID'
				ORDER BY
					Grade
				";
		$row = $this->returnVector($sql);

		return $row;
	}

	/*
	* Get Scheme Pass Fail
	*/
	function GET_SCHEME_PASS_FAIL($ParSchemeID)
	{
		global $button_select;

		$sql = "SELECT DISTINCT
					Pass,
					Fail
				FROM
					RC_GRADING_SCHEME
				WHERE
					SchemeID = '$ParSchemeID'
				";
		$row = $this->returnArray($sql,2);
		list($Pass, $Fail) = $row[0];

		return array($Pass, $Fail);
	}

	/*
	* Get report subject ID
	*/
	function GET_REPORT_SUBJECT_ID($ParReportID, $ParSubjectID="")
	{
		$cond = ($ParSubjectID!="") ? " AND SubjectID = '$ParSubjectID'" : "";
		$sql = "SELECT ReportSubjectID FROM RC_REPORT_TEMPLATE_SUBJECT WHERE ReportID = '".$ParReportID."' $cond";
		$row = $this->returnVector($sql);

		return $row;
	}

	/*
	* Get Report Template Columnb
	*/
	function GET_REPORT_TEMPLATE_COLUMN($ParReportID)
	{
		$sql = "SELECT
					ReportColumnID,
					ColumnTitle,
					Weight,
					ShowPosition,
					PositionRange,
					IsColumnSum,
					MarkSummaryWeight,
					NotForInput
				FROM
					RC_REPORT_TEMPLATE_COLUMN
				WHERE
					ReportID = '".$ParReportID."'
				ORDER BY
					DisplayOrder
				";
		$row = $this->returnArray($sql, 7);

		return $row;
	}

	/*
	* Get Report Template Columnb
	*/
	function GET_IS_COLUMN_SUM_ARR($ParReportID)
	{
		$sql = "SELECT
					ColumnTitle,
					IsColumnSum
				FROM
					RC_REPORT_TEMPLATE_COLUMN
				WHERE
					ReportID = '".$ParReportID."'
				ORDER BY
					DisplayOrder
				";
		$row = $this->returnArray($sql, 2);

		for ($i = 0; $i < sizeof($row); $i++)
		{
			$TempTitle = $row[$i][0];
			$ReturnArr[$i][$TempTitle] = $row[$i][1];
		}

		return $ReturnArr;
	}


	/*
	* Get Student List by ClassID
	*/
	function GET_STUDENT_BY_CLASS($ParClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=0)
	{
		$cond = ($ParStudentIDList!="") ? " AND u.UserID IN ($ParStudentIDList)" : "";
		
		if ($withClassNumber == 1)
		{
			$NameField = getNameFieldWithClassNumberByLang('u.');
			$ArchiveNameField = getNameFieldWithClassNumberByLang('au.');
		}
		else
		{
			if ($isShowBothLangs)
			{
				$NameField = "	CONCAT(	TRIM(u.EnglishName), 
										If (
											u.ChineseName Is Not Null Or u.ChineseName != '',
											Concat(' (', TRIM(u.ChineseName), ')'),
											''
										)
								)
							";
				$ArchiveNameField = "	CONCAT(	TRIM(au.EnglishName), 
												If (
													au.ChineseName Is Not Null Or au.ChineseName != '',
													Concat(' (', TRIM(au.ChineseName), ')'),
													''
												)
										)
									";
			}
			else
			{
				$NameField = getNameFieldByLang2("u.");
				$ArchiveNameField = getNameFieldByLang2("au.");
			}
			//$NameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("u.") : "CONCAT(TRIM(u.EnglishName), ' (', TRIM(u.ChineseName), ')')";
			//$ArchiveNameField = ($isShowBothLangs == 0) ? getNameFieldByLang2("au.") : "CONCAT(TRIM(au.EnglishName), ' (', TRIM(au.ChineseName), ')')";
		}
		
		
		
		if ($isShowStyle==0)
			$starHTML = '*';
		else
			$starHTML = '<font style="color:red;">*</font>';
			
		$sql = "SELECT 
						DISTINCT(ycu.UserID), 
						u.WebSAMSRegNo, 
						ycu.ClassNumber as ClassNumber,
						CASE 
							WHEN au.UserID IS NOT NULL then CONCAT('$starHTML',".$ArchiveNameField.") 
							WHEN u.RecordStatus = 3  THEN CONCAT('$starHTML',".$NameField.") 
							ELSE ".$NameField." 
						END as StudentName,
						yc.ClassTitleEN as ClassName,
						u.EnglishName as StudentNameEn,
						u.ChineseName as StudentNameCh,
						yc.ClassTitleEN as ClassTitleEn,
						yc.ClassTitleB5 as ClassTitleCh,
						u.WebSAMSRegNo,
						u.UserLogin
				FROM 
						YEAR_CLASS_USER as ycu
						INNER JOIN 
						YEAR_CLASS as yc
						ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$this->Year."')
						INNER JOIN
						INTRANET_USER as u 
						ON (ycu.UserID = u.UserID)
						Left Join
						INTRANET_ARCHIVE_USER as au 
						ON (u.UserID = au.UserID) 
				WHERE 
						yc.YearClassID = '$ParClassID'
						$cond
				ORDER BY 
						yc.Sequence,
						ycu.ClassNumber 
				";
		$row_student = $this->returnArray($sql);
		
		$ReturnArr = array();
		if ($ReturnAsso==1)
		{
			foreach ($row_student as $key => $thisStudentInfoArr)
			{
				$thisStudentID = $thisStudentInfoArr['UserID'];
				$ReturnArr[$thisStudentID] = $thisStudentInfoArr;
			}
		}
		else
		{
			$ReturnArr = $row_student;
		}
		
		return $ReturnArr;
	}

	/*
	* Get Student ID by ClassID
	*/
	function GET_STUDENTID_BY_CLASS($ParClassID)
	{
		$sql = "SELECT
					iu.UserID
				FROM
					YEAR_CLASS_USER as ycu
					Inner Join
					INTRANET_USER as iu On (ycu.UserID = iu.UserID)
				WHERE
					ycu.YearClassID = '$ParClassID'
					AND iu.RecordType = 2
					AND iu.RecordStatus = 1
				";
		$row = $this->returnVector($sql);

		return $row;
	}


	/*
	* Get report cell setting by ReposrtSubjectID and ReportColumnID
	*/
	function GET_REPORT_CELL_BY_SUBJECT_COLUMN($ParReportSubjectID, $ParReportColumnList)
	{
		$sql = "SELECT
					ReportColumnID,
					Setting
				FROM
					RC_REPORT_TEMPLATE_CELL
				WHERE
					ReportSubjectID = '$ParReportSubjectID'
					AND ReportColumnID IN ($ParReportColumnList)
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($rc_id, $setting) = $row[$i];
			$ReturnArray[$rc_id] = $setting;
		}

		Return $ReturnArray;
	}

	/*
	* Get Component SubjectID in the report by parent SubjectID
	*/
	function GET_REPORT_CMP_SUBJECT($ParSubjectID, $ParReportID)
	{
		global $eclass_db;

		$sql = "SELECT
					b.RecordID,
					CONCAT(b.EN_DES, ' (', b.CH_DES, ')')
				FROM
					ASSESSMENT_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.CODEID = b.CODEID
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as c ON b.RecordID = c.SubjectID
				WHERE
					c.ReportID = '$ParReportID'
					AND a.RecordID = '$ParSubjectID'
					AND c.ReportSubjectID IS NOT NULL
					And a.RecordStatus = 1
					And b.RecordStatus = 1
					And (b.CMP_CODEID IS NOT NULL Or b.CMP_CODEID != '')
					And a.RecordID != b.RecordID
				";
		$row = $this->returnArray($sql, 2);
		return $row;
	}

	/*
	* Get Component SubjectID with weighting in the report by parent SubjectID
	*/
	function GET_REPORT_CMP_SUBJECT_WEIGHTING($ParSubjectID, $ParReportID, $ParClassID)
	{
		global $eclass_db;

		$sql = "SELECT
					b.RecordID
				FROM
					ASSESSMENT_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.CODEID = b.CODEID AND (b.CMP_CODEID IS NOT NULL Or b.CMP_CODEID != '')
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as c ON b.RecordID = c.SubjectID
				WHERE
					c.ReportID = '$ParReportID'
					AND a.RecordID = '$ParSubjectID'
					AND c.ReportSubjectID IS NOT NULL
					And a.RecordStatus = 1
					And b.RecordStatus = 1
				";
		$row = $this->returnVector($sql);

		$CmpSubjectList = implode(",", $row);
		$ReturnArray = $this->GET_WEIGHTING_BY_SUBJECT_CLASS($CmpSubjectList, $ParClassID);

		return $ReturnArray;
	}

	/*
	* Get Marksheet Score from component subject
	*/
	function GET_MARKSHEET_SCORE_FROM_COMPONENT($ParSubjectID, $ParReportID, $ParClassID, $ParStudentIDList)
	{
		//calmethod
		$RoundDP = $this->GET_DECIMAL_POINT($ParReportID);

		$ParentWeightArray = $this->GET_WEIGHTING_BY_SUBJECT_CLASS($ParSubjectID, $ParClassID);
		list($ParentWeight, $ParentSchemeID) = $ParentWeightArray[$ParSubjectID];
		$ParentFullMark = $this->GET_SCHEME_FULLMARK($ParentSchemeID);

		$SubjectWeightArray = $this->GET_REPORT_CMP_SUBJECT_WEIGHTING($ParSubjectID, $ParReportID, $ParClassID);
		//asdf
		if(is_array($SubjectWeightArray))
		{
			$CmpSubjectIDArray = array_keys($SubjectWeightArray);
			$CmpSubjectList = implode(",", $CmpSubjectIDArray);
		}

		while (list ($CmpSubjectID, $CmpWeightArray) = each ($SubjectWeightArray)) {
			list($CmpWeight, $CmpSchemeID) = $CmpWeightArray;
			$CmpFullMark[$CmpSubjectID] = $this->GET_SCHEME_FULLMARK($CmpSchemeID);
		}

		$sql = "SELECT
					ReportColumnID,
					SubjectID,
					StudentID,
					Mark
				FROM
					RC_MARKSHEET_SCORE
				WHERE
					SubjectID IN ($CmpSubjectList)
					AND StudentID IN ($ParStudentIDList)
					AND IsOverall = 0
					AND IsWeighted = 0
					AND Mark IS NOT NULL
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
				ORDER BY
					StudentID
				";
		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($RCID, $SubjectID, $StudentID, $Mark) = $row[$i];
			$ResultArray[$StudentID][$RCID][$SubjectID] = $Mark;
		}

		if(is_array($ResultArray))
		{
			foreach($ResultArray as $StudentID => $ReportColumnArray)
			{
				foreach($ReportColumnArray as $RCID => $SubjectArray)
				{
					$TotalWeight = 0;
					$TotalMark = 0;
					foreach($SubjectArray as $SubjectID => $Mark)
					{
						$s_weight = $SubjectWeightArray[$SubjectID][0];

						if ($this->CalculationMethodRule == 1)
						{
							//if (($Mark!="-2")&&($Mark>=0)) $TotalWeight = $TotalWeight + $s_weight;
							if ($Mark!="-2") $TotalWeight = $TotalWeight + $s_weight;
						} else
						{
							//if (($Mark!="-2")&&($Mark>=0)) $TotalMark = $TotalMark + ($Mark*$s_weight);
							if ($Mark>=0) $TotalMark = $TotalMark + ($Mark*$s_weight);
						}
					}
					//if ($this->CalculationMethodRule == 0) $TotalMark = $TotalMark * 100;
					if ($this->CalculationMethodRule == 0) $TotalMark = $TotalMark * $ParentFullMark;

					foreach($SubjectArray as $SubjectID => $Mark)
					{
						$s_weight = $SubjectWeightArray[$SubjectID][0];

						if ($this->CalculationMethodRule == 1)
						{
							// use Parent full mark as base
							//if (($Mark!="-2")&&($Mark>=0)) {
							if ($Mark!="-2") {
								if ($Mark=="-1") $Mark = 0;
								$CmpRatio = $s_weight / $TotalWeight;
								$CmpSubjectRatio = $Mark / $CmpFullMark[$SubjectID];
								$CmpWeightedMark = $CmpRatio * $ParentFullMark * $CmpSubjectRatio;
								$TotalMark += $CmpWeightedMark;
							}
						} else
						{
							//if (($Mark!="-2")&&($Mark>=0))
							if ($Mark!="-2")
								$TotalWeight = $TotalWeight + $CmpFullMark[$SubjectID]*$s_weight;
						}
					}

					//$ReturnArray[$StudentID][$RCID] = ($TotalWeight>0) ? round($TotalMark/$TotalWeight, $RoundDP) : "-2";
					if ($this->CalculationMethodRule == 1)
					{
						$ReturnArray[$StudentID][$RCID] = ($TotalWeight>0) ? round($TotalMark, $RoundDP) : "-2";
					} else {
						$ReturnArray[$StudentID][$RCID] = ($TotalWeight>0) ? round($TotalMark/$TotalWeight, $RoundDP) : "-2";
					}
				}
			}
		}

		return $ReturnArray;
	}


	/*
	* Get overall result of from component subject (Method 1)
	*/
	function GET_OVERALL_RESULT_FROM_COMPONENT_1($ParMarksheetResult, $ParWeightArray)
	{
		//asdfasdf
		//calmethod - not finish
		global $ReportID;
		if ($ReportID != "") {
			$RoundDP = $this->GET_DECIMAL_POINT($ReportID);
		} else {
			$RoundDP = 2;
		}

		if(is_array($ParMarksheetResult))
		{
			foreach($ParMarksheetResult as $StudentID => $ReportColumnResultArray)
			{
				$TotalMark = 0;
				$TotalWeight = 0;
				foreach($ReportColumnResultArray as $RCID => $Result)
				{
					if($Result!="-2")
					{
						$Weight = $ParWeightArray[$RCID];
						$TotalMark = $TotalMark+ ($Result*$Weight/100);
						$TotalWeight = $TotalWeight + $Weight;
					}
				}
				$OverallResult[$StudentID] = ($TotalWeight>0) ? round(($TotalMark*100/$TotalWeight), $RoundDP) : 0;
			}
		}

		return $OverallResult;
	}

	/*
	* Get overall result of from component subject (Method 2)
	*/
	function GET_OVERALL_RESULT_FROM_COMPONENT_2($ParSubjectID, $ParReportID, $ParClassID, $ParStudentIDList)
	{
		//calmethod
		$RoundDP = $this->GET_DECIMAL_POINT($ParReportID);

		$ParentWeightArray = $this->GET_WEIGHTING_BY_SUBJECT_CLASS($ParSubjectID, $ParClassID);
		list($ParentWeight, $ParentSchemeID) = $ParentWeightArray[$ParSubjectID];
		$ParentFullMark = $this->GET_SCHEME_FULLMARK($ParentSchemeID);

		$SubjectWeightArray = $this->GET_REPORT_CMP_SUBJECT_WEIGHTING($ParSubjectID, $ParReportID, $ParClassID);
		if(is_array($SubjectWeightArray))
		{
			$CmpSubjectIDArray = array_keys($SubjectWeightArray);
			$CmpSubjectList = implode(",", $CmpSubjectIDArray);
		}

		while (list ($CmpSubjectID, $CmpWeightArray) = each ($SubjectWeightArray)) {
			list($CmpWeight, $CmpSchemeID) = $CmpWeightArray;
			$CmpFullMark[$CmpSubjectID] = $this->GET_SCHEME_FULLMARK($CmpSchemeID);
		}

		$sql = "SELECT
					SubjectID,
					StudentID,
					Mark
				FROM
					RC_MARKSHEET_SCORE
				WHERE
					SubjectID IN ($CmpSubjectList)
					AND StudentID IN ($ParStudentIDList)
					AND IsOverall = 1
					AND Mark IS NOT NULL
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
				ORDER BY
					StudentID
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $StudentID, $Mark) = $row[$i];
			$ResultArray[$StudentID][$SubjectID] = $Mark;
		}

		if(!empty($ResultArray))
		{
			foreach($ResultArray as $student_id => $SubjectMarkArray)
			{
				if(!empty($SubjectMarkArray))
				{
					$TotalMark = 0;
					$TotalWeight = 0;
					foreach($SubjectMarkArray as $subject_id => $Mark)
					{
						$Weight = $SubjectWeightArray[$subject_id][0];
						/* original
						if($Mark>=0)
							$TotalMark = $TotalMark + $Mark*$Weight;

						if($Mark!="-2")
							$TotalWeight = $TotalWeight + $Weight;
						*/

						if ($this->CalculationMethodRule == 1) {
							//if (($Mark!="-2")&&($Mark>=0))
							if ($Mark!="-2")
							{
								if ($Mark=="-2") $Mark = 0;
								$TotalMark = $TotalMark + $Mark*$Weight/$CmpFullMark[$subject_id];
								$TotalWeight = $TotalWeight + $Weight;
							}
						} else {
							//if (($Mark!="-2")&&($Mark>=0))
							if ($Mark!="-2")
							{
								if ($Mark=="-2") $Mark = 0;
								$TotalMark = $TotalMark + $Mark*$Weight;
								$TotalWeight = $TotalWeight + $CmpFullMark[$subject_id]*$Weight;
							}
						}
					}

					//$TotalMark = $TotalMark * 100;
					$TotalMark = $TotalMark * $ParentFullMark;

					$OverallMark = ($TotalWeight>0) ? round($TotalMark/$TotalWeight, $RoundDP) : "/";
				}
				$OverallResult[$student_id] = $OverallMark;
			}
		}

		return $OverallResult;
	}

	function GET_OVERALL_FROM_COMPONENT($ParSubjectID, $ParReportID, $ParClassID, $ParStudentID, $ParParentSubjectID)
	{
		if (($ParParentSubjectID != "")&&($ParParentSubjectID != $ParSubjectID)) {
			$SubjectWeightArray = $this->GET_REPORT_CMP_SUBJECT_WEIGHTING($ParSubjectID, $ParReportID, $ParClassID);
			if(is_array($SubjectWeightArray))
			{
				$CmpSubjectIDArray = array_keys($SubjectWeightArray);
				$CmpSubjectList = implode(",", $CmpSubjectIDArray);
			} else {
				$CmpSubjectList = $ParSubjectID;
			}
		} else {
			$CmpSubjectList = $ParSubjectID;
		}

		$sql = "SELECT
						Mark
				FROM
						RC_MARKSHEET_SCORE
				WHERE
						SubjectID IN ($CmpSubjectList)
						AND IsWeighted = 1
						AND Mark IS NOT NULL
						AND Year = '".$this->Year."'
						AND Semester = '".$this->Semester."'
						AND StudentID = '$ParStudentID'
				ORDER BY
						StudentID
				";
				//AND IsOverall = 1
		$row = $this->returnVector($sql, 1);
		$OverallMark = array_sum($row);

		return $OverallMark;
	}


	function PARENT_GET_OVERALL_FROM_COMPONENT($ParSubjectID, $ParReportID, $ParClassID, $ParStudentID)
	{
		$SubjectWeightArray = $this->GET_REPORT_CMP_SUBJECT_WEIGHTING($ParSubjectID, $ParReportID, $ParClassID);
		if(is_array($SubjectWeightArray))
		{
			$CmpSubjectIDArray = array_keys($SubjectWeightArray);
			$CmpSubjectList = implode(",", $CmpSubjectIDArray);
		} else {
			$CmpSubjectList = $ParSubjectID;
		}

		$sql = "SELECT
						Mark
				FROM
						RC_MARKSHEET_SCORE
				WHERE
						SubjectID IN ($CmpSubjectList)
						AND IsOverall = 1
						AND Mark IS NOT NULL
						AND Year = '".$this->Year."'
						AND Semester = '".$this->Semester."'
						AND StudentID = '$ParStudentID'
				ORDER BY
						StudentID
				";
				//AND IsOverall = 1
		$row = $this->returnVector($sql, 1);

		$OverallMark = array_sum($row);

		return $OverallMark;
	}



	/*
	* Get Previous Marksheet Score
	*/
	function GET_PREVIOUS_MARKSHEET_SCORE($ReturnArray=null, $ParReportID, $ParSubjectID, $ParStudentIDList, $ParExclusiveRCIDList="")
	{
		/*
		$cond = ($this->Semester!="FULL") ? "AND a.Semester <> '".$this->Semester."'" : "";
		$cond .= ($ParExclusiveRCIDList!="") ? " AND d.ReportColumnID NOT IN ($ParExclusiveRCIDList)" : "";

		$sql = "SELECT
					d.ReportColumnID,
					a.StudentID,
					a.Mark,
					a.Grade
				FROM
					RC_MARKSHEET_SCORE as a
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as b ON a.SubjectID = b.SubjectID
					LEFT JOIN RC_REPORT_TEMPLATE as c ON b.ReportID = c.ReportID
					LEFT JOIN RC_REPORT_TEMPLATE_COLUMN as d ON c.ReportType = d.ColumnTitle
				WHERE
					a.IsOverall=1
					AND a.SubjectID = '$ParSubjectID'
					AND a.StudentID IN ($ParStudentIDList)
					AND c.ReportID <> '".$ParReportID."'
					AND d.ReportID = '$ParReportID'
					AND a.Year = '".$this->Year."'
					$cond
				";
		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($RCID, $StudentID, $Mark, $Grade) = $row[$i];
			$ReturnArray[$StudentID][$RCID] = ($Mark=="") ? $Grade : $Mark;
		}
		*/
		# Get semesters
		$SemesterArray = $this->GET_SEMESTER_ARRAY();
		if(!empty($SemesterArray))
		{
			$cond = ($ParExclusiveRCIDList!="") ? " AND ReportColumnID NOT IN ($ParExclusiveRCIDList)" : "";
			$SemesterList = "'".implode("','", $SemesterArray)."'";
			$sql = "SELECT DISTINCT
						ReportColumnID,
						ColumnTitle
					FROM
						RC_REPORT_TEMPLATE_COLUMN
					WHERE
						ReportID = '".$ParReportID."'
						AND ColumnTitle IN ({$SemesterList})
						$cond
					";
			$row_semester = $this->returnArray($sql, 2);
		}
		for($j=0; $j<sizeof($row_semester); $j++)
		{
			list($rcid, $semester) = $row_semester[$j];
			$SemesterReportColumnArray[trim($semester)] = trim($rcid);
		}

		$cond = ($this->Semester!="FULL") ? "AND a.Semester <> '".$this->SemesterName."'" : "";
		$sql = "SELECT DISTINCT
					a.Semester,
					a.StudentID,
					a.Mark,
					a.Grade
				FROM
					RC_MARKSHEET_SCORE as a
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as b ON a.SubjectID = b.SubjectID
				WHERE
					a.IsOverall=1
					AND a.SubjectID = '$ParSubjectID'
					AND a.StudentID IN ($ParStudentIDList)
					AND a.Year = '".$this->Year."'
					$cond
				ORDER BY
					a.StudentID,
					a.Semester
				";
		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($semester, $student_id, $mark, $grade) = $row[$i];
			$ReportColumnID = $SemesterReportColumnArray[trim($semester)];
			if($ReportColumnID!="")
				$ReturnArray[$student_id][$ReportColumnID] = ($mark=="") ? $grade : $mark;
		}

		return $ReturnArray;
	}

	/*
	* Get Marksheet Score
	*/
	function GET_MARKSHEET_SCORE($ParReportID, $ParSubjectID, $ParStudentIDList, $ParSetting, $ParMarkType=0, $ParAllowRetrievePrevious=0, $ParGetAllPrevious=0)
	{
		if($ParMarkType==2)
		{
			# retrieve converted grades
			$sql = "SELECT
						ReportColumnID,
						StudentID,
						IsOverall,
						Mark,
						Grade
					FROM
						RC_MARKSHEET_SCORE
					WHERE
						IsWeighted = 0
						AND SubjectID = '$ParSubjectID'
						AND StudentID IN ($ParStudentIDList)
						AND Year = '".$this->Year."'
						AND Semester = '".$this->Semester."'
					";
			$row = $this->returnArray($sql, 5);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($RCID, $StudentID, $IsOverall, $Mark, $Grade) = $row[$i];
				if($IsOverall==1) {
					$ReturnArray[$StudentID]["Overall"] = $Grade;
				}
				else {
					$ReturnArray[$StudentID][$RCID] = $Grade;
				}
			}
		}
		else
		{
			$IsWeighted = $ParMarkType;
			$sql = "SELECT
						ReportColumnID,
						StudentID,
						IsOverall,
						Mark,
						Grade
					FROM
						RC_MARKSHEET_SCORE
					WHERE
						(IsOverall=1 OR (IsOverall=0 AND IsWeighted = '$IsWeighted'))
						AND SubjectID = '$ParSubjectID'
						AND StudentID IN ($ParStudentIDList)
						AND Year = '".$this->Year."'
						AND Semester = '".$this->Semester."'
					";
			$row = $this->returnArray($sql, 5);

			$RCIDArray = array();
			for($i=0; $i<sizeof($row); $i++)
			{
				list($RCID, $StudentID, $IsOverall, $Mark, $Grade) = $row[$i];
				if($IsOverall==1)
				{
					if($ParSetting=="S") {
						$ReturnArray[$StudentID]["Overall"] = $Mark;
						$ReturnArray[$StudentID]["OverallGrade"] = $Grade;
					}
					else {
						$ReturnArray[$StudentID]["Overall"] = $Grade;
					}
				}
				else
				{
					$ReturnArray[$StudentID][$RCID] = ($ParSetting=="S") ? $Mark : $Grade;
				}

				if($RCID!="" && !in_array($RCID, $RCIDArray))
					$RCIDArray[] = $RCID;
			}

			if($ParAllowRetrievePrevious==1)
			{
				$ExclusiveRCIDList = (is_array($RCIDArray) && $ParGetAllPrevious==0) ? implode(",", $RCIDArray) : "";
				$ReturnArray = $this->GET_PREVIOUS_MARKSHEET_SCORE($ReturnArray, $ParReportID, $ParSubjectID, $ParStudentIDList, $ExclusiveRCIDList);
			}
		}

		return $ReturnArray;
	}

	/*
	* Get Marksheet Result Selection
	*/
	function GET_MARKSHEET_RESULT_SELECTION($ParSelectonArray, $ParName, $ParSelected="", $IsOverall=0)
	{
		global $button_select;

		$ReturnObj = "<SELECT name='".$ParName."'>";
		$ReturnObj .= "<OPTION value=''>-- ".$button_select." --</OPTION>";
		for($i=0; $i<sizeof($ParSelectonArray); $i++)
		{
			$item = $ParSelectonArray[$i];
			$SelectStr = ($ParSelected==$item) ? "SELECTED='SELECTED'" : "";
			$ReturnObj .= "<OPTION value='".$item."' {$SelectStr}>".$item."</OPTION>";
		}
		$ReturnObj .= ($IsOverall!=1) ? "<OPTION value='abs' ".($ParSelected=="abs"?"SELECTED='SELECTED'":"").">abs</OPTION>" : "";
		$ReturnObj .= "<OPTION value='/' ".($ParSelected=="/"?"SELECTED='SELECTED'":"").">/</OPTION>";
		$ReturnObj .= "</SELECT>";

		return $ReturnObj;
	}

	/*
	* Get Scheme Full Mark
	*/
	function GET_SCHEME_FULLMARK($ParSchemeID)
	{
		$sql = "SELECT FullMark FROM RC_GRADING_SCHEME WHERE SchemeID = '$ParSchemeID'";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Generate Student Feedback and Confirmed status
	*/
	function GET_ALL_STUDENT_FEEDBACK_STATUS($ParSubjectID, $ParClassID)
	{
		$sql = "SELECT
					a.StudentID,
					a.Comment,
					a.IsComment
				FROM
					RC_MARKSHEET_FEEDBACK as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
				WHERE
					a.SubjectID = '$ParSubjectID'
					AND a.Year = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND c.ClassID = '$ParClassID'
					AND b.RecordType = 2
					AND b.RecordStatus = 1
				ORDER BY
					a.StudentID
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($student_id, $comment, $is_comment) = $row[$i];
			if($is_comment==0)
			{
				$ConfirmedArray[] = $student_id;
			}
			else
			{
				$FeedbackArray[$student_id] = $comment;
			}
		}

		return array($FeedbackArray, $ConfirmedArray);
	}

	/*
	* Generate Student Feedback and Confirmed status
	*/
	function GET_ALL_STUDENT_FEEDBACK($ParSubjectID, $ParClassID)
	{
		$NameField = getNameFieldByLang2("b.");
		$ClassNumField = getClassNumberField("b.");

		$sql = "SELECT
					b.UserID,
					CONCAT(b.ClassName, ' - ' , $ClassNumField),
					$NameField,
					a.Comment
				FROM
					RC_MARKSHEET_FEEDBACK as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
				WHERE
					a.SubjectID = '$ParSubjectID'
					AND a.Year = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND c.ClassID = '$ParClassID'
					AND a.IsComment = 1
					AND b.RecordType = 2
					AND b.RecordStatus = 1
				ORDER BY
					a.StudentID
				";
		$row = $this->returnArray($sql, 4);

		return $row;
	}

	/*
	* Generate User Feedback status
	*/
	function GET_STUDENT_FEEDBACK_STATUS($ParSubjectID)
	{
		global $UserID;

		# check confirmed
		$sql = "SELECT
					FeedbackID
				FROM
					RC_MARKSHEET_FEEDBACK
				WHERE
					SubjectID = '$ParSubjectID'
					AND StudentID = '".$this->uid."'
					AND IsComment = 0
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
				";
		$row = $this->returnVector($sql);
		$IsConfirmed = ($row[0]>0) ? 1 : 0;

		# check feedback exist
		$sql = "SELECT
					Comment
				FROM
					RC_MARKSHEET_FEEDBACK
				WHERE
					SubjectID = '$ParSubjectID'
					AND StudentID = '".$this->uid."'
					AND IsComment = 1
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
			";
		$row = $this->returnVector($sql);
		$Feedback = $row[0];

		return array($IsConfirmed, $Feedback);
	}

		/*
	* Get Sub-Marksheet Column
	*/
	function GET_ALL_SUB_MARKSHEET_COLUMN($ParReportColumnID, $ParSubjectID)
	{
		$sql = "SELECT
					ColumnID,
					ColumnTitle,
					Weight
				FROM
					RC_SUB_MARKSHEET_COLUMN
				WHERE
					ReportColumnID = '$ParReportColumnID'
					AND SubjectID = '$ParSubjectID'
				ORDER BY
					ColumnID
				";
		$row = $this->returnArray($sql, 3);

		return $row;
	}

	/*
	* Get Sub-Marksheet Column
	*/
	function GET_SUB_MARKSHEET_COLUMN($ParColumnID)
	{
		$sql = "SELECT
					ColumnTitle,
					Weight
				FROM
					RC_SUB_MARKSHEET_COLUMN
				WHERE
					ColumnID = '$ParColumnID'
			";
		$row = $this->returnArray($sql, 2);

		return $row[0];
	}

	/*
	* Get Sub-Marksheet Column
	*/
	function GET_STUDENT_SUB_MARKSHEET_COLUMN_SCORE($ParReportColumnID, $ParSubjectID, $ParStudentID)
	{
		$sql = "SELECT
					a.ColumnID,
					a.ColumnTitle,
					a.Weight,
					b.Mark
				FROM
					RC_SUB_MARKSHEET_COLUMN as a
					LEFT JOIN RC_SUB_MARKSHEET_SCORE as b ON a.ColumnID = b.ColumnID AND b.StudentID = '$ParStudentID'
				WHERE
					a.ReportColumnID = '$ParReportColumnID'
					AND a.SubjectID = '$ParSubjectID'
				ORDER BY
					a.ColumnID
				";
		$row = $this->returnArray($sql, 4);

		return $row;
	}

	/*
	* Get Sub-Marksheet Score
	*/
	function GET_SUB_MARKSHEET_SCORE($ParReportColumnID, $ParSubjectID)
	{
		$sql = "SELECT
					a.ColumnID,
					a.StudentID,
					a.Mark,
					a.IsOverall,
					a.IsOverallWeighted
				FROM
					RC_SUB_MARKSHEET_SCORE as a
				WHERE
					a.ReportColumnID = '$ParReportColumnID'
					AND a.SubjectID = '$ParSubjectID'
			";
		$row = $this->returnArray($sql, 5);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($column_id, $student_id, $mark, $is_overall, $is_overall_weighted) = $row[$i];

			if($is_overall==1)
				$ReturnArray[$student_id]["Overall"] = $mark;
			else if($is_overall_weighted==1)
				$ReturnArray[$student_id]["WeightedOverall"] = $mark;
			else
				$ReturnArray[$student_id][$column_id] = $mark;
		}

		return $ReturnArray;
	}

	/*
	* Check whether sub-marksheet exist
	*/
	function CHECK_SUB_MARKSHEET_EXIST($ParSubjectID, $ParClassID)
	{
		$ReportID = $this->GET_REPORTID_BY_CLASS($ParClassID);
		$SubjectArray = $this->GET_RELATED_SUBJECT($ParSubjectID);
		$SubjectList = (is_array($SubjectArray)) ? implode(",", $SubjectArray) : $ParSubjectID;

		$sql = "SELECT
					COUNT(a.ColumnID)
				FROM
					RC_SUB_MARKSHEET_COLUMN as a
					LEFT JOIN RC_REPORT_TEMPLATE_COLUMN as b ON a.ReportColumnID = b.ReportColumnID
				WHERE
					a.SubjectID IN ($SubjectList)
					AND b.ReportID = '$ReportID'
				";
		$row = $this->returnVector($sql);

		return ($row[0]>0) ? 1 : 0;
	}

	/*
	* get converted grade from scores
	*/
	function GET_CONVERTED_GRADE($ParGradeMarkArray, $ParLowestGrade, $ParMark)
	{
		if($ParMark<0) {
			$ReturnValue = $this->SpecialMarkArray[trim($ParMark)];
		}
		else {
			$ReturnValue = "";
			for($g=0; $g<sizeof($ParGradeMarkArray); $g++)
			{
				list($LowerLimit, $Grade) = $ParGradeMarkArray[$g];
				if($ParMark>=$LowerLimit)
				{
					$ReturnValue = $Grade;
					break;
				}
			}
			if($ReturnValue=="")
				$ReturnValue = $ParLowestGrade;
		}

		return $ReturnValue;
	}

################################## Marksheet Submission Functions End #####################################
#############################################################################################################

######################################## Verify Result Functions Start ##########################################

	/*
	* Get Student ReportID
	*/
	function GET_STUDENT_CLASS_REPORT_ID()
	{
		$sql = "SELECT
					b.ClassID,
					c.ReportID
				FROM
					INTRANET_USER as a
					LEFT JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as c ON b.ClassLevelID = c.ClassLevelID
					LEFT JOIN RC_REPORT_TEMPLATE as d ON c.ReportID = d.ReportID
				WHERE
					a.UserID = '".$this->uid."'
					AND d.RecordStatus = 1
				";
		$row = $this->returnArray($sql, 2);

		return $row;
	}
	function GET_STUDENT_CLASS_REPORT_ID_BY_USERID($ParUserID)
	{
		$sql = "SELECT
					b.ClassID,
					c.ReportID
				FROM
					INTRANET_USER as a
					LEFT JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as c ON b.ClassLevelID = c.ClassLevelID
					LEFT JOIN RC_REPORT_TEMPLATE as d ON c.ReportID = d.ReportID
				WHERE
					a.UserID IN ($ParUserID)

					AND d.RecordStatus = 1
				";

		$row = $this->returnArray($sql, 2);

		return $row;
	}

	/*
	* Get Student Subject
	*/
	function GET_STUDENT_REPORT_SUBJECT($ParClassID, $ParReportID)
	{
		global $eclass_db;

		$sql = "SELECT DISTINCT
					b.RecordID,
					CONCAT(b.EN_DES, '&nbsp;', b.CH_DES)
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
				WHERE
					a.ReportID = '$ParReportID'
					AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
					And b.RecordStatus = 1
				ORDER BY
					a.DisplayOrder
				";
		$row = $this->returnArray($sql, 4);

		return $row;
	}

	/*
	* Get overall score
	*/
	function GET_OVERALL_RESULT($ParSubjectID)
	{
		//asdfasdf
		if ($ReportID != "") {
			$RoundDP = $this->GET_DECIMAL_POINT($ReportID);
		} else {
			$RoundDP = 2;
		}

		$sql = "SELECT DISTINCT
					Mark,
					Grade
				FROM
					RC_MARKSHEET_SCORE
				WHERE
					SubjectID = '$ParSubjectID'
					AND StudentID = '".$this->uid."'
					AND IsOverall = 1
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
				";
		$row = $this->returnArray($sql, 2);
		if(!empty($row))
		{
			$ReturnResult["Mark"] = round($row[0][0], $RoundDP);
			$ReturnResult["Grade"] = $row[0][1];
		}

		return $ReturnResult;
	}

	/*
	* Get overall score by user id
	function GET_OVERALL_RESULT_BY_ID($ParSubjectID, $ParUserID)
	{
		$sql = "SELECT DISTINCT
					Mark,
					Grade
				FROM
					RC_MARKSHEET_SCORE
				WHERE
					SubjectID = '$ParSubjectID'
					AND StudentID = '".$ParUserID."'
					AND IsOverall = 1
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
				";

		$row = $this->returnArray($sql, 2);
		list($Mark, $Grade) = $row[0];

		return ($Mark=="") ? $Grade : $Mark;
	}
	*/

	/*
	* Get columns and scores of a subject
	*/
	function GET_SUBJECT_COLUMNS_RESULTS($ParReportID, $ParSubjectID)
	{
		$RoundDP = $this->GET_DECIMAL_POINT($ParReportID);
		$sql = "SELECT
					a.ReportColumnID,
					a.ColumnTitle,
					a.Weight,
					b.Mark,
					b.Grade,
					b.IsWeighted
				FROM
					RC_REPORT_TEMPLATE_COLUMN as a
					LEFT JOIN RC_MARKSHEET_SCORE as b ON a.ReportColumnID = b.ReportColumnID
				WHERE
					a.ReportID = '$ParReportID'
					AND b.SubjectID = '$ParSubjectID'
					AND b.StudentID = '".$this->uid."'
					AND b.IsOverall = 0
					AND b.Year = '".$this->Year."'
					AND b.Semester = '".$this->Semester."'
				ORDER BY
					a.DisplayOrder
				";
		$row = $this->returnArray($sql, 6);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($ReportColumnID, $ColumnTitle, $Weight, $Mark, $Grade, $IsWeighted) = $row[$i];
			$ReturnArray[$ReportColumnID]["ColumnTitle"] = $ColumnTitle;
			$ReturnArray[$ReportColumnID]["Weight"] = $Weight;
			if($IsWeighted==1)
				$ReturnArray[$ReportColumnID]["WeightedMark"] = round($Mark, $RoundDP);
			else
				$ReturnArray[$ReportColumnID]["RawMark"] = round($Mark, $RoundDP);
			$ReturnArray[$ReportColumnID]["Grade"] = $Grade;
		}

		return $ReturnArray;
	}

	/*
	* Get columns and scores of a subject by userid
	function GET_SUBJECT_COLUMNS_SCORES_BY_USERID($ParReportID, $ParSubjectID, $ParUserID)
	{
		$sql = "SELECT
					a.ReportColumnID,
					a.ColumnTitle,
					a.Weight,
					if(b.Mark IS NULL, 'NULL', b.Mark),
					b.Grade
				FROM
					RC_REPORT_TEMPLATE_COLUMN as a
					LEFT JOIN RC_MARKSHEET_SCORE as b ON a.ReportColumnID = b.ReportColumnID
				WHERE
					a.ReportID = '$ParReportID'
					AND b.SubjectID = '$ParSubjectID'
					AND b.StudentID = '".$ParUserID."'
					AND ((b.Mark IS NOT NULL AND b.IsWeighted = 1) || b.Mark IS NULL)
					AND b.IsOverall = 0
					AND b.Year = '".$this->Year."'
					AND b.Semester = '".$this->Semester."'
				ORDER BY
					a.DisplayOrder
				";

		$row = $this->returnArray($sql, 5);

		return $row;

	}
	*/

	/*
	* Get Scheme full mark by SubjectID and ClassID
	*/
	function GET_FULLMARK_BY_SUBJECT_CLASS($ParSubjectID, $ParClassID)
	{
		# Get Subject Scheme full mark
		$sql = "SELECT DISTINCT
					a.SchemeID,
					d.FullMark
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID
					LEFT JOIN INTRANET_CLASS as c ON b.ClassLevelID = c.ClassLevelID AND c.ClassID = '$ParClassID'
					LEFT JOIN RC_GRADING_SCHEME as d ON a.SchemeID = d.SchemeID
				WHERE
					a.SubjectID = '$ParSubjectID'
				";
		$row = $this->returnArray($sql, 2);

		return (sizeof($row)>1) ? $row[1][1] : $row[0][1];
	}

	/*
	* Generate Result Details table
	*/
	function GET_RESULT_DETAILS_TABLE($ParClassID, $ParSubjectID, $ParReportID, $ParTableID, $ParCloseBtn, $ParMarkTypeDisplay=0, $ParOverallResult="", $ParSubjectScale="")
	{
		global $eReportCard, $image_path, $LAYOUT_SKIN;

		# Get Component Subject(s)
		$SubjectList = $this->GET_RELATED_SUBJECT($ParSubjectID);

		$ReturnContent .= "<table width='65%' id=\"$ParTableID\" align=\"right\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" ";
		if (isset($ParCloseBtn))
			$ReturnContent .="style=\"display:none\"><tr><td>";
		else
			$ReturnContent .="><tr><td>";
		for($k=0; $k<sizeof($SubjectList); $k++)
		{
			$SubjectID = $SubjectList[$k];
			# Check any result allowed to show for this subject
			$CellExist = $this->CHECK_REPORT_CELL_NULL($ParReportID, $SubjectID);

			if($CellExist>0)
			{
				# Get Subject Name
				$SubjectName = $this->GET_SUBJECT_NAME($SubjectID);

				# Get Subject Scheme full mark
				$FullMark = $this->GET_FULLMARK_BY_SUBJECT_CLASS($SubjectID, $ParClassID);

				# Get columns and scores
				$ColumnArray = $this->GET_SUBJECT_COLUMNS_RESULTS($ParReportID, $SubjectID, $ParMarkTypeDisplay);
				if(!empty($ColumnArray))
				{
					$FullMarkDisplay = (!empty($FullMark)) ? " (".$eReportCard['FullMark']." ".$FullMark.")" : "";
					$ReturnContent .= "<table width='100%' border=0 cellpadding=5 cellspacing=0><tr><td>";
					$ReturnContent .= "<tr>
										<td class='tabletoplink tablebluetop' colspan='2'>".$SubjectName.$FullMarkDisplay."</td>
										</tr>";
					$count = 0;
					foreach($ColumnArray as $ReportColumnID => $ColumnResultArray)
					{
						$ColumnTitle = $ColumnResultArray["ColumnTitle"];
						$Weight = $ColumnResultArray["Weight"];
						$RawMark = $ColumnResultArray["RawMark"];
						$WeightedMark = $ColumnResultArray["WeightedMark"];
						$Grade = $ColumnResultArray["Grade"];

						$css = ($count++%2==0) ? 1 : 2;
						$ColumnTitle = (!empty($Weight)) ? $ColumnTitle." ".$Weight."%" : $ColumnTitle;
						if($ParSubjectScale=="S") {
							if($RawMark>=0) {
								if($ParMarkTypeDisplay==1)
									$DisplayResult = $RawMark;
								else if($ParMarkTypeDisplay==2)
									$DisplayResult = $Grade;
								else
									$DisplayResult = $WeightedMark;
							}
							else
								$DisplayResult = $this->SpecialMarkArray[trim($RawMark)];
						}
						else {
							$DisplayResult = $Grade;
						}
						$ReturnContent .= "<tr>
										<td class='tablebluerow$css tabletext' valign='top'>".$ColumnTitle.": </td>
										<td class='tablebluerow$css tabletext' align='right' valign='top'>".($DisplayResult==""?"--":$DisplayResult)."</td>
										</tr>";
					}
					$ReturnContent .= "<tr><td align='right' colspan='2' class=\"dotline\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
					$ReturnContent .= "<tr>
										<td class='tabletext'>".$eReportCard['OverallResult'].": </td>
										<td class='tabletext' align='right'>".($ParOverallResult==""?"--":$ParOverallResult)."</td>
										</tr>";
					$ReturnContent .= "</table>";
					$ReturnContent .= (($k+1)==sizeof($SubjectList)) ? "" : "<br />";
				}
			}
		}
		$ReturnContent .= "</td></tr>";
		$ReturnContent .= "<tr><td align='left' class='tabletextremark' colspan='2'>".$eReportCard['MarkRemind']."</td></tr>";
		$ReturnContent .= "<tr>
							<td align=\"right\" colspan='2'>".$ParCloseBtn."</td>
							</tr>
							";
		$ReturnContent .= "</table>";

		return $ReturnContent;
	}

	/*
	* Get Feedback Count
	*/
	function GET_FEEDBACK_COUNT($ParSubjectID)
	{
		$sql = "SELECT
					c.ClassID,
					COUNT(a.FeedbackID)
				FROM
					RC_MARKSHEET_FEEDBACK as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
				WHERE
					a.SubjectID = '$ParSubjectID'
					AND a.Year = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND a.IsComment = 1
					AND b.RecordType = 2
					AND b.RecordStatus = 1
				GROUP BY
					c.ClassID
				ORDER BY
					c.ClassName
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($ClassID, $FeedbackCount) = $row[$i];
			$ReturnArray[$ClassID] = $FeedbackCount;
		}

		return $ReturnArray;
	}


	/*
	* Generate Feedback table
	*/
	function GENERATE_FEEDBACK_TABLE($ParSubjectID, $ParTableID, $ParSubmitBtn, $ParCancelBtn)
	{
		$ReturnContent = "<form name='form1' action='feedback_update.php' method='POST' onSubmit='return jSUBMIT_FEEDBACK(this)'>";
		$ReturnContent .= "<table width='87%' id=\"$ParTableID\" align=\"center\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" style=\"display:none\">";
		$ReturnContent .= "<tr><td>
							<textarea class=\"textboxtext\" name=\"Feedback\" rows=\"2\" wrap=\"virtual\" onFocus=\"this.rows=5\"></textarea></td></tr>";
		$ReturnContent .= "<tr><td>".$ParSubmitBtn."&nbsp;".$ParCancelBtn."</td></tr>";
		$ReturnContent .= "</table>";
		$ReturnContent .= "<input type='hidden' name='SubjectID' value='".$ParSubjectID."' />";
		$ReturnContent .= "</form>";

		return $ReturnContent;
	}

	/*
	* Generate User Feedback
	*/
	function GET_STUDENT_FEEDBACK($ParNotificationID, $StudentID="")
	{
		$TargetUser = ($StudentID=="") ? $this->uid : $StudentID;
		$sql = "SELECT
					date_format(DateInput, '%Y-%m-%d %H:%i'),
					Comment,
					IsTeacherRead
				FROM
					RC_MARKSHEET_FEEDBACK
				WHERE
					NotificationID = '$ParNotificationID'
					AND StudentID = '$TargetUser'
					AND IsComment = 1
					AND Year = '".$this->Year."'
					AND Semester = '".$this->Semester."'
				";
		$row = $this->returnArray($sql, 3);

		return $row;
	}

######################################## Verify Result Functions End ##########################################
#############################################################################################################

######################################## Report Generation & Printing Functions Start #########################

	/*
	* Check Report Result generated
	*/
	function GET_REPORT_RESULT_COUNT($ParReportID)
	{
		$sql = "SELECT COUNT(ResultID) FROM RC_REPORT_RESULT WHERE ReportID = '$ParReportID' AND Year = '".$this->Year."' AND Semester = '".$this->Semester."'";
		$row = $this->returnVector($sql);

		return $row[0];
	}

	/*
	* Get All Scheme GradeMark
	*/
	function GET_ALL_GRADEMARK()
	{
		# Get Scheme GradeMark Info
		$sql = "SELECT
					a.SchemeID,
					a.Pass,
					a.Fail,
					g.LowerLimit,
					g.Grade,
					g.GradePoint
				FROM
					RC_GRADING_SCHEME as a
					LEFT JOIN RC_GRADING_SCHEME_GRADEMARK as g ON a.SchemeID = g.SchemeID
				ORDER BY
					g.SchemeID,
					g.LowerLimit DESC
				";
		$row = $this->returnArray($sql, 6);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($sid, $Pass, $Fail, $lower_limit, $grade, $grade_point) = $row[$i];
			if($Pass!="" && $Fail!="")
			{
				$PassFailArray[$sid] = array($Pass, $Fail);
			}
			else
			{
				$GradePointArray[$sid][] = $grade_point;
				$AssocArray[$sid][] = array($lower_limit, $grade, $grade_point);
			}
		}

		if(is_array($GradePointArray))
		{
			foreach($GradePointArray as $scheme_id => $GPArr)
			{
				if(array_sum($GPArr)>0)
				{
					for($i=0; $i<sizeof($AssocArray[$scheme_id]); $i++)
					{
						list($lower_limit, $grade, $grade_point) = $AssocArray[$scheme_id][$i];
						$MarkArray[$scheme_id][$lower_limit] = $grade_point;
						$GradeArray[$scheme_id][$grade] = $grade_point;

					}
				}
			}
		}

		return array($MarkArray, $GradeArray, $PassFailArray);
	}

	/*
	* Get Subject weight by subject and class
	*/
	function GET_WEIGHTING_BY_SUBJECT_CLASS($ParSubjectList, $ParClassID)
	{
		$sql = "SELECT DISTINCT
					a.SubjectID,
					a.SchemeID,
					a.SubjectGradingID,
					c.YearClassID as ClassID,
					d.Weighting,
					c.YearID as ClassLevelID
				FROM
					RC_SUBJECT_GRADING_SCHEME as a
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME_FORM as b ON a.SubjectGradingID = b.SubjectGradingID
					LEFT JOIN YEAR_CLASS as c ON b.ClassLevelID = c.YearID
					LEFT JOIN RC_GRADING_SCHEME as d ON a.SchemeID = d.SchemeID
				WHERE
					a.SubjectID IN ($ParSubjectList)
					AND (c.YearClassID IS NULL OR c.YearClassID = '$ParClassID')
				ORDER BY
					a.SubjectID, a.SubjectGradingID
				";
		$row = $this->returnArray($sql, 5);

		for($j=0; $j<sizeof($row); $j++)
		{
			list($subject_id, $scheme_id, $sg_id1, $class_id, $weighting, $classlvl_id) = $row[$j];
			$sql = "SELECT count(YearID) FROM YEAR_CLASS WHERE YearID = '$classlvl_id'";
			$valid_classlvl_id = $this->returnVector($sql);
			if (($ReturnArray[$subject_id]=="" || $class_id==$ParClassID)&&($valid_classlvl_id))
				$ReturnArray[$subject_id] = array($weighting, $scheme_id);
		}

		return $ReturnArray;
	}

	/*
	* Get Student List by ClassLevelID
	*/
	function GET_STUDENT_BY_CLASSLEVEL($ParReportID, $ParClassLevelID, $ParClassID="", $ParStudentID="", $ParVerify=0)
	{
		$RoundDP = $this->GET_DECIMAL_POINT($ParReportID);
		if($ParStudentID!="") {
			$cond = " AND a.UserID IN ($ParStudentID)";
		}
		else if($ParClassID!="") {
			$cond = " AND b.YearClassID = '$ParClassID'";
		}
		$ClassNumField = getClassNumberField("a.");
		$sql = "SELECT
						b.YearClassID as ClassID,
						a.UserID,
						b.ClassTitleEN as ClassName,
						ycu.ClassNumber,
						a.ChineseName,
						a.EnglishName,
						REPLACE(a.WebSAMSRegNo, '#', ''),
						a.DateOfBirth,
						a.Gender,
						c.YearName as LevelName
					FROM
						YEAR_CLASS_USER as ycu
						LEFT JOIN INTRANET_USER as a On ycu.UserID = a.UserID
						LEFT JOIN YEAR_CLASS as b ON ycu.YearClassID = b.YearClassID
						LEFT JOIN YEAR as c ON b.YearID = c.YearID
					WHERE
						b.YearID IN ($ParClassLevelID)
						AND a.RecordType = 2
						AND a.RecordStatus = 1
						And b.AcademicYearID = '".$this->Year."'
						$cond
					ORDER BY
						b.Sequence,
						ycu.ClassNumber
					";
		$row_student = $this->returnArray($sql,8);
		
		if($ParVerify!=1)
		{
			$sql = "SELECT
						b.YearClassID as ClassID,
						a.UserID,
						c.Semester,
						c.GrandTotal,
						c.AverageMark,
						c.GPA,
						c.OrderMeritClass,
						c.OrderMeritClassTotal,
						c.OrderMeritForm,
						c.OrderMeritFormTotal,
						a.DateOfBirth,
						a.Gender
					FROM
						YEAR_CLASS_USER as ycu
						LEFT JOIN INTRANET_USER as a On (ycu.UserID = a.UserID)
						LEFT JOIN YEAR_CLASS as b ON ycu.YearClassID = b.YearClassID
						LEFT JOIN RC_REPORT_RESULT as c ON a.UserID = c.StudentID
					WHERE
						b.YearID IN ($ParClassLevelID)
						AND a.RecordType = 2
						AND a.RecordStatus = 1
						AND c.Year = '".$this->Year."'
						AND b.AcademicYearID = '".$this->Year."'
						AND c.ReportID = '$ParReportID'
						$cond
					ORDER BY
						b.Sequence,
						ycu.ClassNumber
					";
			$row_result = $this->returnArray($sql,10);
		}
		
		for($i=0; $i<sizeof($row_student); $i++)
		{
			if(is_array($row_student[$i]))
			{
				list($class_id, $user_id, $class_name, $class_number, $chi_name, $eng_name, $reg_no, $dob, $gender, $class_level_id) = $row_student[$i];
				$StudentName = ($chi_name=="") ? $eng_name : $eng_name." ".$chi_name;
				if ($_SESSION['intranet_session_language'] != "en")
				{
					$StudentName = ($eng_name=="") ? $chi_name : $chi_name." ".$eng_name;
				}
				$ClassStudentArray[$class_id][$user_id]["UserInfo"] = array($class_name, $class_number, $StudentName, $reg_no, $chi_name, $eng_name, $dob, $gender, $class_level_id);
				$StudentIDArray[] = $user_id;
			}
		}

		for($i=0; $i<sizeof($row_result); $i++)
		{
			if(is_array($row_result[$i]))
			{
				list($class_id, $user_id, $semester, $GT, $AM, $GPA, $OMC, $OMCT, $OMF, $OMFT) = $row_result[$i];
				$ClassStudentArray[$class_id][$user_id]["Result"][$semester] = array(round($GT, $RoundDP), round($AM, $RoundDP), round($GPA, $RoundDP), $OMC, $OMCT, $OMF, $OMFT);
			}
		}

		return array($ClassStudentArray, $StudentIDArray);
	}

	/*
	* Get Class Teacher(s) by ClassLevelID
	*/
	function GET_CLASSTEACHER_BY_CLASSLEVEL($ParClassLevelID, $Signature=false)
	{
		//$NameField = getNameFieldByLang2("a.");
		$i_title_mr = "Mr.";
		$i_title_miss = "Miss";
		$i_title_mrs = "Mrs.";
		$i_title_ms = "Ms.";
		$i_title_dr = "Dr.";

         $title_field = "CASE a.Title
                         WHEN 0 THEN ' $i_title_mr '
                         WHEN 1 THEN ' $i_title_miss '
                         WHEN 2 THEN ' $i_title_mrs '
                         WHEN 3 THEN ' $i_title_ms '
                         WHEN 4 THEN ' $i_title_dr '
                         WHEN 5 THEN ' $i_title_prof '
                         ELSE '' END
                         ";

		$sql = "SELECT
					c.ClassTitleEN,
					a.ChineseName,
					a.EnglishName,
					$title_field,
					c.YearClassID as ClassID
				FROM
					INTRANET_USER as a
					LEFT JOIN YEAR_CLASS_TEACHER as b ON a.UserID = b.UserID
					LEFT JOIN YEAR_CLASS as c ON b.YearClassID = c.YearClassID
				WHERE
					c.YearID = '$ParClassLevelID'
					AND a.RecordType = 1
					AND a.RecordStatus = 1
					And c.AcademicYearID = '".$this->Year."'
				ORDER BY
					c.Sequence
				";
		$row = $this->returnArray($sql,4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($class_name, $teacher_chi_name, $teacher_eng_name, $title_english,$class_id) = $row[$i];
			if ($Signature)
			{
				$ReturnArray[$class_id][] = $title_english." ".$teacher_eng_name."<br />".$teacher_chi_name;
			}
			else
			{
				$ReturnArray[$class_name][] = ($teacher_chi_name=="") ? $teacher_eng_name : $teacher_eng_name." (".$teacher_chi_name.")";
			}
		}

		return $ReturnArray;
	}

	/*
	* Get Report Marksheet Score
	*/
	function GET_REPORT_MARKSHEET_SCORES($ParSubjectIDList, $ParStudentIDList, $ParYear = -1, $ParSemester = -1)
	{
		global $eclass_db;
//asdfasdf
		global $ReportID;
		if ($ReportID != "") {
			$RoundDP = $this->GET_DECIMAL_POINT($ReportID);
		} else {
			$RoundDP = 2;
		}

		if ($ParYear == -1) $ParYear = $this->Year;
		if ($ParSemester == -1) $ParSemester = $this->Semester;

		if(!empty($ParSubjectIDList) && !empty($ParStudentIDList))
		{
			$sql = "SELECT
						a.ReportColumnID,
						a.SubjectID,
						a.StudentID,
						a.IsOverall,
						a.IsWeighted,
						a.Mark,
						a.Grade,
						a.OrderMeritForm,
						a.OrderMeritClass
					FROM
						RC_MARKSHEET_SCORE as a
					WHERE
						a.SubjectID IN ($ParSubjectIDList)
						AND a.StudentID IN ($ParStudentIDList)
						AND a.Year = '".$ParYear."'
						AND a.Semester = '".$ParSemester."'
					";
			$row = $this->returnArray($sql, 11);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($RCID, $SubjectID, $StudentID, $IsOverall, $IsWeighted, $Mark, $Grade, $OMF, $OMC) = $row[$i];

				if($IsOverall==1) {
					$ReturnArray[$StudentID][$SubjectID]["Overall"]["Grade"] = $Grade;
					$ReturnArray[$StudentID][$SubjectID]["Overall"]["Mark"] = round($Mark, $RoundDP);
					$ReturnArray[$StudentID][$SubjectID]["Overall"]["OMF"] = $OMF;
					$ReturnArray[$StudentID][$SubjectID]["Overall"]["OMC"] = $OMC;
				}
				else {
					if($IsWeighted==0) {
						$ReturnArray[$StudentID][$SubjectID][$RCID]["Grade"] = $Grade;
						$ReturnArray[$StudentID][$SubjectID][$RCID]["RawMark"] = round($Mark, $RoundDP);
						$ReturnArray[$StudentID][$SubjectID][$RCID]["OMF"] = $OMF;
						$ReturnArray[$StudentID][$SubjectID][$RCID]["OMC"] = $OMC;
					}
					else {
						$ReturnArray[$StudentID][$SubjectID][$RCID]["WeightedMark"] = round($Mark, $RoundDP);
					}
				}
			}
		}

		return $ReturnArray;
	}

	/*
	* Get Highlights from file
	*/
	function GET_HIGHLIGHTS()
	{
		global $intranet_root;

		$li = new libfilesystem();
        $file_content = trim($li->file_read($intranet_root."/file/reportcard/highlights_setting.txt"));

        if ($file_content != "")
        {
	        $content = explode("\n", $file_content);

	        list($failed_content, $distinction_content) = $content;

	        $failed_style = explode("|", $failed_content);
	        if ($failed_style != "")
	        {
		        if (!is_array($failed_style)) $failed_style = array($failed_style);

		        if ($pos = strpos($failed_style[sizeof($failed_style)-1], ':'))
		        {
			        $TempFailColor = explode(':', $failed_style[sizeof($failed_style)-1]);
			        $FailColor = (is_array($TempFailColor) && sizeof($TempFailColor) > 0) ? $TempFailColor[1] : "";
			        $failed_style[sizeof($failed_style)-1] = substr($failed_style[sizeof($failed_style)-1], 0, $pos);
		        }
	        }

	        $distinction_style = explode("|", $distinction_content);
	        if ($distinction_style != "")
	        {
		        if (!is_array($distinction_style)) $distinction_style = array($distinction_style);

		        if ($pos = strpos($distinction_style[sizeof($distinction_style)-1], ':'))
		        {
			        $TempDistinctionColor = explode(':', $distinction_style[sizeof($distinction_style)-1]);
			        $DistinctionColor = (is_array($TempDistinctionColor) && sizeof($TempDistinctionColor) > 0) ? $TempDistinctionColor[1] : "";
			        $distinction_style[sizeof($distinction_style)-1] = substr($distinction_style[sizeof($distinction_style)-1], 0, $pos);
		        }
	        }
        }

		return array($failed_style, $distinction_style, $FailColor, $DistinctionColor);
	}

	/*
	/*
	* Get Highlights Style
	*/
	function GET_HIGHLIGHTS_STYLE()
	{
		if($this->HighlightStyleArray==null)
		{
			list($FailedStyleArray, $DistinctionStyleArray, $FailColor, $DistinctionColor) = $this->GET_HIGHLIGHTS();

			if(!empty($FailedStyleArray))
			{
				$FailedFontStyle = "";
				for($i=0; $i<sizeof($FailedStyleArray); $i++)
				{
					$style = $FailedStyleArray[$i];

					if($style=="color")
					{
						$FailedColorStyle = "color: $FailColor;";
					}
					else if($style=="underline")
					{
						$FailedUnderlineStyle = "text-decoration: underline;";
					}
					else
					{
						$FailedFontStyle .= ($FailedFontStyle=="") ? "Font: " : "";
						$FailedFontStyle .= $style." ";
					}
				}
				$FailedFontStyle = (!empty($FailedFontStyle)) ? $FailedFontStyle.";" : "";
				if($FailedFontStyle!="" || $FailedColorStyle!="" || $FailedUnderlineStyle!="")
				{
					$FailedFontPrefix = "<span style='$FailedFontStyle $FailedUnderlineStyle $FailedColorStyle' >";
					$FailedFontSuffix = "</span>";
				}
			}

			if(!empty($DistinctionStyleArray))
			{
				$DistinctionFontStyle = "";
				for($i=0; $i<sizeof($DistinctionStyleArray); $i++)
				{
					$style = $DistinctionStyleArray[$i];

					if($style=="color")
					{
						$DistinctionColorStyle = "color: $DistinctionColor;";
					}
					else if($style=="underline")
					{
						$DistinctionUnderlineStyle = "text-decoration: underline;";
					}
					else
					{
						$DistinctionFontStyle .= ($DistinctionFontStyle=="") ? "Font: " : "";
						$DistinctionFontStyle .= $style." ";
					}
				}
				$DistinctionFontStyle = (!empty($DistinctionFontStyle)) ? $DistinctionFontStyle.";" : "";
				if($DistinctionFontStyle!="" || $DistinctionColorStyle || $DistinctionUnderlineStyle!="")
				{
					$DistinctionFontPrefix = "<span style='$DistinctionFontStyle $DistinctionUnderlineStyle $DistinctionColorStyle' >";
					$DistinctionFontSuffix = "</span>";
				}
			}
			$this->HighlightStyleArray = array($FailedFontPrefix, $FailedFontSuffix, $DistinctionFontPrefix, $DistinctionFontSuffix);
		}

		return $this->HighlightStyleArray;
	}

	/*
	* Get Highlights from file
	*/
	function GET_CALCULATION_METHOD()
	{
		global $intranet_root;

		$li = new libfilesystem();
        $file_content = trim($li->file_read($intranet_root."/file/reportcard/calmethod_setting.txt"));

        if ($file_content != "")
        {
	        $content = explode("\n", $file_content);
	        list($CalculationMethodRule, $CalculationOrder) = $content;
        }
		return array($CalculationMethodRule, $CalculationOrder);
	}

	/*
	* Get Grademark by ReportID
	*/
	function GET_REPORT_GRADEMARK($ParSubjectGradingList)
	{
		# Get Scheme GradeMark Info
		$sql = "SELECT
					s.SubjectID,
					g.Nature,
					g.LowerLimit,
					g.Grade,
					g.GradePoint
				FROM
					RC_GRADING_SCHEME_GRADEMARK as g
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME as s ON g.SchemeID = s.SchemeID
				WHERE
					s.SubjectGradingID IN ($ParSubjectGradingList)
				ORDER BY
					s.SubjectID,
					g.Nature DESC,
					g.LowerLimit DESC
				";
		$row = $this->returnArray($sql, 5);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $Nature, $LowerLimit, $Grade, $GradePoint) = $row[$i];
			/*
			if($Nature=="F")
			{
				$FailArray[$SubjectID]["LL"][] = $LowerLimit;
				$FailArray[$SubjectID]["G"][] = $Grade;
				$FailArray[$SubjectID]["GP"][] = $GradePoint;
			}
			else if($Nature=="D")
			{
				$DistinctionArray[$SubjectID]["LL"] = $LowerLimit;
				$DistinctionArray[$SubjectID]["G"] = $Grade;
				$DistinctionArray[$SubjectID]["GP"] = $GradePoint;
			}
			*/

			if($Nature=="P")
			{
				# andy, 2008-02-21
				//$FailBound = $LowerLimit;
				$FailBound[$SubjectID] = $LowerLimit;
			} elseif($Nature=="F")
			{
				if (!isset($FailArray[$SubjectID])) {
					//$FailArray[$SubjectID]["LL"][] = (isset($FailBound)) ? $FailBound : $LowerLimit;
					$FailArray[$SubjectID]["LL"][] = (isset($FailBound[$SubjectID])) ? $FailBound[$SubjectID] : $LowerLimit;
				} else {
					$FailArray[$SubjectID]["LL"][] = $LowerLimit;
				}
				//$FailArray[$SubjectID]["LL"][] = (!isset($FailArray[$SubjectID])) ? $FailBound : $LowerLimit;
				$FailArray[$SubjectID]["G"][] = $Grade;
				$FailArray[$SubjectID]["GP"][] = $GradePoint;
			} elseif($Nature=="D")
			{
				$DistinctionArray[$SubjectID]["LL"] = $LowerLimit;
				$DistinctionArray[$SubjectID]["G"] = $Grade;
				$DistinctionArray[$SubjectID]["GP"] = $GradePoint;
			}
		}

		# Get Pass/Fail
		$sql = "SELECT
					s.SubjectID,
					g.Pass,
					g.Fail
				FROM
					RC_GRADING_SCHEME as g
					LEFT JOIN RC_SUBJECT_GRADING_SCHEME as s ON g.SchemeID = s.SchemeID
				WHERE
					s.SubjectGradingID IN ($ParSubjectGradingList)
					AND g.SchemeType = 'PF'
				ORDER BY
					s.SubjectID
				";
		$row = $this->returnArray($sql, 3);
		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $Pass, $Fail) = $row[$i];
			$PFArray[$SubjectID]["Pass"] = $Pass;
			$PFArray[$SubjectID]["Fail"] = $Fail;
		}

		return array($FailArray, $DistinctionArray, $PFArray);
	}

	/*
	* Get Class Highest Averate
	*/
	function GET_CLASS_HIGHEST_AVERAGE($ParFormList)
	{
		//asdfasdf�@
		global $ReportID;
		if ($ReportID != "") {
			$RoundDP = $this->GET_DECIMAL_POINT($ReportID);
		} else {
			$RoundDP = 2;
		}
		if(!empty($ParFormList))
		{
			$sql = "SELECT
						a.Semester,
						c.ClassID,
						MAX(a.AverageMark)
					FROM
						RC_REPORT_RESULT as a
						LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
						LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
					WHERE
						c.ClassLevelID IN ($ParFormList)
						AND b.RecordType = 2
						AND b.RecordStatus = 1
						AND a.Year = '".$this->Year."'
					GROUP BY
						a.Semester,
						c.ClassID
					";
			$row = $this->returnArray($sql, 3);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($Semester, $ClassID, $HighestAverage) = $row[$i];
				$ReturnArray[$ClassID][trim($Semester)] = round($HighestAverage, $RoundDP);
			}
		}

		return $ReturnArray;
	}

	/*
	* Get Number of pupils in class(es)
	*/
	function GET_CLASS_STUDENT_COUNT($ParClassID="")
	{
		$cond = ($ParClassID!="") ? " AND yc.YearClassID = '".$ParClassID."'" : "";
		$sql = "SELECT
					yc.YearClassID as ClassID,
					COUNT(ycu.YearClassUserID)
				FROM
					YEAR_CLASS as yc
					LEFT JOIN YEAR_CLASS_USER as ycu On (yc.YearClassID = ycu.YearClassID)
					LEFT JOIN INTRANET_USER as u On (ycu.UserID = u.UserID)
				WHERE
					u.RecordType = 2
					AND u.RecordStatus = 1
					And yc.AcademicYearID = '".$this->Year."'
					$cond
				GROUP BY
					yc.YearClassID
				ORDER BY
					yc.ClassTitleEN
				";
		$row = $this->returnArray($sql, 2);
		
		for($i=0; $i<sizeof($row); $i++)
		{
			list($ClassID, $StudentCount) = $row[$i];
			$ReturnArray[$ClassID] = $StudentCount;
		}

		return $ReturnArray;
	}

	/*
	* Get Number of pupils in form(s)
	*/
	function GET_FORM_STUDENT_COUNT($ParClassLevelID="")
	{
		$cond = ($ParClassLevelID!="") ? " AND a.YearID = '".$ParClassLevelID."'" : "";
		$sql = "SELECT
					a.YearID as ClassLevelID,
					COUNT(ycu.YearClassUserID)
				FROM
					YEAR as a
					LEFT JOIN YEAR_CLASS as b ON a.YearID = b.YearID
					LEFT JOIN YEAR_CLASS_USER as ycu ON b.YearClassID = ycu.YearClassID
					LEFT JOIN INTRANET_USER as c ON ycu.UserID = c.UserID
				WHERE
					a.RecordStatus = 1
					AND c.RecordType = 2
					AND c.RecordStatus = 1
					And b.AcademicYearID = '".$this->Year."'
					$cond
				GROUP BY
					a.YearID
				ORDER BY
					a.Sequence
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($ClassLevelID, $StudentCount) = $row[$i];
			$ReturnArray[$ClassLevelID] = $StudentCount;
		}

		return $ReturnArray;
	}
	

	/*
	* Get result style
	*/
	function GET_STYLE($ParSetting, $ParDisplayResult, $ParFailArray, $ParDistArray, $ParPassFailArray)
	{
		global $ReportCardTemplate;

		# get highlights
		list($FailedFontPrefix, $FailedFontSuffix, $DistinctionFontPrefix, $DistinctionFontSuffix) = $this->GET_HIGHLIGHTS_STYLE();

		$ParDisplayResult = trim($ParDisplayResult);
		if($ParSetting=="S")
		{
			$TargetFailArray = $ParFailArray["LL"];
			$TargetDist = $ParDistArray["LL"];

			if($TargetDist!="" && $ParDisplayResult>=$TargetDist) {
				$IsDistinct = 1;
			} else if(!empty($TargetFailArray))
			{
				$FailMark = $TargetFailArray[0];
				$IsFailed = ($ParDisplayResult<$FailMark) ? 1 : 0;
			}
		}
		else if($ParSetting=="G")
		{
			$TargetFailArray = $ParFailArray["G"];
			$TargetDist = trim($ParDistArray["G"]);
			if($TargetDist!="" && $ParDisplayResult==$TargetDist) {
				$IsDistinct = 1;
			}
			else if(!empty($TargetFailArray) && in_array($ParDisplayResult, $TargetFailArray)) {
				$IsFailed = 1;
			}
		}
		else
		{
			$IsFailed = ($ParDisplayResult==trim($ParPassFailArray["Fail"])) ? 1 : 0;
		}

		if ($ReportCardTemplate == 4) $FailedFontPrefix.= "*";

		if($IsDistinct)
			return array($DistinctionFontPrefix, $DistinctionFontSuffix, 1, 0);
		else if($IsFailed)
			return array($FailedFontPrefix, $FailedFontSuffix, 0, 1);
		else
			return array("", "", 0, 0);
	}

######################################## Report Generation & Printing Functions End #########################################
############################################################################################################################

######################################## Marksheet Submission and Result Verification Functions Start #########################

	/*
	* Get Report Template Form(s)
	*/
	function GET_REPORT_FORMS($ParReportID)
	{
		$sql = "SELECT
					a.ClassLevelID,
					b.YearName as LevelName
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN YEAR as b ON a.ClassLevelID = b.YearID
				WHERE
					a.ReportID = '$ParReportID'
					AND b.RecordStatus = 1
				ORDER BY
					b.Sequence
				";
		$row = $this->returnArray($sql, 2);
		
		return $row;
	}


	/*
	* Get Report Form(s) without ReportID
	*/
	function GET_REPORT_FORMS_ID($ParReportID)
	{
		$sql = "SELECT
					distinct(a.ClassLevelID)
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN YEAR as b ON a.ClassLevelID = b.YearID
				WHERE
					a.ReportID = '$ParReportID'
					AND b.RecordStatus = 1
				ORDER BY
					b.Sequence
				";
		$row = $this->returnVector($sql);

		return $row;
	}

	/*
	* Get Report Form(s) without ReportID
	*/
	function GET_REPORT_FORMS_WITHOUT_REPORTID()
	{
		$sql = "SELECT
					distinct(a.ClassLevelID)
				FROM
					RC_REPORT_TEMPLATE_FORM as a
					LEFT JOIN YEAR as b ON a.ClassLevelID = b.YearID
				WHERE
					 b.RecordStatus = 1
				";
		$row = $this->returnVector($sql);

		return $row;
	}
	/*
	* Get Report Template Class(s) Selection
	*/
	function GET_REPORT_CLASSES($ParClassLevelID, $ParReportID)
	{
		$ReportInfoArrr = $this->GET_REPORT_TEMPLATE($ParReportID);
		$ReportAcademicYearID = $ReportInfoArrr['AcademicYear'];
		
		$ClassNameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
		$sql = "SELECT
					YearClassID as ClassID,
					$ClassNameField as ClassName
				FROM
					YEAR_CLASS
				WHERE
					YearID = '$ParClassLevelID'
					And
					AcademicYearID = '$ReportAcademicYearID'
				ORDER BY
					Sequence
				";
		$row = $this->returnArray($sql, 2);

		return $row;
	}

	/*
	* Get Report Student Selection
	*/
	function GET_REPORT_STUDENT_SELECTION($ParClassID)
	{
		$NameField = getNameFieldByLang2("iu.");
		$ClassNumField = getClassNumberField("a.");
		$ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
		
		$sql = "SELECT
					iu.UserID,
					CONCAT('<', $ClassNameField, ' - ', ycu.ClassNumber, '> ', $NameField)
				FROM
					YEAR_CLASS_USER as ycu
					Inner Join
					YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
					Inner Join
					INTRANET_USER as iu On (ycu.UserID = iu.UserID)
				WHERE
					yc.YearClassID = '$ParClassID'
					AND iu.RecordType = 2
					AND iu.RecordStatus = 1
				ORDER BY
					ycu.ClassNumber
				";
		$row = $this->returnArray($sql, 2);
		
		if(!empty($row))
		{
			$ReturnValue = "<SELECT size='21' multiple='multiple' name=\"TargetStudentID[]\">";
			for($i=0; $i<sizeof($row); $i++)
			{
				list($StudentID, $StudentName) = $row[$i];
				$ReturnValue .= "<option value='".$StudentID."' SELECTED>".$StudentName."</option>\n";
			}
			$ReturnValue .= "</SELECT>";
		}

		return $ReturnValue;
	}

	/*
	* GET Classes by form
	*/
	function GET_CLASSES_BY_FORM($YearID="", $AcademicYearID='')
	{
		/*
		$cond = ($ParClassLevelID!="") ? " AND ClassLevelID = '$ParClassLevelID'" : "";
		$sql = "SELECT ClassID, ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 {$cond}";
		$row = $this->returnArray($sql, 2);
		*/
		
		if ($AcademicYearID == '')
			$AcademicYearID = $this->Year;
		
		if ($YearID != '')
			$conds_YearID = " And YearID = '".$YearID."' ";
			
		$ClassNameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
		$sql = "Select 
						YearClassID as ClassID, 
						$ClassNameField as ClassName
				From
						YEAR_CLASS
				Where
						AcademicYearID = '".$AcademicYearID."'
						$conds_YearID
				";
		$row = $this->returnArray($sql);
		return $row;
	}

	/*
	* Check Marksheet Result exist or not
	*/
	function GET_MARKSHEET_RESULT_EXIST()
	{
		$sql = "SELECT
					a.SubjectID,
					c.ClassID,
					COUNT(a.MarksheetScoreID)
				FROM
					RC_MARKSHEET_SCORE as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN INTRANET_CLASS as c ON b.ClassName = c.ClassName
				WHERE
					a.Year = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND b.RecordType = 2
					AND b.RecordStatus = 1
				GROUP BY
					a.SubjectID,
					c.ClassID
				ORDER BY
					a.SubjectID,
					c.ClassID
				";
		$row = $this->ReturnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $ClassID, $RecordCount) = $row[$i];
			$ReturnArray[$ClassID][$SubjectID] = ($RecordCount>0) ? 1 : 0;
		}

		return $ReturnArray;
	}

	/*
	* Get Class Confirmation Count
	*/
	function GET_CLASS_CONFIRMATION_COUNT($ParReportID, $ParClassLevelID, $AcademicYearID)
	{
		global $PATH_WRT_ROOT;
		
		$YearTermIDArr = $this->GET_REPORT_INVOLVED_TERM($ParReportID);
		
		# Get class student count
		$sql = "SELECT
					yc.YearClassID as ClassID,
					st.SubjectID,
					COUNT(iu.UserID) as NumOfStudent
				FROM
					YEAR_CLASS_USER as ycu
					Inner Join
					INTRANET_USER as iu On (ycu.UserID = iu.UserID)
					Inner Join
					YEAR_CLASS as yc ON ycu.YearClassID = yc.YearClassID
					Inner Join
					SUBJECT_TERM_CLASS_USER as scu On (ycu.UserID = scu.UserID)
					Inner Join
					SUBJECT_TERM_CLASS as stc On (scu.SubjectGroupID = stc.SubjectGroupID)
					Inner Join
					SUBJECT_TERM as st On (stc.SubjectGroupID = st.SubjectGroupID)
				WHERE
					yc.AcademicYearID = '".$AcademicYearID."'
					And	yc.YearID = '$ParClassLevelID'
					AND iu.RecordType = 2
					AND iu.RecordStatus = 1
					And st.YearTermID In (".implode(',', $YearTermIDArr).")
				GROUP BY
					yc.YearClassID, st.SubjectID
				ORDER BY
					yc.Sequence
				";
		$row1 = $this->returnArray($sql, 2);
		
		for($i=0; $i<sizeof($row1); $i++)
		{
			$thisClassID = $row1[$i]['ClassID'];
			$thisSubjectID = $row1[$i]['SubjectID'];
			$thisNumOfStudent = $row1[$i]['NumOfStudent'];
			
			$ReturnArray[$thisClassID][$thisSubjectID]['TotalNumOfStudent'] = $thisNumOfStudent;
		}
		
		$sql = "SELECT
					d.SubjectID,
					c.ClassID,
					COUNT(a.FeedbackID)
				FROM
					RC_MARKSHEET_FEEDBACK as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN YEAR_CLASS_USER as ycu On (b.UserID = ycu.UserID)
					LEFT JOIN YEAR_CLASS as c ON ycu.YearClassID = c.YearClassID
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as d ON a.SubjectID = d.SubjectID
				WHERE
					a.IsComment = 0
					AND a.Year = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND c.YearID = '$ParClassLevelID'
					AND b.RecordType = 2
					AND b.RecordStatus = 1
					AND d.ReportID = '$ParReportID'
					AND c.YearID = '".$this->Year."'
				GROUP BY
					d.SubjectID,
					c.YearClassID
				ORDER BY
					d.SubjectID,
					c.Sequence
				";
		$row2 = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row2); $i++)
		{
			list($subject_id, $class_id, $confirmed_num) = $row2[$i];
			$ReturnArray[$class_id][$subject_id]['NumOfConfirmed'] = $confirmed_num;
		}

		return $ReturnArray;
	}
	
	function GET_REPORT_INVOLVED_TERM($ParReportID)
	{
		global $PATH_WRT_ROOT;
		# Get the related Term invloved in the report card
		$ReportInfoArr = $this->GET_REPORT_TEMPLATE($ParReportID);
		$YearTermID = $ReportInfoArr['ReportType'];
		
		$YearTermIDArr = array();
		if ($YearTermID == 'F')
		{
			include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
			$AcademicYearObj = new academic_year($ReportInfoArr['AcademicYear']);
			$TermInfoArr = $AcademicYearObj->Get_Term_List($returnAsso=0);
			$YearTermIDArr = Get_Array_By_Key($TermInfoArr, 'YearTermID');
		}
		else
		{
			$YearTermIDArr = array($YearTermID);
		}
		
		return $YearTermIDArr;
	}

	function GET_USER_PHOTO($ParUserID)
	{
		$Sql = "
					SELECT
							WebSAMSRegNo
					FROM
							INTRANET_USER
					WHERE
							UserID = '$ParUserID'
				";
		$row = $this->returnVector($Sql);
		if (str_replace("#", "", $row[0]) != "") {
			return "/file/official_photo/".str_replace("#", "", $row[0]).".jpg";
		} else {
			return "";
		}
	}

######################################## Report Generation & Printing Functions End #########################################
############################################################################################################################

######################################## Marksheet Revision Functions Start ##################################

	/*
	* Get Report Template Form(s)
	*/
	function GET_ALL_REPORT_FORMS()
	{
		$sql = "SELECT DISTINCT
					b.ClassLevelID,
					c.YearName as LevelName
				FROM
					RC_REPORT_TEMPLATE as a
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as b ON a.ReportID = b.ReportID
					LEFT JOIN YEAR as c ON b.ClassLevelID = c.YearID
				WHERE
					a.RecordStatus = 1
					AND c.RecordStatus = 1
				ORDER BY
					c.Sequence
				";
		$row = $this->returnArray($sql, 2);

		return $row;
	}

	/*
	* Get Subject by Form
	*/
	function GET_SUBJECT_BY_FORM($ParClassLevelID)
	{
		global $eclass_db;

		$SubjectField = "CONCAT(a.EN_DES, ' (', a.CH_DES, ')')";
		$sql = "SELECT DISTINCT
						a.RecordID,
						$SubjectField
					FROM
						ASSESSMENT_SUBJECT AS a
						LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT AS b ON a.RecordID = b.SubjectID
						LEFT JOIN RC_REPORT_TEMPLATE_FORM as c ON b.ReportID = c.ReportID
						LEFT JOIN RC_REPORT_TEMPLATE as d ON c.ReportID = d.ReportID
					WHERE
						a.EN_SNAME IS NOT NULL
						AND (a.CMP_CODEID IS NULL Or a.CMP_CODEID = '')
						And a.RecordStatus = 1
						AND c.ClassLevelID = '$ParClassLevelID'
						AND d.RecordStatus = 1
					ORDER BY
						a.DisplayOrder
				";
		$row = $this->returnArray($sql, 2);

		return $row;
	}

	/*
	* Get Class Notification Status
	*/
	function GET_CLASS_NOTIFICATION($ParSubjectID, $ParClassLevelID="")
	{
		$cond = ($ParClassLevelID!="") ? " AND a.ClassLevelID = '$ParClassLevelID'" : "";
		$sql = "SELECT
					a.ClassID,
					b.NotificationID
				FROM
					INTRANET_CLASS as a
					LEFT JOIN RC_MARKSHEET_NOTIFICATION as b ON a.ClassID = b.ClassID
				WHERE
					b.SubjectID = '$ParSubjectID'
					AND a.RecordStatus = 1
					$cond
				ORDER BY
					a.ClassName
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($ClassID, $NotificationID) = $row[$i];
			$ReturnArray[$ClassID] = ($NotificationID=="") ? 0 : 1;
		}

		return $ReturnArray;
	}

	/*
	* Get Class Last Modified of Marksheet Submission
	*/
	function GET_CLASS_MARKSHEET_LAST_MODIFIED($ParSubjectList, $ParClassLevelID="")
	{
		global $eclass_db;

		if(!empty($ParSubjectList))
		{
			$cond = ($ParClassLevelID!="") ? " AND c.YearID = '$ParClassLevelID'" : "";
			$NameField = getNameFieldByLang2("d.");
			$sql = "SELECT
						c.YearClassID as ClassID,
						MAX(a.DateModified),
						$NameField
					FROM
						RC_MARKSHEET_SCORE as a
						LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
						LEFT JOIN YEAR_CLASS as c ON b.ClassName = c.ClassTitleEN
						LEFT JOIN INTRANET_USER as d ON a.TeacherID = d.UserID
					WHERE
						a.SubjectID IN ($ParSubjectList)
						AND a.Year = '".$this->Year."'
						AND c.AcademicYearID = '".$this->Year."'
						AND a.Semester = '".$this->Semester."'
						AND b.RecordType = 2
						AND b.RecordStatus = 1
						$cond
					GROUP BY
						c.YearClassID
					ORDER BY
						c.Sequence
					";
			$row = $this->returnArray($sql, 3);

			for($i=0; $i<sizeof($row); $i++)
			{
				list($ClassID, $LastModifiedDate, $LastModifiedBy) = $row[$i];
				$ReturnArray[$ClassID] = array($LastModifiedDate, $LastModifiedBy);
			}
		}

		return $ReturnArray;
	}

######################################## Marksheet Revision Functions End ####################################
###############################################################################################################

######################################## Score List Functions Start ##########################################

	function IS_CLASS_TEACHER($YearClassID='')
	{
		$Class_Cond = '';
		if ($YearClassID != '')
			$Class_Cond = " AND yc.YearClassID = '$YearClassID' ";
			
		$sql = "SELECT
						DISTINCT(yct.YearClassID), ClassTitleEN as ClassName
				FROM
						YEAR_CLASS_TEACHER as yct
						INNER JOIN
						YEAR_CLASS as yc ON (yct.YearClassID = yc.YearClassID)
				WHERE
						yct.UserID = '".$_SESSION['UserID']."'
						AND yc.AcademicYearID = '".$this->Year."'
						$Class_Cond
				";
		$resultSet = $this->returnArray($sql);
		
		if (count($resultSet) > 0)
		{
			$this->IsClassTeacher = 1;
			$this->TeachingClassID = $resultSet[0]['YearClassID'];
			$this->TeachingClassName = $resultSet[0]['ClassName'];
			return true;
		}
		else
			return false;
	}

	/*
	* Check if the user is the subject teacher of current class
	*/
	function GET_TEACHING_CLASS()
	{
		$sql = "SELECT
					b.ClassName
				FROM
					INTRANET_CLASSTEACHER as a
					LEFT JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
				WHERE
					a.UserID='".$this->uid."'
				";
			$row = $this->returnVector($sql, 1);

		return $row[0];
	}


	/*
	* GET Score List Result
	*/
	function GET_STUDENT_SUBJECT_RESULT($ParClassID, $ParSubjectID="")
	{
		//asdfasdf
		global $ReportID;
		if ($ReportID != "") {
			$RoundDP = $this->GET_DECIMAL_POINT($ReportID);
		} else {
			$RoundDP = 2;
		}
		$sql = "SELECT
					a.StudentID,
					a.SubjectID,
					a.Mark,
					a.Grade
				FROM
					RC_MARKSHEET_SCORE as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN YEAR_CLASS as c ON a.ClassName = c.ClassTitleEN
				WHERE
					a.IsOverall = 1
					AND a.Year = '".$this->Year."'
					AND c.AcademicYearID = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND c.YearClassID = '$ParClassID'
					AND b.RecordType = 2
					AND b.RecordStatus = 1
				";
		if($ParSubjectID != "")
		  $sql .= "AND a.SubjectID = '".$ParSubjectID."'";

		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($StudentID, $SubjectID, $Mark, $Grade) = $row[$i];
			//$ReturnArray[$StudentID][$SubjectID] = ($Mark=="") ? $Grade : round($Mark, 1);
			$ReturnArray[$StudentID][$SubjectID]["Mark"] = round($Mark, $RoundDP);
			$ReturnArray[$StudentID][$SubjectID]["Grade"] = $Grade;
		}

		return $ReturnArray;
	}

	/*
	* GET Score List Result
	*/
	function GET_STUDENT_SUBJECT_COLUMN_RESULT($ParClassID, $ParReportColumnID, $ParIsWeighted=0, $ParSubjectID="")
	{
		//asdfasdf
		global $ReportID;
		if ($ReportID != "") {
			$RoundDP = $this->GET_DECIMAL_POINT($ReportID);
		} else {
			$RoundDP = 2;
		}
		$sql = "SELECT
					a.StudentID,
					a.SubjectID,
					a.Mark,
					a.Grade
				FROM
					RC_MARKSHEET_SCORE as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN YEAR_CLASS as c ON a.ClassName = c.ClassTitleEN
				WHERE
					a.ReportColumnID = '$ParReportColumnID'
					AND a.Year = '".$this->Year."'
					AND c.AcademicYearID = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND c.YearClassID = '$ParClassID'
					AND b.RecordType = 2
					AND b.RecordStatus = 1
					AND a.IsWeighted = '$ParIsWeighted'
				";
    if($ParSubjectID != "")
      $sql .= "AND a.SubjectID = '$ParSubjectID'";

		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($StudentID, $SubjectID, $Mark, $Grade) = $row[$i];
			//$ReturnArray[$StudentID][$SubjectID] = ($Mark=="") ? $Grade : round($Mark, 1);
			$ReturnArray[$StudentID][$SubjectID]["Mark"] = round($Mark, $RoundDP);
			$ReturnArray[$StudentID][$SubjectID]["Grade"] = $Grade;
		}

		return $ReturnArray;
	}

	/*
	* Get Report Subjects
	*/
	function GET_REPORT_SUBJECTS($ParReportID)
	{
		global $eclass_db, $intranet_session_language;

		$SubjectField = ($intranet_session_language=="en") ? "b.EN_ABBR" : "b.CH_ABBR";
		$CmpSubjectField =  ($intranet_session_language=="en") ? "c.EN_DES" : "c.CH_DES";
		$sql = "SELECT
					b.RecordID,
					$SubjectField,
					c.RecordID,
					$CmpSubjectField
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT jOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
					LEFT JOIN ASSESSMENT_SUBJECT as c ON b.CODEID = c.CODEID
				WHERE
					a.ReportID = '$ParReportID'
					AND b.EN_SNAME IS NOT NULL
					And b.RecordStatus = 1
					And c.RecordStatus = 1
				ORDER BY
					a.DisplayOrder
				";
		$row = $this->returnArray($sql, 4);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $SubjectName, $CmpSubjectID, $CmpSubjectName) = $row[$i];
			if($SubjectID==$CmpSubjectID)
			{
				$ReturnArray[$SubjectID] = $SubjectName;
			}
			else
			{
				$ReturnArray[$CmpSubjectID] = $SubjectName."<br />".$CmpSubjectName;
			}
		}

		return $ReturnArray;
	}

######################################## Score List Functions End ############################################
###############################################################################################################


######################################## Teacher Comment Functions Start ############################################

	/*
	* Get class/subject teacher comments
	*/
	function GET_TEACHER_COMMENT($ParClassID, $ParSubjectID="")
	{
		$cond = ($ParSubjectID!="") ? " AND a.SubjectID = '$ParSubjectID'" : " AND a.SubjectID IS NULL";
		$sql = "SELECT
					a.StudentID,
					a.Comment
				FROM
					RC_MARKSHEET_COMMENT as a
					LEFT JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT JOIN YEAR_CLASS as c ON a.ClassName = c.ClassTitleEN
				WHERE
					c.YearClassID = '".$ParClassID."'
					AND a.Year = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					$cond
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			$StudentID = $row[$i][0];
			$Comment = $row[$i][1];
			$ReturnArray[$StudentID] = trim($Comment);
		}

		return $ReturnArray;
	}

	/*
	* Get report class teacher comments
	*/
	function GET_REPORT_CLASS_TEACHER_COMMENT($LevelList, $ClassID="", $TargetStudentList="")
	{
		if($TargetStudentList!="")
		{
			$cond = " AND a.StudentID IN ($TargetStudentList)";
		}
		else if($ClassID!="")
		{
			$cond = " AND c.YearClassID = '$ClassID'";
		}
		else if ($LevelList!="")
		{
			$cond = " AND c.YearID IN ($LevelList)";
		}

		$sql = "SELECT
					a.StudentID,
					a.Comment
				FROM
					RC_MARKSHEET_COMMENT as a
					LEFT JOIN YEAR_CLASS_USER as b ON a.StudentID = b.UserID
					LEFT JOIN YEAR_CLASS as c ON b.YearClassID = c.YearClassID
				WHERE
					a.Year = '".$this->Year."'
					And c.AcademicYearID = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					AND a.SubjectID IS NULL
					$cond
				";
		$row = $this->returnArray($sql, 2);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($StudentID, $Comment) = $row[$i];
			$ReturnArray[$StudentID] = $Comment;
		}

		return $ReturnArray;
	}

		/*
	* Get report subject teacher comments
	*/
	function GET_REPORT_SUBJECT_TEACHER_COMMENT($LevelList, $ClassID="", $TargetStudentList="")
	{
		if($TargetStudentList!="")
		{
			$cond = " AND a.StudentID IN ($TargetStudentList)";
		}
		else if($ClassID!="")
		{
			$cond = " AND c.YearClassID = '$ClassID'";
		}
		else if ($LevelList!="")
		{
			$cond = " AND c.YearID IN ($LevelList)";
		}

		$sql = "SELECT
					a.SubjectID,
					a.StudentID,
					a.Comment
				FROM
					RC_MARKSHEET_COMMENT as a
					LEFT JOIN YEAR_CLASS_USER as b ON a.StudentID = b.UserID
					LEFT JOIN YEAR_CLASS as c ON b.YearClassID = c.YearClassID
				WHERE
					a.Year = '".$this->Year."'
					And c.AcademicYearID = '".$this->Year."'
					AND a.Semester = '".$this->Semester."'
					$cond
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($SubjectID, $StudentID, $Comment) = $row[$i];
			$ReturnArray[$StudentID][$SubjectID] = $Comment;
		}

		return $ReturnArray;
	}

######################################## Teacher Comment Functions End ############################################
###############################################################################################################

######################################## Data Removal Functions Start ############################################

	/*
	* Retrieve existing assessment data Year
	*/
	function GET_ASSESSMENT_YEAR_SELECTION(&$ParYear)
	{
		global $eReportCard;

		# retrieve existing Year in table RC_MARKSHEET_SCORE
		$sql = "SELECT DISTINCT Year FROM RC_MARKSHEET_SCORE";
		$row_year = $this->returnVector($sql, 2);
		$ParYear = ($ParYear=="") ? $row_year[0] : $ParYear;
		
		$YearNameField = Get_Lang_Selection('YearNameB5', 'YearNameEN');
		$sql = "Select AcademicYearID, $YearNameField From ACADEMIC_YEAR Where AcademicYearID In (".implode(',', $row_year).")";
		$AcademicYearInfoArr = $this->returnArray($sql);
		
		$YearSelection = (!empty($AcademicYearInfoArr)) ? getSelectByArray($AcademicYearInfoArr, "name='Year' onChange='document.form1.year_change.value=1;document.form1.submit()'", $ParYear, 1, 1, "") : "<i>".$eReportCard['NoRecord']."</i>";

		return $YearSelection;
	}

	/*
	* Retrieve existing assessment data Semester
	*/
	function GET_ASSESSMENT_SEMESTER_SELECTION($ParYear, $ParSemester)
	{
		global $eReportCard;

		# retrieve existing Year in table RC_MARKSHEET_SCORE
		$TermNameField = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
		$sql = "SELECT 
						DISTINCT Semester, if(Semester='FULL', '".$eReportCard['FullYear']."', $TermNameField) 
				FROM 
						RC_MARKSHEET_SCORE as ms
						Inner Join
						ACADEMIC_YEAR_TERM as ayt On (ms.Semester = ayt.YearTermID)
				WHERE 
						Year = '".$ParYear."'
				";
		$row_semester = $this->returnArray($sql, 2);

		$SemesterSelection = (!empty($row_semester)) ? getSelectByArray($row_semester, "name='Semester' onChange='document.form1.IsAnnual[1].checked=1'", $ParSemester, 1, 1, "") : "<i>".$eReportCard['NoRecord']."</i>";

		return $SemesterSelection;
	}

######################################## Data Removal Functions End ############################################
###############################################################################################################

	/*
	* Get Classes
	*/
	function GET_FORM_CLASSES()
	{
		$sql = "SELECT
					b.YearName as LevelName,
					a.ClassID,
					a.ClassName
				FROM
					INTRANET_CLASS as a
					LEFT JOIN YEAR as b ON a.ClassLevelID = b.YearID
				WHERE
					a.RecordStatus = 1
				ORDER BY
					b.Sequence,
					a.ClassName
				";
		$row = $this->returnArray($sql, 3);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($ClassLevel, $ClassID, $ClassName) = $row[$i];
			$ReturnArray[trim($ClassLevel)][] = array($ClassID, $ClassName);
		}

		return $ReturnArray;
	}

	// return class array
	function GET_CLASSES()
	{
		$sql = "SELECT
					distinct(a.ClassTitleEN)
				FROM
					YEAR_CLASS as a
					LEFT JOIN YEAR as b ON a.YearID = b.YearID
				WHERE
					a.AcademicYearID = '".$this->Year."'
				ORDER BY
					a.Sequence
				";
		$ReturnArray = $this->returnVector($sql);

		return $ReturnArray;
	}

	/*
	* Get report subject ID
	*/
	function GET_REPORT_COLUMN_ID($ParReportID)
	{
		$sql = "SELECT ReportColumnID FROM RC_REPORT_TEMPLATE_COLUMN WHERE ReportID = '".$ParReportID."'";
		$row = $this->returnVector($sql);

		return $row;
	}

	/*
	* Get GradeMark by Grading Scheme ID
	*/
	function GET_GRADEMARK_BY_SCHEMEID($ParSchemeID)
	{
		# Get Scheme GradeMark Info
		$sql = "SELECT
					LowerLimit,
					Grade
				FROM
					RC_GRADING_SCHEME_GRADEMARK
				WHERE
					SchemeID = '$ParSchemeID'
				ORDER BY
					LowerLimit DESC
				";
		$row = $this->returnArray($sql, 2);

		return $row;
	}

	/*
	* Get Semester Array
	*/
	function GET_SEMESTER_ARRAY()
	{
		$semester_data = getSemesters($this->Year, $ReturnAsso=0, $ParLang='en');
		/*
		for($i=0; $i<sizeof($semester_data); $i++)
		{
			$TempSemesterArray = explode("::", $semester_data[$i]);
			$SemesterArray[] = trim($TempSemesterArray[0]);
		}
		*/
		$SemesterArray = Get_Array_By_Key($semester_data, 'TermName');

		return $SemesterArray;
	}

	/*
	* Check whether result exist in the semester
	*/
	function CHECK_RESULT_EXIST($ParYear, $ParSemester)
	{
		$sql = "SELECT COUNT(*) FROM RC_MARKSHEET_SCORE WHERE Year = '".$ParYear."' AND Semester = '".$ParSemester."' AND IsOverall = 0";
		$row = $this->returnVector($sql);

		return ($row[0]>0) ? 1 : 0;
	}

	/*
	* Get Class Object
	*/
	function GET_CLASS_OBJ($ParClassID)
	{
		$ClassNameField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
		$sql = "SELECT YearClassID as ClassID, YearID as ClassLevelID, $ClassNameField as ClassName FROM YEAR_CLASS WHERE YearClassID = '".$ParClassID."'";
		$row = $this->returnArray($sql, 3);

		return $row[0];
	}

	/*
	* Get Student ClassName and ClassNumber by UserID
	*/
	function GET_CLASS_BY_USERID($ParUserID)
	{
		//$ClassNumField = getClassNumberField('ycu.');
		$NameField = getNameFieldByLang2('iu.');
		
		$sql = "SELECT 
						iu.UserID, yc.ClassTitleEN as ClassName, ycu.ClassNumber, {$NameField}, iu.WebSAMSRegNo, yc.YearClassID
				FROM 
						YEAR_CLASS_USER as ycu
						Inner Join
						YEAR_CLASS as yc On (ycu.YearClassID = yc.YearClassID)
						Inner Join
						INTRANET_USER as iu On (ycu.UserID = iu.UserID)
				WHERE 
						iu.UserID IN (".$ParUserID.") 
						And
						yc.AcademicYearID = '".$this->Year."'
				ORDER BY 
						ycu.ClassNumber
				";
		$row = $this->returnArray($sql, 5);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($user_id, $class_name, $class_number, $student_name, $WebSAMSRegNo, $YearClassID) = $row[$i];
			$ReturnArray[$user_id] = array($class_name, $class_number, $student_name, $WebSAMSRegNo, $YearClassID);
		}

		return $ReturnArray;
	}

	/*
	* Get Student ClassName and ClassNumber by UserID
	*/
	function GET_CLASSNAME_BY_USERID($ParUserID)
	{
		$sql = "SELECT ClassName FROM INTRANET_USER WHERE UserID = '".$ParUserID."'";
		$ReturnVector = $this->returnVector($sql, 1);

		return $ReturnVector[0];
	}

	/*
	* Count Marksheet confirmation of teacher
	*/
	function GET_TEACHER_CONFIRMATION_COUNT()
	{
		global $eclass_db;

		# get total marksheet number
		$sql = "SELECT DISTINCT
					a.SubjectID,
					a.ClassID
				FROM
					RC_SUBJECT_TEACHER as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
					LEFT JOIN INTRANET_CLASS as c ON a.ClassID = c.ClassID
					LEFT JOIN RC_REPORT_TEMPLATE_SUBJECT as d ON a.SubjectID = d.SubjectID
					LEFT JOIN RC_REPORT_TEMPLATE as e ON d.ReportID = e.ReportID
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as f ON e.ReportID = f.ReportID
				WHERE
					a.UserID = '".$this->uid."'
					AND (b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
					And b.RecordStatus = 1
					AND e.RecordStatus = 1
					AND c.ClassLevelID = f.ClassLevelID
				ORDER BY
					b.DisplayOrder,
					c.ClassName
				";
		$row = $this->returnArray($sql, 2);
		$TotalMarksheetCount = sizeof($row);

		if(!empty($TotalMarksheetCount))
		{
			$SubjectIDArray = array();
			for($j=0; $j<sizeof($row); $j++)
			{
				list($sid, $cid) = $row[$j];
				if(!in_array($sid, $SubjectIDArray))
					$SubjectIDArray[] = $sid;
			}
			$SubjectIDList = implode(",", $SubjectIDArray);

			# get confirmed marksheet
			$sql = "SELECT
						COUNT(a.NotificationID)
					FROM
						RC_MARKSHEET_NOTIFICATION as a
					WHERE
						a.TeacherID = '".$this->uid."'
						AND a.SubjectID IN ('{$SubjectIDList}')
					";
			$row = $this->returnVector($sql);

			$ReturnValue = $TotalMarksheetCount - $row[0];
		}
		else
			$ReturnValue = 0;

		return $ReturnValue;
	}

	/*
	* Get Class Confirmation Count
	*/
	function GET_STUDENT_VERIFICATION_COUNT($ParClassID='')
	{
		global $eclass_db;

		if($ParClassID=="")
		{
			$sql = "SELECT b.ClassID FROM INTRANET_USER as a LEFT JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName WHERE a.UserID = '".$this->uid."' AND b.RecordStatus = 1 LIMIT 1";
			$row = $this->returnVector($sql);
			$ParClassID = $row[0];
		}

		# get subjects that with submitted mark in the report
		$sql = "SELECT DISTINCT
					a.SubjectID
				FROM
					RC_REPORT_TEMPLATE_SUBJECT as a
					LEFT JOIN ASSESSMENT_SUBJECT as b ON a.SubjectID = b.RecordID
					LEFT JOIN RC_REPORT_TEMPLATE as c ON a.ReportID = c.ReportID
					LEFT JOIN RC_REPORT_TEMPLATE_FORM as d ON c.ReportID = d.ReportID
					LEFT JOIN INTRANET_CLASS as e ON d.ClassLevelID = e.ClassLevelID
					LEFT JOIN RC_MARKSHEET_SCORE as f ON a.SubjectID = f.SubjectID
				WHERE
					(b.CMP_CODEID IS NULL Or b.CMP_CODEID = '')
					And b.RecordStatus = 1
					AND c.RecordStatus = 1
					AND e.ClassID = '".$ParClassID."'
					AND f.StudentID = '".$this->uid."'
					AND f.Year = '".$this->Year."'
					AND f.Semester = '".$this->Semester."'
				ORDER BY
					a.DisplayOrder
				";
		$SubjectIDArray = $this->returnVector($sql);
		$TotalSubjectCount = sizeof($SubjectIDArray);

		if(!empty($SubjectIDArray))
		{
			# get number of confirmation / appealing
			$SubjectIDList = implode(",", $SubjectIDArray);

			$sql = "SELECT
						COUNT(a.FeedbackID)
					FROM
						RC_MARKSHEET_FEEDBACK as a
					WHERE
						a.Year = '".$this->Year."'
						AND a.Semester = '".$this->Semester."'
						AND a.StudentID = '".$this->uid."'
						AND a.SubjectID IN ('{$SubjectIDList}')
					";
			$row = $this->returnVector($sql);

			# calc number of subjects that required to confirm
			$ReturnValue = $TotalSubjectCount - $row[0];
		}
		else
			$ReturnValue = 0;

		return $ReturnValue;
	}

	/*
	* Get WebSAMSRegNo of students by returning a array
	*/
	function GET_STUDENT_REGNO_ARRAY($LevelList, $ClassID="", $TargetStudentList="")
	{
		if($TargetStudentList!="") {
			$sql = "SELECT UserID, REPLACE(WebSAMSRegNo, '#', '')  FROM INTRANET_USER WHERE UserID IN ($TargetStudentList) AND WebSAMSRegNo IS NOT NULL AND WebSAMSRegNo <> ''";
		}
		else if($ClassID!="") {
			$sql = "SELECT 
							a.UserID, REPLACE(a.WebSAMSRegNo, '#', '') 
					FROM 
							INTRANET_USER as a 
							LEFT JOIN YEAR_CLASS_USER as b ON a.UserID = b.UserID 
					WHERE 
							b.YearClassID = '{$ClassID}' AND a.WebSAMSRegNo IS NOT NULL AND a.WebSAMSRegNo <> ''
					";
		}
		else {
			$sql = "SELECT 
							a.UserID, REPLACE(a.WebSAMSRegNo, '#', '') 
					FROM 
							INTRANET_USER as a 
							LEFT JOIN YEAR_CLASS_USER as b ON a.UserID = b.UserID 
							LEFT JOIN YEAR_CLASS as yc ON b.YearClassID = yc.YearClassID
					WHERE 
							yc.YearID IN ({$LevelList}) And yc.AcademicYearID = '".$this->Year."' AND a.WebSAMSRegNo IS NOT NULL AND a.WebSAMSRegNo <> ''
					";
		}
		$row = $this->returnArray($sql);

		for($i=0; $i<sizeof($row); $i++)
		{
			list($user_id, $reg_no) = $row[$i];
			$ReturnArray[trim($reg_no)] = trim($user_id);
		}

		return $ReturnArray;
	}


	function CHECK_ENROL($SubjectDataIn){

		if (sizeof($SubjectDataIn)>0 && is_array($SubjectDataIn))
		{
			foreach($SubjectDataIn as $i => $SubjectData)
			{
				if (!($SubjectData["Grade"]=="/" &&
					(($SubjectData["RawMark"]==0 && isset($SubjectData["RawMark"])) ||
					($SubjectData["Mark"]==0 && isset($SubjectData["Mark"])))))
				{
					return true;
				}
			}
		}

		return false;
	}

	function UPDATE_CELL_SETTING($ParSubjectID, $ParScale){

		# Step 1: get the list of ReportSubjectID
		$Sql = "
					SELECT
							RC_REPORT_TEMPLATE_SUBJECT.ReportSubjectID
					FROM
							RC_SUBJECT_GRADING_SCHEME
					LEFT JOIN
							RC_REPORT_TEMPLATE_SUBJECT
						ON
							RC_SUBJECT_GRADING_SCHEME.SubjectID = RC_REPORT_TEMPLATE_SUBJECT.SubjectID
					WHERE
							RC_SUBJECT_GRADING_SCHEME.SubjectID = '$ParSubjectID'
				";
		$ReportSubjectIDArr = $this->returnVector($Sql);
		$ReportSubjectIDList = implode("," , $ReportSubjectIDArr);

		# Step 2: update the cell setting with previous ReportSubjectID(s)
		if (trim($ReportSubjectIDList, ", ") != "")
		{
			$Sql = "
						UPDATE
								RC_REPORT_TEMPLATE_CELL.Setting
						SET
								Setting = '$ParScale'
						WHERE
								RC_REPORT_TEMPLATE_CELL.ReportSubjectID IN ($ReportSubjectIDList)
					";
			$this->db_db_query($Sql);
		}
	}


	function GET_YEAR(){
		$sql = "SELECT ay.AcademicYearID as Year, ay.YearNameEN as YearName 
				FROM
					RC_MARKSHEET_COLLECTION as mc
					Inner Join
					ACADEMIC_YEAR as ay On (mc.Year = ay.AcademicYearID)
				Group By
					ay.AcademicYearID
				Order By
					ay.Sequence
				";
		$ReturnArray = $this->returnArray($sql);

		return $ReturnArray;
	}

	// get Standard Deviation
	function getStandardDeviation($ParNumberArray) {

		$i = 0;
		while($i < count($ParNumberArray)){
			if(!is_numeric($ParNumberArray[$i]))
				return -1;
			$i++;
		}

		$DummyTotal = 0;
		foreach($ParNumberArray as $value)
			$DummyTotal += $value;

		$Mean = $DummyTotal / count($ParNumberArray);

		$DummyTotal = 0;
		foreach($ParNumberArray as $value)
			$DummyTotal += pow(($value-$Mean), 2);

		$ReturnVal = sqrt($DummyTotal / count($ParNumberArray));

		return $ReturnVal;
	}
	
	function Get_Term_Selection($ID_Name, $AcademicYearID='', $SelectedYearTermID='', $OnChange='', $NoFirst=0, $WithWholeYear=0)
	{
		global $Lang, $eReportCard, $PATH_WRT_ROOT;
		
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$fcm = new form_class_manage();
		$CurrentYearTermArr = getCurrentAcademicYearAndYearTerm();
		
		# Set to currect Academic Year if there is no target Academic Year
		if ($AcademicYearID == '')
			$AcademicYearID = $CurrentYearTermArr['AcademicYearID'];
		
		# Set to currect Semester if there is no target semester
		if ($SelectedYearTermID == '')
			$SelectedYearTermID = $CurrentYearTermArr['YearTermID'];
			
		$YearTermArr = $fcm->Get_Academic_Year_Term_List($AcademicYearID);
		$numOfYearTerm = count($YearTermArr);
		
		$selectArr = array();
		
		if ($WithWholeYear==1)
			$selectArr[0] = $eReportCard['FullYear'];
		for ($i=0; $i<$numOfYearTerm; $i++)
		{
			$thisYearTermID = $YearTermArr[$i]['YearTermID'];
			$thisYearTermName = Get_Lang_Selection($YearTermArr[$i]['YearTermNameB5'],$YearTermArr[$i]['YearTermNameEN']);
			
			$selectArr[$thisYearTermID] = $thisYearTermName;
		}
		
		$onchange = '';
		if ($OnChange != "")
			$onchange = 'onchange="'.$OnChange.'"';
				
		$selectionTags = ' id="'.$ID_Name.'" name="'.$ID_Name.'" '.$onchange;
		$firstTitle = Get_Selection_First_Title($Lang['SysMgr']['FormClassMapping']['Select']['Term']);
		
		$semesterSelection = getSelectByAssoArray($selectArr, $selectionTags, $SelectedYearTermID, $IsAll=0, $NoFirst, $firstTitle);
		
		return $semesterSelection;
	}
	
	function Get_Semester_By_SemesterName($ParSemester)
	{
		//global $libreportcard, $SemesterArray;
	
		$sem_number = "-1";
		if(trim($ParSemester)=="FULL")
		{
			$sem_number = 0;
		} 
		else
		{
			/*
			for($i=0; $i<sizeof($SemesterArray); $i++)
			{
				if(trim($ParSemester)==trim($SemesterArray[$i]))
				{
					$sem_number = $i+1;
					break;
				}
			}
			*/
			
			$sql = "Select
							YearTermID
					From
							ACADEMIC_YEAR_TERM
					Where
							AcademicYearID = '".$this->Year."'
							And
							(
								YearTermNameEN Like '%".$this->Get_Safe_Sql_Like_Query($ParSemester)."%'
								Or
								YearTermNameB5 Like '%".$this->Get_Safe_Sql_Like_Query($ParSemester)."%'
							)
					";
			$ResultArr = $this->returnArray($sql);
			
			if (count($ResultArr) > 0)
				$sem_number = $ResultArr[0]['YearTermID'];
			
		}
		return $sem_number;
	}
}

?>
