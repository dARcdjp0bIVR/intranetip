<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");

intranet_auth();
intranet_opendb();

echo "<META http-equiv=Content-Type content='text/html; charset=utf-8'>";

$no_file = 5;
$li = new libfilesystem();

$path = $file_path.$folder;
$path = $path."tmp";
$li->folder_new($path);

# remove user deleted files

$lremove = new libfiletable("", $path, 0, 0, "");
$files = $lremove->files;
$attachStr=stripslashes($attachStr);
while (list($key, $value) = each($files)) {
     if(!strstr($attachStr,$files[$key][0])){
          $li->file_remove($path."/".$files[$key][0]);
     }
}

for($i=0;$i<$no_file;$i++){
     $loc = ${"userfile".$i};
     $file = stripslashes(${"hidden_userfile_name$i"});     
     $des = "$path/$file";
     if($loc=="none"){
     } else {
          if(strpos($file, ".")==0){
          }else{
               $li->lfs_copy($loc, $des);
          }
     }
}

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
?>

<html>
<head></head>
<body>
<script language="JavaScript1.2">
par = opener.window;
obj = opener.window.document.<?=$FormName?>.elements["<?=$SelectionID?>"];

par.checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "par.checkOptionAdd(obj, \"".$files[$key][0]." (".convert_size($files[$key][1]).")\", \"".$files[$key][0]."\");\n"; ?>
par.checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");

self.close();
</script>
</body>
</html>