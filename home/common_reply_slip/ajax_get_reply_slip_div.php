<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/replySlip/libReplySlipMgr.php');

if(strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=8OaOAb4COQCRJNyp2jeWSB4kDyYanKf')===false){
	intranet_auth();
}
intranet_opendb();

$replySlipId = $_POST['ReplySlipID'];
$replySlipUserId = $_POST['ReplySlipUserID'];
$showUserAnswer = $_POST['ShowUserAnswer'];
$disabledAnswer = $_POST['DisableAnswer'];

if ($replySlipUserId === '') {
	$replySlipUserId = $_SESSION['UserID'];
}

$libReplySlipMgr = new libReplySlipMgr();
$libReplySlipMgr->setCurUserId($replySlipUserId);
$libReplySlipMgr->setReplySlipId($replySlipId);
echo $libReplySlipMgr->returnReplySlipHtml($forCsvPreview=false, $showUserAnswer, $disabledAnswer);

intranet_closedb();
?>