<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/replySlip/libReplySlipMgr.php');

if(strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=8OaOAb4COQCRJNyp2jeWSB4kDyYanKf')===false){
	intranet_auth();
}
intranet_opendb();

$replySlipId = $_POST['ReplySlipID'];
$defaultShowDiv = $_POST['DefaultShowDiv'];


$libReplySlipMgr = new libReplySlipMgr();
$libReplySlipMgr->setReplySlipId($replySlipId);
echo $libReplySlipMgr->returnReplySlipStatisticsHtml($defaultShowDiv);

intranet_closedb();
?>