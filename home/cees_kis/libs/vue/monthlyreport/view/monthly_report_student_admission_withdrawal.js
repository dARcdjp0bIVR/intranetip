Vue.component('student-admission-withdrawal', {
    // language=HTML
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
	    <col style="width: 3%">\
        <col style="width: 10%">\
        <col style="width: 15%">\
        <col style="width: 13%">\
        <col style="width: 10%">\
        <col style="width: 20%">\
        <col style="width: 15%">\
        <col style="width: 10%">\
    </colgroup>\
    <thead>\
    <tr>\
	    <th style="text-align: left;">#</th>\
	    <th>'+jsLang.Particulars+'</th>\
        <th>'+jsLang.Date+'</th>\
        <th>'+jsLang.StudentName+'</th>\
        <th>'+jsLang.Class+'</th>\
        <th>'+jsLang.AdmissionDropOutReason+'</th>\
        <th>'+jsLang.WithdrawalProspect+'</th>\
        <th>'+jsLang.Remark+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
    			 {{content.particular | getParticular(AdmissionType)}}\
            </td>\
            <td style="text-align: left;">\
    			 {{ content.date}}\
            </td>\
            <td style="text-align: left;">\
                {{ content.studentName}}\
            </td>\
            <td style="text-align: left;">\
                {{content.class}}\
            </td>\
            <td style="text-align: left;">\
                {{content.reason}}\
            </td>\
            <td style="text-align: left;">\
                {{content.prospect}}\
            </td>\
            <td style="text-align: left;">\
                {{content.remark}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            AdmissionType: this.$root.AdmissionType,
            positions: this.$root.positions,
            contents: formData.StudentAdmissionWithdrawal.slice(),
            teachers: this.$root.teachers,
            sickleaveday: this.$root.sickleaveday,
            withCertifications: this.$root.withCertifications
        }
    },
    filters: {
        getParticular: function (particularType,AdmissionType) {
            return AdmissionType[particularType];
        },
        getPositionName: function (positionid, positions) {
            return positions[positionid];
        },
        getWithCertifications: function(withCer, withCertifications){
            return withCertifications[withCer];
            // return 0;
        }
        // getTeacherName: function (teacherid, teachers) {
        //     return teachers[teacherid];
        // }
        // getPositionName: function(positionId, positions){
        //     return positions[positionId];
        // }
    }
});