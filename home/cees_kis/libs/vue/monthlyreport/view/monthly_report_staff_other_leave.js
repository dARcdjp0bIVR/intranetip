Vue.component('teacher-other-leave', {
    // language=HTML
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
        <col style="width: 3%">\
        <col style="width: 20%">\
        <col style="width: 20%">\
        <col style="width: 10%">\
        <col style="width: 10%">\
        <col style="width: 13%">\
        <col style="width: 10%">\
        <col style="width: 10%">\
    </colgroup>\
    <thead>\
    <tr>\
	    <th style="text-align: left;">#</th>\
	    <th>'+jsLang.StaffName +'</th>\
        <th>'+jsLang.Position +'</th>\
        <th>'+jsLang.Date +'</th>\
        <th>'+jsLang.NatureOfAbsence +'</th>\
        <th>'+jsLang.SupportingCertificate +'</th>\
        <th>'+jsLang.Absent +'</th>\
        <th>'+jsLang.AbsentSeptember +'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">{{ key + 1 }}</td>\
            <td style="text-align: left;">\
    			 {{content.teacherName}}\
            </td>\
            <td style="text-align: left;">\
    			 {{ content.positionId | getPositionName(positions)}}\
            </td>\
            <td style="text-align: left;white-space: pre-line;">\
    			 {{content.date | getDisplayDates()}}\
            </td>\
            <td style="text-align: left;">\
    			 {{content.nature}}\
            </td>\
            <td style="text-align: left;">\
                {{ content.WithCer | getWithCertifications(withCertifications)}}\
            </td>\
            <td style="text-align: left;">\
                {{content.thisMonthDay}}\
            </td>\
            <td>\
               <div v-if="content.lastMonthDay == \'--\'"  >\
                 {{content.thisMonthDay}}\
                </div>\
                <div v-else v-bind="countOtherLeaveDate(content.teacherName,key)">\
                    {{Number(content.thisMonthDay) + Number(content.lastMonthDay)}}\
                </div>\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            positions: this.$root.positions,
            contents: formData.TeacherOtherLeave.slice(),
            teachers: this.$root.teachers,
            sickleaveday: this.$root.sickleaveday,
            withCertifications: this.$root.withCertifications
        }
    },
    methods: {
        countOtherLeaveDate: function (teachername,id){
            if(teachername != '') { // avoid empty name
                var self = this;
                var reportID = document.getElementById("reportID").value;

                $.ajax({
                    type: 'POST',
                    url: '/home/cees_kis/monthlyreport/ajax',
                    data: {
                        reportId: reportID,
                        Method: 'getOtherLeaveData',
                        TeacherName: teachername
                    },
                    //  dataType: 'json',
                    success: function (data) {

                        if (data == '--') {
                            sum = parseInt(0);
                        } else {
                            sum = Number(data)
                        }

                        //            self.contents[id].lastMonthDay = sum;

                    }
                }).done(function () {
                    self.contents[id].lastMonthDay = sum;

                });
            }
        }
    },
    filters: {
        getPositionName: function (positionid, positions) {
            return positions[positionid];
        },
        getWithCertifications: function(withCer, withCertifications){
            return withCertifications[withCer];
            // return 0;
        },
        getDisplayDates: function(dates){
            let dateAry = dates.split(',');
            let displayAry = [];
            let dayMs = 86400000;
            let start = '';
            let last = '';
            dateAry.sort((a, b) => {
                if(a==b) return 0;
                if(a>b) return 1;
                return -1;
            }).forEach((val) => {
                if(start == '') {
                    start = val;
                    last = val;
                } else {
                    if(last == '') last = start;
                    if(getTimeDiff(last, val) == dayMs){
                        last = val;
                    } else {
                        if(start == last)
                            displayAry.push(last);
                        else
                            displayAry.push(start + ' ' + jsLang.To + ' ' + last);
                        start = val;
                        last = val;
                    }
                }
            });
            if(start == last)
                displayAry.push(last);
            else
                displayAry.push(start + ' ' + jsLang.To + ' ' + last);
            return displayAry.join(',\r\n');
        }
        // getTeacherName: function (teacherid, teachers) {
        //     return teachers[teacherid];
        // }
        // getPositionName: function(positionId, positions){
        //     return positions[positionId];
        // }
    }
});