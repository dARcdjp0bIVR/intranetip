Vue.component('enrol-vacancy', {
    // language=HTML
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
	    <col style="width: 3%">\
                <col style="width: 20%">\
                <col style="width: 20%">\
                <col style="width: 20%">\
                <col style="width: 19%">\
                <col style="width: 18%">\
    </colgroup>\
    <thead>\
    <tr>\
	    <th style="text-align: left;"></th>\
	    <th>'+jsLang.NoOfClasses + '(' + jsLang.WholeDayAndSpecial + ')</th>\
        <th>'+jsLang.NoOfClasses + '(' + jsLang.HalfDayAndNormal + ')</th>\
        <th>'+jsLang.NoOfClassroom+'</th>\
        <th>'+jsLang.ApprovedEnrolmentOfThisYear+'</th>\
        <th>'+jsLang.EnrolmentOfCorrespondMonth+'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr >\
            <td style="text-align: left;"></td>\
            <td style="text-align: left;">\
    			 {{content.wholeDay}}\
            </td>\
            <td style="text-align: left;">\
    			 {{ content.halfDay}}\
            </td>\
            <td style="text-align: left;">\
    			 {{content.classroom}}\
            </td>\
            <td style="text-align: left;">\
                {{ content.approved}}\
            </td>\
            <td style="text-align: left;">\
                {{content.enrolment}}\
            </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            year: currYear,
            month: currMonth,
            content: formData.EnrolVacancy[0] ? formData.EnrolVacancy[0] : {
                wholeDay: '0',
                halfDay: '0',
                classroom: '0',
                approved: '0',
                enrolment: '0'
            }
        }
    },
    computed: {
        yearDisplay: function(){
            return (parseInt(this.year) - 1) + "/" + this.year;
        },
        correspondMonth: function(){
            date = new Date(this.year+'-'+this.month);
            date.setDate(0);
            return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
        }
    },
    filters: {
        getPositionName: function (positionid, positions) {
            return positions[positionid];
        },
        getWithCertifications: function(withCer, withCertifications){
            return withCertifications[withCer];
            // return 0;
        }
        // getTeacherName: function (teacherid, teachers) {
        //     return teachers[teacherid];
        // }
        // getPositionName: function(positionId, positions){
        //     return positions[positionId];
        // }
    }
});