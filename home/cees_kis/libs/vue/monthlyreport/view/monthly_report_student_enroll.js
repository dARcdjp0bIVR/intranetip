Vue.component('student_enrolment', {
    template: '\
    <table class="table report-style" id="tbl_other" width="100%">\
    <colgroup>\
	    <col style="width: 10%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
	    <col style="width: 15%">\
    	<col style="width: 15%">\
    </colgroup>\
    <thead>\
    <tr>\
	    <th style="text-align: left;">'+ jsLang.Level +'</th>\
	    <th style="text-align: left;">'+jsLang.Approved +'</th>\
	    <th style="text-align: left;">'+jsLang.Operation +'</th>\
	    <th style="text-align: left;">'+jsLang.Places +'</th>\
	    <th style="text-align: left;">'+jsLang.SeptemberNo +'</th>\
    	<th style="text-align: left;">'+jsLang.thisMonthNo + '</th>\
    	<th style="text-align: left;">'+jsLang.AverageRate +'</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td style="text-align: left;">\
    			{{content.level}}\
            </td>\
            <td style="text-align: left;">\
    			{{content.approvedNo}}\
            </td>\
            <td style="text-align: left;">\
    			{{content.operationNo}}\
            </td>\
	        <td style="text-align: left;">\
    			{{content.placeApprovedNo}}\
	    	</td>\
            <td style="text-align: left;">\
    			{{content.lastMonthEnrolled}}\
    		</td>\
	    	<td style="text-align: left;">\
    			{{content.thisMonthEnrolled}}\
            </td>\
		    <td style="text-align: left;">\
    	 		{{content.averageAttendance}}\
		    </td>\
        </tr>\
    </tbody>\
    </table>\
    ',
    data: function () {
        return {
            contents: formData.StudentEnrolment.slice()
        }
    }
});