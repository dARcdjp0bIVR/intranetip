var formData = $.parseJSON(formDataJson);
var jsLang = $.parseJSON(jsLangJson);
var getTimeDiff =  function(dateA, dateB){
    let a = new Date(dateA);
    let b = new Date(dateB);
    let timeDiff = a.getTime() - b.getTime();
    return (timeDiff > 0) ? timeDiff : -timeDiff;
}