Vue.component('student-admission-withdrawal', {
    template: '\
    <div>\
            <table class="table report-style" id="tbl_student_admission_withdrawal">\
            <colgroup>\
            <col style="width: 3%">\
            <col style="width: 10%">\
            <col style="width: 15%">\
            <col style="width: 13%">\
            <col style="width: 10%">\
            <col style="width: 20%">\
            <col style="width: 15%">\
            <col style="width: 10%">\
            <col style="width: 4%">\
            </colgroup>\
            <thead>\
            <tr>\
                <th colspan="9">\
                    <div class="import-menu" >\
                        <ul>\
                            <li><a href="#" data-toggle="modal" data-target="#studentAdmissionWithdrawalRetrieveModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromStudentAttendance+'</a></li>\
                            <li><a href="#" data-toggle="modal" data-target="#studentAdmissionWithdrawalModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                        </ul>\
                    </div>\
                </th>\
            </tr>\
            <tr>\
            <th>#</th>\
            <th>'+jsLang.Particulars+'</th>\
            <th>'+jsLang.Date+'</th>\
            <th>'+jsLang.StudentName+'</th>\
            <th>'+jsLang.Class+'</th>\
            <th>'+jsLang.AdmissionDropOutReason+'</th>\
            <th>'+jsLang.WithdrawalProspect+'</th>\
            <th>'+jsLang.Remark+'</th>\
            <th>&nbsp;</th>\
            </tr>\
            </thead>\
            <tbody>\
                <tr v-for="(content, key) in contents" >\
                    <td>{{ key + 1 }}</td>\
                    <td>\
                    <select v-model="content.particular" :id="\'_withcerselect_\' + key" class="selectpicker" data-live-search="true">\
                        <option disabled>'+jsLang.Select+'</option>\
                        <option v-for="(text,id) in AdmissionType" :value="id">{{ text }}</option>\
                    </select>\
                    <!--<input type="text" class="form-control" id="\'inputParticular\' + key" v-model="content.particular">-->\
                    </td>\
                    <td>\
                        <div is="multiple-date-select" v-bind:id="key" v-bind:type="\'admission_withdrawal\'" v-bind:sourceKey="\'date\'">\
                        </div>\
                    </td>\
                    <td>\
                    <input type="text" class="form-control" id="\'inputStudentName\' + key" v-model="content.studentName">\
                    </td>\
                    <td>\
                    <input type="text" class="form-control" id="\'inputClass\' + key" v-model="content.class">\
                    </td>\
                    <td>\
                    <input type="text" class="form-control" id="\'inputReason\' + key" v-model="content.reason">\
                    </td>\
                    <td>\
                    <input type="text" class="form-control" id="\'inputProspect\' + key" v-model="content.prospect">\
                    </td>\
                    <td>\
                    <input type="text" class="form-control" id="\'inputRemark\' + key" v-model="content.remark">\
                    </td>\
                    <td>\
                    <div is="del-btn" v-bind:remove="remove" v-bind:target="key">\
                    </div>\
                    </td>\
                </tr>\
                <tr is="add-row" v-bind:add="add"></tr>\
            </tbody>\
            </table>\
        </div>\
     </div>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.StudentAdmissionWithdrawal.slice(),
            AdmissionType: this.$root.AdmissionType,
            row: {
                particular: '',
                date: '',
                studentName: '',
                class: '',
                reason: '',
                prospect: '',
                remark: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        addrow: function(data){
            this.contents.push(data);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.StudentAdmissionWithdrawal = this.contents;
        },
        confirmData: function(){
            self = this;
            this.confirmDataNow(this, 'studentAdmissionWithdrawal', {
                before: function(data){
                    data[0] = self.AdmissionType[data[0]];
                    return data;
                },
                addRow: function(data){
                    return {
                        particular: data[0],
                        date: data[1],
                        studentName: data[2],
                        class: data[3],
                        reason: data[4],
                        prospect: data[5],
                        remark: data[6]
                    };
                }
            });
        },
        confirmData2: function () {
            var dbData;
            var self = this;
            var month = document.getElementById("reportMonth").value;
            var year = document.getElementById("reportYear").value;
            $.ajax({
                url: "/home/cees_kis/api",
                type:"GET",
                datatype: 'json',
                data: {
                    Method: 'GetStudentAdmissionWithdrawalMonthlyData',
                    Year: year,
                    Month: month,
                    DisplayLang: pageLang
                },
                success: function (data) {
                    dbData = data;
                }
            })
                .done(function() {
                    _.forEach(dbData, function (row, key) {
                        this.importdata = {
                            particular: row.Type,
                            studentName: row.Name,
                            class: row.ClassName,
                            date: row.Date
                        };
                        self.addrow(this.importdata);
                        self.$nextTick(function () {
                            $('.selectpicker').selectpicker();
                        });
                    });
                });
        },

    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-studentAdmissionWithdrawalRetrieveModal', this.confirmData2);
        eventHub.$on('confirm-info-studentAdmissionWithdrawalModal', this.confirmData);
    }
});

Vue.component('studentAdmissionWithdrawalRetrieveModal', {
    template: '\
     <div class="modal-body">\
        <div class="browse-folder-container">\
            <ul><li>'+jsLang.Month + '/ '+jsLang.Year+': {{month}}/{{year}}</li></ul>\
            <table class="table report-style" >\
                <colgroup>\
                    <col style="width: 32%;">\
                    <col style="width: 68%;">\
                </colgroup>\
                <tbody>\
                    <tr>\
                     <td>'+jsLang.AdmissionCount+'</td>\
                     <td>{{AdmissionCount}}</td>\
                   </tr>\
                   <tr>\
                    <td>'+jsLang.DropOutCount+'</td>\
                    <td>{{DropOutCount}}</td>\
                   </tr>\
               </tbody>\
            </table>\
            <p class="question text-center">'+jsLang.EnsureImport+'</p>\
         </div>\
      </div>\
        ',
    data: function () {

        return {
            dataContents:
                [{
                    Name: '',
                    Date: '',
                    Type: ''
                }]
        }
    },
    computed: {
        recordNumber: function () {
            return _.size(this.dataContents);
        },
        AdmissionCount: function(){
            count = 0;
            _.forEach(this.dataContents, function (value, key) {
                if(value['Type']=='Admission') count++;
            });
            return count;
        },
        DropOutCount: function(){
            count = 0;
            _.forEach(this.dataContents, function (value, key) {
                if(value['Type']=='Withdrawal') count++;
            });
            return count;
        },
        StudentName: function(){
            studentName = [];
            _.forEach(this.dataContents, function (value, key) {
                studName = value.Name;
                if(studentName.indexOf(studName)=='-1') studentName.push(studName);
            });
            if(studentName == 0){
                return '---';
            }else {
                return studentName.sort().join(",");
            }
        },
        month: function(){
            return document.getElementById("reportMonth").value;
        },
        year: function(){
            return document.getElementById("reportYear").value;
        }
    },
    mounted: function () {
        var dbData;
        var self = this;

        $.ajax({
            url: "/home/cees_kis/api",
            type:"GET",
            datatype: 'json',
            data: {
                Method: 'GetStudentAdmissionWithdrawalMonthlyData',
                Year: this.year,
                Month: this.month,
                DisplayLang: pageLang
            },
            success: function (data) {
                // console.log(data);
                dbData = data;
            }
        })
            .done(function() {
                self.dataContents = dbData;
            });
    }

});

Vue.component('studentAdmissionWithdrawalModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Particulars,
                jsLang.Date,
                jsLang.StudentName,
                jsLang.Class,
                jsLang.AdmissionDropOutReason,
                jsLang.WithdrawalProspect,
                jsLang.Remark
            ],
            popup:[
                {title: jsLang.Particulars, content: this.$root.AdmissionType},
                null,
                null,
                null,
                null,
                null,
                null
            ],
            reminder:[
                null,
                "YYYY-MM-DD or DD/MM/YYYY",
                null,
                null,
                null,
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/studentAdmissionWithdrawal",
            fileFieldId: "studentAdmissionWithdrawalModalFile"
        }
    },
    mounted: function(){
    }
});