Vue.component('others', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 93%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="2">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#othersReportModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
    <th>'+jsLang.Content+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
            <input type="text" class="form-control" id="\'inputOthersDes1\' + key" v-model="content.text">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.Others.slice(),
            row: {
                text: ''
            }
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            submitForm.Others = this.contents;
        },
        confirmData: function() {
            this.confirmDataNow(this, 'othersReport', {
                before: function (data) {
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    return data;
                },
                addRow: function (data) {
                    return {
                        text: data[0]
                    };
                }
            });
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);

        eventHub.$on('confirm-info-othersReportModal', this.confirmData);
    }
});

Vue.component('othersReportModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}}: <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.Content,
            ],
            popup:[
                null
            ],
            reminder:[
                null,
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/othersReport",
            fileFieldId: "othersReportFile"
        }
    },
    mounted: function(){
    }
});