Vue.component('teacher-course', {
    template: '\
    <table class="table report-style" id="tbl_other">\
    <colgroup>\
    <col style="width: 3%">\
    <col style="width: 35%">\
    <col style="width: 15%">\
    <col style="width: 15%">\
    <col style="width: 13%">\
    <col style="width: 15%">\
    <col style="width: 4%">\
    </colgroup>\
    <thead>\
    <tr>\
        <th colspan="7">\
            <div class="import-menu" >\
                <ul>\
                    <li><a href="#" data-toggle="modal" data-target="#teacherCourseModal"><i class="form-icon retrieve"></i>'+jsLang.RetrieveFromCSV+'</a></li>\
                </ul>\
            </div>\
        </th>\
    </tr>\
    <tr>\
    <th>#</th>\
        <th>'+jsLang.PeriodOfStudy+'</th>\
        <th>'+jsLang.StaffName+'</th>\
        <th>'+jsLang.Position+'</th>\
        <th>'+jsLang.CourseName+'</th>\
        <th>'+jsLang.OrganizedBy+'</th>\
    <th>&nbsp;</th>\
    </tr>\
    </thead>\
    <tbody>\
        <tr v-for="(content, key) in contents" >\
            <td>{{ key + 1 }}</td>\
            <td>\
            <div is="date-input" v-bind:id="key" v-bind:type="\'teacher_course_from\'" v-bind:sourceKey="\'date\'" style="display:inline-table;width:45%;"> \
            </div>\
            <div style="vertical-align: top;padding-top: 10px;display: inline-block;">'+jsLang.To+'</div>\
            <div is="date-input" v-bind:id="key" v-bind:type="\'teacher_course_to\'" v-bind:sourceKey="\'dateTo\'" style="display:inline-table;width:45%;"> \
            </div>\
            </td>\
            <td>\
                <div is="teacher-select" v-bind:id="key" v-bind:type="\'teacher_course\'">\
        		</div>\
            <!--<div is="teacher-multi-select" v-bind:id="key" v-bind:type="\'teacher_course\'">\-->\
            </div>\
            </td>\
            <td>\
                <select v-model="content.positionId" :id="\'_positionselect_\' + key" class="selectpicker" data-live-search="true">\
                    <option disabled>'+jsLang.Select+'</option>\
                    <option v-for="(position,id) in positions" :value="id">{{ position }}</option>\
                </select>\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputCourseName\'+ key" v-model="content.course">\
            </td>\
            <td>\
            <input type="text" class="form-control" id="\'inputOrganizer\' + key   " v-model="content.organization">\
            </td>\
            <td>\
            <div is="del-btn" v-bind:remove="remove" v-bind:target="key" v-bind:location="contents">\
            </div>\
            </td>\
        </tr>\
        <tr is="add-row" v-bind:add="add"></tr>\
    </tbody>\
    </table>\
    ',
    mixins: [myMixin],
    data: function () {
        return {
            contents: formData.TeacherCourse.slice(),
            row: {
                date: '',
                dateTo:'',
                teacherId: '',
                positionId: '',
                course: '',
                organization: ''
            },
            teachers: this.$root.teachers,
            teacherArray: this.$root.teacherArray,
            positions: this.$root.positions
        }
    },
    methods: {
        add: function (e) {
            // this.addNewRows(e.target.value, this.row, this.contents);
        },
        remove: function (key) {
            this.removeRow(key, this.contents)
        },
        assign: function () {
            //  console.log('teacher-course is assigning');
            submitForm.TeacherCourse = this.contents;
        },
        confirmData: function() {
            var self = this;
            this.confirmDataNow(this, 'teacherCourse', {
                before: function (data) {
                    data[0] = (data[0] != "") ? data[0] : "Error";
                    // data[1] = (data[1] != "") ? data[1] : "Error";
                    data[2] = self.teacherArray[data[2]];
                    if(data[2] === undefined){
                        data[2] = "Error";
                    }
                    data[3] = (data[3] != "") ? data[3] : "Error";
                    data[4] = (data[3] != "") ? data[4] : "Error";
                    data[3] = self.$root.positions[data[3]];
                    return data;
                },
                addRow: function (data) {
                    return {
                        date: data[0],
                        dateTo: data[1],
                        teacherId: data[2],
                        positionId: data[3],
                        course: data[4],
                        organization: data[5]
                    };
                }
            });
        }
    },
    mounted: function () {
        eventHub.$on('collect-info', this.assign);
        eventHub.$on('confirm-info-teacherCourseModal', this.confirmData);
        // eventHub.$on('assign-startdate-teacher_course_from', this.autoDateTo);
    }
});

Vue.component('teacherCourseModal', {
    template: '\
   <div class="modal-body">\
       <div class="browse-folder-container">\
        <ul>\
            <li>{{UploadFile}} : <input :id="fileFieldId" type="file" value="Select File" name="fileImport" /></span></li>\
            <li>{{TemplateFile}} : <a :href="filetemp">'+jsLang.Download+'</a></li>\
            <li>\
                <span class="importInstruction">' + jsLang.ImportInstruction +'</span>\
                <importStructure\
                 :columns="columns" :pContent="popup" :reminder="reminder"\
                ></importStructure>\
            </li>\
           </ul>\
          </div>\
      </div>',
    data: function(){
        return {
            UploadFile : jsLang.UploadFile,
            TemplateFile : jsLang.TemplateFile,
            columns:[
                jsLang.PeriodOfStudyStart,
                jsLang.PeriodOfStudyEnd,
                jsLang.StaffUserLogin,
                jsLang.Position,
                jsLang.CourseName,
                jsLang.OrganizedBy
            ],
            popup:[
                null,
                null,
                null,
                {title: jsLang.Position, content: this.$root.positions},
                null,
                null
            ],
            reminder:[
                "YYYY-MM-DD or DD/MM/YYYY",
                "YYYY-MM-DD or DD/MM/YYYY",
                null,
                null,
                null,
                null
            ],
            filetemp: "/home/cees_kis/monthlyreport/template/teacherCourse",
            fileFieldId: "teacherCourseFile"
        }
    },
    mounted: function(){
    }
});