<?php
global $config_school_code, $sys_custom;

$reportInfo = $_PAGE['View']['Data']['reportInfo'];
$SubmitStatus = $reportInfo['SubmitStatus'];

$displaySchName = isset($Lang['CEES']['School']['Name'][$config_school_code])? $Lang['CEES']['School']['Name'][$config_school_code] : $Lang['CEES']['School']['Name']['Default'];
$displaySchType = $Lang['CEES']['School']['Type']['KG'];
if($_PAGE['View']['Data']['schoolType'] == 'B') {
    $displaySchType .= ' ('.$Lang['CEES']['MonthlyReport']['JSLang']['BoardingSchool'].')';
}

if($sys_custom['SDAS']['CEES']['MonthlyReport']['VueEnv'] == 'dev'){
    $vue = 'vue.js';
}else{
    $vue = 'vue.min.js';
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-SCALE=1">
    <title>TWGHS Monthly Report</title>
    <script type="text/javascript" src="/home/cees/libs/js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="/home/cees/libs/js/moment.js"></script>
    <script type="text/javascript" src="/home/cees/libs/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap/css/bootstrap.min.css"/>
    <script type="text/javascript"
            src="/home/cees/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="/home/cees/libs/bootstrap-datetimepicker/bootstrap-datetimepicker.css"/>
    <script type="text/javascript" src="/home/cees/libs/bootstrap-select/js/bootstrap-select.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/bootstrap-select/css/bootstrap-select.min.css"/>
    <link rel="stylesheet" href="/home/cees/libs/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/home/cees/libs/css/form-TWGHS.css"/>
    <meta name="theme-color" content="#ffffff">
</head>
<body class="monthlyReport">
<script src="/home/cees/libs/js/<?echo $vue?>"></script>
<script src="/home/cees/libs/js/lodash.min.js"></script>
<div id="wrapper">
    <div id="main-body">
        <div class="content-wrapper">
            <div class="content-header">
                <span class="title"><?=$Lang['CEES']['School']['MonthlyReport']?> > <?=$Lang['CEES']['School']['FillIn']?></span>
            </div>
            <hr class="titleDivider"/>
            <div class="content-body">

                <div id="form-header">
                    <div class="form-group row formInfo">
                        <label class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['SchoolName']?></label>
                        <? echo $displaySchName ?>
                    </div>

                    <div class="form-group row formInfo">
                        <label class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['SchoolType']?></label>
                       <? echo $displaySchType ?>
                    </div>

                    <div class="form-group row formInfo">
                        <label class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['MonthYear']?></label>
                        <? echo str_pad($reportInfo['Month'], 2, '0', STR_PAD_LEFT) . '/' . $reportInfo['Year'] ?>
                    </div>

                    <div class="form-group row formInfo">
                        <label for="inputSupervisorName" class="col-sm-4 col-md-3 col-form-label"><?=$Lang['CEES']['SupervisorName']?></label>
                            <? echo $reportInfo['SupervisorName'] ?>
                    </div>
                </div>
                <div id="form-body">
                    <section-item
                            v-for="(section, index) in sections"
                            key="index"
                            class="section"
                            v-bind:id="index + 1"
                            v-bind:title="section.title"
                            v-bind:type="section.type"
                    ></section-item>
                    <div class="submission-area">
                        <button id="exportBtn" class="btn btn-blue" @click="exportasDoc">
                            <?=$Lang['CEES']['MonthlyReport']['ExportAsDoc']?>
                        </button>
                    <!-- Submission Buttons -->
                    <?php if ($SubmitStatus != 1 && ( $_SESSION['CEES']['MonthlyReportPIC'] || $_SESSION['CEES']['Principal'] )) { ?>
                            <button id="submitBtn" class="btn btn-blue" @click="submit">
                                <?=$Lang['CEES']['MonthlyReport']['SubmittoTW']?>
                            </button>
                            <button id="cancelBtn" class="btn btn-secondary btn-cancel" @click="modification">
                                <?=$Lang['CEES']['MonthlyReport']['RequestModification']?>
                            </button>
                    <? } ?>
                    </div>
                    <input type="hidden" id="reportID" value="<?= $reportInfo['ReportID'] ?>">
                    <input type="hidden" id="reportYear" value="<?= $reportInfo['Year'] ?>">
                    <input type="hidden" id="reportMonth" value="<?= $reportInfo['Month'] ?>">
                    <input type="hidden" id="reportSchoolType" value="<?= $reportInfo['SchoolType'] ?>">
                </div>

            </div>
        </div>
    </div>
</div>
</body>
</html>
<script>
    var formDataJson = "<?php echo addslashes($_PAGE['View']['Data']['reportDataJson'])?>";
    var TeacherListJson = "<?php echo addslashes($_PAGE['View']['Data']['TeacherListJson'])?>";
    var OLECategoryJson = "<?php echo addslashes($_PAGE['View']['Data']['OLECategoryJson'])?>";
    var jsLangJson = "<?php echo addslashes($_PAGE['View']['Data']['LangJson'])?>";
    var currYear = "<?php echo $reportInfo['Year']?>";
    var currMonth = "<?php echo $reportInfo['Month']?>";
</script>

<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_common.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_appointment_termination.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_substitute_teacher.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_staff_sick_leave.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_staff_other_leave.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_enrol_quota.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_student_admission_withdrawal.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_visit_inspection.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_teacher_course.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_student_eca.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_school_awards.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_advanced_notice.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_others.js"></script>
<script src="/home/cees_kis/libs/vue/monthlyreport/view/monthly_report_base.js"></script>
