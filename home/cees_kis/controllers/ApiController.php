<?php
/**
 * Created by PhpStorm.
 * User: annajyang
 * Date: 12/1/2018
 * Time: 11:28
 */

include_once("BaseController.php");

    class ApiController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->RequestMapping['/api'] = array('function' => 'index');

    }
    public function index()
    {
        $PATH_WRT_ROOT = "../../";
        include_once($PATH_WRT_ROOT."includes/global.php");
        include_once($PATH_WRT_ROOT."includes/libdb.php");
        include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
        include_once($PATH_WRT_ROOT."includes/cees/libcees_kis_api.php");
        include_once($PATH_WRT_ROOT."includes/json.php");

        $json_obj = new JSON_obj();
        $lapiAuth = new libeclassapiauth();
        $basicApiKey = $lapiAuth->GetAPIKeyByProject('CEES');

        $http = checkHttpsWebProtocol()?'https://':'http://';
        $url =  $http.$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80 && $_SERVER["SERVER_PORT"]!= 443?":".$_SERVER["SERVER_PORT"]:"").'/api/cees_kis.php';

        $postData = array();
        $postData['AccessTime'] = time();
        $postData['AccessCode'] = sha1($basicApiKey.'_'.$postData['AccessTime']);
        if(count($_GET)>0){
            foreach($_GET as $key => $val){
                $postData[$key] = $val;
            }
        }
        /*
        $postData['Method'] = 'GetStaffSickLeaveMonthlyData';
        $postData['Year'] = '2015';
        $postData['Month'] = '04';
        $postData['DisplayLang'] = "b5"; // en or b5 or gb
        */

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        $response = curl_exec($ch);
        curl_close($ch);

        header("Content-Type: application/json;charset=utf-8");
        echo $response;

    }
}

