<?php
// Editing by
// Modification Log
// Date:    2019-02-19 Philips
//          Added $_SESSION['CEES']['monthlyreportsectionpic'] to check access right of section pic
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT . "includes/libdb.php");
include_once("lang/$intranet_session_language.php");
include_once("libs/php/lib.php");
include_once($PATH_WRT_ROOT . "includes/cust/student_data_analysis_system_kis/libSDAS.php");

intranet_opendb();
$db = new libdb();
$objsdas = new libsdas();
// session register
if (!isset($_SESSION['CEES'])) {
    $accessright = $objsdas->getAssessmentStatReportAccessRight();
//    if ($objsdas->isSuperAdmin() && $accessright['admin'] == 1) {
//        $_SESSION['CEES']['SuperAdmin'] = 1;
//    } else
    if ($accessright['admin'] == 1) {
        $_SESSION['CEES']['Principal'] = 1;
    } else if($accessright['MonthlyReportPIC'] == 1){
        $_SESSION['CEES']['MonthlyReportPIC'] = 1;
    } else if($objsdas->isMonthlyReportPIC($_SESSION['UserID'])){
        $_SESSION['CEES']['monthlyreportsectionpic'] = $objsdas->getMonthlyReportSectionRight($_SESSION['UserID']);
    }
    //$_session['cees']['staff'] = 1;
}

// if still no session(access right) redirect
if (!isset($_SESSION['CEES'])) {
    /*header('HTTP/1.0 403 Forbidden');
    die();*/
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
} else {
    if($_SESSION['CEES']['monthlyreportsectionpic']){
        $_SESSION['CEES']['monthlyreportsectionpic'] = $objsdas->getMonthlyReportSectionRight($_SESSION['UserID']);
        //debug_pr($_SESSION['CEES']);die();
    }
}

$basepath = "/home/cees_kis";
$request_method = strtoupper($_SERVER['REQUEST_METHOD']);
$request_uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$request_uri = rtrim($request_uri, '/');
$request_uri = rtrim($request_uri, '#');
$request_uri = str_replace($basepath, '', $request_uri);

$path_params = explode("/", $request_uri);
array_shift($path_params);
$path_params_size = count($path_params);

if ($path_params[0] == '') {
    $path_params[0] = 'index';
}

$_PAGE = array(); // page scope variable to store all data, controller instance, etc...
$_PAGE['db'] = $db;
$_PAGE['REQUEST_METHOD'] = $request_method;
$_PAGE['REQUEST_URI'] = $request_uri;

// handle magic quote
array_walk_recursive($_POST, 'handleFormPost');
array_walk_recursive($_GET, 'handleFormPost');

if ($path_params[0] != '') {
    $controller_dir = '/controllers';

    $request_module = $path_params[0];
    for ($i = 1; $i < $path_params_size; $i++) {
        $path_param = $path_params[$i];
    }

    $controller_module_name = ucfirst($request_module) . 'Controller';
    $controller_module_php = $intranet_root . $basepath . $controller_dir . '/' . $controller_module_name . '.php';

    if (!file_exists($controller_module_php)) { // no matching controller, use default IndexController
        $controller_module_name = 'IndexController';
        $controller_module_php = $intranet_root . $basepath . $controller_dir . '/' . $controller_module_name . '.php';
    }
    if (file_exists($controller_module_php)) {
        include_once($controller_module_php);
        $module_controller = new $controller_module_name();
        $_PAGE['Controller'] = $module_controller;
        $request_mapping = $module_controller->findRequestMapping($_PAGE['REQUEST_URI']);

        if (count($request_mapping) > 0) {
            $_PAGE['REQUEST_URI_PATTERN'] = $request_mapping['uri'];
            $auth_method = 'authenticate';
            if (isset($request_mapping['auth'])) {
                $auth_method = $request_mapping['auth'];
            }
            if ($module_controller->$auth_method()) {
                if (isset($request_mapping['pathVars']) && count($request_mapping['pathVars']) > 0) {
                    call_user_func_array(array($module_controller, $request_mapping['function']), $request_mapping['pathVars']);
                } else {
                    $module_controller->$request_mapping['function']();
                }
            } else {
                $module_controller->sendRedirect("/");
            }
        } else {
            $_PAGE['REQUEST_URI_PATTERN'] = '/';
            $module_controller->index();
        }
    } else {
        header('HTTP/1.0 403 Forbidden');
        die();
    }
}
