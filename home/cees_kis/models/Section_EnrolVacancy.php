<?php

include_once 'Section.php';

class Section_EnrolVacancy extends Section
{
    var $db;
    var $table = 'CEES_KIS_MONTHLY_REPORT_ENROL_VACANCY';
    var $reportId;
    var $contents = array();

    var $dbFieldArr = array('WholeDayClass', 'HalfDayClass', 'Classroom', 'ApprovedEnrolment', 'LastEnrolment');
    var $jsonToDbMapping = array(
        'wholeDay' => 'WholeDayClass',
        'halfDay' => 'HalfDayClass',
        'classroom' => 'Classroom',
        'approved' => 'ApprovedEnrolment',
        'enrolment' => 'LastEnrolment'
    );

    public function __construct($db, $reportId)
    {
        parent::__construct();
        $this->db = $db;
        $this->reportId = $reportId;
    }

    public function fetch()
    {
        $jsonToDbMapping = $this->jsonToDbMapping;
        $selectFieldArr = array();
        foreach ($jsonToDbMapping as $_jsonName => $_dbName) {
            $selectFieldArr[] = $_dbName . ' as ' . $_jsonName;
        }
        $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '{$this->reportId}'";
        $this->contents = $this->db->returnResultSet($sql);

        // Load existed data
        $_thisYear = $this->getReportYear();
        $_thisMonth = $this->getReportMonth();
        if($_thisMonth != '9'){
            if($_thisMonth > 9){
                $_targetYear = $_thisYear;
            } else {
                $_targetYear = (int)$_thisYear - 1;
            }
            $sql = "SELECT ReportID FROM CEES_SCHOOL_MONTHLY_REPORT WHERE Year = '$_targetYear' AND Month = '9'";
            $info = $this->db->returnVector($sql);
            $_targetReportID = $info[0];
            $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '$_targetReportID'";
            $data = $this->db->returnResultSet($sql);

            $this->contents['0']['wholeDay'] = $data['0']['wholeDay'];
            $this->contents['0']['halfDay'] = $data['0']['halfDay'];
            $this->contents['0']['classroom'] = $data['0']['classroom'];
            $this->contents['0']['approved'] = $data['0']['approved'];

            if($this->contents['0']['enrolment'] == '') {
                if ($_thisMonth != '1') {
                    $_lastMonth = (int)$_thisMonth - 1;
                    $_targetYear = $_thisYear;
                } else {
                    $_lastMonth = '12';
                    $_targetYear = (int)$_thisYear - 1;
                }
                $sql = "SELECT ReportID FROM CEES_SCHOOL_MONTHLY_REPORT WHERE Year = '$_targetYear' AND Month = '$_lastMonth'";
                $info = $this->db->returnVector($sql);
                $_targetReportID = $info[0];
                $sql = "SELECT " . implode(',', $selectFieldArr) . " FROM {$this->table} WHERE ReportID = '$_targetReportID'";
                $data = $this->db->returnResultSet($sql);

                $this->contents['0']['enrolment'] = $data['0']['enrolment'];
            }
        }
    }
}