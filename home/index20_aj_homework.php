<?php

## Using By : 

################ Change Log [Start] #####################
#
#	Date 	:	2010-06-23 [Yuen]
#	Details	:	Improved to show the div in Chrome which failed before due to use of "height:100%" in div
#
################ Change Log [End] #####################


$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lh = new libhomework2007();
$ListContent = $lh->displayIndex($classID,$UserID);

$ListType = 1;
include_once("index_right_menu.php");

# Improved to show the div in Chrome which failed before due to use of "height:100%" in div
$HeightUsed = (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) ? "height:100%;" : "";

$x = 	"
				<table width=\"220\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" >
				<tr>
					<td height=\"40\">{$ListMenu}</td>
				</tr>
				<tr>
					<td valign=\"top\">
					<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
					<tr>
						<td width=\"5\" height=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_01.gif\" width=\"5\" height=\"5\"></td>
						<td height=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_02.gif\" width=\"5\" height=\"5\"></td>
						<td width=\"6\" height=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_03.gif\" width=\"6\" height=\"5\"></td>
					</tr>
					<tr>
						<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_04.gif\" width=\"5\" height=\"5\"></td>
						<td valign=\"top\" bgcolor=\"#FFFFFF\" width=\"209\">			
						<div id=\"ListContentDiv\" style=\"width:100%; {$HeightUsed} z-index:1; overflow: auto;\">			
						{$ListContent}						
						</div>
						</td>
						<td width=\"6\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_06.gif\" width=\"6\" height=\"5\"></td>
					</tr>
					<tr>
						<td width=\"5\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_07.gif\" width=\"5\" height=\"6\" /></td>
						<td height=\"6\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_08.gif\" width=\"6\" height=\"6\" /></td>
						<td width=\"6\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_09.gif\" width=\"6\" height=\"6\" /></td>
					</tr>
					</table>
		";
		
echo $x;			

$benchmark['after homework list'] = time();			
intranet_closedb();

?>