<?php

## Using By : Jason

################ Change Log [Start] #####################
#
#	Date 	:	2012-08-29 [Jason] 
#	Details	:	For 190 WWS Project, Customization AJAX Page for retrieving special classrooms which room type belong 13
#
################ Change Log [End] #####################


$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$lu = new libuser2007($UserID);
$le = new libeclass2007();
$lh = new libhomework2007();
$lp = new libportal();

$roomType = (isset($roomType) && $roomType != '') ? $roomType : 0;

# Improved to show the div in Chrome which failed before due to use of "height:100%" in div
//$HeightUsed = (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) ? "height:100%;" : "";
$HeightUsed = "height:150px;";

$ListContent  = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
$ListContent .= "<tr>";
$ListContent .= '<tr  width=\"90%\" class=\"indextabwhiterow\">'.$lp->displayUserEClass($lu->UserEmail,1, $roomType).'</tr>';
$ListContent .= "<td width=\"5%\" class=\"indextabwhiterow\" >&nbsp;</td></tr></table>";


include_once("index_right_menu_wws.php");


$x = 	"
	<table width=\"220\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" >
	<tr>
		<td height=\"20\">{$ListMenu}</td>
	</tr>
	<tr>
		<td valign=\"top\">
		<table width=\"100%\" height=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
		<tr>
			<td width=\"5\" height=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_01.gif\" width=\"5\" height=\"5\"></td>
			<td height=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_02.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_02.gif\" width=\"5\" height=\"5\"></td>
			<td width=\"6\" height=\"5\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_03.gif\" width=\"6\" height=\"5\"></td>
		</tr>
		<tr>
			<td width=\"5\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_04.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_04.gif\" width=\"5\" height=\"5\"></td>
			<td valign=\"top\" bgcolor=\"#FFFFFF\" width=\"100%\" >
			<div id=\"WWSListContentDiv\" style=\"width:100%; {$HeightUsed} z-index:1; overflow: auto;\">
			{$ListContent}						
			</div>
			</td>
			<td width=\"6\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_06.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_06.gif\" width=\"6\" height=\"5\"></td>
		</tr>
		<tr>
			<td width=\"5\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_07.gif\" width=\"5\" height=\"6\" /></td>
			<td height=\"6\" background=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_08.gif\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_08.gif\" width=\"6\" height=\"6\" /></td>
			<td width=\"6\" height=\"6\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/index/group_tab/board_09.gif\" width=\"6\" height=\"6\" /></td>
		</tr>
		</table>
		";
		
echo $x;	

$benchmark['eclass_wws'] = time();
		
intranet_closedb();

?>