<?php

## Using By :  

################ Change Log [Start] #####################
#
#	Date 	:	2010-08-19 [Kelvin]
#	Details	:	Show the classroom list for parent
#
#
#	Date 	:	2010-06-23 [Yuen]
#	Details	:	Improved to show the div in Chrome which failed before due to use of "height:100%" in div
#
################ Change Log [End] #####################

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lu = new libuser2007($UserID);
$lp = new libportal();

echo $lp->displayUserEClassInElearningSection($lu->UserEmail);

$benchmark['eclass'] = time();
		
intranet_closedb();
?>