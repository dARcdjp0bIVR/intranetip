<?php

# For skip browser checking in Dev
if ($skip_checking == 1)
{
	$_SESSION['Is_First_Login'] = 0;
	header("Location: /home/index.php?skip_browser_checking=1");
	
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>eClass IP 2.5 > Warning</title>
<link href="/templates/2009a/css/text" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

</head>


<body>
<table width="100%" height="100%"border="0" cellspacing="20" cellpadding="0">
  <tr> 
    <td valign="top">
		<table width="" border="0" align="center" cellpadding="0" cellspacing="0">
       <tr><td><!-- ****** -Module title and left menu Start  ****** --><!-- ****** Module title and left menu End ****** -->
		</td></tr>
        <tr><td class="board_menu_closed">
			  <!-- ****** Content Board Start ****** -->
			  <table width="99%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
					<td height="33" background="/images/2009a/content_01_bg.gif" class="contentitle_bar"><img src="/images/2009a/10x10.gif" width="13" height="33"></td>
					<td valign="bottom" background="/images/2009a/content_02_bg.gif" class="contentitle_bar">
						<div id="contenttitle">提示 / Warning</div>								</td>
					<td height="33" background="/images/2009a/content_03_bg.gif" class="contentitle_bar"><img src="/images/2009a/10x10.gif" width="11" height="33"></td>
				</tr>
                    <tr> 
                      <td width="13" background="/images/2009a/content_04.gif">&nbsp;</td>
                      <td class="main_content" style="height:auto"><br />
                        <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <? if ($browser_problem == 1) { ?>
                          <tr>
                            <td nowrap="nowrap"><img src="/images/2009a/alert_signal.gif" width="50" height="50" align="absmiddle" />
                            	<span style="font-size:18; color:#FF0000">瀏覽器不支援 / Browser NOT supported</span>
                            </td>
                          </tr>
                          
                          <tr>
                            <td><table width="95%" border="0" align="right" cellpadding="5">
                              <tr>
                                  <td valign="top"> </td>
                                  <td valign="top">
                                  	eClass校園綜合平台並不支援你所使用的瀏覽器版本，本視窗將自動關閉。請使用Internet Explorer 7或Firefox 3進行瀏覽。
                                  	<br />
                                  	<a href="http://www.microsoft.com/windows/internet-explorer/ie7/" target="_self" class="tablelink">[按此下載更新檔]</a>
                                  	<br />
                                  	<br />
									Your browser is NOT supported by eClass Integrated Platform. Please use Internet Explorer 7 or Firefox 3.
									<br />
									<a href="http://www.microsoft.com/windows/internet-explorer/ie7/" target="_self" class="tablelink">[Press here to download the update]</a>
								  </td>
                                </tr>
                                
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                              </table>                            </td>
                          </tr>
                          <? } ?>
                          
                          <? if ($flash_problem == 1) { ?>
                          <tr>
                            <td nowrap="nowrap"><img src="/images/2009a/alert_signal.gif" alt="" width="50" height="50" align="absmiddle" />
                            	<span style="font-size:18; color:#FF0000">Flash播放器不支援 / Flash Player NOT supported </span></td>
                          </tr>
                          <tr>
                            <td><table width="95%" border="0" align="right" cellpadding="5">
                              <tr>
                                  <td valign="top"> </td>
                                  <td valign="top">
                                  	eClass校園綜合平台不支援你所使用的Flash播放器版本，本視窗將自動關閉。請更新至Flash播放器10或更高版本。
                                  	<br />
									<a href="http://get.adobe.com/flashplayer/" target="_self" class="tablelink">[按此下載更新檔]</a>
									<br />
									<br />
									Your Flash Player is NOT supported by eClass Integrated Platform. Please upgrade to Flash Player 10 or above. This window will be closed.
									<br />
									<a href="http://get.adobe.com/flashplayer/" target="_self" class="tablelink">[Press here to download the update]</a>
								  </td>
								</tr>
                                <tr>
                                  <td>&nbsp;</td>
                                  <td>&nbsp;</td>
                                </tr>
                            </table></td>
                          </tr>
                          <? } ?>
                          
                          <tr>
                            <td align="right"><span><br />
                            	<img src="/images/2009a/logo_eclass_footer.gif" width="39" height="15" border="0" align="absmiddle" /></span>
                            </td>
                          </tr>

                        </table>
                        <div></div>
                        <div id="insert"></div>
                        <div></div>
                        <div id="insert2"></div>
                        <div></div>
                        <br style="clear:both" />
                        </div></td>
                      <td width="11" background="/images/2009a/content_06.gif">&nbsp;</td>
                    </tr>
                    <tr> 
                      <td width="13" height="10"><img src="/images/2009a/content_07.gif" width="13" height="10"></td>
                      <td background="/images/2009a/content_08.gif" height="10"><img src="/images/2009a/content_08.gif" width="13" height="10"></td>
                      <td width="11" height="10"><img src="/images/2009a/content_09.gif" width="11" height="10"></td>
                    </tr>
                  </table>
			  <!-- ****** Content Board End  ****** -->
        </td></tr>
    </table></td>
  </tr>
</table>
</body>
</html>
