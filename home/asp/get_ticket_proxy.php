<?php
// This page is acted as a proxy for eclass application, such that there is no library conflict when calling this page using fopen

// note that cURL is not used. Since every site has different set of php extension and everyone is reluctant to install any php extension even such php extension can bring great benefit to system, we cannot be sure that which php extension that clients have.		

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
include_once("../../includes/liblslp.php");
intranet_auth();
intranet_opendb();

$Project = "LSLP";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
$ls = new lslp();

if($ls->hasLSLPAccess() && $ls->isInLicencePeriod())
{
	# do nothing
}
else
{
	echo serialize("unAuthorized");
	exit();
}

if ($li_asp->GET_CURRENT_LICENSE())
{
	list($url, $dataArr) = $li_asp->CHECK_ASP_ACCESS('',false);
}


$url = $li_asp->ASP_Server.'/home/lslp/get_ticket.php';
intranet_closedb();

$postdata = http_build_query($dataArr);
$http_opt = array(
	'method'=>'POST',
	'max_redirects' => '20',
	'content'=>$postdata
	);
$opts = array(
	'http'=>$http_opt
);
$ctx = stream_context_create($opts);
session_write_close();

try{
	$f = fopen ($url, 'r', false,$ctx);

	$content = stream_get_contents($f); 

	fclose($f); 
}catch(Exception $e){
	header("HTTP/1.0 503 Service Unavailable!");
	echo "503 Service Unavailable!";
	exit();
}
 
$ticket = unserialize($content);

echo serialize(array('lslp_httppath'=>$li_asp->ASP_Server,'ticket'=>$ticket));

intranet_closedb();
?>