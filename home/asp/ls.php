<?php

# to access LS server


include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
include_once("../../includes/libls.php");
intranet_auth();
intranet_opendb();

$Project = "LS";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
$ls = new ls();

//if($ksk->hasKSKAccess() && $ksk->isInLicencePeriod())
if($ls->hasLSAccess())
{
	# do nothing
}
else
{
	echo "<script>window.close()</script>";
	die;
}

if ($li_asp->GET_CURRENT_LICENSE())
{
	list($url, $form_data) = $li_asp->CHECK_ASP_ACCESS();
}
$form_data.="<input type='hidden' name=\"RecordID\" value=\"".$RecordID."\" />";
$form_data.="<input type='hidden' name=\"try_login\" value=\"".$try_login."\" />";
$form_data.="<input type='hidden' name=\"signature\" value=\"".$signature."\" />";
$form_data.="<input type='hidden' name=\"course_id\" value=\"".$ck_course_id."\" />";
$form_data.="<input type='hidden' name=\"is_attach\" value=\"".$is_attach."\" />";
if($is_attach)
	$form_data.="<input type='hidden' name=\"fieldID\" value=\"".$fieldID."\" />";
intranet_closedb();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<!--<body>-->
<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
<?=$form_data?>
</form>
</body>

</html>
