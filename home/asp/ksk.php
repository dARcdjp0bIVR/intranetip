<?php

# to access KSK server


include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
include_once("../../includes/libksk.php");
intranet_auth();
intranet_opendb();

$Project = "KSK";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
$ksk = new ksk();

//if($ksk->hasKSKAccess() && $ksk->isInLicencePeriod())
if($ksk->hasKSKAccess())
{
	# do nothing
}
else
{
	echo "<script>window.close()</script>";
	die;
}

if ($li_asp->GET_CURRENT_LICENSE())
{
	list($url, $form_data) = $li_asp->CHECK_ASP_ACCESS();
}


intranet_closedb();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
<?=$form_data?>
</form>
</body>

</html>
