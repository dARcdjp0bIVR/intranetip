<?php

# to access LS server


include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libasp.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/iTextbook/libitextbook.php");
include_once("../../includes/libIntranetModule.php");
include_once("../../includes/libuser.php");
intranet_auth();
intranet_opendb();

$Project = "GrammarTouch3";
$IsServer =  FALSE;
$li_asp = new libasp($Project, $IsServer);
$libitextbook = new libitextbook();
$lib_user = new libuser($UserID);
$course_code = "grammartouch3";
$module_id = $libitextbook->get_iTextbook_ModuleID($course_code);
$libintranetmodule = new libintranetmodule();
$sql = "
	SELECT COUNT(*) CNT FROM
		".$intranet_db.".INTRANET_MODULE AS m 
		INNER JOIN ".$intranet_db.".INTRANET_MODULE_LICENSE AS ml ON m.ModuleID = ml.ModuleID
		INNER JOIN ".$intranet_db.".ITEXTBOOK_DISTRIBUTION AS id ON ml.ModuleLicenseID = id.ModuleLicenseID
		INNER JOIN ".$intranet_db.".ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
	WHERE m.Code = '".$course_code."'
";
$cnt = current($li_asp->returnVector($sql));
if($cnt>0||$libintranetmodule->IS_ADMIN_USER($UserID))
{
	# do nothing
}
else
{
	echo "<script>window.close()</script>";
	die;
}

list($url, $form_data) = $li_asp->CHECK_ASP_ACCESS();
$form_data.="<input type='hidden' name='SchoolName' value='".rawurlencode($BroadlearningClientName)."' />";
$form_data.="<input type='hidden' name='UserGender' value='".rawurlencode($lib_user->Gender)."' />";
$form_data.="<input type='hidden' name='SchoolType' value='S' />";
intranet_closedb();
?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<!--<body>-->
<body onLoad="document.MainForm.submit();">
<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
<?=$form_data?>
<input type="hidden" id="isBackend" name="isBackend" value="<?=$is_backend?>"/>
</form>
</body>

</html>
