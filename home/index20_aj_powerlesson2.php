<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT . "includes/global.php");
include_once($PATH_WRT_ROOT . "includes/libdb.php");
include_once($PATH_WRT_ROOT . "includes/libuser.php");
include_once($PATH_WRT_ROOT . "includes/libuser2007a.php");
include_once($PATH_WRT_ROOT . "includes/libeclass.php");
include_once($PATH_WRT_ROOT . "includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT . "includes/libportal.php");
include_once($PATH_WRT_ROOT . "includes/libaccess.php");
include_once($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lu = new libuser2007($UserID);
$lp = new libportal();

$courses = $lp->returnEClassUserIDCourseIDStandard($lu->UserEmail);

if(count($courses) == 0){
    $lu = new libuser($UserID);
    $la = new libaccess($UserID);
    $la->retrieveAccessEClass();

    $html = '<div class="index-remark">';
    if (
        ($lu->teaching && $la->isAccessEClassMgtCourse()) ||
        (!$lu->teaching && $lu->RecordType == 1 && $la->isAccessEClassNTMgtCourse()) ||
        ($_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] && !$plugin["Disable_eClassroom"])
    ) {
        $leclass = new libeclass2007();
        $createClassroomUrl = "/home/eLearning" . $leclass->eclass_folder . "/organize/new.php";

        $html .= '<div class="add-classroom"></div>
                          <p>' . $Lang['eLearningSection']['NotEnrolledInAnyClassroom'] . '</p>
                          <input type="button" class="formbutton_v30" value="' . $Lang['eLearningSection']['CreateClassroom'] . '" onclick="location.href=\'' . $createClassroomUrl . '\';">';
    } else {
        $html .= '<div class="add-classroom-student"></div>
                          <p>' . $Lang['eLearningSection']['NotEnrolledInAnyClassroom'] . '</p>';
    }
    $html .= '</div>';
} else {
    $html = '<ul class="indexlist-container">';
    foreach ($courses as $course) {
        $courseId = $course[0];
        $courseCode = intranet_wordwrap($course[1], 30, "\n", 1);
        $courseName = intranet_wordwrap($course[2], 30, "\n", 1);
        $memberType = $course[4];
        $statusDisplay = $course[7] == "suspended" ? " - <i>{$i_status_suspended}</i>" : '';

        $html .= <<<HTML
<li class="indexlist-item">
    <a href="javascript:loginPowerLesson2({$courseId})">
        <span class="indexlist-title">{$courseCode} - {$courseName} ({$memberType}{$statusDisplay})</span>
        <!--<span class="new-alert"></span>-->
    </a>
</li>
HTML;
    }
    $html .= '</ul>';
}

echo $html;

$benchmark['eclass'] = time();

intranet_closedb();