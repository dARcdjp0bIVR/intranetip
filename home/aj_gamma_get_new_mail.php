<?
// editing by 
# Get total number of new iMail Gamma mails
/*
 * 2014-09-03 (Carlos): If change password fail, then if check mail account does not exist, auto create the email account 
 * 2014-07-03 (Carlos): Auto sync password if email account password differs with IP25 password
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_opendb();

$IMap = new imap_gamma();
if(!$IMap->ConnectionStatus){
	$IMap = new imap_gamma(1); // skip login email account
	$IMap->change_password($_SESSION["SSV_EMAIL_LOGIN"], $_SESSION["SSV_EMAIL_PASSWORD"]);
	$IMap = new imap_gamma();
	if(!$IMap->ConnectionStatus){
		if(!$IMap->is_user_exist($_SESSION["SSV_EMAIL_LOGIN"])){
			//$li = new libdb();
			$laccount = new libaccountmgmt();
			$account_exist = $IMap->open_account($_SESSION["SSV_EMAIL_LOGIN"], $_SESSION["SSV_EMAIL_PASSWORD"]);
			if($account_exist){
				$luser = new libuser($_SESSION['UserID']);
				// only update self email account, do not update for shared mailbox
				if($luser->ImapUserEmail == $_SESSION["SSV_EMAIL_LOGIN"])
				{
					$userquota = $IMap->default_quota;
					$laccount->setIMapUserEmail($_SESSION['UserID'],$_SESSION["SSV_EMAIL_LOGIN"]);
	        		$IMap->SetTotalQuota($_SESSION["SSV_EMAIL_LOGIN"], $userquota[$_SESSION['UserType']-1], $_SESSION['UserID']);
					
					// Block receive/send internet mails when [iMail Settings > Default Usage Rights and Quota > Internal mail only] is on
					$internal_only = $IMap->internal_mail_only; // index 0 is staff, 1 is student, 2 is parent, 3 is alumni
					if($internal_only[$_SESSION['UserType']-1]){ // shift index by one
						$IMap->addGroupBlockExternal(array($_SESSION["SSV_EMAIL_LOGIN"]));
						$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = '".$_SESSION['UserID']."' AND ACL IN (1,3)";
				    	$laccount->db_db_query($sql);
					}
				}
			}
		}
	}
}

if($IMap->ConnectionStatus==true)
{
	$mail_count = $IMap->Get_Unseen_Mail_Number($IMap->InboxFolder, 0);
}else{
	$mail_count = "-1"; // send back error code
}
echo $mail_count;

intranet_closedb();
die;
?>