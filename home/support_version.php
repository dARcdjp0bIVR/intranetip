<?php
include_once ("../includes/global.php");
include_once ("../includes/libdb.php");
include_once ("../includes/libuser.php");


intranet_auth();

intranet_opendb();
$lu = new libuser($UserID);
$US_Email = $lu->UserEmail;
$US_UserLogin = $lu->UserLogin;
$US_UserType = $lu->RecordType;


if (isset($intranet_version_url) && $intranet_version_url!="")
{
	$version_url = $intranet_version_url;
} else
{
	# by default
	$version_url = "hke-ip30-version.eclass.com.hk";
}

//$PageURLArr = split("//", curPageURL());
$PageURLArr = explode("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}
    //newWindow("http://hke-version.eclass.com.hk/?eclasskey=9de6a02494c1ada965da8e6f5c4fd8fe&eClassURL=http://intranet.lwlc.edu.hk",10);


intranet_closedb();
?>

<html>
<body ONLOAD="document.form1.submit()">
<form name="form1" action="http://<?=$version_url?>" method="post" >
<input type=hidden name="eClassURL" value="<?=$eClassURL?>">
<input type=hidden name="eclasskey" value="<?=$lu->sessionKey?>">
</form>
</body>
</html>