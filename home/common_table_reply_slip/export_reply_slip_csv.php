<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlipMgr.php');

if(strpos($_SERVER['HTTP_REFERER'],'/home/eAdmin/ResourcesMgmt/DocRouting/index.php?pe=8OaOAb4COQCRJNyp2jeWSB4kDyYanKf')===false){
	intranet_auth();
}
intranet_opendb();

$replySlipId = $_REQUEST['ReplySlipID'];
$replySlipTitle = rawurldecode($_REQUEST['Title']);
$exportType = $_REQUEST['exportType'];

$libReplySlipMgr = new libTableReplySlipMgr();
$libReplySlipMgr->setReplySlipId($replySlipId);
$libReplySlipMgr->exportReplySlipStatisticsCSV($replySlipTitle, $exportType);

intranet_closedb();
?>