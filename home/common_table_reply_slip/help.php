<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT.'includes/tableReplySlip/libTableReplySlipMgr.php');

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$libMgr = new libTableReplySlipMgr();

$linterface->LAYOUT_START();

echo $libMgr->getHelpPage();

$linterface->LAYOUT_STOP();

intranet_closedb();
?>