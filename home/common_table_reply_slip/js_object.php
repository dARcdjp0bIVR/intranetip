<?php
include_once($PATH_WRT_ROOT."lang/table_reply_slip_lang.$intranet_session_language.php");
?>
<script language="javascript">
var tableReplySlipObj = {
	_replySlipId: '',
	_linkToModule: '',
	_linkToType: '',
	_linkToId: '',
	_title: '',
	_description: '',
	_ansAllQuestion: '',
	_recordType: '',
	_recordStatus: '',
	
	
	setReplySlipId: function(val) {
		tableReplySlipObj._replySlipId = val;
	},
	getReplySlipId: function() {
		return tableReplySlipObj._replySlipId;
	},
	
	setLintToModule: function(val) {
		tableReplySlipObj._linkToModule = val;
	},
	getLintToModule: function() {
		return tableReplySlipObj._linkToModule;
	},
	
	setLintToType: function(val) {
		tableReplySlipObj._linkToType = val;
	},
	getLintToType: function() {
		return tableReplySlipObj._linkToType;
	},
	
	setLintToId: function(val) {
		tableReplySlipObj._linkToId = val;
	},
	getLintToId: function() {
		return tableReplySlipObj._linkToId;
	},
	
	setTitle: function(val) {
		tableReplySlipObj._title = val;
	},
	getTitle: function() {
		return tableReplySlipObj._title;
	},
	
	setDescription: function(val) {
		tableReplySlipObj._description = val;
	},
	getDescription: function() {
		return tableReplySlipObj._description;
	},
	
	setAnsAllQuestion: function(val) {
		tableReplySlipObj._ansAllQuestion = val;
	},
	getAnsAllQuestion: function() {
		return tableReplySlipObj._ansAllQuestion;
	},
	
	setRecordType: function(val) {
		tableReplySlipObj._recordType = val;
	},
	getRecordType: function() {
		return tableReplySlipObj._recordType;
	},
	
	setRecordStatus: function(val) {
		tableReplySlipObj._recordStatus = val;
	},
	getRecordStatus: function() {
		return tableReplySlipObj._recordStatus;
	},
	
	
	loadObjectData: function(replySlipId) {
		replySlipId = parseInt(replySlipId);
		if (replySlipId > 0) {
			$.ajax({
				url:		"/home/common_table_reply_slip/ajax_get_reply_slip_data.php",
				type:		"POST",
				data:		'replySlipId=' + replySlipId,
				async:		false,
				success:  	function(returnData) {
								var infoAry = returnData.split('<?=$tableReplySlipConfig['ajaxDataSeparator']?>');
								tableReplySlipObj.setReplySlipId(parseInt(infoAry[0]));
								tableReplySlipObj.setLintToModule(Trim(infoAry[1]));
								tableReplySlipObj.setLintToType(Trim(infoAry[2]));
								tableReplySlipObj.setLintToId(parseInt(infoAry[3]));
								tableReplySlipObj.setTitle(Trim(infoAry[4]));
								tableReplySlipObj.setDescription(Trim(infoAry[5]));
								tableReplySlipObj.setAnsAllQuestion(parseInt(infoAry[6]));
								tableReplySlipObj.setRecordType(parseInt(infoAry[7]));
								tableReplySlipObj.setRecordStatus(parseInt(infoAry[8]));
						  	}
			});
		}
	},
	
	returnQuestionIdAry: function() {
		var replySlipId = tableReplySlipObj.getReplySlipId();
		var questionIdHiddenFieldId = 'tableReplySlipItemIdList_' + replySlipId;
		
		return $('input#' + questionIdHiddenFieldId).val().split('<?=$tableReplySlipConfig['itemIdHiddenFieldSeparator']?>');
	},
	
	returnCurUserId: function() {
		var replySlipId = tableReplySlipObj.getReplySlipId();
		return $('input#tableReplySlipUserId_' + replySlipId).val();
	},
	
	submitReplySlip: function(replySlipId) {
		tableReplySlipObj.loadObjectData(replySlipId);
		var success;
		
		if (tableReplySlipObj.checkForm(1)) {
			success = tableReplySlipObj.saveAnswer();
		}
		else {
			success = false;
		}
		
		return success;
	},
	
	checkForm: function(noAlert) {
		noAlert = noAlert || 0;
		
		var replySlipId = tableReplySlipObj.getReplySlipId();
		tableReplySlipObj.loadObjectData(replySlipId);
		
		var itemAry = $('[name*="tableReplySlipItem_'+replySlipId+'_"]');
		var isValid = true;
		var isFocused = false;
		
		// Hide all warning div
		$('div.tableReplySlipWarningDiv_' + replySlipId).hide();
		
		// See if need to check if all questions have been answered or not
		var ansAllQuestion = tableReplySlipObj.getAnsAllQuestion();
		
		// loop questions
		var notAnsweredItemAry = new Array();
		itemAry.each(function(i){
			var _isAnswered = false;
			var _that = $(this);
			var _itemName = _that.attr('name');
			var _warnDivId = _itemName.replace('tableReplySlipItem_','tableReplySlipAnsQuestionWarningDiv_').replace('[]','');
			var _value = '';
			
			switch (_that.attr('tagName').toUpperCase()) 
			{
				case 'INPUT':
					var objectType = _that.attr('type').toUpperCase();
					switch (objectType) {
						case 'TEXT':
							_value = $.trim(_that.val());
							break;
						case 'RADIO':
						case 'CHECKBOX':
							if(_that.is(':checked')) {
								_value = _that.val();
							}
							break;
					}
				break;
				case 'TEXTAREA':
						_value = $.trim(_that.val());
						break;
				case 'SELECT':
					if (_that.attr('multiple')) {
						if( _that.val() ) {
							_value = _that.val().join(',');
						}
					}
					else {
						_value = _that.val();
					}
					break;
				default:
					_value = '';
			}
			if(_value != '') {
				_isAnswered = true;
			}
			
			if (!_isAnswered) {
				notAnsweredItemAry.push(_itemName);
				
				if (ansAllQuestion == '<?=$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['yes']?>') {
					isValid = false;
					$('div#'+_warnDivId).show();
					
					if (!isFocused) {
						// focus the first element of the first error question
						isFocused = true;
						_that.focus();
					}
				}
			}
		});
		
		if (ansAllQuestion == '<?=$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['yes']?>' && notAnsweredItemAry.length == 0) {
			if (noAlert == 1) {
				isValid = false;
			}
			else {
				if (confirm('<?=$Lang['ReplySlip']['WarningMsg']['SubmitReplySlip']?>')) {
					isValid = true;
				}
				else {
					isValid = false;
				}
			}
		}
		else if (ansAllQuestion == '<?=$tableReplySlipConfig['INTRANET_TABLE_REPLY_SLIP']['AnsAllQuestion']['no']?>' && notAnsweredItemAry.length > 0) {
			if (noAlert == 1) {
				isValid = true;
			}
			else {
				var alertText = '<?=$Lang['ReplySlip']['WarningMsg']['ReplySlipQuestionNotAnswered']?>';
				if (confirm(alertText)) {
					isValid = true;
				}
				else {
					isValid = false;
				}
			}
		}
		else {
			if (noAlert != 1) {
				if (confirm('<?=$Lang['ReplySlip']['WarningMsg']['SubmitReplySlip']?>')) {
					isValid = true;
				}
				else {
					isValid = false;
				}
			}
		}
		
		return isValid;
	},
	
	saveAnswer: function() {
		tableReplySlipObj.createTempForm();
		tableReplySlipObj.submitTempForm();
		tableReplySlipObj.removeTempForm();
		
		return true;
	},
	
	createTempForm: function() {
		// collect answer data
		var replySlipId = tableReplySlipObj.getReplySlipId();
		var itemAry = tableReplySlipObj.returnItemAry();
		var itemIdAry = tableReplySlipObj.returnQuestionIdAry();
		
		// build temp form for answer submission
		var x = '';
		x += '<form id="tableReplySlipTempForm" name="tableReplySlipTempForm" method="post" style="display:none">' + "\r\n";
		for(var i=0;i<itemAry.length;i++){
			var _name = itemAry[i]['name'];
			var _value = itemAry[i]['value'];
			x += '<input type="hidden" name="'+_name+'" value="' + htmlspecialchars(_value) + '" />' + "\r\n"; 
		}
		for(var i=0;i<itemIdAry.length;i++) {
			x += '<input type="hidden" name="tableReplySlipItemIdList_'+replySlipId+'[]" value="' + itemIdAry[i] + '" />' + "\r\n"; 
		}
		x += '</form>' + "\r\n";
		
		$('body').append(x);
	},
	
	returnItemAry : function(){
		var replySlipId = tableReplySlipObj.getReplySlipId();
		//var questionIdAry = tableReplySlipObj.returnQuestionIdAry();
		//var numOfQuestion = questionIdAry.length;
		var itemAry = $('[name*="tableReplySlipItem_'+replySlipId+'_"]');
		var itemNameValueAry = [];
		var _name = '';
		var _value = '';
		
		itemAry.each(function(i){
			var that = $(this);
			_name = that.attr('name');
			switch (that.attr('tagName').toUpperCase()) 
			{
				case 'INPUT':
					var objectType = that.attr('type').toUpperCase();
					switch (objectType) {
						case 'TEXT':
							_value = $.trim(that.val());
							itemNameValueAry.push({'name':_name,'value':_value});
							break;
						case 'RADIO':
						case 'CHECKBOX':
							if(that.is(':checked')) {
								_value = that.val();
								itemNameValueAry.push({'name':_name,'value':_value});
							}
							break;
					}
				break;
				case 'TEXTAREA':
						_value = $.trim(that.val());
						itemNameValueAry.push({'name':_name,'value':_value});
						break;
				case 'SELECT':
					if (that.attr('multiple')) {
						var valuesAry = that.val();
						if(valuesAry) {
							for(var i=0;i<valuesAry.length;i++) {
								itemNameValueAry.push({'name':_name,'value':valuesAry[i]});
							}
						}
					}
					else {
						_value = that.val();
						if(_value) {
							itemNameValueAry.push({'name':_name,'value':_value});
						}
					}
					break;
				default:
					_value = '';
			}
		});
		
		return itemNameValueAry;
	},
	
	submitTempForm: function() {
		$.ajax({
			url:		"/home/common_table_reply_slip/ajax_save_reply_slip_answer.php",
			type:		"POST",
			data:		'tableReplySlipId=' + tableReplySlipObj.getReplySlipId() + '&tableReplySlipUserId=' + tableReplySlipObj.returnCurUserId() + '&' + $('form#tableReplySlipTempForm').serialize(),
			async:		false,
			success:  	function(returnData) {
							
					  	}
		});
	},
	
	removeTempForm: function() {
		$('form#tableReplySlipTempForm').remove();
	}
};
</script>