<?php
## Using By :   

/********************** Change Log ***********************/
#	Date:		2017-04-18
#				Project - Power Class
/********************** Change Log ***********************/
$PATH_WRT_ROOT = "../../";
$PATH_WRT_ROOT_ABS = "/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libhomework.php");
include_once($PATH_WRT_ROOT."includes/libhomework2007a.php");

header("Location: /home/index.php");
exit;

if (!$sys_custom['PowerClass']) {
	header("Location: /home/index.php");
	exit;
}
if (!class_exists("libpowerclass_ui", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass_ui.php");
}

intranet_auth();
intranet_opendb();
$li = new libuser($UserID);
if ($UserType == USERTYPE_STAFF && !$_SESSION['isTeaching']) {
	$AcademicYearID = Get_Current_Academic_Year_ID();
	if ($AcademicYearID > 0) {
		$PowerClassLink = "/home/" . $sys_custom['Project_Label']. "/";
	} else {
		$PowerClassLink = "/home/" . $sys_custom['Project_Label']. "/#setup";
	}
	header("Location: " . $PowerClassLink);
	exit;
} else {
	#####################################
	# PowerLesson2 (PL2.0) Logo [Start]
	#####################################
	$libpowerclass_ui = new libpowerclass_ui();
	echo $libpowerclass_ui->getPowerLesson2HTML(false);
}
?>
<script language="javascript" type='text/javascript'>
document.getElementById('powerLessonLogin').submit();
</script>