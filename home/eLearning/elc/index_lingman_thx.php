<?php
$PATH_WRT_ROOT = "../../../";
include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libuser.php");
include($PATH_WRT_ROOT."includes/libeclass.php");
//include("../../lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$li = new libuser($UserID);
$lo = new libeclass();

$courseArray = $lo->returnSpecialRooms($li->UserEmail,2,0);
$l1_course = 0;
$l2_course = 0;
$l3_course = 0;
$l1_id = "";
$l2_id = "";
$l3_id = "";


if ((!is_array($courseArray)) || (count($courseArray)<=0)){
  header ("Location: /home");
} else {
  for ($i=0;$i<count($courseArray);$i++){
    $level = substr($courseArray[$i][13],7);
    if ($level=="1"){
      ++$l1_course;
      $l1_id = $courseArray[$i][6];
    }
    else if ($level=="2"){
      ++$l2_course;
      $l2_id = $courseArray[$i][6];
    }
    else if ($level=="3"){
      ++$l3_course;
      $l3_id = $courseArray[$i][6];
    }
  }
}

if (($l1_course == 0) && ($l2_course == 0) && ($l3_course == 0)){
  header ("Location: /home");
}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>凌文網上教室-嗚謝</title>
<style type="text/css">
<!--
.text2 {
	font-family: "新細明體", "Arial", "Verdana";
	font-size: 12px;
	color: #000000;
}
.text3 {
	font-family: "新細明體", "Arial", "Verdana";
	font-size: 13px;
	font-weight: bold;
}
-->
</style>
</head>

<body bgcolor="#DBDB82" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="<?=$eclass_url_root?>/images/cnecc/index/border_01.gif" width="16" height="16"></td>
          <td height="16" background="<?=$eclass_url_root?>/images/cnecc/index/border_02.gif"><img src="<?=$eclass_url_root?>/images/cnecc/index/10x10.gif" width="16" height="16"></td>
          <td><img src="<?=$eclass_url_root?>/images/cnecc/index/border_03.gif" width="16" height="16"></td>
        </tr>
        <tr>
          <td width="16" background="<?=$eclass_url_root?>/images/cnecc/index/border_04.gif"><img src="<?=$eclass_url_root?>/images/cnecc/index/10x10.gif" width="16" height="10"></td>
          <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="2" cellpadding="5">
              <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="100"><img src="<?=$eclass_url_root?>/images/cnecc/index/thanks_title_01.gif" width="100" height="41"></td>
                      <td align="center" bgcolor="#A33704"><img src="<?=$eclass_url_root?>/images/cnecc/index/thanks_title_02.gif" width="127" height="41"></td>
                      <td width="100"><img src="<?=$eclass_url_root?>/images/cnecc/index/thanks_title_01.gif" width="100" height="41"></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td class="text2">顧問：<span class="text3">何文勝博士</span>（香港教育學院高級講師）</td>
              </tr>
              <tr>
                <td class="text2">主編：<span class="text3">戴志鋒老師</span>（文學碩士、文學士、教育證書）</td>
              </tr>
              <tr>
                <td class="text2">副編：<span class="text3">陳得南老師</span>（文學士、國際關係碩士、修讀教育文憑）</td>
              </tr>
              <tr>
                <td class="text2">編委：<span class="text3">伍文彥老師</span>（文學士、教育文憑、工商管理碩士）</td>
              </tr>
              <tr>
                <td class="text2">　　　<span class="text3">張巧芝老師</span>（文學士、修讀教育文憑）</td>
              </tr>
              <tr>
                <td class="text2">　　　<span class="text3">李然慧老師</span>（文學士、教育證書）</td>
              </tr>
              <tr>
                <td class="text2">　　　<span class="text3">鄭敏儀老師</span>（文學士、教育證書）</td>
              </tr>
            </table></td>
          <td width="16" background="<?=$eclass_url_root?>/images/cnecc/index/border_06.gif"><img src="<?=$eclass_url_root?>/images/cnecc/index/10x10.gif" width="16" height="10"></td>
        </tr>
        <tr>
          <td><img src="<?=$eclass_url_root?>/images/cnecc/index/border_07.gif" width="16" height="17"></td>
          <td height="16" background="<?=$eclass_url_root?>/images/cnecc/index/border_08.gif"><img src="<?=$eclass_url_root?>/images/cnecc/index/10x10.gif" width="16" height="16"></td>
          <td><img src="<?=$eclass_url_root?>/images/cnecc/index/border_09.gif" width="16" height="17"></td>
        </tr>
      </table></td>
  </tr>
</table>
<br>
</body>
</html>
