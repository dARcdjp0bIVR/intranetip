<?php
/*
 * Editing by:
 * 
 * Modification Log:
 * 2011-03-04 (Jason)
 * 		- fix the problem of selecting Subject Group List as ClassTitleB5 should be used, instead of ClassTitleGB
 */ 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb(); 
if ($subjectID!=""){
	$libdb = new libdb();
	$classTitle = "c.ClassTitle".strtoupper(($intranet_session_language == 'gb') ? 'b5' : $intranet_session_language);
	$sql = "select $classTitle as ClassTitle, c.SubjectGroupID
			from {$intranet_db}.SUBJECT_TERM as t inner join 
			{$intranet_db}.SUBJECT_TERM_CLASS as c on
				t.SubjectGroupID = c.SubjectGroupID 
			where
				t.SubjectID = '$subjectID' and 
				(c.course_id is null OR c.course_id = 0 ".(empty($selectedID)?'':"and c.course_id <> '$selectedID'").")
				
				and t.YearTermID in (
					select
						ayt.YearTermID
					from
						{$intranet_db}.ACADEMIC_YEAR as ay
						inner join
						{$intranet_db}.ACADEMIC_YEAR_TERM as ayt
						on ay.AcademicYearID = ayt.AcademicYearID
					where NOW() between ayt.TermStart and ayt.TermEnd
				)
				".(empty($exclusList)?'':" and c.SubjectGroupID not in (".implode(",",$exclusList).") ")."
			";
	$result = $libdb->returnArray($sql); 

	$selectGroup = "<select id='subjectGroupID' onchange='subjectGroupHandler.addSubjectGroup(this)' >";
	$selectGroup .= "<option value=''>--{$button_select}--</option>";
	foreach($result as $r){
		//$selected = $r['selected'];
		$selectGroup .= '<option value="'.$r["SubjectGroupID"].'"   >'.$r["ClassTitle"].'</option>';
	}
	$selectGroup .= "</select>";
	echo $selectGroup;
}

echo "";
intranet_closedb();
?>