<?php
/*
 * Editing by 
 * 
 * Modification Log:
 * 2015-07-14 (Jason) [ip.2.5.6.7.1.0]
 * - change the wording of Import user button from Import to Add
 * - show proper wording in the Identity column of the user list
 * 2011-09-13 (Jason)
 * - add $linkedSubjectGroupIDList and modify sql query to improve the Subject Group pulldown box to 
 * 	 list out possible Subject Group including which xisting students belong to & which the course is linking now
 * 
 * 2011-04-13 (Jason)
 * - fix the problem of not showing class number correctly
 *  
 * 2011-02-23 (Jason)
 * - do not display invalid class number in user record e.g. 0, NULL
 * 
 * 2010-11-11 (Thomas)
 * - get the no. of user from no. of row returned by datatable
 * 
 * 2010-10-27 (Kelvin)
 * - add "and status is null" in the main sql statement which retrieves user
 * 
 * 	2010-07-22 (Jason):
 * 	- add wording Subject Group beside the Selection Box Filter
 */
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();
########################################################################################################

# use Library
$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

$specialClassroom = true;

# eClass
$lo = new libeclass($course_id);
$roomType = $lo->getEClassRoomType($course_id);

$leclass = new libeclass2007($course_id, $roomType, $specialClassroom);

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();

if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->RecordType!=1 || !$la->isAccessEClassMgt())
  {
    header("Location: /");
    exit;
  }
}


# verify user right
// get right of creator of the course / admin
$sql = "SELECT course_id, SubjectGroupID FROM course where course_id='$course_id' ";
$sql .= ($_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])? " LIMIT 1 ":" AND creator_id='$UserID'  LIMIT 1"; // Admin can manage any course
$row = $lo->returnArray($sql, 2);

$linkedSubjectGroupIDList = $row[0][1];
$linkedSubjectGroupIDArr = ($linkedSubjectGroupIDList != '') ? explode(";", $linkedSubjectGroupIDList) : array();

$isAccessGained = ($row[0][0]!="");
if (!$isAccessGained)
{
  // get right of teacher identity in the course
  $sql = "SELECT course_id FROM user_course where user_email='".$lu->UserEmail."' AND memberType='T' AND course_id='$course_id' LIMIT 1";
  $row = $lo->returnArray($sql, 1);
  $isAccessGained = ($row[0][0]!="");
}

$isAccessGained = ($isAccessGained && $leclass->checkeSpecialRoomtAccessPermission(false)) ? true : false;

$eclass_3orup = ($eclass_version >= 3.0);  //is_dir("$eclass_filepath/system/");

$files_dir = ($eclass_3orup? "files":"file");

$dir_prefix = $lo->db_prefix;

$dir_list = array(
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/notes",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/reference",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/glossary",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/assignment",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/question",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/public",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/group");

$lf = new libfilesystem();

if ($eclass_version < 3.0)
{
  for($i=0; $i<sizeof($dir_list); $i++){
    $storage[$i] = $lf->folder_size($dir_list[$i]);
  }
  $c_size = 0;
  $c_files = 0;
  $c_dirs = 0;
  
  for($i=0; $i<sizeof($storage); $i++){
    $size = $storage[$i];
    $c_size += $size[0];
    $c_files += $size[1];
    $c_dirs += $size[2];
  }
  $c_size = number_format(($c_size/1048576),2, ".", "");
}
// eclass 3.0
if ($eclass_version >= 3.0) {
  $li = new libdb();
  $li->db = $eclass_prefix."c".$course_id;
  $sql = "SELECT SUM(size) FROM eclass_file WHERE VirPath is NULL AND Category<>9";
  $row = $li->returnArray($sql, 1);
  $c_size = ($row[0][0]!="") ? number_format(($row[0][0]/1024),2, ".", "") : 0;
}

$ImportBtn 	= "<a href=\"javascript:newWindow('import.php?course_id=".$lo->course_id."',2)\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_import.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_add . "</a>";
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'user_id[]','delete.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";

$transBtn 	= "<a href=\"javascript:void(0)\" onclick=\"document.form1.action='transfer_ownership.php';document.form1.submit()\" class=\"tabletool\">". $i_ec_file_assign  ."</a>";


# check whether it is special subject group value
$withSGPrefix = (strstr($subjectGroupID, 'A_') == false) ? false : true;

# Get Current Year Term ID
$currentYearTermID = getCurrentSemesterID();

$sub_sql= '';
$sql = "select distinct u.user_id 
		from {$intranet_db}.SUBJECT_TERM_CLASS as t
		inner join {$intranet_db}.SUBJECT_TERM as t1 on 
			t1.YearTermID = '".$currentYearTermID."' and t1.SubjectGroupID = t.SubjectGroupID 
		Left join {$intranet_db}.SUBJECT_TERM_CLASS_USER as cu on 
			cu.SubjectGroupID = t.SubjectGroupID
		Left join {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER as ct on 
			ct.SubjectGroupID = t.SubjectGroupID
		inner join {$intranet_db}.INTRANET_USER as i on 
			cu.UserID = i.UserID or ct.UserID = i.UserID
		inner join {$eclass_db}.user_course as u on 
			u.user_email = i.UserEmail and u.course_id = t.course_id
		where u.course_id = $course_id ";
//echo $sql;
$allSubjUser = $lo->returnVector($sql);

if (!empty($subjectGroupID)){
	if(strstr($subjectGroupID, 'A_') == false){
		if ($subjectGroupID!=-1){
			$sql = "select distinct u.user_id from 
				{$intranet_db}.SUBJECT_TERM_CLASS as t 
				inner join {$intranet_db}.SUBJECT_TERM as t1 on 
					t1.YearTermID = '".$currentYearTermID."' and t1.SubjectGroupID = t.SubjectGroupID 
				Left join {$intranet_db}.SUBJECT_TERM_CLASS_USER as cu on 
					cu.SubjectGroupID = t.SubjectGroupID
				Left join {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER as ct on 
					ct.SubjectGroupID = t.SubjectGroupID
				inner join {$intranet_db}.INTRANET_USER as i on 
					cu.UserID = i.UserID or ct.UserID = i.UserID
				inner join {$eclass_db}.user_course as u on 
					u.user_email = i.UserEmail and u.course_id = t.course_id
				where t.SubjectGroupID = '".$subjectGroupID."' ";
			$r = $lo->returnVector($sql);
			$sub_sql = " and user_id in ('".implode("','",$r)."')";
		} else{
			$sub_sql = " and user_id not in ('".implode("','",$allSubjUser)."')";
		}
	} else {
		$subjectGroupID = str_replace("A_", "", $subjectGroupID);
		$sql = "select distinct u.user_id from
				(
					select distinct cu.UserID, i.UserEmail 
					from {$intranet_db}.SUBJECT_TERM_CLASS as t  
					Left join {$intranet_db}.SUBJECT_TERM_CLASS_USER as cu on 
						cu.SubjectGroupID = t.SubjectGroupID
					inner join {$intranet_db}.INTRANET_USER as i on 
						cu.UserID = i.UserID and i.RecordStatus = 1 
					where t.SubjectGroupID = '".$subjectGroupID."'
					UNION ALL
					select distinct ct.UserID, i.UserEmail 
					from {$intranet_db}.SUBJECT_TERM_CLASS as t
					Left join {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER as ct on 
						ct.SubjectGroupID = t.SubjectGroupID
					inner join {$intranet_db}.INTRANET_USER as i on 
						ct.UserID = i.UserID and i.RecordStatus = 1 
					where t.SubjectGroupID = '".$subjectGroupID."'
				) as z 
				inner join {$eclass_db}.user_course as u on 
					u.user_email = z.UserEmail and u.course_id = '".$course_id."'  ";
			$r = $lo->returnVector($sql);
			//echo $sql.'<br>';
			$sub_sql = " and user_id in ('".implode("','",$r)."')";
	}
}

$name_field = getNameFieldForRecord2eClassOnly();

$sql = "
  SELECT 
    ".$name_field." as user_name, 
    if(class_number != '0' and class_number != '', class_number, '') as class_number, 
    CASE 
		WHEN memberType = 'T' THEN '".$Lang['eClass']['Teacher']."' 
		WHEN memberType = 'S' THEN '".$Lang['eClass']['Student']."' 
		WHEN memberType = 'A' THEN '".$Lang['eClass']['TeachingAssistant']."' 
	ELSE memberType END, 
    CONCAT('<input type=\"checkbox\" name=\"user_id[]\" value=\"', user_id ,'\">'),
    if(class_number is null or class_number = '','',concat(left(class_number,LOCATE(' - ',class_number)), ' - ' , LPAD(TRIM(SUBSTRING(class_number, LOCATE(' - ',class_number) + 3)), 3, '0'))) as class_order
  FROM 
    user_course 
  WHERE 
    course_id = $course_id
	and status is null
	$sub_sql
";
//echo $sql;
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# TABLE INFO
if($field == "") $field = 1;
if($order == "") $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("user_name", "class_order", "memberType");
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = 5;
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>&nbsp;#&nbsp;</td>\n";
$li->column_list .= "<td width='75%' class='tabletop'>".$li->column(0, $i_UserName)."</td>\n";
$li->column_list .= "<td width='15%' class='tabletop'>".$li->column(1, "$i_UserClassName ($i_UserClassNumber)")."</td>\n";
$li->column_list .= "<td width='10%' class='tabletop'>".$li->column(2, $i_eClass_Identity)."</td>\n";
$li->column_list .= "<td width='1' class='tabletop'>".$li->check("user_id[]")."</td>\n";

$display = $li->display();

if ($lo->max_user!="")
{
  $user_quota_left = ($lo->ticketUser()>0) ? $lo->ticketUser() : "<font color='red'>".$lo->ticketUser()."</font>";
  $size_msg .= "$i_eClassNumUsers : $li->total_row / $lo->max_user ";
}
if ($lo->max_storage!="")
{ 
  $left = $lo->max_storage - $c_size;
  if ($left < 0) $left = "<font color='red'>0.00</font>";;
  $size_msg .= ($size_msg==""? "":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") . ($intranet_session_language=="en"? "" : $i_Campusquota_used) ."$i_eClassStorage". ($intranet_session_language=="en"? " ".$i_Campusquota_used:"") .": $c_size / $lo->max_storage ";
}
else
{
  $size_msg .= ($size_msg==""? "":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") . ($intranet_session_language=="en"? "" : $i_Campusquota_used) ."$i_eClassStorage". ($intranet_session_language=="en"? " ".$i_Campusquota_used:"") .": $c_size ";
}

### Title ###
$title_table = "<table width='100%' cellpadding='0' cellspacing='0' border='0'>";
$title_table .= "<tr>";
$title_table .= "<td class='contenttitle' style='vertical-align: bottom;'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_management.gif' align='absmiddle' /> $i_frontpage_menu_eclass_mgt</td>";
$title_table .= "<td class='tabletext' style='vertical-align: bottom;' align='right'>$size_msg</td>";
$title_table .= "</tr>";
$title_table .= "</table>";
$TAGS_OBJ[] = array($title_table,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

# Get the Subject Groups which students belong to currently in this course
$classTitle = "c.ClassTitle".strtoupper($intranet_session_language);
$sql = "select $classTitle as ClassTitle, c.SubjectGroupID, count(distinct u.UserID)+count(distinct ut.UserID) as no_user
		from {$intranet_db}.SUBJECT_TERM_CLASS as c 
		inner join {$intranet_db}.SUBJECT_TERM as t on
    		t.YearTermID = '".$currentYearTermID."' and t.SubjectGroupID = c.SubjectGroupID 
		inner join {$eclass_db}.user_course  as uc on
			uc.course_id = c.course_id and uc.course_id = '$course_id'
		Left join {$intranet_db}.INTRANET_USER as i on
			i.UserEmail = uc.user_email
		Left join {$intranet_db}.SUBJECT_TERM_CLASS_USER as u
			on u.SubjectGroupID = c.SubjectGroupID 
			and i.UserID = u.UserID
		Left join {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER as ut
			on ut.SubjectGroupID = c.SubjectGroupID 
			and i.UserID = ut.UserID 
		group by c.SubjectGroupID "; 
//echo $sql;
$result = $lo->returnArray($sql);

# Get Total number of user in this course 
$sql = "select count(*) from {$eclass_db}.user_course where course_id = $course_id and status is null";
$total = $lo->returnVector($sql);

/*
# Get the current Subject Groups which is mapping to this course
$linkedSubGroupArr = array();
if(count($linkedSubjectGroupIDArr) > 0){
	$classTitle = "t.ClassTitle".strtoupper($intranet_session_language);
	$sql = "select distinct z.ClassTitle, z.SubjectGroupID, count(distinct z.UserID) as no_user 
			from 
			(
			    select distinct $classTitle as ClassTitle, t.SubjectGroupID, ct.UserID, ti.UserEmail  
			    from {$intranet_db}.SUBJECT_TERM_CLASS as t 
			    inner join {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER as ct on ct.SubjectGroupID = t.SubjectGroupID 
			    inner join {$intranet_db}.INTRANET_USER as ti on ct.UserID = ti.UserID and ti.RecordStatus = 1
			    where t.SubjectGroupID in (".implode(",", $linkedSubjectGroupIDArr).")
			    UNION ALL
			    select distinct $classTitle as ClassTitle, t.SubjectGroupID, cu.UserID, si.UserEmail  
			    from {$intranet_db}.SUBJECT_TERM_CLASS as t 
			    inner join {$intranet_db}.SUBJECT_TERM_CLASS_USER as cu on cu.SubjectGroupID = t.SubjectGroupID 
			    inner join {$intranet_db}.INTRANET_USER as si on cu.UserID = si.UserID and si.RecordStatus = 1
			    where t.SubjectGroupID in (".implode(",", $linkedSubjectGroupIDArr).")
			) as z 
			left join {$eclass_db}.user_course as u on 
			    u.user_email = z.UserEmail and u.course_id = '".$course_id."' 
			where u.user_email is not null 
			group by z.SubjectGroupID ";
	//echo $sql;
	$linkedSubGroupArr = $lo->returnArray($sql);
}

$groupList  = $Lang['SysMgr']['Homework']['SubjectGroup'].': '.
$groupList .= "<select name='subjectGroupID' id='subjectGroupID' onchange='this.form.submit()' >";
$groupList .= '<option value="" >'.$Lang['Btn']['All'].' ('.$total[0].')</option>';
# optgroup for subject group which linked to course directly in course table 
if(count($linkedSubGroupArr) > 0){
	$groupList .= '<optgroup label="'.$Lang['SysMgr']['SubjectClassMapping']['subjectGrpAssignedToCourse'].'">';
	for($i=0 ; $i<count($linkedSubGroupArr) ; $i++){
		$linkedSGID 	= $linkedSubGroupArr[$i]['SubjectGroupID'];
		$linkedCT 		= $linkedSubGroupArr[$i]['ClassTitle'];
		$linkedNumUser  = $linkedSubGroupArr[$i]['no_user'];
		$linkedSelected = ($withSGPrefix == true && $linkedSGID == $subjectGroupID) ? 'selected="selected"' : '';
		$groupList .= '<option value="A_'.$linkedSGID.'" '.$linkedSelected.'>'.$linkedCT.' ('.$linkedNumUser.')</option>';
	}
	$groupList .= '</optgroup>';
}
# optgroup for subject group which course students do belong to now
if(count($result) > 0){
	$groupList .= '<optgroup label="'.$Lang['SysMgr']['SubjectClassMapping']['currentSubGrpDetectedFromStudents'].'">';
	foreach($result as $r){	
		$groupList .= '<option value="'.$r["SubjectGroupID"].'" '.(!$withSGPrefix && $subjectGroupID==$r["SubjectGroupID"]?'selected':'').'>'.$r["ClassTitle"].' ('.$r["no_user"].')</option>';
	}
	$groupList .= '</optgroup>';
}
$groupList .= '<option value="-1"  '.($subjectGroupID==-1?'selected':'').' >'.$Lang['SysMgr']['SubjectClassMapping']['noneOfTheAbove'].'('.($total[0]-count($allSubjUser)).')</option>';
$groupList .= "</select>";
*/

$PAGE_NAVIGATION = $lo->course_code . " " . $lo->course_name;

?>

<br />
<form name="form1" method="get" action="index.php">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION) ?></td>
    </tr>
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext" align="center">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
<? if ($isAccessGained) {?>
                <tr>
                  <td>
                    <table border="0" cellspacing="0" cellpadding="2">
                      <tr>
                        <td><p><?=$ImportBtn?></p></td>
                      </tr>
                    </table>
                  </td>
                  <td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
                </tr>
                <tr>
                  <td align="left" valign="bottom"><?=$groupList?><?=$searchTag?></td>
                  <td align="right" valign="bottom" height="28">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
                        <td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
                          <table border="0" cellspacing="0" cellpadding="2">
                            <tr>
							<td nowrap><?=$transBtn?></td>
							<td>
							<img width="5" src="/images/2009a/10x10.gif"/>
							</td>
                              <td nowrap><?=$delBtn?></td>
                            </tr>
                          </table>
                        </td>
                        <td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="2" align="center">
                    <?=$display?>
                  </td>
                </tr>
<? } else {?>
                <tr>
                  <td colspan="2" align="center" class="tabletext">
                    <br><br>Sorry, you don't have the access right to add user to this course!
                  </td>
                </tr>
<? } ?>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td>        
        <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
            <? 
            if ($isAccessGained) {
            	echo $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location='../?roomType=".$roomType."';","cancelbtn");
            } else {
              	echo $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back();","cancelbtn");
            } ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>        
  </table>
  <br />
  
  <input type="hidden" name="course_id" value="<?php echo $lo->course_id; ?>" />
  <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
eclass_closedb();
?>
