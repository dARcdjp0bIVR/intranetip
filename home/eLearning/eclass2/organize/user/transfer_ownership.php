<?php
/*
 * modifying by
 * Modification Log: 
 * 		2011-03-02 (Yuen)
 * 		- improved to display teacher and assistant according to membertype in eclass_file
 * 
 * 		2010-11-02 (Jason)
 * 		- improved to display Deleted User including (Teacher and Assistance) in File Own Teacher List
 */
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

$specialClassroom = true;

# eClass
$lo = new libeclass($course_id);
$roomType = $lo->getEClassRoomType($course_id);

$lelcass = new libeclass2007($course_id, $roomType, $specialClassroom);

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])
{
  if ($lu->RecordType!=1|| !$la->isAccessEClassMgt())
  {
    header("Location: /");
    exit;
  }
}
# verify user right
// get right of creator of the course / admin
$sql = "SELECT course_id FROM course where course_id='$course_id' ";
$sql .= ($_SESSION['SSV_USER_ACCESS']['eLearning-eClass'])? " LIMIT 1 ":" AND creator_id='$UserID'  LIMIT 1"; // Admin can manage any course
$row = $lo->returnArray($sql, 1);
$isAccessGained = ($row[0][0]!="");
if (!$isAccessGained)
{
  // get right of teacher identity in the course
  $sql = "SELECT course_id FROM user_course where user_email='".$lu->UserEmail."' AND memberType='T' AND course_id='$course_id' LIMIT 1";
  $row = $lo->returnArray($sql, 1);
  $isAccessGained = ($row[0][0]!="");
}
$isAccessGained = ($isAccessGained && $lelcass->checkeSpecialRoomtAccessPermission(false)) ? true : false;

$eclass_3orup = ($eclass_version >= 3.0);  //is_dir("$eclass_filepath/system/");

$files_dir = ($eclass_3orup? "files":"file");

$dir_prefix = $lo->db_prefix;

$dir_list = array(
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/notes",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/reference",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/glossary",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/assignment",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/question",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/public",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/group");

$lf = new libfilesystem();

if ($eclass_version < 3.0)
{
  for($i=0; $i<sizeof($dir_list); $i++){
    $storage[$i] = $lf->folder_size($dir_list[$i]);
  }
  $c_size = 0;
  $c_files = 0;
  $c_dirs = 0;

  for($i=0; $i<sizeof($storage); $i++){
    $size = $storage[$i];
    $c_size += $size[0];
    $c_files += $size[1];
    $c_dirs += $size[2];
  }
  $c_size = number_format(($c_size/1048576),2, ".", "");
}
// eclass 3.0
if ($eclass_version >= 3.0) {
  $li = new libdb();
  $li->db = $eclass_prefix."c".$course_id;
  $sql = "SELECT SUM(size) FROM eclass_file WHERE VirPath is NULL AND Category<>9";
  $row = $li->returnArray($sql, 1);
  $c_size = ($row[0][0]!="") ? number_format(($row[0][0]/1024),2, ".", "") : 0;
}
if ($lo->max_user!="")
{
  $user_quota_left = ($lo->ticketUser()>0) ? $lo->ticketUser() : "<font color='red'>".$lo->ticketUser()."</font>";
  $size_msg .= "$i_eClassNumUsers : $lo->no_users / $lo->max_user ";
}
if ($lo->max_storage!="")
{
  $left = $lo->max_storage - $c_size;
  if ($left < 0) $left = "<font color='red'>0.00</font>";;
  $size_msg .= ($size_msg==""? "":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") . ($intranet_session_language=="en"? "" : $i_Campusquota_used) ."$i_eClassStorage". ($intranet_session_language=="en"? " ".$i_Campusquota_used:"") .": $c_size / $lo->max_storage ";
}
else
{
  $size_msg .= ($size_msg==""? "":"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") . ($intranet_session_language=="en"? "" : $i_Campusquota_used) ."$i_eClassStorage". ($intranet_session_language=="en"? " ".$i_Campusquota_used:"") .": $c_size ";
}


$ec_db_name = $lo->db_prefix."c$course_id";
//$name_separator = ($intranet_session_language=="en") ? " " : "";


############## GET TEACHER LIST WHO HAVE FILES ###############
$sql = "SELECT u.user_id, CONCAT(LTRIM(CONCAT(if(u.lastname IS NULL, '', u.lastname), ' ', if(u.firstname IS NULL, '', u.firstname))), if(u.status='deleted', ' ($i_SMS_Deleted)', '')) as t_name ";
$sql .= "FROM ".$ec_db_name.".usermaster as u ";
$sql .= ", ".$ec_db_name.".eclass_file as f ";
$sql .= "WHERE (u.memberType in ('T', 'A') OR f.memberType in ('T', 'A'))";
$sql .= "AND (u.user_id=f.User_id OR u.user_email=f.UserEmail) ";
$sql .= "GROUP BY u.user_id ORDER BY t_name ";// echo $sql;
$row_r = $lo->returnArray($sql, 2);

$teacher_removed_list = "<select name='teacher_removed[]' size=5 multiple>\n";
for ($i=0; $i<sizeof($row_r); $i++)
{
	$teacher_removed_list .= "<option value='".$row_r[$i][0]."'>".$row_r[$i][1]."</option>\n";
}
$teacher_removed_list .= "</select>\n";
if (sizeof($row_r)>0)
{
	// show submit button if deleted teacher existing with files
	$submit_button = "<input type=submit class=submit value='$button_submit'>";
}


######################### GET EXISTING TEACHER LIST #########################
$sql = "SELECT user_id, LTRIM(CONCAT(if(lastname IS NULL, '', lastname), ' ', if(firstname IS NULL, '', firstname))) as t_name, user_email ";
$sql .= "FROM ".$ec_db_name.".usermaster ";
$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') ";
$sql .= "ORDER BY t_name ";
$row_e = $lo->returnArray($sql, 3);

$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
$sql .= "FROM INTRANET_USER ";
$sql .= "WHERE RecordType = 1 ";
$sql .= "ORDER BY EnglishName ";
$li = new libdb();
$row_i = $li->returnArray($sql, 4);

$teacher_exist_list = "<select name='teacher_benefit'>\n";
$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_exist_teacher." --&nbsp;</option>\n";
$cours_teacher_size = sizeof($row_e);
$exclus_email = array();
for ($i=0; $i<$cours_teacher_size; $i++)
{
	$teacher_exist_list .= "<option value='".$row_e[$i][0]."'>".$row_e[$i][1]."</option>\n";
	$exclus_email[] = trim($row_e[$i][2]);
}
$teacher_exist_list .= "<option value=''>____________________</option>";
$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_not_exist_teacher." --&nbsp;</option>\n";
for ($i=0; $i<sizeof($row_i); $i++)
{
	if (!in_array($row_i[$i][3], $exclus_email))
	{
		$chiname = ($row_i[$i][2]==""? "": "(".$row_i[$i][2].")" );
		$teacher_exist_list .= "<option value='".$row_i[$i][0]."'>".$row_i[$i][1]." $chiname</option>\n";
	}
}

$teacher_exist_list .= "</select>\n";




### Title ###
$title_table = "<table width='100%' cellpadding='0' cellspacing='0' border='0'>";
$title_table .= "<tr>";
$title_table .= "<td class='contenttitle' style='vertical-align: bottom;'><img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_management.gif' align='absmiddle' /> $i_frontpage_menu_eclass_mgt</td>";
$title_table .= "<td class='tabletext' style='vertical-align: bottom;' align='right'>$size_msg</td>";
$title_table .= "</tr>";
$title_table .= "</table>";
$TAGS_OBJ[] = array($title_table,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION = "<a href='index.php?course_id=$course_id' class='headerLink'>".$lo->course_code . " " . $lo->course_name."</a>".displayArrow().$i_ec_file_assign;
?>
<style type="text/css">
	a.headerLink:visited{
		color:#006600
	}
	a.headerLink:hover{
		color:#FF0000
	}
</style>
<script language="javascript">
function checkform(obj,evt){
	if (evt)
		evt.preventDefault();
	if (typeof(obj.teacher_benefit)!="undefined"){
		if (obj.teacher_benefit.selectedIndex><?=$cours_teacher_size?>+2){
				obj.is_user_import.value = 1;
		}
		obj.submit();		//return true;
	}
	//return false;
}
</script>
<form name=form1 method="post" action="file_assign_update.php" onSubmit="return checkform(this,event);return false">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION) ?></td>
    </tr>
    <tr>
      <td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		  <tr>
		    <td align="left" class="tabletext" align="center">
		      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <? if($isAccessGained){ ?>
		      	<tr>
		           <td>
					<table width=500 border=0 align=center cellpadding=5>
					<tr><td width=200><u><?=$i_ec_file_remove_teacher?></u></td><td width=150>&nbsp;</td><td width=150><u><?=$i_ec_file_exist_teacher?></u></td></tr>
					<tr><td><?=$teacher_removed_list?></td>
					<td style="vertical-align:middle"><?=$i_ec_file_assign_to?> &gt;&gt;</td>
					<td style="vertical-align:middle"><?=$teacher_exist_list?></td></tr>
					<tr><td colspan=3><br>
					<?= $submit_button ?>
					<input onClick="self.location='index.php?course_id=<?=$course_id?>'" type=button class=button value="<?=$button_cancel?>">
					</td></tr>
					</table>
				  </td>
				</tr>
			<? } else {?>
			    <tr>
			      <td colspan="2" align="center" class="tabletext">
			        <br><br>Sorry, you don't have the access right to add user to this course!
			      </td>
			    </tr>
			    <tr>
			      <td>        
			        <table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
			          <tr>
			            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			          </tr>
			          <tr>
			            <td align="center">
			              <?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back();","cancelbtn") ?>
			            </td>
			          </tr>
			        </table>
			      </td>
			    </tr>
			<? } ?>
			  </table>
			</td>
		  </tr>
		</table>
	  </td>
  	</tr>
  </table>

<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type=hidden name=order>
<input type=hidden name=field>
<input type="hidden" name="is_user_import" value="0">
</form>

<?php
$linterface->LAYOUT_STOP();
eclass_closedb();
?>