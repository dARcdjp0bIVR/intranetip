<?php
/* Modify by  */
############# Change Log [Start] ################
# Date: 2015-07-14 (Jason) [ip.2.5.6.7.1.0]
#		change the wording from Status (i_UserRecordStatus) to Identity (i_eClass_Identity)
#		change the wording of Identity Selection Box 
# Date: 2010-10-21 (Thomas)
#		Separate the input parameter of function returnUserForTypeExcludeCourse for student and other user type
#
############## Change Log [End] #################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");

intranet_auth();
intranet_opendb();

# eClass
$lo = new libeclass($course_id);

# retrieve eclass record status
$x0 .= "<select name='MemberType'>\n";
$x0 .= "<option value=S ".(($MemberType=="S")?"SELECTED":"").">".$Lang['eClass']['Student']."</option>";
$x0 .= "<option value=T ".(($MemberType=="T")?"SELECTED":"").">".$Lang['eClass']['Teacher']."</option>";
$x0 .= "<option value=A ".(($MemberType=="A")?"SELECTED":"").">".$Lang['eClass']['TeachingAssistant']."</option>";
$x0 .= "</select>\n";

# retrieve group category
$li = new libgrouping();
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();
if($CatID < 0){
  unset($ChooseGroupID);
  $ChooseGroupID[0] = 0-$CatID;
}

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name='CatID' onChange='checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()'>\n" : "<select name='CatID' onChange='this.form.submit()'>\n";
$x1 .= "<option value='0'></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
  list($id,$name) = $cats[$i];
  if ($id!=0)
  {
    $x1 .= "<option value='$id' ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
  }
}
$x1 .= "<option value='Subj' ".(($CatID=='Subj')?"SELECTED":"").">".$Lang['SysMgr']['Homework']['SubjectGroup']."</option>\n";
$x1 .= "<option value='0'>";
//for($i = 0; $i < 20; $i++)
$x1 .= str_repeat("_",20);
$x1 .= "</option>\n";
$x1 .= "<option value='0'></option>\n";

$x1 .= "<option value='-1' ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
$x1 .= "<option value='-2' ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
$x1 .= "<option value='-3' ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
$x1 .= "<option value='0'></option>\n";
$x1 .= "</select>";

if($CatID!=0 && $CatID > 0 || $CatID=='Subj') {
  
  $x2  = "<select name='ChooseGroupID[]' size='5' multiple>\n";
  if ($CatID=='Subj'){
	$classTitle = "c.ClassTitle".strtoupper($intranet_session_language);
	/*$sql = "select c.SubjectGroupID,$classTitle as ClassTitle
		from {$intranet_db}.SUBJECT_TERM as t inner join 
		{$intranet_db}.SUBJECT_TERM_CLASS as c on
			t.SubjectGroupID = c.SubjectGroupID 
		inner join {$eclass_db}.user_course  as uc on
			uc.course_id = c.course_id
		where
			uc.course_id = '$course_id'
		group by c.SubjectGroupID";  //echo $sql;*/
		
	$sql = "select c.SubjectGroupID, $classTitle as ClassTitle
			from {$intranet_db}.SUBJECT_TERM as t inner join 
			{$intranet_db}.SUBJECT_TERM_CLASS as c on
				t.SubjectGroupID = c.SubjectGroupID 
			where
				 t.YearTermID in (
					select
						ayt.YearTermID
					from
						{$intranet_db}.ACADEMIC_YEAR as ay
						inner join
						{$intranet_db}.ACADEMIC_YEAR_TERM as ayt
						on ay.AcademicYearID = ayt.AcademicYearID
					where NOW() between ayt.TermStart and ayt.TermEnd
				)
			order by ClassTitle
			";	
	
	$row = $lo->returnArray($sql);
	
  }
  else{
	$row = $li->returnCategoryGroups($CatID);
  }
	for($i=0; $i<sizeof($row); $i++){
		$GroupCatID = $row[$i][0];
		$GroupCatName = $row[$i][1];
		$x2 .= "<option value='$GroupCatID'";
		for($j=0; $j<sizeof($ChooseGroupID); $j++){
		  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
		}
		$x2 .= ">$GroupCatName</option>\n";
	
  }
  $x2 .= "<option>";
  $x2 .= str_repeat("&nbsp;",40);
  $x2 .= "</option>\n";
  $x2 .= "</select>\n";
}

if($CatID < 0){
  # Return users with identity chosen
  $selectedUserType = 0-$CatID;
  //$row = $li->returnUserForTypeExcludeCourse($selectedUserType,$course_id);
  
  // Modified by thomas on 2010-10-21:
  // separate the input parameter of function returnUserForTypeExcludeCourse for student and other user type
  if($selectedUserType == USERTYPE_STUDENT)
  {
	  $name_field  = getNameFieldWithClassNumberByLang("u.");
	  $join_parent = "";
	  $order_field = "u.ClassName,u.ClassNumber,u.EnglishName";
  }
  else
  {
  	  $name_field  = "if(s.UserID='' or s.UserID is null, ".getNameFieldWithClassNumberByLang("u.")." ,".getParentNameWithStudentInfo('s.','u.').")";
  	  $join_parent = "LEFT JOIN INTRANET_PARENTRELATION as p ON (p.ParentID = u.UserID) 
				      LEFT JOIN INTRANET_USER as s on s.UserID = p.StudentID AND s.UserID!=u.UserID";
	  $order_field = "s.ClassName,s.ClassNumber,u.EnglishName";
  }
  				  
  $row = $li->returnUserForTypeExcludeCourse($selectedUserType,$course_id,'',$join_parent,$name_field,'Distinct',$order_field);
  
  $x3  = "<select name='ChooseUserID[]' size='5' multiple>\n";
  for($i=0; $i<sizeof($row); $i++)
    $x3 .= "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>\n";
  $x3 .= "<option>";
  for($i = 0; $i < 40; $i++)
    $x3 .= "&nbsp;";
  $x3 .= "</option>\n";
  $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
	if ($CatID=='Subj'){
		$sql = "select i.UserID from {$eclass_db}.user_course as u 
		inner join {$intranet_db}.INTRANET_USER as i 
		on u.user_email = i.UserEmail where u.course_id = $course_id";
		$allUser = $lo->returnVector($sql);
		
		$username_field = getNameFieldWithClassNumberByLang("i."); 
		$sql ="select i.UserID, $username_field from {$intranet_db}.INTRANET_USER as i 
		inner join {$intranet_db}.SUBJECT_TERM_CLASS_USER as u
		on i.UserID = u.UserID and SubjectGroupID in ('".implode("','",$ChooseGroupID)."') 
		where i.UserID not in ('".implode("','",$allUser)."')
		and  i.RecordStatus IN (0, 1, 2)
		group by i.UserID
		Union
		select i.UserID, $username_field from {$intranet_db}.INTRANET_USER as i 
		inner join {$intranet_db}.SUBJECT_TERM_CLASS_TEACHER as t
		on i.UserID = t.UserID and SubjectGroupID in ('".implode("','",$ChooseGroupID)."')
		where i.UserID not in ('".implode("','",$allUser)."')
		and  i.RecordStatus IN (0, 1, 2)
		group by i.UserID";
		$row = $lo->returnArray($sql);
	}
	else{
		$row = $li->returnGroupUsersExcludeCourse($ChooseGroupID,$course_id);
	}
  $x3  = "<select name='ChooseUserID[]' size='5' multiple>\n";
  for($i=0; $i<sizeof($row); $i++)
    $x3 .= "<option value='".$row[$i][0]."'>".$row[$i][1]."</option>\n";
  $x3 .= "<option>";
  for($i = 0; $i < 40; $i++)
    $x3 .= "&nbsp;";
  $x3 .= "</option>\n";
  $x3 .= "</select>\n";
}

/* Edited : 20090921 Sandy */
//$MODULE_OBJ['title'] = $lo->course_code . " " . $lo->course_name;
$MODULE_OBJ['title'] = "<div style='float:left;overflow:hidden;height:20px;' title='".$lo->course_code . " " . $lo->course_name."'>".$lo->course_code ." ".wordwrap2($lo->course_name, 30)."</div>";
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();


$PAGE_NAVIGATION[] = array($button_import, "");
?>

<script language="JavaScript1.2">
function import_update(obj){
  var total_selected = countOption(obj.elements["ChooseUserID[]"]);
  if (total_selected<=0)
  {
    alert("<?=$i_eClass_no_user_selected?>");
    return;
  }

<?php
# check if the quota enough for selected amount
  if ($lo->max_user!="")
  {
    $js_x = "var quota_left = '".$lo->ticketUser()."';\n";
    $js_x .= "if (total_selected>parseInt(quota_left))\n";
    $js_x .= "{ alert(\"$i_eClass_quota_exceeded\"); return; }\n";
    echo $js_x;
  }
?>
  obj.action = "import_update.php";
  obj.submit();
}

function SelectAll(obj)
{
  for (i=0; i<obj.length; i++)
  {
    obj.options[i].selected = true;
  }
}
</script>


<br />
<form name="form1" action="import.php" method="post">
  <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
      <td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
    </tr>
    <tr>
      <td align="center">
        <table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
          <tr valign="top">
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_eClass_Identity?> </span></td>
            <td><?=$x0;?></td>
          </tr>
          <tr valign="top">
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_frontpage_campusmail_select_category?> </span></td>
            <td><?=$x1;?></td>
          </tr>
<?php if($CatID!=0 && $CatID > 0 || $CatID=='Subj') { ?>
          <tr valign="top">
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_frontpage_campusmail_select_group?> </span></td>
            <td>
              <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td><?=$x2;?></td>
                  <td><?= $linterface->GET_BTN($i_frontpage_campusmail_expand, "submit", "checkOption(this.form.elements['ChooseGroupID[]'])", "expand_btn") ?></td>
                </tr>
              </table>
            </td>
          </tr>
<?php } ?>

<?php if(isset($ChooseGroupID)) { ?>
          <tr valign="top">
            <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_frontpage_campusmail_select_user?> </span></td>
            <td>
              <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                  <td><?=$x3;?></td>
                  <td>
                    <?=$linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);import_update(this.form);", "add_btn") ?><br>
                    <?=$linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseUserID[]']);", "select_all_btn") ?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
<? } ?>
        </table>
      </td>
    </tr>
  </table>

  <table width="95%" border="0" cellpadding="5" cellspacing="0">
    <tr>
      <td colspan="3">
        <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
          <tr>
            <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="center">
              <?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br />

  <input type="hidden" name="course_id" value="<?php echo $lo->course_id; ?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>