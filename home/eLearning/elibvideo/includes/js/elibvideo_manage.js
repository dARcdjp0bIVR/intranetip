/*
 * Editing by Charles Ma
 */ 

function close_term_page(){
	$(".fancybox-close").click();
}

function nl2br (str) { // , is_xhtml
	var breakTag = '<br />';
    //var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

var elibvideo_manage = (function(){
	this.db_path = "";
	
	return {		
		on_init_frontpage : function(type, path){					
			this.itemTotalNumber = 0, this.currentPageNumber = 1, this.itemCount = 9;
			this.orderByPublishedTime = 1, this.orderByViewCount = 0;
			this.category = "";
			this.locality = "";
			
			this.viewMode = 1;
			
			this.db_path = path;
			switch(type){
				case "retrieve_video_category_frontpage_html":
					this.filter = "category";
					
					this.retrieve_video_category_frontpage_html();
					this.retrieve_category_list_html();
					
					$("#video_locality_frontpage").parent().removeClass("current_tab",0);
					$("#video_category_frontpage").parent().addClass("current_tab",0);
					
				break;
				case "retrieve_video_locality_frontpage_html":
					this.filter = "locality";
					
					this.retrieve_video_locality_frontpage_html();
					this.retrieve_locality_list_html();
					
					$("#video_locality_frontpage").parent().addClass("current_tab",0);
					$("#video_category_frontpage").parent().removeClass("current_tab",0);
				break;
			}			
			this.attach_function_frontpage_header();
		},			
		retrieve_category_list_html: function(){  
			var current = this;
			$.post(
					this.db_path,
					{
						'task':'retrieve_category_list_html'
					},
					function(feedback){
                       if (feedback){
                        	$("#cat_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_category_control();
                       }
					}
			);				
			return;			
		},
		retrieve_locality_list_html: function(){  
			var current = this;
			$.post(
					this.db_path,
					{
						'task':'retrieve_locality_list_html'
					},
					function(feedback){
                       if (feedback){
                        	$("#cat_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_locality_control();
                       }
					}
			);				
			return;			
		},		
		retrieve_video_category_frontpage_html: function(){ 
			var current = this;
			$.post(
					this.db_path,
					{
						'task':'retrieve_video_category_frontpage_html',
						'orderByPublishedTime':this.orderByPublishedTime,
						'orderByViewCount':this.orderByViewCount,
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'category':this.category,
						'viewMode':this.viewMode
					},
					function(feedback){
                       if (feedback){
                        	$("#video_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_category();
            			    current.attach_function_videoitem();
                       }
					}
			);				
			return;			
		},
		retrieve_video_category_list_html: function(){  
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'retrieve_video_category_list_html',
						'orderByPublishedTime':this.orderByPublishedTime,
						'orderByViewCount':this.orderByViewCount,
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'category':this.category,
						'viewMode':this.viewMode
					},
					function(feedback){
                       if (feedback){
                        	$("#video_item").html(feedback).fadeOut(0).fadeIn(500);
            			    current.change_page_number();
            			    current.attach_function_videoitem();
                       }
					}
			);				
			return;			
		},
		
		retrieve_video_locality_frontpage_html: function(){ 
			var current = this;
			$.post(
					this.db_path,
					{
						'task':'retrieve_video_locality_frontpage_html',
						'orderByPublishedTime':this.orderByPublishedTime,
						'orderByViewCount':this.orderByViewCount,
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'locality':this.locality,
						'viewMode':this.viewMode
					},
					function(feedback){
                       if (feedback){
                        	$("#video_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_locality();
            			    current.attach_function_videoitem();
                       }
					}
			);				
			return;			
		},
		
		retrieve_video_locality_list_html: function(){  
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'retrieve_video_locality_list_html',
						'orderByPublishedTime':this.orderByPublishedTime,
						'orderByViewCount':this.orderByViewCount,
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'locality':this.locality,
						'viewMode':this.viewMode
					},
					function(feedback){
                       if (feedback){
                        	$("#video_item").html(feedback).fadeOut(0).fadeIn(500);
            			    current.change_page_number();
            			    current.attach_function_videoitem();
                       }
					}
			);				
			return;			
		},
		change_page_number:function(){
			var current = this;
			
			var floor = (current.currentPageNumber-1) * current.itemCount + 1;
			var ceil = (current.currentPageNumber) * current.itemCount > current.itemTotalNumber ? current.itemTotalNumber : (current.currentPageNumber) * current.itemCount;
						
			
			$("#video_page_item_ceil").html( ceil );
			$("#video_page_item_floor").html( floor );
			
			$("#video_page_option").children().each(function(){
			    if ($(this).val()==current.currentPageNumber){
			        this.selected = true;   
			    }
			});
		},
		
		attach_function_frontpage_header: function(){
			var current = this;
			$("#video_category_frontpage").bind("click tap", function(evt){
				if(current.filter == "category") return;
				
				current.filter = "category";
				current.category ="";
				current.locality ="";				
				
				var prev_selected_filter = $(".current_tab");
				prev_selected_filter.fadeOut(100).toggleClass( "current_tab", 0 ).fadeIn(200);
				$(this).parent().fadeOut(100).toggleClass( "current_tab", 0 ).fadeIn(200);
				
				current.retrieve_category_list_html();
				current.retrieve_video_category_frontpage_html();
			});
			$("#video_locality_frontpage").bind("click tap", function(evt){
				if(current.filter == "locality") return;				
				
				current.filter = "locality";
				current.category ="";
				current.locality ="";				
				
				var prev_selected_filter = $(".current_tab");
				prev_selected_filter.fadeOut(100).toggleClass( "current_tab", 0 ).fadeIn(200);
				$(this).parent().fadeOut(100).toggleClass( "current_tab", 0 ).fadeIn(200);				

				current.retrieve_locality_list_html();
				current.retrieve_video_locality_frontpage_html();
			});
			
			$("#video_favouritepage").bind("click tap", function(evt){
				document.favouritepage_form.submit();
			});

			$("#video_searchpage").bind("click tap", function(evt){
				document.searchpage_form.submit();
			});
		},
		
		attach_function_category_control:function(){
			var current = this;
			$(".category_item").bind("click tap", function(evt){
				if(current.category == $(this).attr("value")) return;
				
				current.category = $(this).attr("value");
				current.currentPageNumber = 1;
				
				var prev_selected_cat = $(".selected_cat");
				prev_selected_cat.fadeOut(100).toggleClass( "selected_cat", 0 ).fadeIn(200);
				$(this).parent().fadeOut(100).toggleClass( "selected_cat", 0 ).fadeIn(200);
				
				current.retrieve_video_category_frontpage_html();
			});
		},
		
		attach_function_locality_control:function(){
			var current = this;
			$(".locality_item").bind("click tap", function(evt){
				if(current.locality == $(this).attr("value")) return;
				
				current.locality = $(this).attr("value");
				current.currentPageNumber = 1;
				
				var prev_selected_cat = $(".selected_cat");
				prev_selected_cat.fadeOut(100).toggleClass( "selected_cat", 0 ).fadeIn(200);
				$(this).parent().fadeOut(100).toggleClass( "selected_cat", 0 ).fadeIn(200);
				
				current.retrieve_video_locality_frontpage_html();
			});
		},
		
		attach_function_category: function(){
			var current = this;
			this.itemTotalNumber = $("#itemTotalNumber").val(), this.currentPageNumber = $("#currentPageNumber").val(), this.itemCount = $("#itemCount").val();			
			
			$("#video_display_option").children().each(function(){
			    if ($(this).val()==current.itemCount){
			        this.selected = true;   
			    }
			});
			
			$("#video_page_next").bind("click tap", function(evt){
				if(current.currentPageNumber * current.itemCount >= current.itemTotalNumber) return;
				
				current.currentPageNumber++;
				current.retrieve_video_category_list_html();
			});
			
			$("#video_page_prev").bind("click tap", function(evt){
				if((current.currentPageNumber-1) * current.itemCount < 1) return;
				
				current.currentPageNumber--;
				current.retrieve_video_category_list_html();
			});
			
			$("select#video_page_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.currentPageNumber = this.value;
					current.retrieve_video_category_list_html();
			    });
			});
			
			$("select#video_display_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.itemCount = this.value;
			    	current.currentPageNumber = 1;
					current.retrieve_video_category_frontpage_html();
			    });
			});
			
			/////////////////////////////////////////////////////////////////////////////////////
			///////////////////////// Page Number Control
			///////////////////////// Ordering Control
			/////////////////////////////////////////////////////////////////////////////////////
			
			$("#viewList").unbind("click tap").bind("click tap", function(evt){
				if(current.viewMode == 2) return;
				current.viewMode = 2;
				$( "#viewCover" ).parent().switchClass( "selected", "", 400 );				
				$(this).parent().switchClass( "", "selected", 400 );
				
				current.currentPageNumber = 1;
				current.retrieve_video_category_list_html();
			});
			
			$("#viewCover").unbind("click tap").bind("click tap", function(evt){
				if(current.viewMode == 1) return;
				current.viewMode = 1;
				$( "#viewList" ).parent().switchClass( "selected", "", 400 );				
				$(this).parent().switchClass( "", "selected", 400 );
				
				current.currentPageNumber = 1;
				current.retrieve_video_category_list_html();
			});
			
			$("#orderByViewCount").unbind("click tap").bind("click tap", function(evt){
				if(current.orderByViewCount) return;
				
				$( "#orderByPublishedTime" ).switchClass( "selected_toggle", "", 400 );				
				$(this).switchClass( "", "selected_toggle", 400 );
				
				current.orderByPublishedTime = 0;
				current.orderByViewCount = 1;
				current.currentPageNumber = 1;
				current.retrieve_video_category_list_html();
			});
			
			$("#orderByPublishedTime").unbind("click tap").bind("click tap", function(evt){
				if(current.orderByPublishedTime) return;
				
				$( "#orderByViewCount" ).switchClass( "selected_toggle", "", 400 );				
				$(this).switchClass( "", "selected_toggle", 400 );
				
				current.orderByPublishedTime = 1;
				current.orderByViewCount = 0;
				current.currentPageNumber = 1;
				current.retrieve_video_category_list_html();
			});
		},
		attach_function_locality: function(){
			var current = this;
			this.itemTotalNumber = $("#itemTotalNumber").val(), this.currentPageNumber = $("#currentPageNumber").val(), this.itemCount = $("#itemCount").val();				
			
			$("#video_display_option").children().each(function(){
			    if ($(this).val()==current.itemCount){
			        this.selected = true;   
			    }
			});
			
			$("#video_page_next").bind("click tap", function(evt){
				if(current.currentPageNumber * current.itemCount >= current.itemTotalNumber) return;
				
				current.currentPageNumber++;
				current.retrieve_video_locality_list_html();
			});
			
			$("#video_page_prev").bind("click tap", function(evt){
				if((current.currentPageNumber-1) * current.itemCount < 1) return;
				
				current.currentPageNumber--;
				current.retrieve_video_locality_list_html();
			});
			
			$("select#video_page_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.currentPageNumber = this.value;
					current.retrieve_video_locality_list_html();
			    });
			});
			
			$("select#video_display_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.itemCount = this.value;
			    	current.currentPageNumber = 1;
					current.retrieve_video_locality_list_html();
			    });
			});
			
			/////////////////////////////////////////////////////////////////////////////////////
			///////////////////////// Page Number Control
			///////////////////////// Ordering Control
			/////////////////////////////////////////////////////////////////////////////////////
			
			$("#viewList").unbind("click tap").bind("click tap", function(evt){
				if(current.viewMode == 2) return;
				current.viewMode = 2;
				$( "#viewCover" ).parent().switchClass( "selected", "", 400 );				
				$(this).parent().switchClass( "", "selected", 400 );
				
				current.currentPageNumber = 1;
				current.retrieve_video_locality_list_html();
			});
			
			$("#viewCover").unbind("click tap").bind("click tap", function(evt){
				if(current.viewMode == 1) return;
				current.viewMode = 1;
				$( "#viewList" ).parent().switchClass( "selected", "", 400 );				
				$(this).parent().switchClass( "", "selected", 400 );
				
				current.currentPageNumber = 1;
				current.retrieve_video_locality_list_html();
			});
			
			$("#orderByViewCount").unbind("click tap").bind("click tap", function(evt){
				if(current.orderByViewCount) return;

				$( "#orderByPublishedTime" ).switchClass( "selected_toggle", "", 400 );				
				$(this).switchClass( "", "selected_toggle", 400 );

				
				current.orderByPublishedTime = 0;
				current.orderByViewCount = 1;
				current.currentPageNumber = 1;
				current.retrieve_video_locality_list_html();
			});
			
			$("#orderByPublishedTime").unbind("click tap").bind("click tap", function(evt){
				if(current.orderByPublishedTime) return;
				
				$( "#orderByViewCount" ).switchClass( "selected_toggle", "", 400 );				
				$(this).switchClass( "", "selected_toggle", 400 );
				
				current.orderByPublishedTime = 1;
				current.orderByViewCount = 0;
				current.currentPageNumber = 1;
				current.retrieve_video_locality_list_html();
			});
		},
		on_init_videopage: function(type, path, videoID, filter, category, locality, orderByPublishedTime, orderByViewCount, keyword){
			this.videoID = videoID, this.filter = filter;			
			this.orderByPublishedTime = orderByPublishedTime, this.orderByViewCount = orderByViewCount;			
			this.category = category;
			this.locality = locality;
			
			this.keyword = keyword.split(',');
			
			if(filter == "keyword"){
				$("#video_category_videopage").parent().removeClass("current_tab",0);
			}
			else if(filter == "category"){
				$("#video_locality_videopage").parent().removeClass("current_tab",0);
				$("#video_category_videopage").parent().addClass("current_tab",0);
			}else if(filter == "locality"){
				$("#video_locality_videopage").parent().addClass("current_tab",0);
				$("#video_category_videopage").parent().removeClass("current_tab",0);
			}else if(filter == "favourite"){
				$("#video_favouritepage").parent().addClass("current_tab",0);
				$("#video_category_videopage").parent().removeClass("current_tab",0);
			}
			
			
			this.db_path = path;
			switch(type){
				case "retrieve_videopage_html":
					this.retrieve_videopage_html();
				break;				
			}			
			this.attach_function_videopage_header();
		},
		retrieve_videopage_html : function(){
			var current = this;
			$.post(
					this.db_path,
					{
						'task':'retrieve_videopage_html',
						'orderByPublishedTime':this.orderByPublishedTime,
						'orderByViewCount':this.orderByViewCount,
						'videoID':this.videoID,
						'category':this.category,
						'locality':this.locality,
						'keyword':this.keyword,
						'filter':this.filter
					},
					function(feedback){
                       if (feedback){
                        	$("#video_page_main_content").html(feedback);
                        	current.attach_function_videopage();
                       }
					}
			);				
			return;				
		},
		retrieve_videopage_link : function(){
			var current = this;
			$.post(
					this.db_path,
					{
						'task':'retrieve_videopage_link',
						'videoID':this.videoID,
					},
					function(video_file){
                       if (video_file){
                    	   //$("#video_clip > source").attr("src", video_file);
                    	   //$('#video_clip').play();	
                    	   var myVideo = document.getElementsByTagName('video')[0];
                    	   myVideo.src = video_file;
                    	   if(current.currentTime)	myVideo.currentTime = current.currentTime;
                    	   else 
                    		   myVideo.currentTime = 0.1; //setting to zero breaks iOS 3.2, the value won't update, values smaller than 0.1 was causing bug as well.
                    	   myVideo.load();
                    	   myVideo.play();
                       }
					}
			);	
		},
		
		attach_function_videoitem:function(){
			$(".video").bind("click tap", function(evt){
				$("[name='videoID']").attr("value", $(this).attr("value"));
				document.videopage_form.submit();
			});
		},		
		attach_function_videopage : function(){
			var current = this;
//        	$("#main_content").css("margin-left","-70px").animate({
//        		"marginLeft": "0px"
//        	}, 1000);
			current.initializeVideo = 1;
			$("#script_content").fadeOut(0);
			
			$("#next_video_btn").attr("title", $("#nextVideoTitle").attr("value"));
			$("#prev_video_btn").attr("title", $("#prevVideoTitle").attr("value"));
			
			$("#summary_content").html($("#video_summary").attr("value"));
			
			//var script_file = 'script/' + $("#video_script").attr("value") + ".txt";
			//var video_file = 'video/' + $("#video_reference").attr("value") + ".mp4";
			var video_file = '' + $("#video_reference").attr("value") + "";
			var video_file_ogg = '' + $("#video_reference_ogg").attr("value") + "";
			
			this.prevVideoID =  $("#prevVideoID").attr("value");
			this.nextVideoID =  $("#nextVideoID").attr("value");
			
//			jQuery.get(script_file, function(data) {
			   //process text file line by line
				var script = $("#video_script").attr("value");
				
				//console.log(script.indexOf(' '));
				while(script && script.indexOf('\n') >0){
					script = script.replace('\n','<br>')
				}
			   $('#script_content').html(script);
//			});				 
			   //$("#video_clip > source").attr("src", video_file);
			   $("#video_item_mp4").attr("src", video_file);
			   $("#video_item_ogg").attr("src", video_file_ogg);
        	   
			   
			  if(navigator.userAgent.toLowerCase().indexOf("android") != -1){
//				  $("#video_clip").unbind("loadstart").bind("loadstart", function(evt){
//					  var myVideo = document.getElementById("video_clip");
//					  myVideo.currentTime = 20;		  
//					 });
				  
				  var video = document.getElementById("video_clip");
				  video.addEventListener('webkitfullscreenchange', function(e) {
					  	//alert("A");
					    if (document.fullScreen) {
					       /* make it look good for fullscreen */
					    } else {
					       /* return to the normal state in page */
					    }
					}, true);
				  
				  document.addEventListener("webkitfullscreenchange", function () {
					    
					}, false);
				  
				  
				  $("#video_clip").unbind("play").bind("play", function(evt){
					  var video = document.getElementById("video_clip");
					  video.currentTime = current.currentTime;
//					  var video = document.getElementById("video_clip");
//					  var myVideo = document.getElementById("video_clip");alert(current.currentTime);
//					  if(current.currentTime)	myVideo.currentTime = current.currentTime;
//               	   	  else 
//               		    myVideo.currentTime = 0.1; //setting to zero breaks iOS 3.2, the value won't update, values smaller than 0.1 was causing bug as well.		  
					 });
				  
				  $("#video_clip").unbind("pause").bind("pause", function(evt){
					  var video = document.getElementById("video_clip");
					  current.currentTime = video.currentTime;
//					  
//					  current.retrieve_videopage_link(); 
					});
				  
				  var video = document.getElementById("video_clip");
					video.play();
			  }
			 if(navigator.platform.indexOf("iPad") != -1 ){
				 $("#video_clip").unbind("play").bind("play", function(evt){

					 if(current.initializeVideo == 1)
						 current.retrieve_videopage_link(); 

					 current.initializeVideo = 0;
					 return true;
				 });
//				 
//				 //myVid=document.getElementById("video_clip");
//				 //myVid.error=alert("Starting to load video");
			 }
			 
			 $("#add_bookmark_btn").unbind("click tap").bind("click tap", function(evt){				 
					$.post(
							current.db_path,
							{
								'task':'add_favourite',
								'videoID':current.videoID
							},
							function(feedback){
		                       if (feedback){
		                    	   $(".bookmark_btn").toggle();
		                       }
							}
					);		 
			 });
				
			 $("#delete_bookmark_btn").unbind("click tap").bind("click tap", function(evt){
				 $.post(
							current.db_path,
							{
								'task':'delete_favourite',
								'videoID':current.videoID
							},
							function(feedback){
		                       if (feedback){
		                    	   $(".bookmark_btn").toggle();
		                       }
							}
					);	
			 });
			 
			 $("#video_summary_btn").bind("click tap", function(evt){
				 $(".current_tab").toggleClass( "current_tab", 0 );
				 $(this).parent().toggleClass( "current_tab", 0 );
				 
				 $("#summary_content").fadeIn(300);
				 $("#script_content").fadeOut(0);

//				 $(".video_item").fadeOut(0).animate({
//					    width: 480
//					  }, 0, function() {
//					    // Animation complete.
//					  }).fadeIn(1000);
				 
				 //$("#video_board").switchClass("video_board_script", "video_board_summary", 500);
				
			 });			 
			 
			 $("#video_script_btn").bind("click tap", function(evt){
				 $(".current_tab").toggleClass( "current_tab", 0 );
				 $(this).parent().toggleClass( "current_tab", 0 );
				 
				 $("#summary_content").fadeOut(0);
				 $("#script_content").fadeIn(300);
				 
//				 $(".video_item").fadeOut(0).animate({
//					    width: 480
//					  }, 0, function() {
//					    // Animation complete.
//					  }).fadeIn(1000);
				 
				 //$("#video_board").switchClass("video_board_summary", "video_board_script", 500);
				 
				 
			 });			 
		},
		attach_function_videopage_header: function(){
			var current = this;
			
			$("#next_video_btn").bind("click tap", function(evt){
				current.videoID = current.nextVideoID;
				$("[name=videoID]").attr("value", current.videoID);
				current.retrieve_videopage_html();
				//document.videopage_form.submit();
			});
			
			$("#prev_video_btn").bind("click tap", function(evt){
				current.videoID = current.prevVideoID;
				$("[name=videoID]").attr("value", current.videoID);
				current.retrieve_videopage_html();
				//document.videopage_form.submit();
			});
			
			$("#video_category_videopage").bind("click tap", function(evt){
				$("#frontpage_filter").attr("value", "category");
				document.frontpage_form.submit();
			});
			$("#video_locality_videopage").bind("click tap", function(evt){
				$("#frontpage_filter").attr("value", "locality");
				document.frontpage_form.submit();
			});
			
			$("#video_favouritepage").bind("click tap", function(evt){
				document.favouritepage_form.submit();
			});
			$("#video_searchpage").bind("click tap", function(evt){
				document.searchpage_form.submit();
			});
			
		},
		on_init_favouritepage : function(type, path){
			this.currentPageNumber = 1, this.itemCount = 12;
			this.db_path = path;
			switch(type){
				case "retrieve_favouritepage_html":
					this.filter = "favourite";
					this.retrieve_favouritepage_html();
					this.attach_function_favouritepage_header();
				break;
			}
		}, 
		retrieve_favouritepage_html : function(){
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'retrieve_favouritepage_html',
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber
					},
					function(feedback){
                       if (feedback){
                        	$("#video_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_favouritepage();
            			    current.attach_function_favouritevideoitem();
                       }
					}
			);				
			return;		
		},
		attach_function_favouritepage_header: function(){
			var current = this;						
			
			$("#video_category_videopage").bind("click tap", function(evt){
				$("#frontpage_filter").attr("value", "category");
				document.frontpage_form.submit();
			});
			$("#video_locality_videopage").bind("click tap", function(evt){
				$("#frontpage_filter").attr("value", "locality");
				document.frontpage_form.submit();
			});
			
			$("#video_favouritepage").bind("click tap", function(evt){
				document.favouritepage_form.submit();
			});

			$("#video_searchpage").bind("click tap", function(evt){
				document.searchpage_form.submit();
			});
		},
		attach_function_favouritepage: function(){
			var current = this;
			this.itemTotalNumber = $("#itemTotalNumber").val(), this.currentPageNumber = $("#currentPageNumber").val(), this.itemCount = $("#itemCount").val();			
			
			$("#video_display_option").children().each(function(){
			    if ($(this).val()==current.itemCount){
			        this.selected = true;   
			    }
			});
			
			$("#video_page_next").bind("click tap", function(evt){
				if(current.currentPageNumber * current.itemCount >= current.itemTotalNumber) return;
				
				current.currentPageNumber++;
				current.retrieve_favourite_list_html();
			});
			
			$("#video_page_prev").bind("click tap", function(evt){				
				if((current.currentPageNumber-1) * current.itemCount < 1) return;
				
				current.currentPageNumber--;
				current.retrieve_favourite_list_html();
			});
			
			$("select#video_page_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.currentPageNumber = this.value;
					current.retrieve_favourite_list_html();
			    });
			});		
			
			$("select#video_display_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.itemCount = this.value;
			    	current.currentPageNumber = 1;
					current.retrieve_favouritepage_html();
			    });
			});
		},
		
		retrieve_favourite_list_html: function(){  
			var current = this;			
			
			$.post(
					this.db_path,
					{
						'task':'retrieve_favourite_list_html',
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber
					},
					function(feedback){
                       if (feedback){
                        	$("#video_item").html(feedback).fadeOut(0).fadeIn(500);
            			    current.change_page_number();
            			    current.attach_function_favouritevideoitem();
                       }
					}
			);	
				
			return;			
		},
		delete_particular_favourite_video: function(){ 
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'delete_particular_favourite_video',
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'videoID': this.videoID
					},
					function(feedback){
                       if (feedback){
                        	$("#video_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_favouritepage();
            			    current.attach_function_favouritevideoitem();
            			    current.overRemoveBookmark = false;
                       }
					}
			);				
			return;					
		},		
		delete_all_favourite_video : function(){
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'delete_all_favourite_video',
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber
					},
					function(feedback){
                       if (feedback){
                        	$("#video_list").html(feedback).fadeOut(0).fadeIn(500);
                        	current.attach_function_favouritepage();
            			    current.attach_function_favouritevideoitem();
            			    current.overRemoveBookmark = false;
                       }
					}
			);				
			return;		
		},
		attach_function_favouritevideoitem:function(){			
			var current = this;
			this.overRemoveBookmark = false;
			
			$(".video").bind("click tap", function(evt){console.log(current.overRemoveBookmark);
				if(current.overRemoveBookmark) return;
				$("[name='videoID']").attr("value", $(this).attr("value"));
				document.videopage_form.submit();
			});	
			$(".btn_remove_bookmark").bind("click tap", function(evt){
				current.videoID = $(this).attr("value");
				current.delete_particular_favourite_video();
			});	
			$(".btn_remove_bookmark").bind("hover mouseover", function(evt){
				current.overRemoveBookmark = true;
			});	
			$(".btn_remove_bookmark").bind("mouseleave", function(evt){
				current.overRemoveBookmark = false;
			});	
			
			$(".btn_remove_all_bookmark").bind("click tap", function(evt){
				var result = confirm("Remove All Bookmark(s)?");
				if (result==true) {
					current.delete_all_favourite_video();
				}
			});	
		},
		
		on_init_searchpage : function(type, path, keyword, tag){
			this.currentPageNumber = 1, this.itemCount = 12;
			this.db_path = path;
			this.keyword = keyword.split(',');
			this.tag = tag;
			
			switch(type){
				case "retrieve_searchpage_html":
					this.retrieve_searchpage_html();
					this.attach_function_searchpage_header();
				break;
			}
		},
		retrieve_searchpage_html : function(){
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'retrieve_searchpage_html'
					},
					function(feedback){
                       if (feedback){
                        	$("#search_table").html(feedback).fadeOut(0).fadeIn(500);
                        	var keyword = current.keyword;
                        	keyword = keyword == "" ? current.tag : (current.tag == "" ? keyword : keyword + "," + current.tag);
                        	$("#keyword_field").val(keyword);
                        	                        	
                        	if(keyword != ""){
                        		$("#btn_search").click();
                        	}
                        	current.attach_function_searchpage_field();
                       }
					}
			);				
			return;		
		},
		attach_function_searchpage_field : function(){
			$("#btn_search_cat").bind("click tap", function(evt){
				current_category = $("input[name='category[]']:checked:enabled",'#category_option');
				
				var wording_array = new Array();
				
				$.each( current_category, function(i, l){
					wording_array.push($(current_category[i]).attr("wording"));
				});
				if(wording_array.length>0){
					$("#field_search_cat").attr("class", "selected_value");	
					$("#field_search_cat").html(wording_array.join(", "));
				}else{
					$("#field_search_cat").attr("class", "");	
					$("#field_search_cat").html($("#field_search_cat_default").val());
				}
			});
			$("#btn_search_loc").bind("click tap", function(evt){
				current_locality = $("input[name='locality[]']:checked:enabled",'#locality_option');
				
				var wording_array = new Array();
				
				$.each( current_locality, function(i, l){
					wording_array.push($(current_locality[i]).attr("wording"));
				});
				if(wording_array.length>0){
					$("#field_search_loc").attr("class", "selected_value");	
					$("#field_search_loc").html(wording_array.join(", "));
				}else{
					$("#field_search_loc").attr("class", "");	
					$("#field_search_loc").html($("#field_search_loc_default").val());
				}
			});
			
		},
		
		attach_function_searchpage_header: function(){
			var current = this;						
			
			$("#btn_nav_search").bind("click tap", function(evt){
				$(".main_content").toggle();
			});
			
			$("#btn_search_reset").bind("click tap", function(evt){
				$("#keyword_field").val("");
				$("input[name='category[]']").each(function() {
			         $(this).attr("checked", false);
			     });
				$("input[name='locality[]']").each(function() {
			         $(this).attr("checked", false);
			     });		
			});
			
			$("#video_category_videopage").bind("click tap", function(evt){
				$("#frontpage_filter").attr("value", "category");
				document.frontpage_form.submit();
			});
			$("#video_locality_videopage").bind("click tap", function(evt){
				$("#frontpage_filter").attr("value", "locality");
				document.frontpage_form.submit();
			});
			
			$("#video_favouritepage").bind("click tap", function(evt){
				document.favouritepage_form.submit();
			});

			$("#video_searchpage").bind("click tap", function(evt){
				document.searchpage_form.submit();
			});			
			
			$("#btn_search").bind("click tap", function(evt){
				current.keyword = $("[name='keyword']").val().split(',');
				
				current_category = $("input[name='category[]']:checked:enabled",'#category_option');
				
				current.category = new Array();
				
				$.each( current_category, function(i, l){
					current.category.push($(current_category[i]).attr("value"));
				});
				
				current_locality = $("input[name='locality[]']:checked:enabled",'#locality_option');
				
				current.locality = new Array();
				
				$.each( current_locality, function(i, l){
					current.locality.push($(current_locality[i]).attr("value"));
				});				

				current.retrieve_searchresultpage_html();
			});
			
		},
		retrieve_searchresultpage_html: function(){
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'retrieve_searchresultpage_html',
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'keyword':current.keyword,
						'category':current.category,
						'locality':current.locality
					},
					function(feedback){
                       if (feedback){
                        	$("#video_list").html(feedback).fadeOut(0).fadeIn(500);
                        	$(".main_content").toggle();
                        	if(!$("#itemTotalNumber").attr("wording"))
                        		$("#totalNumberFound").html("0 Found");
                        	else $("#totalNumberFound").html($("#itemTotalNumber").attr("wording"));
                        	current.attach_function_searchvideoitem();
                        	current.attach_function_searchresultpage();
                       }
					}
			);				
			return;	
		},
		attach_function_searchvideoitem:function(){			
			var current = this;
			
			$(".video").bind("click tap", function(evt){
				$("[name='videoID']").attr("value", $(this).attr("value"));
				document.videopage_form.submit();
			});	
		},
		attach_function_searchresultpage: function(){
			var current = this;
			this.itemTotalNumber = $("#itemTotalNumber").val(), this.currentPageNumber = $("#currentPageNumber").val(), this.itemCount = $("#itemCount").val();			
			
			$("#video_display_option").children().each(function(){
			    if ($(this).val()==current.itemCount){
			        this.selected = true;   
			    }
			});
			
			$("#video_page_next").bind("click tap", function(evt){				
				if(current.currentPageNumber * current.itemCount >= current.itemTotalNumber) return;
				
				current.currentPageNumber++;
				current.retrieve_searchresultlist_html();
			});
			
			$("#video_page_prev").bind("click tap", function(evt){				
				if((current.currentPageNumber-1) * current.itemCount < 1) return;
				
				current.currentPageNumber--;
				current.retrieve_searchresultlist_html();
			});
			
			$("select#video_page_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.currentPageNumber = this.value;
					current.retrieve_searchresultlist_html();
			    });
			});	
			
			$("select#video_display_option").change(function(){ 			    
			    jQuery('option:selected', this).each(function(){ 
			    	current.itemCount = this.value;
			    	current.currentPageNumber = 1;
					current.retrieve_searchresultlist_html();
			    });
			});
		},
		retrieve_searchresultlist_html: function(){
			var current = this;			
			$.post(
					this.db_path,
					{
						'task':'retrieve_searchresultlist_html',
						'itemCount':this.itemCount,
						'pageNumber':this.currentPageNumber,
						'keyword':current.keyword,
						'category':current.category,
						'locality':current.locality
					},
					function(feedback){
                       if (feedback){
                        	$("#video_item").html(feedback).fadeOut(0).fadeIn(500);
                        	if(!$("#itemTotalNumber").attr("wording"))
                        		$("#totalNumberFound").html("0 Found");
                        	else $("#totalNumberFound").html($("#itemTotalNumber").attr("wording"));
                        	 current.change_page_number();
                        	current.attach_function_searchvideoitem();
                       }
					}
			);				
		}
		,
		on_init_uploadpage : function(type, path){
			$("#btn_upload").bind("click", function(evt){
				document.update_form.submit();
				
//				$.post(
//						path,
//						{
//							'task':'uploadfile',
//							'file': $("#itemfile").val()
//						},
//						function(feedback){
//	                       if (feedback){
//	                        	console.log(feedback);
//	                       }
//						}
//				);			
			});
		}
		
	};
})();