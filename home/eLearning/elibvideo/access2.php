<?php

# to access eLIBVIDEO server

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");	
intranet_auth();
intranet_opendb();
global $intranet_session_language, $intranet_root, $UserID, $US_Intranet_IDType, $config_school_code, $eclass40_httppath;

function encrypt($str){
	$Encrypt1 = base64_encode($str);
	$MidPos = ceil(strlen($Encrypt1)/2);
	$Encrypta = substr($Encrypt1, 0, $MidPos);
	$Encryptb = substr($Encrypt1, $MidPos);

	return base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));	
}

function decrypt($str){
	$Encrypt1 = base64_decode($str);
	$SplitSizePos = strrpos($Encrypt1, "=");
	$MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
	$Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
	$Encrypta = substr($Encrypt1, 0, $MidPos);
	$Decryptb = substr($Encrypt1, $MidPos);

	return base64_decode($Decryptb.$Encrypta);
}

$lib_user = new libuser($UserID);
$SysLang = $intranet_session_language;
$SchoolCode = $config_school_code;

$eClassUserType = $lib_user->RecordType;

$eClassUserID = $UserID;
$UserNameChi = $lib_user->ChineseName;
$UserNameEng = $lib_user->EnglishName;
$UserNickname = $lib_user->NickName;

$ClassLevel = $lib_user->ClassLevel;
$ClassName = $lib_user->ClassName;
$ClassNumber = $lib_user->ClassNumber;

///$APIVersion = "";
$TimeStamp = date("Y-m-d H:i:s");

$SchoolAddress = $eclass40_httppath;

$UserEmail = $lib_user->UserEmail;

$formdata .= "<input type='hidden' name=\"SysLang\" value=\"".encrypt($SysLang)."\" />";
$formdata .= "<input type='hidden' name=\"SchoolCode\" value=\"".encrypt($SchoolCode)."\" />";
$formdata .= "<input type='hidden' name=\"eClassUserType\" value=\"".encrypt($eClassUserType)."\" />";
$formdata .= "<input type='hidden' name=\"eClassUserID\" value=\"".encrypt($eClassUserID)."\" />";
$formdata .= "<input type='hidden' name=\"UserNameChi\" value=\"".encrypt($UserNameChi)."\" />";
$formdata .= "<input type='hidden' name=\"UserNameEng\" value=\"".encrypt($UserNameEng)."\" />";
$formdata .= "<input type='hidden' name=\"UserNickname\" value=\"".encrypt($UserNickname)."\" />";
$formdata .= "<input type='hidden' name=\"ClassLevel\" value=\"".encrypt($ClassLevel)."\" />";
$formdata .= "<input type='hidden' name=\"ClassName\" value=\"".encrypt($ClassName)."\" />";
$formdata .= "<input type='hidden' name=\"ClassNumber\" value=\"".encrypt($ClassNumber)."\" />";
$formdata .= "<input type='hidden' name=\"TimeStamp\" value=\"".encrypt($TimeStamp)."\" />";
$formdata .= "<input type='hidden' name=\"SchoolAddress\" value=\"".encrypt($SchoolAddress)."\" />";
$formdata .= "<input type='hidden' name=\"UserEmail\" value=\"".encrypt($UserEmail)."\" />";
//
$url = 'index.php';

intranet_closedb();
?>

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body onLoad="document.MainForm.submit();">
	<form name="MainForm" id="MainForm" action="<?=$url?>" method="post">
		<input type='hidden' name="Mode" value="action" />
		<input type='hidden' name="Task" value="login" />
		<?=$formdata?>
	</form>
</body>

</html>
