<?php
intranet_opendb();

$Filter = isset($_REQUEST['Filter']) && $_REQUEST['Filter']!=''? $_REQUEST['Filter'] : "category";

//$UserID = $_SESSION["ELIB_VIDEO_UserID"];
//$SchoolCode = $_SESSION["ELIB_VIDEO_SchoolCode"];
//$SchoolUserID = $_SESSION["ELIB_VIDEO_SchoolUserID"];

$Language = isset($_SESSION["ELIB_VIDEO_Lang"]) && $_SESSION["ELIB_VIDEO_Lang"]!=''? $_SESSION["ELIB_VIDEO_Lang"] : "en";

$VideoID = isset($_REQUEST['videoID']) && $_REQUEST['videoID']!=''? $_REQUEST['videoID'] : "";
$Filter = isset($_REQUEST['filter']) && $_REQUEST['filter']!=''? $_REQUEST['filter'] : "category";
$Category = isset($_REQUEST['category']) && $_REQUEST['category']!=''? $_REQUEST['category'] : "";
$Locality = isset($_REQUEST['locality']) && $_REQUEST['locality']!=''? $_REQUEST['locality'] : "";
$OrderByPublishedTime = isset($_REQUEST['orderByPublishedTime']) && $_REQUEST['orderByPublishedTime']!=''? $_REQUEST['orderByPublishedTime'] : 0;
$OrderByViewCount = isset($_REQUEST['orderByViewCount']) && $_REQUEST['orderByViewCount']!=''? $_REQUEST['orderByViewCount'] : 0;
$KeyWord = isset($_REQUEST['keyword']) && $_REQUEST['keyword']!=''? addslashes($_REQUEST['keyword']) : "";

$Tag = isset($_REQUEST['tag']) && $_REQUEST['tag']!=''? $_REQUEST['tag'] : "";

$libelibvideo = new libelibvideo($UserID,$_SESSION["ELIB_VIDEO_SchoolCode"], $UserID);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/content.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="includes/js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="includes/js/jquery-ui.js"></script>
<script type="text/javascript" src="includes/js/elibvideo_manage.js"></script>
<script type="text/javascript" src="includes/js/source/jquery.fancybox.js?v=2.1.0"></script>
<link rel="stylesheet" type="text/css" href="includes/js/source/jquery.fancybox.css?v=2.1.0" media="screen">
<title>:: eClass :: Videos :: APF Channel</title>
<script type="text/javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
    
  if($("#selection_layer_02").css("visibility") == "visible" && args[0] == "selection_layer_01"){
  	$("#selection_layer_02").css("visibility", "hidden") ;
  }
  
  if($("#selection_layer_01").css("visibility") == "visible" && args[0] == "selection_layer_02"){
  	$("#selection_layer_01").css("visibility", "hidden") ;
  }
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
<script type="text/javascript">
$(document).ready(function() {	
	elibvideo_manage.on_init_searchpage('retrieve_searchpage_html', 
	'<?php echo "index.php?Mode=action&Task=elibvideo_manage" ?>', '<?=$KeyWord?>', '<?=$Tag?>');
	
	$('.fancybox').fancybox( {
	 'fitToView'    	 : false,	
	 'autoDimensions'    : false,
	 'autoScale'         : false,
	 'autoSize'          : false,
	 'width'             : 800,
	 'height'            : 600,
	 'padding'			 : 0,
	
	 'transitionIn'      : 'elastic',
	 'transitionOut'     : 'elastic',
	 'type'              : 'iframe'    }
	
	);						
});
</script>
</head>

<body>

        <div id="container">
            <!---->        
            <div class="title_header <?php if($Language == "b5") echo "title_header_logo_chn"; ?>">
                <div class="title_header_logo">
                    <a  href="index.php"  class="apf_logo"></a>
                </div>
                 <div class="terms_conditon"> 
	                <a target="_blank"  href="terms.htm" class="fancybox fancybox.iframe">
	                <?=$Lang['Common']['TermAndConditions']?></a>  
                </div>
                
            </div>
            
            <!---->
            <div class="main_board">
            	<div class="main_board_top"><div class="main_board_top_right"><div class="main_board_top_bg">
                    <div class="board_tab">
                        <ul>
                        	<li><a id="video_category_videopage" href="javascript:void(0);" class="tab_cat"><span><em></em><?=$Lang['Header']['Categories']?></span></a></li>
                            <li><a id="video_locality_videopage" href="javascript:void(0);" class="tab_local"><span><em></em><?=$Lang['Header']['Location']?></span></a></li>
                           	<li><a id="video_favouritepage" href="javascript: void(0)" class="tab_bookmark"><span><em></em><?=$Lang['Header']['MyFavourite']?></span></a></li>
                        </ul>
                         <div class="search_bar"><a id="video_searchpage" href="javascript:void(0);"  ><?=$Lang['Header']['AdvancedSearch']?></a>
		                      <input id="keyword_field_s" type="text"  placeholder="" onKeyDown="javascript:(function(){if (event.keyCode ==13) {$('#searchpage_keyword').attr('value', $('#keyword_field_s').val());document.searchpage_form.submit();} })()"  />
		                      
		                  </div>                        
                    </div>
           		 </div></div></div>
                 <div class="main_board_content"><div class="main_board_content_right"><div class="main_board_content_bg">
                 	 <!---->
                      <p class="spacer"></p>
                 	
                    	
                    	<!---->
                        <div class="main_content">
	                    	<div class="navigator">
	                        	<span class="nav_search"><?=$Lang['SearchPage']['AdvancedSearch']?></span>
	                        </div><br /><br />
							<div class="table_board">
	                             <p class="spacer"></p>
	                             <table id="search_table" class="form_table">
	                                  <!-- Fill from libelibvideo.php  -->
	                            </table>
	                            
	         				</div> <p class="spacer"></p>
                             <div class="edit_bottom">				            
				              <input name="submit3" type="button" class="formbutton" id="btn_search" value="<?=$Lang['SearchPage']['Search']?>" />
				              <input name="submit2" type="button" class="formsubbutton" id="btn_search_reset" value="<?=$Lang['SearchPage']['Reset']?>" />				             
				              </div>
                        </div>
                        
                         <div style="display:none" class="main_content">
                        	<div class="navigator">
                            	<a id="btn_nav_search" href="javascript:void(0);" class="nav_search"><?=$Lang['SearchPage']['AdvancedSearch']?></a><span id="totalNumberFound" > 0 </span>
                            </div><br /><br />
							  <div id="video_list" class="video_list">
							  
                                <!-- Fill from libelibvideo.php -->                                
                                	
						   	</div>	
                        </div>
                        <form method="GET" action="" name="favouritepage_form" id="favouritepage_form">
		                 	<input name="Mode"  value="content" type="hidden"/>
		                 	<input name="Task"  value="favouritepage" type="hidden"/>
		                 </form>
		                 <form method="GET" action="" name="frontpage_form" id="frontpage_form">
							<input name="Mode"  value="content" type="hidden"/>
							<input name="Task"  value="frontpage" type="hidden"/>
							<input name="Filter"  value="" type="hidden" id="frontpage_filter" />
						</form>
						<form method="GET" action="" name="searchpage_form" id="searchpage_form">
		                 	<input name="Mode"  value="content" type="hidden"/>
		                 	<input name="Task"  value="searchpage" type="hidden"/>
		                 	<input name="tag" 		id="searchpage_tag" value="" type="hidden"/>
	                 		<input name="keyword" 	id="searchpage_keyword" value="" type="hidden"/>
		                 </form>
                        <p class="spacer"></p>
                        <!---->
                    
                 
                 </div></div></div>
            </div>
            <!---->
            <p class="spacer"></p>
            <div class="footer">Powered by <a href="#" title="eClass"></a></div>
            <!---->
         </div>
         
</body>
</html>

<?php
	intranet_closedb();
?>
