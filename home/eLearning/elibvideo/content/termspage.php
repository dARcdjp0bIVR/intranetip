<?php
intranet_opendb();

$loginArr["SysLang"] = isset($_REQUEST['SysLang']) && $_REQUEST['SysLang']!=''? $_REQUEST['SysLang'] : "en";
$loginArr["SchoolCode"] = isset($_REQUEST['SchoolCode']) && $_REQUEST['SchoolCode']!=''? $_REQUEST['SchoolCode'] : 0;

$loginArr["eClassUserType"] = isset($_REQUEST['eClassUserType']) && $_REQUEST['eClassUserType']!=''? $_REQUEST['eClassUserType'] : 0;

$loginArr["eClassUserID"] = isset($_REQUEST['eClassUserID']) && $_REQUEST['eClassUserID']!=''? $_REQUEST['eClassUserID'] : 0;
$loginArr["UserNameChi"] = isset($_REQUEST['UserNameChi']) && $_REQUEST['UserNameChi']!=''? $_REQUEST['UserNameChi'] : 0;
$loginArr["UserNameEng"] = isset($_REQUEST['UserNameEng']) && $_REQUEST['UserNameEng']!=''? $_REQUEST['UserNameEng'] : 0;
$loginArr["UserNickname"] = isset($_REQUEST['UserNickname']) && $_REQUEST['UserNickname']!=''? $_REQUEST['UserNickname'] : 0;

$loginArr["ClassLevel"] = isset($_REQUEST['ClassLevel']) && $_REQUEST['ClassLevel']!=''? $_REQUEST['ClassLevel'] : 0;
$loginArr["ClassName"] = isset($_REQUEST['ClassName']) && $_REQUEST['ClassName']!=''? $_REQUEST['ClassName'] : 0;
$loginArr["ClassNumber"] = isset($_REQUEST['ClassNumber']) && $_REQUEST['ClassNumber']!=''? $_REQUEST['ClassNumber'] : 0;

//$APIVersion = "";
$loginArr["TimeStamp"] = isset($_REQUEST['TimeStamp']) && $_REQUEST['TimeStamp']!=''? $_REQUEST['TimeStamp'] : 0;

$loginArr["SchoolAddress"] = isset($_REQUEST['SchoolAddress']) && $_REQUEST['SchoolAddress']!=''? $_REQUEST['SchoolAddress'] : 0;

$loginArr["UserEmail"] = isset($_REQUEST['UserEmail']) && $_REQUEST['UserEmail']!=''? $_REQUEST['UserEmail'] : 0;

//$UserID = $_SESSION["ELIB_VIDEO_UserID"];
//$SchoolCode = $_SESSION["ELIB_VIDEO_SchoolCode"];
//$SchoolUserID = $_SESSION["ELIB_VIDEO_SchoolUserID"];

$Language = isset($_SESSION["ELIB_VIDEO_Lang"]) && $_SESSION["ELIB_VIDEO_Lang"]!=''? $_SESSION["ELIB_VIDEO_Lang"] : "en";


$readUnderstandTerm = isset($_REQUEST['readUnderstandTerm']) && $_REQUEST['readUnderstandTerm']!=''? $_REQUEST['readUnderstandTerm'] : "0";
$acceptTerm = isset($_REQUEST['acceptTerm']) && $_REQUEST['acceptTerm']!=''? $_REQUEST['acceptTerm'] : "0";

$libelibvideo = new libelibvideo($UserID,$_SESSION["ELIB_VIDEO_SchoolCode"], $UserID);

$libelibvideo->checkIfAcceptTerm($readUnderstandTerm, $acceptTerm, $loginArr);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Terms & Condition</title>
<script type="text/javascript">
<!--
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}
//-->
</script>
</head>
<link href="css/content.css" rel="stylesheet" type="text/css" />
<body class="bg_terms">
	<div class="terms_title"><h1>Terms and Conditions 使用者條款及細則</h1></div>
		<?php echo $libelibvideo->retrieve_termpage_html();?>
        <p class="spacer"></p>
            <div class="edit_bottom">
            	<?php echo $libelibvideo->retrieve_termpageform_html($loginArr); ?>         
  			</div>
    </div>
</body>
</html>

<?php
	intranet_closedb();
?>