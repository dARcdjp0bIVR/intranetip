<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("elib_script.php");
###########################################################
$objInstall	= new elibrary_install();

##Get Post Variables
$aryStudentName 	= isset($_REQUEST['aryStudentName'])? $_REQUEST['aryStudentName'] : "";
$aryStudentID 	= isset($_REQUEST['aryStudentID'])? $_REQUEST['aryStudentID'] : "";
$BookLicenseID 	= isset($_REQUEST['BookLicenseID'])? trim($_REQUEST['BookLicenseID']) : "";
$BookID 		= isset($_REQUEST['BookID'])? trim($_REQUEST['BookID']) : "";

$arySucc = array();
$aryFail = array();

## Loop Through list of students
foreach($aryStudentID as $i=>$StudentID){
	if(!empty($StudentID)){
		if($objInstall->add_book_student($BookLicenseID,$BookID,$StudentID)){
			//$arySucc[$i] = $aryStudentName[$i];
			$arySucc[$i] = array($eLib['admin']['Licensed_Students']=>$aryStudentName[$i]);
		}else{										
			//$aryFail[$i] = $aryStudentName[$i];
			$aryFail[$i] = array($eLib['admin']['Licensed_Students']=>$aryStudentName[$i]);
		}		
	}
}
$succ_fail_header	= $eLib["html"]["student_name"];
		 
## Echo succ & fail results				 
//echo $objInstall->gen_result_ui($succ_fail_header, $arySucc, $aryFail);
echo $objInstall->gen_result_ui($arySucc, $aryFail, true, "828");




###########################################################
intranet_closedb();
?>