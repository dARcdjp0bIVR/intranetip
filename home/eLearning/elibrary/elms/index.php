<?php
// Modifing by key

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libkoha.php");

intranet_auth();
intranet_opendb();

if (!$plugin["koha"])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$usr = new libuser($UserID);
$koha = new libkoha();

intranet_closedb();
$koha->db_connect();

$sql = "select * from import_biblios";
$row = $koha->returnArray($sql);

$tableArr = array();
$tableArr[] = "borrowers";
$tableArr[] = "biblioitems";
$tableArr[] = "deletedbiblioitems";
$tableArr[] = "import_biblios";
$tableArr[] = "items";
$tableArr[] = "labels_conf";
$tableArr[] = "tmp_holdsqueue";

function getTabMenu($tableArr="")
{
	$x = "";
	for($i = 0; $i < count($tableArr); $i++)
	{
		$n = $tableArr[$i];
		$x .= "<li id=\"".$n."\"><a href=\"#\" title=\"".$n."\">".$n."</a></li>";
	}
	echo $x;
}

//$koha->debug_t($row);

$koha->db_close();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="niftycube.js"></script>
<script src="jquery.min.js" type="text/javascript"></script>
 <script>
  // When the document loads do everything inside here ...
     $(document).ready(function(){
	 Nifty("#menu a","small top transparent");
	 Nifty("#outcontent","medium bottom transparent");
	 $('.content').load('query.php');	//by default initally load text from boo.php
         $('#menu a').click(function() { //start function when any link is clicked
		 				$(".content").slideUp("slow");
						 var content_show = $(this).attr("title"); //retrieve title of link so we can compare with php file
							$.ajax({
							method: "get",url: "query.php",data: "page="+content_show,
							beforeSend: function(){$("#loading").show("fast");}, //show loading just when link is clicked
							complete: function(){ $("#loading").hide("fast");}, //stop showing loading when the process is complete
							success: function(html){ //so, if data is retrieved, store it in html
							$(".content").show("slow"); //animation
							$(".content").html(html); //show the html inside .content div
					 }
				 }); //close $.ajax(
         }); //close click(
	 }); //close $(
</script>
<style type="text/css">
.page { margin:10px auto 0 auto; width:100%;}
#menu  { list-style:none; margin:0px; padding:0px;}
#menu li { list-style:none; display:inline; }
li.active a { background-color:#FFF;color:#000; }
#menu li a,#menu li a:link { float:left; background-color:#3c3c3c; margin-right:5px; padding:5px; color:#FFFFFF; text-decoration:none; width:7em; text-align:center;} 
#menu li a:visited { }
#menu li a:hover { background-color:#327cc8 }
#menu li a:active { background-color:#FFF;color:#000; }
#loading { clear:both; background:url(wait.gif) center top no-repeat; text-align:center;padding:33px 0px 0px 0px; font-size:12px;display:none; font-family:Verdana, Arial, Helvetica, sans-serif; }
#outcontent {clear:both; background-color:#FFFFFF; }
.content { 
background-color:#FFF; bottom right no-repeat; padding:10px; height:300px; margin:0px; }
</style>
</head>
<body>
<div class="page">
<div>
<ul id="menu" nowrap>  
    <?php echo getTabMenu($tableArr); ?>
</ul>
<br style="clear:both;" />
</div>
<div id="outcontent">
<div id="loading">LOADING</div>
<div class="content"></div>
</div>
</div>
</body>
</html>
