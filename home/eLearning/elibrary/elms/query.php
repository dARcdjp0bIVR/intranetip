<?php

header("Cache-Control: no-cache");
sleep(1);

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libkoha.php");

intranet_auth();
intranet_opendb();

if (!$plugin["koha"])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$p = $_GET['page'];
if($p == "")
$p = "borrowers";

$li = new libdb();
$koha = new libkoha();

if($p == "import_biblios")
{
	$ip_sql = "select * from INTRANET_ELIB_BOOK";
	$ip_row = $li->returnArray($ip_sql);
	
	$BookArr = array();
	for($i=0; $i < count($ip_row); $i++)
	{
		$BookArr[] = array(
		"BookTitle"=>$ip_row[$i]["Title"], 
		"Author"=>$ip_row[$i]["Author"], 
		"ISBN"=>$ip_row[$i]["ISBN"], 
		"SystemSource"=>"eClass");
	}
}

intranet_closedb();
$koha->db_connect();

$sql = "select * from $p";
$row = $koha->returnArray($sql);

if($p == "import_biblios")
{
	for($i=0; $i < count($row); $i++)
	{
		$BookArr[] = array(
		"BookTitle"=>$row[$i]["title"], 
		"Author"=>$row[$i]["author"], 
		"ISBN"=>$row[$i]["isbn"], 
		"SystemSource"=>"Koha");
	}
	$koha->debug_t($BookArr);
	//$koha->debug_t($row);
}
else
{
	$koha->debug_t($row);
}

$koha->db_close();
?>