<?
////////////////////////////////////////////////////////////////////////////
/////////// prest set list array //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
// $listArr["lang"]["category"]["subcategory"] 

$returnArr1 = $lelib->getCountCategory($ParArr);
$returnArr2 = $lelib->getCountSubCategory($ParArr);

for($r = 0;  $r < count($returnArr1) ; $r++)
{
	$Language = $returnArr1[$r]["Language"];
	//$tmpCategory = iconv("utf-8","big-5",$returnArr1[$r]["Category"]);
	$tmpCategory = $returnArr1[$r]["Category"];
	$count_category = $returnArr1[$r]["count_category"];
	
	for($x = 0; $x < count($catListArr[$Language]) ; $x++)
	{
		if($catListArr[$Language][$x]["name"] == $tmpCategory)
		$catListArr[$Language][$x]["total_count"] = $count_category;
	}
}

for($r = 0;  $r < count($returnArr2) ; $r++)
{
	$Language = $returnArr2[$r]["Language"];
	//$tmpSubCategory = iconv("utf-8","big-5",$returnArr2[$r]["SubCategory"]);
	$tmpSubCategory = $returnArr2[$r]["SubCategory"];
	$count_subcategory = $returnArr2[$r]["count_subcategory"];
	
	for($x = 0; $x < count($catListArr[$Language]) ; $x++)
	{
		for($y = 0; $y < count($catListArr[$Language][$x]["subname"]) ; $y++)
		{
			if($catListArr[$Language][$x]["subname"][$y] == $tmpSubCategory)
			$catListArr[$Language][$x]["count"][$y] = $count_subcategory;
		}
	}
}
?>

<script language="JavaScript" type="text/JavaScript">
<!--

function MM_showRecommendedBook(getBookNum) {
	curBook=getBookNum;
	//document.getElementById("bookPhpID").href = "book_detail.php?BookID="+recomBooks[getBookNum].path;
	document.getElementById("bookCover").src = "/file/elibrary/content/"+recomBooks[getBookNum].path+"/image/cover.jpg";
	document.getElementById("bookTitle").innerHTML =  recomBooks[getBookNum].title;
	document.getElementById("bookReason").innerHTML = "<strong><?=$eLib["html"]["recommended_reason"]?>"+":</strong><br>"+recomBooks[getBookNum].description;
	
}

function openRecommendedBook()
{	
	MM_openBrWindowFull("book_detail.php?BookID="+recomBooks[curBook].path, 'bookdetail','features');
}

function onclickPreviousBook(){
	curBook--; 
	if(curBook<0){
		curBook=recomBooks.length-1;
	}
	
	MM_showRecommendedBook(curBook);
}
function onclickNextBook(){

	curBook++;
	if(curBook==recomBooks.length){
		curBook=0;
	}

	MM_showRecommendedBook(curBook);
}

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_openBrWindowFull(theURL,winName,features) { //v2.0

  var is_swf = theURL.indexOf('tool/index.php?BookID=');
  
  if(is_swf != -1)
  {
  params  = 'width='+screen.width;
  params += ', height='+screen.height;
  params += ', top=0, left=0'
  params += ', fullscreen=yes';
  params += ', ' + features;
  window.open(theURL,winName,params);
	}
  else
  {
  params  = 'width=700';
  params += ', height=450';
  params += ', top=0, left=0'
  params += ', fullscreen=no';
  params += ', ' + features;
  window.open(theURL,winName,params);
	}
}

function resetPage()
{
	// refresh the content
	location.href = "index.php";
} // end function reset page

function changeTagLanguage(type)
{
	ChangeCatTabList(type);
	
	if(type == "eng")
	{
		showEnglishBookList();
	}
	else if(type == "chi")
	{
		showChineseBookList();
	}
} // end function change Tag lanuage

function showEnglishBookList()
{
	var obj = document.all["CatList"];
	for(var i = obj.rows.length - 1 ; i >= 0 ; i--)
	obj.deleteRow(i);
	
	// set list array
	var CatArr = new Array();
	var CatCountArr = new Array();
	
	<?
	for($i = 0; $i < count($catListArr["eng"]); $i++)
	{
		$x = "CatArr[".$i."] = \"".$catListArr["eng"][$i]["name"]."\";";
		$y = "CatCountArr[".$i."] = \"".$catListArr["eng"][$i]["total_count"]."\";";
		echo $x;
		echo $y;
	}
	?>
	
	var SubCatArr = new Array();
	var SubCatCountArr = new Array();
	
	<?
	for($j = 0; $j < count($catListArr["eng"]); $j++)
	{
		$y = "SubCatArr[".$j."] = new Array();";
		$z = "SubCatCountArr[".$j."] = new Array();";
		echo $y;
		echo $z;
		
		for($i = 0; $i < count($catListArr["eng"][$j]["subname"]); $i++)
		{
			$x = "SubCatArr[".$j."][".$i."] = \"".$catListArr["eng"][$j]["subname"][$i]."\";";
			$a = "SubCatCountArr[".$j."][".$i."] = \"".$catListArr["eng"][$j]["count"][$i]."\";";
			echo $x;
			echo $a;
		}
	}
	?>
	// end set prelist
	
var str = "";
var str2 = "";
for(var j = 0; j < CatArr.length; j++)
{
var BookCount = CatCountArr[j];


if(BookCount > 0)
str2 += "<div id=\"elib_cata_head\"><a href=\"elib_catalogue_detail.php?Category="+CatArr[j]+"\">"+CatArr[j]+"</a></div>";
else
str2 += "<div id=\"elib_cata_head\">"+CatArr[j]+"</div>";



for(var i = 0; i < SubCatArr[j].length; i++)
{
var SubBookCount = SubCatCountArr[j][i];


if(SubBookCount > 0)
	str += "<p><a href=\"elib_catalogue_detail.php?Category="+CatArr[j]+"&SubCategory="+SubCatArr[j][i]+"\">"+SubCatArr[j][i]+"</a><br></p>";
else
	str += "<p>"+SubCatArr[j][i]+"<br></p>";


}

str2 += "<div id=\"elib_cata_body\">"+str+"</div>"
}
	
	var x = obj.insertRow(0);
	var y = x.insertCell(0);
	y.innerHTML = str2;
	y.align = "center";

} // end function show englisgh book list

function showChineseBookList()
{
	var obj = document.all["CatList"];
	for(var i = obj.rows.length - 1 ; i >= 0 ; i--)
	obj.deleteRow(i);
	
	// set list array
	var CatArr = new Array();
	var CatCountArr = new Array();
	
	<? 
	for($i = 0; $i < count($catListArr["chi"]); $i++)
	{
		$x = "CatArr[".$i."] = \"".$catListArr["chi"][$i]["name"]."\";";
		$y = "CatCountArr[".$i."] = \"".$catListArr["chi"][$i]["total_count"]."\";";
		echo $x;
		echo $y;
	}
	?>
	
	var SubCatArr = new Array();
	var SubCatCountArr = new Array();
	
	<?
	for($j = 0; $j < count($catListArr["chi"]); $j++)
	{
		$y = "SubCatArr[".$j."] = new Array();";
		$z = "SubCatCountArr[".$j."] = new Array();";
		echo $y;
		echo $z;
		
		for($i = 0; $i < count($catListArr["chi"][$j]["subname"]); $i++)
		{
			$x = "SubCatArr[".$j."][".$i."] = \"".$catListArr["chi"][$j]["subname"][$i]."\";";
			$a = "SubCatCountArr[".$j."][".$i."] = \"".$catListArr["chi"][$j]["count"][$i]."\";";
			
			echo $x;
			echo $a;
		}
	}
	?>
	// end set prelist

	var str = "";
	var str2 = "";
	for(var j = 0; j < CatArr.length; j++)
	{
		var BookCount = CatCountArr[j];

		if(BookCount > 0)
			str2 += "<div id=\"elib_cata_head\"><a href=\"elib_catalogue_detail.php?Category="+CatArr[j]+"\">"+CatArr[j]+"</a></div>";
		else
			str2 += "<div id=\"elib_cata_head\">"+CatArr[j]+"</div>";
		
		str = "";
			for(var i = 0; i < SubCatArr[j].length; i++)
			{
				var SubBookCount = SubCatCountArr[j][i];
			
				if(SubBookCount > 0)
					str += "<p><a href=\"elib_catalogue_detail.php?Category="+CatArr[j]+"&SubCategory="+SubCatArr[j][i]+"\">"+SubCatArr[j][i]+"</a><br></p>";
				else
					str += "<p>"+SubCatArr[j][i]+"<br></p>";
			
			}
			
			str2 += "<div id=\"elib_cata_body\">"+str+"</div>"
		
	}
			
	var x = obj.insertRow(0);
	var y = x.insertCell(0);
	//var y=document.getElementById("elib_cata_border");
	y.innerHTML = str2;
	//y.align = "left";
} // end function add Chinese Book List



function ChangeCatTabList(lang)
{
	var obj = document.all["CatTabList"];
	
	for(var i = obj.rows.length - 1 ; i >= 0 ; i--)
	obj.deleteRow(i);
	
	var image_path = null;
	var LAYOUT_SKIN = null;
	var chineseName = null;
	var englishName = null;
	var str = "";
	
	<?
		$x = "";
		$x .= "image_path = \"".$image_path."\";\n";
		$x .= "LAYOUT_SKIN = \"".$LAYOUT_SKIN."\";\n";
		$x .= "chineseName = \"".$eLib["html"]["chinese"]."\";\n";
		$x .= "englishName = \"".$eLib["html"]["english"]."\";\n";
		
		echo $x;
	?>

	str += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	str += "<tr>";
	
	if(lang == "eng")
	{
		str += "<td width=\"5\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_on_01.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td align=\"center\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_on_02.gif\"><span id=\"engTag\" name=\"engTag\" class=\"eLibrary_title_heading\" onClick=\"changeTagLanguage('eng');\" onMouseOver=\"this.style.cursor='hand'\">"+englishName+"</span></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_on_03.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td width=\"5\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_off_01.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td align=\"center\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_ff_02.gif\"><span id=\"chiTag\" name=\"chiTag\" class=\"eLibrary_cat_tab_off\" onClick=\"changeTagLanguage('chi');\" onMouseOver=\"this.style.cursor='hand'\">"+chineseName+"</span></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_off_03.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td width=\"5\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
		str += "</tr>";
		str += "<tr>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";	
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
	}
	else
	{

		str += "<td width=\"5\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_off_01.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td align=\"center\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_off_02.gif\"><span id=\"engTag\" name=\"engTag\" class=\"eLibrary_cat_tab_off\" onClick=\"changeTagLanguage('eng');\" onMouseOver=\"this.style.cursor='hand'\">"+englishName+"</span></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_off_03.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td width=\"5\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_on_01.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td align=\"center\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_on_02.gif\"><span id=\"chiTag\" name=\"chiTag\" class=\"eLibrary_title_heading\" onClick=\"changeTagLanguage('chi');\" onMouseOver=\"this.style.cursor='hand'\">"+chineseName+"</span></td>";
		str += "<td width=\"4\" background=\""+image_path+"/"+LAYOUT_SKIN+"/eLibrary/catalogue_tab_on_03.gif\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"4\" height=\"22\"></td>";
		str += "<td width=\"5\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" alt=\"\" width=\"5\" height=\"22\"></td>";
		str += "</tr>";
		str += "<tr>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td bgcolor=\"#b1bfdb\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
		str += "<td height=\"1\"><img src=\""+image_path+"/"+LAYOUT_SKIN+"/10x10.gif\" width=\"1\" height=\"1\"></td>";
	}
	str += "</tr>";
	str += "</table>";
	
	
	var x = obj.insertRow(0);
	var y = x.insertCell(0);
	y.innerHTML = str;
	y.align = "center";
} // end function change cat tab list



function sortOrder(type)
{
	var order = "<?=$sortFieldOrder?>";
	var field = "<?=$sortField?>";
	
	if(order == "DESC" && field == type)
	document.form1.sortFieldOrder.value = "ASC";
	else
	document.form1.sortFieldOrder.value = "DESC";
	
	document.form1.sortField.value = type;
	document.form1.submit();
	
} // end function sortorder

function changeDisplayMode(mode)
{
	document.form1.DisplayMode.value = mode;
	document.form1.submit();
	
} // end function change display mode

function prevPage()
{
	var curPage = parseInt(document.form1.CurrentPageNum.value);
	
	if(curPage - 1 >= 1)
	{
	document.form1.CurrentPageNum.value = curPage - 1;
	document.form1.submit();
	}
	
} //end function prev page

function nextPage()
{
	var totalPage = "<?=$totalPage?>";
	var curPage = parseInt(document.form1.CurrentPageNum.value);

	if(curPage + 1  <= parseInt(totalPage))
	{
	document.form1.CurrentPageNum.value = curPage + 1;
	document.form1.submit();
	}
} //end function next page

function sortFunction(obj)
{
	if(obj.name == "sortField")
	{
		
	}
	else if(obj.name == "selPageTop" || obj.name == "selPageBottom")
	{
		document.form1.CurrentPageNum.value = obj.value;
	}
	else if(obj.name == "selDisplayPageTop" || obj.name == "selDisplayPageBottom")
	{
		document.form1.DisplayNumPage.value = obj.value;
	}
	
	document.form1.submit();
} // end function sort

function checkSearchForm(obj)
{
	document.searchForm.isSearchSelect.value = document.searchForm.selectSearchType.value;
	
	if(obj.keywords.value == "" || obj.keywords.value == "<?=$eLib["html"]["search_input"]?>")
	{
		alert("<?=$eLib["html"]["please_enter_keywords"]?>");
		return false;
	}
	else
		return true;
		
} // end function check search form

function checkInputText(obj)
{
	if(obj.value == "<?=$eLib["html"]["search_input"]?>")
	obj.value = "";
	
	obj.className = "tabletext";
	
	document.searchForm.isSearch.value = 1;
	
} // end function check input text

-->
</script>