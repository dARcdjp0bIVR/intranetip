<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] 	= $image_path;
$ParArr["LAYOUT_SKIN"] 	= $LAYOUT_SKIN;
$ParArr["currUserID"] 		= $_SESSION["UserID"];
$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

if($DisplayMode == "")
$DisplayMode = 1; // list mode (with image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortField == "")
$sortField = "Title";

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Language";
$tmpArr[3]["value"] = "Category";
$tmpArr[4]["value"] = "SubCategory";

$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]["language"];
$tmpArr[3]["text"] = $eLib["html"]["category"];
$tmpArr[4]["text"] = $eLib["html"]["subcategory"];

$tmpArr[0]["width"] = "20%";
$tmpArr[1]["width"] = "20%";
$tmpArr[2]["width"] = "20%";
$tmpArr[3]["width"] = "20%";
$tmpArr[4]["width"] = "20%";

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 9;
$currUserID = $_SESSION["UserID"];

$order = $sortField;
$order = $order." ".$sortFieldOrder;

$EngName = $eLib["html"]["english"];
$ChiName = $eLib["html"]["chinese"];

if($BookLanguage == "eng" || $BookLanguage == "chi")
$con1 = "AND Language = '".$BookLanguage."'";
else
$con1 = "";

if($BookCategory != "" &&  $BookCategory != "all")
//$con2 = "AND Category = '".iconv("big-5","utf-8",$BookCategory)."'";
$con2 = "AND Category = '".$BookCategory."'";
else
$con2 = "";

$sql = "
		SELECT 
		BookID, 
		Title,
		Author,
		if((Language = 'eng'),'{$EngName}' , '{$ChiName}') as Language,
		Category,
		SubCategory,
		Publisher,
		Source,
		Level, BookFormat, IsTLF
		FROM
		INTRANET_ELIB_BOOK
		WHERE 1
		AND Publish = 1
		$con1
		$con2
		ORDER BY
		$order
		";
		
		//debug_r($sql);

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

////////////////////////////////
if($BookLanguage == "eng")
$langName = $eLib["html"]["english_books"];
else if($BookLanguage == "chi")
$langName = $eLib["html"]["chinese_books"];
else
$langName = $eLib["html"]["all_books"];
////////////////////////////////

// selection box
$LangSelectArr[] = array("all", $eLib["html"]["all_books"]);
$LangSelectArr[] = array("eng", $eLib["html"]["english_books"]);
$LangSelectArr[] = array("chi", $eLib["html"]["chinese_books"]);
$LangSelect = $linterface->GET_SELECTION_BOX($LangSelectArr," name='BookLanguage' onChange='changeOrder(this);' ", "", $BookLanguage);

if($BookLanguage == "eng")
$CatAllName = $eLib["html"]["all_english_categories"];
else if($BookLanguage == "chi")
$CatAllName = $eLib["html"]["all_chinese_categories"];
else
$CatAllName = $eLib["html"]["all_categories"];

$ParArr["Language"] = $BookLanguage;

$CatrReturnArr = $lelib->GET_BOOK_CATEGORY($ParArr);

$CatSelectArr[] = array("all", $CatAllName);

for($i = 0; $i < count($CatrReturnArr); $i++)
{
	//$catName = iconv("utf-8", "big-5", $CatrReturnArr[$i]["Category"]);
	$catName = $CatrReturnArr[$i]["Category"];
	$CatSelectArr[] = array($catName, $catName);
}

$CatSelect = $linterface->GET_SELECTION_BOX($CatSelectArr," name='BookCategory' onChange='changeOrder(this);' ", "", $BookCategory);

////////////////////////////////////////////////

// set view icon
$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$x .= "<td class=\"tabletextremark\">".$eLib["html"]["view"]."</td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 1)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";

$x .= "</a></td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 2)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";

$x .= "</a></td></tr></table>";

$viewChangeSelection = $x;

////////////////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeOrder(obj)
{
	//alert(obj.name);
	if(obj.name == "BookLanguage")
	{
		document.form1.BookCategory.selectedIndex = 0;
	}
	
	document.form1.submit();
	
} // end function change order

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top">
	<!-- ------------------------------------------------------------ -->
	<!-- ---------------      LEFT NAVIGATION BAR ----------------------- --> 
	<?php include_once('elib_left_navigation.php'); ?>
</td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_catalogue_list_category.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="elib_catalogue_list.php"><?=$eLib["html"]["catalogue"]?></a>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$langName?></td>
</tr>
</table>
<!-- end head menu -->


<table width="98%" border="0" cellpadding="0" cellspacing="0">
<!--
<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
</tr>
-->
<tr>
<!--
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>
-->
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/10x10.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="15" height="21"></td>
<!--
<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
-->
<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/10x10.gif">

<!-- show content -->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td align="left"><?=$LangSelect."&nbsp;".$CatSelect?></td>
<td align="right"><?=$viewChangeSelection?></td></tr>
</table>
<?=$contentHtml?>
<!-- end show content -->
</td>

<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/10x10.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="25" height="30"></td>
<!--
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
-->
</tr>
<!--									
<tr>
<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
</tr>
-->
</table>


<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
