<?php

//using: 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//var_dump($intranet_session_language);
include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
//include_once($PATH_WRT_ROOT."lang/lang.b5.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");


intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("elib_script.php");

//debug_r($_SESSION);

$lo 		= new libeclass();
$lelib 		= new elibrary();
$linterface = new interface_html("default3.html");

$objInstall 	= new elibrary_install();

//$CurrentPage= "PageMyeClass";
$CurrentPage= "PageMyeClass";

$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title);
$MODULE_OBJ["title"] 	= $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] 	= $PATH_WRT_ROOT."home/eLearning/elibrary";
$MODULE_OBJ['title_css']= "menu_opened";
$customLeftMenu 		= ' ';
$CurrentPageArr['eLib'] = 1;

$ParArr["currUserID"] 		= $_SESSION["UserID"];

$settingArr = $lelib->GET_USER_BOOK_SETTING($ParArr);

$NumReviewer 	= ($settingArr[0]["DisplayReviewer"] == 0 || $settingArr[0]["DisplayReviewer"] == "")? 10 : $settingArr[0]["DisplayReviewer"];
$NumReview 		= ($settingArr[0]["DisplayReview"] == 0 || $settingArr[0]["DisplayReview"] == "") ? 10 : $settingArr[0]["DisplayReview"];
$NumRecommend 	= ($settingArr[0]["DisplayRecommendBook"] == 0 || $settingArr[0]["DisplayRecommendBook"] == "")? 4 : $settingArr[0]["DisplayRecommendBook"];
$NumWeeklyHit  	= ($settingArr[0]["DisplayWeeklyHitBook"]  == 0 || $settingArr[0]["DisplayWeeklyHitBook"]  == "")? 1 : $settingArr[0]["DisplayWeeklyHitBook"];
$NumHit 		= ($settingArr[0]["DisplayHitBook"]  == 0 || $settingArr[0]["DisplayHitBook"]  == "")? 4 : $settingArr[0]["DisplayHitBook"];

$totalPage = 0;
$DisplayNumPage = 0;

// additional javascript for eLibrary only
include_once("elib_script_function.php");

if($plugin['koha'])
{
	$koha_btn = "<a href='admin/login_koha.php' target='_blank'>Go to Koha</a><br />";
}

$aryRecommendBook = $lelib->GET_RECOMMEND_BOOK($NumRecommend);
$numberOfRecommendBook = count($aryRecommendBook);
$display_recommend_book 	= $lelib->newPrintBookTable($aryRecommendBook, $image_path, $LAYOUT_SKIN, $NumRecommend, "recommend", $eLib);
$display_weekly_hit 		= $lelib->newPrintBookTable($lelib->GET_WEEKLY_HIT_BOOK($NumWeeklyHit), $image_path, $LAYOUT_SKIN, $NumWeeklyHit, "weeklyHit", $eLib);
$display_accumulated_hits 	= $lelib->newPrintBookTable($lelib->GET_HIT_BOOK($NumHit), $image_path, $LAYOUT_SKIN, $NumHit, "hit", $eLib);
$display_most_active 		= $lelib->newPrintMostActiveReviewers($lelib->GET_MOST_ACTIVE_REVIEWERS($NumReviewer), $image_path, $LAYOUT_SKIN);
$display_most_userful 		= $lelib->newPrintMostUsefulReviews($lelib->GET_MOST_USEFUL_REVIEWS($NumReview), $image_path, $LAYOUT_SKIN);

$linterface->LAYOUT_START();
?>
<script>
$(document).ready(function(){
	var numberOfRecommendBook = "<?=$numberOfRecommendBook?>";	
	if(numberOfRecommendBook != "" && numberOfRecommendBook > 0) onclickNextBook();
});

function Book(bookTitle, bookDescription, bookPath, bookOpenLink)
{
	this.title = bookTitle;
	this.description = bookDescription;
	this.path = bookPath;
	this.openLink = bookOpenLink;
	
}

var curBook = -1;
var recomBooks = new Array();

<?php

$book = $lelib->GET_RECOMMEND_BOOK($NumRecommend);

for ($i = 0; $i < count($book); ++$i)
{
	echo "recomBooks[$i] = new Book(\"".$book[$i]["Title"]."\", \"".str_replace("\n", '<br />', str_replace('"','\"',$book[$i]["Description"]) )."\", \"".$book[$i]["BookID"]."\", \"".$objInstall->get_book_reader_link($book[$i]["BookID"], $book[$i]["BookFormat"], $book[$i]["IsTLF"], $_SESSION['UserID'], "COVER")."\");\r\n";
}
?>																																
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">
<link href="css/elib_josephine.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js">	</script>
<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css" media="screen">

<script type="text/javascript">
var range='';
var type='recommend';
$(document).ready(function() {
		
		
		 /*  Simple image gallery. Uses default settings
			 */
		$('.fancybox').fancybox( {
		 'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 800,
         'height'            : 600,
		 'padding'			 : 20,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
		
		$('.fancybox_small').fancybox( {
		'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 600,
         'height'            : 500,
		 'padding'			 : 0,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
		
		$('.fancybox_magazine').fancybox( {
		'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 550,
         'height'            : 320,
		 'padding'			 : 0,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
		$('.fancybox_magazine_learnthenews').fancybox( {
		'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 820,
         'height'            : 500,
		 'padding'			 : 0,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
	
       $('.book_detail').fancybox({
		autoDimensions : false,
		autoScale : false,
		autoSize : false,
		width : 800,
		height : 600,
		type : 'ajax',
		padding: 0,
		nextEffect: 'fade',
		prevEffect: 'fade',
		beforeClose: function(){
		    $('.portal_bookshelf_recommend .bookshelf_title_name li.current a').trigger('click.noanim');
		    $('.portal_mostrate .bookshelf_title_filter li.current a').trigger('click.noanim');
		    $('.portal_mosthit .bookshelf_title_filter li.current a').trigger('click.noanim');
		    $('.ranking_submit').trigger('click.noanim');
		
		}
    
       });     
      
       $('.portal_bookshelf_recommend .bookshelf_title_name li a').on('click.noanim',function(e){
              
		$(this).parent().addClass('current').siblings().removeClass('current');
		var type = $(this).attr('href').replace('#','');
		$('.portal_bookshelf_recommend .bookshelf_title_filter li.btn_more a').attr('href','./?page='+type);
		
		$.fancybox.showLoading();
		if (typeof(e.namespace)=='undefined') $('.portal_bookshelf_recommend .book_list').animate({'margin-top': -200, opacity: 0});
		
		
		$.get('ajax.php',{action: 'getPortalRecommend', type: type},function(data){
		       $.fancybox.hideLoading();
		       $('.portal_bookshelf_recommend .book_list').html(data).animate({'margin-top': 0, opacity: 1});
		});
	      
		return false;
       });
       
       $('.portal_mostrate .bookshelf_title_filter li:not(.btn_more) a').on('click.noanim',function(e){
		  
		$(this).parent().addClass('current').siblings().removeClass('current');
		var range = $(this).attr('href').replace('#','');
		$('.portal_mostrate .bookshelf_title_filter li.btn_more a').attr('href','./?page=ranking&range='+range+'#BestRateBooks');
		
		$.fancybox.showLoading();
		if (typeof(e.namespace)=='undefined') $('.portal_mostrate .book_list').animate({'margin-top': -200, opacity: 0});
		
	    
		$.get('ajax.php',{action: 'getPortalBestRate', range: range},function(data){
		       $.fancybox.hideLoading();
		       $('.portal_mostrate .book_list').html(data).animate({'margin-top': 0, opacity: 1});
		});
		
		
		return false;
       });
       
       $('.portal_mosthit .bookshelf_title_filter li:not(.btn_more) a, .portal_mosthit .bookshelf_title_name li a').on('click.noanim',function(e){
		  
		$(this).parent().addClass('current').siblings().removeClass('current');
		var range = $('.portal_mosthit .bookshelf_title_filter li.current a').attr('href').replace('#','');
		var type = $('.portal_mosthit .bookshelf_title_name li.current a').attr('href').replace('#','');
		$('.portal_mosthit .bookshelf_title_filter li.btn_more a').attr('href','./?page=ranking&range='+range+'#'+type);
		
		
		$.fancybox.showLoading();
		if (typeof(e.namespace)=='undefined') $('.portal_mosthit .book_list').animate({'margin-top': -200, opacity: 0});
		
		$.get('ajax.php',{action: 'getPortalMostHitLoan', range: range, type: type},function(data){
		       $.fancybox.hideLoading();
		       $('.portal_mosthit .book_list').html(data).animate({'margin-top': 0, opacity: 1});
		});
		
		
		
		return false;
       });
       
       $('.ranking_range, .ranking_type').on('change.noanim',function(e){
	
	    $.fancybox.showLoading();
	    var range = $('.ranking_range').val();
	    var type = $('.ranking_type').val();
	    $('.ranking_bottom .btn_more').attr('href','./?page=ranking&range='+range+'#'+type);
	    
	    if (typeof(e.namespace)=='undefined') $('.ranking_container').slideUp();
	    $.get('ajax.php?action=getPortalRank', {range: range, type: type},function(data){
		   $.fancybox.hideLoading();
		   $('.ranking_container').html(data).slideDown();
	    });
	    return false;
       });
       
       $('.p_tab_news').on('click.noanim',function(e){
	
	    $.fancybox.showLoading();
	    $('#portal_news').fadeIn().siblings().hide();
	    $('.p_tab_news').parent().addClass('current_tab').siblings().removeClass('current_tab');


	    $.get('ajax.php?action=getPortalNotices&type=news', {offset:0}, function(data){
		
		$.fancybox.hideLoading();
		$('#portal_news .portal_news_content_detail ul').html(data);
		$('#portal_news .portal_news_content_detail').scrollTop(0);
		
	    });
	    return false;
		
       });
       $('.p_tab_rules').on('click.noanim',function(e){
	
	    $.fancybox.showLoading();
	    $('#portal_rules').fadeIn().siblings().hide();
	    $('.p_tab_rules').parent().addClass('current_tab').siblings().removeClass('current_tab');
	    
	    $.get('ajax.php?action=getPortalNotices&type=rules', function(data){
		
		$.fancybox.hideLoading();
		$('#portal_rules .portal_news_content_detail').html(data);
		$('#portal_rules_new .news_content').val($('#portal_rules .portal_news_content_detail .rules_content').val());
		
	    });

	    return false;
		
       });
       $('.p_tab_open').on('click.noanim',function(e){
	
	    $($(this).attr('href')).fadeIn().siblings().hide();
	    $(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
	    return false;
	   
       });
       
       $('#portal_news .tool_manage').click(function(){
	
	    $.fancybox.open('ajax.php?action=getPortalNoticeForm&type=news',{
		type: 'ajax',
		margin: 0,
		padding: 0
	    });
	    return false;
       });
       
       $('#portal_rules .tool_edit').click(function(){
	   $.fancybox.open('ajax.php?action=getPortalNoticeForm&type=rules',{
		type: 'ajax',
		margin: 0,
		padding: 0
	    });
	   return false;
       });

       
       
       $(document).click(function(e){
	
	    if ($(e.target).closest('.portal_news_board').length==0) $('#portal_news_new, #portal_rules_new').fadeOut();
	});
    
       
       $('.portal_bookshelf_recommend .bookshelf_title_name li.current a').trigger('click.noanim');
       $('.portal_mostrate .bookshelf_title_filter li.current a').trigger('click.noanim');
       $('.portal_mosthit .bookshelf_title_filter li.current a').trigger('click.noanim');
       $('.ranking_range').trigger('change.noanim');
       $('.p_tab_news, .p_tab_rules').trigger('click.noanim');
});
</script>


<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="center">
  				<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
        			<td>
        				<table width="99%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#FFFFFF">
                    			<table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                    			<tr>
                    				<td valign="top" width="175">
									
									<!-- ------------------------------------------------------------ -->
									<!-- ---------------------    SETTING      ---------------------- -->
									<!-- Setting logo -->
									<?
									if($lelib->IS_ADMIN_USER($_SESSION["UserID"]))
									{
									?>
									<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">
									<table width="95%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_01.gif" width="4" height="4"></td>
										<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
										<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_03.gif" width="4" height="4"></td>
									</tr>
									<tr>
										<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
										<td bgcolor="#d7e9f4"><a href="javascript:void()" class="menuon" onClick="MM_openBrWindow('elib_setting.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib["html"]["settings"]?></a></td>
										<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
									</tr>
									<?php
									if($plugin['eLib_license'] == 2){
									?>
									<tr>
										<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
										<td bgcolor="#d7e9f4"><a href="javascript:void()" class="menuon" onClick="MM_openBrWindow('admin_book_license.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib['license']['ManageBookLicense']?></a></td>
										<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
									</tr>
									<?php
									}
									?>
									<tr>
										<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_07.gif" width="4" height="4"></td>
										<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
										<td width="4" height="4" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_09.gif" width="4" height="4"></td>
									</tr>
									</table>
									
									<?
									
									}
									?>
                    				
		                    			<!-- ------------------------------------------------------------ -->
										<!-- ---------------      LEFT NAVIGATION BAR ----------------------- --> 
										<?php include_once('elib_left_navigation.php'); ?>								
									</td>

									<td valign="top" align="left">
										<div class="800width"></div>
										<!-- eLibrary Content Begin -->

<!-- School Recommended Book Begin --> 

<div id="photo_wrap">

    <div class="photo_bg">
        <div class="photo_bg_left_bg">
            <div class="photo_bg_left">
				<div class="photo_recommend">    
               
              	<!-- CarouselArea Begin  -->           
					<div id="recommend_wrap">
							<div class="photo" >
    							<a href="javascript:void()" onclick="openRecommendedBookNow()" title="<?=$objInstall->get_book_action_title("COVER")?>" ><img id="bookCover" height="200" align="center" src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/cover_no_image.jpg"/></a>
    						</div>
							
                           	 <div class="right">								
   								<a href="javascript:void()" onMouseOver="this.style.cursor='pointer'" onclick="openRecommendedBook()" title="<?=$eLib["action"]['view_details']?>"><div class="title" id="bookTitle"></div></a>
    							<span class="reason" id="bookReason" style="line-height:18px; overflow-y: auto;">    							
    							<strong>
    							<?=$eLib["html"]["recommended_reason"]?>    							
    							</strong>
    							<?php 
    							if($numberOfRecommendBook == 0 ){
    								echo "<BR /><BR /><BR /><BR /><BR />".$eLib["html"]["no_recommend_book"];
    							 } 
    							 ?>
    							</span>
							</div>
					</div>
				<!-- CarouselArea End -->

               </div>
               
               
               	<?php if($numberOfRecommendBook > 1 ){ ?>
                 <div class="photo_nav_outer"><div class="photo_nav_inner" >
                	<div class="photo_nav">
				    <ul>
					    <li id="previous"><a href="javascript:void()" onclick="onclickPreviousBook();" title="<?=$eLib["action"]['previous_book']?>" >&nbsp;</a></li>
					    <li id="next"><a href="javascript:void()" onclick="onclickNextBook();" title="<?=$eLib["action"]['next_book']?>" >&nbsp;</a></li>
					    <!--<li id="open"><label title="Open Book"><a href="javascript:void()" onclick="openRecommendedBook();return false;">&nbsp;</a></label></li>-->				
				    </ul>
    				</div>
                </div></div>
                <?php } ?>
             
            </div>
        </div>
     	<div class="photo_bg_middle"></div>
       
        <div class="photo_bg_right">
    		<div class="top"><?=$eLib["html"]["recommended_books_2"]?></div>
            <div class="books9">            
             	<?=$display_recommend_book?>
            </div>
		</div>
    </div>
</div>

<!-- School Recommended Book End --> 




<!-- eLibrary Book Ranking Begin -->
<div id="book_rank">


	<!-- Session for Book with Highest Hit Rates -->
    <div class="book_rank_top_right">
    <div class="book_rank_top_left">
    <p><?=$eLib["html"]["bookS_with_highest_hit_rate"]?></p><p><a href="javascript:void()" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=weeklyhit','listbook','scrollbars=yes')"><?=$eLib["html"]["book_with_hit_rate_last_week"]?><small> ... <a href="javascript:void()" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=weeklyhit','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></small></a></p>
    </div></div>													
						
	
    <div class="book_rank_body_left"><div class="book_rank_body_right">
    <div class="book_rank_body_books" style="min-height:350px;">
   
			<!-- weekly hit -->
			<?=$display_weekly_hit?>
			<!-- end weekly hit -->
	
    </div>
    </div></div>
    
    
    <!-- Session for Accumulated Highest -->
    <div class="book_rank_middle_right">
    <div class="book_rank_middle_left">
    <p><a href="javascript:void()" href="javascript:void()" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=hit','listbook','scrollbars=yes')"><?=$eLib["html"]["book_with_hit_rate_accumulated"]?><small> ... <a href="javascript:void()" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=hit','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></small></a></p>
    </div></div>

					

    <div class="book_rank_body_left"><div class="book_rank_body_right">
    <div class="book_rank_body_books" style="min-height:350px;">
    
    		<!-- hit book -->			
			<?=$display_accumulated_hits?>
			<!-- end hit book -->
    
    </div>
    </div></div>
    
    
    <div class="book_rank_footer_right"><div class="book_rank_footer_left"></div></div>
</div>

<!-- eLibrary Book Ranking End -->

<!-- 2013-08-21 eLibrary addon -->

<? if($plugin['elib_epost'] || $plugin['elib_cosmos'] ||$plugin['elib_emag'] ||$plugin['elib_video']){?>
	<div class="elib_addon">
		<ul>
		<?if($plugin['elib_epost']){?>
	     <li><a href="#" class="btn_epost" title="ePost">&nbsp;</a></li>
	     <li class="elib_addon_break"></li>
	    <? } ?>
	    <?if($plugin['elib_video']){?>
	     <li><a href="../elibvideo/access.php" target="_blank" class="btn_video" title="Videos">&nbsp;</a></li>
	    <li class="elib_addon_break"></li>
	     <? } ?>
	    <?if($plugin['elib_cosmos']){?><li><a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="mag_cosmos" title="Cosmos">&nbsp;</a></li> <? } ?>
		 <?if($plugin['elib_emag']){?><li><a href="../elibplus/magazine_list.php?redirect_to_emag_site=1" class="mag_learnthenews fancybox fancybox_magazine_learnthenews  fancybox.iframe"" title="Learn the News">&nbsp;</a></li> <? } ?>
	    </ul>
	</div>
<? } ?>
<!-- 2013-08-21 eLibrary addon -->

<!-- eLibrary Review Ranking Begin -->
<div class="review_rank">
	
	
	<!-- Begin of Most Active Reviewers -->
	<div class="book_review_top_left"><div class="book_review_top_right"><span class="table_head"><?=$eLib["html"]["most_active_reviewers"]?></span></div></div>

    
	<div class="book_review_body_left"><div class="book_review_body_right">
    	
		<?=$display_most_active?>
			
        <div class="book_review_footer_left"><div class="book_review_footer_right"></div></div>
    </div></div>
	<!-- End of Most Active Reviewers -->
	
	
    <div class="book_review_padding"><p></p></div>
   
     
    <!-- Begin of Most Helpful Reviews -->
    <div class="book_review_top_left2"><div class="book_review_top_right2"><span class="table_head"><?=$eLib["html"]["most_useful_reviews"]?></span></div></div>
        
	<div class="book_review_body_left"><div class="book_review_body_right">
    		
    		
    	 <?=$display_most_userful?> 
    		
        <div class="book_review_footer_left"><div class="book_review_footer_right"></div></div>
    </div></div>               
    <!-- End of Most Helpful Reviews -->

</div>

</td></tr></table>
                      
                      </td>
                      
                    </tr>
                   
                  </table>
                  <br>
                </td>
              </tr>
            </table></td>
        </tr>
    </table></td>
  </tr>
</table>






<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
