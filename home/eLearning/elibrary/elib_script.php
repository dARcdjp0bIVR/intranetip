<?php
//Using by:
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
   	
function new3Get_Catalogue_Menu($Lang="chi") {
	global $image_path, $LAYOUT_SKIN, $eLib, $catListArr, $intranet_session_language;
	$getChineseBookList = getChineseBookList(); 
	$getEnglishBookList = getEnglishBookList();
	
	$ChiBook = ($intranet_session_language=="en") ? $eLib['html']['chinese'] : "&nbsp; ".$eLib['html']['chinese'];
	
	//if($Lang == "chi"){
		$display = ($Lang == "eng")?"style='display:block;visibility:visible;position:relative;'":"style='display:none;visibility:visible;position:relative;'";
		$x .= "<div id=\"elib_cata_wrap_01\" ".$display." >
			<div id=\"elib_cata_top1\">
				<div id=\"elib_lang01\" style='width:66px; heigh=10px;'>".$eLib['html']['english']."</div>
				<div id=\"elib_lang02\" ><a href=\"javascript:void(0);\" onMouseOver=\"document.getElementById('elib_cata_wrap_02').style.display='block';document.getElementById('elib_cata_wrap_01').style.display='none'\" onClick=\"document.getElementById('elib_cata_wrap_02').style.display='block';document.getElementById('elib_cata_wrap_01').style.display='none'\"  >".(($intranet_session_language=="en") ? "&nbsp; " : "").$ChiBook."</a></div>
			</div>									
			<div id=\"elib_cata_border\" >
					$getEnglishBookList
					<div id=\"elib_cata_listall\"><a href=\"elib_catalogue_list.php\" class=\"elib_cata_listall\" >".$eLib["html"]["list_all"]."</a></div>
		
			</div>
	
				<div id=\"elib_cata_bottom\" ></div>
		</div>";
		
		$display = ($Lang == "chi")?"style='display:block;visibility:visible;position:relative;'":"style='display:none;visibility:visible;position:relative;'";
		$x .= "<div id=\"elib_cata_wrap_02\" ".$display." >
					<div id=\"elib_cata_top2\">
						<div id=\"elib_lang02\" ><a href=\"javascript:void(0);\" onClick=\"document.getElementById('elib_cata_wrap_01').style.display='block';document.getElementById('elib_cata_wrap_02').style.display='none'\" onMouseOver=\"document.getElementById('elib_cata_wrap_01').style.display='block';document.getElementById('elib_cata_wrap_02').style.display='none'\" >".$eLib['html']['english']."</a></div>
						<div id=\"elib_lang01\" >".$ChiBook."</div>
					</div>
	
					<div id=\"elib_cata_border\">
							$getChineseBookList
						
							<div id=\"elib_cata_listall\"><a href=\"elib_catalogue_list.php\" class=\"elib_cata_listall\" >".$eLib["html"]["list_all"]."</a></div> 
	    			
					</div>
	
					<div id=\"elib_cata_bottom\"></div>
		</div>";
		
	
	return $x;
} // end function get catalogue Menu


//var_dump($catListArr["eng"][0]["name"]);
///////////////////////////////////////////////////////////////////////////
// preset catalogue list //// 
/*
$catListArr["eng"][0]["name"] = "Readers";
$catListArr["eng"][0]["subname"][0] = "Adventure";
$catListArr["eng"][0]["subname"][1] = "Comedy";
$catListArr["eng"][0]["subname"][2] = "Future Thriller";
$catListArr["eng"][0]["subname"][3] = "Human Interest";
$catListArr["eng"][0]["subname"][4] = "Horror";
$catListArr["eng"][0]["subname"][5] = "Ghost Story";
$catListArr["eng"][0]["subname"][6] = "Murder Mystery";
$catListArr["eng"][0]["subname"][7] = "Romance";
$catListArr["eng"][0]["subname"][8] = "Science Fiction";
$catListArr["eng"][0]["subname"][9] = "Short Stories";
$catListArr["eng"][0]["subname"][10] = "Thriller";



$catListArr["chi"][0]["name"] = "文學";
$catListArr["chi"][0]["subname"][0] = "中國現代文學名著";
$catListArr["chi"][0]["subname"][1] = "中國古典詩詞";
$catListArr["chi"][0]["subname"][2] = "中華歷史文庫";

$catListArr["chi"][1]["name"] = "傳記";
$catListArr["chi"][1]["subname"][0] = "中外名人傳記";

$catListArr["chi"][2]["name"] = "生活";
$catListArr["chi"][2]["subname"][0] = "科普藝術教育";
$catListArr["chi"][2]["subname"][1] = "家庭生活百科";

$catListArr["chi"][3]["name"] = "流行文學";
$catListArr["chi"][3]["subname"][0] = "散文";
$catListArr["chi"][3]["subname"][1] = "文學";
*/

$SchoolBasedCategory["張祝珊英文中學"] = true;
$SchoolBasedCategory["UCCKE"] = true;
$SchoolBasedCategory["匯基（東九龍）"] = true;
$SchoolBasedCategory["循道學校"] = true;
$SchoolBasedCategory["Kap Yan"] = true;
$SchoolBasedCategory["五旬節林漢光"] = true;

$SchoolBasedShow["UCCKE"] = $plugin["eLib_School_Cat"]["uccke"];
$SchoolBasedShow["匯基（東九龍）"] = $plugin["eLib_School_Cat"]["uccke"];
$SchoolBasedShow["張祝珊英文中學"] = $plugin["eLib_School_Cat"]["ccsc"];
$SchoolBasedShow["循道學校"] = $plugin["eLib_School_Cat"]["kmsch"];
$SchoolBasedShow["Kap Yan"] = $plugin["eLib_School_Cat"]["twghkyds"];
$SchoolBasedShow["五旬節林漢光"] = $plugin["eLib_School_Cat"]["lamhonkwong"];

/*
$catListArr["eng"][0]["total_count"] = 0;
$catListArr["eng"][0]["count"][0] = 0;
$catListArr["eng"][0]["count"][1] = 0;
$catListArr["eng"][0]["count"][2] = 0;
$catListArr["eng"][0]["count"][3] = 0;
$catListArr["eng"][0]["count"][4] = 0;
$catListArr["eng"][0]["count"][5] = 0;
$catListArr["eng"][0]["count"][6] = 0;
$catListArr["eng"][0]["count"][7] = 0;
$catListArr["eng"][0]["count"][8] = 0;
$catListArr["eng"][0]["count"][9] = 0;
$catListArr["eng"][0]["count"][10] = 0;

$catListArr["chi"][0]["total_count"] = 0;
$catListArr["chi"][0]["count"][0] = 0;
$catListArr["chi"][0]["count"][1] = 0;
$catListArr["chi"][0]["count"][2] = 0;

$catListArr["chi"][1]["total_count"] = 0;
$catListArr["chi"][1]["count"][0] = 0;

$catListArr["chi"][2]["total_count"] = 0;
$catListArr["chi"][2]["count"][0] = 0;
$catListArr["chi"][2]["count"][1] = 0;
*/
//$eLib_Catalogue = new3Get_Catalogue_Menu("chi");

intranet_opendb();
if (!isset($lelib))
{
	include_once($PATH_WRT_ROOT."includes/libelibrary.php");
	$lelib = new elibrary();
}
$catListArr = $lelib->GetCategories();


if($intranet_session_language == "en"){
	if (sizeof($catListArr["eng"])>0)
	{
		$eLib_Catalogue = new3Get_Catalogue_Menu("eng");
	} else
	{
		$eLib_Catalogue = new3Get_Catalogue_Menu("chi");
	}
	
}else if($intranet_session_language == "b5"){
	if (sizeof($catListArr["chi"])>0)
	{
		$eLib_Catalogue = new3Get_Catalogue_Menu("chi");
	} else
	{
		$eLib_Catalogue = new3Get_Catalogue_Menu("eng");
	}
}



function getChineseBookList(){
	global $catListArr;
	
	$x="";
	
	for($i=0;$i<count($catListArr["chi"]);$i++){
		
		$x .= "<div id=\"elib_cata_head\">" .
				"<p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["chi"][$i]["name"]."\">".$catListArr["chi"][$i]["name"]."</a></p>" .
				"</div>";
		$x2="";
		for($j=0;$j<count($catListArr["chi"][$i]["subname"]);$j++){
			$x2 .= "<p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["chi"][$i]["name"]."&SubCategory=".$catListArr["chi"][$i]["subname"][$j]."\">".$catListArr["chi"][$i]["subname"][$j]."</a></p>";
		
		}
		$x .= "<div id=\"elib_cata_body\">".$x2."</div>";			
	}
	
	return $x;
}


function getEnglishBookList(){
	global $catListArr;
	
	$x= "";
	
	for($i=0;$i<count($catListArr["eng"]);$i++){
		
		$x .= "<div id=\"elib_cata_head\"><p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["eng"][$i]["name"]."\">".$catListArr["eng"][$i]["name"]."</a></p></div>";
		$x2="";
		for($j=0;$j<count($catListArr["eng"][$i]["subname"]);$j++){
			
			$x2 .= "<p><a href=\"elib_catalogue_detail.php?Category=".$catListArr["eng"][$i]["name"]."&SubCategory=".$catListArr["eng"][$i]["subname"][$j]."\">".$catListArr["eng"][$i]["subname"][$j]."</a></p>";
		}
			$x .= "<div id=\"elib_cata_body\">".$x2."</div>";
	}
	
	return $x;
}

/*
# get tags
global $lelib;

intranet_opendb();
if ($lelib==null || !isset($lelib))
{
	include_once($PATH_WRT_ROOT."includes/libelibrary.php");
	$lelib = new elibrary();
}
$TagArr = $lelib->returnPopularTags(5);

for ($i=0; $i<sizeof($TagArr); $i++)
{
	$TagObj = $TagArr[$i];
	$TagsHtml .= "<li><a href=\"elib_advanced_search_result.php?selectSearchType=4&keywords=".$TagObj["TagID"]."\">".$TagObj["TagName"]." <!--(".$TagObj["TotalCount"].")--></a></li>";
}

$eLib_Catalogue .= "<p><ul>".$TagsHtml."</ul></p>";
*/
?>