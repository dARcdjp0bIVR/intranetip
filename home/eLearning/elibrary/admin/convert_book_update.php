<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_cache();
intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$LibFS = new libfilesystem();
$Location1 = $intranet_root."/file/elibrary";
if (!is_dir($Location1))
{
	$LibFS->folder_new($Location1);
}
$Location2 = $intranet_root."/file/elibrary/tmp";
if (!is_dir($Location2))
{
	$LibFS->folder_new($Location2);
}
$Location3 = $intranet_root."/file/elibrary/tmp/orig";
if (!is_dir($Location3))
{
	$LibFS->folder_new($Location3);
}
$Location4 = $intranet_root."/file/elibrary/tmp/convert";
if (!is_dir($Location4))
{
	$LibFS->folder_new($Location4);
}


$File = $Location2."/".mktime().".zip";

if ($Language == "eng")
{
// Convert english book	
	if (move_uploaded_file($HTTP_POST_FILES['InputFile']['tmp_name'],$File))
	{	
		$LibFS->file_unzip($File, $Location3);
		@$LibFS->chmod_R($Location2,0777);
		
		if(!file_exists($Location3."/"."content.xml"))
		{
			intranet_closedb();
			header("Location: convert_book.php?errMsg=1");
		}
		
		$xml_parser = xml_parser_create("UTF-8");
		if (!$fp = fopen($Location3."/"."content.xml", "r")) 
		{
		}
		else
		{	
			/*
			// convert the content - old method
			$xml_contents = fread($fp, filesize($Location3."/"."content.xml"));
			fclose($fp);
			
			$mod_xml_contents = preg_replace("/(\<img)(.*)(alt=\"Image\"\>\<\/img\>)/","[img]\\2[/img]",$xml_contents);
			xml_parse_into_struct($xml_parser, $mod_xml_contents, $temp_arr_vals);
			xml_parser_free($xml_parser);
			
			$PageCnt = 0;
			$PrePageCnt = 0;
			$OutputXML = "";
			for($i=0; $i<count($temp_arr_vals); $i++)
			{
				if($temp_arr_vals[$i]['tag'] == "A") 
				{
					if (strtolower(substr($temp_arr_vals[$i]['attributes']['ID'],0,4)) =="page")
					{
						$PageCnt++;
				
						echo $PageCnt."<br />";
						print_r($temp_arr_vals[$i]);	
						
						$tag_A_index[] = $i;
					}
				}
			} // end for
			*/
			$xml_contents = fread($fp, filesize($Location3."/"."content.xml"));
			fclose($fp);
			
		$LibLibrary = new elibrary();
		
		// convert & output the file to tmp folder
		$ParArr = "";
		$ParArr["xml_contents"] = $xml_contents;
		$ParArr["xml_parser"] = $xml_parser;
		$ParArr["location_orig"] = $Location3;
		$ParArr["location_tmp_convert"] = $Location4;
		
		$LibLibrary->DO_CONVERSION_CUP($ParArr);

		$LibFS->file_zip("convert",$Location2."/convert.zip",$Location2);
		$OutputContent = $LibFS->file_read($Location2."/"."convert.zip");

		$LibFS->folder_remove_recursive($Location3);
		$LibFS->folder_remove_recursive($Location4);
		
		output2browser($OutputContent, "convert.zip");
		
		$LibFS->item_remove($Location2."/"."convert.zip");
		$LibFS->item_remove($File);
		
		unlink($Location2."/"."convert.zip");
		unlink($File);
			
		} // end if exist content.xml
	} // end if exist file
}
else
{
// Convert chinese book	
	if (move_uploaded_file($HTTP_POST_FILES['InputFile']['tmp_name'],$File))
	{	
		$LibFS->file_unzip($File, $Location3);
		@$LibFS->chmod_R($Location2,0777);
		
		if (!file_exists($Location3."/"."control.txt") || !file_exists ($Location3."/"."content.txt"))
		{
			intranet_closedb();
			header("Location: convert_book.php?errMsg=1");
		}
		
		$Content1 = $LibFS->file_read($Location3."/"."control.txt");
		$ContentArr1 = explode("\n",$Content1); 
		
		$LibLibrary = new elibrary($Location3."/"."content.txt");
		$LibLibrary->SET_SUB_TITLE($ContentArr1[0]);
		$LibLibrary->SET_INDEX_PAGE($ContentArr1[1]);
		$LibLibrary->SET_FIRST_PAGE($ContentArr1[2]);	
	
		if ($ContentArr1[3]==1)
		{
			$LibLibrary->CHECK_IDENT(true);
		}
		else
		{
			$LibLibrary->CHECK_IDENT(false);
		}
		if ((trim($ContentArr1[4])=="1") || (trim($ContentArr1[4])=="2") ||  (trim($ContentArr1[4])=="3"))
		{
			$LibLibrary->SET_FOOTNOTES_TYPE(1,$ContentArr1[5]);
		}
		
		$LibLibrary->DO_CONVERSION();
		$LibLibrary->OUTPUT_XML($LibLibrary->ChapterPrepageArr,$LibLibrary->PageContentArr,$Location4."/"."output.xml",$Location3."/image");
		$LibLibrary->OUTPUT_CHAPTER_XML($LibLibrary->ChapterArr,$Location4."/"."chapter.xml");
		
		$saveAsFile = time()."_convert.zip";
		$LibFS->file_zip("convert",$Location2."/".$saveAsFile,$Location2);
		$OutputContent = $LibFS->file_read($Location2."/".$saveAsFile);
			
		$LibFS->folder_remove_recursive($Location3);
		$LibFS->folder_remove_recursive($Location4);
		
		output2browser($OutputContent, "convert.zip");
		
		$LibFS->item_remove($Location2."/".$saveAsFile);
		$LibFS->item_remove($File);
		
		unlink($Location2."/".$saveAsFile);
		unlink($File);
	} // end if zip file
}

intranet_closedb();
//header("Location: index.php?xmsg=add&pageNo=$pageNo&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&noticeType=$noticeType&status=$status&year=$year&month=$month");
//header("Location: convert_book.php");

?>