<?php
//	header("Content-type: text/csv");
//	header("Content-Disposition: attachment; filename=hitrate_report_$source.csv");
//	header("Pragma: no-cache");
//	header("Expires: 0");
		
	$PATH_WRT_ROOT = "../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libelibrary.php");
	
	include_once($PATH_WRT_ROOT."includes/libebookreader.php");
	intranet_auth();
	intranet_opendb();
	
	$str = uniDecode($data,'big-5');

	function uniDecode($str,$charcode){
		$text = preg_replace_callback("/%u[0-9A-Za-z]{4}/",toUtf8,$str);
		return mb_convert_encoding($text, $charcode, 'utf-8');
		}
		function toUtf8($ar){
		foreach($ar as $val){
		$val = intval(substr($val,2),16);
		if($val < 0x7F){        // 0000-007F
		$c .= chr($val);
		}elseif($val < 0x800) { // 0080-0800
		$c .= chr(0xC0 | ($val / 64));
		$c .= chr(0x80 | ($val % 64));
		}else{                // 0800-FFFF
		$c .= chr(0xE0 | (($val / 64) / 64));
		$c .= chr(0x80 | (($val / 64) % 64));
		$c .= chr(0x80 | ($val % 64));
		}
		}
		return $c;
	}
	echo $str;
	
	$fileName = $PATH_WRT_ROOT."file/elibrary/hit_rate.csv";	
	$fileHandle = fopen($fileName, 'w');
	fwrite($fileHandle, $str);	
	fclose($fileHandle);

?>