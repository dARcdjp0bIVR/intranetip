<?php
// editing by CharlesMa

/******************************************* Changes log **********************************************
 * 2013-03-13 (CharlesMa): Able upload cover image
 * 2012-09-12 (CharlesMa): Force to one page mode & one page width
 ******************************************************************************************************/

//ini_set("memory_limit","300M");
ini_set('max_execution_time', 3600);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_auth();
intranet_opendb();


$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";


$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$Title,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();


### step box
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=3, $CustStepArr='');

$linterface->LAYOUT_START();

$libebookreader = new libebookreader();

$error_occured = true;


$success = $libebookreader->Validate_CSV_ImportEPUB("", "upload");


?>
<script>
	$(document).ready(function(){
		
		
	});
	
</script>
<?=$htmlAry['generalImportStepTbl']?>

<form name="form1" id="form1" method="POST" action="import_book_new_validate.php">

	
</form>
<?php

if($success){
	$libebookreader->Update_ImportEPUB();
}

//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
$linterface->LAYOUT_STOP();
ini_set('max_execution_time', 300);
?>
