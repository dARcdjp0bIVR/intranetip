<?php
// editing by CharlesMa

/******************************************* Changes log **********************************************
 * 2013-03-13 (CharlesMa): Able upload cover image
 * 2012-09-12 (CharlesMa): Force to one page mode & one page width
 ******************************************************************************************************/

ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_auth();
intranet_opendb();


$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";


$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}



$InputArray["BookID"] = $BookID;
$InputArray["Title"] = $Title;
$InputArray["Author"] = $Author;
$InputArray["Category"] = $Category;
$InputArray["Level"] = $Level;
$InputArray["AdultContent"] = $AdultContent;
$InputArray["Publisher"] = $Publisher;
$InputArray["Category"] = $Category;
$InputArray["SubCategory"] = $SubCategory;
$InputArray["RelevantSubject"] = $RelevantSubject;
$InputArray["Source"] = $SourceFrom;
$InputArray["Language"] = $Language;
$InputArray["Preface"] = $Preface;
$InputArray["InputBy"] = $InputBy;
$InputArray["SeriesEditor"] = $SeriesEditor;
$InputArray["Publish"] = $Publish;
$InputArray["Copyright"] = "";

//<!----------------  2013-05-30 (CharlesMa)------------------------>	
$InputArray["FirstPublished"] = $FirstPublished;
$InputArray["ePublisher"] = $ePublisher;
$InputArray["CopyrightYear"] = $CopyrightYear;
$InputArray["CopyrightStatement"] = $CopyrightStatement;
//<!----------------  2013-05-30 (CharlesMa) END ------------------------>	

if($SourceFrom == "cup")
{
	// update the copyright format
	$CopyrightTemplate = $LibeLib->GET_COPYRIGHT_TEMPLATE("cup");
	$Copyright = $CopyrightTemplate;
	
	if($JS1 != "") $Copyright = str_replace("<b>[JS1]</b><b>[/JS1]</b>",  "<b>[JS1]</b>".$JS1."<b>[/JS1]</b>", $Copyright);
	if($JS2 != "") $Copyright = str_replace("<b>[JS2]</b><b>[/JS2]</b>",  "<b>[JS2]</b>".$JS2."<b>[/JS2]</b>", $Copyright);
	if($JS3 != "") $Copyright = str_replace("<b>[JS3]</b><b>[/JS3]</b>",  "<b>[JS3]</b>".$JS3."<b>[/JS3]</b>", $Copyright);
	if($JS4 != "") $Copyright = str_replace("<b>[JS4]</b><b>[/JS4]</b>",  "<b>[JS4]</b>".$JS4."<b>[/JS4]</b>", $Copyright);
	if($JS5 != "") $Copyright = str_replace("<b>[JS5]</b><b>[/JS5]</b>",  "<b>[JS5]</b>".$JS5."<b>[/JS5]</b>", $Copyright);
	
	$InputArray["Copyright"] = $Copyright;
}

$new_entry_id = $LibeLib->ADD_BOOK_INFO($InputArray);

$LibeLib->UPDATE_BOOK_TAGS($BookTags, $new_entry_id);

if ($Publish)
{
	$elib_install = new elibrary_install();
	$elib_install->enable_site_book_license($new_entry_id, $UserID);
}

$libebookreader = new libebookreader();

if(isset($_FILES['upload_file']))
{
	
	# to modify this function to follow the bookID
	$ePUBImageBookSetting = array('ImageBook' => $ImageBook, 'ImageWidth' => $ImageWidth);
	$onePageModeSetting = array('ForceToOnePageMode' => $ForceToOnePageMode, 'OnePageModeWidth' => $OnePageModeWidth, 'OnePageModeHeight' => $OnePageModeHeight);
	
	$success = $libebookreader->Upload_EPUB_Book($_FILES, 'upload_file', $new_entry_id, $ePUBImageBookSetting, $onePageModeSetting);
	
	$x .= '<p>Upload EPUB '.$_FILES['upload_file']['name'].($success?' <font color="green">success</font>.' : ' <font color="red">failed</font>.');
	$x .= '</p>';
	if($success)
	{
		# set it as ePub book
		$LibeLib->SET_BOOK_FORMAT($new_entry_id, BOOK_FORMAT_EPUB2);	// existing support format! to be extended
		$book_id = $libebookreader->BookID;
		$x .= '<p><u>BookID '.$book_id.'</u><br /><b>Please wait until this process completes!</b><br />';
		$x .= '<div>Page Mode: <span id="divPageMode"></span> page mode<br />';
		$x .= 'Total number of files: <span id="divTotal"></span><br />';
		$x .= 'Current processing file: <span id="divNum"></span><br />';
		$x .= 'Process status: <span id="divStatus">processing...</span><br />';
		$x .= '</div><br />';
		$x .= '<iframe id="PostProcessFrame" width="1px" height="1px" src="/home/eLearning/ebook_manage/'.$book_id.'/reader/"></iframe></p>';
		//$libebookreader->Parse_EPUB_XML();
		#debug($libebookreader->CoverImagePath);
		$LibeLib->ADD_BOOK_COVER($new_entry_id, $libebookreader->CoverImagePath);
		
		if(isset($_FILES['onePageModeCover']) && $_FILES['onePageModeCover']['tmp_name'] != ""){
			include_once($PATH_WRT_ROOT."includes/libimage.php");
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$photo_des = $intranet_root."/file/elibrary/content/".$new_entry_id."/image";
			$filepath_abs = $_FILES['onePageModeCover']['tmp_name'];
			$image_obj = new SimpleImage();
			$image_obj->load($filepath_abs);
			   $image_obj->resizeToMax(163, 220);
			$image_obj->save($photo_des."/cover.jpg");
		}
		
		if(isset($_FILES['IndexPageImage']) && $_FILES['IndexPageImage']['tmp_name'] != ""){
			include_once($PATH_WRT_ROOT."includes/libimage.php");
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$photo_des = $intranet_root."/file/elibrary/content/".$new_entry_id."/image";
			$filepath_abs = $_FILES['IndexPageImage']['tmp_name'];
			
			move_uploaded_file($filepath_abs, $photo_des."/indexpageimage.png");
		}
		if(isset($_FILES['WorkSheet']) && $_FILES['WorkSheet']['tmp_name'] != ""){
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$ebook_des = $intranet_root."/file/elibrary/content/".$new_entry_id."";
			$filepath_abs = $_FILES['WorkSheet']['tmp_name'];
			
			move_uploaded_file($filepath_abs, $ebook_des."/worksheet.pdf");
		}
	}
}




$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$Title,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();


echo "<table width='90%' align='center' border='0'><tr><td>".$x."</td></tr></table>";

intranet_closedb();


?>

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td align="center">
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button","self.location='index.php'") ?>
	</td>
</tr>
</table>

<?php

//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
$linterface->LAYOUT_STOP();

?>