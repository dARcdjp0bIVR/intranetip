<?php
// Modifing by Charles Ma
/**
 * Change Log:
 * 2018-12-12 Pun [ip.2.5.10.1.1]
 *  - Added ISBN
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// will move to lang file
$button_edit_audio = "Edit Audio";
intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$lu = new libuser($UserID);
$LibeLib = new elibrary();

//echo "admin user? ".$LibeLib->IS_ADMIN_USER($UserID);
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
//echo "eLib_ADMIN? ".$plugin['eLib_ADMIN'];
if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
/////////////////////////////////////////////////////////////////////////////////
// Filtering
/////////////////////////////////////////////////////////////////////////////////
$LangArr = array();
$LangArr[] = array("chi",$i_QB_LangSelectChinese);
$LangArr[] = array("eng",$i_QB_LangSelectEnglish);
if ($intranet_session_language=="en")
{
	$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' onchange='document.form1.submit()' ",$i_status_all."&nbsp;".$i_QB_LangSelect, $Language);
}
else
{
	$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' onchange='document.form1.submit()' ",$i_status_all."".$i_QB_LangSelect, $Language);
}
$TmpArr["Language"] = $Language;


//$AuthorArr = $LibeLib->GET_ALL_AUTHOR_ARRAY();
//if ($intranet_session_language=="en")
//{
//	$AuthorSelect = $linterface->GET_SELECTION_BOX($AuthorArr," name='Author' onchange='document.form1.submit()' ",$i_status_all."&nbsp;".$eLib['Book']["Author"] , $Author);
//}
//else
//{
//	$AuthorSelect = $linterface->GET_SELECTION_BOX($AuthorArr," name='Author' onchange='document.form1.submit()' ",$i_status_all."".$eLib['Book']["Author"] , $Author);
//}
$TmpArr["AuthorID"] = $Author;


//$SourceArr = $LibeLib->GET_ALL_SOURCE_ARRAY();
//if ($intranet_session_language=="en")
//{
//	$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='Source' onchange='document.form1.submit()' ",$i_status_all."&nbsp;".$eLib["SourceFrom"] , $Source);
//}
//else
//{
//	$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='Source' onchange='document.form1.submit()' ",$i_status_all."".$eLib["SourceFrom"] , $Source);
//}
$TmpArr["Source"] = $Source;

//$TmpArr["Link1"] = "import_book_chapter.php";
$TmpArr["Link1"] = "edit_book_info.php";
$TmpArr["Keyword"] = $keyword;

$TmpArr["IsFirstPageBlank"] = "T"; // 2013-10-16 Charles Ma
$TmpArr["IsRightToLeft"] = "T"; // 20140224-twupdate1
/////////////////////////////////////////////////////////////////////////////////


# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 7;
$order = ($order == '') ? 0 : $order;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("BookID", "ISBN", "Title", "Author", "Publisher", "Source", "Language", "DateModified", "Publish", "IsFirstPageBlank", "IsRightToLeft");


$li->sql = $LibeLib->GET_BOOK_OVERVIEW_SQL($TmpArr);
//
$li->IsColOff = "2";
$li->no_col = 13;
$li->column_array = array(12,12,12,12,0,0,0,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Code"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["ISBN"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Title"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Author"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Publisher"])."</td>\n";
$li->column_list .= "<td nowrap='nowrap' class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["SourceFrom"])."</td>\n";
$li->column_list .= "<td nowrap='nowrap' class='tabletop tabletopnolink'>".$li->column($pos++, $i_QB_LangSelect)."</td>\n";
$li->column_list .= "<td nowrap='nowrap' class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["DateModified"])."</td>\n";
$li->column_list .= "<td nowrap='nowrap' class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["html"]["publish"])."</td>\n";
$li->column_list .= "<td nowrap='nowrap' class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["FirstPageBlank"])."</td>\n";
$li->column_list .= "<td nowrap='nowrap' class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["IsRightToLeft"])."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BookID[]")."</td>\n
";

### Button
$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'BookID[]','remove_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'BookID[]','edit_book_info.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";
$PublishBtn 	= "<a href=\"javascript:checkPublish(document.form1,'BookID[]','publish_book_info_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $eLib["html"]["publish"] . "</a>";
$UnpublishBtn 	= "<a href=\"javascript:checkUnpublish(document.form1,'BookID[]','unpublish_book_info_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $eLib["html"]["unpublish"] . "</a>";
$BlankFirstPageBtn 	= "<a href=\"javascript:checkBlankFirstPage(document.form1,'BookID[]','blankfirstpage_book_info_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $eLib['Book']["BlankFirstPage"] . "</a>";
$UnBlankFirstPageBtn 	= "<a href=\"javascript:checkUnblankFirstPage(document.form1,'BookID[]','unblankfirstpage_book_info_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $eLib['Book']["UnblankFirstPage"] . "</a>";
$IsRightToLeftBtn 	= "<a href=\"javascript:checkRightToLeft(document.form1,'BookID[]','isrighttoLeft_book_info_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $eLib['Book']["IsRightToLeft"] . "</a>";
$IsLeftToRightBtn 	= "<a href=\"javascript:checkLeftToRight(document.form1,'BookID[]','islefttoright_book_info_update.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $eLib['Book']["IsLeftToRight"] . "</a>";
//$audioEditBtn 	= "<a href=\"javascript:checkEdit(document.form1,'BookID[]','edit_book_audio.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit_audio . "</a>";
### Title ###

$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"],"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

if ($LibeLib->IS_SCHOOL_ADMIN() || $LibeLib->IS_SYSTEM_ADMIN())			// 20131213-eBookTW
{
   	//$AddBtn 	= "<a href=\"add_book.php\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
    $AddBtn		= $linterface->GET_LNK_NEW("add_book.php",$button_new,"","","",0);
    $AddBtn 	.= $linterface->GET_LNK_IMPORT("import_book_new.php",$button_import,"","","",0);
}

?>

<br />
<form name="form1" method="get" action="index.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	<table width="96%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" class="tabletext">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><p><?=$LangSelect?> <?=$AuthorSelect?> <?=$SourceSelect?></p></td>
					<td align="right"><div class="Conntent_search"><input id="keyword" name="keyword" type="text" value="<?=stripslashes($keyword)?>" /></div></td>
				</tr>
				</table>
				</td>
				<td align="right" valign="bottom">&nbsp;</td>
			</tr>

			<tr>
				<td align="left" valign="middle">
				<?=$AddBtn?>
				</td>
				<td align="right" valign="bottom" height="28">
				<?php
				if($lu->isTeacherStaff())
				{
				?>
					<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
						<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
						<tr>
							<td nowrap><?=$audioEditBtn?>&nbsp;&nbsp;<?=$editBtn?>&nbsp;&nbsp;<?=$delBtn?>&nbsp;&nbsp;<?=$PublishBtn?>&nbsp;&nbsp;<?=$UnpublishBtn?>&nbsp;&nbsp;<?=$BlankFirstPageBtn?>&nbsp;&nbsp;<?=$UnBlankFirstPageBtn?>&nbsp;&nbsp;<?=$IsRightToLeftBtn?>&nbsp;&nbsp;<?=$IsLeftToRightBtn?></td>
						</tr>
						</table>
						</td>
						<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
					</tr>
					</table>
				<?php
				}
				?>
				</td>
			</tr>

			<tr>
				<td colspan="2">
				<?=$li->display();?>
				</td>
			</tr>

			</table>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
<br />

<input type="hidden" name="NoticeID" value="" />
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>


<?php

intranet_closedb();
$linterface->LAYOUT_STOP();

?>