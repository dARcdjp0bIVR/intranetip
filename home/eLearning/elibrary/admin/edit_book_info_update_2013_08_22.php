<?php
// editing by CharlesMa

/******************************************* Changes log **********************************************
 * 2013-01-28 (CharlesMa): Upload_EPUB_Book - add onePageModeSetting
 ******************************************************************************************************/

ini_set("memory_limit","300M");
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");


intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";


$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$InputArray["BookID"] = $BookID;
$InputArray["Title"] = $Title;
$InputArray["Author"] = $Author;
$InputArray["SeriesEditor"] = $SeriesEditor;
$InputArray["Category"] = $Category;
$InputArray["SubCategory"] = $Subcategory;
//$InputArray["RelevantSubject"] = $RelevantSubject;
$InputArray["Level"] = $Level;
$InputArray["AdultContent"] = $AdultContent;
$InputArray["Publisher"] = $Publisher;
$InputArray["Category"] = $Category;
$InputArray["Source"] = $SourceFrom;
$InputArray["Language"] = $Language;
$InputArray["Preface"] = $Preface;
$InputArray["Type"] = $InputBy;
$InputArray["Publish"] = $Publish;
$InputArray["Copyright"] = "";

//<!----------------  2013-05-30 (CharlesMa)------------------------>	
$InputArray["FirstPublished"] = $FirstPublished;
$InputArray["ePublisher"] = $ePublisher;
$InputArray["CopyrightYear"] = $CopyrightYear;
$InputArray["CopyrightStatement"] = $CopyrightStatement;
//<!----------------  2013-05-30 (CharlesMa) END ------------------------>	

if($SourceFrom == "cup")
{
	// update the copyright format
	$CopyrightTemplate = $LibeLib->GET_COPYRIGHT_TEMPLATE("cup");
	$Copyright = $CopyrightTemplate;
	
	if($JS1 != "") $Copyright = str_replace("<b>[JS1]</b><b>[/JS1]</b>",  "<b>[JS1]</b>".$JS1."<b>[/JS1]</b>", $Copyright);
	if($JS2 != "") $Copyright = str_replace("<b>[JS2]</b><b>[/JS2]</b>",  "<b>[JS2]</b>".$JS2."<b>[/JS2]</b>", $Copyright);
	if($JS3 != "") $Copyright = str_replace("<b>[JS3]</b><b>[/JS3]</b>",  "<b>[JS3]</b>".$JS3."<b>[/JS3]</b>", $Copyright);
	if($JS4 != "") $Copyright = str_replace("<b>[JS4]</b><b>[/JS4]</b>",  "<b>[JS4]</b>".$JS4."<b>[/JS4]</b>", $Copyright);
	if($JS5 != "") $Copyright = str_replace("<b>[JS5]</b><b>[/JS5]</b>",  "<b>[JS5]</b>".$JS5."<b>[/JS5]</b>", $Copyright);
	
	$InputArray["Copyright"] = $Copyright;
}

$LibeLib->UPDATE_BOOK_INFO2($InputArray);
$LibeLib->UPDATE_BOOK_TAGS($BookTags, $BookID);

$elib_install = new elibrary_install();
if ($Publish)
{
	$elib_install->enable_site_book_license($BookID, $UserID);
} else
{
	$elib_install->disable_site_book_license($BookID);
}

$libebookreader = new libebookreader();




if(isset($_FILES['upload_file']))
{
	# remove previous ePub
	if (file_exists($_FILES['upload_file']['tmp_name'])){
		$success = $libebookreader->Delete_Book($BookID);		
	}

	# to modify this function to follow the bookID
	$ePUBImageBookSetting = array('ImageBook' => $ImageBook, 'ImageWidth' => $ImageWidth);
	// 2013-01-28 Charles Ma ////////////////////
	$onePageModeSetting = array('ForceToOnePageMode' => $ForceToOnePageMode, 'OnePageModeWidth' => $OnePageModeWidth);
	
	if($BookID==1913){
		$success = $libebookreader->Upload_EPUB_Book_New($_FILES, 'upload_file', $BookID, $ePUBImageBookSetting, $onePageModeSetting,$InputArray);		
	}else{		
		$success = $libebookreader->Upload_EPUB_Book($_FILES, 'upload_file', $BookID, $ePUBImageBookSetting, $onePageModeSetting);
	}
	
	/////////////////////////////////////////
	$x .= '<p>Upload EPUB '.$_FILES['upload_file']['name'].($success?' <font color="green">success</font>.' : ' <font color="red">failed</font>.');
	$x .= '</p>';
	if($success)
	{
		$success_upload_file = true;
		# set it as ePub book
		$LibeLib->SET_BOOK_FORMAT($BookID, BOOK_FORMAT_EPUB2);	// existing support format! to be extended
		$book_id = $libebookreader->BookID;
		$x .= '<p><u>BookID '.$book_id.'</u><br /><b>Please wait until this process completes!</b><br />';
		$x .= '<div>Page Mode: <span id="divPageMode"></span> page mode<br />';
		$x .= 'Total number of files: <span id="divTotal"></span><br />';
		$x .= 'Current processing file: <span id="divNum"></span><br />';
		$x .= 'Process status: <span id="divStatus">processing...</span><br />';
		$x .= '</div><br />';
		$x .= '<iframe id="PostProcessFrame" width="1px" height="1px" src="/home/eLearning/ebook_manage/'.$book_id.'/reader/"></iframe></p>';
		
		//$libebookreader->Parse_EPUB_XML();
		#debug($libebookreader->CoverImagePath);
		$LibeLib->ADD_BOOK_COVER($BookID, $libebookreader->CoverImagePath);
		
				
		$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$Title,"");
		$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();
		
		$linterface->LAYOUT_START();
		
		
		echo "<table width='90%' align='center' border='0'><tr><td>".$x."</td></tr></table>";
		
		
	}
}

if(isset($_FILES['onePageModeCover']) && $_FILES['onePageModeCover']['tmp_name'] != ""){
	include_once($PATH_WRT_ROOT."includes/libimage.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$photo_des = $intranet_root."/file/elibrary/content/".$BookID."/image";
	$filepath_abs = $_FILES['onePageModeCover']['tmp_name'];
	$image_obj = new SimpleImage();
	$image_obj->load($filepath_abs);
	   $image_obj->resizeToMax(163, 220);
	$image_obj->save($photo_des."/cover.jpg");
}

if(isset($_FILES['IndexPageImage']) && $_FILES['IndexPageImage']['tmp_name'] != ""){
	include_once($PATH_WRT_ROOT."includes/libimage.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$photo_des = $intranet_root."/file/elibrary/content/".$BookID."/image";
	$filepath_abs = $_FILES['IndexPageImage']['tmp_name'];
//	$image_obj = new SimpleImage();
//	$image_obj->load($filepath_abs);
//	$image_obj->save($photo_des."/indexpageimage.png",IMAGETYPE_PNG);
	move_uploaded_file($filepath_abs, $photo_des."/indexpageimage.png");

}

if(isset($_FILES['WorkSheet']) && $_FILES['WorkSheet']['tmp_name'] != ""){
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$ebook_des = $intranet_root."/file/elibrary/content/".$BookID."";
	$filepath_abs = $_FILES['WorkSheet']['tmp_name'];
	
	move_uploaded_file($filepath_abs, $ebook_des."/worksheet.pdf");
}

// 2013-05-30 Charles Ma END ////////////////////

//debug_r($InputArray);


//header("Location: index.php");

intranet_closedb();
		
		//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
		//$linterface->LAYOUT_STOP();
	

if($success && $success_upload_file)
{
}else{	
	header("Location: index.php?xmsg=UpdateSuccess");
}
?>