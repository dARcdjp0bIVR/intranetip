<?php
// editing by CharlesMa

/******************************************* Changes log **********************************************
 * 2013-03-13 (CharlesMa): Able upload cover image
 * 2012-09-12 (CharlesMa): Force to one page mode & one page width
 ******************************************************************************************************/

//ini_set("memory_limit","300M");
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_auth();
intranet_opendb();


$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";


$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$Title,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();


### step box
$htmlAry['generalImportStepTbl'] = $linterface->GET_IMPORT_STEPS($CurrStep=2, $CustStepArr='');

$linterface->LAYOUT_START();

$libebookreader = new libebookreader();

$error_occured = true;

if(isset($_FILES['upload_file']) && $_FILES['upload_file']["size"] > 0)
{	
	$success = $libebookreader->Validate_CSV_ImportEPUB($_FILES['upload_file']["tmp_name"], "new");
	
	if($success < 1){
		switch($success){
			case 0:
				$message =  "No records are found.";
			break;
			case -1:
				$message = "Files for upload files did not exist.";
			break;
			case -2:
				$message = "Format of CSV is not correct.";
			break;
			case -3:
				$message = "Uploaded CSV missing.";
			break;
		}	
//		header("location: import_csv_data2.php?xmsg=wrong_header");
//		exit();
	}else {
		// create html table
		$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
		$field = ($field == '') ? 0 : $field;
		$pageNo = ($pageNo == '') ? 1 : $pageNo;
		$page_size = ($numPerPage == '') ? 100 : $numPerPage;
		$li = new libdbtable2007($field, $order, $pageNo);
		$li->field_array = array('BookSubFolder','BookTitle','Author','SeriesEditor','Category','SubCategory',
			'Tag1','Tag2','Level','Publisher','Language','FirstPublished','PublishVersion','ePublisher','Preface','ISBN', 'IsFirstPageBlank', 'IsRightToLeft', 'Status');
		
		$li->sql = "Select 
					if(tei.BookSubFolder = '/', CONCAT('<div style=\"min-width: 100%;background-color: red;\"></div>'), REPLACE ( tei.BookSubFolder , '/', '' ) ) as BookSubFolder,
					if(tei.BookTitle = '', CONCAT('<div style=\"min-width: 100%;background-color: red;\">',' NULL ','</div>'), tei.BookTitle ) as BookTitle,
					if(tei.Author = '', CONCAT('<div style=\"min-width: 100%;background-color: red;\">',' NULL ','</div>'), tei.Author ) as Author,tei.SeriesEditor,tei.Category,
					tei.SubCategory,tei.Tag1,tei.Tag2,tei.Level,tei.Publisher," .
					"tei.Language,tei.FirstPublished,tei.PublishVersion,tei.ePublisher,tei.Preface,tei.ISBN," .
					" if(tei.IsFirstPageBlank = '1', '".$eLib["html"]["yes"]."', '".$eLib["html"]["no"]."' ) as IsFirstPageBlank," .
					" if(tei.IsRightToLeft = '1', '".$eLib["html"]["yes"]."', '".$eLib["html"]["no"]."' ) as IsRightToLeft," .
					" IF (eb.BookID IS NULL, '".$eLib['Book']["New"]."', '".$eLib['Book']["Update"]."') as status
			From 
					INTRANET_TEMP_EBOOK_IMPORT	as tei		
			
			LEFT JOIN INTRANET_EBOOK_BOOK as eb ON tei.BookSubFolder = eb.BookSubFolder				
			";
		$li->no_col = sizeof($li->field_array) + 1;
		$li->IsColOff = "IP25_table";
		$pos = 0;
		$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
		$li->column_list .= "<th width='10%' >".$eLib['Book']["BookID"]."</td>\n";
		$li->column_list .= "<th width='10%'>".$eLib['Book']["Title"]."</td>\n";
		$li->column_list .= "<th width='5%' >".$eLib['Book']["Author"]."</td>\n";
		$li->column_list .= "<th width='5%'>".$eLib['Book']["SeriesEditor"]."</td>\n";		
		$li->column_list .= "<th width='5%'>".$eLib['Book']["Category"]."</td>\n";		
		$li->column_list .= "<th width='5%'>".$eLib['Book']["Subcategory"]."</td>\n";
		$li->column_list .= "<th >".$eLib['Book']["CLC"]."</td>\n";		
		$li->column_list .= "<th >".$eLib['Book']["DDC"]."</td>\n";		
		$li->column_list .= "<th width='3%'>".$eLib['Book']["Level"]."</td>\n";		
		$li->column_list .= "<th width='5%'>".$eLib['Book']["Publisher"]."</td>\n";		
		$li->column_list .= "<th width='3%'>".$eLib['Book']["Language"]."</td>\n";		
		$li->column_list .= "<th width='5%'>".$eLib['Book']["FirstPublished"]."</td>\n";		
		$li->column_list .= "<th >".$eLib['Book']["Edition"]."</td>\n";		
		$li->column_list .= "<th width='7%'>".$eLib['Book']["ePublisher"]."</td>\n";		
		$li->column_list .= "<th width='10%'>".$eLib['Book']["Description"]."</td>\n";		
		$li->column_list .= "<th >".$eLib['Book']["ISBN"]."</td>\n";			
		$li->column_list .= "<th >".$eLib['Book']["IsFirstPageBlank"]."</td>\n";			
		$li->column_list .= "<th >".$eLib['Book']["IsRightToLeft"]."</td>\n";		
		$li->column_list .= "<th >".$eLib_plus["html"]["status"]."</td>\n";		
		$htmlAry['dataTable'] = $li->display();		
		
		$error_occured = false;
	}
}else if ($_FILES['upload_file']["size"] <= 0 ){
	$message =  "CSV file corrupted.";
}


if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_book_new.php'");
}
else
{
	$sql = " Select BookSubFolder From INTRANET_TEMP_EBOOK_IMPORT WHERE BookSubFolder = '/' OR BookTitle = '' OR Author = '' ";
	$invalid_item = $LibeLib->returnArray($sql);
	
	if(count($invalid_item) > 0 ){
		$message = " There are some required field missing in some of the items. ";		
		$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_book_new.php'");
	}else{
		$message = " All items are valid. ";		
		$import_button = $linterface->GET_ACTION_BTN($button_import, "submit" , "return checkInputFrom(this);")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_book_new.php'");
	}
	
	 
}

?>
<script>
	$(document).ready(function(){
		$(".page_no").css("display", "none");
		
	});
	
	function checkInputFrom (){
		if($(".row_suspend").length > 0){
			return false;
		}
		return true;
	}
</script>


<form name="form1" id="form1" method="POST" action="import_book_new_update.php">

	<?=$htmlAry['generalImportStepTbl']?>
	
	<table width="96%" border="0" cellspacing="0" cellpadding="10">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center"><?=$message?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
	<?=$htmlAry['dataTable']?>
	
		<table width="95%" border="0" cellpadding="0" cellspacing="5"
			align="center">
			<tr>
				<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
			</tr>
			<tr>
				<td align="center" colspan="2"><?= $import_button ?>
				</td>
			</tr>
		</table>
</form>
<?php

//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
$linterface->LAYOUT_STOP();

?>
