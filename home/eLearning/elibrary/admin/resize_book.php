<?php
// editing by CharlesMa
/******************************************* Changes **********************************************
  *  2012-09-12 (CharlesMa): Force to one page mode & one page width
 * * 2012-04-03 (Carlos): Add input field Relevant Subject
 **************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$LibeLib = new elibrary();

$LibeBookreader = new libebookreader();

if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if ($BookArr["Title"] != "")
{
	//$BookTitle = " (".iconv("UTF-8", "big5"."//IGNORE", $BookArr["Title"]).")";
	$BookTitle = " (".$BookArr["Title"].")";
}


$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$BookTitle,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

### instruction box
$htmlAry['instructionBox'] = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], "Click [Compress] to compress the ebook");

$sample_file = "ebook_import.csv";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$linterface->LAYOUT_START();


$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
	foreach($eLib['Source'] as $Key => $Value)	
	{
		$SourceArr[] = array($Key,$Value);
	}
}

$tagNameAry = $LibeLib->returnAvailableTag($assocAry=1);
$avaliableTagName = "";
$delim = "";
foreach($tagNameAry as $tag)
{
	$avaliableTagName .= $delim."\"$tag\"";
	$delim = ", ";
}

$compress_book_html =  $LibeBookreader->retrieveCompressBook();

?>


<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
	$(document).ready(function(){
		$('body').on('click', '.bookCompress', function(){
			var bookID = $(this).attr("value");
			var current = $(this);
			$(this).html("Compressing...");
			$.post( "resize_book_update.php", { bookID: bookID }, function( data ) {
				var arr = data.split("|==@==|");			
				if(arr[1]==1)
					$("#book_"+arr[0]).html("Compress Success");
				else{
					$("#book_"+arr[0]).html("Compress Fail");
				}
			}).error(function(){
				current.html("Compress Fail");
			});
			
		});
	});

//-->
</script>


<form  method="post" action="" name="form1" enctype="multipart/form-data" onSubmit="">
<table id="html_body_frame" width="50%" border="0" cellspacing="0" cellpadding="5" style="margin-left: auto;margin-right: auto;">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
	<div style="width:90%;margin-left: 5%;"><?=$htmlAry['instructionBox']?></div>
	<tr>
		<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td><br />
			
			<table id="compress_book" align="center" width="80%" border="0" cellpadding="5" cellspacing="0">
				<?=$compress_book_html?>			
			</table>
			
			</td>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	    <tr>
	    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	    </tr>
	    <tr>
		
		</tr>		
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>                                
		</td>
	</tr>
</table>

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>