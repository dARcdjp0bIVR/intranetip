<?php
// editing by Paul
/******************************************* Changes **********************************************
  *  2012-09-12 (CharlesMa): Force to one page mode & one page width
 * * 2012-04-03 (Carlos): Add input field Relevant Subject
 **************************************************************************************************/
header("Access-Control-Allow-Origin: http://192.168.0.149"); 
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_opendb();

if(!function_exists('json_encode'))
{
    function json_encode($a=false)
    {
        // Some basic debugging to ensure we have something returned
        if (is_null($a)) return 'null';
        if ($a === false) return 'false';
        if ($a === true) return 'true';
        if (is_scalar($a))
        {
            if (is_float($a))
            {
                // Always use '.' for floats.
                return floatval(str_replace(',', '.', strval($a)));
            }
            if (is_string($a))
            {
                static $jsonReplaces = array(array('\\', '/', "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
                return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
            }
            else if(is_int($a)){
                return '"'.$a.'"';            	
            }
            else return $a;
        }
        $isList = true;
        for ($i = 0, reset($a); true; $i++) {
            if (key($a) !== $i)
            {
                $isList = false;
                break;
            }
        }
        $result = array();
        if ($isList)
        {
            foreach ($a as $v) $result[] = json_encode($v);
            return '[' . join(',', $result) . ']';
        }
        else
        {
            foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
            return '{' . join(',', $result) . '}';
        }
    }
}

$LibeLib = new elibrary();

$source_sql = "";

if($source){
	$source_sql = " AND Source = '$source' " ;
}

$current_year = (int)date("Y");
$previous_year = $current_year - 1;
$previous_previous_year = $current_year - 2;

if($isTemplate){	
	$sql = " SELECT Title, BookID, Source, 0 AS HitRateC, 0 AS HitRateP
	   			FROM  INTRANET_ELIB_BOOK   
			     WHERE Publish = 1 $source_sql ORDER BY BookID ";
}else{
	$sql = " SELECT BookID, Source
	   			FROM  INTRANET_ELIB_BOOK   
			     WHERE Publish = 1 AND HitRate >= 1 $source_sql ORDER BY BookID ";	
}

$result = $LibeLib->returnArray($sql);

$hit_rate_Arr = array();

if($isTemplate){
	$hit_rate_Arr[0] = array();
	
	$hit_rate_Arr[0]["Title"] = "Title";
		
	$hit_rate_Arr[0]["BookID"] = "BookID";
	$hit_rate_Arr[0]["Source"] = "Source";
	$hit_rate_Arr[0]["HitRateC"] = "HitRate $previous_year-$current_year ";
	$hit_rate_Arr[0]["HitRateP"] = "HitRate $previous_previous_year-$previous_year";
}


foreach($result as $key=>$value){
	$BookID = (string) $value['BookID'];
	$hit_rate_Arr[$BookID] = array();
	
	if($isTemplate)
		$hit_rate_Arr[$BookID]["Title"] = $value["Title"];
		
	$hit_rate_Arr[$BookID]["BookID"] = $value["BookID"];
	$hit_rate_Arr[$BookID]["Source"] = $value["Source"];
	$hit_rate_Arr[$BookID]["HitRateC"] = 0;
	$hit_rate_Arr[$BookID]["HitRateP"] = 0;
}

if(!$isTemplate){
	$sql = " SELECT BookID, COUNT(*) as count FROM INTRANET_ELIB_BOOK_HISTORY WHERE DateModified > '$previous_year-09-01' AND DateModified < '$current_year-08-31' GROUP BY BookID ";	
	$result = $LibeLib->returnArray($sql);
	
	foreach($result as $key=>$value){
		$BookID = (string) $value['BookID'];
		if($hit_rate_Arr[$BookID]["BookID"])
			$hit_rate_Arr[$BookID]["HitRateC"] = $value["count"];
	}	
	
	$sql = " SELECT BookID, COUNT(*) as count FROM INTRANET_ELIB_BOOK_HISTORY WHERE DateModified > '$previous_previous_year-09-01' AND DateModified < '$previous_year-08-31' GROUP BY BookID ";	
	$result = $LibeLib->returnArray($sql);
	
	foreach($result as $key=>$value){
		$BookID = (string) $value['BookID'];
		if($hit_rate_Arr[$BookID]["BookID"])
			$hit_rate_Arr[$BookID]["HitRateP"] = $value["count"];
	}	
}


echo(json_encode($hit_rate_Arr));

?>