<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$BookIDArr = $BookID;

$LibeLib->REMOVE_BOOK_INFO($BookIDArr);

$libebookreader = new libebookreader();

# remove ePub
for ($i=0; $i<sizeof($BookIDArr); $i++)
{
	$success = $libebookreader->Delete_Book($BookIDArr[$i]);
}

intranet_closedb();

header("Location: index.php?xmsg=DeleteSuccess");


?>