<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_opendb();
###########################################

$LibeLib = new elibrary();
$objInstall = new elibrary_install();

$BookID = isset($_REQUEST['BookID'])? trim($_REQUEST['BookID']) : "";

if($BookID ==""){
	echo "No BookID given";
	die();
}


$sql = "SELECT 
			BookLicenseID,
			SUBSTRING((BookID+100000000), 2) as code, 
			BookID,
			NumberOfCopy,
			InputDate
		FROM 
			INTRANET_ELIB_BOOK_LICENSE 
		WHERE 
			BookID = ".$BookID." 
		ORDER BY 
			InputDate";

$aryRs = $LibeLib->returnArray($sql);

$sql = "SELECT 
			SUM(bl.NumberOfCopy), b.Title  
		FROM 
			INTRANET_ELIB_BOOK_LICENSE as bl
		LEFT JOIN 
			INTRANET_ELIB_BOOK as b
		ON
			bl.BookID = b.BookID 
		WHERE 
			bl.BookID = ".$BookID."
		GROUP BY
			bl.BookID";
			
	
$aryBookInfo 	= $LibeLib->returnArray($sql);

$aryFinalQuota 	= $aryBookInfo[0][0];
$BookTitle 		= $aryBookInfo[0][1];

$aryRsOrig 		= $objInstall->check_book_license_quota_is_original($BookID);
$aryIsOriginal 	= $aryRsOrig[0];
$hasInvalidQuota = false;
####################################################

?>
<div style="height:480px;">
	<div style="height:30px;padding:0 0 0 10px;">
	<h3>Book : <?=$BookTitle?></h3><BR />
	</div>

	<div id="display_content" style="height:420px;padding:0 0 0 10px;overflow-y:auto;">
	<table width="100%" height="95%" border="0" cellpadding="0" cellspacing="0">
		<tr style="font-weight:bold;background:#CCC;" height="30px">
			<td width="100">Code</td>
			<td width="100">BookID</td>
			<td width="*">InputDate</td>
			<td width="100">Quota</td>			
		</tr>
		<?php
		$rsCount	= count($aryRs);
		$BgColor 	= '#EEE'; 
		$curColor 	= "";
		
		for($i=0; $i<$rsCount; $i++){			
		?>
			<tr style="background:<?=$curColor?>" valign="top">
				<td><?=$aryRs[$i]['code']?></td>
				<td><?=$aryRs[$i]['BookID']?></td>
				<td><?=$aryRs[$i]['InputDate']?></td>
				<td><?=$aryRs[$i]['NumberOfCopy']?>
				<?php
				if($aryIsOriginal[$aryRs[$i]['BookLicenseID']] != "TRUE"){
					echo "<font color='red'>*</font>";
					$aryFinalQuota -= $aryIsOriginal[$aryRs[$i]['BookLicenseID']];
					$hasInvalidQuota = true;
				}
				?>				
				</td>				
			</tr>
		<?php
			$curColor = ($curColor == "")? $BgColor : "";
		}
		?>
		<tr><td colspan="4" height="20px">&nbsp;</td></tr>		
		<tr height="30" style="font-weight:bold;" valign="bottom">
			<td colspan="3" align="right" >Enabled Quota :</td><td > <?=$aryFinalQuota?></td>
		</tr>
		
		<tr height="30"valign="bottom">
			<td colspan="4" align="right" >
				<HR />
				<?php if($hasInvalidQuota){?>
				<font color='red'>Note: (*) Quota does not match original, it has been modified.</font></td>
				<?php } ?>
		</tr>
		
		
	</table>
	
	</div>
	<div id="btn_panel" style="height:40px;background:#EEE;">
		<table width="100%"><tr><td valign="center" align="center">
		<input type="button" name="close_booklist" id="close_booklist" value="Close" onClick="window.parent.tb_remove();"/>
		</td></tr></table>
	</div>
</div>
<?php

###########################################
intranet_closedb();
?>