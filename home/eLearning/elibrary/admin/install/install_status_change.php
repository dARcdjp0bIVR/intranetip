<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


intranet_opendb();
###########################################


$LibeLib = new elibrary();
$linterface 	= new interface_html("popup5.html");

$getBookIDList 	= (isset($_REQUEST['getBookIDList']))? trim($_REQUEST['getBookIDList']) : "";
$strBookList 	= (isset($_REQUEST['strBookList']))? trim($_REQUEST['strBookList']) : "";
$enable 		= (isset($_REQUEST['enable']))? trim($_REQUEST['enable']) : "";

$confirm_btn_disabled = "";

################################################################################
################################################################################

$x = "";
$x .= '<div style="height:490px;width:628px;" id="display_content">';	
$x .= '<div style="height:417px;width:100%;overflow-y:auto;">';

########################################
## Main display block	
$x .= '<BR />';
$x .= '<table width="100%" cellpadding="0" cellspacing="0"><tr><td align="center">';
$x .= '<table width="98%" cellpadding="0" cellspacing="0">';
if(!empty($getBookIDList) && $strBookList){
	## Get List of books to be updated
	$sql = "SELECT
			 	*, 
			 	SUBSTRING((BookID+100000000), 2) as code
			FROM 
				INTRANET_ELIB_BOOK
			WHERE 
				BookID in (".$strBookList.")";
	$aryBookList = $LibeLib->returnArray($sql);
	 	
	if($aryBookList != array()){		
		
		$x .= '<tr height="30px" style="font-weight:bold;background:#CCC;"><td>#</td><td>Code</td><td>Title</td></tr>';
		foreach($aryBookList as $b => $info){
			$x .= '<tr height="30px">';
			$x .= '<td width="30px">'.($b+1).'.</td>';
			$x .= '<td width="100px">'.$info['code'].'</td>';
			$x .= '<td width="*">'.$info['Title'].'</td></tr>';
		}		
	}
}else{
	## Disable confirm button
	$confirm_btn_disabled = "DISABLED";
	$x .= "<tr><td><BR /><BR /><BR />No books selected</td></tr>";
}
$x .= '</table>';
$x .= '</td></tr></table>';
$x .= '</br></div>';

########################################
## Button Panel
$x .= '<div id="btn_panel" style="height:70px;">';
$x .= '<HR />&nbsp;&nbsp;&nbsp;Total: '.count($aryBookList).' Book(s) <BR /> <BR />';
$x .= '<div style="background:#EEE" >';
$x .= '<table width="100%" height="50px"><tr><td align="center" valign="center">';
$x .='<input type="button" name="submit_booklist" id="submit_booklist" value="Confirm Update" onClick="confirm_udpate();" '.$confirm_btn_disabled.' />';
$x .= '<input type="button" name="cancel_booklist" id="cancel_booklist" value="Cancel" onClick="window.parent.tb_remove();"/>';
$x .= '</td></tr></table>';
$x .= '</div>';
$x .= '</div>';

$x .= '</div>';
################################################################################
################################################################################


$linterface->LAYOUT_START();
?>

<script language="javascript">
	$(document).ready(function(){
				
		//Get BookID list
		if($("#getBookIDList").val() == ""){
			var strBookList = "";
			$("input[name=BookID[]][checked]", window.parent.document).each(function(){
				strBookList += (strBookList!="")? ",":"";
				strBookList += $(this).val();
			});
			
			$("#strBookList").val(strBookList);
			$("#getBookIDList").val(1);
			
			document.form1.submit();
		}
	});
	
	function confirm_udpate(){
				
		$.post('ajax.php', 
			{action: 'update_site_book_status', strBookID:$("#strBookList").val(), enable:'<?=$enable?>' },
			function(data){				
				$("#display_content").html(data);
			});
	}
	
	function close_and_refresh_parent(){
		window.parent.location.reload();
		window.parent.tb_remove();
	}
</script>

<form name="form1" method="post" style="margin:0px;padding:0px">


<?= $x ?>

<input type="hidden" name="getBookIDList" id="getBookIDList" value="<?=$getBookIDList?>" />
<input type="hidden" name="strBookList" id="strBookList" value="<?=$strBookList?>" />

</form>

<?php

$linterface->LAYOUT_STOP();

###########################################
intranet_closedb();
?>