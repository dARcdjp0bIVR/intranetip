<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();
###########################################

$arySucc  = array();
$aryFail  = array();
$aryQuota = array();
$aryExtraInfo = array();
$aryExtraHeader = array();


$li = new libdb();
$limport = new libimporttext();
$objInstall = new elibrary_install();

$name = $_FILES['csvfile']['name'];

if (!preg_match("/(\.csv|\.txt)$/i",strtolower($name))){	
	intranet_closedb();
	header("location: install_import.php?type=status&xmsg=import_failed");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
$col_name = array_shift($data);

$file_format = array("BookID", "Quota");
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	
	intranet_closedb();
	header("location: install_import.php?type=status&xmsg=".$returnMsg);
	exit();
}


foreach($data as $key => $info)
{
	$is_succ = "";
	list($importBookID, $importQuota, $isEnabled) = $info;
	
	$importBookID 	= trim($importBookID);
 	$importQuota	= trim($importQuota);
	
	if(!is_numeric($importBookID) || ($importQuota=="")){
		$aryFail[$key] = array("BookID"=>str_pad($importBookID, 8, "0", STR_PAD_LEFT), "Quota"=>$importQuota);
 		continue;
 	}
 	
 	## 
 	$is_succ = $objInstall->add_book_license_quota($importBookID, $importQuota); 
 		
 	if($is_succ){
 		//array_push($arySucc, str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 		$arySucc[$key] = array("BookID"=>str_pad($importBookID, 8, "0", STR_PAD_LEFT), "Quota"=>$importQuota);
 	}else{
 		//array_push($aryFail, str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 		$aryFail[$key] = array("BookID"=>str_pad($importBookID, 8, "0", STR_PAD_LEFT), "Quota"=>$importQuota);
 	}
 	
 	//array_push($aryQuota, $importQuota);
 
}

## STEP 1: Setup extra info to pass into gen_result_ui function
/*$succ_fail_header	= "BookID";
$aryExtraHeader[0] 	= "Quota";
$aryExtraInfo[0] 	= $aryQuota;*/

## STEP 2: Display import results
echo $objInstall->gen_result_ui($arySucc,$aryFail, true);



###########################################
intranet_closedb();
?>