<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


intranet_opendb();
###########################################

$arySucc = array();
$aryFail = array();

$LibeLib = new elibrary();
$li = new libdb();
$limport = new libimporttext();
$lo = new libfilesystem();
$objInstall = new elibrary_install();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if (!preg_match("/(\.csv|\.txt)$/i",strtolower($name))){	
	intranet_closedb();
	header("location: install_import.php?type=status&xmsg=import_failed");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);

$col_name = array_shift($data);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$file_format = array("BookID", "UserID", "Enable");
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

$numOfData = count($data);
if($format_wrong || $numOfData==0)
{
	$returnMsg = ($format_wrong)? 'import_header_failed' : 'import_no_record';
	
	intranet_closedb();
	header("location: install_import.php?type=status&xmsg=".$returnMsg);
	exit();
}



foreach($data as $key => $info)
{
	$is_succ = "";
	list($importBookID, $importUserID, $isEnabled) = $info;
	if(!is_numeric($importBookID) || !is_numeric($importUserID)){
		$aryFail[$key] = array("BookID"=>str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 		continue;
 	}
 	
 	$importBookID = trim($importBookID);
 	$importUserID = trim($importUserID);
 	$isEnabled	  = strtolower(trim($isEnabled));
 	
 	##
 	if($isEnabled=="y")
 	{
 		$InputArray["BookID"] = array($importBookID);
		$InputArray["Publish"] = 1;
		$LibeLib->UPDATE_BOOK_PUBLISH($InputArray);
 		$is_succ = $objInstall->enable_site_book_license($importBookID, $importUserID);
 	} else
 		$is_succ = $objInstall->disable_site_book_license($importBookID);
 		
 	if($is_succ){
 		//array_push($arySucc, str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 		$arySucc[$key] = array("BookID"=>str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 	}else{
 		//array_push($aryFail, str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 		$aryFail[$key] = array("BookID"=>str_pad($importBookID, 8, "0", STR_PAD_LEFT));
 	} 
}

## Display import results
echo $objInstall->gen_result_ui($arySucc,$aryFail);

?>
<script>
document.body.style.margin = "0px";
</script>
<?php

###########################################
intranet_closedb();
?>