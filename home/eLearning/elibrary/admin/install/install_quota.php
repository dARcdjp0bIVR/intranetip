<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_opendb();
###########################################


$LibeLib = new elibrary();
$linterface 	= new interface_html("popup5.html");

$getBookIDList 	= (isset($_REQUEST['getBookIDList']))? trim($_REQUEST['getBookIDList']) : "";
$strBookList 	= (isset($_REQUEST['strBookList']))? trim($_REQUEST['strBookList']) : "";
$enable 		= (isset($_REQUEST['enable']))? trim($_REQUEST['enable']) : "";

$aryBookList = array();
$x = "";


if(!empty($getBookIDList) && $strBookList){
	## Get List of books to be updated
	$sql = "SELECT
			 	*, 
			 	SUBSTRING((BookID+100000000), 2) as code			 	
			FROM 
				INTRANET_ELIB_BOOK
			WHERE 
				BookID in (".$strBookList.")";
	$aryBookList = $LibeLib->returnArray($sql);
}
	
	
		$x .= '<div style="height:490px;width:627px;background:white;">';		
		$x .= '<div id="display_content" style="height:425px;overflow-y:auto;">';
		$x .= '<table width="100%" cellpadding="0" cellspacing="0">';
		if($aryBookList != array()){
		$x .= '<tr height="30px" style="font-weight:bold;background:#CCC;"><td>#</td><td>Code</td><td>Title</td>';
		$x .= '<td><input type="text" name="quota_all" id="quota_all" /><input type="button" name="apply_all" id="apply_all" value="Apply All" onClick="apply_all_quota();"/></td>';
		$x .= '</tr>';
		
		foreach($aryBookList as $b => $info){
			$x .= '<tr height="30px">';
			$x .= '<td width="30px">'.($b+1).'.</td>';
			$x .= '<td width="100px">'.$info['code'].'</td>';
			$x .= '<td width="*">'.$info['Title'].'</td>';
			$x .= '<td width="*"><input type="text" class="quota" name="quota['.$info['BookID'].']" id="'.$info['BookID'].'" value="'.$info['NumberOfCopy'].'" /> </td>';
			$x .= '</tr>';
		}
		}else{
			$x .= '<tr><td><BR /><BR /><BR /><BR /><BR /><center>No books selected</center</td></tr>';
		}
		
		$x .= '</table>';
		$x .= '</br></div>';
		$x .= '<div id="btn_panel" style="height:80px;background:#EEE;text-align:center;">';
		$x .= '<HR /><B>Total: '.count($aryBookList).' Book(s) </b><BR /> <BR />';
		$x .='<input type="button" name="submit_booklist" id="submit_booklist" value="Confirm Update" onClick="confirm_update();"/>';
		$x .= '<input type="button" name="cancel_booklist" id="cancel_booklist" value="Cancel" onClick="window.parent.tb_remove();"/>';
		$x .= '</div>';
		$x .= '</div>';
	


$linterface->LAYOUT_START();
?>

<script language="javascript">
	$(document).ready(function(){
				
		//Get BookID list
		if($("#getBookIDList").val() == ""){
			var strBookList = "";
			$("input[name=BookID[]][checked]", window.parent.document).each(function(){
				strBookList += (strBookList!="")? ",":"";
				strBookList += $(this).val();
			});
			
			$("#strBookList").val(strBookList);
			$("#getBookIDList").val(1);
			
			document.form1.submit();
		}
	});
	
	function get_quota_list(){
		var quota_list = [];
		var BookID_list = [];
		var index=0;
		$('.quota').each(function() {
			
			quota_list[index] = $(this).val();			
			BookID_list[index] = $(this).attr('id');

			index++;
		});
		
		var result=[quota_list, BookID_list];
		return result;
	}
	
	function confirm_update(){
		var quota_check = true;
		$("input.quota").each(function(){
			if(!isInteger(Trim($(this).val()))){
				alert("Please enter numeric value for quota");
				$("#quota_all").focus();
				quota_check = false;
				return false;
			}
		});
		if(!quota_check){
			return;
		}			
					
		var quota_bookID = get_quota_list();
		var quota_list = quota_bookID[0];
		var strBookID = quota_bookID[1];
		
		$.post('ajax.php', 
			{action: 'update_student_book_quota', 'aryBookID[]': strBookID, 'aryBookQuota[]':quota_list},
			function(data){
				$('body').html(data);				
				//$("#display_content").html(data);
				//$("#btn_panel").html('<input type="button" name="close_booklist" id="close_booklist" value="close" onClick="close_and_refresh_parent();"/>');
			});
	}
	
	function close_and_refresh_parent(){
		window.parent.location.reload();
		window.parent.tb_remove();
	}
	
	function apply_all_quota(){
		var quote_amt = Trim($("#quota_all").val());
		if( quote_amt== "" || !isInteger(quote_amt)){
			alert("Please enter numeric value for quota");
			$("#quota_all").val("");
			$("#quota_all").focus();
			return;
		}
		
		if(confirm("Are you sure you want to apply quota to all books?")){
			$("input.quota").val($("#quota_all").val());
		}		
	}
</script>
<form name="form1" method="post" style="margin:0px;padding:0px">

<?= $x ?>

<input type="hidden" name="getBookIDList" id="getBookIDList" value="<?=$getBookIDList?>" />
<input type="hidden" name="strBookList" id="strBookList" value="<?=$strBookList?>" />

</form>

<?php

$linterface->LAYOUT_STOP();

###########################################
intranet_closedb();
?>