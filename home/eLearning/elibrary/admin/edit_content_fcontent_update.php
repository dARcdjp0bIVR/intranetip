<?php

//using by 

######################################################################################################
#2010-03-29 : adam: modify to support cut="true" 

######################################################################################################

@set_time_limit(3600);
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if($OrigPage != $PageID)
  $PageID = $OrigPage;

$ChapterID = 0;
$SubChapterID = 0;

if($PageID == 0)
{
  $LibeLib->DELETE_BOOK_CHAPTER($BookID);
  for($i=0; $i<count($Title); $i++)
  {
    if(sizeof($IsDelete) == 0 || !in_array($i, $IsDelete))
    {
      if($IsChapter[$i] == 1)
      {
        $ChapterID++;
        $SubChapterID = 0;
      }
      else
      {
        $SubChapterID++;
      }
      $ChapterInfo = array("BookChapterID"=>$BookChapterID[$i],
                            "ChapterID"=>$ChapterID,
                            "SubChapterID"=>$SubChapterID,
                            "Title"=>$Title[$i],
                            "OrigPageID"=>$OrigPageID[$i],
                            "IsChapter"=>$IsChapter[$i]);
                            
      $LibeLib->ADD_BOOK_CHAPTER($BookID, $ChapterInfo);
    }
  }
}
else
{
	$fields = "IsChapterStart = '".$IsChapterStart."',";
	$fields .= "ChapterID = '".$ChapterID."',";
	
	$Location = $intranet_root."/file/elibrary/content/".$BookID."/image";
	$ExistFile = explode(",",$ExistImage);
	$KeepFile = array();
	$new_content = "";
	$new_content_xml = "";
	$idx_f = 0;
	$idx = 0;

//DEBUG_R($Content);
//DEBUG_R($IsDelete);
//DIE();
	chmod($intranet_root."/file/elibrary/content/", 0777);
	chmod($intranet_root."/file/elibrary/content/".$BookID, 0777);
	chmod($intranet_root."/file/elibrary/content/".$BookID."/image", 0777);

  for($i=0; $i<count($p_type); $i++)
  {
    if(sizeof($IsDelete) == 0 || !in_array($i, $IsDelete))
    {
      // If content is a file
      if($p_type[$i] == "image")
      {
        $file_n = $OldContent[$i]==""?basename($_FILES['ContentF']['name'][$idx_f]):$OldContent[$i];
      
        if($_FILES['ContentF']['tmp_name'][$idx_f] != "")    // a file is uploaded
        {  
          $temp_file = $_FILES['ContentF']['tmp_name'][$idx_f];
          move_uploaded_file($temp_file,$Location."/".$file_n);
          
          chmod($Location."/".$file_n, 0777);
        }

        if($file_n != "")
        {
          $new_content .= "<img src=\"image/".$file_n."\" alt=\"Image\"></img>\n"; 
          $new_content_xml .= "<p class=\"".$Class[$i]."\"";
		  if($Cut[$i] == true || $Cut[$i] == "true"){
			$new_content_xml .= "cut=\"true\"";
		  }	
		  $new_content_xml .=	"><img src=\"image/".$file_n."\" alt=\"Image\"></img></p>\n";
          
          $KeepFile[] = $file_n;
        }
        $idx_f++;
      }
      
      // If content is text
      else
      {
        $new_content .= $Content[$idx]."\n";
        $new_content_xml .= "<p class=\"".$Class[$i]."\"";
		if($Cut[$i] == true || $Cut[$i] == "true"){
			$new_content_xml .= "cut=\"true\"";
		}
		$new_content_xml .= ">".$Content[$idx]."</p>\n";
        $idx++;
      }
    }
    else
    {
      if($p_type[$i] == "image")    // corresponding file needs to be deleted
        $idx_f++;
      else
        $idx++;
    }
  }
  
  if($new_content != "")
    $fields .= "Content = '".trim($new_content)."',";
  if($new_content_xml != "")
    $fields .= "ContentXML = '".trim($new_content_xml)."',";
  $fields = substr($fields, 0, -1);
  
  $Sql = "UPDATE
            INTRANET_ELIB_BOOK_PAGE
          SET
            $fields
          WHERE
            BookID = '".$BookID."' AND
            PageID = '".$PageID."' 
          ";
	$LibeLib->db_db_query($Sql);
/*
echo $Sql."<br />";
echo mysql_error()."<br />";

print_r($ExistFile); 
print_r($KeepFile);

die();          
*/    
  // Delete files
  if(is_array($ExistFile) && is_array($KeepFile))
  {
    $DeleteFiles = array_diff($ExistFile, $KeepFile);
  
    if(is_array($DeleteFiles))
    {
      foreach($DeleteFiles as $DeleteFile)
        if(is_file($Location."/".$DeleteFile))
          unlink($Location."/".$DeleteFile);
    }
  }
}

intranet_closedb();

header("Location: view_content.php?BookID=".$BookID."&pageid=".$PageID);


?>
