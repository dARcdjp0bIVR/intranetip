<?php
// editing by CharlesMa
/******************************************* Changes **********************************************
  *  2012-09-12 (CharlesMa): Force to one page mode & one page width
 * * 2012-04-03 (Carlos): Add input field Relevant Subject
 **************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$LibeLib = new elibrary();

$LibeBookreader = new libebookreader();

if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if ($BookArr["Title"] != "")
{
	//$BookTitle = " (".iconv("UTF-8", "big5"."//IGNORE", $BookArr["Title"]).")";
	$BookTitle = " (".$BookArr["Title"].")";
}


$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$BookTitle,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

### instruction box
//$htmlAry['instructionBox'] = $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], "Click [Compress] to compress the ebook");

$sample_file = "ebook_import.csv";
$csvFile = "<a class=\"tablelink\" href=\"get_sample_file.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$linterface->LAYOUT_START();


$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
	foreach($eLib['Source'] as $Key => $Value)	
	{
		$SourceArr[] = array($Key,$Value);
	}
}

$tagNameAry = $LibeLib->returnAvailableTag($assocAry=1);
$avaliableTagName = "";
$delim = "";
foreach($tagNameAry as $tag)
{
	$avaliableTagName .= $delim."\"$tag\"";
	$delim = ", ";
}

//////////////////////
$publisher_html .= "<option value =''>All publishers</option>";

foreach($eLib['Source'] as $key => $value){
	if($key != "-")
	$publisher_html .= "<option value ='$key'>$value</option>";
}		

$report_book_html = "<select id='publisher'>$publisher_html</select>";

$report_book_html .= "<button id='generate_btn' type='button'>Generate</button>";

//////////////////////

$school_list = array("eclass.cpc.edu.hk", "intranet.pakkau.edu.hk", "202.175.114.30", "113.28.114.26");
array_push($school_list, "eclass.puiching.edu.mo", "203.198.184.235", "eclass.lkpfc.com","218.188.23.99","eclass.shingtak.edu.hk", "eclass.bckss.edu.hk");
array_push($school_list, "eclass.ylmass.edu.hk", "eclass.hofung.edu.hk", "eclass.tkp.edu.hk", "eclass.cfss.edu.hk", "learning.swcs.edu.hk", "eclass.sgss.edu.hk",
"intranet.fyk.edu.hk", "eclass.hfcc.edu.hk", "218.188.3.36", "eclass.lstwcm.edu.hk", "eclass.yy2.edu.hk", "eclass.wss.edu.hk","eclass.jcefs.edu.hk",
"210.0.198.234", "intranet.sfac.edu.hk","58.177.253.164","eclass.kfims.edu.hk","eclass.csa.edu.hk","intranet.sjacs.edu.hk","eclass.plkwyc.edu.hk",
"eclass.chkpcc.net","eclass.sagc.edu.hk","eclass.lck.edu.hk","eclass.lpsmc.edu.hk","elearn.hcs.edu.hk","218.188.1.199","eclass.cpcydss.edu.hk","eclass.ymca-coll.edu.hk",
"eclass.szetoho.edu.hk","intranet.plklsp.edu.hk","eclass.ktmc.edu.hk","eclass.stlouis.edu.hk","eclass.stmgss.edu.hk","eclass.tsk.edu.hk","eclass.uccke.edu.hk",
"eclass.qmss.edu.hk","eclass.wcbss.edu.hk","eclass.tackching.edu.hk","intra.valtorta.edu.hk","eclass.ats.edu.hk","eclass.sdbnsm.edu.hk","eclass.fms.edu.hk",
"intranet.blmcss.edu.hk","mail.twghscgms.edu.hk","eclass.twghkyds.edu.hk","eclass.lamhonkwong.edu.hk","eclass.puikiucollege.edu.hk","eclass.pkms.edu.hk",
"eclass.blcwc.edu.hk","eclass.cchpwss.edu.hk","eclass.cccmmwc.edu.hk","eclass.kcckc.edu.hk","elearning.puishing.edu.hk","113.28.44.14","eclass.mcdhmc.edu.hk",
"eclass.rhs.edu.hk","203.198.184.169","eclass.ccsc.edu.hk","eclass.henrietta.edu.hk","eclass.pas.edu.hk");

//$school_list = array("192.168.0.149");
$school_list_EJ = array("eclass.ymtcps.edu.hk","eclass.sjacps.edu.hk","intranet.kwmwps.edu.hk","202.175.69.146","eclass.sfaeps.edu.hk","ec.kfps.edu.hk");
array_push($school_list_EJ,
"intra.qesosaps.edu.hk","eclass.cpswts.edu.hk","campus.waichow.edu.hk","eclass.wisdom.edu.hk","eclass.fls.edu.hk","eclass.hingtak.edu.hk","intranet.skhstandrews.edu.hk",
"203.198.185.118","203.198.162.148","eclass.apsw.edu.hk","eclass.kmsch.edu.hk","intranet.cmsnp.edu.hk","eclass.lmc.edu.hk");

?>


<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
	function JSON2CSV(objArray) {
	    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;	        
	    var str = '';
	    var line = '';
	    if ($("#labels").is(':checked')) {
	        var head = array[0];
	        if ($("#quote").is(':checked')) {
	            for (var index in array[0]) {
	                var value = index + "";
	                line += '"' + value.replace(/"/g, '""') + '"|';
	            }
	        } else {
	            for (var index in array[0]) {
	                line += index + '|';
	            }
	        }
	
	        line = line.slice(0, -1);
	        str += line + '\r\n';
	    }
	
	    for (var i in array) {
	        var line = '';
	
	        if ($("#quote").is(':checked')) {
	            for (var index in array[i]) {
	                var value = array[i][index] + "";
	                line += '"' + value.replace(/"/g, '""') + '"|';
	            }
	        } else {
	            for (var index in array[i]) {
	                line += array[i][index] + '|';
	            }
	        }
	
	        line = line.slice(0, -1);
	        str += line + '\r\n';
	    }
	    return str;
	    
	}

	var school_list = new Array('<?=implode("','",$school_list)?>');
	var school_list_EJ = new Array('<?=implode("','",$school_list_EJ)?>');
	
	var i = 0;var j = 0;
	var report_start = false;
	var hit_rate_obj = ""; 
	
	
	var printHitRate = function(){
		var csv = JSON2CSV(hit_rate_obj);//
		
		var data = JSON.stringify(hit_rate_obj);
		var source = $("#publisher").val() != "" ? "all" : $("#publisher").val();
//   	$("#download_csv").html('<a href="data:text/csv;charset=utf-8,'+escape(csv)+'" download="hitrate_report_'+source+'.csv">Download</a>');
   		$("#download_csv").html('<a href="report_book_download.php?source=hit_rate" >Download</a>');  
   		
   		$.post("report_book_update.php",{data:csv, source: source},function(result){
		    
		});

	}
	
	var loopArray_EJ = function(arr){
		var index = i+1;
	
		$("#report_book").append("<tr id='school_"+i+"'><td>"+index+". :"+school_list_EJ[j]+"</td><td>Downloading</td></tr>");
		
		$.post( "http://"+school_list_EJ[j]+"/home/elibrary/admin/report_book_retrieve.php", { source : $("#publisher").val() }, function( data ) {
			$("#school_"+i).html("<td>"+index+". :"+school_list_EJ[j]+"</td><td>Finish</td>");
					
			var current_hit_rate_obj = jQuery.parseJSON( data );	
			
			for(var key in current_hit_rate_obj){
				if(current_hit_rate_obj[key] && hit_rate_obj[key]){
					hit_rate_obj[key].HitRateC = parseInt(hit_rate_obj[key].HitRateC) + parseInt(current_hit_rate_obj[key].HitRateC) ;
					hit_rate_obj[key].HitRateP = parseInt(hit_rate_obj[key].HitRateP) + parseInt(current_hit_rate_obj[key].HitRateP) ;				
				}
			}
			
			i++; j++;
			if(j < arr.length){
				if(report_start)
					loopArray_EJ(arr);
			}else{
				printHitRate();
			}	
		}).fail(function(data){
			$("#school_"+i).html("<td>"+index+". :"+school_list_EJ[j]+"</td><td>Failed</td>");
			
			i++;j++;			
			if(j < arr.length){
				if(report_start)
					loopArray_EJ(arr);
			}else{
				printHitRate();
			}	
		})
	}
	
	var loopArray = function(arr) {	   
		var index = i+1;
	
		$("#report_book").append("<tr id='school_"+i+"'><td>"+index+". :"+school_list[i]+"</td><td>Downloading</td></tr>");
		
		$.post( "http://"+school_list[i]+"/home/eLearning/elibrary/admin/report_book_retrieve.php", { source : $("#publisher").val() }, function( data ) {
			$("#school_"+i).html("<td>"+index+". :"+school_list[i]+"</td><td>Finish</td>");
					
			var current_hit_rate_obj = jQuery.parseJSON( data );	
			
			for(var key in current_hit_rate_obj){
				if(current_hit_rate_obj[key] && hit_rate_obj[key]){
					hit_rate_obj[key].HitRateC = parseInt(hit_rate_obj[key].HitRateC) + parseInt(current_hit_rate_obj[key].HitRateC) ;
					hit_rate_obj[key].HitRateP = parseInt(hit_rate_obj[key].HitRateP) + parseInt(current_hit_rate_obj[key].HitRateP) ;				
				}
			}
			
			i++;
			if(i < arr.length){
				if(report_start)
					loopArray(arr);
			}else{
				loopArray_EJ(school_list_EJ);	
			}	
		}).fail(function(data){
			$("#school_"+i).html("<td>"+index+". :"+school_list[i]+"</td><td>Failed</td>");
			
			i++;
			if(i < arr.length){
				if(report_start)
					loopArray(arr);
			}else{
				loopArray_EJ(school_list_EJ);	
			}	
		})
	};
	
	$(document).ready(function(){
		var list_length = school_list.length + school_list_EJ.length;
		$("#total_school_list").html(" Total client: " + list_length);
		
		$('body').on('click', '#generate_btn', function(){		
			if(!report_start){
				report_start = true;
			}else{
				return;
			}					
			
			$.post( "report_book_retrieve.php", { source : $("#publisher").val(), isTemplate : true }, function( data ) {				
				hit_rate_obj = jQuery.parseJSON( data );												
				loopArray(school_list);	
			});	
			 
			
		});
		
		$('body').on('change', '#publisher', function(){
			 report_start = false;
			 $("#report_book").html("");
			 $("#download_csv").html("");
			 i = 0;
		});
	});

//-->
</script>


<form  method="post" action="" name="form1" enctype="multipart/form-data" onSubmit="">
<table id="html_body_frame" width="50%" border="0" cellspacing="0" cellpadding="5" style="margin-left: auto;margin-right: auto;">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
	</tr>
	<div style="width:90%;margin-left: 5%;"><?=$htmlAry['instructionBox']?></div>
	<tr>
		<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr> 
			<td><br />
			<?=$report_book_html?>	
			<p id="total_school_list"></p>
			<p id="download_csv"></p>
			<table id="report_book" align="center" width="80%" border="0" cellpadding="5" cellspacing="0"></table>
			
			</td>
		</tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">        
		<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
	    <tr>
	    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
	    </tr>
	    <tr>
		
		</tr>		
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>&nbsp;</td></tr>
		</table>                                
		</td>
	</tr>
</table>

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>