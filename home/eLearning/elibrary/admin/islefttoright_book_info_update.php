<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$InputArray["BookID"] = $BookID;
$InputArray["ISRIGHTTOLEFT"] = '';
$LibeLib->UPDATE_BOOK_RIGHTTOLEFT($InputArray);


//$elib_install = new elibrary_install();
//for ($i=0; $i<sizeof($BookID); $i++)
//{
//	$elib_install->enable_site_book_license($BookID[$i], $UserID);
//}

intranet_closedb();
header("Location: index.php?Language=$Language&Author=$Author&Source=$Source&keyword=$keyword&num_per_page=$numPerPage&NoticeID=$NoticeID&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage");


?>