<?php
/*
 *	Log	
 *  Date:	2018-10-12 [Rox]
 *			- Modify the default sorting by Book ID
 *          - Removed other sorting features.
 *	Date:	2015-08-28 [Cameron]
 *			- revised to use standard IP table list and store selected/deselected record in db rather than localStorage
 */
 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$LibeLib = new elibrary();
$libelibinstall = new elibrary_install();

$QuotationID = $_REQUEST['QuotationID'];

if (!$LibeLib->IS_ADMIN_USER($_SESSION["UserID"]))
{
	header("location: /home/");
	die();
}


### Set Cookies
$arrCookies = array();
$arrCookies[] = array("eBook_eBook_license_field", "field");							// navigation var start
$arrCookies[] = array("eBook_eBook_license_order", "order");
$arrCookies[] = array("eBook_eBook_license_number", "pageNo");							// navigation var end
$arrCookies[] = array("eBook_eBook_license_keyword", "keyword");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
{
	updateGetCookies($arrCookies);
}

$QuotationID = $_REQUEST['QuotationID'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$keyword = trim($_REQUEST['keyword']);
$keyword = (isset($keyword) && $keyword != '') ? $keyword : '';
$order = 1; // default sort by ascending order
$field = 0;	// default sort by Book ID
$pageNo = 1;


$CurrentPageArr['LIBMS'] = 1;
$CurrentPage = "PageEBookLicense";


$showCheckBoxCol = false;
$action_row_style = "";
$no_extra_col = 4;

$quo_info = $libelibinstall->get_quotation_info($QuotationID);

if ($quo_info != false) {
	$QuotationNumber = $quo_info["QuotationNumber"];
	$QuotationBookCount = $quo_info["QuotationBookCount"];
	if($quo_info["PeriodFrom"] == "0000-00-00 00:00:00" && $quo_info["PeriodTo"] == "0000-00-00 00:00:00"){
		$disp_quo_info = "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : ".$Lang["ebook_license"]["permanent_use"].")<br/><br/>";
	}
	else {
		$PeriodFrom =  date('Y-m-d',strtotime($quo_info["PeriodFrom"]));
		$PeriodTo =  date('Y-m-d',strtotime($quo_info["PeriodTo"]));
		$disp_quo_info = "(".$Lang["ebook_license"]["quotation_number"]." <b>$QuotationNumber</b> : $PeriodFrom - $PeriodTo)<br/><br/>";
	}
	if ($quo_info["Type"] == 2 && $quo_info["IsActivated"] == "") {
		$showCheckBoxCol = true;
		$no_extra_col = 5;
	}
	else {
		$action_row_style = "none";
	}
}
else {
	$disp_quo_info = $Lang['libms']['bookmanagement']['ebook_license_quotation_error'];
	$QuotationBookCount = 0;
}

$nrSelectedBooks = $libelibinstall->get_compulsory_and_selected_book_count($QuotationID);


#########################
## Build sql for list view
$pageSizeChangeEnabled = false;

$sql = $libelibinstall->get_selected_eBook_sql($QuotationID, $keyword);

$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array = array("i.BookID");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$no_extra_col;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = "IP25_table";
$li->no_navigation_row = true;
$li->page_size = 9999;

$li->column_list .= "<th width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<th width='40%' >".$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']."</td>\n";
$li->column_list .= "<th width='22%' >".$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']."</td>\n";
$li->column_list .= "<th width='22%' >".$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher']."</td>\n";
$li->column_list .= "<th width='14%' >".$Lang["ebook_license"]["language"]."</td>\n";
if ($showCheckBoxCol) {
	$li->column_list .= "<th width='1'>".$li->check("SelectedBookID[]")."</td>\n";
}


include_once("elib_script.php"); 


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

<script language="JavaScript" src="/templates/script.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/jquery.min.js"></script>
<link href="css/content.css" rel="stylesheet" type="text/css">
<link href="css/topbar.css" rel="stylesheet" type="text/css">
<link href="css/leftmenu.css" rel="stylesheet" type="text/css">
<link href="css/footer.css" rel="stylesheet" type="text/css">
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<style type="text/css">
	.shadetabs a input{
		width: auto;
		background: transparent;
		border: 0;
		cursor:pointer;
	}

	.shadetabs{
		border-bottom : 0px;
		margin-bottom: 0px;
	}
	
	.shadetabs li.current_li {
		position: relative;
		color: #2b64b2;
	}
	.shadetabs li.current_li a{ /*selected main tab style */
		background-image: url(/images/2009a/shade.gif);
		color: #2b64b2;
		font: bold ;
		border-bottom-color: white;
		border-top-width: 1px;
		border-right-width: 1px;
		border-bottom-width: 1px;
		border-left-width: 1px;
		border-top-style: solid;
		border-right-style: solid;
		border-bottom-style: solid;
		border-left-style: solid;
		border-top-color: #b1bfdb;
		border-right-color: #b1bfdb;
		border-left-color: #b1bfdb;
	}

</style>
<script>
function JSDeleteTag(TagID)
{
	if (confirm("<?=$eLib['admin']['Confirm_Del_Tag']?>"))
	{
		self.location = "elib_setting_tags_delete.php?TagID=" + TagID;
	}
}

$("document").ready(function(){
<?if ($plugin['eLib_Lite']){?>
	$("#leftmenu").css("display","none");
	$("#content").css("padding-left","0px");
<?}?>

	var book_count_quotation = <?=$QuotationBookCount?>;
	$("#selected_books").val($("#selected_books").val() + ' (<?=$nrSelectedBooks?>/'+book_count_quotation+')');
	
	// select button
	$('#deselect').live( 'click', function(){
		var selected_item = $("input[name='SelectedBookID[]']:checked");
		var num_selected = selected_item.length;
		if (num_selected > 0) {
			if (select_ebook_license() > 0) {
				selected_item.each(function(){
					$(this).parent().parent().remove();				
				});
				update_row_no();		
				update_select_count(book_count_quotation, num_selected);
			}
		}
		else {
			alert("<?=$Lang["libms"]["tableNoCheckedCheckbox"]?>!");
		}		
	});
	
	$('#available_books').live( 'click', function(){
		$('#form1').attr("action","elib_setting_license_ebooks.php");
		$('#form1').submit();
	});

	$('#finish').live( 'click', function(){
		if ($('#NrSelectedBooks').val() != book_count_quotation) {
			alert("<?=$Lang["ebook_license"]["select_exactly_books"]?> "+book_count_quotation+" <?=$Lang["ebook_license"]["select_exactly_books_end"]?> ");
		}
		else {
			if(confirm(" <?=$Lang["libms"]["CirculationManagement"]["confirm"]?> ?")){
				$('#form1').attr("action","elib_setting_license_confirm.php");
				$('#form1').submit();
			}
		}
	});

	$('#keyword').live( 'keypress', function(e){
	    if ( e.which == 13 ) {
			e.preventDefault();
		    return false;
	    }
	});

	$('#keyword').live( 'keyup', function(e){
		var key = e.which || e.charCode || e.keyCode;
	    if ( key == 13 ) {
	    	$('#form1').submit();
	    }
	});
});

function select_ebook_license() {
	var ret = 0;
	var rs = [];
	$('#task').val('deselect');
	$.ajax({								
		url : "ajax_elib_setting_license_ebooks_action.php",
		async: false,
		timeout:1000,							
		type : "POST",							
        data: $("#form1").serialize(),
		success : function(msg) {
			ret = parseInt(msg);
        }
	});
	return ret;								
}

function update_select_count(book_count_quotation, num_selected){
	var newVal = parseInt($('#NrSelectedBooks').val()) - parseInt(num_selected);
	$('#NrSelectedBooks').val(newVal);
	$("#selected_books").val('<?=$Lang["ebook_license"]["selected_books"]?> ('+newVal+'/'+book_count_quotation+')');
}

function update_row_no() {
	var i = 0;
	$("#result_table tr td:first-child").each(function(){
		i++;
		$(this).html(i);
	});
}


</script>
</head>
<body background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_prev_month_on.gif','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_next_month_on.gif')">
	<form name="form1" id="form1" method="post" action="elib_setting_license_selected_ebooks.php">
<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="50" valign="top"> 
				<table width="101%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
	                <td width="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" width="10" height="41"></td>
	                <td width="200" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" class="title"><?=$eLib['html']['elibrary_settings']?></td>
	                <td width="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_02.gif" ></td>
	                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_03.gif">&nbsp;</td>
	                <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_04.gif" width="22" height="41"></td>
              	</tr>
            	</table>
            </td>
        </tr>
        <tr> 
          <td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
                <td width="10" valign="top">&nbsp;</td>
                <td>
                	<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_01_t.gif" width="13" height="33"></td>
						<td height="33" valign="bottom" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_02_t.gif" class="imailpagetitle">
							<!--
							<table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="left">&nbsp;<span class="contenttitle"><?=$eLib['html']['portal_display_settings']?></span></td>
								<td align="right"><label></label></td>
							</tr>
							</table>
							-->
							<div id="Content_tab">
											<ul>
											<li ><a href="elib_setting.php"><span><font size="-1"><?=$eLib['html']['portal_display_settings']?></font></span></a></li>
											<li ><a href="elib_setting_tags.php"><span><font size="-1"><?=$Lang['StudentRegistry']['Tag']?></font></span></a></li>
											<li <?=$license_style?> id="current"><a href="elib_setting_license.php"><span><font size="-1"><?=$Lang['StudentRegistry']['License']?></font></span></a></li>
											<!--<li><a href="eLibrary_admin_book_list.htm"><span>Book List</span></a></li>-->
										</ul>
									</div>
						</td>
						<td width="11" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_03_t.gif" width="11" height="33"></td>
					</tr>
					
					
					<tr>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif" width="13" height="13"></td>
						<td align="center" bgcolor="#FFFFFF">
<!-- list table -->
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td colspan="2">
					<?=$disp_quo_info?>
				</td>
			</tr>
			<tr style="display:<?=$action_row_style?>">
				<td valign="bottom">
				  	<div class="shadetabs" style="float:left;">
						<ul>
							<li class="book_btn_li">
								<a><input type="button" id="available_books" class="" style="border:0px; background: transparent;" value="<?=$Lang["ebook_license"]["available_books"]?>"></input></a>
						  	</li>
						  	<li class="book_btn_li current_li">
								<a><input type="button" id="selected_books" class="" style="border:0px; background: transparent;" value="<?=$Lang["ebook_license"]["selected_books"]?>"></input></a>
						  	</li>
						</ul>
					</div>					
				</td>		
				<td valign="bottom">
					<div style="float:right;">
						<input type="button" id="deselect" class="formbutton_v30" style=" float: right;" value="<?=$Lang["ebook_license"]["deselect"]?>"></input>
						<input type="button" id="finish" class="formbutton_v30" style=" float: right;" value="<?=$Lang["ebook_license"]["finish"]?>"></input>
						<div class="Conntent_search">
							<input autocomplete="off" name="keyword" id="keyword" type="text" value="<?=$keyword?>" />
						</div>
					</div>
				</td>
			</tr>
		</table>
	
		<div id="result_table">
		<?=$li->displayFormat_eBook_license_table();?>
		</div>
		<br />
		<input type="hidden" name="QuotationID" value="<?=$QuotationID?>" />
		<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
		<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
		<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
		<input type="hidden" name="task" id="task" value="" />
		<input type="hidden" id="NrSelectedBooks" value="<?=$nrSelectedBooks?>" />
		
						<br />
	    				</td>
	    				<td width="11" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif" width="11" height="13"></td>
					</tr>
					<tr>
						<td width="13" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_07.gif" width="13" height="10"></td>
						<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" width="13" height="10"></td>
						<td width="11" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_09.gif" width="11" height="10"></td>
					</tr>
					</table>
					<br>
				</td>
			</tr>
	        </table>
		</td>
	</tr>
	</table>
</td>
</tr>
</table>

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/board_scheme_03.gif" width="4" height="4">
		
	</form>
</body>

<?php
intranet_closedb();
?>
