<?php
/*
 * using: henry
 * 
 * 	Log
 * 	Date: 2019-05-13 [Henry] Security fix: SQL without quote
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("popup.html");
$CurrentPage	= "PageMyeClass";
	
$tname = $eLib["html"]["most_useful_reviews"];

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$tname;

$TAGS_OBJ[] = array($title,"");

$tab_title = $lelib->getPageTabTitle();

$lelib = new elibrary();
$MODULE_OBJ["title"] = $tname.$tab_title;
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

if($CurrentPageNum == "")
$CurrentPageNum = 1;

if($DisplayMode == "" || $DisplayMode == null)
$DisplayMode = 3; // review mode

if($DisplayNumPage == "")
$DisplayNumPage = 10;

$ParArr["CurrentPageNum"] = $CurrentPageNum;
$ParArr["DisplayNumPage"] = $DisplayNumPage;
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

if($Ranking == "")
$Ranking = 1;

$ParArr["Ranking"] = $Ranking;

$ParArr["ReviewerID"] = $ReviewerID;

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["ReviewDisplayMode"] = 2;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Category";
$tmpArr[3]["value"] = "Publisher";
$tmpArr[4]["value"] = "Source";

$inputArr["sortFieldArray"] = $tmpArr;

if($sortField == "")
$sortField = $tmpArr[0]["value"];

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$currUserID = $_SESSION["UserID"];
$inputArr["currUserID"] = $currUserID;

// disable the top/bottom selection page menu
$inputArr["pageDisplay"] = "none";


$con = $sortField;
$con = $con." ".$sortFieldOrder;

		$sql = "
		SELECT 
		b.BookID, a.Title as Title, 
		b.DateModified as Date, 
		b.Content, b.Rating, b.UserID, b.ReviewID, a.BookFormat, a.IsTLF
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_REVIEW b
		WHERE
		b.ReviewID = '".$ReviewID."'
		AND
		a.BookID = b.BookID
		ORDER BY
		$con
		";

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];

$settingArr = $lelib->GET_USER_BOOK_SETTING($ParArr);
$NumReview = $settingArr[0]["DisplayReview"];

$reviewArr = $lelib->GET_MOST_USEFUL_REVIEWS($NumReview);
//debug_r($reviewArr);
$totalRanking = count($reviewArr);

$sql = "SELECT UserID FROM INTRANET_ELIB_BOOK_REVIEW WHERE ReviewID = '".$ReviewID."'";
$ReturnIDArray = $lelib->returnArray($sql);

$ReviewerID = $ReturnIDArray[0]["UserID"];

$li = new libuser($ReviewerID);
$photo_link = "/images/myaccount_personalinfo/samplephoto.gif";

if ($li->PhotoLink !="")
{
    if (is_file($intranet_root.$li->PhotoLink))
    {
        $photo_link = $li->PhotoLink;
    }
}

$prevReviewID = $reviewArr[$Ranking - 2]["ReviewID"];
$nextReviewID = $reviewArr[$Ranking]["ReviewID"];

if($prevReviewID == "")
$prevReviewID = $ReviewID;

if($nextReviewID == "")
$nextReviewID = $ReviewID;

$sql = "
		SELECT 
		a.ChineseName, a.EnglishName, a.ClassName, a.ClassNumber,
		Count(b.ReviewID) as NumReview
		FROM
		INTRANET_USER a, INTRANET_ELIB_BOOK_REVIEW b
		WHERE
		a.UserID = '".$ReviewerID."'
		AND a.UserID = b.UserID
		GROUP BY
		b.UserID
		";
		
		//debug_r($sql);
		
		$ReturnInfoArray = $lelib->returnArray($sql);
		
		//debug_r($ReturnInfoArray);
		
		$lang = $_SESSION["intranet_session_language"];
		
		if($lang == "en")
		{
			$sName1 = $ReturnInfoArray[0]["EnglishName"];
			$sName2 = $ReturnInfoArray[0]["ChineseName"];
		}
		else
		{
			$sName1 = $ReturnInfoArray[0]["ChineseName"];
			$sName2 = $ReturnInfoArray[0]["EnglishName"];
		}
		
		if($sName1 != "")
		$StudentName = $sName1;
		else
		$StudentName = $sName2;
		
		$ClassName = $ReturnInfoArray[0]["ClassName"];
		$ClassNumber = $ReturnInfoArray[0]["ClassNumber"];
		$NumReview = $ReturnInfoArray[0]["NumReview"];
		
		
if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}
$totalPage = ceil($totalRecord / $DisplayNumPage);


/* some bg color are very light and mix with white text
if($Ranking <= 5)
	$rankBG = "top_no_".$Ranking."b.gif";
else
	$rankBG = "top_off_b.gif";
*/
$bg_num = $Ranking%3;
if ($bg_num==1)
{
	$rankBG = "top_no_1b.gif";
} elseif ($bg_num==2)
{
	$rankBG = "top_no_2b.gif";
} else
{
	$rankBG = "top_no_5b.gif";
}

$linterface->LAYOUT_START();

?>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindowFull(theURL,winName,features) { //v2.0
  params  = 'width='+screen.width;
  params += ', height='+screen.height;
  params += ', top=0, left=0'
  params += ', fullscreen=yes';
  params += ', ' + features;
  window.open(theURL,winName,params);
}

function sortOrder(type)
{
	var order = "<?=$sortFieldOrder?>";
	
	if(order == "DESC")
	document.form1.sortFieldOrder.value = "ASC";
	else
	document.form1.sortFieldOrder.value = "DESC";
	
	document.form1.sortField.value = type;
	document.form1.submit();
	
} // end function sortorder


function changeDisplayMode(mode)
{
	document.form1.DisplayMode.value = mode;
	document.form1.submit();
	
} // end function change display mode

function sortFunction(obj)
{
	
	if(obj.name == "selPageTop" || obj.name == "selPageBottom")
	{
		document.form1.CurrentPageNum.value = obj.value;
	}
	else if(obj.name == "selDisplayPageTop" || obj.name == "selDisplayPageBottom")
	{
		document.form1.DisplayNumPage.value = obj.value;
	}
	
	document.form1.submit();
} // end function sort

function prevPage()
{
	
	var curPage = parseInt(document.form1.CurrentPageNum.value);
	
	if(curPage - 1 >= 1)
	{
	document.form1.CurrentPageNum.value = curPage - 1;
	document.form1.submit();
	}
	
} //end function prev page

function nextPage()
{
	var totalPage = <?=$totalPage?>;
	var curPage = parseInt(document.form1.CurrentPageNum.value);
	
	if(curPage + 1  <= parseInt(totalPage))
	{
	document.form1.CurrentPageNum.value = curPage + 1;
	document.form1.submit();
	}
	
} //end function next page

function prevRanking()
{
	var curRanking = parseInt(document.form1.Ranking.value);
	
	if(curRanking - 1 >= 1)
	{
	document.form1.Ranking.value = curRanking - 1;
	document.form1.ReviewID.value = <?=$prevReviewID?>;
	document.form1.submit();
	}
	
} // end function prev rank

function nextRanking()
{
	var totalRanking = <?=$totalRanking?>;
	var curRanking = parseInt(document.form1.Ranking.value);

	if(curRanking + 1  <= parseInt(totalRanking))
	{
	document.form1.Ranking.value = curRanking + 1;
	document.form1.ReviewID.value = <?=$nextReviewID?>;
	document.form1.submit();
	}
} // end function next ranking

function voteReview(answer, reviewID, bookID)
{
	if(answer == "no")
	document.form1.Helpful.value = 0;
	else if(answer == "yes")
	document.form1.Helpful.value = 1;
	
	document.form1.ReviewID.value = reviewID;
	document.form1.BookID.value = bookID;
	
	document.form1.action = "elib_add_review_helpful_update.php";
	document.form1.submit();
	document.form1.action = "elib_top_reviews.php";
	
} // end function

//-->
</script>

<form name="form1" action="elib_top_reviews.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>

<!-- photo user Info -->
<td width="210" align="left" valign="top">
<table width="200" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="7" height="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_01b.gif" width="7" height="22"></td>
<td height="22" align="center" nowrap background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_02b.gif" class="eLibrary_title_heading2">&nbsp;</td>
<td width="7" height="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_title_03b.gif" alt="" width="7" height="22"></td>
</tr>
<tr>
<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_01.gif" width="7" height="12"></td>
<td align="left" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_02.gif" height="170" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td width="20" align="left"><a href="#" onClick="prevRanking();" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="next" width="11" height="10" border="0" id="next2"></a></td>
<td width="90%" align="center">
<table width="31" height="30" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/<?=$rankBG?>" class="tabletopnolink"><?=$Ranking?></td>
</tr>
</table>
</td>
<td align="right"><a href="#" onClick="nextRanking();" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="next" width="11" height="10" border="0" id="next"></a></td>
</tr>
</table>
<table width="100%" border="0" cellpadding="3" cellspacing="0">
<tr>
<td align="center" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td align="center">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<td>
<table width="50" border="0" cellspacing="0" cellpadding="3">
<tr>
<td><div id="div7"><span><img src="<?=$photo_link?>" alt="" width="100" height="130" border="0"></span></div></td>
</tr>
</table></td></tr>
</table></td></tr>
<tr>
<td colspan="2" valign="top" nowrap>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td align="center"><span class="tabletext"><?=$StudentName?></span></td>
</tr>
<?php if (trim($ClassName)!="" || trim($ClassNumber)!="") {?>
<tr>
<td align="center"><span class="tabletext"><?=$ClassName?> - <?=$ClassNumber?></span></td>
</tr>
<?php } ?>
<tr>
<td align="center"><span class="tabletext"><?=$eLib["html"]["num_of_review"]?> : <?=$NumReview?></span></td>
</tr>
</table>
<p class="tabletext"><br><br></p>
</td></tr>
</table></td></tr>
</table></td>
<td width="7" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_03.gif" alt="" width="7" height="12"></td>
</tr>
<tr>
<td width="7" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_04.gif" width="7" height="6"></td>
<td height="6" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_05.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_05.gif" width="7" height="6"></td>
<td width="7" height="6"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/top10_board_06.gif" width="7" height="6"></td>
</tr>
</table>
</td>
<!-- end photo user Info -->

<td width="1" bgcolor="#d7d7d7"><img src="images/2009a/10x10.gif" width="1" height="200"></td>

<!-- content -->
<td align="left" valign="top">

<table width="100%" border="0" cellspacing="0" cellpadding="3"><tr><td align="left">
<?=$contentHtml?>
</td></tr></table>

</td>
<!-- end content -->

</tr>
</table>

</td></tr></table>
<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="Ranking" value="<?=$Ranking?>" >
<input type="hidden" name="Helpful" value="0" >
<input type="hidden" name="ReviewID" value="<?=$ReviewID?>" >
<input type="hidden" name="BookID" value="0" >
<input type="hidden" name="returnFile" value="elib_top_reviews.php" >

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>