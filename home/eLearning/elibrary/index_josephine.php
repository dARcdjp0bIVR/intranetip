<?php
//using:	Josephine

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//var_dump($intranet_session_language);
include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
//include_once($PATH_WRT_ROOT."lang/lang.b5.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
//include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_josephine.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.".$intranet_session_language.".php");
//include_once("carouselArea_josephine.php");

intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("elib_script_josephine.php");

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

### Title ###
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];

$lelib = new elibrary();

$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$ParArr["UserID"] = $_SESSION["UserID"];

$settingArr = $lelib->GET_USER_BOOK_SETTING($ParArr);

$NumReviewer = $settingArr[0]["DisplayReviewer"];
$NumReview = $settingArr[0]["DisplayReview"];
$NumRecommend = $settingArr[0]["DisplayRecommendBook"];
$NumWeeklyHit = $settingArr[0]["DisplayWeeklyHitBook"];
$NumHit = $settingArr[0]["DisplayHitBook"];

if($NumReviewer == 0 || $NumReviewer == "")
$NumReviewer = 10;

if($NumReview == 0 || $NumReview == "")
$NumReview = 10;

if($NumRecommend == 0 || $NumRecommend == "")
$NumRecommend = 4;

if($NumWeeklyHit  == 0 || $NumWeeklyHit  == "")
$NumWeeklyHit  = 1;

if($NumHit  == 0 || $NumHit  == "")
$NumHit = 4;

$totalPage = 0;
$DisplayNumPage = 0;

// additional javascript for eLibrary only
include_once("elib_script_function_josephine.php");

//debug_r($lelib->GET_RECOMMEND_BOOK($NumRecommend));

if($plugin['koha'])
{
	$koha_btn = "<a href='admin/login_koha.php' target='_blank'>Go to Koha</a><br />";
}

$linterface->LAYOUT_START();
?>
<script language="JavaScript" type="text/JavaScript">
<!--

//-->
</script>
<link href="css/text" rel="stylesheet" type="text/css">
<link href="css/content_josephine.css" rel="stylesheet" type="text/css">
<link href="css/eLibrary_josephine.css" rel="stylesheet" type="text/css">
<link href="css/topbar_josephine.css" rel="stylesheet" type="text/css">
<link href="css/leftmenu_josephine.css" rel="stylesheet" type="text/css">
<link href="css/footer_josephine.css" rel="stylesheet" type="text/css">
<link href="css/elib_josephine.css" rel="stylesheet" type="text/css">

<!-- <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5"> -->


<!-- <body background="images/2007a/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/2007a/topbar/icon_cube_on.gif','images/2007a/topbar/icon_home_on.gif','images/2007a/topbar/icon_ifolder_on.gif','images/2007a/topbar/icon_library_on.gif','images/2007a/topbar/icon_iportfolio_on.gif','images/2007a/topbar/icon_imail_on.gif','images/2007a/topbar/icon_help_on.gif','images/2007a/topbar/icon_logout_on.gif','images/2007a/topbar/icon_sub_arrow_on.gif','images/2007a/leftmenu/icon_hidebtn_more_on.gif','images/2007a/topbar/icon_iCalendar_on.gif','images/2007a/topbar/icon_iSmartCard_on.gif')"> -->
<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">

 <tr><td valign="top">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
 	<tr><td align="center">
  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	<tr><td>
        		<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr><td align="center" bgcolor="#FFFFFF">
                    	<table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                    		<tr><td valign="top" width="175">

<!-- eLibrary Left Nav. Begin -->
<div id="elib_left_bar">
<!-- eLibrary Records Begin -->
<div id="elib_records_top"><p>Records</p></div>
	<div id="elib_records_border">
	
	<?=$lelib->newDisplayRecordListMenu($ParArr,$eLib);?>

	</div>
	
</div>
<div id="elib_records_bottom"></div>
<!-- eLibrary Records End -->

<div id="elib_cata">
	<?=$eLib_Catalogue?>
</div>

</div>
<!-- eLibrary Left Nav. End -->
</td>

<td valign="top" align="left">
<div class="800width"></div>
<!-- eLibrary Content Begin -->

<!-- School Recommended Book Begin --> 
<div id="photo_wrap">

    <div class="photo_bg">
        <div class="photo_bg_left_bg">
            <div class="photo_bg_left">
				<div class="photo_recommend">    
               
              	<!-- CarouselArea Begin  -->           
					<div id="recommend_wrap">
							<div class="photo" >
    							<a href="#" onclick="openRecommendedBook();return false"><img id="bookCover" height="200" align="center"/></a>
    						</div>
							
                           	 <div class="right">
								
   								<div class="title" id="bookTitle"></div>
    							<div class="reason" id="bookReason"><strong><?=$eLib["html"]["recommended_reason"]?></strong></div>
							
							</div>
					</div>
				<!-- CarouselArea End -->

               </div>
               
                 <div class="photo_nav_outer"><div class="photo_nav_inner">
                	<div class="photo_nav">
				    <ul>
				    <li id="previous"><label title="Previous Book"><a href="#" onclick="onclickPreviousBook();">asdsad</a></label></li>
				    <li id="next"><label title="Next Book"><a href="#" onclick="onclickNextBook();">asdsad</a></label></li>
				    <li id="open"><label title="Open Book"><a href="#" onclick="openRecommendedBook();return false;">asdsad</a></label></li>
				
				    </ul>
    				</div>
                </div></div>
             
            </div>
        </div>
         	<div class="photo_bg_middle"></div>
           
                        <div class="photo_bg_right">
                    		<div class="top"><?=$eLib["html"]["recommended_books_2"]?></div>
 
	                        <!-- 9 books preivew --> 
	                        <div class="books9">
	                         <?=$lelib->newPrintBookTable($lelib->GET_RECOMMEND_BOOK($NumRecommend), $image_path, $LAYOUT_SKIN, $NumRecommend, "recommend", $eLib);?>
	                       
	                        </div>
	                    	<!-- 9 books preivew end--> 
                      
           				</div>
    </div>
</div>
<!-- School Recommended Book End --> 

<script>
function Book(bookTitle, bookDescription, bookPath)
{
	this.title = bookTitle;
	this.description = bookDescription;
	this.path = bookPath;
	
}

var curBook = 0;
var recomBooks = new Array();

<?php
$book = $lelib->GET_RECOMMEND_BOOK($NumRecommend);
for ($i = 0; $i < count($book); ++$i)
{
	echo "recomBooks[$i] = new Book(\"".$book[$i]["Title"]."\", \"".$book[$i]["Description"] ."\", \"".$book[$i]["BookID"]."\");\r\n";
}
?>																																
</script>


<!-- eLibrary Book Ranking Begin -->
<div id="book_rank">


	<!-- Session for Book with Highest Hit Rates -->
    <div class="book_rank_top_right">
    <div class="book_rank_top_left">
    <p><?=$eLib["html"]["bookS_with_highest_hit_rate"]?></p><p><a href="#"><?=$eLib["html"]["book_with_hit_rate_last_week"]?><small>...<a href="#" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=weeklyhit','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></small></a></p>
    </div></div>													
						
	
    <div class="book_rank_body_left"><div class="book_rank_body_right">
    <div class="book_rank_body_books">
   
			<!-- weekly hit -->
			
			<?=$lelib->newPrintBookTable($lelib->GET_WEEKLY_HIT_BOOK($NumWeeklyHit), $image_path, $LAYOUT_SKIN, $NumWeeklyHit, "weeklyHit", $eLib);?>
			<!-- end weekly hit -->
	
    </div>
    </div></div>
    
    
    
    
    <!-- Session for Accumulated Highest -->
    <div class="book_rank_middle_right">
    <div class="book_rank_middle_left">
    <p><a href="#"><?=$eLib["html"]["book_with_hit_rate_accumulated"]?><small> ...<a href="#" onClick="MM_openBrWindowFull('elib_popular_list.php?ListType=hit','listbook','scrollbars=yes')" class="eLibrary_list_all"><?=$eLib["html"]["list_all"]?></a></small></a></p>
    </div></div>

					

    <div class="book_rank_body_left"><div class="book_rank_body_right">
    <div class="book_rank_body_books">
    
    		<!-- hit book -->
				
			<?=$lelib->newPrintBookTable($lelib->GET_HIT_BOOK($NumHit), $image_path, $LAYOUT_SKIN, $NumHit, "hit", $eLib);?>
			<!-- end hit book -->
    
    </div>
    </div></div>
    
    
    <div class="book_rank_footer_right"><div class="book_rank_footer_left"></div></div>
</div>

<!-- eLibrary Book Ranking End -->




<!-- eLibrary Review Ranking Begin -->
<div class="review_rank">
	
	
	<!-- Begin of Most Active Reviewers -->
	<div class="book_review_top_left"><div class="book_review_top_right"><span class="table_head"><?=$eLib["html"]["most_active_reviewers"]?></span></div></div>

    
	<div class="book_review_body_left"><div class="book_review_body_right">
    
		<?=$lelib->newPrintMostActiveReviewers($lelib->GET_MOST_ACTIVE_REVIEWERS($NumReviewer), $image_path, $LAYOUT_SKIN);?>
			
        <div class="book_review_footer_left"><div class="book_review_footer_right"></div></div>
    </div></div>
	<!-- End of Most Active Reviewers -->
	
	
    <div class="book_review_padding"><p></p></div>
   
     
    <!-- Begin of Most Helpful Reviews -->
    <div class="book_review_top_left2"><div class="book_review_top_right2"><span class="table_head"><?=$eLib["html"]["most_useful_reviews"]?></span></div></div>
        
	<div class="book_review_body_left"><div class="book_review_body_right">
    		
    	 <?=$lelib->newPrintMostUsefulReviews($lelib->GET_MOST_USEFUL_REVIEWS($NumReview), $image_path, $LAYOUT_SKIN);?> 
    		
        <div class="book_review_footer_left"><div class="book_review_footer_right"></div></div>
    </div></div>               
    <!-- End of Most Helpful Reviews -->

</div>

</td></tr></table>
                      
                      </td>
                      
                    </tr>
                   
                  </table>
                  <br>
                </td>
              </tr>
            </table></td>
        </tr>
    </table></td>
  </tr>
				<tr> 
				    <td bgcolor="#999999"> 
				      <table width="100%" border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td align="right"><span class="footertext">Powered by</span> <a href="#" class="footerlink">eClass</a></td>
				          <td width="20"><img src="images/2007a/10x10.gif" width="20" height="20"></td>
				        </tr>
				      </table></td>
				
				  </tr>
</table>
<!-- </body> -->



<!-- </table> -->
<script language="JavaScript" type="text/JavaScript">
showChineseBookList();
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
