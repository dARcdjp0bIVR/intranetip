<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$ParArr["currUserID"] 		= $_SESSION["UserID"];
$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$EngBookNum = 0;
$ChiBookNum = 0;

/*
 * count when handling category below
$returnArr = $lelib->getAllBookCatalogue();
$TotalBookNum = count($returnArr);


for($i = 0; $i < $TotalBookNum ; $i++)
{
	if ($returnArr[$i]["Category"]!="" && $returnArr[$i]["SubCategory"]!="")
	{
		if($returnArr[$i]["Language"] == "eng")
		{
			$EngBookNum++;
		} elseif($returnArr[$i]["Language"] == "chi")
		{
			$ChiBookNum++;
		}
	}
}
*/

////////////////////////////////////////////////////////////////////////////
/////////// prest set list array //////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
//  $listArr["lang"]["category"]["subcategory"] 

$returnArr1 = $lelib->getCountCategory($ParArr);
$returnArr2 = $lelib->getCountSubCategory($ParArr);
//debug_r($returnArr1);
for($r = 0;  $r < count($returnArr1) ; $r++)
{
	$Language = $returnArr1[$r]["Language"];
	//$Category = iconv("utf-8","big-5",$returnArr1[$r]["Category"]);
	$Category = $returnArr1[$r]["Category"];
	$count_category = $returnArr1[$r]["count_category"];
	for($x = 0; $x < count($catListArr[$Language]) ; $x++)
	{
		if($catListArr[$Language][$x]["name"] == $Category)
		{
			$catListArr[$Language][$x]["total_count"] = $count_category;
			if($Language == "eng")
			{
				$EngBookNum+=$count_category;
			} elseif($Language == "chi")
			{
				$ChiBookNum+=$count_category;
			}
		
		}
	}
}
$TotalBookNum = $EngBookNum + $ChiBookNum;

for($r = 0;  $r < count($returnArr2) ; $r++)
{
	$Language = $returnArr2[$r]["Language"];
	//$SubCategory = iconv("utf-8","big-5",$returnArr2[$r]["SubCategory"]);
	$Category = $returnArr2[$r]["Category"];
	$SubCategory = $returnArr2[$r]["SubCategory"];
	$count_subcategory = $returnArr2[$r]["count_subcategory"];
	
	//debug($Language, $SubCategory, $count_subcategory);
	for($x = 0; $x < count($catListArr[$Language]) ; $x++)
	{
		for($y = 0; $y < count($catListArr[$Language][$x]["subname"]) ; $y++)
		{
			if($catListArr[$Language][$x]["subname"][$y] == $SubCategory && $catListArr[$Language][$x]["name"]==$Category)
			{
				$catListArr[$Language][$x]["count"][$y] = $count_subcategory;
			}
		}
	}
}



	//////////////////// set eng lsit ////////////////
	$x = "";
	for($j = 0; $j < count($catListArr["eng"]); $j++)
	{
		$name = $catListArr["eng"][$j]["name"];
		$BookCount = $catListArr["eng"][$j]["total_count"];
		if ($BookCount=="")
		{
			$BookCount = 0;
		}
		
		$x .= "<tr><td>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$x .= "<tr>";
		$x .= "<td width=\"25\" align=\"right\"><span class=\"eLibrary_cat_list\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif\" width=\"10\" height=\"20\"></span></td>";
		
		if($BookCount > 0)
		$x .= "<td><span class=\"eLibrary_cat_list style2\"><a href=\"elib_catalogue_detail.php?BookLanguage=eng&Category=".$name."\">".$name." (".$BookCount.")</a></span></td>";
		else
		$x .= "<td><span class=\"tabletextremark\">".$name." (".$BookCount.")</span></td>";
		
		$x .= "</tr>";
	
		$x .= "<tr><td>&nbsp;</td><td>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	
			for($i = 0; $i < count($catListArr["eng"][$j]["subname"]); $i++)
			{
				$subname = $catListArr["eng"][$j]["subname"][$i];
				$subBookCount = $catListArr["eng"][$j]["count"][$i];
				if ($subBookCount=="")
				{
					$subBookCount = 0;
				}
				
				$x .= "<tr>";
				$x .= "<td width=\"25\" align=\"right\" valign=\"top\" class=\"eLibrary_cat_list\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif\" width=\"10\" height=\"20\"></td>";
				
				if($subBookCount > 0)
				$x .= "<td valign=\"top\" class=\"eLibrary_cat_list\"><a href=\"elib_catalogue_detail.php??BookLanguage=eng&Category=".$name."&SubCategory=".$subname."\">".$subname." (".$subBookCount.") </a><br></td>";
				else
				$x .= "<td valign=\"top\" class=\"tabletextremark\">".$subname." (".$subBookCount.") <br></td>";
				
				$x .= "</tr>";
			}
	
		$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
	}
	$engList = $x;
	
	
	//////////////////// set chi lsit ////////////////
	$x = "";
	for($j = 0; $j < count($catListArr["chi"]); $j++)
	{
		$name = $catListArr["chi"][$j]["name"];
		$BookCount = $catListArr["chi"][$j]["total_count"];
		if ($BookCount=="")
		{
			$BookCount = 0;
		}
		
		$x .= "<tr><td>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
		$x .= "<tr>";
		$x .= "<td width=\"25\" align=\"right\"><span class=\"eLibrary_cat_list\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif\" width=\"10\" height=\"20\"></span></td>";
		
		if($BookCount > 0)
		$x .= "<td><span class=\"eLibrary_cat_list style2\"><a href=\"elib_catalogue_detail.php?BookLanguage=chi&Category=".$name."\">".$name." (".$BookCount.")</a></span></td>";
		else
		$x .= "<td><span class=\"tabletextremark\">".$name." (".$BookCount.")</span></td>";
		
		$x .= "</tr>";
	
		$x .= "<tr><td>&nbsp;</td><td>";
		$x .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	
			for($i = 0; $i < count($catListArr["chi"][$j]["subname"]); $i++)
			{
				$subname = $catListArr["chi"][$j]["subname"][$i];
				$subBookCount = $catListArr["chi"][$j]["count"][$i];
				if ($subBookCount=="")
				{
					$subBookCount = 0;
				}
				
				$x .= "<tr>";
				$x .= "<td width=\"25\" align=\"right\" valign=\"top\" class=\"eLibrary_cat_list\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/leftmenu/icon_sub_off.gif\" width=\"10\" height=\"20\"></td>";
				
				if($subBookCount > 0)
				$x .= "<td valign=\"top\" class=\"eLibrary_cat_list\"><a href=\"elib_catalogue_detail.php?BookLanguage=chi&Category=".$name."&SubCategory=".$subname."\">".$subname." (".$subBookCount.") </a><br></td>";
				else
				$x .= "<td valign=\"top\" class=\"tabletextremark\">".$subname." (".$subBookCount.") <br></td>";
				
				$x .= "</tr>";
			}
	
		$x .= "</table>";
		$x .= "</td></tr>";
		$x .= "</table>";
		$x .= "</td></tr>";
	}
	$chiList = $x;

//debug_r($catListArr);

////////////////////////////////////////////////////////////////////////////
/////////// end prest set list array //////////////////////////////////////
///////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--
function checkValidField(obj)
{
	obj.submit();
} // end function check valid field
//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top">
	<!-- ------------------------------------------------------------ -->
	<!-- ---------------      LEFT NAVIGATION BAR ----------------------- --> 
	<?php include_once('elib_left_navigation.php'); ?>
</td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="#" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["catalogue"]?></td>
</tr>
</table>
<!-- end head menu -->

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="center" valign="top">
	<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td height="30" class="eLibrary_top10_link2_stu_name"><?=$eLib["html"]["total"]?> : <?=$TotalBookNum?> <?=$eLib["html"]["books"]?> <a href="elib_catalogue_list_category.php" class="tablebluelink">[ <?=$eLib["html"]["show_all"]?> ]</a></td>
		</tr>
	</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<td align="center" valign="top">
	<table width="90%" border="0" cellspacing="0" cellpadding="2">
	<tr>
	<td height="30" bgcolor="#eeeded"><a href="elib_catalogue_list_category.php?BookLanguage=eng" class="tabletoplink"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10" border="0" align="absmiddle"><span class="eLib_recommend_title"><?=$eLib["html"]["english"]?> (<?=$EngBookNum?>) </span></a> </td>
	</tr>
	
	<!-- english list -->
	<?=$engList;?>
	<!-- end english list -->
	
	<tr>
	<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr></table>
	</td>
	
	<!-- chinese list -->
	<td width="1" bgcolor="#d7d7d7"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="1" height="200"></td>
	<td align="center" valign="top">
	<table width="90%" border="0" cellspacing="0" cellpadding="2">
	<tr>
	<td height="30" bgcolor="#eeeded"><a href="elib_catalogue_list_category.php?BookLanguage=chi" class="tabletoplink"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10" border="0" align="absmiddle"><span class="eLib_recommend_title"><?=$eLib["html"]["chinese"]?> (<?=$ChiBookNum?>) </span></a> </td>
	</tr>
	
	<!-- chinese list -->
	<?=$chiList;?>
	<!-- end chinese list -->

</table>
</td></tr></table>
</td></tr></table>

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>