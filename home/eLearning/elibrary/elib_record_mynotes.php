<?php

//using by: Charles MA

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$lo 			= new libeclass();
$lelib 			= new elibrary();
$libebookreader = new libebookreader();
$CurrentPage	= "PageMyeClass";
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 20;

if($sortField == "")
$sortField = "Date";

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["selectBookOrder"] = $selectBookOrder;
$inputArr["selectCatOrder"] = $selectCatOrder;

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Content";
$tmpArr[2]["value"] = "Category";
$tmpArr[3]["value"] = "Date";

$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["notes_content"];
$tmpArr[2]["text"] = $eLib["html"]["category"];
$tmpArr[3]["text"] = $eLib["html"]["last_modified"];

$tmpArr[0]["width"] = "15%";
$tmpArr[1]["width"] = "55%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "15%";

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 5;
$currUserID = $_SESSION["UserID"];

$con = $sortField;
$order = $con." ".$sortFieldOrder;

$con1 = "";
$con2 = "";

//////////////////////2013-09-18 Charles Ma

if($selectBookOrder != "" && $selectBookOrder != "All"){	
	$con1 = "AND a.BookID = '{$selectBookOrder}'";
	$con3 = "AND BookID = '{$selectBookOrder}'";
}

if($selectCatOrder != "" && $selectCatOrder != "All")
{
	//$selectCatOrder = iconv("big-5","utf-8",$selectCatOrder);
	$con2 = "AND b.Category = '{$selectCatOrder}'";	
	$con4 = "AND Category = '{$selectCatOrder}'";
}

$tmp_table_note = "INTRANET_ELIB_TEMP_NOTE_".$currUserID;
$sql = "CREATE TEMPORARY TABLE ".$tmp_table_note."
		(
			BookID int(11) ,
			Title text,
			Content text,
			Category varchar(100) , 
			Date datetime,
			BookFormat varchar(16),
			IsTLF char(2) 
		)  CHARSET=utf8 ";
		
$lelib->db_db_query($sql);

$sql = "
		SELECT 
		a.BookID, 
		a.Title as Title, 
		CONCAT('<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent(',a.BookID,',\'',b.NoteType,'\',\'',b.NoteID,'\');\">',b.Content, '</a>') as Content,
		b.Category, b.DateModified as Date,
		a.BookFormat,
		a.IsTLF
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_MYNOTES b
		WHERE
		b.UserID = {$currUserID}
		AND a.BookID = b.BookID
		AND a.Publish = 1
		ORDER BY
		$order
		";
		
$new_sql = "INSERT INTO ".$tmp_table_note." (BookID,Title,Content,
			Category,Date,BookFormat,IsTLF) ($sql)";
			
$lelib->db_db_query($new_sql);	
///////////////////////////////////////////////

$new_sql = " SELECT  a.BookID,a.Title AS Title,  b.Highlight AS Content," .
		" '-', DATE_FORMAT(b.DateModified,'%Y-%m-%d %H:%i:%s') AS Date, " .
		" a.BookFormat, a.IsTLF, c.ImageBook " .
		" FROM INTRANET_ELIB_BOOK AS a INNER JOIN INTRANET_EBOOK_HIGHLIGHT_NOTES AS b ON a.BookID = b.BookID " .
		" INNER JOIN INTRANET_EBOOK_BOOK AS c ON a.BookID = c.BookID " .
		" WHERE b.UserID = '$currUserID' ";

$highlight_arr = $lelib->returnArray($new_sql);

$new_sql = "";

foreach($highlight_arr as $key=>$value){
	$temp_arr = explode("notes",$value["Content"]);	
	$temp_arr = explode("page",$temp_arr[1]);
	$temp_arr = explode("\\\",\\\"",$temp_arr[0]);
	$temp_arr = explode("\\\":\\\"",$temp_arr[0]);
	$note = trim($temp_arr[1]);
	
	$temp_arr = explode("page",$value["Content"]);	
	$temp_arr = explode("dateModify",$temp_arr[1]);	
	$temp_arr = explode(",\\\"",$temp_arr[0]);
	$temp_arr = explode("\\\":",$temp_arr[0]);
	$page_number = $temp_arr[1];
	
	if($note != ""){
		$note = urldecode($note);
		$page_number_1 = $libebookreader->Get_PageNumber_Mapping_By_PageMode($value["BookID"],1,$page_number,$value["ImageBook"]);
		
		$note = "<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent_htmlbook(\'".$value["BookID"]."\',\'".$value["Title"]."\',\'\',\'$page_number_1\',\'$page_number\' );\">$note</a>";
		
		if($new_sql == "") $new_sql .= " (' ".$value["BookID"]." ','".$value["Title"]."','".$note."','".$value["Category"]."'," .
				"'".$value["Date"]."','".$value["BookFormat"]."','".$value["IsTLF"]."') ";
		else $new_sql .= ", (' ".$value["BookID"]." ','".$value["Title"]."','".$note."','".$value["Category"]."'," .
				"'".$value["Date"]."','".$value["BookFormat"]."','".$value["IsTLF"]."') ";		
	}
}
$new_sql = "INSERT INTO ".$tmp_table_note." (BookID,Title,Content,
			Category,Date,BookFormat,IsTLF) VALUES $new_sql";
			
$lelib->db_db_query($new_sql);	

///////////////////////////////////////////////
$new_sql = " SELECT 
		a.BookID, 
		a.Title as Title, 
        CONCAT('<a href=\"#\" class=\"tablelink\" onClick=\"ViewNotesContent_htmlbook(',a.BookID,',\'',a.Title,'\',\'',b.RecordID,'\',\'',b.PageNum1PageMode,'\',\'',b.PageNum2PageMode,'\'    );\">',b.Notes, '</a>') as Content,
		'-', 
		b.DateModified as Date,
		a.BookFormat,
		a.IsTLF
		FROM
		INTRANET_ELIB_BOOK AS a INNER JOIN INTRANET_EBOOK_PAGE_NOTES AS b ON a.BookID = b.BookID
		WHERE b.UserID = '$currUserID'  "; 


$new_sql = "INSERT INTO ".$tmp_table_note." (BookID,Title,Content,
			Category,Date,BookFormat,IsTLF) ($new_sql)";
			
$lelib->db_db_query($new_sql);	

//debug_r($sql);

$new_sql = " SELECT * FROM ".$tmp_table_note." WHERE 1 $con3 $con4 ORDER BY $order ";

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $new_sql);
//////////////////////2013-09-18 Charles Ma END

$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

//////////////////////////////////////////////////////////
// set selection box
//////////////////////////////////////////////////////////
$ParArr["currUserID"] = $_SESSION["UserID"];

//////////////////////2013-09-18 Charles Ma

$new_sql = " SELECT DISTINCT (Title), BookID FROM ".$tmp_table_note."";

//$returnTitle = $lelib->getBookNotesTitle_ByUserID($ParArr);

$returnTitle = $lelib->returnArray($new_sql, 2);

//////////////////////2013-09-18 Charles Ma END
$returnCategory = $lelib->getBookNotesCategory_ByUserID($ParArr);

$selectBook = "<select name=\"selectBook\" onChange=\"changeBookSelection(this);\">";

$selectBook .= "<option value=\"All\">".$eLib["html"]["all_books"]."</option>";

for($i = 0; $i < count($returnTitle); $i++)
{
	if($selectBookOrder == $returnTitle[$i]["BookID"])
	$checkVal = "selected";
	else
	$checkVal = "";
	
	//$selectBook .= "<option value=\"".$returnTitle[$i]["BookID"]."\"".$checkVal.">".iconv("utf-8","big-5",$returnTitle[$i]["Title"])."</option>";
	$selectBook .= "<option value=\"".$returnTitle[$i]["BookID"]."\"".$checkVal.">".$returnTitle[$i]["Title"]."</option>";
}

$selectBook .= "</select>";

$selectCategory = "<select name=\"selectCategory\" onChange=\"changeCategorySelection(this);\">";
$selectCategory .= "<option value=\"All\">".$eLib["html"]["all_categories"]."</option>";

for($i = 0; $i < count($returnCategory); $i++)
{
	if($selectCatOrder == $returnCategory[$i]["Category"])
	$checkVal = "selected";
	else
	$checkVal = "";
	
	//$selectCategory .= "<option value=\"".iconv("utf-8","big-5",$returnCategory[$i]["Category"])."\"".$checkVal.">".iconv("utf-8","big-5",$returnCategory[$i]["Category"])."</option>";
	$selectCategory .= "<option value=\"".$returnCategory[$i]["Category"]."\"".$checkVal.">".$returnCategory[$i]["Category"]."</option>";
}

$selectCategory .= "</select>";

$selectionTable = "<table border=\"0\" cellpadding=\"4\" cellspacing=\"0\">";
$selectionTable .= "<tr>";
$selectionTable .= "<td>".$selectBook."</td>";
$selectionTable .= "<td>".$selectCategory."</td>";
$selectionTable .= "</tr>";
$selectionTable .= "</table>";
//////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeBookSelection(obj)
{
	document.form1.selectBookOrder.value = obj.value;
	document.form1.submit();
	
} // end function

function changeCategorySelection(obj)
{
	document.form1.selectCatOrder.value = obj.value;
	document.form1.submit();
	
} // end function

function ViewNotesContent(id, notes, noteId)
{
	//alert(id + " " + notes);
	MM_openBrWindowFull('tool/index.php?BookID='+id+'&NoteType='+notes+'&NoteID='+noteId,'booktool','scrollbars=yes');
}

// 2013-09-18 Charles Ma

function ViewNotesContent_htmlbook(id, title, noteId,PageNum1PageMode, PageNum2PageMode)
{
 	var url = '../ebook_reader/'+id+'/reader/';
     var newWindow = window.open(url, "title","status=1");

      if (!newWindow) return false;
	
	var formid = "newForm", ID = 'id';
	
	
     var html = "";
     html += "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";
     html += "<input type='hidden' name='mynote_p1' value='" + PageNum1PageMode+ "'/>";
     html += "<input type='hidden' name='mynote_p2' value='" + PageNum2PageMode+ "'/>";     
     html += "<input type='hidden' name='action' value='mynote'/>";
     html += "</form><script type='text/javascript'>document.getElementById(\"formid\").submit()</script></body></html>";

     newWindow.document.write(html);
     return newWindow;

	
}

// 2013-09-18 END

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top">
	<!-- Template , Left Navigation panel --> 
	<?php include_once('elib_left_navigation.php'); ?>
</td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_record_mynotes.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<!--
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<?=$eLib["html"]["personal_records"]?>
-->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["my_notes"]?></td>
</tr>
</table>
<!-- end head menu -->


<table width="98%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
</tr>

<tr>
<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>

<td style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif); background-repeat:repeat-x;">
<?=$selectionTable?>
<!-- show content -->
<?=$contentHtml?>
<!-- end show content -->
</td>


<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
</tr>
													
<tr>
<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
</tr>
</table>

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >
<input type="hidden" name="selectBookOrder" value="<?=$selectBookOrder?>" >
<input type="hidden" name="selectCatOrder" value="<?=$selectCatOrder?>" >
</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
