<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php"); // 2013-08-19 Charles Ma

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();

if (!$LibeLib->IS_ADMIN_USER($_SESSION["UserID"]))
{
	header("location: /home/");
	die();
}

$Libelibinstall = new elibrary_install();	// 2013-08-19 Charles Ma
//2013-08-19 Charles Ma
if(!$Libelibinstall->retrive_license_enable())
	$license_style = 'style="display:none;"';

include_once("elib_script.php");

$NavTitle = ($TagID!="" && $TagID>0) ? $eLib["html"]["edit_tag"] : $eLib["html"]["new_tag"];
$PAGE_NAVIGATION[] = array($NavTitle, "");

$linterface = new interface_html();
$toolbar = $linterface->GET_LNK_ADD("elib_setting_tags_new.php",$button_new,"","","",0);

if ($TagID!="" && $TagID>0)
{
	$TagName = $LibeLib->returnTagNameByID($TagID);
	$button_save_or_submit = $button_save;
} else
{
	$button_save_or_submit = $button_submit;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

<script language="JavaScript" src="/templates/script.js"></script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
#Layer1 {	
	position:absolute;
	width:150px;
	height:14px;
	z-index:2;
}
-->
</style>
<script>
function goSubmit()
{
	if (document.form1.TagName.value=="")
	{
		alert("<?=$eLib["SysMgr"]["tag_name"]?>");
		document.form1.TagName.focus();
		return false;
	}
	
	var bookLength = document.form1.elements['BookIDSelected[]'].length;
	for(i=0; i<bookLength; i++) {
		document.form1.elements['BookIDSelected[]'][i].selected = true;	
	}
		
	document.form1.submit();
}
</script>
</head>

<body background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/bg.gif" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_prev_month_on.gif','<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/calendar/btn_next_month_on.gif')">

<form name="form1" action="elib_setting_tags_new_update.php" method="post">
<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
  <tr> 
    <td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="50" valign="top"> 
				<table width="101%" border="0" cellspacing="0" cellpadding="0">
              	<tr> 
	                <td width="10" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" width="10" height="41"></td>
	                <td width="200" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_01.gif" class="title"><?=$eLib['html']['elibrary_settings']?></td>
	                <td width="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_02.gif" ></td>
	                <td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_03.gif">&nbsp;</td>
	                <td width="22"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/titlebar_04.gif" width="22" height="41"></td>
              	</tr>
            	</table>
            </td>
        </tr>
        <tr> 
          <td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr> 
                <td width="10" valign="top">&nbsp;</td>
                <td>
                	<table width="99%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="13" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_01_t.gif" width="13" height="33"></td>
						<td height="33" valign="bottom" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_02_t.gif" class="imailpagetitle">
							<!--
							<table width="100%" height="28" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td align="left">&nbsp;<span class="contenttitle"><?=$eLib['html']['portal_display_settings']?></span></td>
								<td align="right"><label></label></td>
							</tr>
							</table>
							-->
							<div id="Content_tab">
											<ul>
											<li ><a href="elib_setting.php"><span><font size="-1"><?=$eLib['html']['portal_display_settings']?></font></span></a></li>
											<li id="current"><a href="elib_setting_tags.php"><span><font size="-1"><?=$Lang['StudentRegistry']['Tag']?></font></span></a></li>
											<li <?=$license_style?>><a href="elib_setting_license.php"><span><font size="-1"><?=$Lang['StudentRegistry']['License']?></font></span></a></li>
											<!--<li><a href="eLibrary_admin_book_list.htm"><span>Book List</span></a></li>-->
										</ul>
									</div>
						</td>
						<td width="11" height="33"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_03_t.gif" width="11" height="33"></td>
					</tr>
					
					
					<tr>
						<td width="13" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_04.gif" width="13" height="13"></td>
						<td align="center" bgcolor="#FFFFFF">

	<table width="100%" border="0" cellspacing="0" cellpadding="8" align="center">	
		<tr>
			<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?>
			</td>
		</tr>
	</table>
	
	<table width="92%" border="0" cellspacing="0" cellpadding="5" align="center">	
		<tr><td>
	<div class="table_board">
							<table class="form_table">
							  <tr>
							    <td class="formfieldtitle" nowrap="nowrap"><?=$Lang['StudentRegistry']['Tag']?> <span class="tabletextrequire">*</span></td>
							    <td >
								    <input name="TagName" id="TagName" type="text"  value="<?=$TagName?>"  />
							    </td>
							  </tr>
							  <tr>
							    <td class="formfieldtitle" nowrap="nowrap"><?=$eLib["html"]["book_of_tag"]?></td>
							    <td >
								   <table width="100%" border="0">
								   <tr><td><?= $LibeLib->DisplayTagBookSelections($TagID, true, "BookIDSelected[]") ?></td>
								   		<td>
								   		<?=
								   		$linterface->GET_BTN("<< ".$i_eNews_AddTo, "button", "checkOptionTransfer(this.form.elements['BookSource[]'],this.form.elements['BookIDSelected[]']);return false;", "submit2") . "<br /><br />" .
                        				$linterface->GET_BTN($i_eNews_DeleteTo . " >>", "button", "checkOptionTransfer(this.form.elements['BookIDSelected[]'],this.form.elements['BookSource[]']);return false;", "submit23")
                        				?>							   		
								   		
								   		</td>
								   		<td><?= $LibeLib->DisplayTagBookSelections($TagID, false, "BookSource[]") ?></td>
								   	</tr>
								   	</table>
							    </td>
							  </tr>
							</table>
							<span class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></span>
							<div class="edit_bottom">
	                          <p class="spacer"></p>
	                          <?= $linterface->GET_ACTION_BTN($button_save_or_submit, "button", "goSubmit()")?>
	                          <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='elib_setting_tags.php'")?>
								<p class="spacer"></p>
	                    </div>
	</div>
	</td></tr></table>
	
										
						<br />
	    				</td>
	    				<td width="11" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_06.gif" width="11" height="13"></td>
					</tr>
					<tr>
						<td width="13" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_07.gif" width="13" height="10"></td>
						<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_08.gif" width="13" height="10"></td>
						<td width="11" height="10"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/content_09.gif" width="11" height="10"></td>
					</tr>
					</table>
					<br>
				</td>
			</tr>
	        </table>
		</td>
	</tr>
	</table>
</td>
</tr>
</table>

<input type="hidden" name="TagID" id="TagID" value="<?=$TagID?>" />
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/board_scheme_03.gif" width="4" height="4">
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.TagName") ?>
</body>
</html>
<?php
intranet_closedb();
?>