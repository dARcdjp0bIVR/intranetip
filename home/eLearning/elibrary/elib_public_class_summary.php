<?php
//using by: 
/* 2012-09-12 CharlesMa
 * HistoryID change to DISTINCT b.BookID - 20140808-J64184 
 */

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once("elib_script.php");



intranet_auth();

intranet_opendb();

########################################################

$curr_month = date("m");
$curr_year = date("Y");

$field = (isset($_REQUEST['field']))? $_REQUEST['field'] : "";
$order = (isset($_REQUEST['order']))? $_REQUEST['order'] : 0;
$pageNo = (isset($_REQUEST['pageNo']))? $_REQUEST['pageNo'] : 1;
$YearClassID = (isset($_REQUEST['YearClassID']))? $_REQUEST['YearClassID']: "";
$FromMonth = (isset($_REQUEST['FromMonth']))? $_REQUEST['FromMonth'] : $curr_month;
$ToMonth = (isset($_REQUEST['ToMonth']))? $_REQUEST['ToMonth']: $curr_month;
$ToYear = (isset($_REQUEST['ToYear']))? $_REQUEST['ToYear']: $curr_year;
$FromYear = (isset($_REQUEST['FromYear']))? $_REQUEST['FromYear']: $curr_year;
$search = (isset($_REQUEST['search']))? $_REQUEST['search']: "";



 
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true; 
 
$linterface 	= new interface_html("default3.html");
$li 			= new libdbtable($field, $order, $pageNo);
$lo 			= new libeclass();
$lelib 			= new elibrary();
$ly				= new year_class($YearClassID, false,false,false);

$ClassName = $ly->ClassTitleEN;

if($lelib->IS_TEACHER() || $lelib->IS_ADMIN())
{
	
}
else
	header("location:index.php");
########################################################



$CurrentPage	= "PageMyeClass";

### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$ParArr["currUserID"] = $_SESSION["UserID"];

$title = $lelib->printSearchInput($ParArr, $eLib);

$tab_title = $lelib->getPageTabTitle();
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 4; // list mode (no image cover)


$lang = $_SESSION["intranet_session_language"];


#############################################################################
#############################################################################


## SQL Query
$con = "";

$con_field = 'count(r.BookID)';
//if(!empty($FromMonth) && $search==1){
if (!empty($FromMonth)){
    $FromDate = date('Y-m-d',mktime(0,0,0,$FromMonth,1,$FromYear));
    $ToDate = date('Y-m-d',mktime(0,0,0,$ToMonth+1,1,$ToYear));
    $con = "AND r.DateModified BETWEEN '$FromDate' AND '$ToDate' ";
}else { $con = ''; }
//TEMPORARY


$tmp_table = "INTRANET_ELIB_TEMP_NUM_".$UserID;
$sql = "CREATE TEMPORARY TABLE ".$tmp_table."
		(
			class_name varchar(255),
			count_history int(10),
			click_history int(10),
			count_review int(10),
			
			num_student int(4)
			
		)";
$lelib->db_db_query($sql);

$sql = "INSERT INTO ".$tmp_table." (class_name,num_student) (select distinct ClassName,count(*) FROM INTRANET_USER where ClassName!='' and ClassName IS NOT NULL group by ClassName)";
$lelib->db_db_query($sql);

$sql = "select u.ClassName, count(ReviewID) as count_rec from 
		INTRANET_USER as u 
		inner join INTRANET_ELIB_BOOK_REVIEW as r on r.UserID = u.UserID
		inner join INTRANET_ELIB_BOOK as b on b.BookID = r.BookID
		where b.publish=1 and u.ClassName!='' and u.ClassName IS NOT NULL
		$con
		group by u.ClassName";
$review_obj = $lelib->returnArray($sql);

for($a=0;$a<sizeof($review_obj);$a++){
	$sql = "UPDATE ".$tmp_table." SET count_review = ".$review_obj[$a]['count_rec']." WHERE class_name = '".$review_obj[$a]['ClassName']."'";
	$lelib->db_db_query($sql);
	
}

//20140808-J64184 
//$sql = "select u.ClassName, count(DISTINCT b.BookID) as count_rec from 
//		INTRANET_USER as u 
//		inner join INTRANET_ELIB_BOOK_HISTORY as r on r.UserID = u.UserID
//		inner join INTRANET_ELIB_BOOK as b on b.BookID = r.BookID
//		where b.publish=1 and u.ClassName!='' and u.ClassName IS NOT NULL
//		$con
//		group by u.ClassName";
		
$sql = "Select a.ClassName, SUM(a.count_rec) as count_rec, SUM(a.click_rec) as click_rec FROM (
		select u.ClassName, count(DISTINCT b.BookID) as count_rec, count(DISTINCT r.HistoryID) as click_rec from 
		INTRANET_USER as u 
		inner join INTRANET_ELIB_BOOK_HISTORY as r on r.UserID = u.UserID
		inner join INTRANET_ELIB_BOOK as b on b.BookID = r.BookID
		where b.publish=1 and u.ClassName!='' and u.ClassName IS NOT NULL
		$con
		group by u.ClassName, u.UserID) as a GROUP BY a.ClassName";		

$history_obj = $lelib->returnArray($sql);

echo mysql_error();
for($a=0;$a<sizeof($history_obj);$a++){
	$sql = "UPDATE ".$tmp_table." SET count_history = '".$history_obj[$a]['count_rec']."', click_history = '".$history_obj[$a]['click_rec']."'  WHERE class_name = '".$history_obj[$a]['ClassName']."'";
	$lelib->db_db_query($sql);

}


$sql = "SELECT
			concat('<a href=\"elib_public_students_summary.php?ClassName=',class_name,'\">',class_name,'</a>'),
			num_student,
			if(sum(click_history)>0,sum(click_history),0) as NumBookClick,
			if(sum(count_history)>0,sum(count_history),0) as NumBookRead,
			if(round(count_history/num_student)>0,round(count_history/num_student),0) as read_average,
			if(sum(count_review)>0,sum(count_review),0) as NumBookReview,
			if(round(count_review/num_student)>0,round(count_review/num_student),0) as review_average			
		FROM
			".$tmp_table."
		group by class_name";
	
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("class_name", "num_student","NumBookClick", "NumBookRead", "read_average","NumBookReview", "review_average");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = "IP25_table";

$sortField = $li->field_array[$field];
$li->column_list .= "<th class='tabletoplink ' style='vertical-align:middle'>#</th>\n";
$li->column_list .= "<th class='tabletoplink ' style='vertical-align:middle'>".$li->column(0,$eLib["html"]["class"])."</th>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='vertical-align:middle'>".$li->column(1,$eLib["html"]["num_of_students"])."</th>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='vertical-align:middle'>".$li->column(2,$eLib["html"]['Total']."(".$eLib_plus["html"]["hitrate"] .")")."</th>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='vertical-align:middle'>".$li->column(2,$eLib["html"]['Total']."(".$eLib["html"]["num_of_book_read"].")")."</th>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='vertical-align:middle'>".$li->column(3,$eLib["html"]['Average']."(".$eLib["html"]["num_of_book_read"].")")."</th>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='vertical-align:middle'>".$li->column(4,$eLib["html"]['Total']."(".$eLib["html"]["num_of_review"].")")."</th>\n";
$li->column_list .= "<th class='tabletop tabletopnolink' style='vertical-align:middle'>".$li->column(5,$eLib["html"]['Average']."(".$eLib["html"]["num_of_review"].")")."</th>\n";


$currUserID = $_SESSION["UserID"];






// drop the temp table create before
//$lelib->db_db_query($dsql1);
//$lelib->db_db_query($dsql2);


#####################################################
#####################################################





///////////////////// Selection Box ////////////////////////////////////////////////

$returnClassNameArr = $lelib->GET_CLASSNAME();

	$ClassNameArr = array(
				array("-1", $eLib["html"]["all_class"])
				);
			
	//debug_r($returnClassNameArr);
	for($i = 0; $i < count($returnClassNameArr); $i++)
	{
		
		if(trim($returnClassNameArr[$i]["ClassTitleEN"]) != "")
		$tmpClassArr = "";
		$tmpClassArr = array($returnClassNameArr[$i]["ClassTitleEN"], $returnClassNameArr[$i]["ClassTitleEN"]);
		
		$ClassNameArr[$i+1] = $tmpClassArr;
	}
	//debug_r($ClassNameArr);
	

	if($ClassName == "")
	$ClassName = $ClassNameArr[0][0];
	
	//$le->getSelectClass( "name='ClassName' id='ClassName' onChange='changeClassName(this);'", $ClassName, );
	$ClassSelection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($ClassNameArr, "name='ClassName' id='ClassName' onChange='changeClassName(this);'", "", $ClassName);
	
	$ThisYear = Date("Y")*1;
	$aryMonth = array(array('','--'),array('01',1),array('02',2),array('03',3),array('04',4),array('05',5),array('06',6),array('07',7),array('08',8),array('09',9),array(10,10),array(11,11),array(12,12));
	$aryYear = array(array('','--'),array($ThisYear,$ThisYear), array($ThisYear-1,$ThisYear-1),array($ThisYear-2,$ThisYear-2),array($ThisYear-3,$ThisYear-3),array($ThisYear-4,$ThisYear-4),array($ThisYear-5,$ThisYear-5)); 
		
	$FromMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='FromMonth' id='FromMonth'", "", $FromMonth);
	$FromYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='FromYear' id='FromYear'", "", $FromYear);
	$ToMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='ToMonth' id='ToMonth'", "", $ToMonth);
	$ToYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='ToYear' id='ToYear'", "", $ToYear);
	
	$classStr = $eLib["html"]["class"]." : ";
	
	$SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	//$SelectionTable .= "<tr><td><span class=\"tabletext\">".$classStr." ".$ClassSelection."</span></td></tr>";
	$SelectionTable .= '<tr><td><span class="tabletext">'.$Lang['Header']['Menu']['Period'].':'.
						$FromMonthSelection.$FromYearSelection.$Lang['SysMgr']['Homework']['To'].$ToMonthSelection.$ToYearSelection.
						'<input type="button" name="search" onClick="applySearch();" value="'.$Lang['Btn']['Apply'].'" /></span></td></tr>';
	
	$SelectionTable .= "</table>";
//////////////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();


?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeClassName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

function applySearch(){
	
	
	
	if($('#FromMonth').val()=='' || $('#FromYear').val()=='' || $('#ToMonth').val()=='' || $('#ToYear').val()=='')
	{
		if($('#FromMonth').val()!='' || $('#FromYear').val()!='' || $('#ToMonth').val()!='' || $('#ToYear').val()!=''){
			alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			return false;
		}
	}
	
	var strFromDate = document.form1.FromYear.value+"-"+document.form1.FromMonth.value+"-0";
	var strToDate = document.form1.ToYear.value+"-"+document.form1.ToMonth.value+"-0";
	
	if(compareDate(strToDate,strFromDate)>=0){
		$("#search").val(1);
		document.form1.submit();
	}
	else{
		alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
	}
	
}


//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">

<form name="form1" action="" method="post">
	

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top" width="172" style="margin:0px padding:0px;" >
		<!-- Template , Left Navigation panel -->
		<?php include_once('elib_left_navigation.php') ?>		
	</td>
	<td align="center" valign="top">
		<!-- Start of Content -->
		
		<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>


			<!-- head menu -->
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
			
			<!-- NAVIGATION CRUMBTRAIL --> 
			<td class="eLibrary_navigation">
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
				<a href="index.php"><?=$eLib["html"]["home"]?></a> 
			
				<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
				<?=$eLib["html"]["class_summary"]?>
			</td>
			</tr>
			</table>
			
			<!-- end head menu -->


			<table width="98%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
				<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
				<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
			</tr>
			
			<tr>
				<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>
				
				<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
				<!-- show content -->
				<?=$SelectionTable; ?>
				<!--<?=$contentHtml; ?>-->
					
				<?=$li->display()?>
				
				<!-- end show content -->
				</td>
				
				
				<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
			</tr>
																
			<tr>
				<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
				<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
				<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
			</tr>
			</table>
		<!-- End of Content -->							
	</td></tr>
</table>	





<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
  <input type="hidden" name="order" value="<?php echo $li->order; ?>">
  <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
  <input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
  <input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

<input type="hidden" name="search" id="search" value="<?=$search?>" >
</form>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
