<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();
// additional javascript for eLibrary only
include_once("elib_script.php");
#################################################################
## Init Library
$linterface 	= new interface_html('popup5.html');
$objTB 		= new libdbtable();
$li 		= new libdbtable2007($field, $order, $pageNo);
$objInstall	= new elibrary_install();

## Get Post Variables
$BookID = isset($_REQUEST['BookID'])? trim($_REQUEST['BookID']) : "";
$msg 	= isset($_REQUEST['msg'])? trim($_REQUEST['msg']) : "";

if($BookID==""){
	echo "Error, no book selected";
	die();
}

if($msg == "1"){
	$msg = "<font color='green'><b>".$eLib["html"]["Success"]."</b></font>";
}else if($msg == "0"){
	$msg = "<font color='red'><b>".$eLib["html"]["Fail"]."</b></font>";
}

$page_size 				= (isset($numPerPage) && $numPerPage != "")? $numPerPage : 20;
$pageSizeChangeEnabled 	= true;
$field 					= ($field=="")? 1 : $field;
$order 					= ($order == '' || $order != 0) ? 1 : 0;

$li = new libdbtable2007($field, $order, $pageNo);

$getSqlOnly 			= true;
$li->sql	 			= $objInstall->get_student_with_name_license_list($BookID, $getSqlOnly);

$li->field_array 		= array("InputDate", "ClassNumber", "ClassName", "Name");

$li->IsColOff 			= "2";
$li->no_col 			= 6;
$li->column_array 		= array(12,12,12,12);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['html']["last_added"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["html"]["class_number"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["html"]["class_name"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["html"]["student_name"])."</td>\n";

$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BookStudentID[]")."</td>\n";

$linterface->LAYOUT_START();
################################################################# 
?>
<script>

function remove_assigned(){
	if(confirm("<?=$eLib['admin']['Confirm_Remove_Assign_Student']?>")){
		//Get remove studentID list
		var strBookStudentID = "";
		 
		$("input[name=BookStudentID[]][checked]").each(function(){			
			strBookStudentID += (strBookStudentID != "")? ", ": "";
			strBookStudentID += $(this).val();
		});
		
		$("#msg").html("");
		$.post("admin_book_license_assign_remove_update.php", 
				{strBookStudentID:strBookStudentID},
				function(data){
					location.href = "admin_book_license_view_assign.php?BookID="+<?=$BookID?>+"&msg="+data;
				});
	}
}
</script>
<form name="form1" id="form" method="POST" style="margin:0px;padding:0px;">
<div style="width:827px;height:490px;background:#FFF;">
	
			
		<div style="width:100%;height:470px; overflow-y:auto;">
		<table border="0" cellspacing="0" cellpadding="0" width="90%" align="center">
		<tr>
		<td></td>
		<td height="20px" id="msg" class="systemmsg" align="center">
			<?=$msg?>		
		</td>
		</tr>
		<tr height="10px;">		
			<td align="left" valign="bottom">
				<?=$eLib['admin']['Delete_Assign_Student_Instruction']?>
			</td>
			<td align="right" valign="bottom">
				<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
					<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td nowrap>
								<a href="javascript:void(0);" class="tabletool" onClick="remove_assigned()">
									<?=$eLib["html"]["Cancel_License"]?>
								</a>
							</td>
						</tr>
						</table>
					</td>
					<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" colspan="2">
				<?=$li->display();?>
			</td>
		</tr>
		</table>
		</div>		
		<div style="height:40px;background-color:#EEE;text-align:center;">
			<table border="0" width="100%" height="100%"><tr><td align="center" valign="center">	
			<input type="button" name="close_btn" id="close_btn" value="<?=$eLib["html"]["Close"]?>" onClick="window.parent.location.reload();window.parent.tb_remove();" />
			</td></tr></table>	
		</div>
	
</div>

<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
	
</form>
	



<?php
#################################################################
$linterface->LAYOUT_STOP();
intranet_closedb();
?>