<?php
//Using: 
/**
 * Change Log:
 * 2018-06-27 Pun [141453] [ip.2.5.9.7.1]
 *  - Fixed cannot use https
 * 2010-04-15 Adam: add javascript to get Flash movie in IE and FF 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//var_dump($intranet_session_language);
include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/lang.b5.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageImportBook";


$LibeLib 		= new elibrary();
$objInstall 	= new elibrary_install();

$protocol = (checkHttpsWebProtocol())? 'https' : 'http';
/*if (!$LibeLib->HAS_RIGHT())
{
	
     header ("Location: /");
     intranet_closedb();
     exit();
}*/

## 2010-04-19 : License, check book readability 
$hasAccess = $objInstall->check_book_is_readable($BookID, $_SESSION['UserID']);

$TAGS_OBJ[] 	= array($eLib['ManageBook']["ImportBook"],"");
$MODULE_OBJ 	= $LibeLib->GET_MODULE_OBJ_ARR();
    
### Title ###
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];



$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
foreach($eLib['Source'] as $Key => $Value)	
{
	$SourceArr[] = array($Key,$Value);
}
}
$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='SourceFrom' ","", $SourceFrom);

$LangArr = array();
$LangArr[] = array("chi",$i_QB_LangSelectChinese);
$LangArr[] = array("eng",$i_QB_LangSelectEnglish);
$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' ","", $Language);

if ($BookID =="")
{
	$BookID = 383;
	//$BookID = 19;
}

if ($intranet_httppath == "")
{
	$urlHost = (getenv("HTTP_HOST")!="") ? getenv("HTTP_HOST") : $_SERVER["HTTP_HOST"];
	$intranet_httppath = $urlHost;
}

$ParArr["BookID"] = $BookID;
$ParArr["UserID"] = $_SESSION["UserID"];

$returnArr = $LibeLib->getBookInformation($ParArr);
if($returnArr != ""){
	$Publish = $returnArr[0]["Publish"];
	$Category = trim($returnArr[0]["Category"]);
	$SubCategory = trim($returnArr[0]["SubCategory"]);
	$Level = trim($returnArr[0]["Level"]);	
}


$langArr = $LibeLib->getBookLanguage($ParArr);

if($langArr[0]["Language"] == "chi")
$langType = "ChineseT";
else
$langType = "English";

$LibeLib->ADD_READING_HISTORY_RECORD($ParArr);
$LibeLib->ADD_BOOK_HIT_RATE($ParArr);

$Param = "ecCourseID=".$intranet_db."&userkey=540SeP1SeP1123808248SeP0SeP0SeP3981SeP0SeP0SeP0";
//$Param .= "&ecGatewayPath=http://".$eclass_httppath."src/includes/flashservices/gateway.php";
//$Param .= "&ecGatewayPath=http://".$intranet_httppath."/home/elibrary/flashservices/gateway.php";
$Param .= "&ecGatewayPath={$protocol}://".$intranet_httppath."/includes/flashservices/gateway.php";
$Param .= "&ecItemID=".$intranet_db."SeP".$UserID."SeP".$BookID."SeP{$protocol}://".$intranet_httppath."/";
//$Param .= "&skinType=skin6";
$Param .= "&language=".$langType;
$Param .= "&NoteType=".$NoteType;
$Param .= "&NoteID=".$NoteID;
$Param .= "&eBookUserID=".$_SESSION["UserID"];
$Param .= "&bookCategory=".$Category;
$Param .= "&bookSubCategory=".$SubCategory;
$Param .= "&bookLevel=".$Level;

if($langType == "English")
{
	$FileName = "eBook_en_3_0";
	$FileSWF = "eBook_en_3_0.swf";
}
else
{
	$FileName = "eBook_b5_3_0";
	$FileSWF = "eBook_b5_3_0.swf";
}

//$ecGatewayPath = getAMFphpGateWay($src_folder_referer, $src_folder);





?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>eBook</title>
<script language="javascript">AC_FL_RunContent = 0;</script>
<script src="AC_RunActiveContent.js" language="javascript"></script>

<style type="text/css">
body { 
	margin: 0px 0px 0px 0px; 
	}
</style>

</head>
<body bgcolor="#cccccc" marginwidth="0" marginheight="0" leftmargin="0" topmargin="0">

<? 
	//if($Publish == "1"){
	if($hasAccess){ 
?> 
	<script language="javascript">
		if (AC_FL_RunContent == 0) {
			alert("this page needs AC_RunActiveContent.js");
		} else {
			AC_FL_RunContent(
				'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0',
				'width', '100%',
				'height', '100%',
				'src', '<?=$FileName?>',
				'quality', 'high',
				'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
				'align', 'middle',
				'play', 'true',
				'loop', 'true',
				'scale', 'showall',
				'wmode', 'window',
				'devicefont', 'false',
				'id', 'eBook',
				'bgcolor', '#cccccc',
				'name', 'eBook',
				'menu', 'true',
				'allowFullScreen', 'false',
				'allowScriptAccess','sameDomain',
				'FlashVars','<?=$Param?>',			
				'movie', '<?=$FileName?>',
				'salign', ''
				); //end AC code
		}
		
		function thisMovie(movieName) {
			if (navigator.appName.indexOf("Microsoft") != -1) {
				return window[movieName];
			} else {
				return document[movieName];
			}
		}

		function Select_people(str)
		{
			var myFlashMovie = thisMovie("eBook");
			myFlashMovie.addPeopleList(str);
		}
		
		// for imail checking
		function checkOptionAdd(obj, text, value)
		{
			 obj.options[obj.length] = new Option(text, value, false, false);
			 
			if(value != "" && text != "")
			{
				var myFlashMovie = thisMovie("eBook");
				myFlashMovie.addPeopleList(text, value);
			}
		} 
		
		// for imail checking
		function checkOption(obj)
		{
			for(i=0; i<obj.length; i++){
	     	 if(obj.options[i].value== ''){
	              obj.options[i] = null;
	                }
	        }
		}
		
		function checkOptionNone(obj){
	        for(i=0; i<obj.length; i++){
	                obj.options[i].selected = false;
	        }
		}
		
		function checkOptionRemove(obj){
	        checkOption(obj);
	        i = obj.selectedIndex;
	        while(i!=-1){
	                obj.options[i] = null;
	                i = obj.selectedIndex;
	        }
		}
		
		function displayTable(tableID, myStyle){
		//block, none, inline
		/*
		var currentStyle = document.all[tableID].style.display;
		var newStyle = "none";
		
		if (typeof(myStyle)!="undefined")
		{
			newStyle = myStyle;
		} else
		{
			newStyle = (currentStyle=="none") ? "block" : "none";
		}
	
		document.all[tableID].style.display = newStyle;
		return;
		*/
		}
		
		function openWindow()
		{
			var feature = "height=400,width=600,toolbar=no,scrollbars=yes";
			//window.open("mail.html", "thewin", feature);
			//window.open("http://emeeting.broadlearning.com/home/imail/choose/index.php", "thewin", feature);
			window.open("<?=$protocol ?>://<?=$intranet_httppath?>/home/imail/choose/index.php?fieldname=Recipient[]", "thewin", feature);
		}	</script>
	<noscript>
		<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0" width="1000" height="700" id="eBook" align="middle">
		<param name="allowScriptAccess" value="sameDomain" />
		<param name="allowFullScreen" value="false" />
		<param name="movie" value="<?=$FileSWF?>?<?=$Param?>" /><param name="quality" value="high" /><param name="bgcolor" value="#cccccc" />	
		<embed src="<?=$FileSWF?>?<?=$Param?>" quality="high" bgcolor="#cccccc" width="1000" height="700" name="eBook" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
		</object>
	</noscript>
	
	<!-- default array for imail checking -->
	<form name="form1">
	<select name="Recipient[]" size="10" multiple style="display:none;">
	</select>
	</form>
	
<? }else{ ?>
	<table width="80%" border="0" cellspacing="0" cellpadding="0">
	<tbody>
	<tr><td height="100px">&nbsp;</td></tr>
	<tr><td align="center" valign="middle"><?=$eLib['SystemMsg']['AccessDenied']?></td></tr>
	<tr><td height="100px">&nbsp;</td></tr>
	</tbody>
	</table>
<? }  ?>	

</body>
<?php
intranet_closedb();


?>