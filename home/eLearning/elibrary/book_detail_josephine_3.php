<?php
//using by Josephine
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");


// additional javascript for eLibrary only
//include_once("elib_script.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("popup.html");
$CurrentPage	= "PageMyeClass";

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];
$TAGS_OBJ[] = array($title,"");

$lelib = new elibrary();
$tab_title = $lelib->getPageTabTitle();
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'].$tab_title;
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';
//$CurrentPageArr['eLib'] = 1;


$ParArr["BookID"] = $BookID;

if($CurrentPageNum == "")
$CurrentPageNum = 1;

if($DisplayNumPage == "")
$DisplayNumPage = 5;

$ParArr["CurrentPageNum"] = $CurrentPageNum;
$ParArr["DisplayNumPage"] = $DisplayNumPage;
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$returnArr = $lelib->getBookInformation($ParArr);

if($returnArr != "")
{
	/*
	$Title = iconv("utf-8","big-5",$returnArr[0]["Title"]);
	$Author = iconv("utf-8","big-5",$returnArr[0]["Author"]);
	$Source = iconv("utf-8","big-5",$returnArr[0]["Source"]);
	$Category = iconv("utf-8","big-5",$returnArr[0]["Category"]);
	$SubCategory = iconv("utf-8","big-5",$returnArr[0]["SubCategory"]);
	$Publisher = iconv("utf-8","big-5",$returnArr[0]["Publisher"]);
	$Level = $returnArr [0]["Level"];
	$DateModified = $returnArr[0]["DateModified"];
	$Description = iconv("utf-8","big-5",$returnArr[0]["Preface"]);
	$AdultContent = iconv("utf-8","big-5",$returnArr[0]["AdultContent"]);
	$Publish = $returnArr[0]["Publish"];
	*/
	$Title = $returnArr[0]["Title"];
	//debug_r($Title);
	$Author = $returnArr[0]["Author"];
	$Source = $returnArr[0]["Source"];
	$Category = $returnArr[0]["Category"];
	$SubCategory = $returnArr[0]["SubCategory"];
	$Publisher = $returnArr[0]["Publisher"];
	$Level = $returnArr[0]["Level"];
	$DateModified = $returnArr[0]["DateModified"];
	$Description = $returnArr[0]["Preface"];
	$AdultContent = $returnArr[0]["AdultContent"];
	$Publish = $returnArr[0]["Publish"];
	
	if($Description == "")
	$Description = "--";
	
	if($Level == "")
	$Level = "--";
	
	$Source = $lelib->getSourceFullName($Source);
	
	$reviewArr = $lelib->getReview($ParArr);
	
	$reviewNum = count($reviewArr);
	
	
	$sumRating = 0;
	for($c = 0; $c < $reviewNum ; $c++)
	{
		$sumRating += $reviewArr[$c]["Rating"];
	}
	
	if($reviewNum > 0)
	$rating = $sumRating / $reviewNum;
	else
	$rating = 0;

//// draw star ////////////////////////////////////////////////////////////////////////////////////////


for ($star = 1; $star <= 5; $star++)
{
	if($star <= $rating)
	{
	$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_on.gif\" width=\"15\" height=\"20\">";
	}
	else
	{
		if($star <= $rating + 0.5)
		$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_half.gif\" width=\"15\" height=\"20\">";
		else
		$x .= "<img src=\"$image_path/$LAYOUT_SKIN/eLibrary/star_off.gif\" width=\"15\" height=\"20\">";
	}
}

$starImg = $x;

////////////////////////////////////////////////////////////////////////////////////////////////////////
} //end if get book detail

$ParArr["UserID"] = $_SESSION["UserID"];
$resultArr = $lelib->displayReviewTable($ParArr, $eLib);
$contentHtml = $resultArr["content"];
$totalRecord = $resultArr["totalRecord"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}
$totalPage = ceil($totalRecord / $DisplayNumPage);

//////////////////////////////////////////////////////////////////////////////
$isFavourtie = $lelib->IS_MY_FAVOURITE_BOOK($ParArr);

if($isFavourtie)
{
	$f_link = "elib_remove_favourite.php?BookID=".$BookID."&returnPath=book_detail";
	$f_word = $eLib["html"]["remove_my_favourite"];
}
else
{
	$f_link = "elib_add_favourite.php?BookID=".$BookID."&returnPath=book_detail";
	$f_word = $eLib["html"]["add_to_my_favourite"];
}

$my_favourite_link = "<a href=\"".$f_link."\" class=\"contenttool\">";
$my_favourite_link .= $f_word."</a>";

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();

if($Publish == "1"){ //this book is published
?>


<link href="css/content_josephine.css" rel="stylesheet" type="text/css">
<link href="css/eLibrary_josephine.css" rel="stylesheet" type="text/css">
<link href="css/elib_josephine.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">
<!--

function voteReview(answer, reviewID)
{
	if(answer == "no")
	document.form1.Helpful.value = 0;
	else if(answer == "yes")
	document.form1.Helpful.value = 1;
	
	document.form1.ReviewID.value = reviewID;
	
	document.form1.action = "elib_add_review_helpful_update.php";
	document.form1.submit();
	document.form1.action = "book_detail.php";
	
} // end function

function addReview()
{
	var formObj = document.getElementById("reviewFormDiv");
	
	formObj.innerHTML = "";

	var x = "";
	
	x += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";
	x += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr>";
	x += "<td width=\"20%\" align=\"left\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">";
	x += "<?=$eLib["html"]["review_content"]?>";
	x += "</span></td>";
	x += "<td width=\"80%\" align=\"left\" valign=\"top\"><textarea name=\"review_content\" rows=\"5\" wrap=\"virtual\" class=\"textboxtext\"></textarea>";
	x += "</td></tr>";
	x += "<tr>";
	x += "<td align=\"left\" valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\"><span class=\"tabletext\">";
	x += "<?=$eLib["html"]["rating"]?>";
	x += "</span></td>";
	x += "<td align=\"left\" valign=\"top\">";
	x += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\"><tr><td>";
	
	for(var i = 1; i <=5 ; i++)
	{
		x += getStar(i);	
	} 
	x += "</td></tr></table>";
	x += "</td></tr></table>";
	x += "</td></tr>";
	
	x += "<tr>";
	x += "<td align=\"center\">";
	x += "<input name=\"comfirm\" type=\"button\" class=\"formbutton\" onClick=\"confirmForm();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"<?=$eLib["html"]["confirm"]?>\">";
	x += "&nbsp;<input name=\"reset\" type=\"reset\" class=\"formbutton\" onClick=\"resetForm();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"<?=$eLib["html"]["reset"]?>\">";
	x += "&nbsp;<input name=\"cancel\" type=\"button\" class=\"formbutton\" onClick=\"cancelForm();\" onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" value=\"<?=$eLib["html"]["cancel"]?>\">";
	x += "</td></tr>";
	//x += "<tr><td height=\"5\" class=\"dotline\"><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif\" width=\"10\" height=\"5\"></td></tr>";
	
	x += "</table>";
	
	formObj.innerHTML = x;
}

function updateRating(n)
{
	document.form1.MyRating.value = n;
}

function getStar(n)
{
	var x = "<img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif\" name=\"star"+n+"\" width=\"30\" height=\"35\" border=\"0\" id=\"star"+n+"\" onMouseOver=\"updateRating("+n+");";
	for(var i = 5; i >= 1; i--)
	{
		if(n >= i)
		x += "MM_swapImage('star"+i+"','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_on.gif',1);";
		else
		x += "MM_swapImage('star"+i+"','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);";
	}
	x += "\">";
	
	return x;
} // end function get star

function cancelForm()
{
	var formObj = document.getElementById("reviewFormDiv");
	
	formObj.innerHTML = "";
	
	x = "";
	x += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td>";
	x += "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	x += "<tr><td>";
	x += "<a href=\"#\" onClick=\"addReview();\" class=\"contenttool\"><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> <?=$eLib['html']['add_new_review']?></a></td>";
	x += "<td><img src=\"<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif\"></td>";
	x += "</tr></table>";
	x += "</td></tr></table>";
	
	formObj.innerHTML = x;
} // end cancel form

function confirmForm()
{
	document.form1.action = "elib_add_review_update.php";
	document.form1.submit();
	document.form1.action = "book_detail.php";
	
} // end confirm form

function resetForm()
{
	MM_swapImage('star1','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star2','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star3','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star4','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
	MM_swapImage('star5','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/star_big_off.gif',1);
} // end function reset form
//-->

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    
    <td align="center" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>

			<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="left">&nbsp;</td>
					</tr>
					<tr>
						<td><table width="100%" border="0" cellpadding="2" cellspacing="0">
								

								<tr>
									<td><table width="100%" border="0" cellspacing="0" cellpadding="3">
											<tr>
												<td align="left" valign="top">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="3"><tr><td>
                                                
                                                
                                                <!-- eLibrary Book Detail Begin -->
                                                <div class="elib_detail_left"></div>
                                                <div class="elib_detail_bg">
                                                
                                                <div class="elib_detail_book">

                                                <p>
                                                	<a title="View Book" href="#" onClick="MM_openBrWindowFull('tool/index.php?BookID=<?=$BookID?>','booktool','scrollbars=yes')" ><img src="/file/elibrary/content/<?=$BookID?>/image/cover.jpg" height="220" width="144">&nbsp;</a>
                                                </p>
                                              
                                                </div>
                                                
                                                <div class="elib_detail_info">
                                                	<table>
                                                    	<tr>
																	<td class="subject"><?=$eLib["html"]["title"]?></td>    
																	<td align="left" nowrap class="tabletext">:
																	<span class="eLibrary_commonlink"><strong><?=$Title?></strong></span>
																	<? if($AdultContent == "Yes"){ ?>
																	<img src="<?=$image_path?>/2009a/eLibrary/icon_sensitiveContent_<?=$intranet_session_language?>.gif" width="116" height="18" align="absmiddle" style="margin-left:20px;" alt="<?=$eLib['Book']['AdultContent']?>" />
																	<? } ?>
																	</td>
																</tr>
																
																<tr>
																	<td class="subject"><?=$eLib["html"]["author"]?></td>
																	<td align="left" nowrap class="tabletext">:
																	<span class="eLibrary_commonlink"><?=$Author?></span></td>
																</tr>
																
																<tr>
																	<td class="subject"><?=$eLib["html"]["category"]?></td>
																	<td align="left" nowrap class="tabletext">:
																	<span class="eLibrary_commonlink"><?=$Category?> -> <?=$SubCategory?>
																	<!--
																	<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle" />
																	-->
																	</span></td>
																</tr>
															
																<tr>
																	<td class="subject"><?=$eLib["html"]["level"]?></td>
																	<td align="left" nowrap class="tabletext">:
																	<span class="eLibrary_commonlink"><?=$Level?></span></td>
																</tr>
																
																<tr>
																	<td class="subject"><?=$eLib["html"]["publisher"]?></td>
																	<td align="left" nowrap class="tabletext">:
																	<span class="eLibrary_commonlink"><?=$Publisher?></span></td>
																</tr>
																
																<tr>
																	<td class="subject"><?=$eLib["html"]["source"]?></td>
																	<td align="left" nowrap class="tabletext">:
																	<span class="eLibrary_commonlink"><?=$Source?></span></td>
																</tr>
																
																<tr>
																	<td class="subject"><?=$eLib["html"]["book_input_date"]?></td>
																	<td align="left" nowrap class="tabletext">:
																	<?=$DateModified?></td>
																</tr>
                                                    </table>                                                    
                            					</div>

                                                
                                                
                                                
                                                </div>
                                                <div class="elib_detail_right"></div>
                                                
                                               	<div class="clear"></div>
                                                     
                                                <div class="elib_bookdes_wrap">
                                                 <div class="elib_bookdes_top" colspan="3"><?=$eLib["html"]["description"]?>:</div>
                                                <div class="elib_bookdes_body" copspan="3"><?=$Description?></div>
                                                
                                                <div class="elib_bookdes_bottom"></div>
                                                </div>
												 
                                                <div class="elib_tools_wrap">
                                                
                                                <!-- add fav layer Begin -->
                                                	<div id="elib_tools_fav">
                                                    		<div class="close"><a href="#" onClick="MM_showHideLayers('elib_tools_fav','','hide')">close</a></div>
                                                      <div class="body">
                                                       	<div class="fav">
                                                            	Add this book to My Favourite
                                                                <!-- This book already in your favourite list -->
                                                                <!-- Remove this book from My Favourite -->

                                                        </div>
    														<div class="bottom"><input name="submit" type="button" class="formbutton_00"
		 														onmouseover="this.className='formbuttonon_00'" onMouseOut="this.className='formbutton_00'" value="Add Book"/>
         												</div>
                                                    </div>
         										<!-- add fav layer end -->
                                                
                                                
   												  </div>
                                               
                                                	<div class="reviews">
                                                		<span class="text01"><a href="book_detail.php?BookID=$BookID" class="contenttool"><?=$eLib["html"]["rating"]?></a></span><?=$starImg?>
                          								 		
														<a href="book_detail.php?BookID=<?=$BookID?>" class="contenttool">(<?=$reviewNum?> <?=$eLib["html"]["reviewsUnit"]?><?=$eLib["html"]["reviews"]?>)</a>

                                                    </div>
                                                
                                                	<div class="fav_add" onClick="showhide('elib_tools_fav');"><span class="text01"><a href="#" onClick="MM_showHideLayers('elib_tools_fav','','show')"><?=$my_favourite_link?><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></a></span></div>
                                                    <!-- <div class="fav_remove"><span class="text01"><a href="#">Remove from My Favourite</a></span></div> -->
                                                    
                                                    <div class="add_review"><span class="text01"><a href="#">Recommend This Book</a></span></div>                                     
                                                </div>

                                                <td>
													<? if($lelib->IS_ADMIN_USER($_SESSION["UserID"])) { ?>
													<a href="elib_add_recommend.php?BookID=<?=$BookID?>" class="menu"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/icon_teacher_recomm.gif" width="20" height="20" border="0" align="absmiddle"></a><a href="elib_add_recommend.php?BookID=<?=$BookID?>" class="contenttool"> <?=$eLib["html"]["recommend_this_book"]?></a>
													<? } ?>
												</td>
												
                                                <!-- eLibrary Book Detail End -->
                                                
                                                
                                                </td></tr></table>
                                                </td>
											</tr>
											
											
										<!-- Display review Start -->
										<tr><td>
											<? if($lelib->IS_ADMIN_USER($_SESSION["UserID"])) { ?>
											<a href="elib_add_recommend.php?BookID=<?=$BookID?>" class="menu"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/icon_teacher_recomm.gif" width="20" height="20" border="0" align="absmiddle"></a><a href="elib_add_recommend.php?BookID=<?=$BookID?>" class="contenttool"> <?=$eLib["html"]["recommend_this_book"]?></a>
											<? } ?>
										</td></tr>
										
										
										<tr><td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td></tr>								
										<!-- show the display top heading -->
										<?=$lelib->displayReviewTableHeading($ParArr, $eLib);?>
										<!-- end show the display top heading -->
										
										<tr><td align="left">
										
										<!-- add review -->
										<div name="reviewFormDiv" id="reviewFormDiv">
										<table border="0" cellspacing="0" cellpadding="0"><tr><td>
										<table border="0" cellspacing="0" cellpadding="2">
										<tr><td>
										<a href="#" onClick="addReview();" class="contenttool"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_new.gif" width="18" height="18" border="0" align="absmiddle"> <?=$eLib["html"]["add_new_review"]?></a></td>
										<td><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif"></td>
										</tr></table>
										</td></tr></table>
										</div>
										<!-- end add review -->
										
										<?=$contentHtml?>
										
										<tr><td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td></tr>
										
										<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
										<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
										<input type="hidden" name="BookID" value="<?=$BookID?>" >
										<input type="hidden" name="MyRating" value="<?=$MyRating?>" >
										<input type="hidden" name="Helpful" value="0" >
										<input type="hidden" name="ReviewID" value="0" >
										
										
										<!-- Display review End -->
											
											</tr>
									</table></td>
								</tr>
						</table></td>
					</tr>
			</table></td>

		</tr>
	</table></td>
   
  </tr>
  

</table>

<? } else {?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td align="center">
<table width="96%" border="0" cellspacing="0" cellpadding="8"><tr><td>	
<table width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td align="left">&nbsp;</td></tr>
<tr><td>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
	<td height="5"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="5"></td>
</tr>
<tr>
<td>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td align="left" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr><td>"Warning: Access denied."</td></tr>
</table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
</td></tr></table>
	
<?}?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>