<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();


$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$Libelibinstall = new elibrary_install();	// 2013-08-19 Charles Ma
//2013-08-19 Charles Ma
if(!$Libelibinstall->retrive_license_enable())
	$license_style = 'style="display:none;"';

$LibeLib->DeleteTag($TagID);

intranet_closedb();

header("location: elib_setting_tags.php?xmsg=DeleteSuccess")
?>