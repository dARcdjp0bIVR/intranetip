<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();
#######################################################
$objInstall	= new elibrary_install();

$YearClassID 	= isset($_REQUEST['YearClassID'])? trim($_REQUEST['YearClassID']) : "";
$BookID 		= isset($_REQUEST['BookID'])? trim($_REQUEST['BookID']) : "";
$aryStudentID	= isset($_REQUEST['aryStudentID'])? $_REQUEST['aryStudentID'] : "";

if(empty($YearClassID)){
	intranet_closedb();
	die();
}

##Get list of users already assigned to this book
$aryStudent = $objInstall->get_studentID_license_list($BookID);

if(is_array($aryStudentID) && $aryStudentID != array()){
	$aryStudent = array_merge($aryStudentID, $aryStudent);
}

if($aryStudent[0] !=""){
	$strUserID = implode(",", $aryStudent);
}

## return select box UI
echo $objInstall->gen_year_class_user_ui("StudentID",$YearClassID,$strUserID);

#######################################################
intranet_closedb();
?>