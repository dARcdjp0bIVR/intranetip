<?
/* !!!!!!!!!!!!!!!!!IMPORTANT NOTE START!!!!!!!!!!!!!!!!!
 * 
 * 	Please update modification log after edit any line below [Date, Name, IP ver. and case number if any]!!
 * 
 * !!!!!!!!!!!!!!!!!!IMPORTANT NOTE END!!!!!!!!!!!!!!!!!!
 * Editing by Jason
 * 
 * Modification Log:
 * 2015-10-04 (Jason) [ip.2.5.6.10.1]
 * 		- search the part with // added or modified on 2015-10-4
 * 		- fix the problem of not showing marked version by teacher in student view
 * 		- revise and improve the logic of showing student version, marked version and redo verion 
 * 
 * 2015-02-27 (Cameron) display Chinese term for marking board if $intranet_session_language is b5
 * 
 * 2015-02-11 (Siuwan) [ip.2.5.6.3.1]
 *		- update the way to get schoolcode and sheme by schemecode since schoolcode has been changed after ip.2.5.5.10.1 (Case:#C75215)
 */

include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingXml.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");
include_once($PATH_WRT_ROOT."includes/w2/libHandinLayout.php");
include_once($PATH_WRT_ROOT."includes/w2/libStepStudentComment.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2RefManager.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2PeerMarkingData.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");

$writingId = $r_writingId;
$stepCode = trim($r_stepCode);
$contentCode = trim($r_contentCode);
$schemeCode = trim($r_schemeCode);
$r_handinSubmitStatus = trim($r_handinSubmitStatus);
$r_fromToBeMarked = trim($r_fromToBeMarked);
$showMarkingSummary = false;
$showMarkingBoard = false;
$showCorrectionArea = false;
$showOverallMark = false;
$isHistory = false;

if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	$writingObjSource = $w2_cfg["writingObjectSource"]["xml"];
}
else {
	$writingObjSource = $w2_cfg["writingObjectSource"]["db"];
}
$objWriting = libWritingFactory::createWriting($writingObjSource, $writingId, $contentCode, $schemeCode);
$w2_objContent = libW2ContentFactory::createContent($r_contentCode);
$objHandinLayout = new libHandinLayout();
$objHandinLayout->setContentCode($contentCode);
$isCurrentMarkingRecordSaveAsScore = $objWriting->isCurrentMarkingRecordSaveAsScore($r_stepId, $p_studentId);	//20140529-w2gsgame
### Get the steps of the Writing
$stepArr = $objWriting->findStep();

### calculate contentCode , schemeCode , stepCode in case r_stepCode is empty
if($stepCode ==''){
	$contentCode = $objWriting->getContentCode();
	$schemeCode = $objWriting->getSchemeCode();
	$stepCode = $stepArr[0]['STEP_CODE']; //DEFAULT TAKE THE STEP CODE FROM THE FIRST STEP => $steps[0]
}
### Get TemplateID, ClipArtID


$templateID =  $objWriting->getTemplateID();
$clipArtID = $objWriting->getClipArtID();

$templateID = ($templateID)?$templateID:1;
$clipArtID = ($clipArtID)?$clipArtID:1;

$stepInfoAry = $w2_libW2->getStepInfoByContent($contentCode);
$lastStepCode = $stepInfoAry[count($stepInfoAry)-1]['code'];
$isLastStep = false;
if ($stepCode == $lastStepCode) {
	$isLastStep = true;
}

/*
ifsharing(){
	$w2_thisStudentID  = $this_sharingStudentID;
}
*/


### Get the info of the writings of the student
$writingInfoAry = $w2_libW2->findStudentWriting($w2_thisStudentID, $contentCode, $writingId);
$writingStudentId = $writingInfoAry[0]['WRITING_STUDENT_ID'];       // store W2_WRITING_STUDENT.WRITING_STUDENT_ID
$writingSumbitStatus = $writingInfoAry[0]['HANDIN_SUBMIT_STATUS'];  // store submit status for the writing
$studentHasSubmittedWriting = ($writingSumbitStatus == $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"])? true : false;


$contentDetailsAry = $w2_libW2->getModelFile($contentCode,$schemeCode,$stepCode);



if($w2_thisAction == $w2_cfg["actionArr"]["sharing"]){
	//sharing view template
	$contentTemplate = $PATH_WRT_ROOT.'home/eLearning/w2/templates/writing/view.php';
}else{
	//individual writing view template
	if((!$isTeaching||!empty($w2_thisAction))&&(strpos($schemeCode,'_')!==false)){

		#Get Dynamic Input Content
		$contentTemplate = $w2_cfg['HandinArr']['WritingTemplate'][$contentCode][$stepCode];	
		$hasDynamicContent = !empty($contentTemplate)?true:false;
		##Retrieve from database
		//2015-02-11 Siuwan get schoolcode and scheme by schemecode
		$w2_code_ary = explode('_',$schemeCode);
		$scheme = array_pop($w2_code_ary);
		$schoolCode = implode("_",$w2_code_ary);
		
		$ansPrefix = $schoolCode.'_'.$contentCode.'_'.$scheme.'_'.$stepCode.'_ans';
		$schemeNum = str_replace('scheme','',$scheme);
		$engContentParser = new libW2EngContentParser();
		$contentData = array($schoolCode,$schemeNum,$contentCode);
		$parseData = $engContentParser->parseEngContent('',$contentData);
		
		###Step Data###
		$engContentParser->getResouceArrayReparsed(&$parseData);
	
		$step1Data = $parseData['step1Data'];
		$step2Data = $parseData['step2Data'];
		$step3Data = $parseData['step3Data'];
		$step4Data = $parseData['step4Data'];
		$step5Data = $parseData['step5Data'];
		$step6Data = $parseData['step6Data'];		
		$curStep = str_replace('c','',$stepCode);
		$hasTeacherAttachment = count($parseData['teacherAttachmentData']['teacherAttachment_'.$curStep])>0;
		if(count($parseData)>0){ //is content input
			$cid = $parseData['cid'];
			
			$conceptType = $parseData["conceptType"];
			switch($conceptType){
				case $w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]:
					$conceptMapId = $parseData["powerConceptID"];
					break;
				case $w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]:
					$conceptMapId = $parseData["powerBoardID"];
					break;
				default:$conceptMapId='';
			}
			if($contentCode=='sci'){
				$myKeywordId = $schoolCode.'_'.$contentCode.'_'.$scheme.'_c1_ans3';
				$w2_m_requestAnsCode = array($myKeywordId);
			}
			if($contentTemplate=='conceptMap'){
				$isConceptMap = true;
				if($contentCode=='sci'){
					$w2_m_refDisplayMode = $w2_cfg["refDisplayMode"]["view"];
					$infoboxStep = array_search('referenceMaterials', $w2_cfg['HandinArr']['WritingTemplate']['sci']);
					$infoboxPrefix = $schoolCode.'_'.$contentCode.'_infobox_'.$scheme.'_'.$infoboxStep.'_box';		
					$articleCnt = count($step2Data['step2ArticleAry']);
					$w2_m_infoboxCode = array();
					for($i=0;$i<$articleCnt;$i++){
						$w2_m_infoboxCode[$i] = $infoboxPrefix.($i+1);
					}	
					$c_model_cfg['refPresetAry'] = $w2_libW2->getSciencePresetCategoryVocabulary($cid,$w2_m_infoboxCode,$myKeywordId);
					
				}
			}else if($contentTemplate=='sampleWriting'){
				$isSampleWriting = true;
				if($contentCode=='eng'){
					if(!empty($step3Data['step3SampleWriting'])){
						$step3ParagraphAry['paragraphItem1']['paragraph'] = $step3Data['step3SampleWriting'];
					}else{
						$step3ParagraphAry = $step3Data['step3ParagraphAry'];
					}
					$step3RemarkAry = $step3Data["step3RemarkAry"];
					$step3ParagraphHTML = $w2_libW2->getEssayBubbleHTML($step3ParagraphAry);
					$step3RemarkHTML = $w2_libW2->getEssayRemarkHTML($step3RemarkAry);
				}elseif($contentCode=='sci'){
					$stepParagraphHTML = $w2_libW2->getEssayBubbleHTML($step4Data['step4ParagraphAry']);
					$stepRemarkHTML = $w2_libW2->getEssayRemarkHTML($step4Data['step4RemarkAry']);
				}
			}else if($contentTemplate=='doWriting'){
				if($contentCode=='chi'){
					$outlineCnt = count($step3Data['step3OutlineAry']);
					$sampleWritingStep = array_search('sampleWriting', $w2_cfg['HandinArr']['WritingTemplate'][$contentCode]);
					$writingPrefix = $schoolCode.'_'.$contentCode.'_'.$scheme.'_'.$sampleWritingStep.'_ans';
					$defaultWriting = $step5Data['step5DefaultWriting'];
				}elseif($contentCode=='sci'){
					$outlineCnt = count($step5Data['step5OutlineAry']);
					$writingApproachStep = array_search('writingApproach', $w2_cfg['HandinArr']['WritingTemplate'][$contentCode]);
					$writingPrefix = $schoolCode.'_'.$contentCode.'_'.$scheme.'_'.$writingApproachStep.'_ans';
					$defaultWriting = $step6Data['step6DefaultWriting'];
				}elseif($contentCode=='eng'){
					$outlineCnt = count($step4Data['step4DraftQList']);
					$writingApproachStep = array_search('writingApproach', $w2_cfg['HandinArr']['WritingTemplate'][$contentCode]);
					$writingPrefix = $schoolCode.'_'.$contentCode.'_'.$scheme.'_'.$writingApproachStep.'_ans';
					$defaultWriting = $step5Data['step5DefaultWriting'];					
				}else{
					$outlineCnt = count($step4Data['step4OutlineAry']);
					$writingApproachStep = array_search('writingApproach', $w2_cfg['HandinArr']['WritingTemplate'][$contentCode]);
					$writingPrefix = $schoolCode.'_'.$contentCode.'_'.$scheme.'_'.$writingApproachStep.'_ans';		
					$defaultWriting = $step5Data['step5DefaultWriting'];			
				}
				$w2_m_writingDefaultContent = '<font color="'.$w2_cfg['defaultWritingTextColor'].'">'.$defaultWriting.'</font><br /><br />';
				$w2_m_requestAnsCode = array();
				
				for($o=1;$o<=$outlineCnt;$o++){
					$w2_m_requestAnsCode[] = $writingPrefix.$o;
					$w2_m_writingDefaultContent .= '<!--answer'.$o.'--><br /><br />';
				}
				$w2_m_ansCode = array($ansPrefix.'1');
			}else if($contentTemplate=='termsAndInformation'){
				$infoboxPrefix = $schoolCode.'_'.$contentCode.'_infobox_'.$scheme.'_'.$stepCode.'_ans';		
				$sourceCnt = count($step2Data['step2SourceAry']);
				$w2_m_infoboxCode = array();
				for($i=0;$i<$sourceCnt;$i++){
					$w2_m_infoboxCode[$i] = $infoboxPrefix.($i+1);
				}
			}else if($contentTemplate=='referenceMaterials'){
				$w2_m_refDisplayMode = $w2_cfg["refDisplayMode"]["manage"];
				$infoboxPrefix = $schoolCode.'_'.$contentCode.'_infobox_'.$scheme.'_'.$stepCode.'_box';		
				$articleCnt = count($step2Data['step2ArticleAry']);
				$w2_m_infoboxCode = array();
				for($i=0;$i<$articleCnt;$i++){
					$w2_m_infoboxCode[$i] = $infoboxPrefix.($i+1);
				}				
				$c_model_cfg['refPresetAry'] = $w2_libW2->getSciencePresetCategoryVocabulary($cid,$w2_m_infoboxCode,$myKeywordId);
			}else if($contentTemplate=='topicAndTheme'&&$contentCode=='eng'){
				$typeList = array();
				$step1TextTypeAry = array();
				foreach ( (array)$w2_cfg_contentSetting[$contentCode] as $item)
				{
					$typeList[] = $item['type'];	
				}
				$step1TextTypeAry = array_unique($typeList);
				$step1TextTypeAry = array_diff($step1TextTypeAry, array($step1Data['step1TextType']));
				
				shuffle($step1TextTypeAry);
				
			}else if(!$hasDynamicContent&&$hasTeacherAttachment){
				$teacherAttachmentBtnHTML = '<div class="btn_right_grp_new"><a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox(\'\',\'<h1 class=ref>'.addslashes($Lang['W2']['teacherAttachment']).'</h1>\',\'\');"><span>'.$Lang['W2']['teacherAttachmentWithBreak'].'</span></a></div>';
			}
		}
	}
	$contentTemplate = !empty($contentTemplate)?$PATH_WRT_ROOT.'home/eLearning/w2/templates/content/'.$contentCode.'/'.$contentTemplate.'.php':$contentDetailsAry['view'];
//error_log("\n\n contentTemplate-->".print_r($contentTemplate,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
	
}
if(!$hasDynamicContent){
	$schemeModel = $contentDetailsAry['schemePath'].'/model.php';
	$contentModel = $contentDetailsAry['model'];
	include_once($schemeModel); // for the whole scheme
	include_once($contentModel); // for individual content 
}else{
	#Set config to true 
	$c_model_cfg['withConceptMapStep'] = count($w2_cfg['HandinArr']['withConceptMapStep'][$contentCode])>0?$w2_cfg['HandinArr']['withConceptMapStep'][$contentCode]:$c_model_cfg['withConceptMapStep'];
	$conceptMapAnsCodePrefix = ($r_contentCode!='eng')?'_conceptmap':'';
	$c_model_cfg['withConceptMapStep_ansCode'] = $schoolCode.'_'.$r_contentCode.'_'.$scheme.'_'.$stepCode.$conceptMapAnsCodePrefix.'_ans1';
	$w2_m_config_hideApprovalBtn							= $w2_cfg['HandinArr']['hideApprovalBtn'][$contentCode][$stepCode];
	$w2_m_config_hideTeacherCommentBtn						= $w2_cfg['HandinArr']['hideTeacherCommentBtn'][$contentCode][$stepCode];
	$w2_m_config_disabledNextBtnIfNotClickedAllModelAnswer	= $w2_cfg['HandinArr']['disabledNextBtnIfNotClickedAllModelAnswer'][$contentCode][$stepCode];
	$w2_m_config_disableAnswerIfCompletedWriting			= $w2_cfg['HandinArr']['disableAnswerIfCompletedWriting'][$contentCode][$stepCode];
	$w2_m_config_hideCheckBtnIfSubmittedWriting				= $w2_cfg['HandinArr']['hideCheckBtnIfSubmittedWriting'][$contentCode][$stepCode];
	$w2_m_config_hideCheckBtnIfCompletedWriting				= $w2_cfg['HandinArr']['hideCheckBtnIfCompletedWriting'][$contentCode][$stepCode];
	$w2_m_config_showModelAnswerIfSubmittedStep				= $w2_cfg['HandinArr']['showModelAnswerIfSubmittedStep'][$contentCode][$stepCode];
	$w2_m_config_showModelAnswerIfCompletedWriting			= $w2_cfg['HandinArr']['showModelAnswerIfCompletedWriting'][$contentCode][$stepCode];
	$w2_m_config_requireInputAllAnswer						= $w2_cfg['HandinArr']['requireInputAllAnswer'][$contentCode][$stepCode];
	$w2_m_config_getStudentFileInThisStep					= $w2_cfg['HandinArr']['getStudentFileInThisStep'][$contentCode][$stepCode];	
	$_m_objWritingStudent = new libWritingStudent($r_writingStudentId);
	$h_vocabDispaly = nl2br($_m_objWritingStudent->getOtherInfo());

}
$stepId = 0;
$thisStepRequestApproval = $w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["NO"]; // default no need to approval
for($i = 0,$i_max = count($stepArr);$i< $i_max;$i++){
	$_stepCode = $stepArr[$i]['STEP_CODE'];
	$_stepSequence = $stepArr[$i]['STEP_CODE'];
	$_stepId = $stepArr[$i]['STEP_ID'];
	$_stepRequestApproval = $stepArr[$i]['STEP_REQUIRE_APPROVAL'];
	if($_stepCode == $stepCode){
		$stepId = $_stepId;
		$thisStepRequestApproval = $_stepRequestApproval;
		break;
	}
}

### Navigation
$objHandinLayout->setNavigationHtml($w2_libW2->getNavigationHtml());

### Scheme
//$schemeName = $w2_libW2->getSchemeNameBySchemeCode($schemeCode,$contentCode);
$w2_objContent->setSchemeCode($schemeCode);
if($w2_thisAction==$w2_cfg["actionArr"]["sharing"]){
	$objHandinLayout->setUnitTitle($w2_objContent->getSchemeTitle());
	$objHandinLayout->setUnitName($w2_objContent->getSchemeName());
	
	/*#Student Name*/
	$objHandinLayout->setSchemeTitle($Lang['W2']['studentName']);
	$shareWritingAuthorStudentInfo = $w2_libW2->getWritingStudentInfoAry($r_contentCode, $r_writingId);
	$shareWritingAuthorStudentName = $intranet_hardcode_lang=='en'?$shareWritingAuthorStudentInfo[0]['USER_ENGLISH_NAME']:$shareWritingAuthorStudentInfo[0]['USER_CHINESE_NAME'];
	$shareWritingAuthorStudentClassName = $shareWritingAuthorStudentInfo[0]['CLASS_NAME'];
	$shareWritingAuthorStudentClassNumber = $shareWritingAuthorStudentInfo[0]['CLASS_NUMBER'];
	$objHandinLayout->setSchemeName("$shareWritingAuthorStudentName ($shareWritingAuthorStudentClassName - $shareWritingAuthorStudentClassNumber)");
	
}else{
	$objHandinLayout->setUnitTitle($w2_objContent->getUnitTitle());
	$objHandinLayout->setUnitName($w2_objContent->getUnitName());
	$objHandinLayout->setSchemeTitle($w2_objContent->getSchemeTitle());
	$objHandinLayout->setSchemeName($w2_objContent->getSchemeName());
}


### Toolbar
// Get Step related Info for this student


$studentStepStatusAry = $w2_libW2->findStudentWritingStepStatus($w2_thisStudentID, $writingId);

$studentStepStatusAssoAry = BuildMultiKeyAssoc($studentStepStatusAry, 'STEP_ID');
$stepStudentStatus = $studentStepStatusAssoAry[$stepId]['STEP_STATUS'];
$stepStudentApprovalStatus = $studentStepStatusAssoAry[$stepId]['STEP_APPROVAL_STATUS'];

$studentHasSubmittedStep = ($stepStudentStatus == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"])? true : false;

$studentStepHandinRecord = $w2_libW2->getStudentStepHandinRecord($stepId,$w2_thisStudentID,$w2_m_ansCode[0]);
$stepStudentCommentId = $studentStepStatusAssoAry[$stepId]['COMMENT_ID'];
$handinVersion = $r_version?$r_version:$studentStepHandinRecord['VERSION'];
$handinLatestVersion = $studentStepHandinRecord['VERSION'];		// added on 2015-10-4
$studentStepHandinAnswerMarked = $studentStepHandinRecord['ANSWER_MARKED'];	// added on 2015-10-4
$isHandinMarked = ($studentStepHandinAnswerMarked != null && $studentStepHandinAnswerMarked != '') ? true : false;	// added on 2015-10-4

###Get Teacher Comment
$teacherCommentAry = current($w2_libW2->getTeacherComment($stepId,$w2_thisStudentID,$w2_thisUserID,$handinVersion));
$teacherSticker = $teacherCommentAry['STICKER'];
$stepStudentCommentId = $teacherCommentAry['COMMENT_ID'];


### Get Peer Marking Data
// for peer marking mode, $w2_thisPeerMarkingMarkerUserId is not null and therefore only return the peer comment of the specific student
// for viewing student's own writing, $w2_thisPeerMarkingMarkerUserId is null and therefore return all peer comment for all other students
$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($w2_thisPeerMarkingMarkerUserId, $writingId, $markedOnly=true, $w2_thisStudentID);

$numOfPeerMarkingData = count($peerMarkingDataInfoAry);
$hasPeerCommented = false;
if ($numOfPeerMarkingData > 0) {
	$hasPeerCommented = true;
}
if($thisStepRequestApproval){
	// $stepStudentApprovalStatus default value in DB is "0" , so it to $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"] 
	// as a default if "$stepStudentApprovalStatus" is empty
	$stepStudentApprovalStatus = ($stepStudentApprovalStatus == 0) ? $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]: $stepStudentApprovalStatus;
}
//get the student submit status , to handle display of save as draft button
$sql = 'select STEP_ID , USER_ID, STEP_STATUS, COMMENT_ID from W2_STEP_STUDENT where STEP_ID = '.$stepId.' and USER_ID = '.$w2_thisStudentID;
$_objDb = new libdb();
$result = current($_objDb->returnResultSet($sql));  // with DB unique key with (`STEP_ID`,`USER_ID`), suppose return only one row
$_stepStatus = $result['STEP_STATUS'];
//$studentInTheStepDetails = getStudentWritingStepStatus();
switch ($w2_thisAction) {
	case $w2_cfg["actionArr"]["marking"]:
		$successMsg = $Lang['W2']['returnMsgArr']['teacherCommentUpdateSuccess'];
		$failMsg = $Lang['W2']['returnMsgArr']['teacherCommentUpdateFailed'];
		###Siuwan 20131211 Get Teacher Mark
		$marker_score = $studentStepHandinRecord['MARK'];
		$marker_comment = $teacherCommentAry['CONTENT'];
		$marker_sticker = $teacherSticker;
		$showMarkingSummary = true;
		$showMarkingBoard = true;
		$showOverallMark = true;
		
		###Siuwan 20131211 Always display redo button
		if($stepStudentStatus==$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]){
			$redoBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"], $stepStudentApprovalStatus, "");
		}else{
			$redoBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]."', '".$w2_thisStudentID."');");
		}
	
		if(!$isLastStep||$objWriting->isCompleted()){
			$redoBtn = '';
		}
		if ($w2_m_config_hideApprovalBtn) {
			// do nth
		}
		else {

			### Adam 20140430 Check the approval flag
			if($thisStepRequestApproval){
				$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $stepStudentStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]."', '".$w2_thisStudentID."');");
				$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]."', '".$w2_thisStudentID."');");
			}
			//$redoBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"], $stepStudentStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]."', '".$w2_thisStudentID."');");
		}

		if($thisStepRequestApproval){
//			if($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]){
				$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]."', '".$w2_thisStudentID."');");
				$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]."', '".$w2_thisStudentID."');");
/*
			}else{
				$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]."', '".$w2_thisStudentID."');");
				$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]."', '".$w2_thisStudentID."');");
			}
*/
		}

		if ($waitingForApprovalBtn != '') {
			$objHandinLayout->addActionButton($waitingForApprovalBtn);
		}
		if ($approveBtn != '') {
			$objHandinLayout->addActionButton($approveBtn);
		}
		if ($redoBtn != '') {
			$objHandinLayout->addActionButton($redoBtn);
		}		
		
		$onchange = "goToStep('', '', '', '', '', this.value);";
		
		$includeStudentIdAry = null;
		if ($r_fromToBeMarked) {
			$toBeMarkedStudentInfoAry = $w2_libW2->getWritingToMarkStudent($writingId);
			$toBeMarkedStudentIdAry = Get_Array_By_Key($toBeMarkedStudentInfoAry[$writingId], 'USER_ID');
		}
		$objHandinLayout->setStudentSelectionHtml($w2_libW2->getHandinStudentSelectionHtml($contentCode, $writingId, $stepId, $w2_thisStudentID, $onchange, $r_handinSubmitStatus, $toBeMarkedStudentIdAry));
		break;
	case $w2_cfg["actionArr"]["sharing"]:
		/*
		$onchange = "goToStep('', '', '', '', '', this.value);";
		
		$includeStudentIdAry = null;
		if ($r_fromToBeMarked) {
			$toBeMarkedStudentInfoAry = $w2_libW2->getWritingToMarkStudent($writingId);
			$toBeMarkedStudentIdAry = Get_Array_By_Key($toBeMarkedStudentInfoAry[$writingId], 'USER_ID');
		}
		*/
		break;
	case $w2_cfg["actionArr"]["peerMarking"]:
			$successMsg = $Lang['W2']['returnMsgArr']['peerCommentUpdateSuccess'];
			$failMsg = $Lang['W2']['returnMsgArr']['peerCommentUpdateFailed'];
			###Siuwan 20131211 Get Peer Mark
			$peerMarkingDataAry = current($w2_libW2->getPeerComment($writingId,$w2_thisStudentID,$w2_thisUserID,$handinVersion,true));
	
			$marker_score = $peerMarkingDataAry['MARK'];
			$marker_comment = $peerMarkingDataAry['COMMENT'];
			$marker_sticker = $peerMarkingDataAry['STICKER'];	
			$showMarkingBoard = true;		
			$showOverallMark = true;
			
		break;
			
	default:
		$w2_thisAction = ($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"])?$w2_cfg["actionArr"]["redo"]:$w2_thisAction;
		if($objWriting->isCompleted()){
			$showOverallMark = true;
			$showMarkingSummary = true; 
			
			// added on 2015-10-4
			$stepHandinHistory = $w2_libW2->getStepHandinHisotryRecord($stepId,array($w2_thisStudentID),$handinVersion);
			$isHistory = count($stepHandinHistory)>0;
			if($isHistory){
				# for history record by redo
				$h_writingTaskContent = ($stepHandinHistory[0]['ANSWER_MARKED'] != '') ? $stepHandinHistory[0]['ANSWER_MARKED'] : $stepHandinHistory[0]['ANSWER'];
			} else {
				# for the latest submission
				$h_writingTaskContent = $studentStepHandinRecord['ANSWER_MARKED'];
			}
		} else {
			if($thisStepRequestApproval){
				if($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]){
					$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentStatus,$onClickAction='');
				//}elseif($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"] || trim($stepStudentApprovalStatus) == '0'){
				}elseif(($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]|| trim($stepStudentApprovalStatus) == '0')&&$studentHasSubmittedStep){
					$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $studentApprovalStatue = $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"],$onClickAction='');
				}else{
					//don't display the button , do nothing
				}
			}
			if ($waitingForApprovalBtn != '') {
				$objHandinLayout->addActionButton($waitingForApprovalBtn);
			}
			if ($approveBtn != '') {
				$objHandinLayout->addActionButton($approveBtn);
			}
			if ($studentHasSubmittedWriting) {
				$showMarkingSummary = true; 
			}
			
			if ($isHandinMarked == true){	// added on 2015-10-4
				# init and set the content to the latest AnswerMarked version, will be overrided later by certain conditions
				$h_writingTaskContent = $studentStepHandinAnswerMarked;
			}
			
			$stepHandinHistory = $w2_libW2->getStepHandinHisotryRecord($stepId,array($w2_thisStudentID),$handinVersion);
			$isHistory = count($stepHandinHistory)>0;
			if($isLastStep&&!empty($handinVersion)&&$handinVersion>1&&$_stepStatus==$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]||$w2_thisAction==$w2_cfg["actionArr"]["redo"]){
				$showOverallMark = true;
				$showMarkingSummary = true; 
				$showCorrectionArea = ($isHandinMarked) ? false : true;		// modified on 2015-10-4
				$answerMarked = $studentStepHandinRecord['ANSWER_MARKED'];
				$draftVersion = $handinVersion;
			}
			if($isHistory){
				$answerMarked = $stepHandinHistory[0]['ANSWER_MARKED'];	
				$h_writingTaskContent = ($answerMarked != '') ? $answerMarked : $stepHandinHistory[0]['ANSWER'];	// modified on 2015-10-4
				$draftVersion = $handinVersion;
				$showCorrectionArea = false;	// added on 2015-10-4
			}elseif($w2_thisAction==$w2_cfg["actionArr"]["redo"]||$handinVersion>1){ //Redo
				if(empty($studentStepHandinRecord['ANSWER_MARKED'])||$_stepStatus!=$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]){
					$stepHandinHistory = $w2_libW2->getStepHandinHisotryRecord($stepId,array($w2_thisStudentID),$handinVersion-1);
					$answerMarked = $stepHandinHistory[0]['ANSWER_MARKED'];	
					$answerOfPreviousVersion = $stepHandinHistory[0]['ANSWER'];	
					$draftVersion = $handinVersion-1;
				}
				$h_writingTaskContent = $answerMarked;
			}
		}
		
		break;
}
	
### Teacher's Comment
if ($w2_m_config_hideTeacherCommentBtn) {
	// hide button => do nth	
}
else {
	//$isCommentEditMode = ($w2_thisAction == $w2_cfg["actionArr"]["marking"])? true : false; 
	//$objHandinLayout->setTeacherCommentHtml($w2_libW2->getTeacherCommentButton().$w2_libW2->getTeacherCommentLayer($isCommentEditMode, $stepStudentCommentId));

	$showCommentBtn = false;
	$showSharingToStudentBtn = false;
	$isCommentEditMode = false;
	$showPeerCommentBtn = false;
	$isPeerCommentEditMode = false;
	$isViewSelfPeerComment = false;
	if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
//		// teacher marking => always show comment button and can always edit the comment
//		$showCommentBtn = true;
//		$isCommentEditMode = true;

		// teacher marking => show comment button if the student has submitted this step
		if ($studentHasSubmittedStep) {
			$showCommentBtn = true;
			$isCommentEditMode = true;
			
			if ($isLastStep) {
				// peer comment only show in last step
				$showPeerCommentBtn = true;
				$showSharingToStudentBtn = true;
				$isViewSelfPeerComment = true;

			}
		}
	}
	else if ($w2_thisAction == $w2_cfg["actionArr"]["peerMarking"]) {
		if ($isLastStep && $studentHasSubmittedStep) {
			$showPeerCommentBtn = true;
			$isPeerCommentEditMode = true;
			$peerMarkingDataRecordId = $peerMarkingDataInfoAry[0]['RECORD_ID'];
		}
	}
	else {
		// not marking 	=> student view own handin => always view only
		
		if ($stepStudentCommentId == '') {
			// no comment => hide the comment button
		}
		else {
			$showCommentBtn = true;
		}
		
		if ($isLastStep && $hasPeerCommented) {
			$showPeerCommentBtn = true;
			$isViewSelfPeerComment = true;
		}
	}
	
	$commentBtnHtml = '';
	if ($showCommentBtn) {
		$commentBtnHtml = $w2_libW2->getTeacherCommentButton().$w2_libW2->getTeacherCommentLayer($isCommentEditMode, $stepStudentCommentId);
	}
	//$objHandinLayout->setTeacherCommentHtml($commentBtnHtml);


	if($showSharingToStudentBtn){
		//function shareToStudent(contentCode,schemeCode,stepId,stepHandinCode,studentId){

		$sql = "select count(*) as `count` from {$intranet_db}.W2_SHARE_TO_STUDENT_HANDIN where CONTENT_CODE='{$contentCode}' and SCHEME_CODE ='{$schemeCode}' and STEP_ID  ={$stepId} and STUDENT_ID={$w2_thisStudentID} and STEP_HANDIN_CODE='{$w2_m_ansCode[0]}'";
		$objDB = new libdb();
		$_rsTmp = $objDB->returnVector($sql);
		$_setToShareBefore = current($_rsTmp);
			


		$_setShareAction = 1; //1 --> insert this sharing to DB
		if($_setToShareBefore > 0){
			//since share before , delete it
			$_setShareAction = 0; //0 --> delete this sharing to DB
		}
		//error_log("_setToShareBefore[".$_setToShareBefore."]   _setShareAction [".$_setShareAction."] sql = ".$sql."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		//$_shareCaption = ($_setShareAction == 1)?'Set Sharing to Student':'Cancel Sharing to Student';
		$sharingToStudentBtnHtml = $w2_libW2->getShareButton($_setShareAction, $contentCode, $schemeCode, $stepId, $w2_m_ansCode, $w2_thisStudentID);

		$_setShareAction = null; // reset it
	}
	$objHandinLayout->setSharingToStudentHtml($sharingToStudentBtnHtml);

	
	$peerCommentBtnHtml = '';
	if ($showPeerCommentBtn) {
		$peerCommentBtnHtml = $w2_libW2->getPeerCommentButton().$w2_libW2->getPeerCommentLayer($isPeerCommentEditMode, $peerMarkingDataRecordId, $isViewSelfPeerComment, $writingId, $w2_thisStudentID);
	}
	//$objHandinLayout->setPeerCommentHtml($peerCommentBtnHtml);
}


### Step
//$stepArr = $w2_libW2->getWritingWholeStep($writingId,1);
//$stepArr = $objWriting->findStep();
$allStepAns = getWritingWholeStepAns($writingId,$w2_thisStudentID);

$stepAns = array();
$stepAns = praseAllStepAns($allStepAns);

$thisStepAns = $stepAns[$stepId];

$w2_s_stepAnsAry = array();
if(is_array($w2_m_requestAnsCode ) && count($w2_m_requestAnsCode) > 0){
	$w2_s_stepAnsAry = $w2_libW2->getWritingStepAnsByStepCode($w2_m_requestAnsCode,$writingId,$w2_thisStudentID);
}

$w2_s_thisStepAns = array();
foreach((array)$thisStepAns as $_stepCode => $_stepAns){
	$w2_s_thisStepAns[$_stepCode] = $_stepAns;
}
$h_walkingStepHtml = $w2_libW2->getWalkingStep($stepArr,$schemeCode,$writingId,$contentCode,$stepId,$studentStepStatusAry, $objWriting->isCompleted(), $w2_thisAction, $stepCode);
if ($w2_thisAction == $w2_cfg["actionArr"]["peerMarking"]) {
	$h_walkingStepHtml = '';
}else if($w2_thisAction == $w2_cfg["actionArr"]["sharing"]){
	$h_walkingStepHtml = '';
}

$objHandinLayout->setStepHtml($h_walkingStepHtml);

### Content
$preStepID = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,0);
$preStepCode = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,0,2);

$nextStepID = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,1);
$nextStepCode = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,1,2);

if($preStepID > 0){
	$h_preStepButton = $w2_libW2->htmlStepButton($schemeCode,$preStepCode,$preStepID,$writingId,$contentCode,0);
}


############### Answer Display [Start] ###############
$w2_h_answerDefaultDisabledAttr = '';
if ($w2_m_config_disableAnswerIfSubmittedWriting && $studentHasSubmittedWriting) {	// disable the answer input if the writing is submitted
	$w2_h_answerDefaultDisabledAttr = 'disabled';
}
if ($w2_m_config_disableAnswerIfCompletedWriting && $objWriting->isCompleted()) {
	$w2_h_answerDefaultDisabledAttr = 'disabled';
}
//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
if ($w2_thisActionIsMarking) {
	$w2_h_answerDefaultDisabledAttr = 'disabled';
}

$w2_h_checkBtnDefaultDisplayAttr = '';
if ($w2_m_config_hideCheckBtnIfSubmittedWriting && $studentHasSubmittedWriting) {	// hide the "check" button if the writing is submitted
	$w2_h_checkBtnDefaultDisplayAttr = 'display:none;';
}
if ($w2_m_config_hideCheckBtnIfCompletedWriting && $objWriting->isCompleted()) {
	$w2_h_checkBtnDefaultDisplayAttr = 'display:none;';
}
//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
if ($w2_thisActionIsMarking) {
	$w2_h_checkBtnDefaultDisplayAttr = 'display:none;';
}

$w2_h_modelAnsDefaultDisplayAttr = 'display:none;';
if ($w2_m_config_showModelAnswerIfSubmittedStep && $studentHasSubmittedStep) {	// show the model answer if the step is submitted
	$w2_h_modelAnsDefaultDisplayAttr = '';
}
if ($w2_m_config_showModelAnswerIfCompletedWriting && $objWriting->isCompleted()) {
	$w2_h_modelAnsDefaultDisplayAttr = '';
}
//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
if ($w2_thisActionIsMarking) {
	$w2_h_modelAnsDefaultDisplayAttr = '';
}


//(20120509) special handle for LS (eng / chi) , display the div only (block) if student has submitted the writing
$h_w2_ls_lastStep_div_displayStyle = ($studentHasSubmittedWriting)? 'block':'none' ;

//show model passage after last step submit for Chinese Writing
$h_w2_CHI_ls_model_passage_displayStyle = ($studentHasSubmittedWriting||$isViewOnly) ? 'block':'none' ;
################ Answer Display [End] ################


############### Infobox Display [Start] ###############
$w2_h_infoboxPropertyArr = array();
$numOfInfobox = count((array)$w2_m_infoboxCode);
for ($i=0; $i<$numOfInfobox; $i++) {
	$_infoboxCode = $w2_m_infoboxCode[$i];
	$_infoboxStatus = $w2_s_thisStepAns[$_infoboxCode];
	
	$w2_h_infoboxPropertyArr[$_infoboxCode]['saveStatus'] = $_infoboxStatus;
	//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"] || $w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
		$w2_h_infoboxPropertyArr[$_infoboxCode]['doneSpanStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['notDoneSpanStyle'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = '';
	}
	else if ($_infoboxStatus == $w2_cfg["DB_W2_STEP_HANDIN_ANSWER"]["infoboxHasSubmitted"]) {
		$w2_h_infoboxPropertyArr[$_infoboxCode]['doneSpanStyle'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['notDoneSpanStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = '';
	}
	else {
		$w2_h_infoboxPropertyArr[$_infoboxCode]['doneSpanStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['notDoneSpanStyle'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivDisplay'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = 'display:none;';
	}
	
	//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"] || $objWriting->isCompleted()) {
	if ($w2_thisActionIsMarking || $objWriting->isCompleted()) {
		// always hide the submit button and show model answer if the writing has completed or teacher doing marking
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivDisplay'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = '';
	}
}

################ Infobox Display [End] ################


############### Right-hand Reference Display [Start] ###############
$w2_h_refHtml = array();
$w2_h_refHighlightAry = array();
for ($i=0, $i_max=count($w2_m_infoboxCode); $i<$i_max; $i++) {
	$_infoxboxCode = $w2_m_infoboxCode[$i];
 	$objW2RefManager = new libW2RefManager();
 	
 	//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"] || $w2_thisAction == $w2_cfg["actionArr"]["preview"] || $objWriting->isCompleted()) {
 	if ($w2_thisActionIsMarking || $w2_thisAction == $w2_cfg["actionArr"]["preview"] || $objWriting->isCompleted()) {
 		// cannot add group and item for marking, preview or completed writing
 		$objW2RefManager->setIsEditable(false);
 	}
 	else {
 		$objW2RefManager->setIsEditable(true);
 	}
 	
 	$objW2RefManager->setInfoboxCode($_infoxboxCode);
 	$objW2RefManager->setWritingStudentId($writingStudentId);
 	$objW2RefManager->setPresetCategoryAry($c_model_cfg['refPresetAry']);
 	$objW2RefManager->setStepAnsAry($w2_s_stepAnsAry);
 	
 	$objW2RefManager->setDisplayMode($w2_m_refDisplayMode);
 	$w2_h_refHtml[$_infoxboxCode] = $objW2RefManager->display();
 
}

################ Right-hand Reference Display [End] ################

################ Vocabulary Suggested by student  for science [Start] ###############
$w2_h_suggestedVocabForStudentStyle  = 'display:none;';
$w2_h_suggestedVocabForStudentButtonStyle= 'color:#a9a9a9';
if(!empty($cid)){
	$stepCode_SuggestedVocabForStudent = $ansPrefix.'4';
}else{
	$stepCode_SuggestedVocabForStudent = $contentCode.'_'.$schemeCode.'_c1_ans4';
}

if($studentHasSubmittedStep){
	$w2_h_suggestedVocabForStudentStyle = '';
	$w2_h_suggestedVocabForStudentButtonStyle='display:none;';
}
$h_suggestedVocabForStudent = $w2_libW2->getSuggestedVocabForStudent($stepCode_SuggestedVocabForStudent);
################ Vocabulary Suggested by student  for science [End] ###############


############### GET STUDENT UPLOAD FILE [Start] ###############
if($w2_m_config_getStudentFileInThisStep||$conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){

	//if (teacher preview this content) or (the student see this writing and it is complete) , do not allow to upload or edit again
	$fileUploadAllowAction = ($w2_thisAction == $w2_cfg["actionArr"]["preview"] || $objWriting->isCompleted())? 
								$w2_cfg['uploadFileActionMode']['doNotAllowUpload'] :
									$w2_cfg['uploadFileActionMode']['allowUpload'];

	//if($w2_thisAction ==  $w2_cfg["actionArr"]["marking"]){
	if ($w2_thisActionIsMarking) {
		$fileUploadAllowAction = $w2_cfg['uploadFileActionMode']['doNotAllowUpload'];
	}

	if($w2_thisAction == $w2_cfg["actionArr"]["preview"]){
		//do nothing , don't show the file upload
	}else{
		$h_fileUpload = $w2_libW2->displayViewStudentUploadFile($w2_thisStudentID,$_stepId,$fileUploadAllowAction,$isConceptMap);
	}

}

############### GET STUDENT UPLOAD FILE [End] ###############

############### GET STUDENT WRITING EDITOR [Start] ###############
$h_submitForm = "$('#handinForm')";
if ($stepCode == $w2_objContent->getWritingTaskStepCode()) {
	$writingTaskStepAns = $w2_s_thisStepAns[$w2_m_ansCode[0]];
	if ($writingTaskStepAns == '') {
		$h_writingTaskContent = $w2_m_writingDefaultContent;
	
		for ($i=0, $i_max=count((array)$w2_m_requestAnsCode); $i<$i_max; $i++) {
			$_requiredAnswerCode = $w2_m_requestAnsCode[$i];
			$_requiredAnswer = $w2_libW2->getWritingStepAnsByStepCodeInParsedArray($_requiredAnswerCode, $w2_s_stepAnsAry);
			
			$_replaceTextCode = '<!--answer'.($i+1).'-->';
			$h_writingTaskContent = str_replace($_replaceTextCode, $_requiredAnswer, $h_writingTaskContent);
		}
	}
	else if(!$isHistory){
		if(!$isHandinMarked){	// added on 2015-10-4
			$h_writingTaskContent = $writingTaskStepAns;
		}
	}
	if($isLastStep &&$w2_thisAction==$w2_cfg["actionArr"]["marking"]){
		$h_writingTaskContent = !empty($studentStepHandinRecord['ANSWER_MARKED'])?$studentStepHandinRecord['ANSWER_MARKED']:$h_writingTaskContent;
	}
	$h_writingTaskContent = $w2_libW2->getAnsValue($h_writingTaskContent);

	$w2_h_editorSource = '';		// 'ip' or 'eclass'
	$isViewOnly = false;
	if($isHistory){
		$isViewOnly = true;
	}elseif ($w2_thisAction==$w2_cfg["actionArr"]["marking"]) {
		// essay marking config
		// enable the below line if you want to turn on essay marking function
		$w2_h_editorSource = $w2_cfg["editorSource"]["eclass"];
		// enable the below line if you want to turn off essay marking function
		//$isViewOnly = true;
	}
	else if ($w2_thisAction==$w2_cfg["actionArr"]["peerMarking"]) {
		$isViewOnly = true;
		$h_writingTaskContent = $studentStepHandinRecord['ANSWER'];
	}
	else {
		if ($objWriting->isCompleted()) {
			$isViewOnly = true;
		}
		else {
			//$w2_h_editorSource = $w2_cfg["editorSource"]["ip"];
			if($isHandinMarked == true){			// added on 2015-10-4
				$isViewOnly = true;
			} else {
				$w2_h_editorSource = $w2_cfg["editorSource"]["eclass"]; //use eclass40 fckeditor which support ipad
			}
		}
	}
	
	$h_writingTaskDisplay = '';
	$h_writingEditorOnDocumentReadyJs = '';
	$editorBoxWidth = "100%";
	$editorName = 'r_question[editor]['.$w2_m_ansCode[0].']';

	if ($w2_h_editorSource == $w2_cfg["editorSource"]["ip"]) {
		// Generate from IP
		$editorBoxHeight = 320;
		
		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		$objHtmlEditor = new FCKeditor($editorName, $editorBoxWidth, $editorBoxHeight);
		$objHtmlEditor->Value = $w2_libW2->getEditorViewContent($h_writingTaskContent);
		$h_writingTaskDisplay = $objHtmlEditor->Create2();
		
		$h_writingEditorIndividualAnsHandlingJs = "editorIndividualAnsHandling('".$editorName."')";
	}
	else if ($w2_h_editorSource==$w2_cfg["editorSource"]["eclass"]) {
		// Generate from eClass
		$editorIFrameHeight = 520;
		$editorBoxHeight = $editorIFrameHeight - 40;
		$iFrameSource = '//'.$eclass40_httppath.'src/tool/misc/html_editor/index_w2'.($w2_thisUserType==USERTYPE_STUDENT?'_std':'').'.php?ts='.strtotime(date('Y-m-d H:i:m')).'&isCorrection='.($showCorrectionArea?1:0);
		
		$objHtmlEditor->Value = $h_writingTaskContent;
		$h_writingTaskContent = intranet_htmlspecialchars($h_writingTaskContent);
		$h_writingTaskDisplay = <<<html
<!-- for start iFrame -->
<iframe id="editorIFrame" name="editorIFrame" frameborder="0" cellspacing="0" style="width:{$editorBoxWidth}; height:{$editorIFrameHeight}px; border:0px solid #000000;"></iframe>
<input type="hidden" id="r_stepHandinCode" name="r_stepHandinCode" value="{$w2_m_ansCode[0]}" />
<input type="hidden" id="r_editorBoxHeight" name="r_editorBoxHeight" value="{$editorBoxHeight}" />
<input type="hidden" id="r_editorInitValue" name="r_editorInitValue" value="{$h_writingTaskContent}" />
<!-- for end iFrame -->
html;
	$h_submitForm = "$('#editorIFrame').contents().find('form')";
	$h_writingEditorIndividualAnsHandlingJs = "
			$('#editorIFrame')[0].contentWindow.editorIndividualAnsHandling('".$editorName."');
		";
		$h_writingEditorOnDocumentReadyJs = "editorOnDocumentReadyHandling('".$w2_h_editorSource."', '".$iFrameSource."');";
	}else if ($isViewOnly) {
		if($isHandinMarked == true){		// added on 2015-10-4
			$h_writingTaskDisplay = $h_writingTaskContent;
		} else {
			$h_writingTaskDisplay = $w2_libW2->getEditorViewContent($h_writingTaskContent);
		}
	}	
}
$fullmark = $objWriting->getFullMark();
$lowestmark = $objWriting->getLowestMark();

### New ClipID, ArtID Start

$paddedTemplateID =	str_pad($templateID,  2, "0", STR_PAD_LEFT);
$paddedClipArtID =	str_pad($clipArtID,  2, "0", STR_PAD_LEFT);
//if($stepCode =='c5' && $r_contentCode=='chi'){
//	$h_w2_CHI_ls_model_passage_displayStyle = 'none'	;
//}
if($stepCode =='c3' && $r_contentCode=='eng'){
	
$essayHTML = <<<HTML
	<div id="templateStructure" style="display:none;" >
		<div id="paper_template" class="template_{$paddedTemplateID}">
	     	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
	        	<div class="pt_top_element"></div>
	        </div></div></div>
	     	<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
	        	<div class="paper_main_content">
	            <!---------------->

	            <!----------------->
	            <p class="spacer"></p>
	            </div>
	        </div></div></div>
	
	       	<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">
	        	
	        </div></div></div>
	        <!--------clipart---------->
	        <div id="template_clipart"><span class="clipart_{$paddedClipArtID}"></span></div>
	    </div>
	</div>
		<script>
		var sampleHTML = $('.content').find('.sample').html(); 
		$('#templateStructure').find('.paper_main_content').html(sampleHTML);
		$('.content').find('.sample').html($('#templateStructure').html());
		$('.paper_main_content').after('<p class="spacer"></p>');
		</script>
HTML;
}
else if($stepCode =='c2' && $r_contentCode=='sci') {
$essayHTML .= <<<HTML
	<div id="templateStructure" style="display:none;" >
		<div id="paper_template" class="template_{$paddedTemplateID}">
	     	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
	        	<div class="pt_top_element"></div>
	        </div></div></div>
	     	<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
	        	<div class="paper_main_content">
	            <!---------------->
	
	            
	            
	            <!----------------->
	            <p class="spacer"></p>
	            </div>
	        </div></div></div>
	
	       	<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">
	        	
	        </div></div></div>
	        <!--------clipart---------->
	        <div id="template_clipart"><span class="clipart_{$paddedClipArtID}"></span></div>
	    </div>
	</div>
		<script>
		$('.sample_sci').find('.essay').each(function(){
			var sampleHTML = $(this).html(); 
			$('#templateStructure').find('.paper_main_content').html(sampleHTML);
			$(this).html($('#templateStructure').html());
		});
		</script>
HTML;
	
}
else if($stepCode =='c4' && $r_contentCode=='sci'){
$essayHTML .= <<<HTML
	<div id="templateStructure" style="display:none;" >
		<div class="sample_new">
                 
                <!---- paper start------------>
                <div id="paper_template" class="template_{$paddedTemplateID}">                                
                     	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
                        	<div class="pt_top_element"></div>
                        </div></div></div>
                     	<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
                        	<div class="paper_main_content">
                            <!---------------->
                            <div id="sci_div"></div>
                            <!----------------->
                            <p class="spacer"></p>
                            </div>
                        </div></div></div>

                       	<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">
                        	
                        </div></div></div>
                        <!--------clipart---------->
                        <div id="template_clipart"><span class="clipart_{$paddedClipArtID}"></span></div>
                 </div>	
                
                <!----------paper end------------->
          </div>
	</div>
		<script>
		$('.content').find('.sample').each(function(){
			var sampleHTML = $(this).html(); 
			$('#templateStructure').find('#sci_div').html(sampleHTML);
//			$('#templateStructure').show();
			$(this).parent().html($('#templateStructure').html());
			$('.tag').addClass('bubble');
			$('.tag').removeClass('tag');
			$('.key').addClass('remarks');
			$('#key_right').attr('id','keynote');
		});
		</script>
		<style>
		</style>
HTML;
		
}
###  New ClipID, ArtID End 

if($showCorrectionArea){ ### REDO ###
	###Siuwan 20131211 Get Student Handin Record for correction
	###Jason 20151004 fix the problem of showing nothing on the left side of the correction area if teacher did not mark the submission but call for Redo
	$draftSelection = $w2_libW2->getStudentStepHandinHistorySelection($studentStepHandinRecord['VERSION'],$handinVersion);
	$displayHandinCommentHTML = $w2_libW2->getDisplayMarkingLayerHTML($stepId,$w2_thisStudentID,$writingId,$w2_m_ansCode[0],$draftVersion);
	$html = '<div class="correction_board">';
		$html .= '<div class="draft_previous">';
			$html .= $draftSelection;
            $html .= '<div class="write">';
            	$html .= '<div class="mark_comment">';
                	$html .= '<div id="display_mark_comment" class="show_sticker">';	
                    	$html .= $displayHandinCommentHTML;
                    $html .= '</div>';
                $html .= '</div>';   
                $html .= '<div id="paper_template" class="template_'.str_pad($templateID,  2, "0", STR_PAD_LEFT).'">';// Add By Adam                                
                	$html .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
                    	$html .= '<div class="pt_top_element"></div>';
                    $html .= '</div></div></div>';
                    $html .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
                    	$html .= '<div class="paper_main_content">';
                        	$html .= '<div class="essay">'.(($answerMarked != '') ? $answerMarked : $answerOfPreviousVersion).'</div>';
                        	$html .= '<p class="spacer"></p>';
                        $html .= '</div>';
                   $html .= '</div></div></div>';
					$html .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
                    $html .= '</div></div></div>';
                    $html .= '<div id="template_clipart"><span class="clipart_'.str_pad($clipArtID,  2, "0", STR_PAD_LEFT).'"></span></div>';// Add By Adam
                $html .= '</div>';	
           $html .= '</div>';
           $html .= '<p class="spacer"></p>';
		$html .= '</div>';
        $html .= '<div class="write_correction">';
        $draftName = $intranet_session_language=='en'?$Lang['W2']['revisedDraft']:str_replace('<-version->',$handinVersion,$Lang['W2']['revisedDraft']);
        	$html .= '<h1 class="correction_title">'.$draftName.'</h1>';
            $html .= '<div class="write">';
            	$html .= '<div id="paper_template" class="template_'.str_pad($templateID,  2, "0", STR_PAD_LEFT).'">';                                
                	$html .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
                    	$html .= '<div class="pt_top_element"></div>';
                    $html .= '</div></div></div>';
                    $html .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
                    	$html .= '<div class="paper_main_content">';
                        	$html .= '<div class="essay">';
                            	$html .= $h_writingTaskDisplay;
                            $html .= '</div>';
                            $html .= '<p class="spacer"></p>';
                        $html .= '</div>';
                    $html .= '</div></div></div>';
					$html .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
                    $html .= '</div></div></div>';
                    $html .= '<div id="template_clipart"><span class="clipart_'.str_pad($clipArtID,  2, "0", STR_PAD_LEFT).'"></span></div>';
                    $html .= '</div>';	
				$html .= '</div>';
                $html .= '<p class="spacer"></p>';
            $html .= '</div>';
            $html .= '<div class="ref_board">';
            	$html .= '<a id="ref_link" href="javascript:void(0);">'.$Lang['W2']['reference'].' </a>';
           		$html .= '<div class="ref" id="ref_layer"></div>';
			$html .= '</div>';
			$html .= '<p class="spacer"></p>';
		$html .= '</div>';
		$html .= '<br style="clear:both" /><br />';
$h_writingTaskDisplay = $html;
}else{
	if($studentStepHandinRecord['VERSION'] > 1){		// added on 2015-10-4
		# if number of version is more than 1, show the submission pull down 
		$draftSelection = $w2_libW2->getStudentStepHandinHistorySelection($studentStepHandinRecord['VERSION'],$handinVersion);
	}
	
	$objComment = new libStepStudentComment();
	$commentBankSelection = $w2_thisAction==$w2_cfg["actionArr"]["peerMarking"]&&$r_contentCode=='eng'?$objComment->getCommentBankSelection():'';
	$displayHandinCommentHTML = '';
	$displayHandinCommentHTML .= $draftSelection;		// added on 2015-10-4
	if($showMarkingSummary){
		// modified on 2015-10-4
		$displayMarkingLayerHTML = $w2_libW2->getDisplayMarkingLayerHTML($stepId,$w2_thisStudentID,$writingId,$w2_m_ansCode[0],$handinVersion,$showOverallMark);
		$displayHandinCommentHTML .= '
	    	<div class="mark_comment">
	           	<div id="display_mark_comment" class="show_sticker">'.$displayMarkingLayerHTML.'</div>
	        </div>
		';
	}
	$displayHandinCommentHTML .= '<div id="paper_template" class="template_'.str_pad($templateID,  2, "0", STR_PAD_LEFT).'">';   
	$displayHandinCommentHTML .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
	$displayHandinCommentHTML .= '<div class="pt_top_element"></div>';
	$displayHandinCommentHTML .= '</div></div></div>';
	$displayHandinCommentHTML .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
	$displayHandinCommentHTML .= '<div class="paper_main_content">';
	$displayHandinCommentHTML .= '<div class="essay">'.$h_writingTaskDisplay.'</div>';
	$displayHandinCommentHTML .= '<p class="spacer"></p>';
	$displayHandinCommentHTML .= '</div>';
	$displayHandinCommentHTML .= '</div></div></div>';
	$displayHandinCommentHTML .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
	$displayHandinCommentHTML .= '</div></div></div>';
	$displayHandinCommentHTML .= '<div id="template_clipart"><span class="clipart_'.str_pad($clipArtID,  2, "0", STR_PAD_LEFT).'"></span></div>';
	$displayHandinCommentHTML .= '</div>';	

	$h_writingTaskDisplay = $displayHandinCommentHTML;
	if($showMarkingBoard){
		$stickerListHTML = '';
		$marker_sticker_ary = explode(":",$marker_sticker);
		for($i = 0,$i_max = count($w2_cfg["contentWriting"]["MarkingSticker"]);$i < $i_max ;$i++){
			$_id = $w2_cfg["contentWriting"]["MarkingSticker"][$i];
			$_class = in_array($_id,$marker_sticker_ary)?' class="selected"':'';
			$stickerListHTML .= '<li id="'.$_id.'" '.$_class.'>';
		   	  $stickerListHTML .= '<a href="javascript:void(0);" class="template_thumb marking_sticker"><span></span></a>';
		    $stickerListHTML .= '</li>';
		}
		
		$suffix = ($intranet_session_language == 'b5')?'_b5':'';
		$marking_css = ($w2_thisAction == $w2_cfg["actionArr"]["marking"])?'teacher':'';
		$mark_out_of_range_warning = $Lang['W2']['scoreOutOfRange'].": (".$lowestmark." - ".$fullmark.")";
		$h_writingTaskDisplay .= '
			<div class="marking_board">
		     	<div class="marking_board_top"><div class="marking_board_top_right"><div class="marking_board_top_bg"></div></div></div>
		     	<div class="marking_board_content"><div class="marking_board_content_right'.$suffix.' '.$marking_css.'"><div class="marking_board_content_bg">
		                <!--mark-->
		                 <div class="tool_mark">
		                    <fieldset>
		                      <legend>'.$Lang['W2']['marks'].'</legend>
		                      <input type="text" name="r_mark" id="r_mark" value="'.$marker_score.'">/'.$fullmark.'</fieldset>
								<br style="clear:both;">
								<span id="markWarningSpan" style="color:red;display:none;">'.$mark_out_of_range_warning.'</span>
		                    </div>
		        		<!-- comment-->
		                <div class="tool_comment">
		                	<a href="javascript:void(0);" class="btn_comment" id="comment_link"> '.$Lang['W2']['addComment'].'</a>
		                	<div id="write_comment">
		                    <h1>'.$Lang['W2']['yourComment'].'</h1>
		                    <textarea rows="3" wrap="virtual" class="write_comment_area" id="r_comment" name="r_comment[0]">'.$marker_comment.'</textarea>
							<br style="clear:both;">
							'.$commentBankSelection.'
		                    </div>
		                </div>
		                <!--sticker-->
		                <div class="tool_sticker">
		                	<a href="javascript:void(0);" class="btn_sticker" id="sticker_link"> '.$Lang['W2']['giveSticker'].'</a>
		                    <div id="select_sticker">
		                    <h1>'.$Lang['W2']['selectSticker'].'</h1>
		                    <div class="control_board">
		                    	<ul class="sticker_list">
		                    		'.$stickerListHTML.'
		               				<p class="spacer"></p>
		              
					        	</ul>  
					        </div>
		                    
		                </div>
		            </div>
		            <!---->
		            <div class="edit_bottom">
		                <input type="button" class="formbutton" value="'.$Lang['Btn']['Submit'].'" onclick="saveStepAns(0);saveHandinMarking(\''.$successMsg.'\',\''.$failMsg.'\');"/>
						<input type="button" class="formsubbutton" value="'.$Lang['Btn']['Reset'].'" onclick="resetMarkingArea();"/>
		            </div>
		    
		    </div></div></div>
		 	<div class="marking_board_bottom"><div class="marking_board_bottom_right"><div class="marking_board_bottom_bg"></div></div></div>                                                                
		 </div> 
		';
	}
}
################ GET STUDENT WRITING EDITOR [End] ################


############### Buttons [Start] ###############
$saveOnlyFlag = 0;
$goToNextStepFlag = 1;

if($thisStepRequestApproval){
	//if this step set require approval
	if($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]){
		// This student has not been approved to go to next step, not allow to next step  , set $goToNextStepFlag = 2;
		//allow submit but do not allow go next step
		$goToNextStepFlag = 2;
	}
}

//error_log("goToNextStepFlag [".$goToNextStepFlag."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
$showNextButton = false;
if($nextStepID > 0){
	$btnDisabled = false;  // default enable the next button 
	
	### Disable the next button if not all the questions are answered (if required)
	if ($w2_m_config_disabledNextBtnIfNotClickedAllModelAnswer) {		// $w2_m_config_disabledNextBtnIfNotClickedAllModelAnswer is defined in model.php of each steps
		$btnDisabled = false;
	}
	
	if($w2_m_config_requireInputAllAnswer){
		$allQuestionAnswered = true;
		for ($i=0, $i_max=count($w2_m_ansCode); $i<$i_max; $i++) {
			$_ansCode = $w2_m_ansCode[$i];
			//if (isset($w2_s_thisStepAns[$_ansCode]) && $w2_s_thisStepAns[$_ansCode] != '') {
			if (isset($w2_s_thisStepAns[$_ansCode])) {		// answer can be blank
				// answered => do nth
			}
			else {
				$allQuestionAnswered = false;
			}
		}
		$btnDisabled = ($allQuestionAnswered)? false : true;
	}
	
	$showNextButton = true;
}

if ($objWriting->isCompleted()) {
	$showNextButton = false;
}

if ($showNextButton) {
	if($isConceptMap&&$conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){
		$h_nextStepButton = $w2_libW2->htmlSaveStudentHandInWithAttachmentButton($goToNextStepFlag, $Lang['Btn']['Next'], $btnDisabled);
	}else{
		$h_nextStepButton = $w2_libW2->htmlSaveStudentHandInButton($goToNextStepFlag, $Lang['Btn']['Next'], $btnDisabled);
	}

}

$h_saveAsDraftButton = $w2_libW2->htmlSaveStudentHandInButton($saveOnlyFlag,'','',$updateStepStatus = $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["draft"]);
$h_saveAndNextButton = $w2_libW2->htmlSaveStudentHandInButton($goToNextStepFlag);
$h_submitHandInButton = $w2_libW2->htmlSubmitStudentHandInButton();
//$h_markHandInButton = $w2_libW2->htmlSubmitStudentHandInButton();


$studentHandInAllowAction = $w2_cfg['studentHandInAllowAction']['edit']; // default is edit mode

if($_SESSION['UserID'] == $w2_thisStudentID && !$isViewOnly){
	//do noting
}else{
	//$_SESSION['UserID'] != $w2_thisStudentID 
	//this case may be teacher doing marking or student peer marking , reset all the button display
	$h_saveAndNextButton = '';
	$h_submitHandInButton = '';
	$h_saveAsDraftButton = '';
	$h_nextStepButton = '';
	$studentHandInAllowAction = $w2_cfg['studentHandInAllowAction']['view'];  // view the handin only (cannot edit)
}

if ($objWriting->isCompleted()) {
	$h_saveAsDraftButton = '';
	$h_saveAndNextButton = '';
	$h_submitHandInButton = '';
	$studentHandInAllowAction = $w2_cfg['studentHandInAllowAction']['view']; // view the handin only (cannot edit)
}



if($_stepStatus == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]){
	$h_saveAsDraftButton = '';
}

### Cancel Button
if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	$h_cancelButton = '';
}
else {
	$h_cancelButton = $w2_libW2->htmlCancelStudentHandInButton($contentCode, $w2_thisAction, $writingId, $objWriting->isCompleted(), $r_handinSubmitStatus, $r_fromToBeMarked);
}


################ Buttons [End] ################
############### GET STUDENT CONCEPT MAP[Start] ###############

if(is_array($c_model_cfg['withConceptMapStep']) 
	&& count($c_model_cfg['withConceptMapStep']) >0 
		&& in_array(strtolower($r_stepCode), $c_model_cfg['withConceptMapStep']) ) {

if($c_model_cfg['withConceptMapStep_ansCode'] == ''){
	//for safe programming , remind programmer only
	echo 'Concept Map Setting Error!! Please set an answer code to the this scheme model file<br/>';
}
		$conceptMapWithDefault = $objWriting->getWithDefaultConceptMap();
		$conceptMapStepCode = $w2_objContent->getConceptMapStepCode();


		$conceptMapLastUpdateTime = $w2_libW2->findStepAnsLastModified($w2_thisStudentID,$stepId,$c_model_cfg['withConceptMapStep_ansCode']);
		$h_conceptMapLastUpdateTime = trim($conceptMapLastUpdateTime) == ''? $w2_cfg['emptyDataStyle1'] : $conceptMapLastUpdateTime;


		$conceptMapEclassTaskId = $w2_libW2->findEclassTaskId($writingId ,$conceptMapStepCode);
		$h_conceptMapEclassTaskId = $conceptMapEclassTaskId;
		$conceptMapDisplayMode = ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) ? 
									$w2_cfg['conceptMapDisplayMode']['teacherPreivewImage']: 
										$w2_cfg['conceptMapDisplayMode']['studentAns'];
	
		$h_conceptMapLink = $w2_libW2->findPowerConceptLinkForStudent($w2_thisStudentID,$objWriting,$w2_classRoomID,$c_model_cfg['withConceptMapStep_ansCode'],$studentHandInAllowAction,$conceptMapWithDefault,$conceptMapDisplayMode,$conceptType);
		$h_conceptType = $conceptType;
//error_log("\n\n h_conceptMapLink-->".print_r($h_conceptMapLink,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
		
}
############### GET STUDENT CONCEPT MAP [End] ###############

$h_content = '';
ob_start();
include_once($contentTemplate);
$h_content .= "<!--w2 start-->\n";
$h_content .= '<!-- layout : '.$contentCode.'/'.$schemeCode.'/'.$stepCode. '-->';
$h_content .= ob_get_contents();
$h_content .= $essayHTML;
$h_content .= $teacherAttachmentBtnHTML;
$h_content .= "<!--w2 end-->\n";
ob_end_clean();

$objHandinLayout->setContentHtml($h_content);




$linterface->LAYOUT_START();
include_once('templates/handin/viewHandin.tmpl.php');
$linterface->LAYOUT_STOP();
?>


<?

function htmlGoToStep($schemeCode,$stepCode,$stepID,$writingid,$content){
	$strJS = 'goToStep(\''.$schemeCode.'\',\''.$stepCode.'\',\''.$stepID.'\',\''.$writingid.'\',\''.$content.'\')';
	return $strJS; 
}


/**
* Return all step answer for a give writing id 
*
* @param : INT $writingid, Writing ID
* @param : INT $studentID, Ans for the Student ID 
* @return : String HTML for the walking step
*/
function getWritingWholeStepAns($writingid,$studentID){
	global $intranet_db;
	$sql= 'select	step.CODE as "STEP_CODE",
					step.SEQUENCE as "STEP_SEQUENCE",
					h.ANSWER as "ANSWER",
					step.STEP_ID as "STEP_ID",
					h.STEP_HANDIN_CODE as "STEP_HANDIN_CODE"
			from 
					'.$intranet_db.'.W2_WRITING as w 
					inner join '.$intranet_db.'.W2_STEP as step on step.WRITING_ID = w.WRITING_ID
					inner join '.$intranet_db.'.W2_STEP_HANDIN as h on h.STEP_ID = step.STEP_ID
			where 
					h.USER_ID = '.$studentID.' and w.WRITING_ID = '.$writingid;

	$objDB  = new libDB();

	$result = $objDB->returnResultSet($sql);

	return $result;
}

/**
* GROUP THE STUDENT STEP HANDIN BY STEPID , THEN STEPHANDINCODE. SUCH THAT ANS CAN RETRIEVE BY $result['12']['HandeInCodeA'];  12 --> STEP ID , HANDEINCODEA --> HAND IN CODE
*
* @param : array $allStepAns , all student handin for a given writing ID
* @return : Array $result , Grouped Array 
*/
function praseAllStepAns($allStepAns){


	$result = array();

	for($i = 0, $i_max = count($allStepAns);$i< $i_max; $i++){
		$_stepID = $allStepAns[$i]['STEP_ID'];
		$_StepHandinCode = $allStepAns[$i]['STEP_HANDIN_CODE'];
		$_StepHandinAns = $allStepAns[$i]['ANSWER'];
		$result[$_stepID][$_StepHandinCode] = $_StepHandinAns;
	}

	return $result;
}


// moved to libw2.php
///**
//* Return STEP ANS by a giving step code that $stepCodeAnsArray is generate in f:getWritingStepAnsByStepCode
//*
//* @param : String $stepHandinCode
//* @param : Array  $stepCodeAnsArray , a set for student 
//* @return : Student answer for the giving writing ID and step handin code and student id
//*/
//function getWritingStepAnsByStepCodeInParsedArray($stepHandinCode,$stepCodeAnsArray){
//	$returnAnswer = '';
//	if(is_array($stepCodeAnsArray) && count($stepCodeAnsArray)){
//		for($i = 0,$i_max = count($stepCodeAnsArray);$i<$i_max;$i++){
//			$_details = $stepCodeAnsArray[$i];
//			$_stepHandinCode = $_details['StepHandinCode'];
//
//			if($_stepHandinCode == $stepHandinCode){
//				$returnAnswer = $_details['ANSWER'];
//				break;
//			}
//		}
//	}
//	return $returnAnswer;
//}



?>