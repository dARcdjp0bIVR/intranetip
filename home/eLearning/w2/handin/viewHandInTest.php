<?
// using: Adam


include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingXml.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");
include_once($PATH_WRT_ROOT."includes/w2/libHandinLayout.php");
include_once($PATH_WRT_ROOT."includes/w2/libStepStudentComment.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2RefManager.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2PeerMarkingData.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");

$writingId = $r_writingId;
$stepCode = trim($r_stepCode);
$contentCode = trim($r_contentCode);
$schemeCode = trim($r_schemeCode);
$r_handinSubmitStatus = trim($r_handinSubmitStatus);
$r_fromToBeMarked = trim($r_fromToBeMarked);
$showMarkingSummary = false;
$showMarkingBoard = false;
$showCorrectionArea = false;
$showOverallMark = false;
$isHistory = false;

if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	$writingObjSource = $w2_cfg["writingObjectSource"]["xml"];
}
else {
	$writingObjSource = $w2_cfg["writingObjectSource"]["db"];
}
$objWriting = libWritingFactory::createWriting($writingObjSource, $writingId, $contentCode, $schemeCode);
$w2_objContent = libW2ContentFactory::createContent($r_contentCode);
$objHandinLayout = new libHandinLayout();
$objHandinLayout->setContentCode($contentCode);

### Get the steps of the Writing
$stepArr = $objWriting->findStep();

### calculate contentCode , schemeCode , stepCode in case r_stepCode is empty
if($stepCode ==''){
	$contentCode = $objWriting->getContentCode();
	$schemeCode = $objWriting->getSchemeCode();
	$stepCode = $stepArr[0]['STEP_CODE']; //DEFAULT TAKE THE STEP CODE FROM THE FIRST STEP => $steps[0]
}

### Get TemplateID, ClipArtID


$templateID =  $objWriting->getTemplateID();
$clipArtID = $objWriting->getClipArtID();

$templateID = ($templateID)?$templateID:1;
$clipArtID = ($clipArtID)?$clipArtID:1;

$stepInfoAry = $w2_libW2->getStepInfoByContent($contentCode);
$lastStepCode = $stepInfoAry[count($stepInfoAry)-1]['code'];
$isLastStep = false;
if ($stepCode == $lastStepCode) {
	$isLastStep = true;
}

/*
ifsharing(){
	$w2_thisStudentID  = $this_sharingStudentID;
}
*/


### Get the info of the writings of the student
$writingInfoAry = $w2_libW2->findStudentWriting($w2_thisStudentID, $contentCode, $writingId);
$writingStudentId = $writingInfoAry[0]['WRITING_STUDENT_ID'];       // store W2_WRITING_STUDENT.WRITING_STUDENT_ID
$writingSumbitStatus = $writingInfoAry[0]['HANDIN_SUBMIT_STATUS'];  // store submit status for the writing
$studentHasSubmittedWriting = ($writingSumbitStatus == $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"])? true : false;


$contentDetailsAry = $w2_libW2->getModelFile($contentCode,$schemeCode,$stepCode);


$schemeModel = $contentDetailsAry['schemePath'].'/model.php';
$contentModel = $contentDetailsAry['model'];

if($w2_thisAction == $w2_cfg["actionArr"]["sharing"]){
	//sharing view template
	$contentTemplate = $PATH_WRT_ROOT.'home/eLearning/w2/templates/writing/view.php';
}else{
	//individual writing view template
	if((!$isTeaching||!empty($w2_thisAction))&&$stepCode=='c2'&&(strpos($schemeCode,'_')!==false)){
		
		list($schoolCode,$schemeNum) = explode('_',$schemeCode);
		$schemeNum = str_replace('scheme','',$schemeNum);
		$engContentParser = new libW2EngContentParser();
		$parseData = $engContentParser->parseEngContent('',$schoolCode,$schemeNum);
		if(count($parseData)>0){ //is content input
			$isContentInputStep2 = true;
			$cid = $parseData['cid'];
			$step2Instruction = $parseData["step2Data"]["step2Int"];
			$conceptType = $parseData["conceptType"];
			switch($conceptType){
				case $w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]:
					$conceptMapId = $parseData["powerConceptID"];
					break;
				case $w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]:
					$conceptMapId = $parseData["powerBoardID"];
					break;
				default:$conceptMapId='';
			}
			
			$contentTemplate = $PATH_WRT_ROOT.'home/eLearning/w2/templates/writing/conceptMap.php';	
		}else{
			$contentTemplate = $contentDetailsAry['view'];
		}
	}else{
		$contentTemplate = $contentDetailsAry['view'];
	}	
}

include_once($schemeModel); // for the whole scheme
include_once($contentModel); // for individual content 

$stepId = 0;
$thisStepRequestApproval = $w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["NO"]; // default no need to approval
for($i = 0,$i_max = count($stepArr);$i< $i_max;$i++){
	$_stepCode = $stepArr[$i]['STEP_CODE'];
	$_stepSequence = $stepArr[$i]['STEP_CODE'];
	$_stepId = $stepArr[$i]['STEP_ID'];
	$_stepRequestApproval = $stepArr[$i]['STEP_REQUIRE_APPROVAL'];
	if($_stepCode == $stepCode){
		$stepId = $_stepId;
		$thisStepRequestApproval = $_stepRequestApproval;
		break;
	}
}

### Navigation
$objHandinLayout->setNavigationHtml($w2_libW2->getNavigationHtml());

### Scheme
//$schemeName = $w2_libW2->getSchemeNameBySchemeCode($schemeCode,$contentCode);
$w2_objContent->setSchemeCode($schemeCode);
if($w2_thisAction==$w2_cfg["actionArr"]["sharing"]){
	$objHandinLayout->setUnitTitle($w2_objContent->getSchemeTitle());
	$objHandinLayout->setUnitName($w2_objContent->getSchemeName());
	
	/*#Student Name*/
	$objHandinLayout->setSchemeTitle($Lang['W2']['studentName']);
	$shareWritingAuthorStudentInfo = $w2_libW2->getWritingStudentInfoAry($r_contentCode, $r_writingId);
	$shareWritingAuthorStudentName = $intranet_hardcode_lang=='en'?$shareWritingAuthorStudentInfo[0]['USER_ENGLISH_NAME']:$shareWritingAuthorStudentInfo[0]['USER_CHINESE_NAME'];
	$shareWritingAuthorStudentClassName = $shareWritingAuthorStudentInfo[0]['CLASS_NAME'];
	$shareWritingAuthorStudentClassNumber = $shareWritingAuthorStudentInfo[0]['CLASS_NUMBER'];
	$objHandinLayout->setSchemeName("$shareWritingAuthorStudentName ($shareWritingAuthorStudentClassName - $shareWritingAuthorStudentClassNumber)");
	
}else{
	$objHandinLayout->setUnitTitle($w2_objContent->getUnitTitle());
	$objHandinLayout->setUnitName($w2_objContent->getUnitName());
	$objHandinLayout->setSchemeTitle($w2_objContent->getSchemeTitle());
	$objHandinLayout->setSchemeName($w2_objContent->getSchemeName());
}


### Toolbar
// Get Step related Info for this student


$studentStepStatusAry = $w2_libW2->findStudentWritingStepStatus($w2_thisStudentID, $writingId);

$studentStepStatusAssoAry = BuildMultiKeyAssoc($studentStepStatusAry, 'STEP_ID');
$stepStudentStatus = $studentStepStatusAssoAry[$stepId]['STEP_STATUS'];
$stepStudentApprovalStatus = $studentStepStatusAssoAry[$stepId]['STEP_APPROVAL_STATUS'];

$studentHasSubmittedStep = ($stepStudentStatus == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"])? true : false;

$studentStepHandinRecord = $w2_libW2->getStudentStepHandinRecord($stepId,$w2_thisStudentID,$w2_m_ansCode[0]);
$stepStudentCommentId = $studentStepStatusAssoAry[$stepId]['COMMENT_ID'];
$handinVersion = $r_version?$r_version:$studentStepHandinRecord['VERSION'];

###Get Teacher Comment
$teacherCommentAry = current($w2_libW2->getTeacherComment($stepId,$w2_thisStudentID,$w2_thisUserID,$handinVersion));
$teacherSticker = $teacherCommentAry['STICKER'];
$stepStudentCommentId = $teacherCommentAry['COMMENT_ID'];


### Get Peer Marking Data
// for peer marking mode, $w2_thisPeerMarkingMarkerUserId is not null and therefore only return the peer comment of the specific student
// for viewing student's own writing, $w2_thisPeerMarkingMarkerUserId is null and therefore return all peer comment for all other students
$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($w2_thisPeerMarkingMarkerUserId, $writingId, $markedOnly=true, $w2_thisStudentID);

$numOfPeerMarkingData = count($peerMarkingDataInfoAry);
$hasPeerCommented = false;
if ($numOfPeerMarkingData > 0) {
	$hasPeerCommented = true;
}
if($thisStepRequestApproval){
	// $stepStudentApprovalStatus default value in DB is "0" , so it to $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"] 
	// as a default if "$stepStudentApprovalStatus" is empty
	$stepStudentApprovalStatus = ($stepStudentApprovalStatus == 0) ? $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]: $stepStudentApprovalStatus;
}
//get the student submit status , to handle display of save as draft button
$sql = 'select STEP_ID , USER_ID, STEP_STATUS, COMMENT_ID from W2_STEP_STUDENT where STEP_ID = '.$stepId.' and USER_ID = '.$w2_thisStudentID;
$_objDb = new libdb();
$result = current($_objDb->returnResultSet($sql));  // with DB unique key with (`STEP_ID`,`USER_ID`), suppose return only one row
$_stepStatus = $result['STEP_STATUS'];
//$studentInTheStepDetails = getStudentWritingStepStatus();
switch ($w2_thisAction) {
	case $w2_cfg["actionArr"]["marking"]:
		$successMsg = $Lang['W2']['returnMsgArr']['teacherCommentUpdateSuccess'];
		$failMsg = $Lang['W2']['returnMsgArr']['teacherCommentUpdateFailed'];
		###Siuwan 20131211 Get Teacher Mark
		$marker_score = $studentStepHandinRecord['MARK'];
		$marker_comment = $teacherCommentAry['CONTENT'];
		$marker_sticker = $teacherSticker;
		$showMarkingSummary = true;
		$showMarkingBoard = true;
		$showOverallMark = true;
		
		###Siuwan 20131211 Always display redo button
		if($stepStudentStatus==$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]){
			$redoBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"], $stepStudentApprovalStatus, "");
		}else{
			$redoBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]."', '".$w2_thisStudentID."');");
		}
	
		if(!$isLastStep||$objWriting->isCompleted()){
			$redoBtn = '';
		}
		if ($w2_m_config_hideApprovalBtn) {
			// do nth
		}
		else {
			$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $stepStudentStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]."', '".$w2_thisStudentID."');");
			$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]."', '".$w2_thisStudentID."');");
			//$redoBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"], $stepStudentStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"]."', '".$w2_thisStudentID."');");
		}

		if($thisStepRequestApproval){
//			if($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]){
				$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]."', '".$w2_thisStudentID."');");
				$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]."', '".$w2_thisStudentID."');");
/*
			}else{
				$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]."', '".$w2_thisStudentID."');");
				$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentApprovalStatus, "updateStepApprovalStatus('".$w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]."', '".$w2_thisStudentID."');");
			}
*/
		}


		if ($waitingForApprovalBtn != '') {
			$objHandinLayout->addActionButton($waitingForApprovalBtn);
		}
		if ($approveBtn != '') {
			$objHandinLayout->addActionButton($approveBtn);
		}
		if ($redoBtn != '') {
			$objHandinLayout->addActionButton($redoBtn);
		}		
		
		$onchange = "goToStep('', '', '', '', '', this.value);";
		
		$includeStudentIdAry = null;
		if ($r_fromToBeMarked) {
			$toBeMarkedStudentInfoAry = $w2_libW2->getWritingToMarkStudent($writingId);
			$toBeMarkedStudentIdAry = Get_Array_By_Key($toBeMarkedStudentInfoAry[$writingId], 'USER_ID');
		}
		
		$objHandinLayout->setStudentSelectionHtml($w2_libW2->getHandinStudentSelectionHtml($contentCode, $writingId, $stepId, $w2_thisStudentID, $onchange, $r_handinSubmitStatus, $toBeMarkedStudentIdAry));
		break;
	case $w2_cfg["actionArr"]["sharing"]:
		/*
		$onchange = "goToStep('', '', '', '', '', this.value);";
		
		$includeStudentIdAry = null;
		if ($r_fromToBeMarked) {
			$toBeMarkedStudentInfoAry = $w2_libW2->getWritingToMarkStudent($writingId);
			$toBeMarkedStudentIdAry = Get_Array_By_Key($toBeMarkedStudentInfoAry[$writingId], 'USER_ID');
		}
		*/
		break;
	case $w2_cfg["actionArr"]["peerMarking"]:
			$successMsg = $Lang['W2']['returnMsgArr']['peerCommentUpdateSuccess'];
			$failMsg = $Lang['W2']['returnMsgArr']['peerCommentUpdateFailed'];
			###Siuwan 20131211 Get Peer Mark
			$peerMarkingDataAry = current($w2_libW2->getPeerComment($writingId,$w2_thisStudentID,$w2_thisUserID,$handinVersion,true));
	
			$marker_score = $peerMarkingDataAry['MARK'];
			$marker_comment = $peerMarkingDataAry['COMMENT'];
			$marker_sticker = $peerMarkingDataAry['STICKER'];	
			$showMarkingBoard = true;		
			$showOverallMark = true;
			$showMarkingSummary = true; 
			
		break;
			
	default:
		$w2_thisAction = ($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["redo"])?$w2_cfg["actionArr"]["redo"]:$w2_thisAction;
		if($objWriting->isCompleted()){
			$showOverallMark = true;
			$showMarkingSummary = true; 
			
		} else {
			if($thisStepRequestApproval){
				if($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"]){
					$approveBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["approved"], $stepStudentStatus,$onClickAction='');
				//}elseif($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"] || trim($stepStudentApprovalStatus) == '0'){
				}elseif(($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]|| trim($stepStudentApprovalStatus) == '0')&&$studentHasSubmittedStep){
					$waitingForApprovalBtn = $w2_libW2->getUpdateApprovalStatusBtn($w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"], $studentApprovalStatue = $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"],$onClickAction='');
				}else{
					//don't display the button , do nothing
				}
			}
			if ($waitingForApprovalBtn != '') {
				$objHandinLayout->addActionButton($waitingForApprovalBtn);
			}
			if ($approveBtn != '') {
				$objHandinLayout->addActionButton($approveBtn);
			}
			if ($studentHasSubmittedWriting) {
				$showMarkingSummary = true; 
			}
			
			$stepHandinHistory = $w2_libW2->getStepHandinHisotryRecord($stepId,$w2_thisStudentID,$w2_m_ansCode[0],$handinVersion);
			$isHistory = count($stepHandinHistory)>0;
			if(!empty($handinVersion)&&$_stepStatus==$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]||$w2_thisAction==$w2_cfg["actionArr"]["redo"]){
				$showOverallMark = true;
				$showMarkingSummary = true; 
				$showCorrectionArea = true;
				$answerMarked = $studentStepHandinRecord['ANSWER_MARKED'];
				$draftVersion = $handinVersion;
			}
			if($isHistory){
				$answerMarked = $stepHandinHistory[0]['ANSWER_MARKED'];	
				$h_writingTaskContent = $stepHandinHistory[0]['ANSWER'];
				$draftVersion = $handinVersion;
			}elseif($w2_thisAction==$w2_cfg["actionArr"]["redo"]||$handinVersion>1){ //Redo
				if(empty($studentStepHandinRecord['ANSWER_MARKED'])||$_stepStatus!=$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]){
					$stepHandinHistory = $w2_libW2->getStepHandinHisotryRecord($stepId,$w2_thisStudentID,$w2_m_ansCode[0],$handinVersion-1);
					$answerMarked = $stepHandinHistory[0]['ANSWER_MARKED'];	
					$draftVersion = $handinVersion-1;
				}
				$h_writingTaskContent = $answerMarked;
			}
		
		}
		
		break;
}
	
### Teacher's Comment
if ($w2_m_config_hideTeacherCommentBtn) {
	// hide button => do nth	
}
else {
	//$isCommentEditMode = ($w2_thisAction == $w2_cfg["actionArr"]["marking"])? true : false; 
	//$objHandinLayout->setTeacherCommentHtml($w2_libW2->getTeacherCommentButton().$w2_libW2->getTeacherCommentLayer($isCommentEditMode, $stepStudentCommentId));

	$showCommentBtn = false;
	$showSharingToStudentBtn = false;
	$isCommentEditMode = false;
	$showPeerCommentBtn = false;
	$isPeerCommentEditMode = false;
	$isViewSelfPeerComment = false;
	if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
//		// teacher marking => always show comment button and can always edit the comment
//		$showCommentBtn = true;
//		$isCommentEditMode = true;

		// teacher marking => show comment button if the student has submitted this step
		if ($studentHasSubmittedStep) {
			$showCommentBtn = true;
			$isCommentEditMode = true;
			
			if ($isLastStep) {
				// peer comment only show in last step
				$showPeerCommentBtn = true;
				$showSharingToStudentBtn = true;
				$isViewSelfPeerComment = true;

			}
		}
	}
	else if ($w2_thisAction == $w2_cfg["actionArr"]["peerMarking"]) {
		if ($isLastStep && $studentHasSubmittedStep) {
			$showPeerCommentBtn = true;
			$isPeerCommentEditMode = true;
			$peerMarkingDataRecordId = $peerMarkingDataInfoAry[0]['RECORD_ID'];
		}
	}
	else {
		// not marking 	=> student view own handin => always view only
		
		if ($stepStudentCommentId == '') {
			// no comment => hide the comment button
		}
		else {
			$showCommentBtn = true;
		}
		
		if ($isLastStep && $hasPeerCommented) {
			$showPeerCommentBtn = true;
			$isViewSelfPeerComment = true;
		}
	}
	
	$commentBtnHtml = '';
	if ($showCommentBtn) {
		$commentBtnHtml = $w2_libW2->getTeacherCommentButton().$w2_libW2->getTeacherCommentLayer($isCommentEditMode, $stepStudentCommentId);
	}
	//$objHandinLayout->setTeacherCommentHtml($commentBtnHtml);


	if($showSharingToStudentBtn){
		//function shareToStudent(contentCode,schemeCode,stepId,stepHandinCode,studentId){

		$sql = "select count(*) as `count` from {$intranet_db}.W2_SHARE_TO_STUDENT_HANDIN where CONTENT_CODE='{$contentCode}' and SCHEME_CODE ='{$schemeCode}' and STEP_ID  ={$stepId} and STUDENT_ID={$w2_thisStudentID} and STEP_HANDIN_CODE='{$w2_m_ansCode[0]}'";
		$objDB = new libdb();
		$_rsTmp = $objDB->returnVector($sql);
		$_setToShareBefore = current($_rsTmp);
			


		$_setShareAction = 1; //1 --> insert this sharing to DB
		if($_setToShareBefore > 0){
			//since share before , delete it
			$_setShareAction = 0; //0 --> delete this sharing to DB
		}
		//error_log("_setToShareBefore[".$_setToShareBefore."]   _setShareAction [".$_setShareAction."] sql = ".$sql."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		//$_shareCaption = ($_setShareAction == 1)?'Set Sharing to Student':'Cancel Sharing to Student';
		$sharingToStudentBtnHtml = $w2_libW2->getShareButton($_setShareAction, $contentCode, $schemeCode, $stepId, $w2_m_ansCode, $w2_thisStudentID);

		$_setShareAction = null; // reset it
	}
	$objHandinLayout->setSharingToStudentHtml($sharingToStudentBtnHtml);

	
	$peerCommentBtnHtml = '';
	if ($showPeerCommentBtn) {
		$peerCommentBtnHtml = $w2_libW2->getPeerCommentButton().$w2_libW2->getPeerCommentLayer($isPeerCommentEditMode, $peerMarkingDataRecordId, $isViewSelfPeerComment, $writingId, $w2_thisStudentID);
	}
	//$objHandinLayout->setPeerCommentHtml($peerCommentBtnHtml);
}


### Step
//$stepArr = $w2_libW2->getWritingWholeStep($writingId,1);
//$stepArr = $objWriting->findStep();
$allStepAns = getWritingWholeStepAns($writingId,$w2_thisStudentID);

$stepAns = array();
$stepAns = praseAllStepAns($allStepAns);

$thisStepAns = $stepAns[$stepId];

$w2_s_stepAnsAry = array();
if(is_array($w2_m_requestAnsCode ) && count($w2_m_requestAnsCode) > 0){
	$w2_s_stepAnsAry = $w2_libW2->getWritingStepAnsByStepCode($w2_m_requestAnsCode,$writingId,$w2_thisStudentID);
}

$w2_s_thisStepAns = array();
foreach((array)$thisStepAns as $_stepCode => $_stepAns){
	$w2_s_thisStepAns[$_stepCode] = $_stepAns;
}
$h_walkingStepHtml = $w2_libW2->getWalkingStep($stepArr,$schemeCode,$writingId,$contentCode,$stepId,$studentStepStatusAry, $objWriting->isCompleted(), $w2_thisAction, $stepCode);
if ($w2_thisAction == $w2_cfg["actionArr"]["peerMarking"]) {
	$h_walkingStepHtml = '';
}else if($w2_thisAction == $w2_cfg["actionArr"]["sharing"]){
	$h_walkingStepHtml = '';
}

$objHandinLayout->setStepHtml($h_walkingStepHtml);

### Content
$preStepID = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,0);
$preStepCode = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,0,2);

$nextStepID = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,1);
$nextStepCode = $w2_libW2->getNextPreStepInfo($stepArr,$stepId,1,2);

if($preStepID > 0){
	$h_preStepButton = $w2_libW2->htmlStepButton($schemeCode,$preStepCode,$preStepID,$writingId,$contentCode,0);
}


############### Answer Display [Start] ###############
$w2_h_answerDefaultDisabledAttr = '';
if ($w2_m_config_disableAnswerIfSubmittedWriting && $studentHasSubmittedWriting) {	// disable the answer input if the writing is submitted
	$w2_h_answerDefaultDisabledAttr = 'disabled';
}
if ($w2_m_config_disableAnswerIfCompletedWriting && $objWriting->isCompleted()) {
	$w2_h_answerDefaultDisabledAttr = 'disabled';
}
//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
if ($w2_thisActionIsMarking) {
	$w2_h_answerDefaultDisabledAttr = 'disabled';
}

$w2_h_checkBtnDefaultDisplayAttr = '';
if ($w2_m_config_hideCheckBtnIfSubmittedWriting && $studentHasSubmittedWriting) {	// hide the "check" button if the writing is submitted
	$w2_h_checkBtnDefaultDisplayAttr = 'display:none;';
}
if ($w2_m_config_hideCheckBtnIfCompletedWriting && $objWriting->isCompleted()) {
	$w2_h_checkBtnDefaultDisplayAttr = 'display:none;';
}
//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
if ($w2_thisActionIsMarking) {
	$w2_h_checkBtnDefaultDisplayAttr = 'display:none;';
}

$w2_h_modelAnsDefaultDisplayAttr = 'display:none;';
if ($w2_m_config_showModelAnswerIfSubmittedStep && $studentHasSubmittedStep) {	// show the model answer if the step is submitted
	$w2_h_modelAnsDefaultDisplayAttr = '';
}
if ($w2_m_config_showModelAnswerIfCompletedWriting && $objWriting->isCompleted()) {
	$w2_h_modelAnsDefaultDisplayAttr = '';
}
//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"]) {
if ($w2_thisActionIsMarking) {
	$w2_h_modelAnsDefaultDisplayAttr = '';
}


//(20120509) special handle for LS (eng / chi) , display the div only (block) if student has submitted the writing
$h_w2_ls_lastStep_div_displayStyle = ($studentHasSubmittedWriting)? 'block':'none' ;

//show model passage after last step submit for Chinese Writing
$h_w2_CHI_ls_model_passage_displayStyle = ($studentHasSubmittedWriting||$isViewOnly) ? 'block':'none' ;

################ Answer Display [End] ################


############### Infobox Display [Start] ###############
$w2_h_infoboxPropertyArr = array();
$numOfInfobox = count((array)$w2_m_infoboxCode);
for ($i=0; $i<$numOfInfobox; $i++) {
	$_infoboxCode = $w2_m_infoboxCode[$i];
	$_infoboxStatus = $w2_s_thisStepAns[$_infoboxCode];
	
	$w2_h_infoboxPropertyArr[$_infoboxCode]['saveStatus'] = $_infoboxStatus;
	//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"] || $w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
		$w2_h_infoboxPropertyArr[$_infoboxCode]['doneSpanStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['notDoneSpanStyle'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = '';
	}
	else if ($_infoboxStatus == $w2_cfg["DB_W2_STEP_HANDIN_ANSWER"]["infoboxHasSubmitted"]) {
		$w2_h_infoboxPropertyArr[$_infoboxCode]['doneSpanStyle'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['notDoneSpanStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = '';
	}
	else {
		$w2_h_infoboxPropertyArr[$_infoboxCode]['doneSpanStyle'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['notDoneSpanStyle'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivDisplay'] = '';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = 'display:none;';
	}
	
	//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"] || $objWriting->isCompleted()) {
	if ($w2_thisActionIsMarking || $objWriting->isCompleted()) {
		// always hide the submit button and show model answer if the writing has completed or teacher doing marking
		$w2_h_infoboxPropertyArr[$_infoboxCode]['btnDivDisplay'] = 'display:none;';
		$w2_h_infoboxPropertyArr[$_infoboxCode]['modelAnsStyle'] = '';
	}
}

################ Infobox Display [End] ################


############### Right-hand Reference Display [Start] ###############
$w2_h_refHtml = array();
$w2_h_refHighlightAry = array();
for ($i=0, $i_max=count($w2_m_infoboxCode); $i<$i_max; $i++) {
	$_infoxboxCode = $w2_m_infoboxCode[$i];
 	$objW2RefManager = new libW2RefManager();
 	
 	//if ($w2_thisAction == $w2_cfg["actionArr"]["marking"] || $w2_thisAction == $w2_cfg["actionArr"]["preview"] || $objWriting->isCompleted()) {
 	if ($w2_thisActionIsMarking || $w2_thisAction == $w2_cfg["actionArr"]["preview"] || $objWriting->isCompleted()) {
 		// cannot add group and item for marking, preview or completed writing
 		$objW2RefManager->setIsEditable(false);
 	}
 	else {
 		$objW2RefManager->setIsEditable(true);
 	}
 	
 	$objW2RefManager->setInfoboxCode($_infoxboxCode);
 	$objW2RefManager->setWritingStudentId($writingStudentId);
 	$objW2RefManager->setPresetCategoryAry($c_model_cfg['refPresetAry']);
 	$objW2RefManager->setStepAnsAry($w2_s_stepAnsAry);
 	
 	$objW2RefManager->setDisplayMode($w2_m_refDisplayMode);

 	$w2_h_refHtml[$_infoxboxCode] = $objW2RefManager->display();
 	
}
################ Right-hand Reference Display [End] ################

################ Vocabulary Suggested by student  for science [Start] ###############
$w2_h_suggestedVocabForStudentStyle  = 'display:none;';
$w2_h_suggestedVocabForStudentButtonStyle= 'color:#a9a9a9';
$stepCode_SuggestedVocabForStudent = 'sci_scheme1_c1_ans4';
if($studentHasSubmittedStep){
	$w2_h_suggestedVocabForStudentStyle = '';
	$w2_h_suggestedVocabForStudentButtonStyle='display:none;';
}
$h_suggestedVocabForStudent = $w2_libW2->getSuggestedVocabForStudent($stepCode_SuggestedVocabForStudent);
################ Vocabulary Suggested by student  for science [End] ###############


############### GET STUDENT UPLOAD FILE [Start] ###############
if($w2_m_config_getStudentFileInThisStep||$conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){

	//if (teacher preview this content) or (the student see this writing and it is complete) , do not allow to upload or edit again
	$fileUploadAllowAction = ($w2_thisAction == $w2_cfg["actionArr"]["preview"] || $objWriting->isCompleted())? 
								$w2_cfg['uploadFileActionMode']['doNotAllowUpload'] :
									$w2_cfg['uploadFileActionMode']['allowUpload'];

	//if($w2_thisAction ==  $w2_cfg["actionArr"]["marking"]){
	if ($w2_thisActionIsMarking) {
		$fileUploadAllowAction = $w2_cfg['uploadFileActionMode']['doNotAllowUpload'];
	}

	if($w2_thisAction == $w2_cfg["actionArr"]["preview"]){
		//do nothing , don't show the file upload
	}else{
		$h_fileUpload = $w2_libW2->displayViewStudentUploadFile($w2_thisStudentID,$_stepId,$fileUploadAllowAction,$isContentInputStep2);
	}

}
############### GET STUDENT UPLOAD FILE [End] ###############

############### GET STUDENT WRITING EDITOR [Start] ###############
if ($stepCode == $w2_objContent->getWritingTaskStepCode()) {
	$writingTaskStepAns = $w2_s_thisStepAns[$w2_m_ansCode[0]];
	if ($writingTaskStepAns == '') {
		$h_writingTaskContent = $w2_m_writingDefaultContent;
	
		for ($i=0, $i_max=count((array)$w2_m_requestAnsCode); $i<$i_max; $i++) {
			$_requiredAnswerCode = $w2_m_requestAnsCode[$i];
			$_requiredAnswer = $w2_libW2->getWritingStepAnsByStepCodeInParsedArray($_requiredAnswerCode, $w2_s_stepAnsAry);
			
			$_replaceTextCode = '<!--answer'.($i+1).'-->';
			$h_writingTaskContent = str_replace($_replaceTextCode, $_requiredAnswer, $h_writingTaskContent);
		}
	}
	else if(!$isHistory){
		$h_writingTaskContent = $writingTaskStepAns;	
	}
	if($isLastStep &&$w2_thisAction==$w2_cfg["actionArr"]["marking"]){
		$h_writingTaskContent = !empty($studentStepHandinRecord['ANSWER_MARKED'])?$studentStepHandinRecord['ANSWER_MARKED']:$h_writingTaskContent;
	}
	$h_writingTaskContent = $w2_libW2->getAnsValue($h_writingTaskContent);
	
	$w2_h_editorSource = '';		// 'ip' or 'eclass'
	$isViewOnly = false;
	if($isHistory){
		$isViewOnly = true;
	}elseif ($w2_thisAction==$w2_cfg["actionArr"]["marking"]) {
		// essay marking config
		// enable the below line if you want to turn on essay marking function
		$w2_h_editorSource = $w2_cfg["editorSource"]["eclass"];
		// enable the below line if you want to turn off essay marking function
		//$isViewOnly = true;
	}
	else if ($w2_thisAction==$w2_cfg["actionArr"]["peerMarking"]) {
		$isViewOnly = true;
	}
	else {
		if ($objWriting->isCompleted()) {
			$isViewOnly = true;
		}
		else {
			$w2_h_editorSource = $w2_cfg["editorSource"]["ip"];
		}
	}
	
	$h_writingTaskDisplay = '';
	$h_writingEditorOnDocumentReadyJs = '';
	$editorBoxWidth = "100%";
	$editorName = 'r_question[editor]['.$w2_m_ansCode[0].']';

	if ($w2_h_editorSource == $w2_cfg["editorSource"]["ip"]) {
		// Generate from IP
		$editorBoxHeight = 320;
		
		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		$objHtmlEditor = new FCKeditor($editorName, $editorBoxWidth, $editorBoxHeight);
		$objHtmlEditor->Value = $w2_libW2->getEditorViewContent($h_writingTaskContent);
		$h_writingTaskDisplay = $objHtmlEditor->Create2();
		
		$h_writingEditorIndividualAnsHandlingJs = "editorIndividualAnsHandling('".$editorName."')";
	}
	else if ($w2_h_editorSource==$w2_cfg["editorSource"]["eclass"]) {
		// Generate from eClass
		$editorIFrameHeight = 520;
		$editorBoxHeight = $editorIFrameHeight - 40;
		$iFrameSource = 'http://'.$eclass40_httppath.'src/tool/misc/html_editor/index_w2.php?ts='.strtotime(date('Y-m-d H:i:m'));
		
		$objHtmlEditor->Value = $h_writingTaskContent;
		$h_writingTaskContent = intranet_htmlspecialchars($h_writingTaskContent);
		
		$h_writingTaskDisplay = <<<html
<!-- for start iFrame -->
<iframe id="editorIFrame" name="editorIFrame" frameborder="0" cellspacing="0" style="width:{$editorBoxWidth}; height:{$editorIFrameHeight}px; border:0px solid #000000;"></iframe>
<input type="hidden" id="r_stepHandinCode" name="r_stepHandinCode" value="{$w2_m_ansCode[0]}" />
<input type="hidden" id="r_editorBoxHeight" name="r_editorBoxHeight" value="{$editorBoxHeight}" />
<input type="hidden" id="r_editorInitValue" name="r_editorInitValue" value="{$h_writingTaskContent}" />
<!-- for end iFrame -->
html;

		$h_writingEditorOnDocumentReadyJs = "editorOnDocumentReadyHandling('".$w2_h_editorSource."', '".$iFrameSource."');";
	}	
	else if ($isViewOnly) {
		$h_writingTaskDisplay = $w2_libW2->getEditorViewContent($h_writingTaskContent);
	}	
}
$fullmark = $objWriting->getFullMark();
$lowestmark = $objWriting->getLowestMark();
if($showCorrectionArea){ ### REDO ###
	###Siuwan 20131211 Get Student Handin Record for correction
	$draftSelection = $w2_libW2->getStudentStepHandinHistorySelection($studentStepHandinRecord['VERSION'],$handinVersion);
	$displayHandinCommentHTML = $w2_libW2->getDisplayMarkingLayerHTML($stepId,$w2_thisStudentID,$writingId,$w2_m_ansCode[0],$draftVersion);
	$html = '<div class="correction_board">';
		$html .= '<div class="draft_previous">';
			$html .= $draftSelection;
            $html .= '<div class="write">';
            	$html .= '<div class="mark_comment">';
                	$html .= '<div id="display_mark_comment" class="show_sticker">';	
                    	$html .= $displayHandinCommentHTML;
                    $html .= '</div>';
                $html .= '</div>';    
                $html .= '<div id="paper_template" class="template_'.str_pad($templateID,  2, "0", STR_PAD_LEFT).'">';// Add By Adam                                
                	$html .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
                    	$html .= '<div class="pt_top_element"></div>';
                    $html .= '</div></div></div>';
                    $html .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
                    	$html .= '<div class="paper_main_content">';
                        	$html .= '<div class="essay">'.$answerMarked.'</div>';
                        	$html .= '<p class="spacer"></p>';
                        $html .= '</div>';
                   $html .= '</div></div></div>';
					$html .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
                    $html .= '</div></div></div>';
                    $html .= '<div id="template_clipart"><span class="clipart_'.str_pad($clipArtID,  2, "0", STR_PAD_LEFT).'"></span></div>';// Add By Adam
                $html .= '</div>';	
           $html .= '</div>';
           $html .= '<p class="spacer"></p>';
		$html .= '</div>';
        $html .= '<div class="write_correction">';
        $draftName = $intranet_session_language=='en'?$Lang['W2']['revisedDraft']:str_replace('<-version->',$handinVersion,$Lang['W2']['revisedDraft']);
        	$html .= '<h1 class="correction_title">'.$draftName.'</h1>';
            $html .= '<div class="write">';
            	$html .= '<div id="paper_template" class="template_01">';                                
                	$html .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
                    	$html .= '<div class="pt_top_element"></div>';
                    $html .= '</div></div></div>';
                    $html .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
                    	$html .= '<div class="paper_main_content">';
                        	$html .= '<div class="essay">';
                            	$html .= $h_writingTaskDisplay;
                            $html .= '</div>';
                            $html .= '<p class="spacer"></p>';
                        $html .= '</div>';
                    $html .= '</div></div></div>';
					$html .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
                    $html .= '</div></div></div>';
                    $html .= '<div id="template_clipart"><span class="clipart_01"></span></div>';
                    $html .= '</div>';	
				$html .= '</div>';
                $html .= '<p class="spacer"></p>';
            $html .= '</div>';
            $html .= '<div class="ref_board">';
            	$html .= '<a id="ref_link" href="javascript:void(0);">'.$Lang['W2']['reference'].' </a>';
           		$html .= '<div class="ref" id="ref_layer"></div>';
			$html .= '</div>';
			$html .= '<p class="spacer"></p>';
		$html .= '</div>';
		$html .= '<br style="clear:both" /><br />';
$h_writingTaskDisplay = $html;
}else{
	$objComment = new libStepStudentComment();
	$commentBankSelection = $objComment->getCommentBankSelection();
	$displayHandinCommentHTML = '';
	if($showMarkingSummary){
		$displayHandinCommentHTML .= $w2_libW2->getDisplayMarkingLayerHTML($stepId,$w2_thisStudentID,$writingId,$w2_m_ansCode[0],$handinVersion,$showOverallMark);
		$displayHandinCommentHTML = '
	    	<div class="mark_comment">
	           	<div id="display_mark_comment" class="show_sticker">'.$displayHandinCommentHTML.'</div>
	        </div>
		';
	}
	$displayHandinCommentHTML .= '<div id="paper_template" class="template_'.str_pad($templateID,  2, "0", STR_PAD_LEFT).'">';   
	$displayHandinCommentHTML .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
	$displayHandinCommentHTML .= '<div class="pt_top_element"></div>';
	$displayHandinCommentHTML .= '</div></div></div>';
	$displayHandinCommentHTML .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
	$displayHandinCommentHTML .= '<div class="paper_main_content">';
	$displayHandinCommentHTML .= '<div class="essay">'.$h_writingTaskDisplay.'</div>';
	$displayHandinCommentHTML .= '<p class="spacer"></p>';
	$displayHandinCommentHTML .= '</div>';
	$displayHandinCommentHTML .= '</div></div></div>';
	$displayHandinCommentHTML .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
	$displayHandinCommentHTML .= '</div></div></div>';
	$displayHandinCommentHTML .= '<div id="template_clipart"><span class="clipart_'.str_pad($clipArtID,  2, "0", STR_PAD_LEFT).'"></span></div>';
	$displayHandinCommentHTML .= '</div>';	

	$h_writingTaskDisplay = $displayHandinCommentHTML;
	if($showMarkingBoard){
		$stickerListHTML = '';
		$marker_sticker_ary = explode(":",$marker_sticker);
		for($i = 0,$i_max = count($w2_cfg["contentWriting"]["MarkingSticker"]);$i < $i_max ;$i++){
			$_id = $w2_cfg["contentWriting"]["MarkingSticker"][$i];
			$_class = in_array($_id,$marker_sticker_ary)?' class="selected"':'';
			$stickerListHTML .= '<li id="'.$_id.'" '.$_class.'>';
		   	  $stickerListHTML .= '<a href="javascript:void(0);" class="template_thumb marking_sticker"><span></span></a>';
		    $stickerListHTML .= '</li>';
		}
		
		$marking_css = ($w2_thisAction == $w2_cfg["actionArr"]["marking"])?'teacher':'';
		$mark_out_of_range_warning = $Lang['W2']['scoreOutOfRange'].": (".$lowestmark." - ".$fullmark.")";
		$h_writingTaskDisplay .= '
			<div class="marking_board">
		     	<div class="marking_board_top"><div class="marking_board_top_right"><div class="marking_board_top_bg"></div></div></div>
		     	<div class="marking_board_content"><div class="marking_board_content_right '.$marking_css.'"><div class="marking_board_content_bg">
		                <!--mark-->
		                 <div class="tool_mark">
		                    <fieldset>
		                      <legend>'.$Lang['W2']['marks'].'</legend>
		                      <input type="text" name="r_mark" id="r_mark" value="'.$marker_score.'">/'.$fullmark.'</fieldset>
								<br style="clear:both;">
								<span id="markWarningSpan" style="color:red;display:none;">'.$mark_out_of_range_warning.'</span>
		                    </div>
		        		<!-- comment-->
		                <div class="tool_comment">
		                	<a href="javascript:void(0);" class="btn_comment" id="comment_link"> '.$Lang['W2']['addComment'].'</a>
		                	<div id="write_comment">
		                    <h1>'.$Lang['W2']['yourComment'].'</h1>
		                    <textarea rows="3" wrap="virtual" class="write_comment_area" id="r_comment" name="r_comment[0]">'.$marker_comment.'</textarea>
							<br style="clear:both;">
							'.($w2_thisAction==$w2_cfg["actionArr"]["peerMarking"]?$commentBankSelection:'').'
		                    </div>
		                </div>
		                <!--sticker-->
		                <div class="tool_sticker">
		                	<a href="javascript:void(0);" class="btn_sticker" id="sticker_link"> '.$Lang['W2']['giveSticker'].'</a>
		                    <div id="select_sticker">
		                    <h1>'.$Lang['W2']['selectSticker'].'</h1>
		                    <div class="control_board">
		                    	<ul class="sticker_list">
		                    		'.$stickerListHTML.'
		               				<p class="spacer"></p>
		              
					        	</ul>  
					        </div>
		                    
		                </div>
		            </div>
		            <!---->
		            <div class="edit_bottom">
		                <input type="button" class="formbutton" value="'.$Lang['Btn']['Submit'].'" onclick="saveHandinMarking(\''.$successMsg.'\',\''.$failMsg.'\');"/>
						<input type="button" class="formsubbutton" value="'.$Lang['Btn']['Reset'].'" onclick="resetMarkingArea();"/>
		            </div>
		    
		    </div></div></div>
		 	<div class="marking_board_bottom"><div class="marking_board_bottom_right"><div class="marking_board_bottom_bg"></div></div></div>                                                                
		 </div> 
		';
	}
}
################ GET STUDENT WRITING EDITOR [End] ################


############### Buttons [Start] ###############
$saveOnlyFlag = 0;
$goToNextStepFlag = 1;

if($thisStepRequestApproval){
	//if this step set require approval
	if($stepStudentApprovalStatus == $w2_cfg["DB_W2_STEP_STUDENT_APPROVAL_STATUS"]["waiting"]){
		// This student has not been approved to go to next step, not allow to next step  , set $goToNextStepFlag = 2;
		//allow submit but do not allow go next step
		$goToNextStepFlag = 2;
	}
}

//error_log("goToNextStepFlag [".$goToNextStepFlag."]   <----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
$showNextButton = false;
if($nextStepID > 0){
	$btnDisabled = false;  // default enable the next button 
	
	### Disable the next button if not all the questions are answered (if required)
	if ($w2_m_config_disabledNextBtnIfNotClickedAllModelAnswer) {		// $w2_m_config_disabledNextBtnIfNotClickedAllModelAnswer is defined in model.php of each steps
		$btnDisabled = false;
	}
	
	if($w2_m_config_requireInputAllAnswer){
		$allQuestionAnswered = true;
		for ($i=0, $i_max=count($w2_m_ansCode); $i<$i_max; $i++) {
			$_ansCode = $w2_m_ansCode[$i];
			//if (isset($w2_s_thisStepAns[$_ansCode]) && $w2_s_thisStepAns[$_ansCode] != '') {
			if (isset($w2_s_thisStepAns[$_ansCode])) {		// answer can be blank
				// answered => do nth
			}
			else {
				$allQuestionAnswered = false;
			}
		}
		$btnDisabled = ($allQuestionAnswered)? false : true;
	}
	
	$showNextButton = true;
}

if ($objWriting->isCompleted()) {
	$showNextButton = false;
}

if ($showNextButton) {
	if($isContentInputStep2&&$conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){
		$h_nextStepButton = $w2_libW2->htmlSaveStudentHandInWithAttachmentButton($goToNextStepFlag, $Lang['Btn']['Next'], $btnDisabled);
	}else{
		$h_nextStepButton = $w2_libW2->htmlSaveStudentHandInButton($goToNextStepFlag, $Lang['Btn']['Next'], $btnDisabled);
	}

}

$h_saveAsDraftButton = $w2_libW2->htmlSaveStudentHandInButton($saveOnlyFlag,'','',$updateStepStatus = $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["draft"]);
$h_saveAndNextButton = $w2_libW2->htmlSaveStudentHandInButton($goToNextStepFlag);
$h_submitHandInButton = $w2_libW2->htmlSubmitStudentHandInButton();
//$h_markHandInButton = $w2_libW2->htmlSubmitStudentHandInButton();


$studentHandInAllowAction = $w2_cfg['studentHandInAllowAction']['edit']; // default is edit mode

if($_SESSION['UserID'] == $w2_thisStudentID && !$isViewOnly){
	//do noting
}else{
	//$_SESSION['UserID'] != $w2_thisStudentID 
	//this case may be teacher doing marking or student peer marking , reset all the button display
	$h_saveAndNextButton = '';
	$h_submitHandInButton = '';
	$h_saveAsDraftButton = '';
	$h_nextStepButton = '';
	$studentHandInAllowAction = $w2_cfg['studentHandInAllowAction']['view'];  // view the handin only (cannot edit)
}

if ($objWriting->isCompleted()) {
	$h_saveAsDraftButton = '';
	$h_saveAndNextButton = '';
	$h_submitHandInButton = '';
	$studentHandInAllowAction = $w2_cfg['studentHandInAllowAction']['view']; // view the handin only (cannot edit)
}



if($_stepStatus == $w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["submitted"]){
	$h_saveAsDraftButton = '';
}

### Cancel Button
if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	$h_cancelButton = '';
}
else {
	$h_cancelButton = $w2_libW2->htmlCancelStudentHandInButton($contentCode, $w2_thisAction, $writingId, $objWriting->isCompleted(), $r_handinSubmitStatus, $r_fromToBeMarked);
}


################ Buttons [End] ################
############### GET STUDENT CONCEPT MAP[Start] ###############

if(is_array($c_model_cfg['withConceptMapStep']) 
	&& count($c_model_cfg['withConceptMapStep']) >0 
		&& in_array(strtolower($r_stepCode), $c_model_cfg['withConceptMapStep']) ) {

if($c_model_cfg['withConceptMapStep_ansCode'] == ''){
	//for safe programming , remind programmer only
	echo 'Concept Map Setting Error!! Please set an answer code to the this scheme model file<br/>';
}
		$conceptMapWithDefault = $objWriting->getWithDefaultConceptMap();
		$conceptMapStepCode = $w2_objContent->getConceptMapStepCode();


		$conceptMapLastUpdateTime = $w2_libW2->findStepAnsLastModified($w2_thisStudentID,$stepId,$c_model_cfg['withConceptMapStep_ansCode']);
		$h_conceptMapLastUpdateTime = trim($conceptMapLastUpdateTime) == ''? $w2_cfg['emptyDataStyle1'] : $conceptMapLastUpdateTime;


		$conceptMapEclassTaskId = $w2_libW2->findEclassTaskId($writingId ,$conceptMapStepCode);
		$h_conceptMapEclassTaskId = $conceptMapEclassTaskId;
		$conceptMapDisplayMode = ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) ? 
									$w2_cfg['conceptMapDisplayMode']['teacherPreivewImage']: 
										$w2_cfg['conceptMapDisplayMode']['studentAns'];
		
		$h_conceptMapLink = $w2_libW2->findPowerConceptLinkForStudent($w2_thisStudentID,$objWriting,$w2_classRoomID,$c_model_cfg['withConceptMapStep_ansCode'],$studentHandInAllowAction,$conceptMapWithDefault,$conceptMapDisplayMode,$conceptType);
		$h_conceptType = $conceptType;
}
############### GET STUDENT CONCEPT MAP [End] ###############


$h_content = '';
ob_start();
include_once($contentTemplate);
$h_content .= "<!--w2 start-->\n";
$h_content .= '<!-- layout : '.$contentCode.'/'.$schemeCode.'/'.$stepCode. '-->';
$h_content .= ob_get_contents();
$h_content .= "<!--w2 end-->\n";
ob_end_clean();

$objHandinLayout->setContentHtml($h_content);




$linterface->LAYOUT_START();
include_once('templates/handin/viewHandinTest.tmpl.php');
$linterface->LAYOUT_STOP();
?>


<?

function htmlGoToStep($schemeCode,$stepCode,$stepID,$writingid,$content){
	$strJS = 'goToStep(\''.$schemeCode.'\',\''.$stepCode.'\',\''.$stepID.'\',\''.$writingid.'\',\''.$content.'\')';
	return $strJS; 
}


/**
* Return all step answer for a give writing id 
*
* @param : INT $writingid, Writing ID
* @param : INT $studentID, Ans for the Student ID 
* @return : String HTML for the walking step
*/
function getWritingWholeStepAns($writingid,$studentID){
	global $intranet_db;
	$sql= 'select	step.CODE as "STEP_CODE",
					step.SEQUENCE as "STEP_SEQUENCE",
					h.ANSWER as "ANSWER",
					step.STEP_ID as "STEP_ID",
					h.STEP_HANDIN_CODE as "STEP_HANDIN_CODE"
			from 
					'.$intranet_db.'.W2_WRITING as w 
					inner join '.$intranet_db.'.W2_STEP as step on step.WRITING_ID = w.WRITING_ID
					inner join '.$intranet_db.'.W2_STEP_HANDIN as h on h.STEP_ID = step.STEP_ID
			where 
					h.USER_ID = '.$studentID.' and w.WRITING_ID = '.$writingid;

	$objDB  = new libDB();

	$result = $objDB->returnResultSet($sql);

	return $result;
}

/**
* GROUP THE STUDENT STEP HANDIN BY STEPID , THEN STEPHANDINCODE. SUCH THAT ANS CAN RETRIEVE BY $result['12']['HandeInCodeA'];  12 --> STEP ID , HANDEINCODEA --> HAND IN CODE
*
* @param : array $allStepAns , all student handin for a given writing ID
* @return : Array $result , Grouped Array 
*/
function praseAllStepAns($allStepAns){


	$result = array();

	for($i = 0, $i_max = count($allStepAns);$i< $i_max; $i++){
		$_stepID = $allStepAns[$i]['STEP_ID'];
		$_StepHandinCode = $allStepAns[$i]['STEP_HANDIN_CODE'];
		$_StepHandinAns = $allStepAns[$i]['ANSWER'];
		$result[$_stepID][$_StepHandinCode] = $_StepHandinAns;
	}

	return $result;
}


// moved to libw2.php
///**
//* Return STEP ANS by a giving step code that $stepCodeAnsArray is generate in f:getWritingStepAnsByStepCode
//*
//* @param : String $stepHandinCode
//* @param : Array  $stepCodeAnsArray , a set for student 
//* @return : Student answer for the giving writing ID and step handin code and student id
//*/
//function getWritingStepAnsByStepCodeInParsedArray($stepHandinCode,$stepCodeAnsArray){
//	$returnAnswer = '';
//	if(is_array($stepCodeAnsArray) && count($stepCodeAnsArray)){
//		for($i = 0,$i_max = count($stepCodeAnsArray);$i<$i_max;$i++){
//			$_details = $stepCodeAnsArray[$i];
//			$_stepHandinCode = $_details['StepHandinCode'];
//
//			if($_stepHandinCode == $stepHandinCode){
//				$returnAnswer = $_details['ANSWER'];
//				break;
//			}
//		}
//	}
//	return $returnAnswer;
//}





?>