<?
# modifying :  

### Must show English UI for eng content and Chinese UI for chi content
$r_contentCode = $_POST['r_contentCode']? $_POST['r_contentCode'] : $_GET['r_contentCode'];
switch ($r_contentCode) {
	case 'eng':
	case 'ls_eng':
		$intranet_hardcode_lang = 'en';
		break;
	case 'chi':
	case 'ls':
	case 'sci':
		$intranet_hardcode_lang = 'b5';
		break;
}


$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/w2_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/w2/w2Config.inc.php");
include_once($PATH_WRT_ROOT."includes/w2/libw2.php");
include_once($PATH_WRT_ROOT."includes/w2/libw2_eclass.php");

include_once($PATH_WRT_ROOT."includes/table/libtable_mgr.php");
include_once($PATH_WRT_ROOT."includes/table/libtable_sqlGenerator.php");
include_once($PATH_WRT_ROOT."includes/table/libtable_layout.php");
include_once($PATH_WRT_ROOT."includes/table/libpagingation.php");


intranet_auth();
intranet_opendb();

$w2_libW2 = new libW2();
$lpf = new libportfolio2007();
$linterface = new interface_html("w2_default.html");


$w2_thisUserID = $_SESSION['UserID'];
$w2_thisUserType = $_SESSION['UserType'];
$w2_thisIsTeaching = $_SESSION['isTeaching'];
$w2_thisIsAdmin = $w2_libW2->isAdminUser();


$w2_thisAction = (isset($_POST['r_action']))? $_POST['r_action'] : $_GET['r_action'];
switch ($w2_thisAction) {
	case $w2_cfg["actionArr"]["marking"]:
		if ($p_studentId == '') {
			// do nth
		}
		else {
			$_SESSION['w2_pretentStudentID'] = $p_studentId;
		}
		break;
	case $w2_cfg["actionArr"]["peerMarking"]:
		if ($p_studentId == '') {
			// do nth
		}
		else {
			$_SESSION['w2_pretentStudentID'] = $p_studentId;
		}
		break;
	case $w2_cfg["actionArr"]["preview"]:
	default:
		$_SESSION['w2_pretentStudentID'] = '';
}
$w2_thisStudentID = ($_SESSION['w2_pretentStudentID'])? $_SESSION['w2_pretentStudentID'] : $_SESSION['UserID'];





if ($w2_thisIsPretentStudent) {
	$_SESSION['w2_pretentStudentID'] = $p_studentId;
}

if ($_SESSION['w2_pretentStudentID'] == '') {
	//$w2_thisStudentID = ($p_studentId=='')? $_SESSION['UserID'] : $p_studentId;
	
	// 1. Student login and view own writing
	// 2. Teacher login but viewing own teaching writing, so this variable should be useless
	$w2_thisStudentID = $_SESSION['UserID'];
}
else {
	// 1. Student login and view other student's writing (peer marking)
	// 2. Teacher login and view student's writing
	$w2_thisStudentID = $_SESSION['w2_pretentStudentID'];
}



//$handinStudent = true;
//$pretentStudent = true;
//$_SESSION['w2']['isPretent'] = false;
//if ($handinStudent) {
//	// not change header
//	$_SESSION['w2']['thisStudentID'] = $p_studentId;
//}
//else if ($pretentStudent) {
//	// change header
//	$_SESSION['w2']['isPretent'] = true;
//	$_SESSION['w2']['thisUserID'] = $p_studentId;
//	$_SESSION['w2']['thisUserType'] = USERTYPE_STUDENT;
//	$_SESSION['w2']['thisStudentID'] = $p_studentId;
//}
//
//if ($_SESSION['w2']['isPretent']) {
//	$w2_thisUserID = $_SESSION['w2']['thisUserID'];
//	$w2_thisUserType = $_SESSION['w2']['thisUserType'];
//}
//$w2_thisStudentID = $_SESSION['w2']['thisStudentID'];





//$w2_thisTeacherID = $UserID;

//RECEIVE VARIABLE
$mod		= trim($mod);
$task		= trim($task);

if($mod == ''){
//	if($w2_thisUserType == USERTYPE_STAFF){
//		$mod='teacher';
//		$task='index';
//	}else{
//		$mod='student';
//		$task='index';
//	}
	
	$mod = 'common';
	$task = 'mainPage';
}

$task= ($task == '')? 'index' :$task;

$CurrentPage = "Student_Volunteer";
$CurrentPageName = $Lang['iPortfolio']['VolunteerArr']['Volunteer'];



### Top Tab Menu
$r_tabTask = ($r_tabTask=='')? $task : $r_tabTask;
if ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) {
	// Hide tab menu for preview mode
	$w2_h_tabMenu = '';
}
else {
	$w2_h_tabMenu = $w2_libW2->generateTabMenu($w2_thisUserType, $r_tabTask);
}



### Title ###
$MODULE_OBJ = $lpf->GET_MODULE_OBJ_ARR();


$w2_objEclass = libw2_eclass::getInstance();
$w2_isClassRoomExist = $w2_objEclass->getIsClassRoomExist();
$w2_classRoomID = $w2_objEclass->getClassRoomID();
$w2_classRoomDB = $w2_objEclass->getClassRoomDB();
//debug_r($w2_classRoomDB);

if($w2_isClassRoomExist){
	//do nothing
}else{
	echo "Setting not completed for writing 2.0 (class room missing)";
//	exit();
}

$task = str_replace("../","",$task);
$task_script = $mod.'/'.$task.'.php';

if(file_exists($task_script)){
	error_log('mod ['.$mod."] task [".$task."]  f [".$task_script."] sID [".$w2_thisStudentID."]-->". date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
	include_once($task_script);
}else{
	error_log(" File not find !! [".$task_script."] -->". date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
	include_once('accessDeny.php');
}

intranet_closedb();
?>
