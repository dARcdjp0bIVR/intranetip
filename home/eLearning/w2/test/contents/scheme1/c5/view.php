<?
///////////////////////
// start concept map //
///////////////////////
/*
$displayMode = ($w2_thisAction == $w2_cfg["actionArr"]["preview"]) ? $w2_cfg['conceptMapDisplayMode']['teacherPreivewImage']: $w2_cfg['conceptMapDisplayMode']['studentAns'];

$withDefaultConceptMap = $objWriting->getWithDefaultConceptMap();
$h_conceptMap = $w2_libW2->findPowerConceptLinkForStudent($w2_thisStudentID,$objWriting,$w2_classRoomID,$studentHandInAllowAction,$withDefaultConceptMap,$displayMode);
*/
//////////////////////
// end concept map  //
//////////////////////

?>

<script>
/*
$('#handinForm').submit(function() {
  alert('Handler for .submit() called.');
  return false;
});
*/

$(document).ready( function() {	
	<?=$h_writingEditorOnDocumentReadyJs?>
});

<?php 
//duplicat upload file in callBack_saveStepAns, comment first
//function callBack_saveFinalStepAns
?>
/*
function callBack_saveFinalStepAns(){
	alert('callBack_saveFinalStepAns');
	uploadFile();
}
*/

function callBack_saveStepAns(){
	uploadFile();
}

function individualAnsHandling() {
	<?=$h_writingEditorIndividualAnsHandlingJs?>
}

function saveMarkReturnMsg(status) {
	var returnMsg = (status=='1')? '<?=$Lang['General']['ReturnMessage']['RecordSaveSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['RecordSaveUnSuccess']?>';
	
	Scroll_To_Top();
	Get_Return_Message(returnMsg);
}
</script>
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>5</span>Writing</div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="content">
                            	Refine your draft by reminding yourself the following:
                            	<br />
                            	<br />
                            	1. What is the purpose of your writing? <span class="modelans_orange">To record</span>
                            	<br />
                            	2. Who is the audience of your writing? <span class="modelans_orange">Self</span>
                            	<br />
                            	3. Is the language formal or informal? <span class="modelans_orange">Informal</span>
                            	<br />
                            </div>
                            <div class="subtitle"><span>My writing</span></div>
                            <div class="content">
                            	<div class="draft">
                                <div class="write">
                                <?=$h_writingTaskDisplay?>
                                <br />
                                <!--<input type="button" class="formsubbutton" value="Reset" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'"/>-->
                                </div>
                                <div class="ref">
                                	<span class="title">Vocabulary:</span>
									<br />
									<span class="subtitle">Adjective describing feelings</span>
									<br /><?=$h_vocabDispaly?><br />
										<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox" onclick="loadVocabByThickbox('/eng/scheme1/reference/referenceVocab.php','<h1 class=ref>More Vocabulary</h1>','');">More...</a>

									<div class="line"></div><span class="title">Grammar:</span><br /><span class="subtitle">Simple past tense</span><br />We use the simple past tense to talk about things that happened in the past.<br/><a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox" onclick="loadContentByThickbox('/eng/scheme1/reference/referenceGrammer.php','<h1 class=ref>Grammar Analysis</h1>','');">More...</a><br /><div class="line"></div><span class="title">Brainstorming:</span> 

									<a href="javaScript:void(0)" onclick="<?=$h_conceptMapLink?>">Click here</a>
									
									<br /><div class="line"></div><span class="title">Userful expressions:</span><br /><ul><li>Dear Diary,</li><li>Hi Diary,</li><li>I had a wonder day with my parents today ...</li><li>I am really happy today ...</li><li>This is the happiest day of my life ...</li><li>I hope to spend more time with them soon</li><li>I need to stop now.</li><li>Good night, Diary</li></ul>
                                </div>
                                </div>
                                <!-- start of student attachment -->
                                <br style="clear:both" /><br />
								<div id="w2_studentUploadFilePanel">
<?php
								echo $h_fileUpload;
?>
								</div>
                              <!-- end of student attachment -->
                          </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
<?php 
	 echo $h_saveAsDraftButton;
?>
<?
	echo $h_submitHandInButton;
?>
<?
	echo $h_cancelButton;
?>    
                              <!--<input name="submit4" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="Cancel" />-->
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
					<input type="hidden" id="r_editorSource" name="r_editorSource" value="<?=$w2_h_editorSource?>" />