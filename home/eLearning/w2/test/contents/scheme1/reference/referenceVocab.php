<table border="0">
      <tr>
        <td colspan="6"><b>Adjectives describing feelings</b></td>
      </tr>

      <tr class="vocab_group">
        <td class="title" width="25%">Foundation :</td>
        <td>amazed <font color="red">(+ve)</font></td>
        <td>disappointed <font color="red">(-ve)</font></td>
        <td>tired <font color="red">(-ve)</font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>

      <tr class="vocab_group">
        <td>&nbsp;</td>
        <td>excited <font color="red">(+ve)</font></td>
        <td>happy <font color="red">(+ve)</font></td>
        <td>bored <font color="red">(-ve)</font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>


      <tr class="vocab_group">
        <td class="title">Advanced :</td>
        <td>anxious  <font color="red">(-ve)</font></td>
        <td>frightened  <font color="red">(-ve)</font></td>
        <td>calm  <font color="red">(+ve)</font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
		<td>&nbsp;</td>
        <td>exhausted  <font color="red">(-ve)</font></td>
        <td>proud   <font color="red">(+ve)</font></td>
        <td>delighted <font color="red">(+ve)</font></td>
        <td>&nbsp;</td>
		<td>&nbsp;</td>
      </tr>
</table>


<table border="0">
      <tr>
        <td colspan="6"><b>Adjectives describing personalities</b></td>
      </tr>

      <tr class="vocab_group">
        <td class="title" width="25%">Foundation :</td>
        <td>honest <font color="red">(+ve)</font></td>
        <td>funny <font color="red">(+ve)</font></td>
        <td>quiet</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>

      <tr class="vocab_group">
        <td>&nbsp;</td>
        <td>kind <font color="red">(+ve)</font></td>
        <td>cheerful <font color="red">(+ve)</font></td>
        <td>shy</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>

      <tr class="vocab_group">
        <td>&nbsp;</td>
        <td>outgoing <font color="red">(+ve)</font></td>
        <td>friendly <font color="red">(+ve)</font></td>
        <td>naughty <font color="red">(-ve)</font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>

      <tr class="vocab_group">
        <td>&nbsp;</td>
        <td>helpful <font color="red">(+ve)</font></td>
        <td>sporty <font color="red">(+ve)</font></td>
        <td>selfish <font color="red">(-ve)</font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>

      </tr>

      <tr class="vocab_group">
        <td class="title">Advanced :</td>
        <td>considerate <font color="red">(-ve)</font></td>
        <td>hardworking <font color="red">(+ve)</font></td>
        <td>supportive  <font color="red">(+ve)</font></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
</table>


<table border="0">
      <tr>
        <td colspan="6"><b>Family members</b></td>
      </tr>

      <tr class="vocab_group">
        <td class="title" width="25%">Foundation :</td>
        <td>aunt</td>
        <td>uncle</td>
        <td>grandfather</td>
        <td>grandmother</td>
        <td>&nbsp;</td>
      </tr>

      <tr class="vocab_group">
        <td>&nbsp;</td>
        <td>brother</td>
        <td>sister</td>
        <td>mother</td>
        <td>father</td>
        <td>&nbsp;</td>
      </tr>

      <tr class="vocab_group">
        <td class="title">Advanced :</td>
        <td>elder brother</td>
        <td>elder sister</td>
        <td>younger brother</td>
        <td>younger sister</td>
        <td>&nbsp;</td>
      </tr>
      
      <tr class="vocab_group">
        <td>&nbsp;</td>
        <td>son</td>
        <td>daughter</td>
        <td>nephew</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
</table>

<br style="clear:both;" />
<table border="0">
      <tr>
        <td>Online dictionary: <a class="tablelink" href="javascript:void(0);" onclick="window.open('http://oxforddictionaries.com/');">http://oxforddictionaries.com/</a></td>
      </tr>
</table>