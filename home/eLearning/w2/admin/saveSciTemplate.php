<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentGenerator.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$cid = trim($_POST['cid']);
$r_contentCode = trim($_POST['r_contentCode']);
$saveTab = isset($_POST['saveTab'])?trim($_POST['saveTab']):exit;
$parseData = $engContentParser->parseEngContent($cid);
### DB Control Info
$schemeNum= $objEngContentBuilder->getNextSchemeNum($r_contentCode);
if($r_contentCode!='sci'){exit;}
//debug_r($_POST);exit;
try 
{
	
	switch($saveTab)
	{
		case 0:
			### General column
			$topicIntro = isset($_POST['topicIntro'])?trim($_POST['topicIntro']):exit;
			$topicName = isset($_POST['topic'])?trim($_POST['topic']):exit;
			$category =  isset($_POST['category'])?intranet_htmlspecialchars($Lang['W2']['SciThemeAry'][$_POST['category']]):exit;
			$grade =  isset($_POST['grade'])?trim($_POST['grade']):exit;			

			if( empty($cid) )
			{
				$buildData[]= $objEngContentBuilder->buildChiGeneralInfoInsert($topicIntro,$topicName,$category,$grade); //same as chi
			}
			else
			{
				$buildData[]= $objEngContentBuilder->buildChiGeneralInfoUpdate($topicIntro,$topicName,$category,$grade);
			}
			break;
		case 1:
			### Step1 column
			$Step1Data = array();
			$Step1Data['step1Int'] =  isset($_POST['step1Int'])?trim($_POST['step1Int']):exit;
			$Step1Data['step1Purpose'] =  isset($_POST['step1Purpose'])?intranet_htmlspecialchars($Lang['W2']['PurposeAry'][$_POST['step1Purpose']]):exit;
			$Step1Data['step1Audience'] = isset($_POST['step1Audience'])?trim($_POST['step1Audience']):exit;
			$Step1Data['step1TeacherVocab'] = isset($_POST['step1TeacherVocab'])?trim($_POST['step1TeacherVocab']):exit;
			$buildData[]= $objEngContentBuilder->buildChiStep1($Step1Data); // same as chi
			break;
		case 2:
			### Step2 column
			$step2Int = isset($_POST['step2Int'])?trim($_POST['step2Int']):exit;
			$step2ArticleAry  = array();
			for($i=0;$i<$Cnt_step2Article;$i++){
				$_paragraphCnt = $_POST['r_step2_'.$i.'ParagraphCounter'];
				$step2ArticleAry['articleItem'.$i] = array();
				for($j=1;$j<=$_paragraphCnt;$j++){
					$__title = isset($_POST['r_step2_'.$i.'BubbleTitle_'.$j])?addslashes(intranet_htmlspecialchars(trim($_POST['r_step2_'.$i.'BubbleTitle_'.$j]))):'';
					$__content = isset($_POST['r_step2_'.$i.'BubbleContent_'.$j])?addslashes(intranet_htmlspecialchars(trim($_POST['r_step2_'.$i.'BubbleContent_'.$j]))):'';
					$__paragraph = trim($_POST['r_step2_'.$i.'editorParagraph'.$j]);
					if(!empty($__paragraph)&&$__paragraph!='&nbsp;'){
						$step2ArticleAry['articleItem'.$i]['paragraphItem'.$j] = array('leftBubbleTitle'=>$__title,'leftBubbleContent'=>$__content,'paragraph'=>$__paragraph);
					}
				}
			}			
			$buildData[] = $objEngContentBuilder->buildSciStep2($step2Int,$step2ArticleAry);
			break;		
		case 3:
			### Step3 column
			$step3Data = $parseData['step3Data'];			
			$step3Int = isset($_POST['step3Int'])?trim($_POST['step3Int']):exit;
			$buildData[] = $objEngContentBuilder->buildSciStep3($step3Int,$step3Data['step3RefItemAry']);
			if($parseData['status']==W2_GENERATED){
				$W2EngContentGenerator=new libW2GenerateEngContent($PATH_WRT_ROOT);
				$W2EngContentGenerator->getMindMapGeneration($parseData, $w2_classRoomDB,$eclass_db);
			}
			break;
		case 4:
			### Step4 column
			$step4Data = $parseData['step4Data'];
			$step4Data['step4Int'] = isset($_POST['step4Int'])?trim($_POST['step4Int']):exit;
			$step4Data['step4Note'] = isset($_POST['step4Note'])?addslashes(intranet_htmlspecialchars(trim($_POST['step4Note']))):'';
			$paragraphCounter = isset($_POST['r_step4ParagraphCounter'])?trim($_POST['r_step4ParagraphCounter']):exit;

			$step4Data['step4ParagraphAry']  = array();
			for($i=1;$i<=$paragraphCounter;$i++){
				$_title = isset($_POST['r_step4BubbleTitle_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['r_step4BubbleTitle_'.$i]))):'';
				$_content = isset($_POST['r_step4BubbleContent_'.$i])?addslashes(intranet_htmlspecialchars(trim($_POST['r_step4BubbleContent_'.$i]))):'';
				$_paragraph = trim($_POST['r_step4editorParagraph'.$i]);
				if(!empty($_paragraph)){
					$step4Data['step4ParagraphAry']['paragraphItem'.$i] = array('leftBubbleTitle'=>$_title,'leftBubbleContent'=>$_content,'paragraph'=>$_paragraph);
				}							
			}
			$step4Data['step4RemarkAry']  = array();
			for($i=0;$i<$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
				$_title = addslashes(intranet_htmlspecialchars(trim($_POST['r_step4RemarkTitle_'.$i])));
				$_content = addslashes(intranet_htmlspecialchars(trim($_POST['r_step4RemarkContent_'.$i])));
				if(!empty($_title)||!empty($_content))
					$step4Data['step4RemarkAry']['remarkItem'.$i] = array('remarkTitle'=>$_title,'remarkContent'=>$_content);		
				else
					$step4Data['step4RemarkAry']['remarkItem'.$i] = array();									
			}	
			$buildData[]= $objEngContentBuilder->buildSciStep4($step4Data);
			
			break;
		case 5:
			### Step5 column
			$step5Int = isset($_POST['step5Int'])?intranet_htmlspecialchars(trim($_POST['step5Int'])):exit;
			$buildData[]= $objEngContentBuilder->buildSciStep5($step5Int,$step5OutlineQuestion);
			
			break;			
		case 6:
			### Step6 column
			$step6Int = isset($_POST['step6Int'])?trim($_POST['step6Int']):exit;
			$step6DefaultWriting = isset($_POST['step6DefaultWriting'])?trim($_POST['step6DefaultWriting']):exit;
			$buildData[]= $objEngContentBuilder->buildSciStep6($step6Int,$step6DefaultWriting);
			break;
		case 7:
			$resourceName = $_POST['resource'];
			$resourceWebsite = $_POST['website'];
			
			$resourceCount = count($resourceName);
			for($i=0; $i<$resourceCount; ++$i)
			{
				$tempObj=null;
				if( trim($resourceName[$i])=='')
				{
					continue;
				}
				$tempObj['resourceName'] = trim($resourceName[$i]);
				$tempObj['resourceWebsite'] = trim($resourceWebsite[$i]);
				
				for($c=0; $c<5; ++$c)
				{
					if($_POST["r".($i+1)."_step".($c+1)])
					{
						$tempObj['checked'][]='c'.($c+1);
					}
				}
				$refObj[] = $tempObj;

			}
			$buildData[]= $objEngContentBuilder->buildResource($refObj);
			break;
		default:
			break;
	}
	
	if( empty($cid) )
	{
			$buildData[]= $objEngContentBuilder->buildDBInfoInsert($r_contentCode,$schemeNum);	
			$sql = 'INSERT INTO W2_CONTENT_DATA
			('
			.$buildData[0]['field'].','
			.$buildData[1]['field'].
			
			') VALUES
			('
			.$buildData[0]['content'].','
			.$buildData[1]['content'].
			');';
			

			if($saveTab==3){
				$buildData[2]['field'] = 'conceptType';
				$buildData[2]['content'] = "'$conceptType'";
			
				$sql = 'INSERT INTO W2_CONTENT_DATA
				('
				.$buildData[0]['field'].','
				.$buildData[1]['field'].','
				.$buildData[2]['field'].
				
				') VALUES
				('
				.$buildData[0]['content'].','
				.$buildData[1]['content'].','
				.$buildData[2]['content'].
				');';
			}
	}
	else
	{
			if($saveTab==0)
			{
				$data = $buildData[0];
			}
			else
			{
				$data = $buildData[0]['field'].'='.$buildData[0]['content'];
			}
			$buildData[]= $objEngContentBuilder->buildDBInfoUpdate($r_contentCode,$schemeNum);
			
			if($saveTab==3){
				$buildData[2]['field'] = 'conceptType';
				$buildData[2]['content'] = "'$conceptType'";
				$data = $buildData[0]['field'].'='.$buildData[0]['content'].','.
						$buildData[2]['field'].'='.$buildData[2]['content'];	
			}		
			$sql = 'UPDATE W2_CONTENT_DATA SET '.			
			$data.','.
			implode(",", (array)$buildData[1]).
			' WHERE cid=\''.$cid.'\';';
			
	}
	$rs = $objDB->db_db_query($sql);

	
	if($rs)
	{
		$cid = empty($cid)?$objDB->db_insert_id():$cid;
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=newSciTemplateWriting&r_contentCode=$r_contentCode&cid=$cid&r_returnMsgKey=AddSuccess&tabNum=$saveTab");
	}
} 
catch (Exception $e) 
{
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}


#####################Helper function #####################################################
function isInjectionCodePresent($stringList)
{
	foreach($stringList as $string)
	{
		if (strpos($string, '<?php') !== false)
	    {
	    	return true;
	    }
	}
}

?>