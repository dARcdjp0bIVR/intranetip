<?
// using:

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");

$r_contentCode = trim($_GET['r_contentCode']);
$cid = trim($_GET['cid']);
$tabNum = isset($_GET['tabNum'])?$_GET['tabNum']:-1;
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$ls_lang = ($r_contentCode=='ls_eng')?'eng':'chi';
### Page title
$h_title = strtoupper($cid ==''?$Lang['W2']['newTemplate']:$Lang['W2']['editTemplate']);	

### Get template information from database
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$conceptType = $parseData['conceptType'];
	$powerConceptID = $parseData['powerConceptID'];
	$powerBoardID = $parseData['powerBoardID'];
	$attachment = $parseData['attachment'];
	$grade = $parseData['grade'];
	$teacherAttachmentData = $parseData['teacherAttachmentData'];
	$powerConceptCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?'checked':'';
	$powerBoardCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'checked':'';
	$attachmentCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]?'checked':'';		
}
$h_gradeSelection = $w2_libW2->getGradeSelection($grade);

$objDB = new libdb();

### Topic & Theme - TextType
$textTypeSelectionBoxItems = getCategoryItems("category","category",$parseData['category']);

### Step 1 TextType
$h_selectStep1TextTypeHTML =  getStep1TextTypeItems("step1TextType","step1TextType",$parseData['step1Data']['step1TextType']);

### Step 2 Terms and Information
$h_step2SourceHTML = getStep2SourceArrayGenerated('step2Source',$parseData['step2Data']['step2SourceAry'],$parseData['status']);

### Step 4 Outline
$h_step4OutlineHTML = getStepQuestionArrayGenerated('step4Outline',$parseData['step4Data']['step4OutlineAry'],$parseData['status']);

### Resource case
$resourceCounter = 1;
$getResourceArrayHTML =  getResrouceTableItemsGenerated ($parseData['resourceData'], $resourceCounter);


### FCK editor objects
$FCKEditor = new FCKeditor ( 'step1Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step1Data']['step1Int'];
$step1InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step2Int' , "100%", "180", "", "", "");
$FCKEditor->Value = stripslashes($parseData['step2Data']['step2Int']);
$step2InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step3Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Int'];
$step3InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step4Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step4Data']['step4Int'];
$step4InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step4ExampleSentence' , "75%", "200", "", "", "");
$FCKEditor->Value = $parseData['step4Data']['step4ExampleSentence'];
$step4ExampleSentenceEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step5Data']['step5Int'];
$step5InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5DefaultWriting' , "100%", "300", "", "", "");
$FCKEditor->Value = !empty($parseData['step5Data']['step5DefaultWriting'])?$parseData['step5Data']['step5DefaultWriting']:"";
$step5DefaultWritingEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

### Set level
$isBasicSelected = $parseData['level']=='Basic'?'checked':'';
$isAdvancedSelected = $parseData['level']=='Advanced'? 'checked':'';

### button panel
$h_cancelButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'doCancel()', 'formsubbutton', ' onmouseover="this.className=\'formsubbuttonon\'" onmouseout="this.className=\'formsubbutton\'" ',0,'formsubbutton');
$h_saveButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', 'doFormCheck(\'-1\');', 'formbutton', 'onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'"',0,'formbutton');
$h_addItemButton = "<a href='#' onclick='addDraftField();return false;' id='addItemBtn' class='new'>Add more</a>";

$h_step2AddMoreButton = "<a href='javascript:void(0);' onclick='addStep2SourceField();return false;' id='step2SourceAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";
$h_step4AddMoreButton = "<a href='javascript:void(0);' onclick='addStepQuestionField(\"step4Outline\",false);return false;' id='step4OutlineAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";


### Form checking warning messages
// divId = {inputId}_warningDiv
$h_inputTopicNameWarning = $linterface->Get_Form_Warning_Msg('r_TopicName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_inputTopicIntroWarning = $linterface->Get_Form_Warning_Msg('r_TopicIntro_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_selectCategoryWarning = $linterface->Get_Form_Warning_Msg('r_SelectedCategory_warningDiv', $Lang['W2']['warningMsgArr']['selectCat'], 'warningDiv_0');
$h_selectLevelWarning = $linterface->Get_Form_Warning_Msg('r_SelectedLevel_warningDiv', $Lang['W2']['warningMsgArr']['selectLevel'], 'warningDiv_0');

$h_inputStep1IntWarning = $linterface->Get_Form_Warning_Msg('r_Step1Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1KeywordsWarning = $linterface->Get_Form_Warning_Msg('r_Step1Keywords_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1PointsWarning = $linterface->Get_Form_Warning_Msg('r_Step1PointsWarning_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');

$h_inputStep2IntWarning = $linterface->Get_Form_Warning_Msg('r_Step2Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');
$h_inputStep2SourceWarning = $linterface->Get_Form_Warning_Msg('r_Step2Source_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');
$h_inputStep2SourceImageWarning = $linterface->Get_Form_Warning_Msg('r_Step2SourceImage_warningDiv', $Lang['W2']['jsWarningAry']['wrongImageFormat'], 'warningDiv_2');

$h_inputStep3IntWarning = $linterface->Get_Form_Warning_Msg('r_Step3Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');

$h_inputStep4IntWarning = $linterface->Get_Form_Warning_Msg('r_Step4Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');
$h_inputStep4OutlineWarning = $linterface->Get_Form_Warning_Msg('r_Step4Outline_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');

$h_inputStep5IntWarning = $linterface->Get_Form_Warning_Msg('r_Step5Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');
$h_inputStep5DefaultWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step5DefaultWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');

$h_inputStep6WebNameWarning = $linterface->Get_Form_Warning_Msg('r_Step6WebName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_6');
$h_inputStep6WebsiteWarning = $linterface->Get_Form_Warning_Msg('r_Step6Website_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_6');

$h_submissionInvalidWarning = $linterface->Get_Form_Warning_Msg('submissionInvalidWarnMsg', $Lang['General']['JS_warning']['InvalidDateRange'], 'warningDiv');
$h_mustUpdatePeerMarkingSettingsWarning = $linterface->Get_Form_Warning_Msg('mustUpdatePeerMarkingSettingsWarnDiv', $Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'], 'warningDiv');


### content
ob_start();
include('templates/'.$mod.'/'.$task.'.tmpl.php');
$html_content = ob_get_contents();
ob_end_clean();

//Logic function helper layer
#####################################################################################################
function getResrouceTableItemsGenerated ($resourceList, &$resourceCounter)
{
	$resourceHTML ='';
	$resourceList = $resourceList['resource'];
	

	foreach( (array) $resourceList as $resourceItem)
	{
		$_website = !empty($resourceItem['resourceItemWebsite'])?$resourceItem['resourceItemWebsite']:'';
		$_itemName = !empty($resourceItem['resourceItemName'])?$resourceItem['resourceItemName']:'';
		$resourceHTML .= "<tr class='resourceArray'>"."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:95%' name='resource[]' class='resource' id='resource_{$resourceCounter}' value='".$_itemName."' data-resourceID = '{$resourceCounter}'/>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:100%' name='website[]' id='website_{$resourceCounter}' class='webiste' value='".$_website."' data-resourceID = '{$resourceCounter}' />"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<span class='table_row_tool'><a href='#' class='deleteResourceBtn tool_delete_dim' title='Delete'>&nbsp;</a></span>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";

		$resourceHTML .= '<td>'."\n\r";
		for ($i=0; $i<5; ++$i)
		{
			
			$isChecked = '';
			if ( $resourceItem["resourceItemChecked"]['c'.($i+1)] )
			{
				$isChecked = 'checked';
			}
			$resourceHTML .= ('<label for="step'.($i+1).'">'.($i+1).'</label><input type="checkbox" id="step'.($i+1).'" name="r'.$resourceCounter.'_step'.($i+1).'" class="step" '.$isChecked.' />&nbsp;'."\n\r" );
		}
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '</tr>'."\n\r";
		++$resourceCounter;
	}
	return $resourceHTML;
}

function getCategoryItems($name, $id,$defaultValue='')
{
	global $Lang;
	$defaultKey = array_search(intranet_undo_htmlspecialchars($defaultValue),$Lang['W2']['LSThemeAry']);
	$returnStr='';
	$returnStr .= '<select id="'.$id.'" name="'.$name.'" >';
	$returnStr .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	foreach ( (array)$Lang['W2']['LSThemeAry'] as $_key => $_value){
		$isSelectedHTML = ("$defaultKey" == "$_key")?'selected':'';
		$returnStr .= '<option value="'.$_key.'" '.$isSelectedHTML.' >'.$_value.'</option>';
	}
	$returnStr .= '</select>';
	return $returnStr;
}
function getStep1TextTypeItems($name,$id,$defaultValue='')
{
	global $Lang;
	$textTypeAry = !empty($defaultValue)?explode(',',$defaultValue):array();
	$returnStr='';
	foreach ( (array)$Lang['W2']['textTypeChoiceAry'] as $_key => $_value){
		$isSelectedHTML = (in_array($_key,$textTypeAry))?'checked':'';
		$returnStr .= '<input type="checkbox" name="'.$name.'[]" id="'.$id.'_'.$_key.'" value="'.$_key.'" '.$isSelectedHTML.'/>';
		$returnStr .= '<label for="'.$id.'_'.$_key.'">&nbsp;'.$_value.'</label>';
		$returnStr .= '&nbsp;';
	}
	return $returnStr;
}
function getStep2SourceArrayGenerated($name,$stepQuestion,$status){
	global $Lang,$w2_cfg,$cid,$intranet_root,$PATH_WRT_ROOT,$LAYOUT_SKIN;
	$html = '';
	$qCnt = (count($stepQuestion)>0)?count($stepQuestion):$w2_cfg['contentInput']['defaultInputNum'];
	$tarPath = '/file/w2/reference/cid_'.$cid;
	for($i=0;$i<$qCnt;$i++){
		$_stepName = $name.$i;
		$_sourceAry = $stepQuestion['sourceItem'.$i];
		$_image = $tarPath.'/'.$_sourceAry['image'];
		list($_timeStamp,$_imageName) = explode('/',$_sourceAry['image']);
		$_imageHtml = '';
		if(file_exists($intranet_root.$_image)&&!empty($_sourceAry['image'])){
			$h_deleteAction = '<a href="javaScript:void(0)" class="tool_delete_dim" title="'.$Lang['Btn']['Delete'].'" onclick="removeSourceImage('.$i.')">&nbsp;</a>';
			$_imageHtml .= '<div id="w2_sourceImage'.$i.'" style="float:left">';
			$_imageHtml .= '<table>';
			$_imageHtml .= '<tr>';
			$_imageHtml .= '<td>'.$Lang['W2']['imageFile'].': </td>';
			$_imageHtml .= '<td><img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/w2/icon_attachment2.gif"></td>';
			$_imageHtml .= '<td><a href="'.$_image.'" target="_blank">'.$_imageName.'</a></td>';
			$_imageHtml .= '<td class="table_row_tool">'.$h_deleteAction.'</td>';
			$_imageHtml .= '</tr>';
			$_imageHtml .= '</table>';
			$_imageHtml .= '</div>';
		}else{
			$_imageHtml .= '<div id="w2_sourceImage'.$i.'" style="float:left">';
			$_imageHtml .= '<table>';
			$_imageHtml .= '<tr>';
			$_imageHtml .= '<td>'.$Lang['W2']['imageFile'].': </td>';
			$_imageHtml .= '<td><input type="file" id="'.$name.'Image'.$i.'" name="'.$name.'Image'.$i.'" style="width:190px" class="sourceImage"></td>';
			$_imageHtml .= '</tr>';
			$_imageHtml .= '</table>';
			$_imageHtml .= '</div>';			
		}
		$html .= '<div>';
		$html .='<div id="div_'.$name.'[]" class="infobox" style="width:70%;line-height: 20px;">';
			$html .= '<a class="top" href="javascript:void(0);">'.$Lang['W2']['source'].' '.($i+1).'</a>';
			$html .= '<div class="infobox_content">';
				$html .= $_imageHtml;
				$html .='<br style="clear:both;">';
				$html .= '<span class="ques">'.$Lang['W2']['checkPoint'].'</span>';
				$html .='<div class="ques_content">';
					$html .='<div id="'.$_stepName.'Array">';
						$html.= getStepQuestionArrayGenerated($_stepName,$_sourceAry['questionAry'],$status,true);
					$html .='</div>';
					$html .= "<span class=\"Content_tool_190\"><a href='javascript:void(0);' onclick='addStepQuestionField(\"".$_stepName."\",true);return false;' id='".$_stepName."AddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a></span>";
       				$html .='<br style="clear:both;">';
       			$html .='</div>';
			$html .='</div>';
		$html .='</div>';
		$html .='<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="'.$Lang['Btn']['Delete'].'">&nbsp;</a></span>';
		$html .= '</div>';
	}
	$html .= '<input type="hidden" name="Cnt_'.$name.'" id="Cnt_'.$name.'" value="'.$qCnt.'">';
	return ( empty($html)==true)?'':$html;	
}
function getStepQuestionArrayGenerated($name,$stepQuestion,$status,$withAnswer=false)
{
	global $Lang,$w2_cfg;
	$draftQHTML=''; 
	$qCnt = (count($stepQuestion)>0)?count($stepQuestion):$w2_cfg['contentInput']['defaultInputNum'];
	for($i=0;$i<$qCnt;$i++){
		$_question = $withAnswer?$stepQuestion['item'.$i]['question']:$stepQuestion['question'.$i]; 
		$_answer = $withAnswer?$stepQuestion['item'.$i]['answer']:'';
		$draftQHTML .='<div id="div_'.$name.'[]">';
			$draftQHTML .='<span style="float:left;width:80px;">'.$Lang['W2']['question'].': </span> <textarea name="'.$name.'Question[]" rows="3" class="'.$name.' textbox" style="width:70%; float:left">'.(!empty($_question)?stripslashes($_question):'').'</textarea>';
			$draftQHTML .='<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="'.$Lang['Btn']['Delete'].'">&nbsp;</a></span>';
			$draftQHTML .='<br style="clear:both;">';
		if($withAnswer){
			$draftQHTML .='<span style="float:left;width:80px;">'.$Lang['W2']['answer'].': </span> <textarea name="'.$name.'Answer[]" rows="3" class="'.$name.' textbox" style="width:70%; float:left">'.(!empty($_answer)?stripslashes($_answer):'').'</textarea>';
			$draftQHTML .='<br style="clear:both;">';
			$draftQHTML .='<br style="clear:both;">';
		}
		$draftQHTML .='</div>';
	}
	$draftQHTML .= '<input type="hidden" name="Cnt_'.$name.'" id="Cnt_'.$name.'" value="'.$qCnt.'">';
	return ( empty($draftQHTML)==true)?'':$draftQHTML;
}

function _frameLayout($title , $content,$bottom){
	global $Lang;
	$html = '<form action ="/home/eLearning/w2/index.php" method = "post" id="w2_form" name="w2_form" enctype="multipart/form-data">';
	$html .= '<div class="main_top_blank"> <!-- for blank div 20140113 -->';
	$html .= '</div>';
	$html .= '<p class="spacer"></p>';   
	$html .= '<div class="themename_grp"> <!-- New themename group 20140113 -->';
		$html .= '<span class="themename_icon"></span>';
		$html .= '<span class="themename_main">'.$Lang['W2']['topic'].': <strong id="topicHeader" data-originalText="('.$Lang['W2']['topicName'].')">('.$Lang['W2']['topicName'].')</strong></span>';
	$html .= '</div>';
	$html .= '<br />';
	$html .= '<div class="write_board write_board_input">';
		//$html .= '<div class="content_input">';
			$html .= $content;
			$html .= '<div class="write_board_bottom_left">';
				$html .= '<div class="write_board_bottom_right"></div>';
			$html .= '</div>';
		//$html .= '</div>';
	$html .= '</div>';
	$html .= '</form>	';		


	return $html;
}

function escapeJavaScriptText($string)
{
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}

//Presentation layer


#####################################################################################################
$str =<<<HTML
HTML;
$linterface->LAYOUT_START($returnMsg);

echo _frameLayout($h_title,$html_content,'');
$linterface->LAYOUT_STOP();

exit();
?>