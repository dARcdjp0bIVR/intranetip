<?
// using:
//include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2PublishMenuGenerator.php");
$r_contentCode = $_GET['r_contentCode'];
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$cidList = ($_POST['allSch']=='')?exit():$_POST['allSch'];

GLOBAL $w2_cfg;
$objDb = new libdb();
$objW2PublishMenuGenerator =new W2PublishMenuGenerator();
$publishList = $objW2PublishMenuGenerator->getPublishMenu($r_contentCode,false,$cidList);

### Toolbar
if ($w2_libW2->isAdminUser()) {
}

### Table

$schCount = count($w2_cfg["schoolCode"])-1;
## Table Width
$h_publishTableWidthSetter='';
$h_publishTableWidthSetter.='<col width="1%" />'."\n\r";
$h_publishTableWidthSetter.='<col width="1%" />'."\n\r";
$h_publishTableWidthSetter.='<col nowrap="nowrap" width="38%" />'."\n\r";
for ($i=0; $i<$schCount;$i++)
{
	$h_publishTableWidthSetter.='<col nowrap="nowrap" width="15%" />'."\n\r";
}

foreach( (array)$publishList as $publishItem)
{
	$schemeNumList[]=$publishItem;
}
## Table Header
$h_publishTableHeader='';
$h_publishTableHeader .= "<th>#</th>"."\n\r";
$h_publishTableHeader .= "<th></th>"."\n\r";

$h_publishTableHeader .= "<th>".$Lang['W2']['topic']."</th>"."\n\r";
foreach( (array)$w2_cfg["schoolCode"] as $schoolDetail)
{
	if($schoolDetail["shortDesc"] != $w2_cfg["schoolCodeCurrent"])
	{
		$h_publishTableHeader .= '<th>'.$schoolDetail["shortDesc"].'<br />';
		$h_publishTableHeader .= "<input type='checkbox' class='selectAllContent' id='${schoolDetail[shortDesc]}_All' data-value='to_". strtolower($schoolDetail["shortDesc"])."' />";
		$h_publishTableHeader .= '</th>'."\n\r";
	}
}

## Table Content
$h_publishTableContent='';

$colNum = 1;
$checkAllSch='';
foreach( (array)$publishList as $publishItem)
{
	$checkAllSch = "<input type='checkbox' class='selectAllSch' id='scheme${publishItem[schemeNum]}_All' data-value='scheme_${publishItem[schemeNum]}' />";
	
	$h_publishTableContent.="<tr class='normal record_bottom'>";
	$h_publishTableContent .= "<td>".$colNum++."</td>"."\n\r";
	$h_publishTableContent .= "<td>".$checkAllSch."</td>"."\n\r";
	$h_publishTableContent .= "<td>".$publishItem["topicName"]."</td>"."\n\r";
	
	foreach( $w2_cfg["schoolCode"] as $schoolDetail)
	{
		if($w2_cfg["schoolCodeCurrent"] != $schoolDetail["shortDesc"])
		{
			$shortDesc = strtolower($schoolDetail["shortDesc"]);
			if ( $publishItem["${shortDesc}Sent"] == true)
			{
				$selectStatus= "Done";
			}
			else
			{
				//value: CID _ SCHEMENUM _ SCHDESC
				$selectStatus= "<input type='checkbox'
						class='scheme_${publishItem[schemeNum]} to_${shortDesc}'
						id='sendScheme${publishItem[schemeNum]}To${shortDesc}'
						value='${publishItem[cid]}|${shortDesc}' name='schemeSelectArray[]'/>";
			}
			$h_publishTableContent .= "<td>".$selectStatus."</td>"."\n\r";
		}
	}
	$h_publishTableContent .="</tr>";
	
}
###

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/admin/publishSelectContent.tmpl.php');
$linterface->LAYOUT_STOP();
?>