<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2Content.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$isCreateNewRecord = false;
if(is_numeric($r_writingId) && $r_writingId > 0) {
	//$objWriting = new libWriting($r_writingId);
	$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
}else{

	$isCreateNewRecord = true;
	//$objWriting = new libWriting();
	$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"]);
}



$objDb = new libdb();
$json = new JSON_obj();

$r_writingName = trim(stripslashes($r_writingName));
$r_introduction = trim(stripslashes($r_introduction));
$r_schemeCode = trim(stripslashes($r_schemeCode));
$r_contentCode = trim($r_contentCode);
$r_conceptMap = trim($r_conceptMap);
$r_startDate = trim($_POST['r_startDate']);
$r_startHour = trim($_POST['r_startHour']);
$r_startMin = trim($_POST['r_startMin']);
$startDateTime = $r_startDate.' '.$r_startHour.':'.$r_startMin.':00';

$r_endDate = trim($_POST['r_endDate']);
$r_endHour = trim($_POST['r_endHour']);
$r_endMin = trim($_POST['r_endMin']);
$endDateTime = $r_endDate.' '.$r_endHour.':'.$r_endMin.':59';

$r_templateID = $_POST['r_templateID'];
$r_clipArtID = $_POST['r_clipArtID'];

$r_recordStatus = trim($_POST['r_recordStatus']);

$r_markingInfoAry = $json->decode(trim(stripslashes($_POST['r_markingJsonString'])));
$r_allowPeerMarking = $r_markingInfoAry['r_allowPeerMarking'];
$r_peerMarkingStartDate = $r_markingInfoAry['r_peerMarkingStartDate'].' 00:00:00';
$r_peerMarkingEndDate = $r_markingInfoAry['r_peerMarkingEndDate'].' 23:59:59';
$r_peerMarkingTargetNum = $r_markingInfoAry['r_peerMarkingTargetNum'];
$r_showPeerName = $r_markingInfoAry['r_showPeerName'];
$r_teacherWeight = $r_markingInfoAry['r_teacherWeight'];
$r_selfWeight = $r_markingInfoAry['r_selfWeight'];
$r_peerWeight = $r_markingInfoAry['r_peerWeight'];
$r_allowSelfMarking = $r_markingInfoAry['r_allowSelfMarking'];
$r_fullMark = $r_markingInfoAry['r_fullMark'];
$r_passMark = $r_markingInfoAry['r_passMark'];
$r_lowestMark = $r_markingInfoAry['r_lowestMark'];
$r_peerMarkingGroupingAry = $r_markingInfoAry['r_peerMarkingGroupingAry'];



$originalPeerMarkingTargetNum = $objWriting->getPeerMarkingTargetNum();

$objContent = libW2ContentFactory::createContent($r_contentCode);


$objWriting->setTitle($r_writingName);
$objWriting->setIntroduction($r_introduction);
$objWriting->setContentCode($r_contentCode);
$objWriting->setSchemeCode($r_schemeCode);
$objWriting->setWithDefaultConceptMap($r_conceptMap);
$objWriting->setStartDateTime($startDateTime);
$objWriting->setEndDateTime($endDateTime);
$objWriting->setRecordStatus($r_recordStatus);
$objWriting->setInputBy($w2_thisUserID);

### Peer Marking Related Settings
$objWriting->setAllowPeerMarking($r_allowPeerMarking);
$objWriting->setPeerMarkingStartDate($r_peerMarkingStartDate);
$objWriting->setPeerMarkingEndDate($r_peerMarkingEndDate);
$objWriting->setPeerMarkingTargetNum($r_peerMarkingTargetNum);
$objWriting->setShowPeerName($r_showPeerName);

$objWriting->setAllowSelfMarking($r_allowSelfMarking);
$objWriting->setFullMark($r_fullMark);
$objWriting->setPassMark($r_passMark);
$objWriting->setLowestMark($r_lowestMark);
$objWriting->setTeacherWeight($r_teacherWeight);
$objWriting->setSelfWeight($r_selfWeight);
$objWriting->setPeerWeight($r_peerWeight);

$objWriting->setTemplateID($r_templateID);
$objWriting->setClipArtID($r_clipArtID);

$writingId = $objWriting->save();
//debug_r($r_selectedStudentId);
//debug_r($r_selectedTeacherId);
//debug_r(count($r_selectedStudentId));
//debug_r(count($r_selectedTeacherId));


### Add students and teachers to writing
$objWriting->handleStudentToWriting($r_selectedStudentId,$w2_thisUserID);
$objWriting->handleTeacherToWriting($r_selectedTeacherId,$w2_thisUserID);

### Peer Marking Grouping
$needUpdatePeerMarkingData = false;
if ($w2_libW2->isMatchedSavedPeerMarkingGrouping($writingId, $r_peerMarkingGroupingAry)) {
	// no need update peer marking grouping info if the data has not updated
	
	if ($originalPeerMarkingTargetNum == $r_peerMarkingTargetNum) {
		$needUpdatePeerMarkingData = false;
	}
	else {
		$needUpdatePeerMarkingData = true;
	}
}
else {
	$needUpdatePeerMarkingData = true;
	
	$successAry['deleteOldPeerMarkingGroup'] = $w2_libW2->deletePeerMarkingGrouping($writingId);
	
	// $peerMarkingTargetAssoAry[$StudentID] = array of target marking studentId
	$peerMarkingTargetAssoAry = array();
	for ($i=0, $i_max=count($r_peerMarkingGroupingAry); $i<$i_max; $i++) {
		$_studentList = $r_peerMarkingGroupingAry[$i];
		$_studentIdAry = explode(',', $_studentList);
		sort($_studentIdAry, SORT_NUMERIC);
		
		$successAry['saveNewPeerMarkingGroup'][$i] = $w2_libW2->insertPeerMarkingGrouping($writingId, $i, $_studentIdAry);
	}
}

### Peer Marking Data
if ($needUpdatePeerMarkingData) {
	$successAry['deleteOldPeerMarkingTargetData'] = $w2_libW2->deleteStudentPeerMarkingTargetData($writingId);
	
	for ($i=0, $i_max=count($r_peerMarkingGroupingAry); $i<$i_max; $i++) {
		$_studentList = $r_peerMarkingGroupingAry[$i];
		$_studentIdAry = explode(',', $_studentList);
		sort($_studentIdAry, SORT_NUMERIC);
		
		$_groupMarkingTargetNum = $r_peerMarkingTargetNum;
		if($r_peerMarkingTargetNum == -1 || $r_peerMarkingTargetNum == '') {
			$_groupMarkingTargetNum = count((array)$_studentIdAry) - 1;
		}
		
		for ($j=0, $j_max=count($_studentIdAry); $j<$j_max; $j++) {
			$__studentId = $_studentIdAry[$j];
			
			$__peerInfo = remove_item_by_value($_studentIdAry, $__studentId, $preserve_keys=false);
			$__targetMarkingStudentIdAry = $w2_libW2->getStudentPeerMarkingTargetList($__peerInfo, $__studentId, $_groupMarkingTargetNum, array());
		
			$peerMarkingTargetAssoAry[$__studentId] = $__targetMarkingStudentIdAry;
		}
	}
		
	$successAry['saveNewPeerMarkingTargetData'][$i] = $w2_libW2->insertStudentPeerMarkingTargetData($writingId, $peerMarkingTargetAssoAry);
}


if(is_numeric($writingId) && $writingId>0) {
	//do nothing
	//create successfully
}else{
	echo 'failed to create writing!<br/>';
	exit();
}

$stepInfoAry = $w2_libW2->getStepInfoByContent($r_contentCode);

if($isCreateNewRecord){
	// do nothing  , continuous the programm

}else{

	//update step approval status

	for ($i=0, $i_max=count($stepInfoAry); $i<$i_max; $i++) {
		//must check with ivan , duplicate source code
		$_stepNodeTitleEng = $stepInfoAry[$i]['titleEng'];
		$_stepNodeTitleChi = $stepInfoAry[$i]['titleChi'];
		$_stepNodeSequence = $stepInfoAry[$i]['sequence'];
		$_stepNodeCode = $stepInfoAry[$i]['code'];

		$_isRequireApprovalForThisStep = 0;
		if(is_array($r_approvalStep) && count($r_approvalStep) > 0){
			$_isRequireApprovalForThisStep = (in_array($_stepNodeCode,$r_approvalStep))? 1:0 ;
		}
		

		$sql = 'update '.$intranet_db.'.W2_STEP set REQUIRE_APPROVAL = \''.$_isRequireApprovalForThisStep.'\' where WRITING_ID = '.$writingId.' and CODE =\''.$_stepNodeCode.'\'';

		
		$objDb->db_db_query($sql);	
//		error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
	}



	//it is a update writing action , skip the following step and return
	//$url = "/home/eLearning/w2/?task=editWriting&mod=admin&r_contentCode=".$r_contentCode."&r_writingId=".$r_writingId.'&r_returnMsg=1';
	$url = $w2_cfg['modulePath'].'/?task=writingMgmt&mod=writing&r_contentCode='.$r_contentCode.'&r_returnMsgKey=UpdateSuccess';

	header("Location: ".$url);
	exit();
}


for ($i=0, $i_max=count($stepInfoAry); $i<$i_max; $i++) {
	$_stepNodeTitleEng = $stepInfoAry[$i]['titleEng'];
	$_stepNodeTitleChi = $stepInfoAry[$i]['titleChi'];
	$_stepNodeSequence = $stepInfoAry[$i]['sequence'];
	$_stepNodeCode = $stepInfoAry[$i]['code'];

	$_isRequireApprovalForThisStep = 0;
	$_isRequireApprovalForThisStep = $w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["NO"];
	if(is_array($r_approvalStep) && count($r_approvalStep) > 0){
		$_isRequireApprovalForThisStep = (in_array($_stepNodeCode,$r_approvalStep))? $w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["YES"] : $w2_cfg["DB_W2_STEP_REQUIRE_APPROVAL"]["NO"];
	}
	

	$sql = 'insert into '.$intranet_db.'.W2_STEP (WRITING_ID,TITLE_ENG,TITLE_CHI,SEQUENCE,CODE,DATE_INPUT,DATE_MODIFIED,REQUIRE_APPROVAL) value('.$writingId.',\''.$_stepNodeTitleEng .'\',\''.$_stepNodeTitleChi .'\','.$_stepNodeSequence.',\''.$_stepNodeCode.'\',now(),now(),'.$_isRequireApprovalForThisStep.')';

	$objDb->db_db_query($sql);	
}

$conceptMapStepCode = '';
$conceptMapStepCode = $objContent->getConceptMapStepCode();
if(trim($conceptMapStepCode) == ''){
	//do nothing
}else{
	//HANDLE POWER CONCEPT IN ECLASS;
	$statusPublished = 2;
	$dataArray['title']			= $objDb->pack_value($objContent->getConceptMapTitle(), "str");
	$dataArray['instruction']	= $objDb->pack_value('JUST DO IT', "str");
	$dataArray['status']		= $objDb->pack_value($statusPublished, "int");
	$dataArray['createdby']		= $objDb->pack_value($w2_thisUserID, "int");
	$dataArray['inputdate']		= $objDb->pack_value('now()', "date");
	$dataArray['modified']		= $objDb->pack_value('now()', "date");


	# set field and value string
	$fieldArr = array();
	$valueArr = array();
	foreach ($dataArray as $field => $value){
		$fieldArr[] = $field;
		$valueArr[] = $value;
	}
			
	$fieldText = implode(", ", $fieldArr);
	$valueText = implode(", ", $valueArr);

	$sql = 'Insert Into '.$w2_classRoomDB.'.assessment ('.$fieldText.') Values ('.$valueText.')';
//error_log($sql."    e:".mysql_error()."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
	$success = $objDb->db_db_query($sql);

	$assessmentId = $objDb->db_insert_id();

		
	$methodForPowerConcept = 3;

	$dataArray['assessment_id']		= $objDb->pack_value($assessmentId, 'int');
	$dataArray['title']				= $objDb->pack_value($objContent->getConceptMapTitle(), 'str');
	$dataArray['method']			= $objDb->pack_value($methodForPowerConcept, 'int');
	$dataArray['inputdate']			= $objDb->pack_value('now()', 'date');
	$dataArray['modified']			= $objDb->pack_value('now()', 'date');
	$dataArray['instruction']		= $objDb->pack_value($objContent->getConceptMapInstruction(), 'str');

	# set field and value string
	$fieldArr = array();
	$valueArr = array();
	foreach ($dataArray as $field => $value){
		$fieldArr[] = $field;
		$valueArr[] = $value;
	}
			
	$fieldText = implode(", ", $fieldArr);
	$valueText = implode(", ", $valueArr);

	$sql = 'Insert Into '.$w2_classRoomDB.'.task ('.$fieldText.') Values ('.$valueText.')';
//error_log($sql."    e:".mysql_error()."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
	$success = $objDb->db_db_query($sql);

	$taskId = $objDb->db_insert_id();


	//update the taskid to step
	$sql = 'update '.$intranet_db.'.W2_STEP set ECLASS_TASK_ID = '.$taskId.'. where WRITING_ID = '.$writingId.' and CODE = \''.$conceptMapStepCode.'\'';
//error_log($sql."    e:".mysql_error()."<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
	$objDb->db_db_query($sql);
}

header("Location: /home/eLearning/w2/?task=writingMgmt&mod=writing&r_contentCode=".$r_contentCode."&r_returnMsgKey=AddSuccess");
exit();
?>