<?
// using:

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT.'includes/json.php');
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");

$objDB = new libdb();
$json = new JSON_obj();

$r_contentCode = $_GET['r_contentCode'];
//$r_contentCode = 'chi'; // hard code first
$stepInfoAry = $w2_libW2->getStepInfoByContent($r_contentCode);

$objWriting = null;
//$thisIsANewProgram=false; // store this is a new action or not
$h_defaultConceptYes = ' checked ';
$peerMarkingInfoAssoAry = array();
if(is_numeric($r_writingId) && $r_writingId > 0){
	// edit => get writing data to preset default values
	$h_title = strtoupper($Lang['Btn']['Edit']);

	//$objWriting = new libWriting($r_writingId);
	$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);

	$title = $objWriting->getTitle();
	$introduction = $objWriting->getIntroduction();
	$schemeCode = $objWriting->getSchemeCode();
	$h_gradeSelection = $Lang['W2']['na_without_slash'];
	if(strpos($schemeCode,'_')!==false){
		list($schoolCode,$schemeNum) = explode('_',$schemeCode);
		$schemeNum = str_replace('scheme','',$schemeNum);
		$engContentParser = new libW2EngContentParser();
		$parseData = $engContentParser->parseEngContent('',$schoolCode,$schemeNum);

		if(count($parseData)>0){
			$h_gradeSelection = $parseData['grade']?$parseData['grade']:$Lang['W2']['na_without_slash'];
		}
	}
	$contentCode = $objWriting->getContentCode();

	$contentAttributes = $w2_libW2->getContentAttributes($contentCode,$schemeCode);

	$schemeName = $contentAttributes['name'];
	$schemeType = $contentAttributes['type'];

	
	//START HANDLE STEP APPROVAL SETTING
	$thisWritingStepInfo = $w2_libW2->getWritingStep($r_writingId);

	$thisWritingStepApprovalStatus  = array();//store each step code require approval status
	for($i = 0, $i_max = count($thisWritingStepInfo); $i < $i_max; $i++){
		$_stepCode = $thisWritingStepInfo[$i]['STEP_CODE'];
		$_stepApprovalStatus = $thisWritingStepInfo[$i]['STEP_REQUIRE_APPROVAL'];
		$thisWritingStepApprovalStatus[$_stepCode] = $_stepApprovalStatus;
	}

	//if there is a "1" in array "$thisWritingStepApprovalStatus", this mean this writing require step approval
	$thisWritingRequireStepApproval = in_array(1,$thisWritingStepApprovalStatus)?1:0;

	//END HANDLE STEP APPROVAL SETTING

	$w2_objContent = libW2ContentFactory::createContent($contentCode);

	$w2_objContent->setSchemeCode($schemeCode);
	$contentType = $w2_objContent->getUnitName();
	$contentTypeSuper = $w2_objContent->getTypeSuperName();
	
	$teacherList = $objWriting->findTeacherInWriting();
	$writingTeacherIdAry = Get_Array_By_Key($teacherList, 'USER_ID');
	$h_selectedTeacher = '';
	for($i = 0,$i_max = count($teacherList);$i < $i_max;$i++){
		$_useID = $teacherList[$i]['USER_ID'];
		$_useName = $teacherList[$i]['USER_NAME'];
		$h_selectedTeacher .= '<option value="'.$_useID.'">'.$_useName.'</option>';

	}
	
	$studentList = $objWriting->findStudentInWriting();
	$h_selectedStudent = '';
	for($i = 0,$i_max = count($studentList);$i < $i_max;$i++){
		$_useID = $studentList[$i]['USER_ID'];
		$_useName = $studentList[$i]['USER_NAME'];
		$h_selectedStudent .= '<option value="'.$_useID.'">'.$_useName.'</option>';
	}

	$startDate = $objWriting->getStartDate();
	$startHour = $objWriting->getStartHour();
	$startMin = $objWriting->getStartMin();
	$endDate = $objWriting->getEndDate();
	$endHour = $objWriting->getEndHour();
	$endMin = $objWriting->getEndMin();

	##Marking Method	
	$fullMark = $objWriting->getFullMark();
	$passMark = $objWriting->getPassMark();
	$lowestMark = $objWriting->getLowestMark();
	$teacherWeight = $objWriting->getTeacherWeight();
	$selfWeight = $objWriting->getSelfWeight();
	$peerWeight = $objWriting->getPeerWeight();

	$withDefaultConcept = $objWriting->getWithDefaultConceptMap();
	

	if($withDefaultConcept == $w2_cfg["DB_W2_WRITING_WITH_DEFAULT_CONCEPTMAP"]["yes"]){
		$h_defaultConceptYes = ' checked ';
		$h_defaultConceptNo = '';
	}else{
		$h_defaultConceptNo = ' checked ';
		$h_defaultConceptYes = '';
	}
	

	$recordStatus = $objWriting->getRecordStatus();
	if ($recordStatus==$w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["public"]) {
		$recordStatusPublicChecked = true;
		$recordStatusPrivateChecked = false;
	}
	else if ($recordStatus==$w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["private"]) {
		$recordStatusPublicChecked = false;
		$recordStatusPrivateChecked = true;
	}
	
	$h_deleteWritingButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Delete'], 'button', 'deleteScheme();', 'deleteBtn', $ParOtherAttribute="", $ParDisabled=0, "formsubbutton");
//	$h_deleteWritingButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Delete'], 'button', 'deleteScheme();', 'deleteBtn', $ParOtherAttribute="", $ParDisabled=0, "formbutton_alert");

	//$h_deleteWritingButton = '<input name="deleteBtn" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'" value="'.$Lang['Btn']['Delete'].'" onclick="deleteScheme();" />';
	
	$peerMarkingInfoAssoAry = $objWriting->returnWritingAryForJs();
	
	if(strtolower($r_contentCode) == strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){
		$h_select1 = $contentTypeSuper.' > ';
	}
	
	$h_select2 = $contentType.' > ';
	
	$h_selectSchemeCode = $schemeName;
	$h_selectSchemeCode .= '<input type="hidden" id="r_schemeCode" name="r_schemeCode" value="'.$schemeCode.'" />';
	
	# Template Change 
	$templateID =  $objWriting->getTemplateID();
	$clipArtID =  $objWriting->getClipArtID();
	
	$templateID = ($templateID)?$templateID:1;
	$clipArtID = ($clipArtID)?$clipArtID:1;
}
else {
	// new record
	$h_title = strtoupper($Lang['Btn']['New']);
	
	$recordStatusPublicChecked = true;
	$recordStatusPrivateChecked = false;
	$startDate = date('Y-m-d');
	$endDate = date('Y-m-d', strtotime(' +7 day'));
	$endHour = 23;
	$endMin = 59;
	
	$allTitleForThisContent = $w2_libW2->getContentList($r_contentCode);

	$contentAryGroupByType  = $w2_libW2->getContentGroupByType($r_contentCode);
	$contentAryGroupBySuperType  = $w2_libW2->getContentGroupByTypeSuper($r_contentCode);
	
	if(strtolower($r_contentCode) == strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){
	
		$h_select1 = '<select name="contentSuperTypeSlt" id="id_contentSuperTypeSlt" onchange="updateContentTypeSlt(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\'); $(\'#id_contentTypeSlt\').val(\'\'); updateSchemeCodeSlt($(\'#id_contentTypeSlt\').val(\'\'),\''.$r_contentCode.'\')">';
		$h_select1 .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	
	
		foreach($contentAryGroupBySuperType as $_typeSuper => $_Details){
			$_selected = '';
			if($contentTypeSuper != '' && $contentTypeSuper == $_typeSuper){
				$_selected = ' SELECTED ';
			}
			$h_select1 .= '<option value="'.$_typeSuper.'" '.$_selected.'>'.$_typeSuper.'</option>';
		}
		$h_select1 .= '</select>';
	
	}
	$h_gradeSelection = $w2_libW2->getGradeSelection($grade,'onchange="resetThemeSelection();"',$showAll=true);
	if(count($contentAryGroupByType) > 0){	
	//	debug_r($contentAryGroupByType);
		$h_select2 = '<select name="contentTypeSlt" id="id_contentTypeSlt" onchange="updateSchemeCodeSlt(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\')">';
		$h_select2 .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
		//comment the below part to prevent early expose of scheme type
		
		if(strtolower($r_contentCode) == strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])) {
			//Prevent early expose of scheme type in English Writing
		} else {
			
			foreach($contentAryGroupByType as $_type => $_typeDetails){
				$_selected = '';
				if($contentType != '' && $contentType == $_type){
					$_selected = ' SELECTED ';
				}
				$h_select2 .= '<option value="'.$_type.'" '.$_selected.'>'.$_type.'</option>';
			}
		}
		///
		$h_select2 .= '</select>';
	}
	
	$h_selectSchemeCode = '<select class="requiredField" id="r_schemeCode" name="r_schemeCode" onchange="updateSchemeInstruction(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\')">';
	$h_selectSchemeCode .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	
		if(trim($schemeType) == ''){
			//this should be a new one
		}else{
			//this is a edit mode , with a saved scheme type. So, the selection box should display the option (with selected) correctly
			$_selected = '';
			$selectSchemeTypes = $contentAryGroupByType[$schemeType];
			for($i =0,$i_max = count($selectSchemeTypes); $i < $i_max; $i++){
	
	
				$_selected = ($schemeCode == $selectSchemeTypes[$i]['schemeCode']) ? ' SELECTED ': '';
	
				$h_selectSchemeCode .= '<option value= "'.$selectSchemeTypes[$i]['schemeCode'].'" '.$_selected.'>'.$selectSchemeTypes[$i]['name'].'</option>';
			}
		$h_selectSchemeCode .= '';
		}
	
	$h_selectSchemeCode .= '</select>';
	
	if($schemeCode != ''){
		$js_string1 = 'updateSchemeInstruction(\''.$schemeCode.'\',\''.$r_contentCode.'\');';
	}
	##Marking Method	
	$fullMark = W2_MARKING_FULL;
	$passMark = W2_MARKING_PASS;
	$lowestMark = W2_MARKING_LOWEST;
	$teacherWeight = W2_MARKING_TEACHER_WEIGHT;
	$selfWeight = W2_MARKING_SELF_WEIGHT;
	$peerWeight = W2_MARKING_PEER_WEIGHT;
	
	$peerMarkingInfoAssoAry['r_fullMark'] = $fullMark;
	$peerMarkingInfoAssoAry['r_passMark'] = $passMark;	
	$peerMarkingInfoAssoAry['r_lowestMark'] = $lowestMark;	
	$peerMarkingInfoAssoAry['r_teacherWeight'] = $teacherWeight;
	$peerMarkingInfoAssoAry['r_peerWeight'] = $peerWeight;
//	$peerMarkingInfoAssoAry['r_selfWeight'] = $selfWeight;

//	$thisIsANewProgram=true;


	# Template Change
	$templateID = 1;
	$clipArtID = 1;
}
$h_markingDisplay = $w2_libW2->getMarkDisplay($fullMark,$passMark,$lowestMark,$teacherWeight,$selfWeight,$peerWeight);
### Class Selection for selecting target students
$h_year_class_selection = _generateClassSelection();

### Teacher Selection
$fcm = new form_class_manage();
$TeacherList = $fcm->Get_Teaching_Staff_List();
$h_teaching_teacher_selection .= '<select name="r_teachingTeacherList" id="r_teachingTeacherList" onchange="Add_Teaching_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$_teacherId = $TeacherList[$i]['UserID'];
	
	if (in_array($_teacherId, (array)$writingTeacherIdAry)) {
		continue;
	}
	
	$h_teaching_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$h_teaching_teacher_selection .= '</select>';

### Writing Teacher Selection
$h_removeSelectedTeacherBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], 'button', 'Remove_Teaching_Teacher();');


### Time Selections
$h_startDate = $linterface->GET_DATE_PICKER('r_startDate', $startDate);
$h_startHour = $linterface->Get_Time_Selection_Box('r_startHour', 'hour', $startHour, $others_tab='');
$h_startMin = $linterface->Get_Time_Selection_Box('r_startMin', 'min', $startMin, $others_tab='');

$h_endDate = $linterface->GET_DATE_PICKER('r_endDate', $endDate);
$h_endHour = $linterface->Get_Time_Selection_Box('r_endHour', 'hour', $endHour, $others_tab='');
$h_endMin = $linterface->Get_Time_Selection_Box('r_endMin', 'min', $endMin, $others_tab='');


### Writing Status
$h_publicSel = $linterface->Get_Radio_Button('statusSel_public', 'r_recordStatus', $w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["public"], $recordStatusPublicChecked, $Class="", $Lang['General']['Public']);
$h_privateSel = $linterface->Get_Radio_Button('statusSel_private', 'r_recordStatus', $w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["private"], $recordStatusPrivateChecked, $Class="", $Lang['General']['Private']);


### Buttons
//$h_submitButton = $linterface->GET_ACTION_BTN($ParTitle, $ParType, $ParOnClick="", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$h_submitButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'doFormCheck();', 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="formbutton");
$h_cancelButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'window.location=\'?task=writingMgmt&mod=writing&r_contentCode='.$r_contentCode.'\'', 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="formsubbutton");

$h_bottomButton = <<<HTML1
	{$h_submitButton}
    {$h_cancelButton}
    {$h_deleteWritingButton}
HTML1;


### Form checking warning messages
// divId = {inputId}_warningDiv
$h_inputTitleWarning = $linterface->Get_Form_Warning_Msg('r_writingName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv');
$h_inputSchemeCodeWarning = $linterface->Get_Form_Warning_Msg('r_schemeCode_warningDiv', $Lang['W2']['warningMsgArr']['selectSchemeCode'], 'warningDiv');
$h_inputIntroWarning = $linterface->Get_Form_Warning_Msg('r_introduction_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv');
$h_selectStudentWarning = $linterface->Get_Form_Warning_Msg('r_selectedStudentId_warningDiv', $Lang['General']['JS_warning']['SelectAtLeastOneStudent'], 'warningDiv');
$h_selectTeacherWarning = $linterface->Get_Form_Warning_Msg('r_selectedTeacherId_warningDiv', $Lang['General']['JS_warning']['SelectAtLeastOneTeacher'], 'warningDiv');
$h_submissionInvalidWarning = $linterface->Get_Form_Warning_Msg('submissionInvalidWarnMsg', $Lang['General']['JS_warning']['InvalidDateRange'], 'warningDiv');
$h_mustUpdatePeerMarkingSettingsWarning = $linterface->Get_Form_Warning_Msg('mustUpdatePeerMarkingSettingsWarnDiv', $Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'], 'warningDiv');


### GENERATE THE STEP APPROVAL UI
$h_approvalStep = '';
for($i = 0, $i_max = count($stepInfoAry);$i < $i_max;$i++){
	$_titleEng = $stepInfoAry[$i]['titleEng'];
	$_code = $stepInfoAry[$i]['code'];
	$_sequence = $stepInfoAry[$i]['sequence'];


	$_isChecked = ($thisWritingStepApprovalStatus[$_code] == 1) ? ' checked ' : '';
	if($i != intval($i_max -1)){
		//DON'T DISPLAY THE STEP SETTINGS FOR LAST STEP
		$h_approvalStep .= "<INPUT NAME=\"r_approvalStep[]\" TYPE=\"CHECKBOX\" id=\"r_approvalStep_id_{$_code}\" w2_sequence=\"{$_sequence}\" value=\"{$_code}\" onclick=\"handleApprovalStepUi(this)\" {$_isChecked}>&nbsp;<label for=\"r_approvalStep_id_{$_code}\">".$_titleEng.'</label>&nbsp;&nbsp;'."\n";
	}
}


$h_isRequireStepApprovalChecked = '';
$h_noRequireStepApprovalChecked = '';
$h_approvalStepStyleDisplay = '';

if($thisWritingRequireStepApproval){
	$h_isRequireStepApprovalChecked = ' checked ';
}else{
	$h_noRequireStepApprovalChecked = ' checked ';
	$h_approvalStepStyleDisplay = ' display:none; ';
}

### Template Settings
$h_templatePaper = 1; //
$h_templateClipArt = 1; //


ob_start();
include('templates/admin/editWriting.tmpl.php');
$html_content .= ob_get_contents();
ob_end_clean();

switch($r_returnMsg){
	case 1:
		//record added successfully
		$returnMsg  = '1|=|Add Writing Success.';
		break;
	case 2:
		break;
	default:
		$returnMsg = '';
		break;
}
if ($r_returnMsgKey != '') {
	$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

echo _frameLayout($h_title,$html_content,$h_bottomButton);
$linterface->LAYOUT_STOP();

exit();

function _generateClassSelection(){
	// year class selection
	$lclass = new libclass();
	$thisAttr = ' id="YearClassSelect" name="YearClassID" class="formtextbox" onchange="Get_Class_Student_List();" ';
	$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
	$CurrentYearClassSelection = $lclass->getSelectClassID($thisAttr, $selected="", $DisplaySelect=1, $CurrentAcademicYearID);
	$h_year_class_selection = '<div id="CurrentYearClassSelectionDiv">';
	$h_year_class_selection .= $CurrentYearClassSelection;
	$h_year_class_selection .= '</div>';
	return $h_year_class_selection;
}
function _frameLayout($title , $content,$bottom){
	$html = <<<HTML
				<form action ="/home/eLearning/w2/index.php" method = "post" id="w2_form" name="w2_form">
					 <div class="write_board">
							<div class="write_board_top_step_right">
								<div class="write_board_top_step_left">
									<div class="title">
										{$title}
									</div>
								</div>
							</div>
							<div class="write_board_left">
								<div class="write_board_right">
									<div class="content">
										{$content}
									</div>
								   <p class="spacer"></p>
								   <div class="edit_bottom">
										{$bottom}            
								   </div>
								</div>
							</div>
							<div class="write_board_bottom_left">
									   <div class="write_board_bottom_right"></div>
							</div>
					</div>	
				</form>
HTML;

	return $html;
}
?>