<?
// using:

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($PATH_WRT_ROOT."templates/html_editor/fckeditor.php");

$r_contentCode = trim($_GET['r_contentCode']);
$cid = trim($_GET['cid']);
$tabNum = isset($_GET['tabNum'])?$_GET['tabNum']:-1;
$r_returnMsgKey = $_GET['r_returnMsgKey'];
$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];

### Page title
$h_title = strtoupper($cid ==''?$Lang['W2']['newTemplate']:$Lang['W2']['editTemplate']);	

### Get template information from database
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	$conceptType = $parseData['conceptType'];
	$powerConceptID = $parseData['powerConceptID'];
	$powerBoardID = $parseData['powerBoardID'];
	$attachment = $parseData['attachment'];
	$grade = $parseData['grade'];
	$teacherAttachmentData = $parseData['teacherAttachmentData'];
	$powerConceptCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerconcept"]?'checked':'';
	$powerBoardCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]?'checked':'';
	$attachmentCheck = $conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]?'checked':'';		
}
$h_gradeSelection = $w2_libW2->getGradeSelection($grade);

$objDB = new libdb();

### Topic & Theme - TextType
$textTypeSelectionBoxItems = getCategoryItems("category","category",$parseData['category']);

### Step 1 TextType
$h_selectStep1TextTypeHTML =  getStep1TextTypeItems("step1TextType","step1TextType",$parseData['step1Data']['step1TextType']);

### Step 2 Concept Question
$h_step2ConceptQuestionHTML = getStepQuestionArrayGenerated('step2Concept',$parseData['step2Data']['step2ConceptAry'],$parseData['status']);

### Step 3 Outline Question
$h_step3MyOutlineQuestionHTML = getStepQuestionArrayGenerated('step3Outline',$parseData['step3Data']['step3OutlineAry'],$parseData['status']);

### Step 4 case
$h_step4ApproachQuestionHTML = getStepQuestionArrayGenerated('step4Approach',$parseData['step4Data']['step4ApproachAry'],$parseData['status']);

### Resource case
$resourceCounter = 1;
$getResourceArrayHTML =  getResrouceTableItemsGenerated ($parseData['resourceData'], $resourceCounter);


### FCK editor objects
$FCKEditor = new FCKeditor ( 'step1Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step1Data']['step1Int'];
$step1InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step2Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step2Data']['step2Int'];
$step2InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step3Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step3Data']['step3Int'];
$step3InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step4Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step4Data']['step4Int'];
$step4InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step4ExampleSentence' , "75%", "200", "", "", "");
$FCKEditor->Value = $parseData['step4Data']['step4ExampleSentence'];
$step4ExampleSentenceEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5Int' , "100%", "180", "", "", "");
$FCKEditor->Value = $parseData['step5Data']['step5Int'];
$step5InstructionEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

$FCKEditor = new FCKeditor ( 'step5DefaultWriting' , "100%", "300", "", "", "");
$FCKEditor->Value = !empty($parseData['step5Data']['step5DefaultWriting'])?$parseData['step5Data']['step5DefaultWriting']:"";
$step5DefaultWritingEditor = escapeJavaScriptText(trim( $FCKEditor->CreateHtml() ) );

### Set level
$isBasicSelected = $parseData['level']=='Basic'?'checked':'';
$isAdvancedSelected = $parseData['level']=='Advanced'? 'checked':'';

### button panel
$h_cancelButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'doCancel()', 'formsubbutton', ' onmouseover="this.className=\'formsubbuttonon\'" onmouseout="this.className=\'formsubbutton\'" ',0,'formsubbutton');
$h_saveButton = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', 'doFormCheck(\'-1\');', 'formbutton', 'onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'"',0,'formbutton');
$h_addItemButton = "<a href='#' onclick='addDraftField();return false;' id='addItemBtn' class='new'>Add more</a>";

$h_step2AddMoreButton = "<a href='javascript:void(0);' onclick='addStepQuestionField(\"step2Concept\");return false;' id='step2ConceptAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";
$h_step3AddMoreButton = "<a href='javascript:void(0);' onclick='addStepQuestionField(\"step3Outline\");return false;' id='step3OutlineAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";
$h_step4AddMoreButton = "<a href='javascript:void(0);' onclick='addStepQuestionField(\"step4Approach\");return false;' id='step4ApproachAddMore' class='new'>".$Lang['W2']['addMoreQuestion']."</a>";


### Form checking warning messages
// divId = {inputId}_warningDiv
$h_inputTopicNameWarning = $linterface->Get_Form_Warning_Msg('r_TopicName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_inputTopicIntroWarning = $linterface->Get_Form_Warning_Msg('r_TopicIntro_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_0');
$h_selectCategoryWarning = $linterface->Get_Form_Warning_Msg('r_SelectedCategory_warningDiv', $Lang['W2']['warningMsgArr']['selectCat'], 'warningDiv_0');
$h_selectLevelWarning = $linterface->Get_Form_Warning_Msg('r_SelectedLevel_warningDiv', $Lang['W2']['warningMsgArr']['selectLevel'], 'warningDiv_0');

$h_inputStep1IntWarning = $linterface->Get_Form_Warning_Msg('r_Step1Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1WordCountWarning = $linterface->Get_Form_Warning_Msg('r_Step1WordCount_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_inputStep1TopicKeywordWarning = $linterface->Get_Form_Warning_Msg('r_Step1TopicKeyword_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');
$h_selectStep1TextTypeWarning = $linterface->Get_Form_Warning_Msg('r_Step1TextType_warningDiv', $Lang['W2']['warningMsgArr']['selectAtLeastOneCat'], 'warningDiv_1');
$h_inputStep1KeywordMeaningWarning = $linterface->Get_Form_Warning_Msg('r_Step1KeywordMeaningWarning_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_1');

$h_inputStep2IntWarning = $linterface->Get_Form_Warning_Msg('r_Step2Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');
$h_inputStep2ConceptWarning = $linterface->Get_Form_Warning_Msg('r_Step2Concept_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_2');

$h_inputStep3IntWarning = $linterface->Get_Form_Warning_Msg('r_Step3Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');
$h_inputStep3MyOutlineWarning = $linterface->Get_Form_Warning_Msg('r_Step3Outline_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_3');

$h_inputStep4IntWarning = $linterface->Get_Form_Warning_Msg('r_Step4Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');
$h_inputStep4ExampleSentenceWarning = $linterface->Get_Form_Warning_Msg('r_Step4ExampleSentence_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');
$h_inputStep4ApproachWarning = $linterface->Get_Form_Warning_Msg('r_Step4Approach_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_4');

$h_inputStep5IntWarning = $linterface->Get_Form_Warning_Msg('r_Step5Int_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');
$h_inputStep5DefaultWritingWarning = $linterface->Get_Form_Warning_Msg('r_Step5DefaultWriting_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_5');

$h_inputStep6WebNameWarning = $linterface->Get_Form_Warning_Msg('r_Step6WebName_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_6');
$h_inputStep6WebsiteWarning = $linterface->Get_Form_Warning_Msg('r_Step6Website_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_6');

$h_submissionInvalidWarning = $linterface->Get_Form_Warning_Msg('submissionInvalidWarnMsg', $Lang['General']['JS_warning']['InvalidDateRange'], 'warningDiv');
$h_mustUpdatePeerMarkingSettingsWarning = $linterface->Get_Form_Warning_Msg('mustUpdatePeerMarkingSettingsWarnDiv', $Lang['W2']['warningMsgArr']['mustUpdatePeerMarkingSettings'], 'warningDiv');


### content
ob_start();
include('templates/admin/newChiTemplateWriting.tmpl.php');
$html_content = ob_get_contents();
ob_end_clean();

//Logic function helper layer
#####################################################################################################
function getResrouceTableItemsGenerated ($resourceList, &$resourceCounter)
{
	$resourceHTML ='';
	$resourceList = $resourceList['resource'];
	

	foreach( (array) $resourceList as $resourceItem)
	{
		$_website = !empty($resourceItem['resourceItemWebsite'])?$resourceItem['resourceItemWebsite']:'';
		$_itemName = !empty($resourceItem['resourceItemName'])?$resourceItem['resourceItemName']:'';
		$resourceHTML .= "<tr class='resourceArray'>"."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:95%' name='resource[]' class='resource' id='resource_{$resourceCounter}' value='".$_itemName."' data-resourceID = '{$resourceCounter}'/>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<input type='text' style='width:100%' name='website[]' id='website_{$resourceCounter}' class='webiste' value='".$_website."' data-resourceID = '{$resourceCounter}' />"."\n\r";
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '<td>'."\n\r";
		$resourceHTML .= "<span class='table_row_tool'><a href='#' class='deleteResourceBtn tool_delete_dim' title='Delete'>&nbsp;</a></span>"."\n\r";
		$resourceHTML .= '</td>'."\n\r";

		$resourceHTML .= '<td>'."\n\r";
		for ($i=0; $i<5; ++$i)
		{
			
			$isChecked = '';
			if ( $resourceItem["resourceItemChecked"]['c'.($i+1)] )
			{
				$isChecked = 'checked';
			}
			$resourceHTML .= ('<label for="step'.($i+1).'">'.($i+1).'</label><input type="checkbox" id="step'.($i+1).'" name="r'.$resourceCounter.'_step'.($i+1).'" class="step" '.$isChecked.' />&nbsp;'."\n\r" );
		}
		$resourceHTML .= '</td>'."\n\r";
		$resourceHTML .= '</tr>'."\n\r";
		++$resourceCounter;
	}
	return $resourceHTML;
}

function getCategoryItems($name, $id,$defaultValue='')
{
	global $Lang;
	$defaultKey = array_search($defaultValue,$Lang['W2']['textTypeAry']);
	$returnStr='';
	$returnStr .= '<select id="'.$id.'" name="'.$name.'" >';
	$returnStr .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	foreach ( (array)$Lang['W2']['textTypeAry'] as $_key => $_value){
		$isSelectedHTML = ("$defaultKey" == "$_key")?'selected':'';
		$returnStr .= '<option value="'.$_key.'" '.$isSelectedHTML.' >'.$_value.'</option>';
	}
	$returnStr .= '</select>';
	return $returnStr;
}
function getStep1TextTypeItems($name,$id,$defaultValue='')
{
	global $Lang;
	$textTypeAry = !empty($defaultValue)?explode(',',$defaultValue):array();
	$returnStr='';
	foreach ( (array)$Lang['W2']['textTypeChoiceAry'] as $_key => $_value){
		$isSelectedHTML = (in_array($_key,$textTypeAry))?'checked':'';
		$returnStr .= '<input type="checkbox" name="'.$name.'[]" id="'.$id.'_'.$_key.'" value="'.$_key.'" '.$isSelectedHTML.'/>';
		$returnStr .= '<label for="'.$id.'_'.$_key.'">&nbsp;'.$_value.'</label>';
		$returnStr .= '&nbsp;';
	}
	return $returnStr;
}

function getStepQuestionArrayGenerated($name,$stepQuestion,$status)
{
	global $Lang,$w2_cfg;
	$draftQHTML='';
	$qCnt = (count($stepQuestion)>0)?count($stepQuestion):$w2_cfg['contentInput']['defaultInputNum'];
	for($i=0;$i<$qCnt;$i++){
		$draftQHTML .='<div id="div_'.$name.'[]">';
				$draftQHTML .='<span style="float:left">'.$Lang['W2']['question'].':</span> <textarea name="'.$name.'[]" rows="2" class="'.$name.' textbox" style="width:50%; float:left">'.(!empty($stepQuestion['question'.$i])?stripslashes($stepQuestion['question'.$i]):'').'</textarea>';
				$draftQHTML .='<span class="table_row_tool"><a href="javascript:void(0);" class="tool_delete_dim deleteBtn" name="deleteBtn[]" title="'.$Lang['Btn']['Delete'].'">&nbsp;</a></span>';
			$draftQHTML .='<br style="clear:both;">';
		$draftQHTML .='</div>';
	}
	$draftQHTML .= '<input type="hidden" name="Cnt_'.$name.'" id="Cnt_'.$name.'" value="'.$qCnt.'">';
	return ( empty($draftQHTML)==true)?'':$draftQHTML;
}

function _frameLayout($title , $content,$bottom){
	global $Lang;
	$html = '<form action ="/home/eLearning/w2/index.php" method = "post" id="w2_form" name="w2_form" enctype="multipart/form-data">';
	$html .= '<div class="main_top_blank"> <!-- for blank div 20140113 -->';
	$html .= '</div>';
	$html .= '<p class="spacer"></p>';   
	$html .= '<div class="themename_grp"> <!-- New themename group 20140113 -->';
		$html .= '<span class="themename_icon"></span>';
		$html .= '<span class="themename_main">'.$Lang['W2']['topic'].': <strong id="topicHeader" data-originalText="('.$Lang['W2']['topicName'].')">('.$Lang['W2']['topicName'].')</strong></span>';
	$html .= '</div>';
	$html .= '<br />';
	$html .= '<div class="write_board write_board_input">';
		//$html .= '<div class="content_input">';
			$html .= $content;
			$html .= '<div class="write_board_bottom_left">';
				$html .= '<div class="write_board_bottom_right"></div>';
			$html .= '</div>';
		//$html .= '</div>';
	$html .= '</div>';
	$html .= '</form>	';		


	return $html;
}

function escapeJavaScriptText($string)
{
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}

//Presentation layer


#####################################################################################################
$str =<<<HTML
HTML;
$linterface->LAYOUT_START($returnMsg);

echo _frameLayout($h_title,$html_content,'');
$linterface->LAYOUT_STOP();

exit();
?>