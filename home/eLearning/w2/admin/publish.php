<?
// using:
//include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2PublishMenuGenerator.php");
$r_contentCode = $_GET['r_contentCode'];
$r_returnMsgKey = $_GET['r_returnMsgKey'];
GLOBAL $w2_cfg;

### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];
$objDB = new libdb();
$objW2PublishMenuGenerator =new W2PublishMenuGenerator();
$objWritingTeacher = new libWritingTeacher($w2_thisUserID);

### Toolbar
if ($w2_libW2->isAdminUser()) {
}


### Prepare temp table for display
$SuccessArr = array();

$additionField='';
if($r_contentCode == 'eng')
{	
	$additionField .= "LEVEL varchar(20),";
}
foreach( $w2_cfg["schoolCode"] as $schoolDetail)
{
	$shortDesc = strtolower($schoolDetail["shortDesc"]);
	$additionField.="${shortDesc}Sent varchar(1000) default null,";
}

$sql = "Create Temporary Table If Not Exists TMP_W2_PUBLISH_PAGE (
			CONTENT_NAME varchar(100) default null,
			CONTENT_NAME_LINK TEXT default null,
			THEME varchar(255) default null,
			${additionField}					
			LAST_MODIFIED varchar(255),
			CHECKBOX_FOR_ALL_SCH varchar(1000)
		) ENGINE=InnoDB Charset=utf8";
$SuccessArr['CreateTempTable'] = $objDB->db_db_query($sql);
$publishList = $objW2PublishMenuGenerator->getPublishMenu($r_contentCode);
foreach( (array)$publishList as $publishItem)
{
	$sql = "Insert Into TMP_W2_PUBLISH_PAGE Values ( '".implode("','", (array)$publishItem)."');";
	$SuccessArr['InsertTempTable'] = $objDB->db_db_query($sql);
}


### Set Display Table Object's Attributes
$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PUBLISH_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

$checkbox = '<input type="checkbox" id="checkall"/>';
$objTableMgr->addColumn('CONTENT_NAME_LINK', 1, $Lang['W2']['topic'], '30%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='CONTENT_NAME');
if($r_contentCode =='eng')
{
	$objTableMgr->addColumn('LEVEL', 1, $Lang['W2']['level'], '8%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='LEVEL');
//	$objTableMgr->addColumn('LEVEL', 1, $Lang['W2']['level'], '10%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='LEVEL');
}
$objTableMgr->addColumn('THEME', 1, $Lang['W2']['textType'], '10%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='THEME');
$objTableMgr->addColumn('LAST_MODIFIED', 1, $Lang['W2']['lastUpdateDate'], '10%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='LAST_MODIFIED');

foreach( (array)$w2_cfg["schoolCode"] as $schoolDetail)
{
	if($schoolDetail["shortDesc"] != $w2_cfg["schoolCodeCurrent"])
	{
		$shortDesc = strtolower($schoolDetail["shortDesc"]);
		$objTableMgr->addColumn("${shortDesc}Sent", 1, $schoolDetail['shortDesc'], '10%');
	}
}
if($r_contentCode !='eng')
{
	$objTableMgr->addColumn('BLANK', 1, '', '8%');
//	$objTableMgr->addColumn('BLANK', 1, '', '10%');
}
$objTableMgr->addColumn('CHECKBOX_FOR_ALL_SCH', 0, $checkbox, '2%', $headerClass='', $dataClass='', $extraAttribute='');


$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());
$h_tableResult = $objTableMgr->display();

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/admin/publish.tmpl.php');
$linterface->LAYOUT_STOP();
?>