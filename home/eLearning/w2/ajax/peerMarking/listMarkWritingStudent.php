<?
// using: 
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

$r_contentCode = $_GET['r_contentCode'];
$r_writingId = $_GET['r_writingId'];
$r_showEvaluated = ($_GET['r_showEvaluated'])? true : false;

$objDb = new libdb();

$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$showPeerName = $objWriting->getShowPeerName();
$schemeCode = $objWriting->getSchemeCode();
$stepArr = $objWriting->findStep();
$numOfStep = count($stepArr);
$finalStepInfoAry = $stepArr[$numOfStep-1];
$finalStepCode = $finalStepInfoAry['STEP_CODE'];


### Legend display
//$h_legend = $w2_libW2->htmlLegendInfo(array($w2_cfg["legendArr"]["incomplete"], $w2_cfg["legendArr"]["completed"], $w2_cfg["legendArr"]["studentWaitingTeacherComment"], $w2_cfg["legendArr"]["studentNeedCheckComment"]));


### Get Student Peer Marking Data
$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($w2_thisUserID, $r_writingId, $r_showEvaluated);
$targetUserIdAry = Get_Array_By_Key($peerMarkingDataInfoAry, 'TARGET_USER_ID');
$numOfTargetStudent = count($peerMarkingDataInfoAry);


### Get Writing Student Info
$writingStudentAssoAry = BuildMultiKeyAssoc($w2_libW2->getWritingStudentInfoAry($r_contentCode, $r_writingId), 'USER_ID');

### Get Student info if required
if ($showPeerName) {
	$objUser = new libuser('', '', $targetUserIdAry);
}


$successAry['setPeerMarkingDataAsViewed'] = $w2_libW2->updatePeerMarkingViewStatus($r_writingId, $w2_cfg["DB_W2_PEER_MARKING_DATA_VIEW_STATUS"]["viewed"] , $w2_thisUserID, $targetUserIdAry);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			SUBMISSION_TIME varchar(64) default null,
			CLASS_NAME varchar(255) default null,
			CLASS_NUMBER int(8) default null,
			STUDENT_NAME varchar(255) default null,
			MARK_NOW_LINK text default null
		) ENGINE=InnoDB Charset=utf8
";
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);


$insertAry = array();
for ($i=0, $i_max=$numOfTargetStudent; $i<$i_max; $i++) {
	$_targetUserId = $peerMarkingDataInfoAry[$i]['TARGET_USER_ID'];
	
	// Get writing submit status
	$_writingStudentSubmitStatus = $writingStudentAssoAry[$_targetUserId]['HANDIN_SUBMIT_STATUS'];
	$_writingStudentSubmitDate = $writingStudentAssoAry[$_targetUserId]['HANDIN_SUBMIT_STATUS_DATE'];
	
	// Submission Time
	$_submissionTime = is_date_empty($_writingStudentSubmitDate)? $Lang['General']['EmptySymbol'] : date('Y-m-d G:i', strtotime($_writingStudentSubmitDate));
	
	// "Class", "Class Number" and "Student Name"
	$_targetClassName = $Lang['W2']['na'];
	$_targetClassNumber = 'null';
	$_targetStudentName = $Lang['W2']['na'];
	if ($showPeerName) {
		$objUser->LoadUserData($_targetUserId);
		$_targetClassName = $objUser->ClassName;
		$_targetClassNumber = $objUser->ClassNumber;
		$_targetStudentName = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
	}
	
	// Mark Now Link
	$_markNowLink = $Lang['General']['EmptySymbol'];
	if ($_writingStudentSubmitStatus == $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"]) {
		$_paraAssoAry = array();
		$_paraAssoAry['mod'] = 'handin';
		$_paraAssoAry['task'] = 'viewHandIn';
		$_paraAssoAry['r_action'] = $w2_cfg["actionArr"]["peerMarking"];
		$_paraAssoAry['r_contentCode'] = $r_contentCode;
		$_paraAssoAry['r_writingId'] = $r_writingId;
		$_paraAssoAry['r_schemeCode'] = $schemeCode;
		$_paraAssoAry['r_stepCode'] = $finalStepCode;
		$_paraAssoAry['p_studentId'] = $_targetUserId;
		$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
		$_markNowLink = '<a class="row_icon edit_icon_alert" href="?'.$_para.'">'.$Lang['W2']['markNow'].'</a>';
	}
	
	
	$insertAry[$i] = "";
	$insertAry[$i] .= " ('".$objDb->Get_Safe_Sql_Query($_submissionTime)."', '".$objDb->Get_Safe_Sql_Query($_targetClassName)."', $_targetClassNumber, '".$objDb->Get_Safe_Sql_Query($_targetStudentName)."', '".$objDb->Get_Safe_Sql_Query($_markNowLink)."') ";
}


$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}


### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

$objTableMgr = new libtable_mgr();
$objTableMgr->setSql("Select SUBMISSION_TIME, CLASS_NAME, IF (CLASS_NUMBER is null, '".$Lang['W2']['na']."', CLASS_NUMBER) as CLASS_NUMBER, STUDENT_NAME, MARK_NOW_LINK From TMP_W2_PAGE");
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

$objTableMgr->addColumn('SUBMISSION_TIME', 1, $Lang['W2']['submissionTime'], '25%');
$objTableMgr->addColumn('CLASS_NAME', 1, $Lang['SysMgr']['FormClassMapping']['Class'], '20%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SCHEME_NAME');
$objTableMgr->addColumn('CLASS_NUMBER', 1, $Lang['SysMgr']['FormClassMapping']['ClassNo'], '15%');
$objTableMgr->addColumn('STUDENT_NAME', 1, $Lang['SysMgr']['FormClassMapping']['StudentName'], '20%');
$objTableMgr->addColumn('MARK_NOW_LINK', 0, '', '20%');

$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode.'&r_writingId='.$r_writingId);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());

$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');


$linterface->LAYOUT_START();
include_once('templates/'.$mod.'/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>