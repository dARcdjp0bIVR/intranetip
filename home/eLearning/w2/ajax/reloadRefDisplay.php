<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2RefManager.php");


$r_writingId = $_POST['r_writingId'];
$r_writingStudentId = $_POST['r_writingStudentId'];
$r_contentCode = $_POST['r_contentCode'];
$r_schemeCode = $_POST['r_schemeCode'];
$r_stepCode = $_POST['r_stepCode'];
$r_infoboxCode = $_POST['r_infoboxCode'];


if(!empty($cid)&&$r_contentCode=='sci'){
	include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	list($schoolCode,$scheme) = explode('_',$r_schemeCode);
	$infoboxPrefix = $schoolCode.'_'.$r_contentCode.'_infobox_'.$scheme.'_'.$r_stepCode.'_box';		
	$w2_m_requestAnsCode = array($schoolCode.'_'.$r_contentCode.'_'.$scheme.'_c1_ans3');
	$articleCnt = count($parseData['step2Data']['step2ArticleAry']);
	$w2_m_infoboxCode = array();
	for($i=0;$i<$articleCnt;$i++){
		$w2_m_infoboxCode[$i] = $infoboxPrefix.($i+1);
	}	
	$c_model_cfg['refPresetAry'] = $w2_libW2->getSciencePresetCategoryVocabulary($cid,$w2_m_infoboxCode,$w2_m_requestAnsCode[0]);
	$w2_m_refDisplayMode = $w2_cfg["refDisplayMode"]["manage"];
}else{
	### Load related config
	$contentDetailsAry = $w2_libW2->getModelFile($r_contentCode, $r_schemeCode, $r_stepCode);
	$schemeModel = $contentDetailsAry['schemePath'].'/model.php';
	$contentModel = $contentDetailsAry['model'];
	include_once($schemeModel); // for the whole scheme
	include_once($contentModel); // for individual content
}

### Get related data
$w2_s_stepAnsAry = array();
if(is_array($w2_m_requestAnsCode ) && count($w2_m_requestAnsCode) > 0){
	$w2_s_stepAnsAry = $w2_libW2->getWritingStepAnsByStepCode($w2_m_requestAnsCode, $r_writingId, $w2_thisStudentID);
}
### Generate the display
$objW2RefManager = new libW2RefManager();
$objW2RefManager->setIsEditable(true);
$objW2RefManager->setInfoboxCode($r_infoboxCode);
$objW2RefManager->setWritingStudentId($r_writingStudentId);
$objW2RefManager->setPresetCategoryAry($c_model_cfg['refPresetAry']);
$objW2RefManager->setStepAnsAry($w2_s_stepAnsAry);

$objW2RefManager->setDisplayMode($w2_m_refDisplayMode);
echo $objW2RefManager->display();
?>