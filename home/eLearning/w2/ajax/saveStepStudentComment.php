<?php
include_once($PATH_WRT_ROOT.'includes/w2/libStepStudentComment.php');

$r_stepId = $_POST['r_stepId'];
$r_sticker = $_POST['r_sticker'];
$r_version = $_POST['r_version']?$_POST['r_version']:1;

$r_commentId = $_POST['r_commentId'];
$r_comment[0] = trim(stripslashes($_POST['r_comment'][0]));

$objComment = new libStepStudentComment($r_commentId);
$objComment->setContent($r_comment[0]);
$objComment->setStepId($r_stepId);
$objComment->setUserId($w2_thisStudentID);
$objComment->setVersion($r_version);
$objComment->setSticker($r_sticker);
$objComment->setCommentStatus($w2_cfg["DB_W2_STEP_STUDENT_COMMENT"]["teacherCommented"]);
$objComment->setDateModified('now()');
$objComment->setModifiedBy($_SESSION['UserID']);
$objComment->save();

$commentId = $objComment->getCommentId();
$success = $w2_libW2->updateStepStudentCommentLinkage($r_stepId, $w2_thisStudentID, $commentId);

echo ($success && $commentId > 0)? $commentId : '0';
?>