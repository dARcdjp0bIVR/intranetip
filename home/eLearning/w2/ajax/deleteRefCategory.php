<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefCategoryFactory.php');
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefItemFactory.php');

$r_refCategoryId = $_POST['r_refCategoryId'];

$objW2RefCategorySelfAdd = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["selfAdd"], $r_refCategoryId);
$success = $objW2RefCategorySelfAdd->delete();

echo ($success)? '1' : '0';
?>