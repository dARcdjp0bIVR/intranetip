<?php

include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$r_filePath = trim(stripslashes($_POST['r_filePath']));
$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));
$step = !empty($r_stepCode)?str_replace('c','',$r_stepCode):trim(stripslashes($_POST['saveTab']));
$cid = $_POST['cid']; 
$h_button = '';
if($w2_thisUserType==USERTYPE_STAFF&&empty($r_writingId)&&empty($r_action)) $can_edit = true;
if(!empty($cid))
{
	$engContentParser = new libW2EngContentParser();
	$parseData = $engContentParser->parseEngContent($cid);
	switch($r_contentCode){
		case 'chi':$stepGrammar = $parseData['step4Data']['step4Grammar'];$saveStepContentTMPL='saveChiStepContent';break;
		case 'eng':$stepGrammar = $parseData['step3Data']['step3Grammar'];$saveStepContentTMPL='saveStep3Content';break;
	}
}
$stepGrammar = !empty($stepGrammar)?intranet_undo_htmlspecialchars($stepGrammar):'';
if($can_edit){
$content = '<div id="w2_teacher_attachment_table">';
$content .= $w2_libW2->displayGrammarAnalysisTemplate($step,$cid,$stepGrammar);
$content .= '</div>';
$content .= $linterface->Get_Form_Warning_Msg('r_Step'.$step.'Grammar_warningDiv', $Lang['General']['JS_warning']['CannotBeBlank'], 'warningDiv_'.$step);

$content .= '<input type="hidden" name="type" id="type" value="grammar">';
$content .= '<input type="hidden" name="step" id="step" value="'.$step.'">';	
$content .= '<input type="hidden" name="mod" id="mod" value="">';	
$content .= '<input type="hidden" name="task" id="task" value="">';
$content .= '<input type="hidden" name="cid" id="cid" value="'.$cid.'">';

$h_button .= <<<html
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Save']}" style="{$submitBtnDisplayAttr}" onclick="saveGrammarAnalysis('{$Lang['W2']['returnMsgArr']['updateSuccess']}', '{$Lang['W2']['returnMsgArr']['updateFailed']}');return false;" />
html;
}else{
	$content = $stepGrammar;
}
$h_button .= <<<html
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />

html;



echo $w2_libW2->getThickboxHtml($r_headerHtml, $content, $h_button);
?>
<script>
function validGrammar(){
	var editor = FCKeditorAPI.GetInstance('step<?=$step?>Grammar');
	var isValid = 1;
	var content = editor.GetHTML();
	if( content =='&nbsp;' || content =='<br />')
	{	
		$("#r_Step<?=$step?>Grammar_warningDiv").show();
		isValid = 0;
	}else{
		$("#thickboxForm input[id=step<?=$step?>Grammar]").val(content);
	}
	return isValid;
}
function saveGrammarAnalysis(successMsg, failedMsg) {
	$("#r_Step<?=$step?>Grammar_warningDiv").hide();
	$("#thickboxForm input[id=mod]").val("ajax");
	$("#thickboxForm input[id=task]").val("<?=$saveStepContentTMPL?>");
	if(validGrammar()){
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#thickboxForm").serialize(),
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
						  var returnMsg = (xml)? successMsg : failedMsg;
						  Get_Return_Message(returnMsg);
					  }
		});
	}
}
</script>