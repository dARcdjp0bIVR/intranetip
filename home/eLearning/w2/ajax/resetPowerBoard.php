<?php
// using:

$r_assignmentId = intval($r_assignmentId);

$objW2 = new libW2();

//FIND THE ECLASS DETAILS FOR THE STUDENT
$result = $objW2->findUserDetailsInW2ClassRoom($w2_classRoomID, $w2_thisStudentID);


if(is_array($result) && count($result) ==1 ){
	//NORMAL CASE , FIND ONLY ONE RECORD , DO NOTHING, DON'T SKIP
	$result = current($result);
}else{
	//ABNORMAL CASE , RETURN
	return;
}

$classroom_user_id = intval($result['CLASSROOM_USER_ID']);


$_objDb = new libdb();
$sql = "SELECT PowerBoardID
				 FROM ".$w2_classRoomDB.".powerboard 
				 WHERE user_id='".$classroom_user_id."' AND assignment_id='".$r_assignmentId."'";
$result = $_objDb->returnResultSet($sql);

if(is_array($result) && count($result) == 1){
	// expected case , return only one record and delete it
	$result = current($result);
	$_PowerBoardID = $result['PowerBoardID'];

	$sql= 'delete from '.$w2_classRoomDB.'.powerboard where PowerBoardID = '.$_PowerBoardID;
	$_objDb->db_db_query($sql);
}

?>