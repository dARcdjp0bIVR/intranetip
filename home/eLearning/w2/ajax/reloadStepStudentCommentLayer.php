<?php
include_once($PATH_WRT_ROOT.'includes/w2/libStepStudentComment.php');

$r_commentId = $_POST['r_commentId'];
$r_forThickbox = $_POST['r_forThickbox'];

$isCommentEditMode = ($w2_thisAction == $w2_cfg["actionArr"]["marking"])? true : false; 
if ($r_forThickbox) {
	$isCommentEditMode = false;
}

echo $w2_libW2->getTeacherCommentLayerContent($isCommentEditMode, $r_commentId);
?>