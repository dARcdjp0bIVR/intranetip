<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
#init
$cid = trim($_POST['cid']);
$step = trim($_POST['step']);
$type = trim($_POST['type']);
$parseData = $engContentParser->parseEngContent($cid);
switch($step){
	case 3:
		$step3Data = $parseData['step3Data'];
		$step3Int = $step3Data['step3Int'];
		$step3OutlineAry = $step3Data['step3OutlineAry'];
		$step3Writing = intranet_htmlspecialchars(trim($_POST['step3Writing']));
		##Prepare Step3 Rmark Data
		$step3RemarkAry = array();
		for($i=1;$i<=$w2_cfg['contentInput']['SampleWritingRemarkNum'];$i++){
			$_title = $_POST['remarkTitle_'.$i];
			$_content = $_POST['remarkContent_'.$i];
			if(!empty($_title)||!empty($_content))
				$step3RemarkAry['remarkItem'.$i] = array('remarkTitle'=>intranet_htmlspecialchars($_title),'remarkContent'=>intranet_htmlspecialchars($_content));		
			else
				$step3RemarkAry['remarkItem'.$i] = array();									
		}	
		$buildData = $objEngContentBuilder->buildChiStep3($step3Int,$step3OutlineAry,$step3Writing,$step3RemarkAry);
		
		$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
		$result = $objDB->db_db_query($sql);
	break;
	case 4:
		$step4Data = $parseData['step4Data'];
		$step4Int = $step4Data['step4Int'];	
		$step4ApproachAry = $step4Data['step4ApproachAry'];	
		$step4ExampleSentence = $step4Data['step4ExampleSentence'];						
		$step4Vocab = $step4Data['step4Vocab'];	
		$step4Grammar = $step4Data['step4Grammar'];	
		$match = false;
		$result = 0;
		switch($type){
			case 'vocab':$match=true;$step4Vocab=intranet_htmlspecialchars(trim($_POST['step4Vocab']));break;
			case 'grammar':$match=true;$step4Grammar=intranet_htmlspecialchars(trim($_POST['step4Grammar']));break;	
		}	
		if($match){
			#Existing Files
			$buildData = $objEngContentBuilder->buildChiStep4($step4Int,$step4ApproachAry,$step4ExampleSentence,$step4Vocab,$step4Grammar);
			$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
			$result = $objDB->db_db_query($sql);
		}			
	break;
}
echo $result;
?>

