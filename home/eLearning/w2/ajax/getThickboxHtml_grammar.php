<?php
$header = <<<html
<h1 class="grammar">Grammar analysis</h1>
html;

/*
$content = <<<html
The past tense of <em>is</em> and <em>am</em> is <em>was</em>:
<br />
For example: I <em>am</em> tired now.&nbsp;&nbsp;&nbsp;I <em>was</em> tired last night.
<br /><br />

The past form of regular verbs usually end with -ed:
<br />
For example: work<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />worked&nbsp;&nbsp;&nbsp;&nbsp;lived<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />lived&nbsp;&nbsp;&nbsp;&nbsp;carry<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />carried&nbsp;&nbsp;&nbsp;&nbsp;stop<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />stopped
<br /><br />
The past form of irregular verbs vary:
<br />
For example: put<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />put&nbsp;&nbsp;&nbsp;&nbsp;come<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />came&nbsp;&nbsp;&nbsp;&nbsp;spend<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />spent&nbsp;&nbsp;&nbsp;&nbsp;go<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />went
html;
*/
$content = <<<html
<b>Simple past tense </b><br/><br/>

We use the <b>simple past tense	</b>to talk about things that happened in the past.<br/><br/>

The past tense of is and am <img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />was:<br/>
For example: I am tired now.	I was tired last night.<br/><br/>

The past form of regular verbs usually ends with -ed:<br/>
For example: work<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />worked &nbsp;&nbsp;&nbsp;&nbsp;
			 lived<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />lived &nbsp;&nbsp;&nbsp;&nbsp;
			 carry<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />carried &nbsp;&nbsp;&nbsp;&nbsp;
			 stop<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />stopped &nbsp;&nbsp;&nbsp;&nbsp;
			 <br/><br/>

The past form of irregular verbs varies:<br/>
For example: put<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />put &nbsp;&nbsp;&nbsp;&nbsp;
			come<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />came &nbsp;&nbsp;&nbsp;&nbsp;
			spend<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />spent &nbsp;&nbsp;&nbsp;&nbsp;
			go<img src="/images/2009a/w2/Resource/190_writing20/icon_grammar_arrow.gif" align="absmiddle" />went &nbsp;&nbsp;&nbsp;&nbsp;
			<br/><br/>

Adverbs commonly used with the simple past tense:<br/>
For example: yesterday   last Monday / week / month / year   ago <br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;this morning / afternoon<br/><br/>


html;

$button = <<<html
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="Close" onclick="js_Hide_ThickBox();" />
html;


echo $w2_libW2->getThickboxHtml($header, $content, $button);
?>