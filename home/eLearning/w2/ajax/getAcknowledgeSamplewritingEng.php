<?php


### Please Save It In UTF-8!!!
$PATH_WRT_ROOT_ABS = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
intranet_auth();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Writing Samples</title>

<script type="text/javascript">
<!--
$( document ).ready(function() {
    $(".writing_grp").click(function(){
		if(!$(this).hasClass("empty")){
			$(".bubble_writing").css("visibility","hidden");
			if(!$(this).hasClass("current")){ //open it
				$(this).next().css("visibility","visible");
			}
			$(".writing_grp").removeClass("current");
			if($(this).next().css("visibility")=="visible"){
				$(this).addClass("current");
			}
			
		}
	});
    $(".bubble_writing .btn_close").click(function(){
		$(this).parents().find('.bubble_writing').css("visibility","hidden");
		$(this).parents().find('.writing_grp').removeClass("current");
	});	

	$('.board_mid_header').find('a').click(function(){
		$('a.current').removeClass('current');
		$(this).addClass('current');
		var page = $(this).attr('data-page-info');
		$('.tabContent').hide();
		$('#'+page).show();
	});		
});
//-->
</script>
</head>

<body style="background:none; overflow:hidden">
<div class="content_pop_board_samplewriting" id="samplewriting_eng">
	<div class="board_top">
		<div class="board_title">English Writing Samples</div>	
    </div>
	<div class="board_mid" style="height:455px">
	    <div class="board_mid_main">
    		<div class="board_mid_header ">
			<a class="current" data-page-info="sample_pop">Text Types</a>
			<a data-page-info="sample_desc">Text Type Description</a>
			</div>
        	<div class="board_mid_content tabContent" id="sample_pop" >
			
            <div class="board_mid_content_list" style="width:230px">
                <!-- #01 -->
                <a class="writing_grp">Advertisement</a>
                <div id="writing_grp01" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E03_1&sampleLang=en')">Job Advertisement</a></li>

                    </ul>
                </div>

                <!-- #02 -->
                <a class="writing_grp ">Argumentative Essay</a>
                <div id="writing_grp02" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E06_1&sampleLang=en')">Influence on teenagers</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E06_2&sampleLang=en')">Cash or cards</a></li>
                    </ul>
                </div>

                <!-- #03 -->
                <a class="writing_grp ">Article</a>
                <div id="writing_grp03" class="bubble_writing right" style="width:360px;">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_1&sampleLang=en')">A Military Camp to Remember</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_2&sampleLang=en')">Act Now! Before it’s too late…</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_3&sampleLang=en')">Environmental Protection in Hong Kong</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_4&sampleLang=en')">My job</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_5&sampleLang=en')">What can we get from sports apart from a healthier body?</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_6&sampleLang=en')">Protecting privacy on the internet</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_7&sampleLang=en')">Charity events you have taken part in</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E07_8&sampleLang=en')">Charity Day</a></li>
                    </ul>
                </div>

                <!-- #04 -->
                <a class="writing_grp">Blog Entry</a>
                <div id="writing_grp04" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E08_1&sampleLang=en')">Holiday with your family</a></li>
                    </ul>
                </div>

                <!-- #05 -->      
                <a class="writing_grp ">Complaint Letter</a>
                <div id="writing_grp05" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E09_1&sampleLang=en')">To complain about a restaurant</a></li>
                    </ul>
                </div>
                
                <!-- #06 -->
                <a class="writing_grp ">Debate</a>
                <div id="writing_grp06" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E10_1&sampleLang=en')">Waste</a></li>
                    </ul>
                </div>
			
                <!-- #07 -->
                <a class="writing_grp">Description</a>
                <div id="writing_grp7" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E33_1&sampleLang=en')">Friends</a></li>
                    </ul>
                </div>
                
                <!-- #08 -->                
                <a class="writing_grp ">Diary Entry</a>
                <div id="writing_grp08" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E11_1&sampleLang=en')">Recent event</a></li>
                    </ul>
                </div>

                <!-- #09 -->
                <a class="writing_grp ">Email</a>
                <div id="writing_grp09" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E04_1&sampleLang=en')">Trip abroad</a></li>
                    </ul>
                </div>

                <!-- #10 -->       
                <a class="writing_grp  ">Fact Sheet</a>
                <div id="writing_grp10" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E01_1&sampleLang=en')">Main character introduction</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E01_2&sampleLang=en')">Facts</a></li>
                    </ul>
                </div>
                <!-- #11 -->                
                <a class="writing_grp ">Leaflet</a>
                <div id="writing_grp11" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E13_1&sampleLang=en')">Top Fashion Clothes Store</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E13_2&sampleLang=en')">Summer Study Tour</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E13_3&sampleLang=en')">Adventure holiday</a></li>
                    </ul>
                </div>         


            </div>
            <div class="board_mid_content_list" style="width:240px">
                
                
                <!-- #12 --> 
                <a class="writing_grp ">Letter of Advice</a>
                <div id="writing_grp12" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E14_1&sampleLang=en')">Diet</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E14_2&sampleLang=en')">School</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E14_3&sampleLang=en')">Problems at school</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E14_4&sampleLang=en')">Personal problems</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E14_5&sampleLang=en')">Teen Problems</a></li>
                    </ul>
                </div>

                <!-- #13 -->    
                <a class="writing_grp ">Letter of Application</a>
                <div id="writing_grp13" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E15_1&sampleLang=en')">Job application</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E15_2&sampleLang=en')">Art project</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E15_3&sampleLang=en')">Volunteering</a></li>
                    </ul>
                </div>

                <!-- #14 -->                
                <a class="writing_grp ">Letter of Complaint</a>
                <div id="writing_grp14" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E16_1&sampleLang=en')">A letter of complaint to the manager</a></li>
                    </ul>
                </div>

                <!-- #15 -->                
                <a class="writing_grp ">Letter to the Editor</a>
                <div id="writing_grp15" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E17_1&sampleLang=en')">Future schools and learning</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E17_2&sampleLang=en')">How to be a responsible pet owner</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E17_3&sampleLang=en')">Effect on young people in Hong Kong</a></li>
                    </ul>
                </div>

                <!-- #16 -->                
                <a class="writing_grp ">List of Rules</a>
                <div id="writing_grp16" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E18_1&sampleLang=en')">Rules for students</a></li>
                    </ul>
                </div>

                <!-- #17 -->                
                <a class="writing_grp ">News Report</a>
                <div id="writing_grp17" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E35_1&sampleLang=en')">Bank robbery</a></li>
                    </ul>
                </div>

                <!-- #18 -->                
                <a class="writing_grp ">Notice</a>
                <div id="writing_grp18" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E36_1&sampleLang=en')">Inter-class sports competition</a></li>
                    </ul>
                </div>

                <!-- #19 -->   
                <a class="writing_grp ">Personal Letter</a>
                <div id="writing_grp19" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E37_1&sampleLang=en')">Getting a pet</a></li>
                    </ul>
                </div>

                <!-- #20 -->                
                <a class="writing_grp ">Picture Description</a>
                <div id="writing_grp20" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E20_1&sampleLang=en')">Secret Birthday Celebration</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E20_2&sampleLang=en')">An open ended story</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E20_3&sampleLang=en')">Health Problems</a></li>
                    </ul>
                </div>

                <!-- #21 -->     
                <a class="writing_grp ">Postcard</a>
                <div id="writing_grp21" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E21_1&sampleLang=en')">Your holiday</a></li>
                    </ul>
                </div>
                <!-- #22 -->  
                <a class="writing_grp ">Poster</a>
                <div id="writing_grp22" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E22_1&sampleLang=en')">School fun fair</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E22_2&sampleLang=en')">Something strange</a></li>
                    </ul>
                </div>
                
            </div>
            <div class="board_mid_content_list" style="width:305px; margin-right:0">
				
                
                
                <!-- #23 -->                
                <a class="writing_grp ">Presentation Script</a>
                <div id="writing_grp23" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E23_1&sampleLang=en')">Greatest invention</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E23_2&sampleLang=en')">A world without money</a></li>
                    </ul>
                </div>

                <!-- #24 -->                
                <a class="writing_grp ">Problem-solution Essay</a>
                <div id="writing_grp24" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E24_1&sampleLang=en')">Preventing obesity</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E24_2&sampleLang=en')">Debt in Hong Kong</a></li>
                    </ul>
                </div>

                <!-- #25 -->                
                <a class="writing_grp ">Profile</a>
                <div id="writing_grp25" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E25_1&sampleLang=en')">The most influential person in the world</a></li>
                    </ul>
                </div>

                <!-- #26 -->                
                <a class="writing_grp ">Proposal</a>
                <div id="writing_grp26" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E26_1&sampleLang=en')">To propose an event for the English Club</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E26_2&sampleLang=en')">Making your school a greener place</a></li>
                    </ul>
                </div>

                <!-- #27 -->                               
                <a class="writing_grp ">Reply Letter</a>
                <div id="writing_grp27" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E27_1&sampleLang=en')">Cultural activities</a></li>
                    </ul>
                </div>
                
                <!-- #28 -->                
                <a class="writing_grp ">Report</a>
                <div id="writing_grp28" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E28_1&sampleLang=en')">Sport competition</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E28_2&sampleLang=en')">To report survey results</a></li>
                    </ul>
                </div>
                
                <!-- #29 -->              
                <a class="writing_grp " >Short Story</a>
                <div id="writing_grp29" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E29_1&sampleLang=en')">A scary story</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E29_2&sampleLang=en')">Unforgettable holiday</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E29_3&sampleLang=en')">The end of the world</a></li>
                    </ul>
                </div>
                
                <!-- #30 --> 
                <a class="writing_grp " >Speech</a>
                <div id="writing_grp30" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E02_1&sampleLang=en')">Fantasy film</a></li>
                    </ul>
                </div>
                
                 <!-- #31 -->
                <a class="writing_grp ">Survey Report</a>
                <div id="writing_grp31" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E31_1&sampleLang=en')">Favourite TV Programme</a></li>
                    </ul>
                </div>
                
                <!-- #32 -->
                <a class="writing_grp ">Two-sided Argumentative Essay</a>
                <div id="writing_grp32" class="bubble_writing left">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=E32_1&sampleLang=en')">E-learning</a></li>
                    </ul>
                </div>
                
            </div>
        </div>
        <span style="clear:both"></span>
		<div class="board_mid_writing_content tabContent" id="sample_desc" style="display:none;height:400px">
				<div class="text">
<!-- Texttype start -->
<h2>1.	Casual writing</h2><br />
<strong>Text types:</strong><br />
<ol start="1">
<li>Diary entry</li>
<li>Personal letter</li>
<li>Speech to fellow classmates</li>
<li>Blog entry</li>
<li>Email</li>
<li>Postcard</li>
</ol><br />
<strong>Introduction:</strong><br />
Casual writing includes writing to people you know fairly well, for example, friends and relatives. Examples include diary entries and personal letters. They usually include things you do and see every day, and how you feel about them. The tone is fairly casual. Use the past tense when writing about what happened and use the present tense to talk about how you are feeling now.
<br /><br />
<strong>Writing steps:</strong><br />
<ol start="1">
<li>Read the question and think of a time where a similar situation happened to you. Think about what lesson you have learnt from the experience and what you want to remind yourself / tell your reader.</li>
<li>Start with ‘Dear’ for diary entries (Dear Diary), personal letters and emails (Dear + name of the person you are writing to). Start with ‘Good morning / afternoon’ if you are writing a speech script.</li>
<li>Explain what happened. Go into detail and build up the story to a climax. </li>
<li>Conclude with how you are feeling and what you hope your reader to do (e.g. ‘Write back soon!’)</li>
<li>Edit and proofread your writing.</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>2.	Promotional materials</h2><br />
<strong>Text types:</strong><br />
<ol start="1">
<li>Leaflet</li>
<li>Poster</li>
<li>Notice</li>
<li>Advertisement</li>
</ol><br />
<strong>Introduction:</strong><br />
The aim of promotional materials, such as leaflet and posters, is to give you information about a product, service or event. It is eye-catching and doesn’t contain too much text. Emotive words and phrases are used a lot.
<br /><br />
<strong>Writing steps:</strong><br />
<ol start="1">
<li>Identify target audience and what the text is for (i.e. event, product, service)</li>
<li>Come up with a catchy title</li>
<li>Write the information under subheadings such as ‘What is it?’ ‘Why should I come?’ ‘When will it take place / Where can I buy it?’ In total, have around 3-4 subheadings.</li>
<li>At the bottom of the text, write a final sentence encouraging people to come to the event / buy the product</li>
<li>Edit and proofread your text</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>3.	Articles</h2><br />
<strong>Text types:</strong><br />
<ol start="1">
<li>Problem-solution essay</li>
<li>Feature article</li>
</ol><br />
<strong>Introduction:</strong><br />
An article is a piece of writing about a particular subject. It gives facts, opinions and information about the subject. An article is a formal text type.
<br /><br />
<strong>Writing steps:</strong><br />
<ol start="1">
<li>Decide who your audience is and research / brainstorm the subject.</li>
<li>Introduce the subject in your introduction and use one or two rhetorical questions</li>
<li>Explain the subject in the middle part of the article, explaining what happened / what people think about the subject.</li>
<li>State your opinions in the conclusion, and keep in mind the message you want the readers to go away with.</li>
<li>Edit and proofread your article</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>4.	Formal letters</h2><br />
<strong>Text types:</strong><br />
<ol start="1">
<li>Letter of complaint</li>
<li>Letter of advice</li>
<li>Job application letter</li>
</ol><br />
<strong>Introduction:</strong><br />
A formal letter is written to somebody you do not know very well. The tone needs to be polite and information given needs to be clearly presented and explained.
<br /><br />
<strong>Writing steps:</strong><br />
<ol start="1">
<li>Start the letter with ‘Dear…’</li>
<li>Start with the purpose of the letter. For letter of advice, offer words of encouragement. If it is a letter of complaint, state your complaint clearly.</li>
<li>Give details regarding your purpose. For example, give practical advice with explanation and/or illustration in letters of advice; describe the situation you are not happy with in a letter of complaint.</li>
<li>Conclude your letter. It can be with a kind remark for letter of advice, stating that they can ask for advice / meet up with you any time; or expected action of the reader in letters of complaint.</li>
<li>Edit and proofread your letter.</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>5.	Argumentative essays</h2><br />
<strong>Text types:</strong><br />
<ol start="1">
<li>One-sided argumentative essay</li>
<li>Two-sided argumentative essay</li>
<li>Letter to editor</li>
<li>Persuasive essay</li>
<li>Debate speech</li>
</ol><br />
<strong>Introduction:</strong><br />
A 2-sided argumentative essay is where both sides of the argument are explained and discussed, and then the writer’s opinion is stated at the end. A formal tone is used throughout.
<br /><br />
<strong>Writing steps:</strong><br />
<ol start="1">
<li>Identify the subject and research / brainstorm on it.</li>
<li>Introduce the subject in the introduction, and state your opinion.</li>
<li>Start with the affirmative argument (agreeing with the subject statement) and give supporting evidence. In two-sided argumentative essays and letters to the editor, you may also present the opposing argument (disagreeing with the subject statement) and explain why this view is not correct.</li>
<li>Conclude by stating your opinion and summarize your reasons.</li>
<li>Edit and proofread your essay.</li>
</ol><br />
<!-- Texttype end -->




				</div>
       	  </div>
 

			
			
    	</div>
    </div>
	<div class="board_bottom"></div>
</div>


</body>
</html>
