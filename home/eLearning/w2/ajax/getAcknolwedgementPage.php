<?php global  $LAYOUT_SKIN, $Lang ;


$PATH_WRT_ROOT_ABS = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
intranet_auth();
?>


<head>
	<title><?php echo $Lang['W2']['projAck'] ?></title>
</head>
<body>
<div class="content_pop_board_190" style="height:502px">
  <h1 class="acknow"><?php echo $Lang['W2']['projAck'] ?><!--//嗚謝--></h1>
    <div class="content_pop_board_write_190" style="height:402px">
      <div class="acknow_title"><?php echo $Lang['W2']['partSch'] ?>
          <!--//參與學校-->
      </div>
      <div class="acknow_table">
      	<table>
        	<tr>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_fukien.jpg"></td>
            	<th>福建中學<br />Fukien Secondary School</th>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_poleungkuk.jpg"></td>
            	<th>保良局羅氏基金中學<br />Po Leung Kuk Laws Foundation College</th>
            </tr>
        	<tr>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_puikiu.png"></td>
            	<th>培僑中學<br />Pui Kiu Middle School</th>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_stmar_co.jpg"></td>
            	<th>聖瑪加利男女英文中小學<br />St. Margaret's Co-educational English<br /> Secondary & Primary School</td>
            </tr>
        	<tr>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_stmar_girl.png"></th>
            	<th>香港聖瑪加利女書院<br />St. Margaret's Girls' College, Hong Kong</th>
            	<td>&nbsp;</td>
            	<th>&nbsp;</th>
            </tr>
        </table>
	  </div>
 	 <span style="clear:both" ></span>
 	 <!--<br style="clear:both" /> -->
      <div class="acknow_title"><?php echo $Lang['W2']['projPart'] ?>
          <!--/計劃協作夥伴-->
      </div>
      <div class="acknow_table">
      	<table>
        	<tr>
        	  <td colspan="2" style="color:#666666"><?php echo $Lang['W2']['systemDev'] ?> :<!--//系統開發 :--></td>
        	  <td colspan="2" style="color:#666666"><?php echo $Lang['W2']['contentPro'] ?> :<!--//內容供應 :--></td>
       	  </tr>
        	<tr>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_bl.png"></td>
            	<th style="padding-right:5px">博文教育(亞洲)有限公司<br />BroadLearning Education (Asia) Limited</th>
            	<td><img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/w2/Resource/190_writing20/ack_hkep.png"></td>
            	<th style="padding-right:5px">香港教育圖書公司<br />Hong Kong Educational Publishing Co.</th>
            </tr>
        </table>
	  </div>
      
    </div>
<div class="edit_bottom" id="edit_content_pop">
		<input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="js_Hide_ThickBox();" value="<?=$Lang['Btn']['Close']?>" />
  	</div>  
</div>
</body>
