<?php
include_once($PATH_WRT_ROOT."includes/libuser.php");

$r_selectedStudentIdAry = $_POST['r_selectedStudentIdAry'];
$r_numOfPeerMarkingGroup = $_POST['r_numOfPeerMarkingGroup'];


$objUser = new libuser('', '', $r_selectedStudentIdAry);


$studentInfoAry = array();
$numOfStudent = count($r_selectedStudentIdAry);
for ($i=0; $i<$numOfStudent; $i++) {
	$_studentId = $r_selectedStudentIdAry[$i];
	$objUser->LoadUserData($_studentId);
	
	$studentInfoAry[$_studentId]['studentName'] = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
	$studentInfoAry[$_studentId]['className'] = $objUser->ClassName;
	$studentInfoAry[$_studentId]['classNumber'] = $objUser->ClassNumber;
}
  

echo $w2_libW2->genRandomPeerMarkingGrouping($studentInfoAry, $r_numOfPeerMarkingGroup);
?>