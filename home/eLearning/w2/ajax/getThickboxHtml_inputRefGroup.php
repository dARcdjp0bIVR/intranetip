<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefCategoryFactory.php');

$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));

$paramAry = breakEncodedURI($r_extraParam);
$param_infoboxCode = trim($paramAry['infoboxCode']);
$param_refCategoryId = trim($paramAry['refCategoryId']);

$cssSetSelectedAry = array();
if (is_numeric($param_refCategoryId)) {
	// edit
	$objRefCategoryContentInput = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["contentInput"], $param_refCategoryId);
	$cssSet = $objRefCategoryContentInput->getCssSet();
	$title = intranet_htmlspecialchars($objRefCategoryContentInput->getTitle());
}
else {
	// add
	$cssSet = $w2_cfg['refDefaultCssSet'];
	$title = '';
}


### Build highlight color options
$h_highlightOptions = $w2_libW2->getHighlightOptionHtml($cssSet);


$header = $r_headerHtml;

$content = '';
$content .= '<table class="grp_table">';
	$content .= '<tbody>';
		$content .= '<tr class="vocab_group">';
			$content .= '<td nowrap="nowrap">Group name</td>';
			$content .= '<td>:</td>';
			$content .= '<td>';
				$content .= '<div class="newgrp_row">';
        			$content .= '<input type="text" size="60" id="r_refCategoryTitleTb" name="r_refCategoryTitle" value="'.$title.'">';
        		$content .= '</div>';
    		$content .= '</td>';
    	$content .= '</tr>';
  		$content .= '<tr class="vocab_group">';
    		$content .= '<td nowrap="nowrap">Group color</td>';
    		$content .= '<td>:</td>';
    		$content .= '<td>';
    			$content .= $h_highlightOptions;
		        $content .= '<br>';
    		$content .= '</td>';
  		$content .= '</tr>';
	$content .= '</tbody>';
$content .= '</table>';
$content .= '<input type="hidden" id="r_infoboxCode" name="r_infoboxCode" value="'.$param_infoboxCode.'" />';
$content .= '<input type="hidden" id="r_refCategoryId" name="r_refCategoryId" value="'.$param_refCategoryId.'" />';
$content .= '<input type="hidden" id="r_refCategoryCssSet" name="r_refCategoryCssSet" value="'.$cssSet.'" />';
$content .= '<input type="hidden" name="step" id="step" value="'.$step.'">';	
$content .= '<input type="hidden" name="mod" id="mod" value="">';	
$content .= '<input type="hidden" name="task" id="task" value="">';
$content .= '<input type="hidden" name="cid" id="cid" value="'.$cid.'">';
$content .= '<input type="hidden" name="r_contentCode" id="r_contentCode" value="'.$r_contentCode.'">';


$button = <<<html
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Save']}" style="{$submitBtnDisplayAttr}" onclick="saveInputRefCategory('{$Lang['W2']['returnMsgArr']['vocabGroupUpdateSuccess']}', '{$Lang['W2']['returnMsgArr']['vocabGroupUpdateFailed']}');" />
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />
html;


echo $w2_libW2->getThickboxHtml($header, $content, $button);
?>
<script>
function saveInputRefCategory(successMsg, failedMsg) {
	$("#thickboxForm input[id=mod]").val("ajax");
	$("#thickboxForm input[id=task]").val("saveInputRefCategory");
	
	var canSave = true;
	if (Trim($('input#r_refCategoryTitleTb').val()) == '') {
		alert('Please enter Group Name.');
		$('input#r_refCategoryTitleTb').focus();
		canSave = false;
	}
	
	if (canSave) {
		$.ajax({
			url:      "/home/eLearning/w2/index.php",
			type:     "POST",
			data:     $("#thickboxForm").serialize(),
			success:  function(result){
							var returnMsg;
						  
							if (result) {
								var infoboxCode = $('input#r_infoboxCode').val();
								reloadInputRefDisplay(infoboxCode);
								
								js_Hide_ThickBox();
								returnMsg = successMsg;
							}
							else {
								returnMsg = failedMsg;
							}
							//Scroll_To_Top();
							Get_Return_Message(returnMsg);
					  }
		});
	}
}

</script>