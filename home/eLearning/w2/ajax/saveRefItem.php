<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefItemFactory.php');

$r_infoboxCode = $_POST['r_infoboxCode'];
$r_writingStudentId = $_POST['r_writingStudentId'];
$r_refCategoryType = $_POST['r_refCategoryType'];
$r_refCategoryCode = $_POST['r_refCategoryCode'];
$r_refItemId = $_POST['r_refItemId'];
$r_refItemTitle = trim(stripslashes($_POST['r_refItemTitle']));


$objW2RefItemSelfAdd = libW2RefItemFactory::createObject($w2_cfg["refItemObjectSource"]["selfAdd"], $r_refItemId);
$objW2RefItemSelfAdd->setInfoboxCode($r_infoboxCode);
$objW2RefItemSelfAdd->setWritingStudentId($r_writingStudentId);
$objW2RefItemSelfAdd->setRefCategoryType($r_refCategoryType);
$objW2RefItemSelfAdd->setRefCategoryCode($r_refCategoryCode);
$objW2RefItemSelfAdd->setTitle($r_refItemTitle);
$objW2RefItemSelfAdd->setModifiedBy($_SESSION['UserID']);
if ($r_refItemId == '') {
	$objW2RefItemSelfAdd->setInputBy($_SESSION['UserID']);
}
$refItemId = $objW2RefItemSelfAdd->save();

echo ($refItemId > 0)? $refItemId : '0';
?>