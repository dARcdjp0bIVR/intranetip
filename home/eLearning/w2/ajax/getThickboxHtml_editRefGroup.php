<?php
include_once($PATH_WRT_ROOT.'includes/w2/libW2RefCategoryFactory.php');

$r_headerHtml = trim(stripslashes($_POST['r_headerHtml']));
$r_extraParam = trim(stripslashes($_POST['r_extraParam']));

$paramAry = breakEncodedURI($r_extraParam);
$param_infoboxCode = trim($paramAry['infoboxCode']);
$param_refCategoryId = trim($paramAry['refCategoryId']);

$cssSetSelectedAry = array();
if (is_numeric($param_refCategoryId)) {
	// edit
	$objRefCategorySelfAdd = libW2RefCategoryFactory::createObject($w2_cfg["refCategoryObjectSource"]["selfAdd"], $param_refCategoryId);
	$cssSet = $objRefCategorySelfAdd->getCssSet();
	$title = intranet_htmlspecialchars($objRefCategorySelfAdd->getTitle());
}
else {
	// add
	$cssSet = $w2_cfg['refDefaultCssSet'];
	$title = '';
}


### Build highlight color options
$h_highlightOptions = $w2_libW2->getHighlightOptionHtml($cssSet);


$header = $r_headerHtml;

$content = '';
$content .= <<<html
<table class="grp_table">
	<tbody>
		<tr class="vocab_group">
    		<td nowrap="nowrap">Group name</td>
    		<td>:</td>
    		<td>
    			<div class="newgrp_row">
        			<input type="text" size="60" id="r_refCategoryTitleTb" name="r_refCategoryTitle" value="{$title}">
        		</div>
    		</td>
    	</tr>
  		<tr class="vocab_group">
    		<td nowrap="nowrap">Group color</td>
    		<td>:</td>
    		<td>
    			{$h_highlightOptions}
		        <br>
    		</td>
  		</tr>
	</tbody>
</table>
<input type="hidden" id="r_infoboxCode" name="r_infoboxCode" value="{$param_infoboxCode}" />
<input type="hidden" id="r_refCategoryId" name="r_refCategoryId" value="{$param_refCategoryId}" />
<input type="hidden" id="r_refCategoryCssSet" name="r_refCategoryCssSet" value="{$cssSet}" />
html;


$button = <<<html
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Save']}" style="{$submitBtnDisplayAttr}" onclick="saveRefCategory('{$Lang['W2']['returnMsgArr']['vocabGroupUpdateSuccess']}', '{$Lang['W2']['returnMsgArr']['vocabGroupUpdateFailed']}');" />
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="{$Lang['Btn']['Close']}" onclick="js_Hide_ThickBox();" />
html;


echo $w2_libW2->getThickboxHtml($header, $content, $button);
?>