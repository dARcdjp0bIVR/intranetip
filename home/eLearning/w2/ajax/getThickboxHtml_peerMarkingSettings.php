<?php
//using: 
/*
 * 	Log
 * 	Date:	2015-02-10 [Cameron] change $Lang['W2']['details'] to $Lang['W2']['standard']
 */
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT.'includes/json.php');


$json = new JSON_obj();


$r_writingId = $_POST['r_writingId'];
$r_selectedStudentIdAry = $_POST['r_selectedStudentId'];
$r_markingInfoAry = $json->decode(trim(stripslashes($_POST['r_markingJsonString'])));

$r_allowPeerMarking = $r_markingInfoAry['r_allowPeerMarking'];
$r_peerMarkingStartDate = is_date_empty($r_markingInfoAry['r_peerMarkingStartDate'])? date('Y-m-d') : date('Y-m-d', strtotime($r_markingInfoAry['r_peerMarkingStartDate']));
$r_peerMarkingEndDate = is_date_empty($r_markingInfoAry['r_peerMarkingEndDate'])? date('Y-m-d', strtotime(' +7 day')) : date('Y-m-d', strtotime($r_markingInfoAry['r_peerMarkingEndDate']));
$r_peerMarkingTargetNum = $r_markingInfoAry['r_peerMarkingTargetNum'];
$r_showPeerName = $r_markingInfoAry['r_showPeerName'];
$r_peerMarkingGroupingAry = $r_markingInfoAry['r_peerMarkingGroupingAry'];

$r_allowSelfMarking = $r_markingInfoAry['r_allowSelfMarking'];
$r_fullMark = $r_markingInfoAry['r_fullMark'];
$r_passMark = $r_markingInfoAry['r_passMark'];
$r_lowestMark = $r_markingInfoAry['r_lowestMark'];
$r_teacherWeight = $r_markingInfoAry['r_teacherWeight'];
$r_selfWeight = $r_markingInfoAry['r_selfWeight'];
$r_peerWeight = $r_markingInfoAry['r_peerWeight'];


### allow peer marking checkbox status
//$h_allowPeerMarkingChecked = '';
//$h_peerMarkingScheduleShow = 'display:none;';
//if ($r_allowPeerMarking) {
//	$h_allowPeerMarkingChecked = 'checked';
//	$h_peerMarkingScheduleShow = '';
//}
$h_allowTeacherMarkingChecked = is_null($r_teacherWeight)?'':'checked';
$h_teacherWeightDisabled = is_null($r_teacherWeight)?'disabled':'';

$h_allowPeerMarkingChecked = !is_null($r_allowPeerMarking)&&$r_allowPeerMarking?'checked':'';
$h_peerWeightDisabled = is_null($r_peerWeight)?'disabled':'';
$h_peerMarkingScheduleShow = $h_allowPeerMarkingChecked?'':'display:none;';


### only allow to set peer marking if there are have then 2 students in the writing
$numOfSelectedStudent = count($r_selectedStudentIdAry);
$canApplyPeerMarking = ($numOfSelectedStudent >= 2)? true : false;


$h_studentMarkEveryoneChecked = '';
$h_studentMarkNumChecked = '';
if ($r_peerMarkingTargetNum==$w2_cfg["DB_W2_WRITING_PEER_MARKING_TARGET_NUM"]["markEveryone"]) {
	$h_studentMarkEveryoneChecked = 'checked';
}
else {
	$h_studentMarkNumChecked = 'checked';
}


$h_showPeerMarkingChecked = ($r_showPeerName)? 'checked' : '';


### Peer Marking Schedule
$h_peerMarkingStartDate = $linterface->GET_DATE_PICKER('r_peerMarkingStartDate', $r_peerMarkingStartDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=1, $CanEmptyField=0);
$h_peerMarkingEndDate = $linterface->GET_DATE_PICKER('r_peerMarkingEndDate', $r_peerMarkingEndDate, $OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=1, $CanEmptyField=0);

### Peer Marking Grouping
// Number of Group Selection
$numOfPeerMarkingGroup = count((array)$r_peerMarkingGroupingAry);
$maxPeerMarkingGroupCount = floor($numOfSelectedStudent / 2);
$h_numOfPeerMarkingGroupSel = $linterface->Get_Number_Selection('r_numOfPeerMarkingGroup', 1, $maxPeerMarkingGroupCount, $numOfPeerMarkingGroup, $Onchange='', $noFirst=1);

// Warning for editing Peer marking settings
$displayWarning = ($r_writingId > 0)? true : false;
$h_modifyPeerMarkingSettingsWarningDiv = $linterface->Get_Form_Warning_Msg('modifyPeerMarkingSettings_warningDiv', $Lang['W2']['warningMsgArr']['modifyPeerMarkingWarning'], '', $displayWarning);

// Warning for not selecting more than 2 students (peer marking required more than or equal to 2 students)
$displayWarning = ($canApplyPeerMarking)? false : true;
$h_selectStudentForPeerMarkingSettingsWarningDiv = $linterface->Get_Form_Warning_Msg('selectStudentForPeerMarkingSettings_warningDiv', $Lang['W2']['warningMsgArr']['selectStudentBeforeApplyingPeerMarking'], 'warningDiv', $displayWarning);

// Show or hide the peer marking settings table
$settingsDisplayAttr = ($canApplyPeerMarking)? 'block' : 'none';

// Build peer marking grouping table
$groupStudentAry = array();
$studentInfoAry = array();
$oldGroupStudentIdAry = array();
for ($i=0, $i_max=$numOfPeerMarkingGroup; $i<$i_max; $i++) {
	$_groupStudentIdList = $r_peerMarkingGroupingAry[$i];
	$_groupStudentIdAry = explode(',', $_groupStudentIdList);
	
	$groupStudentAry[$i] = $_groupStudentIdAry;
	
	for ($j=0, $j_max=count((array)$_groupStudentIdAry); $j<$j_max; $j++) {
		$_studentId = $_groupStudentIdAry[$j];
		$oldGroupStudentIdAry[] = $_studentId;
	}
}
// newly-added student will be added into the first group
$newStudentIdAry = array_values(array_diff((array)$r_selectedStudentIdAry, (array)$oldGroupStudentIdAry));
$numOfNewStudent = count($newStudentIdAry);

$deletedStudentIdAry = array_values(array_diff((array)$oldGroupStudentIdAry, (array)$r_selectedStudentIdAry));
$numOfDeletedStudent = count($deletedStudentIdAry);

$hideCancelBtn = false;
if ($numOfDeletedStudent > 0) {
	$hideCancelBtn = true;
}
if ($numOfNewStudent > 0) {
	$hideCancelBtn = true;
	
	// get the student info of those newly added students
	$objUser = new libuser('', '', $newStudentIdAry);
	for ($i=0, $i_max=$numOfNewStudent; $i<$i_max; $i++) {
		$_studentId = $newStudentIdAry[$i];
		
		$groupStudentAry[0][] = $_studentId;
	}
}

// load all student info
$allStudentIdAry = array_merge((array)$oldGroupStudentIdAry, (array)$newStudentIdAry);
$objUser = new libuser('', '', $allStudentIdAry);
for ($i=0, $i_max=count($allStudentIdAry); $i<$i_max; $i++) {
	$_studentId = $allStudentIdAry[$i];
	
	if (!in_array($_studentId, (array)$r_selectedStudentIdAry)) {
		// student not selected anymore => don't load the student data
		continue;
	}
	
	$objUser->LoadUserData($_studentId);
	$studentInfoAry[$_studentId]['studentName'] = Get_Lang_Selection($objUser->ChineseName, $objUser->EnglishName);
	$studentInfoAry[$_studentId]['className'] = $objUser->ClassName;
	$studentInfoAry[$_studentId]['classNumber'] = $objUser->ClassNumber;
}

$h_peerMarkingGroupingTable = $w2_libW2->getPeerMarkingGroupingDistribution($groupStudentAry, $studentInfoAry);


if ($hideCancelBtn) {
	$h_cancelBtnDisplay = 'display:none;';
}


$h_header = <<<html
<h1 class="marking">{$Lang['W2']['markingMethod']}</h1>
html;

$h_content = <<<html
<div style="width:98%">
<table>
          <tr>
            <td>{$Lang['W2']['standard']}</td>
            <td>:</td>
            <td>{$Lang['W2']['fullmark']}</td>
            <td><input name="r_fullMark" type="text" id="r_fullMark" value="{$r_fullMark}" size="5" /> <span class="warning tabletextrequire" id="warning_fullmark" style="display:none;">{$Lang['W2']['jsWarningAry']['fullMarkMustBePositive']}</span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>{$Lang['W2']['passmark']}</td>
            <td><input name="r_passMark" type="text" id="r_passMark" value="{$r_passMark}" size="5" /> <span class="warning tabletextrequire" id="warning_passmark" style="display:none;">{$Lang['W2']['jsWarningAry']['passMarkMustBePositive']}</span></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>{$Lang['W2']['lowestmark']}</td>
            <td><input name="r_lowestMark" type="text" id="r_lowestMark" value="{$r_lowestMark}" size="5" /> <span class="warning tabletextrequire" id="warning_lowestmark" style="display:none;">{$Lang['W2']['jsWarningAry']['lowestMarkMustBePositive']}</span></td>
          </tr>
        </table>
	<div class="subtitle"><span>{$Lang['W2']['setMarker']}</span></div>
	<table>
		<tbody>
			<tr>
	            <td colspan="2">{$Lang['W2']['marker']}</td>
	            <td>{$Lang['W2']['markingWeight']}(%)</td>
	            <td>&nbsp;</td>
          	</tr>
			<tr>
	    		<td><input type="checkbox" id="r_allowTeacherMarkingChk" name="r_allowTeacherMarking" onclick="clickedAllowTeacherMarking(this.checked);" {$h_allowTeacherMarkingChecked}></td>
	    		<td><label for="r_allowTeacherMarkingChk">{$Lang['W2']['teacher']}</label></td>
	    		<td><input name="r_teacherWeight" type="text" id="r_teacherWeight" value="{$r_teacherWeight}" size="5" {$h_teacherWeightDisabled}/> <span class="warning tabletextrequire" id="warning_teacherweight" style="display:none;">{$Lang['W2']['jsWarningAry']['weighingFormat']}</span></td>
            	<td>&nbsp;</td>
	  		</tr>		
			<tr>
	    		<td><input type="checkbox" id="r_allowPeerMarkingChk" name="r_allowPeerMarking" onclick="clickedAllowPeerMarking(this.checked);" {$h_allowPeerMarkingChecked}></td>
	    		<td><label for="r_allowPeerMarkingChk">{$Lang['W2']['peer']}</label></td>
	    		<td><input name="r_peerWeight" type="text" id="r_peerWeight" value="{$r_peerWeight}" size="5" {$h_peerWeightDisabled}/> <span class="warning tabletextrequire" id="warning_peerweight" style="display:none;">{$Lang['W2']['jsWarningAry']['weighingFormat']}</span></td>
	    		<td>
	    			<div id="peerMarkingScheduleDiv" class="work_choose_period" style="float:left;{$h_peerMarkingScheduleShow}">
						<div class="input_period table_row_tool"> 
							<span> {$Lang['General']['Start']}</span> <span>{$h_peerMarkingStartDate}</span>
	                		<span> {$Lang['General']['End']}</span> <span>{$h_peerMarkingEndDate}</span>
						</div>
					</div>
	    		</td>
	  		</tr>
	  		<tr><td colspan="4"><span class="warning tabletextrequire" id="warning_totalweight" style="display:none;">{$Lang['W2']['jsWarningAry']['alertTotalWeight']}</span></td></tr>
		</tbody>
	</table>
	<div id="markerSettingDiv" style="{$h_peerMarkingScheduleShow}">
		<div class="subtitle"><span>{$Lang['W2']['peerMarkingSettings']}</span></div>
		{$h_selectStudentForPeerMarkingSettingsWarningDiv}
		<div style="display:{$settingsDisplayAttr};">
			{$Lang['W2']['studentsAreDiviedInto']} {$h_numOfPeerMarkingGroupSel} {$Lang['W2']['group(s)']} <input name="groupNowBtn" type="button" class="formsmallbutton" value="{$Lang['W2']['groupNow']}" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="generateRandomPeerMarkingGrouping();"/>
			{$h_modifyPeerMarkingSettingsWarningDiv}
			<div id="div_within_group_content">
				{$h_peerMarkingGroupingTable}
			</div>
			<br />
			<input type="radio" name="r_peerMarkingTargetType" id="r_peerMarkingTargetTypeRadio_markEveryone" value="{$w2_cfg['DB_W2_WRITING_PEER_MARKING_TARGET_NUM']['markEveryone']}" {$h_studentMarkEveryoneChecked} /><label for="r_peerMarkingTargetTypeRadio_markEveryone"> {$Lang['W2']['eachStudentMarksEveryone']}</label>
			&nbsp;&nbsp;&nbsp;
			<input type="radio" name="r_peerMarkingTargetType" id="r_peerMarkingTargetTypeRadio_markNumOfStudent" value="2" {$h_studentMarkNumChecked} /><label for="r_peerMarkingTargetTypeRadio_markNumOfStudent"> {$Lang['W2']['eachStudentCanMark']} </label><select name="r_peerMarkingTargetNum" id="r_peerMarkingTargetNumSel" onclick="clickedPeerMarkingTargetNum();"><!--optionGeneratedByJs--></select><label for="r_peerMarkingTargetTypeRadio_markNumOfStudent"> {$Lang['W2']['student(s)']}</label>
			<br />
			<input type="checkbox" name="r_showPeerName" id="r_showPeerNameChk" {$h_showPeerMarkingChecked} /><label for="r_showPeerNameChk"> {$Lang['W2']['showPeersNameToMarker']}</label>
		</div>
	</div>
</div>
html;

$h_button = <<<html
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Apply']}" onclick="applyPeerMarkingSettingsFromThickbox();" />
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value="{$Lang['Btn']['Cancel']}" onclick="js_Hide_ThickBox();" style="{$h_cancelBtnDisplay}" />
html;


echo $w2_libW2->getThickboxHtml($h_header, $h_content, $h_button, 'style="background:#fdfdfd;"');
?>