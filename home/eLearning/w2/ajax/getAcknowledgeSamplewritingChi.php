<?php

$PATH_WRT_ROOT_ABS = '../../../../';
include_once($PATH_WRT_ROOT."includes/global.php");
intranet_auth();

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Writing Samples</title>

<script type="text/javascript">
<!--
$( document ).ready(function() {
    $(".writing_grp").click(function(){
		if(!$(this).hasClass("empty")){
			$(".bubble_writing").css("visibility","hidden");
			if(!$(this).hasClass("current")){ //open it
				$(this).next().css("visibility","visible");
			}
			$(".writing_grp").removeClass("current");
			if($(this).next().css("visibility")=="visible"){
				$(this).addClass("current");
			}
			
		}
	});
    $(".bubble_writing .btn_close").click(function(){
		$(this).parents().find('.bubble_writing').css("visibility","hidden");
		$(this).parents().find('.writing_grp').removeClass("current");
	});	

	$('.board_mid_header').find('a').click(function(){
		$('a.current').removeClass('current');
		$(this).addClass('current');
		var page = $(this).attr('data-page-info');
		$('.tabContent').hide();
		$('#'+page).show();
	});	
});
//-->
</script>
</head>

<body style="background:none; overflow:hidden">

<div class="content_pop_board_samplewriting" id="samplewriting_chi">
	<div class="board_top">
        <div class="board_title">中文寫作範文</div>
    </div>
	<div class="board_mid" style="height:395px">
	    <div class="board_mid_main">
    		<div class="board_mid_header"><a class="current" data-page-info="sample_pop">文體</a><a data-page-info="sample_desc">寫作文體要求</a></div>
        	<div class="board_mid_content tabContent" id="sample_pop">
           <div class="board_mid_content_list">
                <!-- #01 -->
                <a class="writing_grp ">人物描寫</a>
                <div id="writing_grp01" class="bubble_writing right" style="width:420px;">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_1&sampleLang=b5')">人見人愛的小女孩</a></li>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_2&sampleLang=b5')">穿畢業長袍的長者</a></li>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_3&sampleLang=b5')">我的啟蒙老師</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_4&sampleLang=b5')">模範生</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_5&sampleLang=b5')">我最害怕的人</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_6&sampleLang=b5')">以第一人稱角度，<br />記一位電視新聞報導員</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_7&sampleLang=b5')">家鄉的爺爺</a></li>
                        
                    </ul>
                    <ul style="position:absolute; left:220px; top:0px; width:200px;">

                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_8&sampleLang=b5')">我的同學</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_9&sampleLang=b5')">一位愛出風頭的同學</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_10&sampleLang=b5')">我的鄰居</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_11&sampleLang=b5')">一位小人物</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_12&sampleLang=b5')">我的兒時玩伴</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_13&sampleLang=b5')">一位令我初則畏懼，後卻心悅誠服的訓導老師</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C01_14&sampleLang=b5')">一位校工</a></li>
                    </ul>
                </div>
                
                <!-- #02 -->
                <a class="writing_grp ">半命題，人物描寫</a>
                <div id="writing_grp02" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C02_1&sampleLang=b5')">一個有愛心的服務員</a></li>
                    </ul>
                </div>

                <!-- #03 -->
                <a class="writing_grp ">半命題，自由題</a>
                <div id="writing_grp03" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C03_1&sampleLang=b5')">我的父親</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C03_2&sampleLang=b5')">尋找快樂</a></li>
                    </ul>
                </div>
                
                <!-- #04 -->
                <a class="writing_grp ">半命題，說明</a>
                <div id="writing_grp04" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C04_1&sampleLang=b5')">提升學習成績，<br />從廣泛閱讀做起</a></li>
                    </ul>
                </div>
                
                <!-- #05 -->
                <a class="writing_grp ">自由題</a>
                <div id="writing_grp05" class="bubble_writing right" style="width:350px;">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_1&sampleLang=b5')">一切從說故事開始</a></li>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_2&sampleLang=b5')">電腦</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_3&sampleLang=b5')">放學以後</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_4&sampleLang=b5')">最珍貴的禮物</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_5&sampleLang=b5')">最感動的一刻</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_6&sampleLang=b5')">塗改液</a></li>
                        
                    </ul>
                    <ul style="position:absolute; left:200px; top:0px; width:150px;">
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_7&sampleLang=b5')">汽水與茶</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_8&sampleLang=b5')">黃昏</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_9&sampleLang=b5')">一顆螺絲</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_10&sampleLang=b5')">鏡子</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C05_11&sampleLang=b5')">笑</a></li>
                        
                    </ul>
                </div>

                <!-- #06 -->
                <a class="writing_grp ">抒情文</a>
                <div id="writing_grp06" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C06_1&sampleLang=b5')">童年</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C06_2&sampleLang=b5')">各散東西</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C06_3&sampleLang=b5')">小一成績單</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C06_4&sampleLang=b5')">全家幅</a></li>
                    </ul>
                </div>

                <!-- #07 -->
                <a class="writing_grp ">書信</a>
                <div id="writing_grp07" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C07_1&sampleLang=b5')">一封勸爸爸戒煙的信</a></li>
                    </ul>
                </div>


                <!-- #08 -->
                <a class="writing_grp ">記敍文</a>
                <div id="writing_grp08" class="bubble_writing right" style="width:420px;">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul style=" width:200px;">
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_1&sampleLang=b5')">一次有驚無險的經歷</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_2&sampleLang=b5')">農曆新年的經歷</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_3&sampleLang=b5')">校園生活點滴</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_4&sampleLang=b5')">記一次乘坐地下鐵路的經驗</a></li>
                        
                    </ul>
                    <ul style="position:absolute; left:220px; top:0px; width:200px;">
						<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_5&sampleLang=b5')">再見</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_6&sampleLang=b5')">記一次秋日遠足的經驗</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C09_7&sampleLang=b5')">記一次當義工的經過</a></li>
                        
                    </ul>
                </div>
				<!-- #9 -->
                <a class="writing_grp ">敘事抒情</a>
                <div id="writing_grp9" class="bubble_writing right" style="width:630px;">
	                <a href="#" class="btn_close" title="Close" style="z-index:2;"> </a>
                	<ul style=" width:220px;">
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_1&sampleLang=b5')">我的家庭樂</a></li>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_2&sampleLang=b5')">記一次暑期活動的經歷和感受</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_3&sampleLang=b5')">記一次遲到的經過和感受</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_4&sampleLang=b5')">寫出某同學受學校紀律處分的始末和感受</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_5&sampleLang=b5')">記一次遲到的經歷和感受</a></li>
                        
                    </ul>
                    <ul style="position:absolute; left:240px; top:0px; width:200px;">
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_6&sampleLang=b5')">考試成績發下來</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_7&sampleLang=b5')">那一次，我錯了</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_8&sampleLang=b5')">一場小風波</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_9&sampleLang=b5')">一件令我尷尬的事</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_10&sampleLang=b5')">停電時</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_11&sampleLang=b5')">童年記趣</a></li>
                        
                    </ul>
                    <ul style="position:absolute; left:420px; top:0px; width:200px;">
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_12&sampleLang=b5')">失物記</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_13&sampleLang=b5')">郊遊迷途記</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C10_14&sampleLang=b5')">一次與同學做專題研習有感</a></li>
                    </ul>
                </div>
                
                
            </div>
            
                
			<div class="board_mid_content_list">
            
                <!-- #10 -->
                <a class="writing_grp ">場面描寫</a>
                <div id="writing_grp10" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_1&sampleLang=b5')">超級市場</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_2&sampleLang=b5')">急症室百態</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_3&sampleLang=b5')">瘋狂大減價</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_4&sampleLang=b5')">百貨商場購物記</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_5&sampleLang=b5')">熱鬧的街頭</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_6&sampleLang=b5')">聖誕夜之見聞</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C11_7&sampleLang=b5')">快餐店眾生相</a></li>
                    </ul>
                </div>

                <!-- #11 -->
                <a class="writing_grp ">描寫文</a>
                <div id="writing_grp11" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C12_1&sampleLang=b5')">新市鎮</a></li>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C12_2&sampleLang=b5')">試描寫上學沿途的景物</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C12_3&sampleLang=b5')">歸家途上</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C12_4&sampleLang=b5')">春天</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C12_5&sampleLang=b5')">四季</a></li>
                    </ul>
                </div>
                
                <!-- #12 -->
                <a class="writing_grp ">描寫抒情</a>
                <div id="writing_grp12" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C19_1&sampleLang=b5')">試描寫校園的一角</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C19_2&sampleLang=b5')">一抹彩雲</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C19_3&sampleLang=b5')">美麗的校園</a></li>
                    </ul>
                </div>
                
                <!-- #13 -->
                <a class="writing_grp ">景物描寫</a>
                <div id="writing_grp13" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C13_1&sampleLang=b5')">八號風球下的街道</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C13_2&sampleLang=b5')">太平山下的夜景</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C13_3&sampleLang=b5')">秋色滿校園</a></li>
                    </ul>
                </div>

                <!-- #14 -->
                <a class="writing_grp ">說明文</a>
                <div id="writing_grp14" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C14_1&sampleLang=b5')">談談與父母相處之道</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C14_2&sampleLang=b5')">談談環境保護的重要</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C14_3&sampleLang=b5')">推薦一本好書</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C14_4&sampleLang=b5')">介紹一本你值得向人推薦的課外書</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C14_5&sampleLang=b5')">一則新聞的省思</a></li>
                    </ul>
                </div>
                
                <!-- #15 -->
                <a class="writing_grp ">寫人</a>
                <div id="writing_grp15" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_1&sampleLang=b5')">破壞人</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_2&sampleLang=b5')">這就是我</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_3&sampleLang=b5')">探訪孤兒院</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_4&sampleLang=b5')">一次當義工的經歷</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_5&sampleLang=b5')">弟弟不見了</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_6&sampleLang=b5')">夏夜</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_7&sampleLang=b5')">雨夜</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_8&sampleLang=b5')">石屎森林</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C15_9&sampleLang=b5')">雷電交加的晚上</a></li>
                    </ul>
                </div>

                <!-- #16 -->
                <a class="writing_grp ">論事文</a>
                <div id="writing_grp16" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C16_1&sampleLang=b5')">論自由</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C16_2&sampleLang=b5')">論校規</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C16_3&sampleLang=b5')">論中文科應否以普通話教學</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C16_4&sampleLang=b5')">公德心</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C16_5&sampleLang=b5')">天才與努力</a></li>
                    </ul>
                </div>

                <!-- #17 -->
                <a class="writing_grp ">議論文</a>
                <div id="writing_grp17" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C17_1&sampleLang=b5')">有志者事竟成</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C17_2&sampleLang=b5')">談談你對時下青少年追求名牌的看法</a></li>
                        <li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C17_3&sampleLang=b5')">談交友</a></li>
                    </ul>
                </div>

                <!-- #18 -->
                <a class="writing_grp ">續寫、敘事</a>
                <div id="writing_grp18" class="bubble_writing right">
	                <a href="#" class="btn_close" title="Close"> </a>
                	<ul>
                    	<li><a href="#" onclick="loadSampleWritingThickbox('/home/eLearning/w2/content/sampleWriting/preview.php?code=C18_1&sampleLang=b5')">一杯茶</a></li>
                    </ul>
                </div>

            </div>
  			</div>
			
			<div class="board_mid_writing_content tabContent" id="sample_desc" style="display:none; height:350px">
				<div class="text">
<!-- Texttype start -->
<h2>1.	記敍抒情文</h2><br />
<strong>主題：</strong><br />
<ol start="1">
<li>日常經歷</li>
<li>校園生活</li>
<li>友儕情誼</li>
<li>擬物創作</li>
</ol><br />
<strong>內容：</strong><br />
記敍抒情文，即通過敍述事件抒發感情，敍事要詳略得宜、引人入勝，感情要真摯，並能順應情節的發展自然流露出來。
<br /><br />
<strong>寫作步驟：</strong><br />
<ol start="1">
<li>從題目分析文章的主題，確定記敍人稱，選擇以第一人稱或第三人稱敍述事件。</li>
<li>擬寫大綱，包括「起」、「承」、「轉」及「合」，然後決定記事部分與抒情部分的篇幅及詳略。</li>
<li>鋪排貫穿全文的線索，以時空推移、事件發展的因果關係或人物對事物的情感變化展開敍述。</li>
<li>選定記敍順序，可採用順敍法，按時閒先後或事件發生的經過進行敍述，亦可採用倒敍法，先敍述事件的結果，再記述時間的起因和經過。</li>
<li>引入事件，以直接引用、概括或設置懸念、埋下伏筆的方法回應題目的關鍵詞，引起下文。</li>
<li>承接開首，交代事件的脈絡，即時間、地點、人物、起因、經過和結果六要素。</li>
<li>記敍事件的轉折和變化，交代事件的發展變化對人物心情所產生的影響。在敍述事件的過程中，亦可運用插敍法，對有關的人或事件作補充。</li>
<li>回應題目，呼應開首的內容，抒發感情，總結從事件中所得的感悟。</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>2.	描寫抒情文</h2><br />
<strong>主題：</strong><br />
<ol start="1">
<li>描寫人物及抒發感情</li>
<li>描寫景物及抒發感情</li>
<li>描寫場面及抒發感情</li>
</ol><br />
<strong>內容：</strong><br />
描寫抒情文，即通過描寫的事物帶出感情，可描寫人物的性格及形象特點，以表達對人物的感想；可描寫景物的細節以帶出思考；可描寫場面氣氛、周遭環境及人物活動以表達啟悟。文章須生動而全面地展現描寫對象的特點，抒發真摯的感情。
<br /><br />
<strong>寫作步驟：</strong><br />
<ol start="1">
<li>從題目分析描寫對象的類型，再依據自己會產生的思想感情，確定描寫範疇和描寫重點。</li>
<li>擬寫大綱，包括「起」、「承」、「轉」及「合」，決定描寫部分與抒情部分的篇幅及詳略。</li>
<li>鋪排貫穿全文的線索，按時間的轉移順序、人物步伐的轉移順序或人物視線的轉移順序描寫事物。 </li>
<li>概括描述描寫對象的整體，以直接描寫和正面描寫的方法簡單介紹描寫的事物，勾勒場面的全景。</li>
<li>細緻描寫對象的不同分項，以間接描寫和側面描寫襯托描寫主體的一、兩個特點。如描寫人物時，可採用肖像描寫、行動描寫、語言描寫、心理描寫，或運用比較法表現人物特質；描寫景物時，可運用比喻描寫、擬人描寫、靜態描寫與動態描寫、多角度描寫、多感官描寫等方法描繪景物的特寫，營造氣氛。</li>
<li>在結尾處直接抒發所感所想，亦可邊描寫邊抒情，通過間接抒情的方法表達感受，並以抒發整體感受作結。</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>3.	說明文</h2><br />
<strong>主題：</strong><br />
<ol start="1">
<li>介紹事物</li>
<li>闡述事理</li>
</ol><br />
<strong>內容：</strong><br />
說明文，即介紹事物或闡述事理的文章，主要是對事物的產生、發展、結果、特徵、性質、狀態、功能等進行解釋，具知識性和科學性，着重內容的客觀和準確，文字力求簡明平實。
<br /><br />
<strong>寫作步驟：</strong><br />
<ol start="1">
<li>從題目中找出說明的對象，列出事物的特徵。</li>
<li>擬寫大綱，根據說明對象的特點決定說明結構，可選擇總分式或遞進式組織結構。</li>
<li>若採用總分式結構，可先對事物作總體的交代或說明，再對事物各方面作分別說明。</li>
<li>若採用遞進式結構，可按照事物發展的規律、過程、原則等安排層次的遞升或遞降，如按由淺入深、由表及裏、由次到主、由近到遠、由一般到個別、由逐層剖析事物。</li>
<li>根據說明對象的特點，選擇適當的說明順序，按時間順序、空間順序或邏輯順序有條理地鋪排內容。</li>
<li>運用舉例說明、數據說明、分類說明、描述說明、比喻說明等方法具體說明事物的特點。</li>
<li>總結全文，突出事物的特點，闡明事物所藴含的意義或道理。</li>
</ol><br /><br />
<!-- Texttype end -->
<!-- Texttype start -->
<h2>4.	論說文</h2><br />
<strong>主題：</strong><br />
<ol start="1">
<li>個人價值</li>
<li>品德情意</li>
<li>處世態度</li>
<li>社會時事</li>
</ol><br />
<strong>內容：</strong><br />
論說文，即提出對某一論題的看法或意向，以說服別人接納自己的見解，包括論點和論據兩項元素。論點是指作者對論題的主張、立場與見解；論據是指作者用以支持論點的理據及例子。
<br /><br />
<strong>寫作步驟：</strong><br />
<ol start="1">
<li>從題目中找出討論的問題，確立自己的取向，選擇立論或駁論的論證形式，或正反兼論。</li>
<li>立場可以是贊成、反對、部分贊成，或有條件下的贊成，切忌立場模稜兩可。</li>
<li>開首道破題旨，可以解釋、引言、比喻或問答等方法引出中心論點，統攝全文。</li>
<li>承接開首的中心論點，加以引申和發揮。可把中心論點分成幾個方面論證，以一個段落論述一個分論點。</li>
<li>一個分論點包括中心句、闡釋句、例子及小總結。例子能包括事例及語例尤佳，須是現實生活中的個人經驗。</li>
<li>就每個分論點提出論據和論證，利用引證法、例證法、對比法、歸納法、因果論證、比喻論證、正反論證等方法，層層深入立論。</li>
<li>歸納出全文的總論，回應開首的中心論點，重申立場。或以問題的形式收結，引人深思。</li>
</ol><br />
<!-- Texttype end -->

			  </div>
       	  </div>
 
        	<br style="clear:both" />
    	</div>
    </div>

	<div class="board_bottom"></div>
</div>


</body>
</html>
