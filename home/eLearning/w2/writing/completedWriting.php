<?php
// using: ivan
include_once($PATH_WRT_ROOT."includes/w2/libWritingStudent.php");

$r_contentCode = $_GET['r_contentCode'];

$objDb = new libdb();



### Legend display
$h_legend = $w2_libW2->htmlLegendInfo(array($w2_cfg["legendArr"]["studentNeedCheckComment"], $w2_cfg["legendArr"]["peerCommented"]));


### Get the Student Writing Info of this Content
//$studentWritingInfoArr = $w2_libW2->findStudentWritingByContent($w2_thisStudentID, $r_contentCode, $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"]);
$studentWritingInfoArr = $w2_libW2->findStudentWritingByContent($w2_thisStudentID, $r_contentCode, null, $withinPeriod=false);
$numOfStudentWriting = count($studentWritingInfoArr);


### Get Student Wrinting Step Info
$studentStepStatusInfoArr = $w2_libW2->getStudentWritingStepStatus($w2_thisStudentID, $writingIdAry=null, $stepIdAry=null);
$studentStepStatusAssoArr = BuildMultiKeyAssoc($studentStepStatusInfoArr, array('WRITING_ID', 'STEP_CODE'));


### Get Peer Marking Data
$peerMarkingDataInfoAry = $w2_libW2->getStudentPeerMarkingTargetData($markerUserId=null, $writingIdAry=null, $markedOnly=true, $w2_thisStudentID);
$peerMarkingDataAssoAry = BuildMultiKeyAssoc($peerMarkingDataInfoAry, array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
unset($peerMarkingDataInfoAry);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			WRITING_NAME varchar(255) default null,
			SCHEME_NAME_LINK text default null,
			SCHEME_NAME text default null,
			COMPLETION_DATE varchar(255) default null,
			COMMENT text default null
		) ENGINE=InnoDB Charset=utf8
";
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);


$insertAry = array();
for ($i=0, $i_max=$numOfStudentWriting; $i<$i_max; $i++) {
	$_writingId = $studentWritingInfoArr[$i]['WRITING_ID'];
	$_writingName = $studentWritingInfoArr[$i]['TITLE'];
	$_schemeCode = $studentWritingInfoArr[$i]['SCHEME_CODE'];
	$_completionDate = getDateByDateTime($studentWritingInfoArr[$i]['HANDIN_SUBMIT_STATUS_DATE']);
	$_completionDate = (is_date_empty($_completionDate))? $Lang['General']['EmptySymbol'] : $_completionDate;
	
	$_linkId = 'schemeNameLink_'.$_writingId;
	$_paraAssoAry = array();
	$_paraAssoAry['mod'] = 'handin';
	$_paraAssoAry['task'] = 'viewHandIn';
	$_paraAssoAry['r_contentCode'] = $r_contentCode;
	$_paraAssoAry['r_writingId'] = $_writingId;
	$_paraAssoAry['r_tabTask'] = $task;
	$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
	$_onMouseOut = 'onmouseout="hideFloatLayer();"';
	$_onMouseOver = 'onmouseover="loadWritingIntroFloatLayer(\''.$_writingId.'\', \''.$_linkId.'\', \'layer_ex_brief\');"';
	$_schemeName = $w2_libW2->getSchemeNameBySchemeCode($_schemeCode,$r_contentCode);
	$_schemeNameLink = '<a id="'.$_linkId.'" href="?'.$_para.'" '.$_onMouseOut.' '.$_onMouseOver.'>'.$_schemeName.'</a>';
	
	
	$_iconAry = array();
	
	# Check if there are comments in any one of the steps
	$_hasTeacherComment = false;
	foreach((array)$studentStepStatusAssoArr[$_writingId] as $_stepId => $_studentStepInfoAry) {
		if ($_studentStepInfoAry['COMMENT_ID']) {
			$_hasTeacherComment = true;
			break;
		}
	}
	if ($_hasTeacherComment) {
		$_iconAry[] = '<a href="?'.$_para.'"><span class="commented" style="margin:0 auto; float:left;"></span></a>';
	}
	
	$_showPeerCommentIcon = false;
	if (isset($peerMarkingDataAssoAry[$_writingId])) {
		$_showPeerCommentIcon = true;
	}
	
	if ($_showPeerCommentIcon) {
		// find stepCode of the last step
		$_lastStepCode = '';
		foreach((array)$studentStepStatusAssoArr[$_writingId] as $_stepId => $_studentStepInfoAry) {
			$_lastStepCode = $_studentStepInfoAry['STEP_CODE'];
		}
		
		$_paraAssoAry['r_schemeCode'] = $_schemeCode;
		$_paraAssoAry['r_stepCode'] = $_lastStepCode;
		$_paraLastStep = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
		
		$_iconAry[] = '<a href="?'.$_paraLastStep.'">'.$w2_libW2->getIconPeerCommentHtml($floatLeft=true).'</a>';
	}
	
	$_commentDisplay = '';
	if (count($_iconAry) > 0) {
		$_commentDisplay .= implode('', $_iconAry);
	}
	else {
		$_commentDisplay .= '<span style="float:left;">'.$Lang['General']['EmptySymbol'].'</span>';
	}
	
	
	
	$insertAry[$i] = "";
	$insertAry[$i] .= " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_schemeNameLink)."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."', '".$_completionDate."' , '".$objDb->Get_Safe_Sql_Query($_commentDisplay)."')";
}


$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}



### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

$objTableMgr->addColumn('WRITING_NAME', 1, $Lang['W2']['exerciseName'], '25%');

### 20140925 Change Wording
$objTableMgr->addColumn('SCHEME_NAME_LINK', 1, $Lang['W2']['topic'], '25%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SCHEME_NAME');
//$objTableMgr->addColumn('SCHEME_NAME_LINK', 1, $Lang['W2']['theme'], '25%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='SCHEME_NAME');
$objTableMgr->addColumn('COMPLETION_DATE', 1, $Lang['W2']['completedDate'], '35%');

### 20140925 Change Wording From Teacher's Comment To Comment
$objTableMgr->addColumn('COMMENT', 0, $Lang['W2']['comment'], '15%', $headerClass='', $dataClass='icon_status');

$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode);

//$objTableMgr->setDisplayResultSet($arr);
//$objTableMgr->setResultSetTotalRecord($int);


$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());

$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$linterface->LAYOUT_START();
include_once('templates/writing/'.$task.'.tmpl.php');
$linterface->LAYOUT_STOP();
?>