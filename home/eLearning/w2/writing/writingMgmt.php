<?
/* !!!!!!!!!!!!!!!!!IMPORTANT NOTE START!!!!!!!!!!!!!!!!!
 * 
 * 	Please update modification log after edit any line below [Date, Name, IP ver. and case number if any]!!
 * 
 * !!!!!!!!!!!!!!!!!!IMPORTANT NOTE END!!!!!!!!!!!!!!!!!!
 * Editing by Siuwan
 * 
 * Modification Log:
 * 2015-02-11 (Siuwan) [ip.2.5.6.3.1]
 *		- added REAL_SCHEME_NAME in temp table to sort topic
 */
//include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");

### Cookies handling
$arrCookies = array();
$arrCookies[] = array("ck_w2_mgmt_accessStatus", "r_accessStatus");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$ck_w2_mgmt_accessStatus = $r_accessStatus = $w2_cfg["writingAccessStatus"]["allWriting"];
}
else {
	updateGetCookies($arrCookies);
}

$r_contentCode = $_GET['r_contentCode'];
$r_returnMsgKey = $_GET['r_returnMsgKey'];


### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_GET['curPage']))? $_GET['curPage'] : 1;
$numPerPage = (isset($_GET['numPerPage']))? $_GET['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_GET['sortColumnIndex']))? $_GET['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_GET['sortOrder']))? $_GET['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];


$objDb = new libdb();
$objWritingTeacher = new libWritingTeacher($w2_thisUserID);

### Toolbar
if ($w2_libW2->isAdminUser()) {
	$h_newBtn = '<a href="/home/eLearning/w2/index.php?mod=admin&task=editWriting&r_contentCode='.$r_contentCode.'">'.$Lang['Btn']['New'].'</a>';
}

### Filtering
if ($w2_thisIsAdmin) {
	$r_accessStatus = ($r_accessStatus=='')? $w2_cfg["writingAccessStatus"]["allWriting"] : $r_accessStatus;
	$h_writingAccessStatusSelection = $w2_libW2->getWritingAccessStatusSelection('accessStatusSel', 'r_accessStatus', $r_accessStatus, $onchange='reloadPage();');
	$accessiableOnly = ($r_accessStatus==$w2_cfg["writingAccessStatus"]["allWriting"])? false : true;
}
else {
	$accessiableOnly = true;
}


### Get the Teacher Writing Info of this Content
$teacherWritingInfoArr = $objWritingTeacher->findTeacherWritingByContent($r_contentCode, $accessiableOnly);
//debug_r($teacherWritingInfoArr);
$numOfTeacherWriting = count($teacherWritingInfoArr);


### Get the Student of each Writing
$writingAllStudentAry = $w2_libW2->getWritingStudentInfoAry($r_contentCode);
$writingAllStudentAssoAry = BuildMultiKeyAssoc($writingAllStudentAry, array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$writingCompletedStudentAry = $w2_libW2->getWritingStudentInfoAry($r_contentCode, null, $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"]);
$writingCompletedStudentAssoAry = BuildMultiKeyAssoc($writingCompletedStudentAry, array('WRITING_ID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
$writingIdAry = array_keys($writingAllStudentAssoAry);


### Get Last Step info of each students of each writings 
$writingToBeMarkedStudentAssoAry = $w2_libW2->getWritingToMarkStudent($writingIdAry);


### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			WRITING_NAME varchar(255) default null,
			WRITING_NAME_LINK text default null,
			SCHEME_NAME varchar(255) default null,
			START_DATE varchar(255) default null,			
			END_DATE varchar(255) default null,
			/* RECORD_STATUS varchar(255) default null, */
			COMPLETED text default null,
			TO_MARK_DISPLAY text default null,
			TO_MARK int(8) default 0,
			REAL_SCHEME_NAME varchar(255) default null
		) ENGINE=InnoDB Charset=utf8
";
//debug_r($sql);
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);


### Prepare Temp Table Data
$insertAry = array();
for ($i=0, $i_max=$numOfTeacherWriting; $i<$i_max; $i++) {
	$_writingId = $teacherWritingInfoArr[$i]['WRITING_ID'];

	$_writingName = $teacherWritingInfoArr[$i]['TITLE'];
	$_writingNameLink = '<a href="/home/eLearning/w2/index.php?mod=admin&task=editWriting&r_contentCode='.$r_contentCode.'&r_writingId='.$_writingId.'">'.$_writingName.'</a>';

	$_schemeCode = $teacherWritingInfoArr[$i]['SCHEME_CODE'];
//	$_schemeName = '<a href="?mod=handin&task=viewHandIn&r_contentCode='.$r_contentCode.'&r_writingId='.$_writingId.'">'.$_schemeCode.'</a>';
//	$_schemeName = '<a href="javascript:void(0);">'.$w2_libW2->getSchemeNameBySchemeCode($_schemeCode).'</a>';
	
	$_spanId = 'schemeNameSpan_'.$_writingId;
	$_onMouseOut = 'onmouseout="hideFloatLayer();"';
	$_onMouseOver = 'onmouseover="loadWritingIntroFloatLayer(\''.$_writingId.'\', \''.$_spanId.'\', \'layer_ex_brief\');"';
	$_schemeName = $w2_libW2->getSchemeNameBySchemeCode($_schemeCode,$r_contentCode);
	$_schemeNameWithSpan = '<span id="'.$_spanId.'" '.$_onMouseOut.' '.$_onMouseOver.'>'.$_schemeName.'</span>';
	$_startDate = getDateByDateTime($teacherWritingInfoArr[$i]['START_DATE_TIME']);
	$_startDateDisplay = (is_date_empty($teacherWritingInfoArr[$i]['START_DATE_TIME']))? $Lang['General']['EmptySymbol'] : $_startDate;
	
	$_endDate = getDateByDateTime($teacherWritingInfoArr[$i]['END_DATE_TIME']);
	$_endDateDisplay = (is_date_empty($teacherWritingInfoArr[$i]['END_DATE_TIME']))? $Lang['General']['EmptySymbol'] : $_endDate;
	
	$_writingStatus = $w2_libW2->getStatusTextByCode($teacherWritingInfoArr[$i]['RECORD_STATUS']);
	
	
	$_paraAssoAry = array();
	$_paraAssoAry['mod'] = 'writing';
	$_paraAssoAry['task'] = 'writingStudentMgmt';
	$_paraAssoAry['r_contentCode'] = $r_contentCode;
	$_paraAssoAry['r_writingId'] = $_writingId;
	$_paraAssoAry['r_handinSubmitStatus'] = $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"];
	$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
	$_completedStudentCount = count((array)$writingCompletedStudentAssoAry[$_writingId]);
	$_completedStudentLink = '<a href="?'.$_para.'">'.$_completedStudentCount.'</a>';
	
	$_allStudentCount = count((array)$writingAllStudentAssoAry[$_writingId]);
	unset($_paraAssoAry['r_handinSubmitStatus']);
	$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
	$_allStudentLink = '<a href="?'.$_para.'">'.$_allStudentCount.'</a>';
	
	$_completed = $_completedStudentLink.' / '.$_allStudentLink;
	
	$_toMarkStudentCount = count((array)$writingToBeMarkedStudentAssoAry[$_writingId]);
	$_paraAssoAry = array();
	$_paraAssoAry['mod'] = 'writing';
	$_paraAssoAry['task'] = 'writingStudentMgmt';
	$_paraAssoAry['r_contentCode'] = $r_contentCode;
	$_paraAssoAry['r_writingId'] = $_writingId;
	$_paraAssoAry['r_handinSubmitStatus'] = $w2_cfg["DB_W2_WRITING_STUDENT_HANDIN_SUBMIT_STATUS"]["submitted"];
	$_paraAssoAry['r_fromToBeMarked'] = 1;
	$_para = $w2_libW2->getUrlParaByAssoAry($_paraAssoAry);
	$_toMarkDisplay = '<a href="?'.$_para.'">'.$_toMarkStudentCount.'</a>';
	
	
	//$insertAry[$i] = " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."', '".$_startDate."', '".$_endDate."', '".$objDb->Get_Safe_Sql_Query($_writingStatus)."', '".$_completed."', '".$_toMark."') ";
	$insertAry[$i] = " ('".$objDb->Get_Safe_Sql_Query($_writingName)."', '".$objDb->Get_Safe_Sql_Query($_writingNameLink)."', '".$objDb->Get_Safe_Sql_Query($_schemeNameWithSpan)."', '".$_startDateDisplay."', '".$_endDateDisplay."', '".$_completed."', '".$_toMarkDisplay."', '".$_toMarkStudentCount."', '".$objDb->Get_Safe_Sql_Query($_schemeName)."') ";
	
}

### Insert data into Temp Table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
//	debug_r($sql);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}


### Set Display Table Object's Attributes
$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PAGE');
$objTableMgr->setCurPage($curPage);
$objTableMgr->setNumPerPage($numPerPage);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);

//$objTableMgr->addColumn('WRITING_NAME', 1, $Lang['W2']['exerciseName'], '25%');
//$objTableMgr->addColumn('SCHEME_NAME', 0, $Lang['W2']['theme'], '25%');
//$objTableMgr->addColumn('START_DATE', 0, $Lang['W2']['startDate'], '10%');
//$objTableMgr->addColumn('END_DATE', 0, $Lang['W2']['endDate'], '10%');
//$objTableMgr->addColumn('RECORD_STATUS', 0, $Lang['General']['Status'], '10%');
//$objTableMgr->addColumn('COMPLETED', 0, $Lang['W2']['completed'], '10%', $headerClass='', $dataClass='', $extraAttribute='style="text-align:center;"');
//$objTableMgr->addColumn('TO_MARK', 0, $Lang['W2']['toMark'], '10%', $headerClass='', $dataClass='', $extraAttribute='style="text-align:center;"');
$objTableMgr->addColumn('WRITING_NAME_LINK', 1, $Lang['W2']['exerciseName'], '27%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='WRITING_NAME');

### 20140925 Change theme => topic
$objTableMgr->addColumn('SCHEME_NAME', 1, $Lang['W2']['topic'], '25%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='REAL_SCHEME_NAME');
//$objTableMgr->addColumn('SCHEME_NAME', 1, $Lang['W2']['theme'], '25%');
$objTableMgr->addColumn('START_DATE', 1, $Lang['General']['StartDate'], '12%');
$objTableMgr->addColumn('END_DATE', 1, $Lang['General']['EndDate'], '12%');
$objTableMgr->addColumn('COMPLETED', 0, $Lang['W2']['submitted'].' / '.$Lang['W2']['totalStudents'], '14%', $headerClass='', $dataClass='', $extraAttribute='style="text-align:center;"');
$objTableMgr->addColumn('TO_MARK_DISPLAY', 1, $Lang['W2']['needComment'], '10%', $headerClass='', $dataClass='', $extraAttribute='style="text-align:center;"', $sortDbField='TO_MARK');


$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $objTableMgr->getPaginationReplaceCode()));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$r_contentCode);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());
$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/writing/writingMgmt.tmpl.php');
$linterface->LAYOUT_STOP();
?>