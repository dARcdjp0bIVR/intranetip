<script language="javascript">
$(document).ready( function() {
	$("#goBtn").click(function(){
		$("form#w2_form input[id=mod]").val("report");
		$("form#w2_form input[id=task]").val("listWritingClassReport");
		$('form#w2_form').submit();
	});
	$("#backBtn").click(function(){
		$("form#w2_form input[id=mod]").val("report");
		$("form#w2_form input[id=task]").val("listWritingReport");
		$('form#w2_form').submit();
	});	
});
function resetThemeSelection(){
<? if($r_contentCode==strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){ ?>
	$('#id_contentSuperTypeSlt').val('');
	$('#id_contentTypeSlt').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
	$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');	
<? }else{ ?>
	$('#id_contentTypeSlt').val('');
	$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');	
<?}?>	
}
function updateSchemeCodeSlt(typeValue,contentCode){
	$('input#mod').val('ajax');
	$('input#task').val('refreshContentDetails');
	var superTypeVal = $("#id_contentSuperTypeSlt").val();
	superTypeVal = superTypeVal || '';

	$.ajax({
	    type: "POST",
	    url: "/home/eLearning/w2/index.php",
	    data: $('#w2_form').serialize() + '&r_typeValue=' +typeValue + '&r_superTypeVal='+superTypeVal,
	    success: function(json) {

				if (typeof (JSON) == 'undefined') {
				    eval("var schemeCodeObj = " + json);
				}
				else {
				    var schemeCodeObj = JSON.parse(json);
				}			

				$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
				$('#r_writing').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
				$.each(schemeCodeObj, function(_schemeCode, _schemeName) {
					$('<option value="'+_schemeCode+'">'+_schemeName.name+'</option>').appendTo('#r_schemeCode');
				});
				
	    }
	  });
}
function updateWritingSlt(schemeCode,contentCode){
	$('input#mod').val('ajax');
	$('input#task').val('refreshWritingList');

	$.ajax({
	    type: "POST",
	    url: "/home/eLearning/w2/index.php",
	    data: $('#w2_form').serialize() + '&r_schemeCode=' +schemeCode,
	    success: function(data) {
			$('#spanWritingSelection').html(data);
	    }
	  });
}
function updateContentTypeSlt(typeSuperValue,contentCode){

		$('input#mod').val('ajax');
		$('input#task').val('refreshTypeDetails');
		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + '&r_typeSuperValue=' +typeSuperValue,
		    success: function(json) {

					if (typeof (JSON) == 'undefined') {
					    eval("var typeCodeObj = " + json);
					}
					else {
					    var typeCodeObj = JSON.parse(json);
					}			

					$('#id_contentTypeSlt').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
					$.each(typeCodeObj, function(_typeCode, aryValue) {
						$('<option value="'+_typeCode+'">'+aryValue.type+'</option>').appendTo('#id_contentTypeSlt');
					});
		    }
		  });

}
</script>
<br/>

<form id="w2_form" name="w2_form" method="post">
	<div class="main_top_blank print_hide"></div>
	<p class="spacer"></p>
    <div class="tableinfo tableinfo_report print_hide">
    	<div id="table_filter">
			<?=$h_gradeSelection?>
			<?=$h_select1?>
			<?=$h_select2?>
			<?=$h_selectSchemeCode?>
			<span id="spanWritingSelection"><?=$h_selectExercise?></span>
		  	<input type="button" id="goBtn" name="goBtn" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<?=$Lang['W2']['btn']['go']?>"/>
		</div>
	</div>		
	<div class="Content_tool Content_tool_report print_hide"><a href="javascript:void(0);" class="print" onclick="window.print();"><?=$Lang['Btn']['Print']?></a><a href="<?=$exportAllLink?>" class="export"><?=$Lang['W2']['exportAll']?></a><a href="<?=$exportCSVLink?>" class="export"><?=$Lang['W2']['exportCSV']?></a></div>
	<?=$h_tableResult?>
	<?=$h_floatingLayer?>
	<p class="spacer"></p>
    <div class="edit_bottom print_hide" style="background:none">
		<input id="backBtn" name="backBtn" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Back']?>" />
    </div>
	
	<input type="hidden" id="task" name="task" value="listWritingReport" />
	<input type="hidden" id="mod" name="mod" value="report" />
	<input type="hidden" id="r_contentCode" name="r_contentCode" value="<?=$r_contentCode?>" />
</form>