<script>
$(document).ready( function() {	
	$(".resource_tab").remove();
	$("#user_name_tool").remove();
	$(".instruction").html($(".static_content .content").html());
	$(".static_content").remove();
});

</script>

<div class="write_board">
	<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span><?=$curStep?></span><?=$Lang['W2']['step'.$curStep.'Title'][$r_contentCode]?></div></div></div>
	<div class="write_board_left">
		<div class="write_board_right">
        	<div class="instruction"> <!-- New class 20140113 --><?=$instruction?></div>
        	<div class="subtitle_new"><span><?=$Lang['W2'][$r_contentCode.'MyWriting']?></span></div>
    		<div class="content">
    			<div class="draft">
        			<div class="write">
        				<div id="templateStructure">
        				<?=$displayHandinCommentHTML?>
							<div id="paper_template" class="template_<?=$templateID?>">
	     						<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
	        						<div class="pt_top_element"></div>
	       						</div></div></div>
	     						<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
	     						
	        						<div class="paper_main_content">
	        							<?=$studentWritingRecord['ANSWER']?>
	            						<p class="spacer"></p>
						            </div>
						        </div></div></div>
	       						<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg"></div></div></div>
	        					<div id="template_clipart"><span class="clipart_<?=$clipArtID?>"></span></div>
	    					</div>
						</div>
                    	<br />
                    </div>	
        		</div>
                <!-- start of student attachment -->
                <br style="clear:both" /><br />
				<div id="w2_studentUploadFilePanel">
					<?
					echo $h_fileUpload;
					?>
				</div>
                <!-- end of student attachment -->
              </div>
	           <p class="spacer"></p>
	           <div class="edit_bottom">
	              
	            </div>
		</div>
	</div>
	<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
</div>
<div class="static_content" style="display:none;"><?=$hardCodeContent?></div>