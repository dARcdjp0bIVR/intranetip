<script language="javascript">
$(document).ready( function() {
	$("#goBtn").click(function(){
		$("form#w2_form input[id=mod]").val("report");
		$("form#w2_form input[id=task]").val("listWritingReport");
		$('form#w2_form').submit();
	});
});
function resetThemeSelection(){
<? if($r_contentCode==strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){ ?>
	$('#id_contentSuperTypeSlt').val('');
	$('#id_contentTypeSlt').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
	$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');	
<? }else{ ?>
	$('#id_contentTypeSlt').val('');
	$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');	
<?}?>	
}
function updateSchemeCodeSlt(typeValue,contentCode){

		$('input#mod').val('ajax');
		$('input#task').val('refreshContentDetails');
		var superTypeVal = $("#id_contentSuperTypeSlt").val();
		superTypeVal = superTypeVal || '';

		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + '&r_contentCode=' + contentCode + '&r_typeValue=' +typeValue + '&r_superTypeVal='+superTypeVal,
		    success: function(json) {

					if (typeof (JSON) == 'undefined') {
					    eval("var schemeCodeObj = " + json);
					}
					else {
					    var schemeCodeObj = JSON.parse(json);
					}			

					$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
					$.each(schemeCodeObj, function(_schemeCode, _schemeName) {
						$('<option value="'+_schemeCode+'">'+_schemeName.name+'</option>').appendTo('#r_schemeCode');
					});
		    }
		  });
}
function updateContentTypeSlt(typeSuperValue,contentCode){

		$('input#mod').val('ajax');
		$('input#task').val('refreshTypeDetails');
		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + '&r_contentCode=' + contentCode + '&r_typeSuperValue=' +typeSuperValue,
		    success: function(json) {

					if (typeof (JSON) == 'undefined') {
					    eval("var typeCodeObj = " + json);
					}
					else {
					    var typeCodeObj = JSON.parse(json);
					}			

					$('#id_contentTypeSlt').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
					$.each(typeCodeObj, function(_typeCode, aryValue) {
						$('<option value="'+_typeCode+'">'+aryValue.type+'</option>').appendTo('#id_contentTypeSlt');
					});
		    }
		  });

}
</script>
<br/>

<form id="w2_form" name="w2_form" method="post">
	<div class="main_top_blank"></div>
	<p class="spacer"></p>
    <div class="tableinfo tableinfo_report">
    	<div id="table_filter">
			<?=$h_gradeSelection?>
			<?=$h_select1?>
			<?=$h_select2?>
			<span id='schemeCode_Span'>
			 <?=$h_selectSchemeCode?>
			 </span>
		  	<input type="button" id="goBtn" name="goBtn" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<?=$Lang['W2']['btn']['go']?>"/>
		</div>
		<div class="Conntent_search"><input type="text" name="keyword" id="keyword" placeholder="<?=$Lang['W2']['exerciseName']?>" value="<?=$keyword?>"/></div>  
	</div>		
	<?=$h_tableResult?>
	<?=$h_floatingLayer?>
	<br/>
	
	<input type="hidden" id="task" name="task" value="listWritingReport" />
	<input type="hidden" id="mod" name="mod" value="report" />
	<input type="hidden" id="r_contentCode" name="r_contentCode" value="<?=$r_contentCode?>" />
</form>