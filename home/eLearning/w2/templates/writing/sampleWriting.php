
<div class="write_board">
	<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>3</span>Related Text-type and Vocabulary</div></div></div>
	<div class="write_board_left"><div class="write_board_right">
    	<div class="instruction"> <!-- New class 20140113 --><?=$step3Instruction?></div>
        <div class="subtitle_new"><span><?=$Lang['W2']['sampleWriting']?></span></div> <!-- New class 20140113 -->
        <div class="btn_right_grp_new"">
        <?if($hasTeacherAttachment){?>
        	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['teacherAttachment'])?></h1>','');">
				<span><?=$Lang['W2']['teacherAttachment']?></span>
			</a>
		<?}?>
			<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_vocab" onclick="loadVocabByThickbox('/<?=$r_contentCode?>/<?=$r_schemeCode?>/reference/referenceVocab.php','<h1 class=ref><?=$Lang['W2']['moreVocabulary']?></h1>','');">
				<span><?=$Lang['W2']['moreVocabulary']?></span>
			</a>

			<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_grammar" onclick="loadContentByThickbox('/<?=$r_contentCode?>/<?=$r_schemeCode?>/reference/referenceGrammer.php','<h1 class=ref><?=$Lang['W2']['grammarAnalysis']?></h1>','');">
				<span><?=$Lang['W2']['grammarAnalysis']?></span>
			</a>
		</div>
        <div class="content">
        	<div class="sample_new">
            	<!---- paper start------------>
                <div id="paper_template" class="template_<?=$paddedTemplateID?> paper_template_fit"> <!-- 'paper_template_fit' is for the new structure 20140109 -->
                 	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
                    	<div class="pt_top_element"></div>
                    </div></div></div>
                 	<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">
                    	<div class="paper_main_content">
                    	<?=$step3ParagraphHTML?>
                        <p class="spacer"></p>
                        </div>
                        
                    </div></div></div>
                   	<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">
                    </div></div></div>
                    <div id="template_clipart"><span class="clipart_<?=$paddedClipArtID?>"></span></div>
                </div>
                <div class="remarks remarks_fit"><!-- 'remarks_fit' is for the new structure 20140109 -->
					<?=$step3RemarkHTML?>
				</div>
            </div>
      </div>
      <p class="spacer"></p>
       <div class="edit_bottom">
            <?php
					echo $h_nextStepButton;
					echo $h_cancelButton;
			?> 
		</div>
	</div></div>
	<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
</div>
                        
          <!-- ********* Main  Content end ************ -->