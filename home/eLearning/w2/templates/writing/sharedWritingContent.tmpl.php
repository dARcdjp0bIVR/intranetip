
<?
// using: Siuwan

echo $h_floatingLayer;

?>
<script>
function toggleStickerList() {	
    var target = document.getElementById("item_sticker");
	
    if (target.className == 'sticker_collsape') {
        target.className = 'sticker_expand';
    } else {
        target.className = 'sticker_collsape';
    }
}
</script>
<!-- ************ Main Content Start ************  -->
<div class="main_top_blank"> <!-- for blank div 20140113 -->
	<!--<div class="navigation"><a href="#">Home</a> > English share writing</div>--> <!-- div removed 20140113 -->
</div>
<p class="spacer"></p>
<div class="shareboard">
	<!-- Top Skateboard Background Image Start -->
	<div class="sb_top">
		<div><div></div></div>
	</div>
	<!-- Top Skateboard Background Image End -->
	
	<div class="sb_content">
		<div class="sb_content_right">
			<div class="sb_content_bg">
				<a href="?mod=writing&r_contentCode=<?php echo $r_contentCode ?>&task=listSharingWriting" class="btn_back">
					<span><?php echo $Lang['Btn']['Back']?></span>
				</a>
                <div class="shareboard_theme">
					<h1><?php echo $sharedWriting['WRITING_NAME']?></h1>
					<span class="stu_name"><?php echo $sharedWriting['STUDENT_NAME'].$Lang['W2']['writingEnd']?> (<?php echo $sharedWriting['ClassName']?>-<?php echo $sharedWriting['ClassNumber']?>)</span>
				</div>
				<div class="write">
					<div class="mark_comment">
						<div id="display_mark_comment" class="show_sticker">
							<?php echo $markCommentHTML?>	
						</div>
					</div> <!-- For Spacing adjustment Only-->
	                <!------------>
	                <!--- template paper start----->
	            	 <div id="paper_template" class="template_<?php echo $templateID?>">
	                 	<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">
	                    	<div class="pt_top_element"></div>
	                    </div></div></div>

						<div class="pt_content">
							<div class="pt_content_right">
								<div class="pt_content_bg">
									<div class="paper_main_content">
									<!----------------------------- essay start ----------------------------------------->
									<div class="essay"><?php echo $writingData ?></div>
									<!----------------------------- essay end ----------------------------------------->
									<p class="spacer"></p>
									</div>
								</div>
							</div>
						</div>

						<div class="pt_bottom">
							<div class="pt_bottom_right">
								<div class="pt_bottom_bg"></div>
							</div>
						</div>
						
						<!--------clipart---------->
						<div id="template_clipart">
							<span class="clipart_<?php echo $clipArtID?>"></span>
						</div>
					</div>	
					<!--- template paper end----->
					<p class="spacer"></p>
				</div>
				<div class="shareboard_date">Post Updated: <?php echo $sharedWriting['SHARING_DATE']; ?></div>
			</div>
		</div>
	</div>
	
	<!-- Bottom Skateboard Background Image Start -->
	<div class="sb_bottom">
		<div><div></div></div>
	</div>
	<!-- Bottom Skateboard Background Image End -->

</div>
<!-- ************ Main Content End ************  -->