<? //Using:  
/*
 * 	Log
 * 	Date:	2015-02-11 [Cameron] Change JSLang['Close'] for the selected Lang in loadMainPageSampleWritinghickbox
 */	
?>

<!-- ************ Content Board Start ************  -->
<div>
<form id="w2_form" name="w2_form" method="post">
<div class="subject_main_new" style="height:700px">
    <div class="<?=$frontPagelogoClass?>"></div>
	<div class="logo_plane">
		<div id="obj_line0" class="line0Con">
			<div class="line0"></div>
		</div>
		<div id="obj_plane" class="plane"></div>
	</div>
    <div class="icon_subject_new">
    
    <? 	### 20140925 Hide All Project Writing 2.0 Products, 
    	### if the client brought EasyWriting
    	if($plugin['W2_Old_Name']):?>
		<div class="icon_incentive"></div>
	<? endif; ?>
		<?php if($isFirstLoginToday): ?>
		<!-- New Design Footprint Start-->
        <div id="footprint_chi" class="art_footprint_chi">
        	<span id="pic_1" class="footprint footprint_1"></span>
        	<span id="pic_2" class="footprint footprint_2"></span>
        	<span id="pic_3" class="footprint footprint_3"></span>
        	<span id="pic_4" class="footprint footprint_4"></span>
        	<span id="pic_5" class="footprint footprint_5"></span>
        	<span id="pic_6" class="footprint footprint_6"></span>
       	<span id="pic_7" class="footprint footprint_7"></span>
        	<span id="pic_8" class="footprint footprint_8"></span>
        	<span id="pic_9" class="footprint footprint_9"></span>
        </div>
		<div id="footprint_eng" class="art_footprint_eng">
        	<span id="pic_1" class="footprint footprint_1"></span>
        	<span id="pic_2" class="footprint footprint_2"></span>
        	<span id="pic_3" class="footprint footprint_3"></span>
        	<span id="pic_4" class="footprint footprint_4"></span>
        	<span id="pic_5" class="footprint footprint_5"></span>
        	<span id="pic_6" class="footprint footprint_6"></span>
        	<span id="pic_7" class="footprint footprint_7"></span>
        	<span id="pic_8" class="footprint footprint_8"></span>
        	<span id="pic_9" class="footprint footprint_9"></span>
        	<span id="pic_10" class="footprint footprint_10"></span>
        	<span id="pic_11" class="footprint footprint_11"></span>
        	<span id="pic_12" class="footprint footprint_12"></span>
        	<span id="pic_13" class="footprint footprint_13"></span>
        	<span id="pic_14" class="footprint footprint_14"></span>
        </div>
		<div id="footprint_ls" class="art_footprint_ls">
        	<span id="pic_1" class="footprint footprint_1"></span>
        	<span id="pic_2" class="footprint footprint_2"></span>
        	<span id="pic_3" class="footprint footprint_3"></span>
        	<span id="pic_4" class="footprint footprint_4"></span>
        	<span id="pic_5" class="footprint footprint_5"></span>
        	<span id="pic_6" class="footprint footprint_6"></span>
        	<span id="pic_7" class="footprint footprint_7"></span>
        	<span id="pic_8" class="footprint footprint_8"></span>
        	<span id="pic_9" class="footprint footprint_9"></span>
        	<span id="pic_10" class="footprint footprint_10"></span>
        	<span id="pic_11" class="footprint footprint_11"></span>
        </div>
		<div id="footprint_ls_eng" class="art_footprint_ls_eng">
        	<span id="pic_1" class="footprint footprint_1"></span>
        	<span id="pic_2" class="footprint footprint_2"></span>
        	<span id="pic_3" class="footprint footprint_3"></span>
        	<span id="pic_4" class="footprint footprint_4"></span>
        	<span id="pic_5" class="footprint footprint_5"></span>
        	<span id="pic_6" class="footprint footprint_6"></span>
        	<span id="pic_7" class="footprint footprint_7"></span>
        	<span id="pic_8" class="footprint footprint_8"></span>
        	<span id="pic_9" class="footprint footprint_9"></span>
        	<span id="pic_10" class="footprint footprint_10"></span>
        	<span id="pic_11" class="footprint footprint_11"></span>
        	<span id="pic_12" class="footprint footprint_12"></span>
        	<span id="pic_13" class="footprint footprint_13"></span>
        	<span id="pic_14" class="footprint footprint_14"></span>
        	<span id="pic_15" class="footprint footprint_15"></span>
        	<span id="pic_16" class="footprint footprint_16"></span>
        </div>
		<div id="footprint_sci" class="art_footprint_sci">
        	<span id="pic_1" class="footprint footprint_1"></span>
        	<span id="pic_2" class="footprint footprint_2"></span>
        	<span id="pic_3" class="footprint footprint_3"></span>
        	<span id="pic_4" class="footprint footprint_4"></span>
        	<span id="pic_5" class="footprint footprint_5"></span>
        	<span id="pic_6" class="footprint footprint_6"></span>
        </div>
		<!-- New Design Footprint End -->
		<?php endif ?>
		<div class="art_pole"></div>
		
			<!-- Added 20140527 Start -->
			
			<a class="samplewriting_chi" 
			href="#"></a>
			
			<a class="samplewriting_eng" 
			href="#"></a>
		
			<!-- Added 20140527 End -->

			<?php echo $h_icon ?>



			<!-- Added 20140527 Start -->

			<a class="samplewriting_chi_blank thickbox" 
			href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=592&width=878"
			onclick="loadMainPageSampleWritinghickbox('','','','b5');"
			></a>
			<a class="samplewriting_eng_blank thickbox" 
			href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=592&width=878"
			onclick="loadMainPageSampleWritinghickbox('','','','en');"
			></a>

			<a class="<?php echo $ack_class; ?> thickbox"
			href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=450&width=760"
			onclick="loadAckThickbox('','','');"
			></a>

			<!-- Added 20140527 End -->

			
			<div id="zebra_l" class="art_zebra" style="left:1200px; top:370px"></div>
			<div id="zebra_s" class="art_zebra" style="left:1300px; top:410px; transform:scale(0.7, 0.7); -webkit-transform:scale(0.7, 0.7);"></div>
		</div>
	</div>
<p class="spacer"></p>
<div class="bottom_left"><div class="bottom_right"></div></div>
</div>
<!-- ************ Content Board End ************  -->

<br />
<br />
<br />
<!-- ************ Footer Start ************  -->


<div id="copyright3_main">
	<div class="copy_left">
		Copyright © 2014 BroadLearning Education (Asia) Limited.
		All rights reserved.
	</div>
	<div class="copy_right">
<!--		Powered by <a href="http://www.eclass.com.hk/" target="_blank" title="eClass"><img src="--><?php //echo $web_root_path?><!--/images/2009a/w2/logo_eclass_footer.gif" border="0" align="absmiddle"></a>-->
        <a href="http://www.eclass.com.hk/" target="_blank" title="eClass"><img src="<?php echo $web_root_path?>/images/logo_eclass_footer.png" border="0" align="absmiddle"></a>
	</div>
</div>

<!--
<div id="copyright3_main">
	<div class="copy_left">
		Copyright © <a href="http://www.eclass.com.hk/" target="_blank">
		<img src="<?php echo $web_root_path?>/images/2009a/w2/Resource/190_writing20/logo_bl.png" border="0" align="absmiddle"></a>
		<a href="http://www.eclass.com.hk/" target="_blank">2014 BroadLearning Education (Asia) Limited.</a>
		All rights reserved.
	</div>
	<div class="copy_right">
		Powered by <a href="http://www.eclass.com.hk/" target="_blank" title="eClass"><img src="<?php echo $web_root_path?>/images/2009a/w2/logo_eclass_footer.gif" border="0" align="absmiddle"></a>
	</div>
</div>
-->
<!-- ************ Footer End ************  -->
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="mod" id="mod" value="">
</form>

<script>
	var cntArr = new Array();
	$(document).ready( function() {
		
		$(".icon_incentive").click(function(){
			window.location.href = 'game/play.php';			
		});
		
		// Show Amination Start
		setTimeout ( function() {
			$("#loading").html("<div class='pic_zebra_1'></div><div class='pic_zebra_2'></div><div class='pic_zebra_3'></div><div class='pic_zebra_4'></div><div class='pic_zebra_5'></div><div class='pic_zebra_6'></div><div class='pic_zebra_7'></div><div class='pic_zebra_8'></div><div class='pic_zebra_9'></div><div class='pic_zebra_10'></div><div class='pic_zebra_11'></div><div class='pic_zebra_12'></div><div class='pic_zebra_13'></div>");
		}, 1000);
		setTimeout ( function() {
			$("#obj_line0").addClass("line0_move");
			$("#obj_plane").addClass("plane_move");
			$("#obj_line0").css("width", "422px");
			$("#obj_plane").css("display", "block");
		}, 2000);
		setTimeout ( function() {
			PlaySequence('art_zebra pic_zebra_', 'zebra_l', 13, 30, true);
			PlaySequence('art_zebra pic_zebra_', 'zebra_s', 13, 20, true);
			LoopZebraS();
			LoopZebraL();
		}, 4000);
		// Show Amination End
		
		$('.samplewriting_chi_blank').hover(function(){
			$('.samplewriting_chi').addClass("samplewriting_hover");
		},function(){
			$('.samplewriting_chi').removeClass("samplewriting_hover");
		});
		$('.samplewriting_eng_blank').hover(function(){
			$('.samplewriting_eng').addClass("samplewriting_hover");
		},function(){
			$('.samplewriting_eng').removeClass("samplewriting_hover");
		});
		
	});
	function loadSampleWritingThickbox(filePath) {
		$.ajax({
		  url: filePath,
		  success:(function(data) {
		  $( "#TB_iFrameDiv").html( data );}),
		});
	}
	function loadAckThickbox(filePath, headerHtml, extraParam) {
		loadThickbox('getAcknolwedgementPage', filePath, headerHtml, extraParam, '' , 'w2_form');
	}
	function loadMainPageSampleWritinghickbox(filePath, headerHtml, extraParam, lang) {
		if(lang == "b5"){
			JSLang['Close'] = '關閉';	// global var defined in lang/script.b5.js
			var link = "<?=$plugin['W2_sampleWriting_demo']?'getDemoSamplewritingChi':'getAcknowledgeSamplewritingChi'?>";
		}
		else{
			JSLang['Close'] = 'Close';	// global var defined in lang/script.en.js
			var link = "<?=$plugin['W2_sampleWriting_demo']?'getDemoSamplewritingEng':'getAcknowledgeSamplewritingEng'?>";
		}
		loadThickbox(link, filePath, headerHtml, extraParam, '' , 'w2_form');
	}
	
	
	function LoopZebraS() {
		setTimeout ( function() {
			$("#zebra_s").css("left", "1300px");
			$("#zebra_s").animate( {left: "-500px"}, 9000, "linear", function() {
				LoopZebraS();
			});
		}, 2000);
	}
	function LoopZebraL() {
		setTimeout ( function() {
			$("#zebra_l").css("left", "1200px");
			$("#zebra_l").animate( {left: "-500px"}, 10000, "linear", function() {
				LoopZebraL();
			});
		}, 2500);
	}
	
	
	function PlaySequence(animName, parentDiv, frame, speed, loop) {
		var obj = new Object();
		obj.anim = animName;
		obj.parent = parentDiv;
		obj.current = 1;
		obj.frame = frame;
		obj.loop = loop;
		obj.speed = speed;
		document.getElementById(obj.parent).className = "";
		$("#" + obj.parent).css( { display: "block", opacity: "1" } );
		cntArr.push(obj);
		obj.cnt = setInterval ( function() {
			document.getElementById(obj.parent).className = animName + obj.current;
			obj.current += 1;
			if (obj.current > obj.frame) {
				if (obj.loop == true) {
					obj.current = 1;
				} else {
					clearInterval(obj.cnt);
					FadeOut(obj.parent, 500, 1000);
					var self = -1;
					for (var i = 0; i < cntArr.length; i++) {
						if (cntArr[i] == obj) {
							self = i;
						}
					}
					if (self != -1) {
						cntArr.splice(self, 1);
					}
					setTimeout( function() {
						SequenceEnd(parentDiv);
					}, 1000);
				}
			}
		}, obj.speed );
	}
	function SequenceEnd(animPr) {
	}
	
	<?php if($isFirstLoginToday): ?>
	function BlockEntry(type,url) {
		$("[id^='btn_entry_']").css( { pointerEvents: "none" } );
		document.getElementById("btn_entry_" + type).className = type + "_on";
		var pr, cls = "";
		var frm = -1;
		if (type == "ls") {
			pr = "footprint_ls";	// pic_1
			cls = "art_footprint_ls pic_footprint_ls_";
			frm = 11;
		} else if (type == "ls_eng") {
			pr = "footprint_ls_eng";
			cls = "art_footprint_ls_eng pic_footprint_ls_eng_";
			frm = 16;
		} else if (type == "chi") {
			pr = "footprint_chi";
			cls = "art_footprint_chi pic_footprint_chi_";
			frm = 9;
		} else if (type == "eng") {
			pr = "footprint_eng";
			cls = "art_footprint_eng pic_footprint_eng_";
			frm = 14;
		} else if (type == "sci") {
			pr = "footprint_sci";
			cls = "art_footprint_sci pic_footprint_sci_";
			frm = 6;
		}
		setTimeout( function() {
			PlayFootstep(cls, pr, frm, 200, false,url);
		}, 200);
	}
	
	function PlayFootstep(animName, parentDiv, frame, speed, loop,url) {
		var obj = new Object();
		obj.anim = animName;
		obj.parent = parentDiv;
		obj.current = 1;
		obj.frame = frame;
		obj.loop = loop;
		obj.speed = speed;
		cntArr.push(obj);
		$("#" + parentDiv).css("display", "block");
		obj.cnt = setInterval ( function() {
			$("#" + parentDiv + " #pic_" + obj.current).css({ display: "block" });
			obj.current += 1;
			if (obj.current > obj.frame) {
				if (obj.loop == true) {
					obj.current = 1;
				} else {
					clearInterval(obj.cnt);
					//FadeOut(obj.parent, 500, 1000);
					var self = -1;
					for (var i = 0; i < cntArr.length; i++) {
						if (cntArr[i] == obj) {
							self = i;
						}
					}
					if (self != -1) {
						cntArr.splice(self, 1);
					}
					FootstepEnd(parentDiv,url);
				}
			}
		}, obj.speed );
	}
	
	function FootstepEnd(animPr,url) {
		window.location.href = url;
	}
	function FadeOut(objname, duration, delay) {
		if (delay > 0) {
			setTimeout( function () {
				$("#"+objname).animate( { opacity: "0" }, duration );
			}, delay);
		} else {
			$("#"+objname).animate( { opacity: "0" }, duration );
		}
	}
	<?php endif; ?>
</script>