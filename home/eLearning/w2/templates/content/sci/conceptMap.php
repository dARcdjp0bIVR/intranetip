<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
</script>

<div class="write_board">
	<div class="write_board_top_step_right">
		<div class="write_board_top_step_left">
			<div class="title"><span>3</span><?=$Lang['W2']['step3Title'][$contentCode]?></div>
		</div>
	</div>
	<div class="write_board_left">
		<div class="write_board_right">
			<div class="instruction"><?=intranet_undo_htmlspecialchars($step3Data['step3Int'])?></div>
            <div class="subtitle_new"><span><?=$Lang['W2']['myMindMap']?></span></div>
            <div class="btn_right_grp_new">
            <?if($hasTeacherAttachment){?>
				<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
					<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
				</a>
			<?}?>
			</div>
			<div class="content">
				
        		<div class="draft">
            		<div class="write">
					<? if($h_conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]){?>
							<a href="javaScript:void(0)" onclick="<?=$h_conceptMapLink?>" class="conceptmap_frame"><div><span><?=$Lang['W2']['powerBoard']?></span></div></a>                               
					<?
						if($w2_thisAction == $w2_cfg["actionArr"]["preview"]){
							//do nothing 
						}else{
							//display the reset button
					?>
					                              <input name="submit1" type="button" class="formsubbutton" value="Reset" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" style="float:left; margin-left:10px" OnClick = "resetPowerBoard(<?=$h_conceptMapEclassTaskId?>,'eng')"/>
					<?
						}
					?>	
					<?}elseif($h_conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){?>
						<?=$h_fileUpload?>
					<?}else{?>
						 	<a href="javaScript:void(0)" onclick="<?=$h_conceptMapLink?>" class="conceptmap_frame"><div><span><?=$Lang['W2']['powerConcept']?></span></div></a>
					<?
						if($w2_thisAction == $w2_cfg["actionArr"]["preview"]){
							//do nothing 
						}else{
							//display the reset button
					?>
					                              <input name="submit1" type="button" class="formsubbutton" value="Reset" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" style="float:left; margin-left:10px" OnClick = "resetPowerConcept(<?=$h_conceptMapEclassTaskId?>,'eng')"/>
					<?
						}
					?>	
					<?}?>	
                                </div>
                                 <div class="ref">
                                	<?=$w2_h_refHtml[$w2_m_infoboxCode[0]]?>
                                </div>
                                </div>
                                <br style="clear:both" />
                                <?=$step3Data['step3Resource']?>
								</div>
								<p class="spacer"></p>
								<div class="edit_bottom">					
<?php 
echo $h_nextStepButton;
?>
<?php 
echo $h_cancelButton;
?>		
								</div>
							</div>
						</div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
