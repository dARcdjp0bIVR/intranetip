<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function individualDocumentReadyHandling () {
	$("#txtAreaScienceTerm").keyup(function(){
		$("#btnShowOtherStudentVocab").removeAttr("disabled");
		$("#btnShowOtherStudentVocab").css("color","black");	
	});	
	$('#btnShowOtherStudentVocab').bind('click',function(){
		$('#w2_vocabPanel').css({"display":""});
		
	$('#mod').val("ajax");
	$('#task').val("getSuggestedVocabForStudent");

			var StepCode_SuggestVocabForStudent = '<?=$ansPrefix?>4';
			$.ajax({
					url:      "/home/eLearning/w2/index.php",
					type:     "POST",
					data:     $("#handinForm").serialize() + '&r_StepCode_SuggestVocabForStudent=' + StepCode_SuggestVocabForStudent,
					error:    function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
							  },
					success:  function(returnStr){
								$("#w2_vocabSuggestedByStudent").html(returnStr);
							  }
			  });


	});
}
</script>

		<!-- ************ Main Content Start ************  -->
         
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>1</span><?=$Lang['W2']['step1Title'][$contentCode]?></div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="instruction"><?=intranet_undo_htmlspecialchars($step1Data['step1Int'])?></div>                      
                            <div class="subtitle_new"><span><?=$Lang['W2']['analysis']?></span></div>
                        <?if($hasTeacherAttachment){?>
				            <div class="btn_right_grp_new">
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
									<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
								</a>
							</div>   
                        <?}?>                            
                            <div class="content">
	                            <div class="draft">
	                                <div class="write">
	                            	<table>
	                            		<?$ansIdx=1;?>
	                            		<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
	                                	<tr>
	                                		<td><?=$Lang['W2']['purpose']?></td>
	                                		<td>:</td>
	                                		<td>
		                                		<select name="r_question[select][<?=$w2_m_ansCode?>]" <?=$w2_h_answerDefaultDisabledAttr?>>
		                                		<?for($i=0;$i<count($Lang['W2']['PurposeAry']);$i++){?>
		                                		 	<option value="<?=$i+1?>" <?=$w2_libW2->getAnsSelected($w2_s_thisStepAns[$w2_m_ansCode], $i+1)?>>
														<?=$Lang['W2']['PurposeAry'][$i]?>
													</option>
		                                		<?}?>								
			                                 	</select>
		                                 		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode?>', 'select');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" />
	
											  <span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><br/>
												<?=$step1Data['step1Purpose']?>
											  </span>
		                                	  	
	                                		</td>
	                                	</tr>
	                                	<?$ansIdx++;?>
	                                	<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
	                                	<tr>
	                                		<td><?=$Lang['W2']['audience']?></td>
	                                		<td>:</td>
	                                		<td>
	                                			<input name="r_question[text][<?=$w2_m_ansCode?>]" type="text" id="textfield" class="w2_ansInput" size="40" value="<?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?>" <?=$w2_h_answerDefaultDisabledAttr?> onkeyup="checkEnableSubmitBtn();" />
		                                		<input type="button" class="formsmallbutton" value="<?=$Lang['W2']['check']?>" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" onclick="showModelAns('<?=$w2_m_ansCode?>', 'text');" style="<?=$w2_h_checkBtnDefaultDisplayAttr?>" /> 
		                                		<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><?=$step1Data['step1Audience']?></span>
	                                		</td>
	                                	</tr>
	                                	<?$ansIdx++;?>
	                                	<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
	                      				<tr>
	                                      <td><?=$Lang['W2']['keywords']?></td><td>:</td>
										  <td><?=$Lang['W2']['pressEnterToSeparateVocab']?><br/>
											<textarea cols="30" rows="6" class="textbox" name="r_question[textareaWithDelimiterA][<?=$w2_m_ansCode?>]" <?=$w2_h_answerDefaultDisabledAttr?>   onkeyup="showModelAns('<?=$w2_m_ansCode?>', 'textarea');" ><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea>
											<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><br/>&nbsp;</span>
										  </td>
	                                    </tr>
	                                    <?$ansIdx++;?>
	                                    <?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
	                                    <tr><td><?=$Lang['W2']['relatedScienceTerms']?></td>
	                                      <td>:</td>
	                                      <td><?=$Lang['W2']['pressEnterToSeparateVocab']?><br/>
											<textarea cols="30" rows="6" class="textbox" name="r_question[textareaWithDelimiterA][<?=$w2_m_ansCode?>]" <?=$w2_h_answerDefaultDisabledAttr?> id="txtAreaScienceTerm" onkeyup="showModelAns('<?=$w2_m_ansCode?>', 'textarea');"><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea>		
											<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans" style="<?=$w2_h_modelAnsDefaultDisplayAttr?>"><br/>&nbsp;</span>
	                                    </tr>
	                                    <tr>
	                                      <td>&nbsp;</td>
	                                      <td>&nbsp;</td>
	                                      <td><input name="submit3" id="btnShowOtherStudentVocab" type="button" class="formsmallbutton" value="Suggested Vocabulary List" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" disabled="disabled" style="<?=$w2_h_suggestedVocabForStudentButtonStyle?>"></td>
	                                    </tr>          	
	                                </table>
	                                </div>
	                                <div class="ref" id="w2_vocabPanel" style="<?=$w2_h_suggestedVocabForStudentStyle?>">
	                                	<span class="title"><?=$Lang['W2']['vocabulary']?>:</span><br />                                	
	                                    <span class="subtitle"><?=$Lang['W2']['suggestedByTeacher']?></span><br />
	                                   <?
	                                  		$answerArray = array_unique(split("[\n|\r]", $step1Data['step1TeacherVocab']));
											$allSuggestedVocabAry = array_filter($answerArray);
											sort($allSuggestedVocabAry);
											echo implode(", ", $allSuggestedVocabAry);
	                                   ?>
										<br /><br />
										<div id="w2_vocabSuggestedByStudent">
											<?=$h_suggestedVocabForStudent?>
										</div>
	                               </div>
	                      	</div>
	                      	<br style="clear:both;">
	                        <?=$step1Data['step1Resource']?>
                          </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
                                <?php 
								echo $h_nextStepButton;
								?>
								<?php 
								echo $h_cancelButton;
								?>								
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
          <!-- ********* Main  Content end ************ -->