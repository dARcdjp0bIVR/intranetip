<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function individualDocumentReadyHandling() {
	highlightEssay();
	checkEnableSubmitBtnByInfoxbox();
}
</script>
		<!-- ************ Main Content Start ************  -->
          
          
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>2</span><?=$Lang['W2']['step2Title'][$contentCode]?></div></div></div>
						<div class="write_board_left"><div class="write_board_right"><div class="instruction"><?=intranet_undo_htmlspecialchars($step2Data['step2Int'])?></div>   
                            <div class="subtitle"><span><?=$Lang['W2']['referenceMaterials']?></span></div>
                            
                            <div class="content">
                            	<div class="write">
                            	<?$ansIdx=1;?>
	                            		<?$w2_m_ansCode=$ansPrefix.$ansIdx;?>
	                            		
                               		<!-- infobox start-->
                               		<?php
                               			$html = '';
                               			for($i=0;$i<$articleCnt;$i++){
                               				$_articleAry = $step2Data['step2ArticleAry']['articleItem'.$i];
                               				$_infoBoxCode = $w2_m_infoboxCode[$i];
                               				$html .= '<div class="infobox" id="infobox_'.$_infoBoxCode.'">';
                               					$html .= '<input type="hidden" id="infoboxStatus_'.$_infoBoxCode.'" name="r_question[infoboxStatus]['.$_infoBoxCode.']" class="w2_infoxBoxStatusHidden" value="'.$w2_h_infoboxPropertyArr[$_infoBoxCode]['saveStatus'].'" />';
                               					$html .= '<a class="top" title="View article" href="javascript:void(0);" onclick="triggerInfoBoxContent(this);">'.$Lang['W2']['article'].' '.($i+1).'<span class="arrow">&nbsp;</span><span class="done w2_doneSpan" style="'.$w2_h_infoboxPropertyArr[$_infoBoxCode]['doneSpanStyle'].'">['.$Lang['W2']['completed'].']</span><span class="notdone w2_notDoneSpan" style="'.$w2_h_infoboxPropertyArr[$_infoBoxCode]['notDoneSpanStyle'].'">['.$Lang['W2']['incomplete'].']</span></a>';
                               					$html .= '<div class="infobox_content w2_infoxboxContentDiv" id="infobox_content_'.$_infoBoxCode.'" style="display:none;">';
                                   		 			$html .= '<div class="sample_new sample_new_sci">';
	                                   		 			###Paper start
	                                   		 			$html .= '<div id="paper_template" class="template_'.$paddedTemplateID.' paper_template_fit">';
	                                 						$html .= '<div class="pt_top"><div class="pt_top_right"><div class="pt_top_bg">';
	                                    						$html .= '<div class="pt_top_element"></div>';
	                                    					$html .= '</div></div></div>';
	                                 						$html .= '<div class="pt_content"><div class="pt_content_right"><div class="pt_content_bg">';
		                                    					$html .= '<div class="paper_main_content" id="w2_infoboxEssayDiv_'.$_infoBoxCode.'">';
	                                   		 						###Essay n bubble start
	                                   		 						$html .= $w2_libW2->getEssayBubbleHTML($_articleAry);
	                                   		 						$html .= '<p class="spacer"></p>';
	                                        					$html .= '</div>';
	                                    					$html .= '</div></div></div>';
															$html .= '<div class="pt_bottom"><div class="pt_bottom_right"><div class="pt_bottom_bg">';
	                                    					$html .= '</div></div></div>';
	                           		 						$html .= '<div id="template_clipart"><span class="clipart_'.$paddedClipArtID.'"></span></div>';
      													$html .= '</div>';
	                       		 						###Paper end
	                       		 						$html .= '<div class="key_new_sci">';
															$html .= $w2_h_refHtml[$_infoBoxCode];
      													$html .= '</div>';
		                       		 					###Button
														$html .= '<div style="background:none; border-top:#769e56 1px dotted; '.$w2_h_infoboxPropertyArr[$_infoBoxCode]['btnDivStyle'].'" class="layer_comment_btn w2_infoboxBtnDiv">';
															$html .= '<input id="infoboxViewBtn_'.$_infoBoxCode.'" type="button" onmouseout="this.className=\'btn_off\'" onmouseover="this.className=\'btn_on\'" class="btn_off" onclick="saveInfoboxHandling(\''.$_infoBoxCode.'\', \''.$w2_cfg["DB_W2_STEP_HANDIN_ANSWER"]["infoboxHasSubmitted"].'\', \''.$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["draft"].'\');" value="Finished Reading">';
														$html .= '</div>';
														###Button end
                       		 						$html .= '</div>';
                       		 						
                       		 					$html .= '</div>';
                   		 					$html .= '</div>';	
                               			}
                               			echo $html;
                               		?>
                                   	<!-- infobox end-->
                                </div>
                                <br style="clear:both;">
	                       		 <?=$step2Data['step2Resource']?>
                            </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
                                <?php 
								echo $h_nextStepButton;
								?>
								<?php 
								echo $h_cancelButton;
								?>								
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
          <!-- ********* Main  Content end ************ -->