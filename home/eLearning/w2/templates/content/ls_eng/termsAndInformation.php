<script>
function individualDocumentReadyHandling() {
	checkEnableSubmitBtnByInfoxbox();
}
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
</script>
		<!-- ************ Main Content Start ************  -->
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>2</span><?=$Lang['W2']['step2Title'][$contentCode]?></div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="instruction"><?=stripslashes(intranet_undo_htmlspecialchars($step2Data['step2Int']))?></div>
                            <div class="subtitle_new"><span><?=$Lang['W2']['relatedTermsAndInformation']?></span></div>
                             <?if($hasTeacherAttachment){?>
				            <div class="btn_right_grp_new">
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
									<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
								</a>
							</div>   
                        <?}?> 
                            <div class="content">
                            	<div class="write">
                               		<!-- infobox start-->
                               		<?php
                               			
                               			$ansIdx=1;
                               			$tarPath = '/file/w2/reference/cid_'.$cid;
                               			for($i=0;$i<$sourceCnt;$i++){
                               				$_sourceAry = $step2Data['step2SourceAry']['sourceItem'.$i];
                            				$_image = $tarPath.'/'.$_sourceAry['image'];
                            		?>
                               				<div class="infobox" id="infobox_<?=$w2_m_infoboxCode[$i]?>">
												<input type="hidden" id="infoboxStatus_<?=$w2_m_infoboxCode[$i]?>" name="r_question[infoboxStatus][<?=$w2_m_infoboxCode[$i]?>]" class="w2_infoxBoxStatusHidden" value="<?=$w2_h_infoboxPropertyArr[$w2_m_infoboxCode[$i]]['saveStatus']?>" />
												<a class="top" title="<?=$Lang['W2']['viewSource']?>" href="javascript:void(0);" onclick="triggerInfoBoxContent(this);"><?=$Lang['W2']['source'].' '.($i+1)?><span class="arrow">&nbsp;</span><span class="done w2_doneSpan" style="<?=$w2_h_infoboxPropertyArr[$w2_m_infoboxCode[$i]]['doneSpanStyle']?>">[<?=$Lang['W2']['completed']?>]</span><span class="notdone w2_notDoneSpan" style="<?=$w2_h_infoboxPropertyArr[$w2_m_infoboxCode[$i]]['notDoneSpanStyle']?>">[<?=$Lang['W2']['incomplete']?>]</span></a>
												<div class="infobox_content w2_infoxboxContentDiv" id="infobox_content_<?=$w2_m_infoboxCode[$i]?>" style="display:none;">
 													<img src="<?=$_image?>">
													<span class="ques"><?=$Lang['W2']['checkPoint']?></span>
													<div class="ques_content">
														<span class="type"><?=$Lang['W2']['shortQuestions']?></span>
											           	<table width="100%">
											       			<tbody>
									<?php	
											$_qNo = 1;
											$_showAnsJs = "";
											foreach((array)$_sourceAry["questionAry"] as $__questionAry){
												$w2_m_ansCode=$ansPrefix.$ansIdx;
												$_showAnsJs .= " showAndSaveModelAns('".$w2_m_ansCode."', 'textarea');";
												
									?>
																<tr>
											       					<td width="20"><?=$_qNo?>.</td>	<td><?=nl2br($__questionAry['question'])?></td>
											       				</tr>
											       				<tr>
											       					<td>&nbsp;</td>
											        				<td>
											        					<textarea name="r_question[textarea][<?=$w2_m_ansCode?>]" rows="3" class="textbox w2_ansInput" id="textfield" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea>
																		<span id="modelAnsSpan_<?=$w2_m_ansCode?>" class="modelans w2_modelAnsSpan" style="<?=$w2_h_infoboxPropertyArr[$w2_m_infoboxCode[$i]]['modelAnsStyle']?>"><?=nl2br($__questionAry['answer'])?></span>
																	</td>
																</tr>
									<?php			
												$_qNo++;
												$ansIdx++;
											}
									?>	       			
											       			
												   			</tbody>
												   		</table>
				
											      		<!-- button start-->
											      		<div style="background:none; border-top:#769e56 1px dotted; <?=$w2_h_infoboxPropertyArr[$w2_m_infoboxCode[$i]]['btnDivStyle']?>" class="layer_comment_btn w2_infoboxBtnDiv">
											         		<input id="infoboxViewBtn_<?=$w2_m_infoboxCode[$i]?>" type="button" onmouseout="this.className='btn_off'" onmouseover="this.className='btn_on'" class="btn_off" onclick="<?=$_showAnsJs?> saveInfoboxHandling('<?=$w2_m_infoboxCode[$i]?>', '<?=$w2_cfg["DB_W2_STEP_HANDIN_ANSWER"]["infoboxHasSubmitted"]?>', '<?=$w2_cfg["DB_W2_STEP_STUDENT_STEP_STATUS"]["draft"]?>');" value="Check">
											      		</div>
											      		<!-- button end-->
										       		</div>
											   </div>
											</div>
								<?php
                               			}
								?>
                                <!-- infobox end-->
                                </div>
                                <br style="clear:both" />
                                <?=$step2Data['step2Resource']?>
                            </div>
                           <p class="spacer"></p>
                           <div class="edit_bottom">
                                <?php 
								echo $h_nextStepButton;
								?>
								<?php 
								echo $h_cancelButton;
								?>								
                              </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
          <!-- ********* Main  Content end ************ -->