<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadSampleWritingByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_step3SampleWriting', filePath, headerHtml, extraParam, '' , 'handinForm');
}
</script>
<div class="write_board">
	<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>3</span><?=$Lang['W2']['step3Title'][$contentCode]?></div></div></div>
	<div class="write_board_left"><div class="write_board_right">
    	<div class="instruction"> <!-- New class 20140113 --><?=intranet_undo_htmlspecialchars($step3Data['step3Int'])?></div>
        <div class="subtitle_new"><span><?=$Lang['W2']['sampleWriting']?></span></div> <!-- New class 20140113 -->
        <div class="btn_right_grp_new">
        <?if($hasTeacherAttachment){?>
        	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['teacherAttachment'])?></h1>','');">
				<span><?=$Lang['W2']['teacherAttachment']?></span>
			</a>
		<?}?>
			<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=900" class="thickbox btn_ref_new" onclick="loadSampleWritingByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['sampleWriting']))?></h1>','');"><span><?=$Lang['W2']['sampleWriting']?></span></a>
		</div>
        <div class="content">
        	<div class="draft">
            		<div class="write">
            		<?$i=0;?>
            		<table>
            		<?foreach((array)$step3Data['step3OutlineAry'] as  $_value):?>
			        		<tr>
								<td>
									<?=($i+1)?>. <?=stripslashes($_value)?>
								</td>
							</tr>	
                            <?$w2_m_ansCode=$ansPrefix.($i+1);?>	
							<tr>
								<td><textarea name="r_question[textarea][<?=$w2_m_ansCode?>]" rows="5" class="textbox" id="textfield" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea></td>
							</tr>
						<?$i++;?>
            		<?endforeach;?>
            		</table>
            </div>
            <?=$step3Data['step3Resource']?>
      </div>
      <p class="spacer"></p>
       <div class="edit_bottom">
            <?php
					echo $h_nextStepButton;
					echo $h_cancelButton;
			?> 
		</div>
	</div></div>
	<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
</div>
                        
          <!-- ********* Main  Content end ************ -->