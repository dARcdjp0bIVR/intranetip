<?
?>
<script>
$(document).ready( function() {
	$('input.othersChk').click(function(){
		updateOthersTextboxStatus();
	});
	
	updateOthersTextboxStatus();
});
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadMoreVocabByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_moreVocab', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadGrammarAnalysisByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_grammarAnalysis', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadSampleWritingByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_step3SampleWriting', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function individualAnsHandling() {
	$('input.othersTb').attr('disabled', '');
}
</script>

          
            <div class="write_board">
						<div class="write_board_top_step_right"><div class="write_board_top_step_left"><div class="title"><span>4</span><?=$Lang['W2']['step4Title']['chi']?></div></div></div>
						<div class="write_board_left"><div class="write_board_right">
                            <div class="instruction"> <!-- New class 20140113 --><?=intranet_undo_htmlspecialchars($step4Data['step4Int'])?></div>
                            <div class="subtitle_new"><span><?=$Lang['W2']['myApproach']?></span></div>
                            <div class="btn_right_grp_new">
						        <?if($hasTeacherAttachment){?>
						        	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes($Lang['W2']['teacherAttachment'])?></h1>','');">
										<span><?=$Lang['W2']['teacherAttachment']?></span>
									</a>
								<?}?>
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=900" class="thickbox btn_ref_new" onclick="loadSampleWritingByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['sampleWritingApproach']))?></h1>','');"><span><?=$Lang['W2']['sampleWritingApproach']?></span></a>
								<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_vocab_new" onclick="loadMoreVocabByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['moreVocabulary']))?></h1>','');"><span><?=$Lang['W2']['moreVocabularyForEngine']?></span></a>
		                    	<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" class="thickbox btn_ref_new" onclick="loadGrammarAnalysisByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['grammarAnalysis']))?></h1>','');"><span><?=$Lang['W2']['grammarAnalysisForEngine']?></span></a>
							</div>
							<div class="content">
                            	<div class="draft">
                            		<div class="write">
		                                <?$i=0;?>
					            		<table>
					            		<?foreach((array)$step4Data['step4ApproachAry'] as  $_value):?>
								        		<tr>
													<td>
														<?=($i+1)?>. <?=stripslashes($_value)?>
													</td>
												</tr>	
					                            <?$w2_m_ansCode=$ansPrefix.($i+1);?>	
												<tr>
													<td><textarea name="r_question[textarea][<?=$w2_m_ansCode?>]" rows="5" class="textbox" id="textfield" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea></td>
												</tr>
											<?$i++;?>
					            		<?endforeach;?>
					            		</table>
					            		<br />
					            		<?=$step4Data['step4Resource']?>
                                	</div>
                                	<div class="ref" style="height:200px"><?=intranet_undo_htmlspecialchars($step4Data['step4ExampleSentence'])?></div>
                                </div>
                            </div>

                           <p class="spacer"></p>
                           <div class="edit_bottom">                                
							<?
								echo $h_nextStepButton;
								echo '&nbsp;';
								echo $h_cancelButton;
							?>
                           </div>
						</div></div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>