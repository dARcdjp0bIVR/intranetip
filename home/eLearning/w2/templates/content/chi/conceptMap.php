<script>
function loadTeacherAttachmentByThickbox(filePath, headerHtml, extraParam) {
	loadThickbox('getThickboxHtml_attachment', filePath, headerHtml, extraParam, '' , 'handinForm');
}
function loadReferenceByThickbox(filePath, headerHtml, extraParam) {
	var tabNum = $('.current').attr('data-contentNum');
	$('input#saveTab').val(tabNum);
	loadThickbox('getThickboxHtml_step2Reference', filePath, headerHtml, extraParam, '' , 'handinForm');
}
</script>

<script>
$(document).ready( function() {
	$('input.othersChk').click(function(){
		updateOthersTextboxStatus();
	});
	
	updateOthersTextboxStatus();
});

function individualAnsHandling() {
	$('input.othersTb').attr('disabled', '');
}


</script>
<div class="write_board">
	<div class="write_board_top_step_right">
		<div class="write_board_top_step_left">
			<div class="title"><span>2</span><?=$Lang['W2']['step2Title'][$contentCode]?></div>
		</div>
	</div>
	<div class="write_board_left">
		<div class="write_board_right">
			<div class="instruction"><?=intranet_undo_htmlspecialchars($step2Data['step2Int'])?></div>
            <div class="subtitle_new"><span><?=$Lang['W2']['myConcept']?></span></div>
            <div class="btn_right_grp_new">
            <?if($hasTeacherAttachment){?>
				<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=600" onclick="loadTeacherAttachmentByThickbox('','<h1 class=ref><?=addslashes(strToUpper($Lang['W2']['teacherAttachment']))?></h1>','');" class="thickbox btn_ref_new">
					<span><?php echo $Lang['W2']['teacherAttachmentWithBreak']?></span>
				</a>
			<?}?>
				<?$step2ReferenceTopic = !empty($step2Data['step2ReferenceAry']['referenceTopic'])?$step2Data['step2ReferenceAry']['referenceTopic']:'';?>
			<?if(!empty($step2ReferenceTopic)){?>	
				<a href="#TB_inline?modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true&amp;height=400&width=700" onclick="loadReferenceByThickbox('','<h1 class=ref><?=strToUpper($step2ReferenceTopic)?></h1>','');" class="thickbox btn_ref_new">
					<span><?=stripslashes($step2ReferenceTopic)?></span>
				</a>
			<?}?>	
			</div>
			<div class="content">
				
        		<div class="draft">
            		<div class="write">
            		<?$i=0;?>
            		<table>
            		<?foreach((array)$step2Data['step2ConceptAry'] as  $_value):?>
			        		<tr>
								<td>
									<?=($i+1)?>. <?=stripslashes($_value)?>  
								</td>
							</tr>	
                            <?$w2_m_ansCode=$ansPrefix.($i+1);?>	
							<tr>
								<td><textarea name="r_question[textarea][<?=$w2_m_ansCode?>]" rows="5" class="textbox" id="textfield" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode])?></textarea></td>
							</tr>
						<?$i++;?>
            		<?endforeach;?>
            		<tr>
						<td>
						<?=($i+1)?>. <?=$Lang['W2']['conceptMapInstruction'] ?>
 				<div class="write">
					<? if($h_conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["powerboard"]){?>
							<a href="javaScript:void(0)" onclick="<?=$h_conceptMapLink?>" class="conceptmap_frame"><div><span><?=$Lang['W2']['powerBoard']?></span></div></a>                               
					<?
						if($w2_thisAction == $w2_cfg["actionArr"]["preview"]){
							//do nothing 
						}else{
							//display the reset button
					?>
					                              <input name="submit1" type="button" class="formsubbutton" value="Reset" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" style="float:left; margin-left:10px" OnClick = "resetPowerBoard(<?=$h_conceptMapEclassTaskId?>,'eng')"/>
					<?
						}
					?>	
					<?}elseif($h_conceptType==$w2_cfg["DB_W2_CONTENT_DATA_CONCEPTTYPE"]["attachment"]){?>
						<?=$h_fileUpload?>
					<?}else{?>
						 	<a href="javaScript:void(0)" onclick="<?=$h_conceptMapLink?>" class="conceptmap_frame"><div><span><?=$Lang['W2']['powerConcept']?></span></div></a>
					<?
						if($w2_thisAction == $w2_cfg["actionArr"]["preview"]){
							//do nothing 
						}else{
							//display the reset button
					?>
					                              <input name="submit1" type="button" class="formsubbutton" value="Reset" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" style="float:left; margin-left:10px" OnClick = "resetPowerConcept(<?=$h_conceptMapEclassTaskId?>,'eng')"/>
					<?
						}
					?>	
					<?}?>				        	
										                                
                 </div>
                                	  </td>
                               	    </tr>					
	                           				
                                </table>
                                </div>
                                </div>
                                <br style="clear:both" />
                                <?=$step2Data['step2Resource']?>
								</div>
								<p class="spacer"></p>
								<div class="edit_bottom">					
<?php 
echo $h_nextStepButton;
?>
<?php 
echo $h_cancelButton;
?>		
								</div>
							</div>
						</div>
						<div class="write_board_bottom_left"><div class="write_board_bottom_right"></div></div>
					</div>
                        
