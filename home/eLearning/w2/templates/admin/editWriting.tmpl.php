<?php
// using:

?>
<script>
var obj_marking_data = <?= count($peerMarkingInfoAssoAry) > 0 ? $json->encode($peerMarkingInfoAssoAry) : '{}' ?>;
var forceEditPeerMarkingSettings = false;

$(document).ready(function(){
//	$("#addButton").click(function(){
//		MoveSelectedOptions("ClassStudent", "r_selectedStudentId");
//	});

	$("#ClassStudent").width($("#ClassStudent").parent().width() - 30);
	$("#r_selectedStudentId").width($("#r_selectedStudentId").parent().width() - 30);
	
	$("input#AddAll").click(function(){
		$("select#ClassStudent option").each(function(){$(this).attr("selected","selected");}); 
		MoveSelectedOptions("ClassStudent", "r_selectedStudentId");
		forceEditPeerMarkingSettings = true;
	});
	
	$("input#Add").click(function(){
    	MoveSelectedOptions("ClassStudent", "r_selectedStudentId");
    	forceEditPeerMarkingSettings = true;
	});
	
	$("input#Remove").click(function(){
    	js_Clicked_Remove_Selected_Student_Button(0);
    	forceEditPeerMarkingSettings = true;
	});
	
	$("input#RemoveAll").click(function(){
		js_Clicked_Remove_Selected_Student_Button(1);
		forceEditPeerMarkingSettings = true;
	});

	<?=$js_string1?>
	
	$('input#r_markingJsonString').val(JSON.stringify(obj_marking_data));

});

function enableApprovalStepUi(emt){
	var emtId = emt.id;
	var JEmtId = "#" + emtId;

	var thisValue = $(JEmtId).attr('value');
	$("input:checkbox[id^='r_approvalStep_id_']").each(function(){
		if(thisValue == 1){
			//enable the approval step
			$(this).attr('checked',false);
			$(this).attr("disabled", false); 
			$("#w2_div_approvalStep_id").show();
		}else{
			//diable the approval step
			$(this).attr('checked',false);
			$(this).attr("disabled", true); 
			$("#w2_div_approvalStep_id").hide();
		}
	})

}
function handleApprovalStepUi(emt){
	var emtId = emt.id;
	var JEmtId = "#" + emtId;

	//store "this checkbox" attr value
	var thisSequence = $(JEmtId).attr("w2_sequence");
	var thisIsChecked = $(JEmtId).is(':checked');

/*
	if(thisSequence == 1){
		if(thisIsChecked == true){
			alert('you cannot disable it ar!');
			$(JEmtId).attr('checked',false);
		}
	}else{
*/
			 $("input:checkbox[id^='r_approvalStep_id_']").each(function(){
				 var _sequence = $(this).attr("w2_sequence");

				if (thisIsChecked) {
					if(_sequence > thisSequence){
						//1) check all the checkbox after "this checkbox"
						$(this).attr('checked',true);
					}
				}else{
					//user uncheck "this checkbox" , release all other check box after "this checkbox"
					$(this).attr("checked", false); 
				}
			 })			
//	}


	
}
function Get_Class_Student_List() {

//	var YearClassID = $("#YearClassSelect :selected").val();
	var SelectedStudentID = "";
	$("#r_selectedStudentId option").each(function(i){
		SelectedStudentID += "&r_selectedStudentId[]=" + $(this).val();
	});
  
	$("#ClassStudent").find("option").remove().end();
	
	if ($("#YearClassID").val() == '') {
		// no need refresh list if no class is selected
	}
	else {
//		var url = "/home/eLearning/w2/index.php?mod=ajax&task=getStudentList";
//		var postContent = "YearClassID="+YearClassID;
//		postContent += SelectedStudentID;
		
		$('input#mod').val('ajax');
		$('input#task').val('getStudentList');
		
		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + SelectedStudentID,
		    success: function(data) {
		    	var student_arr = eval(data);
		      
		      $.each(student_arr, function(key, obj) {
		        var student_id = obj.UserID;
		        var student_name = obj.StudentName;
		
		        $("#ClassStudent").append($("<option></option>").attr("value",student_id).text(student_name));   
		        $("#ClassStudent").width($("#ClassStudent").parent().width() - 30);
				//Reorder_Selection_List('ClassStudent');
		      });
		    }
		  });
	}
}

function js_Clicked_Remove_Selected_Student_Button(isAll) {
	if (isAll) {
		$('select#r_selectedStudentId > option').remove();
	}
	else {
		$('select#r_selectedStudentId > option:selected').remove();
	}
	
	Get_Class_Student_List();
}

function Reorder_Selection_List(selectId) {
	var selectList = document.getElementById(selectId);

	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
  }
}

function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    var student_id = $(this).val();
    var student_name = $(this).text();
	
    $("#"+to_select_id).append($("<option></option>").attr("value",student_id).text(student_name));
    $(this).remove();
  });
  
  $("#"+from_select_id).width($("#"+from_select_id).parent().width() - 30);
  $("#"+to_select_id).width($("#"+to_select_id).parent().width() - 30);
  Reorder_Selection_List(from_select_id);
  Reorder_Selection_List(to_select_id);
}

function Add_Teaching_Teacher() {
  var selectedOption = $("#r_teachingTeacherList option:selected");
  
  if($(selectedOption).val() != "") {
    $('#r_selectedTeacherId').append($('<option></option>').text($(selectedOption).text()).val($(selectedOption).val()));
    $(selectedOption).remove();
    Reorder_Selection_List('r_selectedTeacherId');
  }
  
  $('#r_selectedTeacherId').width('350');
}

function Remove_Teaching_Teacher() {
	var TeachingStaffList = document.getElementById('r_selectedTeacherId');
	var TeacherList = document.getElementById('r_teachingTeacherList');
	for (var i = (TeachingStaffList.length -1); i >= 0 ; i--) {
		if (TeachingStaffList.options[i].selected) {
			Teacher = TeachingStaffList.options[i];
			TeacherList.options[TeacherList.length] = new Option(Teacher.text,Teacher.value);
			TeachingStaffList.options[i] = null;
		}
	}
	TeacherList.options[0].selected = true;
	//Reorder_Selection_List('r_teachingTeacherList');
} 

function doFormCheck() {
	var canSubmit = true;
	$('div.warningDiv').hide();
	
	//select all characters
	$("#r_selectedStudentId option").attr("selected","selected");
	$("#r_selectedTeacherId option").attr("selected","selected");
	
	// check if all compulsory fields have filled in or not
	var isFirstInvalid = true;
	$('.requiredField').each( function() {
		if (isObjectValueEmpty($(this).attr('id'))) {
			$('div#' + $(this).attr('id') + '_warningDiv').show();
			canSubmit = false;
			
			if (isFirstInvalid) {
				$(this).focus();
				isFirstInvalid = false;
			}	
		}
	});
	
	// check writing time settings
	if (!compareTimeInput(document.getElementById('r_startDate'), document.getElementById('r_startHour'), document.getElementById('r_startMin'), document.getElementById('r_endDate'), document.getElementById('r_endHour'), document.getElementById('r_endMin'))) {
		$('div#submissionInvalidWarnMsg').show();
		canSubmit = false;
		
		if (isFirstInvalid) {
			$('#r_startDate').focus();
			isFirstInvalid = false;
		}
	}
	
//	 check if need to update peer marking settings
	if (obj_marking_data['r_allowPeerMarking'] == 1 && forceEditPeerMarkingSettings) {
		$('div#mustUpdatePeerMarkingSettingsWarnDiv').show();
		canSubmit = false;
	}


	
	if (canSubmit) {		
		$('input#mod').val('admin');
	    $('input#task').val('updateWriting');
		$('form#w2_form').submit();
	}
}

function deleteScheme() {
	if (confirm('<?=$Lang['W2']['warningMsgArr']['deleteWriting']?>')) {
		$('input#mod').val('admin');
		$('input#task').val('deleteWriting');
		$('form#w2_form').submit();
	}
}

function goPreviewWriting() {
	//If a writing scheme is selected
	//The rightmost selection box in Theme
	if($('#r_schemeCode').val()) {
		newWindow('?mod=handin&task=viewHandIn&r_action=<?=$w2_cfg["actionArr"]["preview"]?>&&r_contentCode=<?=$r_contentCode?>&r_schemeCode=' + $('#r_schemeCode').val(), 32);
	}
	else {
		alert('<?=$Lang['W2']['unselectalert']?>');
	}
}

function updateContentTypeSlt(typeSuperValue,contentCode){

		$('input#mod').val('ajax');
		$('input#task').val('refreshTypeDetails');
		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + '&r_contentCode=' + contentCode + '&r_typeSuperValue=' +typeSuperValue,
		    success: function(json) {

					if (typeof (JSON) == 'undefined') {
					    eval("var typeCodeObj = " + json);
					}
					else {
					    var typeCodeObj = JSON.parse(json);
					}			

//					$('#w2_previewSpan').hide();
					$('#id_contentTypeSlt').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
					$.each(typeCodeObj, function(_typeCode, aryValue) {
						$('<option value="'+_typeCode+'">'+aryValue.type+'</option>').appendTo('#id_contentTypeSlt');
					});
//					$('#w2_previewSpan').show();
		    }
		  });

}
function updateSchemeCodeSlt(typeValue,contentCode){

		$('input#mod').val('ajax');
		$('input#task').val('refreshContentDetails');
		var superTypeVal = $("#id_contentSuperTypeSlt").val();
		superTypeVal = superTypeVal || '';

		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + '&r_contentCode=' + contentCode + '&r_typeValue=' +typeValue + '&r_superTypeVal='+superTypeVal,
		    success: function(json) {

					if (typeof (JSON) == 'undefined') {
					    eval("var schemeCodeObj = " + json);
					}
					else {
					    var schemeCodeObj = JSON.parse(json);
					}			

					$('#w2_previewSpan').hide();
					$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
					$.each(schemeCodeObj, function(_schemeCode, _schemeName) {
						$('<option value="'+_schemeCode+'">'+_schemeName.name+'</option>').appendTo('#r_schemeCode');
					});
					$('#w2_previewSpan').show();
		    }
		  });
}

function resetThemeSelection(){
<? if($r_contentCode=='eng'){ ?>
	$('#id_contentSuperTypeSlt').val('');
	$('#id_contentTypeSlt').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');
	$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');	
<? }else if($r_contentCode=='chi'){ ?>
	$('#id_contentTypeSlt').val('');
	$('#r_schemeCode').empty().append('<option value=\'\'> -- <?=$Lang['General']['PleaseSelect']?> -- </option>');	
<?}?>	

	
}
function updateSchemeInstruction(schemeCode , contentCode){

		$('input#mod').val('ajax');
		$('input#task').val('refreshInstructionDetails');
		
		$.ajax({
		    type: "POST",
		    url: "/home/eLearning/w2/index.php",
		    data: $('#w2_form').serialize() + '&r_contentCode=' + contentCode + '&r_schemeCode=' +schemeCode,
		    success: function(strVal) {
					$('#r_introduction').val(strVal);
		    }
		  });
}

function loadMarkingThickbox() {
	// record the currently selected options
	var selectedOptionValueAry = new Array();
	$("#r_selectedStudentId option").each( function () {
		if ($(this).attr('selected')) {
			selectedOptionValueAry[selectedOptionValueAry.length] = $(this).val();
		}
	});
	
	// select all options for form post
	$("#r_selectedStudentId option").attr("selected","selected");
	
	// load the thickbox content
	loadThickbox('getThickboxHtml_peerMarkingSettings', '', '', '', 'dynSizeThickboxHeightAdjustment(); initMarkingThickbox();', 'w2_form');
	
	// retore the original selected status
	$("#r_selectedStudentId option").each( function () {
		if (jQuery.inArray($(this).val(), selectedOptionValueAry) == -1) {
			// not found => not selected originally
			$(this).attr('selected', '');
		}
		else {
			$(this).attr('selected', 'selected');
		}
	});
}

function loadTemplateThickbox() {
	// load the thickbox content
	loadThickbox('getMgmtPopSelectTemplate', '', '', '', 'dynSizeThickboxHeightAdjustment();', 'w2_form');
}

function initMarkingThickbox() {
	initGroupingHandling();
}

function initGroupingHandling() {
	init_within_group_sorting();
	var min_user_num = $('#within_group_min_user').val();
	
	// generate the selection option of type 'within group'
	remove_selection_option('r_peerMarkingTargetNumSel', true);
	var obj = document.getElementById('r_peerMarkingTargetNumSel');
	while(min_user_num > 1){
		var choice = parseInt(min_user_num) - 1;
		
		var _selected = false;
		if (choice == parseInt(obj_marking_data['r_peerMarkingTargetNum'])) {
			_selected = true;
		}
		
		obj.options[obj.length] = new Option(choice, choice, false, _selected);
		min_user_num--;
	}
}

// modified from /home/web/eclass40/eclass40/js/assessment_new2.js function generate_random_peer_marking_grouping()
function generateRandomPeerMarkingGrouping() {
	var groupNum = $('select#r_numOfPeerMarkingGroup').val();
	
	var i = 0;
	var params = '';
	$('select#r_selectedStudentId option').each( function() {
		//params += (i != 0) ? '&' : '?';
		params += '&';
		params += 'r_selectedStudentIdAry[]=' + $(this).val();
	});
	
	$('input#mod').val('ajax');
	$('input#task').val('getPeerMarkingGroupingDisplay');
	
	$('div#div_within_group_content').html(getAjaxLoadingMsg());
	$.ajax({
	    type: "POST",
	    url: "/home/eLearning/w2/index.php",
	    data: $('#w2_form').serialize() + '&' + $("#thickboxForm").serialize() + params,
	    success: function(returnHtml) {
	    	$('div#div_within_group_content').html(returnHtml);
	    	
			initGroupingHandling();
	    }
	});	
}

// copied from /home/web/eclass40/eclass40/js/assessment_new2.js
function init_within_group_sorting(){
	if($('.within_column').length > 0){
		$('.within_column').sortable({
			connectWith: '.within_column'
		});
		$('.within_column').disableSelection();
	}
}
function clickedAllowPeerMarking(allowPeerMarking) {
	if (allowPeerMarking) {
		$('div#peerMarkingScheduleDiv').show();
		$('div#markerSettingDiv').show();
		$('input#r_peerWeight').removeAttr('disabled');
		
	}
	else {
		$('div#peerMarkingScheduleDiv').hide();
		$('div#markerSettingDiv').hide();
		$('input#r_peerWeight').attr('disabled','disabled');
	}
}
function clickedAllowTeacherMarking(allowTeacherMarking) {
	if (allowTeacherMarking) {
		$('input#r_teacherWeight').removeAttr('disabled');
	}
	else {
		$('input#r_teacherWeight').attr('disabled','disabled');
	}
}

function clickedPeerMarkingTargetNum() {
	$('input#r_peerMarkingTargetTypeRadio_markNumOfStudent').click();
}
/*from common.js in eclass40*/
function show_warning_box(div_id){
	$('#'+div_id).show();
}

function hide_warning_box(div_id, div_class){
	if(div_id != ''){
		$('#'+div_id).hide();
	} else {
		$('.'+div_class).hide();
	}
}
function applyPeerMarkingSettingsFromThickbox() {
	var tmpPeerMarkingTargetNum = '';
	hide_warning_box('','warning');
	var r_fullMark = $('#r_fullMark').val();
	var r_passMark = $('#r_passMark').val();
	var r_lowestMark = $('#r_lowestMark').val();
	var r_teacherWeight = $('#r_teacherWeight').val();
	var r_peerWeight = $('#r_peerWeight').val();	
	var r_allowPeerMarking = ($('#r_allowPeerMarkingChk').attr('checked'))? 1 : 0;
	var r_allowTeacherMarking = ($('#r_allowTeacherMarkingChk').attr('checked'))? 1 : 0;
	var total_weight = 0;
	if(r_fullMark != '' && !IsNumeric(r_fullMark)){ show_warning_box('warning_fullmark'); return;}
	if(r_passMark != '' && !IsNumeric(r_passMark)){ show_warning_box('warning_passmark'); return;}
	if(r_lowestMark != '' && !IsNumeric(r_lowestMark)){ show_warning_box('warning_lowestmark'); return;}
	if(r_allowTeacherMarking){
		if(r_teacherWeight == '' || r_teacherWeight.match(/\D/)){ show_warning_box('warning_teacherweight'); return;}
		total_weight += parseInt(r_teacherWeight);
	}else{
		r_teacherWeight = '';
	}
	if(r_allowPeerMarking){
		if(r_peerWeight == '' || r_peerWeight.match(/\D/)){ show_warning_box('warning_peerweight'); return;}
		total_weight += parseInt(r_peerWeight);
	}else{
		r_peerWeight = '';
	}	
	
	if(total_weight != 100){ show_warning_box('warning_totalweight'); return;}
	if ($('input#r_peerMarkingTargetTypeRadio_markEveryone').attr('checked')) {
		tmpPeerMarkingTargetNum = <?=$w2_cfg["DB_W2_WRITING_PEER_MARKING_TARGET_NUM"]["markEveryone"]?>;
	}
	else {
		tmpPeerMarkingTargetNum = $('select#r_peerMarkingTargetNumSel').val();
	}
	
	// store peer marking basic settings
	obj_marking_data = 	{
							'r_allowPeerMarking': r_allowPeerMarking,
							'r_peerMarkingStartDate': $('#r_peerMarkingStartDate').val(),
							'r_peerMarkingEndDate': $('#r_peerMarkingEndDate').val(),
							'r_peerMarkingTargetNum': tmpPeerMarkingTargetNum,
							'r_peerMarkingGroupingAry': new Array(),
							'r_showPeerName': ($('#r_showPeerNameChk').attr('checked'))? 1 : 0,
							'r_fullMark': r_fullMark,
							'r_passMark': r_passMark,
							'r_lowestMark': r_lowestMark,
							'r_teacherWeight': r_teacherWeight,
							'r_peerWeight': r_peerWeight
							
						};
	
	// store peer marking grouping
	// copied from /home/web/eclass40/eclass40/js/assessment_new2.js function serialize_within_group_sorting()
	var cnt = $('#within_group_column').val();
	var groupCnt = 0;
	if(cnt > 0 && r_allowPeerMarking){
		for(var i=0 ; i<cnt ; i++){
			if($('#group_column_'+i).length > 0){
				$('#group_column_'+i).sortable({
					connectWith: '#group_column_'+i
				});
				
				var _sortedStudentIdList = $('#group_column_'+i).sortable('toArray').join(',').replace(/group_user_id_/g, '');
				if (_sortedStudentIdList == '') {
					// do not add the group to json if there are no members
				}
				else {
					obj_marking_data['r_peerMarkingGroupingAry'][groupCnt++] = _sortedStudentIdList;
				}
				
				$('#group_column_'+i).disableSelection();
			}
		}
	}
	
	// update peer marking settings hidden field			
	$('input#r_markingJsonString').val(JSON.stringify(obj_marking_data));
	var str = "<?=$Lang['W2']['markUsing']?> : <?=$Lang['W2']['mark']?>";	
	str += "(<?=$Lang['W2']['passmark']?>: "+r_passMark+", <?=$Lang['W2']['fullmark']?>: "+r_fullMark+", <?=$Lang['W2']['lowestmark']?>: "+r_lowestMark+")";
	str += "<br />";
	str += "<?=$Lang['W2']['marker']?> : ";	
	str += "<?=$Lang['W2']['teacher']?> (";
	str += r_teacherWeight!=''?r_teacherWeight:"<?=$Lang['W2']['na']?>";
	str += "), <?=$Lang['W2']['peer']?> (";
	str += r_peerWeight!=''?r_peerWeight:"<?=$Lang['W2']['na']?>";
	str += ")";
	$('span#marking_display').html(str);
	forceEditPeerMarkingSettings = false;
	 js_Hide_ThickBox();
}
</script>



								<table style="width:100%">
                                <col width="130px" />
                                <col width="10px" />
                                	<tr>
                                	  <td><?=$Lang['W2']['exerciseName']?></td>
                                	  <td>:</td>
                                	  <td>
                                	  	<input type="text" id="r_writingName" name ="r_writingName" value="<?=$title?>"  class="requiredField" size="40" />
                                	  	<?=$h_inputTitleWarning?>
                                	  </td>
                                	</tr>
                                	<?if(in_array($r_contentCode,$w2_cfg['contentInput']['allowContentCode'])):?>
									<tr>
                                      <td><?=$Lang['W2']['grade']?></td>
                                      <td>:</td>
                                     <td>									 
									 <?=$h_gradeSelection?>
									  </td>
                                    </tr>
                                    <?endif;?>                                	
                                    <tr>
                                      <td><?=$Lang['W2']['theme']?></td>
                                      <td>:</td>
                                     <td>									 
									 <?=$h_select1?>
									 <?=$h_select2?>
									 <span id='schemeCode_Span'>
									 <?=$h_selectSchemeCode?>
									 </span><span id='w2_previewSpan'>									 
										&nbsp;<a href="javascript:void(0);" onclick="goPreviewWriting();">[<?=$Lang['Btn']['Preview']?>]</a>
										 <?=$h_inputSchemeCodeWarning?>
										 </span>
									  </td>
                                    </tr>
                                    
                                    <!-- By Adam -->
                                    <tr>
                                      <td><?php echo $Lang['W2']['paperTemplate']?></td>
                                      <td>:</td>
                                     <td>
                                     	<div id="template_using" class="theme_<?php echo str_pad($templateID,  2, "0", STR_PAD_LEFT); ?>"></div>
                                     	<span class="table_row_tool_text">
                                     		<a href="javascript:void(0);" onclick="show_dyn_thickbox_ip(this.title, '', 'modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true', true, '', '', 2, '', 'loadTemplateThickbox();');" class="thickbox tool_edit" title="<?=$Lang['Btn']['Edit']?>">
                                     			<em>&nbsp;</em>
                                     			<span><?php echo $Lang['Btn']['Edit']?></span>
                                     		</a>
                                     	</span><br style="clear:both">
                                     	<input type="hidden" name="r_templateID" id="r_templateID" value="<?php echo $templateID; ?>" / >
                                     	<input type="hidden" name="r_clipArtID" id="r_clipArtID" value="<?php echo $clipArtID; ?>" / >
									  </td>
                                    </tr>
                                    <!-- By Adam -->

                                	<tr>
                                	  <td><?=$Lang['W2']['introduction']?></td>
                                	  <td>:</td>
                                	  <td>
                                	  	<textarea name="r_introduction" rows="3" class="textbox requiredField" id="r_introduction"><?=$introduction?></textarea>
                                	  	<?=$h_inputIntroWarning?>
                                	  </td>
                                	</tr>
                                	<tr>
                                      <td><?=$Lang['W2']['stepApprovalSetting']?></td>
                                	  <td>:</td>
                                	  <td>
										<input name="r_requireStepApproval" type="radio" id="r_requireStepApproval_n" value="0"  onclick="enableApprovalStepUi(this)" <?=$h_noRequireStepApprovalChecked?>/>
										<label for="r_requireStepApproval_n"><?=$Lang['W2']['stepApprovalSettingNoApproval']?></label>
										<br/>
										<input type="radio" name="r_requireStepApproval" id="r_requireStepApproval_y" value="1" onclick="enableApprovalStepUi(this)" <?=$h_isRequireStepApprovalChecked?>/>
										<label for="r_requireStepApproval_y"><?=$Lang['W2']['stepApprovalSettingRequireApproval']?></label>&nbsp;&nbsp;&nbsp;<br />
										<span class="approve_pad" id="w2_div_approvalStep_id" style="<?=$h_approvalStepStyleDisplay?>">
											<?=$h_approvalStep?>
										</span>
										</td>
                              	  </tr>
                                    <tr>
                                      <td><?=$Lang['W2']['conceptMap']?></td>
                                      <td>:</td>
                                      <td>
                                      	<input type="radio" name="r_conceptMap" id="rd_conceptMap_yes" value="1" <?=$h_defaultConceptYes?> /> <label for="rd_conceptMap_yes"> <?=$Lang['W2']['showDefaultConceptMap']?></label>
                                      	&nbsp;&nbsp;&nbsp;
                                      	<input name="r_conceptMap" type="radio" id="rd_conceptMap_no" value="0" <?=$h_defaultConceptNo?> /><label for="rd_conceptMap_no"> <?=$Lang['W2']['noDefaultConceptMap']?></label></td>
                                    </tr>
                                    <tr>
                                      <td><?=$Lang['W2']['target']?></td>
                                      <td>:</td>
                                      <td>
									  <!--input type="radio" name="radio2" id="radio" value="radio" /> Individual&nbsp;&nbsp;&nbsp;<input name="radio2" type="radio" id="radio" value="radio" checked="checked" /> Group
                                      <br />
                                      <textarea name="textarea2" id="textarea2" rows="4" style="width:250px">All</textarea>
                                      <br />
                                      <a href="#">Select target</a> | <a href="#">Deselect all</a-->
									  <!--hr/-->
									 
									  <table width="100%" border="0" cellspacing="0" cellpadding="5">
							  			<tr>
							  				<td width="50%" bgcolor="#EEEEEE"><?=$h_year_class_selection?></td>
											<td width="40">&nbsp;</td>
											<td width="50%" bgcolor="#EFFEE2" class="steptitletext"><?=$Lang['SysMgr']['FormClassMapping']['StudentSelected']?> </td>
										</tr>
							
										<tr>
											<td bgcolor="#EEEEEE">
							  					<div id="classStudentSelDiv">
							  						<select name="ClassStudent" id="ClassStudent" size="10" multiple="true" width="250" style="width: 250px"></select>
							  					</div>
							  					<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?> </span>
							          		</td>
							          		<td align="center" valign="middle">
									            <input id="AddAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddAll']?>"/><br />
									            <input id="Add" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddSelected']?>"/><br /><br />
									            <input id="Remove" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveSelected']?>"/><br />
									            <input id="RemoveAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveAll']?>"/>
									        </td>
							          		<td bgcolor="#EFFEE2">
							          		 	<select multiple="true" size="10" id="r_selectedStudentId" name="r_selectedStudentId[]" class="requiredField" width="250">
							          		 		<?=$h_selectedStudent?>
							          		 	</select>
							          		 	<?=$h_selectStudentWarning?>
							          		</td>
							        	</tr>
							     	  </table>
									  </td>
                                    </tr>
                                    <tr>
                                      <td><?=$Lang['General']['Teacher']?></td>
                                      <td>:</td>
                                      <td>
										  <?=$h_teaching_teacher_selection?>
	                                      <br />
										  <select multiple="true" id="r_selectedTeacherId" size="5" name="r_selectedTeacherId[]" class="requiredField" style="width:300px">
										  	<?=$h_selectedTeacher?>
										  </select><br/>
										  <?=$h_removeSelectedTeacherBtn?>
										  <br />
										  <?=$h_selectTeacherWarning?>
                                    </tr>
                                    <tr>
                                      <td><?=$Lang['W2']['markingMethod']?></td>
                                      <td>:</td>
                                      <td>
                                      	<span class="table_row_tool_text">
                                      		<a href="javascript:void(0);" class="tool_edit" onclick="show_dyn_thickbox_ip(this.title, '', 'modal=true&amp;forW2=true&amp;KeepThis=true&amp;TB_iframe=true', true, '', '', 2, '', 'loadMarkingThickbox();');" title="<?=$Lang['Btn']['Edit']?>"><em>&nbsp;</em><span><?=$Lang['Btn']['Edit']?></span></a>
                                      	</span>
                                      	<br style="clear:both;" />
                                      	<span id="marking_display"><?=$h_markingDisplay?></span>
                                      	<?=$h_mustUpdatePeerMarkingSettingsWarning?>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td><?=$Lang['General']['Time']?></td>
                                      <td>:</td>
                                      <td>
                                      	<div class="work_choose_period" style="float:left">
											<div class="input_period table_row_tool"> 
											<!--
												<span> Start date</span> <span><input name="textfield" type="text" id="textfield" size="12" /></span> <span><a href="#" class="calendar" title="Select from Calendar"></a></span> <span><input name="textfield" type="text" id="textfield" size="1" />:<input name="textfield" type="text" id="textfield" size="1" /></span> 				
												<span> End date</span> <span><input name="textfield" type="text" id="textfield" size="12" /></span> <span><a href="#" class="calendar" title="Select from Calendar"></a></span> <span><input name="textfield" type="text" id="textfield" size="1" />:<input name="textfield" type="text" id="textfield" size="1" /></span>
											-->
											<span> <?=$Lang['General']['StartDate']?></span> <span><?=$h_startDate?></span> <span><?=$h_startHour?> : <?=$h_startMin?></span>
											<span> <?=$Lang['General']['EndDate']?></span> <span><?=$h_endDate?></span> <span><?=$h_endHour?> : <?=$h_endMin?></span>
											</div>
										</div>
										<br style="clear:both;" />
										<?=$h_submissionInvalidWarning?>
									  </td>
                                    </tr>
                                    
                                    <tr>
                                      <td><?=$Lang['W2']['status']?></td>
                                      <td>:</td>
                                      <td>
                                      	<?=$h_publicSel?>
                                      	&nbsp;&nbsp;&nbsp;
                                      	<?=$h_privateSel?>
									  </td>
                                    </tr>
                                    <!-- change to hidden form field temporarily
                                    <input type="hidden" id="r_recordStatus" name="r_recordStatus" value="<?=$w2_cfg["DB_W2_WRITING_RECORD_STATUS"]["public"]?>" />
                                    -->
                                    <!--tr>
                                      <td>Attachment(s)</td>
                                      <td>:</td>
                                      <td>
											 <table class="sub">
												<thead class="attach">
												<tr>
												  <td>&nbsp;</td>
												  <td>Name of attachment</td>
												  <td>Source</td>
												  <td>&nbsp;</td>
												</tr>
												</thead>
												<tr><td><img src="images/icon_attachment2.gif"></td>
												  <td><input name="textfield" type="text" id="textfield" size="40" /></td>
												  <td><input name="textfield" type="text" id="textfield" size="30" /> <input name="submit3" type="button" class="formsmallbutton" value="Browse" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'"/></td>
												  <td class="table_row_tool"><a href="#" class="tool_delete_dim" title="Delete">&nbsp;</a></td>
												</tr>
												<tr>
												  <td><img src="images/icon_attachment2.gif"></td>
												  <td><input name="textfield" type="text" id="textfield" size="40" /></td>
												  <td><input name="textfield" type="text" id="textfield" size="30" />
													<input name="submit" type="button" class="formsmallbutton" value="Browse" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'"/></td>
												  <td class="table_row_tool"><a href="#" class="tool_delete_dim" title="Delete">&nbsp;</a></td>
												</tr>
												<tr>
												  <td>&nbsp;</td>
												  <td class="Content_tool_190"><a href="#" class="new">Add more</a></td>
												  <td class="Content_tool_190">&nbsp;</td>
												  <td>&nbsp;</td>
												</tr>
											</table>
									</td>
                                    </tr-->
                                </table>
<input type = "hidden" name="task" id="task" value="">
<input type = "hidden" name="mod" id="mod" value="">
<input type = "hidden" name="r_contentCode" id="r_contentCode" value="<?=$r_contentCode?>">
<input type = "hidden" name="r_writingId" id="r_writingId" value="<?=$r_writingId?>">
<input type = "hidden" name="r_markingJsonString" id="r_markingJsonString" value="">
