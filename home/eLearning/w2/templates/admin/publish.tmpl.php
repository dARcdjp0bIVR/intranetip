<script language="javascript">
$(document).ready( function() {
	//check all button
	$('#checkall:checkbox').change(function () {
		if($(this).attr("checked")) 
		{
			$('input:checkbox').attr('checked','checked');
		}
		else
		{
			$('input:checkbox').removeAttr('checked');
		}
	});
	
	//check the all button if all the checkboxes are checked
	$('input:checkbox').change(function () {
		var hasCheckedAll = true;
		$('.contentSelected:checkbox').map(function () {
  			if ( !$(this).attr('checked') )
			{
				hasCheckedAll=false;	
			}
		}).get();
		
		if(hasCheckedAll) 
		{
			$('#checkall:checkbox').attr('checked','checked');
		}
		else
		{
			$('#checkall:checkbox').removeAttr('checked');
		}
	});

	//check if buttons are checked and send the request
	$('#publishSelectContent').click(function(){
		var hasChecked;
		$('input:checkbox').map(function () {
  			if ($(this).attr('checked') )
			{
				hasChecked=true;	
			}
		}).get();
		
		if(hasChecked)
		{
			$('input#mod').val('admin');
			$('input#task').val(this.id);
			$('form#form1').submit();
		}
		else
		{
			alert('Please check the required item(s)');
		}
	});
});

function reloadPage() {
	$('form#form1').submit();
}


</script>
<br/>

<form id="form1" name="form1" method="post">
	
	<br/>
	<div id="btnDiv">
		<input type="button" id="publishSelectContent" value="<?=$Lang['W2']['publishPart']?>" />
	</div>
	<?=$h_tableResult?>

	<br />

	<div id="publishDiv">	
	</div>
	
	
	<?=$h_floatingLayer?>
	<br/>
	
	
<!--	<input type="hidden" id="task" name="task" value="publish_handler" /> -->
	<input type="hidden" id="task" name="task" value="publish" />
	<input type="hidden" id="mod" name="mod" value="admin" />
	<input type="hidden" id="r_contentCode" name="r_contentCode" value="<?=$r_contentCode?>" />
</form>