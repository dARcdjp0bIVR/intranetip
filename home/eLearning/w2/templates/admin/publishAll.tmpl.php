<script language="javascript">

$( document ).ready(function() {
	<?=$js?>
	if(task.length>0){
		task.reverse(); 
		transferToSchool(task);
	}
});

function Hide_Return_Message() {
	$("div.SystemReturnMessage").css("visibility","hidden");
	clearTimeout(TimeoutObj);
}


function transferToSchool(taskDetails){
	if(taskDetails.length > 0){
		data = taskDetails[taskDetails.length-1];
		Get_Return_Message("1|=|Publish in progress...");
		$('#' + data).html('<img src="/home/eLearning/w2/test/processing.gif" border = "0" height="40" width="49"/>');
		$.ajax({
		type: "POST",
		url: "?mod=admin&task=publish_handler",
		data: {schemeInfo: data[0], cid: data[1], r_contentCode:'<?=$r_contentCode?>'},
		timeout:120000,
		success:function( msg ) 
			{
				if(msg)
				{
					$('#' + data).html('<font color="green">DONE</font>');
					taskDetails.pop();
					transferToSchool(taskDetails);
				}
				else
				{
					$('#' + data).html('<font color="red">FAIL</font>');
					taskDetails.pop();
					transferToSchool(taskDetails);
				}
			},
		error:function( msg ) 
			{
				$('#' + data).html('<font color="red">FAIL</font>');
				taskDetails.pop();
				transferToSchool(taskDetails);
			}
		});
	}else{
		Get_Return_Message("1|=|Content(s) shared.");
	}


}

function backToPublish() {
	$('input#mod').val('admin');
	$('input#task').val('publish');
	$('form#form1').submit();
}
function reloadPage() {
	$('form#form1').submit();
}



</script>
<br/>
<form id="form1" name="form1" method="get">
	<?=$h_writingAccessStatusSelection?>
	<?=$h_tableResult?>
<div class="usermgmt_table_190">
<div class="table_board">
<div class="table_top_left">
<div style="height:10px" class="table_top_right">
</div>
</div>
<div class="table_left">
	<div class="table_right">
		<table class="common_table_list">
			<colgroup>
				<?=$h_publishTableWidthSetter?>
			</colgroup>
			<thead>
				<tr class="">
					<?=$h_publishTableHeader?>
				</tr>
			</thead>
			<tbody>
				<?=$h_publishTableContent?>
			</tbody>
		</table>
	</div>
</div>
	<div class="table_bottom_left">
		<div class="table_bottom_right">
			<div class="common_table_bottom"></div>
		</div>
	</div>
</div>
</div>


	<br />
	
	<br />
	<br />
	<hr />
	<br />
	<?=$h_backButton?>
	<?=$h_floatingLayer?>
	<br/>
	
	<!-- publish all -->
	<input type="hidden" id="task" name="task" value="publishAll" />
	<input type="hidden" id="mod" name="mod" value="admin" />
	<input type="hidden" id="r_contentCode" name="r_contentCode" value="<?=$r_contentCode?>" />
</form>