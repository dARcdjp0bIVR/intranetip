<script language="javascript">
$(document).ready( function() {
	
});
function deleteContent(cid)
{
	$('input#cid').val(cid);
	$('input#mod').val('admin');
	$('input#task').val('deleteContent');
	$('form#form1').submit();
}
function editContent(cid)
{	
	$('input#cid').val(cid);
	$('input#mod').val('admin');
	$('input#task').val('newTemplateWriting');
	$('form#form1').submit();
	
}
function displayFloatLayer(ObjID, LayerID, DateModified)
{
	var content = "<b><?php echo $Lang['W2']['lastPublished'];?>:</b><br/>";
	content += "<span style='color:red;' >";
	content += DateModified+"<br />";
	content += "</span>";
	
	
	var leftAdjustment = (is_ie7)? 120 : 0;
	var topAdjustment = (is_ie7)? 3 : 20;
	changeLayerPosition(ObjID, LayerID, leftAdjustment, topAdjustment);
	showFloatLayer(content);
	$('#layer_ex_brief').width('150px');
	
}
function getConfirm(location)
{
	if(arguments[0] != null)
	{
		if(window.confirm('<?php echo $Lang['W2']['deleteConfirmed']; ?>'))
		{
			location.href = location;
		}
		else
		{
			event.cancelBubble = true;
			event.returnValue = false;
			return false;
		}
	}
	
	else
	{
		return false;
	}
	return;
}
function reloadPage() {
	$('form#form1').submit();
}
</script>
<br/>

<form id="form1" name="form1" method="get">
	<div class="new_resource">
		<?=$h_newBtn?>
		<span>&nbsp;</span>
		<?=$h_newTemplateBtn?>
	</div>
	
	<?=$h_writingAccessStatusSelection?>
	<?=$h_tableResult?>
	<?=$h_floatingLayer?>
	<br/>
	
	<input type="hidden" id="cid" name="cid" value="" />
	<input type="hidden" id="task" name="task" value="templateManagement" />
	<input type="hidden" id="mod" name="mod" value="admin" />
	<input type="hidden" id="r_contentCode" name="r_contentCode" value="<?=$r_contentCode?>" />
</form>