<script language="javascript">
$(document).ready( function() {
		$('.selectAllContent:checkbox, .selectAllSch:checkbox').change(function () {
		var checkClass = $(this).attr( "data-value" );
		if($(this).attr("checked")) 
		{
			$('.'+checkClass+':checkbox').attr('checked','checked');
		}
		else
		{
			$('.'+checkClass+':checkbox').removeAttr('checked');
		}
	});

	$('#publishAll').click(function(){
		var hasChecked;
		$('input:checkbox').map(function () {
  			if ($(this).attr('checked') )
			{
				hasChecked=true;
			}
		}).get();
		
		if(hasChecked)
		{
			$('input#mod').val('admin');
			$('input#task').val(this.id);
			$('form#form1').submit();
		}
		else
		{
			alert('Please check the required item(s)');
		}
	});
	
	$('#cancel').click(function () {
		$('input#mod').val('admin');
		$('input#task').val('publish');
		$('form#form1').submit();
	});

});


function reloadPage() {
	$('form#form1').submit();
}
</script>
<br/>

<form id="form1" name="form1" method="post">

<?=$h_writingAccessStatusSelection?>


<div class="usermgmt_table_190">
	<div class="table_board">
	
		<div class="table_top_left">
			<div style="height:10px" class="table_top_right">
			</div>
		</div>
	
		<div class="table_left">
			<div class="table_right">
				<table class="common_table_list">
					<colgroup>
						<?=$h_publishTableWidthSetter?>
					</colgroup>
					<thead>
						<tr class="">
							<?=$h_publishTableHeader?>
						</tr>
					</thead>
					<tbody>
						<?=$h_publishTableContent?>
					</tbody>
				</table>
			</div>
		</div>
		
		<div class="table_bottom_left">
			<div class="table_bottom_right">
				<div class="common_table_bottom"></div>
			</div>
		</div>
		
	</div>	
</div>
	<div id="btnPanel" style="auto 0">
		<input type="button" id="publishAll" value="<?=$Lang['W2']['publishPart']?>" />
		<input type="button" id="cancel" value="<?=$Lang['Btn']['Cancel']?>" />
	</div>

	<?=$h_floatingLayer?>
	
	<br/>
	<!-- publish part -->
	<input type="hidden" id="task" name="task" value="publishSelectContent" />
	<input type="hidden" id="mod" name="mod" value="admin" />
	<input type="hidden" id="r_contentCode" name="r_contentCode" value="<?=$r_contentCode?>" />
</form>