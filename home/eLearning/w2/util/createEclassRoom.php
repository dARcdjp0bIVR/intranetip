<?
//CREATE CLASS ROOM FOR W2
// RUN THIS by [school domain]/home/eLearning/w2/util/createEclassRoom.php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE);
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/w2_lang.$intranet_session_language.php");

//intranet_auth();
intranet_opendb();


		###################################################
		### IES: Create classroom for rubric [Start]
		###################################################
//		if (isset($plugin['W2']) && $plugin['W2'] && !$GSary['W2_CreateClassroom'])
//		{
			$li = new libdb(); //please delete this before copy to /home/web/eclass40/intranetIP25/addon/ip25/index.php
			$x .= "===================================================================<br>\r\n";
			$x .= "W2: Create classroom [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
			
			define("CLASS_ROOM_NOT_EXIST", 0);
			
			include_once($PATH_WRT_ROOT."includes/w2/w2Config.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $w2_cfg["w2ClassRoomType"];
			
			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) == CLASS_ROOM_NOT_EXIST))
			{
				$max_user = "NULL";
				$max_storage = "NULL";
				$course_code = $w2_cfg['moduleCode'];
				$course_name = $w2_cfg['moduleCode'];
				$course_desc = "eClass classroom for Module {$course_code}";
			
				$lo = new libeclass();
			
				$lo->setRoomType($roomType);
			
				$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
				
				$sql = "UPDATE {$eclass_db}.course SET is_guest = 'yes' WHERE course_id = {$course_id}";
				$li->db_db_query($sql);
			
				$subj_id = -9; // $subj_id may not usefull
				$lo->eClassSubjectUpdate($subj_id, $course_id);
			}
			
			# update General Settings - markd the script is executed
//			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'W2_CreateClassroom', 1, now())";
//			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
					
			$x .= "===================================================================<br>\r\n";
			$x .= "W2: Create classroom [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
//		}
		###################################################
		### IES: Create classroom for rubric [End]
		###################################################
echo $x;
?>