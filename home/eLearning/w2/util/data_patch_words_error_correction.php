<?php
/*
 * 	patch data for error correction in writing 2.0 
 */
$PATH_WRT_ROOT = "../../../../";
//include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/global.php");
intranet_opendb();
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/w2/w2Config.inc.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

global $intranet_db, $intranet_db_user, $intranet_db_pass,
		$eclass_db, $eclass_db_user, $eclass_db_pass;
	


$Flag = $_REQUEST['Flag'];

$roomType = $w2_cfg["w2ClassRoomType"];
$w2_classRoomID= getEClassRoomID($roomType);
$w2_classRoomDB = $eclass_prefix.'c'.$w2_classRoomID;
$objDb = new libdb();


###############################################################################
## W2_CONTENT_DATA
$script = array();
$script[] = "update_A_day_spent_volunteering_S3_Advanced.sql";
$script[] = "update_A_day_spent_volunteering_S3_Basic.sql";
$script[] = "update_A_letter_of_complaint_S3_Advanced.sql";
$script[] = "update_A_letter_of_complaint_S3_Basic.sql";
$script[] = "update_a_trip_in_xxx_Basic.sql";
$script[] = "update_Charity_fundraiser_at_school_S3_Advanced.sql";
$script[] = "update_Charity_fundraiser_at_school_S3_Basic.sql";
$script[] = "update_Great_designers_S3_Advanced.sql";
$script[] = "update_Great_designers_S3_Basic.sql";
$script[] = "update_How_to_choose_a_suitable_job_S3_Advanced.sql";
$script[] = "update_How_to_choose_a_suitable_job_S3_Basic.sql";
$script[] = "update_How_to_prevent_obesity_S3_Advanced.sql";
$script[] = "update_How_to_prevent_obesity_S3_Basic.sql";
$script[] = "update_Informational_report_on_healthy_food_S3_Advanced.sql";
$script[] = "update_Informational_report_on_healthy_food_S3_Basic.sql";
$script[] = "update_Is_new_technology_a_help_S3_Advanced.sql";
$script[] = "update_Is_new_technology_a_help_S3_Basic.sql";
$script[] = "update_Job_Application_S3_Advanced.sql";
$script[] = "update_Job_Application_S3_Basic.sql";
$script[] = "update_Mid_Autumn_Festival_Basic.sql";
$script[] = "update_My_dream_job_S3_Advanced.sql";
$script[] = "update_My_dream_job_S3_Basic.sql";
$script[] = "update_My_favourite_film_S3_Advanced.sql";
$script[] = "update_My_favourite_film_S3_Basic.sql";
$script[] = "update_Problems_at_school_S3_Advanced.sql";
$script[] = "update_Problems_at_school_S3_Basic.sql";
$script[] = "update_Teen_Problems_S3_Advanced.sql";
$script[] = "update_Teen_Problems_S3_Basic.sql";

###############################################################################

	echo "###########################################################<br>";
//	echo "<br>ClassRoomID --> ".$w2_classRoomID . "<br>";
//	echo "ClassRoomDB --> ".$w2_classRoomDB . "<br>";
	echo "Flag=" . $Flag . "<br>";
	
	$error = array();
	## 1. update W2_CONTENT_DATA in IP
	foreach ($script as $s) {
		$fileName = "{$PATH_WRT_ROOT}home/eLearning/w2/util/script/error_correction/$s";
		$command = "mysql -hlocalhost -u".OsCommandSafe($intranet_db_user)." -p".OsCommandSafe($intranet_db_pass)." ".OsCommandSafe($intranet_db)." < ".OsCommandSafe($fileName)."";
//		debug_r($command);
		debug_r($fileName);
		if ($Flag==1) {
			exec($command,$output=array(),$res);
			if ($res) {
				$error[] = $s;	
			}
		}
	}

	## 2. create temp table in eClass
	$fileName = "{$PATH_WRT_ROOT}home/eLearning/w2/util/script/error_correction/data_patch_20150305_W2_DEFAULT_CONTENT.sql";
	$command = "mysql -hlocalhost -u".OsCommandSafe($eclass_db_user)." -p".OsCommandSafe($eclass_db_pass)." ".OsCommandSafe($eclass_db)." < ".OsCommandSafe($fileName);
//	debug_r($command);
	debug_r($fileName);
	if ($Flag==1) {
		exec($command,$output=array(),$res);
		if ($res) {
			$error[] = $fileName;	
		}
	}

	## 3. create temp table in class room
	$fileName = "{$PATH_WRT_ROOT}home/eLearning/w2/util/script/error_correction/data_patch_20150305_powerconcept.sql";
	$command = "mysql -hlocalhost -u".OsCommandSafe($eclass_db_user)." -p".OsCommandSafe($eclass_db_pass)." ".OsCommandSafe($w2_classRoomDB)." < ".OsCommandSafe($fileName);
//	debug_r($command);
	debug_r($fileName);
	if ($Flag==1) {
		exec($command,$output=array(),$res);
		if ($res) {
			$error[] = $fileName;	
		}
	}

	## 4. update W2_DEFAULT_CONTENT
$sql=<<<SQL
update {$eclass_db}.W2_DEFAULT_CONTENT d inner join {$eclass_db}.data_patch_20150305_W2_DEFAULT_CONTENT p on 
    p.SCHEME_CODE=d.SCHEME_CODE and p.CONTENT_CODE=d.CONTENT_CODE and p.SETTING_NAME=d.SETTING_NAME
  set d.SETTING_VALUE=p.SETTING_VALUE
  where d.SETTING_NAME='conceptMapContent' and d.CONTENT_CODE='eng'
  and d.SCHEME_CODE in ('scheme6','scheme7','scheme18','scheme19',
  'scheme24','scheme25','scheme28','scheme29','scheme43',
  'scheme44','scheme47','scheme48','scheme45','scheme46',
  'scheme51','scheme52','scheme61','scheme62','scheme69',
  'scheme70','scheme71','scheme72','scheme75','scheme76',
  'scheme77','scheme78','scheme81','scheme82')
SQL;
debug_r($sql);
	if ($Flag==1) {
		$res = $objDb->db_db_query($sql);
		if (!$res) {
			$error[] = "update W2_DEFAULT_CONTENT error: " . $sql;
		}
	}
	
	# 5. update powerconcept
	$sql =<<<SQL
update {$w2_classRoomDB}.powerconcept w inner join {$w2_classRoomDB}.data_patch_20150305_powerconcept p on 
    p.Name=w.Name  
  set w.DataContent=p.DataContent;
SQL;
debug_r($sql);
	if ($Flag==1) {
		$res = $objDb->db_db_query($sql);
		if (!$res) {
			$error[] = "update powerconcept error: " . $sql;
		}
	}
	
	# 6.1 drop temp table:
	$sql =<<<SQL
drop table {$eclass_db}.data_patch_20150305_W2_DEFAULT_CONTENT;
SQL;
debug_r($sql);
	if ($Flag==1) {
		$res = $objDb->db_db_query($sql);
		if (!$res) {
			$error[] = "drop temp table data_patch_20150305_W2_DEFAULT_CONTENT error: " . $sql;
		}
	}
	
	# 6.2 drop temp table:
	$sql =<<<SQL
drop table {$w2_classRoomDB}.data_patch_20150305_powerconcept;
SQL;
debug_r($sql);
	if ($Flag==1) {
		$res = $objDb->db_db_query($sql);
		if (!$res) {
			$error[] = "drop temp table data_patch_20150305_powerconcept error: " . $sql;
		}
	}
	  		
	if ($Flag==1) {	  		
	  	if ($error) {
	  		foreach($error as $e) {
	  			echo "<div><font color=red>". $e . "</font></div><br>";
	  		}
	  	}
	  	else {
	  		echo "<div><font color=blue>successfully update data in wrinting 2.0</font></div><div>";
	  	}
	}
	else {
		print "<form name=f1 method=post action=\"".$_SERVER['PHP_SELF']."\">";
		print "<div><input name='run_script' type=submit value='run script'><input type=hidden name=Flag value=1>";
		print "</form>";		
	}

intranet_closedb();

?>