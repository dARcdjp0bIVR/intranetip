<?
// using:
//include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/w2ShareContentConfig.inc.php");
$r_contentCode = $_GET['r_contentCode'];
$r_returnMsgKey = $_GET['r_returnMsgKey'];
GLOBAL $w2_cfg;

$objDb = new libdb();
$objW2PublishMenuGenerator =new W2PublishMenuGenerator();
$publishList = $objW2PublishMenuGenerator->getPublishMenu($r_contentCode,false,$cidList);

### Toolbar
if ($w2_libW2->isAdminUser()) {
}

### Table
## Table Width
$h_publishTableWidthSetter='';
$h_publishTableWidthSetter.='<col width="1%" />'."\n\r";
$h_publishTableWidthSetter.='<col nowrap="nowrap" width="38%" />'."\n\r";
$h_publishTableWidthSetter.='<col width="1%" />'."\n\r";
for ($i=0; $i<count($w2_cfg["schoolCode"]);$i++)
{
	$h_publishTableWidthSetter.='<col nowrap="nowrap" width="15%" />'."\n\r";
}

foreach( (array)$publishList as $publishItem)
{
	$schemeNumList[]=$publishItem;
}
## Table Header
$h_publishTableHeader='';
$h_publishTableHeader .= "<th>#</th>"."\n\r";
$h_publishTableHeader .= "<th>".$Lang['W2']['topic']."</th>"."\n\r";
foreach( (array)$w2_cfg["schoolCode"] as $schoolDetail)
{
	if($schoolDetail["shortDesc"] != $w2_cfg["schoolCodeCurrent"])
	{
		$h_publishTableHeader .= '<th>'.$schoolDetail["shortDesc"].'<br />' .
		$h_publishTableHeader .= "<input type='checkbox' id='${schoolDetail[shortDesc]}_All' />";
		$h_publishTableHeader .= '</th>'."\n\r";
	}
}

## Table Content
$h_publishTableContent='';

$loadNum = 1;
$checkAllSch='';
foreach( (array)$publishList as $publishItem)
{
	$checkAllSch = "<input type='checkbox' id='${publishItem[cid]}_All' />";
	
	$h_publishTableContent.="<tr class='normal record_bottom'>";
	$h_publishTableContent .= "<td></td>"."\n\r";
	$h_publishTableContent .= "<td>".$publishItem["topicName"]."</td>"."\n\r";
	$h_publishTableContent .= "<td>".$checkAllSch."</td>"."\n\r";
	
	foreach( $w2_cfg["schoolCode"] as $schoolDetail)
	{
		if($w2_cfg["schoolCodeCurrent"] != $schoolDetail["shortDesc"])
		{
			$shortDesc = strtolower($schoolDetail["shortDesc"]);
			if ( $publishItem["${shortDesc}Sent"] == true)
			{
				$selectStatus= "Done";
			}
			else
			{
				$selectStatus= "<input type='checkbox'
						class='scheme_${publishItem[cid]} to_${shortDesc}'
						id='send${publishItem[cid]}To${shortDesc}'
						value='${publishItem[cid]}-to-${shortDesc}'/>";
			}
			$h_publishTableContent .= "<td>".$selectStatus."</td>"."\n\r";
		}
	}
	$h_publishTableContent .="</tr>";
	
}
###

$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/admin/publishSelectContent.tmpl.php');
$linterface->LAYOUT_STOP();
?>