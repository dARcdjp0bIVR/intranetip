<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentGenerator.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
$cid = $_GET['cid'];
$r_contentCode = $_GET['r_contentCode'];
try 
{
	$W2EngContentParser = new libW2EngContentParser();
	$parseData = $W2EngContentParser->parseEngContent($cid);
	
	if(checkEngContentIsCompleted($parseData,$stepsInvolved=5))
	{
		//special branch for questions of array 
		
		$parseData['step4Data']['step4DraftQList'] = getStep4DraftQArrayParsed($parseData['step4Data']['step4DraftQList']);
		$W2EngContentGenerator=new libW2GenerateEngContent($PATH_WRT_ROOT);
		$W2EngContentGenerator->generateEngContent($parseData);
		
		$statusUpdated = $W2EngContentGenerator->getTableUpdateToGenerated($cid);
		
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=templateManagement&r_contentCode=$r_contentCode&cid=$cid&r_returnMsgKey=AddSuccess");		
	}
	else
	{
		echo "Please check if the steps are all filled";
		exit;
	}
	
} 
catch (Exception $e) 
{
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}



###################### Logic helper function ###############################################################
function checkEngContentIsCompleted($parseData,$stepsInvolved)
{
	if($parseData['topicName']=='' || $parseData['topicIntro']==''
		|| $parseData['level']=='' || $parseData['category']=='')
	{		
			return false;
	}
	for ($i=1;$i<=$stepsInvolved; $i++)
	{
		if($parseData['step'.$i.'Data'] =='')
		{

			return false;
		}
	}
	return true;
}
function getStep4DraftQArrayParsed($parseData)
{
	$i=0;

	$returnStr ='';
	$returnStr ='<table>';	

	foreach( (array)$parseData as $draftQ)
	{
	$returnStr.='<tr>';
	$returnStr.='<td>';
	$returnStr.=$draftQ;
	$returnStr.='<textarea rows="5" class="textbox" id="textfield" name="r_question[textarea][<?=$w2_m_ansCode['.$i.']?>]" <?=$w2_h_answerDefaultDisabledAttr?>><?=$w2_libW2->getAnsValue($w2_s_thisStepAns[$w2_m_ansCode['.$i.']])?></textarea>
';
	$returnStr.='</td>';
	$returnStr.='</tr>';
	
	$i++;
	}
	$returnStr .='</table>';

	return $returnStr;
}
?>