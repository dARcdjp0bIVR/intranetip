<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$cid = trim($_POST['cid']);
$r_contentCode = trim($_POST['r_contentCode']);
$saveTab = isset($_POST['saveTab'])?trim($_POST['saveTab']):exit;

### DB Control Info
$schemeNum= $objEngContentBuilder->getNextSchemeNum($r_contentCode);

//isInjectionCodePresent($_POST)?'OK':exit();

try 
{
	
	switch($saveTab)
	{
		case 0:
			### General column
			$topicIntro = isset($_POST['topicIntro'])?trim($_POST['topicIntro']):exit;
			$topicName = isset($_POST['topic'])?trim($_POST['topic']):exit;
			$level =  isset($_POST['level'])?trim($_POST['level']):exit;
			$category =  isset($_POST['category'])?trim($_POST['category']):exit;
			if( empty($cid) )
			{
				$buildData[]= $objEngContentBuilder->buildGeneralInfoInsert($topicIntro,$topicName,$level,$category);
			}
			else
			{
				$buildData[]= $objEngContentBuilder->buildGeneralInfoUpdate($topicIntro,$topicName,$level,$category);
			}
			
			break;
		case 1:
			### Step1 column
			
			$step1Int =  isset($_POST['step1Int'])?trim($_POST['step1Int']):exit;
			$step1TextType = isset($_POST['step1TextType'])?trim($_POST['step1TextType']):exit;
			$step1Purpose = isset($_POST['step1Purpose'])?trim($_POST['step1Purpose']):exit;
			$step1Content = isset($_POST['step1Content'])?trim($_POST['step1Content']):exit;
			$buildData[]= $objEngContentBuilder->buildStep1($step1Int,$step1TextType,$step1Purpose,$step1Content);
			break;
		case 2:
			### Step2 column
			$step2Int = isset($_POST['step2Int'])?trim($_POST['step2Int']):exit;
			$buildData[]= $objEngContentBuilder->buildStep2($step2Int);
			break;
		case 3:
			### Step3 column

			$step3Int = isset($_POST['step3Int'])?trim($_POST['step3Int']):exit;
			$step3Sample = isset($_POST['step3Sample'])?trim($_POST['step3Sample']):exit;
			$step3Vocab = isset($_POST['step3Vocab'])?trim($_POST['step3Vocab']):exit;
			$step3Grammar = isset($_POST['step3Grammar'])?trim($_POST['step3Grammar']):exit;
			$buildData[]= $objEngContentBuilder->buildStep3($step3Int,$step3Sample,$step3Vocab, $step3Grammar);
			break;
		case 4:
			### Step4 column
			$step4Int = isset($_POST['step4Int'])?trim($_POST['step4Int']):exit;
			$step4DraftQ = isset($_POST['step4DraftQ'])?$_POST['step4DraftQ']:exit;
			$buildData[]= $objEngContentBuilder->buildStep4($step4Int,$step4DraftQ);
			
			break;
		case 5:
			### Step5 column
			$step5Int = isset($_POST['step5Int'])?trim($_POST['step5Int']):exit;
			$step5DefaultWriting = isset($_POST['step5DefaultWriting'])?trim($_POST['step5DefaultWriting']):exit;
			$buildData[]= $objEngContentBuilder->buildStep5($step5Int,$step5DefaultWriting);
			break;
		default:
			break;
	}
	
	
	if( empty($cid) )
	{
			$buildData[]= $objEngContentBuilder->buildDBInfoInsert($r_contentCode,$schemeNum);	
			$sql = 'INSERT INTO W2_CONTENT_DATA
			('
			.$buildData[0]['field'].','
			.$buildData[1]['field'].
			') VALUES
			('
			.$buildData[0]['content'].','
			.$buildData[1]['content'].
			');';
	}
	else
	{
			if($saveTab==0)
			{
				$data = $buildData[0];
			}
			else
			{
				$data = $buildData[0]['field'].'='.$buildData[0]['content'];
			}
			$buildData[]= $objEngContentBuilder->buildDBInfoUpdate($r_contentCode,$schemeNum);
			$sql = 'UPDATE W2_CONTENT_DATA SET '.			
			$data.','.
			implode(",", (array)$buildData[1]).
			' WHERE cid="'.$cid.'";';
			
	}
	$rs = $objDB->db_db_query($sql);
	
	if($rs)
	{
		$cid = empty($cid)?$objDB->db_insert_id():$cid;
		
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: /home/eLearning/w2/index.php?mod=admin&task=newTemplateWriting&r_contentCode=$r_contentCode&cid=$cid&r_returnMsgKey=AddSuccess&tabNum=$saveTab");
	}
} 
catch (Exception $e) 
{
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}


#####################Helper function #####################################################
function isInjectionCodePresent($stringList)
{
	foreach($stringList as $string)
	{
		if (strpos($string, '<?php') !== false)
	    {
	    	return true;
	    }
	}
}

?>