<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
if(!$w2_thisIsAdmin) die();
$contentCode = $_REQUEST['r_contentCode'];
$r_returnMsgKey = $_REQUEST['r_returnMsgKey'];
$r_writingId = (isset($_REQUEST['r_writingId']))? $_REQUEST['r_writingId'] : "";


$objDb = new libdb();
$lexport = new libexporttext();
$objWritingTeacher = new libWritingTeacher($w2_thisUserID);
### Get Writing Info
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$writingTitle = $objWriting->getTitle();
$r_schemeCode = $objWriting->getSchemeCode();
$contentAttributes = $w2_libW2->getContentAttributes($contentCode,$r_schemeCode);
$r_contentTypeSuper = $contentAttributes["type_super"];
$r_contentType = $contentAttributes["type"];
$grade = $contentAttributes["grade"];

$filename = $Lang['W2']['subjectFileCode'][$contentCode]."_".$writingTitle."_".date("Ymd").".csv";

### Get Last Step info of each students of each writings 
$writingToBeMarkedStudentAssoAry = $w2_libW2->getStudentWritingRecordByWritingId($contentCode,$r_writingId);

### Export Header
$exportHeader = array();
$exportHeader[] = $Lang['W2']['className'];
$exportHeader[] = $Lang['W2']['ClassNo'];
$exportHeader[] = $Lang['W2']['name'];
for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
	$exportHeader[] = $w2_libW2->getDraftName($i);
}
$exportHeader[] = $Lang['W2']['highestMark'];
$exportHeader[] = $Lang['W2']['totalAttempt'];
$exportData = array();
$r=0;
foreach((array)$writingToBeMarkedStudentAssoAry as $_studentId => $_studentRecordAry){
	$exportData[$r][] = $_studentRecordAry['CLASS']; 
	$exportData[$r][] = $_studentRecordAry['CLASS_NO']; 
	$exportData[$r][] = $_studentRecordAry['STUDENT_NAME'];
	for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
		$exportData[$r][] = $_studentRecordAry['DRAFT'.$i]; 
	} 
	$exportData[$r][] = $_studentRecordAry['HIGHEST_MARK']; 
	$exportData[$r][] = $_studentRecordAry['TOTAL_ATTEMPT']; 
	$r++;
}
//generate csv content
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportData, $exportHeader, "\t", "\r\n", "\t", 0, "9");

//Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content);
?>