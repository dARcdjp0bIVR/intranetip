<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");


$contentCode = $_REQUEST['r_contentCode'];
$r_returnMsgKey = $_REQUEST['r_returnMsgKey'];
$r_writingId = (isset($_REQUEST['r_writingId']))? $_REQUEST['r_writingId'] : "";

### Get / Initialize the db table and pagination related variables 
$curPage = (isset($_REQUEST['curPage']))? $_REQUEST['curPage'] : 1;
$numPerPage = (isset($_REQUEST['numPerPage']))? $_REQUEST['numPerPage'] : $w2_cfg["dbTableDefault"]["NumPerPage"];
$sortColumnIndex = (isset($_REQUEST['sortColumnIndex']))? $_REQUEST['sortColumnIndex'] : $w2_cfg["dbTableDefault"]["sortColumnIndex"];
$sortOrder = (isset($_REQUEST['sortOrder']))? $_REQUEST['sortOrder'] : $w2_cfg["dbTableDefault"]["sortOrder"];

$objDb = new libdb();
$objWritingTeacher = new libWritingTeacher($w2_thisUserID);
### Get Writing Info
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$writingTitle = $objWriting->getTitle();
$r_schemeCode = $objWriting->getSchemeCode();
$contentAttributes = $w2_libW2->getContentAttributes($contentCode,$r_schemeCode);
$r_contentTypeSuper = $contentAttributes["type_super"];
$r_contentType = $contentAttributes["type"];
$grade = $contentAttributes["grade"];

## Get Filter Selection
$contentAryGroupByType  = $w2_libW2->getContentGroupByType($contentCode);
$contentAryGroupBySuperType  = $w2_libW2->getContentGroupByTypeSuper($contentCode);
$h_gradeSelection = $w2_libW2->getGradeSelection($grade,'onchange="resetThemeSelection();"',$showAll=true);

if(strtolower($r_contentCode) == strtolower($w2_cfg["contentArr"]["eng"]["contentCode"])){
	$h_select1 = '<select name="contentSuperTypeSlt" id="id_contentSuperTypeSlt" onchange="updateContentTypeSlt(this.options[this.options.selectedIndex].value,\''.$r_contentCode.'\'); $(\'#id_contentTypeSlt\').val(\'\'); updateSchemeCodeSlt($(\'#id_contentTypeSlt\').val(\'\'),\''.$r_contentCode.'\')">';
	$h_select1 .= '<option value= "">-- '.$Lang['General']['PleaseSelect'].' --</option>';
	foreach($contentAryGroupBySuperType as $_typeSuper => $_Details){
		$_selected = '';
		if($r_contentTypeSuper != '' && $r_contentTypeSuper == $_typeSuper){
			$_selected = ' SELECTED ';
		}
		$h_select1 .= '<option value="'.$_typeSuper.'" '.$_selected.'>'.$_typeSuper.'</option>';
	}
	$h_select1 .= '</select>';

}
$h_select2 = $w2_libW2->getTextTypeSelection($r_contentCode,$r_contentTypeSuper,$r_contentType,$grade);
$searchValueAry['schemeCodeAry'] = array($r_schemeCode);
$h_selectSchemeCode = $w2_libW2->getSchemeCodeSelection($r_contentCode,$r_contentTypeSuper,$r_contentType,$r_schemeCode,$grade);
$h_selectExercise = $w2_libW2->getWritingSelection($r_contentCode,$searchValueAry,$r_writingId);

### Get Last Step info of each students of each writings 
$writingToBeMarkedStudentAssoAry = $w2_libW2->getStudentWritingRecordByWritingId($contentCode,$r_writingId);

### Prepare temp table for display
$SuccessArr = array();
$sql = '';
$sql .= "Create Temporary Table If Not Exists TMP_W2_PAGE (
			CLASS varchar(255) default NULL,
			CLASS_NO int(8) default NULL,
			STUDENT_NAME varchar(255) default null,";
for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
	$sql .= "DRAFT".$i." varchar(16) default NULL,";
	$sql .= "DRAFT".$i."LINK text default NULL,";
}
$sql .= "
			HIGHEST_MARK varchar(16) default NULL,
     		TOTAL_ATTEMPT int(8) default NULL,
			EXPORT_BTN text default null
		) ENGINE=InnoDB Charset=utf8
";
//debug_r($sql);
$SuccessArr['CreateTempTable'] = $objDb->db_db_query($sql);
### Prepare Temp Table Data
$insertAry = array();
foreach((array)$writingToBeMarkedStudentAssoAry as $_studentId => $_studentRecordAry){
	$_sql = " (
					'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['CLASS'])."', 
					'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['CLASS_NO'])."', 
					'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['STUDENT_NAME'])."', ";
	for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
			$_sql .= "'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['DRAFT'.$i])."',";
			$_sql .= "'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['DRAFT'.$i.'LINK'])."',";
	}
 
	$_sql .= "		'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['HIGHEST_MARK'])."', 
					'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['TOTAL_ATTEMPT'])."',  
					'".$objDb->Get_Safe_Sql_Query($_studentRecordAry['EXPORT_BTN'])."'
				) ";
	$insertAry[] = $_sql;
}

### Insert data into Temp Table
$numOfInsert = count($insertAry);
if ($numOfInsert > 0) {
	$sql = "Insert Into TMP_W2_PAGE Values ".implode(",", (array)$insertAry);
	//debug_r($sql);
	$SuccessArr['InsertTempTable'] = $objDb->db_db_query($sql);
}

### Set Display Table Object's Attributes
$objTableMgr = new libtable_mgr();
$objTableMgr->setSql('Select * From TMP_W2_PAGE');
$objTableMgr->setCurPage($curPage);
$recordCnt = count($objDb->returnArray('Select * From TMP_W2_PAGE'));
$objTableMgr->setNumPerPage($recordCnt);
$objTableMgr->setSortColumnIndex($sortColumnIndex);
$objTableMgr->setSortOrder($sortOrder);
$objTableMgr->addExtraSort('CLASS_NO', 0);

$objTableMgr->addColumn('CLASS', 1, $Lang['W2']['className'], '8%');
$objTableMgr->addColumn('CLASS_NO', 1, $Lang['W2']['ClassNo'], '8%');
$objTableMgr->addColumn('STUDENT_NAME', 1, $Lang['W2']['name'], '10%');
for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
	$objTableMgr->addColumn('DRAFT'.$i.'LINK', 1, $w2_libW2->getDraftName($i), '5%', $headerClass='', $dataClass='', $extraAttribute='', $sortDbField='DRAFT'.$i);
}
$objTableMgr->addColumn('HIGHEST_MARK', 1, $Lang['W2']['highestMark'], '8%');
$objTableMgr->addColumn('TOTAL_ATTEMPT', 1, $Lang['W2']['totalSubmit'], '9%');
//$objTableMgr->addColumn('TOTAL_ATTEMPT', 1, $Lang['W2']['totalAttempt'], '9%');
$objTableMgr->addColumn('EXPORT_BTN',0, '', '7%');

$studentTotalHtml = "<div class=\"record_no\">".$Lang['W2']['NumberOfStudents']." : ".$recordCnt."</div>";
$objTableMgr->setDataTrClassAry(array('normal record_bottom'));
$objTableMgr->setDivTemplate($w2_libW2->getDbTableDivTemplate($objTableMgr->getDbTableReplaceCode(), $studentTotalHtml, $writingTitle));

$objTableMgr->setSubmitMode(2);
$objTableMgr->setSubmitExtraPara('?task='.$task.'&mod='.$mod.'&r_contentCode='.$contentCode.'&r_writingId='.$r_writingId);

$objTableMgr->setObjLayout(new libtable_layout());
$objTableMgr->setObjPagination(new libpagination());
$h_tableResult = $objTableMgr->display();
$h_floatingLayer = $w2_libW2->getFloatLayerHtml('');

$exportCSVLink = "?mod=report&task=exportWritingClassReport&r_contentCode=".$r_contentCode."&r_writingId=".$r_writingId;
$exportAllLink = "?mod=report&task=exportWriting&r_contentCode=".$r_contentCode."&r_writingId=".$r_writingId;
$returnMsg = $Lang['General']['ReturnMessage'][$r_returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
include_once('templates/report/listWritingClassReport.tmpl.php');
$linterface->LAYOUT_STOP();
?>