<?
include_once($PATH_WRT_ROOT."includes/w2/libWritingTeacher.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2ContentFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingFactory.php");
include_once($PATH_WRT_ROOT."includes/w2/libWriting.php");
include_once($PATH_WRT_ROOT."includes/w2/libWritingDb.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2Export.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
if(!$w2_thisIsAdmin) die();
$contentCode = $_REQUEST['r_contentCode'];
$r_returnMsgKey = $_REQUEST['r_returnMsgKey'];
$r_writingId = (isset($_REQUEST['r_writingId']))? $_REQUEST['r_writingId'] : "";
$stepId = $w2_libW2->getWritingLastStepId($r_writingId);

$objDb = new libdb();
$lexport = new libw2export();
$fs = new libfilesystem();
$studentWritingAttachmentAry = $lexport->getStudentWritingFile($stepId,$student_id);

### Get Writing Info
$objWriting = libWritingFactory::createWriting($w2_cfg["writingObjectSource"]["db"], $r_writingId);
$writingTitle = $objWriting->getTitle();
$writingToBeMarkedStudentAssoAry = $w2_libW2->getStudentWritingRecordByWritingId($contentCode,$r_writingId,array($student_id));
$studentWritingAry = $writingToBeMarkedStudentAssoAry[$student_id];
$writing_name = $Lang['W2']['subjectFileCode'][$contentCode]."_".$writingTitle."_".$studentWritingAry['CLASS']."_".$studentWritingAry['CLASS_NO']."_".date("Ymd");
$writing_name = $fs->SafeGuardFileName($writing_name,true);
$writing_name_encoded = mb_convert_encoding($writing_name ,"big5","utf8");
$writing_folder = $intranet_root."/file/w2/writing_report/".$r_writingId;
$folder_target = $writing_folder."/".$writing_name_encoded;
createOutputFolder($folder_target);

### Prepare folder and student attachment
$studentAttachmentCnt = count($studentWritingAttachmentAry);
if($studentAttachmentCnt>0){
	$studentAttachmentFolder = $folder_target."/attachment";
	createOutputFolder($studentAttachmentFolder);
	for($i=0;$i<$studentAttachmentCnt;$i++){
		$_from = $studentWritingAttachmentAry[$i]["DOWNLOAD_PATH"];
		$_filename = $studentWritingAttachmentAry[$i]["FILE_ORI_NAME"];
		$_filename = $fs->SafeGuardFileName($_filename,true);
		$_filename = mb_convert_encoding($_filename ,"big5","utf8");
		if(file_exists($_from)){
			$fs->item_copy($_from, $studentAttachmentFolder."/".$_filename);
		}
	}
}


### Get Last Step info of each students of each writings 
$content = "";

$content .= $Lang['W2']['className'].": ".$studentWritingAry['CLASS']." ".$Lang['W2']['ClassNo'].": ".$studentWritingAry['CLASS_NO']."<br/>";"<br/>";
$content .= $Lang['W2']['name'].": ".$studentWritingAry['STUDENT_NAME']."<br/><br/>";


for($i=1;$i<=$w2_cfg['HandinArr']['maxRedoAttemptCount'];$i++){
	if($studentWritingAry['DRAFT'.$i]!='-'&&$studentWritingAry['DRAFT'.$i]>=0){
		$content .= $w2_libW2->getDraftName($i)." ".$Lang['W2']['mark'].": ".$studentWritingAry['DRAFT'.$i]."<br/>";
		$content .= $studentWritingAry['ANSWER'.$i]."<br/><br/>";	
	}
}
$doc_name = $writing_name_encoded.".doc";
$file_content = $lexport->createDoc($content, $doc_name, false);

$fs->file_remove("{$folder_target}/$doc_name");
$fs->file_write($file_content, "{$folder_target}/$doc_name");

$zip_name = $writing_name.".zip";
$zip_name_encode = urlencode($writing_name).".zip";
$zip_path = $writing_folder."/".$zip_name;

//rename_filename_from_utf8_to_big5($folder_target);
chdir($folder_target);
exec("zip -r ../".OsCommandSafe($zip_name_encode)." *");
$fs->file_rename($writing_folder."/".$zip_name_encode, $writing_folder."/".$zip_name);
$fs->folder_remove_recursive($folder_target);

##out the zip file to browser and let the user download it.
output2browser(get_file_content($zip_path), $zip_name);



function createOutputFolder($folder_target){
	global $fs;
	if(!file_exists($folder_target)) //create folder
	{
		$fs->folder_new($folder_target);
	}else{ //remove everything inside
		$row = $fs->return_folderlist($folder_target);
	    for($i=0; $i<sizeof($row); $i++){
	      $fs->item_remove($row[$i]);
	    }
	}
}
?>