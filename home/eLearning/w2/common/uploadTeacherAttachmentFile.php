<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($intranet_root."/includes/libfilesystem.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$objFile = new libfilesystem();
#init
$attachmentAry = array();
$cid = trim($_POST['cid']);
$step = trim($_POST['step']);
$existAttachmentAry = (array)$_POST['existAttachmentAry'];
define("UPLOAD_SUCCESS", 1);
$tarPath = '/file/w2/teacher_attachment/cid_'.$cid;
$destinationPath = $intranet_root.$tarPath;

#Existing Files
$parseData = $engContentParser->parseEngContent($cid);
$teacherAttachmentAry = $parseData['teacherAttachmentData'];
$teacherAttachmentCnt = count($teacherAttachmentAry);
if(is_array($teacherAttachmentAry) && $teacherAttachmentCnt > 0){
	foreach($teacherAttachmentAry as $_step => $_attachmentItem){
		$_stepNum = str_replace('teacherAttachment_','',$_step);
		if($_stepNum>0){
			foreach($_attachmentItem as  $__attachmentAry){
				$__attachmentName = $__attachmentAry['teacherAttachmentItemName'];
				$__attachmentFilePath = $__attachmentAry['teacherAttachmentItemFile'];
				$attachmentAry[$_stepNum][] = array('filename'=>$__attachmentName,'filepath'=>$__attachmentFilePath);
			}
		}
	}
}

#New Files
$uploadFileListAry = array();
for($i=0;$i<$attachmentBox_counter;$i++){
	$_file = $_FILES['r_tatt_'.$i];
	$_name = $_file['name'];
	$_type = $_file['type'];
	$_size = $_file['size'];
	$_tmp_name = $_file['tmp_name'];
	$_error = $_file['error'];
	$_supplementName = ${'r_tattname_'.$i};
	if(trim($_name) != ''){
		$uploadFileListAry[$_name] = array('name'=>$_name,'type'=> $_type,'size'=>$_size,'tmp_name'=>$_tmp_name,'error'=>$_error,'supplementName'=>$_supplementName);
	}
}




foreach ($uploadFileListAry as $_fileName => $_fileInfo){
	//check has any upload error
	if ($_fileInfo["error"] > 0){
		exit();
	}
	$timeStamp = date('Y-m-d_H-i-s');
	$r_filepath = $timeStamp.'/'.$_fileName;
	$_descPath = $destinationPath.'/'.$timeStamp;
	$destinationFile = $destinationPath.'/'.$r_filepath;
	$folderIsReadyForCopy = false;

	//check related folder if not exist
	if(file_exists($_descPath)){
		//do nothing
		$folderIsReadyForCopy  = true;
	}else{
		$objFile->folder_new($_descPath);

		//make sure the folder exist
		if(file_exists($_descPath)){
			$folderIsReadyForCopy  = true;
		}else{
			//cannot create the folder stop the upload
			exit();
		}
	}

	if($folderIsReadyForCopy){
		$result = move_uploaded_file($_fileInfo["tmp_name"],$destinationFile);
		$supplementName = $_fileInfo['supplementName'];
		if($result == UPLOAD_SUCCESS && file_exists($destinationFile)){
			$attachmentAry[$step][] = array('filename'=>$supplementName,'filepath'=>$r_filepath);
		}
	}
	
}

$buildData = $objEngContentBuilder->buildTeacherAttachment($attachmentAry);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
$rs = $objDB->db_db_query($sql);
?>
<script>
//refresh the file upload panel to display the new update file
parent.refreshTeacherAttachmentFilePanel();
</script>
