<?php
// using: Adam
include_once($PATH_WRT_ROOT."includes/w2/libContent.php");

$isFirstLoginToday = $w2_libW2->isFirstVisitW2Today($_SESSION['UserID']);
//$isFirstLoginToday = true;//
$isMain = true;
// Get Appicable Content
$applicableContentArr = $w2_libW2->getApplicableContentAry();

// Build Content Icons HTML
$mod = 'writing';
if($w2_thisUserType == USERTYPE_STAFF){
	$task = 'writingMgmt';
}else{
	$task = 'listWriting';
}


// 20140527
// For Acknowledgement
switch($_SESSION["intranet_session_language"]){
	case "en":
		$ack_class = "acknowledge_eng";
	break;
	case "b5":
		$ack_class = "acknowledge";
	break;
	default:
	break;
}

### 20140917 New Logo Start
//$plugin['W2_Old_Name'] = false;
if($plugin['W2_Old_Name']){
	$frontPagelogoClass = 'logo_big_new';
}
else{
	$frontPagelogoClass = 'logo_big_easyWriting';
}

### 20140917 New Logo End

$h_icon = '';
$subjArray = array();
foreach((array)$w2_cfg['contentArr'] as $_content => $_contentInfoArr) {
	$url = '';
	$event= '';
	$_contentCode = $_contentInfoArr['contentCode'];
	
	if (in_array($_contentCode, (array)$applicableContentArr)) {
		$_objContent = new libW2Content($_contentCode);
		$_css = $_objContent->getCss();
		
		if($isFirstLoginToday){
			$event = 'BlockEntry(\''.$_contentCode.'\', \'?task='.$task.'&mod='.$mod.'&r_contentCode='.$_contentCode.'&clearCoo=1\')';
		}else{
			$url = 'href="?task='.$task.'&mod='.$mod.'&r_contentCode='.$_contentCode.'&clearCoo=1"';
		}
		
		$h_icon .= '<a id="btn_entry_'.$_contentCode.'" '.$url.' class="'.$_css.' '.$_contentCode.'" onclick="'.$event.'"><span></span></a>'."\r\n";
	}
}

$recommandBrowser = "<span><strong>IE9</strong>, <strong>Firefox 3.x</strong> and 1024x768 or above resolution is recommended.</span>";

$linterface = new interface_html("w2_main.html");	// initialize $linterface again here due to special layout
$linterface->LAYOUT_START();
include_once('templates/mainPage.php');
$linterface->LAYOUT_STOP();
?>