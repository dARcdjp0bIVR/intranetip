<?
define("UPLOAD_SUCCESS", 1);

include_once($intranet_root."/includes/libfilesystem.php");

$r_suppleFileName = trim($r_suppleFileName);
$r_attachmentBoxAry = $_FILES["r_attachmentBox"];

$uploadFileListAry = array();

for($i = 0, $i_max = count($r_attachmentBoxAry);$i < $i_max;$i++){
	$_name = $r_attachmentBoxAry['name'][$i];
	$_type = $r_attachmentBoxAry['type'][$i];
	$_size = $r_attachmentBoxAry['size'][$i];
	$_tmp_name = $r_attachmentBoxAry['tmp_name'][$i];
	$_error = $r_attachmentBoxAry['error'][$i];
	$_supplementName = $r_attachmentBox_supplementName[$i];
	if(trim($_name) == ''){
		//do nothing , file name is empty , skip insert
	}else{
		$uploadFileListAry[$_name] = array('name'=>$_name,'type'=> $_type,'size'=>$_size,'tmp_name'=>$_tmp_name,'error'=>$_error,'supplementName'=>$_supplementName);
	}
}


$objFile = new libfilesystem();

$tarPath = '/file/w2/wid_'.$r_writingId.'/uid_'.$w2_thisStudentID;
$destinationPath = $intranet_root.$tarPath;

foreach ($uploadFileListAry as $_fileName => $_fileInfo){
	$r_orgFileName = $_fileName;

	//check has any upload error
	if ($_fileInfo["error"] > 0){
		exit();
	}else{
		$hashFile = sha1($w2_thisStudentID.microtime());
	}

	
	$destinationFile = $destinationPath.'/'.$hashFile;
	$folderIsReadyForCopy = false;

	//check related folder if not exist
	if(file_exists($destinationPath)){
		//do nothing
		$folderIsReadyForCopy  = true;
	}else{
		$objFile->folder_new($destinationPath);

		//make sure the folder exist
		if(file_exists($destinationPath)){
			$folderIsReadyForCopy  = true;
		}else{
			//cannot create the folder stop the upload
			exit();
		}
	}

	if($folderIsReadyForCopy){
		$result = move_uploaded_file($_fileInfo["tmp_name"],$destinationFile);
		$supplementName = $_fileInfo['supplementName'];
		if($result == UPLOAD_SUCCESS && file_exists($destinationFile)){

			//UPLOAD SUCCESS , INSERT INTO DB
			$sql = 'insert into '.$intranet_db.'.W2_STEP_STUDENT_FILE 
			(`USER_ID`,`STEP_ID`,`FILE_ORI_NAME`,`FILE_HASH_NAME`,`FILE_SUPPLEMENTARY_NAME`,`FILE_PATH`,`DATE_INPUT`,`DATE_MODIFIED`) 
				value 
				('.$w2_thisStudentID.','.$r_stepId.',\''.$r_orgFileName.'\',\''.$hashFile.'\',\''.$supplementName.'\',\''.$tarPath.'\',now(),now())';

			$objDb = new libdb();

			$insertResult = $objDb->db_db_query($sql);
error_log($sql."    e:[".mysql_error()."]<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/aaa.txt");
		}else{
			//upload fail

		}
	}
}
?>
<script>
//refresh the file upload panel to display the new update file
parent.saveStepAns(<?=$r_goNextStep?>, <?=$r_stepStatus?>);
</script>