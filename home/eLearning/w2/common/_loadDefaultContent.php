<?
$r_srcFile = trim($r_srcFile);
$r_schemeCode = trim($r_schemeCode);
$r_contentCode = trim($r_contentCode);
$r_stepCode = trim($r_stepCode);

$contentDetailsAry = $w2_libW2->getModelFile($r_contentCode,$r_schemeCode,$r_stepCode);

$schemeModel = $contentDetailsAry['schemePath'].'/model.php';
$contentModel = $contentDetailsAry['model'];
$contentTemplate = $contentDetailsAry['view'];

include_once($schemeModel); // for the whole scheme
include_once($contentModel); // for individual content 

include($intranet_root.'/home/eLearning/w2/content/'.$r_srcFile);
?>