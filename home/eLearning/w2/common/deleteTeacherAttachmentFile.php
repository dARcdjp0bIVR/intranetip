<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($intranet_root."/includes/libfilesystem.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$objFile = new libfilesystem();
#init

$cid = trim($_POST['r_cid']);
$fid = trim($_POST['r_fid']);
$stepid = trim($_POST['r_sid']);
$r_contentCode = trim($_POST['r_contentCode']);
define("UPLOAD_SUCCESS", 1);
$tarPath = '/file/w2/teacher_attachment/cid_'.$cid;
$destinationPath = $intranet_root.$tarPath;
$attachmentAry = array();
#Existing Files
$parseData = $engContentParser->parseEngContent($cid);
$teacherAttachmentAry = $parseData['teacherAttachmentData'];
$teacherAttachmentCnt = count($teacherAttachmentAry);
if(is_array($teacherAttachmentAry) && $teacherAttachmentCnt > 0){
	foreach($teacherAttachmentAry as $_step => $_attachmentItem){
		$_stepNum = str_replace('teacherAttachment_','',$_step);
		$i=1;
		if($_stepNum>0){
			foreach($_attachmentItem as  $__attachmentAry){
				$__attachmentName = $__attachmentAry['teacherAttachmentItemName'];
				$__attachmentFilePath = $__attachmentAry['teacherAttachmentItemFile'];
				list($__timeStamp,$__fileName) = explode('/',$__attachmentFilePath); 
				if($stepid==$_stepNum&&$i==$fid){//remove file
					$objFile->file_remove($destinationPath.'/'.$__attachmentFilePath);
					$row = $objFile->return_folderlist($destinationPath.'/'.$__timeStamp);
					if(sizeof($row)==0){
						$objFile->folder_remove($destinationPath.'/'.$__timeStamp);
					}
				}else{ 
					$attachmentAry[$_stepNum][] = array('filename'=>$__attachmentName,'filepath'=>$__attachmentFilePath);	
				}
				$i++;
				
			}
			
		}
	}
}
$buildData = $objEngContentBuilder->buildTeacherAttachment($attachmentAry);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
$rs = $objDB->db_db_query($sql);

$parseData = $engContentParser->parseEngContent($cid);
$teacherAttachmentAry = $parseData['teacherAttachmentData']['teacherAttachment_'.$stepid];
echo $w2_libW2->uploadTeacherAttachmentTemplate($cid,$teacherAttachmentAry,true);


?>