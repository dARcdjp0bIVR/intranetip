<?php
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($intranet_root."/includes/libfilesystem.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$objFile = new libfilesystem();
#init
list($type,$fid) = explode('_',$r_fid);
$r_contentCode = trim($_POST['r_contentCode']);
define("UPLOAD_SUCCESS", 1);
$tarPath = '/file/w2/reference/cid_'.$cid;
$destinationPath = $intranet_root.$tarPath;

#Existing Files
$parseData = $engContentParser->parseEngContent($cid);
$step2Data = $parseData['step2Data'];
$step2Concept = $step2Data['step2ConceptAry'];
$step2Int = $step2Data['step2Int'];
$step2ReferenceAry = $step2Data['step2ReferenceAry'];

if($type=='normal'){
	$step2ReferenceAry['referenceData']['normalData1']['itemAry'] = updateExistingData($step2ReferenceAry['referenceData']['normalData1']['itemAry']);
}elseif($type=='compare1'){
	$step2ReferenceAry['referenceData']['compareData1']['itemAry'] = updateExistingData($step2ReferenceAry['referenceData']['compareData1']['itemAry']);
}elseif($type=='compare2'){
	$step2ReferenceAry['referenceData']['compareData2']['itemAry'] = updateExistingData($step2ReferenceAry['referenceData']['compareData2']['itemAry']);
}

$buildData = $objEngContentBuilder->buildChiStep2($step2Int,$step2Concept,$step2ReferenceAry);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
$rs = $objDB->db_db_query($sql);

$parseData = $engContentParser->parseEngContent($cid);
$referenceAry = $parseData['step2Data']['step2ReferenceAry'];
echo $w2_libW2->getStep2ReferenceHTML($cid,$referenceAry,true);

function updateExistingData($existingData){
	global $objFile,$destinationPath,$fid;
	if(count($existingData)>0){
		$array = array();
		$i=0;
		foreach((array)$existingData as $_value){
			$_caption = $_value['referenceCaption'];
			$_filePath = $_value['referencePath'];
			list($_timeStamp,$_fileName) = explode('/',$_filePath); 
			if($i==$fid){//remove file
				$objFile->file_remove($destinationPath.'/'.$_filePath);
				$row = $objFile->return_folderlist($destinationPath.'/'.$_timeStamp);
				if(sizeof($row)==0){
					$objFile->folder_remove($destinationPath.'/'.$_timeStamp);
				}
			}else{ 
				$array[] = array('referenceCaption'=>$_caption,'referencePath'=>$_filePath);
			}
			$i++;
		}
	}	
	return $array;
}

?>