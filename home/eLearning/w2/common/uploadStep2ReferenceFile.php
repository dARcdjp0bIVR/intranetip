<?
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentBuilder.php");
include_once($PATH_WRT_ROOT."includes/w2/libW2EngContentParser.php");
include_once($intranet_root."/includes/libfilesystem.php");
$objDB = new libdb();
$objEngContentBuilder = new libEngContentBuilder();
$engContentParser = new libW2EngContentParser();
$objFile = new libfilesystem();
#init
$step2Reference = array();
$uploadFileListAry = array();
$cid = trim($_POST['cid']);
$step = trim($_POST['step']);
$type = trim($_POST['type']);

define("UPLOAD_SUCCESS", 1);
$tarPath = '/file/w2/reference/cid_'.$cid;
$destinationPath = $intranet_root.$tarPath;

$parseData = $engContentParser->parseEngContent($cid);
$step2Data = $parseData['step2Data'];
$step2Concept = $step2Data['step2ConceptAry'];
$step2Int = $step2Data['step2Int'];
$step2ReferenceAry = $step2Data['step2ReferenceAry'];
$typeIsChanged = $type!=$step2ReferenceAry['referenceType'];

if($type=='normal'){
	#Existing Files
	deleteExistingData($step2ReferenceAry['referenceData']['compareData1']['itemAry']);
	deleteExistingData($step2ReferenceAry['referenceData']['compareData2']['itemAry']);
	unset($step2ReferenceAry['referenceData']['compareData1']);
	unset($step2ReferenceAry['referenceData']['compareData2']);
	#New Files
	$counter = $reference_normal_counter;
	for($i=0;$i<$counter;$i++){
		$_file = $_FILES['r_normalImage_'.$i];
		$_name = $_file['name'];
		$_type = $_file['type'];
		$_size = $_file['size'];
		$_tmp_name = $_file['tmp_name'];
		$_error = $_file['error'];
		$_referenceCaption = addslashes(intranet_htmlspecialchars(trim($_POST['r_normalImage_Caption_'.$i])));
		if(trim($_name) != ''){
			$uploadFileListAry['normalData1']['itemAry'][] = array('name'=>$_name,'type'=> $_type,'size'=>$_size,'tmp_name'=>$_tmp_name,'error'=>$_error,'referenceCaption'=>$_referenceCaption);
		}
	}
}else if($type=='compare'){
	#Existing Files
	deleteExistingData($step2ReferenceAry['referenceData']['normalData1']['itemAry']);	
	unset($step2ReferenceAry['referenceData']['normalData1']);
	#New Files
	$counter = $reference_compare1_counter;
	for($i=0;$i<$counter;$i++){
		$_file = $_FILES['r_compare1Image_'.$i];
		$_name = $_file['name'];
		$_type = $_file['type'];
		$_size = $_file['size'];
		$_tmp_name = $_file['tmp_name'];
		$_error = $_file['error'];
		$_referenceCaption = addslashes(intranet_htmlspecialchars(trim($_POST['r_compare1Image_Caption_'.$i])));
		if(trim($_name) != ''){
			$uploadFileListAry['compareData1']['itemAry'][] = array('name'=>$_name,'type'=> $_type,'size'=>$_size,'tmp_name'=>$_tmp_name,'error'=>$_error,'referenceCaption'=>$_referenceCaption);
		}
	}	
	$uploadFileListAry['compareData1']['itemTitle'] = addslashes(intranet_htmlspecialchars(trim($_POST['compare1_title'])));	
	$counter = $reference_compare2_counter;
	for($i=0;$i<$counter;$i++){
		$_file = $_FILES['r_compare2Image_'.$i];
		$_name = $_file['name'];
		$_type = $_file['type'];
		$_size = $_file['size'];
		$_tmp_name = $_file['tmp_name'];
		$_error = $_file['error'];
		$_referenceCaption = addslashes(intranet_htmlspecialchars(trim($_POST['r_compare2Image_Caption_'.$i])));
		if(trim($_name) != ''){
			$uploadFileListAry['compareData2']['itemAry'][] = array('name'=>$_name,'type'=> $_type,'size'=>$_size,'tmp_name'=>$_tmp_name,'error'=>$_error,'referenceCaption'=>$_referenceCaption);
		}
	}
	$uploadFileListAry['compareData2']['itemTitle'] = addslashes(intranet_htmlspecialchars(trim($_POST['compare2_title'])));	
}
foreach ($uploadFileListAry as $_refType => $_refImageAry){
	foreach ((array)$_refImageAry['itemAry'] as $__infoAry){
		//check has any upload error
		if ($__infoAry["error"] > 0){
			exit();
		}
		$timeStamp = date('Y-m-d_H-i-s');
		$__fileName = $__infoAry['name'];
		$r_filepath = $timeStamp.'/'.$__fileName;
		$__descPath = $destinationPath.'/'.$timeStamp;
		$__destinationFile = $destinationPath.'/'.$r_filepath;
		$folderIsReadyForCopy = false;
	
		//check related folder if not exist
		if(file_exists($__descPath)){
			//do nothing
			$folderIsReadyForCopy  = true;
		}else{
			$objFile->folder_new($__descPath);
	
			//make sure the folder exist
			if(file_exists($__descPath)){
				$folderIsReadyForCopy  = true;
			}else{
				//cannot create the folder stop the upload
				exit();
			}
		}
	
		if($folderIsReadyForCopy){
			$result = move_uploaded_file($__infoAry["tmp_name"],$__destinationFile);
			$__referenceCaption = $__infoAry['referenceCaption'];
			if($result == UPLOAD_SUCCESS && file_exists($__destinationFile)){
				$step2ReferenceAry['referenceData'][$_refType]['itemAry'][] = array('referenceCaption'=>$__referenceCaption,'referencePath'=>$r_filepath);
			}
		}
	}
	$step2ReferenceAry['referenceData'][$_refType]['itemTitle'] = $_refImageAry['itemTitle'];
}

$step2ReferenceAry['referenceTopic'] = addslashes(intranet_htmlspecialchars(trim($topic)));
$step2ReferenceAry['referenceType'] = $type;

$buildData = $objEngContentBuilder->buildChiStep2($step2Int,$step2Concept,$step2ReferenceAry);
$sql = "UPDATE W2_CONTENT_DATA SET ".$buildData['field']." = ".$buildData['content']." WHERE cid='".$cid."'";
$rs = $objDB->db_db_query($sql);

function deleteExistingData($existingData){
	global $objFile,$destinationPath;
	if(count($existingData)>0){
		$i=0;
		foreach($existingData as $_referenceItemAry){
			$_path = $_referenceItemAry['referencePath'];
			list($_timeStamp,$_fileName) = explode('/',$_path); 
			$objFile->file_remove($destinationPath.'/'.$_path);
			$row = $objFile->return_folderlist($destinationPath.'/'.$_timeStamp);
			if(sizeof($row)==0){
				$objFile->folder_remove($destinationPath.'/'.$_timeStamp);
			}
			$i++;
		}
	}	
}
?>
<script>
//refresh the file upload panel to display the new update file
parent.refreshStep2ReferenceFilePanel();
</script>
