<?php
$attachmentFile = safe_b64decode($r_fName);
list($timeStamp,$fileName) = explode('/',$attachmentFile);
$tarPath = '/file/w2/teacher_attachment/cid_'.$cid;
$destinationPath = $intranet_root.$tarPath;
$file_abspath = $destinationPath.'/'.$attachmentFile;
if($attachmentFile == ''){
	exit();
}
// Check browser agent
$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
else { $agentName = 'unrecognized'; }

// Output file to browser
$mime_type = mime_content_type($file_abspath);
$file_content = file_get_contents($file_abspath);

switch ($agentName) {
	default:
	case "msie":
		$file_name = urlencode($fileName);
		break;
	case "mozilla":
		$file_name = $fileName;
		break;
}

# header to output
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-type: $mime_type");
header("Content-Length: ".strlen($file_content));
header("Content-Disposition: attachment; filename=\"".$fileName."\"");

echo $file_content;

?>