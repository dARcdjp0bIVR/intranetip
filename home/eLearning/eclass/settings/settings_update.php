<?php
// Modifing by : Pun


/********************** Change Log ***********************/
#	Date	:	2020-03-30 (Pun)
# 	Details	:	 Added eLearning timetable
#
#	Date	:	2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
# 	Details	:	 Added access right checking
#
#	Date	:	2015-09-28 (Paul)
# 	Details	:	 add default max file size for student upload
#
#	Date	:	2014-09-03 (Charles Ma)
# 	Details	:	 eClass setting - PowerLesson and Enable children's eClass  for all class
# 					- [20140903-eclass-setting-pl-setting-parent2cchild-eclass]
#
# 	Date	:	2012-08-22 [Siuwan]
#	Details	:	add $settingdata['isAccessChildrenEClassMgt'] for Children's eClass
#
# 	Date	:	2010-04-16 [Yuen]
#	Details	:	add default max course storage setting
#				will be adopted when create new courses
#
# 	Date	:	Stanley
#	Details	:	Settings are kept in two files.
#				One is for eClass settings used in Intranet while the other is used inside eClass.
#
/******************* End Of Change Log *******************/


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$leclass = new libeclass2007();
$lgeneralsettings = new libgeneralsettings();

$settingdata['isAccessEClassMgt'] = ($ec_mgt) ? "1" : "0";
$settingdata['isAccessEClassMgtCourse'] = ($ec_mgt_course) ? "1" : "0";
$settingdata['isAccessEClassNTMgtCourse'] = ($nt_mgt_course) ? "1" : "0";
$settingdata['isAccessChildrenEClassMgt'] = ($ec_p_mgt) ? "1" : "0";
$settingdata['isEmbeddedPowerLesson'] = ($ec_pl) ? "1" : "0";//20140903-eclass-setting-pl-setting-parent2cchild-eclass
# default email
# 0 = Enable , 1 = Disable
$settingdata['isAccessEClassEmailTeacher'] = ($ec_email_teacher_disabled) ? "0" : "1";
$settingdata['isAccessEClassEmailHelper'] = ($ec_email_helper_disabled) ? "0" : "1";
$settingdata['isAccessEClassEmailStudent'] = ($ec_email_student_disabled) ? "0" : "1";

$settingdata['default_max_quota'] = ($default_max_quota>0)? $default_max_quota : 30;
$settingdata['default_max_size'] = (($default_max_size != "")&&(preg_match((('/(^[0-9]+$)/')), $default_max_size))) ? $default_max_size : -1;

# store in DB
$lgeneralsettings->Save_General_Setting("eClassSettings", $settingdata);

/*
# planner color type
for ($i=0; $i<sizeof($planner_color); $i++)
{
	$ec_planner[] = stripslashes($planner_color[$i]."||".$planner_type[$i]);
}

$li = new libfilesystem();
$li->file_write(implode("\n",$ec_ip_set)."\n", $intranet_root."/file/eclass.txt");

$li->file_write(implode("\n",$ec_set)."\n", $eclass_filepath."/files/settings.txt");

//$li->file_write(implode("\n",$ec_planner)."\n", $eclass_filepath."/files/planner.txt");

*/

# badword #
$data = stripslashes($data);
$data = intranet_htmlspecialchars($data);

$base_dir = "$intranet_root/file/";
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$target_file = "$base_dir"."templates/bulletin_badwords.txt";
$data = write_file_content($data, $target_file);
# badword #

# eLearning timetable #
$enablningTimetableEnabled = IntegerSafe($_POST['eLearningTimetableEnabled']);
$sql = "UPDATE 
	GENERAL_SETTING 
SET 
	SettingValue='{$enablningTimetableEnabled}', 
	DateInput=now()
WHERE
	Module='eLearningTimetable'
AND
	SettingName='enableTimetable'
";
$lgeneralsettings->db_db_query($sql);

# eLearning timetable #

# powervoice #
if ($plugin['power_voice'])
{

	$pvoice_target_file = "$base_dir"."powervoice.txt";
	$pvoice_data = get_file_content($pvoice_target_file);
	$pvoice_array = unserialize($pvoice_data);
	$pvoice_array['bitrate'] = $bitrate;
	$pvoice_array['sampling_frequency'] = $samplerate;
	$pvoice_array['length'] = $voice_length;

	$pvoice_output = serialize($pvoice_array);
	$data = write_file_content($pvoice_output, $pvoice_target_file);

}
# powervoice #
intranet_closedb();
header("Location: index.php?xmsg=update");
?>
