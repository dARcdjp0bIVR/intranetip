<?php
/*
 * Editing by Pun 
 * 
 * Modificaiton Log:
 * 2018-05-30 (Pun) [139963] [ip.2.5.9.7.1]
 *      - Fixed cannot display stats
 * 2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
 *      - Added access right checking
 * 2017-09-11 (Pun) [125228] [ip.2.5.8.10.1]
 * 		- Fix cannot change page size number
 * 2015-12-15 (Paul) [ip.2.5.7.1.1]
 * 		- fix wrong lang file path
 * 2010-12-16 (Jason)
 * 		- fix the problem of duplicated classroom statistics by removing the group by condition and add the sub-select phase 
 */
@SET_TIME_LIMIT(600);
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once("$eclass40_filepath/src/includes/php/lib-quota.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);
include_once("$eclass40_filepath/system/settings/config.inc.php");
include_once("$eclass40_filepath/system/settings/lang/admin/".substr($lang, 0, strpos($lang, ".")).".language.php");


intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface = new interface_html();
$CurrentPage = "PageStatistics";

$lo = new libeclass();
$leclass = new libeclass2007();
$li = new libdb();
$li->db = classNamingDB($eclass_db);

# Left menu

$title = $i_eClass_Admin_Stats;
$TAGS_OBJ[] = array($i_eClass_Admin_stats_files,$PATH_WRT_ROOT."home/eLearning/eclass/stats/usage_files/index.php",0);
$TAGS_OBJ[] = array($i_eClass_Admin_stats_participation,$PATH_WRT_ROOT."home/eLearning/eclass/stats/participation/index.php",1);

$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$qo = new quota($eclass_db, 0);
$qo->calAllCourseFileUsage($eclass_db);



# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 3;


// build temp duration table
$sql = "CREATE TEMPORARY TABLE ec_login_hits (
		course_id int(8),
		hit_total int(8),
		hit_T int(8),
		hit_S int(8),
		hit_A int(8)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";
		
$li->db_db_query($sql);
$sql = "SELECT course_id, RoomType FROM {$eclass_db}.course ";
$row_all = $li->returnArray($sql,2);

$now = time();
$today = date('Y-m-d',$now)." 23:59:59";
if($searchBy==="")
{
	$searchBy = 0;
}
else if($searchBy==1 && $start=="" && $end=="")
{
	$start = date('Y-m-d',$now);
	$end = $start;
}
for ($i=0; $i<sizeof($row_all); $i++)
{
	list($cid, $rtype) = $row_all[$i];
	
	if ($searchBy==1)
	{
		if($start!="" && $end!="" && $start!=$end)
		{
			$sql_range = "AND UNIX_TIMESTAMP(lu.inputdate) BETWEEN UNIX_TIMESTAMP('$start') AND (UNIX_TIMESTAMP('$end')+86400) ";
		}
		else 
		{
			if($start==$end || $end=="")
				$exact_date = $start;
			else if($start=="")
				$exact_date = $end;

			$sql_range = "AND UNIX_TIMESTAMP(lu.inputdate) BETWEEN UNIX_TIMESTAMP('$exact_date') AND (UNIX_TIMESTAMP('$exact_date')+86400) ";
		}
	}
	else
	{
		switch ($range){
		case 0: // today
			$sql_range = ($rtype==0) ? "AND TO_DAYS('".$today."')=TO_DAYS(lu.inputdate)" : "AND TO_DAYS('".$today."')=TO_DAYS(ls.inputdate)";
			break;
		case 1: // last week
			$sql_range = ($rtype==0) ? "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=604800" : "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=604800";
			break;
		case 2: // last 2 weeks
			$sql_range = ($rtype==0) ? "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=1209600" : "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=1209600";
			break;
		case 3: // last month
			$sql_range = ($rtype==0) ? "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=2678400" : "AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(lu.inputdate)<=2678400";
			break;
		case 4: // all
			$sql_range = " ";
			break;
		}
	}

	if($rtype==0)
	{
		$sql = "INSERT INTO ec_login_hits (course_id,hit_total,hit_T,hit_S,hit_A)		
				SELECT z.course_id, z.cnt1, z.cnt2, z.cnt3, z.cnt4 from (
					SELECT 
						'$cid' as course_id, count(*) as cnt1, count(T.user_id) as cnt2, 
						count(S.user_id) as cnt3, count(A.user_id) as cnt4
					FROM 
						".classNamingDB($cid).".login_usage as lu 
					LEFT JOIN 
						".classNamingDB($cid).".login_session AS ls 
					ON 
						ls.login_session_id=lu.login_session_id 
					inner JOIN 
						".classNamingDB($cid).".usermaster AS u 
					ON 
						u.user_id=ls.user_id 
					Left JOIN 
						".classNamingDB($cid).".usermaster AS T 
					ON 
						u.user_id=T.user_id and T.memberType = 'T'
					Left JOIN 
						".classNamingDB($cid).".usermaster AS S 
					ON 
						u.user_id=S.user_id  and S.memberType = 'S'
					Left JOIN 
						".classNamingDB($cid).".usermaster AS A 
					ON 
						u.user_id=A.user_id  and A.memberType = 'A'
					
					WHERE 
						(
						    ( lu.section_name = '{$cfg['usage_module']['econtent']}' AND lu.action LIKE 'read econtent%' ) OR
						    ( lu.section_name = '{$cfg['usage_module']['assessment']}' AND lu.action LIKE 'view assessment details - %' ) OR
						    ( lu.section_name = '{$cfg['usage_module']['assessment']}' AND lu.action = 'view self-test' ) OR
						    ( lu.section_name = '{$cfg['usage_module']['forum']}' AND lu.action = 'view forum' ) OR
						    ( lu.section_name = '{$cfg['usage_module']['member']}' AND lu.action = 'view member list' ) OR
						    ( lu.section_name = '{$cfg['usage_module']['member']}' AND lu.action = 'view message history' ) OR
						    ( lu.section_name = '{$cfg['usage_module']['resource']}' AND lu.action = 'view eclass files' ) OR
    						lu.function_name like '%/src/econtent/econtent.php' OR 
    						lu.function_name like '%/src/assessment/std_assessment_detail.php'  OR
    						lu.function_name like '%/src/assessment/std_assessment_detail2.php'  OR 
    						lu.function_name like '%/src/assessment/selftest/test_index.php' OR					 
    						lu.function_name like '%/src/forum/forum.php' OR 
    						lu.function_name like '%/src/member/member_list.php' OR
    						lu.function_name like '%/src/member/member_list_msg_history.php' OR
    						lu.function_name like '%/src/resource/eclass_files/eclass_files.php'
						) 					
						$sql_range 
				) as z where z.cnt1 > 0 ";
					//-- GROUP BY u.memberType

				//lu.function_name like '%/src/course/resources/links/help.php' OR
	}
	else
	{
		$sql = "INSERT INTO ec_login_hits (course_id,hit_total,hit_T,hit_S,hit_A)
				SELECT z.course_id, z.cnt1, z.cnt2, z.cnt3, z.cnt4 from (
					SELECT 
						'$cid' as course_id, count(*) as cnt1, count(T.user_id) as cnt2, 
						count(S.user_id) as cnt3, count(A.user_id) as cnt4 
						FROM 
						".classNamingDB($cid).".login_usage as lu 
					LEFT JOIN 
						".classNamingDB($cid).".login_session AS ls 
					ON 
						ls.login_session_id=lu.login_session_id  
					LEFT JOIN ".classNamingDB($cid).".usermaster AS u ON u.user_id=ls.user_id 
					LEFT JOIN ".classNamingDB($cid).".usermaster AS T ON T.user_id=u.user_id  and T.memberType = 'T'
					LEFT JOIN ".classNamingDB($cid).".usermaster AS S ON S.user_id=u.user_id  and S.memberType = 'S'
					LEFT JOIN ".classNamingDB($cid).".usermaster AS A ON A.user_id=u.user_id  and A.memberType = 'A'		
					where 1 $sql_range 
				) as z where z.cnt1 > 0 "; // group by u.memberType
	}
	/*$row = $li->returnArray($sql, 2);

	$sql_values = array();
	$total_in_course = 0;
	for ($j=0; $j<sizeof($row); $j++)
	{
		list($total, $memberType) = $row[$j];
		$sql_values[$memberType] = $total;
		$total_in_course += $total;
	}
	if (sizeof($sql_values)>0)
	{
		$sql = "INSERT INTO ec_login_hits VALUES ($cid, $total_in_course, ".((int) $sql_values['T']).", ".((int) $sql_values['S']).", ".((int) $sql_values['A']).") ";
	}*/
	$li->db_db_query($sql);
}

//$fieldname  = "if(a.RoomType=0,CONCAT('<a class=tableContentLink href=\"javascript:viewCourse(', a.course_id, ')\">', a.course_name, '</a>'), a.course_name), ";
$fieldname  = "a.course_name, ";
$fieldname .= "ifnull(h.hit_total, 0), ";
$fieldname .= "ifnull(h.hit_T, 0), ";
$fieldname .= "ifnull(h.hit_S, 0), ";
$fieldname .= "ifnull(h.hit_A, 0) ";
//$sql  = "SELECT DISTINCT $fieldname FROM course AS a ";
//$sql .= "LEFT JOIN ec_login_hits as h ON h.course_id=a.course_id ";
$sql  = "SELECT DISTINCT $fieldname FROM course AS a, ec_login_hits as h ";
$sql .= "WHERE h.course_id=a.course_id AND a.course_name like '%$keyword%' ";


// echo htmlspecialchars($sql); echo '<br>'.$eclass_db;
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.course_name", "h.hit_total", "h.hit_T", "h.hit_S", "h.hit_A");
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = 2;
// debug_r($li->returnArray($sql));
// TABLE COLUMN
$li->column_list .= "<td width=1% height='20' background='$BackTitle' class=tableTitle>&nbsp;#&nbsp;</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(0, $i_eClassCourseName)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(1, $i_eClassHitsTotal)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(2, $i_eClassHitsTeacher)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(3, $i_eClassHitsStudent)."</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(4, $i_eClassHitsAssistant)."</td>\n";



if($searchBy==0)
{
	$periodSelectTable  .= "<select name=range onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
	$periodSelectTable .= "<option value=0 ".(($range==0)?"selected":"").">$admin_range1</option>\n";
	$periodSelectTable .= "<option value=1 ".(($range==1)?"selected":"").">$admin_range2</option>\n";
	$periodSelectTable .= "<option value=2 ".(($range==2)?"selected":"").">$admin_range3</option>\n";
	$periodSelectTable .= "<option value=3 ".(($range==3)?"selected":"").">$admin_range4</option>\n";
	$periodSelectTable .= "<option value=4 ".(($range==4)?"selected":"").">$admin_range5</option>\n";
	$periodSelectTable .= "</select> \n";

	$searchDisplay1 = $periodSelectTable;
	$searchDisplay2 = "<a href=\"javascript:changeDateTable(1)\" class='iconLink'>".$i_Profile_SearchByDate."</a>\n";
}
else
{
	$dateSearchTable .= "$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To " .
						"<input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) " .
						$linterface->GET_BTN($button_find, "submit", "","submit3").
						"</a>\n";

	$searchDisplay2 = "<a href=\"javascript:changeDateTable(0)\" class='iconLink'>".$i_Profile_SearchByPeriod."</a>";
	$searchDisplay1 = $dateSearchTable;
}


$searchTag 	= "<table border=\"0\" cellspacing=\"5\" cellpadding=\"5\"><tr>";
$searchTag  	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3")."</td>";
$searchTag 	.= "</tr></table>";

?>


<script language="javascript">
function viewCourse(id){
	var obj = document.form1;
	obj.course_id.value = id;
	obj.pageNo.value = 1;
	obj.field.value = (obj.field.value>0) ? obj.field.value - 1 : 0;
	obj.action = "participation_course.php";
	obj.submit();
	return;
}

function changeDateTable(type)
{
	var formObj = document.form1;
	formObj.searchBy.value = type;
	formObj.pageNo.value=1;
	formObj.submit();
}
</script>

<form name="form1" method="get">

<!--table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table-->
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left" valign="center"><?=$searchDisplay1?></td>
                  <td align="right" valign="bottom" height="28"><?=$searchTag?></td>
                </tr>
                <tr><td colspan="2" align="left" valign="center"><?=$searchDisplay2?></td></tr>
                <tr><td colspan="2" align="left" valign="center" height="10px">&nbsp;</td>
                <tr>
                  <td colspan="2">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<!--table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table-->

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<input type=hidden name=course_id value="<?=$course_id?>">
<input type=hidden name=searchBy value="<?=$searchBy?>"/>
<p></p>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>