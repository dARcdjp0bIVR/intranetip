<?php
/*
 * Editing by  
 * 
 * Modification Log:
 * 2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
 *      - Added access right checking
 * 2017-09-11 (Pun) [125228] [ip.2.5.8.10.1]
 * 		- Fix cannot change page size number
 * 2015-12-15 (Paul) [ip.2.5.7.1.1]
 * 		- fix wrong lang file path
 * 2015-01-23 (Jason) [ip.2.5.6.3.1]
 * 		- fix the problem of now showing Misc. File Size properly without removing the sys_cust flag 
 * 2014-07-12 (Jason)
 * 		- improve the performance of statisics on searching keyword 
 * 2013-12-16 (Jason)
 * 		- add the count of file uploaded by Direct Upload as miscellaneous file
 */
@SET_TIME_LIMIT(18000);
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/email.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once("$eclass40_filepath/src/includes/php/lib-quota.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linterface = new interface_html();
$CurrentPage = "PageStatistics";

$lo = new libeclass();
$leclass = new libeclass2007();

# Left menu 
$title = $i_eClass_Admin_Stats;
//$TAGS_OBJ[] = array($i_eClass_Admin_stats_files,"index.php",1);
$TAGS_OBJ[] = array($i_eClass_Admin_stats_files,$PATH_WRT_ROOT."home/eLearning/eclass/stats/usage_files/index.php",1);
$TAGS_OBJ[] = array($i_eClass_Admin_stats_participation,$PATH_WRT_ROOT."home/eLearning/eclass/stats/participation/index.php",0);

$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$qo = new quota($eclass_db, 0);

$condition = '';
if($keyword != ''){
	$condition = " and course_name like '%".str_replace(Array("%","_"),Array("\\%","\\_"),htmlspecialchars(addslashes($keyword),ENT_QUOTES))."%' ";
}
$qo->calAllCourseFileUsage($eclass_db, $condition);


$searchTag 	= "<table border=\"0\" cellspacing=\"5\" cellpadding=\"5\"><tr>";
$searchTag  	.= "<td><input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
$searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3")."</td>";
$searchTag 	.= "</tr></table>";

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 3;

$sql = "SELECT
		   CONCAT('<a class=\"tablelink\" href=\"usage_course.php?course_id=', course_id, '\">', course_name, '</a>'), 
		   ifnull(max_storage, '-'),
		   file_no,
		   file_size,
		   (max_storage-file_size) AS storage_left, 
			misc_file_size  
	  FROM
			{$eclass_db}.course_file_usage ";
//	  WHERE
//           (course_name like '%".str_replace(Array("%","_"),Array("\\%","\\_"),htmlspecialchars(addslashes($keyword),ENT_QUOTES))."%')
	  
// echo htmlspecialchars($sql); echo '<br>'.$eclass_db;
# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("course_name", "max_storage", "file_no", "file_size", "storage_left");
//if($sys_custom['enable_misc_file_statistics']){
	$li->field_array[] = "misc_file_size";
//}
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0);
//if($sys_custom['enable_misc_file_statistics']){
	$li->column_array[] = 0;
//}
$li->IsColOff = 2;
// debug_r($li->returnArray($sql));
// TABLE COLUMN
$li->column_list .= "<td width=1 class='tabletop' style='vertical-align:middle'>#</td>\n";
$li->column_list .= "<td width=40% class='tabletop' style='vertical-align:middle'>".$li->column(0, $i_eClassCourseName)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(1, $i_eClassMaxStorage)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(2, $i_eClassFileNo)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(3, $i_eClassFileSize)."</td>\n";
$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(4, $i_eClassFileSpace)."</td>\n";
//if($sys_custom['enable_misc_file_statistics']){
	$li->column_list .= "<td width=15% class='tabletop' style='vertical-align:middle'>".$li->column(5, $Lang['eClassStatistics']['MiscellaneousFileSize'].' ('.$file_mb.')')."</td>\n";
//}
?>


<form name="form1" method="get">

<!--table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table-->
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
      <td align="center">
        <table width="96%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="left" class="tabletext">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td align="left" valign="bottom"><?=$searchTag?></td>
                  <td align="right" valign="bottom" height="28">
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <?=$li->display();?>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
<!--table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table-->

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<p></p>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>