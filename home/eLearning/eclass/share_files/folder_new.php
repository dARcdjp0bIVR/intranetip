<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

if ($courseID=="" || $categoryID=="")
{
	header("Location: index.php");
	die();
}

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] && (!(isset($sys_custom['project']['NCS'])&&$sys_custom['project']['NCS']==true && $_SESSION['UserType']==1 && $_SESSION['isTeaching']==1)))
{
  if (!$lu->teaching || !$la->isAccessEClassMgtCourse())
  {
    header("Location: /");
  }
}

$ori_memberType = $ck_memberType;
$ck_memberType = "Z";

$fm = new fileManager($courseID, $categoryID, $folderID);
$vPath = stripslashes($fm->getVirtualPath());

$linterface = new interface_html();
$CurrentPage = "PageShareArea";

$li = new libuser($UserID);
$lo = new libeclass();
$leclass = new libeclass2007();
$lp = new libportal();

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_directory.gif' align='absmiddle' /> $i_eClass_Admin_Shared_Files";
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
$PAGE_NAVIGATION[] = array($i_eClass_Admin_Shared_Files, "index.php");	
$PAGE_NAVIGATION[] = array($button_newfolder, "");		
?>
<link rel=stylesheet href="<?=$eclass_url_root?>/css/master.css">
<link rel=stylesheet href="<?=$eclass_url_root?>/css/common.css">
<style type="text/css">
body {
	background-color: #e3f3fe;
	background-image: none;
}
</style>
<script language="javascript">
function checkform(obj){
	if(!check_text(obj.filename, "<?php echo $marksheet_alert_msg2; ?>")) return false;
	myfile = obj.filename.value;
	if (myfile.indexOf("\\")>-1 || myfile.indexOf("/")>-1 || myfile.indexOf(":")>-1 ||
		myfile.indexOf("*")>-1 || myfile.indexOf("?")>-1 || myfile.indexOf("\"")>-1 ||
		myfile.indexOf("<")>-1 || myfile.indexOf(">")>-1 || myfile.indexOf("|")>-1) {
		obj.filename.focus();
		alert('<?=$file_msg23?>');
		return false;
	}
}
function switchCheck(obj, element_name, txt) {
	len=obj.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++) {
		if (obj.elements[i].name==element_name && obj.elements[i].value==txt)
			obj.elements[i].checked=false;
	}
}
</script>


<form name=form1 method="post" action="folder_update.php" onSubmit="return checkform(this);">
<br>
<table width=90% border=0 cellpadding=5 cellspacing=0 align="center">
  <tr>
      <td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
  </tr>
  <tr>
  <td colspan="2">
        <table width="70%" border="0" cellspacing="0" cellpadding="5" align="center"> 
        <tr>
        <td width=20% class="formfieldtitle"><span class="tabletext"><?php echo $file_location; ?>:</span>&nbsp;</td>
        <td><?=$vPath?></td>
        </tr>
        
        <tr>
        <td width=20% class="formfieldtitle"><span class="tabletext"><?php echo $file_name; ?>:</span>&nbsp;</td>
        <td><input type=text name=filename class="inputfield" size=30 maxlength=255></td>
        </tr>
        
        <tr>
        <td class="formfieldtitle" valign="top"><span class="tabletext"><?php echo $bulletin_des; ?>:</span>&nbsp;</td>
        <td><textarea class="inputfield" name=description cols=40 rows=5 wrap=virtual></textarea></td>
        </tr>
        
        <tr>
        <td class="formfieldtitle" nowrap><span class="tabletext"><?= $file_access_right ?>(<?=$file_alluser?>) :</span>&nbsp;</td>
        <td>
          <table width="300" border="0" cellspacing="1" cellpadding="0">
        	<tr>
        	  <td align="left" width="100" class="bodycolor5"><?=$file_read?></td>
        	  <td align="left" width="100" class="bodycolor5"><?=$file_readwrite?></td>
        	</tr>
        	<tr>
        	  <td align="left" class="bodycolor3">
        		<input type="checkbox" name="access_all[]" value="AR" checked onClick="switchCheck(this.form, 'access_all[]', 'AW')">
        	  </td>
        	  <td align="left" class="bodycolor3">
        		<input type="checkbox" name="access_all[]" value="AW" onClick="switchCheck(this.form, 'access_all[]', 'AR')">
        	  </td>
        	</tr>
          </table>
        </td>
        </tr>
        </table>
   </td>
   </tr>
</table>

<table width="70%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
  <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
  <td align="center">
    <?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit2") ?>
    <?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2") ?>
    <?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancelbtn") ?>
  </td>
</tr>
</table>
<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="field" value="<?=$field?>" type=hidden>
<input name="order" value="<?=$order?>" type=hidden>

</form>

<?php
$ck_memberType = $ori_memberType;
$linterface->LAYOUT_STOP();
intranet_closedb();
?>