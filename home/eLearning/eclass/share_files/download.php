<?php
# Stanley
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass40_filepath/system/settings/lang/".$lang);

intranet_opendb();

$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";

$fm = new fileManager($courseID, $categoryID, $folderID);

//create user folder to save the zip file
$user_tmp_folder = $ec_file_path."/tmp";
$fm->createfolder($user_tmp_folder);
$user_tmp_folder .= "/admin";
$fm->createfolder($user_tmp_folder);
$fm->tmp_folder = $user_tmp_folder;

//$path_base = the prefix of path, needs to be eliminated
if ($folderID!="") {
	$sql = "SELECT VirPath, Title FROM eclass_file WHERE FileID='$folderID'";
	$row = $fm->returnArray($sql, 2);
	$fm->path_base = ($row[0][0]!="") ?
		$row[0][0].$row[0][1]."/" :
		"/".$row[0][1]."/";

}
//name of zip file for download
if ($fm->path_base!="")
	$fm->file_name = str_replace(" ", "", basename($fm->path_base));
else
	$fm->file_name = str_replace(" ", "", basename($fm->getCategoryRoot($categoryID, $courseID)));
$file_folder = $user_tmp_folder."/".$fm->file_name;
exec("rm -r ".OsCommandSafe($file_folder));
$fm->createfolder($file_folder);

//create file recursively
for($i=0;$i<sizeof($file_id);$i++){
	//if ($fm->checkAccessRight($ck_user_id, $file_id[$i])!="0")
	$fm->createFileFolder($file_id[$i]);
}

//zip the path to file
$dest = $user_tmp_folder . "/download.zip";
$fm->removeFile($dest);							//del old zip file
chdir($user_tmp_folder);						/* change directory in order to avoid zip unwanted paths */

//check if only folders exists (which will cause unzip problem in winzip)
if ($fm->total_file==0) {
	exec("rm -r ".OsCommandSafe($fm->file_name));
	//header("Location: help.php?err=13&".$params);
	header("Location: index.php?xmsg2=".rawurlencode($file_msg21)."&".$params);
	die();
}

exec("zip -r ".OsCommandSafe($dest)." ".OsCommandSafe($fm->file_name));			//zip

//delete files & folders
exec("rm -r ".OsCommandSafe($fm->file_name));

//read the zip file and send to user
if (file_exists($dest)) {
	header('Content-Type: application/octet-stream');
	header("Content-Disposition: inline; filename=".$fm->file_name.".zip");
	readfile($dest);
}

//delete the zip file)
$fm->removeFile($dest);

intranet_closedb();
?>
