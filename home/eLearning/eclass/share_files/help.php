<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# get current lang type from intranet
switch($intranet_session_language){
	case 'en': 	$eclass_lang = 'eng'; 	break;
	case 'b5': 	$eclass_lang = 'chib5'; break;
	case 'gb': 	$eclass_lang = 'chigb'; break;
	default:	$eclass_lang = 'eng'; 	break;
}

# include eclass40 lang pack
include_once($eclass40_filepath.'/system/settings/lang/'.$eclass_lang.'.php');


//head(2);
$x .= "<html><head><title>eClass</title>\n";
$x .= "<META HTTP-EQUIV='Pragma' CONTENT='no-cache'>\n";
$x .= "<meta http-equiv='content-type' content='text/html; charset=UTF-8'>";
$x .= '<style TYPE="text/css">';
$x .= 'body{ background-color:#FFFFFF; background-image:none; }';
$x .= 'body, input, select, textarea, table{
		font-family: Verdana, Arial, Helvetica, sans-serif; 
		color: #000000;
		font-size: 12px;
		}';
$x .= '</style>';
$x .= "<body leftmargin='5' topmargin='5' marginwidth='5' marginheight='5' $scoll_control >\n";
echo $x;


switch($err)
{
	case 1:	$message = $file_msg4; break;
	case 2:	$message = $file_msg6; break;
	case 3:	$message = $file_msg8; break;
	case 4:	$message = $file_msg9; break;
	case 5:	$message = $file_msg10; break;
	case 6:	$message = $file_msg11; break;
	case 7:	$message = $file_msg12; break;
	case 8:	$message = $file_msg13; break;
	case 9:	$message = $file_msg15; break;
	case 10:	$message = $file_msg17; break;
	case 11:	$message = $file_msg18; break;
	case 12:	$message = $file_msg19; break;
	case 13:	$message = $file_msg21; break;
	case 14:	$message = $file_msg22; break;
	case 15:	$message = $file_msg2; break;
	case 16:	$message = $file_msg23; break;
	default:	$message = "&nbsp;";
}


$message .= $msg;

if ($isFromEditor || $isFromFck)
{
	$buttonBack = "<input type=button class=button value='$button_back' onClick='history.back()'>\n";
} elseif ($categoryID!="" && $courseID!="")
{
	$buttonBack = "<input type=button class=button value='$button_back' onClick='window.location=\"browse.php?categoryID=$categoryID&courseID=$courseID&folderID=$folderID&fromPage=$fromPage\"'>\n";
}
else{
	$buttonBack = "<input type=button class=button value='$button_close' onClick='window.close()'>\n";
}

?>
<br><br>
<BLOCKQUOTE><span class=title><?= $message ?></span><br><br><?=$buttonBack?></BLOCKQUOTE>
</body>
</html>