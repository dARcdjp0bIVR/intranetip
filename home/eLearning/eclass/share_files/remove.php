<?php
/*
 * Editing by Paul
 * 
 * Modificaiton Log: 
 * 2015-09-01 (Jason) [ip.2.5.6.10.1]
 * 		- add $fromIntranet to identify the deletion comes from intranet
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");

intranet_opendb();

$lu = new libuser($UserID);
# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] && (!(isset($sys_custom['project']['NCS'])&&$sys_custom['project']['NCS']==true && $_SESSION['UserType']==1 && $_SESSION['isTeaching']==1)))
{
  if (!$lu->teaching || !$la->isAccessEClassMgtCourse())
  {
    header("Location: /");
  }
}

$fm = new fileManager($courseID, $categoryID, $folderID);
$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";

$fromIntranet = 1;

for($i=0;$i<sizeof($file_id);$i++){
	$fm->deleteFile($file_id[$i], $fromIntranet);
}

$fm->updateFolderSize($folderID);

intranet_closedb();

header("Location: index.php?xmsg=delete&$params&reload=1");
?>
