<?php
/**
 * Editing by  Paul
 *
 * Modification Log:
 * 2020-01-31 (Paul) [ip.2.5.11.3.1]
 * - handle non-english name file download using Non-chromium version of Edge
 * 2018-12-03 (Pun) [ip.2.5.10.1.1]
 * - Added NCS cust
 * 2016-09-07 (Paul) [ip.2.5.7.10.1]
 * - replace all split by explode for PHP 5.4
 * 2015-08-10 (Jason)
 * - fix the problem of showing monster code during open the file with chinese name
 * 2012-04-19 (Jason)
 * - fix the problem of not showing warning message if the file does not exist / could not be found
 * 2011-03-22 (Jason)
 * - handle the chinese filename of url in order to support in IE without tunning on the support of UTF8 URL
 * 2010-06-04 (Sandy)
 * - $isDirectOpenURL , change from always TRUE -> true for certain , since must set header type for DOCX file, otherwise will treat it as ZIP file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once("$eclass40_filepath/src/includes/php/lib-filemanager.php");

intranet_opendb();

/* parameters
 1. fid = fileID
 OR
 1. categoryID
 2. fpath = VirPath + Title
 
 optional: $courseID
 */
$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";
if ($courseID=="") $courseID=$ck_course_id;

$fm = new fileManager($courseID, $categoryID, $folderID);
//get fileID from given path

$fid_name = ($fid!="") ? $fid : basename($fpath);
if ($fid=="") {
    $fid = $fm->openFirstFile($fpath, $categoryID);
    $fid = $fm->fileID;
}

// permission control
if ($fid=="" || $fid==0) {
    //no file found
    header("Location: help.php?err=15");
    //header("Location: index.php?xmsg2=".rawurlencode($file_msg2)."&".$params);
    die();
}

$li = new libdb();
$li->db = classNamingDB($courseID);

$sql = "SELECT FileID, Location, User_id, Title, Category, Permission FROM eclass_file ";
if ($fid!="") {
    //get from fileID
    $sql .= " WHERE FileID='$fid' ";
} elseif ($categoryID!="" && $fpath!="") {
    $posit = strrchr($fpath, "/");
    $vpath = substr($fpath, 0, $posit-1);
    $title = substr($fpath, $posit+1, strlen($fpath)-$posit-1);
    $sql .= " WHERE Category='$categoryID' AND VirPath='$vpath' AND Title='$title' ";
}
$row = $li->returnArray($sql, 6);

if (sizeof($row)>0) {
    $FileID = $row[0][0];
    $Location = $row[0][1];
    $User_id = $row[0][2];
    $filename = $row[0][3];
    $categoryID = $row[0][4];
    $Permission = $row[0][5];
    
    $filepath = $fm->getCategoryRoot($categoryID, $li->db)."/".$Location."/".$filename;
    
    if (file_exists($filepath)) {
        ## determinate file type
        $fileType = strtoupper(strrchr($filename, "."));
        
        $isDirectOpenURL = ($fileType==".TXT" || $fileType==".JPG" || $fileType==".JPE" || $fileType==".GIF" ||
            $fileType==".PNG" || $fileType==".BMP" || $fileType==".HTML" || $fileType==".HTM" || $fileType==".PPT" ||
            $fileType==".MP3" ||  $fileType==".MID" || $fileType==".MIDI" || $fileType==".RMI" || $fileType==".ASF" ||
            $fileType==".WM" || $fileType==".WMA" || $fileType==".WMV" || $fileType==".AVI" || $fileType==".MPEG" ||
            $fileType==".MPG" || $fileType==".MP2" || $fileType==".WAV" || $fileType==".AU" || $fileType==".RA" ||
            $fileType==".RM" || $fileType==".RAM" || $fileType==".RMS" || $fileType==".PDF" || $fileType==".SWF" ||
            $fileType==".MOV" || $fileType==".AVI" || $fileType==".MPEG" || $fileType==".MPEG4" || $fileType==".MPG" ||
            $fileType==".MP4" || $fileType==".WMV");
        
        ## for opening file in browser
        if ($isDirectOpenURL) {
            // HTML,PPT, txt, images
            if ($g_chinese=="gb"|| true)
            {
                $tmp = explode("/", $Location);
                for ($j=0; $j<sizeof($tmp); $j++)
                {
                    $tmp[$j] = rawurlencode($tmp[$j]);
                }
                $Location = implode("/", $tmp);
                $url_filename = rawurlencode($filename);
            } else
            {
                $url_filename = $filename;
            }
            $url_filename = str_replace("#", urlencode("#"), $url_filename);
            $url = $eclass_url_root."/".$fm->getCategoryFile($categoryID, $li->db)."/".$Location."/".$url_filename;
        }else{
            //	header("Cache-control: private");		// fix for IE
            
            ##prompt download file , set header type
            if ($fileType==".DOC" || $fileType==".XLS" || $fileType==".CSV") {
                // word/excel
                header("Content-Type: application/x-msword");
                /*} elseif ($fileType==".JPG" || $fileType==".JPE" || $fileType==".GIF" ||
                 $fileType==".PNG" || $fileType==".BMP") {
                 JPEG, JPG, GIF, PNG
                 header("Content-Type: image/jpeg");
                 } elseif ($fileType==".TXT") {
                 // text
                 header("Content-Type: text/plain");*/
            } elseif ($fileType==".SWF_tmp") {
                // flash
                header("Content-Type: application/x-shockwave-flash");
            }else{
                header('Content-Type: application/octet-stream');
            }
        }
        
        //2010-06-04 (Sandy)
        //$isDirectOpenURL = true;
        
        # handle characters in the format of coding level
        $url = str_replace("&#", urlencode("&#"), $url);
        $url = str_replace("#", urlencode("#"), $url);
        
        if(
            (isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="") &&
            ((isVideoFile($filepath) && $fileType != '.WMV') || $fileType == '.MP3')
            ){
                ?>
			<style>
			body{margin:0;}
			</style>
			<?php if($fileType == '.MP3'){ ?>
    			<audio id="video_player" controls="controls">
    				<source type="audio/mp3" src="<?=$url ?>">
    			</audio>
			<?php }else{ ?>
    			<video id="video_player" style="width: 100%;height: 100%;" controls="controls">
    				<source src="<?=$url ?>">
    			</video>
			<?php } ?>
<?php
		}elseif ($isDirectOpenURL)
		{
			header("Location: $url");
		} else {
			$len = filesize($filepath);
			intranet_closedb();
			//header("Content-Length: $len");
			//header("Content-Disposition: inline; filename=".$filename);
			//readfile($filepath);
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-type: application/octet-stream");
			header("Content-Length: " . filesize($filepath));						
			//header("Content-Disposition: attachment; filename=".$filename);
			
			# copied from eclass40
			/*
			if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false) {
				// Encode the filename for IE, so that it can recognize UTF8 Chinese filename.
				// IE will decode the filename, although this may not be a correct behaviour.
				header("Content-Disposition: attachment; filename=\"".rawurlencode($filename)."\"");
			}
			else {
				// It seems that Firefox can recognize UTF8 filename, although this may not always work.
				header("Content-Disposition: attachment; filename=\"".$filename."\"");
			}
			*/
			# copied from eclass40 - lib.php - output2browser_common() 
			$attachment_value = "attachment";
			$useragent = $_SERVER['HTTP_USER_AGENT'];
			if (preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'IE';
			} elseif (preg_match( '|Edge/([0-9\.]+)|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'IE';
			} elseif (preg_match( '|Opera ([0-9].[0-9]{1,2})|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Opera';
			} elseif(preg_match('|Firefox/([0-9\.]+)|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Firefox';
			} elseif(preg_match('|Chrome/([0-9\.]+)|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Chrome';
			} elseif(preg_match('|Safari/([0-9\.]+)|',$useragent,$matched)) {
			    $browser_version=$matched[1];
			    $browser = 'Safari';
			} else {
				// browser not recognized!
			    $browser_version = 0;
			    $browser= 'other';
			}
			#############################For direct download#############################	
			$encoded_filename = $filename;
	        $encoded_filename = urlencode($filename);
			$encoded_filename = str_replace("+", "%20", $encoded_filename);
			
			if ($browser == "IE")
			{
				header('Content-Disposition: '.$attachment_value.'; filename="' . $encoded_filename . '"');
			}
			else if ($browser == "Firefox")
			{
				//header('Content-Disposition: attachment; filename*="utf8\'\'' . $filename . '"');
				header('Content-Disposition: '.$attachment_value.'; filename*=utf8\'\'' . $encoded_filename);
			}
			else if($browser == "Chrome") // for Chrome
			{
				header('Content-Disposition: '.$attachment_value.'; filename="' . $filename . '"');
			}
			else if($browser == "Safari")
			{
	//			header('Content-Disposition: '.$attachment_value.'; filename*=UTF-8\'\'' . $filename);			
	//			header('Content-Disposition: '.$attachment_value.'; filename="' . urlencode($filename) . '"');
				header('Content-Disposition: '.$attachment_value.'; filename="' . $filename . '"');
			}
			else
			{
				header('Content-Disposition: '.$attachment_value.'; filename="' . $encoded_filename . '"');
			}
			#############################For direct download#############################
			echo_file_content_intranet($filepath);
			
		}
	} else {
		intranet_closedb();
		header("Location: help.php?err=15");
		//header("Location: index.php?xmsg2=".rawurlencode($file_msg2)."&".$params);
		die();
	}
}


# real time ouput (useful for large file)
function echo_file_content_intranet($file){
	clearstatcache();
	if (file_exists($file))
	{
		if (is_file($file) && filesize($file)!=0)
		{
			$phpver_check = phpversion_compare_intranet("4.2.0");
			$handle = fopen($file, "r");
			while (!feof($handle))
			{
				echo fread($handle, 8192);
				if ($phpver_check=="LATER" || $phpver_check=="SAME")
				{
					ob_flush();
				}
			}
			fclose($handle);
		}
	}
	/*
	if (file_exists($file))
	{
		$rx = ((is_file($file)) && ($fd = fopen($file, "r"))) ? fread($fd, filesize($file)) : "";
		fclose ($fd);
		echo $rx;
	}
	*/
}

# Check and Compare PHP version
# use to determinate whether new PHP function can be used
function phpversion_compare_intranet($myVersion){
	$sys_arr = explode(".", phpversion());
	$my_arr = explode(".", $myVersion);

	$result = "SAME";
	for ($i=0; $i<sizeof($my_arr); $i++)
	{
		if ($my_arr[$i]>$sys_arr[$i])
		{
			$result = "ELDER";
			break;
		} elseif ($my_arr[$i]<$sys_arr[$i])
		{
			$result = "LATER";
			break;
		}
	}

	return $result;
}

?>