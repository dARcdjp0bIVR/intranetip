<?php
//editing: 
/**
 * Change Log:
 * 
 * 2020-05-18 Tommy
 * - modified permission checking, added $plugin["Disable_eClassroom"]
 * 
 * 2016-03-15 Pun
 * 	-	File Create
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");

intranet_auth();
eclass_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

$lu = new libuser($UserID);
$leclass = new libeclass2007();

# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt()  || $sys_custom['disable_course_removal'])
  {
    header("Location: /");
    die();
  }
}


######## Get Classroom Data START ########
$name_field = getNameFieldByLang("b.");
$courseIdList = implode("','", (array)$course_id);
$sql = "SELECT
	a.course_id,
	a.course_code,
	a.course_name,
	if(b.UserID is null,'-',".$name_field.") as creator_name,
	a.inputdate
FROM
	course as a
LEFT JOIN
	{$intranet_db}.INTRANET_USER as b 
ON 
	a.creator_id = b.UserID
WHERE 
	a.course_id IN ('{$courseIdList}')
ORDER BY
	a.course_code, a.course_name
";
$classroomInfo = $leclass->returnResultSet($sql);
######## Get Classroom Data END ########


######## UI START ########
$TAGS_OBJ[] = array("<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_management.gif' align='absmiddle' /> " . $i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>


<form id="form1" method="post" action="remove.php">
	<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td align="right" colspan="2"><?= $linterface->GET_WARNING_TABLE('',$Lang['eClass']['Classroom']) ?></td>
		</tr>
		<tr>
			<td align="left" class="tabletext" align="center">
				<?=$Lang['eClass']['ClassroomToBeDelete']?> <br>
				
				<table width="100%" border="0" cellpadding="4" cellspacing="0" align="center">
					<tr class="tabletop">
						<td width="1" class="tabletop tabletopnolink">&nbsp;#&nbsp;</td>
						<td width="10%" class="tabletop"><?=$i_eClassCourseID ?></td>
						<td width="15%" class="tabletop"><?=$i_eClassCourseCode ?></td>
						<td width="40%" class="tabletop"><?=$i_eClassCourseName ?></td>
						<td width="20%" class="tabletop"><?=$i_eClassCreatorBy ?></td>
						<td width="25%" class="tabletop"><?=$i_eClassInputdate ?></td>
					</tr>
<!-- -------- Table START -------- -->
					<?php 
					foreach ($classroomInfo as $index=>$classroom){ 
						$trClass = ($index % 2)? 'tablerow1' : 'tablerow2';
					?>
						<tr class="<?=$trClass ?>">
							<td class="tabletext">
								<?=($index+1) ?>
								<input type="hidden" name="course_id[]" value="<?=$classroom['course_id'] ?>" />
							</td>
							<td class="tabletext tablerow"><?=$classroom['course_id'] ?>&nbsp;</td>
							<td class="tabletext tablerow"><?=$classroom['course_code'] ?>&nbsp;</td>
							<td class="tabletext tablerow"><?=$classroom['course_name'] ?>&nbsp;</td>
							<td class="tabletext tablerow"><?=$classroom['creator_name'] ?>&nbsp;</td>
							<td class="tabletext tablerow"><?=$classroom['inputdate'] ?>&nbsp;</td>
						</tr>
					<?php 
					}
					?>
<!-- -------- Table END -------- -->
					
				</table>
						
			</td>
		</tr>
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['DeleteAll'], "submit", "", "", "", "", "formbutton_alert") ?>
							<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:self.location ='index.php';","cancelbtn") ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	
</form>


<script>
$('#form1').submit(function (){
	if(!confirm('<?=$Lang['eClass']['DeleteClassroomConfirmMsg'] ?>')){
		return false;
	}
});
</script>


<?php
eclass_closedb();
$linterface->LAYOUT_STOP();
?>