<?php
/* Modify by Pun */
############# Change Log [Start] ################
# Date: 2015-8-27 (Pun) [ip.2.5.6.10.1]
#		add filter only show the active or pending user [82561]
# Date: 2014-10-07 (Siuwan) [ip.2.5.5.10.1.0]
#		user returnGroupUsersExcludeCourse() to retrieve user list if not subject group (Case:#X69440)
# Date: 2012-10-10 ( CharlesMa)
#		new page for add user by group
#
############## Change Log [End] #################


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$li = new libgrouping();

if(isset($target) && isset($course_id))
{
	$returnData = "";
	if(!is_array($target)){
		$target = array($target);		
	}
	if ($cat_id=='Subj'){	
		$row = $li->returnSubjectGroupUsersExcludeCourse($target,$course_id, ' AND a.RecordStatus IN (1,2)');	
	}else{
		$row = $li->returnGroupUsersExcludeCourse($target,$course_id, ' AND a.RecordStatus IN (1,2)');			
	}
	 for($i=0; $i<sizeof($row); $i++){
	 	echo $row[$i][0] ;
	 	if($i<sizeof($row)-1)echo ",";
 	}    	
}


intranet_closedb();

?>
