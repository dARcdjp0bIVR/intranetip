<?php
/*
 * Editing by
 * 
 * Modification Log:
 * 2015-01-12 (Thomas) [ip.2.5.7.1.1]
 * 		- remove the parameter xmsg in the opener.window.location.href when reloading it in the onload function
 * 2015-10-19 (Pun) ip2.5.6.12.1
 * 		- added sync student to subject group
 * 2013-02-20 (Jason)
 * 		- add the time limit setting to ensure the page could be run completely if lot of students are imported 
 */ 
@SET_TIME_LIMIT(216000);
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
eclass_opendb();

if(!isset($course_id)){
	die();
}

$li = new libdb();
$lo = new libeclass($course_id);
$iCal = new icalendar();

# to avoid putting null ID
$ChooseUserIDTmp = $ChooseUserID;
unset($ChooseUserID);

for ($i=0; $i<sizeof($ChooseUserIDTmp); $i++)
{
	$UserIDCur = (int) trim($ChooseUserIDTmp[$i]);
	if ($UserIDCur>0)
	{
		$ChooseUserID[] = $UserIDCur;
	}
}

if(sizeof($ChooseUserID) > 0){

	$lo->eClassUserAddFullInfoByUserID($ChooseUserID, $MemberType);
	$lo->syncUserToSubjectGroup($course_id);
	
	# insert calendar viewer
	$sql = "select CalID from course
		where course_id = '$course_id'";
	$calID = $lo->returnVector($sql);
	$sql = "select UserID from CALENDAR_CALENDAR
			where CalID = '".$calID[0]."'";
	$existUser = $iCal->returnVector($sql);
	$newUser = array_diff($ChooseUserID,$existUser);
	$iCal->insertCalendarViewer($calID[0],$newUser,"W",$course_id,'C');
}
eclass_closedb();
?>
<body onLoad="document.form1.submit();opener.window.location.href=opener.window.location.href.replace(/&xmsg=[^&]*/, '').replace(/\?xmsg=[^&]*/, '?');">
<form name="form1" action="import.php" method="get">
  <input type="hidden" name="course_id" value="<?php echo $course_id; ?>">
</form>
</body>