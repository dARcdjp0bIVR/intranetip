<?php 
/* Modify by Rox */
############# Change Log [Start] ################
# Date: 2018-10-30 (Rox) [ip.2.5.9.11.1]
#		new page for add specific type of user by group
#
############## Change Log [End] #################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$course_id = IntegerSafe($course_id);

$li = new libgrouping();

if(isset($target) && isset($course_id))
{
    $returnData = "";
    if(!is_array($target)){
        $target = array($target);
    }
        
    $row = $li->returnGroupUsersExcludeCourseByType($target,$course_id, ' AND a.RecordStatus IN (1,2)',$userType);
        
    for($i=0; $i<sizeof($row); $i++){
        echo $row[$i][0] ;
        if($i<sizeof($row)-1)echo ",";
    }
}


intranet_closedb();

?>