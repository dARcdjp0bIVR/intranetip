<?php
//editing: Kit
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
eclass_opendb();

$lu = new libuser($UserID);
$iCal = new icalendar();
# block illegal access
if ($lu->RecordType!=1)
{
  header("Location: /");
}

$lo = new libeclass($course_id);

# remove calendar viewer
$sql = "select CalID from course where course_id = '$course_id'";
$calID = $lo->returnVector($sql);
$dn = $lo->db_prefix."c".$lo->course_id;
$sqlUser = (is_array($user_id)) ? implode("','", $user_id) : $user_id;
$sql = "select user_email from $dn.usermaster where user_id in ('$sqlUser')";
$userEmail = $lo->returnVector($sql);
$sqlEmail = empty($userEmail)?"":implode("','",$userEmail);
$sql = "select UserID from INTRANET_USER where UserEmail in ('$sqlEmail')";
$deleteUser = $iCal->returnVector($sql);
$iCal->removeCalendarViewer($calID[0], $deleteUser);
###

$lo->eClassUserDel($user_id);

// import user to course
if ($is_user_import)
{
	$li = new libdb();
	$lo->setRoomType($lo->getEClassRoomType($course_id));
	$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, Title, EnglishName, ChineseName, NickName FROM INTRANET_USER WHERE UserID='$teacher_benefit' ";
     $row = $li->returnArray($sql,11);
     for($i=0; $i<sizeof($row); $i++){
          if($lo->max_user <> "" && $i == $lo->ticketUser()) break;
         // $UserID = $row[$i][0];
          $UserEmail = $row[$i][1];
          $UserPassword = $row[$i][2];
          $ClassNumber = $row[$i][3];
          $FirstName = $row[$i][4];
          $LastName = $row[$i][5];
          $ClassName = $row[$i][6];
          $Title = $row[$i][7];
          $EngName = $row[$i][8];
          $ChiName = $row[$i][9];
          $NickName = $row[$i][10];
          $eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
          /*
          if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
              $eclassClassNumber = $ClassNumber;
          else
              $eclassClassNumber = $ClassName ." - ".$ClassNumber;
          */    
          $lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, "T", $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName);
    }
	$lo->eClassUserNumber($lo->course_id);

	// find the user_id in course
	$sql = "SELECT user_id FROM ".$lo->db_prefix."c$course_id".".usermaster WHERE user_email='$UserEmail' AND (status IS NULL OR status='') ";
	$row = $lo->returnVector($sql);
	$teacher_benefit = (int) $row[0];
}

# transfer removed teacher's files to another teacher 
if ($teacher_benefit!=0)
{
	for ($i=0; $i<sizeof($teacher_removed); $i++)
	{
		$lo->assignFileTo($teacher_removed[$i], $teacher_benefit, $course_id);
	}
}

eclass_closedb();
header("Location: index.php?course_id=$course_id&xmsg=delete");
?>