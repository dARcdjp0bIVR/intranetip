<?php
#using stanley
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
eclass_opendb();

$lo = new libeclass($course_id);
// debug_r($_REQUEST);
// import user to course
if ($is_user_import)
{
	$li = new libdb();
	$lo->setRoomType($lo->getEClassRoomType($course_id));
	$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, CASE Title WHEN 0 THEN 'MR' WHEN 0 THEN 'MR.' WHEN 1 THEN 'MISS.' WHEN 2 THEN 'MRS.' WHEN 3 THEN 'MS.' WHEN 4 THEN 'DR.' WHEN 5 THEN 'PROF.' END , EnglishName, ChineseName, NickName FROM INTRANET_USER WHERE UserID='$teacher_benefit' ";
     $row = $li->returnArray($sql,11);
     for($i=0; $i<sizeof($row); $i++){
          if($lo->max_user <> "" && $i == $lo->ticketUser()) break;
          //$UserID = $row[$i][0];
          $UserEmail = $row[$i][1];
          $UserPassword = $row[$i][2];
          $ClassNumber = $row[$i][3];
          $FirstName = $row[$i][4];
          $LastName = $row[$i][5];
          $ClassName = $row[$i][6];
          $Title = $row[$i][7];
          $EngName = $row[$i][8];
          $ChiName = $row[$i][9];
          $NickName = $row[$i][10];
          $eclassClassNumber = (trim($ClassName) == "") ? $ClassNumber : trim($ClassName)." - ".trim($ClassNumber);
          /*
          if ($ClassNumber=="" || $ClassName=="" || stristr($ClassNumber,$ClassName))
              $eclassClassNumber = $ClassNumber;
          else
              $eclassClassNumber = $ClassName ." - ".$ClassNumber;
          */
          $lo->eClassUserAddFullInfo($UserEmail, $Title,$FirstName,$LastName, $EngName, $ChiName, $NickName, "T", $UserPassword, $eclassClassNumber,$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,$URL,$Info,$ClassName);
    }
	$lo->eClassUserNumber($lo->course_id);

	// find the user_id in course
	$sql = "SELECT user_id FROM ".$lo->db_prefix."c$course_id".".usermaster WHERE user_email='$UserEmail' AND (status IS NULL OR status='') ";
	$row = $lo->returnVector($sql);
	$teacher_benefit = (int) $row[0];
}
// echo '$teacher_benefit: '.$teacher_benefit;
if ($teacher_benefit!=0)
{
	for ($i=0; $i<sizeof($teacher_removed); $i++)
	{
		$lo->assignFileTo($teacher_removed[$i], $teacher_benefit, $course_id);
	//	echo "teacher added";
	}
}
 // exit;
eclass_closedb();
header("Location: index.php?course_id=$course_id&msg=2");
?>
