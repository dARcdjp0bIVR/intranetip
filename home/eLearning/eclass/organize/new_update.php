<?php
/* 
 * Editing by 
 * 
 * Modification Log: 
 * 2020-05-06 (Tommy)
 *      - modified permission checking, added $plugin["Disable_eClassroom"]
 * 2015-12-18 (Pun) [ip.2.5.7.1.1] [84012]
 * 		- Add Subject to eClass classroom for directory
 * 2011-11-16 (Jason)
 * 		- add the PowerLesson Right Settings in eClass Management if enable only
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
intranet_auth();
eclass_opendb();

$course_code = intranet_htmlspecialchars($course_code);
$course_name = intranet_htmlspecialchars($course_name);
$course_desc = intranet_htmlspecialchars($course_desc);
$max_user = intranet_htmlspecialchars(0 + $max_user);
$max_storage = intranet_htmlspecialchars(0 + $max_storage);
$max_size = intranet_htmlspecialchars(0 + $max_size);
if($max_size <= 0) $max_size = -1;
if($max_user == 0) $max_user = "NULL";
if($max_storage == 0) $max_storage = "NULL";


$lo = new libeclass();
 // debug_r($subjectGroupID);exit();
$lu = new libuser($UserID);
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
  {
    header("Location: /");
    die();
  }
}

if($lo->license != "" &&  $lo->ticket() == 0){
  die();
} else {
  $course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage, $UserID, $subjectGroupID, false, '', false, $max_size, $subjectLinkID);
  $lo->eClassSubjectUpdate($subj_id, $course_id);
}

# update subject group
if (!empty($subjectGroupID)){
$libdb = new libdb();
$sql = "update SUBJECT_TERM_CLASS set course_id = $course_id 
		where
			SubjectGroupID in ('".implode("','",array_unique($subjectGroupID))."')
		";
 $libdb->db_db_query($sql);
}

# insert course Calendar
$iCal = new icalendar();
$calID = $iCal->createSystemCalendar($course_name,3,"P",$course_desc);
$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
$lo->db_db_query($sql);

# Set Equation Editor Rights
if ($hasEquation == 1)
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $equation_class[] = $course_id;
    write_file_content(implode(",",$equation_class),$lo->filepath."/files/equation.txt");
}

# Set PowerLesson Rights
if ($enablePowerLesson == 1)
{
    $plcontent = trim(get_file_content($lo->filepath."/files/powerlesson.txt"));
    $powerlesson_class = ($plcontent=="")? array(): explode(",",$plcontent);
    $powerlesson_class[] = $course_id;
    write_file_content(implode(",",$powerlesson_class),$lo->filepath."/files/powerlesson.txt");
}


eclass_closedb();
header("Location: index.php?xmsg=add");
?>