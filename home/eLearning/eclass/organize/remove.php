<?php
//editing: 
/*
* Modification Log:
* 2020-05-06 (Tommy)
*      - modified permission checking, added $plugin["Disable_eClassroom"]
*      
*/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
eclass_opendb();

$lu = new libuser($UserID);

# block illegal access
$la = new libaccess($UserID);
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
  if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt()  || $sys_custom['disable_course_removal'])
  {
    header("Location: /");
    die();
  }
}

$lo = new libeclass();
$iCal = new icalendar();
if (count($course_id) > 0){
	$cidSql = implode("','",$course_id);
	$sql = "select CalID from course where course_id in ('$cidSql')";
	$calID = $lo->returnVector($sql);
	$iCal->removeCalendar($calID);
}

# the class is from subject group 
/*if ($isRemoveSubjectGroup == 1){
$sql = "select SubjectGroupID from SUBJECT_TERM_CLASS 
		where course_id is not null and course_id in (
			select course_id from {$eclass_db}.course 
			where course_id in ('".implode("','",$course_id)."') and
			IsFromSubjectGroup = 1
		)";
$r = $iCal->returnVector($sql);
if (!empty($r))
$subsql = implode("','",$r);
$sql = 'delete from SUBJECT_TERM where SubjectGroupID in (\''.$subsql.'\') ';
		$Result['DeleteYearTerm'] = $iCal->db_db_query($sql);
		
		// delete Subject term class record 
		$sql= 'delete from SUBJECT_TERM_CLASS where SubjectGroupID in (\''.$subsql.'\')';
		$Result['DeleteYearTermClass'] = $iCal->db_db_query($sql);
		
		// delete class-year relation
		$sql = 'delete from SUBJECT_TERM_CLASS_YEAR_RELATION where SubjectGroupID in (\''.$subsql.'\')';
		$Result['DeleteYearRelation'] = $iCal->db_db_query($sql);
		
		// delete class Teacher
		$sql = 'delete from SUBJECT_TERM_CLASS_TEACHER where SubjectGroupID in (\''.$subsql.'\')';
		$Result['DeleteClassTeacher'] = $iCal->db_db_query($sql);
		
		// delete class student
		$sql = 'delete from SUBJECT_TERM_CLASS_USER where SubjectGroupID in (\''.$subsql.'\')';
		$Result['DeleteClassStudent'] = $iCal->db_db_query($sql);
		
		// delete timetable data
		$sql = 'delete from INTRANET_TIMETABLE_ROOM_ALLOCATION where SubjectGroupID in (\''.$subsql.'\')';
		$Result['DeleteTimetableData'] = $iCal->db_db_query($sql);
}*/
$sql = "update SUBJECT_TERM_CLASS set course_id = null 
		where course_id in ('".implode("','",$course_id)."')";
$Result['update_subject_term_class'] = $iCal->db_db_query($sql);

$lo->eClassDel($course_id);

# Update Equation Editor License
if ($eclass_equation_mck != "")
{
  $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
  $equation_class = ($content=="")? array(): explode(",",$content);
  $updated_class = array();
  
  for ($i=0; $i<sizeof($equation_class); $i++)
  {
    $target = $equation_class[$i];
    if (!in_array($target,$course_id))
    {
      $updated_class[] = $target;
    }
  }
  
  write_file_content(implode(",",$updated_class),$lo->filepath."/files/equation.txt");
}


eclass_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&xmsg=delete");
?>