<?php

// Modifing by : 

/********************** Change Log ***********************/
#
#   Date    :   2020-05-06 (Tommy)
#   Details :   modified permission checking, added $plugin["Disable_eClassroom"]
#
# 	Date	:	2020-04-09 (Ray)
#	Details	:	created
#
/******************* End Of Change Log *******************/

@SET_TIME_LIMIT(600);

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

intranet_auth();
eclass_opendb();

# block illegal access
$la = new libaccess($UserID);
$la->retrieveEClassSettings();
$la->retrieveAccessEClass();
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-eClass'] || $plugin["Disable_eClassroom"])
{
	if ($lu->teaching&&!$la->isAccessEClassMgtCourse() || !$lu->teaching&&$lu->RecordType==1&& !$la->isAccessEClassNTMgtCourse() || $lu->RecordType!=1 || !$la->isAccessEClassMgt() )
	{
		header("Location: /");
	}
}
$limport = new libimporttext();
$iCal = new icalendar();


$format_array = array("CourseCode","Course","UserLogin");
$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
	header("Location: import_course_and_assign_teacher.php?failed=2");
	exit();
} else {

	$data = $limport->GET_IMPORT_TXT($filepath);

	$toprow = array_shift($data);# drop the title bar
	for ($i=0; $i<sizeof($format_array); $i++)
	{
		if (strtoupper(trim($toprow[$i])) != strtoupper($format_array[$i]))
		{
			header("Location: import_course_and_assign_teacher.php?xmsg=import_header_failed");
			exit();
		}
	}

	if($sys_custom['sahk_login_without_prefix']){
		foreach($data as $key=>$value){
			$data[$key][2] = $_SESSION['LoginPrefix'].$data[$key][0];
		}
	}
	$lo = new libeclass();

	# preload all students and teachers in associative array
	$sql = "SELECT UserID, UserEmail, UserPassword, ClassNumber, FirstName, LastName, ClassName, Title, EnglishName, ChineseName , NickName, TitleEnglish, TitleChinese, HomeTelNo, Address, FaxNo,DateOfBirth, UserLogin
				FROM INTRANET_USER WHERE recordstatus=1 AND recordtype IN (1, 2)";
	$row = $li->returnArray($sql,17);

	for ($i=0; $i<sizeof($row); $i++)
	{
		$UserLogin = $row[$i][17];
		$users[$UserLogin] = $row[$i];
	}


	# preload all courses
	$sql = "SELECT course_code, course_name FROM {$eclass_db}.course WHERE roomtype=0";
	$row = $li->returnArray($sql,2);
	for ($i=0; $i<sizeof($row); $i++)
	{
		$courses[] = $row[$i][0];

		$courses_title[$row[$i][0]] = $row[$i][1];
	}

	$Errors = array();

	$current_ticket = $lo->ticket();
	$course_data = array();
	# tidy up data
	for ($i=0; $i<sizeof($data); $i++)
	{
		$data[$i][0] = intranet_htmlspecialchars(trim($data[$i][0]));
		$data[$i][1] = intranet_htmlspecialchars(trim($data[$i][1]));
		$data[$i][2] = trim($data[$i][2]);
		list($course_code, $course_name, $user_login) = $data[$i];

		if(!isset($course_data[$course_code])) {
			$course_data[$course_code] = array();
			$course_data[$course_code]['course_users'] = array();
			$current_ticket--;
		}

		$course_data[$course_code]['course_name'] = $course_name;

		if (!is_array($users[$user_login]))
		{
			# no such user
			if($sys_custom['sahk_login_without_prefix']){
				$user_login = str_replace($_SESSION['LoginPrefix'],'',$user_login);
			}
			$Errors[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".$Lang['eclass']['import_member_error_no_user']." '{$user_login}'! ";
		}
		if (in_array($course_code, $courses))
		{
			$Errors[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".sprintf($Lang['eclass']['import_course_and_assign_teacher_error_course_exists'], $course_code, $course_name);
		}

		if(in_array($user_login, $course_data[$course_code]['course_users'])){
			$Errors[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".sprintf($Lang['eclass']['import_course_and_assign_teacher_error_user_exists'], $course_code, $course_name);
		} else {
			$course_data[$course_code]['course_users'][] = $user_login;
		}

		if($current_ticket < 0) {
			$Errors[] = $Lang['General']['ImportArr']['Row'].($i+2) . " - ".$Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage'];
		}
	}


	if (sizeof($Errors)<1)
	{
		# tidy up data
		$course_user_added = array();
        foreach($course_data as $course_code=>$data)
		{
			$course_name = $data['course_name'];

			$ChooseUserID = array();
            foreach($data['course_users'] as $user_login) {
				$UserObj = $users[$user_login];
				$User_ID = $UserObj[0];
				$ChooseUserID[] = $User_ID;
			}

			$lo = new libeclass();
			$course_id = $lo->eClassAdd($course_code, $lo->Get_Safe_SQL_Query($course_name), "");

			$iCal = new icalendar();
			$calID = $iCal->createSystemCalendar($course_name,3,"P","");
			$sql = "Update course set CalID = '$calID' where course_id = '$course_id'";
			$lo->db_db_query($sql);


			$lo = new libeclass($course_id);
			$lo->eClassUserAddFullInfoByUserID($ChooseUserID, "T");
			$lo->syncUserToSubjectGroup($course_id);

			# insert calendar viewer
			$sql = "select CalID from course
		where course_id = '$course_id'";
			$calID = $lo->returnVector($sql);
			$sql = "select UserID from CALENDAR_CALENDAR
			where CalID = '".$calID[0]."'";
			$existUser = $iCal->returnVector($sql);
			$newUser = array_diff($ChooseUserID,$existUser);
			$iCal->insertCalendarViewer($calID[0],$newUser,"W",$course_id,'C');
		}



        $result_table .= "<tr><td colspan='2'><br /><fieldset class='instruction_box'><legend class='instruction_title''>".$Lang['eclass']['import_course_and_assign_teacher_success']."</legend>\n";
        $result_table .= "<table border='0' cellpadding='5' cellspacing='2'>";
        $result_table .= "<tr><td nowrap='nowrap'>&nbsp; &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap' align='right' style='border-bottom:1px solid grey;'>".$Lang['SysMgr']['SubjectClassMapping']['eClass']."</td><td style='border-bottom:1px solid grey;'>".$Lang['eclass']['import_course_and_assign_teacher_amount']."</td></tr>\n";
        foreach ($course_data as $course_code => $data)
        {
            $result_table .= "<tr><td>&nbsp;</td><td nowrap='nowrap' align='right'>".$data['course_name']."</td><td>".count($data['course_users'])."</td></tr>\n";
        }
        $result_table .= "</table> </fieldset></td></tr>\n";


		$button_finish_cancel = $Lang['Btn']['Done'];
	} else
	{
		# some problems, cannot import...
		$result_table .= "<tr><td colspan='2'><br /><font color='red'>".$Lang['eclass']['import_member_error_failed']."</font></td></tr>\n";
		for ($i=0; $i<sizeof($Errors); $i++)
		{
			$result_table .= "<tr><td>&nbsp;</td><td>".$Errors[$i]."</td></tr>\n";
		}
		$button_finish_cancel = $Lang['Btn']['Cancel'];
	}

}

$linterface 	= new interface_html();
$CurrentPage	= "PageManagement";

$lelcass	= new libeclass2007();



### Title ###
$TAGS_OBJ[] = array($i_frontpage_menu_eclass_mgt,"");
$MODULE_OBJ = $lelcass->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['eclass']['import_course_and_assign_teacher'], "");

$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_imported_result . " (".$i_eClass_group_teacher.")", 1);
?>



	<br />
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
		</tr>
	</table>

	<form name="form1" action="import_course_and_assign_teacher_update.php" method="post" enctype="multipart/form-data">
		<?= $linterface->GET_STEPS($STEPS_OBJ) ?>

		<table id="html_body_frame" width="88%" align="center" border="0" cellspacing="0" cellpadding="5">



			<?=$result_table?>


			<tr>
				<td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
						<tr>
							<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
						</tr>
						<tr>
							<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='import_course_and_assign_teacher.php'","retrybtn") ?>
								<?= $linterface->GET_ACTION_BTN($button_finish_cancel, "button", "self.location='index.php'","finishbtn") ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>


	</form>

<?php

$linterface->LAYOUT_STOP();
eclass_closedb();
?>