<?php
# Progress
# editing by Paul

####################################
         // Change Log //
/*
 * 	
 * 	2015-10-23 (Paul) IP.2.5.7.1.1
 * 		- Create the file for LangFlow centre to for weekly progress checking 
 * 
 */         
####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
eclass_opendb();

$chartjspath = $eclass40_filepath."/js/chartjs/Chart.js";
$home_header_no_EmulateIE7 = true;
$linterface 	= new interface_html();
$CurrentPage	= "PageAssessmentReportProgress";

# eClass
$li = new libuser($UserID);
$lo = new libeclass();
$lp = new libportal();
$leclass = new libeclass2007();

//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_statistics.gif' align='absmiddle' /> ".$Lang['eClass']['LfAssessmentReport']['Progress'];
//$TAGS_OBJ[] = array($title,"");
$TAGS_OBJ[] = array($Lang['eClass']['LfAssessmentReport']['Week'],"week.php",1);
$TAGS_OBJ[] = array($Lang['eClass']['LfAssessmentReport']['Year'],"year.php",0);

$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
	
$row = $lp->returnEClassUserIDCourseIDStandard($li->UserEmail);
$classroom = 0;
// Generation of the filter "classroom"
if($_SESSION['schoolsettings']['isAdmin']){ //is Admin
	$courses_select = $lo->getSelectEclass(' class="course_list" ');
	$display = $courses_select;
}else if ($_SESSION['UserType'] == '2' || (($_SESSION['UserType'] =='1') &&($_SESSION['isTeaching']))){ // is student or teacher
	$row = $lp->returnEClassUserIDCourseIDStandard($li->UserEmail);
	$display = "<div class='classroom'><select id='classroom_list' onchange=jsUpdate()>";
	if(sizeof($row)==0){
		$display .= "<option class='no_class_msg' value=-9999 selected='selected'>".$Lang['eClass']['LfAssessmentReport']['NoClass']."</option>"; 	
	}else{
		for($i=0; $i < sizeof($row); $i++){
			$course_id = $row[$i][0];
			$course_code = $row[$i][1];
			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
			if ($i == 0){
				$display .= "<option value='".$course_id."' selected='selected'>".$course_code." - ".$course_name."</option>";
			}else{
				$display .= "<option value='".$course_id."'>".$course_code." - ".$course_name."</option>";
			}
		}
	}
	$display .= "</select></div>";
}
$display .= "<input type='hidden' name='classroom' id='classroom' value='".$classroom."'>";
if((($_SESSION['UserType'] =='1') &&($_SESSION['isTeaching']))){
	$display .= "<div id='student'>";
	$display .= "</div>";
}
$display .= "<form name='form1' id='form1' action='' method='POST'>";
?>
<style>
div.assessment_progress_graph {
	float:left;
	width:50%;
	margin-right: 2%;
}
div.assessment_progress_table{
	float:left;
	width:30%;
}
</style>
<script src='<?=$PATH_WRT_ROOT?>/templates/chartjs/Chart.js'></script>
<script>
$(document).ready(function(){jsUpdate()});
	function jsUpdate(){
		if (<?=$_SESSION['UserType']?> == 1){
			jsUpdateList();
		}else{
			jsUpdateTable();
		}
	}
	
	function jsUpdateList(){
		var e = document.getElementById("classroom_list");
		try{
			var classID = e.options[e.selectedIndex].value;
			$.post(
				"ajax_progress.php",
				{
					task : "UpdateStudentList",
					ClassroomID : classID
				},
				function(responseData)
				{
					var ReturnVal = "";
					ReturnVal += responseData;
					$("#student").html(ReturnVal);
					jsUpdateTable();
				}
			);
		}catch(err){
			var ReturnVal = "<p><?=$Lang['eClass']['LfAssessmentReport']['NoClass']?></p>";
			$("#student").html(ReturnVal);
		}
	}	
	
	function jsUpdateTable(){
		Block_Document();
		$('#canvas').remove();
		$('#assessment_progress_graph_content').append("<canvas id='canvas' height='450' width='650'></canvas>");
		var e = document.getElementById("classroom_list");
		try{
			var classID = e.options[e.selectedIndex].value;
			if ("<?=$_SESSION['isTeaching']?>" != ""){
				var x = document.getElementById("student_list");
				var studentID = x.options[x.selectedIndex].value;
			}
			$.post(
				"ajax_progress.php",
				{
					task : "UpdateTable",
					Mode : "Week",
					ClassroomID : classID,
					StudentID : studentID
				},
				function(responseData)
				{
					var ReturnArr = JSON.parse(responseData);
					var ReturnVal = "";
					ReturnVal += ReturnArr[2]; //return the table
					var time = ReturnArr[0];
					var mark = ReturnArr[1];
					if (mark == null){
						mark = [0,0,0,0,0,0,0,0,0,0,0,0];
					}
					for (var i=0; i < 12; i++){
						if (isNaN(mark[i])){
							mark[i] = 0.0;
						}else{
							mark[i] = parseFloat(mark[i]);
						}
					}
					var lineChartData = {
						labels : [time[0],time[1],time[2],time[3],time[4],time[5],time[6],time[7],time[8],time[9],time[10],time[11]],
						datasets : [
						{
							label: "Marks",
							fillColor : "rgba(151,187,205,0.2)",
							strokeColor : "rgba(151,187,205,1)",
							pointColor : "rgba(151,187,205,1)",
							pointStrokeColor : "#fff",
							pointHighlightFill : "#fff",
							pointHighlightStroke : "rgba(151,187,205,1)",
							data : [mark[0],mark[1],mark[2],mark[3],mark[4],mark[5],mark[6],mark[7],mark[8],mark[9],mark[10],mark[11]]
						}
						]
					}
					var ctx = document.getElementById("canvas").getContext("2d");
					var markLine = new Chart(ctx).Line(lineChartData, {
						responsive: true,
						bezierCurve : false
					});
					
					$("div.assessment_progress_table").html(ReturnVal);
				}
			)		
		}catch(err){
			var ReturnVal = "<p><?=$Lang['eClass']['LfAssessmentReport']['NoClass']?></p>";
			$("#result_table").html(ReturnVal);
		}
		UnBlock_Document();
	}
</script>
<?=$display?>
<br>
<div id="result_table">
	<div class='assessment_progress_graph'>
		<div id="assessment_progress_graph_content">
			<canvas id='canvas' height='450' width='650'></canvas>
		</div>
	</div>
	<div class='assessment_progress_table'>

	</div>				
</div>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>