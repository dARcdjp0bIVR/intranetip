<?php
#Rank
# editing by Paul

####################################
         // Change Log //
/*
 * 	
 * 	2015-10-23 (Paul) IP.2.5.7.1.1
 * 		- Create the file for LangFlow centre to for weekly rank checking 
 * 
 */         
####################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
eclass_opendb();
$home_header_no_EmulateIE7 = true;
$linterface 	= new interface_html();
$CurrentPage	= "PageAssessmentReportRank";

# eClass
$li = new libuser($UserID);
$lo = new libeclass();
$lp = new libportal();
$leclass = new libeclass2007();

// Top selection tags
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_statistics.gif' align='absmiddle' /> ".$Lang['eClass']['LfAssessmentReport']['Ranking'];
//$TAGS_OBJ[] = array($title,"");
$TAGS_OBJ[] = array($Lang['eClass']['LfAssessmentReport']['Weekly'],"weekly.php",1);
$TAGS_OBJ[] = array($Lang['eClass']['LfAssessmentReport']['Accumulated'],"accumulated.php",0);

// left menu layout
$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Main content

// Generation of the filter "classroom"
if($_SESSION['schoolsettings']['isAdmin']){ //is Admin
	$courses_select = $lo->getSelectEclass(' class="course_list" ');
	$display = $courses_select;
}else if ($_SESSION['UserType'] == '2' || (($_SESSION['UserType'] =='1') &&($_SESSION['isTeaching']))){ // is student or teacher
	$row = $lp->returnEClassUserIDCourseIDStandard($li->UserEmail);
	$display = "<div class='classroom'><select id='classroom_list' onchange=jsUpdateRank()>";
	if(sizeof($row)==0){
		$display .= "<option class='no_class_msg value=-9999 selected='selected'>".$Lang['eClass']['LfAssessmentReport']['NoClass']."</option>"; 	
	}else{
		for($i=0; $i < sizeof($row); $i++){
			$course_id = $row[$i][0];
			$course_code = $row[$i][1];
			$course_name = intranet_wordwrap($row[$i][2],30,"\n",1);
			if ($i == 0){
				$display .= "<option value='".$course_id."' selected='selected'>".$course_code." - ".$course_name."</option>";
			}else{
				$display .= "<option value='".$course_id."'>".$course_code." - ".$course_name."</option>";
			}
		}
	}
	$display .= "</select></div>";
}

// Generation of the filter "Week"
if($WeekStartTimeStamp == "") { 
	$curr_date = date("Y-m-d");
	$curr_weekday = date("w");
} else {
	$curr_date = date("Y-m-d", $WeekStartTimeStamp);
	$curr_weekday = date("w", $WeekStartTimeStamp);
}

$curr_year = substr($curr_date,0,4);
$curr_month = substr($curr_date,5,2);
$curr_day = substr($curr_date,8,2);
		
$StartOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday+1, $curr_year));
$EndOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday+7, $curr_year));
		
$StartYearOfTheWeek = substr($StartOfTheWeek,0,4);
$StartMonthOfTheWeek = substr($StartOfTheWeek,5,2);
$StartDayOfTheWeek = substr($StartOfTheWeek,8,2);
		
$EndYearOfTheWeek = substr($EndOfTheWeek,0,4);
$EndMonthOfTheWeek = substr($EndOfTheWeek,5,2);
$EndDayOfTheWeek = substr($EndOfTheWeek,8,2);
$WeekStartTimeStamp = strtotime($StartOfTheWeek);
		
$prev_week_link = "<a href='#' onClick='PrevWeek()'><img height='21' border='0' align='absmiddle' width='21' src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_prev_off.gif'></a>";
$next_week_link = "<a href='#' onClick='NextWeek()'><img height='21' border='0' align='absmiddle' width='21' src='".$image_path."/".$LAYOUT_SKIN."/icalendar/icon_next_off.gif'></a>";
$week_by_calendar = "<input type='hidden' name='WeekStartDate' id='WeekStartDate' value='' onChange='jsChangeWeek();'>";
		
$DisplayStartOfTheWeek = $StartDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$StartMonthOfTheWeek].",".$StartYearOfTheWeek;
$DisplayEndOfTheWeek = $EndDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$EndMonthOfTheWeek].",".$EndYearOfTheWeek;
$WeekStartTimeStamp = strtotime($StartOfTheWeek);
		
$week_selection = $prev_week_link."<span id='DIV_WeeklyDisplay'>".$DisplayStartOfTheWeek." - ".$DisplayEndOfTheWeek."</span>".$next_week_link.$week_by_calendar;
$tab_content .= "<table border='0' width='100%' cellpadding='0' cellspacing='0'>";
$tab_content .= "<tr>";
$tab_content .= "<td width='30%'></td>";
$tab_content .= "<td width='40%' align='center'><span class='icalendar_title'>".$week_selection."</span></td>";
$tab_content .= "<td width='30%' align='right' valign='bottom'></td>";
$tab_content .= "</tr>";
$tab_content .= "</table>";
$display .= $tab_content;

$display .= "<form name='form1' id='form1' action='' method='POST'>";
?>

<script>
$(document).ready(function(){jsUpdateRank()});
function jsChangeWeek()
	{
		$.post(
			"ajax_rank.php",
			{
				task : "ChangeDisplayWeekByCal",
				WeekStartDate : $("#WeekStartDate").val(),
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsUpdateRank();
			}
		);
	};
	
	function NextWeek()
	{
		$.post(
			"ajax_rank.php",
			{
				task : "ChangeDisplayWeek",
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				ChangeValue : 7,
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsUpdateRank();
			}
		);
	};
	
	function PrevWeek()
	{
		$.post(
			"ajax_rank.php",
			{
				task : "ChangeDisplayWeek",
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				ChangeValue : -7,
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsUpdateRank();
			}
		);
	};
	
	function jsUpdateRank(){
		Block_Document();
		var e = document.getElementById("classroom_list");
		try{
			var classID = e.options[e.selectedIndex].value;
			$.post(
				"ajax_rank.php",
				{
					task : "UpdateRank",
					WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
					Mode : "Week",
					ClassroomID : classID
				},
				function(responseData)
				{
					var ReturnVal = "";
					ReturnVal += responseData;
					$("#ranking").html(ReturnVal);
					UnBlock_Document();
				}
			);
		}catch(err){
			var ReturnVal = "<p><?=$Lang['eClass']['LfAssessmentReport']['NoClass']?></p>";
			$("#ranking").html(ReturnVal);
		}
		
	}
</script>

<?=$display?>
<div id="ranking" class="ranking_table_list" id="ranking_table_list">
</div>
<input type='hidden' name='WeekStartTimeStamp' id='WeekStartTimeStamp' value=<?=$WeekStartTimeStamp?>>
</form>
<style>
.ranking_table_list{
	width: 100%;
}
</style>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>