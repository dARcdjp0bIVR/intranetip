<?php
# editing by
/**
 * Change Log:
 * 2018-02-14 (Pun) [135488] [ip.2.5.9.3.1]
 *      - Added access right checking
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libeclass2007a.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");

intranet_auth();
eclass_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-eClass"]) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface 	= new interface_html();
$CurrentPage	= "PageAssessmentReport";

# eClass
$lo = new libeclass();
$lgroup = new libgrouping();
$leclass = new libeclass2007();
$eclass_quota = $lo->status();
$la = new libaccess($UserID);

$show= isset($show) ? $show : 'all';
$id = intval($id);

$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> $i_eClass_Admin_AssessmentReport";
$TAGS_OBJ[] = array($title,"");
//$TAGS_OBJ[] = array('Markbooks',"/home/eLearning/eclass/assessment_report/markbook.php",0);
//$TAGS_OBJ[] = array($i_eClass_Admin_AssessmentReport,"/home/eLearning/eclass/assessment_report/index.php",1);

$MODULE_OBJ = $leclass->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$fieldName = $intranet_session_language=="en"?"EN_DES":"CH_DES";
$sql = "select RecordID, $fieldName as SubjectName from ASSESSMENT_SUBJECT where RecordStatus = 1 AND
(CMP_CODEID IS NULL OR CMP_CODEID = '')";

$classTitle = "c.ClassTitle".strtoupper($intranet_session_language);
	
$sql = "select c.SubjectGroupID as GroupID, $classTitle as Title
        from {$intranet_db}.SUBJECT_TERM as t inner join 
        {$intranet_db}.SUBJECT_TERM_CLASS as c on
                t.SubjectGroupID = c.SubjectGroupID 
        where
                 t.YearTermID in (
                        select
                                ayt.YearTermID
                        from
                                {$intranet_db}.ACADEMIC_YEAR as ay
                                inner join
                                {$intranet_db}.ACADEMIC_YEAR_TERM as ayt
                                on ay.AcademicYearID = ayt.AcademicYearID
                        where NOW() between ayt.TermStart and ayt.TermEnd
                )
        order by Title";	

$subject_groups = $lo->returnArray($sql);
        
//print_r($libdb->returnArray('select * from SUBJECT_TERM limit 1'));
$class_groups=$lgroup->returnCategoryGroups(3);


?>
<script>
    
    $(document).ready(
    
        function(){
                        
            var selectedClass={
                type: '<?=$type?>',
                id: '<?=($id==0)? '' :$id?>',
                show: '<?=$show?>'
            };
            
            $('#type').val(selectedClass.type);
            if (selectedClass.type!=''){
                $('#'+selectedClass.type).show().val(selectedClass.id);
                $('#show').show().val(selectedClass.show);
                if (selectedClass.id!=''){
                    $('#report_content').html('<h4><?=$i_general_loading?></h4>').load('getReportTable.php',selectedClass,function(){
                            $(this).hide().fadeIn('slow');
                    });
                }
            }
                
            $('#type').change(function(){
                var val=$('#type').val();
                if (val=='class'){
                    $('#class').show();
                    $('#group').hide();
                    $('#show').show();
                }else if (val=='group'){
                    $('#group').show();
                    $('#class').hide();
                    $('#show').show();
                }
                
            });
            
            $('#class, #group, #show').change(function(){
                var id=$('#'+$('#type').val()).val();
                if (id!=''){
                    location.href='./?type='+$('#type').val()+'&id='+id+'&show='+$('#show').val();
                }
            });
            
            
            
        }
        
    );


</script>

<select name='type' id='type'>    
    <option value=''>--<?=$Lang['Btn']['Select']?>--</option>
    <option value='class'><?=$Lang['eAttendance']['ShowAllClasses']?></option>
    <option value='group'><?=$Lang['SysMgr']['Homework']['AllSubjectGroups']?></option>
</select>
<select id='group' style="display:none;">
    <option value=''>--<?=$Lang['Btn']['Select']?>--</option>
    <? foreach ($subject_groups as $subject_group) : ?>
        <option value="<?=$subject_group['GroupID']?>"><?=$subject_group["Title"]?></option>
    <? endforeach; ?>
</select>
<select id='class' style="display:none;">
    <option value=''>--<?=$Lang['Btn']['Select']?>--</option>
    <? foreach ($class_groups as $class_group) : ?>
        <option value="<?=$class_group['GroupID']?>"><?=$class_group["Title"]?></option>
    <? endforeach; ?>
</select>
<select name='show' id='show' style="display:none;">
    <option value='all'><?=$i_eClass_Admin_AllAssessments?></option>
    <option value='passed'><?=$i_eClass_Admin_OnlyAssessmentsPassedDeadline?></option>
</select>
<div id='report_content'>
</div>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>