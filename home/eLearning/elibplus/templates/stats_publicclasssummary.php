<?// 20150210 - Ryan - Fix Class Number sorting  and adjust col width  
if (!isset($publicclasssummary)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(function(){
	
	$('.record_tab li a').click(function(){
		var target=$(this).attr('href');
		$(target).siblings().fadeOut();
		$.fancybox.showLoading();
		$.get('ajax.php?action=get'+target.replace('#','')+'&year_class_id=<?=$year_class_id?>&student_id=<?=$student_id?>&class_name=<?=$class_name?>',{offset: 0}, function(data){
		    $(target).fadeIn().find('table').append(data);
		    $('.record_detail').scrollTop(0);
		    $.fancybox.hideLoading();
		});
		;
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		return false;
	});
	
	$('.record_tab li.current_tab a').click();
	$(".record_detail").attr({"class": "record_detail record_pcs"});
	$("#FromMonth").val($("#FromMonth_Search").val());
	$("#FromYear").val($("#FromYear_Search").val());
	$("#ToMonth").val($("#ToMonth_Search").val());
	$("#ToYear").val($("#ToYear_Search").val());
		
});	

function jsExport()
{
	var StudentID = "";
	if (typeof(student_id)!="undefined")
	{
		StudentID = "&student_id="+student_id;
	}
	self.location = "export.php?page=stats&ClassName_Search="+$("#ClassName_Search").val()+"&FromMonth_Search="+$("#FromMonth_Search").val()+"&FromYear_Search="+$("#FromYear_Search").val()+"&ToMonth_Search="+$("#ToMonth_Search").val()+"&ToYear_Search="+$("#ToYear_Search").val()+StudentID;	
}
</script>

<div class="record_tab" style='display:none'>
	<ul>
	<li class="current_tab"><a href="#PublicClassSummaryRecord"><span><?=$eLib["html"]["my_reading_history"] ?></span></a></li>
    </ul>
</div>

<div class="content_top_tool"  style="float: right;">
		<div class="Conntent_tool"><a href="javascript:jsExport()" class="export"><?=$Lang['Btn']['Export']?></a></div>  
	</div>
	
<div><?=$filter_html?></div>

   

<div class="record_detail" style="height:620px">
	   <div id="PublicClassSummaryRecord" style='display:'>
	      <div class="table_board" >		      
			<table class="common_table_list">
				<col nowrap="nowrap" width="5%"/>
				<col nowrap="nowrap" width="7%"/>
				<col nowrap="nowrap" width="12%"/>
				<col nowrap="nowrap" width="15%"/>
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="15%"/>
				<tr>
				  <th class="num_check">#</th>
				  <th><a href="#" name="sort_c" class="sort_column"><?=$eLib["html"]["class"]?></th>
				  <th><a href="#" name="sort_nos" class="sort_column"><?=$eLib["html"]["num_of_students"]?></th>
				  <th><a href="#" name="sort_tnobr" class="sort_column"><?=$eLib["html"]['Total']."(".$eLib["html"]["num_of_book_hit"].")"?></a></th>
				  <th><a href="#" name="sort_anobr" class="sort_column"><?=$eLib["html"]["Average"]."(".$eLib["html"]["num_of_book_hit"].")"?></a></th>
				  <th><a href="#" name="sort_tnor" class="sort_column"><?=$eLib["html"]['Total']."(".$eLib["html"]["num_of_review"].")"?></a></th>
				  <th><a href="#" name="sort_anor" class="sort_column"><?=$eLib["html"]["Average"]."(".$eLib["html"]["num_of_review"].")"?></a></th>
				</tr>
			</table>			
		  </div>
	   </div>   
</div>       

<input type="hidden" id="FromMonth"  value="">
<input type="hidden" id="FromYear"  value="">
<input type="hidden" id="ToMonth"  value="">
<input type="hidden" id="ToYear"  value="">

 <div id="record_col_template" style='display:none'>
	      <div class="table_board" >	
	   		<table class="common_table_list" style="display:none" id="pulic_class_summary_temp">
				<col nowrap="nowrap" width="5%"/>
				<col nowrap="nowrap" width="7%"/>
				<col nowrap="nowrap" width="12%"/>
				<col nowrap="nowrap" width="15%"/>
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="15%"/>
				<tr>
				  <th class="num_check">#</th>
				  <th><a href="#" name="sort_c" class="sort_column"><?=$eLib["html"]["class"]?></th>
				  <th><a href="#" name="sort_nos" class="sort_column"><?=$eLib["html"]["num_of_students"]?></th>
				  <th><a href="#" name="sort_tnobr" class="sort_column"><?=$eLib["html"]['Total']."(".$eLib["html"]["num_of_book_hit"].")"?></a></th>
				  <th><a href="#" name="sort_anobr" class="sort_column"><?=$eLib["html"]["Average"]."(".$eLib["html"]["num_of_book_hit"].")"?></a></th>
				  <th><a href="#" name="sort_tnor" class="sort_column"><?=$eLib["html"]['Total']."(".$eLib["html"]["num_of_review"].")"?></a></th>
				  <th><a href="#" name="sort_anor" class="sort_column"><?=$eLib["html"]["Average"]."(".$eLib["html"]["num_of_review"].")"?></a></th>
				</tr>
			</table>
			<table class="common_table_list" style="display:none" id="pulic_student_summary_temp">
				<col nowrap="nowrap" width="5%"/>
				<col nowrap="nowrap" width="15%"/>
				<col nowrap="nowrap" width="12%"/>
				<col nowrap="nowrap" width="7%"/>
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="15%"/>
				<tr>
				  <th class="num_check">#</th>
				  <th><a href="#" name="sort_sn" class="sort_column"><?=$eLib["html"]["student_name"]?></a></th>
				  <th><?=$eLib["html"]["class"]?></th><!--<a href="#" name="sort_c" class="sort_column"></a>-->
				  <th><a href="#" name="sort_cn" class="sort_column"><?=$eLib["html"]["class_number"]?></a></th>
				  <th><a href="#" name="sort_nobr" class="sort_column"><?=$eLib["html"]["num_of_book_read"]?></a></th>
				  <th><a href="#" name="sort_hit" class="sort_column"><?=$eLib["html"]["num_of_book_hit"]?></a></th>
				  <th><a href="#" name="sort_nor" class="sort_column"><?=$eLib["html"]["num_of_review"]?></a></th>
				</tr>
			</table>
			<table class="common_table_list" style="display:none" id="pulic_student_particular_summary_temp">
				<col nowrap="nowrap" width="5%"/>
				<col nowrap="nowrap" width="35%"/>
				<col nowrap="nowrap" width="25%"/>
				<col nowrap="nowrap" width="15%"/>				
				<col nowrap="nowrap" width="10%"/>	
				<tr>
				  <th class="num_check">#</th>
				  <th><a href="#" name="sort_bt" class="sort_column"><?=$eLib["html"]["book_title"]?></a></th>
				  <th><a href="#" name="sort_a" class="sort_column"><?=$eLib["html"]["author"]?></a></th>
				  <th><a href="#" name="sort_p" class="sort_column"><?=$eLib["html"]["Progress"]?></a></th>
				  <th><a href="#" name="sort_rt" class="sort_column"><?=$eLib["html"]["ReadTimes"]?></a></th>
				  <th><a href="#" name="sort_lr" class="sort_column"><?=$eLib["html"]["last_read"]?></a></th>
				</tr>
			</table>
		 </div>
	   </div>                       
<p class="spacer"></p>
                
              