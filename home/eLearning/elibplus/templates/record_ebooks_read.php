<?php
/* Change Log:
 * 
 * 	Date: 2015-12-16 (Paul) [ip.2.5.7.1.1]
 * 			- modified the UI for expired book
 * 
 * 
*/
?>
<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if (!$ebook_data['reading']&&$offset==0): ?>
    <tr><td colspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
<? else: ?>
<tr style="display:none;">
    <script>
    
	$(".record_detail").attr({"class": "record_detail record_read"});
	
	<? if (sizeof($ebook_data['reading'])>=$amount): ?>
	$('.record_read').off('scroll').on('scroll',function(e){
	    
	    if ($(this).prop('scrollHeight')-$(this).scrollTop()-150<=$(this).height()){
		
		$.fancybox.showLoading();
		$('.record_detail').off('scroll');
		$.get('ajax.php?action=geteBookReadRecord', {offset:'<?=$offset+$amount?>'}, function(data){
		    $.fancybox.hideLoading();
		    $('.record_read table').append(data);		     
		});
		
	    }
	});
	<? endif; ?>
	
    </script>
</tr>
<? foreach ($ebook_data['reading'] as $num=>$progress) : ?>
    <tr>
	<td><?=$offset+$num+1?></td>
	<td><div class="ebook_title">
	<? if($progress['readable']): ?>			
		<a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$progress['url']?>" ></a>
		<a rel="reading_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$progress['id']?>" class="book_title book_detail"><?=$progress['title']?></a>
	<? else: ?>
		<a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon_read_now" href="#" ></a>
		<a rel="reading_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$progress['id']?>" class="book_title book_detail off_shelf_list"><?=$progress['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a>
	<? endif; ?>
	</div></td>
	<? if($progress['readable']): ?>	
	<td><?=$progress['author']?></td>
	<? else: ?>
	<td class="off_shelf_list"><?=$progress['author']?></td>
	<? endif; ?>
	<td>
		<? if (!empty($progress['percentage'])) :?>
		<div class="book_progress"><span <?=($progress['percentage']==100)? "class='done'" : 'style="left:'.($progress['percentage']-100).'px;"'?>></span><em><?=$progress['percentage']?>%</em></div>
		<? else: ?>
		--
		<? endif; ?>
	</td>
	<? if($progress['readable']): ?>
	<td><?=$progress['last_accessed']?></td>
	<? else: ?>
	<td class="off_shelf_list"><?=$progress['last_accessed']?></td>
	<? endif; ?>
    </tr>
    <? endforeach; ?>
<? endif; ?>