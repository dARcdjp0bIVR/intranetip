<?php
/* Change Log:
 * 
 * 	Date: 2015-12-16 (Paul) [ip.2.5.7.1.1]
 * 			- modified the UI for expired book
 * 
 * 
*/
?>
<? if (!isset($books_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? 
    if(!$books_per_row) $books_per_row=7;
    $books_rows=array_chunk($books_data,$books_per_row);
?>
<script type="text/javascript">
var type='cover';

$(document).ready(function() {
  
    $('.delete_dim').click(function(){
      
	if(confirm('<?=$eLib_plus["html"]["removefavourite"]?>?')){
	    
	    $.fancybox.showLoading();
	    $('.record_content_detail').slideUp();
	    $.post($(this).attr('href'), function(data){
	      
		  $.get('ajax.php',{action: 'getFavouriteRecord', type: type}, function(data){
		      
		      $('.record_content_detail').html(data).slideDown();
		      $.fancybox.hideLoading();
		  });
		  
	    });
	}
	return false;
    });
    
    $('.toggle_tool li a').click(function(){
    
	var target=$(this).attr('href');
	type=target.replace('#favourite_','');
      
	$(target).siblings().hide();
	$(target).fadeIn();
	
	$(this).parent().addClass('selected').siblings().removeClass('selected');
	
	return false;
    });
    
    
});
</script>
<div class="fav_content">
        
    <div class="book_cat_nav">
      
	
      <span class="book_count"><?=sizeof($books_data).' '.$eLib["html"]["books"]?></span>            </div>
      <div class="toggle_tool">
	  <ul>
	      <li <?=($type=='list')? 'class="selected"': ""?>><a href="#favourite_list" class="view_list" title="<?=$eLib_plus["html"]["booklist"]?>"></a></li>
	      <li <?=($type!='list')? 'class="selected"': ""?>><a href="#favourite_cover" class="view_cover" title="<?=$eLib_plus["html"]["bookcover"]?>"></a></li>
	      <li><span><?=$eLib_plus["html"]["viewby"]?>:</span> </li>
	  </ul>
      </div>
      <p class="spacer"></p>
      
      <div>
	 <div class="book_fav_list" id="favourite_cover" <?=($type!='list')? '': 'style="display:none;"'?>>

	<? foreach ($books_rows as $books_row) : ?>
	<div class="bookshelf"><div class="bookshelf_right"><div class="bookshelf_bg"> 
	  
	  <div class="bookshelf_content">
	  
	
	    <ul class="book_list book_list_small">
	       	      
	       <?=elibrary_plus::getBookListView($books_row) ?>
	      
	    </ul>
	     
	    <div class="tool_fav_remove">
	    <? foreach ($books_row as $book): ?>
		<a href="ajax.php?action=handleFavourite&book_id=<?=$book['id']?>" class="delete_dim" title="Remove"><span></span></a>
	    <? endforeach; ?>
	    </div>
	    	  
	   
	  </div>
		       
	</div></div></div>
	<p class="spacer"></p>
	<? endforeach; ?>
      </div>      
      	<div class="table_board" id="favourite_list" <?=($type=='list')? '': 'style="display:none;"'?>>
	
	    <table class="common_table_list">
			    
		  <col nowrap="nowrap"/>
		  <thead>
		      <tr>
			  <th class="num_check">#</th>
			  <th><?=$eLib["html"]["book_title"]?></th>
			  <th><?=$eLib["html"]["author"]?></th>
			  <th><?=$eLib["html"]["publisher"]?></th>
			  <th>&nbsp;</th>
		      </tr>
		  </thead>
	
		  <tbody>
		    <? if (!$books_data): ?>
			<tr><td rowspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
		    <? else: ?>
			<? foreach($books_data as $num=>$book) :?>
			  <tr>
				<td><?=$num+1?></td>
				<td>
				  <div class="ebook_title">
				    <? if (!empty($book['url']) && !($book['type'] == 'expired')) : ?>
				    <a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$book['url']?>" ></a>
				    <? elseif(($book['type'] == 'expired')): ?>
				    <a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon_read_now" href="#" ></a>
				    <? endif; ?>
				    <? if (!($book['type'] == 'expired')) : ?>
				    <a rel="record_favourite_list" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_detail book_cover"><?=$book['title']?></a>
				    <? else: ?>
				    <a rel="record_favourite_list" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_detail book_cover off_shelf_list"><?=$book['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a>
				    <? endif; ?>
				  </div>
				</td>
				<? if (!($book['type'] == 'expired')) : ?>
				<td><?=$book['author']?></td>
				<td><?=$book['publisher']?></td>
				<? else: ?>
				<td class="off_shelf_list"><?=$book['author']?></td>
				<td class="off_shelf_list"><?=$book['publisher']?></td>
				<? endif; ?>
				<td>
				    <div class="table_row_tool">
					<a class="delete_dim" title="Remove" href="ajax.php?action=handleFavourite&book_id=<?=$book['id']?>"><span></span></a>
				    </div>
				</td>
			  </tr>
			<? endforeach; ?>
		    <? endif; ?>
		  <tbody>			
	      </table>
	    
	  </div>		     
	</div>
	<p class="spacer"></p>
    </div>