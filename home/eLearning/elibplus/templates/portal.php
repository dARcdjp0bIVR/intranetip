<?php
/*
 * 	Modifying:	
 * 	Log
 * 
 * 	Date:	2015-11-30 [Cameron]
 * 			- add afterShow() event to $('.book_detail').fancybox to adjust the height of book description
 * 
 * 	Date:	2015-10-19 [Cameron]
 * 			- hide "Most Hit Book" if there's no ebook subscription of the client  
 * 
 * 	Date:	2015-03-11 [Cameron] Add flag $sys_custom['eLibraryPlus']['HideOpeningHours'] to control showing library opening hours tab or not
 * 								 (九龍城浸信會)
 */
?>
<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? // @charset "utf-8"; ?>

<script type="text/javascript">
var range='';
var type='recommend';
$(document).ready(function() {
		
		
		 /*  Simple image gallery. Uses default settings
			 */
		$('.fancybox').fancybox( {
		 'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 800,
         'height'            : 600,
		 'padding'			 : 20,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
		
		$('.fancybox_small').fancybox( {
		'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 600,
         'height'            : 500,
		 'padding'			 : 0,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
		
		$('.fancybox_magazine').fancybox( {
		'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 550,
         'height'            : 320,
		 'padding'			 : 0,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
		$('.fancybox_magazine_learnthenews').fancybox( {
		'fitToView'    	 : false,	
		 'autoDimensions'    : false,
		 'autoScale'         : false,
		 'autoSize'          : false,
         'width'             : 820,
         'height'            : 500,
		 'padding'			 : 0,

         'transitionIn'      : 'elastic',
         'transitionOut'     : 'elastic',
         'type'              : 'iframe'    }
		
		);
	
       $('.book_detail').fancybox({
		autoDimensions : false,
		autoScale : false,
		autoSize : false,
		width : 800,
		height : 600,
		type : 'ajax',
		padding: 0,
		nextEffect: 'fade',
		prevEffect: 'fade',
		beforeClose: function(){
		    $('.portal_bookshelf_recommend .bookshelf_title_name li.current a').trigger('click.noanim');
		    $('.portal_mostrate .bookshelf_title_filter li.current a').trigger('click.noanim');
		    $('.portal_mosthit .bookshelf_title_filter li.current a').trigger('click.noanim');
		    $('.ranking_submit').trigger('click.noanim');
		
		},
		afterShow: function() {
			var isScroll = false;
			var baseline = 140;
	        if ($('#book_description').height() > baseline) {
	        	if ($('#book_info_summary').height() <= baseline) {
	        		$('#book_description').css('height',baseline);
	        		isScroll = true;
	        	}
	        	else {
	        		if ($('#book_info_summary').height() < $('#book_description').height()) {
	        			$('#book_description').css('height',$('#book_info_summary').height()-10);
	        			isScroll = true;
	        		}
	        	}
	        }
	        
	        if (isScroll == true) {
				$('#book_description').css('width','260px');
				$('#book_description').css('overflow-y','scroll');        	
				$('#book_description').css('overflow-x','hidden');
				var h = $('#book_description').height() - 15;
				$('.book_desc span').css('background-position','215px '+ h + 'px');
				
				$('#book_description').scroll(function (event) {
				    var scroll = $('#book_description').scrollTop();
				    var height = $('#book_description').height() - 15 + scroll;  
				    $('.book_desc span').css('background-position','215px '+ height + 'px');
				});
	        }
	        else {
	        	$('#book_description').css('width','240px');
	        	$('#book_description').css('overflow-y','hidden');
	        	$('#book_description').css('overflow-x','hidden');
	        }
    	}    	
    
       });     
      
       $('.portal_bookshelf_recommend .bookshelf_title_name li a').on('click.noanim',function(e){
              
		$(this).parent().addClass('current').siblings().removeClass('current');
		var type = $(this).attr('href').replace('#','');
		$('.portal_bookshelf_recommend .bookshelf_title_filter li.btn_more a').attr('href','./?page='+type);
		
		$.fancybox.showLoading();
		if (typeof(e.namespace)=='undefined') $('.portal_bookshelf_recommend .book_list').animate({'margin-top': -200, opacity: 0});
		
		
		$.get('ajax.php',{action: 'getPortalRecommend', type: type},function(data){
		       $.fancybox.hideLoading();
		       $('.portal_bookshelf_recommend .book_list').html(data).animate({'margin-top': 0, opacity: 1});
		});
	      
		return false;
       });
       
       $('.portal_mostrate .bookshelf_title_filter li:not(.btn_more) a').on('click.noanim',function(e){
		  
		$(this).parent().addClass('current').siblings().removeClass('current');
		var range = $(this).attr('href').replace('#','');
		$('.portal_mostrate .bookshelf_title_filter li.btn_more a').attr('href','./?page=ranking&range='+range+'#BestRateBooks');
		
		$.fancybox.showLoading();
		if (typeof(e.namespace)=='undefined') $('.portal_mostrate .book_list').animate({'margin-top': -200, opacity: 0});
		
	    
		$.get('ajax.php',{action: 'getPortalBestRate', range: range},function(data){
		       $.fancybox.hideLoading();
		       $('.portal_mostrate .book_list').html(data).animate({'margin-top': 0, opacity: 1});
		});
		
		
		return false;
       });
       
       $('.portal_mosthit .bookshelf_title_filter li:not(.btn_more) a, .portal_mosthit .bookshelf_title_name li a').on('click.noanim',function(e){
		  
		$(this).parent().addClass('current').siblings().removeClass('current');
		var range = $('.portal_mosthit .bookshelf_title_filter li.current a').attr('href').replace('#','');
		var type = $('.portal_mosthit .bookshelf_title_name li.current a').attr('href').replace('#','');
		$('.portal_mosthit .bookshelf_title_filter li.btn_more a').attr('href','./?page=ranking&range='+range+'#'+type);
		
		
		$.fancybox.showLoading();
		if (typeof(e.namespace)=='undefined') $('.portal_mosthit .book_list').animate({'margin-top': -200, opacity: 0});
		
		$.get('ajax.php',{action: 'getPortalMostHitLoan', range: range, type: type},function(data){
		       $.fancybox.hideLoading();
		       $('.portal_mosthit .book_list').html(data).animate({'margin-top': 0, opacity: 1});
		});
		
		
		
		return false;
       });
       
       $('.ranking_range, .ranking_type').on('change.noanim',function(e){
	
	    $.fancybox.showLoading();
	    var range = $('.ranking_range').val();
	    var type = $('.ranking_type').val();
	    $('.ranking_bottom .btn_more').attr('href','./?page=ranking&range='+range+'#'+type);
	    
	    if (typeof(e.namespace)=='undefined') $('.ranking_container').slideUp();
	    $.get('ajax.php?action=getPortalRank', {range: range, type: type},function(data){
		   $.fancybox.hideLoading();
		   $('.ranking_container').html(data).slideDown();
	    });
	    return false;
       });
       
       $('.p_tab_news').on('click.noanim',function(e){
	
	    $.fancybox.showLoading();
	    $('#portal_news').fadeIn().siblings().hide();
	    $('.p_tab_news').parent().addClass('current_tab').siblings().removeClass('current_tab');


	    $.get('ajax.php?action=getPortalNotices&type=news', {offset:0}, function(data){
		
		$.fancybox.hideLoading();
		$('#portal_news .portal_news_content_detail ul').html(data);
		$('#portal_news .portal_news_content_detail').scrollTop(0);
		
	    });
	    return false;
		
       });
       $('.p_tab_rules').on('click.noanim',function(e){
	
	    $.fancybox.showLoading();
	    $('#portal_rules').fadeIn().siblings().hide();
	    $('.p_tab_rules').parent().addClass('current_tab').siblings().removeClass('current_tab');
	    
	    $.get('ajax.php?action=getPortalNotices&type=rules', function(data){
		
		$.fancybox.hideLoading();
		$('#portal_rules .portal_news_content_detail').html(data);
		$('#portal_rules_new .news_content').val($('#portal_rules .portal_news_content_detail .rules_content').val());
		
	    });

	    return false;
		
       });
       $('.p_tab_open').on('click.noanim',function(e){
	
	    $($(this).attr('href')).fadeIn().siblings().hide();
	    $(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
	    return false;
	   
       });
       
       $('#portal_news .tool_manage').click(function(){
	
	    $.fancybox.open('ajax.php?action=getPortalNoticeForm&type=news',{
		type: 'ajax',
		margin: 0,
		padding: 0
	    });
	    return false;
       });
       
       $('#portal_rules .tool_edit').click(function(){
	   $.fancybox.open('ajax.php?action=getPortalNoticeForm&type=rules',{
		type: 'ajax',
		margin: 0,
		padding: 0
	    });
	   return false;
       });

       
       
       $(document).click(function(e){
	
	    if ($(e.target).closest('.portal_news_board').length==0) $('#portal_news_new, #portal_rules_new').fadeOut();
	});
    
       
       $('.portal_bookshelf_recommend .bookshelf_title_name li.current a').trigger('click.noanim');
       $('.portal_mostrate .bookshelf_title_filter li.current a').trigger('click.noanim');
       $('.portal_mosthit .bookshelf_title_filter li.current a').trigger('click.noanim');
       $('.ranking_range').trigger('change.noanim');
       $('.p_tab_news, .p_tab_rules').trigger('click.noanim');
       
       
});
	function redirect_to_emag_site (){
		$("#redirect_to_emag_site").click();
	}
</script>
<div id="portal_board" class="elib_content_board">
    
    <div class="elib_content_board_top"><div class="elib_content_board_top_right"><div class="elib_content_board_top_bg">
    	<p class="spacer"></p>
    </div></div></div>
    
    <div class="elib_content_board_content"><div class="elib_content_board_content_right"><div class="elib_content_board_content_bg">
        <div class="portal_board_content"> 
            <!--left-->
            <div class="portal_board_left">              
          
           
               <div class="portal_news">
               	    <div class="portal_news_title"><?=$eLib_plus["html"]["announcement"]?></div>
                    <div class="portal_news_board">
                        <div class="portal_news_tab">
			    <ul>
				<li style="display:none;"><a class="p_tab_staff" href="#portal_staff"><span><?=$eLib_plus["html"]["staff"] ?></span></a></li>                    
				<li><a class="p_tab_rules" href="#portal_rules"><span><?=$eLib_plus["html"]["rules"] ?></span></a></li>
				
				<?if(!$plugin['eLib_Lite'] && !$sys_custom['eLibraryPlus']['HideOpeningHours']){?><li><a class="p_tab_open" href="#portal_open"><span><?=$eLib_plus["html"]["openinghours"]?></span></a></li><?}?>
				
				<li class="current_tab"><a class="p_tab_news" href="#portal_news"><span><?=$eLib_plus["html"]["news"]?></span></a></li>
			    </ul>
                        </div>	
                        <div id="portal_news_container">
			    <div class="portal_news_content portal_news_content_open" id="portal_open" style="display:none;">
				<div class="portal_news_content_title"><?=$eLib_plus["html"]["openinghours"]?><div class="news_tool">
				<? if ($permission['admin']): ?>
				    <a href="/home/library_sys/admin/settings/open_time.php" target="_blank" class="tool_manage"><?=$eLib_plus["html"]["manage"]?></a>
				<? endif; ?>
				</div></div>
			    <? if ($portal_data['open_days_replace_message']): ?>
				<?=$portal_data['open_days_replace_message']?>
			    <? else: ?>
			    
				
				
				<div class="portal_news_content_detail">
			       
				    <p class="spacer"></p>
				    
				    <ul class="opening">
				    <? foreach ($portal_data['opening_days'] as $i=>$day): ?>
					<li class="opening_day_<?=$day['date']?>">
					    <div class="status_left">
						
						    <div class="month"><?=$day['month']?></div>
						    <div class="day"><?=$day['day']?></div>
						    <div class="weekday"><?=$day['weekday']?></div>
						
					    </div>
					    <div class="status_right">
						<? if ($day['is_open']): ?>
						<h1 class="status"><?=$day['open']?> - <?=$day['close']?></h1> 
						
						<? else: ?>
						<h1 class="status closed"><?=$eLib_plus["html"]["closed"]?></h1>
						<? endif; ?>
						<?=$day['description']?>
						<?=$i==0?'('.$portal_data['now_opening_status'].')':''?>

					    </div>
					    <p class="spacer"></p>
	
					</li>
					<p class="spacer"></p>
				    <? endforeach; ?>
				    </ul>
				    <p class="spacer"></p>
			    
				</div>
				
			    
			    <? endif; ?>
			    </div>
			    <div class="portal_news_content portal_news_content_news" id="portal_news" style="display:none;">
				<div class="portal_news_content_title"><?=$eLib_plus["html"]["news"]?><div class="news_tool">
				<? if ($permission['admin']): ?>
				    <a class="tool_manage" href="#"><?=$eLib_plus["html"]["add"]?></a>
				<? endif; ?>
				</div></div>
				
				<div class="portal_news_content_detail">
				    
				    <p class="spacer"></p>
				    <ul>
					
			
				    
				    </ul>
				    <p class="spacer"></p>
			    
				</div>
			    </div>
			    
			    <div class="portal_news_content portal_news_content_rules" id="portal_rules" style="display:none;">
				<div class="portal_news_content_title"><?=$eLib_plus["html"]["rules"]?><div class="news_tool">
				<? if ($permission['admin']): ?>
				    <a href="#" target="_blank" class="tool_edit"><?=$button_edit?></a>
				<? endif; ?>
				</div></div>
				<div class="portal_news_content_detail">
			       
			    
				</div>
			    </div>
			    
			    <div style="display:none;" class="portal_news_content portal_news_content_staff" id="portal_staff">
				<div class="portal_news_content_title"><?=$eLib_plus["html"]["staff"]?><div class="news_tool">
				<? if ($permission['admin']): ?>
				    <a href="/home/library_sys/admin/settings/open_time.php" target="_blank" class="tool_manage"><?=$eLib_plus["html"]["manage"]?></a>
				<? endif; ?>
				</div></div>
				<div class="portal_news_content_detail">
			       
			    
				</div>
			    </div>
			    
                        </div>
			
                    </div>
                </div>
                
              <div id="seasonal_deco_07" class="seasonal_deco"></div>
              <div id="seasonal_deco_08" class="seasonal_deco"></div>
            <p class="spacer"></p>
        
                <div class="ranking">
                
                    <div class="ranking_header">
                       
                        <h1><?=$eLib_plus["html"]["ranking"]?></h1>
                       
                        <p class="spacer"></p>
                        <div class="ranking_filter">
			    <select class="ranking_type">
			      <option value="MostActiveUsers"><?=$eLib_plus["html"]["mostactivereviewers"]?></option>
			      <option value="MostBorrowUsers"><?=$eLib_plus["html"]["mostactiveborrowers"]?></option>
			      <option value="MostUsefulReviews" selected="selected"><?=$eLib_plus["html"]["mosthelpfulreview"]?></option>
			    </select><p class="spacer"></p>
			    <select class="ranking_range">
			      <option value="thisweek"><?=$eLib_plus["html"]["thisweek"]?></option>
			      <option value="accumulated"><?=$eLib_plus["html"]["accumulated"]?></option>
			      
			    </select>
			    
                        </div>
                       
                    </div>
		    <div class="ranking_container" style="display:none;">
		    </div>
                   
                    <div class="ranking_bottom">
                    <a class="btn_more" href="./?page=ranking&range=thisweek#MostUsefulReviews"><?=$eLib_plus["html"]["more"]?>..</a>
                    </div>
                    
                </div>
            </div>
        
            <div class="portal_board_right">

		<div class="portal_book_row">
		    <div class="portal_bookshelf portal_bookshelf_recommend">
			
			<div class="portal_recommend_back"></div>
    
    
			<div class="portal_new_recommend">
			   
			    <div class="bookshelf"><!-- --> <div id="seasonal_deco_01" class="seasonal_deco"></div><div class="bookshelf_right"><div class="bookshelf_bg"> 
				
				<div class="bookshelf_content">                  
				    <ul class="book_list" style="opacity:0;">
				    </ul>
				</div>
				<div class="bookshelf_title"> 
					<div class="bookshelf_title_name"><ul><li class="current"><a href="#recommend"><?=$eLib_plus["html"]["recommend"]?></a></li><li class="sep"></li><li><a href="#new"><?=$eLib_plus["html"]["new"]?></a></li></ul></div>
					<div class="bookshelf_title_filter"><ul><li class="btn_more"><a href="./?page=recommend"><?=$eLib_plus["html"]["more"]?>..</a></li>
				    </ul></div>
				</div>
			    </div><div id="seasonal_deco_02" class="seasonal_deco"></div></div></div>
			</div>
		    </div>
		    <div class="others_book"></div>
                 </div>
                
                
		<p class="spacer"></p>				
                <div class="portal_book_row<?if($plugin['elib_video'] || ($plugin['elib_epost'] && ($plugin['elib_cosmos'] || $plugin['elib_emag'])))echo "_addon";?>">
                    <div class="portal_bookshelf portal_mostrate">
			
			<div class="bookshelf"><div id="seasonal_deco_03" class="seasonal_deco"></div><div class="bookshelf_right"><div class="bookshelf_bg"> 
			    
			    <div class="bookshelf_content">		    
				<ul class="book_list book_list_small" style="opacity:0;">
				</ul>
			    </div>
			    <div class="bookshelf_title"> 
				    <div class="bookshelf_title_name"><ul><li class="current"><span><?=$eLib_plus["html"]["bestrate"]?></span></li></ul></div>
				    <div class="bookshelf_title_filter"><ul><li class="current"><a href="#thisweek"><?=$eLib_plus["html"]["thisweek"]?></a></li><li><a href="#accumulated"><?=$eLib_plus["html"]["accumulated"]?></a></li><li class="sep"></li><li class="btn_more"><a href="./?page=ranking#bestreview"><?=$eLib_plus["html"]["more"]?>..</a></li>
				</ul></div>
			    </div>
			</div><div id="seasonal_deco_04" class="seasonal_deco"></div></div></div>
		    </div>
		    <!-- 2013-04-24 Charles Ma-->
		    <? if($plugin['elib_video']){?>
		    	<?if($plugin['elib_epost'] && ($plugin['elib_cosmos'] || $plugin['elib_emag'])){?>
				    <div class="other_videos other_videos_small">
		                    <a href="../elibvideo/access<?=$plugin['elib_video_local_ver'] ? "2" : ""?>.php" target="_blank" class="video_btn <?=$eLib_plus["html"]["video_btn"]?>">
		                        <div class="video_green"></div>
		                        <div class="video_blue_area">
		                            <div class="video_cam"></div>
		                            <div class="video_cam_shadow"></div>
		                            <div class="video_text">
		                                <div class="video_text_shadow"></div>
		                            </div>
		                        </div>
		                    </a>
		             </div>
	             <?}else{?>
	             	<div class="other_videos">
		                    <a href="../elibvideo/access<?=$plugin['elib_video_local_ver'] ? "2" : ""?>.php" target="_blank" class="video_btn <?=$eLib_plus["html"]["video_btn"]?>">
		                        <div class="video_green"></div>
		                        <div class="video_blue_area">
		                            <div class="video_cam"></div>
		                            <div class="video_cam_shadow"></div>
		                            <div class="video_text">
		                                <div class="video_text_shadow"></div>
		                            </div>
		                        </div>
		                    </a>
		             </div>
	             <?}?>
	         <?}else{?>
	         	<?if($plugin['elib_epost'] && ($plugin['elib_cosmos'] || $plugin['elib_emag'])){?>
	         		<div class="others_epost">
		         		<a id="redirect_to_emag_site" style="display:none;" href="magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>                    	    	
			    		<div class="epost_stand">
							<?if($plugin['elib_cosmos'] && $plugin['elib_emag']){?>
                        	<a href="magazine_list.php" class="magazine  fancybox fancybox_magazine fancybox.iframe" title=""></a>
	                         <?} else if($plugin['elib_cosmos']){?>
	                         	<a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="magazine" title=""></a>
	                          <?} else if($plugin['elib_emag']){?>	
	                          	<a id="redirect_to_emag_site" href="magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>
	                         <?} ?>		 	         
	                        <span class="stand_glass"></span>
	                    </div>
                    </div>
	         	<?}?>
             <?}?>
		    <!-- 2013-04-24 Charles Ma END-->
		    <div class="others_book"></div>
                </div>
                
        <?if($plugin['elib_video'] &&($plugin['elib_epost'] || ($plugin['elib_cosmos'] || $plugin['elib_emag']))|| ($plugin['elib_epost'] && ($plugin['elib_cosmos'] || $plugin['elib_emag']))){?>    
        	<p style="padding: 10px;" class="spacer"></p>
        <?}?>      
		<div class="portal_book_row<?if(($plugin['elib_epost'] || ($plugin['elib_cosmos'] || $plugin['elib_emag'])))echo "_addon";?>">
		    <div class="portal_bookshelf portal_mosthit">
			
				<div class="bookshelf">
					<div id="seasonal_deco_05" class="seasonal_deco"></div>
					<div class="bookshelf_right">
						<div class="bookshelf_bg"> 
				    
						    <div class="bookshelf_content">                     
								<ul class="book_list book_list_small" style="opacity:0;">
								</ul>
						    </div>
						    <div class="bookshelf_title"> 
							    <div class="bookshelf_title_name">
							    	<ul>
							    	<? if ($ebook_exist): ?>
							    		<li class="current"><a href="#MostHitBooks"><?=$eLib_plus["html"]["mosthitbook"]?></a></li><li class="sep"></li>
							    	<? endif;?>	
							    		<li class="<?=($ebook_exist ? '' : 'current')?>"><a href="#MostLoanBooks"><?=$eLib_plus["html"]["mostloanbook"]?></a></li>
							    	</ul>
							    </div>			    
							    <div class="bookshelf_title_filter">
							    	<ul>
							    		<li class="current"><a href="#thisweek"><?=$eLib_plus["html"]["thisweek"]?></a></li>
							    		<li><a href="#accumulated"><?=$eLib_plus["html"]["accumulated"]?></a></li>
							    		<li class="sep"></li><li class="btn_more"><a href="./?page=ranking&range=thisweek#mosthit"><?=$eLib_plus["html"]["more"]?>..</a></li>
									</ul>
								</div>
						    </div>
						</div>
						<div id="seasonal_deco_06" class="seasonal_deco"></div>
					</div>
				</div>
			</div>
		    
		    <script>	<!-- Temporary added for demo 2013-05-03 Charles MA-->
		    	function openNewspaper(NewspaperID, UpdateView)
				{
					var intWidth  = screen.width;
				 	var intHeight = screen.height-100;
					window.open('/home/ePost/newspaper/view_newspaper.php?NewspaperID='+NewspaperID+'&UpdateView=1',"ePost_Newspaper","width="+intWidth+",height="+intHeight+",scrollbars,resizable");
				}
		    </script>
		    
		    <?php $date1 = date("Y-m-d");?>          
		              	
            <?php if($plugin['elib_epost'] && ($plugin['elib_cosmos'] || $plugin['elib_emag']) ){?>
		    	<div class="others_epost">
        			<? if($plugin['elib_video']){?>
            			<a id="redirect_to_emag_site" style="display:none;" href="magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>
                    	<div class="epost_magazine_stand">
                    	<!--<a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="magazine" title="Magazine">	
                        </a>-->
                        
                        <?if($plugin['elib_cosmos'] && $plugin['elib_emag']){?>
                        	<a href="magazine_list.php" class="magazine  fancybox fancybox_magazine fancybox.iframe" title=""></a>
                         <?} else if($plugin['elib_cosmos']){?>
                         	<a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="magazine" title=""></a>
                          <?} else if($plugin['elib_emag']){?>	
                          	<a id="redirect_to_emag_site" href="magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>
                         <?} ?>		                       
                         
                        <span class="stand_glass_magazine"></span>
                        <!--<a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="btn_more btn_more_magazine">more..</a>-->
                        <? if($plugin["elib_plus_demo"]){?>
                        <a href="javascript:openNewspaper(43,0)" class="newspaper">
                        	 <div class="post_title"> <h1>悅讀資訊 </h1><h2>April 2013</h2></div>
                        </a>
                        <?}else{?>
                        <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="newspaper">
                        	 <div class="post_title"> <h1>ePost </h1><h2></h2></div>
                        </a>
                        <?}?>
                        <span class="stand_glass"></span>
                        <span class="logo_epost"></span>
                        <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="btn_more"><?=$eLib_plus["html"]["more"]?></a>
                        </div>
                	<?} else{?>
                		<div class="epost_stand">
	                    	<? if($plugin["elib_plus_demo"]){?>
	                        <a href="javascript:openNewspaper(43,0)" class="newspaper">
	                        	 <div class="post_title"> <h1>悅讀資訊 </h1><h2>April 2013</h2></div>
	                        </a>
	                        <?}else{?>
	                       <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="newspaper">
                        	 <div class="post_title"> <h1>ePost </h1><h2></h2></div>
                       	   </a>
	                        <?}?>
	                        <span class="stand_glass"></span>
	                        <span class="logo_epost"></span>
	                        <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="btn_more"><?=$eLib_plus["html"]["more"]?></a>
	                    </div>
                	<?}?>
		    </div>
		    <?php }else if($plugin['elib_epost']){?>
		    <div class="others_epost">
		    		<div class="epost_stand">
                    	<? if($plugin["elib_plus_demo"]){?>
                        <a href="javascript:openNewspaper(43,0)" class="newspaper">
                        	 <div class="post_title"> <h1>悅讀資訊 </h1><h2>April 2013</h2></div>
                        </a>
                        <?}else{?>
                        <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="newspaper">
                        	 <div class="post_title"> <h1>ePost </h1><h2></h2></div>
                        </a>
                        <?}?>
                        <span class="stand_glass"></span>
                        <span class="logo_epost"></span>
                        <a href='javascript:newWindow("/home/ePost/index.php", 36)' class="btn_more"><?=$eLib_plus["html"]["more"]?></a>
                    </div>
		    </div>
		    <?php }else if(($plugin['elib_cosmos'] || $plugin['elib_emag'])){?>
		    <div class="others_epost">
		    		<a id="redirect_to_emag_site" style="display:none;" href="magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>                    	    	
		    			<div class="epost_stand">
                        
						<?if($plugin['elib_cosmos'] && $plugin['elib_emag']){?>
                        	<a href="magazine_list.php" class="magazine  fancybox fancybox_magazine fancybox.iframe" title=""></a>
                         <?} else if($plugin['elib_cosmos']){?>
                         	<a href='javascript:newWindow("/api/cosmos_access.php", 36)' class="magazine" title=""></a>
                          <?} else if($plugin['elib_emag']){?>	
                          	<a id="redirect_to_emag_site" href="magazine_list.php?redirect_to_emag_site=1" class="magazine  fancybox fancybox_magazine_learnthenews fancybox.iframe" title=""></a>
                         <?} ?>		 

                        <span class="stand_glass"></span>
                    </div>
		    </div>
		     <?php }?>
		    
		    <div class="others_book"></div>
		</div>                
		
		<p class="spacer"></p>
  
            </div>
        <!-- right end -->
        </div>
    </div></div></div>
    
    <div class="elib_content_board_bottom"><div class="elib_content_board_bottom_right"><div class="elib_content_board_bottom_bg">
	<p class="spacer"></p>
    </div></div></div>

</div>
