<? 
/*
 * Editing by: 
 * 
 * Modification Log:
 *
 * 2016-05-25 [Cameron]
 * 		- add 'http://' to url link if it does not contain 'http://' or 'https://' to avoid local link [case #P94419]
 * 		  use $physical_book_url instead of url to distinguish it from ebook
 *   
 * 2016-04-20 [Cameron]
 * 		- show url link in book info area if it's not empty, open the link in new page, the link is wrapped [case #P94419]
 * 
 * 2015-11-26 [Cameron]
 * 		- set vertical scrollbar for books description if the height exceed that of books summary info 
 * 			(set id of these fields so that the height can be adjusted by jquery)
 * 	
 * 2015-11-23 [Cameron]
 * 		- replace <> to ltltgtgt before applying rawurlencode() to $author
 * 
 * 2015-11-20 [Cameron]
 * 		- apply function rawurlencode() to $author 
 *
 * 2015-08-03 (Cameron)
 * 		- show more fields (isbn, no_of_page, subject)
 *
 * 2015-06-19 (Cameron)
 * 		- display book category name according to login language if $sys_custom['eLibraryPlus']['BookCategorySyncTwoLang'] is true
 * 
 * 2015-03-13 (Cameron)
 * 		- add $Lang["libms"]["report"]["onloan"]
 * 		- change $detail_data['loan_count'] to $detail_data['current_loan_count'] 
 * 
 * 2015-02-04 (Henry)
 * 		- added sub title in book details
 * 
 */
if (!isset($detail_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}
?>
<script>

$(document).ready(function(){
	
	$('.fancybox-inner').scrollTop(0);
        $('.add_write, .add_recommend').unbind("click").bind("click",function(e){
                    
                    $($(this).attr('href')).slideDown().siblings().hide();
                    
                    return false;
                    
        });
        $('.cancel').unbind("click").bind("click",function(e){
                    e.preventDefault();
                    $('#recommend_form, #review_form').slideUp();
                    
        });
        $('input[value=""]').unbind("click").bind("click",function(e){
                    e.preventDefault();
                    $('#recommend_form, #review_form').slideUp();
                    
        });
        $('body #review_form').submit(function(e){
                    e.preventDefault();
                    this.rate.value=$(this).find('.star_on').length;
                    if (this.content.value.length==0){
                                alert('<?=$eLib_plus["html"]["pleaseentercontent"]?>');
                                return false;
                    }
                    $(this).slideUp();
                    $('.add_write').fadeIn();
                    $.fancybox.showLoading();
                    $.post('ajax.php?action=submitReview', $(this).serialize(), function(){
                                loadStat();
                                loadReviews();
                                $.fancybox.hideLoading();
                    });
        });
        
        $('body #recommend_form').submit(function(e){
                    e.preventDefault();

                    $(this).slideUp();
                    $.fancybox.showLoading();
                    $.post('ajax.php?action=submitRecommend', $(this).serialize(), function(data){
                                
                                $.fancybox.hideLoading();
                                $('.add_recommend').fadeIn();
                                $('.remove').show();
                    });
        });
        
        $('#check_all').click(function(){
                if ($(this).is(':checked')){
                    $('#recommend_form input:checkbox').attr('checked', 'checked');
                }else{
                    $('#recommend_form input:checkbox').removeAttr('checked');
                }
        });
        
        $('#review_form .star_b li').mouseover(function(){
                    $(this).nextAll().removeClass('star_on');
                    $(this).prevAll().addClass('star_on');
                    $(this).addClass('star_on');
        });
        loadStat();
        loadReviews();
        
});
function loadStat(){
        $.fancybox.showLoading();
	$('.book_control').load('ajax.php?action=getStat&book_id=<?=$book_id?>', function(){$.fancybox.hideLoading();});
}
function loadReviews(){
        $.fancybox.showLoading();
	$('body #review_content').fadeOut().load('ajax.php?action=getReviews&book_id=<?=$book_id?>&offset=0',function(){
            $.fancybox.hideLoading();
            $(this).fadeIn()
        });
}
</script>

<div class="book_info bookdetail_bg">
    <div class="book_info_show">
        <ul class="book_list">
	    <?=$book_item?>
                      <!--<li class="no_cover"> <a href="#" class="book_cover"><h1>Bookname</h1><span>Author</span></a><a href="#" class="icon_ebook"><span>Read Now</span></a></li>-->
        </ul>
	<div class="book_info_detail">
    	
		<h1 id='book_title'><?=$detail_data['title']?></h1>
		<?=(trim($detail_data['sub_title'])!=''?'<div><b> - '.trim($detail_data['sub_title']).'</b></div>':'')?>
		<table id="book_info_summary">
		<col class="book_info_field" />
	       <col class="book_info_field_sep" />
		<tr><td><?=$eLib["html"]["author"]?> 		</td><td>:</td><td>
		<? foreach ($detail_data['authors'] as $i=>$author): ?>
		    <?=$i>0?", ":''?><a href="./?page=search&keyword=<?=rawurlencode(str_replace("<>","ltltgtgt",$author))?>"><?=$author?></a>
		<? endforeach; ?>
		</td></tr>
		
		<? if (!empty($detail_data['publisher'])) : ?>
		<tr><td><?=$eLib["html"]["publisher"]?> 	</td><td>:</td><td><?=$detail_data['publisher']?></td></tr>
		<? endif; ?>	
			
		<? if (!empty($detail_data['publish_year'])) : ?>
		<tr><td><?=$eLib["html"]["publish_year"]?> 	</td><td>:</td><td><?=$detail_data['publish_year']?></td></tr>
		<? endif; ?>
						
		<? if (!empty($detail_data['publisher_place'])) : ?>
		<tr><td><?=$eLib["html"]["publish_place"]?> 	</td><td>:</td><td><?=$detail_data['publisher_place']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['category'])) : 
			if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
				$catTitle = explode('-',$detail_data['category']);
				if (count($catTitle) > 2) {
					if ($intranet_session_language == 'en') {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[1]; 
					}	
					else {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
					}
				}
				else {
					$dispTitle = $detail_data['category'];
				}
			}
			else {
				$dispTitle = $detail_data['category']; 
			}		
		?>
		<tr><td><?=$eLib["html"]["category"]?>  	</td><td>:</td><td><?=$dispTitle?><?=(empty($detail_data['subcategory']))? "" : "->".$detail_data['subcategory']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['subject'])) : ?>
		<tr><td><?=$eLib["html"]['Subject']?>  	</td><td>:</td><td><?=$detail_data['subject']?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['level']) || $detail_data['level'] == "0") : ?>
		<tr><td><?=$eLib['Book']["Level"]?>  	</td><td>:</td><td><?=$detail_data['level']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['language'])) : ?>
		<tr><td><?=$eLib["html"]["language"]?>  	</td><td>:</td><td><?=$detail_data['language']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['edition'])) : ?>
		<tr><td><?=$eLib_plus["html"]["edition"]?>  	</td><td>:</td><td><?=$detail_data['edition']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['callnumber'])) : ?>
		<tr><td><?=$eLib_plus["html"]['call_number']?>  	</td><td>:</td><td><?=$detail_data['callnumber']?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['isbns'])) : ?>
		<tr><td><?=$eLib["html"]['ISBN']?>  	</td><td>:</td><td><?=implode(", ",$detail_data['isbns'])?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['no_of_page'])) : ?>
		<tr><td><?=$eLib["html"]['NoOfPage']?>  	</td><td>:</td><td><?=$detail_data['no_of_page']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['current_loan_count'])) : ?>
		<tr><td><?=$Lang["libms"]["report"]["onloan"]?>  	</td><td>:</td>
		<td>
		<a href="javascript:void(0);" onClick="$('.item_details').hide();$('#borrowed_item_details').show();"><?=$detail_data['current_loan_count']?></a>
		<div id="borrowed_item_details" class="item_details" style="display:none;visibility:visible;">
			<div class="close"><a href="Javascript:void(0);" onClick="$('#borrowed_item_details').hide();"><?=$Lang['Btn']['Close']?></a></div>
		    <div class="body" style="position:relative;">
		   		<div class="fav">
		   			<table cellspacing="5" cellpadding="5">
		   				<tr style="font-weight:bold;"><td><?=$Lang["libms"]["book"]["code"]?></td><td><?=$eLib_plus["html"]["duedate"]?></td></tr>
		   				<?foreach($detail_data['borrowed_item_list'] as $_itemAry):?>
		   					<tr>
		   						<td><?=$_itemAry['ACNO']?></td>
		   						<td><?=$_itemAry['duedate']?></td>
		   					</tr>
		   				<?endforeach;?>
		   				
		   			</table>
		   		</div>
		    </div>
		</div>	
		</td>
		</tr>
		<? endif; ?>
		
		<? 
		//if (!empty($detail_data['available_count'])) :
		if($detail_data['type'] == 'physical') : 
		?>
		<tr><td><?=$eLib_plus["html"]["noofcopyavailable_short"]?>  	</td><td>:</td>
		<?if($detail_data['available_count']>0):?>
			<td>
			<a href="javascript:void(0);" onClick="$('.item_details').hide();$('#available_item_details').show();"><?=$detail_data['available_count']?></a>
			<div id="available_item_details" class="item_details" style="display:none;visibility:visible;">
			<div class="close"><a href="Javascript:void(0);" onClick="$('#available_item_details').hide();"><?=$Lang['Btn']['Close']?></a></div>
		    <div class="body" style="position:relative;">
		   		<div class="fav">
		   			<table cellspacing="5" cellpadding="5">
		   				<tr style="font-weight:bold;"><td width="30%"><?=$eLib_plus["html"]["location"]?></td><td width="70%"><?=$Lang["libms"]["book"]["code"]?> (<?=$eLib["html"]["book_status"]?>)</td></tr>
		   				<?foreach($detail_data['available_item_list'] as $_location => $_itemAry):?>
		   				<?$itemCnt = count($_itemAry);?>
		   					<tr>
		   						<td rowspan="<?=$itemCnt?>" valign="top"><?=$_location?$_location:$Lang["libms"]["status"]["na"]?></td>
		   						<td><?=$_itemAry[0]['ACNO']?> (<?=$Lang["libms"]["book_status"][$_itemAry[0]['status']]?>)</td>
		   					</tr>
		   					<?for($ii=1;$ii<$itemCnt;$ii++):?>
		   						<tr>
		   							<td><?=$_itemAry[$ii]['ACNO']?> (<?=$Lang["libms"]["book_status"][$_itemAry[$ii]['status']]?>)</td>
		   						</tr>
		   					<?endfor;?>
		   				<?endforeach;?>
		   				
		   			</table>
		   		</div>
		    </div>
		</div>
			</td>
			<?else:?>
			<td><?=$detail_data['available_count']?></td>
			<?endif;?>
		</tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['reserved_count'])) : ?>
		<tr><td><?=$eLib_plus["html"]["noofcopyreserved_short"]?>  	</td><td>:</td><td><?=$detail_data['reserved_count']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['location'])) : ?>
		<tr><td><?=$eLib_plus["html"]["location"]?>  	</td><td>:</td><td><?=$detail_data['location']?></td></tr>
		<? endif; ?>
		<? /*
		if (!empty($detail_data['status'])) : ?>
		<tr><td><?=$eLib_plus["html"]["status"]?>  	</td><td>:</td><td><?=$detail_data["status"]? $eLib_plus["html"]["available"] :$eLib_plus["html"]["notavailable"] ?></td></tr>
		<? endif; 
		*/ ?>
		
		<? if (!empty($detail_data['tags'])) : ?>
		<tr><td><?=$eLib_plus["html"]["tags"]?> 	</td><td>:</td><td>
			<? $t_idx = 0; ?>
		    <? foreach($detail_data['tags'] as $tag):?>
			<?=($t_idx>0)?',':''?> <a href="./?page=tag&tag_id=<?=$tag['tag_id']?>"><?=$tag['tag_name']?></a><? $t_idx = 1; ?>  
		    <? endforeach;?>
		<? endif;?>
		</td></tr>

		<? if (!empty($detail_data['physical_book_url'])) : 
			if ((strpos($detail_data['physical_book_url'],'http://') === false) && (strpos($detail_data['physical_book_url'],'https://') === false)) {
				$link = 'http://'.$detail_data['physical_book_url']; 
			}
			else {
				$link = $detail_data['physical_book_url']; 
			}
		?>
		<tr valign="top"><td><?=$Lang["libms"]["book"]["URL"]?></td><td>:</td><td><a target="_blank" href="<?=$link?>"><span style="display: block; width: 140px; word-wrap: break-word;"><?=$detail_data['physical_book_url']?></span></a></td></tr>
		<? endif; ?>
	
	    </table>
	    <div class="book_desc" id="book_description"><span><?=$detail_data['description']?></span></div>
	    
	</div>
	<p class="spacer"></p>
    </div>
</div>
<div class="book_control">
</div>
<div class="book_review">
    <div class="write_review" id="write_review">
	
	<div class="book_tool">
	    <div class="write">
	    
	       <a href="#review_form" class="add_write"> <?=$eLib_plus["html"]["writereview"]?></a>
	    </div>
	</div> 
	<div class="book_tool" id="delete_review">   
	    <? if ($permission['admin']) : ?>
	    <div class="write">
	    
	       <!--<a style="display:none" href="#recommend_form" class="add_recommend"> <?=$eLib["html"]["recommend_this_book"]?></a>-->
	    </div>
	    
	    
	    <!--2013-11-14 Charles Ma  		2013-11-14-deleteallbookcomment-->
	    <div class="book_tool">	    	
	       <a title="Remove" href="ajax.php?action=removeAllReview&amp;book_id=<?=$book_id?>" class="remove_all_review btn_delete"> <?=$eLib["html"]["remove_all_reviews"]?></a>	      
	    </div>
	     <!--2013-11-14 END-->
	    <? endif; ?>
	</div>    
	
	<p class="spacer"></p>
	<div>
	    <form id="review_form">
			<?=$eLib_plus["html"]["yourreview"]?>..
			<p class="spacer"></p>
			<div class="star_b">
			  <ul>
				    <li class="star_on"></li><li></li><li></li><li></li><li></a></li>
			  </ul>
			</div>
			<textarea name='content'></textarea>
			<input type='hidden' name='rate' value='1'/>
			<input type='hidden' name='book_id' value='<?=$book_id?>'/>
			<div class="form_btn">
			<input name="submit2" type="submit" class="formbutton" value="<?=$eLib["html"]["submit"]?>" /> <input name="submit2" type="button" class="formbutton cancel" value="<?=$eLib["html"]["cancel"]?>"/>
			</div>
	     
	    </form>
	    <? if ($permission['admin']) : ?>
	    <form id="recommend_form">
			<?=$eLib_plus["html"]["recommendbookto"]?>..
			<p class="spacer"></p>
			
			<table>
			    <tr><td><input type="checkbox" id="check_all"><?=$eLib["html"]["all_class"]  ?></td></tr>
			<? $class_table = array_chunk($detail_data['recommend']['class_levels'],4) ; foreach ($class_table as $class_level_rows): ?>
			    <tr>
			    <? foreach ($class_level_rows as $class_level): ?>
				<td><input type="checkbox" <?=$class_level['class_level_recommended']? "checked='checked'":""?> name="class_level_ids[]" value="<?=$class_level['class_level_id']?>"><?=$class_level['class_level_name']?></td>
			    <? endforeach ?>
			    </tr>
			<? endforeach ?>
			</table>
			<textarea name='content'><?=$detail_data['recommend']['content']?></textarea>
			<input type='hidden' name='mode' value='<?=$detail_data['recommend']['mode']?>'/>
			<input type='hidden' name='book_id' value='<?=$book_id?>'/>
			<div class="form_btn">
			<input name="submit2" type="submit" class="formbutton" value="<?=$eLib["html"]["submit"]?>" /> <input name="submit2" type="button" class="formbutton cancel" value="<?=$eLib["html"]["cancel"]?>"/>
			</div>
	     
	    </form>
	    <? endif; ?>
	</div>
    </div>
    <div class="review_list  <? if ($permission['admin']) echo "review_list_teacher"; ?>" id="review_content">
    </div>
</div>