<?
/*
 * Editing by  
 * 
 * Modification Log:
 *
 * 2016-02-26 [Cameron]
 * 		- set div height for $("#book_result") so that IE/Edge can also autos croll (case #G93104) 
 *
 * 2015-11-30 [Cameron]
 * 		- add afterShow() event to $('.book_detail').fancybox to adjust the height of book description
 *
 * 2015-11-25 (Cameron)
 * 		- correct words for total number of books in Chinese by using $Lang["libms"]["portal"]["book_total"] 
 * 
 * 2015-09-22 (Cameron)
 * 		- apply encodeURIComponent() function to default_options
 * 
 * 2015-09-16 (Cameron)
 * 		- fix bug on adding backslashes to default_options
 * 
 * 2015-06-19 (Cameron)
 * 		- display book category name according to login language if $sys_custom['eLibraryPlus']['BookCategorySyncTwoLang'] is true
 * 
 * 2015-05-04 (Charles Ma) [ip.2.5.6.5.1]
 * 		 - P77849
 * 
 * 2015-02-11 (Henry)
 * 		- added subtitle in default_options
 * 
 */
 
 
if (!isset($category_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}

$category = htmlspecialchars_decode($category,ENT_QUOTES);		// case from categories link
$subcategory = htmlspecialchars_decode($subcategory,ENT_QUOTES);

?>
<script type="text/javascript">
var book_number = 0;
default_options = {
    offset:		0,
    amount: 		location.hash=='#list'?  10 : 30,
    type:		location.hash=='#list'? 'list' : 'cover',
    language: 		'<?=$language?>',
    category:		encodeURIComponent('<?=addslashes($category)?>'),
    subcategory:	encodeURIComponent('<?=addslashes($subcategory)?>'),
    keyword:		encodeURIComponent('<?=addslashes($keyword)?>'),
    tag_id:		encodeURIComponent('<?=addslashes($tag_id)?>'),
    title:		encodeURIComponent('<?=addslashes($title)?>'),
    subtitle:	encodeURIComponent('<?=addslashes($subtitle)?>'),
    author:		encodeURIComponent('<?=addslashes($author)?>'),
    level:		encodeURIComponent('<?=addslashes($level)?>'),
    sortby:		'<?=$sortby?>',
    order:		'<?=$order?>' ,
};

$(document).ready(function() {

      $('.book_detail').fancybox({
		  autoDimensions : false,
		  autoScale : false,
		  autoSize : false,
		  width : 800,
		  height : 600,
		  type : 'ajax',
		  padding: 0,
		  nextEffect: 'fade',
		  prevEffect: 'fade', 
		afterShow: function() {
			var isScroll = false;
			var baseline = 140;
	        if ($('#book_description').height() > baseline) {
	        	if ($('#book_info_summary').height() <= baseline) {
	        		$('#book_description').css('height',baseline);
	        		isScroll = true;
	        	}
	        	else {
	        		if ($('#book_info_summary').height() < $('#book_description').height()) {
	        			$('#book_description').css('height',$('#book_info_summary').height()-10);
	        			isScroll = true;
	        		}
	        	}
	        }
	        
	        if (isScroll == true) {
				$('#book_description').css('width','260px');
				$('#book_description').css('overflow-y','scroll');        	
				$('#book_description').css('overflow-x','hidden');
				var h = $('#book_description').height() - 15;
				$('.book_desc span').css('background-position','215px '+ h + 'px');
				
				$('#book_description').scroll(function (event) {
				    var scroll = $('#book_description').scrollTop();
				    var height = $('#book_description').height() - 15 + scroll;  
				    $('.book_desc span').css('background-position','215px '+ height + 'px');
				});
	        }
	        else {
	        	$('#book_description').css('width','240px');
	        	$('#book_description').css('overflow-y','hidden');
	        	$('#book_description').css('overflow-x','hidden');
	        }
    	}    	
             
      });
              
      $('.toggle_tool li a').click(function(){
                  
	  loadBookList(true,{
	    offset: 0,
	    type: $(this).attr('href').replace('#','')
	    
	  });
	  
      });
	
	
	
      $('.btn_load_more').click(function(){
	loadBookList(true);
	return false;
      });
      <? if (in_array($page,array('category', 'tag'))): ?>
	    $('.book_cat_filter').mouseover(function(){$('.book_cat_filter_board').css({left:5});}).mouseout(function(){$('.book_cat_filter_board').css({left:-520});})
      
      <? endif; ?>
      loadBookList(true);
      
});
function sortBookList(sortby, order){
    
    <? if (!$category_options['no_sorting']) :?>
    
    loadBookList(false, {
	sortby: sortby,
	order: order,
	offset: 0
	
    });
    <? endif; ?>
    
    return false;
}

function strip(html)
{
   var tmp = document.createElement("DIV");
   tmp.innerHTML = html;
   return tmp.textContent||tmp.innerText;
}

function loadBookList(animate, options){ //2015-05-04 Charles Ma - P77849

      var list_options = $.extend(default_options, options);
      
      $('#book_result').off('scroll');
      $('a[href="#'+list_options.type+'"]').parent().addClass('selected').siblings().removeClass('selected');
      $.fancybox.showLoading();
      
      var amount = list_options.type=='list'? 10 : 30; 
      list_options.amount = list_options.offset==0?amount:amount; //list_options.offset==0?amount*2:amount;

      $.get('ajax.php?action=<?=$category_data['ajax_action']?>', list_options,function(data){
      	//parseInt(book_number) < (list_options.offset+list_options.amount)
      	
		$.fancybox.hideLoading();
		
      	if(strip(data) == data){
      		if(book_number == 0 ){
		      if (animate) $('.book_result_list').hide();
		      $('.book_result_list').html(data).fadeIn('slow');
		      $('#book_result').scrollTop(0);      			
      		}else{
      		
      		}
      	}else{
			if (list_options.offset==0){
			      if (animate) $('.book_result_list').hide();
			      $('.book_result_list').html(data).fadeIn('slow');
			      $('#book_result').scrollTop(0);
			}else{
			      list_options.type=='cover' ? $('.book_result_list').append(data) : $('.book_result_list .common_table_list').append(data);
			}
			list_options.offset+=list_options.amount;
			if (data!=''){
			    $('#book_result').on('scroll',function(e){
				if ($('#book_result').prop('scrollHeight')-$('#book_result').scrollTop()-240<=$('#book_result').height()){
				    loadBookList(false);
				}
			    });
			} 
      	}
      	

      });
      
      
      return false;
}
</script>
<div id="cat_board" class="elib_content_board">
    
    <div class="elib_content_board_top"><div class="elib_content_board_top_right"><div class="elib_content_board_top_bg">
    	  <p class="spacer"></p>
    </div></div></div>
    
    <div class="elib_content_board_content"><div class="elib_content_board_content_right"><div class="elib_content_board_content_bg">
  
        <div class="cat_content">
        
            <div class="book_cat_nav">
            	<div class="book_cat_filter">
		    <a class="book_cat_select" href="#">
		    <?php
			if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
				$catTitle = explode('-',$category_data['selected_category']);
				if (count($catTitle) > 2) {
					if ($intranet_session_language == 'en') {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[1]; 
					}	
					else {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
					}
				}
				else {
					$dispTitle = $category_data['selected_category'];
				}
			}
			else {
				$dispTitle = $category_data['selected_category']; 
			}
			echo $dispTitle;
		    ?></a>
                    
                </div>
		<span class="book_result" style="display:none;"><span class="total"></span> <?=$Lang["libms"]["portal"]["book_total"]?></span>
	    </div>
            <div class="toggle_tool">
            	<ul>
	                            	<li><a title="Book List" class="view_list" href="#list"></a></li>
                    <li class="selected"><a title="Book Cover" class="view_cover" href="#cover"></a></li>
                      <li> <span><?=$eLib_plus["html"]["viewby"] ?>:</span> </li>
                </ul>
            </div>
            <p class="spacer"></p>

            <div id="book_result" style="height:530px;">
		<div class="book_result_list">
		
		</div>
		<p class="spacer"></p>
	      </div>
        </div>
   		 <!---->
    </div></div></div>
    
    <div class="elib_content_board_bottom"><div class="elib_content_board_bottom_right"><div class="elib_content_board_bottom_bg"></div></div></div>
    
</div>


