<script type="text/javascript" src="/templates/jquery/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js"></script>
<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css" media="screen" />
<link href="/templates/<?=$LAYOUT_SKIN?>/css/elibplus.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" >

$(document).ready(function() {

    $('#type_all, #type_physical, #type_pc, #type_ipad').click(function(){
        $.fancybox.showLoading();
        $.post('ajax.php?action=setBookType', {book_type: $(this).attr('id')}, function(){
            location.reload();
        });
        return false;
    });
    
       
    $(".dropdown dt a").click(function() {
           $(".dropdown dd ul").slideToggle('fast');
           return false;
    });
                   
    $(document).click(function(e) {
           var $clicked = $(e.target);
           if (! $clicked.parents().hasClass("dropdown"))
               $(".dropdown dd ul").slideUp('fast');
    });
   
    $(document).keyup(function(e){var r=[83,85,76,80,66,73,76,69];if(typeof(k)=='undefined') k=r;if(e.keyCode==k[k.length-1] && $(':text:focus, textarea:focus').length==0) k.pop();else k=r;if(k.length==0){$.fancybox('<img style="max-width:100px;float:left" src="http://7.mshcdn.com/wp-content/uploads/2012/10/dancing_banana.gif"/><div style="float:right"><h2>eLibrary Plus</h2>by mickchiu@broadlearning 2012</div>');k=r;}});});
</script>
<div class="elib_content">
 	<div class="module_title eLib_title">
           <div class="module_title_icon"><a href="./"><img src="<?=$image_path."/".$LAYOUT_SKIN?>/eLibplus/icon_eLibplus.png" /></a></div>
    	  <div class="module_title_text">eLibrary <span>plus</span></div>
    	  <div class="select_book_type">
            <dl id="sample" class="dropdown">
              <dt><a href="#"><span>
                    <? if ($ck_eLib_bookType=='type_physical'): ?>
                        <?=$eLib_plus["html"]["book"]?>
                    <? elseif ($ck_eLib_bookType=='type_pc'): ?>
                        <?=$eLib_plus["html"]["ebook"] ?>
                        <input type="button" class="type_pc type_pc_selected" />
                        <input type="button" class="type_ipad" />
                    <? elseif ($ck_eLib_bookType=='type_ipad'): ?>
                        <?=$eLib_plus["html"]["ebook"] ?>
                        <input type="button" class="type_pc"/>
                        <input type="button" class="type_ipad type_ipad_selected"/>
                    <? else: ?>
                        <?=$eLib["html"]["all_books"] ?>
                    <? endif; ?>
              </span></a></dt>
              <dd>
                <ul>
                  <li <?=($ck_eLib_bookType=='type_all' || empty($ck_eLib_bookType)) ? 'class="selected"' : ""?>>
                    <a href="#" id="type_all"><?=$eLib["html"]["all_books"] ?></a>
                  </li>
                  <li <?=($ck_eLib_bookType=='type_physical') ? 'class="selected"' : ""?>>
                    <a href="#" id="type_physical"><?=$eLib_plus["html"]["book"] ?></a>
                  </li>
                  <li <?=($ck_eLib_bookType=='type_pc' || $ck_eLib_bookType=='type_ipad') ? 'class="selected"' : ""?>>
                    <a href="#"><?=$eLib_plus["html"]["ebook"] ?>
                          <input type="button" id="type_pc" class="type_pc <?=($ck_eLib_bookType=='type_pc') ? "type_pc_selected" : ""?>" />
                          <input type="button" id="type_ipad" class="type_ipad <?=($ck_eLib_bookType=='type_ipad') ? "type_ipad_selected" : ""?>" />
                    </a>
                  </li>
                </ul>
              </dd>
            </dl>
  	    </div>
    	  <form class="search" action="./" method="get" ><a href="#" style="display:none;"><?=$eLib_plus["html"]["advanced"] ?></a>
              <input type="hidden" name="page" value="search"/>
    	      <input type="textbox" required="required" placeholder="Search" name="keyword"/>
  	  </form>
    	  <p class="spacer"></p>
    	
    </div>
