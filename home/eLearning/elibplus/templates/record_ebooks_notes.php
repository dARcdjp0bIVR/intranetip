<?php
/* Change Log:
 * 
 * 	Date: 2015-12-16 (Paul) [ip.2.5.7.1.1]
 * 			- modified the UI for expired book
 * 
 * 
*/
?>
<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if (!$ebook_data['notes']&&$offset==0): ?>
    <tr><td colspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
<? else: ?>
<tr style="display:none;">
    <script>
    function ViewNotesContent_htmlbook(id, title, noteId,PageNum1PageMode, PageNum2PageMode)
	{
	 	var url = '../ebook_reader/'+id+'/reader/';
	     var newWindow = window.open(url, "title","status=1");
	
	      if (!newWindow) return false;
		
		var formid = "newForm", ID = 'id';
		
		
	     var html = "";	     
	     html += "<html><head></head><body><form  id='formid' method='post' action='" + url + "'>";
	     html += "<input type='hidden' name='mynote_p1' value='" + PageNum1PageMode+ "'/>";
	     html += "<input type='hidden' name='mynote_p2' value='" + PageNum2PageMode+ "'/>";     
	     html += "<input type='hidden' name='action' value='mynote'/>";
	     html += "</form></body><script>document.getElementById('formid').submit();<\/script></html>";
	
	     newWindow.document.write(html);
	     	     
	     return newWindow;	
	};    
    
    
    function MM_openBrWindowFull(theURL,winName,features, centerWindow) { //v2.0
	
	  var is_swf = theURL.indexOf('tool/index.php?BookID=');
	  var centerWindow = true;//(centerWindow)? centerWindow : false;
	  
	  if(is_swf != -1)
	  {
		  params  = 'width='+screen.width;
		  params += ', height='+screen.height;
		  params += ', top=0, left=0'
		  params += ', fullscreen=yes';
		  params += ', ' + features;
		  window.open(theURL,winName,params);
	  }
	  else
	  {
		  var w = 800;
		  var h = 650;
		  params  = 'width='+w;
		  params += ', height='+h;
		  params += ', top=0, left=0'
		  params += ', fullscreen=no';
		  params += ', ' + features;
		  
		  
		  if(centerWindow){
		  	var wleft 	= (screen.width - w) / 2;
		  	var wtop 	= (screen.height - h) / 2;
		  	params += ",left=" +wleft+ ", top=" + wtop
		  }
		  
		  var win = window.open(theURL,winName,params);
		  
		  if(centerWindow){
		  	//Just in case width and height are ignored
			win.resizeTo(w, h);
			
			//Just in case left and top are ignored
			win.moveTo(wleft, wtop);
			win.focus();
		  }
	  }
	}
	
    function ViewNotesContent(id, notes, noteId)
	{
		//alert(id + " " + notes);
		MM_openBrWindowFull('/home/eLearning/elibrary/tool/index.php?BookID='+id+'&NoteType='+notes+'&NoteID='+noteId,'booktool','scrollbars=yes');
	}
    	
	$(".record_detail").attr({"class": "record_detail record_notes"});	
	
	<? if (sizeof($ebook_data['notes'])>=$amount): ?>
	$('.record_notes').off('scroll').on('scroll',function(e){
	    
	    if ($(this).prop('scrollHeight')-$(this).scrollTop()-150<=$(this).height()){
		
		$.fancybox.showLoading();
		$('.record_detail').off('scroll');
		$.get('ajax.php?action=geteBookNotesRecord', {offset:'<?=$offset+$amount?>'}, function(data){
		    $.fancybox.hideLoading();
		    
		    $('.record_notes table').append(data);
		     
		});
		
	    }
	});
	<? endif; ?>
	
    </script>
</tr>
    <? foreach ($ebook_data['notes'] as $num=>$note) : ?>
    
	<tr>
	<td><?=$offset+$num+1?></td>
	<td><div class="ebook_title">
		<? if ($note['readable']): ?>
			<a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$note['url']?>" ></a>
			<a rel="notes_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$note['id']?>" class="book_title book_detail"><?=$note['title']?></a>
		<? else: ?>
			<a title="<?=$eLib_plus["html"]["expiredBook"]["onCoverRemark"]?>" class="icon_read_now" href="#" ></a>
			<a rel="notes_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$note['id']?>" class="book_title book_detail off_shelf_list"><?=$note['title'].' ('.$eLib_plus["html"]["expiredBook"]["onCoverRemark"].')'?></a>
		<? endif; ?>
	</div></td>
	<? if ($note['readable']): ?>
	<td>
		<!--<a href="javascript:<=$note['note_url']?>">
		<=$note['content']?>
		</a>--><?=$note['note_url']?>
	<? else: ?>
	<?php
		$noteContent = explode('>',$note['content']);
	?>	
	<td class="off_shelf_list">
		<?=$noteContent[1]?>
	<? endif; ?>
	</td>
	<!--<td><?=$note['category']?></td>-->
	<? if ($note['readable']): ?>
	<td><?=$note['last_modified']?></td>
	<? else: ?>
	<td class="off_shelf_list"><?=$note['last_modified']?></td>
	<? endif; ?>
	</tr>
    <? endforeach; ?>
<? endif; ?>