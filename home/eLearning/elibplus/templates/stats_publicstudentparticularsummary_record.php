<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if (!$ebook_data['reading']&&$offset==0): ?>
    <tr><td colspan="6" align="center"><?=$eLib_plus["html"]["norecord"]?></td></tr>
<? else: ?>
<tr style="display:none;">
    <script>
    
    var student_id = "<?=$student_id?>";
    
	$(".record_detail").attr({"class": "record_detail record_psps"});
	<? if (sizeof($ebook_data['reading'])>=$amount): ?>
	$('.record_psps').off('scroll').on('scroll',function(e){
	    
	    FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
			var sort =$("#Sort_Search").val();
			var order = $("#Order_Search").val();
	    
	    if ($(this).prop('scrollHeight')-$(this).scrollTop()-150<=$(this).height()){
		$.fancybox.showLoading();
		$('.record_detail').off('scroll');
		$.get('ajax.php?action=getPublicClassSummaryRecord&year_class_id=<?=$year_class_id?>&student_id=<?=$student_id?>&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear, {offset:'<?=$offset+$amount?>', order : order, sort : sort}, function(data){
		    $.fancybox.hideLoading();
		    $('.record_psps table').append(data);		     
		});
		
	    }
	});
	
	<? endif; ?>
	
	$(function(){
		$('.sort_column').attr("class","sort_column");
		$('[name="<?=$sort?>"]').attr("class", "sort_column <?=$order?>");
	});
	
	$('.sort_column').click(function(){		
		$.fancybox.showLoading();
		
		var sort = $(this).attr('name');
		var order = $(this).attr('class').replace("sort_column", "").replace(" ", "");
		
		if(order == ""|| order == "sort_asc") order = "sort_dec";
		else order = "sort_asc";
		
		$("#Sort_Search").val(sort);
		$("#Order_Search").val(order);
		
		FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
		
		$.get('ajax.php?action=getPublicClassSummaryRecord&year_class_id=<?=$year_class_id?>&student_id=<?=$student_id?>&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear,{offset: 0, order : order, sort : sort}, function(data){	  
		    $.fancybox.hideLoading();
			$('.sort_column').attr("class","sort_column");
		    $('.record_detail table').html($("#pulic_student_particular_summary_temp").html());
			 $('.record_detail table').append(data);	   
		    $('.record_detail').scrollTop(0);
		});
		
		return false;
	});
	
    </script>
</tr>
<? foreach ($ebook_data['reading'] as $num=>$progress) : ?>
    <tr>
	<td><?=($offset+$num+1)?></td>
	<td><div class="ebook_title">
		<a title="<?=$eLib_plus["html"]["readnow"]?>" class="icon_read_now" href="javascript:<?=$progress['url']?>" ></a>
		<a rel="reading_ebooks" href="ajax.php?action=getBookDetail&book_id=<?=$progress['id']?>" class="book_title book_detail"><?=$progress['title']?></a>
	</div></td>
	<td><?=$progress['author']?></td>
	<td>
		<? if (!empty($progress['percentage'])) :?>
		<div class="book_progress"><span <?=($progress['percentage']==100)? "class='done'" : 'style="left:'.($progress['percentage']-100).'px;"'?>></span><em><?=$progress['percentage']?></em></div>
		<? else: ?>
		--
		<? endif; ?>
	</td>
	<td><?=$progress['read_times']?></td>
	<td><?=$progress['last_accessed']?></td>
    </tr>
    <? endforeach; ?>
<? endif; ?>