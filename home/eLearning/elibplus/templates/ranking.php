<?php
/*
 * 	Log
 * 
 * 	Date:	2015-11-30 [Cameron]
 * 		- add afterShow() event to $('.book_detail').fancybox to adjust the height of book description
 * 
 */
?>
<? if (!isset($ranking_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script type="text/javascript">
$(document).ready(function() {
	
        $('.book_detail').fancybox({
               autoDimensions : false,
               autoScale : false,
               autoSize : false,
               width : 800,
               height : 600,
               type : 'ajax',
               padding: 0,
               nextEffect: 'fade',
               prevEffect: 'fade',
               beforeClose: function(){$('.rank_menu li.current_tab a').trigger('click.noanim');},
				afterShow: function() {
					var isScroll = false;
					var baseline = 140;
			        if ($('#book_description').height() > baseline) {
			        	if ($('#book_info_summary').height() <= baseline) {
			        		$('#book_description').css('height',baseline);
			        		isScroll = true;
			        	}
			        	else {
			        		if ($('#book_info_summary').height() < $('#book_description').height()) {
			        			$('#book_description').css('height',$('#book_info_summary').height()-10);
			        			isScroll = true;
			        		}
			        	}
			        }
			        
			        if (isScroll == true) {
						$('#book_description').css('width','260px');
						$('#book_description').css('overflow-y','scroll');        	
						$('#book_description').css('overflow-x','hidden');
						var h = $('#book_description').height() - 15;
						$('.book_desc span').css('background-position','215px '+ h + 'px');
						
						$('#book_description').scroll(function (event) {
						    var scroll = $('#book_description').scrollTop();
						    var height = $('#book_description').height() - 15 + scroll;  
						    $('.book_desc span').css('background-position','215px '+ height + 'px');
						});
			        }
			        else {
			        	$('#book_description').css('width','240px');
			        	$('#book_description').css('overflow-y','hidden');
			        	$('#book_description').css('overflow-x','hidden');
			        }
		    	}    	
        });     
              
        $('.rank_menu li a').on('click.noanim',function(e){
	    
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		var href=$(this).attr('href');
		
		
		$('.ranking_title').html($(this).html());
		
	    
		var action=href.replace('#','get');
		
		$.fancybox.showLoading();
		
		if (typeof(e.namespace)=='undefined') $('#ranking_container').slideUp();
		
		$.get('ajax.php',{action: action, range: '<?=$range?>'},function(data){
		       $.fancybox.hideLoading();
		       $('#ranking_container').html(data).slideDown();
		});
		
		
        });
	
	$('.rank_content_list .bookshelf_title_filter li a').click(function(){$(this).attr('href',$(this).attr('href')+location.hash);})
        
	$('.rank_menu li a[href="'+location.hash+'"]').parent().addClass('current_tab').siblings().removeClass('current_tab');
	$('.rank_menu li.current_tab a').trigger('click');
	
     
});

</script>
<div id="rank_board" class="elib_content_board">
    
    <div class="elib_content_board_top"><div class="elib_content_board_top_right"><div class="elib_content_board_top_bg">
       <p class="spacer"></p>
    </div></div></div>
    
    <div class="elib_content_board_content"><div class="elib_content_board_content_right"><div class="elib_content_board_content_bg">
    	<!---->
    	<div class="rank_content">
           <div class="portal_tab ranking_tab">
            </div>
        	<div class="rank_menu">
            
            	<ul>
                    <li class="book"><span></span></li>
                    <li class="current_tab"><a href="#BestRateBooks"><?=$eLib_plus["html"]["bestrate"] ?></a></li>
                    <li><a href="#MostHitBooks"><?=$eLib_plus["html"]["mosthit"] ?></a></li>
                    <li><a href="#MostLoanBooks"><?=$eLib_plus["html"]["mostloan"] ?></a></li>
                    <li class="reviewer"><span></span></li>
                    <li><a href="#MostBorrowUsers"><?=$eLib_plus["html"]["mostactiveborrowers"]?></a></li>
                    <li><a href="#MostActiveUsers"><?=$eLib_plus["html"]["mostactivereviewers"]?></a></li>
                    <li class="review"><span></span></li>
                    <li><a href="#MostUsefulReviews"><?=$eLib["html"]["most_useful_reviews"]?></a></li>
                </ul>
            </div>
            <!------->
            <div class="rank_content_list">
		
		<div class="rank_content_list_top"><div class="rank_content_list_top_right"><div class="rank_content_list_top_bg"></div></div></div>
		
                <div class="rank_content_list_content"><div class="rank_content_list_content_right"><div class="rank_content_list_content_bg">
		
		    <h1 class="ranking_title"></h1>
			 <div class="bookshelf_title_filter"><ul>
			    <li <?=$range=='thisweek'?'class="current"':''?> ><a href="./?page=ranking&range=thisweek"><?=$eLib_plus["html"]["thisweek"] ?></a></li>
			    <li <?=$range!='thisweek'?'class="current"':''?>><a href="./?page=ranking&range=accumulated"><?=$eLib_plus["html"]["accumulated"]?></a></li><!--<li class="sep"></li>--></ul></div>
		    <p class="spacer"></p>
			
			<div id="ranking_container">
			</div>
    
		    <p class="spacer"></p>
		 
            
                
                </div></div></div>
                <div class="rank_content_list_bottom"><div class="rank_content_list_bottom_right"><div class="rank_content_list_bottom_bg"></div></div></div>
            
		
		
	    </div>
        
        
        	</div>
   		 <!---->
    </div></div></div>
    <div class="elib_content_board_bottom"><div class="elib_content_board_bottom_right"><div class="elib_content_board_bottom_bg"></div></div></div>
</div>