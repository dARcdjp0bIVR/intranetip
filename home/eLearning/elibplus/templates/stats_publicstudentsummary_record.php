<? if (!isset($ebook_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? if (!$ebook_data['reading']&&$offset==0): ?>
    <tr><td colspan="6" align="Center"><?=$eLib_plus["html"]["norecord"]?></td></tr>
<? else: ?>
<tr style="display:none;">
    <script>
    
	$(".record_detail").attr({"class": "record_detail record_pss"});
	<? if (sizeof($ebook_data['reading'])>=$amount): ?>
	$('.record_pss').off('scroll').on('scroll',function(e){
	    if ($(this).prop('scrollHeight')-$(this).scrollTop()-150<=$(this).height()){
	    
	    FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
			var sort =$("#Sort_Search").val();
			var order = $("#Order_Search").val();
		
		$.fancybox.showLoading();
		$('.record_detail').off('scroll');
		$.get('ajax.php?action=getPublicClassSummaryRecord&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear, {offset:'<?=$offset+$amount?>', order : order, sort : sort}, function(data){
		    $.fancybox.hideLoading();
		    $('.record_pss table').append(data);		     
		});
		
	    }
	});
	<? endif; ?>
	
	
	$('.student_tab').click(function(){
		var target=$(this).attr('href');				
		var year_class_id = target.slice(target.lastIndexOf("#YearClassID"), target.lastIndexOf("#StudentID")).replace('#YearClassID','');		
		var student_id = target.slice(target.lastIndexOf("#StudentID"), target.lastIndexOf("#ClassName")).replace('#StudentID','');	
		var class_name = target.slice(target.lastIndexOf("#ClassName")).replace('#ClassName','');	
		$.fancybox.showLoading();
		 FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
		$("#Sort_Search").val('');
		 $("#Order_Search").val('');
		$.get('ajax.php?action=getPublicClassSummaryRecord&year_class_id='+year_class_id+'&student_id='+student_id+'&class_name='+class_name+'&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear,{offset: 0}, function(data){
		    $.fancybox.hideLoading();
		     $('.record_detail table').html($("#pulic_student_particular_summary_temp").html());
		    $('.record_detail table').append(data);
		    $('.record_detail').scrollTop(0);
		    
		});
		
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		return false;
	});
	
	$('.review_tab').click(function(){
		var target=$(this).attr('href');				
		var year_class_id = target.slice(target.lastIndexOf("#YearClassID"), target.lastIndexOf("#StudentID")).replace('#YearClassID','');		
		var student_id = target.slice(target.lastIndexOf("#StudentID"), target.lastIndexOf("#ClassName")).replace('#StudentID','');	
		var class_name = target.slice(target.lastIndexOf("#ClassName")).replace('#ClassName','');	
		$.fancybox.showLoading();
		 FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
		$("#Sort_Search").val('');
		 $("#Order_Search").val('');
		$.get('ajax.php?action=getReviewRecord2&student_id='+student_id+'&class_name='+class_name+'&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear,{offset: 0}, function(data){
		    $.fancybox.hideLoading();
		    
		    $('.record_detail table').html(data);
		    $('.record_detail').scrollTop(0);
		    
		});
		
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		return false;
	});
	
	
	$(function(){
		$('.sort_column').attr("class","sort_column");
		$('[name="<?=$sort?>"]').attr("class", "sort_column <?=$order?>");
	});
	
	$('.sort_column').click(function(){		
		$.fancybox.showLoading();
		
		var sort = $(this).attr('name');
		var order = $(this).attr('class').replace("sort_column", "").replace(" ", "");
		
		if(order == ""|| order == "sort_asc") order = "sort_dec";
		else order = "sort_asc";
		
		$("#Sort_Search").val(sort);
		$("#Order_Search").val(order);
		
		FromMonth = $("#FromMonth").val();
		FromYear = $("#FromYear").val();
		ToMonth = $("#ToMonth").val();
		ToYear = $("#ToYear").val();	
		
		$.get('ajax.php?action=getPublicClassSummaryRecord&year_class_id=<?=$year_class_id?>&student_id=<?=$student_id?>&class_name=<?=$class_name?>&FromMonth='+FromMonth+'&FromYear='+FromYear+'&ToMonth='+ToMonth+'&ToYear='+ToYear,{offset: 0, order : order, sort : sort}, function(data){	  
		    $.fancybox.hideLoading();
			$('.sort_column').attr("class","sort_column");
		    $('.record_detail table').html($("#pulic_student_summary_temp").html());
			 $('.record_detail table').append(data);	   
		    $('.record_detail').scrollTop(0);
		});
		
		return false;
	});
	
    </script>
</tr>
	<? foreach ($ebook_data['reading'] as $num=>$progress) : ?>
	    <tr>
		<td><?=$offset+$num+1?></td>	
		<td><?=$progress['user_link']?></td>
		<td><?=$progress['class_name']?></td>
		<td><?=$progress['class_number']?></td>
		<td><?=$progress['NumBooks']?></td>
		<td><?=$progress['NumBookRead']?></td>
		<td><?=$progress['NumBookReview']?></td>
		
	    </tr>
    <? endforeach; ?>
<? endif; ?>