<? if (!isset($portal_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
    <? if(!$portal_data['books']) : ?>No Record<? endif;?>
    <? foreach ($portal_data['books'] as $i=>$book) : ?>
	<? if ($book['image']): ?>
	    <li <?=$portal_data['showrank']&&$i==0?'class="rank_01"':''?>>
		<a class="book_cover">
		    <img src="<?=$book['image']?>"><span class="icon_ebook_01"></span>
		</a>
	<? else: ?>
	    <li class="no_cover <?=$portal_data['showrank']&&$i==0?'rank_01':''?>">
		<a class="book_cover">
		    <h1><?=$book['title']?></h1>
		    <em><?=$book['author']?></em>
		    <span class="icon_ebook_01"></span>
		</a>
	<? endif; ?>
	    
	    <? if ($portal_data['showrank']&&$i==0): ?>
	    <div class="most_rank_book_info">
		<h1><?=$book['title']?></h1>
		<h2><?=$book['author']?></h2>
		
		<? if ($portal_data['showrank']=='review'): ?>
		    <div class="star_s"><ul>
		    <? for ($j=0; $j<5; $j++) : ?>
				  <li <?=$j<$book['rating']? 'class="star_on"': ''?>>
		    <? endfor; ?>
		    </ul></div>
		    <p class="spacer"></p>
		    <div class="rank_no_of_review"><?=$book['review_count']?> reviews</div>
		<? else: ?>
		    <div class="rank_no_of_hit"><?=$book['hit_rate']?> hits</div>
		<? endif;?>
		
	    </div>
		
	    <? endif; ?>
	    
	    <? if ($portal_data['showrank']&&$i<3): ?>
		<div class="most_tag_<?=$i+1?>"></div>
	    <? endif; ?>
		
	    <div class="book_over_btn">eBook (PC+iPad)<a href="javascript:<?=$book['url']?>">Read Now</a><a class='fancybox' href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>">Book Detail</a></div>
	    <? if ($book['reason']): ?>
		<div class="most_recommend_reason">
		    
		    <h1><?=$book['title']?></h1>
		    <h2><?=$book['author']?></h2>
		   
		    <p class="spacer"></p>
		    
			<div class="most_recommend_reason_detail"><?=$book['reason']?></div>
		    
		    
		</div>
	    <? endif; ?>
	</li>
    
    <? endforeach; ?>