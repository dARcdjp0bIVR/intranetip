<? if (!isset($rank_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>

<script>

$(document).ready(function(){
    
    
    $('.ranking_user_list_log_open_shadow a, .no_of_review a').click(function(e){
        
	var target = $(this).attr('href');
	if ($(target).is(':hidden')){
	    $.fancybox.showLoading();
	    $.get('ajax.php?action=getMostActiveUserReviews',{user_id: $(this).find('.id').html(), offset: 0},function(data){
		$(target).fadeIn('slow').parent().addClass('ranking_user_list_log_open');
		$(target).find('ul').html(data).scrollTop(0);
		$.fancybox.hideLoading();
		
	    });
	}else{
	    $(target).fadeOut('slow').parent().removeClass('ranking_user_list_log_open'); 
	}
        
        return false;
    });
    
});
</script>
       
                        <div class="ranking_user_list">
                
                                <ul>
                   <!--rank title-->
                    <li class="ranking_user_list_log_title">
                    <div class="ranking_user_list_title">
                        <span><?=$eLib_plus["html"]["reviewer"]?></span><span><?=$eLib["html"]["num_of_review"]?></span><span><?=$eLib_plus["html"]["lastreview"]?></span>
                    </div>
                    </li>
                 <!--rank title end-->
                        <!-- rank 01-->

                <? foreach ($rank_data as $num=>$user) :?>
                    <li class="ranking_user_list_log">
                        <div class="ranking_user_list_rank"><span <?=$num<3? 'class="rank_0'.($num+1).'"' : ''?>><?=($num+1)?></span></div>
                        <div class="user_photo">
                            <span><img src="<?=$user['image']?>"></span>
                        </div>
                        <div class="ranking_user_list_detail">
                                <h2><?=$user['name']?> <span><?=$user['class']?></span></h2>
                                <div class="no_of_review"><a href="#rank_review_<?=$num+1?>"><?=$user['review_count']?> <?=$eLib["html"]["reviewsUnit"]?><span class="id" style="display:none;"><?=$user['user_id']?></span></a></div>
                                <div class="last_review"><a href="ajax.php?action=getBookDetail&book_id=<?=$user['reviews'][0]['id']?>" class="book_detail"><?=$user['reviews'][0]['title']?></a></div>
                        </div>
                        
            
                       <p class="spacer"></p>
                       <div class="ranking_user_review_container" id="rank_review_<?=$num+1?>" style="display:none"><ul class="ranking_user_review">
                       
                              <p class="spacer"></p>
                            
                        </ul>
                        <div class="ranking_user_list_log_open_shadow"><a href="#rank_review_<?=$num+1?>">Hide</a></div>
                                </div>
                        
                    </li>
                    <!-- rank 01 end -->
                    <? endforeach; ?>
      
                           </ul>
                   </div>

                
                  
                      
                      <p class="spacer"></p>
     

    