<? if (!isset($reviews_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>

$(document).ready(function(){

    $('.record_detail .review_remove').off('click').click(function(){
        return false;
        if (confirm('<?=$eLib_plus["html"]["areusuretoremovethereview"] ?>')){
            $.fancybox.showLoading();
            $.get($(this).attr('href'), function(data){
                    $('.record_detail #review_log_'+data).slideUp(function(){$(this).remove()});
                    $.fancybox.hideLoading();
            });
        }
       
        return false;
    });
    <? if (sizeof($reviews_data)>=$amount): ?>
    $('.record_detail').off('scroll').on('scroll',function(){
	
	if ($('.record_detail').prop('scrollHeight')-$('.record_detail').scrollTop()-210<=$('.record_detail').height()){
	    //$('.record_content_detail  .record_detail').off('scroll');
	    $('.record_detail').off('scroll');
	    $.fancybox.showLoading();
	    $.get('ajax.php',{action: 'getReviewRecord2','class_name': '<?=$class_name?>', 'student_id':'<?=$student_id?>', offset: '<?=$offset+$amount?>', FromMonth : '<?=$FromMonth?>', ToMonth : '<?=$ToMonth?>', FromYear : '<?=$FromYear?>', ToYear : '<?=$ToYear?>'}, function(data){
		
		$('.record_detail>ul').append(data);
		$.fancybox.hideLoading();
		 
	    });
	}
	
    });
    <? endif; ?>
});
</script>
<? if ($offset==0): ?>
<div class="user_review_list">
	<h1 style="margin-left:10px;background:url('')"><?=$eLib["html"]["total"].': '.$total.' '.$eLib["html"]["reviewsUnit"].$eLib["html"]["reviews"]?></h1>
        <div class="review_list record_detail">
        	<ul>
<? endif; ?>
		<? foreach ($reviews_data as $book):?>
            	<li class="review_log" id="review_log_<?=$book['review_id']?>">
                	<div class="review_book">
                    		<ul class="book_list book_list_small">
                    		
				<?=elibrary_plus::getBookListView(array($book)) ?>
                            
                      <!--<li class="no_cover"> <a href="#" class="book_cover"><h1>Bookname</h1><span>Author</span></a><a href="#" class="icon_ebook"><span>Read Now</span></a></li>-->
                     
                    		</ul>
                    </div>
                	<div class="review_detail">
                    	
                    	
			<div class="star_s"><ul>
			<? for ($i=0; $i<5; $i++) : ?>
				    <li <?=$i<$book['rating']? 'class="star_on"': ''?>>
			<? endfor; ?>

			</ul></div>
           			 <p class="spacer"></p>
          			 <div class="review_content"><?=$book['content']?></div>
          			 <p class="spacer"></p>
          			<div class="review_footer">
           				<span class="date_time" title="<?=$book['date']?>" ><?=elibrary_plus::getDaysWord($book['date'])?></span>
           				  <div class="tool_like" id="like_<?=$book['review_id']?>"><a class="icon_like"><?=$book['likes']?></a><span></span></div>
           		  </div>                
                </div>
            </li>
		<? endforeach; ?>
<? if ($offset==0): ?>                
            </ul>
        </div>
                        
</div>         
                              
<p class="spacer"></p>
<? endif; ?>