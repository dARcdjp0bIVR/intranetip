<? 
/*	Using: 
 * 
 *	Log
 *
 *	Date:	2016-01-13	(Cameron)
 *			- add customize flag $sys_custom['eLibraryPlus']['HideBookCategoryTags'] to hide book category tags (case #U91042)
 *
 *	Date:	2015-06-19 [Cameron]
 *			- change INTRANET_ELIB_BOOK.Category to include both Eng & Chi Name if BookFormat is physical 
 *			  and $sys_custom['eLibraryPlus']['BookCategorySyncTwoLang'] = true 	
 */
	if (!isset($book_catalog)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<?
        
    $tabs = ($intranet_session_language=='en')? array('eng','chi','others'): array('chi','eng','others');
    $selected_tab = 'tags';
   
    foreach ($tabs as $l){
	
	if ($book_catalog[$l]['count']>0){
	    $selected_tab = $l;
	    break;
	}
    }

?>

<script type="text/javascript">
$(document).ready(function() {
	
	$('#book_catalog .common_tab li a').click(function(){
       
	      var target=$(this).attr('href');
              $('.cat_list').hide();
              $(target).fadeIn();
	      
	      $(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
	      return false;
       });
	$('#book_catalog').mouseover(function(){$('.book_cat_filter_board').css({left:5, opacity:1});}).mouseout(function(){$('.book_cat_filter_board').css({left:-520, opacity:0});})
	$('#book_catalog .common_tab li.current_tab a').click();
});
</script>
<div class="book_cat_filter_board" style="opacity:0" id="book_catalog">
    <a href="./?page=category" class="btn_list_all"><?=$eLib_plus["html"]["listallbooks"]?></a>
    <div class="common_tab">
               <ul>
           <? foreach ($book_catalog as $type=>$book_language) : ?>
           
	       <? if ($book_language['categories']):?>
               <li <?=($type==$selected_tab) ? "class='current_tab'" : ""?>>
                  <a href="#<?=$type?>">
                         <span><?=$eLib_plus["html"][$type]?> (<?=$book_language['count']?>)</span>
                  </a>
               </li>
	       <? endif;?>
                                               
           <? endforeach; ?>
           
              
	    <? if (!$sys_custom['eLibraryPlus']['HideBookCategoryTags'] && count($book_catalog_tags)>0): ?>
	       <li <?=($selected_tab=='tags') ? "class='current_tab'" : ""?>>
                  <a href="#tags">
                         <span><?=$eLib_plus["html"]["tags_all_books"]?> (<?=count($book_catalog_tags)?>)</span>
                  </a>
               </li>
            <? endif;?>
               
           </ul>
    </div>
               <p class="spacer"></p>

    <? foreach ($book_catalog as $type=>$book_language) : ?>
          <div class="cat_list" id="<?=$type?>">
                   
           <? foreach ($book_language['categories'] as $cat) : ?>
           <div class="cat_item_list">
<?
			if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
				$catTitle = explode('-',$cat['title']);
				if (count($catTitle) > 2) {
					if ($intranet_session_language == 'en') {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[1]; 
					}	
					else {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
					}
				}
				else {
					$dispTitle = $cat['title'];
				}
			}
			else {
				$dispTitle = $cat['title'];
			}
?>
	    
	    <? if (!$cat['subcategories']):?>
		<ul>
		    <li><a href="./?page=category&language=<?=$type?>&category=<?=urlencode($cat['title'])?>"><?=$dispTitle.' ('.$cat['count'].')'?></a></li>
		    <p class="spacer"></p> 
		</ul>
		
	    <? else:?>
              <h1><a href="./?page=category&language=<?=$type?>&category=<?=urlencode($cat['title'])?>"><?=$dispTitle.' ('.$cat['count'].')'?></a></h1>
               <ul>
                   
                   <? foreach ($cat['subcategories'] as $subcategories) :?>
                   <li><a href="./?page=category&language=<?=$type?>&category=<?=urlencode($cat['title'])?>&subcategory=<?=urlencode($subcategories['title'])?>"><?=$subcategories['title']?> (<?=$subcategories['count']?>)</a></li>
                   <? endforeach; ?>
                
                    <p class="spacer"></p>    
               </ul>

	    <? endif; ?>
           </div>
           <? endforeach; ?>

       <p class="spacer"></p>
       
       </div>
       
     <? endforeach; ?>

<? if (!$sys_custom['eLibraryPlus']['HideBookCategoryTags']): ?>            
      <div class="cat_list" id="tags" <?=($selected_tab!='tags' || count($book_catalog_tags)<=0) ? "style='display:none;'" : ""?>>
                   
           <? foreach ($book_catalog_tags as $tag) : ?>
           <div class="cat_item_list">
              <ul>
                  <li><a href="./?page=tag&tag_id=<?=$tag['tag_id']?>"><?=$tag['title']?> (<?=$tag['count']?>)</a></h1></li>
                  <p class="spacer"></p>
              </ul>
           </div>
           <? endforeach; ?>

       <p class="spacer"></p>
       
       </div>
<? endif;?>

</div>
           
          