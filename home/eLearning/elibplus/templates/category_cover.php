<? if (!isset($books_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<? 
    $books_rows=array_chunk($books_data,10);
?>
<? if ($offset==0): ?>
<script>
$(function(){
   book_number = '<?=$total?>';	//2015-05-04 Charles Ma - P77849
   $('.book_result .total').html('<?=$total?>');
   $('.book_result').fadeIn();
});
</script>
    <? if (!$books_rows): ?>
    <?=$eLib["html"]["no_record"]?>
    <? endif; ?>
<? endif; ?>
<? foreach ($books_rows as $books_row) : ?>
<div class="bookshelf"><div class="bookshelf_right"><div class="bookshelf_bg"> 
            	
    <div class="bookshelf_content">
    
	<ul class="book_list book_list_small">
	    <?=elibrary_plus::getBookListView($books_row) ?>
	  
	
	</ul>

    </div>            
            
</div></div></div>

<p class="spacer"></p>
<? endforeach; ?>