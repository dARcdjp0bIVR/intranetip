<?php
/*
 * 	using:
 * 
 * 	Log
 * 
 * 	Date:	2015-04-28 [Cameron]
 * 			- create this file
 * 
 * 	Note: 	1. max number of records to export is 9999
 * 			2. only user who has eAdmin-LibraryMgmtSystem right can access this function
 * 
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$lelibplus = new elibrary_plus();


# Define Column Title
$exportColumn = array();

# get filter condition and prepare sql [start]
	$offset = $offset? $offset: 0;
	$amount = 9999;
	
    $searchDataPeriod 	= $_REQUEST['searchDataPeriod'];
    $searchFromDate 	= $_REQUEST['searchFromDate'];
    $searchToDate 		= $_REQUEST['searchToDate'];
    
    if ($searchDataPeriod == 'All') {	// don't apply date range filter if checked all
    	$searchFromDate = '';
    	$searchToDate	= '';
    }
     
# get filter condition and prepare sql [end]

switch($page)
{
	case "stats":
		
		$filename = "ebook_review_stats.csv";
		
		if($sort == "" || $sort == "sort_ls" ){
			$order_sql = " last_submitted ";
			$sort = "sort_ls";
		}else if($sort == "sort_bt"){
			$order_sql = " title ";
		}else if($sort == "sort_a"){
			$order_sql = " author ";
		}else if($sort == "sort_nor"){
			$order_sql = " num_review ";
		}
		
		if($order == "sort_dec" || $order == "" ){
			$order_sql .= " DESC ";
			$order = "sort_dec";
		}else{
			$order_sql .= " ";
			$order = "sort_asc";
		}
		
		$ebook_data['reading']=$lelibplus->getAllReviewsRecord($offset, $amount, $order_sql, $searchFromDate, $searchToDate);
		
		$exportColumn[0][] = $eLib["html"]["book_title"]; 
		$exportColumn[0][] = $eLib["html"]["author"];
		$exportColumn[0][] = $eLib["html"]["num_of_review"];
		$exportColumn[0][] = $eLib["html"]["last_submitted"];
			
		for ($i=0,$imax=count($ebook_data['reading']); $i<$imax; $i++)
		{
			$dataObj = $ebook_data['reading'][$i];
			$ExportArr[] = array(strip_tags($dataObj["title"]), strip_tags($dataObj["author"]), strip_tags($dataObj["num_review"]), $dataObj["last_submitted"]);
		}
		
		break;
		
	case "reviews":
		$filename = "ebook_reviews.csv";
			
		$order_sql = "b.Title, u.EnglishName";
		
		$ebook_data['reading']=$lelibplus->getAllReviewsDetails($offset, $amount, $order_sql, $searchFromDate, $searchToDate);
		
		$exportColumn[0][] = $eLib_plus["html"]["export"]["Title"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["Identity"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["UserLogin"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["EnglishName"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["ChineseName"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["ClassName"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["ClassNumber"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["Rating"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["Content"];
		$exportColumn[0][] = $eLib_plus["html"]["export"]["DateModified"];
		
		for ($i=0,$imax=count($ebook_data['reading']); $i<$imax; $i++)
		{
			$dataObj = $ebook_data['reading'][$i];
			$ExportArr[] = array(strip_tags($dataObj["Title"]), $dataObj["Identity"], $dataObj["UserLogin"], $dataObj["EnglishName"], $dataObj["ChineseName"], $dataObj["ClassName"], $dataObj["ClassNumber"], $dataObj["Rating"], strip_tags($dataObj["Content"]), $dataObj["DateModified"]);
		}
		
		break;
}

if (isset($ebook_data['reading'])) {
	unset($ebook_data['reading']);
}


$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");

?>