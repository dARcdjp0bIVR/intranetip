<?php
# using: 

/***************************************
 * Date: 	2013-10-08 (Yuen)
 * Details: Create this page
 ***************************************/

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");

intranet_auth();
intranet_opendb();

$laccessright = new libaccessright();

if(!$_SESSION['LIBMS']['admin']['current_right']['reports']['overdue report'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] ){	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;	
}

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$lelibplus = new elibrary_plus();


# Define Column Title
$exportColumn = array();

switch($page)
{
       
	case "stats":
		
		$offset = $offset? $offset: 0;
		$amount = 9999;
		
		$FromMonth = $FromMonth_Search;
		$ToMonth = $ToMonth_Search;
		$FromYear = $FromYear_Search;
		$ToYear = $ToYear_Search;
		$class_name = $ClassName_Search;
		
		if($FromMonth == NULL && $ToMonth== NULL  && $FromYear == NULL && $ToYear == NULL){
			if(is_string($FromMonth)){
				
			}else{
				$ThisYear = Date("Y")*1;$ThisMonth = Date("m")*1;
				$FromMonth = $ThisMonth;
				$ToMonth = $ThisMonth;
				$FromYear = $ThisYear;
				$ToYear = $ThisYear;
			}
		}
		
		if ($class_name == '')
		{
			
			$filename = "ebook_stats_by_class.csv";
		
			$order_sql = " class_name ";
			$order = "sort_asc";
			$sort = "sort_c";
			$ebook_data['reading']=$lelibplus->getPublicClassSummary($offset, $amount, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);					
			
			$exportColumn[0][] = $eLib["html"]["class"];
			$exportColumn[0][] = $eLib["html"]["num_of_students"];
			$exportColumn[0][] = $eLib["html"]['Total']."(".$eLib["html"]["num_of_book_hit"].")";
			$exportColumn[0][] = $eLib["html"]["Average"]."(".$eLib["html"]["num_of_book_hit"].")";
			$exportColumn[0][] = $eLib["html"]['Total']."(".$eLib["html"]["num_of_review"].")";
			$exportColumn[0][] = $eLib["html"]["Average"]."(".$eLib["html"]["num_of_review"].")";
			
			for ($i=0; $i<sizeof($ebook_data['reading']); $i++)
			{
				$dataObj = $ebook_data['reading'][$i];
				$ExportArr[] = array(strip_tags($dataObj["class_link"]), $dataObj["num_student"], $dataObj["NumBookRead"], $dataObj["read_average"], $dataObj["NumBookReview"], $dataObj["review_average"]);
			}
			
		} elseif ($student_id == '')
		{
			$filename = "ebook_stats_by_student.csv";
			
			$order_sql = " class_number ";
			$order = "sort_asc";
			$sort = "sort_sn";			
			$ebook_data['reading']=$lelibplus->getPublicStudentSummary($offset, $amount,$class_name, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);
			
			$exportColumn[0][] = $eLib["html"]["student_name"];
			$exportColumn[0][] = $eLib["html"]["class"];
			$exportColumn[0][] = $eLib["html"]["class_number"];
			$exportColumn[0][] = $eLib["html"]["num_of_book_read"];
			$exportColumn[0][] = $eLib["html"]["num_of_book_hit"];
			$exportColumn[0][] = $eLib["html"]["num_of_review"];

			for ($i=0; $i<sizeof($ebook_data['reading']); $i++)
			{
				$dataObj = $ebook_data['reading'][$i];
				$ExportArr[] = array(strip_tags($dataObj["user_link"]), $dataObj["class_name"], $dataObj["class_number"], strip_tags($dataObj["NumBooks"]), strip_tags($dataObj["NumBookRead"]), strip_tags($dataObj["NumBookReview"]));
			}
		}else
		{
			$filename = "ebook_stats_of_a_student.csv";
			
			$order_sql = " title ";
			$order = "sort_asc";
			$sort = "sort_bt";
			$ebook_data['reading']=$lelibplus->getPublicStudentParticularSummary($offset, $amount,$student_id,$class_name, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);	
			//debug_r($ebook_data['reading']);
			
			$exportColumn[0][] = $eLib["html"]["book_title"];
			$exportColumn[0][] = $eLib["html"]["author"];
			$exportColumn[0][] = $eLib["html"]["Progress"];
			$exportColumn[0][] = $eLib["html"]["ReadTimes"];
			$exportColumn[0][] = $eLib["html"]["last_read"];

			for ($i=0; $i<sizeof($ebook_data['reading']); $i++)
			{
				$dataObj = $ebook_data['reading'][$i];
				$ExportArr[] = array(strip_tags($dataObj["title"]), $dataObj["author"], $dataObj["percentage"], strip_tags($dataObj["read_times"]), $dataObj["last_accessed"]);
			}
		}
		unset($ebook_data['reading']);
	break;
}


$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output The File To User Browser
$lexport->EXPORT_FILE($filename, $export_content, false, false, "UTF-8");

?>