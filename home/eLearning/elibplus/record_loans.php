<? if (!isset($loan_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}?>
<script>
$(document).ready(function(){
	
	$('.record_tab li a').click(function(){
		
		var target=$(this).attr('href');
		$(target).siblings().hide();
		$(target).fadeIn();
		$(this).parent().addClass('current_tab').siblings().removeClass('current_tab');
		
		return false;
	});
	
	$('.delete_dim').click(function(){
                if (confirm('<?=$eLib_plus["html"]["areusuretocancelthereservation"]?>')){
			$.fancybox.showLoading();
			$('.record_content_detail').slideUp();
			
			$.get($(this).attr('href'), function(data){
						
			      $.get('ajax.php',{action: 'getLoanRecord'}, function(data){
				  $('.record_content_detail').html(data).slideDown();
				  $.fancybox.hideLoading();
			      });
			});

		}
		return false;
	});
	
	$('.renew_form').submit(function(){
			
		if (confirm('<?=$eLib_plus["html"]["areusuretorenewthebook"] ?>')){
			$.fancybox.showLoading();
			$('.record_content_detail').slideUp();
			
			$.get('ajax.php', $(this).serialize(),function(data){
				$.fancybox.hideLoading();
				if (data<=0){
					alert('<?=$eLib_plus["html"]["failedtorenew"] ?>');
					$('.record_content_detail').slideDown();
					$.fancybox.hideLoading();
				}else{
					$.get('ajax.php',{action: 'getLoanRecord'}, function(data){
						$('.record_content_detail').html(data).slideDown();
						$.fancybox.hideLoading();
			                });
				}
				
			});
		}
		return false;

	});
});
</script>

           		<div class="record_tab">
                        	<ul>
                            	<li class="current_tab"><a href="#loan_current"><span><?=$eLib_plus["html"]["currentstatus"]?></span></a></li>
                                <li><a href="#loan_record"><span><?=$eLib_plus["html"]["loanrecord"]?></span></a></li>
                                <li><a href="#loan_penalty"><span><?=$eLib_plus["html"]["penaltyrecord"]?></span></a></li>
                            </ul>
                </div>
                <div class="record_detail">
			<div id="loan_current">
                              <div class="table_board">
                              	<h1 class="record_loan_type_01"><?=$eLib_plus["html"]["checkedout"]?></h1>
                              	
				<table class="common_table_list">
							<colgroup><col nowrap="nowrap">
							</colgroup><thead>
								<tr>
								  <th class="num_check">#</th>
								  <th><?=$eLib["html"]["book_title"]?></th>
								  <th><?=$eLib_plus["html"]["checkedoutdate"]?></th>
								  <th><?=$eLib_plus["html"]["duedate"]?></th>
								  <th><?=$eLib_plus["html"]["renewcount"]?></th>
								  <th><?=$eLib_plus["html"]["status"]?></th>
							  </tr>
							<? if (!$loan_data['loan_books']): ?>
							    <tr><td colspan="6"><?=$eLib_plus["html"]["norecord"]?></td></tr>
							<? else: ?>
							<? foreach ($loan_data['loan_books'] as $num=>$book): ?>
							    <tr <?=$book['overdue_days']>0? 'class="alert_row"' : ''?>>
								    <td><?=$num+1?></td>
								    <td><a rel="record_loan" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail"><?=$book['title']?></a></td>
								    <td><?=$book['borrow_date']?></td>
								    <td><?=$book['due_date']?></td>
								    <td><?=$book['renew_count']?></td>
								    <td>
									  <? if ($book['overdue_days']>0) :?>
									  <span class="alert_late"><?=$eLib_plus["html"]["overdue"].' '.$book['overdue_days'].' '.$eLib_plus["html"]["days"] ?></span>
									  <? elseif ($permission['renew']): ?>
									  <form class="renew_form" method="get">
										  <input type="hidden" name="action" value="renewBook"/>
										  <input type="hidden" name="borrow_id" value="<?=$book['borrow_id']?>"/>
										  <input type="submit" value="<?=$eLib_plus["html"]["renew"]?>" class="set_borrow" name="submit2">
									  </form>
									  <? endif; ?>
								    </td>
							    </tr>
							<? endforeach; ?>
							<? endif; ?>	
							</thead>
							<tbody>
							</tbody>
						</table>
				    
                        		&nbsp;
                              </div>
                            <? if ($loan_data['reserved_books']): ?>
                              <div class="table_board">
                              	<h1 class="record_loan_type_02"><?=$eLib_plus["html"]["requesthold"] ?></h1>
				
                              	<table class="common_table_list">
							<colgroup><col nowrap="nowrap">
							</colgroup><thead>
								<tr>
								  <th class="num_check">#</th>
								  <th><?=$eLib["html"]["book_title"]?></th>
								  <th><?=$eLib_plus["html"]["requesteddate"]?></th>
								  
								  <th><?=$eLib_plus["html"]["status"]?></th>
								  <th>&nbsp;</th>
							  </tr>
							  </thead>
							  <tbody>
							    <? if (!$loan_data['reserved_books']): ?>
							    <tr><td colspan="5"><?=$eLib_plus["html"]["norecord"]?></td></tr>
							    <? else: ?>
								<? foreach ($loan_data['reserved_books'] as $num=>$book): ?>
								<tr <?=$book['status']=='READY'? 'class="avaliable_row"' :''?>>
									<td><?=$num+1?></td>
									<td><a rel="record_reserved" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail"><?=$book['title']?></a></td>
									<td><?=$book['reserve_date']?></td>
									<? if ($book['status']=='READY'): ?>
									<td><span class="alert_avaliable"><?=$eLib_plus["html"]["readyforpickup"]?></span></td>
									<? else: ?>
									<td><?=$eLib_plus["html"]["notreturnedyet"]?></td>
									<? endif; ?>
									<td><div class="table_row_tool">
									<a title="Remove" class="delete_dim" href="ajax.php?action=handleReservation&book_id=<?=$book['id']?>"></a>
									</div></td>
      
								</tr>
								<? endforeach; ?>
							    <? endif; ?>								
							</tbody>
						</table>
				
                        		&nbsp;
                              </div>
			    <? endif; ?>
                              <div class="table_board">
                              	<h1 class="record_loan_type_03"><?=$eLib_plus["html"]["outstandingpenalty"] ?></h1>
				
						<table class="common_table_list">
							<colgroup><col nowrap="nowrap">
							</colgroup><thead>
								<tr>
								  <th class="num_check">#</th>
								  <th><?=$eLib["html"]["book_title"]?></th>
								  <th><?=$eLib_plus["html"]["duedate"]?></th>
								  <th><?=$eLib_plus["html"]["returndate"] ?></th>
								  <th><?=$eLib_plus["html"]["reason"] ?></th>
								  <th><?=$eLib_plus["html"]["totalpenalty"] ?></th>
								  <th><?=$eLib_plus["html"]["outstandingpenalty"] ?></th>
							</thead>
							<? if (!$loan_data['penalty_current']): ?>
							    <tr><td colspan="7"><?=$eLib_plus["html"]["norecord"]?></td></tr>
							<? else: ?>
							    <? foreach ($loan_data['penalty_current'] as $num=>$book): ?>
							      </tr>
								    <tr>
								      <td><?=$num+1?></td>
								      <td><a rel="record_penalty_current" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail"><?=$book['title']?></a></td>
								      <td><?=$book['due_date']?></td>
								      <td><?=$book['return_date']?></td>
								      <td><?=$book['penalty_reason']=='LOST' ? $eLib_plus["html"]["booklost"] : $eLib_plus["html"]["overdue"].' '.$book['penalty_reason'].' '.$eLib_plus["html"]["days"]?></td>
								      <td>$<?=$book['penalty_total']?></td>
								      <td><span class="alert_text">$<?=$book['penalty_owing']?></span></td>
							      </tr>
							    <? endforeach; ?>
							<? endif; ?>							
							<tbody>
							</tbody>
						</table>
                        		&nbsp;
				    
			     </div>
                              
			</div>
			
			<div id="loan_record" style='display:none'>
			    
						<table class="common_table_list">
							<colgroup><col nowrap="nowrap">
							</colgroup><thead>
								<tr>
								  <th class="num_check">#</th>
								  <th><?=$eLib["html"]["book_title"]?></th>
								  <th><?=$eLib_plus["html"]["checkedoutdate"]?></th>
								  <th><?=$eLib_plus["html"]["returndate"]?></th>
							</thead>
							<? if (!$loan_data['loan_history']): ?>
							    <tr><td colspan="4"><?=$eLib_plus["html"]["norecord"]?></td></tr>
							<? else: ?>
							    <? foreach ($loan_data['loan_history'] as $num=>$book): ?>
							      </tr>
								    <tr>
								      <td><?=$num+1?></td>
								      <td><a rel="record_loan_history" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail"><?=$book['title']?></a></td>
								      <td><?=$book['borrow_date']?></td>
								      <td><?=$book['return_date']?></td>
							      </tr>
							    <? endforeach; ?>
							<? endif; ?>								
							<tbody>
							</tbody>
						</table>
			    	
			</div>
			<div id="loan_penalty" style='display:none'>
			    
						<table class="common_table_list">
							<colgroup><col nowrap="nowrap">
							</colgroup><thead>
								<tr>
								  <th class="num_check">#</th>
								  <th><?=$eLib["html"]["book_title"]?></th>
								  <th><?=$eLib_plus["html"]["duedate"]?></th>
								  <th><?=$eLib_plus["html"]["returndate"]?></th>
								  <th><?=$eLib_plus["html"]["reason"]?></th>
								  <th><?=$eLib_plus["html"]["totalpenalty"]?></th>
							</thead>
							<? if (!$loan_data['penalty_history']): ?>
							    <tr><td colspan="6"><?=$eLib_plus["html"]["norecord"]?></td></tr>
							<? else: ?>
							    <? foreach ($loan_data['penalty_history'] as $num=>$book): ?>
							      </tr>
								    <tr>
								      <td><?=$num+1?></td>
								      <td><a rel="record_penalty_history" href="ajax.php?action=getBookDetail&book_id=<?=$book['id']?>" class="book_title book_detail"><?=$book['title']?></a></td>
								      <td><?=$book['due_date']?></td>
								      <td><?=$book['return_date']?></td>
								      <td><?=$book['penalty_reason']=='LOST' ? $eLib_plus["html"]["booklost"] : $eLib_plus["html"]["overdue"].' '.$book['penalty_reason'].' '.$eLib_plus["html"]["days"]?></td>
								      <td>$<?=$book['penalty_total']?></td>
							      </tr>
							    <? endforeach; ?>
							<? endif; ?>									
							<tbody>
							</tbody>
						</table>
			   		
						
			</div>
                <p class="spacer"></p>
                
              