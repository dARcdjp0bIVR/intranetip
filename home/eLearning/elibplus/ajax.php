<?php
/*
 * Editing by       
 *
 * Modification Log:
 *
 * 2016-11-01 (Cameron)
 * 		- change attachment path from /file/elibrary to /file/elibplus2 to fix the problem of unable to access attachment after
 * 		change of mapping from /file/elibrary to /home/eclassprogram/elibrary for cloud site [case #W106756]
 * 2016-09-27 (Cameron)
 * 		- show rules in html format in getPortalNotices()
 *
 * 2016-07-20 (Cameron)
 *		- replace session_register with session_register_intranet to support php5.4+
 *
 * 2015-11-10 (Cameron)
 * 		- modify the display order of recommended books according to the setting in preference_recommend_book_order
 *  
 * 2015-09-22 (Cameron)
 * 		- add normalizeFormParameter() function to $param in getBookList
 * 
 * 2015-07-30 (Cameron) case ref# 2015-0713-1520-21170
 * 		- add $offset and $amount to action getPortalNotices and getPortalNoticeForm 
 * 
 * 2015-05-04 (Charles Ma) [ip.2.5.6.5.1]
 * 		 - P77849
 * 2015-04-28 (Cameron)
 * 		- add $linterface for case getAllReviews
 * 		- add arguments $searchFromDate and $searchToDate to getAllReviewsRecord()
 * 
 * 2015-02-11 (Henry)
 * 		- add subtitle to getBookList()
 * 2014-11-04 (Charles Ma)	[ip.2.5.5.12.1]
 * 		- extend the limit to 100 : 20141007-P64683
 * 20150210 Ryan - Fix Class Number Ordering 
 */
 
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
if (!$UserID) die('<script>location.href = "/";</script>');
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened']) 	die('Error: eLibrary Plus Closed');

if($range == "" && ($action == "getBestRateBooks" || $action == "getMostHitBooks" || $action == "getMostLoanBooks" 
|| $action == "getMostBorrowUsers" || $action == "getMostActiveUsers" || $action == "getMostUsefulReviews")){
	$range = "accumulated";
}

$lelibplus = new elibrary_plus($book_id, $UserID, $ck_eLib_bookType, $range);
$linterface = new interface_html("elibplus.html");

$request_ts = time();
switch($action){
       
	case "setBookType":
		
		$ck_eLib_bookType = $book_type;
		session_register_intranet('ck_eLib_bookType', $ck_eLib_bookType);
		
	break;
    
	case "getThemeSettings":
	    
		$theme = $lelibplus->getUserTheme();
		include_once("templates/themesettings.php");
		
	break;
    
	case 'setTheme':
	    
		$lelibplus->setUserTheme($background_theme, $bookshelf_theme);
	break;
    
	case "getBookDetail":
		
		$detail_data=$lelibplus->getBookDetail();

		$book_item = elibrary_plus::getBookListView(array($detail_data));
		
                if ($permission['admin']){
                    $detail_data['recommend']=$lelibplus->getRecommendedClassLevels();
                }
                
		include_once("templates/detail.php");
		
	break;
    
	case 'getReviews':
		
		$offset = $offset? $offset: 0;
		$amount = $offset==0?10:5;
		
		$reviews_data=$lelibplus->getBookReviews($offset, $amount);
		
		include_once("templates/detail_reviews.php");
		
	break;
    
	case 'getStat':
		
		$stat_data = $lelibplus->getBookStat();
		
		include_once("templates/detail_stat.php");
		
	break;
    
	case 'handleFavourite':
		
		echo $lelibplus->addOrRemoveFavourite();

	break;
    
        case 'handleReservation':
		
		echo $lelibplus->addOrRemoveReservation();
		
	break;
    
        case 'submitRecommend':

                if ($permission['admin']){
                    $lelibplus->addOrEditRecommend($class_level_ids, $content, $mode);
                }
	
	break;
    
        case 'removeRecommend':

                if ($permission['admin']){
                    $lelibplus->removeRecommend();
                }
	
	break;
    
	case 'submitReview':
	
                echo $lelibplus->addReview($content, $rate);
	
	break;
    
	case 'removeReview':
		$lelibplus->removeReviews($review_id);
		echo $review_id;
	break;
    
    case 'removeAllReview':	// 		2013-11-14-deleteallbookcomment
		$lelibplus->removeAllReviews($book_id);
	break;
    
	case 'handleLike':
	
		$isAdd = $lelibplus->likeOrUnlikeReview($review_id);
		$votes = $lelibplus->getHelpfulVote($review_id);
		$likes = $votes['yesNum'];
		
		$likeOrUnlike=($isAdd)? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"];
		
		echo $review_id.','.$likes.','.$likeOrUnlike;
	
	break;
    
        case 'renewBook':
                echo $lelibplus->renewBook($borrow_id);
                
        break;
    
	case 'getBookList':
                
		$sortby = empty($sortby)? 'random' : $sortby;
		
		if (!in_array($order,array('asc','desc'))){
		    $order = in_array($sortby, array('hitrate','rating'))? 'desc' :'asc';
		}
		
		$param = array(
		    
		    'language'		=> $language,
		    'category'		=> normalizeFormParameter($category),
		    'subcategory'	=> normalizeFormParameter($subcategory),
		    'title'			=> normalizeFormParameter($title),
		    'subtitle'		=> normalizeFormParameter($subtitle),
		    'author'		=> normalizeFormParameter($author),
		    'level'			=> normalizeFormParameter($level),
		    'keyword'		=> normalizeFormParameter($keyword),
		    'tag_id'		=> normalizeFormParameter($tag_id)
		    
		);
		if ($type=='list'){
                        
		    list($total, $books_data)=$lelibplus->getBookList($param, $offset, $amount, $sortby, $order);
		    
		    include_once("templates/category_list.php");
			
		}else{
			
		    list($total, $books_data)=$lelibplus->getBookList($param, $offset, $amount, $sortby, $order);

		    include_once("templates/category_cover.php");
			
		}
		
	break;
    
        case 'getRecommendList':
            
            $sortby	= '';
            $order	= '';
            
            list($total, $books_data)=$lelibplus->getRecommendBooks($offset,$amount); //30 //2015-05-04 Charles Ma - P77849
            if (!empty($books_data)){
            
                if ($type == 'list'){
                    include_once("templates/category_list.php");
                }else{
                    
                    include_once("templates/category_cover.php");
                }
            }else{
                 echo $eLib["html"]["no_recommend_book"];
            }
            
            
        break;
    
        case 'getNewList':
            
            $sortby	= '';
            $order	= '';
            
            list($total, $books_data)=$lelibplus->getNewBooks(30);
            if (!empty($books_data) && !$offset){
            
                if ($type == 'list'){
                    include_once("templates/category_list.php");
                }else{
                    
                    include_once("templates/category_cover.php");
                }
            }
 
        break;
    
	case 'getLoanRecord':
		
    		
                $loan_data['reserved_books']	= $lelibplus->getUserReservedBooks();
                $loan_data['loan_books']	= $lelibplus->getUserLoanBooksCurrent(0,1000); // 20141104-T71057
                $loan_data['loan_history']	= $lelibplus->getUserLoanBooksHistory();
                $loan_data['penalty_current']	= $lelibplus->getUserPenaltyCurrent();
                $loan_data['penalty_history']	= $lelibplus->getUserPenaltyHistory();
                $loan_data['lost_books']	= $lelibplus->getUserLostBooksHistory();	//20140509-lost-book-list
                 
		include_once("templates/record_loans.php");
	break;
    
	case 'geteBookRecord':
		
		$ebook_data = true;
		
		include_once("templates/record_ebooks.php");
	break;
    
	case 'geteBookReadRecord':
	    
		$offset = $offset? $offset: 0;
		$amount = $offset==0?20:10;
		
		$ebook_data['reading']=$lelibplus->getReadingProgress($offset, $amount);
		
		include_once("templates/record_ebooks_read.php");
	break;
    
	case 'geteBookNotesRecord':
	    
		$offset = $offset? $offset: 0;
		$amount = $offset==0?20:10;
		
		$ebook_data['notes']=$lelibplus->getUserNotes($offset, $amount);
		
		include_once("templates/record_ebooks_notes.php");
	break;
    
	case 'getFavouriteRecord':
		
		$books_data = $lelibplus->getUserFavourites();
		
		include_once("templates/record_favourites.php");

	break;
    
	case 'getReviewRecord':
	    
	    	$offset = $offset? $offset: 0;
		$amount = $offset==0? 10: 5;
		
		list($total, $reviews_data)=$lelibplus->getUserReviews($user_id, $class_name, $offset, $amount);
		
		if ($reviews_data) include_once("templates/record_reviews.php");
	break;
    
	case 'getPortalBestRate':
				
		echo elibrary_plus::getBookListView($lelibplus->getBestRateBooks(3),'review');
		
	break;
    
	case 'getPortalMostHitLoan':
				
		if ($type=='MostLoanBooks'){
		    echo elibrary_plus::getBookListView($lelibplus->getMostLoanBooks(3),'hitrate');
		}else{
		    echo elibrary_plus::getBookListView($lelibplus->getMostHitBooks(3),'hitrate');
		}		
		
	break;
    
    case 'getPortalRecommend':
	    
        if ($type=='new'){
	    	list($total, $books_data) = $lelibplus->getNewBooks(5);
        }else{
           	$libms = new liblms();
			$shuffle = $libms->get_system_setting("preference_recommend_book_order") ? false : true;	// default uses random                 	
	    	list($total, $books_data) = $lelibplus->getRecommendBooks(0,5,$shuffle);
        }
                
		echo elibrary_plus::getBookListView($books_data);
		
        break;
    
	case 'getPortalRank':
	    
        if ($type=='MostBorrowUsers'){
	    
            $portal_data['users'] = $lelibplus->getMostBorrowUsers(5);
	    	
	    	$template = "templates/portal_userlist.php";
	    
        }else if ($type=='MostActiveUsers'){
	    
            $portal_data['users'] = $lelibplus->getMostActiveUsers(5);
	    
	   		$template = "templates/portal_userlist.php";
	    
        }else{
	    
		    $portal_data['reviews'] = $lelibplus->getMostUsefulReviews(5);
		    
		    $template = "templates/portal_reviewlist.php";
		    
		}
               
        include_once($template);
        break;
	
	case "uploadPortalNoticeAttachment":
	    
		if (!$permission['admin']) die('Permission deined');
		
		$fs = new libfilesystem();
				
		$file 		= $_FILES['attachment'];
		$file_name	= str_replace(array("#"," ","'",'"',"&","?"),"_",$file['name']);
		$path = $sys_custom['eLibraryPlus']['CloudServer'] ? '/file/elibplus2/tmp/' : '/file/elibrary/tmp/'; 
		$temp_dir 	= $intranet_root.$path;
		$temp_path 	= $intranet_root.$path.$file_name;
		$temp_url 	= $path.urlencode($file_name);
		
		$fs->createFolder($temp_dir);
		
		move_uploaded_file($file["tmp_name"], $temp_path);
		
		if ($callback){
		    echo "<script>$callback('".$temp_url."', '".$file_name."');</script>";
		}
	break;
    
	case "removePortalNoticeAttachment":
		
		if (!$permission['admin']) die();
		
		if ($temp_file_path){
		    
		    $fs = new libfilesystem();
		    $fs->file_remove($intranet_root.$temp_file_path);
		    
		}
		
	break;
    
	case 'setPortalNotice':
	    	
		if (!$permission['admin']) die();
		$fs = new libfilesystem();
		    
		if ($id){
		    $notice = current($lelibplus->getPortalNotices($type, $id, 0, 1));
		
		    $attachment = $notice['attachment'];
		}
		
		if ($file_temp_path){
		    
		    $file_temp_path 	= urldecode($file_temp_path);
		    if (!$delete_attachment){
			
			$file_name 		= $fs->get_file_basename($file_temp_path);
	    
			$current_time 	= time();
			$path = $sys_custom['eLibraryPlus']['CloudServer'] ? '/file/elibplus2/attachments/' : '/file/elibrary/attachments/';
			$attachment_dir 	= $intranet_root.$path.$current_time.'/';
			$attachment_url	= $path.$current_time.'/'.$file_name;
    
			$fs->createFolder($attachment_dir);
			$fs->file_copy($intranet_root.$file_temp_path, $attachment_dir);
		    }
		    $fs->file_remove($file_temp_path);//force remove attachment
		    
		}
		
		if ($attachment && ($file_temp_path || $delete_attachment)){
			
			$fs->file_remove($intranet_root.$attachment);
			$fs->folder_remove($intranet_root.dirname($attachment));
			$old_attachment_exists = true;
			
			if($delete_attachment){//force remove attachment
			    echo $lelibplus->removePortalNoticeAttachments($id);
			}
		}
	  
		$id = $lelibplus->setPortalNotice($type, $id, array('title'=>$title, 'content'=>$content, 'attachment'=> $attachment_url, 
						'priority' => $priority, 'date_start'=>$date_start, 'date_end' =>$date_end), $old_attachment_exists);
	    
	    
	break;
    
	case 'getPortalNotices':
	    
		if ($type=="rules"){
			$offset = 0;
			$amount = 1;
		    $rules = current($lelibplus->getPortalNotices('rules', false, $offset, $amount));
		    
//		    echo '<div>'.nl2br(htmlspecialchars($rules['content'])).'</div>';
		    echo '<div>'.nl2br(intranet_undo_htmlspecialchars($rules['content'])).'</div>';
		    
		    if ($rules['attachment']){
			echo '<span class="attachment">'.$eLib_plus["html"]["attachment"].': <a target="_blank" href="'.$rules['attachment'].'">'.$rules['attachment_name'].'</a>';
		    }
		   
		}else{
		    
		    $amount = $offset == 0 ? 5: 3;
		    
		    $portal_data['news'] = $lelibplus->getPortalNotices('news', false, $offset, $amount,true);
		    
		    if ($portal_data['news']){
				include_once("templates/portal_news.php");
		    }
		}
			
	break;
    
	case 'getPortalNoticeForm':
	    
		if ($type=="rules"){
			$offset = 0;
			$amount = 1;
		    $portal_data['notice'] = current($lelibplus->getPortalNotices('rules', false, $offset, $amount));
		    $portal_data['header'] = $eLib_plus["html"]["rules"];

		   
		}else if ($id){
		    
		    $portal_data['notice'] = current($lelibplus->getPortalNotices('news', $id));
		    $portal_data['header'] = $eLib_plus["html"]["news"];

		}else{
		    
		    $portal_data=array(true);
		    $portal_data['header'] = $eLib_plus["html"]["news"].'<span>'.$eLib_plus["html"]["add"].'</span>';
		}
		
		include_once("templates/portal_notice_form.php");
			
	break;
    
	case 'removePortalNotice':
	    
		if (!$permission['admin']) die();
		
		if (!$permission['admin']) die();
		
		$notice = current($lelibplus->getPortalNotices(false, $id, 0, 1));
	    
		$attachment = $notice['attachment'];
		
		$fs = new libfilesystem();
		$fs->file_remove($intranet_root.$attachment);
		$fs->folder_remove($intranet_root.dirname($attachment));//will not remove it if still having sth inside
		    
		$lelibplus->removePortalNotice($id);		
	    
	break;
    
	case 'getBestRateBooks':
		$rank_data	= $lelibplus->getBestRateBooks(10);
                $rank_title	= $eLib_plus["html"]["bestrate"];
		
		include_once("templates/rank_books.php");
		
	break;
    
	case 'getMostHitBooks':
	    
		$rank_data	= $lelibplus->getMostHitBooks(10);
                $rank_title	= $eLib_plus["html"]["mosthit"];
		
		include_once("templates/rank_books.php");
		
	break;
    
	case 'getMostLoanBooks':
	    
		$rank_data	= $lelibplus->getMostLoanBooks(10);
                $rank_title	= $eLib_plus["html"]["mostloan"];
		
		include_once("templates/rank_books.php");
	break;
    
	case 'getMostBorrowUsers':
	    
		$rank_data	= $lelibplus->getMostBorrowUsers(5);
		
		include_once("templates/rank_borrowusers.php");
		
	break;
    
	case 'getMostActiveClasses':
	    
		$rank_data	= $lelibplus->getMostActiveClassesWithReviews(5);
		
		include_once("templates/rank_activeclasses.php");
		
	break;
    
	case 'getMostActiveUsers':
	    
		$rank_users	= $lelibplus->getMostActiveUsers();
		$rank_data 	= array();
		foreach ($rank_users as $i=>$user){
		    list(,$user['reviews'])=$lelibplus->getUserReviews($user['user_id'], false, 0, 1);
		    $rank_data[] = $user;
		}
		
		include_once("templates/rank_activeusers.php");
	break;
    
	case 'getMostActiveUserReviews':
	    
		$amount = $offset==0?10:5;
		
		list(,$rank_data)=$lelibplus->getUserReviews($user_id, false, $offset, $amount);
		
		if ($rank_data) include_once("templates/rank_activeusers_reviews.php");
	break;
    
	case 'getMostUsefulReviews':
	    
		$rank_data=$lelibplus->getMostUsefulReviews(5);
		
		include_once("templates/rank_reviews.php");
		
	break;
	
	///// Charles Ma 2013-09-30
	case 'getAllReviews':
		$allreviews = true;
		include_once("templates/stats_allreviews.php");		
	break;
	
	case 'getAllReviewsRecord':
	
	    $searchDataPeriod 	= $_REQUEST['searchDataPeriod'];
	    $searchFromDate 	= $_REQUEST['searchFromDate'];
	    $searchToDate 		= $_REQUEST['searchToDate'];
	    
	    if ($searchDataPeriod == 'All') {	// don't apply date range filter if checked all
	    	$searchFromDate = '';
	    	$searchToDate	= '';
	    } 
		$offset = $offset? $offset: 0;
		$amount = $offset==0?20:10;
		
		if($sort == "" || $sort == "sort_ls" ){
			$order_sql = " last_submitted ";
			$sort = "sort_ls";
		}else if($sort == "sort_bt"){
			$order_sql = " title ";
		}else if($sort == "sort_a"){
			$order_sql = " author ";
		}else if($sort == "sort_nor"){
			$order_sql = " num_review ";
		}
		
		if($order == "sort_dec" || $order == "" ){
			$order_sql .= " DESC ";
			$order = "sort_dec";
		}else{
			$order_sql .= " ";
			$order = "sort_asc";
		}
		
		$ebook_data['reading']=$lelibplus->getAllReviewsRecord($offset, $amount, $order_sql, $searchFromDate, $searchToDate);
		
		include_once("templates/stats_allreviews_record.php");
	break;
	
	case 'getPublicClassSummary':
		$publicclasssummary = true;
		$filter_html = $lelibplus->retrieve_filter($FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear);	
		include_once("templates/stats_publicclasssummary.php");		
	break;
	
	case 'getPublicClassSummaryRecord':
	    
		$offset = $offset? $offset: 0;
		$amount = $offset==0?20:10;

		if($class_name == ''){
			if($sort == "" || $sort == "sort_c" ){
				$order_sql = " class_name ";
				if($sort == ""){
					$order = "sort_asc";
					$sort = "sort_c";
				}
			}else if($sort == "sort_nos"){
				$order_sql = " num_student ";
			}else if($sort == "sort_tnobr"){
				$order_sql = " NumBookRead ";
			}else if($sort == "sort_anobr"){
				$order_sql = " read_average ";
			}else if($sort == "sort_tnor"){
				$order_sql = " NumBookReview ";
			}else if($sort == "sort_anor"){
				$order_sql = " review_average ";
			}
		}
		else if($student_id == '') {
			
			if($sort == "" ){
				$order_sql = " class_number+0 ";
				if($sort == ""){
					$order = "sort_asc";
					$sort = "sort_cn";
				}
			}else if($sort == "sort_c"){
				$order_sql = " class_name ";
			}else if($sort == "sort_sn"){
				$order_sql = " student_name ";
			}else if($sort == "sort_cn"){
				$order_sql = " class_number+0 ";
			}else if($sort == "sort_nobr"){
				$order_sql = " NumBookRead ";
			}else if($sort == "sort_hit"){
				$order_sql = " NumBooks ";
			}else if($sort == "sort_nor"){
				$order_sql = " NumBookReview ";
			}
		}else{
			if($sort == "" || $sort == "sort_bt" ){
				$order_sql = " title ";
				if($sort == ""){
					$order = "sort_asc";
					$sort = "sort_bt";
				}
			}else if($sort == "sort_a"){
				$order_sql = " author ";
			}else if($sort == "sort_p"){
				$order_sql = " ppercentage ";
			}else if($sort == "sort_rt"){
				$order_sql = " read_times ";
			}else if($sort == "sort_lr"){
				$order_sql = " last_accessed ";
			}
		}
		
		if($order == "sort_dec" || $order == "" ){
			$order_sql .= " DESC ";
			$order = "sort_dec";
		}else{
			$order_sql .= " ";
			$order = "sort_asc";
		}
		
		if($FromMonth == NULL && $ToMonth== NULL  && $FromYear == NULL && $ToYear == NULL){
			if(is_string($FromMonth)){
				
			}else{
				$ThisYear = Date("Y")*1;$ThisMonth = Date("m")*1;
				$FromMonth = $ThisMonth;
				$ToMonth = $ThisMonth;
				$FromYear = $ThisYear;
				$ToYear = $ThisYear;
			}
		}
		if($class_name == ''){
			$ebook_data['reading']=$lelibplus->getPublicClassSummary($offset, $amount, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);					
			include_once("templates/stats_publicclasssummary_record.php");
		}
		else if($student_id == '') {
			$ebook_data['reading']=$lelibplus->getPublicStudentSummary($offset, $amount,$class_name, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);
			//$filter_class_html = $lelibplus->retrieve_class_filter($class_name,$FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear);		
			include_once("templates/stats_publicstudentsummary_record.php");
		}else{
			$ebook_data['reading']=$lelibplus->getPublicStudentParticularSummary($offset, $amount,$student_id,$class_name, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql);	
			include_once("templates/stats_publicstudentparticularsummary_record.php");
		}
	break;
	
	case 'getReviewRecord2':
	    
	    $offset = $offset? $offset: 0;
		$amount = $offset==0? 10: 5;
		
		if($FromMonth == NULL && $ToMonth== NULL  && $FromYear == NULL && $ToYear == NULL){
			if(is_string($FromMonth)){
				
			}else{
				$ThisYear = Date("Y")*1;$ThisMonth = Date("m")*1;
				$FromMonth = $ThisMonth;
				$ToMonth = $ThisMonth;
				$FromYear = $ThisYear;
				$ToYear = $ThisYear;
			}
		}
		
		list($total, $reviews_data)=$lelibplus->getUserReviews2($student_id, $class_name, $offset, $amount,$FromMonth, $ToMonth,$FromYear,$ToYear);
		
		if ($reviews_data) include_once("templates/stats_publicstudentsreviews.php");
	break;
	///// Charles Ma 2013-09-30 END
}	

intranet_closedb();
?>
