<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

if($Keyword)
	$Keyword = stripslashes($Keyword);
	
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Management_Forum";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['ForumMgmt']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);
echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Management_Forum_UI($Keyword);

?>
<script>
function Check_All_Forum(checked)
{
	$('input[name="ForumID\\[\\]"]').attr('checked',checked);
}

function js_Reload_Forum_Table()
{
	Block_Element("ForumTableDiv");
	$.post(
		"ajax_task.php",
		{
			"Action":"ReloadForumTable",
			"Keyword":encodeURIComponent($('input#Keyword').val())
		},
		function(ReturnData)
		{
			$("div#ForumTableDiv").html(ReturnData);
			initThickBox();
			js_Init_DND_Table();
			UnBlock_Element("ForumTableDiv");
		}
	);
}

function js_Update_Order()
{
	var jsSubmitString = $("input.RowForumID").serialize();
	jsSubmitString += '&Action=UpdateForumDisplayOrder';
	
	$.ajax({  
		type: "POST",  
		url: "ajax_task.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			Scroll_To_Top();
			if(ReturnData.substring(0,1) != 1)
				js_Reload_Forum_Table();
		} 
	});
}

function js_Init_DND_Table() {
		
	var JQueryObj = $("table#ForumTable");
	var DraggingRowIdx = 0; 
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			if($(table).find("tr").index(DroppedRow)!= DraggingRowIdx) // update db only if order changed
				js_Update_Order();
		},
		onDragStart: function(table, DraggedTD) {
			DraggingRowIdx = $(table).find("tr").index($(DraggedTD).parent());
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
}

$().ready(function(){
	js_Reload_Forum_Table();
})
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>