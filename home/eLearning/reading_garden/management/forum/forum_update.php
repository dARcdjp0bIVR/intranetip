<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

$ForumID = trim($_REQUEST['ForumID']);
$DataArr['ForumID'] = $ForumID;
$DataArr['ForumName'] = trim(stripslashes($_REQUEST['ForumName']));
$DataArr['Description'] = trim(stripslashes($_REQUEST['Description']));
$DataArr['TopicNeedApprove'] = $_REQUEST['TopicNeedApprove'];
$DataArr['AllowAttachment'] = $_REQUEST['AllowAttachment'];
$DataArr['ClassID'] = $_REQUEST['ClassID'];

if(trim($ForumID)=='')
	$Action = "add";
else
	$Action = "edit";

$Success = $ReadingGardenLib->Manage_Forum_Record($Action,$DataArr);

if($Action=="add")
{
	if($Success)
		$Msg = "AddSuccess";
	else
		$Msg = "AddUnsuccess";
}
else if($Action=="edit")
{
	if($Success)
		$Msg = "UpdateSuccess";
	else
		$Msg = "UpdateUnsuccess";
}
header("Location: ./forum.php?Msg=$Msg");
intranet_closedb();
?>