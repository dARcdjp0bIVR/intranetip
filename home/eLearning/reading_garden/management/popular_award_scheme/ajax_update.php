<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "DeletePopularAwardSchemeRecord":
		$StudentItemIDArr = explode(",",$StudentItemID);
		$Success = $ReadingGardenLib->Delete_Popular_Award_Scheme_Record($StudentItemIDArr);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();

		if($Success)
			$msg = $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			$msg = $ReadingGardenUI->Get_Return_Message("DeleteUnsuccess");
		
		echo $msg; 
		
	break;	
}	

intranet_closedb();
?>