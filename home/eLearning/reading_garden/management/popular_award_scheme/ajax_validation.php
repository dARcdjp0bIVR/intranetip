<?php
// using Rita
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


$YearObj = new Year();
$libclass = new libclass();
$lu = new libuser();
$lexport = new libexporttext();
$limport = new libimporttext();
$ReadingGardenLib = new libreadinggarden();
$libdb = new libdb();

### Get Variables
$TargetFilePath = stripslashes($_REQUEST['TargetFilePath']);
	
### Include js libraries
$jsInclude = '';
$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
echo $jsInclude;

### Get Data from the Csv File
$data = $limport->GET_IMPORT_TXT($TargetFilePath);
$col_name = array_shift($data);
//debug_pr($data);
$SuccessArr = array();
$SuccessArr['Delete_Old_Temp_Records'] = $ReadingGardenLib->Delete_Award_Import_Temp_Data();

# Get Exisiting Class
$classList = $YearObj->Get_All_Classes();
$numOfClass = count($classList);
for($i=0; $i<$numOfClass; $i++){
	$classID []= $classList[$i]['YearClassID'];
	$thisClassNumberENGArr []= $classList[$i]['ClassTitleEN'];
	$classNameCHIArr []= $classList[$i]['ClassTitleB5'];													
}		

$AcademicYearID!="";
$AcademicYearID = Get_Current_Academic_Year_ID();

# Get Exsiting Item Code
$itemListArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Item_Info();
$numOfItemListArr = count($itemListArr);
for($i=0;$i<$numOfItemListArr;$i++){
//	$ItemIDArr[] = $itemListArr[$i]['ItemID'];
	$ItemCodeArr[] = $itemListArr[$i]['ItemCode'];
}

##### Check Data and Insert to DB #####
$numOfData = count($data);
$errorCount = 0;
$successCount = 0;
$numForEachRecord=0;
$thisRowNumber ='';
	
for($i=0;$i<$numOfData;$i++)
{
	$thisErrorMsgArr = array();
	$thisRowNumber = $i + 2;
	$thisClassName = trim($data[$i][0]);
	$thisClassNumber = trim($data[$i][1]);
	$thisUserLogin = trim($data[$i][2]);
	$thisItemCode = trim($data[$i][3]);
	$thisTimes = trim($data[$i][4]);
	$thisStudentID ='';
	$InsertTempDataValue ='';	
	### Check Class Name, Class Number, and User Login					
	if(empty($thisClassName)&& empty($thisClassNumber)&& empty($thisUserLogin))
	{
		$thisErrorMsgArr[] = $Lang['Group']['EmptyUserInfo'];		
	}
	elseif(!empty($thisClassName) && !empty($thisClassNumber))
	{
		$thisStudentID = $lu->returnUserID($thisClassName,$thisClassNumber);
	}
	elseif(!empty($thisUserLogin))
	{
		$thisStudentID = $lu->getUserIDByUserLogin($thisUserLogin);
	}

	if(empty($thisStudentID))
	{
		$thisErrorMsgArr[] = $Lang['Group']['UserNotExist'];		
	}	
			
	### Check Item Code
	$thisItemID = '';
	if($thisItemCode !=''){
		if(!in_array($thisItemCode, $ItemCodeArr, true)){
			$thisErrorMsgArr[] =  $Lang['ReadingGarden']['Import_Error']['InvalidItemCode'];
		}
		else{	
			$thisItemIDListArr = $ReadingGardenLib->Get_Popular_Award_Scheme_Item_Info('',$libdb->Get_Safe_Sql_Query($thisItemCode),'','');
			$thisItemID = $thisItemIDListArr[0]['ItemID'];
		}
	
	}else{
		$thisErrorMsgArr[] =  $Lang['ReadingGarden']['Import_Error']['ItemCodeIsMissing'];
	}
		
	### Check Times
	if($thisTimes !=''){
		if($thisTimes < 1){
			$thisErrorMsgArr[] =  $Lang['ReadingGarden']['Import_Error']['InvalidTimes'];
		}	
	}else{
		$thisErrorMsgArr[] =  $Lang['ReadingGarden']['Import_Error']['TimesIsMissing'];
	}
		
	### Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan").html("'.($i + 1).'");';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	### Prepare sql to insert to the temp csv
	$InsertTempDataArr []= " ( '".$_SESSION['UserID']."', '$thisRowNumber', '".$libdb->Get_Safe_Sql_Query($thisClassName)."', '$thisClassNumber', '$AcademicYearID', '$thisUserLogin', '$thisStudentID', '".$libdb->Get_Safe_Sql_Query($thisItemCode)."', '$thisItemID', '$thisTimes', now()
							 ) ";

	### Record the error messages		
	if(count($thisErrorMsgArr) == 0){
		$successCount++;	
	}else{
		$RowErrorRemarksArr[$thisRowNumber] = $thisErrorMsgArr;
	}
		
	### Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= '$("span#BlockUISpan").html("'.($i + 1).'");';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;	
}

$errorCount = count($RowErrorRemarksArr);

### Insert Data in Temp Table
//$InsertTempDataValue = implode(',', $InsertTempDataArr);
//$sql = "Insert Into TEMP_READING_SCHEME_AWARD_IMPORT
//			(UserID, RowNumber, ClassName, ClassNumber, AcademicYearID, UserLogin, StudentID, ItemCode, ItemID, Times, DateInput)
//		Values
//			$InsertTempDataValue
//		";
//$SuccessArr['Insert_Temp_CSV_Records'] = $ReadingGardenLib->db_db_query($sql);



$numOfInsertData = count($InsertTempDataArr);
if ($numOfInsertData > 0) {
	$numOfDataPerChunck = 1000;
	//$numOfChunk = ceil($numOfInsertData / $numOfDataPerChunck);
	$insertDataSplitedArr = array_chunk($InsertTempDataArr, $numOfDataPerChunck);
	//debug_pr($InsertTempDataArr);
	
	foreach((array)$insertDataSplitedArr as $_insertDateArr) {		
		$sql = "Insert Into TEMP_READING_SCHEME_AWARD_IMPORT
				(
				UserID, RowNumber, ClassName, ClassNumber, AcademicYearID, UserLogin, StudentID, ItemCode, ItemID, Times, DateInput
				)
				Values
				".implode(',', (array)$_insertDateArr);
							
		$SuccessArr['Insert_Temp_CSV_Records'] = $ReadingGardenLib->db_db_query($sql);
		//debug_pr($sql);
	}
}

	
$ErrorTable = '';
if($errorCount > 0){ 
		
	$ErrorRowNumArr = array_keys($RowErrorRemarksArr);
	$ErrorRowNumList = implode(',', $ErrorRowNumArr);
	
	$sql = "SELECT	*
			FROM 
			TEMP_READING_SCHEME_AWARD_IMPORT 
			WHERE 
			UserID = '".$_SESSION['UserID']."' And RowNumber In ($ErrorRowNumList) ";
	$ErrorRowInfoArr = $ReadingGardenLib->returnArray($sql);
							
							
	### Error Table Display 
	$ErrorTable .= '<table class="common_table_list_v30 view_table_list_v30">';
	$ErrorTable .= '<thead>';
	$ErrorTable .=  '<tr>';
	$ErrorTable .=  '<th width="10">'.$Lang['General']['ImportArr']['Row'].'</td>';
	$ErrorTable .=  '<th width="20%">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][0].'</th>';
	$ErrorTable .=  '<th width="15%">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][1].'</th>';
	$ErrorTable .=  '<th width="15%">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][2].'</th>';
	$ErrorTable .=  '<th width="15%">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][3].'</th>';
	$ErrorTable .=  '<th width="10%">'.$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][4].'</th>';

	$ErrorTable .=  '<th  width="20%">'.$iDiscipline['AccumulativePunishment_Import_Failed_Reason'].'</th>';
	$ErrorTable .=  '</tr>';	
	$ErrorTable .= '</thead>';		

	$ErrorTable .= '<tbody>';	
	for ($i=0; $i<$errorCount; $i++)
	{	
		$numForEachRecord ++;				
	
		### Display Emtpy Value
		$thisRowNumber 	= $ErrorRowInfoArr[$i]['RowNumber'];		
		$thisClassName = $ErrorRowInfoArr[$i]['ClassName']!=null ? $ErrorRowInfoArr[$i]['ClassName'] : '<font color="red">***</font>';
		$thisClassNumber = $ErrorRowInfoArr[$i]['ClassNumber']!=null? $ErrorRowInfoArr[$i]['ClassNumber']:'<font color="red">***</font>';
		$thisUserLogin = $ErrorRowInfoArr[$i]['UserLogin']!=null? $ErrorRowInfoArr[$i]['UserLogin']:'<font color="red">***</font>';
		$thisItemCode = $ErrorRowInfoArr[$i]['ItemCode']!=null? $ErrorRowInfoArr[$i]['ItemCode']:'<font color="red">***</font>';
		$thisTimes = $ErrorRowInfoArr[$i]['Times']? $ErrorRowInfoArr[$i]['Times']:'<font color="red">***</font>';
		$thisErrorRemarksArr = $RowErrorRemarksArr[$thisRowNumber];
		
		### Add Bullet '-' 
		$thisErrorRemarksArrWithBullet= array();
		for($j=0;$j<count($thisErrorRemarksArr);$j++){
			
			$thisErrorRemarksArrWithBullet[] = '- ' . $thisErrorRemarksArr[$j];
		}
		
		
		if($thisErrorRemarksArr!=''){	
			$thisErrorDisplay = implode('<br />', $thisErrorRemarksArrWithBullet);
		}
		
		### Check If the Field Need to be Displaied in Red Color
		
		if (in_array($Lang['Group']['EmptyUserInfo'], $thisErrorRemarksArr) || in_array($Lang['Group']['UserNotExist'], $thisErrorRemarksArr)){
			$thisClassName = '<font color="red">'.$thisClassName.'</font>';
			$thisClassNumber = '<font color="red">'.$thisClassNumber.'</font>';
			$thisUserLogin = '<font color="red">'.$thisUserLogin.'</font>';				 
		}
	
		if (in_array($Lang['ReadingGarden']['Import_Error']['ItemCodeIsMissing'], $thisErrorRemarksArr) || in_array($Lang['ReadingGarden']['Import_Error']['InvalidItemCode'], $thisErrorRemarksArr) ){
			$thisItemCode = '<font color="red">'.$thisItemCode.'</font>';
		}
		
		if (in_array($Lang['ReadingGarden']['Import_Error']['TimesIsMissing'], $thisErrorRemarksArr) || in_array($Lang['ReadingGarden']['Import_Error']['InvalidTimes'], $thisErrorRemarksArr)){
			$thisTimes = '<font color="red">'.$thisTimes.'</font>';
		}
	
	
		$thisErrorDisplay = '<font color="red">'.$thisErrorDisplay.'</font>';
		
		$css_i = ($i % 2) ? "2" : "";
		
		$ErrorTable .= '<tr>';
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'" width="10">'.$thisRowNumber.'</td>';
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'">' . $thisClassName . '</td>';
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'">' . $thisClassNumber . '</td>';
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'">' . $thisUserLogin . '</td>';
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'">' . $thisItemCode . '</td>';
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'">' . $thisTimes . '</td>';		
			$ErrorTable .= '<td class="tablebluerow'.$css_i.'">' . $thisErrorDisplay . '</td>';
		$ErrorTable .= '</tr>';
	}
	
	$ErrorTable .= '</tbody>';	
}
	
$ErrorTable .=  '</table>';
$ErrorTable = str_replace("\\", "\\\\", $ErrorTable);
$ErrorTable = str_replace("'", "\'", $ErrorTable);

$thisJSUpdate = '';
$thisJSUpdate .= '<script language="javascript">'."\n";
$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$ErrorTable.'\';';
$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCount.'\';';

# Check if Navigation Button Should be Disabled or Not
if ($errorCount == 0) 
{
	$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
	$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
}
		
$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
$thisJSUpdate .= '</script>'."\n";

echo $thisJSUpdate;
	
intranet_closedb();
?>