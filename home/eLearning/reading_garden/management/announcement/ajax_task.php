<?php
// using : 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "GetAnnouncementMessage":
		$AnnouncementID = $_REQUEST['AnnouncementID'];
		$AnnouncementInfo = $ReadingGardenLib->Get_Announcement_Info($AnnouncementID);
		echo $AnnouncementInfo[0]['Message'];
	break;
}

intranet_closedb();
?>