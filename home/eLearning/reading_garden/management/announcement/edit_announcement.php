<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Management_Announcement";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

$AnnouncementID = is_array($AnnouncementID)?$AnnouncementID[0]:$AnnouncementID;

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Management_Announcement_Edit_UI($AnnouncementID);

?>
<script>
function js_Add_Attachment()
{
	for(AttachmentNum=0; $(".AttRow"+AttachmentNum).length>=1;AttachmentNum++);
	
	var x ='';
	x += '<tr class="AttRow'+AttachmentNum+'">'+"\n";
		x += '<td class="AttachmentFileTD">'+"\n";
				x += '<input type="file" name="Attachment[]" class="Attachment" >'+"\n";
			x += '[<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row('+AttachmentNum+')"><?=$Lang['Btn']['Delete']?></a>]'+"\n";
		x += '</td>'+"\n";
	x += '</tr>'+"\n";
	
	$("#AddAttachmentRow").before(x);
	$("#AttachmentTitleTD").attr("rowspan",$(".Attachment").length+1);
}

function js_Remove_Attachment_Row(RowID)
{
	if($(".Attachment").length==1)
	{
		js_Add_Attachment();
		js_Remove_Attachment_Row(RowID);
	}
	else if($(".AttRow"+RowID).find("#AttachmentTitleTD"))
	{
		$(".AttachmentFileTD:eq(1)").before($(".AttRow"+RowID).find("#AttachmentTitleTD"))
		$(".AttRow"+RowID).remove();
		$("#AttachmentTitleTD").attr("rowspan",$(".Attachment").length+1);
	}
}

$().ready(function(){
	if("<?=$AnnouncementID?>"=="")
		Select_All_Options('ClassID',true);
})

function getEditorValue(instanceName) {
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	
	// Get the editor contents as XHTML.
	return oEditor.GetXHTML( false ) ; // "true" means you want it formatted.
}

function js_Check_Form()
{
	var FormValid = true;
	$("div.WarnMsgDiv").hide();
	$("input.Mandatory").each(function(){
		if($(this).val().Trim()=='')
		{
			FormValid = false;
			var WarnDivID = "WarnBlank"+$(this).attr("id");
			
			$("#"+WarnDivID).show();
		}
	})
	
	//Date
	var start_date = $('input#StartDate').val();
	var end_date = $('input#EndDate').val();
	var start_date_obj = document.getElementById('StartDate');
	var end_date_obj = document.getElementById('EndDate');

	if(!check_date_without_return_msg(start_date_obj) || !check_date_without_return_msg(end_date_obj))
	{
		$("#WarnInvalidDateFormat").show();
		FormValid = false;
	}
	
	if(start_date > end_date)
	{
		$("#WarnInvalidDateRange").show();
		FormValid = false;
	}
	
	//Message
	var Message = getEditorValue("Message");
	//Message = Message.substring(0,Message.length-6);
	Message = $.trim(Message.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
	
	if(Message.Trim()=='')
	{
		$("#WarnBlankMessage").show();
		FormValid = false;
	}
		
	//Class
	if(!$("#ClassID").val())
	{
		$("#WarnBlankClassID").show();
		FormValid = false;
	}

	return FormValid;
}

//function FCKeditor_OnComplete( editorInstance )
//{
//	var jsAnnouncementID = $('input#AnnouncementID').val();
//	if(jsAnnouncementID!=''){
//	    $(document).ready(function(){
//	    	$.post(
//	    		"ajax_task.php",
//	    		{
//	    			"Action":"GetAnnouncementMessage",
//	    			"AnnouncementID":jsAnnouncementID
//	    		},
//	    		function(data){
//	    			editorInstance.SetHTML(data);
//	    		}
//	    	);
//	    });
//	}
//}
</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>