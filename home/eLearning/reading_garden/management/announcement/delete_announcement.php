<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

$AnnouncementID = $_REQUEST['AnnouncementID'];
$Action = "delete";

$Success = $ReadingGardenLib->Manage_Announcement_Record($Action,$DataArr,$AnnouncementID);

if($Success)
	$Msg = "DeleteSuccess";
else
	$Msg = "DeleteUnsuccess";
	
header("location: ./announcement.php?Msg=$Msg");

intranet_closedb();
?>
