<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$linterface = new interface_html("popup.html");

$tags_arr[] = array($Lang['ReadingGarden']['StateList'],"view_level.php");
$tags_arr[] = array($Lang['ReadingGarden']['ScoringRule'],"#",1); 

$MODULE_OBJ["title"] = $Lang['ReadingGarden']['StateRequirement'];
$linterface->LAYOUT_START();

echo $ReadingGardenUI->Include_Reading_Garden_CSS();

echo $ReadingGardenUI->GET_SUBTAGS($tags_arr);
echo $ReadingGardenUI->Get_Scoring_Rule_Student_View();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>