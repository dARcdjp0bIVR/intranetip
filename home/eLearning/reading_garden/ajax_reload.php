<?php
// using :
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadClassSelection":
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$YearID = explode(",",$_REQUEST['YearID']);
		$SelectionID = trim(urldecode(stripslashes($_REQUEST['SelectionID'])));
		$YearClassID = $_REQUEST['YearClassID'];
		$onChange = trim(urldecode(stripslashes($_REQUEST['onChange'])));
		$noFirst = $_REQUEST['noFirst'];
		$isMultiple = $_REQUEST['isMultiple'];
		$isAll = $_REQUEST['isAll'];
		$TeachingOnly = $_REQUEST['TeachingOnly'];
		
		include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
		$fcm_ui = new form_class_manage_ui();
		$ClassSelection = $fcm_ui->Get_Class_Selection($AcademicYearID, $YearID, $SelectionID, $YearClassID, $onChange, $noFirst, $isMultiple, $isAll, $TeachingOnly);
		
		echo $ClassSelection;
	break;	
	case "ReloadAssignedReadingClassSelection":
		$AcademicYearID = Get_Current_Academic_Year_ID();
		$BookID = $_REQUEST['BookID'];
		$SelectionID = trim(urldecode(stripslashes($_REQUEST['SelectionID'])));
		$YearClassID = explode(",",$_REQUEST['YearClassIDArr']);
		$onChange = trim(urldecode(stripslashes($_REQUEST['onChange'])));
		$noFirst = $_REQUEST['noFirst'];
		$isMultiple = $_REQUEST['isMultiple'];
		$isAll = $_REQUEST['isAll'];
//		$TeachingOnly = $_REQUEST['TeachingOnly'];
		
		# Get Recommend Book Mappings
		$AvailableClass = $ReadingGardenLib->Get_Available_Class_A_Book_Can_Be_Assigned_To($BookID);
//		$ParArr['BookID'] = $BookID;
//		$RecommendBookMapping = $ReadingGardenLib->Get_Recommend_Book_Mappings($ParArr);
//		$AvailableYear = Get_Array_By_Key($RecommendBookMapping, "YearID");
//		
//		include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//		$fcm = new form_class_manage();
//		$classArr = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $AvailableYear, $TeachingOnly);
//		$AvailableClass = Get_Array_By_Key($classArr,"YearClassID");
//		
//		# Get classes that this book has been assigned to
//		$AssignedClassArr = $ReadingGardenLib->Get_Class_Assign_Reading_Target('','','',$BookID);
//		$AssignedClassIDArr = Get_Array_By_Key($AssignedClassArr, "ClassID");
//		$AvailableClass = array_diff((array)$AvailableClass,(array)$AssignedClassIDArr);
		
		if($isMultiple)
			$other = " multiple size=10 "; 
		
		if($noFirst)
			$DisplaySelect = 2;
		else if($isAll)
			$optionFirst = $Lang['SysMgr']['FormClassMapping']['All']['Class'];
		else
			$optionFirst = $Lang['SysMgr']['FormClassMapping']['Select']['Class'];
		
		include_once($PATH_WRT_ROOT."includes/libclass.php");
		$lclass = new libclass();
		$ClassSelection = $lclass->getSelectClassID(" id='$SelectionID' name='$SelectionID' onchange='$onChange' $other ",$YearClassID, $DisplaySelect,'',$optionFirst,$AvailableClass);
		
		echo $ClassSelection;
	break;	
	
	case "LoadLikeList":
		$FunctionName = trim(urldecode(stripslashes($_REQUEST['FunctionName'])));
		$FunctionID = $_REQUEST['FunctionID'];
		
		echo $ReadingGardenUI->Get_Like_List_Layer($FunctionName,$FunctionID);
	break;
	
	case "GetReadingRecordLayer":
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		echo $ReadingGardenUI->Get_Student_Reading_Record_Layer($ReadingRecordID);
	break;
	
	case "GetReadingRecordEditForm":
		$BookID = $_REQUEST['BookID'];
		$IsNew = $_REQUEST['IsNew'];
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$CallBack = $_REQUEST['CallBack'];
		echo $ReadingGardenUI->Get_Recommend_Book_Reading_Record_Edit_Form($BookID, $IsNew, $ReadingRecordID, $CallBack);
	break;
	
	case "GetBookReportEditForm":
		$Type = $_REQUEST['Type'];
		$IsNew = $_REQUEST['IsNew'];
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$BookReportID = $_REQUEST['BookReportID'];
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		$CallBack = $_REQUEST['CallBack'];
		$CategoryID = $_REQUEST['CategoryID'];
		
		
		echo $ReadingGardenUI->Get_Student_Book_Report_Edit_Form($Type, $IsNew, $ReadingRecordID, $BookReportID, $AssignedReadingID, $CallBack, $CategoryID);
	break;	
	
	case "GetAnswerSheet":
		$AnswerSheetID = $_REQUEST['AnswerSheetID'];
		$AnswerSheet = $ReadingGardenLib->Get_Answer_Sheet_Template($AnswerSheetID);
		echo $AnswerSheet[0]['AnswerSheet'];
	break;
	
	case "GetCategorySelection":
		$Language = $_REQUEST['Language'];
		$SelCategoryID = $_REQUEST['SelCategoryID'];
		$SelectionID = $_REQUEST['SelectionID']?$_REQUEST['SelectionID']:"SelCategoryID";
		$ShowUnclassified = $_REQUEST['HideUncategorized']?0:1;
		echo $ReadingGardenUI->Get_Category_Selection($SelectionID, $SelectionID, $SelCategoryID, '', '', '', $Language, $ShowUnclassified);
	break;
	
	case "GetAnnouncementLayer":
		$AnnouncementID = $_REQUEST['AnnouncementID'];
		
		echo $ReadingGardenUI->Get_Announcement_Layer($AnnouncementID);
	break;

	case "GetAwardLayer":
		$AwardSchemeID = $_REQUEST['AwardSchemeID'];
		echo $ReadingGardenUI->Get_Award_Layer($AwardSchemeID);
	break;
		
	case "ReloadLikeDiv":
		$FunctionName = $_REQUEST['FunctionName'];
		$FunctionID = $_REQUEST['FunctionID'];
		echo $ReadingGardenUI->Get_Like_Comment_Div_Content($FunctionName, $FunctionID);
	break;
	
	case "GetAwardSchemeSelection":
		$ID_Name = $_REQUEST['SelectionID'];
		$Selected = $_REQUEST['AwardSchemeID'];
		$Language = explode(",",$_REQUEST['Language']);
		$ForStudentSelect = $_REQUEST['ForStudentSelect']==1?1:"";
		$all = $_REQUEST['All']==1?1:0;
		$noFirst = $_REQUEST['NoFirst']==1?1:0;
		$FirstTitle = $_REQUEST['FirstTitle'];
		$OnChange = $_REQUEST['OnChange'];
		$other = $_REQUEST['other'];
		
		echo $ReadingGardenUI->Get_Award_Scheme_Selection($ID_Name, $Selected, $Language, $ForStudentSelect, $all, $noFirst, $FirstTitle, $OnChange, $other);
	break;	
	
	case "LoadPopularAwardScheme":
		$CallBack = stripslashes($_REQUEST['CallBack']);
		echo $ReadingGardenUI->Get_Popular_Award_Scheme_Add_Record_Layer($CallBack, $StudentID);
	break;
	
	case "LoadPopularAwardSchemeItemSelect":
		echo $ReadingGardenUI->Get_Popular_Award_Scheme_Item_Select_Form($CategoryID, $StudentSubmit);
	break;
}	

intranet_closedb();
?>