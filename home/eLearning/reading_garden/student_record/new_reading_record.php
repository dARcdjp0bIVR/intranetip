<?php
// editing by 
/********************************
 * Date:    2013-02-27 Rita
 * Details: modified js_Check_TB_Form() for cumstomization [#2013-0225-1415-16054]
 * 
 * Date:	2013-02-06 Rita
 * Details:	add  Customization - Update Popular Award Scheme Togeter
 * 
 *********************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title();
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_DatePicker_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();

echo $ReadingGardenUI->Get_My_Record_New_My_Reading_Book_Layer();

?>
<script>
function js_Reload_Category_Selection()
{
	var Language = $("select#External_Book_Language").val();
	$.post(
		"../ajax_reload.php",
		{
			"Action":"GetCategorySelection",
			"Language":Language,
			"SelectionID":"External_CategoryID",
			"HideUncategorized":"<?=$sys_custom['ReadingGarden']['HideUncategorizedInStudentInputBookSelection']?1:0?>" 
		},
		function (ReturnData){
			$("#CategorySelectSpan").html(ReturnData);
		}
	)
	
}

function js_Reload_Student_Record_Tab(SelectedTab, ReadingStatus)
{
	var clearCoo = '';
	
	if(!SelectedTab) // refresh dbtable
		var SelectedTab = $("#CurrentTab").val()
	else if(SelectedTab==$("#CurrentTab").val()) // if click current tab
		return true;
	else // change tab
		clearCoo = 1;
	
	
	if(!ReadingStatus)
		ReadingStatus = '';
		
	var order = $("#order").val()?$("#order").val():'';
	var field = $("#field").val()?$("#field").val():'';
	var pageNo = $("#pageNo").val()?$("#pageNo").val():'';	
	var numPerPage = $("#numPerPage").val()?$("#numPerPage").val():'';

	SwitchTab(SelectedTab)
	Block_Element("MyRecordDiv");
	$.post(
		"ajax_reload.php",
		{
			Action:"Reload"+SelectedTab+"UI",
			ReadingStatus:ReadingStatus,
			order:order,
			field:field,
			pageNo:pageNo,
			numPerPage:numPerPage,
			clearCoo:clearCoo
		},
		function(ReturnData)
		{
			$("#MyRecordDiv").html(ReturnData);
			
			if(SelectedTab=="AssignedReading" || SelectedTab=="ReadingRecord")
			{
				CurrRow = null;
				js_Show_Book(0);
			}

			initThickBox();			
			UnBlock_Element("MyRecordDiv");
		}
	)	
}

function SwitchTab(SelectedTab)
{
	$(".mybook_record_tab_current").removeClass("mybook_record_tab_current");
	$("li#"+SelectedTab+"Tab").addClass("mybook_record_tab_current");
	
	$("#CurrentTab").val(SelectedTab);
}

var CurrRow;
function js_Show_Book(row)
{
	if(CurrRow == row) return true;
	
	//hide Current
	$("div.reading_recommend_book_list").find("div.recommend_book_cover").hide();
	$("div.reading_recommend_book_list").find("div.like_comment").hide();
	$("div.book_list_item:eq("+CurrRow+")").removeClass("reading_recommend_book_list");
	
	//show Clicked
	$("div.book_list_item:eq("+row+")").addClass("reading_recommend_book_list");
	$("div.reading_recommend_book_list").find("div.recommend_book_cover").show();
	$("div.reading_recommend_book_list").find("div.like_comment").show();
	
	CurrRow = row
}

function js_Apply_Reading_Status()
{
	var ReadingStatus = new Array();
	$(".ReadingStatus:checked").each(function(){
		ReadingStatus.push($(this).val());
	})
	ReadingStatusStr = ReadingStatus.join(",");
	
	js_Reload_Student_Record_Tab('',ReadingStatusStr);
}

// My reading book
function js_Add_Reading_Record()
{
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadMyReadingBookEditLayer"
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			Step = 1;
			js_Change_Book_Source();
		}
	)		
}

function js_Change_Book_Source()
{
	var BookSource = $(".BookSource:checked").val();
	
	if(BookSource==1) // ExistingBook
	{
		$("div#ExternalBookDiv").hide()
		$("div#ExistingBookDiv").show()
		$("div#SearchResultDiv").hide()
		
	}
	else
	{
		$("div#ExternalBookDiv").show()
		$("div#ExistingBookDiv").hide()
		$("div#SearchResultDiv").html("")
		js_Reload_Category_Selection();
	}
	
	$("input#BackToSearchBtn").hide()
	Step = 1;
}

var Step = 1;
function js_Submit_TB_Form()
{
	if(Step==1)
	{
		var BookSource = $(".BookSource:checked").val();
		if(BookSource==1) // ExistingBook
			js_Search_Book();
		else
			js_Save_Book();
	}
	else if(Step==2)
	{
//		checkEditMultiple2(document.SearchResultForm,"BookID[]","js_Load_Book_Cover_View()");
		checkEditMultiple(document.SearchResultForm,"BookIDArr[]","../recommend_book/student_recommend_book.php?clearCoo=1");
		
		return true;
	}
}

function js_Search_Book()
{
	Block_Element("ExistingBookDiv");
	var jsSubmitString = $("form#ExistingBookForm").serialize();
	
	jsSubmitString += '&Action=SearchExistingBook';

	$.ajax({  
		type: "POST",  
		url: "ajax_reload.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			$("div#SearchResultDiv").html(ReturnData);
			$("div#SearchResultDiv").show()
			$("div#ExistingBookDiv").hide()
			$("input#BackToSearchBtn").show()
			Step = 2
			initThickBox();
			UnBlock_Element("ExistingBookDiv");
		} 
	});
}

function js_Back_To_Search()
{
	$("div#ExistingBookDiv").show()
	$("div#SearchResultDiv").hide()
	$("input#BackToSearchBtn").hide()
	Step = 1;
}

function js_Save_Book()
{
	if(!js_Check_TB_Form())
		return false;
		
//	Block_Thickbox();
	
	var obj = document.ExternalBookForm;
	obj.action = "ajax_update.php?Action=SaveExternalBook";
	obj.target = "FileUploadFrame";
	obj.encoding = "multipart/form-data";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function js_Save_External_Book_Complete(ReturnData, BookID)
{
	Get_Return_Message(ReturnData);
	if(ReturnData.substring(0,1)==1)
	{
		js_Load_Book_Cover_View(BookID, ReturnData);
	}
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = '';
	formObj.method = 'post';
}

function js_Check_TB_Form()
{
	FormValid = true;
	var error = 0;
	
	$(".WarnMsgDiv").hide();
	$("form#ExternalBookForm").find("input.Mandatory").each(function(){
		if($(this).val().Trim()==''){
			$("#WarnBlank"+$(this).attr('id')).show();
			//FormValid = false;
			error ++;
		}
	})
	
	FormValid2 = Check_Reading_Record(0); // Check_Reading_Record was located in reading_garden/reading_scheme.js 
	
	if(error==0 && FormValid2!=false){
		FormValid = true;	
	}else{
		FormValid = false;
	}
	
	return FormValid;
}
// (Step2)
function js_Load_Book_Cover_View(BookID, ReturnMsg)
{
//	var BookIDArr = new Array();
//	var Param = '';
//	if(!BookID)
//	{
//		$("[name='BookID\\[\\]']:checked").each(function(){
//			BookIDArr.push("BookIDArr[]="+$(this).val());
//		})
//		Param = BookIDArr.join("&");
//	}
//	else
//		Param = "BookIDArr[]="+BookID;
	if(BookID)
		window.location.href="../recommend_book/student_recommend_book.php?clearCoo=1&BookIDArr[]="+BookID+"&Msg="+ReturnMsg;
	else
		checkEditMultiple(document.SearchResultForm,"BookIDArr[]","../recommend_book/student_recommend_book.php?clearCoo=1");

}

function js_Check_All_Book(checked)
{
	$("[name='BookIDArr\\[\\]']").attr('checked',checked)
}

function js_Delete_Reading_Record()
{
	var BookIDArr = new Array();
	$("[name='BookIDArr\\[\\]']:checked").each(function(){
		BookIDArr.push($(this).val());
	})
	var BookID = BookIDArr.join(",");
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteMyReadingRecord",
			BookID:BookID
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			js_Reload_Student_Record_Tab()
		}
	)
}

function js_Reload_Award_Scheme_Selection()
{
	var Language = $("select#External_Book_Language").val();
	$.post(
		"../ajax_reload.php",
		{
			"Action":"GetAwardSchemeSelection",
			"Language":Language+",3", //get selected language and "either"
			"SelectionID":"AwardSchemeID",
			"ForStudentSelect": 1
		},
		function (ReturnData){
			$("#AwardSchemeSelectSpan").html(ReturnData);
		}
	)
}

function js_Back_To_My_Record()
{
	window.location="index.php?CurrentTab=ReadingRecord";
}

$().ready(function(){
	js_Change_Book_Source();
	js_Reload_Award_Scheme_Selection();
		
	<?php if($ReadingGardenLib->enableReadingSchemeAddPopularDataTogetherRight()){ ?>
		// Show category items and times, value 1 means for student only
		js_Reload_Popular_Award_Scheme_Add_Layer_Category(1);
	<?php } ?>
	
})
</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>