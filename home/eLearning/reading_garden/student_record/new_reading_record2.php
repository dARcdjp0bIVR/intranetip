<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.marcus.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.marcus.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title();
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_DatePicker_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();

echo $ReadingGardenUI->Get_My_Record_New_My_Reading_Record_Form($BookID);

?>
<script>
function js_Back_To_Select_Book()
{
	window.location="new_reading_record.php";
}

function js_Back_To_My_Record()
{
	window.location="index.php?CurrentTab=ReadingRecord";
}

</script>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>