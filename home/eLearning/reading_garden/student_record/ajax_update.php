<?php
// editing by 
/********************************
 * Date:	2013-02-06 Rita
 * Details:	add  Customization - Update Popular Award Scheme Togeter
 * 
 *********************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "SaveExternalBook":
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		
		if($BookCoverImage)
		{
		$isImageValid = exif_imagetype($BookCoverImage);
			
			if(!$isImageValid)
			{
				$msg = $ReadingGardenUI->Get_Return_Message("ImageUploadFail");
				echo '<script type="text/javascript">'."\n";
				echo 'window.parent.js_Save_External_Book_Complete("'.$msg.'");'."\n";
				echo '</script>'."\n";
				exit;
			}
		}
		
		$ReadingGardenLib->Start_Trans();
		
		$BookInfo['BookName'] = trim(stripslashes($_REQUEST['BookName']));
		$BookInfo['Author'] = trim(stripslashes($_REQUEST['Author']));
		$BookInfo['CallNumber'] = trim(stripslashes($_REQUEST['CallNumber']));
		$BookInfo['Publisher'] = trim(stripslashes($_REQUEST['Publisher']));
		$BookInfo['Language'] = trim(stripslashes($_REQUEST['External_Book_Language']));
		$BookInfo['CategoryID'] = trim(stripslashes($_REQUEST['External_CategoryID']));
		$BookInfo['Description'] = trim(stripslashes($_REQUEST['Description']));
		$BookInfo['ByStudent'] = 1;
		
		$InsertID = $ReadingGardenLib->Create_Book($BookInfo);

		if($InsertID)
		{
			$DataArr['Action'] = "add";
			$DataArr['BookID'] = $InsertID;
			$DataArr['StartDate'] = $_REQUEST['StartDate'];
			$DataArr['FinishedDate'] = $_REQUEST['FinishedDate']; 
			$DataArr['ReadingHours'] = $_REQUEST['ReadingHours'];
			$DataArr['Progress'] = $_REQUEST['Progress'];
			$DataArr['AwardSchemeID'] = $_REQUEST['AwardSchemeID'];
			
			$Success[] = $ReadingGardenLib->Manage_Student_Reading_Record($DataArr);
		}
		
		# Customization - Update Popular Award Scheme Togeter
		if($ReadingGardenLib->enableReadingSchemeAddPopularDataTogetherRight()){
			$Times = $_POST['times'];
			$ItemID = $_POST['PopularAwardSchemeItem'];
		
			$StudentID = $_SESSION['UserID']; 
			if($ItemID!=''){
				for($i=0; $i<$Times; $i++){
					$Success[] = $ReadingGardenLib->Submit_Popular_Award_Scheme_Record($ItemID, $StudentID);
				}
			}
			
		}
		
		
		//if($Success)
		if(count($Success)>0 && !in_array(false, (array)$Success))
		{
			if($BookCoverImage)
				$Result = $ReadingGardenLib->Update_Book_Cover_Image($InsertID, $BookCoverImage, $BookCoverImage_name);
				
			if($BookCoverImage && !$Result){
				$msg = $ReadingGardenUI->Get_Return_Message("ImageUploadFail");
				$ReadingGardenLib->RollBack_Trans();
			}
			else
			{
				$msg = $ReadingGardenUI->Get_Return_Message("AddBookSuccess");
				$ReadingGardenLib->Commit_Trans();
			}
		}
		else
		{
			$msg = $ReadingGardenUI->Get_Return_Message("AddBookFail");
			$ReadingGardenLib->RollBack_Trans();
		}
		
		echo '<script type="text/javascript">'."\n";
		echo 'window.parent.js_Save_External_Book_Complete("'.$msg.'",'.$InsertID.');'."\n";
		echo '</script>'."\n";
	

	break;
	
	case "DeleteMyReadingRecord":
		$BookIDArr = explode(",",$BookID);
		$Success = $ReadingGardenLib->Delete_Reading_Record_By_BookID($BookIDArr);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();

		if($Success)
			$msg = $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			$msg = $ReadingGardenUI->Get_Return_Message("DeleteUnsuccess");
		
		echo $msg; 
		
	break;

	case "DeletePopularAwardSchemeRecord":
		$StudentItemIDArr = explode(",",$StudentItemID);
		$Success = $ReadingGardenLib->Delete_Popular_Award_Scheme_Record($StudentItemIDArr);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();

		if($Success)
			$msg = $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			$msg = $ReadingGardenUI->Get_Return_Message("DeleteUnsuccess");
		
		echo $msg; 
		
	break;	
}

intranet_closedb();
?>