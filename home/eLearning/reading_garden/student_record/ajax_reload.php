<?php
// using :

/**********
 * 2013-02-06 (Rita)
 * - add ReadingAwardScheme for customization #[2012-1009-1402-14054]
 * 2012-11-06 (Rita)
 * - add $AcademicYearID for ReloadReadingRecordUI
 * 
 ***********/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadAssignedReadingUI":
		$ReadingStatus = stripslashes($_REQUEST['ReadingStatus']);
		if($ReadingStatus)
			$ReadingStatusArr = explode(",",$ReadingStatus);
		
		$x = $ReadingGardenUI->Get_My_Record_Assigned_Reading_UI($ReadingStatusArr);
		echo $x;
	break;	
	
	case "ReloadReadingRecordUI":
		$ReadingStatus = stripslashes($_REQUEST['ReadingStatus']);
		if($ReadingStatus)
			$ReadingStatusArr = explode(",",$ReadingStatus);

		$field = stripslashes($_REQUEST['field']);
		$order = stripslashes($_REQUEST['order']);
		$pageNo = stripslashes($_REQUEST['pageNo']);
		$numPerPage = stripslashes($_REQUEST['numPerPage']);
		$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);

		if($AcademicYearID=='undefined'){
			$AcademicYearID ='';
		}
		
		$arrCookies[] = array("ck_my_record_reading_record_page_size", "numPerPage");
		$arrCookies[] = array("ck_my_record_reading_record_page_number", "pageNo");
		$arrCookies[] = array("ck_my_record_reading_record_page_order", "order");
		$arrCookies[] = array("ck_my_record_reading_record_page_field", "field");	
		if($_REQUEST['clearCoo'])
		{
			clearCookies($arrCookies);
			$field = $order = $pageNo = $numPerPage = '';
		}else
			updateGetCookies($arrCookies);
		
		$x = $ReadingGardenUI->Get_My_Record_My_Reading_Book_UI($field, $order, $pageNo, $numPerPage, $ReadingStatusArr, $AcademicYearID);
		echo $x;
	break;
	case "ReloadReadingTargetUI":
		$field = stripslashes($_REQUEST['field']);
		$order = stripslashes($_REQUEST['order']);
		$pageNo = stripslashes($_REQUEST['pageNo']);
		$numPerPage = stripslashes($_REQUEST['numPerPage']);

		$arrCookies[] = array("ck_my_record_reading_target_page_size", "numPerPage");
		$arrCookies[] = array("ck_my_record_reading_target_page_number", "pageNo");
		$arrCookies[] = array("ck_my_record_reading_target_page_order", "order");
		$arrCookies[] = array("ck_my_record_reading_target_page_field", "field");	
		if($_REQUEST['clearCoo'])
		{
			clearCookies($arrCookies);
			$field = $order = $pageNo = $numPerPage = '';
		}else
			updateGetCookies($arrCookies);
		$x = $ReadingGardenUI->Get_My_Record_Reading_Target_DBTable($field, $order, $pageNo, $numPerPage);
		echo $x;
	break;
	case "ReloadPopularAwardSchemeUI":
		$field = stripslashes($_REQUEST['field']);
		$order = stripslashes($_REQUEST['order']);
		$pageNo = stripslashes($_REQUEST['pageNo']);
		$numPerPage = stripslashes($_REQUEST['numPerPage']);

		$arrCookies[] = array("ck_my_record_popular_award_scheme_page_size", "numPerPage");
		$arrCookies[] = array("ck_my_record_popular_award_scheme_page_number", "pageNo");
		$arrCookies[] = array("ck_my_record_popular_award_scheme_page_order", "order");
		$arrCookies[] = array("ck_my_record_popular_award_scheme_page_field", "field");	
		if($_REQUEST['clearCoo'])
		{
			clearCookies($arrCookies);
			$field = $order = $pageNo = $numPerPage = '';
		}else
			updateGetCookies($arrCookies);
		$x = $ReadingGardenUI->Get_My_Record_Popular_Award_Scheme_UI($field, $order, $pageNo, $numPerPage);
		echo $x;
	break;	
	case "LoadMyReadingBookEditLayer":
		$x = $ReadingGardenUI->Get_My_Record_New_My_Reading_Book_Layer();
		echo $x;
	break;
	
	case "SearchExistingBook":
		$BookInfo['BookName'] = trim(stripslashes($_REQUEST['BookName']));
		$BookInfo['CallNumber'] = trim(stripslashes($_REQUEST['CallNumber']));
		$BookInfo['Author'] = trim(stripslashes($_REQUEST['Author']));
		$BookInfo['Publisher'] = trim(stripslashes($_REQUEST['Publisher']));
		$Language = $_REQUEST['Language'];
		$CategoryID = $_REQUEST['CategoryID'];
		$BookSource = $_REQUEST['BookSource'];
		
		$x = $ReadingGardenUI->Get_My_Record_New_My_Reading_Book_Search_Result($BookSource,$BookInfo,$CategoryID,$Language);
		echo $x;
	break;
//	case "LoadReadingBookCoverView":
//		$BookID = $_REQUEST['BookID'];
//		$BookIDArr = explode(",",$BookID);
//		
//		$x = $ReadingGardenUI->Get_My_Record_New_My_Reading_Book_Cover_View($BookIDArr);
//		echo $x;
//	break;
//	

	break;
}	

intranet_closedb();
?>