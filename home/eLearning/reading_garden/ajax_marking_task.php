<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$key = trim($_REQUEST['key']);
if($key!=''){
	$valid_key = libreadinggarden::Grant_View_Right_For_Report_Page($key);
	if(!$valid_key) exit;
}else{
	intranet_auth();
}
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Action = stripslashes($_REQUEST['Action']);


switch($Action)
{
	case "MarkBookReport":
		$BookReportID = $_REQUEST['BookReportID'];
		$Grade = trim($_REQUEST['Grade']);
		$TeacherComment = trim(urldecode(stripslashes($_REQUEST['TeacherComment'])));
		$Recommend = $_REQUEST['Recommend'];
		$UpdateData['Grade'] = $Grade;
		$UpdateData['TeacherComment'] = $TeacherComment;
		$UpdateData['Recommend'] = $Recommend;
		$OldRecommend = $_REQUEST['OldRecommend'];
		$StudentID = $_REQUEST['StudentID'];
		
		$StickerID = (array)$_REQUEST['StickerID'];
		$OldStickerList = $ReadingGardenLib->Get_Report_Sticker('',$BookReportID,true);
		$ExistingStickerID = Get_Array_By_Key($OldStickerList,'StickerID');
		$StickerIDToAdd = array_values(array_unique(array_diff($StickerID,$ExistingStickerID)));
		$StickerIDToRemove = array_values(array_unique(array_diff($ExistingStickerID,$StickerID)));
		$UpdateData['StickerIDToAdd'] = $StickerIDToAdd;
		$UpdateData['StickerIDToRemove'] = $StickerIDToRemove;
		
		$Result = $ReadingGardenLib->Mark_Book_Report($BookReportID,$ReadingRecordID,$UpdateData,$StudentID,$OldRecommend);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "GetBookReportAttachment":
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report('',$ReadingRecordID);
		echo trim($BookReportInfo[0]['Attachment']);
	break;
	case "GetBookReportMarkingEditArea":
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$RecommendOnly = $_REQUEST['RecommendOnly'];
		echo $ReadingGardenUI->Get_Book_Report_Marking_Edit_Area($ReadingRecordID,$RecommendOnly);
	break;
	case "GetBookReportID":
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report('',$ReadingRecordID);
		$answersheet = trim($BookReportInfo[0]['AnswerSheet']);
		$onlinewriting = trim($BookReportInfo[0]['OnlineWriting']);
		if($answersheet=="" && $onlinewriting=="")
			echo "";
		else
			echo $BookReportInfo[0]['BookReportID'];
	break;
	case "GetViewButtonsLayer":
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		echo $ReadingGardenUI->Get_Book_Report_Marking_View_Buttons_Layer($ReadingRecordID);
	break;
	case "UnregisterSession";
		unset($_SESSION['UserID']);
		unset($_SESSION["intranet_session_language"]);
		//session_unset();
		//session_destroy();
	break;
}
/*
if($key!=''){
	unset($_SESSION['UserID']);
}
*/

intranet_closedb();
?>