<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();

$arrCookies[] = array("ck_settings_assign_reading_page_ClassID", "YearClassID");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_AssignReadingSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));

$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Settings_Assign_Reading_UI($YearClassID,$Keyword);

?>
<script>
function js_Reload_Assign_Reading_Table()
{
	var YearClassID = $("#YearClassID").val();
	var Keyword = $("#Keyword").val();
	Block_Element("AssignReadingTableDiv");
	$.post(
		"ajax_reload.php",
		{
			Action:"RelaodAssignReadingTable",
			YearClassID:YearClassID,
			Keyword:Keyword
		},
		function(ReturnData)
		{
			$("div#AssignReadingTableDiv").html(ReturnData);
			js_Init_DND_Table();
			initThickBox();
			UnBlock_Element("AssignReadingTableDiv");
		
		}
	);
	
}

function js_Delete_Assign_Reading(AssignedReadingID)
{
	if(!AssignedReadingID)
	{
		var AssignedReadingIDArr = new Array();
		$("input[name='AssignedReadingID\\[\\]']:checked").each(function(){
			AssignedReadingIDArr.push($(this).val().toString());
		})
		AssignedReadingID = AssignedReadingIDArr.join(",");
	}
	else
	{
		if(!confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromAllClass']?>"))
			return false;	
	}
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteAssignReading",
			AssignedReadingID:AssignedReadingID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			Scroll_To_Top()
			tb_remove()
			js_Reload_Assign_Reading_Table();
		}
	);
}

function js_Assign_Reading_Go_To_Step1()
{
	var YearClassID = $("#YearClassID").val();
	
	window.location="assign_reading_select_class.php?YearClassID="+YearClassID;
}

function js_Delete_Assign_Reading_Mapping()
{
	var YearClassID = $("#YearClassID").val();
	var AssignedReadingIDArr = new Array();
	$("input[name='AssignedReadingID\\[\\]']:checked").each(function(){
		AssignedReadingIDArr.push($(this).val().toString());
	})
	AssignedReadingID = AssignedReadingIDArr.join(",");

	$.post(
		"ajax_update.php",
		{
			Action:"DeleteAssignReadingMapping",
			YearClassID:YearClassID,
			AssignedReadingID:AssignedReadingID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			Scroll_To_Top()
			js_Reload_Assign_Reading_Table();
		}
	);
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = '';
	formObj.method = 'post';
}

function js_Update_Order(YearClassID)
{
	var YearClassID = $("#YearClassID").val();
	var jsSubmitString = $("input.ClassAssignedReadingID").serialize();
	
	jsSubmitString += '&Action=UpdateAssignReadingOrder';
	jsSubmitString += '&YearClassID='+YearClassID;

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			Scroll_To_Top();
			if(ReturnData.substring(0,1) != 1)
				js_Reload_Assign_Reading_Table();
		} 
	});
	
}

// AnswerSheet
function js_Preview_AnswerSheet(AnswerSheet, AssignedReadingID)
{
	newWindow('',10)
	var obj = document.form1;
	obj.AnswerSheet.value=AnswerSheet;
	obj.action = "answersheet.php?ViewOnly=1&AssignedReadingID="+AssignedReadingID;
	obj.target = "intranet_popup10";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function js_Init_DND_Table() {
		
	var JQueryObj = $("table#AssignReadingTable");
	var DraggingRowIdx = 0; 
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			if($(table).find("tr").index(DroppedRow)!= DraggingRowIdx) // update db only if order changed
			{
				var YearClassID = $(DroppedRow).attr("class");
				js_Update_Order(YearClassID);
			}	
			
		},
		onDragStart: function(table, DraggedTD) {
			DraggingRowIdx = $(table).find("tr").index($(DraggedTD).parent());
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	
}
function js_Unassign_Class()
{
	var msg = $("#YearClassID").val()?"<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromClass']?>":"<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromAllClass']?>";
	checkRemove2(document.form1,'AssignedReadingID[]','js_Delete_Assign_Reading_Mapping()',msg);
}

function js_Check_All_Assign_Reading(obj)
{
	$(obj.form).find(".AssignedReadingID").attr("checked",obj.checked);
}

$().ready(function(){
js_Reload_Assign_Reading_Table();
})
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>