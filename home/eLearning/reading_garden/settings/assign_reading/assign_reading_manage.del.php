<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();


### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_assign_reading_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_assign_reading_page_number", "pageNo");
$arrCookies[] = array("ck_settings_assign_reading_page_order", "order");
$arrCookies[] = array("ck_settings_assign_reading_page_field", "field");	
$arrCookies[] = array("ck_settings_assign_reading_page_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_AssignReadingSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));

$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();

echo $ReadingGardenUI->Get_Settings_Assign_Reading_Management_UI($YearID,$field, $order, $pageNo, $numPerPage, $Keyword)

?>
<script>
function js_Reload_Class_Selection()
{
	var YearID = $("#YearID").val();
	
	if(YearID==-1)
	{
		$("span#ClassFilterSpan").html('');
		js_Reload_Assign_Reading_Table()
	}
	else
	{
		$("span#ClassFilterSpan").html('<?=$ReadingGardenUI->Get_Ajax_Loading_Image()?>');
		$.post(
			"../../ajax_reload.php",
			{
				Action:"ReloadClassSelection",
				YearID:YearID,
				SelectionID:"YearClassID",
				onChange:"js_Reload_Assign_Reading_Table();",
				isAll:1
			},
			function(ReturnData)
			{
				$("span#ClassFilterSpan").html(ReturnData);
				js_Reload_Assign_Reading_Table()
			}
		);
	}
}

function js_Reload_Assign_Reading_Table()
{
	var jsSubmitString = $("#form1").serialize();
	jsSubmitString += '&Action=RelaodAssignReadingDBTable';
	
	Block_Element("AssignReadingDBTable");
	$.ajax({  
		type: "POST",  
		url: "ajax_reload.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			$("#AssignReadingDBTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("AssignReadingDBTable");
		} 
	});
}

function js_Create_Assign_Reading(AssignedReadingID,ReturnMsg)
{
	AssignedReadingID = AssignedReadingID?AssignedReadingID:''

	$.post(
		"ajax_reload.php",
		{
			Action:"CreateAssignReading",
			AssignedReadingID:AssignedReadingID
		},
		function(ReturnData)
		{
			$("div#TB_ajaxContent").html(ReturnData);
			js_Enable_AnswerSheet();
			if(ReturnMsg)
				Get_Return_Message(ReturnMsg);
			
			if(AssignedReadingID=='')
			{
				js_Select_All_Year_Class();
				$("#TB_ajaxWindowTitle").html("<?=$Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading']?>");
			}
		}
	);
}

var EditingAssignReading = '';
function js_Update_Assign_Reading(AssignReadingID)
{
	if(!AssignReadingID)
		var AssignReadingID = $("select#AssignedReadingID").val();
	
	EditingAssignReading = AssignReadingID;
	js_Create_Assign_Reading(AssignReadingID);
}

function js_Delete_Assign_Reading(AssignedReadingID)
{
	if(!AssignedReadingID)
	{
		var AssignedReadingIDArr = new Array();
		$("input[name='AssignedReadingID\\[\\]']:checked").each(function(){
			AssignedReadingIDArr.push($(this).val().toString());
		})
		AssignedReadingID = AssignedReadingIDArr.join(",");
	}
	else
	{
		if(!confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromAllClass']?>"))
			return false;	
	}
	
	$.post(
		"ajax_update.php",
		{
			Action:"DeleteAssignReading",
			AssignedReadingID:AssignedReadingID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			Scroll_To_Top();
			js_Reload_Assign_Reading_Table();
			tb_remove();
		}
	);
}


function js_Open_Select_Book_Layer()
{
	var YearID = $("#YearID").val();
	$("div#AssignReadingLayerDiv").hide();
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadAssignReadingSelectBookLayer",
			YearID:YearID
		},
		function(ReturnData)
		{
			$("div#SelectBookLayer").html(ReturnData);
			js_Reload_Book_Showcase();
		}
	);
}

function js_Reload_Book_Showcase()
{
	var YearID = $("#BookYearID").val();
	var CategoryID = $("#CategoryID").val();
	var Keyword = $("#BookKeyword").val();
	var BookID = $("#BookID").val();
	Block_Element("ItemShowcase");
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadBookShowCase",
			YearID:YearID,
			CategoryID:CategoryID,
			Keyword:Keyword,
			SelectedBookID:BookID
		},
		function(ReturnData)
		{
			$("div#ItemShowcase").html(ReturnData);
			$("div#SelectBookLayer").show();
			UnBlock_Element("ItemShowcase");
		}
	);
}

function js_Close_Select_Book_Layer()
{
	$("div#SelectBookLayer").hide().html("");
	$("div#AssignReadingLayerDiv").show();
}

function js_Select_Book(BookID)
{
	
	if($("#BookID").val() == BookID)
		return false;
		
	$("#BookID").val(BookID);
	js_Close_Select_Book_Layer();
	js_Preview_Book_Info();
	js_Reload_Assigned_Class_Selection();
}

function js_Preview_Book_Info()
{
	var BookID = $("#BookID").val();
	js_Load_Book_Default_AnswerSheet(BookID);
	if(!BookID)
		return false;
	
	$("div#BookInfoDiv").html('<?=$ReadingGardenUI->Get_Ajax_Loading_Image()?>');	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadBookInfo",
			BookID:BookID
		},
		function(ReturnData)
		{
			$("div#BookInfoDiv").html(ReturnData);
		}
	);
}

function js_Load_Book_Default_AnswerSheet(BookID)
{
		
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadDefaultAnswerSheet",
			BookID:BookID
		},
		function(ReturnData)
		{
			$("input#AnswerSheet").val(ReturnData);
			if(ReturnData)
				$("input#UseAnswerSheet").attr("checked","checked");
			else
				$("input#UseAnswerSheet").attr("checked","");
			
			js_Enable_AnswerSheet()
		}
	);
	
}

function js_Reload_Assigned_Class_Selection()
{
	var BookID = $("#BookID").val();
	var YearClassID = $("#YearClassIDArr\\[\\]").val().toString();
	
	$("span#AssignedClassSelectionSpan").html('<?=$ReadingGardenUI->Get_Ajax_Loading_Image()?>');
	$.post(
		"../../ajax_reload.php",
		{
			Action:"ReloadAssignedReadingClassSelection",
			YearClassIDArr:YearClassID,
			BookID:BookID,
			SelectionID:"YearClassIDArr[]",
			isAll:0,
			noFirst:1,
			isMultiple:1
		},
		function(ReturnData)
		{
			$("span#AssignedClassSelectionSpan").html(ReturnData);
			js_Reload_Assign_Reading_Table()
		}
	);
}

function js_Add_Attachment()
{
	for(AttachmentNum=0; $(".AttRow"+AttachmentNum).length>=1;AttachmentNum++);
	
	var x ='';
	x += '<tr class="AttRow'+AttachmentNum+'">'+"\n";
		x += '<td class="AttachmentFileTD">'+"\n";
				x += '<input type="file" name="Attachment[]" class="Attachment" >'+"\n";
			x += '[<a href="javascript:void(0);" onclick="js_Remove_Attachment_Row('+AttachmentNum+')"><?=$Lang['Btn']['Delete']?></a>]'+"\n";
		x += '</td>'+"\n";
	x += '</tr>'+"\n";
	
	$("#AddAttachmentRow").before(x);
	$("#AttachmentTitleTD").attr("rowspan",$(".Attachment").length+1);
}

function js_Remove_Attachment_Row(RowID)
{
	if($(".Attachment").length==1)
	{
		js_Add_Attachment();
		js_Remove_Attachment_Row(RowID);
	}
	else if($(".AttRow"+RowID).find("#AttachmentTitleTD"))
	{
		$(".AttachmentFileTD:eq(1)").before($(".AttRow"+RowID).find("#AttachmentTitleTD"))
		$(".AttRow"+RowID).remove();
		$("#AttachmentTitleTD").attr("rowspan",$(".Attachment").length+1);
	}
	
}

var isAddMore = '';
function aj_Save_Form(AddMore)
{
	var isEdit = $("#isEdit").val();
	if(isEdit==1 && !confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['AssignedReadingOfAllClassWillBeUpdate']?>"))
		return false;
	
	Block_Thickbox();
	isAddMore = AddMore;
	var obj = document.TBForm;
	obj.action = "ajax_save_assign_reading.php";
	obj.target = "FileUploadFrame";
	obj.encoding = "multipart/form-data";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = '';
	formObj.method = 'post';
}

//called from ajax_save_assign_reading.php
function js_Save_Complete(ReturnMsg)
{
	Get_Return_Message(ReturnMsg);
	
	if(ReturnMsg.substring(0,1)==1)
	{
		if(isAddMore==1)
			js_Create_Assign_Reading('',ReturnMsg);
		else
		{
			tb_remove();
			// refresh assigned reading selection
			js_Reload_Assign_Reading_Table()
		}
	}
	
	isAddMore = 0;
	UnBlock_Thickbox();
}

function CheckTBForm(AddMore)
{
	var FormValid = true;
	$("div.WarnMsgDiv").hide();
	$("input.Mandatory").each(function(){
		
		if($(this).val().Trim()=='')
		{
			FormValid = false;
			var WarnDivClass = "WarnBlank"+$(this).attr("id");
			
			$("#"+WarnDivClass).show();
		}
	})
	
	if(FormValid)
		aj_Save_Form(AddMore)
}

// AnswerSheet
function js_Edit_AnswerSheet(Preview)
{
	newWindow('',10)
	var obj = document.TBForm;
	obj.isPreview.value = Preview;
	obj.action = "answersheet.php";
	obj.target = "intranet_popup10";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function js_View_AnswerSheet()
{
	js_Edit_AnswerSheet(1)
}

function js_Preview_AnswerSheet(AnswerSheet)
{
	newWindow('',10)
	var obj = document.form1;
	obj.AnswerSheet.value=AnswerSheet;
	obj.action = "answersheet.php";
	obj.target = "intranet_popup10";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function js_Enable_AnswerSheet()
{
	if($("input#UseAnswerSheet").attr("checked"))
		$("span#AnswerSheetBtn").show()
	else
		$("span#AnswerSheetBtn").hide()
}

function js_Select_All_Year_Class()
{
	js_Select_All("YearClassIDArr[]");
}

function sortPage(order,field,formObj)
{
	formObj.order.value = order;
	formObj.field.value = field;
	formObj.pageNo.value = 1;
	js_Reload_Assign_Reading_Table();
}

$().ready(function(){
	js_Reload_Assign_Reading_Table();
})
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>