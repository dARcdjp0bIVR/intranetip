<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "RelaodAssignReadingTable":
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$YearClassID = $_REQUEST['YearClassID'];
		
		$arrCookies[] = array("ck_settings_assign_reading_page_ClassID", "YearClassID");

		updateGetCookies($arrCookies);
		
		if(strstr($YearClassID,"::"))
		{
			$YearClassID = str_replace("::",'',$YearClassID);	
		}
		else if($YearClassID==0)
			$YearClassID = '';
		else
		{
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$YearObj = new Year($YearClassID);
			$ClassList = $YearObj->Get_All_Classes();
			$YearClassID = Get_Array_By_Key($ClassList,"YearClassID");
		}
		
		echo $ReadingGardenUI->Get_Settings_Assign_Reading_Setting_Table($YearClassID,$Keyword);
	break;
	case "LoadAssignReadingNewRow":
		$ClassID = $_REQUEST['ClassID'];
		$SelectedAssignReading = $_REQUEST['SelectedAssignReading'];

		echo $ReadingGardenUI->Get_Settings_Assign_Reading_Setting_Table_Add_Row($ClassID, $SelectedAssignReading);
	break;	
	case "CreateAssignReading":
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		
		echo $ReadingGardenUI->Get_Settings_Assign_Reading_Edit_Layer($AssignedReadingID);
	break;
	case "LoadAssignReadingSelectBookLayer":
		$YearID = $_REQUEST['YearID'];
		$CategoryID = $_REQUEST['CategoryID'];
		
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		
		echo $ReadingGardenUI->Get_Settings_Assign_Reading_Select_Book_Layer($CategoryID,$Keyword,$YearID);
	break;
	case "LoadBookShowCase":
		$YearID = $_REQUEST['YearID'];
		$CategoryID = $_REQUEST['CategoryID'];
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$SelectedBookID = $_REQUEST['SelectedBookID'];
		
		echo $ReadingGardenUI->Get_Settings_Assign_Reading_Select_Book_Showcase($CategoryID,$Keyword,$YearID,$SelectedBookID);
	break;
	case "LoadBookInfo":
		$BookID = $_REQUEST['BookID'];
		
		echo $ReadingGardenUI->Get_Settings_Assign_Reading_Book_Preview($BookID);
	break;
	case "LoadDefaultAnswerSheet":
		$BookID = $_REQUEST['BookID'];
		
		$BookInfo = $ReadingGardenLib->Get_Book_List($BookID);
		echo $BookInfo[0]['DefaultAnswerSheetID'];
	break;
}	

intranet_closedb();
?>