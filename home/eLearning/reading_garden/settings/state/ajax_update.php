<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "SaveState";
	debug_pr($_REQUEST);
		$StateInfo['StateName'] = trim(stripslashes($_REQUEST['StateName']));
		$StateInfo['Description'] = trim(stripslashes($_REQUEST['Description']));
		$StateInfo['ScoreRequired'] = trim($_REQUEST['ScoreRequired']);
		$StateID = $_REQUEST['StateID'];
		
		if($ImagePath) // Upload image if path exist.
	 		$Path = $ReadingGardenLib->Settings_Upload_State_Logo($ImagePath, $ImagePath_name);
		
		if($Path) // Update Path if upload success 
			$StateInfo['ImagePath'] = $Path;
	 		
	 	if($StateID) // Edit
	 	{
	 		if($ImagePath && !$Path) // Tried to update image but failed
	 			$Msg="ImageUploadFail"; 
 			else
 			{
	 			$Success = $ReadingGardenLib->Settings_Update_State_Info($StateID, $StateInfo);

		 		if(!$Success)
		 		{
		 			$Msg="UpdateUnsuccess";
		 			if($ImagePath) // delete uploaded image
		 				$ReadingGardenLib->Settings_Delete_State_Logo($Path);
		 		}
		 		else
		 		{
		 			if($ImagePath) // delete uploaded image
						$ReadingGardenLib->Settings_Delete_State_Logo($OldImagePath);  // delete old image
		 			$Msg="UpdateSuccess";
		 		}
		 		
	 		}
	 	}
	 	else  // New
	 	{
	 		if(!$Path) // Failed to upload image
	 			$Msg="ImageUploadFail";
	 		else
	 		{
	 			$Success = $ReadingGardenLib->Settings_Insert_State_Info($StateInfo);
	 		
		 		if(!$Success)
		 		{
		 			$Msg="AddUnsuccess";
		 			if($Path) // delete uploaded image
		 				$ReadingGardenLib->Settings_Delete_State_Logo($Path);
		 		}
		 		else
		 		{
		 			$Msg="AddSuccess";
		 		}
	 		}
		 			 		
	 	}
	 	$x .= '<script>';
		$x .= 'window.top.js_Save_Complete("'.$ReadingGardenUI->Get_Return_Message($Msg).'");';
		$x .= '</script>';
		echo $x;	
	break;

	case "RemoveState":
		$StateID = $_REQUEST['StateID'];
		$StateIDArr = explode(",",$StateID);
		$Success = $ReadingGardenLib->Settings_Delete_State($StateIDArr);
		
		if($Success)
			$Msg = "DeleteSuccess";	
		else
			$Msg = "DeleteUnsuccess";
		echo $ReadingGardenUI->Get_Return_Message($Msg);
	break;
}	

intranet_closedb();
?>