<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_StateSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['StateSettings']['MenuTitle'], "", 1);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['ScoringSettings']['MenuTitle'], "../scoring_rule_settings/scoring_rule_settings.php", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_State_Setting_UI();

?>
<script>
function js_Create_State(Msg)
{
	js_Edit_State('0', Msg)
}

function js_Edit_State(StateID, Msg)
{
	if(!StateID && $("input.StateIDChk:checked").val())
		StateID = $("input.StateIDChk:checked").val();
	tb_show("","#TB_inline?height=550&width=750&inlineId=FakeLayer");

	$.post(
		"ajax_reload.php",
		{
			Action:"LoadStateEditLayer",
			StateID:StateID
		},
		function(ReturnData){
			$("div#TB_ajaxContent").html(ReturnData);
			
			if(Msg)
				Get_Return_Message(Msg);
		}
	)

}
function js_Reload_State_Setting_Table()
{
	Block_Element("StateSettingTable");
	$.post(
		"ajax_reload.php",
		{
			Action:"ReloadStateTable"
		},
		function(ReturnData){
			$("#StateSettingTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("StateSettingTable");
		}
	)
}

function js_Delete_Logo()
{
	var html = '<input type="file" name="ImagePath" id="ImagePath" class="Mandatory"  >';
	$("#ImageUpload").html(html);
}

var isAddMore = 0;
function CheckTBForm()
{
	var FormValid = true;
	
	$("div.WarnMsgDiv").hide();
	$("input.Mandatory").each(function(){
		if($(this).val().Trim() == '')
		{
			$("div#WarnBlank"+$(this).attr("id")).show();
			FormValid = false;
		}
	});

	var ValidID = true;
	$("input.Numeric").each(function(){
		if($(this).val().Trim() != '' && (!isInteger($(this).val()) || $(this).val()<0 ))
		{
			$("div#WarnNonInteger"+$(this).attr("id")).show();
			FormValid = false;
			ValidID = false;
		}
	});	
	
	if($("input#ScoreRequired").val().Trim()!='')
	{
		$.post(
			"ajax_reload.php",
			{
				Action:"CheckScoreExist",
				Score:$("input#ScoreRequired").val(),
				StateID:$("#TBForm").find("input#StateID").val()
			},
			function(ReturnData){
				if(ValidID && ReturnData==1)
				{
					$("div#WarnExistScoreRequired").show();
					FormValid = false;
				}	
	
				if(FormValid)
					aj_Save_Form()
			}
		)
	}
}

function aj_Save_Form()
{
	Block_Thickbox();
	
	var obj = document.TBForm;
	obj.action = "ajax_update.php?Action=SaveState";
	obj.target = "FileUploadFrame";
	obj.encoding = "multipart/form-data";
	obj.method = 'post';
	obj.submit();	
	Restore_Form(obj)
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = '';
	formObj.method = 'post';
}

function js_Save_Complete(ReturnMsg)
{
	Get_Return_Message(ReturnMsg);
	
	if(ReturnMsg.substring(0,1)==1)
	{
		if(isAddMore==1)
			js_Create_State(ReturnMsg);
		else
		{
			tb_remove();
			js_Reload_State_Setting_Table()
		}
	}
	
	isAddMore = 0;
	UnBlock_Thickbox();
}

function js_Remove_State()
{
	var StateIDArr = new Array();
	$("input[name='StateID\\[\\]']:checked").each(function(){
		StateIDArr.push($(this).val().toString());
	})
	var StateID = StateIDArr.join(",");

	$.post(
		"ajax_update.php",
		{
			Action:"RemoveState",
			StateID:StateID
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			js_Reload_State_Setting_Table();
		}
	)
}

function js_Check_All_State(checked)
{
	$("input.StateIDChk").attr("checked",checked)
}
</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>