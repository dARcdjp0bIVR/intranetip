<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

//$linterface = new interface_html("default3.html");
//$ReadingGardenUI = new libreadinggarden_ui();
//$ReadingGardenLib = new libreadinggarden();
//
//$CurrentPageArr['eLearningReadingGarden'] = 1;
//$TAGS_OBJ[] = array($Lang['ReadingGarden']['ReadingTarget'], "", 0);
//$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();
//
//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
//$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
//$linterface->LAYOUT_START($ReturnMessage);
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_ReadingTargetSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['ReadingGarden']['ReadingTarget'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);


echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Settings_Student_Reading_Target_Index();

?>
<script language="javascript">
function Get_Multiple_Selection_Values(name)
{
	var opts = $('select[name='+name+'] option:selected');
	var returnArray = new Array();
	opts.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Get_Checked_Checkbox(name)
{
	var elems = $('input[name='+name+']:checked');
	var returnArray = new Array();
	elems.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Check_Positive_Int(n)
{
	var value = parseInt(n);
	var result = /^\d+$/.test(n);
	if (isNaN(value) || value < 0 || result==false)
		return false;
	else
		return true;
}

function Check_All_Reading_Target(checked)
{
	$('input[name="ReadingTargetID\\[\\]"]').attr('checked',checked);
}

function Get_Reading_Target_Table()
{
	Block_Element('DivReadingTargetTable');
	$.post(
		"ajax_load.php",
		{
			"Action":"GetReadingTargetTable",
			"Keyword":encodeURIComponent($('input#Keyword').val())
		},
		function(ReturnData){
			$("#DivReadingTargetTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivReadingTargetTable");
			Scroll_To_Top();
		}
	);
}

function Get_Reading_Target_Edit_Form(is_new, reading_target_id)
{
	if(!reading_target_id){
		reading_target_id = $('input[name="ReadingTargetID\\[\\]"]:checked').val();
	}
	tb_show("<?=$Lang['ReadingGarden']['EditReadingTarget']?>","#TB_inline?height=600&width=750&inlineId=FakeLayer");
	$.post(
		"ajax_load.php",
		{
			"Action":"GetReadingTargetEditForm",
			"IsNew":is_new,
			"ReadingTargetID":reading_target_id 
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		}
	);
}

function Add_Reading_Target()
{
	Block_Thickbox();
	$.post(
		"ajax_update.php",
		{
			"Action":"AddReadingTarget",
			"TargetName":encodeURIComponent($('input#TargetName').val()),
			"StartDate":$('input#StartDate').val(),
			"EndDate":$('input#EndDate').val(),
			"ReadingRequiredCh":$('input#ReadingRequiredCh').val(),
			"ReadingRequiredEn":$('input#ReadingRequiredEn').val(),
			"ReadingRequiredOthers":$('input#ReadingRequiredOthers').val(),
			"BookReportRequiredCh":$('input#BookReportRequiredCh').val(),
			"BookReportRequiredEn":$('input#BookReportRequiredEn').val(),
			"ClassID[]":Get_Multiple_Selection_Values('ClassID[]')
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['AddReadingTargetSuccess']?>');
			else
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['AddReadingTargetFail']?>');
			Get_Reading_Target_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Update_Reading_Target()
{
	Block_Thickbox();
	$.post(
		"ajax_update.php",
		{
			"Action":"UpdateReadingTarget",
			"ReadingTargetID":$('input#ReadingTargetID').val(),
			"TargetName":encodeURIComponent($('input#TargetName').val()),
			"StartDate":$('input#StartDate').val(),
			"EndDate":$('input#EndDate').val(),
			"ReadingRequiredCh":$('input#ReadingRequiredCh').val(),
			"ReadingRequiredEn":$('input#ReadingRequiredEn').val(),
			"ReadingRequiredOthers":$('input#ReadingRequiredOthers').val(),
			"BookReportRequiredCh":$('input#BookReportRequiredCh').val(),
			"BookReportRequiredEn":$('input#BookReportRequiredEn').val(),
			"ClassID[]":Get_Multiple_Selection_Values('ClassID[]')
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingTargetSuccess']?>');
			else
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingTargetFail']?>');
			Get_Reading_Target_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Delete_Reading_Target()
{
	//if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteReadingTarget']?>')){
		Block_Element('DivReadingTargetTable');
		$.post(
			"ajax_update.php",
			{
				"Action":"DeleteReadingTarget",
				"ReadingTargetID[]":Get_Checked_Checkbox('ReadingTargetID[]')
			},
			function(ReturnData){
				if(ReturnData=="1")
					Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingTargetSuccess']?>');
				else
					Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingTargetFail']?>');
				Get_Reading_Target_Table();
				UnBlock_Element("DivReadingTargetTable");
				Scroll_To_Top();
			}
		);
	//}
}

function Check_Reading_Target_Name(ajaxmode)
{
	var target_name = $.trim($('input#TargetName').val());
	var target_name_warning_layer = $('div#TargetNameWarningDiv');
	var old_target_name = $.trim($('input#OldTargetName').val());
/*	//var submit_btn = $('input#submit_btn');
	if(target_name==''){
		target_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputReadingTargetName']?>');
		target_name_warning_layer.css('display','');
		//submit_btn.attr('disabled',true);
		//return false;
	}else{
		target_name_warning_layer.html('');
		target_name_warning_layer.css('display','none');
		//submit_btn.attr('disabled',false);
	}
	//if(old_target_name!='' && target_name==old_target_name){
	//	target_name_warning_layer.html('');
	//	target_name_warning_layer.css('display','none');
	//}
*/	
	if(ajaxmode==1){
		$.post(
			"ajax_update.php",
			{
				"Action":"CheckTargetName",
				"TargetName":encodeURIComponent(target_name),
				"OldTargetName":encodeURIComponent(old_target_name) 
			},
			function(ReturnData){
				if(ReturnData=="2"){
					target_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputReadingTargetName']?>');
					target_name_warning_layer.css('display','');
					//Check_Reading_Target(0);
				}else if(ReturnData=="1"){
					target_name_warning_layer.html('');
					target_name_warning_layer.css('display','none');
					//submit_btn.attr('disabled',false);
					//Check_Reading_Target(1);
				}else{
					target_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['ReadingTargetNameExist']?>');
					target_name_warning_layer.css('display','');
					//submit_btn.attr('disabled',true);
					//Check_Reading_Target(0);
				}
				Check_Overlap_Periods();
			}
		);
	}
}

function Check_Overlap_Periods()
{
	$.post(
		"ajax_update.php",
		{
			"Action":"CheckOverlapPeriods",
			"ReadingTargetID":$.trim($('input#ReadingTargetID').val()),
			"StartDate":$('input#StartDate').val(),
			"EndDate":$('input#EndDate').val(),
			"ClassID[]":Get_Multiple_Selection_Values('ClassID[]') 
		},
		function(ReturnData){
			var target_class_warning = $('div#TargetClassWarningDiv');
			var overlap_obj = $('input#OverlapPeriod');
			if(ReturnData==""){
				overlap_obj.val('0');
				target_class_warning.html('');
				target_class_warning.css('display','none');
			}else{
				var invalid_class = ReturnData.split(',');
				var msg = '';
				var overlap_cnt = 0;
				for(i=0;i<invalid_class.length;i++){
					var class_name = $('#ClassID\\[\\] option[value=\''+invalid_class[i]+'\']').html();
					if(msg=='') msg += class_name;
					else msg += ', '+class_name;
					overlap_cnt+=1;
				}
				overlap_obj.val(overlap_cnt);
				if(msg!=''){
					msg+='&nbsp;<?=$Lang['ReadingGarden']['WarningMsg']['HaveOverlappingPeriods']?>';
					target_class_warning.html(msg);
					target_class_warning.css('display','');
				}else{
					target_class_warning.html('');
					target_class_warning.css('display','none');
				}
			}
			Check_Reading_Target(1);
		}
	);
}

function Check_Reading_Target(issubmit)
{
	// data validation
	//var submit_btn = $('input#submit_btn');
	var error_cnt = 0;
	var error_focus = null;
	var check_target_name = ($('div#TargetNameWarningDiv').css('display')=='none');
	if(check_target_name==false){
		error_cnt+=1;
		error_focus = $('input#TargetName');
	}
	var start_date = $('input#StartDate').val();
	var end_date = $('input#EndDate').val();
	var start_date_obj = document.getElementById('StartDate');
	var end_date_obj = document.getElementById('EndDate');
	var DatePeriodWarningLayer = $('div#DatePeriodWarningDiv');
	if(!check_date_without_return_msg(start_date_obj) || !check_date_without_return_msg(end_date_obj)){
		DatePeriodWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat']?>');
		DatePeriodWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#StartDate');
	}else{
		DatePeriodWarningLayer.html('');
		DatePeriodWarningLayer.css('display','none');
	}
	if(start_date > end_date){
		DatePeriodWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange']?>');
		DatePeriodWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#EndDate');
	}else{
		DatePeriodWarningLayer.html('');
		DatePeriodWarningLayer.css('display','none');
	}
	
	var reading_required_ch = $.trim($('input#ReadingRequiredCh').val());
	var reading_required_en = $.trim($('input#ReadingRequiredEn').val());
	var reading_required_others = $.trim($('input#ReadingRequiredOthers').val());
	var report_required_ch = $.trim($('input#BookReportRequiredCh').val());
	var report_required_en = $.trim($('input#BookReportRequiredEn').val());
	var ReadingRequiredChWarningLayer = $('div#ReadingRequiredChWarningDiv');
	var ReadingRequiredEnWarningLayer = $('div#ReadingRequiredEnWarningDiv');
	var ReadingRequiredOthersWarningLayer = $('div#ReadingRequiredOthersWarningDiv');
	var ReportRequiredChWarningLayer = $('div#BookReportRequiredChWarningDiv');
	var ReportRequiredEnWarningLayer = $('div#BookReportRequiredEnWarningDiv');
	if(!Check_Positive_Int(reading_required_ch)){
		ReadingRequiredChWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#ReadingRequiredCh');
	}else{
		ReadingRequiredChWarningLayer.css('display','none');
	}
	if(!Check_Positive_Int(reading_required_en)){
		ReadingRequiredEnWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#ReadingRequiredEn');
	}else{
		ReadingRequiredEnWarningLayer.css('display','none');
	}
	if(!Check_Positive_Int(reading_required_others)){
		ReadingRequiredOthersWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#ReadingRequiredOthers');
	}else{
		ReadingRequiredOthersWarningLayer.css('display','none');
	}
	if(!Check_Positive_Int(report_required_ch)){
		ReportRequiredChWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#BookReportRequiredCh');
	}else{
		ReportRequiredChWarningLayer.css('display','none');
	}
	if(!Check_Positive_Int(report_required_en)){
		ReportRequiredEnWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#BookReportRequiredEn');
	}else{
		ReportRequiredEnWarningLayer.css('display','none');
	}
	
	var overlap_cnt = parseInt($('input#OverlapPeriod').val());
	var target_class = Get_Multiple_Selection_Values('ClassID[]');
	var target_class_warning = $('div#TargetClassWarningDiv');
	if(overlap_cnt>0){
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('select[name=ClassID\\[\\]]');
	}else if(target_class.length==0){
		target_class_warning.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses']?>');
		target_class_warning.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('select[name=ClassID\\[\\]]');
	}else{
		target_class_warning.html('');
		target_class_warning.css('display','none');
	}
	
	if(error_cnt>0){
		if(error_focus!=null) error_focus.focus();
		return false;
	}
	var isnew = $('input#IsNew').val();
	if(issubmit==1){
		if(isnew==1)
			Add_Reading_Target();
		else
			Update_Reading_Target();
	}
}

$().ready(function(){
	Get_Reading_Target_Table();
})
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>