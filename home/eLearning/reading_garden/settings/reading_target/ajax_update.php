<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "AddReadingTarget":
		$DataArr['TargetName'] = trim(urldecode(stripslashes($_REQUEST['TargetName'])));
		$DataArr['StartDate'] = trim($_REQUEST['StartDate']);
		$DataArr['EndDate'] = trim($_REQUEST['EndDate']);
		$DataArr['ReadingRequiredCh'] = trim($_REQUEST['ReadingRequiredCh']);
		$DataArr['ReadingRequiredEn'] = trim($_REQUEST['ReadingRequiredEn']);
		$DataArr['ReadingRequiredOthers'] = $_REQUEST['ReadingRequiredOthers'];
		$DataArr['BookReportRequiredCh'] = $_REQUEST['BookReportRequiredCh'];
		$DataArr['BookReportRequiredEn'] = $_REQUEST['BookReportRequiredEn'];
		$DataArr['ClassID'] = $_REQUEST['ClassID'];
		$Result = $ReadingGardenLib->Add_Student_Reading_Target($DataArr);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "UpdateReadingTarget":
		$DataArr['ReadingTargetID'] = $_REQUEST['ReadingTargetID'];
		$DataArr['TargetName'] = trim(urldecode(stripslashes($_REQUEST['TargetName'])));
		$DataArr['StartDate'] = trim($_REQUEST['StartDate']);
		$DataArr['EndDate'] = trim($_REQUEST['EndDate']);
		$DataArr['ReadingRequiredCh'] = trim($_REQUEST['ReadingRequiredCh']);
		$DataArr['ReadingRequiredEn'] = trim($_REQUEST['ReadingRequiredEn']);
		$DataArr['ReadingRequiredOthers'] = $_REQUEST['ReadingRequiredOthers'];
		$DataArr['BookReportRequiredCh'] = $_REQUEST['BookReportRequiredCh'];
		$DataArr['BookReportRequiredEn'] = $_REQUEST['BookReportRequiredEn'];
		$DataArr['ClassID'] = $_REQUEST['ClassID'];
		$Result = $ReadingGardenLib->Update_Student_Reading_Target($DataArr);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "DeleteReadingTarget":
		$ReadingTargetID = $_REQUEST['ReadingTargetID'];
		$Result = $ReadingGardenLib->Delete_Student_Reading_Target($ReadingTargetID);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	case "CheckTargetName":
		$ReadingTargetName = trim(urldecode(stripslashes($_REQUEST['TargetName'])));
		$OldReadingTargetName = trim(urldecode(stripslashes($_REQUEST['OldTargetName'])));
		if($ReadingTargetName==""){
			echo "2"; // blank name
		}else if($OldReadingTargetName!="" && $ReadingTargetName == $OldReadingTargetName)
		{
			echo "1"; // ok
		}else{
			$Result = $ReadingGardenLib->Check_Student_Reading_Target_Name($ReadingTargetName);
			if($Result)
				echo "1";//ok
			else
				echo "0";
		}
	break;
	case "CheckOverlapPeriods":
		$StartDate = trim($_REQUEST['StartDate']);
		$EndDate = trim($_REQUEST['EndDate']);
		$ReadingTargetID = trim($_REQUEST['ReadingTargetID']);
		$TargetClassID = $_REQUEST['ClassID'];
		$Result = $ReadingGardenLib->Check_Reading_Target_Overlap_Periods($TargetClassID,$StartDate,$EndDate,'',$ReadingTargetID);
		$InvalidClass=array();
		if(sizeof($Result)>0){
			foreach($Result as $cid=>$cnt){
				if($cnt>0)
					$InvalidClass[]=$cid;
			}
		}
		echo implode(",",$InvalidClass);
	break;
}

intranet_closedb();
?>