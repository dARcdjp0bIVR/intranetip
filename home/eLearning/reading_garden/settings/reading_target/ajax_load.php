<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "GetReadingTargetTable";
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		echo $ReadingGardenUI->Settings_Student_Reading_Target_Table($Keyword);
	break;
	case "GetReadingTargetEditForm":
		$IsNew = $_REQUEST['IsNew'];
		$ReadingTargetID = $_REQUEST['ReadingTargetID'];
		echo $ReadingGardenUI->Settings_Student_Reading_Target_Edit_Form($IsNew,$ReadingTargetID);
	break;
}

intranet_closedb();
?>