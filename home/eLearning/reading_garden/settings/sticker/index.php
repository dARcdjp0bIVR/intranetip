<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_sticker_page_size", "numPerPage");
$arrCookies[] = array("ck_sticker_page_number", "pageNo");
$arrCookies[] = array("ck_sticker_page_order", "order");
$arrCookies[] = array("ck_sticker_page_field", "field");	
$arrCookies[] = array("ck_sticker_page_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_StickerSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['Sticker'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Get_Settings_Report_Sticker_UI($field,$order,$pageNo,$numPerPage);

?>
<script>
var isAddMore = 0;
function Get_Sticker_Table()
{
	var formData = $('#form1').serialize();
	formData += "&Action=GetStickerTable";
	
	Block_Element("DivStickerTable");
	$.ajax({
		type:"POST",
		url:"ajax_reload.php",
		data: formData,  
		success: function(ReturnData) {
			$("#DivStickerTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivStickerTable");
			Scroll_To_Top();
		}
	});
}

function Get_Sticker_Form(StickerID,Msg)
{
	if(StickerID==-1 && $("input[name=StickerID\\[\\]]:checked").val())
		StickerID = $("input[name=StickerID\\[\\]]:checked").val();
	tb_show("<?=$Lang['Btn']['Edit']?>","#TB_inline?height=400&width=550&inlineId=FakeLayer");
	
	$.post(
		"ajax_reload.php",
		{
			Action:"GetStickerEditForm",
			StickerID:StickerID
		},
		function(ReturnData){
			$("div#TB_ajaxContent").html(ReturnData);
			
			if(Msg)
				Get_Return_Message(Msg);
		}
	)
}

function CheckTBForm()
{
	$("div.WarnMsgDiv").hide();
	var sticker_id = $.trim($("#TBForm").find("input#StickerID").val());
	var upload_image = $.trim($('input#UploadImage').val());
	if(upload_image==''){
		$("div.WarnMsgDiv").show();
	}else {
		Block_Thickbox();
		var obj = document.TBForm;
		obj.action = "ajax_reload.php?Action=SaveSticker";
		obj.target = "FileUploadFrame";
		obj.encoding = "multipart/form-data";
		obj.method = 'post';
		obj.submit();
		Restore_Form(obj);
	}
}

function Save_Complete(ReturnMsg)
{
	Get_Return_Message(ReturnMsg);
	
	if(isAddMore==1)
		Get_Sticker_Form('',ReturnMsg);
	else
	{
		tb_remove();
	}
	
	isAddMore = 0;
	Get_Sticker_Table();
	UnBlock_Thickbox();
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = '';
	formObj.method = 'post';
}

$().ready(function(){
	Get_Sticker_Table();
})
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>