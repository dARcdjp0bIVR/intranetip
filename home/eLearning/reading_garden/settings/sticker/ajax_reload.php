<?php
// using : 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "GetStickerTable":
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		
		$arrCookies[] = array("ck_sticker_page_size", "numPerPage");
		$arrCookies[] = array("ck_sticker_page_number", "pageNo");
		$arrCookies[] = array("ck_sticker_page_order", "order");
		$arrCookies[] = array("ck_sticker_page_field", "field");	
		$arrCookies[] = array("ck_sticker_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Settings_Report_Sticker_DBTable($field, $order, $pageNo, $numPerPage, $Keyword);
	break; 
	case "GetStickerEditForm":
		$StickerID = stripslashes(trim($_REQUEST['StickerID']));
		echo $ReadingGardenUI->Get_Settings_Report_Sticker_Edit_Form($StickerID);
	break;
	case "SaveSticker":
		$StickerID = trim($_REQUEST['StickerID']);
		
		if($StickerID==''){
			// New sticker
			$tmp_location = $_FILES['UploadImage']['tmp_name'];
			$image_name = $_FILES['UploadImage']['name'];
			$error = $_FILES['UploadImage']['error'];
			if($error>0 || !exif_imagetype($tmp_location))
			{
				// fail with error
				$Msg = "AddUnsuccess";
			}else
			{
				$DataArr = array();
				$DataArr['ImageLocation'] = $tmp_location;
				$DataArr['ImageName'] = $image_name;
				$success = $ReadingGardenLib->Settings_Manage_Report_Sticker("add",$DataArr);
				if($success)
					$Msg = "AddSuccess";
				else
					$Msg = "AddUnsuccess";
			}
			$x  = '<script>';
			$x .= 'window.top.Save_Complete("'.$ReadingGardenUI->Get_Return_Message($Msg).'");';
			$x .= '</script>';
			echo $x;
		}else{
			// Edit sticker
			$tmp_location = $_FILES['UploadImage']['tmp_name'];
			$image_name = $_FILES['UploadImage']['name'];
			$error = $_FILES['UploadImage']['error'];
			if($error>0 || !exif_imagetype($tmp_location))
			{
				// fail with error
				$Msg = "UpdateUnsuccess";
			}else
			{
				$DataArr = array();
				$DataArr['StickerID'] = $StickerID;
				$DataArr['ImageLocation'] = $tmp_location;
				$DataArr['ImageName'] = $image_name;
				$success = $ReadingGardenLib->Settings_Manage_Report_Sticker("update",$DataArr);
				if($success)
					$Msg = "UpdateSuccess";
				else
					$Msg = "UpdateUnsuccess";
			}
			$x  = '<script>';
			$x .= 'window.top.Save_Complete("'.$ReadingGardenUI->Get_Return_Message($Msg).'");';
			$x .= '</script>';
			echo $x;
		}
	break;
}	

intranet_closedb();
?>