<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{

	case "ReloadCategoryTable":
		echo $ReadingGardenUI->Get_Settings_Book_Category_Setting_Table();
	break; 

	case "LoadCategorySettingTBLayer":
	
		$CategoryID = $_REQUEST['CategoryID'];
		$Language = $_REQUEST['Language'];

		echo $ReadingGardenUI->Get_Settings_Book_Category_Edit_Layer($Language, $CategoryID);
	break; 
	
	case "CheckCategoryCodeExist";
		
		$CategoryCode = trim($_REQUEST['CategoryCode']);
		$Language = $_REQUEST['Language'];
		$CategoryID = $_REQUEST['CategoryID'];
		
		echo $ReadingGardenLib->Check_Category_Code($CategoryCode, $Language, $CategoryID);
	break;
}	

intranet_closedb();
?>