<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_StateSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['StateSettings']['MenuTitle'], "../state/state.php", 0);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['ScoringSettings']['MenuTitle'], "", 1);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Get_Settings_Scoring_Rule_Setting_UI();

?>
<script>
function CheckForm()
{
	var FormValid = true;
	
	$("div.WarnDiv").hide();
	$("input.NumericField").each(function(){
		var thisVal = $(this).val();
		var thisID = $(this).attr("id");
		
		if(thisVal.Trim()!='')
		{
			if(isNaN(thisVal))
			{
				$("div."+thisID+"WarnMsg").show();
				$(this).focus();
				FormValid = false;			
			}
		}
	})
	
	return FormValid;
}
</script>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>