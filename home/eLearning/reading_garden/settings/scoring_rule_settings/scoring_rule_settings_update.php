<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Success = $ReadingGardenLib->Settings_Save_Scoring_Rule_Settings($ScoringSettings);

if($Success)
{
	$Msg = "SettingsSaveSuccess";
}
else
{
	$Msg = "SettingsSaveFail";
}
header("location: scoring_rule_settings.php?Msg=".urlencode($Msg));


intranet_closedb();
?>