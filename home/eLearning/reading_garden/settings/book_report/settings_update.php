<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

//debug_pr($_POST);
//die;
$ReadingGardenLib = new libreadinggarden();

$SettingArr['FileUpload'] = $_POST['FileUpload']?$_POST['FileUpload']:0;
$SettingArr['FileUploadDesc'] = $SettingArr['FileUpload']?$_POST['FileUploadDesc']:"";
$SettingArr['OnlineWriting'] = $_POST['OnlineWriting']?$_POST['OnlineWriting']:0;
$SettingArr['OnlineWritingDesc'] = $SettingArr['OnlineWriting']?$_POST['OnlineWritingDesc']:"";

//debug_pr($SettingArr);
$Success = $ReadingGardenLib->Settings_Update_Settings($SettingArr);

if($Success)
{
	$Msg = "SettingsSaveSuccess";
}
else
{
	$Msg = "SettingsSaveFail";
}
header("location: settings.php?Msg=".urlencode($Msg));


intranet_closedb();
?>