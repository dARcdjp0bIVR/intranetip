<?php
// using : 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_BookReportSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['BookReportSettings']['MenuTitle'], "", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Get_Book_Report_Setting_Index_UI();

?>
<script language='javascript'>
$().ready(function(){
	js_Display_View();
	js_Disable_Desc("FileUpload");
	js_Disable_Desc("OnlineWriting");
});

function js_Display_View()
{
	$(".DisplayView").show();
	$(".EditView").hide();
}

function js_Edit_View()
{
	$(".DisplayView").hide();
	$(".EditView").show();
}

function js_Disable_Desc(tname)
{
	var Status = $("."+tname+":checked").val();
	if(Status==1)
	{
		$("#"+tname+"Desc").attr("disabled","")
	}
	else
	{
		$("#"+tname+"Desc").attr("disabled","disabled")
	}
}

//function js_Check_Form()
//{
//	var FormValid = true;
//	$("div.WarnMsg").hide();
//	
//	if($("input.BookingMethod:checked").length==0)
//	{
//		$("div#WarnSelectBookingMethod").show();
//		FormValid = false;
//	}
//	
//	if(!FormValid)
//		return false;
//}
</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>