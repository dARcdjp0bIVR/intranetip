<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "GetAwardSchemeTable";
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$AwardLanguage = $_REQUEST['AwardLanguage'];
		echo $ReadingGardenUI->Settings_Award_Scheme_Table($Keyword,$AwardLanguage);
	break;
	case "GetAwardEditForm":
		$IsNew = $_REQUEST['IsNew'];
		$AwardSchemeID = $_REQUEST['AwardSchemeID'];
		echo $ReadingGardenUI->Settings_Award_Scheme_Edit_Form($IsNew,$AwardSchemeID);
	break;
	case "GetAwardLevelEntry":
		$AwardLevelID =	trim($_REQUEST['AwardLevelID']);
		$EntryNumber = $_REQUEST['EntryNumber'];
		$AwardType = $_REQUEST['AwardType'];
		echo $ReadingGardenUI->Settings_Get_Award_Level_Entry($EntryNumber,$AwardLevelID, $AwardType);
	break;
}

intranet_closedb();
?>