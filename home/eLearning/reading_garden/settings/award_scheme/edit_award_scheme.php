<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenUI = new libreadinggarden_ui();
$ReadingGardenLib = new libreadinggarden();

$CurrentPage = "Settings_AwardScheme";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['ReadingGarden']['AwardScheme'], "", 0);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

//$customLeftMenu = $ReadingGardenUI->printLeftMenu();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$linterface->LAYOUT_START($ReturnMessage);

$AwardSchemeID = trim(stripslashes($_REQUEST['AwardSchemeID']));

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Jquery_UI_JS();
//echo $ReadingGardenUI->Settings_Award_Scheme_Index();
echo $ReadingGardenUI->Settings_Award_Scheme_Edit_Form($AwardSchemeID=="",$AwardSchemeID);

?>
<script language="javascript">
function Get_Multiple_Selection_Values(name)
{
	var opts = $('select[name='+name+'] option:selected');
	var returnArray = new Array();
	opts.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Get_Element_Values(name)
{
	var elems = $('input[name='+name+']');
	var returnArray = new Array();
	elems.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Get_Checked_Checkbox(name)
{
	var elems = $('input[name='+name+']:checked');
	var returnArray = new Array();
	elems.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Check_Positive_Int(n)
{
	var value = parseInt(n);
	var result = /^\d+$/.test(n);
	if (isNaN(value) || value < 0 || result==false)
		return false;
	else
		return true;
}

function Add_Requirement_Item(entry_number)
{
	var container = $('div#DivRequirement'+entry_number);
	
	var category = $('select#CategoryID'+entry_number+' option:selected');
	var cat_val = category.val();
	var cat_text = category.text();
	var exist_cat_items = $('input[name=RequirementItemCategory'+entry_number+'\\[\\]]');
	var is_cat_exist = false;
	exist_cat_items.each(
		function(index){
			if($(this).val()==cat_val)
				is_cat_exist = true;
		}
	);
	if(is_cat_exist){
		$('div#CategoryWarningLayer'+entry_number).html('<?=$Lang['ReadingGarden']['WarningMsg']['CategoryAlreadyAdded']?>');
		$('div#CategoryWarningLayer'+entry_number).css('display','');
		return;
	}else{
		$('div#CategoryWarningLayer'+entry_number).html('');
		$('div#CategoryWarningLayer'+entry_number).css('display','none');
	}
	var item_cat = htmlspecialchars(cat_text)+'<input type="hidden" name="RequirementItemCategory'+entry_number+'[]" value="'+cat_val+'"/>';
	var item_quantity = '<input name="RequirementItemQuantity'+entry_number+'[]" type="text" maxlength="10" size="10" value="1"/>';
	//var item_delete_btn = '<span class="table_row_tool row_content_tool"><a title="<?=$Lang['Btn']['Delete']?>" class="delete_dim" href="#" onclick="$(this).parent().parent().remove();return false;"></a><span>';
	var item_delete_btn = '<a href="javascript:void(0);" onclick="$(this).parent().remove();"><img border="0" src="<?=$image_path."/".$LAYOUT_SKIN."/icon_delete.gif"?>" title="<?=$Lang['Btn']['Delete']?>" /></a>';
	container.append('<div>'+item_cat + '&nbsp;' + item_quantity + '&nbsp;' + item_delete_btn + '&nbsp;</div>');
}

function Check_Award(issubmit)
{
	// data validation
	var submit_btn = $('input#submit_btn');
	var error_cnt = 0;
	var error_focus = null;
	
	var award_type = $('#AwardType').val();
	var target_class = Get_Multiple_Selection_Values('ClassID[]');
	var TargetClassWarningLayer = $('div#TargetClassWarningDiv');
	if(award_type==2 && target_class.length<=1){// inter-class
		TargetClassWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastTwoClasses']?>');
		TargetClassWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('select[name="ClassID\\[\\]"]');
	}else if(award_type==1 && target_class.length==0){// individual
		TargetClassWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses']?>');
		TargetClassWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('select[name="ClassID\\[\\]"]');
	}else{
		TargetClassWarningLayer.html('');
		TargetClassWarningLayer.css('display','none');
	}
	
	var start_date = $('input#StartDate').val();
	var end_date = $('input#EndDate').val();
	var start_date_obj = document.getElementById('StartDate');
	var end_date_obj = document.getElementById('EndDate');
	var DatePeriodWarningLayer = $('div#DatePeriodWarningDiv');
	if(!check_date_without_return_msg(start_date_obj) || !check_date_without_return_msg(end_date_obj)){
		DatePeriodWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat']?>');
		DatePeriodWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#StartDate');
	}else{
		DatePeriodWarningLayer.html('');
		DatePeriodWarningLayer.css('display','none');
	}
	
	if(start_date > end_date){
		DatePeriodWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange']?>');
		DatePeriodWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#StartDate');
	}else{
		DatePeriodWarningLayer.html('');
		DatePeriodWarningLayer.css('display','none');
	}
	
	var publish_date = $('input#ResultPublishDate').val();
	var publish_date_obj = document.getElementById('ResultPublishDate');
	var ResultPublishDateWarningLayer = $('div#ResultPublishDateWarningDiv');
	if(publish_date<end_date)
	{
		ResultPublishDateWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['InvalidPublishDate']?>');
		ResultPublishDateWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#ResultPublishDate');
	}else{
		ResultPublishDateWarningLayer.html('');
		ResultPublishDateWarningLayer.css('display','none');
	}
	
	// 	level checking
	// check at least 1 general award level
	var EmptyAwardLevelWarningDiv = $('div#EmptyAwardLevelWarningDiv');
	
	if($("div#AwardLevelDiv>div.AwardLevelEntryClass").length==0)
	{
		EmptyAwardLevelWarningDiv.show();
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('div#AwardLevelDiv');
	}else{
		EmptyAwardLevelWarningDiv.hide();
	}	
	
	var entry_nums = document.getElementsByName('AwardLevelEntryNumber[]');
	
	for(i=0;i<entry_nums.length;i++)
	{
		var n = entry_nums[i].value;
		var level = $.trim($('input#Level'+n).val());
		var level_name = $.trim($('input#LevelName'+n).val());
		var LevelWarningLayer = $('div#LevelWarningDiv'+n);
		var LevelNameWarningLayer = $('div#LevelNameWarningDiv'+n);
		
		// check level
//		if(level == '' || !Check_Positive_Int(level)){
//			LevelWarningLayer.css('display','');
//			error_cnt+=1;
//			if(error_focus!=null) error_focus = $('input#Level'+n);
//		}else{
//			LevelWarningLayer.css('display','none');
//		}
		
		// check level name
		if(level_name == ''){
			LevelNameWarningLayer.css('display','');
			error_cnt+=1;
			if(error_focus!=null) error_focus = $('input#LevelName'+n);
		}else{
			LevelNameWarningLayer.css('display','none');
		}
		
		var min_book = $.trim($('input#MinBookRead'+n).val());
		var min_report = $.trim($('input#MinReportSubmit'+n).val());
		var MinBookWarningLayer = $('div#MinBookWarningDiv'+n);
		var MinReportWarningLayer = $('div#MinReportWarningDiv'+n);
		
		// check book read
		if(min_book == '' || !Check_Positive_Int(min_book)){
			MinBookWarningLayer.css('display','');
			error_cnt+=1;
			if(error_focus!=null) error_focus = $('input#MinBookRead'+n);
		}else{
			MinBookWarningLayer.css('display','none');
		}
		
		// check report num
		if(min_report == '' || !Check_Positive_Int(min_report)){
			MinReportWarningLayer.css('display','');
			error_cnt+=1;
			if(error_focus!=null) error_focus = $('input#MinReportSubmit'+n);
		}else{
			MinReportWarningLayer.css('display','none');
		}
		
		// check category quantity
		var CategoryWarningLayer = $('div#CategoryWarningLayer'+n);
		var RequireItem = $('input[name=RequirementItemQuantity'+n+'\\[\\]]');
		var quantity_valid = true;
		RequireItem.each(
			function(index){
				var amount = $(this).val();
				if(!Check_Positive_Int(amount)){
					quantity_valid = false;
				}
			}
		);
		if(!quantity_valid){
			CategoryWarningLayer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger']?>');
			CategoryWarningLayer.css('display','');
			error_cnt+=1;
			if(error_focus!=null) error_focus = $('#CategoryID'+n);
		}else{
			CategoryWarningLayer.html('');
			CategoryWarningLayer.css('display','none');
		}
	}
	
	if(error_cnt>0){
		if(error_focus != null) error_focus.focus();
		return false;
	}
	var isnew = $('input#IsNew').val();
	if(issubmit==1){
		checkOption(document.getElementById("AttachmentArr[]"));	
		checkOptionAll(document.getElementById("AttachmentArr[]"));
		if(isnew==1)
			//Add_Award();
			document.AwardEditForm.submit();
		else
		{
			if($("input#WinnerGenerated").val()!=1 || confirm("<?=$Lang['ReadingGarden']['ConfirmArr']['WinnerListWasGenerated']?>"))
			{
				//Update_Award();
				document.AwardEditForm.submit();
			}
		}
	}
}

function Check_Award_Name(ajaxmode)
{
	var award_name = $.trim($('input#AwardName').val());
	var award_name_warning_layer = $('div#AwardNameWarningDiv');
	var old_award_name = $('input#OldAwardName').val();
	var submit_btn = $('input#submit_btn');
	if(award_name==''){
		award_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['RequestInputAwardName']?>');
		award_name_warning_layer.css('display','');
		$('input#AwardName').focus();
		//submit_btn.attr('disabled',true);
		//return false;
	}else{
		award_name_warning_layer.html('');
		award_name_warning_layer.css('display','none');
		//submit_btn.attr('disabled',false);
	}
	//if(award_name==old_award_name) return true;
	
	if(ajaxmode==1){
		if(award_name!=''){
			$.post(
				"ajax_update.php",
				{
					"Action":"CheckAwardName",
					"AwardName":encodeURIComponent(award_name),
					"OldAwardName":encodeURIComponent(old_award_name) 
				},
				function(ReturnData){
					if(ReturnData=="1"){
						award_name_warning_layer.html('');
						award_name_warning_layer.css('display','none');
						//submit_btn.attr('disabled',false);
					}else{
						award_name_warning_layer.html('<?=$Lang['ReadingGarden']['WarningMsg']['AwardNameExist']?>');
						award_name_warning_layer.css('display','');
						$('input#AwardName').focus();
						//submit_btn.attr('disabled',true);
					}
					Check_Award(1);
				}
			);
		}else{
			Check_Award(1);
		}
	}
}

function Add_Award_Level_Entry(award_level_id, AwardType)
{
	var AwardType = AwardType || '';
	
	var entry_number = $('input#EntryNumber');
	var new_entry_number = parseInt(entry_number.val(),10)+1;
	$.post(
		"ajax_load.php",
		{
			Action : "GetAwardLevelEntry",
			AwardLevelID : award_level_id,
			EntryNumber : new_entry_number,
			AwardType: AwardType
		},
		function(ReturnData){
			entry_number.val(new_entry_number);
			if(AwardType==1)
				$('div#AwardLevelDiv').append(ReturnData);
			else
				$('div#AdditionalAwardDiv').append(ReturnData);
		}
	);
}

$().ready(function(){
	
	$('#AwardLevelDiv').sortable({
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			axis: 'y',
			opacity: '0.7',
			tolerance: 'pointer'
		});
})

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>