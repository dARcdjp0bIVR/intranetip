<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CommentID = $_REQUEST['CommentID'];
$Result = $ReadingGardenLib->Settings_Manage_Comment_Bank("delete",$CommentID);

if($Result)
	$Msg = "DeleteSuccess";
else
	$Msg = "DeleteUnsuccess";

$ReturnMsg = $ReadingGardenUI->Get_Return_Message($Msg);

intranet_closedb();
header("Location: index.php?Msg=".urlencode($ReturnMsg));
?>