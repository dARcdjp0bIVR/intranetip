<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();
$ParArr['RecommendBookID'] = $RecommendBookID;

$Success = $ReadingGardenLib->Remove_Recommend_Book_From_Form($ParArr);

$Msg = $Success==1?"DeleteSuccess":"DeleteUnsuccess";

header("location:recommend_book_list.php?Msg=$Msg");


intranet_closedb();
?>