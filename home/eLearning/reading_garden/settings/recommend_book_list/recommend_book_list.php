<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/readinggarden_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_recommend_book_list_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_recommend_book_list_page_number", "pageNo");
$arrCookies[] = array("ck_settings_recommend_book_list_page_order", "order");
$arrCookies[] = array("ck_settings_recommend_book_list_page_field", "field");	
$arrCookies[] = array("ck_settings_recommend_book_list_page_keyword", "Keyword");
$arrCookies[] = array("ck_settings_recommend_book_list_page_YearID", "YearID");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_BookSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['BookSettings']['AllBooks'], $PATH_WRT_ROOT."home/eLearning/reading_garden/books/", 0);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['RecommendBookSettings']['MenuTitle'], "recommend_book_list.php", 1);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));

$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();

//echo $ReadingGardenUI->Include_AutoComplete_JS_CSS();

echo $ReadingGardenUI->Get_Settings_Recommend_Book_List_UI($YearID,$field,$order,$pageNo,$numPerPage);

?>
<script>

function js_Relaod_Book_List()
{
	var jsSubmitString = $("#form1").serialize();
	jsSubmitString += '&Action=ReloadReccomendBookListDBTable';
	
	Block_Element("BookListDBTable");
	$.ajax({  
		type: "POST",  
		url: "ajax_reload.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			$("#BookListDBTable").html(ReturnData);
			UnBlock_Element("BookListDBTable");
			js_Init_Preload_Image();
		} 
	});
}

function sortPage(order,field,formObj)
{
	formObj.order.value = order;
	formObj.field.value = field;
	formObj.pageNo.value = 1;
	js_Relaod_Book_List();
}
var isMappingUdapte = 0;
function js_Add_Book()
{
	var YearID = $("#YearID").val();
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadAddRecommendBookLayer",
			YearID:YearID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			isMappingUdapte = 0;
		}
	)
}

function LoadBook(SkipBlockUI)
{
	var AssignCategoryID = $("#AssignCategoryID").val();
	var YearID = $("#YearID").val();
	var Keyword = $("#BookKeyword").val();
	if(!SkipBlockUI)
		Block_Thickbox();
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadBookShowCase",
			YearID:YearID,
			CategoryID:AssignCategoryID,
			Keyword:Keyword
		},
		function(ReturnData)
		{
			$('div#ItemShowcase').html(ReturnData);
			UnBlock_Thickbox();
		}
	)
	
}

function js_Select_Book(BookID)
{
	if($(".Book_"+BookID).hasClass("booking_table_item_select"))
		Action = 2;
	else
		Action = 1;
		
	js_Add_Delete_Form_Book_Mapping(BookID, Action)
		
}

function js_Add_Delete_Form_Book_Mapping(BookID, ActionType)
{
	var YearID = $("#YearID").val();
	var Action = ActionType==1?"AddFormBookMapping":"DeleteFormBookMapping";
	Block_Thickbox();
	$.post(
		"ajax_update.php",
		{
			Action:Action,
			YearID:YearID,
			BookID:BookID
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
			isMappingUdapte = 1;
			LoadBook(1);
		}
	)
}

$().ready(function(){
	js_Relaod_Book_List();	
//	
});
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>