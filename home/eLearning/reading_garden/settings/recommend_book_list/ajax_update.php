<?php
// using :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "AddFormBookMapping":
		$ParArr['YearID'] = $_REQUEST['YearID'];
		$ParArr['BookID'] = $_REQUEST['BookID'];
		
		if($ReadingGardenLib->Assign_Recommend_Book_To_Form($ParArr))
			echo $ReadingGardenUI->Get_Return_Message("AddSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("AddUnSuccess");
	break; 
	
	case "DeleteFormBookMapping":
		$ParArr['YearID'] = $_REQUEST['YearID'];
		$ParArr['BookID'] = $_REQUEST['BookID'];

		if ($ReadingGardenLib->Remove_Recommend_Book_From_Form($ParArr))
			echo $ReadingGardenUI->Get_Return_Message("DeleteSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteUnSuccess");
	break; 	
}	

intranet_closedb();
?>