<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/libform.php");

$key = trim($_REQUEST['key']);
if($key!=''){
	$valid_key = libreadinggarden::Grant_View_Right_For_Report_Page($key);
	if(!$valid_key) exit;
}else{
	intranet_auth();
}
//intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$lform = new libform();

$ReadingRecordID = $_REQUEST['reading_record_id'];
$AssignedReadingID = $_REQUEST['assigned_reading_id'];
$BookReportID = $_REQUEST['book_report_id'];
$inframe = $_REQUEST['inframe']; // optional parameter

$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report($BookReportID);
$AnswerSheetQue = $ReadingGardenLib->Get_Answer_Sheet_From_Assigned_Reading_Or_ReadingRecord($AssignedReadingID,$ReadingRecordID);
if(!$AnswerSheetQue)
{
	$TemplateInfo = $ReadingGardenLib->Get_Answer_Sheet_Template($BookReportInfo[0]['StudentSelectedAnswerSheetID']);
	$AnswerSheetQue = $TemplateInfo[0]['AnswerSheet'];
}

$AnswerSheetAns = $BookReportInfo[0]['AnswerSheet'];
$Grade = trim($BookReportInfo[0]['Grade']);

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/reading_garden_form_edit.js"></script>
<script language="Javascript">
var replyslip = '<?=$Lang['ReadingGarden']['AnswerSheet']?>';
</script>

<br />
<p>
<table width="80%" align="center" border="0">
<form name="ansForm" method="post" action="">
        <input type="hidden" id="qStr" name="qStr" value="<?=htmlspecialchars($AnswerSheetQue,ENT_QUOTES)?>">
        <input type="hidden" id="aStr" name="aStr" value="<?=htmlspecialchars($AnswerSheetAns,ENT_QUOTES)?>">
</form>

<tr>
	<td align="left">
		<div id="AnswerSheetDiv"></div>
	    <script language="Javascript">
	    <?=$lform->getWordsInJS()?>
	    var MarksTxt = '<?=$Lang['ReadingGarden']['Mark(s)']?>';
		var AnswerTxt = '<?=$Lang['ReadingGarden']['Answer']?>';
	    var s = $('input#qStr').val();
		s = s.replace(/"/g, '&quot;');
	    var sheet= new Answersheet();
	    // attention: MUST replace '"' to '&quot;'
	    sheet.qString=s;
	    s = $('input#aStr').val();
	    s = s.replace(/"/g, '&quot;');
	    sheet.aString=s;
	    //edit submitted application
	    sheet.mode=1;
	    <?php
	    if($_SESSION['UserType']==1 || $Grade!=""){
	    ?>
	    sheet.autoMark = true; 
	    <?php
	    }
	    ?>
	    sheet.answer=sheet.sheetArr();
	    
	    Part3 = '';
	   	//document.write(editPanel());
	   	$(document).ready(function(){
			$('div#AnswerSheetDiv').html(editPanel());
			$('div#blockInput').html(sheet.writeSheet());
<?php if($_SESSION['UserType']==1){ ?>
			CalcTotalMark();
<?php } ?>
		});
		
		function CalcTotalMark()
		{
			var total = 0;
			if(typeof(sheet.userScore)=='undefined') return false;
			for(var i=0;i<sheet.userScore.length;i++){
				total += sheet.userScore[i];
			}
			var edit_dom = $(window.parent.frames["EditFrame"].document);
			if(typeof(edit_dom)=='undefined') return false;
			var grade_field = edit_dom.find('input#Grade');
			if(typeof(grade_field)!='undefined' && $.trim(grade_field.val())=='')
				grade_field.val(total);
		}
	    </script>
	</td>
</tr>
<?php
if($inframe!="1")
{
?>
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>

<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
<?php
}
?>
</table>
<br />
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
if($key!=''){
	unset($_SESSION['UserID']);
}
?>