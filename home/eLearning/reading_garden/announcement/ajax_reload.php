<?php
// using :
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadAnnouncementArea":
		$AnnouncementType = $_REQUEST['AnnouncementType'];
		
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
	
		$arrCookies = array();
		$arrCookies[] = array("ck_announcement_list_keyword", "Keyword");
		$arrCookies[] = array("ck_announcement_list_page_size", "numPerPage");
		$arrCookies[] = array("ck_announcement_list_page_number", "pageNo");
		$arrCookies[] = array("ck_announcement_list_page_order", "order");
		$arrCookies[] = array("ck_announcement_list_page_field", "field");	
		$arrCookies[] = array("ck_announcement_list_page_AnnouncementType", "AnnouncementType");
		updateGetCookies($arrCookies);

		echo $ReadingGardenUI->Get_Announcement_List_DBTable($ClassID,$AnnouncementType,$field,$order,$pageNo,$numPerPage,$Keyword);
	break;	

}	

intranet_closedb();
?>