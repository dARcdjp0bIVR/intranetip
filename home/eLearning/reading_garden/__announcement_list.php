<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Announcement_List_DBTable_UI($ClassID);

?>
<script>
$().ready(function(){
	js_Reload_Student_Reading_Record(); 
})

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>