// editing by 

// 2013-02-20 (Rita): modified Add_Reading_Record() - add "times", "PopularAwardSchemeItem" variables for customization [#2012-1009-1402-14054] 

// common js function
// all js message are init in $ReadingGardenUI-> Include_Reading_Garden_CSS();

var js_LAYOUT_SKIN = "2009a";
var loading = "<img src='"+js_PATH_WRT_ROOT + "/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";
var icon_book_path = js_PATH_WRT_ROOT + "/images/" + js_LAYOUT_SKIN + "/icon_pic.gif";
function js_Like(FunctionName, FunctionID)
{
	$("."+FunctionName+"_"+FunctionID+"_like_div").html(loading);
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			Action:"AddLikeRecord",
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			$("."+FunctionName+"_"+FunctionID+"_like_div").html(ReturnData);
		}
	)
}

function js_Unlike(FunctionName, FunctionID)
{
	$("."+FunctionName+"_"+FunctionID+"_like_div").html(loading);
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			Action:"DeleteLikeRecord",
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			$("."+FunctionName+"_"+FunctionID+"_like_div").html(ReturnData);
		}
	)
}

function js_Reload_Like_Div(FunctionName, FunctionID)
{
	$("."+FunctionName+"_"+FunctionID+"_like_div").html(loading);
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			Action:"ReloadLikeDiv",
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			$("."+FunctionName+"_"+FunctionID+"_like_div").html(ReturnData);
		}
	)
	
}

function js_Open_Like_List(obj, FunctionName, FunctionID)
{
	$(obj).after('<div class="like_list_layer">'+loading+'</div>');
	
	var pos = $(obj).position()
	$(".like_list_layer").css(
		{
			'position':'absolute',
			'left': pos.left+15,
			'top': pos.top+20
		}
	)	
	
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			Action:"LoadLikeList",
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			$(".like_list_layer").html(ReturnData);
		}
	)
}

function js_Hide_Like_List()
{
	$(".like_list_layer").remove();
}

function js_Add_Comment( FunctionName, FunctionID)
{
	if(FunctionName=='Report')
	{
		newWindow(js_PATH_WRT_ROOT+"home/eLearning/reading_garden/recommend_report/report_comment.php?clearCoo=1&BookReportID="+FunctionID,10)
	}
}
// View Reading Record
var Cache_Reading_Record_Info = new Object();
function View_Reading_Record(obj, ReadingRecordID)
{
	var ReadingRecordLayer = '<div class="reading_record_layer">'+loading+'</div>';
	var thisObj = $(obj);
	thisObj.after(ReadingRecordLayer);
	var pos = thisObj.position();
	
//	if($(obj).find("a.book_status_read").attr("onclick"))
//	{
//		$(obj).find("a.book_status_read").html(js_EditReadingRecord)
//		$(obj).find("a.book_status_read").attr("title","")
//	}
	$(".reading_record_layer").css(
		{
			'position':'absolute',
			'left': pos.left+20,
			'top': pos.top+20
		}
	);
	
	if(Cache_Reading_Record_Info[ReadingRecordID])
	{
		$(".reading_record_layer").html(Cache_Reading_Record_Info[ReadingRecordID]);
	}
	else
	{
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
			{
				"Action":"GetReadingRecordLayer",
				"ReadingRecordID":ReadingRecordID 
			},
			function(ReturnData){
				$(".reading_record_layer").html(ReturnData);
				Cache_Reading_Record_Info[ReadingRecordID] = ReturnData;
			}
		);
	}
}

function Remove_Reading_Record_Layer()
{
//	$("a.book_status_read").html(js_Read)
	$(".reading_record_layer").remove();
}

// Add Reading Record
function Get_Reading_Record_Edit_Form(BookID, IsNew, ReadingRecordID, CallBack)
{
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			"Action":"GetReadingRecordEditForm",
			"IsNew":IsNew,
			"BookID":BookID,
			"ReadingRecordID":ReadingRecordID,
			"CallBack": CallBack
		},
		function(ReturnData) {
			if (ReturnData == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(ReturnData);
		});
}

function Check_Reading_Record(issubmit, CallBack)
{
	var start_date = $('input#StartDate').val();
	var finished_date = $('input#FinishedDate').val();
	var start_date_obj = document.getElementById('StartDate');
	var finished_date_obj = document.getElementById('FinishedDate');
	var reading_hours = $('input#ReadingHours').val();
	var progress = $('input#Progress').val();
	var error_cnt = 0;
	var error_focus = null;
	var today = getToday();
	
	var StartDateWarningLayer = $('div#StartDateWarningDiv');
	var FinishedDateWarningLayer = $('div#FinishedDateWarningDiv');
	
	StartDateWarningLayer.html('');
	StartDateWarningLayer.css('display','none');
	FinishedDateWarningLayer.html('');
	FinishedDateWarningLayer.css('display','none');
	
	if(!check_date_without_return_msg(start_date_obj)){
		StartDateWarningLayer.html(js_InvalidDateFormat);
		StartDateWarningLayer.css('display','');
		error_cnt+=1;
		error_focus = $('input#StartDate');
	}
	//else{
	//	StartDateWarningLayer.html('');
	//	StartDateWarningLayer.css('display','none');
	//}
	if(!check_date_without_return_msg(finished_date_obj)){
		FinishedDateWarningLayer.html(js_InvalidDateFormat);
		FinishedDateWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#FinishedDate');
	}
	//else{
	//	FinishedDateWarningLayer.html('');
	//	FinishedDateWarningLayer.css('display','none');
	//}
	
	if(start_date > finished_date){
		StartDateWarningLayer.html(js_StartDateGreaterThanEndDate);
		StartDateWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#StartDate');
	}
	//else{
	//	FinishedDateWarningLayer.html('');
	//	FinishedDateWarningLayer.css('display','none');
	//}
	
	if(start_date>today){
		StartDateWarningLayer.html(js_StartDateGreaterThanToday);
		StartDateWarningLayer.css('display','');
		error_cnt+=1;
		error_focus = $('input#StartDate');
	}
	//else{
	//	StartDateWarningLayer.html('');
	//	StartDateWarningLayer.css('display','none');
	//}

	if(finished_date>today){
		FinishedDateWarningLayer.html(js_EndDateGreaterThanToday);
		FinishedDateWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#FinishedDate');
	}
	//else{
	//	FinishedDateWarningLayer.html('');
	//	FinishedDateWarningLayer.css('display','none');
	//}
	
	var ReadingHoursWarningLayer = $('div#ReadingHoursWarningDiv');
	if(!Check_Positive_Number(reading_hours)){
		ReadingHoursWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#ReadingHours');
	}else{
		ReadingHoursWarningLayer.css('display','none');
	}
	/*
	var ProgressWarningLayer = $('div#ProgressWarningDiv');
	if(!Check_Positive_Number(progress) || parseFloat(progress)>100){
		ProgressWarningLayer.css('display','');
		error_cnt+=1;
		if(error_focus!=null) error_focus = $('input#Progress');
	}else{
		ProgressWarningLayer.css('display','none');
	}
	*/
	if(error_cnt>0){
		if(error_focus!=null) error_focus.focus();
		return false;
	}
	
	var isnew = $('input#IsNew').val();
	if(issubmit==1){
		if(isnew==1)
			Add_Reading_Record(CallBack);
		else
			Update_Reading_Record(CallBack);
	}
	else
		return true;
}

function Add_Reading_Record(CallBack)
{
	Block_Thickbox();
	
	if($('.PopularAwardSchemeItem:checked').val()){
		var itemID = $('.PopularAwardSchemeItem:checked').val();
	}else{
		itemID = '';
	}
	
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			"Action":"AddReadingRecord",
			"BookID":$('input#BookID').val(),
			"StartDate":$('input#StartDate').val(),
			"FinishedDate":$('input#FinishedDate').val(),
			"ReadingHours":$('input#ReadingHours').val(),
			"Progress":$('input#Progress').val(),
			"AwardSchemeID":$('#AwardSchemeID').val(),
			"times":$('#times').val(),
			"PopularAwardSchemeItem": itemID
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			eval(CallBack);
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Update_Reading_Record(CallBack)
{
	Block_Thickbox();
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			"Action":"UpdateReadingRecord",
			"BookID":$('input#BookID').val(),
			"ReadingRecordID":$('input#ReadingRecordID').val(),
			"StartDate":$('input#StartDate').val(),
			"FinishedDate":$('input#FinishedDate').val(),
			"ReadingHours":$('input#ReadingHours').val(),
			"Progress":$('input#Progress').val(),
			"AwardSchemeID":$('#AwardSchemeID').val()
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			eval(CallBack);
			Cache_Reading_Record_Info[$('input#ReadingRecordID').val()] = null;
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Delete_Reading_Record(ReadingRecordID,CallBack)
{
	if(confirm(js_ConfirmDeleteReadingRecord)){
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
			{
				"Action":"DeleteReadingRecord",
				"ReadingRecordID":ReadingRecordID 
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				if(ReturnData.substring(0,1)==1)
					eval(CallBack);
				Scroll_To_Top();
			}
		);
	}
}

function Check_Positive_Number(n)
{
	var value = parseFloat(n);
	if (isNaN(value) || value <= 0)
		return false;
	else
		return true;
}

// Book Report

function Get_Book_Report_Edit_Form(Type,IsNew,ReadingRecordID,BookReportID,AssignedReadingID, CallBack, CategoryID)
{
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			"Action":"GetBookReportEditForm",
			"Type":Type,
			"IsNew":IsNew,
			"BookReportID":BookReportID,
			"ReadingRecordID":ReadingRecordID,
			"AssignedReadingID":AssignedReadingID,
			"CallBack":CallBack,
			"CategoryID":  CategoryID
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
			if(Type==2)
				js_Check_Word_Count()
		});
}

function Submit_Book_Report(CallBack)
{
	var UploadFileName = $.trim($('input#BookReportFile').val());
	if(UploadFileName==''){
		$('div#FileUploadWarningDiv').html('Please upload file.');
		$('div#FileUploadWarningDiv').css('display','');
		return false;
	}
	
	Block_Thickbox();
	var Obj = document.getElementById("BookReportEditForm");
	Obj.target = 'FileUploadIframe'; 
	Obj.encoding = "multipart/form-data";
	Obj.method = 'post'; 
	Obj.action = js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php?Action=UploadBookReportFile&CallBack="+CallBack;
	Obj.submit();
}

function Submit_Report_Post_Process(action,ReturnMsg,CallBack)
{
	Get_Return_Message(ReturnMsg);
	eval(CallBack);
	UnBlock_Thickbox();
	window.top.tb_remove();
}

function Delete_Book_Report_File(BookReportID,CallBack)
{
	if(confirm(js_ConfirmDeleteBookReport)){
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
			{
				"Action":"DeleteBookReportFile",
				"BookReportID":BookReportID 
			},
			function(ReturnData){

				Get_Return_Message(ReturnData);
				if(ReturnData.substring(0,1)=='1')
					eval(CallBack);
					
				Scroll_To_Top();
			}
		);
	}
}

function js_View_Mark_Report(ReadingRecordID,RecommendOnly)
{
	newWindow(js_PATH_WRT_ROOT+'home/eLearning/reading_garden/book_report_marking.php?RecommendOnly='+RecommendOnly+'&ReadingRecordID='+ReadingRecordID,31)
}

// Answer sheet
function Submit_Answer_Sheet(CallBack)
{
	var AnswerSheetID = $("#AnswerSheetID").val() || '';
	var StudentSelectedAnswerSheetID = $("#StudentSelectedAnswerSheetID").val() || '';
	if($.trim(StudentSelectedAnswerSheetID)!='')
	{
		if(StudentSelectedAnswerSheetID != AnswerSheetID)
		{
			if(!confirm(js_ConfirmYouCanOnlyAnswerOneAnswerSheet))
				return false;
		}
	}

	Block_Thickbox();
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			"Action":"SubmitBookReportAnswerSheet",
			"ReadingRecordID":$('input#ReadingRecordID').val(),
			"BookReportID":$('input#BookReportID').val(),
			"AssignedReadingID":$('input#AssignedReadingID').val(),
			"aStr":encodeURIComponent($('input#aStr').val()),
			"StudentSelectedAnswerSheetID": AnswerSheetID 
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message(js_SubmitAnswerSheetSuccess);
			else
				Get_Return_Message(js_SubmitAnswerSheetFail);
			eval(CallBack);
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

function Delete_Answer_Sheet(BookReportID,CallBack)
{
	if(confirm(js_ConfirmDeleteAnswerSheet)){
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
			{
				"Action":"DeleteBookReportAnswerSheet",
				"BookReportID":BookReportID 
			},
			function(ReturnData){
				if(ReturnData.substring(0,1)=='1'){
					eval(CallBack);
				}
				Get_Return_Message(ReturnData);
				
				Scroll_To_Top();
			}
		);
	}
}

function js_Refresh_Answer_Sheet()
{
	var AnswerSheetID = $("#AnswerSheetID").val()|| 0;
	var StudentSelectedAnswerSheetID = $("#StudentSelectedAnswerSheetID").val() || '';
	
	if($.trim(StudentSelectedAnswerSheetID)!='')
	{
		if(StudentSelectedAnswerSheetID != AnswerSheetID)
			$("input#aStr").val("")
		else
			$("input#aStr").val($("input#OldAnswer").val());
	}
	
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
			{
				"Action":"GetAnswerSheet",
				"AnswerSheetID":  AnswerSheetID
			},
			function(ReturnData) {
				$("input#qStr").val(ReturnData);

				var s = $("input#qStr").val();
					s = s.replace(/"/g, "&quot;");
					$("input#qStr").val(s);
					var form_templates = new Array(); 
					sheet.qString = s;
					s = $("input#aStr").val();
					s = s.replace(/"/g, "&quot;");
					$("input#aStr").val(s);
					sheet.aString = s;

					sheet.mode=1; // 0:edit 1:fill in application
					sheet.answer=sheet.sheetArr();
					
				$("div#AnswerSheetEditDiv").html(editPanel());
				$("div#blockInput").html(sheet.writeSheet());
				
				
			}
		);
	if(AnswerSheetID)
	{
		$("input#submit_btn").show();
	}
	else
	{
		$("input#submit_btn").hide();
	}
	
}

// Reading Status Filter
function js_Check_Reading_Status_Filter(CallBack)
{
	if($('.ReadingStatus:checked').length==0)
	{ 
		alert(jsWarnSelectReadingStatus)
		return false;
	}
	
	eval(CallBack);
	MM_showHideLayers('status_option','','hide')
	
}

function js_Init_Preload_Image()
{
	var BookCoverLayer = '<div class="bookcover" style="display:none; position:absolute; padding:5px; background-color:#fff; border:1px solid #888;"></div>';
	$(document.body).append(BookCoverLayer);
	$(".preload_image").mouseenter(function(evt){
		if(ShowPhoto==1) return true;
		var img_path = '<img src="'+$(this).attr("path")+'" width=100 height=120>';
		$(".bookcover").html(img_path)
		$(".bookcover").css(
			{
				'left': evt.pageX+5,
				'top': evt.pageY+5
			}
		).show();
	}).mousemove(function(evt){
		if(ShowPhoto==1) return true;
		$(".bookcover").css(
			{
				'left': evt.pageX+5,
				'top': evt.pageY+5
			}
		);
	}).mouseleave(function(){
		if(ShowPhoto==1) return true;
		$(".bookcover").hide();
	});
}

var ShowPhoto = 0;
function Toggle_Cover()
{
	if(ShowPhoto==0)
	{
		$(".preload_image").each(function(){
			$(this).attr("src",$(this).attr("path")).attr("height",120).attr("width",100);
		});
		$("#DisplayCoverText").html(js_Hide_All_Cover)
		ShowPhoto=1
	}
	else
	{
		$(".preload_image").each(function(){
			$(this).attr("src",icon_book_path).attr("height",14).attr("width",14);
		});
		$("#DisplayCoverText").html(js_Display_All_Cover)
		ShowPhoto=0
	}
}

/*View Announcement*/
function js_View_Announcement(AnnouncementID)
{
	tb_show("","#TB_inline?height=420&width=600&inlineId=FakeLayer");
	Block_Thickbox()
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			Action:"GetAnnouncementLayer",
			AnnouncementID:AnnouncementID
		},
		function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			UnBlock_Thickbox();
		}
	);
}

function js_View_Award(AwardSchemeID)
{
	tb_show("","#TB_inline?height=420&width=800&inlineId=FakeLayer");
	Block_Thickbox()
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			Action:"GetAwardLayer",
			AwardSchemeID:AwardSchemeID
		},
		function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			UnBlock_Thickbox();
		}
	);
}


function js_View_Book_Detail(BookID)
{
	newWindow(js_PATH_WRT_ROOT+'home/eLearning/reading_garden/book_detail.php?BookID='+BookID,8);
}

// Online Writing
function js_Check_Word_Count()
{
	var text = $("#OnlineWriting").val();
	var UpperLimit = $("#WritingLimit").val();
	var LowerLimit = $("#LowerLimit").val();
	var count = js_Get_Word_Count(text);
	
	if( (UpperLimit > 0 && count > UpperLimit)  || (LowerLimit > 0 && count < LowerLimit) )
	{
		count = '<span style="color:#f00; font-weight:bold;">'+count+'</span>';
	}
	$("#WordCountSpan").html(count);
}

function js_Check_Online_Writing(CallBack)
{
	var text = $("#OnlineWriting").val();
	var UpperLimit = $("#WritingLimit").val();
	var LowerLimit = $("#LowerLimit").val();
	
	var count = js_Get_Word_Count(text);
	$(".WarnMsgDiv").hide();
	
	if(count==0)
	{
		$("#WarnEmptyOnlineWriting").show();
		return false;
	}
	else if( (UpperLimit > 0 && count > UpperLimit)  || (LowerLimit > 0 && count < LowerLimit) )
	{
		$("#WarnExceedWritingLimit").show();
		return false;
	}
	
	Block_Thickbox();
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			"Action":"SubmitOnlineWriting",
			"ReadingRecordID":$('input#ReadingRecordID').val(),
			"BookReportID":$('input#BookReportID').val(),
			"AssignedReadingID":$('input#AssignedReadingID').val(),
			"OnlineWriting":$('#OnlineWriting').val()
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message(js_SubmitOnlineWritingSuccess);
			else
				Get_Return_Message(js_SubmitOnlineWritingFail);

			eval(CallBack);
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
	
}

function Delete_Online_Writing(BookReportID,CallBack)
{
	if(confirm(js_ConfirmDeleteOnlineWriting)){
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
			{
				"Action":"DeleteOnlineWriting",
				"BookReportID":BookReportID 
			},
			function(ReturnData){
				if(ReturnData==1){
					Get_Return_Message(js_DeleteOnlineWritingSuccess);
					eval(CallBack);
				}else{
					Get_Return_Message(js_DeleteOnlineWritingFail);
				}
				Scroll_To_Top();
			}
		);
	}
}

function Get_Popular_Award_Scheme_Record_Add_Layer(CallBack, StudentID)
{
	var CallBack = CallBack||'';
	var StudentID = StudentID||'';
	

	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
		{
			"Action":"LoadPopularAwardScheme",
			"CallBack":CallBack, 
			"StudentID":StudentID
		},
		function(ReturnData){
			$('div#TB_ajaxContent').html(ReturnData);
			js_Reload_Popular_Award_Scheme_Add_Layer_Category();
		}
	);
}

function js_Reload_Popular_Award_Scheme_Add_Layer_Category(StudentSubmit)
{
	var StudentSubmit = StudentSubmit||'';

	if(!$("#PopularAwardSchemeAddLayerCategoryID").val())
		$('div#PopularAwardSchemeItemSelection').html("");
	else
	{
		Block_Element("PopularAwardSchemeItemSelection");
		$.post(
			js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_reload.php",
			{
				"Action":"LoadPopularAwardSchemeItemSelect",
				"CategoryID": $("#PopularAwardSchemeAddLayerCategoryID").val(),
				"StudentSubmit":StudentSubmit
			},
			function(ReturnData){
				$('div#PopularAwardSchemeItemSelection').html(ReturnData);
				UnBlock_Element("PopularAwardSchemeItemSelection");
			}
		);
	}
}

function js_Check_Submit_Popular_Award_Scheme_Add_Record(CallBack, StudentID)
{
	var StudentID = StudentID||"";

	$(".WarnMsg").hide();
	
	if($("input.PopularAwardSchemeItem:checked").length==0)
	{
		$("div#WarnEmptyPopularAwardSchemeItem").show();
		return false;
	}
	
	$.post(
		js_PATH_WRT_ROOT+"home/eLearning/reading_garden/ajax_update.php",
		{
			Action:"AddPopularAwardSchemeItemUpdate",
			ItemID: $("input.PopularAwardSchemeItem:checked").val(),
			Times: $("select#times").val(),
			StudentID: StudentID
		},
		function(ReturnData){
			Get_Return_Message(ReturnData);
			if(CallBack)
				eval(CallBack);
			js_Hide_ThickBox();
		}
		
	);
}
