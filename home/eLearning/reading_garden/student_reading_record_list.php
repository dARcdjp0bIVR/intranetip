<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ReadingGardenLib->Update_Portal_Class_Session();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Teacher_View_Student_Reading_Record_UI($ClassID,$StudentID);
 

?>
<script>
function js_Reload_Student_Reading_Record()
{
	Block_Element("ReadingRecordListDiv");
	var StudentID = $("#StudentID").val();
	var ClassID = $("#ClassID").val();
	
	if($("#AcademicYearID option:first").is(':selected')){
		var AcademicYearID = 'all';
	}
	else if($("#AcademicYearID").val()!== null){
		var AcademicYearID = $("#AcademicYearID").val();
	}
	
	$.post(
		"ajax_index_reload.php",
		{
			Action:"ReloadStudentReadingRecord",
			StudentID:StudentID,
			ClassID:ClassID,
			AcademicYearID:AcademicYearID
		},
		function(ReturnData){
			$("#ReadingRecordListDiv").html(ReturnData);
			UnBlock_Element("ReadingRecordListDiv");
		}
	)
}

$().ready(function(){
	js_Reload_Student_Reading_Record(); 
})

// Call from js_Save_Marking in home/eLearning/reading_garden/book_report_marking.php
function js_Mark_Report_CallBack()
{
	js_Reload_Student_Reading_Record(); 
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>