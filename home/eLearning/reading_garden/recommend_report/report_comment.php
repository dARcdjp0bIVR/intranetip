<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$arrCookies[] = array("ck_Book_Report_Comment_numPerPage", "numPerPage");

if($clearCoo==1) // use default ClassID instead of ClassID saved in cookie. 
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui("popup.html");
$linterface =$ReadingGardenUI;

$CurrentPageArr['eLearningReadingGarden'] = 1;

//$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();
$MODULE_OBJ["title"] = $Lang['ReadingGarden']['Comment'];
$MODULE_OBJ["logo"] = "";

$ReturnMessage = $ReadingGardenUI->Get_Return_Message($Msg);
$UpdateCommentSuccess = substr($ReturnMessage,0,1)==1?"true":"false";
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Book_Report_Comment_List_UI($BookReportID,$pageNo,$numPerPage);
?>

<script>
$().ready(function(){
	if(<?=$UpdateCommentSuccess?>) // refresh the comment count after update comment 
	{
		// function js_Reload_Like_Div located in home/eLearning/reading_garden/reading_scheme.js
		opener.js_Reload_Like_Div("<?=FUNCTION_NAME_BOOK_REPORT?>","<?=$BookReportID?>"); 
	}
})

function js_Delete_Report_Comment(BookReportCommentID, UserType, UserID)
{
	if(UserType==2) //isStudent
		var confirmMsg = "<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteStudentReportComment']?>";
	else
		var confirmMsg = "<?=$Lang['ReadingGarden']['ConfirmArr']['DeleteTeacherReportComment']?>";
		
	if(confirm(confirmMsg))	
	{
		document.CommentForm.action="update_comment.php?Action=delete&BookReportCommentID="+BookReportCommentID+"&RecordType="+UserType+"&ParUserID="+UserID;
		document.CommentForm.submit();
	}
	else
		return false;
}
</script>

<?
$ReadingGardenUI->LAYOUT_STOP();
intranet_closedb();
?>