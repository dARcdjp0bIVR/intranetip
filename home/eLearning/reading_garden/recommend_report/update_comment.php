<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

switch($Action)
{
	case "add":
		$Success = $ReadingGardenLib->Manage_Book_Report_Comment("SET",$BookReportID, $_SESSION['UserID'],stripslashes(trim($Comment)));
		
		if($Success)
			$msg = "AddSuccess";
		else
			$msg = "AddUnsuccess";
	break;
	
	case "delete":
		$Success = $ReadingGardenLib->Manage_Book_Report_Comment("DEL",'',$ParUserID,'',$BookReportCommentID,$RecordType );
		
		if($Success)
			$msg = "DeleteSuccess";
		else
			$msg = "DeleteUnsuccess";
	break;
}

header("location: ./report_comment.php?BookReportID=$BookReportID&Msg=$msg");
intranet_closedb();
?>