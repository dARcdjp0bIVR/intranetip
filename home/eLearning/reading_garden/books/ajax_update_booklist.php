<?php
// using 

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Import_Book")
{

	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Get the Temp Data To be Insert
	$sql = "Select * from TEMP_READING_SCHEME_BOOK_IMPORT where UserID = '".$_SESSION['UserID']."'  order by RowNumber";
	$TempBookArr = $ReadingGardenLib->returnArray($sql);
	$numOfBook = count($TempBookArr);
	
	$SuccessArr = array();
	for ($i=0; $i<$numOfBook; $i++)
	{
		$thisDataArr = array();
		$thisDataArr['CallNumber'] = $TempBookArr[$i]['CallNumber'];
		$thisDataArr['ISBN'] = $TempBookArr[$i]['ISBN'];
		$thisDataArr['BookName'] = $TempBookArr[$i]['BookName'];
		$thisDataArr['Author'] = $TempBookArr[$i]['Author'];
		$thisDataArr['Publisher'] = $TempBookArr[$i]['Publisher'];
		$thisDataArr['Description'] = $TempBookArr[$i]['Description'];
			
		# Convert to Language DB value
		$thisLanguageID ='';
		$thisLanguageName = $TempBookArr[$i]['Language'];
		if(STRTOUPPER($thisLanguageName)=='CHINESE'){
			$thisLanguageID = CATEGORY_LANGUAGE_CHINESE;
		}
		elseif(STRTOUPPER($thisLanguageName)=='ENGLISH'){
			$thisLanguageID = CATEGORY_LANGUAGE_ENGLISH;			
		}
		elseif(STRTOUPPER($thisLanguage)!='OTHERS'){
			$thisLanguageDBValue = CATEGORY_LANGUAGE_OTHERS;	
		}
			
		$thisDataArr['Language'] = $thisLanguageID;		
		$thisDataArr['CategoryID']  = $TempBookArr[$i]['CategoryID'];	
		$thisDataArr['AnswerSheet'] = $TempBookArr[$i]['DefaultAnswerSheetID'];
	
		# Convert Form to DB value
		$thisYearIDArr ='';
		$thisYearIDList ='';
		$thisYearIDList = $TempBookArr[$i]['YearIDList'];
		$thisYearIDArr = explode(',',$thisYearIDList);
		$thisDataArr['YearID'] = $thisYearIDArr;
		
		#### Insert into DB #####
		$SuccessArr['TempID'] = $ReadingGardenLib->Create_Book($thisDataArr);
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'window.parent.document.getElementById("BlockUISpan").innerHTML = "'.($i + 1).'";';
			$thisJSUpdate .= 'window.parent.document.getElementById("NumOfProcessedPageSpan").innerHTML = "'.($i + 1).'";';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	$ReadingGardenLib->Delete_Book_Import_Temp_Data();
}
echo $success;


intranet_closedb();
?>