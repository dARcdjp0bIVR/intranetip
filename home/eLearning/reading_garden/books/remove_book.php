<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$BookID = $_REQUEST['BookID'];
$Result = $ReadingGardenLib->Remove_Book($BookID);

if($Result)
	$Msg = $Lang['ReadingGarden']['ReturnMsg']['DeleteBookSuccess'];
else
	$Msg = $Lang['ReadingGarden']['ReturnMsg']['DeleteBookFail'];

intranet_closedb();
header("Location: index.php?Msg=".urlencode($Msg));
?>