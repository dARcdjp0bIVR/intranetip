<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	/*
	case "AddBook":
		$DataArr['CallNumber'] = trim(urldecode(stripslashes($_REQUEST['CallNumber'])));
		$DataArr['BookName'] = trim(urldecode(stripslashes($_REQUEST['BookName'])));
		$DataArr['Author'] = trim(urldecode(stripslashes($_REQUEST['Author'])));
		$DataArr['Publisher'] = trim(urldecode(stripslashes($_REQUEST['Publisher'])));
		$DataArr['Description'] = trim(urldecode(stripslashes($_REQUEST['Description'])));
		$DataArr['CategoryID'] = $_REQUEST['SelCategoryID'];
		$DataArr['Language'] = $_REQUEST['Language'];
		$DataArr['YearID'] = $_REQUEST['YearID'];
		$DataArr['AnswerSheet'] = $_REQUEST['AnswerSheet'];
		
		$BookID = $ReadingGardenLib->Create_Book($DataArr);
		if($BookID==false)
			echo "0";
		else
			echo $BookID;
	break;
	case "UpdateBook":
		$DataArr['BookID'] = $_REQUEST['BookID'];
		$DataArr['CallNumber'] = trim(urldecode(stripslashes($_REQUEST['CallNumber'])));
		$DataArr['BookName'] = trim(urldecode(stripslashes($_REQUEST['BookName'])));
		$DataArr['Author'] = trim(urldecode(stripslashes($_REQUEST['Author'])));
		$DataArr['Publisher'] = trim(urldecode(stripslashes($_REQUEST['Publisher'])));
		$DataArr['Description'] = trim(urldecode(stripslashes($_REQUEST['Description'])));
		$DataArr['CategoryID'] = $_REQUEST['SelCategoryID'];
		$DataArr['Language'] = $_REQUEST['Language'];
		$DataArr['YearID'] = $_REQUEST['YearID'];
		$DataArr['AnswerSheet'] = $_REQUEST['AnswerSheet'];
		
		$Result = $ReadingGardenLib->Update_Book($DataArr);
		if($Result)
			echo "1";
		else
			echo "0";
	break;
	*/
	case "AddBook":
		$DataArr['CallNumber'] = trim(stripslashes($_REQUEST['CallNumber']));
		$DataArr['ISBN'] = trim(stripslashes($_REQUEST['ISBN']));
		$DataArr['BookName'] = trim(stripslashes($_REQUEST['BookName']));
		$DataArr['Author'] = trim(stripslashes($_REQUEST['Author']));
		$DataArr['Publisher'] = trim(stripslashes($_REQUEST['Publisher']));
		$DataArr['Description'] = trim(stripslashes($_REQUEST['Description']));
		$DataArr['CategoryID'] = $_REQUEST['SelCategoryID'];
		$DataArr['Language'] = $_REQUEST['Language'];
		$DataArr['YearID'] = $_REQUEST['YearID'];
		$DataArr['AnswerSheet'] = $_REQUEST['AnswerSheet'];
		
		$IsCoverImageValid = $_REQUEST['IsCoverImageValid'];
		
		$BookID = $ReadingGardenLib->Create_Book($DataArr);
		if($BookID==false){
			echo '<script type="text/javascript">'."\n";
			echo 'window.parent.Post_Process("add",0);';
			echo '</script>'."\n";
		}else{
			echo '<script type="text/javascript">'."\n";
			if($BookID != "" && $CoverImage != "" && $IsCoverImageValid==1){
				$Result = $ReadingGardenLib->Update_Book_Cover_Image($BookID,$CoverImage,$CoverImage_name);
				if($Result)
					echo 'window.parent.Post_Process("add",1);';
				else
					echo 'window.parent.Post_Process("add",0);';
			}else{
				echo 'window.parent.Post_Process("add",1);';
			}
			echo '</script>'."\n";
		}
	break;
	case "UpdateBook":
		$DataArr['BookID'] = $_REQUEST['BookID'];
		$DataArr['CallNumber'] = trim(stripslashes($_REQUEST['CallNumber']));
		$DataArr['ISBN'] = trim(stripslashes($_REQUEST['ISBN']));
		$DataArr['BookName'] = trim(stripslashes($_REQUEST['BookName']));
		$DataArr['Author'] = trim(stripslashes($_REQUEST['Author']));
		$DataArr['Publisher'] = trim(stripslashes($_REQUEST['Publisher']));
		$DataArr['Description'] = trim(stripslashes($_REQUEST['Description']));
		$DataArr['CategoryID'] = $_REQUEST['SelCategoryID'];
		$DataArr['Language'] = $_REQUEST['Language'];
		$DataArr['YearID'] = $_REQUEST['YearID'];
		$DataArr['AnswerSheet'] = $_REQUEST['AnswerSheet'];
		$DataArr['eBookID'] = $_REQUEST['eBookID'];
		$DataArr['ByStudent'] = $_REQUEST['ByStudent'];
		
		$IsCoverImageValid = $_REQUEST['IsCoverImageValid'];
		
		$Result = $ReadingGardenLib->Update_Book($DataArr);
		if($Result){
			$BookID = $DataArr['BookID'];
			echo '<script type="text/javascript">'."\n";
			if($BookID != "" && $CoverImage != "" && $IsCoverImageValid==1){
				$Result = $ReadingGardenLib->Update_Book_Cover_Image($BookID,$CoverImage,$CoverImage_name);
				if($Result)
					echo 'window.parent.Post_Process("update",1);';
				else
					echo 'window.parent.Post_Process("update",0);';
			}else{
				echo 'window.parent.Post_Process("update",1);';
			}
			echo '</script>'."\n";
		}else{
			echo '<script type="text/javascript">'."\n";
			echo 'window.parent.Post_Process("update",0);';
			echo '</script>'."\n";
		}
	break;
	case "DeleteBook":
		$BookID = $_REQUEST['BookID'];
		if($BookID != ""){
			$Result = $ReadingGardenLib->Remove_Book($BookID);
			if($Result)
				echo "1";
			else
				echo "0";
		}
	break;
	case "UploadImage":
		$BookID = $_GET['BookID'];
		if($BookID != "" && $CoverImage != ""){
			$Result = $ReadingGardenLib->Update_Book_Cover_Image($BookID,$CoverImage,$CoverImage_name);
			if($Result)
				echo "1";
			else
				echo "0";
		}
	break;
	case "DeleteImage":
		$BookID = $_REQUEST['BookID'];
		if($BookID != ""){
			$Result = $ReadingGardenLib->Delete_Book_Cover_Image($BookID);
			if($Result)
				echo "1";
			else
				echo "0";
		}
	break;
	case "CheckCoverImage":
		if(isset($CoverImage) && $CoverImage != "")
		{
			$is_valid_image = exif_imagetype($CoverImage);
			
			echo '<script type="text/javascript">'."\n";
			
			if ($is_valid_image)
			{
				echo 'window.parent.document.getElementById("IsCoverImageValid").value = 1;'."\n";
				//echo 'window.parent.document.getElementById("FileUploadWarningDiv").innerHTML = "";'."\n";
				echo 'window.parent.document.getElementById("FileUploadWarningDiv").style.display = "none";'."\n";
			}
			else
			{
				echo 'window.parent.document.getElementById("IsCoverImageValid").value = 0;'."\n";
				//echo 'window.parent.document.getElementById("FileUploadWarningDiv").innerHTML = "'.$Lang['ReadingGarden']['WarningMsg']['InvalidCoverImage'].'";'."\n";
				echo 'window.parent.document.getElementById("FileUploadWarningDiv").style.display = "block";'."\n";
			}
			echo 'window.parent.document.getElementById("submit_btn").disabled = false;'."\n";
			echo '</script>'."\n";
		}
	break;
	case "RemoveBookAnswerSheet":
		$BookID = $_REQUEST['BookID'];
		if($BookID>0){
			$Result = $ReadingGardenLib->Set_Book_Default_Answersheet($BookID,"");
			if($Result)
				echo "1";
			else
				echo "0";
		}
	break;
	case "ImportELIBBook":
		$BookID = $_REQUEST['BookID'];// eLib Book IDs
		if(sizeof($BookID)>0){
			include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
			$libYear = new Year();
			$FormArr = $libYear->Get_All_Year_List();
			$YearIDArr = Get_Array_By_Key($FormArr, "YearID");
			
			$BookID = array_values($BookID);
			$ELIBBooks = $ReadingGardenLib->Get_ELIB_Book_List($BookID);
			$CategoryList = $ReadingGardenLib->Get_Category_List();
			$CategoryAssoc = array();
			for($i=0;$i<sizeof($CategoryList);$i++){
				$category_name = $CategoryList[$i]['CategoryName'];
				$category_id = $CategoryList[$i]['CategoryID'];
				$ThisLanguage = $CategoryList[$i]['Language'];
				if ($ThisLanguage!="" && $category_name!="")
				{
					$CategoryAssoc[$category_name][$ThisLanguage] = $category_id;
				}
			}
			$Result=array();
			for($i=0;$i<sizeof($ELIBBooks);$i++){
				$CategoryInfo = array();
				if(strtolower($ELIBBooks[$i]['Language'])=='chi')
					$language = CATEGORY_LANGUAGE_CHINESE;
				else if(strtolower($ELIBBooks[$i]['Language'])=='eng')
					$language = CATEGORY_LANGUAGE_ENGLISH;
				else
					$language = (str_lang($ELIBBooks[$i]['Title'])=="ENG") ? CATEGORY_LANGUAGE_ENGLISH : CATEGORY_LANGUAGE_CHINESE;

				$BookCoverPath = $intranet_root."/file/elibrary/content/".$ELIBBooks[$i]['ELIBBookID']."/image/cover.jpg";
				if(!file_exists($BookCoverPath)) $BookCoverPath = "";
				$CategoryID = $CategoryAssoc[$ELIBBooks[$i]['Category']][$language];
				if ($CategoryID=="")
				{
					$CategoryID = $CategoryAssoc[$ELIBBooks[$i]['SubCategory']][$language];
				}
				if ($CategoryID=="")
				{
					# insert new category
					$CategoryNameUsed = trim($ELIBBooks[$i]['Category']);
					if ($CategoryNameUsed == "")
					{
						$CategoryNameUsed =  trim($ELIBBooks[$i]['SubCategory']);
					}
					
					if ($CategoryNameUsed!="")
					{
						// Settings_Insert_Category()
						$CatTmp = explode("- ", $CategoryNameUsed);
						if (sizeof($CatTmp)>1)
						{
							$CategoryNameUsed = trim($CatTmp[1]);
							$CategoryCodeUsed = trim($CatTmp[0]);
						} else
						{
							$CategoryCodeUsed = $CategoryNameUsed;
						}
						
						if ($CategoryCodeUsed = "")
						{
							$CategoryCodeUsed = $CategoryNameUsed;
						}
						
						
						$CategoryID = $CategoryAssoc[$CategoryNameUsed][$language];
						
						if ($CategoryID=="")
						{
							$CategoryInfo['CategoryName'] = addslashes($CategoryNameUsed);
							$CategoryInfo['CategoryCode'] = addslashes($CategoryCodeUsed);
							$CategoryInfo['Language'] = $language;
							$CategoryID = $ReadingGardenLib->Settings_Insert_Category($CategoryInfo);
							if ($CategoryID<0)
							{
								$CategoryID = "";
							} else
							{
								# add to current list
								$CategoryAssoc[$CategoryNameUsed][$language] = $CategoryID;
							}
						}
					}
					
				}
				$ParArr = array(
							'CallNumber'=>$ELIBBooks[$i]['CallNumber'],
							'ISBN'=>$ELIBBooks[$i]['ISBN'],
							'BookName'=>$ELIBBooks[$i]['Title'],
							'Author'=>$ELIBBooks[$i]['Author'],
							'Publisher'=>$ELIBBooks[$i]['Publisher'],
							'Description'=>$ELIBBooks[$i]['Preface'],
							'eBookID'=>$ELIBBooks[$i]['ELIBBookID'],
							'Language'=>$language,
							'CategoryID'=>$CategoryID,
							'YearID'=>$YearIDArr
								);
				$Result['ImportBook'.$ELIBBooks[$i]['ELIBBookID']] = $ReadingGardenLib->Create_Book($ParArr);
				$insert_bookid = $Result['ImportBook'.$ELIBBooks[$i]['ELIBBookID']];
				if($insert_bookid!=false && $BookCoverPath!=""){
					$Result['CopyBookCover'.$ELIBBooks[$i]['ELIBBookID']] = $ReadingGardenLib->Update_Book_Cover_Image($insert_bookid,$BookCoverPath,$BookCoverPath);
				}
			}
			if(!in_array(false,$Result))
				echo "1";
			else
				echo "0";
		}
	break;
}

intranet_closedb();
?>