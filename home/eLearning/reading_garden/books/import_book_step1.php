<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$YearObj = new form_class_manage();

# Page Title
$CurrentPage = "Settings_BookSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;
$TAGS_OBJ[] = array($Lang['SysMgr']['ReadingGarden']['Button']['ImportBook']);

# Return Message
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();
$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['ReturnMsg']));
$linterface->LAYOUT_START($ReturnMessage);

# Navaigtion
$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['BookSettings']['MenuTitle'], "javascript:js_Cancel();");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['ReadingGarden']['Button']['ImportBook'], "");

# CSV sample
$csvFile = "import_book_sample.csv";
$SampleCSVFile = "<a class='tablelink' href='". GET_CSV($csvFile) ."'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

# Buttons
$ContinueBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button", "js_Continue();");
$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();");

# Column Property Display
$ColumnTitleArr = $Lang['ReadingGarden']['Import_Book_Column'];
$ColumnPropertyArr = array(1,3,3,3,3,1,3,3,3,3);
$RemarksArr = array();
$RemarksArr[5] =  '<a id="remarkBtn_language" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'language\');">['.$Lang['General']['Remark'].']</a>';
$RemarksArr[6] =  '<a id="remarkBtn_category" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'category\');">['.$Lang['General']['Remark'].']</a>';
$RemarksArr[8] =  '<a id="remarkBtn_form" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'form\');">['.$Lang['General']['Remark'].']</a>';
$RemarksArr[9] =  '<a id="remarkBtn_answerSheet" class="tablelink" href="javascript:void(0);" onclick="showRemarkLayer(\'answerSheet\');">['.$Lang['General']['Remark'].']</a>';

$ColumnDisplay = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
		
# Get Exisiting Categories	
$allCategoryListArr = $ReadingGardenLib->Get_Category_List();	
$numOfAllCategoryListArr = count($allCategoryListArr);
for($i=0; $i<$numOfAllCategoryListArr; $i++){
	$langauge = $allCategoryListArr[$i]['Language'];
	$allCategoryListArrDisplay[$langauge][] = $allCategoryListArr[$i]['CategoryCode'] . '-' . $allCategoryListArr[$i]['CategoryName'];	
}

	
# Get Exisiting Form
$yearFormArr = $YearObj-> Get_Form_List('',1,'','','','');
$numOfForm = count($yearFormArr);

# Get Exisiting Answer Sheet
$AnswerSheetDisplayArr = $ReadingGardenLib->Get_Answer_Sheet_Template('','','');
$numOfAnswerSheet = count($AnswerSheetDisplayArr);

$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post" action="import_book_step2.php" enctype="multipart/form-data">'."\n";
	$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
	$x .= '<br />'."\n";
	$x .= $linterface->GET_IMPORT_STEPS($CurrStep=1);
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<table class="form_table_v30">'."\n";
				
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['General']['SourceFile'].' <span class="tabletextremark">'.$Lang['General']['CSVFileFormat'].'</span></td>'."\n";
							$x .= '<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'.$Lang['General']['CSVSample'].'</td>'."\n";
							$x .= '<td>'.$SampleCSVFile.'</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="field_title">'."\n";
								$x .= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']."\n";
							$x .= '</td>'."\n";
							$x .= '<td>'."\n";
								$x .= $ColumnDisplay."\n";
								$x .= "<br /><br />\n";
								$x .= '<span class="tabletextremark">('.$Lang['SysMgr']['ReadingGarden']['ImportBook']['CategoryRemarks'].')</span>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td class="tabletextremark" colspan="2">'."\n";
								$x .= $linterface->MandatoryField();
								$x .= $linterface->ReferenceField();
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	
	# Buttons
	$x .= '<div class="edit_bottom_v30">'."\n";
		$x .= $ContinueBtn;
		$x .= "&nbsp;";
		$x .= $CancelBtn;
	$x .= '</div>'."\n";
	

	# Language Remarks Layer
	$thisRemarksType = 'language';
	$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:200px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
						
									$x .= '<th>'.$ColumnTitleArr[5].'</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							$x .= '<tbody>'."\n";
																								
							$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][1].'</td>'."\n";
							$x .= '</tr>'."\n";
							
							$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][2].'</td>'."\n";
							$x .= '</tr>'."\n";
							
							$x .= '<tr>'."\n";
							$x .= '<td>'.$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][3].'</td>'."\n";
							$x .= '</tr>'."\n";
							
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	
	
	# Category Remarks Layer	
	$thisRemarksType = 'category';
	$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:400px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
							
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";													
									$x .= '<th>'.$ColumnTitleArr[5].'</th>'."\n";
									$x .= '<th>'.$Lang['General']['Code'] .'</th>'."\n";		
									$x .= '<th>'.$ColumnTitleArr[6].'</th>'."\n";														
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							$x .= '<tbody>'."\n";
							
								$x .= '<tr>';
								$x .= '<td>---</td>'."\n";		
									
								$x .= '<td>Uncategorized</td>'."\n";		
														
								$x .= '<td>'.$Lang['ReadingGarden']['BookCategorySettings']['Uncategorized'] .'</td>'."\n";		
																
								$x .= '</tr>';
																			
							foreach ($allCategoryListArrDisplay as $languageDisplay=>$categoryDisplay){					
								if($languageDisplay == CATEGORY_LANGUAGE_CHINESE){
									$thisLanguageDisplayItem = $Lang['ReadingGarden']['BookCategorySettings']['LangArr'][1];
								
								}elseif($languageDisplay == CATEGORY_LANGUAGE_ENGLISH){
									$thisLanguageDisplayItem = $Lang['ReadingGarden']['BookCategorySettings']['LangArr'][2];								
								}
							    elseif($languageDisplay == CATEGORY_LANGUAGE_OTHERS){
									$thisLanguageDisplayItem = $Lang['ReadingGarden']['BookCategorySettings']['LangArr'][3];
								}
								
							
																						
								$x .= '<tr>';
								
								$x .= '<td>'.$thisLanguageDisplayItem.'</td>'."\n";		
								//$x .= '<td rowspan="'.$numOfcategoryDisplay.'">'.$languageDisplayItem.'</td>'."\n";	
								$x .= '<td>';															
								foreach ($categoryDisplay as $categoryDisplayCodeWithItem){								
									$categoryDisplayCodeWithItem = explode('-', $categoryDisplayCodeWithItem);
									$thisCategoryDisplayCode = $categoryDisplayCodeWithItem[0];																																	
									$x .= $thisCategoryDisplayCode . "<br />";																																
								}								
								$x.='</td>'."\n";	
									
								$x .= '<td>';																	
								foreach ($categoryDisplay as $categoryDisplayCodeWithItem){
									$categoryDisplayCodeWithItem = explode('-', $categoryDisplayCodeWithItem);
									$thisCategoryDisplayItem = $categoryDisplayCodeWithItem[1];																
									$x .= $thisCategoryDisplayItem . "<br />";																	
								}	
								$x .= '</td>';																		
								$x .= '</tr>';
								
							}
																										
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
				
	# Form Remarks Layer	
	$thisRemarksType = 'form';
	$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:200px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th>'.$ColumnTitleArr[8].'</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							$x .= '<tbody>'."\n";
							
							for ($i=0; $i<$numOfForm; $i++){	
								$thisFormDisplay = $yearFormArr[$i]['YearName'];																	
								$x .= '<tr>'."\n";								
									$x .= '<td>'.$thisFormDisplay.'</td>'."\n";
								$x .= '</tr>'."\n";
							}
							
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	
			
	# Answer Sheet Remarks Layer
	$thisRemarksType = 'answerSheet';
	$x .= '<div id="remarkDiv_'.$thisRemarksType.'" class="selectbox_layer" style="width:200px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="common_table_list_v30 view_table_list_v30" width="80%">'."\r\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
						
									$x .= '<th>'.$ColumnTitleArr[9].'</th>'."\n";
								$x .= '</tr>'."\n";
							$x .= '</thead>'."\n";
							$x .= '<tbody>'."\n";
							
							
							for ($i=0; $i<$numOfAnswerSheet; $i++){	
								$thisAnswerSheetDisplay = intranet_htmlspecialchars($AnswerSheetDisplayArr[$i]['TemplateName']);																	
								$x .= '<tr>'."\n";								
									$x .= '<td>'.$thisAnswerSheetDisplay.'</td>'."\n";
								$x .= '</tr>'."\n";
							}																
							
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	
$x .= '</form>'."\n";
$x .= '<br />'."\n";

echo $x;

?>

<script language="javascript">

function js_Continue() {
	if (Trim(document.getElementById('csvfile').value) == "") {
		 alert('<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>');
		 document.getElementById('csvfile').focus();
		 return false;
	}
	
	var jsObjForm = document.getElementById('form1');
	jsObjForm.action = "import_book_step2.php";
	jsObjForm.submit();
}

function js_Go_Back() {
	window.location = 'index.php';
}


function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function showRemarkLayer(remarkType) {
	var remarkBtnId = 'remarkBtn_' + remarkType;
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	var leftAdjustment = $('#' + remarkBtnId).width();
	var topAdjustment = 0;
	
	changeLayerPosition(remarkBtnId, remarkDivId, leftAdjustment, topAdjustment);
	hideAllRemarkLayer();
	MM_showHideLayers(remarkDivId, '', 'show');
}

function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}

function js_Cancel()
{
	window.location = 'index.php';
}
</script>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>