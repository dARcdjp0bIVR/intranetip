<?
// Modifying by: 

############# Change Log [Start] ################
#
#
############# Change Log [End] ################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$linterface = new interface_html();

### Get the Temp Data To be Insert
$sql = "Select * from TEMP_READING_SCHEME_BOOK_IMPORT where UserID = '".$_SESSION['UserID']."'  order by RowNumber";
$TempBookArr = $ReadingGardenLib->returnArray($sql);
$numOfBook = count($TempBookArr);


# step information
### Title / Menu
$CurrentPage = "Settings_BookSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['SysMgr']['ReadingGarden']['Button']['ImportBook']);
$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['ReturnMsg']));
$linterface->LAYOUT_START($ReturnMessage);

# navaigtion
$PAGE_NAVIGATION[] = array($Lang['ReadingGarden']['BookSettings']['MenuTitle'], "javascript:js_Cancel();");
$PAGE_NAVIGATION[] = array($Lang['SysMgr']['ReadingGarden']['Button']['ImportBook'], "");


### iFrame for real-time update count
$thisSrc = "ajax_update_booklist.php?Action=Import_Book";
$ImportBookIFrame = '<iframe id="ImportBookIFrame" name="ImportBookIFrame" src="'.$thisSrc.'" style="display:none;"></iframe>'."\n";
					
$ProcessingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfBook, $Lang['SysMgr']['Timetable']['RecordsProcessed']);

?>

<script language="javascript">

$(document).ready(function () {
	//js_Import_Lesson();
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Import_Lesson()
{
	Block_Document('<?=$ProcessingMsg?>');
	$.post(
		"ajax_update_booklist.php", 
		{ 
			Action: "Import_Book",
		},
		function(ReturnData)
		{
			//UnBlock_Document();
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Cancel()
{
	window.location = 'index.php';
}
	
</script>

<br />
<form id="form1" name="form1" method="post">
	<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br />
	<?= $linterface->GET_IMPORT_STEPS($CurrStep=3) ?>
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center"><?=$x?></td>
		</tr>
				
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				
					<tr>
						<td class='tabletext' align='center'>
							<span id="NumOfProcessedPageSpan"><?=$numOfBook?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
						</td>
					</tr>
			
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					
					<tr>
						<td align="center">
							<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "window.location='index.php'"); ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<?=$ImportBookIFrame?>
</form>

<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>