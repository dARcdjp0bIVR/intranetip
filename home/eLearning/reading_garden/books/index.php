<?php
// editing by 
/*************************************************
 * Date: 2012-09-27 (Rita)
 * 		 Comment call number JS checking 
 * 
 * 
 *************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

if(is_array($_REQUEST['BookSource']))
	$BookSource = implode(",",(array)$_REQUEST['BookSource']);

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_book_list_page_size", "numPerPage");
$arrCookies[] = array("ck_book_list_page_number", "pageNo");
$arrCookies[] = array("ck_book_list_page_order", "order");
$arrCookies[] = array("ck_book_list_page_field", "field");	
$arrCookies[] = array("ck_book_list_page_keyword", "Keyword");
$arrCookies[] = array("ck_book_list_page_BookSource", "BookSource");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

if(!empty($BookSource))
	$BookSourceArr = explode(",",$BookSource);

//header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
//header("Cache-Control: no-store, no-cache, must-revalidate");
//header("Cache-Control: post-check=0, pre-check=0", false);
//header("Pragma: no-cache");

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPage = "Settings_BookSettings";
$CurrentPageArr['eLearningReadingGarden'] = 1;

$TAGS_OBJ[] = array($Lang['ReadingGarden']['BookSettings']['AllBooks'], "", 1);
$TAGS_OBJ[] = array($Lang['ReadingGarden']['RecommendBookSettings']['MenuTitle'], $PATH_WRT_ROOT."home/eLearning/reading_garden/settings/recommend_book_list/recommend_book_list.php?clearCoo=1", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR();

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$linterface->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();

echo $ReadingGardenUI->Get_Book_Index($field,$order,$pageNo,$numPerPage,'',$CategoryID, $BookSourceArr);

?>
<script>
/*
function Get_Multiple_Selection_Values(name)
{
	var opts = $('select[name='+name+'] option:selected');
	var returnArray = new Array();
	opts.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}
*/
function Get_Checkbox_Values(name)
{
	var cbs = $('input[name='+name+']:checked');
	var returnArray = new Array();
	cbs.each(function(index){
		returnArray[index]=$(this).val();
	}
	);
	return returnArray;
}

function Get_Book_Table()
{
	var formData = $('#form1').serialize();
	var curtime = new Date().getTime();
	formData += "&Action=GetBookTable&curtime=" + curtime;
	
	Block_Element("DivBookTable");
	$.ajax({  
		type: "POST",  
		url: "ajax_load.php?curtime="+curtime,
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("div#DivBookTable").html(ReturnData);
			initThickBox();
			UnBlock_Element("DivBookTable");
			Scroll_To_Top();
			js_Init_Preload_Image();
		} 
	});
} 

function Get_ELIB_Book_Layer()
{
	$.post(
		"ajax_load.php",
		{
			"Action":"GetELIBBookLayer"
		},
		function(ReturnData) {
			if (ReturnData == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(ReturnData);
		}
	);
}

function Import_ELIB_Book()
{
	Block_Thickbox();
	$.post(
		"ajax_update.php",
		{
			"Action":"ImportELIBBook",
			"BookID[]":Get_Checkbox_Values('BookID[]') 
		},
		function(ReturnData){
			if(ReturnData=="1")
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['BookImportFromeLibrarySuccess']?>');
			else
				Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['BookImportFromeLibraryFail']?>');
			Get_Book_Table();
			window.top.tb_remove();
			UnBlock_Thickbox();
		}
	);
}

var gAddMore = 0;
function Get_Book_Edit_Form(is_new, book_id)
{
	if(!book_id){
		book_id = $('input[name="BookID\\[\\]"]:checked').val();
	}
	tb_show("<?=$Lang['Btn']['Edit']?>","#TB_inline?height=600&width=750&inlineId=FakeLayer");
	
	$.post(
		"ajax_load.php",
		{
			"Action":"GetBookEditForm",
			"IsNew":is_new,
			"BookID":book_id 
		},
		function(ReturnData) {
			if (ReturnData == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(ReturnData);
		});
}

function Check_Book(is_submit, add_more)
{
	gAddMore = add_more;
	var is_data_valid = true;
//	var CallNumber = Trim($('input#CallNumber').val());
//	if(CallNumber == ''){
//		$('input#CallNumber').focus();
//		$('div#CallNumberWarningDiv').css('display','block');
//		is_data_valid = false;
//	}else
//		$('div#CallNumberWarningDiv').css('display','none');
		
	var BookName = Trim($('input#BookName').val());
	if(BookName == ''){
		if(is_data_valid!=false) $('input#BookName').focus();
		$('div#BookNameWarningDiv').css('display','block');
		is_data_valid = false;
	}else
		$('div#BookNameWarningDiv').css('display','none');
	
	var IsCoverImageValid = $('input#IsCoverImageValid').val();
	var CoverImage = Trim($('input#CoverImage').val());
	if(CoverImage != '' && IsCoverImageValid!=1){
		$('div#FileUploadWarningDiv').css('display','block');
		is_data_valid = false;
	}else
		$('div#FileUploadWarningDiv').css('display','none');
	
	if(is_data_valid==false) return false;
	
	if(is_submit==1){
		var is_new = $('input#IsNew').val();
		if(is_new==1){
			Add_Book();
		}else{
			Update_Book();
		}
	}
}
/*
function Add_Book()
{
	Block_Thickbox();
	$.post(
		"ajax_update.php",
		{
			"Action":"AddBook",
			"CallNumber":encodeURIComponent($('input#CallNumber').val()),
			"BookName":encodeURIComponent($('input#BookName').val()),
			"Author":encodeURIComponent($('input#Author').val()),
			"Publisher":encodeURIComponent($('input#Publisher').val()),
			"Description":encodeURIComponent($('#Description').val()),
			"SelCategoryID":$('#SelCategoryID').val(),
			"Language":$('#Language').val(),
			"YearID[]":Get_Multiple_Selection_Values('YearID[]'),
			"AnswerSheet":$('#AnswerSheet').val()  
		},
		function(ReturnData){
			if (ReturnData == "die") 
				window.top.location = '/';
			else{
				var ReturnMsg = '';
				if(ReturnData != "0"){
					var IsCoverImageValid = $('input#IsCoverImageValid').val();
					var CoverImage = Trim($('input#CoverImage').val());
					//console.log('cover: '+CoverImage+"\n");
					//console.log('is valid :' + IsCoverImageValid+"\n");
					if(CoverImage != '' && IsCoverImageValid==1)
					{
						var BookID = ReturnData;
						var Obj = document.getElementById("BookEditForm");
						Obj.target = 'FileUploadIframe'; 
						Obj.encoding = "multipart/form-data";
						Obj.method = 'post'; 
						Obj.action = "ajax_update.php?Action=UploadImage&BookID=" + BookID;
						Obj.submit();
						
						Restore_Form(Obj);
					}
					ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['AddBookSuccess']?>';
				}else{
					ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['AddBookFail']?>';
				}
				Get_Return_Message(ReturnMsg);
				Get_Book_Table();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		}
	);
}

function Update_Book()
{
	Block_Thickbox();
	$.post(
		"ajax_update.php",
		{
			"Action":"UpdateBook",
			"BookID":$('input#BookID').val(),
			"CallNumber":encodeURIComponent($('input#CallNumber').val()),
			"BookName":encodeURIComponent($('input#BookName').val()),
			"Author":encodeURIComponent($('input#Author').val()),
			"Publisher":encodeURIComponent($('input#Publisher').val()),
			"Description":encodeURIComponent($('#Description').val()),
			"SelCategoryID":$('#SelCategoryID').val(),
			"Language":$('#Language').val(),
			"YearID[]":Get_Multiple_Selection_Values('YearID[]'),
			"AnswerSheet":$('#AnswerSheet').val() ,
			"eBookID":$('#eBookID').val() ,
			"ByStudent":$('#ByStudent').val() 
		},
		function(ReturnData){
			if (ReturnData == "die") 
				window.top.location = '/';
			else{
				var ReturnMsg = '';
				if(ReturnData == "1"){
					var IsCoverImageValid = $('input#IsCoverImageValid').val();
					var CoverImage = Trim($('input#CoverImage').val());
					if(CoverImage != '' && IsCoverImageValid==1)
					{
						var BookID = $('input#BookID').val();
						var Obj = document.getElementById("BookEditForm");
						Obj.target = 'FileUploadIframe'; 
						Obj.encoding = "multipart/form-data";
						Obj.method = 'post'; 
						Obj.action = "ajax_update.php?Action=UploadImage&BookID=" + BookID;
						Obj.submit();
						
						Restore_Form(Obj);
					}
					
					ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateBookSuccess']?>';
				}else{
					ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateBookFail']?>';
				}
				Get_Return_Message(ReturnMsg);
				Get_Book_Table();
				window.top.tb_remove();
				UnBlock_Thickbox();
			}
		}
	);
}
*/
function Add_Book()
{
	Block_Thickbox();
	var Obj = document.getElementById("BookEditForm");
	Obj.target = 'FileUploadIframe'; 
	Obj.encoding = "multipart/form-data";
	Obj.method = 'post'; 
	Obj.action = "ajax_update.php?Action=AddBook";
	Obj.submit();
	
	Restore_Form(Obj);
}

function Update_Book()
{
//	Block_Thickbox();
	var Obj = document.getElementById("BookEditForm");
	Obj.target = 'FileUploadIframe'; 
	Obj.encoding = "multipart/form-data";
	Obj.method = 'post'; 
	Obj.action = "ajax_update.php?Action=UpdateBook";
	Obj.submit();
	
	Restore_Form(Obj);
}

function Post_Process(action,type)
{
	var ReturnMsg = '';
	if(action=='add'){
		if(type==1)
			ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['AddBookSuccess']?>';
		else
			ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['AddBookFail']?>';
	}else if(action=="update"){
		if(type==1)
			ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateBookSuccess']?>';
		else
			ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['UpdateBookFail']?>';
	}
	if(ReturnMsg != '') Get_Return_Message(ReturnMsg);
	if(gAddMore==1){
		Get_Book_Edit_Form(1, '');
		UnBlock_Thickbox();
	}else{
		Get_Book_Table();
		UnBlock_Thickbox();
		window.top.tb_remove();
	}
}

function Delete_Book(book_id)
{
	if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBook']?>')){
		$.post(
			"ajax_update.php",
			{
				"Action":"DeleteBook",
				"BookID":book_id 
			},
			function(ReturnData){
				if (ReturnData == "die") 
					window.top.location = '/';
				else{
					if(ReturnData==1){
						Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteBookSuccess']?>');
						Get_Book_Table();
					}else{
						Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteBookFail']?>');
					}
					Scroll_To_Top();
				}
			}
		);
	}
}

function Check_Cover_Image()
{
	$('input#submit_btn').attr('disabled',true);
	var Obj = document.getElementById("BookEditForm");
	Obj.target = 'CheckFileUploadIframe'; 
	Obj.encoding = "multipart/form-data";
	Obj.method = 'post';
	Obj.action = "ajax_update.php?Action=CheckCoverImage";
	Obj.submit();
	
	Restore_Form(Obj);
}

function Restore_Form(formObj)
{
	formObj.target = '_self';
	formObj.action = 'index.php';
	formObj.method = 'post';
}

function Delete_Book_Cover_Image(book_id)
{
	if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteCoverImage']?>'))
	{
		//Block_Thickbox();
		$.post(
			"ajax_update.php",
			{
				"Action":"DeleteImage",
				"BookID":book_id
			},
			function(ReturnData){
				if (ReturnData == "die") 
					window.top.location = '/';
				else{
					var ReturnMsg = '';
					if(ReturnData==1){
						$('div#DivCoverImage').fadeOut('fast').remove();
						$('input#CoverImage').css('display','block');
						Get_Book_Table();
						//$('div#FileUploadWarningDiv').after('<div id="CoverImageFeedbackDiv" style="color:green;">'+'<?=$Lang['ReadingGarden']['WarningMsg']['DeleteCoverImageSuccess']?>'+'</div>');
						ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteCoverImageSuccess']?>';
					}else{
						//$('div#FileUploadWarningDiv').after('<div id="CoverImageFeedbackDiv" style="color:red;">'+'<?=$Lang['ReadingGarden']['WarningMsg']['DeleteCoverImageFail']?>'+'</div>');
						ReturnMsg = '<?=$Lang['ReadingGarden']['ReturnMsg']['DeleteCoverImageFail']?>';
					}
					//setTimeout("$('div#CoverImageFeedbackDiv').remove();",5000);
					Get_Return_Message(ReturnMsg);
					//UnBlock_Thickbox();
					$('div#TB_ajaxContent').scrollTop(0);
				}
			}
		);
	}
}

function Remove_Book_Answer_Sheet(book_id)
{
	if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBookAnswerSheet']?>'))
	{
		$.post(
			"ajax_update.php",
			{
				"Action":"RemoveBookAnswerSheet",
				"BookID":book_id 
			},
			function(ReturnData){
				if(ReturnData=="1"){
					Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['RemoveBookAnswerSheetSuccess']?>');
					$('span#AnswerSheetSelectionSpan').css('display','');
					$('span#AnswerSheetManageSpan').css('display','none');
				}else{
					Get_Return_Message('<?=$Lang['ReadingGarden']['ReturnMsg']['RemoveBookAnswerSheetFail']?>');
				}
				$('div#TB_ajaxContent').scrollTop(0);
			}
		);
	}
}

function Load_Category_Selection(obj)
{
	var thisObj = $(obj);
	thisObj.attr('checked',true);
	var Language = thisObj.val();
	$.post(
		"../ajax_reload.php",
		{
			"Action":"GetCategorySelection",
			"Language":Language,
			"SelCategoryID":$('#SelCategoryID').val()
		},
		function(ReturnData){
			$('td#TdCategoryLayer').html(ReturnData);
		}
	);
}

function js_Check_Book_Source_Filter()
{
	if($("input.BookSource:checked").length==0)
	{
		alert(globalAlertMsg2); // Please check at least one item
	}
	else
	{
		Get_Book_Table();
		MM_showHideLayers('status_option','','hide');
	}
}

function js_Preview_Answer_Sheet()
{
	var TemplateID = $("#AnswerSheet").val();
	newWindow('../answer_sheet/preview.php?TemplateID='+TemplateID,10);
}

$().ready(function(){
	Get_Book_Table()
})
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>