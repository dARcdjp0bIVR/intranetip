<?php
// editing by 
/**
 * ***********************************************************************
 * modification log
 * Date: 2018-08-09 (Henry)
 * fixed sql injection
 * ***********************************************************************
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$BookID = IntegerSafe($_REQUEST['BookID']);

$lelib = new elibrary();
$objInstall = new elibrary_install();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$linterface = new interface_html("popup.html");

include_once($PATH_WRT_ROOT."/home/eLearning/elibrary/elib_script_function.php");

$linterface->LAYOUT_START();

echo $ReadingGardenUI->Include_Reading_Garden_CSS();

echo $ReadingGardenUI->Get_Book_Detail_Entry($BookID);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>