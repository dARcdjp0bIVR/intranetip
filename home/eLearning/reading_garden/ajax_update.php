<?php
// editing by 

/************************************************************************************
 * Date: 	2013-02-19 (Rita) 
 * Details: modified case AddReadingRecord for customization [#2012-1009-1402-14054] 
 ***********************************************************************************/

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "AddLikeRecord":
		$FunctionName = trim(stripslashes($_REQUEST['FunctionName']));
		$FunctionID = $_REQUEST['FunctionID'];
		$ReadingGardenLib->Add_Like_Record($FunctionName, $FunctionID, $_SESSION['UserID']);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		
		echo $ReadingGardenUI->Get_Like_Comment_Div_Content($FunctionName, $FunctionID);
	break;
	case "DeleteLikeRecord":
		$FunctionName = trim(stripslashes($_REQUEST['FunctionName']));
		$FunctionID = $_REQUEST['FunctionID'];
		$ReadingGardenLib->Delete_Like_Record($FunctionName, $FunctionID, $_SESSION['UserID']);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		
		echo $ReadingGardenUI->Get_Like_Comment_Div_Content($FunctionName, $FunctionID);
	break;

	case "AddReadingRecord":
		$result = array();
		
		$DataArr['Action'] = "add";
		$DataArr['BookID'] = $_REQUEST['BookID'];
		$DataArr['StartDate'] = $_REQUEST['StartDate'];
		$DataArr['FinishedDate'] = $_REQUEST['FinishedDate']; 
		$DataArr['ReadingHours'] = $_REQUEST['ReadingHours'];
		$DataArr['Progress'] = $_REQUEST['Progress'];
		$DataArr['AwardSchemeID'] = $_REQUEST['AwardSchemeID'];
		
		$result[] = $ReadingGardenLib->Manage_Student_Reading_Record($DataArr);
		
		# Customization [#2012-1009-1402-14054]  - Update Popular Award Scheme Togeter
		if($ReadingGardenLib->enableReadingSchemeAddPopularDataTogetherRight()){
			$Times = $_REQUEST['times'];
			$ItemID = $_REQUEST['PopularAwardSchemeItem'];
			$StudentID = $_SESSION['UserID']; 
			if($ItemID!=''){
				for($i=0; $i<$Times; $i++){
					$result[] = $ReadingGardenLib->Submit_Popular_Award_Scheme_Record($ItemID, $StudentID);
				}
			}
		}

		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		
		//if($result)
		if(count($result)>0 && !in_array(false, (array)$result)){
			echo $ReadingGardenUI->Get_Return_Message("AddReadingRecordSuccess");
		}else{
			echo $ReadingGardenUI->Get_Return_Message("AddReadingRecordFail");
		}
		
	break;
	case "UpdateReadingRecord";
		$DataArr['Action'] = "update";
		$DataArr['ReadingRecordID'] = $_REQUEST['ReadingRecordID'];
		$DataArr['BookID'] = $_REQUEST['BookID'];
		$DataArr['StartDate'] = $_REQUEST['StartDate'];
		$DataArr['FinishedDate'] = $_REQUEST['FinishedDate']; 
		$DataArr['ReadingHours'] = $_REQUEST['ReadingHours'];
		$DataArr['Progress'] = $_REQUEST['Progress'];
		$DataArr['AwardSchemeID'] = $_REQUEST['AwardSchemeID'];
		
		$result = $ReadingGardenLib->Manage_Student_Reading_Record($DataArr);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		
		if($result)
			echo $ReadingGardenUI->Get_Return_Message("UpdateReadingRecordSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("UpdateReadingRecordFail");
	break;
	case "DeleteReadingRecord":
		$DataArr['Action'] = "delete";
		$DataArr['ReadingRecordID'] = $_REQUEST['ReadingRecordID'];
		$result = $ReadingGardenLib->Manage_Student_Reading_Record($DataArr);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		if($result)
			echo $ReadingGardenUI->Get_Return_Message("DeleteReadingTargetSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteReadingTargetFail");
	break;	
	
	case "UploadBookReportFile":
		$DataArr['BookReportID'] = $_REQUEST['BookReportID'];
		$DataArr['ReadingRecordID'] = $_REQUEST['ReadingRecordID'];
		$DataArr['BookReportFile'] = $BookReportFile;
		$DataArr['BookReportFile_name'] = stripslashes(trim($BookReportFile_name));
		$CallBack = $_REQUEST['CallBack'];
		
		$result = $ReadingGardenLib->Upload_Book_Report_Attachment($DataArr);

		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		if($result)
			$msg = $ReadingGardenUI->Get_Return_Message("UpdateBookReportSuccess");
		else
			$msg = $ReadingGardenUI->Get_Return_Message("UpdateBookReportFail");
			
		echo '<script type="text/javascript">'."\n";
		echo 'window.parent.Submit_Report_Post_Process("add",\''.$msg.'\',\''.$CallBack.'\');'."\n";
		echo '</script>'."\n";
		
	break;
	case "DeleteBookReportFile":
		$BookReportID = $_REQUEST['BookReportID'];
		$CallBack = $_REQUEST['CallBack'];
		
		$result = $ReadingGardenLib->Delete_Book_Report_Attachment($BookReportID);
		
		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		if($result)
			echo $ReadingGardenUI->Get_Return_Message("DeleteBookReportSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteBookReportFail");
	break;
	case "SubmitBookReportAnswerSheet":
		$BookReportID = $_REQUEST['BookReportID'];
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		$Answer = trim(urldecode(stripslashes($_REQUEST['aStr'])));
		$StudentSelectedAnswerSheetID = $_REQUEST['StudentSelectedAnswerSheetID'];
		$result = $ReadingGardenLib->Submit_Answer_To_Book_Report($BookReportID,$Answer,$ReadingRecordID, $StudentSelectedAnswerSheetID);
		if($result)
			echo "1";
		else
			echo "0";
	break;
	
	case "DeleteBookReportAnswerSheet":
		$BookReportID = $_REQUEST['BookReportID'];
		$result = $ReadingGardenLib->Submit_Answer_To_Book_Report($BookReportID,'','');

		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();

		if($result)
			echo $ReadingGardenUI->Get_Return_Message("DeleteAnswerSheetSuccess");
		else
			echo $ReadingGardenUI->Get_Return_Message("DeleteAnswerSheetFail");
//			echo "1";
//		else
//			echo "0";
	break;
	
	case "SubmitOnlineWriting":
		$BookReportID = $_REQUEST['BookReportID'];
		$ReadingRecordID = $_REQUEST['ReadingRecordID'];
		$AssignedReadingID = $_REQUEST['AssignedReadingID'];
		$OnlineWriting  = $_REQUEST['OnlineWriting'];
		$result = $ReadingGardenLib->Submit_Writing_To_Book_Report($BookReportID,$OnlineWriting,$ReadingRecordID);
		if($result)
			echo "1";
		else
			echo "0";
	break;	
	
	case "DeleteOnlineWriting":
		$BookReportID = $_REQUEST['BookReportID'];
		$result = $ReadingGardenLib->Submit_Writing_To_Book_Report($BookReportID,'','');
		if($result)
			echo "1";
		else
			echo "0";
	break;	
	
	case "AddPopularAwardSchemeItemUpdate":
		$ReadingGardenLib->Start_Trans();
	
		for($i=0; $i<$Times; $i++)
			$success[] = $ReadingGardenLib->Submit_Popular_Award_Scheme_Record($ItemID, $StudentID);
			
		if(count($success)>0 && !in_array(false, (array)$success))
		{
			$ReadingGardenLib->Commit_Trans();
			$msg = "AddPopularAwardSchemeRecordSuccess";
		}
		else
		{
			$ReadingGardenLib->RollBack_Trans();
			$msg = "AddPopularAwardSchemeRecordFail";
		}

		include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
		$ReadingGardenUI = new libreadinggarden_ui();
		
		echo $ReadingGardenUI->Get_Return_Message($msg);
	break;
}

intranet_closedb();
?>