<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();
$fcm = new form_class_manage();
//$ColItem = "Progress";
$SeparateByLang = $SeparateByLang&&($ColItem!="Category");
$LangArray = $SeparateByLang?array(CATEGORY_LANGUAGE_CHINESE, CATEGORY_LANGUAGE_ENGLISH):array("total");

// prepare row item data
$RowItemArr = array();
switch($RowItem)
{
	case "Form":
		$StudentList = $fcm->Get_Active_Student_List($YearID);
		$RowItemArr = BuildMultiKeyAssoc($StudentList, "YearID", "YearName", 1);
	break;
	case "Class":
		$StudentList = $fcm->Get_Active_Student_List($YearID, $YearClassID);
		$RowItemArr = BuildMultiKeyAssoc($StudentList, "YearClassID", "ClassTitle", 1);
	break;
	case "Student":
		$StudentList = $fcm->Get_Active_Student_List($YearID, $YearClassID);
		$RowItemArr = BuildMultiKeyAssoc($StudentList, "UserID", "NameFieldWithClassNo", 1);
	break;
}

$StudentInfoArr = BuildMultiKeyAssoc($StudentList, "UserID");
$StudentIDArr = array_keys($StudentInfoArr);

// prepare col item data
$CategoryID = $ColItem=="Category"?$CategoryID:'';
$ReadingRecordArr = $ReadingGardenLib->Get_Student_Reading_Record_And_Report_Grade($StudentIDArr, '', '', $FromDate,$ToDate, '', '', $AcademicYearID='', $CategoryID, $AwardSchemeID);

$ColItemArr = array();
switch($ColItem)
{
	case "Month":
		$FromDateTS = strtotime($FromDate);
		$ToDateTS = strtotime($ToDate);
		for($TS = $FromDateTS; $TS<=$ToDateTS; $TS = strtotime("+1 Month", $TS))
		{
			$ColItemArr[date("Ym",$TS)] = $Lang['General']['month'][date("n",$TS)].' ('.date("Y",$TS).')';
		}
	break;
	case "Category":
		$CategoryInfo = $ReadingGardenLib->Settings_Get_Category_Info($CategoryID);
		$CategoryInfoAssoc = BuildMultiKeyAssoc($CategoryInfo, "CategoryID", "CategoryName",1);
		
		
		$sizeofCategory = sizeof($CategoryID);
		for($i=0; $i<$sizeofCategory; $i++)
		{
			if($CategoryID[$i]==0)
				$ColItemArr[''] = $Lang['ReadingGarden']['UnclassifiedCategory'];
			else
				$ColItemArr[$CategoryID[$i]] = $CategoryInfoAssoc[$CategoryID[$i]];
		}
		
	break;
	case "Total":
		$ColItemArr["total"] = $Lang['ReadingGarden']['Total'];
	break;
}

// Prepare Statistic Arr
$ReadingRecordCount = $BookReportCount = array();
$sizeofRecord = sizeof($ReadingRecordArr);

for($i=0; $i<$sizeofRecord; $i++)
{
	$StudentID = $ReadingRecordArr[$i]["UserID"];
	
	switch($RowItem)
	{
		case "Student":
			$RowID = $StudentID;
		break;
		case "Class":
			$RowID = $StudentInfoArr[$StudentID]['YearClassID'];
		break;
		case "Form":
			$RowID = $StudentInfoArr[$StudentID]['YearID'];
		break;
	}
	
	switch($ColItem)
	{
		case "Month":
			$ColID = Date("Ym",strtotime($ReadingRecordArr[$i]["FinishedDate"]));
		break;
		case "Category":
			$ColID = $ReadingRecordArr[$i]["CategoryID"];
		break;
		case "Total":
			$ColID = "total";
		break;
	}
	
	$ReadingLangCount[$RowID][$ColID][$ReadingRecordArr[$i]['Language']]++; // Lang count
	$ReadingLangCount[$RowID][$ColID]["total"]++; // total count
	
	if($ReadingRecordArr[$i]["BookReportID"])
	{
		$BookReportLangCount[$RowID][$ColID][$ReadingRecordArr[$i]['Language']]++; // Lang count
		$BookReportLangCount[$RowID][$ColID]["total"]++; // total count

		if($ReadingRecordArr[$i]["Grade"])
		{
			$MarkedLangCount[$RowID][$ColID][$ReadingRecordArr[$i]['Language']]++; // Lang count
			$MarkedLangCount[$RowID][$ColID]["total"]++; // total count
		}

	}
}

if($ViewFormat == "HTML")
{
	$SizeOfDisplayItem = 1;
	if($DisplayBookReport) $SizeOfDisplayItem++;
	if($DisplayMarkedReport) $SizeOfDisplayItem++;
	
	# header
	$x .= '<table class="full_border_table">'."\n";
	
//			if($SeparateByLang)
//			{
//				$colspan = ' colspan="2" ';
//				$rowspan = ' rowspan="2" ';
//			}
			$colspan = count($LangArray)*$SizeOfDisplayItem;
			$rowspan = 2+($SeparateByLang?1:0);
			if($colspan>1)
				$colspan_row1 = 'colspan="'.$colspan.'"';
			$x .= '<tr>'."\n";
				$x .= '<th rowspan="'.$rowspan.'">&nbsp;</th>'."\n";
				
				if($ColItem!="Total")
				{
					foreach($ColItemArr as $key => $display)
						$x .= '<th '.$colspan_row1.' align="center">'.$display.'</th>'."\n";
				}
					
				$x .= '<th '.$colspan_row1.' align="center">'.$Lang['General']['Total'].'</th>'."\n";
			$x .= '</tr>'."\n";
			
			// second row
			if($SeparateByLang) 
			{
				if($SizeOfDisplayItem > 1)
					$colspan_row2 = 'colspan="'.$SizeOfDisplayItem.'"';
				
				$x .= '<tr>'."\n";
					if($ColItem!="Total")
					{
						foreach($ColItemArr as $key => $display)
						{
							$x .= '<th align="center" '.$colspan_row2.'>'.$Lang['ReadingGarden']['Ch'].'</th>'."\n";
							$x .= '<th align="center" '.$colspan_row2.'>'.$Lang['ReadingGarden']['En'].'</th>'."\n";
						}
					}
					$x .= '<th align="center" '.$colspan_row2.'>'.$Lang['ReadingGarden']['Ch'].'</th>'."\n";
					$x .= '<th align="center" '.$colspan_row2.'>'.$Lang['ReadingGarden']['En'].'</th>'."\n";
				$x .= '</tr>'."\n";
			}
			
			//third row
			$x .= '<tr>'."\n";
				if($ColItem!="Total")
				{
					foreach($ColItemArr as $key => $display)
					{
						foreach($LangArray as $thisLang)
						{
							$x .= '<th '.$colspan.' align="center">'.$Lang['ReadingGarden']['ReadingRecord'].'</th>'."\n";
							if($DisplayBookReport)
								$x .= '<th '.$colspan.' align="center">'.$Lang['ReadingGarden']['BookReport'].'</th>'."\n";	
							if($DisplayMarkedReport)
								$x .= '<th '.$colspan.' align="center">'.$Lang['ReadingGarden']['MarkedReport'].'</th>'."\n";
						}
					}
				}
				# total
				foreach($LangArray as $thisLang)
				{
					$x .= '<th '.$colspan.' align="center">'.$Lang['ReadingGarden']['ReadingRecord'].'</th>'."\n";
					if($DisplayBookReport)
						$x .= '<th '.$colspan.' align="center">'.$Lang['ReadingGarden']['BookReport'].'</th>'."\n";	
					if($DisplayMarkedReport)
						$x .= '<th '.$colspan.' align="center">'.$Lang['ReadingGarden']['MarkedReport'].'</th>'."\n";
				}
			$x .= '</tr>'."\n";			
			
			# loop row
			foreach($RowItemArr as $RowKey => $RowDisplay)
			{
				$x .= '<tr>'."\n";
					$x .= '<td>'.$RowDisplay.'</td>'."\n";

					$LangArray = $SeparateByLang?array(CATEGORY_LANGUAGE_CHINESE, CATEGORY_LANGUAGE_ENGLISH):array("total");
					$totalReadingCount = $totalBookReport = $totalMarkedReport = array();
					foreach($ColItemArr as $ColKey => $ColDisplay)
					{
						foreach($LangArray as $thisLang)
						{
							$ReadingCountDisplay = $ReadingLangCount[$RowKey][$ColKey][$thisLang]?$ReadingLangCount[$RowKey][$ColKey][$thisLang]:0;
							$BookReportCountDisplay = $BookReportLangCount[$RowKey][$ColKey][$thisLang]?$BookReportLangCount[$RowKey][$ColKey][$thisLang]:0;
							$MarkedReportCountDisplay = $MarkedLangCount[$RowKey][$ColKey][$thisLang]?$MarkedLangCount[$RowKey][$ColKey][$thisLang]:0;
							
							if($ColItem!="Total")
							{
								$x .= '<td align="center">'.$ReadingCountDisplay.'</td>'."\n";
								if($DisplayBookReport)
									$x .= '<td align="center">'.$BookReportCountDisplay.'</td>'."\n";
								if($DisplayMarkedReport)
									$x .= '<td align="center">'.$MarkedReportCountDisplay.'</td>'."\n";
							}
							
							$totalReadingCount[$thisLang] += $ReadingCountDisplay;
							$totalBookReport[$thisLang] += $BookReportCountDisplay;
							$totalMarkedReport[$thisLang] += $MarkedReportCountDisplay;
						}
					}
					
					foreach($LangArray as $thisLang)
					{
						$TotalDisplay = $totalReadingCount[$thisLang];
//						if($DisplayBookReport && $totalReadingCount[$thisLang]>0) // do not display book report number if no reading record submitted.
//							$TotalDisplay .= " (".$totalBookReport[$thisLang].")";
						$x .= '<td align="center">'.$TotalDisplay.'</td>'."\n";
						if($DisplayBookReport)
							$x .= '<td align="center">'.$totalBookReport[$thisLang].'</td>'."\n";
						if($DisplayMarkedReport)
							$x .= '<td align="center">'.$totalMarkedReport[$thisLang].'</td>'."\n";	
					}
					
				$x .= '</tr>'."\n";
			}
	$x .= '</table>'."\n";
	
	include_once($PATH_WRT_ROOT."templates/2009a/layout/print_header.php");
	echo $x ;
	include_once($PATH_WRT_ROOT."templates/2009a/layout/print_footer.php");
}
else
{
	$ExportArr = array();
	
	# header
	$ExportRowArr = array();
	$ExportRowArr[] = ''; 
	if($SeparateByLang) 
	{
		if($ColItem!="Total")
		{
			foreach($ColItemArr as $key => $display)
			{
	
				$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['ReadingRecord'].")(".$Lang['ReadingGarden']['Ch'].")";
				if($DisplayBookReport)
					$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['BookReport'].")(".$Lang['ReadingGarden']['Ch'].")";
				if($DisplayMarkedReport)
					$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['MarkedReport'].")(".$Lang['ReadingGarden']['Ch'].")";
					
				$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['ReadingRecord'].")(".$Lang['ReadingGarden']['En'].")";
				if($DisplayBookReport)
					$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['BookReport'].")(".$Lang['ReadingGarden']['En'].")";
				if($DisplayMarkedReport)
					$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['MarkedReport'].")(".$Lang['ReadingGarden']['En'].")";
			}
		}
		# Total
		$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['ReadingRecord'].")(".$Lang['ReadingGarden']['Ch'].")";
		if($DisplayBookReport)
			$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['BookReport'].")(".$Lang['ReadingGarden']['Ch'].")";
		if($DisplayMarkedReport)
			$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['MarkedReport'].")(".$Lang['ReadingGarden']['Ch'].")";
		$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['ReadingRecord'].")(".$Lang['ReadingGarden']['En'].")";
		if($DisplayBookReport)
			$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['BookReport'].")(".$Lang['ReadingGarden']['En'].")";
		if($DisplayMarkedReport)
			$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['MarkedReport'].")(".$Lang['ReadingGarden']['En'].")";
		$ExportArr[] = $ExportRowArr;
	}
	else
	{
		if($ColItem!="Total")
		{
			foreach($ColItemArr as $key => $display)
			{
				$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['ReadingRecord'].")";
				if($DisplayBookReport)
					$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['BookReport'].")";
				if($DisplayMarkedReport)
					$ExportRowArr[] = $display."(".$Lang['ReadingGarden']['MarkedReport'].")";
			}
		}			
		$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['ReadingRecord'].")";
		if($DisplayBookReport)
			$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['BookReport'].")";
		if($DisplayMarkedReport)
			$ExportRowArr[] = $Lang['General']['Total']."(".$Lang['ReadingGarden']['MarkedReport'].")";
			
		$ExportArr[] = $ExportRowArr;
	}
		
		
	# loop row	
	foreach($RowItemArr as $RowKey => $RowDisplay)
	{
		$ExportRowArr = array();	
		$ExportRowArr[] = $RowDisplay;
		
			$LangArray = $SeparateByLang?array(CATEGORY_LANGUAGE_CHINESE, CATEGORY_LANGUAGE_ENGLISH):array("total");
			$totalReadingCount = $totalBookReport = $totalMarkedReport = array();
			foreach($ColItemArr as $ColKey => $ColDisplay)
			{
				foreach($LangArray as $thisLang)
				{
					$ReadingCountDisplay = $ReadingLangCount[$RowKey][$ColKey][$thisLang]?$ReadingLangCount[$RowKey][$ColKey][$thisLang]:0;
					$BookReportCountDisplay = $BookReportLangCount[$RowKey][$ColKey][$thisLang]?$BookReportLangCount[$RowKey][$ColKey][$thisLang]:0;
					$MarkedReportCountDisplay = $MarkedLangCount[$RowKey][$ColKey][$thisLang]?$MarkedLangCount[$RowKey][$ColKey][$thisLang]:0;
					
					if($ColItem!="Total")
					{
						$ExportRowArr[] = $ReadingCountDisplay;
						if($DisplayBookReport)
							$ExportRowArr[] = $BookReportCountDisplay;
						if($DisplayMarkedReport)
							$ExportRowArr[] = $MarkedReportCountDisplay;
					}

					$totalReadingCount[$thisLang] += $ReadingCountDisplay;
					$totalBookReport[$thisLang] += $BookReportCountDisplay;
					$totalMarkedReport[$thisLang] += $MarkedReportCountDisplay;
				}
			}

			# Total
			foreach($LangArray as $thisLang)
			{
				$ExportRowArr[] = $totalReadingCount[$thisLang];
				if($DisplayBookReport)
					$ExportRowArr[] = $totalBookReport[$thisLang];
				if($DisplayMarkedReport)
					$ExportRowArr[] = $totalMarkedReport[$thisLang];
			}

		$ExportArr[] = $ExportRowArr;
	}
	
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$ExportTxt = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", 0, "11");
	$filename = "Statistic";
	$lexport->EXPORT_FILE("$filename.csv", $ExportTxt);
}



intranet_closedb();
?>