<?php
// using :
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "GetPeriodOfAcademicYear":
		$DateArr = getPeriodOfAcademicYear($AcademicYearID);
		$ReturnValue = Date("Y-m-d",strtotime($DateArr['StartDate'])).'|==|'.Date("Y-m-d",strtotime($DateArr['EndDate']));
		echo $ReturnValue;
	break;	
}	

intranet_closedb();
?>