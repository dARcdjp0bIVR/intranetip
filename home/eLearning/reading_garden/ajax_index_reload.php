<?php
// using : Rita
/*********
 * 2012-11-06 (Rita)
 * 		- add AcademicYearID for ReloadStudentReadingRecord
 * 
 *********/
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch ($Action)
{
	case "ReloadMyRecordArea":
		$TagSelected = stripslashes($_REQUEST['TagSelected']);
		echo $ReadingGardenUI->Get_Reading_Garden_Index_MyRecord_Area_Table($TagSelected);
	break;	
	
	case "ReloadRecommendReportArea":
		echo $ReadingGardenUI->Get_Index_Recommend_Book_Report_List();
	break;
		
	case "ReloadAnnouncementArea":
		$AnnouncementType = $_REQUEST['AnnouncementType'];
		echo $ReadingGardenUI->Get_Reading_Garden_Index_Announcement_List($ClassID,$AnnouncementType);
	break;	
		
	case "ReloadTeacherViewArea":
		$TagSelected = stripslashes($_REQUEST['TagSelected']);
		$ClassID = $_REQUEST['ClassID'];
		switch($TagSelected)
		{
			case "StudentState":
				echo $ReadingGardenUI->Get_Reading_Garden_Index_Teacher_View_Student_State($ClassID);
			break;
			case "ReadingTarget":
				echo $ReadingGardenUI->Get_Reading_Garden_Index_Teacher_View_Reading_Target($ClassID);
			break;
		}
	break;	
	
	case "ReloadStudentReadingRecord":
		$StudentID =  $_REQUEST['StudentID'];
		$ClassID =  $_REQUEST['ClassID'];
		$AcademicYearID = $_REQUEST['AcademicYearID'];
		
		echo $ReadingGardenUI->Get_Teacher_View_Student_Reading_Record($StudentID, $ClassID, $AcademicYearID); 
		
	break;	
	
}	

intranet_closedb();
?>