<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "GetTemplateTable":
		$field = trim($_REQUEST['field'])!=''?trim(urldecode(stripslashes($_REQUEST['field']))):0;
		$order = trim($_REQUEST['order'])!=''?trim(urldecode(stripslashes($_REQUEST['order']))):1;
		$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));
		
		$arrCookies[] = array("ck_answersheet_template_page_size", "numPerPage");
		$arrCookies[] = array("ck_answersheet_template_page_number", "pageNo");
		$arrCookies[] = array("ck_answersheet_template_page_order", "order");
		$arrCookies[] = array("ck_answersheet_template_page_field", "field");	
		$arrCookies[] = array("ck_answersheet_template_page_keyword", "Keyword");
		updateGetCookies($arrCookies);
		
		echo $ReadingGardenUI->Get_Answer_Sheet_Template_Table($field, $order, $pageNo, $numPerPage, $Keyword);
	break;
	case "GetTemplateForm":
		$IsNew = $_REQUEST['IsNew'];
		$TemplateID = $_REQUEST['TemplateID'];
		echo $ReadingGardenUI->Get_Answer_Sheet_Template_Edit_Form($IsNew,$TemplateID);
	break;
	case "GetTemplatePreviewForm":
		$TemplateID = $_REQUEST['TemplateID'];
		echo $ReadingGardenUI->Get_Answer_Sheet_Template_Preview_Form($TemplateID);
	break;
}

intranet_closedb();
?>