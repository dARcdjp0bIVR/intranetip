<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
	
intranet_auth();
intranet_opendb();

if(!isset($_REQUEST['ForumID']) || $_REQUEST['ForumID']=='')
	header("Location: ./forum.php");

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ForumID = $_REQUEST['ForumID'];
$ForumTopicID = $_REQUEST['ForumTopicID'];
//$ForumTopicID = sizeof($ForumTopicID)>0?$ForumTopicID[0]:$ForumTopicID;
$FromPost = $_REQUEST['FromPost'];

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID,1);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);
echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Forum_Topic_Edit_UI($ForumID,$ForumTopicID,$FromPost);

?>
<script>
function js_Check_Form()
{
	var warn_cnt = 0;
	var topic_subject = $.trim($('#TopicSubject').val());
	var message = FCKeditorAPI.GetInstance('Message').GetXHTML(false);
	message = $.trim(message.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
	
	if(topic_subject==''){
		$('#WarnTopicSubject').show();
		warn_cnt++;
	}else{
		$('#WarnTopicSubject').hide();
	}
	
	if(message==''){
		$('#WarnMessage').show();
		warn_cnt++;
	}else{
		$('#WarnMessage').hide();
	}
	
	if(warn_cnt==0)
	{
		checkOption(document.form1.AttachmentArr);	
		checkOptionAll(document.form1.AttachmentArr);
		document.form1.submit();
	}
}

function FCKeditor_OnComplete( editorInstance )
{
	var jsForumID = $('input#ForumID').val();
	var jsForumTopicID = $('input#ForumTopicID').val();
	if(jsForumID!='' && jsForumTopicID!=''){
	    $(document).ready(function(){
	    	$.post(
	    		"ajax_task.php",
	    		{
	    			"Action":"GetForumTopicContent",
	    			"ForumID":jsForumID,
	    			"ForumTopicID":jsForumTopicID
	    		},
	    		function(data){
	    			editorInstance.SetHTML(data);
	    		}
	    	);
	    });
	}
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
