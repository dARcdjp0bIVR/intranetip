<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");
	
intranet_auth();
intranet_opendb();

if(!isset($_REQUEST['ForumID']) || $_REQUEST['ForumID']=='' || !isset($_REQUEST['ForumTopicID']) || $_REQUEST['ForumTopicID']=='')
	header("Location: ./forum.php");

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ForumID = $_REQUEST['ForumID'];
$ForumTopicID = $_REQUEST['ForumTopicID'];
$ForumPostID = $_REQUEST['ForumPostID'];
$ReplyPostID = $_REQUEST['ReplyPostID'];
$ReplyTopicID = $_REQUEST['ReplyTopicID'];

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID,1);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);
echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Forum_Post_Edit_UI($ForumID,$ForumTopicID,$ForumPostID,$ReplyPostID,$ReplyTopicID);

?>
<script>
function js_Check_Form()
{
	var warn_cnt = 0;
	var post_subject = $.trim($('#PostSubject').val());
	var message = FCKeditorAPI.GetInstance('Message').GetXHTML(false);
	message = $.trim(message.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
	
	if(post_subject==''){
		$('#WarnPostSubject').show();
		warn_cnt++;
	}else{
		$('#WarnPostSubject').hide();
	}
	
	if(message==''){
		$('#WarnMessage').show();
		warn_cnt++;
	}else{
		$('#WarnMessage').hide();
	}
	
	if(warn_cnt==0)
	{
		checkOption(document.form1.AttachmentArr);	
		checkOptionAll(document.form1.AttachmentArr);
		document.form1.submit();
	}
}

function FCKeditor_OnComplete( editorInstance )
{
	var jsForumID = $('input#ForumID').val();
	var jsForumTopicID = $('input#ForumTopicID').val();
	var jsForumPostID = $('input#ForumPostID').val();
	var jsReplyPostID = $('input#ReplyPostID').val();
	var jsReplyTopicID = $('input#ReplyTopicID').val();
	var jsAction = "GetForumPostContent";
	if((jsForumID!='' && jsReplyTopicID!='')
		|| (jsForumTopicID!='' && (jsForumPostID!='' || jsReplyPostID!=''))
	){
		if(jsReplyTopicID!='') jsAction="GetForumTopicContent";
		var jsPostID = jsForumPostID;
		if(jsReplyPostID!='') jsPostID = jsReplyPostID;
		
	    $(document).ready(function(){
	    	$.post(
	    		"ajax_task.php",
	    		{
	    			"Action":jsAction,
	    			"ForumID":jsForumID,
	    			"ForumTopicID":jsForumTopicID,
	    			"ForumPostID":jsPostID 
	    		},
	    		function(data){
	    			if(jsReplyPostID!='' || jsReplyTopicID!=''){
	    				data = '<BLOCKQUOTE><div style="border-style:solid;border-width:1px;border-color:#CCCCCC;padding:20px 20px;">'+data+'</div></BLOCKQUOTE><br />';
	    			}
	    			editorInstance.SetHTML(data);
	    		}
	    	);
	    });
	}
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
