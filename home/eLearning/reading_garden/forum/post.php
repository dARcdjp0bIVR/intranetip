<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

if(!isset($ForumID) || $ForumID=='' || !isset($ForumTopicID) || $ForumTopicID=='')
	header("Location: ./forum.php");

$arrCookies = array();
$arrCookies[] = array("ck_forum_post_page_size", "numPerPage");
$arrCookies[] = array("ck_forum_post_page_number", "pageNo");
$arrCookies[] = array("ck_forum_post_page_order", "order");
$arrCookies[] = array("ck_forum_post_page_field", "field");	
$arrCookies[] = array("ck_forum_post_page_keyword", "Keyword");

if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

if($Keyword) $Keyword = stripslashes($Keyword);

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID,1);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Forum_Post_UI($ForumID,$ForumTopicID,$field,$order,$pageNo,$numPerPage,$Keyword,$RecordStatus);

?>
<script>
function js_Reload_Forum_Post_Table()
{
	var formData = $('#form1').serialize();
	formData += "&Action=ReloadForumPostTable";
	
	Block_Element("ForumPostTableDiv");
	$.ajax({  
		type: "POST",  
		url: "ajax_load.php",
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("div#ForumPostTableDiv").html(ReturnData);
			UnBlock_Element("ForumPostTableDiv");
			Scroll_To_Top();
		} 
	});
}

function js_Reload_Forum_Topic_Entry()
{
	Block_Element("ForumTopicTableDiv");
	$('#ForumTopicTableDiv').load(
		"ajax_load.php",
		{
			"Action":"ReloadForumTopicEntry",
			"ForumID":$('input#ForumID').val(),
			"ForumTopicID":$('input#ForumTopicID').val()
		},
		function(data)
		{
			UnBlock_Element("ForumTopicTableDiv");
		}
	);
}
/*
function js_Reload_Table_Tool()
{
	var $tableToolTd = $('#TableToolTd');
	var $recordStatus = $('#RecordStatus option');
	if($recordStatus.length>1)
	{
		$tableToolTd.html('');
		$tableToolTd.load(
			"ajax_load.php",
			{
				"Action":"ReloadForumPostTableTool",
				"RecordStatus":$('#RecordStatus').val(),
				"FieldName":"ForumPostID[]"
			},
			function(data){
				
			}
		);
	}
}
*/
function js_Perform_Action(jsAction,jsElementName)
{
	var items = $('input[name='+jsElementName+']:checked');
	if(items.length==0){
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
		return false;
	}
	var PostIDArr = new Array();
	items.each(function(index){
		PostIDArr[index]=$(this).val();
	}
	);
	var Action = '';
	switch(jsAction)
	{
		case "approve":
			Action = "ApprovePost";
		break;
		case "reject":
			Action = "RejectPost";
		break;
		case "delete":
			Action = "DeletePost";
		break;
	}
	
	if(jsAction=='approve' || jsAction=='reject')
	{
		$.post(
			"ajax_task.php",
			{
				"Action":Action,
				"ForumPostID[]":PostIDArr 
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				Scroll_To_Top();
				if(ReturnData.substring(0,1) == 1)
					js_Reload_Forum_Post_Table();
			}
		);
	}else if(jsAction=='delete')
	{
		if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeletePost']?>')){
			$.post(
			"ajax_task.php",
			{
				"Action":Action,
				"ForumPostID[]":PostIDArr 
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				Scroll_To_Top();
				if(ReturnData.substring(0,1) == 1)
					js_Reload_Forum_Post_Table();
			}
		);
		}
	}
}

function js_Change_Topic_Status(jsAction,jsForumTopicID)
{
	var action = '';
	if(jsAction=='approve')
		action="ApproveTopic";
	else if(jsAction=='reject')
		action="RejectTopic";
	$.post(
			"ajax_task.php",
			{
				"Action":action,
				"ForumTopicID":jsForumTopicID
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				Scroll_To_Top();
				if(ReturnData.substring(0,1) == 1)
					js_Reload_Forum_Topic_Entry();
			}
		);
}

function js_Delete_Post(jsForumPostID)
{
	if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeletePost']?>')){
		Block_Document();
		$.post(
			"ajax_task.php",
			{
				"Action":"DeletePost",
				"ForumPostID":jsForumPostID
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				Scroll_To_Top();
				UnBlock_Document();
				if(ReturnData.substring(0,1) == 1)
					js_Reload_Forum_Post_Table();
			}
		);
	}
}

function js_Delete_Topic(jsForumTopicID)
{
	if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteTopic']?>')){
		var jsForumID = $('#ForumID').val();
		window.location = './delete_topic.php?ForumID='+jsForumID+'&ForumTopicID='+jsForumTopicID;
	}
}

$(document).ready(function(){
	js_Reload_Forum_Topic_Entry();
	js_Reload_Forum_Post_Table();
});
</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>