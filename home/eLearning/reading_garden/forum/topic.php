<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

//if(!isset($ForumID) || $ForumID=='')
//	header("Location: ./forum.php");

$arrCookies = array();
$arrCookies[] = array("ck_forum_topic_page_size", "numPerPage");
$arrCookies[] = array("ck_forum_topic_page_number", "pageNo");
$arrCookies[] = array("ck_forum_topic_page_order", "order");
$arrCookies[] = array("ck_forum_topic_page_field", "field");	
$arrCookies[] = array("ck_forum_topic_page_keyword", "Keyword");

if($clearCoo == 1)
	clearCookies($arrCookies);
else
	updateGetCookies($arrCookies);

if($Keyword) $Keyword = stripslashes($Keyword);

$linterface = new interface_html();
$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

$ReadingGardenLib->Update_Portal_Class_Session();

$CurrentPageArr['eLearningReadingGarden'] = 1;

$MenuTitle = $ReadingGardenUI->Get_Reading_Garden_Index_Title($ClassID);
$TAGS_OBJ[] = array($MenuTitle, "", 0);

$MODULE_OBJ = $ReadingGardenUI->GET_MODULE_OBJ_ARR(1);

$ReturnMessage = $ReadingGardenUI->Get_Return_Message(urldecode($_REQUEST['Msg']));
$ReadingGardenUI->LAYOUT_START($ReturnMessage);

echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
echo $ReadingGardenUI->Get_Forum_Topic_UI($ForumID,$Keyword,$field,$order,$pageNo,$numPerPage,$RecordStatusm, $ClassID);

?>
<script>
function js_Reload_Forum_Topic_Table()
{
	var formData = $('#form1').serialize();
	formData += "&Action=ReloadForumTopicTable";
	
	Block_Element("ForumTopicTableDiv");
	$.ajax({  
		type: "POST",  
		url: "ajax_load.php",
		data: formData,
		cache:false,
		success: function(ReturnData) {
			$("div#ForumTopicTableDiv").html(ReturnData);
			js_Reload_Table_Tool();
			//initThickBox();
			UnBlock_Element("ForumTopicTableDiv");
			Scroll_To_Top();
		} 
	});
}

function js_Reload_Table_Tool()
{
	var $tableToolTd = $('#TableToolTd');
	var $recordStatus = $('#RecordStatus option');
	if($recordStatus.length>1)
	{
		$tableToolTd.html('');
		$tableToolTd.load(
			"ajax_load.php",
			{
				"Action":"ReloadForumTopicTableTool",
				"RecordStatus":$('#RecordStatus').val(),
				"FieldName":"ForumTopicID[]"
			},
			function(data){
				
			}
		);
	}
}

function js_Perform_Action(jsAction,jsElementName)
{
	var items = $('input[name='+jsElementName+']:checked');
	if(jsAction=='edit' && items.length!=1){
		alert(globalAlertMsg1);
		return false;
	}else	
	if(items.length==0){
		alert(globalAlertMsg2);
		return false;
	}
	var TopicIDArr = new Array();
	items.each(function(index){
		TopicIDArr[index]=$(this).val();
	}
	);
	var Action = '';
	switch(jsAction)
	{
		case "markontop":
			Action = "MarkTopicOnTop";
		break;
		case "removeontop":
			Action = "RemoveTopicOnTop";
		break;
		case "approve":
			Action = "ApproveTopic";
		break;
		case "reject":
			Action = "RejectTopic";
		break;
		case "delete":
			Action = "DeleteTopic";
		break;
	}
	
	if(jsAction=='markontop' || jsAction=='removeontop' || jsAction=='approve' || jsAction=='reject')
	{
		$.post(
			"ajax_task.php",
			{
				"Action":Action,
				"ForumTopicID[]":TopicIDArr 
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				Scroll_To_Top();
				if(ReturnData.substring(0,1) == 1)
					js_Reload_Forum_Topic_Table();
			}
		);
	}else if(jsAction=='delete')
	{
		if(confirm('<?=$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteTopic']?>')){
			$.post(
			"ajax_task.php",
			{
				"Action":Action,
				"ForumTopicID[]":TopicIDArr 
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				Scroll_To_Top();
				if(ReturnData.substring(0,1) == 1)
					js_Reload_Forum_Topic_Table();
			}
		);
		}
	}else if(jsAction=='edit'){
		js_Go_Edit_Topic(TopicIDArr[0]);
	}
}

function js_Go_Edit_Topic(jsForumTopicID)
{
	var jsForumID = $('#ForumID').val();
	window.location = './edit_topic.php?ForumID='+jsForumID+'&ForumTopicID='+jsForumTopicID;
}

$(document).ready(function(){
	js_Reload_Forum_Topic_Table();
});

</script>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>