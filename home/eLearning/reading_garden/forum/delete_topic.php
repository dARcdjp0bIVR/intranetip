<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");

$ReadingGardenLib = new libreadinggarden();

$ForumID = $_REQUEST['ForumID'];
$ForumTopicID = $_REQUEST['ForumTopicID'];
$DataArr['ForumID'] = $ForumID;
$DataArr['ForumTopicID'] = $ForumTopicID;
$Action = "delete";

$Success = $ReadingGardenLib->Manage_Forum_Topic_Record($Action,$DataArr);

if($Success)
	$Msg = "DeleteSuccess";
else
	$Msg = "DeleteUnsuccess";

header("Location: ./topic.php?ForumID=$ForumID&Msg=$Msg");
intranet_closedb();
?>