<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
//include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

$key = trim($_REQUEST['key']);
if($key!=''){
	$valid_key = libreadinggarden::Grant_View_Right_For_Report_Page($key);
	if(!$valid_key) exit;
}else{
	intranet_auth();
}
//intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();

$BookReportID = $_REQUEST['book_report_id'];
$BookReportInfo = $ReadingGardenLib->Get_Student_Book_Report($BookReportID);
$OnlineWriting = $BookReportInfo[0]['OnlineWriting'];

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
?>
<table class='common_table_list_v30' width="80%" align="center" border="0">
	<tr>
		<td>
			<div >
				<?=nl2br(htmlspecialchars($OnlineWriting))?>
			</div>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
if($key!=''){
	unset($_SESSION['UserID']);
}
?>