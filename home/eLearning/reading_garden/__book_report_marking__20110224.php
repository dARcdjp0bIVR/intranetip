<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden.php");
include_once($PATH_WRT_ROOT."includes/libreadinggarden_ui.php");

intranet_auth();
intranet_opendb();

$ReadingGardenLib = new libreadinggarden();
$ReadingGardenUI = new libreadinggarden_ui();

?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/script.js" language="JavaScript"></script>
<!--<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css">-->
<link type="text/css" rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css">
<style type="text/css">
a {text-decoration: none;}
a:hover {color: #FF0000;}


.edit_pop_board {height:510px;background-color:#FFFFFF;}
.edit_pop_board h1{ height:25px; margin:0px; font-size:1em; font-weight:normal; padding-left:5px; display:block; line-height:20px; background-color:#ecedec}
.edit_pop_board h1 span{ font-weight:bold;}
.edit_pop_board_write { display:block; height:418px; overflow:auto; padding:5px; margin-bottom:2px;}
p.spacer { line-height:1px; height:0px; clear:both; display:block; padding:0px; margin:0px;}
.edit_bottom { height:50px; padding:3px; display:block; clear:both; border-top:1px dashed #CCCCCC; text-align:center}
.edit_bottom span { float:left; clear:both; display:block; font-size:0.92em; color:#666666}

.edit_pop_board_simple {height:210px;}
.edit_pop_board_simple .edit_pop_board_write{height:118px; }
.edit_pop_board_reorder {height:350px;}
.edit_pop_board_reorder .edit_pop_board_write{height:258px; }

.file {
	width: 300px;
}
.systemmsg {
	color: #FF0000;
	border: 1px dashed #FF0000;
}
#system_message_box {position:absolute; display:block; text-align:center; width:100%; clear:both; margin:-25px;}
.msg_board_left { height:30px; margin:0 auto; float:none; display:block; padding-left:8px; width:200px; background:url(../../../images/2009a/system_message_left.gif) no-repeat top left;}
.msg_board_right {height:30px; display:block; padding-right:8px; background:url(../../../images/2009a/system_message_right.gif) no-repeat top right; line-height:30px;}
.msg_board_right a{ font-size:0.95em;color:#1d7616; }
.msg_board_right a:hover{ color:#FF0000; }
.warning_box .msg_board_left { background-image:url(../../../images/2009a/system_message_warning_left.gif)}
.warning_box .msg_board_right { background-image:url(../../../images/2009a/system_message_warning_right.gif); color:#FFFFFF; font-weight:bold}
.warning_box .msg_board_right a{color:#FFFFFF; font-weight:normal}
.warning_box .msg_board_right a:hover{ text-decoration:underline}
.dotline {
	border-bottom : 1px dashed #999999;
}

.navigation {
	text-align: left;
	color: #000000;
	margin-bottom:5px;
}
.navigation a{
	color: #077C9E;
	text-decoration: underline;
}

.sectiontitle {
	font-size: 14px;
	font-weight: bold;
	color: #006600;
	padding-right: 3px;
	padding-left: 3px;
	padding-top: 5px;
	padding-bottom: 5px;
}

.sectiontitle_table {
	font-size: 1.2em;
	font-weight: bold;
	color: #FFFFFF;
}
.popuptitle {
	font-size: 1.2em;
	font-weight: bold;
	color: #00000;
}

.thumb_list{
	color:#000000;
	float:right;
	padding:2px;
	display:block;
	padding-bottom: 5px;
}
.thumb_list a{
	color:#014d94;
	
}
.thumb_list a:hover{
	color:#FF0000;
}
.thumb_list span{
	font-weight:bold
}


/*******/
.content_top_tool{	
	clear:both;
	display: block;
}

.Conntent_tool span{ display:block; float:left; height:20px; line-height:20px; padding-left:5px; padding-right:5px;}
.Conntent_tool a.print_option{  position:absolute;Z-index:1}
.Conntent_tool .print_option_layer{clear:both; position:absolute; padding:3px; display:block; margin-left:20px; margin-top:0px; visibility:hidden;line-height:18px; Z-index:99; width:400px;background-color:#e9eef2; border:1px solid #CCCCCC}
.Conntent_tool .print_option_layer2{clear:both; position:absolute; padding:3px; display:block; margin-left:20px; margin-top:0px; visibility:hidden;line-height:18px; Z-index:99; background-color:#e9eef2; border:1px solid #CCCCCC}
.Conntent_tool .print_option_layer em{margin:0; padding:0; line-height:18px; display:block; background:#aaaaaa; color:#FFFFFF}
.Conntent_tool .print_option_layer .edit_bottom{ height:25px;}
.Conntent_tool .print_option_layer .form_table col.field_title {width:100px;}
.Conntent_tool .generate_btn { background-position:0px -260px;}


.Conntent_tool .btn_option_layer em{line-height:18px; display:block; color:#FFFFFF; margin-bottom:2px; border-bottom:1px solid #FFFFFF; clear:both}
.Conntent_tool .btn_option_layer .edit_bottom{ height:20px; display:block;  }
.Conntent_tool .btn_option_layer .form_table { width:auto; /*width:220px;*/	margin:0 0;	border-collapse:separate;	border-spacing: 1px; 	*border-collapse: expression('separate', cellSpacing = '1px');} 
.Conntent_tool .btn_option_layer .form_table col.field_title {width:80px;}
.Conntent_tool .btn_option_layer .form_table tr td.field_title {width:80px;}


.thumb_list_tab{	display:block; text-align:right; height:20px; padding:2px; padding-bottom: 5px;}
.thumb_list_tab em{ display:block; float:right; line-height:20px; color:#dedede; font-style:normal;margin-right:2px;margin-left:2px;}
.thumb_list_tab a{ height:20px; display:block; float:right;  padding-left:5px; background-image:url(../../../images/2009a/thumb_list_tab_left.gif); background-repeat: no-repeat; background-position: -1000px -1000px;color:#014d94;}
.thumb_list_tab a span{ background-image:url(../../../images/2009a/thumb_list_tab_right.gif); background-repeat: no-repeat; background-position:-1000px -1000px; display:block;  padding-right:5px; height:20px; line-height:20px; cursor: hand}
.thumb_list_tab a.thumb_list_tab_on{  background-position: left top; color:#FFFFFF; font-weight:bold}
.thumb_list_tab a.thumb_list_tab_on span{ background-position: right top;}
.thumb_list_tab a:hover{ background-position:left top; color:#FFFFFF}
.thumb_list_tab a:hover span{ background-position: right top;}
/*******/
</style>
<?
echo $ReadingGardenUI->Include_JS_CSS();
echo $ReadingGardenUI->Include_Reading_Garden_CSS();
?>
<script language="JavaScript">
function js_Save_Marking(jsClose)
{
	Block_Element("DivMarkingArea");
	var Recommend = ($('#Recommend').attr('checked')?1:0);
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"MarkBookReport",
			"BookReportID":$('input#BookReportID').val(),
			"Grade":$('input#Grade').val(),
			"TeacherComment":encodeURIComponent($('#TeacherComment').val()),
			"Recommend":Recommend,
			"OldRecommend":$('input#OldRecommend').val(),
			"StudentID":$('input#StudentID').val()
		},
		function(ResponseData){
			if(ResponseData=="1"){
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				$('input#OldRecommend').val(Recommend);
				
				if(parent.opener.js_Mark_Report_CallBack)
					parent.opener.js_Mark_Report_CallBack();
			}else{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
			}
			UnBlock_Element("DivMarkingArea");
			if(jsClose==1){
				window.parent.close();
				
			}
		}
	);
}
/*
function js_Load_Attachment()
{
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportAttachment",
			"ReadingRecordID":$('#ReadingRecordList').val()
		},
		function(ReturnData){
			if(ReturnData!=''){
				var indexLastSlash = ReturnData.lastIndexOf('/',ReturnData);
				var folder = ReturnData.substring(0,indexLastSlash+1);
				var file = encodeURIComponent(ReturnData.substr(indexLastSlash+1));
				top.ViewFrame.location = "<?=$ReadingGardenLib->BookReportPath?>"+folder+file;
			}else{
				$(window.parent.frames["ViewFrame"].document).html("");
			}
		}
	);
}

function js_Load_Answersheet()
{
	var reading_record_id = $('#ReadingRecordList').val();
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportID",
			"ReadingRecordID":reading_record_id 
		},
		function(ReturnData){
			top.AnswerSheetFrame.location = 'answer_sheet.php?inframe=1&reading_record_id='+reading_record_id+'&book_report_id='+ReturnData;
		}
	);
}
*/

function js_Load_Attachment()
{
	$('#AnswerSheetBtn').attr('class','');
	$('#AttachmentBtn').attr('class','thumb_list_tab_on');
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportAttachment",
			"ReadingRecordID":$('#ReadingRecordList').val()
		},
		function(ReturnData){
			if(ReturnData!=''){
				var indexLastSlash = ReturnData.lastIndexOf('/',ReturnData);
				var folder = ReturnData.substring(0,indexLastSlash+1);
				var file = encodeURIComponent(ReturnData.substr(indexLastSlash+1));
				top.ViewFrame.location = "<?=$ReadingGardenLib->BookReportPath?>"+folder+file;
			}
		}
	);
}

function js_Load_Answersheet()
{
	$('#AnswerSheetBtn').attr('class','thumb_list_tab_on');
	$('#AttachmentBtn').attr('class','');
	var reading_record_id = $('#ReadingRecordList').val();
	$.post(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportID",
			"ReadingRecordID":reading_record_id 
		},
		function(ReturnData){
			if(ReturnData!=''){
				top.ViewFrame.location = 'answer_sheet.php?inframe=1&reading_record_id='+reading_record_id+'&book_report_id='+ReturnData;
			}
		}
	);
}

function js_Load_Marking_Area()
{
	var edit_frame_dom = $(window.parent.frames["EditFrame"].document)
	var edit_frame_div = edit_frame_dom.find('div#DivMarkingArea');
	edit_frame_div.parent().load(
		"ajax_marking_task.php",
		{
			"Action":"GetBookReportMarkingEditArea",
			"ReadingRecordID":$('#ReadingRecordList').val()
		},
		function(ReturnData){
			
		}
	);
}

function js_Refresh_View_Buttons()
{
	$('#ViewButtonsLayer').load(
		"ajax_marking_task.php",
		{
			"Action":"GetViewButtonsLayer",
			"ReadingRecordID":$('#ReadingRecordList').val()
		},
		function(ReturnData){
			
		}
	);
}

function js_Refresh_Reading_Record()
{
	js_Load_Answersheet();
	js_Load_Attachment();
	js_Load_Marking_Area();
}
</script>
<?

$ReadingRecordID = $_REQUEST['ReadingRecordID'];
$RecommendOnly = $_REQUEST['RecommendOnly'];

$Part = $_REQUEST['Part'];

if($Part=='top'){// top part - student info area
	echo $ReadingGardenUI->Get_Book_Report_Marking_Top_Info_Area($ReadingRecordID,$RecommendOnly);
	echo '<script>'."\n";
	echo '$(document).ready(function(){
			js_Refresh_View_Buttons();
		  });';
	echo '</script>'."\n";
}else if($Part=='right'){ // right part - mark & comment area
	echo $ReadingGardenUI->Get_Book_Report_Marking_Edit_Area($ReadingRecordID);
}else{ // whole frameset
	echo $ReadingGardenUI->Get_Book_Report_Marking_Page($ReadingRecordID,$RecommendOnly);
}

intranet_closedb();
?>