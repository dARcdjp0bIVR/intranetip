<?php
/*
 * 	Log
 * 
 * 	Note:	only allow student to access this page
 * 
 *  2018-07-16 [Cameron] cast $myFriendID to integer to avoid sql injection [case #G142774]
 *   
 * 	2017-09-29 [Cameron]
 * 		- allow staff to use 'My Friends' function
 * 		- fix: check if pass in Friend is really my friend
 * 
 * 	2017-07-13 [Cameron] 
 * 		- apply open_my_friends function checking
 * 
 * 	2017-04-19 [Cameron] create this file
 * 
 */
 
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$libelibplus = new libelibplus();
$open_my_friends = $libelibplus->get_portal_setting('open_my_friends');

if (!$open_my_friends || (($_SESSION['UserType'] != USERTYPE_STUDENT) && ($_SESSION['UserType'] != USERTYPE_STAFF))) {
	header('Location: index.php');
}

$elibplus2_menu_tab = 'myfriends';

$DateRange = empty($DateRange) ? 'accumulated_by_friend' : $DateRange;		// default accumulated 
$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$DateRange);
$myFriendID = (int) $_REQUEST['FriendID'];
$isMyFriend = $lelibplus->isMyFriend($myFriendID);
if (!$isMyFriend) {
	header('Location: index.php');
}

$isPurchasedeBook = $lelibplus->isPurchasedeBook();

## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();

$libelibplus_ui = new libelibplus_ui();


$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
$offset = $record_per_page * ($page_no-1);
$content_layout = '';
$summary = array();

// get eBooks
if ($isPurchasedeBook) {	// have eBook
	$viewPara['FriendID'] = $myFriendID;
	$viewPara['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'h.DateModified';
	$viewPara['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'DESC';
	$viewPara['offset'] = $offset;
	$viewPara['limit'] = $record_per_page;

	list($viewed_total, $viewed_data) = $lelibplus->getMyFriendeBookViewed($viewPara);
		
	$nav_para['show_total'] = false;
	$nav_para['total'] = $viewed_total;
	$nav_para['current_page'] = $page_no;
	$nav_para['record_per_page'] = $record_per_page;
	$nav_para['unit'] = $Lang["libms"]["portal"]["myfriends"]["ebook_total"];
	$nav_para['tab_id'] = "act-history-ebooks"; 
	
	$content_layout = $libelibplus_ui->getMyFriendReadBookCoverUI($viewed_data, $nav_para);
	
	$summary['has_ebook'] = true;
}

// get physical books
if (!$plugin['eLib_Lite']) {	// have physical book
	$borrowPara['FriendID'] = $myFriendID;
	$borrowPara['sortby'] = $_REQUEST['sortby'] ? $_REQUEST['sortby'] : 'BorrowTime';
	$borrowPara['order'] = $_REQUEST['order'] ? $_REQUEST['order'] : 'DESC';
	$borrowPara['offset'] = $offset;
	$borrowPara['limit'] = $record_per_page;

	list($borrowed_total, $borrowed_data) = $lelibplus->getMyFriendBooksBorrowed($borrowPara);
	
	if (!$content_layout) {
		$nav_para['show_total'] = false;
		$nav_para['total'] = $borrowed_total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['unit'] = $Lang["libms"]["portal"]["myfriends"]["book_total"];
		$nav_para['tab_id'] = "act-history-books";
		
		$content_layout = $libelibplus_ui->getMyFriendReadBookCoverUI($borrowed_data, $nav_para);
	}
	$summary['has_pbook'] = true;
}

// get book reviews
$class_name = false;
list($review_total, $reviews_data)=$lelibplus->getUserReviews($myFriendID, $class_name, $offset, $record_per_page);


// get friend profile summary
list($summary['name'],$summary['class'],$summary['image']) = $lelibplus->getUserInfo($myFriendID,$classNameWithBracket=false);
$summary['viewed_total'] = $viewed_total;
$summary['borrowed_total'] = $borrowed_total;
$summary['reviewed_total'] = $review_total;
$summary['friend_id'] = $myFriendID;
$summary_layout = $libelibplus_ui->getFriendsProfileSummary($summary);

?>
<!--CONTENT starts-->
<div class="elib-content">
  <div class="container myfriends"> <a class="btn-back col-sm-1" href="myfriends.php"><span class="caret-left-red"></span><?=$Lang["libms"]["portal"]["myfriends"]["back"]?></a> 
    <!--FRIEND DETAILS starts-->
    	<?=$summary_layout?>
	<!--FRIEND DETAILS ends-->
	
    <!--FRIEND records start-->
    <div class="action-tabs tabs general-tabs friend-news">
      <ul id="history_record_tab" class="nav nav-tabs">
        <li class="active"><a href="#act-history-ebooks" data-toggle="tab"><?=$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["ebooks_viewed"]?></a></li>
        <li><a href="#act-history-books" data-toggle="tab"><?=$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["books_borrowed"]?></a></li>
        <li><a href="#act-history-reviews" data-toggle="tab"><?=$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["reviews"]?></a></li>
        <li><a href="#act-history-sharing" data-toggle="tab"><?=$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["sharing"]?></a></li>
      </ul>
      
      <form class="form-horizontal" name="history_record_form" id="history_record_form" method="post" action="myfriends_profile.php">
      	<div id="history_record_content" class="tab-content">
       	  <?=$content_layout?>
	  	</div>
	  	<input type="hidden" name="FriendID" value="<?=$myFriendID?>">
	  	<input type="hidden" name="view_type" id="view_type" value="">	<!-- cover / list -->
	  	<input type="hidden" name="sortby" id="sortby" value="">
	  	<input type="hidden" name="order" id="order" value="">
	  </form>
	</div>	
	<!--FRIEND activities end-->
  </div>
</div>
<!--CONTENT ends-->

<script language="javascript">
$(document).ready(function(){
	$('#history_record_tab a').on('click', function(e) {
	  	e.preventDefault();
	  	var tab = $(this).attr('href');
	  	tab = tab.replace('#','');
	  	$('#view_type').val(tab+'-cover');
	  	resetSortBy();
	  	ajax_get_history_record();
	});
	
	$(document).on('click', '.review-likes .btn-type1', function(e){    	
    	e.preventDefault();
		$.ajax({
			dataType: 'json',
			async: true,
			type: 'POST',
			url: 'ajax.php',
			data: {	'action': 'handleLike',
					'review_id': $(this).attr('data-review_id'),
					'book_id': $(this).attr('data-book_id')
				},
			success: handle_like_unlike,
			error: show_ajax_error
		});
        return false;
    });
	
	$(document).on('click', '.btn-views a span', function(e){			// cover / list view icon click
		var tab = $('#history_record_tab li.active a').attr('href').replace('#','');
		var target_type = $(e.target).parent().attr('href').replace('#','');
		resetSortBy();
		$('#view_type').val(tab+'-'+target_type);
		ajax_get_history_record();
	});
	
	$(document).on('change', '#page_no, #record_per_page', function(e) {
		e.preventDefault();
		ajax_get_history_record();
	});
	
	$(document).on('click','#PreviousBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())-1);
		$('#PreviousBtn').removeAttr('href');
		ajax_get_history_record();
	});

	$(document).on('click','#NextBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())+1);
		$('#NextBtn').removeAttr('href');
		ajax_get_history_record();
	});
	
	$('#unfriend').on('click', function(e) {
	  	e.preventDefault();
    	if (confirm('<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_unfriend"]?>')){
			$.ajax({
				dataType: 'json',
				async: true,
				type: 'POST',
				url: 'ajax_my_friends.php',
				data: {	
						'action': 'unfriend',
						'FriendID': $(this).attr('data-friend_id')
					},
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
						window.location = "myfriends.php";						
					}
				},
				error: show_ajax_error
			});
    	}        
	});
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
		
});

function sortBookList(field, order) {
	$('#sortby').val(field);
	$('#order').val(order);
	ajax_get_history_record();
}

// reset sortby & order to avoid non-exist field in interface
function resetSortBy() {
	$('#sortby').val('');
	$('#order').val('');
}

function ajax_get_history_record() {
	$.fancybox.showLoading();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_my_friends.php?action='+$('#view_type').val(),
		data : $('#history_record_form').serialize(),		  
		success: load_history_record,
		error: show_ajax_error
	});	
}

function load_history_record(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$.fancybox.hideLoading();
	  	$('#history_record_content').html(ajaxReturn.html);
	}
}	

function handle_like_unlike(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
        var like_data=ajaxReturn.data.split(',');
        $('body #like_'+like_data[0]+' .number-like').html(like_data[1] + '<span class="icon-like"></span>');
        $('body #like_'+like_data[0]+' .btn-type1').html(like_data[2]);
	}	
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
 	 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
