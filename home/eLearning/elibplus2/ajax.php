<?php
/*
 *  Using:
 *  
 * 	Log 
 * 
 *  2018-09-26 [Cameron] add case editBookReview, getBookReview [case #W139304]
 *      - apply IntegerSafe to review_id in handleLike
 *      
 *  2018-07-16 [Cameron] cast $book_id and $borrow_id to integer to avoid sql injection [case #G142774]
 *   
 * 	2016-06-13 [Cameron] create this file
 * 	
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened']) 	die($eLib_plus["html"]["closing"]);



$json['success'] = FALSE;
$non_json = false;
$x = '';
$book_id = $_GET['book_id'] ? $_GET['book_id'] : $_POST['book_id'];
$book_id = (int)($book_id ? $book_id : 0);
$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);

$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];  
switch($action) {
	case 'addFavourite':		// by GET
		$json['success'] = $lelibplus->addFavourite();
		break;

	case 'removeFavourite':		// by GET
		$json['success'] = $lelibplus->removeFavourite();
		break;
	
	case 'handleReservation':	// by GET
		$x = $lelibplus->addOrRemoveReservation();
		$non_json = true;
		break;
		
	case 'handleLike':
	    $review_id = IntegerSafe($_POST['review_id']);
		$isAdd = $lelibplus->likeOrUnlikeReview($review_id);
		$votes = $lelibplus->getHelpfulVote($review_id);
		$likes = $votes['yesNum'];
		$likeOrUnlike=($isAdd)? $eLib_plus["html"]["unlike"] : $eLib_plus["html"]["like"];
		$x = $review_id.','.$likes.','.$likeOrUnlike;
		$json['success'] = true;
		$json['data'] = $x;
		break;
		
	case 'renewBook':		// by GET
	    $borrow_id = IntegerSafe($_GET['borrow_id']);
		$x = $lelibplus->renewBook($borrow_id);
		$non_json = true;
		break;
		
	case 'editBookReview':		// by GET
	    $reviewID = IntegerSafe($_GET['ReviewID']);
	    
	    $reviewAry = $lelibplus->getBookReviews($offset=0, $limit=1, $reviewID);
	    if (count($reviewAry)) {
	        $reviewAry = $reviewAry[0];
	        $rating = $reviewAry['rating'];
	        $content = $reviewAry['content'];
	        
	        $libelibplus_ui = new libelibplus_ui();
	        $x = $libelibplus_ui->getReviewEditUIByReviewID($reviewID, $rating, $content);
	        $non_json = true;
	    }
	    break;

	case 'getBookReview':		// by POST
	    $reviewID = IntegerSafe($_POST['ReviewID']);
        $libelibplus_ui = new libelibplus_ui();	        
        $x = $libelibplus_ui->getBookReviewContentUI($reviewID);
        $json['success'] = empty($x) ? false : true;
        $json['review'] = $x;
	    break;
	    
}

echo $non_json?$x:json_encode($json);

intranet_closedb(); 
?>