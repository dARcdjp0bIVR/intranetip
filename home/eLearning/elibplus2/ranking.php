<?
/*
 *  Using:
 *
 * 	Log
 *
 *  2019-10-09 [Cameron]
 *      - add thismonth range for ebook_most_hit [case #X157865]
 *
 * 	2017-01-17 [Cameron] 
 * 		- don't show eBook if client does not purchase, don't show pBook if it's $plugin['eLib_Lite'] in getRankingHeader()
 *  
 * 	2016-07-12 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$elibplus2_menu_tab = 'ranking';

$libelibplus = new libelibplus();

if ($_GET['more_type'] == 'ebook_most_hit') {
	$book_type = 'ebook';
	$ranking_type = 'most_hit';	
	$ebook_most_hit_range = $libelibplus->get_portal_setting('ebook_most_hit_range');
	if ($ebook_most_hit_range == 1) {
		$range = 'thisweek';
	}
    else if ($ebook_most_hit_range == 2) {
        $range = 'thismonth';
    }
	else {
		$range = 'accumulated';
	}				 
}
else if ($_GET['more_type'] == 'ebook_best_rated') {
	$book_type = 'ebook';
	$ranking_type = 'best_rated';
	$ebook_best_rated_range = $libelibplus->get_portal_setting('ebook_best_rated_range');
	if ($ebook_best_rated_range == 1) {
		$range = 'thisweek';
	}
	else {
		$range = 'accumulated';
	}				 
}
else if ($_GET['more_type'] == 'pbook_most_loan') {
	$book_type = 'physical';
	$ranking_type = 'most_loan';
	$pbook_most_loan_range = $libelibplus->get_portal_setting('pbook_most_loan_range');
	if ($pbook_most_loan_range == 1) {
		$range = 'thisweek';
	}
	else {
		$range = 'accumulated';
	}				 
}
else if ($_GET['more_type'] == 'pbook_best_rated') {
	$book_type = 'physical';
	$ranking_type = 'best_rated';	
	$pbook_best_rated_range = $libelibplus->get_portal_setting('pbook_best_rated_range');
	if ($pbook_best_rated_range == 1) {
		$range = 'thisweek';
	}
	else {
		$range = 'accumulated';
	}				 
}
else {
	$book_type = $_POST['book_type'] ? $_POST['book_type'] : '';
	if ($book_type == 'ebook') {
		$ranking_type = $_POST['ebook_ranking_type'];
		$range = $_POST['ebook_ranking_range'];
	}
	else if ($book_type == 'physical') {
		$ranking_type = $_POST['pbook_ranking_type'];
		$range = $_POST['pbook_ranking_range'];
	}
}

$ranking_type = $ranking_type ? $ranking_type : "best_rated";
$range = $range ? $range : "accumulated";
$layout = $layout ? $layout : "cover";

// elibplus version 1 use $_SESSION['ck_eLib_bookType'], now change to 'type_all', pass $book_type to filter record 
$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], 'type_all', $range);

$isPurchasedeBook = $lelibplus->isPurchasedeBook();

$book_type_selection_for_best_rated = '<select class="form-control" id="book_type">';
$book_type_selection_for_most_active_reviewers = '<select class="form-control" id="book_type">';
$book_type_selection_for_most_helpful_reviewers = '<select class="form-control" id="book_type">';

if ($isPurchasedeBook && !$plugin['eLib_Lite']) {
	$book_type_selection_for_best_rated .= '<option value="" '.(($book_type=='')?'selected':'').'>'.$Lang['libms']['bookmanagement']['allBooks'].'</option>';
	$book_type_selection_for_most_active_reviewers .= '<option value="" selected>'.$Lang['libms']['bookmanagement']['allBooks'].'</option>';
	$book_type_selection_for_most_helpful_reviewers .= '<option value="" selected>'.$Lang['libms']['bookmanagement']['allBooks'].'</option>';
}
 
if ($isPurchasedeBook) {
	$book_type_selection_for_best_rated .= '<option value="ebook" '.(($book_type=='ebook')?'selected':'').'>'.$Lang["libms"]["portal"]["eBooks"].'</option>';
	$book_type_selection_for_most_active_reviewers .= '<option value="ebook">'.$Lang["libms"]["portal"]["eBooks"].'</option>';
	$book_type_selection_for_most_helpful_reviewers .= '<option value="ebook">'.$Lang["libms"]["portal"]["eBooks"].'</option>';
}

if (!$plugin['eLib_Lite']) {
	$book_type_selection_for_best_rated .= '<option value="physical" '.(($book_type=='physical')?'selected':'').'>'.$Lang["libms"]["portal"]["pBooks"].'</option>';
	$book_type_selection_for_most_active_reviewers .= '<option value="physical">'.$Lang["libms"]["portal"]["pBooks"].'</option>';
	$book_type_selection_for_most_helpful_reviewers .= '<option value="physical">'.$Lang["libms"]["portal"]["pBooks"].'</option>';
}

$book_type_selection_for_best_rated .= '</select>';
$book_type_selection_for_most_active_reviewers .= '</select>';
$book_type_selection_for_most_helpful_reviewers .= '</select>';

switch($ranking_type) {
	case "best_rated":
		$rank_data	= $lelibplus->getBestRateBooks(10,$book_type);
		break;
	case "most_hit":
		$rank_data	= $lelibplus->getMostHitBooks(10,$book_type);
		break;
	case "most_loan":
		$rank_data	= $lelibplus->getMostLoanBooks(10);
		break;
	case "most_active_borrowers":
		$rank_data	= $lelibplus->getMostBorrowUsers(5);
		$layout = '';
		break;
	case "most_active_reviewers":
		$rank_users	= $lelibplus->getMostActiveUsers(10,$book_type,USERTYPE_STUDENT);
		$rank_data 	= array();
		foreach ($rank_users as $i=>$user){
		    list(,$user['reviews'])=$lelibplus->getUserReviews($user['user_id'], false, 0, 1, $book_type);
		    $rank_data[] = $user;
		}
		$layout = '';
		break;
	case "most_helpful_review":
		$rank_data=$lelibplus->getMostUsefulReviews(5,$book_type);
		$layout = '';
		break;
}
$tab_id = "tab-".str_replace("_","-",$ranking_type);

//debug_r($rank_data);
//debug_r($ranking_type);

$libelibplus_ui = new libelibplus_ui();

## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();

?>
  
<!--CONTENT starts-->
<div class="elib-content">
  <div class="container ranking">
  	<?=$libelibplus_ui->getRankingLeftMenu($ranking_type)?>
  	<div class="tab-content col-md-9">
      <div class="tab-pane active" id="<?=$tab_id?>">
      	<!-- table-header -->
		<?=$libelibplus_ui->getRankingHeader($ranking_type,$range,$layout,$book_type)?>
        
        <!-- table-body -->
        <div class="table-body">          
          <?
          	if (($ranking_type == "best_rated") || ($ranking_type == "most_hit") || ($ranking_type == "most_loan")) {
          		echo $libelibplus_ui->getRankingCoverviewBooks($rank_data);	
          	}
          	else if ($ranking_type == "most_active_borrowers") {
          		echo $libelibplus_ui->getRankingMostActiveBorrowers($rank_data);
          	}
          	else if ($ranking_type == "most_active_reviewers") {
          		echo $libelibplus_ui->getRankingMostActiveReviewers($rank_data);
          	} 
          	else if ($ranking_type == "most_helpful_review") {
          		echo $libelibplus_ui->getRankingMostHelpfulReview($rank_data);
          	} 
          ?>          
        </div>        
  	  </div>
  	</div>  
  </div>
</div>
<!--CONTENT ends--> 


<script language="javascript">
$(document).ready(function(){
	$(document).on('click', '.btn-views a span', function(e){
		var target_type = $(e.target).parent().attr('href').replace('#','');	// cover or list
		change_filter('view',target_type);
	});

	$(document).on('change', '#range, #book_type', function(){
		var target_type = '';
		if ($('.btn-views a').length) {
			target_type = $('.btn-views a').attr('href').replace('#','');		
			target_type = (target_type == 'list') ? 'cover' : 'list';	// toggle it
		}
		change_filter('range',target_type);
	});

		
	$(document).on('click', '.side-tabs li.type1:not(.active)', function(e){
		var rank_type = $('.side-tabs li.active a').attr('id');     // the one just click (from non-active to active now)
		var title;

        $(".icon1").addClass("on");
        $(".icon2").removeClass("on");
        $(".icon3").removeClass("on");
		
		$(e.target).parent().siblings().removeClass('active');
		$(e.target).parent().addClass('active');
		if (!$('.btn-views a').length) {
			$('#range').after('<span class="btn-table-view off glyphicon glyphicon-th-large"></span><a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>');
		}
		if (rank_type == 'best_rated') {
			if ($('#book_type').length) {
				$('#book_type').remove();	
			}			
			$('#range').before('<?=$book_type_selection_for_best_rated?>');
		}
		else if ((rank_type != 'best_rated') && ($('#book_type').length)) {
			$('#book_type').remove();
		}
		
		switch(rank_type) {
			case "best_rated":
					title = "<?=$Lang["libms"]["portal"]["best_rated"]?>";
				break;
			case "most_hit":
					title = "<?=$Lang["libms"]["portal"]["most_hit"]?>";
				break;
			case "most_loan":
					title = "<?=$Lang["libms"]["portal"]["most_loan"]?>";
				break;
		}

		if (rank_type == 'most_hit') {
		    $('option#thismonth').css('display','');
        }
		else {
		    if ($('#range').val() == 'thismonth') {
                $('#range').val('accumulated');     // set to accumulated if this month is selected
            }
            $('option#thismonth').css('display','none');
        }
		$('#header-title').html(title);
		$('.tab-pane').attr('id',$('.side-tabs li.active a').attr('href').replace('#',''));
		$('.table-body').html('');
		
		var target_type = $('.btn-views a').attr('href').replace('#','');		
		target_type = (target_type == 'list') ? 'cover' : 'list';	
		change_filter('range',target_type);

	});
	
	
	$(document).on('click', '.side-tabs li.type2:not(.active), .side-tabs li.type3:not(.active)', function(e){
		
		$(e.target).parent().siblings().removeClass('active');
		$(e.target).parent().addClass('active');

		var rank_type = $('.side-tabs li.active a').attr('id');
		var title;

		switch(rank_type) {
			case "most_active_borrowers":
					title = "<?=$eLib_plus["html"]["mostactiveborrowers"]?>";
			        $(".icon2").addClass("on");
			        $(".icon1").removeClass("on");
			        $(".icon3").removeClass("on");
				break;
			case "most_active_reviewers":
					title = "<?=$eLib_plus["html"]["mostactivereviewers"]?>";
			        $(".icon2").addClass("on");
			        $(".icon1").removeClass("on");
			        $(".icon3").removeClass("on");
				break;
			case "most_helpful_review":
					title = "<?=$eLib_plus["html"]["mosthelpfulreview"]?>";
			        $(".icon3").addClass("on");
			        $(".icon1").removeClass("on");
			        $(".icon2").removeClass("on");
				break;
		}

        if ($('#range').val() == 'thismonth') {
            $('#range').val('accumulated');     // set to accumulated if this month is selected
        }
        $('option#thismonth').css('display','none');

		$('#header-title').html(title);
		$('.tab-pane').attr('id',$('.side-tabs li.active a').attr('href').replace('#',''));
		$('#range').siblings().remove();

		if ((rank_type == "most_active_reviewers" || rank_type == "most_helpful_review")) {
			if ($('#book_type').length) {
				$('#book_type').remove();	
			}			
			if (rank_type == "most_active_reviewers") {
				$('.btn-views #range').before('<?=$book_type_selection_for_most_active_reviewers?>');
			}
			else if (rank_type == "most_helpful_review") {
				$('.btn-views #range').before('<?=$book_type_selection_for_most_helpful_reviewers?>');
			}
		}
				
		$('.table-body').html('');
		
		change_filter('range','');

	});
	
	
	$(document).on('click', '.review-likes .btn-type1', function(e){    	
    	e.preventDefault();
		$.ajax({
			dataType: 'json',
			async: true,
			type: 'POST',
			url: 'ajax.php',
			data: {	'action': 'handleLike',
					'review_id': $(this).attr('data-review_id'),
					'book_id': $(this).attr('data-book_id')
				},
			success: handle_like_unlike,
			error: show_ajax_error
		});
        return false;
    });

    $(document).on('click', '.ranking_user_list_log_open_shadow a, .no_of_review a', function(e){
    	e.preventDefault();
		var target = $(this).attr('href');
		if ($(target).is(':hidden')){
			$.ajax({
				dataType: "json",
				type: "POST",								
				url: 'ajax_ui.php',
				data : {'action': 'get_most_active_reviewers_review',
						'user_id': $(this).find('.id').html(),
						'offset': 0
					   },			
				success: function(ajaxReturn){
					if (ajaxReturn != null && ajaxReturn.success){
						$(target).fadeIn('slow').prev().children().addClass('ranking_user_list_log_open');
						$(target).find('ul').html(ajaxReturn.html);
					  	$(target).find('a.btn-more').attr('data-reviews-offset',ajaxReturn.offset);	// update reviews offset
					  	if (!ajaxReturn.show_more) {
					  		$(target).find('a.btn-more').hide();
					  	}
					  	else {
							$(target).find('a.btn-more').parent().show();					  		
					  	}
					}
				},
				error: show_ajax_error
			});
		}else{
		    $(target).fadeOut('slow').prev().children().removeClass('ranking_user_list_log_open');
		}
        return false;
    });
	
	$(document).on('click', '.more_reviews a', function(e) {
	  	e.preventDefault();
	  	var obj = $(e.target);

		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_ui.php',
			data : {
				'action': 'get_most_active_reviewers_review',
				'user_id': obj.attr('data-user-id'),
				'offset': obj.attr('data-reviews-offset')
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
				  	obj.parents('.ranking_user_review_detail').children('ul.ranking_user_review').append(ajaxReturn.html);
				  	$(e.target).attr('data-reviews-offset',ajaxReturn.offset);	// update reviews offset
				  	
				  	if (!ajaxReturn.show_more) {
						obj.parents('.more_reviews').hide();
				  	}
				  	else {
				  		obj.parents('.more_reviews').show();
				  	}
				}
			},
			error: show_ajax_error
		});
	});
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
});

function change_filter(change_type, target_type) {
	var rank_type = $('.side-tabs li.active a').attr('id');
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_ui.php',
		data : {'action': 'get_' + rank_type + (target_type == ''? '' : '_' + target_type),
				'range': $('#range').val(),
				'book_type': $('#book_type').length ? $('#book_type').val() : ''},			
		success: function(ajaxReturn){
			if (change_type == 'view') {	// listview or coverview
				show_result_with_view_change(ajaxReturn, target_type);
			}
			else {			// range or empty
				show_result_without_view_change(ajaxReturn);	
			}
		},
		error: show_ajax_error
	});
}

function show_result_with_view_change(ajaxReturn, target_type) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#range').siblings().not('#book_type').remove();
		if (target_type == 'cover') {
			$('#range').after('<span class="btn-table-view off glyphicon glyphicon-th-large"></span><a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>');
		}
		else {
			$('#range').after('<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a><span class="btn-list-view off glyphicon glyphicon-th-list"></span>');
		}
	  	$('.table-body').html(ajaxReturn.html);
	}
}

function show_result_without_view_change(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('.table-body').html(ajaxReturn.html);
	}
}

function handle_like_unlike(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
        var like_data=ajaxReturn.data.split(',');
        $('body #like_'+like_data[0]+' .number-like').html(like_data[1] + '<span class="icon-like"></span>');
        $('body #like_'+like_data[0]+' .btn-type1').html(like_data[2]);
	}	
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>