<?
/*
 * 	Log
 * 
 * 	2016-09-19 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

//intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$libelibplus = new libelibplus();

if ((isset($_GET['more_type']) && ($_GET['more_type'] != '')) || (isset($_POST['more_type']) && ($_POST['more_type'] != ''))) {
	$search_result = $libelibplus->get_more_records_by_group();
}

$books_data = $search_result['books_data'];
$nav_para = $search_result['nav_para'];

if(count($books_data)>1) {
	$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
}
else {
	$navigation_id = '';
}

$triangle_right = ' <span class="glyphicon glyphicon-triangle-right"></span> ';
switch ($_GET['more_type']) {
	case 'ebook_recommend_book':
		$result_title = $Lang["libms"]["portal"]["eBooks"] . $triangle_right . $eLib_plus["html"]["recommend"];
		break;
	case 'ebook_new':
		$result_title = $Lang["libms"]["portal"]["eBooks"] . $triangle_right . $eLib_plus["html"]["new"];
		break;
	case 'pbook_recommend_book':
		$result_title = $eLib_plus["html"]["others"] . $triangle_right . $eLib_plus["html"]["recommend"];
		break;
	case 'pbook_new':
		$result_title = $eLib_plus["html"]["others"] . $triangle_right . $eLib_plus["html"]["new"];
		break;
}


## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['msg']]);

$libelibplus_ui = new libelibplus_ui();

?>
  
<form name="form1" id="form1" method="post" action="more_books.php">
<!--CONTENT starts-->
<div class="elib-content">
<? 
	if ($search_result['view_type']=='cover') {
?>
  <div class="container search-result">
    <div class="table-header">
    	<?=$result_title?> <span class="total-no"><?=$nav_para['total']. ' '. $Lang["libms"]["portal"]["books_found"]?></span>
    	<div class="btn-views">
        	<span class="btn-table-view off glyphicon glyphicon-th-large"></span>  
    		<a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>
        </div>  
    </div>
    <div class="table-body">
    	<?include('book_cover_view.php');?>
    </div>
    
    <?=$libelibplus_ui->getSearchResultListFooter($nav_para)?>
    
  </div>
<?		
	}			// cover type
	else {		// list type
		$start_record_index = $record_per_page * ($page_no-1);
?>
  <div class="container search-result-list">
    <div class="table-header">
    	<?=$eLib["html"]["search_result"]?> <span class="total-no"><?=$nav_para['total']. ' '. $Lang["libms"]["portal"]["books_found"]?></span>
    	<div class="btn-views">
        	<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a> 
    		<span class="btn-list-view off glyphicon glyphicon-th-list"></span>
        </div>  
    </div>
    <div class="table-body">
		<?include('book_list_view.php');?>		
    </div>
    
	<?=$libelibplus_ui->getSearchResultListFooter($nav_para)?>
	
  </div>
		
<?	}   
?>
</div>
<!--CONTENT ends--> 

<!-- hidden form fields for navigation / sorting submit -->
	<input type="hidden" name="form_method" value="post">
	<input type="hidden" name="keyword" value="<?=$search_result['keyword']?>">
	<input type="hidden" name="title" value="<?=$search_result['title']?>">
	<input type="hidden" name="subtitle" value="<?=$search_result['subtitle']?>">
	<input type="hidden" name="author" value="<?=$search_result['author']?>">
	<input type="hidden" name="subject" value="<?=$search_result['subject']?>">
	<input type="hidden" name="publisher" value="<?=$search_result['publisher']?>">
	<input type="hidden" name="isbn" value="<?=$search_result['isbn']?>">
	<input type="hidden" name="call_number" value="<?=$search_result['call_number']?>">
	<input type="hidden" name="class_level_id" value="<?=$search_result['class_level_id']?>">
	<input type="hidden" name="tag_id" value="<?=$search_result['tag_id']?>">
	<input type="hidden" name="category" value="<?=$search_result['category']?>">
	<input type="hidden" name="subcategory" value="<?=$search_result['subcategory']?>">
	<input type="hidden" name="language" value="<?=$search_result['language']?>">
	<input type="hidden" name="sortby" id="sortby" value="<?=$search_result['sortby']?>">
	<input type="hidden" name="order" id="order" value="<?=$search_result['order']?>">
	<input type="hidden" name="view_type" id="view_type" value="<?=$search_result['view_type']?>">	
	<input type="hidden" name="more_type" id="more_type" value="<?=$search_result['more_type']?>">
	<input type="hidden" name="update_navigation" id="update_navigation" value="1">
	<input type="hidden" name="column_sorting" id="column_sorting" value="<?=$search_result['column_sorting']?>">
		
</form>

<script language="javascript">
$(document).ready(function(){

	$(document).on('change', '#page_no, #record_per_page', function(e) {
		e.preventDefault();
		ajax_search_book(1);
	});
	
	$(document).on('click','#PreviousBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())-1);
		$('#PreviousBtn').removeAttr('href');
		ajax_search_book(1);
	});

	$(document).on('click','#NextBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())+1);
		$('#NextBtn').removeAttr('href');
		ajax_search_book(1);
	});
	
	$(document).on('click', '.btn-views a span', function(e){
		var target_type = $(e.target).parent().attr('href').replace('#','');
		$('#view_type').val(target_type);
		$('#update_navigation').val('0');
		ajax_search_book(0);
		$('#update_navigation').val('1');	// set back to original value		
	});
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
});	

function load_search_result(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('.table-header .btn-views a').attr('href').replace('#','') == 'cover') {
			$('.table-header .btn-views').html('<span class="btn-table-view off glyphicon glyphicon-th-large"></span><a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>');
		}
		else {
			$('.table-header .btn-views').html('<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a><span class="btn-list-view off glyphicon glyphicon-th-list"></span>');
		}
		
	  	$('.table-body').html(ajaxReturn.html);
	}
}

function load_search_result_and_navigation(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('.table-body').html(ajaxReturn.html);
	  	$('.table-footer').replaceWith(ajaxReturn.footer_html);
	}
}

function ajax_search_book(update_navigation) {
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_search_result.php',
		data : $('#form1').serialize(),			
		success: update_navigation ? load_search_result_and_navigation : load_search_result,
		error: show_ajax_error
	});
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

function sortBookList(field, order) {
	$('#sortby').val(field);
	$('#order').val(order);
	ajax_search_book(1);	
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>