<?
/*
 *  Using:  
 *  
 * 	Log 
 *
 *  2018-09-26 [Cameron]
 *      - change onmouseover event for id^=act-review [case #W139304] 
 *      - add onClick event function for btnEditReview, btnUpdateReview, btnCancelReview
 *      
 *  2018-09-06 [Cameron] 
 *      - add case (result in (-5, -6)) for reserve book
 *  
 *  2018-07-16 [Cameron] cast book_id and cover_type_id to integer to avoid sql injection [case #G142774]
 *  
 * 	2018-01-30 [Cameron]
 * 		- show ItemSeriesNum when click "On Loan" and "Available" [case #L130405]
 *   
 * 	2017-09-29 [Cameron]
 * 		- allow staff to use 'My Friends' > Share functions
 * 
 * 	2017-07-13 [Cameron] 
 * 		- apply open_my_friends function checking
 * 
 * 	2017-04-28 [Cameron]
 * 		- update total in book review tab when add / delete review
 * 
 * 	2017-04-18 [Cameron]
 * 		- add share book function to my friends
 * 
 * 	2016-11-11 [Cameron] 
 * 		- add delete recommend button handling function
 * 
 * 	2016-11-08 [Cameron] 
 * 		- fix bug: don't show recommend tab for non-admin user 
 *
 * 	2016-06-10 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

if (!isset($_SESSION['UserID']) || $_SESSION['UserID'] == '') {
	$para = "";
	foreach((array)$_GET AS $k=>$v) {
		$para .= "$k=$v&";
	}
	if (!empty($para)) {
		$para = substr($para,0,-1);
	}
	header("location: opac_book_details.php?".$para);
	exit;
}

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

 
$hideTopMenuPart = ($_GET['target'] == '_blank') ? false : true;	// hide top menu or not

$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['msg']]);
$hideTopMenuPart = false;	// reset back to show

$libelibplus = new libelibplus();
$open_my_friends = $libelibplus->get_portal_setting('open_my_friends');

$libelibplus_ui = new libelibplus_ui();

$cover_type_id = (int) $_GET['cover_type_id'];	// generic physical book cover id
$cover_type_id = $cover_type_id ? $cover_type_id : rand(1,10);
$book_id = (int) $_GET['book_id'];
$book_id = $book_id ? $book_id : 0;
$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);

$detail_data=$lelibplus->getBookDetail();
$stat_data = $lelibplus->getBookStat();

if ($detail_data['image']) {
	$image_no = '';
}
else {
	$image_no = $cover_type_id;
}
$detail_data['image_no'] = $image_no;

if ($permission['admin']){
    $detail_data['recommend']=$lelibplus->getRecommendedClassLevels();
}

// get total book reviews
$total_reviews = $lelibplus->getTotalBookReviews();
$disp_total_reviews = sprintf($Lang["libms"]["portal"]["book"]["total_reviews"],$total_reviews);

?>

<!--CONTENT starts-->
<div class="elib-content">

  <!--BOOK DETAILS starts-->
  <div class="container book-details">
    <div class="table-header"><?=$Lang["libms"]["portal"]["book_details"]?></div>
    <div class="table-body bg-brown">
    	<div class="details-left col-sm-3">
        	<div class="book-covers">
        		<?=($detail_data['book_type'] == 'ebook') ? $libelibplus_ui->geteBookCover($detail_data, false, "div") : $libelibplus_ui->getpBookCover($detail_data, false, "div")?>
			</div>
<?php if ($stat_data['is_favourite']) : ?>
     <a id="handle-fav" href="#" data-book_id="<?=$detail_data['id']?>" class="btn-type1"><span class="icon-fav unfav"></span><?=$eLib_plus["html"]["removefavourite"]?></a>
<?php else: ?>
     <a id="handle-fav" href="#" data-book_id="<?=$detail_data['id']?>" class="btn-type1"><span class="icon-fav"></span><?=$eLib_plus["html"]["addtofavourite"]?></a>
<?php endif; ?>

<?php if ($stat_data['status'] && $permission['reserve']) : ?>
     <?php if ($stat_data['is_reserved']) : ?>
     <br><a href="ajax.php?action=handleReservation&book_id=<?=$detail_data['id']?>" class="btn-type1"><span class="icon-reserve cancel-reservation"></span><?=$eLib_plus["html"]["cancelhold"]?></a>
     <?php else: ?>
     	<?php if ($detail_data['user_status'] != 'borrowed'): ?>
     		<br><a href="ajax.php?action=handleReservation&book_id=<?=$detail_data['id']?>" class="btn-type1"><span class="icon-reserve"></span><?=$eLib_plus["html"]["requesthold"]?></a>
		<?php endif;?>     		
     <?php endif; ?>
<?php endif; ?>
		</div>
		
		<!-- book basic info, show if the field exist -->
    	<div class="details-right col-sm-9">
        	<div class="details-info-box">
	            <table class="list-table">
	            <tr><td class="list-title" colspan="2"><?=$detail_data['title']?><?=(trim($detail_data['sub_title'])!=''?'<br> - '.trim($detail_data['sub_title']):'')?></td></tr>
	            <tr><td class="list-label"><?=$eLib["html"]["author"]?>:</td><td>
		<? foreach ((array)$detail_data['authors'] as $i=>$author): ?>
		    <?=$i>0?', '.$author:'<a href="advanced_search.php?author='.(libelibplus::special_raw_string($author)).'" target="_parent">'.$author.'</a>'?>
		<? endforeach; ?>
	        	</td></tr>
            	
		<? if (!empty($detail_data['publisher'])) : ?>            	
            <tr><td class="list-label"><?=$eLib["html"]["publisher"]?>:</td><td><a href="advanced_search.php?publisher=<?=libelibplus::special_raw_string($detail_data['publisher'])?>" target="_parent"><?=$detail_data['publisher']?></a></td></tr>
        <? endif; ?>
        
		<? if (!empty($detail_data['publish_year'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]["publish_year"]?>:</td><td><?=$detail_data['publish_year']?></td></tr>
		<? endif; ?>
						
		<? if (!empty($detail_data['publisher_place'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]["publish_place"]?>:</td><td><?=$detail_data['publisher_place']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['category'])) : 
			if ($sys_custom['eLibraryPlus']['BookCategorySyncTwoLang']) {
				$catTitle = explode('-',$detail_data['category']);
				if (count($catTitle) > 2) {
					if ($intranet_session_language == 'en') {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[1]; 
					}	
					else {
						$dispTitle = $catTitle[0] . ' -' . $catTitle[2];
					}
				}
				else {
					$dispTitle = $detail_data['category'];
				}
			}
			else {
				$dispTitle = $detail_data['category']; 
			}		
		?>
			<tr><td class="list-label"><?=$eLib["html"]["category"]?>:</td><td><a href="advanced_search.php?category=<?=libelibplus::special_raw_string($detail_data['category'])?>" target="_parent"><?=$dispTitle?></a><?=(empty($detail_data['subcategory']))? '' : ' > <a href="advanced_search.php?page=search&subcategory='.libelibplus::special_raw_string($detail_data['subcategory']).'" target="_parent">'.$detail_data['subcategory'].'</a>'?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['subject'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]['Subject']?>:</td><td><?=$detail_data['subject']?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['level']) || $detail_data['level'] == "0") : ?>
			<tr><td class="list-label"><?=$eLib['Book']["Level"]?>:</td><td><a href="advanced_search.php?level=<?=libelibplus::special_raw_string($detail_data['level'])?>" target="_parent"><?=$detail_data['level']?></a></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['language'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]["language"]?>:</td><td><?=$detail_data['language']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['edition'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["edition"]?>:</td><td><?=$detail_data['edition']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['callnumber'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]['call_number']?>:</td><td><?=$detail_data['callnumber']?></td></tr>
		<? endif; ?>

		<? if (!empty($detail_data['isbns'])) :
			foreach((array)$detail_data['isbns'] as $k=>$v) {
				if (!empty($v)) {
					$isbns[] = $v;	
				}
			}
			if (!empty($isbns)):
		 ?>
			<tr><td class="list-label"><?=$eLib["html"]['ISBN']?>:</td><td><?=implode(", ",$isbns)?></td></tr>
		 <? endif; ?>
		<? endif; ?>

		<? if (!empty($detail_data['no_of_page'])) : ?>
			<tr><td class="list-label"><?=$eLib["html"]['NoOfPage']?>:</td><td><?=$detail_data['no_of_page']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['current_loan_count'])) : ?>
			<tr><td class="list-label"><?=$Lang["libms"]["report"]["onloan"]?>:</td>
				<td>
				<a href="javascript:void(0);" onClick="$('.item_details').hide();$('#borrowed_item_details').show();"><?=$detail_data['current_loan_count']?></a>
				<div id="borrowed_item_details" class="item_details" style="display:none;visibility:visible;">
					<div class="close"><a href="Javascript:void(0);" onClick="$('#borrowed_item_details').hide();"><?=$Lang['Btn']['Close']?></a></div>
				    <div class="body" style="position:relative;">
				   		<div class="fav">
				   			<table cellspacing="5" cellpadding="5">
				   				<tr style="font-weight:bold;">
				   					<td><?=$Lang["libms"]["book"]["code"]?></td>
				   					<td><?=$eLib_plus["html"]["duedate"]?></td>
				   					<td><?=$Lang["libms"]["book"]["series_number"]?></td>
				   				</tr>
				   				<?foreach((array)$detail_data['borrowed_item_list'] as $_itemAry):?>
				   					<tr>
				   						<td><?=$_itemAry['ACNO']?></td>
				   						<td><?=$_itemAry['duedate']?></td>
				   						<td><?=$_itemAry['ItemSeriesNum']?></td>
				   					</tr>
				   				<?endforeach;?>
				   				
				   			</table>
				   		</div>
				    </div>
				</div>	
				</td>
			</tr>
		<? endif; ?>
		
		<? 
		if($detail_data['type'] == 'physical') : 
		?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["noofcopyavailable_short"]?>:</td>
		<?if($detail_data['available_count']>0):?>
				<td>
				<a href="javascript:void(0);" onClick="$('.item_details').hide();$('#available_item_details').show();"><?=$detail_data['available_count']?></a>
				<div id="available_item_details" class="item_details" style="display:none;visibility:visible;">
					<div class="close"><a href="Javascript:void(0);" onClick="$('#available_item_details').hide();"><?=$Lang['Btn']['Close']?></a></div>
				    <div class="body" style="position:relative;">
				   		<div class="fav">
				   			<table cellspacing="5" cellpadding="5">
				   				<tr style="font-weight:bold;">
				   					<td width="25%"><?=$eLib_plus["html"]["location"]?></td>
				   					<td width="50%"><?=$Lang["libms"]["book"]["code"]?> (<?=$eLib["html"]["book_status"]?>)</td>
				   					<td width="25%"><?=$Lang["libms"]["book"]["series_number"]?></td>
				   					</tr>
				   				<?foreach((array)$detail_data['available_item_list'] as $_location => $_itemAry):?>
				   				<?$itemCnt = count($_itemAry);?>
				   					<tr>
				   						<td rowspan="<?=$itemCnt?>" valign="top"><?=$_location?$_location:$Lang["libms"]["status"]["na"]?></td>
				   						<td><?=$_itemAry[0]['ACNO']?> (<?=$Lang["libms"]["book_status"][$_itemAry[0]['status']]?>)</td>
				   						<td><?=$_itemAry[0]['ItemSeriesNum']?></td>
				   					</tr>
				   					<?for($ii=1;$ii<$itemCnt;$ii++):?>
				   						<tr>
				   							<td><?=$_itemAry[$ii]['ACNO']?> (<?=$Lang["libms"]["book_status"][$_itemAry[$ii]['status']]?>)</td>
				   							<td><?=$_itemAry[$ii]['ItemSeriesNum']?></td>
				   						</tr>
				   					<?endfor;?>
				   				<?endforeach;?>
				   				
				   			</table>
				   		</div>
				    </div>
				</div>
				</td>
			<?else:?>
				<td><?=$detail_data['available_count']?></td>
			<?endif;?>
			</tr>
		<? endif; ?>
		
			<tr id="reserved_count_row" style="display:<?=$detail_data['reserved_count']?'table-row':'none'?>">
				<td class="list-label"><?=$eLib_plus["html"]["noofcopyreserved_short"]?>:</td>
				<td id="reserved_count_text"><?=$detail_data['reserved_count']?></td></tr>
		
		<? if (!empty($detail_data['location'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["location"]?>:</td><td><?=$detail_data['location']?></td></tr>
		<? endif; ?>
		
		<? if (!empty($detail_data['tags'])) : ?>
			<tr><td class="list-label"><?=$eLib_plus["html"]["tags"]?>:</td><td>
			<? $t_idx = 0; ?>
		    <? foreach((array)$detail_data['tags'] as $tag):?>
			<?=($t_idx>0)?',':''?> <a href="advanced_search.php?tag_id=<?=$tag['tag_id']?>" target="_parent"><?=$tag['tag_name']?></a><? $t_idx = 1; ?>  
		    <? endforeach;?>
		<? endif;?>
				</td></tr>

		<? if (!empty($detail_data['physical_book_url'])) : 
			if ((strpos($detail_data['physical_book_url'],'http://') === false) && (strpos($detail_data['physical_book_url'],'https://') === false)) {
				$link = 'http://'.$detail_data['physical_book_url']; 
			}
			else {
				$link = $detail_data['physical_book_url']; 
			}
		?>
				<tr valign="top"><td class="list-label"><?=$Lang["libms"]["book"]["URL"]?>:</td><td><a target="_blank" href="<?=$link?>"><span style="display: block; width: 140px; word-wrap: break-word;"><?=$detail_data['physical_book_url']?></span></a></td></tr>
		<? endif; ?>
            
			<!-- book stat info: star rating, review count, hit rate, loan count -->
			<?=$libelibplus_ui->getBookStatUI($detail_data['id'])?>
			            
			</table> <!-- end list-table -->
			
			<!-- book description -->
          	<div class="description"><p><?=$detail_data['description']?></div>            
            </div> <!-- end details-info-box -->
            
            <!-- book recommmend info -->
            <?=$libelibplus_ui->getRecommendedInfo($detail_data['id'])?>
            
        </div>
    </div>
  </div>
  <!--BOOK DETAILS ends-->
  
  <!--ACTION BOX starts-->
  <div class="container action-box">
  	<div class="action-left col-sm-8">
  		<div class="action-tabs tabs general-tabs">
    		<ul class="nav nav-tabs">
        		<li class="active" id="review-tab"><a href="#act-review" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span><?=$eLib_plus["html"]["writereview"]?></a></li>
			<? if ($permission['admin']) : ?>        		
        		<li class="teacher" id="recommend-tab"><a href="#act-recommend" data-toggle="tab"><span class="icon-recommend"></span><?=$Lang["libms"]["portal"]["recommend"]?></a></li>
        	<? endif;?>	
        	<? if ($open_my_friends && ($_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_STAFF)):?>
        		<li id="share-tab"><a href="#act-share" data-toggle="tab"><span class="icon-share"></span><?=$Lang["libms"]["portal"]["share"]?></a></li>
        	<? endif;?>
        	</ul>
    		<div class="tab-content">
				<?=$libelibplus_ui->getReviewEditUI($detail_data['id'])?>
			<? if ($permission['admin']) : ?>				
				<?=$libelibplus_ui->getRecommendUI($detail_data['id'])?>
			<? endif;?>
			<? if ($open_my_friends && ($_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_STAFF)):?>			
				<?=$libelibplus_ui->getMyFriendShareBookEditUI($detail_data['id'])?>
			<? endif;?>
            </div>  
        </div>
        
       	<div class="all-reviews action-tabs tabs general-tabs">
      		<ul class="nav nav-tabs" id="my_history_record_tab" data-book_id="<?=$detail_data['id']?>">
          		<li class="active"><a href="#view-review" data-toggle="tab"><?=$disp_total_reviews?></a></li>
			<? if ($open_my_friends && ($_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_STAFF)):
				$sharedByMe = $lelibplus->getSharedByMeOfABook($detail_data['id']);			
				$disp_total_shared_by_me = sprintf($Lang["libms"]["portal"]["book"]["total_shared_by_me"],count($sharedByMe));			

				$sharedToMe = $lelibplus->getSharedToMeOfABook($detail_data['id']);			
				$disp_total_shared_to_me = sprintf($Lang["libms"]["portal"]["book"]["total_shared_to_me"],count($sharedToMe));			
			?>          		
          		<li><a href="#shared-to-me" data-toggle="tab"><?=$disp_total_shared_to_me?></a></li>
          		<li><a href="#shared-by-me" data-toggle="tab"><?=$disp_total_shared_by_me?></a></li>
          	<? endif;?>
          	</ul>
          	<div class="tab-content" id="my_history_tab_content">        
        <?=$libelibplus_ui->getReviewListUI($detail_data['id'])?>
        	</div>
        </div>
    </div> <!-- action-left -->
        
    <?=$libelibplus_ui->getRelatedBooksUI($detail_data['id'])?>
    
  </div>
  <!--ACTION BOX starts-->
  
</div>
<!--CONTENT ends-->  

<script language="javascript">
$(document).ready(function(){

	$(document).on('click', '#handle-fav', function(e) {
	  	e.preventDefault();	  	
	  	var action = $(this).children('span').hasClass('unfav') ? 'removeFavourite' : 'addFavourite';
		$.ajax({
			dataType: "json",
			type: "GET",
			url: 'ajax.php',
			data : {
				'action': action,
				'book_id': $(this).attr('data-book_id')
			},		  
			success: load_handle_fav_result,
			error: show_ajax_error
		});
	});

    $('.icon-reserve').parent().unbind("click").bind("click",function(e){
		if($('.icon-reserve').hasClass('cancel-reservation')){
			if(!confirm('<?=$Lang["libms"]["portal"]["book_detail"]["cancel_reserved_confirm"]?>'))
				return false;
		}

		$.get($(this).attr('href'),function(result){
		    var reservedCount = $('#reserved_count_text').text();
	    	
		    if (result==1){
				$('.icon-reserve').parent().html('<span class="icon-reserve cancel-reservation"></span><?=$eLib_plus["html"]["cancelhold"]?>');
		    	$('#reserved_count_text').html(parseInt(reservedCount) + 1);
		    	$('#reserved_count_row').show();
		    }else if (result==0){
				$('.icon-reserve').parent().html('<span class="icon-reserve"></span><?=$eLib_plus["html"]["requesthold"]?>');
		    	$('#reserved_count_text').html(parseInt(reservedCount) - 1);
		    }else{
		    	if (result==-1)
					alert('<?=$eLib_plus["html"]["cannothold_borrowed"]?>');		// not used
				else if(result==-2)
					alert('<?=$eLib_plus["html"]["cannothold_over_quota"]?>');
				else if(result==-3)
					alert('<?=$eLib_plus["html"]["cannothold_book_disable"]?>');	// not exist as system hide reserve icon if it's not open reserved
				else if(result==-4)
					alert('<?=$eLib_plus["html"]["cannothold_book_available"]?>');	
				else if(result==-5)
					alert('<?=$Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"]?>');		// not possible as it shows cancel reserve icon if the user has reserved the same book
				else if(result==-6)
		    		alert('<?php echo $Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve_of_book_category"];?>');
				else
					alert('<?=$eLib_plus["html"]["cannothold"]?>');
		    }
		});
		return false;                     
    });

    $(document).on('click', 'a.btnEditReview', function(e) {
        e.preventDefault();
        if ($('form#review_edit_form').length > 0 ) {
        	alert('<?php echo $Lang["libms"]["portal"]["warning"]["allow_edit_one_review_only"];?>');
			return;
        }
        $.fancybox.showLoading();
		var reviewID = $(this).attr('data-reviewID');
		$.get($(this).attr('href'),function(result){
			if (result != null) {
				$.fancybox.hideLoading();
				$('#review_span_' + reviewID).html(result);
			}
		});
		return false;                     
    });
	
    $('#review_form').submit(function(e){
        e.preventDefault();
        this.rate.value=$('#act-review').find('.star-on').length;
        if (this.content.value.length==0){
            alert('<?=$eLib_plus["html"]["pleaseentercontent"]?>');
            return false;
        }

        $(this).slideUp();
        $.fancybox.showLoading();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_ui_update.php?action=submitReview',
			data : $(this).serialize(),			
			success: add_review,
			error: show_ajax_error
		});
		
    });

	$('#review_form .btn-cancel').on('click', function(e){
		e.preventDefault();
		$('#review_form').slideUp();	
	});
	
    $(document).on('click','#remove_all_reviews',function(e){
        e.preventDefault();
        if (confirm('<?=$eLib_plus["html"]["areusuretoremoveallthereview"] ?>')){
			$.ajax({
				dataType: "json",
				type: "GET",
				url: $(this).attr('href'),
				success: remove_all_reviews,
				error: show_ajax_error
			});
        }
        return false;
    });

	
	$(document).on('click', '.review_detail_list .btn-remove', function(e) {
		$('.review_detail_list .btn-remove').off();
		e.preventDefault();
        if (confirm('<?=$eLib_plus["html"]["areusuretoremovethereview"] ?>')){
			$.ajax({
				dataType: "json",
				async: true,				
				type: "GET",
				url: $(this).attr('href'),
				success: remove_review,
				error: show_ajax_error
			});
        }
        return false;
	});
	

    $(document).on('mouseover', 'div[id^="act-review"] .ratings li', function(e){
        $(this).nextAll().removeClass('star-on');
        $(this).prevAll().addClass('star-on');
        $(this).addClass('star-on');
    });

	
	$(document).on('click', '.review-likes .btn-type1', function(e){
    	e.preventDefault();
		$.ajax({
			dataType: 'json',
			async: true,
			type: 'POST',
			url: 'ajax.php',
			data: {	'action': 'handleLike',
					'review_id': $(this).attr('data-review_id'),
					'book_id': $(this).attr('data-book_id')
				},
			success: handle_like_unlike,
			error: show_ajax_error
		});
        return false;
    });
	
	$('#more_reviews').on('click', function(e) {
	  	e.preventDefault();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_ui.php',
			data : {
				'action':'getMoreReviews',
				'book_id':$(this).attr('data-book_id'),
				'offset':$(this).attr('data-reviews-offset')
			},		  
			success: load_more_reviews,
			error: show_ajax_error
		});
	});
	
    $('#check_all').click(function(){
   		$('#recommend_form input:checkbox').prop('checked', $(this).prop('checked'));
    });
	
    $('#recommend_form').submit(function(e){
        e.preventDefault();
		if($('input[name="class_level_ids\[\]"]:checked').length == 0) {
			alert("<?=$Lang["libms"]["batch_edit"]["SelectAtLeastOneClassLevel"]?>");	
		}
		else {        
	        $(this).slideUp();
	        $.fancybox.showLoading();
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_ui_update.php?action=submitRecommend',
				data : $(this).serialize(),			
				success: load_recommend,
				error: show_ajax_error
			});
		}
    });
	
	$('#recommend_form .btn-cancel').on('click', function(e){
		e.preventDefault();
		$('#recommend_form').slideUp();	
	});

	$('#recommend_form .btn-delete').on('click', function(e){
		e.preventDefault();
		
		// reset form values
		$('#check_all').prop('checked',false);
		$('input[name="class_level_ids\[\]"]').prop('checked',false);
		$('textarea[name="content"]').val('');
		$('textarea[name="content"]').text('');
		
		$('#recommend_form').slideUp();	
        $.fancybox.showLoading();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_ui_update.php?action=deleteRecommend',
			data : {book_id: $('input[name="book_id"]').val()},			
			success: load_recommend,
			error: show_ajax_error
		});
	});
	
	$('#review-tab').on('click', function(e) {
		$('#act-share').css('display','');
		$('#review_form').slideDown();		
	});
	
	$('#recommend-tab').on('click', function(e) {
		$('#recommend_form').slideDown();		
	});

	$('#share-tab').on('click', function(e) {
		$('#act-share').slideDown();		
	});

	$('#act-share .btn-cancel').on('click', function(e){
		$('#act-share').slideUp();	
	});

	if (self != parent && typeof parent.$.fancybox !== 'undefined') {					
		if (parent.window.name == 'intranet_popup31') {
			parent.window.name = '';		// reset window name
		}			
	}					
	
	$(document).on('click', '#all_friends .user-photo, #all_friends .user-info', function(e){		
		$(this).parent().toggleClass('selected');
		update_selected_total();
    });
    
	$('#select_friend_tab a').on('click', function(e) {
	  	e.preventDefault();
	  	var type = $(this).attr('data-type');
	  	
	  	if (type == 'selected') {
			$('#all_friends li').not('.selected').hide();
	  	}
	  	else {
	  		$('#all_friends li').not('.selected').show();
	  	}
	});

	$('#btn_share').on('click', function(e) {
	  	e.preventDefault();
	  	var friend_ids = [];
	  	$('#all_friends li.selected').each(function(){
	  		friend_ids.push($(this).attr('data-friend-id'));
	  	});
	  	
	  	if (friend_ids.length > 0) {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_my_friends.php',
				data : {
						'action': 'share_book',
						'book_id': '<?=$detail_data['id']?>',
						'selected_friend_ids': friend_ids,
						'Comments': $('#Comments').val() 
					},		  
				success: load_my_shared_item,
				error: show_ajax_error
			});
	  	}
	  	else {
			alert("<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["select_at_least_one_friend"]?>");	
	  	}
	});
	
	$(document).on('click', '.remove_notification', function(e) {
		e.preventDefault();
        if (confirm('<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_notification"] ?>')){
        	var thisItem = $(this).parent();
			$.ajax({
				dataType: "json",
				async: true,				
				type: "POST",
				url: 'ajax_my_friends.php',
				data : {
						'action': 'remove_notification',
						'NotificationID': $(this).attr('data-notification_id')
					},		  
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
						thisItem.remove();  	
					}
				},
				error: show_ajax_error
			});
        }
        return false;
	});
	
	$('#my_history_record_tab a').on('click', function(e) {
	  	e.preventDefault();
	  	var tab = $(this).attr('href');
	  	tab = tab.replace('#','');
		$.fancybox.showLoading();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_my_friends.php',
			data : {'action': tab,
					'book_id': $(this).parent().parent().attr('data-book_id')			
				},					  
			success: load_my_history_record,
			error: show_ajax_error
		});	
	  	
	});

	$(document).on('click', '#shared-to-me .btn-remove', function(e) {
		$('#shared-to-me .btn-remove').off();
		e.preventDefault();
        if (confirm('<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_shared_to_me"]?>')){
			$.fancybox.showLoading();
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_my_friends.php',
				data : {'action': 'remove_shared_to_me',
						'ShareID': $(this).attr('data-ShareID'),
						'BookID': $(this).attr('data-BookID'),
						'update_tab_total': '1'			
					},					  
				success: load_my_history_record,
				error: show_ajax_error
			});	
        }
	});

    $(document).on('click', 'button#btnCancelReview', function(e) {
        e.preventDefault();
        $.fancybox.showLoading();
		var reviewID = $('input#ReviewID').val();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax.php',
			data : {'action': 'getBookReview',
					'ReviewID': reviewID
				},					  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$.fancybox.hideLoading();
					$('#review_span_' + reviewID).html(ajaxReturn.review);
				}
			},
			error: show_ajax_error
		});	
    });

    $(document).on('click', 'button#btnUpdateReview', function(e) {
        e.preventDefault();
		var $thisForm = $('form#review_edit_form'); 
        $thisForm.find('#editRating').val($('#act-review-edit').find('.star-on').length);
        if ($thisForm.find('#editContent').val().length==0){
            alert('<?=$eLib_plus["html"]["pleaseentercontent"]?>');
            return false;
        }
        
        $(this).slideUp();
        
        $.fancybox.showLoading();
		var reviewID = $('input#ReviewID').val();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_ui_update.php?action=updateReview',
			data : {
					'action': 'updateReview',
					'ReviewID': reviewID,
					'book_id': '<?php echo $detail_data['id'];?>',
					'Rating': $('input#editRating').val(),
					'Content': $('textarea#editContent').val()
				},					  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$.fancybox.hideLoading();
					$('#review_span_' + reviewID).html(ajaxReturn.html);
					$('#book_stat_row').replaceWith(ajaxReturn.book_stat_html);
				}
			},
			error: show_ajax_error
		});	
    });
    
});	

function update_selected_total() {
	var str = $('#selected_friends_count').text();
	var left_bracket_pos = str.indexOf('(');
	var right_bracket_pos = str.indexOf(')');
	var num = str.substring(left_bracket_pos+1,right_bracket_pos);
	$('#selected_friends_count').html(str.replace(num,$('#all_friends li.selected').length));
}

function load_handle_fav_result(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		if ($('#handle-fav span').hasClass('unfav')) {
			$('#handle-fav').html('<span class="icon-fav"></span>' + '<?=$eLib_plus["html"]["addtofavourite"]?>');
		}
		else {
			$('#handle-fav').html('<span class="icon-fav unfav"></span>' + '<?=$eLib_plus["html"]["removefavourite"]?>');
		}	  	
	}
}	

function remove_all_reviews(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
		$('#book_stat_row').replaceWith(ajaxReturn.book_stat_html);
		$('#my_history_tab_content').replaceWith(ajaxReturn.html);
		
		if (ajaxReturn.view_review_tab) {
			$('#my_history_record_tab').find('a[href="#view-review"]').html(ajaxReturn.view_review_tab);
		}	    
	}	
}

function add_review(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
		$('#book_stat_row').replaceWith(ajaxReturn.book_stat_html);
		$('.review_detail_list').prepend(ajaxReturn.html);
	    if ($('#book_stat_row span').attr('data-review-count') > 0 ) {
	    	$('.review_detail_list .no-record').remove();
	    	$('#remove_all_reviews').removeClass('hidden').addClass('show');
	    }
	    
		if (ajaxReturn.view_review_tab) {
			$('#my_history_record_tab').find('a[href="#view-review"]').html(ajaxReturn.view_review_tab);
		}	    
	    $.fancybox.hideLoading();
	}	
}

function remove_review(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#book_stat_row').replaceWith(ajaxReturn.book_stat_html);
	    $('.review_detail_list #review_log_'+ajaxReturn.review_id).slideUp(function(){$(this).remove()});
	    if ($('#book_stat_row span').attr('data-review-count') == 0 ) {
	    	$('.review_detail_list').html('<li class="no-record"><?=$eLib_plus["html"]["noreviews"]?></li>');
	    }
	    
		if (ajaxReturn.view_review_tab) {
			$('#my_history_record_tab').find('a[href="#view-review"]').html(ajaxReturn.view_review_tab);
		}	    
	}
}

function handle_like_unlike(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
        var like_data=ajaxReturn.data.split(',');
        $('body #like_'+like_data[0]+' .number-like').html(like_data[1] + '<span class="icon-like"></span>');
        $('body #like_'+like_data[0]+' .btn-type1').html(like_data[2]);
	}	
}
 
function load_more_reviews(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
	  	$('ul.review_detail_list').append(ajaxReturn.html);
	  	$('#more_reviews').attr('data-reviews-offset',ajaxReturn.offset);	// update new offset
	  	if (!ajaxReturn.show_more) {
	  		$('#more_reviews').hide();
	  	}
	}
}

function load_recommend(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#recommended-info').replaceWith(ajaxReturn.html);
	}
	$.fancybox.hideLoading();
}

function load_selected_friend_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#selected_friends').html(ajaxReturn.html);
	}
	$.fancybox.hideLoading();
}

function load_my_shared_item(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#my_history_tab_content').html(ajaxReturn.html);
		if (ajaxReturn.shared_by_me_tab) {
			$('#my_history_record_tab').find('a[href="#shared-by-me"]').html(ajaxReturn.shared_by_me_tab);
		}
	}
	$('#act-share').slideUp();
	$.fancybox.hideLoading();
}

function load_my_history_record(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#my_history_tab_content').html(ajaxReturn.html);
		if (ajaxReturn.shared_to_me_tab) {
			$('#my_history_record_tab').find('a[href="#shared-to-me"]').html(ajaxReturn.shared_to_me_tab);
		}
	}
	$.fancybox.hideLoading();
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
	$.fancybox.hideLoading();
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>