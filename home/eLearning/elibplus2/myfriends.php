<?php
/*
 * 	Log
 * 
 * 	Note:	only allow student to access this page
 * 
 * 	2017-10-16 [Cameron]
 * 		- add remove friend request function
 * 		- update pending friend list after addd friend request
 * 
 * 	2017-09-29 [Cameron]
 * 		- allow staff to use 'My Friends' function
 * 		- add parameter to getMostBorrowUsers() to include staff & student
 * 		- add seach function in my friend list
 * 
 * 	2017-09-27 [Cameron]
 * 		- add search function for adding new friends
 * 	
 * 	2017-07-13 [Cameron] 
 * 		- apply open_my_friends function checking
 * 
 * 	2017-03-29 [Cameron] create this file
 * 
 */
 
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$libelibplus = new libelibplus();
$open_my_friends = $libelibplus->get_portal_setting('open_my_friends');

if (!$open_my_friends || (($_SESSION['UserType'] != USERTYPE_STUDENT) && ($_SESSION['UserType'] != USERTYPE_STAFF))) {
	header('Location: index.php');
}

$elibplus2_menu_tab = 'myfriends';

$DateRange = empty($DateRange) ? 'accumulated_by_friend' : $DateRange;		// default accumulated_by_friend (no date range filter) 
$lelibplus = new elibrary_plus(0, $_SESSION['UserID'],'type_all',$DateRange);

// update recipient view in notification
$lelibplus->updateRecipientView();


## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START();

$libelibplus_ui = new libelibplus_ui();


// friends notifications
$notification = $lelibplus->getFriendNotification($_SESSION['UserID']);
$myFriendNotificationUI = $libelibplus_ui->getMyFriendNotificationUI($notification);

// friends activities
$activities = $lelibplus->getFriendActivities();
$myFriendActivitiesUI = $libelibplus_ui->getMyFriendActivitiesUI($activities);


// add friends
$addFriendUI = $libelibplus_ui->getMyFriendAddFriendsUI();
$myFriendListUI = $libelibplus_ui->getMyFriendList();


// friends ranking
$isPurchasedeBook = $lelibplus->isPurchasedeBook();
if ($isPurchasedeBook) {	// have eBook
	$ranking_data = $lelibplus->getMyFriendRankingReadingeBook($limit=5);	// default
	$tab = 'viewers';
}
else {
	$myFriendIDs = $lelibplus->getMyFriendIDs();
	if (count($myFriendIDs)) {
		$cond = " AND b.UserID in ('".implode("','",(array)$myFriendIDs)."')";
		
		if (!$plugin['eLib_Lite']){	// have physical book
			$ranking_data = $lelibplus->getMostBorrowUsers($limit=5, $classNameWithBracket=true, $cond, '');	// include staff & student
			$tab = 'borrowers';
		}
		else {	// no eBook & physical book, case should not happen
			$ranking_data = $lelibplus->getMostActiveUsers($limit=5, $bookType='type_all', $userType='', $classNameWithBracket=true, $cond);
			$tab = 'reviewers';
		}
	}
	else {
		$ranking_data = array();
		$tab = 'reviewers';
	}	
}
$myFriendRankingUI = $libelibplus_ui->getMyFriendRankingUI($ranking_data,$tab,$DateRange);


?>
  
<!--CONTENT starts-->
<div class="elib-content">
  <!--2 COLUMNS starts-->
  <div class="container myfriends">
  	<div class="action-right col-sm-4 col-sm-push-8">
    	<!--ADD FRIENDS starts-->
    	<?=$addFriendUI?>
    	<!--ADD FRIENDS ends-->
        
        <!--FRIENDS LIST starts-->
        <?=$myFriendListUI?>
    	<!--FRIENDS LIST ends-->
        
        <!--FRIENDS RANKING starts-->
        <?=$myFriendRankingUI?>
        <!--FRIENDS RANKING ends-->
        
    </div>
  
  	<div class="action-left col-sm-8 col-sm-pull-4">
    	<!--NOTIFICATIONS starts-->
    	<?=$myFriendNotificationUI?>
    	<!--NOTIFICATIONS ends-->        
  		       
    	<!--Friend's Activities starts-->
    	<?=$myFriendActivitiesUI?>  
        <!--Friend's Activities ends-->         
    </div> 
      
	
  </div>
  <!--2 COLUMNS ends-->

</div>
<!--CONTENT ends--> 


<script language="javascript">
$(document).ready(function(){
	$(document).on('click', '#btn_add_friend', function(e) {
	  	e.preventDefault();
	  	if ($('#StudentID').val() == '') {
	  		alert("<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["select_friend"]?>");	
	  	}
	  	else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_my_friends.php',
				data : {
						'action': 'addFriend',
						'StudentID': $('#StudentID').val(),
						'ClassName': $('#ClassName').val()
					},		  
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
					  	$('#StudentID').replaceWith(ajaxReturn.html);
					  	$(".friend-list .table-body ul").prepend(ajaxReturn.html2);
					}
				},
				error: show_ajax_error
			});
	  	}
	});
	
	
	$(document).on('change', '#ClassName', function(){
	  	if ($('#ClassName').val() != '') {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_my_friends.php',
				data : {
						'action': 'changeClass',
						'ClassName': $('#ClassName').val()
					},		  
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
					  	$('#StudentID').replaceWith(ajaxReturn.html);
					}
				},
				error: show_ajax_error
			});
	  	}
	});
	
	
	$(document).on('click', '.accept_request', function(e){
    	e.preventDefault();
		$.ajax({
			dataType: 'json',
			async: true,
			type: 'POST',
			url: 'ajax_my_friends.php',
			data: {	
					'action': 'accept_friend_request',
					'NotificationID': $(this).attr('data-notification_id')
				},
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
					window.location = "myfriends.php";
				}
			},
			error: show_ajax_error
		});        
    });


	$(document).on('click', '.ignore_request', function(e){
    	e.preventDefault();
    	var lineItem = $(this).parent().parent();
		$.ajax({
			dataType: 'json',
			async: true,
			type: 'POST',
			url: 'ajax_my_friends.php',
			data: {	
					'action': 'ignore_friend_request',
					'NotificationID': $(this).attr('data-notification_id'),
					'ClassName': $('#ClassName').val()
				},
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
				  	$('#StudentID').replaceWith(ajaxReturn.html);
				  	updateFriendNotificationCount();
				  	lineItem.remove();
				  	show_no_notification();
				}
			},
			error: show_ajax_error
		});        
    });


	$(document).on('click', '.remove_notification', function(e){
    	e.preventDefault();
    	
    	if (confirm('<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_notification"]?>')){
    		var lineItem = $(this).parent();
    		var is_new = $(this).attr('data-status');
			$.ajax({
				dataType: 'json',
				async: true,
				type: 'POST',
				url: 'ajax_my_friends.php',
				data: {	
						'action': 'remove_notification',
						'NotificationID': $(this).attr('data-notification_id')
					},
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
						if (is_new == 1) {
							updateFriendNotificationCount();
						}
					  	lineItem.remove();
					  	show_no_notification();
					}
				},
				error: show_ajax_error
			});
    	}        
    });
    
    
	$(document).on('click', '.review-likes .btn-type1', function(e){    	
    	e.preventDefault();
		$.ajax({
			dataType: 'json',
			async: true,
			type: 'POST',
			url: 'ajax.php',
			data: {	'action': 'handleLike',
					'review_id': $(this).attr('data-review_id'),
					'book_id': $(this).attr('data-book_id')
				},
			success: handle_like_unlike,
			error: show_ajax_error
		});
        return false;
    });
    
    
	$('#DateRange').on('change', function(e) {
		e.preventDefault();
	  	var action = $('#friend-rank-tab .active a').attr('href');
	  	action = action.replace('#','');
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_my_friends.php',
			data : {
				'action': action,
				'DateRange': $('#DateRange').val()	
			},		  
			success: show_my_friend_ranking_layout,
			error: show_ajax_error
		});
		
	});
    
    
	$('#friend-rank-tab a').on('click', function(e) {
	  	e.preventDefault();
	  	var action = $(this).attr('href');
	  	action = action.replace('#','');
		
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_my_friends.php',
			data : {
				'action': action,
				'DateRange': $('#DateRange').val()	
			},		  
			success: show_my_friend_ranking_layout,
			error: show_ajax_error
		});
	});
    

	$(document).on('keyup', '#AddFriendKeyword', function(e) {
	  	e.preventDefault();
		var key = e.which || e.charCode || e.keyCode;
		var keyword = $('#AddFriendKeyword').val();
		if ((key == 13) && (keyword.length > 0)){
			$.fancybox.open('search_to_add_friends.php?AddFriendKeyword='+encodeURIComponent(keyword),{		
				type: 'iframe',	
				nextEffect: 'fade',	
				prevEffect: 'fade',	
				width: '1000',	
				height: '600',	
				maxWidth: '1200',	
				margin: 20,	
				padding: 0	
			});		
			 
	  	}
	});


	$(document).on('keyup', '#FriendKeyword', function(e) {
	  	e.preventDefault();
		var key = e.which || e.charCode || e.keyCode;
		var keyword = $('#FriendKeyword').val();
		if ((key == 13) && (keyword.length > 0)){
			$.fancybox.open('search_my_friends.php?FriendKeyword='+encodeURIComponent(keyword),{		
				type: 'iframe',	
				nextEffect: 'fade',	
				prevEffect: 'fade',	
				width: '1000',	
				height: '600',	
				maxWidth: '1200',	
				margin: 20,	
				padding: 0	
			});		
			 
	  	}
	});
	
	$(document).on('click', '.btn-remove-request', function(e) {
	  	e.preventDefault();
    	if (confirm('<?=$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_remove_friend_request"] ?>')){
			$.ajax({
				dataType: 'json',
				async: true,
				type: 'POST',
				url: 'ajax_my_friends.php',
				data: {	
						'action': 'remove_friend_request',
						'NotificationID': $(this).attr('data-notification_id')
					},
				success: function(ajaxReturn) {
					if (ajaxReturn != null && ajaxReturn.success){
						window.location = "myfriends.php";						
					}
				},
				error: show_ajax_error
			});
    	}        
	});
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
		
});


function show_my_friend_ranking_layout(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('.friend-rank .tab-content .ranking-list').replaceWith(ajaxReturn.html);
	}
}

function handle_like_unlike(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
        var like_data=ajaxReturn.data.split(',');
        $('body #like_'+like_data[0]+' .number-like').html(like_data[1] + '<span class="icon-like"></span>');
        $('body #like_'+like_data[0]+' .btn-type1').html(like_data[2]);
	}	
}


function show_no_notification() {
  	if ($('.notify-list li').length == 0) {
  		$('.notify-list').html('<li class="nothing"><?=$Lang["libms"]["portal"]["myfriends"]["no_notifications"]?></li>');
  	}
}

function updateFriendNotificationCount() {
  	if ($('#MyFriendNotificationCount').length) {
  		var count = parseInt($('#MyFriendNotificationCount').text());
  		if (count) {
  			count--;
  		}
  		if (count) {
  			$('#MyFriendNotificationCount').html(count);
  		}
  		else {
  			$('#MyFriendNotificationCount').replaceWith('');	
  		}
  	}
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>