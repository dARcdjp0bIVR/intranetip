<?
/*
 * 	Log
 * 
 * 	2016-11-09 [Cameron] 
 * 		- add class "col-sm-2" to show / hide filter option
 *  
 * 	2016-06-14 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

//intranet_auth();
intranet_opendb();

$libelibplus = new libelibplus();
$is_opac_open = $libelibplus->get_portal_setting('open_opac');
if ((!$is_opac_open) || (isset($_SESSION['UserID']) && $_SESSION['UserID'] > 0)) {
	header("location: /home/eLearning/elibplus2/index.php");
	exit;
}

if (isset($_GET['search_type']) && ($_GET['search_type'] == 'category')) {
	$ck_eLib_encryptsalt = md5(microtime());
	session_register_intranet('ck_eLib_encryptsalt',$ck_eLib_encryptsalt);//for random book list, use in $lelibplus->getBookList()
}

$lelibplus = new elibrary_plus();
$book_catalog		= $lelibplus->getBookCatrgories();
$book_languages		= $lelibplus->getBookLanguages();
$book_catalog_tags	= $lelibplus->getBookTags();

$libelibplus = new libelibplus();

$search_result = $libelibplus->get_search_result();	

$books_data = $search_result['books_data'];
$nav_para = $search_result['nav_para'];
//debug_r("count=".count($books_data)."<br>");
if(count($books_data)>1) {
	$navigation_id = $libelibplus->getNavigationID();		// for fancybox prev / next navigation
}
else {
	$navigation_id = '';
}

## Layout start
$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['msg']]);

$libelibplus_ui = new libelibplus_ui();

?>
  
<!--CONTENT starts-->
<div class="elib-content">

	<div class="container options-area">
		<span id="spanShowOption" class="spanShowOption col-sm-2" style="display:none">
			<a href="javascript:showOptionLayer();" class="btn-options btn-togglestate active"><?=$Lang["libms"]["report"]["showoption"]?><span class="caret"></span></a>
		</span> 
		
		<span id="spanHideOption" class="spanHideOption col-sm-2" style="display:none">
			<a href="javascript:hideOptionLayer();" class="btn-options btn-togglestate"><?=$Lang["libms"]["report"]["hideoption"]?><span class="caret"></span></a>
		</span>
	</div>

<form class="form-horizontal" name="advanced_search_form" id="advanced_search_form" method="post" action="opac_search_result.php">

<?include('advanced_search_filter.php');?>

<? 
	if ($search_result['view_type']=='cover') {
?>
  <div class="container search-result" id="search-result">
    <div class="table-header">
    	<?=$eLib["html"]["search_result"]?> <span class="total-no"><span id="total"><?=$nav_para['total']?></span><?=' '. $Lang["libms"]["portal"]["books_found"]?></span>
    	<div class="btn-views">
        	<span class="btn-table-view off glyphicon glyphicon-th-large"></span>  
    		<a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>
        </div>  
    </div>
    <div class="table-body">
    	<?include('book_cover_view.php');?>
    </div>
    <div class="table-footer">
    	<?=$libelibplus_ui->getSearchResultListFooter($nav_para)?>
    </div>
  </div>
<?		
	}			// cover type
	else {		// list type
		$column_sorting = true;
		$start_record_index = $record_per_page * ($page_no-1);
?>
  <div class="container search-result-list" id="search-result">
    <div class="table-header">
    	<?=$eLib["html"]["search_result"]?> <span class="total-no"><span id="total"><?=$nav_para['total']?></span><?=' '. $Lang["libms"]["portal"]["books_found"]?></span>
    	<div class="btn-views">
        	<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a> 
    		<span class="btn-list-view off glyphicon glyphicon-th-list"></span>
        </div>  
    </div>
    <div class="table-body">
		<?include('book_list_view.php');?>		
    </div>
    <div class="table-footer">    
		<?=$libelibplus_ui->getSearchResultListFooter($nav_para)?>
	</div>	
	
  </div>
		
<?	}   
?>
</form>
</div>
<!--CONTENT ends--> 


<script language="javascript">
$(document).ready(function(){

    $('#advanced_search_form').submit(function(e){
    	e.preventDefault();    	
    	hideOptionLayer();
    	ajax_search_book($(this),1);
    });

	$(document).on('change', '#page_no, #record_per_page', function(e) {
		e.preventDefault();
		ajax_search_book($('#advanced_search_form'),1);
	});
	
	$(document).on('click','#PreviousBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())-1);
		$('#PreviousBtn').removeAttr('href');
		ajax_search_book($('#advanced_search_form'),1);
	});

	$(document).on('click','#NextBtn', function(e){
		e.preventDefault();
		$('#page_no').val(parseInt($('#page_no').val())+1);
		$('#NextBtn').removeAttr('href');
		ajax_search_book($('#advanced_search_form'),1);
	});
	
	$(document).on('click', '.btn-views a span', function(e){			// cover / list view icon click
		var target_type = $(e.target).parent().attr('href').replace('#','');
		$('#view_type').val(target_type);
		$('#update_navigation').val('0');
		ajax_search_book($('#advanced_search_form'),0);
		$('#update_navigation').val('1');	// set back to original value		
	});

	hideOptionLayer();
	
	<?=$libelibplus_ui->getBookDetailFancybox()?>
});	

function ajax_search_book(form,update_navigation) {
	$.fancybox.showLoading();
	$.ajax({
		dataType: "json",
		type: "POST",
		url: 'ajax_get_search_result.php',
		data : form.serialize(),		  
		success: update_navigation ? load_search_result_and_navigation : load_search_result,
		error: show_ajax_error
	});	
}

function load_search_result(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$.fancybox.hideLoading();
		if ($('.table-header .btn-views a').attr('href').replace('#','') == 'cover') {
			$('.table-header .btn-views').html('<span class="btn-table-view off glyphicon glyphicon-th-large"></span><a href="#list"><span class="btn-list-view glyphicon glyphicon-th-list"></span></a>');
		}
		else {
			$('.table-header .btn-views').html('<a href="#cover"><span class="btn-table-view glyphicon glyphicon-th-large"></span></a><span class="btn-list-view off glyphicon glyphicon-th-list"></span>');
		}
	  	$('#search-result div.table-body').html(ajaxReturn.html);
	  	$('#total').html(ajaxReturn.total);
		$('#search-result').show();
	}
}

function load_search_result_and_navigation(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$.fancybox.hideLoading();
	  	$('#search-result div.table-body').html(ajaxReturn.html);
	  	$('#total').html(ajaxReturn.total);
	  	$('.table-footer').replaceWith(ajaxReturn.footer_html);
	  	$('#search-result').show();
	}
}

function sortBookList(field, order) {
	$('#sortby-select').val(field);
	$('#order').val(order);
	ajax_search_book($('#advanced_search_form'),1);	
}
	
function hideOptionLayer()
{
	$('.adv-search, #adv-book-cat').attr('style', 'display: none');	
	$('.spanHideOption').attr('style', 'display: none');	
	$('.spanShowOption').attr('style', 'padding-left:0px;');
}

function showOptionLayer()
{
	$('.adv-search, #adv-book-cat').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'padding-left:0px;');
}


function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>