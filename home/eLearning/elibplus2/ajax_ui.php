<?
/*
 * 	Log
 * 
 * 	2016-06-22 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

//intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = true;	// whether to remove new line, carriage return, tab and back slash
$default_book_type = 'type_all';

switch($action) {
	case 'getSubCategory':
		$x = $libelibplus_ui->getBookSubCategoryList($_POST['category']);
		$json['success'] = true;		
		break;
		
	case 'getMoreReviews':
		$book_id = $_POST['book_id'];
		$offset = $_POST['offset'];
		$lelibplus = new elibrary_plus($book_id, $_SESSION['UserID']);
		$total_reviews = $lelibplus->getTotalBookReviews();
		$limit = $elibplus_cfg['book_reviews_limit'] ? $elibplus_cfg['book_reviews_limit'] : 10;
		$x = $libelibplus_ui->getReviewItemUI($book_id,$offset,$limit);
		$json['offset'] = $x['offset'];
		$json['show_more'] = ($x['offset'] >= $total_reviews ? false : true);
		$json['success'] = true;
		$x = $x['html'];
		
		$y = $libelibplus_ui->getBookStatUI($book_id);
		$y = remove_dummy_chars_for_json($y);
		$json['book_stat_html'] = $y;
		break;
		
	case 'get_best_rated_cover':
		$range = $_POST['range'];
		$book_type = $_POST['book_type'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data	= $lelibplus->getBestRateBooks(10,$book_type);
		$x = $libelibplus_ui->getRankingCoverviewBooks($rank_data,$range);
		$json['success'] = true;
		break;
		
	case 'get_best_rated_list':
		$range = $_POST['range'];
		$book_type = $_POST['book_type'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data	= $lelibplus->getBestRateBooks(10,$book_type);
		$x = $libelibplus_ui->getRankingListviewBooks($rank_data);
		$json['success'] = true;
		break;	

	case 'get_most_hit_cover':
		$range = $_POST['range'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data	= $lelibplus->getMostHitBooks(10);
		$x = $libelibplus_ui->getRankingCoverviewBooks($rank_data,$range);
		$json['success'] = true;
		break;
		
	case 'get_most_hit_list':
		$range = $_POST['range'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data	= $lelibplus->getMostHitBooks(10);
		$x = $libelibplus_ui->getRankingListviewBooks($rank_data);
		$json['success'] = true;
		break;	

	case 'get_most_loan_cover':
		$range = $_POST['range'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data	= $lelibplus->getMostLoanBooks(10);
		$x = $libelibplus_ui->getRankingCoverviewBooks($rank_data,$range);
		$json['success'] = true;
		break;
		
	case 'get_most_loan_list':
		$range = $_POST['range'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data	= $lelibplus->getMostLoanBooks(10);
		$x = $libelibplus_ui->getRankingListviewBooks($rank_data);
		$json['success'] = true;
		break;	

	case 'get_most_active_borrowers':
		$range = $_POST['range'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data = $lelibplus->getMostBorrowUsers(5);
		$x = $libelibplus_ui->getRankingMostActiveBorrowers($rank_data);
		$json['success'] = true;
		break;	

	case 'get_most_active_reviewers':
		$range = $_POST['range'];
		$book_type = $_POST['book_type'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_users	= $lelibplus->getMostActiveUsers(10,$book_type,USERTYPE_STUDENT);
		$rank_data 	= array();
		foreach ($rank_users as $i=>$user){
		    list(,$user['reviews'])=$lelibplus->getUserReviews($user['user_id'], false, 0, 1, $book_type);
		    $rank_data[] = $user;
		}
		$x = $libelibplus_ui->getRankingMostActiveReviewers($rank_data);
		$json['success'] = true;
		break;	

	case 'get_most_helpful_review':
		$range = $_POST['range'];
		$book_type = $_POST['book_type'];
		$range = $range ? $range : "accumulated";
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], $default_book_type, $range);
		$rank_data = $lelibplus->getMostUsefulReviews(5,$book_type);
		$x = $libelibplus_ui->getRankingMostHelpfulReview($rank_data);
		$json['success'] = true;
		break;	

	case 'get_most_active_reviewers_review':
		$user_id = $_POST['user_id'];
		$offset = $_POST['offset'] ? $_POST['offset'] : 0;
		$limit = $elibplus_cfg['user_reviews_more'] ? $elibplus_cfg['user_reviews_more'] : 10;
		$lelibplus = new elibrary_plus(0, $user_id);		
		list($total,$review_data)=$lelibplus->getUserReviews($user_id, false, $offset, $limit);
		$nr_of_reviews = count($review_data);
		$json['offset'] = $offset+($nr_of_reviews<$limit?$nr_of_reviews:$limit);
		$json['show_more'] = ($json['offset'] >= $total ? false : true);
		$json['success'] = true;
		$x = $libelibplus_ui->getRankingMostActiveUserReview($review_data);
		break;	
			
}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}
$json['html'] = $x;

echo json_encode($json);

intranet_closedb();
?>