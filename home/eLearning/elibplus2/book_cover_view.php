<?
/*
 * 	Log
 * 
 * 	2016-06-15 [Cameron] create this file
 */

if (!isset($books_data)) {header("HTTP/1.0 403 Forbidden"); die('403 Forbidden');}

?>
      <ul class="result-list">
<? if ($nav_para['total']==0):?>
		<li>
			<?=$libelibplus_ui->getNoRecordFoundBookCover()?>		
		</li>
<? else:
		$j = 0;
		foreach ((array)$books_data as $book):
		
			if ($book['image']) {
				$image_no = '';
			}
			else {
				$image_no = $j%10+1;
				$j++;
			}
			$book['image_no'] = $image_no;
			$book['navigation_id'] = $navigation_id;
?>			
			<li>
				<div class="book-covers">
					<?=($book['book_type'] == 'ebook') ? $libelibplus_ui->geteBookCover($book, true, "div", "", ($_SESSION['UserID']?true:false)) : $libelibplus_ui->getpBookCover($book, true, "div", "", ($_SESSION['UserID']?true:false))?>
				</div>
				<?=$libelibplus_ui->getBookItemsNextToCover($book)?>			
			</li>
<?						
		endforeach;
   endif;
   
?>      
      </ul>