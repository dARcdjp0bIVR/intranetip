<?
/*
 * 	Log
 * 
 * 	2016-11-21 [Cameron] add checking of ClassName in get_statistics_by_class
 * 
 * 	2016-07-20 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
	include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");
	$libelibplus_ui = new libelibplus_ui();
	echo $libelibplus_ui->geteLibCloseUI();
	die();
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = true;	// whether to remove new line, carriage return, tab and back slash

switch($action) {

	case 'get_statistics_all_reviews':
		$searchFromDate = $_POST['searchFromDate'];
		$searchToDate = $_POST['searchToDate'];
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		switch ($_POST['sort']) {
			case 'sort_bt':
				$sort = 'title';
				break;
			case 'sort_a':
				$sort = 'author';
				break;
			case 'sort_nor':
				$sort = 'num_review';
				break;
			case 'sort_ls':
			default:
				$sort = 'last_submitted';
				break;
		}
		$order = $_POST['order'] == 'asc' ? 'asc' : 'desc';
		$order_sql = $sort.' '. $order;
		$offset = $record_per_page * ($page_no-1);
						
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);		
		list($total,$data) = $lelibplus->getAllReviewsRecord($offset, $record_per_page, $order_sql, $searchFromDate, $searchToDate, true);

		if (count($data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($total,$data) = $lelibplus->getAllReviewsRecord($offset, $record_per_page, $order_sql, $searchFromDate, $searchToDate, true);			
		}
						
		$start_record_index = $record_per_page * ($page_no-1);
		$x = $libelibplus_ui->getStatisticsAllReviewsContent($data, $start_record_index);
		$json['success'] = true;
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['cust_nav_style'] = 'cust-pagination2';
		
		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;
		
	case 'get_statistics_all_classes':
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		
		switch ($_POST['sort']) {
			case 'sort_nos':
				$sort = 'num_student';
				break;
			case 'sort_tnobr':
				$sort = 'NumBookRead';
				break;
			case 'sort_anobr':
				$sort = 'read_average';
				break;
			case 'sort_tnor':
				$sort = 'NumBookReview';
				break;
			case 'sort_anor':
				$sort = 'review_average';
				break;
			case 'sort_c':
			default:
				$sort = 'class_name';
				break;
		}
		 
		$order = $_POST['order'] == 'desc' ? 'desc' : 'asc';
		
		$order_sql = $sort.' '. $order;
		$offset = $record_per_page * ($page_no-1);
						
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$FromDate = null;
		$ToDate = null;
		$FromMonth = $_POST['FromMonth'] ? intval($_POST['FromMonth']) : intval(date('m'));
		$ToMonth = $_POST['ToMonth'] ? intval($_POST['ToMonth']) : intval(date('m'));
		$FromYear = $_POST['FromYear'] ? intval($_POST['FromYear']) : intval(date('Y'));
		$ToYear = $_POST['ToYear'] ? intval($_POST['ToYear']) : intval(date('Y'));
		list($total,$data) = $lelibplus->getPublicClassSummary($offset, $record_per_page, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql, true);

		if (count($data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($total,$data) = $lelibplus->getPublicClassSummary($offset, $record_per_page, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql, true);			
		}
						
		$start_record_index = $record_per_page * ($page_no-1);
		$show_column_title = $_POST['show_column_title'] ? true : false;
		
		$x = $libelibplus_ui->getStatisticsAllClassesSummaryContent($data, $start_record_index, $show_column_title);
		$json['success'] = true;
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['cust_nav_style'] = 'cust-pagination2';
		
		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;

	case 'get_statistics_by_class':	// by one class
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$ClassName = $_POST['ClassName'];
		
		if ($ClassName) {
			switch ($_POST['sort']) {
				case 'sort_sn':
					$sort = 'student_name';
					break;
				case 'sort_nobr':
					$sort = 'NumBookRead';
					break;
				case 'sort_hit':
					$sort = 'NumBooks';
					break;
				case 'sort_nor':
					$sort = 'NumBookReview';
					break;
				case 'sort_cn':
				default:
					$sort = 'class_number+0';
					break;
			}
			
			$order = $_POST['order'] == 'desc' ? 'desc' : 'asc';
			$order_sql = $sort.' '. $order;
			$offset = $record_per_page * ($page_no-1);
							
			$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
			$FromDate = null;
			$ToDate = null;
			$FromMonth = $_POST['FromMonth'] ? intval($_POST['FromMonth']) : intval(date('m'));
			$ToMonth = $_POST['ToMonth'] ? intval($_POST['ToMonth']) : intval(date('m'));
			$FromYear = $_POST['FromYear'] ? intval($_POST['FromYear']) : intval(date('Y'));
			$ToYear = $_POST['ToYear'] ? intval($_POST['ToYear']) : intval(date('Y'));
			list($total,$data) = $lelibplus->getPublicStudentSummary($offset, $record_per_page, $ClassName, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql, true);
	
			if (count($data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
				$page_no = 1;
				$offset = 0;
				list($total,$data) = $lelibplus->getPublicStudentSummary($offset, $record_per_page, $ClassName, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql, true);
			}
							
			$start_record_index = $record_per_page * ($page_no-1);
			$show_column_title = $_POST['show_column_title'] ? true : false;
		}
		else {
			$total = 0;
			$data =array();
		}
		$x = $libelibplus_ui->getStatisticsByClassSummaryContent($data, $start_record_index, $show_column_title);
		
		$json['success'] = true;
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['cust_nav_style'] = 'cust-pagination2';

		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;

	case 'get_statistics_by_student':	// by student
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$ClassName = $_POST['ClassName'];
		$student_id = $_POST['student_id'];
		
		switch ($_POST['sort']) {
			case 'sort_a':
				$sort = 'author';
				break;
			case 'sort_p':
				$sort = 'ppercentage';
				break;
			case 'sort_rt':
				$sort = 'read_times';
				break;
			case 'sort_lr':
				$sort = 'last_accessed';
				break;
			case 'sort_bt':
			default:
				$sort = 'title';
				break;
		}
		 
		$order = $_POST['order'] == 'desc' ? 'desc' : 'asc';
		$order_sql = $sort.' '. $order;
		$offset = $record_per_page * ($page_no-1);
						
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);
		$FromDate = null;
		$ToDate = null;
		$FromMonth = $_POST['FromMonth'] ? intval($_POST['FromMonth']) : intval(date('m'));
		$ToMonth = $_POST['ToMonth'] ? intval($_POST['ToMonth']) : intval(date('m'));
		$FromYear = $_POST['FromYear'] ? intval($_POST['FromYear']) : intval(date('Y'));
		$ToYear = $_POST['ToYear'] ? intval($_POST['ToYear']) : intval(date('Y'));

		list($total,$data) = $lelibplus->getPublicStudentParticularSummary($offset, $record_per_page, $student_id, $ClassName, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql, true);

		if (count($data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($total,$data) = $lelibplus->getPublicStudentParticularSummary($offset, $record_per_page, $student_id, $ClassName, $FromDate,$ToDate, $FromMonth, $ToMonth,$FromYear,$ToYear, $order_sql, true);
		}
						
		$start_record_index = $record_per_page * ($page_no-1);
		$show_column_title = $_POST['show_column_title'] ? true : false;
		$x = $libelibplus_ui->getStatisticsByStudentContent($data, $start_record_index, $show_column_title);
		$json['success'] = true;
		
		$nav_para['show_total'] = true;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['cust_nav_style'] = 'cust-pagination2';

		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;

	case 'get_statistics_review_by_student':	// review by student
		$page_no = $_POST['page_no'] ? $_POST['page_no'] : 1;
		$record_per_page = $_POST['record_per_page'] ? $_POST['record_per_page'] : 10;
		$ClassName = $_POST['ClassName'];
		$student_id = $_POST['student_id'];
		$offset = $record_per_page * ($page_no-1);
						
		$lelibplus = new elibrary_plus(0, $_SESSION['UserID'], 'type_all', '');
		$FromDate = null;
		$ToDate = null;
		$FromMonth = $_POST['FromMonth'] ? intval($_POST['FromMonth']) : intval(date('m'));
		$ToMonth = $_POST['ToMonth'] ? intval($_POST['ToMonth']) : intval(date('m'));
		$FromYear = $_POST['FromYear'] ? intval($_POST['FromYear']) : intval(date('Y'));
		$ToYear = $_POST['ToYear'] ? intval($_POST['ToYear']) : intval(date('Y'));

		list($total, $data)=$lelibplus->getUserReviews2($student_id, $ClassName, $offset, $record_per_page, $FromMonth, $ToMonth,$FromYear,$ToYear);

		if (count($data) == 0 && $page_no > 1) {	// case: page_no > 1, change record_per_page to a larger one
			$page_no = 1;
			$offset = 0;
			list($total, $data)=$lelibplus->getUserReviews2($student_id, $ClassName, $offset, $record_per_page, $FromMonth, $ToMonth,$FromYear,$ToYear);
		}
						
		$start_record_index = $record_per_page * ($page_no-1);
		
		$x = $libelibplus_ui->getReviewByUserUI($data, $total, $start_record_index);
		$json['success'] = true;
		
		$nav_para['show_total'] = false;
		$nav_para['total'] = $total;
		$nav_para['current_page'] = $page_no;
		$nav_para['record_per_page'] = $record_per_page;
		$nav_para['cust_nav_style'] = 'cust-pagination2';

		$y = $libelibplus_ui->getSearchResultListFooter($nav_para);
		$y = remove_dummy_chars_for_json($y);
		$json['footer'] = $y;
		break;

}

if ($remove_dummy_chars) {
	$x = remove_dummy_chars_for_json($x);
}
$json['html'] = $x;

echo json_encode($json);

intranet_closedb();
?>