<?php
/*
 * 	@Purpose:	handle ranking list of portal page:
 * 				most active reviewers (include ebook & physical books)
 * 				most helpful reviews (include ebook & physical books)
 * 	
 * 	Log
 * 
 * 	2016-05-27 [Cameron] create this file
 * 	
 */
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

//intranet_auth();
intranet_opendb();

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = FALSE;
$x = '';
$rankingInfo = null;
$type 			= $_POST['type'];
$ranking_type 	= $_POST['ranking_type'];
$ranking_range 	= $_POST['ranking_range'];

if ($type == 'ebook') {
	if ($ranking_type == 'most_helpful_review') {
		$rankingInfo = $libelibplus_ui->getMostuserfulReviewsTable($ranking_range, $type);
	}
	else {		// most active reviewer
		$rankingInfo = $libelibplus_ui->geteBookRankingTable($ranking_range, $ranking_type);
	}
}
else {
	if ($ranking_type == 'most_helpful_review') {
		$rankingInfo = $libelibplus_ui->getMostuserfulReviewsTable($ranking_range, $type);
	}
	else {		// most active reviewer
		$rankingInfo = $libelibplus_ui->getpBookRankingTable($ranking_range, $ranking_type);
	}
}

if ($rankingInfo) {
	$x = $rankingInfo;
	$json['success'] = TRUE;
}

$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;

echo json_encode($json);

intranet_closedb(); 
?>