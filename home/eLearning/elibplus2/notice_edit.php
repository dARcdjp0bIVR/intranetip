<?
/*
 * 	Log
 * 
 *  2018-07-16 [Cameron] cast notice_id to integer to avoid sql injection [case #G142774]
 *   
 * 	2016-12-22 [Cameron] 
 * 		- add $home_header_EmulateIE9 to support CKEditor in IE [case #T110678]
 * 		- adjust attachment file input position for IE 
 * 
 * 	2016-05-31 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

if (!$permission['admin']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libelibplus = new libelibplus();
$show_news = $libelibplus->get_portal_setting('show_news');

$libelibplus_ui = new libelibplus_ui();
$lelibplus = new elibrary_plus(0, $_SESSION['UserID']);

$show_image = false;
$Defined = 0;		// for checking if datepicker script has been loaded or not

$type = $_GET['type'];
$notice_id = (int) $_GET['notice_id'];

if ($type=="rules"){
	$offset = 0;
	$amount = 1;
    $notice = current($lelibplus->getPortalNotices('rules', false, $offset, $amount));
    $notice_header = $Lang["libms"]["portal"]["edit_rules"];
}else if ($notice_id){    
    $notice = current($lelibplus->getPortalNotices('news', $notice_id));
    $notice_header = $Lang["libms"]["portal"]["edit_news"];
}else{
	$action = 'add';
    $notice=array();
    $notice_header = $Lang["libms"]["portal"]["add_news"];
}

if ($notice) {	// array not empty
	if ($notice['attachment_name']) {
		$file_extention = getFileExtention($notice['attachment_name'],true);
		if (($file_extention == 'jpg') || ($file_extention == 'jpeg') || ($file_extention == 'png') || ($file_extention == 'gif')) {
			$show_image = true;
		}
	}
}

#### HTML Editor START ####
include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$objHtmlEditor = new FCKeditor ( 'content' , "100%", "250", "", "", intranet_undo_htmlspecialchars($notice['content']));
$htmlEditor = $objHtmlEditor->CreateHtml();
$home_header_EmulateIE9 = true; // FCK Editor does not support IE Edge, but it does support IE9 & IE10
#### HTML Editor END ####

$linterface = new interface_html("elibplus2.html");
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$_GET['msg']]);
$home_header_EmulateIE9 = false; 	// resest

?>


<!--CONTENT starts-->
<div class="elib-content">
	<?=$libelibplus_ui->getActionResultUI()?>
  <div class="container news-edit">
  	<div><a class="btn-back" href="#"><span class="caret-left-red"></span> <?=$Lang['Btn']['Back']?></a></div>
    <div class="table-header"><?=$notice_header?></div>
  	<div class="table-body">
    	<form class="form-horizontal">
<?php if($type=='news'): ?>    	
    	<div class="form-group">
			<label for="input-title" class="col-sm-2 control-label"><?=$eLib_plus["html"]["title"]?></label>
			<div class="col-sm-10"><input type="text" class="form-control" id="input-title" name="title" placeholder="" value="<?=escape_double_quotes($notice['title'])?>"></div>
    	</div>
<?php endif;?>
        <div class="form-group required">
          <label for="input-content" class="col-sm-2 control-label"><?=$eLib_plus["html"]["content"]?></label>
          <div class="col-sm-10" id="ckeditior_content">
<!--            <textarea class="form-control" rows="10" id="input-content" name="content" placeholder=""><?=escape_double_quotes($notice['content'])?></textarea>-->
		<?            
			echo $htmlEditor;
		?>
          </div>
        </div>
        <div class="form-group">
          <label for="input-attachment" class="col-sm-2 control-label"><?=$eLib_plus["html"]["attachment"]?></label>
          <div class="col-sm-10" id="current_attachment" <?=$notice['attachment']?'':'style="display:none"'?>>
          	<a href="<?=str_replace($notice['attachment_name'], urlencode($notice['attachment_name']), $notice['attachment'])?>" target="_blank" class="attachment"><?=$notice['attachment_name']?></a> <a href="#" class="btn-remove"><span class="glyphicon glyphicon-trash"></span></a>
          	<img id="img-attachment" src="<?=$notice['attachment']?>" <?=$show_image?'':'style="display:none"'?>>
		  </div>
		  <div class="col-sm-10" id="add_attachment" <?=$notice['attachment']?'style="display:none"':''?>>
            <a href="#" class="btn-add"><span class="glyphicon glyphicon-plus"></span> <?=$Lang['General']['UploadFiles']?></a>
          </div>
        </div>
<?php if($type=='news'): ?>        
        <div class="form-group">
          <label for="input-priority" class="col-sm-2 control-label"><?=$Lang["libms"]["portal"]["pinned"] ?>:</label>
          <div class="col-sm-10">
            <label><input type="checkbox" name="priority" <?=$notice['priority']?"checked":""?> value="1"><span class="custom-check"></span></label>
          </div>
        </div>
<?php endif;?>        
        <div class="form-group <?=($type=='news')?'required':''?>">
          <label for="input-period" class="col-sm-2 control-label"><?=$eLib["html"]["Period"]?></label>
          <div class="col-sm-10">
            <label><?=$Lang['General']['From']?> &nbsp;</label>
            <label>
 					<div class="form-group date-pick">
                		<div class='input-group date' id='datetimepicker1'>
                		<?=$libelibplus_ui->GET_DATE_PICKER('date_start',(strtotime($notice["date_start"]) == "943891200"  || $notice["date_start"] == "" ? "" : date("Y-m-d",strtotime($notice['date_start']))),$OtherMember="",$DateFormat="yyyy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="date_start",$SkipIncludeJS=0, $CanEmptyField=1, $Disable=false, $cssClass="form-control".($type=='news'?' required':''), $calendarIcon=true)?>
                		</div>
            		</div><!--implement date picker-->
            </label>
            <label>            		
                    &nbsp; <?=$Lang['General']['To']?>  &nbsp;
            </label>
            <label>
 					<div class="form-group date-pick">
                		<div class='input-group date' id='datetimepicker2'>
                		<?=$libelibplus_ui->GET_DATE_PICKER('date_end',(strtotime($notice["date_end"]) == "943891200"  || $notice["date_end"] == "" ? "" : date("Y-m-d",strtotime($notice['date_end']))),$OtherMember="",$DateFormat="yyyy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="date_end",$SkipIncludeJS=1, $CanEmptyField=1, $Disable=false, $cssClass="form-control".($type=='news'?' required':''), $calendarIcon=true)?>
                		</div>
            		</div><!--implement date picker-->
            </label>
          </div>
        </div>

        <div class="form-group">
          <label class="col-sm-12 control-label text-info"><?=$Lang["libms"]["portal"]["require_field"]?></label>
        </div>
        
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn-type1"><?=$eLib["html"]["submit"]?></button>	
            <button class="btn-type1 btn-cancel"><?=$eLib["html"]["cancel"]?></button>
         <?php if ($notice_id): ?>
            <button class="btn-type1 btn-delete"><?=$eLib["html"]["Delete"]?></button>
         <?php endif;?>
          </div>
        </div>
        
        <input type="hidden" name="id" value="<?=$notice['id']?>"/>
		<input type="hidden" name="type" value="<?=$type?>"/>
		<input type="hidden" id="file_temp_path" name="file_temp_path" value=""/>
		<input type="hidden" id="delete_attachment" name="delete_attachment" value="0"/>
        
      </form>
      
	  <form style="display:none;float:left;margin-top:-65px; position:relative;top:-156px;left:300px;" class="attach_form" target="upload_frame" action="ajax_file_handling.php?action=uploadPortalNoticeAttachment&callback=window.parent.portal_news_attachmentSet" method="post" enctype="multipart/form-data">
		<input class="upload_button" id="attachment" name="attachment" type="file"/>
	  </form>
      <iframe name="upload_frame" style="display:none;"></iframe>
        
  	</div>  
  </div>
</div>
<!--CONTENT ends--> 
 
<script language="javascript">
$(document).ready(function(){

	$('.btn-back').on('click',function(e){
		load_index_page('?notice_type=<?=$type?>');
	});
	
	$('.form-horizontal').submit(function(){
	<? if($type=='news'):?> 
		if($('#date_start').val() == '') {
		    alert('<?=$Lang["libms"]["portal"]["news"]["start_date_empty"]?>');		
			return false;
		}
		
		if($('#date_end').val() == '') {
		    alert('<?=$Lang["libms"]["portal"]["news"]["end_date_empty"]?>');		
			return false;
		}
	<? endif;?>	
		if($('#date_start').val() > $('#date_end').val()){
		    alert('<?=$eLib["html"]["Incorrect_Date"]?>');		
			return false;
		}
		
		var contentVal = FCKeditorAPI.GetInstance('content').GetXHTML(true);
		contentVal = $.trim(contentVal.replace(/&nbsp;/gi,''));
		contentVal = $.trim(contentVal.replace(/<br \/>/gi,''));
		contentVal = $.trim(contentVal.replace(/<br type=\"_moz\" \/>/gi,''));
		if (contentVal == '') {
		    alert('<?=$eLib_plus["html"]["pleaseentercontent"]?>');
		    return false;
		}
		
		$('.form-horizontal').attr('method','post');
		$('.form-horizontal').attr('action','notice_edit_update.php');
		return true;
	});
	
	// cancel and return to index.php
    $('.form-horizontal .btn-cancel').click(function(){
		var temp_file_path = $('#file_temp_path').val();
		if (temp_file_path != ''){
			$.ajax({
				dataType: "json",
				type: "POST",
				url: 'ajax_file_handling.php',
				data : {
					'action':'removePortalNoticeAttachment',
					'temp_file_path':temp_file_path
				},		  
				success: function(ajaxReturn){
					if (ajaxReturn != null && ajaxReturn.success) {
						load_index_page('?notice_type=<?=$type?>');
					}
					else {
						alert('<?=$Lang["libms"]["portal"]["warning"]["fail_del_tmp_attachment"]?>');
					}
				},
				error: show_ajax_error
			});
		}
		else{
			load_index_page('?notice_type=<?=$type?>');
		}
		return false;
    });
    
    // delete news
    $('.form-horizontal .btn-delete').click(function(){
		if (!confirm('<?=$eLib["html"]["confirm_remove_msg"]?>')) return false;
		$.ajax({
			dataType: "json",
			type: "POST",
			url: 'ajax_file_handling.php',
			data : {
				'action':'removePortalNotice',
				'id':'<?=$notice['id']?>',
				'type':'<?=$type?>'
			},		  
			success: function(ajaxReturn) {
				var para = (ajaxReturn != null && ajaxReturn.success) ? 'DeleteSuccess' : 'DeleteUnsuccess';
					para = '?notice_type=<?=$type?>&msg=' + para;
				load_index_page(para);
			},
			error: show_ajax_error
		});
    });
    
    // remove attachment before final submit
    $('#current_attachment .btn-remove').click(function(){
		$('#current_attachment').hide();
		if ($('#img-attachment').length) {
			$('#img-attachment').remove();	
		}		
		$('#add_attachment').show();
		$('#attachment').val('');
		$('#delete_attachment').val(1);
		return false;
    });
	
    $('#add_attachment a').click(function(){		
		if (navigator.userAgent.match(/msie/i)){
		    $('.attach_form').show();
		}
		else{
		    $('#attachment').click();
		}
		return false;
    });
	
	// upload attachment
    $('#attachment').change(function(){
		if ($('#attachment').val() != '') {
			if (navigator.userAgent.match(/msie/i)){
			    $('.attach_form').hide();
			}
			$(this).parent().submit();
		}
    });
	
});	

function portal_news_attachmentSet(temp_file_path, file_name, file_extention){
    $('#file_temp_path').val(temp_file_path);
    $('#current_attachment').show().find('.attachment').attr('href',temp_file_path).html(file_name);
    $('#current_attachment .btn-remove').show();
    if (file_extention == 1) {
    	if ($('#img-attachment').length) {
    		$('#img-attachment').attr('src',temp_file_path);	
    	}
    	else {
    		$('#current_attachment .btn-remove').after('<img id="img-attachment" src="'+ temp_file_path +'">');
    	}
    	$('#img-attachment').fadeIn();
    }
    $('#add_attachment').hide();
    $('#delete_attachment').val(0);
}

function load_index_page(msg) {
	window.location="index.php" + msg;
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}


<? if ($_GET['msg']): ?>
Get_Return_Message("<?=$Lang['General']['ReturnMessage'][$_GET['msg']]?>");
<? endif;?>

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>