<?php
/*
 * 	Log
 * 
 * 	2016-05-24 [Cameron] create this file
 * 	
 */
include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

//intranet_auth();
intranet_opendb();

//$permission = elibrary_plus::getUserPermission($UserID);

//if (!$permission['elibplus_opened']) 	die($eLib_plus["html"]["closing"]);

if ($_SESSION['UserID']) {
	$permission = elibrary_plus::getUserPermission($UserID);
	if (!$permission['elibplus_opened'] && !$plugin['eLib_Lite']) {
		$libelibplus_ui = new libelibplus_ui();
		echo $libelibplus_ui->geteLibCloseUI();
		die();
	}
}
else {
	$libelibplus = new libelibplus();
	$is_opac_open = $libelibplus->get_portal_setting('open_opac');
	if (!$is_opac_open) {
		intranet_closedb();
		header("location: /home/eLearning/elibplus2/index.php");
		exit;
	}
}

$libelibplus_ui = new libelibplus_ui();

$json['success'] = FALSE;
$json['no_books'] = 0;
$x = '';
$slideInfo = null;

switch($_POST['type']) {
	case 'ebook_recommend_book':
		$slideInfo = $libelibplus_ui->geteBookSlides('recommend');
		break;
	case 'ebook_most_hit':
		$slideInfo = $libelibplus_ui->geteBookSlides('most_hit');
		break;
	case 'ebook_new':
		$slideInfo = $libelibplus_ui->geteBookSlides('new');
		break;
	case 'ebook_best_rated':
		$slideInfo = $libelibplus_ui->geteBookSlides('best_rated');
		break;
	case 'pbook_recommend_book':
		$slideInfo = $libelibplus_ui->getpbookSlides('recommend');
		break;
	case 'pbook_most_loan':
		$slideInfo = $libelibplus_ui->getpbookSlides('most_loan');
		break;
	case 'pbook_new':
		$slideInfo = $libelibplus_ui->getpbookSlides('new');
		break;
	case 'pbook_best_rated':
		$slideInfo = $libelibplus_ui->getpbookSlides('best_rated');
		break;		
}

if ($slideInfo) {
	$json['no_books'] = $slideInfo['no_books'];
	$x = $slideInfo['html'];
	$json['success'] = TRUE;
}

$x = remove_dummy_chars_for_json($x);
$json['html'] = $x;
echo json_encode($json);

intranet_closedb(); 
?>