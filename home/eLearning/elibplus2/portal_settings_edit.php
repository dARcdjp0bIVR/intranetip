<?
/*
 * 	Log
 * 
 * 	2017-01-17 [Cameron]
 * 		- allow user who has permission to access portal setting if $plugin['eLib_Lite'] == true
 * 
 * 	2017-01-16 [Cameron]
 * 		- add $isPurchasedeBook, don't show eBook related items in portal setting if client do not purchase eBook
 * 
 * 	2016-11-11 [Cameron]
 * 		- change access control to $permission['portal_setting'] [case #Z108442]
 * 
 * 	2016-07-06 [Cameron] create this file
 */

include_once("config.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

intranet_auth();
intranet_opendb();

$permission = elibrary_plus::getUserPermission($UserID);

//if (!$permission['portal_setting'] || ($permission['portal_setting'] && $plugin['eLib_Lite'])) {
if (!$permission['portal_setting']) {	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$elibplus2_menu_tab = '';

$linterface = new interface_html("elibplus2.html");

$linterface->LAYOUT_START();

$libelibplus = new libelibplus();
$internal_settings = $libelibplus->get_portal_setting_by_page_type('internal');
$internal_settings = BuildMultiKeyAssoc($internal_settings,'name');
$internal_settings['page_type'] = 'internal';

$opac_settings = $libelibplus->get_portal_setting_by_page_type('opac');
$opac_settings = BuildMultiKeyAssoc($opac_settings,'name');
$opac_settings['page_type'] = 'opac';

$lelibplus = new elibrary_plus();
$isPurchasedeBook = $lelibplus->isPurchasedeBook();

$libelibplus_ui = new libelibplus_ui();

$page_type = ($_GET['page_type']) ? $_GET['page_type'] : 'internal';

?>

<!--CONTENT starts-->
<div class="elib-content">
  <div class="system-settings">
<?=$libelibplus_ui->getActionResultUI()?>  
    <div class="container">
      
      <div class="general-tabs">
      	<div><label class="back"><a class="btn-back" href="#"><span class="caret-left-red"></span> <?=$Lang['Btn']['Back']?></a></label></div>
        <div class="setting"><?=$Lang["libms"]["portal"]["settings"]?></div>
        	<ul id="settings_tab" class="nav nav-tabs pull-right">
    			<li class="<?=($page_type=='opac'?'':'active')?>"><a href="#internal" data-toggle="tab"><?=$Lang["libms"]["portal"]["settings_tab_internal"]?></a></li>
    			<li class="<?=($page_type=='opac'?'active':'')?>"><a href="#opac" data-toggle="tab"><?=$Lang["libms"]["portal"]["settings_tab_opac"]?></a></li>
        	</ul>
        
        <div class="tab-content" id="settings_content">
        	<div class="tab-pane<?=($page_type=='opac'?'':' active')?>" id="internal"> 
				<?=$libelibplus_ui->getPortalSettingView($internal_settings,$isPurchasedeBook)?>
				<?=$libelibplus_ui->getPortalSettingForm($internal_settings,$isPurchasedeBook)?>
			</div>
        	<div class="tab-pane<?=($page_type=='opac'?' active':'')?>" id="opac"> 
				<?=$libelibplus_ui->getPortalSettingView($opac_settings,$isPurchasedeBook)?>
				<?=$libelibplus_ui->getPortalSettingForm($opac_settings,$isPurchasedeBook)?>
			</div>
			
		</div>        
      </div>
	</div>
  </div>
</div>
<!--CONTENT ends--> 
 
<script language="javascript">
$(document).ready(function(){

	$('.btn-back').on('click',function(e){
		load_index_page();
	});

	$('#settings_tab a').on('click', function(e) {
	  	e.preventDefault();
	  	var page_type = $(this).attr('href');
	  	page_type = page_type.replace('#','');
	  	$('#page_type').val(page_type);
	  	view_mode();
	});

	$('.btn-edit').on('click',function(e){
		e.preventDefault();
		edit_mode();
	});

	$('.btn-cancel').on('click',function(e){
		e.preventDefault();
		view_mode();
	});
	
	$('.form-horizontal').submit(function(){
		var page_type = $('#settings_tab li.active a').attr('href');
		page_type = page_type.replace('#','');
		$('#page_type').val(page_type);
		$('.form-horizontal').attr('method','post');
		$('.form-horizontal').attr('action','portal_settings_edit_update.php');
		return true;
	});
	
});	

function edit_mode() {
	$('.view-form').hide();
	$('.tab-content').css('padding','0px');
	$('.edit-form').show();
}

function view_mode() {
	$('.edit-form').hide();
	$('.tab-content').css('padding','15px');
	$('.view-form').show();		
}

function load_index_page() {
	window.location="index.php";
}

function show_ajax_error() {
	alert('<?=$Lang["libms"]["general"]["ajaxError"]?>');
}

<? if ($_GET['msg']): ?>
Get_Return_Message("<?=$Lang['General']['ReturnMessage'][$_GET['msg']]?>");
<? endif;?>

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>