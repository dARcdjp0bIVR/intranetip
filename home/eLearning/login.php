<?php
/*
 * Editing by: 
 * 
 * Modification Log:
 * 2018-09-06 (Jason) [ip.2.5.9.11.1]
 *	- call the SESSION variable to retrieve the UserID to avoid student to act as a teacher if student captures the URL of teacher via
 * 2017-08-21 (Paul) [ip.2.5.8.10.1]
 * 	- Add lslp / ls flags to session to avoid the problem on including intranet settings.php directly in eClass files
 * 2017-02-15 Paul [ip.2.5.8.3.1]
 * 	- Add the sys_custom flag for project PolyU-NCS to session
 * 2016-09-05 Pun [ip.2.5.7.10.1]
 *  - Change retrieveSessionKey load from libdb getSessionKeyLastUpdatedFromUserId()
 * 2016-06-01 (Paul) [ip.2.5.7.7.1]
 * 		- add new parameter url to pass to check2.php for redirecting classroom to the desired function after login
 * 2015-07-27 (Jason) [ip.2.5.6.7.1]
 * 		- support php 5.4+ : session_register() is removed
 * 2012-08-21 (Siuwan)
 *		- add $isParent and $childID for Parent eLearning - Children's eClass
 * 2012-07-17 (Jason)
 * 		- fix the problem of checking roomType of course 
 * 2012-05-04 (Yuen)
 * 		- do not always reset session ID (as sometimes same user account is being used in training)
 * 2012-03-27 (Thomas)
 * 		- remove the '/' at the end of the $eclass_url_root and $eclass_httppath_first
 * 		  as powerlesson needs to retrieve the action frim url path
 * 2011-11-17 (Jason)
 * 		- add $classLogin, $session_id for start powerlesson in a quick way for student 
 */
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libuser.php");
intranet_auth();
intranet_opendb();
if($_SERVER['SERVER_PORT']=='443'){
	$eclass_url_root = str_replace("https","http",$eclass_url_root);
	$eclass_url_root = str_replace("http","https",$eclass_url_root);
}
# session_register("eClassMode");
# $eClassMode = "I";
# setcookie("eClassMode", "I", 0, "", $eclass_httppath, 0);
$u_id = $_SESSION['UserID'];
$li = new libuser($u_id);

# check if session key from DB is new (i.e. updated within $session_expiry_time mins)
$results = $li->getSessionKeyLastUpdatedFromUserId($u_id);
$session_key = trim($results["SessionKey"]);
if ($session_key=="")
{
	$session_key = $li->generateSessionKey();
	$li->updateSession($session_key);
}
$li->updateSessionLastUpdatedByUserId($u_id);

$email = $li->UserEmail;
$passwd = $li->UserPassword;

/************************* update by fai ****************************/
/* checking for reading / special room , path should be redirect to eclass30 file path
/* if normal class room, path should be redirect to eclass40 file path
/*******************/
$cfg_IsNormalClassRoom	= 0;  
$cfg_IsReadyRoom		= 2;  //may not use this variable or value, for reference only
$roomType = array();
$sql = "select 
			c.RoomType as 'RoomType'
		from ".$eclass_db.".course as c 
			inner join ".$eclass_db.".user_course as uc on c.course_id = uc.course_id 
		where 
	uc.user_course_id = '".IntegerSafe($uc_id)."'";
$roomTypeArray = $li->returnVector($sql);

if(sizeof($roomTypeArray) != 1 )
{	//suppose roomtype should be one value only

	echo "Error in Course ID. Cannot not enter classroom<Br/>";
	exit();
}
//$roomType = $roomTypeArray[0]["RoomType"];
$roomType = $roomTypeArray[0];

## remove the '/' at the end of the $eclass_url_root as powerlesson needs to retrieve the action frim url path
if($eclass_url_root!="" && $eclass_url_root{strlen($eclass_url_root)-1} == "/"){
	$eclass_url_root = substr($eclass_url_root, 0, strlen($eclass_url_root)-1);
}

if($eclass_httppath_first!="" && $eclass_httppath_first{strlen($eclass_httppath_first)-1} == "/"){
	$eclass_httppath_first = substr($eclass_httppath_first, 0, strlen($eclass_httppath_first)-1);
}

//DEFAULT THE URL IS eclass40
$redirect_url_root = $eclass_url_root;
$redirect_httppath_first = $eclass_httppath_first;

//if($roomType != $cfg_IsNormalClassRoom)
if($roomType != $cfg_IsNormalClassRoom && $roomType !=7 && $roomType != 6 && 
	$roomType != 13) # not normal classroom and not itextbook , 6 --> writing 2.0 classroom, 13 - Wong Wah San eLearning Project
{
	//IF THE ROOM TYPE IS NOT A NORMAL CLASSROOM
	//OVERRIDE $redirect_url_root AND   $redirect_httppath_first , SET IT TO ECLASS30 

	$redirect_url_root = $eclass30_url_root;
//	$redirect_httppath_first = "";  have not find the suppose value, find it later
}

## For PowerLesson use only
$session_id = (isset($session_id) && $session_id != '') ? $session_id : '';
$classLogin = (isset($classLogin) && $classLogin != '') ? $classLogin : '';

//$ck_lesson_session_id = $session_id;
//session_register("ck_lesson_session_id");
session_register_intranet('ck_lesson_session_id', $session_id);

//$ck_lesson_classlogin = $classLogin;
//session_register("ck_lesson_classlogin");
session_register_intranet('ck_lesson_classlogin', $classLogin);
## End of PowerLesson use

## Start of Project PolyU NCS
if(isset($sys_custom['project']['NCS'])){
	$sys_custom['project']['NCS'] = ($sys_custom['project']['NCS'])?$sys_custom['project']['NCS']:"";
	session_register_intranet('ck_project_ncs', $sys_custom['project']['NCS']);
}
## End of Project PolyU NCS

## For plugins
### LSLP
session_register_intranet('ck_plugin_lslp', $plugin['lslp']);
session_register_intranet('ck_plugin_lslp_dev', $plugin['lslp_dev']);
session_register_intranet('ck_plugin_lslp_license', $lslp_license);
session_register_intranet('ck_plugin_lslp_license_period', $lslp_license_period);
session_register_intranet('ck_plugin_lslp_httppath', $lslp_httppath);
session_register_intranet('ck_plugin_lslp_school_code', $config_school_code);

### LS
session_register_intranet('ck_plugin_ls_test', $plugin['ls_test']);

//error_log("r [".$r_var."]    eclass_httppath_first [".$eclass_httppath_first."] redirect_url_root [".$redirect_url_root."]<----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
//$r_var  --> store extra request variable

## Parent go to Children Classroom
$isParent = (isset($isParent) && $isParent != '') ? $isParent : '';
$childID = (isset($childID) && $childID != '') ? $childID : '';

if ($eclass_httppath_first == "")
{
    $url = "$redirect_url_root/check2.php?user_course_id=".IntegerSafe($uc_id)."&eclasskey=".$session_key."&jumpback=$jumpback&r_var=".$r_var."&isParent=".$isParent."&childID=".$childID."&url=".$redirectPath;
    #$url = "$eclass_url_root/check.php?user_course_id=$uc_id&email=$email&password=$passwd";
//	error_log("1111 -->".$url."    <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
    header("Location: $eclass_url_root/mode.php?url=".urlencode($url));
    
}
else
{
	
    #$url = "$eclass_httppath_first/check.php?user_course_id=$uc_id&email=$email&password=$passwd";
    $url = "$redirect_httppath_first/check2.php?user_course_id=".IntegerSafe($uc_id)."&eclasskey=".$session_key."&jumpback=$jumpback&r_var=".$r_var."&isParent=".$isParent."&childID=".$childID."&url=".$redirectPath;
//	error_log($url."    22 <----".date("Y-m-d H:i:s")."\n", 3, "/tmp/aaa.txt");
    header("Location: $eclass_httppath_first/mode.php?url=".urlencode($url));
}

intranet_closedb();
?>