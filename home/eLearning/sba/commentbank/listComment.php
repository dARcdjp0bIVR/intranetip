<?php
/*
 * Editing By
 *
 * Modification Log:
 * 2016-07-11 (Catherine)
 * 		- fixed CommentCategory $PAGE_NAVIGATION[] URL
 *
 */

############################## Access checking for Commentbank
$objIES = new libies();
$ldb = new libdb();

if(!$objIES->CHECK_ACCESS("IES-MGMT-COMMENTBANK-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}


############################## Handling page settings
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$CurrentPage = "CommentBank";

$title = $Lang['IES']['CommentBank'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();


############################## Handling task

$categoryID = $_GET["categoryID"];

$html_scheme_list = "";
//Get Comment array
$result = $objIES->getCommentBank($categoryID);

$num_count = 1;
$sizeOfResult = count($result);
for($i=0; $i < $sizeOfResult; $i++)
{
	$displayComment = empty($result[$i]['Comment'])?" -- ":$result[$i]['Comment'];
	$displayCommentEN = empty($result[$i]['CommentEN'])?" -- ":$result[$i]['CommentEN'];
	
	$html_scheme_list .= "<tr><td>$num_count</td>";
	$num_count++;
	$html_scheme_list .= "<td id=\"comment_{$result[$i]['CommentID']}\"><b>".$Lang["IES"]["Chinese"]."</b><br/>".nl2br($displayComment)."<br/><br/><b>".$Lang["IES"]["English"]."</b><br/>".nl2br($displayCommentEN)."<input type=\"hidden\" id=\"h_comment_{$result[$i]['CommentID']}\" VALUE=\"{$result[$i]['Comment']}\"><input type=\"hidden\" id=\"h_commenten_{$result[$i]['CommentID']}\" VALUE=\"{$result[$i]['CommentEN']}\"></td>";
	$html_scheme_list .= "<td><div class=\"table_row_tool\"><a href=\"javascript:EditComment('{$result[$i][CommentID]}')\" class=\"edit_dim\" title=\"Edit\"></a>" .
			"<a href=\"javascript:RemoveComment('{$result[$i][CommentID]}')\" class=\"delete_dim\" title=\"Delete\"></a>" .
			"<input id=\"button_add_{$result[$i]['CommentID']}\" style=\"display:none;\" type=\"button\" onClick=\"CheckForm('edit', {$result[$i][CommentID]})\" class=\"formsmallbutton \" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['Save']}\" />" .
			"<input id=\"button_cancel_{$result[$i]['CommentID']}\" style=\"display:none;\" type=\"button\" onClick=\"CancelEdit('{$result[$i][CommentID]}')\" class=\"formsmallbutton \" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['Cancel']}\" /></div></td></tr>";
}
$html_scheme_list .= "<tr id=\"add_row\" style=\"display:none;\">";
$html_scheme_list .= "<td>$num_count</td>";
$html_scheme_list .= "<td><b>".$Lang["IES"]["Chinese"]."</b><br/><textarea rows=\"4\" name=\"addcomment[B5]\" id=\"addcomment\"></textarea>
						<br/><b>".$Lang["IES"]["English"]."</b><br/><textarea rows=\"4\" name=\"addcomment[EN]\" id=\"addcommenten\"></textarea></td>";
$html_scheme_list .= "<td class=\"sub_row\">";
$html_scheme_list .= "<input type=\"button\" onClick=\"CheckForm('new')\" class=\"formsmallbutton \" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['Save2']}\" />";
$html_scheme_list .= "<input type=\"button\" onClick=\"CancelAddComment()\" class=\"formsmallbutton \" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['Cancel2']}\" />";
$html_scheme_list .= "</td>";
$html_scheme_list .= "</tr>";

$categoryDetails = current($objIES->getCommentCategoryDetails_Lite($categoryID));
$PAGE_NAVIGATION[] = array($Lang['IES']['CommentCategory'],"index.php?mod=commentbank&task=listCategory");
$PAGE_NAVIGATION[] = array($categoryDetails["TITLE"]." / ".$categoryDetails["TITLEEN"]);
$PAGE_NAVIGATION[] = array($Lang['IES']['Comment']);
$html_navigation = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);
$linterface->LAYOUT_START();
include_once("templates/commentbank/listComment.tmpl.php");
$linterface->LAYOUT_STOP();

?>