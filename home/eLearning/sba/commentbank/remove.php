<?
$objIES = new libies();
$ldb = new libdb();

$msg = "delete";

if(!$objIES->CHECK_ACCESS("IES-MGMT-COMMENTBANK-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$sql = "DELETE FROM {$intranet_db}.`IES_COMMENT` WHERE CommentID = $id";

$result = $ldb->db_db_query($sql);

intranet_closedb();
$redirectTaskInfo = "task=listComment&categoryID=$categoryID";
header("Location: index.php?mod=".$mod."&msg=".$msg."&$redirectTaskInfo");
?>
