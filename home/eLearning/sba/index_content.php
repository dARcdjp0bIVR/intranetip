<?
# using : Bill Mak
include_once($PATH_WRT_ROOT."includes/sba/libSba_ui.php");

$sba_libSba_ui = new libSbaUI(); 

# check access right of student (student can only access published scheme)
if($sba_thisMemberType==USERTYPE_STUDENT && (isset($scheme_id) && $scheme_id!="")) {
	$AllowAccessThisScheme = $sba_libSba->AllowAccessScheme($scheme_id, $sba_thisStudentID);
	if(!$AllowAccessThisScheme) {
		header("Location: index.php?mod=content");
		exit;	
	}
}


###########################
# Define Scheme selection #
###########################
$sba_libScheme = new SbaScheme($scheme_id);
//debug_r($scheme_id);
$sba_libStage = new SbaStage($stage_id, $sba_allow_edit);

if(isset($stage_id) && $stage_id!="") {
	$stageRecordStatus = $sba_libStage->getRecordStatus();

	if(!$sba_allow_edit && !$stageRecordStatus) {
		header("Location: index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");
		exit;	
	}
}


$sba_h_scheme_select = $sba_libSba->DisplaySchemeMenuByUser($sba_thisStudentID, $scheme_id, $sba_libScheme->getDefaultSchemeCode());


//Get the detail description
$folder_path = "{$intranet_root}/file/sba/lang/scheme/{$scheme_id}/";
$file_path = $folder_path."scheme_lang.php";
$html_folderDesc ="";
$html_textDesc ="";
$html_noteDesc ="";
$html_worksheetDesc ="";
$html_FAQDesc ="";

$objIESS = new libies();
$All_DESC = $objIESS->GET_DESC_DETAIL($file_path);
if($All_DESC != NULL)
{
	$html_folderDesc = $All_DESC["FolderDesc"];
	$html_textDesc = $All_DESC["TextDesc"];
	$html_noteDesc = $All_DESC["NoteDesc"];
	$html_worksheetDesc = $All_DESC["WorksheetDesc"];
	$html_FAQDesc = $All_DESC["FAQDesc"];
}


##########################
# Define Stage selection #
##########################
//if($scheme_id != "") {
	$sba_h_stage_select = $sba_libSba->DisplayStage($scheme_id, $stage_id);
//}


############################
# Define Toolbar selection #
############################
if($sba_allow_edit || $sba_is_preview){


//		$toolbar_content = '<div class="pretend_stu_tool" style="margin-right:40px; margin-top:-5px;">
//			<div class="below"><span>'.$Lang['SBA']['ContentPreivewMode'].'</span></div></div>';
//$toolbar_content = '<ul style="display:block"><li><div class=" pretend_stu_tool" style="margin-right:40px; margin-top:-5px;"><div class="below"><span>'.$Lang['SBA']['ContentPreivewMode'].'</span></div></div></li></ul>';
	$cssFor_student_tools = ' style="background:none" ';
        
}else{
	
	$noteid = $objIESS->retrieveNoteIdByStageIdAndUserId($UserID,$stage_id);
	$cssFor_student_tools  = '';
}
$toolbar_onclick = array("loadContent('stu_files',$scheme_id)",
                            "loadContent('bibi',$scheme_id)",
                            "loadReflectNote('reflect_note',$scheme_id,$stage_id,'$noteid')",
                            "loadContent('worksheet',$scheme_id)",
                            "loadContent('faq',$scheme_id);",
                            "loadContent('taskans',$scheme_id);",                                 
                           );
$toolbar_content = '<ul style="display:block">
                        <li><a href="javascript:void(0)" onclick="'.$toolbar_onclick[0].'" title="'.$html_folderDesc.'" class="stu_files"><span>'.$Lang['IES']['FolderDesc'].'</span></a></li>
                        <li><a href="javascript:void(0)" onclick="'.$toolbar_onclick[1].'" class="bibi"><span>'.$Lang['IES']['TextDesc'].'</span></a></li>
                        <li><a href="javascript:void(0)" onclick="'.$toolbar_onclick[2].'" class="reflect_note"><span>'.$Lang['IES']['NoteDesc'].'</span></a></li>
                        <li><a href="javascript:void(0)" onclick="'.$toolbar_onclick[3].'" class="worksheet"><span>'.$Lang['IES']['WorksheetDesc'].'</span></a></li>
                        <li><a href="javascript:void(0)" onclick="'.$toolbar_onclick[4].'" class="faq"><span>'.$Lang['IES']['FAQ'].'</span></a></li>
                        <li><a href="javascript:void(0)" onclick="'.$toolbar_onclick[5].'" class="mywork"><span>'.$Lang['IES']['TaskAnswer'].'</span></a></li>
                        <!--li><a href="javascript:void(0)" onclick="" class="theme_setting"><span>'.$Lang['SBA']['SchemeSetting'].'</span></a></li>
                        <li><a href="javascript:void(0)" onclick="" class="survey_ques"><span>'.$Lang['IES']['DefaultSurveyQuestion'].'</span></a></li-->
                    </ul>';
                                
$sba_h_student_toolbar = '

 		<!-- student tool -->


		<div class="student_tools" '.$cssFor_student_tools.'>
			<div>
	        	'.$toolbar_content.'
        	</div>
		</div>
		<div class="IES_file floating_tool_box draggable resizable" id="stu_files" style="width:700px; top:120px; right:15px; display:none;">
			<!-- student file list (ajax display)-->
		</div>
		<div class="IES_bibi floating_tool_box draggable resizable" id="bibi" style="width:700px; top:120px; right:15px; display:none;">
			<!-- student file list (ajax display)-->
		</div>
		<div class="IES_reflect floating_tool_box draggable resizable" id="reflect_note" style="width:700px; top:120px; right:15px; display:none;">
	   		<!-- student file list (ajax display)-->
	  	</div>
		<div class="IES_worksheet floating_tool_box draggable resizable" id="worksheet" style="width:700px; top:120px; right:15px; display:none;">
	   		<!-- student file list (ajax display)-->
	  	</div>
		<div class="IES_faq floating_tool_box draggable resizable" id="faq" style="width:700px; top:120px; right:15px; display:none;">
	   		<!-- student file list (ajax display)-->
	  	</div>
		<div class="IES_mywork floating_tool_box draggable resizable" id="taskans" style="width:700px; top:120px; right:15px; display:none;">
	   		<!-- student file list (ajax display)-->
	  	</div>
		<!-- student tool end-->
';


?>