<?
# using : thomas

# Initialize Variable
$r_sba_reorderStage = !empty($r_sba_reorderStage) && is_array($r_sba_reorderStage) && count($r_sba_reorderStage)>0? $r_sba_reorderStage : array();

# Initialize Objects
$ldb = new libdb();

# Update the Stage Sequence
$result = array();
for($i=0, $i_max=count($r_sba_reorderStage); $i<$i_max; $i++){
	$sql = "UPDATE
				{$intranet_db}.IES_STAGE
			SET
				Sequence = ".($i+1).",
				DateModified = NOW(),
				ModifyBy = $sba_thisUserID
			WHERE
				StageID = ".$r_sba_reorderStage[$i];
	$result[] = $ldb->db_db_query($sql);
}

$msg = in_array(false, $result)? "":"UpdateSuccess";

?>

<script>
	window.parent.location.href = 'index.php?mod=content&task=scheme_list&scheme_id=<?=$scheme_id?>&msg=<?=$msg?>';
	window.parent.tb_remove();
</script>