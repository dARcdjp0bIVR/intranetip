<?
// editing by : thomas
include_once($PATH_WRT_ROOT."includes/sba/libies.php");
$linterface = new interface_html("sba_default2.html");


$h_schemeTitle = $sba_libScheme->getTitle();
$h_schemeTitle = htmlspecialchars($h_schemeTitle);
$h_schemeMaxScore = $sba_libScheme->getMaxScore();

$h_schemeIntroduction = $linterface->Get_Display_From_TextArea($sba_libScheme->getIntroduction());

$objIES = new libies();

$result = $objIES->GET_STAGE_ARR($scheme_id, $sba_allow_edit);
$displayWholeNewUI = false;

if(count($result) == 0){
	//there is no Stage before for this scheme , display whole new UI is true
	$displayWholeNewUI = true;
}
else{


	$u_existingStage  = '<table class="about_stage">';
	$u_existingStage .= '<tr class="title"><td>'.$Lang['IES']['Criteria'].'</td><td>'.$Lang['IES']['DateOfSubmission'].'</td>';
	if($sba_allow_edit) {
		$u_existingStage .= '<td>'.$Lang['IES']['Status'].'</td><td>'.$Lang['IES']['Weight'].'</td><td>'.$Lang['SBA']['FullMark'].'</td>';
	}
	$u_existingStage .= '<td>'.$Lang['SBA']['TasksAndSteps'].'</td>';
	$u_existingStage .= '<td>&nbsp;</td>';
	$u_existingStage .= '</tr>';



	$buttonCaption1  = ($sba_allow_edit)? $Lang['SBA']['GoToStageSetting']:$Lang['SBA']['GoToStageSetting'];
	//GENERATE THE STAGE LIST
	for($i = 0, $i_max = count($result); $i < $i_max; $i++){
		
		$_ColorIndex = ($i % 5) + 1;
		
		$_Title = $result[$i]['Title'];
		$_Deadline = $sba_libStage->ReturnValidSubmissionDate($result[$i]['Deadline']);
		$_StageID = $result[$i]['StageID'];
		$_Weight = $result[$i]['Weight'] ? $result[$i]['Weight'] : "-";
		$_MaxScore = $result[$i]['MaxScore'] ? $result[$i]['MaxScore'] : "-";
		$_RecordStatus = $result[$i]['RecordStatus']==1 ? $Lang['IES']['RecordStatusPublish'] : $Lang['IES']['RecordStatusNotPublish'];
		
		$_stageDetailsBtn = "<a href='javascript:void(0)' onclick='show_dyn_thickbox_ip(\"\",\"index.php?mod=content&task=stageDetails&scheme_id={$scheme_id}&stage_id={$_StageID}\", \"TB_iframe=true&modal=true\", 1, 480, 780, 1)'>".$Lang['SBA']['StageDetails']."</a>";
		
		$u_existingStage .='<tr class="s'.$_ColorIndex.'"><td>'.$_Title.'</td><td>'.$_Deadline.'</td>';
		if($sba_allow_edit) {
			$u_existingStage .= '<td>'.$_RecordStatus.'</td><td>'.$_Weight.'</td><td>'.$_MaxScore.'</td>';
			$u_existingStage .= '<td>'.$_stageDetailsBtn.'</td>';
			$u_existingStage .= '<td><input name="submit2" type="button" class="formbutton" value="'.$buttonCaption1.'" onclick="location.href=\'?mod=content&task=stageInfo&stage_id='.$_StageID.'&scheme_id='.$scheme_id.'\'"/></td>';
		} else {
			$u_existingStage .= '<td>'.$_stageDetailsBtn.'</td>';
			$u_existingStage .= '<td><input name="submit2" type="button" class="formbutton" value="'.$Lang["IES"]['Enter'].'" onclick="location.href=\'?mod=content&task=stageInfo&stage_id='.$_StageID.'&scheme_id='.$scheme_id.'\'"/></td>';	
		}
		
		$u_existingStage .= '</tr>';
	}
	
	$u_existingStage .= '<tr>
						  <td style="padding: 0pt;">&nbsp;</td>
						  <td>&nbsp;</td><td>&nbsp;</td>';
	if($sba_allow_edit) {
		$u_existingStage .= '	
						  <td style="text-align: right;">'.$Lang['SBA']['MaxScore'].':</td>
						  <td>
							<strong>'.$h_schemeMaxScore.'</strong>
						  </td>';
	}
		$u_existingStage .= '
						  <td>&nbsp;</td>
						</tr>';
	
	$u_existingStage  .= '</table>';
	
}

if($msg!="") {
	$ReturnMsg = $Lang['SBA']['ReturnMessage'][$msg];	
}

$linterface->LAYOUT_START($ReturnMsg);

$template_script = "templates/content/scheme_list.tmpl.php";
include_once($template_script);

$linterface->LAYOUT_STOP();
?>