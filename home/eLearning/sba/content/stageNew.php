<?
// modifying : henry chow

$linterface = new interface_html("sba_default.html");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");

$objSbaScheme = new SbaScheme($scheme_id);
$objSbaUI = new libSbaUI();
$objIES = new libies();

# Scheme title
$schemeTitle = $objSbaScheme->getTitle();

# stage title 
$h_headling = "[".$Lang['SBA']['StageName']."]";

# deadline 
$h_deadline = $Lang['IES']['SubmissionDeadline']." : ".$Lang['SBA']['EmptyValue'];

# display the color of next sequence in html form
$h_stage_sequence = $sba_libSba->getMaxStageSequence($scheme_id)%5 + 1;


// navigation
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$schemeTitle, 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");		   
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$Lang['SBA']['AddStage'], 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"");
$h_navigation = $sba_libSba_ui->getNavigation($nav_ary);



$h_main_content = $objSbaUI->GetStageEditUI($scheme_id);

$linterface->LAYOUT_START();

$template_script = "templates/content/stageEdit.tmpl.php";
include_once("templates/paper_template1.php");


$linterface->LAYOUT_STOP();


?>