<?

include_once($intranet_root."/includes/sba/libSbaStep.php");
include_once($intranet_root."/includes/sba/libSbaStepHandinFactory.php");

# Initialize Variable
$step_id 	 	   = isset($step_id) && $step_id!=''? $step_id:'';
$r_answer_id 	   = isset($r_answer_id) && $r_answer_id!=''? $r_answer_id:'';
$r_stdAnswer	   = isset($r_stdAnswer) && is_array($r_stdAnswer)? $r_stdAnswer : array();
$r_powerconcept_id = isset($r_powerconcept_id) && $r_powerconcept_id!=''? $r_powerconcept_id:'';

# Initialize Objects
$objStepMapper = new SbaStepMapper();

# Get Step Object
$step = $objStepMapper->getByStepId($step_id);

if($step->getQuestionType()==$sba_cfg['DB_IES_STEP_QuestionType']['powerConcept'][0]){
	# Get the Step Handin Object
	$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $step->getQuestionType(), $step->getStepID(), $step->getQuestion(), $r_answer_id);
	
	# Insert PowerConcept Record if no PowerconceptID supply 
	if(!$r_powerconcept_id){
		$r_powerconcept_id = $sba_libSba->insertEmptyPowerConceptRecord($sba_thisStudentID);
		
		# Check the PowerConcept ID is in Student Answer Array or not 
		if(!in_array($r_powerconcept_id, $r_stdAnswer)){
			$r_stdAnswer[] = $r_powerconcept_id;
		}
	}
	asort($r_stdAnswer, SORT_NUMERIC);
	
	# Set Step Handin Property
	$objStepHandin->setStepID($step_id);
	$objStepHandin->setUserID($sba_thisStudentID);
	$objStepHandin->setQuestionCode(null);
	$objStepHandin->setStudentAnswer($r_stdAnswer);
	$objStepHandin->setAnswerActualValue(null);
	$objStepHandin->setAnswerExtra(null);
	$objStepHandin->setDateModified(date('Y-m-d H:i:s'));
	$objStepHandin->setModifyBy($sba_thisStudentID);
		
	# Set Step Handin Property for newly created Step Handin
	if(!$r_answer_id){
		$objStepHandin->setDateInput(date('Y-m-d H:i:s'));
		$objStepHandin->setInputBy($sba_thisStudentID);
	}
	
	$objStepHandin->save();
	
	$js_function = "/* Reload the the Step Content in Opener Windwo */
					window.opener.task_content_introStep_tabMenu_onclick($task_id, 0, 0, $step_id);
					
					/* Go to PowerConcept Tool */
					window.location.href = '/templates/powerconcept/index.php?PowerConceptID=$r_powerconcept_id';";
}
else{
	# This Page is for PowerConcept Step Only, Close this Popup if QuestionType is not PowerConcept
	$js_function = "self.close();";
}
?>
<script><?=$js_function?></script>