<?
// modifying : henry chow
//include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");

$linterface = new interface_html("sba_default.html");

$objSbaScheme = new SbaScheme($scheme_id);
$objIES = new libies();
$objSbaUI = new libSbaUI();


$schemeTitle = $objSbaScheme->getTitle();

$schemeDetails = $objIES->GET_SCHEME_DETAIL($scheme_id);


$stageDetailsAry = $objIES->GET_STAGE_DETAIL($stage_id);

$_stageTitle = $stageDetailsAry["Title"];
$_stage_id = $stage_id;
$_stageWeight = $stageDetailsAry["Weight"];
$_stageMaxScore = $stageDetailsAry["MaxScore"];
$_stageDescription = $stageDetailsAry["Description"];
$_stageDeadline = $sba_libStage->ReturnValidSubmissionDate($stageDetailsAry['Deadline']);
$h_stage_sequence = $stageDetailsAry['Sequence']%5==0 ? 5 : $stageDetailsAry['Sequence']%5;

$stage_mark_criteria_arr = libies_static::getStageMarkingCriteria($scheme_id, $stage_id);

$h_main_content = $objSbaUI->GetStageEditUI($scheme_id, $stage_id);


// navigation
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$schemeTitle, 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");		   
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$_stageTitle, 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=stageInfo&scheme_id=$scheme_id&stage_id=$stage_id");
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$Lang['IES']['Edit'], 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"");
$h_navigation = $sba_libSba_ui->getNavigation($nav_ary);

# stage title 
$h_headling = $_stageTitle;

# deadline 
if($_stageDeadline!="") {
	$h_deadline = $Lang['IES']['DateOfSubmission'].' : '.$_stageDeadline;
}

# extra button [Start]

// delete button
$extraButton = '<input name="submit" type="button" class="formbutton" onmouseover="this.className=\'formbuttonon\'" onmouseout="this.className=\'formbutton\'");return document.MM_returnValue" value="'.$Lang['IES']['Delete'].'" onClick="Delete_Stage('.$stage_id.')" />';

# extra button [End]



if($msg!="") {
	$ReturnMsg = $Lang['SBA']['ReturnMessage'][$msg];	
}

$linterface->LAYOUT_START($ReturnMsg);

$template_script = "templates/content/stageEdit.tmpl.php";
include_once("templates/paper_template1.php");


$linterface->LAYOUT_STOP();
?>