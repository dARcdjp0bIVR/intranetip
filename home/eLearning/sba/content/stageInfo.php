<?
//Using: Bill
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");

$linterface = new interface_html("sba_default.html");

$objSbaScheme = new SbaScheme($scheme_id);
$objIES = new libies();
$objSbaUI = new libSbaUI();

if(!isset($stage_id) || $stage_id=="") { 
	if(isset($scheme_id) && $scheme_id!="")
		header("Location: index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");
	else 
		header("Location: index.php?mod=content");
	exit;
}

$schemeTitle = $objSbaScheme->getTitle();

$stageDetailsAry = $objIES->GET_STAGE_DETAIL($stage_id);

$_stage_title = stripslashes($stageDetailsAry['Title']);
//$_stage_description = stripslashes(nl2br($stageDetailsAry['Description']));
$_stage_description = $linterface->Get_Display_From_TextArea($stageDetailsAry['Description']);
$_stage_deadline = $sba_libStage->ReturnValidSubmissionDate($stageDetailsAry['Deadline']);
$_stage_recordstatus = $stageDetailsAry['RecordStatus'];
$h_stage_sequence = $stageDetailsAry['Sequence']%5==0 ? 5 : $stageDetailsAry['Sequence']%5;

$h_headling = $_stage_title;

$h_deadline = $Lang['IES']['SubmissionDeadline'].' : '.$_stage_deadline;

# for student view, the description should be move upwards
if($sba_allow_edit) {	// teacher view

	$thisRecordStatusText = $_stage_recordstatus ? $Lang['IES']['RecordStatusPublish'] : $Lang['IES']['RecordStatusNotPublish'];

	$h_main_content = ' 
                    <br />
                    <table class="form_table">
                    <col width="120">
                    <col class="field_c" />   
                    
                    <tr><td>'.$_stage_description.'</td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td>'.$Lang['IES']['Status'].' : '.$thisRecordStatusText.'</td></td>
                    </table>
				';
} else {		// student view
	$h_main_content = '
                    <table class="form_table">
                    <col width="120">
                    <col class="field_c" />   
                    <tr><td>'.$_stage_description.'</td></tr>
                    </table>
				';
	
}				
				
if($sba_allow_edit) {
	
	$MarkCriteriaContent = $objSbaUI->GetSbaMarkCriteriaTable($scheme_id, $stage_id, $viewOnly=1, $isNewStage=0);
	
	$CriteriaTableContent = $MarkCriteriaContent[0];
	//$StageWeightMaxScore = $MarkCriteriaContent[1];
	
	$h_main_content .= '
		<div class="line_dotted"></div>
		        
			<i>'.$Lang['SBA']['ContentBelowOnlyViewableByTeacher'].'</i><br /><br />

            <table class="form_table">
            <col class="field_title" />
            <col class="field_c" />                    
            <tr><td>'.$Lang['IES']['MarkingSettings'].'</td><td>:</td><td>
				';
	//$h_main_content .= $objSbaUI->GetMarkCriteriaTable($scheme_id, $stage_id, $viewOnly=1, $isNewStage=0);
	$h_main_content .= $CriteriaTableContent;
	$h_main_content .= '
			</td></tr>

		'.$StageWeightMaxScore.'

		</table>';
	
}				
$h_main_content .= '
                  </div>  
';

# page navigation 
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$schemeTitle, 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"index.php?mod=content&task=scheme_list&scheme_id=$scheme_id");
				   
$nav_ary[] = array($sba_cfg['SBA_NAVIGATION_ARY']['Caption']=>$_stage_title, 
				   $sba_cfg['SBA_NAVIGATION_ARY']['Link']	=>"");
$h_navigation = $sba_libSba_ui->getNavigation($nav_ary);
/*
$h_navigation = '<a href="index.php?mod=content&task=scheme_list&scheme_id='.$scheme_id.'">'.$Lang['IES']['Scheme'].'</a>
					&gt;
					<span>'.$_stage_title.'</span>
				';
*/
if($msg!="") {
	$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];	
}

$linterface->LAYOUT_START($ReturnMsg);

$template_script = "templates/content/stageInfo.tmpl.php";
include_once("templates/paper_template1.php");


$linterface->LAYOUT_STOP();
?>