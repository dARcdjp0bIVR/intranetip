<?
# using : Thomas

include_once($intranet_root."/includes/sba/libSbaTask.php");

# Initialize Objects
$objTaskMapper = new SbaTaskMapper();
$linterface    = new interface_html("sba_thickbox.html");

# Define $h_thickbox_header
$h_thickbox_header = $Lang['SBA']['ReOrder'];

# Define $h_class
$h_class = "stage_reorder";

# Get All Tasks of this Stage
$tasks_ary = $objTaskMapper->findByStageId($stage_id);

# Define $h_thickbox_content
for($i=0, $i_max=count($tasks_ary); $i<$i_max; $i++){
	$_objTask = $tasks_ary[$i];
	
	$h_thickbox_content .= "<div class=\"task_title task_open sba_reorderTask_item\" style=\"clear:both\">
								<span>".$_objTask->getTitle()."</span>
								<input type=\"hidden\" id=\"sba_reorderTask_".$_objTask->getTaskID()."\" name=\"r_sba_reorderTask[]\" value=\"".$_objTask->getTaskID()."\">
							</div>";
}

# Define $h_thickbox_bottom_btn
$h_thickbox_bottom_btn = "<input type=\"button\" value=\"".$Lang['SBA']['Btn_Apply']."\" onclick=\"formSubmit()\" onmouseout=\"this.className='formbutton'\" onmouseover=\"this.className='formbuttonon'\" class=\"formbutton\" name=\"submit2\">
						  <input type=\"button\" value=\"".$Lang['IES']['Cancel']."\" onclick=\"window.parent.tb_remove()\" onmouseout=\"this.className='formsubbutton'\" onmouseover=\"this.className='formsubbuttonon'\" class=\"formsubbutton\" name=\"submit2\">";

$linterface->LAYOUT_START();

$template_script = "templates/content/reorderTask.tmpl.php";
include_once("templates/thickbox_template.php");

$linterface->LAYOUT_STOP();
?>