<?
# using : thomas

# Initialize Variable
$r_sba_reorderStep = !empty($r_sba_reorderStep) && is_array($r_sba_reorderStep) && count($r_sba_reorderStep)>0? $r_sba_reorderStep : array();

# Initialize Objects
$ldb = new libdb();

# Update the Stage Sequence
$result = array();
for($i=0, $i_max=count($r_sba_reorderStep); $i<$i_max; $i++){
	$sql = "UPDATE
				{$intranet_db}.IES_STEP
			SET
				Sequence = ".($i+1).",
				DateModified = NOW(),
				ModifyBy = $sba_thisUserID
			WHERE
				StepID = ".$r_sba_reorderStep[$i];
	$result[] = $ldb->db_db_query($sql);
}
?>
<script>
	window.parent.get_task(<?=$task_id?>);
	window.parent.tb_remove();
</script>