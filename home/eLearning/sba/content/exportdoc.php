<?php
/**
 * Editing by Paul
 * 
 * Modification Log: 
 * 2019-05-17 (Paul) [ip.2.5.10.6.1]
 * 		- edit the folder path to ensure the file is under eclass folder
 * 2015-06-19 (Siuwan) [ip.2.5.6.8.1] 
 * 		- set $exportFormat to support 2017 format
 */
include_once($PATH_WRT_ROOT."/includes/sba/libSbaTask.php");
include_once($PATH_WRT_ROOT."/includes/sba/libSbaTaskHandinFactory.php");
include_once($PATH_WRT_ROOT."/includes/sba/libSbaExport.php");
include_once($PATH_WRT_ROOT."/includes/sba/sbaConfig.inc.php");
include_once($PATH_WRT_ROOT."/includes/libuser.php");
include_once($PATH_WRT_ROOT."/includes/libfilesystem.php");

# Retrieve Scheme Title and User English Name
$objUser = new libuser($sba_thisUserID);
$export_stage_title = str_replace(' ','_',$sba_libStage->getTitle());
$export_stage_title = preg_replace('/[\\\\\/\:\*\?\"\<\>\|"]/','',$export_stage_title);
$userEngName = $objUser->EnglishName;

# Initialize Variable
$_folder = $intranet_root.'/file'.$sba_cfg['SBA_EXPORT_STUDENTHANDIN_ROOT_PATH']."u_".$sba_thisUserID."/stage";

$TargetFolder = $_folder."/".$stage_id;
# Initialize Objects
$SbaExport = new libSbaExport($TargetFolder, $sba_thisUserID, $scheme_id, $stage_id);
$fs 	   = new libfilesystem();

# Export the Stage Report to temp location
$SbaExport->setExportFormat($exportFormat);
$SbaExport->exportStageReport();

# Remove the zip file first
$zip_name = $sba_thisUserID."_Stage_".$stage_id.".zip";
$zip_path = $_folder."/".$zip_name;

$fs->file_remove($zip_path);
$fs->file_zip($stage_id, $zip_path,$_folder);

# Remove Stage Report in temp location
$fs->folder_remove_recursive($TargetFolder);
	
# Output the zip file to browser and let the user download it.
output2browser(get_file_content($zip_path), $userEngName."_Stage_".$export_stage_title.".zip");
?>