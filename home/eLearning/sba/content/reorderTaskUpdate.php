<?
# using : thomas

# Initialize Variable
$r_sba_reorderTask = !empty($r_sba_reorderTask) && is_array($r_sba_reorderTask) && count($r_sba_reorderTask)>0? $r_sba_reorderTask : array();

# Initialize Objects
$ldb = new libdb();

# Update the Stage Sequence
$result = array();
for($i=0, $i_max=count($r_sba_reorderTask); $i<$i_max; $i++){
	$sql = "UPDATE
				{$intranet_db}.IES_TASK
			SET
				".($i==0? "Approval = 0,":"")."
				Sequence = ".($i+1).",
				DateModified = NOW(),
				ModifyBy = $sba_thisUserID
			WHERE
				TaskID = ".$r_sba_reorderTask[$i];
	$result[] = $ldb->db_db_query($sql);
}

$msg = in_array(false, $result)? "":"UpdateSuccess";

?>

<script>
	window.parent.location.href = 'index.php?mod=content&task=task&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>&msg=<?=$msg?>';
	window.parent.tb_remove();
</script>