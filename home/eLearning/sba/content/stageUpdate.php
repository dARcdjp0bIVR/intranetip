<?php
# modifying : 
 
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");
//GET USER VARIABLE 
$scheme_id = intval(trim($scheme_id));


if(empty($scheme_id) || !is_int($scheme_id))
{
	echo "Invalid access , Empty SCHEME ID<br/>";
    exit();    
}


$ldb = new libdb();
$objIES = new libies();



if(!isset($stage_id) || $stage_id == "") {
	$isNewStage = true;	
}

$r_stageRecordStatus = (isset($r_stageRecordStatus) && $r_stageRecordStatus!="") ? $r_stageRecordStatus : $ies_cfg["recordStatus_notpublish"];

if($isNewStage) {
	$_thisStageMaxScore = $stageMaxScore[0] ? "'".$stageMaxScore[0]."'" : "NULL";
	$_thisStageWeight = $stageWeight[0] ? "'".$stageWeight[0]."'" : "NULL";
	$_thisStageMaxSequence = $sba_libSba->getMaxStageSequence($scheme_id) + 1;
	
	
	
	$sql = "INSERT INTO {$intranet_db}.IES_STAGE (SchemeID, Title, Description, Sequence, MaxScore, Weight, RecordStatus, Deadline, DateInput, InputBy, ModifyBy) VALUES (".$scheme_id.", '{$r_stage_name}', '{$r_stage_description}', {$_thisStageMaxSequence}, {$_thisStageMaxScore}, {$_thisStageWeight}, {$r_stageRecordStatus}, '{$r_stage_deadline}', NOW(), {$sba_thisUserID}, {$sba_thisUserID})";
	//echo $sql."<br>";
  	$res[] = $objIES->db_db_query($sql);

  	$t_stage_id = $objIES->db_insert_id();
  		
  	for($i=0, $i_max=count($stageCriteriaID); $i<$i_max; $i++)
	{
		$_thisStageCriteria = ($stageCriteriaID[$i]) ? $stageCriteriaID[$i] : $i;
		$_thisStageCriteriaWeight = $stageCriteriaWeight[$_thisStageCriteria] ? $stageCriteriaWeight[$_thisStageCriteria] : $sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore'];
		$_thisStageCriteriaMaxScore = $stageCriteriaMaxScore[$_thisStageCriteria] ? $stageCriteriaMaxScore[$_thisStageCriteria] : $sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore'];
		$_thisMarkCriteriaAry = $rubricCriteria[$i];
		$_thisMarkCriteriaID = array_keys($_thisMarkCriteriaAry);
		$_thisMarkCriteriaID = $_thisMarkCriteriaID[0];
		$_stageRecordStatus = $r_stageStatus[$i] ? $r_stageStatus[$i] : $ies_cfg["recordStatus_notpublish"];
		
		$sql = "INSERT IGNORE INTO {$intranet_db}.IES_STAGE_MARKING_CRITERIA (StageID, MarkCriteriaID, MaxScore, Weight, RecordStatus, DateInput, InputBy, ModifyBy) VALUES ({$t_stage_id}, {$_thisMarkCriteriaID}, {$_thisStageCriteriaMaxScore}, {$_thisStageCriteriaWeight}, {$_stageRecordStatus}, NOW(), $sba_thisUserID, $sba_thisUserID)";
			//$sql .= "SELECT {$t_stage_id}, MarkCriteriaID, defaultMaxScore, defaultWeight, NOW(), {$sba_thisUserID}, {$sba_thisUserID} FROM {$intranet_db}.IES_COMBO_MARKING_CRITERIA";
		//echo " CRITERIA : ".$sql.'<br>';
		$res[] = $objIES->db_db_query($sql);
	}
	
	// Create "MARKING_CRITERIA_OVERALL" by default
	$sql = "INSERT IGNORE INTO {$intranet_db}.IES_STAGE_MARKING_CRITERIA (StageID, MarkCriteriaID, MaxScore, Weight, DateInput, InputBy, ModifyBy) VALUES ({$t_stage_id}, 1, 1, 9, NOW(), $sba_thisUserID, $sba_thisUserID)";
	$res[] = $objIES->db_db_query($sql);
	
	$classRoomID = libies_static::getIESRurbicClassRoomID();
	$objRubric = new libiesrubric($classRoomID);

	if(is_array($rubricCriteria))
	{
		
		foreach($rubricCriteria AS $_stageID => $criteriaArr)
		{
		  foreach($criteriaArr AS $criteriaID => $std_rubric_set_id)
		  {
		    // skip if no rubric selected
		    if(empty($std_rubric_set_id)) continue;
		    //echo $std_rubric_set_id.'/';
		    $std_rubric_arr = $objRubric->getStandardRubric($std_rubric_set_id);
		    
		    // assume rubric set has only one rubric
		    $std_rubric_id = $std_rubric_arr[0]["STD_RUBRIC_ID"];
		    
		    // Copy standard rubric to task rubric
		    $task_rubric_id = $objRubric->stdRubric2taskRubric($scheme_id, $t_stage_id, $std_rubric_id);
		    //echo $scheme_id.'/'.$t_stage_id.'/'.$std_rubric_id.'<br>';debug_pr($task_rubric_id); 
		    // Get max score of rubrics
		    $rubric_max_score = $objRubric->getRubricMaxScore($task_rubric_id);
		    if($rubric_max_score=="") $rubric_max_score = 0;
		    
		    // Map task rubric to stage marking criteria
		    // Update max score by rubrics
		    $sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA set task_rubric_id={$task_rubric_id}, MaxScore={$rubric_max_score}, DateModified=NOW(), ModifyBy={$sba_thisUserID} WHERE StageID={$t_stage_id} AND MarkCriteriaID={$criteriaID}";
		    //$sql .= ", , , NOW(), $sba_thisUserID, NOW(), $sba_thisUserID)";
		    //echo $sql.'<br>';
		    $res['Rubric'] = $objIES->db_db_query($sql);
		    //debug_pr($res);	    
		  }
		}
	}
	
	$msg = "AddSuccess";
	
} else {
	
	$_stageID = $stage_id;
	$_stage_weight = $stageWeight[$_stageID] ? "'".$stageWeight[$_stageID]."'" : "NULL";
	$_stage_maxScore = $stageMaxScore[$_stageID] ? "'".$stageMaxScore[$_stageID]."'" : "NULL";
	$_stage_description = $r_stage_description;
	
	$sql = "UPDATE {$intranet_db}.IES_STAGE SET ";
	$sql .= "Title = '{$r_stage_name}', ";
	$sql .= "Description = '{$_stage_description}', ";
	//$sql .= "Weight = {$_stage_weight}, ";
	$sql .= "RecordStatus = {$r_stageRecordStatus}, ";
	//$sql .= "MaxScore = {$_stage_maxScore}, ";
	$sql .= "Deadline = '{$r_stage_deadline}'";
	$sql .= "WHERE StageID = {$_stageID}";

	if($_SESSION['UserType'] == USERTYPE_STAFF){
		//do nothing
	}else{

		header("Location: /");
		exit();
	}
	//echo $sql;
	$ldb->db_db_query($sql);


	//debug_pr($_POST);
	// Update stage criteria weight and max score
	for($i=0, $i_max=count($stageCriteriaID); $i<$i_max; $i++)
	{
		$_stageMarkingCriteriaID = $stageCriteriaID[$i];
		$_stage_criteria_weight = $stageCriteriaWeight[$_stageMarkingCriteriaID];
		if($_stage_criteria_weight=="") $_stage_criteria_weight = $sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore'];
		$_stage_criteria_maxScore = $stageCriteriaMaxScore[$_stageMarkingCriteriaID];
		if($_stage_criteria_maxScore=="") $_stage_criteria_maxScore = $sba_cfg['MarkingCriteriaDefaultEmptyWeightMaxScore'];
		$_stageRecordStatus = $r_stageStatus[$i] ? $r_stageStatus[$i] : $ies_cfg["recordStatus_notpublish"];
	
		if($_stageRecordStatus=="") $_stageRecordStatus = $ies_cfg["recordStatus_notpublish"];
	
		$sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA SET ";
		$sql .= "Weight = {$_stage_criteria_weight}, ";
		$sql .= "MaxScore = {$_stage_criteria_maxScore}, ";
		$sql .= "RecordStatus = {$_stageRecordStatus} ";
		$sql .= "WHERE StageMarkCriteriaID = {$_stageMarkingCriteriaID}";
		//echo $sql.'<br>';
		$a = $ldb->db_db_query($sql);
		//echo $a.'/';
	}
	
	$classRoomID = libies_static::getIESRurbicClassRoomID();
	$objRubric = new libiesrubric($classRoomID);
	
	//F: save to eclassRubric
	if(is_array($rubricCriteria))
	{
		foreach($rubricCriteria AS $_stageID => $criteriaArr)
		{
		  foreach($criteriaArr AS $criteriaID => $std_rubric_set_id)
		  {
		    // skip if no rubric selected
		    if(empty($std_rubric_set_id)) continue;
		    
		    $std_rubric_arr = $objRubric->getStandardRubric($std_rubric_set_id);
		    
		    // assume rubric set has only one rubric
		    $std_rubric_id = $std_rubric_arr[0]["STD_RUBRIC_ID"];
		    
		    // Copy standard rubric to task rubric
		    $task_rubric_id = $objRubric->stdRubric2taskRubric($scheme_id, $_stageID, $std_rubric_id);
		    
		    // Get max score of rubrics
		    $rubric_max_score = $objRubric->getRubricMaxScore($task_rubric_id);
		    
		    if($rubric_max_score=="") $rubric_max_score = 0;
		    
		    // Map task rubric to stage marking criteria
		    // Update max score by rubrics
		    $sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA ";
		    $sql .= "SET task_rubric_id = {$task_rubric_id}, MaxScore = {$rubric_max_score} ";
		    $sql .= "WHERE StageID = {$_stageID} AND MarkCriteriaID = {$criteriaID}";
		    $ldb->db_db_query($sql);	    
		    //echo $sql.'<br>';
		  }
		}
	}
	
	// re-calculate stage total mark after weight and max mark update
	//for($i=0, $i_max=count($stage_id); $i<$i_max; $i++)
	{
		$_stageID = $stage_id;
		libies_static::calculateStageMark($_stageID);
	}
	
	// re-calculate scheme total mark after weight and max mark update
	libies_static::calculateSchemeMark($scheme_id);
	
	$msg = "UpdateSuccess";
}

intranet_closedb();
//header("Location: scheme_rubric_details.php?msg=".$msg."&scheme_id=".$scheme_id);

//header("Location: index.php?mod=content&task=scheme_list&scheme_id=".$scheme_id);

$thisStageId = ($t_stage_id) ? $t_stage_id : $stage_id; 

header("Location: index.php?mod=content&task=stageInfo&scheme_id=".$scheme_id."&stage_id=".$thisStageId."&msg=".$msg);

?>