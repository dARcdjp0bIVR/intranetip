<?php
//EDITING BY: STANLEY
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$SurveyID = $_REQUEST['SurveyID'];
if(empty($SurveyID)){
	//echo "NO SurveyID";
	exit();
}

$libies_survey = new libies_survey();
$lexport = new libexporttext();

############################
# get the Survey Details
$SurveyDetails = $libies_survey->getSurveyDetails($SurveyID);
$SurveyDetails = current($SurveyDetails);
$SurveyStartDate = $SurveyDetails["DateStart"];
$SurveyEndDate = $SurveyDetails["DateEnd"];
$Survey_title = $SurveyDetails["Title"];
$Survey_desc = $SurveyDetails["Description"]; 
###
$question = $libies_survey->getSurveyQuestion($SurveyID);
$IS_VALID = true;
$answer = $libies_survey->getSurveyAnswer($SurveyID, $IS_VALID);

$questionsArray = $libies_survey->breakQuestionsString($question);
$ExportContent .= "\n".$Survey_title."\n";

for($i = 1; $i <= sizeof($questionsArray) ; $i++){
	$resultArray = $libies_survey->getAnalysisResultArray($questionsArray, $answer, $i);

	switch ($questionsArray[$i]["TYPE"]){
		case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:
			$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n	".GenExcelXorY($resultArray);
			break;
		
		case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:
			$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n	".GenExcelXorY($resultArray);
			break;
		case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:
			$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n".GenExcelText($resultArray);
			break;
			
		case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:
			$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n".GenExcelText($resultArray);
			break;
			
		case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:
			$ExportContent .= "\n\n{$i})".$questionsArray[$i]['TITLE']."\n	".genExcelForLS($resultArray, $i,$questionsArray);
			break;
			
		case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

			break;
	}
}

## Get Analysis Array##

##Get content to print


## Set the filename of excel

$filename = $sba_libSba->filename_safe($Survey_title.".csv");


$lexport->EXPORT_FILE($filename, $ExportContent, $isXLS, $ignoreLength=true);





/*
function filename_safe($name) { 
	    $except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', ' '); 
	    return str_replace($except, '_', $name); 
} 
*/
###
##function that will put into libies / libies_survey
###
/*
 * Gen Excel From Array (For X or Y)
 * 
 * Parameter: $ParResultArray - Array that store all analysis
 * 
 * Output: String -
 * 
 */
 
function GenExcelXorY($ParResultArray){
	global $sba_libSba;

	$return = "";
	if (is_array($ParResultArray)) {
		$tableContent = array();
		foreach($ParResultArray as $key => $element) {
			$header[] = $sba_libSba->trimOptions($element["OPT_POS_1"]);
			$ExcelContent[] = $element["VALUE"];
		}
	}
	$return .= implode("	",$header)."\n	".implode("	",$ExcelContent);
	return $return;
}

/*
 * Gen Excel From Array (For X AND Y)
 * 
 * Parameter: $ParResultArray - Array that store all analysis
 * 
 * Output: String -
 * 
 */
function GenExcelXandY($ParResultArray){
	$return = "";
	global $sba_libSba;
	if (is_array($ParResultArray)) {
		foreach($ParResultArray as $ParResultArrayKey => $ParResultArrayElement) {
			$header[$ParResultArrayElement["OPT_POS_1"]] = $sba_libSba->trimOptions($ParResultArrayElement["OPT_POS_1"]);;
			if (isset($tableContent[$ParResultArrayElement["OPT_POS_2"]])) {
				$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
			} else {
				$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $sba_libSba->trimOptions($ParResultArrayElement["OPT_POS_2"]);
				$tableContent[$ParResultArrayElement["OPT_POS_2"]][] = $ParResultArrayElement["VALUE"];
			}
			 
		}
	}
	$content = "";
	if (is_array($tableContent)) {
		foreach($tableContent as $tableContentKey => $tableContentElement) {
			$content .= implode("	",$tableContentElement)."\n";
		}
		}
	$return .= "	".implode("	",$header)."\n".$content;
	
	return $return;
}

function GenExcelText($ParResultArray){
	$return ="";
	
	for($i = 0; $i < sizeof($ParResultArray["ANSWER"]); $i++){
		$text = str_replace("\n", "\n	", $ParResultArray["ANSWER"][$i]);
		$return .= "	".$text."\n";
	}
	return $return;
}

function genExcelForLS($ParResultArray, $ParQuestionNumber,$ParQuestionsArray){

		global $sba_libSba;
		$return .= "#	問題";
		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["OPTION_NUM"]; $i++){
			$return .= "	".$sba_libSba->trimOptions($ParResultArray[0][$i]["OPT_POS_1"]);
		}

		for($i = 0; $i < $ParQuestionsArray[$ParQuestionNumber]["QUESTION_NUM"]; $i++){
			if (is_array($ParResultArray[$i])) {
				$q_num = $i + 1;
				$return .= "\n";
				$return .= "	{$q_num}";
				$return .= "	".$ParQuestionsArray[$ParQuestionNumber]["QUESTION_QUESTIONS"][$i];
				foreach($ParResultArray[$i] as $key => $element) {
					$return .= "	".$element['VALUE'];
				}
				
			}
		}
		
		return $return;
	}

/*
function trimOptions($ParOption) {
	return substr($ParOption,strpos($ParOption,"[")+1,strpos($ParOption,"]")-strpos($ParOption,"[")-1);
}
*/

?>