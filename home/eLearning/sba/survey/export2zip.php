<?php
/*
 * Modification Log:
 *  2019-05-17 (Paul) [ip.2.5.10.6.1]
 * 	 - Handling download attachment path to be using encrypted param target_e
 */
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
include_once("survey/generateChartScript.php");

$key = $_REQUEST["key"];
$hiddenUpdateResult = empty($hiddenUpdateResult) ? 0 : 1;

$SurveyIDAry = $_REQUEST['SurveyID'];

$export_directory = $intranet_root.'/file'.$sba_cfg['SBA_EXPORT_STUDENTHANDIN_ROOT_PATH']."u_".$sba_thisStudentID."/";

$libies_survey = new libies_survey();


for($a=0, $a_max=count($SurveyIDAry), $script=$html=""; $a<$a_max; $a++) {
			
	$SurveyID = $SurveyIDAry[$a];
	$_SESSION['thisExportSurveyID'] = $SurveyID;
	
	$question = $libies_survey->getSurveyQuestion($SurveyID);
	$questionsArray = $libies_survey->breakQuestionsString($question);
		
	$script .= "
		var QuestionTitleAry = new Array();\n
		var QuestionTypeAry = new Array();\n
		";
				
	for($i = 1; $i <= sizeof($questionsArray) ; $i++){
		$script .= "QuestionTypeAry[$i] = \"".$sba_libSba->filename_safe($questionsArray[$i]['TYPE'])."\";\n";
		
		$displayCreateNewQuestionOption = 2;
		$chartArr[] = $libies_survey->GetChartArr($SurveyID, $i,'',$sba_thisStudentID,$displayCreateNewQuestionOption);
		
		$html .= $libies_survey->ReturnSurveyChartDiv($questionsArray[$i]['TYPE'], $i);
		
	}
	
	$script .= "var dataArr = new Array();
		chartNum =".sizeof($chartArr).";";
	
	foreach($chartArr as $chartID => $chartJSON ) {
	
		if(count($chartJSON)==0) continue; 
		$script.="dataArr[".($chartID)."] = ".$chartJSON->toPrettyString()."\n";
	
	}
}

echo "<script>$script</script><div style='left:-9999px; position:absolute;'>$html</div>";

$fs = new libfilesystem();
$ZipFileName = "survey_u".$sba_thisStudentID.".zip";
$fs->file_zip("u_".$sba_thisStudentID, $ZipFileName, $export_directory);

$exportFilePath=$sba_cfg['SBA_EXPORT_STUDENTHANDIN_ROOT_PATH'].$ZipFileName;

intranet_closedb();
?>

<script type="text/javascript">
	var totalGraphs=0;
	
	function displayChartDiv(questionNo) {
		var ChartLayername = "my_chart_"+questionNo+"parent";
		var ChartTextDiv = "ChartTextDiv"+questionNo;
		
		if(document.getElementById(ChartLayername).style.display=="none") {
			document.getElementById(ChartLayername).style.display = "inline";
			$("#"+ChartTextDiv).html("<?=$Lang['SBA']['HideChart']?>");
		} else {
			document.getElementById(ChartLayername).style.display = "none";
			$("#"+ChartTextDiv).html("<?=$Lang['SBA']['ShowChart']?>");
		}	
	}
	
	function onAutoPostImgBinary(id){
		console.log('hihi');
		if (--totalGraphs==0){
			
			self.location.href='/home/download_attachment.php?target_e=<?=getEncryptedText($exportFilePath)?>';
		}


	}

	for(var k=0; k<chartNum; k++){
		
		if(QuestionTypeAry[k+1]==2 || QuestionTypeAry[k+1]==3 || QuestionTypeAry[k+1]==6) {
			totalGraphs++;
			console.log('post');
			var flashvars = {id:k, postImgUrl:"survey/uploadFlashImage.php"};
			swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart_"+(k+1)+"Final", <?=$sba_cfg['SBA']['SurveyChart']['ChartWidth']?>, <?=$sba_cfg['SBA']['SurveyChart']['ChartHeight']?>, "9.0.0", "", flashvars);
		}
	}
	//end for
</script>
