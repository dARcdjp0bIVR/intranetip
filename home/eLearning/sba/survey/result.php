<?php
//editing by: 
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
/*
//intranet_auth();
intranet_opendb();
*/


if(empty($_GET["key"])){
	echo "ERROR";
	exit();
}
$li = new libdb();
$libies_survey = new libies_survey();


##key - store SurveyID and Respondent
$originalKey = $_REQUEST["key"];
$parameters = $libies_survey->breakEncodedURI($originalKey);


$SurveyID = $parameters["SurveyID"];
$FromPage = $parameters["FromPage"];

$respondentNature = (!empty($_REQUEST["respondentNature"]))? $_REQUEST["respondentNature"] : $parameters["respondentNature"];
$Respondent = (!empty($parameters["Respondent"])) ? $parameters["Respondent"] : $_REQUEST["Respondent"];

$msg = $_REQUEST["msg"];
$isedit = $_REQUEST["isedit"];
//debug_r($_REQUEST);

//DEBUG_R($parameters);

if(empty($SurveyID) && empty($Respondent) || empty($respondentNature)){
	echo "ERROR";
	exit();
}


#### It is posted by the result page with edit mode
if($Edit_Validity){
//	echo "IN";
//	echo $Validity;
	$update_result = $libies_survey->UpdateSurveyAnswerStatus($SurveyID, $Respondent, $Validity, $respondentNature);
	if($update_result){
		$msg = "update";
	}
	else{
		
	}
}
###
$html["close_button"] = libies_ui::getCloseButton();

//######################GET the SURVEY ###########################
$SurveyDetails = $libies_survey->getSurveyDetails($SurveyID);
if(is_array($SurveyDetails) && count($SurveyDetails) == 1){
	$SurveyDetails = current($SurveyDetails);
}
$Survey_title = $SurveyDetails["Title"];
$Survey_desc = $SurveyDetails["Description"]; 
$Survey_question = $SurveyDetails["Question"];
$Survey_type = $SurveyDetails["SurveyType"];
$Survey_startdate = date("Y-m-d", strtotime($SurveyDetails["DateStart"]));
$Survey_enddate = date("Y-m-d", strtotime($SurveyDetails["DateEnd"]));
$html_TypeName = $SurveyDetails["SurveyTypeName"];
#########################################

//######################GET ANSWER OF THIS SURVEY##################
$survey_ans_detail_arr = $libies_survey->getSurveyAnsDetails($SurveyID, $Respondent, $respondentNature);
//DEBUG_R($survey_ans_detail_arr);
$Survey_Respondent = $Respondent;
$Survey_Answer = $survey_ans_detail_arr["Answer"]; 
$Survey_DateInput = $survey_ans_detail_arr["DateInput"];
$Survey_Validity = $survey_ans_detail_arr["Status"];
$Survey_DateModified = date("Y-m-d", strtotime($survey_ans_detail_arr["DateModified"]));
$Suvery_RespondentNature = $survey_ans_detail_arr['RespondentNature'];
//DEBUG_R($survey_ans_detail_arr);

if($Suvery_RespondentNature == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']){
	//$surveyEmailDetails = $libies_survey->getSurveyEmailDetails($SurveyID, $email);
	$html_Respondent = $survey_ans_detail_arr['StudentName'];
}
else{
	$html_Respondent = $Respondent;
}
#################################################


$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($Survey_type);	
$linterface = new interface_html("ies_survey.html");
## if is edit, it will be the editing mode
if($isedit){
	// navigation
//	$nav_arr[] = array($Lang['IES']['FinishedSurvey'], "management.php?key={$originalKey}");
	$nav_arr[] = array($Lang['IES']['FinishedSurvey'], "index.php?mod=survey&task=management&key={$originalKey}");
	$nav_arr[] = array($Lang['IES']['questionaireresult'], "");
	$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
	$html_set_validity .= "<tr>";
	$html_set_validity .= "<td>{$Lang['IES']['question_is_valid']}</td>";
	$html_set_validity .= "<td>:</td>";
	$html_set_validity .= "<td>";
	if($Survey_Validity){
		$html_set_validity .= "<input type=\"radio\" name=\"Validity\" id=\"Validity_1\" value=\"1\" checked=\"checked\"/><label for=\"Validity_1\">{$Lang['IES']['is_valid']}</label>&nbsp;&nbsp;&nbsp;&nbsp;";
		$html_set_validity .= "<input type=\"radio\" name=\"Validity\" id=\"Validity_0\" value=\"0\" /><label for=\"Validity_0\">{$Lang['IES']['is_invalid']}</label>";
	}
	else{
		$html_set_validity .= "<input type=\"radio\" name=\"Validity\" id=\"Validity_1\" value=\"1\"  /><label for=\"Validity_1\">{$Lang['IES']['is_valid']}</label>&nbsp;&nbsp;&nbsp;&nbsp;";
		$html_set_validity .= "<input type=\"radio\" name=\"Validity\" id=\"Validity_0\" checked=\"checked\" value=\"0\" /><label for=\"Validity_0\">{$Lang['IES']['is_invalid']}</label>";	
	}
	$html_set_validity .= "</td></tr>";


	//$html_savebtn = "<INPUT class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" type=\"submit\" Value=\"{$Lang['IES']['Save']}\"/>&nbsp;"; 
	//$html["close_button"] = "<INPUT class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" type=\"button\" Value=\"{$Lang['IES']['Cancel']}\" onclick=\"javascript:window.location='management.php?key={$originalKey}'\"/>";		



	$html_savebtn = "<INPUT class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" type=\"submit\" Value=\"{$Lang['IES']['Save']}\"/>&nbsp;"; 

	$html["close_button"] = "<INPUT class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" type=\"button\" Value=\"{$Lang['IES']['Cancel']}\" onclick=\"javascript:window.location='index.php?mod=survey&task=management&key={$originalKey}'\"/>";		

}
$html_message = isset($msg) ? $libies_survey->Get_WARNING_MSG($msg) : "";

############


$AnsArry = $libies_survey->splitQuestionnaireAnswers($Survey_question, $Survey_Answer);

//DEBUG_R($AnsArry);

$HTML_ANS = libies_ui::getSurveyResultTable($AnsArry);


######################################

$IS_IES_STUDENT = $libies_survey->isIES_Student($sba_thisUserID);
$html_tag = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU("ManageCompleteQuestionnaire",$IS_IES_STUDENT,$originalKey,$FromPage);

$Title = "Result";

$linterface->LAYOUT_START();

include_once("templates/survey/result.tmpl.php");

$linterface->LAYOUT_STOP();
//intranet_closedb();
?>