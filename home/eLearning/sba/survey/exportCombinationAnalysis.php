<?php
/** [Modification Log] Modifying By:
 * *******************************************
 * 2019-05-06 Isaac
 * Added hidden a tag element for trigger save highchart imgs.
 * *******************************************
 */

$linterface = new interface_html("sba_survey.html");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$linterface->LAYOUT_START();
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/sba/libSba.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
// include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
// include_once("survey/generateChartScript.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$objTaskMapper = new SbaTaskMapper();
$libies_survey = new libies_survey();
$sba_libSba = new libSba();
$fs = new libfilesystem();

// $fs->folder_remove_recursive($intranet_root.$sba_cfg['SBA_SURVEY_CHART_ROOT_PATH'].$_SESSION['UserID']."/".$_SESSION['thisExportSurveyID']."/");
$count=0;
$chartArr=array();
$html="<a href='#' id='saveChartImage' style='display:none;'></a>";
			
$SurveyID=$_SESSION['thisExportSurveyID'];
$SurveyMappingIDs=array();

$SurveyMappings = $libies_survey->loadSurveyMapping($SurveyID,$sba_thisStudentID);//mapping graphs
if (!is_array($SurveyMappings)) return;//no mapping, skip
$html = "<a id='saveChartImage' style='display:none;'></a>";
foreach ($SurveyMappings as $SurveyMapping){
	
    $SurveyMappingID=$SurveyMapping['SURVEYMAPPINGID']; //debug_pr($SurveyMappingID);
	$x_axis = $SurveyMapping["XMAPPING"];
	$y_axis = $SurveyMapping["YMAPPING"];
	
	$chartArr[$count]=$libies_survey->GetCombineHighChart($SurveyID,$x_axis,$y_axis,$SurveyMappingID, 'my_chart_'.$count.'_bar_Final');
	
	$html .= $libies_survey->ReturnSurveyChartDiv('surveyMapping', $count+1);
	
	$SurveyMappingIDs[]=$SurveyMappingID;
	$count++;
}

$_SESSION['thisExportSurveyMappingIDs']=$SurveyMappingIDs;

foreach($chartArr as $chart){
    $html .= $chart;
}
$html .= '<div id="chartImg"></div>';
echo '<script src="https://code.highcharts.com/highcharts.js"></script><script src="https://code.highcharts.com/modules/exporting.js"></script>';
echo "<div style='left:-9999px; position:absolute;'>$html</div>".'<script>'.$libies_survey->HighChartexportPngJSFunctions('surveyMapping').'</script>';
// echo "<div style=''>$html</div>".'<script>'.$libies_survey->HighChartexportPngJSFunctions('surveyMapping').'</script>';
?>
<?php echo $linterface->Get_Ajax_Loading_Image(); ?>
<script type="text/javascript">
    $(document).ready(function(){
    	$('#saveChartImage').click();
    });
    window.onload = function () {
        setTimeout(function () {
            document.getElementById('exportPage').click();
            setTimeout(function(){
                window.close();
                window.top.close();
            }, 2000);
        }, 5000);
    }


</script>

    <a id="exportPage" href="index.php?mod=survey&task=getExport&type=surveyMapping" target="_blank" style="display: none">&nbsp;</a>
<!--h2>Loading...</h2/-->
<?$linterface->LAYOUT_STOP();?>