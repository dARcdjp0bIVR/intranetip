<?php
// Modifying by 
/*
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
*/
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
//intranet_opendb();

$objIES = new libies();
$objIES_survey = new libies_survey();
$originalKey = $_REQUEST["key"];
$parameters = $objIES_survey->breakEncodedURI($originalKey);

$SurveyID = base64_encode($parameters["SurveyID"]);
$StudentID = $parameters["StudentID"];
$TaskID = $parameters["TaskID"];
$FromPage = $parameters["FromPage"];
// Survey Detail
$dec_SurveyID = base64_decode($SurveyID);
if(is_numeric($dec_SurveyID))
{
  $survey_detail_arr = $objIES->getSurveyDetails($dec_SurveyID);

  if(is_array($survey_detail_arr) && count($survey_detail_arr) ==1){

	  $survey_detail_arr = current($survey_detail_arr);

	  $survey_title = $survey_detail_arr["Title"];
	  $survey_desc = $survey_detail_arr["Description"];
	  $survey_question = $survey_detail_arr["Question"];

	  $survey_creatorID = $survey_detail_arr["InputBy"];
	  $survey_datemodified = date("Y-m-d", strtotime($survey_detail_arr["DateModified"]));
	  $survey_typename = $survey_detail_arr["SurveyTypeName"];
	  $survey_type = $survey_detail_arr["SurveyType"];
	  $survey_creator = new libuser($survey_creatorID);
  }
}
else
{
  header("Location: createForm.php");
}

if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
	$html_instr = <<<html
{$Lang['IES']['Questionnaire_viewQue']['string1']}<br />
		<ol>
		<li><b>{$Lang['IES']['Questionnaire_viewQue']['string2']}</b>{$Lang['IES']['Questionnaire_viewQue']['string3']}</li>
		<li><b>{$Lang['IES']['Questionnaire_viewQue']['string4']}</b>{$Lang['IES']['Questionnaire_viewQue']['string5']}</li>
		<li><b>{$Lang['IES']['Questionnaire_viewQue']['string6']}</b>{$Lang['IES']['Questionnaire_viewQue']['string7']}<br /><input type="radio" DISABLED/>{$Lang['IES']['Questionnaire_viewQue']['string8']}	<input type="radio" DISABLED/>{$Lang['IES']['Questionnaire_viewQue']['string9']}	
		<input type="radio" DISABLED/>{$Lang['IES']['Questionnaire_viewQue']['string10']}	<input type="radio" DISABLED/>{$Lang['IES']['Questionnaire_viewQue']['string11']}	<input type="radio" DISABLED/>{$Lang['IES']['Questionnaire_viewQue']['string12']}</li>
		</ol>
<hr />
html;
}

// Tab Config
$isTeacher = $objIES->isIES_Teacher($UserID);
$TabMenuArr = array();

if($isTeacher)
{
	$curTabIndex = 0;
	$html_studentName = $objIES_survey->getStudentSurveySelectionBox($originalKey,$survey_detail_arr["SurveyType"]);
}
else
{
//  $TabMenuArr[] = array("createForm.php?SurveyID={$SurveyID}", $Lang['IES']['BasicSettings']);
  $TabMenuArr[] = array("?mod=survey&task=createForm&SurveyID={$SurveyID}", $Lang['IES']['BasicSettings']);
//  $editBtn = "<input type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onclick=\"self.location='createQue.php?SurveyID={$SurveyID}'\" value=\" {$Lang['IES']['Edit']} \" />";

  $editBtn = "<input type=\"button\" class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" onclick=\"self.location='?mod=survey&task=createQue&SurveyID={$SurveyID}'\" value=\" {$Lang['IES']['Edit']} \" />";

  $curTabIndex = 1;
  
  $html_studentName = "";
}

$questionTabCaption = ($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]) ? $Lang['IES']['QuestionnaireQuestion']: $Lang['IES']['QuestionnaireQuestionType2'];

if(is_numeric($dec_SurveyID))
{
  $TabMenuArr[] = array("viewQue.php?key=".base64_encode("SurveyID={$dec_SurveyID}"), $questionTabCaption);
}
$html_tab_menu = libies_ui::GET_SURVEY_TAB_MENU($TabMenuArr, $curTabIndex);

$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	// ivan

$linterface = new interface_html("sba_survey.html");
$linterface->LAYOUT_START();

?>

<!-- Question Manager Class -->
<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/oo_survey/question_manager.js"></script>

<script language="JavaScript">

var qm = new question_manager();
var q_arr = new Array();
var q_order_arr = new Array();

function genQuestionTableDisplay()
{
  var qTable = "";

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    if(typeof q_obj == "object")
    {
      qTable += q_obj.genAnsQuestionDisplay(key);
    }
  });
  
  $("#checkQObj").html(qTable);
}

$(document).ready(function(){

  $.ajax({
    url: "/templates/oo_survey/questionnaire.php",
    type: "POST",
    dataType: "xml",
    success: function(xml){
      var basePath = $(xml).find("BasePath").text();
      var qTypeJSon = eval('(' + $(xml).find("QTypeJson").text() + ')');
      
      // Auto load config and question type js
      qm.load(basePath, qTypeJSon);
      
      // Load question string and parse to array
      var qStr = "<?=$survey_question?>";
      if(qStr != "")
      {
        var q_raw_arr = qm.parseQuestionStr(qStr);
        $.each(q_raw_arr, function(key, obj){
          var qType = obj.qType;
          var qRawStr = obj.qRawStr;
          
          var _obj_name = qm.getQueTypeObjName(qType);
          var _hash_str = qm.randomString();
          
          eval("var q_obj = new "+_obj_name+";");
          q_obj.parseQuestionString(qRawStr);
          q_obj.setQuestionHash(_hash_str);
          q_arr[_hash_str] = q_obj;
          q_order_arr.push(_hash_str);
        });
        
        genQuestionTableDisplay();
      }
    }
  });
});

</script>

<form name="form1" method="POST" action="index.php">

<div class="q_header">
	<h3>
		<span>&nbsp;<?=$survey_typename?>&nbsp;</span><br />
	    <?=$survey_title?>
	</h3>
  	<?=$html_tab_menu?>

</div><!-- q_header end -->
<div class="q_content">
<?=$html_instr?>
  <table class="form_table" border="0" cellpadding="0" cellspacing="0" width="100%">
    <col class="field_title" style="width:15%">
    <col class="field_c" style="width:85%">
    	
    <tr><td><?=$Lang['IES']['QuestionnaireTitle']?>:</td><td><?=$survey_title?></td></tr>
    <tr><td><?=$Lang['IES']['QuestionnaireDescription']?>:</td><td><?=$survey_desc?></td></tr>
    <!--<tr><td>Respondent:</td><td>&nbsp;</td></tr>-->
    <tr><td>&nbsp;</td><td><div id="checkQObj" /></td></tr>
  </table>
  <br />

  <!-- submit btn start -->
  <div class="edit_bottom"> 
    <span> <?=$Lang['IES']['DateLastUpdate']?> : <?=$survey_datemodified?></span>
    <p class="spacer"></p>
    
    <?=$editBtn?>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="window.close();" value=" <?=$Lang['IES']['Close']?> " />
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->

</div> <!-- q_content end -->
<input type="hidden" name="mod" value="survey" />
<input type="hidden" name="task" value="que_update" />
<input type="hidden" name="key" value="<?=$originalKey?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
//intranet_closedb();
?>