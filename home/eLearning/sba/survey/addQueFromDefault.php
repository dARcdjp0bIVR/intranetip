<?php
//modifying By 

include_once($PATH_WRT_ROOT."includes/sba/libies_dbtable.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

//intranet_auth();
//intranet_opendb();

$objIES = new libies();

$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$sql_que_type = "SUBSTRING_INDEX(REPLACE(SUBSTRING_INDEX(QuestionStr, '||', 1), '#QUE#', ''), ',', 1)";
$sql_que = "SUBSTRING_INDEX(SUBSTRING_INDEX(QuestionStr, '||', 2), '||', -1)";
$sql_opt = "SUBSTRING_INDEX(SUBSTRING_INDEX(QuestionStr, '||', -2), '||', -1)";

$cond = empty($searchText) ? "" : " AND ({$sql_que} LIKE '%".$searchText."%' OR {$sql_opt} LIKE '%".$searchText."%')";

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libies_dbtable($field, $order, $pageNo);
$sql = "SELECT {$sql_que}, ";
$sql .= "CASE {$sql_que_type} ";
foreach($ies_cfg["Questionnaire"]["Question"] AS $q_type => $q_config_arr)
{        
  $sql .= "WHEN {$q_type} THEN '{$q_config_arr[1]}' ";
}
$sql .= "END, CONCAT('<input onClick=\"document.form1.checkmaster.checked=false\" type=checkbox name=\"question_id[]\" value=', QuestionID ,'>') ";
$sql .= "FROM IES_DEFAULT_SURVEY_QUESTION ";
$sql .= "WHERE 1 and SchemeType = '{$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']}' and (DefaultQuestion <> {$ies_cfg['Questionnaire']['isDefaultQuestion']} or DefaultQuestion  is null) {$cond}";

// TABLE INFO
$LibTable->field_array = array($sql_que_type);
$LibTable->sql = $sql;
//$LibTable->title = $ec_iPortfolio['ole'];
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = 4;
$LibTable->table_tag = "<table class=\"common_table_list\">";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list = "<thead>";
$LibTable->column_list .= "<tr>";
$LibTable->column_list .= "<th width=\"5%\" class=\"row_content\">#</th>";
$LibTable->column_list .= "<th width=\"50%\" class=\"row_content\">{$Lang['IES']['Question']}</th>";
$LibTable->column_list .= "<th width=\"40%\" class=\"row_content\">{$Lang['IES']['QuestionFormat']}</th>";
$LibTable->column_list .= "<th width=\"5%\" class=\"row_content\">".$LibTable->check("question_id[]")."</th>";
$LibTable->column_list .= "</tr>";
$LibTable->column_list .= "</thead>";

$html_table_content = $LibTable->displayPlain();
$html_table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$html_table_content .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
$html_table_content .= "</table>";

$linterface = new interface_html($ies_cfg["ThickboxFrontEndInterface"]);
$linterface->LAYOUT_START();

?>

<div class="edit_pop_board">
  <br style="clear:both" />
  <form name="searchForm" method="POST" action="addQueFromDefault.php">
    <div class="content_top_tool">
      <div class="Conntent_search">
        <input name="searchText" type="text" value="<?=$html_searchText?>" />
      </div>
    </div>
  </form>

  <br style="clear:both" />
  <form name="form1" method="POST" action="addQueFromDefault.php">
    <div class="edit_pop_board_write">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td>
            <?=$html_table_content?>
          </td>
        </tr>
      </table>
      <br />
      <p class="spacer"></p>
      <div class="edit_bottom">
        <input onclick="parent.addQuestionsFromDefault(this.form);" name="ClassSubmit" id="ClassSubmit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['IES']['Submit']?>" />
        <input onclick="parent.window.tb_remove();" name="ClassCancel" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['IES']['Cancel']?>" />
      </div>
    </div>
    
    <input type="hidden" name="pageNo" value="<?= $pageNo ?>" />
    <input type="hidden" name="order" value="<?= $order ?>" />
    <input type="hidden" name="field" value="<?= $field ?>" />
    <input type="hidden" name="numPerPage" value="<?= $numPerPage ?>" />
    <input type="hidden" name="page_size_change" />
  </form>
</div>

<?php
$linterface->LAYOUT_STOP();
//intranet_closedb();
?>