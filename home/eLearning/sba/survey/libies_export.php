<?php
/** [Modification Log] Modifying By: Paul
 * *******************************************
 *
 * 2011-01-14 Thomas
 *  - Add variable $Survey_Respondent_Nature and pass to getSurveyAnsDetails()
 *  - Modified function export_stage_report(), export different declaration for stage 1 & 2
 *
 * *******************************************
 */
include_once("{$intranet_root}/includes/libexportdoc.php");

class libies_export extends libexportdoc {
	var $folder_target;

	var $header = '<html xmlns:v="urn:schemas-microsoft-com:vml"
				xmlns:o="urn:schemas-microsoft-com:office:office"
				xmlns:w="urn:schemas-microsoft-com:office:word"
				xmlns="http://www.w3.org/TR/REC-html40">

				<head>
				<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
				<meta name=ProgId content=Word.Document>
				<meta name=Generator content="Microsoft Word 9">
				<meta name=Originator content="Microsoft Word 9"></head>
				';
	var $schemeLang;
	public function libies_export($target=''){
		//do nothing now
		$this->libdb();
		$this->folder_target = $target;
	}

	public function Setfoldertarget($target){
		$this->folder_target = $target;
	}

	public function setLang($strLang){
		$this->schemeLang = $strLang;
	}
	public function getLang(){
		return $this->schemeLang;
	}

	/* For exporting Stage 1 & 2 ONLY */
	public function export_stage_report($ParStageId, $ParUserId){
		global $Lang, $ies_cfg, $intranet_root;
		$folder_target = $this->folder_target;
		$li = new libuser($ParUserId);
		$fs = new libfilesystem();

		if(!file_exists($folder_target))
		{
			$fs->folder_new($folder_target);
		}

		$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);


		$content = "";
		$html_header = $this->header;

		$docstyle = <<<EOH
		<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->

				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				<!--
				 /* Font Definitions */
				@font-face
					{font-family:Verdana;
					panose-1:2 11 6 4 3 5 4 4 2 4;
					mso-font-charset:0;
					mso-generic-font-family:swiss;
					mso-font-pitch:variable;
					mso-font-signature:536871559 0 0 0 415 0;}
				 /* Style Definitions */
				p.MsoNormal, li.MsoNormal, div.MsoNormal
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:7.5pt;
				        mso-bidi-font-size:8.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				.mytable{
					width:100%;
					border:2px solid black;
					border-collapse: separate;
					background-color: transparent;
					display: table;
				}
				.mytable th{
					background-color: #A6A6A6;
				}
				.mytable td{
					border:1px solid #A6A6A6;
				}
				.wordtable{
					border:1px solid black;
					width:98%;

				}
				p.small
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:1.0pt;
				        mso-bidi-font-size:1.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				@page WordSection1
					{size:595.3pt 841.9pt;
					margin:72.0pt 72.0pt 72.0pt 72.0pt;
					mso-header-margin:42.55pt;
					mso-footer-margin:49.6pt;
					mso-paper-source:0;
					layout-grid:18.0pt;}
				div.WordSection1
					{page:WordSection1;}
				-->
				</style>
				<!--[if gte mso 9]><xml>
				 <o:shapedefaults v:ext="edit" spidmax="1032">
				  <o:colormenu v:ext="edit" strokecolor="none"/>
				 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
				 <o:shapelayout v:ext="edit">
				  <o:idmap v:ext="edit" data="1"/>
				 </o:shapelayout></xml><![endif]-->

EOH;

		$html_TeachingTeacher = "";
		$html_AssessTeacher = "";
		$next_five = $this-> get_next_line(5);
		$this -> setstyle($docstyle);
		$this -> sethtmlheader($html_header);

		$stage_arr = $this->GET_STAGE_DETAIL($ParStageId);

		list($stage_title, $stage_sequence, $stage_deadline, $schemeID) = $stage_arr;




		$scheme_arr = $this->GET_SCHEME_DETAIL($schemeID);
		$task_arr = $this->getStageTaskArr($ParStageId);

		// Get TeachingTeacher and AssessTeacher

		$result_r = $this->get_teacher($schemeID);
		foreach($result_r as $value){
			if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
				$html_TeachingTeacher .= "{$value['TeacherName']} <br \>";
			else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
				$html_AssessTeacher .= "{$value['TeacherName']} <br \>";
		}
		if(empty($html_TeachingTeacher)){
			$html_TeachingTeacher = "--";
		}
		if(empty($html_AssessTeacher)){
			$html_AssessTeacher = "--";
		}


		# Handling user display name
		$userDisplayName =	(
								$_SESSION['IES_CURRENT_LANG'] == "en"?
								(
									empty($li->EnglishName)?
									$li->ChineseName:
									$li->EnglishName
								):
								(
									empty($li->ChineseName)?
									$li->EnglishName:
									$li->ChineseName
								)
							);
		// Content of the doc********************
		$content .="<div class=\"WordSection1\" style=\"line-spacing:1.15\">";
		//Page 1

		$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
		$content .= $this->get_center_open();
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['HKDSE'];
		$content .= $this-> get_p_close();
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['IESTitle'];

		$content .= $this-> get_p_close();

		$_stageExportDocTitle = "";
		$_stageExportDocDeclaration = '';
		switch($stage_sequence){
			case 1:
				$_stageExportDocTitle = $Lang['IES']['StageExportDoc']['string1_stage1'];
				$_stageExportDocDeclaration = $Lang['IES']['StageExportDoc']['string3'];
				break;
			case 2:
				$_stageExportDocTitle = $Lang['IES']['StageExportDoc']['string1_stage2'];
				$_stageExportDocDeclaration = $Lang['IES']['StageExportDoc']['string3_stage2'];
				break;
			default:
		}
		$content .= $_stageExportDocTitle;


		$content .= $this-> get_p_close();
		$content .= $this->get_center_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this-> get_next_line();
		$content .= $this->get_center_open();  //<center>
		$content .= $this-> get_table_open("wordtable"); //<table>
		$content .= "<tr><td>";
		$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
		$content .= $this-> get_p_open();
		$content .= $this->get_center_open().$Lang['IES']['StageExportDoc']['string2'].$this->get_center_close();
		$content .= $this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this->get_span_open("","font-size:11pt;"); //<span>
		$content .= $this-> get_p_open();
		$content .= $_stageExportDocDeclaration.$Lang['IES']['StageExportDoc']['string27'];
		$content .= $this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= "</td></tr>";
		$content .= $this-> get_table_close(); //</table>
		$content .= $this->get_center_close(); //</center>
		$content .= $this-> get_next_line();
		$content .= $this->get_span_open("","font-size:11pt;");  //<span>
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['SchoolName']."︰ ".GET_SCHOOL_NAME()."</b>".$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StudentName']."︰ "."</b>".$userDisplayName.$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['Class']."︰ "."</b>".$li->ClassName.$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['Instructor']."︰ "."</b>".$html_TeachingTeacher.$this-> get_p_close();
		//$content .= $html_TeachingTeacher;
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['DateSubmitted2']."︰ "."</b>".$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StatusSubmitted']."︰ "."</b>".$Lang['IES']['StageExportDoc']['string4'].$this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this-> get_next_line();
		//content
		$content .= $this->get_span_open("","font-size:11pt;");

		for($i=0; $i<count($task_arr); $i++)
		{
			if($this->isTaskOpen($task_arr[$i]['TaskID'],$ParUserId)){
				$title = $task_arr[$i]['Title'];
				$task_ans_content = $this->gettaskAnswer($task_arr[$i]['TaskID'], $ParUserId);
				$question_type = $this->CheckTaskAnsType($task_arr[$i]['TaskID'], $ParUserId);
				##only print out when it is not a survey type question and the answer is not empty
				if(!empty($task_ans_content) && !stristr($question_type,"survey")){


					$content .= $this-> get_p_open("","margin-left:0cm;");  //<p>
					$content .= "<b>".$title."</b>";  //TITLE
					$content .= $this-> get_p_close();  //</p>
					$content .= $this->get_center_open();   //<center>


			  		if(stristr($question_type, "table")){
			  			$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
			  			$content .= $this-> get_table_open("mytable");   //<table>
			  			//ANS
			  			$content .= $this->GetTableContent($task_arr[$i]['TaskID'], $task_ans_content);
			  			$content .= $this-> get_table_close();   //</table>
			  			$content .= $this-> get_p_close();   //</p>

			  			if ($stage_sequence == 2 && $task_arr[$i]['Sequence']==4) {
			  				$content.= "<br/><div style='text-align:left'>".$Lang['IES']['StageExportDoc']['string27']."</div>";
			  			}

			  		}
			  		else{

			  			$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
			  			$content .= $this-> get_table_open("wordtable");  //<table>
			  			$content .= "<tr><td>";
			  			$ans_ = $this->gettaskAnswer($task_arr[$i]['TaskID'], $ParUserId);
			  			$task_ans_content = $this->stringfilter($ans_);
			  			$task_ans_content = (empty($task_ans_content))?$next_five:$task_ans_content;
			  			$content .= $task_ans_content;   //ANS
			  			$content .= "</td></tr>";
			  			$content .= $this-> get_table_close();   //</table>
			  			$content .= $this-> get_p_close();   //</p>

			  		}
					$content .= $this->get_center_close();  //</center>
				}
			}
		}
		//學生聲明
		$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
		$content .= $this->get_span_close();
		$content .= "<b>".$Lang['IES']['StageExportDoc']['string5']."</b>";
		$content .= $this->get_center_open();  //<center>
		$content .= $this-> get_table_open("wordtable");  //<table>
	    $content .= "<tr><td>";
	    $content .= $this-> get_p_open(); //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string6'];
	    $content .= $this-> get_p_close(); //</p>
	    $content .= $this-> get_ul_open();   //<ul>

	    $content .= $this-> get_li_open();   //<li>
	    $content .= $this-> get_p_open();  //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string7'];
	    $content .= $this-> get_p_close();  //</p>
	    $content .= $this-> get_li_close();   //</li>


	    $content .= $this-> get_li_open();   //<li>
	    $content .= $this-> get_p_open();  //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string8'];
	    $content .= $this-> get_p_close();  //</p>
	    $content .= $this-> get_li_close();   //</li>


	    $content .= $this-> get_li_open();   //<li>
	    $content .= $this-> get_p_open();  //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string9'];
	    $content .= $this-> get_p_close(); //</p>
	    $content .= $this-> get_li_close();   //</li>

	    $content .= $this-> get_ul_close();   //</ul>
	  	$content .= $this-> get_next_line();
	    $content .= $this-> get_p_open(); //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string10']."___________________________ 　　".$Lang['IES']['StageExportDoc']['string11']."_________________________";
	    $content .= $this-> get_p_close(); //</p>
	    $content .= "</td></tr>";
	  	$content .= $this-> get_table_close();    //</table>
	  	$content .= $this->get_center_open();  //</center>
	  	$content .= $this-> get_p_close();    //</p>

		$content .="</div>";
		//****end of doc content
		$file_name = $this->filename_safe($scheme_arr[0].".doc");
		$file_name = mb_convert_encoding($file_name ,"big5","utf8");
		$file_content = $this -> createDoc($content, $file_name, false);
		$fs-> file_remove("{$folder_target}/$file_name");


		return $fs -> file_write($file_content, "{$folder_target}/$file_name");

	}

	/* For export Stage 3 ONLY */
	public function export_stage_3_report($ParStageId, $ParUserId){
		global $Lang, $ies_cfg, $intranet_root;
		$folder_target = $this->folder_target;
		$li = new libuser($ParUserId);
		$fs = new libfilesystem();

		if(!file_exists($folder_target))
		{
			$fs->folder_new($folder_target);
		}

		$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);


		$content = "";
		$html_header = $this->header;

		$docstyle = <<<EOH
		<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->

				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				<!--
				 /* Font Definitions */
				@font-face
					{font-family:Verdana;
					panose-1:2 11 6 4 3 5 4 4 2 4;
					mso-font-charset:0;
					mso-generic-font-family:swiss;
					mso-font-pitch:variable;
					mso-font-signature:536871559 0 0 0 415 0;}
				 /* Style Definitions */
				p.MsoNormal, li.MsoNormal, div.MsoNormal
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:7.5pt;
				        mso-bidi-font-size:8.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				.mytable{
					width:100%;
					border:2px solid black;
					border-collapse: separate;
					background-color: transparent;
					display: table;
				}
				.mytable th{
					background-color: #A6A6A6;
				}
				.mytable td{
					border:1px solid #A6A6A6;
				}
				.wordtable{
					border:1px solid black;
					width:98%;

				}
				.content_table{
					width:98%;
				}
				.content_title{
					border-bottom:1px solid black;
					border-left:1px solid black;
					width:80%;
				}
				.content_page_no{
					border-bottom:1px solid black;
					border-left:1px solid black;
					border-right:1px solid black;
					width:20%;
				}
				p.small
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:1.0pt;
				        mso-bidi-font-size:1.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				@page WordSection1
					{size:595.3pt 841.9pt;
					margin:72.0pt 72.0pt 72.0pt 72.0pt;
					mso-header-margin:42.55pt;
					mso-footer-margin:49.6pt;
					mso-paper-source:0;
					layout-grid:18.0pt;}
				div.WordSection1
					{page:WordSection1;}
				-->
				</style>
				<!--[if gte mso 9]><xml>
				 <o:shapedefaults v:ext="edit" spidmax="1032">
				  <o:colormenu v:ext="edit" strokecolor="none"/>
				 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
				 <o:shapelayout v:ext="edit">
				  <o:idmap v:ext="edit" data="1"/>
				 </o:shapelayout></xml><![endif]-->

EOH;

		$html_TeachingTeacher = "";
		$html_AssessTeacher = "";
		$next_five = $this-> get_next_line(5);
		$this -> setstyle($docstyle);
		$this -> sethtmlheader($html_header);

		$stage_arr = $this->GET_STAGE_DETAIL($ParStageId);

		list($stage_title, $stage_sequence, $stage_deadline, $schemeID) = $stage_arr;




		$scheme_arr = $this->GET_SCHEME_DETAIL($schemeID);
		//$task_arr = $this->getStageTaskArr($ParStageId);

		// Get TeachingTeacher and AssessTeacher

		$result_r = $this->get_teacher($schemeID);
		foreach($result_r as $value){
			if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
				$html_TeachingTeacher .= "{$value['TeacherName']} <br \>";
			else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
				$html_AssessTeacher .= "{$value['TeacherName']} <br \>";
		}
		if(empty($html_TeachingTeacher)){
			$html_TeachingTeacher = "--";
		}
		if(empty($html_AssessTeacher)){
			$html_AssessTeacher = "--";
		}


		# Handling user display name
		$userDisplayName =	(
								$_SESSION['IES_CURRENT_LANG'] == "en"?
								(
									empty($li->EnglishName)?
									$li->ChineseName:
									$li->EnglishName
								):
								(
									empty($li->ChineseName)?
									$li->EnglishName:
									$li->ChineseName
								)
							);
		// Content of the doc********************
		$content .="<div class=\"WordSection1\" style=\"line-spacing:1.15\">";
		//Page 1

		$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
		$content .= $this->get_center_open();
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['HKDSE'];
		$content .= $this-> get_p_close();
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['StageExportDoc']['string1_stage3'];
		$content .= $this-> get_p_close();
		$content .= $this->get_center_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this-> get_next_line();
		$content .= $this->get_center_open();  //<center>
		$content .= $this-> get_table_open("wordtable"); //<table>
		$content .= "<tr><td>";
		$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
		$content .= $this-> get_p_open();
		$content .= $this->get_center_open().$Lang['IES']['StageExportDoc']['string2'].$this->get_center_close();
		$content .= $this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this->get_span_open("","font-size:11pt;"); //<span>
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['StageExportDoc']['string13'];
		$content .= $this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= "</td></tr>";
		$content .= $this-> get_table_close(); //</table>
		$content .= $this->get_center_close(); //</center>
		$content .= $this-> get_next_line();
		$content .= $this->get_span_open("","font-size:11pt;");  //<span>
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['SchoolName']."︰ "."</b>".$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StudentName']."︰ "."</b>".$userDisplayName.$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['Class']."︰ "."</b>".$li->ClassName.$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['Instructor']."︰ "."</b>".$html_TeachingTeacher.$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['DateSubmitted2']."︰ "."</b>".$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StatusSubmitted']."︰ "."</b>".$Lang['IES']['StageExportDoc']['string4'].$this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this-> get_next_line();

		$content .= $this-> get_p_open("","margin-left:0cm;");  //<p>
		$content .= "<b>".$Lang['IES']['StageExportDoc']['string14']."</b>";  //TITLE
		$content .= $this-> get_p_close();  //</p>
		$content .= $this->get_center_open();   //<center>
		$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
		$content .= $this-> get_table_open("wordtable");  //<table>
		$content .= "<tr><td>";

		$target_taskID = $this->getTaskIDByTaskInfo($schemeID, 2, "topic");
		$ans_ = $this->gettaskAnswer($target_taskID, $ParUserId);
		$task_ans_content = $this->stringfilter($ans_);
		$task_ans_content = (empty($task_ans_content))?$next_five:$task_ans_content;

		$content .= $task_ans_content;   //ANS
		$content .= "</td></tr>";
		$content .= $this-> get_table_close();   //</table>
		$content .= $this-> get_p_close();   //</p>

		$section_arr = $this->GET_SECTION_ARR($ParStageId);

		$content .= $this->get_page_break();
		// Table of Content
		$content .= $this->get_center_open();   //<center>
		$content .= $this->get_p_open()."<b>".$Lang['IES']['StageExportDoc']['TableOfContent']."</b>".$this->get_p_close();
		$content .= $this->get_table_open("content_table");
		$content .= "<tr>";
		$content .= 	"<td class='content_title' style='border-top:1px solid black'></td>";
		$content .= 	"<td class='content_page_no' style='border-top:1px solid black'>";
		$content .=			"<b>".$Lang['IES']['StageExportDoc']['Page']."</b>";
		$content .= 	"</td>";
		$content .= "</tr>";
		for($j=0, $j_max=count($section_arr); $j<$j_max;$j++){
//			$chapter = $Lang['IES']['StageExportDoc']['string15'].$this->getChineseNumbering($j+1).$Lang['IES']['StageExportDoc']['string16'];
			$chapter = $this->getNumbering($j+1);

			$content .= "<tr><td class='content_title'><b>$chapter : ".$section_arr[$j]["Title"]."</b></td><td class='content_page_no'></td></tr>";
		}
		$content .= $this->get_table_close();
		$content .= $this->get_center_close();  //</center>
		$content .= $this->get_page_break();

		//content
		$content .= $this->get_span_open("","font-size:11pt;");

		for($j=0, $j_max=count($section_arr); $j<$j_max; $j++)
		{
			$_sectionID    = $section_arr[$j]["SectionID"];
			$_sectionTitle = $section_arr[$j]["Title"];

//			$chapter = $Lang['IES']['StageExportDoc']['string15'].$this->getChineseNumbering($j+1).$Lang['IES']['StageExportDoc']['string16'];
			$chapter = $this->getNumbering($j+1);

			$content .= $this->get_center_open();   //<center>
			$content .= $this-> get_p_open()."<b>$chapter".$this->get_next_line().$section_arr[$j]["Title"]."</b>".$this-> get_p_close();
			$content .= $this->get_center_close();  //</center>

			$task_arr = $this->GET_SECTION_TASK_ARR($_sectionID);

			for($i=0; $i<count($task_arr); $i++)
			{
				if($this->isTaskOpen($task_arr[$i]['TaskID'],$ParUserId)){
					$title = $task_arr[$i]['Title'];
					$task_ans_content = $this->gettaskAnswer($task_arr[$i]['TaskID'], $ParUserId);
					$question_type = $this->CheckTaskAnsType($task_arr[$i]['TaskID'], $ParUserId);
					##only print out when it is not a survey type question and the answer is not empty
					if(!empty($task_ans_content) && !stristr($question_type,"survey")){

						$content .= "<b>".$title."</b>";  //TITLE
						$content .= $this->get_center_open();   //<center>


				  		if(stristr($question_type, "table")){
				  			$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
				  			$content .= $this-> get_table_open("mytable");   //<table>
				  			//ANS
				  			$content .= $this->GetTableContent($task_arr[$i]['TaskID'], $task_ans_content);
				  			$content .= $this-> get_table_close();   //</table>
				  			$content .= $this-> get_p_close();   //</p>

				  			/*
				  			if ($stage_sequence == 2 && $task_arr[$i]['Sequence']==4) {
				  				$content.= "<br/><div style='text-align:left'>".$Lang['IES']['StageExportDoc']['string27']."</div>";
				  			}
				  			*/

				  		}
				  		else{

				  			$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
				  			$content .= $this-> get_table_open("wordtable");  //<table>
				  			$content .= "<tr><td>";
				  			$ans_ = $this->gettaskAnswer($task_arr[$i]['TaskID'], $ParUserId);
				  			$task_ans_content = $this->stringfilter($ans_);
				  			$task_ans_content = (empty($task_ans_content))?$next_five:$task_ans_content;
				  			$content .= $task_ans_content;   //ANS
				  			$content .= "</td></tr>";
				  			$content .= $this-> get_table_close();   //</table>
				  			$content .= $this-> get_p_close();   //</p>

				  		}
						$content .= $this->get_center_close();  //</center>
					}
				}
			}
			//$content .= $this->get_page_break();
		}
		//學生聲明
		$content .= $this-> get_p_open("","margin-left:0cm;");   //<p>
		$content .= $this->get_span_close();
		$content .= "<b>".$Lang['IES']['StageExportDoc']['string5']."</b>";
		$content .= $this->get_center_open();  //<center>
		$content .= $this-> get_table_open("wordtable");  //<table>
	    $content .= "<tr><td>";
	    $content .= $this-> get_p_open(); //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string6'];
	    $content .= $this-> get_p_close(); //</p>
	    $content .= $this-> get_ul_open();   //<ul>

	    $content .= $this-> get_li_open();   //<li>
	    $content .= $this-> get_p_open();  //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string7'];
	    $content .= $this-> get_p_close();  //</p>
	    $content .= $this-> get_li_close();   //</li>


	    $content .= $this-> get_li_open();   //<li>
	    $content .= $this-> get_p_open();  //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string8'];
	    $content .= $this-> get_p_close();  //</p>
	    $content .= $this-> get_li_close();   //</li>


	    $content .= $this-> get_li_open();   //<li>
	    $content .= $this-> get_p_open();  //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string9'];
	    $content .= $this-> get_p_close(); //</p>
	    $content .= $this-> get_li_close();   //</li>

	    $content .= $this-> get_ul_close();   //</ul>
	  	$content .= $this-> get_next_line();
	    $content .= $this-> get_p_open(); //<p>
	    $content .= $Lang['IES']['StageExportDoc']['string10']."___________________________ 　　".$Lang['IES']['StageExportDoc']['string11']."_________________________";
	    $content .= $this-> get_p_close(); //</p>
	    $content .= "</td></tr>";
	  	$content .= $this-> get_table_close();    //</table>
	  	$content .= $this->get_center_open();  //</center>
	  	$content .= $this-> get_p_close();    //</p>

		$content .="</div>";
		//****end of doc content
		$file_name = $this->filename_safe($scheme_arr[0].".doc");
		$file_name = mb_convert_encoding($file_name ,"big5","utf8");
		$file_content = $this -> createDoc($content, $file_name, false);
		$fs-> file_remove("{$folder_target}/$file_name");


		return $fs -> file_write($file_content, "{$folder_target}/$file_name");

	}

	public function GetTableContent($PartaskId, $ParTaskAnswer){
		global $ies_cfg, $Lang, $intranet_root;

		$content = "";
		$html_task_content = nl2br($ParTaskAnswer);
		$parentIds = current($this->getParentsIds("TASK",$PartaskId));
		$stepNoSaveToTask = $this->getMaxStepNoSaveToTask($PartaskId);
		# $_question[1] is get from here
		include_once "$intranet_root/home/eLearning/ies/coursework/templates/default/{$parentIds["STAGE_SEQ"]}/{$stepNoSaveToTask}/model.php";

		##header
		$content .= "<tr><th>#</th><th>".implode("</th><th>",$_question[1])."</th></tr>";

		$sizeOfQuestionHeader = count($_question[1]);
  		$colSpan = $sizeOfQuestionHeader+1;
  		$questionInput = "";

  		$TaskArray = explode($ies_cfg["new_set_question_answer_sperator"], $ParTaskAnswer);

  		$num_count = 0;
  		if(is_array){
  			foreach($TaskArray as $an_answer){
  				$num_count++;
  				$answerarray = explode($ies_cfg["question_answer_sperator"], $an_answer);
  				if(is_array($answerarray)){
  					$content .= "<tr><td>{$num_count}</td>";
  					foreach($answerarray as $col){
  						$content .= "<td>$col</td>";
  					}
  					$content .= "</tr>";
  				}
  			}
  		}
  		return $content;

	}


	public function isTaskOpen($parTaskID, $parUserID)
	{
	  global $ies_cfg, $intranet_db;



		$sql = "SELECT Approved FROM {$intranet_db}.IES_TASK_STUDENT WHERE TaskID = '{$parTaskID}' AND UserID = '{$parUserID}'";
		$taskOpened = (current($this->returnVector($sql)) == $ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"]);

		if(!$taskOpened)
		{
	    $sql =  "SELECT Approval FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$parTaskID}'";
	    $taskOpened = (current($this->returnVector($sql)) == 0);
	  }

	  return $taskOpened;
	}

	####FUNCTION FOR WORD QUESTION PRINT
	/**
	 * For export word for ies survey question
	 *
	 * $SurveyID -> INT, SURVEY ID
	 * $save2tmp -> boolean , if true then save to tmp, otherwise, output2browser
	 */
	public function QuestionPrintForWord($SurveyID, $save2tmp = false){
		global $ies_cfg, $Lang;
		$folder_target = $this->folder_target;
		$header = $this->header;


		##############################################
		# get the Survey Details
		$SurveyDetails = $this->getSurveyDetails($SurveyID);
		$SurveyDetails = current($SurveyDetails);
		//DEBUG_R($SurveyDetails);
		$Survey_question = $SurveyDetails["Question"];
		$SurveyStartDate = $SurveyDetails["DateStart"];
		$SurveyEndDate = $SurveyDetails["DateEnd"];
		$Survey_title = $SurveyDetails["Title"];
		$Survey_desc = $SurveyDetails["Description"];
		$Survey_type = $SurveyDetails["SurveyType"];

		$Survey_remark1 = $SurveyDetails["Remark1"];
		$Survey_remark2 = $SurveyDetails["Remark2"];
		$Survey_remark3 = $SurveyDetails["Remark3"];
		$Survey_remark4 = $SurveyDetails["Remark4"];

		$displayOtherRemarkFlag = true;
		if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
			$displayOtherRemarkFlag = false;
		}

		if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){
			$remarkCaption1 = $Lang['IES']['Questionnaire_InterviewRemark1'];
			$remarkCaption2 = $Lang['IES']['Questionnaire_InterviewRemark2'];
			$remarkCaption3 = $Lang['IES']['Questionnaire_InterviewRemark3'];
			$remarkCaption4 = $Lang['IES']['Questionnaire_InterviewRemark4'];

		}else{
			$remarkCaption1 = $Lang['IES']['Questionnaire_ObserveRemark1'];
			$remarkCaption2 = $Lang['IES']['Questionnaire_ObserveRemark2'];
			$remarkCaption3 = $Lang['IES']['Questionnaire_ObserveRemark3'];
			$remarkCaption4 = $Lang['IES']['Questionnaire_ObserveRemark4'];

		}

		$question_array = $this->breakQuestionsString($Survey_question);
		#################

		//***********************************CONTENT****************************************
		$content .= $header;
		$content .= "<span style='font-style:Times New Roman;'>";
		$content .= "<h3>{$Lang['IES']['QuestionnaireTitle']}:".$Survey_title."</h3>";
		$content .= "<h3>{$Lang['IES']['QuestionnaireDescription']}:".$Survey_desc."</h3>";
		if($displayOtherRemarkFlag){
			$content .= "<h3>{$remarkCaption1}:".$Survey_remark1."</h3>";
			$content .= "<h3>{$remarkCaption2}:".$Survey_remark2."</h3>";
			$content .= "<h3>{$remarkCaption3}:".$Survey_remark3."</h3>";
			$content .= "<h3>{$remarkCaption4}:".$Survey_remark4."</h3>";
		}
		$content .= "<h3>{$Lang['IES']['Respondent']}: ________________________</h3>";

		if(!empty($question_array)){
			$count = 1;
			foreach($question_array as $value){

				$content .= "<b>".$count.". ".$value["TITLE"]."</b><br />";
				switch($value["TYPE"]){

					case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:

						$content .= $this->GetMCAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:

						$content .= $this->GetMPAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:

						$content .= $this->GetShortAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:

						$content .= $this->GetLongAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:

						$content .= $this->GetTableLikertAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

		//				$content .= GetTableInputAnsDisplay($value);

						break;

				}
				$content .= "<br />";
				$count++;
			}
		}
		$content .= "</span>";

		if($save2tmp){
			$fs = new libfilesystem();

			if(!file_exists($folder_target))
			{
				$fs->folder_new($folder_target);
			}
			$header = $this->header;

			$file_name = $this->filename_safe($Survey_title."_questionaire.doc");
			$file_name = mb_convert_encoding($file_name ,"big5","utf8");
			$fs-> file_remove("{$folder_target}/$file_name");
			return $fs -> file_write($header.$content, "{$folder_target}/$file_name");
		}
		else{
			return $content;
		}
	}

	public function GetMCAnsDisplay($ParArry){
		global $image_path;
		$x = "";

		for($i = 0; $i < $ParArry["OPTION_NUM"]; $i++){
			$x .= "&#9675; ".$ParArry["OPTIONS"][$i]."<br />";
		}
		return $x;
	}

	public function GetMPAnsDisplay($ParArry){
		global $ies_cfg;
		$x = "";

		for($i = 0; $i < $ParArry["OPTION_NUM"]; $i++){

				$x .= "&#9744; ".$ParArry["OPTIONS"][$i]."<br />";
		}
			return $x;
		}

	public function GetShortAnsDisplay($ParArry){
			global $ies_cfg;
			$x = "_________________________________________________________________________<br />";
		return $x;
	}

	public function GetLongAnsDisplay($ParArry){
		global $ies_cfg;
		for($i = 0; $i < 5; $i++){
			$x .= "_________________________________________________________________________<br />";
		}

		return $x;
	}

	public function GetTableLikertAnsDisplay($ParArry){
		global $ies_cfg;
		$x = "";

		$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" width='100%'><tr><td style='border:1px solid black;'>Question</td>";

	    for($i=0; $i < $ParArry["OPTION_NUM"]; $i++)
		{
			$x .= "<td style='border:1px solid black;'>".$ParArry["OPTIONS"][$i]."</td>";
		}
		$x .= "</tr>";
		for($i = 0; $i < $ParArry["QUESTION_NUM"]; $i++){
			$x .= "<tr>";
			$x .= "<td style='border:1px solid black;'>".$ParArry["QUESTION_QUESTIONS"][$i]."</td>";
			for($j = 0; $j < $ParArry["OPTION_NUM"]; $j++){
				$x .= "<td style='border:1px solid black; text-align:center;'>&#9675;</td>";

			}
			$x .= "</tr>";

		}
		$x .= "</table>";
		return $x;
	}

	##to export Individual Report
	public function exportAnaylsisreport($ParSurveyID, $ParUserId){
		global $Lang;
		$folder_target = $this->folder_target;
		$header = $this->header;

		$style = <<<html
		<style>
		table{
			width:100%;
			border-collapse:collapse;
		}
		table tr th{
			background-color:grey;
			border-style:solid;
			border-width:1px;
			border-color:black;
		}
		table tr td{
			border-style:solid;
			border-width:1px;
			border-color:black;
		}
		</style>
html;
		$content = $style;
		##get survey details
		$SurveyDetails = $this->getSurveyDetails($ParSurveyID);
		$SurveyDetails = current($SurveyDetails);
		//DEBUG_R($SurveyDetails);
		$Survey_question = $SurveyDetails["Question"];
		$Survey_title = $SurveyDetails["Title"];
		$Survey_desc = $SurveyDetails["Description"];

		#######OverALL PART
		$content .= "<h3>Overall Anaylsis</h3>";
		$questionsArray = $this->breakQuestionsString($Survey_question);
		$this->set_display_survey_mapping_option(false);
		for($i = 1; $i <= sizeof($questionsArray) ; $i++){
			$content .= $this->GetIndividualAnswerDisplay($ParSurveyID, $i);
		}
		if (empty($content)) {
			$content .= $Lang['IES']['NoRecordAtThisMoment'];
		}
		$surveydetail_print .= "<h3>{$Lang['IES']['QuestionnaireTitle']} : {$Survey_title}</h3>";
		$surveydetail_print .= "<h3>{$Lang['IES']['QuestionnaireDescription']} : {$Survey_desc}</h3>";
		$content = $surveydetail_print.$content."<br clear=\"all\" style=\"page-break-before:always\" />";
		##############

		######MAPPING ANAYLSIS PART####
		$SurveyMapping = $this->loadSurveyMapping($ParSurveyID,$ParUserId);
		if(is_array($SurveyMapping)){
			$content .= "<h3>Group Anaylsis</h3>";
			foreach($SurveyMapping as $value){
				$SurveyID = $value['SURVEYID'];
				$XAxis = $value['XMAPPING'];
				$YAxis = $value['YMAPPING'];
				$mappingtitle = $value['MAPPINGTITLE'];
				$content .= "<b>{$mappingtitle}<b><br />";
				$content .= $this->getCombineBoxContent($SurveyID,$XAxis,$YAxis)."<br />";

			}
		}
		#############



		##save the file here;
		$fs = new libfilesystem();

			if(!file_exists($folder_target))
			{
				$fs->folder_new($folder_target);
			}
		$file_name = $this->filename_safe($Survey_title."_analysis.doc");
		$file_name = mb_convert_encoding($file_name ,"big5","utf8");
		$fs-> file_remove("{$folder_target}/$file_name");
		return $fs -> file_write($header.$content, "{$folder_target}/$file_name");
	}

	public function export_SurveyResult($ParSurveyID){
		global $Lang, $ies_cfg;
		include_once 'libies_ui.php';

		$style = <<<html
		<style>
		table{
			width:100%;

		}
		table th{
			background-color: grey;
			border-style:solid;
			border-width:1px;
			border-color:black;
		}
		table tr td{
			border-style:solid;
			border-width:1px;
			border-color:black;
		}
		</style>
html;
		$content .= $style;
		$RespondentList = $this->getSurveyAnsweredRespondentList($ParSurveyID);
		if(is_array($RespondentList)){

			###Survey Details
			$SurveyDetails = $this->getSurveyDetails($ParSurveyID);
			$SurveyDetails = current($SurveyDetails);
			$Survey_title = $SurveyDetails["Title"];
			$Survey_desc = $SurveyDetails["Description"];
			$Survey_question = $SurveyDetails["Question"];
			$Survey_startdate = date("Y-m-d", strtotime($SurveyDetails["DateStart"]));
			$Survey_enddate = date("Y-m-d", strtotime($SurveyDetails["DateEnd"]));
			$html_TypeName = $SurveyDetails["SurveyTypeName"];
			#########################################

			foreach($RespondentList as $value){
				$Survey_Respondent = $value['Respondent'];
				$Survey_Respondent_Nature = $value['RespondentNature'];
				$Survey_Valid = $value['Status'];
				if($Survey_Valid == $ies_cfg['DB_IES_SURVEY_ANSWER_status']['Valid']){
					$survey_ans_detail_arr = $this->getSurveyAnsDetails($ParSurveyID, $Survey_Respondent, $Survey_Respondent_Nature);

					$Survey_Answer = $survey_ans_detail_arr["Answer"];
					$Survey_DateInput = $survey_ans_detail_arr["DateInput"];
					$Survey_Validity = $survey_ans_detail_arr["Status"];
					$Survey_DateModified = date("Y-m-d", strtotime($survey_ans_detail_arr["DateModified"]));

					$AnsArry = $this->splitQuestionnaireAnswers($Survey_question, $Survey_Answer);
					###content##
					$content .= "<h3>{$Lang['IES']['Respondent']} : {$Survey_Respondent}</h3>";
					$content .= "<h3>{$Lang['IES']['QuestionnaireDescription']} : {$Survey_desc}</h3>";
					$content .= "<h3>{$Lang['IES']['InterviewDate']} : {$Survey_DateModified}</h3>";
					$content .= $this->getSurveyResultprint($AnsArry);
					$content .= "<br clear=\"all\" style=\"page-break-before:always\" />";
				}
			}
			$folder_target = $this->folder_target;
			$header = $this->header;
			$fs = new libfilesystem();

			if(!file_exists($folder_target))
			{
				$fs->folder_new($folder_target);
			}
			$file_name = $this->filename_safe($Survey_title."_survey.doc");
			$file_name = mb_convert_encoding($file_name ,"big5","utf8");
			$fs-> file_remove("{$folder_target}/$file_name");
			return $fs -> file_write($header.$content, "{$folder_target}/$file_name");

		}
	}

	######################
	###Get Result Printing
	public function getSurveyResultprint($ParAnsArry){
		global $ies_cfg;
		if(!empty($ParAnsArry)){
			$count = 1;
			foreach($ParAnsArry as $value){

				$HTML_ANS .= "<b>".$count.". ".$value["QUESTION"]["TITLE"]."</b><br />";
				switch($value["QUESTION"]["TYPE"]){

					case $ies_cfg["Questionnaire"]["QuestionType"]["MC"]:

						$HTML_ANS .= $this->GetMCResultAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["MultiMC"]:

						$HTML_ANS .= $this->GetMPResultAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]:

						$HTML_ANS .= $this->GetShortResultAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["FillLong"]:

						$HTML_ANS .= $this->GetLongResultAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["LikertScale"]:

						$HTML_ANS .= $this->GetTableLikertResultAnsDisplay($value);
						break;
					case $ies_cfg["Questionnaire"]["QuestionType"]["Table"]:

//						$HTML_ANS .= $this->GetTableResultInputAnsDisplay($value);

						break;

				}
				$HTML_ANS .= "<br >";
				$count++;
			}
		}
		return $HTML_ANS;
	}

	public function GetMCResultAnsDisplay($ParArry){
		$x = "";

		for($i = 0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++){
			if($i == $ParArry["ANSWER"] && $ParArry["ANSWER"] != ""){
				$x .= "&#10687; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
			else{
				$x .= "&#9675; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
		}
		return $x;
	}

	public function GetMPResultAnsDisplay($ParArry){
		global $ies_cfg;
		$x = "";
		$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);
		for($i = 0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++){
			if($AnsArry[$i] == 1){
				$x .= "&#9745; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
			else{
				$x .= "&#9744; ".$ParArry["QUESTION"]["OPTIONS"][$i]."<br />";
			}
		}
		return $x;
	}

	public function GetShortResultAnsDisplay($ParArry){
		global $ies_cfg;
		$x = "<br/>".$ParArry["ANSWER"]."<br/>";
		return $x;
	}

	public function GetLongResultAnsDisplay($ParArry){
		global $ies_cfg;
		$x = "<br/>".nl2br($ParArry["ANSWER"])."<br/>";
		return $x;
	}

	public function GetTableLikertResultAnsDisplay($ParArry){
		global $ies_cfg;
		$x = "";
		$AnsArry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $ParArry["ANSWER"]);

		$x .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" ><tr><td></td>";

	    for($i=0; $i < $ParArry["QUESTION"]["OPTION_NUM"]; $i++)
		{
			$x .= "<td>".$ParArry["QUESTION"]["OPTIONS"][$i]."</td>";
		}
		$x .= "</tr>";
		for($i = 0; $i < $ParArry["QUESTION"]["QUESTION_NUM"]; $i++){
			$x .= "<tr>";
			$x .= "<td>".$ParArry["QUESTION"]["QUESTION_QUESTIONS"][$i]."</td>";
			for($j = 0; $j < $ParArry["QUESTION"]["OPTION_NUM"]; $j++){
	//			DEBUG_R($AnsArry[$i]);
				if($AnsArry[$i] == $j && $AnsArry[$i] != ""){
					$x .= "<td>&#10687;</td>";
				}
				else{
					$x .= "<td>&#9675;</td>";
				}
			}
			$x .= "</tr>";

		}
		$x .= "</table>";
		return $x;
	}


	public function DocSectionPrintForWord($ParUserID,$ParStageID,$ParSave2Tmp) {
		## get tasks answers
		$answerArray = $this->getDocSectionDetailsRefined($ParStageID);


		global $Lang, $ies_cfg, $intranet_root, $intranet_db;
		$folder_target = $this->folder_target;
		$li = new libuser($ParUserID);
		$fs = new libfilesystem();

		if(!file_exists($folder_target))
		{
			$fs->folder_new($folder_target);
		}

		$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);


		$content = "";
		$html_header = $this->header;

		$docstyle = <<<EOH
		<!--[if !mso]>
				<style>
				v\:* {behavior:url(#default#VML);}
				o\:* {behavior:url(#default#VML);}
				w\:* {behavior:url(#default#VML);}
				.shape {behavior:url(#default#VML);}
				</style>
				<![endif]-->

				<!--[if gte mso 9]><xml>
				 <w:WordDocument>
				  <w:View>Print</w:View>
				  <w:DoNotHyphenateCaps/>
				  <w:PunctuationKerning/>
				  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
				  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
				 </w:WordDocument>
				</xml><![endif]-->
				<style>
				<!--
				 /* Font Definitions */
				@font-face
					{font-family:Verdana;
					panose-1:2 11 6 4 3 5 4 4 2 4;
					mso-font-charset:0;
					mso-generic-font-family:swiss;
					mso-font-pitch:variable;
					mso-font-signature:536871559 0 0 0 415 0;}
				 /* Style Definitions */
				p.MsoNormal, li.MsoNormal, div.MsoNormal
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:7.5pt;
				        mso-bidi-font-size:8.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				.mytable{
					width:100%;
					border:2px solid black;
					border-collapse: separate;
					background-color: transparent;
					display: table;
				}
				.mytable th{
					background-color: #A6A6A6;
				}
				.mytable td{
					border:1px solid #A6A6A6;
				}
				.wordtable{
					border:1px solid black;
					width:98%;

				}
				p.small
					{mso-style-parent:"";
					margin:0in;
					margin-bottom:.0001pt;
					mso-pagination:widow-orphan;
					font-size:1.0pt;
				        mso-bidi-font-size:1.0pt;
					font-family:"Verdana";
					mso-fareast-font-family:"Verdana";}
				@page WordSection1
					{size:595.3pt 841.9pt;
					margin:72.0pt 72.0pt 72.0pt 72.0pt;
					mso-header-margin:42.55pt;
					mso-footer-margin:49.6pt;
					mso-paper-source:0;
					layout-grid:18.0pt;}
				div.WordSection1
					{page:WordSection1;}
				-->
				</style>
				<!--[if gte mso 9]><xml>
				 <o:shapedefaults v:ext="edit" spidmax="1032">
				  <o:colormenu v:ext="edit" strokecolor="none"/>
				 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
				 <o:shapelayout v:ext="edit">
				  <o:idmap v:ext="edit" data="1"/>
				 </o:shapelayout></xml><![endif]-->

EOH;

		$html_TeachingTeacher = "";
		$html_AssessTeacher = "";
		$next_five = $this-> get_next_line(5);
		$this -> setstyle($docstyle);
		$this -> sethtmlheader($html_header);

		$stage_arr = $this->GET_STAGE_DETAIL($ParStageID);

		list($stage_title, $stage_sequence, $stage_deadline, $schemeID) = $stage_arr;
//		$scheme_arr = $this->GET_SCHEME_DETAIL($schemeID);
//		$task_arr = $this->getStageTaskArr($ParUserID);

		// Get TeachingTeacher and AssessTeacher

		$result_r = $this->get_teacher($schemeID);
		foreach($result_r as $value){
			if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
				$html_TeachingTeacher .= "{$value['TeacherName']} <br \>";
			else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
				$html_AssessTeacher .= "{$value['TeacherName']} <br \>";
		}
		if(empty($html_TeachingTeacher)){
			$html_TeachingTeacher = "--";
		}
		if(empty($html_AssessTeacher)){
			$html_AssessTeacher = "--";
		}

		// Content of the doc********************
		$content .="<div class=\"WordSection1\" style=\"line-spacing:1.15\">";
		//Page 1

		$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
		$content .= $this->get_center_open();
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['StageExportDoc']['string20'];
		$content .= $this-> get_next_line();
		$content .= $Lang['IES']['StageExportDoc']['string21'];
		$content .= $this->get_center_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this-> get_next_line();
		$content .= $this->get_center_open();  //<center>
		$content .= $this-> get_table_open("wordtable"); //<table>
		$content .= "<tr><td>";
		$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
		$content .= $this-> get_p_open();
		$content .= $this->get_center_open().$Lang['IES']['StageExportDoc']['string12'].$this->get_center_close();
		$content .= $this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= $this->get_span_open("","font-size:11pt;"); //<span>
		$content .= $this-> get_p_open();
		$content .= $Lang['IES']['StageExportDoc']['string13'];
		$content .= $this-> get_p_close();
		$content .= $this->get_span_close();  //</span>
		$content .= "</td></tr>";
		$content .= $this-> get_table_close(); //</table>
		$content .= $this->get_center_close(); //</center>
		$content .= $this-> get_next_line();
		$content .= $this->get_span_open("","font-size:11pt;");  //<span>
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StageExportDoc']['string22']."︰ ___________________________________"."</b>".$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StageExportDoc']['string23']."︰__<u>".$li->UserName()."</u>__ ".$Lang['IES']['StageExportDoc']['string25']."︰ __<u>".$li->ClassName."</u>__"."</b>".$this-> get_p_close();
		$content .= $this-> get_p_open()."<b>".$Lang['IES']['StageExportDoc']['string24']."︰ __<u>$html_TeachingTeacher</u>__ ".$Lang['IES']['StageExportDoc']['string26']."︰_________________ "."</b>".$this-> get_p_close();
		$content .= $this->get_span_close();  //</span>

		$content .= $this-> get_next_line();
		$content .= $this-> get_next_line();
		$content .= $this-> get_next_line();
		$content .= $this-> get_next_line();
		$content .= $this-> get_next_line();


		$sql = "SELECT ITH.ANSWER
FROM {$intranet_db}.IES_TASK_HANDIN ITH
INNER JOIN {$intranet_db}.IES_TASK IT
	ON ITH.TASKID = IT.TASKID
INNER JOIN {$intranet_db}.IES_STAGE IST
	ON IT.STAGEID = IST.STAGEID
INNER JOIN {$intranet_db}.IES_SCHEME ISC
	ON IST.SCHEMEID = ISC.SCHEMEID
WHERE ISC.SCHEMEID = '{$schemeID}'
	AND IST.SEQUENCE = 2
	AND IT.SEQUENCE = 1";
		$topic = current($this->returnVector($sql));


		$content .= $this->get_span_open("","font-size:11pt;");	//<span>
		$content .= $Lang['IES']['StageExportDoc']['string14'];
		$content .= $this->get_center_open();  //<center>
		$content .= $this-> get_table_open("wordtable"); //<table>
		$content .= "<tr><td>";
		$content .= $topic;
		$content .= "</td></tr>";
		$content .= $this-> get_table_close(); //</table>
		$content .= $this->get_center_close(); //</center>
		 $content .= $this->get_span_close();  //</span>

		$content .= $this->get_page_break();
		//content

//		debug_r($answerArray);
		 if (is_array($answerArray)) {
		 	$TOC = array();
		 	$sizeOfAnswer = count($answerArray);
		 	$content .= "{{{ TOC }}}";
		 	###################################### -- Chapter
		 	foreach($answerArray as $key => $element) {
				$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
				$content .= $this->get_center_open();
				$content .= $this-> get_p_open();
//				$chapter = $Lang['IES']['StageExportDoc']['string15'].$this->getChineseNumbering($key).$Lang['IES']['StageExportDoc']['string16'];
				$chapter = $this->getNumbering($key);
				$content .= $chapter;
				$content .= $this-> get_next_line();
				$content .= $element["SECT_TITLE"];
				$content .= $this->get_center_close();
				$content .= $this-> get_next_line(2);

				$TOC[] = $chapter."：".$element["SECT_TITLE"];

				###################################### -- Task element
				if (is_array($element["TASK_DETAIL"])) {
					foreach($element["TASK_DETAIL"] as $dKey => $dElement) {
//						debug_r($dElement);
						$content .= $dElement["TASK_TITLE"];
						$content .= $this-> get_table_open("wordtable"); //<table>
							$content .= "<tr><td>";
								$content .= $this->get_span_open("","font-size:11pt;");
								$content .= $this-> get_p_open();
									$content .= $dElement["TASK_ANS"];
								$content .= $this-> get_p_close();
								$content .= $this->get_span_close();  //</span>
							$content .= "</td></tr>";
						$content .= $this-> get_table_close(); //</table>
						$content .= $this-> get_next_line(2);
					}
//					if ($sizeOfAnswer>$key) {
						$content .= $this->get_page_break();
//					}
				}


				$content .= $this->get_span_close();  //</span>
		 	}
			$reference = $Lang['IES']['StageExportDoc']['string17'];
		 	$TOC[] .= $reference;
			$content .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
			$content .= $this->get_center_open();
			$content .= $this-> get_p_open();
			$content .= $reference;
			$content .= $this-> get_p_close();
			$content .= $this->get_center_close();
			$content .= $this->get_span_close();  //</span>
			$content .= $this-> get_next_line(2);
		 }



		$content .="</div>";

		 ###################################### -- TOC
		 if (is_array($TOC)) {
			$w_TOC .= $this->get_span_open("","font-size:11pt; font-weight:bold;");
			$w_TOC .= $this->get_center_open();
			$w_TOC .= $this-> get_p_open();
			$w_TOC .= $Lang['IES']['StageExportDoc']['string18'];
			$w_TOC .= $this-> get_p_close();
			$w_TOC .= $this->get_center_close();
			$w_TOC .= $this->get_span_close();
			$w_TOC .= $this-> get_next_line(2);

		 	$w_TOC .= $this-> get_table_open("","width:100%"); //<table>
		 	$w_TOC .= "<tr><td>&nbsp;</td><td>".$Lang['IES']['StageExportDoc']['string19']."</td></tr>";
		 	foreach($TOC as $key => $words) {
				$w_TOC .= "<tr><td>".$words."</td><td>&nbsp;</td></tr>";
		 	}
		 	$w_TOC .= $this-> get_table_close();
		 	$w_TOC .= $this->get_page_break();
		 }
//		 debug_r(htmlspecialchars($w_TOC));
		$content = str_replace("{{{ TOC }}}",$w_TOC,$content);

		//****end of doc content
		$file_name = $this->filename_safe("DocSection.doc");
		$file_name = mb_convert_encoding($file_name ,"big5","utf8");
		$file_content = $this -> createDoc($content, $file_name, false);
		$fs-> file_remove("{$folder_target}/$file_name");


		return $fs -> file_write($file_content, "{$folder_target}/$file_name");
	}
	##end function of result printing
	############################

	public function filename_safe($name) {
	    $except = array('\\', '/', ':', '*', '?', '"', '<', '>', '|', ' ');
	    return str_replace($except, '_', $name);
	}

	public function getNumbering($ParNumber){
		$returnValue = ($this->getLang() == strtolower("en")) ? $this->getEnglishNumbering($ParNumber):$this->getChineseNumbering($ParNumber);
		return $returnValue;
	}
	public function getChineseNumbering($ParNumber) {
		global $Lang;
		$ParNumber = intval($ParNumber);
		$NUMBERS = array("零","一","二","三","四","五","六","七","八","九"
						,"十","十一","十二","十三","十四","十五","十六","十七","十八","十九");
		return $Lang['IES']['StageExportDoc']['string15'].$NUMBERS[$ParNumber].$Lang['IES']['StageExportDoc']['string16'];
	}

	public function getEnglishNumbering($ParNumber) {
		global $Lang;
		return $Lang['IES']['StageExportDoc']['string16']." ".$ParNumber;
	}
}

?>