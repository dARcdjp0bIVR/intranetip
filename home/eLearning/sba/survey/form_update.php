<?php
$TaskID = intval($TaskID);
$StepID = intval($StepID);

$li = new libdb();

$remark1 = mysql_real_escape_string(stripslashes($remark1));
$remark2 = mysql_real_escape_string(stripslashes($remark2));
$remark3 = mysql_real_escape_string(stripslashes($remark3));
$remark4 = mysql_real_escape_string(stripslashes($remark4));

//hard code 
//$scheme_id = 685;
$stepQuestionType = '';
switch($SurveyType){
	default:
	case $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]:
		$stepQuestionType = $sba_cfg['DB_IES_STEP_QuestionType']['surveyEdit'][0];
		break;
	case  $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]:
		$stepQuestionType = $sba_cfg['DB_IES_STEP_QuestionType']['interviewEdit'][0];
		break;
	case $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]:
		$stepQuestionType = $sba_cfg['DB_IES_STEP_QuestionType']['observeEdit'][0];
		break;
}

$dec_SurveyID = base64_decode($SurveyID);
if(is_numeric($dec_SurveyID))
{
  $sql = "UPDATE {$intranet_db}.IES_SURVEY ";
  $sql .= "SET ";
  $sql .= "Title = '".mysql_real_escape_string(stripslashes($title))."', ";
  $sql .= "Description = '".mysql_real_escape_string(stripslashes($description))."', ";
  $sql .= "Remark1 = '".$remark1."',";
  $sql .= "Remark2 = '".$remark2."',";
  $sql .= "Remark3 = '".$remark3."',";
  $sql .= "Remark4 = '".$remark4."',";
  $sql .= "DateStart = '{$startdate} 00:00:00', ";
  $sql .= "DateEnd = '{$enddate} 23:59:59', ";
  $sql .= "ModifiedBy = {$UserID}, ";
  $sql .= "DateModified = NOW() ";
  $sql .= "WHERE SurveyID = {$dec_SurveyID}";
  $li->db_db_query($sql);
  
  $msg = "update";
}
else
{
  $sql =  "INSERT INTO {$intranet_db}.IES_SURVEY ";
  $sql .= "(Title, Description, Remark1, Remark2,Remark3,Remark4,DateStart, DateEnd, SurveyType, SurveyStatus, InputBy, DateInput, ModifiedBy, DateModified,UserID,SchemeID) ";
  $sql .= "VALUES ";
  $sql .= "('".mysql_real_escape_string(stripslashes($title))."', '".mysql_real_escape_string(stripslashes($description))."', '".$remark1."','".$remark2."', '".$remark3."','".$remark4."','{$startdate} 00:00:00', '{$enddate} 23:59:59', {$SurveyType}, 1, {$sba_thisUserID}, NOW(), {$sba_thisUserID}, NOW(),{$sba_thisStudentID},{$scheme_id})";
  $li->db_db_query($sql);

  $dec_SurveyID = $li->db_insert_id();

  $SurveyID = base64_encode($dec_SurveyID);

	$objIES = new libies();

	$survey_map = NULL;
	$answerStrTmp = array();

	//GET THE STUDENT EXISTING NO OF SURVEY FROM STORAGE
	$survey_map = $objIES->getStudentStepSurvey($StepID, $sba_thisStudentID,$stepQuestionType);

	$existingSurveyIdAry = array();

	if($survey_map == ''){
		//do nothing
	}else{
      $existingSurveyIdAry = explode($ies_cfg["answer_within_question_sperator"],$survey_map);
	}

	$existingSurveyIdAry[] = $dec_SurveyID;

	$answerStr = '';
	if(is_array($existingSurveyIdAry) && count($existingSurveyIdAry) >0){
		$answerStr = implode($ies_cfg["answer_within_question_sperator"],$existingSurveyIdAry);
	}


  $sql  = 'INSERT INTO '.$intranet_db.'.IES_QUESTION_HANDIN ';
  $sql .= '(StepID,UserID,Answer,QuestionType,QuestionCode,DateInput,InputBy,DateModified,ModifyBy)';
  $sql .= 'VALUES ';
  $sql .= '('.$StepID.','.$sba_thisStudentID.', \''.$answerStr.'\', \''.$stepQuestionType.'\', \''.$stepQuestionType.'\', NOW(), '.$sba_thisUserID.',NOW(),'.$sba_thisUserID.')';
  $sql .= 'ON DUPLICATE KEY UPDATE Answer = \''.$answerStr.'\' , ModifyBy = '.$sba_thisUserID;

  $li->db_db_query($sql);
  $msg = "add";
}


//http://192.168.0.146:31002/home/eLearning/sba/?mod=survey&task=createForm&TaskID=17238&SurveyType=1
//$url = "createForm.php?SurveyID={$SurveyID}&msg={$msg}";
//&StepID='.$this->getStepID().'&TaskID='.$this->getTaskID().'&SurveyType='.$SurveyType.'
$url = "?mod=survey&task=createForm&SurveyID={$SurveyID}&msg={$msg}&StepID={$StepID}&TaskID={$TaskID}&SurveyType=".$SurveyType;

intranet_closedb();
?>

<script language="JavaScript">
	window.opener.task_content_introStep_tabMenu_onclick(<?=$TaskID?>, 0, 0, <?=$StepID?>)
	self.location = "<?=$url?>";
</script>
