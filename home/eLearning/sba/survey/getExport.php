<?php
/*
 * Modification Log:
 * 2019-07-15 (Pun) [ip.2.5.10.10.1]
 *  - Added remove folder/file that created 7 days before today
 * 2019-05-17 (Paul) [ip.2.5.10.6.1]
 *  - Handling download attachment path to be using encrypted param target_e
 */
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/sba/libSbaExport.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$fs = new libfilesystem();
$export_directory = $intranet_root.'/file'.$sba_cfg['SBA_EXPORT_STUDENTHANDIN_ROOT_PATH']."u_".$sba_thisStudentID.'/survey/';
$surveyID=$_SESSION['thisExportSurveyID'];

// Remove file older then 7 days
//system("find ".escapeshellarg($export_directory)." | xargs touch -t 200001010101"); //TEST SCRIPT
system("find ".escapeshellarg($export_directory)." -mtime +7 | xargs rm -rf");

if ($type=='survey'){
    
    $dirname="survey";
    $libSbaExport= new libSbaExport($export_directory.$dirname,$sba_thisStudentID);
    $libSbaExport->exportQuestionCSV($surveyID);
    $ZipFileName = "survey_u".$sba_thisStudentID.".zip";
}else if ($type=='surveyMapping'){//mapping: comparing 2 questions
    
    $surveyMappingID=$_SESSION['thisExportSurveyMappingIDs'];
    $dirname="surveyMapping";
    $libSbaExport= new libSbaExport($export_directory.$dirname,$sba_thisStudentID);
    $libSbaExport->exportMappingCSV($surveyID,$surveyMappingID);
    $ZipFileName = "surveyMapping_u".$sba_thisStudentID.".zip";
}

$exportFilePath = $export_directory.'/'.$ZipFileName;
$target_e = getEncryptedText($exportFilePath);

$fs->file_remove($export_directory.$ZipFileName);//remove old file
$fs->file_zip($dirname, $ZipFileName, $export_directory);

header("Location: /home/download_attachment.php?target_e=".$target_e);
