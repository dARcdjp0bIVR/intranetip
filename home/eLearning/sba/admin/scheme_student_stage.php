<?php
//modifying By 
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");

include_once("common.php");

$libies_ui = new libies_ui();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";

// Get teacher type of scheme
// 1D-Array format, since teacher may have more than one identity in scheme
$teacher_type_arr = libies_static::getSchemeTeacherType($scheme_id, $sba_thisUserID);

$objIES = new libies();


$scheme_detail_arr = $objIES->GET_SCHEME_DETAIL($scheme_id);
$scheme_stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$stage_detail_arr = $objIES->GET_STAGE_DETAIL($stage_id);
//$task_arr = $objIES->GET_TASK_ARR($stage_id);

$objTaskMapper = new SbaTaskMapper();
$taskObjAry = $objTaskMapper->findByStageId($stage_id);

// Stage Tab
$tab_arr = array();
for($i=0, $i_max=count($scheme_stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $scheme_stage_arr[$i]["StageID"];
  $t_stage_title = $scheme_stage_arr[$i]["Title"];

  $tab_arr["tab{$t_stage_id}"] = array("index.php?mod=admin&task=scheme_student_stage&scheme_id={$scheme_id}&stage_id={$t_stage_id}&StudentID={$StudentID}", $t_stage_title);
}
//$tab_arr["tab"] = array("scheme_student_final.php?SchemeID={$SchemeID}&StageID=&StudentID={$StudentID}", $Lang['IES']['StageSubmitStatus']);
//$tab_arr["tab"] = array("index.php?mod=admin&task=scheme_stage_final&scheme_id={$scheme_id}&stage_id=", $Lang['IES']['StageSubmitStatus']);


$tab_arr["tab"] = array("?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id=", $Lang['IES']['StageSubmitStatus']);

$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab{$stage_id}");

// Student Selection
$student_arr = libies_static::getSchemeStudentArr($scheme_id);
$html_student_selection = getSelectByArray($student_arr, "name=\"StudentID\"", $StudentID, 0, 1, "", 2);

$SchemeTitle = $scheme_detail_arr["Title"];

$html_content = "";
$html_js_array ="";
//for($i=0, $i_max=count($task_arr); $i<$i_max; $i++)
for($i=0, $i_max=count($taskObjAry); $i<$i_max; $i++)
{
	/*
	  $t_task_id = $task_arr[$i]["TaskID"];
  $t_title = $task_arr[$i]["Title"];
*/
	$_taskObj = $taskObjAry[$i];
	$t_task_id = $_taskObj->getTaskID();
    $t_title = $_taskObj->getTitle();

   $t_step_count = count($objIES->GET_STEP_ARR($t_task_id));
//   $t_is_enable = $objIES->isTaskEnable($t_task_id);
	$t_is_enable = $_taskObj->getEnable();
  
//  if(!$t_is_enable) continue;
  if(!$t_is_enable) $t_title .= ' ('.$Lang['IES']['NotReleased'].')';
  // check if task is survey (limited to student has submitted task handin)
  $t_taskType = $objIES->CheckTaskAnsType($t_task_id);
  $t_taskIsSurvey = (strpos(strtoupper($t_taskType), "SURVEY") !== false);
  
  // check if current answer exist in snapshot
  $t_currentStep_included = isCurrentAnswerInSnapshot($t_task_id, $StudentID);
  // get snapshot array
  $displayAnswerArr = getTaskSnapshotAnswer($t_task_id, $StudentID);
   
  $sub_content_arr = array();
  if(!$t_currentStep_included)
  {
    // prepare for display current task answer
    $curAnswerArr = getTaskAnswer($t_task_id, $StudentID);
    
    if(!empty($curAnswerArr))
    {
      array_unshift($displayAnswerArr, $curAnswerArr);
    }
  }
  $sub_content_arr = libies_static::getTaskAnswerStrArr($displayAnswerArr, $teacher_type_arr, $stage_id, '', $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);

  $next_task_id = libies_static::getNextTask($t_task_id);
  $next_task_need_allowance = libies_static::isTaskNeedAllowance($next_task_id);

  $html_approval_content = "";
  if(isset($next_task_id) && $next_task_need_allowance)
  {
    $t_task_status = libies_static::isTaskAllowed($next_task_id, $StudentID);
    $html_approval_content = $t_task_status ? "<span class=\"IES_allow\">{$Lang['IES']['AllowedToNextStep']}</span>" : "<input type=\"button\" name=\"approve\" student_id=\"{$StudentID}\" task_id=\"{$next_task_id}\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['AllowToNextStep']}\" />";
  }

  // count row span
  $rowspan_count = count($sub_content_arr);
  $html_js_array .= "span_array[{$i}]={$rowspan_count};";
  
  // Step thickbox link
  $html_step_thickbox_link = "<a href=\"index.php?mod=admin&task=step_number_thick_box&KeepThis=true&task_id=$t_task_id&StudentID=$StudentID&TB_iframe=true&height=400&width=700\" class=\"thickbox view_step\" title=\"{$Lang['IES']['StudentInputData']}\">&nbsp;</a>";
  	
  // generate display string
  $html_content .= "<tr>";
  $html_content .= "<td class=\"title column_$i\" >".$t_title."<br /><br />{$html_approval_content}<br \>";
//  if(count($sub_content_arr) > 1){
//  	$html_content .= "<a href=\"javascript:ShowTable($i)\"\">+</a>";
//  }
  	
  $html_content .=	"</td>";
  $html_content .= "<td class=\"student_ans column_$i\" >".$t_step_count."</td>";

  for($j=0, $j_max=count($sub_content_arr); $j<$j_max; $j++)
  {
    $html_content .= ($j>0) ? "<tr class=\"display_$i\" style=\"display:none\">" : "";
    
    // show step answer, but not for task type "survey"
    $html_content .= (!$t_taskIsSurvey && $j==0) ? preg_replace("/(<td class=\"student_ans\">)(.*)(<\/td>)(<td class=\"teacher_comment\">.*)/Us", "$1<div style=\"float:left; width: 95%;\">$2</div><div style=\"float:left; width:5%;\">{$html_step_thickbox_link}</div>$3$4 ", $sub_content_arr[$j], 1) : $sub_content_arr[$j];
    $html_content .= "</tr>";
  }
}

$MenuArr = array();
$MenuArr[] = array($ies_lang['StudentProgress'], ".");
$MenuArr[] = array($SchemeTitle, "index.php?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id={$stage_id}");
$MenuArr[] = array($html_student_selection, "");
$html_navigation = $linterface->GET_NAVIGATION($MenuArr);

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/scheme_student_stage.tmpl.php");
$linterface->LAYOUT_STOP();

?>
