<?php
//modifying By : 

include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/sba/libies.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");


$linterface = new interface_html();
$CurrentPage = "SchemeSettings";

$li = new libuser($sba_thisUserID);
$objIES = new libies();


$fcm = new form_class_manage();
$fcm_ui = new form_class_manage_ui();
$TeacherList = $fcm->Get_Teaching_Staff_List();


$html_teaching_teacher_selection .= '<select name="TeachingTeacherList" id="TeachingTeacherList" onchange="Add_Teaching_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$html_teaching_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$html_teaching_teacher_selection .= '</select>';

$html_assess_teacher_selection .= '<select name="AssessTeacherList" id="AssessTeacherList" onchange="Add_Assess_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$html_assess_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$html_assess_teacher_selection .= '</select>';

$html_student_search_input = '<div class="Conntent_search">';
$html_student_search_input .= '<input type="text" id="StudentSearch" name="StudentSearch" value="" />';
$html_student_search_input .= '</div>';

// year class selection
$lclass = new libclass();
$thisAttr = ' id="YearClassSelect" name="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();" ';
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$CurrentYearClassSelection = $lclass->getSelectClassID($thisAttr, $selected="", $DisplaySelect=1, $CurrentAcademicYearID);

$html_year_class_selection = '<div id="CurrentYearClassSelectionDiv">';
$html_year_class_selection .= $CurrentYearClassSelection;
$html_year_class_selection .= '</div>';

// class student list of selected class
$html_student_source_selection = '<div id="ClassStudentList">';
$html_student_source_selection .= '<select name="ClassStudent" id="ClassStudent" size="10" multiple="true">';
$html_student_source_selection .= '</select>';
$html_student_source_selection .= '</div>';
		

//      									$x .= '<div id="ClassStudentList">';
//      										$x .= $fcm_ui->Get_Student_Without_Class_Selection('ClassStudent');
//      									$x .= '</div>';
		
// selected student list
$html_student_target_selection = '<select name="StudentSelected[]" id="StudentSelected" size="10" multiple="true">';
$html_student_target_selection .= '</select>';


$h_RadioLang = $sba_libSba->getSchemeLangRadio();

###
## status
###

$h_schemeStatus = '<input type="radio" name="r_SchemeRecordStatus" id="id_publishIsChecked" value="'.$ies_cfg["recordStatus_approved"].'"/><label for="id_publishIsChecked">'.$Lang['IES']['RecordStatusPublish'].'</label> <input type="radio" name="r_SchemeRecordStatus" id="id_nonPublishIsCheck" value="'.$ies_cfg["recordStatus_rejected"].'" checked/><label for="id_nonPublishIsCheck">'.$Lang['IES']['RecordStatusNotPublish'].'</label>';

###############################################################
##	Start: Scheme Lang Handling
###############################################################
//$h_RadioLang = libies::getSchemeLangRadio();
###############################################################
##	End: Scheme Lang Handling
###############################################################



###############################################################
##	Start: Notes
###############################################################
/*
$notes_display = '<ul>
					<li>'.$Lang['IES']['NewSchemeNotes']['SchemeLanguageCanOnlyBeSetOnce'].'</li>
					<!--li>'.$Lang['IES']['NewSchemeNotes']['Stage1Functionalities'].'</li-->
				</ul>';
$h_notes = $linterface->Get_Warning_Message_Box($Lang['IES']['Remark'], $notes_display, "");
*/
###############################################################
##	End: Notes
###############################################################


// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "index.php?mod=admin&task=settings_scheme_index");
$nav_arr[] = array($Lang['SBA']['NewScheme'], "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);

### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/settings_scheme_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
