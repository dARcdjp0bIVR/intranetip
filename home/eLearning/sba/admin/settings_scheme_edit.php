<?php

//modifying By : 
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));

if(empty($schemeID) || !is_int($schemeID))
{
	echo "Invalide access , Empty SCHEME ID<br/>";
    exit();
}

$html_schemeID = $schemeID;  // FOR DISPLAY HIDDEN FORM FIELD VALUE


$objIES = new libies();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$objDB = new libdb();
$li = new libuser($UserID);

$objDB_UI = new libies_ui();


//$schemeDetails = $objIES->GET_SCHEME_DETAIL($schemeID);

$objSbaScheme = new SbaScheme($schemeID);

###############################################################
##	Start: Scheme Lang Handling
###############################################################
$h_RadioLang = $sba_libSba->getSchemeLangRadio($objSbaScheme->getLanguage());
###############################################################
##	End: Scheme Lang Handling
###############################################################

########################
## start : status
########################

$publishStatus = $objSbaScheme->getRecordStatus();

if($publishStatus == $ies_cfg["recordStatus_approved"]){
	$publishIsChecked = ' checked '; 
	$nonPublishIsCheck = '';
}else{
	$publishIsChecked = ''; 
	$nonPublishIsCheck = ' checked ';
}
$h_schemeStatus = '<input type="radio" name="r_SchemeRecordStatus" id="id_publishIsChecked" value="'.$ies_cfg["recordStatus_approved"].'" '.$publishIsChecked.'/><label for="id_publishIsChecked">'.$Lang['IES']['RecordStatusPublish'].'</label> <input type="radio" name="r_SchemeRecordStatus" id="id_nonPublishIsCheck" value="'.$ies_cfg["recordStatus_rejected"].'" '.$nonPublishIsCheck.'/><label for="id_nonPublishIsCheck">'.$Lang['IES']['RecordStatusNotPublish'].'</label>';

//$h_schemeTitle = $schemeDetails["SchemeType"];
//if the result set (schemeDetails) not equal to one and or is not a array, should have error. Exit the program
/*if(sizeof($schemeDetails) != 1 || !is_array($schemeDetails))
{
	echo "SYSTEM ERROR, SCHEME ID NOT FIND<br/>";
    exit();
}*/
//$schemeTitle =$schemeDetails["Title"];
$schemeTitle = $objSbaScheme->getTitle();

$html_schemeTitle = $schemeTitle;


$schemeIntroduction = $objSbaScheme->getIntroduction();

$html_schemeIntro = $schemeIntroduction;

//get scheme teacher
$dbNameField = getNameFieldWithClassNumberByLang("i.");
$sql = "select
			ist.userid as 'ist_UserID',
			ist.TeacherType as 'TeacherType',
			i.userid as 'i_UserID',
			{$dbNameField} as 'TeacherName'
		from
			IES_SCHEME_TEACHER as ist inner join  /*1) start from IES_SCHEME_TEACHER 2) 'inner join' userid exist in IES_SCHEME_TEACHER must exist in INTRANET_USER*/
			INTRANET_USER as i on ist.Userid = i.Userid
		where 
			SchemeID = {$schemeID}
		";


$assignedTeacherList = $objDB->returnArray($sql);

$html_selected_teaching_teacher = "";  // STORE THE HTML DISPLAY OF THE SELECTED "TEACHERING" TEACHER
$html_selected_assess_teacher = "";   // STORE THE HTML DISPLAY OF THE SELECTED "ASSESS" TEACHER

//HANDLE TEACHER
if(is_array($assignedTeacherList))
{
	for($i =0;$i < sizeof($assignedTeacherList);$i++)
	{
		$_teacherName = $assignedTeacherList[$i]["TeacherName"];
		$_teacherType = $assignedTeacherList[$i]["TeacherType"];
		$_teacherUID = $assignedTeacherList[$i]["i_UserID"];
		if($_teacherType == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
		{
			$html_selected_teaching_teacher .= "<option value=\"{$_teacherUID}\">{$_teacherName}</option>\n";
		}else
		{ // should be equal to $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"]
			$html_selected_assess_teacher .= "<option value=\"{$_teacherUID}\">{$_teacherName}</option>\n";
		}

	}
	
}


//HANDLE STUDENT
$html_selected_selectedStudent = ""; // STORE THE HTML DISPLAY OF THE SELECTED STUDENT
$sql = "select
			i.UserID as 'UserID', 
			SchemeStudentID,
			SchemeID,
			{$dbNameField} as 'StudentName'
		from 
				IES_SCHEME_STUDENT as iss  /*start get student from IES_SCHEME_STUDENT*/
			inner join					   /*inner join , userid exist in iss must be exist in i*/ 
				INTRANET_USER as i on iss.userid = i.userid
		where 
			schemeid = {$schemeID}
		";
//debug_r($sql);
$assignedStudentList = $objDB->returnArray($sql);

if(is_array($assignedStudentList) && sizeof($assignedStudentList)>0) 
{
	for($i = 0;$i < sizeof($assignedStudentList);$i++)
	{
		$_studentUID = $assignedStudentList[$i]["UserID"];
		$_studentName = $assignedStudentList[$i]["StudentName"];

		$html_selected_selectedStudent .= "<option value=\"{$_studentUID}\">{$_studentName}</option>";
	}

}

$fcm = new form_class_manage();
$fcm_ui = new form_class_manage_ui();



//HANDLE TEACHING TEACHER LIST
$TeacherList = $fcm->Get_Teaching_Staff_List();
$html_teaching_teacher_selection .= '<select name="TeachingTeacherList" id="TeachingTeacherList" onchange="Add_Teaching_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$html_teaching_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$html_teaching_teacher_selection .= '</select>';
//END of HANDLE TEACHING TEACHER LIST


//HANDLE ASSESSING TEACHER LIST
$html_assess_teacher_selection .= '<select name="AssessTeacherList" id="AssessTeacherList" onchange="Add_Assess_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$html_assess_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$html_assess_teacher_selection .= '</select>';
//END OF ASSESSING TEACHER LIST


$html_student_search_input = '<div class="Conntent_search">';
$html_student_search_input .= '<input type="text" id="StudentSearch" name="StudentSearch" value="" />';
$html_student_search_input .= '</div>';


// year class selection
$lclass = new libclass();
$thisAttr = ' id="YearClassSelect" name="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();" ';
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
$CurrentYearClassSelection = $lclass->getSelectClassID($thisAttr, $selected="", $DisplaySelect=1, $CurrentAcademicYearID);
$html_year_class_selection = '<div id="CurrentYearClassSelectionDiv">';
$html_year_class_selection .= $CurrentYearClassSelection;
$html_year_class_selection .= '</div>';


// class student list of selected class
$html_student_source_selection = '<div id="ClassStudentList">';
$html_student_source_selection .= '<select name="ClassStudent" id="ClassStudent" size="10" multiple="true">';
$html_student_source_selection .= '</select>';
$html_student_source_selection .= '</div>';
		

//      									$x .= '<div id="ClassStudentList">';
//      										$x .= $fcm_ui->Get_Student_Without_Class_Selection('ClassStudent');
//      									$x .= '</div>';
		
// selected student list
$html_student_target_selection = '<select name="StudentSelected[]" id="StudentSelected" size="10" multiple="true">';
$html_student_target_selection .= '</select>';

//FUNCTION TAB HANDLING
$currentTagFunction = "editScheme";
$tabArray = $ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"];

$tabAppendParameter = "&schemeID={$schemeID}";
$newTab = $objIES->appendParameterToTab($tabAppendParameter,$tabArray);
$html_functionTab = $objIES->GET_TAB_MENU($currentTagFunction,$newTab);	

// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "index.php?mod=admin&task=settings_scheme_index");
$nav_arr[] = array($html_schemeTitle, "");




$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";
### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/settings_scheme_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
