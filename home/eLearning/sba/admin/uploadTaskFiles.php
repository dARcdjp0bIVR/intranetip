<?php
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$libfilesystem = new libfilesystem();
$finalUploadResult = false;
$resultCreatedFolder = false;

		//$libSba->addTaskAttachment($task_id,'s',$origFileName, $targetFileHashName, $UserID);
		$tempFileStates = '';
if (!empty($_FILES)) {
	$targetPath = "/file/sba/scheme_attachment/scheme_s$scheme_id/";
	# create folder
	$targetFullPath = str_replace('//', '/', $intranet_root.$targetPath);
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$targetMidWayPath = "/file/sba/scheme_attachment/";
		$targetMidWayPath = str_replace('//', '/', $intranet_root.$targetMidWayPath);
		if(!file_exists($targetMidWayPath)){
			$libfilesystem->folder_new($targetMidWayPath);
		}
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
	}
	
	chmod($targetFullPath, 0755);
	
	$numOfFiles = count($_FILES['attachments']['name']);
	
	for($i=0;$i<$numOfFiles;$i++) {
		# get file name
		$tempFile = $_FILES['attachments']['tmp_name'][$i];
		$origFileName =  $_FILES['attachments']['name'][$i];
		if (empty($origFileName)) continue;
		
		# encode file name
		$targetFileHashName = sha1($task_id.microtime());
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# upload file
		$uploadResult = move_uploaded_file($tempFile, $targetFile);
		
		
		# add record to DB
		$libSba = new libSba();
		$libSba->addTaskAttachment($task_id,$targetPath,$origFileName, $targetFileHashName, $UserID);
	}
	
	if($i==$numOfFiles)
		echo "<script>parent.task_intro_edit_form_submit(".$task_id.")</script>";
}else{
		echo "<script>parent.task_intro_edit_form_submit(".$task_id.")</script>";
}
?>