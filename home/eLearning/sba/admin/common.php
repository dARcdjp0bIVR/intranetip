<?php
/**
 * Modifying By: Max
 */
function isCurrentAnswerInSnapshot($TaskID, $StudentID){
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT count(*) FROM {$intranet_db}.IES_TASK_HANDIN handin ";
  $sql .= "INNER JOIN {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT snapshot ";
  $sql .= "ON handin.TaskID = snapshot.TaskID AND handin.UserID = snapshot.UserID AND UNIX_TIMESTAMP(handin.DateModified) = UNIX_TIMESTAMP(snapshot.AnswerTime) ";
  $sql .= "WHERE handin.TaskID = '{$TaskID}' AND handin.UserID = '{$StudentID}'";
//  debug_r($sql);
  $tmp_count = current($ldb->returnVector($sql));

  return ($tmp_count > 0);
}

function getTaskSnapshotAnswer($TaskID, $StudentID){
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT SnapshotAnswerID, TaskID, UserID, Answer, QuestionType, DATE_FORMAT(AnswerTime, '%d-%m-%Y') AS AnswerDate FROM {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT WHERE TaskID = '{$TaskID}' AND UserID = '{$StudentID}' ORDER BY AnswerTime DESC";
	//echo $sql.'<br><br>';

  $taskSnapshotAnswerArr = $ldb->returnArray($sql);
  return $taskSnapshotAnswerArr;
}

function getTaskAnswer($TaskID, $StudentID){
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT TaskID, UserID, Answer, DATE_FORMAT(DateModified, '%d-%m-%Y') AS AnswerDate, QuestionType FROM {$intranet_db}.IES_TASK_HANDIN WHERE TaskID = '{$TaskID}' AND UserID = '{$StudentID}'";
//  debug_r($sql);
  $taskAnswerArr = current($ldb->returnArray($sql));

  return $taskAnswerArr;
}

function getCommentArr($SnapshotID){
  global $intranet_db;

  $ldb = new libdb();

  $sql = "SELECT comment.Comment, DATE_FORMAT(comment.DateInput, '%d-%m-%Y') AS DateInput, ".getNameFieldByLang("iu.")." AS CommentTeacher FROM {$intranet_db}.IES_TASK_HANDIN_COMMENT AS comment INNER JOIN {$intranet_db}.INTRANET_USER iu ON iu.UserID = comment.InputBy WHERE comment.SnapshotAnswerID = '{$SnapshotID}'";
  $snapshotCommentArr = $ldb->returnArray($sql);

  return $snapshotCommentArr;
}

function isSchemeDenied($parSchemeID, $parUserID){
  $parSchemeID = intval(trim($parSchemeID));

  return (empty($parSchemeID) || !is_int($parSchemeID));
}

?>