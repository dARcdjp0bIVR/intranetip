<?php
//modifying By 

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/sba/libies.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");

//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));


if(empty($schemeID) || !is_int($schemeID))
{
	echo "Invalide access , Empty SCHEME ID<br/>";
    exit();
    //$schemeID = 0;
    
}

$objIES = new libies();

//create scheme_lang.php which saves the detail of the scheme
$folder_path = "{$intranet_root}/file/sba/lang/scheme/{$schemeID}/";
$objsystem = new libfilesystem();
$objsystem->folder_new($folder_path);

$file_path = $folder_path."scheme_lang.php";




$content = "<?php\n";
if(!empty($folder_desc)){
	
	$folder_desc = addslashes(intranet_htmlspecialchars(trim($folder_desc)));
	$content .= "\$Lang_cus['IES']['FolderDesc'] =\"{$folder_desc}\";\n";	
}	
else {
	$content .= "\$Lang_cus['IES']['FolderDesc'] =\"\";\n";
}
if(!empty($text_desc)){
	$text_desc =  addslashes(intranet_htmlspecialchars($text_desc)); 
	$content .= "\$Lang_cus['IES']['TextDesc'] =\"{$text_desc}\";\n";
}
else {
	$content .= "\$Lang_cus['IES']['TextDesc'] =\"\";\n";
}
if(!empty($note_desc)){
	$note_desc =  addslashes(intranet_htmlspecialchars($note_desc)); 
	$content .= "\$Lang_cus['IES']['NoteDesc'] =\"{$note_desc}\";\n";
}
else {
	$content .= "\$Lang_cus['IES']['NoteDesc'] =\"\";\n";
}
	
if(!empty($worksheet_desc)){
	$worksheet_desc =  addslashes(intranet_htmlspecialchars($worksheet_desc));
	$content .= "\$Lang_cus['IES']['WorksheetDesc'] =\"{$worksheet_desc}\";\n";
}
else {
	$content .= "\$Lang_cus['IES']['WorksheetDesc'] =\"\";\n";
}
if(!empty($faq_desc)){
	$faq_desc =  addslashes(intranet_htmlspecialchars($faq_desc));
	$content .= "\$Lang_cus['IES']['FAQDesc'] =\"{$faq_desc}\";\n";
}
else {
	$content .= "\$Lang_cus['IES']['FAQDesc'] =\"\";\n";
}

$content .= "?>";

if($objsystem ->file_write($content, $file_path))
	$msg = "add";
else
	$msg = "add_failed";

header("Location: index.php?mod=admin&task=scheme_edit_details&schemeID=".$schemeID."&msg=".$msg);
?>