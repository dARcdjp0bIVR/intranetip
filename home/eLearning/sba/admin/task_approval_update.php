<?php

$sba_libSba->Start_Trans();

$sql = "INSERT INTO {$intranet_db}.IES_TASK_STUDENT ";
$sql .= "(TaskID, UserID, Approved, ApprovedBy, DateInput, InputBy, ModifyBy) VALUES ";
$sql .= "({$task_id}, {$student_id}, {$ies_cfg["DB_IES_TASK_STUDENT_Approved"]["approved"]}, {$sba_thisUserID}, NOW(), {$sba_thisUserID}, {$sba_thisUserID}) ";
$sql .= "ON DUPLICATE KEY UPDATE Approved = VALUES(Approved), ApprovedBy = VALUES(ApprovedBy), ApprovedBy = VALUES(ApprovedBy), ModifyBy = VALUES(ModifyBy)";
$res = $sba_libSba->db_db_query($sql);

if($res)
{
  $sba_libSba->Commit_Trans();
  echo 1;
}
else
{
  $sba_libSba->RollBack_Trans();
  echo 0;
}

?>