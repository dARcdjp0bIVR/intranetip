<?
#Using: Paul
/*
debug_r($r_schemeID);
debug_r($r_schemeName);
*/
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


if(is_int(intval($r_schemeID))){
	
	$newSchemeDataAry = array(); 
	$newStageDataAry = array();
	$MarkingCriteriaDataAry = array();
	$TasksDataAry = array();
	$StepsDataAry = array();
	$SuccessArr = array();
	
	$li = new libdb();
	$libfilesystem 	= new libfilesystem();
	$exportPath = $intranet_root.'/file/sba/export/'.$r_schemeID;
	$libfilesystem->createFolder($exportPath);
	
	$objIES = new libies();
	$srcScheme = new SbaScheme($r_schemeID);

	$newSchemeDataAry = array('Title'=>$srcScheme->getTitle(), 'Introduction' =>$srcScheme->getIntroduction(), 'MaxScore'=>$srcScheme->getMaxScore(), 'DateInput'=>'Now()', 'InputBy' => $sba_thisUserID, 'DateModified' => 'Now()', 'ModifyBy' => $sba_thisUserID, 'Version' => $srcScheme->getVersion(), 'Language' => $srcScheme->getLanguage(), 'SchemeType' => $srcScheme->getSchemeType(), 'RecordStatus' => $srcScheme->getRecordStatus(), 'DefaultSchemeCode' => $srcScheme->getDefaultSchemeCode(), 'DefaultSchemeVersion' => $srcScheme->getDefaultSchemeVersion(), 'Description' => $srcScheme->getDescription());
	
	$schemeLangFilePath = "{$intranet_root}/file/sba/lang/scheme/".$r_schemeID."/scheme_lang.php";
	if(file_exists($schemeLangFilePath)){	
		$schemeLangFileContent = file_get_contents($schemeLangFilePath);
		$schemeLangFileContent = str_replace('\'','\\\'',$schemeLangFileContent);
	}

	// HANDLE STAGE
	$stageDetailArr = $objIES->getTableInfo('IES_STAGE','SchemeID',$r_schemeID);
	for($i=0, $i_max= count($stageDetailArr); $i < $i_max; $i++){
		$srcStageId = $stageDetailArr[$i]['StageID'];
		$newStageDataAry[] = array('StageID'=>$srcStageId, 'Title'=>$stageDetailArr[$i]['Title'], 'Description'=>$stageDetailArr[$i]['Description'], 'Sequence'=>$stageDetailArr[$i]['Sequence'], 'Deadline'=>$stageDetailArr[$i]['Deadline'], 'MaxScore'=>$stageDetailArr[$i]['MaxScore'], 'Weight'=>$stageDetailArr[$i]['Weight'], 'RecordStatus'=>$stageDetailArr[$i]['RecordStatus'], 'DateInput'=>'Now()', 'InputBy' => $sba_thisUserID, 'DateModified' => 'Now()', 'ModifyBy' => $sba_thisUserID);
					
		// HANDLE MARKING CRITERIA
		$MarkingCriteriaDetailArr = $objIES->getTableInfo('IES_STAGE_MARKING_CRITERIA','StageID',$srcStageId);
		for($j=0, $j_max=count($MarkingCriteriaDetailArr); $j<$j_max ; $j++){
			$MarkingCriteriaDataAry[] = array('StageID'=>$srcStageId, 'MarkCriteriaID'=>$MarkingCriteriaDetailArr[$j]['MarkCriteriaID'], 'task_rubric_id'=>$MarkingCriteriaDetailArr[$j]['task_rubric_id'], 'MaxScore'=>$MarkingCriteriaDetailArr[$j]['MaxScore'], 'Weight'=>$MarkingCriteriaDetailArr[$j]['Weight'], 'RecordStatus'=>$MarkingCriteriaDetailArr[$j]['RecordStatus'], 'DateInput'=>'Now()', 'InputBy' => $sba_thisUserID, 'DateModified' => 'Now()', 'ModifyBy' => $sba_thisUserID);
		}
		
		// HANDLE TASK 
		$objTaskMapper = new SbaTaskMapper();
		$taskObjs = $objTaskMapper->findByStageId($srcStageId);
		for($j=0, $j_max=count($taskObjs); $j<$j_max ; $j++){
			$srcTask = $taskObjs[$j];
			$srcTaskId = $srcTask->getTaskID();
			### 2014-07-25 Siuwan export attachment too
			$attachmentList = $srcTask->getAttachments();
			$attachmentDetails = $sba_libSba->getAttachmentDetails($attachmentList);
			$attachCnt = count($attachmentDetails);
			for($a=0;$a<$attachCnt;$a++){
				$_attachmentAry = $attachmentDetails[$a];
				$_filePath = $_attachmentAry['FolderPath'].$_attachmentAry['FileHashName'];
				if(file_exists($intranet_root.$_filePath)){
					$_dlPath = $exportPath.'/attachment/'.$srcTaskId.'/'.$_attachmentAry['FileID'];
					$libfilesystem->createFolder($_dlPath);
					$_filename = mb_convert_encoding($_attachmentAry['FileName'] ,"big5","utf8");
					$libfilesystem->file_copy($intranet_root.$_filePath, $_dlPath."/".$_filename);
				}
			}
			$TasksDataAry[] = array('StageID'=>$srcStageId, 'TaskID'=>$srcTaskId, 'Attachments'=>$attachmentList, 'Title'=>$srcTask->getTitle(), 'Code'=>$srcTask->getCode(), 'Sequence'=>$srcTask->getSequence(), 'Approval' =>$srcTask->getApproval(), 'Enable' =>$srcTask->getEnable(), 'Introduction' =>$srcTask->getIntroduction(), 'Description' =>$srcTask->getDescription(), 'InstantEdit' =>$srcTask->getInstantEdit(), 'DateInput'=>'Now()', 'InputBy' => $sba_thisUserID, 'DateModified' => 'Now()', 'ModifyBy' => $sba_thisUserID);
				
			// HANDLE STEP 
			$objStepMapper = new SbaStepMapper();
			$stepObjs = $objStepMapper->findByTaskId($srcTaskId);
			for($k=0, $k_max=count($stepObjs); $k<$k_max ; $k++){
				$srcStep = $stepObjs[$k];
				$srcStepID = $srcStep->getStepID();
				$StepsDataAry[] = array('TaskID'=>$srcTaskId, 'StepID'=>$srcStep->getStepID(), 'Title'=>$srcStep->getTitle(), 'Description' =>$srcStep->getDescription(), 'QuestionType' =>$srcStep->getQuestionType(), 'Question' =>$srcStep->getQuestion(), 'StepNo'=>$srcStep->getStepNo(), 'Status'=>$srcStep->getStatus(), 'Sequence' =>$srcStep->getSequence(), 'SaveToTask' =>$srcStep->getSaveToTask(), 'DateInput'=>'Now()', 'InputBy' => $sba_thisUserID, 'DateModified' => 'Now()', 'ModifyBy' => $sba_thisUserID);
			}
		}
	}
	function aryToText($inputAry){
		
		$outputAry = array();
		
		foreach($inputAry as $key => $val){
			if(is_array($val)){
				$outputAry[] = aryToText($val);
			}else{
				//$val = str_replace("'","\'",$val);
				$val = addslashes($val);
				$outputAry[] = "'".$key."'=>'".$val."'";
			}
		}
		return 'array('.implode(',',$outputAry).')';
	}
	
//	header('Content-type: application/txt');
//	header('Content-Disposition: attachment; filename="outputScheme.txt"');


	$file_content = ' 
####****#### 
//Please Fill in the following: 
$ThisDefaultSchemeCode = \'\';
$ThisDefaultSchemeVersion = \'\';
$ThisDefaultSchemeDescription = \'\';
//SchemeType: leave blank \'\' means need to purchase, \'free\' means free		
$ThisDefaultSchemeType = \'\';
//SchemeIncludedAfterPurchaseChecking: true - need to check $plugin[\'SBADefaultSchemeIncludedAfterPurchased\'] in settings.php	
$ThisDefaultSchemeIncludedAfterPurchaseChecking = false;	
////
$inputSchemeDataAry='.aryToText($newSchemeDataAry).'; 
$inputStageDataAry='.aryToText($newStageDataAry).'; 
$inputMarkingCriteriaDataAry='.aryToText($MarkingCriteriaDataAry).'; 
$inputTasksDataAry='.aryToText($TasksDataAry).'; 
$inputStepsDataAry='.aryToText($StepsDataAry).'; 
$schemeLangFileContent=\''.$schemeLangFileContent.'\';
$inputSchemeDataAry[\'DefaultSchemeCode\']=$ThisDefaultSchemeCode; 
$inputSchemeDataAry[\'DefaultSchemeVersion\']=$ThisDefaultSchemeVersion; 
$inputSchemeDataAry[\'Description\']=$ThisDefaultSchemeDescription;
$inputSchemeDataAry[\'DefaultSchemeType\']=$ThisDefaultSchemeType;
$inputSchemeDataAry[\'DefaultSchemeIncludedAfterPurchaseChecking\']=$ThisDefaultSchemeIncludedAfterPurchaseChecking;
$defaultScheme[$ThisDefaultSchemeCode][\'inputSchemeDataAry\'] = $inputSchemeDataAry; 
$defaultScheme[$ThisDefaultSchemeCode][\'inputStageDataAry\'] = $inputStageDataAry; 
$defaultScheme[$ThisDefaultSchemeCode][\'inputMarkingCriteriaDataAry\'] = $inputMarkingCriteriaDataAry; 
$defaultScheme[$ThisDefaultSchemeCode][\'inputTasksDataAry\'] = $inputTasksDataAry; 
$defaultScheme[$ThisDefaultSchemeCode][\'inputStepsDataAry\'] = $inputStepsDataAry; 
####****####****#### 
 ';
$libfilesystem->file_write($file_content, $exportPath."/outputScheme.txt");	

chdir($exportPath);
$zip_name = "outputScheme".$r_schemeID.".zip";
$zip_path = $intranet_root.'/file/sba/export/'.$zip_name;
exec("zip -r ../".OsCommandSafe($zip_name)." *");
$libfilesystem->folder_remove_recursive($exportPath);
output2browser(get_file_content($zip_path), $zip_name);

}else{
	echo 'error';
	exit();
}




?>