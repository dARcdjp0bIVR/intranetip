<?

//include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_dbtable.php");
include_once("common.php");


$objIES = new libies();
$ldb = new libdb();


$scheme_id = $_GET['scheme_id'];


$IsMarkingCrieteriaComplete = $sba_libSba->IsMarkingCriteriaComplete($scheme_id);


// Check if user has access right
$schemeDenied = isSchemeDenied($scheme_id, $sba_thisUserID);
if($schemeDenied)
{
	echo "Invalide access , Empty SCHEME ID<br/>";
  exit();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";

//$LibTable = new libies_dbtable();
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libies_dbtable($field, $order, $pageNo);


// Get the scheme detail
$schemeDetails = $objIES->GET_SCHEME_DETAIL($scheme_id);
$html_schemeTitle = $schemeDetails["Title"];

// Pull-down for scheme switching
$scheme_arr = $objIES->GET_SCHEME_ARR();
$schemeid_arry = "var sch_arr=new Array();";
$html_scheme_option = "<span class=\"table_filter\">";
$html_scheme_option .= "<select name=\"scheme_id\" class=\"formtextbox\">";
$is_selected = "selected=\"selected\"";
for($i=0, $i_max=count($scheme_arr); $i<$i_max; $i++)
{
  $t_scheme_id = $scheme_arr[$i]["SchemeID"];
  $t_scheme_title = $scheme_arr[$i]["Title"];

	$html_scheme_option .= ($t_scheme_id == $scheme_id) ? "<option value=\"{$t_scheme_id}\" {$is_selected}>" : "<option value=\"{$t_scheme_id}\">";
	$html_scheme_option .= $t_scheme_title;
	$html_scheme_option .= "</option>";
}
$html_scheme_option .= "</select>";
$html_scheme_option .= "&nbsp;[&nbsp;<a href = \"javascript:newWindow('/home/eLearning/sba/index.php?mod=content&task=scheme_list&scheme_id=".$scheme_id."&isPreview=1',95)\">".$button_preview."</a>&nbsp;]";

// Get TeachingTeacher and AssessTeacher
$html_TeachingTeacher = "";
$html_AssessTeacher = "";
$result_r = $objIES->get_teacher($scheme_id);
foreach($result_r as $value){
	if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
		$html_TeachingTeacher .= "{$value['TeacherName']} <br \>";
	else if($value["TeacherType"] == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
		$html_AssessTeacher .= "{$value['TeacherName']} <br \>";	
}
if(empty($html_TeachingTeacher)){ 
	$html_TeachingTeacher = "--";
}
if(empty($html_AssessTeacher)){ 
	$html_AssessTeacher = "--";
}

// Get array of stages
$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);

// Redirect to designated stage if stage ID is not set
// empty Stage ID = final
  //debug_r($stage_id);
if(!isset($stage_id))
{
  $stage_id = $stage_arr[0]["StageID"];
  header("Location: ?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id={$stage_id}");
  die();
}

// Create tab navigation by array of stages
$tab_arr = array();
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $stage_arr[$i]["StageID"];
  $t_stage_title = $stage_arr[$i]["Title"];

$tab_arr["tab{$t_stage_id}"] = array("?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id={$t_stage_id}", $t_stage_title);

}
//$tab_arr["tab"] = array("scheme_student_list.php?scheme_id={$scheme_id}&stage_id=", $Lang['IES']['StageSubmitStatus']);
$tab_arr["tab"] = array("?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id=", $Lang['IES']['StageSubmitStatus']);
$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab{$stage_id}");

// Include different pages to use different query and display for stage and final submission
if(empty($stage_id))
{
  // Final submission
  include_once("scheme_student_list.final.php");
}
else
{
  // Stage submission
  include_once("scheme_student_list.stage.php");
}
$pageSizeChangeEnabled = false;

if(count($stage_arr) > 0) {
	$html_student_list = $LibTable->displayIES_StudentList();
} else {
	$html_student_list = '<table class="common_table_list stage1"><tr><td height="40" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr></table>';	
}

//$html_student_list .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
//$html_student_list .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
//$html_student_list .= "</table>";

### Title ###
$title = $ies_lang['StudentProgress'];

$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

// navigation
$nav_arr[] = array($ies_lang['StudentProgress'], "/home/eLearning/sba/?clearCoo=1&mod=admin&task=schemeProgress");
$nav_arr[] = array($html_schemeTitle, "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);


$linterface->LAYOUT_START();
include_once("templates/admin/studentList.tmpl.php");
$linterface->LAYOUT_STOP();

?>