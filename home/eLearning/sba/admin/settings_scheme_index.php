<?php
/*
 * Editing by Paul
 * 
 * Modification Log:
 * 2016-08-15 (Paul)
 * 		- hide Mr Yeung schema (IES scheme 2017) by Sep 1 
 * 2016-07-18 (Paul)
 * 		- add checking of IES_SCHEME.RecordStatus, hide scheme that with RecordStatus = 0 for default scheme
 * 2014-03-27 (Jason)
 * 		- fix the problem of showing the Teaching Teacher name and Assessing Teacher name as it always showed ChineseName before 
 */ 
/*
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
*/
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
//include_once($PATH_WRT_ROOT."includes/sba/libies.php");



//include_once($PATH_WRT_ROOT."includes/libportal.php");

$objIES = new libies();


//$linterface = new interface_html();
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$li = new libuser($UserID);

// Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

// Temporary query retrieving scheme here
$ldb = new libdb();

$sql = "CREATE TEMPORARY TABLE tempScheme (Title varchar(255), StuCount int(8), TeachingTeacher varchar(255), AssessTeacher varchar(255), Steps varchar(255), SchemeID int(8), DefaultSchemeCode int(8), DefaultSchemeVersion int(8), Description text, RecordStatus int(11), PRIMARY KEY (SchemeID)) DEFAULT CHARSET=utf8";
$ldb->db_db_query($sql);

$searchText = stripslashes($searchText);
$cond = ($searchText == "") ? "" : " AND scheme.Title LIKE '%".mysql_real_escape_string($searchText)."%'";
$cond .= (time() < mktime(0, 0, 0, 9, 1, 2016)) ? "" : " AND scheme.DefaultSchemeCode NOT IN (5)";
$sql = "INSERT INTO tempScheme (Title, StuCount, TeachingTeacher, AssessTeacher, Steps, SchemeID, DefaultSchemeCode, DefaultSchemeVersion, Description, RecordStatus) SELECT scheme.Title, COUNT(ies_ss.SchemeStudentID), '', '', '', scheme.SchemeID, scheme.DefaultSchemeCode, scheme.DefaultSchemeVersion, scheme.Description, scheme.RecordStatus FROM {$intranet_db}.IES_SCHEME scheme LEFT JOIN {$intranet_db}.IES_SCHEME_STUDENT ies_ss ON scheme.SchemeID = ies_ss.SchemeID WHERE 1 AND scheme.SchemeType='{$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']}' {$cond} GROUP BY scheme.SchemeID";
$ldb->db_db_query($sql);
$html_searchText = htmlentities($searchText, ENT_QUOTES, "UTF-8");



// Eric Yip (20101015) : Optimized by performing 2 queries instead of looping
$sql = "UPDATE tempScheme ";
$sql .= "INNER JOIN (";
$sql .= "SELECT scheme_teacher.SchemeID, GROUP_CONCAT(".getNameFieldByLang('iu.')." SEPARATOR '<br />') AS TeachingTeacher ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID ";
$sql .= "WHERE scheme_teacher.TeacherType = {$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"]} ";
$sql .= "GROUP BY scheme_teacher.SchemeID) AS t_tt ";
$sql .= "ON tempScheme.SchemeID = t_tt.SchemeID ";
$sql .= "SET tempScheme.TeachingTeacher = t_tt.TeachingTeacher";
$ldb->db_db_query($sql);

$sql = "UPDATE tempScheme ";
$sql .= "INNER JOIN (";
$sql .= "SELECT scheme_teacher.SchemeID, GROUP_CONCAT(".getNameFieldByLang('iu.')." SEPARATOR '<br />') AS AssessTeacher ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID ";
$sql .= "WHERE scheme_teacher.TeacherType = {$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"]} ";
$sql .= "GROUP BY scheme_teacher.SchemeID) AS t_at ";
$sql .= "ON tempScheme.SchemeID = t_at.SchemeID ";
$sql .= "SET tempScheme.AssessTeacher = t_at.AssessTeacher";
$ldb->db_db_query($sql);

$sql = "SELECT Title, StuCount, TeachingTeacher as `TeachingTeacher`, AssessTeacher as `AssessTeacher`, Steps, SchemeID, DefaultSchemeCode, DefaultSchemeVersion, Description FROM tempScheme WHERE (RecordStatus > 0 OR DefaultSchemeCode='0') order by DefaultSchemeCode desc, title";

$html_default_scheme_list .= "";
$scheme_arr = $ldb->returnArray($sql);
if(count($scheme_arr) > 0)
{
  for($i=0,$j=0,$k=0; $i<count($scheme_arr); $i++)
  {

	$teachingTeaching = (trim($scheme_arr[$i]['TeachingTeacher']) == '')? $Lang['SBA']['EmptyValue']:$scheme_arr[$i]['TeachingTeacher'];
	$assessTeacher = (trim($scheme_arr[$i]['AssessTeacher']) == '')? $Lang['SBA']['EmptyValue']:$scheme_arr[$i]['AssessTeacher'];

	if($scheme_arr[$i]['DefaultSchemeCode']>0){
		$html_default_scheme_list .= "<tr>";
	    $html_default_scheme_list .= "<td>".(($k++)+1)."</td>";
	    $html_default_scheme_list .= "<td>".$scheme_arr[$i][0]."</td>";
	    $html_default_scheme_list .= "<td>".$scheme_arr[$i]['Description']."</td>";
	    $html_default_scheme_list .= "<td><a href=\"?mod=content&task=scheme_list&scheme_id=".$scheme_arr[$i][5]."&isPreview=1\" class=\"edit_dim\" title=\"{$Lang['SBA']['EditSchemeSetting']}\" target=\"_blank\">{$Lang['SBA']['EditSchemeSetting']}</a></td>";
		//$html_default_scheme_list .= "<td class=\"sub_row\"><div class=\"table_row_tool\"><a href=\"index.php?mod=admin&task=settings_scheme_edit&schemeID=".$scheme_arr[$i][5]."\" class=\"edit_dim\" title=\"Edit\" ></a><a href=\"index.php?mod=admin&task=scheme_delete&scheme_id=".$scheme_arr[$i][5]."\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete']}\"></a></div></td>";
		$html_default_scheme_list .= "</tr>";
	}
	else{
	    $html_scheme_list .= "<tr>";
	    $html_scheme_list .= "<td>".(($j++)+1)."</td>";
	    $html_scheme_list .= "<td>".$scheme_arr[$i][0]."</td>";
	    $html_scheme_list .= "<td>".$scheme_arr[$i][1]."</td>";
	    $html_scheme_list .= "<td>".$teachingTeaching."</td>";
	    $html_scheme_list .= "<td>".$assessTeacher."</td>";
	//    $html_scheme_list .= "<td><a href=\"scheme_step_edit.php?SchemeID=".$scheme_arr[$i][5]."\" class=\"edit_dim\" title=\"{$Lang['IES']['Edit']}\">{$Lang['IES']['Edit']}</a></td>";
	
	    $html_scheme_list .= "<td><a href=\"?mod=content&task=scheme_list&scheme_id=".$scheme_arr[$i][5]."&isEdit=1\" class=\"edit_dim\" title=\"{$Lang['SBA']['EditSchemeSetting']}\" target=\"_blank\">{$Lang['SBA']['EditSchemeSetting']}</a></td>";
	
	    $html_scheme_list .= "<td class=\"sub_row\"><div class=\"table_row_tool\"><a href=\"index.php?mod=admin&task=settings_scheme_edit&schemeID=".$scheme_arr[$i][5]."\" class=\"edit_dim\" title=\"Edit\" ></a><a href=\"index.php?mod=admin&task=scheme_delete&scheme_id=".$scheme_arr[$i][5]."\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete']}\"></a></div></td>";
	    $html_scheme_list .= "</tr>";
	}
  }
}
else
{
  $html_scheme_list .= "<tr>";
  $html_scheme_list .= "<td align='center' colspan='7'>".$i_no_record_exists_msg."</td>";
  $html_scheme_list .= "</tr>";
}



### It is for the 'New' button ###
$ImportOptionArr = array();
$ImportOptionArr[] = array('javascript:js_Goto_New();', $Lang['SBA']['NewScheme']);
$ImportOptionArr[] = array('javascript:js_Goto_Copy();', $Lang['SBA']['CopyFromDefault']);
$h_toolBar = $linterface->Get_Content_Tool_v30('new', $href="javascript:void(0);",$Lang['SBA']['Create'], $ImportOptionArr, $other="", $divID='BuildDiv');

//$h_toolBar = '<a href="javascript:void(0);" class="new option_layer" id="btn_new" onclick="js_Goto_New();">'.$Lang['SBA']['Create'].'</a>';


### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/settings_scheme_index.tmpl.php");
$linterface->LAYOUT_STOP();
?>
