<?php
include_once($PATH_WRT_ROOT."includes/sba/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");

include_once("common.php");


$objIES = new libies();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";



// Get teacher type of scheme
// 1D-Array format, since teacher may have more than one identity in scheme
$teacher_type_arr = libies_static::getSchemeTeacherType($scheme_id, $UserID);

$scheme_detail_arr = $objIES->GET_SCHEME_DETAIL($scheme_id);

$SchemeTitle = $scheme_detail_arr["Title"];

// Stage Tab
$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$tab_arr = array();
$html_js_array ="";
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $stage_arr[$i]["StageID"];
  $t_stage_title = $stage_arr[$i]["Title"];

  $tab_arr["tab{$t_stage_id}"] = array("index.php?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id={$t_stage_id}", $t_stage_title);
}
$tab_arr["tab"] = array("index.php?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id=", $Lang['IES']['StageSubmitStatus']);
$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab{$stage_id}");

// Task Selection
$task_arr = $objIES->GET_TASK_ARR($stage_id);
$html_task_selection = getSelectByArray($task_arr, "name=\"TaskID\"", $TaskID, 0, 1, "", 2);

if($objIES->isTaskEnable($TaskID))
{
  $next_task_id = libies_static::getNextTask($TaskID);
  $next_task_need_allowance = libies_static::isTaskNeedAllowance($next_task_id);
}

//
$student_arr = $objIES->getTaskStudent($TaskID);
for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
{
  $t_student_id = $student_arr[$i];
  $t_student_obj = new libuser($t_student_id);
  $t_student_name = $t_student_obj->UserName(0);
  $t_class_name = $t_student_obj->ClassName;
  $t_class_number = $t_student_obj->ClassNumber;

  // check if task is survey (limited to student has submitted task handin)
  $t_taskType = $objIES->CheckTaskAnsType($TaskID);
  $t_taskIsSurvey = (strpos(strtoupper($t_taskType), "SURVEY") !== false);

  // check if current answer exist in snapshot
  $t_currentStep_included = isCurrentAnswerInSnapshot($TaskID, $t_student_id);
  // get snapshot array
  $displayAnswerArr = getTaskSnapshotAnswer($TaskID, $t_student_id);

  $sub_content_arr = array();
  if(!$t_currentStep_included)
  {
    // prepare for display current task answer
    $curAnswerArr = getTaskAnswer($TaskID, $t_student_id);
    
    if(!empty($curAnswerArr))
    {
      array_unshift($displayAnswerArr, $curAnswerArr);
    }
  }
  $sub_content_arr = libies_static::getTaskAnswerStrArr($displayAnswerArr, $teacher_type_arr, $StageID,$scheme_detail_arr["Language"],$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);
  
  $html_approval_content = "";
  if(isset($next_task_id) && $next_task_need_allowance)
  {
    $t_task_status = libies_static::isTaskAllowed($next_task_id, $t_student_id);
    $html_approval_content = $t_task_status ? "<span class=\"IES_allow\">{$Lang['IES']['AllowedToNextStep']}</span>" : "<input type=\"button\" name=\"approve\" student_id=\"{$t_student_id}\" task_id=\"{$next_task_id}\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['AllowToNextStep']}\" />";
  }
  
  // count row span
  $rowspan_count = count($sub_content_arr);
  
  $num_of_content = "";
  if(!empty($displayAnswerArr)){
	  $num_of_content = "<div class=\"IES_works\">$rowspan_count ".$Lang['SBA']['NoOfSubmission']."<br \>";
	  $num_of_content .= ($rowspan_count > 1) ? "<a href=\"javascript:;\" name=\"showtable\" val=\"$i\" >({$Lang['Btn']['Show']})</a>" : "";
	  $num_of_content .= "</div>";
  }
  
  // Step thickbox link
  $html_step_thickbox_link = "<a href=\"index.php?mod=admin&task=step_number_thick_box&KeepThis=true&task_id=$TaskID&StudentID=$t_student_id&TB_iframe=true&height=400&width=700\" class=\"thickbox view_step\" title=\"{$Lang['IES']['StudentInputData']}\">&nbsp;</a>";
  
  // Anchor for direct access
  $html_a_name = "<a name=\"uid{$t_student_id}\"></a>";
  
  $html_js_array .= "span_array[{$i}]={$rowspan_count};";
  
  // generate display string
  $html_content .= "<tr>";
  $html_content .= "<td  class=\"title column_$i\" >{$t_class_name} - {$t_class_number}{$html_a_name}<br /><br />{$num_of_content}</td>";
  $html_content .= "<td  class=\"title column_$i\" >{$t_student_name}<br /><br />{$html_approval_content} </td>";
//  if(count($sub_content_arr) > 1){
//  	$html_content .= "<a href=\"javascript:ShowTable($i)\"\">+</a>";
//  }
  
  //debug_r($sub_content_arr);
  for($j=0, $j_max=count($sub_content_arr); $j<$j_max; $j++)
  {
    $html_content .= ($j>0) ? "<tr class=\"display_$i\" style=\"display:none\">" : "";
    // show step answer, but not for task type "survey"
    $html_content .= (!$t_taskIsSurvey && $j==0) ? preg_replace("/(<td class=\"student_ans\">)(.*)(<\/td>)(<td class=\"teacher_comment\">.*)/Us", "$1<div style=\"float:left; width: 95%;\">$2</div><div style=\"float:left; width:5%;\">{$html_step_thickbox_link}</div>$3$4", $sub_content_arr[$j], 1) : $sub_content_arr[$j];
   
   
    $html_content .= "</tr>";
  }
} 

// if the scheme without any student
if($i_max==0) {
	$html_content .= "<tr><td colspan='100%' align='center' height='80'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";	
}

$MenuArr = array();
$MenuArr[] = array($ies_lang['StudentProgress'], ".");
$MenuArr[] = array($SchemeTitle, "index.php?mod=admin&task=studentList&scheme_id={$scheme_id}");
$MenuArr[] = array($html_task_selection, "");
$html_navigation = $linterface->GET_NAVIGATION($MenuArr);

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/scheme_task.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>