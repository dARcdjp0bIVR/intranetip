<?php

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_rubric.php");

$objIES = new libies();



$classRoomID = libies_static::getIESRurbicClassRoomID();
$objRubric = new libiesrubric($classRoomID);

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";

// Get teacher type of scheme
// 1D-Array format, since teacher may have more than one identity in scheme 
$teacher_type_arr = libies_static::getSchemeTeacherType($scheme_id, $sba_thisUserID);

$scheme_detail_arr = $objIES->GET_SCHEME_DETAIL($scheme_id);

$SchemeTitle = $scheme_detail_arr["Title"];
$html_js_array ="";
// Get stage selection
$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
if(empty($stage_id))
{
  header("Location: index.php?mod=admin&task=scheme_stage_final&scheme_id={$scheme_id}&stage_id={$stage_arr[0][0]}");
  DIE();
}
$html_stage_selection = getSelectByArray($stage_arr, "name=\"stage_id\" id=\"stage_id\"", $stage_id, 0, 1, "", 2);

// Tab menu
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $stage_arr[$i]["StageID"];
  $t_stage_title = $stage_arr[$i]["Title"];
  
  $tab_arr["tab{$t_stage_id}"] = array("index.php?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id={$t_stage_id}", $t_stage_title);
}
$tab_arr["tab"] = array("index.php?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id=", $Lang['IES']['StageSubmitStatus']);
$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab");

// Get student array in scheme
$student_arr = libies_static::getSchemeStudentArr($scheme_id);
$stage_mark_criteria_arr = libies_static::getStageMarkingCriteria($scheme_id, $stage_id);
$stage_detail_arr = $objIES->GET_STAGE_DETAIL($stage_id);

// Get stage rubric array
$stage_rubric_arr = array();
for($i=0, $i_max=count($stage_mark_criteria_arr); $i<$i_max; $i++)
{
  $_rubric_id = $stage_mark_criteria_arr[$i]["TASK_RUBRIC_ID"];
  if(!empty($_rubric_id))
  {
    $stage_rubric_arr[$_rubric_id] = $objRubric->getRubricDetail($_rubric_id);
  }
}

for($i=0, $i_max=count($student_arr); $i<$i_max; $i++)
{
  $t_student_id = $student_arr[$i]["UserID"];
  $t_student_obj = new libuser($t_student_id);
  $t_student_name = $t_student_obj->UserName(0);
  $t_class_name = $t_student_obj->ClassName;
  $t_class_number = $t_student_obj->ClassNumber;
  
  //No need to update status upon handin viewing
  //$objIES->updateStudentStageBatchHandinStatus($StageID, $t_student_id, STATUS_TEACHER_READ, STATUS_SUBMITTED);
  $stage_final_handin_arr = libies_static::getStageFinalHandinArr($stage_id, $t_student_id);
  
  $sub_content_arr = libies_static::getBatchStrArr($stage_detail_arr, $stage_final_handin_arr, $stage_mark_criteria_arr, $stage_rubric_arr, $teacher_type_arr, $scheme_detail_arr["Language"],$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);
 
  // Anchor for direct access
  $html_a_name = "<a name=\"uid{$t_student_id}\"></a>";
  
  $rowspan_count = count($sub_content_arr);
  $num_of_content = "";
  if(!empty($stage_final_handin_arr)){
	  $num_of_content = "<div class=\"IES_works\">$rowspan_count ".$Lang['IES']['Submission']."<br \>";
	  $num_of_content .= ($rowspan_count > 1) ? "<a href=\"javascript:;\" name=\"showtable\" val=\"$i\" >({$Lang['Btn']['Show']})</a>" : "";
	  $num_of_content .= "</div>";
  }
  $html_js_array .= "span_array[{$i}]={$rowspan_count};";
  $html_content .= "<tr>";
  $html_content .= "<td class=\"title column_$i\" nowrap='nowrap'>{$t_class_name} - {$t_class_number}{$html_a_name}<br /><br />$num_of_content</td>";
  $html_content .= "<td class=\"title column_$i\">".$t_student_name."<br /><br />".$html_approval_content."</td>";
  
  
  for($j=0, $j_max=count($sub_content_arr); $j<$j_max; $j++)
  {
    $html_content .= ($j>0) ? "<tr class=\"display_$i\" style=\"display:none\">" : "";
    $html_content .= $sub_content_arr[$j];
    $html_content .= "</tr>";
  }
  $html_content .= "</tr>";

}

if ($objIES->getFileCountByStageID($stage_id)>0) {
	$h_showDownloadFiles = '<td><a href="javascript:void(0);" onClick="doFilesDownload()">'.$Lang['IES']['Download'].'</a></td>';
} else {
	$h_showDownloadFiles = "";
}

$MenuArr = array();
$MenuArr[] = array($ies_lang['StudentProgress'], ".");
$MenuArr[] = array($SchemeTitle, "index.php?mod=admin&task=studentList&scheme_id={$scheme_id}&stage_id=");
$MenuArr[] = array($html_stage_selection, "");
$html_navigation = $linterface->GET_NAVIGATION($MenuArr);

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/admin/scheme_stage_final.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>