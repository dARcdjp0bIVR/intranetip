<?php
// [Modification Log] Modifying By: 
//include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

//GET HTTP VARIABLE 
$schemeID = intval(trim($schemeID));
$scheme_name = trim($scheme_name);
$selectedLang = trim($r_schemeLang);
$r_schemeIntro = stripslashes(trim($r_schemeIntro));

$li = new libdb();
$objIES = new libies();

$isNewScheme = ($schemeID == "")?	true	:	false;

  $li->Start_Trans();

	if($isNewScheme)
	{
		$schemeSQL = "INSERT INTO {$intranet_db}.IES_SCHEME (Title, Introduction,DateInput, InputBy, ModifyBy, Language, SchemeType, RecordStatus) VALUES ('".$scheme_name."','".$r_schemeIntro."', NOW(), ".$UserID.", ".$UserID.",'{$selectedLang}','".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."', ".$r_SchemeRecordStatus.")";
	}
	else
	{
		$schemeSQL = "update {$intranet_db}.IES_SCHEME set Title = '{$scheme_name}' , Language = '{$selectedLang}' ,Introduction = '{$r_schemeIntro}' , ModifyBy = {$sba_thisUserID} ,RecordStatus = {$r_SchemeRecordStatus} where SchemeID = {$schemeID}";
	}

	$res[] = $li->db_db_query($schemeSQL);

	if($isNewScheme)
	{
		//THIS IS A NEW SCHEME , NEED TO GET THE NEW CREATED SCHEME ID
		$schemeID = $li->db_insert_id();
	}


	//HANDLE STUDENT LIST
	if(!$isNewScheme){ 
		//NOT A NEW SCHEME 

		//REMOVE STUDENT FROM IES_SCHEME_STUDENT WHO DOES NOT IN THE HTML SELECT LIST
		$_studentIDList = "";
		$_studentIDList = (sizeof($StudentSelected) > 0) ? implode(',',$StudentSelected) : "";
			
		if(trim($_studentIDList) != ""){
			$keepStudentList = " AND UserID not in (".$_studentIDList.")";
		}
		
		//case 1) HTML LIST IS EMPTY. USER REMOVE ALL EXISTING STUDENT FROM THE HTML LIST , THEN $keepStudentList WILL BECOME ""
		//		  IT WILL REMOVE ALL THE EXISTING STUDENT IN IES_SCHEME_STUDENT
		//case 2) HTML LIST NOT EMPTY. USER MAY KEEP SOME EXISTING STUDENT , THEN $keepStudentList WILL HAVE VALUE
		
		// Eric Yip (20100730): retrieve student records to be deleted first,
		// to prepare for status update in intranet module
		$getDeleteStudentSql = "SELECT UserID FROM {$intranet_db}.IES_SCHEME_STUDENT ";
		$getDeleteStudentSql .= "WHERE SchemeID = {$schemeID} {$keepStudentList}";
		$deleteStudentArr = $li->returnVector($getDeleteStudentSql);

		$removeStudentSql = "DELETE FROM {$intranet_db}.IES_SCHEME_STUDENT "; 
		$removeStudentSql .= "WHERE SchemeID = {$schemeID} {$keepStudentList}";	
		$res[] = $li->db_db_query($removeStudentSql);
		
		// Eric Yip (20100730): update intranet module status if deleted students do not in any scheme
		$_deletedStudentIDList = (sizeof($StudentSelected) > 0) ? implode(',',$deleteStudentArr) : "";
		$getRemainStudentSql = "SELECT UserID FROM {$intranet_db}.IES_SCHEME_STUDENT ";
		$getRemainStudentSql .= "WHERE UserID IN (".$_deletedStudentIDList.")";
		$remainStudentArr = $li->returnVector($getRemainStudentSql);
		
		// Students who are not in any scheme
		$updateStatusStudentArr = array_diff($deleteStudentArr, $remainStudentArr);
		$libIntranetModule = new libIntranetModule();
		$q_result = $libIntranetModule->updateModuleUserUsageStatus($ies_cfg["moduleCode"],$updateStatusStudentArr,$im_cfg["DB_INTRANET_MODULE_USER_UsageStatus"]["NotYetUsed"]);
	}

	if(!empty($StudentSelected)){
		//GET EXISTING STUDENT IN THE SCHEME {$schemeID}
		$sql = "select SchemeID,UserID,SchemeStudentID from {$intranet_db}.IES_SCHEME_STUDENT where SchemeID = {$schemeID} ";
		$existingStudentList  = $li->returnArray($sql);

		//INSERT ALL THE STUDENT IN HTML LIST TO IES_SCHEME_STUDENT
		for($i=0; $i<count($StudentSelected); $i++){
			$_studentIsExistBefore = false;

			//CHECK WHETHER STUDENT EXIST IN THE SCHEME BEFORE
			for($j = 0;$j < sizeof($existingStudentList);$j++)
			{
				$_existingStudentID = $existingStudentList[$j]["UserID"];
				if($StudentSelected[$i] == $_existingStudentID)
				{
					//studented student exist in the scheme before
					$_studentIsExistBefore = true;
					break;
				}
			}

			if($_studentIsExistBefore == false)
			{
				//ADD THE STUDNET TO THE SCHEME
				$values[] = "(".$schemeID.", ".$StudentSelected[$i].", ".$UserID.")";
				$newlyInsertStudent[] = $StudentSelected[$i];
			}
		}

		if(sizeof($values) > 0)	// this means that $newlyInsertStudent also > 0
		{
			//insert into IES_SCHEME_STUDENT
			$sql = "INSERT INTO {$intranet_db}.IES_SCHEME_STUDENT (SchemeID, UserID, InputBy) VALUES ";
			$sql .= implode(", ", $values);
			$q_result = $li->db_db_query($sql);
			if ($q_result) {
				$libIntranetModule = new libIntranetModule();
				$q_result = $libIntranetModule->updateModuleUserUsageStatus($ies_cfg["moduleCode"],$newlyInsertStudent,$im_cfg["DB_INTRANET_MODULE_USER_UsageStatus"]["Used"]);
			}
			
			$res[] = $q_result;
		}

		unset($values);
  }

  //GET EXISTING TEACHER IN THE SCHEME {$schemeID}
	$sql = "select SchemeTeacherID,SchemeID,UserID,TeacherType from {$intranet_db}.IES_SCHEME_TEACHER where SchemeID = {$schemeID} ";
	$existingTeacherList  = $li->returnArray($sql);

  $res["TEACHER"][] = $objIES->handleTeacherToScheme($schemeID,$SelectedTeachingTeacher,$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"],$existingTeacherList,$isNewScheme,$UserID);
	$res["TEACHER"][] = $objIES->handleTeacherToScheme($schemeID,$SelectedAssessTeacher,$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"],$existingTeacherList,$isNewScheme,$UserID);

	if (!in_array(false,$res))
  {
    $li->Commit_Trans();
    $msg = "add";
  }
  else
  {
    $li->RollBack_Trans();
    $msg = "add_failed";
  }

header("Location: index.php?mod=admin&task=settings_scheme_index&msg=".$msg);

?>
