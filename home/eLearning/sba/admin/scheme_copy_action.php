<?
#Using:
/*
debug_r($r_schemeID);
debug_r($r_schemeName);
*/
if(is_int(intval($r_schemeID))){

		$SuccessArr = array();
	$li = new libdb();
	$li->Start_Trans();
	$errorMsg='noerror'; /* it is for checking whether there is any error.*/

	$objIES = new libies();
	$srcScheme = new SbaScheme($r_schemeID);
//	debug_r($srcScheme);


	$newScheme = new SbaScheme();

	$newScheme->setTitle($r_schemeName);
	$newScheme->setIntroduction($srcScheme->getIntroduction());
	$newScheme->setMaxScore($srcScheme->getMaxScore());
	$newScheme->setDateInput('now()');
	$newScheme->setInputBy($sba_thisUserID);
	$newScheme->setDateModified('now()');
	$newScheme->setModifyBy($sba_thisUserID);
	$newScheme->setVersion($srcScheme->getVersion());
	$newScheme->setLanguage($srcScheme->getLanguage());
	$newScheme->setSchemeType($srcScheme->getSchemeType());
	$newScheme->setRecordStatus($srcScheme->getRecordStatus());
	$newScheme->setDefaultSchemeCode(0);
	$newScheme->setDefaultSchemeVersion(0);
	$newSchemeID = $newScheme->save();

	$IsSame = compareCopyRecord('SchemeRecord',$srcScheme->getSchemeID(),$newScheme->getSchemeID());

	if($IsSame=='notsame'){
		$errorMsg =1;
		// debug_r('SCHEME');
		$SuccessArr['compareRecord_SchemeRecord'] = false;
	}else{
		/**start part 2.ToopTip--if the original SchemeID contains scheme_lang.php, create new folder for new SchemeID and copy the .php to the folder**/

		$Old_folder_path = "{$intranet_root}/file/sba/lang/scheme/".$srcScheme->getSchemeID()."/scheme_lang.php";

		if (file_exists($Old_folder_path)) {
			//create scheme_lang.php which saves the detail of the scheme
			$New_folder_path = "{$intranet_root}/file/sba/lang/scheme/".$newScheme->getSchemeID()."/";
			$objsystem = new libfilesystem();
			$objsystem->folder_new($New_folder_path);

			$New_folder_path = $New_folder_path."scheme_lang.php";

			if (!copy($Old_folder_path, $New_folder_path)){
				$errorMsg =1;
				// debug_r('File');
				$SuccessArr['cantcopyfile'] = false; //if cannot copy the file.
			}

		} else {
		}
		/**end part 2**/


		// START PART 3 :HANDLE STAGE
		$stageDetailArr = $objIES->getTableInfo('IES_STAGE','SchemeID',$r_schemeID);
		for($i=0, $i_max= count($stageDetailArr); $i < $i_max; $i++){
			$srcStageId = $stageDetailArr[$i]['StageID'];
			$newStage = new SbaStage();
			$newStage->setSchemeID($newSchemeID);
			$newStage->setTitle($stageDetailArr[$i]['Title']);
			$newStage->setDescription($stageDetailArr[$i]['Description']);
			$newStage->setSequence($stageDetailArr[$i]['Sequence']);
			$newStage->setDeadline($stageDetailArr[$i]['Deadline']);
			$newStage->setMaxScore($stageDetailArr[$i]['MaxScore']);
			$newStage->setWeight($stageDetailArr[$i]['Weight']);
			$newStage->setRecordStatus($stageDetailArr[$i]['RecordStatus']);
			$newStage->setDateInput('now()');
			$newStage->setInputBy($sba_thisUserID);
			$newStage->setDateModified('now()');
			$newStage->setModifyBy($sba_thisUserID);

			$newStage->save();
			$newStageID = $newStage->getStageID();

			//store this stage's Survey Edit Question and Survey Analysis Question ID
			$thisStageStepQuestionEditID = array();
			$thisStageStepQuestionAnalysisData = array();
			//variables handling analysis steps
			$tempStepIDAry = array();
			$tempAnalysisStep = array();

			$IsSame = compareCopyRecord('StageRecord',$srcStageId,$newStageID);

			// END PART 3 : COPY STAGE
			if($IsSame=='notsame'){
				$errorMsg = 1;
				$SuccessArr['compareRecord_StageRecord'] = false;
			}else{
				// START PART 4.1 : HANDLE MARKING CRITERIA
				$MarkingCriteriaDetailArr = $objIES->getTableInfo('IES_STAGE_MARKING_CRITERIA','StageID',$srcStageId);
				for($j=0, $j_max=count($MarkingCriteriaDetailArr); $j<$j_max ; $j++){
					$newStageMarkingCriteriaArr = array();
					$newStageMarkingCriteriaArr['StageID'] = $newStageID;
					$newStageMarkingCriteriaArr['MarkCriteriaID'] = $MarkingCriteriaDetailArr[$j]['MarkCriteriaID'];
					if($srcScheme->getDefaultSchemeCode()==0){
						$newStageMarkingCriteriaArr['task_rubric_id'] = $MarkingCriteriaDetailArr[$j]['task_rubric_id'];
					}
					$newStageMarkingCriteriaArr['MaxScore'] = $MarkingCriteriaDetailArr[$j]['MaxScore'];
					$newStageMarkingCriteriaArr['Weight'] = $MarkingCriteriaDetailArr[$j]['Weight'];
					$newStageMarkingCriteriaArr['RecordStatus'] = $MarkingCriteriaDetailArr[$j]['RecordStatus'];
					$newStageMarkingCriteriaArr['DateInput'] = 'now()';
					$newStageMarkingCriteriaArr['InputBy'] = $sba_thisUserID;
					$newStageMarkingCriteriaArr['DateModified'] = 'now()';
					$newStageMarkingCriteriaArr['ModifyBy'] = $sba_thisUserID;

					$newStageMarkingCriteriaID = $sba_libSba->insertStageMarkingCriteria($newStageMarkingCriteriaArr);

					$oldStageMarkingCriteriaID = $MarkingCriteriaDetailArr[$j]['StageMarkCriteriaID'];

					$IsSame = compareCopyRecord('StageMarkingCriteriaRecord',$oldStageMarkingCriteriaID,$newStageMarkingCriteriaID);
					if($IsSame=='notsame')
					{
						$errorMsg = 1;
						$SuccessArr['compareRecord_StageMarkingCriteriaRecord'] = false;
					}
				}
				// END PART 4.1 : MARKING CRITERIA

				// START PART 4.2 :HANDLE TASK
				$objTaskMapper = new SbaTaskMapper();
				$taskObjs = $objTaskMapper->findByStageId($srcStageId);
				for($j=0, $j_max=count($taskObjs); $j<$j_max ; $j++){
					$srcTask = $taskObjs[$j];
					$srcTaskId = $srcTask->getTaskID();
					$newTask = new Task();
					$newTask->setStageID($newStageID);
					$newTask->setTitle($srcTask->getTitle());
					$newTask->setCode($srcTask->getCode());
					$newTask->setSequence($srcTask->getSequence());
					$newTask->setApproval($srcTask->getApproval());
					$newTask->setEnable($srcTask->getEnable());
					$newTask->setIntroduction($srcTask->getIntroduction());
					$newTask->setDescription($srcTask->getDescription());
					$newTask->setAttachments($srcTask->getAttachments());
					$newTask->setInstantEdit($srcTask->getInstantEdit());
					$newTask->setDateInput('now()');
					$newTask->setInputBy($sba_thisUserID);
					$newTask->setDateModified('now()');
					$newTask->setModifyBy($sba_thisUserID);

					$objTaskMapper->save($newTask);

					$newTaskID = $newTask->getTaskID();

					$IsSame = compareCopyRecord('TaskRecord',$srcTaskId,$newTaskID);
					// END PART 4.2 : COPY TASK
					if($IsSame=='notsame')
					{
						$errorMsg = 1;
						$SuccessArr['compareRecord_TaskRecord'] = false;
					}else{

						// START PART 5 :HANDLE STEP
						$objStepMapper = new SbaStepMapper();
						$stepObjs = $objStepMapper->findByTaskId($srcTaskId);
						for($k=0, $k_max=count($stepObjs); $k<$k_max ; $k++){
							$srcStep = $stepObjs[$k];
							$srcStepID = $srcStep->getStepID();
							$newStep = new Step();

							$newStep->setTaskID($newTaskID);
							$newStep->setTitle($srcStep->getTitle());
							$newStep->setDescription($srcStep->getDescription());
							$newStep->setQuestionType($srcStep->getQuestionType());
							$newStep->setQuestion($srcStep->getQuestion());
							$newStep->setStepNo($srcStep->getStepNo());
							$newStep->setStatus($srcStep->getStatus());
							$newStep->setSequence($srcStep->getSequence());
							$newStep->setSaveToTask($srcStep->getSaveToTask());
							$newStep->setDateInput('now()');
							$newStep->setInputBy($sba_thisUserID);
							$newStep->setDateModified('now()');
							$newStep->setModifyBy($sba_thisUserID);

							$objStepMapper->save($newStep);

							$newStepID = $newStep->getStepID();

							$tempStepIDAry[$newStepID] = $srcStepID;
							if(strtoupper(substr($srcStep->getQuestionType(),-8))=='ANALYSIS'){
								$tempAnalysisStep[$newStepID] = $srcStep->getQuestion();
							}

							$IsSame = compareCopyRecord('StepRecord',$srcStepID,$newStepID);
							if($IsSame=='notsame')
							{
								$errorMsg = 1;
								$SuccessArr['compareRecord_StepRecord'] = false;
							}
						// END PART 5 : COPY STEP
						// Copy of STEP 'Survey Analysis Question' continues before this stage ends

						}
					}
				}
				//Copy of 'Survey Analysis Question' continues here
				if(!empty($tempAnalysisStep)){
					foreach($tempAnalysisStep as $key=>$val){
						$newStep = $objStepMapper->getByStepId($key);
						$srcStepQuestionID = explode(',',$val);
						$newStep->setQuestion(implode(',',array_keys(array_intersect($tempStepIDAry,$srcStepQuestionID))));
						$objStepMapper->save($newStep);
					}
				}
			}
		}
	}

	if (in_array(false, $SuccessArr)){
	  	$li->RollBack_Trans();
	    $msg = "add_failed";
	}else{
		$li->Commit_Trans();
	    $msg = "add";
	}

	if($errorMsg=='noerror'){
	  	$msg='add';
	  	header("Location: index.php?mod=admin&task=settings_scheme_edit&schemeID=".$newSchemeID."&msg=".$msg);
		exit();
	}else{//failed
	 	header("Location: index.php?mod=admin&task=scheme_copy&msg=".$errorMsg);
		exit();
	}

}else{
	echo 'error';
	exit();
}



//get the scheme structure
/*
1) get the scheme details
2) get the stage details
3) get the task details
4) get the step details
5) get the mark details
*/
exit();

$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();


$linterface->LAYOUT_START();
include_once("templates/admin/scheme_copy.tmpl.php");
$linterface->LAYOUT_STOP();


function compareCopyRecord($RecordIndicator,$OriginalID,$NewID)
{
	  global $intranet_db;

	  $ldb = new libdb();

	  $returnResult1= 'same';

	  if($RecordIndicator=='SchemeRecord')
	  {
		$sqlOri = "SELECT * FROM {$intranet_db}.IES_SCHEME WHERE SchemeID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_SCHEME WHERE SchemeID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Introduction'],$OriginalArray[0]['MaxScore'],$OriginalArray[0]['Version'],$OriginalArray[0]['Language'], $OriginalArray[0]['SchemeType'],$OriginalArray[0]['RecordStatus']);
		$arrayNew = array($NewArray[0]['Introduction'],$NewArray[0]['MaxScore'],$NewArray[0]['Version'], $NewArray[0]['Language'],$NewArray[0]['SchemeType'],$NewArray[0]['RecordStatus']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
      }
	  else if($RecordIndicator=='StageRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STAGE WHERE StageID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STAGE WHERE StageID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Title'],$OriginalArray[0]['Description'],$OriginalArray[0]['Sequence'], $OriginalArray[0]['Deadline'],$OriginalArray[0]['MaxScore'],$OriginalArray[0]['Weight'],$OriginalArray[0]['RecordStatus']);
		$arrayNew = array($NewArray[0]['Title'],$NewArray[0]['Description'], $NewArray[0]['Sequence'],$NewArray[0]['Deadline'],$NewArray[0]['MaxScore'],$NewArray[0]['Weight'],$NewArray[0]['RecordStatus']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
	  }
	  else if($RecordIndicator=='TaskRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);
		$sqlNew = "SELECT * FROM {$intranet_db}.IES_TASK WHERE TaskID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Title'],$OriginalArray[0]['Code'],$OriginalArray[0]['Sequence'], $OriginalArray[0]['Approval'],$OriginalArray[0]['Enable'],$OriginalArray[0]['Introduction'],$OriginalArray[0]['Description'],$OriginalArray[0]['InstantEdit']);
		$arrayNew = array($NewArray[0]['Title'],$NewArray[0]['Code'], $NewArray[0]['Sequence'],$NewArray[0]['Approval'],$NewArray[0]['Enable'],$NewArray[0]['Introduction'],$NewArray[0]['Description'],$NewArray[0]['InstantEdit']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);

	  }
	  else if($RecordIndicator=='StepRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STEP WHERE StepID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STEP WHERE StepID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['Title'],$OriginalArray[0]['Description'],$OriginalArray[0]['QuestionType'],$OriginalArray[0]['Question'],$OriginalArray[0]['StepNo'],$OriginalArray[0]['Status'],$OriginalArray[0]['Sequence'],$OriginalArray[0]['SaveToTask']);
		$arrayNew = array($NewArray[0]['Title'],$NewArray[0]['Description'],$NewArray[0]['QuestionType'],$NewArray[0]['Question'],$NewArray[0]['StepNo'],$NewArray[0]['Status'],$NewArray[0]['Sequence'],$NewArray[0]['SaveToTask']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
	  }
	  else if($RecordIndicator=='StageMarkingCriteriaRecord')
	  {
	  	$sqlOri = "SELECT * FROM {$intranet_db}.IES_STAGE_MARKING_CRITERIA WHERE StageMarkCriteriaID = '{$OriginalID}'";
		$OriginalArray = $ldb->returnArray($sqlOri);

		$sqlNew = "SELECT * FROM {$intranet_db}.IES_STAGE_MARKING_CRITERIA WHERE StageMarkCriteriaID = '{$NewID}'";
		$NewArray = $ldb->returnArray($sqlNew);

		$arrayOld = array($OriginalArray[0]['MarkCriteriaID'],$OriginalArray[0]['MaxScore'],$OriginalArray[0]['Weight'],$OriginalArray[0]['RecordStatus']);
		$arrayNew = array($NewArray[0]['MarkCriteriaID'],$NewArray[0]['MaxScore'],$NewArray[0]['Weight'],$NewArray[0]['RecordStatus']);
		$returnResult1 = compareTwoArray($arrayOld,$arrayNew);
	  }
		return $returnResult1;
}

/**
* Compare 2 arrays
* @owner : Connie (20110906)
* @param : $array1
* @param : $array2
* @return : $returnResult1 (if 2 arrays are the same, it would return 'same'. Otherwise, it would return 'notsame')
*
*/
function compareTwoArray($array1,$array2)
{
	/*
	debug_r($array1);
	echo '<hr/>';
	debug_r($array2);
	*/


	$diff = array_diff($array1, $array2);

	if(count($diff)==0){  # is the same
		$returnResult='same';
	}else {
		$returnResult='notsame';
	}
	return $returnResult;
}


?>