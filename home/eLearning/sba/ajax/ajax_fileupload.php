<?php
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$libsba	= new libSba();
$libfilesystem 	= new libfilesystem();


$browser = array(
    'version'   => '0.0.0',
    'majorver'  => 0,
    'minorver'  => 0,
    'build'     => 0,
    'name'      => 'unknown',
    'useragent' => ''
  );

  $browsers = array(
    'firefox', 'msie', 'opera', 'chrome', 'safari', 'mozilla', 'seamonkey', 'konqueror', 'netscape',
    'gecko', 'navigator', 'mosaic', 'lynx', 'amaya', 'omniweb', 'avant', 'camino', 'flock', 'aol'
  );

  if (isset($_SERVER['HTTP_USER_AGENT'])) {
    $browser['useragent'] = $_SERVER['HTTP_USER_AGENT'];
    $user_agent = strtolower($browser['useragent']);
    foreach($browsers as $_browser) {
      if (preg_match("/($_browser)[\/ ]?([0-9.]*)/", $user_agent, $match)) {
        $browser['name'] = $match[1];
        $browser['version'] = $match[2];
        @list($browser['majorver'], $browser['minorver'], $browser['build']) = explode('.', $browser['version']);
        break;
      }
    }
  }


switch ($ajaxAction){
    
    case 'getFile':
  
	$file = current($libsba->getAttachmentDetails($file_id));
	$file_path = $intranet_root.$file['FolderPath'].$file['FileHashName'];
  
	if (is_file($file_path)){	    
	    if($browser['name'] == 'mozilla' || $browser['name'] == 'msie')
	    	header('Content-Disposition: attachment; filename="'.iconv("UTF-8","big5",$file['FileName']).'"');
	    else 
	    	header('Content-Disposition: attachment; filename="'.$file['FileName'].'"');
	    header("Content-Type: application/octet-stream");
	    header("Content-Type: application/download");
	    header("Content-Description: File Transfer");
	    
	    readfile($file_path);
	}else{
	    
	    header("HTTP/1.0 404 Not Found");
	    die('File Not Found');
	    
	}
	
    break;
    case 'removeFiles':
	
	foreach ($new_attach_file_ids as $file_id){
	    $libsba->removeAttachment($file_id);
	}
	
	
    break;
    case 'addFile':
    
	$file = $_FILES['file'];
	
	if (!$file['name'] || $sba_thisMemberType!=USERTYPE_STAFF) die;
		
	$file_name	= $libsba->filename_safe($file['name']);
	$target_dir	= "/file/sba/scheme_attachment/scheme_s$scheme_id/";
	$file_hash	= sha1($scheme_id.microtime());
	
	$libfilesystem->createFolder($intranet_root.$target_dir);
	
	$file_id = $libsba->addAttachment($target_dir, $file_name, $file_hash, $UserID);
	move_uploaded_file($file['tmp_name'], $intranet_root.$target_dir.$file_hash);


?>

<div class='attach_file attach_file file_<?=$file_id?>'>
<a class='file_name' href='./?mod=ajax&task=ajax_fileupload&ajaxAction=getFile&file_id=<?=$file_id?>'><?=htmlspecialchars($file_name)?></a>
<a class='delete_row' href='javascript:void(0)' onclick='remove_attachfile("<?=$file_id?>", "<?=$task_id?>", "<?=$step_id?>")'></a>
<input class='new_attach_file_id' type='hidden' name='r_Attachment[]' value='<?=$file_id?>'/>
</div>


<?
    break;
}

?>