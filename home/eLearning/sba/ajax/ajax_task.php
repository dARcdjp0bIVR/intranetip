<?php
# using : 
include_once($intranet_root."/includes/sba/libSba_ui.php");
include_once($intranet_root."/includes/sba/libSbaTask.php");
include_once($intranet_root."/includes/sba/libSbaTaskHandinFactory.php");

# Initialize variable
$ajaxAction = isset($ajaxAction) && $ajaxAction!=''? $ajaxAction:'';

switch($ajaxAction) {
	
	case "GetTask" : 
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		$objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($stage_id, $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']); // Temporary set the Task Handin Type as $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']
		
		# Get Task Object
		if($task_id){
			# Get the existing Task
			$objTask = $objTaskMapper->getByTaskId($task_id);
		}
		else{
			$objTask = new task();
			
			# Set Task StageID for newly created Task
			$objTask->setStageID($stage_id);
			
			# Set Task Sequence for newly created Task
			$Sequence = count($objTaskMapper->findByStageId($stage_id)) + 1;
			$objTask->setSequence($Sequence);
		}
		
		$returnAry = array(
						   array('TaskTitleUI', $sba_libSba_ui->getTaskHeaderTitleUI($objTask, ($task_id? false:true))),
						   array('UI', 	$sba_libSba_ui->getTaskUI($objTask, $objTaskHandin, $sba_allow_edit))
					 );
		
		break;
	
	case "TaskEditForm" :
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		
		# Get Task Object
		if($task_id){
			# Get the existing Task
			$objTask = $objTaskMapper->getByTaskId($task_id);
		}
		else{
			$objTask = new task();
			
			# Set Task StageID for newly created Task
			$objTask->setStageID($stage_id);
			
			# Set Task Sequence for newly created Task
			$Sequence = count($objTaskMapper->findByStageId($stage_id)) + 1;
			$objTask->setSequence($Sequence);
		}
		
		$returnAry = array(
						   array('TaskTitleUI', $sba_libSba_ui->getTaskHeaderTitleUI($objTask, false)),
						   array('UI', $sba_libSba_ui->getTaskEditUI($objTask))
					 );
		
		break;
			
	case "TaskEditUpdate" : 
		# Initialize variable
		$Title		  = isset($r_task_Title) && $r_task_Title!=''? stripslashes($r_task_Title):'';
		$Sequence	  = isset($r_task_Sequence) && $r_task_Sequence!=''? $r_task_Sequence:0;
		$Approval 	  = isset($r_task_Approval) && $r_task_Approval!=''? $r_task_Approval:0;
		$Enable 	  = isset($r_task_Enable) && $r_task_Enable!=''? $r_task_Enable:0;
		$Description      = isset($r_task_Description) && $r_task_Description!=''? stripslashes($r_task_Description):'';
		
		
		# Set $Approval to be 0 if it is the first task of the stage
		$Approval = $Sequence>1? $Approval:0;
		
		# Decide the value of InstantEdit field of table IES_TASK 
		$r_task_InstantEdit = isset($r_task_InstantEdit) && $r_task_InstantEdit!=''? $r_task_InstantEdit:0;
		$r_task_StepEdit    = isset($r_task_StepEdit) && $r_task_StepEdit!=''? $r_task_StepEdit:0;
		
		if(!$r_task_InstantEdit && $r_task_StepEdit){
			$InstantEdit = $sba_cfg['DB_IES_TASK_InstantEdit']['StepEdit'];
		}
		else if($r_task_InstantEdit && $r_task_StepEdit){
			$InstantEdit = $sba_cfg['DB_IES_TASK_InstantEdit']['InstantStepEdit']; 
		}
		else{
			$InstantEdit = $sba_cfg['DB_IES_TASK_InstantEdit']['InstantEdit']; 
		}
	
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		$objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($stage_id, $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']); // Temporary set the Task Handin Type as $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea']
		
		# Get Task Object
		if($task_id){
			# Get the existing Task
			$objTask = $objTaskMapper->getByTaskId($task_id);
		}
		else{
			$objTask = new task();
			
			# Set Task Instance for newly created Task
			$objTask->setCode($sba_libSba->getUniqID()); // Insert a Unique ID to Code field (which is a unique key) in table IES_TASK
			$objTask->setDateInput(date('Y-m-d H:i:s'));
			$objTask->setInputBy($sba_thisUserID);
		}
		
		# Set Task Instance
		$objTask->setStageID($stage_id);
		$objTask->setTitle($Title);
		$objTask->setSequence($Sequence);
		$objTask->setApproval($Approval);
		$objTask->setEnable($Enable);
		$objTask->setDescription($Description);
		$objTask->setInstantEdit($InstantEdit);
		$objTask->setDateModified(date('Y-m-d H:i:s'));
		$objTask->setModifyBy($sba_thisUserID);
		
		$objTaskMapper->save($objTask);
		
		$returnAry = array(
						array('TaskID', 	 $objTask->getTaskID()),
						array('UI', 		 $sba_libSba_ui->getTaskUI($objTask, $objTaskHandin, true)),
						array('ResponseMsg', "已儲存工作")
					 );
		
		break;
		
	case "TaskIntroduction":
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		
		# Get Task Object
		$objTask = $objTaskMapper->getByTaskId($task_id);
		
		$returnAry = array(array('UI', $sba_libSba_ui->getTaskIntroContentUI($objTask, $sba_allow_edit, false)));
	
		break;
	
	case "TaskIntroductionForm":
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		
		# Get Task Object
		$objTask = $objTaskMapper->getByTaskId($task_id);
		
		$returnAry = array(array('UI', $sba_libSba_ui->getTaskIntroContentUI($objTask, true, true)));
		
		break;
	
	case "TaskIntroductionUpdate":
		# Initialize variable
		$Introduction = isset($r_task_Introduction) && $r_task_Introduction!=''? $r_task_Introduction:'';
		$remove_Attachment = is_array($r_remove_Attachment)? $r_remove_Attachment: array();
		$Attachments	  = is_array($r_Attachment)?implode(',',array_diff($r_Attachment,$remove_Attachment)):'';
		
		foreach ($remove_Attachment as $file_id){//remove attachments as request
		    $sba_libSba->removeAttachment($file_id);
		}
	    
		
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		
		# Get the existing Task
		$objTask = $objTaskMapper->getByTaskId($task_id);
		
		$objTask->setIntroduction(stripslashes($Introduction));
		$objTask->setAttachments($Attachments);
		$objTask->setDateModified(date('Y-m-d H:i:s'));
		$objTask->setModifyBy($sba_thisUserID);
		
		$objTaskMapper->save($objTask);
		$tempFileStates = 0;
		
		$returnAry = array(
						array('UI', $sba_libSba_ui->getTaskIntroContentUI($objTask, true, false)),
						array('ResponseMsg', $Lang['SBA']['IntroductionSaved'])
					);
		
		break;
		
	case "GetTaskIntroductionRaw":
		# Initialize Objects
		$objTaskMapper = new SbaTaskMapper();
		
		# Get the existing Task
		$objTask = $objTaskMapper->getByTaskId($task_id);
		
		$returnAry = array(array('TaskIntro', $objTask->getIntroduction()),
				   //array('AttachmentUI', $sba_libSba_ui->getTaskAttachmentTableUI($objTask, false, $task_id)),
				   );
		
		break;
		
	case "DeleteTask":
		# Delete the Selected Task
		$TaskID_ary[] = $task_id;
		$sba_libSba->deleteTasks($TaskID_ary);
		/*
		# Reorder the Task Sequence
		$objTaskMapper = new SbaTaskMapper();
		$tasks_ary = $objTaskMapper->findByStageId($stage_id);
		for($i=0;$i<count($tasks_ary);$i++){
			$objTask = $tasks_ary[$i];
			$objTask->setSequence($i+1);
			$objTaskMapper->save($objTask);
		}
		*/
		$returnAry = array();
		
		break;
	
	case "TaskHandinForm" :
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		$objTask 	   = $objTaskMapper->getByTaskId($task_id);
		
		$AnswerID = $objTask->getTaskID() && $sba_thisMemberType==USERTYPE_STUDENT? $sba_libSba->getAnswerIDByTaskIDUserID($objTask->getTaskID(), $sba_thisStudentID) : null;
		
		$objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($objTask->getStageID(), $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea'], $objTask->getTaskID(), $AnswerID);
		
		$returnAry = array(array('UI', $sba_libSba_ui->getTaskStdAnswerContentUI($objTask, $objTaskHandin, $sba_allow_edit, true)));
		
		break;
	
	case "GetTaskHandinFormInfo" :
		# Initialize Objects
		$objTaskMapper = new SbaTaskMapper();
		$objTask 	   = $objTaskMapper->getByTaskId($task_id);
		
		$AnswerID = $objTask->getTaskID() && $sba_thisMemberType==USERTYPE_STUDENT? $sba_libSba->getAnswerIDByTaskIDUserID($objTask->getTaskID(), $sba_thisStudentID) : null;
		
		$objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($objTask->getStageID(), $sba_cfg['DB_IES_TASK_HANDIN_QuestionType']['textarea'], $objTask->getTaskID(), $AnswerID);
		
		$returnAry = array(
						array('StdAns', $objTaskHandin->getDisplayAnswer()),
						array('AnswerID', $objTaskHandin->getAnswerID())
					 );
		
		break;
	
	case "TaskHandinUpdate" :
		# Initialize variable
		$AnswerID	  = isset($r_AnswerID) && $r_AnswerID!=''? $r_AnswerID:0;
		$QuestionType = isset($r_QuestionType) && $r_QuestionType!=''? $r_QuestionType:'';
		$Answer		  = isset($r_Answer) && $r_Answer!=''? stripslashes($r_Answer):'';
		
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objTaskMapper = new SbaTaskMapper();
		$objTask 	   = $objTaskMapper->getByTaskId($task_id);
		$objTaskHandin = libSbaTaskHandinFactory::createTaskHandin($objTask->getStageID(), $QuestionType, $objTask->getTaskID(), $AnswerID);
		
		# Set Task Handin Instance
		$objTaskHandin->setTaskID($task_id);
		$objTaskHandin->setUserID($sba_thisStudentID);
		$objTaskHandin->setStudentAnswer($Answer);
		$objTaskHandin->setDateModified(date('Y-m-d H:i:s'));
		$objTaskHandin->setModifyBy($sba_thisStudentID);
		
		# Set Task Handin Instance for newly created Task Handin
		if(!$AnswerID){
			$objTaskHandin->setDateInput(date('Y-m-d H:i:s'));
			$objTaskHandin->setInputBy($sba_thisStudentID);
		}
		
		$objTaskHandin->save();
		
		$returnAry = array(
						array('UI', 	$sba_libSba_ui->getTaskStdAnswerContentUI($objTask, $objTaskHandin, false, false)),
						array('InfoUI', $sba_libSba_ui->getTaskStdAnswerContentInfoUI($objTask, $objTaskHandin)),
						array('ResponseMsg', $Lang['SBA']['StudentHandinSaved'])
					);
		
		break;
	
	default	: 
		break;
}

header("Content-Type: text/xml");

$libies = new libies();
$XML = $libies->generateXML($returnAry, true, false);

echo $XML;
?>