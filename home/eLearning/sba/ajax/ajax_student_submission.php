<?php
$libies = new libies();
$batch_id = $libies->getCurrentBatchId($stage_id, $sba_thisStudentID);
$updateResult = $libies->changeBatchStatus($batch_id, $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]);
$updateResult = $updateResult && $libies->updateBatchSubmissionDate($batch_id);
$updateResult = $updateResult && $libies->updateStudentStageBatchHandinStatus($stage_id,$sba_thisStudentID,$ies_cfg["DB_IES_STAGE_STUDENT_BatchHandinStatus"]["submitted"]);


//once student submit a stage handin , should reset the student stage mark to null (not zero);
$_setMark = 'NULL';
$libies->updateStudentStageScore($stage_id, $sba_thisStudentID,$_setMark);




intranet_closedb();
//# Output the modified date to the page
header("Content-Type:   text/xml");
$libies = new libies();
$XML = $libies->generateXML(
					array(
						array("result", ($updateResult?1:0)),
						array("batch_id", $batch_id)
					)
				);
echo $XML;
?>