<?php
/** [Modification Log] Modifying By:
 * *******************************************
 * 2020-06-16 (Pun) [ip.2.5.11.7.1] [187053]
 *  - Fix cannot export highchart for https
 * 2019-07-15 (Pun) [ip.2.5.10.10.1]
 *  - Added remove folder/file that created 7 days before today
 *
 * *******************************************
 */
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");

$libies_survey = new libies_survey();

$src = $_POST['imgSrc'];
$fileName = $_POST['fileName'];
$fileType = $_POST['fileType'];

$url= 'https://export.highcharts.com/'.$src;
$baseDir = $intranet_root.$sba_cfg['SBA_SURVEY_CHART_ROOT_PATH'].$_SESSION['UserID'].'/';
$dir = $baseDir.$_SESSION['thisExportSurveyID'].'/';

// Remove file older then 7 days
system("find ".escapeshellarg($baseDir)." -mtime +7 | xargs rm -rf");
//TEST SCRIPT: system("find ".escapeshellarg($baseDir)." | xargs touch -t 200001010101");


@mkdir($baseDir, 0755);
if($_POST['type']=='surveyMapping'){
    $SurveyMappings = $libies_survey->loadSurveyMapping($_SESSION['thisExportSurveyID'],$UserID);
    $key = $_POST['key'];
    if(isset($SurveyMappings)){
        foreach ($SurveyMappings as $SurveyMapping){
            $SurveyMappingIDs[]=$SurveyMapping['SURVEYMAPPINGID'];
        }
        $dir .= 'combine'.$SurveyMappingIDs[$key].'/';
    }
    mkdir($dir, 0755);
    $img = $dir.$fileName.'.'.$fileType;
    file_put_contents($img, file_get_contents($url));
}else{
    !is_dir($dir)?mkdir($dir, 0755):'';
    $img = $dir.$fileName.'.'.$fileType;
    file_put_contents($img, file_get_contents($url));
}
echo 'Chart images saved';
