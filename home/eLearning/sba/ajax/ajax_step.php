<?php
# using : 
include_once($intranet_root."/includes/sba/libSba_ui.php");
include_once($intranet_root."/includes/sba/libSbaStep.php");
include_once($intranet_root."/includes/sba/libSbaStepHandinFactory.php");

# Initialize variable
$ajaxAction = isset($ajaxAction) && $ajaxAction!=''? $ajaxAction:'';


switch($ajaxAction){
	case 'GetStep' :
		
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objStepMapper = new SbaStepMapper();
		
		# Get Step Object
		$objStep = $objStepMapper->getByStepId($step_id);
		
		# Get the AnswerID of the Step Handin
		
		$answer_id = $sba_thisMemberType==USERTYPE_STUDENT? $sba_libSba->getAnswerIDByStepIDUserID($step_id, $sba_thisStudentID) : null;
	
		# Get the Step Handin Object
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion(), $answer_id);
		
		$returnAry = array(array('UI', $sba_libSba_ui->getStepUI($objStep, $objStepHandin, $sba_allow_edit)));
		
		break;
		
	case 'GetStepEditForm' :
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objStepMapper = new SbaStepMapper();
		
		# Get Step Object
		if($step_id){
			# Get the existing Step
			$objStep = $objStepMapper->getByStepId($step_id);
			
			# Get the Step Handin Object
			$StepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion());
		}
		else{
			$objStep = new step();
			
			# Set Step TaskID for newly created Step
			$objStep->setTaskID($task_id);
			$objStep->setStepID(0);
			
			# Set Step Sequence for newly created Step
			$Sequence = count($objStepMapper->findByTaskId($task_id)) + 1;
			$objStep->setSequence($Sequence);

			$_SESSION['IES_FILE_TEMP'][$task_id] = array();
			# Set the Step Handin Object to NULL
			$StepHandin = null;
		}
		
		$returnAry = array(array('UI', $sba_libSba_ui->getStepEditUI($objStep, $StepHandin)));
		
		break;
	
	case 'GetStepEditRaw' :
		# Initialize Objects
		$objStepMapper = new SbaStepMapper();
		$sba_libSba_ui = new libSbaUI();
		
		# Get Step Object
		if($step_id){
			# Get the existing Step
			$objStep = $objStepMapper->getByStepId($step_id);
			
			# Get the Step Handin Object
			$StepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion());
		}
		else{
			$objStep = new step();
			
			# Set Step TaskID for newly created Step
			$objStep->setTaskID($task_id);
			$objStep->setStepID(0);
			
			# Set Step Sequence for newly created Step
			$Sequence = count($objStepMapper->findByTaskId($task_id)) + 1;
			$objStep->setSequence($Sequence);
			
			$_SESSION['IES_FILE_TEMP'][$task_id] = array();
			# Set the Step Handin Object to NULL
			$StepHandin = null;
		}
		
		# Get the Step Handin Object
		$StepHandin = libSbaStepHandinFactory::createStepHandin($objStep->getTaskID(), $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion());
			
		$returnAry = array(
						array('StepTitle', 		 $objStep->getTitle()),
						array('StepDescription', $objStep->getDescription()),
						array('StepSequence', 	 $objStep->getSequence()),
						array('StepAttachmentDisplay',  $sba_libSba_ui->getTaskAttachmentTableUI($objStep->getAttachments(), true, $task_id, $step_id)),
						array('StepToolCaption', (is_null($StepHandin)? '':$StepHandin->getCaption())),
						array('StepToolDisplay', (is_null($StepHandin)? '':$StepHandin->getEditQuestionDisplay()))
					);
		
		break;
		
	case 'GetNewStepHandin' :
		# Initialize variable
		$QuestionType = isset($r_step_QuestionType) && $r_step_QuestionType!=''? $r_step_QuestionType:'';
		
		# Initialize Objects
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $QuestionType);
		
		$returnAry = array(array('UI', $objStepHandin->getEditQuestionDisplay()));
		
		break;
	
	case 'DeleteStep' :
		# Delete the Selected Step
		$StepID_ary[] = $step_id;
		$sba_libSba->deleteSteps($StepID_ary);
		/*
		# Reorder the Step Sequence
		$objStepMapper = new SbaStepMapper();
		$steps_ary = $objStepMapper->findByTaskId($task_id);
		for($i=0;$i<count($steps_ary);$i++){
			$objStep = $steps_ary[$i];
			$objStep->setSequence($i+1);
			$objStepMapper->save($objStep);
		}
		*/
		$returnAry = array(array('ResponseMsg', "已刪除項目"));
		
		break;
	
	case 'StepEditUpdate' :

		# Initialize variable
		$Title		  = isset($r_step_Title) && $r_step_Title!=''? stripslashes($r_step_Title):'';
		$Description  = isset($r_step_Description) && $r_step_Description!=''? stripslashes($r_step_Description):'';
		$QuestionType = isset($r_step_QuestionType) && $r_step_QuestionType!=''? $r_step_QuestionType:'';
		$Question	  = isset($r_step_Question) && $r_step_Question!=''? $r_step_Question:array();
		$Sequence	  = isset($r_step_Sequence) && $r_step_Sequence!=''? $r_step_Sequence:0;
		$remove_Attachment = is_array($r_remove_Attachment)? $r_remove_Attachment: array();
		$Attachments	  = is_array($r_Attachment)?implode(',',array_diff($r_Attachment,$remove_Attachment)):'';
		
		foreach ($remove_Attachment as $file_id){ //remove attachments
		    $sba_libSba->removeAttachment($file_id);
		}
		
		foreach($Question as &$val){
			$val = stripslashes($val);
		}
		
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objStepMapper = new SbaStepMapper();
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $QuestionType, $step_id, $Question);
		
		# Get the Question String from $objStepHandin object by passing an $Question array to it
		$QuestionStr = stripslashes($objStepHandin->getQuestionStr());
		
		# Get Step Object
		if($step_id){
			# Get the existing Step
			$objStep = $objStepMapper->getByStepId($step_id);
			
			global $sba_cfg;
			$old_QuestionType=$objStep->getQuestionType();
			$old_QuestionStr=$objStep->getQuestion();
			
			 //remove all handins if type changed, or columns in table type changed
			if ($QuestionType!=$old_QuestionType){
				$StepID_ary = array($step_id);
				$sba_libSba->deleteStepHandins($StepID_ary);
			}else if ($old_QuestionType==$sba_cfg['DB_IES_STEP_QuestionType']['tablePlanner'][0] && $QuestionStr!=$old_QuestionStr){
				$StepID_ary = array($step_id);
				$sba_libSba->deleteStepHandins($StepID_ary);
			}
		
		}
		else{
			$objStep = new step();
			
			# Set Step Instance for newly created Task
			$objStep->setStepNo(null); // Insert a NULL to StepNo field in table IES_STEP AS StepNo is Useless in SBA
			$objStep->setDateInput(date('Y-m-d H:i:s'));
			$objStep->setInputBy($sba_thisUserID);
			if ($_SESSION['IES_FILE_TEMP'][$task_id]){
			    $objStep->setAttachments(implode(',',$_SESSION['IES_FILE_TEMP'][$task_id]));
			    $_SESSION['IES_FILE_TEMP'][$task_id] = array();
			}
			
		}
		
		# Set Step Instance
		$objStep->setTaskID($task_id);
		$objStep->setTitle($Title);
		$objStep->setDescription($Description);
		$objStep->setQuestionType($QuestionType);
		$objStep->setQuestion($QuestionStr);
		$objStep->setSequence($Sequence);
		$objStep->setAttachments($Attachments);
		$objStep->setSaveToTask(0); // Do not allow step answer to directly save to task
		$objStep->setDateModified(date('Y-m-d H:i:s'));
		$objStep->setModifyBy($sba_thisUserID);
		
		/* Unknown field */
		$objStep->setStatus($Status);
		
		$objStepMapper->save($objStep);
		
		$returnAry = array(
						array('StepID',    $objStep->getStepID()),
						array('StepTitle', $sba_libSba_ui->getTaskIntroStepTabMenuStepBtnUI($objStep, true)),
						array('UI', 	   $sba_libSba_ui->getStepUI($objStep, $objStepHandin, $sba_allow_edit)),
						array('ResponseMsg', "已儲存項目")
					 );
		
		break;
	
	case 'StepHandinUpdate' :
		# Initialize Variable
		$r_answer_id = isset($r_answer_id) && $r_answer_id!=''? $r_answer_id:'';
		$r_stdAnswer = isset($r_stdAnswer) && is_array($r_stdAnswer)? $r_stdAnswer : array();
	
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objStepMapper = new SbaStepMapper();
		
		# Get Step Object
		$objStep = $objStepMapper->getByStepId($step_id);
		
		# Get the Step Handin Object
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion(), $r_answer_id);
		
		# Set Step Handin Property
		$objStepHandin->setStepID($step_id);
		$objStepHandin->setUserID($sba_thisStudentID);
		$objStepHandin->setQuestionCode(null);
		$objStepHandin->setAnswerActualValue(null);
		$objStepHandin->setAnswerExtra(null);
		$objStepHandin->setStudentAnswer($r_stdAnswer);
		$objStepHandin->setDateModified(date('Y-m-d H:i:s'));
		$objStepHandin->setModifyBy($sba_thisStudentID);
		
		# Set Step Handin Property for newly created Step Handin
		if(!$r_answer_id){
			$objStepHandin->setDateInput(date('Y-m-d H:i:s'));
			$objStepHandin->setInputBy($sba_thisStudentID);
		}
	
		$objStepHandin->save();
		
		$returnAry = array(
						array('UI', $sba_libSba_ui->getStepUI($objStep, $objStepHandin, $sba_allow_edit)),
						array('ResponseMsg', "已儲存項目答案")
					);
		
		break;
		
	case 'DeletePowerConcept' : 
		# Initialize Variable
		$r_answer_id 	   = isset($r_answer_id) && $r_answer_id!=''? $r_answer_id:'';
		$r_powerconcept_id = isset($r_powerconcept_id) && $r_powerconcept_id!=''? $r_powerconcept_id:'';
		
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objStepMapper = new SbaStepMapper();
		
		# Get Step Object
		$objStep = $objStepMapper->getByStepId($step_id);
		
		# Get the Step Handin Object
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion(), $r_answer_id);
		
		# Handle the Student Answer
		$del_ans_ary[] = $r_powerconcept_id;
		$std_ans_ary   = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"], $objStepHandin->getRawAnswer());
		$std_ans_ary   = array_merge(array_diff($std_ans_ary, $del_ans_ary));
		
		$objStepHandin->setStudentAnswer($std_ans_ary);
		$objStepHandin->setDateModified(date('Y-m-d H:i:s'));
		$objStepHandin->setModifyBy($sba_thisStudentID);
		
		$objStepHandin->save();
		
		# Delete the PowerConcept in PowerConcept Table
		$sba_libSba->deletePowerConceptRecords($del_ans_ary);
		
		$returnAry = array(
						array('UI', $sba_libSba_ui->getStepUI($objStep, $objStepHandin, $sba_allow_edit)),
						array('ResponseMsg', "已刪除概念圖")
					);
		
		break;
		
	case 'DeleteSurveyAnalysis' :
		# Initialize Variable
		$r_answer_id = isset($r_answer_id) && $r_answer_id!=''? $r_answer_id:'';
		$r_survey_id = isset($r_survey_id) && $r_survey_id!=''? $r_survey_id:'';
		
		# Initialize Objects
		$sba_libSba_ui = new libSbaUI();
		$objStepMapper = new SbaStepMapper();
		
		# Get Step Object
		$objStep = $objStepMapper->getByStepId($step_id);
		
		# Get the Step Handin Object
		$objStepHandin = libSbaStepHandinFactory::createStepHandin($task_id, $objStep->getQuestionType(), $objStep->getStepID(), $objStep->getQuestion(), $r_answer_id);
		
		# Handle the Student Answer
		$del_ans_ary[] = $r_survey_id;
		$std_ans_ary   = explode($ies_cfg["answer_within_question_sperator"], $objStepHandin->getRawAnswer());
		$std_ans_ary   = array_merge(array_diff($std_ans_ary, $del_ans_ary));
		
		$objStepHandin->setStudentAnswer($std_ans_ary);
		$objStepHandin->setDateModified(date('Y-m-d H:i:s'));
		$objStepHandin->setModifyBy($sba_thisStudentID);
		
		//$objStepHandin->save();
		
		$returnAry = array(
						array('UI', $sba_libSba_ui->getStepUI($objStep, $objStepHandin, $sba_allow_edit)),
						array('ResponseMsg', "已撤回已呈文的問卷調查Analysis")
					);
		
		break;
		
	default :
		break;
}

header("Content-Type: text/xml");

$libies = new libies();
$XML = $libies->generateXML($returnAry, true, false);

echo $XML;
?>