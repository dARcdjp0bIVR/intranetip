<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");

$Action = $_POST["Action"];
$SurveyMappingID = base64_decode($_POST["SurveyMappingID"]);
$SurveyMappingIDInSeq = $_POST["SurveyMappingIDInSeq"];


$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
$StudentID = $decodedURIs["StudentID"];
$SurveyID = $decodedURIs["SurveyID"];
switch(strtoupper($Action)) {
	case "REMOVEMAPPING":
		$result = $libies_survey->removeSurveyMapping(array($SurveyMappingID));
		break;
	case "SEQUENCE":
		$result = $libies_survey->updateMappingSequence($SurveyID,$StudentID,$SurveyMappingIDInSeq);
		break;
	default:
		break;
}

# Output the modified date to the page
header("Content-Type:   text/xml");
$ISCDATA = true;
$XML = $libies_survey->generateXML(
					array(
						array("result", ($result?1:0))
					),$ISCDATA
				);
echo $XML;

//intranet_closedb();
?>