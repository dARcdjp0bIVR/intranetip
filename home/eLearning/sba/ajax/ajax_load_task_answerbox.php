<?php
$li = new libdb();
$objIES = new libies();

$_tmpTaskAns = $objIES->gettaskAnswer_withQuestionType($taskID, $UserID);

$taskAns = $_tmpTaskAns["Answer"];
$taskQuestionType = $_tmpTaskAns["QuestionType"];

if (in_array(strtolower($taskQuestionType),array("table","survey:edit","survey:collect"))) {
	$taskAns = $Lang['IES']['TaskAnswerNotAvailable'];
	$display_taskAns = nl2br($taskAns);
} else if(empty($taskAns))
{
	$copy_link = "&nbsp;";
	$display_taskAns = $Lang['IES']['NoRecordAtThisMoment'];
}
else
{
	$copy_link = "<a class=\"tablelink\" href=\"javascript:void(0);\" onclick=\"copy_answer()\">{$Lang['IES']['CopyTaskAnswer']}</a>";
	$display_taskAns = nl2br($taskAns);
}

?>

<table>
<tr><td><?=$copy_link?></td></tr>
<tr><td>
<?=$display_taskAns?>
</td></tr>
</table>
<input type="text" id="taskAnsUG" value="<?=$taskAns?>" style="position:relative; z-index:-20; opacity:0; filter:alpha(opacity=0)">
<input type="hidden" id="taskAnsHidden" value="<?=$taskAns?>" />