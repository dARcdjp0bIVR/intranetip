<?

	$libIES = new libies();
	$result = $libIES->GET_CURRENT_STEP_ANSWER($r_stepId,$sba_thisStudentID,$r_questionType);

	if(is_array($result) && count($result) ==1){

		//assume must return one result 
		$result = current($result);
		$answerStr = $result['Answer'];

		$surveyIdAry = array();
		if($answerStr != ''){
			$surveyIdAry = explode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$answerStr);

			//add the new submit surveyid into the answer
			$surveyIdAry[] = $r_surveyId;

			//unique the  surveyIdAry
			$surveyIdAry = array_unique($surveyIdAry);
			
			$answerStr = implode($ies_cfg["Questionnaire"]["Delimiter"]["Answer_Answer"],$surveyIdAry);
		}

	}else{
		//student has not submit answer before. assign $r_surveyId to $answerStr directly
		$answerStr = $r_surveyId;
	}


	$sql  = 'INSERT INTO '.$intranet_db.'.IES_QUESTION_HANDIN ';
	$sql .= '(StepID,UserID,Answer,QuestionType,QuestionCode,DateInput,InputBy,DateModified,ModifyBy)';
	$sql .= 'VALUES ';
	$sql .= '('.$r_stepId.','.$sba_thisStudentID.', \''.$answerStr.'\', \''.$r_questionType.'\', \''.$r_questionType.'\', NOW(), '.$sba_thisUserID.',NOW(),'.$sba_thisUserID.')';
	$sql .= 'ON DUPLICATE KEY UPDATE Answer = \''.$answerStr.'\' , ModifyBy = '.$sba_thisUserID;



	$li = new libdb();
	$return = $li->db_db_query($sql);

	if($return == 1){
		echo 1;
	}else{
		echo 0;
	}



?>