<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */

$loadType = $loadType;
$stage_id = $stage_id;
$UserID = $UserID;



$objIES  = new libies();
intranet_opendb();
$linterface = new interface_html();

$html_SubmitReport = "<b>".$Lang['IES']['SubmitReport']."</b><br \><br \>".$Lang['IES']['SubmitReportDetail'];

switch (strtoupper($loadType)) {
/** Start Current Part **/
	case "CURRENT":
	$jsFileArray = $objIES->ConvertToJSArrayNew($file_info, "jsFileArray");
	$jsFileArray = "<script>".$jsFileArray."</script>";
	# construct html
	$isSubmitting = $current_batch_id;
	$upload_box_on = false;
	$js_allow_remove_on = false;
	$loadingImg = $linterface->Get_Ajax_Loading_Image();
	if($isRedo || $isSubmitting || !$is_batch_record_exist)
	{
		$upload_box_on = true;
		$js_allow_remove_on = true;
	}
	
	if ($upload_box_on) {
		$html_upload_box = <<<HTMLEND
	        <div class="submission_upload" id="submission_upload">
		        <input type="file" name="submitFile"/><br>
				{$linterface->GET_BTN($Lang['IES']['Save'], "submit")}
				
	        </div>
	        <p id="f1_upload_process" style="display:none">{$Lang['IES']['Loading']}{$Lang['_symbol']["dot"]}{$Lang['_symbol']["dot"]}{$Lang['_symbol']["dot"]}<br/>{$loadingImg}<br/></p>
	        <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:0px solid #fff;"></iframe>
HTMLEND;
	} else {
		$html_upload_box = <<<HTMLEND
	        <div id="submission_upload">
	        </div>
HTMLEND;
	}

	################################################
	##	Start: Main variables for html
	################################################
	$html_tbody = <<<HTMLEND
		<tr>
        <td>
		{$html_upload_box}
        </td>
		</td>
        </tr>
HTMLEND;



//	$onClickLoadCurrent=<<<HTMLEND
//onClick=""
//HTMLEND;
//	$onClickLoadPast=<<<HTMLEND
//onClick="javascript: loadHandinBlock('past');"
//HTMLEND;
//	$current_class_on = " class=\"share_tab_on\" ";

	################################################
	##	End: Main variables for html
	################################################

	
	
		break;
/** End Current Part **/

}


$final_HTML = <<<HTMLEND
<div class="submit_left">
  <div class="submit_right">
    <div class="submit_inner">

      <form id="uploadForm" action="/home/eLearning/sba/index.php?mod=worksheet&task=upload" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="return startUpload();" >
        <table>
          <col style="width:38%" />
          <col style="width:48%" />
          <col style="" />
  
          <tbody>
            {$html_tbody}
          </tbody>
        </table>

        <input type="hidden" name="student_id" value="{$UserID}"/>
        <input type="hidden" name="stage_id" value="{$stage_id}"/>
        <input type="hidden" name="scheme_id" value="{$schemeID}"/>
        <input type="hidden" name="type" value="{$userType}"/>
        <input type="hidden" name="sheetID" value="{$sheetID}"/>
      </form>
    </div>
  </div>
</div>
HTMLEND;

//intranet_closedb();
//# Output the modified date to the page
header("Content-Type:   text/xml");
$libies = new libies();
$XML = $libies->generateXML(
					array(
						array("result", $jsFileArray.$final_HTML),
						array("js_allow_remove_on",($js_allow_remove_on?1:0))
					), true, false
				);
echo $XML;


?>
        