<?php
/** [Modification Log] Modifying By: 
 * 
 * *******************************************
 * 2012-08-22 Mick
 * 	Updated Export Link
 *
 * 
 * 2011-01-11	Thomas	(201101111357)
 * -	Add $libies_survey->set_display_survey_mapping_option(false)
 *		before calling $libies_survey->GetIndividualAnswerDisplay($SurveyID, $i);
 *		to hide survey mapping options
 * *******************************************
 */
 /*
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
*/
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_ui.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
//intranet_auth();
//intranet_opendb();
$key = $_REQUEST["key"];

$hiddenUpdateResult = empty($hiddenUpdateResult)?0:1;

$libies_survey = new libies_survey();
$decodedURIs = $libies_survey->breakEncodedURI($key);
echo $SurveyID = $decodedURIs["SurveyID"];
$FromPage = $decodedURIs["FromPage"];
$StudentID = $decodedURIs["StudentID"];

$_SESSION['thisExportSurveyID'] = $SurveyID;



//$IS_IES_STUDENT = $libies_survey->isIES_Student($UserID);	// Check the current user is student or not
$IS_IES_STUDENT = $libies_survey->isIES_Student($sba_thisUserID);	// Check the current user is student or not

//$html["tag"] = $libies_survey->getQuestionnaireTags(1,$IS_IES_STUDENT);

##################################################
##	HTML - tag
$currentTag = "Overall";
$html["tag"] = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU($currentTag,$IS_IES_STUDENT,$key,$FromPage);
##################################################

############################################
##	HTML - close_button
$html["close_button"] = libies_ui::getCloseButton();
############################################

if(empty($SurveyID)){
	$SurveyID = 0;

	list($FromTask,$surveyCode) = explode(":",$FromPage);

	$Survey_type = $ies_cfg["Questionnaire"]["SurveyType"][$surveyCode][0];
	$html_TypeName = $ies_cfg["Questionnaire"]["SurveyType"][$surveyCode][1];

//	echo "NO SurveyID";
//	exit();
}
$question = $libies_survey->getSurveyQuestion($SurveyID);
$questionsArray = $libies_survey->breakQuestionsString($question);

if ($my_chart_types){
    $libies_survey->updateSurveyQuestionChartTypes($SurveyID, $my_chart_types);
    
}

//DEBUG_PR($questionsArray);
$libies_survey->set_display_survey_mapping_option(false); // Do not display the survey mapping option

// store Survey Question in Javascript
$jsAry = "
						var QuestionTitleAry = new Array();\n
						var QuestionTypeAry = new Array();\n
						";

for($i = 1; $i <= sizeof($questionsArray) ; $i++){
	$jsAry .= "QuestionTitleAry[$i] = \"".$sba_libSba->filename_safe($questionsArray[$i]['TITLE'])."\";\n";	
	$jsAry .= "QuestionTypeAry[$i] = \"".$sba_libSba->filename_safe($questionsArray[$i]['TYPE'])."\";\n";
		
	$displayCreateNewQuestionOption = 2;
	$HTML_TABLE .= $libies_survey->GetIndividualAnswerDisplay($SurveyID, $i,'',$StudentID,$displayCreateNewQuestionOption);
}
if (empty($HTML_TABLE)) {
	$HTML_TABLE = $Lang['IES']['NoRecordAtThisMoment'];
}
if ($IS_IES_STUDENT) {
	//<a href=\"#\" class=\"print\" title=\"\">列印</a>
	$html["print_and_export"] = "<a href='#Export' onclick=\"newWindow('index.php?mod=survey&task=getExport&type=survey',13)\" class=\"export\" title=\"\">{$Lang['IES']['Export_Result']}</a>";//export button appear after all chart posted...see below
	//$html["export_with_chart"] = "<a href=\"javascript:newWindow('index.php?mod=survey&task=export2zip&SurveyID[]=$SurveyID&downloadAction=1&key=$key',10);\" class=\"export\" title=\"\">{$Lang['SBA']['ExportWithChart']}</a>";
}
            
if($SurveyID >0){				
	$surveyDetails = $libies_survey->getSurveyDetails($SurveyID);
	if(is_array($surveyDetails) && count($surveyDetails) == 1){
		$surveyDetails = current($surveyDetails);
	}
}else{
	$surveyDetails["Title"] = $Lang['SBA']['Survey']['StudentInputSurveyName'];
	$surveyDetails["SurveyTypeName"] = $html_TypeName;

}
$html["survey_title"] = $libies_survey->getFormattedSurveyTitle($surveyDetails["SurveyType"],$surveyDetails["SurveyTypeName"],$surveyDetails["Title"],$key,$IS_IES_STUDENT);

$html["survey_title"] = (trim($html["survey_title"]) == '')? $Lang['SBA']['Survey']['StudentInputSurveyName'] : $html["survey_title"];

$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	
$linterface = new interface_html("sba_survey.html");
$nav_arr[] = array($Lang['IES']['Overall'], "");

$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$linterface->LAYOUT_START();
include_once("templates/survey/questionnaireDisplay.tmpl.php");

//intranet_closedb();


/**************************************************************/
$folder_directory = $libies_survey->createTempFolder($sba_thisUserID,$SurveyID);

$curTimestamp = time();
		
for($i = 1; $i <= sizeof($questionsArray) ; $i++){
 
	$chartArr[($i-1).'_bar'] = $libies_survey->GetChartArr($SurveyID, $i,'',$StudentID,2, 'bar');
	if ($libies_survey->isQuestionTypehasPieChart($questionsArray[$i]['TYPE'])){//not pie of likert
	    $chartArr[($i-1).'_pie'] = $libies_survey->GetChartArr($SurveyID, $i,'',$StudentID,2, 'pie');
	}
	
}
//debug_pr($chartArr);


?>

<? include_once("survey/generateChartScript.php");?>
	
	
		
		<script type="text/javascript">
			<?=$jsAry?>
			
		
			function displayChartDiv(questionNo) {
				var ChartLayername = "my_chart_"+questionNo+"parent";
				var ChartTextDiv = "ChartTextDiv"+questionNo;
				
				if(document.getElementById(ChartLayername).style.display=="none") {
					document.getElementById(ChartLayername).style.display = "inline";
					$("#"+ChartTextDiv).html("<?=$Lang['SBA']['HideChart']?>");
				} else {
					document.getElementById(ChartLayername).style.display = "none";
					$("#"+ChartTextDiv).html("<?=$Lang['SBA']['ShowChart']?>");
				}	
			}
			function viewChartType(questionNo, charType){
				$('#'+questionNo+'_'+charType).show().siblings().hide();
			    
			}

			function ofc_ready(i){
			    
		
			    var id = i[0];
			    setTimeout(function(){
				var image_binary = swfobject.getObjectById("my_chart_"+id+"_Final").get_img_binary();
								
				$.post("survey/uploadFlashImage.php", {image_data: image_binary, chart_id: id}, function(data){
				   
				    $("#my_chart_"+id+"_Final").replaceWith(data);
				    
				    
				    if(--chartNum == 0){
					$('.my_chart_types_radio:checked').click();
					
					
					<? if ($callback): ?>
					if (typeof(<?=$callback?>)=='function'){
					    <?=$callback?>();
					}
					<? endif; ?>
				    }
				    
				});
			    }, 2000);
			    
			}
			var chartNum = <?=count($chartArr)?>;
			var dataArr = new Array();
			<?php
				
				foreach( (array)$chartArr as $chartID => $chartJSON ) {
				
					if(count($chartJSON)==0) continue; 
					echo "dataArr['".($chartID)."'] = ".$chartJSON->toPrettyString()."\n";
				
				}
			?>
			
		</script>
		
		<script type="text/javascript">
			var chartNum = <?=count($chartArr)?>;
			
			for (var id in dataArr){
			    
			    swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic/open-flash-chart.swf", "my_chart_"+id+"_Final", <?=$sba_cfg['SBA']['SurveyChart']['ChartWidth']?>, <?=$sba_cfg['SBA']['SurveyChart']['ChartHeight']?>, "9.0.0", "", {id: id});
			    
			}
				

		</script>
	
	<?
	$linterface->LAYOUT_STOP();
	?>