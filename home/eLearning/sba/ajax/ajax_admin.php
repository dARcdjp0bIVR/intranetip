<?php
# using :

include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/sba/libies_static.php");

switch($ajaxAction) {

	case "SchemeInfoUpdate" 	:
			$objIES  = new libies();
			$YearClassID = $_POST['YearClassID'];
			$SelectedStudentList = (is_array($_POST['StudentSelected']))? $_POST['StudentSelected'] : array();
			$student_arr = $objIES->GET_IES_STUDENT_ARR($YearClassID, $SelectedStudentList);

			$json = new JSON_obj();
			$json_str = $json->encode($student_arr);
			echo $json_str;

			break;

	case "CalculateSchemeMark"	:
	error_log("Area 100<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/sbaCheckLog.txt");
			echo libies_static::calculateSchemeMark($scheme_id);

			break;

	case "DeleteComment"		:

			$objIES = new libies();

			$sql = "DELETE FROM {$intranet_db}.IES_TASK_HANDIN_COMMENT WHERE SnapshotCommentID = $SnapshotCommentID";
			$res = $objIES->db_db_query($sql);

			if($res)
			{

			    $commentStr = "";
			    if(!empty($snapshotID))
			    {
			      $commentArr = libies_static::getTaskCommentArr($snapshotID);
			      for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
			      {
			        $t_comment = nl2br($commentArr[$j]["Comment"]);
			        $t_inputdate = $commentArr[$j]["DateInput"];
			        $t_teacher = $commentArr[$j]["CommentTeacher"];

			        $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
			        $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:void(0);\" onclick=\"DeleteComment('".$commentArr[$j]["SnapshotCommentID"]."', $snapshotID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
				$commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
			        $commentStr .= "<div class=\"edit_top\"></div>";
			      }
			    }
			    echo $commentStr;
			}

			break;

	case "DeleteStageComment"		:

			$objIES = new libies();



			$sql = "DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_COMMENT WHERE BatchCommentID = '$BatchCommentID'";
			$res = $objIES->db_db_query($sql);

			if($res)
			{
			    $commentArr = libies_static::getBatchCommentArr($batchID);
			    for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
			    {
			      $t_comment = nl2br($commentArr[$j]["Comment"]);
			      $t_inputdate = $commentArr[$j]["DateInput"];
			      $t_teacher = $commentArr[$j]["CommentTeacher"];

			      $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
			      $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:;\" onclick=\"DeleteBatchComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
				  $commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
			     $commentStr .= "<div class=\"edit_top\"></div>";

			    }
			    echo $commentStr;
			}

			break;

	case "AddComment"		:

			$objIES = new libies();

			$objIES->Start_Trans();

			$skipInputNameAry = array('mod' , 'task','ajaxaction');
			foreach($_POST AS $input_name => $input_val){

				if(in_array(strtolower($input_name),$skipInputNameAry)){
					//if the input name in the skip input name array , skip this action handling
					continue;
				}

			  $old_num = $input_name;
			  list($snapshotID, $taskID, $studentID) = explode("_", base64_decode($input_name));

			  if(empty($snapshotID))
			  {
			    // snapshot current answer
			    $sql = "INSERT INTO {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT ";
			    $sql .= "(TaskID, UserID, Answer, QuestionType, AnswerTime, DateInput, InputBy) ";
			    $sql .= "SELECT TaskID, UserID, Answer, QuestionType, DateModified, NOW(), {$sba_thisUserID} ";
			    $sql .= "FROM IES_TASK_HANDIN WHERE TaskID = '{$taskID}' AND UserID = '{$studentID}'";
			    $res[] = $objIES->db_db_query($sql);

			    $snapshotID = $objIES->db_insert_id();
			  }

			  $sql = "INSERT INTO {$intranet_db}.IES_TASK_HANDIN_COMMENT ";
			  $sql .= "(SnapshotAnswerID, Comment, InputBy) VALUES ";
			  $sql .= "({$snapshotID}, '".mysql_real_escape_string(stripslashes($input_val))."', {$sba_thisUserID})";


			  $res[] = $objIES->db_db_query($sql);

			  $final_res = (count($res) == count(array_filter($res)));

			  if($final_res)
			  {
			    $objIES->Commit_Trans();
			        $commentStr = "";
			    if(!empty($snapshotID))
			    {
			      $commentArr = libies_static::getTaskCommentArr($snapshotID);

			      for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
			      {
			        $t_comment = nl2br(intranet_htmlspecialchars($commentArr[$j]["Comment"]));
			        $t_inputdate = $commentArr[$j]["DateInput"];
			        $t_teacher = $commentArr[$j]["CommentTeacher"];

			        $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
			        $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:void(0);\" onclick=\"DeleteComment('".$commentArr[$j]["SnapshotCommentID"]."', $snapshotID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
				    $commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
			     	$commentStr .= "<div class=\"edit_top\"></div>";
			      }
			    }

			    $answerEncodeNo = base64_encode($snapshotID."_".$taskID."_".$studentID);

			    $XML = $objIES->generateXML(
								array(
									array("comment", $commentStr),
									array("new_code", $answerEncodeNo),
									array("old_code", $old_num)
								), TRUE
							);

				header("Content-Type:   text/xml");

				echo $XML;

			  }
			  else
			  {
			    $objIES->RollBack_Trans();
			  }
			}
			break;


	case "AddStageComment"		:

			$objIES = new libies();


			$objIES->Start_Trans();

			foreach($_POST AS $input_name => $input_val){
			  $batchID = base64_decode($input_name);

			  $sql = "INSERT INTO {$intranet_db}.IES_STAGE_HANDIN_COMMENT ";
			  $sql .= "(BatchID, Comment, InputBy) VALUES ";
			  $sql .= "({$batchID}, '".mysql_real_escape_string(stripslashes($input_val))."', {$sba_thisUserID})";
			  $res = $objIES->db_db_query($sql);

			  if($res)
			  {
			    $objIES->Commit_Trans();
			    $objIES->updateStudentStageBatchHandinStatus($stage_id, $StudentID, STATUS_TEACHER_READ, STATUS_SUBMITTED);
			    $commentArr = libies_static::getBatchCommentArr($batchID);

			    for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
			    {
			      $t_comment = nl2br($commentArr[$j]["Comment"]);
			      $t_inputdate = $commentArr[$j]["DateInput"];
			      $t_teacher = $commentArr[$j]["CommentTeacher"];

			      $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
			      $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:;\" onclick=\"DeleteBatchComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
				  	$commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
			      $commentStr .= "<div class=\"edit_top\"></div>";
			    }

			    echo $commentStr;
			  }
			  else
			  {
			    $objIES->RollBack_Trans();

			  }
			}
			break;



	case "GetComment"		:

			$categoryID = $_POST["categoryID"];
			$answerEncodeNo = $_POST["answerEncodeNo"];
			$language = $_POST["language"];

			$objIES = new libies();


			$comments = $objIES->getCommentBank($categoryID, $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);
			if (is_array($comments) && count($comments)>0) {
				switch(strtolower($language)) {
					default:
					case "b5":
					$commentInLang = '';
					break;
					case "en":
					$commentInLang = 'EN';
					break;
				}
				$c_no = 0;
				$h_comment .= "<ul>";
				foreach($comments as $key => $element) {
					if (empty($element["Comment".$commentInLang])) {
						continue;
					}
					$h_comment .= "<li onclick=\"add_from_commentbank(this,'h_commentbank_$c_no', 'text_{$answerEncodeNo}')\">".$element["Comment".$commentInLang]."</li><input type=\"hidden\" id=\"h_commentbank_$c_no\" VALUE=\"".$element["Comment".$commentInLang]."\">";
		        	$c_no++;
				}
				$h_comment .= "</ul>";
			}

			header("Content-Type:   text/xml");

			$ParCData = true;
			$XML = generate_XML(
								array(
									array("h_comment", $h_comment),
								),$ParCData
							);
			echo $XML;

			break;


	case "AddMark"		:

			$objIES = new libies();

			$scoreStr = '';

			foreach($_POST AS $input_name => $input_val){

				if($input_name=="mod" || $input_name=="task" || $input_name=="ajaxAction") continue;

			  $old_num = $input_name;

				list($snapshotID, $taskID, $studentID) = explode("_", base64_decode($input_name));

				$createNewScoreSqlFlag = NULL;

				if(empty($snapshotID)){
					// snapshot current answer
					$sql = "INSERT INTO {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT ";
					$sql .= "(TaskID, UserID, Answer, QuestionType, AnswerTime, DateInput, InputBy) ";
					$sql .= "SELECT TaskID, UserID, Answer, QuestionType, DateModified, NOW(), {$sba_thisUserID} ";
					$sql .= "FROM IES_TASK_HANDIN WHERE TaskID = {$taskID} AND UserID = {$studentID}";
					$res[] = $objIES->db_db_query($sql);

					$snapshotID = $objIES->db_insert_id();

					$createNewScoreSqlFlag = true;
				}else{

					$scoreIsGiveBefore = libies_static::getTaskScoreArr($snapshotID);

					if(is_array($scoreIsGiveBefore) && count($scoreIsGiveBefore) > 0){
						// there is a score for the snapshot, no use new score sql
						$createNewScoreSqlFlag = false;
					}else{
						$createNewScoreSqlFlag = true;
					}
				}

				$scoreSql = '';
				if($createNewScoreSqlFlag == true){
					$scoreSql  = 'insert into '.$intranet_db.'.IES_TASK_HANDIN_SCORE ';
					$scoreSql .= "(SnapshotAnswerID, Score, InputBy) VALUES ";
					$scoreSql .= "({$snapshotID}, ".$input_val.", {$sba_thisUserID})";
				}else{
					$scoreSql = 'update '.$intranet_db.'.IES_TASK_HANDIN_SCORE set Score = '.$input_val.'  , InputBy = '.$sba_thisUserID.' ,DateInput = now() where SnapshotAnswerID = '.$snapshotID;
				}

				if($scoreSql != ''){
					$res[] = $objIES->db_db_query($scoreSql);
				}


				$final_res = (count($res) == count(array_filter($res)));
				if($final_res)
				{

					$scoreArr = libies_static::getTaskScoreArr($snapshotID);

					if(is_array($scoreArr) && count($scoreArr) == 1){
						//suppose return one array only
						$scoreArr = current($scoreArr);
						$_score = $scoreArr['score'];
						$t_inputdate = $scoreArr['DateInput'];
						$t_teacher = $scoreArr['scoreTeacher'];

						$scoreStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";

					}
				}

				   $answerEncodeNo = base64_encode($snapshotID."_".$taskID."_".$studentID);

				   $XML = $objIES->generateXML(
						array(
							array("returnStr", $scoreStr), array("new_code", $answerEncodeNo),array("old_code", $old_num)
						), TRUE
					);
				header("Content-Type:   text/xml");
				echo $XML;
			}

			break;

	case "StageMarkUpdate" :

			$objIES = new libies();
			$_assignmentID;

			$objIES->Start_Trans();
			foreach($mark AS $key => $markArr)
			{
			  $_assignmentID = base64_decode($key);

			  foreach($markArr AS $markCriteria => $score)
			  {
			    $sql = "INSERT INTO {$intranet_db}.IES_MARKING ";
			    $sql .= "(AssignmentID, AssignmentType, MarkCriteria, StageMarkCriteriaID, MarkerID, Score, DateInput, InputBy, ModifyBy) ";
			    $sql .= "SELECT {$_assignmentID}, {$AssignmentType}, {$markCriteria}, ismc.StageMarkCriteriaID, {$sba_thisUserID}, {$score}, NOW(), {$sba_thisUserID}, {$sba_thisUserID} ";
			    $sql .= "FROM {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ";
			    $sql .= "INNER JOIN {$intranet_db}.IES_STAGE_HANDIN_BATCH ishb ";
			    $sql .= "ON ismc.StageID = ishb.StageID ";
			    $sql .= "WHERE ishb.BatchID = {$_assignmentID} AND ismc.MarkCriteriaID = {$markCriteria} ";
			    $sql .= "ON DUPLICATE KEY UPDATE Score = VALUES(Score), MarkerID = VALUES(MarkerID), ModifyBy = VALUES(ModifyBy)";
			    $res[] = $objIES->db_db_query($sql);

			    if($markCriteria == MARKING_CRITERIA_OVERALL)
			    {
			      $sql = "INSERT INTO {$intranet_db}.IES_STAGE_STUDENT ";
			      $sql .= "(StageID, UserID, Score, DateInput, InputBy, ModifyBy) ";
			      $sql .= "VALUES ";
			      $sql .= "({$stage_id}, {$StudentID}, {$score}, NOW(), {$sba_thisUserID}, {$sba_thisUserID})";
			      $sql .= "ON DUPLICATE KEY UPDATE Score = VALUES(Score), ModifyBy = VALUES(ModifyBy)";
			      $res[] = $objIES->db_db_query($sql);
			    }
			  }
			}

			$final_res = (count($res) == count(array_filter($res)));
			if($final_res)
			{
			  $objIES->Commit_Trans();
			  $objIES->updateStudentStageBatchHandinStatus($stage_id, $StudentID, STATUS_TEACHER_READ, STATUS_SUBMITTED);
			  $lastMarkerArr = libies_static::getLastMarker($_assignmentID, $ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]);
			  $markerName = $lastMarkerArr["MarkerName"];
			  $lastModifiedDate = $lastMarkerArr["LastModified"];
			  $html_latest_modified = empty($lastMarkerArr) ? "" : "({$lastModifiedDate} {$markerName})";
			  echo $html_latest_modified;
			}
			else
			{
			  $objIES->RollBack_Trans();

			}

			// re-calculate scheme total mark after weight and max mark update
	error_log("Area 200<----".date("Y-m-d H:i:s")." f:".__FILE__."\n", 3, "/tmp/sbaCheckLog.txt");
			libies_static::calculateSchemeMark($scheme_id,$StudentID);

			break;

	case "RedoStage"		:

			$objIES = new libies();

			$sql = "SELECT StageID, UserID FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH WHERE BatchID = {$batch_id}";
			list($stage_id, $student_id) = current($objIES->returnArray($sql));

			$objIES->Start_Trans();
			//DEBUG_R($batch_status);
			switch($batch_status)
			{
			  case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]:
			    $sql = "UPDATE {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
			    $sql .= "SET BatchStatus = {$batch_status}, ModifyBy = {$sba_thisUserID} ";
			    $sql .= "WHERE BatchID = {$batch_id}";
			    $res = $objIES->db_db_query($sql);

			    $objIES->updateStudentStageBatchHandinStatus($stage_id, $student_id, $batch_status, STATUS_TEACHER_READ);
			    break;
			  case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["approved"]:
			    // Batch Approval
			    $sql = "UPDATE {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
			    $sql .= "SET BatchStatus = {$batch_status}, ModifyBy = {$sba_thisUserID} ";
			    $sql .= "WHERE BatchID = {$batch_id}";
			    $res[] = $objIES->db_db_query($sql);

			    // Stage Approval
			    $sql = "INSERT INTO {$intranet_db}.IES_STAGE_STUDENT ";
			    $sql .= "(StageID, UserID, Approval, ApprovedBy, DateInput, InputBy, ModifyBy) SELECT ";
			    $sql .= "StageID, UserID, {$ies_cfg["DB_IES_STAGE_STUDENT_Approval"]["approved"]}, {$sba_thisUserID}, NOW(), {$sba_thisUserID}, {$sba_thisUserID} ";
			    $sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
			    $sql .= "WHERE BatchID = {$batch_id} ";
			    $sql .= "ON DUPLICATE KEY UPDATE Approval = VALUES(Approval), ApprovedBy = VALUES(ApprovedBy), ModifyBy = VALUES(ModifyBy)";
			    $res[] = $objIES->db_db_query($sql);

			    // Batch Handin Status in Stage
			    $objIES->updateStudentStageBatchHandinStatus($stage_id, $student_id, $batch_status);

			    $res = (count($res) == count(array_filter($res)));
			    break;
			  default:
			    break;
			}

			if($res)
			{
			  $objIES->Commit_Trans();
			  echo 1;
			}
			else
			{
			  $objIES->RollBack_Trans();
			  echo 0;
			}

			break;


	default		:
			break;
}


?>