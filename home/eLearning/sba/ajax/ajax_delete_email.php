<?php
/*
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();
*/
$SurveyEmailID = base64_decode($SurveyEmailID);

if(is_numeric($SurveyEmailID))
{
  $li = new libdb();
  
  $li->Start_Trans();
  
  $sql = "DELETE FROM {$intranet_db}.IES_SURVEY_EMAIL ";
  $sql .= "WHERE SurveyEmailID = {$SurveyEmailID}";

  $res = $li->db_db_query($sql);
  
  if($res)
  {
    $msg = 1;
    $li->Commit_Trans();
  }
  else
  {
    $msg = 0;
    $li->RollBack_Trans();
  }
  
  echo $msg;
}

//intranet_closedb();
?>