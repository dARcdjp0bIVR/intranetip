<?php
// modifying by : 

$ldb = new libdb();
if($f_type=='admin') {
	$conds = " WHERE SchemeType='".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."' ";
	if($searchScheme!="") {
		$conds = " AND Title LIKE '%$searchScheme%'";
	}

	$sql = "SELECT SchemeID, Title FROM IES_SCHEME $conds ORDER BY Title";
	$schemeData = $ldb->returnArray($sql,2);

	$select = '<select name="AvailableSchemeID[]" size="10" style="width:99%" multiple>';
		
	for($i=0; $i<sizeof($schemeData); $i++) {
		$select .= '<option value="'.$schemeData[$i][0].'">'.$schemeData[$i][1].'</option>';
	}
		
	$select .= '</select>';

	echo $select;
}
else if($f_type=='user') {
	if($searchScheme!="")
		$conds = " AND (faq.Question LIKE '%{$searchScheme}%' OR faq.Answer LIKE '%{$searchScheme}%')";
	
	$sql = "SELECT faq.QuestionID, Question, Answer, AnswerDate 
	FROM IES_FAQ faq 
	LEFT OUTER JOIN IES_FAQ_RELATIONSHIP rel ON (rel.QuestionID=faq.QuestionID) 
	LEFT JOIN IES_SCHEME SS ON (SS.SchemeID=faq.FromSchemeID) 
	WHERE faq.RecordStatus=".$ies_cfg["faq_recordStatus_approved"]."   
	AND SS.SchemeType='".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."' 
	AND (faq.AssignType=".$ies_cfg["faq_assignType_to_all"]."  
	OR rel.SchemeID={$scheme_id})  
	AND (Answer IS NOT NULL OR Answer!='') $conds GROUP BY faq.QuestionID ORDER BY faq.DateInput";
	$result = $ldb->returnArray($sql,3);
	$faqContent = "";
	for($i=0; $i<sizeof($result); $i++) {
		$faqContent .= "<li><a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">{$result[$i][1]}</a><div id='spanQuestion{$result[$i][0]}' style='display:none;'>".nl2br($result[$i][2])." <a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">({$Lang['IES']['Hide']})</a></div><input type='hidden' name='field{$result[$i][0]}' id='field{$result[$i][0]}' value='0'></li>";	
	}
	$faqContent = ($faqContent=="") ? "沒有記錄" : "<ul class='IES_faq_list'>{$faqContent}</ul>";
	
	echo $faqContent;
}

?>