
<script language="JavaScript">
var editing = 0;
var org = '';
function AddComment(){
	if(!editing){
		$('#add_row').fadeIn(0);
		$('.edit_dim').hide();
		$('.delete_dim').hide();
		editing = 1;
	}
}

function CancelAddComment(){
	if(editing){
		$('.edit_dim').show();
		$('.delete_dim').show();
		$('#add_row').fadeOut(0);
		$('#addcomment').val('');
		editing = 0;
	}
}

function RemoveComment(id) {
	if(!editing){
		if(confirm('<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>'))	{
			document.form1.action = 'remove.php?id='+id;
			document.form1.submit();
		}
	}
}

function EditComment(id){
	if(!editing){
		org = $('#comment_'+id).html();	
		$('#comment_'+id).html("<textarea rows=\"4\" id=\"editcomment\" name=\"editcomment\">"+$('#h_comment_'+id).val()+"</textarea>");
		$('.edit_dim').hide();
		$('.delete_dim').hide();
		$('#button_add_'+id).show();
		$('#button_cancel_'+id).show();
		editing = 1;
	}
	

}

function CancelEdit(id){
	if(editing){
		$('#button_add_'+id).hide();
		$('#button_cancel_'+id).hide();
		$('.edit_dim').show();
		$('.delete_dim').show();
		$('#comment_'+id).html(org);
		editing = 0;
	}
}

function SubmitForm(type, id){
	if(type == "new"){
		document.form1.action="insert_comment.php";
		document.form1.submit();
	}
	else if(type == "edit"){
		document.form1.action="update.php?id="+id ;
		document.form1.submit();
	}
}



function CheckForm(type, id){
	if(type == "new"){
		if($('#addcomment').val().length != 0){
			SubmitForm(type);
		}
		else{
			alert ("<?=$Lang['IES']['CommentWarning']?>");
		}
	}
	else if(type == "edit"){
		if($('#editcomment').val().length != 0){
			SubmitForm(type, id);
		}
		else{
			alert ("<?=$Lang['IES']['CommentWarning']?>");
		}
	}
}

</script>
<form name="form1" method="POST" >
<table width="100%">
  <tr>
    <td align="right"><?=$html_message?></td>
  </tr>
</table>
<table width="100%">
  <tr>
    <td align="right"></td>
  </tr>
</table>

   <div class="content_top_tool">
   <div class="table_board">
   <table class="common_table_list">
	<thead>
	  <tr>
	  <th class="num_check">#</th>
	  <th width=80%><?=$Lang['IES']['Comment']?></th>
	  <th>&nbsp;</th>
	  </tr>
	</thead>
	<tbody>
	  <?=$html_scheme_list?>
	  <tr>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>
	  <td><div class="table_row_tool row_content_tool"><a href="javascript:void(0);" onclick="AddComment()" class="add" title="<?=$Lang['IES']['AddComment']?>"></a></div></td>
	  </tr>	
	</tbody>
	</table>
   </div>
</div>
             
</form>
