<?php //editing by : ?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/2009a/js/script.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.core.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.draggable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.droppable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/ui.selectable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.jeditable.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.cookies.2.2.0.js"></script>
				

<SCRIPT LANGUAGE="javascript">

$(document).ready(function(){

  // Bind action to scheme selection
  $("select[name=scheme_id]").change(function(){
    var scheme_id = $(this).val();
    // jump to final submission if $stage_id is empty
    var stage_id_str = "<?= (empty($stage_id) ? "&stage_id=" : "") ?>";
    
    window.location = "index.php?mod=admin&task=studentList&scheme_id="+scheme_id+stage_id_str;
  });
  
  // Calculate scheme mark
  $("#calSchemeMark").click(function(){
  		<? if($IsMarkingCrieteriaComplete==$sba_cfg['MarkingCriteriaCompleteStatus']['Complete']) {?>
  	
		$.ajax({
	    url:      "index.php",
	    type:     "POST",
	    data: 	  "mod=ajax&task=ajax_admin&ajaxAction=CalculateSchemeMark&scheme_id=<?=$scheme_id?>",
	    error:    function(xhr, ajaxOptions, thrownError){
	                alert(xhr.responseText);
	              },
	    success:  function(){
						window.location.reload();
	              }
	  });
	  
	  <? } else if($IsMarkingCrieteriaComplete==$sba_cfg['MarkingCriteriaCompleteStatus']['NotComplete']) { ?>
	  		alert("<?=$Lang['SBA']['AlertMsg']['MarkingCriteriaNotComplete'] ?>");	
	  <? } else if($IsMarkingCrieteriaComplete==$sba_cfg['MarkingCriteriaCompleteStatus']['NoStage']) { ?>
	  		alert("<?=$Lang['SBA']['AlertMsg']['MarkingCriteriaNoStage'] ?>");
	  <? } ?>
	});
	

});

function Show_ipFileList(studentID){
	$.post(
		"/ipfiles/viewStudentFile.php", 
		{ 
			studentID: studentID,
			moduleCode:"<?=$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']?>"
//			ole_title: ole_title,
//			PATH_WRT_ROOT: "../../../../"
			 },
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);	
}
</script >

<!-- navigation start -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<br style="clear:both" />

<table class="form_table" style="width:100%">
  <col class="field_title" />
  <col  class="field_c" />
  <tr>
    <td><?=$Lang['IES']['Scheme']?></td>
    <td>:</td>
    <td >
    <?=$html_scheme_option?>
      
    </td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['TeachingTeacher']?></td>
    <td>:</td>
    <td><?=$html_TeachingTeacher?></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['AssessTeacher']?></td>
    <td>:</td>
    <td><?=$html_AssessTeacher?></td>
  </tr>
</table>
<br style="clear:both" />
<!--a href = "testRubric.php" target="_blank">Rubric</a-->
<?=$html_tab_nav?>
<? if ($stage_id): ?>
<div class="content_top_tool">
  <div class="Conntent_tool"><a href="index.php?mod=admin&task=stage_score_export&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>" class="export"><?=$Lang['IES']['Export']?></a></div>
  <br style="clear:both" />	
</div>
<? endif; ?>
<div class="table_board">
  <!--<div class="common_table_tool">
  <a href="#" class="marks_release">{{分數發布}}</a>
  
  </div>-->
  <div class="content_top_tool">
    <div class="Conntent_tool">
		<?=$html_export_score_lnk?>
    <!--<a href="#" class="print"> Print</a>		-->				</div>
    
    <?=$html_student_list?>

    <br />
    <span class="symbol_remark">
      <?=$Lang['IES']['Remark']?>&nbsp;&nbsp;&nbsp;
      <?=$html_legend?>
      <p class="spacer"></p>
    </span>
  </div>
</div>
