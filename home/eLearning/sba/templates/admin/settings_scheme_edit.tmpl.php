<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
?>
<script language="JavaScript">
function Check_Form() {

//$("input:checkbox[id^='r_approvalStep_id_']").each(function(){

	$(".WarningDiv").each(function(){
		$(this).hide();
	});

  if($("#scheme_name").val() == "")
  {
	  $('#schemeNameInputErrorDiv').show();
	  $("#scheme_name").focus();
      return false;
  }

  $("#StudentSelected").each(function(){
    $("#StudentSelected option").attr("selected","selected");
  });
  $("#SelectedTeachingTeacher").each(function(){
    $("#SelectedTeachingTeacher option").attr("selected","selected");
  });
  $("#SelectedAssessTeacher").each(function(){
    $("#SelectedAssessTeacher option").attr("selected","selected");
  });
  return true;

}

function Add_Teaching_Teacher() {

  var selectedOption = $("#TeachingTeacherList option:selected");
  
  if($(selectedOption).val() != "") {
    $('#SelectedTeachingTeacher').append($('<option></option>').text($(selectedOption).text()).val($(selectedOption).val()));
    $(selectedOption).remove();
    Reorder_Selection_List('SelectedTeachingTeacher', false);
  }
  
  $('#SelectedTeachingTeacher').width('350');
}

function Remove_Teaching_Teacher() {
  
  var selectedOptions = $("#SelectedTeachingTeacher option:selected");
  
  $(selectedOptions).each(function(){
    $("#TeachingTeacherList").append($('<option></option>').text($(this).text()).val($(this).val()));
    $(this).remove();
  });
  
  Reorder_Selection_List('TeachingTeacherList', false);
}

function Add_Assess_Teacher() {

  var selectedOption = $("#AssessTeacherList option:selected");
  
  if($(selectedOption).val() != "") {
    $('#SelectedAssessTeacher').append($('<option></option>').text($(selectedOption).text()).val($(selectedOption).val()));
    $(selectedOption).remove();
    Reorder_Selection_List('SelectedAssessTeacher', false);
  }
  
  $('#SelectedAssessTeacher').width('350');
}

function Remove_Assess_Teacher() {

  var selectedOptions = $("#SelectedAssessTeacher option:selected");
  
  $(selectedOptions).each(function(){
    $("#AssessTeacherList").append($('<option></option>').text($(this).text()).val($(this).val()));
    $(this).remove();
  });
  
  Reorder_Selection_List('AssessTeacherList', false);
}

function Reorder_Selection_List(selectId, sortClassName) {

	
	var select_options = $('#'+selectId+' option').sort(function(a, b){
	    
	    var a_value = $(a).text();
	    var b_value = $(b).text();
	    
	    if (sortClassName){
		a_value = a_value.substring(a_value.indexOf('(')+1);
		b_value = b_value.substring(b_value.indexOf('(')+1);
	    }
	    
	    return a_value > b_value? 1 : -1;
	    
	});

	$('#'+selectId).html(select_options);
	

}

function Get_Class_Student_List() {

	var YearClassID = $("#YearClassSelect :selected").val();
	var SelectedStudentID = "";
	$("#StudentSelected option").each(function(i){
    SelectedStudentID += "&StudentSelected[]=" + $(this).val();
  });
  
  $("#ClassStudent").find("option").remove().end();
	
	var url = "index.php";
	var postContent = "YearClassID="+YearClassID;
	postContent += SelectedStudentID;
	postContent += "&mod=ajax&task=ajax_admin&ajaxAction=SchemeInfoUpdate";
	
  $.ajax({
    type: "POST",
    url: url,
    data: postContent,
    success: function(data) {
    	//$("#tempdiv").html(data);
      var student_arr = eval(data);
     
      $.each(student_arr, function(key, obj) {
        var student_id = obj.UserID;
        var student_name = obj.StudentName;
      
        $("#ClassStudent").append($("<option></option>").attr("value",student_id).text(student_name));   
        $("#ClassStudent").width($("#ClassStudent").parent().width());
        
      });
      
      
    }
  });
}

function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    var student_id = $(this).val();
    var student_name = $(this).text();
	
    $("#"+to_select_id).append($("<option></option>").attr("value",student_id).text(student_name));
    $(this).remove();
  });
  
  $("#"+from_select_id).width($("#"+from_select_id).parent().width());
  $("#"+to_select_id).width($("#"+to_select_id).parent().width());
  Reorder_Selection_List(from_select_id, true);
  Reorder_Selection_List(to_select_id, true);
}

$(document).ready(function(){
	
  $('#SelectedTeachingTeacher').width('350');
  $('#SelectedAssessTeacher').width('350');

  $("#AddAll").click(function(){
    $("#ClassStudent option").each(function(){$(this).attr("selected","selected");}); 
    MoveSelectedOptions("ClassStudent", "StudentSelected");
  });
  
  $("#Add").click(function(){
    MoveSelectedOptions("ClassStudent", "StudentSelected");
  });

  $("#Remove").click(function(){
    MoveSelectedOptions("StudentSelected", "ClassStudent");
  });

  $("#RemoveAll").click(function(){
    $("#StudentSelected option").each(function(){$(this).attr("selected","selected");});
    MoveSelectedOptions("StudentSelected", "ClassStudent");
  });
  
  $("#scheme_new").submit(function() {
    return Check_Form();
  });
  
  $("#ClassStudent").width($("#ClassStudent").parent().width());
  $("#StudentSelected").width($("#StudentSelected").parent().width());
  
  Get_Class_Student_List();
});
function checkTitleDuplicate(){
	var title = Trim($('#scheme_name').val()); 
	/*
	$('div.WarningDiv' , 'form#scheme_new').hide();
	
		$.post(
		"ajax/ajax_handler.php", 
		{
			Action:'check_scheme_title',
			schemeTitle: title
		},
		function(ReturnData) {
			if(ReturnData == 1){ //isDuplicate
				$('div#CodeMustNotDuplicateWarningDiv' , 'form#scheme_new').show();

			}else{
				$('div#CodeMustNotDuplicateWarningDiv' , 'form#scheme_new').hide();
			}
		}
	);
	*/
}


</script>
<div id=tempdiv></div>
<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<div align="right">
<?=$html_message?>
</div>

<br style="clear:both" />
<form id="scheme_new" method="POST" action="index.php">
<?=$html_functionTab?>
<table class="form_table">
  <!--col class="field_title" />
  <col class="field_c" /-->

<?
//[Start] please don't delete support with two lang in future FAI 20101013
?>
  <!--tr>
    <td><?=$Lang['IES']['Scheme']?></td>
    <td>:</td>
    <td><?=$h_schemeTitle?></td>
  </tr-->

<?
//echo $h_RadioLang; //hard code to use BIG5 first , should be remove while support with select lang
//[END] please don't delete support with two lang in future FAI 20101013

?>

  <tr>
    <tr>
    <td><?=$Lang['IES']['SchemeLangCaption']?><span class="tabletextrequire">*</span></td>
    <td>:</td>
    <td><?=$h_RadioLang?></td>
  </tr>
  <tr>
    <td>
    	<?=$Lang['IES']['SchemeTitle']?><span class="tabletextrequire">*</span>
    	
    </td>
    <td>:</td>
    <td>
    	<input name="scheme_name" type="text" id="scheme_name" class="textbox" onchange="checkTitleDuplicate()" value="<?=intranet_htmlspecialchars($html_schemeTitle)?>"/>
    	<?=$linterface->Get_Form_Warning_Msg('CodeMustNotDuplicateWarningDiv', $Lang['IES']['SchemeTitleDuplicate'], $Class='WarningDiv')?>
		<?=$linterface->Get_Form_Warning_Msg('schemeNameInputErrorDiv', "Please input Name", $Class='WarningDiv')?>
    </td>
  
  </tr>

  <tr>
    <tr>
    <td><?=$Lang['IES']['Status']?><span class="tabletextrequire">*</span></td>
    <td>:</td>
    <td><?=$h_schemeStatus?></td>
  </tr>

  <tr>
    <td>
    	<?=$Lang['IES']['SchemeIntroduction']?>
    	
    </td>
    <td>:</td>
    <td>
    	<?=$linterface->GET_TEXTAREA("r_schemeIntro", $html_schemeIntro);?>
    </td>
  
  </tr>


  <tr>
    <td><?=$Lang['IES']['TeachingTeacher']?></td>
    <td>:</td>
    <td>
    <?=$html_teaching_teacher_selection?>
    <p class="spacer"></p>
      <select name="SelectedTeachingTeacher[]" size="3" id="SelectedTeachingTeacher" multiple="true">
		<?=$html_selected_teaching_teacher?>
      </select> 
    <p class="spacer"></p>
    <input onclick="Remove_Teaching_Teacher();" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<?=$Lang['Btn']['RemoveSelected']?>"/></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['AssessTeacher']?></td>
    <td>:</td>
    <td>
      <?=$html_assess_teacher_selection?>
      <p class="spacer"></p>
      <select name="SelectedAssessTeacher[]" size="3" id="SelectedAssessTeacher" multiple="true">
		<?=$html_selected_assess_teacher?>
      </select> 
      <p class="spacer"></p>
      <input onclick="Remove_Assess_Teacher();" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<?=$Lang['Btn']['RemoveSelected']?>"/></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['Student']?></td>
    <td>:</td>
    <td>
  		<table width="100%" border="0" cellspacing="0" cellpadding="5">
  			<tr>
  				<td width="50%" bgcolor="#EEEEEE"><!--<?=$html_student_search_input?>--></td>
					<td width="40">&nbsp;</td>
					<td width="50%" bgcolor="#EFFEE2" class="steptitletext"><?=$Lang['SysMgr']['FormClassMapping']['StudentSelected']?> </td>
				</tr>
				<tr>
					<td bgcolor="#EEEEEE" align="center">
  					<?=$html_year_class_selection?>
  					<?=$html_student_source_selection?>
  					<br/>
						<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?> </span>
          </td>
          <td>
            <input id="AddAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddAll']?>"/><br />
            <input id="Add" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddSelected']?>"/><br /><br />
            <input id="Remove" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveSelected']?>"/><br />
            <input id="RemoveAll"  type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveAll']?>"/>
          </td>
          <td bgcolor="#EFFEE2">
				<select name="StudentSelected[]" id="StudentSelected" size="10" multiple="true">
					<?=$html_selected_selectedStudent?>
				</select>
				<br/>
				<span class="tabletextrequire">* </span><?=$Lang['IES']['DeleteStudentWarning']?> 
          </td>
        </tr>
      </table>
      <p class="spacer"></p>
    </td>
  </tr>
<!-- <tr>
<td>Student Class No.</td>
<td>:</td>
<td> <input type="radio" name="radio" id="radio" value="radio" />
跟名次序generate 
<input type="radio" name="radio" id="radio" value="radio" />
跟student code 次序generate</td>
</tr> -->
</table>

<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="submit" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>" onclick="$('#stepSet').val(0)" />
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="window.location='index.php?mod=admin&task=settings_scheme_index'" />
  <p class="spacer"></p>
</div>

<input type="hidden" name="stepSet" id="stepSet" />
<input type="hidden" name="schemeID" id="schemeID" value="<?=$html_schemeID?>"/>

<input type="hidden" name="mod" id="mod" value="admin">
<input type="hidden" name="task" id="task" value="settings_scheme_update">
</form>
