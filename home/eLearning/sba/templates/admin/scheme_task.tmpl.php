<?php
// Using: Pun
/*
 * Pun (2016-04-08) [ip.2.5.7.4.1] [94670]
 * - modified commentShow(), hide popup after select items
 * Ivan (2010-12-13)
 * - modified js function add_from_commentbank() to display the selected comment in the next line of the textarea
 */
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jqueryslidemenu.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-latest.js"></script>
<script type="text/javaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/sba/SBAIES.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/sba/sba.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?=$intranet_session_language?>.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="tet/javaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>


<script language="JavaScript">
var span_array=new Array();
<?=$html_js_array?>
function addComment(jBtnObj){
  var formObj = $(jBtnObj).parent().parent();
  var formAction = $(formObj).attr("action");
  
  $.ajax({
    url:      "index.php",
    type:     "POST",
    data:     $(formObj).serialize(),
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(xml){
                var comment = $(xml).find("comment").text();
                var new_code = $(xml).find("new_code").text();
                var old_code = $(xml).find("old_code").text();
                
                //*******The marking box*******
                var box1 = document.getElementsByName("mark["+old_code+"][<?=$ies_cfg["marking_criteria"]["result"][0]?>]");
                var box2 = document.getElementsByName("mark["+old_code+"][<?=$ies_cfg["marking_criteria"]["progress"][0]?>]");
                var box3 = document.getElementsByName("mark["+old_code+"][<?=$ies_cfg["marking_criteria"]["overall_result"][0]?>]");
                
                //########The Comment textarea#######
                var t_box = document.getElementsByName(old_code);
                var t_boxDivID = document.getElementById('text_'+old_code);
                //Update the code
                $(box1).attr('name', "mark["+new_code+"][22]");
                $(box2).attr('name', "mark["+new_code+"][21]");
                $(box3).attr('name', "mark["+new_code+"][1]");
//                $(t_box).val('');
                $(t_boxDivID).val('');
                
                //Display the comment in the <DIV>		
                $(jBtnObj).parent().parent().parent().find('.IES_comment_string').html(comment);
                $(jBtnObj).parent().hide();
                $(jBtnObj).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");                  
                $(t_box).attr('name', new_code);  
              }
  });
}
function addTaskScore(jBtnObj){
	
	var formObj = $(jBtnObj).parent();
	var formAction = $(formObj).attr("action");

	$.ajax({
		url: "index.php",
		type: "POST",
		data: $(formObj).serialize(),
		error: function(xhr,ajaxOptions,thrownError){
					alert(xhr.responseText);
				},
		success: function(xml){
					var comment = $(xml).find("returnStr").text();
					var new_code = $(xml).find("new_code").text();
					var old_code = $(xml).find("old_code").text();
			       var t_box = document.getElementsByName(old_code);
         
					//Display the retun string in the <DIV>		
					$(jBtnObj).parent().parent().find('.IES_score_string').html(comment);
					  $(t_box).attr('name', new_code);  
				}
	});

	

}
function taskApprove(jBtnObj)
{
  var student_id = $(jBtnObj).attr("student_id");
  var task_id = $(jBtnObj).attr("task_id");

  $.ajax({
    //url:      "task_approval_update.php",
    url: 	  "index.php",
    type:     "POST",
    data:     "mod=admin&task=task_approval_update&task_id="+task_id+"&student_id="+student_id,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
    			//$("#tempdiv").html(data);
                if(data == 1) {
                  //alert("Stage approved");
                  $(jBtnObj).replaceWith("<span class=\"IES_allow\"><?=$Lang['IES']['AllowedToNextStep']?></span>");
                }
                else {
                  alert("<?=$Lang['SBA']['FailToApproveStage']?>");
                }
              }
  });
}

function ShowTable(ID){
	if($(".column_"+ID).attr("rowspan")!=1)
	{
		$(".display_"+ID).each(function(){
			$(this).fadeOut(0);
		});
		
		$(".column_"+ID).each(function(){
			
			$(this).attr("rowspan",1);	
		})
	}
	else{
		$(".column_"+ID).each(function(){
			
			$(this).attr("rowspan",span_array[ID]);
			
		});
		$(".display_"+ID).each(function(){		
			$(this).fadeIn(0);
		
		});
	}
}

function ShowTableAll(obj){
	
	if($('#'+obj +" option:selected'").val() == "hide")
	{
		$('a[name=showtable]').each(function(){
				$(this).html('(<?=$Lang['Btn']['Show']?>)');
		});
		for(var i = 0; i < span_array.length; i++){
			
			$(".display_"+i+":visible").each(function(){
				$(this).css('display','none');
			});
			$(".column_"+i).each(function(){
			$(this).attr("rowspan",1);	
		})
		}
		
	}
	else{
		$('a[name=showtable]').each(function(){
				$(this).html('(<?=$Lang['Btn']['Hide']?>)');
		});
		for(var i = 0; i < span_array.length; i++){
			
			$(".display_"+i+":hidden").each(function(){
				$(this).css('display','');
			});
			$(".column_"+i).each(function(){
				$(this).attr("rowspan",span_array[i]);	
			})
		}
	}
}

function giveMark(jBtnObj)
{
  $(jBtnObj).hide();	
  var formObj = $(jBtnObj).parent().parent();
  var ValInput = true;
  $(jBtnObj).parent().children('.update_record').html("");
  $(formObj).find("input[type=text]").each(function(){
    var mark = $(this).val();
  
    if(mark == "")
    {
      alert("Empty");
      $(this).focus();
      ValInput = false;
      return false;
    }
    else if(!IsNumeric(mark))
    {
      alert("Not A Number");
      $(this).focus();
      ValInput = false;
      return false;
    }
  });
	//alert ($(formObj).serialize());	
  if(ValInput)
  {
    $.ajax({
      url:      "task_mark_update.php",
      type:     "POST",
      data:     $(formObj).serialize(),
      error:    function(xhr, ajaxOptions, thrownError){
                  alert(xhr.responseText);
                },
      success:  function(xml){
                
                var date = $(xml).find("date").text();
                var new_code = $(xml).find("new_code").text();
    			var old_code = $(xml).find("old_code").text();
                if(date.length > 0 && new_code.length > 0 && old_code.length > 0){
                	$(jBtnObj).show();	
                
    			
	    			//Display the marks in the <DIV>	
	                $(jBtnObj).parent().children('.update_record').html(date);
	                
	                //*******The marking box*******
	                var box1 = document.getElementsByName("mark["+old_code+"][<?=$ies_cfg["marking_criteria"]["result"][0]?>]");
	                var box2 = document.getElementsByName("mark["+old_code+"][<?=$ies_cfg["marking_criteria"]["progress"][0]?>]");
	                var box3 = document.getElementsByName("mark["+old_code+"][<?=$ies_cfg["marking_criteria"]["overall_result"][0]?>]");
	               //########The Comment textarea#######
	                var t_box = document.getElementsByName(old_code);   
	                
	                //Update the code
					$(box1).attr('name', "mark["+new_code+"][22]");
					$(box2).attr('name', "mark["+new_code+"][21]");
					$(box3).attr('name', "mark["+new_code+"][1]");		
					$(t_box).attr('name', new_code);
                }
                }
    });
  }
}

function isOverallResultField(jObj){
  return ($(jObj).attr("name").indexOf("[<?=$ies_cfg["marking_criteria"]["overall_result"][0]?>]") != -1);
}

function setOverallMark(jTextObj){
  var tableObj = $(jTextObj).parent().parent().parent();
  var totalMark = 0;
  var finalResultObj;
  
  if(isOverallResultField($(jTextObj)))
  {
    return false;
  }
  
  $(tableObj).find("input").each(function(){
    if(isOverallResultField($(this)))
    {
      finalResultObj = $(this);
    }
    else if($(this).val() == "")
    {
      return false;
    }
    else if(IsNumeric($(this).val()))
    {
      totalMark += parseInt($(this).val());
    }
  });
  
  if(IsNumeric(totalMark))
  {
    $(finalResultObj).val(totalMark);
    return true;
  }
}

// commented on 2010-12-13 by Ivan
//function add_from_commentbank(obj, id, tid){
//	var textarea = document.getElementById(tid);
//	$(textarea).val($(textarea).val()+$(obj).text());	
//}
function add_from_commentbank(obj, id, tid){
	var textarea = document.getElementById(tid);
	var textarea_value = Trim($(textarea).val());
	
	if (textarea_value != '')
		textarea_value += "\r\n";
	textarea_value += $(obj).text();
		
	$(textarea).val(textarea_value);	
}

function DeleteComment(SnapshotCommentID, snapshotID ,obj){
	var formObj = $(obj).parent().parent();
	if(confirm('<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>'))	{
		$.ajax({
		    url:      "index.php",
		    type:     "POST",
		    data: 	  "mod=ajax&task=ajax_admin&ajaxAction=DeleteComment&SnapshotCommentID="+SnapshotCommentID+"&snapshotID="+snapshotID,
		    error:    function(xhr, ajaxOptions, thrownError){
		                alert(xhr.responseText);
		              },
		    success:  function(data){
		    			$(obj).parent().parent().html(data);
		              }
	  });
  }
}


$(document).ready(function(){
  $("a[name=addComment]").click(function(){
    $(this).parent().parent().find("div[name=comment_box]").show();
    $(this).attr("class", "current");
  });
  
  $("a[name=closeComment]").click(function(){
    $(this).parent().parent().hide();
    $(this).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");
  });
  
  $("input[name=giveMark]").click(function(){
    giveMark($(this));
  });
  
  $("input[name=approve]").click(function(){
    taskApprove($(this));
  });
  
  $("input[name^=mark]").change(function(){
    setOverallMark($(this));
  });
  $("select[name=TaskID]").change(function(){
    var taskID = $(this).find("option:selected").val();
    window.location = "index.php?mod=admin&task=scheme_task&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>&TaskID="+taskID;
  });
  
  
  $(".commentbank_btn1").toggle(function() {
  	  commentShow($(this).parent().children('.IES_select_commentbank'),$(this).attr('answerEncodeNo'));
	}, function() {
	  commentHide($(this).parent().children('.IES_select_commentbank'));
	});
	
	$(".commentbank_btn2").toggle(function() {
  	  $(this).parent().children(".categoryList").slideDown();
	}, function() {
	  $(this).parent().children(".categoryList").slideUp();
	});
	
	$('a[name=showtable]').click(function(){
	  	var id = $(this).attr('val');
	  	if($(".column_"+id).attr('rowspan') > 1)
		{
			$(this).html('(<?=$Lang['Btn']['Show']?>)');
			$(".display_"+id+":visible").each(function(){
					$(this).css('display','none');
			});
			
			$(".column_"+id).each(function(){
				$(this).attr("rowspan",1);	
			})
			
		}
		else{
			
			$(this).html('(<?=$Lang['Btn']['Hide']?>)');			
			$(".display_"+id+":hidden").each(function(){
				$(this).css('display','');
			});
			$(".column_"+id).each(function(){
				$(this).attr("rowspan",span_array[id]);	
			})
			
		}
  });
});
function commentShow($ParTarget,ParAnswerEncodeNo,ParCategoryID) {
	if (ParCategoryID == undefined) {
		ParCategoryID = "";
	}
	$.ajax({
		//url:      "ajax/ajax_commentLoadingHandler.php",
		url:	  "index.php",
		type:     "POST",
		data:     "mod=ajax&task=ajax_admin&ajaxAction=GetComment&categoryID="+ParCategoryID+"&answerEncodeNo="+ParAnswerEncodeNo+"&language=<?=$scheme_detail_arr["Language"]?>",
		
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  h_comment = $(xml).find("h_comment").text();
			  $ParTarget.html(h_comment);
			  $ParTarget.show();
			}
	});
	$ParTarget.parent().children(".categoryList").slideUp();
}
function commentHide($ParTarget) {
	$ParTarget.hide();
}
</script>
<div id="tempdiv"></div>
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<br style="clear:both" />

<?=$html_tab_nav?>

<div class="content_top_tool">
  <div class="Conntent_tool"><!--a href="#" class="export"> 匯出</a--></div>
  <br style="clear:both" />			
</div>
<br style="clear:both" /> 

<div class="table_board">
  <table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td valign="bottom">      
        <div class="table_filter">
          
          <select id="DisplayController" name="DisplayController" onchange="ShowTableAll(this.id);">
            <option value="hide"><?=$Lang['IES']['ShowLatestResponse']?></option>
            <option value="show"><?=$Lang['IES']['ShowAllResponse']?></option>
          </select>
        </div>
        <br  style="clear:both"/>
      </td>
      <td valign="bottom">&nbsp;</td>
    </tr>
  </table>
  
  
  <table class="common_table_list">

    <col style="width:10%" />
    <col style="width:10%" />                     
    <col style="width:35%" />
    <col style="width:25%" />
    <col style="width:15%" />


    <!--col style="width:15%" />
    <col style="width:25%" />
    <col style="width:30%" />
    <col style="width:30%" /-->
    <thead>
      <tr>
        <th><?=$Lang['IES']['Class']?> - <?=$Lang['IES']['ClassNumber']?></th>
        <th><?=$Lang['IES']['Student2']?></th>
        <th><?=$Lang['IES']['StudentAnswer']?></th>
        <th class="sub_row_top"><?=$Lang['IES']['TeacherComment']?></th>
		<th class="sub_row_top"><?=$Lang['IES']['GiveMark'].$Lang['SBA']['MarkingOptional']?></th>
      </tr>
    </thead>
    <tbody>
  
      <?=$html_content?>
    </tbody>
  </table>
  
  <p class="spacer"></p>
  <br />
  <p class="spacer"></p>

</div>
