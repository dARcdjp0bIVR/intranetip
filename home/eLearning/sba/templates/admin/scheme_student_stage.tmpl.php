<?php
// Using: 
/*
 * Pun (2016-04-08) [ip.2.5.7.4.1] [94670]
 * - modified commentShow(), hide popup after select items
 * Ivan (2010-12-13)
 * - modified js function add_from_commentbank() to
 * 		1) Fix the cannot display comment problem
 * 		2) Display the selected comment in the next line of the textarea
 */
?>
<link href='<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css' rel='stylesheet' type='text/css'>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script language="JavaScript">
var span_array=new Array();
<?=$html_js_array?>

// commented on 2010-12-13 by Ivan
//function add_from_commentbank(obj, id, tid){
//  var textarea = document.getElementById(tid);
//  var commentTxt = $(textarea).val();
//  commentTxt = commentTxt + $(obj).find("input[id="+id+"]").val();
//
//	$(textarea).val(commentTxt);
//}
function add_from_commentbank(obj, id, tid){
	var textarea = document.getElementById(tid);
	var textarea_value = Trim($(textarea).val());
	
	if (textarea_value != '')
		textarea_value += "\r\n";
	textarea_value += $(obj).text();
		
	$(textarea).val(textarea_value);	
}

/******************** For task use only : Start ********************/
function addComment(jBtnObj){
  var formObj = $(jBtnObj).parent().parent();
  //var formAction = $(formObj).attr("action");
  
  $.ajax({
    url:      "index.php",
    //url:	  "admin/task_comment_update.php",
    type:     "POST",
    data:     $(formObj).serialize(),
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(xml){
    	
                var comment = $(xml).find("comment").text();
                var new_code = $(xml).find("new_code").text();
                var old_code = $(xml).find("old_code").text();
                
                //########The Comment textarea#######
                var t_box = document.getElementsByName(old_code);
				//$(t_box).val('');    

                var t_boxDivID = document.getElementById('text_'+old_code);
				$(t_boxDivID).val('');

                //Display the comment in the <DIV>		
                $(jBtnObj).parent().parent().parent().find('.IES_comment_string').html(comment);
                $(jBtnObj).parent().hide();
                $(jBtnObj).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");                  
                $(t_box).attr('name', new_code); 
              }
  });
}

function addTaskScore(jBtnObj){
	
	var formObj = $(jBtnObj).parent();
	var formAction = $(formObj).attr("action");

	$.ajax({
		url: "index.php",
		type: "POST",
		data: $(formObj).serialize(),
		error: function(xhr,ajaxOptions,thrownError){
					alert(xhr.responseText);
				},
		success: function(xml){
					var comment = $(xml).find("returnStr").text();
					//Display the retun string in the <DIV>		
					$(jBtnObj).parent().parent().find('.IES_score_string').html(comment);
				}
	});

	

}


function DeleteComment(SnapshotCommentID, snapshotID ,obj){
	
	if(confirm('<?=$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete?>'))	{
		$.ajax({
		    url:      "index.php",
		    type:     "POST",
		    data: 	  "mod=ajax&task=ajax_admin&ajaxAction=DeleteComment&SnapshotCommentID="+SnapshotCommentID+"&snapshotID="+snapshotID,
		    error:    function(xhr, ajaxOptions, thrownError){
		                alert(xhr.responseText);
		              },
		    success:  function(data){
      		    			//alert(data);
      		    			$(obj).parent().parent().html(data);
		              }
	  });
  }
}

function ShowTable(ID){
	if($(".column_"+ID).attr("rowspan")!=1)
	{
		$(".display_"+ID+":visible").each(function(){
			$(this).fadeOut(0);
		});
		$(".column_"+ID).each(function(){
			$(this).attr("rowspan",1);	
		})
	}
	else{
		$(".column_"+ID).each(function(){
			
			$(this).attr("rowspan",span_array[ID]);
		});
		$(".display_"+ID+":hidden").each(function(){		
			$(this).fadeIn(0);
		
		});
	}
}


function ShowTableAll(obj){
	
	if(obj.selectedIndex == 0)
	{
		
		for(var i = 0; i < span_array.length; i++){
			
			$(".display_"+i+":visible").each(function(){
				$(this).fadeOut(0);
			});
			$(".column_"+i).each(function(){
			$(this).attr("rowspan",1);	
		})
		}
		
	}
	else{
	
		for(var i = 0; i < span_array.length; i++){
			
			$(".display_"+i+":hidden").each(function(){
				$(this).fadeIn(0);
			});
			$(".column_"+i).each(function(){
				$(this).attr("rowspan",span_array[i]);	
			})
		}
	}
}

function taskApprove(jBtnObj)
{
  var student_id = $(jBtnObj).attr("student_id");
  var task_id = $(jBtnObj).attr("task_id");

  $.ajax({
    //url:      "task_approval_update.php",
    url:	  "index.php",
    type:     "POST",
    data:     "mod=admin&task=task_approval_update&task_id="+task_id+"&student_id="+student_id,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                if(data == 1) {
                  //alert("Stage approved");
                  $(jBtnObj).replaceWith("<span class=\"IES_allow\"><?=$Lang['IES']['AllowedToNextStep']?></span>");
                }
                else {
                  alert("Fail to approve stage");
                }
              }
  });
}
/******************** For task use only : End ********************/

$(document).ready(function(){
  $("a[name=addComment]").click(function(){
    $(this).parent().parent().find("div[name=comment_box]").show();
    $(this).attr("class", "current");
  });
  
  $("a[name=closeComment]").click(function(){
    $(this).parent().parent().hide();
    $(this).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");
  });
  
  $("input[name=approve]").click(function(){
    taskApprove($(this));
  });
  
  $("input[name=redo]").click(function(){
    sendRedoStage($(this));
  });

  $("select[name=StudentID]").change(function(){
    var studentID = $(this).find("option:selected").val();
    window.location = "index.php?mod=admin&task=scheme_student_stage&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>&StudentID="+studentID;
  });
  
  $(".commentbank_btn1").toggle(function() {
  	  commentShow($(this).parent().children('.IES_select_commentbank'),$(this).attr('answerEncodeNo'));
	}, function() {
	  commentHide($(this).parent().children('.IES_select_commentbank'));
	});
	
	$(".commentbank_btn2").toggle(function() {
  	  $(this).parent().children(".categoryList").slideDown();
	}, function() {
	  $(this).parent().children(".categoryList").slideUp();
	});
	
	$('a[name=showtable]').click(function(){
	  	var id = $(this).attr('val');
	  	if($(".column_"+id).attr('rowspan') > 1)
		{
			$(this).html('(<?=$Lang['Btn']['Show']?>)');
			$(".display_"+id+":visible").each(function(){
					$(this).css('display','none');
			});
			
			$(".column_"+id).each(function(){
				$(this).attr("rowspan",1);	
			})
			
		}
		else{
			
			$(this).html('(<?=$Lang['Btn']['Hide']?>)');			
			$(".display_"+id+":hidden").each(function(){
				$(this).css('display','');
			});
			$(".column_"+id).each(function(){
				$(this).attr("rowspan",span_array[id]);	
			})
			
		}
  });
});

function commentShow($ParTarget,ParAnswerEncodeNo,ParCategoryID) {
	if (ParCategoryID == undefined) {
		ParCategoryID = "";
	}
	
	$.ajax({
		url:	  "index.php",
		type:     "POST",
		data:     "mod=ajax&task=ajax_admin&ajaxAction=GetComment&categoryID="+ParCategoryID+"&answerEncodeNo="+ParAnswerEncodeNo+"&language=<?=$scheme_detail_arr["Language"]?>",
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
			  h_comment = $(xml).find("h_comment").text();
			  $ParTarget.html(h_comment);
			  $ParTarget.show();
			}
	});
	$ParTarget.parent().children(".categoryList").slideUp();
}

function commentHide($ParTarget) {
	$ParTarget.hide();
}
</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
  <br />
</div>
<!-- navigation end -->
                        
<br style="clear:both" />
                       
<?=$html_tab_nav?>



<br style="clear:both" /> 

<div class="table_board">
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td valign="bottom">      
        <div class="table_filter">

          <select id="DisplayController" name="DisplayController" onchange="ShowTableAll(DisplayController);">
            <option value="show"><?=$Lang['IES']['ShowLatestResponse']?></option>
            <option value="hide"><?=$Lang['IES']['ShowAllResponse']?></option>
          </select>
        </div>
        <?=$html_stageMarking_btn?>
        <br  style="clear:both"/>
      </td>
      <td valign="bottom">&nbsp;</td>
    </tr>
  </table>


  <table class="common_table_list">
    <!--col style="width:13%" />
    <col style="width:7%" />
    <col style="width:40%" />
    <col style="width:40%" /-->
	    <col style="width:10%" />
    <col style="width:10%" />                     
    <col style="width:35%" />
    <col style="width:25%" />
    <col style="width:15%" />

    <thead>
      <tr>
        <th><?=$Lang['IES']['Task']?></th>
        <th><?=$Lang['IES']['StepNum']?></th>
        <!--<th>老師審批日期</th>-->
        <th><?=$Lang['IES']['StudentAnswer']?></th>
        <th class="sub_row_top"><?=$Lang['IES']['TeacherComment']?></th>
		<th class="sub_row_top"><?=$Lang['IES']['GiveMark']?></th>
      </tr>
    </thead>
    <tbody>
      <?=$html_content?>
    </tbody>
  </table>
  
  <p class="spacer"></p>
  <br />
  <p class="spacer"></p>
</div>

<!--
<input type="hidden" name="mod" id="mod" value="<?=$admin?>">
<input type="hidden" name="task" id="task" value="<?=$task?>">
<input type="hidden" name="scheme_id" id="scheme_id" value="<?=$scheme_id?>">
<input type="hidden" name="stage_id" id="stage_id" value="<?=$stage_id?>">
<input type="hidden" name="ajaxAction" id="ajaxAction" value="">
-->
