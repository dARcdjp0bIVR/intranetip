<?=$html_js_script?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<br>
<table width="100%" align="center">
  <tr>
    <td width="60%"><?=$html_toolbar?></td>
    <td align="right"><?=$html_message?></td>
  </tr>
</table>

<div class="table_board">
  <table class="common_table_list" width="95%" align="center">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <th><?=$Lang['IES']['Title']?></th>
        <th><?=$Lang['IES']['StartDate']?></th>
        <th><?=$Lang['IES']['EndDate']?></th>
        <th><?=$Lang['IES']['UploadedAmount']?></th>
        <th><?=$Lang['IES']['NeedComment']?></th>
        <th>&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?=$html_worksheet_list?>
    </tbody>
  </table>
  <br />
  <p class="spacer"></p>
</div>

