<script language='javascript'>

function goCancel() {
	document.form0.action = 'index.php?mod=worksheet&task=worksheet&scheme_id=<?=$worksheetDetail['SchemeID']?>';
	document.form0.submit();
}

function goEdit() {
	document.form0.action = 'index.php?mod=worksheet&task=edit';
	document.form0.submit();
}

var ClickID = '';
var callback_show_result = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    //DisplayPosition();
	}
}

function show_result(formID,statuFeeDBackID)
{


	$.ajax({
		url:      "index.php?mod=worksheet&task=ajax_submit",
		type:     "POST",
		data:     $("#"+formID).serialize(),
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(str){
					  $("#"+statuFeeDBackID).html(str);
		}

	});



}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility = 'hidden';
}

function getPosition(obj, direction)
{
	var objStr = 'obj';
	var pos_value = 0;
	while (typeof(eval(objStr))!='undefined' && eval(objStr + '.tagName')!='BODY')
	{
		pos_value += eval(objStr + '.' + direction);
		objStr += '.offsetParent';
	}
	return pos_value;
}

function DisplayPosition() {
	document.getElementById('ref_list').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	document.getElementById('ref_list').style.top = getPosition(document.getElementById(ClickID),'offsetTop');
	document.getElementById('ref_list').style.visibility = 'visible';
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation" width="70%"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		<td align="right" width="30%"><div id="ref_list">&nbsp;</div></td>
	</tr>
</table>
<div class="table_board">
<form <?=$html_form_tag?>>
  <table class="form_table" align="center" border="0">
      <?=$html_worksheet_list?>
  </table>
</form>
  <br>
  <table class="common_table_list">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <th width="20%"><?=$i_ClassNumber?></th>
        <th width="15%"><?=$i_identity_student?></th>
        <th width="20%"><?=$Lang['IES']['StudentFile']?></th>
        <th width="35%"><?=$Lang['IEs']['TeacherComment']?></th>
        <th width="10%">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <?=$html_student_list?>
    </tbody>
  </table>
  
  <div class="edit_bottom">
  <form>
	  <p class="spacer"></p>
	  	<div align="center"><?=$html_button?></div>
	  <p class="spacer"></p>
  </div>
  </form <?=$html_form_tag?>>
</div>
