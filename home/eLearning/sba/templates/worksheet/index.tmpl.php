<form name="form1" method="POST">
<?=$html_js_script?>
<table width="100%">
  <tr>
    <td width="60%"><?=$html_toolbar?></td>
    <td align="right"><?=$html_message?></td>
  </tr>
</table>

<div class="table_board">
  <table class="common_table_list">
    <thead>
      <tr>
        <th class="num_check">#</th>
        <th><?=$Lang['IES']['Scheme']?></th>
        <th><?=$Lang['IES']['WorksheetAmount']?></th>
      </tr>
    </thead>
    <tbody>
      <?=$html_worksheet_list?>
    </tbody>
  </table>
  <br />
  <p class="spacer"></p>
</div>
</form>