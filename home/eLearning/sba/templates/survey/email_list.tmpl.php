<?php
/** [Modification Log] Modifying By: Stanley
 * *******************************************
 * *******************************************
 */
?>
<script type="text/javascript" src="/templates/2009a/js/sba/SBAIES.js"></script>
<script>

function delete_email(obj, id){
	$('#mod').val('ajax');
	$('#task').val('ajax_delete_email');

	if (confirm("<?=$Lang['IES']['DeleteWarning']?>")) {
		$.ajax({
			url:      "index.php",
			type:     "POST",
			data:     "SurveyEmailID="+id+"&"+$("form[name=form1]").serialize(),
			success:  function(xml){				 
				 	if (xml) {
				 		$(obj).parents("tr:first").remove();
				 	} else {
				 		alert("Remove Failed");
				 	}
				}
		});
	}
}

</script>
<div class="q_wrapper">
<form id="form1" name="form1" method="POST" action="">
	<div class="q_header">
    	
    	<h3>
        <span>&nbsp;<?=$html_TypeName?>&nbsp;</span><br />
        <?=$Page_title?>&nbsp;
        </h3>
        
       <!-- <?=(base64_encode("SurveyID=$SurveyID&euserid=1740"))?>-->
        <?=$html_tag?>
        
        <!--<ul class="q_tab">
            <li class=""><a href="management.php?key=<?=$originalKey?>"><?=$Lang['IES']['manage_answered_questionaire']?></a></li>
            <li class="current"><a href="#"><?=$Lang['IES']['EmailList']?></a></li>
        </ul>-->
    
    </div><!-- q_header end -->
	<div class="q_content">
    
    <!-- navigation star -->
                  <span class="navigation" style="float:left;"><?=$html_navigation?>
                  </span>
                  <?=$h_statusCount?>
                  <!-- navigation end -->
     <div class="content_top_tool">
     	<?=$html_tool?>
     </div>
    <div class="table_board">
                   <?=$html_table?>
                     <br />
                     <p class="spacer"></p>
                  </div>
    
    
    
    
    <!-- submit btn start -->
    <div class="edit_bottom"> 
    <p class="spacer"></p>

			<?=$html_close_button?>
             <p class="spacer"></p>
    </div>
    <!-- submit btn end -->
                           
	</div> <!-- q_content end -->
	<input type="hidden" name="mod" id="mod" value=""/>
	<input type="hidden" name="task" id="task" value=""/>
	<input type="hidden" name="key" value="<?=$originalKey?>" />
	</form>
</div> <!-- q_wrapper end -->