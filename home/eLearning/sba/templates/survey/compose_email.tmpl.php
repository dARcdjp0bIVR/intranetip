<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
?>
<html>
<head>
<link href="/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/lang/script.en.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/script.js"></script>
<meta http-equiv='content-type' content='text/html; charset=utf-8' />
</head>
<script type="text/javascript">

function Check_Mail_Format(){

	putRecipientToHiddenToAddress();
	putExternalToToHiddenToAddress();
	$('#mod').val('ajax');
	$('#task').val('ajax_email_validation');
	$.ajax({
      url:      "index.php",
      type:     "POST",
      data:     $('#<?=$html["form_name_id"]?>').serialize(),
      success:  function(data){
                	if(data == 1){
                		goSave();
                	}
                	else{
                		alert("<?=$Lang['IES']['EmailInvalid']?>");
                	}
                }

    });

}

function Linkfill(){
	var field = FCKeditorAPI.GetInstance('content');				
	field.InsertHtml('<?=$questionnaireStr?>');	
}
function removeInHiddenToAddress(obj) {
	var $selectedValues = $(obj).find("option:selected");
	$selectedValues.each(function() {
		$("#ToAddress_INTERNAL").text(
			$("#ToAddress_INTERNAL").text().replace($(this).val(),"")
			);
	});
}
function goSave() {
	$('#mod').val('survey');
	$('#task').val('compose_email_send');

    var $targetForm = $("form[name=<?=$html["form_name_id"]?>]");
    $targetForm.attr("action","index.php");
    $targetForm.submit();


}
function goSendEmail() {
    $("input[name=IS_SEND_EMAIL]").val(1);
    Check_Mail_Format();
}
function putRecipientToHiddenToAddress() {
	var $iMailRecipient = $("select[name=Recipient\\[\\]]");
	if ($iMailRecipient.length>0) {
		var mailType = $("#mailType").val().toUpperCase();
		$source = $("#ToAddress_INTERNAL");
		
		var strToAddress = "";
		$iMailRecipient.find("option").each(function() {
			var text = $(this).text();
			var val = $(this).val();
			if (jQuery.trim(text+val) != "") {
				strToAddress = strToAddress + '"'+text+'" \<'+val+'\>\;';
			}
		});
		$source.val(strToAddress);
	} else {
		return false;
	}
}
function putExternalToToHiddenToAddress() {
	$("#ToAddress_EXTERNAL").val($("#ExternalTo").val());
}
function RemoveEmailInSelection(Textfield)
{	
	var SelName = Textfield+"Select";	
	var EmailStr = $("#"+Textfield).val();	
	$("#"+SelName).children(":selected").each(function(){		
		EmailStr = EmailStr.replace($(this).val()+";","");		
		$(this).remove();		
	});	
	$("#"+Textfield).val(EmailStr);	
}
</script>
<style>
<!--override content25.css-->

.form_table tr td.field_title{	border-bottom:1px solid #E6E6E6 ; width:30%; }
.form_table col.field_title {width:100px;}
.form_table tr td{	vertical-align:top;	text-align:left; line-height:18px; padding-top:5px; padding-bottom:5px;}
.form_table tr td.field_title{	border-bottom:1px solid #E6E6E6 ; width:30%; }

</style>
<body>
<form name="<?=$html["form_name_id"]?>" id="<?=$html["form_name_id"]?>" action="" method="post">
   <table class="form_table" style="width:100%" >
            <col class="field_title" />
			<col class="field_c" />      
			<?=$updateMessage?>
            <tr><td><?=$Lang['IES']['Email']['To']?><!--Recipients--></td><td>:</td><td><?=$html["cant_access_mail"]?><?=$html["select_emails_internal"]?><?=$html["select_emails_external"]?><!--You can select receipients or type in multiple email addresses. Separate addresses with ";".-->
&nbsp;&nbsp;<font size= "-5"><?=$Lang['IES']['Email']['string1']?></font>
</td></tr>
            <tr><td><?=$Lang['IES']['Email']['Subject']?></td><td>:</td><td><input name="title" type="text" id="textfield" value="<?=$html["Title"]?>" style="width:90%"/></td></tr>
            <tr><td><?=$Lang['IES']['Email']['QuestionnaireLink_1']?></td><td>:</td><td><a href='javascript:Linkfill()' ><?=$Lang['IES']['Email']['QuestionnaireLink_2']?></a></td></tr>
            <tr><td><?=$Lang['IES']['Email']['Message']?></td><td>:</td><td><?=$html["Content"]?></td></tr>
           
            </table>


                <div style="margin:0 auto; text-align:center">
                <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="Check_Mail_Format()" value="<?=$Lang['IES']['Save']?>" />        
      			<?=$h_sendEmailButton?>
				<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="javascript:self.parent.tb_remove();" value="<?=$Lang['IES']['Cancel']?>" />
				<br/><br/><br/>
<br/>
				<!--Caution: After clicking Save, only the mail message will be saved. Email addresses and receipients will not be saved. If you do not want to send out your questionnaire now, please do NOT enter any mail addresses or receipients.-->
<?=$Lang['IES']['Email']['string2']?><br/><br/>

	</div>
  <input type="hidden" id="mod" name="mod" value=""/>
  <input type="hidden" id="task" name="task" value=""/>
  <input type="hidden" id="SurveyID" name="SurveyID" value="<?=$SurveyID?>" />
  <input type="hidden" id="SurveyEmailID" name="SurveyEmailID" value="<?=$SurveyEmailID?>" />
  <input type="hidden" name="task_id" value="<?=$task_id?>" />
  <input type="hidden" id="IS_SEND_EMAIL" name="IS_SEND_EMAIL" value="0" />
  <input type="hidden" id="mailType" name="mailType" value="<?=$html["mail_type"]?>"/>
  
</form>
</body>
</html>