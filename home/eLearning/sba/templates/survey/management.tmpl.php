<?php
/** [Modification Log] Modifying By: thomas
 * *******************************************
 * *******************************************
 */
?>
<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>
<script>
function DoGetTableList(){
	$('#list_table').html('<center><?=$loading_image?></center>');
	$.ajax({
//      url:      "ajax/ajax_getsurveyansweredrespondentlist.php",
      url:      "index.php",
      type:     "POST",
      data:     "&mod=ajax&task=ajax_getsurveyansweredrespondentlist&key=<?=$originalKey?>&IS_IES_STUDENT=<?=$IS_IES_STUDENT?>&Survey_type=<?=$Survey_type?>",
      error:    function(xhr, ajaxOptions, thrownError){
                  alert(xhr.responseText);
                },
      success:  function(data){
                	$('#list_table').html(data);
                }

    });
}

function DoDelete(ansID){
	if(confirm("<?=$Lang['IES']['DeleteWarning']?>")){
		$('#list_table').html('<center><?=$loading_image?></center>');
		$.ajax({
//	      url:      "ajax/ajax_getsurveyansweredrespondentlist.php",
	      url:      "index.php",
	      type:     "POST",
	      data:     "mod=ajax&task=ajax_getsurveyansweredrespondentlist&action=delete&IS_IES_STUDENT=<?=$IS_IES_STUDENT?>&key=<?=$originalKey?>&ansID="+ansID,
	      error:    function(xhr, ajaxOptions, thrownError){
	                  alert(xhr.responseText);
	                },
	      success:  function(data){
	                	$('#list_table').html(data);
	                }
	
	    });
	}
}


$(document).ready(function() {
	DoGetTableList();
});
</script>

<div class="q_wrapper">
<form name="form1" method="POST" action="index.php">
	<div class="q_header">
    	
    	<h3>
        <span>&nbsp;<?=$html_TypeName?>&nbsp;</span><br />
        <?=$Page_title?>&nbsp;
        </h3>

        <?=$html_tag?>
        
    
    
    </div><!-- q_header end -->
	<div class="q_content">
    
    			  <!-- navigation star -->
                  <div class="navigation">
						<table width="100%" style="font-size:15px">
							<tr>
								<td>
									<?=$html_navigation?>
								</td>
								<td>
									<div class="IES_top_tool" style="margin:0">
										<?=$h_exportQuestionaire?>										
									</div>
								</td>
							</tr>
						</table>
                  </div>
                  <!-- navigation end -->
                  
     <?=$html_instr?>
     <div class="content_top_tool">
     	<?=$html_tool?>
     </div>
    <div class="table_board">
                   <div id="list_table" />
                     <br />
                     <p class="spacer"></p>
                  </div>
    
    
	</div>    
    
    <!-- submit btn start -->
    <div class="edit_bottom"> 
    <p class="spacer"></p>

			<?=$html_close_button?>
             <p class="spacer"></p>
    </div>
    <!-- submit btn end -->
                           
	</div> <!-- q_content end -->
	<input type="hidden" name="mod" id="mod" value="survey" />	
	<input type="hidden" name="task" id="task" value="" />	
	<input type="hidden" name="key" value="<?=$originalKey?>" />
	</form>
</div> <!-- q_wrapper end -->
