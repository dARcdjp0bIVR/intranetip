<?php
/** [Modification Log] Modifying By: Isaac
 * *******************************************
 * 2019-04-24 Isaac
 * Added JS function onloadThickBox() for thickbox generation
 *
 * *******************************************
 */

?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>/templates/2009a/js/sba/SBAIES.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script language="JavaScript">
<!--
$(document).ready(function(){
	// Initialise the table

	if (<?=$hiddenUpdateResult?>==1) {
		$("#showSuccess").show().fadeOut(5000);
		$("input[name=updateResult]").val(0);
	}

	$(".ClassDragAndDrop").tableDnD({
		dragHandle: "Dragable",
		onDragClass: "move_selected",
		onDrop:	function(table, row){
								$.ajax({
									type: "POST",
									url: "index.php",
									data: "mod=ajax&task=ajax_groupingListHandler&Action=Sequence&key=<?=$key?>&"+$.tableDnD.serialize(),
									success: function(msg){
									}
								});
						}
	});
});



function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}
function removeMapping(obj,ParEncodedSurveyMappingID) {
	if (confirm("<?=$Lang['IES']['DeleteWarning']?>")) {
		$.ajax({
			url:      "index.php?mod=ajax&task=ajax_groupingListHandler",
			type:     "POST",
			data:     "Action=removeMapping&SurveyMappingID="+ParEncodedSurveyMappingID,
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
				 
				 	result = $(xml).find("result").text();
				 	if (Number(result)==1) {
				 		//$(obj).parents("tr:first").remove();
				 		//window.parent.opener.location.reload(true);		// reload the parent page to update the discovery in the index page
						//self.location.reload(true);		// reload the parent page to update the discovery in the index page
						$("input[name=hiddenUpdateResult]").val(1);
						$("form[name=form1]").attr("action","");
						$('input#mod').val('survey');
						$('input#task').val('questionnaireGroupingList');
						$("form[name=form1]").submit();
				 	} else {
					  	$("input[name=hiddenUpdateResult]").val(0);
					  	$("#showFailed").show().fadeOut(5000);
				 	}
				}
		});
	}
}

function reloadPage()
{
//	document.form1.action = 'questionnaireGroupingList.php';
//	document.form1.submit();
	$targetPage = $("form[name=form1]");
	$('input#mod').val('survey');
	$('input#task').val('questionnaireGroupingList');
	$targetPage.submit();

}

function onloadThickBox(mod, task, surveyMappingID, URIEdit, keepThis) {
	$.post(
			"index.php", 
			{ 
				mod: mod,
				task: task,
				SurveyMappingID: surveyMappingID,
				IsEdit: URIEdit,
				KeepThis: keepThis
			},
			function(ReturnData) {
				$('div#TB_ajaxContent').html(ReturnData);
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);
}
-->
</script>

<form name="form1" method="POST" action="">
<div class="q_header">
<h3><?=$html["survey_title"]?></h3>
<?=$html["tag"]?> 
</div><!-- q_header end -->
<div class="q_content">

    <!-- navigation star -->
		<div class="navigation">
			<table width="100%" style="font-size:15px">
				<tr>
					<td>
						<?=$html_navigation?>
					</td>
					<td>
						<div class="IES_top_tool" style="margin:0">
							<?=$html["print_and_export"]?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	<!-- navigation end -->
	<?=$html_instr?>
	<br/>
	<span id="showSuccess" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordUpdated())?></span>
	<span id="showFailed" style='display:none; float:right;'><?=str_replace("\n"," ",libies_ui::getMsgRecordFail())?></span>
	<div class="content_top_tool">
		<?=$html_tool?>
	</div>
	<br/>
	<!--a href = "">New</a--> <!-- temp-->
	<br/>
	<br/>
<div class="table_board">
<?=$html["listing_table"]?>
                       
                           
	<br>
	<p class="spacer"></p>
</div>
  <!-- submit btn start -->
  <div class="edit_bottom"> 
    <?=$html["close_button"]?>
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->

<input type="hidden" name="mod" id="mod" value=""/>
<input type="hidden" name="task" id="task" value=""/>

<input type="hidden" name="qStr" />
<input type="hidden" name="key" value="<?=$key?>" />
<input type="hidden" name="hiddenUpdateResult" />
</form>

