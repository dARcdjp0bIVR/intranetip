

<style>
.warning_red{
	border:1px red solid;
}
</style>
<script>

function ch_list(lang){
	if(lang == 'chi'){
		$('#list_td').html("<?=$html_list_chi?>");
	}
	else if(lang == 'eng'){
		$('#list_td').html("<?=$html_list_en?>");
	}
	$('#list_tr').show();
}

function ch_type(obj){
	var type = $(obj).val();
	$('#type').val(type);
	document.form2.submit();
}

function bibi_AddOneField(id,name){
	$('#'+id).append('<br /><input type="text" style="margin-top:5px;" class="input_med" name=\'info[' + id + '][' + name + '][]\'/>');
}

function bibi_AddOrgan(id){
	$('#'+id).append('<br /><input type="text" style="margin-top:5px;" class="input_med" name=\'info[' + id + '][]\'/>');
}

function bibi_IsEditedBookChi(type){

	if(type=="Yes")
	{
		document.getElementById("IsEditedBook_Yes").checked=true;
		document.getElementById("IsEditedBook_No").checked=false;
				
							
		$('#authorChiTr').hide();
		$('#authorEngTr').show();
		$('#translatorTr').show();
		$('#editorTr').show();	
		$('#bookTitleTr').show();	


//		document.getElementById("authorChiInput").setAttribute("class", "input_med");
//		document.getElementById("editorInput").setAttribute("class", "iscompulsory input_med");	
		
	}
	else if(type=="No")
	{
		document.getElementById("IsEditedBook_No").checked=true;
		document.getElementById("IsEditedBook_Yes").checked=false;
					
				
		$('#authorChiTr').show();
		$('#authorEngTr').show();
		$('#translatorTr').show();
		$('#editorTr').hide();		
		$('#bookTitleTr').show();

		
//		document.getElementById("editorInput").setAttribute("class", "input_med");
//		document.getElementById("authorChiInput").setAttribute("class", "iscompulsory input_med");	
				
	}
	
	document.getElementById("ChBookAuthorInput").setAttribute("class", "input_med iscompulsory");
	document.getElementById("chIsEditedBookChoice").setAttribute("class", "");
	
	$('html,body').animate({ scrollTop: $(document).height()}, 1500);
	
//	alert($(document).height());
}

function bibi_IsEditedBookEng(type){

	if(type=="Yes")
	{
		document.getElementById("IsEditedBook_No").checked=false;
		document.getElementById("IsEditedBook_Yes").checked=true;
		

		$('#bookAuthorTr').hide();
		$('#chapterAuthorTr').show();		
		$('#chapterTitleTr').show();	
		$('#bookEditorTr').show();	
		$('#bookTitleTr').show();
			
		
		document.getElementById("bookAuthorInput").setAttribute("class", "");
		document.getElementById("bookEditorInput").setAttribute("class", "iscompulsory");	
	}
	else if(type=="No")
	{		
		document.getElementById("IsEditedBook_Yes").checked=false;
		document.getElementById("IsEditedBook_No").checked=true;
		
		
		$('#bookAuthorTr').show();
		$('#chapterAuthorTr').hide();		
		$('#chapterTitleTr').hide();		
		$('#bookEditorTr').hide();	
		$('#bookTitleTr').show();
				
		
		document.getElementById("bookAuthorInput").setAttribute("class", "iscompulsory");
		document.getElementById("bookEditorInput").setAttribute("class", "");
		
	}
	document.getElementById("EnBooktitileInput").setAttribute("class", "iscompulsory input_med");
	document.getElementById("enIsEditedBookChoice").setAttribute("class", "");
	
	$('html,body').animate({ scrollTop: $(document).height()}, 1500);
	
	//$('.edit_task_pop_board_write').scrollTo('#IsEditedBook_Yes',800,{queue:true});
}



function bibi_AddMore(id){
	$('#'+id).append('<br />Surname <input type="text" style="margin-top:5px;" name=\'info['+id+'][surename][]\'/>&nbsp;&nbsp;Initial <input type="text" name=\'info['+id+'][initial][]\'/>');
}
function bibi_AddMore_new(id){
	$('#'+id).append('<br />Surname<input type="text" style="margin-top:5px;width:25%;" name=\'info['+id+'][surename][]\'/> Initial<input type="text" style="width:25%;" name=\'info['+id+'][initial][]\'/>(e.g. J.W.)');
}

function formcheck(){
	var valid = true;
	$(".input_num").each(function(index) {
		if($(this).val() != ""){
    		if(!isInteger($(this).val())){
    			valid = false;
    			$(this).addClass("warning_red");
    		}
    		else{
    			$(this).removeClass('warning_red');
    		}
		}
 	 });
 	$(".iscompulsory").each(function() {
 		var cont = '';
 		var fieldtype = $(this).attr('type'); 
 		if(fieldtype == 'text'){
 			 cont = $(this).val();
 		}
 		else if(fieldtype == 'textarea'){
 			cont = $(this).val();
 		}
 		if(cont == ''){
 			valid = false;
 			$(this).addClass("warning_red");
 		}
 		else{
 			$(this).removeClass('warning_red');
 		}
 	});
 	if(valid){
		//document.form2.action = "bibiupdate.php";
		document.form2['mod'].value = "content";
		document.form2['task'].value = "bibiupdate";
		document.form2.submit();
 	}
 	else{
 		alert("<?=$Lang['IES']['PleaseFillInRequiredInformation']?><?=$Lang['_symbol']["exclamationMark"]?>");
 	}
}

function isInteger(s) {
  return (s.toString().search(/^-?[0-9]+$/) == 0);
}


function js_remove_iscompulsory_innertable(id)
{
	$("#"+id).attr("class", "innertable");
}

$(document).ready(function(){

	parent.getlisttable();

	$(".lang_bt").click(function(){
		 ch_list($(this).val());
	});
	
	
	$(".iscompulsory").each(function() {
		var ts = $(this).parent("td").prev("td").prev("td").html();		

		if(ts!=null){ 
			if(ts.indexOf('*') == -1){
				$(this).parent("td").prev("td").prev("td").append("<font color='red'>*</font>");	
			}
		}
				
	});	
	
	$(".notShow").each(function() {
		$(this).hide();
				
	});

	var Type_YesNo = "<?=$contentArray['IsEditedBook']?>";
	var type = "<?=$type?>";

	if (type == 'engbook_new') {
		bibi_IsEditedBookEng(Type_YesNo);
	}
	else if (type == 'chibook_new') {
		bibi_IsEditedBookChi(Type_YesNo); 
	}
	
	var biID ='<?=$bibiId?>';
	
	if(biID!='')
	{
		js_remove_iscompulsory_innertable("displaytobibi");
	}
		

});


</script>
<body>
<form name="form2" action="index.php" method="POST">	

<div class="IES_bibi IES_lightbox">

<?=$html_title?>

<div class="edit_task_pop_board_write standard_height">
  
  <table class="form_table IES_table">
  	  <td colspan="4" style="color:red"><center><?=$html_old_data_Warning?></center></td>
      <tr>
      <td colspan="4"><center><?=$html_msg?></center></td>
      </tr>
      <tr>
        <td><?=$Lang['IES']['language_new']?></td>
        <td>:</td>
        <td>
        <?=$html_lang_table?>
        </td>
     	<td></td>
      </tr>
     <span id="content_table" /> 
     
     
      <tr id='list_tr' style="<?=$isdisplay?>">
        <td><?=ucwords($Lang['IES']['type'])?></td>
        <td>:</td>
        <td id='list_td'>
        	<?=$html_list_current?>
        </td>
      </tr>
      
      <? if ($showDisplay) :?>
      <tr style="display:none">
		<!-- 顯示 -->
				
		<td><?=$html_displaytobibi?></td>
		<td>:</td>
        <td colspan='3'>
        <table id="displaytobibi" class="innertable iscompulsory">

        <td><input id='yesID' type="radio" name="todisplay" value="<?=$html_Yes?>" onclick="js_remove_iscompulsory_innertable('displaytobibi')" <?=$isdisplaytobibi?>/><label for='yesID'><?=$html_isdisplaytobibi?></lable></td>
        <td><input id='noID' type="radio" name="todisplay" value="<?=$html_No?>" onclick="js_remove_iscompulsory_innertable('displaytobibi')" <?=$notdisplaytobibi?>/><label for='noID'><?=$html_notdisplaytobibi?></lable></td>
       	</tr>
        </table>
        </td>		
		</tr>
        <? endif; ?>
	  <tr>
     <?=$html_table?>
     
      
      <col class="field_title" />
      <col  class="field_c" />
      
    </table>
</div>
<INPUT type='hidden' id='mod' name='mod' value='content' />
<INPUT type='hidden' id='task' name='task' value='addbibi' />
<INPUT type='hidden' id='type' name='type' value='<?=$type?>' /> 
<INPUT type='hidden' id='bibiId' name='bibiId' value='<?=$bibiId?>' /> 
<INPUT type='hidden' id='content' name='content' value='<?=$content?>' /> 
<INPUT type='hidden' id='scheme_id' name='scheme_id' value='<?=$scheme_id?>' />  

<div class="IES_lightbox_bottom">
      <?=$html_submit_button?>
	  <input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="parent.tb_remove();" value=" <?=$Lang['IES']['Cancel']?> " />
</div>
</div>
</form>
