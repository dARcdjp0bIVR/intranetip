<?
//edit by : 
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script>
var originalUI = '';  //store the original HTML content for the edit the Scheme , use for reset
function displayEditUI(scheme_id){

	$("#sba_editControl").hide();
	$("#sba_newButton").hide();
	
	originalUI = $("#sba_editArea").html();
	$.ajax({
    url:      "index.php",
    type:     "POST",
    async:    false,
    data:     "mod=ajax&task=ajax_scheme&ajaxAction=displaySchemeInfoForEdit&scheme_id="+scheme_id,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){

					$("#sba_editArea").html(data);
				  /*
                $("#task_block_"+parTaskID).find("a.show_intro").hide();
                $("#task_block_"+parTaskID).find("div.task_steps").hide();
                $("#task_content_"+parTaskID).html(data);
				*/

              }
	});
}
function checkInput(form){
	var title = $("#r_title").val();

	title = $.trim(title);

	var checkedIsPassed = true;

	$(".WarningDiv").each(function(){
		$(this).hide();
	});

	if(title == ''){

		$('#schemeNameInputErrorDiv').show();
		$("#r_title").focus();

		checkedIsPassed = false;
	}

	if(checkedIsPassed){
		form.submit();
	}
}
function resetEditToViewMode(){
	$("#sba_editControl").show();
	$("#sba_editArea").html(originalUI);
	$("#sba_newButton").show();
	
}

function getSchemeStageListEditView(scheme_id)
{
	Block_Element('StageListDiv');
	$('#StageListDiv').load(
		'index.php?mod=ajax&task=ajax_scheme',
		{
			'ajaxAction':'getSchemeStageListEditView',
			'scheme_id':scheme_id
		},
		function(data){
			UnBlock_Element('StageListDiv');
		}
	);
}

function getSchemeStageListView(scheme_id)
{
	Block_Element('StageListDiv');
	$('#StageListDiv').load(
		'index.php?mod=ajax&task=ajax_scheme',
		{
			'ajaxAction':'getSchemeStageListView',
			'scheme_id':scheme_id
		},
		function(data){
			UnBlock_Element('StageListDiv');
		}
	);
}

function updateSchemeStageList(scheme_id)
{
	var StageIDArr = $('input[name=StageID[]]');
	var TitleArr = $('input[name=Title[]]');
	var DeadlineArr = $('input[name=Deadline[]]');
	var WeightArr = $('input[name=Weight[]]');
	var MaxScoreArr = $('input[name=MaxScore[]]');
	
	var StageIDVals = Get_JQuery_Element_Values(StageIDArr,false);
	var TitleVals = Get_JQuery_Element_Values(TitleArr,true);
	var DeadlineVals = Get_JQuery_Element_Values(DeadlineArr,false);
	var WeightVals = Get_JQuery_Element_Values(WeightArr,false);
	var MaxScoreVals = Get_JQuery_Element_Values(MaxScoreArr,false);
	var RecordStatusVals = [];
	var TotalMaxScore = $.trim($('input#TotalMaxScore').val());
	
	// validate fields 
	var isValid = true;
	for(var i=0;i<StageIDVals.length;i++){
		var stageID = StageIDVals[i];
		TitleVals[i] = $.trim(TitleVals[i]);
		DeadlineVals[i] = $.trim(DeadlineVals[i]);
		WeightVals[i] = $.trim(WeightVals[i]);
		MaxScoreVals[i] = $.trim(MaxScoreVals[i]);
		
		if(TitleVals[i]==''){
			$('#TitleWarningDiv'+stageID).html('<?=$Lang['SBA']['AlertMsg']['PleaseFillInAssessmentItem']?>').show();
			isValid=false;
		}else{
			$('#TitleWarningDiv'+stageID).html('').hide();
		}
		if(DeadlineVals[i]=='' || !check_date_without_return_msg(DeadlineArr[i])){
			$('#DeadlineWarningDiv'+stageID).html('<?=$Lang['SBA']['AlertMsg']['PleaseFillInDeadSubmitted']?>').show();
			isValid=false;
		}else{
			$('#DeadlineWarningDiv'+stageID).html('').hide();
		}
		
		if(WeightVals[i] != '' && (!jIS_NUMERIC(WeightVals[i]) || (parseFloat(WeightVals[i]) < 0.0))){
			$('#WeightWarningDiv'+stageID).html('<?=$Lang['SBA']['AlertMsg']['PleaseFillInWeight']?>').show();
			isValid=false;
		}else{
			$('#WeightWarningDiv'+stageID).html('').hide();
		}
		if(MaxScoreVals[i] !='' && (!jIS_NUMERIC(MaxScoreVals[i]) || (parseFloat(MaxScoreVals[i]) < 0.0))){
			$('#MaxScoreWarningDiv'+stageID).html('<?=$Lang['SBA']['AlertMsg']['PleaseFillInFullMark']?>').show();
			isValid=false;
		}else{
			$('#MaxScoreWarningDiv'+stageID).html('').hide();
		}
		
		RecordStatusVals.push( $('input#RecordStatusY'+stageID).is(':checked')?1:0 );
	}
	
	if(isValid){
		Block_Element('StageListDiv');
		$.post(
			'index.php?mod=ajax&task=ajax_scheme',
			{
				'ajaxAction':'updateSchemeStageList',
				'StageID[]':StageIDVals,
				'Title[]':TitleVals,
				'Deadline[]':DeadlineVals,
				'Weight[]':WeightVals,
				'MaxScore[]':MaxScoreVals,
				'RecordStatus[]':RecordStatusVals,
				'TotalMaxScore':TotalMaxScore,
				'thisSchemeID':scheme_id
			},
			function(msg){
				Get_Return_Message(msg);
				getSchemeStageListView(scheme_id);
			}
		);
	}
}
/*
function sumTotalMaxScore(TotalMaxScoreID,MaxScoreName)
{
	var TotalMaxScoreObj = $('#' + TotalMaxScoreID);
	var MaxScoreArr = $('input[name='+MaxScoreName+']');
	var TotalMaxScore = 0;
	
	MaxScoreArr.each(function(i){
		var that = $(this);
		TotalMaxScore += parseInt(that.val());
	});
	
	TotalMaxScoreObj.val(TotalMaxScore);
}
*/
function Get_JQuery_Element_Values(jqueryObj,needEncode)
{
	var values = [];
	for(var i=0;i<jqueryObj.length;i++){
		if(needEncode){
			values.push(encodeURIComponent($.trim(jqueryObj[i].value)));
		}else{
			values.push($.trim(jqueryObj[i].value));
		}
	}
	return values;
}
</script>

        <div class="clear vertcial_spacer"></div>
		<?if($sba_allow_edit){?>

		<div class="pretend_stu_tool" style="margin-right:40px; margin-top:-5px;">
			<div class="below"><span><?=$Lang['SBA']['ContentPreivewMode']?></span></div>
		</div>

		<?}?>
        <div class="coverpage_board">
        
	        <div class="board_top"><div></div></div>
	        
	        <div class="board_left"><div class="board_right">      

        <div class="theme_edit" id="sba_editControl">
			<div class="stage_top_tool">
			<?if($sba_allow_edit){?>
			<a href="javaScript:void(0)" OnClick="displayEditUI(<?=$scheme_id?>);" class="edit" title="<?=$Lang['IES']['Edit']?>"><span><?=$Lang['IES']['Edit']?></span></a>
			<?}?>
			</div>
        </div>
        

        <div class="board_content">

        <div class="SBA_IES_logo"></div>
        
        <div class="theme_desc" id="sba_editArea">
			<?=$h_schemeIntroduction?>
        </div>

        <div class="clear"></div>
        </div>


	        </div>
			
			</div>
	        
	        <div class="board_bottom"><div></div></div>
	        
        </div>




<?php 
//if($sba_allow_edit){
	if($displayWholeNewUI && $sba_allow_edit){
	?>
		<!-- Theme shortcut start (empty sample) -->
			<div class="coverpage_board" id="sba_newButton">
			<div class="board_top"><div></div></div>
			<div class="board_left"><div class="board_right">
			<div class="board_content">
					
			<!-- stage left btn start -->
			<div class="stage_left_tool" >
				<a href="index.php?mod=content&task=stageNew&scheme_id=<?=$scheme_id?>" class="add" title="<?=$Lang['SBA']['AddStage']?>"><span><?=$Lang['SBA']['AddStage']?></span></a>
			</div>
			<div class="clear"></div>
			<!-- stage left btn end -->
			
			</div>
			</div></div>
			<div class="board_bottom"><div></div></div>
			</div>
			<!-- Theme shortcut end -->
	<?
	}else{
	?>
	<div class="coverpage_board">
		<div class="board_top">
			<div>
			</div>
		</div>
		<div class="board_left">
			<div class="board_right">
				<div class="board_content" id="StageListDiv">
					<div class="theme_edit">
<?if($sba_allow_edit){?>
						<div class="stage_top_tool">
							<a title="<?=$Lang['IES']['Edit']?>" class="edit" href="javascript:void(0);" onclick="getSchemeStageListEditView('<?=$scheme_id?>');"><span><?=$Lang['IES']['Edit']?></span></a>
							<a title="<?=$Lang['SBA']['ReOrder']?>" class="move" href="javascript:void(0)" onclick="show_dyn_thickbox_ip('','index.php?mod=content&task=reorderStage&scheme_id=<?=$scheme_id?>', 'TB_iframe=true&modal=true', 1, 480, 780, 1)"><span><?=$Lang['SBA']['ReOrder']?></span></a>
						</div>
<?}?>
					</div>
					<br class="clear" /><br />



				<!-- with stage created start -->
				<?=$u_existingStage?>
					<br />
				<!-- with stage created end -->
				
				<?if($sba_allow_edit) { ?>
					<!-- stage left btn start -->
					<div class="stage_left_tool" id="sba_newButton">
						<a href="index.php?mod=content&task=stageNew&scheme_id=<?=$scheme_id?>" class="add" title="<?=$Lang['SBA']['AddStage']?>"><span><?=$Lang['SBA']['AddStage']?></span></a>
					</div>
					<div class="clear"></div>
					<!-- stage left btn end -->
				<? } ?>
			
				</div>
			</div>
		</div>
			
		<div class="board_bottom">
			<div>
			</div>
		</div>
	</div>
	<?
	}
//}
?>
