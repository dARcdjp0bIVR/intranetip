<script>
	var jsFileArray; // This global variable is used to store the file details returned by ajax
	var fileAdded;	 // This global variable is used to control the display of submission button
	
	$(document).ready(function () {
		InitGlobalVariables();
		loadHandinBlock("current");
	});
	
	function InitGlobalVariables() {
		jsFileArray = new Array();
		fileAdded = 0;
	}
	
	function loadHandinBlock(ParType) {
		
		InitGlobalVariables();	// Everytime swap pages need to clear the global variables, else, the data will be accumulated
		$.ajax({
			    url:      "index.php?mod=ajax&task=ajax_load_handintask_block",
			    type:     "POST",
			    data:     "loadType="+ParType+"&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>",
			    async:	  false,
			    error:    function(xhr, ajaxOptions, thrownError){
			                alert(xhr.responseText);
			              },
			    success:  function(xml){
                      		result = $(xml).find("result").text();
                      		jsResult = $(xml).find("jsFileArray").text();                      
                      		allowJsRemoveOn = $(xml).find("js_allow_remove_on").text();
                      		if (result) {
                        		$("div[id=handin_block]").html(result);
		                        //////////////////////////////////////////////////////
        		                //This part is used to handle the Array get from JS
                        		$("div[id=forJS]").html(jsResult);
                        		var jsFileArrayFromPHP;
                        		eval($("div[id=forJS]").html());
                        		if (jsFileArrayFromPHP) {
                        			jsFileArray = jsFileArrayFromPHP;
                        		}
		                        //////////////////////////////////////////////////////
                        
                        		loadFilesDisplay(allowJsRemoveOn);
                        		initializeSubmissionButton();
                      		}
                      		else {
                        		alert("init block failed");
                      		}
			    		  }
		});
	}
	
	function initializeSubmissionButton() {
		if (window.jsFileArray && jsFileArray.length) {
				fileAdded = jsFileArray.length;
		}
		if (fileAdded>0) {
			$targetButton = $("#studentSubmission");
			$targetButton.css("display",'');
			
			$targetButton.click(function() {
				$.ajax({
				    url:      "index.php?mod=ajax&task=ajax_student_submission",
				    type:     "POST",
				    data:     "scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>",
				    async:	  false,
				    error:    function(xhr, ajaxOptions, thrownError){
				                alert(xhr.responseText);
				              },
				    success:  function(xml){
				    			  result = $(xml).find("result").text();
				    			  if (result) {
				    			  	location.reload(true);
				    			  } else {
				    			  	alert("Submission Failed");
				    			  }
	
				              }
				});
			});
		}
	}

	function startUpload(){
		if(!$("input[name=submitFile]").val()){
			alert("Please select a file.");
			return false;
		}
		$("#f1_upload_process").css('display', '');
		$("div[class=submission_upload]").css('display', 'none');
		return true;
	}
	
	function stopUpload(success, file_id, file_name, file_hash_name, date_modified){
		$("#f1_upload_process").css("display", 'none');
		$("div[class=submission_upload]").css("display", '');
		
		if (success) {
			addFileDisplay(file_id, file_name, file_hash_name, date_modified, true);
		  	
			// if it change from no files to 1 file, add the submission button
			if (!fileAdded++) {
				initializeSubmissionButton();
			}
		}
		$("form[id=uploadForm]")[0].reset();
		return true;
	}
	
	function loadFilesDisplay(ParAllowJsRemove) {
		if (window.jsFileArray && jsFileArray.length) {
			$.each(jsFileArray, function(key, element) {
	//			addFileDisplay(element["WorksheetFileID"], element["FileName"], element["FileHashName"], element["DateModified"],ParAllowJsRemove);
				addFileDisplay(element["FILEID"], element["FILENAME"], element["FILEHASHNAME"], element["DATEMODIFIED"],ParAllowJsRemove);
			});
		}
	}
	
	function addFileDisplay(file_id, file_name, file_hash_name, date_modified, is_removable) {
		var removeBtn = "";
		if (Number(is_removable)) {
			removeBtn = "<a href=\"javascript: void(0);\" id=\"remove_"+file_id+"\" class=\"deselect\" title=\"<?=$Lang['IES']['Delete']?>\" onClick=\"javascript: removeFile(this);\">&nbsp;<\/a>";
		}
	    var displayFileHtml = "<div class=\"IES_file_list\"><span><a href=\"index.php?mod=tools&task=download&fhn="+file_hash_name+"\">"+file_name+"</a><br \/><span class=\"update_record\">("+date_modified+")<\/span><\/span>"+removeBtn+"<div class=\"clear\"><\/div>	<\/div>";
	    $(displayFileHtml).insertBefore($("div[id=submission_upload]"));
	}
	
	function removeFile(obj) {
		var confirmDelete = confirm("<?=$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile']?><?=$Lang['_symbol']["questionMark"]?>");
		if (confirmDelete) {
	
			remove_id = $(obj).attr("id");
			
			$target = $(obj).parent();
			$target.hide();
			
			file_id = (remove_id.split("_"))[1];
			
			
			$.ajax({
			    url:      "index.php?mod=ajax&task=ajax_remove_handin_files",
			    type:     "POST",
			    data:     "file_id="+file_id+"&scheme_id=<?=$scheme_id?>&stage_id=<?=$stage_id?>",
			    async:	  false,
			    error:    function(xhr, ajaxOptions, thrownError){
			                alert(xhr.responseText);
			              },
			    success:  function(xml){
			    			  result = $(xml).find("result").text();
			    			  if (result) {
			    			  	$target.remove();
			    			  	fileAdded--;
			    			  	if (fileAdded == 0) {
			    			  		$("#studentSubmission").hide();
			    			  	}
		//	    			  	alert("remove success");
			    			  } else {
			    			  	$target.show();
			    			  	alert("remove failed");
			    			  }
			              }
			});
		}
	}
</script>

<div id="forJS" style="display:none">
</div><!-- this block is used to catch AJAX returned js -->
<div id="handin_block">
</div> <!-- This block is used to display the content switch by ajax -->	