<?
/* Editing : fai */
?>
<script>
	
	function sba_step_form_show(){
		$('#sba_step_ansSheet').hide();
		$('#sba_add_step_btn').hide();
		$('.sba_step_new_btn').hide();
		$('#sba_step_form').show();
	}
	
	function sba_step_form_submit(){
		var formObj = document.sba_step_form;
		
		if(!check_text(formObj.r_step_Title, '請輸入標題')){
			return false;
		}
		else if(!check_text(formObj.r_step_Description, '請輸入描述')){
			return false;
		}
		else if(typeof(formObj.r_step_QuestionType)=='undefined'){
			alert('請選擇答題工具');
			return false;
		}
		
		formObj.submit();
	}
	
	function sba_step_form_cancel(){
		<?php if($step_id){ ?>
			/* Reset the Form */
			$('#sba_step_form').each(function(){
				this.reset();
			});
		
			$('#sba_step_ansSheet').show();
			$('#sba_add_step_btn').show();
			$('.sba_step_new_btn').show();
			$('#sba_step_form').hide();
		<?php } else { ?>
			go_tasks();
		<?php } ?>
	}
	
	function get_new_step_tool(){
		var QuestionType = $("#r_step_TmpQuestionType").val();
		var taskId = $("#task_id").val();		
		if(QuestionType){
			$.ajax({
					url		: "index.php?mod=ajax&task=ajax_step",
					type	: "POST",
					data	: 'ajaxAction=GetNewStepHandin&r_taskId='+taskId+'&r_step_QuestionType=' + QuestionType,
					async	: false,
					error	: function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
						   	  },
					success : function(data){
								$('#sba_step_tool').show();
								$('#sba_new_step_tool').hide();
								
								$('#sba_step_tool_title').html($("#r_step_TmpQuestionType option:selected").html());
								$('#sba_step_tool_content').html(data);
								
								/* Reset the Question Type */
								$('#r_step_TmpQuestionType').val('');
							  }
			});
		}
		else{
			alert("請選擇答題工具的類型");
		}
	}
	
	function delete_step_tool(){
		$('#sba_step_tool').hide();
		$('#sba_new_step_tool').show();
		
		$('#sba_step_tool_title').html('');
		$('#sba_step_tool_content').html('');
	}
	
	
	
	/******************\
	* Go to Tasks Page *
	\******************/
	
	function go_tasks(){
		window.location = "index.php?mod=content&task=task&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>";
	}
	
	
	
	/*****************\
	* Go to Step Page *
	\*****************/
	
	function go_step(task_id, step_id){
		window.location = "index.php?mod=content&task=step&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id=" + task_id + "&step_id=" + step_id;
	}
	
	function go_create_step(task_id){
		window.location = "index.php?mod=content&task=step&scheme_id=<?=$sba_libScheme->getSchemeID()?>&stage_id=<?=$sba_libStage->getStageID()?>&task_id=" + task_id;
	}
</script>

<div class="task_title task_open"><span><?=$h_task_title?></span></div>

<?php if($sba_allow_edit){ ?>
	<div class="stage_top_tool">
		<a title="重新排序" class="move" href="javascript:void(0)"><span>重新步驟排序</span></a>
	</div>
<?php } ?>

<?=$h_steps_list?>
<div class="clear"></div>
<div class="step_writing">
		
	<?php if($step_id){ ?>
			
		<form id="sba_step_ansSheet">
			
		<?php if($sba_allow_edit){ ?>
			<div class="stage_top_tool">
				<a title="編輯" class="edit" href="javascript:void(0)" onclick="sba_step_form_show()"><span>編輯</span></a>
			</div>
		<?php } ?>
		
			<div class="task_writing_top">步驟 <?=$h_step_sequence?>: <?=$h_step_title?></div>
			<div class="writing_area">
				<div class="task_content">
					<?=$h_step_description_display?>
					<?=$h_step_handin?>   
				</div>
			</div>
		</form>
			
	<?php } ?>
			
	<?php if($sba_allow_edit){ ?>
			
		<form id="sba_step_form" name="sba_step_form" action="index.php?mod=content&task=stepUpdate" method="post" <?=$step_id? "style='display:none'":""?>>
				
			<div class="task_writing_top">標題 : <input type="text" style="width:50%" class="textbox" id="StepTitle" name="r_step_Title" value="<?=$h_step_title?>"></div>
			<div class="writing_area">
				<div class="task_content">
					<table class="form_table">
						<colgroup>
							<col width="70">
							<col class="field_c">
							<col>
							<col width="20">
						</colgroup>
						<tr>
							<td>描述</td>
							<td>:</td>
							<td>
								<?=$h_step_description_textarea?>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr id="sba_new_step_tool" <?=$h_step_handin_edit? "style=\"display:none\"":""?>>
							<td>答題工具</td>
							<td>:</td>
							<td>
								<select id="r_step_TmpQuestionType" name="r_step_TmpQuestionType" onchange="get_new_step_tool()">
									<option value="">-- 請選擇 --</option>
									<?=$h_step_question_type_option?>
								</select>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr id="sba_step_tool" <?=$h_step_handin_edit? "":"style=\"display:none\""?>>
							<td id="sba_step_tool_title"><?=$h_step_handin_edit_caption?></td>
							<td>:</td>
							<td id="sba_step_tool_content"><?=$h_step_handin_edit?></td>
							<td><div class="table_row_tool"><a title="刪除" class="delete" href="javascript:void(0)" onclick="delete_step_tool()"></a></div></td>
						</tr>
					</table>
					<div class="clear"></div>                          
				</div>
			</div>       
			<div class="task_writing_bottom">
				<input type="button" value="儲存" onclick="sba_step_form_submit()" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" class="formbutton" name="submit2">
				<input type="button" value="刪除" onclick="void(0)" onmouseout="this.className='formsubbutton'" onmouseover="this.className='formsubbuttonon'" class="formsubbutton" name="submit2">
				<input type="button" value="取消" onclick="sba_step_form_cancel()" onmouseout="this.className='formsubbutton'" onmouseover="this.className='formsubbuttonon'" class="formsubbutton" name="submit2">
			</div>
			<input type="hidden" id="scheme_id" name="scheme_id" value="<?=$scheme_id?>"/>
			<input type="hidden" id="stage_id" name="stage_id" value="<?=$stage_id?>"/>
			<input type="hidden" id="task_id" name="task_id" value="<?=$task_id?>"/>
			<input type="hidden" id="step_id" name="step_id" value="<?=$step_id?>"/>
			<input type="hidden" id="r_step_Sequence" name="r_step_Sequence" value="<?=$h_step_sequence?>"/>
			<input type="hidden" id="r_step_Action" name="r_step_Action" value="StepEditUpdate"/>
		</form>
			
	<?php } ?>
	
</div>

<?php if($sba_allow_edit){ ?>
	<div id="sba_add_step_btn" class="stage_left_tool" <?=$step_id? "":"style='display:none'"?>>
		<a title="新增步驟" class="add" href="javascript:go_create_step(<?=$task_id?>)"><span>新增步驟</span></a>
	</div>
	<br>
	<br class="clear">
<?php } ?>

<div class="edit_bottom_main">
	<br class="clear">
	<p class="spacer"></p>
   	<?=$h_edit_bottom_btn?>                
	<p class="spacer"></p>
</div>