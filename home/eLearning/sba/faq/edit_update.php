<?php
//modifying By : 


$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-MGMT-FAQ-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
	
if(sizeof($_POST)==0)
	header("Location: index.php?mod=".$mod);
	
$ldb = new libdb();	


$question = intranet_htmlspecialchars($question);
$answer = ($answer!="") ? "'".intranet_htmlspecialchars($answer)."'" : "NULL";

switch($replyTo) {
	case 2 : 	$assignType = $ies_cfg["faq_assignType_to_all"];
				break;	
	case 3 : 	$assignType = $ies_cfg["faq_assignType_to_more_scheme"];
				break;	
	default : 	$assignType = $ies_cfg["faq_assignType_to_more_scheme"];
				break;	
}
if(!isset($showOption) || $showOption!=1)
	$assignType = $ies_cfg["faq_assignType_to_more_scheme"];

if($assignType!=$ies_cfg["faq_assignType_to_student"])
	$assignToStudent = "NULL";

$answerDate = ($answer!="") ? "NOW()" : "NULL";

$schemeID = $r_schemeID;

# clear question-scheme relationship
$sql = "DELETE FROM IES_FAQ_RELATIONSHIP WHERE QuestionID=$id";
$ldb->db_db_query($sql);

$sql = "UPDATE IES_FAQ SET FromSchemeID='$schemeID', Question='$question', Answer=$answer, AssignType='$assignType', AssignToStudent=$assignToStudent, AnswerDate=$answerDate, DateModified=NOW(), ModifyBy=$UserID WHERE QuestionID=$id";
$ldb->db_db_query($sql);

if(!isset($showOption) || $showOption!=1) {
	unset($schemeID2);
	$schemeID2[] = $schemeID;
	$schemeID2 = array_unique($schemeID2);	
} else if($assignType==$ies_cfg["faq_assignType_to_more_scheme"] && !in_array($schemeID, $schemeID2)) {
	$schemeID2[] = $schemeID;
	$schemeID2 = array_unique($schemeID2);	
}


if($answer!="NULL" && $assignType==$ies_cfg["faq_assignType_to_more_scheme"] && sizeof($schemeID2)>0) {	# assign to scheme(s)
	for($j=0; $j<sizeof($schemeID2); $j++) {
		$schID = $schemeID2[$j];
		$sql = "INSERT INTO IES_FAQ_RELATIONSHIP (QuestionID, SchemeID) VALUES ('$id','$schID')" ;
		$ldb->db_db_query($sql);
	}
}
#################################### FILE UPLOAD
$qid = $id;	// $qid should be prepared
include_once("uploadFAQFiles.php");	// this file is script to upload shared between new_update.php and edit_update.php
################################################
intranet_closedb();

header("Location: index.php?mod=$mod&msg=update&sch_id=$s_id&questionType=$questionType&recordstatus=$recordstatus&searchText=".intranet_htmlspecialchars($searchText));
?>