<?php
//modifying By : 

$objIES = new libies();
$ldb = new libdb();

if(!$objIES->CHECK_ACCESS("IES-MGMT-FAQ-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "FAQ";



$result = $objIES->getFAQData($id);

$schemeDetail = $objIES->GET_SCHEME_DETAIL($result['FromSchemeID']);
if($schemeDetail['Title']=="") $schemeDetail['Title'] = "---";

# Scheme Menu
$schemeMenu = $objIES->getSchemeSelection($result['FromSchemeID'],'',$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']);

$sql = "SELECT SchemeID, Title FROM IES_SCHEME WHERE SchemeType = '".$sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO']."' ORDER BY Title";
$schemeData = $ldb->returnArray($sql,2);
$relatedSchemeAry = array();

$sql = "SELECT SchemeID FROM IES_FAQ_RELATIONSHIP WHERE QuestionID=".$result['QuestionID'];
$relatedSchemeAry2 = $ldb->returnVector($sql);
$schemeMenu2 .= '
	<div class="narrow_width" style="margin-top:10px">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
	<td width="50%" bgcolor="#EEEEEE">
	
	<div class="Conntent_search"><br>
	<input type="text" name="searchScheme" id="searchScheme" value="'.$Lang['IES']['SearchScheme'].'" onKeyUp="show_search(\'admin\',\'showSpanLayer\')" onFocus="this.select()"/><span id="showSpanLayer"></span>
	</div>
	
	</td>
	<td width="40"></td>
	<td width="50%" bgcolor="#EFFEE2" class="steptitletext">'.$Lang['IES']['SelectedScheme'].'</td>
	</tr>
	<tr>
	<td bgcolor="#EEEEEE" align="center">
	<div id="searchSpan">
	<select name="AvailableSchemeID[]" size="10" style="width:99%" multiple>';
	
for($i=0; $i<sizeof($schemeData); $i++) {
	if(!in_array($schemeData[$i][0], $relatedSchemeAry2))
		$schemeMenu2 .= '<option value="'.$schemeData[$i][0].'">'.$schemeData[$i][1].'</option>';
}	
$schemeMenu2 .= '
	</select></td>
	<td>
	'.$linterface->GET_BTN(" > ", "button", "checkOptionTransfer(this.form.elements['AvailableSchemeID[]'],this.form.elements['schemeID2[]']);checkOptionAll(this.form.elements['schemeID2[]'])", "submit2").'
	<br /><br />
	'.$linterface->GET_BTN(" < ", "button", "checkOptionTransfer(this.form.elements['schemeID2[]'],this.form.elements['AvailableSchemeID[]']);checkOptionAll(this.form.elements['schemeID2[]'])", "submit22").'
	</td>
	<td bgcolor="#EFFEE2">
	<select name="schemeID2[]" id="schemeID2[]" size="10" style="width:99%" multiple>';
$sql = "SELECT sch.SchemeID, sch.Title FROM IES_FAQ_RELATIONSHIP rel LEFT OUTER JOIN IES_SCHEME sch ON (sch.SchemeID=rel.SchemeID) WHERE QuestionID=".$result['QuestionID'];
$relatedSchemeAry = $ldb->returnArray($sql);
for($i=0; $i<sizeof($relatedSchemeAry); $i++) {
		$schemeMenu2 .= '<option value="'.$relatedSchemeAry[$i][0].'" selected>'.$relatedSchemeAry[$i][1].'</option>';
}
		
$schemeMenu2 .='
	</select>
	</div>
	</td>
	</tr>
	</table>
	<span class="tabletextremark">('.$Lang['IES']['HoldCtrlButton'].') </span>
	<p class="spacer"></p>
	</div>
';


$name_field = getNameFieldByLang();
$sql = "SELECT $name_field as name, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID=".$result['AskedBy'];
$userData = $ldb->returnArray($sql,3);

$classNameNum = ($userData[0]['ClassName']!="") ? (($userData[0]['ClassName']!="") ? "-".$userData[0]['ClassNumber'] : "") : "---";


# assign to
$displayMore = ($result['Answer']!="" && ($result['AssignType']==$ies_cfg["faq_assignType_to_all"] || ($result['AssignType']==$ies_cfg["faq_assignType_to_more_scheme"] && ($result['FromSchemeID']=="" || !in_array($result['FromSchemeID'], $relatedSchemeAry2) || sizeof($relatedSchemeAry2)>1)))) ? 1 : 0;

$assignToOption .= '<input type="checkbox" name="showOption" id="showOption" value="1" '.(($displayMore)?" checked" : "").' onClick="if(this.checked==true){showSpan(\'spanAssignOption\');} else {hideSpan(\'spanAssignOption\');}"><label for="showOption">'.$Lang['IES']['MoreScheme'].'</label><br><div id="spanAssignOption" style="display:'.(($displayMore)?"inline":"none").'"><table width="80%"><tr><td><input type="radio" name="replyTo" id="replyTo2" value="2" onClick="hideSpan(\'spanScheme\')"'.(($result['AssignType']==$ies_cfg["faq_assignType_to_all"])?" checked":"").'><label for="replyTo2">'.$Lang['IES']['AssignToAllScheme'].'</label>&nbsp;';
$assignToOption .= '<input type="radio" name="replyTo" id="replyTo3" value="3" onClick="showSpan(\'spanScheme\')"'.(($result['Answer']!="" && $result['AssignType']==$ies_cfg["faq_assignType_to_more_scheme"])?" checked":"").'><label for="replyTo3">'.$Lang['IES']['AssignToSomeScheme'].'</label></td></tr>';
$assignToOption .= '<tr><td><div id="spanScheme" style="position:relative;display:'.(($result['Answer']!="" && $result['AssignType']==$ies_cfg["faq_assignType_to_more_scheme"])?"inline":"none").';left:0px">'.$schemeMenu2.'</div></td></tr></table></div>';


##################################### - FAQ
$numberOfAttachments = 0;
$maxNumberOfAttachments = 5;
$FAQAttachmentDetails = $objIES->getFAQAttachmentDetails($result['QuestionID']);

if (is_array($FAQAttachmentDetails)) {
	foreach($FAQAttachmentDetails as $key => $element) {
		$attInputBox .= "<tr><td><a href='?mod=tools&task=download&f_type=faq&fileHashName={$element["FILEHASHNAME"]}'>{$element["FILENAME"]}</a></td><td class='table_row_tool'><a title='Delete' class='delete_dim' href='#' onClick='removeFile(this,\"{$element["FILEHASHNAME"]}\")' /></td></tr>";
		$numberOfAttachments++;
	}
}


$attInputBoxTable .= '<table id="uploadFileTable">'.$attInputBox.'</table>';


//for($i=$numberOfAttachments;$i<$maxNumberOfAttachments;$i++) {
//	$js_appendBox .= "addFileUploadField('uploadFileTable');";
//}
###########################################


# table content
$html_scheme_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['CreatedBy']."</td><td width='1'> : </td><td width='80%'>".$userData[0]['name']."</td>";
//$html_scheme_list .= "<tr><td valign='top' width='20%'>".$i_ClassNumber."</td><td width='1'> : </td><td width='80%'>".$classNameNum."</td>";
$html_scheme_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Scheme']."</td><td width='1'> : </td><td width='80%'>{$schemeMenu}</td>";
$html_scheme_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Question']."</td><td width='1'> : </td><td width='80%'><input type='text' name='question' id='question' size='50' value='".$result['Question']."'></td>";
$html_scheme_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Answer']."</td><td width='1'> : </td><td width='80%'>".$linterface->GET_TEXTAREA("answer", $result['Answer'])."</td>";
$html_scheme_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Attachment']."</td><td width='1'> : </td><td width='80%'>".$attInputBoxTable."<br/>".$linterface->GET_ACTION_BTN($Lang['IES']['Add'], "button", "addFileUploadField('uploadFileTable')")."</td>";
$html_scheme_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['AssignTo']."</td><td width='1'> : </td><td width='80%'>$assignToOption</td>";

$html_button .= $linterface->GET_ACTION_BTN($button_submit, "button", "goSubmit()")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_reset, "button", "goReset()")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "goCancel()");
$html_button .= "<input type='hidden' name='id' id='id' value='$id'>";
$html_button .= "<input type='hidden' name='s_id' id='s_id' value='{$s_id}'>";
$html_button .= "<input type='hidden' name='questionType' id='questionType' value='{$questionType}'>";
$html_button .= "<input type='hidden' name='recordstatus' id='recordstatus' value='{$recordstatus}'>";
$html_button .= "<input type='hidden' name='searchText' id='searchText' value='".intranet_htmlspecialchars($searchText)."'>";
$html_button .= "<input type='hidden' name='ClickID' id='ClickID' value=''>";

$html_js_script .= "
<script language='javascript'>
function checkForm() {
	var obj = document.form1;	
	checkOptionAll(document.getElementById('schemeID2[]'));
	
	if(obj.schemeID.value=='' && document.getElementById('showOption').checked==false) {
		alert(\"".$i_alert_pleaseselect.' '.$Lang['IES']['Scheme'].' '.$i_general_or.' '.$Lang['IES']['AssignTo']."\");	
		obj.schemeID.focus();
		return false;
	}
	if(obj.question.value=='') {
		alert(\"".$i_alert_pleasefillin.' '.$Lang['IES']['Question']."\");	
		obj.question.focus();
		return false;
	}
	/*
	if(obj.answer.value.length!=0 && document.getElementById('replyTo2').checked==false && document.getElementById('replyTo3').checked==false) {
		alert(\"".$i_alert_pleaseselect.' '.$Lang['IES']['AssignTo']."\");	
		document.getElementById('replyTo2').focus();
		return false;
	}
	*/
	if(obj.answer.value.length==0 && document.getElementById('showOption').checked==true) {		
		alert(\"".$i_alert_pleasefillin.' '.$Lang['IES']['Answer']."\");	
		document.getElementById('answer').focus();
		return false;
	}
	if(document.getElementById('showOption').checked==true) {
		if(document.getElementById('replyTo2').checked==false && document.getElementById('replyTo3').checked==false) {
			alert(\"".$i_alert_pleaseselect.' '.$Lang['IES']['AssignTo']."\");	
			document.getElementById('replyTo2').focus();
			return false;
		}
		if(document.getElementById('replyTo3').checked==true) {
			var sch_length = obj.elements['schemeID2[]'].length;
			var total = 0;
			for(i=0; i<sch_length; i++) {
				if(obj.elements['schemeID2[]'].options[i].selected==true)
					total++;
			}
			if(total==0) {
				alert(\"".$i_alert_pleaseselect.' '.$Lang['IES']['AssignTo']."\");	
				obj.elements['schemeID2[]'].focus();
				return false;
			}
		}
	}
	return true;
}

function showSpan(spanName) {
	document.getElementById(spanName).style.display = 'inline';
}

function hideSpan(spanName) {
	document.getElementById(spanName).style.display = 'none';
}

function goSubmit() {
	if(checkForm()){
		document.form1.task.value = 'edit_update';
		document.form1.submit();
	}	
}

function goReset() {
	document.getElementById('spanScheme').style.display = ".(($result['Answer']!="" && $result['AssignType']==$ies_cfg["faq_assignType_to_more_scheme"])?"\"inline\"":"\"none\"").";
	document.form1.reset();	
}

function goCancel() {
	document.form1.task.value = 'index';
	document.form1.submit();
}

var ClickID = '';
var callback_show_search = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('searchSpan').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_search(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = '../../../ajax/ajax_admin_display_search.php';
    var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_show_search);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = 'obj';
	//alert(typeof(eval(objStr)));
	var pos_value = 0;
	while (typeof(eval(objStr))!='undefined' && (eval(objStr + '.tagName')!='BODY' && eval(objStr + '.tagName')!='HTML'))
	{
		pos_value += eval(objStr + '.' + direction);
		objStr += '.offsetParent';
	}
	return pos_value;
}

function DisplayPosition(){ 
	document.getElementById('searchSpan').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') + 10;
	  document.getElementById('searchSpan').style.top = getPosition(document.getElementById(ClickID),'offsetTop');
	  document.getElementById('searchSpan').style.visibility = 'visible';
}
function removeFile(obj,ParFileHashName) {
	if (confirm('{$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile']}')) {
		\$.ajax({
			url:      'ajax/ajax_FAQFileHandler.php',
			type:     'POST',
			data:     'Action=RemoveFile&fileHashName='+ParFileHashName,
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
				  if (Number(xml) == 1) {
				  	\$(obj).parents('tr:first').remove();
				  } else {
				  	alert('{$Lang['IES']['UploadFailed']}');
				  }
				}
		});
	}
}
</script>
";

$html_form_tag = " name='form1' method='POST' action='index.php' enctype='multipart/form-data' onSubmit=''";

### Title ###
$title = $Lang['IES']['FAQ'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $sba_libSba->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/faq/new.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
