<?php
//modifying By : 
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-MGMT-FAQ-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
	
if(sizeof($_POST)==0)
	header("Location: index.php?mod=".$mod);
	
$ldb = new libdb();	
	

$question = intranet_htmlspecialchars($question);
$answer = ($answer!="") ? "'".intranet_htmlspecialchars($answer)."'" : "NULL";

switch($replyTo) {
	case 2 : 	$assignType = $ies_cfg["faq_assignType_to_all"];
				break;	
	case 3 : 	$assignType = $ies_cfg["faq_assignType_to_more_scheme"];
				break;	
	default : 	$assignType = $ies_cfg["faq_assignType_to_more_scheme"];
				break;	
}

if($assignType!=$ies_cfg["faq_assignType_to_student"])
	$assignToStudent = "NULL";
	
$schemeID = $r_schemeID;
if($schemeID=='') $schemeID = "NULL";
$answerDate = ($answer!="") ? "NOW()" : "NULL";

$sql = "INSERT INTO IES_FAQ (Question, Answer, FromSchemeID, AskedBy, UserType, AssignType, AssignToStudent, RecordStatus, AnswerDate, DateInput, InputBy, DateModified, ModifyBy)
		VALUES ('$question',$answer, '$schemeID', $UserID, '".$ies_cfg["faq_userType_teacher"]."', '$assignType', $assignToStudent, '".$ies_cfg["faq_recordStatus_approved"]."', $answerDate, NOW(), $UserID, NOW(), $UserID)";
$ldb->db_db_query($sql);
$qid = $ldb->db_insert_id();

if(!isset($schemeID2))
	$schemeID2 = array();

if(!in_array($schemeID, $schemeID2)) {
	$schemeID2[] = $schemeID;	
}

if($answer!="NULL" && $assignType==$ies_cfg["faq_assignType_to_more_scheme"]) {	# assign to scheme(s)
	for($j=0; $j<sizeof($schemeID2); $j++) {
		$schID = $schemeID2[$j];
		$sql = "INSERT INTO IES_FAQ_RELATIONSHIP (QuestionID, SchemeID) VALUES ('$qid','$schID')" ;
		$ldb->db_db_query($sql);
	}
}
#################################### FILE UPLOAD
$qid = $qid;	// $qid should be prepared
include_once("uploadFAQFiles.php");	// this file is script to upload shared between new_update.php and edit_update.php
################################################
intranet_closedb();

header("Location: index.php?mod=$mod&msg=add&sch_id=$s_id&questionType=$questionType&recordstatus=$recordstatus&searchText=".intranet_htmlspecialchars($searchText));
?>