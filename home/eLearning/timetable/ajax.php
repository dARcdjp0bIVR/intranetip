<?php
/**
 * Change Log:
 * 2020-03-30 Pun
 *  - File created
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libeclass40.php"); 
include_once("../../../includes/json.php");
$cfg_eclass_lib_path = "{$eclass40_filepath}/src/includes/php";
include_once("$eclass40_filepath/src/includes/php/lib-note.php");
include_once("$eclass40_filepath/system/settings/config.inc.php");
intranet_auth();
intranet_opendb();

//// Check access right START ////
$lu = new libuser($UserID);
if (!$lu->isStudent() && !$lu->isParent() && !$lu->isTeacherStaff()) {
    echo 'Access denied';
    exit;
}

if ($lu->isParent()) {
    $studentIdArr = $lu->getChildren();
    if (isset($_GET['studentId']) && !in_array($_GET['studentId'], $studentIdArr)) {
        echo 'Wrong children';
        exit;
    }
    $studentId = (isset($_GET['studentId'])) ? IntegerSafe($_GET['studentId']) : $studentIdArr[0];
}else{
    $studentId = $UserID;
}

if ($action !== 'login' && !intranet_validateDate($_GET['date'])) {
    echo 'Wrong param';
    exit;
}
//// Check access right END ////

if($action === 'login'){
	$user_course_id = IntegerSafe($_GET['user_course_id']);
	$id = IntegerSafe($_GET['id']);

	$rStr = "fid={$id}";
	switch($_GET['direct']){
		case 'econtent':
			$rStr .= "&t={$cfg['group_function_type']['econtent']}";
			break;
		case 'assignment':
			$rStr .= "&t={$cfg['group_function_type']['assessment']}";
			break;
		default:
			echo 'Wrong param';
			exit;
	}
	$rVar = encrypt_string($rStr);

	$url = '/home/eLearning/login.php?jumpback=cc';
	$url .= "&uc_id={$user_course_id}";
	$url .= "&r_var={$rVar}";
	header("Location: {$url}");
	exit;
}

//// Init START ////
header("Content-Type:text/html;charset=UTF-8");
$json = new JSON_obj();
$moduleArr = array('econtent', 'assessment');
$modulePath = dirname(__FILE__) . '/module/';

if ($plugin['power_lesson_2']) {
    include_once('../../../includes/PowerLesson/PowerLesson.php');
    $moduleArr[] = 'pl2';
}

$date = $_GET['date'];
$db = new libdb();
$ln = new note();
//// Init END ////

//// Get class START ////
$sql = "SELECT 
	uc.user_course_id, 
	c.course_name,
	c.course_id,
	uc.user_id,
	uc.memberType
FROM 
	{$eclass_db}.user_course uc 
INNER JOIN
	{$eclass_db}.course c 
ON
	uc.course_id = c.course_id
AND
	c.RoomType=0
WHERE 
	uc.intranet_user_id = '{$studentId}'
";
$rs = $db->returnResultSet($sql);
$courseArr = BuildMultiKeyAssoc($rs, 'course_id');
//// Get class END ////

//// Get data START ////
$data = array();
foreach ($moduleArr as $module) {
    $moduleFilePath = $modulePath . $module . '/data.php';
    if (!file_exists($moduleFilePath)) {
        continue;
    }

    $_data = include($modulePath . $module . '/data.php');
    if (!$_data || !is_array($_data)) {
        continue;
    }
    $data = array_merge($data, $_data);
}
//// Get data END ////

//// Sort data START ////
function sortData($a, $b){
    if($a['start_time'] != $b['start_time']){
        return strcmp($a['start_time'], $b['start_time']);
    }elseif($a['type'] != $b['type']){
        return strcmp($a['type'], $b['type']);
    }elseif($a['title'] != $b['title']){
        return strcmp($a['title'], $b['title']);
    }
}
usort($data, 'sortData');
//// Sort data END ////

//// Pack data START ////
$addedItem = array();
$result = array();
foreach ($data as $d) {
    $uniqId = "{$d['type']}_{$d['id']}";
    if(in_array($uniqId, $addedItem)){
//        continue;
    }

    $result[$d['course_id']][] = array(
        'title' => $d['title'],
        'titleExtra' => $d['titleExtra'],
        'id' => $d['id'],
        'courseId' => $d['course_id'],
        'userCourseId' => $courseArr[$d['course_id']]['user_course_id'],
        'memberType' => $courseArr[$d['course_id']]['memberType'],
        'type' => $d['type'],
        'startDateTime' => ($d['start_time']) ? $d['start_time'] : '',
        'endDate' => ($d['end_date']) ? $d['end_date'] : '',
    );
    $addedItem[] = $uniqId;
}

$courseInfoArr = array();
foreach($courseArr as $course){
	if(isset($result[$course['course_id']])){
		$courseInfoArr[$course['course_id']] = array(
			'title' => $course['course_name'],
		);
	}
}
//$result = array_values($result);
//foreach($result as &$r){
//    $r = array_values($r);
//}
//// Pack data END ////

//debug_r($result);


echo $json->encode(array(
    'date' => $date,
    'data' => $result,
    'courseInfo' => $courseInfoArr,
));
