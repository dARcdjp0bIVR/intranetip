<?php
/**
 * Change Log:
 * 2020-03-30 Pun
 *  - File created
 */
intranet_auth();
intranet_opendb();

$_results = array();
foreach ($courseArr as $courseInfo) {
    $lo            = new libgroups(classNamingDB($courseInfo['course_id']));
    $exceptionList = $courseInfo['memberType'] === 'S'? $lo->returnFunctionIDs($cfg['group_function_type']['assessment'], $courseInfo['user_id']) : '0'; // Get homework with group not include user
    $subSql        = $courseInfo['memberType'] === 'S'? "
        LEFT JOIN 
            handin AS b 
        ON 
            b.assignment_id = a.assessment_id AND 
            b.user_id = '{$courseInfo['user_id']}' AND 
            (a.fromlesson = '' OR a.fromlesson IS NULL)" : "";

	//// Get homework START ////
	$sql = "SELECT 
        a.assessment_id AS id,
        'assessment' AS type,
		a.phasetype,
		IF(
		    a.phasetype='{$cfg['assessment_phasetype']["multiphase"]}', 
		    CONCAT(a.title, ' (', p.title, ')'),
		    a.title
	    ) AS title,
        '{$courseInfo['course_id']}' as course_id,
        p.startdate as start_time,
        p.enddate as end_date
    FROM 
        assessment as a
    INNER JOIN
        `phase` as p
    ON 
        a.assessment_id = p.assessment_id
    {$subSql}
    WHERE 
        DATE_FORMAT(p.startdate, '%Y-%m-%d') = '{$date}'
    AND 
        a.assessment_id NOT IN ({$exceptionList})
	AND 
		a.status <> '{$cfg['assessment_status']["draft"]}'
    ORDER BY 
        a.title";
	$rs = $lo->returnArray($sql);

	$_results = array_merge($_results, $rs);
	//// Get homework END ////
}

return $_results;
