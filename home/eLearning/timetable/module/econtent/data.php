<?php
/**
 * Change Log:
 * 2020-03-30 Pun
 *  - File created
 */
intranet_auth();
intranet_opendb();


$_results = array();
foreach ($courseArr as $courseInfo) {
    $lo            = new libgroups(classNamingDB($courseInfo['course_id']));
    $exceptionList = $courseInfo['memberType'] === 'S'? $lo->returnFunctionIDs($cfg['group_function_type']['econtent'], $courseInfo['user_id']) : '0'; // Get homework with group not include user

	$sql = "SELECT 
        notes_id AS id, 
        'econtent' AS type,
        title,
        readflag, 
        '{$courseInfo['course_id']}' as course_id,
        starttime as start_time,
        endtime as end_date
    FROM 
        notes 
    WHERE
        notes_id NOT IN ({$exceptionList})
    AND
        DATE_FORMAT(starttime, '%Y-%m-%d') = '{$date}' 
    ORDER BY 
        a_no, b_no, c_no, d_no, e_no
    ";
	$rs = $lo->returnArray($sql);

	foreach($rs as $r) {
//        if(!$ln->Check_Has_read_note($r['readflag'], $r['id'])){
//            continue;
//        }
		$_results[] = $r;
	}
}

return $_results;
