<?php
/**
 * Change Log:
 * 2020-03-26 Pun
 *  - File created
 */
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libuser.php");
include_once("../../../../../includes/libeclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.utf8.php");
include_once('../../../../../includes/PowerLesson/PowerLesson.php');

intranet_auth();
intranet_opendb();

$lu = new libuser($UserID);
if ((!$lu->isStudent() && !$lu->isParent()) || (!$plugin['power_lesson_2'])) {
    echo 'Access denied';
    exit;
}


$powerlesson = new PowerLesson();
$status = $powerlesson->run(
    'getLessonProgressState',
    array(
        'courseId' => IntegerSafe($_GET['courseId']),
        'lessonId' => IntegerSafe($_GET['lessonId']),
    )
);

if($_GET['debug']==1){
    debug_r($status);
    exit;
}

echo $status['progressState']; // "INITIAL" or "IN_PROGRESS" or "FINISHED"
