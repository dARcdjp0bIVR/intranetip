<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libxml.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

//header("Content-type: text/html; charset=utf-8");

$bid = $_REQUEST['bid'];

$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
$ebook_http_path = "/file/eBook/".$bid."/";
$container_xml_path = $intranet_root."/file/eBook/".$bid."/META-INF/container.xml";

if(!file_exists($container_xml_path)){
	echo $container_xml_path." does not exist.<br>";
	intranet_closedb();
	exit;
}

$xml_attr_subfix = '_attr';

$container_xml = file_get_contents($container_xml_path);

$libxml = new LibXML();

// container
$container_xml_array = $libxml->XML_ToArray($container_xml);

//debug_pr($container_xml_array);

$js = 'var g_item = {};'."\n";
$js .= 'var g_spine = [];'."\n";
$js .= 'var g_doc = [];'."\n";
$pages = '';
$buttons = '&nbsp;[<a href="javascript:jsChangeView(2);">Two Column View</a>] | [<a href="javascript:jsChangeView(1);">Scroll View</a>]';
if(!empty($container_xml_array['container']['rootfiles']['rootfile'.$xml_attr_subfix]['full-path']))
{
	$tmp_slash_pos = strrpos($container_xml_array['container']['rootfiles']['rootfile'.$xml_attr_subfix]['full-path'],'/');
	$tmp_subfolder = trim(substr($container_xml_array['container']['rootfiles']['rootfile'.$xml_attr_subfix]['full-path'],0,$tmp_slash_pos));
	$subfolder = $tmp_subfolder==""? "": $tmp_subfolder."/";
	
	$ebook_document_path = $ebook_root_path.$subfolder;
	$ebook_document_http_path = $ebook_http_path.$subfolder;
	$js .= 'var g_doc_root = "'.$ebook_document_path.'";'."\n";
	$js .= 'var g_http_root = "'.$ebook_document_http_path.'";'."\n";
	// OPF
	$content_opf_path = $ebook_root_path.$container_xml_array['container']['rootfiles']['rootfile'.$xml_attr_subfix]['full-path'];
	if(file_exists($content_opf_path))
	{
		$content_opf_xml = file_get_contents($content_opf_path);
		$content_opf_xml_array = $libxml->XML_ToArray($content_opf_xml);
		
		//debug_pr($content_opf_xml_array);
		
		$toc_path = '';
		$items = $content_opf_xml_array['package']['manifest']['item'];
		$items_size = count($items)/2;
		for($i=0;$i<$items_size;$i++){
			if($items[$i.$xml_attr_subfix]['media-type']=='application/x-dtbncx+xml'){
				$toc_path = $items[$i.$xml_attr_subfix]['href'];
				break;
			}
		}
		for($i=0;$i<$items_size;$i++){
			if(isset($items[$i.$xml_attr_subfix]) && sizeof($items[$i.$xml_attr_subfix])>0){
				$js .= "g_item['".$items[$i.$xml_attr_subfix]['id']."'] = {'id':'".$items[$i.$xml_attr_subfix]['id']."','href':'".$items[$i.$xml_attr_subfix]['href']."','media-type':'".$items[$i.$xml_attr_subfix]['media-type']."'};\n";
				$js .= "g_doc.push('".$items[$i.$xml_attr_subfix]['href']."');\n";
			}
		}
		$spine_itemref = $content_opf_xml_array['package']['spine']['itemref'];
		$itemref_size = count($spine_itemref)/2;
		$delimiter = '';
		for($i=0;$i<$itemref_size;$i++){
			if(isset($spine_itemref[$i.$xml_attr_subfix]) && sizeof($spine_itemref[$i.$xml_attr_subfix])>0){
				$js .= "g_spine.push({'idref':'".$spine_itemref[$i.$xml_attr_subfix]['idref']."'});\n";
			}
			
			$pages .= $delimiter.'<a href="javascript:jsLoadDocument('.$i.');" title="Go to page '.$i.'">'.$i.'</a>';
			$delimiter = ' | ';
		}
		
		// TOC - table of content
		if($toc_path != '')
		{
			$toc_full_path = $ebook_document_path.$toc_path;
			if(file_exists($toc_full_path)){
				 $toc_xml = file_get_contents($toc_full_path);
				 $toc_xml_array = $libxml->XML_ToArray($toc_xml);
				 //debug_pr($toc_xml_array);
			}
			
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
	<meta charset="UTF-8" content="text/html" http-equiv="Content-Type">
	<script src="/templates/jquery/jquery-1.3.2.min.js" language="JavaScript" ></script>
	<style type=text/css>
		.multicolumnElement {
		    -webkit-column-count: 2;
		    -moz-column-count: 2;
		    column-count: 2;
		    -webkit-column-width: 15em;
		    -moz-column-width: 15em;
		    column-width: 15em;
		}
	</style>
</header>
<body>
<script language="JavaScript">
<?=$js?>

function jsLoadDocument(id)
{
	var docpath = g_doc_root;
	var found = false;
	if(typeof(g_spine[id]) == 'undefined'){
		return;
	}else if(typeof(g_item[g_spine[id].idref])!='undefined'){
		docpath += g_item[g_spine[id].idref].href;
		found = true;
	}
	
	if(found){
		$('div#Content').load(
			'ajax_task.php',
			{
				'task':'loaddoc',
				'httproot':encodeURIComponent(g_http_root),
				'docpath':encodeURIComponent(docpath),
				'relpath[]':g_doc  
			},
			function(data){
				
			}
		);
	}
}

function jsChangeView(mode)
{
	if(mode==2){
		$('div#Content').addClass('multicolumnElement');
	}else{
		$('div#Content').removeClass('multicolumnElement');
	}
}

$(document).ready(function(){
	jsLoadDocument(0);
});
</script>
<div id="Content" class="multicolumnElement"></div>
<div id="PageNavigator"><?=$pages.$buttons?></div>
</body>
</html>
<?
intranet_closedb();
?>