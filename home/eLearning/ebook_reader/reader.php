<?php
// editing by Paul

/**
 * ***************************************** Changes log **********************************************
 * 2020-09-16 (Paul)
 * - add new sub-category to work as "Young Reader"
 * 2020-07-27 (Paul)
 * - fix the problem of horizontally long books wrongly positioned in some resolution
 * 2020-02-13 (Pun)
 * - Fixed cannot limit book page in trial-s
 * 2019-04-12 (Pun) [159605]
 * - Fixed cannot view full book
 * 2018-11-29 (Pun) [ip.2.5.10.1.1]
 * - added trial-for-{ Classic Stories, Original Series, Young Readers, English Classics, 古詩, 兒童生活故事, 小說, 心靈勵志, 成長自助, 教育, 通識, 漫畫繪本, 科普讀物 }
 * 2017-08-03 (Paul)
 * - fix the problem of wrong fancybox image display position in two page mode
 * 2017-03-03 (Paul)
 * - hide "download" button for audio player used
 * 2016-07-29 (Ronald)
 * - add new mode "Young Rreader" as var $youngreaderMode
 * - hide buttons for $youngreaderMode
 * - modify js function page_to_chapter for getting correct chapter name
 * 2016-07-08 (Jason)
 * - add more books to limit the page in the view of trial site
 * 2015-10-12 (Paul):
 * - modified bookdata.toc_handler() to prevent showing book content behind the table of content
 * - add adjustment for the book zooming factor to make the book well fit the screen on loading
 *
 * 2015-09-30 (Paul):
 * - change the initializing zoom factor consideration by comparing the cover page size and the content size, pick the larger one
 *
 * 2015-09-22 (Paul):
 * - add check for one page mode on page counter in PHP
 * - edit glossary_attach() to fix the problem of wrong glossary positions for crossing row vocabularies.
 * - edit bookdata.toc_handler to fix the problem of wrong page redirection by index button on Slide bar
 * - can go back to information page after going back to table of content page by index button
 * - fix the problem of text overlap on "Adventures of Huckleberry Finn" by changing word-spacing in one page mode to 3.5px
 *
 * 2015-09-06 (Jason):
 * - edit jsUpdatePageNumber() to fix the problem of not showing next page arrow
 * - edit jsLoadDocument() to fix the problem of counting wrong shift when going to the next or previous page by 1 px per page
 *
 * 2013-10-03 (CharlesMa): eBook-update-101003 - show tab when first time enter index page
 *
 * 2013-09-17 (CharlesMa): eBook-update-130917 - story book : hidden search function
 *
 * 2013-06-05 (CharlesMa): eBook-update-130605a - story book : allow user back to cover and index
 *
 * 2013-05-29 (CharlesMa): eBook-update-130529a - book cover shadow
 *
 * 2013-03-25 (CharlesMa): eBook-update-130325a - modification of "text to speech" button presentation;
 * eBook-update-130325b - modification of book cover
 * eBook-update-130325c - swipe page
 * eBook-update-130325d - improvement in highlighting button
 *
 * 2013-03-22 (CharlesMa): page_content_bg, block paper color change if in "force to one page mode"
 *
 * 2012-11-15 (CharlesMa): improve highlight box -- tool_hightlight_bg;
 * Change Recommended reading email format
 * 2012-09-12 (CharlesMa): add check if force to one page mode -- jsLoadDocument [2012-11-15 removed]
 * ****************************************************************************************************
 */
$PATH_WRT_ROOT = "../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libebookreader.php");
include_once ($PATH_WRT_ROOT . "includes/libebookreader_ui.php");
include_once ($PATH_WRT_ROOT . "lang/elib_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libelibrary.php");
include_once ($PATH_WRT_ROOT . "includes/libelibrary_install.php");

intranet_auth();
intranet_opendb();

if (isset($userBrowser)) {
    $platform = $userBrowser->platform;
    $browsertype = $userBrowser->browsertype;
    $version = explode('.', $userBrowser->version);
    if (current($version) == "" && $browsertype == 'MSIE') {
        $ver_temp_arr = explode("Trident/", $userBrowser->useragent);
        $ver_temp_arr = explode(";", $ver_temp_arr[1]);
        $version = (int) $ver_temp_arr[0];
    }
}

if (! isset($_SESSION['eBookReaderPageMode'])) {
    $_SESSION['eBookReaderPageMode'] = 2;
}

if (! isset($_SESSION['eBookReaderPaperStyle'])) {
    $_SESSION['eBookReaderPaperStyle'] = 'white';
}

// /////////////// 2013-09-18 Charles Ma
$bid = $_REQUEST['bid'];
$action = $_REQUEST['action'] != '' ? true : false;

if ($_REQUEST['action'] == "mynote") {
    $mynote_p1 = $_REQUEST['mynote_p1'];
    $mynote_p2 = $_REQUEST['mynote_p2'];
}

// /////////////// 2013-09-18 Charles Ma

$LibeLib = new elibrary();
$objInstall = new elibrary_install();

$hasAccess = $objInstall->check_book_is_readable($bid, $_SESSION['UserID']);

if (! $hasAccess) {
    echo '<table width="80%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
  <tr><td height="100px">&nbsp;</td></tr>
  <tr><td align="center" valign="middle">' . $eLib['SystemMsg']['AccessDenied'] . '</td></tr>
  <tr><td height="100px">&nbsp;</td></tr>
  </tbody>
  </table>';
    intranet_closedb();
    die();
}

// logging
$ParArr["BookID"] = $bid;
$ParArr["UserID"] = $_SESSION["UserID"];
$LibeLib->ADD_READING_HISTORY_RECORD($ParArr);
$LibeLib->ADD_BOOK_HIT_RATE($ParArr);

$ebook_root_path = $intranet_root . "/file/eBook/" . $bid . "/";
$request_url = $_SERVER['REQUEST_URI'];
if ($request_url[strlen($request_url) - 1] != '/') {
    $request_url .= '/';
}

$libebookreader_ui = new libebookreader_ui();
$libebookreader = $libebookreader_ui->Get_libebookreader();
$book_info = $libebookreader->Get_Book_List($bid);

$book_sub_folder = current(explode("/", $book_info[0]['BookSubFolder']));
// $libebookreader->GET_NEW_TABLE_OF_CONTENT($bid, $book_sub_folder);
// $table_of_content = $libebookreader->table_of_content;
// debug_r($table_of_content);

// 2013-10-17 Charles Ma
$book_elibinfo = $libebookreader->Get_eLib_Book_Info($bid);
$Publisher = $book_elibinfo[0]['Publisher'];
$SubCategory = $book_elibinfo[0]['SubCategory'];
$IsFirstPageBlank = $book_elibinfo[0]['IsFirstPageBlank'];
$IsRightToLeft = $book_elibinfo[0]['IsRightToLeft'];
// 2013-10-17 Charles Ma END

$isImageBook = trim($book_info[0]['ImageBook']) == '1';

$InputFormat = $book_info[0]['InputFormat'];
$BookSubFolder = $book_info[0]['BookSubFolder'];

$RTL_bid_arr = array(
    "1198",
    "1612",
    "1613",
    "1625",
    "1660",
    "1661",
    "1664",
    "1782",
    "1742",
    "1743",
    "1792",
    "1793",
    "1736",
    "1794",
    "1795",
    "1796",
    "1831",
    "1832",
    "1836",
    "1740",
    "1741",
    "1739",
    "1837",
    "1838",
    "1864",
    "1865",
    "1866",
    "1902",
    "1903",
    "1867",
    "1868",
    "1869",
    "1870",
    "1871",
    "1872",
    "1873",
    "1886",
    "1888",
    "1889",
    "1890",
    "1891",
    "1892",
    "1893",
    "1894",
    "1895",
    "1896",
    "1897",
    "1898",
    "1899",
    "1900"
);

$isRTL = in_array($bid, $RTL_bid_arr) || $IsRightToLeft == 1 ? 1 : 0; // 20131204-ebook-righttoleft

$imageWidth = trim($book_info[0]['ImageWidth']);
$book_name = rawurlencode($book_info[0]['BookTitle']);

/**
 * ***************************************** Changes log **********************************************
 * 2012-09-12 (CharlesMa): add check if force to one page mode -- jsLoadDocument
 * ****************************************************************************************************
 */
$ForceToOnePageMode = trim($book_info[0]['ForceToOnePageMode']) == '1';
$OnePageModeWidth = trim($book_info[0]['OnePageModeWidth']);
$OnePageModeHeight = trim($book_info[0]['OnePageModeHeight']);

if ($browsertype == 'MSIE' && ! $isImageBook) {
    // $_SESSION['eBookReaderPageMode'] = 1;
} else if ($browsertype == 'MSIE' && $isImageBook) {
    $_SESSION['eBookReaderPageMode'] = 2;
}

if ($ForceToOnePageMode) { // 2013-3-14 Charles Ma
    $_SESSION['eBookReaderPageMode'] = 1;
    $_SESSION['eBookReaderPaperStyle'] = 'white'; // 2013-3-22 Charles Ma
} else {
    if (isset($_SESSION['eBookReaderPreviousBookID']) && $_SESSION['eBookReaderPreviousBookID'] != $bid) {
        $_SESSION['eBookReaderPageMode'] = 2;
    }
}

$_SESSION['eBookReaderPreviousBookID'] = $bid;

$pageModeField = 'Pages' . $_SESSION['eBookReaderPageMode'] . 'PageMode'; // 2013-5-30 Charles Ma
/**
 * ***************************************************************************************************
 */

// ######### below is for trial site use only #############################################
/*
 * Old code * /
 * $istrials_bookIDArr = array(879, 960, 963, 965, 994, 998, 1174, 1202, 1614, 1624,
 * 1657, 1953, 1968, 1972, 1978, 1982, 1985, 1989, 1994, 2007,
 * 2012, 2023, 2046, 2047, 2143, 2149, 2156, 2189, 2201, 2615,
 * 2636, 2661);
 * // 2015-05-05 trial-for-tw-ebook & 2016-07-08 trial-for-other chinese books and readers & 2018-07-19 trail-Young Readers Series
 * // those books are limited to 20 pages on trial site
 * $istrials = $demo_http == "http://trial-s.eclasscloud.hk" && ($bid >=1582 && $bid <=1599 || in_array($bid, $istrials_bookIDArr));
 *
 *
 * // 2015-06-08 trial-for-english-classic & 2016-07-08 add new set
 * $istrials_engClassic = $demo_http == "http://trial-s.eclasscloud.hk" && ($bid >=1600 && $bid <=1602 || $bid >=1931 && $bid <=1933 || $bid >=2351 && $bid <=2353);
 *
 * // 2017-05-17 trial-for-young-readers
 * $istrials_youngReaders = $demo_http == "http://trial-s.eclasscloud.hk" && ($bid>=2488 && $bid <=2490);
 * /*
 */

$isTrialSite = ($demo_http == "http://trial-s.eclasscloud.hk" || $demo_http == "https://trial-s.eclasscloud.hk");
$enablePageLimit = false;
$pageLimit = 20;

if ($bid >= 1582 && $bid <= 1599 || in_array($bid, array(
        879,
        960,
        963,
        965,
        994,
        998,
        1174,
        1202,
        1614,
        1624,
        1657,
        1953,
        1968,
        1972,
        1978,
        1982,
        1985,
        1989,
        1994,
        2007,
        2012,
        2023,
        2046,
        2047,
        2143,
        2149,
        2156,
        2189,
        2201,
        2615,
        2636,
        2661
    ))) {
    // trial-for-tw-ebook, trial-for-other chinese books, trail-Young Readers Series
    $istrials = $isTrialSite;
    $enablePageLimit = $isTrialSite;
    $pageLimit = $_SESSION['eBookReaderPageMode'] == 1 ? 20 : 10;
} elseif (($bid >= 2488 && $bid <= 2490) || ($bid >= 2491 && $bid <= 2536 && $bid != 2519 && $bid != 2530) || ($bid >= 2581 && $bid <= 2582) || ($bid >= 2607 && $bid <= 2609) || ($bid >= 2617 && $bid <= 2619) || ($bid >= 2674 && $bid <= 2679) || (in_array($bid, array(
        2487,
        2539,
        2541,
        2579,
        2586,
        2595,
        2627,
        2630,
        2634,
        2638,
        2641,
        2645,
        2653,
        2659,
        2669,
        2671
    )))) {
    // trial-for-young-readers
    // trial-for-young-readers-2 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = $_SESSION['eBookReaderPageMode'] == 1 ? 20 : 10;
} elseif (($bid >= 1600 && $bid <= 1602) || ($bid >= 1931 && $bid <= 1933) || ($bid >= 2351 && $bid <= 2353) || ($bid >= 1934 && $bid <= 1936) || ($bid >= 2338 && $bid <= 2341) || ($bid >= 2467 && $bid <= 2471 && $bid != 2469) || (in_array($bid, array(
        2384,
        2420,
        2454
    )))) {
    // trial-for-english-classic
    // trial-for-english-classics (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = $_SESSION['eBookReaderPageMode'] == 1 ? 50 : 25;
} elseif (($bid >= 1665 && $bid <= 1674) || ($bid >= 1678 && $bid <= 1679) || ($bid >= 1682 && $bid <= 1684) || ($bid >= 1698 && $bid <= 1706 && $bid != 1699)) {
    // trial-for-classic-stories (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 10;
} elseif ((in_array($bid, array(
    965,
    879
)))) {
    // trial-for-adventure (2020-02-13 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 10;
} elseif ((in_array($bid, array(
    994
)))) {
    // trial-for-thriller (2020-02-13 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 10;
} elseif (($bid >= 1675 && $bid <= 1677) || ($bid >= 1680 && $bid <= 1681) || ($bid >= 1685 && $bid <= 1694 && $bid != 1689) || ($bid >= 1707 && $bid <= 1718 && $bid != 1708) || ($bid >= 1729 && $bid <= 1731) || ($bid >= 1924 && $bid <= 1930) || ($bid >= 2336 && $bid <= 2337) || ($bid >= 2342 && $bid <= 2348) || ($bid >= 2472 && $bid <= 2481) || (in_array($bid, array(
        2519,
        2530,
        2576
    ))) || ($bid >= 2682 && $bid <= 2688) || ($bid >= 2772 && $bid <= 2774) || ($bid >= 2777 && $bid <= 2783)) {
    // trial-for-original-series (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 10;
} elseif (($bid == 2349) || ($bid >= 2463 && $bid <= 2466) || ($bid == 2469) || ($bid >= 2482 && $bid <= 2485)) {
    // trial-for-古詩 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif (($bid >= 2680 && $bid <= 2681) || ($bid >= 2689 && $bid <= 2692) || (in_array($bid, array(
        2590,
        2625,
        2648
    )))) {
    // trial-for-兒童生活故事 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 10;
} elseif ((in_array($bid, array(
    2747,
    2748,
    2750,
    2752,
    2756,
    2758,
    2759,
    2760,
    2766
)))) {
    // trial-for-小說 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif ((in_array($bid, array(
    2749,
    2753,
    2754,
    2755,
    2757
)))) {
    // trial-for-心靈勵志 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif ((in_array($bid, array(
    2751,
    2767
)))) {
    // trial-for-成長自助 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif ((in_array($bid, array(
    2761,
    2762,
    2763,
    2764
)))) {
    // trial-for-教育 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif ((in_array($bid, array(
    2737,
    2738,
    2743,
    2765,
    2769
)))) {
    // trial-for-通識 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif ((in_array($bid, array(
    2744,
    2768
)))) {
    // trial-for-漫畫繪本 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
} elseif ((in_array($bid, array(
    2725,
    2726,
    2727
)))) {
    // trial-for-科普讀物 (2018-11-29 Pun)
    $enablePageLimit = $isTrialSite;
    $pageLimit = 20;
}
/* */
// #########################################################################################

$pages = $libebookreader->Get_Book_Page_Info($bid);
$bookmarks = $libebookreader->Get_Book_User_Bookmarks($bid, $_SESSION['UserID'], $_SESSION['eBookReaderPageMode']);
$user_progress_info = $libebookreader->Get_User_Progress($bid, $_SESSION['UserID']);

$cover_image_path = trim($libebookreader->CoverImageName) != '' ? $request_url . $libebookreader->CoverImageName : '';

$cupOnePageMode = ($Publisher == "Cambridge University Press") && $_SESSION['eBookReaderPageMode'] == 1;
$cupecOnePageMode = ($Publisher == "Cambridge University Press" || $Publisher == "English Classics" || $SubCategory == "English Classics") && $_SESSION['eBookReaderPageMode'] == 1;
$engclassicOnePageMode = ($Publisher == "English Classics" || $SubCategory == "English Classics") && $_SESSION['eBookReaderPageMode'] == 1;

$engclassicMode = $Publisher == "English Classics" || $SubCategory == "English Classics";
$cupecMode = ($Publisher == "Cambridge University Press" || $Publisher == "English Classics" || $SubCategory == "English Classics");
$cupMode = $Publisher == "Cambridge University Press";
$youngreaderMode = $Publisher == "Young Readers" || $SubCategory == "Young Readers" || $SubCategory == "和爺爺一起過的那些傳統節日";

if ($youngreaderMode) {
    // $engclassicMode = true;
    // $cupecMode = true;
}

$newPageMode = $_SESSION['eBookReaderPageMode'];
$newPageModeWidth = 650;

$page_index = $libebookreader->Get_Page_Index($bid, $_SESSION['eBookReaderPageMode']);

$js = 'var bookdata = {';
$js .= "'id' : '" . $bid . "',\n";

$js .= '\'pages\' : [],' . "\n";
$js .= '\'currentPage\' : 0,' . "\n";
$js .= '\'pageMode\' : ' . $newPageMode . ',' . "\n"; // //edited 2012-09-12(CharlesMa)
$js .= '\'paperStyle\' : \'' . $_SESSION['eBookReaderPaperStyle'] . '\',' . "\n";
$js .= '\'pageModeWidth\' : [1,' . $newPageModeWidth . ',980],' . "\n";
$js .= '\'coverImageName\' : \'' . $cover_image_path . '\',' . "\n";
$js .= '\'highlights\' : [],' . "\n";
$js .= '\'bookmarks\' : [],' . "\n";
$js .= '\'requestURL\' : \'' . $request_url . '\',' . "\n";
$js .= '\'isImageBook\' : ' . ($isImageBook ? 'true' : 'false') . ",\n";
$js .= '\'imageWidth\' : ' . ($imageWidth == '' ? "''" : $imageWidth) . ",\n";
$js .= '\'inputFormat\' : ' . ($InputFormat == '' ? "''" : $InputFormat) . ",\n";
$js .= '\'forceToOnePageMode\' : ' . ($ForceToOnePageMode ? 'true' : 'false') . ",\n"; // //edited 2012-09-12(CharlesMa)
$js .= '\'onePageModeWidth\' : ' . ($OnePageModeWidth == '' ? "''" : $OnePageModeWidth) . "\n"; // //edited 2012-09-12(CharlesMa)

$js .= '};' . "\n";

$audio_files_path = $ebook_root_path . "OEBPS/Audio/";
$audio_files = glob($audio_files_path . "*"); // 20140422-cupbook

$audio_pages = array(); // 20140526-cupbook
foreach ($audio_files as $key => $audio_file) {
    preg_match('/p[0-9]+.mp3/i', $audio_file, $result);
    if ($result) {
        $result = preg_replace('/p|.mp3/i', "", $result);
        array_push($audio_pages, current($result));
    }
}

$temp_arr = array();

for ($i = 0; $i < count($pages); $i ++) {
    $refID = $pages[$i]['RefID'];
    array_push($temp_arr, $refID);
}
$temp_arr = array_unique($temp_arr);
$toc_arr = array();
$toc2_arr = array();
if ($engclassicMode) {
    $book_path = $intranet_root . $book_info[0]['BookPath'];
    $parse_success = $libebookreader->Parse_EPUB_XML($book_path);
    $table_of_content = $libebookreader->table_of_content;
    foreach ($table_of_content["navPoint"] as $key => $value) {
        array_push($toc_arr, $value["navLabel"]);
        array_push($toc2_arr, $value["content"]);
    }
}

if (sizeof($audio_files) == 0) {
    $audio_files_path = $ebook_root_path . $BookSubFolder . "mp3/";
    $audio_files = glob($audio_files_path . "*"); // 20140811-tw
    if (sizeof($audio_files) != 0) {
        $istw = true;
    }
} else {
    $isnormalaudio = true;
}

// pad pages[0] with dummy data
$js .= "bookdata.pages.push({'idref':'', 'href':'', 'session':''});\n";
if ($browsertype == 'MSIE' && $version < 6) {
    for ($i = 0; $i < count($pages); $i ++) {
        $refID = $pages[$i]['RefID'];
        $fileName = $pages[$i]['FileName'];
        $js .= "bookdata.pages.push({'idref':'" . $refID . "', 'href':'" . $fileName . "', 'session':'1'});\n";
    }
    if ($IsFirstPageBlank) {
        $i = count($pages) - 1;
        $refID = $pages[$i]['RefID'];
        $fileName = $pages[$i]['FileName'];
        $js .= "bookdata.pages.push({'idref':'" . $refID . "', 'href':'" . $fileName . "', 'session':'1'});\n";
    }
} else {
    $total_page_count = 0;
    // $pageLimit = ($_SESSION['eBookReaderPageMode']==1)? $pageLimit : $pageLimit / 2;
    // debug_r($_SESSION['eBookReaderPageMode']);
    // debug_r($pageLimit);
    // $trial_engClassic_limit = $_SESSION['eBookReaderPageMode'] == 1 ? 49: 24; // for english classic book on trial site
    // $trial_book_limit = $_SESSION['eBookReaderPageMode'] == 1 ? 19: 9; // for general trial book on trial site
    for ($i = 0; $i < count($pages); $i ++) {
        if (($istrials && $i > 19)) {
            continue;
        }
        $refID = $pages[$i]['RefID'];
        $fileName = $pages[$i]['FileName'];

        if ($cupOnePageMode) {
            $pageCount = $pages[$i][$pageModeField] > 1 ? $pages[$i][$pageModeField] - 1 : $pages[$i][$pageModeField];
        } else if ($engclassicMode) {
            if ($_SESSION['eBookReaderPageMode'] == 1) {
                // $pageCount = $pages[$i]['Pages2PageMode']*2 >= 20? $pages[$i]['Pages2PageMode']*2 - 2 : $pages[$i]['Pages2PageMode']*2; //2015-09-21 20 pages engclassic 2 pages excess
                $pageCount = $pages[$i]['Pages2PageMode'] * 2;
            } else {
                // $pageCount = $pages[$i][$pageModeField] >= 10 ? $pages[$i][$pageModeField] -1 : $pages[$i][$pageModeField]; //2015-06-15-20pages-excess-page-engclassic
                $pageCount = $pages[$i][$pageModeField];
            }
        } else {
            $pageCount = $pages[$i][$pageModeField];
        }

        for ($j = 1; $j <= $pageCount; $j ++) {
            /*
             * /
             * if($istrials_engClassic && $total_page_count>$trial_engClassic_limit){
             * break;
             * }
             * if($istrials_youngReaders && $total_page_count > $trial_book_limit){
             * break;
             * }
             * if($istrials && $total_page_count>$trial_book_limit){
             * break;
             * }
             * /*
             */
            if ($enablePageLimit && $total_page_count > $pageLimit - 1) {
                break;
            }
            /* */
            $js .= "bookdata.pages.push({'idref':'" . $refID . "', 'href':'" . $fileName . "', 'session':'" . $j . "'});\n";
            $total_page_count ++;
        }
    }
    if ($IsFirstPageBlank && ! (($istrials && $i > 19))) {
        $i = count($pages) - 1;
        $refID = $pages[$i]['RefID'];
        $fileName = $pages[$i]['FileName'];
        $pageCount = $pages[$i][$pageModeField];
        for ($j = 1; $j <= $pageCount; $j ++) {
            $js .= "bookdata.pages.push({'idref':'" . $refID . "', 'href':'" . $fileName . "', 'session':'" . $j . "', 'extra': '1'});\n";
        }
    }
}
for ($i = 0; $i < count($bookmarks); $i ++) {
    $js .= "bookdata.bookmarks.push(" . $bookmarks[$i]['PageNum'] . ");\n";
}

echo $libebookreader_ui->Get_Layout_Start();
echo $libebookreader_ui->Get_eBook_Reader_Index($_SESSION['eBookReaderPageMode'], $isImageBook, $imageWidth);
?>

<style>
    .summary_btn { //20140919
    -newfeature2014 line-height: 20px;
        background: url(../../../../../images/2009a/icon_public.gif) 0px 0px
        no-repeat;
        cursor: pointer;
        vertical-align: super;
        font-size: 0.6em;
        margin-left: 0.2%;
        margin-right: 1%;
    }

    .tool_glossary_left {
        display: block;
        background:
                url(../../../../../images/2009a/ebook_reader/tool/highlight_tool_left.png)
                no-repeat left top;
        height: 80px;
        padding-left: 20px;
    }

    .tool_glossary_right {
        display: block;
        background:
                url(../../../../../images/2009a/ebook_reader/tool/highlight_tool_right.png)
                no-repeat right top;
        height: 80px;
        padding-right: 20px;
    }

    .tool_glossary_bg {
        display: block;
        background:
                url(../../../../../images/2009a/ebook_reader/tool/highlight_tool_bg.png)
                no-repeat center;
        height: 80px;
    }

    .tool_glossary_list {
        display: block;
        margin-left: -10px;
        margin-right: -10px;
        padding-top: 10px;
    }

    .tool_glossary_list  ul {
        display: block;
        margin: 0;
        padding: 0;
        float: left;
        padding-top: 2px;
    }

    .tool_glossary_list  ul li {
        list-style-type: none;
        display: block;
        float: left;
        width: auto;
        padding-top: 5px;
        height: 35px;
    }

    audio::-internal-media-controls-download-button {
        display: none;
        pointer-events: none;
    }

    audio::-webkit-media-controls-enclosure {
        overflow: hidden;
    }

    audio::-webkit-media-controls-panel {
        width: calc(100% + 25px); /* Adjust as needed */
    }

    #tool_voice.istts audio::-webkit-media-controls-panel {
        width: calc(100% - 30px); /* Adjust as needed */
    }

    #tool_voice.istts audio {
        min-width: 100px;
    }

    #tool_voice.istts {
        width: 90px;
    }

    .page {
        page-break-before: always;
        page-break-after: always;
    }
</style>
<script language="JavaScript">
    var browsertype = '<?=$browsertype?>';
    var audio_list = new Array(<?=implode(",",$audio_pages)?>);
    var temp_list = new Array('<?=implode("','",$temp_arr)?>');
    var toc_list = new Array('<?=implode("','",$toc_arr)?>');
    var toc2_list = new Array('<?=implode("','",$toc2_arr)?>');
    <?=$js?>

    var th = ['','thousand','million','billion','trillion'];

    var dg = ['zero','one','two','three','four','five','six','seven','eight','nine'];
    var tn = ['ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen'];
    var tw = ['twenty','thirty','forty','fifty','sixty','seventy','eighty','ninety'];
    function toWords(s){s = s.replace(/[\, ]/g,''); if (s != parseFloat(s)) return 'not a number'; var x = s.indexOf('.');
        if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++)
        {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}}
        else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}}
        if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');
    }

    navigator.sayswho= (function(){
        var ua= navigator.userAgent, tem,
            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if(/trident/i.test(M[1])){
            tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'IE '+(tem[1] || '');
        }
        if(M[1]=== 'Chrome'){
            tem= ua.match(/\bOPR\/(\d+)/)
            if(tem!= null) return 'Opera '+tem[1];
        }
        M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
        if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
        return M.join(' ');
    })();

    $('#highlight_notes_pager_notes').css("resize", "none"); //2013-03-14 Charles Ma
    $(".cup_mp3_player").css("zoom","115%");

    $("#highlight_pulldown_control1").live("mouseup touch", function(){
        $("#highlight_pulldown1").css("display" , $("#highlight_pulldown1").css("display") == "none" ? "":"none" );
        $(".selected_hightlight").removeClass("selected_hightlight");
        $("#highlight_pulldown_control1").addClass("selected_hightlight");
        $(".highlight_pulldown2").css("display","none");
    });

    $("#highlight_pulldown_control2").live("mouseup touch", function(){
        $("#highlight_pulldown2").css("display" , $("#highlight_pulldown2").css("display") == "none" ? "":"none" );
        $(".selected_hightlight").removeClass("selected_hightlight");
        $("#highlight_pulldown_control2").addClass("selected_hightlight");
        $(".highlight_pulldown1").css("display","none");
    });

    //20140919-newfeature2014
    $('.summary_btn').live("mouseup touch",function(){
        var current_visiblity = $("#tool_summary_paper").css("display");
        $("#tool_summary_paper").css("display", current_visiblity == "none" ? "" : "none");
        $("#notes_summary_notes").val($(".summary").html());
        var dv_ref = $('#book_structure');
        $("#tool_summary_paper").css("left",dv_ref.offset().left);

    });

    var glossary_page = 0;

    //20140919-newfeature2014
    $('#related_result_num').live("mouseup touch",function(){
        bookdata.searching2 = false;
    });

    $('#related_result_close').live("mouseup touch",function(){
        bookdata.searching2 = false;
        $("#tool_page_related").hide();
    });

    $('.btn_glossary_related').live("mouseup touch",function(){

        $('#tool_highlight_board').hide();

        if($("#tool_page_related").css("display") != "none"){
            $("#tool_page_related").hide();
            bookdata.searching2 = false;
        }
        else{

            if(bookdata.pageMode == 2){
                var total_page = (bookdata.pages.length - 1) * 2;
                if($(this).attr("id") == "btn_glossary_left")	{
                    var currentPage = (bookdata.currentPage * 2 - 1);
                    $("#tool_glossary_paper").attr("class", "tool_glossary_list_pageR");
                }
                else {
                    var currentPage = Math.min((bookdata.currentPage * 2), total_page);
                    $("#tool_glossary_paper").attr("class", "tool_glossary_list_pageL");
                }
            }else{
                var currentPage = bookdata.currentPage;
            }

            previous_page_num = currentPage;

            $("#tool_page_related").show();

            var ul =  $('div.related_result ul'); ul.html("");
            var span_total = $('#related_result_num'); span_total.html("");

            if(bookdata.searching2){ // stop search
                bookdata.searching2 = false;
            }else{
                $("#related_result_num").html('Searching...');

                bookdata.searchingKeyword2 = $.trim($(this).attr("read"));


                if(bookdata.pageMode == 2){
                    if( parseInt($(this).parent().parent().css("top")) > 350  ){
                        $("#tool_page_related").css("top", "55px");
                    }else{
                        $("#tool_page_related").css("top", "350px");
                    }
                    if( parseInt($(this).attr("page")) % 2 == 1  ){
                        $("#tool_page_related").css("left", parseInt($("#btn_prev_page").css("left")) + 15);
                    }else{
                        $("#tool_page_related").css("left", parseInt($("#btn_next_page").css("left")) - 500);
                    }
                }else{
                    if( parseInt($(this).parent().parent().css("top")) > 450  ){
                        $("#tool_page_related").css("top", "40px");
                    }else{
                        $("#tool_page_related").css("top", "450px");
                    }
                    <?if($platform!='iPad'){?>
                    $("#tool_page_related").css("left", 270);
                    <?}else{?>
                    $("#tool_page_related").css("left", 180);
                    <?}?>
                }
                if(bookdata.searchingKeyword2 == ''){
                    return;
                }
                // start search
                bookdata.searching2 = true;
                bookdata.searchingFile2 = '';
                bookdata.searchingPageIndex2 = 0;
                bookdata.searchingCount2 = 0;

                jsDoSearchAdvanced();
            }
        }
    });


    $('.glossary_btn').live("mouseup touch",function(){
        if(bookdata.pageMode == 2){
            var total_page = (bookdata.pages.length - 1) * 2;
            if($(this).attr("id") == "btn_glossary_left")	{
                var currentPage = (bookdata.currentPage * 2 - 1);
                $("#tool_glossary_paper").attr("class", "tool_glossary_list_pageR");
            }
            else {
                var currentPage = Math.min((bookdata.currentPage * 2), total_page);
                $("#tool_glossary_paper").attr("class", "tool_glossary_list_pageL");
            }
        }else{
            var currentPage = bookdata.currentPage;
        }

        previous_page_num = currentPage;

        $('*').qtip('hide');
        $("#tool_page_related").hide();
        bookdata.searching2 = false;

        var val = $(this).attr("value");

        if(bookdata.pageMode == 2){
            var chapter_name = toc_list[toc2_list.indexOf(bookdata.pages[Math.ceil(currentPage/2)]["href"])];
        }else{
            var chapter_name = toc_list[toc2_list.indexOf(bookdata.pages[currentPage]["href"])];
        }

        if(val == undefined ){
            $("#glossary_page").html("Page");
        }else{
            $("#glossary_page").html("");
        }

        $("#glossary_header").html("Glossary - "+chapter_name);

        if(glossary_page != currentPage){
            var current_visiblity = "none";
        }else{
            var current_visiblity = $("#tool_glossary_paper").css("display");
        }
        $("#tool_glossary_paper").css("display", current_visiblity == "none" ? "" : "none");

        var content = "";
        var current_glossary_page = 0;

        $("#notes_glossary_notes").html("");

        $(".glossary_data").each(function() {
            //if(page == $(this).attr("page") || val == undefined ){

            if(current_glossary_page != $(this).attr("page")){
                current_glossary_page = $(this).attr("page");
                if(currentPage == $(this).attr("page")){
                    var page_current = "current"; var page_id = "id='page_current'";
                }else{
                    var page_current = ""; var page_id = "";
                }
                if(content != ""){
                    content += '<br/><h1 '+page_id+' class="pageno_header '+page_current+'">Page '+current_glossary_page+'</h1><br/>';
                }else{
                    content += '<h1 '+page_id+' class="pageno_header '+page_current+'">Page '+current_glossary_page+'</h1><br/>';
                }
            }

            content += '<a href="javascript: void(0)" ';
            if(val == undefined) content += ' onclick="jsToPageFromGlossary('+$(this).attr("page")+','+$(this).attr("value")+')" ';
            content += ' class="glossary_vocab">'+$(this).attr("wording")+'</a>';
            content += '<a href="javascript: void(0)" value="'+$(this).attr("value")+'" ';
            content += ' class="glossary_tts_btn btn_glossary_texttospeech">&nbsp;</a>'+': ' + $(this).html() + "<br/>";

        });
        if(content == ""){
            content = "No glossary for this chapter.";
        }

        $("#notes_glossary_notes").html(content);
        $("#glossary_list").scrollTop(0);
        if($("#page_current").offset()){
            $("#glossary_list").scrollTop($("#page_current").offset().top - $("#glossary_list").offset().top);
        }

        var dv_ref = $('#book_structure');
        if(bookdata.pageMode == 1){
            $("#tool_glossary_paper").css("width","730px");
        }
        if(currentPage%2 == 0 || bookdata.pageMode == 1){
            $("#tool_glossary_paper").css("left",dv_ref.offset().left);
        }else{
            $("#tool_glossary_paper").css("left",parseInt(dv_ref.offset().left) + 520);
        }

    });

    $('.glossary_tts_btn').live("mouseup touch",function(){
        var val = $(this).attr("value");
        var text = "";
        $(".glossary_data").each(function() {
            if($(this).attr("value")==val){
                text = $(this).attr("read");
            }
        });

        var dv = $('#tool_voice');
        dv.css({"z-index" : 99999,position:'absolute', left: $(this).offset().left, top: $(this).offset().top + $(this).height()});
        jsLoadTextToSpeech(text);
        <?if($platform!='iPad'){?>
        $('#tool_voice').hide();
        $('#tool_voice').removeClass('istts');
        <?}?>
    });

    $('.glossary_playback_btn').live("mouseup touch",function(){
        var playback = $(this).attr("value");
        var audio = $("#mp3_player");
        $('#mp3_src').attr("src", playback);

        audio[0].pause();
        audio[0].load();//suspends and restores all audio element
        audio[0].play();
    });

    $('.mulmedia_btn').live("mouseup touch",function(){
        var pageno = parseInt(this.parentElement.parentElement.getAttribute('pageno'));
        $.fancybox( [$(this).attr("src")],
                    {
                        helpers	: {
                            title	: {
                                type: 'outside'
                            }
                        },
                        beforeShow: function(){
                            $('.fancybox-wrap').css('visibility', 'hidden');
                        },
                        afterShow: function(){
                            if(bookdata.pageMode==2){
                                if(pageno%2==0){
                                    var leftParm = $('.fancybox-wrap').offset().left;
                                    $('.fancybox-wrap').css('left', leftParm*2);
                                }
                            }
                            $('.fancybox-wrap').css('visibility', 'visible');
                        },
                        beforeClose: function(){
                            $('.fancybox-wrap').css('visibility', 'hidden');
                        }
                    });
//	$('.fancybox-overlay').css({"zoom": 1/$("body").css("zoom")});
        $('.fancybox-wrap').css({"zoom": 1/$("body").css("zoom")});
    });

    $('#btn_notes_glossary_close').mouseup(function(){
        $('#tool_voice').hide();
        $('#tool_voice').removeClass('istts');
        $('#tool_glossary_paper').hide();
    });

    $('#btn_notes_summary_close').mouseup(function(){
        $('#tool_summary_paper').hide();
    });
    var dvb = $("#tool_glossary_board");

    var current_glossary_val = 0;
    var previous_page_num = 0;

    $('#btn_return').on("click touch", function(){

//  var pageIndex = jsTruePageNumberToPageIndex($("this").attr("value"));
//  jsLoadDocument(pageIndex);
//  jsHideTocAfterChangePage();
        jsToPageFromNotes(previous_page_num);
        $("#btn_return").hide();
        $("#tool_glossary_paper").hide();
    })

    function jsToPageFromGlossary(page, value)
    {
        var pageIndex = jsTruePageNumberToPageIndex(page);
        jsLoadDocument(pageIndex);
        jsHideTocAfterChangePage();

        <?if($platform!='iPad'){?>
        $("#btn_return").css({"display": "", "left" :"500px","top": "20px",  "z-index": 4});
        <?}else{?>
        $("#btn_return").css({"display": "", "left" :"400px","top": "20px",  "z-index": 4});
        <?}?>

        $("#inner_page_num").html(previous_page_num);

        setTimeout(function(){
            var chapter_name = toc_list[toc2_list.indexOf(bookdata.pages[Math.ceil(page/2)]["href"])];

            $("#glossary_page").html("");
            $("#glossary_header").html("Glossary - Page "+page);
            $("#tool_glossary_paper").css("display",  "" );

            var content = "";
            $(".glossary").each(function() {
                if(value == $(this).attr("value") ){
                    $(this).trigger( "mouseover" );
                }
            });

            $(".glossary_data").each(function() {
                if(page == $(this).attr("page") ){
                    content += '</span><a href="javascript: void(0)" class="glossary_vocab">'+$(this).attr("wording")+'</a>';
                    content += '<a href="javascript: void(0)" value="'+$(this).attr("value")+'" ';
                    content += ' class="glossary_tts_btn btn_glossary_texttospeech">&nbsp;</a>'+': ' + $(this).html() + "<br/>";
                }
            });

            if(content == ""){
                content = "No glossary for this chapter.";
            }

            $("#notes_glossary_notes").html(content);
            var dv_ref = $('#book_structure');
            if(bookdata.pageMode == 1){
                $("#tool_glossary_paper").css("width","730px");
            }
            if(page%2 == 0 || bookdata.pageMode == 1 ){
                $("#tool_glossary_paper").css("left",dv_ref.offset().left);
            }else{
                $("#tool_glossary_paper").css("left",parseInt(dv_ref.offset().left) + 520);
            }

        },500);
    }

    function glossary_attach(){
        <?if($engclassicMode == 1){?>
        if(isFeatureOn){
            $('.glossary').addClass("glossary_on");
            $('.mulmedia_btn').addClass("mulmedia_btn_on");
        }
        <?}?>

        $('.glossary').each(function() {
            var zoom_effect = parseFloat($("body").css("zoom")) - 1;

            $(this).qtip({
                             show: 'mouseover touch',
                             hide: '',
                             content: {
                                 text: function(event, api) {
                                     var val = $(this).attr("value");
                                     var text = ""; var page = "";

                                     <?php if ($engclassicMode){ ?>
                                     if($(".glossary_data").length == 0){
                                         $("#btn_glossary").css("display","none");
                                     }else{
                                         $("#btn_glossary").css("display","");
                                     }
                                     <?php } ?>
                                     $(".glossary_data").each(function() {
                                         if($(this).attr("value")==val){
                                             text = $(this).html();
                                             page = $(this).attr("page");
                                             read = $(this).attr("read");
                                         }
                                     });
                                     text += '<a href="javascript:void(0)"  class="btn_glossary_related" page="'+page+'" read="'+read+'">&nbsp;</a>';
                                     return text;
                                 },
                                 title: function(event, api) {
                                     var val = $(this).attr("value");
                                     var playback = "";
                                     var title = "";
                                     $(".glossary_data").each(function() {
                                         if($(this).attr("value")==val){
                                             title = $(this).attr("wording");
                                             playback = $(this).attr("playback");
                                         }
                                     });
                                     title += '<span style="margin-top: -11px;margin-bottom: -7px;" class="glossary_tts_btn btn_glossary_texttospeech" value="'+val+'"></span>';
                                     if(playback){
                                         title += '<span class="glossary_playback_btn btn_glossary_texttospeech" value="'+playback+'"></span>';
                                     }
                                     return title;
                                 }
                             },
                             style: {
                                 classes: 'qtip-rounded qtip-shadow'
                             },
                             position: {
                                 viewport: $(window),
                                 target: 'mouse', // Use the mouse position as the position origin
                                 adjust: { mouse: false },
                                 hide: { distance: 20 },
                                 show: { solo: true }
                             },
                             events: {
                                 hidden: function(event, api) {
                                     $('#tool_voice').hide();
                                     $('#tool_voice').removeClass('istts');
                                 },
                                 show: function(event, api) {
                                     $('*').qtip('hide');
                                     $("#tool_page_related").hide();
                                     bookdata.searching2 = false;

                                     var $el = $(api.elements.target[0]);
                                     var isiPad = navigator.userAgent.match(/iPad/i) != null;
                                     var $lineheight = 0;
                                     $lineheight = parseFloat($('div.page p').css('line-height'));
                                     if(navigator.sayswho.indexOf("Firefox")>=0){
                                         var transform = $('body').css('transform');
                                         var zoom = parseFloat(transform.split("matrix(")[1].split(",")[0]);
                                         if($lineheight >= $el.outerHeight()){
                                             $el.qtip('option', 'position.target', [($el.offset().left+$el.outerWidth()/2)/zoom,($el.offset().top-5)/zoom]);
                                         }else{
                                             $el.qtip('option', 'position.target', [($el.offset().left + 10)/zoom,($el.offset().top-35)/zoom]);
                                         }
                                     }else{
                                         var zoom = $('body').css('zoom');
                                         if($lineheight >= $el.outerHeight()){
                                             if(zoom > 1)
                                                 $el.qtip('option', 'position.target', [($el.offset().left+$el.outerWidth()/2)*zoom,($el.offset().top+5)*zoom]);
                                             else
                                                 $el.qtip('option', 'position.target', [($el.offset().left+$el.outerWidth()/2),($el.offset().top+5)]);
                                         }else{
                                             if (zoom > 1)
                                                 $el.qtip('option', 'position.target', [($el.offset().left + 10)*zoom,($el.offset().top+35)*zoom]);
                                             else
                                                 $el.qtip('option', 'position.target', [($el.offset().left + 10),($el.offset().top+35)]);
                                         }
                                     }

                                     if(isiPad){
                                         if($lineheight >= $el.outerHeight()){
                                             $el.qtip('option', 'position.target', [$el.offset().left+$el.outerWidth()/2,$el.offset().top+5]);
                                         }else{
                                             $el.qtip('option', 'position.target', [($el.offset().left + 10),($el.offset().top+35)]);
                                         }
                                     }
                                 }
                             }
                         });

        });
    }


    //20140919-newfeature2014

    <?php if(!$ForceToOnePageMode){ ?>
    if(bookdata.pageMode == 2){ //20131204-ebook-righttoleft
        <?php if($isRTL == 1) { ?>
        $("#btn_prev_page").append('<div class="page_tool_prev"><a title="Next Page"><span>&nbsp;</span><em></em></a></div>');
        <?php } else { ?>
        $("#btn_prev_page").append('<div class="page_tool_prev"><a title="Previous Page"><span>&nbsp;</span><em></em></a></div>');
        <?php } ?>
    }else{
        <?php if($isRTL == 1) { ?>
        $("#btn_prev_page").append('<div class="page_tool_prev"><a  title="Next Page"><span>&nbsp;</span></a></div>');
        <?php } else { ?>
        $("#btn_prev_page").append('<div class="page_tool_prev"><a  title="Previous Page"><span>&nbsp;</span></a></div>');
        <?php } ?>
    }
    <?php if($isRTL == 1) { ?>
    $("#btn_next_page").append('<div class="page_tool_next"><a title="Previous Page"><span>&nbsp;</span><em></em></a></div>');
    <?php } else { ?>
    $("#btn_next_page").append('<div class="page_tool_next"><a title="Next Page"><span>&nbsp;</span><em></em></a></div>');
    <?php } ?>

    $("#btn_prev_page").css("background-color","");
    $("#btn_next_page").css("background-color","");

    $(".page_tool_prev .page_tool_next").css("display", "block");
    $(".page_tool_prev .page_tool_next").css("position", "absolute");
    $(".page_tool_prev .page_tool_next").css("width", "20px");
    $(".page_tool_prev .page_tool_next").css("left", "0");

    <?php if($isRTL == 1) { ?>
    $(".page_tool_prev .page_tool_next").css("z-index", "5");
    <?php } else { ?>
    $(".page_tool_prev .page_tool_next").css("z-index", "2");
    <?php } ?>

    ////2013-05-30 Charles Ma////

    $(".page_tool_prev a span").css("display", "block");
    $(".page_tool_prev a span").css("background", "url(../../../../../images/2009a/ebook_reader/page/btn_prev.png) left center no-repeat");
    $(".page_tool_prev a span").css("width", "30px");
    $(".page_tool_prev a span").css("position", "absolute");

    $(".page_tool_prev a").css("margin-top", "5px");
    $(".page_tool_next a").css("margin-top", "5px");
    $(".page_tool_prev a").css("display", "block");
    $(".page_tool_next a").css("display", "block");

    if(bookdata.pageMode == 2){
        $(".page_tool_prev a").css("height", "648px");
        $(".page_tool_prev a span").css("height", "648px");
    }else{
        $(".page_tool_prev a").css("height", "928px");
        $(".page_tool_prev a span").css("height", "928px");
    }

    $(".page_tool_prev a em").css("background","url(../../../../../images/2009a/ebook_reader/page/page_flip_left.png)");
    $(".page_tool_prev a em").css("left","18px");
    $(".page_tool_prev a em").css("top","5px");
    $(".page_tool_prev a em").css("width","89px");
    $(".page_tool_prev a em").css("height","157px");
    $(".page_tool_prev a em").css("position","absolute");
    $(".page_tool_prev a em").css("display","none");
    $(".page_tool_prev a em").css("background-repeat","no-repeat");

    $(".page_tool_prev a").hover(function() {
        $(this).css("background", "url(../../../../../images/2009a/ebook_reader/page/over_bg.png)");
        $(this).css("display", "block");
        $(this).css("border-radius", "500px 0 0 500px");
        $(this).css("width", "20px");
        $(".page_tool_prev a em").css("display","block");
    }, function() {
        $(this).css("background", "");
        $(this).css("display", "block");
        $(".page_tool_prev a em").css("display","none");
    });

    $(".page_tool_prev a span").hover(function() {
        $(this).css("background-position", "right center");
    }, function() {
        $(this).css("background-position", "left center");
    });

    $(".page_tool_next a span").css("display", "block");
    $(".page_tool_next a span").css("background", "url(../../../../../images/2009a/ebook_reader/page/btn_next.png) left center no-repeat");
    $(".page_tool_next a span").css("width", "30px");
    $(".page_tool_next a span").css("position", "absolute");
    $(".page_tool_next a span").css("height", "648px");
    $(".page_tool_next a span").css("background-position", "12% 50%");
    $(".page_tool_next a span").css("margin-left", "-5px");

    if(bookdata.pageMode == 2){
        $(".page_tool_next a").css("height", "648px");
        $(".page_tool_next a span").css("height", "648px");
    }else{
        $(".page_tool_next a").css("height", "928px");
        $(".page_tool_next a span").css("height", "928px");
    }

    $(".page_tool_next a em").css("background","url(../../../../../images/2009a/ebook_reader/page/page_flip.png)");
    $(".page_tool_next a em").css("right","49px");
    $(".page_tool_next a em").css("top","5px");
    $(".page_tool_next a em").css("width","89px");
    $(".page_tool_next a em").css("height","157px");
    $(".page_tool_next a em").css("position","absolute");
    $(".page_tool_next a em").css("display","none");
    $(".page_tool_next a em").css("background-repeat","no-repeat");

    $(".page_tool_next a").hover(function() {
        $(this).css("background", "url(../../../../../images/2009a/ebook_reader/page/over_bg.png)");
        $(this).css("display", "block");
        $(this).css("border-radius", "0 200px 200px 0");
        $(this).css("width", "20px");
        $(".page_tool_next a em").css("display","block");
    }, function() {
        $(this).css("background", "");
        $(this).css("display", "block");
        $(".page_tool_next a em").css("display","none");
    });

    $(".page_tool_next a span").hover(function() {
        $(this).css("background-position", "right center");
    }, function() {
        $(this).css("background-position", "left center");
    });

    <?php }?>

    if(bookdata.inputFormat == 4){
        $("#btn_feature").css("display","none");
        $("#btn_mp3_r").css({"display": "none"});

        $('#mp3_box').css({ "display": "none", "width": "370px",  "height": "200px",
                              "background-color": "#f5f2f0", "border-radius": "10px", "margin-left": "570px",
                              "margin-top": "40px", "counter-reset": "myAwesomeCounter",  "font": "1em sans-serif", "overflow": "auto"});
        $('#mp3_box ul').css({"list-style-type": "none"});
        $("#btn_mp3").css({"display": "none"});
        $("#btn_cupleft_mp3").css({"display": "none"});
        $("#btn_cupright_mp3").css({"display": "none"});
        $('#player_board1').css("display", "none");
        $('#player_board2').css("display", "none");

//  $.post(
//    '/home/eLearning/ebook_reader/ajax_load.php',
//    {
//      "task":"Get_MP3_Srcs",
//      "BookID":bookdata.id
//    },
//    function(data){
        $('#mp3_box ul').html("<?=$libebookreader->retrieveMP3Path($bid)?>");

        $('#tool_top .mp3_item a').css("background-image", "url()");
        $('#tool_top .mp3_item a').css("width", "100%");

        $('.mp3_item').bind("click tap" , function(){
            $('.mp3_item a').removeClass("current");
            $(this).find("a").toggleClass("current");

            var audio = $("#mp3_player");
            $('#mp3_src').attr("src", $(this).attr("src"));

            audio[0].pause();
            audio[0].load();//suspends and restores all audio element
            audio[0].play();
        });

        function switchMp3(){
            //btn_speech_anim1
            setTimeout(function(){
                if(!isplaying) return;
                $('#btn_mp3').attr("class", "btn_speech_anim"+mp3anim);
                mp3anim = mp3anim == 4 ? 1 : mp3anim + 1;
                switchMp3();
            }, 500);
        }

        $('#mp3_player').on('playing', function() {
            if(!isplaying){
                isplaying = true;
                switchMp3();
            }
        });

        $('#mp3_player').on('pause ended', function() {
            isplaying = false;
            $('#btn_mp3').attr("class", "btn_speech");
        });

        $('#btn_mp3').bind("click tap" , function(){
            $('#mp3_box').css("display",  $('#mp3_box').css("display") == "block" ? "none" : "block" );
        });

        <?php if(sizeof($audio_files) <= 0){?>
        $("#btn_mp3_tw").css({"display": "none"});
        <?php }?>

//    }
//  );
    }else{
        $("#btn_mp3_tw").css({"display": "none"});
        <?if(sizeof($audio_files) >0 && $isnormalaudio){?>	//20140526-cupbook

        $('#cup_mp3_box ul').html("<li></li>");

        $('#btn_cupleft_mp3').bind("click tap" , function(){
            var total_page = (bookdata.pages.length - 1) * 2;
            <?if ($cupecMode || $youngreaderMode){ ?>
            var left_page = (bookdata.currentPage * 2 - 1);
            var right_page = Math.min((bookdata.currentPage * 2), total_page);
            <?}else{?>
            var left_page = bookdata.currentPage;
            var right_page = Math.min(bookdata.currentPage+1, total_page);
            <?}?>
            var audio = $("#cup_mp3_player");
            var audio2 = $("#cup_mp3_player_r");

            var isInit = false;

            if($('#cup_mp3_src').attr("src") != "/file/eBook/<?=$bid?>/OEBPS/Audio/p"+left_page+".mp3"){
                $('#cup_mp3_src').attr("src", "/file/eBook/<?=$bid?>/OEBPS/Audio/p"+left_page+".mp3");
                isInit = true;
            }

            $('#player_board2').css("display", "none");
            audio2[0].pause();

            if($('#player_board1').css("display") == "none"){
                $('#player_board1').css("display", "");
                if(isInit){
                    audio[0].pause();
                    audio[0].load();
                    audio[0].play();
                }
            }else{
                $('#player_board1').css("display", "none");
            }

        });

        $('#btn_cupright_mp3').bind("click tap" , function(){
            if(bookdata.pageMode == 2){
                var total_page = (bookdata.pages.length - 1) * 2;
                <?if ($cupecMode || $youngreaderMode){ ?>
                var left_page = (bookdata.currentPage * 2 - 1);
                var right_page = Math.min((bookdata.currentPage * 2), total_page);
                <?}else{?>
                var left_page = bookdata.currentPage;
                var right_page = Math.min(bookdata.currentPage+1, total_page);
                <?}?>

                var audio2 = $("#cup_mp3_player");
                audio2[0].pause();
            }else{
                var right_page = bookdata.currentPage;

            }
            var audio = $("#cup_mp3_player_r");

            var isInit = false;
            if($('#cup_mp3_src_r').attr("src") != "/file/eBook/<?=$bid?>/OEBPS/Audio/p"+right_page+".mp3"){
                $('#cup_mp3_src_r').attr("src", "/file/eBook/<?=$bid?>/OEBPS/Audio/p"+right_page+".mp3");
                isInit = true;
            }

            $('#player_board1').css("display", "none");

            if($('#player_board2').css("display") == "none"){
                $('#player_board2').css("display", "");
                if(isInit){
                    audio[0].pause();
                    audio[0].load();
                    audio[0].play();
                }
            }else{
                $('#player_board2').css("display", "none");
            }

        });
        $("#btn_highlight_texttospeech").css({"display": "none"});		// 20140422-cupbook

        <?}else{?>
        $("#btn_cupleft_mp3").css({"display": "none"});		// 20140422-cupbook
        $("#btn_cupright_mp3").css({"display": "none"});		// 20140422-cupbook
        <?}?>
        $(".player_board").css({"display": "none"});		// 20140422-cupbook
        $("#btn_mp3").css({"display": "none"});
        $("#btn_mp3_r").css({"display": "none"});

        <?if($engclassicMode == 1){?>

        $("#btn_highlight_texttospeech").css({"display": ""});
        var isFeatureOn = true;

        $("#btn_feature").unbind("click tap").bind("click tap",function(){ //20141024
            isFeatureOn = !isFeatureOn;
            if(isFeatureOn){
                $(this).attr("class","btn_feature");
                $(".glossary").addClass("glossary_on");
                $(".mulmedia_btn").addClass("mulmedia_btn_on");
            }else{
                $(this).attr("class","btn_feature_hide");
                $(".glossary").removeClass("glossary_on");
                $(".mulmedia_btn").removeClass("mulmedia_btn_on");
            }
        });

        $("#btn_mp3, #btn_mp3_r").unbind("click tap").bind("click tap",function(){ //20141024
            if(bookdata.pageMode == 2){
                var total_page = (bookdata.pages.length - 1) * 2;
                if($(this).attr("id") == "btn_mp3")	var currentPage = (bookdata.currentPage * 2 - 1);
                else var currentPage = Math.min((bookdata.currentPage * 2), total_page);
            }else{
//			  var currentPage = bookdata.currentPage+1; //20150311-currentPage
                var currentPage = bookdata.currentPage;
            }

            var txt = "";
            $(".page").each(function(){
                if($(this).attr("pageno") == currentPage){
                    txt = $(this).text().replace(/(\t|\r\n|\n|\r)/gm,"");
                }
            });

            var dv = $('#tool_voice');
            var btn = $(this);

            var num_match = txt.match(/[0-9]+/g);
            if(num_match){
                for(var i =0; i < num_match.length ; i++){
                    var re = new RegExp(num_match[i],"g");
                    txt = txt.replace(re,toWords(num_match[i]));
                }
            }

            txt = txt.replace(/([’|\”|\“|\u00A0|\uFEFF|\u0020|\u3000]+)/g," ").trim();
            txt = txt.replace(/([\u00F6|\u00D6]+)/g,"o").trim();

            if(btn.is(':visible') && dv.css("display") == "none"){
                dv.css({position:'absolute', left: btn.offset().left, top: btn.offset().top + btn.height()});
                if(current_tts_page != currentPage){
                    jsLoadTextToSpeech(txt,3);
                }else{
                    $('#tool_voice').show();
                }
            }else{
                if(current_tts_page != currentPage){
                    dv.css({position:'absolute', left: btn.offset().left, top: btn.offset().top + btn.height()});
                    jsLoadTextToSpeech(txt,3);
                }else{
                    dv.slideUp('fast');
                    $('#tool_voice').hide();
                    $('#tool_voice').removeClass('istts');
                }
            }

            current_tts_page = currentPage;
        });
        <?} else{?>
        $("#btn_feature").css("display","none");
        <?}?>
    }

    var current_tts_page = 0;

    var page_speech_activated = "";

    $('#btn_texttospeech_pdf_left').bind("click tap", function(){//2013-06-24 Charles Ma

        var dv = $('#tool_voice');
        var btn = $('#btn_texttospeech_pdf_left');

        if(btn.is(':visible') && (dv.css("display") == "none" || page_speech_activated != "left")){
            dv.css({position:'absolute', left: btn.offset().left, top: btn.offset().top + btn.height()});
            //if(isResize != true){
            var txt = $("#book_content_left_page .ebook_pdf")[0].innerText;
            jsLoadTextToSpeech(txt);
            //}
        }else{ //2013-03-25 eBook-update-130325a
            jsLoadTextToSpeech(" ");
            dv.slideUp('fast');
            $('#tool_voice').hide();
            $('#tool_voice').removeClass('istts');
        }
        page_speech_activated = "left";
    })

    $('#btn_texttospeech_pdf_right').bind("click tap", function(){//2013-06-24 Charles Ma

        var dv = $('#tool_voice');
        var btn = $('#btn_texttospeech_pdf_right');

        if(btn.is(':visible') && (dv.css("display") == "none" || page_speech_activated != "right")){

            dv.css({position:'absolute', left: btn.offset().left, top: btn.offset().top + btn.height()});
            //if(isResize != true){
            var txt = $("#book_content_right_page  .ebook_pdf")[0].innerText;
            jsLoadTextToSpeech(txt);
            //}
        }else{ //2013-03-25 eBook-update-130325a
            jsLoadTextToSpeech(" ");
            dv.slideUp('fast');
            $('#tool_voice').hide();
            $('#tool_voice').removeClass('istts');
        }
        page_speech_activated = "right";
    })

    <?php if($ForceToOnePageMode) { ?>
    $("#btn_book_diminish").css("display", "none");  // 2013-11-19 (Charles Ma) 20131119-bookreaderpageno
    $("#btn_book_enlarge").css("display", "none"); // 2013-11-19 (Charles Ma) 20131119-bookreaderpageno

    $("#btn_book_setting").css("display", "none");  //edited 2013-06-10(CharlesMa)
    ////////
    $("#btn_pagemode_two").css("display", "none");  //edited 2013-01-25(CharlesMa)
    $("#btn_skin_yellow").css("display", "none"); //edited 2013-03-22(CharlesMa)

    $("#book_structure").css("width", "1040px");
    $("#book_structure").css("height", "600px");
    $("#book_structure").css("opacity", "0");//2013-06-06 CharlesMa


    $(".page_content").css("max-height", "600px");
    $(".page_content").css("min-height", "600px");
    $(".page_content").css("display", "none");

    $(".page_content_right").css("height", "600px");
    $(".page_content_right").css("min-height", "600px");
    $(".page_content_right").css("max-height", "600px");


    $(".page_pattern").css("width", "960px");
    $(".page_pattern").css("height", "600px");


    $(".page_content_bg").css("width", "100%");
    $(".page_content_bg").css("height", "600px");
    $(".page_content_bg").css("min-height", "600px");
    $(".page_content_bg").css("max-height", "600px");


    $(".book_1page_structure").css("width", "1040px");
    $(".book_1page_structure").css("height", "650px");
    //$(".book_1page_structure").css("height", "692px");

    $("#book_content").css("opacity", "0"); //2013-06-06 Charles Ma

    $(".book_content_container").css("width", "1000px");
    $(".book_content_container").css("height", "670px");

    $(".book_content_main").css("width", "110%");
    $(".book_content_main").css("height", "110%");

    // 2013-10-22 Charles Ma

    if(navigator.platform.indexOf("iPad") != -1){
        $(".book_content_main").css("padding-left", "50px");

        $(".icon_audio").css("margin-top", "-20px");
        $(".icon_audio_pause").css("margin-top", "-20px");
        $(".icon_audio_bg").css("margin-top", "-20px");
    }else{
        <?php if($platform == 'Andriod'){ ?>
        $(".book_content_main").css("padding-left", "50px");
        <?php }else{ ?>
        $(".book_content_main").css("padding-left", "30px");
        <?php }?>
    }

    $(".book_content_main").css("margin-top", "-41px");

    $(".tool_top_content").css("width", "1040px");
    $(".tool_bottom_content").css("width", "1040px");

    $(".page_content").css("background-image", "url(../../../../../images/2009a/ebook_reader/page/white2/page_left.png)");
    $(".page_content").css("background-position", "left");
    $(".page_content").css("padding-left", "50px");
    $(".page_top").css("background-image", "url(../../../../../images/2009a/ebook_reader/page/white2/page_top_left.png)");
    $(".page_top").css("background-position", "left");
    $(".page_top").css("padding-left", "50px");


    $(".page_bottom").css("background-image", "url(../../../../../images/2009a/ebook_reader/page/white2/page_btm_left.png)");
    $(".page_bottom").css("background-position", "left");
    $(".page_bottom").css("padding-left", "50px");

    /////////////////////////////////

    //$("#btn_prev_page").css("opacity", "100");
    $("#btn_prev_page").css("background-color","");
    //$("#btn_next_page").css("opacity", "100");
    $("#btn_next_page").css("background-color","");

    //  $("#btn_prev_page").append('<div class="page_tool_prev"><a  title="Previous Page">&nbsp;</a></div>');
    //  $("#btn_next_page").append('<div class="page_tool_next"><a  title="Next Page">&nbsp;</a></div>');

    $("#btn_prev_page").append('<div class="page_tool_prev"><a  title="Previous Page"><span>&nbsp;</span></a></div>');
    $("#btn_next_page").append('<div class="page_tool_next"><a  title="Next Page"><span>&nbsp;</span><em></em></a></div>');

    ///////////////////////////////////////////////////////////

    var dv_ref = $('#book_structure');

    $("#book_page_style").append('<div id="new_layout"></div>');

    $("#new_layout").append('<div class="book_left_top"> <div class="book_right_top"><div class="page_left">&nbsp;</div><div class="page_right"><div class="page_image"><span></span></div><div id="page2" class=""><span></span></div><div id="page3" class="page_image_frontpage"><span></span></div></div></div></div>');

    $('.page_right').css("left", "-988px");
    $('.page_right').css("z-index", "0");


    $(".page_image span").css("display", "block");
    $(".page_image span").css("float", "left");
    $(".page_image span").css("background", "url(../../../../../images/2009a/ebook_reader/new_page/page_shadow_right.png) repeat-y left");
    $(".page_image span").css("width", "42px");
    $(".page_image span").css("min-height", "650px");
    $(".page_image span").css("border-radius", "15px 0 0 15px");

    $(".page_left").css("display", "none");

    $("#btn_search").css({"display": "none"});//2013-09-17

    $(".book_left_top").css("display", "none");
    $(".book_left_top").css("background-repeat", "no-repeat");
    $(".book_left_top").css("background-position", "left top");
    $(".book_left_top").css("padding-left", "22px");
    $(".book_left_top").css("background-image", "url(../../../../../images/2009a/ebook_reader/new_page/book_onepage_left_top.png)");
    $(".book_left_top").css("max-height", "95%");

    $(".page_tool_prev .page_tool_next").css("display", "block");
    $(".page_tool_prev .page_tool_next").css("position", "absolute");
    $(".page_tool_prev .page_tool_next").css("width", "20px");
    $(".page_tool_prev .page_tool_next").css("left", "0");
    $(".page_tool_prev .page_tool_next").css("z-index", "2");

    $(".book_right_top").css("background-image", "url(../../../../../images/2009a/ebook_reader/new_page/book_right_top.png)");
    $(".book_right_top").css("padding-right", "18px");


    $(".book_right_top").css("height", dv_ref.height()-30);
    $(".book_right_top").css("background-repeat", "no-repeat");
    $(".book_right_top").css("background-position", "right top");

    $(".page_right").css("height", dv_ref.height());
    $(".page_right").css("margin-bottom", "-30px");
    $(".page_right").css("box-shadow", "0 0 1px #999");
    $(".page_right").css("border-radius", "15px 0 0 15px");
    $(".page_right").css("background-repeat", "no-repeat");

    $(".page_image").css("height", dv_ref.height());
    $(".page_image").css("border-radius", "15px 0 0 15px");
    //$(".page_image").css("background-size", "cover");
    $(".page_image").css("overflow", "hidden");

    $("#new_layout").append('<div class="book_left_bottom"><div class="book_right_bottom"></div></div>');

    $("#new_layout").css("width", "1026px");
    $("#new_layout").css("top", "5px");
    $("#new_layout").css("position", "absolute");

    $(".book_left_bottom").css("padding-left", "22px");
    $(".book_left_bottom").css("display", "none");
    $(".book_left_bottom").css("background-image", "url(../../../../../images/2009a/ebook_reader/new_page/book_onepage_left_bottom.png)");
    $(".book_left_bottom").css("height", "30px");
    $(".book_left_bottom").css("background-repeat", "no-repeat");
    $(".book_left_bottom").css("background-position", "left bottom");

    $(".book_right_bottom").css("padding-right", "18px");
    //$(".book_right_bottom").css("display", "none");
    $(".book_right_bottom").css("background-image", "url(../../../../../images/2009a/ebook_reader/new_page/right_bottom.png)");
    $(".book_right_bottom").css("height", "30px");
    $(".book_right_bottom").css("background-repeat", "no-repeat");
    $(".book_right_bottom").css("background-position", "right bottom");

    //////////////////////////////////////////////

    $("#tool_bottom").css("margin-top","-54px");

    ////2013-05-30 Charles Ma////

    $(".page_tool_prev a span").css("display", "block");
    $(".page_tool_prev a span").css("background", "url(../../../../../images/2009a/ebook_reader/page/btn_prev.png) left center no-repeat");
    $(".page_tool_prev a span").css("width", "30px");
    $(".page_tool_prev a span").css("position", "absolute");
    $(".page_tool_prev a span").css("height", "648px");

    $(".page_tool_prev a").css("margin-top", "5px");
    $(".page_tool_next a").css("margin-top", "5px");
    $(".page_tool_prev a").css("display", "block");
    $(".page_tool_next a").css("display", "block");

    $(".page_tool_prev a").css("height", "648px");

    $(".page_tool_prev a").hover(function() {
        $(this).css("background", "url(../../../../../images/2009a/ebook_reader/page/over_bg.png)");
        $(this).css("display", "block");
        $(this).css("border-radius", "500px 0 0 500px");
        $(this).css("width", "20px");
    }, function() {
        $(this).css("background", "");
        $(this).css("display", "block");
    });

    $(".page_tool_prev a span").hover(function() {
        $(this).css("background-position", "right center");
    }, function() {
        $(this).css("background-position", "left center");
    });

    $(".page_tool_next a span").css("display", "block");
    $(".page_tool_next a span").css("background", "url(../../../../../images/2009a/ebook_reader/page/btn_next.png) left center no-repeat");
    $(".page_tool_next a span").css("width", "30px");
    $(".page_tool_next a span").css("position", "absolute");

    $(".page_tool_next a span").css("height", "648px");
    $(".page_tool_next a span").css("background-position", "12% 50%");
    $(".page_tool_next a span").css("margin-left", "-5px");

    $(".page_tool_next a").css("height", "648px");

    $(".page_tool_next a em").css("background","url(../../../../../images/2009a/ebook_reader/page/page_flip.png)");
    $(".page_tool_next a em").css("right","50px");
    $(".page_tool_next a em").css("top","5px");
    $(".page_tool_next a em").css("width","89px");
    $(".page_tool_next a em").css("height","157px");
    $(".page_tool_next a em").css("position","absolute");
    $(".page_tool_next a em").css("display","none");

    $(".page_tool_next a").on('touchstart mouseover', function(){
        $(this).css("background", "url(../../../../../images/2009a/ebook_reader/page/over_bg.png)");
        $(this).css("display", "block");
        $(this).css("border-radius", "0 200px 200px 0");
        $(this).css("width", "20px");
        $(".page_tool_next a em").css("display","block");
    });

    $(".page_tool_next a").on('touchend mouseout', function(){
        setTimeout(function(){
            $(".page_tool_next a").css("background", "");
            $(".page_tool_next a").css("display", "block");
            $(".page_tool_next a em").css("display","none");
        },500);
    });

    $(".page_tool_next a span").on('touchstart mouseover', function(){
        $(this).css("background-position", "right center");
    });
    $(".page_tool_next a span").on('touchend mouseout', function(){
        $(this).css("background-position", "left center");
    });

    var width_content = $("#book_content").css("width").toString().split("px")[0];
    var width_content_main = $("#book_content_main").css("width").toString().split("px")[0];
    var width_page = (width_content - width_content_main)/2;


    $(".book_info_bottom_right").css("font-size", "1.1em");
    $(".book_info_bottom_right").css("font-family", "'Sofia', cursive");
    $(".book_info_bottom_right").css("padding", "0 10px");
    $(".book_info_bottom_right").css("bottom", "20px");
    //$(".book_info_bottom_right").css("right", width_page + "px");
    $(".book_info_bottom_right").css("left", "890px");
    $(".book_info_bottom_right").css("width", "65px");
    $(".book_info_bottom_right").css("top", "-80px");

    $(".book_info_bottom_right").css("position", "relative");
    $(".book_info_bottom_right").css("line-height", "normal");
    $(".book_info_bottom_right").css("display", "none");  //2013-06-11 Charles Ma
    $(".book_info_bottom_right").css("text-align", "center");
    $(".book_info_bottom_right").css("border-radius", "10px");
    $(".book_info_bottom_right").css("height", "30px");
    $(".book_info_bottom_right").css("background-color", "rgba(255,255,255,0.5)");
    $(".book_info_bottom_right").css("box-shadow", "0 0 2px #FFF");

    ////////////////////////////////////////////////


    $("#btn_toc").css("display", "none");

    var loop = true;
    function playSound(index) {
        document.getElementById("sound" + String(index)).load();
        document.getElementById("sound" + String(index)).play();
        if(document.getElementById("btnPlay" + String(index))){
            document.getElementById("btnPlay" + String(index)).style.display = "none";
            document.getElementById("btnPause" + String(index)).style.display = "block";
        }
        return;
    }

    function playSound_repeat(index) {
        loop = true;
        document.getElementById("sound" + String(index)).load();
        document.getElementById("sound" + String(index)).play();
        if(document.getElementById("btnPlay" + String(index))){
            document.getElementById("btnPlay" + String(index)).style.display = "none";
            document.getElementById("btnPause" + String(index)).style.display = "block";
        }
        return;
    }

    function playSound_bg(index) {
        if(loop ){
            document.getElementById("sound" + String(index)).load();
            document.getElementById("sound" + String(index)).play();
            //$("#sound"+String(index)).attr("autoplay" , "autoplay");
            if(document.getElementById("btnPlay" + String(index)) && navigator.platform.indexOf('iPad') == -1){
                document.getElementById("btnPlay" + String(index)).style.display = "none";
                document.getElementById("btnPause" + String(index)).style.display = "block";
            }
            return;
        }
    }

    function stopSound_repeat(index) {
        loop = false;
        document.getElementById("sound" + String(index)).pause();

        if(document.getElementById("btnPlay" + String(index))){
            document.getElementById("btnPlay" + String(index)).style.display = "block";
            document.getElementById("btnPause" + String(index)).style.display = "none";
        }
    }

    function stopSound(index) {
        document.getElementById("sound" + String(index)).pause();

        if(document.getElementById("btnPlay" + String(index))){
            document.getElementById("btnPlay" + String(index)).style.display = "block";
            document.getElementById("btnPause" + String(index)).style.display = "none";
        }
    }
    <?php } ?>
    //////////////////////// Charles Ma END
    if (!Array.prototype.indexOf)
    {
        Array.prototype.indexOf = function(elt /*, from*/)
        {
            var len = this.length;

            var from = Number(arguments[1]) || 0;
            from = (from < 0)
                ? Math.ceil(from)
                : Math.floor(from);
            if (from < 0)
                from += len;

            for (; from < len; from++)
            {
                if (from in this &&
                    this[from] === elt)
                    return from;
            }
            return -1;
        };
    }

    function jsGetTruePageNumber(refElementID){
        if(bookdata.pageMode == 2){
            var posx = $('#' + refElementID).offset().left;
            var orgx = $('#book_content_main').offset().left;
            if(bookdata.isImageBook){
                if(posx < orgx + bookdata.pageModeWidth[2]/2){
                    var page = bookdata.currentPage;
                }else{
                    var page = bookdata.currentPage + 1;
                }
            }else{
                var pageno =  $('#' + refElementID).parent().parent().attr("pageno") || $('#' + refElementID).parent().parent().parent().attr("pageno") ||  $('#' + refElementID).parent().parent().parent().parent().attr("pageno") ;
                if(pageno != "" && pageno != undefined){
                    var page = pageno;
                }
                else if(posx < orgx + bookdata.pageModeWidth[2]/2){
                    var page = bookdata.currentPage * 2 - 1;
                }else{
                    var page = bookdata.currentPage * 2;
                }
            }
        }else{
            var page = bookdata.currentPage;
        }
        return page;
    }

    function jsTruePageNumberToPageIndex(pageNum)
    {
        if(bookdata.pageMode == 2){
            if(bookdata.isImageBook){
                var pageIndex = pageNum;
            }else{
                var mod = pageNum % 2;
                var pageIndex = Math.floor( pageNum / 2 ) + (mod == 0? 0:1);
            }
        }else{
            var pageIndex = pageNum;
        }
        return pageIndex;
    }

    function jsPageIndexToPageNumber(pageIndex)
    {
        if(bookdata.pageMode == 2){
            if(bookdata.isImageBook){
                var pageNum = pageIndex;
            }else{
                var pageNum = pageIndex * 2 - 1;
            }
        }else{
            var pageNum = pageIndex;
        }
        return pageNum;
    }
    ///// 2013-2-19 Charles Ma - New eBook format:



    function changeBookFormat(){
        <?php if($ForceToOnePageMode) { ?>

        var page_image = $('.background_inset')[0];

        if(page_image && page_image.src){


            $("#book_content_main").css("margin-top", "-100px");
            $("#book_content_main").css("margin-left", "-25px");
            $(".book_left_top").css("display", "block");
            $(".book_left_bottom").css("display", "block");
            $(".book_right_bottom").css("display", "block");

            $(".page_top").css("display", "none");
            $(".page_content").css("display", "none");
            $(".page_bottom").css("display", "none");

            $(".book_1page_structure").css("height", "652px");

            $('.background_inset').css("display", "none");
            $(".page_image").css("background-image", "url("+page_image.src+")");
            $(".page_image").css("background-color", "white");



            //$("#book_page_style").css("opacity", "0");
        }else{
            $(".book_left_top").css("display", "none");
            $(".book_left_bottom").css("display", "none");
            $(".book_right_bottom").css("display", "none");

            $(".book_1page_structure").css("height", "692px");

            $(".page_top").css("display", "block");
            $(".page_content").css("display", "block");
            $(".page_bottom").css("display", "block");
        }
        <?php  }?>
    }


    ///////
    var load_count = 0; //// For loading problem in One page mdoe


    ////////////////////////////////////////////////
    // Loading the document(page data) of current page
    ////////////////////////////////////////////////
    function jsLoadDocument(i,loadTOC)
    {
        $('#tool_glossary_board').hide(); // 20140919-newfeature2014
        glossary_attach();
        <?php if($ForceToOnePageMode){ ?>
        if(load_count >0){
            $('div#book_content').css("opacity","1"); // 2013-06-10 Charles Ma
            $(".book_info_bottom_right").css("display", "block"); //2013-06-11 Charles Ma
        }
        <?php }?>

        load_count++;
        if(loadTOC == true){
            $('div#book_content').hide();
        }


        if(bookdata.isImageBook){
            jsLoadImageDocument(i, loadTOC);
            return;
        }else if(browsertype == 'MSIE'){
            jsLoadDocumentIE(i, loadTOC);
            return;
        }
        var docpath = '';
        var prevPage = bookdata.currentPage;
        var requestPage = i;

        $(".notes_container").css("display","none");
        $("#note_page_"+requestPage).css({"display":"", "opacity" : 0});
        $("#note_page_"+requestPage).animate({opacity: 1.0}, 'slow');

        if(typeof(bookdata.pages[requestPage]) == 'undefined'){
            return;
        }else if(typeof(bookdata.pages[requestPage].idref)!='undefined'){
            docpath = bookdata.pages[requestPage].href;
            prevdocpath = bookdata.pages[prevPage].href;
            $("div#book_content_main").animate({opacity: 0.0}, 100);
            if(prevdocpath != docpath){ // not same doc, load it
                jsBlockDocument();


                $('div#book_content_main').load(
                    bookdata.requestURL + docpath,
                    {

                    },
                    function(data){
                        $('#tool_glossary_board').hide(); // 20140919-newfeature2014
                        glossary_attach();
                        jsUnblockDocument();
                        var scale = parseInt(parseFloat($('body').css('zoom')) * 100);
                        if(requestPage > prevPage){
                            if((requestPage - prevPage) == 1){ // next page - next doc P.1
                                $("div#book_content_main").animate({left:'0px'},'fast');
                                //$("div#book_content_main").scrollLeft(0);
                            }else{ // jump to next doc page n
                                var diff = bookdata.pages[requestPage].session - 1;
                                var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff <?if(!$cupMode && !$engclassicOnePageMode){?> + (diff) <?}?>;
                                <?php if($cupecOnePageMode){ ?>
                                if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                                    dist = dist -5;
                                }
                                <?php } ?>
                                $("div#book_content_main").css({left:'-' + dist + 'px'});
                                //$("div#book_content_main").scrollLeft(dist);
                            }
                        }else{ // prev page - prev doc last page
                            var diff = bookdata.pages[requestPage].session - 1;

                            var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff <?if(!$cupMode && !$engclassicOnePageMode){?> + (diff) <?}?>;
                            $("div#book_content_main").css({left:'0px'});
                            <?php if($cupecOnePageMode){ ?>
                            if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                                dist = dist -5;
                            }
                            <?php } ?>
                            $("div#book_content_main").animate({left : '-=' + (dist) + 'px'}, "fast");
                            //$("div#book_content_main").scrollLeft(dist);
                        }
                        bookdata.currentPage = requestPage;
                        jsUpdatePageNumber(bookdata.pageMode);

                        if($("#book_content").css("display") == "none"){
                            setTimeout(function(){
                                $("#book_content").css({"display":"",opacity:0});
                                bookdata.loadHightlights(true);
                            },2000);
                        }else{
                            setTimeout(function(){
                                bookdata.loadHightlights();
                            },1000);
                        }

//          $("div#book_content_main").animate({opacity: 1.0}, 'slow');

                        if(loadTOC == true){
                            bookdata.toc_handler(false);
                        }else{
                            $("div#book_content_main").animate({opacity: 1.0}, 1500);
                        }
                        changeBookFormat(); // 2013-2-19 Charles MA

                        $("p").css({opacity: 0});

                        setTimeout(function() {
                            fiilPageNo();
                            $("p").animate({opacity: 1},700);

                            <?php if($cupOnePageMode){ ?>
                            if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                                $(".chapter_title").css({"line-height": "2.5em", "font-size": "24.5px", "margin-bottom":"1.5em", "margin-top": "6.5px"});
                                $("p").css({"margin-right": "0.48em","font-size": "23px","word-spacing": "4px","letter-spacing": "0.45px","line-height": "1.35em","text-indent": "29px","margin-top": "6.5px"});
                                $(".morenext").css({"font-size": "26.5px", "margin-top": ""});
                                $("p img").css({"zoom": "160%"});
                                $(".spacer").css({"margin-top":"0px"});
                            }else{
                                $(".chapter_title").css({"line-height": "2.5em", "font-size": "21.5px", "margin-bottom":"1.5em", "margin-top": "6.5px"});
                                $("p").css({"font-size": "16.5px","word-spacing": "2px","letter-spacing": "0.5px","line-height": "1.35em","text-indent": "29px","margin-top": "6.5px"});
                                $(".morenext").css({"font-size": "23.5px", "margin-top": ""});
                                $(".spacer").css({"margin-top":"0px"});
                            }
                            <?php }else if($engclassicOnePageMode){ ?>
                            if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                                $(".chapterheader").css({ "font-size": "31.2px", "padding-top":"24px"});
                                if("<?=$newPageMode?>"=="2"){
                                    $(".chapterheader").css("margin-bottom","-15px");
                                }
                                $(".chapterHeader .translation").css({"margin-top": "37px", "font-size": "36px"});
                                $(".chapterHeader .count").css({"font-size": "78px", "margin-top": "8px"});
                                $("p").css({ "padding-bottom": "18px","font-size": "20px", "word-spacing":"4px", "letter-spacing": "-0.21px", "margin-top": "6.5px", "line-height": "30.6px", "margin-top": "-10px"});
                                $("p .chp_emptyline").css({"height": "40px"});
                                $("img.leadin_name").css({"width":"286px","float": "left","margin-top": "-10px"});
                                $(".leadinHeader").css({"font-size": "17.4px"});
                                $(".leadin_intro").css({ "line-height": "30px", "margin-top": "6.5px"});
                                $(".divider2").css({"width": "300px"});
                                $(".chapterbox").css({ "margin-bottom": "18px"});
                                $(".title").css({ "font-size": "40px", "padding-top": "50px"});

                                $(".divider1").css({ "width": "400px"});
                                $(".company_logo").css({ width: "130px", "margin-top": "-8px"});
                                $(".morenext").css({ "font-size": "26.9px", "padding-top" : "644.5px"});
                                $(".Chapterbg").css({height: "790px"});$(".notes").css({"word-spacing": "0px", "line-height":"23px"});
                            }else{
                                $(".chapterheader").css({ "font-size": "31.2px", "padding-top":"24px"});
                                if("<?=$newPageMode?>"=="2"){
                                    $(".chapterheader").css("margin-bottom","-15px");
                                }
                                $(".chapterHeader .translation").css({"margin-top": "37px", "font-size": "36px"});
                                $(".chapterHeader .count").css({"font-size": "78px", "margin-top": "8px"});
                                $("p").css({ "padding-bottom": "18px","font-size": "19.4px", "word-spacing":"4px", "letter-spacing": "-0.21px", "margin-top": "6.5px", "line-height": "30.6px", "margin-top": "-10px"});
                                $("p .chp_emptyline").css({"height": "40px"});
                                $("img.leadin_name").css({"width":"286px","float": "left","margin-top": "-10px"});
                                $(".leadinHeader").css({"font-size": "17.4px"});
                                $(".leadin_intro").css({ "line-height": "30px", "margin-top": "6.5px"});
                                $(".divider2").css({"width": "300px"});
                                $(".chapterbox").css({ "margin-bottom": "18px"});
                                $(".title").css({ "font-size": "34px", "padding-top": "82px"});
                                $(".divider1").css({ "width": "400px"});
                                $(".company_logo").css({ width: "107px", "margin-top": "-8px"});
                                $(".morenext").css({ "font-size": "26.9px", "padding-top" : "644.5px"});
                                $(".Chapterbg").css({height: "790px"});$(".notes").css({"word-spacing": "0px", "line-height":"23px"});
                            }
                            <?php }else if(!($engclassicMode && $engclassicOnePageMode)){ ?>
                            if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
//		         		$("p").css({"font-size": "14.95px"});
//		         		$(".morenext").css({"font-size": "24px"});

                            }
                            <?php } ?>
                            $(".spacer").removeAttr("style");
                        }, 1500);  // 2013-2-19 Charles MA

//     	 $(".morenext").css({"margin-top": "110%"});
                    }
                );
            }else{ // go to sub page
                //var diff = Math.abs(requestPage - prevPage);
                var diff = bookdata.pages[requestPage].session - 1;       //2013-3-6 Charles Ma
                var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff;
                //console.log(-(diff)*bookdata.pageModeWidth[bookdata.pageMode]);
                var scale = parseInt(parseFloat($('body').css('zoom')) * 100);

                if(requestPage > prevPage){

                    var new_px = -(diff)*bookdata.pageModeWidth[bookdata.pageMode] <?if(!$cupMode && !$engclassicOnePageMode){?> - (diff) <?}?>;
//        console.log('SUB > new_px: '+new_px);
                    if(<?=$cupecOnePageMode ? 1: 0?>){
                        if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                            new_px = new_px +5;
                        }
                    }
                    $("div#book_content_main").animate({left : new_px + 'px'}, "slow");
                }else{

                    var new_px = -(diff)*bookdata.pageModeWidth[bookdata.pageMode] <?if(!$cupMode && !$engclassicOnePageMode){?> - (diff) <?}?>;
                    if(<?=$cupecOnePageMode ? 1: 0?>){
                        if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                            new_px = new_px +5;
                        }
                    }
                    $("div#book_content_main").animate({left : new_px + 'px'}, "slow");
                }

                $("div#book_content_main").animate({opacity: 1.0}, 'slow');

                bookdata.currentPage = requestPage;

                jsUpdatePageNumber(bookdata.pageMode);
                <?php if($cupOnePageMode){ ?>
                if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                    $(".chapter_title").css({"line-height": "2.5em", "font-size": "24.5px", "margin-bottom":"1.5em", "margin-top": "6.5px"});
                    $("p").css({"margin-right": "0.48em","font-size": "23px","word-spacing": "4px","letter-spacing": "0.45px","line-height": "1.35em","text-indent": "29px","margin-top": "6.5px"});
                    $(".morenext").css({"font-size": "26.5px", "margin-top": ""});
                    $("p img").css({"zoom": "160%"});
                    $(".spacer").css({"margin-top":"0px"});
                }else{
                    $(".chapter_title").css({"line-height": "2.5em", "font-size": "21.5px", "margin-bottom":"1.5em", "margin-top": "6.5px"});
                    $("p").css({"font-size": "16.5px","word-spacing": "2px","letter-spacing": "0.5px","line-height": "1.35em","text-indent": "29px","margin-top": "6.5px"});
                    $(".morenext").css({"font-size": "23.5px", "margin-top": ""});
                    $(".spacer").css({"margin-top":"0px"});
                }
                <?php }else if($engclassicOnePageMode){ ?>
                if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
                    $(".chapterheader").css({ "font-size": "31.2px", "padding-top":"24px"});
                    if("<?=$newPageMode?>"=="2"){
                        $(".chapterheader").css("margin-bottom","-15px");
                    }
                    $(".chapterHeader .translation").css({"margin-top": "37px", "font-size": "36px"});
                    $(".chapterHeader .count").css({"font-size": "78px", "margin-top": "8px"});
                    $("p").css({ "padding-bottom": "18px","font-size": "20px", "word-spacing":"4px", "letter-spacing": "-0.21px", "margin-top": "6.5px", "line-height": "30.6px", "margin-top": "-10px"});
                    $("p .chp_emptyline").css({"height": "40px"});
                    $("img.leadin_name").css({"width":"286px","float": "left","margin-top": "-10px"});
                    $(".leadinHeader").css({"font-size": "17.4px"});
                    $(".leadin_intro").css({ "line-height": "30px", "margin-top": "6.5px"});
                    $(".divider2").css({"width": "300px"});
                    $(".chapterbox").css({ "margin-bottom": "18px"});
                    $(".title").css({ "font-size": "40px", "padding-top": "50px"});

                    $(".divider1").css({ "width": "400px"});
                    $(".company_logo").css({ width: "130px", "margin-top": "-8px"});
                    $(".morenext").css({ "font-size": "26.9px", "padding-top" : "644.5px"});
                    $(".Chapterbg").css({height: "790px"});$(".notes").css({"word-spacing": "0px", "line-height":"23px"});

                }else{
                    $(".chapterheader").css({ "font-size": "31.2px", "padding-top":"24px"});
                    if("<?=$newPageMode?>"=="2"){
                        $(".chapterheader").css("margin-bottom","-15px");
                    }
                    $(".chapterHeader .translation").css({"margin-top": "37px", "font-size": "36px"});
                    $(".chapterHeader .count").css({"font-size": "78px", "margin-top": "8px"});
                    $("p").css({ "padding-bottom": "18px","font-size": "19.4px", "word-spacing":"3.5px", "letter-spacing": "-0.21px", "margin-top": "6.5px", "line-height": "30.6px", "margin-top": "-10px"});
                    $("p .chp_emptyline").css({"height": "40px"});
                    $("img.leadin_name").css({"width":"286px","float": "left","margin-top": "-10px"});
                    $(".leadinHeader").css({"font-size": "17.4px"});
                    $(".leadin_intro").css({ "line-height": "30px", "margin-top": "6.5px"});
                    $(".divider2").css({"width": "300px"});
                    $(".chapterbox").css({ "margin-bottom": "18px"});
                    $(".title").css({ "font-size": "34px", "padding-top": "82px"});
                    $(".divider1").css({ "width": "400px"});
                    $(".company_logo").css({ width: "107px", "margin-top": "-8px"});
                    $(".morenext").css({ "font-size": "26.9px", "padding-top" : "644.5px"});
                    $(".Chapterbg").css({height: "790px"});$(".notes").css({"word-spacing": "0px", "line-height":"23px"});
                }
                <?php }?>

                $(".spacer").removeAttr("style");
            }
        }

        //2013-03-27 END
        <?php if($cupecMode==1){?>
        passageZoom();
        <?php  } ?>
    }

    function passageZoom(){	//20140326-passagezoom

        var scale = parseFloat($('body').css('zoom')) * 100;

        <?php if(!$cupecOnePageMode){?>
        if("<?=$platform == 'Andriod'?>" == 1 || navigator.platform.indexOf("iPad") != -1){
        }else{
            scale = (scale-100)*0.25 + 100;
        }
        <?php  } ?>
        <?php if($Publisher=="Cambridge University Press"){?>
        $('.book_content_main p').css('zoom', (scale-1) + '%');
        scale= scale / 100;
        $('.book_content_main p').css('-moz-transform','scale('+(scale-1)+')');
        <?php }else if(!$engclassicOnePageMode){?>
        $('.book_content_main .morenext').css('font-size','');
        <?php  } ?>
    }

    function fiilPageNo(){
        <?php if($engclassicMode){?>
        var glossary_list = [];
        $('.glossary').each(function(i,elem){
            var pageno = $(this).parent().parent().attr("pageno") > 0 ? $(this).parent().parent().attr("pageno") : $(this).parent().parent().parent().attr("pageno");
            glossary_list[$(this).attr("value")] =  pageno;

        });

        $('.glossary_data').each(function(i,elem){
            $(this).attr("page",glossary_list[$(this).attr("value")]);
        });
        <?php  } ?>
    }

    function jsLoadPDFDocument(i, loadTOC)
    {
        var docpath = '';
        var prevPage = bookdata.currentPage;
        var requestPage = i;
        if(typeof(bookdata.pages[requestPage]) == 'undefined'){
            return;
        }else if(typeof(bookdata.pages[requestPage].idref)!='undefined'){
            docpath = bookdata.pages[requestPage].href;
            prevdocpath = bookdata.pages[prevPage].href;
            $("div#book_content_main").animate({opacity: 0.0}, 100);
            if(prevdocpath != docpath){ // not same doc, load it
                jsBlockDocument();
                $('div#book_content_main').load(
                    bookdata.requestURL + docpath,
                    {
                    },
                    function(data){
                        jsUnblockDocument();

                        if(requestPage > prevPage){
                            if((requestPage - prevPage) == 1){ // next page - next doc P.1
                                $("div#book_content_main").animate({left:'0px'},'fast');
                            }else{ // jump to next doc page n
                                var diff = bookdata.pages[requestPage].session - 1;
                                var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff;
                                $("div#book_content_main").css({left:'-' + dist + 'px'});
                            }
                        }else{ // prev page - prev doc last page
                            var diff = bookdata.pages[requestPage].session - 1;
                            var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff;
                            $("div#book_content_main").css({left:'0px'});
                            $("div#book_content_main").animate({left : '-=' + (dist) + 'px'}, "fast");
                        }
                        bookdata.currentPage = requestPage;
                        jsUpdatePageNumber(bookdata.pageMode);
                        bookdata.loadHightlights();

                        $("div#book_content_main").animate({opacity: 1.0}, 'slow');

                        if(loadTOC == true){
                            bookdata.toc_handler(false);
                        }
                        $("p").css({opacity: 0});

                        var background_img = $(".ebook_pdf_bg")[0];

                        $('.page_left > .page_image').css('background-image','url(\''+$(background_img).attr('src')+'\')');
                        $(".ebook_pdf_bg").css("display","none");


                        $(".ebook_pdf").css("-webkit-transform", "scale(0.56)translate(-50%,-50%) ");

                        setTimeout(function() {
                            $("p").animate({opacity: 1},700);
                        }, 1000);  // 2013-2-19 Charles MA
                    }
                );
            }else{ // go to sub page
                var diff = bookdata.pages[requestPage].session - 1;       //2013-3-6 Charles Ma
                var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff;

                if(requestPage > prevPage){
                    var new_px = -(diff)*bookdata.pageModeWidth[bookdata.pageMode];
                    $("div#book_content_main").animate({left : new_px + 'px'}, "slow");

                }else{
                    var new_px = -(diff)*bookdata.pageModeWidth[bookdata.pageMode];
                    $("div#book_content_main").animate({left : new_px + 'px'}, "slow");

                }

                $("div#book_content_main").animate({opacity: 1.0}, 'slow');

                bookdata.currentPage = requestPage;
                jsUpdatePageNumber(bookdata.pageMode);
            }
        }
    }

    ////////////////////////////////////////////////
    // Implement the document(page data) of [IMAGE BOOK] of current page
    ////////////////////////////////////////////////
    function jsLoadImageDocument(i, loadTOC)
    {
        if(loadTOC == true){
            $('#book_comic').hide();
        }
        if(bookdata.pageMode == 2){
            var docpath = [];
            var currentPage = bookdata.currentPage;
            var requestPage = i;
            var total_page = Math.ceil(bookdata.pages.length / 2);
            if(requestPage % 2 == 0){
                var right_page = requestPage;
                var left_page = requestPage - 1;
            }else{
                var left_page = requestPage;
                var right_page = requestPage + 1;
            }
            if(currentPage == left_page){
                //  return;
            }

            if(typeof(bookdata.pages[left_page]) != 'undefined'){
                docpath.push(bookdata.pages[left_page].href);
            }
            if(typeof(bookdata.pages[right_page]) != 'undefined'){
                docpath.push(bookdata.pages[right_page].href);
            }
            //2013-10-17 Charles Ma
            if(bookdata.pages[left_page] && bookdata.pages[left_page].extra == 1)
                var extra =  1;
            else if (bookdata.pages[right_page] && bookdata.pages[right_page].extra == 1)
                var extra =  2;
            else var extra =  0;

            if(docpath.length == 0){
                return;
            }else{
                $("#book_comic").animate({opacity: 0.0}, 'fast');
                //Block_Document();
                jsBlockDocument();
                $.post(
                    '/home/eLearning/ebook_reader/ajax_load.php',
                    {
                        "task":"Get_HTML_Image_Srcs",
                        "BookID":bookdata.id,
                        "RequestPaths[]":docpath,
                        "extra" : extra   //2013-10-17 Charles Ma
                    },
                    function(data){
                        eval("var src_paths = " + decodeURIComponent(data) + ";");
                        if(src_paths.length > 0){
                            <?php if($userBrowser->browsertype == 'MSIE' || ($userBrowser->browsertype == 'Firefox' && $version[0] <= 8)){ ?>
                            $('<?=$isRTL && ($bid != 1664 && $bid != 1198) ? "#book_comic_image_right" : "#book_comic_image_left"?>').attr('src',src_paths[0]);
                            if(src_paths.length > 1){
                                $('<?=$isRTL && ($bid != 1664 && $bid != 1198)  ? "#book_comic_image_left" : "#book_comic_image_right"?>').attr('src',src_paths[1]);
                            }else{
                                $('<?=$isRTL && ($bid != 1664 && $bid != 1198) ? "#book_comic_image_left" : "#book_comic_image_right"?>').attr('src','');
                            }
                            <?php }else{ ?>
                            $('<?=$isRTL && ($bid != 1664 && $bid != 1198) ? "#book_comic_image_right" : "#book_comic_image_left"?>').css('background-image','url(\''+src_paths[0]+'\')');
                            if(src_paths.length > 1){
                                $('<?=$isRTL && ($bid != 1664 && $bid != 1198) ? "#book_comic_image_left" : "#book_comic_image_right"?>').css('background-image','url(\''+src_paths[1]+'\')');
                            }else{
                                $('<?=$isRTL && ($bid != 1664 && $bid != 1198) ? "#book_comic_image_left" : "#book_comic_image_right"?>').css('background-image','url(\'\')');
                            }

                            $("#book_comic_image_right_canvas").remove();
                            $("#book_comic_image_right .main_content").after('<canvas class="book_comic_image_canvas" id="book_comic_image_right_canvas" height="'+parseInt($("#book_comic_image_right").css("height"))+'" width="'+parseInt($("#book_comic_image_right").css("width"))+'" style="position: absolute;"></canvas>');
                            $("#book_comic_image_right_canvas").css("margin-top", "-"+parseInt($("#book_comic_image_right").css("height")) +"px");


                            $("#book_comic_image_left_canvas").remove();
                            $("#book_comic_image_left .main_content").after('<canvas class="book_comic_image_canvas" id="book_comic_image_left_canvas" height="'+parseInt($("#book_comic_image_left").css("height"))+'" width="'+parseInt($("#book_comic_image_left").css("width"))+'" style="position: absolute;"></canvas>');
                            $("#book_comic_image_left_canvas").css("margin-top", "-"+parseInt($("#book_comic_image_left").css("height")) +"px");

                            $(".main_content").css({"position":"relative", "z-index": 5});

                            $("#book_comic_image_left .main_content").unbind("mousemove touchend").bind("mousedown touchstart", function(){
//            	setTimeout(function(){
                                $("#book_comic_image_left .main_content").css({"position":"initial", "z-index": 3});
                                $("#book_comic_image_left .book_comic_image_canvas").css({"position":"absolute", "z-index": 4});
                                $("#book_comic_image_left .book_comic_image_canvas").trigger( "mousedown" );
//				}, 500);
                            });



                            $("#book_comic_image_right .main_content").unbind("mousemove touchend").bind("mousedown touchstart", function(){
//            	setTimeout(function(){
                                $("#book_comic_image_right .main_content").css({"position":"initial", "z-index": 3});
                                $("#book_comic_image_right .book_comic_image_canvas").css({"position":"absolute", "z-index": 4});
                                $("#book_comic_image_right .book_comic_image_canvas").trigger( "mousedown" );
//				}, 500);
                            });

//            initate_drawing_page("left");
//            initate_drawing_page("right");
                            <?php } ?>
                            bookdata.currentPage = left_page;
                            jsUpdatePageNumber(bookdata.pageMode);
                        }
                        $("#book_comic").animate({opacity: 1.0}, 'slow');
                        //UnBlock_Document();
                        jsUnblockDocument();
                        if(loadTOC == true){
                            bookdata.toc_handler(false);
                        }
                    }
                );
                //2013-06-24 Charles Ma
                $('div#book_content_main').replaceWith('<div id="book_content_main"><div style="display:none" id="book_content_left_page" name="left_page"></div><div style="display:none"  id="book_content_right_page" name="right_page"></div></div>');

                var page_id_array = ["book_content_left_page","book_content_right_page"];

                for(var index = 0; index < docpath.length; index++){
                    $('div#'+page_id_array[index]).load(
                        bookdata.requestURL + docpath[index],
                        {

                        },
                        function(data){

                        }
                    );
                };

            }
        }else{
            var docpath = [];
            var currentPage = bookdata.currentPage;
            var requestPage = i;
            var total_page = bookdata.pages.length;

            if(typeof(bookdata.pages[requestPage]) != 'undefined'){
                docpath.push(bookdata.pages[requestPage].href);
            }
            if(docpath.length == 0){
                return;
            }else{
                $("#book_comic").animate({opacity: 0.0}, 'fast');
                jsBlockDocument();
                $.post(
                    '/home/eLearning/ebook_reader/ajax_load.php',
                    {
                        "task":"Get_HTML_Image_Srcs",
                        "BookID":bookdata.id,
                        "RequestPaths[]":docpath
                    },
                    function(data){
                        eval("var src_paths = " + decodeURIComponent(data) + ";");
                        if(src_paths.length > 0){
                            <?php if($userBrowser->browsertype == 'MSIE' || ($userBrowser->browsertype == 'Firefox' && $version[0] <= 8)){ ?>
                            $('#book_page_style').attr('src',src_paths[0]);
                            <?php }else{ ?>
                            //$('#book_page_style').css('background-image','url(\''+src_paths[0]+'\')');
                            $('#book_page_style').attr('src',src_paths[0]);
                            <?php } ?>
                            bookdata.currentPage = requestPage;
                            jsUpdatePageNumber(bookdata.pageMode);
                        }
                        $("#book_comic").animate({opacity: 1.0}, 'slow');
                        jsUnblockDocument();
                        if(loadTOC == true){
                            bookdata.toc_handler(false);
                        }
                    }
                );

                $('div#book_content_main').replaceWith('<div id="book_content_main"><div style="display:none" id="book_content_left_page" name="left_page"></div></div>');

                var page_id_array = ["book_content_left_page"];

                for(var index = 0; index < docpath.length; index++){console.log(index);
                    $('div#'+page_id_array[index]).load(
                        bookdata.requestURL + docpath[index],
                        {

                        },
                        function(data){

                        }
                    );
                };
            }
        }
    }

    function jsLoadDocumentIE(i, loadTOC)
    {
        var docpath = '';
        var prevPage = bookdata.currentPage;
        var requestPage = i;
        if(typeof(bookdata.pages[requestPage]) == 'undefined'){
            return;
        }else if(typeof(bookdata.pages[requestPage].idref)!='undefined'){
            docpath = bookdata.pages[requestPage].href;
            prevdocpath = bookdata.pages[prevPage].href;
            $("div#book_content_main").animate({opacity: 0.0}, 100);

            jsBlockDocument();
            $('div#book_content_main').load(
                bookdata.requestURL + docpath,
                {

                },
                function(data){
                    jsUnblockDocument();

                    bookdata.currentPage = requestPage;
                    $('div#book_content_main').scrollTop(0);
                    jsUpdatePageNumber(bookdata.pageMode);

                    $("div#book_content_main").animate({opacity: 1.0}, 'slow');

                    if(loadTOC == true){
                        bookdata.toc_handler(false);
                    }
                }
            );
        }
    }

    function jsLoadTOC()
    {
        //Block_Document();
        jsBlockDocument();
        $.post(
            '/home/eLearning/ebook_reader/ajax_load.php',
            {
                "task":"Get_Table_of_Content_Layer",
                "BookID":bookdata['id'],
                "PageMode":bookdata['pageMode'],
                "ImageBook":(bookdata['isImageBook']?'1':''),
                "ImageWidth":bookdata['imageWidth']
            },
            function(data){
                $('#book_content').after(data);

                <?php if($ForceToOnePageMode){ ?>
                $('.btn_add_bookmarkes').css('display','none');
                $('#book_table_of_content').css({
                                                    left: '14%'         //2013-06-20
                                                });
                <?php }else{ ?>
                if(bookdata['pageMode'] == 1){
                    $('#book_table_of_content').css({
                                                        left: '23%'         //2013-06-20
                                                    });
                }else{
                    $('#book_table_of_content').css({
                                                        left: '13%'         //2013-06-20
                                                    });
                }
                <?php }?>

                $('#book_table_of_content').css({
                                                    left: $('#book_structure').offset().left,
                                                    top: $('#book_structure').offset().top
                                                });

                var cover_image_dv = $('#book_cover_image');
                if(cover_image_dv.length > 0){
                    $('#book_structure').hide();
                    $('#book_table_of_content').hide();

                    //2013-08-08 Charles Ma

                    if($('#book_cover')[0]){
                        var client_height = parseInt($('#book_cover')[0].clientHeight);
                        // 20131217-ebook-oversize
                        $("#btn_prev_page").css("opacity", "0");  // 20131204-ebook-righttoleft
                        $("#btn_next_page").css("opacity", "0");  // 20131204-ebook-righttoleft
                        <?php if(!$ForceToOnePageMode){ ?>
                        $('#book_cover')[0].onload = function(){
                            var natural_width = parseInt($(this)[0]['naturalWidth']);
                            var natural_height = parseInt($(this)[0]['naturalHeight']);
                            var client_width = (natural_width * client_height) / natural_height;
                            $('#book_cover_page_pattern').css({"width" : client_width});
                            $('#book_cover_image').css({"width" : client_width});
                            if( (client_width * 2 * initial_scale / 100)/0.9 > parseInt($(window).width())){
                                var scale = initial_scale * 0.9 * parseInt($(window).width()) / (client_width * 2 * initial_scale / 100);

                                excess = scale % 10;
                                if(bookdata['pageMode'] == 1){
                                    scale = scale - excess;
                                }else{
                                    scale = scale - excess;
                                }
                                if ($('body').css('zoom')*100 < scale){
                                    $('body').css('zoom', scale + '%');
                                    $('body').css('width', parseInt($('body').css('width'))-0.1 );
                                }else{
                                    scale = $('body').css('zoom');
                                }
                                initial_scale = scale;
                                scale= scale / 100;
                                $('body').css('-moz-transform','scale('+scale+')');
                                $('body').css('-moz-transform-origin','top left');
                            }
                        };
                        <?php } ?>
                    }

                    //2013-08-08 END


                    var cover_handler = function(e){

                        //////////////////////// Charles Ma 2013-3-15 ebook_demo
                        var page_demo = $('.eclass_demo')[0];
                        if(page_demo){
                            return false;
                        }

                        setTimeout(function(){
                            cover_image_dv.fadeOut(300, function(){   // 2013-10-03 Charles Ma  -eBook-update-101003

                                $('#btn_top_tool_bar').mouseup();
                                setTimeout(function() {
                                    var dv_top = $('#tool_top');
                                    if(dv_top.is(':visible')){
                                        $('#btn_top_tool_bar').mouseup();
                                    }
                                }, 3500);

                                <?php if($ForceToOnePageMode){ ?>
                                $("#btn_prev_page").css("opacity", "1");
                                $("#btn_next_page").css("opacity", "1");
                                $('#book_page_style > .page_top').css("display", "none"); // 2013-06-05 Charles Ma
                                $('#book_page_style > .page_content').css("display", "none"); //2013-06-05 Charles Ma
                                $('#book_page_style > .page_bottom').css("display", "none");  //2013-06-05 Charles Ma
                                <?php }else{ ?>
                                cover_image_dv.remove(); //2013-06-05 Charles Ma
                                <?php } ?>
                                $('#ebook_transparent_block').remove();
                                <?php if($ForceToOnePageMode){ ?>  //2013-06-06 Charles Ma
                                $(".book_content_container").css("opacity", "100");
                                $('#book_structure').fadeIn('fast',function(){
                                    $("#book_structure").css("opacity", "100");
                                });
                                $('#book_table_of_content').fadeIn('fast',function(){
                                    bookdata.toc_handler(true);
                                });

                                $("#book_content").css("opacity", "100");$("#book_structure").css("opacity", "100");
                                <?php }else{ ?>
                                $('#book_structure').fadeIn('slow');
                                $('#book_table_of_content').fadeIn('slow',function(){
                                    bookdata.toc_handler(true);
                                });
                                <?php } ?>

                                <?php if($isRTL == 1){ ?>
                                $("#btn_next_page").css("opacity", 0);
                                $("#btn_prev_page").css("opacity", 1);
                                <?php }else{ ?>
                                $("#btn_prev_page").css("opacity", 0);
                                $("#btn_next_page").css("opacity", 1);
                                <?php } ?>

                                bookdata.toc_handler(true);
                            });
                        },700);

                    }
                    cover_image_dv.mouseup(cover_handler);
                    $('#ebook_transparent_block').mouseup(cover_handler);
                }
                bookdata.cover_image_handler();

                if(bookdata.isImageBook){
                    $('#book_table_of_content .book_content_container').css('width',$('#book_structure').width());
                }

                $('#book_content').hide();
                $('#book_comic').hide();
                $('#bookmark_img_left').hide();
                if(bookdata.pageMode==2) $('#bookmark_img_right').hide();

                <?php if($isRTL == 1){ ?>   //20131204-ebook-righttoleft
                $('#btn_prev_page').css('z-index',5);
                $('#btn_next_page').css('opacity',0);
                <?php }else{ ?>
                $('#btn_next_page').css('z-index',5);
                $('#btn_prev_page').css('opacity',0);
                <?php } ?>

                jsUnblockDocument();

                <?php if($ForceToOnePageMode){ ?>     //2013-1-25 CharlesMa
                $(".book_info_detail").css("width", "1040px");
                $(".book_info_detail").css("height", "630px");

                $(".book_info_detail").css("padding-left", "52px");

                $("#book_table_of_content_container").css("width", "960px");
                $("#book_table_of_content_container").css("height", "630px");

                // 2013-03-25 eBook-update-130325b - modification of book cover

                $("#book_cover_image").css("width", "1000px");
                $("#book_cover_image").css("height", "500px");
                $("#book_cover_image").css("margin-top", "-6px");

                $("#book_page_style").css("width", "100%");
                $("#book_page_style").css("height", "100%");


                if(navigator.platform.indexOf("iPad") != -1 ||navigator.platform.indexOf("iPhone") != -1){
                    $(".page_pattern").css("height", "141%");
                    $(".page_pattern").css("top", "-5px");
                }else{
                    $(".page_pattern").css("height", "111%");
                    $(".page_pattern").css("left", "-25px");
                }

                $(".page_pattern").css("width", "110%");

                // 2013-05-29 eBook-update-130529a
                $(".page_pattern").css("z-index", "9");
                $(".page_pattern > img").css("background", "none");
                $(".page_pattern > img").css("box-shadow", "0 5px 8px #343434");
                $(".page_pattern > img").css("border-radius", "0");
                // 2013-05-29 eBook-update-130529a


                // 2013-03-25 eBook-update-130325b - END

                $(".page_content").css("max-height", "100%");
                $(".page_content").css("min-height", "80%");

                $(".page_content_right").css("max-height", "620px");
                $(".page_content_right").css("min-height", "620px");

                $(".page_content_bg").css("max-height", "620px");
                $(".page_content_bg").css("min-height", "620px");


                $(".book_toc_list_page_detail").css("margin-top", "-50px");
                $(".book_toc_list_page_detail").css("margin-left", "50px");

                // 2013-10-22 Charles Ma
                <?php if($platform=='iPad'){ ?>
                $('#book_cover').attr({"height":"80.8%"});
                $('#book_cover').attr({"height":"95.8%"});
                $('#book_cover').attr({"width" : "102%"});
                <?php }elseif($platform=='Andriod'){ ?>
                $('#book_cover').attr({"height":"125%"});
                $('#book_cover').attr({"width" : "95%"});
                $('#book_cover_page_pattern').css({"left" : "0px"});
                <?php }else{ ?>
                $('#book_cover').attr({"width" : "96.8%"});
                $('#book_cover').attr({"height":"94.8%"});
                <?php } ?>
                $('.page_content').css("background-image", "url('')");
                $('.page_content_right').css("background-image", "url('')");
                $('.page_top').css("background-image", "url('')");
                $('.page_bottom').css("background-image", "url('')");
                $('.page_bottom_right').css("background-image", "url('')");
                $('.page_bottom_bg').css("background-image", "url('')");

                <?php } ?>

                <?php if($action != ''){ ?>
                var cover_image_dv = $('#book_cover_image');
                cover_image_dv.fadeOut(200);
                cover_image_dv.remove();

                $('#book_table_of_content').fadeIn('slow');
                <?php if($_SESSION['eBookReaderPageMode'] == 1){ ?>
                jsToPageFromNotes(<?=$mynote_p1?>);
                <?php }else{ ?>
                jsToPageFromNotes(<?=$mynote_p2?>);
                <?php } ?>
                $('#book_structure').fadeIn('fast', function(){$('#book_structure').css("opacity", 1);});
                $('#ebook_transparent_block').remove();
                setTimeout(function() {
                    $('#btn_top_tool_bar').trigger("mouseup");
                    $('#btn_book_tool').trigger("mouseup");
                    $('#btn_my_notes').trigger("mouseup");
                }, 1000);
                <?php } ?>

                hideAudioBtn();

            }
        );
    }

    function disableDefaultKeywordSearch(){
        $(window).keydown(function(e) { //2013-3-6 Charles MA
            if (e.keyCode == 70 && e.ctrlKey || (e.metaKey && e.keyCode == 70)) {
                if($("#tool_bottom").css("display") == "none")
                    $("#btn_bottom_tool_bar").mouseup();
                setTimeout(function() {
                    $("#btn_search").mouseup();
                }, 100);

                return false;
            }
        });

        $(".book_content_container").bind("click touch",function(e) {   	//window
            var isiPad = navigator.userAgent.match(/iPad/i) != null;
            //if(!isiPad){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;
            //}
        });
    }

    function jsLoadTextToSpeech(txt, speed, voice)
    {
        var speed = speed || 2;
        var voice = voice || 1;
        var container = $('#tool_page_control_container');
        if(typeof(bookdata.loadingImage)=='undefined'){
            bookdata.loadingImage = container.html(); // save loading image
        }else{
            container.html(bookdata.loadingImage);
        }
        if(typeof(bookdata.ttsTooltipFunc) != 'undefined'){
            clearTimeout(bookdata.ttsTooltipFunc);
        }

        $('#tool_voice').addClass('istts');
        $('#tool_voice').css('width', '90px');
        $('#tool_voice').show();
        txt = txt.replace(/(<([^>]+)>)/ig,"");
        txt = txt.replace(/&nbsp;/ig,"");
        if(txt != ''){
            var img = $('#tool_page_control_container img');
            if(img.length > 0){
                img.css({width: container.width(), height: container.height()});
            }
            $.post(
                '/includes/texttospeech/text_to_speech.php?inputText='+encodeURIComponent(txt)+'&speed='+ speed +'&voice=' + voice,
                {

                },
                function(data){
                    var parts = data.split('|=|');
                    if(parts[0] == '1'){
                        container.load(
                            '/home/eLearning/ebook_reader/ajax_load.php',
                            {
                                'task':'Get_Audio_Control',
                                'src':encodeURIComponent(parts[1]),
                                'width':container.width(),
                                'height':container.height()
                            },
                            function(returnData){
                                $('#audio_control').attr('controlsList','nodownload');
                                <?php if($platform=='iPad'){ ?>
                                document.getElementById('audio_control').play();
                                <?php } ?>
                                delete bookdata.selectionBuffer;
                            }
                        );
                    }else{
                        $('#tool_voice').hide();
                        $('#tool_voice').removeClass('istts');
                        container.html(bookdata.loadingImage);
                    }
                }
            );
        }else{
            container.html('<font style="color:#FFFFFF">' + jsLang["PleaseSelectText"] + '</font>');
            bookdata.ttsTooltipFunc = setTimeout(function(){
                $('#tool_voice').slideUp('slow');
            },1000 * 3);
        }
    }
    var isStartPage = true; //Charles Ma 2013-06-06
    function jsUpdatePageNumber(page_mode)
    {
        if(page_mode == 2){
            if(bookdata.isImageBook){
                var total_page = (bookdata.pages.length - 1);
                var left_page = bookdata.currentPage;
                var right_page = Math.min(bookdata.currentPage+1, total_page);
                <?php if($isRTL==1){ ?>
                $('#book_right_page_number').html(left_page + ' / ' + total_page);
                $('#book_left_page_number').html(right_page + ' / ' + total_page);
                <?php }else{ ?>
                $('#book_left_page_number').html(left_page + ' / ' + total_page);
                $('#book_right_page_number').html(right_page + ' / ' + total_page);
                <?php } ?>

                $('#tool_page_num_span').html(bookdata.currentPage);
            }else{
                var total_page = (bookdata.pages.length - 1) * 2;
                var left_page = (bookdata.currentPage * 2 - 1);
                var right_page = Math.min((bookdata.currentPage * 2), total_page);
                $('#book_left_page_number').html(left_page + ' / ' + total_page);
                $('#book_right_page_number').html(right_page + ' / ' + total_page);
                $('#tool_page_num_span').html(bookdata.currentPage * 2 - 1);
            }
            bookdata.bookmark_image_toggle_handler('bookmark_img_left',left_page);
            bookdata.bookmark_image_toggle_handler('bookmark_img_right',right_page);
        }else if(page_mode == 1){

            $("p").css("font-size", "18px"); // 20140422-cupbook
            $(".page").css("height", "800px"); // 20140422-cupbook
            <?php if($youngreaderMode==1){ ?>
            $(".page").css("height", "765px");
            $(".page").css("padding-left", "50px");
            $(".book_content_container").css("width","651px");
            <?php } ?>
            $('.page p').css("font-size", "18.1px");
            $('.page p,.page h2').css("word-spacing", "3.8px");
            $('.page p,.page h2').css("letter-spacing", "-0.4px");
            $('.page p').css("line-height", "1.35em");
            <?php if($youngreaderMode==1){ ?>
            $('.page p').css("line-height", "3.35em");
            <?php } ?>

            $('.page h2').css("font-size", "25px");


            var total_page = (bookdata.pages.length - 1);
            $('#book_right_page_number').html((bookdata.currentPage) + ' / ' + total_page);
            bookdata.bookmark_image_toggle_handler('bookmark_img_left',bookdata.currentPage);
            $('#tool_page_num_span').html(bookdata.currentPage);

            //Charles Ma 2013-2-19
            <?php if($ForceToOnePageMode || $engclassicOnePageMode){ ?>
            $('#book_right_page_number').html((bookdata.currentPage) + '<span>' + ' / ' + total_page + '</span>');
            if(bookdata.currentPage == 1)
                $("#btn_prev_page").css("display", "");//eBook-update-130605a
            else if (bookdata.currentPage == (bookdata.pages.length - 1))
                $("#btn_next_page").css("display", "none");
            else{
                $("#btn_prev_page").css("display", "").css("opacity", "1");
                $("#btn_next_page").css("display", "").css("opacity", "1");
            }if(isStartPage){             //Charles Ma 2013-06-06
                $("#btn_prev_page").css("opacity", "0");
                $("#btn_next_page").css("opacity", "0");
                isStartPage = false;
            }
            <?php } ?>
        }else{
            if($('#book_left_page_number').length > 0) $('#book_left_page_number').html('');
            if($('#book_right_page_number').length > 0) $('#book_right_page_number').html('');
        }
        if($('#tool_page_select').slider){
            if("<?=$isRTL?>" == "1"){
                var temp_max =  bookdata.pages.length;
                var temp_neg = -1;
            }else{
                var temp_max =  0;
                var temp_neg = 1;
            }
            $('#tool_page_select').slider("value",temp_max + temp_neg * bookdata.currentPage);
            bookdata.tool_page_number_handler(true);
        }
        $('#tool_highlight_board').hide();
        if($('#tool_voice').is(':visible')){
            $('#tool_page_control_container').html('');
            $('#tool_voice').hide();
            $('#tool_voice').removeClass('istts');
        }
        <?php if($isRTL==1){ ?>
        //20131204-ebook-righttoleft
        $('#btn_prev_page').css('z-index',2);
        $('#btn_next_page').css('opacity',1);
        <?php } else { ?>
        $('#btn_next_page').css('z-index',2);
        $('#btn_prev_page').css('opacity',1);

        <?php } ?>

        <?if(sizeof($audio_files) >0 && $isnormalaudio){?>	//20140526-cupbook
        docpath = bookdata.pages[bookdata.currentPage];
        // 	prevdocpath = bookdata.pages[prevPage].href;

        if(page_mode == 2){
            if( audio_list.indexOf(left_page) >= 0 ){
                $("#btn_cupleft_mp3").css("display", "");
            }else{
                $("#btn_cupleft_mp3").css("display", "none");
            }

            if( audio_list.indexOf(right_page) >= 0 ){
                $("#btn_cupright_mp3").css("display", "");
            }else{
                $("#btn_cupright_mp3").css("display", "none");
            }
            var audio = $("#cup_mp3_player");
            audio[0].pause();
            $('#cup_mp3_src').attr("src", "");
        }else{
            if( audio_list.indexOf(bookdata.currentPage) >= 0 ){
                $("#btn_cupright_mp3").css("display", "");
            }else{
                $("#btn_cupright_mp3").css("display", "none");
            }
        }
        var audio2 = $("#cup_mp3_player_r");
        audio2[0].pause();
        $('#cup_mp3_src_r').attr("src", "");
        $('#player_board1').css("display", "none");
        $('#player_board2').css("display", "none");
        <?php } ?>

        bookdata.book_comic_content_handler();
        bookdata.repositionHighlights();

        <?if($engclassicMode && !$youngreaderMode){?>
        if(page_mode == 2){
            $("#btn_mp3").css("display","none");
            $("#btn_mp3_r").css("display","none");
            $(".page").each(function(){
                if($(this).attr("pageno") == left_page){
                    $("#btn_mp3").css("display","");
                }else if ($(this).attr("pageno") == right_page){
                    $("#btn_mp3_r").css("display","");
                }
            });
        }else{
            $("#btn_mp3").css("display","none");
            $(".page").each(function(){
                if($(this).attr("pageno") == bookdata.currentPage){ //20150311-currentPage +1
                    $("#btn_mp3").css("display","");
                }
            });
        }
        <?php } ?>

        initThickBox();
        <?if($cupecMode == 1){?>
        passageZoom();
        <?php } ?>
    }

    function jsToPage(href)
    {
        $("#btn_return").hide();
        $("#tool_glossary_paper").hide();
        for(i=1;i<bookdata.pages.length;i++){
            if(bookdata.pages[i]['href'] == href){

                if(!bookdata.isImageBook){
                    $('#book_content').show();
                    $('#book_comic').hide();
                }else{
                    $('#book_comic').show();
                    $('#book_content').hide();
                }

                jsLoadDocument(i);
                $('#book_table_of_content').fadeOut('slow',function(){
                    bookdata.book_comic_content_handler();
                });
                $("#book_content").css({opacity:1});
                break;
            }
        }
    }

    function jsToPageFromBookmark(i)
    {
        $("#tool_page_related").hide();
        bookdata.searching2 = false;
        $("#btn_return").hide();
        $("#tool_glossary_paper").hide();
        jsLoadDocument(i);
        $('#tool_bookmark_list').hide();
        jsHideTocAfterChangePage();
        jsUnblockUI();
    }

    function jsToPageFromNotes(page)
    {
        $("#tool_page_related").hide();
        bookdata.searching2 = false;
        $("#btn_return").hide();
        $("#tool_glossary_paper").hide();
        var pageIndex = jsTruePageNumberToPageIndex(page);
        jsLoadDocument(pageIndex);
        $('#tool_my_notes').hide();
        jsHideTocAfterChangePage();
        jsUnblockUI();
    }

    function jsToPageFromSearch(page)
    {
        if(bookdata.pageMode == 2){
            var total_page = (bookdata.pages.length - 1) * 2;
            if($(this).attr("id") == "btn_glossary_left")	{
                var currentPage = (bookdata.currentPage * 2 - 1);
                $("#tool_glossary_paper").attr("class", "tool_glossary_list_pageR");
            }
            else {
                var currentPage = Math.min((bookdata.currentPage * 2), total_page);
                $("#tool_glossary_paper").attr("class", "tool_glossary_list_pageL");
            }
        }else{
            var currentPage = bookdata.currentPage;
        }

        previous_page_num = currentPage;

        $("#btn_return").hide();

        $("#tool_page_related").hide();
        bookdata.searching2 = false;

        $("#tool_glossary_paper").hide();
        var pageIndex = jsTruePageNumberToPageIndex(page);
        jsLoadDocument(pageIndex);
        jsHideTocAfterChangePage();

        <?if($platform!='iPad'){?>
        $("#btn_return").css({"display": "", "left" :"44%","top": "30px",  "z-index": 4});
        <?}else{?>
        $("#btn_return").css({"display": "", "left" :"400px","top": "20px",  "z-index": 4});
        <?}?>

        $("#inner_page_num").html(previous_page_num);
    }

    function jsHideTocAfterChangePage()
    {
        if($('#book_table_of_content').is(':visible')){
            if((browsertype=='MSIE' && !bookdata.isImageBook) || bookdata.pageMode==1){
                var dv_slip = $('#book_table_of_content_container').children().eq(0);
                var dv_toc = $('#book_table_of_content_container').children().eq(1);
                if(dv_slip.is(':visible')){
                    dv_toc.show();
                    dv_slip.hide();
                    <?php if($isRTL == 1){ ?>//20131204-ebook-righttoleft
                    $('#btn_prev_page').css('z-index',5);
                    $('#btn_next_page').css('opacity',0);
                    <?php }else{ ?>
                    $('#btn_next_page').css('z-index',5);
                    $('#btn_prev_page').css('opacity',0);
                    <?php } ?>
                }else{
                    $('#book_table_of_content').fadeOut('slow',function(){
                        bookdata.book_comic_content_handler();
                    });
                    $('#book_content').show();
                    if(bookdata.isImageBook){
                        $('#book_comic').show();
                    }
                    dv_toc.hide();
                    dv_slip.show();
                    <?php if($isRTL == 1){ ?>//20131204-ebook-righttoleft
                    $('#btn_next_page').css('z-index',2);
                    $('#btn_prev_page').css('opacity',1);
                    <?php }else{ ?>
                    $('#btn_prev_page').css('z-index',2);
                    $('#btn_next_page').css('opacity',1);
                    <?php } ?>

                }
            }else{
                $('#book_table_of_content').fadeOut('slow',function(){
                    bookdata.book_comic_content_handler();
                });
                $('#book_content').show();
                if(bookdata.isImageBook){
                    $('#book_comic').show();
                }
            }
        }
    }

    bookdata.tool_page_number_handler = function(isResize,isVisible){
        var dv = $('#tool_page_select a');
        var dvp = $('#tool_page_num');
        var dvb = $('#btn_bottom_tool_bar');
        if(dv.length > 0){
            //Charles Ma 2013-06-20
            //Charles Ma 2013-10-17

            scale = isZoom ? parseFloat($('body').css('zoom')) : 1;


            if(!is_loading_finish)
                dvp.css({position:'absolute', left: dv.offset().left - dvp.width()/2 + dv.width()/2, top: dvb.offset().top - dvp.height() + 20});
            else
            {
                <?php if($ForceToOnePageMode){ ?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale});  //top: 0.72 *scale * 100 + "%"

                var dv_ref = $('#book_structure');
                $('#book_table_of_content').css({
                                                    left: parseInt(dv_ref.offset().left)
                                                });
                <?php }else{ ?>
                <?php if($_SESSION['eBookReaderPageMode']==1){ ?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale, top: "88%"});
                <?php }else{ ?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale, top: "84%"});
                <?php } ?>
                <?php } ?>
            }
            //Charles Ma 2013-10-17 END

            if(isResize != true){
                if(isVisible == true){
                    dvp.show();
                }else{
                    dvp.hide();
                }
            }
        }
    }
    //2012-10-11 CharlesMa
    bookdata.tool_highlight_board_handler = function(){

        var self = $(this);
        var dvb = $('#tool_highlight_board');
        if(dvb.is(':visible') && typeof(bookdata.prevHighlightBtn) != 'undefined'){

            var window_width = $(window).width(), narrowest_window_width =  bookdata.pageMode == 1? 760: 1020, leftest_posx =  bookdata.pageMode == 1? 270:530;
            var pos_limit = (((window_width - narrowest_window_width)/2)>0? ((window_width - narrowest_window_width)/2) + leftest_posx : leftest_posx)+35;                          //2012-10-11 CharlesMa


            //2013-03-27 CharlesMa eBook-update-130325d
            var btn_left = (bookdata.prevHighlightBtn.offset().left - bookdata.prevHighlightBtn.context.offsetLeft);

            if(btn_left > 0)
                var posx = bookdata.prevHighlightBtn.offset().left - 30 < pos_limit ? bookdata.prevHighlightBtn.offset().left - 30 : pos_limit;         //2012-10-11 CharlesMa
            else
                var posx = bookdata.prevHighlightBtn.context.offsetLeft + 130 < pos_limit ? bookdata.prevHighlightBtn.context.offsetLeft + 130 : pos_limit;         //2012-10-11 CharlesMa

            //2013-03-27 END

            //var posx = bookdata.prevHighlightBtn.offset().left - 30;
            var posy = bookdata.prevHighlightBtn.offset().top - dvb.height();
            var dvbx = dvb.offset().left;
            var dvby = dvb.offset().top;
            dvb.css({position:'absolute', left: posx, top: posy});
        }
    }

    bookdata.highlight_change_color_handler = function(){
        var self = $(this); // a.btn_hightlight_xx
        btn_class = self.attr('class');
        var highlight_btn = bookdata.prevHighlightBtn;
        if(typeof(highlight_btn)!='undefined'){
            var uid = highlight_btn.attr('id').replace('hightlight_','');
            var highlight_id = highlight_btn.attr('id');
            $('#' + highlight_id).removeClass().addClass(bookdata.highlightConfig['colorClassOptions'][btn_class]);
            bookdata.highlightConfig['selectedColorClass'] = bookdata.highlightConfig['colorClassOptions'][btn_class];
            highlightObj = bookdata.findHighlightByUniqueID(uid);
            // flush to DB
            if(typeof(highlightObj)!='undefined' && highlightObj != null){
                highlightObj['class'] = bookdata.highlightConfig['colorClassOptions'][btn_class];
                ajAddHighlight(highlightObj);
            }
            $('ul.tool_selected_hightlight li').removeClass();
            $('selected_hightlight a').attr("class", '.btn_select_hightlight_01');

            $('#tool_highlight_board').hide();
        }
    }


    bookdata.removeHighlight = function(e){
        var self = $(this); // a.btn_clear_highlight
        var btn_class = self.attr('class');
        var highlight_btn = bookdata.prevHighlightBtn;

        if(typeof(highlight_btn)!='undefined'){
            var highlight_id = highlight_btn.attr('id');
            var uid = highlight_id.replace('hightlight_','');
            var highlight_span = $('#' + highlight_id);

            var span = document.getElementById(highlight_id);

            if(span){
                var childnode = span.traverseTop({});
            }
            if(highlight_span.length > 0){
                var content_text = span.previousSibling.textContent + highlight_span.html()+ span.nextSibling.textContent;

                if(span.previousSibling.length > 0)
                    span.parentNode.removeChild(span.previousSibling);
                if(span.nextSibling.length > 0)
                    span.parentNode.removeChild(span.nextSibling);

                highlight_btn.after(content_text);

                highlight_span.remove();

                delete bookdata.prevHighlightBtn;
                $('#tool_highlight_board').hide();

                //TODO: DATABASE
                highlightObj = bookdata.findHighlightByUniqueID(uid);

                if(typeof(highlightObj)!='undefined' && highlightObj != null){
                    ajDeleteHighlight(highlightObj);
                }

            }
        }
    }


    bookdata.addHighlight = function(newHighlight){
        this.highlights.push(newHighlight);
        ajAddHighlight(newHighlight);
    }

    bookdata.getHighlights = function(){
        return this.highlights;
    }

    bookdata.findHighlightByUniqueID = function(uid){
        for(var i=0;i<bookdata.highlights.length;i++){
            if(bookdata.highlights[i]['uniqueID'] == uid){
                return bookdata.highlights[i];
                break;
            }
        }
        return undefined;
    }

    bookdata.highlight_notes_handler = function(isResize){
        var self = $(this); // #btn_add_highlight_notes
        var dvs = $('#book_structure');
        var dvp = $('#tool_highlight_notes_paper');
        var posx = dvs.offset().left + dvs.width()/2 - dvp.width()/2;
        var posy = dvs.offset().top + dvs.height()/2 - dvp.height()/2;
        dvp.css({position:'absolute', left: posx, top: posy});

        if(isResize != true){
            if(typeof(bookdata.prevHighlightBtn)!= 'undefined'){
                var uid = bookdata.prevHighlightBtn.attr('id').replace('hightlight_','');
                var highlightObj = bookdata.findHighlightByUniqueID(uid);
                if(typeof(highlightObj)!='undefined' && highlightObj != null){
                    bookdata.prevHighlightObj = highlightObj;
                    var dt = new Date(highlightObj.dateModify);
                    $('#highlight_note_pager_date').html(jsFormatDatetime(dt));
                    $('#highlight_notes_pager_notes').val(highlightObj.notes);
                }
            }

            if(dvp.is(':visible')){
                dvp.hide();
            }else{
                jsBlockUI();
                dvp.show();
                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.hide();
                }
            }
        }
    }

    bookdata.highlight_notes_icon_handler = function(isResize){
        var self = $(this); // #note_ID
        var dvs = $('#book_structure');
        var dvp = $('#tool_highlight_notes_paper');
        var posx = dvs.offset().left + dvs.width()/2 - dvp.width()/2;
        var posy = dvs.offset().top + dvs.height()/2 - dvp.height()/2;
        dvp.css({position:'absolute', left: posx, top: posy});

        if(isResize != true){
            //if(typeof(bookdata.prevHighlightBtn)!= 'undefined'){
            var uid = self.attr('id').replace('note_','');
            var highlightObj = bookdata.findHighlightByUniqueID(uid);
            if(typeof(highlightObj)!='undefined' && highlightObj != null){

                var dt = new Date(highlightObj.dateModify);
                $('#highlight_note_pager_date').html(jsFormatDatetime(dt));
                $('#highlight_notes_pager_notes').val(highlightObj.notes);
            }
            //}

            if(dvp.is(':visible')){
                dvp.hide();
            }else{
                jsBlockUI();
                dvp.show();
                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.hide();
                }
            }
        }
    }

    function ajAddHighlight(highlightObj)
    {
        <?php if($ForceToOnePageMode){ ?>
        return true;
        <?php } else { ?>
        if(typeof(highlightObj) != 'undefined'){
            var temp_chapter_arr = [];
            var temp_chapter_page_arr = [];
            var index = bookdata.pageMode == 1 ? 1 : 2;
            for(var i in bookdata.pages){
                if(bookdata.pages[i]["href"] == ""){
                    continue;
                }
                else if(jQuery.inArray( bookdata.pages[i]["href"], temp_chapter_arr ) == -1){
                    temp_chapter_arr.push(bookdata.pages[i]["href"]);
                    temp_chapter_page_arr[jQuery.inArray( bookdata.pages[i]["href"], temp_chapter_arr )] = index;
                }else{
                    temp_chapter_page_arr[jQuery.inArray( bookdata.pages[i]["href"], temp_chapter_arr )] += index;
                }
            }

            var current_chapter = "";

            for(var j =0 ; j< temp_chapter_page_arr.length; j++){
                temp_chapter_page_arr[j] = j == 0 ? temp_chapter_page_arr[0] : temp_chapter_page_arr[j] + temp_chapter_page_arr[j-1];

                if(highlightObj['page'] > temp_chapter_page_arr[j] ){
                }else{
                    current_chapter= temp_chapter_arr[j];
                    break;
                }
            }

            var copyHighlight = {"uniqueID": highlightObj['uniqueID'],
                "id": highlightObj['id'],
                "text": encodeURIComponent(highlightObj['text']),
                "startOffset": highlightObj['startOffset'],
                "endOffset": highlightObj['endOffset'],
                "class": highlightObj['class'],
                "notes": encodeURIComponent(highlightObj['notes']),
                "page": highlightObj['page'],
                "dateModify": highlightObj['dateModify'],
                "childNode": highlightObj['childNode'],
                "chapter":current_chapter
            };

            $.post(
                '/home/eLearning/ebook_reader/ajax_task.php',
                {
                    'task':'AddHighlight',
                    'BookID':bookdata.id,
                    'RefID':encodeURIComponent(bookdata.pages[bookdata.currentPage]['idref']),
                    'UniqueID':highlightObj['uniqueID'],
                    'Highlight':JSON.stringify(copyHighlight)
                },
                function(feedback){
                    if(bookdata.highlights && bookdata.highlights.length > 0){
                        bookdata.highlights.sort(jsSortHighlightsComparer);
                    }
                    ajUpdateAllHighlights();
                }
            );
        }

        <?php } ?>
    }

    function ajDeleteHighlight(highlightObj)
    {
        if(typeof(highlightObj) != 'undefined'){
            $.post(
                '/home/eLearning/ebook_reader/ajax_task.php',
                {
                    'task':'DeleteHighlight',
                    'BookID':bookdata.id,
                    'UniqueID':highlightObj['uniqueID']
                },
                function(feedback){
                    if(bookdata.highlights && bookdata.highlights.length > 0){
                        bookdata.highlights.sort(jsSortHighlightsComparer);
                    }
                    ajUpdateAllHighlights();
                }
            );
        }
    }

    function updateOffset(currentNode, startOffset, endOffset){
        var occupied_offset = endOffset-startOffset;
        var new_startOffset = currentNode.previousSibling.length;
        currentNode = currentNode.previousSibling;
        while(currentNode.previousSibling != null && currentNode.previousSibling.length != undefined){
            new_startOffset += currentNode.previousSibling.length;
            currentNode = currentNode.previousSibling;
        }


        return [new_startOffset,new_startOffset + occupied_offset];
    }

    function ajUpdateAllHighlights()
    {
        <?php if($ForceToOnePageMode){ ?>
        return true;
        <?php } else { ?>
        var hl_list = bookdata.highlights;
        var str_list = [];
        var uid_list = [];
        var index = bookdata.pageMode == 1 ? 1 : 2;

        for(var i=0;i<hl_list.length;i++){
            var span = document.getElementById(hl_list[i]['id']);
            if(span){
                hl_list[i]['childNode'] = span.traverseTop({});
                var newOffset = updateOffset(span, hl_list[i]['startOffset'], hl_list[i]['endOffset']);
                hl_list[i]['startOffset'] = newOffset[0];
                hl_list[i]['endOffset'] = newOffset[1];
                uid_list.push(hl_list[i]['uniqueID']);

                var temp_chapter_arr = [];
                var temp_chapter_page_arr = [];

                for(var k in bookdata.pages){
                    if(bookdata.pages[k]["href"] == ""){
                        continue;
                    }
                    else if(jQuery.inArray( bookdata.pages[k]["href"], temp_chapter_arr ) == -1){
                        temp_chapter_arr.push(bookdata.pages[k]["href"]);
                        temp_chapter_page_arr[jQuery.inArray( bookdata.pages[k]["href"], temp_chapter_arr )] = index;
                    }else{
                        temp_chapter_page_arr[jQuery.inArray( bookdata.pages[k]["href"], temp_chapter_arr )] += index;
                    }
                }

                var current_chapter = "";

                for(var j =0 ; j< temp_chapter_page_arr.length; j++){
                    temp_chapter_page_arr[j] = j == 0 ? temp_chapter_page_arr[0] : temp_chapter_page_arr[j] + temp_chapter_page_arr[j-1];

                    if(hl_list[i]['page'] > temp_chapter_page_arr[j] ){
                    }else{
                        current_chapter= temp_chapter_arr[j];
                        break;
                    }
                }

                var copyHighlight = {"uniqueID": hl_list[i]['uniqueID'],
                    "id": hl_list[i]['id'],
                    "text": encodeURIComponent(hl_list[i]['text']),
                    "startOffset": hl_list[i]['startOffset'],
                    "endOffset": hl_list[i]['endOffset'],
                    "class": hl_list[i]['class'],
                    "notes": encodeURIComponent(hl_list[i]['notes']),
                    "page": hl_list[i]['page'],
                    "dateModify": hl_list[i]['dateModify'],
                    "childNode": hl_list[i]['childNode'],
                    "chapter" : current_chapter
                };
                str_list.push(JSON.stringify(copyHighlight));
            }
        }

        bookdata.note_arr = [];
        $("#book_note_main_container").html("");
        var highlights = [];

        for(var i =0; i < str_list.length; ++i){
            highlights[i] = JSON.parse(str_list[i]);
        }

        for (var i =0; i < highlights.length; ++i)
        {
            var currentHighlightBtn =  $('#' + highlights[i].id);
            <? if($cupMode){?>
            var current_paragraph = $(currentHighlightBtn.parent().parent()[0]);
            var current_offsetLeft = current_paragraph[0]["offsetLeft"];
            <?}else{?>
            var current_paragraph = $(currentHighlightBtn.parent()[0]);
            var current_offsetLeft = currentHighlightBtn[0]["offsetLeft"];
            <?}?>

            if(current_paragraph[0].nodeName == "P"){
                var nextElementSibling = $(current_paragraph[0].nextElementSibling);
                var previousElementSibling = $(current_paragraph[0].previousElementSibling);
            }else{
                var nextElementSibling = $(current_paragraph.parent()[0].nextElementSibling);
                var previousElementSibling = $(current_paragraph.parent()[0].previousElementSibling);
            }

            var current_highlight_offset = currentHighlightBtn.offset();
            var current_offsetTop = currentHighlightBtn[0]["offsetTop"];
            //var current_offsetLeft = currentHighlightBtn[0]["offsetLeft"];
            var current_offsetHeight = currentHighlightBtn[0]["offsetHeight"];
            var current_paragraph_offsetWidth = current_paragraph[0]["offsetWidth"];
            var current_paragraph_offsetLeft = current_paragraph[0]["offsetLeft"];

            if(currentHighlightBtn.length > 0){
                if(highlights[i]['notes']!=''){
                    var note_btn = '<span id="note_'+highlights[i]['uniqueID']+'" class="btn_notes" title="Notes"></span>';

                    var current_chapter_first_page = bookdata.currentPage - bookdata.pages[bookdata.currentPage].session + 1;
                    if(bookdata.pageMode == 1){
                        var requestPage = parseInt(current_offsetLeft/650)+current_chapter_first_page;
                    }else{
                        var requestPage = parseInt(current_offsetLeft/980)+current_chapter_first_page;
                    }

                    var note_container = '<div id="note_page_'+requestPage+'" page="'+requestPage+'" class="notes_container" title="Notes"></div>';

                    if($('#note_page_'+requestPage).length == 0){
                        $("#book_note_main_container").append(note_container);
                    }

                    var note_container_elem = $('#note_page_'+requestPage);

                    var diff = requestPage - bookdata.currentPage;
                    var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff;

                    if(note_container_elem.length == 1){ //20141021

                        var note_btn_elem = $('#note_' + requestPage+'_'+current_offsetTop+'_'+offsetPage);

                        if((Math.floor((current_offsetLeft%980)/490))%2 == 1) var offsetPage = "right";
                        else var offsetPage = "left";

                        if((Math.floor(((current_paragraph.offset().left%980+980)%980)/490))%2 == 1) var offsetParagraph = "right";
                        else var offsetParagraph = "left";

                        if(note_btn_elem.length == 0){
                            var note_btn = '<div page="'+requestPage+'" id="note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage+'" class="notes_side" title="Notes"><a href="javascript:void(0)"></a></div>';
                            $('#note_page_'+requestPage).append(note_btn);
                            var note_btn_elem = $('#note_' + requestPage+'_'+current_offsetTop+'_'+offsetPage);
                            $('#note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage).mouseup(bookdata.tool_my_notes_handler);
                        }
                        var current_note_market_height = parseInt(note_btn_elem.css("height"));
                        note_btn_elem.css("height", current_note_market_height > current_offsetHeight? current_note_market_height : current_offsetHeight);

                        note_btn_elem.css("top", current_highlight_offset.top);

                        if(bookdata.pageMode == 1){
                            note_btn_elem.css("left", current_paragraph.offset().left+current_paragraph_offsetWidth+5 - dist);
                        }else{
                            if(offsetPage == "right"){
                                if(offsetParagraph == "right"){
                                    note_btn_elem.css("left", current_paragraph.offset().left+current_paragraph_offsetWidth+5 - dist);
                                } else{
                                    if(current_paragraph.offset().left < current_offsetLeft ){
                                        note_btn_elem.css("left", nextElementSibling.offset().left+current_paragraph_offsetWidth+5 - dist);
                                    }else{
                                        note_btn_elem.css("left", previousElementSibling.offset().left+current_paragraph_offsetWidth+5 - dist);
                                    }
                                }
                            }else{
                                if(offsetParagraph == "left"){
                                    note_btn_elem.css("left", current_paragraph.offset().left-25 - dist);
                                }else{
                                    if(current_paragraph.offset().left < current_offsetLeft ){
                                        if((nextElementSibling.offset().left-25 - dist) < 0){
                                            note_btn_elem.css("left", current_paragraph.offset().left+current_paragraph_offsetWidth+25 - dist);
                                        }else{
                                            note_btn_elem.css("left", nextElementSibling.offset().left-25 - dist);
                                        }
                                    }else{
                                        note_btn_elem.css("left", previousElementSibling.offset().left-25 - dist);
                                    }
                                }
                                note_btn_elem.addClass("left");
                            }
                        }

                        if(bookdata.note_arr['note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage] == undefined){
                            bookdata.note_arr['note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage] = [];
                        }
                        bookdata.note_arr['note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage].push(highlights[i]['uniqueID']);
                    }
                }
            }
        }

        $(".notes_container").css("display","none");
        $("#note_page_"+bookdata.currentPage).css({"display":"", "opacity" : 0});
        $("#note_page_"+bookdata.currentPage).animate({opacity: 1.0}, 'slow');
        if(uid_list.length > 0){
            $('#tool_highlight_board').hide();
            $.post(
                '/home/eLearning/ebook_reader/ajax_task.php',
                {
                    'task':'UpdateAllHighlights',
                    'BookID':bookdata.id,
                    'RefID':encodeURIComponent(bookdata.pages[bookdata.currentPage]['idref']),
                    'UniqueID[]':uid_list,
                    'Highlight[]':str_list
                },
                function(feedback){
                    glossary_attach();
                }
            );
        }
        <?php } ?>
    }

    function ajUpdateAllHighlights1()
    {
        <?php if($ForceToOnePageMode){ ?>
        return true;
        <?php } else { ?>
        var hl_list = bookdata.highlights;
        var str_list = [];
        var uid_list = [];
        for(var i=0;i<hl_list.length;i++){
            var span = document.getElementById(hl_list[i]['id']);
            if(span){
                hl_list[i]['childNode'] = span.traverseTop({});
                uid_list.push(hl_list[i]['uniqueID']);
                var copyHighlight = {"uniqueID": hl_list[i]['uniqueID'],
                    "id": hl_list[i]['id'],
                    "text": encodeURIComponent(hl_list[i]['text']),
                    "startOffset": hl_list[i]['startOffset'],
                    "endOffset": hl_list[i]['endOffset'],
                    "class": hl_list[i]['class'],
                    "notes": encodeURIComponent(hl_list[i]['notes']),
                    "page": hl_list[i]['page'],
                    "dateModify": hl_list[i]['dateModify'],
                    "childNode": hl_list[i]['childNode']};
                str_list.push(JSON.stringify(copyHighlight));
            }
        }

        if(uid_list.length > 0){
            $.post(
                '/home/eLearning/ebook_reader/ajax_task.php',
                {
                    'task':'UpdateAllHighlights',
                    'BookID':bookdata.id,
                    'RefID':encodeURIComponent(bookdata.pages[bookdata.currentPage]['idref']),
                    'UniqueID[]':uid_list,
                    'Highlight[]':str_list
                },
                function(feedback){

                }
            );
        }
        <?php }  ?>
    }

    function jsSortHighlightsComparer(a,b)
    {
        var cp = 0;
        var nodeA = a;
        var nodeB = b;
        //while(nodeA.childNode && nodeB.childNode && nodeA.childNode.node && nodeB.childNode.node && cp == 0){ 2013-3-26 Charles Ma
        while(nodeA.childNode && nodeB.childNode  && cp == 0){
            if(nodeA.childNode.node == nodeB.childNode.node){
                cp = 0;
            }else if(nodeA.childNode.node < nodeB.childNode.node){
                cp = -1;
            }else{
                cp = 1;
            }
            nodeA = nodeA.childNode;
            nodeB = nodeB.childNode;
        }

        return cp;
    }

    function ajAddBookmark(pageNum)
    {
        if(<?=$ForceToOnePageMode?'true':'false'?>){
            //		return true;
        }
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                'task':'AddBookmark',
                'BookID':bookdata.id,
                'PageMode':bookdata.pageMode,
                'PageNum': pageNum
            },
            function(feedback){

            }
        );
    }

    function ajDeleteBookmark(pageNum)
    {
        if(<?=$ForceToOnePageMode?'true':'false'?>){
            //		return true;
        }
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                'task':'DeleteBookmark',
                'BookID':bookdata.id,
                'PageMode':bookdata.pageMode,
                'PageNum': pageNum
            },
            function(feedback){

            }
        );
    }

    bookdata.bookmark_image_handler = function(isResize){
        if(this.pageMode == 2){
            var dvs = $('#book_structure');

            var img_left = $('#bookmark_img_left');
            var img_right = $('#bookmark_img_right');
            //TODO: Charles Ma 2013-06-20
            if(img_left.is(':visible')){
                //img_left.css({position:'absolute', left: dvs.offset().left + dvs.width()/2 - 50, top: dvs.offset().top});
                img_left.css({position:'absolute', left: '46.5%', top: '1.3%'});
            }
            if(img_right.is(':visible')){
                //img_right.css({position:'absolute', left: dvs.offset().left + dvs.width()/2 + 15, top: dvs.offset().top});
                img_right.css({position:'absolute', left: '51%', top: '1.3%'});
            }
            if(isResize != true){
                if(img_left.is(':visible')){
                    img_left.hide();
                }else{
                    img_left.show();
                }
                if(img_right.is(':visible')){
                    img_right.hide();
                }else{
                    img_right.show();
                }
            }
        }else{
            var dvs = $('#book_structure');
            var img_left = $('#bookmark_img_left');
            if(img_left.is(':visible')){
                img_left.css({position:'absolute', left: dvs.offset().left + 106, top: dvs.offset().top});
            }
            if(isResize != true){
                if(img_left.is(':visible')){
                    img_left.hide();
                }else{
                    img_left.show();
                }
            }
        }
    }

    bookdata.bookmark_image_toggle_handler = function(id, page_num){
        var bookmarkId = "btn_bookmark_" + id.replace("bookmark_img_","");
        var img = $('#' + id);
        var bookmark = $('#'+bookmarkId);
        var num_index = bookdata.bookmarks.indexOf(page_num);
        if(num_index >=0){
            img.show();
            if(<?=$ForceToOnePageMode?'true':'false'?>){
                bookmark.hide();
            }
        }else{
            img.hide();
            if(<?=$ForceToOnePageMode?'true':'false'?>){
                bookmark.show();
            }
        }
        bookdata.bookmark_image_handler(true);
    }

    function ajGetBookNotes(page_num)
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                "task":"GetBookNotes",
                'BookID':bookdata.id,
                'PageMode':bookdata.pageMode,
                'PageNum': page_num
            },
            function(data){
                $('#notes_pager_notes').css("resize", "none"); //2013-03-14 Charles Ma
                eval("var dataObj = " + data);
                $('#notes_pager_date').html(dataObj['DateModified']);
                $('#notes_pager_notes').val(decodeURIComponent(dataObj['Notes']));
                $('#tool_notes_paper').show();
            }
        );
    }

    function ajUpdateBookNotes(page_num, notes)
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                "task": "UpdateBookNotes",
                'BookID':bookdata.id,
                'PageMode':bookdata.pageMode,
                'PageNum': page_num,
                'Notes': encodeURIComponent(notes)
            },
            function(data){

            }
        );
    }

    function ajDeleteBookNotes(page_num)
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                "task": "DeleteBookNotes",
                'BookID':bookdata.id,
                'PageMode':bookdata.pageMode,
                'PageNum': page_num
            },
            function(data){

            }
        );
    }

    function ajGetBookmarkLogList()
    {
        $('#bookmark_log_list').load(
            '/home/eLearning/ebook_reader/ajax_load.php',
            {
                "task":"Get_Tool_Bookmark_List_Log_List",
                "BookID" : bookdata.id,
                "PageMode" : bookdata.pageMode,
                "ImageBook" : (bookdata.isImageBook?'1':'')
            },
            function(data){
                $('#tool_bookmark_list').show();
                bookdata.tool_bookmark_list_handler(true);
                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.hide();
                }
            }
        );
    }

    function ajGetPageNotesList()
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_load.php',
            {
                "task": "Get_Tool_Page_Notes_Log_Entries",
                "BookID": bookdata.id,
                "PageMode": bookdata.pageMode
            },
            function(data){
                var dvn = $('div#notes_log_list');
                if(data!=''){
                    dvn.append(data);
                }else if($.trim(dvn.html()) == ''){
                    dvn.append(jsLang["NoNotesAtTheMoment"]);
                }
                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.hide();
                }
            }
        );
    }

    function ajGetMyNotes(notes_entry,note_id)
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_load.php',
            {
                "task": "Get_Book_User_Highlight_Notes",
                "BookID": bookdata.id
            },
            function(data){
                data = data.replace(/\[object Object\][,]?/g,"");
                eval("bookdata.allMyNotesList = " + data + ";");
                var x = '';
                bookdata.myNotesList = [];

                if(note_id != "btn_my_notes"){
                    for(var j=0;j<bookdata.allMyNotesList.length;j++){
                        for(var k=0;k<bookdata.note_arr[note_id].length;k++){
                            if(bookdata.allMyNotesList[j]["uniqueID"] == bookdata.note_arr[note_id][k]){
                                bookdata.myNotesList.push(bookdata.allMyNotesList[j]);
                            }
                        }
                    }
                }else{
                    bookdata.myNotesList = bookdata.allMyNotesList;
                }

                var temp_note_arr = [];
                if(bookdata.myNotesList.length>0){
                    for(var i=0;i<bookdata.myNotesList.length;i++){
                        var current_chapter_index = jQuery.inArray(bookdata.myNotesList[i]['chapter'], toc2_list);
                        current_chapter_index = current_chapter_index == -1 ? 100 : current_chapter_index;
                        bookdata.myNotesList[i]['text'] = decodeURIComponent(bookdata.myNotesList[i]['text']);
                        bookdata.myNotesList[i]['text'] = bookdata.myNotesList[i]['text'].replace(/\<[\.\/0-9\-a-zA-Z\'\"\=\_ ]+\>|\&nbsp\;/g,"");

                        bookdata.myNotesList[i]['notes'] = decodeURIComponent(bookdata.myNotesList[i]['notes']);

                        bookdata.myNotesList[i]['chapterurl'] = bookdata.myNotesList[i]['chapter'];
                        bookdata.myNotesList[i]['chapter'] = current_chapter_index != 100 ? toc_list[current_chapter_index] : "Others";

                        if(temp_note_arr[current_chapter_index]){
                            temp_note_arr[current_chapter_index] += jsMyNotesEntry(notes_entry,bookdata.myNotesList[i]);
                        }else{
                            temp_note_arr[current_chapter_index] = jsMyNotesEntry(notes_entry,bookdata.myNotesList[i]);
                        }
//          x += jsMyNotesEntry(notes_entry,bookdata.myNotesList[i]);
                    }
                }

                x = temp_note_arr.join("");
                $('#notes_log_list').html(x);
                ajGetPageNotesList();

                $(".noteslog_notes").each(function(){
                    $(this).attr("value",$(this).html());
                    if($(this).html().length>=55){
                        $(this).html($(this).html().substring(0, 55) + "...");
                    }
                });

                $('#tool_my_notes').show();
                bookdata.tool_my_notes_handler(true);
                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.hide();
                }
            }
        );
    }

    function ajGetMyNotesLogEntry(note_id)
    {
        $.get(
            '/home/eLearning/ebook_reader/ajax_load.php',
            {
                "task":"Get_Tool_Notes_Log_Entry"
            },
            function(data){
                ajGetMyNotes(data,note_id);
            }
        );
    }

    function jsMyNotesEntry(template, obj)
    {
        var x = template;
        x = x.replace(/{UNIQUEIDd9dff820-6828-11e1-b86c-0800200c9a66}/g, obj['uniqueID']);
        //x = x.replace('{DATETIMEd9dff820-6828-11e1-b86c-0800200c9a66}',jsFormatDatetime(obj['dateModify']));
        x = x.replace('{DATETIMEd9dff820-6828-11e1-b86c-0800200c9a66}',jsFormatDatetime(obj['dateModify']));

        x = x.replace(/{PAGE_NUMBERd9dff820-6828-11e1-b86c-0800200c9a66}/g,obj['page']);
        x = x.replace('{NOTESd9dff820-6828-11e1-b86c-0800200c9a66}',obj['notes']);
        x = x.replace('{HIGHLIGHT_CLASSd9dff820-6828-11e1-b86c-0800200c9a66}',obj['class']);
        x = x.replace('{HIGHLIGHT_TEXTd9dff820-6828-11e1-b86c-0800200c9a66}',obj['text']);
        x = x.replace('{CHAPTERd9dff820-6828-11e1-b86c-0800200c9a66}',obj['chapter']);

        x = x.replace('{CHAPTERURLd9dff820-6828-11e1-b86c-0800200c9a66}',obj['chapterurl']);
        return x;
    }

    function jsDeleteNotesLog(uid)
    {
        if(confirm(jsLang['ConfirmDeleteNotes'])){
            if(typeof(bookdata.myNotesList)!='undefined' && bookdata.myNotesList.length > 0){
                var highlightObj = null;
                var arrIndex = -1;
                for(var i=0;i<bookdata.myNotesList.length;i++){
                    highlightObj = bookdata.myNotesList[i];
                    if(highlightObj['uniqueID'] == uid){
                        arrIndex = i;
                        break;
                    }
                }
                if(highlightObj != null){
                    $.post(
                        '/home/eLearning/ebook_reader/ajax_task.php',
                        {
                            'task':'DeleteHighlight',
                            'BookID':bookdata.id,
                            'UniqueID':highlightObj['uniqueID']
                        },
                        function(feedback){
                            var result = feedback.split('|=|');
                            if(result[0] == '1'){
                                var div_log = $('#noteslog_' + uid);
                                var spacer = $('#noteslog_spacer_' + uid);
                                spacer.remove();
                                div_log.remove();
                                bookdata.myNotesList.splice(arrIndex,1);
                                var highlightObjFound = null;
                                var foundIndex = -1;
                                for(var i=bookdata.highlights.length-1;i>=0;i--){
                                    if(bookdata.highlights[i]['uniqueID'] == uid){
                                        highlightObjFound = bookdata.highlights[i];
                                        foundIndex = i;
                                        break;
                                    }
                                }
                                if(typeof(highlightObjFound) != 'undefined' && highlightObjFound != null){
                                    /*
                var note_btn = $('#note_'+uid);
                if(note_btn.length > 0){
                  var highlight_span = $('#hightlight_' + uid);
                  if(highlight_span.length > 0){
                    var content_text = highlightObjFound['text'];
                    highlight_span.remove();
                    note_btn.after(content_text);
                    note_btn.remove();
                    bookdata.highlights.splice(foundIndex,1);
                  }
                }
                */
                                    var highlight_span = $('#hightlight_'+uid);
                                    if(highlight_span.length > 0){
                                        var notes_btn = $('#notes_' + uid);
                                        var content_text = highlightObjFound['text'];
                                        highlight_span.after(content_text);
                                        highlight_span.remove();
                                        if(notes_btn.length>0) notes_btn.remove();
                                        bookdata.highlights.splice(foundIndex,1);
                                    }
                                }
                            }ajUpdateAllHighlights();
                        }
                    );
                }
            }
        }
    }

    function jsDeletePageNotesLog(recordID)
    {
        if(confirm(jsLang['ConfirmDeleteNotes'])){
            $.post(
                '/home/eLearning/ebook_reader/ajax_task.php',
                {
                    'task':'DeleteBookNotesByRecordID',
                    'RecordID':recordID
                },
                function(feedback){
                    var result = feedback.split('|=|');
                    if(result[0] == '1'){
                        var div_log = $('#page_noteslog_' + recordID);
                        var spacer = $('#page_noteslog_spacer_' + recordID);
                        spacer.remove();
                        div_log.remove();
                    }
                }
            );
        }
    }

    function jsEditNotesLog(uid)
    {
        if(typeof(bookdata.myNotesList)!='undefined' && bookdata.myNotesList.length > 0){
            var highlightObj = null;
            for(var i=0;i<bookdata.myNotesList.length;i++){
                highlightObj = bookdata.myNotesList[i];
                if(highlightObj['uniqueID'] == uid){
                    break;
                }
            }
            if(highlightObj != null){
                var btn_done = $('#btn_noteslog_done_' + uid);
                btn_done.attr('disabled',true);
                highlightObj['dateModified'] = (new Date).getTime();
                var textarea = $('#noteslog_textedit_'+uid);
                var div_notes = $('#noteslog_notes_div_'+uid);
                var div_done = $('#noteslog_btn_layer_' + uid);
                var div_date = $('#noteslog_datetime_'+uid);
                var chapter_url = $('#chapterurl_'+uid);

                highlightObj['notes'] = textarea.val();
                var copyHighlight = {"uniqueID": highlightObj['uniqueID'],
                    "id": highlightObj['id'],
                    "text": encodeURIComponent(highlightObj['text']),
                    "startOffset": highlightObj['startOffset'],
                    "endOffset": highlightObj['endOffset'],
                    "class": highlightObj['class'],
                    "notes": encodeURIComponent(highlightObj['notes']),
                    "page": highlightObj['page'],
                    "dateModify": highlightObj['dateModify'],
                    "childNode": highlightObj['childNode'],
                    "chapter" : chapter_url.attr("value")};
                $.post(
                    '/home/eLearning/ebook_reader/ajax_task.php',
                    {
                        'task':'UpdateHighlightNotesByID',
                        'BookID':bookdata.id,
                        'UniqueID':highlightObj['uniqueID'],
                        'HighlightNotes':JSON.stringify(copyHighlight)
                    },
                    function(feedback){
                        var result = feedback.split('|=|');
                        if(result[0] == '1'){
                            // search current page highlight objects to update notes and modify date
                            var highlightObjFound = bookdata.findHighlightByUniqueID(uid);
                            if(typeof(highlightObjFound) != 'undefined' && highlightObjFound != null){
                                highlightObjFound['notes'] = highlightObj['notes'];
                                highlightObjFound['dateModified'] = highlightObj['dateModified'];
                            }

                            if(textarea.val().length>=55){
                                div_notes.html(textarea.val().substring(0, 55) + "...");
                            }else{
                                div_notes.html(textarea.val());
                            }

                            div_notes.attr("value", textarea.val());
                            div_date.html(jsFormatDatetime(highlightObj['dateModified']));
                        }
                        textarea.hide();
                        div_done.hide();
                        div_notes.show();
                        btn_done.attr('disabled', false);
                    }
                );
            }
        }
    }

    function jsEditPageNotesLog(recordID)
    {
        var btn_done = $('#btn_page_noteslog_done_' + recordID);
        btn_done.attr('disabled',true);
        var modify_ts = (new Date).getTime();
        var textarea = $('#page_noteslog_textedit_'+recordID);
        var div_notes = $('#page_noteslog_notes_div_'+recordID);
        var div_done = $('#page_noteslog_btn_layer_' + recordID);
        var div_date = $('#page_noteslog_datetime_'+recordID);
        var notes = $.trim(textarea.val());

        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                'task':'UpdateBookNotesByRecordID',
                'RecordID':recordID,
                'Notes': encodeURIComponent(notes)
            },
            function(feedback){
                var result = feedback.split('|=|');
                if(result[0] == '1'){
                    div_notes.html(notes);
                    div_date.html(jsFormatDatetime(modify_ts));
                }
                textarea.hide();
                div_done.hide();
                div_notes.show();
                btn_done.attr('disabled', false);
            }
        );
    }

    function jsToggleEditNotesLog(uid)
    {
        var textarea = $('#noteslog_textedit_' + uid);
        var div_done = $('#noteslog_btn_layer_' + uid);
        var div_notes = $('#noteslog_notes_div_' + uid);

//  textarea.val(div_notes.html());
        textarea.val(div_notes.attr("value"));
        div_notes.hide();
        textarea.show();
        div_done.show();
    }

    function jsToggleEditPageNotesLog(recordID)
    {
        var textarea = $('#page_noteslog_textedit_' + recordID);
        var div_done = $('#page_noteslog_btn_layer_' + recordID);
        var div_notes = $('#page_noteslog_notes_div_' + recordID);

        textarea.val(div_notes.html());
        div_notes.hide();
        textarea.show();
        div_done.show();
    }

    bookdata.tool_progress_update_handler = function(e){
        var progress_status = $('input[name=ProgressStatus\\[\\]]:checked').val();
        if(progress_status == 'undefined' || progress_status == null){
            alert(jsLang["PleaseSelectYourProgress"]);
        }else{
            var percentage = $('select#tool_book_progress_percentage').val();
            if(progress_status == 2){
                // count percentage
                if(bookdata.pageMode == 2){
                    if(bookdata.isImageBook){
                        var total_page = bookdata.pages.length - 1;
                        var current_page = Math.min(bookdata.currentPage, total_page);
                    }else{
                        var total_page = (bookdata.pages.length - 1) * 2;
                        var current_page = Math.min((bookdata.currentPage * 2), total_page);
                    }
                }else{
                    var total_page = bookdata.pages.length - 1;
                    var current_page = bookdata.currentPage;
                }
                percentage = Math.round((current_page / total_page) * 100);
            }else if(progress_status == 1){
                pertcentage = '';
            }
            $.post(
                '/home/eLearning/ebook_reader/ajax_task.php',
                {
                    "task":"Update_User_Progress",
                    "BookID":bookdata.id,
                    "ProgressStatus":progress_status,
                    "Percentage":percentage
                },function(feedback){
                    $('#tool_book_progress').hide('slow');
                }
            );
        }
    }

    function jsInitUserProgress(progress_status, percentage)
    {
        var statusArr = $('input[name=ProgressStatus\\[\\]]');
        statusArr.each(function(i, o){
            if(o.value == progress_status ){
                o.checked = true;
            }
        });
        if(progress_status == '3' && percentage != ''){
            $('select#tool_book_progress_percentage').val(percentage);
        }
    }

    function nl2br (str, is_xhtml) {                             // 2012-11-16 Charles Ma
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
    }

    bookdata.tool_recommend_send_handler = function(e){
        var btn_send = $('#btn_recommend_send');
        btn_send.attr('disabled', true);
        var to_email = $.trim( $('input#tool_book_recommend_to').val());

        var book_title = '<br/><br/><a href="'+window.location.href+'">'+$('#book_name').html()+'</a>';// 2012-11-15 Charles Ma

        var msg = nl2br( $.trim($('#tool_book_recommend_text').val()), false) + book_title;// 2012-11-15 Charles Ma

        if(to_email == ''){
            alert(jsLang["PleaseInputEmailAddress"]);
            btn_send.attr('disabled',false);
            return;
        }
        var emails = to_email.split(";");
        var valid_email = true;
        var re = /^([\w_\.\-])+\@(([\w_\.\-])+\.)+([\w]{2,4})+$/;
        for(var i=0;i<emails.length;i++){
            var tmp_email = $.trim(emails[i]);
            if(tmp_email !='' && !re.test(tmp_email)){
                valid_email = false;
                break;
            }
        }
        if(!valid_email){
            alert(jsLang["PleaseInputValidEmailAddresses"]);
            btn_send.attr('disabled',false);
            return;
        }

        if(msg == ''){
            alert(jsLang["PleaseInputRecommendMessage"]);
            btn_send.attr('disabled',false);
            return;
        }

        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                "task":"Send_Recommend_Email",
                "To": encodeURIComponent(to_email),
                "Content": encodeURIComponent(msg),
                "BookID": bookdata.id
            },
            function(feedback){
                var result = feedback.split('|=|');
                alert(result[1]);
                $('#tool_book_recommend').hide('slow');
                $('input#tool_book_recommend_to').val('');
                $('#tool_book_recommend_text').val('');
                btn_send.attr('disabled',false);
            }
        );
    };

    function ajGetBookReview()
    {
        $('#review_log_list').load(
            '/home/eLearning/ebook_reader/ajax_load.php',
            {
                "task":"Get_Tool_Book_Review_Log_List",
                "BookID":bookdata.id
            },
            function(data){
                $('#tool_book_review').show();
                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.hide();
                }
            }
        );
    }

    function jsVoteReviewHelpful(reviewID,choose)
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                "task":"Add_Book_Review_Helpful",
                "BookID":bookdata.id,
                "ReviewID":reviewID,
                "Choose":choose
            },
            function(data){
                var result = data.split('|=|');
                if(result[0] == '1'){
                    $('#review_log_helpful_'+reviewID).load(
                        '/home/eLearning/ebook_reader/ajax_load.php',
                        {
                            "task":"Get_Tool_Book_Review_Helpful_Vote",
                            "ReviewID":reviewID
                        },
                        function(data){

                        }
                    );
                }
            }
        );
    }

    function ajAddBookReview()
    {
        var rating = $('#tool_book_review_edit li.star_on').length;
        var content = $.trim($('#tool_book_review_text').val());
        if(content == ''){
            alert(jsLang["PleaseInputContent"]);
            return;
        }
        $('#btn_tool_book_review_cancel').attr('disabled',true);
        $('#btn_tool_book_review_done').attr('disabled',true);
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                "task":"Add_Book_Review",
                "BookID":bookdata.id,
                "Rating":rating,
                "Content":encodeURIComponent(content)
            },
            function(data){
                ajGetBookReview();
                $('#tool_book_review_edit').hide('fast');
                $('#tool_book_review_edit li').removeClass('star_on');
                $('#tool_book_review_text').val('');
                $('#btn_tool_book_review_cancel').attr('disabled',false);
                $('#btn_tool_book_review_done').attr('disabled',false);
            }
        );
    }

    function jsStarOn(obj, onoff)
    {
        var that = $(obj);
        var li = that.parent('li');
        if(onoff==1){
            li.prevAll('li').removeClass('star_on').addClass('star_on');
            li.removeClass('star_on').addClass('star_on');
            li.nextAll('li').removeClass('star_on');
        }else{
            li.prevAll('li').removeClass('star_on');
            li.removeClass('star_on');
            li.nextAll('li').removeClass('star_on');
        }
    }

    function next_page_event_handler(){ //eBook-update-130605a
        $("#tool_glossary_paper").hide();
        $('#tool_page_control_container').html('');
        $("#tool_page_related").hide();
        bookdata.searching2 = false;
        $('#tool_voice').hide();
        $('#tool_voice').removeClass('istts');
        $("#btn_return").hide();
        <?php if($ForceToOnePageMode){?>//2013-06-06 CharlesMa
        $(".book_content_container").css("opacity", "100");
        $("#book_structure").css("opacity", "100");
        $("#book_content").css("opacity", "100");
        <?php  } ?>

        if($('#book_table_of_content').is(':visible')){
            if((browsertype=='MSIE' && !bookdata.isImageBook) || bookdata.pageMode==1){
                var dv_slip = $('#book_table_of_content_container').children().eq(0);
                var dv_toc = $('#book_table_of_content_container').children().eq(1);

                // 2013-05-29 eBook-update-130529b
                <?php if($ForceToOnePageMode){?>
                jsLoadDocument(1);
                $('#book_table_of_content').fadeOut('slow');
                $('#book_content').show();
                //2013-06-05 Charles Ma
                dv_toc.hide();
                dv_slip.show();
                $('#btn_prev_page').css('z-index',2);
                <?php }else{ ?>
                if(dv_slip.is(':visible')){
                    dv_toc.show();
                    dv_slip.hide();
                    <?php if($isRTL == 1){?>//20131204-ebook-righttoleft
                    $('#btn_next_page').css('z-index',5);
                    $('#btn_prev_page').css('opacity',0);
                    <?php }else{ ?>
                    $('#btn_prev_page').css('z-index',5);
                    $('#btn_next_page').css('opacity',0);
                    <?php } ?>
                }else{
                    jsLoadDocument(1);
                    $('#book_table_of_content').fadeOut('slow');
                    $('#book_content').show();
                    if(bookdata.isImageBook){
                        $('#book_comic').show();
                        bookdata.book_comic_content_handler();
                    }
                    dv_toc.hide();
                    dv_slip.show();
                    <?php if($isRTL == 1){?>//20131204-ebook-righttoleft
                    $('#btn_next_page').css('z-index',2);
                    $('#btn_prev_page').css('opacity',1);
                    <?php }else{ ?>
                    $('#btn_prev_page').css('z-index',2);
                    $('#btn_next_page').css('opacity',1);
                    <?php } ?>
                }
                <?php }?>
                // 2013-05-29 eBook-update-130529b END

            }else{
                jsLoadDocument(1);
                $('#book_table_of_content').fadeOut('slow');
                $('#book_content').show();
                if(bookdata.isImageBook){
                    $('#book_comic').show();
                    bookdata.book_comic_content_handler();
                }
            }
        }else if((bookdata.currentPage + 1) < bookdata.pages.length){
            if(bookdata.isImageBook){
                if(bookdata.pageMode==1){
                    if((bookdata.currentPage + 1) < bookdata.pages.length){
                        jsLoadDocument(bookdata.currentPage + 1);
                    }
                }else{
                    if((bookdata.currentPage + 2) < bookdata.pages.length){
                        jsLoadDocument(bookdata.currentPage + 2);
                    }
                }
            }else{
                jsLoadDocument(bookdata.currentPage + 1);
            }
        }
        changeBookFormat();
    }

    function prev_page_event_handler(){   //eBook-update-130605a
        $("#tool_glossary_paper").hide();
        $('#tool_page_control_container').html('');
        $("#tool_page_related").hide();
        bookdata.searching2 = false;
        $('#tool_voice').hide();
        $('#tool_voice').removeClass('istts');
        $("#btn_return").hide();
        if(((browsertype=='MSIE' && !bookdata.isImageBook) || bookdata.pageMode==1) && $('#book_table_of_content').is(':visible')){
            <?php if($ForceToOnePageMode){?>  //2013-06-05 Charles Ma
            if(bookdata.currentPage== 1){
                var cover_image_dv = $('#book_cover_image');
                $('.btn_add_bookmarkes').css('display','none');
                cover_image_dv.fadeIn(200, function(){
                    //cover_image_dv.remove();
                    //$('#ebook_transparent_block').remove();
                    $('#book_structure').fadeOut('fast');
                    $('#book_table_of_content').fadeOut('slow');
                    $('#book_page_style > .page_top').css("display", "");
                    $('#book_page_style > .page_content').css("display", "");
                    $('#book_page_style > .page_bottom').css("display", "");

                    var dv_top = $('#tool_top');
                    var dv_bottom = $('#tool_bottom');

                    dv_top.fadeOut('fast');
                    dv_bottom.fadeOut('fast');
                    bookdata.tool_page_number_handler(false,false);
                });
            }
            <?php }?>
            var dv_slip = $('#book_table_of_content_container').children().eq(0);
            var dv_toc = $('#book_table_of_content_container').children().eq(1);
            if(dv_toc.is(':visible')){
                dv_toc.hide();
                dv_slip.show();
                <?php if($isRTL == 1){?>  //20131204-ebook-righttoleft

                $('#btn_next_page').css('z-index',2);
                $('#btn_next_page').css('opacity',0);
                $('#btn_prev_page').css('opacity',1);
                <?php }else {?>
                $('#btn_prev_page').css('z-index',2);
                $('#btn_prev_page').css('opacity',0);
                $('#btn_next_page').css('opacity',1);
                <?php }?>
            }
        }else if((bookdata.currentPage - 1) > 0){
            jsLoadDocument(bookdata.currentPage - 1);
        }else{
            <?php if($ForceToOnePageMode){?>
            $('#book_table_of_content').fadeIn('slow');
            $('#book_content').hide();
            var dv_slip = $('#book_table_of_content_container').children().eq(0);
            var dv_toc = $('#book_table_of_content_container').children().eq(1);
            dv_toc.show();
            dv_slip.hide();
            $('#btn_prev_page').css('z-index',2);
            $('.btn_add_bookmarkes').css('display','none');
            <?php }?>
        }
    }

    function jsAssignEvents()
    {
        var book_tool_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            var dv = $('#tool_book_board');
            var btn = $('#btn_book_tool');
            dv.css({position:'absolute', left: btn.offset().left - btn.width()/2, top: btn.offset().top + btn.height()});
            if(isResize != true){
                if(dv.is(':visible')){
                    dv.hide();
                    $('#tool_book_progress').hide();
                    $('#tool_book_review').hide();
                    $('#tool_book_recommend').hide();
                }else{
                    dv.show();
                }
            }
        };

        var book_setting_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            var dv = $('#tool_setting_board');
            var btn = $('#btn_book_setting');
            dv.css({position:'absolute', left: btn.offset().left - btn.width()/2, top: btn.offset().top + btn.height()});
            if(isResize != true){
                if(dv.is(':visible')){
                    dv.hide();
                }else{
                    dv.show();
                }
            }
        };

        //2013-10-17 Charles Ma
        var book_enlarge_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            isZoom = true;
            scale = (parseFloat($('body').css('zoom'))  + 0.1)* 100;
            scale = parseInt(Math.ceil(scale));

            if(scale > (initial_scale*1.3)){
                $("#btn_book_enlarge").css("opacity", "0.5");
                return;
            }else{
                $("#btn_book_enlarge").css("opacity", "1");
            }

            excess = scale % 10;
            if(bookdata['pageMode'] == 1){
                scale = scale - excess;
            }else{
                scale = scale - excess;
            }

            $('body').css('zoom', scale + '%');
            $('#tool_page_num').css('zoom', scale + '%');
            scale= scale / 100;


            $("#btn_book_diminish").css("opacity", "1");
            var dv = $('#tool_page_select a');
            var dvp = $('#tool_page_num');
            var dvb = $('#btn_bottom_tool_bar');

            if(dv.length > 0){
                //Charles Ma 2013-10-17
                scale = isZoom ? parseFloat($('body').css('zoom')) : 1;


                <?php if($ForceToOnePageMode){?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale, top: 0.72 *scale * 100 + "%"});
                var dv_ref = $('#book_structure');
                $('#book_table_of_content').css({
                                                    left: parseInt(dv_ref.offset().left)
                                                });
                $('#btn_prev_page').css({left: dv_ref.offset().left});
                $('#btn_next_page').css({left: dv_ref.offset().left+dv_ref.width()-$('#btn_next_page').width()+20});
                <?php }else{?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale, top: "84%"});
                <?php }?>
                //Charles Ma 2013-10-17 END
            }

        };

        //2013-10-17 Charles Ma END

        //2013-10-17 Charles Ma
        var book_diminish_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            isZoom = true;
            scale = (parseFloat($('body').css('zoom'))  - 0.1)* 100;
            scale = parseInt(scale);

            if(scale <(initial_scale*0.8)){
                $("#btn_book_diminish").css("opacity", "0.5");
                return;
            }else{
                $("#btn_book_diminish").css("opacity", "1");
            }
            $("#btn_book_enlarge").css("opacity", "1");

            excess = scale % 10;
            if(bookdata['pageMode'] == 1){
                scale = scale - excess;
            }else{
                scale = scale - excess;
            }

            $('body').css('zoom', scale + '%');
            $('#tool_page_num').css('zoom', scale + '%');

            scale= scale / 100;

            var dv = $('#tool_page_select a');
            var dvp = $('#tool_page_num');
            var dvb = $('#btn_bottom_tool_bar');
            if(dv.length > 0){
                //Charles Ma 2013-10-17
                scale = isZoom ? parseFloat($('body').css('zoom')) : 1;
                <?php if($ForceToOnePageMode){?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale, top: 0.72 *scale * 100 + "%"});
                var dv_ref = $('#book_structure');
                $('#book_table_of_content').css({
                                                    left: parseInt(dv_ref.offset().left)
                                                });
                $('#btn_prev_page').css({left: dv_ref.offset().left});
                $('#btn_next_page').css({left: dv_ref.offset().left+dv_ref.width()-$('#btn_next_page').width()+20});
                <?php }else{?>
                dvp.css({position:'absolute', left: (dv.offset().left - dvp.width()/2 + dv.width()/2)/scale, top: "84%"});
                <?php }?>
                //Charles Ma 2013-10-17 END
            }
        };

        //2013-10-17 Charles Ma END

        var add_notes_left_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            var dvs = $('#book_structure');
            var dvp = $('#tool_notes_paper');
            var posx = dvs.offset().left + dvs.width()/2 - dvp.width()/2;
            var posy = dvs.offset().top + dvs.height()/2 - dvp.height()/2;
            dvp.css({position:'absolute', left: posx, top: posy});

            if(isResize != true){
                if(dvp.is(':visible')){
                    dvp.hide();
                }else{
                    jsBlockUI();
                    if(bookdata.pageMode == 2){
                        if(bookdata.isImageBook){
                            var page_num = bookdata.currentPage;
                        }else{
                            var page_num = bookdata.currentPage * 2 - 1;
                        }
                    }else{
                        var page_num = bookdata.currentPage;
                    }
                    //var page_num = bookdata.pageMode == 2? (bookdata.currentPage * 2 - 1 ): bookdata.currentPage;
                    dvp.find('input#notes_pager_page_number').val(page_num);
                    ajGetBookNotes(page_num);
                }
            }
        };

        var add_notes_right_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            var dvs = $('#book_structure');
            var dvp = $('#tool_notes_paper');
            var posx = dvs.offset().left + dvs.width()/2 - dvp.width()/2;
            var posy = dvs.offset().top + dvs.height()/2 - dvp.height()/2;
            dvp.css({position:'absolute', left: posx, top: posy});

            if(isResize != true){
                if(dvp.is(':visible')){
                    dvp.hide();
                }else{
                    jsBlockUI();
                    if(bookdata.pageMode == 2){
                        if(bookdata.isImageBook){
                            var page_num = bookdata.currentPage + 1;
                        }else{
                            var page_num = bookdata.currentPage * 2;
                        }
                    }else{
                        var page_num = bookdata.currentPage;
                    }
                    //var page_num = bookdata.pageMode == 2? (bookdata.currentPage * 2 ): bookdata.currentPage;
                    dvp.find('input#notes_pager_page_number').val(page_num);
                    ajGetBookNotes(page_num);
                }
            }
        };

        $('#btn_notes_pager_close').mouseup(function(){
            $('#tool_notes_paper').hide();
            jsUnblockUI();
        });
        $('#btn_notes_pager_delete').mouseup(function(){
            var page_num = $('#tool_notes_paper').find('input#notes_pager_page_number').val();
            if(confirm(jsLang["ConfirmDeleteNotes"])){
                $('#notes_pager_notes').val('');
                $('#notes_pager_date').html('');
                ajDeleteBookNotes(page_num);
                $('#tool_notes_paper').hide();
                jsUnblockUI();
            }
        });
        $('#btn_notes_pager_done').mouseup(function(){
            var page_num = $('#tool_notes_paper').find('input#notes_pager_page_number').val();
            var notes = $('#notes_pager_notes').val();
            ajUpdateBookNotes(page_num, notes);
            $('#tool_notes_paper').hide();
            jsUnblockUI();
        });

        var search_handler = function(isResize){
            $('*').qtip('hide');
            bookdata.searching2 = false;
            $("#tool_page_related").hide();

            var dv = $('#tool_page_search');
            var btn = $('#btn_search');
            dv.css({position:'absolute', left: btn.offset().left - dv.width() + btn.width(), top: btn.offset().top - dv.height()});
            if(isResize != true){
                if(dv.is(':visible')){
                    dv.hide();
                }else{
                    dv.show();
                }
            }
        };

        var texttospeech_handler = function(isResize){
            var dv = $('#tool_voice');
            var btn = $('#btn_texttospeech');
            dv.css({position:'absolute', left: btn.offset().left, top: btn.offset().top + btn.height()});
            if(isResize != true){
                var txt = jsGetSelection();
                jsLoadTextToSpeech(txt);
            }
        };
        var current_texttospeech_txt = "";
        var highlight_texttospeech_handler = function(isResize){
            var dv = $('#tool_voice');
            var btn = $('#btn_highlight_texttospeech');
            current_tts_page = 0;
            if(btn.is(':visible') && typeof(bookdata.prevHighlightBtn)!= 'undefined' && dv.css("display") == "none"){
                dv.css({position:'absolute', left: btn.offset().left, top: btn.offset().top + btn.height()});
                var uid = bookdata.prevHighlightBtn.attr('id').replace('hightlight_','');
                var highlightObj = bookdata.findHighlightByUniqueID(uid);

                $('#tool_voice').show();
                if(isResize != true && current_texttospeech_txt != highlightObj.text){
                    current_texttospeech_txt = highlightObj.text;
                    var txt = highlightObj.text;
                    jsLoadTextToSpeech(txt);
                }

            }else{ //2013-03-25 eBook-update-130325a
//      jsLoadTextToSpeech(" ");
                dv.slideUp('fast');
                $('#tool_voice').hide();
                $('#tool_voice').removeClass('istts');
            }
        };

        bookdata.toc_handler = function(isResize){
            $('*').qtip('hide');
            $("#tool_page_related").hide();
            bookdata.searching2 = false;

            var dv = $('#book_table_of_content');
            var dvc = $('#book_content');

            if(isResize != true && dv.length == 0){
                jsLoadTOC();
            }else{
                <?php if($ForceToOnePageMode){?>
                $('#book_table_of_content').css({
                                                    left: '14%'         //2013-06-20
                                                });
                $('.btn_add_bookmarkes').css('display','none');
                <?php }else{ ?>
                if(bookdata['pageMode'] == 1){
                    $('#book_table_of_content').css({
                                                        left: '23%'         //2013-06-20
                                                    });
                }else{
                    $('#book_table_of_content').css({
                                                        left: '13%'         //2013-06-20
                                                    });
                }
                <?php } ?>
                $('#book_table_of_content').css({
                                                    left: $('#book_structure').offset().left,
                                                    top: $('#book_structure').offset().top
                                                });
                if(isResize != true){
                    if(dv.is(':visible')){
                        dv.fadeOut('slow');
                        $('#book_content_main').css('opacity', 1);
                        dvc.show();
                        if(bookdata.isImageBook){
                            $('#book_comic').show();
                        }
                        jsUpdatePageNumber(bookdata.pageMode);
                    }else{
                        if (bookdata.pageMode == 1){
                            $('#book_table_of_content').find('.book_info_detail_page').css('display', 'none');
                            $('#book_table_of_content').find('.book_toc_list_page').css('display', 'block');
                        }
                        dv.fadeIn('slow');
                        $('#book_content_main').stop();
                        $('#book_content_main').css('opacity', 0);
                        dvc.hide();
                        if(bookdata.isImageBook){
                            $('#book_comic').hide();
                        }
                        $('#bookmark_img_left').hide();
                        if(bookdata.pageMode == 2) $('#bookmark_img_right').hide();
                        <?php if($isRTL == 1){?> //20131204-ebook-righttoleft
                        $('#btn_prev_page').css('z-index',5);
                        $('#btn_next_page').css('z-index',5);
                        if(bookdata.pageMode == 2)
                            $('#btn_next_page').css('opacity',0);
                        else
                            $('#btn_next_page').css('opacity',1);
                        <?php }else{?>
                        $('#btn_next_page').css('z-index',5);
                        $('#btn_prev_page').css('z-index',5);
                        if(bookdata.pageMode == 2)
                            $('#btn_prev_page').css('opacity',0);
                        else
                            $('#btn_prev_page').css('opacity',1);
                        <?php } ?>
                        hideAudioBtn();
                    }
                }
            }
            <?php if($ForceToOnePageMode){?>
            dvc.fadeIn('slow');   // Charles Ma 2013-1-31
            $('.btn_add_bookmarkes').css('display','none');
            <?php } ?>
            $('#tool_highlight_board').hide();
        };

        bookdata.tool_bookmark_list_handler = function(isResize){
            var dvb = $('#tool_bookmark_list');
            var dvs = $('#book_structure');

            if(isResize != true){
                if(dvb.is(':visible')){
                    dvb.hide();
                    jsUnblockUI();
                }else{
                    jsBlockUI();
                    ajGetBookmarkLogList();
                    //dvb.show();
                }
            }
            $('#tool_highlight_board').hide();

            var posx = dvs.offset().left + dvs.width()/2 - dvb.width()/2;
            var posy = dvs.offset().top + dvs.height()/2 - dvb.height()/2;
            dvb.css({position:'absolute', left: posx, top: posy});
        };

        bookdata.tool_my_notes_handler = function(isResize){

            var dvn = $('#tool_my_notes');
            var dvs = $('#book_structure');

            if(isResize != true){
                if(dvn.is(':visible')){
                    dvn.hide();
                    jsUnblockUI();
                }else{
                    jsBlockUI();
                    ajGetMyNotesLogEntry( $(this).attr("id"));
                }
            }
            $('#tool_highlight_board').hide();

            var posx = dvs.offset().left + dvs.width()/2 - dvn.width()/2;
            var posy = dvs.offset().top + dvs.height()/2 - dvn.height()/2;
            dvn.css({position:'absolute', left: posx, top: posy});
        };



        var tool_book_progress_handler = function(isResize){
            var dvp = $('#tool_book_progress');
            var btn = $('#btn_my_progress');

            if(isResize != true){
                if(dvp.is(':visible')){
                    dvp.hide();
                }else{
                    dvp.show();
                    $('#tool_book_review').hide();
                    $('#tool_book_recommend').hide();
                }
            }
            $('#tool_highlight_board').hide();

            var posx = btn.offset().left - btn.width()/2;
            var posy = btn.offset().top + btn.height();
            dvp.css({position:'absolute', left: posx, top: posy});
        };

        var tool_book_recommend_handler = function(isResize){
            var dvr = $('#tool_book_recommend');
            var btn = $('#btn_send_recommend');

            if(isResize != true){
                if(dvr.is(':visible')){
                    dvr.hide();
                }else{
                    //$('#tool_book_recommend_text').val(window.location.href);     //2012-11-15 Charles Ma - remove window.location.href
                    $('#tool_book_recommend_text').val('');
                    dvr.show();
                    $('#tool_book_progress').hide();
                    $('#tool_book_review').hide();
                }
            }
            $('#tool_highlight_board').hide();

            var posx = btn.offset().left - btn.width()/2;
            var posy = btn.offset().top + btn.height();
            dvr.css({position:'absolute', left: posx, top: posy});
        };

        var tool_book_review_handler = function(isResize){
            var dvr = $('#tool_book_review');
            var btn = $('#btn_book_review');

            if(isResize != true){
                if(dvr.is(':visible')){
                    dvr.hide();
                    $('#tool_book_review_edit').hide();
                }else{
                    //dvr.show();
                    ajGetBookReview();
                    $('#tool_book_progress').hide();
                    $('#tool_book_recommend').hide();
                }
            }
            $('#tool_highlight_board').hide();

            var posx = btn.offset().left - btn.width()/2;
            var posy = btn.offset().top + btn.height();
            dvr.css({position:'absolute', left: posx, top: posy});
        };

        var tool_book_review_edit_handler = function(isResize){
            var dvr = $('#tool_book_review_edit');
            var btn = $('#btn_review_call_writer');

            if(isResize != true){
                if(dvr.is(':visible')){
                    dvr.hide();
                }else{
                    dvr.show();
                }
            }
            $('#tool_highlight_board').hide();

            var posx = btn.offset().left - btn.width()/2;
            var posy = btn.offset().top + btn.height();
            dvr.css({position:'absolute', left: posx, top: posy});
        };

        var resize_handler = function(){
            <?php if(!$ForceToOnePageMode){?>
            if(is_loading_finish) return;
            <?php }?>

            var dv_ref = $('#book_structure');
            $('#btn_prev_page').css({opacity:0,
                                        position:'absolute',
                                        left: dv_ref.offset().left,
                                        //top : dv_ref.offset().top + dv_ref.height() - $('#btn_prev_page').height()
                                        top : dv_ref.offset().top,
                                        height : dv_ref.height()
                                    });
            $('#btn_next_page').css({opacity:0,
                                        position:'absolute',
                                        //left: dv_ref.offset().left+dv_ref.width()-$('#btn_next_page').width(),
                                        //top : dv_ref.offset().top + dv_ref.height() - $('#btn_next_page').height()
                                        left: dv_ref.offset().left+dv_ref.width()-$('#btn_next_page').width()+20,
                                        top : dv_ref.offset().top,
                                        height : dv_ref.height()
                                    });

            $("#btn_prev_page").css("opacity", "100");
            $("#btn_next_page").css("opacity", "100");
            <?php if(!$ForceToOnePageMode){?>  //2013-07-03 Charles Ma
            $('#btn_prev_page').css({
                                        top : parseInt($('#btn_next_page').css("top")) + 4
                                    });
            $('#btn_next_page').css({
                                        left: parseInt($('#btn_next_page').css("left")) + 15,
                                        top : parseInt($('#btn_next_page').css("top")) + 4
                                    });
            <?php }?>

            $('#btn_top_tool_bar').css({
                                           opacity:0,
                                           position:'absolute',
                                           left: dv_ref.offset().left,
                                           top: dv_ref.offset().top,
                                           width: dv_ref.width(),
                                           height: dv_ref.height()/25
                                       });

            $('#btn_bottom_tool_bar').css({
                                              opacity:0,
                                              position:'absolute',
                                              left: dv_ref.offset().left,
                                              top: dv_ref.offset().top + dv_ref.height() - dv_ref.height()/12,
                                              width: dv_ref.width(),
                                              height: dv_ref.height()/12
                                          });

            book_tool_handler(true);
            book_setting_handler(true);
            add_notes_left_handler(true);
            if(bookdata.pageMode==2) add_notes_right_handler(true);
            search_handler(true);
            texttospeech_handler(true);
            highlight_texttospeech_handler(true);
            bookdata.toc_handler(true);
            bookdata.tool_page_number_handler(true);
            bookdata.tool_highlight_board_handler();

            bookdata.highlight_notes_handler(true);
            bookdata.bookmark_image_handler(true);

            bookdata.tool_bookmark_list_handler(true);
            bookdata.tool_my_notes_handler(true);

            tool_book_recommend_handler(true);
            tool_book_progress_handler(true);
            tool_book_review_handler(true);
            tool_book_review_edit_handler(true);

            bookdata.cover_image_handler();
            bookdata.book_comic_content_handler();

            var blockui = $('#blockUI');
            if(blockui.length > 0){
                // 2013-06-20 Charles Ma
                //$('#blockUI').css({left: dv_ref.offset().left, top: dv_ref.offset().top, width: dv_ref.width(), height: dv_ref.height()});

                var blockui_loadingtext = $('#blockUI .loadingText');
                if(blockui_loadingtext.length > 0){
                    blockui_loadingtext.offset({left: dv_ref.offset().left + dv_ref.width()/2 - blockui_loadingtext.width()/2, top: dv_ref.offset().top + dv_ref.height()/2 - blockui_loadingtext.height()/2});
                }
            }

            bookdata.repositionHighlights();
        };
        resize_handler();

        <?php if($isRTL){?>    //20131204-ebook-righttoleft
        $('#btn_next_page').live("mouseup",prev_page_event_handler);
        $('#btn_prev_page').live("mouseup", next_page_event_handler);
        <?php }else{?>
        $('#btn_prev_page').live("mouseup",prev_page_event_handler);
        $('#btn_next_page').live("mouseup", next_page_event_handler);
        <?php }?>

        $('#btn_book_tool').mouseup(book_tool_handler);
        $('#btn_book_setting').mouseup(book_setting_handler);
        $('#btn_add_notes_left').mouseup(add_notes_left_handler);
        if(bookdata.pageMode == 2) $('#btn_add_notes_right').mouseup(add_notes_right_handler);
        $('#btn_search').mouseup(search_handler);
        $('#btn_toc').mouseup(bookdata.toc_handler);
        //$('#btn_texttospeech').mouseup(texttospeech_handler);
        $('#btn_texttospeech').hide();

        // 20130110-mp3  Charles Ma
        $('#btn_mp3').mouseup();

        //2013-10-17 Charles Ma
        $('#btn_book_enlarge').mouseup(book_enlarge_handler);
        $('#btn_book_diminish').mouseup(book_diminish_handler);

        //2013-10-17 Charles Ma END

        disableDefaultKeywordSearch(); // 2013-3-6 Charles Ma

        var toggle_toolbar_handler = function(){
            var dv_top = $('#tool_top');
            var dv_bottom = $('#tool_bottom');

            if(dv_top.is(':visible')){
                dv_top.slideUp('fast');
                dv_bottom.slideUp('fast');
                bookdata.tool_page_number_handler(false,false);
            }else{
                dv_top.slideDown('fast');
                dv_bottom.slideDown('fast');
                bookdata.tool_page_number_handler(false,true);
            }
        };

        $('#btn_top_tool_bar').mouseup(toggle_toolbar_handler);
        $('#btn_bottom_tool_bar').mouseup(toggle_toolbar_handler);

        $('#btn_book_close').mouseup(function(){
            //window.close();
            //if($('#tool_top').is(':visible')){
            $('#tool_book_board').hide();
            $('#tool_setting_board').hide();
            $('#tool_notes_book').hide();
            $('#tool_page_search').hide();
            $('#tool_voice').hide();
            $('#tool_voice').removeClass('istts');
//      $('#tool_page_control_container').html(''); // remove audio control
            $('#tool_book_progress').hide();
            $('#tool_book_review').hide();
            $('#tool_book_review_edit').hide();
            $('#tool_book_recommend').hide();
//      bookdata.searching2 = false;
//	$("#tool_page_related").hide();
            //}
            toggle_toolbar_handler();
        });

        for(i=1;i<=7;i++){
            $('.btn_hightlight_0' + i).mouseup(bookdata.highlight_change_color_handler);
        }
        $('.btn_clear_highlight').mouseup(bookdata.removeHighlight);
        $('#btn_add_highlight_notes').mouseup(bookdata.highlight_notes_handler);

        $('#btn_highlight_notes_pager_close').mouseup(function(){
            $('#tool_highlight_notes_paper').hide();
            jsUnblockUI();
        });
        $('#btn_highlight_notes_pager_delete').mouseup(function(){
            var highlightObj = bookdata.prevHighlightObj;
            if(typeof(highlightObj) != 'undefined'){
                if(confirm(jsLang["ConfirmDeleteNotes"])){
                    highlightObj['notes'] = '';
                    $('#highlight_notes_pager_notes').val('');
                    highlightObj['dateModify'] = (new Date).getTime();
                    ajAddHighlight(highlightObj);
                    var note_btn = $('#note_' + highlightObj['uniqueID']);
                    if(note_btn.length > 0){
                        note_btn.remove();
                    }
                    $('#tool_highlight_notes_paper').hide();
                    jsUnblockUI();
                }
            }
        });
        $('#btn_highlight_notes_pager_done').mouseup(function(){
            var highlightObj = bookdata.prevHighlightObj;
            if(typeof(highlightObj) != 'undefined'){
                highlightObj['notes'] = $('#highlight_notes_pager_notes').val();
                highlightObj['dateModify'] = (new Date).getTime();
                ajAddHighlight(highlightObj);
                var highlight_span = $('#hightlight_' + highlightObj['uniqueID']);
                if(highlight_span.length > 0){
                    var note_btn = $('#note_' + highlightObj['uniqueID']);
                    if(note_btn.length == 0){
                        var note_btn_html = '<span id="note_'+highlightObj['uniqueID']+'" class="btn_notes" title="Notes"></span>'; // 20141017
                        //highlight_span.after(note_btn_html);	//20141020
                        bookdata.repositionHighlights();
                        var note_btn_elem = $('#note_' + highlightObj['uniqueID']);
                        if(note_btn_elem.length > 0){
                            note_btn_elem.bind("mouseup",bookdata.highlight_notes_icon_handler);
                        }
                    }
                }
                $('#tool_highlight_notes_paper').hide();
                jsUnblockUI();
            }
        });
        $('#btn_highlight_texttospeech').mouseup(highlight_texttospeech_handler);

        bookdata.bookmark_image_left_onclick_handler = function(){
            var that = $(this);
            var img_left = $('#bookmark_img_left');
            var page_num = bookdata.isImageBook||(bookdata.pageMode == 1)? bookdata.currentPage:(bookdata.currentPage*2 - 1);//2013-06-10 Charles Ma
            var num_index = bookdata.bookmarks.indexOf(page_num);
            if(num_index >=0){
                if(that.attr('id') != 'btn_bookmark_left'){ // need to click on image icon to delete bookmark
                    bookdata.bookmarks.splice(num_index,1);
                    ajDeleteBookmark(page_num);
                }
            }else{
                if(that.attr('id') != 'bookmark_img_left'){ // need to click on btn to add bookmark
                    bookdata.bookmarks.push(page_num);
                    ajAddBookmark(page_num);
                }
            }

            bookdata.bookmark_image_toggle_handler('bookmark_img_left',page_num);
        };

        bookdata.bookmark_image_right_onclick_handler = function(){
            var that = $(this);
            var img_right = $('#bookmark_img_right');
            var page_num = bookdata.isImageBook? bookdata.currentPage+1 : bookdata.currentPage*2;
            var num_index = bookdata.bookmarks.indexOf(page_num);
            if(num_index >=0){
                if(that.attr('id') != 'btn_bookmark_right'){ // need to click on image icon to delete bookmark
                    bookdata.bookmarks.splice(num_index,1);
                    ajDeleteBookmark(page_num);
                }
            }else{
                if(that.attr('id') != 'bookmark_img_right'){ // need to click on btn to add bookmark
                    bookdata.bookmarks.push(page_num);
                    ajAddBookmark(page_num);
                }
            }

            bookdata.bookmark_image_toggle_handler('bookmark_img_right',page_num);
        };

        if(bookdata.pageMode == 2){
            $('#btn_bookmark_left').mouseup(bookdata.bookmark_image_left_onclick_handler);
            $('#btn_bookmark_right').mouseup(bookdata.bookmark_image_right_onclick_handler);

            $('#bookmark_img_left').mouseup(bookdata.bookmark_image_left_onclick_handler);
            $('#bookmark_img_right').mouseup(bookdata.bookmark_image_right_onclick_handler);
        }else{
            $('#btn_bookmark_left').mouseup(bookdata.bookmark_image_left_onclick_handler);
            $('#bookmark_img_left').mouseup(bookdata.bookmark_image_left_onclick_handler);
        }

        $('#btn_tool_bookmark_list_close').mouseup(function(e){
            $('#tool_bookmark_list').hide();
            jsUnblockUI();
        });
        $('#btn_my_notes_close').mouseup(function(e){
            $('#tool_my_notes').hide();
            jsUnblockUI();
        });
        $('#btn_my_bookmarks').mouseup(bookdata.tool_bookmark_list_handler);
        $('#btn_my_notes').mouseup(bookdata.tool_my_notes_handler);

        $('#btn_tool_book_progress_done').mouseup(bookdata.tool_progress_update_handler);
        $('#btn_my_progress').mouseup(tool_book_progress_handler);
        $('#btn_book_review').mouseup(tool_book_review_handler);
        $('#btn_send_recommend').mouseup(tool_book_recommend_handler);
        $('#btn_recommend_send').mouseup(bookdata.tool_recommend_send_handler);
        $('#btn_review_call_writer').mouseup(tool_book_review_edit_handler);
        $('#btn_tool_book_review_done').mouseup(function(e){
            ajAddBookReview();
        });
        $('#btn_tool_book_review_cancel').mouseup(function(e){
            $('#tool_book_review_edit li').removeClass('star_on');
            $('#tool_book_review_text').val('');
            $('#tool_book_review_edit').hide('fast');
        });


        $('#btn_tool_book_review_close').mouseup(tool_book_review_handler);   // 2012-11-15 Charles Ma
        $('#btn_recommend_cancel').mouseup(tool_book_recommend_handler);  // 2012-11-15 Charles Ma
        $('#btn_tool_book_progress_cancel').mouseup(tool_book_progress_handler);  // 2012-11-15 Charles Ma


        $(window).resize(resize_handler);

        if(bookdata.isImageBook){
            $('#btn_texttospeech').hide();
            //$('#btn_my_notes').hide();
            //$('#btn_search').hide(); //Charles Ma 2013-06-24
        }

        if($('#btn_do_search').length > 0){
            bookdata.searching = false;
            bookdata.searchingFile = '';
            bookdata.searchingPageIndex = 0;
            bookdata.searchingKeyword = '';
            bookdata.searchingCount = 0;
            $('#search_result_total').hide();

            //2013-10-17 Charles Ma

            $('#search_keyword').keyup(function(e){
                if (e.keyCode == 13) {
                    $('#btn_do_search').mouseup();
                }
            });

            //2013-10-17 Charles Ma END


        }

        $(".tool_board_content_bg a").bind("click touch", function(){
            if($('#tool_voice').is(':visible')){
//	    $('#tool_page_control_container').html('');
                $('#tool_voice').hide();
                $('#tool_voice').removeClass('istts');
            }
        })

        $('#btn_do_search').mouseup(function(e){
            if(bookdata.searching){ // stop search
                bookdata.searching = false;
                $(this).val(jsLang["Search"]);
            }else{
                bookdata.searchingKeyword = $.trim($('input#search_keyword').val());
                if(bookdata.searchingKeyword == ''){
                    alert(jsLang["PleaseInputSearchKeyword"]);
                    return;
                }
                // start search
                bookdata.searching = true;
                bookdata.searchingFile = '';
                bookdata.searchingPageIndex = 0;
                bookdata.searchingCount = 0;
                $(this).val(jsLang["Stop"]);

                //$('#search_result_total').html(jsLang["ResultsFound"].replace('<!--NUMBER-->','0')).show(); // 2013-07-15 result found
                $('#search_result_total').html('Searching...').show();

                $('div.search_result ul').html('');

                // do search
                jsDoSearch();
            }
        });

        var trial_msg_handler = function(e){
            alert(jsLang["FeatureNotAvailableForTrial"]);
        };

    }

    function shortenSearchResult(paragraph, keyword){
        var keyword_pos = new Array();
        var current_pos = 0;
        var test = 0;
        var paragraph_length = paragraph.length;

        var search_width = keyword.length > 15 ? keyword.length : 15;
        t_paragraph = paragraph.toLowerCase();
        t_keyword = keyword.toLowerCase();

        while(t_paragraph.indexOf(t_keyword, current_pos) >=0 ){ // || paragraph.indexOf(keyword.toUpperCase(), current_pos) >=0
            if(t_paragraph.indexOf(t_keyword, current_pos) >=0){
                keyword_pos.push(t_paragraph.indexOf(t_keyword, current_pos));
                current_pos = t_paragraph.indexOf(t_keyword, current_pos)+1;
            }else{
                keyword_pos.push(t_paragraph.indexOf(t_keyword.toUpperCase(), current_pos));
                current_pos = t_paragraph.indexOf(t_keyword.toUpperCase(), current_pos)+1;
            }
            test ++;
            if(test > 100) break;
        }


        var start_pos = 0, end_pos = 0;
        var sh_paragraph = "", temp = "";
        var pre = "", post = "";
        for(var index = 0;  index < keyword_pos.length; index ++){
            pre = "", post = "";
            if(index == 0)  start_pos = 0;
            else start_pos = keyword_pos[index-1];

            if(index+1 == keyword_pos.length)end_pos = paragraph_length;
            else end_pos = keyword_pos[index+1];

            if(keyword_pos[index]-search_width > start_pos){
                start_pos = keyword_pos[index]-search_width;
                pre = "...";
            }
            if(keyword_pos[index]+search_width < end_pos ){
                end_pos = keyword_pos[index]+search_width;
                if(index+1 == keyword_pos.length)
                    post = "..." ;
            }
            sh_paragraph = sh_paragraph + pre + paragraph.substring(start_pos,end_pos) + post;
        }
        return sh_paragraph;
    }

    function appendSearchResult(search_result){
        var tag = '<li><a href="javascript:jsToPageFromSearch(<!--PAGE_NUMBER-->);"><h1><span class="search_result_chapter"><!--CHAPTER--></span>';
        tag+= '<span class="page_num">'+jsLang["Page"]+' <!--PAGE_NUMBER--></span><p class="spacer"></p></h1>';
        tag+= '<span class="search_result_content"><!--CONTENT--></span></a><p class="spacer"></p></li>';
        var ul =  $('div.search_result ul');
        var span_total = $('span#search_result_total');
        var count = 0;
        for(var page_num in search_result){
            //var text = decodeURIComponent(search_result[page_num]);
            var text = search_result[page_num];

            var re = new RegExp(bookdata.searchingKeyword.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$1'),"gi");

            text = shortenSearchResult(text, bookdata.searchingKeyword);

            text = text.replace(re,'<strong>$&</strong>');
            var tag_copy = tag + '';
            bookdata.searchingCount += 1;
            tag_copy = tag_copy.replace(/<!--CHAPTER-->/g,current_chapter);
            tag_copy = tag_copy.replace(/<!--PAGE_NUMBER-->/g,page_num);
            tag_copy = tag_copy.replace('<!--CONTENT-->',text);
            ul.append(tag_copy);
        }

        if(bookdata.searchingCount > 0) // 2013-07-15 result found
            span_total.html(jsLang["ResultsFound"].replace('<!--NUMBER-->',bookdata.searchingCount));
        else span_total.html('0 results found.'); // 2013-10-17 Charles Ma

        bookdata.searching = false;
        $('#btn_do_search').val(jsLang["Search"]);
    }

    function jsDoSearchAdvanced()
    {
        bookdata.searchingPageIndex2 += 1;
        while(typeof(bookdata.pages[bookdata.searchingPageIndex2])!='undefined' && bookdata.pages[bookdata.searchingPageIndex2]['session'] != '1'){
            bookdata.searchingPageIndex2 += 1;
        }
        if(bookdata.searching2 && bookdata.searchingPageIndex2 < bookdata.pages.length && typeof(bookdata.pages[bookdata.searchingPageIndex2])!='undefined'){
            bookdata.searchingFile2 = bookdata.pages[bookdata.searchingPageIndex2]['href'];
            document.getElementById('search_frame2').src = '/home/eLearning/ebook_search/' + bookdata.id + '/search/query&filename=' + bookdata.searchingFile2 + '&PageMode=' + bookdata.pageMode + '&keyword=' + encodeURIComponent(bookdata.searchingKeyword2);
        }else{
            bookdata.searching2 = false;
            bookdata.searchingFile2 = '';
            bookdata.searchingPageIndex2 = 0;
            bookdata.searchingKeyword2 = '';
            bookdata.searchingCount2 = 0;
        }
    }

    function jsDoSearch()
    {
        if(bookdata.inputFormat == 4){
            var search_keyword = $("#search_keyword").val();

            $.post(
                '/home/eLearning/ebook_reader/ajax_load.php',
                {
                    'task': 'Search_Keyword',
                    'BookID': bookdata.id,
                    'KeyWord' : search_keyword
                },function(data){
                    //var search_result = eval('(' + decodeURIComponent(data) + ')');
                    eval("var search_result = " + decodeURIComponent(data) + ";");
                    appendSearchResult(search_result);
                }
            );

            return;
        }
        bookdata.searchingPageIndex += 1;
        while(typeof(bookdata.pages[bookdata.searchingPageIndex])!='undefined' && bookdata.pages[bookdata.searchingPageIndex]['session'] != '1'){
            bookdata.searchingPageIndex += 1;
        }
        if(bookdata.searching && bookdata.searchingPageIndex < bookdata.pages.length && typeof(bookdata.pages[bookdata.searchingPageIndex])!='undefined'){
            bookdata.searchingFile = bookdata.pages[bookdata.searchingPageIndex]['href'];
            document.getElementById('search_frame').src = '/home/eLearning/ebook_search/' + bookdata.id + '/search/query&filename=' + bookdata.searchingFile + '&PageMode=' + bookdata.pageMode + '&keyword=' + encodeURIComponent(bookdata.searchingKeyword);
        }else{
            bookdata.searching = false;
            bookdata.searchingFile = '';
            bookdata.searchingPageIndex = 0;
            bookdata.searchingKeyword = '';
            bookdata.searchingCount = 0;
            $('#btn_do_search').val(jsLang["Search"]);
        }
    }

    function page_to_chapter(page_num){
        <? if($engclassicMode){?>
        if(bookdata.pages[page_num] && bookdata.pageMode == 1){
            if(toc_list[toc2_list.indexOf(bookdata.pages[page_num]["href"])]){
                var chapter_name = toc_list[toc2_list.indexOf(bookdata.pages[page_num]["href"])];
            }else{
                var chapter_name = "";
            }
        }else if (bookdata.pages[Math.ceil(page_num/2)] && bookdata.pageMode == 2){
            if(toc_list[toc2_list.indexOf(bookdata.pages[page_num]["href"])]){
                var chapter_name = toc_list[toc2_list.indexOf(bookdata.pages[Math.ceil(page_num/2)]["href"])];
            }else{
                var chapter_name = "";
            }
        }else{
            var chapter_name = "";
        }
        return chapter_name;
        <?}else{?>
        return "";
        <?}?>
    }

    function jsSearchCallback(pages)
    {
        if(bookdata.searching2){
            var tag = '<li><a href="javascript:jsToPageFromSearch(<!--PAGE_NUMBER-->);">';

            <? if ($engclassicMode){ ?>
            tag+= '<h1><span class="related_result_chapter"><!--CHAPTER--></span><span class="page_num"> Page <!--PAGE_NUMBER--></span>';
            <? }else{?>
            tag+= '<h1><span class="related_result_chapter"><!--CHAPTER--></span><span class="page_num"> '+jsLang["Page"]+' <!--PAGE_NUMBER--></span>';
            <?}?>

            tag+= '  <p class="spacer" style="opacity: 1;"></p></h1>';
            tag+= '<span class="search_result_content"><!--CONTENT--></span></a><p class="spacer" style="opacity: 1;"></p></li>';

            var ul =  $('div.related_result ul');
            var span_total = $('#related_result_num');
            for(var i=0;i<pages.length;i++){
                var current_chapter_item = pages[i]['page'][1] != "exact" ?  bookdata.pages[pages[i]['page']] :  bookdata.pages[pages[i]['page'][0]];
                var current_chapter_index = jQuery.inArray(current_chapter_item["href"], toc2_list);
                current_chapter_index = current_chapter_index == -1 ? 100 : current_chapter_index;
                current_chapter = current_chapter_index != 100 ? toc_list[current_chapter_index] : "Others";

                var page_num = pages[i]['page'][1] != "exact" ? jsPageIndexToPageNumber(bookdata.searchingPageIndex2) + pages[i]['page'] : pages[i]['page'][0];

                var text = decodeURIComponent(pages[i]['text']);
                var re = new RegExp(bookdata.searchingKeyword2.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$1'),"gi");

                current_chapter = page_to_chapter(page_num);

                text2 = shortenSearchResult(text, bookdata.searchingKeyword2);

                text3 = text2.replace(re,'<strong>$&</strong>');

                var tag_copy = tag + '';
                bookdata.searchingCount2 += 1;
                tag_copy = tag_copy.replace(/<!--CHAPTER-->/g,current_chapter);
                tag_copy = tag_copy.replace(/<!--PAGE_NUMBER-->/g,page_num);
                tag_copy = tag_copy.replace('<!--CONTENT-->',text3);
                ul.append(tag_copy);
            }
            if(bookdata.searchingCount2 > 0){
                <? if ($engclassicMode){ ?>
                span_total.html("<!--NUMBER--> result(s) found...".replace('<!--NUMBER-->',bookdata.searchingCount2));
                <? }else{?>
                span_total.html(jsLang["ResultsFound"].replace('<!--NUMBER-->',bookdata.searchingCount2));
                <?}?>
            } // 2013-07-15 result found

            else span_total.html('Searching...');

            jsDoSearchAdvanced();
        }else if (bookdata.searching){

            // append display result to search_result
            var tag = '<li><a href="javascript:jsToPageFromSearch(<!--PAGE_NUMBER-->);"><h1><span class="search_result_chapter"><!--CHAPTER--></span>';
            <? if ($engclassicMode){ ?>
            tag+= '<span class="page_num">Page <!--PAGE_NUMBER--></span><p class="spacer"></p></h1>';
            <? }else{?>
            tag+= '<span class="page_num">'+jsLang["Page"]+' <!--PAGE_NUMBER--></span><p class="spacer"></p></h1>';
            <?}?>

            tag+= '<span class="search_result_content"><!--CONTENT--></span></a><p class="spacer"></p></li>';
            var ul =  $('div.search_result ul');
            var span_total = $('span#search_result_total');
            for(var i=0;i<pages.length;i++){
                var page_num = pages[i]['page'][1] != "exact" ? jsPageIndexToPageNumber(bookdata.searchingPageIndex) + pages[i]['page'] : pages[i]['page'][0];

                var text = decodeURIComponent(pages[i]['text']);
                var re = new RegExp(bookdata.searchingKeyword.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$1'),"gi");

                current_chapter = page_to_chapter(page_num);

                text = shortenSearchResult(text, bookdata.searchingKeyword);

                text = text.replace(re,'<strong>$&</strong>');
                var tag_copy = tag + '';
                bookdata.searchingCount += 1;

                tag_copy = tag_copy.replace(/<!--CHAPTER-->/g,current_chapter);
                tag_copy = tag_copy.replace(/<!--PAGE_NUMBER-->/g,page_num);
                tag_copy = tag_copy.replace('<!--CONTENT-->',text);
                ul.append(tag_copy);
            }
            if(bookdata.searchingCount > 0){
                <? if ($engclassicMode){ ?>
                span_total.html("<!--NUMBER--> result(s) found...".replace('<!--NUMBER-->',bookdata.searchingCount));
                <? }else{?>
                span_total.html(jsLang["ResultsFound"].replace('<!--NUMBER-->',bookdata.searchingCount));
                <?}?>
            } // 2013-07-15 result found
            else span_total.html('Searching...');

            jsDoSearch();
        }
    }

    function jsGetSelection()
    {
        var sel = '';
        if (document.getSelection) {    // all browsers, except IE before version 9
            sel = document.getSelection().toString();
        }
        else {
            if (document.selection) {   // Internet Explorer before version 9
                var textRange = document.selection.createRange ();
                sel = textRange.text;
            }
        }
        // store text to buffer memory
        if(sel != ''){
            bookdata.selectionBuffer = sel;
        }else if(typeof(bookdata.selectionBuffer) != 'undefined'){
            sel = bookdata.selectionBuffer;
        }

        return sel;
    }

    function uniqueID()
    {
        var newDate = new Date;
        return (newDate.getTime());
    }

    function jsFormatDatetime(ts)
    {
        var padZero = function(n){
            return n < 10 ? '0' + n : n;
        };
        var today = new Date;
        var tsDate = new Date(ts);
        var todayYear = today.getFullYear();
        var todayMonth = padZero(today.getMonth()+1);
        var todayDay = padZero(today.getDate());
        var today_date = todayYear + '-' + todayMonth + '-' + todayDay;

        var tsYear = tsDate.getFullYear();
        var tsMonth = padZero(tsDate.getMonth()+1);
        var tsDay = padZero(tsDate.getDate());

        var ts_time = padZero(tsDate.getHours()) + ':' + padZero(tsDate.getMinutes()) + ':' + padZero(tsDate.getSeconds());
        var ts_date = tsYear + '-' + tsMonth + '-' + tsDay;
        var format_date = ts_date + ' ' + ts_time;

        if(ts_date == today_date){
            format_date = jsLang["Today"] + ' ' + ts_time;
        }else if(tsYear == todayYear && tsMonth == todayMonth && (todayDay - tsDay) == 1 )
        {
            format_date = jsLang["Yesterday"] + ' ' + ts_time;
        }
//  console.log([format_date, tsDate, tsDate.getFullYear(), tsDate.getMonth(), tsDate.getDate() ]);
        return format_date;
    }

    bookdata.highlightConfig = {
        'colorClassOptions':{
            'btn_hightlight_01':'hightlight_01',
            'btn_hightlight_02':'hightlight_02',
            'btn_hightlight_03':'hightlight_03',
            'btn_hightlight_04':'hightlight_04',
            'btn_hightlight_05':'hightlight_05',
            'btn_hightlight_06':'hightlight_06',
            'btn_hightlight_07':'hightlight_07',
            'btn_select_hightlight_01':'hightlight_01',
            'btn_select_hightlight_02':'hightlight_02',
            'btn_select_hightlight_03':'hightlight_03',
            'btn_select_hightlight_04':'hightlight_04',
            'btn_select_hightlight_05':'hightlight_05',
            'btn_select_hightlight_06':'hightlight_06',
            'btn_select_hightlight_07':'hightlight_07'
        },
        'selectedColorClass':'hightlight_01'
    };

    bookdata.repositionHighlights = function(){

        if(<?=$ForceToOnePageMode?'true':'false'?> || browsertype == 'MSIE' || bookdata.isImageBook){
            return;
        }
        var list = $('span[id*=hightlight_]');
        var dvm = $('#book_content_main');
        list.each(function(i,elem){
            var that = $(elem);
            var id = that.attr('id').replace('hightlight_','');
            var btn = $('span#note_'+id);

            if(btn.length > 0){
                btn.offset({left: that.offset().left, top: that.offset().top - btn.height()});
            }
        });
    };

    function jsInitHighlightEvent()
    {
        <?php if($ForceToOnePageMode){?>
        return;
        <?php }?>
        if(bookdata.isImageBook || browsertype=='MSIE'){
            return;
        }
        var touch_time = 0, prev_selText = " " ;
        $("#book_content_main").bind("<?=(($platform=='iPad' || $platform == 'Andriod' || $platform == 'Android')?'touchend':'mouseup')?>", function(e){
            <?php if(!($platform=='iPad' || $platform == 'Andriod' || $platform == 'Android')){?>
            touch_time = 1;
            <?php }?>
            // Check if user select any text
            var selText = document.getSelection();
            var str= selText.toString();
            if (selText != "")
            {
                if(touch_time == 0){
                    touch_time = 1;
                    return;
                }else{

                }

                // Obtain the selection OBJECT
                var selection = window.getSelection();
                //Returns a range object representing one of the ranges currently selected.
                var range = selection.getRangeAt(0);
                var selectionText = selection.toString().replace(/(\r\n|\n|\r)/gm,"");


                // ## Boolean to check if the following functions should be proceed
                var isValid = true;

                /////////{STEP 1}////////////   Check If starting point and ending point is in same node

                // This two parent nodes is used to check if the selection is in same node / <span></span>
                var startNodeParent = selection.anchorNode.parentNode;
                var endNodeParent = selection.focusNode.parentNode;

                // Text in the node where the selection started -- update the natural expression in replace
                var startNodeText = $(startNodeParent).text().replace(/(\r\n|\n|\r)/gm,"");
                // Obtains the number of characters that the selection's anchor is offset within the anchorNode.
                var startOffset = selection.anchorOffset;
                var endOffset = selection.focusOffset;

                if(startNodeParent != endNodeParent){
                    isValid = false;
                    //startNodeText += $(endNodeParent).text().replace(/\r\n/g,"");
                }

                /////////{STEP 2}////////////   Check If Overlap Previously-Highlighted area

                // ## The array which contain overlapping highlight areas
                var trueOverlapHighlights = [];

                if(isValid)
                {    // Record the start point and end point
                    var selectionTextStartPoint = startNodeText.indexOf(selectionText);
                    var selectionTextEndPoint = selectionTextStartPoint + selectionText.length;

                    // ## In order to collect the part in start node which has already been highlighted
                    var overlapHighlights = jsxCheckOverlapInChildNodes(startNodeParent);
                    // Also collect part in end node which has already been highlighted
                    var tmp_overlaps = jsxCheckOverlapInChildNodes(endNodeParent);
                    for(var j=0;j<tmp_overlaps.length;j++){
                        overlapHighlights.push(tmp_overlaps[j]);
                    }
                    // Also collect part between start and end node which has already been highlighted
                    tmp_overlaps = jsxCheckOverlapBetweenTwoNodes(startNodeParent,endNodeParent);
                    for(var j=0;j<tmp_overlaps.length;j++){
                        overlapHighlights.push(tmp_overlaps[j]);
                    }
                    // Also collect part overlapping end and start node which has already been highlighted
                    tmp_overlaps = jsxCheckOverlapBetweenTwoNodes(endNodeParent,startNodeParent);
                    for(var j=0;j<tmp_overlaps.length;j++){
                        overlapHighlights.push(tmp_overlaps[j]);
                    }

                    for(var i=0;i<overlapHighlights.length;i++){
                        // tmp_span is the overlapping span
                        var tmp_span = $(overlapHighlights[i]);
                        var tmp_text = tmp_span.text().replace(/(\r\n|\n|\r)/gm,"");
                        var tmp_start_point = startNodeText.indexOf(tmp_text);
                        var tmp_end_point = tmp_start_point + tmp_text.length;
                        if( (selectionTextStartPoint <= tmp_start_point && selectionTextEndPoint >= tmp_end_point) // whole overlap
                            || (selectionTextStartPoint >= tmp_start_point && selectionTextStartPoint < tmp_end_point && selectionTextEndPoint >= tmp_end_point) // right overlap
                            || (selectionTextStartPoint <= tmp_start_point && selectionTextEndPoint <= tmp_end_point && selectionTextEndPoint > tmp_start_point) // left overlap
                            || (selectionTextStartPoint >= tmp_start_point && selectionTextEndPoint <= tmp_end_point) ) // inner overlap
                        {
                            trueOverlapHighlights.push(overlapHighlights[i]);
                        }

                    }
                }
                //console.log(isValid);
                // Check if there really has overlap
                if(isValid && trueOverlapHighlights.length > 0){
                    //isValid = false;
                }
                //console.log(isValid);
                // Check if startNode is already highlighted
                if(isValid && startNodeParent.id && startNodeParent.id.match(/^hightlight_[0-9]+$/)){
                    isValid = false;
                }
                //console.log(isValid);
                // Check if endNode is already highlighted
                if(isValid && endNodeParent.id && endNodeParent.id.match(/^hightlight_[0-9]+$/)){
                    isValid = false;
                }
                //console.log(isValid);
                // Check if the start and end of selection is in ambook_board_body or not
                if(isValid && ( $(selection.anchorNode).parents('.book_content_main').length == 0
                        || $(selection.focusNode).parents('.book_content_main').length == 0) ){
                    isValid = false;
                }
                //console.log(isValid);
                if(isValid){
                    var tspan = document.createElement('SPAN');
                    tspan.appendChild(range.cloneContents());
                    var spans_arr = $(tspan).find("span");

                    for(var i =0; i< spans_arr.length; i++){
                        if($(spans_arr[i]).attr("name") == "highlighted"){
                            isValid = false;
                            break;
                        }
                    }
//      if(tspan.getElementsByTagName("span").length >0 )
//          isValid = false;
                }
                if(selection.focusNode.parentNode.nodeName == "SPAN"){
                    isValid = false;
                }

                var fragment = range.cloneContents();  //https://gist.github.com/gleuch/2475825
                var div = document.createElement('div');
                div.appendChild( fragment.cloneNode(true) );
                var actual_text = div.innerHTML;

                /////////{STEP 3}////////////   Add Highlight <span/> if isValid is true
                if(isValid)
                {     touch_time = 0;
                    var newID = uniqueID();
                    var newHighlightID = "hightlight_"+uniqueID();
                    var newNoteID = "note_"+uniqueID();
                    var highlight = {
                        "uniqueID": newID,
                        "id": newHighlightID,
                        //"text": selText.toString(),
                        "text": actual_text,
                        "startOffset": range.startOffset,
                        "endOffset": range.endOffset,
                        "class": bookdata.highlightConfig['selectedColorClass'],
                        "notes": '',
                        "page": 0,
                        "dateModify": newID // datetime stamp
                    };

                    var span = document.createElement('SPAN');
                    span.setAttribute("id", newHighlightID);
                    span.setAttribute("class", bookdata.highlightConfig['selectedColorClass']);
                    span.setAttribute("name", "highlighted");
                    span.appendChild(range.cloneContents());
                    range.deleteContents();
                    range.insertNode(span);

                    highlight.childNode = document.getElementById(newHighlightID).traverseTop({}); //, 'content_entry_text_' + getSecIndex()

                    highlight.page = jsGetTruePageNumber(newHighlightID);

                    bookdata.addHighlight(highlight);

                    var thisHighlightBtnID =  $('#' + newHighlightID);
                    if(thisHighlightBtnID.length > 0){
                        thisHighlightBtnID.bind("mouseup", bookdata.onHighlight);
                    }
                }
            }else{
                touch_time = 0;
            }
        });

        $("#book_content_main1").bind("<?=(($platform=='iPad' || $platform == 'Andriod' || $platform == 'Android')?'touchend':'mouseup')?>", function(e){
            var selText = document.getSelection();
            if (selText != "")
            {
                var selection = window.getSelection();
                var range = selection.getRangeAt(0);
                var selectionText = selection.toString().replace(/\r\n/g,"");

                <?php if($platform == 'iPad' || $platform == 'Andriod' || $platform == 'Android'){ ?>
                if(typeof(bookdata.highlightState)=='undefined'){
                    bookdata.highlightState = 'READY';
                }else if(bookdata.highlightState == 'READY'){
                    bookdata.highlightState = 'HIGHLIGHTED';
                }else{
                    delete bookdata.highlightState;
                }

                if(bookdata.highlightState == 'HIGHLIGHTED')
                {
                    <?php } ?>
                    var isValid = true;
                    var startNodeParent = selection.anchorNode.parentNode;
                    var endNodeParent = selection.focusNode.parentNode;
                    var startNodeText = $(startNodeParent).text().replace(/\r\n/g,"");
                    var startOffset = selection.anchorOffset;
                    var endOffset = selection.focusOffset;

                    if(startNodeParent != endNodeParent){
                        isValid = false;
                        startNodeText += $(endNodeParent).text().replace(/\r\n/g,"");
                    }

                    if(isValid)
                    {
                        var selectionTextStartPoint = startNodeText.indexOf(selectionText);
                        var selectionTextEndPoint = selectionTextStartPoint + selectionText.length;
                        var trueOverlapHighlights = [];

                        var overlapHighlights = jsxCheckOverlapInChildNodes(startNodeParent);
                        if(startNodeParent != endNodeParent){
                            var tmp_overlaps = jsxCheckOverlapInChildNodes(endNodeParent);
                            for(var j=0;j<tmp_overlaps.length;j++){
                                overlapHighlights.push(tmp_overlaps[j]);
                            }

                            tmp_overlaps = jsxCheckOverlapBetweenTwoNodes(startNodeParent,endNodeParent);
                            for(var j=0;j<tmp_overlaps.length;j++){
                                overlapHighlights.push(tmp_overlaps[j]);
                            }

                            tmp_overlaps = jsxCheckOverlapBetweenTwoNodes(endNodeParent,startNodeParent);
                            for(var j=0;j<tmp_overlaps.length;j++){
                                overlapHighlights.push(tmp_overlaps[j]);
                            }
                        }

                        for(var i=0;i<overlapHighlights.length;i++){
                            var tmp_span = $(overlapHighlights[i]);
                            var tmp_text = tmp_span.text().replace(/\r\n/g,"");
                            var tmp_start_point = startNodeText.indexOf(tmp_text);
                            var tmp_end_point = tmp_start_point + tmp_text.length;
                            if( (selectionTextStartPoint <= tmp_start_point && selectionTextEndPoint >= tmp_end_point) // whole overlap
                                || (selectionTextStartPoint >= tmp_start_point && selectionTextStartPoint < tmp_end_point && selectionTextEndPoint >= tmp_end_point) // right overlap
                                || (selectionTextStartPoint <= tmp_start_point && selectionTextEndPoint <= tmp_end_point && selectionTextEndPoint > tmp_start_point) // left overlap
                                || (selectionTextStartPoint >= tmp_start_point && selectionTextEndPoint <= tmp_end_point) ) // inner overlap
                            {

                                trueOverlapHighlights.push(overlapHighlights[i]);
                            }
                        }
                    }

                    if(isValid && trueOverlapHighlights.length > 0){
                        isValid = false;
                    }
                    if(isValid && startNodeParent.id && startNodeParent.id.match(/^hightlight_[0-9]+$/)){
                        isValid = false;
                    }
                    if(isValid && endNodeParent.id && endNodeParent.id.match(/^hightlight_[0-9]+$/)){
                        isValid = false;
                    }
                    if(isValid && ( $(selection.anchorNode).parents('#book_content_main').length == 0
                            || $(selection.focusNode).parents('#book_content_main').length == 0) ){
                        isValid = false;
                    }

                    if(isValid)
                    {

                        var newID = uniqueID();
                        var newHighlightID = "hightlight_"+uniqueID();
                        var newNoteID = "note_"+uniqueID();
                        var highlight = {
                            "uniqueID": newID,
                            "id": newHighlightID,
                            "text": selText.toString(),
                            "startOffset": range.startOffset,
                            "endOffset": range.endOffset,
                            "class": bookdata.highlightConfig['selectedColorClass'],
                            "notes": '',
                            "page": 0,
                            "dateModify": newID // datetime stamp
                        };

                        newRange = document.createRange();
                        //newRange.setStart(selection.anchorNode, range.startOffset);
                        //newRange.setEnd(selection.focusNode, range.endOffset);
                        //newRange.selectNodeContents(selection.anchorNode);
                        if(startOffset > endOffset){ // select from back to front
                            newRange.setStart(selection.focusNode, selection.focusOffset);
                            newRange.setEnd(selection.anchorNode, selection.anchorOffset);
                        }else{
                            newRange.setStart(selection.anchorNode, selection.anchorOffset);
                            newRange.setEnd(selection.focusNode, selection.focusOffset);
                        }
                        /*
            newRange.setStart(selection.anchorNode, selection.anchorOffset);
            if(selection.anchorNode == selection.focusNode){
              newRange.setEnd(selection.anchorNode, selection.focusOffset);
            }else{
              newRange.setEnd(selection.focusNode, selection.focusOffset);
            }
            */
                        var style = document.createElement('span');

                        style.setAttribute("id", newHighlightID);
                        style.setAttribute("class",bookdata.highlightConfig['selectedColorClass']);
                        style.innerHTML = selText;

                        //var noteButton = document.createElement('span');
                        //noteButton.setAttribute('class', 'btn_notes');
                        //noteButton.setAttribute('id', newNoteID);
                        //noteButton.setAttribute('title', 'Notes');

                        newRange.deleteContents();
                        newRange.insertNode(style);
                        //  newRange.insertNode(noteButton);

                        highlight.childNode = document.getElementById(newHighlightID).traverseTop({});

                        highlight.page = jsGetTruePageNumber(newHighlightID);
                        bookdata.addHighlight(highlight);
                        var highlightBtn =  $('#' + newHighlightID);
                        if(highlightBtn.length > 0){
                            highlightBtn.bind("mouseup", bookdata.onHighlight);
                        }
                        bookdata.repositionHighlights();

                        // store text buffer for text to speech
                        bookdata.selectionBuffer = selText.toString();
                        <?php if($platform=='iPad' || $platform == 'Andriod' || $platform == 'Android'){ ?>
                        delete bookdata.highlightState;
                        <?php } ?>
                    }
                    <?php if($platform=='iPad' || $platform == 'Andriod' || $platform == 'Android'){ ?>
                    else{
                        delete bookdata.highlightState;
                    }
                }
                else if(bookdata.highlightState == 'READY'){
                    // keep ready to select
                }
                else{
                    delete bookdata.highlightState;
                }
                <?php } ?>

            }
        });

        bookdata.note_arr = [];

        bookdata.loadHightlights = function(isClickFromStart){
            if(<?=$ForceToOnePageMode?'true':'false'?>){
                return true;
            }
            bookdata.note_arr = [];
            $("#book_note_main_container").html("");

            if(bookdata.isImageBook || browsertype=='MSIE'){
                return;
            }

            $.post(
                '/home/eLearning/ebook_reader/ajax_load.php',
                {
                    'task': 'Get_Page_Highlights',
                    'BookID': bookdata.id,
                    'RefID' : encodeURIComponent(bookdata.pages[bookdata.currentPage]['idref'])
                },function(data){
                    if($("#book_content").css("display") == "none"){
                        $("#book_content").css("display","");
                    }

                    data = data.replace(/\[object Object\][,]?/g,"");
                    if(data != '')
                        eval("bookdata.highlights = [" + data + "];");
                    else{
                        bookdata.highlights = [];
                    }

                    if(bookdata.highlights && bookdata.highlights.length > 0){
                        bookdata.highlights.sort(jsSortHighlightsComparer);
                    }

                    var highlights = bookdata.highlights;
                    var lastOffset = 0;
                    for (var i =0; i < highlights.length; ++i)
                    {
                        highlights[i]['text'] = decodeURIComponent(highlights[i]['text']);
                        highlights[i]['notes'] = $.trim(decodeURIComponent(highlights[i]['notes']));

                        var current_text = highlights[i]['text'].replace(/\<[\<\>\.\/0-9\-a-zA-Z\'\"\=\_ ]+\>|\&nbsp\;/g,"");

                        var focusNode = document.getElementById('book_content_main').getNode(highlights[i].childNode);

                        var nextNode = focusNode;

                        var range = document.createRange();

                        if(typeof(focusNode) == 'undefined' || focusNode==null){
                            continue;
                        }

                        //20141020

                        range.setStart(focusNode, highlights[i].startOffset);

                        //var temp_endOffset = highlights[i].endOffset;

                        var startNodeLength = focusNode.length - highlights[i].startOffset;

                        var totalLength = startNodeLength;

                        var error_handle = false;

                        while(totalLength < current_text.length){
                            if(!nextNode.nextSibling){
                                error_handle = true;
                                break;
                            }
                            nextNode = nextNode.nextSibling.nextSibling;
                            totalLength =  totalLength + nextNode.length;
                        }

                        if(error_handle){
                            error_handle = false;
                            continue;
                        }

                        if(highlights[i].endOffset < 0){
                            highlights[i].endOffset = lastOffset + highlights[i].endOffset;
                        }

                        lastOffset = highlights[i].endOffset;

                        range.setEnd(nextNode, highlights[i].endOffset);

                        var span = document.createElement('SPAN');

                        span.setAttribute("id", highlights[i].id);
                        span.setAttribute("class", highlights[i]['class']);
                        span.setAttribute("name", "highlighted");

                        <? if($engclassicMode || $youngreaderMode){?>
                        span.innerHTML = highlights[i].text.replace( new RegExp ( "\n" , "gm" ), "" );
                        <?}else{?>
                        span.innerHTML = highlights[i].text.replace( new RegExp ( "\n" , "gm" ), "<br>" );
                        <?}?>

                        range.deleteContents();
                        range.insertNode(span);

//          console.log(highlights[i]);

                        var currentHighlightBtn =  $('#' + highlights[i].id);
                        <? if($cupMode){?>
                        var current_paragraph = $(currentHighlightBtn.parent().parent()[0]);
                        var current_offsetLeft = current_paragraph[0]["offsetLeft"];
                        <?}else{?>
                        var current_paragraph = $(currentHighlightBtn.parent()[0]);
                        var current_offsetLeft = currentHighlightBtn[0]["offsetLeft"];
                        <?}?>

                        if(current_paragraph[0].nodeName == "P"){
                            var nextElementSibling = $(current_paragraph[0].nextElementSibling);
                            var previousElementSibling = $(current_paragraph[0].previousElementSibling);
                        }else{
                            var nextElementSibling = $(current_paragraph.parent()[0].nextElementSibling);
                            var previousElementSibling = $(current_paragraph.parent()[0].previousElementSibling);
                        }

                        var current_highlight_offset = currentHighlightBtn.offset();
                        var current_offsetTop = currentHighlightBtn[0]["offsetTop"];
                        var current_paragraph_offsetWidth = current_paragraph[0]["offsetWidth"];
                        var current_paragraph_offsetLeft = current_paragraph[0]["offsetLeft"];
                        var current_offsetHeight = currentHighlightBtn[0]["offsetHeight"];

                        if(currentHighlightBtn.length > 0){
                            currentHighlightBtn.bind("mouseup", bookdata.onHighlight);
                            if(highlights[i]['notes']!=''){
                                var note_btn = '<span id="note_'+highlights[i]['uniqueID']+'" class="btn_notes" title="Notes"></span>';

//			  var requestPage = Math.ceil(highlights[i]['page']/2);
                                var current_chapter_first_page = bookdata.currentPage - bookdata.pages[bookdata.currentPage].session + 1;
                                if(bookdata.pageMode == 1){
                                    var requestPage = parseInt(current_offsetLeft/650)+current_chapter_first_page;
                                }else{
                                    var requestPage = parseInt(current_offsetLeft/980)+current_chapter_first_page;
                                }

                                var note_container = '<div id="note_page_'+requestPage+'" page="'+requestPage+'" class="notes_container" title="Notes"></div>';

                                if($('#note_page_'+requestPage).length == 0){
                                    $("#book_note_main_container").append(note_container);
                                }

                                var note_container_elem = $('#note_page_'+requestPage);

                                var diff = requestPage - bookdata.currentPage;  //current_chapter_first_page
                                var dist = bookdata.pageModeWidth[bookdata.pageMode] * diff;

                                if(note_container_elem.length == 1){ //20141021

                                    if((Math.floor((current_offsetLeft%980)/490))%2 == 1) var offsetPage = "right";
                                    else var offsetPage = "left";

                                    if((Math.floor(((current_paragraph.offset().left%980+980)%980)/490))%2 == 1) var offsetParagraph = "right";
                                    else var offsetParagraph = "left";

                                    var note_btn_elem = $('#note_' + requestPage+'_'+current_offsetTop+'_'+offsetPage);
                                    if(note_btn_elem.length == 0){
                                        var note_btn = '<div page="'+requestPage+'" id="note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage+'" class="notes_side" title="Notes"><a href="javascript:void(0)"></a></div>';
                                        $('#note_page_'+requestPage).append(note_btn);
                                        var note_btn_elem = $('#note_' + requestPage+'_'+current_offsetTop+'_'+offsetPage);
                                        $('#note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage).mouseup(bookdata.tool_my_notes_handler);
                                    }
                                    var current_note_market_height = parseInt(note_btn_elem.css("height"));
                                    note_btn_elem.css("height", current_note_market_height > current_offsetHeight? current_note_market_height : current_offsetHeight);

                                    note_btn_elem.css("top", current_highlight_offset.top);
                                    if(bookdata.pageMode == 1){
                                        note_btn_elem.css("left", current_paragraph.offset().left+current_paragraph_offsetWidth+5 - dist);
                                    }else{
                                        if(offsetPage == "right"){
                                            if(offsetParagraph == "right"){
                                                note_btn_elem.css("left", current_paragraph.offset().left+current_paragraph_offsetWidth+5 - dist);
                                            } else{
                                                if(current_paragraph.offset().left < current_offsetLeft ){
                                                    note_btn_elem.css("left", nextElementSibling.offset().left+current_paragraph_offsetWidth+5 - dist);
                                                }else{
                                                    note_btn_elem.css("left", previousElementSibling.offset().left+current_paragraph_offsetWidth+5 - dist);
                                                }
                                            }
                                        }else{
                                            if(offsetParagraph == "left"){
                                                note_btn_elem.css("left", current_paragraph.offset().left-25 - dist);
                                            }else{
                                                if(current_paragraph.offset().left < current_offsetLeft ){
                                                    if(!nextElementSibling.offset() ||  (nextElementSibling.offset().left-25 - dist) < 0){
                                                        note_btn_elem.css("left", current_paragraph.offset().left+current_paragraph_offsetWidth+25 - dist);
                                                    }else{
                                                        note_btn_elem.css("left", nextElementSibling.offset().left-25 - dist);
                                                    }
                                                }else{
                                                    note_btn_elem.css("left", previousElementSibling.offset().left-25 - dist);
                                                }
                                            }
                                            note_btn_elem.addClass("left");
                                        }
                                    }

                                    if(bookdata.note_arr['note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage] == undefined){
                                        bookdata.note_arr['note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage] = [];
                                    }
                                    bookdata.note_arr['note_'+requestPage+'_'+current_offsetTop+'_'+offsetPage].push(highlights[i]['uniqueID']);
                                }
                            }
                        }
                    }
                    $(".notes_container").css("display","none");
                    $("#note_page_"+bookdata.currentPage).css({"display":"", "opacity" : 0});
                    $("#note_page_"+bookdata.currentPage).animate({opacity: 1.0}, 'slow');

                    if(isClickFromStart){  			//20141022
                        if($("#book_cover_image").length >0 || $("#book_table_of_content").css("display") == "none"){
                            $("#book_content").css({"display":"none",opacity:1});
                        }else{
                            $("#book_content").css({opacity:1});
                        }
                    }
                    else
                        $("#book_content").css({"display":""});
                    //bookdata.repositionHighlights();
                    glossary_attach();
                }
            );
        };

        bookdata.loadHightlights1 = function(){

            if(<?=$ForceToOnePageMode?'true':'false'?> || bookdata.isImageBook || browsertype=='MSIE'){
                return;
            }
            $.post(
                '/home/eLearning/ebook_reader/ajax_load.php',
                {
                    'task': 'Get_Page_Highlights',
                    'BookID': bookdata.id,
                    'RefID' : encodeURIComponent(bookdata.pages[bookdata.currentPage]['idref'])
                },function(data){
                    if(data != '')
                        eval("bookdata.highlights = [" + data + "];");
                    else{
                        bookdata.highlights = [];
                    }
                    if(bookdata.highlights && bookdata.highlights.length > 0){
                        bookdata.highlights.sort(jsSortHighlightsComparer);
                    }
                    var highlights = bookdata.highlights;
                    for (var i =0; i < highlights.length; ++i)
                    {
                        highlights[i]['text'] = decodeURIComponent(highlights[i]['text']);
                        highlights[i]['notes'] = $.trim(decodeURIComponent(highlights[i]['notes']));
                        var node1 = document.getElementById('book_content_main').getNode(highlights[i].childNode);
                        //var node1 = document.body.getNode(highlights[i].childNode);
                        var newRange = document.createRange();
                        // prevent error...
                        if(typeof(node1) == 'undefined' || node1==null){
                            continue;
                        }
                        newRange.setStart(node1, highlights[i].startOffset);
                        newRange.setEnd(node1, highlights[i].endOffset);
                        var style = document.createElement('span');
                        style.setAttribute("id", highlights[i].id);
                        style.setAttribute("class", highlights[i]['class']);
                        style.innerHTML = highlights[i].text;
                        newRange.deleteContents();
                        newRange.insertNode(style);

                        var highlightBtn =  $('#' + highlights[i].id);
                        if(highlightBtn.length > 0){
                            highlightBtn.bind("mouseup", bookdata.onHighlight);

                            if(highlights[i]['notes']!=''){
                                var note_btn = '<span id="note_'+highlights[i]['uniqueID']+'" class="btn_notes" title="Notes"></span>';
                                highlightBtn.after(note_btn);
                                var note_btn_elem = $('#note_' + highlights[i]['uniqueID']);
                                if(note_btn_elem.length > 0){
                                    note_btn_elem.bind("mouseup",bookdata.highlight_notes_icon_handler);
                                }
                            }
                        }
                    }
                    bookdata.repositionHighlights();
                }
            );
        };

        bookdata.onHighlight = function(e){
            $('#tool_glossary_board').hide();//20140919-newfeature2014
            var self = $(this); // class hightlight_0X
            var prevHighlightBtnID = typeof(bookdata.prevHighlightBtn)!='undefined'? bookdata.prevHighlightBtn.attr('id'):'';
            var dvb = $('#tool_highlight_board');
            if(dvb.is(':visible')){
                if(prevHighlightBtnID == self.attr('id')){
                    //dvb.slideUp('fast');
                    dvb.hide();
                }
            }else{
                //dvb.slideDown('fast');
                dvb.show();
            }
            //var posx = self.offset().left - self.width()/2;
            //var posy = self.offset().top - self.height()*2;

            //2013-03-27 CharlesMa eBook-update-130325d
            var window_width = $(window).width(), narrowest_window_width =  bookdata.pageMode == 1? 760: 1020, leftest_posx =  bookdata.pageMode == 1? 270:530;
            var pos_limit = (((window_width - narrowest_window_width)/2)>0? ((window_width - narrowest_window_width)/2) + leftest_posx : leftest_posx) +35;                         //2012-10-11 CharlesMa

            var btn_left = (self.offset().left - self.context.offsetLeft);

            while(btn_left < 0){
                btn_left +=  bookdata.pageModeWidth[bookdata.pageMode];     //2013-6-10 Charles Ma
            }
//    var posx = btn_left < pos_limit? btn_left : pos_limit;

            if(btn_left > 0)
                var posx = self.offset().left - 30 < pos_limit ? self.offset().left - 30 : pos_limit;
            else
                var posx = self.context.offsetLeft + 130 < pos_limit ? self.context.offsetLeft + 130 : pos_limit;

            //2013-03-27 END

            var posy = self.offset().top - dvb.height();
            //var dvbx = dvb.offset().left;
            //var dvby = dvb.offset().top;

            //if(bookdata.pageMode == 1 && self.width() > 629)  posx += 220;

            dvb.offset({left: posx, top: posy});

            var background_position = self.width() > 400 ? -100 : -500 + self.width() - 15;
            if(self.offset().left - 35 + self.width()  > pos_limit){
                background_position = -500 - pos_limit + self.offset().left - 50  + self.width();               //2013-03-27 CharlesMa
            }
            if(background_position > -250){
                if(bookdata.pageMode == 1){
                    background_position = background_position-100;
                }else{
                    background_position = background_position-270;
                }
            }
            background_position = background_position +'px';
//    console.log(background_position);

            $('.tool_hightlight_bg').css('background-position',background_position);              //2012-11-15 CharlesMa
            $('.tool_hightlight_bg').css('margin-top','-1px');

            if($('#tool_voice').is(':visible')){
                $('#tool_voice').hide();
                $('#tool_voice').removeClass('istts');
                $('#tool_page_control_container').html('');
            }
            bookdata.prevHighlightBtn = self;

            if(self[0].className == "hightlight_06" || self[0].className == "hightlight_07"){
                $('#highlight_pulldown_control1').attr("class", '');
                $('#highlight_pulldown_control2').attr("class", 'selected_hightlight');
                $('#btn_select_hightlight1').attr("class", 'btn_select_hightlight_01');
                $('#btn_select_hightlight2').attr("class", 'btn_select_'+self[0].className);
            }else{
                $('#highlight_pulldown_control1').attr("class", 'selected_hightlight');
                $('#highlight_pulldown_control2').attr("class", '');
                $('#btn_select_hightlight1').attr("class", 'btn_select_'+self[0].className);
                $('#btn_select_hightlight2').attr("class", 'btn_select_hightlight_06');
            }

            $('#tool_glossary_paper').hide();
            $('.highlight_pulldown').hide();
        };
    }


    function jsxCheckOverlapInChildNodes(node)
    {
        var overlaps = [];
        if(node.id){
            if(node.id.match(/^hightlight_[0-9]+$/)){
                overlaps.push(node);
                return overlaps;
            }
        }
        for(var i=0;i<node.childNodes.length;i++){
            var findChildren = true;
            if(node.childNodes[i].id){
                if(node.childNodes[i].id.match(/^hightlight_[0-9]+$/)){
                    overlaps.push(node.childNodes[i]);
                    findChildren = false;
                }
            }
            if(findChildren && node.childNodes[i].childNodes.length > 0){
                var tmp_arr = jsxCheckOverlapInChildNodes(node.childNodes[i]);
                for(var j=0;j<tmp_arr.length;j++){
                    overlaps.push(tmp_arr[j]);
                }
            }
        }

        return overlaps;
    }

    function jsxCheckOverlapBetweenTwoNodes(startNode,endNode)
    {
        var overlaps = [];
        var currentNode = startNode.nextSibling;
        while(currentNode && !jsxCheckEqualContainsNode(currentNode, endNode) ){
            var tmp_overlaps = jsxCheckOverlapInChildNodes(currentNode);
            for(var i=0;i<tmp_overlaps.length;i++){
                overlaps.push(tmp_overlaps[i]);
            }
            currentNode = currentNode.nextSibling;
        }
        currentNode = endNode.previousSibling;
        while(currentNode && !jsxCheckEqualContainsNode(currentNode, startNode) ){
            var tmp_overlaps = jsxCheckOverlapInChildNodes(currentNode);
            for(var i=0;i<tmp_overlaps.length;i++){
                overlaps.push(tmp_overlaps[i]);
            }
            currentNode = currentNode.previousSibling;
        }

        return overlaps;
    }

    function jsxCheckEqualContainsNode(givenNode,targetNode)
    {
        var isEqual = false;
        if(givenNode == targetNode){
            isEqual = true;
        }
        var childNodes = givenNode.childNodes;
        if(!isEqual && childNodes && childNodes.length > 0){
            for(var i=0;i<childNodes.length;i++){
                isEqual = jsxCheckEqualContainsNode(childNodes[i],targetNode);
                if(isEqual){
                    break;
                }
            }
        }

        return isEqual;
    }

    function jsCaptureSelection()
    {
        $("#book_content_main").bind("<?=$platform=='iPad'?'touchend':'mouseup'?>", function(e){
            var txt = jsGetSelection();
        });
    }

    function tmpDebugLog(txt)
    {
        var t = txt.toString() + "\n<br>";
        $('.search_result').append(t);
    }

    function jsBlockUI()
    {
        <?php if($ForceToOnePageMode){?>
        $('body').append('<div id="blockUI" class="loading_layer" ><div class="bubblingG"><span id="bubblingG_1"></span><span id="bubblingG_2"></span><span id="bubblingG_3"></span><em id="loading_text" title="Loading"></em></div></div>');
        <?php }else{?>
        $('body').append('<div id="blockUI" class="ebook_blockUI"><div class="loadingText">'+jsLang["Loading"]+'</div></div>');
        <?php }?>
        var dvs = $('#book_structure');
        //$('#blockUI').css({left: dvs.offset().left, top: dvs.offset().top, width: dvs.width(), height: dvs.height()});
        <?php if($ForceToOnePageMode){?>
        $('#blockUI').css({position: "absolute",zIndex: 9,left: dvs.offset().left, top: dvs.offset().top, width: 1040, height: 660}); // 2013-03-27 Charles Ma eBook-update-130325d
        <?php }else{?>
        //$('#blockUI').css({position: "absolute",zIndex: 9,left: dvs.offset().left, top: dvs.offset().top, width: $(window).width(), height: $(window).height()}); // 2013-03-27 Charles Ma eBook-update-130325d
        $('#blockUI').css({position: "absolute",zIndex: 9, width: $(window).width(), height: '100%'}); // 2013-06-20 Charles Ma eBook-update-130325d
        <?php }?>
        var blockui_loadingtext = $('#blockUI .loadingText');
        if(blockui_loadingtext.length > 0){
            blockui_loadingtext.show();
            blockui_loadingtext.offset({left: dvs.offset().left + dvs.width()/2 - blockui_loadingtext.width()/2, top: dvs.offset().top + dvs.height()/2 - blockui_loadingtext.height()/2});
        }
    }

    function jsUnblockUI()
    {
        $('#blockUI').remove();
    }

    function jsBlockDocument()  //2013-06-03  Charles Ma
    {
        <?php if($ForceToOnePageMode){?>
        if($('#blockUI')[0]){}
        else
            jsBlockUI();
        <?php }else{?>
        Block_Document(jsLang["Loading"]);
        <?php }?>
    }

    function jsUnblockDocument()  //2013-06-03  Charles Ma
    {
        <?php if($ForceToOnePageMode){?>
        jsUnblockUI();
        <?php }else{?>
        UnBlock_Document();
        <?php }?>
    }

    function jsSetupSlider()
    {
        <?php if($isRTL == 1){?>
        var temp_max =  bookdata.pages.length;
        var temp_neg = -1;
        <?php }else{?>
        var temp_max =  0;
        var temp_neg = 1;
        <?php }?>

        $('#tool_page_select').slider({
                                          handle: '#slider-handle',
                                          min: 1,
                                          max: bookdata.pages.length - 1,
                                          stop: function(e,ui){
                                              if((temp_max + temp_neg * ui.value) != bookdata.currentPage){
                                                  jsLoadDocument(temp_max + temp_neg * ui.value);   //2013-3-6 Charles Ma
                                                  jsHideTocAfterChangePage();
                                              }
                                          },
                                          slide: function(e,ui){
                                              bookdata.tool_page_number_handler(true);
                                              if(bookdata.pageMode == 2){
                                                  if(bookdata.isImageBook){
                                                      $('#tool_page_num_span').html(temp_max + temp_neg * ui.value);
                                                  }else{
                                                      $('#tool_page_num_span').html((temp_max + temp_neg * ui.value) * 2 -1);
                                                  }
                                              }else{
                                                  $('#tool_page_num_span').html(temp_max + temp_neg * ui.value);
                                              }
                                          }
                                      });
    }

    function jsSwitchPageMode(pageMode)
    {
        if(pageMode != bookdata.pageMode){
            jsBlockDocument();
            var bookID = bookdata.id;
            var pageNum = bookdata.currentPage;
            var isImageBook = bookdata.isImageBook;
            var showTableOfContent = $('#book_table_of_content').is(':visible')?'1':'0';
            $.get(
                "/home/eLearning/ebook_reader/ajax_task.php?task=Switch_Page_Mode&PageMode="+pageMode+'&BookID='+bookID+'&LastViewPageNumber='+pageNum+'&IsImageBook='+isImageBook+'&ShowTableOfContent='+showTableOfContent,
                {},
                function(returnData){
                    bookdata.searching = false;
                    location.reload();
                }
            );
        }
    }

    function jsSwitchPaperStyle(paperStyle)
    {
        if(paperStyle != bookdata.paperStyle){
            jsBlockDocument();
            var showTableOfContent = $('#book_table_of_content').is(':visible')?'1':'0';
            $.get(
                "/home/eLearning/ebook_reader/ajax_task.php?task=Switch_Paper&PaperStyle="+paperStyle+'&LastViewPageNumber='+bookdata.currentPage+'&ShowTableOfContent='+showTableOfContent,
                {},
                function(returnData){
                    bookdata.searching = false;
                    location.reload();
                }
            );
        }
    }

    bookdata.cover_image_handler = function(){

    }

    bookdata.book_comic_content_handler = function(){

        <?php if($ForceToOnePageMode){?>
        if($('#bookmark_img_left').css('display')=='none'){
            $('.btn_add_bookmarkes').css('display','block');
        }
        <?php }?>
        if(bookdata.isImageBook){
            var dvc = $('#book_comic_content');
            var dvs = $('#book_structure');
            if(dvc.length > 0){
                if(bookdata.pageMode == 2){
                    //dvc.offset({left : dvs.offset().left + ((dvs.width() - dvc.width()) / 2) }); //20130617a Charles Ma
                }else{
                    //dvc.offset({left : dvs.offset().left + ((dvs.width() - dvc.width()) / 2) });
                }
            }
        }
    }

    function jsDisplayLearningObjectInTB(that,width,height,title)
    {
        $.post(
            '/home/eLearning/ebook_reader/ajax_task.php',
            {
                'task':'Get_iFrame_Tag',
                'id':encodeURIComponent(that.id),
                'src':encodeURIComponent(that.title),
                'width':width,
                'height':height
            },
            function(data) {
                var tb_dv = $('div#TB_ajaxContent');
                tb_dv.css('overflow','hidden');
                tb_dv.html(data);
                if(title){
                    $('div#TB_ajaxWindowTitle').html(decodeURIComponent(title));
                }
            }
        );
    }

    function atob(str) {
        var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
        var invalid = {
            strlen: (str.length % 4 != 0),
            chars:  new RegExp('[^' + chars + ']').test(str),
            equals: (/=/.test(str) && (/=[^=]/.test(str) || /={3}/.test(str)))
        };
        if (invalid.strlen || invalid.chars || invalid.equals)
            throw new Error('Invalid base64 data');
        var decoded = [];
        var c = 0;
        while (c < str.length) {
            var i0 = chars.indexOf(str.charAt(c++));
            var i1 = chars.indexOf(str.charAt(c++));
            var i2 = chars.indexOf(str.charAt(c++));
            var i3 = chars.indexOf(str.charAt(c++));
            var buf = (i0 << 18) + (i1 << 12) + ((i2 & 63) << 6) + (i3 & 63);
            var b0 = (buf & (255 << 16)) >> 16;
            var b1 = (i2 == 64) ? -1 : (buf & (255 << 8)) >> 8;
            var b2 = (i3 == 64) ? -1 : (buf & 255);
            decoded[decoded.length] = String.fromCharCode(b0);
            if (b1 >= 0) decoded[decoded.length] = String.fromCharCode(b1);
            if (b2 >= 0) decoded[decoded.length] = String.fromCharCode(b2);
        }
        return decoded.join('');
    }

    var handler_ondrag = false;
    var handler_mouseover = false;
    var reset_page_btn = false;

    function displayControl(){
        if(handler_ondrag) {
            handler_ondrag = false;
            return;
        }
        var element = document.getElementById('calculatorDIV');
        var calculator = document.getElementById('draggable1');

        var left = document.body.clientWidth - 64;

        if(element.style.display == ""){
            $("#draggable1").draggable("disable");
            element.style.display ="none";
            var top = document.body.clientHeight * 0.4;
            calculator.style.top = top + 'px';
            calculator.style.left = '';
            calculator.style.right = '-10px';
            calculator.style.width = '64px';
            calculator.style.height = '64px';
        }
        else{
            $("#draggable1").draggable("enable");
            calculator.style.left = '';
            calculator.style.right = '-10px';
            element.style.display ="";
            // calculator.style.width = '450px';
            calculator.style.width = '250px';
            calculator.style.height = '150px';
        }
    }

    function insertCalculator() {
        return ; //2013-1-25 (CharlesMa)
        var calculatorHtml = "PGltZyBzdHlsZT0iei1pbmRleDogLTM7IG1heC13aWR0aDogMTAwJTsiIHNyYz0iLi4vLi4vaW1hZ2UvY2FsY3VsYXRvcl9QTkcyLnBuZyIgLz4=";
        //var calculatorHtml = "PFNDUklQVCBMQU5HVUFHRT0iSmF2YVNjcmlwdCI+Cgp2YXIgY3VycmVudF9kaXNwbGF5ID0gMCwgZGlzcGxheV9pbml0ID0gdHJ1ZSwgZXF1YWxfcHJlc3MgPSBmYWxzZTsKZnVuY3Rpb24gdXBkYXRlX2FjdHVhbERpc3BsYXkxKGlucHV0KXsKCgl2YXIgZWxlbWVudCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhY3R1YWxEaXNwbGF5Jyk7Cgl2YXIgZGlzcGxheV92YWx1ZSA9IGlucHV0LnZhbHVlLnRvU3RyaW5nKCk7CgllbGVtZW50LmlubmVySFRNTCA9ICIiOwoJZm9yKHZhciBpID0wOyBpIDwgZGlzcGxheV92YWx1ZS5sZW5ndGg7IGkrKyl7CgkJaWYoZGlzcGxheV92YWx1ZVtpXSA9PSAiLiIpCgkJCWVsZW1lbnQuaW5uZXJIVE1MICs9JzxpbWcgc3JjPSIuLi8uLi9pbWFnZS9kb3QucG5nIiAvPic7CgkJZWxzZSBpZihkaXNwbGF5X3ZhbHVlW2ldIDwgIjAiIHx8IGRpc3BsYXlfdmFsdWVbaV0gPiAiOSIpewoJCQljdXJyZW50X2Rpc3BsYXkgPSAwOwoJCQlpbnB1dC52YWx1ZSA9IDA7CgkJCWVsZW1lbnQuaW5uZXJIVE1MID0nPGltZyBzcmM9Ii4uLy4uL2ltYWdlLzAucG5nIiAvPic7CgkJCWRpc3BsYXlfaW5pdCA9IHRydWU7CgkJCWVxdWFsX3ByZXNzID0gZmFsc2U7CgkJCWJyZWFrOwoJCX0KCQllbHNlIGVsZW1lbnQuaW5uZXJIVE1MICs9JzxpbWcgc3JjPSIuLi8uLi9pbWFnZS8nICsgZGlzcGxheV92YWx1ZVtpXSArJy5wbmciIC8+JzsKCQlpZihpID49IDgpIGJyZWFrOwoJfQp9CgpmdW5jdGlvbiB1cGRhdGVfYWN0dWFsRGlzcGxheShpbnB1dCxjaGFyYWN0ZXIpewoJaWYoY3VycmVudF9kaXNwbGF5ID49OSkgcmV0dXJuOwoJdmFyIGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYWN0dWFsRGlzcGxheScpOwkgICAgCglpZihkaXNwbGF5X2luaXQpIHsKCQllbGVtZW50LmlubmVySFRNTCA9ICcnOwoJCWRpc3BsYXlfaW5pdCA9IGZhbHNlOwoJCWN1cnJlbnRfZGlzcGxheT0gMDsKCX0KCWlmKGVxdWFsX3ByZXNzKXsKCQllbGVtZW50LmlubmVySFRNTCA9ICcnOwoJCWVxdWFsX3ByZXNzID0gZmFsc2U7CgkJY3VycmVudF9kaXNwbGF5PSAwOwoJfQoJCglpZihjaGFyYWN0ZXIgPT0gIi4iKQoJCWVsZW1lbnQuaW5uZXJIVE1MICs9JzxpbWcgc3JjPSIuLi8uLi9pbWFnZS9kb3QucG5nIiAvPic7CgllbHNlIGVsZW1lbnQuaW5uZXJIVE1MICs9JzxpbWcgc3JjPSIuLi8uLi9pbWFnZS8nICsgY2hhcmFjdGVyICsnLnBuZyIgLz4nOwoJY3VycmVudF9kaXNwbGF5Kys7Cn0KCgpmdW5jdGlvbiBhZGROdW1iZXIoaW5wdXQsIGNoYXJhY3RlcikgewoJaWYoY3VycmVudF9kaXNwbGF5ID49OSkgcmV0dXJuOwoJCglpZihpbnB1dC52YWx1ZSA9PSBudWxsIHx8IGlucHV0LnZhbHVlID09ICIwIiB8fCBlcXVhbF9wcmVzcyl7CgkJaW5wdXQudmFsdWUgPSBjaGFyYWN0ZXI7Cgl9CgllbHNlCgkJaW5wdXQudmFsdWUgKz0gY2hhcmFjdGVyOwkKCXVwZGF0ZV9hY3R1YWxEaXNwbGF5KGlucHV0LCBjaGFyYWN0ZXIpOwp9CgpmdW5jdGlvbiBhZGRPcGVyYXRpb24oaW5wdXQsIGNoYXJhY3RlcikgewoJaWYoY3VycmVudF9kaXNwbGF5ID49OSkgcmV0dXJuOwoJCglpZihpbnB1dC52YWx1ZSA9PSBudWxsIHx8IGlucHV0LnZhbHVlID09ICIwIikKCQlyZXR1cm47CgllbHNlewoJCXZhciBjaCA9IGlucHV0LnZhbHVlLnN1YnN0cmluZyhpbnB1dC52YWx1ZS5sZW5ndGgtMSwgaW5wdXQudmFsdWUubGVuZ3RoKTsKCQlpZihjaCA8ICIwIiB8fCBjaCA+ICI5Iil7CgkJCWlucHV0LnZhbHVlID0gaW5wdXQudmFsdWUuc3Vic3RyaW5nKDAsIGlucHV0LnZhbHVlLmxlbmd0aC0xKTsKCQl9CgkJaW5wdXQudmFsdWUgKz0gY2hhcmFjdGVyOwkKCX0KCQkKCWRpc3BsYXlfaW5pdCA9IHRydWU7CgllcXVhbF9wcmVzcyA9IGZhbHNlOwp9CgpmdW5jdGlvbiBjaGVja051bWJlcihzdHIpICB7Cglmb3IgKHZhciBpID0gMDsgaSA8IHN0ci5sZW5ndGg7IGkrKykgewoJCXZhciBjaCA9IHN0ci5zdWJzdHJpbmcoaSwgaSsxKTsKCQlpZiAoY2ggPCAiMCIgfHwgY2ggPiAiOSIpIHsKCQkJaWYgKGNoICE9ICIvIiAmJiBjaCAhPSAiKiIgJiYgY2ggIT0gIisiICYmIGNoICE9ICItIiAmJiBjaCAhPSAiLiIgJiYgY2ggIT0gIigiICYmIGNoIT0gIikiKSB7CgkJCQlhbGVydCgiaW52YWxpZCBlbnRyeSEiKTsKCQkJCXJldHVybiBmYWxzZTsKCQkJCX0KCQkJfQoJICAgfQoJcmV0dXJuIHRydWU7Cn0KCmZ1bmN0aW9uIGNsZWFyX2N1cnJlbnQoaW5wdXQpewoJdmFyIHN0ciA9IGlucHV0LnZhbHVlOwoJdmFyIGkgPSBzdHIubGVuZ3RoOwoJZm9yICg7IGkgPj0wOyBpLS0pIHsKCQl2YXIgY2ggPSBzdHIuc3Vic3RyaW5nKGktMSwgaSk7CgkJaWYgKGNoIDwgIjAiIHx8IGNoID4gIjkiKSB7CgkJCWJyZWFrOwkJCQoJICAgfQogICAgfQkKCWlucHV0LnZhbHVlID0gc3RyLnN1YnN0cmluZygwLCBpKTsKCWlmKGlucHV0LnZhbHVlLmxlbmd0aCA+MCkJewoJCXZhciBjaCA9IGlucHV0LnZhbHVlLnN1YnN0cmluZyhpbnB1dC52YWx1ZS5sZW5ndGgtMywgaW5wdXQudmFsdWUubGVuZ3RoLTIpOwoJCWlmKGNoIDwgIjAiIHx8IGNoID4gIjkiKXsKCQkJZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2FjdHVhbERpc3BsYXknKS5pbm5lckhUTUwgPSAnPGltZyBzcmM9Ii4uLy4uL2ltYWdlLzAucG5nIiAvPic7CgkJCWRpc3BsYXlfaW5pdCA9IHRydWU7CgkJfWVsc2UgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2FjdHVhbERpc3BsYXknKS5pbm5lckhUTUwgPSAnJzsKCX1lbHNlIHsKCQlkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnYWN0dWFsRGlzcGxheScpLmlubmVySFRNTCA9ICc8aW1nIHNyYz0iLi4vLi4vaW1hZ2UvMC5wbmciIC8+JzsJCgkJZGlzcGxheV9pbml0ID0gdHJ1ZTsKCX0JCgljdXJyZW50X2Rpc3BsYXk9IDA7Cn0KCmZ1bmN0aW9uIGNsZWFyX2FsbChpbnB1dCl7CQoJaW5wdXQudmFsdWUgPSAiIjsKCWRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdhY3R1YWxEaXNwbGF5JykuaW5uZXJIVE1MID0gJzxpbWcgc3JjPSIuLi8uLi9pbWFnZS8wLnBuZyIgLz4nOwoJZGlzcGxheV9pbml0ID0gdHJ1ZTsKCWN1cnJlbnRfZGlzcGxheT0gMDsKfQoKZnVuY3Rpb24gY29tcHV0ZV9yZXN1bHQoaW5wdXQpICB7CglpbnB1dC52YWx1ZSA9IGV2YWwoaW5wdXQudmFsdWUpOwoJdXBkYXRlX2FjdHVhbERpc3BsYXkxKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpKTsKCWVxdWFsX3ByZXNzID0gdHJ1ZTsKfQoKPC9TQ1JJUFQ+Cgo8aW1nIHN0eWxlPSJ6LWluZGV4OiAtMyIgc3JjPSIuLi8uLi9pbWFnZS9jYWN1bGF0b3JfYmxhbmsucG5nIiB1c2VtYXA9IiNjYWxNYXAiLz4KPGRpdiBzdHlsZT0icG9zaXRpb246IGFic29sdXRlOyB6LWluZGV4OiAyOyB0b3A6IDQ2cHg7IGxlZnQ6IDYwcHggIiBpZD0iYWN0dWFsRGlzcGxheSI+PGltZyBzcmM9Ii4uLy4uL2ltYWdlLzAucG5nIiAvPjwvZGl2Pgo8aW5wdXQgbmFtZT0iY2FsX2Rpc3BsYXkiIGlkPSJjYWxfZGlzcGxheSIgc3R5bGU9ImRpc3BsYXk6bm9uZSIgdmFsdWU9IjAiIHNpemU9IjI1Ii8+CjxtYXAgbmFtZT0iY2FsTWFwIiBpZD0iY2FsTWFwIj4KCjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl83IiBjb29yZHM9IjcwLDE0NiwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkTnVtYmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnNycpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuXzgiIGNvb3Jkcz0iMTIyLDE0NiwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkTnVtYmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnOCcpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuXzkiIGNvb3Jkcz0iMTc0LDE0NiwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkTnVtYmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnOScpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuX2NlIiBjb29yZHM9IjIyNiwxNDYsMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImNsZWFyX2N1cnJlbnQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhbF9kaXNwbGF5JykpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuX2MiIGNvb3Jkcz0iMjc4LDE0NiwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iY2xlYXJfYWxsKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpKSI+Cgo8YXJlYSB0YXJnZXQ9ImlmcmFtZV9ub1VzZSIgaHJlZj0iaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS9sb2dvcy9jbGFzc2ljcGx1cy5wbmciIGlkPSJidG5fNCIgY29vcmRzPSI3MCwxOTksMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE51bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJzQnKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl81IiBjb29yZHM9IjEyMiwxOTksMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE51bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJzUnKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl82IiBjb29yZHM9IjE3NCwxOTksMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE51bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJzYnKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl9tdWwiIGNvb3Jkcz0iMjI2LDE5OSwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkT3BlcmF0aW9uKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnKicpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuXz8iIGNvb3Jkcz0iMjc4LDE5OSwyMCIgc2hhcGU9ImNpcmNsZSI+Cgo8YXJlYSB0YXJnZXQ9ImlmcmFtZV9ub1VzZSIgaHJlZj0iaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS9sb2dvcy9jbGFzc2ljcGx1cy5wbmciIGlkPSJidG5fMSIgY29vcmRzPSI3MCwyNTAsMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE51bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJzEnKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl8yIiBjb29yZHM9IjEyMiwyNTAsMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE51bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJzInKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl8zIiBjb29yZHM9IjE3NCwyNTAsMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE51bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJzMnKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl9kaXYiIGNvb3Jkcz0iMjI2LDI1MCwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkT3BlcmF0aW9uKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnLycpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuX3BlcmNlbnQiIGNvb3Jkcz0iMjc4LDI1MCwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkT3BlcmF0aW9uKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnJScpIj4KCjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl8wIiBjb29yZHM9IjcwLDMwMSwyMCIgc2hhcGU9ImNpcmNsZSIgb25jbGljaz0iYWRkTnVtYmVyKGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdjYWxfZGlzcGxheScpLCAnMCcpIj4KPGFyZWEgdGFyZ2V0PSJpZnJhbWVfbm9Vc2UiIGhyZWY9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbG9nb3MvY2xhc3NpY3BsdXMucG5nIiBpZD0iYnRuX2FkZCIgY29vcmRzPSIxMjIsMzAxLDIwIiBzaGFwZT0iY2lyY2xlIiBvbmNsaWNrPSJhZGRPcGVyYXRpb24oZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhbF9kaXNwbGF5JyksICcrJykiPgo8YXJlYSB0YXJnZXQ9ImlmcmFtZV9ub1VzZSIgaHJlZj0iaHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS9sb2dvcy9jbGFzc2ljcGx1cy5wbmciIGlkPSJidG5fbWluIiBjb29yZHM9IjE3NCwzMDEsMjAiIHNoYXBlPSJjaXJjbGUiIG9uY2xpY2s9ImFkZE9wZXJhdGlvbihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKSwgJy0nKSI+CjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl9lcSIgY29vcmRzPSIyMjYsMzAxLDIwIiBzaGFwZT0iY2lyY2xlIiAKCW9uY2xpY2s9ImlmIChjaGVja051bWJlcihkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnY2FsX2Rpc3BsYXknKS52YWx1ZSkpIHsgY29tcHV0ZV9yZXN1bHQoZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2NhbF9kaXNwbGF5JykpIH0iPgoJCjxhcmVhIHRhcmdldD0iaWZyYW1lX25vVXNlIiBocmVmPSJodHRwczovL3d3dy5nb29nbGUuY29tL2xvZ29zL2NsYXNzaWNwbHVzLnBuZyIgaWQ9ImJ0bl8/PyIgY29vcmRzPSIyNzgsMzAxLDIwIiBzaGFwZT0iY2lyY2xlIj4KCjwvbWFwPgo8aWZyYW1lIG5hbWU9J2lmcmFtZV9ub1VzZScgaGVpZ2h0PScwJyB3aWR0aCA9ICcwJyBmcmFtZWJvcmRlcj0nMCcgc3R5bGU9ImRpc3BsYXk6bm9uZSI+PC9pZnJhbWU+";
        var divHtml = "<div id='draggable1'><img id='handler' src='../../calculator_handler.png'/><div style='top: -50px' id='calculatorDIV'></div></div>";

        $("#book_content").append(divHtml);
        $("#calculatorDIV").append(atob(calculatorHtml) );

        $( "#handler" ).ondrag = false;

        $( "#handler" ).bind( "click touch", displayControl);

        $( "#handler" ).bind( "mouseover touchstart", function(event){
            $('#btn_prev_page').hide();
            $('#btn_next_page').hide();
            document.body.style.cursor = "pointer"
        });
        $( "#handler" ).bind( "mouseout touchend", function(event){
            $('#btn_prev_page').show();
            $('#btn_next_page').show();
            handler_mouseover = false;
            document.body.style.cursor = "default";
        });

        $( "#draggable1" ).bind( "mouseover touchstart", function(event){
            $('#btn_prev_page').hide();
            $('#btn_next_page').hide();
        });
        $( "#draggable1" ).bind( "mouseout touchend", function(event){
            handler_mouseover = false;
            $('#btn_prev_page').show();
            $('#btn_next_page').show();
        });

        $( "#draggable1" ).bind( "dragstart", function(event, ui) {
        });
        $( "#draggable1" ).bind( "dragstop", function(event, ui) {
            handler_ondrag = true;
        });


        $("#draggable1").draggable({handle:"#handler", containment: "#book_content", scroll: false});
        $("#draggable1").draggable("disable");
        var element = document.getElementById('handler');
        handler.style.left= "0px";
        handler.style.position= "absolute";

        var element = document.getElementById('calculatorDIV');
        element.style.display ="none";
        element.style.left ="41px";
        element.style.position= "absolute";

        var calculator = document.getElementById('draggable1');
        var top = document.body.clientHeight * 0.4;
        calculator.style.width = '64px';
        calculator.style.height = '64px';
        calculator.style.top = top + 'px';
        calculator.style.right = '-10px';
        calculator.style.position = "fixed";
        calculator.style.zIndex = 999;
//            var handler = document.getElementById('handler');
//            handler.style.zIndex = 99;


    }

    function hideAudioBtn(){
        if($('#btn_mp3').css('display')!='none'){
            $('#btn_mp3').css('display','none');
            $('#btn_mp3').attr('audio_using','1');
        }

        if($('#btn_mp3_tw').css('display')!='none'){
            $('#btn_mp3_tw').css('display','none');
            $('#btn_mp3_tw').attr('audio_using','1');
        }

        if($('#btn_cupleft_mp3').css('display')!='none'){
            $('#btn_cupleft_mp3').css('display','none');
            $('#btn_cupleft_mp3').attr('audio_using','1');
        }

        if($('#btn_mp3_r').css('display')!='none'){
            $('#btn_mp3_r').css('display','none');
            $('#btn_mp3_r').attr('audio_using','1');
        }

        if($('#btn_cupright_mp3').css('display')!='none'){
            $('#btn_cupright_mp3').css('display','none');
            $('#btn_cupright_mp3').attr('audio_using','1');
        }
    }

    var is_loading_finish = false, isZoom = false, initial_scale = 100;

    $(document).ready(function(){

        insertCalculator();
        <?php
        if (isset($_SESSION['eBookReaderLastViewPageNumber']) && $_SESSION['eBookReaderLastViewPageNumber'] > 0) {
        ?>
        jsLoadDocument(<?=$_SESSION['eBookReaderLastViewPageNumber']?>,<?=$_SESSION['eBookReaderLastViewShowTableOfContent']==1?'true':'false'?>);
        <?php
        unset($_SESSION['eBookReaderLastViewPageNumber']);
        unset($_SESSION['eBookReaderLastViewShowTableOfContent']);
        } else {
        ?>

        jsLoadDocument(1,true);
        <?php
        }
        ?>
        jsAssignEvents();
        jsSetupSlider();
        //jsCaptureSelection();
        jsInitHighlightEvent();
        <?php if(count($user_progress_info)>0){ ?>
        jsInitUserProgress('<?=$user_progress_info[0]['ProgressStatus']?>','<?=$user_progress_info[0]['Percentage']?>');
        <?php } ?>

        document.title += ' - ' + decodeURIComponent('<?=$book_name?>');
        if(browsertype=='MSIE' && !bookdata.isImageBook){
            $('div#book_content_title').html(decodeURIComponent('<?=$book_name?>'));
        }
        //2013-06-20 Charles Ma
        if(bookdata['pageMode'] == 1){
            var scale = $(document).height() / 9;
        }else{
            var scale = $(document).height() / 6.82;
        }
        var translate_scale = -(100 - (scale * 100))*21/40;
        var translate_scale = 3;
        if(<?=$ForceToOnePageMode?'true':'false'?>){
            $(window).resize();
            is_loading_finish = true;
        }else{
            scale = parseInt(scale);
            excess = scale % 10;

            if(bookdata['pageMode'] == 1){
                scale = scale - excess;
            }else{
                scale = scale - excess;
            }
            // adjustment of zoom factor

            var shown_width = (parseInt($('#book_structure').css('width'))+30) * scale / 100;
            var shown_height = (parseInt($('#book_structure').css('height'))+30) * scale / 100;
            var max_width = $( window ).width();
            var max_height = $( window ).height();
            var zoom_width;
            var zoom_height;
            while (max_width > shown_width){
                scale = scale + 10;
                shown_width = parseInt($('#book_structure').css('width')) * scale / 100;
            }
            while (max_width < shown_width){
                scale = scale - 10;
                shown_width = parseInt($('#book_structure').css('width')) * scale / 100;
            }
            zoom_width = scale;
            while (max_height > shown_height){
                scale = scale + 10;
                shown_height = parseInt($('#book_structure').css('height')) * scale / 100;
            }
            while (max_height < shown_height){
                scale = scale - 10;
                shown_height = parseInt($('#book_structure').css('height')) * scale / 100;
            }
            zoom_height = scale;
            if (zoom_width > zoom_height){
                scale = zoom_height;
            }else{
                scale = zoom_width;
            }

            // End of adjustment of zoom factor

            //$('body').css('-webkit-transform', 'scale(' + scale + ') translate(0%,' + translate_scale + '%)');
            $('body').css('zoom', scale + '%');
            $('body').css('width', parseInt($('body').css('width'))-0.1 );
            initial_scale = scale;
            scale= scale / 100;

            if(bookdata.isImageBook && <?=$imageWidth * 2.1?> > 1224 && parseFloat($('body').css('width')) < parseFloat($('body').css('height'))){
                $('body').css('width', <?=$imageWidth * 2.1?> );
            }
            $('body').css('-moz-transform','scale('+scale+')');
            $('body').css('-moz-transform-origin','top left');
            $(window).resize();
            is_loading_finish = true;
        }

        <?php if ($engclassicMode){ ?>
        $(".btn_glossary").css("display","");
        <?php }else{?>
        $(".btn_glossary").css("display","none");
        <?php } ?>

        <?php if ($youngreaderMode){ ?>
        $(".btn_glossary").css("display","none");
        $(".btn_feature").css("display","none");
        $(".btn_texttospeech").css("display","none");
        <?php } ?>
    });
</script>
<?php

echo $libebookreader_ui->Get_Layout_Stop();
intranet_closedb();
?>
