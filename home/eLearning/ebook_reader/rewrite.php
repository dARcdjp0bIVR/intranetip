<?php 
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$libebookreader = new libebookreader();

$param = str_replace("/home/eLearning/ebook_reader/", "", $_SERVER['REQUEST_URI']);

$array_param = preg_split('[\\/]', $param, -1, PREG_SPLIT_NO_EMPTY);
$bookID = array_shift($array_param);
if(strtolower($array_param[0])=='reader'){
	array_shift($array_param);
}
$file = implode("/", $array_param);

$ebook_root_path = $intranet_root."/file/eBook/".$bookID."/";

// subfolder should be retrived by querying DB for book ID
//$subfolder = "OEBPS/";
$book = $libebookreader->Get_Book_List($bookID);
$subfolder = trim($book[0]['BookSubFolder']);

$file_path = $ebook_root_path . $subfolder.$file;
/*
if (file_exists($file_path))
{
	$fileext = substr($file, strrpos($file, ".")+1);
	
	switch (strtolower($fileext))
	{
		case "js":
			header("Content-type: text/javascript");
			break;
		case "css":
			header("Content-type: text/css");
			break;
		case "txt":
			header("Content-type: text/plain");
			break;
		case "htm":
		case "xhtml":
		case "html":
			header("Content-type: text/html");
			$isHtml = true;
			break;
		case "jpg":
			header("Content-type: image/jpeg");
			break;
		case "gif":
			header("Content-type: image/gif");
			break;
		case "png":
			header("Content-type: image/png");
			break;
		default:
			header("Content-type: text/plain");
			break;
	}
	$x = file_get_contents($file_path);
	echo $x;
}
else
{
	echo "file: " . $file . " not found"; 
}
*/
echo $libebookreader->Get_Rewrite_File_Content($file_path);	

intranet_closedb();
?>
