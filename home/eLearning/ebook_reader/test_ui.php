<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libebookreader_ui.php");
intranet_auth();
intranet_opendb();

if(!isset($_SESSION['eBookReaderPageMode'])){
	$_SESSION['eBookReaderPageMode'] = 2;
}

$bid = $_REQUEST['bid'];

$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";
$request_url = $_SERVER['REQUEST_URI'];
if($request_url[strlen($request_url)-1] != '/'){
	$request_url .= '/';
}

$libebookreader_ui = new libebookreader_ui();
$libebookreader = $libebookreader_ui->Get_libebookreader();

$parse_success = $libebookreader->Parse_EPUB_XML($ebook_root_path);

if(!$parse_success){
	echo "Fail to parse ebook.";
	intranet_closedb();
	exit;
}

$ebook_doc_path = $libebookreader->Encrypt($libebookreader->ebook_doc_path,$libebookreader->ENCRYPT_KEY);
$ebook_subfolder_path = $libebookreader->Encrypt($libebookreader->ebook_subfolder_path, $libebookreader->ENCRYPT_KEY);
$ebook_http_root_path = $libebookreader->Encrypt("/file/eBook/".$bid."/", $libebookreader->ENCRYPT_KEY);

$js = 'var bookdata = {';
$js .= "'id' : '".$bid."',\n";
$js .= '\'item\' : {},'."\n";
$js .= '\'spine\' : [],'."\n";
$js .= '\'doc_root\' : \''.$ebook_doc_path.'\','."\n";
$js .= '\'http_root\' : \''.$ebook_http_root_path.'\','."\n";
$js .= '\'subfolder\' : \''.$ebook_subfolder_path.'\''."\n";
$js .= '};'."\n";

$pages = '';
$buttons = '&nbsp;[<a href="javascript:jsChangeView(2);">Two Column View</a>] | [<a href="javascript:jsChangeView(1);">Scroll View</a>]';

$items = $libebookreader->opf_items;
$spines = $libebookreader->opf_spine;

if(count($items)>0){
	foreach($items as $key => $val){
		$js .= "bookdata.item['".$key."'] = {'id':'".$val['id']."', 'href':'".$val['href']."', 'media-type':'".$val['media-type']."'};\n";
	}
}
$pages = '';
$delimiter = '';
if(count($spines)>0){
	foreach($spines as $key => $val){
		$js .= "bookdata.spine.push({'idref':'".$val['idref']."'});\n";
		
		$pages .= $delimiter.'<a href="javascript:jsLoadDocument('.$key.');" title="Go to page '.$key.'">'.$key.'</a>';
		$delimiter = ' | ';
	}
}



echo $libebookreader_ui->Get_Layout_Start();
echo $libebookreader_ui->Get_eBook_Reader_Index($_SESSION['eBookReaderPageMode']);
?>
<script language="JavaScript">
<?=$js?>

function jsLoadDocument(i)
{
	var docpath = '';
	if(typeof(bookdata.spine[i]) == 'undefined'){
		return;
	}else if(typeof(bookdata.item[bookdata.spine[i].idref])!='undefined'){
		docpath = bookdata.item[bookdata.spine[i].idref].href;
		$('div#Content').load(
			'<?=$request_url?>' + docpath,
			{
				
			},
			function(data){
				init();
			}
		);
	}
}

function jsChangeView(mode)
{
	var $dc = $('div#Content');
	if(mode==2){
		if(!$dc.hasClass('multicolumnElement'))
			$dc.addClass('multicolumnElement');
	}else{
		$dc.removeClass('multicolumnElement');
	}
}

var ebook = {
	currentPage: 1,
	totalPage: 3,
	pageWidth: 1024,
	movementTolerance: 30,
	tableOfContent: null,

	initialize: function()
	{
		$.ajax({
			type: "GET",
			url: "column_count.htm",
			dataType: "xml",
			success: function(xml) {
				this.tableOfContent = xml; //...
			}
		});
	},

	moveWithinTolerance: function(start, stop)
	{
		return Math.abs(stop - start) <= this.movementTolerance;
	},

	movePage: function(start, stop)
	{
		if ((stop < start) && (this.currentPage + 1 > this.totalPage)) return false;
		if ((stop > start) && (this.currentPage - 1 < 1)) return false;

		if (this.moveWithinTolerance(start, stop))
		{
			return false;
		}
		else
		{
			this.currentPage += (stop < start ? 1 : -1);
			return true;
		}
	}
};

var init = function(){
 
 $("div#Content").draggable({
 	axis: "x",
 	start: function(event, ui) {
 		start = ui.position.left;
 	},
 	stop: function(event, ui) {
 		stop = ui.position.left;

 		if (ebook.movePage(start, stop))
 			$(this).animate({left: "+="+((ebook.pageWidth-Math.abs(stop-start))*(stop>start?1:-1))}, "slow");
 		else
 			$(this).animate({left: "-="+(stop-start)}, "fast");
	}
 });
 var contentObj = document.getElementById('Content'); 
}; 

$(document).ready(function(){
	//jsLoadDocument(0);
});
</script>
<?php
echo $libebookreader_ui->Get_Layout_Stop();
intranet_closedb();
?>