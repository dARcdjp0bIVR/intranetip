<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
intranet_auth();
intranet_opendb();

$bid = $_REQUEST['bid'];
$ebook_root_path = $intranet_root."/file/eBook/".$bid."/";

$request_url = $_SERVER['REQUEST_URI'];
if($request_url[strlen($request_url)-1] != '/'){
	$request_url .= '/';
}

$request_url = "http://192.168.0.146:31002/home/eLearning/ebook_reader/".$bid."/";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<header>
	<meta charset="UTF-8" content="text/html" http-equiv="Content-Type">
	<script src="/home/eLearning/ebook_reader/jquery-1.5.1.min.js" language="JavaScript" ></script>
	<script src="/home/eLearning/ebook_reader/jquery-ui-1.8.11.custom.min.js" language="JavaScript" ></script>
	<script src="/home/eLearning/ebook_reader/jquery.highlight-3.js" language="JavaScript" ></script>
	<style type=text/css>
		#divBook {
			position: absolute;
			width: 1024px;
			height: 768px;
			left: 10px;
			overflow-x: scroll;
			overflow-y: hidden;
			border: #000000 1px solid;
			padding: 20px;
		} 
		
		.multicolumnElement {
		    -moz-column-count: 2;
			-moz-column-width: 200px;
			-moz-column-gap: 30px;
			-webkit-column-count: 2;
			-webkit-column-width: 200px;
			-webkit-column-gap: 30px;
			height: 768px;
			position: relative; 
		}
		
		.highlight {
			background: #FFFF00;
		} 
		
	</style>
</header>
<body>
<script language="JavaScript">
function jsLoad()
{
	var docpath = '9781118235348b03.xhtml';
	$('div#divContent').load(
	'<?=$request_url?>' + docpath,
	{
		
	},
	function(data){
		var dv = document.getElementById('divContent');
		console.log(dv.clientWidth);
		console.log(dv.clientHeight);
		console.log(dv.scrollTop);
		console.log(dv.scrollLeft);
		console.log(dv.innerWidth);
		console.log(dv.innerHeight);
	}
	);
}



$(document).ready(function(){
	jsLoad();
});
</script>
<div id="divBook">
<div id="divContent" class="multicolumnElement"></div>
</div>
</body>
</html>
<?php
intranet_closedb();
?>