<?php
// editing by Carlos
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libebookreader = new libebookreader();

header("Content-type: text/html; charset=utf-8");

echo '<script src="/templates/script.js" language="JavaScript" ></script>';

//$linterface->LAYOUT_START($ReturnMessage);
if(isset($msg)){
	$msg_arr = explode("|=|",$msg);
	echo "<div>".htmlspecialchars($msg_arr[1])."</div><br>";
}

echo "eBooks: ";
echo "<br>";

$books = $libebookreader->Get_Book_List();

for($i=0;$i<count($books);$i++){
	echo '<a href="javascript:newWindow(\''.$books[$i]['BookID'].'/reader/\', 31)">'.htmlspecialchars(($i+1).". ".$books[$i]['BookTitle']).'</a>';
	echo '&nbsp;&nbsp;<a href="delete_update.php?BookID='.$books[$i]['BookID'].'" title="Delete this book">[ X ]</a>';
	echo '<br>';
}
/*
echo '<a href="javascript:newWindow(\'reader.php?bid=1\', 31)">Programming HTML5 Applications</a>';
echo '<br>';

echo '<a href="javascript:newWindow(\'1/\', 31)">Programming HTML5 Applications</a>';
echo '<br>';
*/
echo '<br>';
echo '<a href="upload.php" title="Upload an EPUB book">Upload an EPUB book</a>';
echo '<br>';

//$linterface->LAYOUT_STOP();
intranet_closedb();
?>