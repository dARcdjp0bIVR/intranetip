<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebookreader.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

header("Content-type: text/html; charset=utf-8");

$libebookreader = new libebookreader();
if(isset($_FILES['upload_file'])){
	$success = $libebookreader->Upload_EPUB_Book($_FILES, 'upload_file');
	echo 'Upload EPUB '.$_FILES['upload_file']['name'].($success?' success.' : ' failed.');
	echo '<br>';
	if($success){
		$book_id = $libebookreader->BookID;
		echo 'BookID '.$book_id.'<br>';
		$x = '<div>Page Mode: <span id="divPageMode"></span> page mode<br>';
		$x .= 'Total number of files: <span id="divTotal"></span><br>';
		$x .= 'Current processing file: <span id="divNum"></span><br>';
		$x .= 'Process status: <span id="divStatus">processing...</span><br>';
		$x .= '</div><br>';
		$x .= '<iframe id="PostProcessFrame" width="1px" height="1px" src="/home/eLearning/ebook_manage/'.$book_id.'/reader/"></iframe>';
	}
}

echo '<form method="post" action="upload.php" enctype="multipart/form-data">';
echo '<input type="file" name="upload_file" /><br>';
echo '<input type="submit" name="submit" value="Submit" />';
echo '</form>';

echo $x;

echo '<br>';
echo '<a href="index.php" title="Go back to index page">Back</a>';

intranet_closedb();
?>