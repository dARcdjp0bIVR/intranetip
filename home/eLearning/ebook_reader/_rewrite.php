<?php 
	$param = str_replace("/home/eLearning/ebook_reader/", "", $_SERVER['REQUEST_URI']);

	$array_param = preg_split('[\\/]', $param, -1, PREG_SPLIT_NO_EMPTY);
	$bookID = array_shift($array_param);
	$file = implode("/", $array_param);
	
	$ebook_root_path = "/home/web/eclass40/intranetIP25/file/eBook/".$bookID."/";
	$file_path = $ebook_root_path . "OEBPS/".$file;
	
	if (file_exists($file_path))
	{
		$fileext = substr($file, strrpos($file, ".")+1);
		
		switch (strtolower($fileext))
		{
			case "js":
				header("Content-type: text/javascript");
				break;
			case "css":
				header("Content-type: text/css");
				break;
			case "txt":
				header("Content-type: text/plain");
				break;
			case "htm":
			case "html":
				header("Content-type: text/html");
				break;
			case "jpg":
				header("Content-type: image/jpeg");
				break;
			case "gif":
				header("Content-type: image/gif");
				break;
			case "png":
				header("Content-type: image/png");
				break;
			default:
				header("Content-type: text/plain");
				break;
		}
		echo file_get_contents($file_path);
	}
	else
	{
		echo "file: " . $file . " not found"; 
	}
?>