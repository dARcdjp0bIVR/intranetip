<?php
/// Editing by: 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
intranet_auth();
intranet_opendb();

$ldb = new libdb();

switch($type) {
	case $ies_cfg['HandinFileByTeacher']	:	$table = "IES_WORKSHEET_TEACHER_FILE";
				break;
	case $ies_cfg['HandinFileByStudent']	:	$table = "IES_WORKSHEET_HANDIN_FILE";
				break;
	default	:	break;
}

$sql = "SELECT FileName, CONCAT(FolderPath, FileHashName) ";
$sql .= "FROM $table ";
$sql .= "WHERE FileHashName = '{$fhn}'";

list($raw_file_name, $file_abspath) = current($ldb->returnArray($sql));


$file_abspath = $intranet_root.$file_abspath;


// Check browser agent
$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
else { $agentName = 'unrecognized'; }


if(file_exists($file_abspath)){
	// Output file to browser
	$mime_type = mime_content_type($file_abspath);
	$file_content = file_get_contents($file_abspath);
	$file_name = ($agentName == "mozilla") ? $raw_file_name : iconv("UTF-8", "Big5", $raw_file_name);

	# header to output
	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-type: $mime_type");
	header("Content-Length: ".strlen($file_content));
	header("Content-Disposition: attachment; filename=\"".$file_name."\"");

	echo $file_content;
}else{
	echo 'File Not Found!<br/>';
	exit();
}
?>