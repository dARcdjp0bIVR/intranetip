<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$ldb = new libdb();

$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_STUDENT");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_COMMENT");
$ldb->db_db_query("DELETE FROM {$intranet_db}.IES_STAGE_HANDIN_FILE");

?>