<?php
/// Editing by: 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$ldb = new libdb();

switch(strtoupper($task)) {
	case "FAQ":
		$fileHashName = $_GET["fileHashName"];		
		$sql = "SELECT FILENAME,CONCAT('$intranet_root', FOLDERPATH, FILEHASHNAME) FROM {$intranet_db}.IES_FAQ_FILE WHERE FILEHASHNAME = '{$fileHashName}'";
		break;
	default:	
		$sql = "SELECT FileName, CONCAT('$intranet_root', FolderPath, FileHashName) ";
		$sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_FILE ";
		$sql .= "WHERE FileHashName = '{$fhn}'";
		break;
}
list($raw_file_name, $file_abspath) = current($ldb->returnArray($sql));

// Check browser agent
$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
else { $agentName = 'unrecognized'; }

// Output file to browser
$mime_type = mime_content_type($file_abspath);
$file_content = file_get_contents($file_abspath);
//$file_name = ($agentName == "mozilla") ? $raw_file_name : iconv("UTF-8", "Big5", $raw_file_name);
switch ($agentName) {
	default:
	case "msie":
		$file_name = urlencode($raw_file_name);
		break;
	case "mozilla":
		$file_name = $raw_file_name;
		break;
}

# header to output
header("Pragma: public");
header("Expires: 0"); // set expiration time
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-type: $mime_type");
header("Content-Length: ".strlen($file_content));
header("Content-Disposition: attachment; filename=\"".$file_name."\"");

echo $file_content;

?>