<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

define("CLASS_ROOM_NOT_EXIST", 0);

// temporary access right checking
//if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"]) {
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}
intranet_auth();
intranet_opendb();

$jumpback = "ies";

$roomType = $ies_cfg['DB_course_RoomType'];

$c_id = getEClassRoomID($roomType);

if((intval($c_id) != CLASS_ROOM_NOT_EXIST))
{
	$url = "$eclass_url_root/guest.php?course_id=$c_id&jumpback={$jumpback}";
	header("Location: $eclass_url_root/mode.php?url=".urlencode($url));
}else
{
	echo "Invalid Access Rubric for IES<br/>";
}

intranet_closedb();
?>

