<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

/*
// Save and set steps button
$html_button_display = "<input type=\"submit\" class=\"formbutton\"";
$html_button_display .= "onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" " .
		"value=\"{$Lang['IES']['Button']['SaveAndSetStep']}\" onclick=\"$('#stepSet').val(1)\" />";
*/


$li = new libuser($UserID);
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$fcm = new form_class_manage();
$fcm_ui = new form_class_manage_ui();
$TeacherList = $fcm->Get_Teaching_Staff_List();


$html_teaching_teacher_selection .= '<select name="TeachingTeacherList" id="TeachingTeacherList" onchange="Add_Teaching_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$html_teaching_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$html_teaching_teacher_selection .= '</select>';

$html_assess_teacher_selection .= '<select name="AssessTeacherList" id="AssessTeacherList" onchange="Add_Assess_Teacher();">
				<option value="" selected="selected">'.$Lang['SysMgr']['FormClassMapping']['AddTeacher'].'</option>';
for ($i=0; $i< sizeof($TeacherList); $i++) {
	$html_assess_teacher_selection .= '<option value="'.$TeacherList[$i]['UserID'].'">'.$TeacherList[$i]['Name'].'</option>';
}
$html_assess_teacher_selection .= '</select>';

$html_student_search_input = '<div class="Conntent_search">';
$html_student_search_input .= '<input type="text" id="StudentSearch" name="StudentSearch" value="" />';
$html_student_search_input .= '</div>';

// year class selection
$lclass = new libclass();
$thisAttr = ' id="YearClassSelect" name="YearClassSelect" class="formtextbox" onchange="Get_Class_Student_List();" ';
$CurrentAcademicYearID = Get_Current_Academic_Year_ID();

$CurrentYearClassSelection = $lclass->getSelectClassID($thisAttr, $selected="", $DisplaySelect=1, $CurrentAcademicYearID);

$html_year_class_selection = '<div id="CurrentYearClassSelectionDiv">';
$html_year_class_selection .= $CurrentYearClassSelection;
$html_year_class_selection .= '</div>';

// class student list of selected class
$html_student_source_selection = '<div id="ClassStudentList">';
$html_student_source_selection .= '<select name="ClassStudent" id="ClassStudent" size="10" multiple="true">';
$html_student_source_selection .= '</select>';
$html_student_source_selection .= '</div>';
		

//      									$x .= '<div id="ClassStudentList">';
//      										$x .= $fcm_ui->Get_Student_Without_Class_Selection('ClassStudent');
//      									$x .= '</div>';
		
// selected student list
$html_student_target_selection = '<select name="StudentSelected[]" id="StudentSelected" size="10" multiple="true">';
$html_student_target_selection .= '</select>';

// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "scheme_index.php");
$nav_arr[] = array($button_new, "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);

### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_edit_test.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
