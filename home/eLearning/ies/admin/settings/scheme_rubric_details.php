<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_rubric.php");
//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));


if(empty($schemeID) || !is_int($schemeID))
{
	//echo "Invalide access , Empty SCHEME ID<br/>";
    //exit();
    $schemeID = 0;
}
# Create a new interface instance
$linterface = new interface_html();


$html_schemeID = $schemeID;  // FOR DISPLAY HIDDEN FORM FIELD VALUE

intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$objDB = new libdb();
$li = new libuser($UserID);

$objDB_UI = new libies_ui();


$schemeDetails = $objIES->GET_SCHEME_DETAIL($schemeID);

$stageDetailsAry = $objIES->GET_STAGE_ARR($schemeID);

//$stage_mark_criteria_arr = libies_static::getMarkingCriteria($StageID);

//GET STANDARD RUBRIC
$classRoomID = libies_static::getIESRurbicClassRoomID();

//FOR SAFE , if there is no elcass
$objRubric = new libiesrubric($classRoomID);
$standardRubricSet = $objRubric->getStandardRubricSet();


$criteria_data_select = array();

for($i = 0, $j = sizeof($standardRubricSet); $i< $j; $i++){
	$_tmpID = $standardRubricSet[$i]["STD_RUBRIC_SET_ID"];
	$_tmpTitle = $standardRubricSet[$i]["SET_TITLE"];
	$criteria_data_select[]  = array($_tmpID,$_tmpTitle);
}

$schemeMaxScore = $schemeDetails["MaxScore"];

//$h_content .= "<table class=\"form_table\" border =\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" >\n";
$h_content = "<table border =\"0\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" >\n";
$h_content .= "<tr><td class=\"tabletop\">{$Lang['IES']['Stage']}</td>";
$h_content .= "<td class=\"tabletop\">{$Lang['IES']['Criteria']}</td>";
$h_content .= "<td class=\"tabletop\">{$Lang['IES']['Weight']}</td>";
$h_content .= "<td class=\"tabletop\">{$Lang['IES']['MaxScore']}</td>";
$h_content .= "<td class=\"tabletop\">{$Lang['IES']['Rubric']}</td>";
$h_content .= "<td class=\"tabletop\">&nbsp;</td>";
$h_content .= "<td class=\"tabletop\">{$Lang['IES']['Weight']}</td>";
$h_content .= "<td class=\"tabletop\">{$Lang['IES']['MaxScore']}</td>";
$h_content .= "</tr>";
$trStyle = "";
for($i=0, $j=sizeof($stageDetailsAry); $i < $j; $i++)
{
	//LOOP for each stage
	$_stageTitle = $stageDetailsAry[$i]["Title"];
	$_stageID = $stageDetailsAry[$i]["StageID"];
	$_stageWeight = $stageDetailsAry[$i]["Weight"];
	$_stageMaxScore = $stageDetailsAry[$i]["MaxScore"];
	$stage_mark_criteria_arr = libies_static::getStageMarkingCriteria($schemeID, $_stageID);

	$trStyle = ($trStyle == "#FFFFFF") ? "#F3F3F3" : "#FFFFFF";	

	for($k = 0, $max_k = sizeof($stage_mark_criteria_arr); $k< $max_k; $k++){
		//LOOP for each marking criteria
		$_tempCriteriaID = $stage_mark_criteria_arr[$k]["MarkCriteriaID"];
		if($_tempCriteriaID == MARKING_CRITERIA_OVERALL) continue;    // No rubric for overall mark

		$_stageCriteriaID = $stage_mark_criteria_arr[$k]['StageMarkCriteriaID'];
		$_criteriaTitle = $stage_mark_criteria_arr[$k]['CRITERIATITLE'];
		$_criteriaMaxScore = $stage_mark_criteria_arr[$k]['MaxScore'];
		$_criteriaWeight = $stage_mark_criteria_arr[$k]['Weight'];
		$_selectName = " name = rubricCriteria[{$_stageID}][{$_tempCriteriaID}] ";
		$_rubricDetail = $objRubric->getStageRubric($_stageID, $_tempCriteriaID);
		$_rubricID = (sizeof($_rubricDetail) == 1 && is_array($_rubricDetail)) ? $_rubricDetail[0]["TASK_RUBRIC_ID"] : null;
		if(isset($_rubricID))
		{
			$_taskRubric = current($objRubric->getTaskRubricByRubricID($_rubricID));
			$_taskRubricTitle = $_taskRubric["title"];
		}
//debug_r($_rubricDetails);
		$_criteriaMaxScoreReadonly = isset($_rubricID) ? "READONLY" : "";
		$_criteriaMaxScoreNoteReadonly = isset($_rubricID) ? "<span class=\"tabletextrequire\">#</span>" : "";
		$h_rubric_set = !isset($_rubricID) ? getSelectByArray($criteria_data_select,$_selectName,$_rubricID) : $_taskRubricTitle;
		$h_rubric_edit = isset($_rubricID) ? "<a href=\"scheme_stage_rubric_edit.php?task_rubric_id={$_rubricID}&KeepThis=true&TB_iframe=true&height=400&width=700\" class=\"thickbox edit_dim\" title=\"{$Lang['IES']['Edit']}\"></a>" : "&nbsp;";
		$h_rubric_delete = isset($_rubricID) ? "<a href=\"javascript:Remove_Task_Rubric({$_rubricID})\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete']}\"></a>" : "&nbsp;";

		if($k == 0){
			//k == 0 , displaying / handling first row of the table
			$h_content .= "<tr bgcolor=\"{$trStyle}\">";
			$h_content .= "<td width=\"15%\">{$_stageTitle}<input type=\"hidden\" name=\"stageID[]\" value=\"{$_stageID}\" /></td>";
			$h_content .= "<td width=\"15%\">{$_criteriaTitle}<input type=\"hidden\" name=\"stageCriteriaID[]\" value=\"{$_stageCriteriaID}\" /></td>";
			$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"stageCriteriaWeight[{$_stageCriteriaID}]\" value=\"{$_criteriaWeight}\" size=\"3\" /></td>";
			$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"stageCriteriaMaxScore[{$_stageCriteriaID}]\" value=\"{$_criteriaMaxScore}\" size=\"3\" {$_criteriaMaxScoreReadonly} /> {$_criteriaMaxScoreNoteReadonly}</td>";
			$h_content .= "<td width=\"30%\">{$h_rubric_set}</td>";
			$h_content .= "<td width=\"8%\"><span class=\"table_row_tool\">{$h_rubric_edit} {$h_rubric_delete}</span></td>";
			$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"stageWeight[{$_stageID}]\" value=\"{$_stageWeight}\" size=\"3\" /></td>";
			$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"stageMaxScore[{$_stageID}]\" value=\"{$_stageMaxScore}\" size=\"3\" /></td>";
			$h_content .= "</tr>";
		}else{
			$h_content .= "<tr bgcolor=\"{$trStyle}\">";
			$h_content .= "<td width=\"15%\">&nbsp;</td>";
			$h_content .= "<td width=\"15%\">{$_criteriaTitle}<input type=\"hidden\" name=\"stageCriteriaID[]\" value=\"{$_stageCriteriaID}\" /></td>";
			$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"stageCriteriaWeight[{$_stageCriteriaID}]\" value=\"{$_criteriaWeight}\" size=\"3\" /></td>";
			$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"stageCriteriaMaxScore[{$_stageCriteriaID}]\" value=\"{$_criteriaMaxScore}\" size=\"3\" {$_criteriaMaxScoreReadonly} /> {$_criteriaMaxScoreNoteReadonly}</td>";
			$h_content .= "<td width=\"30%\">{$h_rubric_set}</td>";
			$h_content .= "<td width=\"8%\"><span class=\"table_row_tool\">{$h_rubric_edit} {$h_rubric_delete}</span></td>";
			$h_content .= "<td width=\"8%\">&nbsp;</td>";
			$h_content .= "<td width=\"8%\">&nbsp;</td>";
			$h_content .= "</tr>";
		}
	}
}
$h_content .= "<tr bgcolor=\"#FFFFFF\">";
$h_content .= "<td width=\"15%\">&nbsp;</td>";
$h_content .= "<td width=\"15%\">&nbsp;</td>";
$h_content .= "<td width=\"8%\">&nbsp;</td>";
$h_content .= "<td width=\"8%\">&nbsp;</td>";
$h_content .= "<td width=\"30%\">&nbsp;</td>";
$h_content .= "<td width=\"8%\">&nbsp;</td>";
$h_content .= "<td width=\"8%\">{$Lang['IES']['MaxScore']}</td>";
$h_content .= "<td width=\"8%\"><input type=\"text\" name=\"schemeMaxScore\" value=\"{$schemeMaxScore}\" size=\"3\" /></td>";
$h_content .= "</tr>";

$h_content .= "</table>";



$schemeTitle =$schemeDetails["Title"];
$html_schemeTitle = $schemeTitle;
//get scheme teacher

//FUNCTION TAB HANDLING
$currentTagFunction = "rubricSetUp";
$tabArray = $ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"];

$tabAppendParameter = "?schemeID={$schemeID}";
$newTab = $objIES->appendParameterToTab($tabAppendParameter,$tabArray);
$html_functionTab = $objIES->GET_TAB_MENU($currentTagFunction,$newTab);	


// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "scheme_index.php");
$nav_arr[] = array($html_schemeTitle, "");




$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";
### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_rubric_details.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();


?>