<?php
######################## being used in /home/web/eclass40/intranetIP25/addon/ip25/index.php

//$PATH_WRT_ROOT = "../../../../../";
//include_once($PATH_WRT_ROOT."includes/global.php");
//include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/ies/libies.php");
//
//intranet_auth();
//intranet_opendb();
function IES_WEIGHT_PATCH_3() {
	global $PATH_WRT_ROOT;
$libies = new libies();
$mode = ini_get('zend.ze1_compatibility_mode');
ini_set('zend.ze1_compatibility_mode', '0');
$schemeDefaultXML = null;

$xmlPath = $PATH_WRT_ROOT."home/eLearning/ies/admin/settings/scheme.default.xml";
if(file_exists($xmlPath)){
	$xmlstr = get_file_content($xmlPath);
	$schemeDefaultXML = new SimpleXMLElement($xmlstr);
}
foreach($schemeDefaultXML->stage as $scheme_key => $stage) {
	$stage_sequence = (int) $stage->sequence;
	if ($stage_sequence==3) {
		$stage_weight[$stage_sequence] = (float) $stage->weight;
	}
}

ini_set('zend.ze1_compatibility_mode', $mode);

$sql = <<<SQLEND
SELECT IST.STAGEID FROM {$intranet_db}.IES_STAGE IST INNER JOIN {$intranet_db}.IES_SCHEME ISC ON IST.SCHEMEID = ISC.SCHEMEID WHERE ISC.LANGUAGE = 'en' and ISC.SchemeType = 'ies'
SQLEND;
$stageidEN = $libies->returnVector($sql);


foreach($stage_weight as $seq=>$weight) {
		$ids = implode(",",$stageidEN);
		$sql =<<<SQLEND
UPDATE {$intranet_db}.IES_STAGE SET WEIGHT = '{$weight}' WHERE SEQUENCE = {$seq} AND STAGEID IN ({$ids})
SQLEND;
		$libies->db_db_query($sql);
}
}

//intranet_closedb();
?>