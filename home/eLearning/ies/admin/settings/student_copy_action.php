<?php
/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_move_student.php");


//http://192.168.0.146:31002/home/eLearning/ies/admin/settings/student_copy_action.php
intranet_auth();
intranet_opendb();
	$objIES = new libies();
	if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
		$objIES->NO_ACCESS_RIGHT_REDIRECT();
	}


	$trgSchemeID = intval(trim($_POST["trgSchemeID"]));
//$scheme_name = trim($scheme_name);

    $SelectedStudentArr = $_POST["SelectedStudentArr"];
    
  //  debug_r($trgSchemeID);
  //  debug_r($SelectedStudentArr);

	$stuCopy = new libies_move_student();
//	$srcSchemeID = 619;
//	$trgSchemeID = 621;
//	$studentID = 1698;

	$SuccessArr = array();
	for($i=0, $i_MAX = count($SelectedStudentArr);$i<$i_MAX;$i++)
	{
		$UserID_srcScheme_Arr = explode('_',$SelectedStudentArr[$i]);
		$_studentID = $UserID_srcScheme_Arr[0];
		$_srcSchemeID = $UserID_srcScheme_Arr[1];
		
		//debug_r($UserID);
		//debug_r($srcSchemeID);
		
		$SuccessArr[$i] = $stuCopy->CopyStudentWithScheme($_srcSchemeID,$trgSchemeID,$_studentID);
		
		//debug_r($SuccessArr);
	}
	
	if (in_array(false, $SuccessArr))
	  {
	  	
	    $msg = "add_failed";
	  }
	  else
	  {
	    $msg = "add";
	  }
  	  header("Location: scheme_edit.php?schemeID=$trgSchemeID&msg=".$msg);
  	

		//return $SuccessArr;

//	$SuccessArr = $stuCopy->CopyStudentWithScheme($srcSchemeID,$trgSchemeID,$studentID);



  intranet_closedb();
  
  //redirect
?>
