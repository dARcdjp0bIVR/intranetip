<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_rubric.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$classRoomID = libies_static::getIESRurbicClassRoomID();
$objRubric = new libiesrubric($classRoomID);

$task_rubric_arr = $objRubric->getTaskRubricByRubricID($task_rubric_id);
$task_rubric_detail_arr = $objRubric->getRubricDetail($task_rubric_id);

for($i=0, $i_max=count($task_rubric_arr); $i<$i_max; $i++)
{
	$_rubric_id = $task_rubric_arr[$i]["rubric_id"];
	$_title = $task_rubric_arr[$i]["title"];
	$_rubric_detail_cnt = count($task_rubric_detail_arr);
	$_showorder = $task_rubric_arr[$i]["showorder"];
	
	$html_rubric_detail_row .= <<<HTML
<tr id="tr_std_rubric_{$_rubric_id}">
	<td>
		<input type="text" id="task_rubric_title_{$_rubric_id}" name="task_rubric_title[{$_rubric_id}]" value="{$_title}"  size="60" class="textbox"/>
		<p class="spacer"></p>
		<table id="tb_task_rubric_detail_{$_rubric_id}" class="common_table_list" style="border:1px dashed #CCCCCC">
			<thead>
				<col width="5%">
				<col width="25%">
				<col width="20%">
				<col width="50%">
			</thead>  
			<tbody>
				<tr>
					<td class="num_check">&nbsp;</td>
					<td nowrap>{$Lang['IES']['ScoreRange']}</td>
					<td >{$Lang['IES']['Level']}</td>
					<td >{$Lang['IES']['Description']}</td>
				</tr>
HTML;

	for($j=0, $j_max=count($task_rubric_detail_arr); $j<$j_max; $j++)
	{
		$_score = $task_rubric_detail_arr[$j]["score"];
		$_score_from = $task_rubric_detail_arr[$j]["score_from"];
		$_score_to = $task_rubric_detail_arr[$j]["score_to"];
		$_desc = $task_rubric_detail_arr[$j]["description"];
	
		$html_rubric_detail_row .= <<<HTML
				<tr class="sub_row"><!--tr_rubric_detail_-->
					<td><span class="table_row_tool"><a href="javascript:;" class="delete_dim" title="New Criteria">&nbsp;</a></span></td>
					<td class="mark_range">
						<input name="rubric_detail[{$_rubric_id}][]" type="hidden" />
						<input name="score_from[{$_rubric_id}][]" type="text" value="{$_score_from}" size="4" class="textbox" />
						{$i_To}
						<input name="score_to[{$_rubric_id}][]" type="text" value="{$_score_to}"  size="4" class="textbox"/ >
					</td>
					<td><input type="text" name="score[{$_rubric_id}][]" value="{$_score}" size="4" class="textbox"/></td>
					<td>
						<textarea name="desc[{$_rubric_id}][]" rows="2" wrap="virtual" style="width:90%">{$_desc}</textarea>
					</td>
				</tr>
HTML;
	}
	
	$html_rubric_detail_row .= <<<HTML
				<TR id="addRow" class="sub_row">
					<TD COLSPAN="4">
						<SPAN class="table_row_tool">
							<A class="add_dim" title="undefined" href="javascript:add_new_rubric_range_detail_row('{$_rubric_id}')">&nbsp;</A>
						</SPAN>
					</TD>
				</TR>
			</tbody>
		</table>

		<input type="hidden" id="task_rubric_id_{$_rubric_id}" name="task_rubric_id[]" value="{$_rubric_id}"/>
	  <!--<input type="hidden" id="task_rubric_detail_cnt_{$_rubric_id}" name="task_rubric_detail_cnt[{$_rubric_id}]" value="{$_rubric_detail_cnt}" />-->
	  <!--<input type="hidden" id="task_rubric_showorder_{$_rubric_id}" name="task_rubric_showorder[{$_rubric_id}]" value="{$_showorder}" />-->
	</td>
</tr>
HTML;

}

$linterface = new interface_html("popup5.html");
$linterface->LAYOUT_START();
?>

<script language="JavaScript">

var clonedSrcRowObj;

function add_new_rubric_range_detail_row(task_rubric_id)
{
	var clonedRowObj = $(clonedSrcRowObj).clone();

	$("#addRow").before(
		$(clonedRowObj)
	);
}

function checkform(){
	var rubricDetailCnt = $("table[id*=tb_task_rubric_detail_] tbody").children().length - 2;		// Exclude head row and add row
	
	if(rubricDetailCnt == 0)
	{
		alert("<?=$Lang['IES']['Warning']['AtLeastOneDescriptor']?>");
		return false;
	}
	else
	{
		return true;
	}
}

$(document).ready(function(){

	clonedSrcRowObj = $("#addRow").prev().clone(true);	// Found previous sibiling (last input row) and clone
	$(clonedSrcRowObj).find("textarea").val("");
	$(clonedSrcRowObj).find("input[type=text]").val("");

	$(".delete_dim").click(function(){
		$(this).parent().parent().parent().remove();
	});

});

</script>

<form name="form1" method="POST" action="scheme_stage_rubric_update.php" onSubmit="return checkform();">

	<p class="spacer"></p>
	<table id="tb_task_rubric" class="common_table_list normal_table_list" align="center" style="width:95%">
		<tr class="nodrop nodrag">
			<th><?=$Lang['IES']['CriteriaDesc']?></th>
		</tr>
		
		<?=$html_rubric_detail_row?>
	</table>
  
  <br />
  <p class="spacer"></p>
  <div class="edit_bottom">
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Submit']?>" />
    <input onclick="parent.window.tb_remove();" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Cancel']?>" />
  </div>

</form>

<?php
//DEBUG_R($task_rubric_detail_arr);

$linterface->LAYOUT_STOP();
intranet_closedb();

?>