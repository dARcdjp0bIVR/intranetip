<?php
//modifying By thomas
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

//$task_arr = $objIES->GET_TASK_ARR($stageID);
$sql = "SELECT TaskID, Title, Enable FROM {$intranet_db}.IES_TASK WHERE StageID = '".$stageID."' ORDER BY Sequence";
$task_arr = $objIES->returnArray($sql);

$html_task_selection = "<select name=\"TaskID[]\" multiple size=\"".count($task_arr)."\" style=\"width:100%\">";
for($i=0, $i_max=count($task_arr); $i<$i_max; $i++)
{
	$_taskID     = $task_arr[$i]["TaskID"];
	$_taskTitle  = $task_arr[$i]["Enable"]? $task_arr[$i]["Title"] : $task_arr[$i]["Title"]." (".$Lang['IES']['Disable'].")";
	$_taskEnable = $task_arr[$i]["Enable"]? "" : "disabled=\"disabled\"";
	$_taskStyle  = $task_arr[$i]["Enable"]? "" : "style=\"font-style:italic\"";

	$html_task_selection .= "<option value=\"{$_taskID}\" {$_taskEnable} {$_taskStyle}>{$_taskTitle}</option>";
}
$html_task_selection .= "</select>";

$linterface = new interface_html("popup5.html");
$linterface->LAYOUT_START();
?>

<form name="form1" method="POST" action="scheme_add_doc_section_task_update.php" onSubmit="return checkform();">

	<p class="spacer"></p>
	<table align="center" style="width:95%">
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle" width="30%"><?=$Lang['IES']['Task']?></td>
			<td width="65%"><?=$html_task_selection?></td>
		</tr>
	</table>

  <br />
  <p class="spacer"></p>
  <div class="edit_bottom">
    <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Submit']?>" />
    <input onclick="parent.window.tb_remove();" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['Btn']['Cancel']?>" />
  </div>

<input type="hidden" name="SchemeID" value="<?=$schemeID?>" />
<input type="hidden" name="StageID" value="<?=$stageID?>" />
<input type="hidden" name="SectionID" value="<?=$sectionID?>" />
</form>

<?php
//DEBUG_R($task_rubric_detail_arr);

$linterface->LAYOUT_STOP();
intranet_closedb();

?>