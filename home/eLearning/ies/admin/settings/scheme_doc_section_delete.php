<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li = new libdb();

/*
$sql = "DELETE isds, isdst ";
$sql .= "FROM {$intranet_db}.IES_STAGE_DOC_SECTION isds ";
$sql .= "INNER JOIN {$intranet_db}.IES_STAGE_DOC_SECTION_TASK isdst ON isds.SectionID = isdst.SectionID ";
$sql .= "WHERE isds.SectionID = {$SectionID}";
$li->db_db_query($sql);
*/

$sql = "DELETE FROM {$intranet_db}.IES_STAGE_DOC_SECTION WHERE SectionID = {$SectionID}";
$li->db_db_query($sql);
$sql = "DELETE FROM {$intranet_db}.IES_STAGE_DOC_SECTION_TASK WHERE SectionID = {$SectionID}";
$li->db_db_query($sql);

echo interface_html::GET_SYS_MSG("delete");

intranet_closedb();
?>