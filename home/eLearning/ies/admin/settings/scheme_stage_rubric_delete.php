<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

include_once($PATH_WRT_ROOT."includes/ies/libies.php");

include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_rubric.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$classRoomID = libies_static::getIESRurbicClassRoomID();
$objRubric = new libiesrubric($classRoomID);

$objRubric->Start_Trans();

$sql = "DELETE FROM task_rubric_detail WHERE rubric_id = {$taskRubricID}";
$res[] = $objRubric->db_db_query($sql);

$sql = "DELETE FROM task_rubric WHERE rubric_id = {$taskRubricID}";
$res[] = $objRubric->db_db_query($sql);

$sql = "UPDATE {$intranet_db}.IES_STAGE_MARKING_CRITERIA SET task_rubric_id = NULL WHERE task_rubric_id = {$taskRubricID}";
$res[] = $objRubric->db_db_query($sql);

$final_res = (count($res) == count(array_filter($res)));

if($final_res)
{
	$objRubric->Commit_Trans();
	$msg = "delete";
}
else
{
	$objRubric->RollBack_Trans();
	$msg = "delete_failed";
}

intranet_closedb();
header("Location: scheme_rubric_details.php?schemeID={$schemeID}&msg={$msg}");

?>