<script language="JavaScript">
function Check_Form() {
  if($("#scheme_name").val() == "")
  {
    alert("<?=$Lang['IES']['Warning']['FillInSchemeName']?>");
    return false;
  }

  $("#StudentSelected").each(function(){
    $("#StudentSelected option").attr("selected","selected");
  });
  $("#SelectedTeachingTeacher").each(function(){
    $("#SelectedTeachingTeacher option").attr("selected","selected");
  });
  $("#SelectedAssessTeacher").each(function(){
    $("#SelectedAssessTeacher option").attr("selected","selected");
  });
  return true;
}

function Add_Teaching_Teacher(TeacherID) {

	var TeacherList = document.getElementById('TeachingTeacherList');
	var Teacher = TeacherList.options[TeacherList.selectedIndex];
	var TeachingStaffList = document.getElementById('SelectedTeachingTeacher[]');

	if (Teacher.value != "") {
		TeachingStaffList.options[TeachingStaffList.length] = new Option(Teacher.text,Teacher.value);
		TeacherList.options[TeacherList.selectedIndex] = null;
		Reorder_Selection_List('SelectedTeachingTeacher[]');
	}
}

function Remove_Teaching_Teacher() {

	var TeachingStaffList = document.getElementById('SelectedTeachingTeacher[]');
	var TeacherList = document.getElementById('TeachingTeacherList');

	for (var i = (TeachingStaffList.length -1); i >= 0 ; i--) {
		if (TeachingStaffList.options[i].selected) {
			Teacher = TeachingStaffList.options[i];
			TeacherList.options[TeacherList.length] = new Option(Teacher.text,Teacher.value);
			TeachingStaffList.options[i] = null;	
		}
	}

	TeacherList.options[0].selected = true;
	Reorder_Selection_List('TeachingTeacherList');
}

function Add_Assess_Teacher(TeacherID) {

	var TeacherList = document.getElementById('AssessTeacherList');
	var Teacher = TeacherList.options[TeacherList.selectedIndex];
	var TeachingStaffList = document.getElementById('SelectedAssessTeacher[]');

	if (Teacher.value != "") {
		TeachingStaffList.options[TeachingStaffList.length] = new Option(Teacher.text,Teacher.value);
		TeacherList.options[TeacherList.selectedIndex] = null;
		Reorder_Selection_List('SelectedAssessTeacher[]');
	}
}

function Remove_Assess_Teacher() {

	var TeachingStaffList = document.getElementById('SelectedAssessTeacher[]');
	var TeacherList = document.getElementById('AssessTeacherList');

	for (var i = (TeachingStaffList.length -1); i >= 0 ; i--) {
		if (TeachingStaffList.options[i].selected) {
			Teacher = TeachingStaffList.options[i];
			TeacherList.options[TeacherList.length] = new Option(Teacher.text,Teacher.value);
			TeachingStaffList.options[i] = null;	
		}
	}

	TeacherList.options[0].selected = true;
	Reorder_Selection_List('AssessTeacherList');
}

function Reorder_Selection_List(selectId) {

	var selectList = document.getElementById(selectId);

	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
  }
}

function Get_Class_Student_List() {

	var YearClassID = $("#YearClassSelect :selected").val();
	var SelectedStudentID = "";
	$("#StudentSelected option").each(function(i){
    SelectedStudentID += "&StudentSelected[]=" + $(this).val();
  });
  
  $("#ClassStudent").find("option").remove().end();
	
	var url = "../../ajax/ajax_get_ies_student_list.php";
	var postContent = "YearClassID="+YearClassID;
	postContent += SelectedStudentID;
	
  $.ajax({
    type: "POST",
    url: url,
    data: postContent,
    success: function(data) {
      var student_arr = eval(data);
      
      $.each(student_arr, function(key, obj) {
        var student_id = obj.UserID;
        var student_name = obj.StudentName;
      
        $("#ClassStudent").append($("<option></option>").attr("value",student_id).text(student_name));   
        $("#ClassStudent").width($("#ClassStudent").parent().width());
        Reorder_Selection_List('ClassStudent');
      });
    }
  });
}

function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    var student_id = $(this).val();
    var student_name = $(this).text();
	
    $("#"+to_select_id).append($("<option></option>").attr("value",student_id).text(student_name));
    $(this).remove();
  });
  
  $("#"+from_select_id).width($("#"+from_select_id).parent().width());
  $("#"+to_select_id).width($("#"+to_select_id).parent().width());
  Reorder_Selection_List(from_select_id);
  Reorder_Selection_List(to_select_id);
}

$(document).ready(function(){
  $("#AddAll").click(function(){
    $("#ClassStudent option").each(function(){$(this).attr("selected","selected");}); 
    MoveSelectedOptions("ClassStudent", "StudentSelected");
  });
  
  $("#Add").click(function(){
    MoveSelectedOptions("ClassStudent", "StudentSelected");
  });

  $("#Remove").click(function(){
    MoveSelectedOptions("StudentSelected", "ClassStudent");
  });

  $("#RemoveAll").click(function(){
    $("#StudentSelected option").each(function(){$(this).attr("selected","selected");});
    MoveSelectedOptions("StudentSelected", "ClassStudent");
  });
  
  $("#scheme_new").submit(function() {
    //return Check_Form();
  });
  
  $("#ClassStudent").width($("#ClassStudent").parent().width());
  $("#StudentSelected").width($("#StudentSelected").parent().width());
});

</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<div align="right">
<?=$html_message?>
</div>

<br style="clear:both" />
<form id="scheme_new" method="POST" action="scheme_edit_details_update.php">
<?=$html_functionTab?>
<table class="form_table">
  <col class="field_title" />
  <col class="field_c" />
  <tr>
    <td><?=$Lang['IES']['FolderDesc']?></td>
    <td>:</td>
    <td><?=$linterface->GET_TEXTAREA("folder_desc", $html_FolderDesc)?></td>
  </tr>
<?php if($plugin['IES_bio']) { ?>
  <tr>
    <td><?=$Lang['IES']['TextDesc']?></td>
    <td>:</td>
    <td><?=$linterface->GET_TEXTAREA("text_desc", $html_TextDesc)?></td>
  </tr>
<?php } ?>
  <tr>
    <td><?=$Lang['IES']['NoteDesc']?></td>
    <td>:</td>
    <td><?=$linterface->GET_TEXTAREA("note_desc", $html_NoteDesc)?></td>
  </tr>
<?php if($plugin['IES_worksheet']) { ?>
  <tr>
    <td><?=$Lang['IES']['WorksheetDesc']?></td>
    <td>:</td>
    <td><?=$linterface->GET_TEXTAREA("worksheet_desc", $html_WorksheetDesc)?></td>
  </tr>
<?php } ?>
  <tr>
    <td><?=$Lang['IES']['FAQDesc']?></td>
    <td>:</td>
    <td><?=$linterface->GET_TEXTAREA("faq_desc", $html_FAQDesc)?></td>
  </tr>
 
</table>

<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="submit" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>" onclick="$('#stepSet').val(0)" />
   
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="window.location='scheme_index.php'" />
  <p class="spacer"></p>
</div>

<input type="hidden" name="stepSet" id="stepSet" />
<input type="hidden" name="schemeID" id="schemeID" value="<?=$html_schemeID?>"/>
</form>
