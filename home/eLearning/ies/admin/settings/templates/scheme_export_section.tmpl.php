<script type="text/javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>	
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="JavaScript">
function Check_Form() {
  return true;
}

function ResetSection(toDefault){
	//$("#scheme_reset").find("input[name=toDefault]").val(toDefault);
	$("#scheme_reset").submit();
}

$(document).ready(function(){
	// Initialise the table
	$(".ClassDragAndDrop").tableDnD({
		dragHandle: "Dragable",
		onDragClass: "move_selected",
		onDrop:	function(table, row){
							switch($(table).attr("id"))
							{
								case "<?=$ies_cfg['DocSectionType']['section']?>":
									var stageID = $(table).attr("stageID");
								
									$.ajax({
										type: "POST",
										url: "scheme_doc_section_order_update.php",
										data: "type=<?=$ies_cfg['DocSectionType']['section']?>&StageID="+stageID+"&"+$.tableDnD.serialize(),
										success: function(msg){
										}
									});
								break;
								case "<?=$ies_cfg['DocSectionType']['task']?>":
									var sectionID = $(table).attr("sectionID");
								
									$.ajax({
										type: "POST",
										url: "scheme_doc_section_order_update.php",
										data: "type=<?=$ies_cfg['DocSectionType']['task']?>&SectionID="+sectionID+"&"+$.tableDnD.serialize(),
										success: function(msg){
										}
									});
								break;
							}
						}
	});
	
	$(".delete_dim").click(function(){
		var taskID = $(this).attr("taskID");
		var sectionID = $(this).attr("sectionID");

		if(confirm(globalAlertMsg3))
		{
			if(typeof(taskID) == "undefined")
			{
				// Delete section
				$.ajax({
					type: "POST",
					url: "scheme_doc_section_delete.php",
					data: "SectionID="+sectionID,
					success: function(msg){
						$("#"+sectionID).remove();
						$("#message").html(msg);
					}
				});
			}
			else
			{
				// Delete task
				$.ajax({
					type: "POST",
					url: "scheme_doc_section_task_delete.php",
					data: "SectionID="+sectionID+"&TaskID="+taskID,
					success: function(msg){
						$("#"+sectionID).find("#"+taskID).remove();
						$("#message").html(msg);
					}
				});
			}
		}
	});
	
	$("a[name=resetSecLnk]").click(function(){
		var schemeID = $(this).attr("schemeID");
		var stageSeq = $(this).attr("stageSeq");
		
		$("#scheme_reset").find("input[name=schemeID]").val(schemeID);
		$("#scheme_reset").find("input[name=stageSeq]").val(stageSeq);
		
		tb_show("<?=$Lang['IES']['Reset']?>", "#TB_inline?height=150&width=300&inlineId=confirm2Default", null);
	});
});

</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->
<div id="message" align="right">
	<?=$html_message?>
</div>
<table width = "100%">
  <tr>
  	<td><?= $linterface->Get_Warning_Message_Box($eDiscipline["Instruction"], $Lang['IES']['WordDocSettingsInstruction'], "")?></td>
  </tr>
</table>
<br style="clear:both" />

<form id="scheme_new" method="POST" action="scheme_rubric_details_update.php">
<?=$html_functionTab?>
<?=$html_stage_doc_section?>

<input type="hidden" name="schemeID" id="schemeID" value="<?=$html_schemeID?>"/>
</form>

<div id="confirm2Default" style="display:none;">
	<form id="scheme_reset" method="POST" action="scheme_doc_section_reset.php">
		<table width="100%">
			<tr>
				<td colspan="2"><?=$Lang['IES']['ResetDoc']['SelectMode']?></td>
			</tr>
			<tr>
				<td width="10%"><input type="radio" id="toDefault_1" name="toDefault" value="1" checked="checked"></td>
				<td><label for="toDefault_1"><?=$Lang['IES']['ResetDoc']['DefaultMode']?></label></td>
			</tr>
			<tr>
				<td width="10%"><input type="radio" id="toDefault_0" name="toDefault" value="0"></td>
				<td><label for="toDefault_0"><?=$Lang['IES']['ResetDoc']['DeleteMode']?></label></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br/>
					<?=$html_reset_btn?>
					<?=$html_cancel_reset_btn?>
				</td>
			</tr>
		</table>
		
		<input type="hidden" name="schemeID" />
		<input type="hidden" name="stageSeq" />
	</form>
</div>