<script language="JavaScript">
function Check_Form() {
  if($("#scheme_name").val() == "")
  {
    alert("<?=$Lang['IES']['Warning']['FillInSchemeName']?>");
    return false;
  }

  $("#StudentSelected").each(function(){
    $("#StudentSelected option").attr("selected","selected");
  });
  $("#SelectedTeachingTeacher").each(function(){
    $("#SelectedTeachingTeacher option").attr("selected","selected");
  });
  $("#SelectedAssessTeacher").each(function(){
    $("#SelectedAssessTeacher option").attr("selected","selected");
  });
  return true;
}

function Add_Teaching_Teacher() {

  var selectedOption = $("#TeachingTeacherList option:selected");
  
  if($(selectedOption).val() != "") {
    $('#SelectedTeachingTeacher').append($('<option></option>').text($(selectedOption).text()).val($(selectedOption).val()));
    $(selectedOption).remove();
    Reorder_Selection_List('SelectedTeachingTeacher');
  }
  
  $('#SelectedTeachingTeacher').width('350');
}

function Remove_Teaching_Teacher() {
  
  var selectedOptions = $("#SelectedTeachingTeacher option:selected");
  
  $(selectedOptions).each(function(){
    $("#TeachingTeacherList").append($('<option></option>').text($(this).text()).val($(this).val()));
    $(this).remove();
  });
  
  Reorder_Selection_List('TeachingTeacherList');
}

function Add_Assess_Teacher() {

  var selectedOption = $("#AssessTeacherList option:selected");
  
  if($(selectedOption).val() != "") {
    $('#SelectedAssessTeacher').append($('<option></option>').text($(selectedOption).text()).val($(selectedOption).val()));
    $(selectedOption).remove();
    Reorder_Selection_List('SelectedAssessTeacher');
  }
  
  $('#SelectedAssessTeacher').width('350');
}

function Remove_Assess_Teacher() {

  var selectedOptions = $("#SelectedAssessTeacher option:selected");
  
  $(selectedOptions).each(function(){
    $("#AssessTeacherList").append($('<option></option>').text($(this).text()).val($(this).val()));
    $(this).remove();
  });
  
  Reorder_Selection_List('AssessTeacherList');
}

function Reorder_Selection_List(selectId) {

	var selectList = document.getElementById(selectId);

	for (var i = 0; i < selectList.length; i++) {
		for (var j=i; j < selectList.length; j++) {
			if (selectList.options[i].text.toLowerCase() > selectList.options[j].text.toLowerCase()) {
				var tempOption1 = new Option(selectList.options[i].text,selectList.options[i].value);
				var tempOption2 = new Option(selectList.options[j].text,selectList.options[j].value);
				selectList.options[i] = tempOption2;
				selectList.options[j] = tempOption1;
			}
		}
  }
}

function Get_Class_Student_List() {

	var YearClassID = $("#YearClassSelect :selected").val();
	var SelectedStudentID = "";
	$("#StudentSelected option").each(function(i){
    SelectedStudentID += "&StudentSelected[]=" + $(this).val();
  });
  
  $("#ClassStudent").find("option").remove().end();
	
	var url = "../../ajax/ajax_get_ies_student_list.php";
	var postContent = "YearClassID="+YearClassID;
	postContent += SelectedStudentID;
	
  $.ajax({
    type: "POST",
    url: url,
    data: postContent,
    success: function(data) {
      var student_arr = eval(data);
      
      $.each(student_arr, function(key, obj) {
        var student_id = obj.UserID;
        var student_name = obj.StudentName;
      
        $("#ClassStudent").append($("<option></option>").attr("value",student_id).text(student_name));   
        $("#ClassStudent").width($("#ClassStudent").parent().width());
        Reorder_Selection_List('ClassStudent');
      });
    }
  });
}

function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    var student_id = $(this).val();
    var student_name = $(this).text();
	
    $("#"+to_select_id).append($("<option></option>").attr("value",student_id).text(student_name));
    $(this).remove();
  });
  
  $("#"+from_select_id).width($("#"+from_select_id).parent().width());
  $("#"+to_select_id).width($("#"+to_select_id).parent().width());
  Reorder_Selection_List(from_select_id);
  Reorder_Selection_List(to_select_id);
}

$(document).ready(function(){
  $('#SelectedTeachingTeacher').width('350');
  $('#SelectedAssessTeacher').width('350');

  $("#AddAll").click(function(){
    $("#ClassStudent option").each(function(){$(this).attr("selected","selected");}); 
    MoveSelectedOptions("ClassStudent", "StudentSelected");
  });
  
  $("#Add").click(function(){
    MoveSelectedOptions("ClassStudent", "StudentSelected");
  });

  $("#Remove").click(function(){
    MoveSelectedOptions("StudentSelected", "ClassStudent");
  });

  $("#RemoveAll").click(function(){
    $("#StudentSelected option").each(function(){$(this).attr("selected","selected");});
    MoveSelectedOptions("StudentSelected", "ClassStudent");
  });
  
  $("#scheme_new").submit(function() {
    return Check_Form();
  });
  
  $("#ClassStudent").width($("#ClassStudent").parent().width());
  $("#StudentSelected").width($("#StudentSelected").parent().width());
});

</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
</div><!-- navigation end -->

<br style="clear:both" />
<form id="scheme_new" method="POST" action="scheme_update_test.php">
<?=$html_functionTab?>
<table class="form_table">
  <col class="field_title" />
  <col class="field_c" />
  <tr>
    <td><?=$Lang['IES']['SchemeTitle']?></td>
    <td>:</td>
    <td><input name="scheme_name" type="text" id="scheme_name" class="textbox" value="<?=$html_schemeTitle?>"/></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['TeachingTeacher']?></td>
    <td>:</td>
    <td>
    <?=$html_teaching_teacher_selection?>
    <p class="spacer"></p>
      <select name="SelectedTeachingTeacher[]" size="3" id="SelectedTeachingTeacher" multiple="true">
		<?=$html_selected_teaching_teacher?>
      </select> 
    <p class="spacer"></p>
    <input onclick="Remove_Teaching_Teacher();" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<?=$Lang['Btn']['RemoveSelected']?>"/></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['AssessTeacher']?></td>
    <td>:</td>
    <td>
      <?=$html_assess_teacher_selection?>
      <p class="spacer"></p>
      <select name="SelectedAssessTeacher[]" size="3" id="SelectedAssessTeacher" multiple="true">
		<?=$html_selected_assess_teacher?>
      </select> 
      <p class="spacer"></p>
      <input onclick="Remove_Assess_Teacher();" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="<?=$Lang['Btn']['RemoveSelected']?>"/></td>
  </tr>
  <tr>
    <td><?=$Lang['IES']['Student']?></td>
    <td>:</td>
    <td>
  		<table width="100%" border="0" cellspacing="0" cellpadding="5">
  			<tr>
  				<td width="50%" bgcolor="#EEEEEE"><!--<?=$html_student_search_input?>--></td>
					<td width="40">&nbsp;</td>
					<td width="50%" bgcolor="#EFFEE2" class="steptitletext"><?=$Lang['SysMgr']['FormClassMapping']['StudentSelected']?> </td>
				</tr>
				<tr>
					<td bgcolor="#EEEEEE" align="center">
  					<?=$html_year_class_selection?>
  					<?=$html_student_source_selection?>
						<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?> </span>
          </td>
          <td>
            <input id="AddAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddAll']?>"/><br />
            <input id="Add" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="<?=$Lang['Btn']['AddSelected']?>"/><br /><br />
            <input id="Remove" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveSelected']?>"/><br />
            <input id="RemoveAll" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="<?=$Lang['Btn']['RemoveAll']?>"/>
          </td>
          <td bgcolor="#EFFEE2">
				<select name="StudentSelected[]" id="StudentSelected" size="10" multiple="true">
					<?=$html_selected_selectedStudent?>
				</select>
          </td>
        </tr>
      </table>
      <p class="spacer"></p>
    </td>
  </tr>
<!-- <tr>
<td>Student Class No.</td>
<td>:</td>
<td> <input type="radio" name="radio" id="radio" value="radio" />
跟名次序generate 
<input type="radio" name="radio" id="radio" value="radio" />
跟student code 次序generate</td>
</tr> -->
</table>

<div class="edit_bottom"> <!-- <span> 更新日期 :兩天前由管理員</span> -->
  <p class="spacer"></p>
    <input type="submit" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>" onclick="$('#stepSet').val(0)" />
    <input type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="window.location='scheme_index.php'" />
  <p class="spacer"></p>
</div>

<input type="hidden" name="stepSet" id="stepSet" />
<input type="hidden" name="schemeID" id="schemeID" value="<?=$html_schemeID?>"/>
</form>
