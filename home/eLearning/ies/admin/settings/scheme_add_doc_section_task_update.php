<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li = new libdb();
for($i=0, $i_max=count($TaskID); $i<$i_max; $i++)
{
	$_taskID = $TaskID[$i];

	$sql = "INSERT IGNORE INTO IES_STAGE_DOC_SECTION_TASK ";
	$sql .= "(SectionID, TaskID, Sequence, DateInput, InputBy, ModifyBy) ";
	$sql .= "SELECT {$SectionID}, {$_taskID}, IFNULL(MAX(Sequence), 0)+1, NOW(), {$UserID}, {$UserID} ";
	$sql .= "FROM IES_STAGE_DOC_SECTION_TASK ";
	$sql .= "WHERE SectionID = {$SectionID}";
	$li->db_db_query($sql);
}

?>

<script language="JavaScript">
	parent.window.location = "scheme_export_section.php?schemeID=<?=$SchemeID?>&msg=add";
	parent.window.tb_remove();
</script>