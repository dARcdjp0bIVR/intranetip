<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

// validate date
function is_date($date)
{
  //match the format of the date
  if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $date, $parts))
  {
    //check weather the date is valid of not
    if(checkdate($parts[2],$parts[3],$parts[1]))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

$li->Start_Trans();

// Update deadline
for($i=0; $i<count($StageID); $i++)
{
  $t_stage_id = $StageID[$i];
  $t_deadline = is_date(${"Deadline_".$t_stage_id}) ? "'".${"Deadline_".$t_stage_id}."'" : "NULL";

  $sql = "UPDATE {$intranet_db}.IES_STAGE SET Deadline = $t_deadline, ModifyBy = $UserID WHERE StageID = ".$t_stage_id;
  $res[] = $li->db_db_query($sql);
}

// update task approval status
$sql = "UPDATE {$intranet_db}.IES_TASK SET Enable = 0, ModifyBy = $UserID WHERE StageID IN (".implode(", ", $StageID).")";
$res[] = $li->db_db_query($sql);
if(is_array($TaskEnable))
{
  $task_id_arr = array_keys($TaskEnable);
    
  $sql = "UPDATE {$intranet_db}.IES_TASK SET Enable = 1, ModifyBy = $UserID WHERE TaskID IN (".implode(", ", $task_id_arr).")";
  $res[] = $li->db_db_query($sql);
}

// update task approval status
$sql = "UPDATE {$intranet_db}.IES_TASK SET Approval = 0, ModifyBy = $UserID WHERE StageID IN (".implode(", ", $StageID).")";
$res[] = $li->db_db_query($sql);
if(is_array($TaskApproval))
{
  $task_id_arr = array_keys($TaskApproval);
    
  $sql = "UPDATE {$intranet_db}.IES_TASK SET Approval = 1, ModifyBy = $UserID WHERE TaskID IN (".implode(", ", $task_id_arr).")";
  $res[] = $li->db_db_query($sql);
}

// update step status
$sql = "UPDATE {$intranet_db}.IES_STEP step INNER JOIN {$intranet_db}.IES_TASK task ON step.TaskID = task.TaskID SET step.Status = 0, step.ModifyBy = $UserID WHERE task.StageID IN (".implode(", ", $StageID).")";
$res[] = $li->db_db_query($sql);
if(is_array($StepStatus))
{
  $step_id_arr = array_keys($StepStatus);

  $sql = "UPDATE {$intranet_db}.IES_STEP SET Status = 1, ModifyBy = $UserID WHERE StepID IN (".implode(", ", $step_id_arr).")";
  $res[] = $li->db_db_query($sql);
}

$final_res = (count($res) == count(array_filter($res)));
if($final_res)
{
  $li->Commit_Trans();
  $msg = "update";
}
else
{
  $li->RollBack_Trans();
  $msg = "update_failed";
}

header("Location: scheme_step_edit.php?SchemeID=$SchemeID&msg=".$msg);

?>
