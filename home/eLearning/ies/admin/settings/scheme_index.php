<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");




//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}


$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$li = new libuser($UserID);

// Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

// Temporary query retrieving scheme here
$ldb = new libdb();

$sql = "CREATE TEMPORARY TABLE tempScheme (Title varchar(255), StuCount int(8), TeachingTeacher varchar(255), AssessTeacher varchar(255), Steps varchar(255), SchemeID int(8), PRIMARY KEY (SchemeID)) DEFAULT CHARSET=utf8";
$ldb->db_db_query($sql);

$searchText = stripslashes($searchText);
$cond = ($searchText == "") ? "" : " AND scheme.Title LIKE '%".mysql_real_escape_string($searchText)."%'";
$sql = "INSERT INTO tempScheme (Title, StuCount, TeachingTeacher, AssessTeacher, Steps, SchemeID) SELECT scheme.Title, COUNT(ies_ss.SchemeStudentID), '&nbsp;', '&nbsp;', '&nbsp;', scheme.SchemeID FROM {$intranet_db}.IES_SCHEME scheme LEFT JOIN {$intranet_db}.IES_SCHEME_STUDENT ies_ss ON scheme.SchemeID = ies_ss.SchemeID WHERE 1 
and SchemeType = '{$ies_cfg["moduleCode"]}' {$cond} GROUP BY scheme.SchemeID";
$ldb->db_db_query($sql);
$html_searchText = htmlentities($searchText, ENT_QUOTES, "UTF-8");



/*
$sql = "SELECT scheme_teacher.SchemeID, iu.ChineseName, scheme_teacher.TeacherType FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID";
$t_scheme_teacher_arr = $ldb->returnArray($sql);
for($i=0; $i<count($t_scheme_teacher_arr); $i++)
{
  $t_schemeID = $t_scheme_teacher_arr[$i]["SchemeID"];
  $t_chineseName = $t_scheme_teacher_arr[$i]["ChineseName"];
  $t_teacherType = $t_scheme_teacher_arr[$i]["TeacherType"];

  $scheme_teacher_arr[$t_schemeID][$t_teacherType][] = $t_chineseName;
}

if(sizeof($scheme_teacher_arr) > 0){
	foreach($scheme_teacher_arr AS $t_schemeID => $t_scheme_arr)
	{
		if(sizeof($t_scheme_arr) > 0){
		  foreach($t_scheme_arr AS $t_teacherType => $t_teacher_arr)
		  {
			if($t_teacherType == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"])
			{
			  $sql = "UPDATE tempScheme SET TeachingTeacher = '".implode("<br />", $t_teacher_arr)."' WHERE SchemeID = ".$t_schemeID;
			}
			else if($t_teacherType == $ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"])
			{
			  $sql = "UPDATE tempScheme SET AssessTeacher = '".implode("<br />", $t_teacher_arr)."' WHERE SchemeID = ".$t_schemeID;
			}
		  
			if(isset($sql))
			{
			  $ldb->db_db_query($sql);
			  unset($sql);
			}
		  }
		}
	}
}
*/
// Eric Yip (20101015) : Optimized by performing 2 queries instead of looping
if($intranet_session_language=='b5'){
	$TeachersNameExtractSQL = "GROUP_CONCAT(ifnull(iu.ChineseName,iu.EnglishName) SEPARATOR '<br />')";
}else{
	$TeachersNameExtractSQL = "GROUP_CONCAT(ifnull(iu.EnglishName,iu.ChineseName) SEPARATOR '<br />')";
}
$sql = "UPDATE tempScheme ";
$sql .= "INNER JOIN (";
$sql .= "SELECT scheme_teacher.SchemeID, {$TeachersNameExtractSQL} AS TeachingTeacher ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID ";
$sql .= "WHERE scheme_teacher.TeacherType = {$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"]} ";
$sql .= "GROUP BY scheme_teacher.SchemeID) AS t_tt ";
$sql .= "ON tempScheme.SchemeID = t_tt.SchemeID ";
$sql .= "SET tempScheme.TeachingTeacher = t_tt.TeachingTeacher";
$ldb->db_db_query($sql);

$sql = "UPDATE tempScheme ";
$sql .= "INNER JOIN (";
$sql .= "SELECT scheme_teacher.SchemeID, {$TeachersNameExtractSQL} AS AssessTeacher ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID ";
$sql .= "WHERE scheme_teacher.TeacherType = {$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"]} ";
$sql .= "GROUP BY scheme_teacher.SchemeID) AS t_at ";
$sql .= "ON tempScheme.SchemeID = t_at.SchemeID ";
$sql .= "SET tempScheme.AssessTeacher = t_at.AssessTeacher";
$ldb->db_db_query($sql);

$sql = "SELECT Title, StuCount, TeachingTeacher, AssessTeacher, Steps, SchemeID FROM tempScheme order by title";
$scheme_arr = $ldb->returnArray($sql);
if(count($scheme_arr) > 0)
{
  for($i=0; $i<count($scheme_arr); $i++)
  {
    $html_scheme_list .= "<tr>";
    $html_scheme_list .= "<td>".($i+1)."</td>";
    $html_scheme_list .= "<td>".$scheme_arr[$i][0]."</td>";
    $html_scheme_list .= "<td>".$scheme_arr[$i][1]."</td>";
    $html_scheme_list .= "<td>".$scheme_arr[$i][2]."</td>";
    $html_scheme_list .= "<td>".$scheme_arr[$i][3]."</td>";
    $html_scheme_list .= "<td><a href=\"scheme_step_edit.php?SchemeID=".$scheme_arr[$i][5]."\" class=\"edit_dim\" title=\"{$Lang['IES']['Edit']}\">{$Lang['IES']['Edit']}</a></td>";
    $html_scheme_list .= "<td class=\"sub_row\"><div class=\"table_row_tool\"><a href=\"scheme_edit.php?schemeID=".$scheme_arr[$i][5]."\" class=\"edit_dim\" title=\"Edit\"></a><a href=\"scheme_delete.php?SchemeID=".$scheme_arr[$i][5]."\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete']}\"></a></div></td>";
    $html_scheme_list .= "</tr>";
  }
}
else
{
  $html_scheme_list .= "<tr>";
  $html_scheme_list .= "<td align='center' colspan='7'>".$i_no_record_exists_msg."</td>";
  $html_scheme_list .= "</tr>";
}



### It is for the 'New' button ###
$ImportOptionArr = array();
$ImportOptionArr[] = array('javascript:js_Goto_New();', $Lang['IES']['New']);
$ImportOptionArr[] = array('javascript:js_Goto_Copy();', $Lang['IES']['Copy']);
$h_toolBar = $linterface->Get_Content_Tool_v30('new', $href="javascript:void(0);",$Lang['IES']['New'], $ImportOptionArr, $other="", $divID='BuildDiv');


### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
