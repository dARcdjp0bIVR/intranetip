<?php
//MODIFY :
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$li = new libuser($UserID);
$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$SchemeID = (empty($SchemeID)) ?$schemeID : $SchemeID;
$ldb = new libdb();


$sql = "SELECT StageID, Title, Sequence, Deadline FROM {$intranet_db}.IES_STAGE WHERE SchemeID = '".$SchemeID."' ORDER BY Sequence";
$t_stage_arr = $ldb->returnArray($sql);

$html_stage_list = "";
for($i=0; $i<count($t_stage_arr); $i++)
{
  $t_stage_id = $t_stage_arr[$i]["StageID"];
  $t_stage_title = $t_stage_arr[$i]["Title"];
  $t_stage_seq = $t_stage_arr[$i]["Sequence"];
  $t_stage_deadline = $t_stage_arr[$i]["Deadline"];

  $html_stage_list .= "<tr>";
  $html_stage_list .= "<td colspan=\"2\" class=\"sub_row_top stage".$t_stage_seq."\"><strong>".$t_stage_title."</strong></td>";
  $html_stage_list .= "<td class=\"sub_row_top stage".$t_stage_seq."\"><input type=\"checkbox\" name=\"taskEnableMaster\" stage_id=\"".$t_stage_id."\" /></td>";
  $html_stage_list .= "<td class=\"sub_row_top stage".$t_stage_seq."\"><input type=\"checkbox\" name=\"taskApprovalMaster\" stage_id=\"".$t_stage_id."\" /></td>";
  $html_stage_list .= "</tr>";

  $sql = "SELECT TaskID, Title, Enable, Approval FROM {$intranet_db}.IES_TASK WHERE StageID = ".$t_stage_id." ORDER BY Sequence";
  $t_task_arr = $ldb->returnArray($sql);

  for($j=0; $j<count($t_task_arr); $j++)
  {
    $t_task_id = $t_task_arr[$j]["TaskID"];
    $t_task_title = $t_task_arr[$j]["Title"];
    $t_task_enable_checked = $t_task_arr[$j]["Enable"] ? "CHECKED=\"CHECKED\"" : "";
    $t_task_approval_enable = $t_task_enable_checked ? "" : "DISABLED=\"DISABLED\"";
    $t_task_approval_checked = $t_task_arr[$j]["Approval"] ? "CHECKED=\"CHECKED\"" : "";

    $html_stage_list .= "<tr>";
    $html_stage_list .= "<td colspan=\"2\"><strong>".$t_task_title."</strong></td>";
    $html_stage_list .= "<td><input type=\"checkbox\" name=\"TaskEnable[".$t_task_id."]\" stage_id=\"".$t_stage_id."\" task_id=\"".$t_task_id."\" $t_task_enable_checked /></td>";
    $html_stage_list .= "<td><input type=\"checkbox\" name=\"TaskApproval[".$t_task_id."]\" stage_id=\"".$t_stage_id."\" task_id=\"".$t_task_id."\" $t_task_approval_enable $t_task_approval_checked /></td>";
    $html_stage_list .= "</tr>";

    $sql = "SELECT StepID, Title, Status FROM {$intranet_db}.IES_STEP WHERE TaskID = '".$t_task_id."' ORDER BY Sequence";
    $t_step_arr = $ldb->returnArray($sql);

    for($k=0; $k<count($t_step_arr); $k++)
    {
      $t_step_id = $t_step_arr[$k]["StepID"];
      $t_step_title = $t_step_arr[$k]["Title"];
      $t_step_status = $t_step_arr[$k]["Status"];

      $html_stage_list .= "<tr>";
      //$html_stage_list .= "<td>".$ies_stage.".".$ies_step."</td>";
      $html_stage_list .= "<td>&nbsp;</td>";
      $html_stage_list .= "<td>".$t_step_title."</td>";
      //$checked = STUDENT_ACCESS_STEP($ies_scheme_name, $ies_stage, $ies_step) ? "CHECKED=\"CHECKED\"" : "";
      $checked = ($t_step_status == 1) ? "CHECKED=\"CHECKED\"" : "";
      $style_class = ($t_step_status == 1) ? "class=\"rights_selected\"" : "";
      $html_stage_list .= "<td $style_class  style=\"display:none\"><input type=\"checkbox\" name=\"StepStatus[".$t_step_id."]\" value=\"1\" $checked /></td>";
      $html_stage_list .= "<td>&nbsp;</td>";
      $html_stage_list .= "<td>&nbsp;</td>";
      $html_stage_list .= "</tr>";
    }
  }

  $html_stage_list .= "<tr>";
  $html_stage_list .= "<td colspan=\"4\" class=\"sub_row_top stage".$t_stage_seq."\">";

  $html_stage_list .= $t_stage_title.'&nbsp;&nbsp;'.$Lang['IES']['LastSubmissionDate'].": ";
  $html_stage_list .= $linterface->GET_DATE_PICKER("Deadline_".$t_stage_id, $t_stage_deadline);
  $html_stage_list .= "<input type=\"hidden\" name=\"StageID[]\" value=\"".$t_stage_id."\" />";
  $html_stage_list .= "</td>";
  $html_stage_list .= "</tr>";
}


/*
for($i=0; $i<count($t_stage_arr); $i++)
{
  $t_stage_id = $t_stage_arr[$i]["StageID"];
  $t_stage_title = $t_stage_arr[$i]["StageTitle"];
  $t_stage_deadline = $t_stage_arr[$i]["Deadline"];
  $t_task_code = $t_stage_arr[$i]["Code"];
  $t_task_title = $t_stage_arr[$i]["TaskTitle"];
  $t_task_approval = $t_stage_arr[$i]["Approval"];

  $stage_arr[$t_stage_id]["title"] = $t_stage_title;
  $stage_arr[$t_stage_id]["deadline"] = $t_stage_deadline;
  $stage_arr[$t_stage_id]["task"][$t_task_code] =  array(
                                                      "title" => $t_task_title,
                                                      "approval" => $t_task_approval
                                                    );
}

$sql = "SELECT stage.StageID, step.StepNo, step.Title, step.Status FROM {$intranet_db}.IES_STAGE stage LEFT JOIN {$intranet_db}.IES_TASK task ON stage.StageID = task.StageID LEFT JOIN {$intranet_db}.IES_STEP step ON task.TaskID = step.TaskID WHERE stage.SchemeID = ".$SchemeID." ORDER BY stage.Sequence, task.Sequence, step.StepNo";
$t_step_arr = $ldb->returnArray($sql);

for($i=0; $i<count($t_step_arr); $i++)
{
  $t_stage_id = $t_step_arr[$i]["StageID"];
  $t_step_no = $t_step_arr[$i]["StepNo"];
  $t_step_title = $t_step_arr[$i]["Title"];
  $t_step_status = $t_step_arr[$i]["Status"];

  $stage_step_arr[$t_stage_id][$t_step_no]["title"] = $t_step_title;
  $stage_step_arr[$t_stage_id][$t_step_no]["status"] = $t_step_status;
}


$sql = "SELECT task.StageID, task.Code, task.FirstStepNo FROM {$intranet_db}.IES_TASK task INNER JOIN {$intranet_db}.IES_STAGE stage ON stage.StageID = task.StageID WHERE stage.SchemeID = ".$SchemeID." ORDER BY stage.Sequence";
$t_task_arr = $ldb->returnArray($sql);

for($i=0; $i<count($t_task_arr); $i++)
{
  $t_stage_id = $t_task_arr[$i]["StageID"];
  $t_task_code = $t_task_arr[$i]["Code"];
  $t_first_step_no = $t_task_arr[$i]["FirstStepNo"];

  $stage_module_step_arr[$t_stage_id][$t_task_code] = $t_first_step_no;
}

$sql = "SELECT task.StageID, prev_step.StepNo AS PrevStepNo, IFNULL(next_step.StepNo, 999) AS NextStepNo FROM {$intranet_db}.IES_TASK task INNER JOIN {$intranet_db}.IES_STAGE stage ON stage.StageID = task.StageID LEFT JOIN {$intranet_db}.IES_STEP prev_step ON prev_step.TaskID = task.TaskID LEFT JOIN {$intranet_db}.IES_STEP next_step ON prev_step.NextStepNo = next_step.StepNo WHERE stage.SchemeID = ".$SchemeID." ORDER BY stage.Sequence, prev_step.StepNo";
$t_next_step_arr = $ldb->returnArray($sql);
for($i=0; $i<count($t_next_step_arr); $i++)
{
  $t_prev_step_no = (int) $t_next_step_arr[$i]["PrevStepNo"];
  $t_prev_stage_id = (int) $t_next_step_arr[$i]["StageID"];
  $t_next_step_no = (int) $t_next_step_arr[$i]["NextStepNo"];
  $t_next_stage_id = ($t_next_step_no == TERMINAL_NO) ? TERMINAL_NO : (int) $t_next_step_arr[$i]["StageID"];

  $next_step_arr[$t_prev_stage_id][$t_prev_step_no] = array($t_next_stage_id, $t_next_step_no);
}

$i = 1;
$html_stage_list = "";
foreach($stage_arr AS $stage_id => $stage_detail)
{
  $html_stage_list .= "<tr>";
  $html_stage_list .= "<td colspan=\"4\" class=\"sub_row_top stage".$i."\"><strong>".$stage_arr[$stage_id]["title"]."</strong></td>";
  $html_stage_list .= "</tr>";

  foreach($stage_detail['task'] AS $task_code => $task_detail)
  {
    $step_no = $stage_module_step_arr[$stage_id][$task_code];

    $html_stage_list .= "<tr>";
    $html_stage_list .= "<td colspan=\"3\"><strong>".$task_detail["title"]."</strong></td>";

    //$checked = APPROVAL_STAGE($ies_scheme_name, $ies_stage, $module_code) ? "CHECKED=\"CHECKED\"" : "";
    $checked = $task_detail["approval"] ? "CHECKED=\"CHECKED\"" : "";
    $html_stage_list .= "<td><input type=\"checkbox\" name=\"Approval[".$stage_id."][".$task_code."]\" $checked /></td>";
    $html_stage_list .= "</tr>";

    while($step_no != TERMINAL_NO)
    {
      $html_stage_list .= "<tr>";
      //$html_stage_list .= "<td>".$ies_stage.".".$ies_step."</td>";
      $html_stage_list .= "<td>&nbsp;</td>";
      $html_stage_list .= "<td>".$stage_step_arr[$stage_id][$step_no]["title"]."</td>";

      //$checked = STUDENT_ACCESS_STEP($ies_scheme_name, $ies_stage, $ies_step) ? "CHECKED=\"CHECKED\"" : "";
      $checked = ($stage_step_arr[$stage_id][$step_no]["status"] == 1) ? "CHECKED=\"CHECKED\"" : "";
      $style_class = ($stage_step_arr[$stage_id][$step_no]["status"] == 1) ? "class=\"rights_selected\"" : "";
      $html_stage_list .= "<td $style_class><input type=\"checkbox\" name=\"Status[".$stage_id."][".$step_no."]\" value=\"1\" $checked /></td>";
      $html_stage_list .= "<td>&nbsp;</td>";
      $html_stage_list .= "</tr>";

/*
      $prev_stage = $ies_stage;
      $prev_step = $ies_step;
      $prev_module_code = $module_code;
*/
//      list($dummy, $step_no) = $next_step_arr[$stage_id][$step_no];
      //list($ies_stage, $ies_step) = NEXT_STEP(IDENTITY_TEACHER, $prev_stage, $prev_step);
//    }

//  }

/*
  $html_stage_list .= "<tr>";
  $html_stage_list .= "<td colspan=\"4\" class=\"sub_row_top stage".$i."\">";
  $html_stage_list .= $stage_detail["title"].$Lang['IES']['LastSubmissionDate'].": ";
  $html_stage_list .= $linterface->GET_DATE_PICKER("Deadline_".$stage_id, $stage_detail["deadline"]);
  $html_stage_list .= "<input type=\"hidden\" name=\"StageID[]\" value=\"".$stage_id."\" />";
  $html_stage_list .= "</td>";
  $html_stage_list .= "</tr>";

  $i++;
}
*/
// Result message after operation
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

// navigation
$schemeDetails = $objIES->GET_SCHEME_DETAIL($SchemeID);
$nav_arr[] = array($Lang['IES']['Scheme'], "scheme_index.php");
$nav_arr[] = array($schemeDetails["Title"], "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);

//FUNCTION TAB HANDLING
$currentTagFunction = "editFlow";
$tabArray = $ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"];

$tabAppendParameter = "?schemeID={$SchemeID}";
$newTab = $objIES->appendParameterToTab($tabAppendParameter,$tabArray);
$html_functionTab = $objIES->GET_TAB_MENU($currentTagFunction,$newTab);

### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_step_edit.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
