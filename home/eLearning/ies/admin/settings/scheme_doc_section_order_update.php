<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li = new libdb();

switch($type)
{
	case $ies_cfg['DocSectionType']['section']:
		// Reset all section order of the stage
		$sql = "UPDATE $intranet_db}.IES_STAGE_DOC_SECTION ";
		$sql .= "SET Sequence = NULL ";
		$sql .= "WHERE StageID = {$StageID}";
		$li->db_db_query($sql);
		
		// filter out empty entries (by first row and last row)
		${$ies_cfg['DocSectionType']['section']} = eval("return array_values(array_filter(\${$ies_cfg['DocSectionType']['section']}));");
		
		// loop for update
		for($i=0, $i_max=count(${$ies_cfg['DocSectionType']['section']}); $i<$i_max; $i++)
		{
			$_sectionID = ${$ies_cfg['DocSectionType']['section']}[$i];
		
			$sql = "UPDATE {$intranet_db}.IES_STAGE_DOC_SECTION ";
			$sql .= "SET Sequence = ".($i+1)." ";
			$sql .= "WHERE SectionID = {$_sectionID}";
			$li->db_db_query($sql);
		}
		
	break;
	case $ies_cfg['DocSectionType']['task']:
		// Reset all task section order of the section
		$sql = "UPDATE $intranet_db}.IES_STAGE_DOC_SECTION_TASK ";
		$sql .= "SET Sequence = NULL ";
		$sql .= "WHERE SectionID = {$SectionID}";
		$li->db_db_query($sql);
		
		// filter out empty entries (by first row and last row)
		${$ies_cfg['DocSectionType']['task']} = eval("return array_values(array_filter(\${$ies_cfg['DocSectionType']['task']}));");
		
		// loop for update
		for($i=0, $i_max=count(${$ies_cfg['DocSectionType']['task']}); $i<$i_max; $i++)
		{
			$_taskID = ${$ies_cfg['DocSectionType']['task']}[$i];
		
			$sql = "UPDATE {$intranet_db}.IES_STAGE_DOC_SECTION_TASK ";
			$sql .= "SET Sequence = ".($i+1)." ";
			$sql .= "WHERE TaskID = {$_taskID}";
			$li->db_db_query($sql);
		}
	break;
}

?>