<?php
//$PATH_WRT_ROOT = "../../../../../";
//include_once($PATH_WRT_ROOT."includes/global.php");
//include_once($PATH_WRT_ROOT."includes/libdb.php");
$temp = <<<EOT
Stage 1
##擬題##Formulation of Title
:-B5
--題目太空泛。(-)
--題目探究野心過大。(-)
--題目探究範圍過闊。(-)
--題目內概念定義不清。(-)
--題目沒有可爭議的地方/純屬描述性的資料整合。(-)
--題目流於個人興趣探究，不符合社會研究原意。(-)
--題目需要的資料或研究對象難以尋找。(-)
--研究題目切入點非常新穎，有創意。(+)
--研究題目能結合通識科相關概念。(+)
--研究題目深淺適中，是個好開始。(+)
--研究題目的原創性頗高，見心思。(+)
--研究題目基本上是可行的。(+)
--研究題目可容許跨單元、多角度探究，符合本科精神。(+)

:-EN
--The title is too vague.(-)
--The study is too ambitious.(-)
--The scope of the study is too wide.(-)
--Definitions of the title concepts are not clear enough.(-)
--The title is undebatable. / The title consists solely of descriptive integrated information.(-)
--The title is based more on personal interests without fulfilling the purpose of community related studies.(-)
--The data or the research objects required for the study are inaccessible.(-)
--The title has a very innovative and creative entry point.(+)
--The title can integrate related concepts covered in Liberal Studies.(+)
--The difficulty level of the study is optimal. A good start.(+)
--The title of the study is innovative and thoughtful.(+)
--The study is basically feasible.(+)
--The study can allow cross-module and multi-perspective enquiries to match the rationale of the subject.(+)

##擬題建議##Recommendations for formulating a title
:-B5
--應考慮搜集第一手資料（例如：問卷訪問）時的困難。
--應考慮是否有足夠能力去完成研究。
--應考慮是否能在短時間內完成此研究。
--應考慮研究的目的。
--應到圖書館或上網查找更多有關研究題目的資料，藉此更深入了解題目。
:-EN
--(You should consider) the difficulties in collecting first-hand information (e.g. questionnaires).
--(You should consider) your ability and the manageability of the study.
--(You should consider) whether the study can be completed in a short time.
--(You should consider) the objectives of the study.
--(You should) search for more information about the study in the library or on the Internet for a more thorough understanding.
##題目範圍和定義##Scope and definition of title
:-B5
--題目中的關鍵字定義不清晰，請加以定義。(-)
--題目所涉及的地域範圍太廣，搜集資料時可能會遇到困難。(-)
--題目原創性略為不足，話題欠缺新意。(-)
--題目稍為局限於個人興趣層面，難成社會研究題目。
--題目探究範圍適中，重點明確。(+)
--題目的關鍵字定義清晰，在研究初期已有深入考慮各變項之間的關係。(+)
--題目富有一定的原創性。(+)
--題目富有一定的社會意義，值得研究。(+)
:-EN
--The key words of the title should be well-defined as they are not clear enough.(-)
--The geographical area covered in the study is too large for data collection at a manageable level.(-)
--The title is not originative enough.(-)
--The scope of the title is optimal with objectives clearly stated.(+)
--The key words of the title are well-defined. The relations of all variables have been taken into account at the initial stage of the study.(+)
--The title is originative.(+)
--The title is worthy of study for its inherent social implications.(+)
--The study is rather confined to personal interests, hard to convert into a social enquiry topic.(-)

##探究切入點##Perspectives/Entry points of the study
:-B5
--請留意尋找探究對象時所出現的難度。(-)
--題目中的字眼含有價值觀判斷，先入為主，不夠中肯。(-)
--題目難以實現多角度分析議題的原則。(-)
--題目未能引發跨單元和多角度探究。(-)
--題目難度甚高，比較難說服已有固定想法的讀者。(-)
--用某個概念，如「生活素質」作為探究切入點比較空泛，需要具體說明。(-)
--題目本身具一定難度，然而所選取的切入點比較容易處理。(+)
--題目能引發多角度分析，試考慮會否加入其他持分者，使觀點更多元化。(+)
--題目能引發跨單元和多角度探究，要把握機會，加以發揮。(+)
:-EN
--Consider the accessibility of the research objects.(-)
--The wording of the title implies pre-occupied value judgement, not objective enough.(-)
--The title does not enhance/support the principle of a multi-perspective analysis.(-)
--The title does not initiate/facilitate a cross-module and multi-perspective enquiry.(-)
--The study is of a very high difficulty level, unlikely to convince readers with pre-occupations .(-)
--It is rather vague to use a concept such as 'quality of life' as an entry point for a study. More concrete specifications needed.(-)
--The study is rather hard but the approach/entry point of the enquiry is quite manageable.(+)
--The title can initiate a multi-perspective analysis.You may consider including other stakeholders for more diversified viewpoints.(+)
--The title can initiate a cross-module and multi-perspective study.The study can be well elaborated/expanded.(+)

##相關概念的應用##Application of related concepts
:-B5
--題目未能引申到通識科相關概念探究。(-)
--題目未有包含通識科的相關概念。(-)
--題目未有符合通識科跨課題探究精神。(-)
--題目偏重於專門學科知識，難以與通識科概念連結互通。(-)
--未有清楚定義相關概念/重點字眼。(-)
--未能應用相關概念作探究的切入點，引致分析的焦點欠清晰。(-)
--未能應用相關概念。(-)
--應用錯誤的相關概念。(-)
--有其他更好的相關概念可選擇，但並未使用。(-)
--焦點不清，模棱兩可。(-)
--題目結合通識科相關概念作探究，見其應用能力。(+)
--題目符合通識科跨課題探究精神，能作多角度分析。(+)
--能清楚定義相關概念/重點字詞。(+)
--能應用相關概念作探究的切入點。(+)
:-EN
--The title cannot be extended to the study of related concepts in Liberal Studies.(-)
--The title does not include the related concepts in Liberal Studies.(-)
--The study does not meet the cross-topic requirement of the Liberal Studies subject.(-)
--The title inclines towards technical knowledge of a certain subject, hard to correlate with the concepts of Liberal Studies.(-)
--The related concepts/key words are not well-defined.(-)
--Related concepts have not been applied to begin the enquiry, which leads to an unclear focus for analysis.(-)
--Related concepts have not been applied.(-)
--Incorrect related concepts have been applied.(-)
--There are other better related concepts available but not applied.(-)
--The unclear focus makes the study ambiguous.(-)
--The study integrates the related concepts in Liberal Studies, and is clearly applicable.(+)
--The title serves the objective of cross-topic enquiry in Liberal Studies and enables multi-perspective analysis.(+)
--The related concepts/key words are well-defined.(+)
--Related concepts are applied to begin the study.(+)

##研究動機/目的##Research motivation/objective
:-B5
--能清楚明確交代研究目的/動機。(+)
--研究具有社會意義，符合通識科課程精神，有研究價值。(+)
--研究目的清晰。(+)
--能把個人的研究興趣推及群眾和社會。(+)
--研究只局限於個人興趣。(-)
--擬題的野心太大，應考慮自己的能力範圍是否足以處理題目。(-)
--研究目的與題目不相關。(-)
--研究目的不夠清晰具體。(-)
:-EN
--The research objective /motivation is clearly stated.(+)
--The study is worthy for its inherent social implications and attachment to the Liberal Studies curriculum.(+)
--The research objective is clear.(+)
--The study enables personal interests to be extended to the public and society.(+)
--The study is solely confined to personal interests.(-)
--The study is too ambitious. Should consider own ability in handling the enquiry.(-)
--The research objective and the title are unrelated..(-)
--The research objective is not specific and concrete enough.(-)

##焦點問題##Focus questions
:-B5
--焦點問題具體清晰。(+)
--焦點問題能有效引導往後的資料蒐集。(+)
--焦點問題能回應研究目的及題目。(+)
--焦點問題與研究目的和題目沒有關連。(-)
--所提出的焦點問題無法有效引導蒐集資料。(-)
--部分焦點問題偏離了研究重點。(-)
--所提出的焦點問題無法得出有用的資料。(-)
--所提出的焦點問題未能協助達到研究目的。(-)
:-EN
--The focus questions are concrete and specific.(+)
--The focus questions can effect subsequent data collection.(+)
--The focus questions can address the research objective and the title.(+)
--The focus questions and the research objective and title are unrelated.(-)
--The focus questions cannot lead to effective data collection.(-)
--Some focus questions deviate from the scope of the study.(-)
--The focus questions cannot help to locate useful information.(-)
--The focus questions cannot help to serve the research objective.(-)

##文獻探討##Literature review##
:-B5
--文獻資料全面、充足、具體，可見其心思慎密，投入度高。(+)
--文獻資料來源廣，分析深入，能有效貼近研究主題。(+)
--引述適當的文獻資料。(+)
--準確地使用有關文獻資料。(+)
--沒有包含不相關的資料。(+)
--文獻探討包含與主題不相關的資料，沙石甚多。(-)
--沒有蒐集相關文獻和背景資料，對研究話題所知甚為表面。(-)
--文獻資料與研究題目關係不大。(-)
--文獻資料來源不夠多元化，建議參考更多類型的資料。(-)
--參考資料大部分來自（網上討論區、維基百科、教科書等），權威性不高。(-)
:-EN
--The literature is comprehensive,sufficient and specific,reflecting a high degree of meticulousness and participation.(+)
--The literature comes from a variety of sources with profound analysis and is effectively related to the research topic.(+)
--Appropriate literature is cited.(+)
--Relevant literature is accurately used.(+)
--No irrelevant information is included.(+)
--The literature review includes information not related to the study.(-)
--Relevant literature and background information has not been collected for reference. Understanding of the study is shallow.(-)
--There is no close relationship between the literature and the study.(-)
--Sources of the literature are not diversified enough. Different types of references are recommended.(-)
--Most of the references come from the Internet,Wikipedia and textbooks which are not quite authoritative.(-)

##工作計劃##Work plan
:-B5
--能清晰具體交代研究計劃。(+)
--能全面掌握受訪者的特性，充分理解在收集數據時所遇到的困難。(+)
--能辨別掌握資料來源的關鍵人物，能提供可靠而重要的資料。(+)
--能選用正確的收集數據方法。(+)
--整體計劃完善，基本上可行。(+)
--能坦誠交代研究的困難與限制，充分表現危機意識。(+)
--有能力預計各種可能出現的困難。(+)
--能考慮研究所需的資源、時間、人力，作合理安排。(+)
--所計劃的工作大致符合現實。(+)
--探究計劃未能切合研究目的。(-)
--未有深入考慮各種可能出現的困難。(-)
--訂立工作計劃未夠具體/流於空泛。(-)
--所計劃的工作難以符合現實。(-)
--工作項目有重要遺漏。(-)
--沒有考慮一些可能遇到的困難。(-)
:-EN
--The research plan is clearly stated.(+)
--The traits of the interviewees are well monitored and the difficulties of data collection fully perceived.(+)
--Key persons who are the source of information are identified so that reliable and important information is collected.(+)
--Appropriate methods of data collection are used.(+)
--The research plan is well worked out and is basically feasible.(+)
--Difficulties and limitations of the study are frankly stated, reflecting a good sense of crisis.(+)
--The ability to foresee all possible difficulties is demonstrated.(+)
--All resources needed for the study in terms of materials, time and manpower are taken into account and reasonably deployed.(+)
--The research plan is largely practical.(+)
--The study plan cannot meet the research objectives.(-)
--Various possible difficulties of the study have not been thoroughly considered.(-)
--The work plan is not specific enough.(-)
--The work plan is impractical.(-)
--Some important processes are missed out in the study.(-)
--Some foreseeable difficulties of the study have not been considered.(-)

##反思##Reflection
:-B5
--有良好深刻的反思，可見在過程中所學甚多，態度值得鼓勵。(+)
--反思內容具體，沒有包含不相關的資料。(+)
--反思內容流於空泛表面。(-)
--沒有顯示在探究過程中有深刻反思。(-)
--未能掌握階段一和課業的學習目標。(-)
--在探究過程中，有條理及持續地作自我評估及反思。(+)
--在探究過程中，只作一或兩次的自我評估/反思。
--在探究過程中，沒有計劃作自我評估和反思。(-)
--反思內容豐富，可見過程中對探究方法獲益不少。(+)
--反思內容與研習技巧不太相關，或流於表面，未見深刻。
--反思內容空泛，未達自我學習、自我反省的評核目標。(-)
--能詳細分析所學知識、技巧、成功或失敗的關鍵，以啟發將來學習。(+)
--稍有提及所學知識、技巧、成功或失敗的關鍵，但不夠深刻具體。
--未有意識總結過程所學所長、成功或失敗的原因。(-)
:-EN
--There is deep reflection indicating a lot of things are learned during the study. This attitude is commendable.(+)
--The reflection is specific without irrelevant information.(+)
--The reflection is too superficial.(-)
--There is no indication of deep reflection in the course of the study.(-)
--The learning objectives of the task in Stage 1 have not been grasped.(-)
--Continuous self-evaluation and reflection is demonstrated in the course of study. (+)
--Self-evaluation and reflection is demonstrated only once or twice in the course of study.
--No self-evaluation and reflection is planned in the course of study. (-)
--Profound reflection is demonstrated indicating much has been learnt in the course of study. (+)
--The reflection content is not quite relevant to the research skills, or it is superficial and not profound enough.
--The reflection contains vague information and does not meet the assessment objectives of self-learning and self-reflection. (-)
--Comprehensive analysis is done on the knowledge and skills learnt, and factors governing success or failure to inspire future learning. (+)
--The brief mention on knowledge and skills learnt, and factors governing success or failure is not profound and concrete enough.
--No intention is shown to summarise what is learnt in the course of study and the reasons behind success or failure. (-)

##獨立思考##Independent thinking
:-B5
--能使用與議題相關的資料，並考慮其準確性
--能聯系相關概念和知識到所屬議題
--能提出有邏輯、有理據的論點
--能提出不同構思和觀點
--能審視和/或比較多個角度觀點
--能反思個人學習體驗
:-EN
--Issue-related information is used and its accuracy considered.
--Related concepts and knowledge are correlated with the issue.
--Logical and justifiable arguments are brought out.
--Different conceptions and points of view are brought out.
--Different points of view are examined and compared.
--Personal learning experience is reflected on./Reflection on personal learning experience is done.

##溝通##Communication
:-B5
--能與其他人交換意見和資訊
--溝通清晰、連貫、流暢和有組織
--使用有效的溝通媒介和方法去傳達構思和資訊
:-EN
--Opinion and information is exchanged with others.
--Communication is clear,connected, fluent and organised.
--Effective means of communication are employed to pass on conceptions and information.
##努力與付出##Effort
:-B5
--良好的時間與資源的管理
--願意發問和尋求協助
--願意尋找其他解決方法和可能性
--表現出願意解決問題和持續改善的積極態度
:-EN
--Good management of time and resources.
--Willing to ask questions and seek assistance
--Willing to look for other solutions and alternatives.
--Displays positive attitudes towards problem solving and self-improvement.

Stage 2
##研究方法##Research method
:-B5
--探究方法描述具體，大致可行。 (+)
--探究方法未算具體，然而尚算可行。 
--探究方法含糊，欠具體，可行性不大。(-)
--探究方法完全切合題目所需。(+)
--探究方法頗為切合題目所需。
--探究方法未能切合題目所需。(-)
--能提供有說服力的原因支持所選的方法。(+)
--只能提供少量原因支持所選的方法，解釋含糊，說服力一般。
--沒有提供原因支持所選的方法。(-)
--能深入分析搜集資料前的準備事項、所需工具及危機處理的方法。(+)
--能粗略描述搜集資料前的準備功夫，惜未夠深入。
--未有慎重地選擇探究方法。(-)
:-EN
--The research method is specific and workable in general. (+)
--The research method is not quite specific, but still workable.
--The research method is too vague, not specific enough and most likely unworkable. (-)
--The research method meets the requirements of the enquiry completely. (+)
--The research method meets the requirements of the enquiry in general.
--The research method does not meet the requirements of the enquiry. (-)
--Convincing arguments are provided to support the choice of research method. (+)
--Only few arguments are provided to support the choice of research method. The explanation is vague and barely persuasive.
--No arguments are provided to support the choice of research method. (-)
--In-depth analysis of preparations for data collection, selection of tools and crisis management is demonstrated. (+)
--Preparations for data collection are roughly stated but without going into detail.
--The research method has not been selected with caution. (-)
##運用的工具##Research tool
:-B5
--所選工具能收集與題目及焦點問題有關的資料。(+)
--所選工具未能收集題目及焦點問題所需的關鍵資料。
--所選工具就是次研究而言並非最適切。(-)
--收集數據前能充分測試研究工具（例如：問卷或訪問題目），並收集意見加以改進。(+)
--收集數據前有測試研究工具（例如：問卷或訪問題目），但未能有效改善工具。
--收集數據前沒有測試研究工具（例如：問卷或訪問題目），缺乏憂患意識。 (-)
--能充分考慮老師與同儕的意見，輔以個人思考與判斷，在收集數據前改進研究工具。(+)
--能參考老師與同儕的部分意見，在收集數據前改進研究工具。
--未能因應老師與同儕的意見，在收集數據前改進研究工具。(-)
:-EN
--The tool selected enables collection of data relevant to the title and focus questions. (+) 
--The tool selected does not enable collection of data relevant to the title and focus questions.
--The tool selected is not the most appropriate for this enquiry. (-)
--The research tool (e.g. questionnaire or interview questions) is thoroughly tested and modified before data collection. (+)
--The research tool (e.g. questionnaire or interview questions) is tested, but not modified accordingly before data collection.
--The research tool (e.g. questionnaire or interview questions) is not tested before data collection, showing the lack of a sense of crisis. (-)
--The research tool is modified before data collection with full reference to teacher and peer opinions as well as own thinking and judgment. (+)
--The research tool is modified before data collection with partial reference to teacher and peer opinions. 
--The research tool is not modified before data collection using reference from teacher and peer opinions. 
##處理數據##Data management
:-B5
--能把數據/資料整合和歸類成能回應焦點問題的論據或論證。(+)
--能把數據/資料整合和歸類，但未能充分回應焦點問題或研究重點。
--所呈交的資料雜亂，大部分為原始數據，未經妥善歸類和整合。(-)
:-EN
--Data and information is integrated and categorised to form the premise and evidence of the focus question.(+)
--Data and information is integrated and categorised, but does not address the focus question or the focus of the research.
--The information submitted is disorganised, most of it being raw data without proper categorisation and integration.(-)
##研究所得##Research outcome
:-B5
--能有系統、有組織，並準確地表達研究所得。(+)
--能大致交代研究所得，然而表達方式、準確性和組織方面有待改善。
--未能清楚交代研究所得，結果含糊。(-)
--展示結果時能使用最有效的方法（例如：適當使用圖表），令讀者容易明白要表達的訊息。(+)
--展示結果時未能使用最有效的方法，可考慮在適當時候使用圖表。
--只是羅列研究數據，未有進一步處理或歸納。資料組織雜亂、重點不清、與研究相關性不高。(-)
:-EN
--The research outcome is presented systematically and precisely.(+)
--The research outcome is fairly presented, but improvement needed in the presentation mode, accuracy and organisation.
--The research outcome is not clearly stated. The result is unsettled.(-)
--The most effective means (e.g. proper use of graphs and charts) is adopted to present the outcome to make the message easily understood.(+)
--The means of result presentation is not the most effective. Graphs and charts can be used in certain situations.
--Raw data is simply listed without further management or induction. Information is disorganised, without a clear focus and not totally relevant to the study.(-)
##研究困難/限制##Limitations / Difficulties of research
:-B5
--能坦誠分析過程中可能遇到的困難與限制，並提出合適的解決方法，充分展示其解難能力。(+)
--未有充分預期過程中可能出現的困難，見其危機意識不足。只粗略地指出一些與探究工作不太有關的問題。
--未有提及探究可能出現的困難，見其危機意識不足。(-)
:-EN
--The possible difficulties and limitations in the course of study are frankly stated and suitable solutions are offered. Good problem solving skills are demonstrated.(+)
--Not all possible difficulties in the course of study are foreseen, indicating a lack of sense of crisis. Only problems not quite relevant to the study are roughly brought out.
--Possible difficulties in the course of study are not stated. A lack of sense of crisis is evident.(-)


EOT;



?>