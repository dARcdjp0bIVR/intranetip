<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
//GET USER VARIABLE 
$schemeID = intval(trim($schemeID));

if(empty($schemeID) || !is_int($schemeID))
{
	echo "Invalide access , Empty SCHEME ID<br/>";
    exit();
}

$html_schemeID = $schemeID;  // FOR DISPLAY HIDDEN FORM FIELD VALUE

intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-SETTING-SCHEME-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "SchemeSettings";

$objDB = new libdb();
$li = new libuser($UserID);

$objDB_UI = new libies_ui();


$schemeDetails = $objIES->GET_SCHEME_DETAIL($schemeID);

###############################################################
##	Start: Scheme Lang Handling
###############################################################
$h_RadioLang = $Lang['IES']['SchemeLang'][$schemeDetails["Language"]];
###############################################################
##	End: Scheme Lang Handling
###############################################################
$h_schemeTitle = $schemeDetails["SchemeType"];
//if the result set (schemeDetails) not equal to one and or is not a array, should have error. Exit the program
/*if(sizeof($schemeDetails) != 1 || !is_array($schemeDetails))
{
	echo "SYSTEM ERROR, SCHEME ID NOT FIND<br/>";
    exit();
}*/
$schemeTitle =$schemeDetails["Title"];
$html_schemeTitle = $schemeTitle;


// Connie TBD

//$html_fromSchemeSel = '';
//$html_fromSchemeSel .= '<select id="fromSchemeIDSel" name="fromSchemeID" onchange="js_Changed_FromScheme_Selection(this.value);">';
//	$html_fromSchemeSel .= '<option id="">- Select Scheme -</option>';
//	$html_fromSchemeSel .= '<option id="1">Scheme 1</option>';
//	$html_fromSchemeSel .= '<option id="2">Scheme 2</option>';
//$html_fromSchemeSel .= '</select>';

$html_fromSchemeSel = $objIES->getSchemeSelection('',$schemeID);


//FUNCTION TAB HANDLING
// Connie TBD
$currentTagFunction = "schemeMoveStudent";
$tabArray = $ies_cfg["MODULE_TAB"]["ADMIN_SCHEME_SETTING"];

$tabAppendParameter = "?schemeID={$schemeID}";
$newTab = $objIES->appendParameterToTab($tabAppendParameter,$tabArray);
$html_functionTab = $objIES->GET_TAB_MENU($currentTagFunction,$newTab);	

// navigation
$nav_arr[] = array($Lang['IES']['Scheme'], "scheme_index.php");
$nav_arr[] = array($html_schemeTitle, "");




$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";


### Title ###
$title = $Lang['IES']['Scheme'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_move_student.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
