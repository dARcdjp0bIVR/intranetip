<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}

if(sizeof($_POST)==0 && (!isset($sheetID) || $sheetID==""))
	header("Location: index.php");
	

$objIES = new libies();	
$lfs = new libfilesystem();	
	
$objIES->removeWorksheet($sheetID);

intranet_closedb();

header("Location: worksheet.php?xmsg=delete&scheme_id=$schemeID");
?>