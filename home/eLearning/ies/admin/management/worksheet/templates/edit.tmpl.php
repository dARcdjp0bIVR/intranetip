<script language='javascript'>

function checkForm() {
	
	if(document.getElementById('title').value=='') {
		alert("<?=$i_alert_pleasefillin.' '.$Lang['IES']['Title']?>");	
		document.getElementById('title').focus();
		return false;
	}
	if(document.getElementById('schemeID').value=='') {
		alert("<?=$i_alert_pleaseselect.' '.$Lang['IES']['Scheme']?>");	
		document.getElementById('schemeID').focus();
		return false;
	}

	if (!check_date(document.getElementById('startDate'),'<?=$i_invalid_date?>')) return false;
	if (!check_date(document.getElementById('endDate'),'<?=$i_invalid_date?>')) return false;
	if(compareDate(document.getElementById('endDate').value, document.getElementById('startDate').value)<0) {
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		document.getElementById('startDate').focus();
		return false;
	}
}

function goCancel() {


window.location='/home/eLearning/ies/admin/management/worksheet/worksheet.php?scheme_id=<?=$scheme_id?>';
}

</script>

<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<form name='form1' method='POST' action='<?=$html_form_action?>' onSubmit='return checkForm()'>
<div class="table_board">
  <table class="form_table" align="center" border="0">
      <?=$html_worksheet_list?>
  </table>
  
  <div class="edit_bottom">
	  <p class="spacer"></p>
<input type='hidden' name='schemeID' id='schemeID' value='<?=$scheme_id?>'>
<input type='hidden' name='sheetID' id='sheetID' value='<?=$sheetID?>'>

	  	<div align="center"><?=$html_button?></div>
	  <p class="spacer"></p>


  </div>
</div>
</form>
