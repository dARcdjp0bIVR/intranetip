<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();
$linterface = new interface_html();

$comment = ${'comment_'.$studentID};

$comment = intranet_htmlspecialchars($comment);

$result = $objIES->updateWorksheetHandinComment($sheetID, $studentID, $comment);

if(is_numeric($result)){
	$sql = 'select '.getNameFieldByLang2("i.").' as teacherName, DATE_format(m.DateModified , \'%Y-%m-%d\') as DateModified from '.$intranet_db.'.IES_WORKSHEET_HANDIN_COMMENT as m left join '.$intranet_db.'.INTRANET_USER as i on m.ModifyBy = i.UserID  where m.CommentID = '.$result;
	$result = $objIES->returnArray($sql);
//error_log($sql."\n", 3, "/tmp/aaa.txt");

	if(is_array($result) && count($result) == 1){
		$result = current($result);
		$resultStr = "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$result['DateModified']} {$Lang['IES']['From']} {$result['teacherName']})</span></div>";		
	}else{
		$result = "Update Failed";
	}
}


intranet_closedb();

echo $resultStr;

?>