<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"]) {
	header("Location: /");
	exit;
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "Worksheet";

$objIES = new libies();
//$ldb = new libdb();

# Scheme Menu
//$schemeMenu = $objIES->getSchemeSelection();
$schemeData = $objIES->GET_SCHEME_DETAIL($scheme_id);

//remove any "Orphan" with worksheet id = 0 in IES_WORKSHEET_TEACHER_FILE that is created by the user
$objIES->removeTemporaryWorkSheet($UserID);

/*
$attSelect = $linterface->GET_SELECTION_BOX($objIES->getAttachmentArray($Attachment),"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");
$attachmentHTML = "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$attSelect."
				</td>
				<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."', 9)")."
				<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment()")."
				</td>
				</tr>
				</table>";
*/
# table content
$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['WorksheetTitle']."</td><td width='1'> : </td><td width='80%'><input type='text' name='title' id='title' size='50'></td></tr>";

//$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['Scheme']."</td><td width='1'> : </td><td width='80%'>$schemeMenu</td></tr>";

$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['StartEndDate']."</td><td width='1'> : </td><td width='80%'>{$i_From} ".$linterface->GET_DATE_PICKER("startDate",$startDate)." {$i_To} ".$linterface->GET_DATE_PICKER("endDate",$endDate)."</td></tr>";


$html_worksheet_list .= "<tr><td valign='top' width='20%'>".$Lang['IES']['UploadFile']."</td><td width='1'> : </td><td width='80%'>".$objIES->returnFileUploadResources($scheme_id,$ies_cfg['HandinFileByTeacher'],$ies_cfg['WorksheetFlag_New'],"")."<!--<div id=\"handin_block\"></div>--></td></tr>";

$html_form_action = 'new_update.php';

//$html_form_tag = " name='form1' method='POST' action='new_update.php' onSubmit='return checkForm()'";



### Title ###
$title = $Lang['IES']['Worksheet'];
$TAGS_OBJ[] = array($title,"");

$PAGE_NAVIGATION[] = array($Lang['IES']['Scheme'], "index.php");
$PAGE_NAVIGATION[] = array($schemeData['Title'], "worksheet.php?scheme_id=$scheme_id");
$PAGE_NAVIGATION[] = array($button_new,"");

$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

# button
$html_button .= $linterface->GET_ACTION_BTN($button_submit, "submit")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_reset, "reset")."&nbsp;";
$html_button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "goCancel()");
//$html_button .= "<input type='hidden' name='scheme_id' id='scheme_id' value='{$scheme_id}'>";


$linterface->LAYOUT_START();
include_once("templates/edit.tmpl.php");

echo $linterface->FOCUS_ON_LOAD("form1.title"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>