<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
//include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");

include_once("common.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

# Get parameters from previous page
$task_id = $task_id;
$StudentID = $StudentID;

$CurrentPage = "StudentProgress";

################################################
##	Start: Constructing the html for display
################################################

$libuser = new libuser($StudentID);

$UserDisplayName = (strtoupper($intranet_session_language) == "EN" ? $libuser->EnglishName:$libuser->ChineseName);
$answerAreaTemplate = $objIES->template_AnswerArea02($UserDisplayName);
$IS_DISPLAY_ALL_STEPS = true;
$IS_SHOW_DATE = true;
$html_display=$objIES->getAnswerArea($task_id,"",$StudentID,$answerAreaTemplate,$IS_DISPLAY_ALL_STEPS,$IS_SHOW_DATE);

################################################
##	End: Constructing the html for display
################################################

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");

include_once("templates/step_number_thick_box.tmpl.php");

intranet_closedb();

?>