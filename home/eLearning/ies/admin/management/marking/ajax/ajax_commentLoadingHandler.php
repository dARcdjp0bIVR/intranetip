<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$Action = $_POST["Action"];
$categoryID = $_POST["categoryID"];
$answerEncodeNo = $_POST["answerEncodeNo"];
$language = $_POST["language"];

$libies = new libies();

switch(strtolower($Action)) {
	case "getcomment":
	$comments = $libies->getCommentBank($categoryID);
	if (is_array($comments) && count($comments)>0) {
		switch(strtolower($language)) {
			default:
			case "b5":
			$commentInLang = '';
			break;
			case "en":
			$commentInLang = 'EN';
			break;
		}
		$c_no = 0;
		$h_comment .= "<ul>";
		foreach($comments as $key => $element) {
			if (empty($element["Comment".$commentInLang])) {
				continue;
			}
			$h_comment .= "<li onclick=\"add_from_commentbank(this,'h_commentbank_$c_no', 'text_{$answerEncodeNo}')\">".nl2br($element["Comment".$commentInLang])."</li><input type=\"hidden\" id=\"h_commentbank_$c_no\" VALUE=\"".$element["Comment".$commentInLang]."\">";
        	$c_no++;
		}
		$h_comment .= "</ul>";
	}	
	break;
}



intranet_closedb();




header("Content-Type:   text/xml");
$libies = new libies();
$ParCData = true;
$XML = generate_XML(
					array(
						array("h_comment", $h_comment),
					),$ParCData
				);
echo $XML;
?>