<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$li->Start_Trans();

$sql = "INSERT INTO {$intranet_db}.IES_STAGE_STUDENT ";
$sql .= "(StageID, UserID, Approval, ApprovedBy, DateInput, InputBy, ModifyBy) VALUES ";
$sql .= "({$stage_id}, {$student_id}, {$ies_cfg["DB_IES_STAGE_STUDENT_Approval"]["approved"]}, {$UserID}, NOW(), {$UserID}, {$UserID}) ";
$sql .= "ON DUPLICATE KEY UPDATE Approval = VALUES(Approval), ApprovedBy = VALUES(ApprovedBy), ModifyBy = VALUES(ModifyBy)";
$res = $li->db_db_query($sql);

if($res)
{
  $li->Commit_Trans();
  echo 1;
}
else
{
  $li->RollBack_Trans();
  echo 0;
}

?>