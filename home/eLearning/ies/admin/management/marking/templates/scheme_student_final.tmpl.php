<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
?>
<script language="JavaScript">

function addBatchComment(jBtnObj){
  var formObj = $(jBtnObj).parent().parent();
  var formAction = $(formObj).attr("action");
  
  $.ajax({
    url:      formAction,
    type:     "POST",
    data:     $(formObj).serialize(),
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                // alert(data);
                // Reset textarea since firefox will not reset textarea on page reload
                $("textarea").val("");
                window.location.reload();
              }
  });
}

function stageAllow(jBtnObj)
{
  var student_id = $(jBtnObj).attr("student_id");
  var stage_id = $(jBtnObj).attr("stage_id");

  $.ajax({
    url:      "stage_approval_update.php",
    type:     "POST",
    data:     "stage_id="+stage_id+"&student_id="+student_id,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                if(data == 1) {
                  //alert("Stage approved");
                  $(jBtnObj).replaceWith("<span class=\"IES_allow\">已准許到下一步</span>");
                }
                else {
                  alert("Fail to approve stage");
                }
              }
  });
}

function sendRedoStage(jBtnObj)
{
  var batch_id = jBtnObj.attr("batch_id");

  $.ajax({
    url:      "stage_status_update.php",
    type:     "POST",
    data:     "batch_id="+batch_id+"&batch_status=<?=$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]?>",
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                if(data == 1) {
                  $(jBtnObj).replaceWith("<span class=\"IES_redo\">已發還重做</span>");
                }
                else {
                  alert("FAIL TO SEND REQUEST");
                }
              }
  });
}

function giveMark(jBtnObj)
{
  var formObj = $(jBtnObj).parent().parent();
  var ValInput = true;
  
  $(formObj).find("input[type=text]").each(function(){
    var mark = $(this).val();
  
    if(mark == "")
    {
      alert("Empty");
      $(this).focus();
      ValInput = false;
      return false;
    }
    else if(!IsNumeric(mark))
    {
      alert("Not A Number");
      $(this).focus();
      ValInput = false;
      return false;
    }
  });
  
  if(ValInput)
  {
    $.ajax({
      url:      "stage_mark_update.php",
      type:     "POST",
      data:     $(formObj).serialize(),
      error:    function(xhr, ajaxOptions, thrownError){
                  alert(xhr.responseText);
                },
      success:  function(data){
                  //alert(data);
                }
    });
  }
}

function isOverallResultField(jObj){
  return ($(jObj).attr("name").indexOf("[<?=$ies_cfg["marking_criteria"]["overall_result"][0]?>]") != -1);
}

function setOverallMark(jTextObj){
  var tableObj = $(jTextObj).parent().parent().parent();
  var totalMark = 0;
  var finalResultObj;
  
  if(isOverallResultField($(jTextObj)))
  {
    return false;
  }
  
  $(tableObj).find("input").each(function(){
    if(isOverallResultField($(this)))
    {
      finalResultObj = $(this);
    }
    else if($(this).val() == "")
    {
    }
    else if(IsNumeric($(this).val()))
    {
      totalMark += parseInt($(this).val());
    }
  });
  
  if(IsNumeric(totalMark))
  {
    $(finalResultObj).val(totalMark);
    return true;
  }
}

$(document).ready(function(){
  $("a[name=addComment]").click(function(){
    $(this).parent().parent().find("div[name=comment_box]").show();
    $(this).attr("class", "current");
  });
  
  $("a[name=closeComment]").click(function(){
    $(this).parent().parent().hide();
    $(this).parent().parent().parent().parent().find("a[name=addComment]").removeAttr("class");
  });
  
  $("a[name=addMarking]").click(function(){
    $(this).parent().parent().find("div[name=marking_box]").show();
    $(this).attr("class", "current");
  });
  
  $("a[name=closeMarking]").click(function(){
    $(this).parent().parent().hide();
    $(this).parent().parent().parent().parent().find("a[name=addMarking]").removeAttr("class");
  });
  
  $("input[name=giveMark]").click(function(){
    giveMark($(this));
  });
  
  $("input[name=redo]").click(function(){
    sendRedoStage($(this));
  });
  
  $("input[name=allow]").click(function(){
    stageAllow($(this));
  });
  
  $("select[name=StudentID]").change(function(){
    window.location = "scheme_student_final.php?SchemeID=<?=$SchemeID?>&StudentID="+$(this).val();
  });
});

</script>

<!-- navigation star -->
<div class="navigation">
  <?=$html_navigation?>
<!--
  <img src="/images/2009a/nav_arrow.gif" width="15" height="15" align="absmiddle" /><a href="#">學生進度表</a>
  <img src="/images/2009a/nav_arrow.gif" width="15" height="15" align="absmiddle" /><a href="#">專題001A</a>
  <img src="/images/2009a/nav_arrow.gif" width="15" height="15" align="absmiddle" />
    <select>
      <option>張學明</option>
      <option>陳仔</option>
      <option>阿強</option>
    </select>
-->
  <br />
</div>
<!-- navigation end -->

<div class="content_top_tool">	 	
  <div class="thumb_list_tab">
    <a href="scheme_stage_final.php?SchemeID=<?=$SchemeID?>"><span><img src="/images/2009a/ediscipline/icon_record.gif" width="17" height="20" border="0" align="absmiddle" /><!--{{-->Item<!--}}--></span></a>
    <em>|</em>
    <a href="javascript:;" class="thumb_list_tab_on"><span><img src="/images/2009a/eEnrollment/icon_member.gif" width="20" height="20" border="0" align="absmiddle" /><!--{{-->Student<!--}}--></span></a>
  </div>
  <br style="clear:both" />
</div>
                       
<?=$html_tab_nav?>
                       
<div class="table_board">

  <div class="content_top_tool">
    <div class="Conntent_tool">
      <a href="#" class="export"><?=$Lang['Btn']['Export']?></a>
      <!--<a href="#" class="print"> Print</a>		-->
    </div>
    
    <div class="table_board">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td valign="bottom">      
            <div class="table_filter">        
              <?=$html_student_selection?><br/><br/>
<!--
              <select>
                <option>{{只顯示最新答案}}</option>
                <option>{{顯示所有答案}}</option>
              </select>
-->
            </div><br  style="clear:both"/>
          </td>
          <td valign="bottom">&nbsp;</td>
        </tr>
      </table>
    
      <table class="common_table_list">
          <col style="width:10%" />
          <col style="width:15%" />
          <col style="width:10%" />
          
          <col style="width:30%" />
          <col style="width:35%" />
          <thead>
            <tr>
              <th><?=$Lang['IES']['Stage']?></th>
              <th><?=$Lang['IES']['SubmitDeadLine']?></th>
              <th><?=$Lang['IES']['StudentSubmitData']?></th>
              <th class="sub_row_top">評語</th>
              <th class="sub_row_top"><?=$Lang['IES']['GiveMark']?></th>
            </tr>
          </thead>
        <tbody>
          <?=$html_content?>
        </tbody>
      </table>
    
      <p class="spacer"></p>
      <br />
      <p class="spacer"></p>
  </div></div>
<!--
  <div class="edit_bottom"> 
    <p class="spacer"></p>
    <input name="submit2" type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="儲存" onclick="MM_goToURL('parent','IES_admin_setting_group_detail_flow.html');return document.MM_returnValue" 
    onmouseover="this.className='formbuttonon'" />
    <input name="submit2" type="button" class="formbutton"
    onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="取消" onclick="MM_goToURL('parent','IES_admin_setting_group_detail_flow.html');return document.MM_returnValue" 
    onmouseover="this.className='formbuttonon'" />
    <p class="spacer"></p>
  </div>
-->


</div>




