<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";



$scheme_detail_arr = $objIES->GET_SCHEME_DETAIL($SchemeID);
$SchemeTitle = $scheme_detail_arr["Title"];
$stage_arr = $objIES->GET_STAGE_ARR($SchemeID);

// Get student array in scheme
$student_arr = libies_static::getSchemeStudentArr($SchemeID);
if(empty($StudentID))
{
  header("Location: scheme_student_final.php?SchemeID={$SchemeID}&StudentID={$student_arr[0][0]}");
}
$html_student_selection = getSelectByArray($student_arr, "name=\"StudentID\"", $StudentID, 0, 1, "", 2);

$html_content = "";
for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $stage_arr[$i]["StageID"];
  $t_stage_title = $stage_arr[$i]["Title"];
  $t_stage_detail = $objIES->GET_STAGE_DETAIL($t_stage_id);
  $t_stage_deadline = $t_stage_detail["Deadline"];
  
  // Set stages to "read" status if the status is "submitted"
  $objIES->updateStudentStageBatchHandinStatus($t_stage_id, $StudentID, STATUS_TEACHER_READ, STATUS_SUBMITTED);
  
  $stage_final_handin_arr = libies_static::getStageFinalHandinArr($t_stage_id, $StudentID);
  $sub_content_arr = libies_static::getBatchStrArr($stage_final_handin_arr);
  
  $rowspan_count = count($sub_content_arr);
  
  $t_stage_status = libies_static::isStageAllowed($t_stage_id, $StudentID);
  $html_approval_content = $t_stage_status ? "<span class=\"IES_allow\">已准許到下一步</span>" : "<input type=\"button\" name=\"allow\" student_id=\"{$StudentID}\" stage_id=\"{$t_stage_id}\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"准許到下一步\" />";
  
  $html_content .= "<tr>";
  $html_content .= "<td class=\"title\" rowspan=\"".$rowspan_count."\">".$t_stage_title."<br /><br />".$html_approval_content."</td>";
  $html_content .= "<td class=\"student_ans\" rowspan=\"".$rowspan_count."\">".$t_stage_deadline."</td>";

  for($j=0, $j_max=count($sub_content_arr); $j<$j_max; $j++)
  {
    $html_content .= ($j>0) ? "<tr>" : "";
    $html_content .= $sub_content_arr[$j];
    $html_content .= "</tr>";
  }
  $html_content .= "</tr>";
  
  $tab_arr["tab{$t_stage_id}"] = array("scheme_student_stage.php?SchemeID={$SchemeID}&StageID={$t_stage_id}&StudentID={$StudentID}", $t_stage_title);
}

$tab_arr["tab"] = array("scheme_student_final.php?SchemeID={$SchemeID}&StageID=&StudentID={$StudentID}", $Lang['IES']['StageSubmitStatus']);
$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab{$StageID}");

$MenuArr = array();
$MenuArr[] = array($ies_lang['StudentProgress'], ".");
$MenuArr[] = array($SchemeTitle, "scheme_student_list.php?SchemeID={$SchemeID}&StageID=");
$html_navigation = $linterface->GET_NAVIGATION($MenuArr);

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_student_final.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
