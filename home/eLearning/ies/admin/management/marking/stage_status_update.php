<?php
// mODIFYING bY:
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
$li = new libdb();

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$sql = "SELECT StageID, UserID FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH WHERE BatchID = '{$batch_id}'";
list($stage_id, $student_id) = current($li->returnArray($sql));

$li->Start_Trans();
//DEBUG_R($batch_status);
switch($batch_status)
{
  case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["redo"]:
    $sql = "UPDATE {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
    $sql .= "SET BatchStatus = '{$batch_status}', ModifyBy = '{$UserID}' ";
    $sql .= "WHERE BatchID = '{$batch_id}'";
    $res = $li->db_db_query($sql);

    $objIES->updateStudentStageBatchHandinStatus($stage_id, $student_id, $batch_status, STATUS_TEACHER_READ);
    break;
  case $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["approved"]:
    // Batch Approval
    $sql = "UPDATE {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
    $sql .= "SET BatchStatus = '{$batch_status}', ModifyBy = '{$UserID}' ";
    $sql .= "WHERE BatchID = '{$batch_id}'";
    $res[] = $li->db_db_query($sql);

    // Stage Approval
    $sql = "INSERT INTO {$intranet_db}.IES_STAGE_STUDENT ";
    $sql .= "(StageID, UserID, Approval, ApprovedBy, DateInput, InputBy, ModifyBy) SELECT ";
    $sql .= "StageID, UserID, {$ies_cfg["DB_IES_STAGE_STUDENT_Approval"]["approved"]}, '{$UserID}', NOW(), '{$UserID}', '{$UserID}' ";
    $sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
    $sql .= "WHERE BatchID = '{$batch_id}' ";
    $sql .= "ON DUPLICATE KEY UPDATE Approval = VALUES(Approval), ApprovedBy = VALUES(ApprovedBy), ModifyBy = VALUES(ModifyBy)";
    $res[] = $li->db_db_query($sql);

    // Batch Handin Status in Stage
    $objIES->updateStudentStageBatchHandinStatus($stage_id, $student_id, $batch_status);

    $res = (count($res) == count(array_filter($res)));
    break;
  default:
    break;
}

if($res)
{
  $li->Commit_Trans();
  echo 1;
}
else
{
  $li->RollBack_Trans();
  echo 0;
}

?>