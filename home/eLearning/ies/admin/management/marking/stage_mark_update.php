<?php
// Modifying By: Max
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();
$_assignmentID;

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$li->Start_Trans();

foreach($mark AS $key => $markArr)
{
  $_assignmentID = base64_decode($key);
  
  foreach($markArr AS $markCriteria => $score)
  {
    $sql = "INSERT INTO {$intranet_db}.IES_MARKING ";
    //$sql .= "(AssignmentID, AssignmentType, MarkCriteria, MarkerID, Score, DateInput, InputBy, ModifyBy) VALUES ";
    //$sql .= "({$_assignmentID}, {$AssignmentType}, {$markCriteria}, {$UserID}, {$score}, NOW(), {$UserID}, {$UserID}) ";
    $sql .= "(AssignmentID, AssignmentType, MarkCriteria, StageMarkCriteriaID, MarkerID, Score, DateInput, InputBy, ModifyBy) ";
    $sql .= "SELECT {$_assignmentID}, {$AssignmentType}, {$markCriteria}, ismc.StageMarkCriteriaID, {$UserID}, {$score}, NOW(), {$UserID}, {$UserID} ";
    $sql .= "FROM {$intranet_db}.IES_STAGE_MARKING_CRITERIA ismc ";
    $sql .= "INNER JOIN {$intranet_db}.IES_STAGE_HANDIN_BATCH ishb ";
    $sql .= "ON ismc.StageID = ishb.StageID ";
    $sql .= "WHERE ishb.BatchID = {$_assignmentID} AND ismc.MarkCriteriaID = {$markCriteria} ";
    $sql .= "ON DUPLICATE KEY UPDATE Score = VALUES(Score), MarkerID = VALUES(MarkerID), ModifyBy = VALUES(ModifyBy)";
    $res[] = $li->db_db_query($sql);
    
    if($markCriteria == MARKING_CRITERIA_OVERALL)
    {
      $sql = "INSERT INTO {$intranet_db}.IES_STAGE_STUDENT ";
      $sql .= "(StageID, UserID, Score, DateInput, InputBy, ModifyBy) ";
      //$sql .= "SELECT StageID, UserID, {$score}, NOW(), {$UserID}, {$UserID} ";
      //$sql .= "FROM {$intranet_db}.IES_STAGE_HANDIN_BATCH ";
      //$sql .= "WHERE BatchID = {$_assignmentID} ";
      $sql .= "VALUES ";
      $sql .= "({$StageID}, {$StudentID}, {$score}, NOW(), {$UserID}, {$UserID})";
      $sql .= "ON DUPLICATE KEY UPDATE Score = VALUES(Score), ModifyBy = VALUES(ModifyBy)";
      $res[] = $li->db_db_query($sql);
    }
  } 
}

$final_res = (count($res) == count(array_filter($res)));
if($final_res)
{
  $li->Commit_Trans();
  $objIES->updateStudentStageBatchHandinStatus($StageID, $StudentID, STATUS_TEACHER_READ, STATUS_SUBMITTED);
  $lastMarkerArr = libies_static::getLastMarker($_assignmentID, $ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]);
  $markerName = $lastMarkerArr["MarkerName"];
  $lastModifiedDate = $lastMarkerArr["LastModified"];
  $html_latest_modified = empty($lastMarkerArr) ? "" : "({$lastModifiedDate} {$markerName})";
  echo $html_latest_modified;
}
else
{
  $li->RollBack_Trans();
  
}

// re-calculate scheme total mark after weight and max mark update
libies_static::calculateSchemeMark($SchemeID,$StudentID);

intranet_closedb();

?>