<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$scoreStr = '';
foreach($_POST AS $input_name => $input_val){
  $old_num = $input_name;	

	list($snapshotID, $taskID, $studentID) = explode("_", base64_decode($input_name));

	$createNewScoreSqlFlag = NULL;
	
	if(empty($snapshotID)){
		// snapshot current answer
		$sql = "INSERT INTO {$intranet_db}.IES_TASK_HANDIN_SNAPSHOT ";
		$sql .= "(TaskID, UserID, Answer, QuestionType, AnswerTime, DateInput, InputBy) ";
		$sql .= "SELECT TaskID, UserID, Answer, QuestionType, DateModified, NOW(), {$UserID} ";
		$sql .= "FROM IES_TASK_HANDIN WHERE TaskID = {$taskID} AND UserID = {$studentID}";
		$res[] = $li->db_db_query($sql);
		
		$snapshotID = $li->db_insert_id();

		$createNewScoreSqlFlag = true;
	}else{

		$scoreIsGiveBefore = libies_static::getTaskScoreArr($snapshotID);

		if(is_array($scoreIsGiveBefore) && count($scoreIsGiveBefore) > 0){
			// there is a score for the snapshot, no use new score sql
			$createNewScoreSqlFlag = false;
		}else{
			$createNewScoreSqlFlag = true;
		}
	}

	$scoreSql = '';
	if($createNewScoreSqlFlag == true){
		$scoreSql  = 'insert into '.$intranet_db.'.IES_TASK_HANDIN_SCORE ';
		$scoreSql .= "(SnapshotAnswerID, Score, InputBy) VALUES ";
		$scoreSql .= "({$snapshotID}, ".$input_val.", {$UserID})";
	}else{
		$scoreSql = 'update '.$intranet_db.'.IES_TASK_HANDIN_SCORE set Score = '.$input_val.'  , InputBy = '.$UserID.' ,DateInput = now() where SnapshotAnswerID = '.$snapshotID;
	}

	if($scoreSql != ''){
		$res[] = $li->db_db_query($scoreSql);
	}

	
	$final_res = (count($res) == count(array_filter($res)));
	if($final_res)
	{

		$scoreArr = libies_static::getTaskScoreArr($snapshotID);

		if(is_array($scoreArr) && count($scoreArr) == 1){
			//suppose return one array only
			$scoreArr = current($scoreArr);
			$_score = $scoreArr['score'];
			$t_inputdate = $scoreArr['DateInput'];
			$t_teacher = $scoreArr['scoreTeacher'];

			$scoreStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";



		}
	}

	   $answerEncodeNo = base64_encode($snapshotID."_".$taskID."_".$studentID);

	   $XML = $objIES->generateXML(
			array(
				array("returnStr", $scoreStr), array("new_code", $answerEncodeNo),array("old_code", $old_num)
			), TRUE
		);

header("Content-Type:   text/xml");
echo $XML;
}



exit();
?>