<?php
//modifying By 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");

//include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");

include_once("common.php");
intranet_auth();
intranet_opendb();

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

# Get parameters from previous page
$stage_id = $stage_id;
$StudentID = $StudentID;

$libuser = new libuser($StudentID);

$UserDisplayName = (strtoupper($intranet_session_language) == "EN" ? $libuser->EnglishName:$libuser->ChineseName);

if(is_numeric($stage_id)){
	$taskDetails = $objIES->GET_TASK_ARR($stage_id);

	for($i =0,$i_max = sizeof($taskDetails);$i< $i_max; $i++){
		$_score = '';
		$_dateInput = '';
		

		$_TaskID = $taskDetails[$i]['TaskID'];
		$_TaskTitle = $taskDetails[$i]['Title'];

				
		$snapShotAnswer = $objIES->getTaskSnapshotAnswer($_TaskID, $StudentID);

		if(is_array($snapShotAnswer) && (count($snapShotAnswer) > 0)){
			//since the return arry is order by AnswerTime, only get the most updated snap shot
			$_snapShotAnswer = current($snapShotAnswer);
			$_snapShotAnswerID = $_snapShotAnswer['SnapshotAnswerID'];

			// current --> suppose only return on array only
			$_taskScoreArr = current(libies_static::getTaskScoreArr($_snapShotAnswerID));
			$_score = $_taskScoreArr['score'];
			$_dateInput = $_taskScoreArr['DateInput'];
		}

		$_score = ($_score == '') ? '--': $_score;
		$_dateInput = ($_dateInput == '') ? '--': $_dateInput;

		$html .= '<tr>';
		$html .= '<td>'.$_TaskTitle.'</td>';
		$html .= '<td>'.$_score.'&nbsp;</td>';
		$html .= '<td>'.$_dateInput.'&nbsp;</td>';
		$html .= '</tr>';
	}

}else{
	$html .= '<tr><td rowspan="3">Stage ID not found!</td></tr>';
}

	$HTML_Template = <<<HTMLEND
<div class="IES_thickbox_list">		
	<table class="form_table" style="width:100%">
	   
	     
		<col class="field_title" />
		<col  class="field_c" />
		
		<tr>
		<td>{$Lang['Identity']['Student']}</td>
		<td>:</td>
		<td>{$UserDisplayName}</td>
		</tr>
	</table>
	<table class="common_table_list">
		<col width="150px"/>
		<col />		
		<thead>
		<tr>
		<th>{$Lang['IES']['Task']}</th>
		
		<th>{$Lang['IES']['Mark']}</th>
		<th>{$Lang['IES']['inputdate']}</th>
		                                           
		</tr>
		</thead>
		{{{ HTML_ANSWER_AREA }}}
	</table> 
</div>
HTMLEND;


$html_display = str_replace("{{{ HTML_ANSWER_AREA }}}",$html,$HTML_Template);


include_once("templates/step_number_thick_box.tmpl.php");

intranet_closedb();

exit();

?>