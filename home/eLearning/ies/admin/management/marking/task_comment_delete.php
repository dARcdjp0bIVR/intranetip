<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$sql = "DELETE FROM {$intranet_db}.IES_TASK_HANDIN_COMMENT WHERE SnapshotCommentID = $SnapshotCommentID";
$res = $li->db_db_query($sql);

if($res)
{	
	
    $commentStr = "";
    if(!empty($snapshotID))
    {
      $commentArr = libies_static::getTaskCommentArr($snapshotID);
      for($j=0, $j_max=count($commentArr); $j<$j_max; $j++)
      {
        $t_comment = nl2br($commentArr[$j]["Comment"]);
        $t_inputdate = $commentArr[$j]["DateInput"];
        $t_teacher = $commentArr[$j]["CommentTeacher"];
        
        $commentStr .= "<div style=\"float:left; width:85%;\">".$t_comment."</div>";
        $commentStr .= "<div class=\"table_row_tool\" style=\"float:right\"><a href=\"javascript:void(0);\" onclick=\"DeleteComment('".$commentArr[$j]["SnapshotCommentID"]."', $snapshotID, this)\" class=\"delete_dim\" title=\"{$Lang['IES']['Delete2']}\"></a></div>";
	    $commentStr .= "<div style=\"width:100%; float:left;\"><span class=\"update_record\">({$t_inputdate} {$Lang['IES']['From']} {$t_teacher})</span></div>";
     // $commentStr .= "<input type=\"button\" value=\"{$Lang['IES']['Delete2']}\" onmouseout=\"this.className='formsmallbutton'\" onmouseover=\"this.className='formsmallbuttonon'\" class=\"formsmallbutton\" onclick=\"DeleteComment('".$commentArr[$j]["BatchCommentID"]."', $batchID, this)\" style=\"float: right;\">";
        $commentStr .= "<div class=\"edit_top\"></div>";
      }
    }
    echo $commentStr;
}



?>