<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/libuser.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once("{$PATH_WRT_ROOT}includes/libfilesystem.php");
intranet_auth();
intranet_opendb();


# Get request parameters
$StageID = $_REQUEST["StageID"];


# Retrieve Data
$libies = new libies();
$fileDetailsArray = $libies->getFileDetailsByStageID($StageID);
$parentDetails = current($libies->getParentsIds("stage",$StageID));
$schemeDetail = $libies->GET_SCHEME_DETAIL($parentDetails["SCHEMEID"]);
$schemeTitle = $schemeDetail["Title"];


# Prepare File structure array
$currentTime = date("YmdHis",time());
$libfilesystem = new libfilesystem();
$fileNameCountArray = array();
$reconstructedDetailsArray = array();
if (is_array($fileDetailsArray)) {
	foreach($fileDetailsArray as $key => $details) {
		$fileNameCountArray[$details["FILENAME"]]++;
		# filename exist more then once need to rename
		if ($fileNameCountArray[$details["FILENAME"]]>1) {
			$_tmpFileName = explode(".",$details["FILENAME"]);
			$ext = array_pop($_tmpFileName);			
			$outputFileName = implode(".",$_tmpFileName)." (".$fileNameCountArray[$details["FILENAME"]].").".$ext;
			$fileNameCountArray[$outputFileName]++;
		} else {
			$outputFileName = $details["FILENAME"];
		}
		if (!isset($reconstructedDetailsArray[$details["CLASSTITLE"]][$details["CLASSNUMBER"]][$details["STUDENTNAME"]])) {
			$reconstructedDetailsArray[$details["CLASSTITLE"]][$details["CLASSNUMBER"]][$details["STUDENTNAME"]] = array();
		}
		array_push($reconstructedDetailsArray[$details["CLASSTITLE"]][$details["CLASSNUMBER"]][$details["STUDENTNAME"]],array("OUT_FILE_NAME"=>$libfilesystem->convert_basename_from_utf8_to_big5($outputFileName),"SRC_PATH"=>$details["FILEABSPATH"]));
	}
}


# Prepare paths
$safeGuardedFileName = $libfilesystem->SafeGuardFileName($schemeTitle."_".$currentTime);
$outMostFolderName = $libfilesystem->SafeGuardFileName(base64_encode($safeGuardedFileName));	// the encode is used to cater chinese name
$targetPath = $libfilesystem->SafeGuardFileName("/tmp/ies_stage_file/");
$outMostFolder = $libfilesystem->SafeGuardFileName($targetPath.$outMostFolderName);


# Create folder structure
if (is_array($reconstructedDetailsArray)) {
	foreach($reconstructedDetailsArray as $className => $student) {
		if (is_array($student)) {
			foreach($student as $classNumber => $studentFileDetail) {
				if (is_array($studentFileDetail)) {
					foreach($studentFileDetail as $studentName => $files) {
						$folderPath = $outMostFolder.$libfilesystem->SafeGuardFileName("/".$className."/".$classNumber."/".$studentName);
						$libfilesystem->folder_new($folderPath);
						
						if (is_array($files)) {
							foreach($files as $key => $fileDetail) {
								$destPath = $folderPath."/".$fileDetail["OUT_FILE_NAME"];
								$libfilesystem->file_copy($fileDetail["SRC_PATH"],$destPath);
								
							}
						}
					}
				}
			}
		}
	}
}

# ZIP the file
chdir("$outMostFolder");
$Str = exec("zip -r ../".OsCommandSafe($outMostFolderName).".zip *");


# Output to browswer
output2browser(get_file_content($outMostFolder.".zip"),base64_decode(basename($outMostFolder)).".zip");

intranet_closedb();
?>
