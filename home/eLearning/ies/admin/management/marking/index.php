<?php
//modifying By 

/**********************************************************************
* Modified by Ivan (20101213)
* - added cookies logic to store the Search Textbox Value
*  
* Checked by Eric (20100619)
* - matching template: templates/index.tmpl.php
* - comment added
* - special character search: pass
**********************************************************************/ 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_dbtable.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_ies_management_studentProgress_searchText", "searchText");	

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

intranet_auth();
intranet_opendb();

$objIES = new libies();
$ldb = new libdb();

if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";


//handle a teacher login , show his / her scheme only
$teacherSQLQuery = "";
$teacherCriteria = "";
if($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"]){
	//check not a IES Admin login
	//do nothing
}else{
	$teacherSQLQuery = "LEFT JOIN {$intranet_db}.IES_SCHEME_TEACHER ies_st ON ies_st.SchemeID = scheme.SchemeID ";
	$teacherCriteria = " and ies_st.UserID = {$UserID} ";  // the teacher id is equal to this teacher login
	$distinct = " distinct ";// there is duplicate record if a teacher is both a teaching teacher and marking teacher (left join IES_SCHEME_TEACHER );
}

// Temporary table for storing scheme list
$sql = "CREATE TEMPORARY TABLE tempScheme (Title varchar(255), StuCount int(8), TeachingTeacher varchar(255), AssessTeacher varchar(255), Steps varchar(255), SchemeID int(8), PRIMARY KEY (SchemeID)) DEFAULT CHARSET=utf8";
$ldb->db_db_query($sql);

$searchText = stripslashes($searchText);
$cond = ($searchText == "") ? "" : " AND scheme.Title LIKE '%".mysql_real_escape_string($searchText)."%'";
$sql = "INSERT INTO 
			tempScheme (Title, StuCount, TeachingTeacher, AssessTeacher, Steps, SchemeID) 

		select Title ,count(SchemeStudentID),TeachingTeacher,AssessTeacher,Steps ,SchemeID 
		from (
			SELECT 
				{$distinct}
				scheme.Title, ies_ss.SchemeStudentID, '&nbsp;' as 'TeachingTeacher', '&nbsp;' as 'AssessTeacher', '&nbsp;' as 'Steps', scheme.SchemeID 
			FROM 
				{$intranet_db}.IES_SCHEME scheme 
			LEFT JOIN 
				{$intranet_db}.IES_SCHEME_STUDENT ies_ss ON scheme.SchemeID = ies_ss.SchemeID 
			{$teacherSQLQuery}
			WHERE 1 
				and SchemeType = '{$ies_cfg["moduleCode"]}'
				{$cond} 
				{$teacherCriteria}
		) as result1
		group by SchemeID 
		";

$ldb->db_db_query($sql);
$html_searchText = htmlentities($searchText, ENT_QUOTES, "UTF-8");


// Eric Yip (20101015) : Optimized by performing 2 queries instead of looping
if($intranet_session_language=='b5'){
	$TeachersNameExtractSQL = "GROUP_CONCAT(ifnull(iu.ChineseName,iu.EnglishName) SEPARATOR '<br />')";
}else{
	$TeachersNameExtractSQL = "GROUP_CONCAT(ifnull(iu.EnglishName,iu.ChineseName) SEPARATOR '<br />')";
}
$sql = "UPDATE tempScheme ";
$sql .= "INNER JOIN (";
$sql .= "SELECT scheme_teacher.SchemeID, {$TeachersNameExtractSQL} AS TeachingTeacher ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID ";
$sql .= "WHERE scheme_teacher.TeacherType = {$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["TeachingTeacher"]} ";
$sql .= "GROUP BY scheme_teacher.SchemeID) AS t_tt ";
$sql .= "ON tempScheme.SchemeID = t_tt.SchemeID ";
$sql .= "SET tempScheme.TeachingTeacher = t_tt.TeachingTeacher";
$ldb->db_db_query($sql);

$sql = "UPDATE tempScheme ";
$sql .= "INNER JOIN (";
$sql .= "SELECT scheme_teacher.SchemeID, {$TeachersNameExtractSQL} AS AssessTeacher ";
$sql .= "FROM {$intranet_db}.IES_SCHEME_TEACHER scheme_teacher ";
$sql .= "INNER JOIN {$intranet_db}.INTRANET_USER iu ON scheme_teacher.UserID = iu.UserID ";
$sql .= "WHERE scheme_teacher.TeacherType = {$ies_cfg["DB_IES_SCHEME_TEACHER_TeacherType"]["AssessTeacher"]} ";
$sql .= "GROUP BY scheme_teacher.SchemeID) AS t_at ";
$sql .= "ON tempScheme.SchemeID = t_at.SchemeID ";
$sql .= "SET tempScheme.AssessTeacher = t_at.AssessTeacher";
$ldb->db_db_query($sql);

# Main query
if ($order=="") $order=1;
if ($field=="") $field=0;
$LibTable = new libies_dbtable($field, $order, $pageNo);
$sql = "SELECT CONCAT('<a href=\"scheme_student_list.php?SchemeID=', SchemeID,'\">', Title, '</a>') as 'schemeTitle', StuCount, TeachingTeacher, AssessTeacher, Steps, SchemeID FROM tempScheme";

// TABLE INFO
$LibTable->field_array = array("Title", "StuCount");
$LibTable->sql = $sql;
$LibTable->no_msg = $no_record_msg;
$LibTable->page_size = ($numPerPage=="") ? $page_size : $numPerPage;
$LibTable->no_col = 5;
$LibTable->table_tag = "<table class=\"common_table_list\">";
$LibTable->row_alt = array("#FFFFFF", "#F3F3F3");
$LibTable->row_height = 20;
$LibTable->sort_link_style = "class='tbheading'";
$LibTable->row_valign = "top";

// TABLE COLUMN
$LibTable->column_list = "<thead>";
$LibTable->column_list .= "<tr class=\"tabletop\">";
$LibTable->column_list .= "<th width=\"5%\" align=\"center\">#</th>";
$LibTable->column_list .= "<th width=\"45%\">".$LibTable->column(0,$Lang['IES']['Scheme'])."</th>";
$LibTable->column_list .= "<th width=\"10%\">".$LibTable->column(1,$Lang['IES']['StudentNumber'])."</th>";
$LibTable->column_list .= "<th width=\"20%\">{$Lang['IES']['TeachingTeacher']}</th>";
$LibTable->column_list .= "<th width=\"20%\">{$Lang['IES']['AssessTeacher']}</th>";
$LibTable->column_list .= "</tr>";
$LibTable->column_list .= "</thead>";

$html_scheme_list = $LibTable->displayPlain();
$html_scheme_list .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"h-13-black\">";
$html_scheme_list .= ($LibTable->navigationHTML!="") ? "<tr class='tablebottom'><td class=\"tabletext\" align=\"right\">".$LibTable->navigationHTML."</td></tr>" : "";
$html_scheme_list .= "</table>";


### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>