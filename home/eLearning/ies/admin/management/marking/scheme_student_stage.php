<?php
//modifying By Eric
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");

include_once("common.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$CurrentPage = "StudentProgress";

// Get teacher type of scheme
// 1D-Array format, since teacher may have more than one identity in scheme
$teacher_type_arr = libies_static::getSchemeTeacherType($SchemeID, $UserID);

$objIES = new libies();
if(!$objIES->CHECK_ACCESS("IES-MGMT-STUDENTPROG-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$scheme_detail_arr = $objIES->GET_SCHEME_DETAIL($SchemeID);
$scheme_stage_arr = $objIES->GET_STAGE_ARR($SchemeID);
$stage_detail_arr = $objIES->GET_STAGE_DETAIL($StageID);
$task_arr = $objIES->GET_TASK_ARR($StageID);

// Stage Tab
$tab_arr = array();
for($i=0, $i_max=count($scheme_stage_arr); $i<$i_max; $i++)
{
  $t_stage_id = $scheme_stage_arr[$i]["StageID"];
  $t_stage_title = $scheme_stage_arr[$i]["Title"];

  $tab_arr["tab{$t_stage_id}"] = array("scheme_student_stage.php?SchemeID={$SchemeID}&StageID={$t_stage_id}&StudentID={$StudentID}", $t_stage_title);
}
//$tab_arr["tab"] = array("scheme_student_final.php?SchemeID={$SchemeID}&StageID=&StudentID={$StudentID}", $Lang['IES']['StageSubmitStatus']);
$tab_arr["tab"] = array("scheme_stage_final.php?SchemeID={$SchemeID}&StageID=", $Lang['IES']['StageSubmitStatus']);
$html_tab_nav = libies_ui::GET_TAB_MENU($tab_arr,"tab{$StageID}");

// Student Selection
$student_arr = libies_static::getSchemeStudentArr($SchemeID);
$html_student_selection = getSelectByArray($student_arr, "name=\"StudentID\"", $StudentID, 0, 1, "", 2);

$SchemeTitle = $scheme_detail_arr["Title"];

$html_content = "";
$html_js_array ="";
for($i=0, $i_max=count($task_arr); $i<$i_max; $i++)
{
  $t_task_id = $task_arr[$i]["TaskID"];
  $t_title = $task_arr[$i]["Title"];
  $t_step_count = count($objIES->GET_STEP_ARR($t_task_id));
  $t_is_enable = $objIES->isTaskEnable($t_task_id);
  
  if(!$t_is_enable) continue;
  
  // check if task is survey (limited to student has submitted task handin)
  $t_taskType = $objIES->CheckTaskAnsType($t_task_id);
  $t_taskIsSurvey = (strpos(strtoupper($t_taskType), "SURVEY") !== false);
  
  // check if current answer exist in snapshot
  $t_currentStep_included = isCurrentAnswerInSnapshot($t_task_id, $StudentID);
  // get snapshot array
  $displayAnswerArr = getTaskSnapshotAnswer($t_task_id, $StudentID);
   
  $sub_content_arr = array();
  if(!$t_currentStep_included)
  {
    // prepare for display current task answer
    $curAnswerArr = getTaskAnswer($t_task_id, $StudentID);
    if(!empty($curAnswerArr))
    {
      array_unshift($displayAnswerArr, $curAnswerArr);
    }
  }
  $sub_content_arr = libies_static::getTaskAnswerStrArr($displayAnswerArr, $teacher_type_arr, $StageID);

  $next_task_id = libies_static::getNextTask($t_task_id);
  $next_task_need_allowance = libies_static::isTaskNeedAllowance($next_task_id);

  $html_approval_content = "";
  if(isset($next_task_id) && $next_task_need_allowance)
  {
    $t_task_status = libies_static::isTaskAllowed($next_task_id, $StudentID);
    $html_approval_content = $t_task_status ? "<span class=\"IES_allow\">{$Lang['IES']['AllowedToNextStep']}</span>" : "<input type=\"button\" name=\"approve\" student_id=\"{$StudentID}\" task_id=\"{$next_task_id}\" class=\"formsmallbutton\" onmouseover=\"this.className='formsmallbuttonon'\" onmouseout=\"this.className='formsmallbutton'\" value=\"{$Lang['IES']['AllowToNextStep']}\" />";
  }

  // count row span
  $rowspan_count = count($sub_content_arr);
  $html_js_array .= "span_array[{$i}]={$rowspan_count};";
  
  // Step thickbox link
  $html_step_thickbox_link = "<a href=\"step_number_thick_box.php?KeepThis=true&task_id=$t_task_id&StudentID=$StudentID&TB_iframe=true&height=400&width=700\" class=\"thickbox view_step\" title=\"{$Lang['IES']['StudentInputData']}\">&nbsp;</a>";
  	
  // generate display string
  $html_content .= "<tr>";
  $html_content .= "<td class=\"title column_$i\" >".$t_title."<br /><br />{$html_approval_content}<br \>";
//  if(count($sub_content_arr) > 1){
//  	$html_content .= "<a href=\"javascript:ShowTable($i)\"\">+</a>";
//  }
  	
  $html_content .=	"</td>";
  $html_content .= "<td class=\"student_ans column_$i\" >".$t_step_count."</td>";

  for($j=0, $j_max=count($sub_content_arr); $j<$j_max; $j++)
  {
    $html_content .= ($j>0) ? "<tr class=\"display_$i\" style=\"display:none\">" : "";
    
    // show step answer, but not for task type "survey"
    $html_content .= (!$t_taskIsSurvey && $j==0) ? preg_replace("/(<td class=\"student_ans\">)(.*)(<\/td>)(<td class=\"teacher_comment\">.*)/Us", "$1<div style=\"float:left; width: 95%;\">$2</div><div style=\"float:left; width:5%;\">{$html_step_thickbox_link}</div>$3$4", $sub_content_arr[$j], 1) : $sub_content_arr[$j];
    $html_content .= "</tr>";
  }
}

$MenuArr = array();
$MenuArr[] = array($ies_lang['StudentProgress'], ".");
$MenuArr[] = array($SchemeTitle, "scheme_student_list.php?SchemeID={$SchemeID}&StageID={$StageID}");
$MenuArr[] = array($html_student_selection, "");
$html_navigation = $linterface->GET_NAVIGATION($MenuArr);

### Title ###
$title = $ies_lang['StudentProgress'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
include_once("templates/scheme_student_stage.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
