<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();

if(!$objIES->CHECK_ACCESS("IES-MGMT-FAQ-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

	
if(!isset($id))
	header("Location: index.php");
	
$ldb = new libdb();	
	
$sql = "DELETE FROM IES_FAQ WHERE QuestionID=$id";
$ldb->db_db_query($sql);

$sql = "DELETE FROM IES_FAQ_RELATIONSHIP WHERE QuestionID=$id";
$ldb->db_db_query($sql);

intranet_closedb();

header("Location: index.php?msg=delete&sch_id=$s_id&questionType=$questionType&recordstatus=$recordstatus&searchText=".intranet_htmlspecialchars($searchText));
?>