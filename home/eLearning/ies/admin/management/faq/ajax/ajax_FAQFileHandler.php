<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$fileHashName = $_POST["fileHashName"];
$libies = new libies();
$libfilesystem = new libfilesystem();
$FAQAttachmentDetails = current($libies->getFAQAttachmentDetails_byFileHashName($fileHashName));
if ($libies->removeFAQAttachmentRecord($fileHashName)) {
	$result = $libfilesystem->file_remove("{$intranet_root}{$FAQAttachmentDetails["FOLDERPATH"]}{$fileHashName}");
}


if ($result) {
	echo 1;
} else {
	echo 0;
}
intranet_closedb();
?>