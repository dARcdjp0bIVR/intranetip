<?php
# the following script is included by new_update.php and edit_update.php to perform the save upload on files
# *$qid is required and all the libraries required are included in new_update.php and edit_update.php
$libfilesystem = new libfilesystem();
$finalUploadResult = false;
$resultCreatedFolder = false;

if (!empty($_FILES)) {
	$targetPath = "/file/ies/faq/question_$qid/";
	# create folder
	$targetFullPath = str_replace('//', '/', $intranet_root.$targetPath);
	if ( file_exists($targetFullPath) ) {
		$resultCreatedFolder = true;
	} else {
		$resultCreatedFolder = $libfilesystem->folder_new($targetFullPath);
	}
	
	chmod($targetFullPath, 0755);
	
	$numOfFiles = count($_FILES['attachments']['name']);
	
	for($i=0;$i<$numOfFiles;$i++) {
		# get file name
		$tempFile = $_FILES['attachments']['tmp_name'][$i];
		$origFileName =  $_FILES['attachments']['name'][$i];
		if (empty($origFileName)) continue;
		
		# encode file name
		$targetFileHashName = sha1($qid.microtime());
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# upload file
		$uploadResult = move_uploaded_file($tempFile, $targetFile);
		
		# add record to DB
		$libies = new libies();
		$libies->addFAQAttachment($qid,$targetPath,$origFileName, $targetFileHashName, $UserID);
	}
}
?>