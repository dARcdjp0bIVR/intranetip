<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();


$objIES = new libies();
$ldb = new libdb();
$msg = "delete";

if(!$objIES->CHECK_ACCESS("IES-MGMT-COMMENTBANK-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}

$sql = "DELETE FROM {$intranet_db}.`IES_COMMENT` WHERE CommentID = $id";

$result = $ldb->db_db_query($sql);

intranet_closedb();
$redirectTaskInfo = "task=listComment&categoryID=$categoryID";
header("Location: index.php?msg=".$msg."&$redirectTaskInfo");
?>
