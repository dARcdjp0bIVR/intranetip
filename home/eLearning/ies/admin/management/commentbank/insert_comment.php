<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$categoryID = $_POST["categoryID"];

$objIES = new libies();
$ldb = new libdb();
$msg = "add";

$addcomment = array_map("htmlspecialchars",$addcomment);
if(!$objIES->CHECK_ACCESS("IES-MGMT-COMMENTBANK-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}
$redirectTaskInfo = "task=listComment&categoryID=$categoryID";
if(!empty($addcomment)){
	$sql = "INSERT INTO {$intranet_db}.IES_COMMENT ";
	$sql .= "(Comment, DateInput, InputBy, ModifyBy, CommentEN, CategoryID) ";
	$sql .= "VALUES ";
	$sql .= "('$addcomment[B5]', NOW(), $UserID, $UserID, '$addcomment[EN]', $categoryID)";
	$result = $ldb->db_db_query($sql);
	
	intranet_closedb();
	
	header("Location: index.php?msg=".$msg."&$redirectTaskInfo");
}
else{
	echo "<script language='javascript'>";
	echo "alert('Comment blanked');";
	echo "window.location='index.php?$redirectTaskInfo'";
	echo "</script>";
}
?>
