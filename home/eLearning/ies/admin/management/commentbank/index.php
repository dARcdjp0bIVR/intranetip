<?php
//modifying By : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

############################## Access checking for Commentbank
$objIES = new libies();
$ldb = new libdb();

if(!$objIES->CHECK_ACCESS("IES-MGMT-COMMENTBANK-ALL")) {
	$objIES->NO_ACCESS_RIGHT_REDIRECT();
}


############################## Handling page settings
$linterface = new interface_html($ies_cfg["DefaultInterface"]);
$html_message = isset($msg) ? $linterface->GET_SYS_MSG($msg) : "";

$CurrentPage = "CommentBank";

$title = $Lang['IES']['CommentBank'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();


############################## Handling task
$task = trim($task);
$task = ($task == "")? "listCategory" : $task;
switch($task)
{
  case "ajax":
    $task_script = "ajax/" . str_replace("../", "", $script) . ".php";

		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
		
	default:
		// By default, open the script with the same name as the task (after appended with ".php").
		// Prevent searching parent directories, like 'task=../../index'.
		$task_script = "task/" . str_replace("../", "", $task) . ".php";
		
		// Task exists?
		if (file_exists($task_script)) {
			include_once($task_script);
		}
		else {
			$linterface->LAYOUT_START();
			echo "error! task not find<br/>";
			$linterface->LAYOUT_STOP();
		}
		break;
			
}

intranet_closedb();
?>
