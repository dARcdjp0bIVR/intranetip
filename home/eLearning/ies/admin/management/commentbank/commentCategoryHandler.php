<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");

intranet_auth();
intranet_opendb();

$Action = $_REQUEST["Action"];

$libies = new libies();
switch(strtolower($Action)) {
	default:
	case "new":
	$title = $_REQUEST["addcommentcategory"];
	$result = $libies->addCommentCategory($title);
	if ($result) {
		$msg = "add";
	} else {
		$msg = "update_failed";
	}
	break;
	case "edit":
	$title = $_REQUEST["editcommentcategory"];
	$categoryID = $_REQUEST["id"];
	$result = $libies->editCommentCategory($categoryID,$title);
	if ($result) {
		$msg = "update";
	} else {
		$msg = "update_failed";
	}
	break;
	case "remove":
	$categoryID = $_REQUEST["id"];
	$result = $libies->removeCommentCategory($categoryID);
	if ($result) {
		$msg = "delete";
	} else {
		$msg = "delete_failed";
	}
	break;
}
intranet_closedb();
$redirectTaskInfo = "task=listCategory";
header("Location: index.php?msg=".$msg."&$redirectTaskInfo");
?>
