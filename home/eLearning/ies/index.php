<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

// temporary access right checking
if(!$_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] && !$_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"]) {
	header("Location: /");
	exit;
}


//$UserType _SESSION VALUE
switch($UserType)
{
  // Teacher
  case 1:
    $url = "admin/management/marking/";
    break;
  // Student
  case 2:
    $url = "coursework/";
    break;
  default:
    die();
    break;
}

header("Location: ".$url);

?>