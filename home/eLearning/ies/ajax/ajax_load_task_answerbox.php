<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();

$_tmpTaskAns = $objIES->gettaskAnswer_withQuestionType($taskID, $UserID);

$taskAns = $_tmpTaskAns["Answer"];
$taskQuestionType = $_tmpTaskAns["QuestionType"];

if (in_array(strtolower($taskQuestionType),array("table","survey:edit","survey:collect"))) {
	$taskAns = $Lang['IES']['TaskAnswerNotAvailable'];
	$display_taskAns = nl2br($taskAns);
} else if(empty($taskAns))
{
	$copy_link = "&nbsp;";
	$display_taskAns = $Lang['IES']['NoRecordAtThisMoment'];
}
else
{
	$copy_link = "<a class=\"tablelink\" href=\"javascript:copy_answer();\">{$Lang['IES']['CopyTaskAnswer']}</a>";
	$display_taskAns = nl2br($taskAns);
}

?>

<table>
<tr><td><?=$copy_link?></td></tr>
<tr><td>
<?=$display_taskAns?>
</td></tr>
</table>

<input type="hidden" id="taskAnsHidden" value="<?=$taskAns?>" />

<?php
intranet_closedb();
?>