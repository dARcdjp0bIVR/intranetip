<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();
//DEBUG_R($objIES->getTaskSnapshotAnswer($task_id, $UserID));
//DEBUG_R($objIES->getCommentArr(21));
$AllTaskSnapshotAnswerIDArray = array();
$TaskSnapshotAnswer = $objIES->getTaskSnapshotAnswer($task_id, $UserID);


list($tasktype) = $objIES->get_task_type($task_id);

if(sizeof($TaskSnapshotAnswer) > 0){
	

	foreach($TaskSnapshotAnswer as $value){
		$AllTaskSnapshotAnswerIDArray[] = $value["SnapshotAnswerID"];
		$teachercomment_ARR = $objIES->getCommentArr($value["SnapshotAnswerID"]);
		$teacherScore_ARR = $objIES->getTaskHandInScore($value["SnapshotAnswerID"]);				

		$snapShotScore = '';
		//requirement on 20110916 , assume there is only one score for each snapshot
		if(is_array($teacherScore_ARR) && count($teacherScore_ARR) > 0){
			$teacherScore_ARR = current($teacherScore_ARR);
			$snapShotScore = $Lang['IES']['Mark'].' : '.$teacherScore_ARR['Score'].'<span class="update_record"> ('.$teacherScore_ARR["DateInput"].$Lang['IES']['CommentBy'].$teacherScore_ARR["ScoreTeacher"].')</span>';
		}

	?>
	
	<div class="IES_comment_entry">
	<?
	//DON"T display the answer if it is an survey###
	if(!stristr($tasktype, "survey")){
	?>
		<div class="stu_ans">
		<?=nl2br($value["Answer"])?>
		
		<br /><span class="update_record"><?=$value["AnswerDate"]?></span>
		</div>
	<?
	}
	//###
		for($i=0;$i<sizeof($teachercomment_ARR);$i++)
		{
			if(!$teachercomment_ARR[$i]["Status"]){
				$new_class = "new";
			}
			else{
				$new_class = "";
			}
	?>
		<div class="IES_comment icon_tea"><?=nl2br($teachercomment_ARR[$i]["Comment"])?><br /><span class="update_record <?=$new_class?>">
		<?="(".$teachercomment_ARR[$i]["DateInput"]." ".$Lang['IES']['CommentBy']." ".$teachercomment_ARR[$i]["CommentTeacher"].")"?></span></div>

	           
	<?}//close for($i=0;$i<sizeof($teachercomment_ARR);$i++)?>

		<?=$snapShotScore?>
	</div> 
	<?   
	} // close foreach($TaskSnapshotAnswer as $value)
	$objIES->updateTeacherCommentStatus($AllTaskSnapshotAnswerIDArray, 1);	
}
else{
	echo "0";
}
//DEBUG_R($AllTaskSnapshotAnsweIDArray);

intranet_closedb();
return;
?>

                
                
            
                
              
                
        