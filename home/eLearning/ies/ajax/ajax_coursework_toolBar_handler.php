<?php
/** [Modification Log] Modifying By: Connie
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once ("$PATH_WRT_ROOT/includes/ies/libies.php");
	intranet_auth();
	intranet_opendb();
	$libdb = new libdb();

	//USER INPUT
	$type = trim($type);
	$noteid = $noteid;
	$noteContent = $noteContent;
	$stageId = $stageId;
    $scheme_id=$scheme_id;
	$htmlReturn = "";

	switch (strtoupper($type)){
		case "STU_FILES":
			$htmlReturn = displayStudentFile();
		break;
		case "BIBI":
			$htmlReturn = displayStudentFile_bibi($scheme_id);
		break;
		case "REFLECT_NOTE":
			$htmlReturn = displayReflectNote2($noteid);
		break;
		case "EDIT_NOTE":
			$htmlReturn = displayReflectNote2($noteid,true);
		break;
		case "UPDATE_NOTE":
			updateReflectNote($noteContent, $noteid, $stageId, $UserID);
			if (mysql_insert_id()) {
				$htmlReturn = displayReflectNote2(mysql_insert_id());
			} else {
				$htmlReturn = displayReflectNote2($noteid);
			}

		break;
		case "WORKSHEET":
			$htmlReturn = displayStudentFile_worksheet($scheme_id);
		break;
		case "FAQ":
			$htmlReturn = displayFAQ($scheme_id);
		break;
		case "TASKANS":
			$htmlReturn = displayTaskAnswer($scheme_id);
		break;

	}
	intranet_closedb();
	echo $htmlReturn;

exit();

function displayStudentFile() {
	global $Lang;
	$encodedModuleCode = base64_encode("ies");
	$studentFileLang = libies::getThisSchemeLang();
	$HTML = <<<HTMLEND
  <a href="#" class="IES_tool_close"  onclick="$(this).parent().hide();" title="{$Lang['IES']['Close']}">&nbsp;</a>
  <!--<a href="#" class="IES_tool_resize">&nbsp;</a>-->
  <h4>{$Lang['IES']['MyFolder']}</h4>
  <!-- overflow-x: hidden; in the iframe style is used to set no horizontal scrollbar -->
	<iframe src="/ipfiles/studentFile.php?moduleCode={$encodedModuleCode}&SF_Lang=$studentFileLang" style="width:100%; height:550px; border:0; overflow-x: hidden;"></iframe>
HTMLEND;
	return $HTML;
}


function displayStudentFile_bibi($Parscheme_id)
{
	global $PATH_WRT_ROOT, $Lang;

	include_once ("$PATH_WRT_ROOT/includes/ies/libies_bibi.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html();
	$loading_image = $linterface->Get_Ajax_Loading_Image();
	$libies_bibi = new libies_bibi();

	$html_list_chi = $libies_bibi->getTypeListDisplay('chi', 'lang_list', '');
	$html_list_en = $libies_bibi->getTypeListDisplay('eng', 'lang_list', '');
	$html_list_chi = str_replace("\n", "", $html_list_chi);
	$html_list_en = str_replace("\n", "", $html_list_en);

	$return = <<<HTML

	<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>

	<script>
	function getlisttable(){
		var type = $('#cur_type').val();
		$("#loading").show();
		$("#record_list").html("");
		 $.ajax({
		    url:      "$PATH_WRT_ROOT/home/eLearning/ies/ajax/ajax_get_bibi_list.php",
		    type:     "POST",
		    data:     "scheme_id={$Parscheme_id}&type="+type,
		    success:  function(data){
		    			$("#loading").hide();
						$("#record_list").html(data);
						tb_init('a.edit_dim');
						//$("#record_list").find("a.edit_dim").attr("class", "thickbox");
		    }

		  });
	}

	function ch_type(obj){
		var type = $(obj).val();

		if(type == ''){
			type = $('#the_lang').val();
			$('#cur_type').val(type);
		}
		else if(type == 'all'){
			$('#cur_type').val('');
		}
		else{
			$('#cur_type').val(type);
		}
		getlisttable();
	}

	function delete_bibi(obj){
		var type = $('#cur_type').val();
		var id = $(obj).attr('del_id');

		if(confirm("{$Lang['IES']['DeleteWarning']}")){
			$("#record_list").html("");
			$("#loading").show();
			$.ajax({
			    url:      "$PATH_WRT_ROOT/home/eLearning/ies/ajax/ajax_get_bibi_list.php",
			    type:     "POST",
			    data:     "scheme_id={$Parscheme_id}&del=1&bibiid="+id+"&type="+type,
			    success:  function(data){
			    		$("#loading").hide();
						$("#record_list").html(data);
			    }

			  });
		}

	}

	function do_search(obj){
		var word = $(obj).val();
		var type = $('#cur_type').val();


		$("#record_list").html("");
		$("#loading").show();
		$.ajax({
		    url:      "$PATH_WRT_ROOT/home/eLearning/ies/ajax/ajax_get_bibi_list.php",
		    type:     "POST",
		    data:     "scheme_id={$Parscheme_id}&type="+type+"&cond="+word,
		    success:  function(data){
		    		$("#loading").hide();
					$("#record_list").html(data);
					tb_init('a.edit_dim');
		    }

		  });

	}

	function disableEnterKey(e)
	{
	     var key;
	     if(window.event)
	          key = window.event.keyCode; //IE
	     else
	          key = e.which; //firefox

	     return (key != 13);
	}

	$(document).ready(function(){
		getlisttable();
		$('#the_lang').change(function(){
			var type = $(this).val();
			if(type == 'chi'){
				$('#list_selection').html("{$html_list_chi}");
				}
				else if(type == 'eng'){
					$('#list_selection').html("{$html_list_en}");
				}
				else{
					$('#list_selection').html('');
				}
			});
//		$('#search_box')

	});
	</script>
	<a href="#" class="IES_tool_close" onclick="$(this).parent().hide()" title="{$Lang['IES']['Close']}">&nbsp;</a>
<!--<a href="#" class="IES_tool_resize">&nbsp;</a>-->
<h4>{$Lang['IES']['BibliographyRecords']}</h4>
<div class="IES_tool_btn">
<a href="addbibi.php?scheme_id={$Parscheme_id}&KeepThis=true&amp;TB_iframe=true&amp;height=480&amp;width=740" class="IES_file_upload thickbox"><span>{$Lang['IES']['newbibi']}</span></a>
</div>
<div class="Conntent_search faq_search"><input type="text" value="{$Lang['IES']['Search']}" id="search_box" onKeyUp="do_search(this)" onFocus="this.select();if(!this._haschanged){this.value=''};this._haschanged=true;" onKeyPress="return disableEnterKey(event)"/></div>

<br class="clear" />

<div style="float:left">
<select id="the_lang" onchange="ch_type(this)" ><option value='all'>-- {$Lang['IES']['bibilang']} --</option><option value='chi'>中文</option><option value='eng'>English</option></select>
<input type='hidden' value='' id='cur_type'/>
&nbsp;&nbsp;

<span id="list_selection" />

</div>
<br class="clear" />


<!-- IES tool floating box start -->
<div class="IES_tool_list" style="height:400px">

<!-- bibi list header start -->
<table class="tool_list_head">
<tr>
<td class="tool_title_col">{$Lang['IES']['bibi']}</td>
<td class="tool_type_col">{$Lang['IES']['type']}</td>
<td class="tool_tags_col">{$Lang['IES']['remark']}</td>
<td class="tool_comment_col sub_row">&nbsp;</td>
</tr>
</table>
<!-- bibi list header end -->
<div id="loading" style="text-align:center">$loading_image</div>
<div id="record_list" />

HTML;
	return $return;
}

//temp function*****************
function GetWorksheetFile($WorksheetID){

	$ldb = new libdb();
	$sql = "SELECT FileName, FolderPath, FileHashName FROM
IES_WORKSHEET_TEACHER_FILE WHERE WorksheetID = ''{$WorksheetID}';";


	return $array = $ldb->returnArray($sql);

}

// return work_sheet
function displayStudentFile_worksheet($scheme_id){
	global $Lang, $UserID, $ies_cfg;
	global $intranet_db, $intranet_root;

//=============*****************
//== Content of the worksheet (tmpl)
//=============********************
	$ws_tmpl_header = <<<HTML
<!-- worksheet list header end -->

<!-- ###### worksheet record start ###### -->
<div class="tool_record_list">
<table class="tool_record_top">
<tr>
<td class="tool_title_col">WS_TITLE</td>
<td class="tool_files_col">WS_UPLOADED</td>
<td class="tool_comment_col">WS_IS_COMMENT</td>
<td class="tool_deadline_col">WS_DEADLINE</td>
</tr>
</table>
HTML;

	$ws_tmpl_content_part1 = <<<HTML
	<!-- list content start -->
	<table class="common_table_list tool_content">
	<col style="width:140px" />
	<col style="width:10px" />
	<tr><td><div class="attachment">{$Lang['IES']['WorksheetFile']}</div></td><td>:</td>
	<td class="tool_last_col">
HTML;

	$ws_tmpl_content_part2 = <<<HTML
	<a href="../getfile.php?fhn=WS_FILEPATH&type={$ies_cfg['HandinFileByTeacher']}">WS_FILENAME</a><br class="clear" />
HTML;

	$ws_tmpl_content_part3 = <<<HTML
	</td></tr>
	<tr><td><div class="my_file">{$Lang['IES']['WsUploadFile']}</div></td>
	<td>:</td>
	<td class="tool_last_col">
HTML;

	//FOR STUDENT HANDIN FILE
	$ws_tmpl_content_part4 = <<<HTML

	<div class='IES_file_list'>
	<br class="clear" />
	<a href="/home/eLearning/ies/getfile.php?fhn=WS_FILEPATH&type={$ies_cfg['HandinFileByStudent']}" style="float:left">HANDIN_FILE_NAME</a><a href="#" id="remove_HANDIN_FILE_ID" class="cancel_btn" title="{$Lang['IES']['Delete']}" onClick='javascript: removeHandInFile(this);'>&nbsp;</a>
	</div>

HTML;

	$ws_tmpl_content_part5 = <<<HTML
	<div class="IES_tool_btn"><a href="handinWorkSheet.php?KeepThis=true&scheme_id=SCHEME_ID&worksheetID=WS_ID&amp;TB_iframe=true&amp;height=480&amp;width=550" target="worksheet_upload" class="IES_file_upload thickbox"><span>{$Lang['IES']['UploadFile']}</span></a></div>
	</td>
	</tr>
	<tr><td><div class="comment_list">{$Lang['IES']['TeacherComment']}</div></td><td>:</td><td class="tool_last_col">WS_COMMENT</td></tr>
	</table>

    <!-- list content end -->
HTML;

	$ws_tmpl_footer = <<<HTML
	</div>
<!-- ###### worksheet record end ###### -->
HTML;
//==================****************
//==  End of HERE tmpl
//===================*****************


	$ldb = new libdb();
	$sql = "SELECT a.Title,DATE_format(a.EndDate, '%Y-%m-%d') as 'EndDate', a.WorksheetID, b.Comment FROM IES_WORKSHEET AS a
LEFT JOIN IES_WORKSHEET_HANDIN_COMMENT AS b ON a.WorksheetID = b.WorksheetID AND b.StudentID = '{$UserID}'
WHERE a.SchemeID = '{$scheme_id}' ;";


	$worksheet_array = $ldb->returnArray($sql);



	//************
	//**  Display
	//**************
	$return = <<<HTML
	<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
	 <link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
	<script language="JavaScript">
	$('table.tool_record_top').toggle(
          function(event) {
			$(this).parent().addClass('selected_list');
          },
          function(event) {
            $(this).parent().removeClass('selected_list');
          }
    );
//    $('.cancel_btn').live('click', function() {
	function removeHandInFile(obj){
//		alert('hi');
				var confirmDelete = confirm("{$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile']}{$Lang['_symbol']["questionMark"]}");
				if (confirmDelete) {
					remove_id = $(obj).attr("id");
					\$target = $(obj).parent();
					\$target.hide();

					file_id = (remove_id.split("_"))[1];
					$.ajax({
						url:      "/home/eLearning/ies/ajax/ajax_remove_files.php",
						type:     "POST",
						data:     "file_id="+file_id+"&userType={$ies_cfg['HandinFileByStudent']}",
						async:	  false,
						error:    function(xhr, ajaxOptions, thrownError){
									alert(xhr.responseText);
								  },
						success:  function(xml){
									  result = $(xml).find("result").text();
									  if (result) {
										\$target.remove();
										/*
										fileAdded--;
										if (fileAdded == 0) {
											$("#studentSubmission").hide();
										}*/
									  } else {
										\$target.show();
										alert("remove failed");
									  }
								  }
					});
				}
	}
//	});
	</script>
	<a href="#" class="IES_tool_close" onclick="$(this).parent().hide()" title="{$Lang['IES']['Close']}">&nbsp;</a>
<a href="#" class="IES_tool_resize">&nbsp;</a>
<h4>{$Lang['IES']['Worksheet']}</h4>
<div class="Conntent_search faq_search"><input type="text" value="{$Lang['IES']['Search']}"/></div>
<br class="clear" />
<!-- IES tool floating box start -->
<div class="IES_tool_list" style="height:400px">

<!-- worksheet list header start -->
<table class="tool_list_head">
<tr>
<td class="tool_title_col">{$Lang['IES']['Worksheet']}</td>
<td class="tool_files_col">{$Lang['IES']['UploadedFiles']}</td>
<td class="tool_comment_col">{$Lang['IES']['Comment']}</td>
<td class="tool_deadline_col">{$Lang['IES']['EndDate']}</td>
</tr>
</table>
HTML;
	$return = str_replace("DELETE_WRANING", $Lang['IES']['DeleteWarning'], $return);

	foreach($worksheet_array as $value){
		//Get WORKSHEET_TEACHER_FILE
		$upload_ws_array = GetWorksheetFile($value["WorksheetID"]);
		$comment_logo = '';
		$sql = "SELECT WorksheetFileID,FileName, FileHashName FROM IES_WORKSHEET_HANDIN_FILE WHERE WorksheetID = '{$value['WorksheetID']}' AND StudentID = '{$UserID}'";

		$handin_array = $ldb->returnArray($sql);

		if(strlen($value["Comment"]) > 0){
			$comment_logo = '<div class="comment_list" title="'.$Lang['IES']['TeacherComment'].'">&nbsp;</div>';
		}

		//To sub the NAME of TMPL====
		$order   = array("WS_TITLE", "WS_FILENAME","WS_UPLOADED" ,"WS_COMMENT", "WS_DEADLINE", "WS_IS_COMMENT", "SCHEME_ID", "WS_ID");
		$replace = array($value["Title"], $value["FileName"], sizeof($upload_ws_array) ,$value["Comment"], $value["EndDate"], $comment_logo, $scheme_id, $value["WorksheetID"]);
		//====


		$return .= str_replace($order, $replace, $ws_tmpl_header);
		$return .= $ws_tmpl_content_part1;
		if(sizeof($upload_ws_array) > 0){
			foreach($upload_ws_array as $w){
				$or = array("WS_FILEPATH", "WS_FILENAME");
				$rep = array($w["FileHashName"], $w["FileName"]);
				$return .= str_replace($or,$rep,$ws_tmpl_content_part2);
			}
			unset($w);
		}

		$return .= $ws_tmpl_content_part3;
		for($k = 0; $k < sizeof($handin_array); $k++){
			$h_order = array("HANDIN_FILE_NAME", "WS_FILEPATH","HANDIN_FILE_ID");
			$h_replace = array($handin_array[$k]["FileName"], $handin_array[$k]["FileHashName"],$handin_array[$k]['WorksheetFileID']);
			$return .= str_replace($h_order, $h_replace, $ws_tmpl_content_part4);

		}

		$return .= str_replace($order,$replace,$ws_tmpl_content_part5);
		$return .= $ws_tmpl_footer;
	}
	unset($value);
	$return .= "</div>";
	return $return;
}

function updateReflectNote($ParContent="", $ParNoteId=0, $ParStageId=0, $ParUserId=0) {

	global $libdb, $intranet_db;
	if ($ParNoteId>0) {
	$sql = <<<SQLEND
	UPDATE $intranet_db.IES_REFLECT_NOTE SET CONTENT = '{$ParContent}' WHERE NOTEID = '$ParNoteId'
SQLEND;
	} else {

	$schemeStudentId = retrieveSchemeStudentIdByStageIdAndUserId($ParUserId,$ParStageId);
	$sql = <<<SQLEND
	INSERT INTO $intranet_db.IES_REFLECT_NOTE SET CONTENT = '{$ParContent}', SCHEMESTUDENTID = '{$schemeStudentId}'
SQLEND;
	}

	$q_result = $libdb->db_db_query($sql);
	return $q_result;
}
//function displayReflectNote()
//{
//	$var = <<<HTML
//<div class = "reflection_note">
//  <div class="board_top"><div></div></div>
//  <div class="board_left"><div class="board_right">
//    <div class="board_content">
//      <div class="inner">
//        <a href="javascript:;" name ="reflect_note" class="reflect_note toolBar IES_tool_close" title="{$Lang['IES']['Close']}">&nbsp;</a>
//        <h4>反思手記</h4>
//        在思考探究題目時，我需要複習以前所學的通識單元內容及相關概念，這深化了我對學習內容的理解，啓發我聯繫不同單元的知識……
//        <div class="edit_bottom"> <span> 最後修改日期 : 08-10-2010</span> <!-- added 18-05-2010 by ELI -->
//          <div style="float:right; padding-top:5px">          
//            <a href="IES_student_task1_step4(reflect_edit).html" class="edit_pen">修改</a>
//        </div></div>
//      </div> <!-- inner end -->
//    </div> <!-- board_content end -->
//  </div></div>
//  <div class="board_bottom"><div></div></div>
//</div>
//HTML;
//
//	return $var;
//}
function getReflectNoteElement($ParNoteId=-1) {
	global $libdb;
	$sql = <<<SQLEND
	SELECT NOTEID,SCHEMESTUDENTID,CONTENT,DATEMODIFIED FROM IES_REFLECT_NOTE
	WHERE NOTEID = $ParNoteId
SQLEND;
	$returnArray = array();
	$returnArray = $libdb->returnArray($sql);

	return $returnArray;
}
/**
 * Retrieve schemeStudentId by the stageid and the userid
 * @param	int	$ParUserId - the user id
 * @param	int	$ParStageId - the stage id
 * @return	A schemeStudentId that belong to a student in a scheme
 */
function retrieveSchemeStudentIdByStageIdAndUserId($ParUserId,$ParStageId) {
	global $intranet_db;
	$ldb = new libdb();
	$sql = "SELECT ISS.SCHEMESTUDENTID
			FROM IES_SCHEME_STUDENT ISS
				INNER JOIN IES_STAGE ISTA
				ON ISTA.SCHEMEID = ISS.SCHEMEID
			WHERE ISTA.STAGEID = '$ParStageId'
				AND ISS.USERID = '$ParUserId'
			";

	$returnArray = $ldb->returnArray($sql);

	$schemeStudentId = $returnArray[0]["SCHEMESTUDENTID"];

	return $schemeStudentId;
}
function displayReflectNote2($ParNoteId="", $ParIsEdit=false)
{
	global $Lang;
	# Retrieve Notes Content and Modified Date
	if (!empty($ParNoteId)) {
		$noteElement = getReflectNoteElement($ParNoteId);
	}
	$sizeOfNoteElement = count($noteElement);
	if ($sizeOfNoteElement>0) {
		$noteId = $noteElement[0]["NOTEID"];
		$noteContent = htmlspecialchars($noteElement[0]["CONTENT"]);
		$noteDateModified = $noteElement[0]["DATEMODIFIED"];
	} else {
		$noteId = "";
		if ($ParIsEdit) {$noteContent = "";} else {$noteContent = $Lang['IES']['TemporarilyNoContent'];}
		$noteDateModified = "----";
	}
	if ($ParIsEdit) {
		$noteContent = "<textarea class=\"input_reflection\" id=\"note_content\">$noteContent</textarea>";
	} else {
		$noteContent = nl2br($noteContent);
	}

	# Handle the Actions can be done when in EDIT mode and VIEW mode
	if ($ParIsEdit) {
		$noteSaveCancelField = <<<HTML
		<div class="IES_tools_bottom_btn">&nbsp;
		<input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="doUpdateNote()" value=" {$Lang['IES']['Save']} " />
        <input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="doCancelEditNote()" value=" {$Lang['IES']['Cancel']} " />
        </div>
HTML;
	} else {
		$noteEditLinkField = <<<HTML
		<div>
            <a href="javascript: void(0);" onClick="doEditNote();" name="edit_note" class="edit_pen">{$Lang['IES']['Modify']}</a>
        </div>
HTML;
	}
	if(!empty($noteId)){
		$changenoteId = "<script>$(\"#note_id\").attr('value', $noteId);</script>";
	}
	else{
		$changenoteId = "";
	}
	# Contruct the HTML to send back
	$var = <<<HTML
$changenoteId
<div class = "reflection_note">
  <div class="board_top"><div></div></div>
  <div class="board_left"><div class="board_right">
    <div class="board_content">
      <div class="inner">
        <a href="javascript:;" name ="reflect_note" class="reflect_note toolBar IES_tool_close" title="{$Lang['IES']['Close']}">&nbsp;</a>
        <h4>{$Lang['IES']['ReflectNote']}</h4>
        {$noteContent}
        <input type="hidden" id="noteId" value="{$noteId}" />
        <div class="edit_bottom"> <span> {$Lang['IES']['LastModifiedDate']} : {$noteDateModified}</span> <!-- added 18-05-2010 by ELI -->
        $noteEditLinkField
          </div>
          $noteSaveCancelField
      </div> <!-- inner end -->
    </div> <!-- board_content end -->
  </div></div>
  <div class="board_bottom"><div></div></div>
</div>
HTML;

	return $var;
}

function displayFAQ($scheme_id)
{
	global $libdb, $ies_cfg, $Lang;
	$ldb = new libdb();

	$sql = "SELECT faq.QuestionID, Question, Answer, AnswerDate
FROM IES_FAQ faq
LEFT OUTER JOIN IES_FAQ_RELATIONSHIP rel
	ON (rel.QuestionID=faq.QuestionID)
WHERE
	RecordStatus=".$ies_cfg["faq_recordStatus_approved"]."
	AND (faq.AssignType=".$ies_cfg["faq_assignType_to_all"]." OR rel.SchemeID='{$scheme_id}')
	AND (Answer IS NOT NULL OR Answer!='')
ORDER BY DateInput";
	//echo $sql;
	$result = $ldb->returnArray($sql,3);

	$libies = new libies();
	for($i=0; $i<sizeof($result); $i++) {
		$FAQAttachmentDetails = $libies->getFAQAttachmentDetails($result[$i]['QuestionID']);
		$faqContent .= "<li><a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">{$result[$i][1]}</a><div id='spanQuestion{$result[$i][0]}' style='display:none;'>".nl2br($result[$i][2])." <a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">(".$Lang['IES']['Hide'].")</a></div><input type='hidden' name='field{$result[$i][0]}' id='field{$result[$i][0]}' value='0'>";
		$downloadFiles = array();
		if (is_array($FAQAttachmentDetails)) {
			foreach($FAQAttachmentDetails as $key => $element) {
				$downloadFiles[] = "<a href='/home/eLearning/ies/download.php?task=faq&fileHashName={$element["FILEHASHNAME"]}'>{$element["FILENAME"]}</a></td><td class='table_row_tool'><a title='Delete' class='delete_dim' href='#' onClick='removeFile(this,\"{$element["FILEHASHNAME"]}\")' />";
			}
		}
		if (count($downloadFiles)>0) {
			$faqContent .= "<br/>(".implode(", ",$downloadFiles).")";
		}
		$faqContent .= "</li>";
	}


	$var = <<<HTML
<div class = "IES_faq">
  <div class="board_top"><div></div></div>
  <div class="board_left"><div class="board_right">
    <div class="board_content">
      <div class="inner">
        <a href="javascript:;" class="IES_tool_close" title="{$Lang['IES']['Close']}">&nbsp;</a>
        <h4><a href="javascript:;" class="faq_tab current"><span>{$Lang['IES']['FAQ']}</span></a><!--<a href="IES_student_task1_step4(FAQ_2).html" class="faq_tab"><span>{$Lang['IES']['IWouldLikeToAsk']}</span></a>--></h4>

        <div class="Conntent_search faq_search">
          <!--<input type="text" name="textStr" id="textStr" value="{$Lang['IES']['Search']}"/>-->
        <input type="text" name="searchScheme" id="searchScheme"  value="{$Lang['IES']['Search']}" onKeyUp="show_search('user','showSpanLayer')" onFocus="this.select();if(!this._haschanged){this.value=''};this._haschanged=true;" onKeyPress="return disableEnterKey(event)"/><span id="showSpanLayer"></span>
        </div>
        <br class="clear" />
		<div id="searchSpan">
        <ul class="IES_faq_list">
          {$faqContent}
        </ul>
		</div>
      </div> <!-- inner end -->
    </div> <!-- board_content end -->

  </div></div>
  <div class="board_bottom"><div></div></div>
</div>
<input type='hidden' name='ClickID' id='ClickID' value=''>
<input type='hidden' name='task' id='task' value=''>
<script language='javascript'>
function disableEnterKey(e)
{
     var key;
     if(window.event)
          key = window.event.keyCode; //IE
     else
          key = e.which; //firefox

     return (key != 13);
}
function spanDisplay(spanName, field) {
	if(document.getElementById(field).value==0) {
		document.getElementById(spanName).style.display = 'block';
		document.getElementById(field).value = 1;
	} else {
		document.getElementById(spanName).style.display = 'none';
		document.getElementById(field).value = 0;
	}
}
var ClickID = '';
var callback_show_search = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('searchSpan').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_search(type,click)
{
	var searchScheme = $("#searchScheme").val();
	ClickID = click;

	$.ajax({
		url:      "/home/eLearning/ies/ajax/ajax_admin_display_search.php",
		type:     "POST",
		data:     "task="+type+"&searchScheme="+searchScheme+"&scheme_id={$scheme_id}",
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function( o ){
			    var tmp_str = o;
			    document.getElementById('searchSpan').innerHTML = tmp_str;
			    DisplayPosition();
			}
	});
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = 'obj';
	//alert(typeof(eval(objStr)));
	var pos_value = 0;
	while (typeof(eval(objStr))!='undefined' && (eval(objStr + '.tagName')!='BODY' && eval(objStr + '.tagName')!='HTML'))
	{
		pos_value += eval(objStr + '.' + direction);
		objStr += '.offsetParent';
	}
	return pos_value;
}

function DisplayPosition(){
	document.getElementById('searchSpan').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') + 10;
	  document.getElementById('searchSpan').style.top = getPosition(document.getElementById(ClickID),'offsetTop');
	  document.getElementById('searchSpan').style.visibility = 'visible';
}

</script>

HTML;
		return $var;
}

function displayTaskAnswer($scheme_id)
{
	global $ies_cfg, $Lang;
	$objIES = new libies();

	$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);

	$html_task_selection = "<select id=\"taskSel\" onChange=\"change_task()\" style=\"width:250px\">";
	$html_task_selection .= "<option value=\"\">-- {$Lang['IES']['Select']} --</option>";
	for($i=0, $i_max=count($stage_arr); $i<$i_max; $i++)
	{
		$_stage_id = $stage_arr[$i]["StageID"];
		$_stage_title = $stage_arr[$i]["Title"];

		$html_task_selection .= "<optgroup label=\"{$_stage_title}\">";

		$task_arr = $objIES->GET_TASK_ARR($_stage_id);
		for($j=0, $j_max=count($task_arr); $j<$j_max; $j++)
		{
			$_task_id = $task_arr[$j]["TaskID"];
			$_task_title = $task_arr[$j]["Title"];

			$html_task_selection .= "<option value=\"{$_task_id}\">{$_task_title}</option>";
		}

		$html_task_selection .= "</optgroup>";
	}
	$html_task_selection .= "</select>";

	$var = <<<HTML
<div class = "IES_faq">
	<div class="board_top"><div></div></div>
	<div class="board_left"><div class="board_right">
		<div class="board_content">
      <div class="inner">
      	<a href="javascript:;" class="IES_tool_close" title="{$Lang['IES']['Close']}">&nbsp;</a>
      	<h4 style="background:url('');">{$Lang['IES']['TaskAnswer']}</h4>
      	<br class="clear" />

      	{$html_task_selection}

      	<div id="taskAnsDiv">

      	</div>
      </div> <!-- inner end -->
    </div> <!-- board_content end -->
	</div></div>
  <div class="board_bottom"><div></div></div>
</div>

<script language='javascript'>
function change_task()
{
	var taskID = $("#taskSel").val();

	$.ajax({
		url:      "/home/eLearning/ies/ajax/ajax_load_task_answerbox.php",
		type:     "POST",
		data:     "taskID="+taskID+"&scheme_id={$scheme_id}",
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
								alert(xhr.responseText);
							},
		success:  function( msg ){
								$("#taskAnsDiv").html(msg);
							}
	});
}

function copy_answer()
{
	$.clipboard($("#taskAnsHidden").val());
}



</script>

HTML;

/*
	$sql = "SELECT faq.QuestionID, Question, Answer, AnswerDate FROM IES_FAQ faq LEFT OUTER JOIN IES_FAQ_RELATIONSHIP rel ON (rel.QuestionID=faq.QuestionID) WHERE RecordStatus=".$ies_cfg["faq_recordStatus_approved"]." AND (faq.AssignType=".$ies_cfg["faq_assignType_to_all"]." OR rel.SchemeID={$scheme_id}) AND (Answer IS NOT NULL OR Answer!='') ORDER BY DateInput";
	//echo $sql;
	$result = $ldb->returnArray($sql,3);

	for($i=0; $i<sizeof($result); $i++) {
		$faqContent .= "<li><a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">{$result[$i][1]}</a><div id='spanQuestion{$result[$i][0]}' style='display:none;'>".nl2br($result[$i][2])." <a href='javascript:;' onClick=\"spanDisplay('spanQuestion{$result[$i][0]}','field{$result[$i][0]}')\">(".$Lang['IES']['Hide'].")</a></div><input type='hidden' name='field{$result[$i][0]}' id='field{$result[$i][0]}' value='0'></li>";
	}


	$var = <<<HTML
<div class = "IES_faq">
  <div class="board_top"><div></div></div>
  <div class="board_left"><div class="board_right">
    <div class="board_content">
      <div class="inner">
        <a href="javascript:;" class="IES_tool_close" title="{$Lang['IES']['Close']}">&nbsp;</a>
        <h4><a href="javascript:;" class="faq_tab current"><span>{$Lang['IES']['FAQ']}</span></a><!--<a href="IES_student_task1_step4(FAQ_2).html" class="faq_tab"><span>{$Lang['IES']['IWouldLikeToAsk']}</span></a>--></h4>

        <div class="Conntent_search faq_search">
          <!--<input type="text" name="textStr" id="textStr" value="{$Lang['IES']['Search']}"/>-->
        <input type="text" name="searchScheme" id="searchScheme"  value="{$Lang['IES']['Search']}" onKeyUp="show_search('user','showSpanLayer')" onFocus="this.select();if(!this._haschanged){this.value=''};this._haschanged=true;" onKeyPress="return disableEnterKey(event)"/><span id="showSpanLayer"></span>
        </div>
        <br class="clear" />
		<div id="searchSpan">
        <ul class="IES_faq_list">
          {$faqContent}
        </ul>
		</div>
      </div> <!-- inner end -->
    </div> <!-- board_content end -->

  </div></div>
  <div class="board_bottom"><div></div></div>
</div>
<input type='hidden' name='ClickID' id='ClickID' value=''>
<input type='hidden' name='task' id='task' value=''>
<script language='javascript'>
function disableEnterKey(e)
{
     var key;
     if(window.event)
          key = window.event.keyCode; //IE
     else
          key = e.which; //firefox

     return (key != 13);
}
function spanDisplay(spanName, field) {
	if(document.getElementById(field).value==0) {
		document.getElementById(spanName).style.display = 'block';
		document.getElementById(field).value = 1;
	} else {
		document.getElementById(spanName).style.display = 'none';
		document.getElementById(field).value = 0;
	}
}
var ClickID = '';
var callback_show_search = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('searchSpan').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_search(type,click)
{
	var searchScheme = $("#searchScheme").val();
	ClickID = click;

	$.ajax({
		url:      "/home/eLearning/ies/ajax/ajax_admin_display_search.php",
		type:     "POST",
		data:     "task="+type+"&searchScheme="+searchScheme+"&scheme_id={$scheme_id}",
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function( o ){
			    var tmp_str = o;
			    document.getElementById('searchSpan').innerHTML = tmp_str;
			    DisplayPosition();
			}
	});
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = 'obj';
	//alert(typeof(eval(objStr)));
	var pos_value = 0;
	while (typeof(eval(objStr))!='undefined' && (eval(objStr + '.tagName')!='BODY' && eval(objStr + '.tagName')!='HTML'))
	{
		pos_value += eval(objStr + '.' + direction);
		objStr += '.offsetParent';
	}
	return pos_value;
}

function DisplayPosition(){
	document.getElementById('searchSpan').style.left = getPosition(document.getElementById(ClickID),'offsetLeft') + 10;
	  document.getElementById('searchSpan').style.top = getPosition(document.getElementById(ClickID),'offsetTop');
	  document.getElementById('searchSpan').style.visibility = 'visible';
}

</script>

HTML;
*/


		return $var;
}
?>