<?php
/** [Modification Log] Modifying By:
 * *******************************************
 * 
 * 2011-01-13	Thomas	201101131046
 * 	-	In 'Step 2', check if in stage 3, display link for Insert Figure Thickbox
 * 
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_static.php");


$loadType = $loadType;
$stage_id = $stage_id;
$UserID = $UserID;





$objIES  = new libies();
intranet_opendb();
$linterface = new interface_html();
$stage_arr = $objIES->GET_STAGE_DETAIL($stage_id);

# Step 1
$html_SubmitReport .= "<span style=' font-size: 18px;'><b>{$Lang['IES']['Step']}1: {$Lang['IES']['Export']}</b></span><br/>

						<span class=\"IES_top_tool\" style=\"width:100%; text-align:left;\"><br/>
							<span style='float: left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
							<a href=\"./exportdoc.php?ParUserId={$UserID}&ParStageId={$stage_id}\" class=\"export\" title=\"\">{$Lang['IES']['Export']}</a><br /><br/>
						</span>
						";
# Step 2 (Modified by Thomas on 2011-01-13)
if($stage_arr["Sequence"]==3){
	$html_SubmitReport .= "<span style=' font-size: 18px;'><b>".$Lang['IES']['Step']."2: ".$Lang['IES']['TidyUp'].(libies::getThisSchemeLang()=='b5'?"、":"").$Lang['IES']['Edit']."</b></span><br/>
						   <span style=\"width:100%; text-align:left;\"><br/>
								<script type=\"text/javascript\" src=\"/templates/jquery/thickbox.js\"></script>
								<link rel=\"stylesheet\" href=\"/templates/jquery/thickbox.css\" type=\"text/css\" media=\"screen\" />
								<span style='float: left'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
								<a href=\"tips/stage3_lightbox_F.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true\" class=\"thickbox task_demo\">".$Lang['IES']['AddFigureThickboxLink']."</a><br /><br/>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['IES']['TidyUpDetail']."<br/><br/>
						   </span>";
}
else{
	$html_SubmitReport .= "<span style=' font-size: 18px;'><b>".$Lang['IES']['Step']."2: ".$Lang['IES']['TidyUp'].(libies::getThisSchemeLang()=='b5'?"、":"").$Lang['IES']['Edit']."</b><br/><br/></span>
						   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['IES']['TidyUpDetail']."<br/><br/>";
}

# Step 3
$html_SubmitReport .= "<span style=' font-size: 18px;'><b>".$Lang['IES']['Step']."3: ".$Lang['IES']['SubmitReport']."</b><br /><br /></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Lang['IES']['SubmitReportDetail'];

switch (strtoupper($loadType)) {
/** Start Current Part **/
	case "CURRENT":

		# get the current file list
		$current_batch_id = $objIES->getCurrentBatchId($stage_id, $UserID);
		$is_batch_record_exist = $objIES->isExistBatchRecord($stage_id,$UserID);
		
		# get the last records information
		$lastBatchInfo = $objIES->getLastSubmittedBatchId_and_BatchStatus($stage_id, $UserID);
		if ($lastBatchInfo["BATCHSTATUS"] == $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]
			|| $lastBatchInfo["BATCHSTATUS"] == $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["approved"]) {
				
			$last_submitted_batch_id = $lastBatchInfo["BATCHID"];
			if ($lastBatchInfo["BATCHSTATUS"] == $ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitted"]) {
				$show_batch_status = $Lang['IES']['Submitted'];
			} else {
				$show_batch_status = $objIES->getBatchStatusLangByConfigNum($lastBatchInfo["BATCHSTATUS"]);
			}
		}
	
		##############################################
		# Constructing the file detail array in js
		$target_batch_id = ($current_batch_id?$current_batch_id:$last_submitted_batch_id);
	
	
	
	
	
		if (!$target_batch_id) { // Handle the case that when initially no records inside DB table
			// do nothing
	
		} else {
			$comment_info = $objIES->getBatchCommentRequiredElement($target_batch_id);
			$file_info = $objIES->getStageHandinFileInfo_byBatchId_orderByDateModifiedDesc($target_batch_id);
		}
	
		if ($file_info) {
			$jsFileArray = $objIES->ConvertToJSArrayNew($file_info, "jsFileArrayFromPHP");
		}
		##############################################
		
		# construct html
		$isSubmitting = $current_batch_id;
		$isRedo = $objIES->isHandinRedo($stage_id,$UserID);
		
		if ($isRedo) {
			$html_redo_message = <<<HTMLEND
			    <div id="message">
		        	{$Lang['IES']['PleaseRedo']}
		        </div>
HTMLEND;
		}
		
		$upload_box_on = false;
		$js_allow_remove_on = false;
		$loadingImg = $linterface->Get_Ajax_Loading_Image();
		if($isRedo || $isSubmitting || !$is_batch_record_exist)
		{
			$upload_box_on = true;
			$js_allow_remove_on = true;
		}

		if ($upload_box_on) {
			$html_submission_field = <<<HTMLEND
			<input id="studentSubmission" style="display:none" name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="" value=" {$Lang['IES']['Submit']} " />
HTMLEND;
			# the target is stated below
			$html_upload_box = <<<HTMLEND
		        <div class="submission_upload" id="submission_upload">
		        	{$Lang['IES']['UploadFiles']}{$Lang['_symbol']["colon"]}&nbsp;&nbsp;&nbsp;&nbsp;
			        <input type="file" name="submitFile"/><br/>
			        <div class="submission_manage_btn">
		        	<input name="submit2" type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" {$Lang['IES']['Save']} " />
					<input type="reset" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" {$Lang['IES']['Reset']}" />
			        </div>
		        </div>
		        <p id="f1_upload_process" style="display:none">{$Lang['IES']['Loading']}{$Lang['_symbol']["dot"]}{$Lang['_symbol']["dot"]}{$Lang['_symbol']["dot"]}<br/>{$loadingImg}<br/></p>
HTMLEND;
	
	
		} else {
			// show the score only when the upload_box is off
			// get student score for this stage
			$studentStageDetails = $objIES->get_student_stage_details($stage_id, $UserID);		
			$studentStageScore = $studentStageDetails["Score"];
	
			$html_studentStageScore = "";
			if(trim($studentStageScore)!= "") 
			{
				$html_studentStageScore = $Lang['IES']['TeacherMark'].$Lang['_symbol']["colon"].$studentStageScore." / ".$stage_arr["MaxScore"]."<br/>";
			}


			$html_submission_field = <<<HTMLEND
			{$show_batch_status}
HTMLEND;
			$html_upload_box = <<<HTMLEND
		        <div id="submission_upload" id="submission_upload">
		        </div>
HTMLEND;
	}
	
		$html_comment_list = $objIES->generateHandinTaskCommentPartHtml($comment_info);
		################################################
		##	Start: Main variables for html
		################################################
		$html_tbody = <<<HTMLEND
			<tr class="IES_submission_list">
			<td>
			{$html_redo_message}
			{$html_upload_box}
	        </td>
	        <td>{$html_comment_list}
			</td>
	        <td> {$html_submission_field} <br/>{$html_studentStageScore}</td> 
	        </tr>
HTMLEND;
	


	$onClickLoadCurrent=<<<HTMLEND
onClick=""
HTMLEND;
	$onClickLoadPast=<<<HTMLEND
onClick="javascript: loadHandinBlock('past');"
HTMLEND;
	$current_class_on = " class=\"share_tab_on\" ";
	$old_class_on = "";
	################################################
	##	End: Main variables for html
	################################################

	
	
		break;
/** End Current Part **/
/** Start Past Part **/
	case "PAST":
	
	$batch_info = $objIES->getStageHandinBatchInfo_notSubmitting_orderByDateModifiedDesc($stage_id, $UserID);
	
	$rowNumber = 0;
	foreach($batch_info as $bKey => $bElement) {
/*~~~~~~~~~~~~~~~~~~~~Start: Rubrics Table~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		############################################
		# Get Current Mark          
        $_assignment_mark = libies_static::getCurrentMark($bElement["BATCHID"], $ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"], MARKING_CRITERIA_OVERALL);
        ############################################
        
        
		############################################
		# Prepare data for rubics table (Markers, Last Modified Date)
        $lastMarkerArr = libies_static::getLastMarker($bElement["BATCHID"], $ies_cfg["DB_IES_MARKING_AssignmentType"]["stage"]);
        $markerName = $lastMarkerArr["MarkerName"];
        $lastModifiedDate = $lastMarkerArr["LastModified"];
        $html_latest_modified = empty($lastMarkerArr) ? "" : "<br /><span class=\"update_record\" style=\"float:left\">({$lastModifiedDate} {$markerName})</span>";
        ############################################
        
        
        ############################################
        # Construct the rubrics table
        if ($bElement["BATCHID"]!=$ies_cfg["DB_IES_STAGE_HANDIN_BATCH_BatchStatus"]["submitting"]){
        	if (isset($_assignment_mark)) {
          		$returnStr = $Lang['IES']['TeacherMark'].$Lang['_symbol']["colon"]." $_assignment_mark / ".$stage_arr["MaxScore"];
        	} else {
        		$returnStr = "--";
        	}
        }
        else
        {
          $returnStr .= "<span class=\"update_record\">{$Lang['IES']['SubmittingNoMarking']}</span>";
        }
        unset($html_mark_criteria);
        ############################################
/*~~~~~~~~~~~~~~~~~~~~End: Rubrics Table~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
		
		$file_info = $objIES->getStageHandinFileInfo_byBatchId_orderByDateModifiedDesc($bElement["BATCHID"]);
		$comment_info = $objIES->getBatchCommentRequiredElement($bElement["BATCHID"]);
		$html_file_list = "";
		$html_comment_list = "";
		foreach($file_info as $fKey => $fElement) {
		
			$_fileHashName = $fElement["FILEHASHNAME"];
		$html_file_list .= <<<HTMLEND
		<li><a href="/home/eLearning/ies/download.php?fhn={$_fileHashName}">{$fElement["FILENAME"]}</a><br/><span class="update_record">({$fElement["DATEMODIFIED"]})</span></li>
HTMLEND;
		}
		$html_comment_list = $objIES->generateHandinTaskCommentPartHtml($comment_info);


		# Display the BATCHSTATUS in human readable wordings
		$batch_status = intval($bElement["BATCHSTATUS"]);
		$html_batch_status = $objIES->getBatchStatusLangByConfigNum($batch_status);	

	################################################
	##	Start: Main variables for html
	################################################
		++$rowNumber;
		$html_tbody .= <<<HTMLEND
			<tr class="IES_submission_list">
				<td>{$rowNumber}</td>
                <td>
		        <ul class="attachment_list">
		        {$html_file_list}
		        </ul>
		        </td>
		        <td>{$html_comment_list}
				</td>
		        <td> {$html_batch_status}<br/>{$returnStr} </td>        
	        </tr>
HTMLEND;
		
	}
	
	# when no record, display
	$html_tbody = (empty($html_tbody)?"<tr class=\"IES_submission_list\"><td colspan='4' style='text-align: center'>{$Lang['IES']['NoRecordAtThisMoment']}</td></tr>":$html_tbody);
	
	$html_table_number_th = "<th>#</th>";
	$current_class_link = "handInTask.php?stage_id={$stage_id}&scheme_id={$scheme_id}&is_current_record=1";
	$onClickLoadCurrent=<<<HTMLEND
onClick="javascript: loadHandinBlock('current');"
HTMLEND;
	$onClickLoadPast=<<<HTMLEND
onClick=""
HTMLEND;
	$old_class_on = " class=\"share_tab_on\" ";
	$old_col_width = "<col />";
	################################################
	##	End: Main variables for html
	################################################
	
	
		break;
/** End Past Part **/
}
$html_tag = <<<HTMLEND
		<a href="javascript: void(0);" $current_class_on $onClickLoadCurrent><span>&nbsp;{$Lang['IES']['CurrentRecords']}&nbsp;</span></a>
        <a href="javascript: void(0);" $old_class_on $onClickLoadPast><span>&nbsp;{$Lang['IES']['PastRecords']}&nbsp;</span></a><br style="clear:both" />
HTMLEND;


$final_HTML = <<<HTMLEND
<div class="submit_left">
  <div class="submit_right">
    <div class="submit_inner">
      {$html_SubmitReport}
      <br style="clear:both" />
      <br style="clear:both" />
      
      <!-- tab start -->
      <div class="IES_white_tab">
        {$html_tag}
      </div>
      <br style="clear:both" />
      <!-- tab end -->

      <form id="uploadForm" action="upload.php" method="post" enctype="multipart/form-data" target="upload_target" onsubmit="return startUpload();" >
        <table class="common_table_list IES_submission_list">
          {$old_col_width}
          <col style="width:35%" />
          <col style="width:42%" />
          <col style="" />
          <thead>
            <tr class="IES_submission_list">
              {$html_table_number_th}
              <th>{$Lang['IES']['File']}</th>
              <th>{$Lang['IES']['TeacherComment']}</th>
              <th>{$Lang['IES']['Status']}</th>
            </tr>
          </thead>
  
          <tbody>
            {$html_tbody}
          </tbody>
        </table>

        <input type="hidden" name="student_id" value="{$UserID}"/>
        <input type="hidden" name="stage_id" value="{$stage_id}"/>
        <input type="hidden" name="scheme_id" value="{$scheme_id}"/>
      </form>
      <iframe id="upload_target" name="upload_target" src="#" style="width:0px;height:0px;border:0px solid #fff;" noresize="noresize" frameborder="0" cellspacing="0" scrolling="no" marginwidth="0" marginheight="0"></iframe>
    </div>
  </div>
</div>
HTMLEND;

intranet_closedb();
//# Output the modified date to the page
header("Content-Type:   text/xml");
$libies = new libies();
$XML = $libies->generateXML(
					array(
						array("result", $final_HTML),
						array("jsFileArray", $jsFileArray),
						array("js_allow_remove_on",($js_allow_remove_on?1:0))
					), true, false
				);
echo $XML;


?>
        