<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$objIES = new libies();
// Get task info
$task_arr = $objIES->GET_TASK_BLOCK_INFO($task_id, $UserID);


echo libies_ui::GET_TASK_BLOCK_CONTENT($task_arr);

?>