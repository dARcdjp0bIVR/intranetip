<?php
// using 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$objIES  = new libies();
intranet_opendb();

$YearClassID = $_POST['YearClassID'];

$SelectedStudentList = (is_array($_POST['StudentSelected']))? $_POST['StudentSelected'] : array();

$student_arr = $objIES->GET_IES_STUDENT_ARR($YearClassID, $SelectedStudentList);

$json = new JSON_obj();
$json_str = $json->encode($student_arr);

echo $json_str;

intranet_closedb();
?>