<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$objIES = new libies();

if($objIES->IS_ADMIN_USER($UserID) || $objIES->isIES_Teacher($UserID))
{
  $html_disable_submit = "DISABLED";
}
else
{
  $sql = "SELECT Answer FROM {$intranet_db}.IES_TASK_HANDIN ";
  $sql .= "WHERE TaskID = {$task_id} AND UserID = {$UserID}";
  $answer = current($li->returnVector($sql));
  
  $html_disable_submit = "";
}

?>
<form id="task_<?=$task_id?>">
<div class="writing_area_min task_wrap">
<!--textarea name="" class="stage_index_short"></textarea-->
<textarea name="task_content" rows="5" ><?=$answer?></textarea>
</div>                         
<div class="task_writing_bottom task_wrap"><div>
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="javascript:doSaveTask(<?=$task_id?>)" value=" <?=$Lang['IES']['Save']?> " <?=$html_disable_submit?> />
<input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="javascript:doLoadTaskBlock(<?=$task_id?>)" value=" <?=$Lang['IES']['Cancel']?> " />
</div></div>

<input type="hidden" name="task_id" value="<?=$task_id?>" />
</form>