<?php
//EDITING BY: 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexportdoc.php");
intranet_auth();
intranet_opendb();


$SurveyID = $_REQUEST['SurveyID'];
if(empty($SurveyID)){
	//echo "NO SurveyID";
	exit();
}
$libies_survey = new libies_survey();
$libDOC = new libexportdoc();

##############################################
# get the Survey Details
$SurveyDetails = $libies_survey->getSurveyDetails($SurveyID);
$SurveyDetails = current($SurveyDetails);
$SurveyStartDate = $SurveyDetails["DateStart"];
$SurveyEndDate = $SurveyDetails["DateEnd"];
$Survey_title = $SurveyDetails["Title"];
$Survey_desc = $SurveyDetails["Description"];
$Survey_type = $SurveyDetails["SurveyType"];

# get the Survey Mapping Details
$SurveyMappingAry = $libies_survey->loadSurveyMapping($SurveyID,$UserID);

$content .= "<h2>".$Lang['IES']['QuestionnaireTitle'].":".$Survey_title."</h2>";
$content .= "<h2>".$Lang['IES']['QuestionnaireDescription'].":".$Survey_desc."</h2>";

$content .= "<table class=\"common_table_list\">";


	# Generate Table Title
	$content .= "	<tr>";
	$content .= "		<th style=\"border-bottom\">#</th>";
	$content .= "		<th>".($Survey_type==$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]? $Lang['IES']['CombinationName'] : $Lang['IES']['DiscoveryName'])."</th>";
	
	if($IsStage3)
	{
		$content .= "		<th>".($Survey_type==$ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]? $Lang['IES']['CombinationAnalysis'] : $Lang['IES']['DiscoveryDetails'])."</th>";
	}
	
	$content .= "	</tr>";
	
	# Generate Table Content
	for($i = 0; $i < sizeof($SurveyMappingAry) ; $i++){
		$content .= "<tr>";
		$content .= "	<td>".($i+1).".</td>";
		$content .= "	<td>".$SurveyMappingAry[$i]["MAPPINGTITLE"]."</td>";
		if($IsStage3)
		{
			$content .= "	<td>".$SurveyMappingAry[$i]["COMMENT"]."</td>";
		}
		$content .= "</tr>";
	}
	$content .= "</table>";



$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']);
	if (preg_match('/opera/', $userAgent)) { $agentName = 'opera'; } 
	elseif (preg_match('/webkit/', $userAgent)) { $agentName = 'safari'; } 
	elseif (preg_match('/msie/', $userAgent)) { $agentName = 'msie'; } 
	elseif (preg_match('/mozilla/', $userAgent) && !preg_match('/compatible/', $userAgent)) { $agentName = 'mozilla'; } 
	else { $agentName = 'unrecognized'; }
	

	$html_header = '<html xmlns:v="urn:schemas-microsoft-com:vml"
			xmlns:o="urn:schemas-microsoft-com:office:office"
			xmlns:w="urn:schemas-microsoft-com:office:word"
			xmlns="http://www.w3.org/TR/REC-html40">
			
			<head>
			<meta http-equiv=Content-Type content="text/html; charset=utf-8">
			<meta name=ProgId content=Word.Document>
			<meta name=Generator content="Microsoft Word 9">
			<meta name=Originator content="Microsoft Word 9">
			';
			
	$docstyle = <<<EOH
	<!--[if !mso]>
			<style>
			v\:* {behavior:url(#default#VML);}
			o\:* {behavior:url(#default#VML);}
			w\:* {behavior:url(#default#VML);}
			.shape {behavior:url(#default#VML);}
			</style>
			<![endif]-->
			
			<!--[if gte mso 9]><xml>
			 <w:WordDocument>
			  <w:View>Print</w:View>
			  <w:DoNotHyphenateCaps/>
			  <w:PunctuationKerning/>
			  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
			  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
			 </w:WordDocument>
			</xml><![endif]-->
			<style>
			<!--
			 /* Font Definitions */
			@font-face
				{font-family:Verdana;
				panose-1:2 11 6 4 3 5 4 4 2 4;
				mso-font-charset:0;
				mso-generic-font-family:swiss;
				mso-font-pitch:variable;
				mso-font-signature:536871559 0 0 0 415 0;}
			 /* Style Definitions */
			p.MsoNormal, li.MsoNormal, div.MsoNormal
				{mso-style-parent:"";
				margin:0in;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:7.5pt;
			        mso-bidi-font-size:8.0pt;
				font-family:"Verdana";
				mso-fareast-font-family:"Verdana";}
			.common_table_list{
				
				border-collapse: collapse;
				background-color: transparent;
				width:100%;
				display: table;
			}
			.common_table_list th{
				border:1px solid black;
			}
			.common_table_list td{
				border:1px solid black;
			}
			.tool_files_col{
				width:100px;
			}
			.mytable th{
				background-color: #A6A6A6;
			}
			.mytable td{
				border:1px solid #A6A6A6;
			}
			.wordtable{
				border:1px solid black;
				width:98%;
				
			}
			p.small
				{mso-style-parent:"";
				margin:0in;
				margin-bottom:.0001pt;
				mso-pagination:widow-orphan;
				font-size:1.0pt;
			        mso-bidi-font-size:1.0pt;
				font-family:"Verdana";
				mso-fareast-font-family:"Verdana";}
			@page WordSection1
				{size:595.3pt 841.9pt;
				margin:72.0pt 72.0pt 72.0pt 72.0pt;
				mso-header-margin:42.55pt;
				mso-footer-margin:49.6pt;
				mso-paper-source:0;
				layout-grid:18.0pt;}
			div.WordSection1
				{page:WordSection1;}
			-->
			</style>
			<!--[if gte mso 9]><xml>
			 <o:shapedefaults v:ext="edit" spidmax="1032">
			  <o:colormenu v:ext="edit" strokecolor="none"/>
			 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
			 <o:shapelayout v:ext="edit">
			  <o:idmap v:ext="edit" data="1"/>
			 </o:shapelayout></xml><![endif]-->
			 
EOH;

$libDOC -> setstyle($docstyle);
$libDOC -> sethtmlheader($html_header);
$file_name = $Survey_title;
$file_name = ($agentName == "mozilla") ? $file_name : iconv("UTF-8", "Big5", $file_name);
	
$libDOC -> createDoc($content, $file_name);  //create the doc

intranet_closedb();
?>