<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$sql = "SELECT Title, Description, Question, DateStart, DateEnd ";
$sql .= "FROM {$intranet_db}.IES_SURVEY ";
$sql .= "WHERE SurveyID = {$SurveyID}";
$survey_detail_arr = current($li->returnArray($sql));

$survey_title = $survey_detail_arr["Title"];
$survey_desc = $survey_detail_arr["Description"]; 
$survey_question = $survey_detail_arr["Question"];
$survey_startdate = date("Y-m-d", strtotime($survey_detail_arr["DateStart"]));
$survey_enddate = date("Y-m-d", strtotime($survey_detail_arr["DateEnd"]));

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>

<script language="JavaScript">

function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}

</script>

<form name="form1" method="POST" action="update.php">
<table border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td>Title</td>
          <td><input type="text" name="title" value="<?=$survey_title?>" /></td>
        </tr>
        <tr>
          <td>Description</td>
          <td><textarea name="description"><?=$survey_desc?></textarea></td>
        </tr>
        <tr>
          <td>Start Date</td>
          <td><?=$linterface->GET_DATE_PICKER("startdate", $survey_startdate)?></td>
        </tr>
        <tr>
          <td>End Date</td>
          <td><?=$linterface->GET_DATE_PICKER("enddate", $survey_enddate)?></td>
        </tr>
        <tr>
          <td>Survey Form</td>
          <td><input type="button" value="Create Form" onClick="window.open('createForm.php')" /></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td>
      <table border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td><input type="submit" /></td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<input type="hidden" name="qStr" value="<?=$survey_question?>" />
<input type="hidden" name="SurveyID" value="<?=$SurveyID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>