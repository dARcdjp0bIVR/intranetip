<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$dec_SurveyID = base64_decode($SurveyID);
if(is_numeric($dec_SurveyID))
{
  $sql = "UPDATE {$intranet_db}.IES_SURVEY ";
  $sql .= "SET ";
  $sql .= "Question = '{$qStr}', ";
  $sql .= "ModifiedBy = {$UserID}, ";
  $sql .= "DateModified = NOW() ";
  $sql .= "WHERE SurveyID = {$dec_SurveyID}";
  
  $li->db_db_query($sql);
}

header("Location: createQue.php?SurveyID={$SurveyID}&msg=update");
intranet_closedb();
?>