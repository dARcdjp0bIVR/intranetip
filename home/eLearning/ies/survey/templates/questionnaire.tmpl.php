<!-- Question Manager Class -->
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/oo_survey/question_manager.js"></script>

<script language="JavaScript">
var qm = new question_manager();
var q_arr = new Array();
var q_order_arr = new Array();
// Load question string and parse to array
var qStr = "<?=$Survey_question?>";

function genQuestionTableDisplay()
{
  var qTable = "";
  var i = 0;
  $.each(q_order_arr, function(key, val)
  {
  	
    var q_obj = q_arr[val];
    if(typeof q_obj == "object")
    {
      qTable += q_obj.genAnsQuestionDisplay(i);
      i++;
    }
  });
  
  $("#checkQObj").html(qTable);
}

function NameValidation(ParName){

	if(ParName.length == 0){
		alert('<?=$Lang['IES']['FillInTheRespondent']?>');
		return false;
	}
	else if(ParName.match(/<|>|\"|\'|%/)){
		alert ('<?=$Lang['IES']['IncorrectRespondent']?>');
		return false;
	}
	else{
		return true;
	} 
}

function checkform(AddMore){
	var valid = false;
	
	
	valid = NameValidation($('[name=Respondent]').val());
	
	if(valid){
		var alert_sentence = "<?=$Lang['IES']['IsSubmitQuestionnaire']?>";
		if(confirm(alert_sentence)){
			if(AddMore){
				$("#AddMore").val("1");
			}
			document.form1.submit(); 
		}
	}
}

$(document).ready(function(){
	  $.ajax({
    url: "/templates/oo_survey/questionnaire.php",
    type: "POST",
    dataType: "xml",
    success: function(xml){
      var basePath = $(xml).find("BasePath").text();
      var qTypeJSon = eval('(' + $(xml).find("QTypeJson").text() + ')');
      
      // Auto load config and question type js
      qm.load(basePath, qTypeJSon);
  
      // Load question string and parse to array

      if(qStr != "")
      {
        var q_raw_arr = qm.parseQuestionStr(qStr);
        $.each(q_raw_arr, function(key, obj){
          var qType = obj.qType;
          var qRawStr = obj.qRawStr;
          
          var _obj_name = qm.getQueTypeObjName(qType);
          var _hash_str = qm.randomString();
          
          eval("var q_obj = new "+_obj_name+";");
          q_obj.parseQuestionString(qRawStr);
          q_obj.setQuestionHash(_hash_str);
          q_arr[_hash_str] = q_obj;
          q_order_arr.push(_hash_str);
        });
        
        genQuestionTableDisplay();
      }
    }
  });
  
  
});	

</script>
</head>

<body>


<form name="form1" method="POST" action="fill_update.php">
<div class="q_header">
		<h3>
        	<span>&nbsp;<?=$html_TypeName?>&nbsp;</span><?=$html_studentname?><br />
        	<?=$Survey_title?>&nbsp;
        </h3>
 <!-- <h3><?=$Survey_title?></h3>-->
    <ul class="q_tab">
      <li class="current"><a href="#"><?=$Title?></a></li>
    </ul>

</div><!-- q_header end -->
<div class="q_content">
<div class="navigation">
<?=$html_navigation?>

</div>
<table width="100%">
  <tr>
    <td align="right"><?=$html_message?></td>
  </tr>
</table>

<table class="form_table">

			<col class="field_title" />

			<col  class="field_c" />
            <tr>
				<td><?=$Lang['IES']['Respondent']?></td>
				<td>:</td>
				<td ><?=$HTML_Respondent?></td>
			</tr>
			<tr>
				<td><?=$Lang['IES']['QuestionnaireDescription']?></td>
				<td>:</td>
				<td ><?=$Survey_desc?></td>
			</tr>
		<?=$html_SubmitDate?>
</table>



<div id="checkQObj" />

</div>

<input type="hidden" name="key" value="<?=$originalKey?>" />
<INPUT type="hidden" name="qstr" Value="<?=$Survey_question?>" />
<INPUT type="hidden" name="SurveyID" Value="<?=$SurveyID?>" />
<INPUT type="hidden" name="AddMore" id="AddMore" Value="0" />
<INPUT type="hidden" name="isedit" value="<?=$is_edit?>" />
<INPUT type="hidden" name="respondentNature" value="<?=$RESPONDENTNATURE?>" />

<div class="edit_bottom"> 
	<?=$html_SubmitAndNew?>
	<input class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="button" onClick='checkform(0)' Value="<?=$Lang['IES']['Submit']?>" />
	<input class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="reset" Value="<?=$Lang['IES']['Reset']?>" />
</div>
</form>