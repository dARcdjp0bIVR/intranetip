<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/jquery.tablednd_0_5.js"></script>
<script type="text/javascript" src="<?=$intranet_root?>/templates/ies.js"></script>
<script language="JavaScript">
<!--
jsSurveyMappingID = '<?=$SurveyMappingID?>';

$(document).ready(function(){
	// Initialise the table
	$(".ClassDragAndDrop").tableDnD({
		dragHandle: "Dragable",
		onDragClass: "move_selected",
		onDrop:	function(table, row){
								$.ajax({
									type: "POST",
									url: "ajax/ajax_groupingListHandler.php",
									data: "Action=Sequence&key=<?=$key?>&"+$.tableDnD.serialize(),
									success: function(msg){
									}
								});
						}
	});
});


function saveComment() {
	
	$.ajax({
		url:      "ajax/ajax_groupingHandler.php",
		type:     "POST",
		data:     "Action=saveSurveyDiscovery&"+$("form[name=form1]").serialize(),
		async:	  false,
		error:    function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
				  },
		success:  function(xml){
				  	  result = $(xml).find("result").text();
		 			  if (Number(result) == 1) {
					  	$("#showSuccess").show().fadeOut(5000);
					  	
					  	if (jsSurveyMappingID == '')
					  		reloadPage();	// reload the page if user adding a new discovery
					  		
					  	window.opener.location.reload(true);	// reload the parent page to update the discovery in the index page
					  		
					  } else {
					  	$("#showFailed").show().fadeOut(5000);
					  }
			}
	});
}

function reloadPage()
{
	document.form1.action = 'questionnaire_discovery_list.php';
	document.form1.submit();
}
-->
</script>

<form name="form1" method="POST" action="">
<div class="q_header">
<h3><?=$html["survey_title"]?></h3>
<?=$html["tag"]?> 
</div><!-- q_header end -->
<div class="q_content">

    <!-- navigation star -->
		<div class="navigation">
			<table width="100%" style="font-size:15px">
				<tr>
					<td>
						<?=$html_navigation?>
					</td>
					<td>
						<div class="IES_top_tool" style="margin:0">
							<?=$html["print_and_export"]?>
						</div>
					</td>
				</tr>
			</table>
		</div>
	<!-- navigation end -->
	<?=$html_instr?>
	
	<div class="content_top_tool">
		<?=$html_tool?>
	</div>
<div class="table_board">
    <table class="form_table">
        <col class="field_title" />
		<col  class="field_c" />
		<?=$html["status_display"]?>
        <?=$html["title"]?>
    </table>
    
    <table class="form_table">
        <col class="field_title" />
		<col  class="field_c" />
        <?=$html["commentBox"]?>
    </table>
                           
	<br>
	<p class="spacer"></p>
</div>
  <!-- submit btn start -->
  <div class="edit_bottom"> 
  	<?=$html["save_button"]?>
  	
    <?=$html["close_button"]?>
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->

<input type="hidden" name="qStr" />
<input type="hidden" name="key" value="<?=$key?>" />

<input type="hidden" id="SurveyID" name="SurveyID" value="<?=$SurveyID?>" />
<input type="hidden" id="SurveyMappingID" name="SurveyMappingID" value="<?=$SurveyMappingID?>" />

</form>