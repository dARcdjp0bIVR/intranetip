<!-- Question Manager Class -->
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/oo_survey/question_manager.js"></script>

<script language="JavaScript">
var qm = new question_manager();
var q_arr = new Array();
var q_order_arr = new Array();
// Load question string and parse to array
var qStr = "<?=$Survey_question?>";

function genQuestionTableDisplay()
{
  var qTable = "";
  var i = 0;
  $.each(q_order_arr, function(key, val)
  {
  	
    var q_obj = q_arr[val];
    if(typeof q_obj == "object")
    {
      qTable += q_obj.genAnsQuestionDisplay(i);
      i++;
    }
  });
  
  $("#checkQObj").html(qTable);
}


$(document).ready(function(){
	  $.ajax({
    url: "/templates/oo_survey/questionnaire.php",
    type: "POST",
    dataType: "xml",
    success: function(xml){
      var basePath = $(xml).find("BasePath").text();
      var qTypeJSon = eval('(' + $(xml).find("QTypeJson").text() + ')');
      
      // Auto load config and question type js
      qm.load(basePath, qTypeJSon);
  
      // Load question string and parse to array

      if(qStr != "")
      {
        var q_raw_arr = qm.parseQuestionStr(qStr);
        $.each(q_raw_arr, function(key, obj){
          var qType = obj.qType;
          var qRawStr = obj.qRawStr;
          
          var _obj_name = qm.getQueTypeObjName(qType);
          var _hash_str = qm.randomString();
          
          eval("var q_obj = new "+_obj_name+";");
          q_obj.parseQuestionString(qRawStr);
          q_obj.setQuestionHash(_hash_str);
          q_arr[_hash_str] = q_obj;
          q_order_arr.push(_hash_str);
        });
        
        genQuestionTableDisplay();
      }
    }
  });
  
  
});	

</script>
</head>

<body>


<form name="form1" method="POST" action="fill_update.php">
<div class="q_header">
 
	<h3>
        <span>&nbsp;<?=$html_TypeName?>&nbsp;</span><?=$html_studentname?><br />
        <?=$Survey_title?>&nbsp;
    </h3>
<div class="watermark"></div>
    <ul class="q_tab">
      <li class="current"><a href="#"><?=$Title?></a></li>
    </ul>
	<div class="IES_top_tool">
	<a href="export_questionaire.php?key=<?=$encode_key?>" class="export" title="<?=$Lang['IES']['Export_Question']?>"><?=$Lang['IES']['Export_Question']?></a>
	</div>

</div><!-- q_header end -->
<div class="q_content">

<table class="form_table">
			<col class="field_title" />

			<col  class="field_c" />
            <tr>
				<td><?=$Lang['IES']['QuestionnaireTitle']?></td>
				<td>:</td>
				<td ><?=$Survey_title?></td>
			</tr>
			<tr>
				<td><?=$Lang['IES']['QuestionnaireDescription']?></td>
				<td>:</td>
				<td ><?=$Survey_desc?></td>
			</tr>
</table>



<div id="checkQObj" />

</div>

<input type="hidden" name="key" value="<?=$originalKey?>" />
<INPUT type="hidden" name="qstr" Value="<?=$Survey_question?>" />
<INPUT type="hidden" name="SurveyID" Value="<?=$SurveyID?>" />

<div class="edit_bottom"> 
	<!--input class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" type="button" onClick='javascript:window.close();' Value="Close Window" /-->
	<?=$html["close_button"]?>
</div>
</form>