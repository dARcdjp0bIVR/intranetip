<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title></title>
	<link href="css/text" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES_q_pop.css" rel="stylesheet" type="text/css">
	
	<link href="<?=$PATH_WRT_ROOT?>/templates/2009a/css/IES_q_pop_thick.css" rel="stylesheet" type="text/css">
	
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/lang/script.en.js"></script>
<script language="JavaScript" src="/templates/2007script.js"></script>
<script language="JavaScript" src="/templates/script.js"></script>
	
	<script>
	<!--
	jsSurveyMappingID = '<?=$SurveyMappingID?>';
	
	function saveComment() {
		
		$.ajax({
			url:      "ajax/ajax_groupingHandler.php",
			type:     "POST",
			data:     "Action=saveSurveyDiscovery&"+$("form[name=form1]").serialize(),
			async:	  false,
			error:    function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
					  },
			success:  function(xml){
					  	  result = $(xml).find("result").text();
			 			  if (Number(result) == 1) {
						  	$("#showSuccess").show().fadeOut(5000);
						  	
						  	if (jsSurveyMappingID == '')
						  		window.parent.reloadPage();	// reload the page if user adding a new discovery
						  		
						  	window.parent.opener.location.reload(true);	// reload the parent page to update the discovery in the index page
						  		
						  } else {
						  	$("#showFailed").show().fadeOut(5000);
						  }
				}
		});
	}
	-->
	</script>
  </head>
  <body>
  <form name="form1" method="POST">
  <div class="form_board thick_box">
  <div class="ies_q_box q_group">

        	
            <table class="form_table">
	            <col class="field_title" />
				<col  class="field_c" />
	            <?=$html["title"]?>
            </table>
            
            <table class="form_table">
	            <col class="field_title" />
				<col  class="field_c" />
	            <?=$html["commentBox"]?>
            </table>
 </div>
 </div>
 
 <input type="hidden" id="SurveyID" name="SurveyID" value="<?=$surveyID?>" />
 <input type="hidden" id="SurveyMappingID" name="SurveyMappingID" value="<?=$SurveyMappingID?>" />
</form>



  </body>
</html>