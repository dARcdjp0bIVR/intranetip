<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey_email.php");
intranet_auth();
intranet_opendb();

$SurveyID = $_POST["SurveyID"];
$SurveyEmailID = $_POST["SurveyEmailID"];
$title = $_POST["title"];
$content = $_POST["content"];
$IS_SEND_EMAIL = $_POST["IS_SEND_EMAIL"];
$ToAddress_INTERNAL = $_POST["ToAddress_INTERNAL"];
$ToAddress_EXTERNAL = $_POST["ToAddress_EXTERNAL"];

$libies_survey_email = new libies_survey_email();
$libies_survey_email->surveyID = $SurveyID;
$libies_survey_email->surveyEmailID = $SurveyEmailID;
$libies_survey_email->mailTitle = $title;
$libies_survey_email->mailContent = $content;

if (!empty($ToAddress_INTERNAL)) {
	$libies_survey_email->setCampusMailList($ToAddress_INTERNAL,libies_survey_email::IS_SET_RAW,libies_survey_email::IS_COMPOSE_PUREMAILLIST);
}
if (!empty($ToAddress_EXTERNAL)) {
	$libies_survey_email->setWebMailList($ToAddress_EXTERNAL,libies_survey_email::IS_SET_RAW);
}
######################################
## Save Email
if ($IS_SEND_EMAIL) {
	$sentStatus = $ies_cfg['DB_IES_SURVEY_EMAIL_Status']['SENT'];
} else {
	$sentStatus = $ies_cfg['DB_IES_SURVEY_EMAIL_Status']['NOT_SENT'];
}
$result = $libies_survey_email->saveEmail($sentStatus);
if (!in_array(false,$result)) {
	$msg = "update";
} else {
	$msg = "update_failed";
}
######################################

######################################
## Send Email
if ($IS_SEND_EMAIL) {
	# send email
	$libies_survey_email->sendEmail();
	$e_msg = "email";
}
######################################

$task_id = $task_id;
$code_key = "SurveyID={$SurveyID}&task_id=$task_id";
$key = base64_encode($code_key);

header("Location: compose_email.php?key=$key&msg=$msg&e_msg=$e_msg");
intranet_closedb();
?>