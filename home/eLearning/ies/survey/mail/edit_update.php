<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
intranet_auth();
intranet_opendb();


class libies_temp extends libies {
	function processExternalEmail($ParRecipients, $ParTitle, $ParContent, $ParSurveyID, $ParMailType) {
		global $ies_cfg;
		$tempRecipient = $ParRecipients;
		$tempRecipient = array_map(array($this,"getEmailInBlanket"),$tempRecipient);
		$result = array();
		foreach($tempRecipient as $key => $email) {
			$newContent = $this->returnCustomizedContent($ParSurveyID,$email,$ParContent);
			$sendResult = $this->sendEmailForSurveyEmail(array($email),$ParTitle,$newContent,$ParMailType);
			if ($sendResult) {
				$this->updateEmailStatus($ParSurveyID, $email, 1, $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);
			}
			$result[]=$sendResult;
		}
		return $result;
	}
	function processInternalEmail($ParRecipients,$ParMailType, $ParTitle, $ParContent, $ParSurveyID) {
		$tempRecipient = $ParRecipients;		
		$tempRecipient = array_map(array($this,"getUIDInBlanket"),$tempRecipient);
		
		switch ( strtoupper($ParMailType) ) {
			default:
			case "IMAIL":
				$UserIDArray = $this->extractIMailUserID($tempRecipient);
				break;
			case "GAMMAMAIL":
				$UserIDArray = $this->extractGammaMailUserID($tempRecipient);
				break;
		}
		$result = $this->IESSendModuleMail($UserIDArray,$ParTitle,$ParContent, $ParSurveyID);
		return $result;
	}
	function IESSendModuleMail($ParUserIDArray,$ParTitle,$ParContent, $ParSurveyID) {
		global $ies_cfg;
		$libwebmail = new libwebmail();
		if (is_array($ParUserIDArray)) {
			foreach($ParUserIDArray as $key =>$userid) {
				$customizedContent = str_replace("{{{ Q_KEY }}}", base64_encode("SurveyID=$ParSurveyID&email=$userid&respondentNature=".$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']), stripslashes($ParContent));
		
				$ToArray = array($userid);
				$Subject = $ParTitle;
				$Message = $customizedContent;
				$UseAdminEmailAsSender = 0;
//				global $UserID;
//				debug_r($libwebmail->GetUserEmailAddress($UserID));
				$sendResult = $libwebmail->sendModuleMail($ToArray,$Subject,$Message,$UseAdminEmailAsSender);
				if ($sendResult) {
					$this->updateEmailStatus($ParSurveyID, $userid, 1, $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']);
				}
				$result[] = $sendResult;
			}
		}
		return $result;
	}
	function extractGammaMailUserID($ParRecipients) {
		global $intranet_db;
		$returnArray = array();
		if (is_array($ParRecipients)) {
			$sql = "SELECT USERID FROM {$intranet_db}.INTRANET_USER WHERE IMAPUSEREMAIL IN ('".implode("','",$ParRecipients)."')";
		}
		$returnArray = $this->returnVector($sql);
		return $returnArray;		
	}
	function extractIMailUserID($ParRecipients) {
		$UserIDArray = array();
		if (is_array($ParRecipients)) {
			$libcampusmail = new libcampusmail();
			$UserInfoArray = $libcampusmail->returnRecipientUserIDArrayWithQuota(implode(",",$ParRecipients));
			$UserIDArray = array_map(array($this,"extractPureUserIDArray"),$UserInfoArray);
		}
		return $UserIDArray;
	}
	function getEmailInBlanket($ParEmailDisplay) {
		return $this->getEmailFromEmailDisplay($ParEmailDisplay);
	}
	function getUIDInBlanket($ParEmailDisplay) {
		return $this->getEmailFromEmailDisplay($ParEmailDisplay,false);
	}
	function extractPureUserIDArray($ParUserInfoArray) {
		return $ParUserInfoArray[0];
	}
	
	
	function newInternalSurveyEmailContent($elistInternal,$mailType,$SurveyID) {
		global $ies_cfg;
		$tempRecipient = $elistInternal;
		$tempRecipient = array_map(array($this,"getUIDInBlanket"),$tempRecipient);
		$sizeOf_tempRecipient = count($tempRecipient);
		
		for($i=0;$i<$sizeOf_tempRecipient;$i++) {
			$temp_amail = $tempRecipient[$i];
			if (strtoupper($mailType) == "GAMMAMAIL") {
				$temp_amail = $this->extractGammaMailUserID(array($temp_amail));
			} else {
				$temp_amail = $this->extractIMailUserID(array($temp_amail));
			}
			
			/* Since the IMail may get from a group like 'S3' will return a bundle of users, need to do foreach */
			if (is_array($temp_amail)) {
				foreach($temp_amail as $key => $amail) {
					$amail = preg_replace("/(?<=<).*(?=>)/",$amail,$elistInternal[$i]);
					$updateResult = $this->newSurveyEmailContents($SurveyID,$amail,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']);
				}
			}
		}
		return $updateResult;
	}
	
	function deleteInternalSurveyEmailContents($delmailInternal,$mailType,$SurveyID) {
		global $ies_cfg;
		$tempRecipient = $delmailInternal;
		$tempRecipient = array_map(array($this,"getEmailInBlanket"),$tempRecipient);
		$sizeOf_tempRecipient = count($tempRecipient);
		
		for($i=0;$i<$sizeOf_tempRecipient;$i++) {
			$temp_dmail = $tempRecipient[$i];
			if (strtoupper($mailType) == "GAMMAMAIL") {
				$temp_dmail = current($this->extractGammaMailUserID(array($temp_dmail)));
			} else {
				$temp_dmail = current($this->extractIMailUserID(array($temp_dmail)));
			}
			
			$dmail = preg_replace("/(?<=<).*(?=>)/",$temp_dmail,$delmailInternal[$i]);
			$updateResult = $this->deleteSurveyEmailContents($SurveyID,$dmail,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['INTERNAL']);
		}
		return $updateResult;
	}
	function returnCustomizedContent($ParSurveyID,$ParEmailOrUserID,$ParContent) {
		global $ies_cfg;
		$key = base64_encode("SurveyID=$ParSurveyID&email=$ParEmailOrUserID&respondentNature=".$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);
		$customizedContent = str_replace("{{{ Q_KEY }}}", $key, stripslashes($ParContent));
		return $customizedContent;
	}
}


$SurveyID = $_POST["SurveyID"];
$SurveyEmailID = $_POST["SurveyEmailID"];
$title = $_POST["title"];
$content = $_POST["content"];

$IS_SEND_EMAIL = $_POST["IS_SEND_EMAIL"];
$mailType = $_POST["mailType"];

$elistInternal = $_POST["MailList"]["INTERNAL"];
$delmailInternal = $_POST["DeleteMail"]["INTERNAL"];
$elistExternal = $_POST["MailList"]["EXTERNAL"];
$delmailExternal = $_POST["DeleteMail"]["EXTERNAL"];

# handling content update
$libies = new libies_temp();
//debug_r(array($elistInternal,$delmailInternal,$elistExternal,$delmailExternal));
#--- Update database
# Update for RespondentNature is Internal
if (is_array($elistInternal)) {
	$updateResult = $libies->newInternalSurveyEmailContent($elistInternal,$mailType,$SurveyID);
}
//debug_r(time());
if (is_array($delmailInternal)) {
	$updateResult = $libies->deleteInternalSurveyEmailContents($delmailInternal,$mailType,$SurveyID);
}
//debug_r(time());
# Update for RespondentNature is External
if (is_array($elistExternal)) {
	foreach($elistExternal as $amail){
		$updateResult = $libies->newSurveyEmailContents($SurveyID,$amail,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);
	}
}
//debug_r(time());
if (is_array($delmailExternal)) {
	foreach($delmailExternal as $dmail){
		$updateResult = $libies->deleteSurveyEmailContents($SurveyID,$dmail,$amail,$ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']);
	}
}
//debug_r(time());

#save email content
$libies->updateSurveryEmailContent($SurveyID, $title, $content);
//echo "updateSurveryEmailContent<br/>";
//debug_r(time());

if ( $IS_SEND_EMAIL ) {
	//echo "SEND EMAIL<br/>";
	//echo "INTERNAL<br/>";
	#--- process internal
	$internalEmails = ($_POST["Email"]["INTERNAL"])?$_POST["Email"]["INTERNAL"]:array();
	if (count($internalEmails)>0) {
		$libies->processInternalEmail($internalEmails,$mailType,$title,$content, $SurveyID);
	}
//debug_r(time());
	//echo "EXTERNAL<br/>";
	#--- process external
	$externalEmails = ($_POST["Email"]["EXTERNAL"])?$_POST["Email"]["EXTERNAL"]:array();
	if (count($externalEmails)>0) {
		$libies->processExternalEmail($externalEmails,$title,$content, $SurveyID, $mailType);
	}
//debug_r(time());
}
//echo "SEND EMAIL END<br/>";
//debug_r(time());

$code_key = "SurveyID={$SurveyID}&task_id=$task_id";
$key = base64_encode($code_key);

header("Location: edit.php?key=$key&msg=$msg");
intranet_closedb();
?>