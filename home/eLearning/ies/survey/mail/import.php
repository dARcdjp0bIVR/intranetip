<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();
$SurveyID = $_POST["SurveyID"];
$SurveyEmailID = $_POST["SurveyEmailID"];

switch(strtoupper($Action)) {
	default:
		break;
	case "START_IMPORT":
		/*
		 * Get CSV data
		 * Turn the CSV data to required array format
		 * if $SurveyEmailID
		 * 		get existing DB data
		 * 		combine data
		 * 		update record
		 * else
		 * 		new a record
		 * 
		 */
		 
		$libies = new libies();
		$csvRawData = $libies->getSurveyEmailCsvData();
		# TODO: temp. block the checking, May open later
//		$checkCsvDataResult = $libies->checkSurveyEmailCsvData($csvRawData);
		$checkCsvDataResult = true;
		if ($checkCsvDataResult === true) {
			array_shift($csvRawData);	# remove header row
			$csvData = $csvRawData;
			$newData = $libies->refineSurveyEmailCsvDataToRequiredFormat($csvData);
		
			if (!empty($SurveyEmailID)) {
				$oldData = $libies->getSurveyEmailList($SurveyEmailID);
				$combinedData = $libies->combineOldNewSurveyEmailList($oldData,$newData);
				$updateResult = $libies->updateSurveyEmailList($SurveyEmailID,$combinedData);
			} else {
				$updateResult = $libies->newSurveyEmailList($SurveyID,$newData);
			}
		}
		if ($updateResult) {
			$updateMessage = "update";
		} else {
			$updateMessage = "update_fail";
		}
		header("Location: edit.php?SurveyEmailID=$updateResult&msg=$updateMessage");
		break;
}

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
include_once("templates/import.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>