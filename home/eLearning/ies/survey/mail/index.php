<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$sql = "SELECT SurveyID, Title FROM {$intranet_db}.IES_SURVEY";
$survey_arr = $li->returnArray($sql);

//debug_r($survey_arr);
$html["survey_selection"] = getSelectByArray($survey_arr, "name=\"SurveyID\"", $SurveyID, 0, 0, "", 2);

if (!empty($SurveyID)) {
	$sql = "SELECT SURVEYEMAILID, TITLE FROM {$intranet_db}.IES_SURVEY_EMAIL WHERE SURVEYID = '$SurveyID'";
	$survey_email_arr = $li->returnArray($sql);
	$html["survey_email_selection"] = getSelectByArray($survey_email_arr, "name=\"SurveyEmailID\"", $SurveyEmailID, 0, 0, "", 2);
	$html["survey_email_selection"] .= <<<HTMLEND
	<input type="button" value="Edit" onClick="goEdit()" />
HTMLEND;
}

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

