<?php
/** [Modification Log] Modifying By: thomas
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

$libies_survey = new libies_survey();
$SurveyMappingID = $_REQUEST["SurveyMappingID"];
$IsEdit = $_REQUEST["IsEdit"];

$SurveyMapping = $libies_survey->loadSurveyMapping(null,null,$SurveyMappingID);
$currentSurveyMapping = current($SurveyMapping);
$surveyID = $currentSurveyMapping["SURVEYID"];
$x_axis = $currentSurveyMapping["XMAPPING"];
$y_axis = $currentSurveyMapping["YMAPPING"];
$x_title = $currentSurveyMapping["XTITLE"];
$y_title = $currentSurveyMapping["YTITLE"];
$title = $currentSurveyMapping["MAPPINGTITLE"];
$comment = htmlspecialchars($currentSurveyMapping["COMMENT"]);

$questionsElement = $libies_survey->breakQuestionsString($libies_survey->getSurveyQuestion($surveyID));
//error_log("x_axis ==>".$x_axis." y_axis -->".$y_axis."\n", 3, "/tmp/aaa.txt");

$x_questionSubquestion = explode(".",$x_axis);
$y_questionSubquestion = explode(".",$y_axis);
$x_question_pos = $x_questionSubquestion[0];
$x_subquestion_pos = $x_questionSubquestion[1];
$y_question_pos = $y_questionSubquestion[0];
$y_subquestion_pos = $y_questionSubquestion[1];


//DEBUG_R($questionsElement);
$x_question = $questionsElement[$x_question_pos]["TITLE"];
if (isset($questionsElement[$x_question_pos]["QUESTION_QUESTIONS"])) {
	$x_subquestion_index = $x_subquestion_pos-1;
	$x_question.=" - ".$questionsElement[$x_question_pos]["QUESTION_QUESTIONS"][$x_subquestion_index];
}

$html["x_question"] = "<tr><td nowrop='nowrap'>".$Lang['IES']['CombinationElement1']."</td><td>:</td><td>{$x_question}</td></tr>";
if (!empty($y_axis)) {
	$y_question = $questionsElement[$y_question_pos]["TITLE"];
	if (isset($questionsElement[$y_question_pos]["QUESTION_QUESTIONS"])) {
		$y_subquestion_index = $y_subquestion_pos-1;
		$y_question.=" - ".$questionsElement[$y_question_pos]["QUESTION_QUESTIONS"][$y_subquestion_index];
	}
	$html["y_question"] = "<tr><td nowrop='nowrap'>".$Lang['IES']['CombinationElement2']."</td><td>:</td><td>{$y_question}</td></tr>";
}


$html["display_table"] = $libies_survey->getCombineBoxContent($surveyID,$x_axis,$y_axis);

if(empty($y_axis)){
	//y_axis empty  , this is a single mapping 
}else{
	// this is a combine mapping
	if($IsEdit){
		$h_xTitle = $x_title;
		$h_yTitle = $y_title;
	}else{
		$x_title = intranet_htmlspecialchars($x_title);
		$y_title = intranet_htmlspecialchars($y_title);
		$h_xTitle = "<input type = \"text\" name=\"xTitle\" id=\"xTitle\" value=\"{$x_title}\">";
		$h_yTitle = "<input type = \"text\" id = \"yTitle\" name=\"yTitle\" value=\"{$y_title}\">";

	}
	$html["x_title"] = "<tr><td><div class=\"x-axis\"></div></td><td>:</td><td>{$h_xTitle}</td></tr>";
	$html["y_title"] = "<tr><td><div class=\"y-axis\"></div></td><td>:</td><td>{$h_yTitle}</td></tr>";
}


//	window.opener.location = parentURL;
//$saveAction = 'saveSurvey();window.parent.location.reload(true)';
$saveAction = 'saveSurvey();window.parent.reloadPage()';


if ($IsEdit) {
	$html['commentBOX'] = <<<HTML_COMMENT
	{$Lang['IES']['CombinationAnalysis']}<Br/>
		<textarea name="commentBox" rows="5">{$comment}</textarea>
HTML_COMMENT;

	$saveAction = 'saveComment();parent.window.opener.location.reload(true)';
}

	$successMessage = str_replace("\n"," ",libies_ui::getMsgRecordUpdated());
	$failMessage = str_replace("\n"," ",libies_ui::getMsgRecordFail());
	$html['editUI'] = <<<HTMLEND
	<tr>
		<td>&nbsp;</td>
		<td colspan="2">&nbsp;
		    <span id="showSuccess" style="display:none; float:right;">{$successMessage}</span>
			<span id="showFailed" style="display:none; float:right;">{$failMessage}</span>
		</td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:center">
		{$html["commentBOX"]}&nbsp;
		<p class="spacer"></p> <br/>  
	    <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" {$Lang['IES']['Save']} " onClick="{$saveAction}"/>
	    <input name="SurveyMappingID" value="{$SurveyMappingID}" type="hidden" />
	    </td>
    </tr>
HTMLEND;

$title = intranet_htmlspecialchars($title);
if($IsEdit){
	$h_title = $title;
}else{
	$h_title = "<input type = \"text\" name= \"title\" id=\"title\" value=\"{$title}\">";
}

$html["title"] = "<tr><td>".$Lang['IES']['CombinationName']."</td><td>:</td><td>{$h_title}</td></tr>";
include_once("templates/questionnaireShowTable.tmpl.php");
intranet_closedb();
?>