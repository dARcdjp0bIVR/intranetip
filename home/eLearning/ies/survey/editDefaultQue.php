<?php

/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
intranet_opendb();

$objIES = new libies();
$objIES_survey = new libies_survey();

// Survey Detail
if(isset($question_id))
{
  $li = new libdb();
  
  $sql = "SELECT QuestionStr ";
  $sql .= "FROM {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
  $sql .= "WHERE QuestionID = {$question_id}";
  $question_str = current($li->returnVector($sql));  
}

// Post-action message
if(isset($msg))
{
  $html_alertMsg = libies_ui::getMsgRecordUpdated();
}

// Tab Config
$linterface = new interface_html("ies_survey.html");
$linterface->LAYOUT_START();

?>

<!-- Question Manager Class -->
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/oo_survey/question_manager.js"></script>

<script language="JavaScript">

var qm = new question_manager();
var q_arr = new Array();
var q_order_arr = new Array();

function initQuestion()
{
  var qStr = "<?=$question_str?>";
  if(qStr != "")
  {
    var q_raw_arr = qm.parseQuestionStr(qStr);
    $.each(q_raw_arr, function(key, obj){
      var qType = obj.qType;
      var qRawStr = obj.qRawStr;
      
      var _obj_name = qm.getQueTypeObjName(qType);
      var _hash_str = qm.randomString();
      
      eval("var q_obj = new "+_obj_name+";");
      q_obj.parseQuestionString(qRawStr);
      q_obj.setQuestionHash(_hash_str);
      q_arr[_hash_str] = q_obj;
      q_order_arr.push(_hash_str);
    });
    
    genQuestionTableDisplay();
  }
}

function genQuestionTableDisplay()
{
  var qTable = "";

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    if(typeof q_obj == "object")
    {
      qTable += q_obj.genQuestionDisplay();
    }
  });
  
  $("#checkQObj").html(qTable);
  displayQuestionPanel(undefined, false);
  
  // Hide operation
  $(".q_box_manage").hide();
}

function BatchSet()
{
  BatchSetQuestion();
  BatchSetOption();
  BatchSetTableQuestion();
}

function BatchSetQuestion()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];

    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if($("#"+val).find("input[name=q]").length > 0)
      {
        var q = $("#"+val).find("input[name=q]").val();
        q_obj.setQuestion(q);
      }
    }
  });
}

function BatchSetTableQuestion()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];

    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.setTableQuestion == "function") {
        var que_txt_arr = new Array();
      
        $("#"+val).find("input[name=qQue]").each(function(){
          que_txt_arr[que_txt_arr.length] = $(this).val();
        });
        q_obj.setTableQuestion(que_txt_arr);
      }
    }
  });
}

function BatchSetOption()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.setOption == "function") {
        var opt_txt_arr = new Array();
      
        $("#"+val).find("input[name=qOpt]").each(function(){
          opt_txt_arr[opt_txt_arr.length] = $(this).val();
        });
        q_obj.setOption(opt_txt_arr);
      }
    }
  });
}

function BatchCheckDuplicateOptions()
{
  var dupOptQHashID = false;

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.checkDuplicateOption == "function") {
        var hasDupOpt = q_obj.checkDuplicateOption();
        
        if(hasDupOpt)
        {
          dupOptQHashID = val;
          return false;
        }
      }
    }
  });
  
  return dupOptQHashID;
}

function BatchCheckDuplicateQuestions()
{
  var dupQueQHashID = false;

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.checkDuplicateQuestion == "function") {
        var hasDupQue = q_obj.checkDuplicateQuestion();
        
        if(hasDupQue)
        {
          dupQueQHashID = val;
          return false;
        }
      }
    }
  });
  
  return dupQueQHashID;
}

function displayQuestionPanel(jDisplayPanel, jDisplayLink){

  var displayPanel = (typeof jDisplayPanel == "undefined") ? (q_order_arr.length == 0) : jDisplayPanel;
  var displayLink = (typeof jDisplayLink == "undefined") ? false : jDisplayLink;
  
  (displayPanel) ? $("#q_panel").show() : $("#q_panel").hide();
  (displayLink) ? $("#show_q_panel").show() : $("#show_q_panel").hide();
}

$(document).ready(function(){

  $.ajax({
    url: "/templates/oo_survey/questionnaire.php",
    type: "POST",
    dataType: "xml",
    success: function(xml){
      var basePath = $(xml).find("BasePath").text();
      var qTypeJSon = eval('(' + $(xml).find("QTypeJson").text() + ')');
      
      // Auto load config and question type js
      qm.load(basePath, qTypeJSon);
  
      // Append question type options to pull-down
      $.each(qm.qType, function(key, val){
        var _opt_name = val[1];
        
        $("#qTypeSel").append(
          $("<option></option>").
          attr("value",key).
          text(_opt_name)
        );
      });
      
      // Load question string and parse to array
      initQuestion();
      
      // Display add question link / panel
      displayQuestionPanel();
    }
  });
  
  
  // Bind action on pull-down change
  // (to add option number selection for MC)
  $("#qTypeSel").change(function(){
    var qTypeSel = $(this).find("option:selected").val();
    
    if(qTypeSel == "")
    {
      $("#qOptNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qQueNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qOptNumNA").show();
      $("#qQueNumNA").show();
    }
    else
    {
      var _obj_name = qm.getQueTypeObjName(qTypeSel);
      
      eval("var q_obj = new "+_obj_name+";");
      if(typeof q_obj.genQuestionNumSelection == "function")
      {
        $("#qQueNumSel").html(q_obj.genQuestionNumSelection());
        $("#qQueNumSel").show();
        $("#qQueNumNA").hide();
      }
      else
      {
        $("#qQueNumSel").hide();
        $("#qQueNumNA").show();
      }
  
      if(typeof q_obj.genOptionNumSelection == "function")
      {
        $("#qOptNumSel").html(q_obj.genOptionNumSelection());
        $("#qOptNumSel").show();
        $("#qOptNumNA").hide();
      }
      else
      {
        $("#qOptNumSel").hide();
        $("#qOptNumNA").show();
      }
    }
  });
  
  // Show question panel
  $("#show_q_panel").click(function(){
    displayQuestionPanel(true, false);
  });

  // Add question
  $("#addQ").click(function(){
    var q = $("#q").val();
    var qTypeSel = $("#qTypeSel option:selected").val();
   
    if(qTypeSel != "")
    {
      var qOptNumSel = $("#qOptNumSel option:selected").val();
      var qQueNumSel = $("#qQueNumSel option:selected").val();
      
      var _obj_name = qm.getQueTypeObjName(qTypeSel);
      var _hash_str = qm.randomString();
  
      eval("var q_obj = new "+_obj_name+";");
      q_obj.setQuestion(q);
      q_obj.setQuestionType(qTypeSel);
      q_obj.setQuestionHash(_hash_str);
      if(typeof q_obj.setOptionNum == "function") { q_obj.setOptionNum(qOptNumSel); }
      if(typeof q_obj.setQuestionNum == "function") { q_obj.setQuestionNum(qQueNumSel); }
      q_arr[_hash_str] = q_obj;
      q_order_arr.push(_hash_str);

      BatchSet();
      genQuestionTableDisplay();
      
      // Reset question input
      $("#q").val("");
      $("#qTypeSel").val("");
      $("#qOptNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qQueNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qOptNumNA").show();
      $("#qQueNumNA").show();
    }
    else
    {
      alert("<?=$Lang['IES']['ChooseQueType']?>");
    }
  });

/*  
  $("#cancelAddQ").click(function(){
    displayQuestionPanel(false, true);
  });

  // Remove question
  $("a[name=removeQ]").live("click", function(){
    var hashID = $(this).attr("hashID");
    var temp_q_arr = new Array();
    var temp_q_order_arr = new Array();
    
    $.each(q_order_arr, function(key, val)
    {
      if(val != hashID)
      {
        temp_q_arr[val] = q_arr[val];
        temp_q_order_arr[temp_q_order_arr.length] = val;
      }
    });
    q_arr = temp_q_arr;
    q_order_arr = temp_q_order_arr;
    
    BatchSet();
    genQuestionTableDisplay();
  });
*/
  
  // Add option
  $("a[name=addOpt]").live("click", function(){
    var hashID = $(this).attr("hashID");
    
    var q_obj = q_arr[hashID];
    q_obj.setOptionNum(q_obj.getOptionNum()+1);

    BatchSet();
    genQuestionTableDisplay();
  });
  
  // Add table question
  $("a[name=addQue]").live("click", function(){
    var hashID = $(this).attr("hashID");
    
    var q_obj = q_arr[hashID];
    q_obj.setQuestionNum(q_obj.getQuestionNum()+1);

    BatchSet();
    genQuestionTableDisplay();
  });

  // Generate question string
  $("#genQ").click(function(){
    var qStr = "";
    
    BatchSet();
    var dupOptQhashID = BatchCheckDuplicateOptions();
    var dupQueQhashID = BatchCheckDuplicateQuestions();

    if(dupOptQhashID)
    {
      alert(Lang['IES']['DuplicateOptions']);
      $("#"+dupOptQhashID).find("input[name=qOpt]:eq(0)").focus();
    }
    else if(dupQueQhashID)
    {
      alert(Lang['IES']['DuplicateQuestions']);
      $("#"+dupQueQhashID).find("input[name=qQue]:eq(0)").focus();
    }
    else
    {
      $.each(q_order_arr, function(key, val)
      {
        var q_obj = q_arr[val];
        
        // To get rid of "inArray" function sticked to array
        if(typeof q_obj == "object")
        {
          qStr += q_obj.genQuestionString();
        }
      });
      
      $("input[name=qStr]").val(qStr);
      document.form1.submit();
    }
  });
  
  // Reset Question
  $("#resetEdit").click(function(){
    q_arr = new Array();
    q_order_arr = new Array();
    
    initQuestion();
    genQuestionTableDisplay();
    displayQuestionPanel();
    
    // Reset input
    // Trigger change of type selection after resetting value
    $("#q").val("");
    $("#qTypeSel").val("").change();
  });
  
  // Cancel edit
  $("#cancelEdit").click(function(){
    //viewSurvey();
    parent.tb_remove();
  });
});

</script>

<form name="form1" method="POST" action="defaultQue_update.php">

<div class="q_content">
  <?=$html_alertMsg?>

  <div id="checkQObj"></div>

<!--
  <div class="q_box_manage"><a href="javascript:;" id="show_q_panel" class="q_add" style="display:none"><?=$Lang['IES']['AddNewQuestion']?></a></div>
-->

  <div id="q_panel" class="ies_q_box" style="display:none">
<!--
    <div class="q_box_manage">
      <a href="#" class="move_up"><?=$Lang['IES']['MoveUp']?></a>
      <a href="#" class="move_down"><?=$Lang['IES']['MoveDown']?></a>
      <a href="#" class="q_cancel"><?=$Lang['IES']['Delete']?></a>
    </div>
-->
              
    <div class="">
      <table class="form_table">
        <col class="field_title" />
        <col  class="field_c" />
        <tr>
          <td><?=$Lang['IES']['QuestionTitle']?></td>
          <td>:</td>
          <td ><input type="text" id="q" value="" class="textbox" /></td>
        </tr>

        <tr>
          <td><?=$Lang['IES']['QuestionFormat']?></td>
          <td>:</td>
          <td>
            <select id="qTypeSel"><option value="">- <?=$Lang['IES']['QuestionFormat']?> -</option></select> 
          </td>
        </tr>
        
        <tr>
          <td><?=$Lang['IES']['NumberOfQuestions']?></td>
          <td>:</td>
          <td>
            <span id="qQueNumNA"><?=$Lang['IES']['NotApplicable']?></span>
            <select id="qQueNumSel" style="display:none"> </select>
          </td>
        </tr>

        <tr>
          <td><?=$Lang['IES']['NumberOfOptions']?></td>
          <td>:</td>
          <td>
            <span id="qOptNumNA"><?=$Lang['IES']['NotApplicable']?></span>
            <select id="qOptNumSel" style="display:none"> </select>
          </td>
        </tr>
        
      </table>
      
      <div class="btn_area">
        <input id="addQ" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$Lang['IES']['Edit']?> " />
        <!--<input id="cancelAddQ" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$button_cancel?> " />-->
      </div>
    </div>
  </div>
  
  <!-- submit btn start -->
  <div class="edit_bottom"> 
    <p class="spacer"></p>
    <input id="genQ" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$button_save?> " />
    <input id="resetEdit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$button_reset?> " />
    <input id="cancelEdit" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$button_close?> " />
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->

</div> <!-- q_content end -->

<input type="hidden" name="QuestionID" value="<?=$question_id?>" />
<input type="hidden" name="qStr" value="<?=$qStr?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>