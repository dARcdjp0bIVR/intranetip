<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();

$dec_SurveyID = base64_decode($SurveyID);

if(is_numeric($dec_SurveyID))
{
  $li = new libdb();
  
  $li->Start_Trans();
  
  $sql = "DELETE FROM {$intranet_db}.IES_SURVEY_ANSWER ";
  $sql .= "WHERE SurveyID = {$dec_SurveyID}";
  $res = $li->db_db_query($sql);
  
  if($res)
  {
    $msg = "delete";
    $li->Commit_Trans();
  }
  else
  {
    $msg = "delete_failed";
    $li->RollBack_Trans();
  }
  
  echo $msg;
}

intranet_closedb();
?>