<?php

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
intranet_auth();
intranet_opendb();

$li = new libdb();
$json = new JSON_obj();

if(count($question_id) > 0)
{
  $sql = "SELECT GROUP_CONCAT(QuestionStr SEPARATOR '') ";
  $sql .= "FROM {$intranet_db}.IES_DEFAULT_SURVEY_QUESTION ";
  $sql .= "WHERE QuestionID IN (".implode(",", $question_id).")";
  
  echo current($li->returnVector($sql));
}

intranet_closedb();
?>