<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey_email.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();



if(isset($ToAddress_EXTERNAL)){
	$libies_survey = new libies_survey_email();
	
	$array = $libies_survey->returnPureMailListFromRaw($ToAddress_EXTERNAL);
	
	if(is_array($array)){
		foreach($array as $value){
			if(!strstr($value, "@") || strstr($value, " ") || !strstr($value, ".") ){
				echo 0;
				exit();
			}
		}
	}

}
echo 1;
exit();
?>