<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$libies_survey = new libies_survey();
$originalKey = $_REQUEST["key"];
$parameters = $libies_survey->breakEncodedURI($originalKey);
$SurveyID = $parameters["SurveyID"];
$StudentID = $parameters["StudentID"];
$TaskID = $parameters["TaskID"];
$Survey_type = trim($Survey_type);

if($action == "delete"){
	$AnswerID = base64_decode($ansID);
	$AnswerDetails = $libies_survey->getSurveyAnsDetailsByAnswerID($AnswerID);
	$result = $libies_survey->DeleteSurveyAnswer($AnswerID);
	$linterface = new interface_html();
	if($result){
		//$libies_survey->updateEmailStatus($SurveyID, $AnswerDetails["Respondent"], 1);
		$msg = "delete";
		
	}
	else{
		$msg = "delete_failed";
	}
	$msg = $libies_survey->Get_WARNING_MSG($msg);
	
	$html .= "<table width=\"100%\">";
  	$html .= "<tr>";
    $html .= "<td align=\"right\">{$msg}</td>";
 	$html .= "</tr></table>";

	//echo $AnswerID;
}

$RespondentList = $libies_survey->getSurveyAnsweredRespondentList($SurveyID);
//print_r($RespondentList);

$html .= libies_ui::getSurveyAnsweredRespondentTable($RespondentList, $IS_IES_STUDENT, $originalKey,$Survey_type);
echo $html;

intranet_closedb();
?>