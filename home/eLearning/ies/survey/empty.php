<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_opendb();

$linterface = new interface_html("ies_survey.html");
$linterface->LAYOUT_START();
?>
<script language="JavaScript">
function setSurveyQuestion(qStr)
{
  document.form1.qStr.value = qStr;
}
</script>

<form name="form1" method="POST" action="update.php">

<div class="q_header">
  <h3>建立新問卷</h3>
    <ul class="q_tab">
      <li class="current"><a href="#">基本設定</a></li>
      <li><a href="createForm.php">問卷問題</a></li>
    </ul>

</div><!-- q_header end -->
<div class="q_content">


  <!-- submit btn start -->
  <div class="edit_bottom"> 
    <span> 更新日期 :兩天前</span>
    <p class="spacer"></p>
    
    <input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" 儲存 " />
    <input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" 取消 " />
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->
</div> <!-- q_content end -->

<input type="hidden" name="qStr" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>