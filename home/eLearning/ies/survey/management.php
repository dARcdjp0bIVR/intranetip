<?
/** [Modification Log] Modifying By: Stanley
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$libies_survey = new libies_survey();
$originalKey = $_REQUEST["key"];
$parameters = $libies_survey->breakEncodedURI($originalKey);

$SurveyID = $parameters["SurveyID"];
$StudentID = $parameters["StudentID"];
$TaskID = $parameters["TaskID"];
$FromPage = $parameters["FromPage"];
$IsStage3 = $parameters["IsStage3"];
$Stage3TaskID = $parameters["Stage3TaskID"];

// Handle stage 3 upate task 
//if ($IsStage3) {
//	$updateTaskID = $Stage3TaskID;
//} else {
	$updateTaskID = $TaskID;
//}


if(empty($SurveyID)){
	echo "NO SURVEYID";
	exit();
}
$libies_survey = new libies_survey();
$libies_survey->UPDATE_TASK_CONTENT($updateTaskID, "", "survey:collect", $StudentID);
//######################GET the SURVEY ###########################
$SurveyDetails = $libies_survey->getSurveyDetails($SurveyID);

//DEBUG_R($SurveyDetails);
if(is_array($SurveyDetails) && count($SurveyDetails) == 1){
$SurveyDetails = current($SurveyDetails);
	$Survey_title = $SurveyDetails["Title"];
	$Survey_desc = $SurveyDetails["Description"]; 
	$Survey_question = $SurveyDetails["Question"];
	$Survey_type = $SurveyDetails["SurveyType"];
	$Survey_InputBy = $SurveyDetails["InputBy"];

	$Survey_startdate = date("Y-m-d", strtotime($SurveyDetails["DateStart"]));
	$Survey_enddate = date("Y-m-d", strtotime($SurveyDetails["DateEnd"]));
	$html_TypeName = $SurveyDetails["SurveyTypeName"];
}
#########################################


$IS_IES_STUDENT = $libies_survey->isIES_Student($UserID);
if($IS_IES_STUDENT){
	$encode_SurveyID = base64_encode($SurveyID); 
	if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
		$html_instr = "<hr />".$Lang['IES']['Questionnaire_management']['string1']."<hr />";
	}
	else if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){
		$html_instr = "<hr />".$Lang['IES']['Questionnaire_management']['string2']."<hr />";
	}
	else if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
		$html_instr = "<hr />".$Lang['IES']['Questionnaire_management']['string3']."<hr />";
	}
	$html_tool = "<div class=\"Conntent_tool\"><a href=\"/home/eLearning/ies/survey/questionnaire.php?key={$originalKey}\" title=\"{$Lang['Btn']['New']}\">{$Lang['IES']['New']} </a></div><br style=\"clear:both\" />";	
}
else{
	$html_studentSelectionBox = $libies_survey->getStudentSurveySelectionBox($originalKey,$Survey_type);
}


$h_exportButtonCaption = $Lang['IES']['Export_Question'];
$finishedSurveyCaption = $Lang['IES']['FinishedSurvey'];
if($Survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
	$h_exportButtonCaption = $Lang['IES']['Export_QuestionObserve'];
	$finishedSurveyCaption = $Lang['IES']['FinishedObserve'];
}
############################################
##	Construct Tag
$html_tag = libies_ui::GET_MANAGEMENT_BROAD_TAB_MENU("ManageCompleteQuestionnaire",$IS_IES_STUDENT,$originalKey,$FromPage);
############################################

############################################
##	close button
$html_close_button = libies_ui::getCloseButton();
############################################
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($Survey_type);
$linterface = new interface_html("ies_survey.html");



// navigation
$nav_arr[] = array($finishedSurveyCaption, "");
$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
$loading_image = $linterface->Get_Ajax_Loading_Image();

$linterface->LAYOUT_START();
$Title = "Testing";
$Page_title = $Survey_title;
include_once("templates/management.tmpl.php");



$linterface->LAYOUT_STOP();
intranet_closedb();
?>