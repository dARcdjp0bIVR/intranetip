<?php

/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
intranet_opendb();


$objIES = new libies();
$thisLang = $objIES->getThisSchemeLang();
//debug_r($thisLang);
$objIES_survey = new libies_survey();



// Survey Detail
$dec_SurveyID = base64_decode($SurveyID);
if(is_numeric($dec_SurveyID))
{
	/*
  $li = new libdb();
  
  $sql = "SELECT Title, Question ";
  $sql .= "FROM {$intranet_db}.IES_SURVEY ";
  $sql .= "WHERE SurveyID = {$dec_SurveyID}";
  $survey_detail_arr = current($li->returnArray($sql));
  */

  $survey_detail_arr = $objIES->getSurveyDetails($dec_SurveyID);
  if(is_array($survey_detail_arr) && count($survey_detail_arr) == 1){
	  $survey_detail_arr = current($survey_detail_arr);
  }
  $survey_title = $survey_detail_arr["Title"];
  $survey_question = $survey_detail_arr["Question"];
  
//$survey_question = addslashes($survey_question);
$survey_question = htmlspecialchars($survey_question);
//  debug_r($survey_question);
  $survey_typename = $survey_detail_arr["SurveyTypeName"];
  $survey_type = $survey_detail_arr["SurveyType"];  
  $survey_answer_arr = $objIES_survey->getSurveyAnswer($dec_SurveyID);
  $html_js_survey_ans_cnt = count($survey_answer_arr);
  $html_js_has_survey_ans = ($survey_ans_cnt > 0) ? 1 : 0;
}
else
{
  header("Location: createForm.php");
}

$observerDefaultQuestion = '';


$defaultQuestionRequestLang = (strtolower($thisLang) == 'en') ? $ies_cfg['DB_IES_DEFAULT_SURVEY_QUESTION']['EN']: $ies_cfg['DB_IES_DEFAULT_SURVEY_QUESTION']['B5'];
if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
	//load a set of default for survey = Observe
	$observerDefaultQuestion = $objIES_survey->getDefaultQuestion($ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0],$defaultQuestionRequestLang);
}elseif( $survey_type  == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){
	$observerDefaultQuestion = $objIES_survey->getDefaultQuestion($ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0],$defaultQuestionRequestLang);
}

// Post-action message
if(isset($msg))
{
  $html_alertMsg = libies_ui::getMsgRecordUpdated();
}

$questionTabCaption = ($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]) ? $Lang['IES']['QuestionnaireQuestion']: $Lang['IES']['QuestionnaireQuestionType2'];


// Tab Config
$TabMenuArr = array();
if($objIES->isIES_Teacher($UserID))
{
  $curTabIndex = 0;
}
else
{
  $TabMenuArr[] = array("createForm.php?SurveyID={$SurveyID}", $Lang['IES']['BasicSettings']);
  $curTabIndex = 1;
}
if(is_numeric($dec_SurveyID))
{
  $TabMenuArr[] = array("viewQue.php?key=".base64_encode("SurveyID={$dec_SurveyID}"), $questionTabCaption);
}
$html_tab_menu = libies_ui::GET_SURVEY_TAB_MENU($TabMenuArr, $curTabIndex);
$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);	
$linterface = new interface_html("ies_survey.html");
$html_ajax_loading = $linterface->Get_Ajax_Loading_Image();

$linterface->LAYOUT_START();

?>

<link href='<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css' rel='stylesheet' type='text/css'>

<!-- Question Manager Class -->
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/oo_survey/question_manager.js"></script>

<script language="JavaScript">

var qm = new question_manager();
var q_arr = new Array();
var q_order_arr = new Array();

function genQuestionTableDisplay()
{
  var qTable = "";

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    if(typeof q_obj == "object")
    {
      qTable += q_obj.genQuestionDisplay();
    }
  });
  
  $("#checkQObj").html(qTable);
  displayQuestionPanel();
}

function BatchSet()
{
  BatchSetQuestion();
  BatchSetOrder();
  BatchSetOption();
  BatchSetTableQuestion();
}

function BatchSetQuestion()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];

    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if($("#"+val).find("input[name=q]").length > 0)
      {
        var q = $("#"+val).find("input[name=q]").val();
        q_obj.setQuestion(q);
      }
    }
  });
}

function BatchSetOrder()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];

    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      var order = key + 1;
      q_obj.setQuestionOrder(order);
    }
  });
}

function BatchSetTableQuestion()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];

    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.setTableQuestion == "function") {
        var que_txt_arr = new Array();
      
        $("#"+val).find("input[name=qQue]").each(function(){
          que_txt_arr[que_txt_arr.length] = $(this).val();
        });
        q_obj.setTableQuestion(que_txt_arr);
      }
    }
  });
}

function BatchSetOption()
{
  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.setOption == "function") {
        var opt_txt_arr = new Array();
      
        $("#"+val).find("input[name=qOpt]").each(function(){
          opt_txt_arr[opt_txt_arr.length] = $(this).val();
        });
        q_obj.setOption(opt_txt_arr);
      }
    }
  });
}

function BatchCheckDuplicateOptions()
{
  var dupOptQHashID = false;

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.checkDuplicateOption == "function") {
        var hasDupOpt = q_obj.checkDuplicateOption();
        
        if(hasDupOpt)
        {
          dupOptQHashID = val;
          return false;
        }
      }
    }
  });
  
  return dupOptQHashID;
}

function BatchCheckDuplicateQuestions()
{
  var dupQueQHashID = false;

  $.each(q_order_arr, function(key, val)
  {
    var q_obj = q_arr[val];
    
    // To get rid of "inArray" function sticked to array
    if(typeof q_obj == "object")
    {
      if(typeof q_obj.checkDuplicateQuestion == "function") {
        var hasDupQue = q_obj.checkDuplicateQuestion();
        
        if(hasDupQue)
        {
          dupQueQHashID = val;
          return false;
        }
      }
    }
  });
  
  return dupQueQHashID;
}

function addQuestionsFromDefault(formObj)
{
  var selectedQueIDs = new Array();
  $(formObj).find("input[name^=question_id]").each(function(){
    if($(this).attr("checked"))
    {
      selectedQueIDs.push("question_id[]="+$(this).val());
    }
  });
  var query = selectedQueIDs.join("&");
  
  if(selectedQueIDs.length > 0)
  {

    $.ajax({
      url: "ajax/ajax_getDefaultQuestion.php",
      type: "POST",
      data: query,
      success: function(qStr){
        loadQStrtoArray(qStr);
        tb_remove();
      }
    });
  }
  else
  {
    alert("Nothing selected");
    tb_remove();
  }
}
function loadObserverDefaultQuestionTemplate(){


	var qStr = $("#observerDefaultQuestion").val();

	if(qStr == ''){
	}else{
	    loadQStrtoArray(qStr);
        tb_remove();
	}
}
function loadQStrtoArray(qStr)
{
  // Load question string and parse to array

  if(qStr != "")
  {
    var q_raw_arr = qm.parseQuestionStr(qStr);
    $.each(q_raw_arr, function(key, obj){
      var qType = obj.qType;
      var qRawStr = obj.qRawStr;
      var _obj_name = qm.getQueTypeObjName(qType);
      var _hash_str = qm.randomString();
      var _order = key + 1;
      
      eval("var q_obj = new "+_obj_name+";");
      q_obj.parseQuestionString(qRawStr);
      q_obj.setQuestionHash(_hash_str);
      q_obj.setQuestionOrder(_order);
      q_arr[_hash_str] = q_obj;
      q_order_arr.push(_hash_str);
    });
  }
  
  genQuestionTableDisplay();
}

function displayQuestionPanel(jDisplay){

  var show = (typeof jDisplay == "undefined") ? (q_order_arr.length == 0) : jDisplay;
  if(show)
  {
    $("#q_panel").show();
    $("#show_q_panel").hide();
  }
  else
  {
    $("#show_q_panel").show();
    $("#q_panel").hide();
  }
}

function viewSurvey(){
  self.location = "viewQue.php?key=<?=base64_encode("SurveyID=".$dec_SurveyID)?>";
}

$(document).ready(function(){

  if(<?=$html_js_survey_ans_cnt?>)
  {
    var confirmDelMsg = "<?=$Lang['IES']['CurrentSurveyAnswerNum']?>\n" +
                        "<?=$Lang['IES']['DelSurveyAnswer']?>\n\n" +
                        "<?=$Lang['IES']['DeleteWarning']?>";
    confirmDelMsg = confirmDelMsg.replace("#NUM#","<?=$html_js_survey_ans_cnt?>");
  
    if(confirm(confirmDelMsg))
    {
      $.ajax({
        url: "./ajax/ajax_deleteSurveyAnswer.php",
        type: "POST",
        data: "SurveyID=<?=$SurveyID?>",
        success: function(msg){
          switch(msg)
          {
            case "delete":
              alert("<?=$Lang['IES']['AlertMsg']["delete"]?>");
              break;
            case "delete_failed":
              alert("<?=$Lang['IES']['AlertMsg']["delete_failed"]?>");
              viewSurvey();
              break;
          }
        }
      });
    }
    else
    {
      viewSurvey();
    }
  }

  $.ajax({
    url: "/templates/oo_survey/questionnaire.php",
    type: "POST",
    dataType: "xml",
    beforeSend: function(){
      // Lock save / cancel / close window button
      $("input[type=button]").attr("disabled", true);
    },
    success: function(xml){
      var basePath = $(xml).find("BasePath").text();
      var qTypeJSon = eval('(' + $(xml).find("QTypeJson").text() + ')');
      
      // Auto load config and question type js
      qm.load(basePath, qTypeJSon);
  
      // Load question string and parse to array
      var qStr = "<?=$survey_question?>";
      loadQStrtoArray(qStr);
      

      // Append question type options to pull-down
      $.each(qm.qType, function(key, val){
        var _opt_name = val[1];
        
        $("#qTypeSel").append(
          $("<option></option>").
          attr("value",key).
          text(_opt_name)
        );
      });

<?
		if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]) {
			//if the type is observe, make the question type to short question as default			
			//1) change the  question input selection to hidden
			//2) hide some input option
?>
//		$('#questionTypeHandler').html('<?=$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]][1]?><input type= "hidden" name="qTypeSel" value="<?=$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]?>">');

		$("#qTypeSel").val(<?=$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]?>);
		$("#qTypeSel").hide();
		$("#questionTypeHandler").append('<?=$ies_cfg["Questionnaire"]["Question"][$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]][1]?>');
		/*
		$('#qTypeSel').unbind();
		$('#qTypeSel').change(function()
		{
			this.selectedIndex = <?=$ies_cfg["Questionnaire"]["QuestionType"]["FillShort"]?> - 1;
		});
		*/

		$('#questionOptions1').hide();
		$('#questionOptions2').hide();

<?
	  }
?>
      // Display add question link / panel
      displayQuestionPanel();
      $("#add_default_q").show();
      
      // Release save / cancel / close window button
      $("input[type=button]").removeAttr("disabled");
    }
  });
  
  
  // Bind action on pull-down change
  // (to add option number selection for MC)
  $("#qTypeSel").change(function(){
    var qTypeSel = $(this).find("option:selected").val();
    
    if(qTypeSel == "")
    {
      $("#qOptNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qQueNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qOptNumNA").show();
      $("#qQueNumNA").show();
    }
    else
    {
      var _obj_name = qm.getQueTypeObjName(qTypeSel);
      
      eval("var q_obj = new "+_obj_name+";");
      if(typeof q_obj.genQuestionNumSelection == "function")
      {
        $("#qQueNumSel").html(q_obj.genQuestionNumSelection());
        $("#qQueNumSel").show();
        $("#qQueNumNA").hide();
      }
      else
      {
        $("#qQueNumSel").hide();
        $("#qQueNumNA").show();
      }
  
      if(typeof q_obj.genOptionNumSelection == "function")
      {
        $("#qOptNumSel").html(q_obj.genOptionNumSelection());
        $("#qOptNumSel").show();
        $("#qOptNumNA").hide();
      }
      else
      {
        $("#qOptNumSel").hide();
        $("#qOptNumNA").show();
      }
    }
  });
  
  // Show question panel
  $("#show_q_panel").click(function(){
    displayQuestionPanel(true);
  });

  // Add question
  $("#addQ").click(function(){
    var q = $("#q").val();
    var qTypeSel = $("#qTypeSel option:selected").val();

    if(qTypeSel != "")
    {
      var qOptNumSel = $("#qOptNumSel option:selected").val();
      var qQueNumSel = $("#qQueNumSel option:selected").val();
      
      var _obj_name = qm.getQueTypeObjName(qTypeSel);
      var _hash_str = qm.randomString();
      var _order = q_order_arr.length + 1;
  
      eval("var q_obj = new "+_obj_name+";");
      q_obj.setQuestion(q);
      q_obj.setQuestionType(qTypeSel);
      q_obj.setQuestionHash(_hash_str);
      q_obj.setQuestionOrder(_order);
      if(typeof q_obj.setOptionNum == "function") { q_obj.setOptionNum(qOptNumSel); }
      if(typeof q_obj.setQuestionNum == "function") { q_obj.setQuestionNum(qQueNumSel); }
      q_arr[_hash_str] = q_obj;
      q_order_arr.push(_hash_str);

      BatchSet();
      genQuestionTableDisplay();
      
      // Reset question input
      $("#q").val("");
      $("#qTypeSel").val("");
      $("#qOptNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qQueNumSel").hide().find("option").each(function(){$(this).remove();});
      $("#qOptNumNA").show();
      $("#qQueNumNA").show();
    }
    else
    {
      alert("<?=$Lang['IES']['ChooseQueType']?>");
    }
  });
  
  $("#cancelAddQ").click(function(){
    displayQuestionPanel(false);
  });
  
  // Remove question
  $("a[name=removeQ]").live("click", function(){
    var hashID = $(this).attr("hashID");
    var temp_q_arr = new Array();
    var temp_q_order_arr = new Array();
    
    $.each(q_order_arr, function(key, val)
    {
      if(val != hashID)
      {
        temp_q_arr[val] = q_arr[val];
        temp_q_order_arr[temp_q_order_arr.length] = val;
      }
    });
    q_arr = temp_q_arr;
    q_order_arr = temp_q_order_arr;
    
    BatchSet();
    genQuestionTableDisplay();
  });
  
  // Move up question
  $("a[name=moveUpQ]").live("click", function(){
    var hashID = $(this).attr("hashID");
    
    $.each(q_order_arr, function(key, val)
    {
      if(key > 0 && val == hashID)
      {
        var _val = q_order_arr[key-1];
      
        q_order_arr[key-1] = val;
        q_order_arr[key] = _val;
        
        return false;
      }
    });
    
    BatchSet();
    genQuestionTableDisplay();
  });
  
  // Move down question
  $("a[name=moveDownQ]").live("click", function(){
    var hashID = $(this).attr("hashID");
    
    $.each(q_order_arr, function(key, val)
    {
      if(key < q_order_arr.length-1 && val == hashID)
      {
        var _val = q_order_arr[key+1];
      
        q_order_arr[key+1] = val;
        q_order_arr[key] = _val;
        
        return false;
      }
    });
    
    BatchSet();
    genQuestionTableDisplay();
  });
  
  // Add option
  $("a[name=addOpt]").live("click", function(){
    var hashID = $(this).attr("hashID");
    
    var q_obj = q_arr[hashID];
    q_obj.setOptionNum(q_obj.getOptionNum()+1);

    BatchSet();
    genQuestionTableDisplay();
  });
  
  // Add table question
  $("a[name=addQue]").live("click", function(){
    var hashID = $(this).attr("hashID");
    
    var q_obj = q_arr[hashID];
    q_obj.setQuestionNum(q_obj.getQuestionNum()+1);

    BatchSet();
    genQuestionTableDisplay();
  });

  // Generate question string
  $("#genQ").click(function(){
    var qStr = "";
    
    BatchSet();
    var dupOptQhashID = BatchCheckDuplicateOptions();
    var dupQueQhashID = BatchCheckDuplicateQuestions();

    if(dupOptQhashID)
    {
      alert(Lang['IES']['DuplicateOptions']);
      $("#"+dupOptQhashID).find("input[name=qOpt]:eq(0)").focus();
    }
    else if(dupQueQhashID)
    {
      alert(Lang['IES']['DuplicateQuestions']);
      $("#"+dupQueQhashID).find("input[name=qQue]:eq(0)").focus();
    }
    else
    {
      $.each(q_order_arr, function(key, val)
      {
        var q_obj = q_arr[val];
        
        // To get rid of "inArray" function sticked to array
        if(typeof q_obj == "object")
        {
          qStr += q_obj.genQuestionString();
        }
      });
      
      $("input[name=qStr]").val(qStr);
      document.form1.submit();
    }
  });
  
  // Cancel edit
  $("#cancelEdit").click(function(){
    viewSurvey();
  });
});

</script>

<form name="form1" method="POST" action="que_update.php">

<div class="q_header">
	<h3>
    <span>&nbsp;<?=$survey_typename?>&nbsp;</span><br />
    <?=$survey_title?>
  </h3>
  <?=$html_tab_menu?>

</div><!-- q_header end -->
<div class="q_content">
  <?=$html_alertMsg?>

  <div id="checkQObj"><?=$html_ajax_loading?></div>

  <div id="q_panel" class="ies_q_box" style="display:none">
<!--
    <div class="q_box_manage">
      <a href="#" class="move_up"><?=$Lang['IES']['MoveUp']?></a>
      <a href="#" class="move_down"><?=$Lang['IES']['MoveDown']?></a>
      <a href="#" class="q_cancel"><?=$Lang['IES']['Delete']?></a>
    </div>
-->
              
    <div class="">
      <table class="form_table">
        <col class="field_title" />
        <col  class="field_c" />
        <tr>
          <td><?=$Lang['IES']['QuestionTitle']?></td>
          <td>:</td>
          <td ><input type="text" id="q" value="" class="textbox" /></td>
        </tr>

        <tr>
          <td><?=$Lang['IES']['QuestionFormat']?></td>
          <td>:</td>
          <td>
            <div id="questionTypeHandler"><select id="qTypeSel" name="qTypeSel"><option value="">- <?=$Lang['IES']['QuestionFormat']?> -</option></select> </div>
          </td>
        </tr>
        
        <tr id="questionOptions1">
          <td><?=$Lang['IES']['NumberOfQuestions']?></td>
          <td>:</td>
          <td>
            <span id="qQueNumNA"><?=$Lang['IES']['NotApplicable']?></span>
            <select id="qQueNumSel" style="display:none"> </select>
          </td>
        </tr>

        <tr id="questionOptions2">
          <td><?=$Lang['IES']['NumberOfOptions']?></td>
          <td>:</td>
          <td>
            <span id="qOptNumNA"><?=$Lang['IES']['NotApplicable']?></span>
            <select id="qOptNumSel" style="display:none"> </select>
          </td>
        </tr>
        
      </table>
      
      <div class="btn_area">
        <input id="addQ" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$Lang['IES']['Add']?> " />
        <!--<input id="addDefaultQ" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$button_add?> From Default " />-->
        <input id="cancelAddQ" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$Lang['IES']['Cancel']?> " />
      </div>
    </div>
  </div>
  
  <div class="q_box_manage">
    <a href="javascript:;" id="show_q_panel" class="q_add" style="display:none"><?=$Lang['IES']['AddNewQuestion']?></a>

<?
	//SHOW THE TEACHER CREATE QUESTION FOR STUDENT SELECTED ONLY
	if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){	
?>
    <a id="add_default_q" href="addQueFromDefault.php?KeepThis=true&TB_iframe=true&height=400&width=500" class="thickbox q_add" style="display:none"><?=$Lang['IES']['AddFromDefaultQuestion']?></a>
<?}?>

<?
	//SHOW DEFAULT WHOLE SET OF QUESTION TEMPLATE FOR OBSERVE AND INTERVIEW
	if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0] || $survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){
?>
	<a id="observerDefaultLink" href = "javascript: loadObserverDefaultQuestionTemplate()"><?=$Lang['IES']['DefaultLoadSurveyQuestion']?></a>
<?}?>

  </div>
  
  <!-- submit btn start -->
  <div class="edit_bottom"> 
    <p class="spacer"></p>
    <input id="genQ" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
    <input id="cancelEdit" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$Lang['IES']['Cancel']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="window.close();" value=" <?=$Lang['IES']['Close']?> " />
    <p class="spacer"></p>
  </div>
  <!-- submit btn end -->

</div> <!-- q_content end -->

<input type="hidden" name="SurveyID" value="<?=$SurveyID?>" />
<input type="hidden" name="qStr" value="<?=$qStr?>" />
<input type ="hidden" name="observerDefaultQuestion" id = "observerDefaultQuestion" value="<?=$observerDefaultQuestion?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();


?>