<?php
//EDITING BY: Max
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_survey.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_opendb();

$libies_survey = new libies_survey();
$originalKey = $_REQUEST["key"];
$parameters = $libies_survey->breakEncodedURI($originalKey);
$SurveyID = $parameters["SurveyID"];
$StudentID = $parameters["StudentID"];
$TaskID = $parameters["TaskID"];
$FromPage = $parameters["FromPage"];
$email = $parameters["email"];
$Par_respondentNature = $parameters["respondentNature"];

//DEBUG_R($Par_respondentNature);
if(empty($_GET["key"]) && empty($_GET["SurveyID"])){
	echo "ERROR";
	exit();
}


$libies = new libies();
$IS_VALID_PERIOD = false;
$IS_VALID_TO_DO = false;
$IS_DONE_SURVEY = false;
$Survey_Compulsory = false; //Must answer in all question 
$is_edit = 0;

##############################################
# get the Survey Details
$SurveyDetails = $libies->getSurveyDetails($SurveyID);
if(is_array($SurveyDetails) && count($SurveyDetails) == 1 ){
	$SurveyDetails = current($SurveyDetails);
}
$SurveyStartDate = $SurveyDetails["DateStart"];
$SurveyEndDate = $SurveyDetails["DateEnd"];
$Survey_title = $SurveyDetails["Title"];
$Survey_desc = $SurveyDetails["Description"]; 
$Survey_question = $SurveyDetails["Question"];
$Survey_question = htmlspecialchars($Survey_question);
$Survey_type = $SurveyDetails["SurveyType"];
$html_TypeName = $SurveyDetails["SurveyTypeName"];


# Checking the valid period
if (strtotime($SurveyStartDate)<time() && strtotime($SurveyEndDate)>time() || empty($email)) {
	$IS_VALID_PERIOD = true;
}

$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($Survey_type);	
$linterface = new interface_html("ies_survey.html");

switch($Survey_type){
	case $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]:
		$h_dateCaption = $Lang['IES']['inputdateInterview'];
	break;
	case $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]:
		$h_dateCaption = $Lang['IES']['inputdateObserve'];
	break;
	default:
		$h_dateCaption = $Lang['IES']['inputdateSurvey'];
	break;
}



if(empty($email)){
	$IS_VALID_TO_DO = true;
	$HTML_Respondent = "<INPUT type='text' class='textbox' name='Respondent'/>";
	// navigation
	$nav_arr[] = array($Lang['IES']['FinishedSurvey'], "management.php?key={$originalKey}");
	$nav_arr[] = array($Lang['IES']['New'], "");
	$html_SubmitAndNew = "<input class=\"formbutton\" onmouseover=\"this.className='formbuttonon'\" onmouseout=\"this.className='formbutton'\" type=\"button\" onClick='checkform(1)' Value=\"{$Lang['IES']['SubmitAndMore']}\" />";
	$html_SubmitDate = "<tr><td>{$h_dateCaption}</td><td>:</td><td>".$linterface->GET_DATE_PICKER("InterviewDate", '')."</td></tr>";
	$is_edit = 1;
	$RESPONDENTNATURE = $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['Manual'];
}
else if(!empty($_GET["key"])){

	 
	$Respondent = $email;

	##	Checkings
	# get the Survey Email Details
	$surveyEmailDetails = $libies->getSurveyEmailDetails($SurveyID, $email, $Par_respondentNature);
//	DEBUG_R($surveyEmailDetails);
	if(!empty($surveyEmailDetails)){
		$EmailFromDB = $surveyEmailDetails[0]["EMAIL"];
		$Status =$surveyEmailDetails[0]['STATUS'];
		$EmailDisplayName = $surveyEmailDetails[0]['EmailDisplayName'];
		$RESPONDENTNATURE = $surveyEmailDetails[0]['RESPONDENTNATURE'];
		$email_userid = $surveyEmailDetails[0]["USERID"];
//		DEBUG_R($surveyEmailDetails);
		//DEBUG($EmailFromDB);
		# Checking user authority to do the survey
		
		if($RESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']){
			if (strtolower ($email) == $EmailFromDB) {
				$IS_VALID_TO_DO = true;
				
				if($Status == 2){
					$IS_DONE_SURVEY = true;
				}
				$HTML_Respondent = "{$email}<INPUT type='hidden' name='Respondent' Value='$email' />";
			}
		}
		else{
			if (strtolower ($email) == $email_userid) {
				$IS_VALID_TO_DO = true;
				
				if($Status == 2){
					$IS_DONE_SURVEY = true;
				}
			}
			$HTML_Respondent = "{$EmailDisplayName}<INPUT type='hidden' name='Respondent' Value='$email' />";
		}
		
	}

}





##############################################
##	HTML - do_survey
# Valid to do the survey
if ($IS_VALID_PERIOD && $IS_VALID_TO_DO && !$IS_DONE_SURVEY) {
	
	$li = new libdb();
	$Title = $Lang['IES']['FillInTheQuestionnaire'];
	
	if (empty($email)) {
		$html_navigation = $linterface->GET_NAVIGATION($nav_arr);
	}
	
	$html_message = isset($msg) ? $libies->Get_WARNING_MSG($msg) : "";
	$linterface->LAYOUT_START();
	
	include_once("templates/questionnaire.tmpl.php");
	$linterface->LAYOUT_STOP();
}

# Not valid to do the survey
if (!$IS_VALID_PERIOD) {
	$html["do_survey"] .= "The submission period is before {$SurveyEndDate}.....<br/>";
} else if (!$IS_VALID_TO_DO) {
	$html["do_survey"] .= "{$Lang['IES']['IS_VALID_TO_DO']}<br/>";
}
# Survey Result
if ($IS_DONE_SURVEY) {	
	if($RESPONDENTNATURE == $ies_cfg['DB_IES_SURVEY_EMAIL_RespondentNature']['EXTERNAL']){
		$key = base64_encode("SurveyID=$SurveyID&Respondent=$EmailFromDB&respondentNature=$RESPONDENTNATURE");
		header("Location: result.php?key=$key");
	}
	else{
//		echo $email;
		$key = base64_encode("SurveyID=$SurveyID&Respondent=$email_userid&respondentNature=$RESPONDENTNATURE");
		header("Location: result.php?key=$key");
	}
}
##############################################



echo $html["do_survey"];
intranet_closedb();
?>