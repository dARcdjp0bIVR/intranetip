<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

$remark1 = mysql_real_escape_string(stripslashes($remark1));
$remark2 = mysql_real_escape_string(stripslashes($remark2));
$remark3 = mysql_real_escape_string(stripslashes($remark3));
$remark4 = mysql_real_escape_string(stripslashes($remark4));

$dec_SurveyID = base64_decode($SurveyID);
if(is_numeric($dec_SurveyID))
{
  $sql = "UPDATE {$intranet_db}.IES_SURVEY ";
  $sql .= "SET ";
  $sql .= "Title = '".mysql_real_escape_string(stripslashes($title))."', ";
  $sql .= "Description = '".mysql_real_escape_string(stripslashes($description))."', ";
  $sql .= "Remark1 = '".$remark1."',";
  $sql .= "Remark2 = '".$remark2."',";
  $sql .= "Remark3 = '".$remark3."',";
  $sql .= "Remark4 = '".$remark4."',";
  $sql .= "DateStart = '{$startdate} 00:00:00', ";
  $sql .= "DateEnd = '{$enddate} 23:59:59', ";
  $sql .= "ModifiedBy = {$UserID}, ";
  $sql .= "DateModified = NOW() ";
  $sql .= "WHERE SurveyID = {$dec_SurveyID}";
  $li->db_db_query($sql);
  
  $msg = "update";
}
else
{
  $sql =  "INSERT INTO {$intranet_db}.IES_SURVEY ";
  $sql .= "(Title, Description, Remark1, Remark2,Remark3,Remark4,DateStart, DateEnd, SurveyType, SurveyStatus, InputBy, DateInput, ModifiedBy, DateModified) ";
  $sql .= "VALUES ";
  $sql .= "('".mysql_real_escape_string(stripslashes($title))."', '".mysql_real_escape_string(stripslashes($description))."', '".$remark1."','".$remark2."', '".$remark3."','".$remark4."','{$startdate} 00:00:00', '{$enddate} 23:59:59', {$SurveyType}, 1, {$UserID}, NOW(), {$UserID}, NOW())";
  $li->db_db_query($sql);

  $dec_SurveyID = $li->db_insert_id();

  $SurveyID = base64_encode($dec_SurveyID);

	$objIES = new libies();

	$survey_map = NULL;
	$answerStrTmp = array();

	//GET THE STUDENT EXISTING NO OF SURVEY FROM STORAGE
	$survey_map = $objIES->getStudentTaskSurvey($TaskID, $UserID);

	foreach($survey_map as $_surveyType => $_surveyIDAry){
		
		//$_surveyType --> 1, 2, 3 (TYPE OF THE SURVEY , eg Survey, Interview,Observe)
		//$_surveyIDAry --> 123, 234 (saved surveyID in t:IES_SURVEY for each type of survey)

		if($_surveyType == $SurveyType){
			//APPEN THE NEW CREATED SURVEY ID INTO THE END OF $_surveyIDAry[]
			$_surveyIDAry[] = $dec_SurveyID;
		}
		
		if(count($_surveyIDAry) >0){
			//FOR SAVE , REMOVE EMPTY ELEMENT IN THE $_surveyIDAry F: ARRAY_FILTER WITHOUT CALLBACK, DEFAULT REMOVE ANY "FALSE" ELEMENT IN THE ARRAY
			$_surveyIDAry = array_filter($_surveyIDAry);

			$_surveyIDStr = implode($ies_cfg["answer_within_question_sperator"] ,$_surveyIDAry);
			$answerStrTmp[] = $_surveyType.$ies_cfg["question_answer_sperator"].$_surveyIDStr;
		}
	}

	$answerStr = '';
	if(is_array($answerStrTmp ) && count($answerStrTmp) > 0){
		$answerStr = implode($ies_cfg["new_set_question_answer_sperator"],$answerStrTmp);
	}

  $sql  = 'INSERT INTO '.$intranet_db.'.IES_TASK_HANDIN ';
  $sql .= '(TaskID, UserID, Answer, QuestionType, DateInput, InputBy, ModifyBy)';
  $sql .= 'VALUES ';
  $sql .= '('.$TaskID.', '.$UserID.', \''.$answerStr.'\', \'survey:edit\', NOW(), '.$UserID.','.$UserID.')';
  $sql .= 'ON DUPLICATE KEY UPDATE Answer = \''.$answerStr.'\' , ModifyBy = '.$UserID;
  $li->db_db_query($sql);

  $msg = "add";
}

$url = "createForm.php?SurveyID={$SurveyID}&msg={$msg}";


intranet_closedb();
?>

<script language="JavaScript">

	var parentURL = window.opener.location.href;
	parentURL += '&defaultSurveyType=<?=$SurveyType?>';
	window.opener.location = parentURL;
	self.location = "<?=$url?>";
</script>
