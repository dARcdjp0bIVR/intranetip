<?php
/** [Modification Log] Modifying By: 
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");
intranet_opendb();

$objIES = new libies();

// Survey Detail
$dec_SurveyID = base64_decode($SurveyID);

if(is_numeric($dec_SurveyID))
{
  $survey_detail_arr = $objIES->getSurveyDetails($dec_SurveyID);
  
  if(count($survey_detail_arr) == 1 && is_array($survey_detail_arr)){
		$survey_detail_arr = current($survey_detail_arr);
  }

  $survey_title = $survey_detail_arr["Title"];
  $survey_disptitle = $survey_detail_arr["Title"];
  $survey_desc = $survey_detail_arr["Description"]; 
  $survey_question = $survey_detail_arr["Question"];
  $survey_startdate = date("Y-m-d", strtotime($survey_detail_arr["DateStart"]));
  $survey_enddate = date("Y-m-d", strtotime($survey_detail_arr["DateEnd"]));
  $survey_type = $survey_detail_arr["SurveyType"];

  $survey_typename = $survey_detail_arr["SurveyTypeName"];
  $survey_remark1 = $survey_detail_arr["Remark1"];
  $survey_remark2 = $survey_detail_arr["Remark2"];
  $survey_remark3 = $survey_detail_arr["Remark3"];
  $survey_remark4 = $survey_detail_arr["Remark4"];
}
else
{
  $survey_title = "";
  $survey_disptitle = $Lang['IES']['CreateNewQuestionnaire'];
  $survey_type = $SurveyType;
	if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
		$survey_disptitle = $Lang['IES']['CreateNewObserve'];
	}


  
  foreach($ies_cfg["Questionnaire"]["SurveyType"] AS $surveyTypeArr)
  {
    $_surveyTypeCode = $surveyTypeArr[0];
    $_surveyTypeName = $surveyTypeArr[1];
    
    if($_surveyTypeCode == $SurveyType)
    {
      $survey_typename = $_surveyTypeName;
      break;
    }
  }
}
//debug_r('type = '.$survey_type);
// End date for survey only (not for interview and field observation)
$show_enddate_input = ($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]);

// Post-action message
if(isset($msg))
{
  $html_alertMsg = libies_ui::getMsgRecordUpdated();
}

$questionTabCaption = ($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]) ? $Lang['IES']['QuestionnaireQuestion']: $Lang['IES']['QuestionnaireQuestionType2'];


// Tab Config
$TabMenuArr = array();
if($objIES->isIES_Teacher($UserID))
{
}
else
{
  $TabMenuArr[] = array("createForm.php?SurveyID={$SurveyID}", $Lang['IES']['BasicSettings']);
}
if(is_numeric($dec_SurveyID))
{
  $TabMenuArr[] = array("viewQue.php?key=".base64_encode("SurveyID={$dec_SurveyID}"), $questionTabCaption);
}
$html_tab_menu = libies_ui::GET_SURVEY_TAB_MENU($TabMenuArr, 0);

$html_ies_survey_body_class = libies_ui::getSurveyBodyClass($survey_type);

$linterface = new interface_html("ies_survey.html");
$linterface->LAYOUT_START();

$displayOtherRemarkInputFlag= true;
if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Survey"][0]){
	$displayOtherRemarkInputFlag = false;
}

$h_questionnaireTitle = $Lang['IES']['QuestionnaireTitle'];
if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Interview"][0]){
	$h_remarkCaption1 = $Lang['IES']['Questionnaire_InterviewRemark1'];
	$h_remarkCaption2 = $Lang['IES']['Questionnaire_InterviewRemark2'];
	$h_remarkCaption3 = $Lang['IES']['Questionnaire_InterviewRemark3'];
	$h_remarkCaption4 = $Lang['IES']['Questionnaire_InterviewRemark4'];
	$h_questionnaireTitle = $Lang['IES']['Questionnaire_InterviewTitle'];
}else if($survey_type == $ies_cfg["Questionnaire"]["SurveyType"]["Observe"][0]){
	$h_remarkCaption1 = $Lang['IES']['Questionnaire_ObserveRemark1'];
	$h_remarkCaption2 = $Lang['IES']['Questionnaire_ObserveRemark2'];
	$h_remarkCaption3 = $Lang['IES']['Questionnaire_ObserveRemark3'];
	$h_remarkCaption4 = $Lang['IES']['Questionnaire_ObserveRemark4'];
	$h_questionnaireTitle = $Lang['IES']['Questionnaire_ObserveTitle'];


}
?>

<form name="form1" method="POST" action="form_update.php">

  <div class="q_header">
    <h3>
      <span>&nbsp;<?=$survey_typename?>&nbsp;</span><br />
      <?=$survey_disptitle?>
    </h3>
    <?=$html_tab_menu?>
  
  </div><!-- q_header end -->
  
  <div class="q_content">
    <?=$html_alertMsg?>
  
    <table class="form_table">
      <col class="field_title" />
      <col  class="field_c" />
      <tr>
        <td><?=$h_questionnaireTitle?></td>
        <td>:</td>
        <td ><input type="text" name="title" value="<?=$survey_title?>" class="textbox" /></td>
      </tr>
  
      <tr>
        <td><?=$Lang['IES']['QuestionnaireDescription']?></td>
        <td>:</td>
        <td>
          <textarea name="description" style="width:100%; height:50px"><?=$survey_desc?></textarea>  
        </td>
      </tr>

<?
	if($displayOtherRemarkInputFlag){
?>

    <tr>
        <td><?=$h_remarkCaption1?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark1" value="<?=$survey_remark1?>" class="textbox" />
        </td>
      </tr>

    <tr>
        <td><?=$h_remarkCaption2?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark2" value="<?=$survey_remark2?>" class="textbox" />
        </td>
      </tr>


    <tr>
        <td><?=$h_remarkCaption3?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark3" value="<?=$survey_remark3?>" class="textbox" />
        </td>
      </tr>

    <tr>
        <td><?=$h_remarkCaption4?></td>
        <td>:</td>
        <td>
			<input type="text" name="remark4" value="<?=$survey_remark4?>" class="textbox" />
        </td>
      </tr>
<?
}	//close if($displayOtherRemarkInputFlag)
?>

<?php if($show_enddate_input) { ?>  
      <tr>
        <td><?=$Lang['IES']['QuestionnaireEnddate']?></td>
        <td>:</td>
        <td>
          <span class="row_content">
            <?=$linterface->GET_DATE_PICKER("enddate", $survey_enddate)?>
          </span>
        </td>
      </tr>
<?php } ?>
  
    </table>
  
    <!-- submit btn start -->
    <div class="edit_bottom"> 
      <p class="spacer"></p>
      
      <input type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'"  value=" <?=$Lang['IES']['Save']?> " />
      <input type="reset" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" value=" <?=$Lang['IES']['Reset']?> " />
      <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="window.close();" value=" <?=$Lang['IES']['Close']?> " />
      <p class="spacer"></p>
    </div>
    <!-- submit btn end -->
  </div> <!-- q_content end -->
  
  <input type="hidden" name="SurveyID" value="<?=$SurveyID?>" />
  <input type="hidden" name="SurveyType" value="<?=$SurveyType?>" />
  <input type="hidden" name="TaskID" value="<?=$TaskID?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>