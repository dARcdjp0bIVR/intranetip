<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");

intranet_auth();
intranet_opendb();

$objIES = new libies();


$layout_tmpl = ($objIES->isIES_Teacher($UserID) || $objIES->IS_ADMIN_USER($UserID)) ? $ies_cfg["PreviewFrontEndInterface"] : $ies_cfg["DefaultFrontEndInterface"];
$linterface = new interface_html($layout_tmpl);
$CurrentPage = "StudentProgress";

### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

//$scheme_arr = $objIES->GET_SCHEME_ARR();
$scheme_arr = $objIES->GET_USER_SCHEME_ARR($UserID);
$scheme_id = ($scheme_id == "") ? $scheme_arr[0]["SchemeID"] : $scheme_id;
//-- Start: Set Language
$schemeDetail = $objIES->GET_SCHEME_DETAIL($scheme_id);
$objIES->setSchemeLang($schemeDetail["Language"]);	// Put Lang To Session
include($intranet_root."/lang/ies_lang.".$_SESSION["IES_CURRENT_LANG"].".php");	// over
//echo libies_static::getThisSchemeLang();
//-- End: Set Language
$html_scheme_selection = libies_ui::GET_SCHEME_SELECTION($scheme_arr, $scheme_id,$objIES->isIES_Teacher($UserID));

$scheme_stage_arr = $objIES->getSchemeStage($scheme_id);
$table_content = "<table class=\"about_stage\">";
$table_content .= "<tr class=\"title\">";
$table_content .= "<td>".$Lang['IES']['Criteria']."</td>";
$table_content .= "<td>".$Lang['IES']['DateOfSubmission']."</td>";
$table_content .= "<td>&nbsp;</td>";
$table_content .= "</tr>";
for($i=0; $i<count($scheme_stage_arr); $i++)
{
  $t_stage_id = $scheme_stage_arr[$i]["StageID"];
  $t_stage_deadline = ($scheme_stage_arr[$i]["Deadline"] == "") ? "--" : $scheme_stage_arr[$i]["Deadline"];
  $t_stage_title = $scheme_stage_arr[$i]["Title"];

  $table_content .= "<tr class=\"s".($i+1)."\"><td>".$t_stage_title."</td><td>".$t_stage_deadline."</td><td>";
//	if($i == 0){
	  //hard core to open for stage on only first
		$table_content .= "<input type=\"button\" class=\"formbutton\" onClick=\"goToCoverPage({$scheme_id},{$t_stage_id})\" value=\" {$Lang['IES']['Enter']} \">";
//	}
//	else{
//	   $table_content .= "--";
//	}
  $table_content .= "</td></tr>";
}
$table_content .= "</table>";




$linterface->LAYOUT_START();
include_once("templates/index.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();



?>