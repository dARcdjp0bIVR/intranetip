<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_bibi.php");

intranet_auth();
intranet_opendb();

$info = $_POST['info'];
$type = $_POST['type'];

//DEBUG_R($info);
//DEBUG_R($_POST);

##$ies_cfg["new_set_question_answer_sperator"] = "@~@";  //eg Field @~@Field
##$ies_cfg["question_answer_sperator"] = "#~#";  //eg column %~% column
##$ies_cfg["comment_answer_sperator"] = "%~%"; //eg surename %~% initial
$libies_bibi = new libies_bibi();
$content = $libies_bibi->getContentString($info);

$result = $libies_bibi->updatebibi($content, $type, $UserID, $bibiId, $scheme_id, $todisplay);

$libies_bibi->addAnswer($scheme_id, $UserID, '1', 'reference');


if(!empty($bibiId))
	header("Location: addbibi.php?scheme_id={$scheme_id}&bibiId={$bibiId}&msg=update");
else{
	header("Location: addbibi.php?scheme_id={$scheme_id}&bibiId={$result}&msg=add");
}

intranet_closedb();
?>