<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "如何分析要點？";
$ies_lang["en"]["string1"] = "How to analyse the research findings?";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "跨單元分析";
$ies_lang["en"]["string2"] = "Cross-module analysis";
$_string2 = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "多角度分析";
$ies_lang["en"]["string3"] = "Multi-perspective analysis";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "持分者分析";
$ies_lang["en"]["string4"] = "Stakeholder analysis";
$_string4 = $ies_lang[$_lang]["string4"];

if($_lang == "b5"){
$S0001_Answer = <<<hhtml
同學盡量在撰寫分析時展示跨單元思考能力。雖然很多時我們的研究話題明顯是偏向某一至兩個單元，但也應盡量牽涉更多其它單元或學習範疇的相關概念。
hhtml;

$S0002_Answer = <<<hhtml
分析時盡量包含不同角度立論（例如：個人角度、社會角度、環境角度、政治角度、經濟角度、文化角度），使分析更全面、論點更多元化。
hhtml;

$S0003_Answer = <<<hhtml
通識專題研習的探究問題很多時涉及不同人物。不同持分者會因為個人利益、價值觀和態度的分歧產生衝突。撰寫分析時，同學可以考慮敘述不同持分者對問題的觀點，並結合研究所得（例如：深入訪問的訪問內容），分析有哪些重要因素形成這樣的觀點，或分析各個持分者的理據是否合理。
hhtml;

}else{
$S0001_Answer = <<<hhtml
It is preferable to demonstrate cross-module thinking skills when writing up the analysis. Although our enquiry topics usually focus on one to two modules only, we should incorporate as many as possible modules or related concepts in other areas of study.
hhtml;

$S0002_Answer = <<<hhtml
Different aspects should be considered in the argumentation (e.g. personal, social, environmental, political, economical and cultural) to make the analysis more comprehensive and the arguments more diversified.
hhtml;

$S0003_Answer = <<<hhtml
The enquiry topics in Liberal Studies usually involve many parties. Different personal interests, values and attitudes of various stakeholders may induce conflicts. You may consider describing the viewpoints of various stakeholders, then integrate the research findings (e.g. contents of in-depth interviews), analyse and find out the key factors leading to the viewpoint, or analyse the rationality of the argumentation of various stakeholders.
hhtml;

}
$S0001 = array($_string2, $S0001_Answer);
$S0002 = array($_string3, $S0002_Answer);
$S0003 = array($_string4, $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>