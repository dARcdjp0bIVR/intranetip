<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "立論的例子";
$ies_lang["en"]["string1"] = "An example of argumentation";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "議題：補習<br><b>從《學生補習行為研究問卷》得出 :</b>";
$ies_lang["en"]["string2"] = "Issue: Tutoring<br><b>The following data is obtained from ‘The survey on students’ tutoring behaviour’:</b>";
$subtitle = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "以下數據";
$ies_lang["en"]["string3"] = "The following data is collected:";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "方法一";
$ies_lang["en"]["string4"] = "Method 1";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "方法二";
$ies_lang["en"]["string5"] = "Method 2";
$_string5 = $ies_lang[$_lang]["string5"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<table style="width: 80%;" class="common_table_list">
  <tbody><tr>
    <th width="70%">(表一) 補習的主要原因	</th>
    <th>人數 (比例)</th>
  </tr>
  <tr>
    <td valign="top"><p>考取好成績<strong> </strong></p></td>
    <td valign="top"><p>40    (80%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>父母要求<strong> </strong></p></td>
    <td valign="top"><p>2    (4%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>朋輩影響<strong> </strong></p></td>
    <td valign="top"><p>6    (12%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>追求課堂以外知識<strong> </strong></p></td>
    <td valign="top"><p>2    (4%)</p></td>
  </tr>
  <tr>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>&nbsp;</p></td>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>共50 (100%)</p></td>
  </tr>
</tbody></table>
<br/>
<table style="width: 80%;" class="common_table_list">
  <tr>
    <th width="70%">(表二) 補習的學生認為補習最大的好處是： </th>
    <th>人數 (比例)</th>
  </tr>
  <tr>
    <td valign="top"><p>溫故知新 </p></td>
    <td valign="top"><p>5 (10%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>補習社能提供考試技巧<strong> </strong></p></td>
    <td valign="top"><p>35    (70%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>學習氣氛濃厚<strong> </strong></p></td>
    <td valign="top"><p>5    (10%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>補習導師能解答疑難<strong> </strong></p></td>
    <td valign="top"><p>4    (8%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>認識不同學校的學生 </p></td>
    <td valign="top"><p>1    (2%)</p></td>
  </tr>
  <tr>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>&nbsp;</p></td>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>共50 (100%)</p></td>
  </tr>
</table>
hhtml;

$S0002_Answer = <<<hhtml
<table class="sample_table">
  <tr>
    <td width="20%"><div class="method_board"><span class="m0001">論點</span></div> </td>
    <td width="8%"><div id="arrow_1" class="step_arrow"><a href="javascript:show_box(1)">&nbsp;</a></div></td>
    <td width="70%"><span id="box_1" style="visibility: hidden;" class="sample_box_m0001">學生補習主要是現今考試制度所造成的。</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0002">論據</span></div></td>
    <td><div id="arrow_2" class="step_arrow"><a href="javascript:show_box(2)">&nbsp;</a></div></td>
    <td><span id="box_2" style="visibility: hidden;" class="sample_box_m0002">能否在香港升學主要取決於公開試的成績。</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0003">論證</span></div></td>
    <td><div id="arrow_3" class="step_arrow"><a href="javascript:show_box(3)">&nbsp;</a></div></td>
    <td><span id="box_3" style="visibility: hidden;" class="sample_box_m0003">問卷調查結果顯示，八成受訪學生認同補習的主要原因是為了考取好成績。</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0004">反方意見/其他因素</span></div></td>
    <td><div id="arrow_4" class="step_arrow"><a href="javascript:show_box(4)">&nbsp;</a></div></td>
    <td><span id="box_4" style="visibility: hidden;" class="sample_box_m0004">當然也有兩成受訪者是基於其他因素而補習，例如：父母要求、朋輩影響、追求課堂以外知識，但比例不算多。</span></td>
  </tr>
</table>
hhtml;

$S0003_Answer = <<<hhtml
<table class="sample_table">
  <tr>
    <td width="20%"><div class="method_board"><span class="m0003">論證</span></div></td>
    <td width="8%"><div id="arrow_5" class="step_arrow"><a href="javascript:show_box(5)">&nbsp;</a></div></td>
    <td width="70%"><span id="box_5" style="visibility: hidden;" class="sample_box_m0003">問卷調查結果顯示，八成受訪學生認同補習的主要原因是為了考取好成績。</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0001">論點</span></div> </td>
    <td><div id="arrow_6" class="step_arrow"><a href="javascript:show_box(6)">&nbsp;</a></div></td>
    <td><span id="box_6" style="visibility: hidden;" class="sample_box_m0001">學生補習主要是現今考試制度所造成的。</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0002">論據</span></div></td>
    <td><div id="arrow_7" class="step_arrow"><a href="javascript:show_box(7)">&nbsp;</a></div></td>
    <td><span id="box_7" style="visibility: hidden;" class="sample_box_m0002">能否在香港升學主要取決於公開試的成績。</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0004">反方意見/其他因素</span></div></td>
    <td><div id="arrow_8" class="step_arrow"><a href="javascript:show_box(8)">&nbsp;</a></div></td>
    <td><span id="box_8" style="visibility: hidden;" class="sample_box_m0004">當然也有兩成受訪者是基於其他因素而補習，例如：父母要求、朋輩影響、追求課堂以外知識，但比例不算多。</span></td>
  </tr>
</table>
hhtml;

}else{
$S0001_Answer = <<<hhtml
<table style="width: 80%;" class="common_table_list">
  <tbody><tr>
    <th width="70%">(Table 1) Main reasons for tutoring</th>
    <th>No. of people (ratio)</th>
  </tr>
  <tr>
    <td valign="top"><p>Higher academic achievement<strong> </strong></p></td>
    <td valign="top"><p>40    (80%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Parents’ demand<strong> </strong></p></td>
    <td valign="top"><p>2    (4%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Peer influence<strong> </strong></p></td>
    <td valign="top"><p>6    (12%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Acquiring knowledge outside classroom<strong> </strong></p></td>
    <td valign="top"><p>2    (4%)</p></td>
  </tr>
  <tr>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>&nbsp;</p></td>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>Total: 50 (100%)</p></td>
  </tr>
</tbody></table>
<br/>
<table style="width: 80%;" class="common_table_list">
  <tr>
    <th width="70%">(Table 2)The major advantages of tutoring according to students attending tutorial classes:</th>
    <th>No. of people (ratio)</th>
  </tr>
  <tr>
    <td valign="top"><p>Gaining new insights through revision</p></td>
    <td valign="top"><p>5 (10%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Examination techniques are taught in tutorial classes<strong> </strong></p></td>
    <td valign="top"><p>35    (70%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Rich learning atmosphere<strong> </strong></p></td>
    <td valign="top"><p>5    (10%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Tutors can help solve students’ problems<strong> </strong></p></td>
    <td valign="top"><p>4    (8%)</p></td>
  </tr>
  <tr>
    <td valign="top"><p>Making friends with students from other schools </p></td>
    <td valign="top"><p>1    (2%)</p></td>
  </tr>
  <tr>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>&nbsp;</p></td>
    <td valign="top" style="border-top: 1px solid rgb(204, 204, 204);"><p>Total: 50 (100%)</p></td>
  </tr>
</table>
hhtml;

$S0002_Answer = <<<hhtml
<table class="sample_table">
  <tr>
    <td width="20%"><div class="method_board"><span class="m0001">Assertion</span></div> </td>
    <td width="8%"><div id="arrow_1" class="step_arrow"><a href="javascript:show_box(1)">&nbsp;</a></div></td>
    <td width="70%"><span id="box_1" style="visibility: hidden;" class="sample_box_m0001">Students going for tutoring is mainly a consequence of the present examination system.</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0002">Premise</span></div></td>
    <td><div id="arrow_2" class="step_arrow"><a href="javascript:show_box(2)">&nbsp;</a></div></td>
    <td><span id="box_2" style="visibility: hidden;" class="sample_box_m0002">Whether students can pursue further studies locally is mainly dependent on their public examination results.</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0003">Evidence</span></div></td>
    <td><div id="arrow_3" class="step_arrow"><a href="javascript:show_box(3)">&nbsp;</a></div></td>
    <td><span id="box_3" style="visibility: hidden;" class="sample_box_m0003">Findings of the survey show that 80% of the interviewees admit getting better grades is the main reason for tutoring.</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0004_eng">Counter-arguments/Other reasons</span></div></td>
    <td><div id="arrow_4" class="step_arrow"><a href="javascript:show_box(4)">&nbsp;</a></div></td>
    <td><span id="box_4" style="visibility: hidden;" class="sample_box_m0004">Of course there are 20% of the interviewees who have other reasons for tutoring, e.g. parents’ demand, peer influence and acquiring knowledge outside classroom. However, the proportion is not high.</span></td>
  </tr>
</table>
hhtml;

$S0003_Answer = <<<hhtml
<table class="sample_table">
  <tr>
    <td width="20%"><div class="method_board"><span class="m0003">Evidence</span></div></td>
    <td width="8%"><div id="arrow_5" class="step_arrow"><a href="javascript:show_box(5)">&nbsp;</a></div></td>
    <td width="70%"><span id="box_5" style="visibility: hidden;" class="sample_box_m0003">The survey findings show that 80% of the interviewees admit getting better grades is the main reason for tutoring.</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0001">Assertion</span></div> </td>
    <td><div id="arrow_6" class="step_arrow"><a href="javascript:show_box(6)">&nbsp;</a></div></td>
    <td><span id="box_6" style="visibility: hidden;" class="sample_box_m0001">Students going for tutoring is mainly a consequence of the present examination system.</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0002">Premise</span></div></td>
    <td><div id="arrow_7" class="step_arrow"><a href="javascript:show_box(7)">&nbsp;</a></div></td>
    <td><span id="box_7" style="visibility: hidden;" class="sample_box_m0002">Whether students can pursue further studies locally is mainly dependent on their public examination results.</span></td>
  </tr>
  <tr>
    <td><div class="method_board"><span class="m0004_eng">Counter-arguments/Other reasons</span></div></td>
    <td><div id="arrow_8" class="step_arrow"><a href="javascript:show_box(8)">&nbsp;</a></div></td>
    <td><span id="box_8" style="visibility: hidden;" class="sample_box_m0004">Of course there are 20% of the interviewees who have other reasons for tutoring, e.g. parents’ demand, peer influence and acquiring knowledge outside classroom. However, the proportion is not high.</span></td>
  </tr>
</table>
hhtml;

}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);
$S0003 = array($_string5, $S0003_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>
<script>
	function show_box(step){
		document.getElementById("arrow_"+step).className = "step_arrow_clicked";
		document.getElementById("box_"+step).style.visibility = "visible";
	}
</script>