<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "如何問對問題？";
$ies_lang["en"]["string1"] = "How to ask the right questions?";
$title = $ies_lang[$_lang]["string1"];


$ies_lang["b5"]["string2"] = "問卷的問題要設定得清晰準確。設定問題的常見錯誤如下，同學應引以為鑑：";
$ies_lang["en"]["string2"] = "Questions of the questionnaires should be set concisely and precisely . Note the following common error made when setting questions. Don’t repeat the same mistakes!";
$subtitle = $ies_lang[$_lang]["string2"];


$ies_lang["b5"]["string3"] = "典型錯誤（一）";
$ies_lang["en"]["string3"] = "Typical error 1";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "典型錯誤（二）";
$ies_lang["en"]["string4"] = "Typical error 2";
$_string4 = $ies_lang[$_lang]["string4"];


$ies_lang["b5"]["string5"] = "典型錯誤（三）";
$ies_lang["en"]["string5"] = "Typical error 3";
$_string5 = $ies_lang[$_lang]["string5"];


$ies_lang["b5"]["string6"] = "典型錯誤（四）";
$ies_lang["en"]["string6"] = "Typical error 4";
$_string6 = $ies_lang[$_lang]["string6"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<strong>雙重意義問題</strong><br /><br />
雙重意義問題，即是一條題目包含兩條問題。
<ul>
<u>錯誤例子</u><li>你認為政府是否需要全面立法禁止市民在戶外巴士站吸煙及即時對違法人士罰款？<br /><font color="red">評語：雙重意義，一條題目包含了兩條問題。</font></li>
</ul>

<ul>
<u>正確例子</u>
<li>你認為政府是否需要全面立法禁止市民在戶外巴士站吸煙？</li>
<li>你認為政府是否應該即時對違法人士罰款？
<br />
<font color="red">
評語：把具有雙重意義的問題拆開後，問題的焦點更清晰。
</font></li>
</ul>

hhtml;

$S0002_Answer = <<<hhtml
<strong>問題對被訪者要求太高</strong><br /><br />
1. 被訪者不是必須回答你的問卷。在過程中，他們需要付出時間和精神，所以設計問題時，不要對他們有過分要求。<br /></li>
<ul>
<u>錯誤例子</u><li>請列出你瀏覽社交網站時所進行的活動。<br /><font color="red">評語：在毫無提示的情況下，要求受訪者憑空想出自己的答案，要求的確太高。</font></li>
<br />
<u>正確例子</u><br />
<li>請選擇你瀏覽社交網站時所進行的活動。<br />
<input type="checkbox" DISABLED />玩遊戲<br />
<input type="checkbox" DISABLED />聯絡朋友<br />
<input type="checkbox" DISABLED />分享相片<br />
<input type="checkbox" DISABLED />寫日記<br />
<input type="checkbox" DISABLED />結交新朋友<br />
<input type="checkbox" DISABLED />其他：_______
</li>
</ul>

2. 當研究需要問及某些敏感的個人資料時（例如：收入水平、教育水平），被訪者可能不好意思直接回答，所以同學應該把這類問題放到問卷的最後部分，並把選項歸類，方便他們回答，也方便資料處理。
<ul>
<u>錯誤例子</u><br />
<li>請問你每月的收入是多少？ ____________</li><br />
<u>正確例子</u>
<li>以下那個選項最能反映你的收入水平？</li>
<input type="radio" DISABLED />$10000 或以下<br />
<input type="radio" DISABLED />$10001-$20000<br />
<input type="radio" DISABLED />$20001-$40000<br />
<input type="radio" DISABLED />$40001-$60000<br />
<input type="radio" DISABLED />$60001或 以上 <br />
hhtml;

$S0003_Answer = <<<hhtml
<strong>有預設立場的問題/ 引導性的問題</strong><br /><br />
<ul>
<u>錯誤例子</u><br />
<li>你是否認為在酷熱天氣下，政府應該豁免停車熄匙，以免職業司機和乘客在車內中暑？<br />
<input type="radio" DISABLED />是<br />
<input type="radio" DISABLED />否<br />
<input type="radio" DISABLED />不知道<br />
<font color="red">評語：題目本身有預設立場，引導讀者判斷，不夠客觀。</font>
</li><br />
<u>正確例子</u><br />
<li>你是否認為在酷熱天氣下，政府應該豁免停車熄匙？<br />
<input type="radio" DISABLED />是<br />
<input type="radio" DISABLED />否<br />
<input type="radio" DISABLED />不知道<br /></li>
</ul>

hhtml;

$S0004_Answer = <<<hhtml
<strong>用語模棱兩可</strong><br />
如何界定「經常」、「間中」、「有時」、「一般」、「好」、「不好」、「有效」、「沒效」？這些字詞的定義都是模棱兩可的，應避免使用。<br />
<ul>
<u>錯誤例子</u><br />
<li>在過去一星期，以下哪個選項最能描述你瀏覽社交網站Facebook的情況？<br />
<input type="radio" DISABLED />從不<br />
<input type="radio" DISABLED />間中<br />
<input type="radio" DISABLED />經常<br />
<font color="red">評語：選項用語比較主觀，如何量化「間中」及「經常」，實在因人而異。</font>
</li>
</ul>

<ul>
<u>正確例子</u><br />
<li>在過去一星期，你瀏覽了多少次社交網站Facebook？<br />
<input type="radio" DISABLED />0次<br />
<input type="radio" DISABLED />1-2次<br />
<input type="radio" DISABLED />3-4次<br />
<input type="radio" DISABLED />5-6次<br />
<input type="radio" DISABLED />7-8次<br />
<input type="radio" DISABLED />8次以上<br />
</li>
</ul>
hhtml;
}else{
$S0001_Answer = <<<hhtml
<strong>Double Barreled Question</strong><br /><br />
A Double Barreled Question is one that consists of more than one question. 
<ul>
<u>Bad example</u><li>Do you think the government should legislate for the prohibition of smoking at open-air bus-stops and issue a fixed penalty notice to the offender immediately?
<br /><font color="red">Comment: A double meaning question consisting of 2 inquiries. </font></li>
</ul>

<ul>
<u>Good example</u>
<li>Do you think the government should legislate for the prohibition of smoking at open-air bus-stops?</li>
<li>Do you think the government should issue a fixed penalty notice to the smoking offender immediately?
<br />
<font color="red">
Comment: Separate the double meaning question and the focus will be clearer.
</font></li>
</ul>

hhtml;

$S0002_Answer = <<<hhtml
<strong>The question asks for too much from the interviewees</strong><br /><br />
1. Research objects do not necessarily have to respond to the questionnaire. They have to spend time and effort in completing it. Therefore, you should not design questions which are too demanding for them. <br /></li>
<ul>
<u>Bad example</u><li>Please list all the activities you usually do when browsing the social networking site.<br /><font color="red">Comment: Given that there are no hints, the question is too demanding to require respondents to brainstorm their own answers from zero.</font></li>
<br />
<u>Good example</u><br />
<li>Please tick all the activities you usually do when browsing the social networking site.<br />
<input type="checkbox" DISABLED />Play games<br />
<input type="checkbox" DISABLED />Connect friends<br />
<input type="checkbox" DISABLED />Share photos<br />
<input type="checkbox" DISABLED />Update your blog<br />
<input type="checkbox" DISABLED />Make new friends<br />
<input type="checkbox" DISABLED />Others:_______
</li>
</ul>

2. If the research requires sensitive personal information like income and education levels, respondents may be embarrassed to give the direct answer. Therefore, you can put these questions in the final section of the questionnaire and categorise the possible options for ease of response and data processing.
<ul>
<u>Bad example</u><br />
<li>How much is your monthly salary? ____________</li><br />
<u>Correct example</u>
<li>Which of the following option can best reflect your income level?</li>
<input type="radio" DISABLED />$10000 or below<br />
<input type="radio" DISABLED />$10001-$20000<br />
<input type="radio" DISABLED />$20001-$40000<br />
<input type="radio" DISABLED />$40001-$60000<br />
<input type="radio" DISABLED />$60001 or above <br />
hhtml;

$S0003_Answer = <<<hhtml
<strong>Questions with preset stances or manipulating questions are set</strong><br /><br />
<ul>
<u>Bad example</u><br />
<li>Do you think the government should exempt vehicle drivers from switching off idling engines in hot weather, so as to prevent professional drivers and passengers suffering from heatstroke?<br />
<input type="radio" DISABLED />Yes<br />
<input type="radio" DISABLED />No<br />
<input type="radio" DISABLED />Not certain<br />
<font color="red">Comment: The question has a preset stance which will manipulate respondents’ thought. It is not objective enough.</font>
</li><br />
<u>Correct example</u><br />
<li>Do you think the government should exempt vehicle drivers from switching off idling engines in hot weather?<br />
<input type="radio" DISABLED />Yes<br />
<input type="radio" DISABLED />No<br />
<input type="radio" DISABLED />Not certain<br /></li>
</ul>

hhtml;

$S0004_Answer = <<<hhtml
<strong>Ambiguous wording</strong><br />
How to define ‘always’, ‘occasionally’, ‘sometimes’, ‘generally’? The definitions of ‘good’, ‘bad’, ‘effective’ and ‘ineffective’ are all ambiguous. You should avoid using them.<br />
<ul>
<u>Bad example</u><br />
<li>Which of the following option can best describe how frequently you browsed Facebook last week?<br />
<input type="radio" DISABLED />Never<br />
<input type="radio" DISABLED />Sometimes<br />
<input type="radio" DISABLED />Always<br />
<font color="red">Comment: The wordings of the options are quite subjective. How do you quantify ‘sometimes’ and ‘always’? The definition varies with people.</font>
</li>
</ul>

<ul>
<u>Good example</u><br />
<li>Which of the following options can best describe how frequently you browsed Facebook last week?<br />
<input type="radio" DISABLED />0  time<br />
<input type="radio" DISABLED />1-2 time(s)<br />
<input type="radio" DISABLED />3-4 times<br />
<input type="radio" DISABLED />5-6 times<br />
<input type="radio" DISABLED />7-8 times<br />
<input type="radio" DISABLED />8 times or above<br />
</li>
</ul>
hhtml;
}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);		
$S0003 = array($_string5, $S0003_Answer);
$S0004 = array($_string6, $S0004_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,	
				   "S0004" => $S0004,
			);

$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>