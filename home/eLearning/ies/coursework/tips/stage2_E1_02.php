<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$_lang = libies::getThisSchemeLang();

$libies = new libies();

$ies_lang["b5"]["string1"] = "問卷的設計";
$ies_lang["en"]["string1"] = "The design of a questionnaire";
$title = $ies_lang[$_lang]["string1"];


$ies_lang["b5"]["string2"] = "第一步";
$ies_lang["en"]["string2"] = "Step 1";
$_string2 = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "第二步";
$ies_lang["en"]["string3"] = "Step 2";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "第三步";
$ies_lang["en"]["string4"] = "Step 3";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "第四步";
$ies_lang["en"]["string5"] = "Step 4";
$_string5 = $ies_lang[$_lang]["string5"];


$ies_lang["b5"]["string6"] = "第五步";
$ies_lang["en"]["string6"] = "Step 5";
$_string6 = $ies_lang[$_lang]["string6"];

$subtitle = "";


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<strong>確定需要甚麼資料去回答研究問題</strong><br /><br />
<ul>
<li>在設計問卷的時候，要確保所問的問題能充分回應研究題目、研究目的和焦點問題，否則可能會偏離或遺漏研究焦點而被扣分。</li>
<li>要充分理解研究的對象，所問的問題要切合他們的能力與身份（例如：若受訪者教育水平不高，問卷用詞便不能太深）。</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<strong>確定使用哪一種問卷研究方法</strong><br /><br />
<ul>
<li>問卷研究方法也可以細分為：街頭問卷訪問、網上問卷訪問、電話問卷訪問……</li>
<li>每種方法各有利弊，採用哪一種方法則要視乎使用哪一種方法最能接觸到研究對象。此外，還要考慮回答率和回答質量。</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<strong>細心評估每一條問題的質量</strong><br /><br />
<ul>
<li>當撰寫完所有問卷題目後，我們就要進入編輯問卷的階段，把問題稍為修改，同時把不必要的問題刪去。</li>
<li>當我們審視每一條問題時，可以問自己：<br />
<input type="checkbox" DISABLED>這條問題是否真的必不可少？<br />
<input type="checkbox" DISABLED>這條問題能取得甚麼有用的資料？<br />
<input type="checkbox" DISABLED>這條問題能回應哪一條焦點問題？<br />
<input type="checkbox" DISABLED>這條問題的用詞是否適合研究對象的身份？
</li>
</ul>
hhtml;

$S0004_Answer = <<<hhtml
<strong>確定問題的次序</strong><br /><br />
<ul>
<li>起首的問題應該盡量簡單和較少爭議性。</li>
<li>起首的問題可以用來過濾訪問對象。</li>
<li>應該先問最基本的資料，然後再深入探討每一個變項。由廣闊到聚焦，由淺入深。</li>
<li>敏感或涉及個人私隱的題目應盡量放在問卷的較後部分。</li>
</ul>
hhtml;

$S0005_Answer = <<<hhtml
<strong>最後檢查與測試問卷</strong><br /><br />
<ul>
<li>在正式分發問卷收集數據前，自己先要試答一遍，看看有沒有錯漏。</li>
<li>同時設想自己是被訪者，看看這樣的訪問流程是否讓人感到自然。</li>
<li>檢視問題的數量及用詞是否適切。</li>
<li>回頭看看焦點問題、研究目的等重要綱領，檢查問卷是否已經涵蓋所有焦點問題，確保沒有遺漏。</li>
<li>若能及早發現問題，及時修正問題，蒐集數據時定能事半功倍。一份有效的問卷通常要經過多番修改的。</li>
</ul>
hhtml;
}else{
$S0001_Answer = <<<hhtml
<strong>What information will you need to answer the research question?</strong><br /><br />
<ul>
<li>When designing the questionnaires, make sure the questions set should be able to address the topic for enquiry, enquiry objectives and focus questions. Otherwise, marks will be deducted for digression or missing any of the focuses.</li>
<li>You should have a thorough understanding of your research objects. The register of the questions should be appropriate and should cater for their ability and social status. For instance, if the education level of the interviewee is rather low, the diction / wording used in the questionnaire should not be too complex.</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<strong>In what way do you want to use the questionnaire?</strong><br /><br />
<ul>
<li>There are a number of ways in which a questionnaire can be delivered: face-to-face interview on the street, online questionnaire, telephone interview.</li>
<li>Each of the above methods has its own strengths and weaknesses. Research object accessibility, response rate and the quality of responses are the determining factors for the method to use. </li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<strong>Assess the quality of each question carefully.</strong><br /><br />
<ul>
<li>When you have finished writing up the questions for the questionnaire, refine them and delete those unnecessary ones. </li>
<li>You can ask yourself the following questions:<br />
<input type="checkbox" DISABLED>Is this question essential?<br />
<input type="checkbox" DISABLED>What useful information does this question help to obtain?<br />
<input type="checkbox" DISABLED>Which focus questions does this question respond to?<br />
<input type="checkbox" DISABLED>Does the diction/wording used in this question match the social status of the research object?
</li>
</ul>
hhtml;

$S0004_Answer = <<<hhtml
<strong>Decide on the order of the questions.</strong><br /><br />
<ul>
<li>The beginning questions should be simpler and less disputable.</li>
<li>The beginning questions can be used to screen interviewees.</li>
<li>The questionnaire should begin with questions asking about basic information, and then proceed to the enquiry of each variable. Questions should be set in this way, from broad to focused and easy to difficult. </li>
<li>Sensitive questions or questions involving personal data should be placed at a later section of the questionnaire.</li>
</ul>
hhtml;

$S0005_Answer = <<<hhtml
<strong>Lastly, review and test the questionnaire. </strong><br /><br />
<ul>
<li>Try to answer all the questions once before distributing the questionnaire and collecting data to see whether any mistakes are made.</li>
<li>Imagine you are an interviewee. See if the flow of questions set is natural. </li>
<li>Check if the number of questions and the use of diction are appropriate. </li>
<li>Double-check whether the questions have covered all focus questions, enquiry objectives, etc. Ensure you have not missed any of the important points.</li>
<li>If you can identify problems as early as possible and rectify them, it will save you time and effort in collecting data. A valid questionnaire usually has undergone several rounds of revisions.</li>
</ul>
hhtml;
}
$S0001 = array($_string2, $S0001_Answer);
$S0002 = array($_string3,  $S0002_Answer);		
$S0003 = array($_string4, $S0003_Answer);
$S0004 = array($_string5, $S0004_Answer);
$S0005 = array($_string6, $S0005_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,	
				   "S0004" => $S0004,
				   "S0005" => $S0005,
			);
$arrow_image = $image_path."/2009a/ies/student/orange_arrow2.gif";

$html_class = "five_example";
$html_answer = $libies->getTipsAnswerDisplayWithArrow($nr_array, $arrow_image);

include "templates/general_tip.tmpl.php";

intranet_closedb();
?>