<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "如何撰寫研究設計？";
$ies_lang["en"]["string1"] = "How to write up the research design?";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "撰寫研究設計時，你需要說明下列各項：";
$ies_lang["en"]["string2"] = "When writing up the research design, you have to explain the following items:";
$subtitle = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "研究對象與抽樣方法";
$ies_lang["en"]["string3"] = "Research object(s) and sampling method";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "研究程序";
$ies_lang["en"]["string4"] = "Research process";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "研究工具";
$ies_lang["en"]["string5"] = "Research tool";
$_string5 = $ies_lang[$_lang]["string5"];

$ies_lang["b5"]["string6"] = "研究日誌";
$ies_lang["en"]["string6"] = "Research diary";
$_string6 = $ies_lang[$_lang]["string6"];

if($_lang == "b5"){
$S0001_Answer = <<<hhtml
例子：
<br><br>
是次研究在我校（XX中學）進行，採用了網上問卷調查的方法研究，共訪問了90名中四至中七級同學，男女比例各佔一半。
hhtml;

$S0002_Answer = <<<hhtml
例子：
<br><br>
本人採用了學校內聯網eClass，在中四至中六各級隨機抽出30人，男女比例各佔一半，並發送問卷及邀請被訪者自願填寫。所有問卷於2010年3月1日發出，回應率為100%，而且全部為有效問卷。
hhtml;

$S0003_Answer = <<<hhtml
例子：
<br><br>
本研究以問卷為研究工具，每份問卷共設問題15題，以多項選擇題及短答題為主。正式發出前，已經過3次測試，並根據收集到的老師與同學意見，加以改良後才正式發出。問卷詳見附件一。(附件一為當時使用的問卷)
hhtml;

$S0004_Answer = <<<hhtml
例子：<br>
<table class=common_table_list>
	<tr>
    	<th>日期	</th>
    	<th>工作進度</th>
  	</tr>
  	<tr>
    	<td>1/12/2010- 1/1/2011</td>
    	<td>設計問卷</td>
  	</tr>
</table>
hhtml;

}else{
$S0001_Answer = <<<hhtml
Example:
<br><br>
This enquiry was carried out in my school (XX Secondary School) using internet surveys as the enquiry method. 90 Secondary 4 to 7 students were interviewed with equal opportunities between both genders.
hhtml;

$S0002_Answer = <<<hhtml
Example:
<br><br>
30 Secondary 4 to 6 students were selected by random sampling through the Intranet (eClass) in my school, both genders in equal numbers. Questionnaires were sent out on 1st March 2010 for the interviewees to complete voluntarily. Both the response rate and validity were 100%.
hhtml;

$S0003_Answer = <<<hhtml
Example:
<br><br>
Questionnaire is the major research tool of my enquiry. Each one contains 15 questions of mainly multiple choice and short answer types. Before formal distribution, the questionnaire was pre-tested 3 times and modified with full reference to teacher and peer opinions. Please refer to Appendix 1 for the questionnaire used.
hhtml;

$S0004_Answer = <<<hhtml
Example:<br>
<table class=common_table_list>
	<tr>
    	<th>Date</th>
    	<th>Work progress</th>
  	</tr>
  	<tr>
    	<td>1/12/2010- 1/1/2011</td>
    	<td>Questionnaire design</td>
  	</tr>
</table>
hhtml;

}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);
$S0003 = array($_string5, $S0003_Answer);
$S0004 = array($_string6, $S0004_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
				   "S0004" => $S0004,
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>