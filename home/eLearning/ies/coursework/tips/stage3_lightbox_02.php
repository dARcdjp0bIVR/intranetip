<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "「緒論」寫作步驟例子";
$ies_lang["en"]["string1"] = "Examples of ‘Preface’";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "題目例子 :";
$ies_lang["b5"]["string2"] = "題目例子 :";
$subtitle = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "我校大規模推行電子學習的機遇與挑戰";
$ies_lang["en"]["string3"] = "Opportunities and challenges in extensive promotion of eLearning in my school.";
$_string3 = $ies_lang[$_lang]["string3"];


$ies_lang["b5"]["string4"] = "本港低下階層看最低工資立法-以生活素質角度探究";
$ies_lang["en"]["string4"] = "Viewpoints of the working class on minimum wage legislation ── in terms of quality of life.";
$_string4 = $ies_lang[$_lang]["string4"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
寫作步驟：
  <br>
  <ol>
  	<li>描述電子學習在香港的趨勢（描述大趨勢、大環境、客觀現象）</li>
	<li>描述傳統教育的限制</li>
	<li>電子學習的優點和功效 （補足傳統教育的不足）</li>
	<li>我校推行電子學習的現況</li>
	<li>帶出研究目的：探究大規模推行電子學習的可行性，當中的機遇與挑戰，持分者看法等等</li>
	<li>給大規模和電子化教育下定義 （也可選擇在「文獻回顧」才下定義）</li>
	<li>焦點問題</li>
    </ol>

hhtml;

$S0002_Answer = <<<hhtml
寫作步驟：
  <br>
  <ol>
  	<li>描述社會各界討論最低工資立法的情況</li>
	<li>簡述立法原意（保障低收入人士）或簡述社會各界看法</li>
	<li>描述低收入階層的特徵與面對的困境</li>
	<li>帶出研究目的：透過研究，了解他們的觀點與看法、提供建議給政府完善立法</li>
	<li>給低收入人士、生活素質等關鍵詞語下定義（也可選擇在「文獻回顧」才下定義）</li>
	<li>焦點問題</li>

    </ol>
    
hhtml;

}else{
$S0001_Answer = <<<hhtml
Writing procedures：
  <br>
  <ol>
  	<li>Describe the trend of eLearning in Hong Kong (describe the current trend, scenario and objective phenomena).</li>
	<li>Describe the constraints of traditional education.</li>
	<li>Give the benefits and efficacies of eLearning (to supplement the inadequacies of traditional education).</li>
	<li>Explain the current situation at your school in implementing eLearning.</li>
	<li>Bring out the enquiry objectives: To examine the feasibility of extensive implementation of eLearning, the subsequent opportunities and challenges and the viewpoints of various stakeholders, etc.</li>
	<li>Define ‘extensive implementation’ and ‘eLearning’ (or define the terms in ‘Literature Review’).</li>
	<li>Focus questions</li>
    </ol>

hhtml;

$S0002_Answer = <<<hhtml
Writing procedures：
  <br>
  <ol>
  	<li>Describe ongoing discussions among various sectors in the society on minimum wage legislation.</li>
	<li>Briefly explain the rationale for the legislation (protecting the low-income group) or briefly explain the viewpoints of various sectors in the society.</li>
	<li>Describe the typical situations and adversities of the low-income group.</li>
	<li>Bring out the enquiry objectives: To understand the situation of the low-income group through the enquiry and make suggestions for the government to complete the legislation.</li>
	<li>Define ‘low-income group, ‘quality of life’ and other key words (you may define the terms in ‘Literature Review’).</li>
	<li>Focus questions</li>
  </ol>
    
hhtml;

}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002
			);
$html_class  = "two_example";
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>