<?
//================================ Content ==================================
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/IES.js"></script>

<script>
var last = "";

function reheight(){

	//var IE = /*@cc_on!@*/false;
	/*
	var WholeHeight = $(document).height();
	var topHeight = $("#top_content").outerHeight();

	var top = (!IE) ? parseInt($("#show_nr").css("margin-top")) : parseInt($("#show_nr").css("margin-top")) * -1;
	var padding = (!IE) ? parseInt($("#show_nr").css("padding-top")) +  parseInt($("#show_nr").css("padding-bottom")): 0;
	var theHeight = WholeHeight - topHeight - top - padding;
	*/
	//HARD CODE TO RETURN 300 HEIGHT
	theHeight = 300;
	return theHeight;
}


$(document).ready(function() {
	<?
    	$firstContentArray = $nr_array['S0001'];
		$firstContent = '';
		$_temp = explode("\n", $firstContentArray[1]);
		foreach($_temp as $_line){
			$firstContent .= trim($_line);
		}		
		echo 'var firstContent = "'.addslashes($firstContent)."\";\n";
	?>

	
	
	$('#show_nr').html(firstContent);
	$('#S0001').addClass('S0001_current');
	last = 'S0001';

    var bottomHeight = reheight();
	$('#show_nr').css('height', bottomHeight);

	$('.sw_click').click(function(){

		bottomHeight = reheight();
		$('#show_nr').css('height', bottomHeight);

    	if(last == ""){
//    		bottomHeight = reheight();
//			$('#show_nr').css('height', bottomHeight);
    	}
    	if(last != ""){
    		$('#' + last).removeClass(last + '_current');
    	}
    	last = $(this).attr('id');
    	$(this).addClass($(this).attr('id') + '_current');
    	var content= '';
    	<?
    	if(is_array($nr_array)){
    		foreach($nr_array as $key=>$value){
    			//addslashes(nl2br($value[1]))
    			$w_string_array = explode("\n", $value[1]);
    			$w_string = "";
    			if(is_array($w_string_array)){
    				foreach($w_string_array as $a_string){
    										
    					$w_string .= trim($a_string);
    				}
    				
    			}
    			echo "if(last == '{$key}'){";
    			echo "content = \"".addslashes($w_string)."\"; }";
    		}
    	}
    	?>
    	
    	$('#show_nr').html(content);
    	
    });
});
</script>
<style>
.five_example .narrow_sample  {width: 17%}
</style>


<body style="overflow:hidden;">

<div class="narrow_range_box <?=$html_class?>"> <!-- add class three_example -->
	<div id='top_content'>
		<h2><?=$title?></h2>	
		<a href="javascript:self.parent.tb_remove();" class="IES_tool_close" title="關閉">&nbsp;</a>
		<h3><?=$subtitle?></h3>
		<?=$html_answer?>
		<div class="clear"></div>
	</div>
	<div class="narrow_range_bottom" id="show_nr" style="text-align:left; padding-left:35px; font-size:15px;">
	</div>
	<div class="clear"></div>
	
</div> <!-- end of .narrow_range_box -->   
</body>
