<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "反思寫作方法";
$ies_lang["en"]["string1"] = "Reflection writing method";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "第一段";
$ies_lang["en"]["string2"] = "Paragraph 1";
$_string2 = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "第二段";
$ies_lang["en"]["string3"] = "Paragraph 2";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "第三段";
$ies_lang["en"]["string4"] = "Paragraph 3";
$_string4 = $ies_lang[$_lang]["string4"];

$ies_lang["b5"]["string5"] = "第四段";
$ies_lang["en"]["string5"] = "Paragraph 4";
$_string5 = $ies_lang[$_lang]["string5"];

if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<strong>反思知識上的提升</strong>
<br><br>
<ul>
	<li>對所研究的議題多了認識</li>
	<li>對某概念多了認識</li>
	<li>對不同持分者的價值觀有更深入的認識</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<strong>反思技巧上的提升 </strong>
<br><br>
<ul>
	<li>選擇研究方法的技巧</li>
	<li>事前準備功夫</li>
	<li>設定與測試研究工具的技巧</li>
	<li>搜集數據的技巧</li>
	<li>數據整合的技巧</li>
	<li>多角度分析的技巧</li>
	<li>建議技巧</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<strong>反思價值觀上的提升</strong>
<br><br>
<ul>
	<li>人際關係</li>
	<li>探究的態度</li>
	<li>對知識的態度</li>
</ul>
hhtml;

$S0004_Answer = <<<hhtml
<strong>總結以上三方面的反思</strong>
<br><br>
<ul>
	<li>複述各段重點</li>
</ul>
hhtml;

}else{
$S0001_Answer = <<<hhtml
<strong>Improvement in knowledge</strong>
<br><br>
<ul>
	<li>Understanding of the enquiry issue</li>
	<li>Understanding of certain concepts</li>
	<li>Understanding of the values of various stakeholders</li>
</ul>
hhtml;

$S0002_Answer = <<<hhtml
<strong>Improvement in skills</strong>
<br><br>
<ul>
	<li>Skill of research method selection</li>
	<li>Preparation work</li>
	<li>Skill of research tool setting and examination</li>
	<li>Skill of data collection</li>
	<li>Skill of data integration</li>
	<li>Skill of multiple-perspective analysis</li>
	<li>Skill of recommendation</li>
</ul>
hhtml;

$S0003_Answer = <<<hhtml
<strong>Improvement in values</strong>
<br><br>
<ul>
	<li>Interpersonal relationship</li>
	<li>Attitude towards enquiry</li>
	<li>Attitude towards knowledge</li>
</ul>
hhtml;

$S0004_Answer = <<<hhtml
<strong>Summary of the reflection on the three aspects above</strong>
<br><br>
<ul>
	<li>Restate the main points of the above paragraphs</li>
</ul>
hhtml;

}
$S0001 = array($_string2, $S0001_Answer);
$S0002 = array($_string3, $S0002_Answer);
$S0003 = array($_string4, $S0003_Answer);
$S0004 = array($_string5, $S0004_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002,
				   "S0003" => $S0003,
				   "S0004" => $S0004
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>