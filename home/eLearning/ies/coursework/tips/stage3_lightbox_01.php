<?php
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "「焦點問題」與整個研究的關係";
$ies_lang["en"]["string1"] = "The relationship between the ‘focus questions’ and the study ";
$title = $ies_lang[$_lang]["string1"];


$html_class = "two_example";
$html_answer = "";
$flash_link = "/images/2009a/ies/student/stage3_A_".$_lang;
$flash_content = "<script type=\"text/javascript\">
					AC_FL_RunContent( 'codebase','http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0','width','780','height','400','src','$flash_link','quality','high','pluginspage','http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash','movie','$flash_link' ); //end AC code
				  </script>
				  <noscript>
					<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=9,0,28,0\" width=\"780\" height=\"400\">
  						<param name=\"movie\" value=\"$flash_link.swf\"/>
  						<param name=\"quality\" value=\"high\" />
  						<embed src=\"$flash_link.swf\" quality=\"high\" pluginspage=\"http://www.adobe.com/shockwave/download/download.cgi?P1_Prod_Version=ShockwaveFlash\" type=\"application/x-shockwave-flash\" width=\"780\" height=\"400\"></embed>
					</object>
				  </noscript>";

include "templates/flash_tip.tmpl.php";		

intranet_closedb();
?>
