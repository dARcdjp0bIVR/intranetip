<?
$PATH_WRT_ROOT = "../../../../../";
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/ies/libies.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$_lang = libies::getThisSchemeLang();

$libies = new libies();


$ies_lang["b5"]["string1"] = "寫作小貼士";
$ies_lang["en"]["string1"] = "Writing hints";
$title = $ies_lang[$_lang]["string1"];

$ies_lang["b5"]["string2"] = "用詞與語調︰";
$ies_lang["en"]["string2"] = "Wording and tone:";
$subtitle = $ies_lang[$_lang]["string2"];

$ies_lang["b5"]["string3"] = "措詞技巧";
$ies_lang["en"]["string3"] = "Diction";
$_string3 = $ies_lang[$_lang]["string3"];

$ies_lang["b5"]["string4"] = "具體應用";
$ies_lang["en"]["string4"] = "Application";
$_string4 = $ies_lang[$_lang]["string4"];


if($_lang == "b5"){
$S0001_Answer = <<<hhtml
<b>學術寫作的用詞宜小心謹慎，同一個信息，不同的語氣和表達方式，得出的結果差別可以很大。</b>
<br>
<u class="eg_title">例子1</u>
<p class="spacer"></p>
<span class="narrow_r" style="color:#2286C5;">最低工資可能使低下階層求職更困難。</span>
<span class="narrow_q">(較不肯定，只是提出一個可能性。）</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">最低工資會使低下階層求職更困難。 </span>
<span class="narrow_q">(較肯定，需要很強的證據去證實此論點。）</span>
<br><br>
<u class="eg_title">例子2</u>
<p class="spacer"></p>
<span class="narrow_r" style="color:#2286C5;">研究結果支持了剛才的假設。</span>
<span class="narrow_q"> （語氣最肯定，可見作者對自己的推論充滿信心。）</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">研究結果可以支持剛才的假設。</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">研究結果可能支持剛才的假設。</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">研究結果應該可以支持剛才的假設。</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">相信研究結果應該可以支持剛才的假設。</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">本人相信研究結果或許可以支持剛才的假設。</span>
<span class="narrow_q"> （語氣最不肯定，可見作者對自己的推論並沒有信心。）</span>
hhtml;

$S0002_Answer = <<<hhtml
一般情況下，撰寫學術文章時很少會使用過份肯定的詞語，例如：必定、絕對、肯定。
可是，在某些情況下，筆者會刻意採用較肯定的語氣，有時則不然。要用甚麼詞語或語氣來表達意思，則視乎情況而定，不能一概而論：
<br><br>
<b>情況一：在描述研究結果時，可使用較肯定的語氣。這樣的表達予人可靠、踏實的感覺。</b>
<br><br>
<u class="eg_title">例子1</u>
<p class="spacer"></p>
從上述圖表應該可以看出受訪者普遍支持小班教學。
<br>
<span class="tabletextrequire">評語：作者用不肯定的語氣描述研究所得，顯示對研究結果信心不足，難以說服讀者。</span>
<br><br>
<u class="eg_title">例子2</u>
<p class="spacer"></p>
上述圖表顯示受訪者普遍支持小班教學。
<br>
<span class="tabletextrequire">評語：語氣較肯定，顯示作者對自己的研究結果充滿信心，較能說服讀者。</span>
<br><br>
<b>情況二：在分析數據、推論影響、提出建議時，則不宜採用過份肯定或武斷的語氣。</b>
<br><br>
<u class="eg_title">例子1</u>
<p class="spacer"></p>
根據研究顯示，大部分受訪教育界人士、學生和家長都支持小班教學。本報告建議政府應考慮盡快落實此政策，否則會引起教育界和社會強烈不滿。<br>
<span class="tabletextrequire">評語：作者用比較強硬的語氣推論有可能出現的結果，用語上不太合適。畢竟，上述後果只是「有機會發生」，但我們不能確定事件「必定會發生」，所以在這裡使用過分肯定的語氣，容易予人不夠客觀的感覺。</span>
<br><br>
<u class="eg_title">例子2</u>
<p class="spacer"></p>
根據研究顯示，大部分受訪教育界人士、學生和家長都支持小班教學。本報告建議政府應考慮在資源許可的情況下盡快落實此政策，否則可能會引起教育界和社會的不滿。
<br>
<span class="tabletextrequire">評語：作者用比較溫和的語氣推論事件的後果，用語較為謹慎，同時照顧到「資源分配」此反方論點。 顯示作者明白「可能性」與「必然性」的分別，也明白到研究本身存在局限，未必能完全反映事實。相比例子一，例子二予人較客觀踏實的感覺。</span>
hhtml;

}else{
$S0001_Answer = <<<hhtml
<b>Phrasing and diction has to be handled with care in academic writing. The same message presented in different tones and expressions can have very diverse outcomes.</b>
<br>
<u class="eg_title">Example 1</u>
<p class="spacer"></p>
<span class="narrow_r" style="color:#2286C5;">The minimum wage may make job hunting more difficult for the lower class.</span>
<span class="narrow_q"> (Not quite sure, just bringing out a possibility.)</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">The minimum wage will make job hunting more difficult for the lower class.</span>
<span class="narrow_q"> (Quite sure, strong premise required for this assertion.)</span>
<br><br>
<u class="eg_title">Example 2</u>
<p class="spacer"></p>
<span class="narrow_r" style="color:#2286C5;">The research outcomes support the hypothesis.</span>
<span class="narrow_q"> (The tone is certain, which demonstrates the full confidence of the writer in his/her inference.)</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">The research outcomes can support the hypothesis.</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">The research outcomes may support the hypothesis.</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">The research outcomes should be able to support the hypothesis.</span>
<br>
<br>
<span class="narrow_r" style="color:#2286C5;">I believe the research outcomes may probably support the hypothesis.</span>
<span class="narrow_q"> (The tone is the most uncertain, which demonstrates the writer’s lack of confidence in his/her inference.)</span>
hhtml;

$S0002_Answer = <<<hhtml
Generally speaking, an over-definite tone is seldom used in academic writing, e.g. definitely, absolutely, surely. 
However, under certain situations, the writer will use a more definite tone intentionally. What phrasing or tone is used depends on the situation and cannot be generalised.
<br><br>
<b>Case 1: A more definite tone can be used to describe the research outcomes to ensure they are reliable and dependable.</b>
<br><br>
<u class="eg_title">Example 1</u>
<p class="spacer"></p>
The graph above should show that the interviewees support small class teaching in general.
<br>
<span class="tabletextrequire">Comment: The writer using an indefinite tone to describe the research findings indicates that he/she is not confident about them, which makes it hard to convince the readers.</span>
<br><br>
<u class="eg_title">Example 2</u>
<p class="spacer"></p>
The graph above shows that the interviewees support small class teaching in general.
<br>
<span class="tabletextrequire">Comment: A more definite tone indicates that the writer is confident about the research findings, which makes it easier to convince the readers.</span>
<br><br>
<b>Case 2:  It is inappropriate to use an over-definite or arbitrary tone when analysing data, deducing consequences or making suggestions.</b>
<br><br>
<u class="eg_title">Example 1</u>
<p class="spacer"></p>
According to the research outcomes, most of the educators, students and parents interviewed support small class teaching. This report suggests that the government should implement the policy as soon as possible, otherwise strong discontent among educators and the public will arise.<br>
<span class="tabletextrequire">Comment: The writer uses a strong tone to infer possible consequences, which is not very appropriate. After all, the consequences mentioned are only ‘possible’, not ‘definite’. Therefore, the degree of objectivity will be affected if the tone is too strong/over-definite.</span>
<br><br>
<u class="eg_title">Example 2:</u>
<p class="spacer"></p>
According to the research outcomes, most of the educators, students and parents interviewed support small class teaching. This report suggests that the government should implement the policy at their earliest possibility with resources available, otherwise strong discontent among educators and the public may arise.
<br>
<span class="tabletextrequire">Comment: The writer uses a more moderate tone and deliberate wording to infer the consequences of the issue, while at the same time considering ‘resource allocation’ as the counter-argument. This indicates that the writer can distinguish ‘possibility’ from ‘definiteness’, and recognises the limitations of the enquiry in fully reflecting the reality. Compared to Example 1, Example 2 is more objective and dependable.</span>
hhtml;

}
$S0001 = array($_string3, $S0001_Answer);
$S0002 = array($_string4, $S0002_Answer);

##the key must be s000X, X which is a int
$nr_array = array ("S0001" => $S0001,
				   "S0002" => $S0002
			);

$html_class  = $libies->getTipsClass($nr_array);
$html_answer = $libies->getTipsAnswerDisplay($nr_array);

include "templates/general_tip.tmpl.php";		

intranet_closedb();
?>
<style type="text/css">
a {color:#2286C5;}
</style>