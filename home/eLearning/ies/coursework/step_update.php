<?php
# Modifying By : Max
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
intranet_auth();
intranet_opendb();

$li = new libdb();

//debug_r($question_actual_answer);
$li->Start_Trans();


//IN CASE STUDENT DOES NOT INPUT / REMOVE SOME SELECTION TYPE ANS , REMOVE ALL THE ANS FIRST (UPDATE IF THERE ARE NO ANY INPUT)
$sql = "UPDATE {$intranet_db}.IES_QUESTION_HANDIN ";
$sql .= "SET Answer = NULL, AnswerActualValue = NULL, AnswerExtra = NULL ";
$sql .= "WHERE StepID = {$step_id} AND UserID = '{$UserID}'";
$res[] = $li->db_db_query($sql);

//AVOID the error IF THERE IS NO $question variable though form submit
if(is_array($question) && sizeof($question) > 0 )
{
	foreach($question AS $questionType => $questionArr)
	{
	  switch($questionType)
	  {
		case "text":
		case "textarea":
		  foreach($questionArr AS $questionCode => $answer)
		  {
			$sql = "INSERT INTO {$intranet_db}.IES_QUESTION_HANDIN (StepID, UserID, QuestionType, QuestionCode, Answer, AnswerActualValue, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES ('{$step_id}', '{$UserID}', '{$questionType}', '{$questionCode}', '".mysql_real_escape_string(stripslashes($answer))."','".mysql_real_escape_string(stripslashes($answer))."', now(), '{$UserID}', '{$UserID}') ";
			$sql .= "ON DUPLICATE KEY UPDATE Answer = VALUES(Answer), AnswerActualValue = VALUES(AnswerActualValue),ModifyBy = VALUES(ModifyBy), QuestionType = VALUES(QuestionType)";

			$res[] = $li->db_db_query($sql);
		  }
		  break;
		case "select_mod":
		  foreach($questionArr AS $questionCode => $answer)
		  {
			$_actualValue = "";  //FOR STORING THE ACTAUL HTML DISPLAY VALUE / MEANING OF THE CHECK BOX

			foreach($answer as $answerKey => $studentAnswerValue)
			{
			  // MAP THE ACTUAL VALUE FROM STUDENT SELECTED VALUE OF THE CHECKBOX AND APPEND A SPERATOR TO IT
			  $_actualValue .= $answer_text["select_mod"][$questionCode][$studentAnswerValue].$ies_cfg["question_answer_sperator"];
			}

			//REMOVE THE LAST OCCURRENCE OF THE SPERATOR
			$_actualValue = substr($_actualValue,0, 0 - intval(strlen($ies_cfg["question_answer_sperator"])));

			$sql = "INSERT INTO {$intranet_db}.IES_QUESTION_HANDIN (StepID, UserID, QuestionType, QuestionCode, Answer, AnswerActualValue,DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES ('{$step_id}', '{$UserID}', '{$questionType}', '{$questionCode}', '".mysql_real_escape_string(stripslashes(implode($ies_cfg["question_answer_sperator"], $answer)))."', '".mysql_real_escape_string(stripslashes($_actualValue))."', now(), '{$UserID}', '{$UserID}') ";
			$sql .= "ON DUPLICATE KEY UPDATE Answer = VALUES(Answer), AnswerActualValue = VALUES(AnswerActualValue),ModifyBy = VALUES(ModifyBy), QuestionType = VALUES(QuestionType)";
	//echo $sql;
			$res[] = $li->db_db_query($sql);
		  }

		  break;
		case "select_mod_custom":
		  foreach($questionArr AS $questionCode => $answer)
		  {
			$sql = "INSERT INTO {$intranet_db}.IES_QUESTION_HANDIN (StepID, UserID, QuestionType, QuestionCode, AnswerExtra, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES ('{$step_id}', '{$UserID}', 'select_mod', '{$questionCode}', '".mysql_real_escape_string(stripslashes(implode($ies_cfg["question_answer_sperator"], $answer)))."', now(), '{$UserID}', '{$UserID}') ";
			$sql .= "ON DUPLICATE KEY UPDATE AnswerExtra = VALUES(AnswerExtra), ModifyBy = VALUES(ModifyBy), QuestionType = VALUES(QuestionType)";
	//echo $sql;
			$res[] = $li->db_db_query($sql);
		  }
		  break;
		case "radio":

		  foreach($questionArr AS $questionCode => $answer)
		  {

			$temp_ans =mysql_real_escape_string(stripslashes($answer));
			list($ans_part, $comment_part) = explode($ies_cfg["comment_answer_sperator"],$temp_ans);

			$sql = "INSERT INTO {$intranet_db}.IES_QUESTION_HANDIN (StepID, UserID, QuestionType, QuestionCode, Answer, AnswerActualValue, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES ('{$step_id}', '{$UserID}', '{$questionType}', '{$questionCode}', '".$ans_part."','".$comment_part."', now(), '{$UserID}', '{$UserID}') ";
			$sql .= "ON DUPLICATE KEY UPDATE Answer = VALUES(Answer), AnswerActualValue = VALUES(AnswerActualValue),ModifyBy = VALUES(ModifyBy), QuestionType = VALUES(QuestionType)";

			$res[] = $li->db_db_query($sql);

		  }
		  break;
		 case "table":
		  foreach($questionArr AS $questionCode => $answer)
		  {
			$sql = "INSERT INTO {$intranet_db}.IES_QUESTION_HANDIN (StepID, UserID, QuestionType, QuestionCode, Answer, AnswerActualValue, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES ('{$step_id}', '{$UserID}', '{$questionType}', '{$questionCode}', '".mysql_real_escape_string(stripslashes($answer))."','".mysql_real_escape_string(stripslashes($answer))."', now(), '{$UserID}', '{$UserID}') ";
			$sql .= "ON DUPLICATE KEY UPDATE Answer = VALUES(Answer), AnswerActualValue = VALUES(AnswerActualValue),ModifyBy = VALUES(ModifyBy), QuestionType = VALUES(QuestionType)";

			$res[] = $li->db_db_query($sql);

		  }
			break;
	  }
	}
}
# get the id of record that being modified
$sql = "SELECT DateModified FROM {$intranet_db}.IES_QUESTION_HANDIN WHERE StepID = '{$step_id}' AND UserID = '{$UserID}'";
$returnVector = $li->returnVector($sql);
$date = $returnVector[0];
//$date = date("d-m-Y",strtotime($returnVector[0]));

$final_res = (count($res) == count(array_filter($res)));
if($final_res)
{
  $li->Commit_Trans();
  $msg = "update";
}
else
{
  $li->RollBack_Trans();
  $msg = "update_failed";
}

intranet_closedb();

# Output the modified date to the page
header("Content-Type:   text/xml");
$libies = new libies();
$XML = $libies->generateXML(
					array(
						array("date", $date)
					)
				);
echo $XML;
?>