<?php
/**
 * Modifying By: Max
 */
/**************************************************
 * For last submission of task guide
 **************************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
intranet_auth();
intranet_opendb();

$lies = new libies();

// Get step handin which are to be transferred to task according to setting
$sql =  "
          SELECT
            handin.Answer,
			handin.QuestionType
          FROM
            {$intranet_db}.IES_STEP step
          INNER JOIN {$intranet_db}.IES_QUESTION_HANDIN handin
            ON step.StepID = handin.StepID
          WHERE
            step.TaskID = {$task_id} AND
            step.SaveToTask = 1 AND
			handin.UserID = {$UserID}
          ORDER BY
            step.Sequence
        ";
$resultArray = $lies->returnArray($sql);
$handin_arr = array();
$type_arr = array();
//debug_r($sql);
if (count($resultArray)>0) {
	foreach($resultArray as $key => $element) {
		$handin_arr[] = $element["Answer"];
		$type_arr[] = strtoupper($element["QuestionType"]);
	}
}

if (in_array("TABLE",$type_arr)) {
	$task_type = "table";
} else {
	$task_type = "textarea";
}
//debug_r($task_type);die;
// Concat task content from several step
$task_content = implode("\n", $handin_arr);

$lies->Start_Trans();
$res = $lies->UPDATE_TASK_CONTENT($task_id, $task_content, $task_type, $UserID);
if($res)
{
  $lies->Commit_Trans();
//  echo "Saved";
}
else
{
  $lies->RollBack_Trans();
//  echo "Not Saved";
}

intranet_closedb();
?>