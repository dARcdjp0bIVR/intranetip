<?

if(empty($stage_id) || empty($scheme_id))
{
	echo "Error !<br/>";
	echo "<!--Invalide access , Empty STAGE ID or SCHEME ID -->";
    exit();
}
?>
<script language="JavaScript">
// function for going to specific scheme
/*
function doScheme(jParSchemeID){
  document.form1.scheme_id.value = jParSchemeID;
  document.form1.action = "index.php";
  document.form1.submit();
}
*/
$(document).ready(function(){
	
	//show hide scheme option					   
	$("ul.scheme_option").hide(); 
	$("a.scheme_select").click( 
    function () {
      $("ul.scheme_option").slideToggle("normal");
      return false;
    }
	);

	//selection box action			   
  $('.selectAnsList ul li').toggle(
    function(event) {
      $(this).addClass('active');  
    },
    function(event) {
      $(this).removeClass('active');
    }
  );
	
	//student tool
  $('a.reflect_note').toggle(
    function(event) {
      $('.IES_sheet_wrap').css('padding-right', '320px');
      $('.IES_right_tool').css('display', 'block');
      $('li.reflect_note').addClass('current');
    },
    function(event) {
      $('.IES_sheet_wrap').css('padding-right', '1px');
      $('.IES_right_tool').css('display', 'none');
      $('li.reflect_note').removeClass('current');
    }
  );

});
</script>

<form name="form1" method="POST" action="stage.php">

<div class="<?=$h_frameClass?>"><div class="theme"> <!-- stage 1 background -->

<div class="top_left_btn"> <!-- logo , scheme, stage btn -->
  <div class="IES_logo"><div><a href="javascript:doScheme(<?=$scheme_id?>)">&nbsp;</a></div></div> <!-- IES logo -->
  <?=$html_scheme_selection?>
  <a href="#" class="scheme_select" title="<?=$Lang['IES']['ChooseOtherScheme']?>">&nbsp;</a>
  <?=$html_stage_selection?>
  <!--ul class="stage">
    <li class="current"><a href="#" class="s1">階段 I</a></li>
    <li class="line"></li>
    <li><a href="#" class="s2">階段 II</a></li>
    <li class="line"></li>
    <li><a href="#" class="s3">階段 III</a></li>
  </ul-->
</div> <!--class "top_left_btn" end -->

<div class="clear"></div>
<!-- start content -->
  <?=$h_content?>
<!-- end content -->

<!-- IES_sheet end-->
          
<div class="clear"></div>

<input type="hidden" name="stage_id" value="<?=$stage_id?>" />
<input type="hidden" name="scheme_id" value="<?=$scheme_id?>" />
</form>