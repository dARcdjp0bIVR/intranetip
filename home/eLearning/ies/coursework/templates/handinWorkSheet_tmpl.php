<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>eClass IP 2.5 :: IES ::</title>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>

<link href="css/text" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/content_25.css" rel="stylesheet" type="text/css">
 <link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES.css" rel="stylesheet" type="text/css">
 <link href="<?=$PATH_WRT_ROOT?>templates/2009a/css/IES_font_L.css" rel="stylesheet" type="text/css">
 <link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<script>
<!--
$(document).ready(function() {
	$("form[name=upload_form]").submit(function(){
//		$("#fileUploadTd").hide();
//		$("#loadingImage").show();
	})
	uploadMoreFile();
});
function uploadMoreFile() {

	var cloneItem = $("#submitFile").clone().removeAttr("id").css("display","").appendTo($("#submitFileSpan"));
	$("#submitFileSpan").append("<br/>");
}

/* object debug */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}
function stopUpload_remove(ParResultArray) {

	return ;
	var failList = "";
	

	var fileIdArray = new Array();
	$.each(ParResultArray, function(key, element) {
		if (element["UPLOAD_RESULT"] == "1") {
			fileIdArray.push(element["FILEID"]);
		} else {
			failList = failList + "," + element["FILENAME"];
		}
	});
	
	$.ajax({
    url:      "ajax/ajax_load_file_detail_by_fileIds.php",
    type:     "POST",
    data:     "fileIdArray="+fileIdArray+"&requestedField=FILEID,FILENAME,FILEHASHNAME",
    async:	  false,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(xml){
	    		$files = $(xml).find("files");
	    		$.each($files,function(){
	    			$file = $(this).find("file");
	    			$.each($file,function(){
		    			var fileId = $(this).find("FILEID").text();
		    			var fileName = $(this).find("FILENAME").text();
		    			var fileHashName = $(this).find("FILEHASHNAME").text();
		    			$("#IES_upload_list").prepend("<li><a href=\"download.php?fileId="+fileId+"\" style=\"float: left;\">"+fileName+"</a><a href=\"#\" class=\"cancel_btn\" title=\"刪除\" onClick=\"$(this).parent().remove();setRemove("+fileId+")\">&nbsp;</a><br class=\"clear\"></li>");
	    			});
	    		});
	    		$("#removeFile").val("");
	    		$("form[name=upload_form]").each(function(){
	    			this.reset();
	    		});
              }
	});
	$("#loadingImage").hide();
	$("#fileUploadTd").show();
}
function setRemove(ParFileId) {
	var targetRemove = document.getElementById('removeFile');
	if (targetRemove.value == "") {
		targetRemove.value = targetRemove.value+ParFileId;
	} else {
		targetRemove.value = targetRemove.value+","+ParFileId;
	}	
}
-->
</script>
</head>

<body>
<div class="IES_worksheet IES_lightbox">
	<h4>上傳檔案</h4>
  <form target="" name="upload_form" action="" method="post" enctype="multipart/form-data" >
  
  <div class="edit_task_pop_board_write standard_height">
  <table class="form_table IES_table">
      <tr>
        <td>檔案</td>
        <td>:</td>
<!--       <td id="loadingImage" style="display:none"><?=$loadingImg?></td>
        <td id="fileUploadTd">
        <ul class="IES_upload_list" id="IES_upload_list">
        <li>
        	<span id="submitFileSpan">
		    <input type="file" name="submitFiles[]" id="submitFile" style="display: none"/>
		    <input type="hidden" name="removeFiles" id="removeFile" />
		    </span>
	        <iframe id="upload_target" name="upload_target" src="#" style="width:0;height:0;border:1px solid #fff;"></iframe>
		</li>
        </ul>
        <a href="javascript: void(0);" onClick="uploadMoreFile()";>上載更多檔案</a>
        
        </td>-->
      <td><?=$html_upload?></td>
      </tr>

      <col class="field_title" />
      <col  class="field_c" />
    </table>
  </div>
    <div class="IES_lightbox_bottom">
     <!-- <input name="submit2" type="submit" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" 儲存 " /> -->
	  <input name="submit2" type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onclick="javascript:window.parent.tb_remove()" value=" 返回 " />
	</div>
	
	</form>
</div>

</body>
</html>