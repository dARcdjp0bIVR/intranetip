<?php 
/** [Modification Log] Modifying By:Max 
 * *******************************************
 * *******************************************
 */
 if($scheme_id == "")
{
	echo "schemeID not exist. Program exist<br/>";	
	exit();
}
if($stage_id == "")
{
	echo "stageID not exist. Program exist<br/>";	
	exit();
}
?>

<script language="javascript" type="text/javascript">
<!--
var jsFileArray;	// This global variable is used to store the file details returned by ajax
var fileAdded;	// This global variable is used to control the display of submission button

$(document).ready(function () {
	InitGlobalVariables();
	loadHandinBlock("current");
});

function InitGlobalVariables() {
	jsFileArray = new Array();
	fileAdded = 0;
}


function loadHandinBlock(ParType) {
			InitGlobalVariables();	// Everytime swap pages need to clear the global variables, else, the data will be accumulated
			$.ajax({
			    url:      "../ajax/ajax_load_handintask_block.php",
			    type:     "POST",
			    data:     "loadType="+ParType+"&stage_id=<?=$stage_id?>&UserID=<?=$UserID?>",
			    async:	  false,
			    error:    function(xhr, ajaxOptions, thrownError){
			                alert(xhr.responseText);
			              },
			    success:  function(xml){
                      result = $(xml).find("result").text();
                      jsResult = $(xml).find("jsFileArray").text();                      
                      allowJsRemoveOn = $(xml).find("js_allow_remove_on").text();
                      if (result) {
                        $("div[id=handin_block]").html(result);
                        //////////////////////////////////////////////////////
                        //This part is used to handle the Array get from JS
                        $("div[id=forJS]").html(jsResult);
                        var jsFileArrayFromPHP;
                        eval($("div[id=forJS]").html());
                        if (jsFileArrayFromPHP) {
                        	jsFileArray = jsFileArrayFromPHP;
                        }
                        //////////////////////////////////////////////////////
                        
                        loadFilesDisplay(allowJsRemoveOn);
                        initializeSubmissionButton();
                      }
                      else {
                        alert("init block failed");
                      }
			    	}
			});
}
function initializeSubmissionButton() {
	if (window.jsFileArray && jsFileArray.length) {
			fileAdded = jsFileArray.length;
	}
	if (fileAdded>0) {
		$targetButton = $("#studentSubmission");
		$targetButton.css("display",'');
		
		$targetButton.click(function() {
			$.ajax({
			    url:      "../ajax/ajax_student_submission.php",
			    type:     "POST",
			    data:     "stage_id="+<?=$stage_id?>+"&user_id="+<?=$UserID?>,
			    async:	  false,
			    error:    function(xhr, ajaxOptions, thrownError){
			                alert(xhr.responseText);
			              },
			    success:  function(xml){
			    			  result = $(xml).find("result").text();
			    			  if (result) {
			    			  	location.reload(true);
			    			  } else {
			    			  	alert("Submission Failed");
			    			  }

			              }
			});
		});
	}
}

function startUpload(){
	  if (!$("input[name=submitFile]").val()) {
	  	alert("Please Select A File");
	  	return false;
	  }
	  $("#f1_upload_process").css("display", '');
	  $("div[class=submission_upload]").css("display", 'none');
      return true;
}

function stopUpload(success, file_id, file_name, file_hash_name, date_modified){
//alert("success : "+success+", file_id: "+file_id+",file_name"+file_name+",date_modified: "+date_modified);
	  $("#f1_upload_process").css("display", 'none');
	  $("div[class=submission_upload]").css("display", '');
	  
	  
	  if (success) {
	  	addFileDisplay(file_id, file_name, file_hash_name, date_modified, true);
	  	
	  	// if it change from no files to 1 file, add the submission button
	  	if (!fileAdded++) {
	  		initializeSubmissionButton();
	  	}
	  }
	  $("form[id=uploadForm]")[0].reset();
      return true;
}
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}
function loadFilesDisplay(ParAllowJsRemove) {
	if (window.jsFileArray && jsFileArray.length) {
		$.each(jsFileArray, function(key, element) {
//			addFileDisplay(element["WorksheetFileID"], element["FileName"], element["FileHashName"], element["DateModified"],ParAllowJsRemove);
			addFileDisplay(element["FILEID"], element["FILENAME"], element["FILEHASHNAME"], element["DATEMODIFIED"],ParAllowJsRemove);
		});
	}
}

function addFileDisplay(file_id, file_name, file_hash_name, date_modified, is_removable) {
	var removeBtn = "";
	if (Number(is_removable)) {
		removeBtn = "<a href=\"javascript: void(0);\" id=\"remove_"+file_id+"\" class=\"deselect\" title=\"<?=$Lang['IES']['Delete']?>\" onClick=\"javascript: removeFile(this);\">&nbsp;<\/a>";
	}
    var displayFileHtml = "<div class=\"IES_file_list\"><span><a href=\"../download.php?fhn="+file_hash_name+"\">"+file_name+"</a><br \/><span class=\"update_record\">("+date_modified+")<\/span><\/span>"+removeBtn+"<div class=\"clear\"><\/div>	<\/div>";
    $(displayFileHtml).insertBefore($("div[id=submission_upload]"));
}

function removeFile(obj) {
	var confirmDelete = confirm("<?=$Lang['IES']['AreYouSureYouWouldLikeToDeleteThisFile']?><?=$Lang['_symbol']["questionMark"]?>");
	if (confirmDelete) {

		remove_id = $(obj).attr("id");
		
		$target = $(obj).parent();
		$target.hide();
		
		file_id = (remove_id.split("_"))[1];
		
		
		$.ajax({
		    url:      "../ajax/ajax_remove_handin_files.php",
		    type:     "POST",
		    data:     "file_id="+file_id,
		    async:	  false,
		    error:    function(xhr, ajaxOptions, thrownError){
		                alert(xhr.responseText);
		              },
		    success:  function(xml){
		    			  result = $(xml).find("result").text();
		    			  if (result) {
		    			  	$target.remove();
		    			  	fileAdded--;
		    			  	if (fileAdded == 0) {
		    			  		$("#studentSubmission").hide();
		    			  	}
	//	    			  	alert("remove success");
		    			  } else {
		    			  	$target.show();
		    			  	alert("remove failed");
		    			  }
		              }
		});
	}
}
function doStage(scheme_id, stage_id) {
	document.form1.scheme_id.value = scheme_id;
	document.form1.stage_id.value = stage_id;
	document.form1.action = "stage.php";
	document.form1.submit();
}

//-->
</script>
<div class="IES_sheet"> <!-- no need change anything for the action of 反思手記 and FAQ -->
	<!-- ###### submission page ###### -->
	<div class="IES_submission">
		<div class="submit_top">
			<div class="submit_top_inner">
				<div class="IES_submission_title">
					<span class="title"><?=$html["stage_title"]?></span>
					<span class="deadline"><?=$Lang['IES']['SubmissionDeadline']?><?=$Lang['_symbol']["colon"]?> <?=$html_StageDeadLine?></span>
					<!-- top right btn start #### added 07-06-2010 #### -->
					<div class="IES_top_tool">
					<!--	<a href="#" class="font_S" title="">&nbsp;</a>
						<a href="#" class="font_L_current" title="">&nbsp;</a>
						<div class="v_line"></div>-->
						<a href="javascript: doStage(<?=$scheme_id?>,<?=$stage_id?>);" class="submission" title=""><span><?=$Lang['IES']['BackToHomepage']?></span></a>
					</div>
					<!-- top right btn end -->
					<div class="clear">
					</div>
				</div><!-- end of submit_top_inner !-->
			</div>
		</div> <!-- end of class submit_top -->
		<div id="forJS" style="display:none">
		</div><!-- this block is used to catch AJAX returned js -->
		<div id="handin_block">
		</div> <!-- This block is used to display the content switch by ajax -->	
		<div class="submit_bottom">
			<div></div>
		</div>
	</div>
	<!-- ###### submission page end ###### -->
	<form name="form1" method="POST">
		<input type="hidden" value="<?=$scheme_id?>" name="scheme_id"/>
		<input type="hidden" value="<?=$stage_id?>" name="stage_id"/>
	</form>
</div>
<!-- IES_sheet end-->