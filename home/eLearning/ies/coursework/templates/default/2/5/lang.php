<?php
$ies_lang["b5"]["string1"] = " 說明了研究方法和步驟後，我們需要解釋選擇背後的原因。";
$ies_lang["en"]["string1"] = "State the rationale for choosing the research tool(s) / method(s)
";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "第二階段的其中一個評核重點是要測試同學是否能深入了解各種研究方法的利弊，並懂得如何應用。為了展示你的判斷能力， 你必須在這部分解釋剛才所選擇的方法背後的原因。你可以與其他研究方法互相比較，突出你所選的方法有何優勝之處。";
$ies_lang["en"]["string2"] = "One of the major assessment items at Stage 2 is to test if students can fully understand the pros and cons of various research methods, and know their applications. To demonstrate your good sense of judgement, you should justify the rationale for choosing your tool(s) / method(s). To highlight the point that the strengths of your chosen method(s) outweigh those of the others, you may have to compare your method or tool with the others.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "寫作方法：";
$ies_lang["en"]["string3"] = "Writing guides:";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "你希望在過程中搜集到甚麼資料？";
$ies_lang["en"]["string4"] = "What information would you like to collect?";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "為甚麼你認為所選擇的方法能幫你搜集到你想要的資料？";
$ies_lang["en"]["string5"] = "Why do you think the chosen method(s) can help collect the necessary information?";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "與其他方法比較，你所選的探究方法有何優勝之處？";
$ies_lang["en"]["string6"] = "Comparing with the other method(s), in what way does your chosen method outweigh the others?";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "相信你考慮採用所選的研究方法時，必定考慮過以下的準則。這些要點有助你解釋選擇研究方法背後的理念：";
$ies_lang["en"]["string7"] = "You should have considered the following criteria before opting for your research method(s). These key points are important for you to explain the rationales behind your option(s).";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "實行成本";
$ies_lang["en"]["string8"] = "Implementation cost";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "研究對象的回應率";
$ies_lang["en"]["string9"] = "Response rate";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "數據的質量";
$ies_lang["en"]["string10"] = "Data quality";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "問題的複雜性";
$ies_lang["en"]["string11"] = "Problem complexity";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "能否接觸到目標人物";
$ies_lang["en"]["string12"] = "Target accessibility";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "要比較不同研究方法，請參考：";
$ies_lang["en"]["string13"] = "To compare different research methods, refer to the Pros and Cons of Common Research Methods.";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "作答模式一";
$ies_lang["en"]["string14"] = "Sample answer 1";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "我將會透過 (問卷調查 / 深入訪問 / 實地考察)，搜集到有關(研究問題)的資料，用以 (證明某些論據 / 達到某些目的 / 研究目的)(加以解釋）。";
$ies_lang["en"]["string15"] = "(Survey / In-depth interview / Field observation) will be conducted to collect data relevant to (the research question) to (prove certain evidences / achieve certain purposes / achieve the research objectives)(with explanation).";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "與 (問卷調查 / 深入訪問 / 實地考察) 的研究方法相比，(剛才選擇的方法) 更 (能收集到有用的數據 / 能深入了解探究問題 / 能多角度了解不同持分者的觀點)。";
$ies_lang["en"]["string16"] = "Compared to (Survey / In-depth interview / Field observation), (the chosen research method) can (collect more useful data / dig up the research issue / learn the viewpoints of different stakeholders from various perspectives).";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "作答模式二";
$ies_lang["en"]["string17"] = "Sample answer 2";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/

$ies_lang["b5"]["string18"] = "在第二階段，我需要蒐集有關（能回應焦點問題的資料），這些第一手資料都不能在現有的文獻中獲取，而透過（剛才選取的研究方法），我不但能獲得（能回應焦點問題的資料），而且相對其他方法，採用（剛才選取的研究方法）有以下優勢，包括…… (加以解釋）。";
$ies_lang["en"]["string18"] = "(Data which is useful to answer the research question) should be collected at Stage 2. (The chosen method) is adopted because first-hand information cannot be obtained from existing literature. Compared to other research methods, (the chosen method) has the following strengths: (…with explanation).";
/*
<?=$ies_lang[$_lang]["string18"]?>
*/

$ies_lang["b5"]["string19"] = "你可參考上述兩種作答模式，解釋你選擇研究方法背後的理念：";
$ies_lang["en"]["string19"] = "Justify your rationale for selecting the present research method(s). You may refer to the above sample answers.";
/*
<?=$ies_lang[$_lang]["string19"]?>
*/

$ies_lang["b5"]["string20"] = "各種常用研究方法的利弊";
$ies_lang["en"]["string20"] = "The Pros and Cons of Common Research Methods";
/*
<?=$ies_lang[$_lang]["string20"]?>
*/

?>