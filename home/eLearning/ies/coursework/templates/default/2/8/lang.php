<?php
$ies_lang["b5"]["string1"] = "當老師核准了你的研究工具後，你便可開始收集數據，並整理數據結果。";
$ies_lang["en"]["string1"] = "After your teacher has approved your research tool(s), you can start collecting and organising data.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "在這部分，你可檢視之前設定的問卷調查(如有)、深入訪問(如有)、實地考察(如有)的問題。";
$ies_lang["en"]["string2"] = "In this section, you can view the questions you have set in the questionnaire, in-depth interview record form and field observation record form you have created, if applicable.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "若你採用問卷調查的研究方法，收集數據的方式有兩種：";
$ies_lang["en"]["string3"] = "If you have opted for survey as your research method, data can be collected in these 2 ways:";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "經本系統發送電郵給研究對象。";
$ies_lang["en"]["string4"] = "Email your research objects the questionnaire you have set through our system.";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "列印問卷，分發至研究對象。";
$ies_lang["en"]["string5"] = "Print out the questionnaire and distribute the hard copies to your research objects.";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "若你決定進行深入訪問或實地考察，請預先列印有關工具，在訪談或觀察時記錄要點，以便稍後整理。";
$ies_lang["en"]["string6"] = "If you have opted for in-depth interview or field observation, please print out the corresponding research tools for jotting notes in the interview or field observation. This facilitates data organisation afterward.";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/


?>