<?php
$ies_lang["b5"]["string1"] = "完成第一階段後，相信老師已給了你一些評語和建議。如有需要，你現在可以根據這些評語，稍為修改題目：";
$ies_lang["en"]["string1"] = "Teachers might have already given you some comments and suggestions on your completion of Stage 1. If necessary, fine tune the title with reference to these comments.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

//$ies_lang["b5"]["string2"] = "如你需要按建議修改題目，可在此輸入。如不需要修改，可留空此欄︰";
//$ies_lang["en"]["string2"] = "Enter a new title here if revision is made. Otherwise, just leave the blank empty.";

$ies_lang["b5"]["string2"] = "如需要按建議修改題目，可在此輸入；如不需要修改，請複製以上的題目並在下方貼上：";
$ies_lang["en"]["string2"] = "Revise your title with reference to the recommendations if necessary. Otherwise, copy the title above and paste it in the box below."; 

/*
<?=$ies_lang[$_lang]["string2"]?>
*/

?>