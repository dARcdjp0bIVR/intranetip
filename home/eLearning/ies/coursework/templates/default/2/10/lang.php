<?php

$ies_lang["b5"]["string1"] = "在第二階段完結前，你需要反思整個蒐集資料的過程，寫下你在第二階段學到了甚麼。";
$ies_lang["en"]["string1"] = "Before the end of Stage 2, reflect on the data collection process and write down what you have learnt in the process.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "例子一";
$ies_lang["en"]["string2"] = "Example 1";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "總括來說，我在這個階段學到：";
$ies_lang["en"]["string3"] = "To conclude, at Stage 2 I have learnt:";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "如何按題目的特質，選擇合適的研究方法。";
$ies_lang["en"]["string4"] = "how to select appropriate research method based on the characteristics of my topic for enquiry.";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "比較各種研究方法的利弊，作出理性的抉擇。";
$ies_lang["en"]["string5"] = "how to compare the pros and cons of various research methods and opt for an appropriate method rationally.";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "如何有條不紊地記錄數據和整存資料，方便日後使用。";
$ies_lang["en"]["string6"] = "how to record and store data systematically for ease of future use.";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "如何設計一份有效的問卷，得到我想要的資料。";
$ies_lang["en"]["string7"] = "how to design an effective questionnaire to obtain useful data.";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "如何整理歸納數據，方便第三階段分析。";
$ies_lang["en"]["string8"] = "how to organise and generalise data for ease of analysis at Stage 3.";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "例子二";
$ies_lang["en"]["string9"] = "Example 2";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "完成了搜集數據這部分後，我對搜集數據的整體價值觀有所改變：";
$ies_lang["en"]["string10"] = "After the data collection process, my perception of data collection is transformed.";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "在工作開始之前，我以為";
$ies_lang["en"]["string11"] = "Before collecting data, I thought…";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "問卷調查的方法適用於所有專題研習(加以解釋）。";
$ies_lang["en"]["string12"] = "survey is the research method suitable for any enquiry study (with elaboration).";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "問卷的設計非常簡單，不需要細心思考每條問題背後的意義。";
$ies_lang["en"]["string13"] = "the design of the questionnaire is simple that careful consideration of the underlying significance for each question is not necessary.";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "所有的工作都能夠完美地按照計劃進行。";
$ies_lang["en"]["string14"] = "scheduled tasks can be perfectly done according to the work plan.";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "完成了第二階段之後，我學會了";
$ies_lang["en"]["string15"] = "On completion of Stage 2, I have learnt…";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "要按照題目的特點和議題的特性選擇適當的研究方法(加以解釋）。";
$ies_lang["en"]["string16"] = "to select (an) appropriate research method(s) based on the characteristics of my topic for enquiry (with elaboration).";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "問卷設計背後的原理和要訣，原來問問題有時比答問題還要難(加以解釋）。";
$ies_lang["en"]["string17"] = "the key to design a questionnaire and the rationale behind, and have found asking questions can be more difficult than answering them (with elaboration).";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/

$ies_lang["b5"]["string18"] = "危機處理方法，腦海中要時常思考有哪些突發事件出現，影響搜集資料(加以解釋）。";
$ies_lang["en"]["string18"] = "to work out a contingency plan. I always have to ask myself if there will be any contingencies that will interfere with the data collection process (with elaboration).";
/*
<?=$ies_lang[$_lang]["string18"]?>
*/

$ies_lang["b5"]["string19"] = "寫下你在階段二的反思：";
$ies_lang["en"]["string19"] = "Write down your reflection on Stage 2:";
/*
<?=$ies_lang[$_lang]["string19"]?>
*/

?>