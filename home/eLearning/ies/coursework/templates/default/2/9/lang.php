<?php

$ies_lang["b5"]["string1"] = "搜集好資料後，我們需要把原始數據加以整理和歸納，然後解釋所搜集的數據對研究有何作用和關連，如何能夠幫助你回應是次探究的焦點問題。";
$ies_lang["en"]["string1"] = "You have to organise and generalise the raw data collected. Then, justify the usefulness and relevancy of the data collected in relation to the enquiry issue and in what way the data helps answer the focus questions for enquiry.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "你需要回答以下問題：";
$ies_lang["en"]["string2"] = "Answer the following questions:";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "簡單歸納你蒐集了哪些類型的數據/資料和主要內容。";
$ies_lang["en"]["string3"] = "What type of data you have collected and what is the main idea?";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "所搜集的數據/資料如何有效地回應焦點問題。";
$ies_lang["en"]["string4"] = "In what way can the data help answer the focus questions effectively?";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "如要檢視整理好的數據結果及資料，請點擊有關連結。";
$ies_lang["en"]["string5"] = "To view the results of the organised data and information, click on the corresponding link";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "在階段一設定的焦點問題：";
$ies_lang["en"]["string6"] = "The focus questions set at Stage 1:";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "說明你如何使用所搜集的數據/資料來回應研究的焦點問題：";
$ies_lang["en"]["string7"] = "State how your focus questions for enquiry are answered with the use of the data collected.";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "經過多個星期的努力，本人已完成（多少份問卷/多少個訪問/多少個觀察報告）。所搜集的資料，大致可分為幾個範疇，（把資料類型稍作歸納並解釋）。這些資料將有助回應是次研究的焦點問題，（加以解釋）。";
$ies_lang["en"]["string8"] = "After many weeks of hard work, (number of questionnaires / number of interviews / number of observation reports) are completed. The data collected can be divided into several categories. (Categorise the data and elaborate.) The data collected helps answer the focus questions for enquiry (with elaboration).";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/
?>