<?php
$ies_lang["b5"]["string1"] = "在正式收集資料/數據前，你要先解說將使用甚麽研究方法與工具，以取得有助研究的相關資料/數據。";
$ies_lang["en"]["string1"] = "Explain what research tool(s) and method(s) you will adopt for collecting relevant data before getting started.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "研究方法大致分為兩類：";
$ies_lang["en"]["string2"] = "There are mainly two types of research methods:";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "質化研究";
$ies_lang["en"]["string3"] = "Qualitative method";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "量化研究";
$ies_lang["en"]["string4"] = "Quantitative method";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "認識質化及量化研究方法，有助你選擇合適的研究工具，來搜集數據或資料。";
$ies_lang["en"]["string5"] = "Getting to know more about qualitative and quantitative methods will help you choose the appropriate research tool(s) for data or information collection.";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "何謂質化研究？";
$ies_lang["en"]["string6"] = "What is a qualitative method?";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "何謂量化研究？";
$ies_lang["en"]["string7"] = "What is a quantitative method?";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "選擇研究方法前，請先考慮下列準則：";
$ies_lang["en"]["string8"] = "Consider the following criteria before selecting the research method(s):";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "實行成本 (相對其他研究方法，所花費的資源，包括時間、人力、金錢是多少？）";
$ies_lang["en"]["string9"] = "Implementation cost (resources used, including time, labour and money, compared to other research methods)";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "研究對象的回應率 (哪種方法最能得到最高的回應率？)";
$ies_lang["en"]["string10"] = "Response rate (Which method has the highest response rate?)";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "數據的質量 （哪種方法能獲取最多有用的資料？）";
$ies_lang["en"]["string11"] = "Data quality (Which method helps obtain the most useful data?)";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "問題的複雜性 （如果問題非常複雜，單靠問卷能不能得到深入資訊？）";
$ies_lang["en"]["string12"] = "Problem complexity (If the problem is very complicated, the more comprehensive data cannot be collected solely from questionnaires.)";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "能否接觸到目標人物（哪種方法最能成功接觸到研究對象？）";
$ies_lang["en"]["string13"] = "Target accessibility (Which method is most successful in reaching the research objects?)";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "現在，請說明你將使用甚麽研究方法及工具來搜集所需資料或數據。";
$ies_lang["en"]["string14"] = "Now, state what research method(s) and tool(s) you would like to use for data collection.";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "寫作方法：";
$ies_lang["en"]["string15"] = "Writing guides:";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "描述你將會採用質化研究或量化研究，還是結合兩者。";
$ies_lang["en"]["string16"] = "Describe the research method(s) you will adopt. Qualitative, quantitative or both?";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "描述你在搜集數據或資料時，需要用到的工具（例如：問卷、錄音筆）。";
$ies_lang["en"]["string17"] = "Describe the research tool(s) you will use for data collection, e.g. questionnaire and recording pen.";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/

$ies_lang["b5"]["string18"] = "描述你的研究對象。";
$ies_lang["en"]["string18"] = "Describe your research objects.";
/*
<?=$ies_lang[$_lang]["string18"]?>
*/

$ies_lang["b5"]["string19"] = "描述你在進行資料搜集前所需要的預備工作。";
$ies_lang["en"]["string19"] = "Describe what you have to prepare before collecting data.";
/*
<?=$ies_lang[$_lang]["string19"]?>
*/

$ies_lang["b5"]["string20"] = "舉出一些實際例子，說明在過程中可能遇到的問題或困難，並提出一些可行的預防措施或解決方法。";
$ies_lang["en"]["string20"] = "State the problems or difficulties you may encounter with concrete examples and suggest some practical precautions or solutions.";
/*
<?=$ies_lang[$_lang]["string20"]?>
*/

$ies_lang["b5"]["string21"] = "說明你將使用甚麽研究方法及工具來搜集所需資料或數據：";
$ies_lang["en"]["string21"] = "State what research method(s) and tool(s) you would like to use for data collection.";
/*
<?=$ies_lang[$_lang]["string21"]?>
*/

$ies_lang["b5"]["string22"] = "以下是你在階段一的選擇：";
$ies_lang["en"]["string22"] = "The following is what you have selected in Stage 1:";
/*
<?=$ies_lang[$_lang]["string22"]?>
*/

$ies_lang["b5"]["string23"] = "本研究將採用（質化研究/量化研究/質化與量化研究互相結合）。\\n我將會（使用網上問卷/深入訪問/實地考察），(訪問/觀察)（多少人/研究對象的特點）。當中需要使用的工具包括：（問卷/錄音機/筆記本等），用以記錄資料。\\n\\n在進行資料搜集前，我需要（確定和預約訪問對象/設計問卷/測試問卷/確定觀察的時間地點和目的/視察環境等），確保事前準備功夫充足。此外，我也預計到在過程中會出現一些潛在的問題和困難，例如：（訪問對象臨時不能出席/被訪者回答問卷時回應的質素參差/錄音與錄影器材有機會失靈/收集數據受天氣影響等）。然而這些問題都不難（解決/預防），我會（提出上述問題的相應解決方法）。";
$ies_lang["en"]["string23"] = "This study adopts (qualitative research method / quantitative research method / qualitative and quantitative research methods). (Online survey / In-depth interview / Field observation) will be conducted. (How many people / The characteristics of research objects) will be (interviewed / observed). Research tools like (questionnaires / recorder / notebook) will be used for recording purposes.\\n\\nTo ensure enough preparation work is done before data collection, I have to (decide on the interviewees and confirm with them the interview appointment / set the questionnaire / test the questionnaire / confirm the date and venue for field observation / conduct site inspection / others). Besides, it is predicted that potential problems and difficulties will arise. For example, (the interviewee could not turn up for the interview in the last minute / the answer quality varies with respondents / recorder and recording devices could be out of order / the weather factor affects the data collection process). Yet, these problems are not hard to (solve / avoid) with (corresponding solutions).";
/*
<?=$ies_lang[$_lang]["string23"]?>
*/
?>