<?php

$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 1, "method");
$step_ans_arr = $objIES->getAllStepAnswerForDisplay($target_taskID, $UserID);

for($i=0, $i_max=count($step_ans_arr); $i<$i_max; $i++)
{
  if(!in_array($step_ans_arr[$i]["StepSeq"], array(1,2))) continue;

  $step_ans_arr[$i] = nl2br($step_ans_arr[$i]["Answer"]);
}
$html_prev_step_answer = implode("<br />", $step_ans_arr);
if(!empty($html_prev_step_answer)){
	$html_prev_step_answer = $ies_lang[$_lang]["string22"]."<br /><span class=\"prev_answer\">". $html_prev_step_answer."</span>";
}
$html_templateFile = "view.php";
$template_word = $ies_lang[$_lang]["string23"];

?>