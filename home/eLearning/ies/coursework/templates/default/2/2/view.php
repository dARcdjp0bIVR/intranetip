<?php


$html_tips = '<a href="javascript:insertTemplateAtCursor(\'textbox_\', \''.$template_word.'\')">'.$Lang['IES']['InsertTemplateWord'].'</a>';
$html_textarea = '<textarea style="width:100%; height:50px" name="question[textarea][1]" id="textbox_">'.$answer[1].'</textarea> ';
$html_textareaWithTitle = libies_ui::getTextareaDisplayWithTitle($ies_lang[$_lang]["string21"].$html_tips, $html_textarea);
?>

<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
  	 
	 	 <?=$ies_lang[$_lang]["string1"]?><br />
	 <br />
           <?=$ies_lang[$_lang]["string2"]?>
      <ol>
         <li><?=$ies_lang[$_lang]["string3"]?></li>
         <li><?=$ies_lang[$_lang]["string4"]?></li>
      </ol><br />
       	 <?=$ies_lang[$_lang]["string5"]?><br />
           
             
     <a href="tips/stage2_B1_01.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo"> <?=$ies_lang[$_lang]["string6"]?></a><br /><br />
             
     <a href="tips/stage2_B1_02.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo"> <?=$ies_lang[$_lang]["string7"]?> </a><br />
      
      <br /><?=$ies_lang[$_lang]["string8"]?>
       <ul>
         <li><?=$ies_lang[$_lang]["string9"]?></li>
         <li><?=$ies_lang[$_lang]["string10"]?></li>
         <li><?=$ies_lang[$_lang]["string11"]?></li>
         <li><?=$ies_lang[$_lang]["string12"]?></li>
		 <li><?=$ies_lang[$_lang]["string13"]?></li>
		</ul><br /> 

	<?=$ies_lang[$_lang]["string14"]?><br />
	
 	<div class="task_content">
      <?=$html_prev_step_answer?>
    </div>
<br /><?=$ies_lang[$_lang]["string15"]?>
	<ol>
        <li><?=$ies_lang[$_lang]["string16"]?></li>
        <li><?=$ies_lang[$_lang]["string17"]?></li>
		<li><?=$ies_lang[$_lang]["string18"]?></li>
		<li><?=$ies_lang[$_lang]["string19"]?></li>
		<li><?=$ies_lang[$_lang]["string20"]?></li>
      </ol><br />
    </div>
       
	<?=$html_textareaWithTitle?>
    
    <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
      <div style="float:right; padding-top:5px">
        <input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
      </div>
    </div> 
    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doSaveStep(<?=$next_step_id?>,true);" value="<?=$Lang['IES']['SaveAndNextStep']?>" />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Back']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->