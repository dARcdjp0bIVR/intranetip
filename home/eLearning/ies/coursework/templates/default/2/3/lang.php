<?php
$ies_lang["b5"]["string1"] = "獨立專題探究一般會結合第一手和第二手資料，同學應該從多方面著手蒐集資料，讓資料更全面和完整。不應集中在某一兩個文獻資料，或單靠問卷調查的方法，不然整個研究報告也會欠缺說服力。";
$ies_lang["en"]["string1"] = "Both first and second-hand information will be used in the IES in general. To make your analysis more comprehensive, you should collect information from various sources instead of just relying on one to two references or only the data collected from survey. Otherwise, the report does not sound convincing enough.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "你知道甚麼是第一手資料？甚麼是第二手資料嗎？";
$ies_lang["en"]["string2"] = "Can you tell what first-hand information and second-hand information is?";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "第一手資料";
$ies_lang["en"]["string3"] = "First-hand information";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "第一手資料是由研究員親身蒐集、獲取和記錄的資料，例如：實地考察後的筆記、問卷調查的結果、深入訪問的筆記。這些都是直接在現場或向當事人取得的資料，可以稱為第一手資料。";
$ies_lang["en"]["string4"] = "First-hand information is collected and recorded by researchers themselves. Examples of first-hand information are the notes jotted in field observations or interviews as well as survey results. They are all collected directly from the research objects or on the spot.";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "第二手資料";
$ies_lang["en"]["string5"] = "Second-hand information";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "第二手資料是一些經過轉述的現成資料。常見例子有：書籍、百科全書、互聯網的文章、雜誌、報章。這些都是就第一手資料所作的分析、歸納或評價。在研究開始前，我們通常透過了解這些現成的第二手資料，增加我們對問題的認識，然後再決定蒐集所需的一手資料，回應研究題目。";
$ies_lang["en"]["string6"] = "Second-hand information is the new analyses, conclusions or evaluations made based on the information from primary sources. Common examples are books, encyclopedia, web articles, magazines and newspapers. Before the study starts, we usually refer to the existing second-hand information to know more about the topic. Later on, we will decide on the necessary first-hand information to answer the research question.";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "為使你掌握何謂第一手、第二手資料，請完成以下練習：";
$ies_lang["en"]["string7"] = "Finish the following exercise to help you identify first-hand and second-hand information.";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "檢視評估結果";
$ies_lang["en"]["string8"] = "Assessment";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "重新評估";
$ies_lang["en"]["string9"] = "Re-assessment";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "某機構在2010年做了一個與青少年吸毒有關的研究，訪問了3000個學生，得到一些數據。這些數據是：";
$ies_lang["en"]["string10"] = "An institution conducted a research in 2010 on teenage drug abuse and data was collected after interviewing 3000 students. Such data is:";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "本地教科書作者參考了美國學者曼昆的 <經濟學原理>，寫成了一本新高中經濟學的教科書。這本教科書是：";
$ies_lang["en"]["string11"] = "With reference to Principles of Economics by N. Gregory Mankiw, a local textbook writer wrote an NSS Economics textbook. It is:";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "為了知道本校同學對新開張小賣部有甚麽意見，張同學用問卷抽樣訪問了40位來自各級的同學。張同學所收集的是：";
$ies_lang["en"]["string12"] = "To know more about the opinions of fellow schoolmates on the new tuck shop, Cheung interviewed 40 students of different forms with questionnaires on a random basis. What he will have collected is:";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "為了解大角咀居民對興建高鐵的看法，小強決定訪問3位大角咀區的居民。小強收集的是：";
$ies_lang["en"]["string13"] = "To know more about how Tai Kok Tsui residents view the Guangzhou-Shenzhen-Hong Kong Express Rail Link, Keung decides to interview 3 of them. What he will have collected is:";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "某報紙發表一篇文章，引用某些政府數據，而你引用了該篇文章的數據作為專題研習的內容。你引用了：";
$ies_lang["en"]["string14"] = "The data from an official source is quoted in a newspaper article and you have quoted such data in your enquiry study. You have used:";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "維基百科歸納了很多原著的觀點，你引用了維基百科的資料作為專題研習的文獻參考。你引用了：";
$ies_lang["en"]["string15"] = "Wikipedia has summed up many viewpoints of the original work. You have used such information in your literature review of the enquiry study. You have used:";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "全對！你已掌握何謂第一手、第二手資料。下一步，你將要分析自己收集的數據和資料是屬於哪一類。";
$ies_lang["en"]["string16"] = "All correct! You can tell what first-hand and second-hand information is. The next step is to identify which category your data and information collected falls in.";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "為使你掌握何謂第一手、第二手資料，請再細心閱讀上述有關說明。";
$ies_lang["en"]["string17"] = "Read the above information carefully to understand what the first-hand and second-hand information is.";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/

?>