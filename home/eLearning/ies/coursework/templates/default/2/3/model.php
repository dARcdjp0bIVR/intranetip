<?php
/**************************/
/** START CONFIG SECTION **/
/**************************/
$html_templateFile = "view.php";

//HardCode the Info Value 
$infoType["firstInfo"] = 1;    //第一手資料
$infoType["secondInfo"] = 2;   //第二手資料

//code1 , code2 .. code6 is to  identfiy the question , very important but can be any value(value cannot be duplicated within this question / step)
$_question[1] = array(
					"code1".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["string10"].$ies_cfg["question_answer_sperator"].$infoType["firstInfo"],
					"code2".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["string11"].$ies_cfg["question_answer_sperator"].$infoType["secondInfo"],
					"code3".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["string12"].$ies_cfg["question_answer_sperator"].$infoType["firstInfo"],
					"code4".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["string13"].$ies_cfg["question_answer_sperator"].$infoType["firstInfo"],
					"code5".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["string14"].$ies_cfg["question_answer_sperator"].$infoType["secondInfo"],
					"code6".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["string15"].$ies_cfg["question_answer_sperator"].$infoType["secondInfo"]
				);

// hard code the comment , first element is all correct answer comment , second element is comment with wrong answer
$_comment[1] = array($ies_lang[$_lang]["string16"],
					 $ies_lang[$_lang]["string17"]);
$comment_get = "";
$_isDone = FALSE;
$noOfOption = 2;

/**************************/
/** END CONFIG SECTION ****/
/**************************/

function generateQuestion($questionTitle,$noOfOption,$answerArray = null)
{
	global $ies_cfg , $comment_get,$_comment, $ies_lang, $_lang;
	$_isDone = false;
	$_studentIsCorrect  = 1;
	$_studentIsWrong	= 0;
	$_correctAns = 1;  //cfg value
	$_wrongAns   = 0;  //cfg value

	if(is_array($answerArray) && sizeof($answerArray) > 0){
		$_isDone = true;
	}

	$q_arr = split($ies_cfg["new_set_question_answer_sperator"], $answerArray[0]["Answer"]);
	//split student answer
	for($i = 0,$j = sizeof($q_arr); $i < $j ; $i++){

		//$_qCode  --> question code of the student Ans
		list($_qCode,$_ans) = explode($ies_cfg["question_answer_sperator"],$q_arr[$i]);
		$studentAnsArray[$_qCode] = $_ans;
	}


	$html = "";


	for($i =0;$i< sizeof($questionTitle);$i++)
	{
		list($_questionCode, $_title, $_standardAns) = explode($ies_cfg["question_answer_sperator"],$questionTitle[$i]); 

		$_questionNo = $i +1;
		$html .= "<tr class=\"question\">";
		$html .= "<td>{$_questionNo}&nbsp;</td>";
		$html .= "<td>{$_title}&nbsp;</td>";

		for($j = 0; $j < $noOfOption;$j++){
			$_displayMarkingImg = $_resultImg = $_checked = "";

			$_disable = ($_isDone) ?" DISABLED ":"";

			$_optionValue = $j+1;

			if($_isDone){
				$_studentInputAnswer = $studentAnsArray[$_questionCode];

				//student has ans this before , display student fill in answer with "CHECKED" for the radio button
				$_checked = ($_studentInputAnswer == $_optionValue)? " CHECKED " : "";

				// 1 --> mark as correct (student with a correct answer), 0 --> mark as incorrect (student with a wrong answer)
				if($_studentInputAnswer == $_standardAns) {
					$_marking = $_studentIsCorrect;
				}else{
					$_marking = $_studentIsWrong;

					//student has a wrong answer , set the comment to a "wrong comment ($_comment[1][1])";
					//error_log("assing to wrong comment \n", 3, "/tmp/111.txt");
					$comment_get = $_comment[1][1];
				}
				
//				$haveWrongAns = ($_studentInputAnswer == $_standardAns)? "": $_withWrongAns;

				//this "if" is for last option ($j == $noOfOption)
				if($j==$noOfOption-1)
				{					
					$_resultImg = ($_marking == $_studentIsCorrect)  ?"ies/selected_tick.gif":"icon_cross.gif";
					$_displayMarkingImg = "<img src=\"/images/2009a/{$_resultImg}\" />";
				}			
			}else{
				// ! $_isDone , student have not answer this question set all the checkbox default is checked
				// $j == 0; set first option as default
				$_checked  = ($j ==0)? " CHECKED " : "" ;
			}





			$html .= "<td class=\"option\"><input name=\"setQuestion1[{$i}]\" type=\"radio\" {$_disable} value=\"{$_questionCode}{$ies_cfg["question_answer_sperator"]}{$_optionValue}\" {$_checked}/> &nbsp;{$_displayMarkingImg}</td>";
		}
		$html .= "</tr>\n";

	}

	$htmlTable = "<table class=\"about_stage estimate_table\">
        <tr><td>&nbsp;</td><td>&nbsp;</td><td class=\"option\">{$ies_lang[$_lang]["string3"]}</td><td class=\"option\">{$ies_lang[$_lang]["string5"]}</td></tr>";
	$htmlTable .= $html;
	$htmlTable .= "</table>";

	//if $comment_get is empty , that mean student answer all the question correctly , set the comment to a "correct comment". Otherwise remain a "wrong" comment
	$comment_get = (trim($comment_get == "")) ? $_comment[1][0]."<br/>": $comment_get ."<Br/>";
	 
	return $htmlTable;
}

?>
