<?php
$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 1, "difficulty");
$html_prev_task_answer_1 = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));
$html_prev_task_answer_1 = $html_prev_task_answer_1? $html_prev_task_answer_1 : '--';

$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 2, "tooldesc");
$html_prev_task_answer_2 = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));
$html_prev_task_answer_2 = $html_prev_task_answer_2? $html_prev_task_answer_2 : '--';

$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 2, "concept");
$html_prev_task_answer_3 = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));
$html_prev_task_answer_3 = $html_prev_task_answer_3? $html_prev_task_answer_3 : '--'; 

$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 2, "dairy");
$raw_answer_4 = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));
if($raw_answer_4){
	$_question[1] = array(
					"date" => $Lang['IES']['Date'],
					"mainwork" => $Lang['IES']['MainWork'],
					"workpurpose" => $Lang['IES']['WorkPurpose'],
					"difficulties"=>$Lang['IES']['Difficulties'],
					"solution"=>$Lang['IES']['Solution']
					
				);
	$question_tr_fields = "<tr><th>#</th><th>" . implode("</th><th>",$_question[1]) . "</th></tr>";
	
	$diary_ary = explode("@~@",$raw_answer_4);
	
	$html_prev_task_answer_4 = "<table class=\"common_table_list IES_form_table\" id=\"addWorkTable\">
	    							<col class=\"\" />
	    							<col class=\"field_title\" />
	    							<thead>
										$question_tr_fields
									</thead>
	    							<tbody>";
	for($i=0;$i<count($diary_ary);$i++){
		$html_prev_task_answer_4 .= "<tr><td class=\"num_check\">".($i+1)."</td><td>".str_replace("#~#","</td><td>",$diary_ary[$i])."</td></tr>";
	}
	$html_prev_task_answer_4 .= "	</tbody>
	    						</table>";
}
else{
	$html_prev_task_answer_4 = '--';
}

$html_templateFile = "view.php";
?>
