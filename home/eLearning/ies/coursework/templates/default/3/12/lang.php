<?php

$ies_lang["b5"]["string1"] = "讀者在正式知道研究結果前，需要了解你用甚麼方法取得數據。因此，你要解釋如何透過選擇研究工具，搜集數據/資料來回答研究題目。";
$ies_lang["en"]["string1"] = "Readers need to know how you collect the data before formally reading your research outcomes. Therefore, you have to explain the rationale behind the tools selected for data collection to address the topic for enquiry.";

$ies_lang["b5"]["string2"] = "現在，你需要整合第一及第二階段的研究設計理念，把整個研究框架說明清楚，讓讀者知道你如何搜集數據。";
$ies_lang["en"]["string2"] = "Now you have to integrate the research design at Stages 1 and 2, and explain the enquiry framework clearly to help the readers understand how you collect the data.";

$ies_lang["b5"]["string3"] = "如何撰寫研究設計？";
$ies_lang["en"]["string3"] = "How to write up the research design?";

$ies_lang["b5"]["string4"] = "階段一撰寫的可預期的困難和限制：<br/>";
$ies_lang["en"]["string4"] = "The foreseeable difficulties and limitations written up at Stage 1:<br/>";

$ies_lang["b5"]["string5"] = "階段二撰寫的搜集數據/資料採用的工具及方法：<br/>";
$ies_lang["en"]["string5"] = "The data collection tool(s) and method(s) written up at Stage 2:<br/>";

$ies_lang["b5"]["string6"] = "階段二撰寫的背後的理念：<br/>";
$ies_lang["en"]["string6"] = "The rationale written up at Stage 2:<br/>";

$ies_lang["b5"]["string7"] = "階段二撰寫的研究日誌：<br/>";
$ies_lang["en"]["string7"] = "The enquiry diary written up at Stage 2:<br/>";

$ies_lang["b5"]["string8"] = "參考階段一和階段二的資料，概括描述你的「研究設計」：";
$ies_lang["en"]["string8"] = "Refer to the information of Stage 1 and 2 and explain your ‘Research design’ briefly.";

$ies_lang["b5"]["string9"] = "研究報告簡介";
$ies_lang["en"]["string9"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string10"] = "3. 研究方法";
$ies_lang["en"]["string10"] = "3. Enquiry method";

$ies_lang["b5"]["string11"] = "描述研究方法時，我們需要解釋如何透過選擇工具搜集數據 / 資料來回應研究題目。";
$ies_lang["en"]["string11"] = "We have to explain the rationale behind the tools selected for data collection to address the topic for enquiry when describing the enquiry method.";
?>