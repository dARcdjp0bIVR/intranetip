<?php
/**************************/
/** START CONFIG SECTION **/
/**************************/
$html_templateFile = "view.php";

//Answer Array
$question_ans = array(
					array($ies_lang[$_lang]["answer1"],$ies_lang[$_lang]["answer2"]),
					array($ies_lang[$_lang]["answer3"],$ies_lang[$_lang]["answer4"],$ies_lang[$_lang]["answer5"]),
					array($ies_lang[$_lang]["answer6"],$ies_lang[$_lang]["answer7"],$ies_lang[$_lang]["answer8"],$ies_lang[$_lang]["answer9"]),
					array($ies_lang[$_lang]["answer10"],$ies_lang[$_lang]["answer11"]),
					array($ies_lang[$_lang]["answer12"],$ies_lang[$_lang]["answer13"],$ies_lang[$_lang]["answer14"]),
					array($ies_lang[$_lang]["answer15"],$ies_lang[$_lang]["answer16"],$ies_lang[$_lang]["answer17"])
				);

//code1 , code2 .. code6 is to  identfiy the question , very important but can be any value(value cannot be duplicated within this question / step)
$question_code = array("code1","code2","code3","code4","code5","code6");
$_question[1] = array(
					$question_code[0].$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["question1"].$ies_cfg["question_answer_sperator"].explode(", ",$question_ans[0]),
					$question_code[1].$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["question2"].$ies_cfg["question_answer_sperator"].explode(", ",$question_ans[1]),
					$question_code[2].$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["question3"].$ies_cfg["question_answer_sperator"].explode(", ",$question_ans[2]),
					$question_code[3].$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["question4"].$ies_cfg["question_answer_sperator"].explode(", ",$question_ans[3]),
					$question_code[4].$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["question5"].$ies_cfg["question_answer_sperator"].explode(", ",$question_ans[4]),
					$question_code[5].$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["question6"].$ies_cfg["question_answer_sperator"].explode(", ",$question_ans[5])
				);

$explanation = array($ies_lang[$_lang]["explain1"],
					 $ies_lang[$_lang]["explain2"],
					 $ies_lang[$_lang]["explain3"],
					 $ies_lang[$_lang]["explain4"],
					 $ies_lang[$_lang]["explain5"],
					 $ies_lang[$_lang]["explain6"]);

// hard code the comment , first element is all correct answer comment , second element is comment with wrong answer
$question_comment = array($ies_lang[$_lang]["string12"],
					 	  $ies_lang[$_lang]["string13"]);
$_comment[1] = array($ies_lang[$_lang]["string16"],
					 $ies_lang[$_lang]["string17"]);
$_ans_str = array($ies_lang[$_lang]["string8"],
				  $ies_lang[$_lang]["string9"]);
					 
$comment_get = "";
$_isDone = FALSE;

/**************************/
/** END CONFIG SECTION ****/
/**************************/

function get_question_comment($question_no){
	global $question_ans, $explanation, $ies_lang, $_lang;
	
	$question_comment = "<span style=\"color: #2286C5;\">".$ies_lang[$_lang]["string13"];
	for($i=0;$i<count($question_ans[$question_no]);$i++){
		$question_comment .= ($i+1).". ".$question_ans[$question_no][$i]."&nbsp;&nbsp;&nbsp;";
	}
	$question_comment .= "</span><br/><span style=\"color: #2286C5;\">".$ies_lang[$_lang]["string14"].$explanation[$question_no]."</span>";
	
	return $question_comment;
}

function generateQuestion($questionTitle, $answerArray=null)
{
	global $ies_cfg , $comment_get, $question_comment, $_comment, $ies_lang, $_lang, $explanation;
	$_isDone = false;
	$_studentIsCorrect  = 1;
	$_studentIsWrong	= 0;
	$_correctAns = 1;  //cfg value
	$_wrongAns   = 0;  //cfg value

	if(is_array($answerArray) && sizeof($answerArray) > 0){
		$_isDone = true;
	}

	$q_arr = split($ies_cfg["new_set_question_answer_sperator"], $answerArray[0]["Answer"]);
	//split student answer
	for($i = 0,$j = sizeof($q_arr); $i < $j ; $i++){
		
		//$_qCode  --> question code of the student Ans
		list($_qCode,$_ans) = explode($ies_cfg["question_answer_sperator"],$q_arr[$i]);
		$studentAnsArray[$_qCode] = $_ans;
	}

	$html = "";

	for($i =0;$i< sizeof($questionTitle);$i++)
	{
		list($_questionCode, $_title, $_standardAns) = explode($ies_cfg["question_answer_sperator"],$questionTitle[$i]); 

		$_questionNo = $i +1;
		$html .= "<tr class=\"question\">";
		$html .= "<td>{$_questionNo}&nbsp;</td>";
		$html .= "<td>{$_title}&nbsp;".($_isDone? "<br/><br/>".get_question_comment($i):"")."</td>";
		
		if($_isDone){
			$html .= "<td align=\"center\">";
			$html .= "<span>".$studentAnsArray[$_questionCode]."</span>";
			/*
			for($j=0, $cnt=count($_ans_ary); $j<$cnt; $j++){
				$html .= "<span>".$_ans_ary[$j]."</span><img src=\"/images/2009a/".(in_array($_ans_ary[$j], $_standardAns)? "ies/selected_tick.gif" : "icon_cross.gif")."\" />";
				$html .= $j==($cnt-1)? "" : "<br/>";
			}
			*/
			$html .= "</td>";
		}
		else{
			$html .= "<td align=\"center\">
							<input type=\"text\" name=\"std_ans[]\" style=\"width:95%\">
					  </td>";
		}
		$html .= "</tr>\n";

	}

	$htmlTable = "<table class=\"about_stage estimate_table\">
        			<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td class=\"option\" style=\"width:300px\">{$ies_lang[$_lang]["string12"]}</td>
					</tr>";
	$htmlTable .= $html;
	$htmlTable .= "</table>";

	//if $comment_get is empty , that mean student answer all the question correctly , set the comment to a "correct comment". Otherwise remain a "wrong" comment
	//$comment_get = (trim($comment_get == "")) ? $_comment[1][0]."<br/>": $comment_get ."<Br/>";
	 
	return $htmlTable;
}

?>