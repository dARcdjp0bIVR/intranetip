<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "研究設計清楚交代研究對象與抽樣方法。";
$ies_lang["en"]["string2"] = "The research object(s) and sampling method are clearly stated in the research design.";

$ies_lang["b5"]["string3"] = "研究設計清楚交代研究程序。";
$ies_lang["en"]["string3"] = "The research process is clearly stated in the research design.";

$ies_lang["b5"]["string4"] = "研究設計清楚交代研究工具。";
$ies_lang["en"]["string4"] = "The research tool is clearly stated in the research design.";

$ies_lang["b5"]["string5"] = "修訂研究設計，務求達到自我檢測所有指標。";
$ies_lang["en"]["string5"] = "Revise the research design to meet all requirements of the self-check.";

?>