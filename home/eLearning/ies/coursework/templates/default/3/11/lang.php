<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "文獻探討的資料來源多樣化 (不會只集中在某一種資料來源，例如單靠報紙或網上資料)。";
$ies_lang["en"]["string2"] = "Diversified sources of references are quoted in the literature review (citations not only from a single source, e.g. newspapers or the Internet).";

$ies_lang["b5"]["string3"] = "文獻資料的來源有清楚記錄，避免有抄襲之嫌。";
$ies_lang["en"]["string3"] = "The sources of references are clearly recorded to avoid plagiarism.";

$ies_lang["b5"]["string4"] = "文獻資料經過分析歸納，並非不著邊際。";
$ies_lang["en"]["string4"] = "The references have undergone analytical deductions and are not discursive.";

$ies_lang["b5"]["string5"] = "文獻資料當中的定義或前人的研究結果，有助稍後分析之用。";
$ies_lang["en"]["string5"] = "Definitions or predecessor research results from the references are useful for data analysis in my enquiry.";

$ies_lang["b5"]["string6"] = "修訂文獻探討，務求達到自我檢測所有指標。";
$ies_lang["en"]["string6"] = "Revise the literature review to meet all requirements of the self-check.";

?>