<?php

$ies_lang["b5"]["string1"] = "在下結論前，我們要先簡略地重申研究的目的和研究範圍，然後歸納所有能夠回答焦點問題的論據和研究所得。如果你在分析時提出過建議或解決方法，你也可以在這裡做簡單歸納。";
$ies_lang["en"]["string1"] = "We have to mention the enquiry objectives and areas of enquiry briefly before making the conclusion, then integrate the premises and findings that can address the focus questions. If you have provided recommendations or solutions in the analysis, summarise them here as well.";

$ies_lang["b5"]["string2"] = "同學要緊記，結論部分對一份研究報告非常重要。尤其是結論能否回答研究目的和焦點問題，更是評核整個專題研習是否成功的首要考慮，絕對不容忽視！";
$ies_lang["en"]["string2"] = "Keep firmly in mind that the conclusion is the vital part of the research report. Whether it can address the enquiry objectives and focus questions is critical to the judgement on success or failure of the whole enquiry. So don’t overlook this!";

$ies_lang["b5"]["string3"] = "若現在發現「結論」與「緒論」的焦點有差異，就應細心檢視前後的邏輯聯繫，確保首尾呼應，邏輯一致。";
$ies_lang["en"]["string3"] = "If you find that the focuses of the ‘Conclusion’ and ‘ Preface’ are different, you should examine the logical flow carefully to ensure the echo and consistency.";

$ies_lang["b5"]["string4"] = "階段一撰寫的探究題目：<br/>";
$ies_lang["en"]["string4"] = "The topic for enquiry written up at Stage 1:<br/>";

$ies_lang["b5"]["string5"] = "階段一撰寫的研究背景、相關概念和知識：<br/>";
$ies_lang["en"]["string5"] = "The background information, related concepts and relevant knowledge written up at Stage 1:<br/>";

$ies_lang["b5"]["string6"] = "階段二撰寫的探究題目：<br/>";
$ies_lang["en"]["string6"] = "The topic for enquiry written up at Stage 2:<br/>";

$ies_lang["b5"]["string7"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string7"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string8"] = "階段三撰寫的焦點問題：<br/>";
$ies_lang["en"]["string8"] = "The focus questions written up at Stage 3:：<br/>";

$ies_lang["b5"]["string9"] = "階段三撰寫的分析結果：<br/>";
$ies_lang["en"]["string9"] = "The analysis of findings written up at Stage 3:<br/>";

$ies_lang["b5"]["string10"] = "階段三撰寫的研究建議：<br/>";
$ies_lang["en"]["string10"] = "The research recommendations written up at Stage 3:<br/>";

$ies_lang["b5"]["string11"] = "撰寫結論：";
$ies_lang["en"]["string11"] = "Write up the Conclusion:";

$ies_lang["b5"]["string12"] = "研究報告簡介";
$ies_lang["en"]["string12"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string13"] = "6. 結論";
$ies_lang["en"]["string13"] = "6. Conclusion";

$ies_lang["b5"]["string14"] = "結論是整份報告的精華所在，一個富有邏輯和完整的結論，會為你爭取到較佳的分數，不能掉以輕心！";
$ies_lang["en"]["string14"] = "The conclusion is the essence of the report. A logical and comprehensive conclusion can earn you good grades. So don’t treat it lightly!";
?>