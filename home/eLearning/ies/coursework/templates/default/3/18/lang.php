<?php

$ies_lang["b5"]["string1"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string1"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string2"] = "研究建議是基於某些真實問題，並非為建議而建議。";
$ies_lang["en"]["string2"] = "The research recommendations are based on authentic issues, not just for the sake of recommendations.";

$ies_lang["b5"]["string3"] = "所提出的建議可行性高，能真正有效幫助解答問題。";
$ies_lang["en"]["string3"] = "The research recommendations are feasible, and can help solve problems effectively.";

$ies_lang["b5"]["string4"] = "研究建議內容具體，並非泛泛而談。 ";
$ies_lang["en"]["string4"] = "The research recommendations are concrete without generalities.";

$ies_lang["b5"]["string5"] = "研究建議能回應焦點問題與研究目的。";
$ies_lang["en"]["string5"] = "The research recommendations can address the focus questions and enquiry objectives.";

$ies_lang["b5"]["string6"] = "修訂研究建議，務求達到自我檢測所有指標。 ";
$ies_lang["en"]["string6"] = "Revise the research recommendations to meet all requirements of the self-check.";
?>