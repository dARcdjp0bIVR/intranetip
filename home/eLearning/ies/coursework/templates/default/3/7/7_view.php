﻿<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
      <?=$ies_lang[$_lang]["string1"]?><br />
      <ol>
		<li><?=$ies_lang[$_lang]["string2"]?></li>
		<li><?=$ies_lang[$_lang]["string3"]?></li>
		<li><?=$ies_lang[$_lang]["string4"]?></li>
		<li><?=$ies_lang[$_lang]["string5"]?></li>
	  </ol>
      
      <?=$ies_lang[$_lang]["string6"]?><br /><br />
      <?=$ies_lang[$_lang]["string7"]?>
      <ul>
        <li><?=$ies_lang[$_lang]["string8"]?></li>
        <li><?=$ies_lang[$_lang]["string9"]?></li>
        <li><?=$ies_lang[$_lang]["string10"]?></li>
        <li><?=$ies_lang[$_lang]["string11"]?></li>
        <li><?=$ies_lang[$_lang]["string12"]?></li>
        <li><?=$ies_lang[$_lang]["string13"]?></li>
        <li><?=$ies_lang[$_lang]["string14"]?></li>
      </ul>
    </div>
    
    <span class="task_instruction_text"><?=$ies_lang[$_lang]["string15"]?></span>
    <textarea style="width:100%; height:50px" name="question[textarea][1]"><?=$answer[1]?></textarea>  

	<div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>        
		<div style="float:right; padding-top:5px">          	
		<input id="save" name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$Lang['IES']['Save']?>" onclick="doSaveStep(<?=$step_id?>,false);"/>
    </div></div>     
    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['LastStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doSaveStep(<?=$next_step_id?>, true);" value="<?=$Lang['IES']['SaveAndNextStep']?>" />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->