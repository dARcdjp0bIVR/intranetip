<?php

$ies_lang["b5"]["string1"] = "以下是你在整個專題研習過程中搜集到的資料：";
$ies_lang["en"]["string1"] = "The following is the information collected in the course of study.";

$ies_lang["b5"]["string2"] = "請選取合適的參考資料，複製並貼在下方：〈以上的中文參考資料並非以筆劃排序，如有需要，請重新排列。〉";
$ies_lang["en"]["string2"] = "Choose the appropriate references, copy and paste them in the box below.";

$ies_lang["b5"]["string3"] = "研究報告簡介";
$ies_lang["en"]["string3"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string4"] = "7. 編輯你的參考資料清單，夾附在報告中。";
$ies_lang["en"]["string4"] = "7. Edit your bibliography and include it in your report.";
?>