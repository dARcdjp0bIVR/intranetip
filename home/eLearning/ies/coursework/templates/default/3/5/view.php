﻿<?php
$html_textarea = '<textarea style="width:100%; height:50px" name="question[textarea][1]">'.$answer[1].'</textarea> ';
$html_textareaWithTitle = libies_ui::getTextareaDisplayWithTitle($ies_lang[$_lang]["string13"], $html_textarea);
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
		<p/><?=$ies_lang[$_lang]["string1"]?></p>
		<p/><?=$ies_lang[$_lang]["string2"]?></p>
		<a href="tips/stage3_lightbox_01.php?KeepThis=true&amp;TB_iframe=false&amp;height=450&amp;width=751&amp;modal=true" class="thickbox task_demo"><?=$ies_lang[$_lang]["string3"]?></a>
		<p class="spacer"></p> 
		<p>
		<?=$ies_lang[$_lang]["string4"]?>
		<span class="prev_answer"><?=$html_prev_task_answer_1?></span>
		<br /><br />
		<?=$ies_lang[$_lang]["string5"]?>
		<span class="prev_answer"><?=$html_prev_task_answer_2?></span>
		<br /><br />
		<?=$ies_lang[$_lang]["string6"]?>
		<span class="prev_answer"><?=$html_prev_task_answer_3?></span>
		<br />
		</p>
		<div class="show_hint">
			<script>
				function toggle_hint_block(hint_id){
					obj = document.getElementById(hint_id);
					
					if(obj.style.display != 'none'){
						obj.style.display = 'none';
					}
					else{
						obj.style.display = 'block';
					}
				}
			</script>
			<a class="task_hint" href="javascript:void(0)" onclick="toggle_hint_block('hint_content_01')"><?=$ies_lang[$_lang]["string7"]?></a>
			<p class="spacer"></p> 
			<div style="display: none;" id="hint_content_01" class="hint_content">
				<ol>
					<li><?=$ies_lang[$_lang]["string8"]?></li>
					<li><?=$ies_lang[$_lang]["string9"]?></li>
					<li><?=$ies_lang[$_lang]["string10"]?></li>
					<li><?=$ies_lang[$_lang]["string11"]?></li>
					<li><?=$ies_lang[$_lang]["string12"]?></li>
				</ol>
			</div>
		</div>
		<p class="spacer"></p><br/>

<?=$html_textareaWithTitle?>

	    <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
	      <div style="float:right; padding-top:5px">
	        <input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
	      </div>
	    </div>
	</div>     
		
	<!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input id="save_submit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Submit']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />                                      
  </div></div>
  
</div> <!-- task_wrap end -->