<?
$ies_lang["b5"]["string1"] = "「焦點問題」是把「研究目的」分拆成三至五條比較細緻具體的次問題。其佔的篇幅雖然不多，但對整個報告非常重要。因為它們將引導你收集資料數據，讓你在分析時有清晰方向。";
$ies_lang["en"]["string1"] = "The ‘enquiry objectives’ are broken down into 3 to 5 more detailed and concrete sub-questions to form the ‘focus questions’. Although the focus questions only occupy a small part in the report, they are crucial in guiding you to collect data and provide you with a clear direction to analyse them.";

$ies_lang["b5"]["string2"] = "所以，「焦點問題」應該定得比「研究目的」更具體、更準確。而一旦設定了焦點問題，就必須在分析和結論部分作重點回應。完成整個研究後，同學理應能回應所有焦點問題，否則會有偏題離題之嫌。同時，必須確保自己已回應所有焦點問題，遺漏回答焦點問題，分數肯定會大打折扣！";
$ies_lang["en"]["string2"] = "Therefore, the ‘focus questions’ should be more concrete and precise than the ‘enquiry objectives’. After the focus questions are set, they must be addressed in the section of ‘Analysis and Conclusion’. On completion of the enquiry, you should have attended to each and every one of the focus questions to avoid digressions, or marks will definitely be deducted!";

$ies_lang["b5"]["string3"] = "「焦點問題」與整個研究的關係";
$ies_lang["en"]["string3"] = "The relationship between the ‘focus questions’ and the study";

$ies_lang["b5"]["string4"] = "階段三撰寫的研究背景：<br/>";
$ies_lang["en"]["string4"] = "The background information written up at Stage 3:<br/>";

$ies_lang["b5"]["string5"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string5"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string6"] = "階段一撰寫的焦點問題：<br/>";
$ies_lang["en"]["string6"] = "The focus questions written up at Stage 1:<br/>";

$ies_lang["b5"]["string7"] = "寫作小貼士 ";
$ies_lang["en"]["string7"] = "Writing hints";

$ies_lang["b5"]["string8"] = "「焦點問題」與「研究目的」相關。";
$ies_lang["en"]["string8"] = "The ‘focus questions’ are related to the ‘enquiry objectives’.";

$ies_lang["b5"]["string9"] = "「焦點問題」中沒有含糊字詞。";
$ies_lang["en"]["string9"] = "There is no vague wording in the ‘focus questions’.";

$ies_lang["b5"]["string10"] = "回答了「焦點問題」就能有效解答研究問題。";
$ies_lang["en"]["string10"] = "The enquiry topic can be effectively addressed after answering the‘focus questions’.";

$ies_lang["b5"]["string11"] = "「焦點問題」比「研究目的」和「研究題目」更具體。";
$ies_lang["en"]["string11"] = "The ‘focus questions’ are more specific than the ‘enquiry objectives’ and ‘enquiry topic’.";

$ies_lang["b5"]["string12"] = "「焦點問題」能引領我設計問卷或訪問問題。";
$ies_lang["en"]["string12"] = "The ‘focus questions’  give guidance in the design of questionnaires or interview questions.";

$ies_lang["b5"]["string13"] = "如先前設定的焦點問題未能乎合小貼士內所列的標準，請調整或重設你的焦點問題：";
$ies_lang["en"]["string13"] = "If the focus questions fail to meet the above standards, revise and reset them.";
?>