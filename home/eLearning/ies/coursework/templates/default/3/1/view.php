<?php
$parStudentAnswer = $current_step_answer_arr;
$submit1_display_html = "";
$submit2_display_html = "style=\"display:none\"";

if(sizeof($current_step_answer_arr) > 0 && is_array($current_step_answer_arr))
{
	//student have answer with this question


	if($redo == 1){
		//since this is a redo , empty student answer
		$parStudentAnswer = null;
	}
	else
	{
		$submit2_display_html = '';
		$submit1_display_html = "style=\"display:none\"";
	}
}

$html_question = generateQuestion($_question[1],$noOfOption,$parStudentAnswer);
?>
<script>

var question_answer_sperator = "<?=$ies_cfg["question_answer_sperator"]?>";  //question[question_answer_sperator]mark
var new_set_data_sperator = "<?=$ies_cfg["new_set_question_answer_sperator"]?>"; 
var comment_answer_sperator = "<?=$ies_cfg["comment_answer_sperator"]?>";
var comment_array = new Array();
var question_array = new Array();
var ans_array = new Array();
<?php
//generate JS array for question
for($i =0,$j = sizeof($_question[1]); $i < $j ; $i++)
{
	list($_questionCode, $_title, $_standardAns) = explode($ies_cfg["question_answer_sperator"],$_question[1][$i]); 
	echo "question_array[\"$_questionCode\"] = {$_standardAns};\n";
}

//generate JS array for comment
for($i = 0, $j = sizeof($_comment[1]); $i < $j; $i++)
{
	echo "comment_array[{$i}] = \"{$_comment[1][$i]}\";\n";
}
//generate JS array for answer string
for($i = 0, $j = sizeof($_ans_str); $i < $j; $i++)
{
	echo "ans_array[{$i}] = \"{$_ans_str[$i]}\";\n";
}
?>
function handleAnswer()
{
	var studentAllAns = "";  // FOR STORING ALL THE STUDENT SELECTED RADIO VALUE
	var storeMarkArry = new Array();       // store student mark  1--> with all correct , 0 --> with any one question is wrong , default is all wrong
	var dummy = " "; // for radio question type , to fit for the data structure 
	//APPEND all the student selected radio value to a studentAllAns
	$('input[name^=setQuestion1]:checked').each(function(key, val){

		var _stduentAns = $(this).val();
		  
		if(studentAllAns == ""){
			studentAllAns= _stduentAns;			
		}
		else{
			studentAllAns += new_set_data_sperator + _stduentAns;
		}
		var tmpArray = _stduentAns.split(question_answer_sperator);
		var answeringQuestionCode = tmpArray[0];
		var studentAnswer		  = tmpArray[1];
		storeMarkArry[key] = studentAnswer;
		//storeMarkArry[key] = (question_array[answeringQuestionCode] == studentAnswer)? 1 : 0 ;

	});
	
	//APPEND studentAllAns and caculated comment to a  hidden form field question[radio][1] for submitting answer
	try { 
		//studentFinalAns = studentAllAns  + comment_answer_sperator + dummy + question_answer_sperator + get_comment(storeMarkArry);
		studentFinalAns = studentAllAns  + comment_answer_sperator + dummy + question_answer_sperator + get_std_answer_str(storeMarkArry);
		$("input[name^=question[radio][1]]").val(studentFinalAns);	
	} 
	catch(err) { 
		alert(err.toString());  
	}	
}
function get_std_answer_str(std_ans_ary){
	var returnStr = "";
	for(i=0, j=std_ans_ary.length; i < j; i++)
	{
		returnStr += returnStr? "\n":"";
		returnStr += (i+1) + ". " + ans_array[std_ans_ary[i]-1];
	}
	
	return returnStr;
}

function get_comment(storeMarkArry)
{
	var returnStr = "";
	var withWrongAns = 1;  // default with wrong ans
	var i ; // store loop index
	var j ; // store size of storeMarkArry
	for(i =0, j = storeMarkArry.length; i < j; i++)
	{
		var _mark = storeMarkArry[i];
		if(_mark == 0){  // 0 ==> wrong answer
			withWrongAns = 1;  // once detect any answer is wrong , skip the checking
			break;
		}else{
			withWrongAns = 0;
		}
	}

	returnStr = (withWrongAns == 1) ? comment_array[1]: comment_array[0];

	return returnStr;
}
function ReDoStep(jParStepID,jParSchemID){
 	 	window.location = "coursework.php?step_id="+jParStepID+"&scheme_id="+jParSchemID+"&redo=1";
}

</script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
    	<div class="show_intro" style="width:100%">
    		<script>
    			function toggle_hint_block(hint_id){
    				obj = document.getElementById(hint_id);
					if(obj.style.display != 'none'){
						obj.style.display = 'none';
					}
					else{
						obj.style.display = 'block';
					}
				}
    		</script>
    		<a class="task_intro" href="javascript:toggle_hint_block('hint_content_01')"><?=$ies_lang[$_lang]["string18"]?></a>
			<p class="spacer"></p> 
			<div id="hint_content_01" class="intro_content" style="display:none;width:98%">
				<b><?=$ies_lang[$_lang]["string19"]?></b><br/>
				<?=$ies_lang[$_lang]["string20"]?>
				<br/><br/>
				<?=$ies_lang[$_lang]["string21"]?>
			</div>
		</div>
		<p class="spacer"></p><br/>
   		<?=$ies_lang[$_lang]["string1"]?>
   		<br/><br/>
   		<?=$ies_lang[$_lang]["string2"]?>
   		<br/><br/>
		<b><?=$ies_lang[$_lang]["string3"]?></b><br/>
		<b><?=$ies_lang[$_lang]["string4"]?></b>
		<br/><br/>
		<?=$ies_lang[$_lang]["string5"]?>
		<br/><br/>
		<?=$ies_lang[$_lang]["string6"]?>
		<br/><br/>
		<span class="task_instruction_text"><?=$ies_lang[$_lang]["string7"]?></span>
		<br/>
		
		<div class="IES_task_form">
        	<div class="wrapper" id="question_area">
				<?=$html_question?>
			</div>
        	<div class="view_result" id="submit1" <?=$submit1_display_html?>>
        		<input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="handleAnswer();doSaveStep(<?=$step_id?>,true);" value=" <?=$ies_lang[$_lang]["string14"]?> " />
        	</div>  
            <div class="view_result" id="submit2" <?=$submit2_display_html?>>
            	<input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="ReDoStep(<?=$step_id?>,<?=$scheme_id?>)" value=" <?=$ies_lang[$_lang]["string15"]?> " />
            </div>  
		</div>
		
    </div>

	<div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
      <div style="float:right; padding-top:5px">
        <!--input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" 儲存 " /-->
      </div>
    </div>
	<input type = "hidden" name = "question[radio][1]" id="question1" value="">
    <!-- End Content Here -->
  </div>
  

  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
  
  
</div> <!-- task_wrap end -->