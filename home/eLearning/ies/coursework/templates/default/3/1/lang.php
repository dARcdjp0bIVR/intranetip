<?php

$ies_lang["b5"]["string1"] = "「緒論」又名「前言」或「引言」，是研究報告的開端，作用是吸引讀者進入文章。";
$ies_lang["en"]["string1"] = "A preface, also known as a foreword, is the beginning of the enquiry report, which is used to attract readers to read further.";

$ies_lang["b5"]["string2"] = "你需要在這部分先敘述客觀的研究背景，從而帶出研究問題或議題的矛盾衝突，最後概括解釋研究目的。";
$ies_lang["en"]["string2"] = "You need to describe the objective background in this part first to bring out the enquiry questions or the conflict of the issue, then explain the enquiry objectives briefly.";

$ies_lang["b5"]["string3"] = "寫作步驟：";
$ies_lang["en"]["string3"] = "Writing procedures:";

$ies_lang["b5"]["string4"] = "簡述問題背景，說明研究動機，從背景帶出研究問題。";
$ies_lang["en"]["string4"] = "Briefly describe the background and motives of research to bring out the enquiry questions.";

$ies_lang["b5"]["string5"] = "「緒論」要求大家從描述客觀現象入題，所以分辨「客觀現象」與「個人評論」對於撰寫問題背景是頗為重要的。";
$ies_lang["en"]["string5"] = "The preface requires us to start off with a portrayal of the objective phenomena. It is therefore significant to distinguish “objective phenomena” from “personal comments”.";

$ies_lang["b5"]["string6"] = "分辨客觀現象與個人評論";
$ies_lang["en"]["string6"] = "Distinguishing objective phenomena from personal comments.";

$ies_lang["b5"]["string7"] = "試分辨以下哪些屬於客觀現象，哪些是個人評論:";
$ies_lang["en"]["string7"] = "Classify the following into objective phenomena and personal comments:";

$ies_lang["b5"]["string8"] = "客觀現象";
$ies_lang["en"]["string8"] = "Objective phenomena";

$ies_lang["b5"]["string9"] = "個人評論";
$ies_lang["en"]["string9"] = "Personal comments";

$ies_lang["b5"]["string10"] = "近年來，教育局不斷強調「求學不是求分數」的理念，但補習社依然作鋪天蓋地的宣傳，令不少香港中學生把讀書目標著眼於公開考試成績。因此每年補習社依然深受學生歡迎。";
$ies_lang["en"]["string10"] = "The Education Bureau has been focussing a lot on the rationale of ‘Learning, not scoring’. However, the overwhelming promotions of tutorial centres have caused many secondary students in Hong Kong to set their goals of study on public examination results. The outcome is that tutorial centres are still welcomed by students as ever.";

$ies_lang["b5"]["string11"] = "近年來，教育局不斷強調「求學不是求分數」的理念，這些不切實際的口號式宣傳漠視了學生求學的真正目的，是為了考取好成績及繼續升學。面對補習社鋪天蓋地的宣傳，很多香港中學生都把讀書變得功利化───求學其次，分數為先。";
$ies_lang["en"]["string11"] = "The Education Bureau has been focussing a lot on the rationale of ‘Learning, not scoring’. However, such impractical propaganda has ignored students’ ultimate learning objectives to strive for academic excellence and pursue further studies. In the face of the overwhelming promotions of tutorial centres, many secondary students in Hong Kong have taken a practical stand towards study - scoring before learning.";

$ies_lang["b5"]["string12"] = "評語：此段落雖然樸實無華，然而所描述乃客觀現象，能讓讀者對所研究話題有基本認識。";
$ies_lang["en"]["string12"] = "Comment: Although this is a plain paragraph, the objective phenomena described can provide readers with the basic knowledge of the enquiry.";

$ies_lang["b5"]["string13"] = "評語：「不切實際的口號式宣傳」是作者的主觀意見，此乃個人評論。由於讀者還沒有了解研究所得，在開頭部分不宜加入過度主觀的判斷，否則影響報告客觀性。";
$ies_lang["en"]["string13"] = "Comment: “Impractical propaganda” is a biased opinion of the writer and considered a personal comment. Since the readers do not know the research outcomes yet, it is inappropriate to include this kind of subjective judgement at the very start. Otherwise, the objectivity of the enquiry report will be affected.";

$ies_lang["b5"]["string14"] = "檢視練習結果";
$ies_lang["en"]["string14"] = "Check your answers";

$ies_lang["b5"]["string15"] = "重做練習";
$ies_lang["en"]["string15"] = "Redo the exercise";

$ies_lang["b5"]["string16"] = "全對！你已掌握何謂客觀現象、個人評論。下一步，你將要撰寫研究背景。";
$ies_lang["en"]["string16"] = "All correct! You have already known what objective phenomena and personal comments are. You have to write up the background information next.";

$ies_lang["b5"]["string17"] = "為使你掌握何謂客觀現象、個人評論，請再細心閱讀上述有關說明。";
$ies_lang["en"]["string17"] = "Read the above illustration carefully so as to master what objective phenomena and personal comments are.";

$ies_lang["b5"]["string18"] = "研究報告簡介";
$ies_lang["en"]["string18"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string19"] = "1. 緒論";
$ies_lang["en"]["string19"] = "1. Preface";

$ies_lang["b5"]["string20"] = "「緒論」即研究簡介，包括以下元素：背景資料、研究目的、焦點問題、文獻回顧、關鍵詞定義。 目的是引起讀者對探究話題的興趣，讓讀者對題目有了初步認識後，才展示深入分析。";
$ies_lang["en"]["string20"] = "A preface is a brief introduction of the enquiry that includes background information, enquiry objectives, focus questions, literature review and definitions of key words. The aim of the preface is to arouse the readers’ interest in the topic. After providing them with a brief introduction of the enquiry, in-depth analysis can be displayed.";

$ies_lang["b5"]["string21"] = "相信你在階段一撰寫探究計劃書時，已完成上述部分。你可在此階段，參考自己過往累積的成果，加以修改，擴展成文。";
$ies_lang["en"]["string21"] = "You should have finished the above when writing up the project proposal on completion of Stage 1. At this stage, you can refer to your previous work, modify and elaborate it to become a product.";
?>