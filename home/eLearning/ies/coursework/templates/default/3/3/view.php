<?php
$html_textarea = '<textarea style="width:100%; height:50px" name="question[textarea][1]">'.$answer[1].'</textarea> ';
$html_textareaWithTitle = libies_ui::getTextareaDisplayWithTitle($ies_lang[$_lang]["string23"], $html_textarea);
?>

<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
		<?=$ies_lang[$_lang]["string1"]?>
		<br/>
		<?=$ies_lang[$_lang]["string2"]?>
		<ul>
			<li><?=$ies_lang[$_lang]["string3"]?></li>
			<li><?=$ies_lang[$_lang]["string4"]?></li>
			<li><?=$ies_lang[$_lang]["string5"]?></li>
			<li><?=$ies_lang[$_lang]["string6"]?></li>
			<li><?=$ies_lang[$_lang]["string7"]?></li>
			<li><?=$ies_lang[$_lang]["string8"]?></li>
			<li><?=$ies_lang[$_lang]["string9"]?></li>
			<li><?=$ies_lang[$_lang]["string10"]?></li>
			<li><?=$ies_lang[$_lang]["string11"]?></li>
			<li><?=$ies_lang[$_lang]["string12"]?></li>
		</ul>
		<?=$ies_lang[$_lang]["string13"]?>
	 	<br />
	 	<?=$ies_lang[$_lang]["string14"]?>
		<ul>
			<li><?=$ies_lang[$_lang]["string15"]?></li>
			<li><?=$ies_lang[$_lang]["string16"]?></li>
			<li><?=$ies_lang[$_lang]["string17"]?></li>
			<li><?=$ies_lang[$_lang]["string18"]?></li>
			<li><?=$ies_lang[$_lang]["string19"]?></li>
		</ul>
		<?=$ies_lang[$_lang]["string20"]?>
		<span class="prev_answer"><?=$html_prev_task_answer_1?></span>
		<br /><br />
		<?=$ies_lang[$_lang]["string21"]?>
		<span class="prev_answer"><?=$html_prev_task_answer_2?></span>
		<br /><br />
		<?=$ies_lang[$_lang]["string22"]?>
		<span class="prev_answer"><?=$html_prev_task_answer_3?></span>
		<br /><br />
		
		<?=$html_textareaWithTitle?>

    	<div class="edit_bottom">
    		<span><?=$html_answer_lastUpdate?></span>
      		<div style="float:right; padding-top:5px">
        		<input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
      		</div>
    	</div>
    	<br class="clear" /><br class="clear" />
	</div>     
	<!-- End Content Here -->
</div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doSaveStep(<?=$next_step_id?>,true);" value="<?=$Lang['IES']['SaveAndNextStep']?>" />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->