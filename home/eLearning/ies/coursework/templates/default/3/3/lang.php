<?
$ies_lang["b5"]["string1"] = "撰寫「研究目的」時，需要說明你想透過是次研究獲得甚麼或達到甚麽目的。<br />你可參考以下角度，並結合題目思考。<br />";
$ies_lang["en"]["string1"] = "When writing up ‘enquiry objectives ‘, mention what you expect to achieve in the study.<br />Link up the question with the enquiry objectives with reference to the following perspectives.<br />";

$ies_lang["b5"]["string2"] = "本人希望透過是次研究：";
$ies_lang["en"]["string2"] = "In this enquiry, I expect to:";

$ies_lang["b5"]["string3"] = "<b><font color=#006400>研究</font></b>不同持分者對某件事的觀點和回應";
$ies_lang["en"]["string3"] = "<b><font color=#006400>study</font></b> the viewpoints and responses of various stakeholders about a certain issue.";

$ies_lang["b5"]["string4"] = "<b><font color=#006400>驗證</font></b>某種因果關係是否成立/多大程度上成立";
$ies_lang["en"]["string4"] = "<b><font color=#006400>verify</font></b> whether a particular causal relationship is justified / justified to a certain extent.";

$ies_lang["b5"]["string5"] = "<b><font color=#006400>分析</font></b>某種現象/問題對不同人的影響";
$ies_lang["en"]["string5"] = "<b><font color=#006400>analyse</font></b> the effect of a certain phenomenon / problem on various people.";

$ies_lang["b5"]["string6"] = "<b><font color=#006400>分析</font></b>在某件關鍵事件當中誰的角色最重要";
$ies_lang["en"]["string6"] = "<b><font color=#006400>analyse</font></b> who plays the crucial role in a certain key event.";

$ies_lang["b5"]["string7"] = "<b><font color=#006400>分析</font></b>某種現象/問題的成因";
$ies_lang["en"]["string7"] = "<b><font color=#006400>analyse</font></b> the rationale behind a certain phenomenon / problem.";

$ies_lang["b5"]["string8"] = "<b><font color=#006400>分析</font></b>某種現象下對不同人所帶來的機遇與挑戰";
$ies_lang["en"]["string8"] = "<b><font color=#006400>analyse</font></b> the opportunities and challenges a certain phenomenon brings about for various people.";

$ies_lang["b5"]["string9"] = "根據研究所得，為某種問題<b><font color=#006400>提出</font></b>可行的解決方法";
$ies_lang["en"]["string9"] = "<b><font color=#006400>provide</font></b> possible solutions for certain problems according to the research outcomes.";

$ies_lang["b5"]["string10"] = "<b><font color=#006400>評估</font></b>某種現有的解決方法是否有效";
$ies_lang["en"]["string10"] = "<b><font color=#006400>evaluate</font></b> the effectiveness of a certain existing solution.";

$ies_lang["b5"]["string11"] = "<b><font color=#006400>驗證</font></b>某個假設是否成立";
$ies_lang["en"]["string11"] = "<b><font color=#006400>verify</font></b> whether a certain hypothesis is justified.";

$ies_lang["b5"]["string12"] = "其他";
$ies_lang["en"]["string12"] = "others";

$ies_lang["b5"]["string13"] = "注意：你的研究題目可以包含多於一個研究目的，只須利用動詞，帶出實際目的。 <br />";
$ies_lang["en"]["string13"] = "Attention: There may be more than one research objective in your enquiry topic. <br />";

$ies_lang["b5"]["string14"] = "你可用以下句型，撰寫研究動機： <br />";
$ies_lang["en"]["string14"] = "You may use the following sentence patterns to write up the enquiry motives: <br />";

$ies_lang["b5"]["string15"] = "本報告將包括以下分析：";
$ies_lang["en"]["string15"] = "This report will include the following analyses:";

$ies_lang["b5"]["string16"] = "本報告將主要分析（）與（）的關係……";
$ies_lang["en"]["string16"] = "This report will mainly analyse the relationship between ( ) and ( )….";

$ies_lang["b5"]["string17"] = "本報告將討論（）……";
$ies_lang["en"]["string17"] = "( ) will be discussed in this report.";

$ies_lang["b5"]["string18"] = "本報告先分析（）與（）的關係，然後……";
$ies_lang["en"]["string18"] = "Firstly, this report will analyse the relationship between ( ) and ( ), and then….";

$ies_lang["b5"]["string19"] = "本報告將會探討（你研究的主題）";
$ies_lang["en"]["string19"] = "(Your enquiry topic) will be studied in this report.";

$ies_lang["b5"]["string20"] = "階段三撰寫的研究背景：<br/>";
$ies_lang["en"]["string20"] = "The background information written up at Stage 3:<br/>";

$ies_lang["b5"]["string21"] = "階段一撰寫的研究目的：<br/>";
$ies_lang["en"]["string21"] = "The enquiry objectives written up at Stage 1:<br/>";

$ies_lang["b5"]["string22"] = "階段一撰寫的相關概念和知識：<br/>";
$ies_lang["en"]["string22"] = "The related concepts and relevant knowledge written up at Stage 1:<br/>";

$ies_lang["b5"]["string23"] = "參考上述角度及資料，修正你的研究目的：<br/>";
$ies_lang["en"]["string23"] = "Revise your enquiry objectives with reference to the above perspectives and information.<br/>";
?>