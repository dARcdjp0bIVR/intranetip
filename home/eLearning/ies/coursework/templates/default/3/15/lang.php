<?php

$ies_lang["b5"]["string1"] = "分析結果是第三階段最重要的部分，表現好壞將直接影響第三階段的評分。";
$ies_lang["en"]["string1"] = "Analysis of findings is the most important part at Stage 3, and highly related to the marks given at this stage.";

$ies_lang["b5"]["string2"] = "你需要根據你搜集得來的資料作多角度分析，並歸納出有意義的結論。";
$ies_lang["en"]["string2"] = "The data collected has to go through multi-perspective analysis to infer a meaningful conclusion.";

$ies_lang["b5"]["string3"] = "高分小貼士";
$ies_lang["en"]["string3"] = "Tips for good grades";

$ies_lang["b5"]["string4"] = "分析研究結果時，要不時思考研究目的，看看你想寫的論點是否能與研究目的互相呼應，這樣會使邏輯更嚴謹。";
$ies_lang["en"]["string4"] = "Always keep the enquiry objectives in mind when analysing the research findings, and check that your assertions echo the enquiry objectives with good logic.";

$ies_lang["b5"]["string5"] = "所寫的論據要與焦點問題/假設互相聯繫。";
$ies_lang["en"]["string5"] = "Your premise should be interconnected with the focus questions/hypothesis.";

$ies_lang["b5"]["string6"] = "如情況許可，應盡量包含不同角度的觀點，並分析為甚麼會有這樣的觀點。";
$ies_lang["en"]["string6"] = "Multi-perspective viewpoints and the rationale behind should be included in the analysis if possible.";

$ies_lang["b5"]["string7"] = "如何分析要點？";
$ies_lang["en"]["string7"] = "How to analyse the research findings?";

$ies_lang["b5"]["string8"] = "階段一撰寫的探究題目：<br/>";
$ies_lang["en"]["string8"] = "The topic for enquiry written up at Stage 1:<br/>";

$ies_lang["b5"]["string9"] = "階段二撰寫的探究題目：<br/>";
$ies_lang["en"]["string9"] = "The topic for enquiry written up at Stage 2:<br/>";

$ies_lang["b5"]["string10"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string10"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string11"] = "階段三撰寫的焦點問題：<br/>";
$ies_lang["en"]["string11"] = "The focus questions written up at Stage 3:<br/>";

$ies_lang["b5"]["string12"] = "階段三撰寫的研究發現和結果：<br/>";
$ies_lang["en"]["string12"] = "The research findings and outcomes written up at Stage 3:<br/>";

$ies_lang["b5"]["string13"] = "撰寫你的研究分析：";
$ies_lang["en"]["string13"] = "Write up your research analysis:";

$ies_lang["b5"]["string14"] = "研究報告簡介";
$ies_lang["en"]["string14"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string15"] = "5. 分析和建議"; 
$ies_lang["en"]["string15"] = "5. Analysis and recommendation";

$ies_lang["b5"]["string16"] = "數據本身是客觀的，是研究員獨到的分析，找出數據背後隱藏的信息。";
$ies_lang["en"]["string16"] = "Data itself is objective. It is through the unique analysis of the researcher that the implications behind the data can be brought out.";

$ies_lang["b5"]["string17"] = "同學需要展示個人分析力，從數據裡看到一些獨特的觀點，並提出針對性的解決方法。";
$ies_lang["en"]["string17"] = "You need to show your analytical skills by establishing distinctive viewpoints from the data, and providing a targeted solution.";
?>