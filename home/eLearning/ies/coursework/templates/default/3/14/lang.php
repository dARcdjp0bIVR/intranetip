<?php

$ies_lang["b5"]["string1"] = "完成階段二後，你應該搜集並整理好一系列的資料/數據。在這部分，你可以運用這些整理好的資料，輔以文字論述，展示研究發現和結果。";
$ies_lang["en"]["string1"] = "On completion of Stage 2, you should have collected and organised a set of data/information. In this section, you can make use of the organised information to present the findings and outcomes in expository writing.";

$ies_lang["b5"]["string2"] = "描述圖表的數據不是單純為了要把圖像化的資料用文字表達出來，最重要是希望透過文字，引導讀者思考並分析研究結果。因此，描述時需要把數據當作例證，用以支持某論點，或推論出一個有意義的結果。";
$ies_lang["en"]["string2"] = "Using expository writing to present the data in graphs not only brings out the implications of graphic information, but more importantly guides the readers to think and analyse the findings. Therefore, the data should be used as evidence in your exposition to support certain assertions or derive meaningful results.";

$ies_lang["b5"]["string3"] = "數據描述與分析範例";
$ies_lang["en"]["string3"] = "An exemplar for data exposition and analysis";

$ies_lang["b5"]["string4"] = "論點、論據、論證是立論的要素。立論的方法主要有兩種：";
$ies_lang["en"]["string4"] = "Assertions, premises and evidence are essential elements of argumentation. There are two ways to establish argumentation:";

$ies_lang["b5"]["string5"] = "方法一";
$ies_lang["en"]["string5"] = "Method 1";

$ies_lang["b5"]["string6"] = "方法二";
$ies_lang["en"]["string6"] = "Method 2";

$ies_lang["b5"]["string7"] = "論點";
$ies_lang["en"]["string7"] = "Assertion";

$ies_lang["b5"]["string8"] = "論據";
$ies_lang["en"]["string8"] = "Premise";

$ies_lang["b5"]["string9"] = "論證";
$ies_lang["en"]["string9"] = "Evidence";

$ies_lang["b5"]["string10"] = "立論的例子";
$ies_lang["en"]["string10"] = "An example of argumentation";

$ies_lang["b5"]["string11"] = "按照方法二立論，綜合成文如下:";
$ies_lang["en"]["string11"] = "According to Method 2, the argumentation will be as follows:";

$ies_lang["b5"]["string12"] = "根據表一問卷調查結果顯示，八成受訪學生認同補習的主要原因是為了考取好成績，當然也有兩成受訪者是基於其他因素而補習，例如：父母要求、朋輩影響、追求課堂以外知識，但比例不算多。這顯示學生補習主要由現今考試制度促成，因為能否在香港升學主要取決於公開試的成績。因此，好成績是入大學的先決條件。而就表二所表示，七成到補習社的學生表示補習社能提供考試技巧，直接對考試成績有正面影響。";
$ies_lang["en"]["string12"] = "The survey findings in Table 1 show that 80% of the interviewees admit getting better grades to be the main reason for tutoring. Of course there are 20% of the interviewees who have other reasons for tutoring, e.g. parents’ demand, peer influence and acquiring knowledge outside classroom. However, the proportion is not high. This indicates that students opting for tutoring is mainly caused by the present examination system. Whether students can pursue further studies is mainly dependent on their public examination results, a prerequisite for entrance to university. Table 2 shows that 70% of the students receiving tutoring believe that examination techniques taught in tutorial classes have a positive effect on their examination results.";

$ies_lang["b5"]["string13"] = "常見錯誤";
$ies_lang["en"]["string13"] = "Common errors";

$ies_lang["b5"]["string14"] = "同學常常按問卷題目的次序來描述結果，這樣反映他們未真正了解例證的意義。既然花了那麼多精力去做問卷及取得各種數據，何不細心想想哪些數據能成為例證，加強論點的說服力？";
$ies_lang["en"]["string14"] = "Some students describe the survey findings in the same order as the questions in the questionnaire, which reflects that they do not fully understand the meaning of evidence.Since enormous effort has been put in the survey and various data collected, why not select the appropriate data as evidence to make a more convincing argument?";

$ies_lang["b5"]["string15"] = "寫作小貼士 ";
$ies_lang["en"]["string15"] = "Writing hints";

$ies_lang["b5"]["string16"] = "階段二撰寫的搜集數據及整理結果：<br/>";
$ies_lang["en"]["string16"] = "The data collected and research outcomes written up at Stage 2：<br/>";

$ies_lang["b5"]["string17"] = "以下是你在階段二搜集的資料，請按「管理資料」作出適當的修訂。<br/>";
$ies_lang["en"]["string17"] = "The following is the data/information collected at Stage 2, please click 'Manage data' to revise them.<br/>";

$ies_lang["b5"]["string18"] = "你不但要描述及歸納關鍵的數據，還要撰寫研究發現，分析數據支持了甚麼論點。";
$ies_lang["en"]["string18"] = "You should not only describe and integrate key data, but also write up research findings and analyse the data to see what assertions have been made.";

$ies_lang["b5"]["string19"] = "研究報告簡介";
$ies_lang["en"]["string19"] = "A brief introduction of the enquiry report";

$ies_lang["b5"]["string20"] = "4. 研究結果";
$ies_lang["en"]["string20"] = "4. Research outcomes";

$ies_lang["b5"]["string21"] = "這部分需要你敘述和歸納所搜集到的第一手資料，並加以分析，提出富說服力的論點。";
$ies_lang["en"]["string21"] = "You have to describe and generalise the first-hand information collected in this section with analysis and convincing arguments.";

$ies_lang["b5"]["string22"] = "請在呈交課業前先完成自我檢測，確保內容合乎以下要求。如要修改，請返回上一步：";
$ies_lang["en"]["string22"] = "Finish the self-check to ensure all the following requirements are met before submitting the ‘Task’. If any revision is required, return to the previous step.";

$ies_lang["b5"]["string23"] = "研究結果回應了焦點問題。";
$ies_lang["en"]["string23"] = "The research outcomes address the focus questions.";

$ies_lang["b5"]["string24"] = "研究結果回應了研究目的。";
$ies_lang["en"]["string24"] = "The research outcomes address the research objectives.";

$ies_lang["b5"]["string25"] = "研究結果有經過歸納和整合，並非純虛描述結果。";
$ies_lang["en"]["string25"] = "The research outcomes have been summarised and integrated, not purely descriptions.";

$ies_lang["b5"]["string26"] = "研究結果能帶出一些重要信息，使研究變得有意義。";
$ies_lang["en"]["string26"] = "The research outcomes have brought out important messages to make the enquiry significant.";

?>