<style>
#blankRow {display: none}
table {empty-cells:show}
</style>
<script>
$(document).ready(function(){
//	initializeAnswerArray("<?= addslashes($answer[1]) ?>");
//	initializeResultTable();
});

/******************************************************************************************
	Functions in this page can be categorize into the followings,
	please put the functions under one of the category.
	If you need to create a new category, just follow the format to create.
	- Global Variables
	- Initialization Functions
	- Answer Conversion Manipulation Functions
	- Actions Binding
	- Others
******************************************************************************************/

////////////////////////////////////////////////////
//	Start: Global Variables
////////////////////////////////////////////////////
var rowIndex=0;
var answerArray = new Array();
var $currentEditingElement;
////////////////////////////////////////////////////
//	End: Global Variables
////////////////////////////////////////////////////


////////////////////////////////////////////////////
//	Start: Initialization Functions
////////////////////////////////////////////////////
function initializeAnswerArray(ParAnswer) {
	// put the answer to question field
	$("input[name=question\\[table\\]\\[1\\]]").val(ParAnswer);
	answerArray = ConvertAnswerToAnswerArray(ParAnswer);
}


function initializeResultTable() {	
	$.each(answerArray, function(RowPos, RowElement) {
		createRow(++rowIndex, RowElement, $("#addRowTr"));
	});
	bindAddBtn();
	bindSaveAndNext();
}
//////////////////////////////////////////////////
//	End: Initialization Functions
//////////////////////////////////////////////////

////////////////////////////////////////////////////
//	Start: Answer Conversion Manipulation Functions
////////////////////////////////////////////////////
function ConvertAnswerToAnswerArray(ParAnswer) {
	returnAnswerArray = new Array();
	if (ParAnswer) {
		// Break down the answer to array to store
		var RowArray = new Array();
		RowArray = ParAnswer.split("<?=$ies_cfg["new_set_question_answer_sperator"]?>");
		
		$.each(RowArray, function(key, element) {
			BoxArray = element.split("<?=$ies_cfg["question_answer_sperator"]?>");
			returnAnswerArray[returnAnswerArray.length] = BoxArray;
		});
	}
	return returnAnswerArray;
}
function ConvertAnswerFromArrayToString(ParAnswerArray) {

	// Remove Items that deleted by users
	var length = ParAnswerArray.length;
	tempArray = new Array();
	for(i=0;i<length;i++) {
		if (ParAnswerArray[i] == null) {
			// do nothing
		} else {
			tempArray[tempArray.length] = ParAnswerArray[i];
		}
	}
	
	rowString = new Array();
	$.each(tempArray, function(key, element) {
		rowString[rowString.length] = element.join("<?=$ies_cfg["question_answer_sperator"]?>");
	});
	
	concatedContent = rowString.join("<?=$ies_cfg["new_set_question_answer_sperator"]?>");
	
	return concatedContent;
}
////////////////////////////////////////////////////
//	End: Answer Conversion Manipulation Functions
////////////////////////////////////////////////////


////////////////////////////////////////////////////
//	Start: Actions Binding
////////////////////////////////////////////////////
/* Bind Add Edit Row Function */
function bindAddBtn() {
	var $addRowBtn = $(".IES_task_form #addRow");
	$addRowBtn.bind('click',function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		createEdit($("#addRowTr"));
	});
}

/* Bind Edit Function */
function bindEdit(ParRowIndex) {
	var currentEditId = 'edit_'+ ParRowIndex;
	$('#'+currentEditId).click(function() {
		if ($currentEditingElement) {
			Cancel($currentEditingElement.parent().parent());
		}
		$currentEditingElement = $("#"+currentEditId);
		$currentEditingRow = $currentEditingElement.parent().parent().parent();
		createEdit($currentEditingRow, true, ParRowIndex);
		
		currentAnswer = answerArray[ParRowIndex-1];
		$editRow = $("input", $(".newRow"));
		$.each($editRow, function(key, element) {
			$(element).val(currentAnswer[key]);
		});
		$currentEditingRow.hide();
	});
}

/* Bind Remove Function */
function bindRemove(ParRowIndex) {
	var currentRemoveId = 'remove_'+ ParRowIndex;
	$('#'+currentRemoveId).click(function() {
		UpdateAnswerArray("Remove",ParRowIndex);
		Save(answerArray);
		$removeElement = $("#"+currentRemoveId).parent().parent().parent().remove();
	});
}

/* Bind Cancel Function */
function bindCancel() {
	$(".cancel").click(function() {
		Cancel();
	});
}

/* Bind Save and Next Function */
function bindSaveAndNext(ParInsertBefore, ParIsOldRecord, ParEditIndex) {
	$("#saveAndNext").unbind("click");
	$("#saveAndNext").click(function() {
		if (ParIsOldRecord) {
			updateType = "Update";
		} else {
			updateType = "New";
		}
		if ($(".newRow").length) {
			contentArray = getContentFromRow($(".newRow"));
			UpdateAnswerArray(updateType, ParEditIndex, contentArray);
		}
		Save(answerArray);
		doStep(<?=$next_step_id?>);
	});
}

/* Bind Saving Function */
function bindSave(ParInsertBefore, ParIsEdit, ParEditIndex) {
	$('.save').click(function() {	
		$targetRow = $(this.parentNode.parentNode);
		SaveOrUpdate(ParInsertBefore, ParIsEdit, ParEditIndex, $targetRow);
	});
}
////////////////////////////////////////////////////
//	End: Actions Binding
////////////////////////////////////////////////////


////////////////////////////////////////////////////
//	Start: Others
////////////////////////////////////////////////////
function createEdit(ParInsertBefore, ParIsOldRecord, ParEditIndex) {
	

	var $table = $("#addWorkTable");
	var $tableBody = $("tbody",$table);
	var blankRowID = "blankRow";
	var newRowClass = "newRow";
	var hiddenClass = "hidden"
	$('#addRowTr').hide();
	newClasses = hiddenClass+" "+newRowClass;
	//clone the blank row, put the clone at the top, set the correct classes, remove the ID, animate the divs inside
	//normally I'd use .addClass(), but I want the classes I set to replace the current set classes, so I use .attr("class") to overwrite the classes
	newRow = $("#"+blankRowID,$tableBody).clone(true).insertBefore(ParInsertBefore).attr("class",newClasses).removeAttr("id").show().find(ParInsertBefore);
	
	bindSave(ParInsertBefore, ParIsOldRecord, ParEditIndex);
	bindCancel();
	bindSaveAndNext(ParInsertBefore, ParIsOldRecord, ParEditIndex);
	return false; //kill the browser default action
}


// Create a row with row index and answer array of a row and specify insert before where
function createRow(ParRowIndex, ParElementArray, ParInsertBefore) {
	var row = "<tr><td class=\"num_check\">"+ParRowIndex+"</td>";
	
	$.each(ParElementArray, function(key, element) {
		if (element == '') {
			element = '&nbsp;';
		}
		row = row + "<td>" + element + "</td>";
	});	
	
	row = row + "<td class=\"tool_col_min\" nowrap><div class=\"table_row_tool\"><a href=\"javascript: void(0);\" class=\"edit_dim\" title=\"Edit\" id=\"edit_" + ParRowIndex + "\"></a><a href=\"javascript: void(0);\" class=\"delete_dim\" title=\"Delete\" id=\"remove_" + ParRowIndex + "\"></a></div></td></tr>";
	$(row).insertBefore(ParInsertBefore);
	bindEdit(ParRowIndex);
	bindRemove(ParRowIndex);
	return row;
}


function Save(ParAnswerArray) {

	concatedContent = ConvertAnswerFromArrayToString(ParAnswerArray);
	
	$("input[name=question\\[table\\]\\[1\\]]").val(concatedContent);

	result = false;
	$.ajax({
	    url:      "step_update.php",
	    type:     "POST",
	    data:     $("#form1").serialize(),
	    async:	  false,
	    error:    function(xhr, ajaxOptions, thrownError){
	                alert(xhr.responseText);
	              },
	    success:  function(xml){
                    date = $(xml).find("date").text();
                    $("#lastModifiedDate").html(date);
					result = true;
	              }
	});
	$(".save").unbind("click");
	return result;
}

function UpdateEditRemoveIds(ParUpdateIndex) {
	UpdateElementIds(ParUpdateIndex,"edit_");
	UpdateElementIds(ParUpdateIndex,"remove_");
}

function UpdateElementIds(ParUpdateIndex,ParTargetIndicator) {
	$targets = $("a[id*="+ParTargetIndicator+"]");
	$.each($targets, function(key, element) {
		var splitedId = $(element).attr("id").split("_");
		var IdNum = splitedId[1];
		if (IdNum>ParUpdateIndex) {
			$(element).attr("id", ParTargetIndicator+(IdNum-1));
			bindRemove(IdNum-1);
		}
	});
}

function UpdateAnswerArray(ParUpdateType,ParUpdateIndex,ParUpdateElement) {
	switch (ParUpdateType.toUpperCase()) {
		case "UPDATE":
			answerArray[ParUpdateIndex-1] = ParUpdateElement;
			break;
		case "REMOVE":
			answerArray[ParUpdateIndex-1] = null;
			break;
		default:	// NEW
			answerArray[answerArray.length] = ParUpdateElement;
			break;
	}
}


function getContentFromRow($ParTargetRow) {
		// Catch Content From Updating Row
		$contentElement = $("input[name=content1]",$ParTargetRow[0]);
	
		var contentArray = new Array();		
		$.each($contentElement, function(index,value) {
			contentArray[contentArray.length] = $(value).val();
		});
		return contentArray;
}

function SaveOrUpdate(ParInsertBefore, ParIsEdit, ParEditIndex, $targetRow) {
	
		contentArray = getContentFromRow($targetRow);
		
		if (ParEditIndex) {
			updateType = "Update";
		} else {
			updateType = "New";
		}
		UpdateAnswerArray(updateType, ParEditIndex, contentArray);

		if (Save(answerArray)) {
			if (isNaN(ParEditIndex)) {
				createRowIndex = ++rowIndex;
			} else {
				createRowIndex = ParEditIndex;
			}
			
            createRow(createRowIndex, contentArray,ParInsertBefore);
            if (ParIsEdit) {
            	ParInsertBefore.remove();
            }
            $targetRow.remove();
            $('#addRowTr').show();
		}
}

function Cancel() {
	$(".newRow").remove();
	if ($currentEditingElement) {
		$currentEditingRow = $currentEditingElement.parent().parent().parent();
		$currentEditingRow.show();
	}
    $('#addRowTr').show();
}

/* event listener, submit form when enter */
$(document).ready(function() {
	var enterTarget = $("input[name=keyword].keyword_box");
	
	enterTarget.keydown(function(event) {
		if (event.keyCode == '13') { // The charCode for "Enter"
			event.preventDefault(); // prevent keydown default action
			document.form1.submit();
		}
	});
});
////////////////////////////////////////////////////
//	End: Others
////////////////////////////////////////////////////
////////////////////////////////////////////////////
// Table - a table allow users to edit each row to save answers to DB: only on IES
// @param String targetDisplayId - the area's id that this table is going to display at
// @param String name - the name for this table
// @param String answer - the raw answer
// @param Array headerArray - the array storing the header that the table will display in order
// @param String rowDelimiter - the delimiter to split answer to row (tr)
// @param String fieldDelimiter - the delimiter to split answer to box (td)
// @param boolean isEditable - true, allow New, Edit, Remove answers
//////////////////////////////////////////////////

function Table(targetDisplayId, name, answer, headerArray, rowDelimiter, fieldDelimiter, isEditable) {
	
	
		this.name = name;
		this.answer = answer;
		this.headerArray = headerArray;
		this.answerArray = ConvertAnswerToAnswerArray(this.answer);
					
		
		this.ConvertAnswerToAnswerArray = ConvertAnswerToAnswerArray;
		function ConvertAnswerToAnswerArray(ParAnswer) {
			returnAnswerArray = new Array();
			if (ParAnswer) {
				// Break down the answer to array to store
				var RowArray = new Array();
				RowArray = ParAnswer.split(rowDelimiter);
				
				$.each(RowArray, function(key, element) {
					BoxArray = element.split(fieldDelimiter);
					returnAnswerArray[returnAnswerArray.length] = BoxArray;
				});
			}
			return returnAnswerArray;
		}
		
		
		
		this.GenerateTable = GenerateTable;
		function GenerateTable() {
			$table = $("<table></table>")
						.attr("class","common_table_list IES_form_table")
						.append(
								$("<col></col>")
								)
						.append(
								$("<col></col>")
									.attr("class","field_title")
								)
						.append(
								$("<thead></thead>")
									.append(
											$("<tr></tr>")
											)
								)
						.append(
								$("<tbody></tody>")
								);
			/////////////////////////////////
			// Append the header
			$.each(headerArray, function(key, element) {
				$table.children("thead>tr").append($("<th>"+element"</th>"));
			});
			///////////////////////////////
			
			
			///////////////////////////////
			// Append data
			$.each(this.answerArray, function(rKey, rAnswer) {
				$currentTr = $("<tr></tr>");
				$.each(rAnswer, function(bKey, bAnswer) {
					$currentTr.append("<td>"+bAnswer+"</td>");
				});
				$table.children("tbody").append($currentTr);
			});
			///////////////////////////////

			$temp = $("#"+targetDisplayId).append($table);
		}
		
		// User Actions				
		// function include [New, Edit, Save, Cancel, Remove]
		this.New = New;
		this.Edit = Edit;
		this.Save = Save;
		this.Cancel = Cancel;
		this.Remove = Remove;
		
		function New() {
			alert("New");
		}
		function Edit() {
			alert("Edit");
		}
		function Save() {
			alert("Save");
		}
		function Cancel() {
			alert("Cancel");
		}
		function Remove() {
			alert("Remove");
		}


}

$(document).ready(function() {
	var headerArray = ["header1","header2"];
	Table = new Table('testing','tablename','<?=addslashes($answer[1])?>',headerArray,"<?=$ies_cfg["new_set_question_answer_sperator"]?>","<?=$ies_cfg["question_answer_sperator"]?>",true);
//	alert(Table.answerArray);
	Table.GenerateTable();
//	alert($(".IES_form_table>tbody>tr>	td.num_check").attr("class"));
});
</script>
<script type="text/javascript" src="../../../../../../../templates/jquery/jquery.datepick.js"></script>
<link href="../../../../../../../templates/jquery/jquery.datepick.css" rel="stylesheet" type="text/css">


<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

    <?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
      選擇研究方法後，你需要訂立一個詳細的探究計劃，儘量列明各項工作的細節及量化每項工作。例如，「分派四十份問卷給中四同學填寫」、「整理二十分鐘訪答為文字記錄」、「完成200字的反思記錄」等工作細節。此外，也要為每項工作定下完成日期。<br /><br />
      有了詳細的工作計劃，在日後進行探究工作時，更易掌握自己的工作進度。<br /><br />
      注意：填寫工作計劃時間表，應訂立與個人能力相符的工作計劃，也要注意呈交功課的期限及每個階段的工作重點。 <br /><br />
      工作目的參考列表︰
      <ul>
        <li>擬訂研究題目</li>
        <li>確定研究目的</li>
        <li>確定相關概念</li>
        <li>訂立焦點問題</li>
        <li>撰寫文獻回顧</li>
        <li>選擇研究方法</li>
        <li>擬寫研究計劃</li>
        <li>收集資料數據</li>
        <li>歸納整理數據</li>
        <li>分析資料數據</li>
        <li>歸納研究發現</li>
        <li>撰寫研究建議</li>
      </ul>
    </div>
    
    <span class="task_instruction_text"><?=$Lang['IES']['PleaseFillInTheWorkPlanSchedule']?><?=$Lang["_symbol"]["colon"]?></span>
    
	<div class="IES_task_form" id="testing"> <!-- IES_task_form start -->
<!--	    <table class="common_table_list IES_form_table" id="addWorkTable">
	    <col class="" />
	    <col class="field_title" />
	    <thead>
		<?=$question_tr_fields?>
		</thead>
	    <tbody>
	    <tr id="blankRow">
		    <td class="num_check">#</td>
		    <td><span class="row_content"><input name="content1" type="text" id="textfield2" class=""/>
		    </span></td>
		    <?/*<td><span class="row_content"><input name="content1" type="text" id="textfield2" class="textbox_date"/>
		    </span><div class="table_row_tool"><a href="#" class="select_date" title="Select"></a></div></td>*/?>
		    <?/*<td><span class="row_content"><input name="content1" id="content1" type="text" id="textfield2" class="textbox_date"/>
		    <?=$linterface->GET_CALENDAR("form1", "content1")?></span></td>*/?>
		    <td><input name="content1" class="input_long" type="text" /></td>
		    <td><input name="content1" class="input_long" type="text" /></td>
		    <td class="tool_col_min" nowrap><input name="submit2" type="button" class="formbutton save" onmouseover="this.className='formbuttonon save'" onmouseout="this.className='formbutton save'" value=" <?=$Lang['IES']['Save']?> "/>&nbsp;&nbsp;<input name="submit2" type="button" class="formsubbutton removeRow cancel" onmouseover="this.className='formsubbuttonon removeRow cancel'" onmouseout="this.className='formsubbutton removeRow cancel'" value=" 取消 "/></td>
	    </tr>
	    <tr id="addRowTr"><td colspan="5" style="text-align:right" ><a href="javascript: void(0);" class="add_row" id="addRow"><?=$Lang['IES']['AddRow']?></a></td></tr>
	    </tbody>
	    </table>
-->
	    </div>
	    <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
    </div> <!-- IES_task_form end --->
     
    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$prev_step_id?>);" value=" <?=$Lang['IES']['LastStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" id="saveAndNext" value="<?=$Lang['IES']['SaveAndNextStep']?>" />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?>" />
  </div></div>
  <input type="hidden" value="" name="question[table][1]" />
</div> <!-- task_wrap end -->