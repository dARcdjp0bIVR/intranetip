﻿<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<script>
$(document).ready(function() {

	$("#sType_1").addClass("current");
	$("#area_sType_1").show();

	$('.surveyType').click(function(){
		
		$("#area_sType_1").hide();
		$("#area_sType_2").hide();
		$("#area_sType_3").hide();				

		$("#sType_1").removeClass("current");
		$("#sType_2").removeClass("current");
		$("#sType_3").removeClass("current");
		
		var _id = $(this).attr("id");
		var _displayElement = 'area_'+_id;

		$("#"+_displayElement).show();
		$(this).addClass("current");

		
	});
});
</script>

<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  <?=$html_answer_display?>
  
    <!-- Start Content Here -->
    <div class="task_content">
    	<div class="show_intro" style="width:100%">
    		<script>
    			function toggle_hint_block(hint_id){
    				obj = document.getElementById(hint_id);
					if(obj.style.display != 'none'){
						obj.style.display = 'none';
					}
					else{
						obj.style.display = 'block';
					}
				}
    		</script>
    		<a class="task_intro" href="javascript:toggle_hint_block('hint_content_01')"><?=$ies_lang[$_lang]["string19"]?></a>
			<p class="spacer"></p> 
			<div id="hint_content_01" class="intro_content" style="display:none;width:98%">
				<b><?=$ies_lang[$_lang]["string20"]?></b><br/>
				<?=$ies_lang[$_lang]["string21"]?>
			</div>
		</div>
		<p class="spacer"></p><br/>
		<?=$ies_lang[$_lang]["string1"]?><br/><br/>
		<?=$ies_lang[$_lang]["string2"]?><br/><br/>
		<a href="tips/stage3_lightbox_E.php?KeepThis=true&amp;TB_iframe=false&amp;height=575&amp;width=751&amp;modal=true" class="thickbox task_demo"><?=$ies_lang[$_lang]["string3"]?></a>
		<br/><br/>
		<b><?=$ies_lang[$_lang]["string4"]?></b>
		<br/>
		<div class="method_board_sample">
		    <div class="method_board">
		        <h3><?=$ies_lang[$_lang]["string5"]?></h3>
		        <span class="m0001"><?=$ies_lang[$_lang]["string7"]?></span>
		        <span class="m0002"><?=$ies_lang[$_lang]["string8"]?></span>
		        <span class="m0003"><?=$ies_lang[$_lang]["string9"]?></span>
		    
		    </div>
		</div>
		<div class="method_board_sample">
			<div class="method_board">
		        <h3><?=$ies_lang[$_lang]["string6"]?></h3>
		        <span class="m0003"><?=$ies_lang[$_lang]["string9"]?></span>    
		        <span class="m0001"><?=$ies_lang[$_lang]["string7"]?></span>
		        <span class="m0002"><?=$ies_lang[$_lang]["string8"]?></span>
		    </div>
		</div>
		<p class="spacer"></p>
		<br/>
		<a href="tips/stage3_lightbox_E2.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo"><?=$ies_lang[$_lang]["string10"]?></a>
		<br/><br/>
		<b><?=$ies_lang[$_lang]["string11"]?></b>
		<br/>
		<?=$ies_lang[$_lang]["string12"]?>
 		<br/><br/>
		<div class="show_hint">
			<a class="task_hint" href="javascript:void(0)" onclick="toggle_hint_block('hint_content_02')"><?=$ies_lang[$_lang]["string13"]?></a>
			<p class="spacer"></p> 
			<div style="display: none;" id="hint_content_02" class="hint_content">
				<?=$ies_lang[$_lang]["string14"]?>
			</div>
		</div>
		<p class="spacer"></p>
		<br/>
		<a href="tips/stage3_lightbox_E3.php?KeepThis=true&amp;TB_iframe=false&amp;height=480&amp;width=751&amp;modal=true" class="thickbox task_demo"><?=$ies_lang[$_lang]["string15"]?></a>
		<br/><br/>
		<p>
		<?php/*
    		<?=$ies_lang[$_lang]["string16"]?>
    		<?=$html_prev_task_answer_1?>
    		<br/><br/>
    	*/?>
    		<?=$ies_lang[$_lang]["string17"]?>
    		<?//=$html_prev_task_answer_2;?>
			<?=$html_table?>
    		<br/><br/>
    	</p>
    	<?=$ies_lang[$_lang]["string22"]?>
		<br/><br/>
    	<div class="check_list">
			<ul>
				<li><?=$ies_lang[$_lang]["string23"]?></li>
				<li><?=$ies_lang[$_lang]["string24"]?></li>
				<li><?=$ies_lang[$_lang]["string25"]?></li>
				<li><?=$ies_lang[$_lang]["string26"]?></li>
			</ul>
		</div>
		<p class="spacer"></p><br/>
		<?=$html_SurveyMapping?><br/>
		<textarea name="question[textarea][1]" style="display:none"><?=$task_ans_str?></textarea>
	</div>     
	<!-- End Content Here -->
  </div>
  <div class="task_writing_bottom"><div>
    <input id="save_submit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Submit']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />                                      
  </div></div>
  
</div> <!-- task_wrap end -->