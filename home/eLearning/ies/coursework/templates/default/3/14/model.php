<?php
/*
$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 2, "datacollect");
$html_prev_task_answer_1 = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));
$html_prev_task_answer_1 = $html_prev_task_answer_1? $html_prev_task_answer_1 : '--';

$target_taskID = $objIES->getTaskIDByTaskInfo($scheme_id, 2, "datarelation");
$html_prev_task_answer_2 = nl2br($objIES->gettaskAnswer($target_taskID, $UserID));
$html_prev_task_answer_2 = $html_prev_task_answer_2? $html_prev_task_answer_2 : '--';
*/

$parentsInfo = current($objIES->getParentsIds("task",$task_id));

$requiredStageSeq = 2;
$requiredTaskCode = "datarelation";
$requiredTaskID = $objIES->getTaskIDByTaskInfo($parentsInfo["SCHEMEID"],$requiredStageSeq,$requiredTaskCode);

$requiredTaskParentsInfo = current($objIES->getParentsIds("task",$requiredTaskID));


$SurveyDetailArray = $objIES->getTastAllSurveyDetail($requiredTaskParentsInfo["STAGEID"], $UserID, $requiredTaskID);

$IsStage3 = true;
$SurveyDetails = $objIES->handleSurveyURLDetails($SurveyDetailArray,$IsStage3);


//$html_table = libies_ui::getQuestionCollectTableDisplay($SurveyDetailArray,$IsStage3,$task_id);

//Hard code this stage is 3
$thisStageNo = 3;
$html_table = libies_ui::getQuestionnaireTableDisplayWithGuide($SurveyDetails,$ies_cfg['Questionnaire']['SurveyAction']['COLLECT'],$ies_cfg['Questionnaire']['AnswerTableDisplayLocation']['INSIDE'],$thisStageNo);

$Survey_info_arr = $objIES->getStudentSurveyMappingInfoBySchemeID($scheme_id, $UserID);
//debug_r($Survey_info_arr);
$html_SurveyMapping = libies_ui::getSurveyMappingInfoDisplay($Survey_info_arr);

# Process Task Ans
$task_ans_str = "";
$Survey_info_arr_keys = array_keys($Survey_info_arr);
$survey_title_arr = array($Survey_info_arr_keys[0]=>$Lang['IES']['SurveyTable']['string13'],
						$Survey_info_arr_keys[1]=>$Lang['IES']['SurveyTable']['string14'],
						$Survey_info_arr_keys[2]=>$Lang['IES']['SurveyTable']['string15']);
foreach($Survey_info_arr as $key=>$Survey_info){
	for($j = 0, $j_max = count($Survey_info); $j < $j_max; $j++){
		$_details = $Survey_info[$j];
		if(count($_details["Mapping"])){
			$task_ans_str .= $survey_title_arr[$key]." - ".$_details["Title"]."\n";
			for($i=0;$i<count($_details["Mapping"]);$i++){
				$task_ans_str .= ($i+1)." ) ".$_details["Mapping"][$i]["MappingTitle"]." : ".$_details["Mapping"][$i]["Comment"]."\n";
			}
			$task_ans_str .= "\n";
		}
	}
}

$html_templateFile = "view.php";		
?>
