﻿<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<div class="task_steps">
  <ul class="line">
    <?=$html_task_chain?>
  </ul>
</div>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
      <?=$html_answer_display?>
    
    <div class="task_content">
    <?=$ies_lang[$_lang]["string1"]?><br /><br />
      
     <?=$ies_lang[$_lang]["string2"]?><br /><br />
    </div>
    
    <span class="task_instruction_text"><?=$ies_lang[$_lang]["string3"]?></span>
    <textarea name="question[textarea][1]" style="width:100%; height:50px"><?=$answer[1]?></textarea>
    
    <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
      <div style="float:right; padding-top:5px">
        <input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
      </div>
    </div>
    <br class="clear" /><br class="clear" />
    
  </div>
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$prev_step_id?>);" value=" <?=$Lang['IES']['LastStep']?> " />
    <input id="save_submit" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Submit']?> " />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />                                      
  </div></div>
</div> <!-- task_wrap end -->