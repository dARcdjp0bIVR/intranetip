<?
$ies_lang["b5"]["string1"] = "相信你已重新整理好報告內「研究背景」、「研究目的」及「焦點問題」等部分，為使你的緒論內容更能互相呼應，以下提供有關緒論部分的寫作例子予以參考：";
$ies_lang["en"]["string1"] = "By this time, you should have re-organised the ‘background information’, ‘enquiry objectives’ and ‘focus questions’ in your report already. To maximise the echo effect of the preface on the content, the following examples can be taken for reference:";

$ies_lang["b5"]["string2"] = "「緒論」寫作步驟例子";
$ies_lang["en"]["string2"] = "Examples of ‘Preface’";

$ies_lang["b5"]["string3"] = "以下是你在階段三重新整理的報告內容：背景資料、研究目的、焦點問題";
$ies_lang["en"]["string3"] = "Your report revised for Stage 3 should consist of the following: Background information, Enquiry objectives, Focus questions:";

$ies_lang["b5"]["string4"] = "階段三撰寫的研究背景：<br/>";
$ies_lang["en"]["string4"] = "The background information written up at Stage 3:<br/>";

$ies_lang["b5"]["string5"] = "階段三撰寫的研究目的：<br/>";
$ies_lang["en"]["string5"] = "The enquiry objectives written up at Stage 3:<br/>";

$ies_lang["b5"]["string6"] = "階段三撰寫的焦點問題：<br/>";
$ies_lang["en"]["string6"] = "The focus questions written up at Stage 3:<br/>";

$ies_lang["b5"]["string7"] = "寫作小貼士 ";
$ies_lang["en"]["string7"] = "Writing hints";

$ies_lang["b5"]["string8"] = "當你完成整份報告後，或許會出現「結論」與「緒論」的焦點有一定差異，所以你要在提交報告前細心檢視前後的邏輯聯繫，確保首尾呼應，邏輯一致。";
$ies_lang["en"]["string8"] = "When the report is done, you may notice discrepancies in the focuses between ‘Conclusion’ and ‘Preface’. Therefore, check the report carefully again before submission to ensure sound logic relationships and consistency.";

$ies_lang["b5"]["string9"] = "參考「研究緒論」的寫作步驟後，試整合你的「研究緒論」：";
$ies_lang["en"]["string9"] = "Now construct your ‘Preface ’with reference to the writing procedures.";
?>