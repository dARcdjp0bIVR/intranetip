<?

$ies_lang["b5"]["string1"] = "擬訂題目後，你需要寫一個簡短的副標題，來說明題目的探究重點、對象、時間等重要元素，讓讀者更清楚題目背後的含義。";
$ies_lang["en"]["string1"] = "Having the topic formulated, you have to write a short subtitle to elaborate on the important elements such as the enquiry scope, research objects and time. With the subtitle, readers will understand the implication of the topic.";

$ies_lang["b5"]["string2"] = "假設你的題目是「活化大澳，是建設還是破壞？」。讀者看了題目後很難理解當中的研究重點。因此，應加上適當的說明及描述，例如︰「活化大澳，是建設還是破壞？— 探討大澳不同持分者對活化該區的看法，並提出一些建議」。";
$ies_lang["en"]["string2"] = "For example, this is your topic for enquiry: 'Is the revitalisation of Tai O constructive or destructive?' Readers may not understand the focus of the study by this topic. Therefore, an appropriate elaboration or description should be added, for instance, 'Is the revitalisation of Tai O constructive or destructive? A study on how different stakeholders of Tai O view the issue and recommendations for future development'. ";

$ies_lang["b5"]["string3"] = "請就你所擬訂的題目，加上適當的說明或描述︰";
$ies_lang["en"]["string3"] = "Add an appropriate elaboration or description to the topic you have formulated.";
?>