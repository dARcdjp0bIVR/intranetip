<?php
$ies_lang["b5"]["string1"] = "同學不要誤以為在探究計劃書寫下研究困難會自暴其短。事實剛好相反，做過研究的老師或讀者都深明這一點，沒有研究是完美的。與其讓讀者找出研究的盲點，不如事先聲明，向讀者解釋研究的局限，消除他們的疑慮。此外，能清楚預期所面對的困難，也能展示你的危機意識和先見之明，反而會加分。";
$ies_lang["en"]["string1"] = "Please do not mistake that putting down research difficulties in the project proposal will expose your weaknesses. It is in fact the opposite. Teachers or readers who have conducted research before will know best that no research is perfect. If readers will eventually find out the blind spot of your research, why don't you explain the research constraints beforehand to eliminate their doubts? Moreover, you are able to demonstrate your crisis awareness and foresight if you could predict the foreseeable difficulties. This will gain you extra marks.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "例如：";
$ies_lang["en"]["string2"] = "Below are examples of foreseeable difficulties and constraints.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "「由於時間及資源所限，無法訪問全校的高中生，因此我的探究對象只限於……」";
$ies_lang["en"]["string3"] = "'Due to the limited time and resources, interviewing all senior secondary school students in my school is not possible. Therefore, my research objects only include…'";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "「因缺乏資金，未能列印足夠的問卷，改為透過電郵邀請同學填寫問卷，因此抽樣樣本……」";
$ies_lang["en"]["string4"] = "'A tight budget does not allow me to print enough copies of questionnaires. I will invite fellow students to fill in the questionnaires by email instead, hence the sample size…'";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "請列出可預期的困難及限制︰";
$ies_lang["en"]["string5"] = "State the foreseeable difficulties and constraints.";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/
?>