<?

$ies_lang["b5"]["string1"] = "探究計劃書的其中一個取分關鍵，是清楚解釋探究目的。當讀者看到你的題目後，心裡會問：「為甚麼要做這個研究？」或「你想透過這次研究證明甚麼或得到甚麼？」之類的問題，而我們需要在這部分交代清楚。";
$ies_lang["en"]["string1"] = "One crucial criterion for assessing your project proposal is whether you have explained the enquiry objectives clearly. When readers read your topic for enquiry, they will ask these questions: Why do you have to conduct such research? What do you want to prove or obtain from the enquiry study? And so on. That is why you have to give a detailed account of your objectives. ";

$ies_lang["b5"]["string2"] = "假設你的題目是︰「活化大澳，是建設還是破壞？— 探討不同利益相關者對活化大澳的看法」，那麼你的研究目的可能會是：「藉着這項研究，了解不同持分者對活化大澳的看法和理據，持分者包括政府、民間組織、大澳居民、市民大眾……」";
$ies_lang["en"]["string2"] = "Supposing your topic goes like this - 'Is the revitalisation of Tai O constructive or destructive? A study on how different stakeholders view the issue'. The objective of such an enquiry study could be 'to find out the viewpoints and grounds of the different stakeholders on the revitalisation of Tai O, including the government, social groups, Tai O residents, and the public.'";

$ies_lang["b5"]["string3"] = "以下提供一些角度讓同學參考：";
$ies_lang["en"]["string3"] = "You may write your enquiry objectives with reference to the sentence structures and perspectives below:";

$ies_lang["b5"]["string4"] = "是次研究，是為了……";
$ies_lang["en"]["string4"] = "The purpose of the study is to…";

$ies_lang["b5"]["string5"] = "調查一個現象發生的原因";
$ies_lang["en"]["string5"] = "investigate the cause(s) of a phenomenon";

$ies_lang["b5"]["string6"] = "找出一些事物的關係";
$ies_lang["en"]["string6"] = "find out the relationship of certain things";

$ies_lang["b5"]["string7"] = "探究一個問題，並提出一些解決方法";
$ies_lang["en"]["string7"] = "study a problem and suggest solutions";

$ies_lang["b5"]["string8"] = "評估某事件／政策／現象的影響";
$ies_lang["en"]["string8"] = "evaluate the influences of an incident / a policy / a phenomenon on …";


$ies_lang["b5"]["string9"] = "探討不同持分者的觀點";
$ies_lang["en"]["string9"] = "explore the viewpoints of different stakeholders";

$ies_lang["b5"]["string10"] = "瞭解某些因素之間的關係";
$ies_lang["en"]["string10"] = "find out the relationship(s) between certain factors";

$ies_lang["b5"]["string11"] = "解決某種社會問題的方法及其限制";
$ies_lang["en"]["string11"] = "bring up solutions to a social problem and anticipate their limitations";

$ies_lang["b5"]["string12"] = "找出事件或現象的成因";
$ies_lang["en"]["string12"] = "find out the causes of an incident or a phenomenon";

$ies_lang["b5"]["string13"] = "比較社會上不同人士對某事件或現象的評價或觀點";
$ies_lang["en"]["string13"] = "compare the comments or viewpoints of different parties in society on an incident or a phenomenon";

$ies_lang["b5"]["string14"] = "探究某現象的趨勢及其影響";
$ies_lang["en"]["string14"] = "explore the trend and impact of a phenomenon";

$ies_lang["b5"]["string15"] = "瞭解研究對象對某些議題的看法";
$ies_lang["en"]["string15"] = "find out the opinions of the target group on certain issues…";

$ies_lang["b5"]["string16"] = "從而能夠…… ";
$ies_lang["en"]["string16"] = "so as to…";

$ies_lang["b5"]["string17"] = "引起社會關注";
$ies_lang["en"]["string17"] = "arouse social concern";

$ies_lang["b5"]["string18"] = "作出分析，加深讀者對題目的了解";
$ies_lang["en"]["string18"] = "provide an analysis to enhance readers' understanding of the topic";

$ies_lang["b5"]["string19"] = "提出解決或改善方法";
$ies_lang["en"]["string19"] = "bring up solutions or methods of improvement";

$ies_lang["b5"]["string20"] = "請說明你的探究目的，或寫出你預期的探究結果︰";
$ies_lang["en"]["string20"] = "Illustrate your objectives of enquiry or state your expected outcome(s).";



?>