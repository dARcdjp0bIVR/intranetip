<?php
$ies_lang["b5"]["string1"] = "訂好題目後，我們要進一步解釋是次研究背後的意義，提高成功批核的機會。";
$ies_lang["en"]["string1"] = "When you have formulated the topic for enquiry, explain the significance of your enquiry study further to maximise the chance of approval. ";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "同學可以參考以下角度嘗試解釋：";
$ies_lang["en"]["string2"] = "The following aspects may help you justify the significance:";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "是次研究結果將會對你的研究對象、甚至整體社會帶來甚麼益處？";
$ies_lang["en"]["string3"] = "What benefits would the research outcome bring to your research objects, or even society as a whole? ";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "你預期研究結果對社會有何啟示？";
$ies_lang["en"]["string4"] = "What implications would the research outcome bring to society?";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "為甚麼你認為這個題目值得探究？";
$ies_lang["en"]["string5"] = "Why do you find the research question worth studying?";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "請撰寫有關是次研究的意義與作用：";
$ies_lang["en"]["string6"] = "Write up the justification/significance of your enquiry study.";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/
?>