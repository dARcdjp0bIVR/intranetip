<?php



function getQuestionJS($_question, $_answer="")
{
  $js_option_arr = "var option_arr = {\n";
  foreach($_question AS $_q_no => $_choice_arr)
  {
    foreach($_choice_arr AS $_a_value => $_a_text)
    {
      $js_option_arr .= "\"".$_a_value."\" : \"".$_a_text."\",\n";
    }
  }
  $js_option_arr = substr($js_option_arr, 0, -2)."};\n";
  
  return $js_option_arr;
}

$_question[1] = array(
                  "media" => $ies_lang[$_lang]["choice1"], 
                  "edu" => $ies_lang[$_lang]["choice2"],
                  "religion" => $ies_lang[$_lang]["choice3"],
                  "sports" => $ies_lang[$_lang]["choice4"],
                  "art" => $ies_lang[$_lang]["choice5"],
                  "IT" => $ies_lang[$_lang]["choice6"],
                  "unit1" => $ies_lang[$_lang]["choice7"],
				  "自我瞭解" => $ies_lang[$_lang]["choice8"],
				  "人際關係" => $ies_lang[$_lang]["choice9"],
				  "unit2" => $ies_lang[$_lang]["choice10"],
				  "生活素質" => $ies_lang[$_lang]["choice11"],
				  "法治和社會政治參與" => $ies_lang[$_lang]["choice12"],
  				  "身份和身份認同" => $ies_lang[$_lang]["choice13"],
				  "unit3" => $ies_lang[$_lang]["choice14"],
				  "中國的改革開放" => $ies_lang[$_lang]["choice15"],
				  "中華文化與現代生活" => $ies_lang[$_lang]["choice16"],
                  "unit4" => $ies_lang[$_lang]["choice17"],
                  "unit5" => $ies_lang[$_lang]["choice18"],
				  "對公共衛生的理解" => $ies_lang[$_lang]["choice19"],
				  "科學、科技與公共衛生" => $ies_lang[$_lang]["choice20"],
				  "unit6" => $ies_lang[$_lang]["choice21"],
				  "能源科技的影響" => $ies_lang[$_lang]["choice22"],
				  "環境與可持續發展" => $ies_lang[$_lang]["choice23"],
                  $ies_lang[$_lang]["choice24"] => $ies_lang[$_lang]["choice24"]
                );

$js_option_arr = getQuestionJS($_question);

$html_templateFile = "view.php";

?>