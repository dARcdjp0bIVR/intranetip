<script language="JavaScript">

<?=$js_option_arr?>

function CreateInputObj(input_type, input_name, input_val, input_text)
{
  var inputObj =  {
                    0 : $("<input>").val(input_val).attr("name", "question["+input_name+"][1][]").attr("type", input_type),
                    1 : $("<input>").val(input_text).attr("name", "answer_text["+input_name+"][1]["+input_val+"]").attr("type", "hidden")
                  };

  return inputObj;
}

function AddInputEntry(from_select_id, to_select_id, display_text, input_obj)
{
  $("#"+to_select_id)
    .append($("<div></div>").attr("class", "selected")
      .append($("<span></span>").html(display_text+" ").append(input_obj[0]).append(input_obj[1]))
      .append($("<a></a>").attr("class", "deselect").click(function(){
        // bind action to remove button
        var inputField = $(this).prev().find('input');
        var inputName = $(inputField).attr("name");
        var inputVal = inputField.val();
        
        $.each(option_arr, function(val, text) {
          if(inputName.indexOf("select_mod_custom") == -1 && inputVal == val)
          {
            var inputText = inputField.val();
          
            $("#"+from_select_id).append(
              $('<option></option>').val(inputVal).html(inputText)
            );
          }
        });
        
        $(this).parent().remove();
        doCountOptionSelected();
      }))
      .append($("<div></div>").attr("class", "clear"))
    );
}

// Move selected options, and bind action to remove button
function MoveSelectedOptions(from_select_id, to_select_id)
{
	$("#"+from_select_id+" option:selected").each(function(i){
    var val = $(this).val();
    var text = $(this).text();

    var inputType = (val == "<?=$ies_lang[$_lang]["choice24"]?>") ? "text" : "hidden";
    var inputName = (val == "<?=$ies_lang[$_lang]["choice24"]?>") ? "select_mod_custom" : "select_mod";
    var inputObj =  CreateInputObj(inputType, inputName, val, text);

    // Move selected options    
    AddInputEntry(from_select_id, to_select_id, text, inputObj);
    
    // remove option from selection
    if(inputType == "hidden")
    {
      $(this).remove();
    }
  });
  
  doCountOptionSelected();
  $("#"+from_select_id).width($("#"+from_select_id).parent().width());
}

// Update selected option count 
function doCountOptionSelected(){
  var count = $("#target").find(".selected").size();
  
  $('#optionCount').html(count);
}

$(document).ready(function(){
  // Initialize options and checked saved options
  $.each(option_arr, function(val, text) {
    var optionObj = $('<option></option>').val(val).html(text);
    if($.inArray(val, <?="[".$answer[1]["standard"]."]"?>) > -1) {
      optionObj.attr("selected", "selected");
    }
    $('#source').append(optionObj);
    
  });
  
  // Bind action to transfer button
  $('#transfer').click(function(){
    MoveSelectedOptions("source", "target");
  });
  
<?php for($i=0; $i<count($answer[1]["custom"]); $i++) { ?>
  inputObj = CreateInputObj("hidden", "select_mod_custom", "<?=$answer[1]["custom"][$i]?>", "<?=$answer[1]["custom"][$i]?>");
  AddInputEntry("source", "target", "<?=$answer[1]["custom"][$i]?>", inputObj);
<?php }?>
  
  // Bind action to transfer button
  $('#save').click(function(){
    doSaveStep(<?=$step_id?>,false);
  });
  
  // Bind action to remove all entry link
  $('.del_all').click(function(){
    // Remove all entries
    $('#target').find('.selected').each(function(){
      $(this).remove();
    });
    
    // Re-initialize all options
    // 1. Remove all options 
    $("#source option").each(function(){
      $(this).remove();
    });
    
    // 2. Add back all options
    $.each(option_arr, function(val, text) {
      var optionObj = $('<option></option>').val(val).html(text);
      $('#source').append(optionObj);
    });
    
    // Update entry count
    doCountOptionSelected();
  }); 
  
  // Move selected options and update count
  MoveSelectedOptions("source", "target");
  doCountOptionSelected();
});

</script>

<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  
    <?=$html_answer_display?>
    
    <!-- Start Content Here -->
    <div class="task_content">
      <?=$ies_lang[$_lang]["string1"]?><br /><br />
	<?=$ies_lang[$_lang]["string2"]?><br /><br />
      <?=$ies_lang[$_lang]["string3"]?><br /><br />
      <?=$ies_lang[$_lang]["string4"]?><br /><br />
      
    </div>
    
    <table class="form_table" style="width:98%;font-size:15px">
      <col class="field_title" />
      <col  class="field_c" />
      <col />
      
      <tr>
        <td> </td>
        <td colspan="2"> </td>
      </tr>
      <tr>
        <td colspan="2" style="width:44%">
          <div style="text-align:left;padding-right:5px;padding-bottom:5px">
												
            <!--<select id="groupSelection" onchange="member_selection_block.select_members_in_group(this)">
              <option value="allStudent">All Students</option>
            
            
            				
            
            </select>-->
          </div> 
          <select id="source" multiple class="selectAnsList">
<!-- selection box display here-->
          </select>
        </td>
        <td  style="text-align:center;padding-top:100px;width:6%">
          <input id="transfer" class="formbutton" type="button" value=">>" onmouseout="this.className='formbutton'" onmouseover="this.className='formbuttonon'" />
        </td>
        <td style="width:44%">
          <div class="task_instruction_box"><?=$ies_lang[$_lang]["string5"]?></div>
          <div id="target" class="task_selected_box">
            <h5><span style="float:left;"><span class="stu_edit"></span><?=$ies_lang[$_lang]["string6"]?> <span id="optionCount"></span> </span> <span style="float:right;"> <a href="javascript:;" class="del_all"><?=$ies_lang[$_lang]["string7"]?></a></span>
              <p class="spacer"></p>
            </h5>

          </div>
          <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
            <div style="float:right; padding-top:5px">          	
            <input id="save" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value=" <?=$Lang['IES']['Save']?> " />
            </div>
          </div>
        </td>
      </tr>
    </table>
    
    <!-- End Content Here -->
  </div>

  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$prev_step_id?>);" value=" <?=$Lang['IES']['LastStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?>  " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doSaveStep(<?=$next_step_id?>,true);" value="<?=$Lang['IES']['SaveAndNextStep']?>" />
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->
