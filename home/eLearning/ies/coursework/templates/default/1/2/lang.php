<?
$ies_lang["b5"]["string1"] = "初步訂立的題目所涵蓋的範圍可能很廣泛，就「Facebook」這話題而言，即使當中有很多方向值得探究，我們也不可能探究所有關於「Facebook」的議題，以免失去重點。";
$ies_lang["en"]["string1"] = "The scope of the initially set topic for enquiry may be very wide. Take 'Facebook' as an example. Although the topic may involve lots of issues worth studying, we cannot explore them all as we have to stay focused.";

$ies_lang["b5"]["string2"] = "因此，在這部分，我們需要嘗試結合各大單元的重點，收窄研究範圍。";
$ies_lang["en"]["string2"] = "In this section, we can try to narrow down the scope of enquiry by integrating the themes of the six modules.";

$ies_lang["b5"]["string3"] = "以「Facebook」為例，你可以從「個人成長與人際關係」的方向出發，研究「Facebook」這個社交網站是否能讓中學生維持良好的人際關係。同時，社交網站「Facebook」也體現了「資訊與通訊科技」的應用。透過結合各大單元的重點，我們很容易會想出一道跨單元的探究題目。";
$ies_lang["en"]["string3"] = "Let's look again at the example of 'Facebook'. You can begin with the aspect of 'Personal Development & Interpersonal Relationships' to find out if the social networking site Facebook can help secondary school students maintain good interpersonal relationships, and if it exemplifies the application of 'Information and Communication Technology'. By integrating the main themes of the modules, we can come up with a cross-module topic easily.";

$ies_lang["b5"]["string4"] = "當然，這只不過是題目的雛形，稍後還需要加以修正。";
$ies_lang["en"]["string4"] = "Yet, this is still a preliminary draft which needs to be fine-tuned later on. ";

$ies_lang["b5"]["string5"] = "請從左方選擇探究議題所屬的探究範圍（可選多個），假如你的探究範圍在下列的選項以外，可選擇「自訂」，並自行輸入︰";
$ies_lang["en"]["string5"] = "Please select from the left menu the scope(s) of enquiry for the issue(s) to be investigated. If your choice(s) fall(s) outside the options on the menu, you may select 'Propose' and input text.";

$ies_lang["b5"]["string6"] = "已選擇 :";
$ies_lang["en"]["string6"] = "Scope(s) chosen:";

$ies_lang["b5"]["string7"] = "全部刪除";
$ies_lang["en"]["string7"] = "Delete All";


$ies_lang["b5"]["choice1"] = "傳媒";
$ies_lang["en"]["choice1"] = "Media";

$ies_lang["b5"]["choice2"] = "教育";
$ies_lang["en"]["choice2"] = "Education";

$ies_lang["b5"]["choice3"] = "宗教";
$ies_lang["en"]["choice3"] = "Religion";

$ies_lang["b5"]["choice4"] = "體育運動";
$ies_lang["en"]["choice4"] = "Sports";

$ies_lang["b5"]["choice5"] = "藝術";
$ies_lang["en"]["choice5"] = "Art";

$ies_lang["b5"]["choice6"] = "資訊與通訊科技";
$ies_lang["en"]["choice6"] = "Information and Communication Technology";

$ies_lang["b5"]["choice7"] = "個人成長與人際關係";
$ies_lang["en"]["choice7"] = "Personal Development & Interpersonal Relationships";

$ies_lang["b5"]["choice8"] = "自我瞭解 (個人成長與人際關係)";
$ies_lang["en"]["choice8"] = "Self Understanding (Personal Development & Interpersonal Relationships)";

$ies_lang["b5"]["choice9"] = "人際關係 (個人成長與人際關係)";
$ies_lang["en"]["choice9"] = "Interpersonal Relationships (Personal Development & Interpersonal Relationships)";

$ies_lang["b5"]["choice10"] = "今日香港";
$ies_lang["en"]["choice10"] = "Hong Kong Today";

$ies_lang["b5"]["choice11"] = "生活素質 (今日香港)";
$ies_lang["en"]["choice11"] = "Quality of Life (Hong Kong Today)";

$ies_lang["b5"]["choice12"] = "法治和社會政治參與(今日香港)";
$ies_lang["en"]["choice12"] = "Rule of Law and Socio-political Participation (Hong Kong Today)";

$ies_lang["b5"]["choice13"] = "身份和身份認同 (今日香港)";
$ies_lang["en"]["choice13"] = "Identity and Identity Recognition (Hong Kong Today)";

$ies_lang["b5"]["choice14"] = "現代中國";
$ies_lang["en"]["choice14"] = "Modern China";

$ies_lang["b5"]["choice15"] = "中國的改革開放 (現代中國)";
$ies_lang["en"]["choice15"] = "China's Reform and Opening up (Modern China)";

$ies_lang["b5"]["choice16"] = "中華文化與現代生活 (現代中國)";
$ies_lang["en"]["choice16"] = "Chinese Culture and Modern Life (Modern China)";

$ies_lang["b5"]["choice17"] = "全球化";
$ies_lang["en"]["choice17"] = "Globalisation";

$ies_lang["b5"]["choice18"] = "公共衛生";
$ies_lang["en"]["choice18"] = "Public Health";

$ies_lang["b5"]["choice19"] = "對公共衛生的理解 (公共衛生)";
$ies_lang["en"]["choice19"] = "Understanding of Public Health (Public Health)";

$ies_lang["b5"]["choice20"] = "科學、科技與公共衛生 (公共衛生)";
$ies_lang["en"]["choice20"] = "Science, Technology and Public Health (Public Health)";

$ies_lang["b5"]["choice21"] = "能源科技與環境";
$ies_lang["en"]["choice21"] = "Energy Technology and the Environment";

$ies_lang["b5"]["choice22"] = "能源科技的影響 (能源科技與環境)";
$ies_lang["en"]["choice22"] = "The Influences of Energy Technology (Energy Technology and the Environment)";

$ies_lang["b5"]["choice23"] = "環境與可持續發展 (能源科技與環境)";
$ies_lang["en"]["choice23"] = "The Environment and Sustainable Development (Energy Technology and the Environment)";

$ies_lang["b5"]["choice24"] = "自訂";
$ies_lang["en"]["choice24"] = "Propose";


?>