<?

$ies_lang["b5"]["string1"] = "介紹研究背景是撰寫探究計劃書的重要部分。研究背景的作用主要有四個：";
$ies_lang["en"]["string1"] = "Introducing the background of the study is important for project proposal writing. There are 4 main purposes:";

$ies_lang["b5"]["string2"] = "讓讀者對你的題目有粗略認識";
$ies_lang["en"]["string2"] = "To give readers a rough idea about your topic for enquiry";

$ies_lang["b5"]["string3"] = "讓讀者覺得這個題目值得研究";
$ies_lang["en"]["string3"] = "To convince readers that the topic is worth studying";

$ies_lang["b5"]["string4"] = "展示你所蒐集的參考資料，並從中突出問題的重要性";
$ies_lang["en"]["string4"] = "To show all references collected and highlight the significance of the problem";

$ies_lang["b5"]["string5"] = "給一些關鍵詞下定義";
$ies_lang["en"]["string5"] = "To define keywords";

$ies_lang["b5"]["string6"] = "撰寫研究背景大致可分為兩部分。首先，描述一個客觀現象或研究議題的背景，因為讀者對議題的認識未必比你深入，所以我們必須對事情的背景作簡單描述。概括描述客觀事實後，便要解釋為甚麼這個題目值得探究，作用是突出題目的研究價值。";
$ies_lang["en"]["string6"] = "The setting of the study consists of two parts. The first is a description of the background of an objective phenomenon or a research issue. As readers may not be as familiar with the issue as you are, you have to briefly describe the background for them. Second, in order to highlight the value of the study, you have to explain why the topic is worth studying.";

$ies_lang["b5"]["string7"] = "要解釋題目何以值得研究，可以考慮以下觀點︰";
$ies_lang["en"]["string7"] = "To explain why the issue is worth studying, you may consider the following viewpoints: ";

$ies_lang["b5"]["string8"] = "突出問題的嚴重性 » 社會需要正視 » 所以題目有研究價值";
$ies_lang["en"]["string8"] = "Highlight the severity of the problem. >> Society needs to address the problem. >> So, the topic is worth studying. ";

$ies_lang["b5"]["string9"] = "問題長遠對社會會造成深遠影響 » 社會需要正視 » 所以題目有研究價值";
$ies_lang["en"]["string9"] = "The problem will bring far-reaching influences on society in the long run. >> Society needs to address the problem. >> So, the topic is worth studying.";
//
$ies_lang["b5"]["string10"] = "受問題影響的人很多 » 研究可以幫助他們解決困難 » 所以題目有研究價值";
$ies_lang["en"]["string10"] = "Many people are affected by the problem. >> The study can help them solve the problem. >> So, the topic is worth studying.";

$ies_lang["b5"]["string11"] = "突出問題的逼切性 » 問題需要盡快解決，不能再拖 » 所以題目有研究價值";
$ies_lang["en"]["string11"] = "Highlight the urgency of the problem >> An immediate solution is needed. >> So, the topic is worth studying.";

$ies_lang["b5"]["string12"] = "用一個新角度去看事物 » 有別於前人的研究 » 所以題目有研究價值";
$ies_lang["en"]["string12"] = "View the case from a new perspective. >> The outcome is different from that of previous studies. >> So, the topic is worth studying.";

$ies_lang["b5"]["string13"] = "挑戰傳統的既有想法和假設 » 有別於前人的研究／帶來一些新的分析角度 » 所以題目有研究價值";
$ies_lang["en"]["string13"] = "Challenge traditional perceptions and assumptions. >> The study should stand out from the previous ones or bring in new perspectives for analysis. >> So, the topic is worth studying.";

$ies_lang["b5"]["string14"] = "發現新的原因 » 帶來一些新的分析角度 » 所以題目有研究價值";
$ies_lang["en"]["string14"] = "Discover new grounds for attention to the issue. >> Bring in new perspectives for analysis. >> So, the topic is worth studying.";

$ies_lang["b5"]["string15"] = "請整理並寫出題目的背景資料︰";
$ies_lang["en"]["string15"] = "Edit and write down the background information for the topic.";

?>