<?php
$ies_lang["b5"]["string1"] = "擬訂研究題目和釐清研究對象後，下一步就是要選擇收集資料或數據的方法。";
$ies_lang["en"]["string1"] = "Once the topic for enquiry is set and research objects established, you have to choose the method(s) of data collection. ";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "你可以選擇一種方法，或將不同方法混合使用，以配合你的探究目的，並取得有用的資料作分析。";
$ies_lang["en"]["string2"] = "You may pick a single method or a combination of methods. Either way will do, as long as it fits your enquiry objectives and helps obtain useful data for analysis. ";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "要緊記，不同的研究方法各有其優勝或不足之處。同學應綜合考慮探究目的、個人能力、時間及資源各方面，選擇合適的研究方法。";
$ies_lang["en"]["string3"] = "Please be reminded that all research methods have their own merits and shortcomings. To choose a research method suitable for you, you are advised to take into account each of the following aspects such as the enquiry objectives, individual ability, time and resources. ";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "以下是一些研究設計／資料搜集的不同研究方法：";
$ies_lang["en"]["string4"] = "Below are some research designs/data collection methods.";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "二手資料：";
$ies_lang["en"]["string5"] = "Information from secondary sources:";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "統計數據";
$ies_lang["en"]["string6"] = "Statistical data";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "研究報告";
$ies_lang["en"]["string7"] = "Research reports";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "網上資料";
$ies_lang["en"]["string8"] = "Online information";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "錄影資料";
$ies_lang["en"]["string9"] = "Videos";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "新聞報章";
$ies_lang["en"]["string10"] = "Newspapers and Magazines";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "參考書籍";
$ies_lang["en"]["string11"] = "Reference books";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "一手資料:";
$ies_lang["en"]["string12"] = "Information from primary sources:";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "問卷調查";
$ies_lang["en"]["string13"] = "Questionnaires";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "焦點小組";
$ies_lang["en"]["string14"] = "Focus groups";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "實地考察";
$ies_lang["en"]["string15"] = "Field observation";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "深入訪問";
$ies_lang["en"]["string16"] = "In-depth interview";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "參與觀察";
$ies_lang["en"]["string17"] = "Participant observation";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/

$ies_lang["b5"]["string18"] = "分析方法：";
$ies_lang["en"]["string18"] = "Methods of analysis:";
/*
<?=$ies_lang[$_lang]["string18"]?>
*/

$ies_lang["b5"]["string19"] = "質化方法";
$ies_lang["en"]["string19"] = "Qualitative method";
/*
<?=$ies_lang[$_lang]["string19"]?>
*/

$ies_lang["b5"]["string20"] = "量化方法";
$ies_lang["en"]["string20"] = "Quantitative method";
/*
<?=$ies_lang[$_lang]["string20"]?>
*/

$ies_lang["b5"]["string21"] = "質化與量化方式結合";
$ies_lang["en"]["string21"] = "Combination of Qualitative and Quantitative methods";
/*
<?=$ies_lang[$_lang]["string21"]?>
*/

$ies_lang["b5"]["string22"] = "請列出你會採用的研究方法，並簡述如何分析資料及數據︰";
$ies_lang["en"]["string22"] = "State what research method(s) you are going to adopt and briefly explain how you are going to analyse the data and information collected.";
/*
<?=$ies_lang[$_lang]["string22"]?>
*/

?>