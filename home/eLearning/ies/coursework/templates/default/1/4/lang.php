<?

$ies_lang["b5"]["step"] = "步驟";
$ies_lang["en"]["step"] = "step";

$ies_lang["b5"]["string1"] = "假設你已選擇了感興趣的議題、研究範圍和相關概念，接下來可以把以上的元素整合，讓題目進一步成形。";
$ies_lang["en"]["string1"] = "If you have worked out an interesting issue, the enquiry scope and related concepts, you may now incorporate these elements into your topic to let it take shape. ";

$ies_lang["b5"]["string2"] = "收窄範圍是一個自問自答的過程。透過不斷自我引導、自我發問，逐步把焦點推敲出來。";
$ies_lang["en"]["string2"] = "Narrowing down the scope of enquiry is a process of answering questions raised by yourself. The focus of the topic will be inferred step by step through answering the questions.";

$ies_lang["b5"]["string3"] = "請看看以下例子，理解如何從一個廣闊的範圍收窄成一個有意義的焦點︰";
$ies_lang["en"]["string3"] = "Please refer to the 'Exemplars' below to find out how a wide scope can be narrowed down to a meaningful focus for enquiry.";

$ies_lang["b5"]["string4"] = "範例";
$ies_lang["en"]["string4"] = "Exemplars";

$ies_lang["b5"]["string5"] = "你可參考下列題型範本，結合你的題目、探究範圍和應用的相關概念，進一步收窄探究範圍，確立題目。";
$ies_lang["en"]["string5"] = "With reference to the different types of question below, you may further narrow down the scope of enquiry and formulate the topic by integrating your topic, scope of enquiry and any related concepts you are going to apply. ";

$ies_lang["b5"]["string6"] = "現在，你可以嘗試把探究範圍、相關概念和題型範本結合，組織並確立你的探究題目。";
$ies_lang["en"]["string6"] = "Now, you may try to link up the scope of enquiry and related concepts with the exemplars to organise and set your own topic of enquiry. ";

$ies_lang["b5"]["string7"] = "已選擇 :";
$ies_lang["en"]["string7"] = "Scope(s) chosen: ";

$ies_lang["b5"]["string8"] = "全部刪除";
$ies_lang["en"]["string8"] = "Delete All";


$ies_lang["b5"]["choice1"] = "分析關係";
$ies_lang["en"]["choice1"] = "Relationship analysis";

$ies_lang["b5"]["choice2"] = "X與Y有甚麼關係？";
$ies_lang["en"]["choice2"] = "What is the relationship between X and Y?";

$ies_lang["b5"]["choice3"] = "X和Y有沒有關係？";
$ies_lang["en"]["choice3"] = "Is X related to Y?";

$ies_lang["b5"]["choice4"] = "X在Y擔當甚麼角色？";
$ies_lang["en"]["choice4"] = "What is the role of X in Y?";

$ies_lang["b5"]["choice5"] = "X和Y是否可以相容？";
$ies_lang["en"]["choice5"] = "Is X compatible with Y?";

$ies_lang["b5"]["choice6"] = "X怎樣改變人們溝通和建立關係的方式？";
$ies_lang["en"]["choice6"] = "How does X change the way people communicate and build up relationships?";

$ies_lang["b5"]["choice7"] = "分析影響";
$ies_lang["en"]["choice7"] = "Impact analysis";

$ies_lang["b5"]["choice8"] = "X對於Y有甚麼影響？";
$ies_lang["en"]["choice8"] = "What is/are the influence(s) of X on Y?";

$ies_lang["b5"]["choice9"] = "X怎樣影響Y和Z？";
$ies_lang["en"]["choice9"] = "How does X influence Y and Z?";

$ies_lang["b5"]["choice10"] = "X對人們的日常生活有甚麼影響？";
$ies_lang["en"]["choice10"] = "How does X influence people’s daily life?";

$ies_lang["b5"]["choice11"] = "X在何等程度上有助促進Y的發展？";
$ies_lang["en"]["choice11"] = "To what extent does X help facilitate the development of Y?";

$ies_lang["b5"]["choice12"] = "X (改革/政策/措施) 的成效如何？面對甚麼限制？有甚麼解決方法？";
$ies_lang["en"]["choice12"] = "How effective is X (reform/policy/measure)? What are the constraints and solutions?";

$ies_lang["b5"]["choice13"] = "評估重要性";
$ies_lang["en"]["choice13"] = "Evaluation on importance";

$ies_lang["b5"]["choice14"] = "X對Y有何重要性？";
$ies_lang["en"]["choice14"] = "In what way is X important to Y?";

$ies_lang["b5"]["choice15"] = "X對Y有甚麼貢獻？";
$ies_lang["en"]["choice15"] = "In what way does X contribute to Y?";

$ies_lang["b5"]["choice16"] = "分析成因";
$ies_lang["en"]["choice16"] = "Cause analysis";

$ies_lang["b5"]["choice17"] = "X是否必然(達到一個好的效果/結果)？";
$ies_lang["en"]["choice17"] = "Are the effects/results of X necessarily positive?";

$ies_lang["b5"]["choice18"] = "X在何等程度上促進Y？";
$ies_lang["en"]["choice18"] = "To what extent does X facilitate Y?";

$ies_lang["b5"]["choice19"] = "X對Y的形成，擔當了甚麼角色？";
$ies_lang["en"]["choice19"] = "What role does X play in the formation of Y?";

$ies_lang["b5"]["choice20"] = "利益相關者X與利益相關者Y可以怎樣解決Z(一個問題)？";
$ies_lang["en"]["choice20"] = "In what ways can stakeholder X and stakeholder Y solve Z (a problem)?";

$ies_lang["b5"]["choice21"] = "甚麼因素影響X的發展？";
$ies_lang["en"]["choice21"] = "What are the factors affecting the development of X?";

$ies_lang["b5"]["choice22"] = "X是否有足夠的條件去推廣/發展Y？";
$ies_lang["en"]["choice22"] = "Are there sufficient conditions for X to promote/develop Y?";

$ies_lang["b5"]["choice23"] = "自訂";
$ies_lang["en"]["choice23"] = "Propose";

$ies_lang["b5"]["choice24"] = "自訂";
$ies_lang["en"]["choice24"] = "Propose";


$ies_lang["b5"]["string9"] = "教育";
$ies_lang["en"]["string9"] = "Education";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "教育(問︰與教育相關的議題很多，教育的哪個面向？)";
$ies_lang["en"]["string10"] = "Education (There are many educational issues. Which facet of education?)";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "教育制度(問︰教育制度的哪個範疇？)";
$ies_lang["en"]["string11"] = "Education system (Which sector of the education system?)";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "新高中的推行度(問︰教育制度的哪個範疇？)";
$ies_lang["en"]["string12"] = "Implementation of the New Senior Secondary (NSS) curriculum (Which part of the NSS curriculum?)";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "新高中通識科的推行情況(問︰探究對象是誰？)";
$ies_lang["en"]["string13"] = "Implementation of NSS Liberal Studies (Who is the target of enquiry?)";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "新高中通識科在我校的推行情況(問︰是否需要再收窄探究對象？)";
$ies_lang["en"]["string14"] = "Implementation of NSS Liberal Studies in my school (Do you need to narrow down the scope of the enquiry target?)";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "新高中通識科在我校高一級的推行情況(問︰能否界定時間？)";
$ies_lang["en"]["string15"] = "Implementation of NSS Liberal Studies in SS1 in my school (Can you set a time frame?)";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "新高中通識科第一年在我校高一級的推行情況";
$ies_lang["en"]["string16"] = "Implementation of NSS Liberal Studies in SS1 in my school in the first year";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "日本文化";
$ies_lang["en"]["string17"] = "Japanese Culture";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/

$ies_lang["b5"]["string18"] = "日本文化(問︰日本文化範圍很廣，你想研究的範疇是甚麼？)";
$ies_lang["en"]["string18"] = "Japanese culture (Under the broad issue of Japanese culture, which area would you like to examine?)";
/*
<?=$ies_lang[$_lang]["string18"]?>
*/

$ies_lang["b5"]["string19"] = "日本流行文化(問︰你想研究日本流行文化的變遷？發展？影響？)";
$ies_lang["en"]["string19"] = "Japanese popular culture (Which aspect of Japanese popular culture would you like to examine, its changes, development or impact?)";
/*
<?=$ies_lang[$_lang]["string19"]?>
*/

$ies_lang["b5"]["string20"] = "日本流行文化的影響(問︰日本文化影響對甚麼人的影響？)";
$ies_lang["en"]["string20"] = "Impact of Japanese popular culture (Who are influenced by Japanese popular culture?)";
/*
<?=$ies_lang[$_lang]["string20"]?>
*/

$ies_lang["b5"]["string21"] = "日本流行文化對青少年的影響(問︰是否需要再收窄探究對象？)";
$ies_lang["en"]["string21"] = "Impact of Japanese popular culture on teenagers (Do you need to narrow down the scope of the enquiry target?)";
/*
<?=$ies_lang[$_lang]["string21"]?>
*/

$ies_lang["b5"]["string22"] = "日本流行文化對香港青少年的影響(問︰對青少年的哪方面影響？)";
$ies_lang["en"]["string22"] = "Impact of Japanese popular culture on teenagers in Hong Kong (What kind of impact does Japanese popular culture have on our teenagers?)";
/*
<?=$ies_lang[$_lang]["string22"]?>
*/

$ies_lang["b5"]["string23"] = "日本流行文化對香港青少年消費行為的影響(問︰是否需要再修飾題目的用詞？)";
$ies_lang["en"]["string23"] = "Impact of Japanese popular culture on teenage consumer behaviour in Hong Kong (Do you need to refine the wording of the title?)";
/*
<?=$ies_lang[$_lang]["string23"]?>
*/

$ies_lang["b5"]["string24"] = "探討日本流行文化對香港青少年消費行為的影響";
$ies_lang["en"]["string24"] = "A study on the impact of Japanese popular culture on teenage consumer behaviour in Hong Kong";
/*
<?=$ies_lang[$_lang]["string24"]?>
*/

$ies_lang["b5"]["string25"] = "Facebook";
$ies_lang["en"]["string25"] = "Facebook";
/*
<?=$ies_lang[$_lang]["string25"]?>
*/

$ies_lang["b5"]["string26"] = "Facebook(問︰你想研究哪個面向？)";
$ies_lang["en"]["string26"] = "Facebook (Which aspect of Facebook would you like to examine?)";
/*
<?=$ies_lang[$_lang]["string26"]?>
*/

$ies_lang["b5"]["string27"] = "Facebook受歡迎(問︰你想研究其成因？影響？)";
$ies_lang["en"]["string27"] = "Facebook is popular. (What would you like to examine, the causes of the popularity of Facebook or its impact?)";
/*
<?=$ies_lang[$_lang]["string27"]?>
*/

$ies_lang["b5"]["string28"] = "Facebook受歡迎的原因及其影響(問︰探究對象是誰？)";
$ies_lang["en"]["string28"] = "The causes of the popularity of Facebook and its impact (Who is the target of enquiry?)";
/*
<?=$ies_lang[$_lang]["string28"]?>
*/

$ies_lang["b5"]["string29"] = "Facebook受青少年歡迎的原因及其影響(問︰題目用詞是否有清楚定義？)";
$ies_lang["en"]["string29"] = "The causes and impact of the popularity of Facebook among teenagers (Is the wording of the title clearly defined?)";
/*
<?=$ies_lang[$_lang]["string29"]?>
*/

$ies_lang["b5"]["string30"] = "社交網站受青少年歡迎的原因及其影響((問︰是否需要再修飾題目的用詞？)";
$ies_lang["en"]["string30"] = "The causes and impact of the popularity of social networking sites among teenagers (Do you need to refine the wording of the title?)";
/*
<?=$ies_lang[$_lang]["string30"]?>
*/

$ies_lang["b5"]["string31"] = "探討社交網站受香港青少年歡迎的原因及其影響";
$ies_lang["en"]["string31"] = "A study on the causes and impact of the popularity of social networking sites among teenagers in Hong Kong";
/*
<?=$ies_lang[$_lang]["string31"]?>
*/

$ies_lang["b5"]["string32"] = "世博";
$ies_lang["en"]["string32"] = "Expo 2010 Shanghai China";
/*
<?=$ies_lang[$_lang]["string32"]?>
*/

$ies_lang["b5"]["string33"] = "世博(問︰你想研究哪個面向？)";
$ies_lang["en"]["string33"] = "Expo 2010 Shanghai China (Which aspect would you like to examine?)";
/*
<?=$ies_lang[$_lang]["string33"]?>
*/

$ies_lang["b5"]["string34"] = "世博與國民身份認同(問︰兩者關係如何？)";
$ies_lang["en"]["string34"] = "Expo 2010 Shanghai China and national identity (How are the two things related?)";
/*
<?=$ies_lang[$_lang]["string34"]?>
*/

$ies_lang["b5"]["string35"] = "世博提升國民身份認同(問︰探究對象是誰？)";
$ies_lang["en"]["string35"] = "Expo 2010 Shanghai China enhances national identity (Who is the target of enquiry?)";
/*
<?=$ies_lang[$_lang]["string35"]?>
*/

$ies_lang["b5"]["string36"] = "世博提升香港人國民身份認同(問︰探究題目應是一條問題。)";
$ies_lang["en"]["string36"] = "Expo 2010 Shanghai China enhances the national identity of Hong Kong people (The title for enquiry should be a question.)";
/*
<?=$ies_lang[$_lang]["string36"]?>
*/

$ies_lang["b5"]["string37"] = "世博是否能提升香港人國民身份認同(問︰題目用詞是否有清楚定義？是否容許多角度探究？)";
$ies_lang["en"]["string37"] = "Can Expo 2010 Shanghai China enhance the national identity of Hong Kong people? (Is the wording of the title clearly defined? Is a multi-dimensional study possible?)";
/*
<?=$ies_lang[$_lang]["string37"]?>
*/

$ies_lang["b5"]["string38"] = "香港參與世博，在多大程度上能提升香港市民的國民身份認同？ ";
$ies_lang["en"]["string38"] = "To what extent will Hong Kong's participation in Expo 2010 Shanghai China enhance the national identity of Hong Kong people?";
/*
<?=$ies_lang[$_lang]["string38"]?>
*/

?>