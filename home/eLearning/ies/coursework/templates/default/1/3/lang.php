<?


$ies_lang["b5"]["string1"] = "在通識課程中，各單元也有不同的相關概念。你可以在探究過程中應用這些相關概念，進一步收窄探究範圍，並增加探究報告的理論基礎和思考深度。";
$ies_lang["en"]["string1"] = "There are related concepts in different modules under the Liberal Studies curriculum. You may apply any related concepts in the enquiry process to narrow down the scope of enquiry, to provide a sound theoretical basis for the report and to enhance profundity in thinking. ";

$ies_lang["b5"]["string2"] = "在此步驟中，你可以選擇與探究議題相關的概念，幫助收窄探究範圍。";
$ies_lang["en"]["string2"] = "You may choose any concepts relevant to the enquiry issues to narrow down the scope of enquiry.";

$ies_lang["b5"]["string3"] = "相關概念的字眼不一定出現在題目中，但很大機會在組織焦點問題、建立分析架構或闡述探究結果等部分時會用到。";
$ies_lang["en"]["string3"] = "Keywords related to the concepts may not appear in the topic, but are very likely used in organizing focus questions, constructing frameworks for analysis, or elaborating the enquiry outcome. ";

$ies_lang["b5"]["string4"] = "例子︰";
$ies_lang["en"]["string4"] = "Example:";

$ies_lang["b5"]["string5"] = "假設你對「網上討論區的文化」這個題目感興趣，但對如何收窄探究題目卻茫無頭緒。參考通識概念時，你發現「溝通」、「流行文化」、「文化價值」等相關概念可以是其中一些思考角度，你便可以點選這些概念並儲存。稍後可以參考這些相關概念，收窄探究範圍及撰寫焦點問題。";
$ies_lang["en"]["string5"] = "You are interested in the issue of 'The Culture of Online Forum' but have no idea how to narrow down the scope of enquiry. With reference to the concepts learnt in Liberal Studies, you discover that 'communication', 'popular culture' and 'cultural value' could be some of the perspectives for thought. You may click on these concepts and save the options for future reference. This helps you narrow down the scope of enquiry and write up focus questions. ";

$ies_lang["b5"]["string6"] = "請從左方選擇你認為合適的相關概念。可選多個，也可以選取不同單元的相關概念。假如當中沒有合適的相關概念，可選「自訂」，並自行輸入。";
$ies_lang["en"]["string6"] = "Please select from the left menu the concept(s) you find appropriate. You may also select related concepts under different modules. If there are no related concepts suitable for you, you may select 'Propose' and enter your own.";

$ies_lang["b5"]["string7"] = "已選擇 :";
$ies_lang["en"]["string7"] = "Scope(s) chosen: ";

$ies_lang["b5"]["string8"] = "全部刪除";
$ies_lang["en"]["string8"] = "Delete All";


$ies_lang["b5"]["choice1"] = "自我瞭解 (個人成長與人際關係)";
$ies_lang["en"]["choice1"] = "Self-understanding (Personal Development and Interpersonal Relationship)";

$ies_lang["b5"]["choice2"] = "青少年期";
$ies_lang["en"]["choice2"] = "Adolescence";

$ies_lang["b5"]["choice3"] = "自我";
$ies_lang["en"]["choice3"] = "Self";

$ies_lang["b5"]["choice4"] = "身份";
$ies_lang["en"]["choice4"] = "Identity";

$ies_lang["b5"]["choice5"] = "身體";
$ies_lang["en"]["choice5"] = "Body";

$ies_lang["b5"]["choice6"] = "情緒";
$ies_lang["en"]["choice6"] = "Emotions";

$ies_lang["b5"]["choice7"] = "需要";
$ies_lang["en"]["choice7"] = "Needs";

$ies_lang["b5"]["choice8"] = "希望與失落";
$ies_lang["en"]["choice8"] = "Hope and Despair";

$ies_lang["b5"]["choice9"] = "價值";
$ies_lang["en"]["choice9"] = "Values";

$ies_lang["b5"]["choice10"] = "文化價值";
$ies_lang["en"]["choice10"] = "Cultural Values";

$ies_lang["b5"]["choice11"] = "成長與發展";
$ies_lang["en"]["choice11"] = "Growth and Development";

$ies_lang["b5"]["choice12"] = "生活風格";
$ies_lang["en"]["choice12"] = "Lifestyle";

$ies_lang["b5"]["choice13"] = "生活技能";
$ies_lang["en"]["choice13"] = "Life Skills";

$ies_lang["b5"]["choice14"] = "權利與義務";
$ies_lang["en"]["choice14"] = "Rights and Responsibilities";

$ies_lang["b5"]["choice15"] = "人際關係 (個人成長與人際關係)";
$ies_lang["en"]["choice15"] = "Interpersonal Relationships (Personal Development & Interpersonal Relationships)";

$ies_lang["b5"]["choice16"] = "溝通";
$ies_lang["en"]["choice16"] = "Communication";

$ies_lang["b5"]["choice17"] = "家庭";
$ies_lang["en"]["choice17"] = "Family";

$ies_lang["b5"]["choice18"] = "性取向";
$ies_lang["en"]["choice18"] = "Sexual Orientation";

$ies_lang["b5"]["choice19"] = "性與性別";
$ies_lang["en"]["choice19"] = "Sex and Gender";

$ies_lang["b5"]["choice20"] = "次文化";
$ies_lang["en"]["choice20"] = "Subculture";

$ies_lang["b5"]["choice21"] = "流行文化";
$ies_lang["en"]["choice21"] = "Popular Culture";

$ies_lang["b5"]["choice22"] = "越軌行為";
$ies_lang["en"]["choice22"] = "Delinquency";

$ies_lang["b5"]["choice23"] = "衝突與和諧";
$ies_lang["en"]["choice23"] = "Conflict and Harmony";

$ies_lang["b5"]["choice24"] = "生活素質 (今日香港)";
$ies_lang["en"]["choice24"] = "Quality of Life (Hong Kong Today)";

$ies_lang["b5"]["choice25"] = "生活素質";
$ies_lang["en"]["choice25"] = "Quality of Life";

$ies_lang["b5"]["choice26"] = "人權";
$ies_lang["en"]["choice26"] = "Human Rights";

$ies_lang["b5"]["choice27"] = "文化資本";
$ies_lang["en"]["choice27"] = "Cultural Capital";

$ies_lang["b5"]["choice28"] = "民俗文化";
$ies_lang["en"]["choice28"] = "Ethnic Culture";

$ies_lang["b5"]["choice29"] = "多元文化社會";
$ies_lang["en"]["choice29"] = "Multicultural Society";

$ies_lang["b5"]["choice30"] = "消費主義";
$ies_lang["en"]["choice30"] = "Consumerism";

$ies_lang["b5"]["choice31"] = "法治和社會政治參與 (今日香港)";
$ies_lang["en"]["choice31"] = "Rule of Law and Socio-political Participation (Hong Kong Today)";

$ies_lang["b5"]["choice32"] = "法治";
$ies_lang["en"]["choice32"] = "Rule of Law";

$ies_lang["b5"]["choice33"] = "法律與民主";
$ies_lang["en"]["choice33"] = "Law and Democracy";

$ies_lang["b5"]["choice34"] = "民族國家與主權";
$ies_lang["en"]["choice34"] = "Nation State and Sovereignty";

$ies_lang["b5"]["choice35"] = "憲法";
$ies_lang["en"]["choice35"] = "Constitution";

$ies_lang["b5"]["choice36"] = "憲制性文件";
$ies_lang["en"]["choice36"] = "Constitutional Documents";

$ies_lang["b5"]["choice37"] = "基本法";
$ies_lang["en"]["choice37"] = "Basic Law";

$ies_lang["b5"]["choice38"] = "一國兩制";
$ies_lang["en"]["choice38"] = "One Country, Two Systems";

$ies_lang["b5"]["choice39"] = "高度自治";
$ies_lang["en"]["choice39"] = "High Degree of Autonomy";

$ies_lang["b5"]["choice40"] = "積極不干預";
$ies_lang["en"]["choice40"] = "Positive Non-intervention";

$ies_lang["b5"]["choice41"] = "政治參與";
$ies_lang["en"]["choice41"] = "Political Participation";

$ies_lang["b5"]["choice42"] = "遵守法律與公民抗命";
$ies_lang["en"]["choice42"] = "Compliance with the Law and Civil Disobedience";

$ies_lang["b5"]["choice43"] = "社會公義";
$ies_lang["en"]["choice43"] = "Social Justice";
//
$ies_lang["b5"]["choice44"] = "社會運動";
$ies_lang["en"]["choice44"] = "Social Movement";

$ies_lang["b5"]["choice45"] = "保育";
$ies_lang["en"]["choice45"] = "Conservation";

$ies_lang["b5"]["choice46"] = "身份和身份認同 (今日香港)";
$ies_lang["en"]["choice46"] = "Identity and Identity Recognition (Hong Kong Today)";

$ies_lang["b5"]["choice47"] = "公民社會";
$ies_lang["en"]["choice47"] = "Civil Society";

$ies_lang["b5"]["choice48"] = "公民身份";
$ies_lang["en"]["choice48"] = "Citizenship";

$ies_lang["b5"]["choice49"] = "公民意識";
$ies_lang["en"]["choice49"] = "Civic Awareness";

$ies_lang["b5"]["choice50"] = "社會身份認同";
$ies_lang["en"]["choice50"] = "Social Identity";

$ies_lang["b5"]["choice51"] = "公民參與";
$ies_lang["en"]["choice51"] = "Civic Participation";

$ies_lang["b5"]["choice52"] = "集體回憶";
$ies_lang["en"]["choice52"] = "Collective Memories";

$ies_lang["b5"]["choice53"] = "中國的改革開放 (現代中國)";
$ies_lang["en"]["choice53"] = "China’s Reform and Opening up (Modern China)";

$ies_lang["b5"]["choice54"] = "改革開放";
$ies_lang["en"]["choice54"] = "Reform and Opening up";

$ies_lang["b5"]["choice55"] = "綜合國力";
$ies_lang["en"]["choice55"] = "Overall National Strength";

$ies_lang["b5"]["choice56"] = "現代化與發展";
$ies_lang["en"]["choice56"] = "Modernisation and Development";

$ies_lang["b5"]["choice57"] = "人權與國家發展";
$ies_lang["en"]["choice57"] = "Human Rights and National Development";

$ies_lang["b5"]["choice58"] = "治理和民主";
$ies_lang["en"]["choice58"] = "Governance and Democracy";

$ies_lang["b5"]["choice59"] = "市場經濟與計劃經濟";
$ies_lang["en"]["choice59"] = "Market Economy and Planned Economy";

$ies_lang["b5"]["choice60"] = "城市化";
$ies_lang["en"]["choice60"] = "Urbanisation";

$ies_lang["b5"]["choice61"] = "三農問題";
$ies_lang["en"]["choice61"] = "Issue of Sannong, Three Rural issues";

$ies_lang["b5"]["choice62"] = "人口流動";
$ies_lang["en"]["choice62"] = "Population Mobility";

$ies_lang["b5"]["choice63"] = "社會矛盾";
$ies_lang["en"]["choice63"] = "Social Conflict";

$ies_lang["b5"]["choice64"] = "文化保育與創新";
$ies_lang["en"]["choice64"] = "Cultural Conservation and Innovation";

$ies_lang["b5"]["choice65"] = "中華文化與現代生活 (現代中國)";
$ies_lang["en"]["choice65"] = "Chinese Culture and Modern Life (Modern China)";

$ies_lang["b5"]["choice66"] = "傳統與現代";
$ies_lang["en"]["choice66"] = "Tradition and Modernity";

$ies_lang["b5"]["choice67"] = "儒家思想";
$ies_lang["en"]["choice67"] = "Confucianism";

$ies_lang["b5"]["choice68"] = "家庭觀念";
$ies_lang["en"]["choice68"] = "Concepts of the Family";

$ies_lang["b5"]["choice69"] = "家庭與親族";
$ies_lang["en"]["choice69"] = "Family and Kinship";

$ies_lang["b5"]["choice70"] = "習俗";
$ies_lang["en"]["choice70"] = "Customs";

$ies_lang["b5"]["choice71"] = "全球化";
$ies_lang["en"]["choice71"] = "Globalisation";

$ies_lang["b5"]["choice72"] = "全球化的視點";
$ies_lang["en"]["choice72"] = "Globalisation Perspectives";

$ies_lang["b5"]["choice73"] = "全球化與本土化";
$ies_lang["en"]["choice73"] = "Globalisation and Localisation";

$ies_lang["b5"]["choice74"] = "全球文化與本土文化";
$ies_lang["en"]["choice74"] = "Global and Local Cultures";

$ies_lang["b5"]["choice75"] = "全球化與歷史";
$ies_lang["en"]["choice75"] = "Globalisation and History";

$ies_lang["b5"]["choice76"] = "全球公民";
$ies_lang["en"]["choice76"] = "Global Citizenship";

$ies_lang["b5"]["choice77"] = "全球公民社會";
$ies_lang["en"]["choice77"] = "Global Civil Society";

$ies_lang["b5"]["choice78"] = "全球管治";
$ies_lang["en"]["choice78"] = "Global Governance";

$ies_lang["b5"]["choice79"] = "全球與地區參與";
$ies_lang["en"]["choice79"] = "Global and Regional Participation";

$ies_lang["b5"]["choice80"] = "全球生產鏈";
$ies_lang["en"]["choice80"] = "Global Supply Chain";

$ies_lang["b5"]["choice81"] = "文化同質化";
$ies_lang["en"]["choice81"] = "Cultural Homogenisation";

$ies_lang["b5"]["choice82"] = "文化多元";
$ies_lang["en"]["choice82"] = "Cultural Pluralism";

$ies_lang["b5"]["choice83"] = "文化帝國主義";
$ies_lang["en"]["choice83"] = "Cultural Imperialism";

$ies_lang["b5"]["choice84"] = "文明衝突論";
$ies_lang["en"]["choice84"] = "The Clash of Civilisations";

$ies_lang["b5"]["choice85"] = "經濟一體化";
$ies_lang["en"]["choice85"] = "Economic Integration";

$ies_lang["b5"]["choice86"] = "競爭力";
$ies_lang["en"]["choice86"] = "Competitiveness";

$ies_lang["b5"]["choice87"] = "跨國企業";
$ies_lang["en"]["choice87"] = "Multinational Corporations";

$ies_lang["b5"]["choice88"] = "多邊組織";
$ies_lang["en"]["choice88"] = "Multilateral Organisations";

$ies_lang["b5"]["choice89"] = "財富的分配與再分配";
$ies_lang["en"]["choice89"] = "Distribution and Redistribution of Wealth";

$ies_lang["b5"]["choice90"] = "全球兩極化與不平等";
$ies_lang["en"]["choice90"] = "Global Polarisation and Inequality";

$ies_lang["b5"]["choice91"] = "反全球化運動";
$ies_lang["en"]["choice91"] = "Anti-globalisation Movement";

$ies_lang["b5"]["choice92"] = "國際分工";
$ies_lang["en"]["choice92"] = "International Division of Labour";

$ies_lang["b5"]["choice93"] = "剝削與壓制";
$ies_lang["en"]["choice93"] = "Exploitation and Domination";

$ies_lang["b5"]["choice94"] = "無力的國家";
$ies_lang["en"]["choice94"] = "The Powerless States";

$ies_lang["b5"]["choice95"] = "對公共衛生的理解 (公共衛生)";
$ies_lang["en"]["choice95"] = "Understanding of Public Health (Public Health)";

$ies_lang["b5"]["choice96"] = "公共衛生";
$ies_lang["en"]["choice96"] = "Public Health";

$ies_lang["b5"]["choice97"] = "公共衛生服務";
$ies_lang["en"]["choice97"] = "Public Health Services";

$ies_lang["b5"]["choice98"] = "傳染病";
$ies_lang["en"]["choice98"] = "Infectious Diseases";

$ies_lang["b5"]["choice99"] = "非傳染病";
$ies_lang["en"]["choice99"] = "Non-infectious Diseases";

$ies_lang["b5"]["choice100"] = "流行病";
$ies_lang["en"]["choice100"] = "Epidemics";

$ies_lang["b5"]["choice101"] = "精神病";
$ies_lang["en"]["choice101"] = "Mental Diseases	";

$ies_lang["b5"]["choice102"] = "健康";
$ies_lang["en"]["choice102"] = "Health";

$ies_lang["b5"]["choice103"] = "健康生活與習慣";
$ies_lang["en"]["choice103"] = "Healthy Lifestyle and Habits";

$ies_lang["b5"]["choice104"] = "健康教育";
$ies_lang["en"]["choice104"] = "Health Education";

$ies_lang["b5"]["choice105"] = "科學、科技與公共衛生 (公共衛生)";
$ies_lang["en"]["choice105"] = "Science, Technology and Public Health (Public Health)";

$ies_lang["b5"]["choice106"] = "疾病診斷";
$ies_lang["en"]["choice106"] = "Disease diagnosis";

$ies_lang["b5"]["choice107"] = "疾病預防";
$ies_lang["en"]["choice107"] = "Prevention of Diseases";

$ies_lang["b5"]["choice108"] = "疾病治療";
$ies_lang["en"]["choice108"] = "Treatment of diseases";

$ies_lang["b5"]["choice109"] = "醫學科技與倫理";
$ies_lang["en"]["choice109"] = "Medical Technology and Ethics";

$ies_lang["b5"]["choice110"] = "公共衛生與國際合作";
$ies_lang["en"]["choice110"] = "International Collaboration in Public Health";

$ies_lang["b5"]["choice111"] = "藥物專利權";
$ies_lang["en"]["choice111"] = "Patenting of Drugs";

$ies_lang["b5"]["choice112"] = "能源科技的影響 (能源科技與環境)";
$ies_lang["en"]["choice112"] = "The Influences of Energy Technology (Energy Technology and the Environment)";

$ies_lang["b5"]["choice113"] = "能源";
$ies_lang["en"]["choice113"] = "Energy Sources";

$ies_lang["b5"]["choice114"] = "能源使用";
$ies_lang["en"]["choice114"] = "Energy Usage";

$ies_lang["b5"]["choice115"] = "能源科技";
$ies_lang["en"]["choice115"] = "Energy Technology";

$ies_lang["b5"]["choice116"] = "天然資源的消耗";
$ies_lang["en"]["choice116"] = "Natural Resources Consumption";

$ies_lang["b5"]["choice117"] = "能源引致的環境影響";
$ies_lang["en"]["choice117"] = "Energy-induced Environmental Impact";

$ies_lang["b5"]["choice118"] = "環境與可持續發展  (能源科技與環境)";
$ies_lang["en"]["choice118"] = "The Environment and Sustainable Development (Energy Technology and the Environment)";

$ies_lang["b5"]["choice119"] = "生態系統";
$ies_lang["en"]["choice119"] = "Ecosystem";

$ies_lang["b5"]["choice120"] = "可持續性";
$ies_lang["en"]["choice120"] = "Sustainability";

$ies_lang["b5"]["choice121"] = "可持續城市化";
$ies_lang["en"]["choice121"] = "Sustainable Urbanisation";

$ies_lang["b5"]["choice122"] = "天然資源管理";
$ies_lang["en"]["choice122"] = "Natural Resources Management";

$ies_lang["b5"]["choice123"] = "廢物管理";
$ies_lang["en"]["choice123"] = "Waste Management";

$ies_lang["b5"]["choice124"] = "科學方法";
$ies_lang["en"]["choice124"] = "Scientific Methods";

$ies_lang["b5"]["choice125"] = "科學研究與解決方案";
$ies_lang["en"]["choice125"] = "Scientific Research and Solutions";

$ies_lang["b5"]["choice126"] = "能源科技與環境國際合作";
$ies_lang["en"]["choice126"] = "International Collaboration and Interrelationships (in Energy Technology and the Environment)";

$ies_lang["b5"]["choice127"] = "環境教育";
$ies_lang["en"]["choice127"] = "Environmental Education";

$ies_lang["b5"]["choice128"] = "環境健康與安全";
$ies_lang["en"]["choice128"] = "Environmental Health and Safety";

$ies_lang["b5"]["choice129"] = "自訂";
$ies_lang["en"]["choice129"] = "Propose";

$ies_lang["b5"]["choice130"] = "自訂";
$ies_lang["en"]["choice130"] = "Propose";


?>