<?php



//SKIP the step to display in the display area array("stepSEQ","stepSEQ");
//$stepCfg_skip_displayStepSeq = array("1","2");

function getQuestionJS($_question)
{
  $js_option_arr = "var optgroup_arr = {\n";
  foreach($_question AS $_q_no => $_optgroup_arr)
  {
    foreach($_optgroup_arr AS $_optgroup_title => $_optgroup_opt_arr)
    {
      $js_option_arr .= "\"".$_optgroup_title."\" : {";
      foreach($_optgroup_opt_arr AS $opt_key => $opt_val)
      {
        $js_option_arr .= "\"".$opt_key."\" : \"".$opt_val."\",\n";
      }
      $js_option_arr = substr($js_option_arr, 0, -2)."},\n";
    }
  }
  $js_option_arr = substr($js_option_arr, 0, -2)."}\n";
  
  return $js_option_arr;
}

$_question[1] = array(
	$ies_lang[$_lang]["choice1"] =>
	array(
		"Adolescence" => $ies_lang[$_lang]["choice2"],
		"Self" => $ies_lang[$_lang]["choice3"],
		"Identity" => $ies_lang[$_lang]["choice4"],
		"Body" => $ies_lang[$_lang]["choice5"],
		"Emotion" => $ies_lang[$_lang]["choice6"],
		"Needs" => $ies_lang[$_lang]["choice7"],
		"Hope & despair" => $ies_lang[$_lang]["choice8"],
		"Values" => $ies_lang[$_lang]["choice9"],
		"Cultural value" => $ies_lang[$_lang]["choice10"],
		"Growth & development" => $ies_lang[$_lang]["choice11"],
		"Lifestyle" => $ies_lang[$_lang]["choice12"],
		"Life skills" => $ies_lang[$_lang]["choice13"],
		"Rights and obligations" => $ies_lang[$_lang]["choice14"]
	),
	$ies_lang[$_lang]["choice15"] =>
	array(
		"Communication" => $ies_lang[$_lang]["choice16"],
		"Family" => $ies_lang[$_lang]["choice17"],
		"Sexual orientation" => $ies_lang[$_lang]["choice18"],
		"Sexes & genders" => $ies_lang[$_lang]["choice19"],
		"Subculture" => $ies_lang[$_lang]["choice20"],
		"Popular culture" => $ies_lang[$_lang]["choice21"],
		"Delinquency" => $ies_lang[$_lang]["choice22"],
		"Conflict and harmony" => $ies_lang[$_lang]["choice23"]			
	),
	$ies_lang[$_lang]["choice24"] =>
	array(
		"Quality of life" => $ies_lang[$_lang]["choice25"] ,
		"Human rights" => $ies_lang[$_lang]["choice26"] ,
		"Cultural capital" => $ies_lang[$_lang]["choice27"] ,
		"Ethnic culture (& HK tradition)" => $ies_lang[$_lang]["choice28"] ,
		"Multi-cultural society" => $ies_lang[$_lang]["choice29"],
		"Consumerism" => $ies_lang[$_lang]["choice30"] 
	),
	$ies_lang[$_lang]["choice31"]  =>
	array(
		"Rule of law" => $ies_lang[$_lang]["choice32"],
		"Law and democracy" => $ies_lang[$_lang]["choice33"],
		"Nation-state and sovereignty" => $ies_lang[$_lang]["choice34"],
		"Constitution" => $ies_lang[$_lang]["choice35"],
		"Constitutional document" => $ies_lang[$_lang]["choice36"],
		"Basic Law" => $ies_lang[$_lang]["choice37"],
		"One country, two systems*" => $ies_lang[$_lang]["choice38"],
		"High degree of autonomy" => $ies_lang[$_lang]["choice39"],
		"Active non-intervention/laissez faire" => $ies_lang[$_lang]["choice40"],
		"Political participation" => $ies_lang[$_lang]["choice41"],
		"Obedience to the law and civil disobedience" => $ies_lang[$_lang]["choice42"],
		"Social justice" => $ies_lang[$_lang]["choice43"],
		"Social movement" => $ies_lang[$_lang]["choice44"],
		"Conservation" => $ies_lang[$_lang]["choice45"]
	),
	$ies_lang[$_lang]["choice46"] =>
	array(
		"Civil society" => $ies_lang[$_lang]["choice47"],
		"Citizenship" => $ies_lang[$_lang]["choice48"],
		"Civic awareness" => $ies_lang[$_lang]["choice49"],
		"Social identity" => $ies_lang[$_lang]["choice50"],
		"Civic participation" => $ies_lang[$_lang]["choice51"],
		"Collective memories" => $ies_lang[$_lang]["choice52"],
	),
	$ies_lang[$_lang]["choice53"] =>
	array(
		"Reform and opening-up" => $ies_lang[$_lang]["choice54"],
		"Overall national strengths" => $ies_lang[$_lang]["choice55"],
		"Modernisation and development" => $ies_lang[$_lang]["choice56"],
		"Human rights and national development*" => $ies_lang[$_lang]["choice57"],
		"Governance and democracy" => $ies_lang[$_lang]["choice58"],
		"Market economy and planned economy" => $ies_lang[$_lang]["choice59"],
		"Urbanisation" => $ies_lang[$_lang]["choice60"],
		"The issue of sannong (Three rural issues)" => $ies_lang[$_lang]["choice61"],
		"Population mobility" => $ies_lang[$_lang]["choice62"],
		"Social contradictions" => $ies_lang[$_lang]["choice63"],
		"Cultural conservation and innovation" => $ies_lang[$_lang]["choice64"]
	),
	$ies_lang[$_lang]["choice65"] =>
	array(
		"Tradition and modernity" => $ies_lang[$_lang]["choice66"],
		"Confucianism" => $ies_lang[$_lang]["choice67"],
		"Concepts of the family" => $ies_lang[$_lang]["choice68"],
		"Family and kinship" => $ies_lang[$_lang]["choice69"],
		"Customs" => $ies_lang[$_lang]["choice70"]
	),
	$ies_lang[$_lang]["choice71"] =>
	array(
		"Globalization perspectives" => $ies_lang[$_lang]["choice72"],
		"Globalization and localisation" => $ies_lang[$_lang]["choice73"],
		"Global and local cultures" => $ies_lang[$_lang]["choice74"],
		"Globalization and history" => $ies_lang[$_lang]["choice75"],
		"Global citizenship*" => $ies_lang[$_lang]["choice76"],
		"Global civil society" => $ies_lang[$_lang]["choice77"],
		"Global governance" => $ies_lang[$_lang]["choice78"],
		"Global and regional participation" => $ies_lang[$_lang]["choice79"],
		"Global production chain" => $ies_lang[$_lang]["choice80"],
		"Cultural homogenisation" => $ies_lang[$_lang]["choice81"],
		"Cultural pluralism" => $ies_lang[$_lang]["choice82"],
		"Cultural imperialism" => $ies_lang[$_lang]["choice83"],
		"The clash of civilisations" => $ies_lang[$_lang]["choice84"],
		"Economic integration" => $ies_lang[$_lang]["choice85"],
		"Competitiveness" => $ies_lang[$_lang]["choice86"],
		"Multinational corporations" => $ies_lang[$_lang]["choice87"],
		"Multilateral organisations" => $ies_lang[$_lang]["choice88"],
		"Distribution and redistribution of wealth" => $ies_lang[$_lang]["choice89"],
		"Global polarisation and inequality" => $ies_lang[$_lang]["choice90"],
		"Anti-globalization movement" => $ies_lang[$_lang]["choice91"],
		"International division of labour" => $ies_lang[$_lang]["choice92"],
		"Exploitation and domination" => $ies_lang[$_lang]["choice93"],
		"The powerless state" => $ies_lang[$_lang]["choice94"]
	),
	$ies_lang[$_lang]["choice95"] =>
	array(
		"Public health" => $ies_lang[$_lang]["choice96"],
		"Public health services" => $ies_lang[$_lang]["choice97"],
		"Infectious diseases" => $ies_lang[$_lang]["choice98"],
		"Non-infectious diseases" => $ies_lang[$_lang]["choice99"],
		"Epidemics" => $ies_lang[$_lang]["choice100"],
		"Mental diseases" => $ies_lang[$_lang]["choice101"],
		"Health" => $ies_lang[$_lang]["choice102"],
		"Healthy lifestyle and habits" => $ies_lang[$_lang]["choice103"],
		"Health education" => $ies_lang[$_lang]["choice104"]
	),
	$ies_lang[$_lang]["choice105"] =>
	array(
		"Disease diagnosis" => $ies_lang[$_lang]["choice106"],
		"Prevention of diseases" => $ies_lang[$_lang]["choice107"],
		"Treatment of diseases" => $ies_lang[$_lang]["choice108"],
		"Medical technology and ethics" => $ies_lang[$_lang]["choice109"],
		"International collaboration (in public health)" => $ies_lang[$_lang]["choice110"],
		"Patenting of drugs" => $ies_lang[$_lang]["choice111"]
	),
	$ies_lang[$_lang]["choice112"] =>
	array(
		"Energy sources" => $ies_lang[$_lang]["choice113"],
		"Energy usage" => $ies_lang[$_lang]["choice114"],
		"Energy technology" => $ies_lang[$_lang]["choice115"],
		"Natural resources consumption" => $ies_lang[$_lang]["choice116"],
		"Energy-induced environmental impacts" => $ies_lang[$_lang]["choice117"]
	),
	$ies_lang[$_lang]["choice118"] =>
	array(
		"Ecosystem" => $ies_lang[$_lang]["choice119"],
		"Sustainability" => $ies_lang[$_lang]["choice120"],
		"Sustainable urbanisation" => $ies_lang[$_lang]["choice121"],
		"Natural resources management" => $ies_lang[$_lang]["choice122"],
		"Waste management" => $ies_lang[$_lang]["choice123"],
		"Scientific methods" => $ies_lang[$_lang]["choice124"],
		"Scientific research and solution" => $ies_lang[$_lang]["choice125"],
		"International collaboration and interrelationships (in energy technology and the environment)" => $ies_lang[$_lang]["choice126"],
		"Environmental education" => $ies_lang[$_lang]["choice127"],
		"Environmental health and safety" => $ies_lang[$_lang]["choice128"]
	),
	$ies_lang[$_lang]["choice129"] => array($ies_lang[$_lang]["choice130"] => $ies_lang[$_lang]["choice130"])
);

$js_option_arr = getQuestionJS($_question);

$html_templateFile = "view.php";

?>