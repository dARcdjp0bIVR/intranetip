<?php
$ies_lang["b5"]["string1"] = "焦點問題的作用是進一步把研究目的細分，讓你在研究目的這個大方向中，找到更具體的焦點。";
$ies_lang["en"]["string1"] = "Focus questions help to divide the pool of enquiry objectives into smaller parts. This will enable you to identify specific focuses more easily.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "因此，焦點問題是探究的核心。整個探究報告也應圍繞這些焦點問題探究，所得的資料也理應能充分回答焦點問題，而到了第三階段寫研究問題時，也要逐一回應所有焦點問題。";
$ies_lang["en"]["string2"] = "Therefore, focus questions form the core of the enquiry study. The whole enquiry report should cover all these questions. In return, any information collected should be fully used to answer the focus questions. Even at Stage 3, you have to answer the focus questions one by one. ";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "假設你的題目是︰「活化大澳，是建設還是破壞？— 探討不同利益相關者對活化大澳的看法」，焦點問題可能是︰";
$ies_lang["en"]["string3"] = "For the topic 'Is the revitalisation of Tai O constructive or destructive? A study on how different stakeholders view the issue', the focus questions could be the following:";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "焦點問題1︰政府對「活化大澳」的定義、主張及政策實例是甚麼？";
$ies_lang["en"]["string4"] = "1.What are the government's definition, stance and policy implementations on 'the revitalisation of Tai O'?";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "焦點問題2︰大澳居民對「活化大澳」的定義及意見是甚麼？";
$ies_lang["en"]["string5"] = "2.How do the Tai O residents define and view 'the revitalisation of Tai O'?";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "焦點問題3︰民間組織對「活化大澳」的定義及建議是甚麼？";
$ies_lang["en"]["string6"] = "3.What are the definitions and suggestions of social groups on 'the revitalisation of Tai O'?";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "現在，請列出你的焦點問題︰";
$ies_lang["en"]["string7"] = "Now list your focus questions.";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

?>