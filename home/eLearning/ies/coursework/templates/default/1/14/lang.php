<?php

$ies_lang["b5"]["string1"] = "選擇研究方法後，你需要訂立一個詳細的探究計劃，盡量列明各項工作的細節及量化每項工作，例如：「分派四十份問卷給中四同學填寫」、「整理二十分鐘訪問為文字記錄」、「完成反思記錄」。此外，也要為每項工作定下完成日期。";
$ies_lang["en"]["string1"] = "After choosing the research method(s), draw up a detailed work plan in which you should record the details of every task and quantify the tasks to the full. Examples of working details are 'to distribute 40 questionnaires to S4 students', 'to transcribe a 20-minute interview', 'to complete a 200-word reflection journal', etc. You also have to fix a deadline for every task.";
/*
<?=$ies_lang[$_lang]["string1"]?>
*/

$ies_lang["b5"]["string2"] = "有了詳細的工作計劃，在日後進行探究工作時，便更容易掌握工作的進度。";
$ies_lang["en"]["string2"] = "With a detailed work plan, you can manage your work progress with ease once the investigation is under way.";
/*
<?=$ies_lang[$_lang]["string2"]?>
*/

$ies_lang["b5"]["string3"] = "注意：填寫工作計劃時間表時，應訂立與個人能力相符的工作計劃，也要注意呈交功課的期限及每個階段的工作重點。";
$ies_lang["en"]["string3"] = "Note: You are advised to draw up a work schedule in accordance with your own ability. Pay attention to the submission deadlines and the task objectives of every stage when filling in the schedule. ";
/*
<?=$ies_lang[$_lang]["string3"]?>
*/

$ies_lang["b5"]["string4"] = "工作目的參考列表︰";
$ies_lang["en"]["string4"] = "The following are some task objectives for your reference:";
/*
<?=$ies_lang[$_lang]["string4"]?>
*/

$ies_lang["b5"]["string5"] = "擬訂研究題目";
$ies_lang["en"]["string5"] = "Formulate the topic for enquiry";
/*
<?=$ies_lang[$_lang]["string5"]?>
*/

$ies_lang["b5"]["string6"] = "確定研究目的";
$ies_lang["en"]["string6"] = "Confirm research objectives";
/*
<?=$ies_lang[$_lang]["string6"]?>
*/

$ies_lang["b5"]["string7"] = "確定相關概念";
$ies_lang["en"]["string7"] = "Confirm related concepts";
/*
<?=$ies_lang[$_lang]["string7"]?>
*/

$ies_lang["b5"]["string8"] = "訂立焦點問題";
$ies_lang["en"]["string8"] = "Construct focus questions";
/*
<?=$ies_lang[$_lang]["string8"]?>
*/

$ies_lang["b5"]["string9"] = "撰寫文獻回顧";
$ies_lang["en"]["string9"] = "Write up literature review";
/*
<?=$ies_lang[$_lang]["string9"]?>
*/

$ies_lang["b5"]["string10"] = "選擇研究方法";
$ies_lang["en"]["string10"] = "Select research method(s)";
/*
<?=$ies_lang[$_lang]["string10"]?>
*/

$ies_lang["b5"]["string11"] = "擬寫研究計劃";
$ies_lang["en"]["string11"] = "Draft research work plan";
/*
<?=$ies_lang[$_lang]["string11"]?>
*/

$ies_lang["b5"]["string12"] = "收集資料數據";
$ies_lang["en"]["string12"] = "Collect data/information";
/*
<?=$ies_lang[$_lang]["string12"]?>
*/

$ies_lang["b5"]["string13"] = "歸納整理數據";
$ies_lang["en"]["string13"] = "Edit and generalise data";
/*
<?=$ies_lang[$_lang]["string13"]?>
*/

$ies_lang["b5"]["string14"] = "分析資料數據";
$ies_lang["en"]["string14"] = "Analyse data/information";
/*
<?=$ies_lang[$_lang]["string14"]?>
*/

$ies_lang["b5"]["string15"] = "歸納研究發現";
$ies_lang["en"]["string15"] = "Generalise research findings";
/*
<?=$ies_lang[$_lang]["string15"]?>
*/

$ies_lang["b5"]["string16"] = "撰寫研究建議";
$ies_lang["en"]["string16"] = "Write up recommendations";
/*
<?=$ies_lang[$_lang]["string16"]?>
*/

$ies_lang["b5"]["string17"] = "請填寫工作計劃時間表：";
$ies_lang["en"]["string17"] = "Fill in the work schedule:";
/*
<?=$ies_lang[$_lang]["string17"]?>
*/
?>