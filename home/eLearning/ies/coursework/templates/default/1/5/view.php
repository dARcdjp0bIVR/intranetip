﻿<?
# Modifying By: 
//GET THE STUDENT FOR THIS STEP
$student_answer = $answer["1"];
//DEBUG($student_answer);
$html_question = generateQuestion($_question[1],$_noOfOption,$student_answer);
	
?>
<script>

var question_answer_sperator = "<?=$ies_cfg["question_answer_sperator"]?>";  //question[question_answer_sperator]mark
var new_set_data_sperator = "<?=$ies_cfg["new_set_question_answer_sperator"]?>"; 
var comment_answer_sperator = "<?=$ies_cfg["comment_answer_sperator"]?>";
var comment_array = [];
<?
for($m = 0; $m < $_noOfOption; $m++){
	echo "comment_array[$m] = \"{$_Answer[1][$m]}\";";
}
?>
function get_mark(input){
	var myarray = input.split(question_answer_sperator);
	return parseInt(myarray[1]);
}

function get_comment(mark){
	for(i = 0; i < <?=$_noOfOption?> ; i++){
		var myarray = comment_array[i].split(question_answer_sperator);
		var mark_arr = myarray[0].split("-");
		var min = mark_arr[0];
		var max = mark_arr[1];
		if(mark >= min && mark <= max){
			return myarray[1];
		}
	}
}

function handleAnswer()
{
	var studentAllAns = "";  // FOR STORING ALL THE STUDENT SELECTED RADIO VALUE
	var total_mark = 0;
	
	//APPEND all the student selected radio value to a studentAllAns
	$('input[name^=setQuestion1]:checked').each(function(){
		var _stduentAns = $(this).val();
		  
		if(studentAllAns == ""){
			studentAllAns= _stduentAns;
			total_mark = total_mark + get_mark(_stduentAns);
			
		}
		else{
			studentAllAns += new_set_data_sperator + _stduentAns;
			total_mark = total_mark + get_mark(_stduentAns);
			
		}
	});
	
	//APPEND studentAllAns a hidden form field question[radio][1] for submitting answer
	try { 
		studentAllAns += comment_answer_sperator + total_mark + question_answer_sperator + get_comment(total_mark);
		$("input[name^=question[radio][1]]").val(studentAllAns);
		
		
    	
	} 
	catch(err) { 
		alert(err.toString());  
	}
	
}
function ReDoStep(jParStepID){
 	 	window.location = "coursework.php?step_id="+jParStepID+"&scheme_id="+<?=$scheme_id?>+"&type=1";
}


</script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />


<div class="task_title task_open"><span><?=$task_title?></span></div> <!-- task title -->

<?=$html_task_chain?>

<div class="clear"></div>

<div class="task_wrap"> <!-- task_wtap -->
  <div class="task_writing_top"><div><?=$Lang['IES']['Step']?> <?=$step_seq?>: <?=$step_title?></div></div>

  <div class="writing_area">
  
    <?=$html_answer_display?>
    
    <!-- Start Content Here -->
    <div class="task_content">
	
 	<?=$ies_lang[$_lang]["string1"]?>
	<ul>
	<li><?=$ies_lang[$_lang]["string2"]?></li>
	<li><?=$ies_lang[$_lang]["string3"]?></li>
	<li><?=$ies_lang[$_lang]["string4"]?> </li>
	<li><?=$ies_lang[$_lang]["string5"]?></li>
	<li><?=$ies_lang[$_lang]["string6"]?></li>
	</ul>
	
	<?=$ies_lang[$_lang]["string7"]?><br /><br />


<a href="tips/six_w.php?KeepThis=true&amp;TB_iframe=false&amp;height=510&amp;width=751&amp;modal=true" class="task_demo thickbox"><?=$ies_lang[$_lang]["string8"]?></a><br class="clear"/>
				</div>
				

	<?=$ies_lang[$_lang]["string9"]?><br />
	<br />


                <div class="IES_task_form">
                <h4 class="stu_edit"><strong><?=$ies_lang[$_lang]["string10"]?></strong></h4>

               <h4><?=$ies_lang[$_lang]["string11"]?></h4>
        <div class="wrapper" id="question_area">
				<?=$html_question?>
				
		</div>
		<div class="result" id="result" <?=$submit2_display_html?>><?=$comment_get?></div>
        <!--<div class="view_result"><input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="MM_goToURL('parent','IES_student_task1_step5result.html#result');return document.MM_returnValue" value=" 檢視評估結果 " /></div>-->
            <div class="view_result" id="submit1" <?=$submit1_display_html?>><input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="handleAnswer();doSaveStep(<?=$step_id?>,true);" value=" <?=$ies_lang[$_lang]["string14"]?> " /></div>  
             <div class="view_result"  id="submit2" <?=$submit2_display_html?>><input name="submit2" type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onclick="ReDoStep(<?=$step_id?>)" value=" <?=$ies_lang[$_lang]["string15"]?> " /></div>  
                </div>
          <div class="edit_bottom"> <span><?=$html_answer_lastUpdate?></span>
            <div style="float:right; padding-top:5px">
            </div>
          </div>
	<input type = "hidden" name = "question[radio][1]" id="question1" value="">
 
    <!-- End Content Here -->
  </div>
  
  <div class="task_writing_bottom"><div>
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$prev_step_id?>);" value="  <?=$Lang['IES']['LastStep']?> " />
    <input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="doStep(<?=$next_step_id?>);" value=" <?=$Lang['IES']['NextStep']?> " />
    <!--input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" onClick="handleAnswer();doSaveStep(<?=$next_step_id?>,true);" value="<?=$Lang['IES']['SaveAndNextStep']?>" /-->
    <input type="button" class="formsubbutton" onmouseover="this.className='formsubbuttonon'" onmouseout="this.className='formsubbutton'" onClick="backStage()" value=" <?=$Lang['IES']['Cancel']?> " />
  </div></div>
  
</div> <!-- task_wrap end -->
