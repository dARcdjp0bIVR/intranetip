<?php
/** [Modification Log] Modifying By:
 * *******************************************
 * *******************************************
 */
$_question[1] = array(
                  $ies_lang[$_lang]["choice1"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice2"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice3"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice4"].$ies_cfg["question_answer_sperator"]."-",
                  $ies_lang[$_lang]["choice5"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice6"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice7"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice8"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice9"].$ies_cfg["question_answer_sperator"]."-",
                  $ies_lang[$_lang]["choice10"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice11"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice12"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice13"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice14"].$ies_cfg["question_answer_sperator"]."-",
                  $ies_lang[$_lang]["choice15"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice16"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice17"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice18"].$ies_cfg["question_answer_sperator"]."-",
                  $ies_lang[$_lang]["choice19"].$ies_cfg["question_answer_sperator"]."+",
                  $ies_lang[$_lang]["choice20"].$ies_cfg["question_answer_sperator"]."-",
                );
$_Answer[1] = array("0-29".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["comment1"],
					"30-39".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["comment2"],
					"39-49".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["comment3"],
					"50-55".$ies_cfg["question_answer_sperator"].$ies_lang[$_lang]["comment4"]
				);
$_noOfOption = 4;
$comment_get = '';
$new_question = '';
$_isDone = FALSE;
$submit1_display_html = "";
$submit2_display_html = "style=\"display:none\"";
$ans_id = ($ans_id == "")? -1: $ans_id;
$comment_html = '';


$rs = GetAnswer($UserID, $step_id);



// if the record exists
if(sizeof($rs) != 0){
	$temp_q_arr = array();
	$q_i = 0;
	// if not redo
	if($type != 1){
		$_isDone = TRUE;
		$submit2_display_html = '';
		$submit1_display_html = "style=\"display:none\"";
	}


	$q_arr = split($ies_cfg["new_set_question_answer_sperator"], $rs[0][0]);
	$comment_html = $rs[0][1];
	foreach($q_arr as $value){
		$temp_q_arr[$q_i] = $value;
		$q_i++;
	}
	unset($value);
	$new_question[1] = $temp_q_arr;
}





$html_templateFile = "view.php";


function GetAnswer($UserID, $step_id){
	global $intranet_db;
	$ldb = new libdb();
  	$sql = "SELECT Answer, AnswerActualValue, AnswerID FROM $intranet_db.IES_QUESTION_HANDIN WHERE StepID = '$step_id' AND UserID = '$UserID'";

  	return $ldb->returnArray($sql);
}



function generateQuestion($questionTitle,$_noOfOption,$studentAnswer = null)
{
	global $ies_cfg, $_isDone, $type, $comment_html, $comment_get, $new_question, $ies_lang, $_lang;

	//if there is no record in the database
	// then use the orginal one


	$html = "";

	//split student answer into a set of question ($answeredQuestion), there may / should be more than one question for the radio button
	//$studentAnswer answer format should be "titleA[speratorA]answerA[speratorB]titleB[speratorA]answerB[speratorB]titleC[speratorA]answerC
	// split to array [0] = titleA[speratorA]answerA
	//		    array [1] = titleB[speratorA]answerB
	//		    array [2] = titleC[speratorA]answerC
//	$answeredQuestion = explode($ies_cfg["new_set_question_answer_sperator"], $studentAnswer);

	//split the question and answer from $answeredQuestion
//	for($i = 0; $i < sizeof($answeredQuestion);$i++)
//	{
//
//		//split the answerQuestion one by one from format titleA[speratorA]answerA
//		list($_question,$_answer) = explode($ies_cfg["question_answer_sperator"],$answeredQuestion[$i]);
//
//		//compose a new format $studentAnswerArray["titleA"] = answerA
//		//                     $studentAnswerArray["titleB"] = answerB
//		$studentAnswerArray[$_question] = $_answer;
//	}
	if(!$_isDone){
		for($i =0;$i< sizeof($questionTitle);$i++)
		{
			//$_title = $questionTitle[$i];
			//split the $questionTitle one by one from format $_title[speratorA]$plus(+/-)
			list($_title, $plus) = explode($ies_cfg["question_answer_sperator"],$questionTitle[$i]);

			//Get the selected option
			if(!empty($new_question)){
				list($new_title, $new_plus) = explode($ies_cfg["question_answer_sperator"],$new_question[1][$i]);
				$new_plus = abs($new_plus);
			}
			else{
				$new_plus = 1;
			}


			//=====

			$html .= "<tr class=\"question\">";
			$html .= "<td>{$_questionNo}</td>";
			$html .= "<td>{$_title}</td>";

			for($j = 0; $j < $_noOfOption;$j++)
			{
				$_noOfRadio = $j + 1;
				$_checked = "";
				$mark = $plus.($j+1);
				//get the student answer for this question
	//			$_studentAnswer = $studentAnswerArray[$_title];

				//check whether student answer this question and match to this option
				$_checked = ($j == ($new_plus - 1))?"checked" : "";

				$html .= "<td class=\"option\"><input name=\"setQuestion1[{$i}]\" type=\"radio\" value=\"{$_title}{$ies_cfg["question_answer_sperator"]}{$mark}\" {$_checked}/></td>";
			}
			$html .= "</tr>\n";
		}
	}
	else{
		$questionTitle = $new_question[1];
		for($i =0;$i< sizeof($questionTitle);$i++)
		{
			//$_title = $questionTitle[$i];
			//split the $questionTitle one by one from format $_title[speratorA]$plus(+/-)
			list($_title, $plus) = explode($ies_cfg["question_answer_sperator"],$questionTitle[$i]);
			$plus = abs($plus);
			//echo $plus;
			$image = "<img src=\"images/2007a/IES/selected_tick.gif\" />";

			$html .= "<tr class=\"question\">";
			$html .= "<td>{$_questionNo}</td>";
			$html .= "<td>{$_title}</td>";

			for($j = 0; $j < $_noOfOption;$j++)
			{
				$_noOfRadio = $j + 1;
				$_checked = "";


				//get the student answer for this question
	//			$_studentAnswer = $studentAnswerArray[$_title];

				//check whether student answer this question and match to this option
				if($j == ($plus - 1)){
					$html .= "<td class=\"option\"><img src=\"/images/2009a/ies/selected_tick.gif\" /></td>";
				}
				else
					$html .= "<td class=\"option\"><input name=\"setQuestion1[{$i}]\" type=\"radio\" disabled=\"disabled\" /></td>";
			}
			$html .= "</tr>\n";
		}
	}

	$agree = $ies_lang[$_lang]['string12'];
	$disagree= $ies_lang[$_lang]['string13'];
	$htmlTable = "<table class=\"about_stage estimate_table\">
        <tr><td>&nbsp;</td><td>&nbsp;</td><td class=\"option\">{$agree}</td><td class=\"option\">&nbsp;</td><td class=\"option\">&nbsp;</td><td class=\"option\">{$disagree}</td></tr>";
	$htmlTable .= $html;
	$htmlTable .= "</table>";
	if($_isDone){
		list($_mark_get, $comment_get) = explode($ies_cfg["question_answer_sperator"],$comment_html);
	}

	return $htmlTable;
}

?>
