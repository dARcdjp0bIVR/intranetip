<?

$ies_lang["b5"]["string1"] = "擬訂題目後，要檢查一下這是不是一個好題目。你可嘗試回答下列問題，作初步檢測︰";
$ies_lang["en"]["string1"] = "After formulating the topic, you have to check whether it is good enough. Answer the following questions for a preliminary examination.";

$ies_lang["b5"]["string2"] = "探究題目有沒有清楚訂明探究方向和目標？";
$ies_lang["en"]["string2"] = "Does the topic clearly set out the directions and objectives of the enquiry?";

$ies_lang["b5"]["string3"] = "你有足夠的時間和資源完成探究題目嗎？簡言之，你有能力回答探究問題嗎？";
$ies_lang["en"]["string3"] = "Do you have enough time and resources to finish the IES? In brief, do you have the ability to answer the enquiry question?";

$ies_lang["b5"]["string4"] = "你有能力搜集第一手資料作分析，而不是純粹拼貼其他二手資料嗎？";
$ies_lang["en"]["string4"] = "Are you capable of collecting first-hand information for analysis instead of simply compiling other second-hand information?";

$ies_lang["b5"]["string5"] = "如你的探究題目涉及某些概念，你能清楚定義這些概念嗎？";
$ies_lang["en"]["string5"] = "If your topic for enquiry involves some concepts, can you clearly define them?";

$ies_lang["b5"]["string6"] = "你的題目牽涉甚麼研究或訪問對象？你能接觸到這些探究對象嗎？";
$ies_lang["en"]["string6"] = "What research or interview object(s) does your topic involve? Can you reach them?";

$ies_lang["b5"]["string7"] = "請按一下「範例」看看如何做初步檢測。:";
$ies_lang["en"]["string7"] = "Find out how to do a preliminary examination with a click on 'Exemplars'. ";

$ies_lang["b5"]["string8"] = "範例";
$ies_lang["en"]["string8"] = "Exemplars";

$ies_lang["b5"]["string9"] = "以下的自我評估問題能幫助你檢測初步擬定的題目是不是一個好題目︰";
$ies_lang["en"]["string9"] = "The following self-evaluation questions help check whether your preliminary topic is a good enough one.";

$ies_lang["b5"]["string10"] = "自我評估問題";
$ies_lang["en"]["string10"] = "Self-evaluation Questions";

$ies_lang["b5"]["string11"] = "為幫助你評估所擬定的題目，請回答下列各項。";
$ies_lang["en"]["string11"] = "To help evaluate the topic you have formulated, comment on the following:";

$ies_lang["b5"]["string12"] = "非常不同意";
$ies_lang["en"]["string12"] = "Strongly disagree";

$ies_lang["b5"]["string13"] = "非常同意";
$ies_lang["en"]["string13"] = "Strongly agree";

$ies_lang["b5"]["string14"] = "檢視評估結果";
$ies_lang["en"]["string14"] = "View the evaluation result";

$ies_lang["b5"]["string15"] = "重新評估";
$ies_lang["en"]["string15"] = "Re-evaluation";

$ies_lang["b5"]["choice1"] = "探究過程中可應用到我學過的通識科相關概念。 ";
$ies_lang["en"]["choice1"] = "I can apply the concepts learnt in Liberal Studies in the course of enquiry.";

$ies_lang["b5"]["choice2"] = "題目可讓我探討和了解不同持分者的觀點。";
$ies_lang["en"]["choice2"] = "The topic enables me to study and understand the viewpoints of different stakeholders.";

$ies_lang["b5"]["choice3"] = "我認為這個題目不單是出自個人興趣，還有一定社會意義。";
$ies_lang["en"]["choice3"] = "I think the topic does not only reflect my personal interest but also have a certain social meaning.";

$ies_lang["b5"]["choice4"] = "我認為題目的發揮空間有限，難以寫出有深度的分析。";
$ies_lang["en"]["choice4"] = "I think the topic can hardly be brought into full play which makes an in-depth analysis difficult.";

$ies_lang["b5"]["choice5"] = "我對研究的題材非常熟悉，亦掌握到探究重點。";
$ies_lang["en"]["choice5"] = "I know the subject matter and the focus of the enquiry well.";

$ies_lang["b5"]["choice6"] = "我認為這個題目的焦點明確清晰。";
$ies_lang["en"]["choice6"] = "I think the focus of the topic is clear.";

$ies_lang["b5"]["choice7"] = "我認為題目的探究範圍適中，不會太廣或太窄。";
$ies_lang["en"]["choice7"] = "I think the scope of enquiry for the topic is appropriate. It would not be too wide or too narrow.";

$ies_lang["b5"]["choice8"] = "我有能力完成題目所設定的探究範圍。 ";
$ies_lang["en"]["choice8"] = "I can finish the project based on the enquiry scope set in the topic.";

$ies_lang["b5"]["choice9"] = "我不太肯定用甚麽角度或方法開始探究，只覺得這個題目值得一試。";
$ies_lang["en"]["choice9"] = "I am not sure what perspectives or methods I should adopt to begin the enquiry. I just find the topic worth trying. ";

$ies_lang["b5"]["choice10"] = "在構想這個題目的時候，我已考慮到研究方向和方法，並認為大致可行。";
$ies_lang["en"]["choice10"] = "When I set the topic, I have taken account of the research directions and methods. They are generally practical.";

$ies_lang["b5"]["choice11"] = "我能定義清楚題目的關鍵詞語，使讀者更明白我的探究範圍。";
$ies_lang["en"]["choice11"] = "I can clearly define the keyword(s) of the topic for readers' better understanding of my enquiry scope. ";

$ies_lang["b5"]["choice12"] = "我認為題目的研究對象不會太難接觸，收集一手資料不會有太大困難。";
$ies_lang["en"]["choice12"] = "I think the research objects will not be too inaccessible. Collecting first-hand information would not be too difficult.";

$ies_lang["b5"]["choice13"] = "我已經找到充足的參考資料，亦掌握了有用的背景資料。 ";
$ies_lang["en"]["choice13"] = "I have collected sufficient reference materials and useful background information.";

$ies_lang["b5"]["choice14"] = "我認為題目牽涉的題材比較新穎，我沒有信心找到所需的文獻資料。";
$ies_lang["en"]["choice14"] = "I think the subject matter in the title is too innovative. I am not confident in locating all the literature needed.";

$ies_lang["b5"]["choice15"] = "我有足夠的時間和資源在限期內完成這個題目。 ";
$ies_lang["en"]["choice15"] = "I have sufficient time and resources to finish the project on the topic within the given period of time. ";

$ies_lang["b5"]["choice16"] = "我在構想題目的時候，已經在網上或圖書館做過資料蒐集，題目所需的資料已經大致齊全。";
$ies_lang["en"]["choice16"] = "Before setting the topic, I have done some research on the Internet or in the library. The information required is generally sufficient.";

$ies_lang["b5"]["choice17"] = "我能以客觀方法來量度和驗證題目的結果（例如：有數據支持）。";
$ies_lang["en"]["choice17"] = "I can measure and verify the enquiry outcome with objective methods (e.g. with data support)";

$ies_lang["b5"]["choice18"] = "這個題目不用蒐集任何一手數據或資料，因為網上有很多資料，可以拼合成文。 ";
$ies_lang["en"]["choice18"] = "I do not have to collect any first-hand data or information because there is a lot on the Internet for compilation.";

$ies_lang["b5"]["choice19"] = "題目並沒有預設立場，客觀中立。";
$ies_lang["en"]["choice19"] = "The topic is objective and impartial. There is no preset stance.";

$ies_lang["b5"]["choice20"] = "題目已經有預設答案，我的研究只是為了證實之前已經證實的立場。";
$ies_lang["en"]["choice20"] = "There is a preset answer to the topic. My investigation is to confirm what has been verified.";

$ies_lang["b5"]["comment1"] = "你需要再審視初步擬定的題目，這題目似乎不太可行，亦無明確的探究焦點，你可搜集多些背景資料，再修正題目。如有任何困難，建議你主動約見老師，找出問題。";
$ies_lang["en"]["comment1"] = "You need to re-examine your preliminary topic. If it does not seem feasible and there is no explicit enquiry focus, you may collect more background information and revise it. If you have any difficulties, discuss with your teacher to identify the problem.";

$ies_lang["b5"]["comment2"] = "題目似乎可行，但你需要再收窄範圍，以得出清晰的探究角度。建議你再搜集多些背景資料，這有助你收窄探究範圍、得出探究焦點。建議你與老師商量，看看有沒有改善的方法。";
$ies_lang["en"]["comment2"] = "Your topic seems to be feasible but you have to present a clear enquiry perspective by narrowing down the scope. You are advised to collect more background information in order to narrow down the enquiry scope and draw up more specific focuses.";

$ies_lang["b5"]["comment3"] = "題目大致可行，似乎已有清晰的探究角度，亦能聯繫部份所學的概念和知識。你可再審視搜集所得的資料，看看有沒有需改善的地方。";
$ies_lang["en"]["comment3"] = "Your topic is generally feasible. It seems to have an explicit enquiry perspective and clear association with some of the concepts and knowledge acquired. You may re-examine the information collected to see if any revision is needed.";

$ies_lang["b5"]["comment4"] = "你大概已掌握足夠的資料，亦有清晰的探究角度，並能聯繫所學的概念和知識，題目的可行性頗高。你可整理資料，準備下一步的工作。";
$ies_lang["en"]["comment4"] = "You have collected sufficient information on the topic and have shown clear enquiry perspectives in connection with the concepts and knowledge acquired. The topic is highly feasible. Just tidy up the information and get ready for the next task.";


?>