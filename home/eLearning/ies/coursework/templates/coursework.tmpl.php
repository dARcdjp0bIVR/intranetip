<?
	//modifying By Max
?>
<script language="JavaScript">

$(document).ready(function() {
	bindNormalSave();
	bindDoLastStep();
});

/* This function is used to debug object of javascript */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}

function bindNormalSave() {
  // Bind action to save button
  $('#save').click(function(){
    // Submit for save
    doSaveStep(<?=$step_id?>,false);
  });
}

function backStage(){
  document.form1.action = "stage.php";
  document.form1.submit();
}

function doLastStep() {
    // Submit for save
    $.ajax({
      url:      "step_update.php",
      type:     "POST",
      data:     $("#form1").serialize(),
      error:    function(xhr, ajaxOptions, thrownError){
                  alert(xhr.responseText);
                },
      success:  function(data){
                  if(confirm("<?=$Lang['IES']['Coursework__general']['string1']?>")){
                    $.ajax({
                      url:      "step2task_handin_update.php",
                      type:     "POST",
                      data:     "task_id=<?=$task_id?>",
                      async:	false,
                      error:    function(xhr, ajaxOptions, thrownError){
                                  alert(xhr.responseText);
                                },
                      success:  function(data){
//                                  alert(data);
                                }
                    });
                    
                    backStage();
                  }
                }
    });
}

function bindDoLastStep() {
  // Bind action to save button
  $('#save_submit').click(function(){doLastStep()});
}

function doSaveStep(jParStepID,goNextStep){
  $.ajax({
    url:      "step_update.php",
    type:     "POST",
    data:     $("#form1").serialize(),
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(xml){
                if(goNextStep)
                {
                  //if goNextStep == true , go to next step
                  doStep(jParStepID);
                }
                else
                {
                  date = $(xml).find("date").text();
                  $("#lastModifiedDate").html(date);
                  return 1;
                }
              }
  });
}

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
        '\n\nThe output div should have already been updated with the responseText.'); 
}

</script>

<form name="form1" id="form1" method="POST">

<?php include_once("paperframe.tmpl.php") ?>

<input type="hidden" name="stage_id" value="<?=$stage_id?>" />
<input type="hidden" name="task_id" value="<?=$task_id?>" />
<input type="hidden" name="step_id" value="<?=$step_id?>" />
<input type="hidden" name="scheme_id" value="<?=$scheme_id?>" />

</form>