

	<h4>新增文獻</h4>
  <div class="edit_task_pop_board_write standard_height">
  <table class="form_table IES_table">
      <tr>
        <td>語言</td>
        <td>:</td>
        <td>
        <table class="innertable">
                    <col class="field_title" />
					<col class="field_title" />
                    	<tr><td><input type="radio" name="2" checked="checked" id='bi_ch'/><label for='bi_ch'>中文</label></td>
                        	<td><input type="radio" onClick="self.location='?bi_lang=en'" name="2" id='bi_en'/><label for='bi_en'>English</label></td></tr>
                    </table>
        </td>
      </tr>
      
      <tr>
        <td>類型</td>
        <td>:</td>
        <td>
        <select>
        <optgroup label="書本或報告">
        <option>一名或以上作者</option>
        <option>只有編者</option>
        <option>沒有作者或編者</option>
        <option>書內其中一篇文章</option>
        <option>翻譯書</option>
        </optgroup>
        
        <optgroup label="期刊、雜誌或報紙">
        <option>一至七位作者</option>
        <option>八位或以上作者（列出首六位及最後一位作者）</option>
        </optgroup>
        
        <optgroup label="網上資源">
        <option>網上資源</option>
        </optgroup>
        </select>
        </td>
      </tr>
      
      <tr>
        <td>作者姓名</td>
        <td>:</td>
        <td><input type="text" class="input_med" />
        <br />
        <input type="text" class="input_med" />&nbsp;&nbsp;&nbsp;<a href="IES_student_task1_step4(bibi)add02M.html">+更多作者</a></td>
      </tr>
      
      <tr>
        <td>年份</td>
        <td>:</td>
        <td><input type="text" /></td>
      </tr>
      
      <tr>
        <td>書名</td>
        <td>:</td>
        <td><input type="text" class="input_med" /></td>
      </tr>
      
      <tr>
        <td>版數 (如再版)</td>
        <td>:</td>
        <td><input type="text" /></td>
      </tr>
      
      <tr>
        <td>出版地點</td>
        <td>:</td>
        <td><input type="text" /></td>
      </tr>
      
      <tr>
        <td>出版社</td>
        <td>:</td>
        <td><input type="text" class="input_med" /></td>
      </tr>
      
      <tr>
        <td>標籤</td>
        <td>:</td>
        <td><input type="text" class="input_med" /></td>
      </tr>
      
      <tr>
        <td>內容</td>
        <td>:</td>
        <td>頁數: <input type="text" class="input_num" /><br />
        <textarea rows="4"></textarea>
        <br />
        <br /><a href="IES_student_task1_step4(bibi)add02M.html">+更多內容</a></td>
      </tr>

      <col class="field_title" />
      <col  class="field_c" />
    </table>
  