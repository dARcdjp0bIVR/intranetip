<?php 
// Modifying by : Max
//For save , in case missing any important paramter , it will stop the program
if(trim($scheme_id) == "" || trim($stage_id) == "" || trim($scheme_id) =="" || trim($stage_id) == "")
{
	echo "Invalid Prameter!! Program exit!<br/>";
	exit();
}
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script language="JavaScript">

<?=$html_js_load_task_map?>

function doSaveTask(parTaskID){
  $.ajax({
    url:      "task_update.php",
    type:     "POST",
    data:     $("#task_"+parTaskID).serialize(),
    async:		false,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                doLoadTaskBlock(parTaskID);
                // Show description button after task content saved
                $("#task_block_"+parTaskID).find("a.show_intro").show();
              }
  });
}

function doLoadTaskBlock(parTaskID){

  // Find first step ID of task by task ID from mapping
  for(var i=0; i<task_step_map.length; i++)
  {
    var t_task_id = task_step_map[i][0];
  
    if(parTaskID == t_task_id)
    {
      var t_step_id = task_step_map[i][1];
      break;
    }
  }

  // load task block by ajax (content or description)
  $.ajax({
    url:        "../ajax/ajax_load_task_block.php",
    type:       "POST",
    async:      false,
    data:       "task_id="+t_task_id+"&step_id="+t_step_id,
    error:      function(xhr, ajaxOptions, thrownError){
                  alert(xhr.responseText);
                },
    success:    function(data){
                  $("#task_block_"+parTaskID).html(data);
                }
  });
}

// Load edit panel (text area and save buttons) to task block
function doEditTask(parTaskID){
  $.ajax({
    url:      "../ajax/ajax_load_edit_task.php",
    type:     "POST",
    async:    false,
    data:     "task_id="+parTaskID+"&UserID"+<?=$UserID?>,
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                $("#task_block_"+parTaskID).find("a.show_intro").hide();
                $("#task_block_"+parTaskID).find("div.task_steps").hide();
                $("#task_content_"+parTaskID).html(data);
              }
  });
}

// Load Teacher Comment (Form) to 
function doGetComment(parTaskID){
	if ( $("#Comment_block_"+parTaskID).is(':visible') ) {
                	$("#Comment_block_"+parTaskID).hide();
    }
  	else{
  		$.ajax({
	    url:      "../ajax/ajax_load_teacher_comment.php",
	    type:     "POST",
	    async:    false,
	    data:     "task_id="+parTaskID,
	    error:    function(xhr, ajaxOptions, thrownError){
	                alert(xhr.responseText);
	              },
	    success:  function(data){
	    			if(data != "0"){
	    				$("#Comment_block_"+parTaskID).html(data);
	    				$("#Comment_block_"+parTaskID).show("fast");	
	    			}
	              }
	
  });
  }
}

// Load description of task on button click
function doLoadDescription(parTaskID){
  $.ajax({
    url:      "../ajax/ajax_load_task_description.php",
    type:     "POST",
    async:    false,
    data:     "task_id="+parTaskID+"&close_btn=1",
    error:    function(xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
              },
    success:  function(data){
                $("#task_block_"+parTaskID).find("div.task_steps").hide();
                $("#task_content_"+parTaskID).html(data);
              }
  });
}

$(document).ready(function(){

  // Bind action to button to show edit tool bar, and hide edit button on click
  $("a[name=edit_task_btn]").live("click", function(){
    var task_id = $(this).attr("task_id");
  
    // Eric Yip (20100603): Direct edit
    //$(this).parent().find(".task_index_edit").show();
    //$(this).hide();
    doEditTask(task_id);
  });
});

/*****************************************************************/

//$(document).ready(function(){
//	initializeAnswerArray("<?= addslashes($answer[1]) ?>");
//	initializeResultTable();
//});

/******************************************************************************************
	Functions in this page can be categorize into the followings,
	please put the functions under one of the category.
	If you need to create a new category, just follow the format to create.
	- Global Variables
	- Initialization Functions
	- Answer Conversion Manipulation Functions
	- Actions Binding
	- Others
******************************************************************************************/

////////////////////////////////////////////////////
//	Start: Global Variables
////////////////////////////////////////////////////
var rowIndex=0;
var answerArray = new Array();
var $currentEditingElement;
////////////////////////////////////////////////////
//	End: Global Variables
////////////////////////////////////////////////////


////////////////////////////////////////////////////
//	Start: Initialization Functions
////////////////////////////////////////////////////
function initializeAnswerArray(ParAnswer) {
	// put the answer to question field
	$("input[name=question\\[table\\]\\[1\\]]").val(ParAnswer);
	answerArray = ConvertAnswerToAnswerArray(ParAnswer);
}
/* object debug */
function jdebug(objName) {
	obj = eval(objName);
	var temp = "";
	for (x in obj)
			temp += x + ": " + obj[x] + "\n";
	alert (temp);
}

function initializeResultTable() {	
	$.each(answerArray, function(RowPos, RowElement) {
		createRow(++rowIndex, RowElement, $("#addRowTr"));
	});
	bindAddBtn();
}
//////////////////////////////////////////////////
//	End: Initialization Functions
//////////////////////////////////////////////////

////////////////////////////////////////////////////
//	Start: Answer Conversion Manipulation Functions
////////////////////////////////////////////////////
function ConvertAnswerToAnswerArray(ParAnswer) {
	tempAnswerArray = new Array();
	if (ParAnswer) {
		// Break down the answer to array to store
		var RowArray = new Array();
		RowArray = ParAnswer.split("<?=$ies_cfg["new_set_question_answer_sperator"]?>");
		
		$.each(RowArray, function(key, element) {
			BoxArray = element.split("<?=$ies_cfg["question_answer_sperator"]?>");
			tempAnswerArray[tempAnswerArray.length] = BoxArray;
		});
	}
	return tempAnswerArray;
}
function ConvertAnswerFromArrayToString(ParAnswerArray) {

	// Remove Items that deleted by users
	var length = ParAnswerArray.length;
	tempArray = new Array();
	for(i=0;i<length;i++) {
		if (ParAnswerArray[i] == null) {
			// do nothing
		} else {
			tempArray[tempArray.length] = ParAnswerArray[i];
		}
	}
	
	rowString = new Array();
	$.each(tempArray, function(key, element) {
		rowString[rowString.length] = element.join("<?=$ies_cfg["question_answer_sperator"]?>");
	});
	
	concatedContent = rowString.join("<?=$ies_cfg["new_set_question_answer_sperator"]?>");
	
	return concatedContent;
}
////////////////////////////////////////////////////
//	End: Answer Conversion Manipulation Functions
////////////////////////////////////////////////////


////////////////////////////////////////////////////
//	Start: Actions Binding
////////////////////////////////////////////////////
/* Bind Add Edit Row Function */
function bindAddBtn() {
	var $addRowBtn = $(".IES_task_form #addRow");
	$addRowBtn.bind('click',function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		$(".edit_dim").hide();
		createEdit($("#addRowTr"));
	});
}

/* Bind Edit Function */
function bindEdit(ParRowIndex) {
	var currentEditId = 'edit_'+ ParRowIndex;
	$('#'+currentEditId).click(function() {
		if ($currentEditingElement) {
			Cancel($currentEditingElement.parent().parent());
		}
		$currentEditingElement = $("#"+currentEditId);
		$currentEditingRow = $currentEditingElement.parent().parent().parent();
		createEdit($currentEditingRow, true, ParRowIndex);
		
		currentAnswer = answerArray[ParRowIndex-1];
		$editRow = $("input", $(".newRow"));
		$.each($editRow, function(key, element) {
			$(element).val(currentAnswer[key]);
		});
		$currentEditingRow.hide();
		$(".edit_dim").hide();
	});
}

/* Bind Remove Function */
function bindRemove(ParRowIndex) {
	var currentRemoveId = 'remove_'+ ParRowIndex;
	$('#'+currentRemoveId).click(function() {
		UpdateAnswerArray("Remove",ParRowIndex);
		Save(answerArray);
		$removeElement = $("#"+currentRemoveId).parent().parent().parent().remove();
	});
}

/* Bind Cancel Function */
function bindCancel() {
	$(".cancel").click(function() {
		Cancel();
		$(".edit_dim").show();
	});
}

/* Bind Saving Function */
function bindSave(ParInsertBefore, ParIsEdit, ParEditIndex) {
	$('.save').click(function() {	
		$targetRow = $(this.parentNode.parentNode);
		SaveOrUpdate(ParInsertBefore, ParIsEdit, ParEditIndex, $targetRow);
		$(".edit_dim").show();
	});
	
	// Handling the submit that need to save at the same time
	$('#save_submit').unbind('click.SaveSubmit');
	$('#save_submit').bind("click.SaveSubmit",function() {	
		$targetRow = $("tr[class*=newRow]");
		SaveOrUpdate("", "", ParEditIndex, $targetRow);
	});
}
////////////////////////////////////////////////////
//	End: Actions Binding
////////////////////////////////////////////////////


////////////////////////////////////////////////////
//	Start: Others
////////////////////////////////////////////////////
function createEdit(ParInsertBefore, ParIsOldRecord, ParEditIndex) {
	var displaySubmit=false, newRowCount=0;
	var maxRows=1000;
	var rowSpeed = 300;
	var $table = $("#addWorkTable");
	var $tableBody = $("tbody",$table);
	var $insertBeforeRow = ParInsertBefore;
	var blankRowID = "blankRow";
	var newRowClass = "newRow";
	var oddRowClass = "rowOdd";
	var evenRowClass = "rowEven"
	var hiddenClass = "hidden"
	$('#addRowTr').hide();
	if(newRowCount < maxRows){
		newRowCount++;
		//get the class on the first row...
		if($tableBody.find("tr:first-child").hasClass(evenRowClass)){
			//the curent first row is even, so we add an odd class
			newClasses = hiddenClass+" "+newRowClass+" "+oddRowClass;
		}else{
			//the current first row is odd, so we add an even row
			newClasses = hiddenClass+" "+newRowClass+" "+evenRowClass;
		}
		//clone the blank row, put the clone at the top, set the correct classes, remove the ID, animate the divs inside
		//normally I'd use .addClass(), but I want the classes I set to replace the current set classes, so I use .attr("class") to overwrite the classes
		newRow = $("#"+blankRowID,$tableBody).clone(true).insertBefore($insertBeforeRow).attr("class",newClasses).removeAttr("id").show().find($insertBeforeRow).slideDown(rowSpeed);
	}
	//disable button so you know you've reached the max
	if(newRowCount >= maxRows){
		$addRowBtn.attr("disabled","disabled");//set the "disabled" property on the button
	}
	bindSave(ParInsertBefore, ParIsOldRecord, ParEditIndex);
	bindCancel();
	return false; //kill the browser default action
}


// Create a row with row index and answer array of a row and specify insert before where
function createRow(ParRowIndex, ParElementArray, ParInsertBefore) {
	var row = "<tr><td class=\"num_check\">"+ParRowIndex+"</td>";
	
	$.each(ParElementArray, function(key, element) {
		if (element == '') {
			element = '&nbsp;';
		} else {
			element = htmlspecialchars(element);
		}
		row = row + "<td class=\"text_stu\">" + element + "</td>";
	});	
	
	row = row + "<td class=\"tool_col_min\" nowrap><div class=\"table_row_tool\"><a href=\"javascript: void(0);\" class=\"edit_dim\" title=\"Edit\" id=\"edit_" + ParRowIndex + "\"></a><a href=\"javascript: void(0);\" class=\"delete_dim\" title=\"Delete\" id=\"remove_" + ParRowIndex + "\"></a></div></td></tr>";
	$(row).insertBefore(ParInsertBefore);
	bindEdit(ParRowIndex);
	bindRemove(ParRowIndex);
	return row;
}


function Save(ParAnswerArray) {

	concatedContent = ConvertAnswerFromArrayToString(ParAnswerArray);
	
	$("input[name=task_content]").val(concatedContent);

	result = false;
	$.ajax({
	    url:      "task_update.php",
	    type:     "POST",
	    data:     $("#formx").serialize(),
	    async:	  false,
	    error:    function(xhr, ajaxOptions, thrownError){
	                alert(xhr.responseText);
	              },
	    success:  function(xml){
                    date = $(xml).find("date").text();
                    $("#lastModifiedDate").html(date);
					result = true;
	              }
	});
	$(".save").unbind("click");
	return result;
}

function UpdateEditRemoveIds(ParUpdateIndex) {
	UpdateElementIds(ParUpdateIndex,"edit_");
	UpdateElementIds(ParUpdateIndex,"remove_");
}

function UpdateElementIds(ParUpdateIndex,ParTargetIndicator) {
	$targets = $("a[id*="+ParTargetIndicator+"]");
	$.each($targets, function(key, element) {
		var splitedId = $(element).attr("id").split("_");
		var IdNum = splitedId[1];
		if (IdNum>ParUpdateIndex) {
			$(element).attr("id", ParTargetIndicator+(IdNum-1));
			bindRemove(IdNum-1);
		}
	});
}

function UpdateAnswerArray(ParUpdateType,ParUpdateIndex,ParUpdateElement) {
	switch (ParUpdateType.toUpperCase()) {
		case "UPDATE":
			answerArray[ParUpdateIndex-1] = ParUpdateElement;
			break;
		case "REMOVE":
			answerArray[ParUpdateIndex-1] = null;
			break;
		default:	// NEW
			answerArray[answerArray.length] = ParUpdateElement;
			break;
	}
}


function getContentFromRow($ParTargetRow) {
		// Catch Content From Updating Row
		$contentElement = $("input[name=content1]",$ParTargetRow[0]);
	
		var contentArray = new Array();		
		$.each($contentElement, function(index,value) {
			contentArray[contentArray.length] = $(value).val();
		});
		return contentArray;
}

function SaveOrUpdate(ParInsertBefore, ParIsEdit, ParEditIndex, $targetRow) {
	
		contentArray = getContentFromRow($targetRow);
		
		if (ParEditIndex) {
			updateType = "Update";
		} else {
			updateType = "New";
		}
		UpdateAnswerArray(updateType, ParEditIndex, contentArray);

		if (Save(answerArray)) {
			if (isNaN(ParEditIndex)) {
				createRowIndex = ++rowIndex;
			} else {
				createRowIndex = ParEditIndex;
			}
			
            createRow(createRowIndex, contentArray,ParInsertBefore);
            if (ParIsEdit) {
            	ParInsertBefore.remove();
            }
            $targetRow.remove();
            $('#addRowTr').show();
		}
}

function Cancel() {
	$(".newRow").remove();
	if ($currentEditingElement) {
		$currentEditingRow = $currentEditingElement.parent().parent().parent();
		$currentEditingRow.show();
	}
    $('#addRowTr').show();
}

////////////////////////////////////////////////////
//	End: Others
////////////////////////////////////////////////////
</script>

<!-- Eric Yip (20100713): Move form to end of the page
<form name="form1" id="form1" method="POST">
-->

<?php include_once("paperframe.tmpl.php") ?>

<form name="form1" id="form1" method="POST">

<input type="hidden" name="scheme_id" value="<?=$scheme_id?>" />
<input type="hidden" name="stage_id" value="<?=$stage_id?>" />
<!--input type="hidden" name="html_scheme_id" value="<?=$scheme_id?>" /-->
<!--input type="hidden" name="html_stage_id" value="<?=$stage_id?>" /-->

</form>