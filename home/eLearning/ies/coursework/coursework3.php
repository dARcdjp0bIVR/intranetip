<?php
//modifying By fai
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ies_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/ies/libies.php");
include_once($PATH_WRT_ROOT."includes/ies/libies_ui.php");

//include_once($PATH_WRT_ROOT."includes/libportal.php");

intranet_auth();
intranet_opendb();

/**
 * GET INPUT PARAMETER
 */
 $step_id = intval(trim($step_id));
// debug_r($step_id);
 if(empty($step_id) || !is_int($step_id))
 {
	 echo "Invalide access , Empty Step ID<br/>";
	 exit();
 }

$objIES = new libies();

$layout_tmpl = ($objIES->isIES_Teacher($UserID) || $objIES->IS_ADMIN_USER($UserID)) ? $ies_cfg["PreviewFrontEndInterface"] : $ies_cfg["DefaultFrontEndInterface"];
$linterface = new interface_html($layout_tmpl);
$CurrentPage = "StudentProgress";

### Title ###
$title = $ies_lang['IES_TITLE'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objIES->GET_MODULE_OBJ_ARR();

//$scheme_arr = $objIES->GET_SCHEME_ARR();

$stepDetails = $objIES->getStepDetail($step_id); 

//list($stage_id, $stage_seq, $stage_title, $task_id, $task_title, $step_title, $step_no, $step_seq) = getStepDetail($step_id);
list($stage_id, $stage_seq, $stage_title, $task_id, $task_title, $step_title, $step_no, $step_seq) = $stepDetails;
$task_step_arr = $objIES->getTaskStepArr($task_id);
$stage_details = $objIES->GET_STAGE_DETAIL($stage_id);
list($stage_title, $stage_sequence, $stage_deadline) = $stage_details;
$html_stage_deadline = $stage_deadline;

$scheme_arr = $objIES->GET_USER_SCHEME_ARR($UserID);
$html_scheme_selection = libies_ui::GET_SCHEME_SELECTION($scheme_arr, $scheme_id);

$stage_arr = $objIES->GET_STAGE_ARR($scheme_id);
$html_stage_selection = libies_ui::GET_STAGE_SELECTION($stage_arr,$stage_id,$scheme_id);

//INCLUDE THE SCHEME MODEL / DATA / CONFIG FILE FOR THIS STEP , ALL THE VARIABLE IN THIS FILE SHOULD BE STARTED WITH $schemeCfg_
//$schemeModelFile = "templates/default/scheme_model.php";
//include_once($schemeModelFile);

//####Lang in IES~~~<<<<
$_lang = libies::getThisSchemeLang();   //please don't change this variable name , it is a global for the scheme lang (used in all view.php and model.php)
$lang_LangFile = "lang.php";
//$html_lang_LangFile = "templates/default_lang/{$stage_seq}/{$step_no}/{$lang_LangFile}";
$html_lang_LangFile = "templates/default/{$stage_seq}/{$step_no}/{$lang_LangFile}";
if(file_exists($html_lang_LangFile)){
//  ob_start();
  include_once($html_lang_LangFile);
  //$html_content = ob_get_contents();
//  ob_end_clean();
}

//INCLUDE THE SPECIFIC MODEL FILE FOR THIS STEP, ALL THE VARIABLE IN THIS FILE should BE STARTED with $stepCfg_
//$stepModelFile = "templates/default_lang/{$stage_seq}/{$step_no}/model.php";
$stepModelFile = "templates/default/{$stage_seq}/{$step_no}/model.php";
include_once($stepModelFile);

//debug_r($task_step_arr);

$prev_step_id = getNextPreStep(0,$task_step_arr,$step_id);
$next_step_id = getNextPreStep(1,$task_step_arr,$step_id);

/** HANDLE THE HTML TASK CHAIN **/
$step_arr = $objIES->GET_STEP_ARR($task_id);
$html_task_chain = libies_ui::GET_STEP_CHAIN($step_arr, $step_id);

$current_step_answer_arr = $objIES->GET_CURRENT_STEP_ANSWER($step_id, $UserID);


$html_answer_lastUpdate = getLastModifyAnswerDateDivision($current_step_answer_arr);


$answer = parseAnswer($current_step_answer_arr);

$answerAreaTemplate = $objIES->template_AnswerArea01();
$html_answer_display = $objIES->getAnswerArea($task_id, $step_id, $UserID,$answerAreaTemplate);

# set scheme id and stage id for submission
$html_scheme_id = $scheme_id;
$html_stage_id = $stage_id;
$html_task_id = $task_id;

// $html_templateFile this variable exist in each model.php 

//$html_templateFile = "templates/default_lang/{$stage_seq}/{$step_no}/{$html_templateFile}";
$html_templateFile = "templates/default/{$stage_seq}/{$step_no}/{$html_templateFile}";


//echo $html_templateFile."<br/>";
// Centralize paper frame content

if(file_exists($html_templateFile)){
  ob_start();
  include_once($html_templateFile);
  $html_content = ob_get_contents();
  ob_end_clean();
}


$html_frameInfo = $objIES->GET_PAGEFRAME_INFO($stage_sequence);

$linterface->LAYOUT_START();
echo "<!-- layout : {$stage_seq} / {$step_no} -->\n";  //for debug , easy to trace the layout file
include_once("templates/coursework.tmpl.php");
$linterface->LAYOUT_STOP();
intranet_closedb();
exit();

















/**
* GET STUDENT ANSWER
*
* @param : INT $task_id TASK ID
* @param : INT $student_id STUDENT USER ID
* @return : ARRAY array of student all step answer for a task ($task_id)
* 
*/
function getAllStepAnswer($task_id, $student_id)
{
  global $intranet_db;
  
  $ldb = new libdb();
  
  $sql =  "
            SELECT
              handin.StepID,
              handin.QuestionType,
              handin.QuestionCode,
              handin.Answer,
	          handin.AnswerActualValue,
			  handin.AnswerExtra,
			  step.StepID as \"s_StepID\" /*should be same as handin.StepID*/,
			  step.Sequence as StepSeq,
			  task.TaskID,
			  task.Sequence as TaskSeq,
			  stage.StageID,
			  stage.Sequence as StageSeq
            FROM
              {$intranet_db}.IES_QUESTION_HANDIN handin
            INNER JOIN {$intranet_db}.IES_STEP step
              ON handin.StepID = step.StepID
			INNER JOIN {$intranet_db}.IES_TASK task
			  ON step.TaskID = task.TaskID
			INNER JOIN {$intranet_db}.IES_STAGE stage
			  ON task.StageID = stage.StageID
            WHERE
              step.TaskID = {$task_id} AND
              handin.UserID = {$student_id}
          ";
//debug($sql);
  $_taskAnswerArr = $ldb->returnArray($sql);
  
  $returnArr = array();
  for($i=0; $i<count($_taskAnswerArr); $i++)
  {
    $t_step_id						= $_taskAnswerArr[$i]["StepID"];
    $t_task_id						= $_taskAnswerArr[$i]["TaskID"];
    $t_stage_id						= $_taskAnswerArr[$i]["StageID"];
    $t_question_type				= $_taskAnswerArr[$i]["QuestionType"];
    $t_question_code				= $_taskAnswerArr[$i]["QuestionCode"];
    $t_question_answer				= $_taskAnswerArr[$i]["Answer"];
    $t_question_answer_actual_value = $_taskAnswerArr[$i]["AnswerActualValue"];
    $t_question_answer_extra		= $_taskAnswerArr[$i]["AnswerExtra"];
	$t_question_step_seq		= $_taskAnswerArr[$i]["StepSeq"];
	$t_question_task_seq			= $_taskAnswerArr[$i]["TaskSeq"];
	$t_question_stage_seq		= $_taskAnswerArr[$i]["StageSeq"];

    $returnArr[$t_step_id][$t_question_code] =  array(
											      "StepID" => $t_step_id,
												  "TaskID" => $t_task_id,
												  "StageID" => $t_stage_id,
                                                  "QuestionType" => $t_question_type,
                                                  "Answer" => $t_question_answer,
		                                          "AnswerActualValue" => $t_question_answer_actual_value,
                                                  "AnswerExtra" => $t_question_answer_extra,
		                                          "StepSeq" => $t_question_step_seq,
												  "TaskSeq" => $t_question_task_seq,
												  "StageSeq" => $t_question_stage_seq
                                                );
  }

  return $returnArr;
}

function parseAnswer($raw_ans_arr)
{
  global $ies_cfg;

  $answer_arr = array();
  for($i=0; $i<count($raw_ans_arr); $i++)
  {
    $t_question_code		= $raw_ans_arr[$i]["QuestionCode"];
    $t_question_type		= $raw_ans_arr[$i]["QuestionType"];
    $t_answer				= $raw_ans_arr[$i]["Answer"];
    $t_answer_actual		= $raw_ans_arr[$i]["AnswerActualValue"];
    $t_answer_extra			= $raw_ans_arr[$i]["AnswerExtra"];
    $t_answer_DateModified	= $raw_ans_arr[$i]["DateModified"];

    switch($t_question_type)
    {
      case "text":
	  //same as case "textarea"
      case "textarea":
        $answer_arr[$t_question_code] = $t_answer;
        break;
      case "select_mod":
        $answer_arr[$t_question_code]["standard"] = "'".implode("', '", explode($ies_cfg["question_answer_sperator"], $t_answer))."'";
        if(!empty($t_answer_extra))
        {
          $answer_arr[$t_question_code]["custom"] = explode($ies_cfg["question_answer_sperator"], $t_answer_extra);
        }
        break;
	   case "radio":
        $answer_arr[$t_question_code] = $t_answer;
        break;
       case "table":
        $answer_arr[$t_question_code] = $t_answer;
        break;
    }
  }

  return $answer_arr;
}



/**
* Generate TASK CHAIN WITH HTML format
*
* @param : Array $task_step_arr , this value can be get from function "getTaskStepArr" 
* @param : INT  $currentStepId current step id
* @return : String HTML TASK CHAIN
* 
*/
/*
function getTaskChain($task_step_arr,$currentStepId){
	$html_task_chain = "";
	for($i=0; $i<count($task_step_arr); $i++)
	{
	  $t_step_id = $task_step_arr[$i]["StepID"];
	  $t_step_no = $task_step_arr[$i]["StepNo"];
	  $t_step_title = $task_step_arr[$i]["Title"];
	  $t_step_seq = $task_step_arr[$i]["step_seq"];

	  if($t_step_id == $currentStepId)
	  {
		$prev_step_id = $task_step_arr[$i-1]["StepID"];
		$next_step_id = $task_step_arr[$i+1]["StepID"];
	  }

	  $html_task_chain .= "<li class=\"circle\">";
	  $html_task_chain .= ($t_step_id == $currentStepId) ? "<a class=\"current\" href=\"#\">" : "<a href=\"javascript:doStep({$t_step_id})\">";
	  $html_task_chain .= $t_step_seq."</a></li>";
	  $html_task_chain .= (next($task_step_arr) === false) ? "" : "<li class=\"spacer\"></li>";
	}

	return $html_task_chain;
}
*/
/**
* GET NEXT OR PRE STEP ID 
*
* @param : INT $nextStep , indicate need next step or prev step
* @param : Array TASK DETAIS FOR A GIVEN TASK ID
* @param : INT $currentStepId , CURRENT STEP ID
* @return : INT pre / next step  ID , Depend on $nextStep;
*/
function getNextPreStep($nextStep = 1 , $task_step_arr,$currentStepId)
{

	for($i = 0; $i < count($task_step_arr);$i++)
	{
		$t_step_id = $task_step_arr[$i]["StepID"];

		if($t_step_id == $currentStepId)
		{
			$prev_step_id = $task_step_arr[$i-1]["StepID"];
			$next_step_id = $task_step_arr[$i+1]["StepID"];
			break;
		}
	}
	if($nextStep == 1)
	{
		return $next_step_id;
	}
	else
	{
		return $prev_step_id;
	}
}



/**
* GET LAST MODIFY ANSWER DATE
*
* @param : array  $student_step_answer_arr array of student answer
* @return : string date of the last modify answer
* 
*/
function getLastModifyAnswerDate($student_step_answer_arr)
{
	$answerLastUpdate = "";

	//STEP INTO THE STUDENT ANSWER ONE BY ONE
	for($i = 0;$i < sizeof($student_step_answer_arr);$i++)
	{

		$answerLastUpdate = $student_step_answer_arr[$i]["DateModified"];
		if($answerLastUpdate != "")
		{
			//ALTHOUGH THERE MAY BE LOT OF ANSWER FOR EACH STEP, IT MAKE A ASSUMPTION THAT ALL THE ANWER SHOULD HAVE THE SAME LAST UPDATE TIME
			//SO TAKE THE FIRST ONE FOR NOT EMPTY AND EXIT THE LOOP/ CHECKING
			break;
		}
	}

	$answerLastUpdate  = (trim($answerLastUpdate) == "") ? "--" : $answerLastUpdate ;
	return $answerLastUpdate;
}

function getLastModifyAnswerDateDivision($student_step_answer_arr) {
	global $Lang;
	$lastModifiedDate = getLastModifyAnswerDate($student_step_answer_arr);
	$HTML = <<<HTMLEND
	<div style="float:left"> {$Lang['IES']['LastModifiedDate']}{$Lang['_symbol']["colon"]}</div><div style="float:left" id="lastModifiedDate">{$lastModifiedDate}</div>
HTMLEND;
	return $HTML;
}


?>


