<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->HAS_RIGHT())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$ParArr["BookID"] = $BookID;
$ParArr["ReviewID"] = $ReviewID;
$ParArr["Choose"] = $Helpful;

$LibeLib->addBookReviewHelpful($ParArr);

intranet_closedb();

if ($returnFile == "")
header("Location: book_detail.php?BookID=$BookID&CurrentPageNum=$CurrentPageNum&DisplayNumPage=$DisplayNumPage");
else if( $returnFile == "elib_top_reviewers.php")
header("Location: ".$returnFile."?ReviewerID=$ReviewerID&BookID=$BookID&CurrentPageNum=$CurrentPageNum&DisplayNumPage=$DisplayNumPage&Ranking=$Ranking");
else if( $returnFile == "elib_top_reviews.php")
header("Location: ".$returnFile."?ReviewID=$ReviewID&BookID=$BookID&CurrentPageNum=$CurrentPageNum&DisplayNumPage=$DisplayNumPage&Ranking=$Ranking");
?>