<?php
//using by 
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");


intranet_auth();
intranet_opendb();
######################################################

$method = (isset($_REQUEST['method']))? trim($_REQUEST['method']) : "";
$BookID = (isset($_REQUEST['BookID']))? trim($_REQUEST['BookID']) : "";
$currUserID = $_SESSION['UserID'];

$lelib = new elibrary();

$ParArr["BookID"] = $BookID;
$ParArr["currUserID"] = $currUserID;

switch($method){
	case "add_favourite":
	
		## Check favourite exist for this BOOK + USER already or not
		$aryParam = array("BookID"=>$BookID, "currUserID"=>$currUserID);
		$is_succ =  $lelib->addMyFavourite($aryParam);
		echo $is_succ;
		
		break;	
	case "remove_favourite":
		
		echo $lelib->removeMyFavourite($ParArr);
		
		break;
		
	case "add_recommend":
	
		$Mode = (isset($_REQUEST['Mode']))? trim($_REQUEST['Mode']) : "";
		$description = (isset($_REQUEST['description']))? trim($_REQUEST['description']) : "";
		$recommendSchool = (isset($_REQUEST['recommendSchool']))? trim($_REQUEST['recommendSchool']) : "";		
				
		$ParArr["Description"] = $description;
		$ParArr["ClassLevelID"] = $recommendSchool;
		
		if($Mode == "edit")
			$is_succ = $lelib->editBookRecommend($ParArr);
		else
			$is_succ = $lelib->addBookRecommend($ParArr);
			
		echo $is_succ;			
		break;
	case "vote_review":
		$answer = (isset($_REQUEST['answer'])) ? trim($_REQUEST['answer']) : "";
		$ReviewID = (isset($_REQUEST['ReviewID'])) ? trim($_REQUEST['ReviewID']) : "";
		
		if($answer != "" && $ReviewID != ""){			
			$ParArr["ReviewID"] = $ReviewID;
			$ParArr["Choose"] = $answer;
			$is_succ = $lelib->addBookReviewHelpful($ParArr);
			
			if($is_succ){					
				$reviewCountUI = $lelib->getBookReviewHelpfulCountUI($ReviewID);
				echo $reviewCountUI;
			}		
		}		
		break;
}


######################################################
intranet_closedb();
?>