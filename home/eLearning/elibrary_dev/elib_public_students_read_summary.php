<?php

//using by: Sandy

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

// additional javascript for eLibrary only
include_once("elib_script.php");


intranet_opendb();


$CurrUserID = (isset($_REQUEST['StudentID']))? $_REQUEST['StudentID'] : "";
$field = (isset($_REQUEST['field']))? $_REQUEST['field'] : "";
$order = (isset($_REQUEST['order']))? $_REQUEST['order'] : 0;
$pageNo = (isset($_REQUEST['pageNo']))? $_REQUEST['pageNo'] : 1;
$YearClassID = (isset($_REQUEST['YearClassID']))? $_REQUEST['YearClassID']: "";
$FromMonth = (isset($_REQUEST['FromMonth']))? $_REQUEST['FromMonth'] : "";
$ToMonth = (isset($_REQUEST['ToMonth']))? $_REQUEST['ToMonth']: "";
$ToYear = (isset($_REQUEST['ToYear']))? $_REQUEST['ToYear']: Date("Y");
$FromYear = (isset($_REQUEST['FromYear']))? $_REQUEST['FromYear']: "";


$linterface 	= new interface_html("default3.html");
$lo 	= new libeclass();
$lelib 	= new elibrary();
$libclass = new libclass();
//$ly		= new year_class($YearClassID, false,false,false);
//$ClassName = $ly->ClassTitleEN;

$CurrentPage	= "PageMyeClass";

### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortFieldOrder == "")
$sortFieldOrder = "ASC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$lang = $_SESSION["intranet_session_language"];

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Progress";
$tmpArr[3]["value"] = "ReadTimes";
$tmpArr[4]["value"] = "LastAccessed";


$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]['Progress'];
$tmpArr[3]["text"] = $eLib["html"]['ReadTimes'];
$tmpArr[4]["text"] = $eLib["html"]["last_read"];


$tmpArr[0]["width"] = "*";
$tmpArr[1]["width"] = "20%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "10%";
$tmpArr[4]["width"] = "10%";
$tmpArr[4]["width"] = "15%";

if($sortField == "")
$sortField = $tmpArr[0]["value"];

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;
$inputArr["sortFieldArray"] = $tmpArr;
$inputArr["selectNumField"] = 5;

$sql = "
		SELECT
			b.BookID,	
			b.Title as Title,
			if(a.Author !='' , a.Author , '--') as Author,
			count(h.BookID) as ReadTimes, 
			MAX(h.DateModified) as LastAccessed,
			CONCAT(if(p.Percentage!='' , p.Percentage, 0), '%') as Progress 
		FROM
			INTRANET_ELIB_BOOK_HISTORY as h
		INNER JOIN
			INTRANET_ELIB_BOOK as b
		 ON 
			h.BookID = b.BookID
		LEFT JOIN 
			INTRANET_ELIB_BOOK_AUTHOR as a
		ON
			b.AuthorID = a.AuthorID
		LEFT JOIN 
			INTRANET_ELIB_USER_PROGRESS as p
		ON 
			b.BookID = p.BookID
	 	WHERE 
	 		b.Publish = 1 AND
			b.ContentTLF IS NOT NULL AND
	 		h.UserID = ".$CurrUserID." 
	 	GROUP BY
	 		b.BookID
		";		

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

///////////////////// Selection Box ////////////////////////////////////////////////

#Get Selected Classname
//echo $ClassName;

$returnClassNameArr = $lelib->GET_CLASSNAME();

	$ClassNameArr = array(
				array("-1", $eLib["html"]["all_class"])
				);
			
	//debug_r($returnClassNameArr);
	for($i = 0; $i < count($returnClassNameArr); $i++)
	{
		
		if(trim($returnClassNameArr[$i]["ClassTitleEN"]) != "")
		$tmpClassArr = "";
		$tmpClassArr = array($returnClassNameArr[$i]["YearClassID"], $returnClassNameArr[$i]["ClassTitleEN"]);
		
		$ClassNameArr[$i+1] = $tmpClassArr;
	}

	$fcm = new form_class_manage();
	$ClassSelection= $libclass->getSelectClass( "name='ClassName' id='ClassName' onChange='changeClassName(this);'", $ClassName );
	//$ClassSelection = $linterface->GET_SELECTION_BOX($ClassNameArr, "name='ClassName' id='ClassName' onChange='changeClassName(this);'", "", $YearClassID);

	if($ClassName == "")
	$ClassName = $ClassNameArr[0][0];
	
	$ParArr["ClassName"] = $ClassName;
	$returnStudentArr = $lelib->GET_STUDENT_NAME_BY_CLASS($ParArr);
	
	if($lang == "b5")
	{
		$prefix1 = "ChineseName";
		$prefix2 = "EnglishName";
	}
	else
	{
		$prefix1 = "EnglishName";
		$prefix2 = "ChineseName";	
	}
	
	for($i = 0; $i < count($returnStudentArr); $i++)
	{
		
		if($returnStudentArr[$i][$prefix1] == "")
		$sName = $returnStudentArr[$i][$prefix2];
		else
		$sName = $returnStudentArr[$i][$prefix1];
		
		$tmpStudentArr = "";
		$tmpStudentArr = array($returnStudentArr[$i]["UserID"], $sName);
		$StudentNameArr[$i] = $tmpStudentArr;
	}

	if($StudentID == "")
	$StudentID = $StudentNameArr[0]["UserID"];
	
	
	$studentSelection = $linterface->GET_SELECTION_BOX($StudentNameArr, "name='StudentID' id='StudentID' onChange='changeStudentName(this);'", "", $StudentID);
	
	
	
	
	
	$classStr = $eLib["html"]["class"]." : ";
	
	$SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	$SelectionTable .= "<tr><td><span class=\"tabletext\">".$classStr." ".$ClassSelection.$studentSelection."</span></td></tr>";
	$SelectionTable .= "</table>";
//////////////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--
function changeStudentName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name


function changeClassName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/elib.css" rel="stylesheet" type="text/css">
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">


<form name="form1" action="elib_public_students_read_summary.php" method="post">	

<table width="100%" border="0">
	<tr>
	<td width="172" style="margin:0px padding:0px;" valign="top">
		<!-- Template , Left Navigation panel --> 
		<?php include('elib_left_navigation.php')?>
	</td>
	<td align="center" valign="top">
		<!-- Start of Content -->


		<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

		<!-- head menu -->
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td class="eLibrary_navigation">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
			<a href="index.php"><?=$eLib["html"]["home"]?></a> 
			
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
			<a href="elib_public_class_summary.php"><?=$eLib["html"]["class_summary"]?></a>
					
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
				<a href="elib_public_students_summary.php?YearClassID=<?=$YearClassID?>"><?=$eLib["html"]["student_summary"]?></a>
			
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
			<?=$eLib["html"]["Books_Read"]?>
		</tr>
		</table>
		<!-- end head menu -->
		
		
		<table width="98%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
		<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
		<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
		</tr>
		
		<tr>
		<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>
		
		<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
		<!-- show content -->
		<?=$SelectionTable; ?>
		<?=$contentHtml; ?>
		<!-- end show content -->
		</td>
		
		
		<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
		</tr>
															
		<tr>
		<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
		<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
		<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
		</tr>
		</table>

<!-- End of Content -->
</td></tr></table>


<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

<input type="hidden" name="YearClassID" value="<?=$YearClassID?>" >


</form>



<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
