<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("default3.html");
$CurrentPage	= "PageMyeClass";

$lo 	= new libeclass();

$lelib = new elibrary();
### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$keywords = trim($keywords);
$_SESSION["elib"]["is_search"] = 1;
$_SESSION["elib"]["is_search_select"] = $isSearchSelect;
$_SESSION["elib"]["search_word"] = $keywords;

$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

if($selectSearchType == "0")
$BookTitle = $keywords;

if($selectSearchType == "1")
$Author = $keywords;

if($BookTitle == "" || $BookTitle == null)
$BookTitleStr = "---";

if($DisplayMode == "" || $DisplayMode == null)
$DisplayMode = 1;

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortField == "")
$sortField = "Title";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["BookTitle"] = $BookTitle;
$inputArr["Author"] = $Author;
$inputArr["Source"] = $Source;
$inputArr["Category"] = $selCategory;
$inputArr["Level"] = $selLevel;
$inputArr["startdate"] = $startdate;
$inputArr["enddate"] = $enddate;
//the "With Worksheets" field is disabled (by Adam 2008.11.06)
$checkWorksheets = NULL;
//////////////////////////////////////////////////////////////
$inputArr["checkWorksheets"] = $checkWorksheets;
$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

// for normal search
$inputArr["selectSearchType"] = $selectSearchType;
$inputArr["keywords"] = $keywords;

$inputArr["all_category"] = $eLib["html"]["all_category"];
$inputArr["all_level"] = $eLib["html"]["all_level"];

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

//debug_r($inputArr);
//debug_r($inputArr);
$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Category";
$tmpArr[3]["value"] = "Publisher";
$tmpArr[4]["value"] = "Source";

$tmpArr[0]["text"] = $eLib["html"]["title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]["category"];
$tmpArr[3]["text"] = $eLib["html"]["publisher"];
$tmpArr[4]["text"] = $eLib["html"]["source"];

$tmpArr[0]["width"] = "25%";
$tmpArr[1]["width"] = "15%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "20%";
$tmpArr[4]["width"] = "25%";

$inputArr["sortFieldArray"] = $tmpArr;

$resultArr = $lelib->displayBookDetail($inputArr, $eLib);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

$select_s1 = "";
$select_s2 = "";
$select_s3 = "";
$select_s4 = "";
$select_s5 = "";

if($sortField == "Title")
$select_s1 = "selected";
else if($sortField == "Author")
$select_s2 = "selected";
else if($sortField == "Source")
$select_s3 = "selected";
else if($sortField == "Category")
$select_s4 = "selected";
else if($sortField == "Publisher")
$select_s5 = "selected";

if($DisplayMode == 2)
$sortStr = "";
else if ($DisplayMode == 1)
{
	$sortArr = Array();
	$sortArr[] = array("Title", $eLib["html"]["book_title"]);
	$sortArr[] = array("Author", $eLib["html"]["author"]);
	$sortArr[] = array("Source", $eLib["html"]["source"]);
	$sortArr[] = array("Category", $eLib["html"]["category"]);
	$sortArr[] = array("Publisher", $eLib["html"]["publisher"]);
	
	$tmpSortSelection = $linterface->GET_SELECTION_BOX($sortArr, "name='sortField' id='sortField' onChange='sortFunction(this);'", "", $sortField);
	
$sortStr = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\">";
$sortStr .= "<tr>";
$sortStr .= "<td align=\"left\" valign=\"middle\" class=\"tabletext\">".$eLib["html"]["sort_by"]." :</td>";
$sortStr .= "<td align=\"left\" valign=\"middle\">";
$sortStr .= $tmpSortSelection;
$sortStr .= "</td></tr></table>";
}
$sortSelection = $sortStr;
////////////////////////////////////////////////////////////////////////////

// set view icon
$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$x .= "<td class=\"tabletextremark\">".$eLib["html"]["view"]."</td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 1)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";

$x .= "</a></td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 2)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";

$x .= "</a></td></tr></table>";

$viewChangeSelection = $x;

/////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="center">
	
<table width="100%" border="0" cellspacing="0" cellpadding="2">
<tr>
<td height="320" align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="175" valign="top"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">

<!-- Setting logo -->
<!--
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_01.gif" width="4" height="4"></td>
<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_03.gif" width="4" height="4"></td>
</tr>
<tr>
<td width="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td bgcolor="#d7e9f4"><a href="#" class="menuon" onClick="MM_openBrWindow('elib_setting.php','','scrollbars=yes')"> <img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_manage.gif" width="20" height="20" border="0" align="absmiddle"><?=$eLib["html"]["settings"]?></a></td>
<td width="4" bgcolor="#d7e9f4">&nbsp;</td>
</tr>
<tr>
<td width="4" height="4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_07.gif" width="4" height="4"></td>
<td height="4" bgcolor="#d7e9f4"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="4" height="4"></td>
<td width="4" height="4" ><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/title_setting_09.gif" width="4" height="4"></td>
</tr>
</table>
-->
<!-- End Setting logo -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- Record List Menu -->									
<?=$lelib->displayRecordListMenu($ParArr,$eLib);?>
<!-- End Record List Menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- catalogue Menu -->
<?=$eLib_Catalogue?>
<!-- end catalogue Menu -->
<br></td>
<td align="center" valign="top">
<!-- Start of Content -->
<form name="form1" action="elib_advanced_search_result.php" method="post">

<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>

<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="elib_advanced_search_form.php"><?=$eLib["html"]["advance_search"]?></a>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$eLib["html"]["search_result"]?></td>
</tr>
</table>
<!-- end head menu -->

<!-- result / select -->
<table width="95%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td height="30" align="left" valign="bottom">
<span class="tabletext">
<?=$eLib["html"]["book_title"]?></span> :<span class="eLibrary_commonlink"> &quot;<strong><?=$BookTitle?></strong>&quot;</span> <span class="tabletext">, 
<?=$eLib["html"]["author"]?></span> :<span class="eLibrary_commonlink"> &quot;<strong><?=$Author?></strong>&quot;</span> <span class="tabletext">, 
<?=$eLib["html"]["category"]?></span> :<span class="eLibrary_commonlink"> &quot;<strong><?=$selCategory?></strong>&quot;</span> <span class="tabletext">, 
<?=$eLib["html"]["level"]?></span> :<span class="eLibrary_commonlink"> &quot;<strong><?=$selLevel?></strong>&quot;
</span>
</td>
</tr>
<tr>
<td align="left">
<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr>
<td><?=$sortSelection?></td>
<td align="right">

<!-- view icon -->
<?=$viewChangeSelection?>
<!-- end view icon -->

</td></tr></table>
</td></tr></table>
<!-- end result / select -->

<!-- show content -->
<?=$contentHtml; ?>
<!-- end show content -->

<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >

<?
if($DisplayMode == 2){
?>
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<?
}
?>

<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

<input type="hidden" name="BookTitle" value="<?=$BookTitle?>" >
<input type="hidden" name="Author" value="<?=$Author?>" >
<input type="hidden" name="Source" value="<?=$Source?>" >
<input type="hidden" name="selCategory" value="<?=$selCategory?>" >
<input type="hidden" name="selLevel" value="<?=$selLevel?>" >
<input type="hidden" name="startdate" value="<?=$startdate?>" >
<input type="hidden" name="enddate" value="<?=$enddate?>" >
<input type="hidden" name="checkWorksheets" value="<?=$checkWorksheets?>" >

<input type="hidden" name="selectSearchType" value="<?=$selectSearchType?>" >
<input type="hidden" name="keywords" value="<?=$keywords?>" >

</form>
<!-- End of Content -->
</td></tr></table>
</td></tr></table>
</td></tr></table>

<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
