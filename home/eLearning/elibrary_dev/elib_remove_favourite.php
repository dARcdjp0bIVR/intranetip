<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->HAS_RIGHT())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$ParArr["BookID"] = $BookID;
$ParArr["UserID"] = $_SESSION["UserID"];

$LibeLib->removeMyFavourite($ParArr);

intranet_closedb();

if($returnPath != "" && $returnPath != NULL)
header("Location: $returnPath.php?CurrentPageNum=$CurrentPageNum&DisplayNumPage=$DisplayNumPage&BookID=$BookID");
else
header("Location: elib_record_favourite.php?CurrentPageNum=$CurrentPageNum&DisplayNumPage=$DisplayNumPage");
?>