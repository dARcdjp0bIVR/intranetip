<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->HAS_RIGHT())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$ParArr["BookID"] = $BookID;
$ParArr["UserID"] = $_SESSION["UserID"];
$ParArr["Description"] = $Description;
$ParArr["ClassLevelID"] = $ClassLevelID;

if($Mode == "edit")
$LibeLib->editBookRecommend($ParArr);
else
$LibeLib->addBookRecommend($ParArr);

intranet_closedb();

header("Location: book_detail.php?BookID=$BookID");
?>