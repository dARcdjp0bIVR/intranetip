<?php
header('Content-Type: text/html; charset=utf-8');

// Modifing by 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

// function to get chapter id by page id
function GET_CHAPTERID_BY_PAGEID($PageID="", $PageChapterArr="")
{
	$ChapterID = 0;
	
	for($i = 0; $i < count($PageChapterArr); $i++)
	{
		$TmpPageID = $PageChapterArr[$i]["PAGE"];
		$TmpChapterID = $PageChapterArr[$i]["CID"];
		
		if($PageID >= $TmpPageID)
		$ChapterID = $TmpChapterID;
		else
		break;
	}
	
	return $ChapterID;
}

function GET_IS_CHAPTER_START_BY_PAGEID($PageID="", $PageChapterArr="")
{
	$IsChapter = 0;
	
	for($i = 0; $i < count($PageChapterArr); $i++)
	{
		$TmpPageID = $PageChapterArr[$i]["PAGE"];
		$TmpIsChapter = $PageChapterArr[$i]["ISCHAPTER"];
		
		if($PageID == $TmpPageID)
		$IsChapter = $TmpIsChapter;	
	}
	
	return $IsChapter;
}

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
$book_id = $_POST['BookID'];

// Import book chapter
$xml_file = $HTTP_POST_FILES['InputChapterFile']['tmp_name'];

// To store the page chapter array
$PageChapterArr = array();

$xml_parser = xml_parser_create("UTF-8");
if (!$fp = fopen($xml_file, "r")) 
{
	header("Location: import_book_chapter.php");
}
else
{
	$sql = "	
        			DELETE FROM
						INTRANET_ELIB_BOOK_CHAPTER
					WHERE
						BookID = '".$book_id."' 
		    ";
	$LibeLib->db_db_query($sql);
	
	$xml_contents = fread($fp, filesize($xml_file));
  	fclose($fp);
  	xml_parse_into_struct($xml_parser, $xml_contents, $temp_arr_vals);
  	xml_parser_free($xml_parser);
  
  	foreach($temp_arr_vals as $key=>$value)
  	{
    	if($value['type'] == "complete")
    	{
      		$a_page = $value['attributes']['PAGE'];
			$a_cid = $value['attributes']['CID'];
			$a_sid = $value['attributes']['SID'];
			$a_ischapter = $value['attributes']['ISCHAPTER'];
			$title = htmlspecialchars($value['value'], ENT_QUOTES);
			
			$PageChapterArr[] = $value['attributes'];
      
	      	$sql = "	SELECT 
	      					*
	              		FROM
	                		INTRANET_ELIB_BOOK_CHAPTER
	              		WHERE
	                		BookID = '".$book_id."' AND
	                		ChapterID = '".$a_cid."' AND
	                		SubChapterID = '".$a_sid."'
	              	";
	      	$rs = $LibeLib->returnArray($sql);
	      
	      	if(count($rs) == 0)
	      	{
	        	$sql = "	INSERT INTO 
	        					INTRANET_ELIB_BOOK_CHAPTER 
	                  			(ChapterID,SubChapterID,BookID,Title,OrigPageID,IsChapter,DateModified)
	                		VALUES
	                  			('".$a_cid."','".$a_sid."','".$book_id."','".$title."','".$a_page."','".$a_ischapter."',NOW())
	                  	 ";
	        	$LibeLib->db_db_query($sql);
	      	}
	      	else
	      	{
	        	$sql = "	UPDATE 
	        					INTRANET_ELIB_BOOK_CHAPTER 
	        				SET	
	        					SubChapterID ='".$a_sid."',
	        					Title ='".$title."',
	        					OrigPageID ='".$a_page."',
	        					IsChapter ='".$a_ischapter."',
	        					DateModified=NOW()
		              		WHERE
		                		BookID = '".$book_id."' AND
		                		ChapterID = '".$a_cid."'
	                  	 ";
	        	$LibeLib->db_db_query($sql);		      	
	      	}
    	}
  	}
}

//Import book page
$xml_file = $HTTP_POST_FILES['InputOutputFile']['tmp_name'];

$xml_parser = xml_parser_create("UTF-8");
if (!$fp = fopen($xml_file, "r")) {
	header("Location: import_book_chapter.php");
}
else
{
	$xml_contents = fread($fp, filesize($xml_file));
	fclose($fp);
  
	$mod_xml_contents = preg_replace("/(\<img)(.*)(alt=\"Image\"\>\<\/img\>)/","[img]\\2[/img]",$xml_contents);
	//$mod_xml_contents = preg_replace("/(\<pdf)(.*)(\>\<\/pdf\>)/","[pdf]\\2[/pdf]",$xml_contents);
	xml_parse_into_struct($xml_parser, $mod_xml_contents, $temp_arr_vals);
	xml_parser_free($xml_parser);
  
	//echo "<pre> number of temp_arr_vals is ".count($temp_arr_vals)."</pre>";
	for($i=0; $i<count($temp_arr_vals); $i++)
	{
		//echo "<pre> temp_arr_vals[i][tag] is ".$temp_arr_vals[$i]['tag']."</pre>";
  		if($temp_arr_vals[$i]['tag'] == "A") 
  		{
  			$tag_A_index[] = $i;
		}
	}
	$tag_A_index[] = count($temp_arr_vals)-1;
  
	//$sql = "	
    //    			DELETE FROM
	//					INTRANET_ELIB_BOOK_PAGE
	//				WHERE
	//					BookID = '".$book_id."' 
	//	    ";
	//$LibeLib->db_db_query($sql);	
	
	$offset_pos = 0;
	$a_pageid = 0;
	
	//echo "<pre> number of A tag is ".count($tag_A_index)."</pre>";
	for($i=0; $i<count($tag_A_index)-1; $i++)
	{
    	for($j=$tag_A_index[$i]; $j<$tag_A_index[$i+1]; $j++)
    	{
			$tag = $temp_arr_vals[$j];
			
			if($tag['type'] == "complete")
			{
				if($tag['tag'] == "P")
				{
					$tag['value'] = preg_replace("/\[img\](.*)\[\/img\]/", "<img\\1alt=\"Image\"></img>", $tag['value']);
					//$tag['value'] = preg_replace("/\[pdf\](.*)\[\/pdf\]/", "<pdf\\1></pdf>", $tag['value']);
					$p_value .= $tag['value']."\n";
				}
				else
				{
					$start = strpos($xml_contents,"</a>",$offset_pos) + strlen("</a>");
					$end = strpos($xml_contents,"<a",$start);
					if($end === false)
						$a_value = substr($xml_contents,$start);
					else
						$a_value = substr($xml_contents,$start,$end-$start);
			
					//$a_cid = $tag['attributes']['CID'];
					//$a_ischapterstart = $tag['attributes']['ISCHAPTERSTART'];
					$a_pageid++;
					
					$a_cid = GET_CHAPTERID_BY_PAGEID($a_pageid, $PageChapterArr);
					$a_ischapterstart = GET_IS_CHAPTER_START_BY_PAGEID($a_pageid, $PageChapterArr);
					
					//$a_page_type = strpos($a_pageid, "pre") === false?"page":"pre";
					$a_page_type = strpos($tag['attributes']['ID'], "pre") === false?"page":"pre";
					$a_o_pageid = substr($tag['attributes']['ID'],(strpos($tag['attributes']['ID'], "_")+1));
					
					if($tag['attributes']['PDFID']){
						$a_pdfid = (int)$tag['attributes']['PDFID'];
					}else{
						$a_pdfid = -1;	
					}
								
					$p_value = "";
					$offset_pos = $end;
				}
			}
  		}
    
		$sql = "	
            SELECT 
  						*
  					FROM
  						INTRANET_ELIB_BOOK_PAGE
  					WHERE
  						BookID = '".$book_id."' AND
  						PageID = '".$a_pageid."'
  		    ";
		$rs = $LibeLib->returnArray($sql);
	  	if(count($rs) == 0)
	  	{
		  		//echo "<pre>INSERT INTO INTRANET_ELIB_BOOK_PAGE</pre>";
		  		//echo "<pre>PageID is ".$a_pageid."</pre>";
	    		$sql = "	INSERT INTO
	              			INTRANET_ELIB_BOOK_PAGE
	                			(BookID,ChapterID,IsChapterStart,PageID,PageType,OrigPageID,PdfID,Content,ContentXML,DateModified)
	            			VALUES
	              			('".$book_id."','".$a_cid."','".$a_ischapterstart."','".$a_pageid."','".$a_page_type."','".$a_o_pageid."','".$a_pdfid."','".addslashes($p_value)."','".addslashes($a_value)."',NOW())
	           ";
	    		$LibeLib->db_db_query($sql);
	  	}
	  	else
	  	{
			  	//echo "<pre>UPDATE INTRANET_ELIB_BOOK_PAGE</pre>";
		  		//echo "<pre>PageID is ".$a_pageid."</pre>";
	    		$sql = "	UPDATE
	              				INTRANET_ELIB_BOOK_PAGE
	              			SET
	              				ChapterID='".$a_cid."',
	              				IsChapterStart='".$a_ischapterstart."',
	              				PageID='".$a_pageid."',
	              				PageType='".$a_page_type."',
	              				OrigPageID='".$a_o_pageid."',
	              				PdfID = '".$a_pdfid."',
	              				Content='".addslashes($p_value)."',
	              				ContentXML='".addslashes($a_value)."',
	              				DateModified=NOW()
		  					WHERE
		  						BookID = '".$book_id."' AND
		  						PageID = '".$a_pageid."'
	           ";
	    		$LibeLib->db_db_query($sql);		  	
	  	}
	  	
//echo $sql."<br />";	  	
	}

	//Import book paragraph
	$chapter_id = 0;
	$sc_id = 0;
	$i = -1;
	
	$fp = fopen($xml_file, "r");
	while (!feof($fp)) {
    $buffer = fgets($fp);
    $pos = strpos($buffer, "chapter_title");
    if(!($pos === false))
      $i++;
    if($i >= 0)
      $scx_arr[$i] .= $buffer; 
  }
  fclose($fp);
	
	for($i=0; $i<count($temp_arr_vals); $i++)
	{
		if($temp_arr_vals[$i]['tag'] == "P")
		{
			$pos = strpos($temp_arr_vals[$i]['attributes']['CLASS'], "chapter_title");
	
			if(!($pos === false))
				$sub_chapter_index[] = $i;
		}
	}
	$sub_chapter_index[] = count($temp_arr_vals)-1;
 
	for($i=0; $i<count($sub_chapter_index)-1; $i++)
	{
    if($temp_arr_vals[$sub_chapter_index[$i]]['attributes']['CLASS'] == "sub_chapter_title")
      $sc_id++;
    else if($temp_arr_vals[$sub_chapter_index[$i]]['attributes']['CLASS'] == "chapter_title")
    {
      $sc_id = 0;
      $chapter_id++;
    }
   
		for($j=$sub_chapter_index[$i]; $j<$sub_chapter_index[$i+1]; $j++)
		{
			$tag = $temp_arr_vals[$j];
		
			if($tag['tag'] == "P")
			{      
				$tag['value'] = preg_replace("/\[img\](.*)\[\/img\]/", "<img\\1alt=\"Image\"></img>", $tag['value']);
				//$tag['value'] = preg_replace("/\[pdf\](.*)\[\/pdf\]/", "<pdf\\1></pdf>", $tag['value']);
          
        if($sc_id != 0)
          $sc_value[$chapter_id][$sc_id] .= $tag['value']."\n";
        $sc_value[$chapter_id][0] .= $tag['value']."\n";
			}
		}
    
    if($sc_id != 0)
      $scx_value[$chapter_id][$sc_id] = $scx_arr[$i];
    $scx_value[$chapter_id][0] .= $scx_arr[$i];
	}
  
  $p_id = 0;
  for($i=0; $i<count($sc_value); $i++)
  {
    for($j=0; $j<count($sc_value[$i]); $j++)
    {
      $sql = "SELECT *
              FROM
                INTRANET_ELIB_BOOK_PARAGRAPH
              WHERE
                BookID = '".$book_id."' AND
                ParagraphID = '".$p_id."'
              ";
      $rs = $LibeLib->returnArray($sql);
      if(count($rs) == 0)
      {
        $sql = "INSERT INTO
                  INTRANET_ELIB_BOOK_PARAGRAPH
                    (BookID,ChapterID,ParagraphID,Content,ContentXML,DateModified)
                VALUES
                  ('".$book_id."','".$chapter_id."','".$p_id."','".addslashes($sc_value[$i][$j])."','".addslashes($scx_value[$i][$j])."',NOW())
               ";
    
        $LibeLib->db_db_query($sql);
        $p_id++;
      }
      else
      {
	    		$sql = "	UPDATE
	              				INTRANET_ELIB_BOOK_PARAGRAPH
	              			SET
	              				ChapterID='".$chapter_id."',
	              				Content='".addslashes($sc_value[$i][$j])."',
	              				ContentXML='".addslashes($scx_value[$i][$j])."',
	              				DateModified=NOW()
			              WHERE
								BookID = '".$book_id."' AND
			                	ParagraphID = '".$p_id."'
	           ";
	    		$LibeLib->db_db_query($sql);      
    	}
    }      
  }	
}

// Unzip file
$LibFS = new libfilesystem();
$Location1 = $intranet_root."/file/elibrary";
if (!is_dir($Location1))
{
	$LibFS->folder_new($Location1);
}
$Location2 = $intranet_root."/file/elibrary/tmp";
if (!is_dir($Location2))
{
	$LibFS->folder_new($Location2);
}

$Location3 = $intranet_root."/file/elibrary/content";
if (!is_dir($Location3))
{
	$LibFS->folder_new($Location3);
}
$Location4 = $intranet_root."/file/elibrary/content/".$book_id;
if (!is_dir($Location4))
{
	$LibFS->folder_new($Location4);
}
else
{
	if($book_id){
		@$LibFS->folder_remove_recursive($Location4);
	}	
	$LibFS->folder_new($Location4);
}

$File = $Location2."/".mktime().".zip";
if (move_uploaded_file($HTTP_POST_FILES['InputZipFile']['tmp_name'],$File))
{	
	$LibFS->file_unzip($File, $Location4);
	@$LibFS->chmod_R($Location2,0777);
	@$LibFS->chmod_R($Location4,0777);
	$LibFS->item_remove($File);
}

intranet_closedb();
header("Location: import_book_chapter.php?BookID=$book_id");

?>
