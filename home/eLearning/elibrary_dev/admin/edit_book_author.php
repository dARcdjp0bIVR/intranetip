<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentManage";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
### Title ###
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];
if ($BookID=="")
{
	$BookID=1;
}
$BookArr = $LibeLib->GET_BOOK_DETAILS($BookID);
if ($BookArr["Title"] != "")
{
	//$BookTitle = " (".iconv("UTF-8", "big5"."//IGNORE", $BookArr["Title"]).")";
	$BookTitle = " (".$BookArr["Title"].")";
}
$TAGS_OBJ[] = array($eLib['ManageBook']["ContentManage"].$BookTitle,"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();


$AuthorArr = $LibeLib->GET_AUTHOR_DETAILS($AuthorID);

$Author = $AuthorArr[0]["Author"];
//$Author = iconv("UTF-8", "big5"."//IGNORE", $Author);

$AuthorDesc = $AuthorArr[0]["Description"];
//$AuthorDesc = iconv("UTF-8", "big5"."//IGNORE", $AuthorDesc);



?>

<table width="95%"  height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="100%"  height="100%" >
	<iframe frameborder="0" name="content_frame" id="content_frame" scrolling="no" width="100%" 
	height="100%" src="edit_book_author_fcontent.php?AuthorID=<?=$AuthorID?>&BookID=<?=$BookID?>" marginwidth="0" marginheight="0" ></iframe>	
	</td>
</tr>	
</table>

<?php
intranet_closedb();

//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
$linterface->LAYOUT_STOP();

?>