<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageImportBook";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if ($BookID=="")
{
	$BookID=1;
}

$BookArr = $LibeLib->GET_BOOK_DETAILS($BookID);
$AuthorID = $BookArr["AuthorID"];   
$AuthorArr = $LibeLib->GET_AUTHOR_DETAILS($AuthorID);

$Author = $AuthorArr[0]["Author"];
$AuthorDesc = $AuthorArr[0]["Description"];
$InputBy = $BookArr["InputBy"];

if($BookArr["Publish"] == 1)
{
	$Publish1 = "checked";
	$Publish2 = "";
}
else
{
	$Publish1 = "";
	$Publish2 = "checked";
}


$SourceFrom = $BookArr["Source"];
$SourceArr = array();
if (is_array($eLib['Source']) && count($eLib['Source'])>0)
{
foreach($eLib['Source'] as $Key => $Value)	
{
	$SourceArr[] = array($Key,$Value);
}
}
$SourceSelect = $linterface->GET_SELECTION_BOX($SourceArr," name='SourceFrom' ","", $SourceFrom);

$Language = $BookArr["Language"];   
$LangArr = array();
$LangArr[] = array("chi",$i_QB_LangSelectChinese);
$LangArr[] = array("eng",$i_QB_LangSelectEnglish);
$LangSelect = $linterface->GET_SELECTION_BOX($LangArr," name='Language' ","", $Language);

$AdultContent = $BookArr["AdultContent"];
if($AdultContent=="" || $AdultContent=="NULL"){
	$AdultContent = "No"; 
}	   
$AdultContentArr = array();
$AdultContentArr[] = array("Yes","Yes");
$AdultContentArr[] = array("No","No");
$AdultContentSelect = $linterface->GET_SELECTION_BOX($AdultContentArr," name='AdultContent' ","", $AdultContent);


?>
<META http-equiv=Content-Type content='text/html; charset=UTF-8'>
<link href="/templates/style_mail_utf8.css" rel="stylesheet" type="text/css"  />
<link href="/templates/2009a/css/content_utf8.css" rel="stylesheet" type="text/css" />

<style type="text/css" >
html, body
{
	background-color:#FFFFFF;
	scrollbar-face-color:#F7F7F7;
	scrollbar-base-color:#F7F7F7;
	scrollbar-arrow-color:888888;
	scrollbar-track-color:#E2DFDF;
	scrollbar-shadow-color:#F7F7F7;
	scrollbar-highlight-color:#F7F7F7;
	scrollbar-3dlight-color:#C6C6C6;
	scrollbar-darkshadow-Color:#C6C6C6;
	background-image: url("");
	background-repeat: no-repeat;
	background-position: left top;
}
</style>

<script language="JavaScript" type="text/JavaScript">
<!--
function checkInputFrom(obj)
{
	if(document.form1.Title.value == "")
	{
		alert("<?=$eLib["html"]["please_enter_title"]?>");
		return false;
	}
	
	if(document.form1.Author.value == "")
	{
		alert("<?=$eLib["html"]["please_enter_author"]?>");
		return false;
	}
	
	return true;
}
//-->
</script>

<base target="_parent" >
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff" >

<form name="form1" method="post" action="edit_book_info_update.php" onSubmit="return checkInputFrom(this);">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
	<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr> 
		<td><br />
		
		<table align="center" width="80%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Title"]?></span></td>
			<td>
			<input type="text" name="Title" value="<?=$BookArr['Title']?>" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Author"]?></span></td>
			<td>
			<input type="text" name="Author" value="<?=$BookArr['Author']?>" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["SeriesEditor"]?></span></td>
			<td>
			<input type="text" name="SeriesEditor" value="<?=$BookArr['SeriesEditor']?>" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Category"]?></span></td>
			<td>
			<input type="text" name="Category" value="<?=$BookArr['Category']?>" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Subcategory"]?></span></td>
			<td>
			<input type="text" name="Subcategory" value="<?=$BookArr['SubCategory']?>" />	
			</td>
		</tr>	
				
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Level"]?></span></td>
			<td>
			<input type="text" name="Level" value="<?=$BookArr['Level']?>" />	
			</td>
		</tr>	

		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["AdultContent"]?></span></td>
			<td>
			<?=$AdultContentSelect?>
			</td>
		</tr>			
						
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Publisher"]?></span></td>
			<td>
			<input type="text" name="Publisher" value="<?=$BookArr['Publisher']?>" />	
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib["SourceFrom"]?></span></td>
			<td>
			<?=$SourceSelect?>
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Language"]?></span></td>
			<td>
			<?=$LangSelect?>
			</td>
		</tr>	
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Description"]?></span></td>
			<td>	
			<?=$linterface->GET_TEXTAREA("Preface", $BookArr['Preface'], 100, 8)?>
			</td>
		</tr>	
		
		<?
			if($SourceFrom == "cup")
			{ 
				$TmpCpy = $BookArr['Copyright'];
				if($TmpCpy == "")
				{
					$TmpCpy = $LibeLib->GET_COPYRIGHT_TEMPLATE("cup");
					$JS1 = $JS2 = $JS3 = $JS4 = $JS5 = "";
				}
				else
				{
					// find the varibale values from the content
					
					$pos1 = strpos($TmpCpy, "<b>[JS1]</b>");
					$pos2 = strpos($TmpCpy, "<b>[/JS1]</b>");
					$JS1 = substr($TmpCpy, $pos1+12, $pos2 - $pos1 - 12);
					//echo $pos1."---".$pos2;
					//debug_r($JS1);
					$pos1 = strpos($TmpCpy, "<b>[JS2]</b>");
					$pos2 = strpos($TmpCpy, "<b>[/JS2]</b>");
					$JS2 = substr($TmpCpy, $pos1+12, $pos2 - $pos1 - 12);
					
					$pos1 = strpos($TmpCpy, "<b>[JS3]</b>");
					$pos2 = strpos($TmpCpy, "<b>[/JS3]</b>");
					$JS3 = substr($TmpCpy, $pos1+12, $pos2 - $pos1 - 12);
					
					$pos1 = strpos($TmpCpy, "<b>[JS4]</b>");
					$pos2 = strpos($TmpCpy, "<b>[/JS4]</b>");
					$JS4 = substr($TmpCpy, $pos1+12, $pos2 - $pos1 - 12);
					
					$pos1 = strpos($TmpCpy, "<b>[JS5]</b>");
					$pos2 = strpos($TmpCpy, "<b>[/JS5]</b>");
					$JS5 = substr($TmpCpy, $pos1+12, $pos2 - $pos1 - 12);
					
				}
		?>
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib['Book']["Copyright"]?></span></td>
			<td>	
			<!--<?=$linterface->GET_TEXTAREA("Copyright", $BookArr['Copyright'], 100, 8)?>-->
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr><td><?=$TmpCpy?></td></tr>
			</table>
			
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr><td>[JS1]: </td><td><input type="text" value="<?=$JS1?>" name="JS1"/></td></tr>
			<tr><td>[JS2]: </td><td><input type="text" value="<?=$JS2?>" name="JS2"/></td></tr>
			<tr><td>[JS3]: </td><td><input type="text" value="<?=$JS3?>" name="JS3"/></td></tr>
			<tr><td>[JS4]: </td><td><input type="text" value="<?=$JS4?>" name="JS4"/></td></tr>
			<tr><td>[JS5]: </td><td><input type="text" value="<?=$JS5?>" name="JS5"/></td></tr>
			</table>
			</td>
		</tr>	
		<?
			}
		?>
		
		<tr>
			<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'>
			<span class='tabletext'><?=$eLib["html"]["publish"]?></span></td>
			<td>		
			<input type="radio" id="Publish" name="Publish" value="1" <?=$Publish1?> /> <?=$eLib["html"]["publish"]?> 
			<input type="radio" id="Publish" name="Publish" value="0" <?=$Publish2?> /> <?=$eLib["html"]["unpublish"]?>	
			</td>
		</tr>	

		</table>
		
		</td>
	</tr>
	</table>
	</td>
</tr>


<tr>
	<td colspan="2">        
	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
    <tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button","parent.location='index.php'") ?>
		</td>
	</tr>
	</table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="BookID" id="BookID" value="<?=$BookID?>" />
<input type="hidden" name="InputBy" id="InputBy" value="<?=$InputBy?>" />

</form>

</body>
<?php
	//echo $linterface->FOCUS_ON_LOAD("form1.InputFile");
?>
