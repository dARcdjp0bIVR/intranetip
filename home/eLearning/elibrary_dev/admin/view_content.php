<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageContentView";

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}
if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

### Title ###
//$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$ip20TopMenu['eLibrary'];
$TAGS_OBJ[] = array($eLib['ManageBook']["ImportBook"],"");
$MODULE_OBJ = $LibeLib->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$BookArr = $LibeLib->GET_BOOK_DETAILS($BookID);
$BookPageArr = $LibeLib->GET_BOOK_PAGE_TYPE_ARRAY($BookID);
$PrePageNum = ($BookPageArr["pre"] != "") ? $BookPageArr["pre"] : 0; 
$PageNum = ($BookPageArr["page"] != "") ? $BookPageArr["page"] : 0; 

if ($pageid == "")
{
	$pageid = 0;
}

if($pageid > 0)
{
  $sql = "SELECT *
          FROM
            INTRANET_ELIB_BOOK_PAGE
          WHERE
            BookID = '".$BookID."' AND
            PageID = '".$pageid."'
          ";
  $BookContent = $LibeLib->returnArray($sql);
}
else
{
	$sql = "	SELECT 
					Title, OrigPageID, IsChapter
				FROM
					INTRANET_ELIB_BOOK_CHAPTER
				WHERE
					BookID = '".$BookID."'
	";
	$ContentPage = $LibeLib->returnArray($sql);
 
}

$MinPage = 0;
$MaxPage = $PrePageNum + $PageNum;

if ($CurrentMode=="1")
{
	$CurrentLink = "edit_content_fcontent.php";
}
else
{
	$CurrentLink = "view_content_fcontent.php";	
}

?>

<table width="95%"  height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td width="100%"  height="100%" >
	<iframe frameborder="0" name="content_frame" id="content_frame" scrolling="no" width="100%" 
	height="100%" src="<?=$CurrentLink?>?BookID=<?=$BookID?>&pageid=<?=$pageid?>" marginwidth="0" marginheight="0" ></iframe>	
	</td>
</tr>	
</table>


<?php
intranet_closedb();

$linterface->LAYOUT_STOP();

?>
