<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->IS_ADMIN_USER($UserID))
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'] || !$plugin['eLib_Book_Management'])
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$InputArray["BookID"] = $BookID;
$InputArray["Title"] = $Title;
$InputArray["Author"] = $Author;
$InputArray["Category"] = $Category;
$InputArray["Level"] = $Level;
$InputArray["AdultContent"] = $AdultContent;
$InputArray["Publisher"] = $Publisher;
$InputArray["Category"] = $Category;
$InputArray["SubCategory"] = $SubCategory;
$InputArray["Source"] = $SourceFrom;
$InputArray["Language"] = $Language;
$InputArray["Preface"] = $Preface;
$InputArray["InputBy"] = $InputBy;
$InputArray["SeriesEditor"] = $SeriesEditor;
$InputArray["Publish"] = $Publish;
$InputArray["Copyright"] = "";

if($SourceFrom == "cup")
{
// update the copyright format
$CopyrightTemplate = $LibeLib->GET_COPYRIGHT_TEMPLATE("cup");
$Copyright = $CopyrightTemplate;

if($JS1 != "") $Copyright = str_replace("<b>[JS1]</b><b>[/JS1]</b>",  "<b>[JS1]</b>".$JS1."<b>[/JS1]</b>", $Copyright);
if($JS2 != "") $Copyright = str_replace("<b>[JS2]</b><b>[/JS2]</b>",  "<b>[JS2]</b>".$JS2."<b>[/JS2]</b>", $Copyright);
if($JS3 != "") $Copyright = str_replace("<b>[JS3]</b><b>[/JS3]</b>",  "<b>[JS3]</b>".$JS3."<b>[/JS3]</b>", $Copyright);
if($JS4 != "") $Copyright = str_replace("<b>[JS4]</b><b>[/JS4]</b>",  "<b>[JS4]</b>".$JS4."<b>[/JS4]</b>", $Copyright);
if($JS5 != "") $Copyright = str_replace("<b>[JS5]</b><b>[/JS5]</b>",  "<b>[JS5]</b>".$JS5."<b>[/JS5]</b>", $Copyright);

$InputArray["Copyright"] = $Copyright;
}

$LibeLib->ADD_BOOK_INFO($InputArray);

//debug_r($InputArray);

intranet_closedb();

header("Location: index.php");

?>