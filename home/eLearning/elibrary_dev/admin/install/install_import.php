<?php


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");


intranet_opendb();
###########################################

## Init Library
$LibeLib = new elibrary();
$linterface 	= new interface_html("popup5.html");

## Get Post Variables
$type = isset($_REQUEST['type'])? strtolower(trim($_REQUEST['type'])) : "";

if($type == ""){
	echo("Import method not selected. Please close popup and try again.");
	die();
}

if($type =="status"){
	$heading = "Status";
	$form_action = "install_status_import_update.php";
}else{
	## quota
	$heading = "Quota";
	$form_action = "install_quota_import_update.php";
	
}

$sample_import = GET_CSV("Sample_".$heading."_Import.csv");

$linterface->LAYOUT_START();
?>
<script>

	function submit_import(){
		if("Are you sure you want to import?"){
			document.form1.submit();
		}		
	}
</script>
<form name="form1" id="form1" Method="post" action="<?=$form_action?>" enctype="multipart/form-data" style="margin:0px;padding="0px" >
<div style="height:480px;">
	<div style="height:30px;padding:0 0 0 10px;">
	<h3>Import Book <?=$heading?></h3><BR />
	</div>

	<div id="display_content" style="height:400px;padding:0 0 0 10px;overflow-y:auto;">
	<table width="100%" border="1">
		<tr style="font-weight:bold;" height="30px">
			<td width="100">Import File :</td>
			<td width="100"><input type="file" name="csvfile" id="csvfile" size="50" /></td>			
		</tr>
		<tr style="font-weight:bold;" height="30px">
			<td width="100" >Sample :</td>
			<td width="100"><a href="<?=$sample_import?>" target="_blank">Sample_<?=$heading?>_Import.csv</a></td>			
		</tr>		
	</table>
	</div>
	<div id="btn_panel" style="height:40px;padding:0 0 5px 10px;">
	<input type="button" name="close_booklist" id="close_booklist" value="Confirm Import" onClick="submit_import();"/>
		<input type="button" name="close_booklist" id="close_booklist" value="Close" onClick="window.parent.tb_remove();"/>
	</div>
</div>
</form

<?php
$linterface->LAYOUT_STOP();

###########################################
intranet_closedb();

?>