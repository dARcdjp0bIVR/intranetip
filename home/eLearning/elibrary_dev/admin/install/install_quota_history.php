<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");


include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_opendb();
###########################################

$LibeLib = new elibrary();

$BookID = isset($_REQUEST['BookID'])? trim($_REQUEST['BookID']) : "";

if($BookID ==""){
	echo "No BookID given";
	die();
}


$sql = "SELECT 
			SUBSTRING((BookID+100000000), 2) as code, 
			BookID,
			NumberOfCopy,
			InputDate
		FROM 
			INTRANET_ELIB_BOOK_LICENSE 
		WHERE 
			BookID = ".$BookID." 
		ORDER BY 
			InputDate";

$aryRs = $LibeLib->returnArray($sql);

$sql = "SELECT 
			SUM(bl.NumberOfCopy), b.Title  
		FROM 
			INTRANET_ELIB_BOOK_LICENSE as bl
		LEFT JOIN 
			INTRANET_ELIB_BOOK as b
		ON
			bl.BookID = b.BookID 
		WHERE 
			bl.BookID = ".$BookID."
		GROUP BY
			bl.BookID";
			
			
$aryBookInfo = $LibeLib->returnArray($sql);

$aryFinalQuota = $aryBookInfo[0][0];
$BookTitle = $aryBookInfo[0][1];

####################################################

?>
<div style="height:480px;">
	<div style="height:30px;padding:0 0 0 10px;">
	<h3>Book : <?=$BookTitle?></h3><BR />
	</div>

	<div id="display_content" style="height:400px;padding:0 0 0 10px;overflow-y:auto;">
	<table width="100%" border="1">
		<tr style="font-weight:bold;" height="50px">
			<td width="100">Code</td>
			<td width="100">BookID</td>
			<td width="100">Quota</td>
			<td width="*">InputDate</td>
		</tr>
		<?php
		$rsCount = count($aryRs);
		 
		for($i=0; $i<$rsCount; $i++){
		?>
			<tr>
				<td><?=$aryRs[$i]['code']?></td>
				<td><?=$aryRs[$i]['BookID']?></td>
				<td><?=$aryRs[$i]['NumberOfCopy']?></td>
				<td><?=$aryRs[$i]['InputDate']?></td>
			</tr>
		<?php
		}
		?>		
		<tr height="100">
			<td colspan="4" align="right" valign="bottom"><b>Enabled Quota : <?=$aryFinalQuota?></b></td>
		</tr>
	</table>
	</div>
	<div id="btn_panel" style="height:40px;padding:0 0 5px 10px;">
		<input type="button" name="close_booklist" id="close_booklist" value="Close" onClick="window.parent.tb_remove();"/>
	</div>
</div>
<?php

###########################################
intranet_closedb();
?>