<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_install.php");

intranet_opendb();
###########################################
#Init Variables
$aryFail = array();
$arySucc = array();

## Init Library
$objElib = new elibrary_install();

## Get POST Variables
$action 		= (isset($_REQUEST['action']))? trim($_REQUEST['action']) : "";

//debug_r($_REQUEST);

switch($action){
	## Enable / Disable book(s) SITE license
	case "update_site_book_status":
	
		$strBookID = (isset($_REQUEST['strBookID']))? trim($_REQUEST['strBookID']) :  "";
		$enable = (isset($_POST['enable']))? trim($_POST['enable']) :  "";
		
				
		if(!empty($strBookID)){
						
			$aryBookID = explode(",", $strBookID);
			if(is_array($aryBookID) && $aryBookID != array()){
				
				if($enable == 1){
					foreach($aryBookID as $b=> $BookID){						
						if($objElib->enable_site_book_license($BookID, $UserID)){							
							array_push($arySucc, str_pad($BookID, 8, "0", STR_PAD_LEFT));
						}else{						
							array_push($aryFail, str_pad($BookID, 8, "0", STR_PAD_LEFT));
						}
					}
				}else{
					foreach($aryBookID as $b=> $BookID){
						if($objElib->disable_site_book_license($BookID, $UserID)){							
							array_push($arySucc, str_pad($BookID, 8, "0", STR_PAD_LEFT));
						}else{						
							array_push($aryFail, str_pad($BookID, 8, "0", STR_PAD_LEFT));
						}
					}
				}
				
				## Echo succ & fail results
				echo $objElib->gen_result_ui($arySucc, $aryFail);							
			}			
		}
		break;
	
	## Update the quota for STUDENT book(s)
	case "update_student_book_quota":
		$aryBookQuota = (isset($_REQUEST['aryBookQuota']))? $_REQUEST['aryBookQuota'] :  "";
		$aryBookID = (isset($_REQUEST['aryBookID']))? $_REQUEST['aryBookID'] :  "";
		
		debug_r($aryBookQuota);
		debug_r($aryBookID);
		foreach($aryBookID as $i => $BookID){			
			if($objElib->add_book_license_quota($BookID, $aryBookQuota[$i])){
				array_push($arySucc, str_pad($BookID, 8, "0", STR_PAD_LEFT));
			}else{										
				array_push($aryFail, str_pad($BookID, 8, "0", STR_PAD_LEFT));
			}
		}
		
		## Echo succ & fail results
				echo $objElib->gen_result_ui($arySucc, $aryFail);			
		
		break;	
		
	default:
		break;
}


###########################################
intranet_closedb();
?>