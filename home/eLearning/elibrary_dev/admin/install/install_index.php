<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


## Prompt login
//include_once("../../../check_admin.php");


intranet_opendb();
###########################################

$linterface 	= new interface_html();
$lu = new libuser($UserID);
$LibeLib = new elibrary();

$plugin['eLib_license'] = 1;

$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = "Install License";
$TAGS_OBJ[] = array($plugin['eLib_license'] == 1? "Install Site License" : "Install Student Book License");


//IF (b.Publish='1','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."'),

if($plugin['eLib_license'] == 1){
	## Site License
$Sql = "
			SELECT
				SUBSTRING((b.BookID+100000000), 2), 
				CONCAT('<a href=\"import_book_chapter.php?BookID=',b.BookID,' \" class=\"tablelink\">',b.Title,'</a>'),
				CONCAT('<a href=\"edit_book_author.php?BookID=',b.BookID,'&AuthorID=',AuthorID,' \" class=\"tablelink\">',b.Author,'</a>'),
				b.Publisher,
				IF (b.Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."') ,
				IF (b.Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."'),
				b.DateModified,
				IF (bl.BookID !='','"."<b>Y</b>"."', '"."<font color=gray>N</font>"."') as enabled,
				CONCAT('<input type=\"checkbox\" name=\"BookID[]\" value=\"',b.BookID,'\" />')					
			FROM
				INTRANET_ELIB_BOOK as b
			LEFT JOIN 
				INTRANET_ELIB_BOOK_ENABLED as bl
			ON
				b.BookID = bl.BookID
		  ";
}else{
	## Student License
	$Sql = "
			SELECT
				SUBSTRING((b.BookID+100000000), 2), 
				b.Title,
				b.Author,
				b.Publisher,
				IF (b.Source='green','".$eLib['Source']["green"]."', '".$eLib['Source']["cup"]."') ,
				IF (b.Language='eng','".$i_QB_LangSelectEnglish."', '".$i_QB_LangSelectChinese."'),
				b.DateModified,
				IF (bl.BookID !='', CONCAT('<a href=\"install_quota_history.php?BookID=', bl.BookID,'&TB_iframe=true&amp;height=500&amp;width=600\" class=\"thickbox\">',SUM( bl.NumberOfCopy), '</a>') , '"."<font color=gray>Disabled</font>"."') as enabled,						
				CONCAT('<input type=\"checkbox\" name=\"BookID[]\" id=\"BookID_',b.BookID,'\" value=\"',b.BookID,'\" />')					
			FROM
				INTRANET_ELIB_BOOK as b
			LEFT JOIN 
				INTRANET_ELIB_BOOK_LICENSE as bl
			ON
				b.BookID = bl.BookID
			GROUP BY 
			    b.BookID
		  ";
echo $sql;	
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 1;
$order = ($order == '' || $order != 0) ? 1 : 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("BookID", "Title", "Author", "Publisher", "Source", "Language", "DateModified", "enabled");


$li->sql = $Sql;//$LibeLib->GET_BOOK_OVERVIEW_SQL($TmpArr);
//echo htmlspecialchars($li->sql);
$li->IsColOff = "2";
$li->no_col = 10;
$li->column_array = array(12,12,12,12,12,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Code"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Title"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Author"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["Publisher"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib["SourceFrom"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_QB_LangSelect)."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $eLib['Book']["DateModified"])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $plugin['eLib_license']==1? $eLib["admin"]["Enable"] : $eLib['admin']['Enabled_Quota'])."</td>\n";
$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("BookID[]")."</td>\n
";
####################################################
$rootPath = "../../";
echo $rootPath[strlen($rootPath)-1];
echo strlen($rootPath);
echo "111";

$linterface->LAYOUT_START();

?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>

<BR />
<BR />
<form name="form1" >
<table border="1" cellspacing="0" cellpadding="0">
<tr>
	<td align="left">
		<?php if($plugin['eLib_license'] == 1){ ?>
			<a href="install_import.php?type=status&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox">Import Status</a>
		<?php }else{?>
			<a href="install_import.php?type=quota&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox">Import Quota</a>
		<?php }?>
	</td>
	<td align="right">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
			<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td nowrap>
					<?php if($plugin['eLib_license'] == 1){ ?>
					<!-- SITE LICENSE -->
						<a href="install_status_change.php?enable=1&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox tabletool">
						Enable</a>&nbsp;&nbsp;
						<a href="install_status_change.php?enable=0&TB_iframe=true&amp;height=500&amp;width=600" class="thickbox tabletool">
						Disable</a>
					<?php }else{?>
					<!-- STUDENT LICENSE -->
						<a href="install_quota.php?TB_iframe=true&amp;height=500&amp;width=600" class="thickbox tabletool">
						Modify Quota</a>	
					<?php }?>
					
				</td>
			</tr>
			</table>
			</td>
			<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
		<?=$li->display();?>
	</td>
</tr>
</table>

</form>
<?php

$linterface->LAYOUT_STOP();
###########################################
intranet_closedb();

?>