<?php
header('Content-Type: text/html; charset=utf-8');

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$LibeLib = new elibrary();
if (!$LibeLib->HAS_RIGHT())
{
     header ("Location: /");
     intranet_closedb();
     exit();
}

$InputArray["DisplayReviewer"] = $DisplayReviewer;
$InputArray["DisplayReview"] = $DisplayReview;
$InputArray["RecommendBook"] = $RecommendBook;
$InputArray["WeeklyHitBook"] = $WeeklyHitBook;
$InputArray["HitBook"] = $HitBook;
$InputArray["UserID"] = $_SESSION["UserID"];

//debug_r($InputArray);

$LibeLib->UPDATE_USER_BOOK_SETTING($InputArray);	


intranet_closedb();

header("Location: elib_setting.php");
?>