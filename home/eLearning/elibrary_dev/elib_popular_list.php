<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");

// additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html("popup.html");
$CurrentPage	= "PageMyeClass";

if($ListType == "recommend")
{
	$tname = $eLib["html"]["recommended_books"];
}
else if($ListType == "hit")
{
	$tname = $eLib["html"]["book_with_hit_rate_accumulated"];
}
else if($ListType == "weeklyhit")
{
	$tname = $eLib["html"]["book_with_hit_rate_last_week"];
}
else
	$tname = $ip20TopMenu['eLibrary'];

### Title ###
$title = "<img src='{$image_path}/{$LAYOUT_SKIN}/eclass/icon_myeclass.gif' align='absmiddle' /> ".$tname;

$TAGS_OBJ[] = array($title,"");

$lelib = new elibrary();
$MODULE_OBJ["title"] = $tname;
$MODULE_OBJ["logo"] = "";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

if($CurrentPageNum == "")
$CurrentPageNum = 1;

if($DisplayMode == "" || $DisplayMode == null)
$DisplayMode = 1;

if($DisplayNumPage == "")
$DisplayNumPage = 10;

$ParArr["CurrentPageNum"] = $CurrentPageNum;
$ParArr["DisplayNumPage"] = $DisplayNumPage;
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$tmpArr[0]["value"] = "Title";
$tmpArr[1]["value"] = "Author";
$tmpArr[2]["value"] = "Category";
$tmpArr[3]["value"] = "SubCategory";
$tmpArr[4]["value"] = "Publisher";
$tmpArr[5]["value"] = "Source";

$tmpArr[0]["text"] = $eLib["html"]["book_title"];
$tmpArr[1]["text"] = $eLib["html"]["author"];
$tmpArr[2]["text"] = $eLib["html"]["category"];
$tmpArr[3]["text"] = $eLib["html"]["subcategory"];
$tmpArr[4]["text"] = $eLib["html"]["publisher"];
$tmpArr[5]["text"] = $eLib["html"]["source"];

$tmpArr[0]["width"] = "25%";
$tmpArr[1]["width"] = "15%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "15%";
$tmpArr[4]["width"] = "15%";
$tmpArr[5]["width"] = "15%";

$inputArr["sortFieldArray"] = $tmpArr;

if($sortField == "")
$sortField = $tmpArr[0]["value"];

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["selectNumField"] = 7;
$currUserID = $_SESSION["UserID"];

$con = $sortField." ".$sortFieldOrder;

if($ListType == "recommend")
{
	/*
$sql = "
		SELECT DISTINCT
		a.BookID, a.Title, a.Author, a.Category,a.Publisher,a.Source
		FROM
		INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_RECOMMEND b
		WHERE
		a.BookID = b.BookID
		AND a.Publish = 1
		ORDER BY
		$con
		";
		*/
		
		if($lelib->IS_TEACHER() || $lelib->IS_ADMIN())
		$con = "";
		else
		$con = "AND b.ClassName = d.ClassName";
		
		$sql = "
				SELECT DISTINCT
				c.BookID, c.Title, c.Author, c.Category,c.Publisher,c.Source, c.SubCategory
				FROM 
				INTRANET_ELIB_BOOK_RECOMMEND a, INTRANET_ELIB_BOOK c, INTRANET_USER b, INTRANET_CLASS d
				WHERE 
				a.BookID = c.BookID
				$con
				AND b.UserID = '{$currUserID}'
				AND 
				(
				a.RecommendGroup LIKE CONCAT('%|',d.ClassLevelID,'|%')
				OR
				a.RecommendGroup LIKE CONCAT('%|',-1,'|%')
				)
				AND c.Publish = 1
				AND c.ContentTLF IS NOT NULL
				GROUP BY
				a.BookID
				ORDER BY 
				a.RecommendOrder ASC, a.DateModified DESC
				";
}
else if($ListType == "weeklyhit")
{
	$today = date("Y-m-d h:i:s"); 
	$lastweek = date('Y-m-d h:i:s', mktime(date("h"), date("i"), date("s"), date("m"),   date("d")-7,   date("Y"))); // 2009-04-04 12:00:00 

	$start_date = $lastweek;
	$end_date = $today;
		
	$sql = "
			   SELECT DISTINCT
			   a.BookID, a.Title, a.Author, a.Category,a.Publisher,a.Source, a.SubCategory,
			   count(c.BookID) countNum
			   FROM 
			   INTRANET_ELIB_BOOK_HISTORY c, INTRANET_ELIB_BOOK a
			   WHERE
			   a.BookID = c.BookID
			   AND a.Publish = 1
			   AND a.ContentTLF IS NOT NULL
			   AND
			   (c.DateModified BETWEEN '$start_date' AND '$end_date')
			   GROUP BY
			   c.BookID
			   ORDER BY countNum desc, $con
			   ";
}
else if($ListType == "hit")
{
		$sql = "
		       SELECT DISTINCT
			   a.BookID, a.Title, a.Author, a.Category, a.Publisher, a.Source, a.SubCategory
			   FROM 
			   INTRANET_ELIB_BOOK a
			   WHERE
			   a.Publish = 1
			   a.ContentTLF IS NOT NULL
			   AND a.HitRate >= 1
			   ORDER BY 
			   a.HitRate desc
			   ";
}
else
{
		$sql = "
		SELECT DISTINCT
		a.BookID, a.Title, a.Author, a.Category,a.Publisher,a.Source, a.SubCategory
		FROM
		INTRANET_ELIB_BOOK a
		WHERE
		a.Publish = 1
		a.ContentTLF IS NOT NULL
		ORDER BY
		$con
		";
}
		//debug_r($sql);

$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}
$totalPage = ceil($totalRecord / $DisplayNumPage);

////////////////////////////////////////////////////////////////////////////

// set view icon
$x = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><tr>";
$x .= "<td class=\"tabletextremark\">".$eLib["html"]["view"]."</td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 1)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list.gif\" alt=\"".$eLib["html"]["with_books_cover"]."\" name=\"view_list21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_list21\" onMouseOver=\"MM_swapImage('view_list21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_list_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(1);\">";

$x .= "</a></td>";
$x .= "<td><a href=\"#\">";

if($DisplayMode == 2)
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";
else
$x .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table.gif\" alt=\"".$eLib["html"]["table_list"]."\" name=\"view_21\" width=\"24\" height=\"20\" border=\"0\" id=\"view_21\" onMouseOver=\"MM_swapImage('view_21','','".$image_path."/".$LAYOUT_SKIN."/eLibrary/icon_view_table_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\" onClick=\"changeDisplayMode(2);\">";

$x .= "</a></td></tr></table>";

$viewChangeSelection = $x;

////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();

?>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--

//-->
</script>

<form name="form1" action="elib_popular_list.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5"><tr><td align="center">

<table width="100%" border="0" cellspacing="0" cellpadding="3">

<tr><td align="right">
<?=$viewChangeSelection?>
</td></tr>

<tr><td align="left">
<?=$contentHtml?>
</td></tr>

</table>

</td></tr></table>
<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="ListType" value="<?=$ListType?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >

</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>