<?php
//using by: Sandy

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

//additional javascript for eLibrary only
include_once("elib_script.php");

intranet_auth();
intranet_opendb();


############################################
$YearClassID = (isset($_REQUEST['YearClassID']))? $_REQUEST['YearClassID']: "";
$search = (isset($_REQUEST['search']))? $_REQUEST['search']: "";
$FromMonth = (isset($_REQUEST['FromMonth']) && $search)? $_REQUEST['FromMonth'] : "";
$ToMonth = (isset($_REQUEST['ToMonth']) && $search)? $_REQUEST['ToMonth']: "";
$ToYear = (isset($_REQUEST['ToYear']) && $search)? $_REQUEST['ToYear']: (($search)?Date("Y"): "");
$FromYear = (isset($_REQUEST['FromYear']) && $search)? $_REQUEST['FromYear']: "";


$linterface = new interface_html("default3.html");
$lo 		= new libeclass();
$lelib 		= new elibrary();
//$ly			= new year_class($YearClassID, false,false,false);

//debug_r($_REQUEST);
//$ClassName = $YearClassID;//$ly->ClassTitleEN;
$libclass = new libclass();

$CurrentPage	= "PageMyeClass";

### Title ###
$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;
$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] = $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] = "";
$MODULE_OBJ['title_css'] = "menu_opened";
$customLeftMenu = ' ';
$CurrentPageArr['eLib'] = 1;

$DisplayMode = 2; // list mode (no image cover)

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 10;

if($sortFieldOrder == "")
$sortFieldOrder = "ASC";

$inputArr["DisplayMode"] = $DisplayMode;
$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$lang = $_SESSION["intranet_session_language"];

$tmpArr[0]["value"] = "UserName";
$tmpArr[1]["value"] = "ClassName";
$tmpArr[2]["value"] = "ClassNumber";
$tmpArr[3]["value"] = "NumBookRead";
$tmpArr[4]["value"] = "NumBookReview";
$tmpArr[5]["value"] = "NumBookComplete";

$tmpArr[0]["text"] = $eLib["html"]["student_name"];
$tmpArr[1]["text"] = $eLib["html"]["class"];
$tmpArr[2]["text"] = $eLib["html"]["class_number"];
$tmpArr[3]["text"] = $eLib["html"]["num_of_book_read"];
$tmpArr[4]["text"] = $eLib["html"]["num_of_review"];
$tmpArr[5]["text"] = $eLib["html"]["num_of_completed_books"];

$tmpArr[0]["width"] = "*";
$tmpArr[1]["width"] = "15%";
$tmpArr[2]["width"] = "15%";
$tmpArr[3]["width"] = "10%";
$tmpArr[4]["width"] = "10%";
$tmpArr[4]["width"] = "10%";

if($sortField == "")
$sortField = $tmpArr[0]["value"];

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["sortFieldArray"] = $tmpArr;

$inputArr["selectNumField"] = 5;
$currUserID = $_SESSION["UserID"];

$con = $sortField." ".$sortFieldOrder;

$dsql1 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID}";
$dsql2 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_READ_{$currUserID}";
$dsql3 = "drop table IF EXISTS INTRANET_ELIB_TEMP_NUM_COMPLETE_{$currUserID}";

$lelib->db_db_query($dsql1);
$lelib->db_db_query($dsql2);
$lelib->db_db_query($dsql3);

$tsql1 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID}
		SELECT d.UserID, Count(d.ReviewID) as NumBookReview 
		FROM INTRANET_ELIB_BOOK a, INTRANET_USER c, INTRANET_ELIB_BOOK_REVIEW d 
		WHERE a.BookID = d.BookID AND c.UserID = d.UserID AND a.Publish = 1 AND a.ContentTLF IS NOT NULL
		GROUP BY d.UserID
		";

$tsql2 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_READ_{$currUserID}
		SELECT b.UserID, Count(DISTINCT b.BookID) as NumBookRead, b.DateModified
		FROM INTRANET_ELIB_BOOK a, INTRANET_ELIB_BOOK_HISTORY b, INTRANET_USER c
		WHERE a.BookID = b.BookID AND b.UserID = c.UserID AND a.Publish = 1 AND a.ContentTLF IS NOT NULL
		GROUP BY b.UserID
		";	
$tsql3 = "
		CREATE TABLE INTRANET_ELIB_TEMP_NUM_COMPLETE_{$currUserID}
		SELECT b.UserID, Count(DISTINCT b.BookID) as NumBookComplete
		FROM INTRANET_ELIB_BOOK a, INTRANET_ELIB_USER_PROGRESS b, INTRANET_USER c
		WHERE 
			a.BookID = b.BookID AND b.UserID = c.UserID AND a.Publish = 1 AND a.ContentTLF IS NOT NULL
			AND b.Percentage = 100
		GROUP BY b.UserID
		";	
		
		
$lelib->db_db_query($tsql1);
$lelib->db_db_query($tsql2);
$lelib->db_db_query($tsql3);

/*$selName1 = "if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), a.ChineseName, a.EnglishName) as UserName";
$selName2 = "if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), a.EnglishName, a.ChineseName) as UserName";*/

$selName1 = "CONCAT('<a href=\"elib_public_students_read_summary.php?YearClassID=', y.YearClassID, '&StudentID=', a.UserID ,'&ClassName=', a.ClassName,'\">' , if((a.ChineseName IS NOT NULL AND a.ChineseName != ''), a.ChineseName , a.EnglishName), '</a>') as UserName";
$selName2 = "CONCAT('<a href=\"elib_public_students_read_summary.php?YearClassID=', y.YearClassID, '&StudentID=', a.UserID ,'&ClassName=', a.ClassName,'\">' , if((a.EnglishName IS NOT NULL AND a.EnglishName != ''), a.EnglishName, a.ChineseName), '</a>') as UserName";


$selName = ($lang == "b5")? $selName1 : $selName2;
$selClass = ($ClassName == -1 || $ClassName == "")? "" : "AND a.ClassName='".$ClassName."'";

## SQL Query
$search_con = "";
if(!empty($FromMonth)){
	$search_con = '
			AND Date_Format(b.DateModified, "%Y-%m")  >= "'.$FromYear.'-'.$FromMonth.'"
			AND Date_Format(b.DateModified, "%Y-%m") <= "'.$ToYear.'-'.$ToMonth.'" ';
}

$sql = "
		SELECT
			a.UserID,
			a.ClassName,
			a.ClassNumber,
			$selName,
			if((b.NumBookRead IS NOT NULL),CONCAT('<a href=\"elib_public_students_read_summary.php?YearClassID=', y.YearClassID, '&ClassName=',a.ClassName,'&StudentID=',a.UserID, '\" class=\"tablelink\">',b.NumBookRead,'</a>'), 0) as NumBookRead,
			if((c.NumBookReview IS NOT NULL),CONCAT('<a href=\"elib_public_students_summary_review.php?YearClassID=', y.YearClassID , '&ClassName=',a.ClassName,'&StudentID=',a.UserID,'\" class=\"tablelink\">',c.NumBookReview,'</a>'), 0) as NumBookReview,
			if((p.NumBookComplete IS NOT NULL), p.NumBookComplete, 0) as NumBookComplete,
			y.YearClassID
		FROM
			INTRANET_ELIB_TEMP_NUM_REVIEW_{$currUserID} c
		RIGHT JOIN
			INTRANET_USER a
		ON
			a.UserID = c.UserID ";
$sql .= !empty($search_con) && !empty($search)? " INNER " : " LEFT ";
$sql .= "
		JOIN
			INTRANET_ELIB_TEMP_NUM_READ_{$currUserID} b
		ON
			a.UserID = b.UserID
		LEFT JOIN 
			INTRANET_ELIB_TEMP_NUM_COMPLETE_{$currUserID} as p
		ON
			a.UserID = p.UserID
		LEFT JOIN 
			YEAR_CLASS as y
		ON 
			a.ClassName = y.ClassTitleEN
	 	WHERE 1 ";
$sql .=  !empty($search)? $search_con : "";
$sql .=	"
		$selClass
	 	AND (a.ClassName IS NOT NULL)
	 	AND !(a.ClassName = '')
	 	AND y.AcademicYearID = ".Get_Current_Academic_Year_ID()."  
	 	group by a.UserID
		ORDER BY
		$con
		
		";
		
		//debug_r($sql);


$resultArr = $lelib->displayBookDetail($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

// drop the temp table create before
$lelib->db_db_query($dsql1);
$lelib->db_db_query($dsql2);
$lelib->db_db_query($dsql3);

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);

///////////////////// Selection Box ////////////////////////////////////////////////

$returnClassNameArr = $lelib->GET_CLASSNAME();

	$ClassNameArr = array(
				array("-1", $eLib["html"]["all_class"])
				);
			
	//debug_r($returnClassNameArr);
	for($i = 0; $i < count($returnClassNameArr); $i++)
	{
		
		if(trim($returnClassNameArr[$i]["ClassTitleEN"]) != "")
		$tmpClassArr = "";
		$tmpClassArr = array($returnClassNameArr[$i]["YearClassID"], $returnClassNameArr[$i]["ClassTitleEN"]);
		
		$ClassNameArr[$i+1] = $tmpClassArr;
	}
	//debug_r($ClassNameArr);
	

	//if($ClassName == "")
	//$ClassName = $ClassNameArr[0][0];

	$fcm = new form_class_manage();

	//debug_r($fcm->Get_Class_List_By_Academic_Year($AcademicYearID));
	$ClassSelection= $libclass->getSelectClass( "name='ClassName' id='ClassName' onChange='changeClassName(this);'", $ClassName );
	//$ClassSelection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($ClassNameArr, "name='YearClassID' id='ClassName' onChange='changeClassName(this);'", "", $YearClassID);
	
	$ThisYear = Date("Y")*1;
	$aryMonth = array(array(' ','--'),array(1,1),array(2,2),array(3,3),array(4,4),array(5,5),array(6,6),array(7,7),array(8,8),array(9,9),array(10,10),array(11,11),array(12,12));
	$aryYear = array(array(' ','--'),array($ThisYear,$ThisYear), array($ThisYear-1,$ThisYear-1),array($ThisYear-2,$ThisYear-2),array($ThisYear-3,$ThisYear-3),array($ThisYear-4,$ThisYear-4),array($ThisYear-5,$ThisYear-5)); 
		
	$FromMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='FromMonth' id='FromMonth'", "", $FromMonth);
	$FromYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='FromYear' id='FromYear'", "", $FromYear);
	$ToMonthSelection = $linterface->GET_SELECTION_BOX($aryMonth, "name='ToMonth' id='ToMonth'", "", $ToMonth);
	$ToYearSelection = $linterface->GET_SELECTION_BOX($aryYear, "name='ToYear' id='ToYear'", "", $ToYear);
	
	$classStr = $eLib["html"]["class"]." : ";
	
	$SelectionTable = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\">";
	//$SelectionTable .= "<tr><td><span class=\"tabletext\">".$classStr." ".$ClassSelection."</span></td></tr>";
	$SelectionTable .= '<tr><td>';
	$SelectionTable .= '<span class="tabletext">'.$classStr.' '.$ClassSelection.'</span>&nbsp;&nbsp;&nbsp;';
	$SelectionTable	.= '<span class="tabletext">'.$Lang['Header']['Menu']['Period'].':'.
						$FromMonthSelection.$FromYearSelection.$Lang['SysMgr']['Homework']['To'].$ToMonthSelection.$ToYearSelection.
						'<input type="button" name="search" onClick="applySearch();" value="'.$Lang['Btn']['Apply'].'" /></span></td></tr>';
	
	$SelectionTable .= "</table>";
//////////////////////////////////////////////////////////////////////////////////

// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>


<script language="JavaScript" type="text/JavaScript">
<!--

function changeClassName(obj)
{
	document.form1.CurrentPageNum.value = 1;
	document.form1.submit();
} // end function change class name

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field


function applySearch(){
	
	var strFromDate = document.form1.FromYear.value+"-"+document.form1.FromMonth.value+"-0";
	var strToDate = document.form1.ToYear.value+"-"+document.form1.ToMonth.value+"-0";
	if(compareDate(strToDate,strFromDate)==1){
		$("#search").val(1);
		document.form1.submit();
	}else{	
		alert("<?=$eLib["html"]["Incorrect_Date"]?>");	
	}
}


//-->
</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">


<form name="form1" action="elib_public_students_summary.php" method="post">
<table width="100%" border="0">
	<tr>
	<td width="175" style="margin:0px padding:0px;" valign="top">
				
		<!-- Template , Left Navigation panel --> 
		<?php include_once('elib_left_navigation.php')?>
		
	</td>
	<td valign="top">
		<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
		
		<!-- head menu -->
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
		<td class="eLibrary_navigation">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
			<a href="index.php"><?=$eLib["html"]["home"]?></a>
			 
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
			<a href="elib_public_class_summary.php"><?=$eLib["html"]["class_summary"]?></a>
			
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle">
			<?=$eLib["html"]["student_summary"]?>
			
		</td>
		</tr>
		</table>
		<!-- end head menu -->
		
		
		<table width="98%" border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_01.gif" width="15" height="18"></td>
		<td align="left" valign="top" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_02.gif" width="19" height="18"></td>
		<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_03.gif" width="25" height="18"></td>
		</tr>
		
		<tr>
		<td width="15" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_04.gif" width="15" height="21"></td>
		
		<td background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_05.gif">
		<!-- show content -->
		<?=$SelectionTable; ?>
		<?=$contentHtml; ?>
		<!-- end show content -->
		</td>
		
		
		<td width="25" background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_06.gif" width="25" height="30"></td>
		</tr>
															
		<tr>
		<td width="15" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_07.gif" width="15" height="26"></td>
		<td height="26" style="background-image:url(<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_08.gif); background-repeat:repeat-x; background-position:right"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" alt="" width="8" height="20"></td>
		<td width="25" height="26"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/eLibrary/myrecord_paper_09.gif" width="25" height="26"></td>
		</tr>
		</table>
		
		</td>
		</tr>
		</table>




<input type="hidden" name="DisplayMode" value="<?=$DisplayMode?>" >
<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

<input type="hidden" name="search" id="search" value="" >

</form>


<script language="JavaScript" type="text/JavaScript">
/*if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}*/
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>