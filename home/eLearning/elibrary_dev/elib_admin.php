<?php

//using: 

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//var_dump($intranet_session_language);
include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
//include_once($PATH_WRT_ROOT."lang/lang.b5.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_dev.php");

intranet_auth();
intranet_opendb();

// additional javascript for eLibrary only
include_once("elib_script.php");

//debug_r($_SESSION);

$lo 		= new libeclass();
$lelib 		= new elibrary();
$linterface = new interface_html("default3.html");
$CurrentPage= "PageMyeClass";

$ParArr["image_path"] = $image_path;
$ParArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$title = $lelib->printSearchInput($ParArr, $eLib);
	
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ["title"] 	= $ip20TopMenu['eLibrary'];
$MODULE_OBJ["logo"] 	= "";
$MODULE_OBJ['title_css']= "menu_opened";
$customLeftMenu 		= ' ';
$CurrentPageArr['eLib'] = 1;

$ParArr["UserID"] 		= $_SESSION["UserID"];

$settingArr = $lelib->GET_USER_BOOK_SETTING($ParArr);

//$totalPage = 0;
//$DisplayNumPage = 0;

if($CurrentPageNum == "" || $CurrentPageNum == null)
$CurrentPageNum = 1;

if($DisplayNumPage == "" || $DisplayNumPage == null)
$DisplayNumPage = 20;

if($sortField == "")
$sortField = "DateModified";

if($sortFieldOrder == "")
$sortFieldOrder = "DESC";

$inputArr["CurrentPageNum"] = $CurrentPageNum;
$inputArr["DisplayNumPage"] = $DisplayNumPage;

$inputArr["sortField"] = $sortField;
$inputArr["sortFieldOrder"] = $sortFieldOrder;

$inputArr["image_path"] = $image_path;
$inputArr["LAYOUT_SKIN"] = $LAYOUT_SKIN;

$currUserID = $_SESSION["UserID"];

//$order = $sortField;
//$order = $order." ".$sortFieldOrder;

$sql = "";
$resultArr = $lelib->displayBookAdmin($inputArr, $eLib, $sql);
$contentHtml = $resultArr["contentHtml"];
$totalRecord = $resultArr["totalRecord"];
$DisplayNumPage = $resultArr["DisplayNumPage"];

if($CurrentPageNum * $DisplayNumPage > $totalRecord)
{
	$CurrentPageNum = ceil($totalRecord / $DisplayNumPage);
	
	if($CurrentPageNum <= 0)
	$CurrentPageNum = 1;
}

$totalPage = ceil($totalRecord / $DisplayNumPage);


// additional javascript for eLibrary only
include_once("elib_script_function.php");

$linterface->LAYOUT_START();
?>
<script>
$(document).ready(function(){
	onclickNextBook();
});

function Book(bookTitle, bookDescription, bookPath)
{
	this.title = bookTitle;
	this.description = bookDescription;
	this.path = bookPath;
	
}

var curBook = 0;
//var recomBooks = new Array();

<?php
//$book = $lelib->GET_RECOMMEND_BOOK($NumRecommend);
//for ($i = 0; $i < count($book); ++$i)
//{
//	echo "recomBooks[$i] = new Book(\"".$book[$i]["Title"]."\", \"".str_replace("\n", '\n', $book[$i]["Description"])."\", \"".$book[$i]["BookID"]."\");\r\n";
//}
?>

function checkValidField(obj)
{
	obj.submit();
} // end function check valid field

</script>
<link href="css/eLibrary.css" rel="stylesheet" type="text/css">
<link href="css/elib.css" rel="stylesheet" type="text/css">


<link href="css/elib_josephine.css" rel="stylesheet" type="text/css">


<table width="100%"  height="100%"border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="top">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
 		<tr>
 			<td align="center">
  				<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
        			<td>
        				<table width="99%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center" bgcolor="#FFFFFF">
                    			<table cellpadding="0" cellspacing="0" border="0" width="100%" align="left">
                    			<tr>
                    				<td valign="top" width="175">
									                    					
										<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="20">
									
										<!-- ------------------------------------------------------------ -->
										<!-- ---------------      LEFT NAVIGATION BAR ----------------------- --> 
										<?php include_once('elib_left_navigation.php'); ?>								
									</td>

									<td valign="top" align="left">
										<div class="800width"></div>

<!-- eLibrary Content Begin -->
<form name="form1" action="elib_admin.php" method="post">


<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>
<!-- head menu -->
<table width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
<td class="eLibrary_navigation">
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="index.php"><?=$eLib["html"]["home"]?></a> 
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" align="absmiddle">
<a href="elib_admin.php">Book Editing</a>
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/nav_arrow.gif" width="15" height="15" border="0" align="absmiddle"><?=$langName?></td>
</tr>
</table>
<!-- end head menu -->
<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"><br>



<!-- eLibrary Book Begin -->
<div id="admin_rank">
	
	
	<!-- Begin of Most Active Reviewers -->
	<div class="book_admin_top_left">
		<div class="book_admin_top_right">
			<div style="float:right; margin-right:20px">
				<span class="table_head"><a href="elib_admin_add_book.php">Add New Book</a></span>
			</div>
			<div>
				<span class="table_head">Book Admin Page</span>
			</div>
		</div>
	</div>

    
	<div class="book_admin_body_left">
		<div class="book_admin_body_right">
			<div class="book_admin_body">
				<?=$contentHtml?>
			</div>
    		<div class="book_admin_footer_left">
				<div class="book_admin_footer_right">
				</div>
			</div>
		</div>
	</div>
	<!-- End of Most Active Reviewers -->
	
 
</div>

<!-- eLibrary Book End -->


<input type="hidden" name="CurrentPageNum" value="<?=$CurrentPageNum?>" >
<input type="hidden" name="DisplayNumPage" value="<?=$DisplayNumPage?>" >
<input type="hidden" name="sortField" value="<?=$sortField?>" >
<input type="hidden" name="sortFieldOrder" value="<?=$sortFieldOrder?>" >

</form>

</td></tr></table>
                      
                      </td>
                      
                    </tr>
                   
                  </table>
                  <br>
                </td>
              </tr>
            </table></td>
        </tr>
    </table></td>
  </tr>
				<tr> 
				    <td bgcolor="#999999"> 
				      <table width="100%" border="0" cellspacing="0" cellpadding="0">
				        <tr>
				          <td align="right"><span class="footertext">Powered by</span> <a href="#" class="footerlink">eClass</a></td>
				          <td width="20"><img src="images/2007a/10x10.gif" width="20" height="20"></td>
				        </tr>
				      </table></td>
				
				  </tr>
</table>






<script language="JavaScript" type="text/JavaScript">
if('<?=$intranet_session_language?>' == "en"){
	showEnglishBookList();
}else if('<?=$intranet_session_language?>' == "b5"){
	showChineseBookList();
}
</script>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
