<?php
//modifying By
if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];

$field	= isset($field) && $field!=''? $field : 0;
$order	= isset($order) && $order!=''? $order : 1;
$pageNo	= isset($pageNo) && $pageNo!=''? $pageNo : 1;

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);
$objiTextbook->admin_allow_create_book($Current_Code);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$sql = "SELECT
			ib.BookName AS BookName,
			IFNULL(tmp_d.UserCount, 0) AS UserCount,
			CONCAT('<a title=\'".$Lang['itextbook']['ChapterList']."\' class=\'thickbox\' href=\'book_chapter_list.php?BookID=', ib.BookID, '&KeepThis=true&TB_iframe=true&height=400&width=600\'>".$Lang['itextbook']['View']."</a>') AS ChapterList,
			CONCAT('<input type=\'checkbox\' id=\'BookID_', ib.BookID, '\' name=\'BookID[]\' value=\'', ib.BookID, '\' allow_delete=\'', IF(tmp_d.UserCount > 0, 0, 1), '\'>')
		FROM
			ITEXTBOOK_BOOK AS ib LEFT JOIN
			(
				SELECT
					id.BookID,
					COUNT(*) AS UserCount
				FROM
					ITEXTBOOK_DISTRIBUTION AS id INNER JOIN
					ITEXTBOOK_DISTRIBUTION_USER AS idu ON id.DistributionID = idu.DistributionID
				GROUP BY
					id.BookID
			) AS tmp_d ON ib.BookID = tmp_d.BookID
		WHERE
			ib.BookType = '".$Current_Code."' AND
			ib.Status = ".$iTextbook_cfg['db_setting']['book']['status']['not_deleted'];

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("BookName", "UserCount");
$li->sql = $sql;
$li->no_col = 5;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = "IP25_table";

# TABLE COLUMN
$li->column_list .= "<th width='5%' class='tabletop tabletopnolink' style='vertical-align:middle'>&nbsp;#&nbsp;</th>\n";
$li->column_list .= "<th width='*' class='tabletop' style='vertical-align:middle'>".$li->column(0, $Lang['itextbook']['BookName'])."</th>";
$li->column_list .= "<th width='20%' class='tabletop' style='vertical-align:middle'>".$li->column(1, $Lang['itextbook']['NoOfStudents'])."</th>";
$li->column_list .= "<th width='10%' class='tabletop' style='vertical-align:middle'>".$Lang['itextbook']['ChapterList']."</th>";
$li->column_list .= "<th width='1%' class='tabletop' style='vertical-align:middle'>".$li->check("BookID[]")."</th>\n";

# Generate System Message
$html_message = $msg? $linterface->GET_SYS_MSG($msg) : "&nbsp;";

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['BookList'], "link"=>"javascript:go_book_list()");

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>	
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script>
function go_book_list(){
	var obj = document.header_form;
	
	obj.action = 'book_list.php';
	obj.submit();
}

function go_add_new_book(){
	var obj = document.header_form;
	
	obj.action = 'book_list_add.php';
	obj.submit();
}

function remove_book(){
	var err_exist = false;
	
	$(':input[name=BookID[]]:checked').each(function(index){
		if($(this).attr('allow_delete')==0){
			err_exist = true;
			return false;
		}
	});
	
	if(err_exist){
		alert("<?=$Lang['itextbook']['DeleteBookWarning']?>");
	}
	else{
		checkRemove(document.form1,'BookID[]','book_list_remove.php');
	}
}
</script>
<table width="100%">
	<tr><td><?=$objiTextbook->gen_tab_menu(2)?></td></tr>
	<tr><td><?=$objiTextbook->gen_admin_nav($page_ary)?></td></tr>
	<tr>
		<td>
			<form name="form1" method="POST">
				<div class="content_top_tool">
  					<div style="float:right">
  						<?=$html_message?>
  					</div>
				</div>
				<div class="content_top_tool">
  					<div class="Conntent_tool">
  						<a title="Add New Book" href="javascript:go_add_new_book()"><?=$Lang['itextbook']['AddNewBook']?></a>
  					</div>
				</div>
				<div class="table_board">
					<table width="100%" cellspacing="0" cellpadding="0" border="0">
						<tr>
							<td align="right">
								<table border="0" cellpadding="0" cellspacing="0">
          							<tr>
            							<td width="21"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_01.gif" height="23" width="21"></td>
            							<td background="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_02.gif">
              								<table border="0" cellpadding="0" cellspacing="2">
												<tr>
                    								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
                    								<td nowrap="nowrap"><a class="tabletool" href="javascript:void(0)" onclick="checkEdit(document.form1,'BookID[]','book_list_add.php')"><img height="12" src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_edit.gif" width="12" align="absMiddle" border="0" /><?=$ec_iPortfolio['edit']?></a></td>
                    								<td><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/10x10.gif" width="5"></td>
                    								<td nowrap="nowrap"><a class="tabletool" href="javascript:void(0)" onclick="remove_book()"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/icon_delete.gif"  name="imgDelete" align="absmiddle" border="0" ><?=$ec_iPortfolio['delete']?></a></td>
                  								</tr>
              								</table>
            							</td>
            							<td width="6"><img src="<?=$PATH_WRT_ROOT?><?=$image_path?>/<?=$LAYOUT_SKIN?>/management/table_tool_03.gif" height="23" width="6"></td>
          							</tr>
        						</table>
							</td>
						</tr>
						<tr>
							<td><?=$li->display();?></td>
						</tr>
					</table>
					<br />
			  		<p class="spacer"></p>
				</div>
			  	<input type="hidden" name="pageNo" value="<?=$li->pageNo?>"/>
			  	<input type="hidden" name="order" value="<?=$li->order?>"/>
			  	<input type="hidden" name="field" value="<?=$li->field?>"/>
			  	<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
			  	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
			</form>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>