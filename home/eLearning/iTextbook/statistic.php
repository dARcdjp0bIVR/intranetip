<?php
//modifying By Thomas

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);

# Generate Year Class selection list
$fcm = new form_class_manage();
$ClassList = $fcm->Get_Class_List_By_Academic_Year($CurrentSchoolYearInfo['AcademicYearID']);

$ClassListHtml = '<select id="SelectedYearClassID" name="SelectedYearClassID" onchange="change_class_list()">
					<option value="0">'.$Lang['itextbook']['AllClasses'].'</option>';
for ($i=0; $i<count($ClassList); $i++) {
	if ($ClassList[$i]['YearID'] != $ClassList[$i-1]['YearID']) {
		if ($i == 0) 
			$ClassListHtml .= '<optgroup label="'.$ClassList[$i]['YearName'].'">';
		else {
			$ClassListHtml .= '</optgroup>';
			$ClassListHtml .= '<optgroup label="'.$ClassList[$i]['YearName'].'">';
		}
	}
	
	$ClassListHtml .= '<option value="'.$ClassList[$i]['YearClassID'].'" '.($ClassList[$i]['YearClassID']==$SelectedYearClassID? 'selected':'').'>'.Get_Lang_Selection($ClassList[$i]['ClassTitleB5'],$ClassList[$i]['ClassTitleEN']).'</option>';
	
	if ($i==count($ClassList)-1) 
		$ClassListHtml .= '</optgroup>';
}

$ClassListHtml .= '</select>';

# Handle the book language
$current_page_lang = $intranet_session_language;
$current_page_lang = $current_page_lang=='b5'? 'ch' : $current_page_lang;
$BookLang = in_array($current_page_lang, $iTextbook_cfg[$Current_Code]['BookLang'])? $current_page_lang : $iTextbook_cfg[$Current_Code]['DefaultLang'];

# Generate Chapter selection list
$ChapterPageAry = $objiTextbook->get_chapter_page_ary($Current_Code);

$ChapterListHtml = '<select id="SelectedChapter" name="SelectedChapter" onchange="change_chapter_list(0)">
						<option value="0">'.$Lang['itextbook']['AllChapters'].'</option>';
for($i=0; $i<count($ChapterPageAry); $i++){
	if(!is_array($iTextbook_cfg[$Current_Code]['DisableUnit']) || !in_array($ChapterPageAry[$i]['Unit'], $iTextbook_cfg[$Current_Code]['DisableUnit'])){
		if($ChapterPageAry[$i]['Unit']==$SelectedChapter){
			$IsSelected = 'selected';
			$SelectedChapterName = $Lang['itextbook']['Chapter'][$BookLang].$ChapterPageAry[$i]['Unit'].' - '.$ChapterPageAry[$i]['ChapterName'][$BookLang];
		}
		else{
			$IsSelected = '';
		}
		
		$ChapterListHtml .= '<option value="'.$ChapterPageAry[$i]['Unit'].'" '.$IsSelected.'>'.$Lang['itextbook']['Chapter'][$BookLang].$ChapterPageAry[$i]['Unit'].' - '.$ChapterPageAry[$i]['ChapterName'][$BookLang].'</option>';
	}
}
$ChapterListHtml .= '</select>';

# Generate Statistic Table
$view_count_ary = $objiTextbook->get_chapter_page_view_ary_with_filter($Current_Code, $SelectedYearClassID, $SelectedChapter);
$ChapterListTableHtml = '';
$total_view_count = 0;
for($i=0; $i<count($ChapterPageAry); $i++){
	if(!is_array($iTextbook_cfg[$Current_Code]['DisableUnit']) || !in_array($ChapterPageAry[$i]['Unit'], $iTextbook_cfg[$Current_Code]['DisableUnit'])){
		if(!$SelectedChapter){
			$ChapterListTableHtml .= '<tr>
										<td>'.($i+1).'</td>
										<td>'.$Lang['itextbook']['Chapter'][$BookLang].$ChapterPageAry[$i]['Unit'].' - '.$ChapterPageAry[$i]['ChapterName'][$BookLang].'</td>
										<td style="text-align:right">
											<a href="javascript:void(0)" onclick="change_chapter_list('.$ChapterPageAry[$i]['Unit'].')">'.$view_count_ary[$ChapterPageAry[$i]['Unit']].'</a>
										</td>
									  </tr>';
			$total_view_count += $view_count_ary[$ChapterPageAry[$i]['Unit']];
		}
		else if($ChapterPageAry[$i]['Unit']==$SelectedChapter){
			for($j=0; $j<count($ChapterPageAry[$i]['Notes'][$BookLang]); $j++){
				$ChapterListTableHtml .= '<tr>
											<td>'.($j+1).'</td>
											<td>'.$ChapterPageAry[$i]['Notes'][$BookLang][$j].'</td>
											<td style="text-align:right">'.$view_count_ary[$SelectedChapter][$j].'</td>
									 	  </tr>';
				$total_view_count += $view_count_ary[$SelectedChapter][$j];
			}
		}
	}
}

$ChapterListTableHtml .= '<tr class="sub_row">
							<td colspan="2" style="text-align:right"><strong>'.$Lang['itextbook']['Total'].' :</strong></td>
							<td style="text-align:right">'.$total_view_count.'</td>
					 	  </tr>';

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['Statistic'], "link"=>"javascript:go_statistic()");
$page_ary[] = array("display"=>($SelectedChapterName? $SelectedChapterName:$Lang['itextbook']['AllChapters']), "link"=>"");

# Generate System Message after operation
$html_message = isset($msg)? $linterface->GET_SYS_MSG($msg) : "";

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<script>
function go_assign_teacher(){
	var obj = document.header_form;
	
	obj.action = 'statistic.php';
	obj.submit();
}

function change_class_list(){
	var obj = document.form1;
	
	obj.action = 'statistic.php';
	obj.submit();
}

function change_chapter_list(req_chapter){
	var obj = document.form1;
	
	if(req_chapter){
		obj.SelectedChapter.value = req_chapter;
	}
	
	obj.action = 'statistic.php';
	obj.submit();
}
</script>
<table width="100%">
	<tr><td><?=$objiTextbook->gen_tab_menu(4)?></td></tr>
	<tr><td><?=$objiTextbook->gen_admin_nav($page_ary)?></td></tr>
	<tr>
		<td>
			<form name="form1" method="POST">
				<div class="content_top_tool">
  					<div style="float:right">
  						<?=$html_message?>
  					</div>
				</div>
				<div class="content_top_tool">
  					<div class="Conntent_tool">
  						<?=$ClassListHtml?>
  						<?=$ChapterListHtml?>
  					</div>
				</div>
				<div class="table_board">
					<table class="common_table_list">
						<col width="25px"/>
						<col width="*"/>
						<col width="10%"/>
						<thead>
							<th>#</th>
							<th><?=$Lang['itextbook']['Title']?></th>
							<th><?=$Lang['itextbook']['Views']?></th>
						</thead>
						<tbody>
							<?=$ChapterListTableHtml?>
						</tbody>
					</table>
					<br />
			  		<p class="spacer"></p>
				</div>
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
			</form>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>