<?php
# Modifying by: thomas

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_auth();
intranet_opendb();

$objiTextbook = new libitextbook();

$DistributionID = isset($DistributionID) && $DistributionID!=''? $DistributionID : 0;
$DistributionNo = isset($DistributionNo) && $DistributionNo!=''? $DistributionNo : 1;
$ChooseUserID 	= isset($ChooseUserID) && $ChooseUserID!=''? $ChooseUserID : array();
$Current_Code	= isset($Current_Code) && $Current_Code!=''? $Current_Code : '';

$quota_left = $objiTextbook->get_distributions_quota_left($ModuleLicenseID, $Current_Code);
$result = array();

# Get ModuleCode and Distribution Input Date
$sql = "SELECT 
    		im.Code, id.DateInput
		FROM 
    		ITEXTBOOK_DISTRIBUTION AS id INNER JOIN 
    		INTRANET_MODULE_LICENSE AS iml ON id.ModuleLicenseID = iml.ModuleLicenseID INNER JOIN
	    	INTRANET_MODULE AS im ON iml.ModuleID = im.ModuleID
		WHERE
			id.DistributionID = $DistributionID";
list($ModuleCode, $DateInput) = current($objiTextbook->returnArray($sql));

$objiTextbook->Start_Trans();

if(count($ChooseUserID)*$iTextbook_cfg[$Module_Code]['MaxChapters'] <= $quota_left && !$objiTextbook->Is_Cooldown_passed($iTextbook_cfg[$ModuleCode]['cooldown'], $DateInput)){
	# Get iTextbook Special Room course_id
	$sql = "SELECT course_id FROM $eclass_db.course WHERE course_code = '$Current_Code' AND RoomType = 7";
	$course_id = current($objiTextbook->returnVector($sql));
	
	$lo = new libeclass($course_id);
	
	# Get User who are not in iTextbook Special Room
	$sql = "SELECT
				UserID, UserEmail, UserPassword, ClassNumber, FirstName,
				LastName, ClassName, Title, EnglishName, ChineseName,
				NickName, Gender, Info, RecordType
			FROM
				$intranet_db.INTRANET_USER
			WHERE
				UserID IN (".implode(",", $ChooseUserID).") AND
				UserID NOT IN (
								SELECT
								    iu.UserID
								FROM
								    $eclass_db.course AS c INNER JOIN
								    $eclass_db.user_course AS uc ON c.course_id = uc.course_id INNER JOIN
								    $intranet_db.INTRANET_USER AS iu ON uc.intranet_user_id = iu.UserID OR uc.user_email = iu.UserEmail
								WHERE
								    c.course_code = '$Current_Code' AND
								    c.RoomType = 7
							  )";
	$UserInfoList = $objiTextbook->returnArray($sql);
	
	for($i=0;$i<count($UserInfoList);$i++){
		if($lo->max_user <> "" && $i == $lo->ticketUser())
			break;
		
		switch($UserInfoList[$i]['RecordType']){
			case 1: $par_MemberType="T"; break; // Teacher
			case 2: $par_MemberType="S"; break; // Student
			case 3: $par_MemberType="P"; break; // Parent
		}
		
		$par_UserID 	  = $UserInfoList[$i]['UserID'];
		$par_UserEmail 	  = $UserInfoList[$i]['UserEmail'];
		$par_UserPassword = $UserInfoList[$i]['UserPassword'];
          
		if($par_MemberType=="S" && $zero_padding['ClassNum']){
			$par_ClassNumber = str_pad($UserInfoList[$i]['ClassNumber'], 2, "0", STR_PAD_LEFT);
		}
		else{
			$par_ClassNumber = ($UserInfoList[$i]['ClassNumber']==0) ? "" : $UserInfoList[$i]['ClassNumber'];
		}
          
		$par_FirstName = $UserInfoList[$i]['FirstName'];
		$par_LastName  = $UserInfoList[$i]['LastName'];
		$par_ClassName = $UserInfoList[$i]['ClassName'];
		$par_Title 	   = $UserInfoList[$i]['Title'];
		$par_EngName   = $UserInfoList[$i]['EnglishName'];
		$par_ChiName   = $UserInfoList[$i]['ChineseName'];
		$par_NickName  = $UserInfoList[$i]['NickName'];
		$par_Gender    = $UserInfoList[$i]['Gender'];
		$par_Info 	   = $UserInfoList[$i]['Info'];

		if ($par_ClassNumber=="" || $par_ClassName=="" || stristr($par_ClassNumber,$par_ClassName))
			$par_eclassClassNumber = $par_ClassNumber;
		else
			$par_eclassClassNumber = $par_ClassName ." - ".$par_ClassNumber;
          
		$lo->eClassUserAddFullInfo($par_UserEmail, $par_Title, $par_FirstName, $par_LastName, $par_EngName, $par_ChiName, $par_NickName, $par_MemberType, $par_UserPassword, $par_eclassClassNumber, $par_Gender, $ICQNo, $HomeTelNo ,$FaxNo, $DateOfBirth, $Address, $Country, $URL, $par_Info, $par_ClassName, $par_UserID);
	}
	$lo->eClassUserNumber($lo->course_id);
	
	for($i=0;$i<count($ChooseUserID);$i++){
		$sql = "INSERT INTO
					ITEXTBOOK_DISTRIBUTION_USER
					(DistributionID, UserID, DateInput, DateModified, ModifiedBy)
				VALUES
					('$DistributionID', '".$ChooseUserID[$i]."', NOW(), NOW(), $UserID)";
		$result[] = $objiTextbook->db_db_query($sql);
	}
}
else{
	$result[] = false;
}

if(!in_array(false, $result)){
	$objiTextbook->Commit_Trans();
	if(strpos($Current_Code,'grammartouch') !== false){
		$objiTextbook->sync_grammartouch_license($ChooseUserID,$Current_Code,$DistributionID,$ModuleLicenseID);
	}
	$sys_msg = "add";
}
else{
	$objiTextbook->RollBack_Trans();
	$sys_msg = "add_failed";
}

intranet_closedb();
?>
<body onLoad="opener.reload_distribution(<?=$DistributionID?>, <?=$DistributionNo?>, '<?=$sys_msg?>'); document.form1.submit();">
	<form name="form1" action="import.php" method="get">
  		<input type="hidden" name="DistributionID" value="<?=$DistributionID?>"/>
  		<input type="hidden" name="DistributionNo" value="<?=$DistributionNo?>"/>
	</form>
</body>