<?php
//modifying By thomas
if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
$msg			  = isset($msg) && $msg!=''? $msg : "";
$BookID			  = isset($BookID) && is_array($BookID)? current($BookID) : $BookID;
$BookID			  = $BookID? $BookID : 0;

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);
$objiTextbook->admin_allow_create_book($Current_Code);

# Get Selected Book Info
if($BookID){
	$BookInfo 			   = $objiTextbook->get_book_info($BookID, true);
	$selected_chapter_list = $BookInfo['Chapters'];
}
else{
	$BookInfo 			   = array();
	$BookInfo['BookLang']  = $iTextbook_cfg[$Current_Code]['DefaultLang'];
	$selected_chapter_list = array();
}

# Student assigned with this book
$std_assigned = $BookInfo['UserCount']? $BookInfo['UserCount'] : 0;

# Check the Book is Locked or not
$IsLock = $std_assigned > 0;

# Generate System Message
$html_message   = $msg? $linterface->GET_SYS_MSG($msg) : "&nbsp;";

# Generate Update Message
$update_message = $BookID? $Lang['itextbook']['LastUpdate']." : ".$BookInfo['DateModified']." (".$BookInfo['ModifiedBy'].")" : ""; 

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['BookList'], "link"=>"javascript:go_book_list()");
$page_ary[] = array("display"=>($BookID? $Lang['itextbook']['EditBook'] : $Lang['itextbook']['AddNewBook']), "link"=>"");

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<style>
.chapter_list_title {
    background-color: #EBEBEB;
    display: block;
    padding: 2px;
}

.chapter_list_title_name {
    float: left;
    font-weight: bold;
}

.chapter_list_chapter {
    display: block;
    padding: 2px;
    margin: 10px 5px;
}

.chapter_list_chapter a {
    background-color: #E7EDFA;
    cursor: move;
    padding: 2px;
    color: #2286C5;
}

.chapter_list_chapter a:visited {
    color: #2286C5;
}

.chapter_list_chapter.fixed a{
	background-color: #C0C0C0;
	cursor: default;
	color: #FFFFFF;
}

.peer_within_group_board {
    border-collapse: separate;
    border-spacing: 0;
    width: 100%;
}
</style>
<script src="/templates/jquery/ui/ui.core_dd.js" language="JavaScript"></script>
<script src="/templates/jquery/ui/ui.draggable_dd.js" language="JavaScript"></script>
<script src="/templates/jquery/ui/ui.droppable_dd.js" language="JavaScript"></script>
<script src="/templates/jquery/ui/ui.sortable_dd.js" language="JavaScript"></script>
<script>
$(document).ready(function(){
	if($('.within_column').length > 0){
		$('.within_column').sortable({
			connectWith	: ".within_column",
			items		: ".chapter_list_chapter:not('.fixed')",
			update		: function(event, ui){
							$('#selected_chapter_list .IsSelected').val(1);
							$('#chapter_list .IsSelected').val(0);
						  }
		});
		$(".chapter_list_chapter:not('.fixed')").disableSelection();
	}
});

function go_book_list(){
	var obj = document.header_form;
	
	obj.action = 'book_list.php';
	obj.submit();
}

function check_form(){
	var obj = document.form1;
	
	if(obj.BookName.value==''){
		obj.BookName.focus();
		alert('<?=$Lang['itextbook']['PleaseEnterBookName']?>');
		return false;
	}
	
	var chapter_check = $("#selected_chapter_list > div:not('.chapter_list_title')").length - <?=$iTextbook_cfg[$Current_Code]['MaxChapters']?>;
	if(chapter_check > 0){
		alert('<?=$Lang['itextbook']['NoOfChapterExceedWarning'][0]?>' + chapter_check + '<?=$Lang['itextbook']['NoOfChapterExceedWarning'][1]?>');
		return false;
	}
	else if(chapter_check < 0){
		alert('<?=$Lang['itextbook']['NoOfChapterNotEnoughWarning'][0]?>' + (chapter_check*-1) + '<?=$Lang['itextbook']['NoOfChapterNotEnoughWarning'][1]?>');
		return false;
	}
	
	return true;
}

function submit_form(){
	var obj = document.form1;

	if(check_form()){
		obj.action = 'book_list_update.php';
		obj.submit();
		return true;
	}
	else{
		return false;
	}
}

function change_book_lang(lang){
	$('.book_lang').hide();
	$('.book_lang_'+lang).show();
}
</script>
<table width="100%">
	<tr><td><?=$objiTextbook->gen_tab_menu(2)?></td></tr>
	<tr><td><?=$objiTextbook->gen_admin_nav($page_ary)?></td></tr>
	<tr>
		<td>
			<form name="form1" method="POST">
				<div class="table_board">
					<table width="100%">
						<?=$html_message? "<tr><td align=\"right\">$html_message</td></tr>":""?>
						<tr>
							<td>
								<fieldset class="instruction_box">
									<legend class="instruction_title"><?=$Lang['itextbook']['Instruction']?></legend>
									<span>
										<ul>
											<li><?=$Lang['itextbook']['BookInstruction'][0]?></li>
											<li><?=$Lang['itextbook']['BookInstruction'][1]?></li>
											<li><?=$Lang['itextbook']['BookInstruction'][2]?></li>
										</ul>
									</span>
								</fieldset>
					  		</td>
						</tr>
						<tr>
							<td>
								<table width="100%">
									<col width="*"/>
									<col width="1%"/>
									<col width="90%"/>
									<tr>
										<td style="white-space: nowrap"><?=$Lang['itextbook']['BookName']?></td>
										<td>:</td>
										<td><input type="text" id="BookName" name="BookName" value="<?=$BookInfo['BookName']?>" maxlength="255" style="width:99%"/></td>
									</tr>
									
									<?php if($BookID) {?>
									<tr>
										<td style="white-space: nowrap"><?=$Lang['itextbook']['StudentsAssigned']?></td>
										<td>:</td>
										<td><?=$std_assigned?></td>
									</tr>
									<?php } ?>
									
									<tr>
										<td style="white-space: nowrap"><?=$Lang['itextbook']['AllowableNoOfChapters']?></td>
										<td>:</td>
										<td><?=$iTextbook_cfg[$Current_Code]['MaxChapters']?></td>
									</tr>
									
									<?php if(count($iTextbook_cfg[$Current_Code]['SpecialUnit'])){ ?>
										<tr>
											<td valign='top' style="white-space: nowrap"><?=$Lang['itextbook']['IncludeGiftChapter']?></td>
											<td valign='top'>:</td>
											<td valign='top'>
												<?php
													foreach($iTextbook_cfg[$Current_Code]['SpecialUnit'] as $special_chapter){
														echo "<input type='checkbox' id='special_unit_$special_chapter' name='special_unit[]' value='$special_chapter' ".(in_array($special_chapter, $selected_chapter_list)? "checked":"")." ".($IsLock? "onclick='return false'":"")."/>";
														echo " ";
														echo "<label for='special_unit_$special_chapter'>";
														
														foreach($iTextbook_cfg[$Current_Code]['BookLang'] as $book_lang){
															echo "<span class='book_lang book_lang_$book_lang' style='".($book_lang!=$BookInfo['BookLang']? "display:none":"")."'>".$Lang['itextbook']['Chapter'][$book_lang]."$special_chapter - ".$iTextbook_cfg[$Current_Code]['ChapterName'][$special_chapter][$book_lang]."</span>";
														}
														
														echo "</label><br/>";
													}
												?>
											</td>
										</tr>
									<?php } ?>
									
									<tr>
										<td style="white-space: nowrap"><?=$Lang['itextbook']['BookLang']?></td>
										<td>:</td>
										<td>
											<?php
												foreach($iTextbook_cfg[$Current_Code]['BookLang'] as $book_lang){
													echo "<input type='radio' id='book_lang_$book_lang' name='BookLang' value='$book_lang' onclick='change_book_lang(\"$book_lang\")' ".($BookInfo['BookLang']==$book_lang? "checked":"").">";
													echo " ";
													echo "<label for='book_lang_$book_lang'>".$Lang['itextbook']['Lang'][$book_lang]."</label>";
													echo " ";
												}
											?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspna="2">&nbsp;</td>
						</tr>
						<tr>
							<td>
								<?=$objiTextbook->gen_chapter_selection_list($Current_Code, $selected_chapter_list, $IsLock, false, false, $BookInfo['BookLang'])?>
					  		</td>
						</tr>
						<?php if($update_message){ ?>
						<tr>
							<td colspna="2">&nbsp;</td>
						</tr>
						<tr>
							<td colspna="2">
								<span class="tabletextremark"><?=$update_message?></span>
							</td>
						</tr>
						<?php } ?>
					</table>
			  		<p class="spacer"></p>
				</div>
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
				<input type="hidden" name="BookID" value="<?=$BookID?>"/>
			</form>
		</td>
	</tr>
	<tr>
		<td>
			<div class="edit_bottom">
				<p class="spacer"></p>
   	 				<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_save?>"   onclick="submit_form()"/>
    				<input type="button" class="formbutton" onmouseover="this.className='formbuttonon'" onmouseout="this.className='formbutton'" value="<?=$button_cancel?>" onclick="go_book_list()"/>
  				<p class="spacer"></p>
  			</div>
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>