<?php
//modifying By

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
$ClassID		  = isset($ClassID) && $ClassID!=''? $ClassID : $objiTextbook->get_default_class();

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$total_quota  = $objiTextbook->get_iTextbook_total_quota($Current_ModuleID);
$quota_used	  = $objiTextbook->get_iTextbook_total_used($Current_ModuleID, false);
$quota_left	  = $total_quota - $quota_used;

$NoOfAllowSelUnit = count($iTextbook_cfg[$Current_Code]['allowSelUnits']);

$objiTextbook->admin_auth($Current_ModuleID);

# Generate Page navigation array
$page_ary[] = array("display"=>$Lang['itextbook']['PurchaseRecord'], "link"=>"index.php?Current_ModuleID=".$Current_ModuleID);
$page_ary[] = array("display"=>$Lang['itextbook']['QuotaAllocation'], "link"=>"");

# Generate System Message after operation
$html_message = isset($msg)? $linterface->GET_SYS_MSG($msg) : "";

# Generate UI
$title = $iTextbook_cfg[$Current_Code]['book_name'];
$TAGS_OBJ[] = array($title,"");
$MODULE_OBJ = $objiTextbook->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>
<style>
.common_table_list_v30 tr.error td.start {border-top: 2px solid red; border-bottom: 2px solid red; border-left: 2px solid red;}
.common_table_list_v30 tr.error td.row   {border-top: 2px solid red; border-bottom: 2px solid red;}
.common_table_list_v30 tr.error td.end   {border-top: 2px solid red; border-bottom: 2px solid red; border-right: 2px solid red;}
</style>
<script>
var NoOfCol  			= 0;
var NoOfRow 			= 0;
var quota_left 			= <?=$quota_left?>;
var original_quota_left = <?=$quota_left?>;
var NoOfSpecialUnit 	= <?=count($iTextbook_cfg[$Current_Code]['SpecialUnit'])?>;

var allowSelUnit     = new Array(<?=$NoOfAllowSelUnit?>);
<?php
if($NoOfAllowSelUnit>0)
	echo "allowSelUnit[0] = 0;\n";
	
for($i=0;$i<$NoOfAllowSelUnit;$i++){
	echo "allowSelUnit[".($i+1)."] = ".$iTextbook_cfg[$Current_Code]['allowSelUnits'][$i].";\n";
}
?>

$(document).ready(function(){
	NoOfCol = $('.check_col').length + NoOfSpecialUnit;
	NoOfRow = $('.check_row').length;
	$('.lock').click(function(){return false;});
});

function check_row(row){
	var IsChecked = $('#check_row_' + row).attr('checked');
	
	quota_used = IsChecked? $('.NormalUnit.free.row_'+row+':not(:checked)').length : $('.NormalUnit.free.row_'+row+':checked').length * -1;
	
	if(is_quota_left(quota_used)){
		$('.NormalUnit.free.row_'+row+':checkbox').attr('checked', IsChecked);
		check_all_checking(row, 0);
	}
	else{
		$('#check_row_' + row).attr('checked', false);
		alert('<?=$Lang['itextbook']['NoQuotaLeft']?>');
	}
}

function check_column(col){
	var IsChecked = $('#check_column_' + col).attr('checked');
	
	quota_used = IsChecked? $('.NormalUnit.free.col_'+col+':not(:checked)').length : $('.NormalUnit.free.col_'+col+':checked').length * -1;
	
	if(is_quota_left(quota_used)){
		$('.NormalUnit.free.col_'+col+':checkbox').attr('checked', IsChecked);
		check_all_checking(0, col);
	}
	else{
		$('#check_column_' + col).attr('checked', false);
		alert('<?=$Lang['itextbook']['NoQuotaLeft']?>');
	}
}

function check_row_column(row, col){
	var IsChecked = $('.NormalUnit.row_' + row + '.col_' + col).attr('checked');
	
	quota_used = IsChecked? 1 : -1;
	
	if(is_quota_left(quota_used)){
		check_all_checking(row, col);
	}
	else{
		$('.NormalUnit.row_' + row + '.col_' + col).attr('checked', false);
		alert('<?=$Lang['itextbook']['NoQuotaLeft']?>');
	}
}

function check_special_unit(row){
	var IsChecked = $('.NormalUnit.row_' + row + ':checked').length > 0;
	$('.SpecialUnit.row_'+row).attr('checked', IsChecked);
}

function check_all_checking(row, col){
	/* Only check corresponding row and col specified in parameter */
	
	/* Row Checking */
	if(row > 0){
		check_special_unit(row);
		
		var row_checked = $('.row_' + row + ':checked').length;
		
		if(row_checked==NoOfCol)
			$('#check_row_' + row).attr('checked', true);
		else
			$('#check_row_' + row).attr('checked', false);
	}
	else{
		for(i=1;i<=NoOfRow;i++){
			check_special_unit(i);
			
			var row_checked = $('input.row_' + i + ':checked').length;
			
			if(row_checked==NoOfCol)
				$('#check_row_' + i).attr('checked', true);
			else
				$('#check_row_' + i).attr('checked', false);
		}
	}
	
	/* Column Checking */
	if(col > 0){
		var col_checked = $('.col_' + col + ':checked').length;
		
		if(col_checked==NoOfRow)
			$('#check_column_' + col).attr('checked', true);
		else
			$('#check_column_' + col).attr('checked', false);
	}
	else{
		for(i=1;i<=NoOfCol;i++){
			var col_checked = $('input.col_' + i + ':checked').length;
			
			if(col_checked==NoOfRow)
				$('#check_column_' + i).attr('checked', true);
			else
				$('#check_column_' + i).attr('checked', false);
		}
	}
}

/* If quota > 0, quota will be used  */
/* If quota < 0, quota will be added */
function is_quota_left(quota){
	var new_quota_left = quota_left - quota;
	if(new_quota_left >= 0){
		quota_left = new_quota_left;
		$('#quota_left').html(quota_left);
		
		var quota_released = quota_left - original_quota_left;
		if(quota_released >= 0) $('#quota_released').html(quota_released);
		
		return true;
	}
	else
		return false;
}

function check_form(obj){
	var global_result = true;
	
	/* Check No. of Student is larger than zero or not */
	if(obj.NoOfRecord.value <= 0)
		return false;
	
	/* Check No. of Units selected per Student */
	if(allowSelUnit.length > 0){
		for(i=1;i<=NoOfRow;i++){
			var SelectedUnitNo = $('.row_' + i + ':not(.SpecialUnit):checked').length;
			var result = false;
			
			for(j=0;j<allowSelUnit.length;j++){
				if(allowSelUnit[j]==SelectedUnitNo){
					result = true;
					break;
				}
			}
			
			if(result){
				$('#row_' + i).removeClass('error');
				global_result = global_result? true : false;
			}
			else{
				$('#row_' + i).addClass('error');
				global_result = false;
			}
		}
	}
	else
		global_result = true;
	
	if(!global_result){
		alert('<?=$Lang['itextbook']['UnitSelectedInvald']?>');
		return false;
	}
	
	/* Check No. of Units assigned */
	if(quota_left > original_quota_left){
		global_result = false;
		alert('<?=$Lang['itextbook']['YouHave']?> ' + (quota_left - original_quota_left) + ' <?=$Lang['itextbook']['QuotaReleasedNotReassign']?>');
		return false;
	}
		
	return global_result;
}

function assign_student_submit(){
	var obj = document.form1;
	
	if(check_form(obj)){
		obj.action = "assign_student_update.php";
		obj.submit();
	}
}

function change_class(){
	var obj = document.form1;
	
	if(confirm("<?=$Lang['itextbook']['ChangesWontSave']?>")){
		obj.action = "assign_student.php";
		obj.submit();
	}
	else
		$('#ClassID').val('<?=$ClassID?>');
}
</script>
<table width="100%">
	<tr>
		<td><?=$objiTextbook->gen_admin_nav($page_ary)?></td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td><?=$objiTextbook->gen_instruction_table('', $Current_Code, $quota_left, $total_quota)?></td>
	</td>
	<tr>
		<td>
			<form name="form1" method="POST" action="">
				<table width="100%">
					<tr>
						<td width="50%" align="left"><?=$objiTextbook->get_class_drop_down_list('ClassID', 'ClassID', $ClassID, 'onchange="change_class()"');?></td>
						<td width="50%" align="right"><?=$html_message?></td>
					</tr>
				</table>
				<div class="table_board" style='margin-top:5px'>
					<?=$objiTextbook->gen_assign_student_table($Current_ModuleID, $ClassID)?>
			  		<p class="spacer"></p>
			  		<span style='color:#666666'><img src='<?=$image_path?>/2009a/icon_tick_green.gif'> : <?=$Lang['itextbook']['Unit_Assigned_and_Cant_Modified']?></span>
				</div>
				<input type="hidden" name="Current_ModuleID" value="<?=$Current_ModuleID?>"/>
			</form>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="button" onclick="assign_student_submit()" value="<?=$Lang['Btn']['Submit']?>" class="formbutton">
		</td>
	</tr>
</table>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>