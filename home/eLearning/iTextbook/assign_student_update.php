<?php
//modifying By thomas

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$debug_mode = false;

$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
 
$ClassID  = isset($ClassID) && $ClassID!=''? $ClassID : $objiTextbook->get_default_class();
$std_unit = isset($std_unit) && is_array($std_unit)? $std_unit : array();

$sql_error_ary = array("Update"=>array(), "Delete"=>array(), "Assign"=>array());

$code 	  = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$std_list = $objiTextbook->get_class_student_with_assigned_module($Current_ModuleID, $ClassID);

$objiTextbook->Start_Trans();

foreach($std_list as $std_user_id=>$std_user_ary){
	$UnitAry = is_array($std_user_ary['Unit'])? $std_user_ary['Unit'] : array();
	$available_record_id = array();
	
	if($debug_mode){
		echo '<table border=\'1\'>';
		echo '	<tr>';
		echo '		<td>UserID</td>';
		echo '		<td>Unit</td>';
		echo '		<td>Cooldown Passed</td>';
		echo '		<td>Selected Prev.</td>';
		echo '		<td>Selected Now</td>';
		echo '		<td>SQL</td>';
		echo '	</tr>';
	}
	
	for($Unit=1;$Unit<=$iTextbook_cfg[$code]['TotalNoOfUnit'];$Unit++){
		$Is_Cooldown_passed    = $objiTextbook->Is_Cooldown_passed($iTextbook_cfg[$code]['cooldown'], $UnitAry[$Unit]['InputDate']);
		$Is_Unit_Selected_Prev = (isset($UnitAry[$Unit]) && is_array($UnitAry[$Unit]))? true : false;
		$Is_Unit_Selected_Now  = (isset($std_unit[$std_user_id][$Unit]) && $std_unit[$std_user_id][$Unit]==1)? true : false;
		
		if(!$Is_Cooldown_passed && $Is_Unit_Selected_Prev && !$Is_Unit_Selected_Now){
			$available_record_id[] = $UnitAry[$Unit]['RecordID'];
		}
	}
	
	/* Get records that need to selected */
	for($Unit=1;$Unit<=$iTextbook_cfg[$code]['TotalNoOfUnit'];$Unit++){
		$Is_Cooldown_passed    = $objiTextbook->Is_Cooldown_passed($iTextbook_cfg[$code]['cooldown'], $UnitAry[$Unit]['InputDate']);
		$Is_Unit_Selected_Prev = (isset($UnitAry[$Unit]) && is_array($UnitAry[$Unit]))? true : false;
		$Is_Unit_Selected_Now  = (isset($std_unit[$std_user_id][$Unit]) && $std_unit[$std_user_id][$Unit]==1)? true : false;
		$sql = '';
		
		if(!$Is_Cooldown_passed && !$Is_Unit_Selected_Prev && $Is_Unit_Selected_Now){
			if(count($available_record_id)>0){
				$RecordID = array_shift($available_record_id);
				$sql = "UPDATE
							INTRANET_MODULE_USER
						SET
							Unit = $Unit, DateModified = NOW()
						WHERE
							ModuleStudentID = $RecordID";
			}
			else{
				$sql = "INSERT INTO
							INTRANET_MODULE_USER
							(ModuleID, Unit, UserID, InputDate, DateModified)
						VALUES
							($Current_ModuleID, $Unit, $std_user_id, NOW(), NOW())";
			}
			if(!$debug_mode) $sql_error_ary['Update'] = $objiTextbook->db_db_query($sql);
		}
		
		if($debug_mode){
			echo '<tr>';
			echo '	<td>'.$std_user_id.'</td>';
			echo '	<td>'.$Unit.'</td>';
			echo '	<td>'.($objiTextbook->Is_Cooldown_passed($iTextbook_cfg[$code]['cooldown'], $UnitAry[$Unit]['InputDate'])? 'T':'<font style="color:red">F</font>').'</td>';
			echo '	<td>'.($Is_Unit_Selected_Prev? 'T('.$UnitAry[$Unit]['RecordID'].')':'<font style="color:red">F</font>').'</td>';
			echo '	<td>'.($Is_Unit_Selected_Now?  'T':'<font style="color:red">F</font>').'</td>';
			echo '	<td>'.$sql.'</td>';
			echo '</tr>';
		}
	}
	
	/* Delete thoes records remain in $available_record_id */
	if(count($available_record_id)>0){
		$sql = "DELETE FROM INTRANET_MODULE_USER WHERE ModuleStudentID IN (".implode(',', $available_record_id).")";
		if(!$debug_mode) $sql_error_ary['Delete'] = $objiTextbook->db_db_query($sql);
		
		if($debug_mode){
			echo '<tr>';
			echo '	<td colspan="6">'.$sql.'</td>';
			echo '</tr>';
		}
	}
	
	# Check No. Of Unit Assigned for each student
	if(is_array($iTextbook_cfg[$code]['allowSelUnits']) && count($iTextbook_cfg[$code]['allowSelUnits'])>0){
		$sql = "SELECT
					COUNT(*)
				FROM
					INTRANET_MODULE_USER
				WHERE
					ModuleID = $Current_ModuleID AND
					UserID = $std_user_id
					".(is_array($iTextbook_cfg[$code]['SpecialUnit']) && count($iTextbook_cfg[$code]['SpecialUnit'])>0? "AND Unit NOT IN (".implode(',',$iTextbook_cfg[$code]['SpecialUnit']).")" : "")."";
		$NoOfUnitAssigned = current($objiTextbook->returnVector($sql));
		$sql_error_ary['Assign'][] = $NoOfUnitAssigned==0 || in_array($NoOfUnitAssigned, $iTextbook_cfg[$code]['allowSelUnits'])? true : false;
	} 
	
	if($debug_mode) echo '</table>';
}

# Check total no of quotas left is valid or not
$total_quota = $objiTextbook->get_iTextbook_total_quota($Current_ModuleID);
$quota_used	 = $objiTextbook->get_iTextbook_total_used($Current_ModuleID, false);
$quota_left	 = $total_quota - $quota_used;


if((is_array($sql_error_ary['Update']) && in_array(false, $sql_error_ary['Update'])) || 
   (is_array($sql_error_ary['Delete']) && in_array(false, $sql_error_ary['Delete'])) ||
   (is_array($sql_error_ary['Assign']) && in_array(false, $sql_error_ary['Assign'])) ||
   $quota_left < 0){
	$objiTextbook->RollBack_Trans();
	$msg = 'update_failed';
}
else{
	$objiTextbook->Commit_Trans();
	$msg = 'update';
}

header('Location:assign_student.php?Current_ModuleID='.$Current_ModuleID.'&ClassID='.$ClassID.'&msg='.$msg);

?>