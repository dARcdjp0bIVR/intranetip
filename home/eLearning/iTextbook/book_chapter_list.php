<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('popup5.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
$BookID			  = isset($BookID) && $BookID!=''? $BookID : 0;

$Current_Code = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);

# Get Selected Book Info
$BookInfo 			   = $objiTextbook->get_book_info($BookID, true);
$selected_chapter_list = $BookInfo['Chapters'];

# Generate UI
$linterface->LAYOUT_START();
?>
<style>
.chapter_list_title {
    background-color: #EBEBEB;
    display: block;
    padding: 2px;
}

.chapter_list_title_name {
    float: left;
    font-weight: bold;
}

.chapter_list_chapter {
    display: block;
    padding: 2px;
    margin: 10px 5px;
}

.chapter_list_chapter a {
    background-color: #E7EDFA;
    cursor: default;
    padding: 2px;
    color: #2286C5;
}

.chapter_list_chapter a:hover,
.chapter_list_chapter a:visited {
    color: #2286C5;
}

.peer_within_group_board {
    border-collapse: separate;
    border-spacing: 0;
    width: 100%;
}
</style>
<div style="min-height:410px">
	<table width="100%">
		<tr>
			<td align="center">
				<strong><?=$BookInfo['BookName']?></strong>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?=$objiTextbook->gen_chapter_selection_list($Current_Code, $selected_chapter_list, $IsLock, true, true, $BookInfo['BookLang'])?>
			</td>
		</tr>
	</table>
</div>
<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>