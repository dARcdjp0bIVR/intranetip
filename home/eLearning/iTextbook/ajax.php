<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/iTextbookConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/iTextbook/libitextbook.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_auth();
intranet_opendb();

$linterface   = new interface_html('ies_admin_default.html'); // temporary use ies_admin_default.html as UI
$objiTextbook = new libitextbook();

$default_book 	  	  = current($objiTextbook->get_enabled_iTextbook());
$Current_ModuleID 	  = isset($Current_ModuleID) && $Current_ModuleID!=''? $Current_ModuleID : $default_book['ModuleID'];
$type			  	  = isset($type) && $type!=''? $type : '';
$DistributionNo   	  = isset($DistributionNo) && $DistributionNo!=''? $DistributionNo : 1;
$DistributionID	  	  = isset($DistributionID) && $DistributionID!=''? $DistributionID : 0;
$BookID			  	  = isset($BookID) && $BookID!=''? $BookID : 0;
$sys_msg		  	  = isset($sys_msg) && $sys_msg!=''? $sys_msg : '';
$DistributionUser_ary = isset($DistributionUser_ary) && $DistributionUser_ary!=''? $DistributionUser_ary : array();
$AssignedTeacher_ary  = isset($AssignedTeacher_ary) && $AssignedTeacher_ary!=''? $AssignedTeacher_ary : array();
 
$Current_Code 	  = $objiTextbook->get_iTextbook_code($Current_ModuleID);

$objiTextbook->admin_auth($Current_ModuleID);

switch($type){
	case 'Get_No_of_Books':
		$result = count($objiTextbook->get_books_info($iTextbook_cfg[$Current_Code]['module_code']));
	break;
	case 'Get_New_Distribution_Table':
		# Get No of License Left
		$LicenseLeft = $objiTextbook->get_distributions_quota_left($ModuleLicenseID, $Current_Code);
		
		if($LicenseLeft){
			$sql = "SELECT
						BookID
					FROM
						ITEXTBOOK_BOOK
					WHERE
						BookType = '".$iTextbook_cfg[$Current_Code]['module_code']."' AND
						Status   = '".$iTextbook_cfg['db_setting']['book']['status']['not_deleted']."'
					ORDER BY
						BookName";
			$BookID_ary = $objiTextbook->returnVector($sql);
			
			if(count($BookID_ary)>0){
				$BookID = current($BookID_ary);
				$BookID = $BookID? $BookID : 0;
			
				$sql = "INSERT INTO
							ITEXTBOOK_DISTRIBUTION
							(ModuleLicenseID, BookID, DateInput, DateModified, ModifiedBy)
						VALUES
							('$ModuleLicenseID', '$BookID', NOW(), NOW(), '$UserID')";
				
				if($objiTextbook->db_db_query($sql)){
					$DistributionID = $objiTextbook->db_insert_id();
					$DistributionUserAry = $objiTextbook->get_distributions_user_info($ModuleLicenseID, $DistributionID);
					$result = '1|=|'.$objiTextbook->gen_distribution_info_table($Current_Code, $DistributionNo, $DistributionUserAry);
				}
				else{
					$result = '0|=|Fail to create distribution';
				}
			}
			else{
				$result = '0|=|'.$Lang['itextbook']['NoBookCreated'];
			}
		}
		else{
			$result = '0|=|'.$Lang['itextbook']['NoMoreLicenseLeft'];
		}
	break;
	case 'Update_Distribution_Table_Book':
		$BookInfo = $objiTextbook->get_book_info($BookID, false);
		
		if(count($BookInfo)){
			$sql = "UPDATE
						ITEXTBOOK_DISTRIBUTION
					SET
						BookID = $BookID
					WHERE
						DistributionID = $DistributionID";
		}
		
		$result  = count($BookInfo) && $objiTextbook->db_db_query($sql)? $linterface->GET_SYS_MSG('', '<font color="#008000">'.$i_con_msg_update.' - '.$BookInfo['BookName'].' '.$Lang['itextbook']['is_distributed'].'</font>') : $linterface->GET_SYS_MSG('update_failed');
		$result .= "<script>
					SysMsgTimerObj_$DistributionNo = new SysMsgTimerObj('SysMsgTimerObj_$DistributionNo', $DistributionNo, 5);
					SysMsgTimerObj_$DistributionNo.StartTimer();
					</script>";
	break;
	case 'Reload_Distribution':
		# Get No of License Left
		$LicenseLeft = $objiTextbook->get_distributions_quota_left($ModuleLicenseID, $Current_Code);
		
		# Get Distribution Table
		$sys_msg  = $linterface->GET_SYS_MSG($sys_msg);
		$sys_msg .= "<script>
					 SysMsgTimerObj_$DistributionNo = new SysMsgTimerObj('SysMsgTimerObj_$DistributionNo', $DistributionNo, 5);
					 SysMsgTimerObj_$DistributionNo.StartTimer();
					 </script>";
		$Distribution_user_info = $objiTextbook->get_distributions_user_info($ModuleLicenseID, $DistributionID);
		$result = count($Distribution_user_info)? $objiTextbook->gen_distribution_info_table($Current_Code, $DistributionNo, $Distribution_user_info, $sys_msg) : "";
		
		$result = $LicenseLeft.'|=|'.$result;
	break;
	case 'Delete_Distribution':
		# Get ModuleCode and Distribution Input Date
		$sql = "SELECT 
		    		im.Code, id.DateInput
				FROM 
		    		ITEXTBOOK_DISTRIBUTION AS id INNER JOIN 
		    		INTRANET_MODULE_LICENSE AS iml ON id.ModuleLicenseID = iml.ModuleLicenseID INNER JOIN
			    	INTRANET_MODULE AS im ON iml.ModuleID = im.ModuleID
				WHERE
					id.DistributionID = $DistributionID";
		list($ModuleCode, $DateInput) = current($objiTextbook->returnArray($sql));
		
		if(!$objiTextbook->Is_Cooldown_passed($iTextbook_cfg[$ModuleCode]['cooldown'], $DateInput)){
			$objiTextbook->Start_Trans();
			
			$sql = "DELETE FROM ITEXTBOOK_DISTRIBUTION WHERE DistributionID = $DistributionID";
			$delete_result[] = $objiTextbook->db_db_query($sql);
			
			$sql = "DELETE FROM ITEXTBOOK_DISTRIBUTION_USER WHERE DistributionID = $DistributionID";
			$delete_result[] = $objiTextbook->db_db_query($sql);
			
			if(!in_array(false, $delete_result)){
				# Delete corresponding user in iTextbook special room
				# (function delete_user_from_iTextbook_special_room() defined in bottom of this page)
				delete_user_from_iTextbook_special_room($Current_Code);
				
				##sync to grammartouch central site
				if(strpos($Current_Code,'grammartouch') !== false){
					$objiTextbook->delete_grammartouch_distribution($DistributionID);
				}
				
				$objiTextbook->Commit_Trans();
				$result = "";
			}
			else{
				$objiTextbook->RollBack_Trans();
				$result = $linterface->GET_SYS_MSG('delete_failed');
			}
		}
		else{
			$result = $linterface->GET_SYS_MSG('delete_failed');
		}
	break;
	case 'Delete_Distribution_User':
		# Get ModuleCode and Distribution Input Date
		$sql = "SELECT 
		    		im.Code, id.DateInput
				FROM 
		    		ITEXTBOOK_DISTRIBUTION AS id INNER JOIN 
		    		INTRANET_MODULE_LICENSE AS iml ON id.ModuleLicenseID = iml.ModuleLicenseID INNER JOIN
			    	INTRANET_MODULE AS im ON iml.ModuleID = im.ModuleID
				WHERE
					id.DistributionID = $DistributionID";
		list($ModuleCode, $DateInput) = current($objiTextbook->returnArray($sql));
		
		if(!$objiTextbook->Is_Cooldown_passed($iTextbook_cfg[$ModuleCode]['cooldown'], $DateInput)){
			$objiTextbook->Start_Trans();
			//get UserID for grammartouch
			if(strpos($Current_Code,'grammartouch') !== false){
				$sql = "SELECT UserID FROM ITEXTBOOK_DISTRIBUTION_USER WHERE DistributionUserID IN ('".implode("','", $DistributionUser_ary)."')";
				$ChooseUserID = $objiTextbook->returnVector($sql);
			}
			$sql = "DELETE FROM ITEXTBOOK_DISTRIBUTION_USER WHERE DistributionUserID IN ('".implode("','", $DistributionUser_ary)."')";
			$result =$sql;
			if($objiTextbook->db_db_query($sql)){
				# Delete corresponding user in iTextbook special room
				# (function delete_user_from_iTextbook_special_room() defined in bottom of this page)
				delete_user_from_iTextbook_special_room($Current_Code);
				
				##sync to grammartouch central site
				if(strpos($Current_Code,'grammartouch') !== false){
					$objiTextbook->delete_grammartouch_user_from_distribution($ChooseUserID,$DistributionID);
				}
				$objiTextbook->Commit_Trans();
				$sys_msg = "delete";
				
			}
			else{
				$objiTextbook->RollBack_Trans();
				$sys_msg = "delete_failed";
			}
		}
		else
			$sys_msg = "delete_failed";
		
		# Get No of License Left
		$LicenseLeft = $objiTextbook->get_distributions_quota_left($ModuleLicenseID, $Current_Code);
		
		# Get Distribution Table
		$sys_msg  = $linterface->GET_SYS_MSG($sys_msg);
		$sys_msg .= "<script>
					 SysMsgTimerObj_$DistributionNo = new SysMsgTimerObj('SysMsgTimerObj_$DistributionNo', $DistributionNo, 5);
					 SysMsgTimerObj_$DistributionNo.StartTimer();
					 </script>";
		$Distribution_user_info = $objiTextbook->get_distributions_user_info($ModuleLicenseID, $DistributionID);
		$result = count($Distribution_user_info)? $objiTextbook->gen_distribution_info_table($Current_Code, $DistributionNo, $Distribution_user_info, $sys_msg) : "";
		
		$result = $LicenseLeft.'|=|'.$result;
	break;
	case 'Delete_Teacher':
		# Get iTextbook Special Room course_id
		$sql = "SELECT course_id FROM $eclass_db.course WHERE course_code = '$Current_Code' AND RoomType = 7";
		$course_id = current($objiTextbook->returnVector($sql));
		
		$lo = new libeclass($course_id);
		
		if(count($AssignedTeacher_ary)){
			$lo->eClassUserDel($AssignedTeacher_ary);
		}
	break;
}

function delete_user_from_iTextbook_special_room($Current_Code){
	global $eclass_db, $intranet_db, $objiTextbook;
	
	# Get iTextbook Special Room course_id
	$sql = "SELECT course_id FROM $eclass_db.course WHERE course_code = '$Current_Code' AND RoomType = 7";
	$course_id = current($objiTextbook->returnVector($sql));
				
	$lo = new libeclass($course_id);

	$sql = "SELECT
				iu.UserID, iu.UserEmail
			FROM
				$intranet_db.INTRANET_USER AS iu INNER JOIN
				$intranet_db.ITEXTBOOK_DISTRIBUTION_USER AS idu ON iu.UserID = idu.UserID INNER JOIN
				$intranet_db.ITEXTBOOK_DISTRIBUTION AS id ON idu.DistributionID = id.DistributionID INNER JOIN
				$intranet_db.INTRANET_MODULE_LICENSE AS iml ON id.ModuleLicenseID = iml.ModuleLicenseID INNER JOIN
				$intranet_db.INTRANET_MODULE AS im ON iml.ModuleID = im.ModuleID
			WHERE
				im.code = '$Current_Code'";
	$result_ary = $objiTextbook->returnArray($sql);
	
	$UserIDAry 	  = array();
	$UserEmailAry = array();
	for($i=0;$i<count($result_ary);$i++){
		$UserIDAry[]	= $result_ary[$i]['UserID'];
		$UserEmailAry[] = $result_ary[$i]['UserEmail'];
	}
				
	$sql = "SELECT
				uc.user_id
			FROM
				$eclass_db.user_course AS uc INNER JOIN
				$eclass_db.course AS c ON uc.course_id = c.course_id
			WHERE
				c.course_code = '$Current_Code' AND
				c.RoomType = 7 AND
				uc.memberType != 'T' AND
				uc.intranet_user_id NOT IN (".implode(",", $UserIDAry).") AND
				uc.user_email NOT IN ('".implode("','", $UserEmailAry)."')";
	$user_id = $objiTextbook->returnVector($sql);
	$lo->eClassUserDel($user_id);
}

echo $result;

intranet_closedb();
?>