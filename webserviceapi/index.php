<?php
# Editing by:

/*
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Modification of this file may affect different modules.                          !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Please check carefully and inform team leaders after modifying it.               !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

/*
 * 20190311 (Paul)
 * 		- Remove the line of broken chinese words under the function GetEBookList()
 * 20190306 (Thomas)
 *      - Fixed the problem of fail to applying addslash() to SessionID and ApiKey if they are array
 * 20181120 (Paul)
 * 		- Handle SessionID and ApiKey by addslash to prevent MySQL injection
 * 20170829 (Thomas)
 *      - Added $RequestMethod == 'GetPL2LessonPlanUsersUdid'
 * 20170516 (Siuwan)
 *      - Added $RequestMethod == 'IsPowerLessonCompatible'
 * 20170331 (Siuwan)
 * 		- Added $RequestMethod == 'GetEclassGroups / ValidateLessonPin', removed 'GetCourseInfo'
 * 20161013 (Pun)
 * 		- Added $RequestMethod == 'RemoveAuthToken'
 * 20160901 (Pun)
 * 		- Added $RequestMethod == 'RenewAuthToken'
 * 20160821 (Pun)
 * 		- Added $RequestMethod == 'GetCourseInfo'
 * 20160816 (Thomas)
 * 		- Added $RequestMethod == 'AppRelaunchLessonValidate'
 * 20160530	(Siuwan)
 * 		- Added $RequestMethod == 'GetLessonPlanUsersUdid'
 * 20150311	(Thomas)
 * 		- For $RequestMethod == 'Login' or $RequestMethod == 'LoginByQRCode', Add handling for LDAP Client
 * 20150106 (Jason)
 *  	- add new param 'reqtype' to support json request - use for PowerLesson Android App
 * 		Deploy	: ip.2.5.6.1.1.0
 * 20140311 (Ivan)
 * 		- added "GetSchooliCal" method logic
 */
$PATH_WRT_ROOT = "../";
include_once("../includes/libxml.php");
include_once("../includes/libmethod.php");
include_once("../includes/libdb.php");
include_once("../includes/libauth.php");
include_once("../includes/global.php");
//include_once("../includes/lib_homework.php");
//include_once("../includes/libRSAProcessor.php");
//include_once("../includes/libAES.php");
//include_once("../includes/big5_func/big5_func.inc");
include_once("../includes/libwebservice.php");
include_once("../includes/libeclassapiauth.php");
// include_once("../includes/settings_ischoolbag.php");
//@ini_set('memory_limit', -1);
//$xmlContent = $_REQUEST['xmlContent'];


if($reqtype == 'json')
{
	include_once($PATH_WRT_ROOT."includes/json.php");
	
	# get data
	$jsonContent = stripslashes($HTTP_RAW_POST_DATA);
//	echo $jsonContent;die();
	# init object 
	$jsonObj = new JSON_obj();
	
	# main
	if ($jsonContent != '') {
		
		$requestJsonAry = $jsonObj->decode($jsonContent);
		$xml = new LibXML();
		
		$OmitHeader = true; // omit header 'eClassResponse'
		$xmlContent = $xml->Array_To_XML($requestJsonAry, true);
		
		$xmlContent = '<?xml version="1.0" encoding="utf-8"?>'.$xmlContent;
//		debug_r($xmlContent);die();
	}
} else {
	$xmlContent = $HTTP_RAW_POST_DATA;
}
//echo "abcde";

// debug_r($_REQUEST);
$xmlContent = stripslashes($xmlContent);

if($xmlContent != '') {
	$xml = new LibXML();	
	$arr = $xml->XML_ToArray($xmlContent, 0);
	
	$eClassEncryptedRequest = false;
	$eClassEncryptedServiceError = false;
	$RequestingToken = false;
	
	
	
	/*
	//TEST utf8 -> Big5
	include_once("../lang/circular_lang.b5.php");			
	if($value = $arr['eClassRequest']['Request']['FileName'] == $i_title){
		echo "Converter works";
	}
	else{
		echo "Converter fails";
	}
	exit();
	*/		
		
	intranet_opendb();
	
	if(is_array($arr)) {
		
		$libauth = new libauth();
		$libwebservice = new libwebservice();
		
		# Detecting eClassEncryptedService Directives
		if ($arr['eClassEncryptedRequest']) {
			$eClassEncryptedRequest = true;
			$requestStream = $arr['eClassEncryptedRequest']['requestStream'];
			$requesterCPUID = $arr['eClassEncryptedRequest']['token'];
			$AES = new AES();
			if (isset($arr['eClassEncryptedRequest']['getToken']) && $arr['eClassEncryptedRequest']['getToken'] == 1) {
				$RequestingToken = true;
				$decryptedXML = base64_decode($requestStream);
				$RSAKeyType = new RSAKeyType();
				$XMLFile = $RSAKeyType->XMLFiles;
				$XMLString = $RSAKeyType->XMLString;
				$processor = new RSAProcessor();
				$processor->xmlRSAKey = $decryptedXML;
				$processor->type = $XMLString;
				$processor->init();
				
				$AES->AESGenerateKey($requesterCPUID, time());
				$AESKey = base64_encode($AES->AESKey);
				$AESIV = base64_encode($AES->AESIV);
				$requestToken = sha1($requesterCPUID.time());
				$libauth->SetAESKeyByUident($requesterCPUID, $requestToken, $AESKey, $AESIV);
				
				$PlainReturn = array();
				$PlainReturn['AES']['RequestToken'] = $requestToken;
				$PlainReturn['AES']['Key'] = $AESKey;
				$PlainReturn['AES']['IV'] = $AESIV;
				
				# 2011-03-23: handle GBK
				//$PlainReturn = iconv_array('Big5', 'UTF8', $PlainReturn);
				$PlainReturn = ischoolbag_iconv($PlainReturn, true);
				
				$PlainReturn = $xml->Array_To_XML($PlainReturn, true);
				// debug_r($PlainReturn);
				
				$encryptedReturn = array();
				$encryptedReturn['EncryptedResponse'] = $processor->encrypt($PlainReturn);
				if ($encryptedReturn['EncryptedResponse'])
					$encryptedReturn = $xml->Array_To_XML($encryptedReturn,true);
				else {
					$eClassEncryptedServiceError = true;
					$encryptedReturn = array();
					$encryptedReturn['Result']['Error']['ErrorCode'] = '400';
				}
				$returnContent = $xml->EncryptedXMLWrapper($encryptedReturn);
				// debug_r($returnContent);
			} else if (isset($arr['eClassEncryptedRequest']['token']) && isset($arr['eClassEncryptedRequest']['requestToken'])){
				$requestToken = $arr['eClassEncryptedRequest']['requestToken'];
				$AESKey = $libauth->GetAESKeyByUident($requesterCPUID, $requestToken);
				$AESIV = $libauth->GetAESIVByUident($requesterCPUID, $requestToken);
				
				$AES->AESKey = base64_decode($AESKey);
				$AES->AESIV = base64_decode($AESIV);
				// debug_r($requestStream);
				$decryptedXML = $AES->AESDecrypt($requestStream);
				// debug_r($decryptedXML);
				if ($decryptedXML) {
					$arr = $xml->XML_ToArray($decryptedXML, 0);
				} else {
					$encryptedReturn = array();
					$eClassEncryptedServiceError = true;
					$encryptedReturn['Result']['Error']['ErrorCode'] = '400';
					$encryptedReturn = $xml->Array_To_XML($encryptedReturn);
					$returnContent = $xml->EncryptedXMLWrapper($encryptedReturn);
				}
				
			} else {
				$encryptedReturn = array();
				$eClassEncryptedServiceError = true;
				$encryptedReturn['Result']['Error']['ErrorCode'] = '400';
				$encryptedReturn = $xml->Array_To_XML($encryptedReturn);
				$returnContent = $xml->EncryptedXMLWrapper($encryptedReturn);
			}
			
		}
		
		// EJ use Big 5 encoding	
		# 2011-03-23: handle GBK
		//$arr = iconv_array('UTF8', 'Big5', $arr);
		$arr = ischoolbag_iconv($arr, false);
	    if(isset($arr['eClassRequest']['SessionID']) && is_string($arr['eClassRequest']['SessionID'])){
			$arr['eClassRequest']['SessionID'] = addslashes($arr['eClassRequest']['SessionID']);
		}
		if(isset($arr['eClassRequest']["APIKey"]) && is_string($arr['eClassRequest']["APIKey"])){
			$arr['eClassRequest']["APIKey"] = addslashes($arr['eClassRequest']["APIKey"]);
		}
		if($arr['eClassRequest']) {
			$SessionID = $arr['eClassRequest']['SessionID']? $arr['eClassRequest']['SessionID'] : '';
			$RequestID = $arr['eClassRequest']['RequestID'];
			$RequestMethod = $arr['eClassRequest']['RequestMethod'];
			
			$method = new LibMethod();
			
			# Checking before call method
			if(trim($SessionID) != '') {				
				$UserID = $libauth->GetUserIDFromSessionKey(trim($SessionID));
				
				# Check SessionKey valid or not
				if($UserID != '') {
					
					if($RequestMethod == 'Logout'){
						$arr['eClassRequest']['Request']['UserID'] = $UserID;
						$Result = $method->callMethod($RequestMethod, $arr);
					}
					else{
						$Result = $method->callMethod($RequestMethod, $arr);
					}
					
					## 2011-08-04: log ws last request 
					$libwebservice->update_request_log_lastrequest($UserID, $xmlContent, $apikey = '');
					
				}else{
					$Result = '5';	
				}
			}elseif($RequestMethod == 'Login' || $RequestMethod == 'LoginByQRCode') {
				$RequestID = $method->Generate_RequestID();
				$Result = '';
				
				if (((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP') && $ldap_user_type_mode){
					# Different users using different LDAP dn string
					
					# Get User Type
					$sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE ".($RequestMethod=='LoginByQRCode'? "UserID='".$arr['eClassRequest']['Request']['UserID']."'":"UserLogin='".$arr['eClassRequest']['Request']['UserName']."'");
					$temp = $libauth->returnArray($sql,2);
					
					list($t_id, $t_user_type) = $temp[0];
					
					if (!is_array($temp) || sizeof($temp)==0 || $t_user_type == "" || $t_user_type < 1 || $t_user_type > 4)
					{
						$Result = '401';
					}
					else{
						switch ($t_user_type)
						{
							case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
							case 2: $special_ldap_dn = $ldap_student_base_dn; break;
							case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
							default: $special_ldap_dn = $ldap_teacher_base_dn;
						}
					}
				}
				
				# !!! should put the check api and validate license in earlier steps !!!
				# check the api key or the license is valid first before handling any requests
				if(empty($Result)){
					if ($arr['eClassRequest']["APIKey"])
					{
						$eclassApiAuth = new libeclassapiauth();
						if ($eclassApiAuth->VerifyAPIKey($arr['eClassRequest']["APIKey"]))
						{
							$arr['eClassRequest']['Request']["hasAPIKey"] = true;
							$Result = $method->callMethod($RequestMethod, $arr);
						}
						else
						{
							$Result = '402';
						}
					}
					else
					{
						$arr['eClassRequest']['Request']["hasAPIKey"] = false;
						$Result = $method->callMethod($RequestMethod, $arr);
					}
				}
				
				## 2011-08-04: log ws last login time
				if(is_array($Result)) {
					$libwebservice->update_request_log_login($Result['UserID']);
				}

			}else if(in_array($RequestMethod, array(
			    'AppQRCodeValidate',
			    'AppGetStudent',
			    'AppRelaunchLessonValidate',
			    'ValidateLessonPin',
			    'GetEclassGroups',
			    'RenewAuthToken',
			    'RemoveAuthToken',
			    'LoginCourseByLessonPin',
			    'LogoutCourseByAuthToken',
			    'IsPowerLessonCompatible'
			))){
				$RequestID = $method->Generate_RequestID();
				
				$eclassApiAuth = new libeclassapiauth();
				
				if ($arr['eClassRequest']["APIKey"] && $eclassApiAuth->VerifyAPIKey($arr['eClassRequest']["APIKey"]))
				{
					$arr['eClassRequest']['Request']["hasAPIKey"] = true;
					$Result = $method->callMethod($RequestMethod, $arr);
				}
				else
				{
					$Result = '402';
				}
			}elseif($RequestMethod == 'GetLatestVersion') {
				// $RequestID = $method->Generate_RequestID();
				$Result = $method->callMethod($RequestMethod, $arr);
			}elseif($RequestMethod == 'GetConfig') {				
				$Result = $method->callMethod($RequestMethod, $arr);
			}elseif($RequestMethod == 'AppValidate'){
				$Result = $method->callMethod($RequestMethod, $arr);
			}elseif($RequestMethod == 'GetSchooliCal'){
				if ($arr['eClassRequest']["APIKey"]) {
					$arr['eClassRequest']['Request']["APIKey"] = $arr['eClassRequest']["APIKey"];
					$Result = $method->callMethod($RequestMethod, $arr);
				}
				else {
					$Result = '402';
				}
			}elseif($RequestMethod == 'GetLessonPlanUsersUdid'){
				$Result = $method->callMethod($RequestMethod, $arr);
			}elseif($RequestMethod == 'GetPL2LessonPlanUsersUdid'){
			    $Result = $method->callMethod($RequestMethod, $arr);
			}else{
				$Result = '5';	
			}
			
			# if $Result is not array, it is the error code
			if(!is_array($Result)) {
				$Result = $method->GetErrorArray($Result);	
				
				$returnResult = 'N';
			}
			else{
				$returnResult = 'Y';
			}			
			
			$ReturnContent = array();
			
			if ($RequestMethod != 'GetSchooliCal') {
				$ReturnContent['RequestID'] = $RequestID;
			}
			
			## 2011-07-06: insert <ReturnResult>Y</ReturnResult> if there is no error
			$ReturnContent['ReturnResult'] = $returnResult;	
			
			
			# 2011-04-11: use server time to set lastSyncTime for ischoolbag client
			if ($RequestMethod != 'GetSchooliCal') {
				$ReturnContent['RequestTime'] = ischoolbag_getCurrentTimestamp();
			}
			$ReturnContent['Result'] = $Result;			
								
			// convert back to UTF8 encoding for sending back to client	
			# 2011-03-23: handle GBK		
			//$ReturnContent = iconv_array('Big5', 'UTF8', $ReturnContent);
			//$ReturnContent = ischoolbag_iconv($ReturnContent, true);							
								
			if($reqtype == 'json'){
				$returnContent = $jsonObj->encode($ReturnContent);
			} else {
				$returnContent = $xml->Array_To_XML($ReturnContent);
				
				# 2011-03-23: strip illegal xml characters
				$returnContent = ischoolbag_stripInvalidXml($returnContent);
				$returnContent = ischoolbag_iconv($returnContent, true);
			}
		}
		
		if ($eClassEncryptedRequest && !$RequestingToken) {
			$encryptedReturn = array();
			
			if (!$eClassEncryptedServiceError) {
				$encryptedReturn['EncryptedResponse'] = $AES->AESEncrypt($returnContent);
				$encryptedReturn = $xml->Array_To_XML($encryptedReturn,true);
				$returnContent = $xml->EncryptedXMLWrapper($encryptedReturn);
			}
		}
	}
	// exit;
	if($reqtype == 'json'){
		header('Content-Type: text/json; charset=utf-8');
	} else {
		header('Content-Type: text/xml;');
	}
		
	echo $returnContent;
}

function GetEBookList() {
	# Begin Henry Yuen (2010-05-03): NOTICE !!!! eBook is not yet implemented in EJ, data is hard-coded for demonstration purpose
	
		$ReturnContent = "<eClassResponse xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"><RequestID>req201005031272869058</RequestID><Result>";
		/*
		echo "<Book>";
		echo "<BookID>101</BookID>";
		echo "<Author>The Brothers Grimm</Author>";
		echo "<Title>Grimm's Household Tales</Title>";
		echo "<Preface>These fairy tales by brothers Grimm are based on the original 1884 translation \"Household Tales\" of Margaret Hunt. This text is based on the book \"Grimm's household tales with the author's notes.\" By Grimm Jakob Ludwig Karl. Translated by Margaret Hunt. This text includes ALL Grimm's fairy tales and 10 children's legends. The Margaret Hunt's translation is very true to the German original.</Preface>";
		echo "<CoverImageUrl>http://192.168.0.245:31001/eBook/101/bookcover.jpg</CoverImageUrl>";
		echo "<Publisher>George Bell and Sons, London</Publisher>";
		echo "<Source></Source>";
		echo "<DateModified></DateModified>";
		echo "</Book>";
		echo "<Book>";
		echo "<BookID>102</BookID>";
		echo "<Author>Mary Shelly</Author>";
		echo "<Title>Frankenstein</Title>";
		echo "<Preface>Frankenstein, set in Europe in the 1790's, begins with the letters of Captain Robert Walton to his sister. These letters form the framework for the story in which Walton tells his sister the story of Victor Frankenstein and his monster as Frankenstein told it to him. Walton set out to explore the North Pole. The ship got trapped in frozen water and the crew, watching around them, saw a giant man in the distance on a dogsled. Hours later they found Frankenstein and his dogsled near the ship, so they brought the sick man aboard. As he recovered, Frankenstein told Walton his story so that Walton would learn the price of pursuing glory at any cost.</Preface>";
		echo "<CoverImageUrl>http://192.168.0.245:31001/eBook/102/bookcover.jpg</CoverImageUrl>";
		echo "<Publisher></Publisher>";
		echo "<Source></Source>";
		echo "<DateModified></DateModified>";
		echo "</Book>";
		echo "<Book>";
		echo "<BookID>103</BookID>";
		echo "<Author>George Orwell</Author>";
		echo "<Title>Animal Farm</Title>";
		echo "<Preface>Old Major, the old boar on the Manor Farm, calls the animals on the farm for a meeting, where he compares the humans to parasites and teaches the animals a revolutionary song, \"Beasts of England.\" When Major dies three days later, two young pigs, Snowball and Napoleon, assume command and turn his dream into a philosophy. The animals revolt and drive the drunken and irresponsible Mr. Jones from the farm, renaming it \"Animal Farm.\"</Preface>";
		echo "<CoverImageUrl>http://192.168.0.245:31001/eBook/103/bookcover.jpg</CoverImageUrl>";
		echo "<Publisher></Publisher>";
		echo "<Source></Source>";
		echo "<DateModified></DateModified>";
		echo "</Book>";
		echo "<Book>";
		echo "<BookID>104</BookID>";
		echo "<Author>George Orwell</Author>";
		echo "<Title>Nineteen Eighty-Four</Title>";
		echo "<Preface>published in 1949, is a dystopian novel about the totalitarian regime of the Party, an oligarchical collectivist society where life in the Oceanian province of Airstrip One is a world of perpetual war, pervasive government surveillance, public mind control, and the voiding of citizens' rights. In the Ministry of Truth (Minitrue), protagonist Winston Smith is a civil servant responsible for perpetuating the Party's propaganda by revising historical records to render the Party omniscient and always correct, yet his meager existence disillusions him into rebellion against Big Brother, which leads to his arrest, torture, and conversion.</Preface>";
		echo "<CoverImageUrl>http://192.168.0.245:31001/eBook/104/bookcover.jpg</CoverImageUrl>";
		echo "<Publisher></Publisher>";
		echo "<Source></Source>";
		echo "<DateModified></DateModified>";
		echo "</Book>";
		*/
		$ReturnContent .= "<Book>";
		$ReturnContent .= "<BookID>12933</BookID>";
		$ReturnContent .= "<Author>Elbert Hubbard</Author>";
		$ReturnContent .= "<Title>Little Journeys to the Homes of the Great, Vol 1 of 14</Title>";
		$ReturnContent .= "<Preface>Elbert Hubbard is dead, or should we say, has gone on his last Little Journey to the Great Beyond. But the children of his fertile brain still live and will continue to live and keep fresh the memory of their illustrious forebear.</Preface>";
		$ReturnContent .= "<CoverImageUrl>".$ischoolbag_website."/eBook/12933/bookcover.jpg</CoverImageUrl>";
		$ReturnContent .= "<Publisher>Proofreading Team at http://www.pgdp.net</Publisher>";
		$ReturnContent .= "<Source></Source>";
		$ReturnContent .= "<DateModified></DateModified>";
		$ReturnContent .= "</Book>";
		$ReturnContent .= "<Book>";
		$ReturnContent .= "<BookID>19033</BookID>";
		$ReturnContent .= "<Author>Lewis Carroll</Author>";
		$ReturnContent .= "<Title>Alice in Wonderland</Title>";
		$ReturnContent .= "<Preface>It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures.The tale plays with logic in ways that have given the story lasting popularity with adults as well as children. It is considered to be one of the best examples of the \"literary nonsense\" genre, and its narrative course and structure have been enormously influential, especially in the fantasy genre.</Preface>";
		$ReturnContent .= "<CoverImageUrl>".$ischoolbag_website."/eBook/19033/bookcover.jpg</CoverImageUrl>";
		$ReturnContent .= "<Publisher>SAM'L GABRIEL SONS &amp; COMPANY</Publisher>";
		$ReturnContent .= "<Source></Source>";
		$ReturnContent .= "<DateModified></DateModified>";
		$ReturnContent .= "</Book>";
		/*
		echo "<Book><BookID>9990</BookID><Author>Lewis Carrol</Author><Title>Alice's Adventures in Wonderland</Title><Preface>Alice's Adventures in Wonderland (commonly shortened to Alice in Wonderland) is an 1865 novel written by English author Charles Lutwidge Dodgson under the pseudonym Lewis Carroll.  It tells the story of a girl named Alice who falls down a rabbit hole into a fantasy world populated by peculiar and anthropomorphic creatures.The tale plays with logic  in ways that have given the story lasting popularity with adults as well as children.  It is considered to be one of the best examples of the \"literary nonsense\" genre,  and its narrative course and structure have been enormously influential,  especially in the fantasy genre.</Preface><CoverImageUrl>http://192.168.0.245:31001/eBook/9990/bookcover.jpg</CoverImageUrl><Publisher></Publisher><Source></Source><DateModified></DateModified></Book>";*/
		
		return $ReturnContent;
	# End Henry Yuen (2010-05-03): NOTICE !!!! eBook is not yet implemented in EJ, data is hard-coded for demonstration purpose
	
}

// This function extends the use of iconv from string type to n-dimensional array type
function iconv_array($fr, $to, $arr){
	if(is_array($arr)){
		foreach($arr as $key => $value){						
			$arr[$key] = iconv_array($fr, $to, $arr[$key]);										
		}
	}
	else{		
		$arr = iconv($fr, $to, $arr);		
	}
	return $arr;	
} 

function ischoolbag_iconv($input, $serverToClient = true){
	
	# [IP2.5]: no need to convert encoding
	return $input;
	
	global $intranet_default_lang;
	if($intranet_default_lang == "gb"){
		$serverEncoding = 'GBK';
	}
	elseif($intranet_default_lang == "b5" or $intranet_default_lang == "en"){
		$serverEncoding = 'Big5';
	}
	$clientEncoding = 'UTF8';
	
	if($serverToClient){
		$fromEncoding = $serverEncoding;
		$toEncoding = $clientEncoding. '//IGNORE';
	}
	else{
		$fromEncoding = $clientEncoding;
		$toEncoding = $serverEncoding. '//IGNORE';
	}	
	if(is_array($input)){
		return iconv_array($fromEncoding, $toEncoding, $input);
	}
	else{
		return iconv($fromEncoding, $toEncoding, $input);
	}
}

function ischoolbag_stripInvalidXml($value)
{
    $ret = "";
    $current;
    if (empty($value)) 
    {
        return $ret;
    }
 
    $length = strlen($value);
    for ($i=0; $i < $length; $i++)
    {
        $current = ord($value{$i});
        if (($current == 0x9) ||
            ($current == 0xA) ||
            ($current == 0xD) ||
            (($current >= 0x20) && ($current <= 0xD7FF)) ||
            (($current >= 0xE000) && ($current <= 0xFFFD)) ||
            (($current >= 0x10000) && ($current <= 0x10FFFF)))
        {
            $ret .= chr($current);
        }
        else
        {           	     
            $ret .= " ";
        }
    }
    return $ret;
}

function ischoolbag_getCurrentTimestamp(){
	$offset = mktime(0, 0, 0, 1, 2, 1970) - gmmktime(0, 0, 0, 1, 2, 1970);
	return time() - $offset;
}
?>