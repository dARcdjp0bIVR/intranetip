<?php
// Editing by

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/json.php");


intranet_opendb();

if(!$sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem'] && !$sys_custom['eEnrolment']['SubmitAbsentLateRecordsToExternalSystem']) {
	intranet_closedb();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.b5.php");

$sql = "SELECT
a.*,
b.STRN,
CASE a.DayPeriod
   WHEN ".PROFILE_DAY_TYPE_WD." THEN '".$Lang['StudentAttendance']['DayTypeWD']."'
   WHEN ".PROFILE_DAY_TYPE_AM." THEN '".$Lang['StudentAttendance']['DayTypeAM']."'
   WHEN ".PROFILE_DAY_TYPE_PM." THEN '".$Lang['StudentAttendance']['DayTypePM']."'
   ELSE '".$Lang['General']['NotApplicable']."'
END as DayPeriod_txt,
CASE a.TargetStatus
   WHEN ".CARD_LEAVE_NORMAL." THEN '".$Lang['StudentAttendance']['RequestLeave']."'
   WHEN ".PROFILE_TYPE_ABSENT." THEN '".$Lang['StudentAttendance']['Absent']."'
   WHEN ".PROFILE_TYPE_LATE." THEN '".$Lang['StudentAttendance']['Late']."'
END as TargetStatus_txt,
IF(a.SessionFrom IS NULL, '".$Lang['General']['NotApplicable']."', a.SessionFrom) as SessionFrom_txt,
IF(a.SessionTo IS NULL, '".$Lang['General']['NotApplicable']."', a.SessionTo) as SessionTo_txt,
IF(a.EnrolGroupID!=0, g.Title,
 IF(a.EnrolEventID!=0, e.EventTitle, '".$Lang['General']['NotApplicable']."')) as Title
FROM STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD a
LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
LEFT OUTER JOIN INTRANET_ENROL_EVENTINFO e ON (a.EnrolEventID=e.EnrolEventID)
LEFT OUTER JOIN INTRANET_ENROL_GROUPINFO gi ON (a.EnrolGroupID=gi.EnrolGroupID)
LEFT OUTER JOIN INTRANET_GROUP g ON (g.GroupID=gi.GroupID)
WHERE a.Send='0' ";

$lc = new libcardstudentattend2();

$check_record_can_send = true;
if(isset($RecordIDs) && $RecordIDs != "") {
	$sql .= " AND a.RecordID IN (".$lc->Get_Safe_Sql_Query($RecordIDs).")";
	$check_record_can_send = false;
}

$sql .= " ORDER BY a.DateInput DESC";

$records = $lc->returnArray($sql);

//if(count($records) > 0) {
	$send_data = array();
	$send_data[] = array("校內編號", "日期", "全日/上午/下午/不適用", "活動名/不適用", "遲到/請假/曠課", "節數(由)", "節數(至)");
	$update_ids = array();
	$skip_ids_2 = array();
	$skip_ids_3 = array();
	if(count($records) > 0) {
		foreach ($records as $row) {

			if($check_record_can_send == true) {
				$sql = "SELECT * FROM STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD WHERE RecordID!='".$row['RecordID']."' AND StudentID='".$row['StudentID']."'
						AND RecordDate='".$row['RecordDate']."' AND DayPeriod='".$row['DayPeriod']."' AND DateInput>='".$row['DateInput']."' AND RecordID>'".$row['RecordID']."'";
				$temp_record = $lc->returnArray($sql);
				if(!empty($temp_record)) {
					$skip_ids_2[] = $row['RecordID'];
					continue;
				} else {
					$sql = "SELECT * FROM STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD WHERE RecordID!='".$row['RecordID']."' AND StudentID='".$row['StudentID']."'
						AND RecordDate='".$row['RecordDate']."' AND DayPeriod='".$row['DayPeriod']."' AND Send='1'";
					$temp_record = $lc->returnArray($sql);
					if(!empty($temp_record)) {
						$skip_ids_3[] = $row['RecordID'];
						continue;
					}
				}
			}
			
			$temp = array();
			$temp[] = $row['STRN'];
			$temp[] = date("d/m/Y", strtotime($row['RecordDate']));
			$temp[] = $row['DayPeriod_txt'];
			$temp[] = $row['Title'];
			$temp[] = $row['TargetStatus_txt'];
			$temp[] = $row['SessionFrom_txt'];
			$temp[] = $row['SessionTo_txt'];
			$send_data[] = $temp;
			$update_ids[] = $row['RecordID'];
		}
	}


	$url = $sys_custom['StudentAttendance']['SubmitAbsentLateRecordsToExternalSystem_API_URL'];

	$jsonObj = new JSON_obj();
	$jsonDataEncoded = $jsonObj->encode($send_data);

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
	$output = curl_exec($ch);
	curl_close($ch);

	if(count($skip_ids_2)){
		$sql = "UPDATE STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD SET Send='2', DateSend=now() WHERE RecordID IN (" . implode(',', $skip_ids_2) . ")";
		$lc->db_db_query($sql);
	}
	if(count($skip_ids_3)){
		$sql = "UPDATE STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD SET Send='3', DateSend=now() WHERE RecordID IN (" . implode(',', $skip_ids_3) . ")";
		$lc->db_db_query($sql);
	}
	if(count($update_ids)){
		$sql = "UPDATE STUDENT_CUST_SUBMITTED_ABSENT_LATE_RECORD SET Send='1', DateSend=now() WHERE RecordID IN (" . implode(',', $update_ids) . ")";
		$lc->db_db_query($sql);
	}
	
	echo $output;
//}

intranet_closedb();
?>