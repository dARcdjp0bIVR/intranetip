<?php
// using by 

/* Modify History
 *
Date: 2020-10-28
By: Ray
Details: add include schedule_task/update_merchant_account_quota.php

Date: 2020-10-23
By: Ray
Details: add include schedule_task/delete_group_message.php

Date: 2020-09-29
By: Cameron
Details: add include schedule_task/maillog_house_keeping.php [case #V197630]

Date: 2019-09-26
By: Carlos
Details: Remove the job script to download AlipayHK daily settlement csv, would tigger it on central payment server. 

Date: 2019-03-06
By: Carlos
Details: Setup the job script to download AlipayHK daily settlement csv and sync to ePayment.

Date: 2019-02-19
By: Carlos
Details: Added a marker to mark this job script has been run today to avoid duplicatedly run many times in one day. 
		 Can use parameter $force_run=1 to force run this job script.

Date: 2019-01-11
By: Carlos
Details: Included /schedule_task/imail_gamma/clean_imail_gamma_receipt_records.php

Date: 2018-04-12
By: Anna
Details: eNotice run before eLib+

Date: 2018-03-19 
By: Carlos
Details: Reset HKUSPH daily send data cronjob.

Date: 2018-03-05 
By: Anna
Details: added daily_send_return_date_push_message remind student/parent sign eNotice

Date: 2018-02-02 
By: Carlos
Details: added the script /schedule_task/backup_data.php to backup INTRANET_USER records daily.

Date: 2016-09-01
By: Henry HM
Details: Included for "hkusph_send_push_message" for HKUSPH

Date: 2016-05-18
By: Kenneth
Details: Included for "epcm_send_push_notification_reminder" for ePCM

Date: 2016-01-14
By: Paul
Details: Included "rmInactiveEbook" to remove expired book from server

Date: 2015-08-07
By: Henry
Details: Included "daily_send_overdue_notice_email" to remind reader to return book

Date: 2015-07-16
By: Carlos
Details: Added a query to clean up token records that older than 1 days.

Date: 2013-11-28
By: Henry
Details: Included "daily_send_return_date_email" to remind reader to return book

Date: 2013-10-07
By: Ivan
Details: Added data sync logic of eLib+ to the script

Date: 2013-09-26
By: YatWoon
Details: Add log for checking the script is run or not.
 
Date: 2013-09-26
By: Henry
Details: Included daily_update_reserved_status.php" to update the expired record of the reservation

Date: 2012-03-06
By: YatWoon
Details: Don't clear classname classnumber for Alumni user

Date: 2011-04-06
By: Marcus
Details: skip process for alumni group and campus tv group

Date: 2010-09-13
By: Kenneth Chung
Details: disable server time out limitation for the script as there is client had more than 1000 eClass class room to update

Date: 2010-09-07
By: Kenneth Chung
Details: add logic to translate student attendance class confirm information

Date: 2010-09-01
By: Kenneth Chung
Details: add logic to translate student attendance class special settings
*/

@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libcal.php");
include_once($PATH_WRT_ROOT."includes/libcalevent.php");
include_once($PATH_WRT_ROOT."includes/libcalevent2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_opendb();

$AcademicYearID = Get_Current_Academic_Year_ID();
$txt = "AcademicYearID === $AcademicYearID\n";
$db = new libgeneralsettings();
$luser = new libuser();
$lcalevent = new libcalevent2007();

$script_name = $_SERVER['SCRIPT_NAME'];
$today = date('Y-m-d');

if($force_run != '1'){
	$run_records = $db->Get_General_Setting('ScheduleTask',array("'$script_name'"));
	if(count($run_records)>0 && isset($run_records[$script_name]) && $run_records[$script_name] == $today){
		echo "$script_name has been run on $today.";
		intranet_closedb();
		return;
	}
}

// get current academic year in school
$sql = 'select 
					SettingValue 
				from 
					GENERAL_SETTING 
				where 
					Module = \'ScheduleTask\' 
					and 
					SettingName = \'CurrentAcademicYear\'';
$Temp = $db->returnArray($sql);
$CurrentAcademicYearInDB = $Temp[0][0];
$txt .= "CurrentAcademicYearInDB === $CurrentAcademicYearInDB\n";

if ($AcademicYearID != $CurrentAcademicYearInDB) {
	$db->Start_Trans();
	
	$sql = 'update INTRANET_USER set ClassName = NULL, ClassNumber = NULL where RecordType<4';
	$Result['Reset-INTRANET_USER'] = $db->db_db_query($sql);
	if ($Result['Reset-INTRANET_USER']) {
		$sql = 'UPDATE
						  YEAR_CLASS AS yc
						  INNER JOIN
						  YEAR_CLASS_USER as ycu
						  on yc.AcademicYearID = \''.$AcademicYearID.'\' and yc.YearClassID = ycu.YearClassID
						  INNER JOIN
						  INTRANET_USER AS u
						  on ycu.UserID = u.UserID
						SET u.ClassName = yc.ClassTitleEN, u.ClassNumber = ycu.ClassNumber
						where u.RecordType<4';
		$Result['RecalculateClassNameClassNumber'] = $db->db_db_query($sql);
		
		
		### Update eClass Student ClassName ClassNumber
		$sql = "Select UserEmail, ClassName, ClassNumber From INTRANET_USER Where RecordType = '2'";
		$StudentInfoArr = $db->returnArray($sql);
		$numOfStudent = count($StudentInfoArr);
		
		$lc = new libeclass();
		for($i=0; $i < $numOfStudent; $i++)
	    {
			list($thisStudentEmail, $thisClassName, $thisClassNumber) = $StudentInfoArr[$i];
			$lc->eClassUserUpdateClassNameIP($thisStudentEmail, $thisClassName, $thisClassNumber);
	    }
	    
	    
	    if ($Result['RecalculateClassNameClassNumber']) {
			$sql = 'insert into GENERAL_SETTING (
								Module,
								SettingName,
								SettingValue,
								DateInput
								)
							Values (
								\'ScheduleTask\',
								\'CurrentAcademicYear\',
								\''.$AcademicYearID.'\',
								NOW()
								)
							ON DUPLICATE KEY UPDATE 
								SettingValue = \''.$AcademicYearID.'\', 
								DateModified = NOW()';
			$Result['UpdateGeneralSetting'] = $db->db_db_query($sql);
		}
	}
	
	if (in_array(false,$Result)) {
		$ErrorMsg = mysql_error();
		echo 'Class Name/Number reset failed';
		$db->RollBack_Trans();
		
		$sql = 'insert into GENERAL_LOG (
							Process,
							Result,
							Remarks 
							)
						values (
							\'Check academic year change\',
							\'0\', 
							\''.$db->Get_Safe_Sql_Query($ErrorMsg).'\'
							)';
	}
	else {
		$AffectedRow = $db->db_affected_rows();
		echo 'Class Name/Number reset successfully';
		$db->Commit_Trans();
		
		$sql = 'insert into GENERAL_LOG (
							Process,
							Result,
							Remarks 
							)
						values (
							\'Check academic year change\',
							\'1\', 
							\'affected record: '.$AffectedRow.'\'
							)';
							
							
		### sync data to library management system
		$successAry = array();
		$successAry['synUser'] = $luser->synUserDataToModules();
		$successAry['synHoliday'] = $lcalevent->synEventToModules();
	}
	
	$db->db_db_query($sql);
	
	//ecomm
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	$lfile = new libfilesystem();
	
	$db->Start_Trans();
	
	$Result = array();
	$sql = "
		SELECT 
			GroupID,
			Title
		FROM 
			INTRANET_GROUP
		WHERE
			AcademicYearID = '$AcademicYearID'
	";
	$NewGroupList = $db->returnArray($sql);
	
	$PreviousYearID = Get_Previous_Academic_Year_ID();
	$sql = "
		SELECT 
			GroupID,
			Title
		FROM 
			INTRANET_GROUP
		WHERE
			AcademicYearID = '$PreviousYearID'
	";
	$OldGroupList = $db->returnArray($sql);
	
	$NewGroupList = BuildMultiKeyAssoc($NewGroupList,"Title","GroupID",1);
	
	# excluded campus tv and alumni group
	global $tv_bulletin_groupid, $alumni_GroupID;
	include_once($PATH_WRT_ROOT."plugins/alumni_conf.php");
	include_once($PATH_WRT_ROOT."plugins/tv_conf.php");
	
	for($i=0; $i<sizeof($OldGroupList); $i++)
	{
		list($GroupID, $Title) = $OldGroupList[$i];
		
		if(in_array($GroupID,array($tv_bulletin_groupid, $alumni_GroupID)))
			continue;
		
		if($NewGroupList[$Title]=='')
			continue;
			
		$GroupIDMapping[$GroupID] = $NewGroupList[$Title];
		
		$txt .= "\n\n\n\n---GroupIDMaaping---\n";
		$txt .="$GroupID\n";
	}
	
	$eCommRelatedTable = array(
		"INTRANET_FILE",
		"INTRANET_FILE_FOLDER",
		"INTRANET_GROUPANNOUNCEMENT",
		"INTRANET_BULLETIN",
		"INTRANET_ANNOUNCEMENT",
		"INTRANET_HOUSE",
		"INTRANET_GROUPRESOURCE"
	);

	foreach((array)$GroupIDMapping as $OldGroupID => $NewGroupID)
	{
		$FailSql = '';
		foreach($eCommRelatedTable as $table)
		{ 
			if($table == 'INTRANET_ANNOUNCEMENT')
				$GroupID = 'OwnerGroupID';
			else
				$GroupID = 'GroupID';
				
			$sql = "
				UPDATE
					$table
				SET
					$GroupID = '$NewGroupID'
				WHERE
					$GroupID = '$OldGroupID'
			";
			
			if($Result[$OldGroupID."->".$NewGroupID] = $db->db_db_query($sql))
				$AffectedRow++;
			else
				$FailSql .= "Failed:\n".$sql."\n";
		}
		
		$old_file_path = $PATH_WRT_ROOT."file/group/g$OldGroupID";
		if(file_exists($old_file_path))
		{
			$new_file_path = $PATH_WRT_ROOT."file/group/g$NewGroupID";
			
			if(file_exists($new_file_path))
				$lfile->folder_content_copy($old_file_path,$new_file_path);
			else
				rename($old_file_path, $new_file_path);
		}
		
		$txt.= "$OldGroupID => $NewGroupID\n";
		if($FailSql)
			$txt.= $FailSql;
		
	}

	
	$success = $lfile->file_write($txt,$PATH_WRT_ROOT."file/eCommConv2.txt");
	
	
	if (in_array(false,$Result)) {
		$ErrorMsg = mysql_error();
		echo 'Renew eComm GroupID failed';
		$db->RollBack_Trans();
		
		$sql = 'insert into GENERAL_LOG (
							Process,
							Result,
							Remarks 
							)
						values (
							\'Renew eComm GroupID\',
							\'0\', 
							\''.$db->Get_Safe_Sql_Query($ErrorMsg).'\'
							)';
	}
	else {
//		$AffectedRow = $db->db_affected_rows();
		echo 'Renew eComm GroupID successfully';
		$db->Commit_Trans();
		
		$sql = 'insert into GENERAL_LOG (
							Process,
							Result,
							Remarks 
							)
						values (
							\'Renew eComm GroupID\',
							\'1\', 
							\'affected record: '.$AffectedRow.'\'
							)';
	}
	$db->db_db_query($sql);
	
	//ecomm

	### Class History Start ###
	$SuccessClassHistoryArr = array();
	$db->Start_Trans();
	
	### Get all current student class info
	$sql = "Select
					ycu.UserID as StudentID,
					yc.ClassTitleEN as ClassName,
					ycu.ClassNumber,
					ay.YearNameEN as AcademicYearName,
					yc.YearClassID,
					yc.AcademicYearID
			From
					YEAR_CLASS_USER as ycu
					Inner Join
					YEAR_CLASS as yc
					On (ycu.YearClassID = yc.YearClassID)
					Inner Join
					ACADEMIC_YEAR as ay
					On (yc.AcademicYearID = ay.AcademicYearID)
			Where
					yc.AcademicYearID = '".$AcademicYearID."'
			";
	$StudentClassInfoArr = $db->returnArray($sql);
	$numOfStudentClassInfo = count($StudentClassInfoArr);
	
	for ($i=0; $i<$numOfStudentClassInfo; $i++)
	{
		$thisStudentID 			= $StudentClassInfoArr[$i]['StudentID'];
		$thisClassName 			= $db->Get_Safe_Sql_Query($StudentClassInfoArr[$i]['ClassName']);
		$thisClassNumber 		= $StudentClassInfoArr[$i]['ClassNumber'];
		$thisAcademicYearName 	= $db->Get_Safe_Sql_Query($StudentClassInfoArr[$i]['AcademicYearName']);
		$thisYearClassID 		= $StudentClassInfoArr[$i]['YearClassID'];
		$thisAcademicYearID 	= $StudentClassInfoArr[$i]['AcademicYearID'];
		
		$sql = "Select RecordID From PROFILE_CLASS_HISTORY Where UserID = '$thisStudentID' And AcademicYearID = '$thisAcademicYearID'";
		$thisResultArr = $db->returnArray($sql);
		$thisRecordID = $thisResultArr[0]['RecordID'];
		
		if ($thisRecordID != '')
		{
			# update
			$sql = "Update 
						PROFILE_CLASS_HISTORY 
					Set
						ClassName = '$thisClassName', ClassNumber = '$thisClassNumber', DateModified = now(), YearClassID = '$thisYearClassID', AcademicYear = '$thisAcademicYearName', AcademicYearID = '$thisAcademicYearID'
					Where
						RecordID = '$thisRecordID'
					";
			$SuccessClassHistoryArr['Update'][$thisStudentID] = $db->db_db_query($sql);
		}
		else
		{
			# insert
			$sql = "Insert Into PROFILE_CLASS_HISTORY
						(UserID, ClassName, ClassNumber, AcademicYear, DateInput, DateModified, YearClassID, AcademicYearID)
					Values
						('$thisStudentID', '$thisClassName', '$thisClassNumber', '$thisAcademicYearName', now(), now(), '$thisYearClassID', '$thisAcademicYearID')
					";
			$SuccessClassHistoryArr['Insert'][$thisStudentID] = $db->db_db_query($sql);
		}
	}
	
	if (in_array(false, $SuccessClassHistoryArr)) {
		$ErrorMsg = mysql_error();
		echo '<br />Class History Update Failed';
		$db->RollBack_Trans();
		
		$sql = 'insert into GENERAL_LOG (
							Process,
							Result,
							Remarks 
							)
						values (
							\'Update_Class_History_In_Schedule_Task\',
							\'0\', 
							\''.$db->Get_Safe_Sql_Query($ErrorMsg).'\'
							)';
	}
	else {
		$AffectedRow = $db->db_affected_rows();
		echo '<br />Class History Update Success';
		$db->Commit_Trans();
		
		$msg = "Updated ".count($SuccessClassHistoryArr['Update'])." record(s) and inserted ".count($SuccessClassHistoryArr['Insert'])." record(s)."; 
		$sql = 'insert into GENERAL_LOG (
							Process,
							Result,
							Remarks 
							)
						values (
							\'Update_Class_History_In_Schedule_Task\',
							\'1\', 
							\''.$msg.'\'
							)';
	}
	
	$db->db_db_query($sql);
	
	### Class History End ###
	
	### Student Attendance Start ###
	$db->Start_Trans();
	// get previous academic years classIDs
	$PreviousYearID = Get_Previous_Academic_Year_ID();
	$sql = "select 
						ClassTitleEN,
						YearClassID 
					from 
						YEAR_CLASS 
					where 
						AcademicYearID = '".$PreviousYearID."'
				 ";
	$PrevClassList = $db->returnArray($sql);
	
	$sql = "select 
						ClassTitleEN,
						YearClassID 
					from 
						YEAR_CLASS 
					where 
						AcademicYearID = '".$AcademicYearID."'
				 ";
	$CurClassList = $db->returnArray($sql);
	$CurClassList = build_assoc_array($CurClassList);
	
	$sql = "show tables like 'CARD_STUDENT_DAILY_CLASS_CONFIRM_%'";
	$ClassConfirmTable = $db->returnVector($sql);
	for ($i=0; $i< sizeof($PrevClassList); $i++) {
		list($ClassTitle,$ClassID) = $PrevClassList[$i];
		
		if ($CurClassList[$ClassTitle] != "") {
			$sql = "update CARD_STUDENT_TIME_SESSION_REGULAR set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_TIME_SESSION_REGULAR:".$ClassTitle] = $db->db_db_query($sql);
			$sql = "update CARD_STUDENT_TIME_SESSION_DATE set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_TIME_SESSION_DATE:".$ClassTitle] = $db->db_db_query($sql);
			$sql = "update CARD_STUDENT_SPECIFIC_DATE_TIME set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_SPECIFIC_DATE_TIME:".$ClassTitle] = $db->db_db_query($sql);
			$sql = "update CARD_STUDENT_CLASS_SPECIFIC_MODE set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_CLASS_SPECIFIC_MODE:".$ClassTitle] = $db->db_db_query($sql);
			$sql = "update CARD_STUDENT_CLASS_PERIOD_TIME set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_CLASS_PERIOD_TIME:".$ClassTitle] = $db->db_db_query($sql);
			$sql = "update CARD_STUDENT_CLASS_SPECIFIC_MODE set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_CLASS_SPECIFIC_MODE:".$ClassTitle] = $db->db_db_query($sql);
			for ($j=0; $j< sizeof($ClassConfirmTable); $j++) {
				$sql = "update ".$ClassConfirmTable[$j]." set 
									ClassID = '".$CurClassList[$ClassTitle]."' 
								where 
									ClassID = '".$ClassID."'";
				$ResultStudentAttend["Update:".$ClassConfirmTable[$j].":".$ClassTitle] = $db->db_db_query($sql);
			}
		}
	}
	
	if (in_array(false,$ResultStudentAttend)) {
		$db->RollBack_Trans();
	}
	else {
		$db->Commit_Trans();
	}
	### End of Student Attendance ###

	include_once($intranet_root."/schedule_task/delete_group_message.php");
}

### clean token records that are older than 1 days
$sql_clean_token = "DELETE FROM INTRANET_SECURETOKEN WHERE UNIX_TIMESTAMP() > Time+86400";
$db->db_db_query($sql_clean_token);

#### Add log for checking the script is run or not.
$sql = 'insert into GENERAL_SETTING (
								Module,
								SettingName,
								SettingValue,
								DateInput
								)
							Values (
								\'ScheduleTask\',
								\'RunAddonDateTime\',
								NOW(),
								NOW()
								)
							ON DUPLICATE KEY UPDATE 
								SettingValue = NOW(), 
								DateModified = NOW()';
			$db->db_db_query($sql);

$sql = "insert into GENERAL_SETTING (
								Module,
								SettingName,
								SettingValue,
								DateInput
								)
							Values (
								'ScheduleTask',
								'$script_name',
								'$today',
								NOW()
								)
							ON DUPLICATE KEY UPDATE 
								SettingValue = '$today', 
								DateModified = NOW()";
			$db->db_db_query($sql);
			
intranet_closedb();


##### Backup INTRANET_USER records daily
if(file_exists($intranet_root."/schedule_task/backup_data.php")){
	include_once($intranet_root."/schedule_task/backup_data.php");
}

##### eNotice
if($plugin['notice'] && $plugin['eClassApp']){
    include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/notice/daily_send_return_date_push_message.php");
    include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/notice/daily_send_overdue_notice_push_message.php");
}

##### ePCM
if($plugin['ePCM']){
    include_once($PATH_WRT_ROOT."/schedule_task/ePCM/epcm_send_push_notification_reminder.php");
    include_once($PATH_WRT_ROOT."/schedule_task/ePCM/epcm_clear_unsaved_file.php");
    
}

if($plugin['library_management_system']){
    ##### eLib+
    // debug_pr($PATH_WRT_ROOT);
    include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/daily_update_reserved_status.php");
    include_once($PATH_WRT_ROOT."/home/library_sys/management/circulation/daily_send_return_date_email.php");
    include_once($PATH_WRT_ROOT."/home/library_sys/management/circulation/daily_send_return_date_push_message.php");
    include_once($PATH_WRT_ROOT."/home/library_sys/management/circulation/daily_send_overdue_notice_email.php");
    include_once($PATH_WRT_ROOT."/home/library_sys/management/circulation/daily_send_overdue_notice_push_message.php");
    include_once($PATH_WRT_ROOT."/addon/script/ebook/rmInactiveEbook.php"); // Cron Job to deal with expired book.
}

if($plugin['imail_gamma']){
	include_once($intranet_root."/schedule_task/imail_gamma/clean_imail_gamma_receipt_records.php");
}

##### HKUSPH
if($sys_custom['eClassApp']['HKUSPH']){
	intranet_opendb();
	//include_once($PATH_WRT_ROOT."/schedule_task/hkusph/hkusph_send_push_message.php");
	include_once($PATH_WRT_ROOT."/includes/libcrontab.php");
	$crontab = new libcrontab();
	$db = new libgeneralsettings();
	
	$http_protocol = (checkHttpsWebProtocol()?"https":"http")."://";
	$domain_and_port = $_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	$site = $http_protocol.$domain_and_port;
	$script_name = "/schedule_task/hkusph/hkusph_send_push_message.php";
	$script = $site.$script_name;
	
	// remove duplicated jobs which is set as localhost
	if($_SERVER['SERVER_NAME'] == '127.0.0.1'){
		$crontab->removeJob("http://".$domain_and_port.$script_name);
		$crontab->removeJob("https://".$domain_and_port.$script_name);
	}
	$crontab->removeJob($script);
	if($_SERVER['SERVER_NAME'] != '127.0.0.1'){
		$crontab->setJob('0', '18', '*', '*', '*', $script);
	}
	
	// send data cronjob
	$script_name = "/schedule_task/hkusph.php";
	$script = $site.$script_name;
	// remove duplicated jobs which is set as localhost
	if($_SERVER['SERVER_NAME'] == '127.0.0.1'){
		$crontab->removeJob("http://".$domain_and_port.$script_name);
		$crontab->removeJob("https://".$domain_and_port.$script_name);
	}
	$crontab->removeJob($script);
	
	$hkusph_setting_keys = array("'HKUSPH_JobStatus'","'HKUSPH_JobExecutionTime'");
	$hkusph_settings = $db->Get_General_Setting('StudentAttendance',$hkusph_setting_keys);
	if($hkusph_settings['HKUSPH_JobStatus'] == '1'){
		$time = $hkusph_settings['HKUSPH_JobExecutionTime'];
		if($time == ''){
			$time = '00:00';
		}
		$time_parts = explode(':',$time);
		$hour = $time_parts[0];
		$minute = $time_parts[1];
		if($_SERVER['SERVER_NAME'] != '127.0.0.1'){
			$crontab->setJob($minute, $hour, '*', '*', '*', $script);
		}
	}
	intranet_closedb();
}

// Setup the job script to download AlipayHK daily settlement csv and sync to ePayment
if($sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['TopUpEWallet']['Alipay']){
	include_once($PATH_WRT_ROOT."/includes/libcrontab.php");
    $crontab = new libcrontab();
    
    $http_protocol = (checkHttpsWebProtocol()?"https":"http")."://";
	$domain_and_port = $_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	$site = $http_protocol.$domain_and_port;
	$script_name = "/schedule_task/alipayhk_daily_settlement.php";
	$script = $site.$script_name;
	
	// remove duplicated jobs which is set as localhost
	//if($_SERVER['SERVER_NAME'] == '127.0.0.1'){
	//	$crontab->removeJob("http://".$domain_and_port.$script_name);
	//	$crontab->removeJob("https://".$domain_and_port.$script_name);
	//}
	$crontab->removeJob($script);
	//if($_SERVER['SERVER_NAME'] != '127.0.0.1'){
//		$crontab->setJob('0', '23', '*', '*', '*', $script);
	//}
}

include_once($intranet_root."/schedule_task/maillog_house_keeping.php");


include_once($intranet_root."/schedule_task/update_merchant_account_quota.php");

?>