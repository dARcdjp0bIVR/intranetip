<?php
// using:
/*
 *  purpose:    remove mail log files and directories in following folders which are older than half a year
 *              - file/mailDeleteLog
 *              - file/maillog
 *              - file/mailStructurelog
 *
 *              run on first day of each month
 *
 *  2020-09-29 Cameron
 *      - create this file [case #V197630]
 */

$PATH_WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");


intranet_opendb();
$li = new libdb();
$lf = new libfilesystem();


$cutoffDate = date('Y-m-d', strtotime(' - 6 months'));      // house keep record for half a year

$sql = 'SELECT RunDate FROM GENERAL_LOG WHERE Process LIKE "remove old mail%" ORDER BY RunDate DESC LIMIT 1';
$hasRunBefore = $li->returnResultSet($sql);

// first time to run or first day of a month
if ((count($hasRunBefore)==0) || (date('j') === '1')) {
    $mailLogDirAry = array();
    $mailLogDirAry[] = "maillog";
    $mailLogDirAry[] = "mailDeleteLog";
    $mailLogDirAry[] = "mailStructurelog";

    foreach($mailLogDirAry as $mailLogDir) {
        $fullMailLogDir = $intranet_root."/file/".$mailLogDir;
        if (is_dir($fullMailLogDir)) {
            $result = array();

            $userFolderList = $lf->return_folder($fullMailLogDir);
            if (count($userFolderList)) {
                foreach ((array) $userFolderList as $userFolder) {

                    $userFileList = $lf->return_files($userFolder);

                    if (($userFileList != false) && count($userFileList)) {
                        foreach((array)$userFileList as $userFile) {

                            $fileName = $lf->file_name($userFile);      // file name without dot and extension

                            if ($fileName < $cutoffDate) {
                                $result[] = $lf->file_remove($userFile);
                            }
                        }
                    }


                    $userFileList = $lf->return_files($userFolder);
                    if (count($userFileList) == 0) {        // no more file in the directory, remove the directory
                        $result[] = $lf->folder_remove($userFolder);
                    }
                }
            }

            if (count($result)) {
                $logResult = in_array(false,$result) ? 0 : 1;
                $remark = "deleted ". count($result) . " file(s) and directory(ies) ";
                $sql = "INSERT INTO GENERAL_LOG (Process, Result, Remarks) VALUES ('remove old $mailLogDir', $logResult, '$remark')";
                $li->db_db_query($sql);
//debug_pr($sql);
            }
        }
    }
}

intranet_closedb();

?>