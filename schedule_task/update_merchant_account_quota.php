<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_opendb();

$libdb = new libdb();
$current_day = date('d');
$last_day_of_month = date('t', strtotime('now'));


if($current_day == $last_day_of_month) {
	$year = date('Y');
	$month = date('m');
	$next_year = $year;
	$next_month = $month + 1;
	if ($next_month > 12) {
		$next_year++;
		$next_month = 1;
	}

	$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month'";
	$rs = $libdb->returnArray($sql);
	foreach ($rs as $temp) {
		$AccountID = $temp['AccountID'];
		$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$next_year' AND Month='$next_month' AND AccountID='$AccountID'";
		$exists_rs = $libdb->returnArray($sql);
		if (count($exists_rs) == 0) {
			$new_quota = $temp['Quota'];
			$sql = "INSERT INTO PAYMENT_MERCHANT_ACCOUNT_QUOTA SET Quota='$new_quota', DateInput=now(), ModifiedDate=now(), ModifiedBy='0', Year='$next_year', Month='$next_month', AccountID='$AccountID'";
			$libdb->db_db_query($sql);
		}
	}
}

