<?php
// editing by 
$is_standalonely_run = $_SERVER['REQUEST_URI'] == '/schedule_task/imail_gamma/clean_imail_gamma_receipt_records.php';
if($is_standalonely_run){
	$PATH_WRT_ROOT = "../../";
}
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_opendb();


if($plugin['imail_gamma'])
{
	$IMap = new imap_gamma(true);
	
	$days_in_trash = $IMap->days_in_trash;
	$days_in_spam = $IMap->days_in_spam;
	
	if(!is_numeric($days_in_trash) || !is_numeric($days_in_spam))
	{
		echo 'Please set the number of days to clean trash and spam at Admin Console.<br />';
		intranet_closedb();
		return;
	}
	
	$days_in_trash = intval($days_in_trash);
	$days_in_spam = intval($days_in_spam);
	
	$seconds_in_trash = $days_in_trash * 24 * 60 * 60;
	$seconds_in_spam = $days_in_spam * 24 * 60 * 60;
	
	$libdb = new libdb();
	
	$current_ts = time();
	
	$sql = 'SELECT 
				RecordID,
				UNIX_TIMESTAMP(TrashDate) as TrashTimestamp,
				RecordType  
			FROM 
				INTRANET_IMAIL_RECEIPT_STATUS
			WHERE 
				TrashDate IS NOT NULL ';
	$result_array = $libdb->returnArray($sql);
	
	$delete_trash_record_array = array();
	$delete_spam_record_array = array();
	for($i=0;$i<sizeof($result_array);$i++)
	{
		list($record_id,$trash_timestamp,$record_type) = $result_array[$i];
		
		if($record_type==1)// 1 = Trash
		{
			if($current_ts >= ($trash_timestamp + $seconds_in_trash))
			{
				$delete_trash_record_array[] = $record_id;
			}
		}else if($record_type==2) // 2 = Junk or Spam
		{
			if($current_ts >= ($trash_timestamp + $seconds_in_spam))
			{
				$delete_spam_record_array[] = $record_id;
			}
		}
	}
	
	if(sizeof($delete_trash_record_array)>0)
	{
		$libdb->Start_Trans();
		$sql = 'DELETE FROM 
					INTRANET_IMAIL_RECEIPT_STATUS 
				WHERE RecordID IN ('.implode(',',$delete_trash_record_array).') ';
		$success = $libdb->db_db_query($sql);
		if($success){
			$libdb->Commit_Trans();
			echo sizeof($delete_trash_record_array).' receipt(s) of trash records have been deleted successfully.<br />';
		}
		else{
			$libdb->RollBack_Trans();
			echo 'Failed to delete '.sizeof($delete_trash_record_array).' receipt(s) of trash records.<br />';
		}
	}
	
	if(sizeof($delete_spam_record_array)>0)
	{
		$libdb->Start_Trans();
		$sql = 'DELETE FROM 
					INTRANET_IMAIL_RECEIPT_STATUS 
				WHERE RecordID IN ('.implode(',',$delete_spam_record_array).') ';
		$success = $libdb->db_db_query($sql);
		if($success){
			$libdb->Commit_Trans();
			echo sizeof($delete_spam_record_array).' receipt(s) of spam records have been deleted successfully.<br />';
		}
		else{
			$libdb->RollBack_Trans();
			echo 'Failed to delete '.sizeof($delete_spam_record_array).' receipt(s) of spam records.<br />';
		}
	}
	
	if(sizeof($delete_trash_record_array)==0 && sizeof($delete_spam_record_array)==0)
	{
		echo 'No receipt records need to be deleted.<br />';
	}
}else
{
	echo 'iMail Gamma is not activated.<br />';
}

//intranet_closedb();
?>