<?php
// Editing by 
/*
 * Job script to download AlipayHK daily settlement csv and sync to ePayment. 
 * Default would check and download yesterday's csv and process it. 
 */
/*
 * 2019-09-25 Carlos: Log result to DB table PAYMENT_JOB_LOG as run cron job on 127.0.0.1 cannot log to client site's file system. 
 * 2019-08-12 Carlos: Changed to get settlement records by each merchant account, and charge handling fee for each account independently.
 */
set_time_limit(10*60);
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/json.php");

function writeToLog($log_content)
{
	global $file_path;
	
	$log_file = $file_path."/file/alipay_daily_settlement_log";
	$file_handle = fopen($log_file,"a+");
	fwrite($file_handle,$log_content);
	fclose($file_handle);
}

$use_alipay = $sys_custom['ePayment']['Alipay'] || $sys_custom['ePayment']['TopUpEWallet']['Alipay'];

$output = '';

if(!$use_alipay)
{
	if(!$use_alipay){
		$output .= "Not using AlipayHK.<br />\n";
	}
	echo $output;
	writeToLog($output);
	exit;
}

$cur_timestamp = time();
$interval = 10 * 1000; // 10 seconds make a dummy output to prevent script time out
function preventTimeout()
{
	global $cur_timestamp, $interval;
	$new_timestamp = time();
	if($new_timestamp > ($cur_timestamp+$interval)){
		$cur_timestamp = $new_timestamp;
		echo ' ';
		flush();
	}
}

function flushOutput()
{
	flush();
}

//outputHeaderCharset();

$target_date = isset($_GET['target_date']) && $_GET['target_date']!=''? $_GET['target_date'] : date('Ymd',time()-86400); // yesterday
$silence_mode = isset($_GET['silence_mode']) && $_GET['silence_mode'] == 1;
$is_debug = $_REQUEST['is_debug'] == 1 && !$silence_mode;

if(!$silence_mode){
	$x = "[BEGIN][".date('Y-m-d H:i:s')."]<br />\n";
	$output .= $x;
	echo $x;
	flushOutput();
}

$cur_sessionid = md5(session_id());
$cur_time = date("Y-m-d H:i:s");
$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
//$exclude_fields = array(5,7,8,9,11,12);

intranet_opendb();

$lpayment = new libpayment();
$libJson = new JSON_obj();

$sql = "CREATE TABLE IF NOT EXISTS PAYMENT_JOB_LOG(
			LogID int(11) not null auto_increment,
			LogTime datetime not null,
			LogUserID int(11) not null default 0,
			ScriptPath text,
			LogResult mediumtext,
			PRIMARY KEY(LogID),
			INDEX IdxLogTime(LogTime),
			INDEX IdxLogUserID(LogUserID)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";
$lpayment->db_db_query($sql);


$merchant_accounts = $lpayment->getMerchantAccounts(array('ServiceProvider'=>'ALIPAY'));
$merchant_account_size = count($merchant_accounts);

for($i=0;$i<$merchant_account_size;$i++)
{
	$merchant_account = $merchant_accounts[$i]['MerchantAccount'];
	$handling_fee = $merchant_accounts[$i]['HandlingFee'];
	$merchant_account_id = $merchant_accounts[$i]['AccountID'];
	$service_provider = $merchant_accounts[$i]['ServiceProvider'];
	
	$result = $lpayment->getAlipaySettlementData($target_date,$merchant_account, $service_provider);
	
	$json_str = base64_decode($result['data']);
	$output .= $json_str;
	
	$data = $libJson->decode($json_str);
	
	//debug_pr($data);
	
	preventTimeout();
	
	if(count($data) > 0)
	{   
	    $imported_temp_records_success = $lpayment->importPrepareAlipayTransactionRecordsToTempTable($data, $cur_sessionid, $threshold_date, $cur_time, 'alipay');
	    preventTimeout();
	    if($imported_temp_records_success){
	    	$result_ary = $lpayment->importProcessAlipayTransactionRecords($cur_sessionid,'alipay');
	    	if(count($result_ary) && $handling_fee != '' && $handling_fee > 0){
	    		// charge handling fee
	    		$last_settlement_time = $data[count($data)-1][7];
	    		$ts = strtotime($last_settlement_time);
				$existing_logs = $lpayment->getOverallTransactionLogs(array('TransactionType'=>12,'RelatedTransactionID'=>$merchant_account_id,'TransactionStartTime'=>date('Y-m-d 00:00:00',$ts),'TransactionEndTime'=>date('Y-m-d 23:59:59',$ts)));
	    		if(count($existing_logs)==0) // check existing added handling fee on that date for the merchant account, avoid duplicated charge handling fee
	    		{
		    		$insert_handling_fee_result = $lpayment->upsertOverallTransactionLog(array('TransactionType'=>12,'Amount'=>$handling_fee,'TransactionTime'=>$last_settlement_time,'RelatedTransactionID'=>$merchant_account_id,'Details'=>$Lang['ePayment']['AlipayHandlingFee'],'InputBy'=>0));
		    		if($insert_handling_fee_result){
		    			$x = 'Charged handling fee $'.$handling_fee." with settlement time $last_settlement_time.<br />\n";
		    			$output .= $x;
		    			echo $x;
		    			flushOutput();
		    		}
	    		}
	    	}
	    	if(!$silence_mode){
	    		$x = "Processed ".count($result_ary)." transaction records in $target_date.<br />\n";
	    		$output .= $x;
	    		echo $x;
	    		flushOutput();
	    	}
	    }
	}else{
		if(!$silence_mode){
			$x = "No data in $target_date.<br />\n";
			$output .= $x;
			echo $x;
			flushOutput();
		}
	}

}



if(!$silence_mode){
	$x = "\n<br />[END][".date('Y-m-d H:i:s')."]<br />\n";
	$output .= $x;
	echo $x;
}

writeToLog($output);

$sql = "INSERT INTO PAYMENT_JOB_LOG (LogTime,LogUserID,ScriptPath,LogResult) VALUES (NOW(),0,'".$lpayment->Get_Safe_Sql_Query($_SERVER['REQUEST_URI'])."','".$lpayment->Get_Safe_Sql_Query($output)."')";
$lpayment->db_db_query($sql);

intranet_closedb();
?>