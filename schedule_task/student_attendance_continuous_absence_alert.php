<?php
// Editing by 
/*
 * 2019-07-05 (Ray):    Added send to admin, send to teacher
 * 2019-02-08 (Carlos): Improved to compare student absence results with previous result individually.  
 * 2018-05-07 (Carlos): Created.
 */
set_time_limit(86400);
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

if(!$plugin['attendancestudent']){
	intranet_closedb();
	exit;
}

echo "[BEGIN][".date('Y-m-d H:i:s')."] Student Attendance - Find continuous absence students.<br />\n";

$lc = new libcardstudentattend2();
$fcm = new form_class_manage();
$webmail = new libwebmail();
$jsonObj = new JSON_obj();

$GeneralSettings = $lc->GetLibGeneralSettings();

$settings = $lc->Settings;

$absent_days = $settings['ContinuousAbsenceAlertAbsentDays'];
if($absent_days == '' || $absent_days <= 0){
	intranet_closedb();
	echo "Missing absence days setting.<br />\n";
	echo "\n<br />[END][".date('Y-m-d H:i:s')."]";
	exit;
}


$send_to_admin = ($settings['ContinuousAbsenceAlertJobSendToAdmin'] == 1) ? true : false;
$send_to_admin_all = ($settings['ContinuousAbsenceAlertJobSendToAdminAll'] == 1) ? true : false;
$send_to_admin_ids = (strlen($settings['ContinuousAbsenceAlertJobSendToAdminIds']) > 0) ? explode(',', $settings['ContinuousAbsenceAlertJobSendToAdminIds']) : array();
$send_to_teacher = ($settings['ContinuousAbsenceAlertJobSendToTeacher'] == 1) ? true : false;
$send_to_teacher_all = ($settings['ContinuousAbsenceAlertJobSendToTeacherAll'] == 1) ? true : false;
$send_to_teacher_ids = (strlen($settings['ContinuousAbsenceAlertJobSendToTeacherIds']) > 0) ? explode(',', $settings['ContinuousAbsenceAlertJobSendToTeacherIds']) : array();


if($settings['ContinuousAbsenceAlertJobDateRangeAcademicYear'] == 1) {
	$academic_year_startdate = date("Y-m-d", getStartOfAcademicYear());
	//$academic_year_enddate = date("Y-m-d",getEndOfAcademicYear());
	$today = date("Y-m-d");
} else {
	$academic_year_startdate = $settings['ContinuousAbsenceAlertJobDateRangeStartDate'];
	$today = $settings['ContinuousAbsenceAlertJobDateRangeEndDate'];
}

$start_date = $academic_year_startdate;
$end_date = $today;

$cur_time = time();
$interval = 30 * 1000; // 30 seconds make a dummy output to prevent script time out

$students = $fcm->Get_Active_Student_List();
$student_size = count($students);

function preventTimeout()
{
	global $cur_time, $interval;
	$new_time = time();
	if($new_time > ($cur_time+$interval)){
		$cur_time = $new_time;
		echo ' ';
		flush();
	}
}

function transformTo1DArray($ary)
{
	$result_ary = array();
	for($i=0;$i<count($ary);$i++){
		if(is_array($ary[$i])){
			$tmp_ary = transformTo1DArray($ary[$i]);
			$result_ary = array_values(array_merge($result_ary,$tmp_ary));
		}else{
			$result_ary[] = $ary[$i];
		}
	}
	
	return $result_ary;
}

preventTimeout();

$absence_students = array();

for($i=0;$i<$student_size;$i++){
	
	$student_id = $students[$i]['UserID'];
	$student_data = $lc->calculateContinuousAbsentStudent($student_id, $start_date, $end_date, $absent_days);
	$absent_date_size = count($student_data['AbsentDates']);
	if($absent_date_size>0){
		$student_data['YearClassID'] = $students[$i]['YearClassID'];
		$absence_students[] = $student_data;
	}
	
	preventTimeout();
}

if(count($absence_students)>0){
	//debug_pr("absence_students");
	//debug_pr($absence_students);
	
	//$hashed_string = md5(serialize($absence_students));
	//$setting_name = 'ContinuousAbsenceAlertHashedResult';
	//$settings = $GeneralSettings->Get_General_Setting('StudentAttendance',array("'$setting_name'"));
	
	// Get last time saved json result
	$setting_name = 'ContinuousAbsenceAlertJsonResult';
	$settings = $GeneralSettings->Get_General_Setting('StudentAttendance',array("'$setting_name'"));
	
	$prevStudentIdToDates = array(); // Construct a student id to absent dates array
	if(count($settings)>0 && $settings[$setting_name] != ''){
		
		$previous_result = $jsonObj->decode($settings[$setting_name]);
		//debug_pr("previous_result");
		//debug_pr($previous_result);
		for($i=0;$i<count($previous_result);$i++){
			$prevStudentIdToDates[$previous_result[$i]['UserID']] = $previous_result[$i]['AbsentDates'];
		}
	}
	
	//if(count($settings) == 0 || (count($settings)>0 && $settings[$setting_name]!=$hashed_string))
	//{
		$absence_students_to_process = array();
		$title = $Lang['StudentAttendance']['ContinuousAbsenceAlertEmailTitle'];
		$content = "<p>".str_replace('<!--DAY-->',$absent_days,$Lang['StudentAttendance']['ContinuousAbsenceAlertEmailContent']).":</p>";
		$email_content_header = $content;
		for($i=0;$i<count($absence_students);$i++){
			$user_id = $absence_students[$i]['UserID'];
			$class_name = Get_String_Display($absence_students[$i]['ClassName'],1);
			$class_number = Get_String_Display($absence_students[$i]['ClassNumber'],1);
			$student_name = Get_String_Display($absence_students[$i]['StudentName'],1);
			$absent_dates = $absence_students[$i]['AbsentDates'];
			$one_d_absent_dates = transformTo1DArray($absent_dates);
			
			if(isset($prevStudentIdToDates[$user_id])){
				$one_d_prev_absent_dates = transformTo1DArray($prevStudentIdToDates[$user_id]);
				$prev_dates_csv = implode(',',$one_d_prev_absent_dates);
				$dates_csv = implode(',',$one_d_absent_dates);
				if($prev_dates_csv == $dates_csv){
					// if no change on absent dates compared with last time, then no need to include this student
					continue;
				}
			}
			
			$absence_students_to_process[] = $absence_students[$i];
			$student_content = '';
			$student_content.= "<p>$class_name($class_number) $student_name ";
			$sep = "";
			for($j=0;$j<count($absent_dates);$j++){
				$student_content .= $sep."[".implode(', ',$absent_dates[$j])."]";
				$sep = ", ";
			}
			$student_content.= "</p>";

			$absence_students_to_process_content[] = $student_content;

			$content .= $student_content;
		}
		echo $content;
		
		preventTimeout();
		
		if(count($absence_students_to_process) > 0)
		{
			//debug_pr("absence_students_to_process");
			//debug_pr($absence_students_to_process);
			$email_to_ids = array();
			$class_teacher_ids = array();
			$admin_user_ids = array();
			$teacher_receive_content = array();
			for($i=0;$i<count($absence_students_to_process);$i++){
				$class_id = $absence_students_to_process[$i]['YearClassID'];
				$yc = new year_class($class_id,false,true);
				$class_teacher_list = $yc->ClassTeacherList;
				if(count($class_teacher_list)>0){
					$temp = Get_Array_By_Key($class_teacher_list,'UserID');
					$class_teacher_ids = array_merge($class_teacher_ids, $temp);

					foreach($temp as $teacher_uid) {
						$teacher_receive_content[$teacher_uid][] = $absence_students_to_process_content[$i];
					}
				}

				preventTimeout();
			}

			/*$year_class_ids = array_values(array_unique(Get_Array_By_Key($absence_students_to_process,'YearClassID')));
			for($i=0;$i<count($year_class_ids);$i++){
				$yc = new year_class($year_class_ids[$i],false,true);
				$class_teacher_list = $yc->ClassTeacherList;
				if(count($class_teacher_list)>0){
					$temp = Get_Array_By_Key($class_teacher_list,'UserID');
					$class_teacher_ids = array_merge($class_teacher_ids, $temp);

					foreach($temp as $teacher_uid) {
						$teacher_receive_content[$teacher_uid][] = $absence_students_to_process_content[$i];
					}
				}
				
				preventTimeout();
			}*/


			$class_teacher_ids = array_values(array_unique($class_teacher_ids));

			if($send_to_teacher == true && count($class_teacher_ids)>0) {
				foreach($class_teacher_ids as $teacher_uid) {
					$send_content = $email_content_header.implode('', $teacher_receive_content[$teacher_uid]);
					$send_to_teacher = false;
					if($send_to_teacher_all == true) {
						$send_to_teacher = true;
					} else if(count($send_to_teacher_ids) > 0) {
						if(in_array($teacher_uid, $send_to_teacher_ids) == true) {
							$send_to_teacher = true;
						}
					}

					if($send_to_teacher == true) {
						$webmail->sendModuleMail(array($teacher_uid), $title, $send_content);
						preventTimeout();
					}
				}
			}
			/*
			if(count($class_teacher_ids)>0){
				$email_to_ids = array_merge($email_to_ids,$class_teacher_ids);
			}
			*/
			$sql = "SELECT 
						DISTINCT u.UserID 
					FROM ROLE as r 
					INNER JOIN ROLE_RIGHT as rr ON rr.RoleID=r.RoleID 
					INNER JOIN ROLE_MEMBER as rm ON rm.RoleID=r.RoleID 
					INNER JOIN INTRANET_USER as u ON u.UserID=rm.UserID 
					WHERE u.RecordStatus='1' AND u.RecordType=1 AND rr.FunctionName='eAdmin-StudentAttendance' ";
			$admin_user_ids = $lc->returnVector($sql);
			
			preventTimeout();

			if($send_to_admin == true && count($admin_user_ids)>0) {
				if($send_to_admin_all == true) {
					$email_to_ids = array_merge($email_to_ids,$admin_user_ids);
				} else if(count($send_to_admin_ids) > 0) {
					$email_to_ids = array_merge($email_to_ids,array_intersect($send_to_admin_ids, $admin_user_ids));
				}
			}


			/*
			if(count($admin_user_ids)>0){
				$email_to_ids = array_merge($email_to_ids,$admin_user_ids);
			}
			*/
			if(count($email_to_ids)>0){
				$email_to_ids = array_values(array_unique($email_to_ids));
				//debug_pr($email_to_ids);
				
				$webmail->sendModuleMail($email_to_ids,$title,$content);
				preventTimeout();
			}
		}
		
		//$new_settings = array($setting_name=>$hashed_string);
		//$GeneralSettings->Save_General_Setting('StudentAttendance',$new_settings);
		
		$json_result = $jsonObj->encode($absence_students);
		$new_settings = array($setting_name=>$json_result);
		$GeneralSettings->Save_General_Setting('StudentAttendance',$new_settings);
	//}
}

intranet_closedb();

echo "\n<br />[END][".date('Y-m-d H:i:s')."]  Student Attendance - Find continuous absence students.\n";
?>