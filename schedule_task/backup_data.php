<?php
// Editing by 
/*
 * This script is to daily backup INTRANET_USER records, because many data lose cases happened.
 * 2019-06-14 Carlos: Rename backup file name from INTRANET_USER__YMDhms to IU__YMDhms
 */
//$PATH_WRT_ROOT = "../";
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libfilesystem.php");

if($_REQUEST['Debug'] == 1){
	echo "[".date('Y-m-d H:i:s')." BEGIN BACKUP]<br />\n";
}

//if(! @mysql_ping() ){
intranet_opendb();
//}

$li = new libdb();
$lf = new libfilesystem();

$backup_dir = $file_path.'/file/backup';
$backup_dir_htaccess = $backup_dir.'/.htaccess';

$lf->folder_new($backup_dir);

if(!file_exists($backup_dir)){
	if($_REQUEST['Debug'] == 1){
		echo "[".date('Y-m-d H:i:s')." HALT BACKUP - CANNOT CREATE DIR]<br />\n";
	}
	//exit;
	return;
}

if(!file_exists($backup_dir_htaccess)){
	$fh = fopen($backup_dir_htaccess,'w');
	if($fh){
		fwrite($fh, "Order allow,deny\n");
		fwrite($fh, "Deny from all\n");
		fclose($fh);
	}
}


// rename old backup files from INTRANET_USER__YMDhms to IU__YMDhms
$cmd = "find '$backup_dir/' -maxdepth 1 -type f -name 'INTRANET_USER__*'";
$cmd_result = trim(shell_exec($cmd));
$existing_backup_files = explode("\n",$cmd_result);
$existing_backup_files_size = count($existing_backup_files);
for($i=0;$i<$existing_backup_files_size;$i++){
	$old_file_path = $existing_backup_files[$i];
	$new_file_path = str_replace('INTRANET_USER__','IU__',$existing_backup_files[$i]);
	$rename_cmd = "mv '$old_file_path' '$new_file_path'";
	shell_exec($rename_cmd);
}



// Due to the cronjob would run several times daily, check if today has already backuped, only make one backup per day is enough.
$cmd = "find '$backup_dir/' -maxdepth 1 -type f -name 'IU__".date('Ymd')."*'";
$cmd_result = trim(shell_exec($cmd));
$existing_today_backup_files = explode("\n",$cmd_result);
$existing_today_backup_files = array_values(array_diff($existing_today_backup_files,array('')));
if(count($existing_today_backup_files)>0){
	if($_REQUEST['Debug'] == 1){
		echo "[".date('Y-m-d H:i:s')." HALT BACKUP - ALREADY HAS TODAY BACKUP]<br />\n";
	}
	return;
}


$sql = "SELECT * FROM INTRANET_USER";
$records = $li->returnResultSet($sql);
$record_size = count($records);

if($record_size == 0){
	if($_REQUEST['Debug'] == 1){
		echo "[".date('Y-m-d H:i:s')." HALT BACKUP - NO DATA]<br />\n";
	}
	//exit;
	return;
}

$backup_file = $backup_dir.'/IU__'.date('YmdHis');

$fh = fopen($backup_file,'w');
if($fh){
	$header_fields = array_keys($records[0]);
	$line = encrypt_string(implode("\t",$header_fields));
	fwrite($fh,$line."\n");
	for($i=0;$i<$record_size;$i++){
		$line = encrypt_string(str_replace(array(chr(13),chr(10)),"",implode("\t",$records[$i])));
		fwrite($fh,$line."\n");
	}
	fclose($fh);
	if($_REQUEST['Debug'] == 1){
		echo "[".date('Y-m-d H:i:s')." WRITTEN $record_size RECORDS]<br />\n";
	}
}

$max_backup = 60;
$cmd = "find '$backup_dir/' -maxdepth 1 -type f -name 'IU__*' | sort";
$cmd_result = trim(shell_exec($cmd));
$backup_files = explode("\n",$cmd_result);
$backup_files_size = count($backup_files);
if($backup_files_size>$max_backup){
	// remove the old backups
	for($i=0;$i<$backup_files_size-$max_backup;$i++){
		$lf->file_remove($backup_files[$i]);
	}
}

if($_REQUEST['Debug'] == 1){
	echo "[".date('Y-m-d H:i:s')." END BACKUP]<br />\n";
}

?>