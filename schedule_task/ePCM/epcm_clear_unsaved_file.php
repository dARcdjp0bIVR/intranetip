<?php
$WRT_ROOT= "{$_SERVER['DOCUMENT_ROOT']}/";
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_db_file.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();



$libdb = new libdb();
$libPCM_db_file = new libPCM_db_file();
$libfilesystem = new libfilesystem();



// Step 1: Get unsaved file's path => delete
$nowtime = time();
$twoDaysBefore = $nowtime - (2*24*60*60);

$sql = "SELECT * FROM INTRANET_PCM_ATTACHMENT WHERE CaseID = '0' AND IsDeleted = '0' AND Session < '$twoDaysBefore' ";
$fileAry = $libdb->returnResultSet($sql);
$successDeletedId = array();
$counter = 0;
foreach($fileAry as $_file){
	$path = $libPCM_db_file->getStoragePathForPCM($_file['FilePath']);
// 	debug_pr($path);
	if(file_exists($path)){
		$libfilesystem->file_remove($path);
		$counter++;
	}
	if(!file_exists($path)){
		$successDeletedId[] = $_file['AttachmentID'];
	}
}
$sql = "UPDATE INTRANET_PCM_ATTACHMENT SET IsDeleted = '1' WHERE AttachmentID IN ('".implode("','",$successDeletedId)."')";
$libdb->db_db_query($sql);

echo $counter.' files is deleted';


//Step 2: Get file with willDelete =1 => 0
$sql = "UPDATE INTRANET_PCM_ATTACHMENT SET WillDelete = '0' WHERE WillDelete = '1' AND Session < '$twoDaysBefore' ";
$libdb->db_db_query($sql);


echo '<br>ePCM cron job end <br>';
?>