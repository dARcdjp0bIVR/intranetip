<?php
# editing by 

/*
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Modification of this file may affect different modules.                          !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!   Please check carefully and inform elearning team leader after modifying it.      !!!!!!!!!!!!!!!!!!!!!!!!!   
 *      !!!!!!!!!!!!!!!!!!!!!!!!!                                                                                    !!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 *      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
 
/*
 * 20181120 (Paul)
 * 		- Handle SessionID and ApiKey by addslash to prevent MySQL injection
 */
$NoLangOld20 = true;
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libmethod.php");
include_once($PATH_WRT_ROOT."includes/libeclassapiauth.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");
include_once($PATH_WRT_ROOT."includes/libeclass_ws.php");
include_once($PATH_WRT_ROOT."includes/elearning/libAES.php");
include_once($PATH_WRT_ROOT."includes/elearning/libelearning.php");


intranet_opendb();

$jsonObj = new JSON_obj();
$lapiauth = new libeclassapiauth();
$lmethod = new LibMethod();
$lauth = new libauth();
$lelearning = new libelearning();


$AESKey = $elearningConfig['aesKey'];
$laes = new libAES($AESKey);


$successAry = array();
$requestJsonEncryptedString = '';
$requestJsonDecryptedString = '';
$responseJsonEncryptedString = '';
$responseJsonDecryptedString = '';



$requestJsonString = file_get_contents("php://input");

if (($plugin['power_lesson_2'] || $plugin['stem_x_pl2']) && $requestJsonString != '') {
	
	$requestJsonAry = $jsonObj->decode($requestJsonString);
	
	
	# AES Key transmission by RSA
	if ($requestJsonAry['PublicKey']) {
		
		$requestJsonDecryptedString = $requestJsonString;
		$publicKey = $requestJsonAry['PublicKey'];
		
		# sample key pairs for testing
		// $publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuN3fDHVczTBB+MGM+25BEy8UsOgGX5UV\nsHArTxYKcIHgPWU+lQDpZFz/liTwCdtPasOfN8veD2ZIf83/zjYn4cZUd5AqPAis4srEvqcmTbC2\ng1GTrHUrmUMAjpuQ5tkvwU2Xj6ZTZXgJSMP1ttQe1EPNtCtK1IUpDQxcO4PDLGNkiY2MJBeAo+hj\nxGuigrEQO1ndLDwFzetORxdEyBOc+EjMW9cjKXlnq0FWzvgYw62QqjvvOEWvMs3itCOKQZjjUWg+\nBuNUdx9oz+mrtxXhOBXXjyd0zh9bFbE0QtdvWjAn5I47fzZfX7m5MAAUXZrQ1TbgXQve2EGWdzAU\nNkelewIDAQAB";
		// $privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC43d8MdVzNMEH4wYz7bkETLxSw\n6AZflRWwcCtPFgpwgeA9ZT6VAOlkXP+WJPAJ209qw583y94PZkh/zf/ONifhxlR3kCo8CKziysS+\npyZNsLaDUZOsdSuZQwCOm5Dm2S/BTZePplNleAlIw/W21B7UQ820K0rUhSkNDFw7g8MsY2SJjYwk\nF4Cj6GPEa6KCsRA7Wd0sPAXN605HF0TIE5z4SMxb1yMpeWerQVbO+BjDrZCqO+84Ra8yzeK0I4pB\nmONRaD4G41R3H2jP6au3FeE4FdePJ3TOH1sVsTRC129aMCfkjjt/Nl9fubkwABRdmtDVNuBdC97Y\nQZZ3MBQ2R6V7AgMBAAECggEBAIKbFcM6g1bfuiwhqhXUfhssjwSw2BfdPxaJGL/mBXgY+bDGf6FL\npJhob5XHAOMl0wTNsgX8fSUKETxh8FDix+hlczWJX51MT8BmmvtLIe+PYlKCZr9XhHbh9FnMJGJD\nnM6HoAU38AFm/cAv2b/SOvmh8YlRcKuWbDswuVS+sTwAw19vqjhjh/jTUoFeTmZJ+iqXjiRICid/\n3uh8LcorW4iu4dvzuKSn8mUWy8f7u7/JFCf+tSje5Qz6y/10Gwb86VYmd/JM0nmKx9W8Rw3W0JRG\nnwm4J+0IRZA6BGzH4FItM9dfl1GGxzGkPkYudOZiiZYzz0ZSmbhUupmuzoD7NVECgYEA8d7NvIDd\nGKHlxbJZ2OZjFq75ps6Kb1MqioxG6EmeG8f1VYG9BPEaUEKo7EpSo4k6C1w8KJq3Ap9Nk33x1Y7g\nk3mKh3dcDC1xk9oXM+DeexQ1u0LsA+kTaxL3sPtY/RIE5SJ77GZvdVyjHNi+qxKCxjmmPBWPKtw2\n+7waih3Ns6MCgYEAw6qSdrxkZbeED7z+xkMf6OzGsc7mVoRAccPrdSViqGUcBYebBjdwdqElM+R5\nBy5WjIOe2G1EMstVk13kYpdlUyNgkX7JrSKtAByNbDKQKOwQcCkMwuOpzKIS6IGKqNTDle8K1d3P\nLRDgFXO7RsEacPIyg0JUR7AJaoKA1FL5pEkCgYATOWtO1Us9fvi9WuyrQbyUe/nZ2UbFjG2wL3Af\nXGKVwLHuRu6YKvW1dWbpyQqCdxDHAGDDtXBgY/sNgxAqsj2FWz64f7MwQJhrcQUxGWl1jOisZqhN\ns0PY2dwYFBJBoyICeFgzNP0c7e0FCPE2tbTxPnnJAsmrVW+FWLEfUzkbUwKBgArBCz+/Zv04Mufy\niUY4Vj6lFN6c8of6yNf33q8XmO6McBfVti2HSoUaokLR4d0FLPiYG1jl2IO6LT/mPzE0BPumzB3z\nC/6aE0wYRaWWudml79laSDtF7AU1OzjQNeDFoOJSuxO6FzLw5IAJVlezMVLX3PLd6GfwQvQ5q/7/\neH0RAoGBAOCUbQ7VbH4w/NA/QzIx7Fh5tC10Adyns1HburOTuvyufK3cUupFxzW3pbspnOQogP+a\ndqc5MG11GIjEONO66pTpkT8WfbXNyO5D/hmVIIOcEQGIu5MQuvoB+tw3Dra8V3wrx1pA+yKE6Sow\n4e2cZLebT/D6k2SCaWn1VCU7R9HI";
		
		$publicKey = base64_decode(($publicKey));

		# Key translation: DER encoded keys into PEM
		$pem = chunk_split(base64_encode($publicKey), 64, "\n");
		$publicKey = "-----BEGIN PUBLIC KEY-----\n".$pem."-----END PUBLIC KEY-----\n";
		
		# do encrypt
		if (openssl_public_encrypt($AESKey, $encrypted, $publicKey, OPENSSL_PKCS1_PADDING)) {
            $encryptedKey = base64_encode($encrypted);
		}
		
		# check if server installed mcrypt or not
		$mcryptStatus = function_exists('mcrypt_module_open');
		
		# return AESKey as response
		$returnJsonAry = array();
		$returnJsonAry['EncryptedAESKey'] = $encryptedKey;
		$returnJsonAry['McryptStatus'] = $mcryptStatus;
	}
	else {
		# decrypt request
		$requestJsonDecryptedString = $requestJsonString;
		if ($requestJsonAry["eClassRequestEncrypted"]) {
			$AESEnabled = true;
			$requestJsonEncryptedString = $requestJsonString;
			$encryptedRequest = $requestJsonAry["eClassRequestEncrypted"];
					
			$requestJsonDecryptedString = $laes->decrypt($encryptedRequest);
			if ($requestJsonDecryptedString != '') {
				# decrypted request
				$requestJsonAry = $jsonObj->decode($requestJsonDecryptedString);
			}
		}
		
		# handle request
		if(isset($requestJsonAry['eClassRequest']['SessionID'])){
			$requestJsonAry['eClassRequest']['SessionID'] = addslashes($requestJsonAry['eClassRequest']['SessionID']);
		}
		if(isset($requestJsonAry['eClassRequest']['APIKey'])){
			$requestJsonAry['eClassRequest']['APIKey'] = addslashes($requestJsonAry['eClassRequest']['APIKey']);
		}
		if ($requestJsonAry['eClassRequest']) {
			$sessionID = $requestJsonAry['eClassRequest']['SessionID'];
			$requestMethod = $requestJsonAry['eClassRequest']['RequestMethod'];
			$apiKey = $requestJsonAry['eClassRequest']['APIKey'];
			
			
			// commented as not used in eclas app now
			$verifyAPIKey = false;
			if ($apiKey) {
				$projectName = $lapiauth->GetProjectByApiKey($apiKey);
				if ($projectName == $elearningConfig['appApiProjectName']['PL2']) {
					$appType = $elearningConfig['appType']['PowerLesson2'];
					$verifyAPIKey = true;
					$requestJsonAry['eClassRequest']['Request']['hasAPIKey'] = true;
				}
			}
			
			if ($verifyAPIKey) {
				# handle request method:
				if (trim($sessionID) != '') {
					$UserID = $lauth->GetUserIDFromSessionKey($sessionID);
						
					if ($UserID != '') {
						if($requestMethod == 'Logout'){
							$requestJsonAry['eClassRequest']['Request']['UserID'] = $UserID;
						}
						$requestID = $lmethod->Generate_RequestID();
						$result = $lmethod->callMethod($requestMethod, $requestJsonAry);						
					}else{
						$result = '5';	
					}
				} else if ($requestMethod == "Login") {
					// check license
					$isSchoolInAppLicense = $lelearning->isSchoolInLicense($appType);
					
					if ($isSchoolInAppLicense) {
						// in license => do login
						$requestID = $lmethod->Generate_RequestID();
						
						if (((isset($UseLDAP) && $UseLDAP) || $intranet_authentication_method == 'LDAP') && $ldap_user_type_mode){
							# Different users using different LDAP dn string
							
							# Get User Type
							$sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '".$curUserLogin."'";
							$temp = $lelearning->returnArray($sql,2);
							
							list($t_id, $t_user_type) = $temp[0];
							
							if (!is_array($temp) || sizeof($temp)==0 || $t_user_type == "" || $t_user_type < 1 || $t_user_type > 4)
							{
								$Result = '401';
							}
							else{
								switch ($t_user_type)
								{
									case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
									case 2: $special_ldap_dn = $ldap_student_base_dn; break;
									case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
									default: $special_ldap_dn = $ldap_teacher_base_dn;
								}
							}
						}
							
						$result = $lmethod->callMethod('Login', $requestJsonAry);
						if (is_array($result)) {
							$curUserId = $result['UserID'];
							$targetUserLogin = $result['UserLogin'];
							$isTeacher = ($result['UserType']=='T')? true : false;
							$isStudent = ($result['UserType']=='S')? true : false;
							$isParent = ($result['UserType']=='P')? true : false;
							
							$matchedAppType = false;
							if (($isTeacher || $isStudent) && $appType == $elearningConfig['appType']['PowerLesson2']) {
								$matchedAppType = true;

                                // return user cust flag
                                $result['userCustFlagAry'] = $lelearning->getUserExtraFlag($curUserLogin, $curUserId);

                                // return school cust flag
                                $result['schoolCustFlagAry'] = $lelearning->getSchoolCustFlag();
							}
						}
					}
					else {
						# Invalid license
						$result = '402';
					}
				} else if ($requestMethod == "PrePinLogin" || $requestMethod == "GetPL2SchoolCustFlags") {
					
					// check license
					$isSchoolInAppLicense = $lelearning->isSchoolInLicense($appType);
					
					if ($isSchoolInAppLicense) {
						// return school cust flag
						$result['schoolCustFlagAry'] = $lelearning->getSchoolCustFlag();
						
					} else {
						# Invalid license
						$result = '402';
					}
					
                }
                else if ($requestMethod == "PowerLesson") {
                    # Call PowerLesson method
                    $UserID    = '';
                    $requestID = $lmethod->Generate_RequestID();
                    $result    = $lmethod->callMethod("PowerLesson", $requestJsonAry);
                }
                else if ($requestMethod == "StemLicense") {
                    # Call StemLicense method
                    $UserID    = '';
                    $requestID = $lmethod->Generate_RequestID();
                    $result    = $lmethod->callMethod("StemLicense", $requestJsonAry);
                }
				else {
					$result = '5';
				}
				
			} else {
				# Invalid license
				$result = '402';
			}
			
			if(!is_array($result)) {
				$result = $lmethod->GetErrorArray($result);	
				$returnResult = 'N';
			}
			else{
				$returnResult = 'Y';
			}
			
			# generate return JSON content
			$returnJsonAry = array();
			$returnJsonAry['RequestID'] = $requestID;
			$returnJsonAry['ReturnResult'] = $returnResult;
			$returnJsonAry['Result'] = $result;
		}
	}
	
	$returnJsonString = $jsonObj->encode($returnJsonAry);
	$responseJsonDecryptedString = $returnJsonString;
	
	if ($AESEnabled) {
		$encryptedReturnContent["eClassResponseEncrypted"] = $laes->encrypt($returnJsonString);
		$returnJsonString = $jsonObj->encode($encryptedReturnContent);
		
		$responseJsonEncryptedString = $returnJsonString;
	}
	
	
	// log request
	if ($requestMethod == 'Login') {
		$requestJsonDecryptedString = '';	// because this request has user password
	}

	header('Content-Type: application/json; charset=utf-8');
	echo $returnJsonString;
	
}
else {
	$returnJsonAry = array();
	$returnJsonAry['ReturnResult'] = 'Y';
	$returnJsonAry['Result']['PowerLesson2Enabled'] = $plugin['power_lesson_2'] ?  '1' : '0';
	
	header('Content-Type: application/json; charset=utf-8');
	$returnJsonString = $jsonObj->encode($returnJsonAry);
	
	echo $returnJsonString;
}

intranet_closedb();

?>