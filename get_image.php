<?php 
// Editing by 

/*
 *  2018-05-07 (Carlos)
 *  - session_write_close() before sending the file content.
 *
 *	2018-01-16 (Bill)
 *	- support Student Photo Cache for eReportCard (Kindergarten)	[removed]
 *
 *  2017-07-06 (Bill)	[2017-0706-1211-34066]
 * 	- fixed cannot access photo when user print preview report using IE
 *  
 * 2017-03-10 (Carlos): Prevent photo from direct access from browser, applied to the following path patterns: 
 * 	- /file/user_photo/*.jpg
 * 	- /file/official_photo/*.jpg
 * 	- /file/photo/personal/*.jpg
 *  - require to work together with /file/.htaccess
 */

/*
// Special Handling for eReportCard (Kindergarten)
if($_SERVER["HTTP_REFERER"] != "" && strpos($_SERVER["HTTP_REFERER"], "/reportcard_kindergarten/index.php?task=lesson") !== false)
{
	$image_dir = $_GET['image_dir'];
	$image_name = $_GET['image_name'];
	
	//$abs_image_dir = '/home/web/eclass40/intranetIP25/file/'.$image_dir;
	$abs_image_dir = '/home/web/sites/eclass.puiching.edu.mo/intranetIP/file/'.$image_dir;
	$abs_image_path = $abs_image_dir.'/'.$image_name;
	if (file_exists($abs_image_path))
	{
		$ext_no_dot = substr($abs_image_path, strrpos($abs_image_path,'.')+1);
	    header('Content-Type: image/'.$ext_no_dot);
   
   	    // Allow Student Photo Cache in eReportCard (Kindergarten)
		header('Cache-Control: max-age=43200');
		header('Pragma: ""');
		
		readfile($abs_image_path);
   }
}
*/

include_once('includes/global.php');

//ob_start();
//debug_pr($_SERVER);
//$tmp = ob_get_contents();
//ob_end_clean();
//error_log($tmp."\n", 3, "/tmp/aaa.txt");
//error_log("==============\n", 3, "/tmp/aaa.txt");

$image_dir = $_GET['image_dir'];
$image_name = $_GET['image_name'];
$abs_image_dir = $file_path.'/file/'.$image_dir;
$abs_image_path = $abs_image_dir.'/'.$image_name;

$fromAndriodApp = false;
if (!isset($_SERVER['HTTP_ACCEPT'])) {
	$fromAndriodApp = true;
}

$fromAppleApp = false;
if ($_SERVER['HTTP_ACCEPT'] == '*/*') {
	$fromAppleApp = true;
}

// [2017-0706-1211-34066] added condition - {strpos($_SERVER["HTTP_ACCEPT"], "image/*")!==false]} 
$fromHtmlImgTag = false;
if ($_SERVER["HTTP_REFERER"] != '' || strpos($_SERVER["HTTP_ACCEPT"], "image/*")!==false) {
	$fromHtmlImgTag = true;
}

$fromDownloadEngine = false;
if ($_SERVER["HTTP_REFERER"] == curPageURL($withQueryString=0, $withPageSuffix=0).'/file/'.$image_dir) {
	$fromDownloadEngine = true;
}

if ($fromDownloadEngine) {
	die();
}
else if ($fromAndriodApp || $fromAppleApp || $fromHtmlImgTag) {
	if (file_exists($abs_image_path))
	{
		session_write_close(); // may help improve performance
		
		$ext_no_dot = substr($abs_image_path, strrpos($abs_image_path,'.')+1);
        header('Content-Type: image/'.$ext_no_dot);
        
        /*
        // Special Handling for eReportCard (Kindergarten)
        if($fromHtmlImgTag && strpos($_SERVER["HTTP_REFERER"], "/reportcard_kindergarten/index.php?task=lesson")!==false)
        {
       	    // Allow Student Photo Cache in eReportCard (Kindergarten)
        	header('Cache-Control: max-age=30');
	        header('Cache-Control: max-age=43200');
	        header('Pragma: ""');
        }
        */
        
  		readfile($abs_image_path);
  	}
}
else {
	die();
}
?>