<?
// Editing by 
/*
 * 2016-01-19 (Carlos): For non-IE browsers, redirect to /adminauth.php to re-authenticate login to clear previous cached credentials. 
 * 						Please see the comments in /adminauth.php
 */
include_once("includes/global.php");
$_SESSION['ADMIN_AUTHENTICATED'] = 0;
$_SESSION['ADMIN_USER'] = $_SERVER['PHP_AUTH_USER'];
$_SESSION['ADMIN_PASS'] = $_SERVER['PHP_AUTH_PW'];
?>
<html>
<head>
<meta http-equiv="Content-Type"  content="text/html" Charset="<?=returnCharset()?>" />
<script type="text/javascript" language="javascript">
document.execCommand("ClearAuthenticationCache") //clear cache
//parent.location.href="/" //redirect after logged out
if(!document.all) // not IE
{
	parent.window.setTimeout(function(){
		parent.window.location.href='/adminauth.php';
	}, 1000);
}
</script>
</head>
<body>
<?
if ($intranet_session_language=="b5")
{
    $response_msg = "你已經登出 eClass 綜合平台管理中心.<br><br>
基於安全理由, 請關閉所有瀏覽器視窗以完全登出.";
}
else
{
    $response_msg = "You have already logged out eClass IP Admin Console.<br><br>

To safe keep your account information, you are advised to close all your browsers instantly.";
}
echo $response_msg;
?>

</body>
</html>
