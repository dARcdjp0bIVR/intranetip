<?php
# critical deployment version log

############################################ Remarks ############################################
# To get the version array, pls defined $JustWantVersionData = true; before including this script
# The format of the version array will be the same as version.php
# e.g. array("ip.2.5.8.7.1.0.1", "2017-08-09 16:00", "critical deploy");
# if $JustWantVersionData is not defined, this script will echo the latest version string
#################################################################################################

######################################## array ######################################
################# Format: time of deployment, fixes #####################
################# DECENDING order, show first entry  ####################
####################################################################################
$version_critical["ip.2.5.11.1.1"]["12"] = array("2020-05-02 12:30", "");
$version_critical["ip.2.5.11.1.1"]["11"] = array("2020-04-24 01:00", "");
$version_critical["ip.2.5.11.1.1"]["10"] = array("2020-04-17 15:00", "");
$version_critical["ip.2.5.11.1.1"]["9"] = array("2020-04-16 18:00", "");
$version_critical["ip.2.5.11.1.1"]["8"] = array("2020-04-06 18:00", "");
$version_critical["ip.2.5.11.1.1"]["7"] = array("2020-03-26 18:00", "");
$version_critical["ip.2.5.11.1.1"]["6"] = array("2020-03-03 18:00", "");
$version_critical["ip.2.5.11.1.1"]["5"] = array("2020-02-27 18:00", "");
$version_critical["ip.2.5.11.1.1"]["4"] = array("2020-02-20 12:00", "");
$version_critical["ip.2.5.11.1.1"]["3"] = array("2020-02-18 20:00", "");
$version_critical["ip.2.5.11.1.1"]["2"] = array("2020-02-11 10:00", "");
$version_critical["ip.2.5.11.1.1"]["1"] = array("2020-01-17 18:00", "");

$version_critical["ip.2.5.10.10.1"]["2"] = array("2019-11-26 18:00", "");
$version_critical["ip.2.5.10.10.1"]["1"] = array("2019-11-04 17:00", "");

$version_critical["ip.2.5.10.6.1"]["8"] = array("2019-09-26 16:00", "");
$version_critical["ip.2.5.10.6.1"]["7"] = array("2019-09-17 12:00", "");
$version_critical["ip.2.5.10.6.1"]["6"] = array("2019-09-06 12:00", "");
$version_critical["ip.2.5.10.6.1"]["5"] = array("2019-09-05 16:00", "");
$version_critical["ip.2.5.10.6.1"]["4"] = array("2019-08-29 12:00", "");
$version_critical["ip.2.5.10.6.1"]["3"] = array("2019-08-08 16:00", "");
$version_critical["ip.2.5.10.6.1"]["2"] = array("2019-07-18 16:00", "");
$version_critical["ip.2.5.10.6.1"]["1"] = array("2019-07-09 16:00", "");

$version_critical["ip.2.5.10.2.1"]["16"] = array("2019-06-17 19:00", "");
$version_critical["ip.2.5.10.2.1"]["15"] = array("2019-05-24 17:30", "");
$version_critical["ip.2.5.10.2.1"]["14"] = array("2019-05-21 16:00", "");
$version_critical["ip.2.5.10.2.1"]["13"] = array("2019-05-17 18:30", "");
$version_critical["ip.2.5.10.2.1"]["12"] = array("2019-05-17 16:30", "");
$version_critical["ip.2.5.10.2.1"]["11"] = array("2019-05-16 09:15", "");
$version_critical["ip.2.5.10.2.1"]["10"] = array("2019-05-14 16:00", "");
$version_critical["ip.2.5.10.2.1"]["9"] = array("2019-05-10 14:00", "");
$version_critical["ip.2.5.10.2.1"]["8"] = array("2019-05-07 10:00", "");
$version_critical["ip.2.5.10.2.1"]["7"] = array("2019-05-03 10:00", "");
$version_critical["ip.2.5.10.2.1"]["6"] = array("2019-04-29 17:30", "");
$version_critical["ip.2.5.10.2.1"]["5"] = array("2019-04-26 17:20", "");
$version_critical["ip.2.5.10.2.1"]["4"] = array("2019-04-16 10:40", "");
$version_critical["ip.2.5.10.2.1"]["3"] = array("2019-03-25 13:45", "");
$version_critical["ip.2.5.10.2.1"]["2"] = array("2019-03-12 11:00", "");
$version_critical["ip.2.5.10.2.1"]["1"] = array("2019-03-06 16:00", "");

$version_critical["ip.2.5.9.11.1"]["5"] = array("2019-01-31 16:30", "");
$version_critical["ip.2.5.9.11.1"]["4"] = array("2019-01-25 17:45", "");
$version_critical["ip.2.5.9.11.1"]["3"] = array("2018-12-06 14:45", "");
$version_critical["ip.2.5.9.11.1"]["2"] = array("2018-11-23 14:45", "");
$version_critical["ip.2.5.9.11.1"]["1"] = array("2018-11-21 14:00", "");

$version_critical["ip.2.5.9.7.1"]["4"] = array("2018-10-23 10:00", "");
$version_critical["ip.2.5.9.7.1"]["3"] = array("2018-09-20 11:45", "");
$version_critical["ip.2.5.9.7.1"]["2"] = array("2018-09-05 16:30", "");
$version_critical["ip.2.5.9.7.1"]["1"] = array("2018-08-31 11:00", "");

$version_critical["ip.2.5.9.5.1"]["2"] = array("2018-06-11 10:00", "");
$version_critical["ip.2.5.9.5.1"]["1"] = array("2018-05-17 13:30", "");

$version_critical["ip.2.5.9.3.1"]["3"] = array("2018-04-16 10:30", "");
$version_critical["ip.2.5.9.3.1"]["2"] = array("2018-04-03 16:55", "");
$version_critical["ip.2.5.9.3.1"]["1"] = array("2018-03-27 13:45", "");

$version_critical["ip.2.5.9.1.1"]["2"] = array("2018-01-30 17:25", "");
$version_critical["ip.2.5.9.1.1"]["1"] = array("2018-01-29 10:15", "");

$version_critical["ip.2.5.8.10.1.0"]["4"] = array("2017-12-12 12:00", "");
$version_critical["ip.2.5.8.10.1.0"]["3"] = array("2017-12-05 11:30", "");
$version_critical["ip.2.5.8.10.1.0"]["2"] = array("2017-11-23 16:00", "");
$version_critical["ip.2.5.8.10.1.0"]["1"] = array("2017-11-13 17:00", "");

$version_critical["ip.2.5.8.7.1.0"]["3"] = array("2017-09-04 17:00", "");
$version_critical["ip.2.5.8.7.1.0"]["2"] = array("2017-08-24 17:00", "");
$version_critical["ip.2.5.8.7.1.0"]["1"] = array("2017-08-09 16:00", "");

$version_critical["ip.2.5.8.4.1.0"]["3"] = array("2017-06-07 10:00", "");
$version_critical["ip.2.5.8.4.1.0"]["2"] = array("2017-05-23 17:00", "");
$version_critical["ip.2.5.8.4.1.0"]["1"] = array("2017-05-15 14:00", "");

$version_critical["ip.2.5.8.1.1.0"]["4"] = array("2017-03-24 11:00", "");
$version_critical["ip.2.5.8.1.1.0"]["3"] = array("2017-03-14 15:00", "");
$version_critical["ip.2.5.8.1.1.0"]["2"] = array("2017-03-13 12:00", "");
$version_critical["ip.2.5.8.1.1.0"]["1"] = array("2017-02-28 12:00", "");

$version_critical["ip.2.5.7.10.1.0"]["1"] = array("2016-11-01 10:42", "");

$version_critical["ip.2.5.7.7.1.0"]["5"] = array("2016-09-20 10:00", "");
$version_critical["ip.2.5.7.7.1.0"]["4"] = array("2016-09-08 17:00", "");
$version_critical["ip.2.5.7.7.1.0"]["3"] = array("2016-09-02 10:00", "");
$version_critical["ip.2.5.7.7.1.0"]["2"] = array("2016-08-23 10:00", "");
$version_critical["ip.2.5.7.7.1.0"]["1"] = array("2016-08-16 10:00", "");

$version_critical["ip.2.5.7.4.1.0"]["2"] = array("2016-05-12 14:30", "");

$version_critical["ip.2.5.7.4.1.0"]["1"] = array("2016-04-26 14:30", "");

$version_critical["ip.2.5.7.1.1.0"]["2"] = array("2016-02-16 09:30", "");

$version_critical["ip.2.5.7.1.1.0"]["1"] = array("2016-01-20 15:20", "");

$version_critical["ip.2.5.6.10.1.0"]["2"] = array("2015-11-26 14:30", "");

$version_critical["ip.2.5.6.10.1.0"]["1"] = array("2015-10-26 15:25", "");

$version_critical["ip.2.5.6.7.1.0"]["2"] = array("2015-08-28 14:00",
"[Fix][eDiscipline] Z82859 - 五旬節林漢光中學 - Fail to find eDis module<br>- Fix cannot access eAdmin > eDiscipline if not eDiscipline admin
[Fix][installation] error msg of creating LIBMS DB table when run addon schema
[Fix][Group] K82730 - 九龍真光中學 - Fail to copy admin for group
[Fix][eEnrolment] Enrol Summary Report cannot show all type of club
[Fix] X82045 - 聖公會呂明才紀念小學 - QEF 學生課堂紀錄有問題班級 <br>Patch plan_section_id for old quiz_result record
[Fix][KIS] iMail selecting recipients fail to include identity group members. 
[Fix][ePayment] Fail to add ref code to import add value records
[Fix][iSmarCard] Fail to get class list when today is non-school day and change to another day to take attendance. 
[Fix][iMail/iMail plus] Fail to save html signature and multiple forwarding emails at preference settings as it use form method GET. 
[Fix][TeacherApp] Get wrong current school year id if login to one site and then login to another site (Take attendance,ehomework,student performance,student list)
[Fix][StudentAttendance] Fix and rerun the schema for the customized Student Attendance absent session fields.
[Fix] Fail to unpack powerboard with large content
[Fix][GroupMessage] If all devices of the account is logged out, the device logout status cannot sync to central server and therefore the user can still receive the push message from Group Message"
);

$version_critical["ip.2.5.6.7.1.0"]["1"] = array("2015-08-14 14:00",
"[Fix][IP25] Cater customized login page client for the secure token checking mechanism.<br>[X82071 - 金巴崙長老會耀道中學 - Failed to access eClass]
[Fix] E81774 - IP25 Training - Save As target -Garbled file name<br>- fix the problem of showing monster code of the downloaded file from Teacher Sharing Area using IE browser
[Fix] L81911 - 喇沙書院 - can't go to class room after update source<br>- fix the problem of not be able to access eClass classroom if client site uses \"https\" as web protocol<br>*** Can't be tested as it required https as web protocol setting***
[New] support Flipped Channels Portal
[Fix][eClassApp] iOS app force closed if the student has no payment account record [Case#N82072]
[Fix][eEnrolment] C82057 - 仁濟醫院王華湘中學 - Club PIC could add members via Progress even set disallowed 
[Minor bugs & Improvement][eEnrolment]<br>-Hide data deletion option in activity setting<br>-new/edit activity content checking<br>-group attendance helper not allow to handle payment
[Fix][eBooking] R81715 - 佛教善德英文中學 - How to allow some teacher booking the facility without limit 
[Improvement][eNotice] - new/edit notice to applicable student will autofocus to searchbox
[Fix][PowerLesson] File Upload - Fail to hide the upload button when using PowerLesson Android App even the activity is ended.
[Fix][PowerLesson] Polling - If the content of an option is '0', it will be disappeared when teacher try to edit the activity
[Fix][PowerLesson] Discussion - Message - &quot; \" / &amp; & / &lt; < / &gt; > become \" \" / &amp; & / < < / > > after quote
[Fix][eLibPlus] cannot receive notification email (change due date & Overdue happens)
[Fix][eLibPlus] X81387 - total number of not take item in stocktake process page
[Fix][PowerLesson] FileUpload - The hyper link could not be opened in a popup window after the user refreshes the page<br>- link includes external sources, teacher projects student submission, etc"
);

$version_critical["ip.2.5.6.5.1.0"]["1"] = array("2015-05-21 12:00",
"[Fix][iMail plus] Fixed comma changed to semi-colon issue in jquery auto complete for iMail plus. Fixed issue of failing to parse email datetime string if contains (GMT+).
[Fix][eEnrolment] K78631 - 基督書院 - Failed to transfer club records to OLE
[Improvement][PowerLesson]Quiz - Question percentage improvement in Learner Diversity Mode
[Fix][Assessment]Online Question Paper - Timer reset after user saves as draft and close browser
[Fix][PowerLesson] Answersheet - Fail to upload image file when using PC
[Fix][eHomework]improve the upload function (change the type=“file” to multiple, for the the iOS8 bug)
[Improvement][eHomework]show class number at hand in page
[Improvement][eHomework]Change the \"no need to hand-in\" to \"---\"
[Fix][eHomework]fix the language bug when get the class by  ajax in new homework page 
[DigitalChannel] - improve webview comment and submit code.
[Improvement][eInventory] adding total assets at Fixed Assets Register
[Cust][eInventory] edit bulk item purchase price, unit price and quantity for SKH
[Improvement][DigitalChannels] support AVI for video uploading
[Fix][App+eNotice] eNotice push message show \"&\" as \"&amp;\" if the notice number and title contains \"&\" symbol
[Fix][eLibraryPlus] fixed to show the recommedation book at front end"
);

$version_critical["ip.2.5.6.3.1.0"]["2"] = array("2015-04-22 11:22",
"[Flipped Channels] Temp fix for json_decode returning object instead of array problem.
[eContent] Fail to close glossary.
[iMail][Fix] Auto clean trash/spam mails by in folder datetime.
[iMail plus] Prevent self email added to auto forward list to avoid mail-looping.
[Staff Attendance] Fix array merge with + operator with duplicated key issue. 
[Student Attendance] Fix update PM late/earlyleave/absent list change AM last updated user and date issue.
[DR] Let module admin can complete/reopen routings, not only the creator.
[PowerLesson] Fail to click OK button in jAlert / jConfirm Box when using iPad
[PowerLesson][Discussion] No response when clicking 'Add File' button if using Alcatel
[PowerLesson][PowerBoard] Status shown of a marked record become \"not yet marked\" after viewing original student submission
[PowerLesson] Hide the text to speech highlight function for android devices
[PowerLesson][Discussion] Remove the wordings \"Assistance Request\" behind the icon in the student submission status table
[PowerLesson] Hide 'Evaluation' option when create/edit lesson plan
[PowerLesson] When editing lesson plan, fail to select activity in the list if user is using iPad with iOS 8
[PowerSketch] Line and Eraser new thickness control
[eLibraryPlus] P77420 - Why Review can only show 10 records
[Fix][eLibPlus] Student reach loan limit if overdue record show on circulation page
[Fix][eNotice] teacher teach only one class cannot view notice statistics via View Own Class"
);

$version_critical["ip.2.5.6.3.1.0"]["1"] = array("2015-03-24 16:30",
"[Student Attendance] B76475 - 香港華人基督教聯會真道書院 - Failed to print and export Late Student List 
- Fix late list print & export page. 
- Fix submit prove document waive option fail to auto apply bug.
[Staff Attendance] D76339 - 聖公會聖馬利亞堂莫慶堯中學 - Fail to follow the default waive for early leave
Fixed fail to auto apply waive option to early leave reason. 
[ePayment] Fix copy payment items should also copy subsidy amount records issue
[eBooking] Fix Priority booking detect time clash wrongly
[eLibPlus] Fix cannot patch the book language to the new language setting table
[eClass Survey] Fail to delete a list of survey due to IntegerSafe checking
[Flipped Channels] Development on eContent, eClass App, Teacher App
[PowerLesson App] Fail to perform login re-map in demo site when using PowerLesson App
[PowerLesson] PowerBoard
1. Fail to copy or pack PowerBoard Backgroud image
2. Fail to copy or pack the simplify setting of PowerBoard
[PowerLesson] Marksheet - Fail to display student quiz result if functionSerializedObj is set but SubmitMode is not set"
);

$version_critical["ip.2.5.6.3.1.0"]["0"] = array("2015-03-13 09:00",
"[Fix]PowerLesson Discussion - unable to show the complete Attachment Bubble in Discussion Activity when using Chrome Version 41.0.2272
[iPortfolio] - Hide customization report button"
);

$version_critical["ip.2.5.6.1.1.0"]["6"] = array("2015-03-06 09:00",
"[iPortfolio] - Fix Performance Outside School change to OLE #U75738"
);

$version_critical["ip.2.5.6.1.1.0"]["5"] = array("2015-03-03 09:00",
"[iPortfolio] Fixed - iPortfolio OLE order  set to 0 and cause cannot display in report  #J75587
[Teacher App iMail][Fix] Remove attachment folder
[Teacher App iMail][Fix] forward一封有附件的電郵，附件是0KB.
[Teacher App iMail][Fix] INBOX返回上一頁，app都會updating
[Teacher App iMail][Fix] 草稿箱attachment size 0"
);

$version_critical["ip.2.5.6.1.1.0"]["4"] = array("2015-02-05 15:00",
"[Fix] J74596 - 香港華仁書院 - Fail to send sms - add basic checking to '' and not numbers phone
[Fix] Staff Attendance - Ignore the time slot tap card period setting for non-overnight duty. Previous fix cause early arrive staff cannot tap card in earlier time.
[Fix][iPortfolio] Student account cannot delete SLP records > SBS - Fail to add/remove table field in a form in scheme due to js error
[Fix] PowerLesson Discussion - role changed after upload attachment in replies"
);

$version_critical["ip.2.5.6.1.1.0"]["3"] = array("2015-02-04 14:00",
"[Fix] imail - clcik delete button on email list, list start refreshing and chaing the selected items to unselected
[Fix] imail - refresh email list, the receiver names change to their userLogin
[Fix] iPortfolio - Display question mark in dynamic report
[Fix] Assessment - Fail to copy assessment if task instruction contains single quotes.
[Fix] RS1-3 - Scrolling in iframe problem caused by css 
[Fix] eInventory - Fail to show purchase date if exported Fixed Assets Register
[Fix] eLibPlus - missing to update book quantity at import writeoff function
[Fix] PowerLesson Answersheet - Fail to pack image files of a html file which is coverted from doc file
[Fix] PowerLesson Quiz - Fail to pack and append Quiz activity if the 'functionSerializedObj' exists but 'SubmitMode' doesn't exist
[Fix] PowerLesson - Fail to show the marksheet button if the user is Super Admin and no powertools are enabled in the site
[Fix] PowerLesson - When editing lesson plan info, pre lesson and post lesson setting are checked and the start date and end date are set to 0000-00-00 00:00:00
[Fix] PowerLesson Answersheet - Enable student to switch files after submit the answer of Answersheet (i.e. only block to answer panel)
[Improvement] App - do request log housekeeping when the first user logout everyday"
);

$version_critical["ip.2.5.6.1.1.0"]["2"] = array("2015-01-23 14:00",
"[Fix] eNotice - Parent fails to sign payment notice under \"All School Notices\" filtering
[Fix] eNotice & eCircular - Failed to choose specific group user if the notice/circular is for specific users only [Case#X74349]
[Fix] eClass Statistics - Does not show the column of Miscellaneous File Size"
);

$version_critical["ip.2.5.6.1.1.0"]["1"] = array("2015-01-22 09:00",
"[Fix] eNotice - Parent fails to sign the notice - All School Notices
[Fix] P74198 - 聖士提反書院 - Club title in mapping file to WebSAMS
- Fixed sample file get club english name
[Fix] N74208 - 香港耀能協會 - Fail to increase number at Repair System
- Fixed right logo menu showing no.
[Fix] J74187 - 天主教鳴遠中學 - Some students cannot enrol the club for 2nd term
[Fix] S73984 - 潔心林炳炎中學 - Fail to export Outstanding Payment List
[Fix] ePayment - P73801 - 真理浸信會何袁惠琼幼稚園 - Fail to show last modify after payment at Student Payment Item
[Fix] Student Attendance - V73643 - 伯裘書院 - Fail to show the confirmation time in Student attendance
- Fixed the take attedance of subject group at eAttendance.
[Fix] Student Attendance - B74191 - 利瑪竇中學(中學部) - fail to remove attendance records for one day 
- Fixed customization data of absent session data are not catered at [Remove past data].
[Fix] Staff Attendance - K73953 - 心光學校 - Fail to record all in out time correctly for overnight shift
- Fix edit past duty fail to handle over night duty problem.
[Fix] iCalendar - Z73692 - 香海正覺蓮社佛教陳式宏學校 - Wrong iCalendar event creator
- Fixed sync google calendar events overwrite event creator problem.
[Fix] iCalendar - N74043 - 香港耀能協會 - Missing Show in school calendar option
- Fixed fail to default display [Show in school calendar] option when create a new event if a group calendar is choosen as the default calendar.
[Fix] Teacher App iMail: if webmail set false, compose mail page dosen't work."
);

$version_critical["ip.2.5.5.10.1.0"]["1"] = array("2014-12-05 14:00",
"[Fix] Digital Archive - Fixed portal page simple search enter key bug.
[Fix] ePOS<br>1) Fixed invoice number has extra digit problem when more than 10 transactions on the same day.<br>2) Changed void/cancel transaction can refund money by selected items amount.<br>3) Fixed transaction list end date filter problem.
[Fix] ePayment - Fixed Outstanding payment list sorting problem
[Fix] ePayment - Student payment item auto generate reference code for adding money.
[Fix] iCalendar - Fixed repeat group events wrongly deleted problem.
[Fix] iMail plus - [Mail removal by Mail Subject and Date Range] make sender condition optional for easier search.
[Fix] iMail plus - Fixed quotes display problem from auto completion result when adding recipients.
[Fix] Staff Attendance - fixed customized report fail to count late & early leave record if either one state is waived.
[Fix] Student Attendance - fixed Class Monthly Attendance Report fail to count continuous absent if non-school days are in bewteen the absent days.
[Fix] LP - display folder in template selection list when edit a LP 
[Fix]LP - Fail to retrieve \$ck_room_type in eclass30 so redirect to access page instead of content update page
[Fix]Forum - Fail to archive each forum using archive button
[Fix] PowerLesson FileUpload - Fail to play MP3 file with Chinese file name after upload
[Fix] PowerLesson - Show \"Pre-Lesson Students\" instead of \"Pre-Lesson Groups\" at the top right hand corner of the pre-lesson status table
[Improve] PowerLesson Discussion - Convert .mov file to .mp4 for viewing in android devices
[Fix] PowerLesson PowerBoard - If a student is using IE 9 or earlier version, JS Error found when teahcer ended the PowerBoard activity
[Fix] PowerLesson - If student login / logout lesson when teacher is viewing the pre/post lesson status table, the status of that student will be changed to 'Login' / 'Logout' instead of 'Done' / 'Not yet Done'
[Fix] PowerLesson - For a group submit activity, show no of group instead of no of student that have completed the pre/post lesson
[Fix] PowerLesson Quiz - For a Pre-lesson Quiz Activity, Post-Lesson instead of Pre-Lesson student status table is shown when user click on the \"Pre-Lesson Students\" button after submitting the quiz settings
[Fix] PowerLesson - function id is reset to 0 after editing the activity (e.g. update activity title)
[Fix] PowerLesson Marksheet - In setting, if teacher delete a category with no title inputed, \"Please enter the category title.\" warning message will also be shown when submit
[Improve] PowerLesson - Add JS object pluploadManager to manage the plupload uploader object and upload result
[Fix] PowerLesson Quiz - Fail to play MP3 attached in answer with Chinese file name when using iPad / Android
[Fix] PowerLesson - In group manage page, the students in the deleted group are not put back in the drop down list of \"students with no group\" after deleting a group
[Fix] eInventory -  Export write off approval - incorrect unit price in approval list
[Fix] eInventory - Export location with 2 format in Fixed Assets Register - CSV problem (missing bulidings info location column)
[Fix] eInventory - Bulk item cannot display purchase price correctly at Fixed Assets Register
[Fix] eInventory - Restored items still got write-off date and reason
[Fix] Account Mgmt - Parent account info changed when redirect from child account
[Fix] eBooking - Follow up arrangement failed to display in order
[Fix] eInventory - Fixed Assets Register - have to remove purchase date several times
[Fix] iPortfolio - JUPUS - Student failed to save additional information
[Fix] eDiscipline - Management<br>1. Award and Punishment - Incorrect History event date sequence<br>2. Detention - Print detention attendance sheet cannot follow sorting result<br>
[Fix] eDiscipline - Report<br>1. Award and Punishment Report - Failed to show statistics of last school year<br>2. Master Report - Failed to show class and name of last school year<br>3. Good Conduct & Misconduct Statistics - Failed to show misconduct \"欠交功課\" statistics
[Fix]Teacher App Mail<br>1. android - email subject : \"測試普通電郵\" > 有一條灰色線, ios 正常<br>2. 按了星星後返回再按同一封電郵, 星星又熄了<br>3. ios - show two loading effect at the same time<br>4. android - reply an email + add an image attachment > weird image name, cannot display, ios - remove one image = remove all<br>5. add \"只限內聯網電郵\" stting<br>6.save mail with attachment and CC or BCC but fail to display; save draft again not generate a new mail;reopen draft don't add signature<br>7.mail in send folder with multi receivers reply in one tag<br>8.hide the title in email list when it is too long
[Improve] App login status report: keyword search by UserLogin also [Case#G71109]
[Improve] App login status report add push message status filtering [Case#G71109]
[Improve] App: add subject name in homework list request
[Fix] App cannot login for LDAP client [Case#T71743]
[Fix] eNotice - fixed the page (cannot zoom in zoom out now)
[Improvement] Take attendance -  can follow the basic settings : attendance mode(can set only am, only pm or whole day )
[New] eEnrolment - take attendance for clubs and activities
[New] Teacher App: Teacher status module
[New] Teacher App: Student status module"
);

$version_critical["ip.2.5.5.8.1.0"]["3"] = array("2014-09-24 15:00",
"[Fix] PowerLesson App - In classroom lesson list, remove the extra ')' if the lesson plan without planned and post lesson date is not yet delivered
[Fix] PowerLesson - Student will be requested to leave lesson after they click refresh when using Chrome
[Improvement] Quiz - Show submit button for teacher to submit comment when Teacher view the quiz result of selected student from the activity status table
[Improvement] PowerLesson - add Category pull down in Lesson Plan List
[Improvement] PowerLesson<br>1. Improve the wording of filter \"全部\" of lesson plan list to be \"我所使用及製作的教案\"<br>2. after copy lesson plan, go to \"我任教的教案\"<br>3. show \"Teacher-In-Charge\" in the lesson plan list for filter \"我製作的教案\" 及 \"其他老師的教案\"<br>4. show a icon in front of the lesson title to indicate which lesson plan is set to group lesson
[Localization] Request to change the wording of \"Public\" & \"Private\" in Forum List<br>- the meaning / usage of \"Public\" & \"Private\" is different from that of assessment and caused confusion<br> - Public > All users  所有使用者<br>- Private > Designated users 指定使用者
[Improvement] Add record delete log when deleting a student handin record
[Improvement] eHomework - Let homework page to show image without clicking the link(APP Webview)
[Improvement] eLibPlus - Accession Date and then ACNO sorting in accession report
[Fix] eLibPlus - Fail to get the correct number of group member
[Improvement] eLibPlus - change the position of \"ESC\" button in circulation page
[Fix] eLibPlus - student can auto change book status \"不可借閱\" > \"可借出\" after \"展出中\"
[Fix] eLibPlus - Fail to check admin right of the page
[Fix] eLibPlus - fail to import book cover if the book's ACNO starts from numeric
[Fix] eLibPlus - print patron barcode label sorting by class
[Fix] App - Failed to delete suspended account in app
[Improvement] App - Change wordings from \"Parent App\" to \"eClass App\" in different modules
[Fix] eClass App - Parent without English Name will cause iOS app force close [Case#U67632]
[Fix] eClass App - Login status report failed to go to next page [Case#X68394]
[Fix] Teacher App - eCircular push message wording changed from \"eClass App\" to \"eClass Teacher App\"
[New] Teacher App - Teacher App user login status report
[Improvement] Account Mgmt - Student Account<br>- Add Home Tel column when import and export student account<br>- Edit remarks of import page
[Fix] School Calendar - Holiday / Event - Add Remark and new error message when import Holiday / Event <br>- not Group Event with Group ID<br>- Group Event without Group ID
[Fix] eEnrolment - Club Process - Number of Clubs to be enrolled incorrect
[Improvement] eEnrolment - Club Settings - Add delete log when remove all enrollment records
[Fix] eEnrolment - Club Process - Normal Export and Export with Priority 
[Fix] Account Mgmt - Missing roles in all groups after user edit account
[Improvement] iMAIL(app)<br>1. delete attachment<br>2. Imail add trash folder(delete when trash folder, move when other folder<br>3. 星星 icon > valign = top<br>4.  missed signature appended when reply/compose/forward an email and also add loading for  send button<br>5.  Add attachment icon in email list if the email has attachment<br>6. add loading icon for each page of each actions(email-list)<br>7. compose > empty all fields > send > add checking
[Fix]  iMAIL(app) <br>1. delete attach and then add,no response<br>2. html codes (like &nbsp;) are displayed after replied    3. self folder imail cannot show content
[Fix] Account Mgmt - Parent account fail to assign students if student UserID is similar.
[Fix] Digital Archive - Fix emty file name in Chrome when download single file.
[Improvemnt] eAttendance - new cardapi for client program to download studentinfo.csv
[Fix] iMail plus - Chinese named folder fail to download attachment
[Fix] iMail plus - separate with webmail's spam, blacklist and whitelist api.
[Fix] Add the students in Subject Group into classroom"
);

$version_critical["ip.2.5.5.8.1.0"]["2"] = array("2014-09-10 09:00",
'[Improved] eLibPlus - let export Marc21 can handle huge data 
[Fix] eLibPlus - let export Marc21 can handle huge data 
[Improved] eAttendance - Change the Acknowledgment Status after acknowledge the record
[Improvement] iTextbook - update flag name: $sys_custom[\'enable_gvlistening\'] to $sys_custom[\'enable_itextbook_question_input\']
[Fix] Student Attendance - bring AM absent reason to PM
[Fix] iMail plus - Add checking on reply to email address if inputted
[Fix] iMail plus/iMail - create missing email account if fail to create with osapi.
[Fix] ePayment - Outstanding payment records, no class students are sorted by name.
[Fix] Account Mgmt - When remove/archive student accounts, checking on unreturned books and unpay overdue payment of eLibrary
[Fix] Teacher App - iMail and Take Attendance, wrongly re-directed to portal page if idle for long time
[Fix] App - iMail cannot view attachment
[Fix] App - iMail failed to view correct personal folder email [Case#P67180]
[Fix] Message Centre - cannot select teacher in the send push message to teacher page if the client only have purchased eClass Teacher App
[Fix] App - fixed cannot load school logo if the filename of school logo has "space" [Case#G67165]
[Improvement] App - eNotice, School News and iMail web-view display improvement. Able to scroll left and right if the content is too wide [Case#S67069]
[Improvement] App - display no license rather than server busy if the client did not purchase app (work with app v1.41 or later)
[Improvement] Message Centre - remove the user DeviceID if GCM returned "Unregistered" error (user has deleted app / has 30 days did not connected to google)
[Fix] App - Failed to login the app if client server has enabled json php module [Case#C67095]'
);

$version_critical["ip.2.5.5.8.1.0"]["1"] = array("2014-08-29 18:00",
"[Fix] eBookreader - CUP eBook layout improvement
[Fix] PowerLesson - Fail to show current selected quiz in resources table when switching from Individual Quiz to Different Quiz for Different Group Mode
[Fix] PowerLesson - Fail to show the image of the percentage bar when viewing question statistic in Quiz Activity
[Fix] PowerLesson - A Long Poll is started immediately after abort by calling longPoll.disconnect()
[Fix] PowerLesson - Support editing the quiz paper from resources table
[Fix] PowerLesson - The ordering activity status table of Quiz Activity (Different Quiz for Different Group Mode) is incorrect if there are any status change
[Fix] PowerLesson - Trim the response and comment before verifying user input in Discussion Activity
[Fix] PowerLesson - Fail to update the student status table even they have changed their status in Discussion Activity
[Fix] PowerLesson - Fail to submit Instant Reply and show 'Please input your comment.' even user has input some text in the textarea in Discussion Activity
[Fix] PowerLesson - <br>1) The Disable PowerSpeech Setting will be removed if teacher try to modify the group settings (Add member or Remove member) <br>2) The relationship between the eContent, Discussion, Quiz resource and the group are not removed even the group is removed
[Fix] iMail plus - Separate webmail and iMail plus's spam and anti-virus api.<br>iMail/iMail plus/iFolder osapi modify file permission patch<br>Reupload the changed osapi files.
[Fix] iMail plus - Fail to print email in Chinese name folder. 
[Fix] Digital Archive - Fix DA root directory fail to be created problem.
[New] App - Apply Leave required password settings API
[Fix] App - Delete scheduled push message - delete message from central server as well
[Improved] App - Sign eNotice push message, change wording \"eClass Parent App\" to \"eClass App\"
[New] App - Teacher app staff attendance tap card time API
[New] App - Central server control API
[Improved] App - Cache license status in school database for one hour
[Improved] App - Display School News of parent's group
[Improved] Message Center - can send push message by import even if there are invalid rows (skip the invalid records and send the valid records)
[Improved] Message Center - warn the user if the target parent has not logged in eClass App yet when sending message by import
[Improved] App - Multiple request API for app homepage to reduce number of requests sent when updating app homepage data
[New] App - Teacher App eCircular API
[Improved] App - new push message json format to use \"UserID\" instead of \"UserLogin\" to verify user
[Improved] App - in the image settings page, added suggested resolution of school badge and background image
[New] App - script to check the connection between school server and central server
[Improved] App - added flag to control the sending of attendance tap card push message
[Improved] App - added flag to control the display of attendance time in app homepage
[Fix] eLibPlus - fail to import items records
[Fix] eLibPlus - display the html special character in barcode label
[Fix] eLibPlus - Fail to get a warning msg in Circulation page when the library is closing
[Improved] eLibPlus - Marc21 import and export support Illustration,Height,Accompanying material
[Improved] eLibPlus - Can continue import even if has duplicate acno or barcode
[Improved] eLibPlus <br>1. Let marc21 import and export support Resources Type, Country and Language<br>2. Change the 991|t to Circulation Type<br>3. Combine multiple 440|v
[Fix] eLibPlus - Can`t trim the first line of the file
[Improved] eAttendance - Add Acknowledge page to apply leave (app), that can edit and apply the leave applications.
[Fixed] staff failed to view student notice in eService [Case#Z66399]"
);

$version_critical["ip.2.5.5.5.1.0"]["5"] = array("2014-07-23 12:00",
"[Fix] PowerLesson - Image uploaded at discussion activity using iPad is shown in wrong orientation when viewing in PC. 
[Localization] PowerLesson - change the Disable PowerSpeech wording from 'Disable' to 'Enable'
[Localization] PowerLesson - change the activity duration wording from 'lessonTime / 課節長度' to 'activityTime / 活動長度' 
[Localization] Add description remark to all activities 'Teacher can see only'
[Fix] Fail to copy content of PowerBoard, PowerVoice, PowerConcept, File Upload Actvitiy if the creator and the teacher of the lesson plan are differnet
[Fix] PowerLesson - Fail to unpack PowerBoard which contains images
[Fix] PowerLesson - Fail to pack the setting of Discussion and eContent Activity completely (i.e. Setting of Disable PowerSpeech Can't be packed)
[Fix] PowerSpeech - interpert imporperly if content contains '&nbsp;' 
[Fix] PowerLesson - The Disable PowerSpeech Setting will be removed if teacher has change the lesson plan setting (e.g. change the target, change from group lesson plan to individual lesson plan and vice versa)
[Fix] PowerLesson - The relationship between the eContent or Discussion resource and the group are not removed even the group is removed");

$version_critical["ip.2.5.5.5.1.0"]["4"] = array("2014-07-09 11:10",
"[Fix] Fix the js error caused by the double quote when calling returnHtmlMETA() in js checkHTML()
T62371 - 保良局顏寶鈴書院 - Cannot set default of self account
S63574 - 顯理中學 - Fail to import average mark
B62760 - 利瑪竇中學(中學部) - suggestion for SLP exported file
[Fix] Fix the js error which failed to get static LP file name correctly.");

$version_critical["ip.2.5.5.5.1.0"]["3"] = array("2014-06-25 15:30",
"[Fix] Fix the eclass system security");

$version_critical["ip.2.5.5.5.1.0"]["2"] = array("2014-06-13 09:30",
"[Fix] The Disable PowerSpeech Setting will be removed if teacher has change the lesson plan setting (e.g. change the target, change from group lesson plan to individual lesson plan and vice versa)
[Fix] The relationship between the eContent or Discussion resource and the group are not removed even the group is removed
[Fix] Fail to use powerspeech on other modules except PowerLesson");

$version_critical["ip.2.5.5.5.1.0"]["1"] = array("2014-06-09 14:00", 
"2014-0605-1629-18066  A62769 - 世界龍岡學校劉皇發中學 - Failed to save student's OLE data
2014-0605-0937-22066  V62712 - 蘇浙公學 - Failed to save student's OLE data
[Fix] PowerBoard - improve the 'Set to background' feature to keep the original size of object after set to background
[Fix] PowerLesson - wrong spelling of 'functionSerializedObj' was found which might caused abnormal display when teacher release the quiz answer in quiz activity");

$version_critical["ip.2.5.5.3.1.0"]["3"] = array("2014-03-13 16:32", 
"[iPortfolio] fixed security issue that student membertype switched to teacher when drafting a learning portfolio
[iPortfolio] fixed missing images and css error in learning portfolio");

$version_critical["ip.2.5.5.3.1.0"]["2"] = array("2014-03-13 10:36", 
"[eNotice] Fixed the failure to view the notice by teachers in eService. (case:B59802)");


$version_critical["ip.2.5.5.3.1.0"]["1"] = array("2014-03-12 12:07", 
"[eClass] Fixed improper checking on getting paramater in file upload page in eClass > Resources > eClass Files [Case#D59807]
[PowerLesson] Fixed the UI Problem of Attachment Bubble in the PowerLesson Discussion Reply Box
[PowerBoard] Fixed the ratio of width and height to be 1:1 in the preview image of HTML5 PowerBoard
[iPortfolio] Fixed failure to generate JUPAS academic result [Case#V59594]
[iPortfolio] Fixed: subject name is beyond the table border if the subject name is too long for PDF format SLP report [Case#S59671]");










#####################################################################################################################################
## end of patch logs!
#####################################################################################################################################

$originalJustWantVersionData = isset($JustWantVersionData) && $JustWantVersionData;

$JustWantVersionData = true;
include_once("includes/version.php");

$JustWantVersionData = $originalJustWantVersionData;

foreach($version_critical as $mainVersion => $mainVersionData){
    foreach($mainVersionData as $criticalDeployNo => $criticalDeployData){
        $versions[] = array($mainVersion.'.'.$criticalDeployNo, $criticalDeployData[0], 'critical deploy');
    }
}

function sortVersionsAryInDescendingOrder($a, $b){
    $aVersion = str_replace('ip.', '', $a[0]);
    $bVersion = str_replace('ip.', '', $b[0]);

    return version_compare($bVersion, $aVersion);
}

usort($versions, "sortVersionsAryInDescendingOrder");

if(!$JustWantVersionData){
    echo $versions[0][0];
}