<?php
/*
 * Editing by 
 * 
 * Modification Log:
 * 2016-05-20 (Paul)
 * 	- copied from intranetIP/pl/index.php for PowerFlip SaaS
 * 
 */
$PATH_WRT_ROOT = "../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/SecureToken.php");

# This page can be access by Android Only
if(!$sys_custom['power_lesson_app_web_view_all_platform'] && isset($userBrowser) && $userBrowser->platform!='Andriod'){
	header('location:/');
	die();
}


intranet_opendb();
$SecureToken = new SecureToken();

# PowerLesson App Web View Login
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="no-cache" http-equiv="Cache-Control">
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
		<title>eClass PowerFlip</title>
		<link rel="stylesheet" type="text/css" href="<?=$intranet_httppath?>/templates/<?=$LAYOUT_SKIN?>/css/powerlessonwebview.css"/>
		<script src="/templates/script.js" language="JavaScript"></script>
		<script src="/templates/jquery/jquery-1.3.2.min.js" language="JavaScript"></script>
		<script src="/templates/jquery/jquery.form.js" language="JavaScript"></script>
		<script>
			$(document).ready(function(){
				$('form#form1').submit(function(event){
					event.preventDefault();
					go_login();
				});
				
				$('#UserLogin').focus();
			});
			
			function checkLoginForm(formData, jqForm, options){
				var obj = document.form1;
				var pass=1;
				
				if(!check_text(document.getElementById('UserLogin'), "Please enter the user login."))
					pass = 0;
				else if(!check_text(document.getElementById('UserPassword'), "Please enter the password."))
					pass = 0;
					
				if(pass) {
					// obj.UserLogin.value = document.getElementById('UserLoginInput').value;
					// obj.UserPassword.value = document.getElementById('UserPasswordInput').value;
					// obj.submit();
					return true;
				}
				return false;
			} 
			
			function loginSuccess(responseObj, statusText, xhr, $form){
				if(responseObj.IsSuccess){
					var obj = document.form1;
					obj.action = '/login.php';
					obj.submit();
				}
				else{
					var alertMsg = '';
					switch(responseObj.Result){
						case 401: alertMsg = '<?=$Lang['PowerLessonAppWeb']['ErrorDesc'][401]?>'; break;
						case 402: alertMsg = '<?=$Lang['PowerLessonAppWeb']['ErrorDesc'][402]?>\n\r<?=$Lang['PowerLessonAppWeb']['ContactEClassForInfo']?>'; break;
						case 801: alertMsg = '<?=$Lang['PowerLessonAppWeb']['ErrorDesc'][801]?>\n\r<?=$Lang['PowerLessonAppWeb']['ContactEClassForInfo']?>'; break;
						case 802: alertMsg = '<?=$Lang['PowerLessonAppWeb']['ErrorDesc'][802]?>\n\r<?=$Lang['PowerLessonAppWeb']['ContactEClassForInfo']?>'; break;
						case 803: alertMsg = '<?=$Lang['PowerLessonAppWeb']['ErrorDesc'][803]?>\n\r<?=$Lang['PowerLessonAppWeb']['ContactEClassForInfo']?>'; break;
					}
					
					if(alertMsg){
						alert(alertMsg);
					}
					return false;
				}
			}
			
			function go_login(){
				$('#form1').ajaxSubmit({
					url			 : 'login.php',
					type		 : 'post',
					dataType	 : 'json',
					beforeSubmit : checkLoginForm,
					success 	 : loginSuccess,
					clearForm	 : false,
					resetForm	 : false
				});
			}
			
		</script>
	</head>
	<body class="pl_login">
		<div id="wrapper">
			<div class="login_box PowerFlip">
				<form id="form1" name="form1" method="post" action="">
					<ul> 
	            		<li>
	            			<span><?=$Lang['PowerLessonAppWeb']['Username']?> :</span>
	            			<input id="UserLogin" name="UserLogin" type="text" class="inputbox"/>
	            		</li>
	       		  		<li>
	       		  			<span><?=$Lang['PowerLessonAppWeb']['Password']?> :</span>
							<input id="UserPassword" name="UserPassword" type="password" class="inputbox"/>
						</li>
	                	<li>
	                		&nbsp;
							<!--
	                		<input id="keepSignIn" name="" type="checkbox" value="" />
							<label for="keepSignIn">Keep me signed in</label>
							-->
						</li>
	      			</ul>
	      			<? /* *** Old login button
			  		<a href="javascript:void(0)" onclick="go_login()"><?=$Lang['PowerLessonAppWeb']['Login']?></a>
			  		*/ ?>
			  		<input type="submit" id="submitBtn" name="submitBtn" class="login_input" value="<?=$Lang['PowerLessonAppWeb']['Login']?>" />
			  		<input type="hidden" id="from_pf" name="from_pf" value="1">
			  		<input type="hidden" id="target_url" name="target_url" value="/home/pl/index.php">
			  		<?=$SecureToken->WriteFormToken()?>
		  		</form>
		  	</div>
    	</div>
	</body>
</html>
<?
intranet_closedb();
?>