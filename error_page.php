<?php

switch ($_GET["errno"])
{
    case "11":
        $msg = "As you have already attempted logins but failed for too many times, please try again later";
        if (isset($_GET["duration"]))
        {
            $msg .= " (i.e. ".$_GET["duration"] ." mins)";
        }
        $msg .= ".";
        echo $msg;
        break;
    default:
        header("HTTP/1.0 404 Not Found");
        break;
}
?>