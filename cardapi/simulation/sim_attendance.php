<?
####################################################################
# Created by : Ronald Yeung
# Creation Date : 20070824
####################################################################
# Version updates
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
include_once("../attendance/attend_functions.php");
######################################################
# Param : (From QueryString)
#    Action
#          Empty / 0 : Init checking
#          1 : Enter school in morning
#          2 : Leave school
#    CardID
#    dlang
######################################################


if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("../attendance/attend_lang.php");
include_once("../attendance/attend_const.php");

# Check IP
if (!isIPAllowed())
{
     echo CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'];
     intranet_closedb();
     exit();
}

/*
1. Select query from user table
2. Select ClassID from ClassName
3. Select from Time table mode
4. Create a table if not exists
5. Select query from Daily log table
6. Select query from timetable
7. if Action==1, insert query; if Action==2, update query
*/

$year = date('Y');
$month = date('m');
$sim_daily_log = "SIM_SMARTCARD_DAILY_LOG_".$year."_".$month;

if(($action == "0")||($action == ""))
{
	$sql = "DROP TABLE $sim_daily_log";
	$db_engine->db_db_query($sql);
	
	$sql = "CREATE TABLE IF NOT EXISTS $sim_daily_log (
	 RecordID int(11) NOT NULL auto_increment,
	 UserID int,
	 DayNumber int,
	 InSchoolTime time,
	 LeaveSchoolTime time,
	 DateInput datetime,
	 DateModified datetime,
	 PRIMARY KEY (RecordID),
	 UNIQUE UserDay (UserID, DayNumber)
	)";
	
	$db_engine->db_db_query($sql);
	
	if($db_engine->db_affected_rows($sql) == 0)
	{
		$OutputToTerminal = 1;
	}
	else
	{
		$OutputToTerminal = 0;
	}
}


if($action==1)
{
	$sys_current_year = date("Y");
	$sys_current_day = date("d");
	$sys_current_month = date("m");
	$sys_current_time = date("H:i:s");
	$inTime = date("H:i:s");

	# Get UserInfo
	$namefield = getNameFieldByLang();
	$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
	$temp = $db_engine->returnArray($sql,6);
	list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];

	# Get ClassID
	$sql = "SELECT ClassID FROM INTRANET_CLASS WHERE ClassName = '$targetClass'";
	$temp = $db_engine->returnVector($sql);
	$targetClassID = $temp[0];
	
	# Get The Attendance Mode (2 - No Need Attendance, 1 - Class Time Table, 0 - School Time Table)
	$sql = "SELECT Mode FROM CARD_STUDENT_CLASS_SPECIFIC_MODE WHERE ClassID = '$targetClassID'";
	$temp = $db_engine->returnVector($sql);
	$mode = $temp[0];
	
	if ($mode == 2)
	{
		//echo output_env_str(CARD_RESP_NO_NEED_TO_TAKE."###".$attend_lang['NoNeedToTakeAttendance']."###$targetUserLogin");
		echo "err_NoNeedToTakeAttendance";
		intranet_closedb();
		exit();
	}
	
	$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
	$time_table_mode = $time_table;         ### 0 - Time Slot, 1 - Time Session
	
	if($targetUserID!="")
	{
		/*  Get Reminder Message */
		if ($getremind)
		{
			$sql = "SELECT ReminderID, Reason
			            FROM CARD_STUDENT_REMINDER
			            WHERE StudentID = $targetUserID AND DateOfReminder = '$today'";
			$temp = $db_engine->returnArray($sql,2);
		
			if (sizeof($temp)==0 || $temp[0][0] == "")
			{
				$t_remind_id = 0;
				$t_remind_msg = $reminder_null_text;
			}
			else
			{
				$t_remind_id = 0;
				$t_remind_msg = "";
				$delim = "";
				for($i=0; $i<sizeof($temp); $i++)
				{
					list($s_remind_id, $s_remind_msg) = $temp[$i];
					$t_remind_msg .= $delim.$s_remind_msg;
					$delim = "\n";
				}
				$t_remind_id = $s_remind_id+0;
			}
		}
		else
		{
			$t_remind_id = 0;
			$t_remind_msg = $reminder_null_text;
		}
		
		# Get Time Boundaries
		# Get Cycle Day
		$sql = "SELECT TextShort FROM INTRANET_CYCLE_DAYS WHERE RecordDate = '$today'";
		$temp = $db_engine->returnVector($sql);
		$cycleDay = $temp[0];
		
		
		# Get Week Day
		$weekDay = date('w');
		
		# Get Class-specific time boundaries
		
		# Get Special Day Settings
		$sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
		            TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
		            NonSchoolDay
		            FROM CARD_STUDENT_SPECIFIC_DATE_TIME
		            WHERE RecordDate = '$today'
		                  AND ClassID = '$targetClassID'
		            ";
		$temp = $db_engine->returnArray($sql,6);
		list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
		
		if (sizeof($temp)==0 || $ts_recordID == "")
		{
			if ($cycleDay != "")
			{
				$conds = " OR (a.DayType = 2 AND a.DayValue = '$cycleDay')";
			}
		
			 $sql = "SELECT a.RecordID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
			                TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
			                a.NonSchoolDay
			                FROM CARD_STUDENT_CLASS_PERIOD_TIME as a
			                     LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassID = b.ClassID
			                WHERE b.ClassName = '$targetClass' AND
			                  (
			                    (a.DayType = 1 AND a.DayValue = '$weekDay') $conds
			                      OR
			                    (a.DayType = 0 AND a.DayValue = 0)
			                  )
			                  ORDER BY a.DayType DESC
			                  ";
			 $temp = $db_engine->returnArray($sql,6);
			 list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
			 if (sizeof($temp)==0 || $ts_recordID == "")
			 {
				# Get Special Day based on School
				$sql = "SELECT RecordID, TIME_TO_SEC(MorningTime), TIME_TO_SEC(LunchStart),
				            TIME_TO_SEC(LunchEnd), TIME_TO_SEC(LeaveSchoolTime),
				            NonSchoolDay
				     FROM CARD_STUDENT_SPECIFIC_DATE_TIME
				     WHERE RecordDate = '$today'
				           AND ClassID = 0
				           ";
				$temp = $db_engine->returnArray($sql,6);
				list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
				
				if (sizeof($temp)==0 || $ts_recordID == "")
				{
					# Get School Settings
					$sql = "SELECT a.SlotID, TIME_TO_SEC(a.MorningTime), TIME_TO_SEC(a.LunchStart),
					            TIME_TO_SEC(a.LunchEnd), TIME_TO_SEC(a.LeaveSchoolTime),
					            a.NonSchoolDay
					     FROM CARD_STUDENT_PERIOD_TIME as a
					          WHERE (a.DayType = 1 AND a.DayValue = '$weekDay') $conds OR (a.DayType = 0 AND a.DayValue = 0)
					          ORDER BY a.DayType DESC
					     ";
					$temp = $db_engine->returnArray($sql,6);
					list($ts_recordID, $ts_morningTime, $ts_lunchStart, $ts_lunchEnd, $ts_leaveSchool, $ts_nonSchoolDay) = $temp[0];
				}
			}
		}
		#if ($ts_morningTime == "")
		if (sizeof($temp)==0 || $ts_recordID == "")
		{
			//echo 3;	 	# System Not Init
			//intranet_closedb();
			//exit();
		}
		
		if ($ts_nonSchoolDay)
		{
			//echo 4; 	# Non School Day
			//intranet_closedb();
			//exit();
		}
		
		$sql = "INSERT IGNORE INTO 
							$sim_daily_log 
							(UserID, DayNumber, InSchoolTime, DateInput, DateModified) 
				VALUES
							('$targetUserID', $sys_current_day, '$inTime', NOW(), NOW())";
		
		$db_engine->db_db_query($sql);
		
		if ($db_engine->db_affected_rows($sql) == 1)
		{
			$OutputToTerminal = 1; ### Arrive On Time
		}
		else
		{
			//$sql = "UPDATE $sim_daily_log SET InSchoolTime = '$inTime', DateModified = NOW() WHERE UserID = '$targetUserID' AND DayNumber = $sys_current_day";
			//$db_engine->db_db_query($sql);
			$OutputToTerminal = 2; ### Login Already
		}
	}
	else
	{
		$OutputToTeminal = 0; ### Invalid Card ID
	}
}

echo $OutputToTerminal;

intranet_closedb();
?>

