<?php
// Editing by 
/************************************************************* Change log ******************************************************************************
 * 2014-11-05 (Carlos): Modified checkSession($key), changed key file directory from /tmp to /file/ePaymentTerminal catering cloud server sharing hosts
 *******************************************************************************************************************************************************/
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("functions.php");

intranet_opendb();

# Check IP
$ip_ok = isIPAllowed();
if (!$ip_ok) exit();


if ($ip_ok)
{
    $now = time();
    $secret = md5(session_id().$now);
    $dir = $intranet_root."/file/ePaymentTerminal";
    if(!file_exists($dir) || !is_dir($dir)){
     	mkdir($dir);
    }
    //$path = "$payment_keyfile_temp/payment_ip_$clientIP"."_t$TerminalID";
    $path = "$dir/payment_ip_$clientIP"."_t$TerminalID";
    write_file_content("$now\n$secret",$path);
} else {
        echo "Invalid IP address. Action cancelled.";
        exit();
}

echo $secret;
intranet_closedb();
?>
