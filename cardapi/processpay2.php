<?
########## Updated Log ##########
##
##	Date:		16/09/2008
##	By:			Ronald
##	Details: 	Modify the get payment item query, so now student can only pay the
##				active payment items
##	
#################################

include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

#####
# return:
# 0 if CardID no match, no need to pay, amount not enough
# 1 if successful
######
if (!$payment_api_open_userpay)
{
     echo "-2 ";
     exit();
}
# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,1))
{
     echo "-1 ";
     exit();
}

$username = getUsername($key);

if ($action == 1)
{
    $isPaymentAction = true;
}

if ($CardID == "")
{
    # Card not registered
    echo "0 ";
    intranet_closedb();
    exit();
}

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             , INTRANET_USER READ
             , PAYMENT_PAYMENT_ITEMSTUDENT WRITE
             , PAYMENT_PAYMENT_ITEMSTUDENT as a WRITE
             , PAYMENT_PAYMENT_ITEM WRITE
             , PAYMENT_PAYMENT_ITEM as b WRITE
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);

# Retrieve UserID
$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID = '$CardID' AND RecordType = 2 AND RecordStatus = 1";
$temp = $lpayment->returnVector($sql);
$uid = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo "-1 ";
    $text_response = "";
}
else
{
    # Retrieve a list of payment items
    $sql = "SELECT a.PaymentID, b.Name, a.Amount FROM
                   PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON (a.ItemID = b.ItemID AND b.RecordStatus = 0)
                   WHERE a.StudentID = '".IntegerSafe($uid)."'  AND a.RecordStatus = 0 AND b.StartDate <= CURDATE()
                   ORDER BY b.PayPriority, b.EndDate, b.DateInput";
    $payments = $lpayment->returnArray($sql,3);

    if (sizeof($payments)==0)
    {
        echo "0 ";
        exit();
    }

    # Retrieve Balance
    $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '".IntegerSafe($uid)."'";
    $temp = $lpayment->returnVector($sql);
    $balance = $temp[0];

    $count_paid = 0;
    $count_unpaid = 0;
    $text_paid = "";
    $text_unpaid = "";
    $amount_paid = 0;
    $amount_unpaid = 0;
    for ($i=0; $i<sizeof($payments); $i++)
    {
         list($paymentID, $itemname, $amount) = $payments[$i];
         $str_amount = number_format($amount,2);
         
	     $balanceAfter = $balance - $amount;
     
	     if (abs($balanceAfter) < 0.001)    # Smaller than 0.1 cent
	     {
	         $balanceAfter = 0;
	     }                   
	     
         //if ($balance >= $amount)
         if($balanceAfter>=0)
         {
             $balance -= $amount;
             $text_paid .= "$itemname $ $str_amount\n";
             $count_paid++;
             $amount_paid += $amount;
             if ($isPaymentAction)
             {
                 # Update records
                 # Payment Record
                 $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = 1 WHERE StudentID = '".IntegerSafe($uid)."' AND PaymentID = '".IntegerSafe($paymentID)."'";
                 $lpayment->db_db_query($sql);
                 $affected = $lpayment->db_affected_rows();
                 if ($affected == 0)    # No updates then rollback
                 {
                     $balance += $amount; # Add back
                     $count_paid--;
                     $amount_paid -= $amount;
                 }
                 else     # Update Other tables
                 {
                     # Transaction Record
                     $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET PaidTime = NOW() WHERE StudentID = '".IntegerSafe($uid)."' AND PaymentID = '".IntegerSafe($paymentID)."'";
                     $lpayment->db_db_query($sql);

                     $balance_to_db = round($balance,2);
                     $balance_to_db = abs($balance_to_db);
                     
                     # Overall Transaction
                     $itemname = addslashes($itemname);
                     $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                             (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
                             VALUES
                             ($uid, 2,'$amount','$paymentID','$balance_to_db',NOW(),'$itemname')";
                     $lpayment->db_db_query($sql);
                     $logID = $lpayment->db_insert_id();
                     $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = '".IntegerSafe($logID)."'";
                     $lpayment->db_db_query($sql);

                     # Account balance
                     $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$balance_to_db'
                               ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                               WHERE StudentID = $uid";
                     $lpayment->db_db_query($sql);
                 }
             }
         }
         else
         {
             $text_unpaid .= "$itemname $ $str_amount\n";
             $count_unpaid++;
             $amount_unpaid += $amount;
         }
    }

    if ($dlang=="b5")
    {
        $msg_no_item = "沒有任何款項";
    }
    else
    {
        $msg_no_item = "No Items";
    }

    if ($count_unpaid==0)
    {
        $text_unpaid = $msg_no_item;
    }
    if ($count_paid==0)
    {
        $text_paid = $msg_no_item;
    }
    $text_response = "$count_paid###$amount_paid###$text_paid###$count_unpaid###$amount_unpaid###$text_unpaid###$balance ";

}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);
echo mb_convert_encoding($text_response,"BIG5","UTF-8");


intranet_closedb();
?>
