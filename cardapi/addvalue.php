<?
#############################
/*
# Modification Log:
# 2019-05-14 (Anna): Added IntegerSafe and ' to avoid sql injection
#
#2007-02-08 (Kenneth) : Allow teacher to use this API
#
#
*/
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

#####
# return:
# 0 if CardID no match, no need to pay, amount not enough
# 1 if successful
######
if (!$payment_api_open_addvalue)
{
     echo -1;
     exit();
}


# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,2))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo -1;
     exit();
}

$username = getUsername($key);

/*
Param:
$key - Secret Key
$amount - Amount to be paid
$item - Item name to be on the transaction record
$CardID - Card ID of the student
*/

# Check Arguments
$amount = (float)$amount;
if ($CardID == "" || $amount <= 0)
{
    echo -2;
    exit();
}

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             , INTRANET_USER READ
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             , PAYMENT_CREDIT_TRANSACTION WRITE
             ";
$lpayment->db_db_query($sql);

# Retrieve UserID
$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID = '$CardID' AND (RecordType = 2 OR RecordType = 1 ) AND RecordStatus = 1";
$temp = $lpayment->returnVector($sql);
$uid = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo -1;
}
else
{
    # No need to Check Amount enough
        # Increment balance
        $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount
                ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                WHERE StudentID = '".IntegerSafe($uid)."'";
        $lpayment->db_db_query($sql);
        # Insert Credit Transaction Record
        $sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (StudentID, Amount,RecordType, RecordStatus, AdminInCharge, TransactionTime)
                   VALUES ('".IntegerSafe($uid)."', '$amount',3,1,'$username', NOW())";
                   # RecordType 3 means �W�Ⱦ�
        $lpayment->db_db_query($sql);
        $trans_id = $lpayment->db_insert_id();

        # Update RefCode
        $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET RefCode = 'AVM$trans_id', DateInput = now()
                       WHERE TransactionID = $trans_id";
        $lpayment->db_db_query($sql);

        # Insert Transaction Record
        $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '".IntegerSafe($uid)."'";
        $temp = $lpayment->returnVector($sql);
        $balanceAfter = $temp[0];
        $detail = "�W�Ⱦ� / Add value Machine";

        $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details, RefCode)
               VALUES
               ('".IntegerSafe($uid)."', 1,'$amount',$trans_id,'$balanceAfter',NOW(),'$detail','AVM$trans_id')";
        $lpayment->db_db_query($sql);
        echo "1###".number_format($balanceAfter,2);
        echo "###".date('Y-m-d H:i:s');
}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);



intranet_closedb();
?>
