<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("functions.php");
intranet_opendb();

# Check IP
if (!isIPAllowed()) exit();

# Check Session key
if (!checkSession($key)) exit();

if ($type==1)
{
    if (isPaymentNoLogin())
        echo "1";
    else echo "0";
}
else if ($type==2)
{
    if (isPurchaseNoLogin())
        echo "1";
    else echo "0";
}
else echo 0;
intranet_closedb();
?>
