<?
include_once("../../plugins/staff_attend_conf.php");


$clientIP = getRemoteIpAddress();

function isIPAllowed()
{
         global $clientIP;
         global $intranet_root;

         # Check IP
         $ips = get_file_content("$intranet_root/file/staffattend_ip.txt");
         $ipa = explode("\n", $ips);
         $ip_ok = false;
         foreach ($ipa AS $ip)
         {
              $ip = trim($ip);
              if ($ip == "0.0.0.0")
              {
                  $ip_ok = true;
              }
              else if (testip($ip,$clientIP))
              {
                   $ip_ok = true;
              }
         }
         if (!$ip_ok)
         {
              return false;
         }
         return true;
}

function checkSession($key)
{
         global $staff_attend_keyfile_temp,$clientIP,$intranet_root,$TerminalID;

         $temp = trim(get_file_content("$intranet_root/file/staffattend_expiry.txt"));
         if ($temp!="" && is_numeric($temp))
         {
             $staff_attend_expiry = $temp;
         }
         # Check session key
         $keyfilepath = "$staff_attend_keyfile_temp/staffattend_ip_$clientIP"."_t$TerminalID";
         if (!is_file($keyfilepath))
         {
              return false;
         }

         $content = get_file_content($keyfilepath);
         $now = time();
         $data = explode("\n",$content);
         list ($ts,$oldkey) = $data;

         if ($oldkey != $key || ($now - $ts) > $staff_attend_expiry*60 )
         {
              return false;
         }
         return true;
}




?>