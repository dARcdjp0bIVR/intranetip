<?
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libpos.php");
include_once("../../includes/libfilesystem.php");
include_once("../functions.php");

$MacAddress = stripslashes(trim(urldecode($_REQUEST['MAC']))); // the client machine Mac Address

intranet_opendb();

$lfs = new libfilesystem();

//chdir("../../file/pos/");
chdir($intranet_root."/file/pos/");
$lfs->folder_new($MacAddress);
exec("tar -czvf ".OsCommandSafe($MacAddress)."/photo.tar.gz photo/*");

$data = $lfs->file_read($MacAddress."/photo.tar.gz");

ob_flush();
header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

header('Content-type: application/octet-stream');
header('Content-Length: '.strlen($data));

header('Content-Disposition: attachment; filename="photo.tar.gz";');

echo $data;


$POS = new libpos();
$POS->Update_Terminal_Last_Photo_Sync($MacAddress);
$lfs->file_remove($MacAddress."/photo.tar.gz");
intranet_closedb();
?>