<?
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libpos.php");
include_once("../functions.php");

intranet_opendb();
$POS = new libpos();

header("Content-type: text/plain; charset=utf-8");

/*
#####
# return:
-6 : Not item purchase request
-5 : SQL Error
-4 : Not Enough Inventory
-3 : Invalid key
-2 : Argument Error
-1 : No this student
0 : Not enough balance
1 : Successful transaction
######
*/

/*
Param:
$key - Secret Key
$amount - Amount to be paid
$SiteName - Site name Of Client Program
$CardID - Card ID of the student
$rand - random string
$InvoiceMethod - 0 (YYYYMMDDXXXX), ..... to be define later by client
$Details - Payment Item details
*/
$key = $_REQUEST['key'];
$amount = $_REQUEST['amount'];
$SiteName = urldecode($_REQUEST['SiteName']);
$CardID = $_REQUEST['CardID'];
$rand = $_REQUEST['rand'];
$InvoiceMethod = $_REQUEST['InvoiceMethod'];
$Details = $_REQUEST['Details'];

# Check Arguments
$string_amount = $amount;
$amount = (float)$amount;
if ($CardID == "" || $amount <= 0 || $SiteName == "")  # Only debit
{
	echo "-2";
	exit();
}

# Check secret key
$keysalt = array("1adkeoi7gk2");
$delimiter = "###";
$valid = false;

foreach ($keysalt as $t_key => $t_value)
{
	$string = $t_value.$delimiter.$string_amount.$delimiter.strtolower(urlencode($SiteName)).$delimiter.$CardID.$delimiter.$rand;
	$hashed_key = md5($string);
	if ($key==$hashed_key) {
	  $valid = true;
	}
}

if (!$valid)
{
	echo "-3";
	exit();
}

// convert item detail to array
$TransactionDetail = array();
$entries = explode('*****',$Details);
$all_entries_valid = true;
$values = "";
$delim = "";
for ($i=0; $i<sizeof($entries); $i++)
{
	$t_data = explode('***',$entries[$i]);
	if (sizeof($t_data)<3)
	{
		$all_entries_valid = false;
		break;
	}
	else
	{
		list($ItemID, $Quantity, $t_subtotal) = $t_data;
		$TransactionDetail[] = array("ItemID"=>$ItemID,"Quantity"=>$Quantity,"UnitPrice"=>$t_subtotal);
	}
}

if ($all_entries_valid) {
	$Result = $POS->Process_POS_TRANSACTION($CardID,$amount,$SiteName,$TransactionDetail,$InvoiceMethod);
	echo $Result;
}
else {
	echo "-2";
}

intranet_closedb();
?>