<?php
// Editing by 
// this page uses "COMET" Method to post item changes to client, 
// the page will not end unless client close the connection
/*
 * 2018-02-06 (Carlos): Added ConfirmBeforePay attribute to xml document root.
 * 2017-08-18 (Carlos): Added terminal status checking to pause certain terminals.
 * 2015-05-07 (Carlos): Added control parameter $FastUpdate for immediate reconnection after timeout.
 * 2015-03-11 (Carlos): Modified to end this script after certain time interval.
 */
//ignore_user_abort(1);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");

//header("Content-type: html/text; charset=utf-8");
$SiteName = stripslashes(trim(urldecode($_REQUEST['SiteName']))); // the terminal site name in setting
$MacAddress = stripslashes(trim(urldecode($_REQUEST['MAC']))); // the client machine Mac Address

intranet_opendb();
$POS = new libpos();

$FastUpdate = $_REQUEST['FastUpdate'] == '1'; // Skip the first time update of all items and inventory, for reconnection after timeout

$ts_interval = 60*60; // terminate this script after this interval
//$ts_interval = 30; // for testing shorten the time interval
if(isset($sys_custom['ePOS']['LongPollingTimeout']) && $sys_custom['ePOS']['LongPollingTimeout'] > 0){
	$ts_interval = intval($sys_custom['ePOS']['LongPollingTimeout']);
}
$ts_start = time(); // start time
$ts_end = $ts_start + $ts_interval; // expected end time

if($sys_custom['ePOS']['DiableServerBuffering']){ // cloud server require this handling
	// turn off proxy_buffering and fastcgi_buffering for nginx
	header('X-Accel-Buffering: no');
}
//session_write_close(); // end the script session access
$JustAfterMaintenance = $FastUpdate? false : true;
while (1==1) {
	
	$ts_current = time();
	if($ts_current > $ts_end) break;
	
	if (!$POS->Check_POS_Client_IP()) {
		echo "die\n";
		die();
	}
	else {
		$TerminalID = $POS->Log_Terminal_Connection_Info($SiteName,$MacAddress,$TerminalID); // update the last connection time for client program
		if ($TerminalID === FALSE) { 
			echo "duplicatesitename";
			echo "\n"; // if same site name found in same network, return error to client program and it will prompt user to input another
			die();
		}
		
		$x = '';
		$Settings = $POS->Get_POS_Settings();
		$TerminalRecords = $POS->Get_Terminal_Records(array('MacAddress'=>$MacAddress));
		if ($Settings['AllowClientProgramConnect'] == '0' || ($PrevTerminalIndependentCatSetting != $Settings['TerminalIndependentCat'] && !$FastUpdate) || (count($TerminalRecords)>0 && $TerminalRecords[0]['TerminalStatus']!='1')) {
			$ItemCount = array();
			$JustAfterMaintenance = true;
			echo 'maintenance';
			echo "\n";
		}
		else if ($POS->Check_Terminal_Need_To_Sync_Photo($MacAddress)) {
			echo 'downloadphotos';
			echo "\n";
		}
		else {
			$InventoryChanged = false;
			$PaymentMethod = $Settings['PaymentMethod']; // 1: Tap Card (default), 2: Student Selection
			$ConfirmBeforePay = $Settings['ConfirmBeforePay']; // For (2) Student Selection
			
			$Result = $POS->Get_Item_Category_Info_For_Client_Program($MacAddress);
			
			$x .= '<root type="'.(($JustAfterMaintenance)? "FullList":"InventoryChange").'" paymentmethod="'.$PaymentMethod.'" confirmbeforepay="'.($ConfirmBeforePay?'1':'0').'" nobalance="'.($sys_custom['ePayment']['KIS_NoBalance']?'1':'0').'">';
			for ($i=0; $i< sizeof($Result); $i++) {
				if ($JustAfterMaintenance) { // for program first load or first load after the maintenance, load full list to client
					if ($Result[($i-1)]['CategoryID'] != $Result[$i]['CategoryID']) {
						if ($i != 0) 
							$x .= '</category>';
						$x .= '<category name="'.urlencode($Result[$i]['CategoryName']).'" code="'.urlencode($Result[$i]['CategoryCode']).'" id="'.$Result[$i]['CategoryID'].'" fileext="'.$Result[$i]['CatExt'].'">';
					}
					if ($Result[$i]['ItemID'] != "") {
						$x .= '<item>';
						$x .= '<id>';
						$x .= $Result[$i]['ItemID'];
						$x .= '</id>';			
						$x .= '<name>';
						$x .= urlencode($Result[$i]['ItemName']);
						$x .= '</name>';
						$x .= '<barcode>';
						$x .= $Result[$i]['Barcode'];
						$x .= '</barcode>';
						$x .= '<unitprice>';
						$x .= $Result[$i]['UnitPrice'];
						$x .= '</unitprice>';
						$x .= '<itemcount>';
						$x .= $Result[$i]['ItemCount'];
						$x .= '</itemcount>';
						$x .= '<fileext>';
						$x .= $Result[$i]['ItemExt'];
						$x .= '</fileext>';
						$x .= '</item>';
					}
					
					if ($i == (sizeof($Result)-1)) {
						$x .= '</category>';
					}
				}
				else { // otherwise, just return inventory changed
					if ($ItemCount[$Result[$i]['ItemID']] != $Result[$i]['ItemCount']) {
						$x .= '<item>';
						$x .= '<id>';
						$x .= $Result[$i]['ItemID'];
						$x .= '</id>';
						$x .= '<itemcount>';
						$x .= $Result[$i]['ItemCount'];
						$x .= '</itemcount>';
						$x .= '</item>';
						$InventoryChanged = true;
					}
				}
				$ItemCount[$Result[$i]['ItemID']] = $Result[$i]['ItemCount'];
			}
			$x .= '</root>';
			
			if ($InventoryChanged || $JustAfterMaintenance) {
				echo $x;
			}
			else {
				echo 'nochange';
			}
			$JustAfterMaintenance = false;
			echo "\n";
		}
		flush();
		ob_flush();
		
		$PrevTerminalIndependentCatSetting = $Settings['TerminalIndependentCat'];
		sleep(3);
	}
}

intranet_closedb();
?>