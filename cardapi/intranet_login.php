<?php
// Editing by 
/*
 * 2019-07-15 (Carlos): Added $e_UserPassword to pass in the encrypted password due to security concern.
 */
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libauth.php");
intranet_opendb();
$li = new libauth();

if(isset($e_UserPassword)){
	$encryption_key = "vcIR4d8jGmHBNwGh";
	$UserPassword = aes_cbc_128_decrypt(base64_decode($e_UserPassword),$encryption_key);
}

if ($intranet_authentication_method == 'LDAP')
{
    # Different users using different LDAP dn string
    if ($ldap_user_type_mode)
    {
        # Get User Type
        $sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '$UserLogin'";
        $temp = $li->returnArray($sql,2);

        list($t_id, $t_user_type) = $temp[0];

        if (!is_array($temp) || sizeof($temp)==0 || $t_user_type == "" || $t_user_type < 1 || $t_user_type > 4)
        {
             die("0,Failed");
        }

        switch ($t_user_type)
        {
                case 1: $special_ldap_dn = $ldap_teacher_base_dn; break;
                case 2: $special_ldap_dn = $ldap_student_base_dn; break;
                case 3: $special_ldap_dn = $ldap_parent_base_dn; break;
                default: $special_ldap_dn = $ldap_teacher_base_dn;
        }
    }


    $lldap = new libldap();
    $lldap->connect();
    if (!$lldap->validate($UserLogin,$UserPassword))
    {
         $ID = 0;
    }
    else
    {
    }
}
else    # Normal authentication
{
    $ID = $li->validate($UserLogin, $UserPassword);
}

if($ID==0){
        die("0,Failed");
}else{

      # Check User Type
      if ($UserType != "")
      {
          $sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '$ID'";
          $temp = $li->returnVector($sql);
          if ($temp[0]!=$UserType)
          {
              die("0,Failed");
          }
      }

      $UserID = $ID;
      die("1,Successful");
}

intranet_closedb();
?>