<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdiscipline_api_cust.php");
//include_once("../functions.php");
intranet_opendb();

$api = new libdiscipline_api_cust();
# Check IP
if (!$sys_custom['eDiscipline']['TerminalProgram'] || !$api->isIPAllowed()){
	intranet_closedb();
	header('HTTP/1.0 401 Unauthorized', true, 401);
	exit;
}

$is_post_request = isset($_POST['task']);
$task = $is_post_request? $_POST['task'] : $_GET['task'];

if(!method_exists($api, $task) || !in_array($task,$api->apiMethods)){
	intranet_closedb();
	header('HTTP/1.0 400 Bad Request', true, 400);
	exit;
}

header("Content-Type: application/json;charset=utf-8");

$args = $is_post_request? $_POST : $_GET;
unset($args['task']);

$response = call_user_func_array(array($api,$task), $args);

echo $response;

intranet_closedb();
exit;
?>