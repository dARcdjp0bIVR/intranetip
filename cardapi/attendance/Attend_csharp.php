<?
// Editing by 
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20051207
####################################################################
# Version updates 								
# 20051207: Kenneth Wong
# 20061108: Kenneth Wong
#           Check whether last modified is card read or confirmation
# 20070503: Kenneth Wong
#           Add checking of "0" and "" in InSchoolTime to prevent accidentally 00:00:00 record written
# 20070503: Peter Ho
#			Staff attendance - Add removing the previous records when the new status and old status are not the same
# 20130419: Carlos
#			Return userlogin as 8th ### for displaying photo, ###2 to ###7 are no usage, free to use for future
# 2013-10-09: Carlos
#			Modified Club member checking query 
# 2013-11-13: Carlos
#			$sys_custom['SupplementarySmartCard'] - Added CardID2 and CardID3, get user by matching either CardID or CardID2 or CardID3
# 2014-07-17: Bill
#			For eEnrolment IP checking
# 2014-12-11: Bill
#			$sys_custom['SupplementarySmartCard'] - Added CardID4, get user by matching either CardID or CardID2 or CardID3 or CardID4
# 2014-12-19: Carlos
#			Removed the time range limit that can take attendance. Added $sys_custom['eEnrolment']['ActivityTakeAttendanceMinute'] for controlling how many minutes before/after event start time can take attendance for.
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");

intranet_opendb();
/*
include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");
*/
include_once("attend_functions.php");
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");

header("Content-type: text/plain; charset=utf-8");
if ($CardID == "")
{
    # Card not registered
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}

######################################################
# Param : (From QueryString)
#    username, passwd, activityID, CardID, sitename
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed('eEnrolment'))
{
     echo CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'];
     intranet_closedb();
     exit();
}
$libenroll = new libclubsenrol();
################################################
$directProfileInput = false;
$getremind = true;
$reminder_null_text = "No Reminder";
$repeatMinCheck = 1; // in minutes
################################################

# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, ".$namefield.", RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
if($sys_custom['SupplementarySmartCard']){
	$sql.= " OR CardID2='$CardID' OR CardID3='$CardID' OR CardID4='$CardID' ";
}
$temp = $db_engine->returnArray($sql,6);
list($targetUserID , $TargetName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];

# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}
else if ($targetType == 2)
{	
	$extra_info = "###2###3###4###5###6###".$targetUserLogin;
	
	# Get ClassID
	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE ClassTitleEN = '$targetClass' and AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
	$temp = $db_engine->returnVector($sql);
	$targetClassID = $temp[0];
		
	// check it is club or event first
	(str_replace('e', '', $activityID) == $activityID) ? $IsEvent = false : $IsEvent = true;
	$activityID = str_replace('e', '', $activityID);
	if ($IsEvent) {
		
		// get EnrolEventID
		$Sql = "
					SELECT
							EnrolEventID
					FROM
							INTRANET_ENROL_EVENT_DATE
					WHERE
							EventDateID = '".$activityID."'
				";
		$temp = $db_engine->returnVector($Sql,1);
		$EnrolEventID = $temp[0];

		
		
		if ($libenroll->enableUserJoinDateRange()) {
			$Sql = "
					SELECT
							StudentID,
							IF (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%', NULL, EnrolAvailiableDateStart) as EnrolAvailiableDateStart,
							IF (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%', NULL, EnrolAvailiableDateEnd) as EnrolAvailiableDateEnd,
							NOW() as today
					FROM
	     					INTRANET_ENROL_EVENTSTUDENT
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					EnrolEventID = '".$EnrolEventID."'
				";
			$temp = $db_engine->returnResultSet($Sql,1);
			
			$checkToday = strtotime(date("Y-m-d", strtotime($temp[0]["today"])));
			if (!empty($temp[0]["EnrolAvailiableDateStart"])) $AvailStart = strtotime(date("Y-m-d", strtotime($temp[0]["EnrolAvailiableDateStart"])));
			else $AvailStart = "";
			if (!empty($temp[0]["EnrolAvailiableDateEnd"])) $AvailEnd = strtotime(date("Y-m-d", strtotime($temp[0]["EnrolAvailiableDateEnd"])));
			else $AvailEnd = "";
			
			if ((empty($AvailStart) || $checkToday >= $AvailStart) && (empty($AvailEnd) || $checkToday <= $AvailEnd)) { 
				$EventStudentID = $temp[0]["StudentID"];
			} else {
				$EventStudentID = "";
			}
		} else {
			// check if the student is enrolled
			$Sql = "
					SELECT
							StudentID
					FROM
	     					INTRANET_ENROL_EVENTSTUDENT
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					EnrolEventID = '".$EnrolEventID."'
				";
			
			$temp = $db_engine->returnArray($Sql,1);
			$EventStudentID = $temp[0];
		}
		
		if ($EventStudentID != "") {				
					
			$Sql = "
	     			SELECT
	     					EventAttendanceID, RecordStatus, UNIX_TIMESTAMP(DateModified + INTERVAL " . $repeatMinCheck . " MINUTE) as NextDateModified
	     			FROM
	     					INTRANET_ENROL_EVENT_ATTENDANCE     					
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					EventDateID = '".$activityID."'
					";  
			$temp = $db_engine->returnArray($Sql,1);
			$EventAttendanceID = $temp[0]["EventAttendanceID"];
			$ATT_RecordStatus = $temp[0]["RecordStatus"];
			$ATT_NextDateModified = $temp[0]["NextDateModified"];
			
			if ($EventAttendanceID == "") {
				
				// check in time range
				$Sql = "
							SELECT
									EventDateID, UNIX_TIMESTAMP(ActivityDateStart) as ActivityDateStart, UNIX_TIMESTAMP(ActivityDateEnd) as ActivityDateEnd, UNIX_TIMESTAMP(NOW()) as currDateTime
							FROM
									INTRANET_ENROL_EVENT_DATE
							WHERE
									EventDateID = '".$activityID."'
								AND
									(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."') ";
				if(isset($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']) && $sys_custom['eEnrolment']['ActivityTakeAttendanceMinute'] > 0){
					$minute = intval($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']);
					$Sql .=	" AND (
									UNIX_TIMESTAMP(ActivityDateStart - INTERVAL $minute MINUTE) < UNIX_TIMESTAMP(NOW())
									AND
									UNIX_TIMESTAMP(ActivityDateEnd + INTERVAL $minute MINUTE) > UNIX_TIMESTAMP(NOW())
									) ";
				}
				
				$temp = $db_engine->returnArray($Sql,1);
				$ActivityDateStart = $temp[0]["ActivityDateStart"]; 
				$ActivityDateEnd = $temp[0]["ActivityDateEnd"];
				$currDateTime = $temp[0]["currDateTime"];
				$InTimeRange = (sizeof($temp) > 0);
				if ($InTimeRange) {

					$RecordStatus = ENROL_ATTENDANCE_PRESENT;
					if ($libenroll->enableAttendanceLateStatusRight() && ($currDateTime > $ActivityDateStart)) {
						$RecordStatus = ENROL_ATTENDANCE_LATE;
					}
					
					$Sql = "
								INSERT INTO
											INTRANET_ENROL_EVENT_ATTENDANCE
											(EventDateID, EnrolEventID, StudentID, SiteName, DateInput, DateModified, RecordStatus)
								VALUES
											('$activityID', '$EnrolEventID', '$targetUserID', '$sitename', now(), now(), '" . $RecordStatus . "')
							";
					$db_engine->db_db_query($Sql);

					$Sql = "
								SELECT
										EventAttendanceID, DateInput
								FROM
										INTRANET_ENROL_EVENT_ATTENDANCE
								WHERE
										EventDateID = '".$activityID."'
									AND
										StudentID = '".$targetUserID."'
							";
					$temp = $db_engine->returnArray($Sql,1);
					$time_recorded = $temp[0]["DateInput"];
					$EventAttendanceID = $temp[0]["EventAttendanceID"];

					$Sql = "INSERT INTO INTRANET_ENROL_EVENT_ATTENDANCE_LOG (EnrolEventID, EventDateID, EventAttendanceID, StudentID, CardAttendTime)";
					$Sql .= " VALUES (";
					$Sql .= "'" . $EnrolEventID . "', '" . $activityID . "', '" . $EventAttendanceID . "', '" . $targetUserID . "', NOW()";
					$Sql .= ")";
					$db_engine->db_db_query($Sql);
					
					//echo "1###$time_recorded"; //1,$time_recorded
					//echo "1###$ChineseName###$EnglishName###$targetClass###$targetClassNumber###$time_recorded";
					echo output_env_str("1###".$TargetName.$attend_lang['attend']." $time_recorded".$extra_info);
										
					intranet_closedb();
					exit();
					
				} else {
					
					$Sql = "
								SELECT
										EventAttendanceID, DateInput
								FROM
										INTRANET_ENROL_EVENT_ATTENDANCE
								WHERE
										EventDateID = '".$activityID."'
									AND
										StudentID = '".$targetUserID."'
							";
					$temp = $db_engine->returnArray($Sql,1);
					$time_recorded = $temp[0]["DateInput"];
					$EventAttendanceID = $temp[0]["EventAttendanceID"];
					
					$Sql = "INSERT INTO INTRANET_ENROL_EVENT_ATTENDANCE_LOG (EnrolEventID, EventDateID, EventAttendanceID, StudentID, CardAttendTime)";
					$Sql .= " VALUES (";
					$Sql .= "'" . $EnrolEventID . "', '" . $activityID . "', '" . $EventAttendanceID . "', '" . $targetUserID . "', NOW()";
					$Sql .= ")";
					$db_engine->db_db_query($Sql);
					
					# not in time range
					echo "0###".$attend_lang['not_in_time_range'].$extra_info; //0,$fail_msg
					intranet_closedb();
					exit();
				}
				
			} else {
				
				$Sql = "INSERT INTO INTRANET_ENROL_EVENT_ATTENDANCE_LOG (EnrolEventID, EventDateID, EventAttendanceID, StudentID, CardAttendTime)";
				$Sql .= " VALUES (";
				$Sql .= "'" . $EnrolEventID . "', '" . $activityID . "', '" . $EventAttendanceID . "', '" . $targetUserID . "', NOW()";
				$Sql .= ")";
				$db_engine->db_db_query($Sql);
				
				if ($libenroll->enableAttendanceEarlyLeaveStatusRight()) {
					// check in time range
					$Sql = "
							SELECT
									EventDateID, UNIX_TIMESTAMP(ActivityDateStart) as ActivityDateStart,
									UNIX_TIMESTAMP(ActivityDateEnd) as ActivityDateEnd, UNIX_TIMESTAMP(NOW()) as currDateTime
							FROM
									INTRANET_ENROL_EVENT_DATE
							WHERE
									EventDateID = '".$activityID."'
								AND
									(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."') ";

					if(isset($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']) && $sys_custom['eEnrolment']['ActivityTakeAttendanceMinute'] > 0){
						$minute = intval($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']);
						$Sql .=	" AND (
						UNIX_TIMESTAMP(ActivityDateStart - INTERVAL $minute MINUTE) < UNIX_TIMESTAMP(NOW())
						AND
						UNIX_TIMESTAMP(ActivityDateEnd + INTERVAL $minute MINUTE) > UNIX_TIMESTAMP(NOW())
						) ";
					}
					$temp = $db_engine->returnArray($Sql,1);
					$ActivityDateStart = $temp[0]["ActivityDateStart"];
					$ActivityDateEnd = $temp[0]["ActivityDateEnd"];
					
					$InTimeRange = (sizeof($temp) > 0);
					if ($InTimeRange) {
						if ($libenroll->enableAttendanceEarlyLeaveStatusRight() && ($currDateTime > $ATT_NextDateModified) ) {
							$Sql = "UPDATE INTRANET_ENROL_EVENT_ATTENDANCE set DateModified=NOW() WHERE EventAttendanceID='" . $EventAttendanceID . "'";
							if ($ATT_RecordStatus == ENROL_ATTENDANCE_PRESENT) {
								if ($currDateTime < $ActivityDateEnd) {
									$Sql = "UPDATE INTRANET_ENROL_EVENT_ATTENDANCE set RecordStatus='" . ENROL_ATTENDANCE_EARLY_LEAVE . "', DateModified=NOW() WHERE EventAttendanceID='" . $EventAttendanceID . "'";
								}
							}
							$db_engine->db_db_query($Sql);
							
							$Sql = "
									SELECT
											DateModified
									FROM
											INTRANET_ENROL_EVENT_ATTENDANCE
									WHERE
											EventDateID = '".$activityID."'
										AND
											StudentID = '".$targetUserID."'
								";
							$temp = $db_engine->returnVector($Sql);
							$time_recorded = $temp[0];
							
							//echo "1###$time_recorded"; //1,$time_recorded
							//echo "1###$ChineseName###$EnglishName###$targetClass###$targetClassNumber###$time_recorded";
							echo output_env_str("1###".$TargetName.$attend_lang['leave']." $time_recorded".$extra_info);
							exit;
						}
					}
				}
				
				# have attendance record
				echo "0###".$TargetName.$attend_lang['have_attendance_record'].$extra_info; //0,$fail_msg
				intranet_closedb();
				exit();
			}
			
		} else {
			echo "0###".$TargetName.$attend_lang['not_enroll'].$extra_info; //0,$fail_msg
			intranet_closedb();
			exit();
		}
		
	} else {
		
		// get GroupID
		$Sql = "
					SELECT
							INTRANET_ENROL_GROUPINFO.EnrolGroupID
					FROM
							INTRANET_ENROL_GROUPINFO
							LEFT JOIN
							INTRANET_ENROL_GROUP_DATE
							ON
							INTRANET_ENROL_GROUPINFO.EnrolGroupID = INTRANET_ENROL_GROUP_DATE.EnrolGroupID
					WHERE
							GroupDateID = '".$activityID."'
				";
		$temp = $db_engine->returnVector($Sql,1);
		$EnrolGroupID = $temp[0];

		
		if ($libenroll->enableUserJoinDateRange()) {
			$Sql = "
					SELECT
							iug.UserID as StudentID,
							IF (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%', NULL, EnrolAvailiableDateStart) as EnrolAvailiableDateStart,
							IF (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%', NULL, EnrolAvailiableDateEnd) as EnrolAvailiableDateEnd,
							NOW() as today
					FROM
	     					INTRANET_USERGROUP as iug
							Inner Join INTRANET_USER as iu ON (iug.UserID = iu.UserID)
	     			WHERE
	     					iu.UserID = '".$targetUserID."'
	     					AND
	     					iug.EnrolGroupID = '".$EnrolGroupID."'
							AND
							iu.RecordType = '2'
			";
			$temp = $db_engine->returnResultSet($Sql,1);
				
			$checkToday = strtotime(date("Y-m-d", strtotime($temp[0]["today"])));
			if (!empty($temp[0]["EnrolAvailiableDateStart"])) $AvailStart = strtotime(date("Y-m-d", strtotime($temp[0]["EnrolAvailiableDateStart"])));
			else $AvailStart = "";
			if (!empty($temp[0]["EnrolAvailiableDateEnd"])) $AvailEnd = strtotime(date("Y-m-d", strtotime($temp[0]["EnrolAvailiableDateEnd"])));
			else $AvailEnd = "";
				
			if ((empty($AvailStart) || $checkToday >= $AvailStart) && (empty($AvailEnd) || $checkToday <= $AvailEnd)) {
				$GroupStudentID = $temp[0]["StudentID"];
			} else {
				$GroupStudentID = "";
			}
		} else {
			// check if the student is enrolled
			$Sql = "
						SELECT
								iug.UserID as StudentID
						FROM
		     					INTRANET_USERGROUP as iug
								Inner Join INTRANET_USER as iu ON (iug.UserID = iu.UserID)
		     			WHERE
		     					iu.UserID = '".$targetUserID."'
		     					AND
		     					iug.EnrolGroupID = '".$EnrolGroupID."'
								AND
								iu.RecordType = '2'
					";  
			$temp = $db_engine->returnArray($Sql,1);
			$GroupStudentID = $temp[0];
		}


		if ($GroupStudentID != "") {				
					
			$Sql = "
	     			SELECT
	     					GroupAttendanceID, RecordStatus, UNIX_TIMESTAMP(DateModified + INTERVAL " . $repeatMinCheck . " MINUTE) as NextDateModifie
	     			FROM
	     					INTRANET_ENROL_GROUP_ATTENDANCE     					
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					GroupDateID = '".$activityID."'
					";  
			$temp = $db_engine->returnArray($Sql,1);
			$GroupAttendanceID = $temp[0]["GroupAttendanceID"];
			$ATT_RecordStatus = $temp[0]["RecordStatus"];
			$ATT_NextDateModified = $temp[0]["NextDateModifie"];
			
			if ($GroupAttendanceID == "") {
				
				// check in time range
				$Sql = "
							SELECT
									GroupDateID, UNIX_TIMESTAMP(ActivityDateStart) as ActivityDateStart,
									UNIX_TIMESTAMP(ActivityDateEnd) as ActivityDateEnd, UNIX_TIMESTAMP(NOW()) as currDateTime
							FROM
									INTRANET_ENROL_GROUP_DATE
							WHERE
									GroupDateID = '".$activityID."'
								AND
									(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."') ";
				if(isset($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']) && $sys_custom['eEnrolment']['ActivityTakeAttendanceMinute'] > 0){
					$minute = intval($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']);
					$Sql .=	"	AND
									(
									UNIX_TIMESTAMP(ActivityDateStart - INTERVAL $minute MINUTE) < UNIX_TIMESTAMP(NOW())
									AND
									UNIX_TIMESTAMP(ActivityDateEnd + INTERVAL $minute MINUTE) > UNIX_TIMESTAMP(NOW())
									) ";
				}
				// debug_r($Sql);
				// echo $Sql;
				$temp = $db_engine->returnArray($Sql,1);
				$ActivityDateStart = $temp[0]["ActivityDateStart"];
				$ActivityDateEnd = $temp[0]["ActivityDateEnd"];
				$currDateTime = $temp[0]["currDateTime"];
				$InTimeRange = (sizeof($temp) > 0);
				
				if ($InTimeRange) {			

					$RecordStatus = ENROL_ATTENDANCE_PRESENT;
					if ($libenroll->enableAttendanceLateStatusRight() && ($currDateTime > $ActivityDateStart)) {
						$RecordStatus = ENROL_ATTENDANCE_LATE;
					}
					
					$Sql = "
								INSERT INTO
											INTRANET_ENROL_GROUP_ATTENDANCE
											(GroupDateID, EnrolGroupID, StudentID, SiteName, DateInput, DateModified, RecordStatus)
								VALUES
											('$activityID', '$EnrolGroupID', '$targetUserID', '$sitename', now(), now(), '" . $RecordStatus . "')
							";
					$db_engine->db_db_query($Sql);
	
					$Sql = "
								SELECT
										GroupAttendanceID, DateInput
								FROM
										INTRANET_ENROL_GROUP_ATTENDANCE
								WHERE
										GroupDateID = '".$activityID."'
									AND
										StudentID = '".$targetUserID."'
							";
					$temp = $db_engine->returnArray($Sql,1);
					$time_recorded = $temp[0]["DateInput"];
					$GroupAttendanceID = $temp[0]["GroupAttendanceID"];
					
					$Sql = "INSERT INTO INTRANET_ENROL_GROUP_ATTENDANCE_LOG (GroupID, GroupDateID, GroupAttendanceID, StudentID, CardAttendTime)";
					$Sql .= " VALUES (";
					$Sql .= "'" . $EnrolGroupID . "', '" . $activityID . "', '" . $GroupAttendanceID . "', '" . $targetUserID . "', NOW()";
					$Sql .= ")";
					$db_engine->db_db_query($Sql);
					
					//echo "1###$time_recorded"; //1,$time_recorded
					echo output_env_str("1###".$TargetName.$attend_lang['attend']." $time_recorded".$extra_info);

					intranet_closedb();
					exit();
					
				} else {
					
					$Sql = "
								SELECT
										GroupAttendanceID, DateInput
								FROM
										INTRANET_ENROL_GROUP_ATTENDANCE
								WHERE
										GroupDateID = '".$activityID."'
									AND
										StudentID = '".$targetUserID."'
							";
					$temp = $db_engine->returnArray($Sql,1);
					$time_recorded = $temp[0]["DateInput"];
					$GroupAttendanceID = $temp[0]["GroupAttendanceID"];
						
					$Sql = "INSERT INTO INTRANET_ENROL_GROUP_ATTENDANCE_LOG (GroupID, GroupDateID, GroupAttendanceID, StudentID, CardAttendTime)";
					$Sql .= " VALUES (";
					$Sql .= "'" . $EnrolGroupID . "', '" . $activityID . "', '" . $GroupAttendanceID . "', '" . $targetUserID . "', NOW()";
					$Sql .= ")";
					$db_engine->db_db_query($Sql);
					
					# not in time range
					echo "0###".$attend_lang['not_in_time_range'].$extra_info; //0,$fail_msg
					intranet_closedb();
					exit();
				}
				
			} else {
				
				$Sql = "INSERT INTO INTRANET_ENROL_GROUP_ATTENDANCE_LOG (GroupID, GroupDateID, GroupAttendanceID, StudentID, CardAttendTime)";
				$Sql .= " VALUES (";
				$Sql .= "'" . $EnrolGroupID . "', '" . $activityID . "', '" . $GroupAttendanceID . "', '" . $targetUserID . "', NOW()";
				$Sql .= ")";
				$db_engine->db_db_query($Sql);
				
				if ($libenroll->enableAttendanceEarlyLeaveStatusRight()) {
					// check in time range
					$Sql = "
							SELECT
									GroupDateID, UNIX_TIMESTAMP(ActivityDateStart) as ActivityDateStart,
									UNIX_TIMESTAMP(ActivityDateEnd) as ActivityDateEnd, UNIX_TIMESTAMP(NOW()) as currDateTime, NOW() as cNow
							FROM
									INTRANET_ENROL_GROUP_DATE
							WHERE
									GroupDateID = '".$activityID."'
								AND
									(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."') ";
					if(isset($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']) && $sys_custom['eEnrolment']['ActivityTakeAttendanceMinute'] > 0){
						$minute = intval($sys_custom['eEnrolment']['ActivityTakeAttendanceMinute']);
						$Sql .=	"	AND
						(
						UNIX_TIMESTAMP(ActivityDateStart - INTERVAL $minute MINUTE) < UNIX_TIMESTAMP(NOW())
						AND
						UNIX_TIMESTAMP(ActivityDateEnd + INTERVAL $minute MINUTE) > UNIX_TIMESTAMP(NOW())
						) ";
					}
					// debug_r($Sql);
					$temp = $db_engine->returnArray($Sql,1);
					$ActivityDateStart = $temp[0]["ActivityDateStart"];
					$ActivityDateEnd = $temp[0]["ActivityDateEnd"];
					$currDateTime = $temp[0]["currDateTime"];
					
					$InTimeRange = (sizeof($temp) > 0);
					if ($InTimeRange) {
						if ($libenroll->enableAttendanceEarlyLeaveStatusRight() && ($currDateTime > $ATT_NextDateModified) ) {
							$Sql = "UPDATE INTRANET_ENROL_GROUP_ATTENDANCE set DateModified=NOW() WHERE GroupAttendanceID='" . $GroupAttendanceID . "'";
							if ($ATT_RecordStatus == ENROL_ATTENDANCE_PRESENT) {
								if ($currDateTime < $ActivityDateEnd) {
									$Sql = "UPDATE INTRANET_ENROL_GROUP_ATTENDANCE set RecordStatus='" . ENROL_ATTENDANCE_EARLY_LEAVE . "', DateModified=NOW() WHERE GroupAttendanceID='" . $GroupAttendanceID . "'";
								}
							}
							$db_engine->db_db_query($Sql);
							$Sql = "
									SELECT
											DateModified
									FROM
											INTRANET_ENROL_GROUP_ATTENDANCE
									WHERE
											GroupDateID = '".$activityID."'
										AND
											StudentID = '".$targetUserID."'
								";
							$temp = $db_engine->returnVector($Sql);
							$time_recorded = $temp[0];
							
							//echo "1###$time_recorded"; //1,$time_recorded
							//echo "1###$ChineseName###$EnglishName###$targetClass###$targetClassNumber###$time_recorded";
							echo output_env_str("1###".$TargetName.$attend_lang['leave']." $time_recorded".$extra_info);
							exit;
						}
					}
				}
				
				# have attendance record
				echo "0###".$TargetName.$attend_lang['have_attendance_record'].$extra_info; //0,$fail_msg
				intranet_closedb();
				exit();
			}
			
		} else {
			echo "0###".$TargetName.$attend_lang['not_enroll'].$extra_info; //0,$fail_msg
			intranet_closedb();
			exit();
		}				
	}	
}
else     # Card Not Registered
{
    echo CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'];
    intranet_closedb();
    exit();
}

intranet_closedb();
?>