<?
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20051207
####################################################################
# Version updates
# 20051207: Kenneth Wong
# 20061108: Kenneth Wong
#           Check whether last modified is card read or confirmation
# 20070503: Kenneth Wong
#           Add checking of "0" and "" in InSchoolTime to prevent accidentally 00:00:00 record written
# 20070503: Peter Ho
#			Staff attendance - Add removing the previous records when the new status and old status are not the same
####################################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libauth.php");

intranet_opendb();

include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");

######################################################
# Param : (From QueryString)
#    username, passwd, category
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed())
{
     echo mb_convert_encoding(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'],"BIG5","UTF-8");
     intranet_closedb();
     exit();
}

######################################################
#
# get list of categroy
#
######################################################

$le = new libclubsenrol();
$la = new libauth();

if ($la->validate($username, $passwd)) {
	
	$Sql = "
				SELECT
						UserID
				FROM
						INTRANET_USER
				WHERE
						UserLogin = '".$username."'
			";
	$temp = $db_engine->returnVector($Sql);
	$UID = $temp[0];
}

if (($le->IS_ENROL_ADMIN($UID))||($le->IS_ENROL_MASTER($UID))) {

	$Sql = "
			SELECT
					INTRANET_ENROL_GROUP_DATE.GroupDateID,
					if (INTRANET_GROUP.Title IS NULL, CONCAT(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d'), ' ".$attend_lang['meeting']."'), CONCAT(INTRANET_GROUP.Title, ' ', DATE_FORMAT(ActivityDateStart, '%Y-%m-%d'), ' ".$attend_lang['meeting']."'))
			FROM
				INTRANET_ENROL_GROUP_DATE					
				LEFT JOIN			
				INTRANET_ENROL_GROUPINFO
				ON INTRANET_ENROL_GROUPINFO.EnrolGroupID = INTRANET_ENROL_GROUP_DATE.EnrolGroupID
				LEFT JOIN
				INTRANET_GROUP
				ON INTRANET_ENROL_GROUPINFO.GroupID = INTRANET_GROUP.GroupID
			WHERE
				INTRANET_ENROL_GROUP_DATE.RecordStatus = 1 
				AND 
					GroupCategory = '".$category."'
				AND
					DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."'
			";
	// debug_r($Sql);
	$GroupArr = $db_engine->returnArray($Sql, 2);

	// get event info with GroupID > 0, and output
	$Sql = "
			SELECT
					CONCAT(INTRANET_ENROL_EVENT_DATE.EventDateID, 'e'),
					IF (INTRANET_GROUP.Title IS NULL, CONCAT(INTRANET_ENROL_EVENTINFO.EventTitle, ' ".$attend_lang['meeting']."'), CONCAT(INTRANET_GROUP.Title, ' - ', INTRANET_ENROL_EVENTINFO.EventTitle, ' ".$attend_lang['meeting']."'))
			FROM
					INTRANET_ENROL_EVENT_DATE
					LEFT JOIN
					INTRANET_ENROL_EVENTINFO
					ON INTRANET_ENROL_EVENTINFO.EnrolEventID = INTRANET_ENROL_EVENT_DATE.EnrolEventID
					LEFT JOIN
					INTRANET_ENROL_GROUPINFO
					ON INTRANET_ENROL_EVENTINFO.EnrolGroupID = INTRANET_ENROL_GROUPINFO.EnrolGroupID
					LEFT JOIN
					INTRANET_GROUP
					ON INTRANET_ENROL_GROUPINFO.GroupID = INTRANET_GROUP.GroupID
			WHERE
					INTRANET_ENROL_EVENT_DATE.RecordStatus = 1 
					AND 
					(GroupCategory = '".$category."'
					OR
					EventCategory = '".$category."')
				AND
					(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."')
			";
	// debug_r($Sql);
	$EventGroupArr = $db_engine->returnArray($Sql, 2);

} else {
	
	$Sql = "
			SELECT
					INTRANET_ENROL_GROUP_DATE.GroupDateID, 
					IF (INTRANET_GROUP.Title IS NULL, CONCAT(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d'), ' ".$attend_lang['meeting']."'), CONCAT(INTRANET_GROUP.Title, ' ', DATE_FORMAT(ActivityDateStart, '%Y-%m-%d'), ' ".$attend_lang['meeting']."'))
			FROM
					INTRANET_ENROL_GROUP_DATE					
					LEFT JOIN			
					INTRANET_ENROL_GROUPINFO
					ON
					INTRANET_ENROL_GROUPINFO.EnrolGroupID = INTRANET_ENROL_GROUP_DATE.EnrolGroupID
					LEFT JOIN
					INTRANET_GROUP
					ON
					INTRANET_ENROL_GROUPINFO.GroupID = INTRANET_GROUP.GroupID
					LEFT JOIN
					INTRANET_ENROL_GROUPSTAFF
					ON
					INTRANET_ENROL_GROUPINFO.EnrolGroupID = INTRANET_ENROL_GROUPSTAFF.EnrolGroupID
			WHERE
				INTRANET_ENROL_GROUP_DATE.RecordStatus = 1 
				AND 
					INTRANET_ENROL_GROUPSTAFF.UserID = '".$UID."'
				AND
					GroupCategory = '".$category."'
				AND
					DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."'
			";
	//debug_r($Sql);
	$GroupArr = $db_engine->returnArray($Sql, 2);
		
	$Sql = "
			SELECT
					CONCAT(INTRANET_ENROL_EVENT_DATE.EventDateID, 'e'),
					IF (INTRANET_GROUP.Title IS NULL, CONCAT(INTRANET_ENROL_EVENTINFO.EventTitle, ' ".$attend_lang['meeting']."'), CONCAT(INTRANET_GROUP.Title, ' - ', INTRANET_ENROL_EVENTINFO.EventTitle, ' ".$attend_lang['meeting']."'))
			FROM
					INTRANET_ENROL_EVENT_DATE
					LEFT JOIN
					INTRANET_ENROL_EVENTINFO
					ON
					INTRANET_ENROL_EVENTINFO.EnrolEventID = INTRANET_ENROL_EVENT_DATE.EnrolEventID
					LEFT JOIN
					INTRANET_ENROL_GROUPINFO
					ON
					INTRANET_ENROL_GROUPINFO.EnrolGroupID = INTRANET_ENROL_EVENTINFO.EnrolGroupID
					LEFT JOIN
					INTRANET_GROUP
					ON
					INTRANET_ENROL_GROUPINFO.GroupID = INTRANET_GROUP.GroupID
					LEFT JOIN
					INTRANET_ENROL_EVENTSTAFF
					ON
					INTRANET_ENROL_EVENTINFO.EnrolEventID = INTRANET_ENROL_EVENTSTAFF.EnrolEventID
			WHERE
				INTRANET_ENROL_EVENT_DATE.RecordStatus = 1 
					AND 
					(GroupCategory = '".$category."'
					OR
					EventCategory = '".$category."')
				AND
					(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."')
				AND
					INTRANET_ENROL_EVENTSTAFF.UserID = '".$UID."'
			";
	//debug_r($Sql);
	$EventGroupArr = $db_engine->returnArray($Sql, 2);
}

for ($i = 0; $i < sizeof($EventGroupArr); $i++)
	$GroupArr[] = $EventGroupArr[$i];

for ($i = 0; $i < sizeof($GroupArr); $i++) {
	$ReturnStr .= $GroupArr[$i][0]."###".$GroupArr[$i][1]."\n";
}

echo mb_convert_encoding($ReturnStr,"BIG5","UTF-8");
intranet_closedb();
exit();

?>