<?
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20051207
####################################################################
# Version updates
# 20051207: Kenneth Wong
# 20061108: Kenneth Wong
#           Check whether last modified is card read or confirmation
# 20070503: Kenneth Wong
#           Add checking of "0" and "" in InSchoolTime to prevent accidentally 00:00:00 record written
# 20070503: Peter Ho
#			Staff attendance - Add removing the previous records when the new status and old status are not the same
####################################################################
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");

if ($CardID == "")
{
    # Card not registered
    echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']),"BIG5","UTF-8");
    intranet_closedb();
    exit();
}

######################################################
# Param : (From QueryString)
#    username, passwd, activityID, CardID, sitename
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed())
{
     echo mb_convert_encoding(output_env_str(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP']),"BIG5","UTF-8");
     intranet_closedb();
     exit();
}

################################################
$directProfileInput = false;
$getremind = true;
$reminder_null_text = "No Reminder";
################################################

# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, ChineseName, EnglishName, RecordType, ClassName, ClassNumber, UserLogin FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $db_engine->returnArray($sql,6);
list($targetUserID , $ChineseName, $EnglishName, $targetType, $targetClass, $targetClassNumber, $targetUserLogin) = $temp[0];

# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']),"BIG5","UTF-8");
    intranet_closedb();
    exit();
}
else if ($targetType == 2)
{	
	# Get ClassID
	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE ClassTitleEN = '$targetClass' and AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
	$temp = $db_engine->returnVector($sql);
	$targetClassID = $temp[0];
		
	// check it is club or event first	
	(str_replace('e', '', $activityID) == $activityID) ? $IsEvent = false : $IsEvent = true;
	$activityID = str_replace('e', '', $activityID);
	
	if ($IsEvent) {
		
		// get EnrolEventID
		$Sql = "
					SELECT
							EnrolEventID
					FROM
							INTRANET_ENROL_EVENT_DATE
					WHERE
							EventDateID = '".$activityID."'
				";
		$temp = $db_engine->returnVector($Sql,1);
		$EnrolEventID = $temp[0];

		// check if the student is enrolled
		$Sql = "
					SELECT
							StudentID
					FROM
	     					INTRANET_ENROL_EVENTSTUDENT     	
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					EnrolEventID = '".$EnrolEventID."'
				";  

		$temp = $db_engine->returnArray($Sql,1);
		$EventStudentID = $temp[0];

		if ($EventStudentID != "") {				
					
			$Sql = "
	     			SELECT
	     					EventAttendanceID
	     			FROM
	     					INTRANET_ENROL_EVENT_ATTENDANCE     					
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					EventDateID = '".$activityID."'
					";  
			$temp = $db_engine->returnArray($Sql,1);
			$EventAttendanceID = $temp[0];
		
			if ($EventAttendanceID == "") {
				
				// check in time range
				$Sql = "
							SELECT
									EventDateID
							FROM
									INTRANET_ENROL_EVENT_DATE
							WHERE
									EventDateID = '".$activityID."'
								AND
									(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."')
								AND
									(
									UNIX_TIMESTAMP(ActivityDateStart - INTERVAL 15 MINUTE) < UNIX_TIMESTAMP(NOW())
									AND
									UNIX_TIMESTAMP(ActivityDateEnd + INTERVAL 15 MINUTE) > UNIX_TIMESTAMP(NOW())
									)
						";

				$temp = $db_engine->returnArray($Sql,1);
				$InTimeRange = (sizeof($temp) > 0);
				
				if ($InTimeRange) {			
	
					$Sql = "
								INSERT INTO
											INTRANET_ENROL_EVENT_ATTENDANCE
											(EventDateID, EnrolEventID, StudentID, SiteName, DateInput, DateModified, RecordStatus)
								VALUES
											('$activityID', '$EnrolEventID', '$targetUserID', '$sitename', now(), now(), '1')
							";
					$db_engine->db_db_query($Sql);

					$Sql = "
								SELECT
										DateInput
								FROM
										INTRANET_ENROL_EVENT_ATTENDANCE
								WHERE
										EventDateID = '".$activityID."'
									AND
										StudentID = '".$targetUserID."'
							";
					$temp = $db_engine->returnVector($Sql);
					$time_recorded = $temp[0];
					
					//echo "1###$time_recorded"; //1,$time_recorded
					//echo "1###$ChineseName###$EnglishName###$targetClass###$targetClassNumber###$time_recorded";
										
					if ($dlang=="b5") {						
						echo mb_convert_encoding(output_env_str("1###$ChineseName "),"BIG5","UTF-8");
					} else {
						echo mb_convert_encoding(output_env_str("1###$EnglishName "),"BIG5","UTF-8");
					}
					echo mb_convert_encoding(output_env_str($attend_lang['attend']." $time_recorded###$ChineseName###$EnglishName###$targetClass###$targetClassNumber###$time_recorded###$targetUserLogin"),"BIG5","UTF-8");
										
					intranet_closedb();
					exit();
					
				} else {
					# not in time range
					echo mb_convert_encoding(output_env_str("0###".$attend_lang['not_in_time_range']),"BIG5","UTF-8"); //0,$fail_msg
					intranet_closedb();
					exit();
				}
				
			} else {
				# have attendance record
				echo mb_convert_encoding(output_env_str("0###".$attend_lang['have_attendance_record']),"BIG5","UTF-8"); //0,$fail_msg
				intranet_closedb();
				exit();
			}
			
		} else {
			echo mb_convert_encoding(output_env_str("0###".$attend_lang['not_enroll']),"BIG5","UTF-8"); //0,$fail_msg
			intranet_closedb();
			exit();
		}
		
	} else {
		// get GroupID
		$Sql = "
					SELECT
							INTRANET_ENROL_GROUPINFO.EnrolGroupID
					FROM
							INTRANET_ENROL_GROUPINFO
							LEFT JOIN
							INTRANET_ENROL_GROUP_DATE
							ON
							INTRANET_ENROL_GROUPINFO.EnrolGroupID = INTRANET_ENROL_GROUP_DATE.EnrolGroupID
					WHERE
							GroupDateID = '".$activityID."'
				";
		$temp = $db_engine->returnVector($Sql,1);
		$EnrolGroupID = $temp[0];

		// check if the student is enrolled
		$Sql = "
					SELECT
							StudentID
					FROM
	     					INTRANET_ENROL_GROUPSTUDENT     	
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					EnrolGroupID = '".$EnrolGroupID."'
				";  
		$temp = $db_engine->returnArray($Sql,1);
		$GroupStudentID = $temp[0];			

		if ($GroupStudentID != "") {				
					
			$Sql = "
	     			SELECT
	     					GroupAttendanceID
	     			FROM
	     					INTRANET_ENROL_GROUP_ATTENDANCE     					
	     			WHERE
	     					StudentID = '".$targetUserID."'
	     				AND
	     					GroupDateID = '".$activityID."'
					";  
			$temp = $db_engine->returnArray($Sql,1);
			$GroupAttendanceID = $temp[0];
		
			if ($GroupAttendanceID == "") {
				
				// check in time range
				$Sql = "
							SELECT
									GroupDateID
							FROM
									INTRANET_ENROL_GROUP_DATE
							WHERE
									GroupDateID = '".$activityID."'
								AND
									(DATE_FORMAT(ActivityDateStart, '%Y-%m-%d') = '".date("Y-m-d")."')
								AND
									(
									UNIX_TIMESTAMP(ActivityDateStart - INTERVAL 15 MINUTE) < UNIX_TIMESTAMP(NOW())
									AND
									UNIX_TIMESTAMP(ActivityDateEnd + INTERVAL 15 MINUTE) > UNIX_TIMESTAMP(NOW())
									)
						";
				$temp = $db_engine->returnArray($Sql,1);
				$InTimeRange = (sizeof($temp) > 0);
				
				if ($InTimeRange) {			
	
					$Sql = "
								INSERT INTO
											INTRANET_ENROL_GROUP_ATTENDANCE
											(GroupDateID, EnrolGroupID, StudentID, SiteName, DateInput, DateModified, RecordStatus)
								VALUES
											('$activityID', '$EnrolGroupID', '$targetUserID', '$sitename', now(), now(), '1')
							";
					$db_engine->db_db_query($Sql);
	
					$Sql = "
								SELECT
										DateInput
								FROM
										INTRANET_ENROL_GROUP_ATTENDANCE
								WHERE
										GroupDateID = '".$activityID."'
									AND
										StudentID = '".$targetUserID."'
							";
					$temp = $db_engine->returnVector($Sql);
					$time_recorded = $temp[0];
					
					//echo "1###$time_recorded"; //1,$time_recorded
					if ($dlang=="b5") {						
						echo mb_convert_encoding(output_env_str("1###$ChineseName "),"BIG5","UTF-8");
					} else {
						echo mb_convert_encoding(output_env_str("1###$EnglishName "),"BIG5","UTF-8");
					}
					echo mb_convert_encoding(output_env_str($attend_lang['attend']." $time_recorded###$ChineseName###$EnglishName###$targetClass###$targetClassNumber###$time_recorded"),"BIG5","UTF-8");

					intranet_closedb();
					exit();
					
				} else {
					# not in time range
					echo mb_convert_encoding(output_env_str("0###".$attend_lang['not_in_time_range']),"BIG5","UTF-8"); //0,$fail_msg
					intranet_closedb();
					exit();
				}
				
			} else {
				# have attendance record
				echo mb_convert_encoding(output_env_str("0###".$attend_lang['have_attendance_record']),"BIG5","UTF-8"); //0,$fail_msg
				intranet_closedb();
				exit();
			}
			
		} else {
			echo mb_convert_encoding(output_env_str("0###".$attend_lang['not_enroll']),"BIG5","UTF-8"); //0,$fail_msg
			intranet_closedb();
			exit();
		}				
	}
}
else     # Card Not Registered
{
    echo mb_convert_encoding(output_env_str(CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered']),"BIG5","UTF-8");
    intranet_closedb();
    exit();
}

intranet_closedb();
?>

