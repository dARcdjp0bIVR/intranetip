<?php
// Editing by 
$attend_lang['CardNotRegistered'] = "此智能卡并未登记。 请联络系统管理员。";
$attend_lang['APINoAccess'] = "此系统并没有此项功能。 请联络系统管理员。";
$attend_lang['WithinIgnorePeriod'] = "此咭已被处理。";
$attend_lang['SystemNotInit'] = "系统并未进行初始化。";
$attend_lang['NotAllowToOutLunch'] = "不能外出用膳。";
$attend_lang['NotAllowToOutLunchAgain'] = "不能再外出。";
$attend_lang['InSchool'] = "已经回校。 时间为: ";
$attend_lang['LeaveSchool'] = "已经离开。 时间为: ";
$attend_lang['InSchool_late'] = "已经回校(迟到)。 时间为:";
$attend_lang['LeaveSchool_early'] = "已经离开(早退)。 时间为:";
$attend_lang['OutLunch'] = "已经外出用膳。 时间为:";
$attend_lang['BackFromLunch'] = "已经由外出用膳回校。 时间为:";
$attend_lang['BackFromLunch_late'] = "已经由外出用膳回校(迟到)。 时间为:";
$attend_lang['InvalidIP'] = "此终端机没有权限进行点名。";
$attend_lang['NoNeedToTakeAttendance'] = "无须拍咭点名。";
$attend_lang['AlreadyPresent'] = "已经回校。";
$attend_lang['LateCount'] = "迟到次数:";
$attend_lang['NoSuchFunctionForStaff'] = "此项功能只用作纪录学生出入";
$attend_lang['RecordSuccessful'] = "已被记录。 时间为: ";
$attend_lang['StaffEarlyLeave'] = "已被记录。 请于离校时再次拍咭。 时间为: ";
$attend_lang['NonSchoolDay'] = "今天无需上课。";
$attend_lang['QualiEd_LaterThanType2'] = "已回校。 时间为:";
$attend_lang['PresetOut1'] = " 已设定外出活动 (";
$attend_lang['PresetOut2'] = ")。 请联络相关老师(";
$attend_lang['PresetOut3'] = ")。";
$attend_lang['PresetAbsence'] = " 已设定预设缺席。 原因: ";
$attend_lang['PresetAbsence1'] = "。 请联络相关老师，作出安排。";
$attend_lang['On'] = "於";
$attend_lang['To'] = "至";
$attend_lang['OverLateMinWarning'] = "已迟到多於 ";
$attend_lang['OverLateMinWarning1'] = "分钟。 请联络您的班主任确认考勤纪录。";
$attend_lang['EntryLogRecorded'] = " 已被记錄於拍卡纪錄。 时间为: ";
$attend_lang['HasTappedCard'] = " 已拍卡。 时间为: ";
$attend_lang['IgnoreWithinLunchPeriod'] = "下午时段尚未开始，请于 <!--TIME--> 后再拍卡。";

# eEnrollment
$attend_lang['not_in_time_range'] = "不在活动时间范围。";
$attend_lang['have_attendance_record'] = "已有出席纪录。";
$attend_lang['not_enroll'] = "没有成功参加此活动。";
$attend_lang['meeting'] = "聚会";
$attend_lang['attend'] = "已经到达。 时间:";

# Patrol
$attend_lang['OnlyAvaliableForStaff'] = "职员专用。";
?>
