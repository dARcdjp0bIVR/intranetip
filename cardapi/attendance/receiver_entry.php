<?
// using kenneth chung
####################################################################
# Created by : Kenneth Wong
# Creation Date : 20060614
####################################################################
# Data Handler for Entry log ONLY
# Client Terminal Program is required to change!
####################################################################
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgeneralsettings.php");
intranet_opendb();

// get all general setting for student attendance
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance');

include_once("attend_functions.php");
if ($dlang != "b5") $intranet_session_language = "en";
else $intranet_session_language = "b5";

include_once("attend_lang.php");
include_once("attend_const.php");


######################################################
# Param : (From QueryString)
#    CardID
######################################################

# $db_engine (from functions.php)

# Check IP
if (!isIPAllowed())
{
     echo mb_convert_encoding(CARD_RESP_INVALID_IP."###".$attend_lang['InvalidIP'],"BIG5","UTF-8");
     intranet_closedb();
     exit();
}

if ($CardID == '')
{
    # Card not registered
    echo mb_convert_encoding( CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'],"BIG5","UTF-8");
    intranet_closedb();
    exit();
}

# 1. Get UserInfo
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield, RecordType, ClassName, ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID'";
$temp = $db_engine->returnArray($sql,5);
list($targetUserID , $targetName, $targetType, $targetClass, $targetClassNumber) = $temp[0];



# Check Card
if ($targetUserID == 0)
{
    # Card not registered
    echo mb_convert_encoding( CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'],"BIG5","UTF-8");
    intranet_closedb();
    exit();
}
else if ($targetType == 1)    # Staff, redirection
{
     echo mb_convert_encoding( CARD_RESP_NO_SUCH_FUNCTION_FOR_STAFF."###".$attend_lang['NoSuchFunctionForStaff'],"BIG5","UTF-8");
     intranet_closedb();
     exit();
}
else if ($targetType == 2)
{

     # Create Monthly data table
     $current_time = time();
     if ($external_time != "")
     {
         $current_time = strtotime(date('Y-m-d') . " " . $external_time);
     }
     $month = date('m',$current_time);
     $year = date('Y',$current_time);
     $day = date('d',$current_time);
     $today = date('Y-m-d',$current_time);
     $time_string = date('H:i:s',$current_time);
     $ts_now = $current_time - strtotime($today);

     $tablename = buildEntryLogMonthTable($year, $month);


     # Change the name display
     $targetName = "$targetName ($targetClass-$targetClassNumber)";

     # Check Last Card Read Time for ignore period
     $sql = "SELECT TIME_TO_SEC(MAX(RecordTime)) FROM $tablename WHERE DayNumber = '$day' AND UserID = '$targetUserID'";
     $temp = $db_engine->returnVector($sql);
     $curr_tslastMod = $temp[0];

     if ($curr_tslastMod == "")
     {
     }
     else
     {
         # Check whether within last n mins
         //$ignore_period = get_file_content("$intranet_root/file/stattend_ignore.txt");
         $ignore_period = $Settings['IgnorePeriod'];
         $ignore_period += 0;
         if ($ignore_period > 0)
         {
             $ts_ignore = $ignore_period * 60;
             # Check whether it is w/i last n mins
             if ($ts_now - $curr_tslastMod < $ts_ignore)
             {
                 echo mb_convert_encoding( CARD_RESP_IGNORE_WITHIN_PERIOD."###".$attend_lang['WithinIgnorePeriod'],"BIG5","UTF-8");
                 intranet_closedb();
                 exit();
             }
         }
     }

     # Insert Record
     $sql = "INSERT INTO $tablename (UserID, DayNumber, RecordTime, DateInput ,DateModified)
                    VALUES ('$targetUserID', '$day', '$time_string' , now(), now())";
     $db_engine->db_db_query($sql);
     echo mb_convert_encoding( CARD_RESP_RECORD_SUCCESSFUL."###".$targetName.$attend_lang['RecordSuccessful'].$time_string,"BIG5","UTF-8");
     intranet_closedb();
     exit();

}
else     # Card Not Registered
{
    echo mb_convert_encoding( CARD_RESP_CARD_NOT_REGISTER."###".$attend_lang['CardNotRegistered'],"BIG5","UTF-8");
    intranet_closedb();
    exit();
}




intranet_closedb();
?>
