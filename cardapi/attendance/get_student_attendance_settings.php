<?php
// Editing by 
/*
 * 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
 * 2017-05-05 (Carlos): Created for eAttendance client program Cloud Mode.
 */
set_time_limit(86400);
ini_set("memory_limit", "512M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

include_once("attend_functions.php");
/*
if ($dlang != "b5" && $dlang!= "gb") $intranet_session_language = "en";
else $intranet_session_language = $dlang;

include_once("attend_lang.".$intranet_session_language.".php");
include_once("attend_const.php");
*/

$json = new JSON_obj();

header("Content-Type: application/json;charset=utf-8");

# Check IP
if (!((isIPAllowed('StudentAttendance') || isIPAllowed('StaffAttendance')) && checkValidAccess()))
{
	intranet_closedb();
	//header('HTTP/1.0 401 Unauthorized', true, 401);
	$ary = array('Error'=>'401 Unauthorized access.');
	$output = $json->encode($ary);
	echo $output;
    exit();
}

$li = new libgeneralsettings();

$is_dev = preg_match('/^192\.168\.0\.146.*$/', $_SERVER['SERVER_NAME'])? 1: 0;

$target_date = isset($_GET['TargetDate']) && $_GET['TargetDate']!=''? date("Y-m-d", strtotime($_GET['TargetDate'])) : date("Y-m-d");

$ary = array();

if($plugin['attendancestudent'])
{
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	
	$lc = new libcardstudentattend2();
	
	$attendance_mode = $lc->attendance_mode;
	$Settings = $li->Get_General_Setting('StudentAttendance');
	$ary['BasicSettings'] = $Settings;
	
	$sql = "SELECT o.UserID as StudentID,o.RecordDate,u.UserLogin,u.CardID,o.DayType,o.OutTime,o.BackTime,o.Location,o.FromWhere,o.Objective,o.PIC,o.Detail FROM CARD_STUDENT_OUTING as o 
			INNER JOIN INTRANET_USER as u ON u.UserID=o.UserID 
			WHERE o.RecordDate='$target_date' AND u.RecordStatus=1 AND u.RecordType=2 AND u.CardID IS NOT NULL AND TRIM(u.CardID)<>''";
	$preset_outings = $li->returnResultSet($sql);
	$ary['PresetOutings'] = $preset_outings;
	
	$sql = "SELECT p.StudentID,p.RecordDate,p.DayPeriod as DayType,p.Reason,u.UserLogin,u.CardID FROM CARD_STUDENT_PRESET_LEAVE as p INNER JOIN INTRANET_USER as u ON u.UserID=p.StudentID 
			WHERE p.RecordDate='$target_date' AND u.RecordStatus=1 AND u.RecordType=2 AND u.CardID IS NOT NULL AND TRIM(u.CardID)<>''";
	$preset_absences = $li->returnResultSet($sql);
	$ary['PresetAbsences'] = $preset_absences;
	
	if($ary['BasicSettings']['AttendanceMode'] == 2){ // whole day with lunch time settings
		$sql = "SELECT 
					a.StudentID,b.UserLogin,b.CardID 
				FROM CARD_STUDENT_LUNCH_ALLOW_LIST AS a
	            INNER JOIN INTRANET_USER AS b ON (a.StudentID=b.UserID) 
				WHERE b.RecordStatus=1 AND b.RecordType=2 AND b.CardID IS NOT NULL AND TRIM(b.CardID)<>'' ";
		$lunch_allow_list = $li->returnResultSet($sql);
		$ary['LunchAllowList'] = $lunch_allow_list;
	}
	
	$sql = "SELECT 
				r.StudentID,u.CardID,u.UserLogin,r.ReminderID,r.DateOfReminder,r.Reason 
			FROM CARD_STUDENT_REMINDER as r 
			INNER JOIN INTRANET_USER as u ON u.UserID=r.StudentID 
			WHERE r.DateOfReminder='$target_date' AND u.RecordStatus=1 AND u.RecordType=2 AND u.CardID IS NOT NULL AND TRIM(u.CardID)<>'' ";
	$reminders = $li->returnResultSet($sql);
	$ary['Reminders'] = $reminders;
	
	if($sys_custom['SmartCardStudentAttend_LeaveSchoolOption'])
	{
		$current_weekday = date("w");
		$sql = "SELECT 
					o.StudentID,
					u.CardID,
					u.UserLogin,
					o.Weekday".$current_weekday." as LeaveType  
				FROM CARD_STUDENT_LEAVE_OPTION as o 
				INNER JOIN INTRANET_USER as u ON u.UserID=o.StudentID 
				WHERE o.Weekday".$current_weekday." IS NOT NULL AND u.RecordStatus=1 AND u.RecordType=2 AND u.CardID IS NOT NULL AND TRIM(u.CardID)<>''";
		$leave_options = $li->returnResultSet($sql);
		$ary['LeaveOptions'] = $leave_options;
	}
	
	$sql = "SELECT RecordType,UserID,UserLogin,CardID FROM INTRANET_USER WHERE RecordStatus='1' AND RecordType='".USERTYPE_STUDENT."' AND TRIM(CardID)!='' ORDER BY ClassName,ClassNumber";
	$userIdAry = $li->returnResultSet($sql);
	$user_count = count($userIdAry);
	
	for($i=0;$i<$user_count;$i++){
		if($userIdAry[$i]['RecordType'] == USERTYPE_STUDENT)
		{ 
			$time_setting = $lc->Get_Student_Attend_Time_Setting($target_date,$userIdAry[$i]['UserID']);
			$timeAry = array();
			if($time_setting != "NonSchoolDay")
			{
				if($attendance_mode == 0) // AM
				{
					$timeAry = array($time_setting['MorningTime'],$time_setting['LeaveSchoolTime']);
				}else if($attendance_mode == 1){ // PM
					$timeAry = array($time_setting['LunchEnd'],$time_setting['LeaveSchoolTime']);
				}else{ // WD
					$timeAry = array($time_setting['MorningTime'],$time_setting['LunchStart'],$time_setting['LunchEnd'],$time_setting['LeaveSchoolTime']);
				}
			}
			$userIdAry[$i]['Times'] = implode(",", $timeAry);
		}
	}
	
	$ary['TimeRecords'] = $userIdAry;

}

if($plugin['attendancestaff'] && $module_version['StaffAttendance'] >= 3.0)
{
	include_once($PATH_WRT_ROOT."includes/libstaffattend3_api.php");
    	
    $StaffAttend3 = new libstaffattend3_api();
    
    $staff_attendance_settings = $li->Get_General_Setting('StaffAttendance');
    $ary['StaffAttendanceSettings'] = $staff_attendance_settings;
    
    $sql = "SELECT RecordType,UserID,UserLogin,CardID FROM INTRANET_USER WHERE RecordStatus='1' AND RecordType='".USERTYPE_STAFF."' AND CardID IS NOT NULL AND TRIM(CardID)<>'' ORDER BY EnglishName";
    $staffAry = $StaffAttend3->returnResultSet($sql);
    $staff_size = count($staffAry);
    
    $staffInfoAry = array();
    for($i=0;$i<$staff_size;$i++)
    {
	    $duty = $StaffAttend3->Get_Duty_By_Date($staffAry[$i]['UserID'],$target_date); 
		if(count($duty["CardUserToDuty"][$staffAry[$i]['UserID']])>0){
			$staffAry[$i]['TimeSlots'] = array();
			$slots = $duty["CardUserToDuty"][$staffAry[$i]['UserID']];
			$slot_count = count($slots);
			for($j=0;$j<$slot_count;$j++){
				if($slots[$j]['DutyStart'] != ''){
					$staffAry[$i]['TimeSlots'][] = array('SlotName'=>$slots[$j]['SlotName'],'DutyStart'=>$slots[$j]['DutyStart'],'DutyEnd'=>$slots[$j]['DutyEnd'],'InWavie'=>$slots[$j]['InWavie'],'OutWavie'=>$slots[$j]['OutWavie'],'InSchoolStatus'=>trim($slots[$j]['InSchoolStatus']),'OutSchoolStatus'=>trim($slots[$j]['OutSchoolStatus']));
				}
			}
			$staffInfoAry[] = $staffAry[$i];
		}
    }
    
    $ary['StaffTimeSlots'] = $staffInfoAry;
}

$local_ip_address_ary = array();
$local_ip_settings = $li->Get_General_Setting('eAttendanceTerminal',array("'LocalIPAddress'"));
$local_ip_address_text = $local_ip_settings['LocalIPAddress'];
if($local_ip_address_text != ''){
	$local_ip_address_ary = explode(",",$local_ip_address_text);
}
if(isset($_GET['LocalIPAddress']) && $_GET['LocalIPAddress']!=''){
	if(!in_array($_GET['LocalIPAddress'],$local_ip_address_ary)){
		$local_ip_address_ary[] = $_GET['LocalIPAddress'];
	}
	$li->Save_General_Setting('eAttendanceTerminal',array('LocalIPAddress'=>implode(",",$local_ip_address_ary)));
}

$http_log_path = isset($sys_custom['CardAPI']['HttpLogPath']) && $sys_custom['CardAPI']['HttpLogPath']!=''? $sys_custom['CardAPI']['HttpLogPath'] : '/var/log/httpd/eclass-access_log';
$grep_ip_cmd = ($is_dev?'':'sudo ' ).' grep \'receiver_csharp\' \''.$http_log_path.'\' | sed -e \'s/\([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+\).*$/\1/\' -e t -e d  | sort | uniq -d';
$ip_result = trim(shell_exec($grep_ip_cmd));
$tmp_ip_addresses = $ip_result == ''? array() : explode("\n",$ip_result);
$ip_addresses = array();
for($i=0;$i<count($tmp_ip_addresses);$i++){
	$ip_addresses[] = str_replace(array('-',' '),'',$tmp_ip_addresses[$i]);
}
if(count($local_ip_address_ary)>0){
	$ip_addresses = array_merge($ip_addresses,$local_ip_address_ary);
	$ip_addresses = array_values(array_unique($ip_addresses));
}
$ary['IPAddresses'] = $ip_addresses;

$output = $json->encode($ary);

intranet_closedb();

echo $output;

exit;
?>