<?php
//
/*
 * 2019-06-03 (Carlos): Apply checkValidAccess() to verify valid access.
 * 2017-11-20 (Carlos): Created. 
 */
set_time_limit(86400);
ini_set("memory_limit", "512M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_opendb();

include_once("attend_functions.php");

$json = new JSON_obj();

header("Content-Type: application/json;charset=utf-8");

# Check IP
if (!((isIPAllowed('StudentAttendance') || isIPAllowed('StaffAttendance')) && checkValidAccess()))
{
	intranet_closedb();
	//header('HTTP/1.0 401 Unauthorized', true, 401);
	$ary = array('Error'=>'401 Unauthorized access.');
	$output = $json->encode($ary);
	echo $output;
    exit();
}

$ary = array();

$li = new libdb();

$photo_path = $file_path.'/file/user_photo/';

if($plugin['attendancestudent'])
{
	$cmd = "find '$photo_path' -maxdepth 1 -type f";
	$photo_full_paths = explode("\n", trim(shell_exec($cmd)));
	$photo_full_path_size = count($photo_full_paths);
	//debug_pr($cmd);
	//debug_pr($photo_full_paths);
	
	$userloginToPhotoPath = array();
	for($i=0;$i<$photo_full_path_size;$i++){
		if(preg_match('/^.+file\/user_photo\/(.+)\.[^\.]+$/', $photo_full_paths[$i], $matches)){
			//debug_pr($matches);
			$userloginToPhotoPath[$matches[1]] = $photo_full_paths[$i];
		}
	}
	
	$get_photo_data = $_REQUEST['GetPhotoData'] == 1;
	$conds = "";
	if(isset($_REQUEST['UserID'])){
		$user_id = $_REQUEST['UserID'];
		$conds .= " AND UserID ".(is_array($user_id)?" IN (".implode(",",$user_id).") " : (strpos($user_id,',')>=0? " IN ($user_id) " : "=".$user_id." ") );
	}
	
	$sql = "SELECT UserID,UserLogin,CardID,RecordType as UserType,IFNULL(ClassName,'') as ClassName,IFNULL(ClassNumber,'') as ClassNumber,IF(TRIM(PersonalPhotoLink)='','',CONCAT('$file_path',TRIM(PersonalPhotoLink))) as PhotoPath,'' as PhotoData FROM INTRANET_USER WHERE RecordStatus='1' AND ".($_REQUEST['GetStaffPhoto']==1?" RecordType IN (1,2) ":" RecordType='2' ")." AND CardID IS NOT NULL AND CardID<>'' $conds ORDER BY UserLogin";
	$users = $li->returnResultSet($sql);
	//$userloginToIndex = array();
	$user_size = count($users);
	for($i=0;$i<$user_size;$i++){
		$userlogin = $users[$i]['UserLogin'];
		//$userloginToIndex[$userlogin] = $i;
		if(isset($userloginToPhotoPath[$userlogin])){
			$photo_full_path = $userloginToPhotoPath[$userlogin];
			//$index = $userloginToIndex[$userlogin];
			$users[$i]['PhotoPath'] = $photo_full_path;
			if($get_photo_data && file_exists($photo_full_path)){
				$fh = fopen($photo_full_path, "rb");
				$binary_contents = fread($fh, filesize($photo_full_path));
				fclose($fh);
				$base64_photo_data = base64_encode($binary_contents);
				$users[$i]['PhotoData'] = $base64_photo_data;
			}
			$ary[] = $users[$i];
		}else if($users[$i]['PhotoPath'] != ''){
			$photo_full_path = $users[$i]['PhotoPath'];
			if($get_photo_data && file_exists($photo_full_path)){
				$fh = fopen($photo_full_path, "rb");
				$binary_contents = fread($fh, filesize($photo_full_path));
				fclose($fh);
				$base64_photo_data = base64_encode($binary_contents);
				$users[$i]['PhotoData'] = $base64_photo_data;
			}
			$ary[] = $users[$i];
		}
	}
	/*
	if(count($userloginToPhotoPath)>0)
	{
		foreach($userloginToPhotoPath as $userlogin => $photo_full_path)
		{
			if(isset($userloginToIndex[$userlogin])){
				$index = $userloginToIndex[$userlogin];
				$users[$index]['PhotoPath'] = $photo_full_path;
				if($get_photo_data && file_exists($photo_full_path)){
					$fh = fopen($photo_full_path, "rb");
					$binary_contents = fread($fh, filesize($photo_full_path));
					fclose($fh);
					$base64_photo_data = base64_encode($binary_contents);
					$users[$index]['PhotoData'] = $base64_photo_data;
				}
				$ary[] = $users[$index];
			}
		}
	}
	*/
}

$output = $json->encode($ary);

intranet_closedb();

echo $output;

exit;
?>