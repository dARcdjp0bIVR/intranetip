<?php
/*
 * 2019-06-03 Carlos: Apply checkValidAccess() to verify valid access.
 * 2019-03-18 Carlos: Created to get and save the posted logs.zip from eAttendance client program.
 */
ini_set("memory_limit", "1024M");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

include_once("attend_functions.php");

# Check IP
if (!((isIPAllowed('StudentAttendance') || isIPAllowed('StaffAttendance')) && checkValidAccess()))
{
	intranet_closedb();
	echo "0";
    exit();
}

intranet_closedb();

$file_field_name = 'LogFile';
if(isset($_FILES[$file_field_name])){
	
	$lf = new libfilesystem();
	$target_dir = $file_path.'/file/student_attendance';
	if(!file_exists($target_dir) || !is_dir($target_dir)){
		$lf->folder_new($target_dir);
	}else{
		chmod($target_dir,0777);
	}
	
	$target_dir .= '/terminal_logs';
	if(!file_exists($target_dir) || !is_dir($target_dir)){
		$lf->folder_new($target_dir);
	}else{
		chmod($target_dir,0777);
	}
	
	$base_name = $_FILES[$file_field_name]["name"];
	$target_path = $target_dir.'/'.$base_name;
	$copy_success = move_uploaded_file($_FILES[$file_field_name]["tmp_name"], $target_path);
	
	echo $copy_success? "1" : "0";
	
}else{
	echo "0";	
}
?>