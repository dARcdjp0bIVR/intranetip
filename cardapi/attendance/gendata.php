<?
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

      function getContentFromURL($target_path)
      {
               $content = "";
               $fp = @fopen($target_path,"r");
               if ($fp)
               {
                   while (!feof($fp))
                   {
                           $line = trim(fgets($fp,1024*2));
                           if ($line == "") continue;
                           $content .= $line;
                   }
               }
               else
               {
                   $content = "No response got";
               }
               return $content;
      }
/*
$param_info = array(
array("0001243445","08:00:01"),
array("0001245887","08:00:02"),
array("0001243465","08:01:01"),
array("0008925394","08:03:01"),
array("0008572291","08:07:01"),
array("0001243441","08:09:01")
);
*/

$li = new libdb();
$sql = "SELECT CardID FROM INTRANET_USER WHERE ClassName = '2A' AND RecordType = 2 AND RecordStatus IN (0,1) ORDER BY ClassNumber";
$result = $li->returnVector($sql);
for ($i=0; $i<sizeof($result); $i++)
{
     if ($result[$i] != "" && $i!=23 && $i!=25 )
     {
         $param_info[] = array($result[$i],"08:". ($i<10? "0":"").$i.":05");
     }
}

$sitename = urlencode("School Entrance");
for ($i=0; $i<sizeof($param_info); $i++)
{
     $url = "http://192.168.0.245:52001/cardapi/attendance/receiver.php";
     list ($CardID, $time) = $param_info[$i];
     $CardID = urlencode($CardID);
     $time = urlencode($time);
     $url .= "?sitename=$sitename&CardID=$CardID&external_time=$time";
     $content = getContentFromURL($url);
     echo $i . ". $content <br>\n";
}


intranet_closedb();
?>
