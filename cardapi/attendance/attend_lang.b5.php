<?php
// Editing by 
$attend_lang['CardNotRegistered'] = "此智能卡並未登記。 請聯絡系統管理員。";
$attend_lang['APINoAccess'] = "此系統並沒有此項功能。 請聯絡系統管理員。";
$attend_lang['WithinIgnorePeriod'] = "此卡已被處理。";
$attend_lang['SystemNotInit'] = "系統並未進行初始化。";
$attend_lang['NotAllowToOutLunch'] = "不能外出用膳。";
$attend_lang['NotAllowToOutLunchAgain'] = "不能再外出。";
$attend_lang['InSchool'] = " 已經回校。 時間為: ";
$attend_lang['LeaveSchool'] = " 已經離開。 時間為: ";
$attend_lang['InSchool_late'] = " 已經回校(遲到)。 時間為: ";
$attend_lang['LeaveSchool_early'] = " 已經離開(早退)。 時間為: ";
$attend_lang['OutLunch'] = " 已經外出用膳。 時間為: ";
$attend_lang['BackFromLunch'] = " 已經由外出用膳回校。 時間為: ";
$attend_lang['BackFromLunch_late'] = " 已經由外出用膳回校(遲到)。 時間為: ";
$attend_lang['InvalidIP'] = "此終端機沒有權限進行點名。";
$attend_lang['NoNeedToTakeAttendance'] = " 無須拍卡點名。";
$attend_lang['AlreadyPresent'] = " 已經回校。";
$attend_lang['LateCount'] = "遲到次數: ";
$attend_lang['NoSuchFunctionForStaff'] = "此項功能只用作紀錄學生出入";
$attend_lang['RecordSuccessful'] = "已被記錄。 時間為: ";
$attend_lang['StaffEarlyLeave'] = "已被記錄。 請於離校時再次拍卡。 時間為: ";
$attend_lang['NonSchoolDay'] = "今天無需上課。";
$attend_lang['QualiEd_LaterThanType2'] = "已回校。 時間為: ";
$attend_lang['PresetOut1'] = " 已設定外出活動 (";
$attend_lang['PresetOut2'] = ")。 請聯絡相關老師(";
$attend_lang['PresetOut3'] = ")。";
$attend_lang['PresetAbsence'] = " 已設定預設缺席。 原因: ";
$attend_lang['PresetAbsence1'] = "。 請聯絡相關老師，作出安排。";
$attend_lang['On'] = "於";
$attend_lang['To'] = "至";
$attend_lang['OverLateMinWarning'] = "已遲到多於 ";
$attend_lang['OverLateMinWarning1'] = "分鐘。 請聯絡您的班主任確認考勤紀錄。";
$attend_lang['EntryLogRecorded'] = " 已被記錄於拍卡紀錄。 時間為: ";
$attend_lang['HasTappedCard'] = " 已拍卡。 時間為: ";
$attend_lang['IgnoreWithinLunchPeriod'] = "下午時段尚未開始，請於 <!--TIME--> 後再拍卡。";

# eEnrollment
$attend_lang['not_in_time_range'] = "不在活動時間範圍。";
$attend_lang['have_attendance_record'] = "已有出席紀錄。";
$attend_lang['not_enroll'] = "沒有成功參加此活動。";
$attend_lang['meeting'] = "聚會";
$attend_lang['attend'] = "已經到達。 時間:";
$attend_lang['leave'] = "已經離開。 時間:";
?>