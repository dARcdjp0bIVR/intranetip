<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

#####
# return:
# 0 if CardID no match, no need to pay, amount not enough
# 1 if successful
######
if (!$payment_api_open_itempay)
{
     echo -1;
     exit();
}

# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,1))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo -1;
     exit();
}

$username = getUsername($key);

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             , INTRANET_USER READ
             , PAYMENT_PAYMENT_ITEMSTUDENT WRITE
             , PAYMENT_PAYMENT_ITEMSTUDENT as a WRITE
             , PAYMENT_PAYMENT_ITEM WRITE
             , PAYMENT_PAYMENT_ITEM as b WRITE
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);

# Retrieve UserID
$sql = "SELECT UserID FROM INTRANET_USER WHERE CardID = '$CardID' AND RecordType = 2 AND RecordStatus = 1";
$temp = $lpayment->returnVector($sql);
$uid = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo -1;
}
else
{
    # Retrieve Amount
    $sql = "SELECT a.PaymentID, a.RecordStatus, a.Amount, b.Name
            FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
                 WHERE a.StudentID = '$uid' AND b.ItemID = '$ItemID'";
    #echo "1. $sql\n";
    $temp = $lpayment->returnArray($sql,4);
    list($paymentID, $status, $amount, $detail) = $temp[0];

    if ($status == 1) # Paid
    {
        echo 0;
    }
    else
    {
        $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '$uid'";
    #echo "2. $sql\n";
        $temp = $lpayment->returnVector($sql);
        $balance = $temp[0];

        if ($balance < $amount)         # Not enough
        {
            echo 0;
        }
        else
        {
            # Process Payment

            # Payment Record
            $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = 1 WHERE StudentID = '$uid' AND ItemID = '$ItemID'";
    #echo "3. $sql\n";
            $lpayment->db_db_query($sql);
            $affected = $lpayment->db_affected_rows();

            if ($affected == 0)
            {
                echo 0;
            }
            else     # Update Other tables
            {
                $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET PaidTime = NOW() WHERE StudentID = '$uid' AND ItemID = '$ItemID'";
                $lpayment->db_db_query($sql);

                $balanceAfter = $balance - $amount;
                # Overall Transaction
                $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                        (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
                        VALUES
                        ('$uid', 2,'$amount','$paymentID','$balanceAfter',NOW(),'$detail')";
    #echo "4. $sql\n";
                $lpayment->db_db_query($sql);
                $logID = $lpayment->db_insert_id();
                $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = '$logID'";
                $lpayment->db_db_query($sql);

                # Account balance
                $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $amount
                               ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                               WHERE StudentID = '$uid'";
                $lpayment->db_db_query($sql);

                echo 1;
            }

        }
    }
}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);



intranet_closedb();
?>
