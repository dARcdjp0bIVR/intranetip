<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();
# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,1))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo -1;
     exit();
}

$cats = $lpayment->returnPaymentCats();

if (sizeof($cats)==0)
{
    echo 0;
}
else
{
    for ($i=0; $i<sizeof($cats); $i++)
    {
         list($cid, $name) = $cats[$i];
         echo "$cid###$name\n";
    }
}
intranet_closedb();
?>
