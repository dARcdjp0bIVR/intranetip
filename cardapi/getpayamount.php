<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

#####
# return:
# 0 if CardID no match, no need to pay
#
# Row 1: 1 if paid, 0 if unpaid
# Row 2: ChineseName
# Row 3: EnglishName
# Row 4: ClassName
# Row 5: ClassNumber
# Row 6: Amount to be paid
# Row 7: Balance (if Unpaid, paid will not have 7th row)
######

# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,1))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     #echo $msg;
     echo -1;
     exit();
}

$sql = "SELECT UserID,ChineseName,EnglishName,ClassName,ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID' AND RecordType = 2 AND RecordStatus = 1";
$temp = $lpayment->returnArray($sql,5);
list($uid,$chiName,$engName,$classname,$classnum) = $temp[0];
if ($uid == "" || $uid == 0)  # No such user
{
    echo -2;
}
else
{
    $sql = "SELECT RecordStatus, Amount FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE StudentID = '".IntegerSafe($uid)."' AND ItemID = '".IntegerSafe($ItemID)."'";
    $temp = $lpayment->returnArray($sql,2);
    list($status, $amount) = $temp[0];
    if (sizeof($temp)==0 || $status == "" || $amount == "")
    {
        echo 1;
    }
    else
    {
        if ($status == 0)
        {
            $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = '".IntegerSafe($uid)."'";
            $temp = $lpayment->returnVector($sql);
            $balance = $temp[0];
        }
        echo "$status\n$chiName\n$engName\n$classname\n$classnum\n$amount\n$balance";
    }
}

intranet_closedb();
?>
