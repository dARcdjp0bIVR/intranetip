<?
// used by : 
# Creation date: 20071113 (Kenneth Wong)
# Payment API used by C# Terminal
/* 									
 * 2014-12-11 (Bill): $sys_custom['SupplementarySmartCard'] - get user by matching CardID or CardID2 or CardID3 or CardID4
 * 2013-11-13 (Carlos): $sys_custom['SupplementarySmartCard'] - get user by matching CardID or CardID2 or CardID3
 * 2012-11-15 (Carlos): round result of minus operation to 2 decimal places
 */

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libpayment.php");

intranet_opendb();
include_once("functions.php");
/*
#echo "<?xml version=\"1.0\" encoding=\"big5\" ?><PaymentInfo><BasicInfo><Signal>1</Signal><StudentName>���j��</StudentName><CardID>0005684325</CardID><ClassName>4E</ClassName><ClassNumber>11</ClassNumber><Balance>12.3</Balance><PaidCount>2</PaidCount><UnpaidCount>1</UnpaidCount></BasicInfo><Paid><PaidItem amount=\"100\">�ǶO</PaidItem><PaidItem amount=\"200\">���O</PaidItem></Paid><Unpaid><UnpaidItem amount=\"500\">��ï�O</UnpaidItem></Unpaid></PaymentInfo>";
*/
$lpayment = new libpayment();

#####
# Param:
# type: [empty] - normal payment, CheckLoginAnonymous - check terminal requires login or not, encoding - returns encoding of the site
#  , Authenticate - Authentication
# CardID - CardID of the student
# username - terminal user name
# action - 1 if direct pay, otherwise just data retrieve
# rand - random string to prevent packet replay
# key - hash key
# return:
# if error: -1 - Student not exists, -2 - API closed, -3 - key unmatched, -4 - IP not allowed
# if no error:
# XML document with information of
# Signal : 0 - not enough balance, 1 - Successful and not enough
#
######

header("Content-type: text/plain; charset=utf-8");

define("PAYMENT_API_ERROR_CODE_STUDENT_NOT_EXISTS",-1);
define("PAYMENT_API_ERROR_CODE_API_CLOSED",-2);
define("PAYMENT_API_ERROR_CODE_KEY_UNMATCHED",-3);
define("PAYMENT_API_ERROR_CODE_IP_NOT_ALLOWED",-4);

if ($type=="CheckLoginAnonymous")
{
    if (isPaymentNoLogin())
    {
        echo "YES";
    }
    else
    {
        echo "NO";
    }
    exit();
}

# Compute hash value
$keysalt = array("1adkeoi7gk2");
$delimiter = "###";
$hashkey_valid = false;


if ($type == "Authenticate")
{
    # Get password from database
    $sql = "SELECT TerminalUserID, Password FROM PAYMENT_TERMINAL_USER WHERE Username = '$username' AND PaymentAllowed = 1";
    $temp = $lpayment->returnArray($sql,2);
    list($t_terminalUserID, $t_password) = $temp[0];

    if ($t_terminalUserID == "" || $t_terminalUserID == 0)
    {
        echo "NO";
        exit();
    }

    foreach ($keysalt as $t_key => $t_value)
    {
             $string = $t_value.$delimiter.$username.$delimiter.$t_password.$delimiter.$rand;
             $hashed_key = md5($string);
             if ($key==$hashed_key)
             {
                 $hashkey_valid = true;
                 break;
             }
             else
             {
             }
    }
    if ($hashkey_valid)
    {
        echo "YES";
    }
    else
    {
        echo "NO";
    }
    exit();
}


if (!$payment_api_open_userpay)
{
     echo PAYMENT_API_ERROR_CODE_API_CLOSED;
     exit();
}

# Check IP
if (!isIPAllowed())
{
     echo PAYMENT_API_ERROR_CODE_IP_NOT_ALLOWED;
     exit();
}



$lpayment = new libpayment();
$hashkey_valid = false;

if (isPaymentNoLogin())
{
    foreach ($keysalt as $t_key => $t_value)
    {
             //$string = $t_value.$delimiter.$CardID.$delimiter.$delimiter.$delimiter.$rand;
             $string = $t_value.$delimiter.$CardID.$delimiter.$rand;
             $hashed_key = md5($string);
             if ($key==$hashed_key)
             {
                 $hashkey_valid = true;
                 break;
             }
             else
             {
             }
    }
}
else
{
    # Get password from database
    $sql = "SELECT TerminalUserID, Password FROM PAYMENT_TERMINAL_USER WHERE Username = '$username' AND PaymentAllowed = 1";
    $temp = $lpayment->returnArray($sql,2);
    list($t_terminalUserID, $t_password) = $temp[0];

    if ($t_terminalUserID == "" || $t_terminalUserID == 0)
    {
        echo PAYMENT_API_ERROR_CODE_KEY_UNMATCHED;
        exit();
    }

    foreach ($keysalt as $t_key => $t_value)
    {
             $string = $t_value.$delimiter.$CardID.$delimiter.$username.$delimiter.$t_password.$delimiter.$rand;
             $hashed_key = md5($string);
             if ($key==$hashed_key)
             {
                 $hashkey_valid = true;
                 break;
             }
             else
             {
             }
    }

}

if (!$hashkey_valid)
{
     echo PAYMENT_API_ERROR_CODE_KEY_UNMATCHED;
     exit();
}

if ($action == "1")
{
    $isPaymentAction = true;
}
else
{
}


if ($CardID == "")
{
    echo PAYMENT_API_ERROR_CODE_STUDENT_NOT_EXISTS;
    exit();
}

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             ,PAYMENT_ACCOUNT as b
             , INTRANET_USER READ
             , INTRANET_USER as a READ
             , PAYMENT_PAYMENT_ITEMSTUDENT WRITE
             , PAYMENT_PAYMENT_ITEMSTUDENT as a WRITE
             , PAYMENT_PAYMENT_ITEM WRITE
             , PAYMENT_PAYMENT_ITEM as b WRITE
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);


# Retrieve UserID
$sql = "SELECT UserID FROM INTRANET_USER WHERE (CardID = '$CardID'".($sys_custom['SupplementarySmartCard']?" OR CardID2 = '$CardID' OR CardID3 = '$CardID' OR CardID4 = '$CardID'":"").") AND RecordType = 2 AND RecordStatus = 1";
$temp = $lpayment->returnVector($sql);
$uid = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo PAYMENT_API_ERROR_CODE_STUDENT_NOT_EXISTS;
    exit();
}
else
{
    $xml_header = '<?xml version="1.0" encoding="big5" ?> ';

    # Retrieve Student info
    $sql = "SELECT a.UserID,a.ChineseName,a.EnglishName,a.ClassName,a.ClassNumber, b.Balance FROM INTRANET_USER as a
                   LEFT OUTER JOIN PAYMENT_ACCOUNT as b ON a.UserID = b.StudentID
                   WHERE (a.CardID = '$CardID' ".($sys_custom['SupplementarySmartCard']?" OR a.CardID2 = '$CardID' OR a.CardID3 = '$CardID' OR a.CardID4 = '$CardID'":"").") 
					AND a.RecordType = 2 AND a.RecordStatus = 1";
    $temp = $lpayment->returnArray($sql,6);
    list($s_UserID, $s_ChineseName, $s_EnglishName, $s_ClassName, $s_ClassNumber, $s_Balance) = $temp[0];

    $basic_info = "<ChineseName>$s_ChineseName</ChineseName><EnglishName>$s_EnglishName</EnglishName><CardID>$CardID</CardID>";
    $basic_info .= "<ClassName>$s_ClassName</ClassName><ClassNumber>$s_ClassNumber</ClassNumber>";
    #$basic_info .= "<EnglishName>$s_EnglishName</EnglishName><CardID>$CardID</CardID>";

    # Retrieve a list of payment items
    /*
    $sql = "SELECT a.PaymentID, b.Name, a.Amount FROM
                   PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON a.ItemID = b.ItemID
                   WHERE a.StudentID = $uid AND a.RecordStatus = 0 AND b.StartDate <= CURDATE()
                   ORDER BY b.PayPriority, b.EndDate, b.DateInput";
	*/
	$sql = "SELECT a.PaymentID, b.Name, a.Amount FROM
                   PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEM as b ON (a.ItemID = b.ItemID AND b.RecordStatus = 0)
                   WHERE a.StudentID = $uid AND a.RecordStatus = 0 AND b.StartDate <= CURDATE()
                   ORDER BY b.PayPriority, b.EndDate, b.DateInput";
    $payments = $lpayment->returnArray($sql,3);

    $response = "";
    if (sizeof($payments)==0)
    {
        $response .= $xml_header;
        $response .= "<PaymentInfo><BasicInfo><Signal>1</Signal>";
        $response .= $basic_info;
        $response .= "<Balance>$s_Balance</Balance>";
        $response .= "<PaidCount>0</PaidCount><UnpaidCount>0</UnpaidCount></BasicInfo>";
        $response .= "</PaymentInfo>";
    }
    else
    {
        $balance = $s_Balance;
        $uid = $s_UserID;
        $count_paid = 0;
        $count_unpaid = 0;
        $array_paid = array();
        $array_unpaid = array();
        $amount_paid = 0;
        $amount_unpaid = 0;
        for ($i=0; $i<sizeof($payments); $i++)
        {
             list($paymentID, $itemname, $amount) = $payments[$i];
             if ($balance >= $amount)
             {
                 if ($isPaymentAction)
                 {
                     # Update records
                     # Payment Record
                     $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET RecordStatus = 1 WHERE StudentID = $uid AND PaymentID = $paymentID";
                     $lpayment->db_db_query($sql);
                     $affected = $lpayment->db_affected_rows();
                     if ($affected == 0)    # No updates
                     {

                     }
                     else     # Update Other tables
                     {

                         //$balance -= $amount;
                         $balance = round($balance - $amount,2);
                         $array_paid[] = array($itemname, $amount);
                         $count_paid++;
                         $amount_paid+=$amount;

                         # Transaction Record
                         $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET PaidTime = NOW(), DateModified = NOW(), ProcessingUserID = '$uid' WHERE StudentID = $uid AND PaymentID = $paymentID";
                         $lpayment->db_db_query($sql);

                         # Overall Transaction
                         $itemname = addslashes($itemname);
                         $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                                        (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
                                        VALUES
                                 ($uid, 2,'$amount','$paymentID','$balance',NOW(),'$itemname')";
                         $lpayment->db_db_query($sql);
                         $logID = $lpayment->db_insert_id();
                         $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RefCode = CONCAT('PAY',LogID) WHERE LogID = $logID";
                         $lpayment->db_db_query($sql);

                         # Account balance
                         $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = '$balance'
                                        ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                                        WHERE StudentID = $uid";
                         $lpayment->db_db_query($sql);
                     }

                 }
                 else # Non-payment action
                 {
                     //$balance -= $amount;
                     $balance = round($balance - $amount,2);
                     $array_paid[] = array($itemname, $amount);
                     $count_paid++;
                     $amount_paid+=$amount;

                 }
             }
             else
             {
                 #echo "Balance=".$balance."\n";
                 #echo "amount=".$amount."\n";
                 $array_unpaid[] = array($itemname, $amount);
                 $count_unpaid++;
                 $amount_unpaid += $amount;

             }
        }


        ### Response
        $response .= $xml_header;
        $response .= "<PaymentInfo><BasicInfo><Signal>1</Signal>";
        $response .= $basic_info;
        $response .= "<Balance>$balance</Balance>";
        $response .= "<PaidCount>$count_paid</PaidCount><UnpaidCount>$count_unpaid</UnpaidCount></BasicInfo>";
        if ($count_paid > 0)
        {
            $response .= "<Paid>";
            for ($i=0; $i<sizeof($array_paid); $i++)
            {
                 list($t_name, $t_amount) = $array_paid[$i];
                 $response .= "<PaidItem amount=\"$t_amount\">$t_name</PaidItem>";
            }
            $response .= "</Paid>";
        }
        if ($count_unpaid > 0)
        {
            $response .= "<Unpaid>";
            for ($i=0; $i<sizeof($array_unpaid); $i++)
            {
                 list($t_name, $t_amount) = $array_unpaid[$i];
                 $response .= "<UnpaidItem amount=\"$t_amount\">$t_name</UnpaidItem>";
            }
            $response .= "</Unpaid>";
        }


        $response .= "</PaymentInfo>";



    }
    echo $response;

}

intranet_closedb();
?>
