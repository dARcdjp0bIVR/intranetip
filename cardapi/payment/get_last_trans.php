<?php
// Editing by 
/*
 * 	Log
 * 	Date:	2014-01-08 [Cameron] Remove encryption, use plain text only
 * 	Date:	2013-11-11 [Cameron] Create this file.
 * 	@return Last transaction log recordset: (LogDate, SeqNo, CardNo)
 * 			-1 - Error (i.e. No record found);  
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

//$key = "0df6a4fa47dd96e3";
//$query = AES_128_Decrypt($q, $key);
//$query = AES_128_Decrypt($q, $ricoh2_aes_key);
//print $ricoh2_aes_key . "<br>";



$lib = new libdb();

$sql = "SELECT 
			LogDate, SeqNo, CardNo			 
		FROM RICOH_TRANS_DETAILS
		ORDER BY LogDate DESC, SeqNo DESC LIMIT 1";
$result = $lib->returnArray($sql);

if(count($result) == 0){
	echo "-1";
	intranet_closedb();
	exit;
}
$rs = $result[0];
//print_r($rs);
// e.g. LogDate=2013-11-14 10:30:28&SeqNo=3&CardNo=0607471268
$ret = "LogDate=".$rs[0]."&SeqNo=".$rs[1]."&CardNo=".$rs[2];
//$encryptedRet = AES_128_Encrypt($ret, $ricoh2_aes_key);
$encryptedRet = $ret;	// No enctyption
//print($ret);
//echo sprintf("%s", $ret);
echo sprintf("%s", $encryptedRet);

intranet_closedb();
?>