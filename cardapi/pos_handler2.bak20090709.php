<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

/*
#####
# return:
-4 : Refund amount larger than original
-3 : Invalid key
-2 : Argument Error
-1 : No this student
0 : Not enough balance
1 : Successful transaction
######
*/
if (!$payment_api_open_purchase)
{
     echo -1;
     exit();
}

/*
# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,2))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo "-1";
     exit();
}

$username = getUsername($key);
*/


/*
Param:
$key - Secret Key
$amount - Amount to be paid
$item - Item name to be on the transaction record
$CardID - Card ID of the student
$type - 1 (credit/refund), -1 (debit)
$refund - 1 (yes), else no
$rand - random string
*/

# Check Arguments
$string_amount = $amount;
$amount = (float)$amount;
if ($CardID == "" || $amount <= 0 || $item == "" || ($type!=1 && $type!=-1))
{
    echo -2;
    exit();
}


# Check secret key
$keysalt = array("1sxvga12");
$delimiter = "###";
$valid = false;

foreach ($keysalt as $t_key => $t_value)
{
    $string = $t_value.$delimiter.$string_amount.$delimiter.$item.$delimiter.$CardID.$delimiter.$type.$delimiter.$refund.$delimiter.$rand;
    $hashed_key = md5($string);
    if ($key==$hashed_key)
    {
        $valid = true;
    }
    else
    {
    }
}

if (!$valid)
{
     echo "-3";
     exit();
}

# Lock tables
$sql = "LOCK TABLES
             PAYMENT_ACCOUNT WRITE
             , INTRANET_USER READ
             , PAYMENT_OVERALL_TRANSACTION_LOG WRITE
             ";
$lpayment->db_db_query($sql);

# Retrieve UserID
$sql = "SELECT UserID, EnglishName, ChineseName, ClassName, ClassNumber FROM INTRANET_USER WHERE CardID = '$CardID' AND (RecordType = 1 OR RecordType = 2) AND RecordStatus = 1";
$temp = $lpayment->returnArray($sql,5);
list($uid, $t_engName, $t_chiName, $t_className, $t_classNum) = $temp[0];
if ($uid == "" || $uid == 0)
{
    echo -1;
}
else
{
    # Check Amount enough
    $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = $uid";
    $temp = $lpayment->returnVector($sql);
    $balance = $temp[0]+0;

    if ($type==-1 && $balance < $amount)         # Not enough balance
    {
        echo "0###".number_format($balance,2);
        echo "###".date('Y-m-d H:i:s');
    }
    else
    {
        if ($type==-1)
        {
            # Deduct balance
            $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance - $amount
                           ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                           WHERE StudentID = $uid";
            $lpayment->db_db_query($sql);
            # Insert Transaction Record
            $balanceAfter = $balance - $amount;
            $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                           (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details)
                           VALUES
                           ($uid, 3,'$amount',NULL,'$balanceAfter',NOW(),'$item')";
            $lpayment->db_db_query($sql);
            echo "1###".number_format($balanceAfter,2);
            echo "###".date('Y-m-d H:i:s');
        }
        else if ($type ==1)
        {
             if ($refund == 1)
             {
                 # Retrieve Record to refund
                 $sql = "SELECT LogID, Amount, BalanceAfter FROM PAYMENT_OVERALL_TRANSACTION_LOG
                                WHERE StudentID = '$uid' ORDER BY TransactionTime DESC LIMIT 0,1";
                 $temp = $lpayment->returnArray($sql,3);
                 list($t_LogID, $t_Amount, $t_BalanceAfter) = $temp[0];
                 if ($amount > $t_Amount)
                 {
                     echo "-4###".number_format($t_Amount,2);
                     echo "###".date('Y-m-d H:i:s');
                 }
                 else
                 {
                     # Increment balance
                     $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount
                                    ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                                    WHERE StudentID = $uid";
                     $lpayment->db_db_query($sql);

                     # Update Transaction Log
                     $t_Amount = $t_Amount - $amount;
                     $t_BalanceAfter = $t_BalanceAfter + $amount;
                     $sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET Amount = '$t_Amount',
                                    BalanceAfter = '$t_BalanceAfter',
                                    TransactionTime = NOW() WHERE LogID = $t_LogID";
                     $lpayment->db_db_query($sql);
                     echo "1###".number_format($t_BalanceAfter,2);
                     echo "###".date('Y-m-d H:i:s');
                 }
             }
             else
             {
                 # Increment balance
                 $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount
                            ,LastUpdateByAdmin = NULL,LastUpdateByTerminal='$username',LastUpdated = NOW()
                            WHERE StudentID = $uid";
                 $lpayment->db_db_query($sql);

                 # Insert Credit Transaction Record
                 $sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (StudentID, Amount,RecordType, RecordStatus, AdminInCharge, TransactionTime)
                                VALUES ($uid, '$amount',3,1,'$username', NOW())";
                 # RecordType 3 means 增值機
                 $lpayment->db_db_query($sql);
                 $trans_id = $lpayment->db_insert_id();

                 # Update RefCode
                 $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET RefCode = 'AVM$trans_id'
                                WHERE TransactionID = $trans_id";
                 $lpayment->db_db_query($sql);

                 # Insert Transaction Record
                 $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = $uid";
                 $temp = $lpayment->returnVector($sql);
                 $balanceAfter = $temp[0];
                 #$detail = "增值機 / Add value Machine";
                 $detail = $item;

                 $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                            (StudentID, TransactionType, Amount, RelatedTransactionID, BalanceAfter, TransactionTime, Details, RefCode)
                            VALUES
                        ($uid, 3,'$amount',$trans_id,'$balanceAfter',NOW(),'$detail','AVM$trans_id')";
                 $lpayment->db_db_query($sql);
                 echo "1###".number_format($balanceAfter,2);
                 echo "###".date('Y-m-d H:i:s');
             }
        }
    }

}

$sql = "UNLOCK TABLES";
$lpayment->db_db_query($sql);

# Student information
echo "###$t_engName###$t_chiName###$t_className###$t_classNum";



intranet_closedb();
?>