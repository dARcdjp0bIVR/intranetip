<?
include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

/*
#####
# return:
-4 : Refund amount larger than original
-3 : Invalid key
-2 : Argument Error
-1 : No this student
0 : Not enough balance
1 : Successful transaction
######
*/
if (!$payment_api_open_purchase)
{
     echo -1;
     exit();
}

/*
# Check IP
if (!isIPAllowed()) exit();

# Check session key
if (!checkSession($key)) exit();

# Check Access Control
if (!checkAccess($key,2))
{
     if ($dlang=="b5")
     {
         $msg = "你不可以進入繳費系統, 請與管理員聯絡.";
     }
     else $msg = "You cannot enter Payment System. Please contact Administrator.";
     echo "-1";
     exit();
}

$username = getUsername($key);
*/


/*
Param:
$key - Secret Key
$amount - Amount to be paid
$item - Item name to be on the transaction record
$CardID - Card ID of the student
$type - 1 (credit/refund), -1 (debit)
$refund - 1 (yes), else no
$rand - random string
*/

echo "Received from POST:\n";
print_r($_POST);

echo "Received from GET:\n";
print_r($_GET);

intranet_closedb();
?>