<?
// using 

/********************** Change Log ***********************/
#	Date	:	2013-02-20 (Yuen)
#				support authentication using login ID and password
#
/******************* End Of Change Log *******************/

######################################################################
###																   ###
### PLEASE also change the coding in ricoh_student_authenicate.php ###
###																   ###
######################################################################

#*********** IMPORTANT [Start] ************
/*
 * 		update $PrinterIPLincense in /includes/settings.php in order to add Printer IPs
 */
#************ IMPORTANT [End] *************


include_once("../includes/global.php");
include_once("../includes/libdb.php");
include_once("../includes/libpayment.php");
include_once("functions.php");
intranet_opendb();
$lpayment = new libpayment();

/*
#####
# return:
-4 : Refund amount larger than original
-3 : Invalid key
-2 : Argument Error
-1 : No this student
1 : success student authentication
######
*/
if (!$payment_api_open_purchase)
{
     echo -1;
     exit();
}


/*
Param:
$key - Secret Key
$CardID - Card ID of the student
$LicenseMode - 1 for using license mode
$rand - random string
*/
# Check Arguments
if (($CardID == "" && ($ULogin=="" || $UPwd==""))|| $key == "")
{
    echo -2;
    exit();
}


# Check secret key
if ($LicenseMode != 1) {
	$keysalt = array("1sxvga12");
}
else {
	$client_ip = getRemoteIpAddress();
	if ($PrinterIPLincense[$client_ip] != "") {
		$keysalt = array($PrinterIPLincense[$client_ip]);
	}
	else {
		echo "-3";
		exit();
	}
}
$delimiter = "###";
$valid = false;

foreach ($keysalt as $t_key => $t_value)
{
    $string = $t_value.$delimiter.$CardID.$delimiter.$rand;
    $hashed_key = md5($string);

    if ($key==$hashed_key)
    {
        $valid = true;
    }
    else
    {
    }
}

if (!$valid)
{
     echo "-3";
     exit();
}

# Retrieve UserID
if ($CardID!="")
{
	$sql = "SELECT UserID, EnglishName, ChineseName, ClassName, ClassNumber, CardID FROM INTRANET_USER WHERE CardID = '$CardID' AND (RecordType = 1 OR RecordType = 2) AND RecordStatus = 1";
	$temp = $lpayment->returnArray($sql,6);
	list($uid, $t_engName, $t_chiName, $t_className, $t_classNum, $t_CardID) = $temp[0];
} elseif ($ULogin!="" && $UPwd!="")
{
	if ($intranet_authentication_method == 'LDAP')
	{
		include_once("../includes/libldap.php");
		$lldap = new libldap();
   		$lldap->connect();
   		$PasswordVerified = $lldap->validate($ULogin,$UPwd);
	} else
	{
		include_once("../includes/libauth.php");
		$li = new libauth();
		$PasswordVerified = $li->validate($ULogin, $UPwd);
	}
	
	# get info if login is authenticated
	if ($PasswordVerified)
	{
		$sql = "SELECT UserID, EnglishName, ChineseName, ClassName, ClassNumber, CardID FROM INTRANET_USER WHERE UserLogin = '$ULogin' AND (RecordType = 1 OR RecordType = 2) AND RecordStatus = 1";
		$temp = $lpayment->returnArray($sql,6);
		list($uid, $t_engName, $t_chiName, $t_className, $t_classNum, $t_CardID) = $temp[0];
	}
}

if ($uid == "" || $uid == 0)
{
    echo -1;
}
else
{
	//echo mb_convert_encoding("1###$t_engName###$t_chiName###$t_className###$t_classNum","BIG5","UTF-8");
	# no need to change encoding in IP 2.5!
	echo "1###$t_engName###$t_chiName###$t_className###$t_classNum###$t_CardID";
}

intranet_closedb();
?>