<?php
/*
 * Editing by 
 * 
 * Modification Log: 
 * 2013-06-06 (Jason)
 * 		- exit this page as it is no longer used in admin console
 */

#####################################################
# this page is no longer used
if ($plugin['library_management_system'])
	exit();
#####################################################

include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_eLibrary_System, '') ?>
<?= displayTag("head_eLibrary_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?php if (trim($warning_message) != "") { ?>
<div><?=$warning_message.'<br><br>'?></div>
<?php } ?>
<?php
	echo displayOption($i_eLibrary_System_Admin_User_Setting, 'admin_setting.php',1);
	if ($plugin['eLib_ADMIN'])
	{
		//echo displayOption($i_eLibrary_System_Update_Batch, 'batch.php',1);
	}
	//echo displayOption($eReportCard["TeachingAppointmentSettings"], 'teaching.php',1);
?>	
<!--<?=$display?>-->

</blockquote>
</td></tr>
</table>

<?php
intranet_closedb();

include_once("../../templates/adminfooter.php");
?>