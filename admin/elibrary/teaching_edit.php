<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libteaching.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$lu = new libuser($tid);
$name = $lu->EnglishName;
$lteaching = new libteaching();
?>
<script language="javascript">
function checkform(obj){
	var jSize = obj.size.value;
	var i, jSubjectID, jClassIDList;

	for (i=0; i<jSize; i++)
	{
		jSubjectObj = eval("obj.subject"+i);
		jClassIDListObj = eval("obj.class_id_list"+i);

		jSubjectID = jSubjectObj.value;
		jClassIDList = jClassIDListObj.value;

		if(jSubjectID>0 && jClassIDList=='')
		{
			alert('<?=$eReportCard['AssignSubjectClassWarning']?>');
			jSubjectObj.focus;

			return false;
		}
	}

	return true;
}

function jSELECT_CLASSES(jID)
{
	jCIDList = eval("document.form1.class_id_list"+jID+".value");
	url = "teaching_add_class.php?ID="+jID+"&CIDList="+jCIDList;
	newWindow(url,15);
}

function jADD_CLASSES(jSID, jCIDList, jCNameList)
{
	document.getElementById("class_list"+jSID).innerHTML = jCNameList;
	var jObj = eval("document.form1.class_id_list"+jSID);
	jObj.value = jCIDList;
}
</script>

<form name="form1" action="teaching_edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_fs, '', $i_ReportCard_System, 'index.php', $eReportCard["TeachingAppointmentSettings"], 'teaching.php', $name, '') ?>
<?= displayTag("head_reportcard_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<?=$lteaching->displayReportCardTeacherEdit($tid)?>
</blockquote>
</td></tr></table>
<input type=hidden name=tid value="<?=$tid?>" />
<input type=hidden name=SelectedClass />
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
<!--<?= btnReset() ?>-->
<a href="javascript:self.location.reload();"><img src='/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
&nbsp;<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</p>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>