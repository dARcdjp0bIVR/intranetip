<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."file/elibrary/elibrary_batch.php");
include_once("../../includes/libaccount.php");
intranet_opendb();

$db = new libdb();
$LibeLib = new elibrary();
$fs = new libfilesystem();

if (!$LibeLib->HAS_RIGHT())
{
     header ("Location: /admin/main.php");
     intranet_closedb();
     exit();
}

if (!$plugin['eLib_ADMIN'])
{
     header ("Location: /admin/main.php");
     intranet_closedb();
     exit();
}
include_once("../../templates/adminheader_setting.php");

$filesArr = Array();

//debug_r($elib_batch);

$batch_file_path = $PATH_WRT_ROOT."/home/eLearning/elibrary/batch_definition/";
$batch_file_path_custom = $PATH_WRT_ROOT."/file/elibrary/";

foreach($elib_batch as $key => $data)
{
	$filesArr[] = array("filename" => "batch".$key.".txt", "status" => $data["value"], "desc" => $data["desc_".$intranet_session_language], "batch_index" => $key);
}

//debug_r($filesArr);

// reset all books publish status to private
$sql = "UPDATE INTRANET_ELIB_BOOK SET Publish = '0'";
$result_update = $db->db_db_query($sql);

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_eLibrary_System, 'index.php', $i_eLibrary_System_Update_Batch, '') ?>
<?= displayTag("head_eLibrary_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>

<?php
if(!$result_update)
{
	echo "<p><b>".$i_eLibrary_System_Reset_Fail."</b></p>";	
	exit;
}
else
{
	echo "<p><b>".$i_eLibrary_System_Updated_Batch.":</b></p>";
}

for($i = 0; $i < count($filesArr); $i++)
{
	if($filesArr[$i]["status"])
	{

	$tmpFile = (($filesArr[$i]["batch_index"]=="custom") ? $batch_file_path_custom:$batch_file_path).$filesArr[$i]["filename"];
	$result = $fs->file_read($tmpFile);
	
	//debug_r($result);
	$tmpBookIDArr = Array();
	$tmpBookIDArr =  preg_split("[\n\r\t ]+", $result);
	//debug_r($tmpBookIDArr);
	
	if(is_array($tmpBookIDArr))
	{
		$BookIDArr = Array();
		for($k = 0; $k < count($tmpBookIDArr); $k++)
		{
			if(trim($tmpBookIDArr[$k]) != "" && $tmpBookIDArr[$k] > 0 && !in_array($tmpBookIDArr[$k], $BookIDArr))
			$BookIDArr[] = trim($tmpBookIDArr[$k]);
		}
		
		if(count($BookIDArr) > 0)
		{
			// set the batch books to publish
			$BookIDSet = implode(",", $BookIDArr);
			$sql = "UPDATE INTRANET_ELIB_BOOK SET Publish = '1' WHERE BookID in ($BookIDSet)";
			$result_update_batch = $db->db_db_query($sql);
			$num_records_updated = $db->db_affected_rows();
			
			// success
			if($result_update_batch)
			{
				if($filesArr[$i]["status"])
				{
					echo "<p>".$filesArr[$i]["desc"]." - <b>".$i_eLibrary_System_Updated_Records." (".$num_records_updated.")</b></p>";
				}
			} // end if success query update
		} // end if number of BookID > 0
		else
		{
			echo "<p>".$filesArr[$i]["desc"]." - <b>".$i_eLibrary_System_Updated_Records." (0)</b></p>";
		}
	} // end if number Book ID in file > 0
	else
	{
		echo "<p>".$filesArr[$i]["desc"]." - <b>".$i_eLibrary_System_Updated_Records." (0)</b></p>";
	}
} // end if batch status
} // end for each batch


//debug_r($result_update);
//debug_r($result_update_batch);
//debug_r($sql);
?>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<a href="batch_update.php"><img src="/images/admin/button/s_btn_update_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</td></tr></table>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>