<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$values = "";
$delim = "";
for ($i=0; $i<sizeof($targetUserID); $i++)
{
     $target = $targetUserID[$i];
     $values .= "$delim('$target','$adminlevel')";
     $delim = ",";
}
$sql = "INSERT IGNORE INTO STARPICKING_ADMIN_USER_ACL (UserID, RecordType)
               VALUES $values";
$li->db_db_query($sql);

intranet_closedb();
header("Location: admin.php?msg=1");
?>