<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libqualied_starpicking.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lqualied_starpicking = new libqualied_starpicking();
$toolbar = "<a class=iconLink href=\"admin_new.php\">".newIcon()."$button_add</a>";

?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_QualiEd_StarPicking, 'index.php',$i_QualiEd_StarPicking_Admin,'') ?>
<?= displayTag("head_service_mgmt_$intranet_session_language.gif", $msg) ?>

<SCRIPT language=Javascript>
function removeAdminUser(id)
{
         if (confirm('<?=$i_AdminJob_Announcement_RemoveAdmin?>'))
         {
             location.href = "admin_remove.php?aid="+id;
         }
}
</SCRIPT>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $toolbar; ?></td></tr>
<tr>
<td>
<?=$lqualied_starpicking->displayAdminListTable()?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>