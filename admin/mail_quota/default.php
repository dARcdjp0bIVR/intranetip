<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

$li = new libfilesystem();
$file_content = $li->file_read($intranet_root."/file/default_mail_quota.txt");
$id_names = array($i_identity_teachstaff,$i_identity_student,$i_identity_parent);
if ($file_content == "")
{
   $userquota = array(10,10,10);
}
else
{
    $userquota = explode("\n", $file_content);
}
// $i_Campusquota_basic_identity
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         d = obj.quota0;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }

         d = obj.quota1;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }

         d = obj.quota2;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }
         return true;
}
</SCRIPT>


<form name="form1" action="default_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccount_Webmail_QuotaSetting, 'index.php',$i_LinuxAccount_SetDefaultQuota,'') ?>
<?= displayTag("head_webmail_quota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_identity ?></u></td>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_LinuxAccount_Quota ?></u></td>
</tr>
<?php for($i=0; $i<sizeof($userquota); $i++) {
$q = $userquota[$i];

?>
<tr>
<td style="vertical-align:middle"><?=$id_names[$i]?></td>
<td><input class=text type=text name=quota<?=$i?> size=7 maxlength=10 value="<?php echo $q; ?>"></td>
</tr>
<?php } ?>
</table>

</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>