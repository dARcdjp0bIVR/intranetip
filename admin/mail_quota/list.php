<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/liblinux.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

// $i_Campusquota_basic_identity
$user_select = "<SELECT name=UserType onChange=\"this.form.type[0].checked=true\">
<OPTION value=1>$i_identity_teachstaff</OPTION>
<OPTION value=2>$i_identity_student</OPTION>
<OPTION value=3>$i_identity_parent</OPTION>
</SELECT>";

$lg = new libgroup();
$sql = "SELECT a.GroupID,CONCAT('(',b.CategoryName,') ',a.Title)
        FROM INTRANET_GROUP as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
        ORDER BY b.GroupCategoryID ASC, a.Title";
$groups = $lg->returnArray($sql,2);
$group_select = getSelectByArray($groups,"name=GroupID onChange=\"this.form.type[1].checked=true\"");

?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         if (obj.type[0].checked) obj.action = 'list_type.php';
         else obj.action = 'list_group.php';
}
</SCRIPT>
<form name="form1" action="" method="get" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccountQuotaSetting, 'index.php',$i_LinuxAccount_DisplayQuota,'') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<input type=radio name=type value=0 CHECKED> <?=$i_LinuxAccount_SelectUserType?> <?=$user_select?><br>
<input type=radio name=type value=1> <?=$i_LinuxAccount_SelectGroup?> <?=$group_select?>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>