<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/liblinux.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
# Search Type
$type_name = $i_identity_array[$UserType];

?>
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccountQuotaSetting, 'index.php',$i_LinuxAccount_DisplayQuota,'list.php',$type_name,'') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>
<?
if ($UserType == 1 || $UserType == 3 || $ClassName != "")
{
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle><?=$i_UserName?></td>
<td class=tableTitle><?=$i_LinuxAccount_Quota?> (Mbytes)</td>
<td class=tableTitle><?=$i_LinuxAccount_UsedQuota?> (Mbytes)</td>
</tr>
<?
# Retrieve Quota
$llinux = new liblinux();
$list = $llinux->getQuotaTable();
for ($i=0; $i<sizeof($list); $i++)
{
     list($login,$delimiter,$used,$soft,$hard) = $list[$i];
     $quota[$login] = array($used,$soft);
}
$name_field = getNameFieldWithClassNumberByLang();
if ($UserType == 1 || $UserType == 3)
{
    $sql = "SELECT UserID, UserLogin, $name_field FROM INTRANET_USER WHERE RecordType = $UserType ORDER BY ClassName, ClassNumber, EnglishName";
}
else
{
    $sql = "SELECT UserID, UserLogin, $name_field FROM INTRANET_USER WHERE RecordType = $UserType AND ClassName = '$ClassName' ORDER BY ClassName, ClassNumber, EnglishName";
}
#echo "1. $sql\n";
$users = $lclass->returnArray($sql,3);
for ($i=0; $i<sizeof($users); $i++)
{
     list($uid, $login, $name) = $users[$i];
     $user_used = $quota[$login][0];
     $user_quota = $quota[$login][1];
     if (!isset($quota[$login]))
     {
         $user_quota = "N/A";
     }
     else if ($user_quota == 0)
     {
         $user_quota = "$i_LinuxAccount_NoLimit";
     }
     if (!isset($quota[$login]))
     {
         $user_used = "N/A";
     }
     $css = ($i%2? "":"2");
     $en_login = urlencode($login);
     if ($user_quota == "N/A")
     {
         echo "<tr class=tableContent$css><td>$name ($login)</td><td colspan=2 align=center>$i_LinuxAccount_NoAccount</td></tr>\n";
     }
     else
     {
         echo "<tr class=tableContent$css><td><a href=user_set.php?loginName=$en_login>$name ($login)</a></td><td>$user_quota</td><td>$user_used</td></tr>\n";
     }
}

?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</td>
</tr>
</table>
<?
}
else
{
    $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()");
?>
<form name=form1 action='' method=GET>
<blockquote>
<?=$select_class?>
</blockquote>
<input type=hidden name=UserType value="<?=$UserType?>">
</form>
<?
}
?>


<?php
include_once("../../templates/adminfooter.php");
?>