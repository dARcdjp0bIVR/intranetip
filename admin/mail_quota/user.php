<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if ($error==1) $xmsg = $i_LinuxAccount_NotExist;
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (!check_text(obj.loginName,'<?=$i_LinuxAccount_Alert_NameMissing?>')) return false;
         return true;
}
</SCRIPT>


<form name="form1" action="user_set.php" method="get" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccount_Webmail_QuotaSetting, 'index.php',$i_LinuxAccount_SetUserQuota,'') ?>
<?= displayTag("head_webmail_quota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<?=$i_LinuxAccount_AccountName?>: <input type=text size=40 name=loginName>

</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>