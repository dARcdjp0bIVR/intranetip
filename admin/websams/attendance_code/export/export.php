<?php
// editing by 
/************************************************ Changes ******************************************************
 * 2018-09-26 (Carlos): Fixed archived profile record type to match with reason type.
 * 2018-01-02 (Carlos): Fix and apply default early leave reason code to LE record.
 * 2017-03-15 (Carlos): Handle options [Merge and count as whole day(WD) if AM and PM status are the same.] 
 * 						and [Count as whole day(WD) if only has half day(AM/PM) attendance record.].
 * 2013-01-21 (Carlos): Fix archive user NON_ATT_TYPE 
 * 2011-12-19 (Carlos): Added query for selecting archived students
 ***************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwebsamsattendcode.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

$MAX_ROWS_ALLOWED = 10000;  // allow to export maximum 10000 records

$err_msg = array();
$found_error = false;

# check Websams Basic School Information
$lwebsams_code = new libwebsamsattendcode();
$lwebsams_code->retrieveBasicInfo();

if($lwebsams_code->SchoolID=="" || $lwebsams_code->SchoolYear=="" 
	|| $lwebsams_code->SchoolLevel==""|| $lwebsams_code->SchoolSession==""){
		
		$err_msg[] = $i_WebSAMS_Attendance_Export_Warning1;
		$found_error = true;
}

$li = new libdb();

// this function checks whether a student has only one day session record on that day
// pass the result set by reference to save memory
function isOnlyHalfDay(&$records, $currentRow, $date, $studentId, $dayType)
{
	$lower_bound = 0;
	$upper_bound = count($records)-1;
	// check the previous 3 records
	for($i=max($currentRow-3,$lower_bound);$i<$currentRow;$i++)
	{
		if($records[$i]['AttendanceDate'] != $date || $records[$i]['UserID'] != $studentId){
			continue;
		}
		
		if($records[$i]['NON_ATT_SESS'] != $dayType){
			return false;
		}
	}
	// check the following 3 records
	for($i=$currentRow+1;$i<=min($currentRow+3,$upper_bound);$i++)
	{
		if($records[$i]['AttendanceDate'] != $date || $records[$i]['UserID'] != $studentId){
			continue;
		}
		
		if($records[$i]['NON_ATT_SESS'] != $dayType){
			return false;
		}
	}
	
	return true;
}

# check ClassLevel
if(!$found_error){
	$sql = "SELECT 
						YearName 
					FROM 
						YEAR 
					WHERE 
						RecordStatus=1 
						AND 
						(
							WEBSAMSCode IS NULL 
							or 
							WEBSAMSCode = ''
						)";
	$level_list = $li->returnVector($sql);
	
	if(sizeof($level_list)>0){ # there exists some class level which does not map to websams class level
	
		$err_msg[] = $i_WebSAMS_Attendance_Export_Warning2."<br><li>".implode("<li>",$level_list);
		$found_error = true;
	}
	
}

# count records, if total records > MAX_ROWS_ALLOWED (e.g 10000)  , ask user to adjust Date Range.
if(!$found_error){
	$sql = "SELECT 
						COUNT(*) 
					FROM 
						PROFILE_STUDENT_ATTENDANCE 
					WHERE AttendanceDate Between '$FromDate' AND '$ToDate'";
	
	$temp = $li->returnVector($sql);
	if($temp[0]>$MAX_ROWS_ALLOWED){
		$err_msg[] = $i_WebSAMS_Attendance_Export_Warning3; 
		$found_error = true;
	}
}

if($found_error){ # listing out the errors
	include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
	?>
	<form name="form1">
		<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../index.php', $i_WebSAMS_Menu_AttendanceCode, '../index.php',$button_export,'') ?>
		<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>
		<br>
		<blockquote>
		<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
			<?php
				for($i=0;$i<sizeof($err_msg);$i++)
					echo "<tr><td>".$err_msg[$i]."</td></tr>";
		
			?>
		</table>
		</blockquote>

		<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
		<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
		<tr><td align="right">
		 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
		</td>
		</tr>
		</table>
	</form>
	
<?php
	include_once($PATH_WRT_ROOT."templates/adminfooter.php");
}
else{
	# get codeID for other reasons
	$sql ="SELECT 
		CASE ReasonType
			WHEN  '".PROFILE_TYPE_ABSENT."' THEN 'ABSNT'
			WHEN  '".PROFILE_TYPE_LATE."' THEN 'LATE'
			WHEN  '".PROFILE_TYPE_EARLY."' THEN 'LEAVE'
		END, 
		CodeID 
		FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE RecordType=1 ORDER By ReasonType";
	$r = $li->returnArray($sql,2);
	for($i=0;$i<sizeof($r);$i++){
		list($type,$code) = $r[$i];
		$other[$type]=$code;
	}
	//print_r($other);
		
	 # no error, then export
	$SCHOOL_ID		= $lwebsams_code->SchoolID;
	$SCHOOL_YEAR	= $lwebsams_code->SchoolYear;
	$SCHOOL_LEVEL	= $lwebsams_code->SchoolLevel;
	$SCHOOL_SESS	= $lwebsams_code->SchoolSession;
				
	$GetArchivedUser = strstr($StudentStatus,"3")!==FALSE;
	
	/*
	$start_ts = strtotime($FromDate);
	$end_ts = strtotime($ToDate);
	$cur_year = intval(date("Y",$start_ts));
	$cur_month = intval(date("n",$start_ts));
	$cur_day = 1;
	
	$uidDateToDailylog = array();
	for($cur_ts=$start_ts;$cur_ts<=$end_ts;$cur_ts=mktime(0,0,0,$cur_month+1,$cur_day,$cur_year))
	{
		$cur_year = intval(date("Y",$cur_ts));
		$cur_month = intval(date("n",$cur_ts));
		
		//echo "$cur_ts ".date("Y-m-d",$cur_ts)." ".$cur_year." ".$cur_month."<br />\n";
		
		$CARD_STUDENT_DAILY_LOG_TABLE = sprintf("CARD_STUDENT_DAILY_LOG_%4d_%02d",$cur_year,$cur_month);
		// This query can get RecordStatus IN (0,1,2,3) i.e. Suspended/Inactive, Active, Left
		$sql="SELECT 
						t.UserID,
						DATE_FORMAT(CONCAT('$cur_year-$cur_month-',t.DayNumber),'%Y-%m-%d') as RecordDate,
						t.AMStatus,
						t.PMStatus,
						t.LeaveStatus 
					FROM 
						$CARD_STUDENT_DAILY_LOG_TABLE as t 
						inner join INTRANET_USER AS b ON t.UserID = b.UserID 
						inner join YEAR_CLASS AS d on b.ClassName = d.ClassTitleEN and d.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
						inner join YEAR AS c on d.YearID = c.YearID 
					WHERE b.RecordType=2 and b.RecordStatus IN (".$StudentStatus.") AND (DATE_FORMAT(CONCAT('$cur_year-$cur_month-',t.DayNumber),'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate') 
					ORDER BY t.UserID,t.DayNumber";
		
		$tmp_ary = $li->returnResultSet($sql);
	}
	
	//intranet_closedb();
	//exit;
	*/
	
	// This query can get RecordStatus IN (0,1,2,3) i.e. Suspended/Inactive, Active, Left
	$sql="SELECT 
					a.UserID,
					c.WEBSAMSCode AS CLASS_LEVEL,
					IF(d.WEBSAMSCode IS NULL, d.ClassTitleEN, d.WEBSAMSCode) AS CLASS_CODE,
					b.WebSAMSRegNo AS REG_NO,
					DATE_FORMAT(a.AttendanceDate,'%d/%m/%Y') AS NON_ATT_DATE,
					CASE a.DayType
						WHEN '".PROFILE_DAY_TYPE_AM."' THEN 'AM'
						WHEN '".PROFILE_DAY_TYPE_PM."' THEN 'PM'
						WHEN '".PROFILE_DAY_TYPE_WD."' THEN 'WD'
					END AS NON_ATT_SESS,
					CASE a.RecordType
						WHEN  '".PROFILE_TYPE_ABSENT."' THEN 'ABSNT'
						WHEN  '".PROFILE_TYPE_LATE."' THEN 'LATE'
						WHEN  '".PROFILE_TYPE_EARLY."' THEN 'LEAVE'
						ELSE  'PRESNT'
					END AS NON_ATT_TYPE,
					e.CodeID AS NON_ATT_REASON_CODE,
					a.AttendanceDate,
					b.ClassName,
					b.ClassNumber 
				FROM 
					PROFILE_STUDENT_ATTENDANCE AS a 
					inner join 
					INTRANET_USER AS b 
					ON 
						a.AttendanceDate BETWEEN '$FromDate' AND '$ToDate' 
						and a.UserID = b.UserID 
						and b.RecordType=2 and b.RecordStatus IN (".$StudentStatus.")
					inner join 
					YEAR_CLASS AS d 
					on b.ClassName = d.ClassTitleEN and d.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					inner join 
					YEAR AS c 
					on d.YearID = c.YearID 
					LEFT OUTER JOIN
					INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE AS e 
					ON 
						(a.RecordType = e.ReasonType 
						AND a.Reason = e.ReasonText 
						AND (e.RecordType IS NULL OR e.RecordType=0)
						)
				";
	
	
	if($GetArchivedUser){
		$sql .= "UNION 
				SELECT 
					a.UserID,
					c.WEBSAMSCode AS CLASS_LEVEL,
					IF(d.WEBSAMSCode IS NULL, d.ClassTitleEN, d.WEBSAMSCode) AS CLASS_CODE,
					b.WebSAMSRegNo AS REG_NO,
					DATE_FORMAT(a.RecordDate,'%d/%m/%Y') AS NON_ATT_DATE,
					CASE a.DayType
						WHEN '".PROFILE_DAY_TYPE_AM."' THEN 'AM'
						WHEN '".PROFILE_DAY_TYPE_PM."' THEN 'PM'
						WHEN '".PROFILE_DAY_TYPE_WD."' THEN 'WD' 
						ELSE a.DayType 
					END AS NON_ATT_SESS,
					CASE a.RecordType 
						WHEN 'ABSENT' THEN 'ABSNT' 
						WHEN 'LATE' THEN 'LATE'
						WHEN 'EARLY LEAVE' THEN 'LEAVE' 
						ELSE 'PRESNT'
					END AS NON_ATT_TYPE,
					e.CodeID AS NON_ATT_REASON_CODE,
					a.RecordDate as AttendanceDate,
					b.ClassName,
					b.ClassNumber 
				FROM 
					PROFILE_ARCHIVE_ATTENDANCE AS a 
					inner join 
					INTRANET_ARCHIVE_USER AS b 
					ON 
						a.RecordDate BETWEEN '$FromDate' AND '$ToDate' 
						and a.UserID = b.UserID 
						and b.RecordType=2 
					inner join 
					YEAR_CLASS AS d 
					on b.ClassName = d.ClassTitleEN and d.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					inner join 
					YEAR AS c 
					on d.YearID = c.YearID 
					LEFT OUTER JOIN
					INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE AS e 
					ON 
						(
							((a.RecordType='ABSENT' AND e.ReasonType='1')
							OR (a.RecordType='LATE' AND e.RecordType='2') 
							OR (a.RecordType='EARLY LEAVE' AND e.RecordType='3')
							) 
							AND a.Reason = e.ReasonText 
							AND (e.RecordType IS NULL OR e.RecordType=0)
						)";
	}
	
	$sql .= "ORDER BY 
				 AttendanceDate, ClassName, ClassNumber, NON_ATT_SESS, NON_ATT_TYPE";
	
	//echo $sql;		
	//die;
	
//		$prev_record = "";
		$curr_record = "";
		$csv = "SCHOOL_ID,SCHOOL_YEAR,SCHOOL_LEVEL,SCHOOL_SESS,CLASS_LEVEL,CLASS_CODE,REG_NO,NON_ATT_DATE,NON_ATT_SESS,NON_ATT_TYPE,NON_ATT_REASON_CODE,NON_ATT_REASON_CODE_2\n";
//		$utf_csv = "SCHOOL_ID \tSCHOOL_YEAR \tSCHOOL_LEVEL \tSCHOOL_SESS \tCLASS_LEVEL \tCLASS_CODE \tREG_NO \tNON_ATT_DATE \tNON_ATT_SESS \tNON_ATT_TYPE \tNON_ATT_REASON_CODE \tNON_ATT_REASON_CODE_2\r\n";
		$result = $li->returnArray($sql);
		$result_size = count($result);
		
		
		
		for($i=0;$i<$result_size;$i++){
			list($uid, $CLASS_LEVEL,$CLASS_CODE,$REG_NO,$NON_ATT_DATE,$NON_ATT_SESS,$NON_ATT_TYPE,$NON_ATT_REASON_CODE)=$result[$i];
			
			if($skip_next_record){
				$skip_next_record = false;
				continue;
			}
			
			if($SCHOOL_LEVEL == -1)
			{		
				if(($CLASS_LEVEL == "K1") || ($CLASS_LEVEL == "K2") || ($CLASS_LEVEL == "K3") || ($CLASS_LEVEL == "LP"))
					$OUTPUT_SCHOOL_LEVEL = 1;

				if(($CLASS_LEVEL == "P1") || ($CLASS_LEVEL == "P2") || ($CLASS_LEVEL == "P3") || ($CLASS_LEVEL == "P4") || ($CLASS_LEVEL == "P5") || ($CLASS_LEVEL == "P6") || ($CLASS_LEVEL == "PP") || ($CLASS_LEVEL == "PR"))
					$OUTPUT_SCHOOL_LEVEL = 2;
				
				if(($CLASS_LEVEL == "S1") || ($CLASS_LEVEL == "S2") || ($CLASS_LEVEL == "S3") || ($CLASS_LEVEL == "S4") || ($CLASS_LEVEL == "S5") || ($CLASS_LEVEL == "S6") || ($CLASS_LEVEL == "S7") || ($CLASS_LEVEL == "SJ") || ($CLASS_LEVEL == "SS") || ($CLASS_LEVEL == "UP"))
					$OUTPUT_SCHOOL_LEVEL = 3;
			}
			else
			{
				$OUTPUT_SCHOOL_LEVEL = $SCHOOL_LEVEL;
			}
	
			if(substr(trim($REG_NO),0,1)=="#")
				$REG_NO = substr(trim($REG_NO),1);
			if($NON_ATT_TYPE!="PRESNT"){
				if($NON_ATT_REASON_CODE=="")
					$NON_ATT_REASON_CODE = $other[$NON_ATT_TYPE];
			}
			$NON_ATT_REASON_CODE_2 = '';
			
			$skip_next_record = false;
			if($SameAMPMCountAsWD)
			{
				// on the same day, both AM and PM status are the same for the same student, merge the AM and PM records and display as one WD record
				if( ($i+1) < $result_size && $result[$i]['AttendanceDate'] == $result[$i+1]['AttendanceDate'] && $result[$i]['UserID'] == $result[$i+1]['UserID']
					&& $result[$i]['NON_ATT_TYPE'] == $result[$i+1]['NON_ATT_TYPE'] && $result[$i]['NON_ATT_SESS'] != $result[$i+1]['NON_ATT_SESS'])
				{
					$NON_ATT_SESS = 'WD';
					if($result[$i+1]['NON_ATT_REASON_CODE'] != $result[$i]['NON_ATT_REASON_CODE']) $NON_ATT_REASON_CODE_2 = $result[$i+1]['NON_ATT_REASON_CODE']!=''? $result[$i+1]['NON_ATT_REASON_CODE'] : $other[$result[$i+1]['NON_ATT_TYPE']];
					$skip_next_record = true;
				}
			}
			
			if($HalfDayCountAsWD)
			{
				if(!$skip_next_record && isOnlyHalfDay($result, $i, $result[$i]['AttendanceDate'], $result[$i]['UserID'], $result[$i]['NON_ATT_SESS']))
				{
					$NON_ATT_SESS = 'WD';
				}
			}
			
			if( ($i+1) < $result_size &&  $result[$i]['AttendanceDate'] == $result[$i+1]['AttendanceDate'] && $result[$i]['UserID'] == $result[$i+1]['UserID'] 
				&& $result[$i]['NON_ATT_SESS'] == $result[$i+1]['NON_ATT_SESS']	&& $result[$i]['NON_ATT_TYPE'] == 'LATE' && $result[$i+1]['NON_ATT_TYPE'] == 'LEAVE' )
			{
				$NON_ATT_TYPE = 'LE';
				$NON_ATT_REASON_CODE_2 = $result[$i+1]['NON_ATT_REASON_CODE'] != ''? $result[$i+1]['NON_ATT_REASON_CODE'] : $other[$result[$i+1]['NON_ATT_TYPE']];
				$skip_next_record = true;
			}
			
			//$curr_record = "$SCHOOL_ID,$SCHOOL_YEAR,$SCHOOL_LEVEL,$SCHOOL_SESS,$CLASS_LEVEL,$CLASS_CODE,$REG_NO,$NON_ATT_DATE,$NON_ATT_SESS,$NON_ATT_TYPE,$NON_ATT_REASON_CODE";
			//$utf_curr_record = "$SCHOOL_ID \t$SCHOOL_YEAR \t$SCHOOL_LEVEL \t$SCHOOL_SESS \t$CLASS_LEVEL \t$CLASS_CODE \t$REG_NO \t$NON_ATT_DATE \t$NON_ATT_SESS \t$NON_ATT_TYPE \t$NON_ATT_REASON_CODE";
			
			$curr_record = "$SCHOOL_ID,$SCHOOL_YEAR,$OUTPUT_SCHOOL_LEVEL,$SCHOOL_SESS,$CLASS_LEVEL,$CLASS_CODE,$REG_NO,$NON_ATT_DATE,$NON_ATT_SESS,$NON_ATT_TYPE,$NON_ATT_REASON_CODE,$NON_ATT_REASON_CODE_2";
//			$utf_curr_record = "$SCHOOL_ID \t$SCHOOL_YEAR \t$OUTPUT_SCHOOL_LEVEL \t$SCHOOL_SESS \t$CLASS_LEVEL \t$CLASS_CODE \t$REG_NO \t$NON_ATT_DATE \t$NON_ATT_SESS \t$NON_ATT_TYPE \t$NON_ATT_REASON_CODE";
			$csv.=$curr_record."\n";
/*			
			# Late & Early Leave
			if($prev_user_id ==$uid &&
				$prev_att_date == $NON_ATT_DATE &&
				$prev_att_sess == $NON_ATT_SESS &&
				(( $prev_status  == "LATE" && $NON_ATT_TYPE=="LEAVE" )||($prev_status=="LEAVE" && $NON_ATT_TYPE=="LATE"))){ # LE
				$NON_ATT_TYPE = "LE";
				$reason1 = $prev_status=="LATE"?$prev_reason_code:$NON_ATT_REASON_CODE;
				$reason2 = $prev_status=="EARLY"?$prev_reason_code:$NON_ATT_REASON_CODE;
				//$prev_record = "$SCHOOL_ID,$SCHOOL_YEAR,$SCHOOL_LEVEL,$SCHOOL_SESS,$CLASS_LEVEL,$CLASS_CODE,$REG_NO,$NON_ATT_DATE,$NON_ATT_SESS,$NON_ATT_TYPE,$reason1,$reason2";
				//$utf_prev_record = "$SCHOOL_ID \t$SCHOOL_YEAR \t$SCHOOL_LEVEL \t$SCHOOL_SESS \t$CLASS_LEVEL \t$CLASS_CODE \t$REG_NO \t$NON_ATT_DATE \t$NON_ATT_SESS \t$NON_ATT_TYPE \t$reason1 \t$reason2";
				
				$prev_record = "$SCHOOL_ID,$SCHOOL_YEAR,$OUTPUT_SCHOOL_LEVEL,$SCHOOL_SESS,$CLASS_LEVEL,$CLASS_CODE,$REG_NO,$NON_ATT_DATE,$NON_ATT_SESS,$NON_ATT_TYPE,$reason1,$reason2";
//				$utf_prev_record = "$SCHOOL_ID \t$SCHOOL_YEAR \t$OUTPUT_SCHOOL_LEVEL \t$SCHOOL_SESS \t$CLASS_LEVEL \t$CLASS_CODE \t$REG_NO \t$NON_ATT_DATE \t$NON_ATT_SESS \t$NON_ATT_TYPE \t$reason1 \t$reason2";
				$curr_record = "";
//				$utf_curr_record = "";
			}
			if($prev_record !=""){
				$csv.=$prev_record;
				//if($NON_ATT_REASON_CODE!="")
				//	$csv.= ",$NON_ATT_REASON_CODE";
				$csv.="\n";
				$prev_record="";
			}
//			if($utf_prev_record !=""){
//				$utf_csv.=$utf_prev_record;
				//if($NON_ATT_REASON_CODE!="")
				//	$csv.= ",$NON_ATT_REASON_CODE";
//				$utf_csv.="\r\n";
//				$utf_prev_record = "";
//			}
			$prev_record = $curr_record;
//			$utf_prev_record = $utf_curr_record;
			$prev_user_id = $uid;
			$prev_att_date = $NON_ATT_DATE;
			$prev_att_sess = $NON_ATT_SESS;
			$prev_status  = $NON_ATT_TYPE;
			$prev_reason_code = $NON_ATT_REASON_CODE;
			*/
/*
			if($i==sizeof($result)-1 && $curr_record!=""){
				$csv.=$curr_record;
				if($NON_ATT_REASON_CODE!="")
					$csv.= ",$NON_ATT_REASON_CODE";
			}
*/
			
		}

//		if($curr_record!=""){
//				$csv.=$curr_record;
				//if($NON_ATT_REASON_CODE!="")
				//	$csv.= ",$NON_ATT_REASON_CODE";
//		}
//		if($utf_curr_record!=""){
//				$utf_csv.=$utf_curr_record;
//		}
		$filename = "websams_attendance_record_".time().".csv";
		
		//$lexport->EXPORT_FILE($filename, $utf_csv);
		//$lexport->EXPORT_FILE($filename, $csv);
		output2browser($csv, $filename);
}
?>
<?
intranet_closedb();
?>