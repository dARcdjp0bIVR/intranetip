<?php
// editing by 
/*********************************************** Changes *******************************************************
 * 2017-03-15 (Carlos): Added options [Merge and count as whole day(WD) if AM and PM status are the same.] 
 * 						and [Count as whole day(WD) if only has half day(AM/PM) attendance record.].
 * 2011-12-19 (Carlos): Added option Student status for selecting Archived students 
 ***************************************************************************************************************/
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libwebsamsattendcode.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

$lwebsams_code = new libwebsamsattendcode();
$lwebsams_code->retrieveBasicInfo();

# default date range  = 1 Month
$currentMonth = date('n');
$FromDate = date('Y-m-d',mktime(0,0,0,$currentMonth,1));  // first day of current month
$ToDate   = date('Y-m-d',mktime(0,0,0,$currentMonth+1,0)); // last day of current month
$sql="SELECT COUNT(*) 
			FROM 
				INTRANET_USER 
			WHERE 
				RecordType=2 
				AND RecordStatus IN(0,1,2) 
				AND (WebSamsRegNo IS NULL OR TRIM(WebSamsRegNo) ='') "; 
$li = new libdb();
$temp = $li->returnVector($sql);
$count = $temp[0]+0;
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
	      // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           		document.forms['form1'].FromDate.value = dateValue;
                           
          }
          function calendarCallback2(date, month, year)
          {								
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
		                           document.forms['form1'].ToDate.value = dateValue;

          }


	function checkForm(formObj){
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			if(!checkDate(fromV)){
					return false;
			}
			else if(!checkDate(toV)){
						return false;
			}
				return true;
	}
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}

	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
</script>
<form name="form1" method="GET" onSubmit="return checkForm(this)" action="export.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../attendance_code/index.php', $i_WebSAMS_AttendanceCode_Export,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>
<table border=0 width=560 align=center>
<tr><td><?=$i_Profile_SelectSemester?>:</td></tr>
	<tr>
		<td nowrap class=tableContent>
		<br>&nbsp;&nbsp;<?=$i_Profile_From?>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;
		 	<?=$i_Profile_To?>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 </td></tr>
 <tr>
 	<td nowrap class=tableContent>
 	<?php if($lwebsams_code->SchoolSession==3){ // Only meaningful for WD mode ?>
 		<input type="checkbox" name="SameAMPMCountAsWD" id="SameAMPMCountAsWD" value="1" /><label for="SameAMPMCountAsWD"><?=$Lang['StudentAttendance']['WebSAMS_MergeAMPMAsWD']?></label><br />
 	<?php } ?>
 		<input type="checkbox" name="HalfDayCountAsWD" id="HalfDayCountAsWD" value="1" /><label for="HalfDayCountAsWD"><?=$Lang['StudentAttendance']['WebSAMS_HalfDayCountAsWD']?></label><br />
 	</td>
 </tr>
 <tr>
	<td nowrap class=tableContent>
		<?=$Lang['StudentAttendance']['StudentStatus']?>:&nbsp;
		<input type="radio" name="StudentStatus" value="0,1,2,3" ><?=$Lang['General']['All']?>
		&nbsp;
		<input type="radio" name="StudentStatus" value="0,1,2" checked="checked"><?=$Lang['Status']['Active'].' + '.$Lang['Status']['Suspended']?>
		<input type="radio" name="StudentStatus" value="1"><?=$Lang['Status']['Active']?>
	</td>
 </tr>
 <tr><td><?php 
 				if($count>0){
	 				echo "<Br><font color=red>$i_WebSAMS_AttendanceCode_No_RegNo_Students : $count</font><Br>";
	 			}
 		?></td></tr>
<tr><td><hr size=1 ></td></tr>
<tr><td align=right>
<a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a>
<?= btnReset() ?>
<a href="../index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</tr><td>
<?php
if($special_feature['websams_attendance_export']){
	$Notice1 = $i_WebSAMS_Notice_Export1;
	$Notice2 = "$i_WebSAMS_Notice_Export2";
	$Notice3 = "$i_WebSAMS_Notice_Export3";
	echo $i_WebSAMS_Notice_Export.$Notice1.$Notice2.$Notice3;
}
?></td></tr>
</table>
</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>