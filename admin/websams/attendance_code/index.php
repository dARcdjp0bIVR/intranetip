<?php
########################################
# Date:	2012-09-03	YatWoon
#		update the flag checking for Export Attendance Records, use $plugin['attendancestudent'] flag checking instead of $special_feature['websams_attendance_export'].
######################################## 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($plugin['WebSAMS']) && $plugin['WebSAMS']) {
	//$special_feature['websams_attendance_export']
	$displayOption = displayOption(
                  $i_WebSAMS_AttendanceCode_Basic, 'basic/',1,
                  $i_WebSAMS_AttendanceCode_ReasonCode, 'reason/',1,
                  $i_WebSAMS_AttendanceCode_Export,'export/',$plugin['attendancestudent']
                                );
} else {
	$displayOption = displayOption(
                  $i_WebSAMS_AttendanceCode_Basic, 'basic/',1
                                );
}
?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../attendance_code/index.php') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= $displayOption ?>


</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>