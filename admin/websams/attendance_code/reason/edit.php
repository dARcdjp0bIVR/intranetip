<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

if(is_array($ReasonRecordID) && sizeof($ReasonRecordID)>0)
	$recordID = $ReasonRecordID[0];
if($recordID=="")
	header("index.php");

$li = new libdb();
	# get codeID for other reasons
	$sql ="SELECT ReasonType,CodeID ,ReasonText
		FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE RecordType=1 ORDER By ReasonType";
	$r = $li->returnArray($sql,3);
	
	
$sql = "SELECT CodeID,ReasonText,ReasonType,RecordType FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE RecordID='$recordID'";

$temp = $li->returnArray($sql,4);

list($code_id,$reason_text,$reason_type,$record_type)=$temp[0];

$select_reason = "<SELECT name=\"reasonType\" onChange='changeText(this)'>\n";
# $select_reason.="<OPTION VALUE=''>-- $button_select --</OPTION>\n";
$select_reason.="<OPTION VALUE='1'".($reason_type==1?"SELECTED":"").">$i_Profile_Absent</OPTION>\n";
$select_reason.="<OPTION VALUE='2'".($reason_type==2?"SELECTED":"").">$i_Profile_Late</OPTION>\n";
$select_reason.="<OPTION VALUE='3'".($reason_type==3?"SELECTED":"").">$i_Profile_EarlyLeave</OPTION>\n";
$select_reason.="</SELECT>\n";

for($i=0;$i<sizeof($r);$i++){
	if($r[$i][0]==$reason_type)
		$current = $r[$i][1]." - ".$r[$i][2];
}

?>
<script language='javascript'>
other = new Array();
<?php
	for($i=0;$i<sizeof($r);$i++){
		list($type,$code,$text) = $r[$i];
		echo "other[$type]=\"$code - $text\";\n";
	}
?>
function changeText(obj){
	if(obj==null) return;
	index = obj.options[obj.selectedIndex].value;
	objCurrent = document.getElementById('current');
	if(objCurrent!=null){
		if(typeof(other[index])!='undefined')
			objCurrent.innerHTML = other[index];
		else objCurrent.innerHTML = '';
	}
	
}
</script>
<form name="form1" method="get" action="edit_update.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../attendance_code/index.php', $i_WebSAMS_AttendanceCode_ReasonCode,'index.php',$button_edit,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>
<br>
<blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=right><?=$i_WebSAMS_Attendance_Reason_ReasonCode?>: </td><td><input type="text" maxlength="3" name="codeID" size="3" value="<?=$code_id?>"></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td align=right><?=$i_WebSAMS_Attendance_Reason_ReasonText?>: </td><td><input type="text" name="reasonText" value="<?=htmlspecialchars($reason_text,ENT_QUOTES)?>" size="50"></td></tr>
<tr><td align=right></td><td>
	<table border=0 cellpadding=0 cellspacing=0><tr><td>
		<input type="checkbox" name="otherReason" value="1" <?=($record_type==1?"CHECKED":"")?>></td><td><?=$i_WebSAMS_Attendance_Reason_OtherReason_Notice?>.</td></tr>
		<tr><Td>&nbsp;</td><td><font color=red><?=$i_WebSAMS_Attendance_Reason_CurrenlyMapTo?>: <span id='current'><?=$current?></span></font></td></tr>
	</table>
</td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><td align=right><?=$i_WebSAMS_Attendance_Reason_ReasonType?>: </td><td><?=$select_reason?></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name="recordID" value="<?=$recordID?>">
</form>

<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>