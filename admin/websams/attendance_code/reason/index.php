<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field == "") $field = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
# TABLE INFO

$sql = "SELECT IF(RecordType=1, CONCAT(CodeID,'<font color=red>*</font>'),CodeID), 
			   CONCAT('<a href=\'edit.php?recordID=',RecordID,'\'>',ReasonText,'</a>'),
			   IF(ReasonType=1,'$i_Profile_Absent',IF(ReasonType=2,'$i_Profile_Late',IF(ReasonType=3,'$i_Profile_EarlyLeave','&nbsp'))),
	           CONCAT('<input type=checkbox name=ReasonRecordID[] value=',RecordID,'>')
 			   FROM INTRANET_WEBSAMS_ATTENDANCE_REASON_CODE WHERE (ReasonText LIKE '%$keyword%' 
 			   	OR CodeID LIKE '%$keyword%')
 	   ";
$conds = $record_type ==""?"":" AND ReasonType='$record_type' ";

$sql.=$conds;

$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("CodeID","ReasonText","ReasonType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=5 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_WebSAMS_Attendance_Reason_ReasonCode)."</td>\n";
$li->column_list .= "<td width=45% class=tableTitle>".$li->column(1, $i_WebSAMS_Attendance_Reason_ReasonText)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $i_WebSAMS_Attendance_Reason_ReasonType)."</td>\n";
$li->column_list .= "<td width=5% class=tableTitle>".$li->check('ReasonRecordID[]')."</td>\n";

// TABLE FUNCTION BAR
$searchbar  = "<select name=record_type onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value='' ".(($record_type=='')?"selected":"").">$i_status_all</option>\n";
$searchbar .= "<option value=1 ".(($record_type==1)?"selected":"").">$i_Profile_Absent</option>\n";
$searchbar .= "<option value=2 ".(($record_type==2)?"selected":"").">$i_Profile_Late</option>\n";
$searchbar .= "<option value=3 ".(($record_type==3)?"selected":"").">$i_Profile_EarlyLeave</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$toolbar  = "<a class=iconLink href='new.php'>".newIcon()."$button_new</a>\n".toolBarSpacer();
$toolbar .="<a class=iconLink href='import.php'>".importIcon().$button_import."</a>&nbsp;";
$toolbar .="<a class=iconLink href='export.php?keyword=".urlencode($keyword)."&record_type=$record_type&field=$field&order=$order'>".exportIcon().$button_export."</a>";

$functionbar =" <a href=\"javascript:checkEdit(document.form1,'ReasonRecordID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'ReasonRecordID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
?>
 

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../../attendance_code/index.php', $i_WebSAMS_AttendanceCode_ReasonCode,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center"><tr><td>
<font color=red>*</font> - <?=$i_WebSAMS_Attendance_Reason_OtherReason_Notice?>
</td></tr></table>

<br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td class=admin_bg_menu><img src=/images/space.gif width=1 height=4 border=0></td></tr>
<tr><td class=admin_bg_menu><img src=/images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?php
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>
