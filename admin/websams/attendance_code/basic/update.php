<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libwebsamsattendcode.php");
include_once("../../../../includes/libaccount.php");

intranet_opendb();

$li = new libwebsamsattendcode();

$sql = "SELECT COUNT(*) FROM INTRANET_WEBSAMS_ATTENDANCE_BASIC";
$result = $li->returnVector($sql);

if($result[0]==1){ # Record Exists , do UPDATE
	$sql = "UPDATE INTRANET_WEBSAMS_ATTENDANCE_BASIC SET SchoolID='$SchoolID',SchoolYear ='$SchoolYear',SchoolLevel='$SchoolLevel',SchoolSession='$SchoolSession' WHERE RecordID=0";
}
else{ # Record Not Exists , do INSERT
	$sql = "INSERT INTO INTRANET_WEBSAMS_ATTENDANCE_BASIC (SchoolID,SchoolYear,SchoolLevel,SchoolSession) VALUES('$SchoolID','$SchoolYear','$SchoolLevel','$SchoolSession') ";
}

$li->db_db_query($sql);
intranet_closedb();
header("Location: index.php?msg=2");
?>