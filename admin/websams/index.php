<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

$li = new libaccount();
$li->is_access_function($PHP_AUTH_USER);
$hasAccessRight = $li->is_access_school_settings;
?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent align=left>
<blockquote>
<?= displayOption(
                  $i_WebSAMS_Menu_Attendance, 'attendance/',$special_feature['websams_attendance_syn'],
                  $i_WebSAMS_Menu_AttendanceCode, 'attendance_code/',$special_feature['websams_attendance_export']
                                ) ?>


                                
</blockquote>
<Br><Br>
</td></tr>
</tr><td>
<?php
if($special_feature['websams_attendance_export']){
	$Notice1 = $i_WebSAMS_Notice_Export1;
	$Notice2 = "<a href='attendance_code/basic/'>$i_WebSAMS_Notice_Export2</a>";
	$Notice3 = "<a href='attendance_code/reason/'>$i_WebSAMS_Notice_Export3</a>";
	echo $i_WebSAMS_Notice_Export.$Notice1.$Notice2.$Notice3;
}
?></td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>