<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();


?>

<script language="javascript">
function submit2update(){
        document.form1.action='archive_update.php';
        parent.intranet_admin_menu.runStatusWin("/admin/pop_processing.php");
        return;
}
</script>


<form name="form1" method="POST" action="archive_update.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_WebSAMS_Integration, '../',$i_WebSAMS_Menu_Attendance,'') ?>
<?= displayTag("head_websams_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?=$i_AdminLogin?>:</td><td><input type=text name=input_userlogin size=20 maxlength=20 value=''></td></tr>
<tr><td align=right nowrap><?=$i_AdminPassword?>:</td><td><input type=password name=input_password size=20 maxlength=12 value=''></td></tr>
<tr><td align=right nowrap><?=$i_WebSAMS_Attendance_StartDate?>:</td><td><input type=text name=startdate size=10 value='<?=date('Y-m-d')?>'>(YYYY-MM-DD)</td></tr>
<tr><td align=right nowrap><?=$i_WebSAMS_Attendance_EndDate?>:</td><td><input type=text name=enddate size=10 value='<?=date('Y-m-d')?>'>(YYYY-MM-DD)</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" onClick="submit2update()"></td></tr>
</table>
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>