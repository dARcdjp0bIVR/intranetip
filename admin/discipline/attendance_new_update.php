<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdiscipline.php");
intranet_opendb();

$ldiscipline = new libdiscipline();

if($type == 0)
{
	$sql = "LOCK TABLES DISCIPLINE_LATE_UPGRADE_RULE, DISCIPLINE_MERIT_ITEM WRITE";
	$ldiscipline->db_db_query($sql);

	$existing_rules = $ldiscipline->getLateUpdateRules();

	$nextCount  = $_POST['nextCount'];
	$MeritType = $_POST['MeritType'];
	$profileNum = $_POST['profileNum'];
	$ItemID = $_POST['ItemID'];
	$ConductScore = $_POST['ConductScore'];

	$reason_name = $ldiscipline->returnItemName($ItemID);
	
	$existing_rules[] = array($nextCount, $MeritType, $profileNum, $ItemID, $reason_name, $ConductScore);

	$values = "";
	$delim = "";
	for ($i=0; $i<sizeof($existing_rules); $i++)
	{
		 list($t_next, $t_type, $t_profileNum, $t_reasonID, $t_reasonName, $t_ConductScore) = $existing_rules[$i];
		 $t_ruleOrder = $i+1;
		 $values .= "$delim('$t_ruleOrder','$t_next','$t_type','$t_profileNum', '$t_reasonID', '$t_reasonName', '$t_ConductScore')";
		 $delim = ",";
	}
	$sql = "DELETE FROM DISCIPLINE_LATE_UPGRADE_RULE";
	$ldiscipline->db_db_query($sql);

	$sql = "INSERT INTO DISCIPLINE_LATE_UPGRADE_RULE (RuleOrder, NextNumLate, ProfileMeritType, ProfileMeritNum, ReasonItemID, Reason, ConductScore)
				   VALUES $values";
	$ldiscipline->db_db_query($sql);

}
else
{
	$sql = "LOCK TABLES DISCIPLINE_LATE_UPGRADE_RULE_DETENTION WRITE";
	$ldiscipline->db_db_query($sql);

	$existing_rules = $ldiscipline->getDetentionUpdateRules();

	$nextCount  = $_POST['nextCount'];
	$detentionMin = $_POST['DetentionMinutes'];
	$reason = $_POST['Reason'];
	
	$existing_rules[] = array($nextCount, $detentionMin, $reason);
	
	$values = "";
	$delim = "";
	for ($i=0; $i<sizeof($existing_rules); $i++)
	{
		 list($t_next, $d_minute, $d_reason) = $existing_rules[$i];

		 $t_ruleOrder = $i+1;
		 $values .= "$delim('$t_ruleOrder','$t_next','$d_minute', '$d_reason')";
		 $delim = ",";
	}
	
	$sql = "DELETE FROM DISCIPLINE_LATE_UPGRADE_RULE_DETENTION";
	$ldiscipline->db_db_query($sql);

	$sql = "INSERT INTO DISCIPLINE_LATE_UPGRADE_RULE_DETENTION (RuleOrder, NextNumLate, DetentionMinutes, Reason)
				  VALUES $values";

	$ldiscipline->db_db_query($sql);

}

$sql = "UNLOCK TABLES";
$ldiscipline->db_db_query($sql);

intranet_closedb();
header("Location: attendance.php?msg=1");

?>