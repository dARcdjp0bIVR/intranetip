<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();


if (!is_array($StaffID) || sizeof($StaffID)==0)
{
     header("Location: userset.php");
     exit();
}
# Insert records
$values = "";
$delim = "";
for ($i=0; $i<sizeof($StaffID); $i++)
{
     $values .= "$delim ('".$StaffID[$i]."','$TargetLevel')";
     $delim = ",";
}
$sql = "INSERT IGNORE INTO DISCIPLINE_USER_ACL (UserID, UserLevel) VALUES $values";
$li->db_db_query($sql);

# Update levels of old records
$list = implode(",",$StaffID);
$sql = "UPDATE DISCIPLINE_USER_ACL SET UserLevel = '$TargetLevel' WHERE UserID IN ($list)";
$li->db_db_query($sql);


intranet_closedb();
header("Location: userset.php?msg=2");

?>