<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");


if ($isEdit==1)
{
    # Get from form input
    $data = $InputLevelName;
}
else  # Get from DB
{
    intranet_opendb();
    $li = new libdb();
    $sql = "SELECT LevelName FROM DISCIPLINE_ACCESS_LEVEL ORDER BY LevelNum";
    $data = $li->returnVector($sql);
    if (sizeof($data)==0)
    {
        $data = array("Normal Teacher","Discipline Teacher","Discipline Master","Principal");
    }
    $num = sizeof($data);

    #$num = 4;
}

?>

<form name="form1" action="levelset_update.php" method="post">
<?= displayNavTitle($i_adminmenu_fs, '', $i_Discipline_System, 'index.php', $i_Discipline_System_Control_LevelSetting,'') ?>
<?= displayTag("head_discipline_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript>
function changeControl(newValue)
{
         if (newValue < <?=$num?>)
         {
             if (confirm('<?=$i_Discipline_System_alert_NumLevelLess?>'))
             {
                 document.form1.action = "";
                 document.form1.method = "GET";
                 document.form1.submit();
                 return true;
             }
             else
             {
                 document.form1.num.selectedIndex = <?=$num-1?>;
                 return false;
             }
         }
         else
         {
                 document.form1.action = "";
                 document.form1.method = "GET";
                 document.form1.submit();
                 return true;
         }
}
</SCRIPT>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<? if ($isEdit==1) { ?>
<font color=red><?=$i_Discipline_System_alert_NeedToSave?></font><br>
<? } ?>
<?=$i_Discipline_System_NumOfLevel?>: <select name=num onChange=changeControl(this.value)>
<? for ($i=1; $i<=10; $i++) { ?>
<OPTION value=<?=$i?> <?=($i==$num?"SELECTED":"")?>><?=$i?></OPTION>
<? } ?>
</select>
<hr width=90% align=center>
<table width=95% border=0 cellspacing=1 cellpadding=1>
<? for ($i=0; $i<$num; $i++) { ?>
<tr>
<td class=tableTitle_new><?=$i+1?></td><td>:</td><td><input type=text name=InputLevelName[] value='<?=$data[$i]?>'></td>
</tr>
<? } ?>
</table>


</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
&nbsp;<a href=index.php><img src="<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border=0></a>
</td>
</tr>
</table>
<input type=hidden name=originalNum value="<?=$num?>">
<input type=hidden name=isEdit value=1>
</form>

<?php
include("../../templates/adminfooter.php");
?>