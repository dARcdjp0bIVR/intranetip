<?php
#############################
# Add next late rule
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdiscipline.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
$attendance_rule_edit = 1;
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

##### Data retrieve
$lsp = new libstudentprofile();
$ldiscipline = new libdiscipline();
$rule = $_GET['rule'];
$type = $_GET['type'];

$accumulated_lates = ($type==0) ? $ldiscipline->getAccumulatedLateRulesCount($rule) : $ldiscipline->getAccumulatedLateRulesCountDetention($rule);

if($type == 0)
{
        $rule_detail = $ldiscipline->getRuleDetail($rule);
        list($r_count, $r_pType, $r_pCount, $reason_id, $reason, $r_conductscore) = $rule_detail;

        $select_next = "<SELECT name=nextCount>\n";
        for ($i=1; $i<=200; $i++)
        {
                $select_next .= "<OPTION value=$i ".($r_count==$i?"SELECTED":"").">$i</OPTION>\n";
        }
        $select_next .= "</SELECT>\n";

        $select_profile_num = "<SELECT name=profileNum>\n";
        for ($i=0; $i<=10; $i++)
        {
                 $select_profile_num .= "<OPTION value=$i ".($r_pCount==$i?"SELECTED":"").">$i</OPTION>\n";
        }
        $select_profile_num .= "</SELECT>\n";

        # Get selection of merit types
        $select_merit_type = "<SELECT name=MeritType>\n";
        if (!$lsp->is_warning_disabled)
        {
                 $select_merit_type .= "<OPTION value=0 ".($r_pType==0?"SELECTED":"").">$i_Merit_Warning</OPTION>\n";
        }
        if (!$lsp->is_black_disabled)
        {
                 $select_merit_type .= "<OPTION value=-1 ".($r_pType==-1?"SELECTED":"").">$i_Merit_BlackMark</OPTION>\n";
        }
        if (!$lsp->is_min_demer_disabled)
        {
                 $select_merit_type .= "<OPTION value=-2 ".($r_pType==-2?"SELECTED":"").">$i_Merit_MinorDemerit</OPTION>\n";
        }
        if (!$lsp->is_maj_demer_disabled)
        {
                 $select_merit_type .= "<OPTION value=-3 ".($r_pType==-3?"SELECTED":"").">$i_Merit_MajorDemerit</OPTION>\n";
        }
        if (!$lsp->is_sup_demer_disabled)
        {
                 $select_merit_type .= "<OPTION value=-4 ".($r_pType==-4?"SELECTED":"").">$i_Merit_SuperDemerit</OPTION>\n";
        }
        if (!$lsp->is_ult_demer_disabled)
        {
                 $select_merit_type .= "<OPTION value=-5 ".($r_pType==-5?"SELECTED":"").">$i_Merit_UltraDemerit</OPTION>\n";
        }
        $select_merit_type .= "</SELECT>\n";

        #Get the Record Type of the Current Item
        $currCat = $ldiscipline->returnCatIDByItemID($reason_id);

        # Get Category selection
        $cats = $ldiscipline->returnMeritItemCategoryByType("-1");
        $select_cats = getSelectByArray($cats,"name=CatID onChange=changeCat()", $currCat);

        # Get item selection in Javascript
        $items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat("-1");

        $gen_i_select = " -- $button_select -- ";

        # Conduct Score selection
        $select_conduct = "<SELECT name=ConductScore>\n";
        for ($i=0; $i<=DISCIPLINE_CONDUCT_ITEM_MAX; $i++)
        {
             $select_conduct .= "<OPTION value='$i' ".($r_conductscore==$i?"SELECTED":"").">$i</OPTION>\n";
        }
        $select_conduct .= "</SELECT>\n";
}
else
{
        $rule_detail = $ldiscipline->getRuleDetailDetention($rule);
        list($r_count, $d_minutes, $reason) = $rule_detail;

        $select_next = "<SELECT name=nextCount>\n";
        for ($i=1; $i<=200; $i++)
        {
                $select_next .= "<OPTION value=$i ".($r_count==$i?"SELECTED":"").">$i</OPTION>\n";
        }
        $select_next .= "</SELECT>\n";

        #Get Selection of Detention Minutes
        $select_detention = "<SELECT name=DetentionMinutes>";
        for ($i=1; $i<=DISCIPLINE_DETENTION_SELECT_COUNT; $i++)
        {
                $value = $i*DISCIPLINE_DETENTION_INTERVAL;
                $select_detention .= "<OPTION value='$value' ".($d_minutes==$value?"SELECTED":"").">$value</OPTION>\n";
        }
        $select_detention .= "</SELECT>\n";

}

?>
<SCRIPT LANGUAGE=Javascript>

function changeCat()
{
         var item_select = document.form1.elements["ItemID"];
         var selectedCatID = document.form1.elements["CatID"].value;
                 var reasonID = document.form1.elements["reasonID"].value;

         while (item_select.options.length > 0)
         {
                item_select.options[0] = null;
         }
         if (selectedCatID == '')
         {
             item_select.options[0] = new Option('<?=$gen_i_select?>',0);

                        <?
                        $curr_cat_id = "";
                        $pos = 1;

                        for ($i=0; $i<sizeof($items); $i++)
                        {
                                 list($r_catID, $r_itemID, $r_itemName) = $items[$i];
                                if ($r_catID != $curr_cat_id)
                                {
                                         $pos = 1;
                        ?>
                                }
                                else if (selectedCatID == "<?=$r_catID?>")
                                {
                                        item_select.options[0] = new Option('<?=$gen_i_select?>',0);
                                 <?
                                        $curr_cat_id = $r_catID;
                                 }
                                ?>
                                var tempPos = "<?=$pos++?>";

                                item_select.options[tempPos] = new Option('<?=$r_itemName?>','<?=$r_itemID?>');
                                //alert(reasonID);
                                        //alert("<?=$r_itemID?>");
                                        //alert(tempPos);
                                if(reasonID == "<?=$r_itemID?>")
                                {
                                        item_select.options[tempPos].selected = true;
                                }
                                <?
                        }
                        ?>
                }
}
function checkform(){
	<?php if($type!=1){?>
	    obj = document.form1;
		objCatID = obj.CatID;
		objItemID= obj.ItemID;
		
        if(objCatID==null || objItemID==null || objCatID.value == 0 || objItemID.value == 0)
        {
                alert('<?=$i_alert_pleaseselect.$i_Discipline_System_general_record?>');
                return false;
        }
   <?php } ?>
        return true;
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Discipline_System, 'index.php',$i_Discipline_System_Control_AttendanceSettings,'attendance.php',$button_edit,'') ?>
<?= displayTag("head_discipline_$intranet_session_language.gif", $msg) ?>
<form name=form1 action=attendance_rule_edit_update.php method=POST onSubmit='return checkform()'>
<BLOCKQUOTE>
<table width=500 border=0 cellspacing=0 cellpadding=4>
<tr><td align=right><?=$i_Discipline_System_LateUpgrade_Accumulated?></td><td><?=$accumulated_lates?></td></tr>
<tr><td align=right><?=$i_Discipline_System_LateUpgrade_Next?></td><td><?=$select_next?></td></tr>

<?php
if($type == 0)
{?>
        <tr><td align=right><?=$i_Discipline_System_Add_Demerit?></td><td><?=$select_profile_num?> <?=$select_merit_type?></td></tr>
        <tr><td align=right><?=$i_Discipline_System_general_record?>:</td><td><?=$select_cats?> - <SELECT name=ItemID><OPTION value=0><?=$gen_i_select?></OPTION></SELECT></td></tr>
        <tr><td align=right><?=$i_Discipline_System_ConductScore. " (".$i_Discipline_System_general_decrement.")"?>:</td><td><?=$select_conduct?></td>
<?php
}
else
{?>
        <tr><td align=right><?=$i_Discipline_System_LateUpgrade_Detention_Minutes?></td><td><?=$select_detention?></td></tr>
        <tr><td align=right><?=$i_SmartCard_DetentionReason?>:</td><td><textarea name=Reason rows=3 cols=40><?=$reason?></textarea></td></tr>
<?php
}?>
</table>
</BLOCKQUOTE>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=rule value="<?=$rule?>">
<input type=hidden name=type value="<?=$type?>">
<input type=hidden name=reasonID value="<?=$reason_id?>">
</form>
<?php
include_once("../../templates/adminfooter.php");
?>
