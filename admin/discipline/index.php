<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
if($plugin['Discipline'])
	include_once("../../includes/libdiscipline.php");
if($plugin['Disciplinev12'])
	include_once("../../includes/libdisciplinev12.php");
	
intranet_opendb();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Discipline_System, '') ?>
<?= displayTag("head_discipline_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>

<!--
<?= displayOption($i_Discipline_System_Control_LevelSetting, 'levelset.php',1,
                  $i_Discipline_System_Control_UserACL, 'userset.php',1,
                  $i_Discipline_System_Control_ItemLevel, 'functionset.php',1,
                  $i_Discipline_System_Control_AttendanceSettings, 'attendance.php',$plugin['attendancestudent']
                                ) ?>
-->
<?php	
	$li = new libdb();
	
	if($plugin['Discipline']){
	$step1_pass = false;
	$step2_pass = false;
	$step3_pass = false;
	
	// Step 1 : Check record counts in DISCIPLINE_ACCESS_LEVEL
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCESS_LEVEL";
	$result = $li->returnVector($sql);
	if (sizeof($result) > 0 && $result[0] >= 1)
	{
		$step1_pass = true;
	}
	
	// Step 2 : Check permissions in DISCIPLINE_FUNCTION_SETTING are set
	if ($step1_pass)
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_FUNCTION_SETTING";
		$result = $li->returnVector($sql);		
		if (sizeof($result) > 0 && $result[0] >= 1)
		{
			$step2_pass = true;
		}		
	}
	
	// Step 3 : Check record counts in DISCIPLINE_USER_ACL
	if ($step1_pass && $step2_pass)
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_USER_ACL";
		$result = $li->returnVector($sql);
		if (sizeof($result) > 0 && $result[0] >= 1)
		{
			$step3_pass = true;
		}	
	}
	
	if (!$step1_pass)
	{
		$warning_message = $i_Discipline_System_Control_Warning_LevelSetting;
	}
	elseif (!$step2_pass)
	{
		$warning_message = $i_Discipline_System_Control_Warning_ItemLevel;
	}
	elseif (!$step3_pass)
	{
		$warning_message = $i_Discipline_System_Control_Warning_UserACL;
	}
	
	
		$ldiscipline = new libdiscipline();
		$target_date = date('Y-m-d');
		 $use_accumulative_setting = $ldiscipline->isUseAccumulativeLateSetting($target_date);
	}
?>                                
<?php if (trim($warning_message) != "") { ?>
<div><?=$warning_message.'<br><br>'?></div>
<?php } ?>
<?php
	if($plugin['Disciplinev12'])
	{
		echo displayOption($eDiscipline_System_Admin_User_Setting, 'admin_setting.php',1);
	}
	else
	{
		echo displayOption($i_Discipline_System_Control_LevelSetting, 'levelset.php',1);
		if ($step1_pass)
		{		
			echo displayOption($i_Discipline_System_Control_ItemLevel, 'functionset.php',1);		
		}
		if ($step2_pass)
		{
			echo displayOption($i_Discipline_System_Control_UserACL, 'userset.php',1);
		}
		if ($step3_pass && !$use_accumulative_setting)
		{
			echo displayOption($i_Discipline_System_Control_AttendanceSettings, 'attendance.php',$plugin['attendancestudent']);
		}
	}	
	
?>	
<!--<?=$display?>-->

</blockquote>
</td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>