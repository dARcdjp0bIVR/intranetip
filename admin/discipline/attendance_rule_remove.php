<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdiscipline.php");
intranet_opendb();

$ldiscipline = new libdiscipline();

if($type == 0)
{
	$sql = "LOCK TABLES DISCIPLINE_LATE_UPGRADE_RULE WRITE";
	$ldiscipline->db_db_query($sql);

	# Get the last rule
	$sql = "SELECT MAX(RuleOrder) FROM DISCIPLINE_LATE_UPGRADE_RULE";
	$temp = $ldiscipline->returnVector($sql);
	$last_order = $temp[0];
	if ($last_order > 0)

	$sql = "DELETE FROM DISCIPLINE_LATE_UPGRADE_RULE
				   WHERE RuleOrder = '$last_order'";
	$ldiscipline->db_db_query($sql);

}
else
{
	$sql = "LOCK TABLES DISCIPLINE_LATE_UPGRADE_RULE_DETENTION WRITE";
	$ldiscipline->db_db_query($sql);

	# Get the last rule
	$sql = "SELECT MAX(RuleOrder) FROM DISCIPLINE_LATE_UPGRADE_RULE_DETENTION";
	$temp = $ldiscipline->returnVector($sql);
	$last_order = $temp[0];
	if ($last_order > 0)

	$sql = "DELETE FROM DISCIPLINE_LATE_UPGRADE_RULE_DETENTION
				   WHERE RuleOrder = '$last_order'";
	$ldiscipline->db_db_query($sql);

}
$sql = "UNLOCK TABLES";
$ldiscipline->db_db_query($sql);

intranet_closedb();
header("Location: attendance.php?msg=3");

?>