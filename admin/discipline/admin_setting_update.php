<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

$lf = new libfilesystem();
if (is_array($target) && sizeof($target)>0)
{
	$target = str_replace("&#160;", "", $target);
	
	$target = array_remove_empty($target);
	$file_content = implode(",", $target);
}

// remove empty and non-numeric entries from the array so that no space will be written in the text file
$newTarget = array();
for($i=0; $i<sizeof($target); $i++) {
	if(trim($target[$i]) != "" && is_numeric($target[$i])) {
		$newTarget[] = $target[$i];
	}
}
$target = $newTarget;

$file_content = str_replace("&#160;", "", $file_content);
$file_content = trim($file_content, ",");

# Write to file
$li = new libfilesystem();

$location = $intranet_root."/file/disciplinev12";
$li->folder_new($location);
$file = $location."/admin_user.txt";

$success = $li->file_write($file_content, $file);

intranet_closedb();
header("Location: admin_setting.php?msg=2");
?>