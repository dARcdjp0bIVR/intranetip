<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdiscipline.php");
intranet_opendb();

$ldiscipline = new libdiscipline();


$nextCount  = $_POST['nextCount'];
$MeritType = $_POST['MeritType'];
$profileNum = $_POST['profileNum'];
$rule = $_POST['rule'];
$DetentionMinutes = $_POST['DetentionMinutes'];
$type = $_POST['type'];
$ItemID = $_POST['ItemID'];
$reason = $_POST['Reason'];
$ConductScore = $_POST['ConductScore'];

if($type == 0)
{
	$reason_name = $ldiscipline->returnItemName($ItemID);

	$sql = "UPDATE DISCIPLINE_LATE_UPGRADE_RULE SET
				   NextNumLate = '$nextCount', ProfileMeritType = '$MeritType',
				   ProfileMeritNum = '$profileNum', ReasonItemID = '$ItemID', Reason = '$reason_name',
				   ConductScore = '$ConductScore'
				   WHERE RuleOrder = '$rule'";
	$ldiscipline->db_db_query($sql);
}
else
{
	$sql = "UPDATE DISCIPLINE_LATE_UPGRADE_RULE_DETENTION SET
				   NextNumLate = '$nextCount', DetentionMinutes = '$DetentionMinutes', Reason = '$reason'
				   WHERE RuleOrder = '$rule'";
	$ldiscipline->db_db_query($sql);
}

intranet_closedb();
header("Location: attendance.php?msg=2");

?>