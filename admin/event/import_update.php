<?php

// Modifing by 

include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
include("../../includes/libcycle.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();
$lc = new libcycle();
#$is_cycle = ($lc->CycleID >= 1);
$is_cycle = true;
if ($EventSkip != 1)
    $EventSkip = 0;
# uploaded file information
//$filepath = $HTTP_POST_FILES['userfile']['tmp_name'];
//$filename = $HTTP_POST_FILES['userfile']['name'];
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: import.php?filter=$RecordType");
} else {
        /*
		$ext = strtoupper($lo->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lo->file_read_csv($filepath);
                array_shift($data);                   # drop the title bar
        }
        */
        
        $data = $limport->GET_IMPORT_TXT($filepath);
		array_shift($data);		# drop the title bar
        $values = "";

        for ($i=0; $i<sizeof($data); $i++)
        {
                list($EventDate,$Title,$EventVenue,$EventNature,$Description) = $data[$i];
                $Title = addslashes($Title);
                $Description = addslashes ($Description);
                $EventVenue = addslashes ($EventVenue);
                $EventNature = addslashes ($EventNature);

                # pack insert values
                if ($i!=0) $values .= ",";
                $values .= "('$Title','$Description','$EventDate','$EventVenue','$EventNature',$RecordType,$RecordStatus, now(), now()";

                if ($is_cycle) $values .= ",$EventSkip)";
                else $values .= ")";
        }
        $fields = "Title";
        $fields .= ",Description";
        $fields .= ",EventDate";
        $fields .= ",EventVenue";
        $fields .= ",EventNature";
        $fields .= ",RecordType";
        $fields .= ",RecordStatus";
        $fields .= ",DateInput ,DateModified";

        if ($is_cycle) $fields .= ",isSkipCycle";

        $sql = "INSERT INTO INTRANET_EVENT ( $fields ) VALUES $values";
        $num = $li->db_db_query($sql);
        $EventID = $li->db_insert_id();
        
        if($RecordType == 3 && $num)
        {
     		for($i=0; $i<sizeof($GroupID); $i++)
     		{
         		$sql = "INSERT INTO INTRANET_GROUPEVENT (GroupID, EventID) VALUES (".$GroupID[$i].", $EventID)";
          		$li->db_db_query($sql);
     		}
		}
        
        intranet_closedb();
        header("Location: event.php?TabID=$RecordType&msg=1&num=$num");
}
?>