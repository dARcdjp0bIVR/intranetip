<?php

//kelvin
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($TabID){
     case 0: $TabID = 0; break;
     case 1: $TabID = 1; break;
     case 2: $TabID = 2; break;
     case 3: $TabID = 3; break;
     default: $TabID = 0; break;
}
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;
$sql  = "SELECT
               DATE_FORMAT(EventDate, '%Y-%m-%d'),
               CONCAT('<a class=tableContentLink href=\"edit.php?EventID[]=', EventID, '\">', Title, '</a>'),
               EventVenue,
               EventNature,
               DateModified,
               CONCAT('<input type=checkbox name=EventID[] value=', EventID ,'>')
          FROM
               INTRANET_EVENT
          WHERE
               (Title like '%$keyword%' OR EventDate like '%$keyword%' OR EventNature like '%$keyword%' OR EventVenue like '%$keyword%') AND
               RecordStatus = $filter AND
               RecordType = $TabID
          ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("EventDate", "Title", "EventVenue", "EventNature", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_event;
$li->column_array = array(0,0,0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = "adminMgtSchoolCalendarEvents";

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_EventDate)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $i_EventTitle)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(2, $i_EventVenue)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_EventNature)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(4, $i_EventDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("EventID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'new.php')\">".newIcon()."$button_new</a>\n";
$toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1,'import.php')\">".importIcon()."$button_import</a>";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'EventID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'EventID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_pending</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_publish</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_im_event, 'index.php', $i_EventTypeString[$TabID], '') ?>
<?php
switch ($TabID)
{
        case 0: $imgTitle = "head_event_school_$intranet_session_language.gif"; break;
        case 1: $imgTitle = "head_event_teaching_$intranet_session_language.gif"; break;
        case 2: $imgTitle = "head_event_holiday_$intranet_session_language.gif"; break;
        case 3: $imgTitle = "head_event_group_$intranet_session_language.gif"; break;
}
echo displayTag($imgTitle, $msg);
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=TabID value="<?php echo $TabID; ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>