<?php

// Modifing by 

include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
include("../../includes/libcycle.php");
include("../../includes/libaccount.php");
include("../../includes/libgrouping.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();
$linterface = new interface_html();
$limport = new libimporttext();
$lo = new libgrouping();

$lc = new libcycle();
#$is_cycle = ($lc->CycleID >= 1);
$is_cycle = true;

$filter = ($filter == 0) ? 1 : $filter;
switch ($TabID){
     case 0: $TabID = 0; $checked[0] = "checked"; break;
     case 1: $TabID = 1; $checked[1] = "checked"; break;
     case 2: $TabID = 2; $checked[2] = "checked"; break;
     case 3: $TabID = 3; $checked[3] = "checked"; break;
     default: $TabID = 0; $checked[0] = "checked"; break;
}

if($TabID == 3)
{
	$group_row_style = "display:block;";
}
else
{
	$group_row_style = "display:none;";
}

?>

<script language="javascript">
var RecordTypeValue = <?php echo $TabID ?>;

function checkform(obj)
{
     if(RecordTypeValue == 3)
     {
     	checkOptionAll(obj.elements["GroupID[]"]);
     	//if(obj.elements["GroupID[]"].length==0){ alert(globalAlertMsg15); return false; }
 	 }
}

function checkRecordType(type)
{
	RecordTypeValue = type;
	if(type == 3)
	{
		document.getElementById("group_row").style.display = "block";
	}
	else
	{
		document.getElementById("group_row").style.display = "none";
	}
}

</script>

<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_im_event, 'index.php', $i_EventTypeString[$TabID], 'javascript:history.back()', $button_import, '') ?>
<?php
switch ($TabID)
{
        case 0: $imgTitle = "head_event_school_$intranet_session_language.gif"; break;
        case 1: $imgTitle = "head_event_teaching_$intranet_session_language.gif"; break;
        case 2: $imgTitle = "head_event_holiday_$intranet_session_language.gif"; break;
        case 3: $imgTitle = "head_event_group_$intranet_session_language.gif"; break;
}
echo displayTag($imgTitle, $msg);
?>


<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right nowrap><?php echo $i_EventImport_msg; ?>:</td><td><input class=file type=file name=userfile size=25><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<tr><td align=right nowrap><?php echo $i_EventImport_type; ?>:</td><td>
<input type="radio" name="RecordType" value="0" onClick="checkRecordType(0);" <?=$checked[0]?>><?=$i_EventTypeSchool?></OPTION><br>
<input type="radio" name="RecordType" value="1" onClick="checkRecordType(1);" <?=$checked[1]?>><?=$i_EventTypeAcademic?></OPTION><br>
<input type="radio" name="RecordType" value="2" onClick="checkRecordType(2);" <?=$checked[2]?>><?=$i_EventTypeHoliday?></OPTION><br>
<input type="radio" name="RecordType" value="3" onClick="checkRecordType(3);" <?=$checked[3]?>><?=$i_EventTypeGroup?></OPTION><br>
</td></tr>

<tr id="group_row" style="<?=$group_row_style?>"><td align="right"><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayEventGroups(); ?></td></tr>

<?php
if ($is_cycle)
{
?>
<tr><td align=right nowrap><?php echo $i_EventSkipCycle; ?>:</td><td><INPUT type=checkbox NAME=EventSkip VALUE=1 <?=$skipChecked?>></td></tr>
<?php
}
?>
<tr><td align=right nowrap><?php echo $i_EventRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 CHECKED> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0> <?php echo $i_status_pending; ?></td></tr>
<tr><td colspan=2><br><span class=extraInfo>
<?=$i_EventImport_notes?>
</span></td></tr>
<tr><td colspan=2>
<a class=functionlink_new href="<?= GET_CSV("eventsample.csv")?>"><?=$i_EventImport_sample?></a>
</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>