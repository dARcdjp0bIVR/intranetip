<?php 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libevent.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libevent($EventID);
?>

<form action=edit.php method=get>
<p class=admin_head><?php echo $i_admintitle_im.displayArrow(); ?><a href=javascript:history.back()><?php echo $i_admintitle_im_event; ?></a><?php echo displayArrow();?><?php echo $li->Title; ?></p>
<blockquote>
<table width=400 border=0 cellpadding=2 cellspacing=1>
<tr><td><?php echo $li->display(); ?></td></tr>
<tr><td><input class=submit type=submit value="<?php echo $button_edit; ?>"></td></tr>
</table>
</blockquote>
<input type=hidden name=EventID[] value="<?php echo $li->EventID; ?>">
</form>

<?php 
intranet_closedb();
include("../../templates/adminfooter.php"); 
?>
