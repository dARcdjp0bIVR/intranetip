<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libservicemgmt.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 1; break;
}

if (isset($targetClass) && $targetClass != "")
{
    $conds = "AND a.ClassName = '$targetClass'";
}
$user_field = getNameFieldByLang("a.");

$sql = "SELECT $user_field, a.ClassName, a.ClassNumber, a.SocialDeptNumber,
               CONCAT('<input type=checkbox name=StudentID[] value=', a.UserID ,'>')
               FROM INTRANET_USER as a WHERE a.RecordType = 2 AND a.RecordStatus != 3 $conds
               ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.EnglishName","a.ClassName","a.ClassNumber","a.SocialDeptNumber");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_Extra_SocialNumber)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";

$lc = new libclass();
$select_classes = $lc->getSelectClass("name=targetClass onChange='this.form.submit()'", $targetClass);

$toolbar .= toolBarSpacer()."<a class=iconLink href=javascript:checkNew('socialnumber_import.php')>".importIcon()."$button_import</a>";

$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'StudentID[]','socialnumber_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$searchbar = $select_classes;
?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_ServiceMgmt_System, 'index.php',$i_Extra_Manage_SocialNumber,'') ?>
<?= displayTag("head_service_mgmt_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../templates/adminfooter.php");
intranet_closedb();
?>
