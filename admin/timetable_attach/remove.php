<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "DELETE FROM INTRANET_GROUPTIMETABLE WHERE TimetableID IN (".implode(",", $TimetableID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_TIMETABLE WHERE TimetableID IN (".implode(",", $TimetableID).")";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=3");
?>
