<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();
$GroupCategoryID = (is_array($GroupCategoryID)? $GroupCategoryID[0]:$GroupCategoryID);
$lg = new libgroupcategory($GroupCategoryID);

$displayOption = "<SELECT name=display>
                   <OPTION value=1>$i_OrganizationPage_NotDisplay</OPTION>
                   <OPTION value=2 SELECTED>$i_OrganizationPage_Unchange</OPTION>
                   <OPTION value=3>$i_OrganizationPage_DisplayAll</OPTION>
                   </SELECT>";



?>



<script language="javascript">
function checkform(obj){
     if(!check_text(obj.CategoryName, "<?php echo $i_alert_pleasefillin.$i_GroupCategoryName; ?>.")) return false;
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_group_settings, '/admin/group_settings/', $i_GroupCategorySettings, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_group_category_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_GroupCategoryName; ?>:</td><td><input class=text type=text name=CategoryName size=30 maxlength=100 value='<?=$lg->CategoryName?>'></td></tr>
<? if ($GroupCategoryID!=0) { ?>
<tr><td align=right><?php echo $i_OrganizationPage_DisplayOption; ?>:</td><td><?=$displayOption?></td></tr>
<? } ?>
</table>
<br><br>
</blockquote>
<input type=hidden name=GroupCategoryID value=<?=$GroupCategoryID?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>