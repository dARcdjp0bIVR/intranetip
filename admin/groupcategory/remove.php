<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$default_cats = array(0,1,2,3,4,5);

# Cannot delete those with groups linking
$sql = "SELECT DISTINCT RecordType FROM INTRANET_GROUP";
$currents = $li->returnVector($sql);
$currents = array_merge($default_cats,$currents);
$exlist = implode(",",$currents);

$list = implode(",",$GroupCategoryID);
# Grab category can be deleted
$sql = "SELECT GroupCategoryID FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID IN ($list) AND GroupCategoryID NOT IN ($exlist)";
$valids = $li->returnVector($sql);

if (sizeof($valids)==sizeof($GroupCategoryID))              # All deleted successfully
{
    $signal = 3;
}
else
{
    $signal = 11;
}
$valid_list = implode(",",$valids);

$sql = "DELETE FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID IN ($valid_list)";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_ROLE WHERE RecordType IN ($valid_list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=$signal");
?>