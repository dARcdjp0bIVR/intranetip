<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$CategoryName = intranet_htmlspecialchars(trim($CategoryName));
$DefaultRole = intranet_htmlspecialchars(trim($DefaultRole));

$fieldname = "CategoryName,DateInput,DateModified";
$fieldvalue = "'$CategoryName',now(),now()";

$sql = "INSERT INTO INTRANET_GROUP_CATEGORY ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);
$catID = $li->db_insert_id();

$fieldname = "Title,Description,RecordType,RecordStatus,DateInput,DateModified";
$fieldvalue = "'$DefaultRole','$DefaultRole','$catID',1,now(),now()";
$sql = "INSERT INTO INTRANET_ROLE ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=1");
?>