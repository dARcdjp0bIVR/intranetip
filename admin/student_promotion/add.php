<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");

include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

intranet_opendb();

$selectClassLevel = new libclass();
$selectClassLevel = $selectClassLevel->getSelectLevel("name=\"selectedClassLevel\" onChange=\"this.form.submit()\"", $selectedClassLevel);

if ($selectedClassLevel<>""){
        $selectClass = new libclass();
        $selectClass = $selectClass->getClassListByLevel( $selectClass->getLevelID($selectedClassLevel) , "name=\"selectedClass\" ", $selectedClass);
}

# show student detail
if (isset($selectedClass)){

                $lsp = new libstudentpromotion();

                if ($selectedClass != "")
                {
                    $selectedClass = $lsp->getClassID($selectedClass);
                    $ClassName = $lsp->getClassName($selectedClass);
                    $conds = " AND a.ClassName = '$ClassName'";
                }

                /*
                switch ($field){
                        case 0: $field = 0; break;
                        case 1: $field = 1; break;
                        case 2: $field = 2; break;
                        case 3: $field = 3; break;
                        case 4: $field = 4; break;
                        case 5: $field = 5; break;
                        default: $field = 0; break;
                }
                $order = ($order == 1) ? 1 : 0;
                */

                $sql = "SELECT
                              a.ChineseName, a.EnglishName,
                              a.Gender,
                               a.ClassName, a.ClassNumber, a.UserLogin,
                              CONCAT('<input type=hidden name=ClassStudentID[] value=',a.UserID,'>','<input type=checkbox name=StudentID[] value=',a.UserID,'>')
                        FROM INTRANET_USER as a LEFT OUTER JOIN  INTRANET_PROMOTION_STATUS as b ON a.UserID = b.UserID
                        WHERE b.UserID IS NULL AND a.RecordType = 2 AND a.RecordStatus = 1 $conds
                        ";

                # TABLE INFO
                /*
                if ($order_selection != 2 && $order_selection != 3)
                {
                    $order_selection = 1;
                }
                if ($order_selection == 1)         # Pure English Name
                {
                    $order = 1;
                    $field = 0;
                }
                else if ($order_selection == 2)      # Boys First, English Name
                {
                     $order = 0;
                     $field = 1;
                }
                else                                   # Girls first, English Name
                {
                    $order = 1;
                    $field = 1;
                }
                */

                $li = new libdbtable($field, $order, $pageNo);
                $li->field_array = array("a.ChineseName","a.EnglishName","a.Gender","a.ClassName","a.ClassNumber","a.UserLogin");
                /*
                if ($order_selection == 2 || $order_selection == 3)
                    $li->field_order2 = ",a.EnglishName ASC";
                    */
                $li->sql = $sql;
                $li->no_col = sizeof($li->field_array) + 2;
                $li->title = "";
                $li->column_array = array(0,0,0,0,0,0);
                $li->wrap_array = array(0,0,0,0,0,0);
                $li->IsColOff = 2;

                // TABLE COLUMN
                $pos = 0;
                $li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
                $li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserChineseName)."</td>\n";
                $li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserEnglishName)."</td>\n";
                $li->column_list .= "<td width=1 class=tableTitle>".$li->column($pos++, $i_UserGender)."</td>\n";
                $li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_StudentPromotion_OldClass)."</td>\n";
                $li->column_list .= "<td width=12% class=tableTitle>".$li->column($pos++, $i_StudentPromotion_OldClassNumber)."</td>\n";
                $li->column_list .= "<td width=18% class=tableTitle>".$li->column($pos++, $i_UserLogin)."</td>\n";
                /*
                $li->column_list .= "<td width=15% class=tableTitle>$i_UserChineseName</td>\n";
                $li->column_list .= "<td width=20% class=tableTitle>$i_UserEnglishName</td>\n";
                $li->column_list .= "<td width=1 class=tableTitle>$i_UserGender</td>\n";
                $li->column_list .= "<td width=15% class=tableTitle>$i_StudentPromotion_OldClass</td>\n";
                $li->column_list .= "<td width=12% class=tableTitle>$i_StudentPromotion_OldClassNumber</td>\n";
                $li->column_list .= "<td width=18% class=tableTitle>$i_UserLogin</td>\n";
                */
                $li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";
}
# end of show student detail

$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<a href=\"javascript:myCheckPost(document.form1,'StudentID[]', 'add_update.php')\"><img src='/images/admin/button/t_btn_save_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>

<script language="JavaScript">

function myCheckPost(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.method="POST";
                obj.submit();
        }
}

</script>

<form name=form1 action='' method=get>
<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" >
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td><?=$i_CampusTV_SelectClip?></td></tr>
</table>
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_bar.gif" width=422 height=16 border=0></td></tr>
<tr><td>
        <table>
                <tr><td><?php echo $selectClassLevel ?></td>
                                <td><?php echo $selectClass ?></td>
                                <? if ( $selectedClassLevel ) {echo "<td>".$searchbar."</td>";} ?>
                </tr>
        </table>
<td></tr>
</table>
<table width=422 border=0 cellpadding=10 cellspacing=0>
<tr><td align=center>
<?php if ($selectedClassLevel && $selectedClass) echo $li->display();
?>
</td>
</tr>
</table>
</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
<input type=hidden name=pos value='<?=$pos?>'>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=ClassID value="<?=$ClassID?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/filefooter.php");
?>
