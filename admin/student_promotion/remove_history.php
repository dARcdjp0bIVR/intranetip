<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/lib.php");
include_once("../../includes/libuser.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();
$lmh = new libmanagehistory();

$strStudentId 	= "";
$classname		= "";
$strRecordId 	= "";
$msg = 3; //Delete successful



if(isset($from)){
	
	switch($from){
		#-------------------------------------
		###   From Year View page
		case '1':  		
			foreach($chk as $key1=>$year){			
				$aryRecordId = $lmh->getRecordIdByYearAndUserID($year, false);
								
				foreach($aryRecordId as $key=>$value)				
					(!empty($strRecordId))? $strRecordId .= ",".$value: $strRecordId=$value;
				
			}
			$referer = "Location:class_history_years.php?msg=";
			break;
			
		#-------------------------------------	
		###   From Class view page
		case '2': 
			foreach($chk as $key1=>$classID){			
				$aryRecordList = $lmh->getRecordIdListByClassId($y, $classID);
				
				if($aryRecordList != array()){
					foreach($aryRecordList as $key=>$value){
						(!empty($strRecordId))? $strRecordId .= ",".$value: $strRecordId=$value;
					}																	
				}
			}			
			$referer = "Location:class_history_class.php?y=$optYear&msg=";
			break;
			
		#-------------------------------------
		###   From Student view page
		case '3':						
			$classname = $c;
			foreach($chk as $key=>$value){				
				(!empty($strRecordId))? $strRecordId .= ",".$value: $strRecordId=$value;
			}
			$referer = "Location:class_history_student.php?y=$optYear&c=$optClass&msg=";
			break;
			
		#-------------------------------------	
		###   Duplicate :: Delete by checked records
		case '4':			
		
			foreach($chkId as $key=>$strRecordID){				
				$aryHistory = $lmh->doGetHistoryByRecordID($strRecordID);				
				$userId   = $lmh->getUserIDByRecordId($strRecordID);
						
				# Get Duplicate Records				
				$strTmpRecordID = $lmh->getDuplicate($userId, $aryHistory['AcademicYear'], $aryHistory['ClassName'], $aryHistory['ClassNumber']);			
				
				if(!empty($strTmpRecordID)){
					(!empty($strRecordId))? $strRecordId .= ", ".$strTmpRecordID:$strRecordId .= $strTmpRecordID;					
				}
			}			
			
				$referer = "Location:handleDuplicate.php?msg=";
			
			break;
			
		#-------------------------------------
		###   Duplicate:: Remove ALL 
		case '5': 		
			
			# Get ALL users with problematic/duplicate Data
			$aryProblemList = $lmh->getDuplicateList();
					
			
			# Accumulate Records that are duplicated for deletion			
			foreach($aryProblemList as $key=>$data){				
				$strTmpRecordID = $lmh->getDuplicate($data['UserID'], $data['AcademicYear'], $data['ClassName'], $data['ClassNumber']);		// Get  ALL records
				
				if(!empty($strTmpRecordID))
					(!empty($strRecordId))? $strRecordId .= ", ".$strTmpRecordID:$strRecordId .= $strTmpRecordID;
			}
			
			$referer = "Location:handleDuplicate.php?msg=";
			
			break;
			
		#-------------------------------------	
		###   Problematic :: Delete by checked records
		case 6:	
		
			foreach($chk as $key=>$strRecordID){
				
				$aryHistory = $lmh->doGetHistoryByRecordID($strRecordID);				
				$userId   = $lmh->getUserIDByRecordId($strRecordID);
						
				# Get Duplicate Records				
				$strTmpRecordID = $lmh->getDuplicate($userId, $aryHistory['AcademicYear']);			
				
				if(!empty($strTmpRecordID))
					(!empty($strRecordId))? $strRecordId .= ", ".$strTmpRecordID:$strRecordId .= $strTmpRecordID;					
				
			}			
			
			$referer = "Location:handleProblematic.php?msg=";
				
			break;
			
		#-------------------------------------	
		###   Problematic :: Delete ALL			
		case 7: 	
		
		echo "here";
			# Get ALL users with problematic/duplicate Data
			$aryProblemList = $lmh->getGenerateProblemList();			 
			
			# Accumulate Records that are duplicated for deletion						
			foreach($aryProblemList as $key=>$data){								
				$strTmpRecordID = $lmh->getDuplicate($data['UserID'], $data['AcademicYear']);
				
				if(!empty($strTmpRecordID))
					(!empty($strRecordId))? $strRecordId .= ", ".$strTmpRecordID : $strRecordId = $strTmpRecordID;														
			}
			
			$referer = "Location:handleProblematic.php?msg=";
						
			break;
			
		default:
			break;
	}
//echo "-->".$strRecordId."<BR />";
	if(!empty($strRecordId))
		$lmh->doRemoveHistoryRecord($strRecordId);
	else
		$msg = 22;
	
}else{
	$msg = 22;
}

intranet_closedb();
header($referer.$msg);
?>