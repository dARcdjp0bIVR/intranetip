<?php
#Modify by : 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();

$lmh = new libmanagehistory();

$aryYear = $lmh->getYearList();

//$aryYearCount = $lmh->getStudentCountByYear($aryYear);

foreach($aryYear as $key=>$value){
	$aryCount = $lmh->getClassListByYear($value);
	$aryYearCount[$key][0] = $value;
	$aryYearCount[$key][1] = $aryCount[1];
}


$x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>
<td class=tableTitle width=300>".$i_StudentPromotion_mgt['ACADEMICYEAR']."</td>
<td class=tableTitle width=260>".$i_StudentPromotion_mgt['NUMSTUDENT']."</td>
<td class=tableTitle><input type='checkbox' name='chkall' onClick=\"Javascipt:(this.checked)?setChecked(1,document.form1,'chk[]'):setChecked(0,document.form1,'chk[]')\" /></td>
</tr>\n";
$total = 0;

for ($i=0; $i<sizeof($aryYear); $i++)
{
	
	$css = ($i%2) ? "2" : "";    
     $x .= "<tr>
     		<td class=tableContent$css style='font:black;'>".$aryYearCount[$i][0]."</td>
     		<td class='tableContent$css '><a class='tableContentLink' href='class_history_class.php?y=".$aryYearCount[$i][0]."'>".$aryYearCount[$i][1]."</a></td>
     		<td class=tableContent$css><input type='checkbox' name='chk[]' id='chk[]' value='".$aryYearCount[$i][0]."' /></td></tr>";
}


$x .= "</table>\n";

?>
<SCRIPT LANGUAGE=Javascript>
	function prepareSubmit(year){
		
		if(confirm('<?=$i_StudentPromotion_mgt['CONFIRMDELETE'][2]?>')){
			location.href ="remove_history.php?from=1";
		}
	}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_mgt['NAV']['YEAR'], '') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name=form1 action=''>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
<div style='float:left;'>&nbsp;&nbsp;<a class=iconLink href="javascript:checkGet(document.form1,'handleDuplicate.php')"><img src='<?=$image_path?>/admin/icon_manage.gif' border='none' align='absmiddle'><?=$i_StudentPromotion_mgt['BTNDUPLICATE']?></a> <img src='<?=$image_path?>/admin/icon_manage.gif' border='none' align='absmiddle'><a class=iconLink href="javascript:checkPost(document.form1,'handleProblematic.php')"><?=$i_StudentPromotion_mgt['BTNPROBLEMDATA']?></a><BR />&nbsp;&nbsp;<a class=iconLink href="javascript:checkGet(document.form1,'import_history.php?y=<?=$y?>&c=<?=$c?>')"><?=importIcon()."$button_import"?></a></div>
<div style='float:right;'><a href='javascript:checkRemove(document.form1, "chk[]", "remove_history.php?from=1");'><img src="<?=$image_path?>/admin/button/t_btn_delete_<?=$intranet_session_language?>.gif" border='none'></a>&nbsp;</div>
</td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?=$x?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>


</form>
<?php
include_once("../../templates/adminfooter.php");
?>