<?php

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();

$recordCount = 1;

$lmh = new libmanagehistory();
	
$aryData = $lmh->getDuplicateList(trim($search));

## Build html 
if(is_array($aryData) && $aryData != array()){
	
	foreach($aryData as $key=>$value){	
	
		$css = ($i%2) ? "2" : "";   
		$x .= "<tr>
				<td class=tableContent$css>$recordCount</td>
				<td class=tableContent$css>".$value[0]."</td>
				<td class=tableContent$css>".$value['AcademicYear']."</td>
				<td class=tableContent$css>".$value['ClassName']."</td>
				<td class=tableContent$css>".$value['ClassNumber']."&nbsp;</td>
				<td class=tableContent$css>".$value['rowCount']."&nbsp;</td>
				<td class=tableContent$css align='right' valign='center'>
				<input type='checkbox' name='chkId[]' value='".$value['RecordID']."' />
				</td>
				</tr>";
		$i++;
		$recordCount++;
	}
}else{
	$x = "<tr><td colspan='6' align='center'><BR />".$i_StudentPromotion_mgt['NORECORD']."<BR /><BR /></td></tr>";
}


		
#####################################################
?>
<script LANGUAGE='Javascript'>
	function prepareSubmit(type){
		switch(type){
			case 'remove':				
				document.getElementById('from').value='4';
				break;		
			case 'removeAll':
				if(confirm('<?=$i_StudentPromotion_mgt['CONFIRMDELETE'][3]?>')){										
					document.getElementById('from').value='5';					
					document.form1.action = 'remove_history.php';
					document.form1.submit();
					
				}
				break;
		}
	}
	
	function getPaging(obj, url, action){
		
		var page = parseInt(document.getElementById('page').value);
		
		if(action == 'previous'){
			page = parseInt(document.getElementById('page').value)-1;
		}else if(action == 'next'){
			page = parseInt(document.getElementById('page').value)+1;
		}
		
		location.href=url+".php?page="+page+"&display="+document.getElementById('display').value+"&search="+document.getElementById('search').value;
	}
	
	function keysearch(){	
		if((document.getElementById('search').value).length > 0)
			document.form1.submit();	
	}
</script>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_mgt['NAV']['YEAR'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['DUPLICATE'],'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name='form1' method='POST'>
		
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td  colspan="6"><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr>
		<td class=admin_bg_menu colspan="" align="left">
			<div style='float:left;'>&nbsp;<input type='text' name='search' id='search' value='<?=$displaySearch?>' size='20' onClick='this.select()' /><a href="javascript:keysearch();"><img src='<?=$image_path?>/admin/button/t_btn_find_<?=$intranet_session_language?>.gif' border='none' align='absmiddle' /></a></div>
			
			<div style='float:right;'>			
			<span style='cursor:pointer' onClick='javascript:prepareSubmit("remove");checkRemove(document.form1, "chkId[]", "remove_history.php");'><img src="<?=$image_path?>/admin/button/t_btn_delete_<?=$intranet_session_language?>.gif" align='absmiddle'></span>
			<a href='javascript:prepareSubmit("removeAll");'><img src="<?=$image_path?>/admin/button/t_btn_delete_all_<?=$intranet_session_language?>.gif" border="none" align='absmiddle'></a>&nbsp;
			</div>
		</td>
	</tr>
	<tr><td colspan="6"><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
	
	<tr><td>
		<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align="center">
		<tr>
			<td class=tableTitle width="40">#</td>
			<td class=tableTitle width="230"><?=$i_StudentPromotion_mgt['STUDENT']?></td>
			<td class=tableTitle width="110"><?=$i_StudentPromotion_mgt['ACADEMICYEAR']?></td>
			<td class=tableTitle width="60"><?=$i_StudentPromotion_mgt['CLASS']?></td>
			<td class=tableTitle width="50"><?=$i_StudentPromotion_mgt['CLASSNUM']?></td>
			<td class=tableTitle width="50"><?=$i_StudentPromotion_mgt['DUPLICATE']?></td>
			<td class=tableTitle width='20' align="right"><input type="checkbox" name="chkall" onClick="Javascipt:(this.checked)?setChecked(1,document.form1,'chkId[]'):setChecked(0,document.form1,'chkId[]')" /></td>	
		</tr>
		<?=$x?>
		</table>
	</td>
	</tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
		<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>
	
	
	<br /><br />
	
	<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>" />
	<input type=hidden name=order value="<?php echo $li->order; ?>" />
	<input type=hidden name=field value="<?php echo $li->field; ?>" />
	<input type=hidden name=page_size_change value="" />
	<input type=hidden name=numPerPage value="<?=$li->page_size?>" />
	<input type="hidden" name="from" id="from" value="duplicate" />	
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>