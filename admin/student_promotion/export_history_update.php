<?php
## Modify By : Sandy
$PATH_WRT_ROOT = "../../";

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbdump.php");
include_once("../../includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();
$li = new libdb();

$col_count = 5;
$cond = "";

$name_field = getNameFieldByLang();
//debug_r($_GET);
$nxtYr = $optYear+1;
if(!isset($optClass)){
	$cond = "h.AcademicYear in ('$optYear', '$optYear-$nxtYr') ORDER BY h.ClassName, u.EnglishName ";
}else{//student
	$cond = "h.AcademicYear in ('$optYear', '$optYear-$nxtYr') AND h.ClassName = '$optClass' ORDER BY h.ClassName, u.EnglishName ";
}

/*$sql = 	"SELECT distinct u.UserLogin, $name_field, h.AcademicYear, h.ClassName, h.ClassNumber ".
		"FROM INTRANET_USER as u INNER JOIN PROFILE_CLASS_HISTORY as h ON u.UserID = h.UserID WHERE  ".
		"u.RecordType = 2 AND u.RecordStatus IN (0,1,2) AND $cond ";
		*/
$sql = 	"SELECT  u.UserLogin, $name_field, h.AcademicYear, h.ClassName, h.ClassNumber ".
	"FROM INTRANET_USER as u INNER JOIN PROFILE_CLASS_HISTORY as h ON u.UserID = h.UserID WHERE  ".
	"u.RecordType = 2 AND u.RecordStatus IN (0,1,2) AND $cond ";


$row = $li->returnArray($sql,$col_count);		

$x = "UserLogin,Student,AcademicYear,ClassName,ClassNumber";
$exportColumn = array("UserLogin", "Student", "AcademicYear", "ClassName", "ClassNumber");		

$x .= "\n";

$aryYearExistForUser = array();

for($i=0; $i<sizeof($row); $i++){
	$delim = "";
	$row_length = $col_count;
	
	//$aryTempYear = explode("-", $row[$i]['AcademicYear']);	
	//(is_array($foundYear[$row[$i]['UserLogin']]))? $aryYearExistForUser = $foundYear[$row[$i]['UserLogin']]: $aryYearExistForUser= array();
		
	
	//if(!in_array( $aryTempYear[0], $aryYearExistForUser )){
//		array_push($aryYearExistForUser, $aryTempYear[0]);
//		$foundYear[$row[$i]['UserLogin']] = $aryYearExistForUser;			
	
		for($j=0; $j<$row_length; $j++){	
			
			$utf_row[] = intranet_undo_htmlspecialchars($row[$i][$j]);
			$row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
			$x .= $delim.$row[$i][$j];		             
			$delim = ",";
			
		}
//	}
	
	//if(isset($utf_row) && !empty($utf_row)){
		$utf_rows[] = $utf_row;
		unset($utf_row);
		$x .="\n";         
	//}
}


//debug_r($utf_rows);

if ($g_encoding_unicode){     
	$export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);	
}else{
	$export_content = $x;
}	

$filename = "eclass-user.csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();


?>