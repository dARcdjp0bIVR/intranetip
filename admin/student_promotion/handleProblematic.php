<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();

$aryTemp = array();
$aryDuplicate = array();
$aryExcludeList = array();

(isset($ck_page_size) && $ck_page_size != "")? $page_size = $ck_page_size :'';
(!isset($pageNo))? $pageNo = 1 : "";


$pageSizeChangeEnabled = true;

$lmh = new libmanagehistory();

$li = new libdbtable(1, 2, $pageNo);

if($intranet_session_language == "en")
	$li->field_array = array("h.UserID", "EnglishName", "h.AcademicYear", "COUNT(*)",'');
else
	$li->field_array = array("h.UserID", "ChineseName",  "h.AcademicYear","COUNT(*)", '');
	
(isset($search))? $search = addslashes(trim($search)): $search = "";
		
	
$li->sql = $lmh->getProblemData($search);

$li->no_col = sizeof($li->field_array);
$li->title = $i_StudentPromotion_mgt['RECORD'];
$li->column_array = array(0,0,0,0);
$li->IsColOff = 2;

# TABLE COLUMN
$li->column_list .= "<td width='40' class=tableTitle>#</td>\n";
$li->column_list .= "<td width='290' class=tableTitle>".$li->column(0, $i_StudentPromotion_mgt['STUDENT'])."</td>\n";
$li->column_list .= "<td width='180' class=tableTitle>".$li->column(1, $i_StudentPromotion_mgt['ACADEMICYEAR'])."</td>\n";
$li->column_list .= "<td width='30' class=tableTitle>".$li->column(1, $i_StudentPromotion_mgt['OCCURRENCE'])."</td>\n";
$li->column_list .= "<td width='20' class=tableTitle>".$li->check("chk[]")."</td>\n";

# Function Bar
$searchbar = "<div style='float:left;'>&nbsp;<input type='text' name='search' id='search' value='".stripslashes(stripslashes(trim($search)))."' size='20' onClick='this.select()' /><a href='javascript:keysearch();'><img src='$image_path/admin/button/t_btn_find_$intranet_session_language.gif' border='none' align='absmiddle' /></a></div>";

$functionbar = "
		<div style='float:right;'>
			<a href='Javascript:checkEdit(document.form1,\"chk[]\",\"edit_history.php\")'><img src='$image_path/admin/button/t_btn_edit_$intranet_session_language.gif' border='none' align='absmiddle'></a>
			<a href='Javascript:prepareSubmit(\"remove\");checkRemove(document.form1, \"chk[]\", \"remove_history.php\");'><img src=\"$image_path/admin/button/t_btn_delete_$intranet_session_language.gif\" border='none' align='absmiddle'></a> &nbsp;
			<a href='javascript:prepareSubmit(\"removeAll\");'><img src=\"$image_path/admin/button/t_btn_delete_all_$intranet_session_language.gif\" border=\"none\" align='absmiddle'></a>&nbsp;
		</div>";
		
#####################################################
?>
<script LANGUAGE='Javascript'>
	function prepareSubmit(type){
		switch(type){
			case 'remove':				
				document.getElementById('from').value='6';
				break;		
			case 'removeAll':
				if(confirm('<?=$i_StudentPromotion_mgt['CONFIRMDELETE'][3]?>')){										
					document.getElementById('from').value='7';					
					document.form1.action = 'remove_history.php';
					document.form1.submit();
					
				}
				break;
		}
	}
	
	function getPaging(obj, url, action){
		
		var page = parseInt(document.getElementById('page').value);
		
		if(action == 'previous'){
			page = parseInt(document.getElementById('page').value)-1;
		}else if(action == 'next'){
			page = parseInt(document.getElementById('page').value)+1;
		}
		
		location.href=url+".php?page="+page+"&display="+document.getElementById('display').value+"&search="+document.getElementById('search').value;
	}
	
	function keysearch(){	
		if((document.getElementById('search').value).length > 0)
			document.form1.submit();	
	}
</script>
 

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_mgt['NAV']['YEAR'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['PROBLEM'],'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name='form1' method='POST'>
	
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
		<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
		<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
		<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar2, $functionbar); ?></td></tr>
		<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
	</table>
	
	<?php echo $li->display(); ?>
	
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
		<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>
	
	<br /><br />
	
	<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>" />
	<input type=hidden name=order value="<?php echo $li->order; ?>" />
	<input type=hidden name=field value="<?php echo $li->field; ?>" />
	<input type=hidden name=page_size_change value="" />
	<input type=hidden name=numPerPage value="<?=$li->page_size?>" />	
	<input type="hidden" name="from" id="from" value="problem" />	
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>