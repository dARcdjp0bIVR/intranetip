<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

intranet_opendb();

$lsp = new libstudentpromotion();
$studentStatus = $lsp->getStudentStatusByClassName($ClassName);
$classes = $lsp->getClassList();
$select_class_overall = getSelectByArray($classes,"name=overallclass onChange=\"changeAll(this)\"");

$x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>
<td class=tableTitle width=15%>$i_UserChineseName</td>
<td class=tableTitle width=20%>$i_UserEnglishName</td>
<td class=tableTitle width=15%>$i_StudentPromotion_OldClass</td>
<td class=tableTitle width=15%>$i_StudentPromotion_OldClassNumber</td>
<td class=tableTitle width=20%>$select_class_overall</td>
<td class=tableTitle width=15%>$i_StudentPromotion_NewClassNumber</td>
</tr>\n";
$total = 0;
for ($i=0; $i<sizeof($studentStatus); $i++)
{
     list ($studentid, $chiName, $engName, $classnum, $newClass, $newnum) = $studentStatus[$i];
     $css = ($i%2) ? "2" : "";
     $x .= "<tr class=tableContent$css>\n";
     $x .= "<td>$chiName</td><td>$engName</td><td>$ClassName</td><td>$classnum</td>\n";
     $class_select = getSelectByArray($classes,"name=targetClass$studentid",$newClass);
     $x .= "<td>$class_select <input type=hidden name=StudentID[] value='$studentid'></td>\n";
     $x .= "<td>$newnum&nbsp;</td>";
     $x .= "</tr>\n";
}
$x .= "</table>\n";
$functionbar .= "<a href=\"student2.php\"><img alt='$button_back' src=\"$image_path/admin/button/t_btn_back_$intranet_session_language.gif\" border=0></a>&nbsp;";
$functionbar .= "<a href='javascript:document.form1.reset()'><img src='$image_path/admin/button/t_btn_reset_$intranet_session_language.gif' border='0'></a>";
$functionbar .= "&nbsp;<a href=\"javascript:checkPost(document.form1,'class2_save.php')\"><img alt='$button_save' src='/images/admin/button/t_btn_save_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
/*
$toolbar = "";
$toolbar = "<a href=\"javascript:clear2Next()\"><img src=\"$image_path/admin/button/s_btn_proceed2next_$intranet_session_language.gif\" border=0 alt=\"$i_ClubsEnrollment_Proceed2NextRound\"></a>";
if ($lc->mode==2)
{
    $toolbar .= " <a href=\"javascript:goLottery()\"><img src=\"$image_path/admin/button/s_btn_draw_$intranet_session_language.gif\" border=\"0\" alt=\"$i_ClubsEnrollment_GoLottery\"></a>";
    $toolbar .= " <a href=\"javascript:confirmFinal()\"><img src=\"$image_path/admin/button/s_btn_confirm_finalized_$intranet_session_language.gif\" border=\"0\" alt=\"$i_ClubsEnrollment_FinalConfirm\"></a>";
}
    $toolbar .= "\n".toolBarSpacer()."\n";

<a class=iconLink href="javascript:newWindow('allgroup.php',1)"><?=printIcon().$i_ClubsEnrollment_AllMemberList?>
*/
?>
<SCRIPT LANGUAGE=Javascript>
function changeAll(obj)
{
         formObj = obj.form
         len = formObj.elements.length;
         var i=0;
         for( i=0 ; i<len ; i++)
         {
              str = formObj.elements[i].name;
              if (str.substring(0,11) == "targetClass")
                  formObj.elements[i].selectedIndex = obj.selectedIndex;
         }

}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ViewEditList, 'student2.php',$ClassName,'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name=form1 action='class2_save.php' method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu align=right><?=$functionbar?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?=$x?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<input type=hidden name=ClassName value="<?=$ClassName?>">
</form>

<?php
include_once("../../templates/adminfooter.php");
?>