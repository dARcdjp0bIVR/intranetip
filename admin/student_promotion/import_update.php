<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

#include_once("../../includes/libclass.php");
#include_once("../../includes/libstudentpromotion.php");
intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if ($format == 1)
    $format_array = array("OldClass","OldClassNumber","NewClass","NewClassNumber");
else
    $format_array = array("UserLogin","NewClass","NewClassNumber");

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: import.php");
} else {
	/*
        $ext = strtoupper($lo->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lo->file_read_csv($filepath);
                $header_row = array_shift($data);                   # drop the title bar
        }
*/
		$data = $limport->GET_IMPORT_TXT($filepath);
		$header_row = array_shift($data);		# drop the title bar

        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($header_row[$i] != $format_array[$i])
             {
                 $format_wrong = true;
                 #header("Location: import.php?msg=13");
                 #exit();
             }
        }
        if ($format_wrong)
        {
            $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
            $display .= "<table width=100 border=1>\n";
            for ($i=0; $i<sizeof($format_array); $i++)
            {
                 $display .= "<tr><td>".$format_array[$i]."</td></tr>\n";
            }
            $display .= "</table></blockquote>\n";
        }
        else
        {

            $sql = "LOCK TABLES
                         INTRANET_USER READ
                         ,INTRANET_PROMOTION_STATUS WRITE
                         ,INTRANET_CLASS READ
                         ";
            $li->db_db_query($sql);

            # Get Class List
            $sql = "SELECT ClassName, ClassID FROM INTRANET_CLASS WHERE RecordStatus=1";
            $temp = $li->returnArray($sql,2);
            $classes = build_assoc_array($temp);

            if ($format == 1)           # Use Class and Class number
            {
                # Get Existing Students
                $sql = "SELECT ClassName, ClassNumber, UserID
                               FROM INTRANET_USER
                        WHERE RecordType = 2 AND RecordStatus IN (0,1,2)
                              AND ClassName != '' AND ClassNumber != ''
                              ORDER BY ClassName, ClassNumber";
                $result = $li->returnArray($sql,3);
                for ($i=0; $i<sizeof($result); $i++)
                {
                     list($class,$classnum,$uid) = $result[$i];
                     $students[$class][$classnum] = $uid;
                }
                # Get student ID already set
                $sql = "SELECT UserID FROM INTRANET_PROMOTION_STATUS ORDER BY UserID";
                $students_set = $li->returnVector($sql);
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($oldClass,$oldNum,$newClass,$newNum) = $data[$i];
                     if ($oldClass != "" && $oldNum != "" && $newClass != "")
                     {
                         $targetUID = $students[$oldClass][$oldNum];
                         $classID = $classes[$newClass];
                         if ($targetUID != "" && $classID != "")            # Student and Class Found
                         {
                             if (in_array($targetUID,$students_set))
                             {
                                 $sql = "UPDATE INTRANET_PROMOTION_STATUS SET NewClass = '$classID',NewClassNumber = '$newNum'
                                                WHERE UserID = '$targetUID'";
                             }
                             else
                             {
                                 $sql = "INSERT INTO INTRANET_PROMOTION_STATUS (UserID, NewClass, NewClassNumber)
                                                VALUES ('$targetUID','$classID','$newNum')";
                             }
                             $li->db_db_query($sql);
                             $recordValid[$i] = true;
                         }
                         else
                         {
                             $recordValid[$i] = false;
                             if ($targetUID == "")
                             {
                                 $reason[$i] = "Student not exists ($oldClass, $oldNum)";
                             }
                             else if ($classID == "")
                             {
                                  $reason[$i] = "New Class not exists ($newClass)";
                             }
                         }
                     }
                     else
                     {
                         if ($oldClass == "" && $oldNum == "" && $newClass == "")
                         {
                             break;             # Assume already EOF
                         }
                         else
                         {
                             $recordValid[$i] = false;
                             $reason[$i] = "Some fields missing: ";
                             $delim = "";
                             if ($oldClass != "")
                             {
                                 $reason[$i] .= "Old Class";
                                 $delim = ",";
                             }
                             if ($oldNum != "")
                             {
                                 $reason[$i] .= "$delim Old Class Number";
                                 $delim = ",";
                             }
                             if ($newClass != "")
                             {
                                 $reason[$i] .= "$delim New Class Number";
                                 $delim = ",";
                             }
                         }
                     }
                }
            }
            else                          # Login -> Class
            {
                # Get Existing Students
                $sql = "SELECT UserLogin, UserID
                               FROM INTRANET_USER
                               WHERE RecordType = 2 AND RecordStatus IN (0,1,2) 
                                     AND ClassName != '' AND ClassNumber != ''
                               ORDER BY ClassName, ClassNumber";
                $result = $li->returnArray($sql,2);
                $students = build_assoc_array($result);

                # Get student ID already set
                $sql = "SELECT UserID FROM INTRANET_PROMOTION_STATUS ORDER BY UserID";
                $students_set = $li->returnVector($sql);
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($login,$newClass,$newNum) = $data[$i];
                     if ($login != "" && $newClass != "")
                     {
                         $targetUID = $students[$login];
                         $classID = $classes[$newClass];
                         if ($targetUID != "" && $classID != "")            # Student and Class Found
                         {
                             if (in_array($targetUID,$students_set))
                             {
                                 $sql = "UPDATE INTRANET_PROMOTION_STATUS SET NewClass = '$classID',NewClassNumber = '$newNum'
                                                WHERE UserID = '$targetUID'";
                             }
                             else
                             {
                                 $sql = "INSERT INTO INTRANET_PROMOTION_STATUS (UserID, NewClass, NewClassNumber)
                                                VALUES ('$targetUID','$classID','$newNum')";
                             }
                             $li->db_db_query($sql);
                             $recordValid[$i] = true;
                         }
                         else
                         {
                             $recordValid[$i] = false;
                             if ($targetUID == "")
                             {
                                 $reason[$i] = "Student not exists ($login)";
                             }
                             else if ($classID == "")
                             {
                                  $reason[$i] = "New Class not exists ($newClass)";
                             }
                         }
                     }
                }
            }
            $sql = "UNLOCK TABLES";
            $li->db_db_query($sql);

            # Mark Reason to display
            for ($i=0; $i<sizeof($recordValid); $i++)
            {
                 if (!$recordValid[$i] && $reason[$i]!="")
                 {
                      $display .= "<tr><td>Line ".($i+1)."</td><td>".$reason[$i]."</td></tr>\n";
                 }
            }
            if ($display != "")
            {
                $display = "<table width=90% align=center border=1 cellspacing=0 cellpadding=0>\n$display\n</table>\n";
            }

        }


        if ($display != '')
        {

            include_once("../../templates/adminheader_setting.php");
            echo displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ViewEditList, 'javascript:history.back()',$button_import,'');
            echo displayTag("head_studentpromotion_$intranet_session_language.gif", $msg);
            if (!$format_wrong)
            {
                 $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
                 echo "$i_StudentPromotion_ImportFailed<br>";
            }
            else
            {
                 $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
            }
            echo $display;
            include_once("../../templates/adminfooter.php");

        }
        else
        {
            header("Location: student2.php?msg=1");
        }
}


intranet_closedb();
#header("Location: index.php?msg=");
?>