<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if ($confirmed != 1)
{
include_once("../../templates/adminheader_intranet.php");

$lsp = new libstudentpromotion();
$assigned = $lsp->returnNumOfAssigned();
$total = $lsp->returnNumOfStudents();
$left = $total - $assigned;

?>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_ViewEditList, 'javascript:history.back()',$button_import,'') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="GET" action="finalize.php" enctype="multipart/form-data">
<blockquote>
<?=$i_StudentPromotion_Summary?>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_StudentPromotion_StudentAssigned; ?>:</td><td><?=$assigned?></td></tr>
<tr><td align=right nowrap><?php echo $i_StudentPromotion_Unset; ?>:</td><td><?=$left?></td></tr>
<tr><td align=right nowrap></td><td><?=$i_StudentPromotion_ConfirmFinalize?></td></tr>
</table>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit()?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirmed value=1>
</form>
<?

}
else
{
            $CurrentYear = getCurrentAcademicYear();

            # Perform finalization
                    $finalize = new libstudentpromotion();
                        $finalize->libstudentpromotionfinalize( $CurrentYear );

            # 1. Update Class History (insert data to PROFILE_CLASS_HISTORY)
                                $finalize->updateClassHistory();
            # 2. Archive Class History for unset students (insert data to PROFILE_ARCHIVE_CLASS_HISTORY and remove from PROFILE_CLASS_HISTORY)
                            $finalize->archiveClassHistory();
            # 3. Archive Student profile information for unset students (Refer to /user/remove.php)
                                $finalize->archiveStudentProfile();
            # 4. Update Class Name Class Number (update INTRANET_USER)
                            $finalize->updateStudentInfoPromotion();
            # 5. Archive Groups (Rename group, e.g. 1A -> 1A-2004 , 2004 is Current Year, not academic year)
                               $finalize->archiveGroup();
            # 6. Create new groups (Refer to class table)
                               $finalize->createNewGroups();
            # 7. Build Class-Group relation (execute lclass->fixClassGroupRelation()  )
                                $finalize->buildCGRelation();
            # 8. Add students to new class group (insert user to INTRANET_USERGROUP)
                               $finalize->addStuToNewClassGroup();
            /*
            # 9. Set unset students status to left (update INTRANET_USER set RecordStatus = 3)
                               $finalize->setUnsetStuToLeft();
        */

            # ^^^After modification^^^
            # Extra step - 10. For unset student, move records from INTRANET_USER to archived table
            #                                                                Then remove records from INTRANET_USER
                                $finalize->archiveIntranetUser();

            # Synchronize eClass DB
            $sql = "SELECT UserEmail, Title, EnglishName, ChineseName, FirstName, LastName, UserPassword, ClassName, ClassNumber, UserEmail, Gender FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1";
            $users = $finalize->returnArray($sql,11);

            $leclass = new libeclass();
            for($i=0; $i<sizeof($users); $i++)
            {
                list($email, $title, $englishname,$chiname, $firstname,$lastname,$password,$class_name,$class_number,$new_email,$gender) = $users[$i];
                $query = $leclass->eClassUserUpdateInfoImport($email, $title, $englishname,$chiname, $firstname,$lastname,"",$class_name,$class_number,"",$gender);
            }



               header ("Location: index.php?step=2");
}

include_once("../../templates/adminfooter.php");

intranet_closedb();
?>
