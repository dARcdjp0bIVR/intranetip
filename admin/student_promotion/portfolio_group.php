<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libeclass40.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

include_once("../../templates/adminheader_intranet.php");

$li = new libdb();

# get the iPortfolio Course ID
$sql = "SELECT course_id FROM {$eclass_db}.course WHERE RoomType = '4'";
$row = $li->returnVector($sql);
$course_id = $row[0];
$le = new libeclass($course_id);

# retrieve UserID and Class of iPortfolio
$sql = "SELECT DISTINCT
			ps.CourseUserID, 
			iu.ClassName
		FROM 
			{$eclass_db}.PORTFOLIO_STUDENT as ps,
			INTRANET_USER as iu
		WHERE 
			ps.UserID = iu.UserID
			AND iu.RecordType <> '4'
			AND ps.CourseUserID IS NOT NULL
		";
$Users = $li->returnArray($sql,2);

$userTotal = sizeof($Users);

//$current_year = $le->getAcademicYear();
//USE ENGLISH ACADEMIC YEAR NAME AS DEFAULT
$current_year = getCurrentAcademicYear('en');

echo displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_Menu_iPortfolioGroup, '');
echo displayTag("head_studentpromotion_$intranet_session_language.gif", $msg);

if($confirmed)
{
	# Join to group named by academicYear + Class (Libeclass.php, JoineClassGroup)
	for($i=0; $i<sizeof($Users); $i++)
	{
		list($userid, $classname) = $Users[$i];
		$group_name = $current_year." {$classname}";
		$le->joinEClassGroup($userid, $group_name);
	}
?>
	<blockquote>
	<table width=<?=($table_width+100)?> border=0 cellpadding=3 cellspacing=0 align=center >
	<tr><td align="center" nowrap><?php echo $i_StudentPromotion_Confirm_iPortfolioGroup; ?></td></td></tr>
	</table><br>
	</blockquote>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><hr size=1></td></tr>
	</table>
<?php
}
else
{
	$table_width = ($intranet_session_language=="en"?"300":"200");
?>

<SCRIPT LANGUAGE=Javascript>
function submit2update(){
	if (confirm("<?=$i_StudentPromotion_Alert_iPortfolioGroup?>"))
	{
        document.form1.action='portfolio_group.php';
        document.form1.submit();
	}
    return;
}
</SCRIPT>

<form name="form1" method="GET" action="portfolio_group.php" enctype="multipart/form-data">
<blockquote>
<span><u><strong><?=$i_StudentPromotion_iPortfolioGroup_Summary?></strong></u></span>
<br /><br />
<table width=<?=($table_width+100)?> border=1 cellpadding=3 cellspacing=0 align=center >
<tr><td align=right nowrap><?php echo $i_StudentPromotion_iPortfolioGroup_UserNum; ?>:</td><td nowrap><?=$userTotal?></td></tr>
<tr><td align=right nowrap><?php echo $i_StudentPromotion_Current_AcademicYear; ?>:</td><td nowrap><?=$current_year?></td></tr>
</table>
<br><?=$Lang['AdminConsole']['i_StudentPromotion_iPortfolioRemind']?><br>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a href="javascript:submit2update()"><image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0></a>
&nbsp;<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirmed value=1>
</form>

<?php
}
intranet_closedb();
?>
