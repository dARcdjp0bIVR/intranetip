<?php
#Modify by : 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();

$lmh = new libmanagehistory();

$aryYear = $lmh->getYearList();
$aryClassList = $lmh->getClassListByYear($y,true);

$displayOption .= $i_StudentPromotion_mgt['ACADEMICYEAR'].": <select name='optYear' id='optYear' >";
foreach($aryYear as $key=>$value){
	$displayOption .= "<option value='$value'";
	if(isset($y) && $y == $value)
		$displayOption .= " SELECTED";
	$displayOption .= ">$value</option>";
}
$displayOption.= "</select>";
$displayOption.= "&nbsp;<input type='button' name='search' value='".$i_StudentPromotion_mgt['BTNSEARCH']."' onClick='prepareSubmit(\"search\", \"\");'";



$x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>
<td class=tableTitle width=300>".$i_StudentPromotion_mgt['CLASS']."</td>
<td class=tableTitle width=260>".$i_StudentPromotion_mgt['NUMSTUDENT']."</td>
<td class=tableTitle><input type='checkbox' name='chkall' onClick=\"Javascipt:(this.checked)?setChecked(1,document.form1,'chk[]'):setChecked(0,document.form1,'chk[]')\" /></td>
</tr>\n";

for ($i=0; $i<sizeof($aryClassList[0]); $i++)
{
	
	$css = ($i%2) ? "2" : "";     
     $x .= "<tr>
     		<td class=tableContent$css>".$aryClassList[0][$i][1]."&nbsp;</td>
     		<td class=tableContent$css><a class='tableContentLink' href='class_history_student.php?y=".$y."&c=".$aryClassList[0][$i][1]."'>".$aryClassList[0][$i][2]."</a>&nbsp;</td>
     		<td class=tableContent$css><input type='checkbox' name='chk[]' value='".$aryClassList[0][$i][0]."' /></td></tr>";
}

$x .= "<tr><td align='right'>".$i_StudentPromotion_ClassSize."</td><td colspan='2'>".$aryClassList[1]."</td></tr>";
$x .= "</table>\n";

?>
<SCRIPT LANGUAGE=Javascript>
	function prepareSubmit(type, id){	
		switch(type){
			case 'search':
				location.href= "class_history_class.php?y="+document.getElementById('optYear').value;		
				break;
			case 'remove':
				if(confirm('<?=$i_StudentPromotion_mgt['CONFIRMDELETE'][1]?>')){
					location.href= "remove_history.php?from=2&y=<?=$y?>&c=<?=$c?>&id="+id;
				}
				break;
		}
		
	}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_am, '', $i_StudentPromotion, 'index.php', $i_StudentPromotion_mgt['NAV']['YEAR'], 'class_history_years.php', $i_StudentPromotion_mgt['NAV']['CLASS'], '') ?>
<?= displayTag("head_studentpromotion_$intranet_session_language.gif", $msg) ?>

<form name="form1" post="POST" action="">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="25"><?=$displayOption ?></td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
<div style='float:left;'><a class=iconLink href="javascript:checkGet(document.form1,'import_history.php')"><?=importIcon()."$button_import"?></a><a class=iconLink href="javascript:checkGet(document.form1,'export_history_update.php?y=<?=$y?>')"><?=exportIcon()."$button_export"?></a></div>
<div style='float:right'><a href='javascript:checkRemove(document.form1, "chk[]", "remove_history.php?from=2&y=<?=$y?>&c=<?=c?>");'><img src="<?=$image_path?>/admin/button/t_btn_delete_<?=$intranet_session_language?>.gif" border="none"></a>&nbsp;</div>

</td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?=$x?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type="hidden" name="fromPage" value="class" />
</form>
<?php
include_once("../../templates/adminfooter.php");
?>