<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");

include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");

intranet_opendb();

$sql = "SELECT
              b.UserID
        FROM INTRANET_PROMOTION_STATUS as b LEFT OUTER JOIN INTRANET_USER as a
             ON b.UserID = a.UserID AND a.RecordType = 2
             AND a.RecordStatus IN (0,1,2) 
             WHERE b.NewClass = $ClassID AND a.UserID IS NOT NULL
             ";

                if ($order_selection != 2 && $order_selection != 3)
                        $order_selection = 1;

                if ($order_selection == 1)        # Pure English Name
                    $sql .= "order by a.EnglishName" ;
                else if ($order_selection == 2)        # Boys First, English Name
                    $sql .= "order by a.Gender DESC, a.EnglishName ASC" ;
                else        # Girls first, English Name
                    $sql .= "order by a.Gender ASC, a.EnglishName ASC" ;

                $li = new libdb();
                $users = $li->returnVector($sql);
                
                for ( $i=0; $i<sizeOf($users); $i++){
                      $uid = $users[$i];
                      $newClassNumber = $i+1;
                      if ($newClassNumber < 10)
                      {
                          $newClassNumber = "0".$newClassNumber;
                      }
                      $sql = "update INTRANET_PROMOTION_STATUS
                              set NewClassNumber = '$newClassNumber'
                              where UserID = $uid";
                      $li->db_db_query($sql);
                }
                /*
                $li->rs = $li->db_db_query($sql);
                $result = $li->db_fetch_array();

                $i=0;
                while ( $result ){
                        $promotion_status[$i][UserID] = $result[0];

                        $newClassNumber = $i+1;
                        if ( $newClassNumber < 10 )
                                $newClassNumber = "0" . $newClassNumber;

                        $promotion_status[$i][NewClassNumber]=$newClassNumber;
                        $promotion_status[$i][NewClass]=$ClassID;

                        $result = $li->db_fetch_array();
                        $i++;
                }

                for ( $i=0; $i<sizeOf($promotion_status); $i++){
                                        $sql = "update INTRANET_PROMOTION_STATUS
                                                                                        set NewClassNumber = '" . $promotion_status[$i][NewClassNumber] . "'
                                                                                where UserID = " . $promotion_status[$i][UserID] . " AND
                                                                                        NewClass = " .  $promotion_status[$i][NewClass] ;
                                        $promotionResult = $li->db_db_query($sql);
                }
*/

intranet_closedb();
header("Location: class.php?ClassID=$ClassID&order_selection=$order_selection&msg=2");
?>
