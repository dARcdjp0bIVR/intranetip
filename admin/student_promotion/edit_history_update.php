<?php 

# using: yat

############################
#
#	Date:	2011-10-07 [YatWoon]
#			Fixed: Cannot retrieve $userID due to there is no $keepRecord[0] (user remove the first record)
#
############################

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
//include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/lib.php");
include_once("../../includes/libuser.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/libmanagehistory.php");

intranet_opendb();
$lmh = new libmanagehistory();

$error = false;

if(!isset($keepRecord))
{
	$referer = "handleProblematic.php?page=$page&display=$display&search=$search&msg=24";
	header("Location:".$referer);
	exit;
}

if(isset($keepRecord)){

	### retrieve the 1st keepRecord data (maybe there is no [0])
	$first_record = "";
	foreach($keepRecord as $k=>$d)
	{
		if(trim($d))
		{
			$first_record = $d;
			break;
		}
	}
	
	//Get UserID
	//$userID = $lmh->getUserIDByRecordId($keepRecord[0]);
	$userID = $lmh->getUserIDByRecordId($first_record);
	if(!$userID)
	{
		$referer = "handleProblematic.php?page=$page&display=$display&search=$search&msg=24";
		header("Location:".$referer);
		exit;
	}
	
	if((isset($from) && $from != "duplicate") || !isset($from)  ){
		//Remove records not in keep list
		if(trim($strDeleteRecordID) != ""){
			$lmh->doRemoveHistoryRecordInList($strDeleteRecordID);	
		}else{
			$lmh->doRemoveHistoryRecordNotInList($keepRecord, $userID);	
		}
	}

	//Perform Update	
	for($i=0; $i<sizeof($keepRecord); $i++){		
		$rs = $lmh->updateStudentHistoryByRecord($history[$i],$year[$i],$class[$i],$classnum[$i]);
		if(!$rs)
			$error = true;
	}
}

if($error){
	$msg = 10;	//Edit success, some records were duplicates
}else{
	$msg = 2;	//Edit success
}

intranet_closedb();

if(isset($from)){
	switch($from){
		case "problem":
			$referer = "handleProblematic.php?page=$page&display=$display&search=$search&msg=$msg";
			break;
		case "duplicate":	
			$referer = "handleDuplicate.php?page=$page&display=$display&search=$search&msg=$msg";
			break;
		case 3:
			$referer = "class_history_student.php?y=$fromYear&c=$fromClass&search=$search&msg=$msg";
			break;
		default:
			$referer = "class_history_student.php?y=$fromYear&c=$fromClass&search=$search&msg=$msg";
			break;
	}
}else{
	$referer = "class_history_student.php?y=$fromYear&c=$fromClass&search=$search&msg=$msg";
}

header("Location:".$referer);
?>