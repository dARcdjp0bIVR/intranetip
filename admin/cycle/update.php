<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");

switch ($cycleID){
        case 0: $cycleID = 0; break;
        case 1: $cycleID = 1; break;
        case 2: $cycleID = 2; break;
        case 3: $cycleID = 3; break;
        case 4: $cycleID = 4; break;
        case 5: $cycleID = 5; break;
        case 6: $cycleID = 6; break;
        case 7: $cycleID = 7; break;
        case 8: $cycleID = 8; break;
        case 9: $cycleID = 9; break;
        case 10: $cycleID = 10; break;
        case 11: $cycleID = 11; break;
        case 12: $cycleID = 12; break;
        default: $cycleID = 0; break;
}

$cycleTime = ($cycleID==0) ? date("Y-m-d") : $cycleTime;
$y = substr($cycleTime,0,4);
$m = substr($cycleTime,5,2);
$d = substr($cycleTime,8,2);
$ts = mktime(0,0,0,$m,$d,$y);

$li = new libfilesystem();
$li->file_write(htmlspecialchars(trim(stripslashes($cycleID))), $intranet_root."/file/cycle.txt");
$li->file_set_modification_time($intranet_root."/file/cycle.txt", $ts);

# Special Arrangement for SARS
if ($specialstart != "" && $specialend != "")
{
    $li->file_write(htmlspecialchars(trim($specialstart))."\n".htmlspecialchars(trim($specialend)),"$intranet_root/file/specialDates.txt");
}

header("Location: index.php?msg=2");
?>