<?php
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libgeneralsettings.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/eclass_api/libeclass_api.php");

intranet_opendb();

$libeclass_api = new libeclass_api();
$settingValuesAry = array();

for($i=0;$i<count($eClassAPIConfig['settings']);$i++) {
	$settingValuesAry[$eClassAPIConfig['settings'][$i]] = $_REQUEST[$eClassAPIConfig['settings'][$i]];
}

$update_success = $libeclass_api->Save_General_Setting($eClassAPIConfig['moduleCode'],$settingValuesAry);
$msg = $update_success ? 2:13;

intranet_closedb();
header("Location: index.php?msg=$msg");
?>