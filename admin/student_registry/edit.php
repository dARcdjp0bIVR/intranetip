<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser_ext.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsystemaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");

include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");
intranet_opendb();

//error_reporting(E_ALL);

/*
switch for special handle (Macau spec.)
available in the import page as well
*/
$page_check = array();
$page_check['EnglishNameUpper'] = true;
$page_check['ClassNumber'] = true;
$page_check['SP_IssueDate_Not_Null'] = true;
$page_check['SP_ValidDate_Not_Null_For_Type_2_3'] = true;
$page_check['CountryText_For_Code_0_Only'] = true;
$page_check['R_AreaText_For_Code_O_Only'] = true;
$page_check['R_AreaText_Not_Null_For_Code_O'] = true;
$page_check['EC_NullOrFillCertain6Fields'] = true;

///////////////////////////////////////////////


$ClassIDs = $_REQUEST['ClassID'];
$ClassID_String = '';
for($i=0;$i<sizeof($ClassID);$i++){
	$ClassID_String .= '&ClassID[]='.$ClassID[$i];
}

if($SID!=""){
	$student_id = $SID;
}
else if(sizeof($StudentID)==1){
	$student_id = $StudentID[0];
}


$li = new libuser($student_id);
$TabID = $li->RecordType;

# Get more detail of a student
$lu_ext = new libuser_ext();
$lu_ext->loadByUserID($li->UserID);
//echo mysql_error();
$iportfolio_activated = false;

# Get student detail from iPortfolio
if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();
	if($TabID==2){
		for($i=0;$i<sizeof($data);$i++){
			if($student_id==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}
	}
}

$Gender0 = ($li->Gender=="M") ? "CHECKED" : "";
$Gender1 = ($li->Gender=="F") ? "CHECKED" : "";

# Get guardian details
$sql =	"
					SELECT
						igs.RecordID,
						igs.ChName,
						igs.EnName,
						igs.Phone,
						igs.EmPhone,
						igs_ext_1.Occupation,
						igs_ext_1.IsLiveTogether,
						LPAD(igs.Relation,2,0) Relation,
						igs.IsMain,
						igs.IsSMS,
						igs_ext_1.IsEmergencyContact,
						igs_ext_1.AreaCode,
						igs_ext_1.Road,
						igs_ext_1.Address
					FROM
						$eclass_db.GUARDIAN_STUDENT igs
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 igs_ext_1
					ON
						igs.RecordID = igs_ext_1.RecordID
					WHERE
						igs.UserID = $student_id
					ORDER BY
						igs.IsMain DESC,
						igs_ext_1.IsEmergencyContact DESC,
						igs.IsSMS DESC
				";

$r = $li->returnArray($sql,39);
$OtherGuardian = array();

for($i=0; $i<count($r);$i++){
	if($r[$i]['Relation'] == '01'){
		$Father = $r[$i];
	}
	elseif($r[$i]['Relation'] == '02'){
		$Mother = $r[$i];
	}
	else{
		$OtherGuardian[] = $r[$i];
	}
}
$GuardianExist = (count($r)>0);
if(empty($Father)){
	$Father = array	(
										'RecordID'=>'',
										'ChName'=>'',
										'EnName'=>'',
										'Phone'=>'',
										'EmPhone'=>'',
										'Occupation'=>'',
										'IsLiveTogether'=>1,
										'Relation'=>'01',
										'IsMain'=>0,
										'IsSMS'=>0,
										'IsEmergencyContact'=>0,
										'AreaCode'=>'O',
										'Road'=>'',
										'Address'=>''
									);
}
if(empty($Mother)){
	$Mother = array	(
										'RecordID'=>'',
										'ChName'=>'',
										'EnName'=>'',
										'Phone'=>'',
										'EmPhone'=>'',
										'Occupation'=>'',
										'IsLiveTogether'=>1,
										'Relation'=>'02',
										'IsMain'=>0,
										'IsSMS'=>0,
										'IsEmergencyContact'=>0,
										'AreaCode'=>'O',
										'Road'=>'',
										'Address'=>''
									);
}

$lclass = new libclass();
$selection = $lclass->getSelectClass("onChange=\"this.form.ClassName.value=this.value\"",$li->ClassName);

# Generate place of birth selection 
$PlaceOfBirthCode_Select = "<select name='PlaceOfBirthCode'>";
foreach($i_StudentRegistry_PlaceOfBirth_Code as $v => $d){
	$PlaceOfBirthCode_Select .= "<option value='$v' ".($v==$lu_ext->PlaceOfBirthCode? "selected " :"")."/>".$d;
}
$PlaceOfBirthCode_Select .= "</select>";

# Generate type of ID selection
$ID_Type_Select = "<select name='ID_Type' >";
foreach($i_StudentRegistry_ID_Type_Code as $v => $d){
	$ID_Type_Select .= "<option value='$v' ".($v==$lu_ext->ID_Type? "selected " :"")."/>".$d;
}
$ID_Type_Select .= "</select>";

# Generate issue place of ID selection
$ID_IssuePlace_Select = "<select name='ID_IssuePlace' >";
foreach($i_StudentRegistry_ID_IssuePlace_Code as $v => $d){
	$ID_IssuePlace_Select .= "<option value='$v' ".($v==$lu_ext->ID_IssuePlace? "selected " :"")."/>".$d;
}
$ID_IssuePlace_Select .= "</select>";

# Generate type of SP selection
$SP_Type_Select = "<select name='SP_Type' >";
foreach($i_StudentRegistry_SP_Type_Code  as $v => $d){
	$SP_Type_Select .= "<option value='$v' ".($v==$lu_ext->SP_Type? "selected " :"")."/>".$d;
}
$SP_Type_Select .= "</select>";

# Generate country code selection
$CountryCode_Select = "<select name='CountryCode' onchange='updateCountryUI(this.form);'>";
foreach($i_StudentRegistry_Country_Code as $v => $d){
	$CountryCode_Select .= "<option value='$v' ".($v==$lu_ext->CountryCode? "selected " :"")."/>".$d;
}
$CountryCode_Select .= "</select>";
$CountryText = ($lu_ext->CountryCode == '' || $lu_ext->CountryCode == '0') ? $li->Country : '';

# Generate residential(night) area code selection
$R_AreaCode_Select = "<select name='R_AreaCode' onchange='updateR_AreaUI(this.form);'>";
foreach($i_StudentRegistry_ResidentialAreaNight_Code as $v => $d){
	$R_AreaCode_Select .= "<option value='$v' ".($v==$lu_ext->R_AreaCode? "selected " :"")."/>".$d;
}
$R_AreaCode_Select .= "</select>";

# Generate residential area code selection
$AreaCode_Select = "<select name='AreaCode'>";
foreach($i_StudentRegistry_AreaCode as $v => $d){
	$AreaCode_Select .= "<option value='$v' ".($v==$lu_ext->AreaCode? "selected " :"")."/>".$d;
}
$AreaCode_Select .= "</select>";

## blank js relation selection
$js_select_relation ="<SELECT name='relation_new_\"+row.id+\"'>";
$js_select_relation.="<OPTION value=''>-- $button_select --</OPTION>";
foreach($ec_guardian AS $key =>$relation_name){
	if($key=='01' || $key=='02'){continue;}
        $js_select_relation.="<OPTION value='$key'>$relation_name</OPTION>";
}
$js_select_relation.="</SELECT>";

?>


<script language="javascript">
function updateEnName(form){
<?php if ($page_check['EnglishNameUpper']){ ?>

	var tmp = form.EnglishName.value.toUpperCase();
	if(tmp!=form.EnglishName.value){
		alert("<?php echo $i_StudentRegistry_EnglishName_Alert;?>");
		form.EnglishName.value = tmp;
	}

<?php } ?>
}

function updateR_AreaUI(form){
<?php if($page_check['R_AreaText_For_Code_O_Only']){ ?>

	var code_val = form.R_AreaCode.value;
	var text_disabled = (code_val != '' && code_val != 'O');
	if(text_disabled){
		form.R_AreaText.value = '';
	}
	form.R_AreaText.disabled = text_disabled;

<?php } ?>
}

function updateCountryUI(form){
<?php if($page_check['CountryText_For_Code_0_Only']){ ?>

	var code_val = form.CountryCode.value;
	var text_disabled = (code_val != '' && code_val != '0');
	if(text_disabled){
		form.CountryText.value = '';
	}
	form.CountryText.disabled = text_disabled;

<?php } ?>
}
function updateMainGuardianUI(form){
	//get radio value
	var i;
	var theValue = '';
	for(i=0; i<form.Guardian_Type.length;i++){
		if(form.Guardian_Type[i].checked){
			theValue = form.Guardian_Type[i].value;
			break;
		}
	}
	var disabled_other_mg = (theValue !='O' && theValue !='');
	var mg_elements = document.getElementsByName('main[]');
	for(i=0; i<mg_elements.length;i++){
		if(mg_elements[i].form==form){
			mg_elements[i].disabled = disabled_other_mg;
		}
	}
}
function updateEmergencyContactUI(form){
	//get radio value
	var i;
	var theValue = '';
	for(i=0; i<form.EmergencyContact_Type.length;i++){
		if(form.EmergencyContact_Type[i].checked){
			theValue = form.EmergencyContact_Type[i].value;
			break;
		}
	}
	var disabled_other_ec = (theValue !='O' && theValue !='');
	var ec_elements = document.getElementsByName('ec[]');
	for(i=0; i<ec_elements.length;i++){
		if(ec_elements[i].form==form){
			ec_elements[i].disabled = disabled_other_ec;
		}
	}
}

function updateSMSUI(form){
	//get radio value
	var i;
	var theValue = '';
	for(i=0; i<form.SMS_Type.length;i++){
		if(form.SMS_Type[i].checked){
			theValue = form.SMS_Type[i].value;
			break;
		}
	}
	var disabled_other_sms = (theValue !='O' && theValue !='');
	var sms_elements = document.getElementsByName('sms[]');
	for(i=0; i<sms_elements.length;i++){
		if(sms_elements[i].form==form){
			sms_elements[i].disabled = disabled_other_sms;
		}
	}
}
function checkGuardian(objChName,objEnName,objPhone,objEmPhone){
	if(objChName.value == '' && objEnName.value==''){
		alert('<?=$i_StudentGuardian_warning_enter_chinese_english_name?>');
		objChName.focus();
		return false;
	}
	if(objPhone.value=='' && objEmPhone.value==''){
		alert('<?=$i_StudentGuardian_warning_enter_phone_emphone?>');
		objPhone.focus();
		return false;
	}
	return true;
}

function addGuardian(){
  objTable = document.getElementById("guardians");
  tableRows = objTable.rows;

  rowStyle ='tableContent';
  row = objTable.insertRow(-1);
  row.id = parseInt(document.form1.tsize.value,10)+1;

  document.form1.tsize.value = row.id;
  document.form1.newids.value+=document.form1.newids.value!=""?","+row.id:row.id;

  cell0 = "<table border=0 cellpadding=0 cellspacing=0 width=100%>";
  cell0+="<tr><td align=right><?=$i_UserChineseName?> :&nbsp;</td><td><input type=text name='chname_new_"+row.id+"' value='' size=10></td></tr>";
  cell0+="<tr><td align=right><?=$i_UserEnglishName?> :&nbsp;</td><td><input type=text name='enname_new_"+row.id+"' value='' size=20></td></tr>";
  cell0+="<tr><td align=right><?=$i_StudentGuardian_Occupation?> :&nbsp;</td><td><input type=text name='occupation_new_"+row.id+"' value='' size=20 maxlength=60></td></tr>";
  cell0+="<tr><td align=right><?=$ec_iPortfolio['relation']?> :&nbsp;</td><td><?=$js_select_relation?></td></tr>";
  cell0+="<tr><td align=right><?=$i_StudentGuardian_Phone?> :&nbsp;</td><td><input type=text name='phone_new_"+row.id+"' value='' size=10></td></tr>";
  cell0+="<tr><td align=right><?=$i_StudentGuardian_EMPhone?> :&nbsp;</td><td><input type=text name='emphone_new_"+row.id+"' value='' size=10></td></tr>";
  cell0+="<tr><td align=right><?=$i_StudentGuardian_LiveTogether?> :&nbsp;</td><td><input type=radio name='liveTogether_new_"+row.id+"' value='1' id='liveTogether_new_"+row.id+"_1' checked/><label for='liveTogether_new_"+row.id+"_1'><?=$i_general_yes?></label> <input type=radio name='liveTogether_new_"+row.id+"' value='0' id='liveTogether_new_"+row.id+"_0'/><label for='liveTogether_new_"+row.id+"_0'><?=$i_general_no?></label></td></tr>";
<?php
  $js_select_area ="<SELECT name='add_area_new_\"+row.id+\"'>";
  foreach($i_StudentRegistry_AreaCode as $v=>$d)
		$js_select_area .="<OPTION value='$v'/>$d";
        
	$js_select_area.="</SELECT>";
?>
  cell0+="<tr><td align=right><?=$i_UserAddress_Area?> :&nbsp;</td><td ><?=$js_select_area?></td></tr>";
  cell0+="<tr><td align=right><?=$i_UserAddress_Road?> :&nbsp;</td><td ><input type=text name='add_road_new_"+row.id+"' value='' size=20 maxlength='50' /></td></tr>";
  cell0+="<tr><td align=right><?=$i_UserAddress_Address?> :&nbsp;</td><td ><input type=text name='add_address_new_"+row.id+"' value='' size=20 maxlength='50' /></td></tr>";

	cell1="<input type=radio name='main[]' value='new_"+row.id+"'>";

	cell12 = "<input type=radio name='ec[]' value='new_"+row.id+"'>";
	<?php if($plugin['sms']){ ?>
		cell2 = "<input type=radio name='sms[]' value='new_"+row.id+"'>";
		cell3 = "<input type='button' value=' - ' onClick=\"javascript:removeGuardian('"+row.id+"')\">";
  <?php } else{?>
    cell2 = "<input type='button' value=' - ' onClick=\"javascript:removeGuardian('"+row.id+"')\">";
  <?php } ?>

  cell = row.insertCell(0);
  cell.className = rowStyle;
  cell.innerHTML = cell0;

  cell = row.insertCell(1);
  cell.className = rowStyle;
  cell.innerHTML = cell1;

  cell = row.insertCell(2);
  cell.className = rowStyle;
  cell.innerHTML = cell12;

  cell = row.insertCell(3);
  cell.className = rowStyle;
  cell.innerHTML = cell2;

	<?php if($plugin['sms']){?>
    cell = row.insertCell(4);
    cell.className = rowStyle;
    cell.innerHTML = cell3;
  <?php } ?>

	updateMainGuardianUI(document.form1);
	updateEmergencyContactUI(document.form1);
	<?php if($plugin['sms']) {?>
		updateSMSUI(document.form1);
	<?php } ?>
}

// remove existing guardian
function checkRemoveGuardian(obj){
	if(obj.checked==false)
		return;
  guardian_id = obj.value;
  
  objMains = document.getElementsByName('main[]');
	for(i=0;i<objMains.length;i++){
		if(objMains[i].value == guardian_id){
			if(objMains[i].checked){
				alert('<?=$i_StudentGuardian_warning_Delete_Main_Guardian?>');
			}
			break;
		}
	}

  objECs = document.getElementsByName('ec[]');
	for(i=0;i<objECs.length;i++){
		if(objECs[i].value == guardian_id){
			if(objECs[i].checked){
				alert('<?=$i_StudentGuardian_warning_Delete_EmergencyContact?>');
			}
			break;
		}
	}
        
	<?php if($plugin['sms']){?>
		objSms  = document.getElementsByName('sms[]');
		for(i=0;i<objSms.length;i++){
			if(objSms[i].value == guardian_id){
				if(objSms[i].checked){
					alert('<?=$i_StudentGuardian_warning_Delete_SMS?>');
				}
				break;
			}
		}
	<?php } ?>
}


// to remove new guardian
function removeGuardian(rid){

	objTable = document.getElementById("guardians");
	tableRows = objTable.rows;
	for(i=0;i<tableRows.length;i++)
	{
		if(tableRows[i].id==rid){
			objTable.deleteRow(i);
			if(rid.indexOf("r_")>-1){
			}
			else{
				newids = document.form1.newids.value;
				if(newids==rid) newids="";
					newids = newids.replace(","+rid,"");
				newids = newids.replace(rid+",","");
				document.form1.newids.value = newids;
			}
			break;
		}
	}
}

function checkform(obj){
	updateEnName(obj);

	// Chinese name and English name must not both be empty
	if(obj.ChineseName.value == ""){
		if(!check_text(obj.EnglishName, "<?php echo $i_alert_pleasefillin.$i_UserEnglishName; ?>.")) {
			return false;
		}
	}

<?php if($page_check['ClassNumber']) { ?>
  // Class number must not be empty
	if(!check_text(obj.ClassNumber, "<?php echo $i_alert_pleasefillin.$i_UserClassNumber; ?>.")) {
		return false;
	}
<?php } ?>

<?php if($page_check['SP_IssueDate_Not_Null']) { ?>
  // SP issue date must not be empty if SP type is filled
	var sp_type_value = obj.SP_Type.value;
	if(sp_type_value !=""){
		if(!check_text(obj.SP_IssueDate, "<?php echo $i_alert_pleasefillin.$i_StudentRegistry_SP_IssueDate; ?>.")) {
			return false;
		}
	}
<?php } ?>

<?php if($page_check['SP_ValidDate_Not_Null_For_Type_2_3']) { ?>
  // SP valid date must not be empty if SP type is 2 or 3
	var sp_type_value = obj.SP_Type.value;
	if(sp_type_value=='2' || sp_type_value=='3'){
		if(!check_text(obj.SP_ValidDate, "<?php echo $i_alert_pleasefillin.$i_StudentRegistry_SP_ValidDate; ?>.")) {
			return false;
		}
	}
<?php } ?>

<?php if($page_check['R_AreaText_Not_Null_For_Code_O']) { ?>
  // R_AreaText must not be empty if R_AreaCode is filled
	var code_val = obj.R_AreaCode.value;
	if(code_val == 'O'){
		if(!check_text(obj.R_AreaText, "<?php echo $i_alert_pleasefillin.$i_StudentRegistry_ResidentialArea_Night; ?>.")) {
			return false;
		}
	}
<?php } ?>

<?php if($page_check['EC_NullOrFillCertain6Fields']){ ?>
  // Emergency contact must all be filled or empty
	objECs = document.getElementsByName('ec[]');
	for(i=0;i<objECs.length;i++){
		if(objECs[i].checked){
			if(obj.elements['relation_'+objECs[i].value].value == ''){
				alert("<?php echo $i_alert_pleasefillin.$ec_iPortfolio['relation']; ?>.");
				return false;
			}
			if(!check_text(obj.elements['add_road_'+objECs[i].value], "<?php echo $i_alert_pleasefillin.$i_StudentRegistry_ResidentialRoad; ?>.")) {
				return false;
			}
			if(!check_text(obj.elements['add_address_'+objECs[i].value], "<?php echo $i_alert_pleasefillin.$i_StudentRegistry_ResidentialAddress; ?>.")) {
				return false;
			}
			break;
		}
	}
<?php } ?>
	// Check guardians
	// Get IDs from main[]
	var ids = new Array();
	var objMains = document.getElementsByName('main[]');
	for(i=0; i<objMains.length;i++){
		ids[ids.length] = objMains[i].value;
	}

	// Get delete IDs
	var deletedids='';
	var delim =',';
	var objsDelete = document.getElementsByName('Delete[]');
	for(j=0;j<objsDelete.length;j++){
		if(objsDelete[j].checked){
			deletedids+=delim+objsDelete[j].value;
		}
	}
  for(i=0;i<ids.length;i++){
		// Do not perform checking if deleted
		if(deletedids.indexOf(delim+ids[i])>-1) continue;
		
		objChName = getElement('chname_'+ids[i]);
		objEnName = getElement('enname_'+ids[i]);
		objPhone   = getElement('phone_'+ids[i]);
		objEmPhone = getElement('emphone_'+ids[i]);
		
		// Guardian objects correctly loaded
		if(objChName==null || objEnName==null || objPhone==null || objEmPhone==null) return false;
		
		// Chinese name or English name, phone number or emergency phone number must not be empty
		if(!checkGuardian(objChName,objEnName,objPhone,objEmPhone)) return false;
	}

	// Default checked the first other guardian if the Guardian Type is set to 'O' 
	GType = returnChecked(document.form1, 'Guardian_Type');
	SMain = returnChecked(document.form1, 'main[]');
	if(GType == 'O' && SMain == null && typeof document.form1.elements['main[]'] != 'undefined')
		document.getElementsByName('main[]')[0].checked = true;

	return true;
}

function submitform(formObj){
  if(checkform(formObj)){
		formObj.method = 'POST';
		formObj.submit();
	}
}

function resetform(formObj){
  formObj.action='';
  formObj.submit();
}

function back(){
	//core parameters:order field page no, page size, filter
	var url;
	url = 'search_result.php?order=<?=$order?>&field=<?=$field?>&pageNo=<?=$pageNo?>&keyword=<?=$keyword?>&ts=<?=$ts?><?=$ClassID_String?>';
	document.location = url;
}

function getElement(str_name){
	objs = document.getElementsByName(str_name);
	return objs[0];
}

</script>

<form name="form1" action="edit_update.php" method="post">
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentRegistry['System'], 'index.php',$button_search,'search.php', $button_edit, '')?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>

<?php if($TabID == 2){ ?>
	<tr>
		<td align=right nowrap><?=$i_WebSAMS_Registration_No ?>:</td>
		<td><input class=text type=text name=WebSamsRegNo size=20 maxlength=20 value="<?=$li->WebSamsRegNo ?>" <?=($iportfolio_activated?"DISABLED=TRUE":"")?> /><br /><?=$i_WebSAMSRegNo_Format_Notice?></td>
	</tr>
<? } ?>

	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_AuthCodeMain ?>:</td>
		<td><input class=text type=text name="AuthCodeMain" size=7 maxlength=7 value="<?=$lu_ext->AuthCodeMain ?>" /> <input class=text type=text name="AuthCodeCheck" size=1 maxlength=1 value="<?=$lu_ext->AuthCodeCheck ?>"></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_UserChineseName ?>:</td>
		<td><input class=text type=text name=ChineseName size=20 maxlength=100 value="<?=$li->ChineseName ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_UserEnglishName ?>:</td>
		<td><input class=text type=text name=EnglishName size=20 maxlength=100 value="<?=$li->EnglishName ?>"  onchange="updateEnName(this.form)" /></td>
	</tr>

<?php if ($TabID != 4) { ?>
	<tr>
		<td align=right nowrap><?= $i_UserClassName ?>:</td>
		<td><input class=text type=text name=ClassName size=10 maxlength=20 value="<?=$li->ClassName ?>" /><?=$selection?></td>
	</tr>
<?php } ?>

	<tr>
		<td align=right nowrap><?=$i_UserClassNumber ?>:</td>
		<td><input class=text type=text name=ClassNumber size=10 maxlength=20 value="<?=$li->ClassNumber ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_UserGender ?>:</td>
		<td>
			<input type=radio name=Gender value=M <?=$Gender0 ?> /> <?=$i_gender_male ?>
			<input type=radio name=Gender value=F <?=$Gender1 ?> /> <?=$i_gender_female ?>
		</td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_UserDateOfBirth ?>:</td>
		<td><input class=text type=text name=DateOfBirth size=10 maxlength=10 value="<?=$li->DateOfBirth ?>" /> <span class=extraInfo>(yyyy-mm-dd)</span></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_PlaceOfBirth ?>:</td>
		<td><?=$PlaceOfBirthCode_Select?></td>
	</tr>

<?php
$ID_Table = "<table border=0 cellpadding=1 cellspacing=0>";
$ID_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_ID_No:&nbsp;</td><td><input class=text type=text name='ID_Num' size=10 maxlength=20 value='".$lu_ext->ID_Num."'></td></tr>";
$ID_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_ID_IssuePlace:&nbsp;</td><td>$ID_IssuePlace_Select</td></tr>";
$ID_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_ID_IssueDate:&nbsp;</td><td><input class=text type=text name=ID_IssueDate size=10 maxlength=10 value='".$lu_ext->ID_IssueDate."'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>";
$ID_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_ID_ValidDate:&nbsp;</td><td><input class=text type=text name=ID_ValidDate size=10 maxlength=10 value='".$lu_ext->ID_ValidDate."'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>";
$ID_Table .= "</table>";

$SP_Table = "<table border=0 cellpadding=1 cellspacing=0>";
$SP_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_SP_No:&nbsp;</td><td><input class=text type=text name='SP_Num' size=10 maxlength=20 value='".$lu_ext->SP_Num."'> <span class=extraInfo>($i_ifapplicable)</span></td></tr>";
$SP_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_SP_IssueDate:&nbsp;</td><td><input class=text type=text name=SP_IssueDate size=10 maxlength=10 value='".$lu_ext->SP_IssueDate."'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>";
$SP_Table .= "<tr><td align=right nowrap>$i_StudentRegistry_SP_ValidDate:&nbsp;</td><td><input class=text type=text name=SP_ValidDate size=10 maxlength=10 value='".$lu_ext->SP_ValidDate."'> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>";
$SP_Table .= "</table>";

?>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_ID_Type ?>:</td>
		<td><?=$ID_Type_Select?><br /><?=$ID_Table?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_SP_Type ?>:</td>
		<td><?=$SP_Type_Select?><br /><?=$SP_Table?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_UserCountry ?>:</td>
		<td><?=$CountryCode_Select?> <input class=text type=text name=CountryText size=10 maxlength=32 value="<?=$CountryText ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_Province ?>:</td>
		<td><input class=text type=text name=Province size=10 maxlength=20 value="<?=$lu_ext->Province ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_UserHomeTelNo ?>:</td>
		<td><input class=text type=text name=HomeTelNo size=15 maxlength=20 value="<?=$li->HomeTelNo ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_ResidentialArea_Night ?>:</td>
		<td><?=$R_AreaCode_Select?> <input class=text type=text name=R_AreaText size=10 maxlength=40 value="<?=$lu_ext->R_AreaText ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_ResidentialArea ?>:</td>
		<td><?=$AreaCode_Select?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_ResidentialRoad ?>:</td>
		<td><input class=text type=text name=Road size=15 maxlength=50 value="<?=$lu_ext->Road ?>" /></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentRegistry_ResidentialAddress ?>:</td>
		<td><input class=text type=text name=Address size=15 maxlength=50 value="<?=$lu_ext->Address ?>" /></td>
	</tr>

<?php
# Guardian related
$MainGuard = $Father['IsMain'] ? 'F' : ($Mother['IsMain'] ? 'M' : 'O');
$MG_Select = "<input type=radio name='Guardian_Type' value='F' ".($MainGuard=='F'?'checked ':'')." id='Guardian_Type_F' onclick='updateMainGuardianUI(this.form);'/><label for='Guardian_Type_F'>".$ec_guardian['01']."</label>";
$MG_Select .= "<input type=radio name='Guardian_Type' value='M' ".($MainGuard=='M'?'checked ':'')." id='Guardian_Type_M' onclick='updateMainGuardianUI(this.form);'/><label for='Guardian_Type_M'>".$ec_guardian['02']."</label>";
$MG_Select .= "<input type=radio name='Guardian_Type' value='O' ".($MainGuard=='O'?'checked ':'')." id='Guardian_Type_O' onclick='updateMainGuardianUI(this.form);'/><label for='Guardian_Type_O'>".$ec_guardian['08']."</label>";

$EC = $Father['IsEmergencyContact'] ? 'F' : ($Mother['IsEmergencyContact'] ? 'M' : 'O');
$EC_Select = "<input type=radio name='EmergencyContact_Type' value='F' ".($EC=='F'?'checked ':'')." id='EmergencyContact_Type_F' onclick='updateEmergencyContactUI(this.form);'/><label for='EmergencyContact_Type_F'>".$ec_guardian['01']."</label>";
$EC_Select .= "<input type=radio name='EmergencyContact_Type' value='M' ".($EC=='M'?'checked ':'')." id='EmergencyContact_Type_M' onclick='updateEmergencyContactUI(this.form);'/><label for='EmergencyContact_Type_M'>".$ec_guardian['02']."</label>";
$EC_Select .= "<input type=radio name='EmergencyContact_Type' value='O' ".($EC=='O'?'checked ':'')." id='EmergencyContact_Type_O' onclick='updateEmergencyContactUI(this.form);'/><label for='EmergencyContact_Type_O'>".$ec_guardian['08']."</label>";

$SMS = $Father['IsSMS'] ? 'F' : ($Mother['IsSMS'] ? 'M' : 'O');
$SMS_Select = "<input type=radio name='SMS_Type' value='F' ".($SMS=='F'?'checked ':'')." id='SMS_Type_F' onclick='updateSMSUI(this.form);'/><label for='SMS_Type_F'>".$ec_guardian['01']."</label>";
$SMS_Select .= "<input type=radio name='SMS_Type' value='M' ".($SMS=='M'?'checked ':'')." id='SMS_Type_M' onclick='updateSMSUI(this.form);'/><label for='SMS_Type_M'>".$ec_guardian['02']."</label>";
$SMS_Select .= "<input type=radio name='SMS_Type' value='O' ".($SMS=='O'?'checked ':'')." id='SMS_Type_O' onclick='updateSMSUI(this.form);'/><label for='SMS_Type_O'>".$ec_guardian['08']."</label>";

$father_table = "<table border=0 cellpadding=0 cellspacing=0 >";
$father_table .= "<tr><td align=right>$i_UserChineseName :&nbsp;</td><td ><input type=text name='chname_R01' value='".$Father['ChName']."' size=10></td></tr>";
$father_table .= "<tr><td align=right>$i_UserEnglishName :&nbsp;</td><td ><input type=text name='enname_R01' value='".$Father['EnName']."' size=20></td></tr>";
$father_table .= "<tr><td align=right>$i_StudentGuardian_Occupation :&nbsp;</td><td ><input type=text name='occupation_R01' value='".$Father['Occupation']."' size=20 maxlength=60></td></tr>";
$father_table .= "<tr><td align=right>$i_StudentGuardian_Phone :&nbsp;</td><td ><input type=text name='phone_R01' value='".$Father['Phone']."' size=10></td></tr>";
$father_table .= "<tr><td align=right>$i_StudentGuardian_EMPhone :&nbsp;</td><td ><input type=text name='emphone_R01' value='".$Father['EmPhone']."' size=10></td></tr>";
$father_table .= "<tr><td align=right>$i_StudentGuardian_LiveTogether :&nbsp;</td><td ><input type=radio name='liveTogether_R01' value='1'".($Father['IsLiveTogether']==1?" CHECKED ":"")." id='liveTogether_R01_1'/><label for='liveTogether_R01_1'>".$i_general_yes."</label> <input type=radio name='liveTogether_R01' value='0'".($Father['IsLiveTogether']!=1?" CHECKED ":"")." id='liveTogether_R01_0'/><label for='liveTogether_R01_0'>".$i_general_no."</label></td></tr>";

$F_AreaCode_Select = "<select name='add_area_R01'>";
if(!empty($i_StudentRegistry_AreaCode)){
	foreach($i_StudentRegistry_AreaCode as $v=>$d){
		$F_AreaCode_Select .= "<option value='$v' ".($Father['AreaCode']==$v?"selected ":"")."/>".$d;
	}
}
$F_AreaCode_Select .= "</select>";

$father_table .= "<tr><td align=right>$i_UserAddress_Area :&nbsp;</td><td >$F_AreaCode_Select</td></tr>";
$father_table .= "<tr><td align=right>$i_UserAddress_Road :&nbsp;</td><td ><input type=text name='add_road_R01' value='".$Father['Road']."' size=20 maxlength='50' /></td></tr>";
$father_table .= "<tr><td align=right>$i_UserAddress_Address :&nbsp;</td><td ><input type=text name='add_address_R01' value='".$Father['Address']."' size=20 maxlength='50' /></td></tr>";
$father_table .= "</table>";
$father_table .= "<input type='hidden' name='record_id_R01' value='".$Father['RecordID']."'>";

$mother_table = "<table border=0 cellpadding=0 cellspacing=0 >";
$mother_table .= "<tr><td align=right>$i_UserChineseName :&nbsp;</td><td ><input type=text name='chname_R02' value='".$Mother['ChName']."' size=10></td></tr>";
$mother_table .= "<tr><td align=right>$i_UserEnglishName :&nbsp;</td><td ><input type=text name='enname_R02' value='".$Mother['EnName']."' size=20></td></tr>";
$mother_table .= "<tr><td align=right>$i_StudentGuardian_Occupation :&nbsp;</td><td ><input type=text name='occupation_R02' value='".$Mother['Occupation']."' size=20 maxlength=60></td></tr>";
$mother_table .= "<tr><td align=right>$i_StudentGuardian_Phone :&nbsp;</td><td ><input type=text name='phone_R02' value='".$Mother['Phone']."' size=10></td></tr>";
$mother_table .= "<tr><td align=right>$i_StudentGuardian_EMPhone :&nbsp;</td><td ><input type=text name='emphone_R02' value='".$Mother['EmPhone']."' size=10></td></tr>";
$mother_table .= "<tr><td align=right>$i_StudentGuardian_LiveTogether :&nbsp;</td><td ><input type=radio name='liveTogether_R02' value='1'".($Mother['IsLiveTogether']==1?" CHECKED ":"")." id='liveTogether_R02_1'/><label for='liveTogether_R02_1'>".$i_general_yes."</label> <input type=radio name='liveTogether_R02' value='0'".($Mother['IsLiveTogether']!=1?" CHECKED ":"")." id='liveTogether_R02_0'/><label for='liveTogether_R02_0'>".$i_general_no."</label></td></tr>";

$M_AreaCode_Select = "<select name='add_area_R02'>";
if(!empty($i_StudentRegistry_AreaCode)){
	foreach($i_StudentRegistry_AreaCode as $v=>$d){
		$M_AreaCode_Select .= "<option value='$v' ".($Mother['AreaCode']==$v?"selected ":"")."/>".$d;
	}
}
$M_AreaCode_Select .= "</select>";

$mother_table .= "<tr><td align=right>$i_UserAddress_Area :&nbsp;</td><td >$M_AreaCode_Select</td></tr>";
$mother_table .= "<tr><td align=right>$i_UserAddress_Road :&nbsp;</td><td ><input type=text name='add_road_R02' value='".$Mother['Road']."' size=20 maxlength='50' /></td></tr>";
$mother_table .= "<tr><td align=right>$i_UserAddress_Address :&nbsp;</td><td ><input type=text name='add_address_R02' value='".$Mother['Address']."' size=20 maxlength='50' /></td></tr>";
$mother_table .= "</table>";
$mother_table .= "<input type='hidden' name='record_id_R02' value='".$Mother['RecordID']."'>";

$other_table = "<table id='guardians' width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$other_table .= "<tr>";
$other_table .= "<td class='tableTitle' width=70%>&nbsp;</td>";
$other_table .= "<Td class='tableTitle' width=18%>$i_StudentGuardian_MainContent</td>";

$other_table .= "<Td class='tableTitle' >$i_StudentGuardian_EmergencyContact</td>";
if($plugin['sms']){
	$other_table .= "<Td class='tableTitle'>$i_StudentGuardian_SMS</td>";
}
$other_table .= "<td class='tableTitle'>$button_remove</td>";
$other_table .= "</tr>";
$other_table .=	"
									<input type=hidden name='tsize' value=0>
									<input type=hidden name='newids' value=''>
								";

if(sizeof($OtherGuardian)>0){
	for($i=0;$i<sizeof($OtherGuardian);$i++){
		$record_id = $OtherGuardian[$i]['RecordID'];
		$ch_name = $OtherGuardian[$i]['ChName'];
		$en_name = $OtherGuardian[$i]['EnName'];
		$occupation = $OtherGuardian[$i]['Occupation'];
		$phone = $OtherGuardian[$i]['Phone'];
		$emphone = $OtherGuardian[$i]['EmPhone'];
		$isLiveTogether = $OtherGuardian[$i]['IsLiveTogether'];
		$relation = $OtherGuardian[$i]['Relation'];
		$add_areacode = $OtherGuardian[$i]['AreaCode'];
		$add_road = $OtherGuardian[$i]['Road'];
		$add_address = $OtherGuardian[$i]['Address'];
		$isMain = $OtherGuardian[$i]['IsMain'];
		$isSMS = $OtherGuardian[$i]['IsSMS'];
		$isEmergencyContact = $OtherGuardian[$i]['IsEmergencyContact'];

		$css ="tableContent";
		$select_relation = "<SELECT name='relation_$record_id'>";
		$select_relation .= "<OPTION value=''>-- $button_select --</OPTION>";
		foreach($ec_guardian AS $key => $relation_name){
			if($key == '01' || $key == '02'){continue;}
			$select_relation .= "<OPTION value='$key'".($key==$relation?" SELECTED ":"").">$relation_name</OPTION>\n";
		}
		$select_relation .= "</SELECT>\n";

		$table = "<table border=0 cellpadding=0 cellspacing=0 width=100%>";
		$table .= "<tr><td align=right>$i_UserChineseName :&nbsp;</td><td ><input type=text name='chname_$record_id' value='$ch_name' size=10></td></tr>";
		$table .= "<tr><td align=right>$i_UserEnglishName :&nbsp;</td><td ><input type=text name='enname_$record_id' value='$en_name' size=20></td></tr>";
		$table .= "<tr><td align=right>$i_StudentGuardian_Occupation :&nbsp;</td><td ><input type=text name='occupation_$record_id' value='$occupation' size=20 maxlength=60></td></tr>";
		$table .= "<tr><td align=right>".$ec_iPortfolio['relation']." :&nbsp;</td><td>$select_relation</td></tr>";
		$table .= "<tr><td align=right>$i_StudentGuardian_Phone :&nbsp;</td><td ><input type=text name='phone_$record_id' value='$phone' size=10></td></tr>";
		$table .= "<tr><td align=right>$i_StudentGuardian_EMPhone :&nbsp;</td><td ><input type=text name='emphone_$record_id' value='$emphone' size=10></td></tr>";
		$table .= "<tr><td align=right>$i_StudentGuardian_LiveTogether :&nbsp;</td><td ><input type=radio name='liveTogether_$record_id' value='1'".($isLiveTogether==1?" CHECKED ":"")." id='liveTogether_".$record_id."_1'/><label for='liveTogether_".$record_id."_1'>".$i_general_yes."</label> <input type=radio name='liveTogether_$record_id' value='0'".($isLiveTogether!=1?" CHECKED ":"")." id='liveTogether_".$record_id."_0'/><label for='liveTogether_".$record_id."_0'>".$i_general_no."</label></td></tr>";

		$O_AreaCode_Select = "<select name='add_area_$record_id'>";
		if(!empty($i_StudentRegistry_AreaCode)){
			foreach($i_StudentRegistry_AreaCode as $v=>$d){
				$O_AreaCode_Select .= "<option value='$v' ".($add_areacode==$v?"selected ":"")."/>".$d;
			}
		}
		$O_AreaCode_Select .= "</select>";

		$table .= "<tr><td align=right>$i_UserAddress_Area :&nbsp;</td><td >$O_AreaCode_Select</td></tr>";
		$table .= "<tr><td align=right>$i_UserAddress_Road :&nbsp;</td><td ><input type=text name='add_road_$record_id' value='$add_road' size=20 maxlength='50' /></td></tr>";
		$table .= "<tr><td align=right>$i_UserAddress_Address :&nbsp;</td><td ><input type=text name='add_address_$record_id' value='$add_address' size=20 maxlength='50' /></td></tr>";
		$table .= "</table>";

		$other_table .= "<tr id='r_$record_id'>";
		$other_table .= "<td class='$css'>$table</td>";
		$other_table .= "<td class='$css'><input type=radio name='main[]' value='$record_id'".($isMain==1?" CHECKED ":"")."></td>";
		$other_table .= "<td class='$css'><input type=radio name='ec[]' value='$record_id'".($isEmergencyContact==1?" CHECKED ":"")."></td>";
		if($plugin['sms']){
			$other_table .= "<td class='$css'><input type=radio name='sms[]' value='$record_id'".($isSMS==1?" CHECKED ":"")."></td>";
		}
		$other_table .= "<td class='$css'><input type=checkbox name='Delete[]' value='$record_id' onClick=\"javascript:checkRemoveGuardian(this);\"></td>";
		$other_table .= "</tr>";
		$other_table .= "<input type='hidden' name='record_id[]' value='$record_id'>";
	}
	$other_table .= "</table>\n";
}
else{
	$other_table .= "</table>\n";
	if(!$GuardianExist){
		$other_table .= "<script language=javascript>\n";
		$other_table .= "addGuardian();\n";
		$other_table .= "</script>\n";
	}
}

?>
	<tr>
		<td align=right nowrap><?=$ec_guardian['01'] ?>:</td>
		<td><?=$father_table ?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$ec_guardian['02'] ?>:</td>
		<td><?=$mother_table ?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentGuardian_MainContent ?>:</td>
		<td><?=$MG_Select ?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentGuardian_EmergencyContact ?>:</td>
		<td><?=$EC_Select;?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$i_StudentGuardian_SMS ?>:</td>
		<td><?=$SMS_Select;?></td>
	</tr>
	<tr>
		<td align=right nowrap><?=$ec_guardian['08'] ?>:</td>
		<td><?=$other_table?><br /><input type="button" value=" + " onClick="addGuardian()" /></td>
	</tr>
</table>
</blockquote>

<input type=hidden name=UserID value="<?=$li->UserID ?>" />
<input type=hidden name=pageNo value="<?=$pageNo ?>" />
<input type=hidden name=order value="<?=$order ?>" />
<input type=hidden name=field value="<?=$field ?>" />
<input type=hidden name=TabID value="<?=$TabID ?>" />
<input type=hidden name=keyword value="<?=$keyword ?>" />
<input type=hidden name=ts value="<?=$ts ?>" />

<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."'>";
}
?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1 /></td>
	</tr>
	<tr>
		<td align="right">
			<a href='javascript:submitform(document.form1)'><img src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'></a>
			<a href='javascript:resetform(document.form1)'><img src='<?=$image_path?>/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
			<a href="javascript:back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
		</td>
	</tr>
</table>
</form>

<script type="text/javascript" defer="1">
	updateEnName(document.form1);
	updateCountryUI(document.form1);
	updateR_AreaUI(document.form1);
	updateMainGuardianUI(document.form1);
	updateEmergencyContactUI(document.form1);
<?php if($plugin['sms']) { ?>
	updateSMSUI(document.form1);
<?php } ?>
</script>

<?
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
