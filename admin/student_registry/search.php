<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
intranet_opendb();

$lclass = new libclass();
$sql =	"
					SELECT
						a.ClassName,
						b.LevelName,
						b.ClassLevelID
					FROM
						INTRANET_CLASS AS a
					LEFT OUTER JOIN
						INTRANET_CLASSLEVEL AS b
					ON
						a.ClassLevelID = b.ClassLevelID
					WHERE
						a.RecordStatus = 1
					ORDER BY
						b.ClassLevelID, a.ClassName
				";
$temp = $lclass->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($name,$level,$levelid)=$temp[$i];

	if($levelid=="")
		$no_level[] = $name;
	else{
		$result[$levelid]['name']=$level;
		$result[$levelid]['class'][]=$name;
	}
}

$table =	"
						<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
							<tr>
								<td class='tableTitle'><b>$i_ClassLevel</b></td>
								<td class='tableTitle'><B>$i_ClassName</b></td>
							</tr>
					";
if(sizeof($result)>0){
	foreach($result as $key =>$values){
		$level_name = $values['name'];
		$classes = $values['class'];
		
		$table .=	"<tr><td><input type=checkbox name='LevelID[]' value='$key' onClick='selectByClassLevel(this)' /> $level_name</td><td>";
		for($i=0;$i<sizeof($classes);$i++){
			$class_name = $classes[$i];
			$table .= "<input type=checkbox name='ClassID[]' value='$class_name' id='ClassID_".$key."_".$i."' /> $class_name&nbsp;&nbsp;";
		}
		$table .= "<input type=hidden name='$key' value='$i'>";
		$table .= "</td></tr>";
	}
}
if(sizeof($no_level)>0){
	$table .= "<tr><td>&nbsp;</td><td>";
	for($i=0;$i<sizeof($no_level);$i++){
		$name = $no_level[$i];
		$table .= "<input type=checkbox name='ClassID[]' value='$name' /> $name";
	}
	$table .= "</td></tr>";
}
$table .= "</table>";

?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentRegistry['System'], 'index.php',$button_search,'') ?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>

function selectByClassLevel(obj){
	classid = obj.value;
	check = obj.checked;
	objLevels = document.getElementsByName(classid);
	objLevel = objLevels[0];
	no_of_class = objLevel.value;
		
	for(i=0;i<no_of_class;i++){
		obj = document.getElementById('ClassID_'+classid+'_'+i);
		if(typeof(obj)!='undefined')
			obj.checked = check;
	}
}

function checkform(obj){
	classes = document.getElementsByName('ClassID[]');
	for(i=0;i<classes.length;i++){
		if(classes[i].checked) return true;
	}
	alert("<?=$i_StudentGuardian_warning_PleaseSelectClass?>");
	return false;
}

function selectAll(obj){
	classes = document.getElementsByName('ClassID[]');
	levels = document.getElementsByName('LevelID[]');
	check = obj.checked;
	for(i=0;i<classes.length;i++){
		classes[i].checked = check;
	}
	for(i=0;i<levels.length;i++){
		levels[i].checked = check;
	}
}

</script>

<form name=form1 method='get' action='search_result.php' onsubmit='return checkform(this)'>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
	<tr>
		<td><input type=checkbox name='all' value=1 onClick='selectAll(this)' /> <?=$i_StudentGuardian_all_students?></td>
	</tr>
	<tr>
		<td><?=$table?></td>
	</tr>
	<tr>
		<td>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr>
		<td><b><u><?=$i_StudentGuardian_Additional_Searching_Criteria?>:</u></b></td>
	</tr>
	<tr>
		<td><?=$i_UserName." / ".$i_UserLogin." / ".$i_ClassNameNumber?>:&nbsp;<input type=text name='keyword' /></td>
	</tr>
	<tr>
		<td><?=$i_StudentRegistry_ModifiedSince?>:&nbsp;<input type=text name='ts' maxlength=19 /> <span class='extraInfo'>(YYYY-MM-DD [HH:II:SS])</span></td>
	</tr>
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1 /></td>
	</tr>
	<tr>
		<td align="right">
			<input type="image" src="<?=$image_path?>/admin/button/s_btn_find_<?=$intranet_session_language?>.gif" border='0' />
			<a href='javascript:document.form1.reset()'><img src='<?=$image_path?>/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
		</td>
	</tr>
</table>

</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
