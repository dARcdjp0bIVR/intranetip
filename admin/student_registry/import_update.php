<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser_ext.php");
//include_once("../../includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");
intranet_opendb();

//error_reporting(E_ALL);
//var_dump($_POST);

$li = new libdb();

$RecordCount = isset($_POST['UserID'])? count($_POST['UserID']) : 0;

$result = array();//debug use
$executed_sql = array();//debug use
$tmp_msg = '';

/*
workflow
//student part
update eIU
update eIU_ext_1
//guardian part
update iGS
update iGS_ext_1
*/
$delete_record_ids = array();
$error_msg="";

for($i=0; $i<$RecordCount; $i++){
	$UserID = intranet_htmlspecialchars(trim($_POST['UserID'][$i]));

	# check if iportfolio activated
	$iportfolio_activated = false;
	if($plugin['iPortfolio']){
		$lportfolio = new portfolio();
		$data = $lportfolio->returnAllActivatedStudent();
		for($j=0;$j<sizeof($data);$j++){
			if($UserID==$data[$j][0]){
				$iportfolio_activated = true;
				break;
			}
		}
	}
	# eIU
	# used websamsregno for UserID...so...no need to update 

	$EnglishName = intranet_htmlspecialchars(trim($_POST['EnglishName'][$i]));
	$ChineseName = intranet_htmlspecialchars(trim($_POST['ChineseName'][$i]));
	$ClassNumber = intranet_htmlspecialchars(trim($_POST['ClassNumber'][$i]));
	$ClassName = intranet_htmlspecialchars(trim($_POST['ClassName'][$i]));
	$Gender = intranet_htmlspecialchars(trim($_POST['Gender'][$i]));
	$DateOfBirth = intranet_htmlspecialchars(trim($_POST['DateOfBirth'][$i]));
	$HomeTelNo = intranet_htmlspecialchars(trim($_POST['HomeTelNo'][$i]));

	# eIU_ext_1
	$AuthCodeMain = intranet_htmlspecialchars(trim($_POST['AuthCodeMain'][$i]));
	$AuthCodeCheck = intranet_htmlspecialchars(trim($_POST['AuthCodeCheck'][$i]));
	$PlaceOfBirthCode = intranet_htmlspecialchars(trim($_POST['PlaceOfBirthCode'][$i]));
	$ID_Type = intranet_htmlspecialchars(trim($_POST['ID_Type'][$i]));
	$ID_Num = intranet_htmlspecialchars(trim($_POST['ID_Num'][$i]));
	$ID_IssuePlace = intranet_htmlspecialchars(trim($_POST['ID_IssuePlace'][$i]));
	$ID_IssueDate = intranet_htmlspecialchars(trim($_POST['ID_IssueDate'][$i]));
	$ID_ValidDate = intranet_htmlspecialchars(trim($_POST['ID_ValidDate'][$i]));
	$SP_Type = intranet_htmlspecialchars(trim($_POST['SP_Type'][$i]));
	//$SP_Num = intranet_htmlspecialchars(trim($_POST['SP_Num'][$i]));
	$SP_IssueDate = intranet_htmlspecialchars(trim($_POST['SP_IssueDate'][$i]));
	$SP_ValidDate = intranet_htmlspecialchars(trim($_POST['SP_ValidDate'][$i]));
	$Province = intranet_htmlspecialchars(trim($_POST['Province'][$i]));

	$R_AreaCode = intranet_htmlspecialchars(trim($_POST['R_AreaCode'][$i]));
	$R_AreaText = isset($_POST['R_AreaText']) ? intranet_htmlspecialchars(trim($_POST['R_AreaText'][$i])) : '';

	# eIU_ext_1 + special handle to eIU.Address
	$AreaCode = intranet_htmlspecialchars(trim($_POST['AreaCode'][$i]));
	$Road = intranet_htmlspecialchars(trim($_POST['Road'][$i]));
	$Address = intranet_htmlspecialchars(trim($_POST['Address'][$i]));
	# single line eIU_Address
	$eIU_Address = '';
	if($AreaCode != 'O'){
		$eIU_Address .= $i_StudentRegistry_AreaCode[$AreaCode];
	}
	if($eIU_Address!='' && $Road!=''){
		$eIU_Address .= ', ';
	}
	$eIU_Address .= $Road;
	if($eIU_Address!='' && $Address!=''){
		$eIU_Address .= ', ';
	}
	$eIU_Address .= $Address;
	$CountryCode = intranet_htmlspecialchars(trim($_POST['CountryCode'][$i]));
	if($CountryCode=='0'){
        	$eIU_Country = $CountryText;
	}else{
        	$eIU_Country = $i_StudentRegistry_Country_Code[$CountryCode];
	}

	$lu = new libuser($UserID);
	$lu_ext = new libuser_ext();

	/**************************************************
	Student part
	***************************************************/
  $fieldname = "EnglishName = '$EnglishName', ";
  $fieldname .= "ChineseName = '$ChineseName', ";

	$fieldname .= "Gender = '$Gender', ";
	$fieldname .= "DateOfBirth = '$DateOfBirth', ";
	$fieldname .= $HomeTelNo != ""?"HomeTelNo = '$HomeTelNo', ":"";
	$fieldname .= $eIU_Address != ""?"Address = '$eIU_Address', ":"";

	$fieldname .= "Country = '$eIU_Country', ";
	$fieldname .= $ClassNumber != ""?"ClassNumber = '$ClassNumber', ":"";
	$fieldname .= "ClassName = '$ClassName', ";
	
	$fieldname .= "DateModified = now()";

	$sql =	"
						UPDATE
							INTRANET_USER
						SET
							$fieldname
						WHERE
							UserID = $UserID
					";

	$result['eIU_u_'.$i] = $li->db_db_query($sql);
	if(mysql_error()){
		$tmp_msg .= mysql_error();
	}
	$executed_sql['eIU_u_'.$i] = $sql;

	if($AuthCodeMain!="" && $AuthCodeCheck!=""){
		$ac_UserID = $lu_ext->getUserIDByAuthCode($AuthCodeMain, $AuthCodeCheck);
		if($ac_UserID && $ac_UserID!=$UserID){
			$error_msg .= "$i_StudentRegistry_AuthCodeMain Error ( $i_Form_FormNameMustBeUnique )";
		}
	}

	# IU_ext_1 sql
	$eIU_ext_1_field_values = array();
	$eIU_ext_1_field_values['UserID'] = $UserID;
	$eIU_ext_1_field_values['AuthCodeMain'] = $AuthCodeMain;
	$eIU_ext_1_field_values['AuthCodeCheck'] = $AuthCodeCheck;
	$eIU_ext_1_field_values['PlaceOfBirthCode'] = $PlaceOfBirthCode;
	$eIU_ext_1_field_values['ID_Type'] = $ID_Type;
	$eIU_ext_1_field_values['ID_Num'] = $ID_Num;
	$eIU_ext_1_field_values['ID_IssuePlace'] = $ID_IssuePlace;
	$eIU_ext_1_field_values['ID_IssueDate'] = $ID_IssueDate;
	$eIU_ext_1_field_values['ID_ValidDate'] = $ID_ValidDate;
	$eIU_ext_1_field_values['SP_Type'] = $SP_Type;
//$eIU_ext_1_field_values['SP_Num'] = $SP_Num;
	$eIU_ext_1_field_values['SP_IssueDate'] = $SP_IssueDate;
	$eIU_ext_1_field_values['SP_ValidDate'] = $SP_ValidDate;
	$eIU_ext_1_field_values['CountryCode'] = $CountryCode;
//$eIU_ext_1_field_values['CountryText'] = $CountryText;
	$eIU_ext_1_field_values['Province'] = $Province;
	$eIU_ext_1_field_values['R_AreaCode'] = $R_AreaCode;
	$eIU_ext_1_field_values['R_AreaText'] = $R_AreaText;
	$eIU_ext_1_field_values['AreaCode'] = $AreaCode;
	$eIU_ext_1_field_values['Road'] = $Road;
	$eIU_ext_1_field_values['Address'] = $Address;

	$ExtendRecordID = $lu_ext->getExtendRecordIDByUserID($UserID);

	if($ExtendRecordID){
		# update
		$sql = "UPDATE INTRANET_USER_EXT_1 SET ";
		foreach($eIU_ext_1_field_values as $k => $v){
			$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
		}
		$sql .= " DateModified=NOW() WHERE ExtendRecordID = '$ExtendRecordID'";
	}
	else{
		$field_sql = '';
		$value_sql = '';
	
		foreach($eIU_ext_1_field_values as $k => $v){
			$field_sql .= "$k, ";
			$value_sql .= ($v==''?"NULL":"'$v'").", ";
		}
		# insert
		$sql =	"
							INSERT INTO
								INTRANET_USER_EXT_1
									($field_sql DateInput, DateModified)
							VALUES
								($value_sql NOW(), NOW())
						";
	}
	# update or create?
	$result['eIU_ext_1_'.$i] = $li->db_db_query($sql);
	if(mysql_error()){
		$tmp_msg .= mysql_error();
	}
	$executed_sql['eIU_ext_1_'.$i] = $sql;
	/******************************************
	Guardian Part
	*******************************************/
	# insert / update / Delete Father
	# insert / update / Delete Mother
	# insert / update other
	# record_id for existing


	$Guardian_Type = intranet_htmlspecialchars(trim($_POST['GuardianType'][$i]));
	$Guardian_IsLiveTogether = (int)intranet_htmlspecialchars(trim($_POST['GuardianIsLiveTogether'][$i]));

	$OM_GuardianName = intranet_htmlspecialchars(trim($_POST['OM_GuardianName'][$i]));
	$OM_Guardian_Relation = intranet_htmlspecialchars(trim($_POST['OM_Guardian_Relation'][$i]));
	$OM_Guardian_Occupation = intranet_htmlspecialchars(trim($_POST['OM_Guardian_Occupation'][$i]));

	$Guardian_AreaCode = intranet_htmlspecialchars(trim($_POST['Guardian_AreaCode'][$i]));
	$Guardian_Road = intranet_htmlspecialchars(trim($_POST['Guardian_Road'][$i]));
	$Guardian_Address = intranet_htmlspecialchars(trim($_POST['Guardian_Address'][$i]));
	$Guardian_Tel = intranet_htmlspecialchars(trim($_POST['Guardian_Tel'][$i]));
	$Guardian_Mobile = intranet_htmlspecialchars(trim($_POST['Guardian_Mobile'][$i]));

	$EmergencyContact_Name = intranet_htmlspecialchars(trim($_POST['EmergencyContactName'][$i]));

	$EmergencyContact_Relation = intranet_htmlspecialchars(trim($_POST['EmergencyContact_Relation'][$i]));
	$EmergencyContact_Tel = intranet_htmlspecialchars(trim($_POST['EmergencyContact_Tel'][$i]));
	$EmergencyContact_AreaCode = intranet_htmlspecialchars(trim($_POST['EmergencyContact_AreaCode'][$i]));
	$EmergencyContact_Road = intranet_htmlspecialchars(trim($_POST['EmergencyContact_Road'][$i]));
	$EmergencyContact_Address = intranet_htmlspecialchars(trim($_POST['EmergencyContact_Address'][$i]));

	$sql_gs =	"
							SELECT
								RecordID,
								Relation,
								ChName
							FROM
								$eclass_db.GUARDIAN_STUDENT
							WHERE
								UserID = $UserID
						";
	$r = $li->returnArray($sql_gs);

	$F_record_id = '';
	$M_record_id = '';
	$Other_G_records = array();
	for($j=0; $j<count($r);$j++){
		if(((int)$r[$j]['Relation']) == 1){
			$F_record_id = $r[$j]['RecordID'];
		}
		else if(((int)$r[$j]['Relation']) == 2){
			$M_record_id = $r[$j]['RecordID'];
		}
		else{
			$Other_G_records[] = $r[$j];
		}
	}
	$F_chname = intranet_htmlspecialchars(trim($_POST['FatherName'][$i]));
	$F_occupation = intranet_htmlspecialchars(trim($_POST['FatherOccupation'][$i]));
	if($F_chname!=''){
		$iGS_field_values = array();
		$iGS_ext_1_field_values = array();

		$F_Address_Available = false;
		if($Guardian_Type == 'F'){
			if( $Guardian_IsLiveTogether==0 || ($Guardian_IsLiveTogether==1 && $Guardian_AreaCode!='') ){
				$F_add_area = $Guardian_AreaCode;
				$F_add_road = $Guardian_Road;
				$F_add_address = $Guardian_Address;
				$F_Address_Available = true;
			}
			if($Guardian_Tel!==''){
				$iGS_field_values['Phone'] = $Guardian_Tel;
			}
			if($Guardian_Mobile!==''){
				$iGS_field_values['EmPhone'] = $Guardian_Mobile;
			}
		}
		else if(((int)$EmergencyContact_Relation)==1){
			$F_add_area = $EmergencyContact_AreaCode;
			$F_add_road = $EmergencyContact_Road;
			$F_add_address = $EmergencyContact_Address;
			$F_Address_Available = true;
			if($EmergencyContact_Tel!==''){
				$iGS_field_values['EmPhone'] = $EmergencyContact_Tel;
			}
		}
		
		if($F_Address_Available){
			if($F_add_area == "" || $F_add_area == "O"){
				$F_address = "";
			}
			else{
				$F_address = $i_StudentRegistry_AreaCode[$F_add_area]."\n";
			}
			$F_address .= trim($F_add_road)."\n";
			$F_address .= trim($F_add_address);

			$iGS_field_values['Address'] = $F_address;

			$iGS_ext_1_field_values['AreaCode'] = $F_add_area;
			$iGS_ext_1_field_values['Road'] = $F_add_road;
			$iGS_ext_1_field_values['Address'] = $F_add_address;
		}

		$iGS_field_values['UserID'] = $UserID;
		$iGS_field_values['ChName'] = $F_chname;
		$iGS_field_values['Relation'] = '01';
		$iGS_field_values['IsMain'] = $Guardian_Type == 'F' ?  "1" : "0";

		$iGS_ext_1_field_values['UserID'] = $UserID;
		if($Guardian_Type == 'F'){
			$iGS_ext_1_field_values['IsLiveTogether'] = $Guardian_IsLiveTogether;
		}
		$iGS_ext_1_field_values['IsEmergencyContact'] = (((int)$EmergencyContact_Relation)==1)? "1": "0";
		$iGS_ext_1_field_values['Occupation'] = $F_occupation;

		# update/ insert the GS
		if($F_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET ";
			foreach($iGS_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE RecordID = '$F_record_id'";
			$result['iGS_R01_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_R01_u_'.$i] = $sql;
			$sql = "SELECT ExtendRecordID 
				FROM $eclass_db.GUARDIAN_STUDENT_EXT_1
				WHERE RecordID = '$F_record_id'";
			$r = $li->returnArray($sql,39);
			$F_gs_ext_record_id = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID']:'';
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_R01_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_R01_i_'.$i] = $sql;
			$F_record_id = mysql_insert_id();
			$F_gs_ext_record_id = '';
		}
		$iGS_ext_1_field_values['RecordID'] = $F_record_id;
		if($F_gs_ext_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET ";
			foreach($iGS_ext_1_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE ExtendRecordID = '$F_gs_ext_record_id'";
			$result['iGS_ext_1_R01_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_R01_u_'.$i] = $sql;
		}else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_ext_1_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT_EXT_1
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_ext_1_R01_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_R01_i_'.$i] = $sql;
		}
	}
	else{
		# delete or ignore if no record ahead
		if($F_record_id){
			# delete, later in batch
			$delete_record_ids[] = $F_record_id;
		}
		else{
		 # ignore
		}
	}

	$M_chname = intranet_htmlspecialchars(trim($_POST['MotherName'][$i]));
	$M_occupation = intranet_htmlspecialchars(trim($_POST['MotherOccupation'][$i]));

	if($M_chname!=''){
		$iGS_field_values = array();
		$iGS_ext_1_field_values = array();

		$M_Address_Available = false;
		if($Guardian_Type == 'M'){
			if( $Guardian_IsLiveTogether==0 || ($Guardian_IsLiveTogether==1 && $Guardian_AreaCode!='') ){
				$M_add_area = $Guardian_AreaCode;
				$M_add_road = $Guardian_Road;
				$M_add_address = $Guardian_Address;
				$M_Address_Available = true;
			}
			if($Guardian_Tel!==''){
				$iGS_field_values['Phone'] = $Guardian_Tel;
			}
			if($Guardian_Mobile!==''){
				$iGS_field_values['EmPhone'] = $Guardian_Mobile;
			}
		}
		else if(((int)$EmergencyContact_Relation)==2){
			$M_add_area = $EmergencyContact_AreaCode;
			$M_add_road = $EmergencyContact_Road;
			$M_add_address = $EmergencyContact_Address;
			$M_Address_Available = true;
			if($EmergencyContact_Tel!==''){
				$iGS_field_values['EmPhone'] = $EmergencyContact_Tel;
			}
		}
		
		if($M_Address_Available){
			if($M_add_area == "" || $M_add_area == "O"){
				$M_address = "";
			}
			else{
				$M_address = $i_StudentRegistry_AreaCode[$M_add_area]."\n";
			}
			$M_address .= trim($M_add_road)."\n";
			$M_address .= trim($M_add_address);

			$iGS_field_values['Address'] = $M_address;

			$iGS_ext_1_field_values['AreaCode'] = $M_add_area;
			$iGS_ext_1_field_values['Road'] = $M_add_road;
			$iGS_ext_1_field_values['Address'] = $M_add_address;
		}

		$iGS_field_values['UserID'] = $UserID;
		$iGS_field_values['ChName'] = $M_chname;
		$iGS_field_values['Relation'] = '02';
		$iGS_field_values['IsMain'] = $Guardian_Type == 'M' ?  "1" : "0";

		$iGS_ext_1_field_values['UserID'] = $UserID;
		if($Guardian_Type == 'F'){
			$iGS_ext_1_field_values['IsLiveTogether'] = $Guardian_IsLiveTogether;
		}
		$iGS_ext_1_field_values['IsEmergencyContact'] = (((int)$EmergencyContact_Relation)==2)? "1": "0";
		$iGS_ext_1_field_values['Occupation'] = $M_occupation;

		# update/ insert the GS
		if($M_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET ";
			foreach($iGS_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE RecordID = '$M_record_id'";
			$result['iGS_R02_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_R02_u_'.$i] = $sql;
			$sql =	"
								SELECT
									ExtendRecordID
								FROM
									$eclass_db.GUARDIAN_STUDENT_EXT_1
								WHERE
									RecordID = '$M_record_id'
							";
			$r = $li->returnArray($sql,39);
			$M_gs_ext_record_id = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID']:'';
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_R02_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_R02_i_'.$i] = $sql;
			$M_record_id = mysql_insert_id();
			$M_gs_ext_record_id = '';
		}
		$iGS_ext_1_field_values['RecordID'] = $M_record_id;
		if($M_gs_ext_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET ";
			foreach($iGS_ext_1_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE ExtendRecordID = '$M_gs_ext_record_id'";
			$result['iGS_ext_1_R02_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_R02_u_'.$i] = $sql;
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_ext_1_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT_EXT_1
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_ext_1_R02_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_R02_i_'.$i] = $sql;
		}
	}
	else{
		# delete or ignore if no record ahead
		if($M_record_id){
			# delete, later in batch
			$delete_record_ids[] = $M_record_id;
		}
		else{
			# ignore
		}
	}

	$OM_EQUAL_EC = false;
	if($OM_GuardianName !== '' && ((int)$OM_Guardian_Relation)!= 1 && ((int)$OM_Guardian_Relation)!= 2 ){
		# extra gs
		$OM_EQUAL_EC = $OM_GuardianName == $EmergencyContact_Name;
		$iGS_field_values = array();
		$iGS_ext_1_field_values = array();

		$OM_add_area = $Guardian_AreaCode;
		$OM_add_road = $Guardian_Road;
		$OM_add_address = $Guardian_Address;

		$iGS_field_values['Phone'] = $Guardian_Tel;
		$iGS_field_values['EmPhone'] = $Guardian_Mobile;
		
		if($OM_add_area == "" || $OM_add_area == "O"){
			$OM_address = "";
		}
		else{
			$OM_address = $i_StudentRegistry_AreaCode[$OM_add_area]."\n";
		}
		$OM_address .= trim($OM_add_road)."\n";
		$OM_address .= trim($OM_add_address);

		$iGS_field_values['Address'] = $OM_address;

		$iGS_ext_1_field_values['AreaCode'] = $OM_add_area;
		$iGS_ext_1_field_values['Road'] = $OM_add_road;
		$iGS_ext_1_field_values['Address'] = $OM_add_address;

		$iGS_field_values['UserID'] = $UserID;
		$iGS_field_values['ChName'] = $OM_GuardianName;
		$iGS_field_values['Relation'] = $OM_Guardian_Relation;

		if($Guardian_Type == 'O'){
			$iGS_field_values['IsMain'] =  "1";
			# clear any other IsMain Value
			$sql =	"
								UPDATE
									$eclass_db.GUARDIAN_STUDENT
								SET
									IsMain = 0 ,
									ModifiedDate = NOW()
								WHERE
									UserID = '$UserID' AND
									IsMain = 1
							";
			$result['iGS_G_clear_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_G_clear_'.$i] = $sql;
		}
		else{
			$iGS_field_values['IsMain'] =  "0";
		}

		$iGS_ext_1_field_values['UserID'] = $UserID;
		if($Guardian_Type == 'O'){
			$iGS_ext_1_field_values['IsLiveTogether'] = $Guardian_IsLiveTogether;
		}
		$iGS_ext_1_field_values['IsEmergencyContact'] = $OM_EQUAL_EC ? "1": "0";
		$iGS_ext_1_field_values['Occupation'] = $OM_Guardian_Occupation;

		$OM_record_id = '';
		for($j=0;$j<count($Other_G_records);$j++){
			if(addslashes($Other_G_records[$j]['ChName']) == $OM_GuardianName){
				$OM_record_id = $Other_G_records[$j]['RecordID'];
				break;
			}
		}

		# update/ insert the GS
		if($OM_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET ";
			foreach($iGS_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE RecordID = '$OM_record_id'";
			$result['iGS_G_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_G_u_'.$i] = $sql;
			$sql =	"
								SELECT
									ExtendRecordID
								FROM
									$eclass_db.GUARDIAN_STUDENT_EXT_1
								WHERE
									RecordID = '$OM_record_id'
							";
			$r = $li->returnArray($sql,39);
			$OM_gs_ext_record_id = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID']:'';
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_G_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_G_i_'.$i] = $sql;
			$OM_record_id = mysql_insert_id();
			$OM_gs_ext_record_id = '';
		}
		$iGS_ext_1_field_values['RecordID'] = $OM_record_id;
		if($OM_gs_ext_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET ";
			foreach($iGS_ext_1_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE ExtendRecordID = '$OM_gs_ext_record_id'";
			$result['iGS_ext_1_G_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_G_u_'.$i] = $sql;
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_ext_1_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT_EXT_1
										($field_sql InputDate, ModifiedDate)
								VALUES($value_sql NOW(), NOW())
							";
			$result['iGS_ext_1_G_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_G_i_'.$i] = $sql;
		}
	}
	else{
		# no del machanism here
	}

	if(	$EmergencyContact_Name !== '' && !$OM_EQUAL_EC &&
			((int)$EmergencyContact_Relation) != 1 && ((int)$EmergencyContact_Relation) != 2){
		# extra gs
		$iGS_field_values = array();
		$iGS_ext_1_field_values = array();

		$EC_add_area = $EmergencyContact_AreaCode;
		$EC_add_road = $EmergencyContact_Road;
		$EC_add_address = $EmergencyContact_Address;

		$iGS_field_values['EmPhone'] = $EmergencyContact_Tel;
		
		if($EC_add_area == "" || $EC_add_area == "O"){
			$EC_address = "";
		}
		else{
			$EC_address = $i_StudentRegistry_AreaCode[$EC_add_area]."\n";
		}
		$EC_address .= trim($EC_add_road)."\n";
		$EC_address .= trim($EC_add_address);

		$iGS_field_values['Address'] = $EC_address;

		$iGS_ext_1_field_values['AreaCode'] = $EC_add_area;
		$iGS_ext_1_field_values['Road'] = $EC_add_road;
		$iGS_ext_1_field_values['Address'] = $EC_add_address;

		$iGS_field_values['UserID'] = $UserID;
		$iGS_field_values['ChName'] = $EmergencyContact_Name;
		$iGS_field_values['Relation'] = $EmergencyContact_Relation;

		# clear any other IsEC Value
		$sql =	"
							UPDATE
								$eclass_db.GUARDIAN_STUDENT
							SET
								IsEmergencyContact = 0,
								ModifiedDate = NOW()
							WHERE
								UserID = '$UserID' AND
								IsEmergencyContact = 1
						";
		$result['iGS_EC_clear_'.$i] = $li->db_db_query($sql);
		if(mysql_error()){
			$tmp_msg .= mysql_error();
		}
		$executed_sql['iGS_EC_clear_'.$i] = $sql;

		$iGS_field_values['IsMain'] =  "0";
		$iGS_ext_1_field_values['IsEmergencyContact'] = "1";
		$iGS_ext_1_field_values['UserID'] = $UserID;

		$EC_record_id = '';
		for($j=0;$j<count($Other_G_records);$j++){
			if(addslashes($Other_G_records[$j]['ChName']) == $EmergencyContact_Name){
				$EC_record_id = $Other_G_records[$j]['RecordID'];
				break;
			}
		}

		# update/ insert the GS
		if($EC_record_id){
			//update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET ";
			foreach($iGS_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE RecordID = '$EC_record_id'";
			$result['iGS_EC_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_EC_u_'.$i] = $sql;
			$sql =	"
								SELECT
									ExtendRecordID
								FROM
									$eclass_db.GUARDIAN_STUDENT_EXT_1
								WHERE
									RecordID = '$EC_record_id'
							";
			$r = $li->returnArray($sql,39);
			$EC_gs_ext_record_id = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID']:'';
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_EC_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_EC_i_'.$i] = $sql;
			$EC_record_id = mysql_insert_id();
			$EC_gs_ext_record_id = '';
		}
		$iGS_ext_1_field_values['RecordID'] = $EC_record_id;
		if($EC_gs_ext_record_id){
			# update
			$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET ";
			foreach($iGS_ext_1_field_values as $k => $v){
				$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
			}
			$sql .= " ModifiedDate = NOW() WHERE ExtendRecordID = '$EC_gs_ext_record_id'";
			$result['iGS_ext_1_EC_u_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_EC_u_'.$i] = $sql;
		}
		else{
			# insert
			$field_sql = '';
			$value_sql = '';
	
			foreach($iGS_ext_1_field_values as $k => $v){
				$field_sql .= "$k, ";
				$value_sql .= ($v==''?"NULL":"'$v'").", ";
			}
			$sql =	"
								INSERT INTO
									$eclass_db.GUARDIAN_STUDENT_EXT_1
										($field_sql InputDate, ModifiedDate)
								VALUES
									($value_sql NOW(), NOW())
							";
			$result['iGS_ext_1_EC_i_'.$i] = $li->db_db_query($sql);
			if(mysql_error()){
				$tmp_msg .= mysql_error();
			}
			$executed_sql['iGS_ext_1_EC_i_'.$i] = $sql;
		}
	}
	else{
		# no del machanism here
	}
}
//var_dump($tmp_msg);
//var_dump($result);
//var_dump($executed_sql);
//die;

### Delete
if(sizeof($delete_record_ids)>0){
	$deletedids = implode(",",$delete_record_ids);
	$sql =	"
						DELETE FROM
							$eclass_db.GUARDIAN_STUDENT
						WHERE
							RecordID IN ($deletedids)
					";
	$result['Remove_iGS'] = $li->db_db_query($sql);

	$sql =	"
						DELETE FROM
							$eclass_db.GUARDIAN_STUDENT_EXT_1
						WHERE
							RecordID IN ($deletedids)
					";
	$result['Remove_iGS_ext_1'] = $li->db_db_query($sql);
}

$url = "import_xml.php?msg=2";

intranet_closedb();
header("Location: $url");
?>
