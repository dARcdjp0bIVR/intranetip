<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libuser_ext.php");
//include_once("../../includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");
intranet_opendb();

//error_reporting(E_ALL);
//var_dump($_POST);

$ClassID = $_POST['ClassID'];
$UserID = $_POST['UserID'];

$result = array();	# debug use
$executed_sql = array();	# debug use
//check if need to update iPS_Nationality
/*
bring to here by POST

TabID
*/
/*
workflow
//student part
update eIU
update eIU_ext_1
//guardian part
update iGS
update iGS_ext_1
*/

# check if iportfolio activated
$iportfolio_activated = false;
if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();
	if($TabID==2){
		for($i=0;$i<sizeof($data);$i++){
			if($UserID==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}
	}
}

$li = new libdb();
$lu = new libuser($UserID);
$lu_ext = new libuser_ext();
//$lc = new libeclass();

/**************************************************
Student part
***************************************************/

# eIU
$WebSamsRegNo = intranet_htmlspecialchars(trim($WebSamsRegNo));
$EnglishName = intranet_htmlspecialchars(trim($EnglishName));
$ChineseName = intranet_htmlspecialchars(trim($ChineseName));
$ClassNumber = intranet_htmlspecialchars(trim($ClassNumber));
$ClassName = intranet_htmlspecialchars(trim($ClassName));
$Gender = intranet_htmlspecialchars(trim($Gender));
$DateOfBirth = intranet_htmlspecialchars(trim($DateOfBirth));
$HomeTelNo = intranet_htmlspecialchars(trim($HomeTelNo));

# eIU_ext_1
$AuthCodeMain = intranet_htmlspecialchars(trim($_POST['AuthCodeMain']));
$AuthCodeCheck = intranet_htmlspecialchars(trim($_POST['AuthCodeCheck']));
$PlaceOfBirthCode = intranet_htmlspecialchars(trim($_POST['PlaceOfBirthCode']));
$ID_Type = intranet_htmlspecialchars(trim($_POST['ID_Type']));
$ID_Num = intranet_htmlspecialchars(trim($_POST['ID_Num']));
$ID_IssuePlace = intranet_htmlspecialchars(trim($_POST['ID_IssuePlace']));
$ID_IssueDate = intranet_htmlspecialchars(trim($_POST['ID_IssueDate']));
$ID_ValidDate = intranet_htmlspecialchars(trim($_POST['ID_ValidDate']));
$SP_Type = intranet_htmlspecialchars(trim($_POST['SP_Type']));
$SP_Num = intranet_htmlspecialchars(trim($_POST['SP_Num']));
$SP_IssueDate = intranet_htmlspecialchars(trim($_POST['SP_IssueDate']));
$SP_ValidDate = intranet_htmlspecialchars(trim($_POST['SP_ValidDate']));
$Province = intranet_htmlspecialchars(trim($_POST['Province']));

$R_AreaCode = intranet_htmlspecialchars(trim($_POST['R_AreaCode']));
$R_AreaText = isset($_POST['R_AreaText']) ? intranet_htmlspecialchars(trim($_POST['R_AreaText'])) : '';

# eIU_ext_1 + special handle to eIU.Address
$AreaCode = intranet_htmlspecialchars(trim($_POST['AreaCode']));
$Road = intranet_htmlspecialchars(trim($_POST['Road']));
$Address = intranet_htmlspecialchars(trim($_POST['Address']));
# single line eIU_Address
$eIU_Address = '';
if($AreaCode != 'O'){
	$eIU_Address .= $i_StudentRegistry_AreaCode[$AreaCode];
}
if($eIU_Address!='' && $Road!=''){
	$eIU_Address .= ', ';
}
$eIU_Address .= $Road;
if($eIU_Address!='' && $Address!=''){
	$eIU_Address .= ', ';
}
$eIU_Address .= $Address;

# eIU / eIU_ext_1  special handle
$CountryCode = intranet_htmlspecialchars(trim($_POST['CountryCode']));

if($CountryCode=='0'){
	$CountryText = intranet_htmlspecialchars(trim($_POST['CountryText']));
	$eIU_Country = $CountryText;
}
else{
	$eIU_Country = $i_StudentRegistry_Country_Code[$CountryCode];
}

# eIU sql
$fieldname  = "EnglishName = '$EnglishName', ";
$fieldname .= "ChineseName = '$ChineseName', ";

$fieldname .= "Gender = '$Gender', ";
$fieldname .= "DateOfBirth = '$DateOfBirth', ";
$fieldname .= "HomeTelNo = '$HomeTelNo', ";
$fieldname .= "Address = '$eIU_Address', ";

$fieldname .= "Country = '$eIU_Country', ";

$fieldname .= "ClassNumber = '$ClassNumber', ";
$fieldname .= "ClassName = '$ClassName', ";

$error_msg = "";

if ($TabID == 2){
	if($WebSamsRegNo!="" && substr(trim($WebSamsRegNo),0,1)!="#"){
		$WebSamsRegNo = "#".$WebSamsRegNo;
	}
//	if(!$iportfolio_activated){
		### check if websams regno exists ####
	if($WebSamsRegNo!=""){
		$sql =	"
							SELECT
								UserID
							FROM
								INTRANET_USER
							WHERE
								WebSamsRegNo = '".$WebSamsRegNo."' AND
								UserID != $UserID AND
								RecordType = 2
						";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)!=0){ ## WebSamsRegNo exists
			$error_msg .= " WebSAMS Registration Number has been used by other user.";
		}
	}
	$fieldname .= "WebSAMSRegNo = '$WebSamsRegNo',";
//	}

	if($AuthCodeMain != "" && $AuthCodeCheck != ""){
		$ac_UserID = $lu_ext->getUserIDByAuthCode($AuthCodeMain, $AuthCodeCheck);
		if($ac_UserID && $ac_UserID!=$UserID){
			$error_msg .= "$i_StudentRegistry_AuthCodeMain Error ( $i_Form_FormNameMustBeUnique )";
		}
	}
}
if($error_msg!=""){
	include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
	echo " <br />\n $error_msg \n";
	echo	"
					<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
						<tr>
							<td><hr size=1></td>
						</tr>
						<tr>
							<td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td>
						</tr>
					</table>
				";
	include_once($PATH_WRT_ROOT."templates/adminfooter.php");
	exit();
}
$fieldname .= "DateModified = now()";
$sql =	"
					UPDATE
						INTRANET_USER
					SET
						$fieldname
					WHERE
						UserID = $UserID
				";
//echo $sql;die;
$result['eIU_u'] = $li->db_db_query($sql);
$executed_sql['eIU_u'] = $sql;


# IU_ext_1 sql
$eIU_ext_1_field_values = array();
$eIU_ext_1_field_values['UserID'] = $UserID;
$eIU_ext_1_field_values['AuthCodeMain'] = $AuthCodeMain;
$eIU_ext_1_field_values['AuthCodeCheck'] = $AuthCodeCheck;
$eIU_ext_1_field_values['PlaceOfBirthCode'] = $PlaceOfBirthCode;
$eIU_ext_1_field_values['ID_Type'] = $ID_Type;
$eIU_ext_1_field_values['ID_Num'] = $ID_Num;
$eIU_ext_1_field_values['ID_IssuePlace'] = $ID_IssuePlace;
$eIU_ext_1_field_values['ID_IssueDate'] = $ID_IssueDate;
$eIU_ext_1_field_values['ID_ValidDate'] = $ID_ValidDate;
$eIU_ext_1_field_values['SP_Type'] = $SP_Type;
$eIU_ext_1_field_values['SP_Num'] = $SP_Num;
$eIU_ext_1_field_values['SP_IssueDate'] = $SP_IssueDate;
$eIU_ext_1_field_values['SP_ValidDate'] = $SP_ValidDate;
$eIU_ext_1_field_values['CountryCode'] = $CountryCode;
$eIU_ext_1_field_values['CountryText'] = $CountryText;
$eIU_ext_1_field_values['Province'] = $Province;
$eIU_ext_1_field_values['R_AreaCode'] = $R_AreaCode;
$eIU_ext_1_field_values['R_AreaText'] = $R_AreaText;
$eIU_ext_1_field_values['AreaCode'] = $AreaCode;
$eIU_ext_1_field_values['Road'] = $Road;
$eIU_ext_1_field_values['Address'] = $Address;

# update or create?
$ExtendRecordID = $lu_ext->getExtendRecordIDByUserID($UserID);
if($ExtendRecordID){
	# update
	$sql = "UPDATE INTRANET_USER_EXT_1 SET ";
	foreach($eIU_ext_1_field_values as $k => $v){
		$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
	}
	$sql .= " DateModified=NOW() WHERE ExtendRecordID = '$ExtendRecordID'";
}
else{
	$field_sql = '';
	$value_sql = '';

	foreach($eIU_ext_1_field_values as $k => $v){
		$field_sql .= "$k, ";
		$value_sql .= ($v==''?"NULL":"'$v'").", ";
	}
	# insert
	$sql =	"
						INSERT INTO
							INTRANET_USER_EXT_1
								($field_sql DateInput, DateModified)
						VALUES
							($value_sql NOW(), NOW())
					";
}
$result['eIU_ext_1'] = $li->db_db_query($sql);
$executed_sql['eIU_ext_1'] = $sql;

/******************************************
Guardian Part
*******************************************/
# insert / update / Delete Father
# insert / update / Delete Mother
# insert / update other
# record_id for existing

$delete_record_ids = array();


$F_chname = intranet_htmlspecialchars(trim($_POST['chname_R01']));
$F_enname = intranet_htmlspecialchars(trim($_POST['enname_R01']));
$F_occupation = intranet_htmlspecialchars(trim($_POST['occupation_R01']));
$F_phone = intranet_htmlspecialchars(trim($_POST['phone_R01']));
$F_emphone = intranet_htmlspecialchars(trim($_POST['emphone_R01']));
$F_liveTogether = intranet_htmlspecialchars(trim($_POST['liveTogether_R01']));
$F_add_area = intranet_htmlspecialchars(trim($_POST['add_area_R01']));
$F_add_road = intranet_htmlspecialchars(trim($_POST['add_road_R01']));
$F_add_address = intranet_htmlspecialchars(trim($_POST['add_address_R01']));
$F_record_id = intranet_htmlspecialchars(trim($_POST['record_id_R01']));

$M_chname = intranet_htmlspecialchars(trim($_POST['chname_R02']));
$M_enname = intranet_htmlspecialchars(trim($_POST['enname_R02']));
$M_occupation = intranet_htmlspecialchars(trim($_POST['occupation_R02']));
$M_phone = intranet_htmlspecialchars(trim($_POST['phone_R02']));
$M_emphone = intranet_htmlspecialchars(trim($_POST['emphone_R02']));
$M_liveTogether = intranet_htmlspecialchars(trim($_POST['liveTogether_R02']));
$M_add_area = intranet_htmlspecialchars(trim($_POST['add_area_R02']));
$M_add_road = intranet_htmlspecialchars(trim($_POST['add_road_R02']));
$M_add_address = intranet_htmlspecialchars(trim($_POST['add_address_R02']));
$M_record_id = intranet_htmlspecialchars(trim($_POST['record_id_R02']));

$Guardian_Type = intranet_htmlspecialchars(trim($_POST['Guardian_Type']));
$EmergencyContact_Type = intranet_htmlspecialchars(trim($_POST['EmergencyContact_Type']));

switch($Guardian_Type === 0 ? '' : $Guardian_Type){
	case 'F':
		$main = array($F_record_id);
		break;
	case 'M':
		$main = array($M_record_id);
		break;
	default:
		$main = isset($_POST['main']) ? $_POST['main'] : array();
		break;
}

switch($EmergencyContact_Type === 0 ? '' : $EmergencyContact_Type){
	case 'F':
		$ec = array($F_record_id);
		break;
	case 'M':
		$ec = array($M_record_id);
		break;
	default:
		$ec = isset($_POST['ec']) ? $_POST['ec'] : array();
		break;
}

if($plugin['sms']){
	switch($SMS_Type === 0 ? '' : $SMS_Type){
		case 'F':
			$sms = array($F_record_id);
			break;
		case 'M':
			$sms = array($M_record_id);
			break;
		default:
			$sms = isset($_POST['sms']) ? $_POST['sms'] : array();
			break;
	}
}
else{
	$sms = array();
}

if($F_chname!='' || $F_enname!=''){
	if($F_add_area == "" || $F_add_area == "O"){
		$F_address = "";
	}
	else{
		$F_address = $i_StudentRegistry_AreaCode[$F_add_area]."\n";
	}
	$F_address .= trim($F_add_road)."\n";
	$F_address .= trim($F_add_address);

	$iGS_field_values = array();
	$iGS_ext_1_field_values = array();

	$iGS_field_values['UserID'] = $UserID;
	$iGS_field_values['EnName'] = $F_enname;
	$iGS_field_values['ChName'] = $F_chname;
	$iGS_field_values['Relation'] = '01';
	$iGS_field_values['Phone'] = $F_phone;
	$iGS_field_values['EmPhone'] = $F_emphone;
	$iGS_field_values['Address'] = $F_address;
	$iGS_field_values['IsMain'] = in_array($F_record_id,$main)? "1":"0";
	$iGS_field_values['isSMS'] = in_array($F_record_id, $sms)? "1":"0";

	$iGS_ext_1_field_values['UserID'] = $UserID;
	$iGS_ext_1_field_values['IsLiveTogether'] = $F_liveTogether;
	$iGS_ext_1_field_values['IsEmergencyContact'] = in_array($F_record_id,$ec)? "1":"0";
	$iGS_ext_1_field_values['Occupation'] = $F_occupation;
	$iGS_ext_1_field_values['AreaCode'] = $F_add_area;
	$iGS_ext_1_field_values['Road'] = $F_add_road;
	$iGS_ext_1_field_values['Address'] = $F_add_address;
	
	# update/ insert the GS
	if($F_record_id){
		# update
		$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET ";
		foreach($iGS_field_values as $k => $v){
			$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
		}
		$sql .= " ModifiedDate = NOW() WHERE RecordID = '$F_record_id'";
		$result['iGS_R01_u'] = $li->db_db_query($sql);
		$executed_sql['iGS_R01_u'] = $sql;
		$sql =	"
							SELECT
								ExtendRecordID
							FROM
								$eclass_db.GUARDIAN_STUDENT_EXT_1
							WHERE
								RecordID = '$F_record_id'
						";
		$r = $li->returnArray($sql,39);
		$F_gs_ext_record_id = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID']:'';
	}
	else{
		# insert
		$field_sql = '';
		$value_sql = '';

		foreach($iGS_field_values as $k => $v){
			$field_sql .= "$k, ";
			$value_sql .= ($v==''?"NULL":"'$v'").", ";
		}
		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT
									($field_sql InputDate, ModifiedDate)
							VALUES
								($value_sql NOW(), NOW())
						";
		$result['iGS_R01_i'] = $li->db_db_query($sql);
		$executed_sql['iGS_R01_i'] = $sql;
		$F_record_id = mysql_insert_id();
		$F_gs_ext_record_id = '';
	}
	$iGS_ext_1_field_values['RecordID'] = $F_record_id;
	if($F_gs_ext_record_id){
		# update
		$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET ";
		foreach($iGS_ext_1_field_values as $k => $v){
			$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
		}
		$sql .= " ModifiedDate = NOW() WHERE ExtendRecordID = '$F_gs_ext_record_id'";
		$result['iGS_ext_1_R01_u'] = $li->db_db_query($sql);
		$executed_sql['iGS_ext_1_R01_u'] = $sql;
	}
	else{
		# insert
		$field_sql = '';
		$value_sql = '';

		foreach($iGS_ext_1_field_values as $k => $v){
			$field_sql .= "$k, ";
			$value_sql .= ($v==''?"NULL":"'$v'").", ";
		}
		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT_EXT_1
									($field_sql InputDate, ModifiedDate)
							VALUES
								($value_sql NOW(), NOW())
						";
		$result['iGS_ext_1_R01_i'] = $li->db_db_query($sql);
		$executed_sql['iGS_ext_1_R01_i'] = $sql;
	}
}
else{
	# delete or ignore if no record ahead
	if($F_record_id){
		# delete, later in batch
		$delete_record_ids[] = $F_record_id;
	}
	else{
		# ignore
	}
}

if($M_chname!='' || $M_enname!=''){
	if($M_add_area == "" || $M_add_area == "O"){
		$M_address = "";
	}
	else{
		$M_address = $i_StudentRegistry_AreaCode[$M_add_area]."\n";
	}
	$M_address .= trim($M_add_road)."\n";
	$M_address .= trim($M_add_address);

	$iGS_field_values = array();
	$iGS_ext_1_field_values = array();

	$iGS_field_values['UserID'] = $UserID;
	$iGS_field_values['EnName'] = $M_enname;
	$iGS_field_values['ChName'] = $M_chname;
	$iGS_field_values['Relation'] = '02';
	$iGS_field_values['Phone'] = $M_phone;
	$iGS_field_values['EmPhone'] = $M_emphone;
	$iGS_field_values['Address'] = $M_address;
	$iGS_field_values['IsMain'] = in_array($M_record_id,$main)? "1":"0";
	$iGS_field_values['isSMS'] = in_array($M_record_id, $sms)? "1":"0";

	$iGS_ext_1_field_values['UserID'] = $UserID;
	$iGS_ext_1_field_values['IsLiveTogether'] = $M_liveTogether;
	$iGS_ext_1_field_values['IsEmergencyContact'] = in_array($M_record_id,$ec)? "1":"0";
	$iGS_ext_1_field_values['Occupation'] = $M_occupation;
	$iGS_ext_1_field_values['AreaCode'] = $M_add_area;
	$iGS_ext_1_field_values['Road'] = $M_add_road;
	$iGS_ext_1_field_values['Address'] = $M_add_address;
	
	# update/ insert the GS
	if($M_record_id){
		# update
		$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT SET ";
		foreach($iGS_field_values as $k => $v){
			$sql .= " $k = ".($v==='' ?"NULL":"'$v'").", ";
		}
		$sql .= " ModifiedDate = NOW() WHERE RecordID = '$M_record_id'";
		$result['iGS_R02_u'] = $li->db_db_query($sql);
		$executed_sql['iGS_R02_u'] = $sql;
		$sql =	"
							SELECT
								ExtendRecordID
							FROM
								$eclass_db.GUARDIAN_STUDENT_EXT_1
							WHERE
								RecordID = '$M_record_id'
						";
		$r = $li->returnArray($sql,39);
		$M_gs_ext_record_id = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID']:'';
	}
	else{
		# insert
		$field_sql = '';
		$value_sql = '';

		foreach($iGS_field_values as $k => $v){
			$field_sql .= "$k, ";
			$value_sql .= ($v==''?"NULL":"'$v'").", ";
		}
		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT
									($field_sql InputDate, ModifiedDate)
							VALUES
								($value_sql NOW(), NOW())
						";
		$result['iGS_R02_i'] = $li->db_db_query($sql);
		$executed_sql['iGS_R02_i'] = $sql;
		$M_record_id = mysql_insert_id();
		$M_gs_ext_record_id = '';
	}
	$iGS_ext_1_field_values['RecordID'] = $M_record_id;
	if($M_gs_ext_record_id){
		# update
		$sql = "UPDATE $eclass_db.GUARDIAN_STUDENT_EXT_1 SET ";
		foreach($iGS_ext_1_field_values as $k => $v){
			$sql .= " $k = ".($v==''?"NULL":"'$v'").", ";
		}
		$sql .= " ModifiedDate = NOW() WHERE ExtendRecordID = '$M_gs_ext_record_id'";
		$result['iGS_ext_1_R02_u'] = $li->db_db_query($sql);
		$executed_sql['iGS_ext_1_R02_u'] = $sql;
	}
	else{
		# insert
		$field_sql = '';
		$value_sql = '';

		foreach($iGS_ext_1_field_values as $k => $v){
			$field_sql .= "$k, ";
			$value_sql .= ($v==''?"NULL":"'$v'").", ";
		}
		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT_EXT_1
									($field_sql InputDate, ModifiedDate)
							VALUES
								($value_sql NOW(), NOW())
						";
		$result['iGS_ext_1_R02_i'] = $li->db_db_query($sql);
		$executed_sql['iGS_ext_1_R02_i'] = $sql;
	}
}
else{
	# delete or ignore if no record ahead
	if($M_record_id){
		# delete, later in batch
		$delete_record_ids[] = $M_record_id;
	}
	else{
		# ignore
	}
}

$RecordIDs = isset($_POST['record_id'])? $_POST['record_id'] : array();
$DeleteIDs = isset($_POST['Delete']) ?$_POST['Delete'] : array();
$newids = intranet_htmlspecialchars(trim($_POST['newids']));

if(count($delete_record_ids)>0){
	for($i=0; $i<count($delete_record_ids);$i++){
		$DeleteIDs[] = $delete_record_ids[$i];
	}
}

## Other G UPDATE
for($i=0;$i<sizeof($RecordIDs);$i++){
	$record_id = $RecordIDs[$i];
		
	// deleted
	if(is_array($DeleteIDs) && in_array($record_id,$DeleteIDs)) continue;
		
	$ch_name = ${'chname_'.$record_id};
	$en_name = ${'enname_'.$record_id};
	$phone   = ${'phone_'.$record_id};
	$emphone = ${'emphone_'.$record_id};

	$add_area = ${'add_area_'.$record_id};
	$add_road = ${'add_road_'.$record_id};
	$add_address = ${'add_address_'.$record_id};
	if($add_area == "" || $add_area == "O"){
		$address = "";
	}else{
		$address = $i_StudentRegistry_AreaCode[$add_area]."\n";
	}
	$address .= trim($add_road)."\n";
	$address .= trim($add_address);

	$relation= ${'relation_'.$record_id};

	$fields  = "ChName ='$ch_name'";
	$fields .= ", EnName = '$en_name'";
	$fields .= ", Phone = '$phone'";
	$fields .= ", EmPhone = '$emphone'";
	$fields .= ", Address = '$address'";
	$fields .= ", Relation = '$relation'";
	$fields .= is_array($main) && in_array($record_id,$main) ? ", IsMain=1": ", IsMain=0";
	$fields .= ($plugin['sms']) ? (is_array($sms) && in_array($record_id,$sms) ? ", IsSMS=1":", IsSMS=0"):"";
	$fields .= ", ModifiedDate=NOW()";
	$sql =	"
						UPDATE
							$eclass_db.GUARDIAN_STUDENT
						SET
							$fields
						WHERE
							RecordID = '$record_id'
					";
	$result['iGS_'.$i.'_u'] = $li->db_db_query($sql);
	$executed_sql['iGS_'.$i.'_u'] = $sql;

	$occupation = ${'occupation_'.$record_id};
	$IsLiveTogether = (int)${'liveTogether_'.$record_id};
	$IsEmergencyContact = (int)(is_array($ec) && in_array($record_id,$ec));

	$sql =	"
						SELECT
							ExtendRecordID
						FROM
							$eclass_db.GUARDIAN_STUDENT_EXT_1
						WHERE
							RecordID = '$record_id'
					";
	$r = $li->returnArray($sql, 9);
	$ExtendRecordID = isset($r[0]['ExtendRecordID']) ? $r[0]['ExtendRecordID'] : '';
	if($ExtendRecordID){
		# update the extend record
		$sql =	"
							UPDATE
								$eclass_db.GUARDIAN_STUDENT_EXT_1
							SET
								IsLiveTogether = '$IsLiveTogether',
								IsEmergencyContact = '$IsEmergencyContact',
								Occupation = '$occupation',
								AreaCode = '$add_area',
								Road = '$add_road',
								Address = '$add_address',
								ModifiedDate = NOW()
							WHERE
								ExtendRecordID = '$ExtendRecordID'
						";
	}
	else{
		# insert the extend record
		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT_EXT_1
									(UserID, RecordID, IsLiveTogether, IsEmergencyContact, Occupation, AreaCode, Road, Address, InputDate, ModifiedDate)
							VALUES
								('$UserID','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation', '$add_area','$add_road', '$add_address', NOW(), NOW())
						";
	}
	$result['iGS_ext_1_'.$i] = $li->db_db_query($sql);
	$executed_sql['iGS_ext_1_'.$i] = $sql;
}

## INSERT
if($newids!=""){
	$ids = explode(",",$newids);
	for($i=0;$i<sizeof($ids);$i++){
		$id = $ids[$i];
		$ch_name = ${'chname_new_'.$id};
		$en_name = ${'enname_new_'.$id};
		$phone   = ${'phone_new_'.$id};
		$emphone = ${'emphone_new_'.$id};

		$add_area = ${'add_area_new_'.$id};
		$add_road = ${'add_road_new_'.$id};
		$add_address = ${'add_address_new_'.$id};
		if($add_area == "" || $add_area == "O"){
			$address = "";
		}else{
			$address = $i_StudentRegistry_AreaCode[$add_area]."\n";
		}
		$address .= trim($add_road)."\n";
		$address .= trim($add_address);
		$relation= ${'relation_new_'.$id};

		$new_str ="new_$id";
		$IsMain = (int)(is_array($main) && in_array($new_str,$main));
		$IsSMS = (int) ($plugin['sms'] && is_array($sms) && in_array($new_str,$sms));
		$IsEmergencyContact = (int)(is_array($ec) && in_array($new_str,$ec));
		$occupation = ${'occupation_new_'.$id};

		$IsLiveTogether = (int)${'liveTogether_new_'.$id};

		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT
									(UserID,ChName,EnName,Phone,EmPhone,Address,Relation,IsMain,IsSMS,InputDate,ModifiedDate)
							VALUES
								('$UserID','$ch_name','$en_name','$phone','$emphone','$address','$relation','$IsMain', '$IsSMS',NOW(), NOW())
						";
		$result['iGS_'.$i.'_i'] = $li->db_db_query($sql);
		$executed_sql['iGS_'.$i.'_i'] = $sql;

		$record_id = mysql_insert_id();

		# insert the extend record
		$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT_EXT_1
									(UserID,RecordID, IsLiveTogether, IsEmergencyContact, Occupation, AreaCode, Road, Address, InputDate, ModifiedDate)
							VALUES
								('$UserID','$record_id', '$IsLiveTogether', '$IsEmergencyContact', '$occupation', '$add_area','$add_road', '$add_address', NOW(), NOW())
						";
		$result['iGS_ext_1_'.$i.'_i'] = $li->db_db_query($sql);
		$executed_sql['iGS_ext_1_'.$i.'_i'] = $sql;
	}
}

### Delete
if(sizeof($DeleteIDs)>0){
	$deletedids = implode(",",$DeleteIDs);
	$sql =	"
						DELETE FROM
							$eclass_db.GUARDIAN_STUDENT
						WHERE
							RecordID IN ($deletedids)
					";
	$result['Remove_iGS'] = $li->db_db_query($sql);

	$sql =	"
						DELETE FROM
							$eclass_db.GUARDIAN_STUDENT_EXT_1
						WHERE
							RecordID IN ($deletedids)
					";
	$result['Remove_iGS_ext_1'] = $li->db_db_query($sql);
}
	
$ClassID_Str = '';
for($i=0; $i<count($ClassID); $i++){
	$ClassID_Str .= '&ClassID[]='. $ClassID[$i];
}

$url = "edit.php?SID=".$UserID.$ClassID_Str."&order=$order&field=$field&pageNo=$pageNo&keyword=".$keyword."&ts=".$ts."&msg=2";

intranet_closedb();
header("Location: $url");
?>
