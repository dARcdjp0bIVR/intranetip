<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");

?>

<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentRegistry['System'], '') ?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
	<tr>
		<td class=tableContent height=300>
			<blockquote>
				<?= displayOption($button_search,'search.php',1,$button_import_xml,'import_xml.php',1) ?>
			</blockquote>
		</td>
	</tr>
</table>

<?
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
