<?php
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libwebsamsattendcode.php");
include_once($PATH_WRT_ROOT."includes/libSimpleXML.php");

intranet_opendb();

$lwebsams_code = new libwebsamsattendcode();
$lwebsams_code->retrieveBasicInfo();

//error_reporting(E_ALL);

$sql_fields = array();

$lsx = new libSimpleXML();

$SchoolID = $lwebsams_code->SchoolID;

$Setting  = array();

$BasicColumnType = array();
if($ExportType==1){
	$output_file_name = 'searchcode_'.$SchoolID.'.xml';
	$RootTag = 'SEARCHCODE';
	$RowTag = 'CODE';

	$BasicColumnType[] = array('CAT', 'CAT');
	$BasicColumnType[] = array('NAME_C', 'ChineseName');
	$BasicColumnType[] = array('NAME_P', 'EnglishName');
	$BasicColumnType[] = array('TEL', 'Tel');
	$BasicColumnType[] = array('B_DATE', 'DateOfBirth');
	$BasicColumnType[] = array('ID_TYPE', 'ID_Type');
	$BasicColumnType[] = array('ID_NO', 'ID_Num');

	$sql_fields[] = " 1 CAT ";
	$sql_fields[] = " iu.ChineseName ";
	$sql_fields[] = " iu.EnglishName ";
	$sql_fields[] = " IFNULL(iu.MobileTelNo, iu.HomeTelNo) Tel ";
	$sql_fields[] = " IF(DATE_FORMAT(iu.DateOfBirth,'%Y')=0, '', DATE_FORMAT(iu.DateOfBirth,'%Y-%m-%d')) DateOfBirth ";
	$sql_fields[] = " iu_ext_1.ID_Type ";
	$sql_fields[] = " iu_ext_1.ID_Num ";
}
else if($ExportType==2){
	$output_file_name = 'stud2_'.$SchoolID.'_'.'[batch]'.'.xml';
	$dtd_file = 'http://app.dsej.gov.mo/prog/edu/sra/sra2.dtd';
	$Setting['DocType'] = '<!DOCTYPE SRA SYSTEM "'.$dtd_file.'">';
	$RootTag = 'SRA';
	$RowTag = 'STUDENT';

	$BasicColumnType[] = array('CODE', 'Code');
	$BasicColumnType[] = array('S_CODE', 'SCode');
	$BasicColumnType[] = array('ID_NO', 'ID_Num');
	$BasicColumnType[] = array('NAME_P', 'EnglishName');
	$BasicColumnType[] = array('GRADE', 'Grade');
	$BasicColumnType[] = array('CLASS', 'WebSAMSClassCode');
	$BasicColumnType[] = array('C_NO', 'ClassNumber');

	$sql_fields[] = " IF(iu_ext_1.AuthCodeMain IS NOT NULL, CONCAT(iu_ext_1.AuthCodeMain,'-',IFNULL(iu_ext_1.AuthCodeCheck,'')), '') Code";
	$sql_fields[] = " IF(iu_ext_1.AuthCodeMain IS NULL, iu_ext_1.ID_Num, '') ID_Num";
	$sql_fields[] = " IF(iu_ext_1.AuthCodeMain IS NULL, iu.EnglishName, '') EnglishName";
	$sql_fields[] = "'".$SchoolID."' SCode";
	$sql_fields[] = "icl.WebSAMSLevel Grade";
	$sql_fields[] = "ic.WebSAMSClassCode WebSAMSClassCode";
	$sql_fields[] = " iu.ClassNumber ";
}
else if($ExportType==3){
	$output_file_name = 'stud3_'.$SchoolID.'_'.'[batch]'.'.xml';
	$dtd_file = 'http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd';
	$Setting['DocType'] = '<!DOCTYPE SRA SYSTEM "'.$dtd_file.'">';
	$RootTag = 'SRA';
	$RowTag = 'STUDENT';

	$BasicColumnType[] = array('STUD_ID', 'WebSAMSRegNo');
	$BasicColumnType[] = array('CODE', 'Code');
	$BasicColumnType[] = array('NAME_C', 'ChineseName');
	$BasicColumnType[] = array('NAME_P', 'EnglishName');
	$BasicColumnType[] = array('SEX', 'Gender');
	$BasicColumnType[] = array('B_DATE', 'DateOfBirth');
	$BasicColumnType[] = array('B_PLACE', 'PlaceOfBirthCode');
	$BasicColumnType[] = array('ID_TYPE', 'ID_Type');
	$BasicColumnType[] = array('ID_NO', 'ID_Num');
	$BasicColumnType[] = array('I_PLACE', 'ID_IssuePlace');

	$sql_fields[] = " iu.WebSAMSRegNo ";
	$sql_fields[] = " IF(iu_ext_1.AuthCodeMain IS NOT NULL, CONCAT(iu_ext_1.AuthCodeMain,'-',IFNULL(iu_ext_1.AuthCodeCheck,'')), '') Code ";
	$sql_fields[] = " iu.ChineseName ";
	$sql_fields[] = " iu.EnglishName ";
	$sql_fields[] = " iu.Gender ";
	$sql_fields[] = " IF(DATE_FORMAT(iu.DateOfBirth,'%Y')=0, '', DATE_FORMAT(iu.DateOfBirth,'%Y-%m-%d')) DateOfBirth ";
	$sql_fields[] = " iu_ext_1.PlaceOfBirthCode ";
	$sql_fields[] = " iu_ext_1.ID_Type ";
	$sql_fields[] = " iu_ext_1.ID_Num ";
	$sql_fields[] = " iu_ext_1.ID_IssuePlace ";

	$BasicColumnType[] = array('I_DATE', 'ID_IssueDate');
	$BasicColumnType[] = array('V_DATE', 'ID_ValidDate');
	$BasicColumnType[] = array('S6_TYPE', 'SP_Type');
	$BasicColumnType[] = array('S6_IDATE', 'SP_IssueDate');
	$BasicColumnType[] = array('S6_VDATE', 'SP_ValidDate');
	$BasicColumnType[] = array('NATION', 'CountryCode');
	$BasicColumnType[] = array('ORIGIN', 'Province');
	$BasicColumnType[] = array('TEL', 'Tel');
	$BasicColumnType[] = array('R_AREA', 'R_AreaCode');
	$BasicColumnType[] = array('RA_DESC', 'R_AreaText');

	$sql_fields[] = " iu_ext_1.ID_IssueDate ";
	$sql_fields[] = " iu_ext_1.ID_ValidDate ";
	$sql_fields[] = " iu_ext_1.SP_Type ";
	$sql_fields[] = " iu_ext_1.SP_IssueDate ";
	$sql_fields[] = " iu_ext_1.SP_ValidDate ";
	$sql_fields[] = " iu_ext_1.CountryCode ";
	$sql_fields[] = " iu_ext_1.Province ";
	$sql_fields[] = " IFNULL(iu.MobileTelNo, iu.HomeTelNo) Tel ";
	$sql_fields[] = " iu_ext_1.R_AreaCode ";
	$sql_fields[] = " iu_ext_1.R_AreaText ";

	$BasicColumnType[] = array('AREA', 'AreaCode');
	$BasicColumnType[] = array('ROAD', 'Road');
	$BasicColumnType[] = array('ADDRESS', 'Address');
	$BasicColumnType[] = array('FATHER', 'FatherName');
	$BasicColumnType[] = array('MOTHER', 'MotherName');
	$BasicColumnType[] = array('F_PROF', 'FatherOccupation');
	$BasicColumnType[] = array('M_PROF', 'MotherOccupation');
	$BasicColumnType[] = array('GUARD', 'GuardianType');
	$BasicColumnType[] = array('LIVE_SAME', 'GuardianIsLiveTogether');
	$BasicColumnType[] = array('EC_NAME', 'EmergencyContactName');

	$sql_fields[] = " iu_ext_1.AreaCode ";
	$sql_fields[] = " iu_ext_1.Road ";
	$sql_fields[] = " iu_ext_1.Address ";
	$sql_fields[] = " IFNULL(i_gs_father.ChName, i_gs_father.EnName) FatherName ";
	$sql_fields[] = " IFNULL(i_gs_mother.ChName, i_gs_mother.EnName) MotherName ";
	$sql_fields[] = " i_gs_ext_1_father.Occupation FatherOccupation ";
	$sql_fields[] = " i_gs_ext_1_mother.Occupation MotherOccupation ";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, 'F',
											IF(i_gs_mother.IsMain=1, 'M',
												IF(i_gs_main.IsMain=1, 'O', '')
											)
										) GuardianType
									";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.IsLiveTogether,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.IsLiveTogether,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.IsLiveTogether, '')
											)
										) GuardianIsLiveTogether
									";
	$sql_fields[] = " IFNULL(i_gs_ec.ChName, i_gs_ec.EnName) EmergencyContactName ";

	$BasicColumnType[] = array('EC_REL', 'EmergencyContact_RelationString');
	$BasicColumnType[] = array('EC_TEL', 'EmergencyContact_Tel');
	$BasicColumnType[] = array('EC_AREA', 'EmergencyContact_AreaCode');
	$BasicColumnType[] = array('EC_ROAD', 'EmergencyContact_Road');
	$BasicColumnType[] = array('EC_ADDRESS', 'EmergencyContact_Address');
	$BasicColumnType[] = array('S_CODE', 'SCode');
	$BasicColumnType[] = array('GRADE', 'Grade');
	$BasicColumnType[] = array('CLASS', 'WebSAMSClassCode');
	$BasicColumnType[] = array('C_NO', 'ClassNumber');
	$BasicColumnType[] = array('G_NAME', 'OM_GuardianName');//OM--Other / Main 

	$ec_relationSql = " CASE i_gs_ec.Relation ";
	foreach($ec_guardian AS $key =>$relation_name){
		$ec_relationSql .= " WHEN '$key' THEN '$relation_name' ";
	}
	$ec_relationSql .= " ELSE '' END EmergencyContact_RelationString ";
	$sql_fields[] = $ec_relationSql;
	$sql_fields[] = " IFNULL(i_gs_ec.EmPhone, i_gs_ec.Phone) EmergencyContact_Tel ";
	$sql_fields[] = " i_gs_ext_1_ec.AreaCode EmergencyContact_AreaCode ";
	$sql_fields[] = " i_gs_ext_1_ec.Road EmergencyContact_Road ";
	$sql_fields[] = " i_gs_ext_1_ec.Address EmergencyContact_Address ";
	$sql_fields[] = "'".$SchoolID."' SCode";
	$sql_fields[] = "icl.WebSAMSLevel Grade";
	$sql_fields[] = "ic.WebSAMSClassCode WebSAMSClassCode";

	$sql_fields[] = " iu.ClassNumber ";
	$sql_fields[] = " IFNULL(i_gs_main.ChName, i_gs_main.EnName) OM_GuardianName ";

	$BasicColumnType[] = array('G_RELATION', 'OM_Guardian_RelationString');
	$BasicColumnType[] = array('G_PROFESSION', 'OM_Guardian_Occupation');
	$BasicColumnType[] = array('G_AREA', 'Guardian_AreaCode');
	$BasicColumnType[] = array('G_ROAD', 'Guardian_Road');
	$BasicColumnType[] = array('G_ADDRESS', 'Guardian_Address');
	$BasicColumnType[] = array('G_TEL', 'Guardian_Tel');
	$BasicColumnType[] = array('GUARDMOBILE', 'Guardian_Mobile');

	$om_relationSql = " CASE i_gs_main.Relation ";
	foreach($ec_guardian AS $key =>$relation_name){
		$om_relationSql .= " WHEN '$key' THEN '$relation_name' ";
	}
	$om_relationSql .=  " ELSE '' END OM_Guardian_RelationString ";
	$sql_fields[] = $om_relationSql;
	$sql_fields[] = " i_gs_ext_1_main.Occupation OM_Guardian_Occupation ";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.AreaCode,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.AreaCode,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.AreaCode, '')
											)
										) Guardian_AreaCode
									";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.Road,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.Road,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.Road, '')
											)
										) Guardian_Road
									";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.Address,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.Address,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.Address, '')
											)
										) Guardian_Address
									";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_father.Phone,
											IF(i_gs_mother.IsMain=1, i_gs_mother.Phone,
												IF(i_gs_main.IsMain=1, i_gs_main.Phone, '')
											)
										) Guardian_Tel
									";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_father.EmPhone,
											IF(i_gs_mother.IsMain=1, i_gs_mother.EmPhone,
												IF(i_gs_main.IsMain=1, i_gs_main.EmPhone, '')
											)
										) Guardian_Mobile
									";
	//$sql_fields[] = "
}

$lsx->changeSetting($Setting);


$li = new libdb();

$namefield = getNameFieldByLang("iu.");
$namefield2= getNameFieldWithClassNumberByLang("iu.");
$cond="";
$cond_keyword="";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$delim="";
$class_str = '';
for($i=0;$i<sizeof($ClassID);$i++){
        $class_str .= $delim."'".$ClassID[$i]."'";
        $delim=",";
}
$cond = " AND ic.ClassName IN ($class_str) ";
if($keyword!=""){
        $cond_keyword=" AND ($namefield2 LIKE '%".addslashes($keyword)."%' OR iu.UserLogin LIKE '%$keyword%') ";
}else{
        $cond_keyword = "";
}
if($ts){
        $cond_ts = " AND (iu.DateModified >= '$ts' OR iu_ext_1.DateModified >= '$ts'
                        OR i_gs_father.ModifiedDate >= '$ts' OR i_gs_ext_1_father.ModifiedDate >= '$ts'
                        OR i_gs_mother.ModifiedDate >= '$ts' OR i_gs_ext_1_mother.ModifiedDate >= '$ts'
                        OR i_gs_ec.ModifiedDate >= '$ts' OR i_gs_ext_1_ec.ModifiedDate >= '$ts'
                        OR i_gs_main.ModifiedDate >= '$ts' OR i_gs_ext_1_main.ModifiedDate >= '$ts'
                        )";
}else{
        $cond_ts = "";
}


$sql =	"
					SELECT
						".implode(', ' ,$sql_fields)."
					FROM
						INTRANET_USER as iu
					INNER JOIN
						INTRANET_CLASS as ic
					ON
						iu.ClassName = ic.ClassName AND
						ic.RecordStatus = 1
					LEFT OUTER JOIN
						INTRANET_CLASSLEVEL as icl
					ON
						ic.ClassLevelID = icl.ClassLevelID
					LEFT OUTER JOIN
						INTRANET_USER_EXT_1 as iu_ext_1
					ON
						iu.UserID = iu_ext_1.UserID

					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_father
					ON
						iu.UserID = i_gs_father.UserID AND
						LPAD(i_gs_father.Relation,2,0) = '01'
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_father
					ON
						i_gs_father.RecordID = i_gs_ext_1_father.RecordID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_mother
					ON
						iu.UserID = i_gs_mother.UserID AND
						LPAD(i_gs_mother.Relation,2,0) = '02'
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_mother
					ON
						i_gs_mother.RecordID = i_gs_ext_1_mother.RecordID

					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_ec
					ON
						iu.UserID = i_gs_ext_1_ec.UserID AND
						i_gs_ext_1_ec.IsEmergencyContact = 1 AND
						i_gs_ext_1_ec.RecordID != i_gs_father.RecordID AND
						i_gs_ext_1_ec.RecordID != i_gs_mother.RecordID
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_ec
					ON
						i_gs_ec.RecordID = i_gs_ext_1_ec.RecordID

					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_main
					ON
						iu.UserID = i_gs_main.UserID AND
						i_gs_main.IsMain = 1 AND
						NOT
							(	LPAD(i_gs_main.Relation,2,0) = '01' OR
								LPAD(i_gs_main.Relation,2,0) = '02'
							)
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_main
					ON
						i_gs_main.RecordID = i_gs_ext_1_main.RecordID
					
					WHERE
						iu.RecordType = 2 AND
						iu.RecordStatus IN (0,1,2)
						$cond
						$cond_keyword
						$cond_ts
				";

$field_array = array("$namefield","iu.ClassName","iu.ClassNumber","iu.WebSAMSRegNo", "iu_ext_1.AuthCodeMain", "iu_ext_1.AuthCodeCheck");
$sql .= " ORDER BY ".$field_array[$field].( $order==0 ? " DESC" : " ASC");
$result = $li->returnArray($sql);

$XMLData = array();
$XMLData[$RootTag] = array();
for($i=0; $i<count($result); $i++){
	$tmpData = array();
	for($j=0; $j<count($BasicColumnType); $j++){
		$tmpData[$BasicColumnType[$j][0]][]['data'] = $result[$i][$BasicColumnType[$j][1]];
	}
	$tmpData2 = array();
	$tmpData2[$RowTag][] = $tmpData;
	$XMLData[$RootTag][] = $tmpData2;
}

$lsx->setXMLData($XMLData);
$xml_result = $lsx->getXML();

/*
echo 'abc';
echo mysql_error();
//echo '<br/><textarea cols=120 rows=15>';
//echo $xml_result;
//echo '</textarea><br/>';
echo '<pre>';
//var_dump($XMLData);

echo $sql;
echo '</pre>';
die;
*/

$lsx->exportFile($output_file_name,$xml_result);

intranet_closedb();
//header("Location: index.php");
?>
