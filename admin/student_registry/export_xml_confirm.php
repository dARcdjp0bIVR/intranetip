<?php
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libwebsamsattendcode.php");
include_once($PATH_WRT_ROOT."includes/libSimpleXML.php");

intranet_opendb();

function RowDisplay($ParErrorCode, $ParItem){
  $ReturnStr = '<td nowrap>';
  if($ParErrorCode != "")
  {
    if($ParErrorCode)
    {
      if(trim($ParItem)=="")
        $ReturnStr .= "<span style=\"color:red\">-</span>";  
      else
        $ReturnStr .= $ParItem;
    }
    else
      $ReturnStr .= $ParItem;
  }
  else if(trim($ParItem)=="")
    $ReturnStr .= "-";
  else
    $ReturnStr .= $ParItem;
  $ReturnStr .= '</td>';
  
  return $ReturnStr;
}

$lwebsams_code = new libwebsamsattendcode();
$lwebsams_code->retrieveBasicInfo();

//error_reporting(E_ALL);

$sql_fields = array();

$lsx = new libSimpleXML();

$SchoolID = $lwebsams_code->SchoolID;

$Setting  = array();

$BasicColumnType = array();
if($ExportType==1){
	$output_file_name = 'searchcode_'.$SchoolID.'.xml';
	$RootTag = 'SEARCHCODE';
	$RowTag = 'CODE';

	$BasicColumnType[] = array('CAT', 'CAT');
	$BasicColumnType[] = array('NAME_C', 'ChineseName');
	$BasicColumnType[] = array('NAME_P', 'EnglishName');
	$BasicColumnType[] = array('TEL', 'Tel');
	$BasicColumnType[] = array('B_DATE', 'DateOfBirth');
	$BasicColumnType[] = array('ID_TYPE', 'ID_Type');
	$BasicColumnType[] = array('ID_NO', 'ID_Num');

	$sql_fields[] = " 1 CAT ";
	$sql_fields[] = " iu.ChineseName ";
	$sql_fields[] = " iu.EnglishName ";
	$sql_fields[] = " IFNULL(iu.MobileTelNo, iu.HomeTelNo) Tel ";
	$sql_fields[] = " IF(DATE_FORMAT(iu.DateOfBirth,'%Y')=0, '', DATE_FORMAT(iu.DateOfBirth,'%Y-%m-%d')) DateOfBirth ";
	$sql_fields[] = " iu_ext_1.ID_Type ";
	$sql_fields[] = " iu_ext_1.ID_Num ";
}
else if($ExportType==2){
	$output_file_name = 'stud2_'.$SchoolID.'_'.'[batch]'.'.xml';
	$dtd_file = 'http://app.dsej.gov.mo/prog/edu/sra/sra2.dtd';
	$Setting['DocType'] = '<!DOCTYPE SRA SYSTEM "'.$dtd_file.'">';
	$RootTag = 'SRA';
	$RowTag = 'STUDENT';

	$BasicColumnType[] = array('CODE', 'Code');
	$BasicColumnType[] = array('ID_NO', 'ID_Num');
	$BasicColumnType[] = array('NAME_P', 'EnglishName');
	$BasicColumnType[] = array('S_CODE', 'SCode');
	$BasicColumnType[] = array('GRADE', 'Grade');
	$BasicColumnType[] = array('CLASS', 'WebSAMSClassCode');
	$BasicColumnType[] = array('C_NO', 'ClassNumber');

	$sql_fields[] = " IF(iu_ext_1.AuthCodeMain IS NOT NULL, CONCAT(iu_ext_1.AuthCodeMain,'-',IFNULL(iu_ext_1.AuthCodeCheck,'')), '') Code ";
	$sql_fields[] = " iu_ext_1.ID_Num ";
	$sql_fields[] = " iu.EnglishName ";
	$sql_fields[] = "'".$SchoolID."' SCode";
	$sql_fields[] = "icl.WebSAMSLevel Grade";
	$sql_fields[] = "ic.WebSAMSClassCode WebSAMSClassCode";
	$sql_fields[] = " iu.ClassNumber ";
	
	$cols = 5;
}
else if($ExportType==3){
	$output_file_name = 'stud3_'.$SchoolID.'_'.'[batch]'.'.xml';
	$dtd_file = 'http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd';
	$Setting['DocType'] = '<!DOCTYPE SRA SYSTEM "'.$dtd_file.'">';
	$RootTag = 'SRA';
	$RowTag = 'STUDENT';

	$BasicColumnType[] = array('STUD_ID', 'WebSAMSRegNo');
	$BasicColumnType[] = array('CODE', 'Code');
	$BasicColumnType[] = array('NAME_C', 'ChineseName');
	$BasicColumnType[] = array('NAME_P', 'EnglishName');
	$BasicColumnType[] = array('SEX', 'Gender');
	$BasicColumnType[] = array('B_DATE', 'DateOfBirth');
	$BasicColumnType[] = array('B_PLACE', 'PlaceOfBirthCode');
	$BasicColumnType[] = array('ID_TYPE', 'ID_Type');
	$BasicColumnType[] = array('ID_NO', 'ID_Num');
	$BasicColumnType[] = array('I_PLACE', 'ID_IssuePlace');

	$sql_fields[] = " iu.WebSAMSRegNo ";
	$sql_fields[] = " IF(iu_ext_1.AuthCodeMain IS NOT NULL, CONCAT(iu_ext_1.AuthCodeMain,'-',IFNULL(iu_ext_1.AuthCodeCheck,'')), '') Code ";
	$sql_fields[] = " iu.ChineseName ";
	$sql_fields[] = " iu.EnglishName ";
	$sql_fields[] = " iu.Gender ";
	$sql_fields[] = " IF(DATE_FORMAT(iu.DateOfBirth,'%Y')=0, '', DATE_FORMAT(iu.DateOfBirth,'%Y-%m-%d')) DateOfBirth ";
	$sql_fields[] = " iu_ext_1.PlaceOfBirthCode ";
	$sql_fields[] = " iu_ext_1.ID_Type ";
	$sql_fields[] = " iu_ext_1.ID_Num ";
	$sql_fields[] = " iu_ext_1.ID_IssuePlace ";

	$BasicColumnType[] = array('I_DATE', 'ID_IssueDate');
	$BasicColumnType[] = array('V_DATE', 'ID_ValidDate');
	$BasicColumnType[] = array('S6_TYPE', 'SP_Type');
	$BasicColumnType[] = array('S6_IDATE', 'SP_IssueDate');
	$BasicColumnType[] = array('S6_VDATE', 'SP_ValidDate');
	$BasicColumnType[] = array('NATION', 'CountryCode');
	$BasicColumnType[] = array('ORIGIN', 'Province');
	$BasicColumnType[] = array('TEL', 'Tel');
	$BasicColumnType[] = array('R_AREA', 'R_AreaCode');
	$BasicColumnType[] = array('RA_DESC', 'R_AreaText');

	$sql_fields[] = " iu_ext_1.ID_IssueDate ";
	$sql_fields[] = " iu_ext_1.ID_ValidDate ";
	$sql_fields[] = " iu_ext_1.SP_Type ";
	$sql_fields[] = " iu_ext_1.SP_IssueDate ";
	$sql_fields[] = " iu_ext_1.SP_ValidDate ";
	$sql_fields[] = " iu_ext_1.CountryCode ";
	$sql_fields[] = " iu_ext_1.Province ";
	$sql_fields[] = " IFNULL(iu.MobileTelNo, iu.HomeTelNo) Tel ";
	$sql_fields[] = " iu_ext_1.R_AreaCode ";
	$sql_fields[] = " iu_ext_1.R_AreaText ";

	$BasicColumnType[] = array('AREA', 'AreaCode');
	$BasicColumnType[] = array('ROAD', 'Road');
	$BasicColumnType[] = array('ADDRESS', 'Address');
	$BasicColumnType[] = array('FATHER', 'FatherName');
	$BasicColumnType[] = array('MOTHER', 'MotherName');
	$BasicColumnType[] = array('F_PROF', 'FatherOccupation');
	$BasicColumnType[] = array('M_PROF', 'MotherOccupation');
	$BasicColumnType[] = array('GUARD', 'GuardianType');
	$BasicColumnType[] = array('LIVE_SAME', 'GuardianIsLiveTogether');
	$BasicColumnType[] = array('EC_NAME', 'EmergencyContactName');

	$sql_fields[] = " iu_ext_1.AreaCode ";
	$sql_fields[] = " iu_ext_1.Road ";
	$sql_fields[] = " iu_ext_1.Address ";
	$sql_fields[] = " IFNULL(i_gs_father.ChName, i_gs_father.EnName) FatherName ";
	$sql_fields[] = " IFNULL(i_gs_mother.ChName, i_gs_mother.EnName) MotherName ";
	$sql_fields[] = " i_gs_ext_1_father.Occupation FatherOccupation ";
	$sql_fields[] = " i_gs_ext_1_mother.Occupation MotherOccupation ";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, 'F',
											IF(i_gs_mother.IsMain=1, 'M',
												IF(i_gs_main.IsMain=1, 'O',
													''
												)
											)
										) GuardianType";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.IsLiveTogether,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.IsLiveTogether,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.IsLiveTogether,
													''
												)
											)
										) GuardianIsLiveTogether";
	$sql_fields[] = " IFNULL(i_gs_ec.ChName, i_gs_ec.EnName) EmergencyContactName ";

	$BasicColumnType[] = array('EC_REL', 'EmergencyContact_RelationString');
	$BasicColumnType[] = array('EC_TEL', 'EmergencyContact_Tel');
	$BasicColumnType[] = array('EC_AREA', 'EmergencyContact_AreaCode');
	$BasicColumnType[] = array('EC_ROAD', 'EmergencyContact_Road');
	$BasicColumnType[] = array('EC_ADDRESS', 'EmergencyContact_Address');
	$BasicColumnType[] = array('S_CODE', 'SCode');
	$BasicColumnType[] = array('GRADE', 'Grade');
	$BasicColumnType[] = array('CLASS', 'WebSAMSClassCode');
	$BasicColumnType[] = array('C_NO', 'ClassNumber');
	$BasicColumnType[] = array('G_NAME', 'OM_GuardianName');//OM--Other / Main 

	$ec_relationSql = " CASE i_gs_ec.Relation ";
	foreach($ec_guardian AS $key =>$relation_name){
		$ec_relationSql .= " WHEN '$key' THEN '$relation_name' ";
	}
	$ec_relationSql .= " ELSE '' END EmergencyContact_RelationString ";
	$sql_fields[] = $ec_relationSql;
	$sql_fields[] = " IFNULL(i_gs_ec.EmPhone, i_gs_ec.Phone) EmergencyContact_Tel ";
	$sql_fields[] = " i_gs_ext_1_ec.AreaCode EmergencyContact_AreaCode ";
	$sql_fields[] = " i_gs_ext_1_ec.Road EmergencyContact_Road ";
	$sql_fields[] = " i_gs_ext_1_ec.Address EmergencyContact_Address ";
	$sql_fields[] = "'".$SchoolID."' SCode";
	$sql_fields[] = "icl.WebSAMSLevel Grade";
	$sql_fields[] = "ic.WebSAMSClassCode WebSAMSClassCode";

	$sql_fields[] = " iu.ClassNumber ";
	$sql_fields[] = " IFNULL(i_gs_main.ChName, i_gs_main.EnName) OM_GuardianName ";

	$BasicColumnType[] = array('G_RELATION', 'OM_Guardian_RelationString');
	$BasicColumnType[] = array('G_PROFESSION', 'OM_Guardian_Occupation');
	$BasicColumnType[] = array('G_AREA', 'Guardian_AreaCode');
	$BasicColumnType[] = array('G_ROAD', 'Guardian_Road');
	$BasicColumnType[] = array('G_ADDRESS', 'Guardian_Address');
	$BasicColumnType[] = array('G_TEL', 'Guardian_Tel');
	$BasicColumnType[] = array('GUARDMOBILE', 'Guardian_Mobile');

	$om_relationSql = " CASE i_gs_main.Relation ";
	foreach($ec_guardian AS $key =>$relation_name){
		$om_relationSql .= " WHEN '$key' THEN '$relation_name' ";
	}
	$om_relationSql .=  " ELSE '' END OM_Guardian_RelationString ";
	$sql_fields[] = $om_relationSql;
	$sql_fields[] = " i_gs_ext_1_main.Occupation OM_Guardian_Occupation ";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.AreaCode,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.AreaCode,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.AreaCode,
													''
												)
											)
										) Guardian_AreaCode";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.Road,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.Road,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.Road,
													''
												)
											)
										) Guardian_Road";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_ext_1_father.Address,
											IF(i_gs_mother.IsMain=1, i_gs_ext_1_mother.Address,
												IF(i_gs_main.IsMain=1, i_gs_ext_1_main.Address,
													''
												)
											)
										) Guardian_Address";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_father.Phone,
											IF(i_gs_mother.IsMain=1, i_gs_mother.Phone,
												IF(i_gs_main.IsMain=1, i_gs_main.Phone,
													''
												)
											)
										) Guardian_Tel";
	$sql_fields[] = " IF(i_gs_father.IsMain=1, i_gs_father.EmPhone,
											IF(i_gs_mother.IsMain=1, i_gs_mother.EmPhone,
												IF(i_gs_main.IsMain=1, i_gs_main.EmPhone,
													''
												)
											)
										) Guardian_Mobile";
	//$sql_fields[] = "
	
	$cols = 45;
}

$lsx->changeSetting($Setting);


$li = new libdb();

$namefield = getNameFieldByLang("iu.");
$namefield2= getNameFieldWithClassNumberByLang("iu.");
$cond="";
$cond_keyword="";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$delim="";
$class_str = '';
for($i=0;$i<sizeof($ClassID);$i++){
        $class_str .= $delim."'".$ClassID[$i]."'";
        $delim=",";
}
$cond = " AND ic.ClassName IN ($class_str) ";
if($keyword!=""){
        $cond_keyword=" AND ($namefield2 LIKE '%".addslashes($keyword)."%' OR iu.UserLogin LIKE '%$keyword%') ";
}else{
        $cond_keyword = "";
}
if($ts){
        $cond_ts =	"	AND
												(	iu.DateModified >= '$ts' OR iu_ext_1.DateModified >= '$ts' OR
													i_gs_father.ModifiedDate >= '$ts' OR i_gs_ext_1_father.ModifiedDate >= '$ts' OR
													i_gs_mother.ModifiedDate >= '$ts' OR i_gs_ext_1_mother.ModifiedDate >= '$ts' OR
													i_gs_ec.ModifiedDate >= '$ts' OR i_gs_ext_1_ec.ModifiedDate >= '$ts' OR
													i_gs_main.ModifiedDate >= '$ts' OR i_gs_ext_1_main.ModifiedDate >= '$ts')
										";
}else{
        $cond_ts = "";
}


$sql =	"
					SELECT
						".implode(', ' ,$sql_fields)."
					FROM
						INTRANET_USER as iu
					INNER JOIN
						INTRANET_CLASS as ic
					ON
						iu.ClassName = ic.ClassName AND
						ic.RecordStatus = 1
					LEFT OUTER JOIN
						INTRANET_CLASSLEVEL as icl
					ON
						ic.ClassLevelID = icl.ClassLevelID
					LEFT OUTER JOIN
						INTRANET_USER_EXT_1 as iu_ext_1
					ON
						iu.UserID = iu_ext_1.UserID

					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_father
					ON
						iu.UserID = i_gs_father.UserID AND
						LPAD(i_gs_father.Relation,2,0) = '01'
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_father
					ON
						i_gs_father.RecordID = i_gs_ext_1_father.RecordID

					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_mother
					ON
						iu.UserID = i_gs_mother.UserID AND
						LPAD(i_gs_mother.Relation,2,0) = '02'
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_mother
					ON
						i_gs_mother.RecordID = i_gs_ext_1_mother.RecordID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_ec
					ON
						iu.UserID = i_gs_ext_1_ec.UserID AND
						i_gs_ext_1_ec.IsEmergencyContact = 1 AND
						i_gs_ext_1_ec.RecordID != i_gs_father.RecordID AND
						i_gs_ext_1_ec.RecordID != i_gs_mother.RecordID
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_ec
					ON
						i_gs_ec.RecordID = i_gs_ext_1_ec.RecordID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_main
					ON
						iu.UserID = i_gs_main.UserID AND
						i_gs_main.IsMain = 1 AND
						NOT (LPAD(i_gs_main.Relation,2,0) = '01' OR LPAD(i_gs_main.Relation,2,0) = '02')
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_main
					ON
						i_gs_main.RecordID = i_gs_ext_1_main.RecordID
					
					WHERE
						iu.RecordType=2 AND
						iu.RecordStatus IN (0,1,2)
						$cond
						$cond_keyword
						$cond_ts
				";

$field_array = array("$namefield","iu.ClassName","iu.ClassNumber","iu.WebSAMSRegNo", "iu_ext_1.AuthCodeMain", "iu_ext_1.AuthCodeCheck");
$sql .= " ORDER BY ".$field_array[$field].( $order==0 ? " DESC" : " ASC");
$result = $li->returnArray($sql);
/*
$XMLData = array();
$XMLData[$RootTag] = array();
for($i=0; $i<count($result); $i++){
	$tmpData = array();
	for($j=0; $j<count($BasicColumnType); $j++){
		$tmpData[$BasicColumnType[$j][0]][]['data'] = $result[$i][$BasicColumnType[$j][1]];
	}
	$tmpData2 = array();
	$tmpData2[$RowTag][] = $tmpData;
	$XMLData[$RootTag][] = $tmpData2;
}

$lsx->setXMLData($XMLData);
$xml_result = $lsx->getXML();


echo 'abc';
echo mysql_error();
//echo '<br/><textarea cols=120 rows=15>';
//echo $xml_result;
//echo '</textarea><br/>';
echo '<pre>';
//var_dump($XMLData);

echo $sql;
echo '</pre>';
die;


$lsx->exportFile($output_file_name,$xml_result);
*/

$display = "<tr>";
$display .= $ExportType==3?"<td class=tableTitle width=130 nowrap>$i_WebSAMS_Registration_No</td>":"";
$display .= "<td class=tableTitle width=60 nowrap>$i_StudentRegistry_AuthCodeMain</td>";
$display .= $ExportType==3?"<td class=tableTitle width=60 nowrap>$i_UserChineseName</td>":"";
$display .= "<td class=tableTitle width=60 nowrap>$i_UserEnglishName</td>";
$display .= "<td class=tableTitle width=80 nowrap>$i_UserClassName</td>";
$display .= "<td class=tableTitle width=80 nowrap>$i_UserClassNumber</td>";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_UserGender</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_UserDateOfBirth</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_PlaceOfBirth</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_Type</td>":"";
$display .= "<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_No</td>";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_IssuePlace</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_IssueDate</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ID_ValidDate</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_SP_Type</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_SP_IssueDate</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_SP_ValidDate</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_UserCountry</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_Province</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_UserHomeTelNo</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialArea_Night</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialArea_Night (".$i_StudentRegistry_ResidentialAreaNight_Code["O"].")</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialArea</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialRoad</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentRegistry_ResidentialAddress</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>".$ec_guardian['01']."<br>$i_UserChineseName</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>".$ec_guardian['02']."<br>$i_UserChineseName</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>".$ec_guardian['01']."<br>$i_StudentGuardian_Occupation</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>".$ec_guardian['02']."<br>$i_StudentGuardian_Occupation</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_StudentGuardian_LiveTogether</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserChineseName</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>".$ec_iPortfolio['relation']."</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_StudentGuardian_EMPhone</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserAddress_Area</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserAddress_Road</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_EmergencyContact<br>$i_UserAddress_Address</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserChineseName</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>".$ec_iPortfolio['relation']."</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_StudentGuardian_Occupation</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserAddress_Area</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserAddress_Road</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserAddress_Address</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_UserHomeTelNo</td>":"";
$display .= $ExportType==3?"<td class=tableTitle width=80 nowrap>$i_StudentGuardian_MainContent<br>$i_StudentGuardian_EMPhone</td>":"";
$display .= "</tr>\n";

$RecordRow = '';
$invalidRecordRows = 0;

for($i=0; $i<count($result); $i++)
{
  $error = array_fill(0,10,false);

  switch($ExportType)
  {
    case 3:
      //criteria 1: S6_TYPE not empty, S6_IDATE not empty
      if(trim($result[$i]['SP_Type']) != "")
      {
        if(trim($result[$i]['SP_IssueDate']) == "")
          $error[0] = true;
      }
      
      //criteria 2: S6_TYPE = [2,3], S6_VDATE not empty
      if($result[$i]['SP_Type'] == 2 || $result[$i]['SP_Type'] == 3)
      {
        if(trim($result[$i]['SP_ValidDate']) == "")
          $error[1] = true;
      }
      
      //criteria 3: R_AREA not empty, RA_DESC not empty
      if(trim($result[$i]['R_AreaCode']) == "O")
      {
        if(trim($result[$i]['R_AreaText']) == "")
          $error[2] = true;
      }
      
      //criteria 4: Emergency contact all filled or all empty
      if(trim($result[$i]['EmergencyContactName']) == ""
        && trim($result[$i]['EmergencyContact_RelationString']) == ""
        && trim($result[$i]['EmergencyContact_Tel']) == ""
        && trim($result[$i]['EmergencyContact_AreaCode']) == ""
        && trim($result[$i]['EmergencyContact_Road']) == ""
        && trim($result[$i]['EmergencyContact_Address']) == ""  
      )
        $error[3] = false;
      else if(trim($result[$i]['EmergencyContactName']) != ""
        && trim($result[$i]['EmergencyContact_RelationString']) != ""
        && trim($result[$i]['EmergencyContact_Tel']) != ""
        && trim($result[$i]['EmergencyContact_AreaCode']) != ""
        && trim($result[$i]['EmergencyContact_Road']) != ""
        && trim($result[$i]['EmergencyContact_Address']) != ""  
      )
        $error[3] = false;
      else
        $error[3] = true;
        
      //criteria 9: GUARD='O', G_NAME, G_RELATION, G_PROFESSION not empty
      if($result[$i]['GuardianType'] == "O")
      {
        if(trim($result[$i]['OM_GuardianName']) == ""
          || trim($result[$i]['OM_Guardian_RelationString']) == ""
          || trim($result[$i]['OM_Guardian_Occupation']) == ""
        )
        $error[8] = true;
      }
    
      //criteria 10: LIVE_SAME=0, G_AREA, G_ROAD, G_ADDRESS, G_TEL not empty
      if($result[$i]['GuardianIsLiveTogether'] == "0")
      {
        if(empty($result[$i]['Guardian_AreaCode'])
          || empty($result[$i]['Guardian_Road'])
          || empty($result[$i]['Guardian_Address'])
          || empty($result[$i]['Guardian_Tel'])
        )
        $error[9] = true;
      }
    
    case 2:
      //criteria 5: S_CODE not empty
      if(trim($result[$i]['SCode']) == "")
      {
        $error[4] = true;
      }
  
      //criteria 6: GRADE not empty
      if(trim($result[$i]['Grade']) == "")
      {
        $error[5] = true;
      }

      //criteria 7: CLASS not empty
      if(trim($result[$i]['WebSAMSClassCode']) == "")
      {
        $error[6] = true;
      }
    
      //criteria 8: C_NO not empty
      if(trim($result[$i]['ClassNumber']) == "")
      {
        $error[7] = true;
      }
  }


  
  
  if(count(array_filter($error)) > 0)
  {
    $RecordRow .= "<tr>";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['WebSAMSRegNo']):"";
    $RecordRow .= RowDisplay("",$result[$i]['Code']);
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['ChineseName']):"";
    $RecordRow .= RowDisplay("",$result[$i]['EnglishName']);
    $RecordRow .= '<td nowrap>';
    $RecordRow .= $error[4]?"<span style=\"color:red\">-</span>":$result[$i]['SCode']."/";
    $RecordRow .= $error[5]?"<span style=\"color:red\">-</span>":$result[$i]['Grade']."/";
    $RecordRow .= $error[6]?"<span style=\"color:red\">-</span>":$result[$i]['WebSAMSClassCode'];
    $RecordRow .= '</td>';
    $RecordRow .= RowDisplay($error[7],$result[$i]['ClassNumber']);
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['Gender']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['DateOfBirth']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_PlaceOfBirth_Code[$result[$i]['PlaceOfBirthCode']]):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_ID_Type_Code[$result[$i]['ID_Type']]):"";
    $RecordRow .= RowDisplay("",$result[$i]['ID_Num']);
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_ID_IssuePlace_Code[$result[$i]['ID_IssuePlace']]):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['ID_IssueDate']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['ID_ValidDate']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_SP_Type_Code[$result[$i]['SP_Type']]):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[0],$result[$i]['SP_IssueDate']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[1],$result[$i]['SP_ValidDate']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_Country_Code[$result[$i]['CountryCode']]):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['Province']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['Tel']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_ResidentialAreaNight_Code[$result[$i]['R_AreaCode']]):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[2],$result[$i]['R_AreaText']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$i_StudentRegistry_AreaCode[$result[$i]['AreaCode']]):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['Road']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['Address']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['FatherName']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['MotherName']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['FatherOccupation']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['MotherOccupation']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['GuardianType']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['GuardianIsLiveTogether']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[3],$result[$i]['EmergencyContactName']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[3],$result[$i]['EmergencyContact_RelationString']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[3],$result[$i]['EmergencyContact_Tel']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[3],$i_StudentRegistry_AreaCode[$result[$i]['EmergencyContact_AreaCode']]):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[3],$result[$i]['EmergencyContact_Road']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[3],$result[$i]['EmergencyContact_Address']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[8],$result[$i]['OM_GuardianName']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[8],$result[$i]['OM_Guardian_RelationString']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[8],$result[$i]['OM_Guardian_Occupation']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[9],$i_StudentRegistry_AreaCode[$result[$i]['Guardian_AreaCode']]):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[9],$result[$i]['Guardian_Road']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[9],$result[$i]['Guardian_Address']):"";
    $RecordRow .= $ExportType==3?RowDisplay($error[9],$result[$i]['Guardian_Tel']):"";
    $RecordRow .= $ExportType==3?RowDisplay("",$result[$i]['Guardian_Mobile']):"";
    $RecordRow .= "</tr>\n";
    
    $invalidRecordRows++;
  }
  
}

$display .= "<tr><td colspan=\"$cols\">".(str_replace("<--NoOfValid-->", (count($result) - $invalidRecordRows), $i_StudentRegistry_Export_Valid_Record))."</td></tr>";
if($invalidRecordRows > 0){
	$expeect_result_msg = $i_StudentRegistry_Export_Invalid_Record.'<br>'.$i_StudentRegistry_Export_Invalid_Reason;
	$display .= "<tr><td class=tableTitle colspan=\"45\" >$i_general_incomplete (".$invalidRecordRows.")</td></tr>";
	$display .= $RecordRow;
}


include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
?>

<script language="javascript">

function jEXPORT(){
	document.form1.submit();
}

</script>

<?= displayNavTitle($i_admintitle_am, '', $i_StudentRegistry['System'], 'index.php',$button_search, 'javascript:history.back()', $button_export_xml, '') ?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<form name="form1" action="export_xml_update.php" method="post">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><?=$expeect_result_msg ?></td>
	</tr>
</table>

<table width="100%" border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
	<?=$display ?>
</table>

<input type=hidden name=pageNo value="<?=$pageNo ?>" />
<input type=hidden name=order value="<?=$order ?>" />
<input type=hidden name=field value="<?=$field ?>" />
<input type=hidden name=keyword value="<?=$keyword ?>" />
<input type=hidden name=ts value="<?=$ts ?>" />
<input type=hidden name=ExportType value="<?=$ExportType ?>" />

<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."' />";
}
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td align="right">
			<img src="/images/admin/button/s_btn_export_<?=$intranet_session_language?>.gif" border="0" onclick="jEXPORT();" />
			<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
		</td>
	</tr>
</table>

</form>

<?php

intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
