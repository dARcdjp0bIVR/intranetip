<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "")
	$page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

//error_reporting(E_ALL);

$namefield = getNameFieldByLang("iu.");
$namefield2 = getNameFieldWithClassNumberByLang("iu.");
$cond = "";
$cond_keyword = "";

$order = ($order == 1) ? 1 : 0;
if($field == "")
	$field = 0;

$delim="";
$class_str = '';
for($i=0;$i<sizeof($ClassID);$i++){
	$class_str .= $delim."'".$ClassID[$i]."'";
	$delim = ",";
}
$cond = " AND ic.ClassName IN ($class_str) ";
if($keyword!=""){
	$cond_keyword=" AND ($namefield2 LIKE '%".addslashes($keyword)."%' OR iu.UserLogin LIKE '%$keyword%') ";
}
else{
	$cond_keyword = "";
}
if($ts){
	# try to reformat it
	if(strlen($ts)<10){
		$ts_parts = explode('-',$ts);
		if(count($ts_parts)>0 && $ts_parts[0]!=''){
			$ts = $ts_parts[0];
		}
		else{
			$ts = date('Y');
		}
		$ts .= '-';
		if(count($ts_parts)>1 && $ts_parts[1]!=''){
			$ts .= $ts_parts[1];
		}
		else{
			$ts .= '01';
		}
		$ts .= '-';
		if(count($ts_parts)>2 && $ts_parts[2]!=''){
			$ts .= $ts_parts[2];
		}
		else{
			$ts .= '01';
		}
	}
	$ts = date('Y-m-d H:i:s',strtotime($ts));
	$cond_ts =	"
								AND
									(	iu.DateModified >= '$ts' OR iu_ext_1.DateModified >= '$ts' OR
										i_gs_father.ModifiedDate >= '$ts' OR i_gs_ext_1_father.ModifiedDate >= '$ts' OR
										i_gs_mother.ModifiedDate >= '$ts' OR i_gs_ext_1_mother.ModifiedDate >= '$ts' OR
										i_gs_ec.ModifiedDate >= '$ts' OR i_gs_ext_1_ec.ModifiedDate >= '$ts' OR
										i_gs_main.ModifiedDate >= '$ts' OR i_gs_ext_1_main.ModifiedDate >= '$ts'
									)
							";
}
else{
	$cond_ts = "";
}

/*
//debug use
$debug_mod_1 = " IF (iu.DateModified >= IFNULL(iu_ext_1.DateModified,''), IFNULL(iu.DateModified,''), IFNULL(iu_ext_1.DateModified,'')) ";	
$debug_mod_2 = " IF (IFNULL(i_gs_father.ModifiedDate,'') >= IFNULL(i_gs_ext_1_father.ModifiedDate,''), IFNULL(i_gs_father.ModifiedDate,''), IFNULL(i_gs_ext_1_father.ModifiedDate,'')) ";	
$debug_mod_3 = " IF (IFNULL(i_gs_mother.ModifiedDate,'') >= IFNULL(i_gs_ext_1_mother.ModifiedDate,''), IFNULL(i_gs_mother.ModifiedDate,''), IFNULL(i_gs_ext_1_mother.ModifiedDate,'')) ";	
$debug_mod_4 = " IF (IFNULL(i_gs_ec.ModifiedDate,'') >= IFNULL(i_gs_ext_1_ec.ModifiedDate,''), IFNULL(i_gs_ec.ModifiedDate,''), IFNULL(i_gs_ext_1_ec.ModifiedDate,'')) ";	
$debug_mod_5 = " IF (IFNULL(i_gs_main.ModifiedDate,'') >= IFNULL(i_gs_ext_1_main.ModifiedDate,''), IFNULL(i_gs_main.ModifiedDate,''), IFNULL(i_gs_ext_1_main.ModifiedDate,'')) ";

$debug_mod_12 = " IF ($debug_mod_1 >= $debug_mod_2, $debug_mod_1,$debug_mod_2) ";	
$debug_mod_34 = " IF ($debug_mod_3 >= $debug_mod_4, $debug_mod_3,$debug_mod_4) ";	
$debug_mod_1234 = " IF ($debug_mod_12 >= $debug_mod_34, $debug_mod_12,$debug_mod_34) ";	
$debug_mod_12345 = " IF ($debug_mod_1234 >= $debug_mod_5, $debug_mod_1234,$debug_mod_5) ";	

$debug_mod = $debug_mod_1;
*/
$sql =	"
					SELECT
						CONCAT('<a href=\"javascript:editRegistry(',iu.UserID,')\">',$namefield,'</a>'),
						iu.ClassName,
						iu.ClassNumber,
						iu.WebSAMSRegNo,
						iu_ext_1.AuthCodeMain,
						iu_ext_1.AuthCodeCheck,
						CONCAT('<input type=checkbox name=StudentID[] value=',iu.UserID,'>')
					FROM
						INTRANET_USER as iu
					INNER JOIN
						INTRANET_CLASS as ic
					ON
						iu.ClassName = ic.ClassName AND
						ic.RecordStatus = 1
					LEFT OUTER JOIN
						INTRANET_USER_EXT_1 as iu_ext_1
					ON
						iu.UserID = iu_ext_1.UserID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_father
					ON
						iu.UserID = i_gs_father.UserID AND
						LPAD(i_gs_father.Relation,2,0) = '01'
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_father
					ON
						i_gs_father.RecordID = i_gs_ext_1_father.RecordID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_mother
					ON
						iu.UserID = i_gs_mother.UserID AND
						LPAD(i_gs_mother.Relation,2,0) = '02'
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_mother
					ON
						i_gs_mother.RecordID = i_gs_ext_1_mother.RecordID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_ec
					ON
						iu.UserID = i_gs_ext_1_ec.UserID AND
						i_gs_ext_1_ec.IsEmergencyContact = 1 AND
						i_gs_ext_1_ec.RecordID != i_gs_father.RecordID AND
						i_gs_ext_1_ec.RecordID != i_gs_mother.RecordID
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_ec
					ON
						i_gs_ec.RecordID = i_gs_ext_1_ec.RecordID
						
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT as i_gs_main
					ON
						iu.UserID = i_gs_main.UserID AND
						i_gs_main.IsMain = 1 AND
						NOT
							(	LPAD(i_gs_main.Relation,2,0) = '01' OR
								LPAD(i_gs_main.Relation,2,0) = '02')
					LEFT OUTER JOIN
						$eclass_db.GUARDIAN_STUDENT_EXT_1 as i_gs_ext_1_main
					ON
						i_gs_main.RecordID = i_gs_ext_1_main.RecordID
						
					WHERE
						iu.RecordType=2 AND
						iu.RecordStatus IN (0,1,2)
						$cond
						$cond_keyword
						$cond_ts
				";
/*
var_dump($_GET);
echo '<textarea cols="100" rows=20>';
echo $sql;
echo '</textarea>';
*/
$li = new libdbtable($field, $order, $pageNo);

$li->field_array = array("$namefield","iu.ClassName","iu.ClassNumber","iu.WebSAMSRegNo", "iu_ext_1.AuthCodeMain", "iu_ext_1.AuthCodeCheck");
	
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=5% class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassNumber)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_WebSAMS_Registration_No)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_StudentRegistry_AuthCodeMain)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_StudentRegistry_AuthCodeCheck)."</td>\n";
$li->column_list .= "<td width=5% class=tableTitle>".$li->check("StudentID[]")."</td>\n";

$toolbar = "<a class=iconLink href=javascript:checkGet(document.form1,'export_xml.php')>".exportIcon()."$button_export_xml</a>\n".toolBarSpacer();

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'StudentID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

	
if(sizeof($ClassID)){	
	$title = implode(",",$ClassID);	
}
?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentRegistry['System'], 'index.php',$button_search,'search.php') ?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>
function editRegistry(studentid){
	//document.form1.action = 'edit.php?SID='+studentid;
	document.form1.SID.value=studentid;
	document.form1.action='edit.php';
	document.form1.submit();
}
</script>

<form name="form1" method="GET">

<table  width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><?=$i_StudentGuardian_Searching_Area?>:</td>
	</tr>
	<tr>
		<td><?=$title?></td>
	</tr>
<?php
if($keyword){
	echo '<tr><td>'.($i_UserName." / ".$i_UserLogin." / ".$i_ClassNameNumber).':</td></tr>';
	echo '<tr><td>'.stripslashes($keyword).'</td></tr>';
}
if($ts){
	echo '<tr><td>'.$i_StudentRegistry_ModifiedSince.':</td></tr>';
	echo '<tr><td>'.$ts.'</td></tr>';
}
?>
	<tr>
		<td height=20></td>
	</tr>
</table>
	
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0" /></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?=$li->displayFunctionbar("-", "-", "$toolbar", $searchbar) ?></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?=$li->displayFunctionbar("-", "-", "$toolbar2", $functionbar) ?></td>
	</tr>
	<tr>
		<td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0 /></td>
	</tr>
</table>

<?=$li->display() ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0" /></td>
	</tr>
</table>

<br /><br />
<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."'>";
}
?>

<input type=hidden name=SID value='' />
<input type=hidden name=keyword value="<?=$keyword?>" />
<input type=hidden name=ts value="<?=$ts?>" />
<input type=hidden name=pageNo value="<?=$li->pageNo ?>" />
<input type=hidden name=order value="<?=$li->order ?>" />
<input type=hidden name=field value="<?=$li->field ?>" />
<input type=hidden name=page_size_change value="" />
<input type=hidden name=numPerPage value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
