<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$linterface = new interface_html();
$limport = new libimporttext();

?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentRegistry['System'], 'index.php',$button_import_xml,'') ?>
<?= displayTag("head_registry_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="POST" action="import_xml_confirm.php" enctype="multipart/form-data">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=2>
	<tr>
		<td align=right nowrap><?=$i_select_file ?>:</td>
		<td>
			<input type=file size=50 name=userfile /><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX() ?>
		</td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr>
		<td colspan=2>
			<?=$i_StudentRegistry_Import_XML_Format?><br />&nbsp;
			<a class=functionlink_new href="./sample_stud3.xml" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a><br />&nbsp;
			<a class=functionlink_new href="./ref_sra3.dtd" target=_blank>[<?=$i_general_clickheredownloaddtd?>]</a>
		</td>
	</tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1 /></td>
	</tr>
	<tr>
		<td align="right">
			<?= btnSubmit() ?>
			<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
		</td>
	</tr>
</table>

</form>

<?php
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
