<?php
include("../../includes/global.php");
include("../../lang/email.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.webmaster, "<?php echo $i_alert_pleasefillin.$i_EmailWebmaster; ?>.")) return false;
}
</script>

<form name="form1" action="update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sa, '', $i_admintitle_sa_email, '') ?>
<?= displayTag("head_email_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<br><br>
<p><?php echo $i_EmailWebmaster; ?><br><input class=text type=text name=webmaster size=40 maxlength=50 value="<?php echo $webmaster; ?>">
<br><br><br>
</blockquote>
</td>
</tr>
<tr><td></td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>