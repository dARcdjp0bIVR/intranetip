<?php
// Editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libgeneralsettings.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/eclass_api/libeclass_api.php");

intranet_opendb();
$libgeneralsettings = new libgeneralsettings();
if($_POST['task'] == 'token_generate')
{
	$token = generateRandomString();
	$update_success = $libgeneralsettings->Save_General_Setting('HKSSF',array('access_token' => $token ));
	echo $update_success ?  $token : 0;
	exit();
}
$update_success = $libgeneralsettings->Save_General_Setting('HKSSF',array('avaliable' => $_POST['avaliable']));
$msg = $update_success ? 2:13;

intranet_closedb();
header("Location: index.php?msg=$msg");

function generateRandomString($length = 16) {
	$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
?>