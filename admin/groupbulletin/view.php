<?php 
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libbulletin.php");
include("../../lang/lang.$intranet_session_language.php"); 
include("../../templates/fileheader_admin.php"); 
intranet_opendb();

$UserID = 0;
$ThreadID = (!isset($ThreadID)) ? -1 : $ThreadID;
$ThreadID += 0;
$li = new libbulletin($BulletinID, $ThreadID);
?>

<script language="javascript">
function bulletin_thread(id){
	obj = document.form1;
	obj.ThreadID.value = id;
	obj.submit();
}
</script>

<form name=form1 method=get>
<p><?php echo $li->displayMessage(); ?>
<p><?php echo $li->displayThread(); ?>
<input type=hidden name=BulletinID value="<?php echo $BulletinID; ?>">
<input type=hidden name=ThreadID value="<?php echo $ThreadID; ?>">
<input type=hidden name=ThreadSize value="<?php echo $li->replyThreadSize(); ?>">
</form>

<?php
intranet_closedb();
include("../../templates/filefooter.php"); 
?>
