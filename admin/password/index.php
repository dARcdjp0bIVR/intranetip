<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.OldPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_old; ?>")) return false;
     if(!check_text(obj.NewPassword, "<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) return false;
     if(!checkRegEx(obj.NewPassword.value,"<?php echo $i_alert_pleasefillin.$i_frontpage_myinfo_password_new; ?>")) {
          obj.NewPassword.value = "";
          obj.ReNewPassword.value="";
          obj.NewPassword.focus();
          return false;
     }
     if(!check_text(obj.ReNewPassword, "<?php echo $i_frontpage_myinfo_password_retype; ?>")) return false;
     if(obj.NewPassword.value!=obj.ReNewPassword.value){
          alert("<?php echo $i_frontpage_myinfo_password_mismatch; ?>");
          obj.NewPassword.value="";
          obj.ReNewPassword.value="";
          obj.NewPassword.focus();
          return false;
     }
}
</script>

<form name="form1" action="update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sa, '', $i_admintitle_sa_password, '') ?>
<?= displayTag("head_password_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<p><br><br>
<p><?php echo $i_frontpage_myinfo_password_old; ?><br><input class=password type=password name=OldPassword size=10 maxlength=50>
<p><?php echo $i_frontpage_myinfo_password_new; ?><br><input class=password type=password name=NewPassword size=10 maxlength=50> <span class='extraInfo'>(0-9a-zA-Z)</span>
<p><?php echo $i_frontpage_myinfo_password_retype; ?><br><input class=password type=password name=ReNewPassword size=10 maxlength=50>
<br><br><br>
</blockquote>
</td>
</tr>
<tr><td></td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
</td>
</tr>
</table>
</form>


<?php
include("../../templates/adminfooter.php");
?>