<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");

$OldPassword = htmlspecialchars(trim(substr($OldPassword,0,50)));
$NewPassword = htmlspecialchars(trim(substr($NewPassword,0,50)));

$li = new libaccount();
$msg = ($li->update_user($OldPassword, $NewPassword)==1) ? 5 : 6;

header("Location: index.php?msg=$msg");
?>