<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libalbum.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$lo = new libgrouping();
$li = new libalbum($album_id);
$li->db = $intranet_db;
$user_list = $li->returnUserAllowedList();
$list = $li->returnAlbumList();


$Title = htmlspecialchars(trim($Title));
$Description = htmlspecialchars(trim($Description));
$DisplayOrder = htmlspecialchars(trim($DisplayOrder));
$album_id = htmlspecialchars(trim($album_id));
$parent_id = htmlspecialchars(trim($parent_id));

?>

<script language="javascript">
function checkform(obj){
        checkOptionAll(obj.elements["GroupID[]"]);      
        checkOptionAll(obj.elements["child[]"]); 
}
</script>

<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<!--<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, 'javascript:history.back()', $button_new, '') ?> -->
<?= $li->displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_photoalbum, '/admin/photoalbum/', $list, $button_edit, '') ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align="right"><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayPhotoalbumGroups($album_id); ?></td></tr>
<tr><td align="right"><?=$i_AlbumUserGroup?>:</td>
<td>
<table width=100% border=0 cellspacing=1 cellpadding=1>
<tr><td width=1>
<select name=child[] size=4 multiple>
<?php 
	for($i = 0; $i < sizeof($user_list); $i++) { 		
		echo "<option value={$user_list[$i][1]}>{$user_list[$i][0]}</option>\n";
	} 
	echo "<option>";
	for ($i=sizeof($user_list); $i<40; $i++) {
		echo "&nbsp;";
	}
	echo "</option>";		
?>
</select>		
</td><td>
<a href="javascript:newWindow('choose/index.php?fieldname=child[]',2)"><img src="/images/admin/button/s_btn_select_std_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['child[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</td></tr>

</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.go(-2)"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type="hidden" name="Title" value="<?=$Title?>">
<input type="hidden" name="Description" value="<?=$Description?>">
<input type="hidden" name="DisplayOrder" value="<?=$DisplayOrder?>">
<input type="hidden" name="AccessType" value="2">
<input type="hidden" name="parent_id" value="<?=$parent_id?>">
<input type="hidden" name="album_id" value="<?=$album_id?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>