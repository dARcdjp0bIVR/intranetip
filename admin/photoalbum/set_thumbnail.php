<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libalbum.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libalbum($album_id);
$li->db = $intranet_db;
$li->setThumbnail($ItemID[0]);

intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count");
?>