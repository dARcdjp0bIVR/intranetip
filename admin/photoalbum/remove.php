<?php

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libalbum.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lf = new libfilesystem(); 
$li = new libalbum();
$li->db = $intranet_db;

$AlbumID = $_POST['AlbumID'];
$ParentID = $_REQUEST['parent_id'];

for ($i=0; $i<sizeof($AlbumID); $i++) {	
	$li->removeAlbum($AlbumID[$i]);
}

intranet_closedb();
header("Location: index.php?album_id=$ParentID");


/*
$field = $_POST['field'];
$order = $_POST['order'];
$pageNo = $_POST['pageNo'];
$AlbumID = $_POST['AlbumID'];


$lib_file = new phpduoFileSystem();


$l_ca = new eventalbum($field, $order, $pageNo);

$l_ca->db = $intranet_db;

for ($i=0; $i<sizeof($AlbumID); $i++)
{
	$l_ca->removeAlbum($AlbumID[$i]);
}
*/

?>