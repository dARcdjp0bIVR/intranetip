<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libalbum.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$album_id = $_GET['album_id'];
$parent_id = $_GET['parent_id'];
global $file_path;

$field = (!isset($field) || $field == "") ? 2 : $field;

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 2; break;
}

$order = ($order == 1) ? 0 : 1;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;
$user_field = getNameFieldWithClassNumberByLang("b.");

$ParentID = (isset($_REQUEST['album_id']) && $_REQUEST['album_id']!="") ? $_REQUEST['album_id'] : 0;

$lo = new libalbum($_REQUEST['album_id']);
$lo->db = $intranet_db;
$album_list = $lo->returnAlbumList();
$album_name = $lo->returnAlbumName();

/*
$sql  = "SELECT
                        DATE_FORMAT(a.AnnouncementDate, '%Y-%m-%d'),
                        DATE_FORMAT(a.EndDate, '%Y-%m-%d'),
                        CONCAT('<a class=tableContentLink href=edit.php?AnnouncementID[]=', a.AnnouncementID, '>', a.Title, '</a>',' <a href=javascript:showRead(',a.AnnouncementID,')><img src=$image_path/icon_viewstatics.gif border=0 alt=\"$i_AnnouncementViewReadStatus\"></a>'),
                        IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0,
                            IF (a.UserID IS NOT NULL OR a.UserID != 0,$user_field,'$i_AnnouncementNoAnnouncer'),
                            '$i_AnnouncementSystemAdmin'),
                        IF (a.OwnerGroupID IS NOT NULL OR a.OwnerGroupID != 0, c.Title, '--'),
                        a.DateModified,
                        CONCAT('<input type=checkbox name=AnnouncementID[] value=', a.AnnouncementID ,'>')
                FROM
                        INTRANET_ANNOUNCEMENT as a LEFT OUTER JOIN INTRANET_USER as b ON b.UserID = a.UserID LEFT OUTER JOIN INTRANET_GROUP as c ON c.GroupID = a.OwnerGroupID
                WHERE
                        (a.Title like '%$keyword%') AND
                        a.RecordStatus = $filter
                ";
                */

$sql  = "SELECT
			CONCAT('<a href=\"javascript:viewPhoto(', ItemID, ')\">', '<img src=\"/includes/imagethumbnail.php?image=',Path, FileName, '\" width=100 border=0 ><br>', FileName, '</a>'),
			Description,
			DisplayOrder,
			CONCAT('<input type=checkbox name=ItemID[] value=', ItemID ,'>')
		FROM
			INTRANET_PHOTO_ITEM
		WHERE
			AlbumID=$album_id AND			
			(FileName like '%$keyword%' OR Description like '%$keyword%')
		";
          
          
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("FileName", "Description", "DisplayOrder");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_photoalbum;
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column(0, $i_AlbumName)."</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column(1, $i_AlbumDescription)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(2, $i_AlbumDisplayOrder)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("ItemID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1, 'photo_add.php')\">".newIcon()."$button_new</a>";
$toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1, 'photo_add_batch.php')\">".newIcon()." New Batch</a>";
if ($_REQUEST['count'] == 0) {
	$toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1, 'new.php')\">".newFolderIcon()."$button_new_album</a>";
}
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'ItemID[]','photo_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:swapPhoto()\"><img src='/images/admin/button/t_btn_swap_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:setThumbnail()\"><img src='/images/admin/button/t_btn_thumbnail_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'ItemID[]','photo_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>
<script language="javascript">
function viewPhoto(pid){
	newWindow('photo_view.php?album_id=<?=$album_id?>&photo_id='+pid, 16);
	return ;
}

function swapPhoto(){
	var obj = document.form1;

	// no of checked checkbox must not greater than 1
	if (countChecked(obj, "ItemID[]")!=2)
	{
		alert("<?=$jr_warning['album_photo_swap']?>");
		return ;
	} else
	{
		obj.action = "photo_swap.php";
		obj.submit();
	}

	return ;
}

function setThumbnail(){
	var obj = document.form1;
	
	if(countChecked(obj,"ItemID[]")==1) {
	    obj.action = "set_thumbnail.php";
	    obj.submit();
	} else {
	    alert(globalAlertMsg1);
	}
}		
</script>

<form name="form1" method="get">
<?= $lo->displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_photoalbum, '/admin/photoalbum/', $album_list) ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name='album_id' value="<?=$album_id?>">
<input type=hidden name='parent_id' value="<?=$album_id?>">
<input type=hidden name='album_name' value="<?=$album_name?>">
<input type=hidden name='count' value="<?=$count?>">

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
