<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libalbum.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$lo = new libgrouping();
$li = new libalbum($album_id);
$list = $li->returnAlbumList();
?>

<script language="javascript">
function checkform(obj){
	var accesstype;
	for (i=0; i<obj.AccessType.length; i++) {
		if (obj.AccessType[i].checked == true) {
			accesstype = obj.AccessType[i].value;
		}
	}	      
	 
	if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_AnnouncementTitle; ?>.")) return false;  
	if (accesstype == 2) {	    
	    document.form1.action = "new_album.php";
	}
        
}
</script>

<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<!--<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_announcement, 'javascript:history.back()', $button_new, '') ?>-->
<?= $li->displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_im_photoalbum, '/admin/photoalbum/', $list, $button_new, '') ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right nowrap width=100><?php echo $i_general_DisplayOrder; ?>:</td><td><input class=text type=text name=DisplayOrder size=10 maxlength=10 value=""></td></tr>
<tr><td align=right nowrap width=100><?php echo $i_AnnouncementTitle; ?>:</td><td><input class=text type=text name=Title value="" size=30 maxlength=100></td></tr>
<tr><td align=right nowrap width=100><?php echo $i_AlbumDescription; ?>:</td><td><textarea name=Description cols=60 rows=10><?=$d?></textarea></td></tr>
<tr><td align=right nowrap width=100><?= $i_AlbumAccessType; ?>:</td>
<td><input type=radio name=AccessType value=3 CHECKED> <?php echo $i_AlbumIntranet; ?></td></tr>
<tr><td align=right nowrap width=100>&nbsp;</td>
<td><input type=radio name=AccessType value=4> <?php echo $i_AlbumInternet; ?></td></tr>
<tr><td align=right nowrap width=100>&nbsp;</td>
<td><input type=radio name=AccessType value=2> <?php echo $i_AlbumSelectedGroupsUsers; ?></td></tr>


</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type="hidden" name="parent_id" value="<?=$album_id?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
