<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libalbum.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

global $file_path;
$album_id = $_POST['album_id'];
$order = $_POST['order'];
$pageNo = $_POST['pageNo'];
$field = $_POST['field'];

$li = new libdb();
$lf = new libfilesystem(); 

$l_ca = new libalbum($album_id);
$l_ca->db = $li->db;

$album_path = $l_ca->getAlbumPathFile($album_id);

$flag1 = 0;
$flag2 = 0;
$flag3 = 0;

for ($i=0; $i<sizeof($userfile); $i++)
{
	$filename = $userfile_hidden[$i];
	
	if(trim($filename) == "")
	{
		$filename = $userfile[$i];
	}
	$loc = $userfile[$i];	
	$des = $file_path.$album_path.stripslashes($filename);	
	$data = $l_ca->returnAlbumStorageQuota();
	$file_size = ceil(($userfile_size[$i])/1024);	

	if ($loc<>"none" && $l_ca->checkImageFormat($filename) && file_exists($loc))
	{
		$is_exist_file = (file_exists($des) && trim($filename)!="");
        $lf->lfs_copy($loc, $des);        
              
		// insert into DB
		if (!$is_exist_file)
		{	
			$l_ca->addPhoto2Album($album_id, $album_photo, $filename, $photo_description[$i], $album_path, $file_size);				
			if ($l_ca->OwnerGroup != "")
			{
				$l_ca->updateStorageQuota($file_size);			
			}			
			$flag1 = 1;
		}
		else
			$flag2 = 1;
	}
	else
	{
		$flag3 = 1;
	}
}

if($flag1 == 1)
	$msg = 1;
else if($flag2 == 1 && $flag1 == 0)
	$msg = 103;
else if($flag3 == 1 && $flag1 == 0)
	$msg = 104;

$l_ca->updatePhotoOrder();
$l_ca->updateAlbumDate();
$l_ca->updateThumbnail();
$count = $l_ca->getNumberOfItems();

intranet_closedb();
header("Location: list_photo.php?album_id=$album_id&count=$count");
?>
