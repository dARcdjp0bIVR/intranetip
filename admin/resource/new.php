<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libresource.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../includes/libbatch.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libresource();
$lo = new libgrouping();
$lb = new libbatch();
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.ResourceCode, "<?php echo $i_alert_pleasefillin.$i_ResourceCode; ?>")) return false;
     if(!check_text(obj.ResourceCategory, "<?php echo $i_alert_pleasefillin.$i_ResourceCategory; ?>")) return false;
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_ResourceTitle; ?>.")) return false;
     if(!check_positive_int(obj.daysbefore, "<?=$i_ResourceDaysBeforeWarning?>")) return false;
     checkOptionAll(obj.elements["GroupID[]"]);
}
function fileAttach(obj){
     url = "attach.php?folderID=1&fieldname=" + obj.name + "&attachment=" + obj.value;
     newWindow(url,1);
}
</script>


<form name="form1" action="new_update.php" method=post onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_rm, '', $i_admintitle_rm_item, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_resource_list_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td <?=($intranet_session_language=="en"?"":"nowrap")?> align=right><?php echo $i_ResourceCode; ?>:</td><td><input class=text type=text name=ResourceCode size=15 maxlength=50></td></tr>
<tr><td align=right><?php echo $i_ResourceCategory; ?>:</td><td><input class=text type=text name=ResourceCategory size=15 maxlength=50> <?php echo $li->displayResourceCategory("ResourceCategory"); ?></td></tr>
<tr><td align=right><?php echo $i_ResourceTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_ResourceDaysBefore; ?>:</td><td><input class=text type=text name=daysbefore value=0 size=5 maxlength=10></td></tr>
<tr><td align=right><?php echo $i_ResourceTimeSlot; ?>:</td><td><?=$lb->returnBatchSelect("name=batchID")?> <span class=extraInfo>[</span><?=$i_ResourceViewScheme?><span class=extraInfo>]</span></td></tr>
<tr><td align=right><?php echo $i_ResourceDescription; ?>:</td><td><textarea name=Description cols=30 rows=5></textarea></td></tr>
<tr><td align=right><?php echo $i_ResourceRemark; ?>:</td><td><textarea name=Remark cols=30 rows=5></textarea></td></tr>
<tr><td align=right><?php echo $i_ResourceAttachment; ?>:</td><td><input class=text type=text name=Attachment size=30 maxlength=255>
<a href="javascript:fileAttach(document.form1.Attachment)"><img src="/images/admin/button/s_btn_attach_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a></td></tr>
<tr><td align=right><?php echo $i_ResourceAutoApprove; ?>:</td><td><input type=checkbox name=DefaultStatus value=yes CHECKED></td></tr>
<tr><td align=right><?php echo $i_ResourceRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 CHECKED> <?php echo $i_status_publish; ?> <input type=radio name=RecordStatus value=0> <?php echo $i_status_pending; ?></td></tr>
<tr><td align="right"><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayResourceGroups(); ?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>