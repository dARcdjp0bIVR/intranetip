<?php

$PATH_WRT_ROOT = "../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

#$default_booking_status = 4;          // RecordType in Item

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none"){          # import failed
        header("Location: import.php");
} else {
        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                //$data = $lo->file_read_csv($filepath);
                $data = $limport->GET_IMPORT_TXT($filepath);
                array_shift($data);                   # drop the title bar
        }
		
        # Retrieve BatchID
        $sql = "SELECT BatchID, Title FROM INTRANET_SLOT_BATCH";
        $batcharray = $li->returnArray($sql,2);
        $insert_values = "";
        $delimiter = "";

        for ($i=0; $i<sizeof($data); $i++)
        {
                list($code,$cat,$title,$daysbefore,$batchTitle,$desp,$remark,$autoapproval) = $data[$i];
                /*
                $code = intranet_htmlspecialchars($code);
                $cat = intranet_htmlspecialchars($cat);
                $title = intranet_htmlspecialchars($title);
                $desp = intranet_htmlspecialchars($desp);
                $remark = intranet_htmlspecialchars($remark);
                */
                $code = addslashes($code);
                $cat = addslashes($cat);
                $title = addslashes($title);
                $desp = addslashes($desp);
                $remark = addslashes($remark);
                $autoapproval = strtolower($autoapproval);
                $id = getBatchID($batchTitle);
                $RecordType = (($autoapproval == "yes")? 4:0 );
                if (!$id)               // Batch not exists, skip
                    continue;
                else
                {
                    # pack insert values
                    $insert_values .= $delimiter. "('$code','$cat','$title','$daysbefore','$id','$desp','$remark',$RecordType,1,now(),now()) ";
                    $delimiter = ",";
                }
        }
        $fields_name = "ResourceCode, ResourceCategory,Title,DaysBefore,TimeSlotBatchID,Description,Remark,RecordType,RecordStatus,DateInput,DateModified";
        $sql = "INSERT IGNORE INTO INTRANET_RESOURCE ( $fields_name ) VALUES $insert_values";
        $li->db_db_query($sql);
        intranet_closedb();
        header("Location: index.php?msg=1");
}

function getBatchID ($target_batch)
{
         global $batcharray;
         for ($i=0; $i<sizeof($batcharray); $i++)
         {
              if (trim($target_batch) == trim($batcharray[$i][1]))
              {
                  return $batcharray[$i][0];
              }
         }
         return 0;
}
?>