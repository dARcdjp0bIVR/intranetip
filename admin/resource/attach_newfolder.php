<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/fileheader_admin.php"); 
include("menu.php"); 

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$li = new libfilesystem();
$path = str_replace("..","",$path);
$x = (!file_exists("$root/$path/$folderName") && $li->folder_new("$root/$path/$folderName")) ? "$button_newfolder: $folderName ...... SUCCESS" : "$button_newfolder: $folderName ...... FAIL";
?>

<form name="form1" action="attach.php" method="get">
<?= displayNavTitle($title, 'javascript:history.back()', $button_newfolder, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td>
<blockquote>
<p><br><br><?php echo $x; ?><br><br>
</blockquote>
</td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border="0"></td></tr>
</table>

<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
<input type=hidden name=attachment value="<?php echo $attachment; ?>">
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php 
include("../../templates/filefooter.php"); 
?>
