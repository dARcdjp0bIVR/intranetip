<?php
$PATH_WRT_ROOT = "../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include($PATH_WRT_ROOT."includes/libaccount.php");
include($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include($PATH_WRT_ROOT."templates/adminheader_intranet.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$limport = new libimporttext();
$linterface = new interface_html();
?>


<form action="import_update.php" method="post" enctype="multipart/form-data" name="form1">
<?= displayNavTitle($i_admintitle_rm, '', $i_admintitle_rm_item, 'javascript:history.back()', $button_import, '') ?>
<?= displayTag("head_resource_list_$intranet_session_language.gif", $msg) ?>

<table width='560' border='0' cellspacing='0' cellpadding='0' align='center'>
<tr><td>

<blockquote>
<br>
<?= $i_ResourceImportFile ?>: &nbsp; 
<input class=file type=file name="userfile" size=25><br>
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>

<br>
<table width=400 border=0 cellpadding=2 cellspacing=1>
<tr><td nowrap><?=$i_ResourceImportNote?>:</td>
<td><?=$Lang['ResourceBooking']['ImportNote1']?></td></tr>
</table>

</blockquote>
</td></tr>

<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_upload_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>
</form>

<?php
include($PATH_WRT_ROOT."templates/adminfooter.php");
?>