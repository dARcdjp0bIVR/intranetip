<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$ResourceCode = intranet_htmlspecialchars(trim($ResourceCode));
$ResourceCategory = intranet_htmlspecialchars(trim($ResourceCategory));
$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$Remark = intranet_htmlspecialchars(trim($Remark));
$Attachment = intranet_htmlspecialchars(trim($Attachment));
$daysbefore = intranet_htmlspecialchars(trim($daysbefore));
$batchID = intranet_htmlspecialchars(trim($batchID));
$DefaultStatus = intranet_htmlspecialchars(trim($DefaultStatus));
if ($DefaultStatus == 'yes')
    $RecordType = 4;
else $RecordType = 0;

$sql = "INSERT INTO INTRANET_RESOURCE (ResourceCode, ResourceCategory, Title, Description, Remark, Attachment,RecordType, RecordStatus, DateInput, DateModified, DaysBefore, TimeSlotBatchID) VALUES ('$ResourceCode', '$ResourceCategory', '$Title', '$Description', '$Remark', '$Attachment','$RecordType', '$RecordStatus', now(), now(),'$daysbefore','$batchID')";
$li->db_db_query($sql);
$ResourceID = $li->db_insert_id();

for($i=0; $i<sizeof($GroupID); $i++){
     $sql = "INSERT INTO INTRANET_GROUPRESOURCE (GroupID, ResourceID) VALUES (".$GroupID[$i].", $ResourceID)";
     $li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?filter=$RecordStatus&msg=1");

?>