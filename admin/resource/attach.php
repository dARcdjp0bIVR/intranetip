<?php
include("../../includes/global.php");
include("../../includes/libfiletable.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/fileheader_admin.php");
include("menu.php");

$keyword = trim($keyword);
$order = ($order==1) ? 1 : 0;
$field = ($field==1 || $field==2) ? $field : $field = 0;
$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$path = str_replace("..","",$path);
$li = new libfiletable($root, $path, $order, $field, $keyword);
$li->checkField2 = returnCheckField($folderID);
$li->radioField = returnRadioField($folderID);
$li->attachment = $attachment;
$toolbar  = "<a class='iconLink' href=\"javascript:fs_newfolder(document.form1,'attach_newfolder.php')\">".newFolderIcon()."$button_newfolder</a>\n";
$functionbar .= "<a href=\"javascript:fs_attachfile(document.form1,'filename[]',opener.window.document.form1.$fieldname)\"><img src='/images/admin/button/t_btn_attach_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$titlebar  = "<a href=\"javascript:fs_fileupload(document.form1,'attach_upload.php')\"><img src='/images/admin/button/t_btn_upload_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$titlebar .= "<input class=text type=text size=1 maxlength=1 name=no_file value=1> $i_FileSystemFiles \n";
$searchbar  = "&nbsp;<input class=text type=text name=keyword size=5 maxlength=20 value=\"".stripslashes($keyword)."\">\n";
$searchbar .= "<a href=\"javascript:document.form1.submit()\"><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form action="attach.php" name="form1" method="get">
<?= displayNavTitle($title, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?= $li->displayFunctionbar("", $titlebar.$searchbar) ?></td></tr>
<tr><td class=admin_bg_menu><?= $li->displayFunctionbar($toolbar, $functionbar) ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?php echo $li->display(); ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=folderName value="">
<input type=hidden name=pageNo value="0">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
<input type=hidden name=attachment value="<?php echo $attachment; ?>">
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php
include("../../templates/filefooter.php");
?>
