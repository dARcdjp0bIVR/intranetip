<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader_admin.php");
intranet_opendb();

# SQL TABLE
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($filter5 != "")
    $target_status = $filter5-1;

$status_str = "";
$default_status = "UNKNOWN";
for ($i=0; $i<sizeof($i_BookingStatusArray); $i++)
{
     $status_str .= " WHEN $i THEN '".$i_BookingStatusArray[$i]."'";
}

$sql  = "SELECT
               BookingDate, Item, ItemCode, Category, TimeSlot, Username,
               CASE FinalStatus $status_str ELSE '$default_status' END,
               TimeApplied, LastAction,
               CONCAT('<input type=checkbox name=BookingArchiveID[] value=', BookingArchiveID ,'>')
          FROM
               INTRANET_BOOKING_ARCHIVE
               ";

if ($filter1 != '')
    $conds = " WHERE Item = '$filter1'";
else if ($filter2!='')
    $conds = " WHERE ItemCode = '$filter2'";
else if ($filter3!='')
    $conds = " WHERE Category = '$filter3'";
else if ($filter4!='')
    $conds = " WHERE Username = '$filter4'";
else if ($filter5!='')
    $conds = " WHERE FinalStatus = '$target_status'";
else $conds = "";

$sql .= $conds;

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("BookingDate", "Item", "ItemCode","Category","TimeSlot", "Username","FinalStatus","TimeApplied","LastAction");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_booking;
$li->column_array = array(0,0,0,0,0,0,0,0,0);
$li->IsColOff = 5;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(0, $i_BookingDate)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(1, $i_ResourceSelectItem)."</td>\n";
$li->column_list .= "<td width=5% class=tableTitle>".$li->column(2, $i_ResourceCode)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_ResourceCategory)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_BookingTimeSlots)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(5, $i_Booking_ArchiveUser)."</td>\n";
$li->column_list .= "<td width=5% class=tableTitle>".$li->column(6, $i_BookingStatus)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(7, $i_BookingApplied)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(8, $i_Booking_ArchiveLastAction)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("BookingArchiveID[]")."</td>\n";

// TABLE FUNCTION BAR
$btn_remove = "<a href=\"javascript:checkRemove(document.form1,'BookingArchiveID[]','archive_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$btn_remove_all = "<a href=\"javascript:checkRemoveAll(document.form1,'archive_removeall.php')\"><img src='/images/admin/button/t_btn_delete_all_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
/*
$functionbar .= ($filter==3 || $filter==2 || $filter==1) ? "" : "<input class=button type=button value='$button_cancel' onClick=checkRemove(this.form,'BookingID[]','cancel.php')>";
$functionbar .= ($filter==3 || $filter==2 || $filter==1) ? "" : "<input class=button type=button value='$button_checkin' onClick=checkRemove(this.form,'BookingID[]','checkin.php')>";
$functionbar .= ($filter==3 || $filter==1 || $filter==0) ? "" : "<input class=button type=button value='$button_checkout' onClick=checkRemove(this.form,'BookingID[]','checkout.php')>";
$functionbar .= "<input class=button type=button value='$button_remove' onClick=checkRemove(this.form,'BookingID[]','remove.php')>\n";
*/
$functionbar .= "$btn_remove $btn_remove_all \n";
$method = "this.form.pageNo.value=1;this.form.submit();";

$sqlf = "SELECT DISTINCT Item FROM INTRANET_BOOKING_ARCHIVE ORDER BY Item";
$result = $li->returnVector($sqlf);
$ff1 = "<select name=filter1 onChange=\"setFilter(this); $method\">\n";
$ff1 .= "<OPTION VALUE='' ".($filter1==""? "SELECTED":"")."> -- $i_ResourceSelectAllItem -- </OPTION>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $value = $result[$i];
     $ff1 .= "<OPTION VALUE='$value' ".($filter1==$value? "SELECTED":"").">$value</OPTION>\n";
}
$ff1 .= "</SELECT>\n";

$sqlf = "SELECT DISTINCT ItemCode FROM INTRANET_BOOKING_ARCHIVE ORDER BY ItemCode";
$result = $li->returnVector($sqlf);
$ff2 = "<select name=filter2 onChange=\"setFilter(this); $method\">\n";
$ff2 .= "<OPTION VALUE='' ".($filter2==""? "SELECTED":"")."> -- $i_ResourceSelectAllResourceCode -- </OPTION>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $value = $result[$i];
     $ff2 .= "<OPTION VALUE='$value' ".($filter2==$value? "SELECTED":"").">$value</OPTION>\n";
}
$ff2 .= "</SELECT>\n";

$sqlf = "SELECT DISTINCT Category FROM INTRANET_BOOKING_ARCHIVE ORDER BY Category";
$result = $li->returnVector($sqlf);
$ff3 = "<select name=filter3 onChange=\"setFilter(this); $method\">\n";
$ff3 .= "<OPTION VALUE='' ".($filter3==""? "SELECTED":"")."> -- $i_ResourceSelectAllCat -- </OPTION>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $value = $result[$i];
     $ff3 .= "<OPTION VALUE='$value' ".($filter3==$value? "SELECTED":"").">$value</OPTION>\n";
}
$ff3 .= "</SELECT>\n";

$sqlf = "SELECT DISTINCT Username FROM INTRANET_BOOKING_ARCHIVE ORDER BY Username";
$result = $li->returnVector($sqlf);
$ff4 = "<select name=filter4 onChange=\"setFilter(this); $method\">\n";
$ff4 .= "<OPTION VALUE='' ".($filter4==""? "SELECTED":"")."> -- $i_status_all $i_Booking_ArchiveUser -- </OPTION>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $value = $result[$i];
     $ff4 .= "<OPTION VALUE='$value' ".($filter4==$value? "SELECTED":"").">$value</OPTION>\n";
}
$ff4 .= "</SELECT>\n";

/*
$sqlf = "SELECT DISTINCT Item FROM INTRANET_BOOKING_ARCHIVE ORDER BY Item";
$result = $li->returnVector($sqlf);
$ff1 = "<select name=filter1 onChange=\"setFilter(this); $method\">\n";
$ff1 .= "<OPTION VALUE='' ".($filter1==""? "SELECTED":"")."> -- $i_status_all $i_ResourceSelectItem -- </OPTION>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $value = $result[$i];
     $ff1 .= "<OPTION VALUE='$value' ".($filter1==$value? "SELECTED":"").">$value</OPTION>\n";
}
$ff1 .= "</SELECT>\n";
*/
$searchbar = "$ff1 $ff2 $ff3 $ff4";


$searchbar .= "<select name=filter5 onChange=\"setFilter(this); $method\">\n";
$searchbar .= "<option value='' ".(($filter5=="")?"selected":"")."> -- $i_status_all $i_BookingStatus -- </option>\n";
for ($i=0; $i<sizeof($i_BookingStatusArray); $i++)
{
     $count = $i+1;
     $searchbar .= "<option value=$count ".(($filter5==$count)?"selected":"").">".$i_BookingStatusArray[$i]."</option>\n";
}
$searchbar .= "</select>\n";
$exportLink .= "<a class=iconLink href=javascript:checkGet(document.form1,'archive_export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();
$toolbar = $exportLink;
/*
$lr = new libresource();
$filter_cat = $lr->getSelectCats("name=filter2 onChange=\"this.form.pageNo.value=1;this.form.filter3.value='';this.form.submit();\"",$filter2);
$filter_item = $lr->getSelectItems("name=filter3 onChange=\"this.form.pageNo.value=1;this.form.filter2.value='';this.form.submit();\"",$filter3);
*/
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function setFilter (obj)
{
         var target = obj.value;
         obj.form.filter1.value = '';
         obj.form.filter2.value = '';
         obj.form.filter3.value = '';
         obj.form.filter4.value = '';
         obj.form.filter5.value = '';
         obj.value = target;
}
</SCRIPT>

<form name=form1 method="get">
<?= displayNavTitle($i_admintitle_rm, '', $i_admintitle_rm_record, '', $i_Booking_ArchiveBooking, '') ?>
<table width=100% border=0 cellpadding=0 cellspacing=0 style='border-top: solid #DBD6C4; border-right: solid #FFFFFF;  border-left: solid #FFFFFF'>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $functionbar);?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-","-","",$searchbar); ?></td></tr>
<!--
<tr><td><img src=../../images/admin/menu_bottom.gif width=560 height=8 border=0></td></tr>
-->
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/filefooter.php");
?>