<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbdump.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();
$lexport = new libexporttext();

# Export default format

if ($filter5 != "")
    $target_status = $filter5-1;

$status_str = "";
$default_status = "UNKNOWN";
for ($i=0; $i<sizeof($i_BookingStatusArray); $i++)
{
     $status_str .= " WHEN $i THEN '".$i_BookingStatusArray[$i]."'";
}

$sql  = "SELECT
               BookingDate, Item, ItemCode, Category, TimeSlot, Username,
               CASE FinalStatus $status_str ELSE '$default_status' END,
               TimeApplied, LastAction
          FROM
               INTRANET_BOOKING_ARCHIVE
               ";

if ($filter1 != '')
    $conds = " WHERE Item = '$filter1'";
else if ($filter2!='')
    $conds = " WHERE ItemCode = '$filter2'";
else if ($filter3!='')
    $conds = " WHERE Category = '$filter3'";
else if ($filter4!='')
    $conds = " WHERE Username = '$filter4'";
else if ($filter5!='')
    $conds = " WHERE FinalStatus = '$target_status'";
else $conds = "";

$sql .= $conds;

     $li = new libdb();
     $row = $li->returnArray($sql,9);
     $x = "$i_BookingDate,$i_ResourceSelectItem,$i_ResourceCode,$i_ResourceCategory,$i_BookingTimeSlots,$i_Booking_ArchiveUser,$i_BookingStatus,$i_BookingApplied,$i_Booking_ArchiveLastAction\n";
     $exportColumn = array($i_BookingDate,$i_ResourceSelectItem,$i_ResourceCode,$i_ResourceCategory,$i_BookingTimeSlots,$i_Booking_ArchiveUser,$i_BookingStatus,$i_BookingApplied,$i_Booking_ArchiveLastAction);
     /*
     for($i=0; $i<sizeof($row); $i++)
     {
         for($j=0; $j<sizeof($row[$i]); $j++)
         {
             $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($row[$i][$j])."\"";
         }
         $x .= implode(",", $row[$i])."\n";
     }
     */
     $url = "/file/export/eclass-booking-".session_id()."-".time().".csv";
     $filename = "eclass-booking-".session_id()."-".time().".csv";
     /*
     $lo = new libfilesystem();
     $lo->file_write($x, $intranet_root.$url);
     */
     $export_content = $lexport->GET_EXPORT_TXT($row, $exportColumn);
	 $lexport->EXPORT_FILE($filename, $export_content);



intranet_closedb();
//header("Location: $url");
?>