<?php
include_once("../../includes/global.php");
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$order = ($order == 1) ? 1 : 0;

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);

$sql = "DROP TEMPORARY TABLE IF EXISTS TEMP_PAGAMO_USER_QUOTATION";
$li->db_db_query($sql);
$sql = "CREATE TEMPORARY TABLE TEMP_PAGAMO_USER_QUOTATION(
		 	 RecordID int(11) NOT NULL auto_increment,
		     QuotationID int(11) default NULL,
		     StdUsed int(11) default NULL,
			 TchUsed int(11) default NULL,
		     PRIMARY KEY (RecordID)
    );";
$li->db_db_query($sql);

$sql = "SELECT q.QuotationID, UserType, COUNT(u.RecordID) as Used FROM PAGAMO_USER u
		RIGHT JOIN PAGAMO_QUOTATION q ON u.QuotationID=q.QuotationID
		GROUP BY q.QuotationID, UserType";
$catCountInfoArr = $li->returnArray($sql);
$quotationUsage = array();
foreach($catCountInfoArr as $catCount){	
	$quotationUsage[$catCount['QuotationID']][$catCount['UserType']] = $catCount['Used'];
}

foreach($quotationUsage as $qid=>$usage){
	$sql = "INSERT INTO TEMP_PAGAMO_USER_QUOTATION (QuotationID, StdUsed, TchUsed) VALUES ('".$qid."','".$usage['S']."','".$usage['T']."')";
	$li->db_db_query($sql);
}

$classLevelDisplay = "y.YearName";
if($sys_custom['non_eclass_PaGamO']==true){
	$classLevelDisplay = "CONCAT(q.SchoolName,'-',q.ClassLevel)";
}
$classLevelDisplay = "IF(y.YearName IS NULL, CONCAT('<a href=\"class_level_select.php?qid=',q.QuotationID,'\">','".$Lang['Btn']['Select']."','</a>'), y.YearName)";
$sql = "SELECT 
			q.QuotationName, 
			DATE_FORMAT(q.Startdate, '%Y-%m-%d') as Start, 
			DATE_FORMAT(q.Enddate, '%Y-%m-%d') as Expiry, 
			$classLevelDisplay as Year, 
			IF(y.YearName IS NULL, '', CONCAT('<a href=\"std_import.php?qid=',q.QuotationID,'\">',uq.StdUsed,'/',q.StdQuota,'</a>')) as StdNo, 
			IF(y.YearName IS NULL, '', CONCAT('<a href=\"tch_import.php?qid=',q.QuotationID,'\">',uq.TchUsed,'/',q.TchQuota,'</a>')) as TchNo
		FROM PAGAMO_QUOTATION q 
		LEFT JOIN TEMP_PAGAMO_USER_QUOTATION uq ON q.QuotationID=uq.QuotationID
		LEFT JOIN YEAR y ON q.YearID=y.YearID
		WHERE QuotationName LIKE '%".$keyword."%'";

$li->field_array = array("q.QuotationName", "Start","Expiry", "Year", "StdNo", "TchNo");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_group;
$li->wrap_array = array(20,20,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $Lang['StudentRegistry']['Quotation#'])."</td>\n";
$li->column_list .= "<td width=17% class=tableTitle>".$li->column(1, $Lang['General']['StartDate'])."</td>\n";
$li->column_list .= "<td width=17% class=tableTitle>".$li->column(2, $Lang['itextbook']['ExpiryDate'])."</td>\n";
$li->column_list .= "<td width=14% class=tableTitle>".$li->column(3, $i_UserClassLevel)."</td>\n";
$li->column_list .= "<td width=13% class=tableTitle>".$li->column(4, $Lang['PaGamO']['std_quota'])."</td>\n";
$li->column_list .= "<td width=13% class=tableTitle>".$li->column(5, $Lang['PaGamO']['tch_quota'])."</td>\n";


//if(!$ksk->hasUsedUpAllLicense() && $ksk->isInLicencePeriod()) 
{
//	$toolbar = "<a class=iconLink href=\"javascript:newWindow('user_add.php',2)\">".newIcon()."$button_new</a>";
	//$toolbar .= "\n".toolBarSpacer()."<a class=iconLink href=\"javascript:checkPost(document.form1,'import.php')\">".importIcon()."$button_import</a>\n";
}
$toolbar = "";
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<script language="JavaScript1.2">
function icon(id){
        url = "icon/index.php?GroupID=" + id;
        newWindow(url,2);
}
function info(id){
        url = "info/info.php?GroupID=" + id;
        newWindow(url,2);
}
function user(id){
        f = document.form1.filter.value;
        url = "info/index.php?filter=" + f + "&GroupID=" + id;
        self.location.href = url;
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $Lang['PaGamO']['admin_module'],'index.php') ?>



<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>