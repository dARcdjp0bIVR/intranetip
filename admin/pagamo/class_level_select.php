<?php
include_once("../../includes/global.php");
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/pagamo/libpagamo_install.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$libpagamoinstall = new pagamo_install();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$order = ($order == 1) ? 1 : 0;

$sql = "SELECT QuotationName FROM PAGAMO_QUOTATION WHERE QuotationID='".$qid."'";
$title = current($libpagamoinstall->returnVector($sql));
$sql = "SELECT
			YearName,
			YearID
		FROM YEAR";
$yearData = $libpagamoinstall->returnArray($sql);
?>

<script language="JavaScript1.2">
function icon(id){
        url = "icon/index.php?GroupID=" + id;
        newWindow(url,2);
}
function info(id){
        url = "info/info.php?GroupID=" + id;
        newWindow(url,2);
}
function user(id){
        f = document.form1.filter.value;
        url = "info/index.php?filter=" + f + "&GroupID=" + id;
        self.location.href = url;
}
function setUpClassLevel(){
	var YearID = document.getElementById('YearChoose').value;
	var YearName = document.getElementById('YearName').value;
	if(confirm("<?=$Lang['PaGamO']['confirm_class_level']?>" + YearName + "?")){
		var qid = document.form1.qid.value;
		url = "class_level_update.php?qid=" + qid + "&YearID=" + YearID;
	    self.location.href = url;
	}
}
function fillText() {
    var elt = document.getElementById('YearChoose');

    if (elt.selectedIndex == -1)
        return null;

    document.getElementById('YearName').value = elt.options[elt.selectedIndex].text;
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $Lang['PaGamO']['admin_module'],'index.php', $title." - ".$i_UserClassLevel, 'std_import.php?qid='.$qid) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<div style="text-align:center">
<select id='YearChoose' name='YearChoose' onchange='fillText()'>
	<option value=''> -- <?=$button_select?> -- </option>
<?php 
	foreach($yearData as $yd){
		echo "<option value='".$yd['YearID']."'>".$yd['YearName']."</option>";	
	}
?>
</select>
<input type='hidden' name='YearName' id='YearName' value=''>
<a href="javascript:void(0)" onclick="setUpClassLevel()"><img src="/images/admin/button/s_btn_confirm_b5.gif" border="0" align="absmiddle"></a>
</div>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=qid id="qid" value="<?php echo $qid;?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>