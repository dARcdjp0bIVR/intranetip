<?php
include_once("../../includes/global.php");
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/pagamo/libpagamo_install.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$libpagamoinstall = new pagamo_install();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$order = ($order == 1) ? 1 : 0;

# get total used license
$license = $libpagamoinstall->getLicenseArray($qid, 'T');
$license_html = $Lang['PaGamO']['license_display_1_t'].$license[0].$Lang['PaGamO']['license_display_2'].$license[1].$Lang['PaGamO']['license_display_3'].$Lang['PaGamO']['license_display_4'].$license[2].$Lang['PaGamO']['license_display_5'];

$sql = "SELECT
			u.EnglishName,
			pu.LastLogin,
			IF(pu.IsActivated=1, 'Activated', CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"',pu.RecordID,'\">')) as checkbox
		FROM PAGAMO_USER pu INNER JOIN
		INTRANET_USER u ON pu.UserID=u.UserID
		WHERE pu.QuotationID = '".$qid."' AND pu.UserType='T' AND u.EnglishName LIKE '%".$keyword."%'";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("u.EnglishName","pu.LastLogin", "checkbox");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_group;
$li->wrap_array = array(20,20,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $Lang['SysMgr']['Timetable']['TeacherName'] )."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $Lang['login']['last_used'])."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

$sql = "SELECT QuotationName FROM PAGAMO_QUOTATION WHERE QuotationID='".$qid."'";
$title = current($li->returnVector($sql));

$toolbar = "<a class=iconLink href=\"javascript:newWindow('user_add.php?qid=".$qid."&userType=T',2)\">".newIcon()."$button_new</a>";
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','user_delete_update.php?qid=".$qid."&userType=T')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<script language="JavaScript1.2">
function icon(id){
        url = "icon/index.php?GroupID=" + id;
        newWindow(url,2);
}
function info(id){
        url = "info/info.php?GroupID=" + id;
        newWindow(url,2);
}
function user(id){
        f = document.form1.filter.value;
        url = "info/index.php?filter=" + f + "&GroupID=" + id;
        self.location.href = url;
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $Lang['PaGamO']['admin_module'],'index.php', $title." - ".$Lang['PaGamO']['tch_quota'], 'tch_import.php?qid='.$qid) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?=$license_html?></td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=qid value="<?php echo $qid;?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>