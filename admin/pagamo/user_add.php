<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/fileheader_admin.php");
include_once($PATH_WRT_ROOT."includes/pagamo/libpagamo_install.php");
intranet_opendb();

$ls = new pagamo_install();
$fcm = new form_class_manage();

$fcmArr = $fcm->Get_Current_Academic_Year_And_Year_Term();
list($currentYearID,$currentYearNameEn,$currentYearNameB5,$Sequence, $currentTermID, $currentTermNameEn, $currentTermB5, $TermStart, $TermEnd) = $fcmArr[0];

if($ls->hasUsedUpAllLicense($qid, $userType) || !$ls->isInLicencePeriod($qid)){
	echo "<script>self.close()</script>";
	die;
}

$sql = "SELECT YearID FROM PAGAMO_QUOTATION WHERE QuotationID='".$qid."'";
$classLevel = current($ls->returnVector($sql));

$sql = "SELECT UserID FROM PAGAMO_USER WHERE QuotationID='".$qid."'";
$userList = $ls->returnVector($sql);

$lo = new libgroup($GroupID);

$liuser = new libuser($UserID);

# retrieve group category

$li = new libgrouping();

unset($ChooseGroupID);
$ChooseGroupID[0] = 0-$CatID;
$x1  = "<select name=CatID onChange=this.form.submit()>\n";
if($userType=='S'){
	$cats = array();
	if($sys_custom['PaGamO']['ReleaseClassLimit']){
		$classes = array();
		$sql = "SELECT DISTINCT YearID FROM YEAR_CLASS WHERE AcademicYearID='".$currentYearID."'";
		$classLevelList = $ls->returnVector($sql);
		foreach($classLevelList as $classLevel){
			$classList = $fcm->Get_Class_List($currentYearID,$classLevel,$CatID);
			foreach($classList as $c){
				array_push($classes,$c);
			}
		}
	}else{
		$classes = $fcm->Get_Class_List($currentYearID,$classLevel,$CatID);
	}
	foreach($classes as $cc){
		$cats[] = array($cc['YearClassID'],$cc['ClassTitleEN']);
	}
	$x1 .= "<option value=0></option>\n";
	for ($i=0; $i<sizeof($cats); $i++)
	{
	     list($id,$name) = $cats[$i];
	     if ($id!=0)
	     {
	         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
	     }
	}
	$x1 .= "<option value=0>";
	for($i = 0; $i < 20; $i++)
		$x1 .= "_";
	$x1 .= "</option>\n";

}
$x1 .= "<option value=0></option>\n";
if($userType=='T'){
	$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
}else if($userType='S'){
	$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
}

$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";

//selection of all classes
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if($CatID < 0){
   	# Return users with identity chosen
   	$selectedUserType = 0-$CatID;
   	$cond = "";
   	
   	if(!empty($userList)){
   		$cond .= " AND u.UserID NOT IN ('".implode("','",$userList)."')";
   	}

   	if($selectedUserType==2){
   		if($sys_custom['PaGamO']['ReleaseClassLimit']){
   			$sql = "SELECT u.UserID, u.EnglishName FROM YEAR_CLASS_USER ycu
					INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
					INNER JOIN YEAR y ON y.YearID=yc.YearID AND yc.AcademicYearID='".$currentYearID."'
					INNER JOIN INTRANET_USER u ON ycu.UserID=u.UserID
					WHERE u.RecordType='".$selectedUserType."' $cond";
   		}else{	
		   	$sql = "SELECT u.UserID, u.EnglishName FROM YEAR_CLASS_USER ycu
					INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
					INNER JOIN YEAR y ON y.YearID=yc.YearID AND yc.AcademicYearID='".$currentYearID."'
					INNER JOIN INTRANET_USER u ON ycu.UserID=u.UserID
					WHERE y.YearID='".$classLevel."' AND u.RecordType='".$selectedUserType."' $cond";
   		}		
   	}elseif($selectedUserType==1){
   		$sql = "SELECT u.UserID, u.EnglishName FROM INTRANET_USER u
				WHERE u.RecordType='".$selectedUserType."' $cond";
   	}
	$row = $li->returnArray($sql);
	$x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
	for($i=0; $i<sizeof($row); $i++)
	   		$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	$x3 .= "<option>";
	for($i = 0; $i < 40; $i++)
			$x3 .= "&nbsp;";
	$x3 .= "</option>\n";
	$x3 .= "</select>\n";   
}
else {
	# Return users with class chosen
	$cond = "";
	
	if(!empty($userList)){
		$cond .= " AND u.UserID NOT IN ('".implode("','",$userList)."')";
	}
	if($sys_custom['PaGamO']['ReleaseClassLimit']){
		$sql = "SELECT u.UserID, u.EnglishName FROM YEAR_CLASS_USER ycu
				INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
				INNER JOIN YEAR y ON y.YearID=yc.YearID AND yc.AcademicYearID='".$currentYearID."'
				INNER JOIN INTRANET_USER u ON ycu.UserID=u.UserID
				WHERE u.RecordType='2' AND yc.YearClassID='".$CatID."' $cond";
	}else{	
		$sql = "SELECT u.UserID, u.EnglishName FROM YEAR_CLASS_USER ycu
				INNER JOIN YEAR_CLASS yc ON ycu.YearClassID=yc.YearClassID
				INNER JOIN YEAR y ON y.YearID=yc.YearID AND yc.AcademicYearID='".$currentYearID."'
				INNER JOIN INTRANET_USER u ON ycu.UserID=u.UserID
				WHERE y.YearID='".$classLevel."' AND u.RecordType='2' AND yc.YearClassID='".$CatID."' $cond";
	}		
	$row = $li->returnArray($sql);
	$x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
	for($i=0; $i<sizeof($row); $i++)
		$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	$x3 .= "<option>";
	for($i = 0; $i < 40; $i++)
		$x3 .= "&nbsp;";
	$x3 .= "</option>\n";
	$x3 .= "</select>\n";
}
?>


<script language="JavaScript1.2">
function add_role(obj){
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          obj.role.value = role_name;
          obj.action = "add_role.php";
          obj.submit();
     }
}
function add_user(obj){
     obj.action = "user_add_update.php";
     obj.submit();
}

function import_update(){
        var obj = document.form1;
        checkOption(obj.elements["ChooseUserID[]"]);
        obj.action = "user_add_update.php";
        obj.submit();
}

function SelectAll()
{
        var obj = document.form1.elements['ChooseUserID[]'];
        for (i=0; i<obj.length; i++)
        {
          obj.options[i].selected = true;
        }
}

function expandGroup()
{
        var obj = document.form1;
        checkOption(obj.elements['ChooseGroupID[]']);
        obj.submit();
}

</script>

<form name="form1" action="user_add.php" method="post">
	<?= displayNavTitle($i_LSLP['user_list'], '',  $button_new, '') ?>
	
	<p style="padding-left:20px">
		<table width="422" border="0" cellpadding="0" cellspacing="0">
			<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
			<tr>
				<td style="background-image: url(../../../images/admin/pop_bg.gif);" >
					
					<table width="422" border="0" cellpadding="0" cellspacing="0">
						<tr><td><img src="../../../images/admin/pop_bar.gif" width="422" height="16" border="0"></td></tr>
					</table>
					<table width="422" border="0" cellpadding="10" cellspacing="0">
						<tr>
							<td>
								<p>
								<?php echo $i_frontpage_campusmail_select_category; ?>:
								<br>
								<?php echo $x1; ?>								
								
								<?php
								
								if(isset($ChooseGroupID)) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_user; ?>:
									<br>
									<?php echo $x3; ?>
									<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
									<a href="javascript:SelectAll()"><img src="/images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
							</td>
						</tr>
					</table>
	
				</td>
			</tr>
			<tr><td><img src="../../../images/admin/pop_bottom.gif" width="422" height="18" border="0"></td></tr>
			<tr>
				<td align=center height="40" style="vertical-align:bottom">
					<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
				</td>
			</tr>
		</table>
	<input type="hidden" name="qid" value="<?=$qid?>">
	<input type="hidden" name="userType" value="<?=$userType?>">
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/filefooter.php");
?>