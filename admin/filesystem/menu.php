<?php
die();
function returnFolderRoot($folderID){
        global $file_path;
        switch ($folderID){
                #case 0: $root = "$file_path/file/timetable"; break;
                case 1: $root = "$file_path/file/resource"; break;
                default: $root = "$file_path/file/resource"; break;
        }
        return $root;
}

function returnFolderTitle($folderID){
        global $i_admintitle_timetable, $i_admintitle_resource;
        switch ($folderID){
                #case 0: $title = $i_admintitle_timetable; break;
                case 1: $title = $i_admintitle_resource; break;
                default: $title = $i_admintitle_resource; break;
        }
        return $title;
}

function returnFolderDestinationOption($folderID){
        global $i_admintitle_timetable, $i_admintitle_resource;
        $x  = "";
        #if ($folderID<>0) $x .= "<option value=0>$i_admintitle_timetable</option>\n";
        if ($folderID<>1) $x .= "<option value=1>$i_admintitle_resource</option>\n";
        return $x;
}

function isInSelectedFolder($item,$arr){
        global $path;
        $flag = 0;
        for($i=0;$i<sizeof($arr);$i++){
                if (strstr($item,"$path/".$arr[$i])) $flag = 1;
        }
        return $flag;
}

function returnCheckField($folderID){
        return ($folderID==0) ?  0 : 1;
}

function returnRadioField($folderID){
        return ($folderID==0) ?  1 : 0;
}

function returnTabName($folderID){
        switch ($folderID){
                #case 0: $x = "ttable"; break;
                case 1: $x = "resource"; break;
                default: $x = "resource"; break;
        }
        return $x;
}
?>