<?php
die();
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("menu.php");

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$li = new libfilesystem();
$path = str_replace("..", "", $path);
$path = stripslashes($path);

for($i=0;$i<sizeof($filename);$i++){
        $file = stripslashes($filename[$i]);
        $x .= (Is_dir("$root/$path/$file")) ? "<img src=../../images/folder.gif border=0>" : "<img src=../../images/file.gif border=0>";
        $x .= "$file <input type=hidden name=filename[] value='".urlencode(addslashes($file))."'><br>\n";
}

$newpath = $li->return_folder($root); # destination directories into an array
for($i=0;$i<sizeof($newpath);$i++){
        $dest = str_replace($root, "", $newpath[$i]);
        $y .= (isInSelectedFolder($dest,$filename)) ? "" : "<option value='".urlencode(addslashes($dest))."'>$dest</option>\n";
}
?>

<form action="moveupdate.php" name="form1" method="post">
<?= displayNavTitle($i_admintitle_sf, '', $title, 'javascript:history.back()', $button_move, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td>
<blockquote>
<p><br><?php echo $x; ?>
<br><hr size=1 class="hr_sub_separator">
<p>
<!---
<input type="radio" name="moveUpdateFile" value="moveupdate2.php" checked>
--->
<input type=hidden name="moveUpdateFile" value="moveupdate2.php">
<select name=newpath><option value="">/</option><?php echo $y; ?></select>
<!--
<p><input type="radio" name="moveUpdateFile" value="moveupdate3.php">
<select name=targetFolderID onChange='this.form.moveUpdateFile[0].checked=0;this.form.moveUpdateFile[1].checked=1'><?php echo returnFolderDestinationOption($folderID); ?></select>
-->
<br><br>
</blockquote>
</td></tr>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>

<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
</form>

<?php
include("../../templates/adminfooter.php");
?>