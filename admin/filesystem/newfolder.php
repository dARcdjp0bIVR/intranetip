<?php
die();
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("menu.php");

$root = returnFolderRoot($folderID);
$title = returnFolderTitle($folderID);
$li = new libfilesystem();
$path = str_replace("..","",$path);
$folderName = stripslashes($folderName);
$x = ($li->folder_new("$root/$path/$folderName")) ? "$button_newfolder: $folderName ...... SUCCESS" : "$button_newfolder: $folderName ...... FAIL";
?>

<form name=form1 action="index.php" method="get">
<p class=admin_head><?php echo $i_admintitle_sf; ?> <?php echo displayArrow();?> <a href=javascript:history.back()><?php echo $title; ?></a> <?php echo displayArrow();?> <?php echo $button_newfolder; ?></p>
<blockquote>
<p><?php echo $x; ?>
<p align=right><input class=submit type=submit value="<?php echo $button_continue; ?>">
</blockquote>
<input type=hidden name=path value="<?php echo $path; ?>">
<input type=hidden name=folderID value="<?php echo $folderID; ?>">
</form>

<?php
include("../../templates/adminfooter.php");
?>
