<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
eclass_opendb();

$li = new libdb();
$sql = "SELECT MAX(course_id) FROM {$eclass_db}.course ";
$row = $li->returnVector($sql);
$course_index = (int) $row[$i];
if ($course_index==0) $course_index = 1;

$str_RoomType = ($RoomType==1?$i_eClass_reading_room:$i_eClass_normal_course);

// hidden values
$course_total = sizeof($course_add);
$form_hiddens = "<input type='hidden' name='course_total' value='".$course_total."'>\n";
$form_hiddens .= "<input type='hidden' name='course_current' value='0'>\n";
for ($i=0; $i<$course_total; $i++)
{
	$form_hiddens .= "<input type='hidden' name='course_add[]' value='".$course_add[$i]."'>\n";
	$std_impt = 0;
	for ($j=0; $j<sizeof($std_import); $j++)
	{
		if ($std_import[$j]==("std_".$course_add[$i]))
		{
			$std_impt = 1;
			break;
		}
	}
	$form_hiddens .= "<input type='hidden' name='student_import[]' value='".$std_impt."'>\n";
}


include_once("../../templates/adminheader_eclass.php");
?>

<style type="text/css">
.buttonText {
	COLOR: black; BACKGROUND-COLOR: white;
	FONT-SIZE: 14px;
	BORDER-RIGHT: blue 1px solid; BORDER-TOP: blue 1px solid;
	BORDER-LEFT: blue 1px solid; BORDER-BOTTOM: blue 1px solid;
}
</style>

<script language="javascript">
function submitProcess(){
	var obj = document.form1;
	updateProgress();
	if (parseInt(obj.course_current.value)>=parseInt(obj.course_total.value))
	{
		alert("<?=$i_eClassCourseDone?>");
		top.intranet_admin_main.location.replace("/admin/eclass/?msg=1");
	} else
	{
		obj.submit();
	}
	obj.course_current.value = parseInt(obj.course_current.value) + 1;
	return;
}

function updateProgress(){
	var obj = document.form1;
	obj.course_num.value = parseInt(Math.ceil(obj.course_current.value)/parseInt(obj.course_total.value)*100) + "%";

	return;
}
</script>


<form name="form1" target="fmProcess" action="new_batch_process.php" method="post">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, '', $button_quickadd." (".$str_RoomType.")", '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>

<br>
<p>
<table width=500 border=0 cellpadding=2 cellspacing=1 align="center">
<tr><td nowrap align="center"><font size=3><?=$i_eClassCourseProcessing?></font> <input class='buttonText' type='button' name='course_num' value=" 0%"></td></tr>
<tr><td nowrap align="center"><br><?=$i_eClassCourseWait?></td></tr>
</table>
</p>
<br>

<?= $form_hiddens ?>
<input type='hidden' name='max_user' value="<?=$max_user?>">
<input type='hidden' name='max_storage' value="<?=$max_storage?>">
<input type='hidden' name='course_code_prefix' value="<?=stripslashes($course_code_prefix)?>">
<p></p>
</form>


<script language="javascript">
submitProcess();
</script>

<?php
eclass_closedb();
include_once("../../templates/adminfooter.php");
?>