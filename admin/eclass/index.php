<?php
/**
 * Log :
 *  2017-03-14 (Paul)
 * 		change the logic to check number of iTextbook classroom as the previous logic is buggy
 *	2010-09-02 Kelvin:
 *		change eclass image to special room image
 */
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libeclass.php");
include_once("../../lang/lang.$intranet_session_language.php");
eclass_opendb();
# eClass
$lo = new libeclass();
#$eclass_quota = $lo->status();    # No place to display now

/*
if ($RoomType=="")
{
        $RoomType = -1;
}
*/

if ($lo->license_readingrm != 0)
{
    $course_type_tmp[] = array(1,$i_eClass_reading_room);
}
if ($lo->license_elp != 0)
{
        $course_type_tmp[] = array(2,$i_eClass_elp);
}
if ($lo->license_ssr != 0)
{
        $course_type_tmp[] = array(3,$i_eClass_ssr);
}
if ($lo->license_iPortfolio != 0)
{
        $course_type_tmp[] = array(4,$i_SpecialRoom_iPortfolio);
}

# check if there's any itextbook course
$sql = "SELECT course_id FROM ".$eclass_db.".course WHERE roomtype=7";
$itextbook_obj = $lo->returnVector($sql);

if(sizeof($itextbook_obj) > 0)
	$course_type_tmp[] = array(7,$Lang['itextbook']['itextbook']);

//unset($course_type_tmp);
if (is_array($course_type_tmp))
{
        //$course_type_normal[] = array(0,$i_eClass_normal_course);
        if ($eclass_lite_license_only)
        {
                $course_type = $course_type_tmp;
                if ($RoomType==0)
                {
                        $RoomType = $course_type_tmp[0][0];
                }
        } else
        {
                //$course_type = array_merge($course_type_normal, $course_type_tmp);
                $course_type = $course_type_tmp;
        }
    $selection = "$button_select $i_eClass_course_type: ".getSelectByArray($course_type,"name='RoomType' onChange=this.form.submit()",$RoomType,0,1);
}
else
{
        if ($eclass_lite_license_only)
        {
                eclass_closedb();
                die("<blockquote><p><br>No License!</p><blockquote>");
        }
    $selection = "<input type=hidden name='RoomType' value=0>";
}

if ($RoomType=="")
{
	$RoomType = $course_type[0][0];
    if ($RoomType=="")
	{
		$RoomType = -1;
	}
}


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field == "") $field = 6;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     case 6: $field = 6; break;
     default: $field = 6; break;
}


$sql = "SELECT
               course_id,
               IF(false AND RoomType = 4, CONCAT('<a class=tableContentLink href=portfolio25/index.php?course_id=', course_id, '>', course_code, '</a>'), CONCAT('<a class=tableContentLink href=user/index.php?RoomType=$RoomType&course_id=', course_id, '>', course_code, '</a>')),
               IF(false AND RoomType = 4, CONCAT('<a class=tableContentLink href=portfolio25/index.php?course_id=', course_id, '>', course_name, '</a>'), CONCAT('<a class=tableContentLink href=user/index.php?RoomType=$RoomType&course_id=', course_id, '>', course_name, '</a>')),
               no_users,
               ifnull(max_user,'-'),
               ifnull(max_storage,'-'),
               inputdate,
               CONCAT('<input type=checkbox name=course_id[] value=', course_id ,'>')
          FROM
               course
          WHERE
                           (course_name like '%$keyword%' OR course_code like '%$keyword%')
                           AND RoomType='$RoomType'
          ";



# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("course_id", "course_code", "course_name", "no_users", "max_user", "max_storage", "inputdate");
$li->db = $eclass_db;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(0, $i_eClassCourseID)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_eClassCourseCode)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(2, $i_eClassCourseName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_eClassNumUsers)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(4, $i_eClassMaxUser)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(5, $i_eClassMaxStorage)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(6, $i_eClassInputdate)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("course_id[]")."</td>\n";

if ($RoomType==0 && $lo->license!="")
{
        $l_left = $lo->ticket();
        $l_used = $lo->license - $l_left;
    if ($left < 0) $left = 0.00;
        $license_html = $i_eClassLicenseDisplayN1 . $lo->license . $i_eClassLicenseDisplay2 . $l_left . $i_eClassLicenseDisplay3 . $i_eClassLicenseDisplay4 . $l_used . $i_eClassLicenseDisplay5;
}
else if ($RoomType == 1)
{
        $l_left = $lo->ticket(1);
        $l_used = $lo->license_readingrm - $l_left;
    if ($left < 0) $left = 0.00;
        $license_html = $i_eClassLicenseDisplayR1 . $lo->license_readingrm . $i_eClassLicenseDisplay2 . $l_left . $i_eClassLicenseDisplay3 . $i_eClassLicenseDisplay4 . $l_used . $i_eClassLicenseDisplay5;
}


// TABLE FUNCTION BAR
if (!($RoomType==3 && $lo->ssr_left<=0))
{
        $toolbar = ($RoomType!=4) ? "<a class=iconLink href=\"javascript:checkGet(document.form1,'new.php')\">".newIcon()."$button_new</a>\n" : "";
}

if ($RoomType == 0)
{
//        $toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1,'new_batch.php')\">".newIcon()."$button_quickadd</a>";
        $temp = trim(get_file_content("$intranet_root/file/eclassbatch.txt"));
        if ($temp==1)
        {
//            $toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1,'import_new.php')\">".importIcon()." $i_eClass_batch_new</a>";
            $toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1,'import_member.php')\">".importIcon()." $i_eClass_batch_import</a>";
        }
}

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'course_id[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= ($RoomType==3 || $RoomType==4) ? "" : "<a href=\"javascript:checkEdit(document.form1,'course_id[]','copy.php')\"><img src='/images/admin/button/t_btn_copy_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
#$functionbar .= "<a href=\"javascript:checkRemoveAll(document.form1,'remove_all.php')\"><img src='/images/admin/button/t_btn_delete_all_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= ($RoomType==4) ? "" : "<a href=\"javascript:checkRemove(document.form1,'course_id[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
if($RoomType!=4)
{
	$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
	$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}

include_once("../../templates/adminheader_eclass.php");

?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, '') ?>
<?= displayTag("head_Specialroom_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $selection ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<?php if ($license_html!="") { ?>
<tr><td><?= $license_html ?></td></tr>
<?php } ?>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<?php

if ($RoomType>0 && $RoomType!=7)
{
?>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<?php
}
?>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<p><br></p>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php

eclass_closedb();
include_once("../../templates/adminfooter.php");
?>
