<?php
include_once("../../../includes/global.php");
//include_once("../../../includes/libdb.php");
include_once("$eclass_filepath/src/includes/php/lib-db.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("$eclass_filepath/src/includes/php/lib-groups.php");
include_once("../../../lang/lang.$intranet_session_language.php");

# include eClass's language file
$tmp_room_type = $ck_room_type;
$ck_room_type = "iPORTFOLIO";
include_once("$eclass_filepath/system/settings/lang/$lang");

eclass_opendb();

$portfolio_config_file = "$eclass_filepath/system/settings/portfolio.php";
if(file_exists($portfolio_config_file))
{
	include_once($portfolio_config_file);
}

# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);

// reading room
if ($RoomType!=4)
{
	header("Location: ../");
}

include_once("common.php");

# get the group name
$sql = "SELECT group_name FROM ".classNamingDB($course_id).".grouping WHERE group_id = '$group_id'";
$row = $lo->returnVector($sql);
$GroupName = $row[0];

# get current settings
$sql = "SELECT mgt_grouping_function_id, function_right FROM ".classNamingDB($course_id).".mgt_grouping_function WHERE group_id = '$group_id' LIMIT 1";
$row = $lo->returnArray($sql, 2);
list($mgt_grouping_function_id, $gfunction) = $row[0];

//$function_right_table = getFunctionConfigForm($group_id);
# Skip subject settings in ipf 2.5 or above
if($iportfolio_version != "" && $iportfolio_version != "2.0")
{
  for($i=0; $i<count($student_profile_functions); $i++)
  {
    if($student_profile_functions[$i][0] != "Profile:Subject")
      $t_student_profile_functions[] = $student_profile_functions[$i];
  }
  $student_profile_functions =$t_student_profile_functions; 
}
$profile_row_num = sizeof($student_profile_functions);
$sharing_row_num = sizeof($sharing_functions);
$rx .= "<table width='560' border='1' cellpadding='5' cellspacing='0' align='center' bordercolordark='#DBD6C4' bordercolorlight='#FCF7E5'>\n";
// table header
$rx .= "<tr>\n";
$rx .= "<td class='tableTitle'>&nbsp;</td>\n";
$rx .= "<td class='tableTitle'>&nbsp;</td>\n";
$rx .= "<td class='tableTitle' align='center'>".IsAllCheck("gfunction[]").$namelist_groups_admin_right."</td>\n";
$rx .= "</tr>\n";
$rx .= "<tr>\n";
$rx .= "<td class='tableTitle' align='center' valign='middle' rowspan='".$profile_row_num."'>".$ec_iPortfolio['school_profile']."</td>\n";
$rx .= "<td class='tableContent2' align='center'>".$student_profile_functions[0][1]."</td>\n";
$rx .= "<td class='tableContent' align='center'>".(IsCheck("gfunction[]", $student_profile_functions[0][0], $gfunction))."</td>\n";
$rx .= "</tr>\n";
for($i=1; $i<$profile_row_num; $i++)
{
	$rx .= "<tr>\n";
	$rx .= "<td class='tableContent2' align='center'>".$student_profile_functions[$i][1]."</td>\n";
	$rx .= "<td class='tableContent' align='center'>".(IsCheck("gfunction[]", $student_profile_functions[$i][0], $gfunction))."</td>\n";
	$rx .= "</tr>\n";
}

if($portfolio_config['learning_portfolio']==1)
{
	$rx .= "<tr>\n";
	$rx .= "<td class='tableTitle' align='center' valign='middle' rowspan='".$sharing_row_num."'>".$ec_iPortfolio['learning_sharing']."</td>\n";
	$rx .= "<td class='tableContent2' align='center'>".$sharing_functions[0][1]."</td>\n";
	$rx .= "<td class='tableContent' align='center'>".(IsCheck("gfunction[]", $sharing_functions[0][0], $gfunction))."</td>\n";
	$rx .= "</tr>\n";
	for($i=1; $i<$sharing_row_num; $i++)
	{
		$rx .= "<tr>\n";
		$rx .= "<td class='tableContent2' align='center'>".$sharing_functions[$i][1]."</td>\n";
		$rx .= "<td class='tableContent' align='center'>".(IsCheck("gfunction[]", $sharing_functions[$i][0], $gfunction))."</td>\n";
		$rx .= "</tr>\n";
	}
}
if($portfolio_config['school_based_scheme']==1)
{
	$rx .= "<tr>\n";
	$rx .= "<td class='tableTitle' align='center'>".$ec_iPortfolio['growth_scheme']."</td>\n";
	$rx .= "<td class='tableContent2'>&nbsp;</td>\n";
	$rx .= "<td class='tableContent' align='center'>".(IsCheck("gfunction[]", "Growth", $gfunction))."</td>\n";
	$rx .= "</tr>\n";
}

$rx .= "<tr>\n";
$rx .= "<td class='tableTitle' align='center'>".$ec_iPortfolio['view_alumni']."</td>\n";
$rx .= "<td class='tableContent2'>&nbsp;</td>\n";
$rx .= "<td class='tableContent' align='center'>".(IsCheck("gfunction[]", "Alumni", $gfunction))."</td>\n";
$rx .= "</tr>";
$rx .= "</table>\n";

include("../../../templates/adminheader_eclass.php");
?>

<form name="form1" method="post" action="grouping_function_settings_update.php">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../index.php?RoomType=$RoomType", $lo->course_code ." ". $lo->course_name, "../user/index.php?course_id=$course_id", $i_eClass_eclass_group, "../user/group/index.php?course_id=$course_id", $namelist_groups_set_function_rights, '') ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="30"><b><?=$namelist_groups_name?>: <?=$GroupName?></b></td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?= $rx ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
<input type=hidden name=course_id value="<?= $course_id ?>">
<input type=hidden name=group_id value="<?= $group_id ?>">
<input type=hidden name=mgt_grouping_function_id value="<?= $mgt_grouping_function_id ?>">
</form>

<?php
eclass_closedb();
include("../../../templates/adminfooter.php");
?>