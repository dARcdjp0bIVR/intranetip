<?php
include_once("../../../includes/global.php");
//include_once("../../../includes/libdb.php");
include_once("$eclass_filepath/src/includes/php/lib-db.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("$eclass_filepath/src/includes/php/lib-groups.php");
include_once("../../../lang/lang.$intranet_session_language.php");

# include eClass's language file
$tmp_room_type = $ck_room_type;
$ck_room_type = "iPORTFOLIO";
include_once("$eclass_filepath/system/settings/lang/$lang");

eclass_opendb();

$portfolio_config_file = "$eclass_filepath/system/settings/portfolio.php";
if(file_exists($portfolio_config_file))
{
	include_once($portfolio_config_file);
}

# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);

// maybe set by checking what rights are allowed to be set
$right_ids = "1,2";

$lg = new libgroups($eclass_prefix."c$course_id");


// reading room
if ($RoomType!=4)
{
	header("Location: ../");
}


include_once("common.php");

// load settings
$filecontent = trim(get_file_content($portfolio_setting_file));

$access_config = unserialize($filecontent);

$access_right_table = getAccessConfigForm($portfolio_functions, $access_config);



include("../../../templates/adminheader_eclass.php");
?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>&nbsp;</td></tr>
<tr><td>
iPortfolio Function Access Right Management is no longer in use.  To perform your settings, go to <strong>iPortfolio > Settings > Group > Function Access Right</strong> of the front-end.<br /><br />
學習檔案功能權限設定已經停用。要進行有關設定，請前往前台 <strong>學習檔案  > 設定 > 小組 > 功能權限設定</strong>。
<hr size=1 class="hr_sub_separator"></td></tr>
<tr><td align="right"><a href="../user/index.php?course_id=<?=$course_id?>"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
<!--form name="form1" method="post" action="access_right_update.php">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../index.php?RoomType=$RoomType", $lo->course_code ." ". $lo->course_name, "../user/index.php?course_id=$course_id", $ec_iPortfolio['right_title'], '') ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?=$ec_iPortfolio['right_instruction']?></td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?= $access_right_table ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:window.location.reload();"><img src="/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="../user/index.php?course_id=<?=$course_id?>"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<p><br></p>
<input type=hidden name="course_id" value="<?php echo $lo->course_id; ?>">
<input type=hidden name="right_ids" value="<?=$right_ids?>">
</form-->
<br>
<?php
eclass_closedb();
include("../../../templates/adminfooter.php");
$ck_room_type = $tmp_room_type;
?>
