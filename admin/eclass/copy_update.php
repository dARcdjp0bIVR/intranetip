<?php
/** [Modification Log] Modifying By: Max
 * *******************************************
 * *******************************************
 */
@SET_TIME_LIMIT(6000);
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libeclass40.php");
include("../../includes/libfilesystem.php");
eclass_opendb();

$course_code = htmlspecialchars($course_code);
$course_name = htmlspecialchars($course_name);
$course_desc = htmlspecialchars($course_desc);
$max_user = htmlspecialchars(0 + $max_user);
$max_storage = htmlspecialchars(0 + $max_storage);
if($max_user == 0) $max_user = "NULL";
if($max_storage == 0) $max_storage = "NULL";

$lo = new libeclass();
$lo->setRoomType($RoomType);
if($lo->license != "" &&  $lo->ticket() == 0){
     die();
}else{
     $course_id = $lo->eClassCopy($course_id, $course_code, $course_name, $course_desc, $max_user, $max_storage);
     ####################################
     ## TODO: Delete student's quiz info
     # if RoomType == 2 i.e. eLC && does not check the "Keep student Data"
     	if ($RoomType==2 && !$keep_quiz_result) {
     		# $lo->removeELPData($course_id);
     		$removeResult = $lo->removeELPData($course_id);
     	}
     ####################################
     $lo->eClassSubjectUpdate($subj_id, $course_id);
     if ($eclass_version >=3.0 && $is_release_file) {
         $sql = "UPDATE eclass_file SET memberType='S', User_id=0, UserName=null, UserEmail=null WHERE memberType='T'";
         $lo->db = $eclass_prefix."c$course_id";
         $lo->db_db_query($sql);
     }

}

eclass_closedb();
header("Location: index.php?msg=1&RoomType=$RoomType");
?>