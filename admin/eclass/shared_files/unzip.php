<?php
include_once("../../../includes/global.php");
include_once("../../../lang/email.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
if ($courseID=="" || $categoryID=="")
{
	header("Location: index.php");
	die();
}
include_once("../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);


intranet_opendb();

$ck_memberType = "Z";

$fm = new fileManager($courseID, $categoryID, $folderID);
$vPath = stripslashes($fm->getVirtualPath());

$file_ids = (is_array($file_id)) ? implode(",", $file_id) : $file_id;
for($i=0;$i<sizeof($file_id);$i++){
	$hidden .= "<input type=hidden name='file_id[]' value='".$file_id[$i]."'>\n";
}
if ($file_ids!="") {
	$sql  = "SELECT Title, IsDir, Size FROM eclass_file WHERE FileID IN ($file_ids) ORDER BY IsDir desc, Title asc ";
	$row = $fm->returnArray($sql, 3);
	for ($i=0; $i<sizeof($row); $i++) {
		$fileNFolder .= ($row[$i][1]==1) ?
			"<img src='$eclass_url_root/images/icon/resources/files/folder.gif' align='absmiddle' hspace='3' border='0'>".$row[$i][0]." - ".number_format($row[$i][2])." $file_kb<br>" :
			"<img src='$eclass_url_root/images/icon/resources/files/file.gif' align='absmiddle' hspace='3' border='0'>".$row[$i][0]." - ".number_format($row[$i][2])." $file_kb<br>";
	}
	if (!strstr(strtoupper($row[0][0]), ".ZIP") || $row[0][1]==1)
		$notZipFile = true;
}
?>

<script language=javascript>
function checkform(obj) {
	var msg = "<?=$classfiles_alertMsgFile15?>";
	if(confirm(msg)){
		return true;
	} else
	{
		return false;
	}
}
</script>


<form name=form1 method="post" action="unzip_update.php" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Shared_Files, 'javascript:history.back()', $button_move, '') ?>
<?= displayTag("head_teachingsharingarea_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=55% border=0 cellpadding=5 cellspacing=0 align="center">
  <tr>
	<td colspan=2 class=bodycolor3 align=center><u><?=$profiles_from?></u></td>
  </tr>
  <tr>
	<td align=right nowrap valign="top"><span class=title><?=$file_location?>:</span>&nbsp;</td>
	<td >
	<?= stripslashes($fm->getVirtualPath()) ?>
	</td>
  </tr>
  <tr>
	<td align=right nowrap valign="top"><span class=title><?php echo $file_file."/".$file_folder; ?>:</span>&nbsp;</td>
	<td >
	  <?= $fileNFolder ?><br>
	</td>
  </tr>
<?php
if (!$notZipFile) {
?>
  <tr>
	<td colspan=2 class=bodycolor3 align=center><span class=title><?=$profiles_to?></span></td>
  </tr>
  <tr>
	<td align=right><span class=title><?=$file_location?>:</span>&nbsp;</td>
	<td valign="top">
	  <select name="dest_folder">
		<?php echo $fm->getFolderOptionAdmin($folderID, $categoryID, $courseID, 1, 1); ?>
	  </select>
	</td>
  </tr>
<?php
} else {
?>

<tr>
	<td colspan=2 class=bodycolor align=center><font color='red'><?=$file_msg3?></font></td>
</tr>

<?php
}
?>

</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?php
if (!$notZipFile)
{
	echo btnSubmit() ." ". btnReset();
}
?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="course_id" value="<?=$courseID?>" type=hidden>
<?=$hidden?>
<p></p>
</form>

<?php
intranet_closedb();
include("../../../templates/adminfooter.php");
?>