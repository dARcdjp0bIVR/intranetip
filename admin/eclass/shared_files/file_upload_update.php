<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/email.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);

intranet_opendb();

$fs = new phpduoFileSystem();
$fm = new fileManager($courseID, $categoryID, $folderID);

$params = "categoryID=$categoryID&courseID=$courseID&folderID=$folderID";

//get username
$user_name = $i_admintitle_sa;

//location
if ($folderID!="") {
	$vPath = "'".$fm->getVirtualPath(0)."'";
	$parentID = "'$folderID'";
} else {
	$parentID = "NULL";
	$vPath = "NULL";
}

$desc = HTMLtoDB($description);

$permission = $access_all[0];

$root = $fm->getCategoryRoot($categoryID, $fm->db);
$FLocation = $fm->genUniqueID($folderID, $name);
$folderPath = $root."/".$FLocation;
$folderPath = stripslashes($folderPath);

//$location = "/".$fm->getCategoryFile($categoryID, $fm->db)."/".$FLocation; //to DB
$location = $FLocation; //to DB


for ($z=1; $z<=$file_no; $z++) {

	$name = (${"userfile".$z."_hidden"}=="") ? ${"userfile".$z."_name"} : ${"userfile".$z."_hidden"};
	$loc = ${"userfile".$z};

	$FileSize = (file_exists($loc)) ? filesize($loc)/1024 : 0;	//to KB
	if ($FileSize<1)
	{
		$FileSize = 1;
	}

	//check existence of file with same name in same folder
	$checkParent = ($folderID!="") ? "ParentDirID='".$folderID."'" : "ParentDirID is NULL ";
	$sql = "SELECT FileID, User_ID, Size FROM eclass_file ";
	$sql .= " WHERE Title='$name' AND $checkParent AND Category='".$categoryID."'";

	$updateNeed = false;
	$row = $fm->returnArray($sql, 3);
	if (sizeof($row)>0) {
		$FileID = $row[0][0];
		$Owner = $row[0][1];
		$Size = $row[0][2];
		$updateNeed = true;
	}

	/*

	$spaceNeed = ($Size!="") ? $FileSize - $Size : $FileSize;
	//YuEn: check quota (class quota & folder quota)
	if ($folderID!="") {
		$folderQuota = $fm->checkFolderQuota($folderID, $spaceNeed);
		if ($folderQuota!="0")
			$folderQuotaMsg = "<br><br>".$file_msg16.":<br>".$folderQuota;
	} else
		$folderQuota="0";
	if(!($folderID==9 || $qo->max_storage == "" || ($qo->ticketStorage()-($spaceNeed/1024))>0 )){
		header("Location: help.php?err=9&$params");
		die();
	} elseif ($folderQuota!="0") {
		header("Location: help.php?err=9&msg=".$folderQuotaMsg."&".$params);
		die();
	}
	*/

	//get file location (if exists)
	if ($updateNeed && !$locationGet) {
		$sql  = "SELECT Location FROM eclass_file WHERE FileID='$FileID' ";
		$row = $fm->returnArray($sql, 1);
		$Location = $row[0][0];
		$folderPath = $fm->getCategoryRoot($categoryID, $fm->db)."/".$Location;
		$locationGet = true;
	}

	//upload file
	$tmp_name = stripslashes($name);
	$des="$folderPath/$tmp_name";

	if($loc!="none" && file_exists($loc)) {
		if(strpos($name, ".")==0){ // Fail
			$isOk = false;
		}else{ // Success
			$a .= "$button_upload: $name ...... $file_ok <br>\n";
			$fm->createFolder(str_replace("\'", "'", $folderPath));
			$fs->phpduoCopy($loc, str_replace("\'", "'", $des));
			$isOk = true;
		}
	} else
		$isOk = false;

	if ($isOk) {
		if ($updateNeed) {
			//update only file content
			$update = " size='$FileSize', dateModified=now(), lastModifiedBy='$user_name' ";
			$sql = "UPDATE eclass_file SET $update WHERE FileID='$FileID' ";
			$fm->db_db_query($sql);

		} else {
			//new
			$fieldnames = "user_id, userName, userEmail, title, description, virpath, category, permission, location, size, ";
			$fieldnames .= " memberType, parentDirID, isDir, dateInput, dateModified, lastModifiedBy ";
			$fieldvalue = "'0', '$user_name', '$webmaster', '$name', '$description', $vPath, '$categoryID', '$permission', '$location', '$FileSize', ";
			$fieldvalue .= " 'T', $parentID, 0, now(), now(), '$user_name' ";
			$sql = "INSERT INTO eclass_file ($fieldnames) values ($fieldvalue)";
			//echo $sql;
			$fm->db_db_query($sql);
			$FileID = $fm->db_insert_id();
		}

		//update parent folder's size
		if ($folderID!="") $fm->updateFolderSize($folderID);
	} else {
		$a = "<p>$button_upload: $name ...... $file_notok <br>\n";
		$a .= "<p>$upload_rules <br>\n";
	}
}

intranet_closedb();
header("Location: index.php?msg=1&$params");
?>