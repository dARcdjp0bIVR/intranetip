<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/email.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);
intranet_opendb();

$fm = new fileManager($courseID, $categoryID, $folderID);

$li = new libdb();
$li->db = classNamingDB($courseID);

$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";

//get username
$user_name = $i_admintitle_sa;

$fm->checkFileName($filename, $params);

$name = HTMLtoDB($filename, 1);
$desc = HTMLtoDB($description);
$permission = $access_all[0];
if ($maxSize=="") $maxSize="NULL";

$fileID = $fid;
$IsUpdatePath = false;

if ($fileID!="") {
	//edit file

	//check whether file/folder name is changed
	$sql = "SELECT Title, IsDir, User_id, UserName, UserEmail FROM eclass_file WHERE FileID='$fileID' ";
	$row = $li->returnArray($sql, 5);
	$filename_old = $row[0][0];
	if ($filename_old!=$name)
		$IsUpdatePath = true;
	$isDir = $row[0][1];
	$User_id = $row[0][2];
	$UserName = $row[0][3];
	$UserEmail = $row[0][4];

	// update db
	$update = " title='$name', description='$description', permission='$permission', sizeMax=$maxSize, ";
	$update .= " dateModified=now(), lastModifiedBy='$user_name' ";

	// set as file owner for migrated file (for teacher or assistant only)
	$update .= ", user_id='0', userName='$user_name', userEmail='$webmaster' ";

	$sql = "UPDATE eclass_file SET $update WHERE FileID='$fileID' ";
	$li->db_db_query($sql);

	// update physical file name if necessary
	$sql = "SELECT location FROM eclass_file WHERE FileID='$fileID' ";
	$row = $li->returnArray($sql, 1);
	$Location = $row[0][0];

	$myPath = $fm->getCategoryRoot($categoryID, $li->db)."/".$Location;
	$filepath_new = str_replace("\'", "'", ($myPath."/".$filename));
	
	if ($filename!=$filename_old) {
		$filepath_old = $myPath."/".$filename_old;
		if (file_exists($filepath_old)) rename($filepath_old, $filepath_new);
	}
}

//update VirPath for itself and files inside it
if ($IsUpdatePath)
	$fm->updateVirPath($fileID);

intranet_closedb();

header("Location: index.php?msg=1&$params&reload=1");
?>
