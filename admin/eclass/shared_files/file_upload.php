<?php
include_once("../../../includes/global.php");
include_once("../../../lang/email.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
if ($courseID=="" || $categoryID=="")
{
	header("Location: index.php");
	die();
}
include_once("../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);


intranet_opendb();

$ck_memberType = "Z";

$fm = new fileManager($courseID, $categoryID, $folderID);
$vPath = stripslashes($fm->getVirtualPath());

$js_big5file = generateFileUploadNameHandler("form1","userfile1","userfile1_hidden");
?>
<?=$js_big5file?>
<script language="javascript">
function checkform(obj){
	if(!check_text(obj.userfile1, "<?php echo $file_msg7; ?>")) return false;
	Big5FileUploadHandler();
}
</script>


<form name=form1 method="post" action="file_upload_update.php" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Shared_Files, 'javascript:history.back()', $button_upload, '') ?>
<?= displayTag("head_teachingsharingarea_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?= $file_location ?>:</td><td><?=$vPath?></td></tr>
<tr><td align=right><?= $file_file ?>:</td><td><input class="file" type="file" name='userfile1' size=30><input type="hidden" name='userfile1_hidden'></td></tr>
<tr><td align=right><?= $bulletin_des ?>:</td><td><textarea class="inputfield" name=description cols=40 rows=5 wrap=virtual></textarea></td></tr>
<tr><td align=right><?= $file_access_right ?> (<?=$file_alluser?>):</td><td>
<input type="radio" name="access_all[]" value="AR"><?=$file_read?> &nbsp; &nbsp;
<input type="radio" name="access_all[]" value="AW" checked><?=$file_readwrite?>
</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="file_no" value="1" type="hidden">
<p></p>
</form>

<?php
intranet_closedb();
include("../../../templates/adminfooter.php");
?>