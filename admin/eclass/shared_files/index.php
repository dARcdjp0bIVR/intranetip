<?php
include_once("../../../includes/global.php");
include_once("../../../lang/email.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);

intranet_opendb();

$ck_memberType = "Z";
/*
$sql = "SELECT FileID FROM eclass_file WHERE IsDir=1 AND VirPath is NULL AND UserEmail='$ck_user_email'";
$li = new libdb();
$li->db = $eclass_db;
$row = $li->returnVector($sql, 1);
$folderID = $row[0][0];
*/
$courseID = $eclass_db;
$categoryID = 9;

$fm = new fileManager($courseID, $categoryID, $folderID);

$toolbar  = "<a class='iconLink' href=\"javascript:submitPage(document.form1,'folder_new.php')\">".newIcon()."$button_newfolder</a>\n";
$functionbar .= "<a href=\"javascript:submitPage(document.form1,'file_upload.php')\"><img src='/images/admin/button/t_btn_upload_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:downloadFile(document.form1,'file_id[]','download.php')\"><img src='/images/admin/button/t_btn_download_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'file_id[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'file_id[]','unzip.php')\"><img src='/images/admin/button/t_btn_unzip_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_copy(document.form1,'file_id[]','copy.php')\"><img src='/images/admin/button/t_btn_copy_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:fs_move(document.form1,'file_id[]','move.php')\"><img src='/images/admin/button/t_btn_move_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'file_id[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;<br>\n";
//$searchbar  = "<a href=\"javascript:fs_fileupload(document.form1,'upload.php')\"><img src='/images/admin/button/t_btn_upload_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
/*
$searchbar .= " <input class=text type=text size=1 maxlength=1 name=no_file value=1> $i_FileSystemFiles \n";
$searchbar .= "&nbsp;<input class=text type=text name=keyword size=10 maxlength=20 value=\"".stripslashes($keyword)."\">\n";
$searchbar .= "<a href=\"javascript:document.form1.submit()\"><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
*/

$fm->field = ($field!="") ? $field : 0;
$fm->order = ($order!="") ? $order : 1;

$display = $fm->getDBResult($courseID);

if ($err==1)
{
	$xmsg = "<font color='red'>".$file_msg4."</font>";
}
?>

<script language="JavaScript" src="<?=$eclass_url_root?>/src/includes/js/tooltip.js" type="text/javascript" ></script>
<style type="text/css">
#ToolTip{position:absolute; top: 0px; left: 0px; z-index:1; visibility:hidden;}
</style>

<script language="JavaScript">
isToolTip = true;
var folder_field = new Array("<?=$file_location?>", "<?=$quizbank_desc?>", "<?=$quizbank_id?>", "<?=$file_owner?>", "<?=$file_owner_email?>", "<?=$file_modified_by?>", "<?=$file_folder_quota?>");
var file_field = new Array("<?=$file_location?>", "<?=$quizbank_desc?>", "<?=$quizbank_id?>", "<?=$file_owner?>", "<?=$file_owner_email?>", "<?=$file_modified_by?>");
function tipsNow(text, type)
{
  if (document.readyState=="complete")
  {

	if (type==0) {
		//folder
		title = "<?=$file_folder_info?>";
		arr_txt = "folder_field";
	} else {
		//folder
		title = "<?=$file_file_info?>";
		arr_txt = "file_field";
	}
	tt= "<table border='0' cellspacing='0' cellpadding='1'>\n";
	tt += "<tr><td bgcolor='#AA9977' align=center><b>"+title+"</b></td></tr>";
	tt += "<tr><td class='tipborder'><table width='100%' border='0' cellspacing='0' cellpadding='3'>\n";

	tmp_arr = text.split("|=|");
	for (var i=0; i<tmp_arr.length; i++){
		tt += "<tr><td class='tipbg' nowrap valign='top'>" + eval(arr_txt+"["+i+"]") + ":</td><td class='tipbg'>";
		tt += (tmp_arr[i]!="") ? tmp_arr[i]+"</td></tr>" : "--</tr>";
	}
	tt += "</table></td></tr></table>\n";
	showTip('ToolTip', tt);
  }
}

function sortPage(a, b, obj){
	obj.order.value=a;
	obj.field.value=b;
	obj.submit();
}

function submitPage(obj, url){
	obj.action = url;
	obj.submit();
	return;
}

function downloadFile(obj, element, url){
	if(countChecked(obj,element)==0)
	{
		alert(globalAlertMsg2);
	} else
	{
		obj.action = url;
		obj.submit();
	}
	return;
}
</script>

<form name="form1" method="get">
<div id="ToolTip"></div>
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Shared_Files, '') ?>
<?= displayTag("head_teachingsharingarea_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan="2"><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu nowrap><?=$toolbar?></td><td class=admin_bg_menu align=right><?=$functionbar?></td></tr>
<tr><td colspan="2"><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<table width=560 border=0 cellpadding=2 cellspacing=0 align='center' style='border-right: solid #DBD6C4;  border-left: solid #DBD6C4'>
<tr><td><?=$display?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input name="courseID" value="<?=$courseID?>" type=hidden>
<input name="categoryID" value="<?=$categoryID?>" type=hidden>
<input name="folderID" value="<?=$folderID?>" type=hidden>
<input name="field" value="<?=$field?>" type=hidden>
<input name="order" value="<?=$order?>" type=hidden>
<p></p>
</form>

<?php
intranet_closedb();
include("../../../templates/adminfooter.php");
?>