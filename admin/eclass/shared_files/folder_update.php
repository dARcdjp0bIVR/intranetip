<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/email.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);

intranet_opendb();

$params = "categoryID=$categoryID&courseID=$courseID&folderID=$folderID";

//get username
$user_name = $i_admintitle_sa;

$fm = new fileManager($courseID, $categoryID, $folderID);

//location
if ($folderID!="") {
	$vPath = "'".$fm->getVirtualPath(0)."'";
	$parentID = "'$folderID'";
} else {
	$parentID = "NULL";
	$vPath = "NULL";
}

$fm->checkFileName($filename, $params);

$name = HTMLtoDB($filename, 1);
$desc = HTMLtoDB($description);

$permission = $access_all[0];

// create actual folder
$FLocation = $fm->genUniqueID($folderID, $name);
$FLocation = stripslashes($FLocation);

$folderPath = $fm->getCategoryRoot($categoryID, $fm->db)."/".stripslashes($FLocation);
$location = $FLocation; //to DB
$fm->createFolder(str_replace("\'", "'", $folderPath));

// IP: use admin email and 
$fieldnames = "user_id, userName, userEmail, title, description, virpath, category, permission, sizeMax, size, ";
$fieldnames .= " memberType, parentDirID, isDir, dateInput, dateModified, lastModifiedBy, location ";
$fieldvalue = "'0', '$user_name', '$webmaster', '$name', '$description', $vPath, '$categoryID', '$permission', NULL, 0, ";
$fieldvalue .= " 'T', $parentID, 1, now(), now(), '$user_name', '$location' ";
$sql = "INSERT INTO eclass_file ($fieldnames) values ($fieldvalue)";

$fm->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=1&$params&reload=1");
?>
