<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../lang/email.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/src/includes/php/lib-filesystem.php");
include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);

intranet_opendb();

if ($courseID=="") $courseID=$ck_course_id;

$li = new libdb();
$li->db = classNamingDB($courseID);

$params = "courseID=$courseID&categoryID=$categoryID&folderID=$folderID";

$fs = new phpduoFileSystem();
$fm = new fileManager($courseID, $categoryID, $folderID);
$fm->isCopy = true;

$user_name = $i_admintitle_sa;
$fm->user_name = $user_name;

if (sizeof($file_id)>0 && $course_id!="" && $dest_folder!="") {
	$tmp_id = split("\_", $dest_folder);
	if (sizeof($tmp_id)==2) {
		$fm->dest_categoryID = $tmp_id[0];
		$fm->dest_courseID = $eclass_db;
		$fm->dest_folderID = $tmp_id[1];

		for($i=0;$i<sizeof($file_id);$i++){
			// check disk space
			$sql = "SELECT Size FROM eclass_file ";
			$sql .= " WHERE FileID='".$file_id[$i]."'";
			$row = $fm->returnArray($sql, 1);
			$spaceNeed = $row[0][0];

			if ($folderID!="") {
				$fm->db = classNamingDB($fm->dest_courseID);
				$folderQuota = $fm->checkFolderQuota($fm->dest_folderID, $spaceNeed);
				$fm->db = classNamingDB($courseID);
				if ($folderQuota!="0")
					$folderQuotaMsg = "<br><br>".$file_msg16.":<br>".$folderQuota;
			} else
				$folderQuota="0";

			if (!($folderID>=9 || $qo->max_storage == "" || ($qo->ticketStorage()-($spaceNeed/1024))>0 )) {
				header("Location: help.php?err=9&".$params);
				die();
			} elseif ($folderQuota!="0") {
				header("Location: help.php?err=9&msg=".$folderQuotaMsg."&".$params);
				die();
			} else {
				$fm->moveFile($file_id[$i], $fm->dest_folderID);
			}
		}

		// update folder size
		$fm->db = classNamingDB($fm->dest_courseID);
		$fm->updateFolderSize($fm->dest_folderID);
	}
}

intranet_closedb();

$url = "index.php?msg=1&$params&reload=1";

if ($fm->error!="") {
	$url .= "&err=".$fm->error;
}
header("Location: $url");
?>