<?php
#modify by : 
/**
 * Change Log:
 * 2017-05-12 Pun [ip.2.5.8.7.1]
 *  - Fix PHP5.4 split() deprecated 
 */
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass40.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libhomework.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("eclass_common.php");
eclass_opendb();

# include eClass's language file
$tmp_room_type = $ck_room_type;
$ck_room_type = "iPORTFOLIO";
include_once("$eclass_filepath/system/settings/lang/eng.php");
$default_function_name_web_e = $ec_iPortfolio['learning_sharing'];
$default_function_name_form_e = $ec_iPortfolio['growth_view'];


if($intranet_session_language == "b5")
{
	include_once("$eclass_filepath/system/settings/lang/chib5.php");
}

/*
if ($intranet_default_lang=="gb" || (is_array($intranet_default_lang_set) && in_array("gb", $intranet_default_lang_set)))
{
	include_once("$eclass_filepath/system/settings/lang/chigb.php");
} else
{
	include_once("$eclass_filepath/system/settings/lang/chib5.php");
}
*/
$default_function_name_web_c = $ec_iPortfolio['learning_sharing'];
$default_function_name_form_c = $ec_iPortfolio['growth_view'];

# eClass
$lo = new libeclass($course_id[0]);
$eclass_quota = $lo->status();

$lgroup = new libgroup();
$info = $lgroup->getCourseGroupSubjectID($course_id[0]);
list($GroupID,$SubjectID) = $info;

$groups = $lgroup->returnGroupsByCategory(3);
$group_select = getSelectByArrayTitle($groups,"name=GroupID",$i_notapplicable,$GroupID);

$lhw = new libhomework();
$subjects = $lhw->returnSubjectTable();
$subject_select = getSelectByArrayTitle($subjects,"name=SubjectID",$i_notapplicable,$SubjectID);

$tool_select = "";

# Retrieve Equation Editor Rights
$content = trim(get_file_content($lo->filepath."/files/equation.txt"));
$equation_class = ($content=="")? array(): explode(",",$content);
$left = $lo->license_equation - sizeof($equation_class);
if ($left > 0 && !in_array($course_id[0],$equation_class))
{
    $tool_select .= "<input type=checkbox name=hasEquation value=1> $i_eClass_Tool_EquationEditor ($i_eClass_Tool_LicenseLeft: $left)";
}
else
{
    if (in_array($course_id[0],$equation_class))
    {
        $tool_select .= "$i_eClass_Tool_EquationEditorUsing";
    }
}

if ($RoomType == 3)
{
	$lo->getCourseDate();
	$StartDate = $lo->course_date["start"];
	$EndDate = $lo->course_date["end"];
	$sql = "SELECT COUNT(*) FROM ".classNamingDB($course_id[0]).".handin ";
	$row = $lo->returnVector($sql);
	$is_survey_attempted = ($row[0]>0);
	if ($is_survey_attempted && $lo->course_remark=="NAME_BLOCKED")
	{
		$name_blocked_disabled = "disabled";
		$name_blocked_remark = "<br><span class='extraInfo'>( $i_ssr_anonymous_guide )</span>";
	}
	$name_blocked_checked = ($lo->course_remark=="NAME_BLOCKED") ? "checked" : "";
} elseif ($RoomType == 4)
{
	$portfolio_config_file = "$eclass_filepath/system/settings/portfolio.php";
	if(file_exists($portfolio_config_file))
	{
		include_once($portfolio_config_file);
	}

	$lo->getCourseDate();

	# parent access setting
	$is_parent_checked = ($lo->is_parent) ? "checked" : "";

	# iPortfolio function settigns
	$arr_tmp = explode("=|=", $lo->course_remark);
	for ($i=0; $i<sizeof($arr_tmp); $i++)
	{
		list($item_index, $item_title_e, $item_title_c, $item_enabled) = explode("|", $arr_tmp[$i]);
		if (trim($item_index)!="")
		{
			$ra[$item_index] = array($item_title_e, $item_title_c, $item_enabled);
		}
	}
	$function_name_web_e = $ra["web"][0];
	$function_name_web_c = $ra["web"][1];
	$function_web_checked = ($ra["web"][2]) ? "checked" : "";
	$function_name_form_e = $ra["growth"][0];
	$function_name_form_c = $ra["growth"][1];
	$function_form_checked = ($ra["growth"][2]) ? "checked" : "";

}

$course_code_edit = ($RoomType!=4) ? "<input type='text' name='course_code' value=\"".$lo->course_code."\" size=30 maxlength=100>" : $lo->course_code."<input type='hidden' name='course_code' value=\"".$lo->course_code."\">";

$course_name_edit = ($RoomType!=4) ? "<input type='text' name='course_name' value=\"".$lo->course_name."\" size=30 maxlength=100>" : $lo->course_name."<input type='hidden' name='course_name' value=\"".$lo->course_name."\">";

include_once("../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
     if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
     	if (obj.RoomType.value==3)
	{
		if (obj.StartDate.value!="")
		{
			if(!check_date(obj.StartDate, "<?=$i_invalid_date?>")) return false;
		}
		if (obj.EndDate.value!="")
		{
			if(!check_date(obj.EndDate, "<?=$i_invalid_date?>")) return false;
		}
		if (obj.Remark.checked && !obj.Remark.disabled)
		{
			alert("<?=$i_ssr_anonymous_warning?>");
		}
	}
}
</script>


<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, 'javascript:history.back()', $button_edit." (".$str_RoomType.")", '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>

<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr>
	<td align=right><?php echo $i_eClassCourseCode; ?>:</td>
	<td><?=$course_code_edit?></td>
</tr>
<tr>
	<td align=right><?php echo $i_eClassCourseName; ?>:</td>
	<td><?=$course_name_edit?></td>
</tr>
<?php
if($RoomType!=4)
{?>
	<tr>
		<td align=right><?php echo $i_eClassCourseDesc; ?>:</td>
		<td><textarea name=course_desc cols=30 rows=5><?php echo $lo->course_desc; ?></textarea></td>
	</tr>
	<tr>
		<td align=right><?php echo $i_eClassNumUsers; ?>:</td>
		<td><input class=text type=text name=max_user size=4 maxlength=5 value="<?php echo $lo->max_user; ?>"></td>
	</tr>
<?php
}?>
<tr>
	<td align=right><?php echo $i_eClassMaxStorage; ?>:</td>
	<td><input class=text type=text name=max_storage size=4 maxlength=5 value="<?php echo $lo->max_storage; ?>"> MB</td>
</tr>

<?php if ($RoomType==3) {
?>
<tr><td align=right><?= $i_CycleSpecialStart ?>:</td><td><input class=text name="StartDate" value="<?=$StartDate?>" size=10 maxlength=10> <span class="extraInfo">(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?= $i_CycleSpecialEnd ?>:</td><td><input class=text name="EndDate" value="<?=$EndDate?>" size=10 maxlength=10> <span class="extraInfo">(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?= $i_Survey_Anonymous ?>:</td><td><input type="checkbox" name="Remark" value="NAME_BLOCKED" <?=$name_blocked_disabled?> <?=$name_blocked_checked?> ><?=$name_blocked_remark?></td></tr>
<?php } ?>

<?php if ($RoomType==4) {
?>
<tr><td align=right><?=$i_eClass_function_access?>:</td><td>
<table border="0" cellpadding="3" cellspacing="0">
<?php
if($portfolio_config['learning_portfolio']==1)
{
	?>
	<tr><td><input type="checkbox" name="is_function_web" <?=$function_web_checked?> ></td><td><?=$default_function_name_web_e?> (<?=$default_function_name_web_c?>)</td></tr>
<?php
}
if($portfolio_config['school_based_scheme']==1)
{?>
	<tr><td><input type="checkbox" name="is_function_form" <?=$function_form_checked?> ></td><td><?=$default_function_name_form_e?> (<?=$default_function_name_form_c?>)</td></tr>
	<tr><td>&nbsp;</td><td><?=$i_UserDisplayName?>:</td></tr>
	<tr><td>&nbsp;</td><td><input class=text name="function_form_e" value="<?=$function_name_form_e?>" size=20 maxlength=20><?=$i_eClass_portfolio_lang_e?></td></tr>
	<tr><td>&nbsp;</td><td><input class=text name="function_form_c" value="<?=$function_name_form_c?>" size=20 maxlength=20><?=$i_eClass_portfolio_lang_c?></td></tr>
<?php
}?>
</table>
</td></tr>
<tr><td align=right><?=$i_eClass_parent_access?>:</td><td><input type=checkbox name="is_parent" <?=$is_parent_checked?> ></td></tr>
<?php } ?>

<?php if ($RoomType==0) { ?>
<tr><td align=right><?php echo $i_eClass_ClassLink; ?>:</td><td><?=$group_select?></td></tr>
<tr><td align=right><?php echo $i_eClass_SubjectLink; ?>:</td><td><?=$subject_select?><br><?=$i_eClass_ClassSubjectNote?></td></tr>
<? if ($tool_select != "") { ?>
<tr><td align=right><?php echo $i_eClass_Tools; ?>:</td><td><?=$tool_select?></td></tr>
<? } ?>
<tr><td><br></td><td><?php echo $lo->eClassCategory(); ?></td></tr>
<?php } ?>

</table>

<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type='image' src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'>
<?= btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</p>
<input type=hidden name=course_id value="<?php echo $lo->course_id; ?>">
<input type="hidden" name="RoomType" value="<?=$RoomType?>">
</form>

<?php
eclass_closedb();
include_once("../../templates/adminfooter.php");
?>
