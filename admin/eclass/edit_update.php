<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libeclass.php");
include_once("../../includes/libfilesystem.php");
eclass_opendb();


$course_code = htmlspecialchars($course_code);
$course_name = htmlspecialchars($course_name);
$course_desc = htmlspecialchars($course_desc);
$max_user = htmlspecialchars(0 + $max_user);
$max_storage = htmlspecialchars(0 + $max_storage);
if($max_user == 0) $max_user = "NULL";
if($max_storage == 0) $max_storage = "NULL";

$lo = new libeclass();
if ($RoomType == 3)
{
	$lo->setCourseDate($StartDate, $EndDate);
	if (isset($Remark))
	{
		$lo->setCourseRemark($Remark);
	}
} elseif ($RoomType == 4)
{
	$Remark = "web|$function_web_e|$function_web_c|".(($is_function_web)?"1":"0");
	$Remark .= "=|=";
	$Remark .= "growth|$function_form_e|$function_form_c|".(($is_function_form)?"1":"0");
	$lo->setCourseRemark($Remark);
	$lo->setParentAccess($is_parent);
}
$lo->eClassUpdate($course_id, $course_code, $course_name, $course_desc, $max_user, $max_storage);
$lo->eClassSubjectUpdate($subj_id, $course_id);

# Update INTRANET_COURSE
$li = new libdb();
$sql = "DELETE FROM INTRANET_COURSE WHERE CourseID = $course_id";
$li->db_db_query($sql);

if ($GroupID != "" && $SubjectID != "")
{
    $sql = "INSERT INTO INTRANET_COURSE (CourseID,ClassGroupID, SubjectID) VALUES ('$course_id','$GroupID','$SubjectID')";
    $li->db_db_query($sql);
}

# Set Equation Editor Rights
if ($hasEquation == 1)
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $equation_class[] = $course_id;
    write_file_content(implode(",",$equation_class),$lo->filepath."/files/equation.txt");
}


eclass_closedb();
header("Location: index.php?msg=2&RoomType=$RoomType");
?>