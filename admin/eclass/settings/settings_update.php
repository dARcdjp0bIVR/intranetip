<?php
/*
Remark:
Settings are kept in two files.
One is for eClass settings used in Intranet while the other is used inside eClass.
*/

include("../../../includes/global.php");
include("../../../includes/libfilesystem.php");

$ec_ip_set[] = ($ec_mgt) ? "1" : "0";
$ec_ip_set[] = ($ec_mgt_course) ? "1" : "0";

// default email
$ec_set[] = ($ec_email_teacher_disabled) ? "0" : "1";
$ec_set[] = ($ec_email_helper_disabled) ? "0" : "1";
$ec_set[] = ($ec_email_student_disabled) ? "0" : "1";

# planner color type
for ($i=0; $i<sizeof($planner_color); $i++)
{
	$ec_planner[] = stripslashes($planner_color[$i]."||".$planner_type[$i]);
}

$li = new libfilesystem();
$li->file_write(implode("\n",$ec_ip_set)."\n", $intranet_root."/file/eclass.txt");

$li->file_write(implode("\n",$ec_set)."\n", $eclass_filepath."/files/settings.txt");

//$li->file_write(implode("\n",$ec_planner)."\n", $eclass_filepath."/files/planner.txt");



# badword #
$data = stripslashes($data);
$data = intranet_htmlspecialchars($data);

$base_dir = "$intranet_root/file/";
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$target_file = "$base_dir"."templates/bulletin_badwords.txt";
$data = write_file_content($data, $target_file);
# badword #

# powervoice #
if ($plugin['power_voice'])
{

	$pvoice_target_file = "$base_dir"."powervoice.txt";
	$pvoice_data = get_file_content($pvoice_target_file);
	$pvoice_array = unserialize($pvoice_data);
	$pvoice_array['bitrate'] = $bitrate;
	$pvoice_array['sampling_frequency'] = $samplerate;
	$pvoice_array['length'] = $voice_length;

	$pvoice_output = serialize($pvoice_array);
	$data = write_file_content($pvoice_output, $pvoice_target_file);

}
# powervoice #

header("Location: index.php?msg=2");
?>
