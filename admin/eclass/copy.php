<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libeclass40.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include_once("eclass_common.php");
eclass_opendb();

if ($RoomType==3)
{
	header("Location: index.php");
	die();
}

# eClass
$lo = new libeclass($course_id[0]);
$eclass_quota = $lo->status();

if ($RoomType==2) {
	$html["keep_result"] = '<tr><td align="right">'.$Lang['AdminConsole']['KeepStudentQuizResult'].':</td><td><input type="checkbox" value="1" name="keep_quiz_result" checked="checked"></td></tr>';
}


include("../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.course_code, "<?php echo $i_alert_pleasefillin.$i_eClassCourseCode; ?>.")) return false;
     if(!check_text(obj.course_name, "<?php echo $i_alert_pleasefillin.$i_eClassCourseName; ?>.")) return false;
}
</script>

<form name="form1" action="copy_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, 'javascript:history.back()', $button_copy, '') ?>
<?= displayTag("head_eclass_$intranet_session_language.gif") ?>

<?php if($lo->license == "" || $lo->ticket()>0){ ?>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?php echo $i_eClassCourseCode; ?>:</td><td><input class=text type=text name=course_code size=10 maxlength=10 value="<?php echo $lo->course_code; ?>"></td></tr>
<tr><td align=right><?php echo $i_eClassCourseName; ?>:</td><td><input class=text type=text name=course_name size=30 maxlength=100 value="Copy Of <?php echo $lo->course_name; ?>"></td></tr>
<tr><td align=right><?php echo $i_eClassCourseDesc; ?>:</td><td><textarea name=course_desc cols=30 rows=5><?php echo $lo->course_desc; ?></textarea></td></tr>
<tr><td align=right><?php echo $i_eClassNumUsers; ?>:</td><td><input class=text type=text name=max_user size=2 maxlength=3 value="<?php echo $lo->max_user; ?>"></td></tr>
<tr><td align=right><?php echo $i_eClassMaxStorage; ?>:</td><td><input class=text type=text name=max_storage size=2 maxlength=3 value="<?php echo $lo->max_storage; ?>"> MB</td></tr>

<?php if ($RoomType==0) { ?>
<tr><td><br></td><td><?php echo $lo->eClassCategory(); ?></td></tr>
<?php } ?>

<?php if ($eclass_version >= 3.0) { ?>
<tr><td align='right'><?=$i_eClassAdminFileRight ?>:</td><td><input type="checkbox" value="1" name="is_release_file"></td></tr>
<?php } ?>
<?=$html["keep_result"]?>
</table>
<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</p>
<?php }
else
{
    echo $i_eClassLicenseFull;
}
?>
<p><br></p>
<input type=hidden name=course_id value="<?php echo $lo->course_id; ?>">
<input type="hidden" name="RoomType" value="<?=$RoomType?>">
</form>

<?php
eclass_closedb();
include("../../templates/adminfooter.php");
?>