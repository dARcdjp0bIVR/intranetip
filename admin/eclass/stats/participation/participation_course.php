<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../lang/email.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libeclass.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

if (!isset($course_id) || $course_id=="")
{
	header("Location: ./");
	die();
}

include_once("../../../../templates/adminheader_eclass.php");
include_once("$eclass_filepath/src/includes/php/lib-participation.php");
include_once("$eclass_filepath/system/settings/lang/".$lang);
include_once("$eclass_filepath/system/settings/lang/admin/".substr($lang, 0, strpos($lang, ".")).".language.php");


$functions[] = array($contents, "contents", "/src/course/contents/index.php");
$functions[] = array($CourseWork, "course_work", "/src/student/work_student/index.php");
$functions[] = array($Assessment, "assessment", "/src/assessment/selftest/index.php", "/src/assessment/practice/index.php", "/src/assessment/testroom/index.php");
$functions[] = array($TutorBox, "tutor_box", "/src/student/information/message_box/index.php");
$functions[] = array($SocialCorner, "social_corner", "/src/social_corner/bulletin/index.php", "/src/social_corner/directory/index.php", "/src/course/resources/links/help.php");
$functions[] = array($ClassFiles, "class_files", "/src/course/resources/files/index.php");


intranet_opendb();

$lo = new libeclass($course_id);

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field=0;
if($filter=="") $filter=0;
switch ($filter){
	case 0:
		//$sql_filter=" and a.memberType IN ('A','S','T')";
		break;
	case 1: // teacher
		$sql_filter=" and memberType IN ('T')";
		break;
	case 2: // assistant
		$sql_filter=" and memberType IN ('A')";
		break;
	case 3: // student
		$sql_filter=" and memberType IN ('S')";
		break;
}

$now = time();
$today = date('Y-m-d',$now)." 23:59:59";
if($searchBy==="")
{
	$searchBy = 0;
}
else if($searchBy==1 && $start=="" && $end=="")
{
	$start =  date('Y-m-d',$now);
	$end = $start;
}
if ($searchBy==1)
{
	if($start!="" && $end!="" && $start!=$end)
	{
		$sql_range = "AND UNIX_TIMESTAMP(s.inputdate) BETWEEN UNIX_TIMESTAMP('$start') AND (UNIX_TIMESTAMP('$end')+86400) ";
	}
	else 
	{
		if($start==$end || $end=="")
		{
			$exact_date = $start;
			$end = $start;
		}
		else if($start=="")
		{
			$exact_date = $end;
			$start = $end;
		}
		$sql_range = "AND UNIX_TIMESTAMP(s.inputdate) BETWEEN UNIX_TIMESTAMP('$exact_date') AND (UNIX_TIMESTAMP('$exact_date')+86400) ";
	}
}
else
{
	if($range=="") $range=0;
	switch ($range){
		case 0: // today
			$sql_range="AND TO_DAYS('".$today."')=TO_DAYS(s.inputdate)";
			break;
		case 1: // last week
			//$sql_range="AND year(now())=year(s.inputdate) and week(now())-week(s.inputdate)<=0";
			$sql_range="AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(s.inputdate)<=604800 ";
			break;
		case 2: // last 2 weeks
			//$sql_range="AND year(now())=year(s.inputdate) and week(now())-week(s.inputdate)<=1";
			$sql_range="AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(s.inputdate)<=1209600 ";
			break;
		case 3: // last month
			//$sql_range="AND year(now())=year(s.inputdate) and (month(now())-1=month(s.inputdate) or month(now())=month(s.inputdate))";
			$sql_range="AND UNIX_TIMESTAMP('".$today."')-UNIX_TIMESTAMP(s.inputdate)<=2678400 ";
			break;
		case 4: // all
			$sql_range="";
			break;
	}
}

// build temp duration table
$lu = new participation();
$lu->db = classNamingDB($course_id);
$lu->functions = $functions;
$lu->returnUserHits(1, $sql_filter, $sql_range);


$fieldname = "CONCAT(a.lastname, if(a.lastname is null OR a.lastname='', '', ' '), a.firstname, if(a.class_number='' OR a.class_number IS NULL, '', CONCAT(' <', a.class_number, '>') )) ";
for ($i=0; $i<sizeof($functions); $i++)
{
	$fieldname .= ",ifnull(".$functions[$i][1].", 0) ";
}
$sql  = "SELECT $fieldname FROM usermaster AS a LEFT JOIN ec_login_hits AS h ON h.user_id=a.user_id ";
$sql .= "WHERE (a.user_email like '%$keyword%' OR a.firstname like '%$keyword%' OR a.lastname like '%$keyword%') ";
$sql .= $sql_filter;

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array[] = "CONCAT(a.class_number, a.lastname, a.firstname)";
for ($i=0; $i<sizeof($functions); $i++)
{
	$li->field_array[] = $functions[$i][1];
}
$li->db = classNamingDB($course_id);
$li->sql = $sql;
$li->title = $admin_user;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $i_admintitle_eclass;
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1% height='20' background='$BackTitle' class=tableTitle>&nbsp;#&nbsp;</td>\n";
$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(0, $i_UserName)."</td>\n";
for ($i=0; $i<sizeof($functions); $i++)
{
	$li->column_list .= "<td background='$BackTitle' class=tableTitle>".$li->column(($i+1), $functions[$i][0])."</td>\n";;
}

if($searchBy==0)
{
	$periodSelectTable  .= "<select name=range onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
	$periodSelectTable .= "<option value=0 ".(($range==0)?"selected":"").">$admin_range1</option>\n";
	$periodSelectTable .= "<option value=1 ".(($range==1)?"selected":"").">$admin_range2</option>\n";
	$periodSelectTable .= "<option value=2 ".(($range==2)?"selected":"").">$admin_range3</option>\n";
	$periodSelectTable .= "<option value=3 ".(($range==3)?"selected":"").">$admin_range4</option>\n";
	$periodSelectTable .= "<option value=4 ".(($range==4)?"selected":"").">$admin_range5</option>\n";
	$periodSelectTable .= "</select> \n";

	$searchDisplay1 = $periodSelectTable;
	$searchDisplay2 = "<a href=\"javascript:changeDateTable(1)\" class='iconLink'>".$i_Profile_SearchByDate."</a>\n";
}
else
{
	$dateSearchTable .= "$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To <input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) <a href='javascript:document.form1.pageNo.value=1;document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";

	$searchDisplay1 = "<a href=\"javascript:changeDateTable(0)\" class='iconLink'>".$i_Profile_SearchByPeriod."</a>";
	$searchDisplay2 = $dateSearchTable;
}

$searchbar = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$admin_all_users</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$admin_teacher</option>\n";
$searchbar .= "<option value=2 ".(($filter==2)?"selected":"").">$admin_assistant</option>\n";
$searchbar .= "<option value=3 ".(($filter==3)?"selected":"").">$admin_student</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input size=10 type=text name=keyword  maxlength=255 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>
<script language="javascript">
function changeDateTable(type)
{
	var formObj = document.form1;
	formObj.searchBy.value = type;
	formObj.pageNo.value=1;
	formObj.submit();
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_Stats, '../', $i_eClass_Admin_stats_participation, './', $lo->course_code ." ". $lo->course_name, '') ?>
<?= displayTag("head_eclassstatisitcs_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan="2"><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu valign="top" height="35">&nbsp;<?=$searchDisplay1?></td><td nowrap="nowrap" class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td colspan="2" class=admin_bg_menu valign="bottom">&nbsp;<?=$searchDisplay2?></td></tr>
<tr><td colspan="2"><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?php echo $li->display(); ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=course_id value="<?=$course_id?>">
<input type=hidden name=searchBy value="<?=$searchBy?>"/>

<p></p>
</form>

<?php
intranet_closedb();
include("../../../../templates/adminfooter.php");
?>