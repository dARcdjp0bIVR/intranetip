<?php
##	Modifying By: 
$PATH_WRT_ROOT = '../../';
include_once("{$PATH_WRT_ROOT}includes/global.php");
include_once("{$PATH_WRT_ROOT}includes/libdb.php");
include_once("{$PATH_WRT_ROOT}includes/libeclass40.php");
include_once("{$PATH_WRT_ROOT}includes/libfilesystem.php");
include_once("{$PATH_WRT_ROOT}lang/lang.$intranet_session_language.php");

//include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
eclass_opendb();

$course_code = htmlspecialchars($course_code);
$course_name = htmlspecialchars($course_name);
$course_desc = htmlspecialchars($course_desc);
$max_user = htmlspecialchars(0 + $max_user);
$max_storage = htmlspecialchars(0 + $max_storage);
if($max_user == 0) $max_user = "NULL";
if($max_storage == 0) $max_storage = "NULL";

$lo = new libeclass();

$lo->setRoomType($RoomType);

//$lo->getConnectionCharacterSet();
$valid = false;

if ($RoomType == 0)
{
    if($lo->license != "" &&  $lo->ticket() == 0)
    {
       $valid = false;
    }
    else $valid = true;
} elseif ($RoomType == 1)
{
     if($lo->ticket(1)==0)
     {
        $valid = false;
     }
     else $valid = true;
} elseif ($RoomType == 2)
{
	# OW: detection
	$isOnlineWorksheet = (substr($ELPSelected, 0, 3)=="OW_");
	//$is_CNECC_R = strstr($ELPSelected, "CNECC_R");
	$is_CNECC_R = strpos($ELPSelected, "CNECC_R") !== false;

	$valid = true;
	 // set ReferenceID = courseware ID
	 if ($ELPSelected)
	 {
		$lo->rights_student = "Course:Notes, SocialCorner:Bulletin";
		$lo->default_helper_job = "Course:Contents,Student:Information,Student:Usage,SocialCorner:Bulletin";
		$courseware_index = $eclass_filepath."/files_elp/".$ELPSelected."/courseware_index.php";
//debug_r($courseware_index);die;
//debug_r(file_exists($courseware_index));die;
		if (file_exists($courseware_index))
		{
			include_once($courseware_index);
			$lo->setReferenceID($elp_info['id']);
		} elseif ($isOnlineWorksheet || $is_CNECC_R)
		{
			if ($is_CNECC_R)
			{
				$lo->default_helper_job = "";
			}
			$elp_info['language'] = "chib5";
			$lo->setReferenceID($ELPSelected);
			$lo->setCourseRemark($ELPSelected);
		} else
		{
			echo "<blockquote>No courseware configuration file exist!</blockquote>";
			die();
		}
	 }
} elseif ($RoomType == 3)
{
	$valid = true;
	// set ReferenceID = courseware ID
	if ($SSRSelected)
	{
		$lo->rights_student = "Student:Work";
		$lo->default_helper_job = "Student:Survey";
		$lo->setReferenceID(strtoupper(substr($SSRSelected, 0, strpos($SSRSelected, "."))));
		$lo->setCourseDate($StartDate, $EndDate);
		$lo->setCourseRemark($Remark);
	}
} elseif ($RoomType == 4)
{
	$valid = true;
	$lo->rights_student = "Course:Notes, Course:Files, Contents:indexOrder ";
	$lo->default_helper_job = "";

	# iPortfolio config!
	$Remark = "web|$function_web_e|$function_web_c|".(($is_function_web)?"1":"0");
	$Remark .= "=|=";
	$Remark .= "growth|$function_form_e|$function_form_c|".(($is_function_form)?"1":"0");

	$lo->setCourseRemark($Remark);
	$lo->setParentAccess($is_parent);
} else
{
	$valid = false;
}

if(!$valid){
     die();
}else{
     $course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
     $lo->eClassSubjectUpdate($subj_id, $course_id);
	 if ($RoomType == 2)
	 {
		// update language if necessary
		if ($elp_info['language']=="chigb" || $elp_info['language']=="chib5")
		{
			$sql_lang = "UPDATE course SET language='{$elp_info['language']}' WHERE course_id='$course_id' ";
			$lo->db_db_query($sql_lang);
		}

		if ($isOnlineWorksheet)
		{
			# Online Worksheet
			$userfile = "$eclass_filepath/files_elp/online_worksheet/{$ELPSelected}.ec";
//			echo $userfile;die;
			if (file_exists($userfile))
			{
				include_once("$eclass_filepath/src/includes/php/lib-packing.php");
				$lp = new packing($course_id, $userfile, "", "");
				$lp->user_name = "System Admin";
				$lp->doUnpack();
				$lo->publicAllExercises($course_id);
			} else
			{
				die("Failed: Online Worksheet package ($userfile) is not installed successfully.");
			}
		} elseif ($is_CNECC_R)
		{
			# Lingman
			$userfile = "$eclass_filepath/files_elp/CNECC_R/{$ELPSelected}.ec";
			if (file_exists($userfile))
			{
				include_once("$eclass_filepath/src/includes/php/lib-packing.php");
				$lp = new packing($course_id, $userfile, "", "");
				$lp->user_name = "System Admin";
				$lp->doUnpack();
				$lo->publicAllExercises($course_id);
			} else
			{
				die("Failed: Online Worksheet package ($userfile) is not installed successfully.");
			}
		} else
		{
			$lo->eClassBuildContents($elp_index, $elp_exercise, $elp_quiz, $course_id, $ELPSelected);
		}
	 }

	 if ($RoomType == 3)
	 {
		// update language if necessary

		// build survey and add groups
		$ssr_file = "$eclass_filepath/files/addon/ssr/".$SSRSelected;
		//include_once("$eclass_filepath/src/includes/php/lib-filemanager.php");
		include_once("$eclass_filepath/src/includes/php/lib-packing.php");
		$lp = new packing($course_id, $ssr_file);
		$survey_date = ($survey_date!="") ? $survey_date." 23:59:59" : "";
		$lp->doUnpack($survey_date);
	 }
}

# Set Equation Editor Rights
if ($hasEquation == 1)
{
    $content = trim(get_file_content($lo->filepath."/files/equation.txt"));
    $equation_class = ($content=="")? array(): explode(",",$content);
    $equation_class[] = $course_id;
    write_file_content(implode(",",$equation_class),$lo->filepath."/files/equation.txt");
}

# Update INTRANET_COURSE
if ($GroupID != "" && $SubjectID != "")
{
    $li = new libdb();
    $sql = "INSERT INTO INTRANET_COURSE (CourseID,ClassGroupID,SubjectID) VALUES ('$course_id','$GroupID','$SubjectID')";
    $li->db_db_query($sql);
}
eclass_closedb();
header("Location: index.php?msg=1&RoomType=$RoomType");
?>