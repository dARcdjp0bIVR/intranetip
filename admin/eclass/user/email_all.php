<?php
@SET_TIME_LIMIT(600);
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libemail.php");
include("../../../includes/libsendmail.php");
include("../../../lang/email.php");
intranet_opendb();

$li = new libdb();
// Original, send to users who have not login yet
//$sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, a.UserEmail, a.FirstName, a.LastName FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b WHERE a.LastUsed Is Null AND a.RecordStatus = 1 AND a.UserID = b.UserID AND b.GroupID = $TabID";
$sql = "SELECT DISTINCT a.UserID, a.UserLogin, a.UserPassword, a.UserEmail, a.EnglishName, a.Title FROM INTRANET_USER AS a, ".$eclass_db.".user_course AS uc WHERE course_id='".IntegerSafe($course_id)."' AND a.UserEmail=uc.user_email";
$row = $li->returnArray($sql, 6);
for($i=0; $i<sizeof($row); $i++){
     $UserID = $row[$i][0];
     $UserLogin = $row[$i][1];
     $UserPassword = $row[$i][2];
     $UserEmail = $row[$i][3];
     $EnglishName = $row[$i][4];
     $title = $row[$i][5];
     # Call sendmail function
     $mailSubject = registration_title();
     $mailBody = registration_body($UserEmail, $UserLogin, $UserPassword, $title,$EnglishName);
     $mailTo = $UserEmail;
     $lu = new libsendmail();
     $lu->SendMail( $mailTo, $mailSubject, $mailBody,"");
}

intranet_closedb();
header("Location: index.php?course_id=$course_id");
?>
