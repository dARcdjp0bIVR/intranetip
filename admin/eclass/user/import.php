<?php

// Modifing by fai

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libgrouping.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader_admin.php");
include_once("$eclass40_filepath/system/settings/lang/$lang");
intranet_opendb();

# eClass
$lo = new libeclass($course_id);

# retrieve eclass record status
$x0 .= "<select name=MemberType>\n";
$x0 .= ($RoomType!=4) ? "<option value=S ".(($MemberType=="S")?"SELECTED":"").">&nbsp;S (".$usertype_s.")&nbsp;</option>" : "";
$x0 .= "<option value=T ".(($MemberType=="T")?"SELECTED":"").">&nbsp;T (".$usertype_t.")&nbsp;</option>";
$x0 .= ($RoomType!=4) ? "<option value=A ".(($MemberType=="A")?"SELECTED":"").">&nbsp;A (".$usertype_a.")&nbsp;</option>" : "";
$x0 .= "</select>\n";

# retrieve group category
$li = new libgrouping();
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();
if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}
/*
$x1 .= "<option value=3 ".(($CatID==3)?"SELECTED":"").">".$i_GroupRole[3]."</option>\n";
$x1 .= "<option value=1 ".(($CatID==1)?"SELECTED":"").">".$i_GroupRole[1]."</option>\n";
$x1 .= "<option value=2 ".(($CatID==2)?"SELECTED":"").">".$i_GroupRole[2]."</option>\n";
$x1 .= "<option value=4 ".(($CatID==4)?"SELECTED":"").">".$i_GroupRole[4]."</option>\n";
$x1 .= "<option value=5 ".(($CatID==5)?"SELECTED":"").">".$i_GroupRole[5]."</option>\n";
$x1 .= "<option value=6 ".(($CatID==6)?"SELECTED":"").">".$i_GroupRole[6]."</option>\n";
*/
$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";
/*
$row = $li->returnCategoryGroups(0);
for($i=0; $i<sizeof($row); $i++){
     $IdentityCatID = $row[$i][0] + 10;
     $IdentityCatName = $row[$i][1];
     $x1 .= "<option value=$IdentityCatID ".(($IdentityCatID==$CatID)?"SELECTED":"").">$IdentityCatName</option>\n";
}
*/
$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
$x1 .= ($RoomType!=4) ? "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n" : "";
$x1 .= ($RoomType!=4) ? "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n" : "";
$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";


if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          if ($GroupCatID != $GroupID)
          {
              $x2 .= "<option value=$GroupCatID";
              for($j=0; $j<sizeof($ChooseGroupID); $j++){
                  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
              }
              $x2 .= ">$GroupCatName</option>\n";
          }
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

//selection of all classes
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;
   
   //if ($selectedUserType == 2 || $selectedUserType == 3)
   if ($selectedUserType == 3)
   {
	   $x3 = $select_class;
	   
	   if (isset($targetClass) && $targetClass!="")
	   {
		   if ($selectedUserType == 3)	//parent
		   {
			   	// get all students from the class
			   	$username_field = getNameFieldWithClassNumberByLang("");
                $sql = "SELECT UserID,$username_field 
                		FROM INTRANET_USER 
                		WHERE RecordType = 2
				                AND ClassName = '$targetClass' 
                		ORDER BY ClassName, ClassNumber, TRIM(EnglishName)";
                $row = $li->returnArray($sql,2);
			   
			    $parentID2DArr = array();
			    $parentIDArr = array();
			    $memberIDArr = array();
				//get all parent whose children is in that class
				for ($i=0; $i<sizeof($row); $i++)
				{
				    $sql = "SELECT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID = '".IntegerSafe($row[$i][0])."'";
					$parentID2DArr[] = $li->returnVector($sql);
				}				
				//change 2D array -> string list for SQL later		
				foreach ($parentID2DArr as $list) {
				    $parentIDArr = array_merge($parentIDArr,$list);
				    $parentList = implode(",", $parentIDArr);
				}
				
				//get all course members
/*
				$sql = "SELECT user_id 
                 		FROM $eclass_db.user_course 
                 		WHERE course_id = $course_id ";
				$memberIDArr = $li->returnVector($sql);
				if (sizeof($memberIDArr))
				{
					$memberList = implode(",", $memberIDArr);
					$member_conds = " AND UserID NOT IN ($memberList) ";
				}
*/
				$sql = "SELECT user_email 
                 		FROM $eclass_db.user_course 
                 		WHERE course_id = $course_id ";
				$memberIDArr = $li->returnVector($sql);
				if (sizeof($memberIDArr))
				{
					$memberList = implode("','", $memberIDArr);
					$member_conds = " AND UserEmail NOT IN ('$memberList') ";
				}
				
								
				//get all parents whose children is in the class and the parent himself has not joined the club yet
				$name_field = getNameFieldByLang("");
				$sql = "SELECT UserID, $name_field 
						FROM INTRANET_USER 
						WHERE UserID IN ($parentList) $member_conds ";
				
				$row = $li->returnArray($sql,2);
		   }
		   else
		   {
				//get all students in the class except those have joined the club already
				$row = $li->returnUserForTypeExcludeCourse($selectedUserType,$course_id, "AND ClassName = '$targetClass'");
		   }
		   
		   $x4  = "<select name=ChooseUserID[] size=5 multiple>\n";
		   for($i=0; $i<sizeof($row); $i++)
		   		$x4 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
		   $x4 .= "<option>";
		   for($i = 0; $i < 40; $i++)
		   		$x4 .= "&nbsp;";
		   $x4 .= "</option>\n";
		   $x4 .= "</select>\n";
	   }
   }
   else
   {
	   $row = $li->returnUserForTypeExcludeGroup($selectedUserType,$GroupID);
	   $row2 = $li->returnUserForTypeExcludeCourse($selectedUserType,$course_id);
	   for($i=0; $i<count($row2); $i++)
	   	$row2[$i] = $row2[$i][0];

	   $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
	   for($i=0; $i<sizeof($row); $i++)
	   	if(in_array($row[$i][0], $row2))
	   		$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	   $x3 .= "<option>";
	   for($i = 0; $i < 40; $i++)
	   		$x3 .= "&nbsp;";
	   $x3 .= "</option>\n";
	   $x3 .= "</select>\n";
   }
   
   
}
else if(isset($ChooseGroupID)) 
{
     $row = $li->returnGroupUsersExcludeGroup($ChooseGroupID, $GroupID, $course_id);
     $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     {
        # Student not selectable and Roomtype == 4 (iPortfolio)
        $lu = new libuser($row[$i][0]);
        if(($lu->RecordType == 2) && ($RoomType == 4)) continue;
     
        $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     }
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
?>


<script language="JavaScript1.2">
function import_update(){
        var obj = document.form1;
	  var total_selected = countOption(document.getElementsByName("ChooseUserID[]")[0]);
        if (total_selected<=0)
        {
        	alert("<?=$i_eClass_no_user_selected?>");
        	return;
	  }
<?php
# check if the quota enough for selected amount
	if ($lo->max_user!="")
	{
		$js_x = "var quota_left = '".$lo->ticketUser()."';\n";
		$js_x .= "if (total_selected>parseInt(quota_left))\n";
		$js_x .= "{ alert(\"$i_eClass_quota_exceeded\"); return; }\n";
		echo $js_x;
	}
?>
        checkOption(document.getElementsByName("ChooseUserID[]")[0]);
        obj.action = "import_update.php";
        obj.submit();
}

function SelectAll(obj)
{
        var obj = document.getElementsByName("ChooseUserID[]")[0];
        for (i=0; i<obj.length; i++)
        {
          obj.options[i].selected = true;
        }
}

function expandGroup()
{
        var obj = document.form1;
        checkOption(document.getElementsByName("ChooseGroupID[]")[0]);
        obj.submit();
}
</script>
<?php
if($RoomType==4){

displayNavTitle($i_admintitle_eclass, '', $lo->course_code . $lo->course_name, '', $button_import, '') ?>
<table width=400 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>&nbsp;</td></tr>
<tr><td>
iPortfolio Teacher Import is no longer in use.  To perform your settings, go to <strong>iPortfolio > Settings > Group > Teacher</strong> of the front-end.<br /><br />
學習檔案匯入教師功能已經停用。要進行有關設定，請前往前台 <strong>學習檔案  > 設定 > 小組 > 教師</strong>。
<hr size=1 class="hr_sub_separator"></td></tr>
			<tr>
				<td align=center height="40" style="vertical-align:bottom">
					<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
				</td>
			</tr>
</table>
<?php
}else if($RoomType==7){

displayNavTitle($i_admintitle_eclass, '', $lo->course_code . $lo->course_name, '', $button_import, '') ?>
<table width=400 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>&nbsp;</td></tr>
<tr><td>
iTextbook Teacher Import is no longer in use.  To perform your settings, go to <strong>eLearning > iTextbook > [Choose your iTextbook] > Assign Teacher-in-charge</strong> of the front-end.<br /><br />
電子課本匯入教師功能已經停用。要進行有關設定，請前往前台 <strong>學與教管理工具  > 電子課本 > [選擇你的電子課本] > Assign Teacher-in-charge</strong>。
<hr size=1 class="hr_sub_separator"></td></tr>
			<tr>
				<td align=center height="40" style="vertical-align:bottom">
					<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
				</td>
			</tr>
</table>
<?php
}else{

?>
<form name="form1" action="import.php" method="post">
	<?= displayNavTitle($i_admintitle_eclass, '', $lo->course_code . $lo->course_name, '', $button_import, '') ?>
	
	<p style="padding-left:20px">
		<table width="422" border="0" cellpadding="0" cellspacing="0">
			<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
			<tr>
				<td style="background-image: url(../../../images/admin/pop_bg.gif);" >
					<table width="422" border="0" cellpadding="10" cellspacing="0">
						<tr><td><?php echo $i_eClass_Identity; ?>: <?php echo $x0; ?></td></tr>
					</table>
					<table width="422" border="0" cellpadding="0" cellspacing="0">
						<tr><td><img src="../../../images/admin/pop_bar.gif" width="422" height="16" border="0"></td></tr>
					</table>
					<table width="422" border="0" cellpadding="10" cellspacing="0">
						<tr>
							<td>
								<p>
								<?php echo $i_frontpage_campusmail_select_category; ?>:
								<br>
								<?php echo $x1; ?>
								<?php if($CatID!=0 && $CatID > 0) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_group; ?>:
									<br>
									<?php echo $x2; ?>
									<a href="javascript:expandGroup()"><img src="/images/admin/button/s_btn_expand_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
								<?php if($CatID == -3){ ?>
									<p>
									<?php echo $i_UserParentLink_SelectClass; ?>:
									<br>
									<?php echo $x3; ?>
									<?php if (isset($targetClass) && $targetClass!="") { ?>
										<p>
										<?php echo $i_frontpage_campusmail_select_user; ?>:
										<br>
										<?php echo $x4; ?>
										<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
										<a href="javascript:SelectAll()"><img src="/images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
										
								<?php
									} 
								}
								else if(isset($ChooseGroupID)) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_user; ?>:
									<br>
									<?php echo $x3; ?>
									<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
									<a href="javascript:SelectAll()"><img src="/images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
							</td>
						</tr>
					</table>
	
				</td>
			</tr>
			<tr><td><img src="../../../images/admin/pop_bottom.gif" width="422" height="18" border="0"></td></tr>
			<tr>
				<td align=center height="40" style="vertical-align:bottom">
					<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
				</td>
			</tr>
		</table>
<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type=hidden name=RoomType value=<?=$RoomType?>>
</form>

<?php
}
intranet_closedb();
include_once("../../../templates/filefooter.php");
?>
