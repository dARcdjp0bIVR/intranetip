<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libeclass.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("$eclass_filepath/system/settings/lang/$lang");
eclass_opendb();

# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);
$lg = new libgroups(classNamingDB($course_id));


include("../../../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj){
	if(!check_text(obj.group_name, "<?php echo $namelist_groups_alert1; ?>")) return false;
	checkOption(obj.elements["target[]"]);
	for(i=0; i<obj.elements["target[]"].length; i++)
	obj.elements["target[]"].options[i].selected = true;
}

function checkOption(obj){
	for(i=0; i<obj.length; i++){
		if(obj.options[i].value== '')
		obj.options[i] = null;
	}
}

function check(from,to){
	checkOption(from);
	checkOption(to);
	i = from.selectedIndex;
	while(i!=-1){
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
		from.options[i] = null;
		i = from.selectedIndex;
	}
}

function checkLeader(objname){
	var leader = "";
	var len = document.form1.elements.length;
	for (j=0; j<len; j++) {
		if (document.form1.elements[j].name==objname) {
			obj = document.form1.elements[j];
			for(i=0; i<obj.length; i++){
				if(obj.options[i].value!='' && obj.options[i].selected)
					leader += (leader!="") ? "|" + obj.options[i].value : obj.options[i].value;
			}
		}
	}
	obj.form.leader.value = leader;
}
</script>


<form name="form1" method="post" action="update.php"  onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../../index.php?RoomType=$RoomType", $lo->course_code ." ". $lo->course_name, "../index.php?course_id=$course_id", $i_eClass_eclass_group, "index.php?course_id=$course_id", $button_new, '') ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>


<table width=55% border=0 cellpadding=2 cellspacing=0 align="center">
<tr><td cospan="2">&nbsp;</td></tr>
<tr><td align=right nowrap><span class="title"><?php echo $namelist_groups_name; ?> :</span>&nbsp;</td><td><input type=text name=group_name size=25 maxlength=20></td></tr>
<tr><td align=right nowrap><span class="title"><?php echo $namelist_groups_desc; ?> :</span>&nbsp;</td><td><textarea name=group_desc cols=40 rows=5 wrap="virtual"></textarea></td></tr>
<!--<tr><td>&nbsp;</td><td nowrap><input type="checkbox" name="manage_right" value="1" checked="checked"/>&nbsp;<span class="title"><?php echo $namelist_groups_no_admin_right; ?></span></td></tr>-->
<tr>
<td align=right nowrap><span class="title"><?php echo $namelist_groups_members; ?> :</span>&nbsp;</td><td>

	<table>
	<tr>
	<td>
	<select name="target[]" size=7 multiple>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
	<td align=center><br>
	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='check(this.form.elements["source[]"],this.form.elements["target[]"]);return false;'></p>
	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='check(this.form.elements["target[]"],this.form.elements["source[]"]);return false;'></p>
	</td>
	<td>
	<select name="source[]" size=7 multiple>
	<?= ($RoomType==4?$lg->userListOutPortfolio($group_id):$lg->userListELP($group_id)) ?>
	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
	</select>
	</td>
	</tr><tr>
	<td valign="top">&nbsp;</td>
	<td>&nbsp;</td>
	<td valign="top"><span class="guide"><?=$namelist_groups_members_msg?></span></td>
	</tr>
	</table>

</td>
</tr>
<tr><td>&nbsp;</td><td nowrap><input type="checkbox" name="has_right" id="has_right" value="-1" checked="checked"/>&nbsp;<span class="title"><label for="has_right"><?php echo $namelist_groups_no_admin_right; ?></label></span></td></tr>
<?= $folder_max_size ?>
<tr><td cospan="2">&nbsp;</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>


<input type=hidden name=course_id value="<?= $course_id ?>">
</form>

<?php
eclass_closedb();
include("../../../../templates/adminfooter.php");
?>