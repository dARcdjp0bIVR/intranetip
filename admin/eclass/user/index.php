<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libeclass40.php");
include("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include("../../../lang/lang.$intranet_session_language.php");
include_once("$eclass40_filepath/system/settings/lang/$lang");

eclass_opendb();

# eClass
$lo = new libeclass($course_id);
$RoomType = $lo->getEClassRoomType($course_id);
$lo->order = $order;
$lo->field = $field;

/*
$eclass_quota  = $lo->status();

$eclass_quota .= "- ".$i_eClassNumUsers.": ".$lo->max_user."<br>\n";
$eclass_quota .= "- ".$i_eClassMaxStorage.": ".$lo->max_storage."<br>\n";

$eclass_quota .= "<hr size=1 width='80%' color='#2878A9'>\n";
$eclass_quota .= "<table width='100%' border=0 cellspacing=0 cellpadding=2>";
$eclass_quota .= "<tr><td width='20%'>&nbsp;</td><td width='30%' nowrap><font color='#FFE2FF'>$i_eClassNumUsers:</font></td><td width='50%'><font color='#FFF0FA'>".$lo->max_user."</font></td></tr>\n";
$eclass_quota .= "<tr><td>&nbsp;</td><td nowrap><font color='#FFE2FF'>$i_eClassMaxStorage:</font></td><td><font color='#FFF0FA'>".$lo->max_storage."</font></td></tr>\n";
$eclass_quota .= "</table>\n";
*/

$eclass_3orup = ($eclass_version >= 3.0);  //is_dir("$eclass_filepath/system/");

$files_dir = ($eclass_3orup? "files":"file");

$dir_prefix = $lo->db_prefix;

$dir_list = array(
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/notes",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/reference",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/glossary",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/assignment",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/question",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/public",
"$eclass_filepath/$files_dir/$dir_prefix"."c$course_id/group");

$lf = new libfilesystem();

if ($eclass_version < 3.0)
{
for($i=0; $i<sizeof($dir_list); $i++){
     $storage[$i] = $lf->folder_size($dir_list[$i]);
}
$c_size = 0;
$c_files = 0;
$c_dirs = 0;

for($i=0; $i<sizeof($storage); $i++){
     $size = $storage[$i];
     $c_size += $size[0];
     $c_files += $size[1];
     $c_dirs += $size[2];

}
$c_size = number_format(($c_size/1048576),2, ".", "");
}
// eclass 3.0
if ($eclass_version >= 3.0) {
        $li = new libdb();
        $li->db = $eclass_prefix."c".$course_id;
        $sql = "SELECT SUM(size) FROM eclass_file WHERE VirPath is NULL AND Category<>9";
        $row = $li->returnArray($sql, 1);
        $c_size = ($row[0][0]!="") ? number_format(($row[0][0]/1024),2, ".", "") : 0;
}

if ($lo->max_user!="")
{
	$user_quota_left = ($lo->ticketUser()>0) ? $lo->ticketUser() : "<font color='red'>".$lo->ticketUser()."</font>";
	$size_msg .= "$i_eClassUserQuotaMsg1 ".$lo->max_user ." $i_eClassUserQuotaMsg2 ".$user_quota_left." $i_eClassUserQuotaMsg3 $i_eClassUserQuotaMsg4 ".$lo->no_users.".<br>";
}
if ($lo->max_storage!="")
{
	$left = $lo->max_storage - $c_size;
	if ($left < 0) $left = "<font color='red'>0.00</font>";;
	$size_msg .= "$i_eClassStorageDisplay1 ".$lo->max_storage ."$i_eClassStorageDisplay2 $left $i_eClassStorageDisplay3 ";
}
$size_msg .= "$i_eClassStorageDisplay4 $c_size $i_eClassStorageDisplay5";

$toolbar  = "<a class='iconLink' href=javascript:newWindow('import.php?RoomType=".$RoomType."&course_id=".$lo->course_id."',2)>".importIcon()."$button_import</a>\n";
$toolbar .= "<a class=iconLink href=\"javascript:confirmSendPassword(document.form1,'email_all.php')\"><img src=$image_path/admin/icon_send_password.gif border=0 hspace=1 vspace=0 align=absmiddle>$button_emailpassword</a>\n";
// reading room
if ($RoomType==1 || $RoomType==4)
{
	$toolbar .= "<a class='iconLink' href=javascript:checkGet(document.form1,'teacher_rights.php?course_id=".$lo->course_id."')><img src=$image_path/admin/icon_setting.gif border=0 hspace=1 vspace=0 align=absmiddle>$i_eClass_set_teacher_rights</a>\n";
}
// eClass Learning Park
if ($RoomType==2)
{
	//$IsLingman = $lo->checkLingmanCourse($course_id);
//	$toolbar .= (!$IsLingman) ? "<a class='iconLink' href=javascript:checkGet(document.form1,'group/index.php?course_id=".$lo->course_id."')><img src=$image_path/admin/icon_setting.gif border=0 hspace=1 vspace=0 align=absmiddle>$i_eClass_eclass_group</a>\n" : "";
		$toolbar .= "<a class='iconLink' href=javascript:checkGet(document.form1,'group/index.php?course_id=".$lo->course_id."')><img src=$image_path/admin/icon_setting.gif border=0 hspace=1 vspace=0 align=absmiddle>$i_eClass_eclass_group</a>\n";
}
if ($RoomType==4){
	// this page is moved to front end, just a reminder
	$toolbar .= "<a class='iconLink' href=javascript:checkGet(document.form1,'group/notice4.php')><img src=$image_path/admin/icon_setting.gif border=0 hspace=1 vspace=0 align=absmiddle>$i_eClass_eclass_group</a>\n";
}
if ($RoomType==4)
{
	$toolbar .= "<a class='iconLink' href=\"../portfolio/access_right.php?course_id=".$lo->course_id."\"><img src=$image_path/admin/icon_setting.gif border=0 hspace=1 vspace=0 align=absmiddle>".$function_access_rights."</a>\n";
}
if ($RoomType==0)
{
	$functionbar = "<a href='file_assign.php?course_id=".$lo->course_id."'><img src='/images/admin/button/t_btn_transferfileown_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
}
if ($RoomType!=4 && $RoomType!=7){
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'user_id[]','delete.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";	
}



include("../../../templates/adminheader_eclass.php");
?>
<SCRIPT>
function confirmSendPassword(obj, url)
{
         if (confirm(globalAlertMsgSendPassword))
             checkGet(obj, url);
}
</SCRIPT>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_eclass, '', $i_eClass_Admin_MgmtCenter, "../index.php?RoomType=$RoomType", $lo->course_code ." ". $lo->course_name, '') ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<?=$size_msg?>
<br><br>
</td></tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $lo->displayFunctionbar($toolbar, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $lo->eClassUserDisplay(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<p><br></p>
<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type=hidden name=order>
<input type=hidden name=field>
</form>

<?php
eclass_closedb();
include("../../../templates/adminfooter.php");
?>
