<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include("../../../lang/lang.$intranet_session_language.php");
eclass_opendb();

$lo = new libeclass($course_id);
//$lo->eClassUserDel($user_id);
$uids = (is_array($user_id)) ? implode(",", $user_id) : $user_id;
$sql = "SELECT user_id, firstname, lastname, memberType, class_number FROM user_course WHERE user_id IN (".$uids.") AND course_id = ".$course_id;
$row = $lo->returnArray($sql,5);

$x .= "<table width=400 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>";
$x .= "<td class=tableTitle width=1>#</td>\n";
$x .= "<td class=tableTitle width=55% nowrap>$i_UserName</td>\n";
$x .= "<td class=tableTitle width=35% nowrap>$i_UserClassName ($i_UserClassNumber)</td>\n";
$x .= "<td class=tableTitle width=10% nowrap>$i_eClass_Identity</td>\n";
$x .= "</tr>\n";
for($i=0; $i<sizeof($row); $i++){
	$user_id = $row[$i][0];
	$firstname = $row[$i][1];
	$lastname = $row[$i][2];
	$member = $row[$i][3];
	$class_number = ($row[$i][4]=="") ? "&nbsp;" : $row[$i][4];
	$css = ($i%2==0) ? "" : "2";
	$x .= "<tr>";
	$x .= "<td class=tableContent$css>".($i+1)."</td>\n";
	$x .= "<td class=tableContent$css>$lastname $firstname</td>\n";
	$x .= "<td class=tableContent$css>$class_number</td>\n";
	$x .= "<td class=tableContent$css>$member <input type=hidden name='user_id[]' value='$user_id'>";
	if (strtoupper($member)=="T")
	{
		$x .= "<input type=hidden name='teacher_removed[]' value='$user_id'>";
		$teacher_delete .= ($teacher_delete!="") ? ",".$user_id : $user_id;
	}
	$x .= "</td>\n";
	$x .= "</tr>\n";
}
$x .= "</table>\n";

if ($teacher_delete!="")
{
	######################### GET EXISTING TEACHER LIST #########################
	$ec_db_name = $lo->db_prefix."c$course_id";
	$sql = "SELECT user_id, LTRIM(CONCAT(ifnull(lastname,''), ' ', ifnull(firstname,''))) as t_name, user_email ";
	$sql .= "FROM ".$ec_db_name.".usermaster ";
	$sql .= "WHERE memberType='T' AND (status IS NULL OR status='') AND user_id NOT IN ($teacher_delete)";
	$sql .= "ORDER BY t_name ";
	$row_e = $lo->returnArray($sql, 3);

	$sql = "SELECT UserID, EnglishName, ChineseName, UserEmail ";
	$sql .= "FROM INTRANET_USER ";
	$sql .= "WHERE RecordType = 1 ";
	$sql .= "ORDER BY EnglishName ";
	$li = new libdb();
	$row_i = $li->returnArray($sql, 4);

	$sql = "SELECT user_email ";
	$sql .= "FROM ".$ec_db_name.".usermaster ";
	$sql .= "WHERE user_id IN ($teacher_delete)";
	$row_d = $lo->returnArray($sql, 1);
	$exclus_email = array();
	for ($i=0; $i<sizeof($row_d); $i++)
	{
		$exclus_email[] = trim($row_d[$i][0]);
	}

	$teacher_exist_list = "<select name='teacher_benefit'>\n";
	$teacher_exist_list .= "<option value='0'>".$i_ec_file_no_transfer."</option>\n";
	$teacher_exist_list .= "<option value=''>____________________</option>";
	$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_exist_teacher." --&nbsp;</option>\n";
	$cours_teacher_size = sizeof($row_e);
	for ($i=0; $i<$cours_teacher_size; $i++)
	{
		$teacher_exist_list .= "<option value='".$row_e[$i][0]."'>".$row_e[$i][1]."</option>\n";
		$exclus_email[] = trim($row_e[$i][2]);
	}

	$teacher_exist_list .= "<option value=''>____________________</option>";
	$teacher_exist_list .= "<option value=''>&nbsp;-- ".$i_ec_file_not_exist_teacher." --&nbsp;</option>\n";
	for ($i=0; $i<sizeof($row_i); $i++)
	{
		if (!in_array($row_i[$i][3], $exclus_email))
		{
			$chiname = ($row_i[$i][2]==""? "": "(".$row_i[$i][2].")" );
			$teacher_exist_list .= "<option value='".$row_i[$i][0]."'>".$row_i[$i][1]." $chiname</option>\n";
		}
	}

	$teacher_exist_list .= "</select>\n";
	$y = "<br><br><br>";
	$y .= "<u>" . $i_ec_file_msg_transfer . "</u><br><br><blockquote>" . $teacher_exist_list ."</blockquote>";
}


include("../../../templates/adminheader_eclass.php");
?>

<script language="javascript">
function checkform(obj){
	if (typeof(obj.teacher_benefit)!="undefined")
	{
		if (obj.teacher_benefit.options[obj.teacher_benefit.selectedIndex].value=="")
		{
			alert("<?=$i_ec_file_warning2 ?>");
			return false;
		}
		if (confirm("<?=$i_ec_file_user_delete_confirm?>"))
		{			
			if (obj.teacher_benefit.selectedIndex><?=$cours_teacher_size?>+4)
			{
				obj.is_user_import.value = 1;
				if (confirm("<?=$i_ec_file_confirm2?>"))
				{
					return true;
				} else
				{
					return false;
				}
			} else
			{
				return true;
			}
		} else
		{
			return false;
		}
	} else
	{
		return true;
	}
}
</script>

<form name="form1" method="post" action="delete_update.php" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_eclass, "../index.php", $lo->course_code ." ". $lo->course_name, "index.php?course_id=$course_id", $button_remove." ".$i_admintitle_us_user, "") ?>
<?= displayTag("head_eclass_namelist_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>

<u><?= $i_ec_file_user_delete ?></u>
<br><br>
<?= $x . $y ?>

</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border="0">
<a href="index.php?course_id=<?=$course_id?>"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>

<input type=hidden name=course_id value=<?php echo $lo->course_id; ?>>
<input type='hidden' name='is_user_import' value='0'>
</form>


<?php
eclass_closedb();
include("../../../templates/adminfooter.php");
?>
