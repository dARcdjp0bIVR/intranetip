<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();
$li->file_write(htmlspecialchars(trim(stripslashes($website))), $intranet_root."/file/email_website.txt");
$school_data = "$schoolname\n$schoolorg";
$li->file_write(htmlspecialchars(trim(stripslashes($school_data))), $intranet_root."/file/school_data.txt");
$li->file_write(htmlspecialchars(trim(stripslashes($academic_yr))), $intranet_root."/file/academic_yr.txt");

$semester_data = "";
for ($i=0; $i<sizeof($semester); $i++)
{
     $tag = ($currentsem == $i)?1:0;
     $semester[$i] = htmlspecialchars(trim(stripslashes($semester[$i])));
     if ($semester[$i] != "")
     {
         $semester_data .= $semester[$i]."::$tag\n";
     }
     else
     {
         break;
     }

}
$li->file_write(htmlspecialchars(trim(stripslashes($semester_data))), $intranet_root."/file/semester.txt");
$li->file_write(htmlspecialchars(trim(stripslashes($DayType))), $intranet_root."/file/schooldaytype.txt");
header("Location: index.php?msg=2");
?>