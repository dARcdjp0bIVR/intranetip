<?php
# using: 

###################################################### Change Log ######################################################
# Date		: 20160825 (Carlos) - Get the $website var from $_POST due to $website is a global config var that conflict with the post value.
# Date		: 20140303 (Yuen) - Rolocated Account Lockout Policy to front-end
# Date		: 20120914 (Yuen) - Introduced Account Lockout Policy
#
###################################################### Change Log ######################################################


include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();
$li->file_write(htmlspecialchars(trim(stripslashes($_POST['website']))), $intranet_root."/file/email_website.txt");
$school_data = "$schoolname\n$schoolorg\n$schoolphone\n".trim($schooladdress);
$li->file_write(htmlspecialchars(trim(stripslashes($school_data))), $intranet_root."/file/school_data.txt");
$li->file_write(htmlspecialchars(trim(stripslashes($academic_yr))), $intranet_root."/file/academic_yr.txt");

## Update Semester Mode ###
if($SemesterMode != ""){
	$li->file_write(htmlspecialchars(trim(stripslashes($SemesterMode))), $intranet_root."/file/semester_mode.txt");
}
$semester_data = "";
for ($i=0; $i<sizeof($semester); $i++)
{
     $tag = ($currentsem == $i)?1:0;
     $semester[$i] = htmlspecialchars(trim(stripslashes($semester[$i])));
     if ($semester[$i] != "")
     {
         $semester_data .= $semester[$i]."::$tag::$start_date[$i]::$end_date[$i]\n";
     }
     else
     {
         break;
     }
}
$li->file_write(htmlspecialchars(trim(stripslashes($semester_data))), $intranet_root."/file/semester.txt");
$li->file_write(htmlspecialchars(trim(stripslashes($DayType))), $intranet_root."/file/schooldaytype.txt");


// update language
switch ($LangID){
        case 0: $LangID = 0; break;
        case 1: $LangID = 1; break;
        case 2: $LangID = 2; break;
        default: $LangID = 0; break;
}
$li->file_write(htmlspecialchars(trim(stripslashes($LangID))), $intranet_root."/file/language.txt");


// update badge
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$path = "$intranet_root/file/$imgfile";

if ($use_badge == 1)
{
    if (is_uploaded_file($badge_image))
    {
        if (is_file($path))
        {
            $li->file_remove($path);
        }
        $li->lfs_copy($badge_image, "$intranet_root/file/$badge_image_name");
        $li->file_write(htmlspecialchars(trim(stripslashes($badge_image_name))), $intranet_root."/file/schoolbadge.txt");
    }
}
else
{
    if (is_file($path))
    {
        $li->file_remove($path);
    }
    $li->file_remove("$intranet_root/file/schoolbadge.txt");
}

/*
# login attempt and lock
if (trim($login_attempt_limit)=="")
{
	$login_attempt_limit = 10;
}
if (trim($login_lock_duration)=="")
{
	$login_lock_duration = 10;
}
$content_login = "$login_attempt_limit\n$login_lock_duration";
$li = new libfilesystem();
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($content_login))), $intranet_root."/file/login_attempt_lock.txt");
*/


# Popup date
$content = "$helpstart\n$helpend";
$li = new libfilesystem();
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($content))), $intranet_root."/file/popupdate.txt");


// update misc
if(!file_exists($intranet_root."/file/user_info_settings.txt")){
  $email_allowed = ($email_allowed == 1? 1: "");
  $title_disabled = ($title_disabled == 1? 1: "");
  $home_tel_allowed = ($home_tel_allowed == 1? 1: "");
  $li->file_write($email_allowed."\n".$title_disabled."\n".$home_tel_allowed, $intranet_root."/file/basic_misc.txt");
}



header("Location: index.php?msg=2");
?>
