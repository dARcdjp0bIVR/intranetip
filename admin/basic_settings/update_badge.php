<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$path = "$intranet_root/file/$imgfile";


if ($use_badge == 1)
{
    if (is_uploaded_file($badge_image))
    {
        if (is_file($path))
        {
            $li->file_remove($path);
        }
        $li->lfs_copy($badge_image, "$intranet_root/file/$badge_image_name");
        $li->file_write(htmlspecialchars(trim(stripslashes($badge_image_name))), $intranet_root."/file/schoolbadge.txt");
    }
}
else
{
    if (is_file($path))
    {
        $li->file_remove($path);
    }
    $li->file_remove("$intranet_root/file/schoolbadge.txt");
}


header("Location: index.php?msg=2");
?>