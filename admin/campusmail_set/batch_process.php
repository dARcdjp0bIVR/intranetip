<?php
// editing by 
/********************************************** Change log ************************************************
 * 2011-10-19 (Carlos): added alumni quota
 **********************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
intranet_opendb();

if($plugin['imail_gamma']===true){
	include_once("../../includes/imap_gamma.php");
	$IMap = new imap_gamma(true);
	if($type==1){
		$quota = $IMap->default_quota[$target-1];
	}else{
		$quota = 10;
	}
	if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
}else{
	$file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
	if ($file_content == "")
	{
	    $userquota = array(10,10,10,10);
	}
	else
	{
	    $userquota = explode("\n", $file_content);
	}
	if ($type==1)
	{
	    $quota = $userquota[$target-1];
	}
	else
	{
	    $quota = 10;
	}
}
#$used = $lwebmail->getUsedQuota($loginName);
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (obj.batchoption.value==1)
         {
             if (!check_int(obj.quota,'<?=$i_LinuxAccount_Alert_QuotaMissing?>')) return false;
         }
         return true;
}
</SCRIPT>


<form name="form1" action="batch_process_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_LinuxAccount_BatchProcess,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=400 border=0>
<tr><td>
<input type=radio name=batchoption value=1 CHECKED> <?=$i_CampusMail_New_BatchOption_OpenAccount?><input type=text size=5 name=quota value='<?=$quota?>'><br>
<input type=radio name=batchoption value=2 > <?=$plugin['imail_gamma']==true?$Lang['Gamma']['NotAllowListedUsersSendReceiveInternetMails']:$i_CampusMail_New_BatchOption_UnlinkAccount?><br>
<input type=radio name=batchoption value=3 > <?=$i_CampusMail_New_BatchOption_RemoveAccount?><br>
<?php 
if($plugin['imail_gamma']===true)
{
?>
<input type=radio name=batchoption value=4 > <?=$Lang['Gamma']['ActivateListedUsersMailAccount']?><br>
<input type=radio name=batchoption value=5 > <?=$Lang['Gamma']['SuspendListedUsersMailAccount']?><br>
<?php
}
?>
</td></tr>
<tr><td><br>
<font color=red><?=$i_LinuxAccount_GroupSetNote?></font>
</td></tr>
</table>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
<input type=hidden name=type value="<?=$type?>">
<input type=hidden name=target value="<?=$target?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
</form>

<?php
include_once("../../templates/adminfooter.php");
?>