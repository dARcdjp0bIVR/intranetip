<?php
// editing by 
/********************************************* Changes ***************************************************
 * 2020-07-29 (Henry): general removal for iMail
 * 2017-06-14 (Carlos): added user status checkboxes for filtering different status users to search for.
 * 2012-08-29 (Carlos): rename UserType (conflict with session variable) to TargetUserType 
 * 2012-07-17 (Carlos): added User type selection
 **********************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");

// $i_Campusquota_basic_identity
$filecontent = trim(get_file_content("$intranet_root/file/cm_removal.txt"));
$remove_allowed = ($filecontent == 1);
if (!$remove_allowed && $plugin['imail_gamma']===true)
{
     header("Location: index.php");
     exit();
}
if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

// Identity type
$usertype_select = '<SELECT name="TargetUserType">'."\n";
$usertype_select .= '<OPTION value="1" '.($TargetUserType=='1'?'selected':'').'>'.$i_identity_teachstaff.'</OPTION>'."\n";
$usertype_select .= '<OPTION value="2" '.($TargetUserType=='2'?'selected':'').'>'.$i_identity_student.'</OPTION>'."\n";
$usertype_select .= '<OPTION value="3" '.($TargetUserType=='3'?'selected':'').'>'.$i_identity_parent.'</OPTION>'."\n";
if ($special_feature['alumni']){
    $usertype_select .= '<OPTION value="4" '.($TargetUserType=='4'?'selected':'').'>'.$i_identity_alumni.'</OPTION>'."\n";
}
$usertype_select .= '</SELECT>';

include_once("../../templates/adminheader_setting.php");
?>

<form name="form1" action="remove_update.php" method="post" >
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$i_campusmail_forceRemoval.($plugin['imail_gamma']?' ('.$Lang['Gamma']['iMailArchive'].')':''),'') ?>
<?= displayTag("$img_tag", $msg, $msg != ''? ($msg == 1? $i_con_msg_delete : $i_con_msg_delete_failed):'') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<?=$i_LinuxAccount_SelectUserType.': '.$usertype_select?>
<br />
<?=$i_general_status.': '?>
<input type="checkbox" name="TargetUserStatus[]" id="TargetUserStatus[1]" value="1" <?=!isset($TargetUserStatus) || (is_array($TargetUserStatus) && in_array(1,$TargetUserStatus))?' checked ':''?> /><label for="TargetUserStatus[1]"><?=$Lang['Status']['Active']?></label>
<input type="checkbox" name="TargetUserStatus[]" id="TargetUserStatus[0]" value="0" <?=!isset($TargetUserStatus) || (is_array($TargetUserStatus) && in_array(0,$TargetUserStatus))?' checked ':''?>  /><label for="TargetUserStatus[0]"><?=$Lang['Status']['Suspended']?></label>
<input type="checkbox" name="TargetUserStatus[]" id="TargetUserStatus[3]" value="3" <?=!isset($TargetUserStatus) || (is_array($TargetUserStatus) && in_array(3,$TargetUserStatus))?' checked ':''?>  /><label for="TargetUserStatus[3]"><?=$Lang['Status']['Left']?></label>
<input type="checkbox" name="TargetUserStatus[]" id="TargetUserStatus[-1]" value="-1" <?=!isset($TargetUserStatus) || (is_array($TargetUserStatus) && in_array(-1,$TargetUserStatus))?' checked ':''?>  /><label for="TargetUserStatus[-1]"><?=$Lang['Status']['Archived']?></label>
<br /><br />
<?=$i_campusmail_forceRemoval_instruction1?>:
<br><?=$i_campusmail_forceRemoval_Start?>:
<input type=text name=start value='<?=((isset($start) && $start!="")?$start:date('Y-m-d'))?>' size=10> <span class="extraInfo">(yyyy-mm-dd)</span>
<br><?=$i_campusmail_forceRemoval_End?>:
<input type=text name=end value='<?=((isset($end) && $end!="")?$end:date('Y-m-d'))?>' size=10> <span class="extraInfo">(yyyy-mm-dd)</span>

</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>