<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

?>

<script language="javascript">
function validateEmail(obj,msg){
        //var re = /^[\w_.-]+@[\w-]+(\.\w+)+$/;
        var re1 = /^@.+\..{2,3}$/;
        //var re = /^[\w_\.\-]+@[\w_\-]+\.[\w]{2,3}$/;
        var re2 = /^([\w_\.\-])+\@(([\w_\.\-])+\.)+([\w]{2,4})+$/;	
        if (re1.test(obj.value) || re2.test(obj.value)) {
                return true;
        }else{
                alert(msg);
                obj.focus();
                return false;
        }
}
function checkform(obj){
  if(!check_text(obj.target, "<?=$i_alert_pleasefillin?><?=$i_spam['EmailAddress']?>"))
    return false;

  if(!validateEmail(obj.target, "<?=$i_invalid_email?>"))
    return false;

  return true;
}
</script>

<form name="form1" action="blacklist_new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, '../',$i_spam['Setting'],'index.php',$i_spam['BlackList'],'blacklist.php',$button_new,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><?=$i_spam['EmailAddress']?></td><td><input type="text" name="target" size="40"></td></tr>
</table>
</blockquote>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0' />
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>


<?
include_once("../../../templates/adminfooter.php");
?>
