<?php
// Editing by 
/*
 * 2014-09-11 (Carlos): Revise the check options logic
 * 2014-08-26 (Carlos): Cater iMail plus and Webmail api calls
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libwebmail.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

if($plugin['imail_gamma'])
{
	include("../../../includes/imap_gamma.php");
	$IMap = new imap_gamma(1);
}

$lwebmail = new libwebmail();

//$bypass_spam_check = $lwebmail->readSpamCheck();
//$bypass_virus_check = $lwebmail->readVirusCheck();

if($plugin['imail_gamma']){
	$bypass_spam_check = $IMap->readSpamCheck();
	$bypass_virus_check = $IMap->readVirusCheck();
}else{
	$bypass_spam_check = $lwebmail->readSpamCheck();
	$bypass_virus_check = $lwebmail->readVirusCheck();
}

$mail_server_quota_control = $special_feature['imail'] && $lwebmail->has_webmail && ($lwebmail->type==3);
$has_imail = $special_feature['imail'];

?>

<script language="javascript">

function check_int(f,d,msg){
	
        if(isNaN(parseFloat(f.value))){
                alert(msg);
                f.value=d;
                f.focus();
                return false;
        }else{
                f.value=parseFloat(f.value);
                return true;
        }
}

function displayCustomLevel(val) {
	var obj = document.form1.spam_control_score;
	
	if(val == 0) {
		obj.disabled = true;
	}
	if(val == 1) {
		obj.disabled = false;
	}
}

function checkCustomSpamLevel() {
	var obj = document.form1.spam_control_score;
	
	<? if(($custom_spam_control == "Y")||($custom_spam_control == "")) { ?>
		if(check_text(obj, "<?=$i_spam['js_Alert_SpamControlLevelEmpty'];?>")) {
			if(check_int(obj,0,"<?=$i_spam['js_Alert_SpamControlLevelNotVaild'];?>")) {
				if(obj.value >= 0 && obj.value <= 6){
					return true;
				}else{
					alert("<?=$i_spam['js_Alert_SpamControlLevelNotVaild'];?>");
					return false;
				}
			} else {
				return false;
			}
		}
	<? } else { ?>
		return true;
	<? } ?>
}

function checkForm() {
	var obj = document.form1;
	if(obj.flag.value == 1){
		
		for(i=0; i<obj.spam_control.length; i++) {
			if(obj.spam_control[0].checked) {
				for(j=0; j<obj.custom_control_level.length; j++) {
					if(obj.custom_control_level[1].checked) {
						if(checkCustomSpamLevel())
						{
							obj.action = "bypass_update.php";
							return true;
						} else {
							obj.action = "";
							return false;
						}
					} else {
						obj.action = "bypass_update.php";
						return true;
					}
				}
			} else {
				obj.action = "bypass_update.php";
				return true;
			}
		}
	} else {
		obj.action = "";
		return false;
	}
}
	
</script>

<form name="form1" action="" method="post" onSubmit="return checkForm();">
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, '../',$i_spam['Setting'],'index.php',$i_spam['BypassSetting'],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent>
<br>
<blockquote>

<table width=500 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr>
	<td style="vertical-align:middle" class=tableTitle_new><?=$i_spam['SpamControl']?></td>
	
	<?	# Check Spam Control 
		if(isset($spam_control)){
			// use form value
			$spam_activated_checked = $spam_control == "Y" ? " CHECKED": "";
			$spam_bypass_checked = $spam_control != "Y" ? " CHECKED" : "";
			$spam_activated = $spam_control == "Y" ? true : false;
		}else{
			// use osapi value
			$spam_activated_checked = $bypass_spam_check == "N" ? " CHECKED" : "";
			$spam_bypass_checked = $bypass_spam_check != "N" ? " CHECKED": "";
			$spam_activated = $bypass_spam_check == "N" ? true : false;
		}
	 	/*
		if($bypass_spam_check == "N" || $bypass_spam_check == "") {
			
			if ($spam_control == "Y"){
				$spam_activated_checked = " CHECKED";
				$spam_bypass_checked = "";
				$spam_activated = true;
			} else
			{
				$spam_activated_checked = "";
				$spam_bypass_checked = " CHECKED";
				$spam_activated = false;
			}
			
		} else {
			
			if($spam_control == "N") {
				$spam_activated_checked = "";
				$spam_bypass_checked = " CHECKED";
				$spam_activated = false;
			} else
			{
				$spam_activated_checked = "";
				$spam_bypass_checked = " CHECKED";
				$spam_activated = false;
			}
			
		}
		*/
	?>
	<td align=center>
	<!--
		<input type=radio name="spam_control" id="spam_activated" value="Y" <?=($spam_status=='Y'?"CHECKED":"")?> onClick="javascript: spamLevelControl(1);" /><LABEL for="spam_activated"><?=$i_spam['Option_Activated']?></LABEL> &nbsp;
		<input type=radio name="spam_control" id="spam_bypass" value="N" <?=($spam_status=='N'?"CHECKED":"")?> onClick="document.form1.flag.value = 1; document.form1.submit();" /><LABEL for="spam_bypass"><?=$i_spam['Option_Bypass']?></LABEL>
	-->
		<input type=radio name="spam_control" id="spam_activated" value="Y" <?=$spam_activated_checked;?> onClick="document.form1.flag.value = 0; document.form1.submit();" /><LABEL for="spam_activated"><?=$i_spam['Option_Activated']?></LABEL> &nbsp;
		<input type=radio name="spam_control" id="spam_bypass" value="N" <?=$spam_bypass_checked;?> onClick="document.form1.flag.value = 0; document.form1.submit();" /><LABEL for="spam_bypass"><?=$i_spam['Option_Bypass']?></LABEL>
	</td>
</tr>


<? if($spam_activated) {
	
	if(isset($custom_control_level)){
		// use form value
		if($custom_control_level == 1){
			$custom_spam_control_level = $spam_control_score;
			$custom_checked = " CHECKED";
			$default_checked = "";
			$control_level_box = "";
		}else{
			$custom_spam_control_level = "";
			$custom_checked = "";
			$default_checked = " CHECKED";
			$control_level_box = " DISABLED";
		}
	}else{
		// use osapi value
		$custom_spam_control = $plugin['imail_gamma']? $IMap->retriveSpamControlSetting() :  $lwebmail->retriveSpamControlSetting(); 
		if($custom_spam_control == 1) {
			$custom_spam_control_level = $plugin['imail_gamma']? $IMap->retriveSpamControlLevel() :  $lwebmail->retriveSpamControlLevel();
			$custom_checked = " CHECKED";
			$default_checked = "";
			$control_level_box = "";
		}else{
			$custom_spam_control_level = "";
			$custom_checked = "";
			$default_checked = " CHECKED";
			$control_level_box = " DISABLED";
		}
	}
	/*
	$custom_spam_control = $plugin['imail_gamma']? $IMap->retriveSpamControlSetting() :  $lwebmail->retriveSpamControlSetting(); 
			
	if($custom_spam_control == 1) {
		
		$custom_spam_control_level = $plugin['imail_gamma']? $IMap->retriveSpamControlLevel() :  $lwebmail->retriveSpamControlLevel();
		$custom_checked = " CHECKED";
		$default_checked = "";
		$control_level_box = "";
	}else{
		
		$custom_spam_control_level = "";
		$custom_checked = "";
		$default_checked = " CHECKED";
		$control_level_box = " DISABLED";
	}
	*/
?>
<tr>
	<td style="vertical-align:middle" class=tableTitle_new><?=$i_spam['SpamControlLevel'];?></td>
	<td align=center>
	<!-- custom_control_level: 0 - system default setting, 1 - custom setting -->
	<input type="radio" name="custom_control_level" id="default_level" value="0" onClick="displayCustomLevel(0);" <?=$default_checked;?> \><label for="default_level"><?=$i_spam['SpamControlLevel_DefaultLevel'];?></label>&nbsp;
	<input type="radio" name="custom_control_level" id="customs_level" value="1" onClick="displayCustomLevel(1)" <?=$custom_checked;?> \>&nbsp;<label for="customs_level"><span class=\"tabletextrequire\">*</spam><?=$i_spam['SpamControlLevel_CustomLevel'];?></label>
	<input type="text" name="spam_control_score" value="<?=$custom_spam_control_level;?>" size="3" maxlength="3" <?=$control_level_box;?> \>
	</td>
</tr>
<? } ?>

<?	# Check Virus
	if(isset($virus_scan)){
		// use form value
		$virus_activated_checked = $virus_scan == "Y" ? " CHECKED" : "";
		$virus_bypass_checked = $virus_scan != "Y" ? " CHECKED" : "";
	}else{
		// use osapi value
		$virus_activated_checked = $bypass_virus_check == "N" ? " CHECKED" : "";
		$virus_bypass_checked = $bypass_virus_check != "N" ? " CHECKED" : "";
	}
	/*
	if($bypass_virus_check == "N") {
		
		if ($virus_scan == "Y"){
			$virus_activated_checked = " CHECKED";
			$virus_bypass_checked = "";
		} else {
			$virus_activated_checked = "";
			$virus_bypass_checked = " CHECKED";
		}
		
	} else {
		
		if($virus_scan == "N") {
			$virus_activated_checked = "";
			$virus_bypass_checked = " CHECKED";
		} else {
			$virus_activated_checked = " CHECKED";
			$virus_bypass_checked = "";
		}
		
	}
	*/
?>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_spam['VirusScan']?></td><td align=center>
<!--
<input type=radio name="virus_scan" id="virus_activated" value="Y" <?=($virus_status=='Y'?"CHECKED":"")?> /><LABEL for="virus_activated"><?=$i_spam['Option_Activated']?></LABEL> &nbsp;
<input type=radio name="virus_scan" id="virus_bypass" value="N" <?=($virus_status=='N'?"CHECKED":"")?> /><LABEL for="virus_bypass"><?=$i_spam['Option_Bypass']?></LABEL>
-->
<input type=radio name="virus_scan" id="virus_activated" value="Y" <?=$virus_activated_checked;?> /><LABEL for="virus_activated"><?=$i_spam['Option_Activated']?></LABEL> &nbsp;
<input type=radio name="virus_scan" id="virus_bypass" value="N" <?=$virus_bypass_checked;?> /><LABEL for="virus_bypass"><?=$i_spam['Option_Bypass']?></LABEL>
</td></tr>
</table>

<br>

<? if($spam_activated) { ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center> 
<tr><td colspan="2"><span class="tabletextrequire"><?=$i_spam['CustoSpamControlLevel_Description'];?></spam></tr>
</table>
<? } ?>

</blockquote>
</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0' onClick="document.form1.flag.value = 1;" />
 <?= btnReset() ?>
</td>
</tr>
</table>

<input type = "hidden" name="flag" value="0">
</form>


<?
include_once("../../../templates/adminfooter.php");
?>
