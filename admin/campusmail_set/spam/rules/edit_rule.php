<?php
// editing by 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$li = new libdb();

$RecordID = (is_array($RecordID) && sizeof($RecordID)>0)?$RecordID[0]:$RecordID;
$isNew = $RecordID=="" ? true:false;
$EditNewTitle = ($RecordID=="")? $Lang['Btn']['New'] : $Lang['Btn']['Edit'];

$type = '';

if(!$isNew){
	$sql = "SELECT * FROM MAIL_AVAS_RULES WHERE RecordID='$RecordID'";
	$records = $li->returnArray($sql);
	$record = $records[0];
	$type = $record['Type'];
}

$type_selection = '<select id="Type" name="Type">';
$type_selection.= '<option value="subject" '.($type=='subject'?'selected':'').'>'.$Lang['Gamma']['Subject'].'</option>';
$type_selection.= '<option value="body" '.($type=='body'?'selected':'').'>'.$Lang['Gamma']['Message'].'</option>';
$type_selection.= '</select>';

?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, '../../',$i_spam['Setting'],'../index.php', $Lang['Gamma']['RuleSettings'], 'index.php' ,$EditNewTitle, '') ?>
<?php
## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";
?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $xmsg) ?>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="JavaScript">
function remove_specialchars(text)
{
	var chars_to_remove = '~!@#$%^&*()_+|{}:"<>?`-=\\[];\',./';
	
	for(var i=0;i<chars_to_remove.length;i++){
		var c = chars_to_remove.charAt(i);
		while(text.indexOf(c) != -1) {
			text = text.replace(c,'');
		}
	}
	return text;
}

function checkform(formObj)
{
	var rule = $.trim(remove_specialchars($('input#Rule').val()));
	var score = parseFloat($.trim($('input#Score').val()));
	var rule_warn_div = $('#rule_warning');
	var score_warn_div = $('#score_warning');
	var is_valid = true;
	
	$('input#Rule').val(rule);
	
	if(rule == ''){
		rule_warn_div.show();
		if(is_valid) {
			$('input#Rule').focus();
		}
		is_valid = false;
	}else{
		rule_warn_div.hide();
	}
	
	if((score!=0 && isNaN(score)) || (score!=0 && score == '') || !(score >= -20.0 && score <= 20.0)){
		score_warn_div.show();
		if(is_valid){
			$('input#Score').focus();
		}
		is_valid = false;
	}else{
		score_warn_div.hide();
	}
	
	if(is_valid){
		formObj.submit();
	}
}
</script>
<form id="form1" name="form1" action="rule_update.php" method="post" onsubmit="checkform(this);return false;">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td>
			<blockquote>
			<p>
				<table cellspacing="0" cellpadding="5" border="0" width="100%">
				<tbody>
					<tr>
						<td style="vertical-align: middle;"><font color="red">*</font>^<?=$Lang['Gamma']['Rule']?>:</td>
						<td>
							<input type="text" value="<?=intranet_htmlspecialchars($record['Rule'])?>" maxlength="255" size="50" id="Rule" name="Rule" class="text">
							<div  id="rule_warning" style="display:none"><font color="red">*<span><?=$Lang['Gamma']['Warning']['PleaseInputRule']?></span></font></div>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;"><font color="red">*</font>*<?=$Lang['Gamma']['Score']?>:</td>
						<td>
							<input type="text" value="<?=$record['Score']?>" maxlength="255" size="20" id="Score" name="Score" class="text">
							<div id="score_warning" style="display:none"><font color="red">*<span><?=$Lang['Gamma']['Warning']['PleaseInvalidScore']?></span></font></div>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;"><font color="red">*</font>*<?=$Lang['General']['Type']?>:</td>
						<td>
							<?=$type_selection?>
						</td>
					</tr>
				</tbody>
				</table>
			</p>
			<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
				<tr><td><span class="tabletextrequire"><?=$Lang['Gamma']['RuleRemark']?></span></tr>
				<tr><td><span class="tabletextrequire"><?=$Lang['Gamma']['AVASScoreDescription']?></span></tr>
				<tr><td><span class="tabletextrequire"><?=str_replace('*','<font color="red">*</font>',$Lang['General']['RequiredField'])?></span></tr>
			</table>
			</BLOCKQUOTE>
		</td>
	</tr>
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1></td>
	</tr>
	<tr>
		<td align="right">
			<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 			<?= btnReset() ?>
		</td>
	</tr>
</table>
<input type="hidden" id="RecordID" name="RecordID" value="<?=$record['RecordID']?>" />
</form>
<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
