<?php
// Editing by 
/*
 * 2014-09-15 (Carlos): iMail plus use its own osapi call
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libwebmail.php");
include_once("../../../includes/imap_gamma.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

if($plugin['imail_gamma']){
	$lwebmail = new imap_gamma(1);
}else{
	$lwebmail = new libwebmail();
}
$whitelist = $lwebmail->readWhiteList();
$toolbar = "<a class=iconLink href=\"whitelist_new.php\">".newIcon()."$button_add</a>";

?>
<SCRIPT LANGUAGE=Javascript>
function confirmRemove(target)
{
         if (confirm("<?=$i_spam['AlertRemove']?>"))
         {
             window.location = "whitelist_remove.php?target="+target;
         }
         else
         {
         }
}
</SCRIPT>
<form name="form1" action="bypass_update.php" method="post">
<?= displayNavTitle($i_adminmenu_fs, '', $i_CampusMail_New_iMail_Settings, '../',$i_spam['Setting'],'index.php',$i_spam['WhiteList'],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $toolbar; ?></td></tr>
<tr><td>

  <table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 >
    <tr><td class="tableTitle"><?=$i_spam['EmailAddress']?></td><td class="tableTitle"><?=$button_remove?></td></tr>
<?php
  $x = "";
  for ($i=0; $i<sizeof($whitelist); $i++)
  {
     $css = ($i%2==0? "":"2");
     $target = $whitelist[$i];
     $link = "<a href=\"javascript:confirmRemove('".urlencode($target)."')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
     $x .= "<tr class=tableContent$css><td>$target</td><td>$link</td></tr>\n";
  }

  if (sizeof($whitelist)==0)
  {
      $x .= "<tr><td colspan=2 align=center>$i_no_record_exists_msg</td></tr>\n";
  }

  echo $x
?>
  </table>

</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>


<?
include_once("../../../templates/adminfooter.php");
?>
