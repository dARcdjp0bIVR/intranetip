<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

intranet_opendb();

$lwebmail = new libwebmail();

$days_in_trash = $lwebmail->retriveDayInTrashByAdmin();

$days = array(1,2,3,4,5,6,7,14,21,30,60);

if($plugin['imail_gamma'] === true)
{
	$IMap = new imap_gamma($skipLogin = 1);
 
//	$li = new libfilesystem();
//	$location = $intranet_root."/file/gamma_mail";
//	$li->folder_new($location);
//	$file = $location."/days_in_trash_spam.txt";

//	list($days_in_trash,$days_in_spam) = explode(",",trim(get_file_content($file)));
	$days_in_spam = $IMap->days_in_spam;
	$days_in_trash = $IMap->days_in_trash;
	
	$Days = " ".$Lang['Gamma']['Days'];
	$select_spam_days = "<select name='days_in_spam'  >";
	//$select_trash_days.="<option value='-1' ".($days_in_trash==-1?" SELECTED ":"").">$i_CampusMail_New_Settings_Forever</option>";
	for($i=0;$i<sizeof($days);$i++){
		$select_spam_days.="<option value='".$days[$i]."'".($days_in_spam==$days[$i]?" SELECTED ":"").">".$days[$i].$Days."</option>";
	}
	$select_spam_days.="</select>";
}


$select_trash_days = "<select name='days_in_trash'  >";
//$select_trash_days.="<option value='-1' ".($days_in_trash==-1?" SELECTED ":"").">$i_CampusMail_New_Settings_Forever</option>";
for($i=0;$i<sizeof($days);$i++){
	$select_trash_days.="<option value='".$days[$i]."'".($days_in_trash==$days[$i]?" SELECTED ":"").">".$days[$i].$Days."</option>";
}
$select_trash_days.="</select>";

### Generate Layout Title Bar ###
$mail_server_quota_control = $special_feature['imail'] && $lwebmail->has_webmail && ($lwebmail->type==3);
$has_imail = $special_feature['imail'];
if ($has_imail)
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}
?>

<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, '../campusmail_set/index.php', $i_CampusMail_Admin_DayInTrashSettings, '') ?>
<?= displayTag("$img_tag", $msg) ?>

<form name='form1' method='POST' action='day_in_trash_update.php'>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
	<td class=tableContent>
	<br>
	<blockquote>
	<table width=500 border=0 cellspacing=1 cellpadding=3>

	<?if($plugin['imail_gamma'] === true){?>
	<tr>
		<td style="vertical-align:middle; text-align:right; width:40%" ><?=$Lang['Gamma']['DayInTrash']?></td>
		<td><?=$select_trash_days;?></td>
	</tr>
	<tr>
		<td style="vertical-align:middle; text-align:right; width:40%" ><?=$Lang['Gamma']['DayInSpam']?></td>
		<td><?=$select_spam_days;?></td>
	</tr>
	<?}
	else
	{
	?>
	<tr>
		<td style="vertical-align:middle; text-align:right; width:40%" ><?=$i_CampusMail_New_Settings_DaysInTrash?></td>
		<td><?=$select_trash_days;?></td>
	</tr>
	<?}?>
	</table>
	</BLOCKQUOTE>
	</td>
</tr>
<?if($plugin['imail_gamma'] !== true){?>
<tr><td><span class="tabletextrequire2"><font color="red"><?=$i_CampusMail_Admin_DayInTrashSettings_Notice;?></font></span></td></tr>
<?}?>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
<?= btnReset() ?>
</td></tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>