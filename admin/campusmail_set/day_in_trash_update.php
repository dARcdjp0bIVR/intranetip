<?php
// editing by 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

if($plugin['imail_gamma'] === true)
{
	include_once($PATH_WRT_ROOT."includes/imap_gamma.php");	
	$IMap = new imap_gamma($skipLogin =1);

	$curr_days_in_trash = $IMap->days_in_trash;
	$curr_days_in_spam =  $IMap->days_in_spam;
	
	if($days_in_trash!=$curr_days_in_trash || ($days_in_spam!=$curr_days_in_spam))
	{

		
		if($days_in_trash!=$curr_days_in_trash)
		{
			if($IMap->changeTrashDay($days_in_trash))
			{
				$curr_days_in_trash = $days_in_trash;
			}
		}
		
		if($days_in_spam!=$curr_days_in_spam)
		{
			
			if($IMap->changeSpamDay($days_in_spam))
			{
				$curr_days_in_spam = $days_in_spam;
			}
		}
		
		$IMap->setDayInTrashSpam($curr_days_in_trash,$curr_days_in_spam);
		
	}
	$success = true;
	
}
else
{
	$file_content = $days_in_trash;
	
	$li = new libfilesystem();
	$location = $intranet_root."/file/iMail";
	$li->folder_new($location);
	$file = $location."/days_in_trash.txt";
	$success = $li->file_write($file_content, $file);
}

intranet_closedb();

if($success){
	header("Location: day_in_trash.php?msg=2");
}else{
	header("Location: day_in_trash.php?msg=13");
}
?>