<?php
// editing by 
/*
 * 2018-11-13 (Carlos): Added $sys_custom['iMailPlus']['ForceRemovalExcludeUserID'] to exclude certain users avoid being cleaned emails.
 * 2017-08-28 (Carlos): Cater LDAP $intranet_authentication_method for getting password.
 * 2014-07-03 (Carlos): Use new approach to retrieve password if using hash password mechanism
 * 2013-09-04 (Carlos): Fix get password problem
 * 2012-08-14 (Carlos): for iMail plus force removal
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once("../../includes/libpwm.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	echo "0";
	exit();
}

$TargetUserType = $_REQUEST['TargetUserType'];
$ActionType = $_REQUEST['ActionType'];
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$RemoveUserID = $_REQUEST['RemoveUserID'];

$li = new libdb();

//intranet_closedb();
//exit;

$more_conds = "";
if(isset($sys_custom['iMailPlus']['ForceRemovalExcludeUserID']) && is_array($sys_custom['iMailPlus']['ForceRemovalExcludeUserID']) && count($sys_custom['iMailPlus']['ForceRemovalExcludeUserID'])>0){
	$more_conds .= " AND u.UserID NOT IN (".implode(",",$sys_custom['iMailPlus']['ForceRemovalExcludeUserID']).") ";
}

if($ActionType==1){ // Clean mails by user type
	$sql = "SELECT u.UserID,u.ImapUserEmail,u.UserPassword,s.EncPassword 
			FROM INTRANET_USER as u 
			LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = u.UserID 
			WHERE u.RecordType='$TargetUserType' AND u.RecordStatus IN (0,1) AND u.UserID IN (".implode(",",$RemoveUserID).") $more_conds 
				AND u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> '' ";
	$user_list = $li->returnArray($sql);
	$numOfData = sizeof($user_list);
	
	$success = true;
	if($numOfData>0)
	{
		if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
			$libpwm = new libpwm();
			$uid_ary = Get_Array_By_Key($user_list,'UserID');
			$uidToPw = $libpwm->getData($uid_ary);
		}
		
		# delete action log  
		if (!is_dir("$intranet_root/file/mailDeleteLog")) {
			mkdir("$intranet_root/file/mailDeleteLog",0777);
		}
		if (!is_dir("$intranet_root/file/mailDeleteLog/adminconsole")) {
			mkdir("$intranet_root/file/mailDeleteLog/adminconsole",0777);
		}
		$maillogPath = $intranet_root.'/file/mailDeleteLog/adminconsole';
		
		$logFilename = 'force_removal_'.date('Y-m-d').'.log';
		
		$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
		if($logFileHandle) {
			$log_content = "Time: ".date("Y-m-d H:i:s")."\n";
			$log_content .= "Client IP: ".$_SERVER['REMOTE_ADDR']."\n";
			$log_content .= "Date range: ".$StartDate." - ".$EndDate."\n";
			$log_content .= "Query: ".$sql."\n\n";
			fwrite($logFileHandle, $log_content);
			fclose($logFileHandle);
		}
		# end of log
		
		//$start_time = time();
		$Result = array();
		for($i=0;$i<$numOfData;$i++){
			list($uid, $imap_user_email,$user_password,$enc_password) = $user_list[$i];
			$password = $user_password;
			/*
			if($user_password == "" && $enc_password != ""){
		     	$password = GetDecryptedPassword($enc_password);
		    }else if($user_password != ""){
		    	$password = $user_password;
		    }
			*/
			if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
     			$password = $uidToPw[$uid];
     		}
			//$_SESSION['SSV_EMAIL_LOGIN'] = $imap_user_email;
			//$_SESSION['SSV_EMAIL_PASSWORD'] = $password;
			//$_SESSION['SSV_LOGIN_EMAIL'] = $imap_user_email;
			//$IMap = new imap_gamma();
			if($imap_user_email == '' || $password == ''){
				$Result[] = false;
				continue;
			}
			$IMap = new imap_gamma(false,$imap_user_email,$password);
			if($IMap->ConnectionStatus != true){
				$Result[] = false;
				continue;
			}
			
			$FolderList = $IMap->getAllFolderList();
			$CleanResult = array();
			for($j=0;$j<count($FolderList);$j++){
				$set_delete_success = false;
				if($IMap->Go_To_Folder($FolderList[$j]))
				{
					$IMap_obj = imap_check($IMap->inbox);
					$max_msgno = $IMap_obj->Nmsgs;
					if($max_msgno>0){
						$set_delete_success = imap_setflag_full($IMap->inbox,"1:$max_msgno","\\Deleted");
					}
				}
				if($set_delete_success) imap_expunge($IMap->inbox);
				$CleanResult[] = $set_delete_success;
			}
			$IMap->close();
			//$Result[] = !in_array(false,$CleanResult);
		}
		$success = !in_array(false,$Result);
		//$end_time = time();
		//$process_time = sprintf("%.2f",($end_time - $start_time));
		//debug_pr($process_time);
	}
}else if($ActionType==2){ // Clean mails by user type and date range
	$sql = "SELECT u.UserID,u.ImapUserEmail,u.UserPassword,s.EncPassword 
			FROM INTRANET_USER as u 
			LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = u.UserID 
			WHERE u.RecordType='$TargetUserType' AND u.RecordStatus IN (0,1) AND u.UserID IN (".implode(",",$RemoveUserID).") $more_conds 
				AND u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> '' ";
	$user_list = $li->returnArray($sql);
	$numOfData = sizeof($user_list);
	
	$success = true;
	if($numOfData>0)
	{
		if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
			$libpwm = new libpwm();
			$uid_ary = Get_Array_By_Key($user_list,'UserID');
			$uidToPw = $libpwm->getData($uid_ary);
		}
		
		# delete action log  
		if (!is_dir("$intranet_root/file/mailDeleteLog")) {
			mkdir("$intranet_root/file/mailDeleteLog",0777);
		}
		if (!is_dir("$intranet_root/file/mailDeleteLog/adminconsole")) {
			mkdir("$intranet_root/file/mailDeleteLog/adminconsole",0777);
		}
		$maillogPath = $intranet_root.'/file/mailDeleteLog/adminconsole';
		
		$logFilename = 'force_removal_'.date('Y-m-d').'.log';
		
		$logFileHandle = (file_exists($maillogPath.'/'.$logFilename)) ? fopen($maillogPath.'/'.$logFilename,'a') : fopen($maillogPath.'/'.$logFilename,'w');
		if($logFileHandle) {
			$log_content = "Time: ".date("Y-m-d H:i:s")."\n";
			$log_content .= "Client IP: ".$_SERVER['REMOTE_ADDR']."\n";
			$log_content .= "Date range: ".$StartDate." - ".$EndDate."\n";
			$log_content .= "Query: ".$sql."\n\n";
			fwrite($logFileHandle, $log_content);
			fclose($logFileHandle);
		}
		# end of log
		
		$Result = array();
		for($i=0;$i<$numOfData;$i++){
			list($uid, $imap_user_email,$user_password,$enc_password) = $user_list[$i];
			$password = $user_password;
			/*
			if($user_password == "" && $enc_password != ""){
		     	$password = GetDecryptedPassword($enc_password);
		    }else if($user_password != ""){
		    	$password = $user_password;
		    }
			*/
			if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
     			$password = $uidToPw[$uid];
     		}
			
			//$_SESSION['SSV_EMAIL_LOGIN'] = $imap_user_email;
			//$_SESSION['SSV_EMAIL_PASSWORD'] = $password;
			//$_SESSION['SSV_LOGIN_EMAIL'] = $imap_user_email;
			//$IMap = new imap_gamma();
			if($imap_user_email == '' || $password == ''){
				$Result[] = false;
				continue;
			}
			$IMap = new imap_gamma(false,$imap_user_email,$password);
			if($IMap->ConnectionStatus != true){
				$Result[] = false;
				continue;
			}
			
			$FolderList = $IMap->getAllFolderList();
			$CleanResult = array();
			//debug_r($FolderList);
			for($j=0;$j<count($FolderList);$j++){
				if($IMap->Go_To_Folder($FolderList[$j]))
				{
					$uids = $IMap->Search_Mail($FolderList[$j],"","","","","",$StartDate,$EndDate,"","",1);
					//debug_r($uids);
					for($k=0;$k<count($uids);$k++){
						imap_delete($IMap->inbox,$uids[$k][1]);
					}
				}
				imap_expunge($IMap->inbox);
			}
			$IMap->close();
			//$Result[] = !in_array(false,$CleanResult);
		}
		$success = !in_array(false,$Result);
	}
}

echo $success?"1":"0";

intranet_closedb();
?>