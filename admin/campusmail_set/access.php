<?php
// editing by 
/***************************************** Change log **********************************************
 * 2011-10-19 (Carlos): Added Alumni access and quota setting
 *****************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if($plugin['imail_gamma'] === true )
{
	include_once("../../includes/imap_gamma.php");	
	$IMap = new imap_gamma($skipLogin = 1);

//	$location = $intranet_root."/file/gamma_mail";
//	$li->folder_new($location);
//	$file = $location."/internet_mail_access.txt";
//	
//	if(file_exists($file))
//		$enable = get_file_content($file);
//	else
//		$enable = 1;
	
	$enable = $IMap->internet_mail_access;
	
	$access_right  = $IMap->access_right;
	$userquota  = $IMap->default_quota;
	
	$internal_only = $IMap->internal_mail_only;
}
else
{
	$li = new libfilesystem();
	$file_content = $li->file_read($intranet_root."/file/campusmail_set.txt");
	
	if ($file_content == "")
	{
	    $access_right = array(1,1,1);
	    $userquota = array(10,10,10);
	    if($special_feature['alumni']){
	    	$access_right[] = 1;
	    	$userquota[] = 10;
	    }
	}
	else
	{
	    unset($campusmail_set_line);
	    $campusmail_set_line = array();
	
	    $content = explode("\n", $file_content);
	    $campusmail_set_line[] = explode(":",$content[0]);
	    $campusmail_set_line[] = explode(":",$content[1]);
	    $campusmail_set_line[] = explode(":",$content[3]);
	    if($special_feature['alumni']) $campusmail_set_line[] = explode(":",$content[2]); // alumni UserType is 4 but quota is saved at index 2
	    $access_right = array($campusmail_set_line[0][0],$campusmail_set_line[1][0],$campusmail_set_line[2][0]);
	    if($special_feature['alumni']) $access_right[] = $campusmail_set_line[3][0];
	    for ($i=0; $i<sizeof($campusmail_set_line); $i++)
	    {
	         $userquota[] = ( $campusmail_set_line[$i][1]!=""? $campusmail_set_line[$i][1]:10);
	    }
	}
}
$id_names = array($i_identity_teachstaff,$i_identity_student,$i_identity_parent);
if($special_feature['alumni']) $id_names[] = $i_identity_alumni;

if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $quota_title = $i_CampusMail_New_DefaultQuota;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $quota_title = $i_Campusquota_quota;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}




?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         len = obj.elements.length;
         var i=0;
         for (i=0; i<len; i++)
         {
              d = obj.elements[i];
              if (d.name== "quota[]")
              {
                  temp = parseInt(d.value);
                  if (isNaN(temp) || temp < 0)
                  {
                      alert(NonIntegerWarning);
                      d.focus();
                      return false;
                  }
                  else
                  {
                      d.value =  temp;
                  }
              }
         }
         return true;
}
<?if($plugin['imail_gamma'] === true ){?>
function showHideInternalOnly(obj)
{
	col_head = document.getElementById('internal_layer_header');
	col0 = document.getElementById('internal_layer0');
	col1 = document.getElementById('internal_layer1');
	col2 = document.getElementById('internal_layer2');
<?php if($special_feature['alumni']){ ?>
	col3 = document.getElementById('internal_layer3');
<?php } ?>
	if(obj.options[obj.selectedIndex].value==1){
		col_head.style.display='';
		col0.style.display='';
		col1.style.display='';
		col2.style.display='';
<?php if($special_feature['alumni']){ ?>
		col3.style.display='';
<?php } ?>
	}else{
		col_head.style.display='none';
		col0.style.display='none';
		col1.style.display='none';
		col2.style.display='none';
<?php if($special_feature['alumni']){ ?>
		col3.style.display='none';
<?php } ?>
	}
}
<?}?>
</SCRIPT>


<form name="form1" action="access_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['GeneralSetting'],'') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p>
<?if($plugin['imail_gamma'] === true ){
$enable_onchange = " onChange='showHideInternalOnly(this)' ";
$internal_only_style = $enable==1?"":" style='display:none' ";
?>
	
<table border=0 cellpadding=5 cellspacing=0>
	<tr>
		<td><?=$Lang['Btn']['Enable']?></td>
		<td>
			<select name="enable" <?=$enable_onchange?>>
				<option value=0 <?=$enable==0?"selected":""?>><?=$Lang['Gamma']['InternalMailOnly']?></option>
				<option value=1 <?=$enable==1?"selected":""?>><?=$Lang['Gamma']['InternalAndInternet']?></option>
			</select>
		</td>
	</tr>
</table>
<?}?>
<br>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_Campusquota_identity ?></u></td>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_Set_enabled ?></u></td>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $quota_title ?></u></td>
<?if($plugin['imail_gamma'] === true ){?>
<td class=tableTitle_new style="vertical-align:bottom;" id=internal_layer_header <?=$internal_only_style?>><u><?= $Lang['Gamma']['InternalMailOnly'] ?></u></td>
<?}?>
</tr>
<?php for($i=0; $i<sizeof($access_right); $i++) {
if ($access_right[$i] == 0)
{
    $checked = "";
    $disabled = "DISABLED";
}
else
{
    $checked = " CHECKED";
    $disabled = "";
}
$q = $userquota[$i];

?>
<tr>
<td style="vertical-align:middle"><?=$id_names[$i]?></td>
<td><input onClick="this.form.quota<?=$i?>.disabled =!(this.form.quota<?=$i?>.disabled);this.form.internal_only<?=$i?>.disabled=!(this.form.internal_only<?=$i?>.disabled);" type=checkbox name=allow<?=$i?><?=$checked?> value=1></td>
<td><input class=text type=text <?=$disabled?> name=quota<?=$i?> size=7 maxlength=10 value="<?php echo $q; ?>"></td>
<?if($plugin['imail_gamma'] === true ){?>
<td id=internal_layer<?=$i?> <?=$internal_only_style?>><input type="checkbox" name=internal_only<?=$i?> value=1 <?=$internal_only[$i]==1?"checked":""?> <?=$disabled?>></input></td>
<?}?>
</tr>
<?php } ?>
</table>

</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>
