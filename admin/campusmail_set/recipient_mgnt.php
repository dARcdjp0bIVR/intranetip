<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");


if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

?>

<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$i_campusmail_recipient_selection_control,'') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?= displayOption(              $i_identity_student,'recipient_mgnt_edit.php?usertype=2&teaching=0',1,
                                $i_identity_parent, 'recipient_mgnt_edit.php?usertype=3&teaching=0', 1,
                                $i_campusmail_teaching_staff, 'recipient_mgnt_edit.php?usertype=1&teaching=1', 1,
                                $i_campusmail_non_teaching_staff, 'recipient_mgnt_edit.php?usertype=1&teaching=2', 1
                  
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>