<?php
// editing by 
/*
 * 2017-06-29 (Carlos): Added Quota Used table column and search text input.
 */
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

intranet_opendb();

$lclass = new libclass();
# Search Type
$UserType = $_GET['UserType'];
$type_name = $i_identity_array[$UserType];

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;
$namefield = getNameFieldByLang("a.");
if ($UserType == 2 && $ClassName != "")
{
    $conds = " AND a.ClassName = '$ClassName'";
}
$sql  = "SELECT
               CONCAT('<a class=tableContentLink href=real_list_edit.php?UserID[]=',a.UserID,'>',$namefield,'</a>'),
               a.UserLogin,
               a.ClassName, 
			   a.ClassNumber,
               CONCAT(IF(c.QuotaUsed IS NULL,0,ROUND(c.QuotaUsed/1024.00,2)),'MB') as QuotaUsed,
			   CONCAT(IF(b.Quota IS NULL,0,b.Quota),'MB') as Quota,
               CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>')
               FROM
                        INTRANET_USER AS a
                LEFT JOIN
                        INTRANET_CAMPUSMAIL_USERQUOTA AS b ON a.UserID = b.UserID 
				LEFT JOIN INTRANET_CAMPUSMAIL_USED_STORAGE as c ON c.UserID=a.UserID 
                WHERE a.RecordType = '$UserType' ";
	if($keyword != ''){
		$safe_keyword = $lclass->Get_Safe_Sql_Like_Query($keyword);
        $sql .= " AND ($namefield like '%$safe_keyword%' OR a.UserLogin like '%$safe_keyword%') ";
	}
       $sql .= $conds;

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.EnglishName","a.UserLogin", "a.ClassName", "a.ClassNumber", "QuotaUsed", "Quota");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_group;
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$name_title = ($intranet_session_language=="en"?$i_UserEnglishName:$i_UserChineseName);
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $name_title)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_LinuxAccount_UsedQuota)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_CampusMail_New_Quota)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("UserID[]")."</td>\n";

if ($UserType == 2)
    $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()",$ClassName);

$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'real_list_type_edit.php')\">".newIcon()."$i_CampusMail_New_BatchUpdate</a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'UserID[]','real_list_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$searchbar = "$select_class";
$searchbar  .= "<input class=\"text\" type=\"text\" name=\"keyword\" size=\"10\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_CampusMail_New_QuotaSettings,'realquota.php',$i_CampusMail_New_ViewQuota_Identity." ".$i_identity_array[$UserType],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="get">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=UserType value="<?=$UserType?>">
</form>




<?php
include_once("../../templates/adminfooter.php");
?>