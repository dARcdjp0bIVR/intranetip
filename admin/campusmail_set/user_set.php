<?php
// editing by 
/****************************************************** Change log **********************************************************************
 * 2013-12-16 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - if INTRANET_USER.ImapUserLogin is set, use it as email account name
 ****************************************************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libwebmail.php");
intranet_opendb();

if($plugin['imail_gamma']!==true) $lwebmail = new libwebmail();
$li = new libdb();
$sql = "SELECT UserID, RecordType, IMapUserEmail ".($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']?",ImapUserLogin":"")." FROM INTRANET_USER WHERE UserLogin = '$loginName'";
$temp = $li->returnArray($sql,2);
list($uid, $utype, $imapemail) = $temp[0];
if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName']){
	$imap_user_login = $temp[0]['ImapUserLogin'];
}
if ($uid == "")
{
    header("Location: user.php?error=1");
    intranet_closedb();
    exit();
}
if (!in_array($utype,$webmail_identity_allowed))
{
     header("Location: user.php?error=2");
     intranet_closedb();
     exit();
}

$loginName = strtolower($loginName);

if($plugin['imail_gamma']===true){
	include_once("../../includes/imap_gamma.php");
	$IMap = new imap_gamma(1);
	if($sys_custom['iMailPlus']['EmailAliasName'] && $imap_user_login != ''){
		$IMapLoginName = $imap_user_login."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}else{
		$IMapLoginName = $loginName."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}
	$account_exists = $IMap->is_user_exist ($IMapLoginName);
	if($account_exists){
		$quota = $IMap->getTotalQuota($IMapLoginName,"iMail");
	}else{
		$quota = $IMap->default_quota[$utype-1];
	}
	if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
}else{
	$account_exists = $lwebmail->isAccountExist($loginName,"iMail");
	
	if ($account_exists)
	{
	     $quota = $lwebmail->getTotalQuota($loginName,"iMail");
	}
	else
	{
	    $file_content = get_file_content($intranet_root."/file/account_mail_quota.txt");
	    if ($file_content == "")
	    {
	        $userquota = array(10,10,10,10);
	    }
	    else
	    {
	        $userquota = explode("\n", $file_content);
	    }
	    $quota = $userquota[$utype-1];
	}
}
#$used = $lwebmail->getUsedQuota($loginName);
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (obj.quota.value==0) return true;
         if (!check_int(obj.quota,<?=$quota?>,'<?=$i_LinuxAccount_Alert_QuotaMissing?>')) return false;
         return true;
}
</SCRIPT>


<form name="form1" action="user_set_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_LinuxAccount_SetUserQuota,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=400 border=0>
<tr><td align=right><?=$i_LinuxAccount_AccountName?>:</td><td><?=$loginName.($plugin['imail_gamma'] && $sys_custom['iMailPlus']['EmailAliasName'] && $imap_user_login!=''?"($imap_user_login)":"")?></td></tr>
<? if ($account_exists) { ?>
<tr><td align=right><?=$i_LinuxAccount_CurrentQuota?>:</td><td><?=$quota?></td></tr>
<? } else { ?>
<tr><td colspan=2 align=center><font COLOR=red><?=$i_LinuxAccount_NewCreation?></font></td></tr>
<? } ?>
<tr><td align=right><?=$i_LinuxAccount_Quota?>:</td><td><input type=text size=5 name=quota value=<?=$quota?>> MBytes </td></tr>
<?
	if($plugin['imail_gamma']===true){
		if(trim($imapemail)!=''){
			$activate_checked = " CHECKED ";
			$suspend_checked = "";
		}else{	
			$activate_checked = "";
			$suspend_checked = " CHECKED ";
		}
	}
//if($plugin['imail_gamma']===true){
//	if(trim($imapemail)!='') $linked_checked = " CHECKED ";
//	else $linked_checked = "";
//}else{
	$sql = "SELECT ACL FROM INTRANET_SYSTEM_ACCESS WHERE UserID = '$uid'";
	$arr_linked = $li->returnVector($sql);
	if(sizeof($arr_linked)>0){
		$linked_val = $arr_linked[0];
	}
	if($linked_val == 1 || $linked_val == 3)
		$linked_checked = " CHECKED ";
	else
		$linked_checked = "";
//}
?>
<tr><td align=right><?=$i_CampusMail_New_ExternalAllow?>:</td><td><input type=checkbox name=linked value=1 <?=$linked_checked;?>></td></tr>
<?php 
if($plugin['imail_gamma']===true)
{
?>
<tr>
<td align=right><?=$Lang['General']['Status']?>:</td>
<td>
	<label for="status_activate"><input type="radio" id="status_activate" name="status" value="1" <?=$activate_checked?> /><?=$Lang['Status']['Activate']?></label>&nbsp;
	<label for="status_suspend"><input type="radio" id="status_suspend" name="status" value="0" <?=$suspend_checked?> /><?=$Lang['Status']['Suspend']?></label>
</td>
</tr>
<?php
}
?>
<tr>
<td>&nbsp;</td>
<td><font color=red><?=$i_LinuxAccount_UserSetNote?></font></td>
</tr>
</table>


</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
<input type="hidden" name="loginName" value="<?=$loginName?>">
<input type="hidden" name="oldQuota" value="<?=$quota?>">
<input type="hidden" name="type" value="<?=$type?>">
<input type="hidden" name="target" value="<?=$target?>">
<input type="hidden" name="ClassName" value="<?=$ClassName?>">
<input type="hidden" name="uid" value="<?=$uid?>">
<? if (!$account_exists) { ?>
<input type="hidden" name="newAccount" value="1">
<? } ?>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>