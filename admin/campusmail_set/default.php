<?php
// editing by 
/************************************************* Change log ****************************************************
 * 2011-10-19 (Carlos): Added alumni quota
 *****************************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

$li = new libfilesystem();
$file_content = $li->file_read($intranet_root."/file/account_mail_quota.txt");
$id_names = array($i_identity_teachstaff,$i_identity_student,$i_identity_parent);
if($special_feature['alumni']) $id_names[] = $i_identity_alumni;

if ($file_content == "")
{
   $userquota = array(10,10,10);
   if($special_feature['alumni']) $userquota[] = 10;
}
else
{
    $userquota = explode("\n", $file_content);
    if(!$special_feature['alumni'] && count($userquota)>3) array_pop($userquota); 
    if($special_feature['alumni'] && $userquota[3]=="") $userquota[3] = 10; // Alumni is lately added, may not have been saved before
}
// $i_Campusquota_basic_identity

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         d = obj.quota0;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }

         d = obj.quota1;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }

         d = obj.quota2;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }
<?php if($special_feature['alumni']){ ?>         
         d = obj.quota3;
         temp = parseInt(d.value);
         if (isNaN(temp) || temp < 0)
         {
             alert(NonIntegerWarning);
             d.focus();
             return false;
         }
         else
         {
             d.value =  temp;
         }
<?php } ?>
         return true;
}
</SCRIPT>


<form name="form1" action="default_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_LinuxAccount_SetDefaultQuota,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_identity ?></u></td>
<td class=tableTitle_new style="vertical-align:bottom"><u><?= $i_LinuxAccount_Quota ?></u></td>
</tr>
<?php for($i=0; $i<sizeof($userquota); $i++) {
if (in_array(($i+1),$webmail_identity_allowed)) {

$q = $userquota[$i];

?>
<tr>
<td style="vertical-align:middle"><?=$id_names[$i]?></td>
<td><input class=text type=text name=quota<?=$i?> size=7 maxlength=10 value="<?php echo $q; ?>"></td>
</tr>
<?php
}
else
{
?>
<input type=hidden name=quota<?=$i?> value=0>
<?
}
} ?>
</table>

</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>