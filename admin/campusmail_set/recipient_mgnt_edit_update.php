<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if($usertype=="")
	header("Location: index.php");
if($usertype==2 && $ClassLevelID=="")
	header("Location: index.php");

$teaching = $teaching==""?0:$teaching;
	
		
$db = new libdb();

# process new interface
if($interface==1){
	
	$sql="UPDATE INTRANET_IMAIL_RECIPIENT_RESTRICTION SET Restricted=1,ToStaffOption='$toStaff',ToStudentOption='$toStudent',ToParentOption='$toParent',DateModified=NOW(),BlockedGroupCat='',BlockedUserType='' ";
	$sql.="WHERE TargetType='$usertype'";
	$conds="";
	
	# process staff
	if($usertype==1){
		$conds =" AND Teaching='$teaching'";
	}
	
	# process student
	else if($usertype==2){
		$conds = " AND ClassLevel='$ClassLevelID'";
	}
	# process parent
	else if($usertype==3){
		$conds="";
	}
	$sql.=$conds;
	
	$db->db_db_query($sql);
	//echo "<p>$sql</p>";
	if($db->db_affected_rows()<=0){
		$sql="INSERT INTO INTRANET_IMAIL_RECIPIENT_RESTRICTION (TargetType,Teaching,ClassLevel,Restricted,ToStaffOption,ToStudentOption,ToParentOption,DateInput,DateModified,BlockedGroupCat,BlockedUserType) VALUES";
		$sql.="('$usertype','$teaching','$ClassLevelID',1,'$toStaff','$toStudent','$toParent',NOW(),NOW(),'','')";
		$db->db_db_query($sql);
		//echo "<p>$sql</p>";
	}	
}


# process old interface
else if($interface==0){
	$str_blocked_groups = $blocked_group_cat_ids;
	$str_blocked_usertypes=$blocked_usertype_ids;
	
	$sql ="UPDATE INTRANET_IMAIL_RECIPIENT_RESTRICTION SET Restricted=0,BlockedGroupCat='$str_blocked_groups', BlockedUserType='$str_blocked_usertypes',DateModified=NOW(),ToStaffOption=0,ToStudentOption=0,ToParentOption=0";
	$sql.=" WHERE TargetType = '$usertype'";
	
	# process staff
	if($usertype==1){
		$conds = " AND Teaching='$teaching'";
	}
	# process student
	else if($usertype==2){
		$conds=" AND ClassLevel='$ClassLevelID'";
	}
	# process parent
	else if($usertype==3){
		$conds ="";
	}
	$sql.=$conds;
	$db->db_db_query($sql);
	//echo "<p>$sql</p>";
	if($db->db_affected_rows()<=0){
		$sql ="INSERT INTO INTRANET_IMAIL_RECIPIENT_RESTRICTION (TargetType,Teaching,ClassLevel,Restricted,BlockedGroupCat,BlockedUserType,DateInput,DateModified,ToStaffOption,ToStudentOption,ToParentOption) VALUES";
		$sql.="('$usertype','$teaching','$ClassLevelID',0,'$str_blocked_groups','$str_blocked_usertypes',NOW(),NOW(),0,0,0)";
		$db->db_db_query($sql);
		//echo "<p>$sql</p>";
	}
	
}

intranet_closedb();
header("Location: recipient_mgnt_edit.php?usertype=$usertype&teaching=$teaching&ClassLevelID=$ClassLevelID&msg=2");

?>