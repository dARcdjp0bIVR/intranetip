<?php
## Using By : 
/**************************************** Changes log ***********************************************
 * 2014-11-11 (Carlos): Generate a random password for users that do not have encrypted password logged.
 * 2014-07-03 (Carlos): Use new approach to retrieve password if using hash password mechanism
 * 2014-05-07 (Carlos): Added suspend/unsuspend mail account api call
 * 2013-12-16 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - if INTRANET_USER.ImapUserLogin is set, use it as email account name
 * 2012-11-21 (Carlos): split users into several chunks to get quota with Get_Quota_Info_List() to prevent timeout
 * 2011-12-05 (Carlos): modified array key userlogin to lowercase for comparison
 * 2011-10-19 (Carlos): Added process alumni
 * 2011-09-30 (Carlos): Cater schools that only use hashed password problem
 ***************************************************************************************************/
set_time_limit(0);
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libpwm.php");
intranet_opendb();

$li = new libdb();
$lwebmail = new libwebmail();
$allowed_type_list = implode(",",$webmail_identity_allowed);

if ($type==1)                # User Type
{
    if ($target == 1 || $target == 3 || $target == 4)
    {
        $sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, b.ACL, a.IMapUserEmail, s.EncPassword ".($sys_custom['iMailPlus']['EmailAliasName']?",a.ImapUserLogin":"")."
				FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID 
				   LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = a.UserID 
                   WHERE a.RecordType = $target
                         AND a.RecordType IN ($allowed_type_list)
                   ";
    }
    else
    {
        $sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, b.ACL, a.IMapUserEmail, s.EncPassword ".($sys_custom['iMailPlus']['EmailAliasName']?",a.ImapUserLogin":"")."
				FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID 
				   LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = a.UserID 
                   WHERE a.RecordType = $target AND a.ClassName = '$ClassName'
                         AND a.RecordType IN ($allowed_type_list)
                   ";
    }
}
else if ($type == 2)
{
     $sql = "SELECT a.UserID, b.UserLogin, b.UserPassword, c.ACL, b.IMapUserEmail, s.EncPassword ".($sys_custom['iMailPlus']['EmailAliasName']?",b.ImapUserLogin":"")."
        FROM INTRANET_USERGROUP as a 
		INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as c ON a.UserID = c.UserID 
		LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = b.UserID 
        WHERE a.GroupID = $target AND b.UserID IS NOT NULL
              AND b.RecordType IN ($allowed_type_list)
        ";
}
else
{
    header("Location: index.php");
    exit();
}

$quota += 0;
$users = $li->returnArray($sql,7);

if($plugin['imail_gamma']===true)
{
	include_once("../../includes/libaccountmgmt.php");
	include_once("../../includes/imap_gamma.php");
	$laccount = new libaccountmgmt();
	$IMap = new imap_gamma(true);
	$ban_ext_mail_list = trim($IMap->ban_ext_mail_list);
	$ban_ext_mail_list_arr = explode("\r\n",$ban_ext_mail_list);
	
	$user_logins = array();
	for($i=0;$i<sizeof($users);$i++){
		list($uid,$login,$password,$acl,$email) = $users[$i];
		if($sys_custom['iMailPlus']['EmailAliasName'] && $users[$i]['ImapUserLogin']!=''){
			$login = strtolower($users[$i]['ImapUserLogin']);
		}
		//if(trim($email)!='')
			$user_logins[] = strtolower($login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
	}
	//$quota_list = $IMap->Get_Quota_Info_List($user_logins);
	$quota_list = array();
	$userlogin_chunks = array_chunk($user_logins,200); // too many would wait long time to get function return, page will timeout
	for($i=0;$i<count($userlogin_chunks);$i++){
		$tmp_quota = $IMap->Get_Quota_Info_List($userlogin_chunks[$i]);
		$quota_list = array_merge($quota_list,$tmp_quota);
	}
	
	for($i=0;$i<sizeof($quota_list);$i++){
		$imap_user_login = strtolower($quota_list[$i]['UserLogin']);
		$account_exist = $quota_list[$i]['AccountExist'];
		if($account_exist==true) $CurrentAccount[$imap_user_login]['Quota'] = $quota_list[$i]['QuotaTotal'];
	}
	
	$to_block_email = array();
	$to_unblock_email = array();
}else
{
	### Get All current existing OS account ###
	$quota_list = $lwebmail->getQuotaTable("iMail");
	if(is_array($quota_list)){
		$quota_list = array_unique($quota_list);
		if(sizeof($quota_list)>0){
			foreach($quota_list as $key=>$value)
			{
				$CurrentAccount[strtolower($value[0])]['Quota'] = $value[2];
			}
		}
	}
	
	//foreach($quota_list as $key=>$value)
	//{
	//	$CurrentAccount[$value[0]]['Quota'] = $value[2];
	//}
}

if($intranet_authentication_method == 'HASH'){
	$libpwm = new libpwm();
	$uid_ary = Get_Array_By_Key($users,'UserID');
	$uidToPw = $libpwm->getData($uid_ary);
}

for ($i=0; $i<sizeof($users); $i++)
{
     list($uid, $userlogin, $password, $acl, $imapUserEmail, $encPassword) = $users[$i];
     $userlogin = strtolower($userlogin);
     $password = trim($password);
     /*
     $encPassword = trim($encPassword);
     if($password == "" && $encPassword != ""){
     	$password = GetDecryptedPassword($encPassword);
     }
     */
     if($intranet_authentication_method == 'HASH'){
     	$password = $uidToPw[$uid];
     }
     if($password == "" && $encPassword == ""){
     	$password = $userlogin;
     }
     
     if($password == ""){
		// generate a random password if password is not available
		$password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),0,10);
	 }
     
     if($plugin['imail_gamma']===true){
     	if($sys_custom['iMailPlus']['EmailAliasName'] && $users[$i]['ImapUserLogin']!=''){
     		$fulluserlogin = $users[$i]['ImapUserLogin']."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
     	}else{
     	 	$fulluserlogin = $userlogin."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
     	}
     }else{
	     ## if using ASAV, login name must have a domain name ##
		 if ($lwebmail->login_type=="email" && $lwebmail->login_domain != "")
			$fulluserlogin = $userlogin . "@" . $lwebmail->login_domain;
		 else
			$fulluserlogin = $userlogin;
     }
     
     if ($batchoption==1)              # Create accounts and link up
     {
     	/*
     	### Disable by Ronald 
     	### Reason : $lwebmail->is_user_exist($userlogin) need around 2sec to check user have os account or not.
     	### 			It will take a long time if it is too many target user, may make this page timeout.
     	
         if ($lwebmail->is_user_exist($userlogin))
         {
	         $lwebmail->setTotalQuota($userlogin,$quota,"iMail");
         }
         else
         {
             $lwebmail->open_account($userlogin, $password);
             $lwebmail->setTotalQuota($userlogin,$quota,"iMail");
         }
        */
        if($plugin['imail_gamma']===true){
        	if(trim($CurrentAccount[$fulluserlogin]['Quota']) == ""){		### No current quota, means no gamma mail account at this moment
        		$open_account_succeed = $IMap->open_account ($fulluserlogin, $password);
        		if($open_account_succeed){
        			$laccount->setIMapUserEmail($uid,$fulluserlogin);
        			$IMap->SetTotalQuota ($fulluserlogin, $quota, $uid);
        		}
        	}else{ # reset quota if required
        		//if($CurrentAccount[$fulluserlogin]['Quota'] != $quota){
        			$IMap->SetTotalQuota ($fulluserlogin, $quota, $uid);
        		//}
        		$laccount->setIMapUserEmail($uid,$fulluserlogin);
        		if(!in_array($fulluserlogin,$ban_ext_mail_list_arr)){// if it is not in ban send/receive mail list, unblock it
        			$to_unblock_email[] = $fulluserlogin;
        		}
        	}
        }else{
	         if($CurrentAccount[$fulluserlogin]['Quota'] == ""){		### No current quota, means no OS account at this moment
	         		$lwebmail->open_account($userlogin, $password);
	            	$lwebmail->setTotalQuota($userlogin,$quota,"iMail");
	            	//$lwebmail->setDBTotalQuota($uid,$quota);
	         }else{
	        	if($lwebmail->actype == "postfix")
				{	
						## if using ASAV, need to convert the quota from bytes to Mb
						$CurrentAccount[$fulluserlogin]['Quota']= $CurrentAccount[$fulluserlogin]['Quota']/1024/1024;
				}
				
				//if($CurrentAccount[$fulluserlogin]['Quota'] != $quota){		### If original quota is not match with the input one, then update the storage quota
	             		$lwebmail->setTotalQuota($userlogin,$quota,"iMail");
	             		//$lwebmail->setDBTotalQuota($uid,$quota);
				//}
			 }
        }
        if ($acl == "")
	    {
	         $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid,1)";
	    }
	    else
	    {
	         $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL + 1 WHERE UserID = $uid AND ACL IN (0,2)";
	    }
	    $li->db_db_query($sql);
	    
	    if(!$plugin['imail_gamma']){ // resume webmail
        	$lwebmail->setUnsuspendUser($fulluserlogin,"iMail");
        }
     }
     else if ($batchoption==2)   # unlink
     {
     	//if($plugin['imail_gamma']===true){
     	//	$laccount->setIMapUserEmail($uid,"");
     	//}
     	if($plugin['imail_gamma']===true){
     		$to_block_email[] = $fulluserlogin;
     	}
     	
        $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = $uid AND ACL IN (1,3)";
        $li->db_db_query($sql);
        
        if(!$plugin['imail_gamma']){ // suspend webmail
        	$lwebmail->setSuspendUser($fulluserlogin,"iMail");
        }
     }
     else if ($batchoption==3)   # remove accounts
     {
     	if($plugin['imail_gamma']===true){
     		if($IMap->is_user_exist($fulluserlogin)){
				if($IMap->delete_account($fulluserlogin)){
					$laccount->setIMapUserEmail($uid,"");
					$IMap->setIMapUserEmailQuota($uid,0);
					if($sys_custom['iMailPlus']['EmailAliasName']){
						$sql = "UPDATE INTRANET_USER SET ImapUserLogin=NULL WHERE UserID='$uid'";
						$li->db_db_query($sql);
					}
				}
			}
     	}
     	
 		$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 1 WHERE UserID = $uid AND ACL IN (1,3)";
    	$li->db_db_query($sql);
        $lwebmail->delete_account($userlogin);
     }else if($batchoption==4)  # activate mail account
     {
     	if($plugin['imail_gamma']===true && trim($CurrentAccount[$fulluserlogin]['Quota']) != ""){
     	 	$laccount->setIMapUserEmail($uid,$fulluserlogin);
     	}
     	if($plugin['imail_gamma']===true){ // resume iMail plus
     		$IMap->setUnsuspendUser($fulluserlogin,"iMail",$password);
     	}
     }else if($batchoption==5)  # suspend mail account
     {
     	if($plugin['imail_gamma']===true && trim($CurrentAccount[$fulluserlogin]['Quota']) != ""){
     	 	$laccount->setIMapUserEmail($uid,"");
     	}
     	if($plugin['imail_gamma']===true){ // suspend iMail plus
     		$IMap->setSuspendUser($fulluserlogin,"iMail");
     	}
     }
}

if($plugin['imail_gamma']===true){
	if($batchoption==1 && sizeof($to_unblock_email)>0)   # link & unblock send/receive email
	{
		$IMap->removeGroupBlockExternal($to_unblock_email);
	}
	
	if($batchoption==2 && sizeof($to_block_email)>0)  # unlink & block send/receive email
	{
		$IMap->addGroupBlockExternal($to_block_email);
	}
	
	if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
}

if ($type==1)       # From Type List
{
    $url = "list_type.php?UserType=$target&ClassName=$ClassName&msg=2";
}
else if ($type==2)    # From Group List
{
    $url = "list_group.php?GroupID=$target&msg=2";
}
else     # From Login name input
{
    $url = "index.php";
}

intranet_closedb();
header("Location: $url");
?>