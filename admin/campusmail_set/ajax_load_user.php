<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgroup.php");

intranet_opendb();

$lg = new libgroup($GroupID);
$UserList=array();
//debug_pr($lg->getStudentSelectByGroup($GroupID," id='UserSelection' name='UserIDArr' multiple size=10 style='width:80%'"));
if(trim($GroupID)!=''){
	$UserList = $lg->returnGroupUser(""," (a.IMapUserEmail <> '' AND a.IMapUserEmail IS NOT NULL) ");
}else if(trim($Type)!=''){
	$sql="SELECT UserID, ChineseName, EnglishName FROM INTRANET_USER WHERE RecordStatus = '1' AND RecordType='$Type' AND IMapUserEmail <> '' AND IMapUserEmail IS NOT NULL ORDER BY EnglishName";
	$UserList = $lg->returnArray($sql,3);
}

$options = array();
foreach($UserList as $key => $UserInfo)
{
	$options[] = array($UserInfo["UserID"],Get_Lang_Selection($UserInfo["ChineseName"],$UserInfo["EnglishName"]));
}
echo getSelectByArray($options," id='UserSelection' name='UserIDArr[]' multiple size=10 style='width:70%'",'','',1);
echo "<input type='button' value='".$Lang['Btn']['SelectAll']."' onclick='SelectAll(\"UserSelection\")'>";

?>