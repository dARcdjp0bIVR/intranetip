<?php
//editing by 
/*
 * 2017-06-29 (Carlos): Added Quota used info. 
 */
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libimailquota.php");
include_once("../../includes/libcampusquota.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

$UserID = (is_array($UserID)? $UserID[0]: $UserID);
$lu = new libuser($UserID);

$libcampusquota = new libcampusquota($UserID);
$used_quota = round($libcampusquota->returnUsedQuota() / 1024, 2); // KB to MB

$liq = new libimailquota();
$quota = $liq->returnQuotaForUser($UserID);


?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (!check_positive_int(obj.quota,'<?=$i_LinuxAccount_Alert_QuotaMissing?>',<?=$quota?>,1)) return false;
         return true;
}
</SCRIPT>


<form name="form1" action="real_list_edit_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_CampusMail_New_QuotaSettings,'realquota.php',$i_CampusMail_New_SingleEdit,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=300 border=0>
<tr><td><?=$i_CampusMail_New_TargetUser?>:</td><td><?=$lu->UserNameClassNumber()?></td></tr>
<tr><td><?=$i_LinuxAccount_UsedQuota?>:</td><td><?=$used_quota?> MBytes</td></tr>
<tr><td><?=$i_CampusMail_New_Quota?>:</td><td><input type=text size=5 name=quota value=<?=$quota?>> MBytes
<br>
</td></tr>
</table>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>

</td>
</tr>
</table>
<input type=hidden name=UserID value="<?=$UserID?>">
<input type=hidden name=url value="<?=$_SERVER["HTTP_REFERER"]?>">
</form>
<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>