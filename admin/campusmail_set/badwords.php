<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();
$has_imail = $special_feature['imail'];

if ($has_imail)
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}


if($plugin['imail_gamma'] === true){
	include_once("../../includes/imap_gamma.php");
	$IMap = new imap_gamma();
	$data = $IMap->bad_word_list;
}
else
{
	$base_dir = "$intranet_root/file/templates/";
	if (!is_dir($base_dir))
	{
	     $lf->folder_new($base_dir);
	}
	
	$target_file = "$base_dir"."mail_badwords.txt";
	$data = get_file_content($target_file);
}
?>

<script language="javascript">
function checkform(obj){
     return true;
}
</script>

<form action="badwords_update.php" name="form1" method=POST>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$i_CampusMail_New_BadWords_Settings,'') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<br>
<?=$i_CampusMail_New_BadWords_Instruction_top?><br>
<textarea name=data COLS=60 ROWS=20>
<?=$data?>
</textarea>
<br><span class="extraInfo"><?=$i_CampusMail_New_BadWords_Instruction_bottom?></span>
</blockquote>
</td></tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type='image' src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'>
<?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>