<?php
// editing by 
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

$lgroup = new libgroup($GroupID);
$groupname = $lgroup->Title;

?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (!check_int(obj.quota,obj.quota.value,'<?=$i_LinuxAccount_Alert_QuotaMissing?>')) return false;
         return true;
}
</SCRIPT>
<form name="form1" action="real_list_group_update.php" method="post" ONSUBMIT="return checkform(this)">
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_CampusMail_New_QuotaSettings,'realquota.php',$i_LinuxAccount_SetGroupQuota,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=300 border=0 cellspacing=1 cellpadding=0>
<tr><td><?=$i_CampusMail_New_BatchUpdateTarget?>:</td><td><?=$groupname?></td></tr>
<tr><td><?=$i_CampusMail_New_Quota?>:</td><td><input type=text name=quota size=10> (Mbytes)</td></tr>
<tr><td></td><td><input type=radio name=force value=0 CHECKED> <?=$i_LinuxAccount_IncreaseOnly?> <br>
<input type=radio name=force value=1> <?=$i_LinuxAccount_Reset?><br></td></tr>
</table>
</BLOCKQUOTE>

</td>
</tr>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
<input type=hidden name=GroupID value="<?=$GroupID?>">
</form>

<?php
include_once("../../templates/adminfooter.php");
?>