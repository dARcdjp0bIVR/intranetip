<?php
// editing by 
/************************************************* Change log ****************************************************
 * 2011-10-19 (Carlos): added alumni type
 *****************************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

if($plugin['imail_gamma'] === true){
$onclick = " onclick='loadUserList()'; ";
$onchange = " loadUserList(); ";
}
// $i_Campusquota_basic_identity
$user_select = "<SELECT id=UserType name=UserType onChange=\"this.form.type[0].checked=true; $onchange\">\"";
if (in_array(1, $webmail_identity_allowed))
    $user_select .= "<OPTION value=1>$i_identity_teachstaff</OPTION>\n";
if (in_array(2, $webmail_identity_allowed))
    $user_select .= "<OPTION value=2>$i_identity_student</OPTION>\n";
if (in_array(3, $webmail_identity_allowed))
    $user_select .= "<OPTION value=3>$i_identity_parent</OPTION>\n";
if ($special_feature['alumni'] && in_array(4, $webmail_identity_allowed))
    $user_select .= "<OPTION value=4>$i_identity_alumni</OPTION>\n";
$user_select .= "</SELECT>\n";;

$lg = new libgroup();
$AcademicYear = Get_Current_Academic_Year_ID();

$sql = "SELECT a.GroupID,CONCAT('(',b.CategoryName,') ',a.Title)
        FROM INTRANET_GROUP as a LEFT OUTER JOIN INTRANET_GROUP_CATEGORY as b ON a.RecordType = b.GroupCategoryID
        WHERE a.RecordType != 0 AND a.AcademicYearID = '$AcademicYear' 
        ORDER BY b.GroupCategoryID ASC, a.Title";
$groups = $lg->returnArray($sql,2);
$group_select = getSelectByArray($groups,"id=GroupID name=GroupID onChange=\"this.form.type[1].checked=true; $onchange\"");

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

?>
<form name="form1" action="group_update.php" method="post" onsubmit="return checksubmit();">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$Lang['Gamma']['MailBoxQuota'],'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<span class="extraInfo">[<span class=subTitle><?=$i_general_SelectTarget?></span>]</span>
<br><br>
<input type=radio name=type value=0 CHECKED <?=$onclick?>> <?=$i_LinuxAccount_SelectUserType?> <?=$user_select?><br>
<input type=radio name=type value=1 <?=$onclick?>> <?=$i_LinuxAccount_SelectGroup?> <?=$group_select?>
<br>
<span id="UserListSpan"></span>
<br>
<hr width=80% size=1 class="hr_sub_separator">
<br><br>
<span class="extraInfo">[<span class=subTitle><?=$i_general_SelectOption?></span>]</span>
<br><br>
<input type=radio name=force value=0 CHECKED> <?=$i_LinuxAccount_IncreaseOnly?> <br>
<input type=radio name=force value=1> <?=$i_LinuxAccount_Reset?><br>
<br>
<input type=text id=quota name=quota size=10> (Mbytes)<br>

<br>
<font color=red><?=$i_LinuxAccount_GroupSetNote?></font>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<script src="../../templates/jquery/jquery-1.3.2.min.js"></script>
<script>
<?if($plugin['imail_gamma'] === true ){?>
function loadUserList()
{
	var Type='', GroupID='';
	
	if($("input[name='type']:checked").val()==0)
		Type = $("select#UserType").val();	
	else
		GroupID = $("select#GroupID").val();		
	
	$.post(
		"ajax_load_user.php",
		{
			'Type': Type,
			'GroupID': GroupID
		},
		function(data)
		{
			$("span#UserListSpan").html(data);
			SelectAll("UserSelection")
		}
	);
}

function SelectAll(objID)
{
	$("#"+objID).children().attr("selected","selected");
}

$().ready(function(){
	loadUserList();		
});
<?}?>

function checksubmit()
{
	if($("#UserSelection").length>0 && $("#UserSelection").children(":selected").length==0)
	{
		alert("<?=$Lang['Gamma']['Warning']['PleaseSelectUser']?>");
		return false;
	}
	
	var quota = $("#quota").val();
	if(quota == '')
	{
		alert("<?=$Lang['Gamma']['Warning']['PleaseInputQuota']?>");
		return false;
	}
	else if(!IsNumeric(quota))
	{
		alert("<?=$Lang['Gamma']['Warning']['WrongQuota']?>");
		return false;
	}
	return true;
}
</script>

<?php
include_once("../../templates/adminfooter.php");
?>