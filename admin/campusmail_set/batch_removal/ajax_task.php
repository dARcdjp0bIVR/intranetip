<?php
// Editing by 
/*
 * 2015-07-28 (Carlos): Modified task "get_email_user_id", added identity and group filter.
 * 2014-11-05 (Carlos): Do not use session to login email account. Pass email and password to imap constructor. 
 * 2014-09-24 (Carlos): Created to do realtime hiding of emails that match the rules
 */
set_time_limit(60*60);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libpwm.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	intranet_closedb();
	exit();
}

switch($task)
{
	case "get_email_user_id":
		
		$li = new libdb();
		$records = array();
		$cond = "";
		if(isset($_REQUEST['identity']) && count($_REQUEST['identity'])>0){
			$cond .= " AND u.RecordType IN ('".implode("','",$_REQUEST['identity'])."') ";
			$sql = "SELECT u.UserID FROM INTRANET_USER as u WHERE u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> '' ".$cond;
			$records = $li->returnVector($sql);
		}
		
		$groupuser_records = array();
		if(isset($_REQUEST['group']) && count($_REQUEST['group'])>0){
			$sql = "SELECT u.UserID FROM INTRANET_USER as u 
					INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=u.UserID 
					WHERE u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> '' 
					 AND ug.GroupID IN ('".implode("','",$_REQUEST['group'])."')";
			$groupuser_records = $li->returnVector($sql);
				
			$records = array_merge($records,$groupuser_records);
			$records = array_values(array_unique($records));
		}
		
		if(!isset($_REQUEST['identity']) && !isset($_REQUEST['group'])){
			$sql = "SELECT u.UserID FROM INTRANET_USER as u WHERE u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> ''";
			$records = $li->returnVector($sql);
		}
		
		echo implode(',',$records);
		
	break;
	
	case "collect_mails":
		$is_debug = $sys_custom['iMailPlusBatchRemovalRealtimeActionDebug'];
		
		$li = new libdb();
		$TargetUserID = $_REQUEST['TargetUserID'];
		
		$sql = "SELECT RecordID FROM MAIL_REMOVAL_RULES WHERE RecordStatus = '1'";
		$recordIdAry = $li->returnVector($sql);
		if(count($recordIdAry)==0){
			if($is_debug){
				echo "No rules.<br />\n";
			}
			intranet_closedb();
			exit;
		}
		
		$sql = "SELECT u.UserID,u.ImapUserEmail,u.UserPassword 
				FROM INTRANET_USER as u 
				WHERE u.UserID='$TargetUserID'   
					AND u.ImapUserEmail IS NOT NULL AND u.ImapUserEmail <> '' ";
		$user_list = $li->returnResultSet($sql);
		if(count($user_list)==0){
			if($is_debug){
				echo "User $TargetUserID is not found.<br />";
			}
			intranet_closedb();
			exit;
		}
		
		$imap_user_email = $user_list[0]['ImapUserEmail'];
		$user_id_list = array();
		$user_id_list[] = $TargetUserID;
		if($intranet_authentication_method == 'HASH' || $intranet_authentication_method == 'LDAP'){
			$libpwm = new libpwm();
			$uidToPw = $libpwm->getData($user_id_list);
			$user_password = $uidToPw[$TargetUserID];
		}else{
			$user_password = $user_list[0]['UserPassword'];
		}
		if($user_password == ''){
			if($is_debug){
				echo "No password found for $imap_user_email.<br />\n";
			}
			intranet_closedb();
			exit;
		}
		
		//$_SESSION['SSV_EMAIL_LOGIN'] = $imap_user_email;
		//$_SESSION['SSV_EMAIL_PASSWORD'] = $user_password;
		//$_SESSION['SSV_LOGIN_EMAIL'] = $imap_user_email;
		$IMap = new imap_gamma(false,$imap_user_email,$user_password);
		if($IMap->ConnectionStatus != true){
			if($is_debug){
				echo "fail to connect $imap_user_mail.<br />\n";
			}
			//unset($_SESSION['SSV_EMAIL_LOGIN']);
			//unset($_SESSION['SSV_EMAIL_PASSWORD']);
			//unset($_SESSION['SSV_LOGIN_EMAIL']);
			intranet_closedb();
			exit;
		}
		
		$result = $IMap->Collect_Batch_Removal_Mails($recordIdAry);
		$IMap->close();
		if($is_debug){
			echo "$imap_user_email collect mails ".($result?"success.":"fail.");
			echo "<br />\n";
		}
		
		//unset($_SESSION['SSV_EMAIL_LOGIN']);
		//unset($_SESSION['SSV_EMAIL_PASSWORD']);
		//unset($_SESSION['SSV_LOGIN_EMAIL']);
		
	break;
}

intranet_closedb();
?>