<?php
// editing by 
/*
 * 2014-09-25 (Carlos): $sys_custom['iMailPlusBatchRemovalRealtimeAction'] - added realtime search button to hide emails in realtime (caution: it takes very long time)
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$sys_custom['iMailPlusBatchRemovalRealtimeAction'] = true;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;

$keyword = stripslashes(trim($keyword));

$li = new libdbtable($field, $order, $pageNo);

$sql = "SELECT 
			CONCAT(StartDate,' - ',EndDate) as DateRange,
			Sender,
			SubjectKeyword,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"', RecordID ,'\">') as Checkbox 
		FROM MAIL_REMOVAL_RULES  
		WHERE RecordStatus = '1' ";
if($keyword!=''){
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$sql .= " AND (SubjectKeyword LIKE '%$safe_keyword%' 
					OR Sender LIKE '%$safe_keyword%' 
					OR CONCAT(StartDate,' - ',EndDate) LIKE '%$safe_keyword%' ) ";
}

# TABLE INFO
$li->field_array = array("DateRange","SubjectKeyword","Sender");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(18,18,18,18);
$li->wrap_array = array(18,18,18,18);
$li->IsColOff = 2;

// TABLE COLUMN
$name_title = ($intranet_session_language=="en"?$i_UserEnglishName:$i_UserChineseName);
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['DateRange'])."</td>\n";
$li->column_list .= "<td width=35% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Sender'])."</td>\n";
$li->column_list .= "<td width=35% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Subject'])."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

$toolbar = "<a class=\"iconLink\" href=\"removal_settings.php\">".newIcon().$Lang['Btn']['New']."</a>\n".toolBarSpacer();
//if($sys_custom['iMailPlusBatchRemovalRealtimeAction']){
//	$toolbar.= "<a class=\"iconLink\" href=\"javascript:PerformSearch();\"><img src=\"$image_path/admin/icon_revise.gif\" border=\"0\" hspace=\"1\" vspace=\"0\" align=\"absmiddle\">".$Lang['Gamma']['RealTimeSearch']."</a>\n".toolBarSpacer();
//}
$functionbar = "<a href=\"javascript:checkEdit(document.form1,'RecordID[]','removal_settings.php')\"><img src='../../../images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','removal_settings_remove.php')\"><img src='../../../images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".htmlspecialchars($keyword,ENT_QUOTES)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$linterface = new interface_html();

## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";

include_once("../../../templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, '../index.php',$Lang['Gamma']['BatchRemoval'],'index.php',$Lang['Gamma']['RemovalSettings'],'') ?>
<?php
## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";
?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif",$xmsg) ?>

<form name="form1" method="post">
<table id="progressbarContainer" width="560" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0 0 16px 0;">
	<tbody>
		<tr>
			<td align="center">
            	<span id="progressbar" class="progressBar" style="display:none;">
					<img width="240px" id="progressbarPercentImage" src="/images/space.gif" style="width: 240px; height: 18px; background-image: url('/images/progress_bar_green.gif'); padding: 0pt; margin: 0pt; background-position: -0px 0px;" alt="0%" title="0%">
				</span>
				<span id="progressbarPercentText" style="display:none;">0%</span>
				<span id="statusMsg" style="display:none;color:green;"><?=$Lang['Gamma']['Done']?></span>
			</td>
		</tr>
	</tbody>
</table>
<div id="ContentDiv">
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td><img src="../../../images/admin/table_head0.gif" width="560" height="13" border="0"></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td>
	</tr>
	<tr>
		<td><img src=../../../images/admin/table_head1.gif width=560 height=7 border=0></td>
	</tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="../../../images/admin/table_bottom.gif" width="560" height="16" border="0"></td>
	</tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</div>
</form>
<?php
if($sys_custom['iMailPlusBatchRemovalRealtimeAction'])
{
?>
<script src="../../../templates/jquery/jquery-1.3.2.min.js"></script>
<script src="../../../templates/jquery/jquery.blockUI.js" type="text/JavaScript" language="JavaScript"></script>
<script type="text/javascript" language="JavaScript">
var userIdAry = [];
var timerId = null;
function PerformSearch()
{
	if(timerId) return;
	
	if(confirm('<?=$Lang['Gamma']['ConfirmMg']['ConfirmDoRealTimeSearch']?>'))
	{
		$.get(
			'ajax_task.php',
			{
				'task':'get_email_user_id'
			},
			function(data){
				userIdAry = data.split(',');
				DoSearch();
			}
		);
	}
}

function DoSearch()
{
	if(timerId) return;
	<?php if($sys_custom['iMailPlusBatchRemovalRealtimeActionDebug']){ ?>
	if(window.console){
		console.log(userIdAry);
		console.log('Number of user: ' + userIdAry.length);
	}
	<?php } ?>
	var total_user = userIdAry.length;
	var percent_done = 0;
	var delay = 1;
	blockMsg = '<?=$linterface->Get_Ajax_Loading_Image(1).$Lang['General']['Procesesing']?>';
	Block_Element('ContentDiv');
	
	var timerTask = function(){
		if(userIdAry.length == 0){
			percent_done = 100;
			UpdateProgress(percent_done);
			UnBlock_Element('ContentDiv');
			timerId = null;
			return;
		}
		var cur_user_id = userIdAry.shift();
		<?php if($sys_custom['iMailPlusBatchRemovalRealtimeActionDebug']){ ?>
		if(window.console){
			console.log('Number of user left is ' + userIdAry.length);
		}
		<?php } ?>
		UpdateProgress(percent_done);
		$.post(
			'ajax_task.php',
			{
				'task':'collect_mails',
				'TargetUserID':cur_user_id 
			},
			function(data){
				percent_done = Number(((total_user - userIdAry.length) / total_user) * 100).toFixed(2);
				UpdateProgress(percent_done);
				timerId = setTimeout(timerTask,delay);
			}
		);
	};
	timerId = setTimeout(timerTask,delay);
}

function UpdateProgress(percent_done)
{
	var bar = $('#progressbar');
	var barContainer = $('#progressbarContainer');
	var barText = $('#progressbarPercentText');
	var barImage = $('#progressbarPercentImage');
	var statusMsg =$('#statusMsg');
	
	barText.html(percent_done + '%');
	barImage.css('background-position','-'+(240-(240*(percent_done/100)))+'px 0px');
	barImage.attr('alt',percent_done+'%');
	barImage.attr('title',percent_done+'%');
	bar.show();
	barText.show();
	if(percent_done >= 100){
		statusMsg.show();
		
		UnBlock_Element('ContentDiv');
		setTimeout(
			function(){
				timerId = null;
				bar.hide();
				barText.hide();
				statusMsg.hide();
			},
			2000
		);
	}
}
</script>
<?php 
}
?>
<?php
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>