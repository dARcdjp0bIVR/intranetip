<?php
// editing by 
/*
 * 2012-09-21 (Carlos): added column "No .of Mails"
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}

$order = ($order == 1) ? 1 : 0;

$record_status = $record_status == 1?1:2;

$keyword = stripslashes(trim($keyword));

$li = new libdbtable($field, $order, $pageNo);

$li->db_db_query("SET SESSION group_concat_max_len = 1000000");

$sql = "SELECT 
			ArrivalDate,
			FromEmail,
			Subject,
			COUNT(RecordID) as NumOfMail,
			CONCAT('<input type=\"checkbox\" name=\"RecordID[]\" value=\"', GROUP_CONCAT(RecordID SEPARATOR ',') ,'\">') as Checkbox 
		FROM MAIL_REMOVAL_LOG 
		WHERE RecordStatus = '$record_status' 
		GROUP BY RuleID, FromEmail, Subject ";
if($keyword!=''){
	$safe_keyword = $li->Get_Safe_Sql_Like_Query($keyword);
	$sql .= " AND (FromEmail LIKE '%$safe_keyword%' 
					OR Subject LIKE '%$safe_keyword%' 
					OR ArrivalDate LIKE '%$safe_keyword%') ";
}

# TABLE INFO
$li->field_array = array("ArrivalDate","FromEmail","Subject","NumOfMail");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(18,18,18,18,18,18);
$li->wrap_array = array(18,18,18,18,18,18);
$li->IsColOff = 2;

// TABLE COLUMN
$name_title = ($intranet_session_language=="en"?$i_UserEnglishName:$i_UserChineseName);
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Date'])."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Sender'])."</td>\n";
$li->column_list .= "<td width=60% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['Subject'])."</td>\n";
$li->column_list .= "<td width=5% class=tableTitle>".$li->column($pos++, $Lang['Gamma']['NumberOfMails'])."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

$RecordStatusArr[] = array(2,$Lang['Gamma']['FilteredMessages']);
$RecordStatusArr[] = array(1,$Lang['Gamma']['DeletedMessages']);
$view_selection = getSelectByArray($RecordStatusArr, " id='record_status' name='record_status' onchange='document.form1.submit();' ", $record_status, $all=0, $noFirst=1, $FirstTitle="", $ParQuoteValue=1);
$toolbar = "&nbsp;".$Lang['Btn']['View'].":&nbsp;".$view_selection;
$functionbar = "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','view_result_update.php?action=delete')\">".$Lang['Btn']['Delete']."</a>&nbsp;\n";
if($record_status==2) $functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','view_result_update.php?action=undo','".$Lang['Gamma']['ConfirmMsg']['UndoMessages']."')\">".$Lang['Btn']['Undo']."</a>&nbsp;\n";
$searchbar .= $toolbar;
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".htmlspecialchars($keyword,ENT_QUOTES)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


include_once("../../../templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, '../index.php',$Lang['Gamma']['BatchRemoval'],'index.php',$Lang['Gamma']['FilteredRemovedMails'],'') ?>
<?php
## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";
?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $xmsg) ?>
<form name="form1" method="post">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="../../../images/admin/table_head0.gif" width="560" height="13" border="0"></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $searchbar); ?></td>
	</tr>
	<tr>
		<td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td>
	</tr>
	<tr>
		<td><img src=../../../images/admin/table_head1.gif width=560 height=7 border=0></td>
	</tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td><img src="../../../images/admin/table_bottom.gif" width="560" height="16" border="0"></td>
	</tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>