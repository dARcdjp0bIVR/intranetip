<?php
// editing by 
/*
 * 2014-11-17 (Carlos): Changed send email as optional. 
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$RecordID = (is_array($RecordID) && sizeof($RecordID)>0)?$RecordID[0]:$RecordID;

$EditOrNew = ($RecordID=="")? $Lang['Btn']['New'] : $Lang['Btn']['Edit'];

$IMap = new imap_gamma($skipLogin = 1);
$settings = ($RecordID!="")? $IMap->Get_Batch_Removal_Settings($RecordID): array();

if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $quota_title = $i_CampusMail_New_DefaultQuota;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $quota_title = $i_Campusquota_quota;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

$linterface = new interface_html();

?>
<script src="../../../templates/jquery/jquery-1.3.2.min.js"></script>
<?=$linterface->Include_DatePicker_JS_CSS()?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
    var error_cnt = 0;
    var date_warning = $('span#date_warning');
    var subject_warning = $('span#subject_warning');
    var sender_warning = $('span#sender_warning');
    date_warning.parent().parent().hide();
    subject_warning.parent().parent().hide();
    sender_warning.parent().parent().hide();
    date_warning.html('');
    
    var start_date = $.trim($('input#StartDate').val());
    var end_date = $.trim($('input#EndDate').val());
    var subject = $.trim($('input#SubjectKeyword').val());
    var sender = $.trim($('input#Sender').val());
    var start_date_valid = true, end_date_valid = true;
    
    if(start_date=='' || !check_date(start_date)){
    	error_cnt++;
    	start_date_valid = false;
    	date_warning.append('<?=$Lang['Gamma']['Warning']['PleaseInputValidStartDate']?>');
    	date_warning.parent().parent().show();
    }
    
    if(end_date=='' || !check_date(end_date)){
    	error_cnt++;
    	end_date_valid = false;
    	if(start_date_valid)
    		date_warning.html('<?=$Lang['Gamma']['Warning']['PleaseInputValidEndDate']?>');
    	else
    		date_warning.append('<br>*'+'<?=$Lang['Gamma']['Warning']['PleaseInputValidEndDate']?>');
    	date_warning.parent().parent().show();
    }
    
    if(start_date_valid && end_date_valid){
    	if(compareDate(start_date,end_date)>0){
	    	error_cnt++;
	    	date_warning.html('<?=$Lang['Gamma']['Warning']['PleaseInputValidDateRange']?>');
	    	date_warning.parent().parent().show();
    	}
    }
    
    if(subject==''){
    	error_cnt++;
    	subject_warning.parent().parent().show();
    }
    
//    if(sender=='' || !check_email(sender)){
//    	error_cnt++;
//    	sender_warning.parent().parent().show();
//    }
    
    if(error_cnt==0)
   		obj.submit();
}

function check_date(date_string)
{
    var err = 0;
    d_a = date_string;
    if (d_a.length != 10) err = 1;
    d_b = d_a.substring(0, 4);        // year
    d_c = d_a.substring(4, 5);                                // '-'
    d_d = d_a.substring(5, 7);        // month
    d_e = d_a.substring(7, 8);                                // '-'
    d_f = d_a.substring(8, 10);        // day
    
    // basic error checking
    if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
    if(d_c != '-') err = 1;
    if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
    if(d_e != '-') err = 1;
    if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
    // advanced error checking
    // months with 30 days
    if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
    // february, leap year
    if(d_d==2){ // feb
            var d_g = parseInt(d_b/4)
            if(isNaN(d_g)) err = 1;
            if(d_f>29) err = 1;
            if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
    }
    if(err==1){
            return false;
    }else{
            return true;
    }
}

function check_email(email_string)
{
	var re = /^([\w_\.\-])+\@(([\w_\.\-])+\.)+([\w]{2,4})+$/;	
    if (re.test(email_string))
    	return true;
    else
    	return false;
}
</SCRIPT>

<form name="form1" action="removal_settings_update.php" method="post" onSubmit="checkform(this);return false;">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, '../index.php',$Lang['Gamma']['BatchRemoval'],'./index.php',$Lang['Gamma']['RemovalSettings'],'./removal_settings_list.php', $EditOrNew,'') ?>
<?php
## Return Message
$msgarr = explode("|=|",$msg);
if (count($msgarr)>1)
{
	if($msgarr[0]=="1"){
		$xmsg = "<font color=green>".$msgarr[1]."</font>";
	}else if($msgarr[0]=="0"){
		$xmsg = "<font color=red>".$msgarr[1]."</font>";
	}else
		$xmsg = "";
}else
	$xmsg = "";
?>
<?= displayTag("$img_tag", $xmsg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td>
			<blockquote>
			<br>
			<!--<span style="border:2px dashed #CCCCCC;padding:4px"><?=$Lang['Gamma']['Warning']['LeaveAllFieldsBlankToDisable']?></span>-->
			<p>
				<table cellspacing="0" cellpadding="5" border="0" width="100%">
				<tbody>
					<tr>
						<td width="30%" style="vertical-align: bottom;" class="tableTitle_new"><u><?=$Lang['Gamma']['Criteria']?></u></td>
						<td width="70%" style="vertical-align: bottom;" class="tableTitle_new"><u><?=$Lang['Gamma']['Settings']?></u></td>
					</tr>
					<tr>
						<td style="vertical-align: middle;"><font color="red">*</font><?=$Lang['Gamma']['DateRange']?>:</td>
						<td>
							<?=$Lang['General']['From']?><?=$linterface->GET_DATE_PICKER("StartDate",$settings[0]['StartDate'])?> <?=$Lang['General']['To']?> <?=$linterface->GET_DATE_PICKER("EndDate",$settings[0]['EndDate'])?><span style="color: gray;"> (YYYY-MM-DD)</span>
							<div style="display:none"><font color="red">*<span id="date_warning"></span></font></div>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;"><font color="red">*</font><?=$Lang['Gamma']['SubjectKeywordKeyPhrase']?>:</td>
						<td>
							<input type="text" value="<?=htmlspecialchars(trim($settings[0]['SubjectKeyword']),ENT_QUOTES)?>" maxlength="255" size="50" id="SubjectKeyword" name="SubjectKeyword" class="text">
							<div style="display:none"><font color="red">*<span id="subject_warning"><?=$Lang['Gamma']['Warning']['PleaseInputSubjectKeyword']?></span></font></div>
						</td>
					</tr>
					<tr>
						<td style="vertical-align: middle;"><!--<font color="red">*</font>--><?=$Lang['Gamma']['SendersMailAddress']?>:</td>
						<td>
							<input type="text" value="<?=trim($settings[0]['Sender'])?>" maxlength="255" size="50" id="Sender" name="Sender" class="text">
							<div style="display:none"><font color="red">*<span id="sender_warning"><?=$Lang['Gamma']['Warning']['PleaseInputValidEmailAddress']?></span></font></div>
						</td>
					</tr>
				</tbody>
				</table>
			</p>
			<?=str_replace('*','<font color="red">*</font>',$Lang['General']['RequiredField'])?>
			</BLOCKQUOTE>
		</td>
	</tr>

	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1></td>
	</tr>
	<tr>
		<td align="right">
			<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 			<?= btnReset() ?>
		</td>
	</tr>
</table>
<input type="hidden" id="RecordID" name="RecordID" value="<?=$settings[0]['RecordID']?>" />
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
