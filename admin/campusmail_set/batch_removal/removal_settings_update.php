<?php
// editing by 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
intranet_opendb();

## Only For iMail Gamma/Plus
if($plugin['imail_gamma']!==true){
	header("Location: ../index.php");
	exit();
}

$IMap = new imap_gamma($skipLogin = 1);

$DataArray=array();
$DataArray['RecordID']=$RecordID;
$DataArray['StartDate']=$StartDate;
$DataArray['EndDate']=$EndDate;
$DataArray['SubjectKeyword']=stripslashes($SubjectKeyword);
$DataArray['Sender']=stripslashes($Sender);

$success = $IMap->Set_Batch_Removal_Settings($DataArray);

if(trim($RecordID)==""){
	// New
	$msg = $success?$Lang['General']['ReturnMessage']['AddSuccess']:$Lang['General']['ReturnMessage']['AddUnsuccess'];
}else{
	// Edit
	$msg = $success?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

intranet_closedb();
header("Location: removal_settings_list.php?msg=".urlencode($msg));
?>