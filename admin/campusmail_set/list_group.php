<?php
//editing by 
/************************************* Changes *********************************************
 * 2013-12-16 (Carlos): $sys_custom['iMailPlus']['EmailAliasName'] - if INTRANET_USER.ImapUserLogin is set, use it as email account name
 * 2013-12-04 (Carlos): iMail plus - Added [Percentage] table field
 * 2012-11-21 (Carlos): split users into several chunks to get quota with Get_Quota_Info_List() to prevent timeout
 * 2011-12-05 (Carlos): modified array key userlogin to lowercase for comparison
 *******************************************************************************************/
set_time_limit(0);
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if ($GroupID == "")
{
	header("Location: index.php");
    exit();
}

intranet_opendb();

$lgroup = new libgroup($GroupID);

if ($special_feature['imail'])
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
else $mail_set_title = $i_adminmenu_fs_campusmail;

?>
<SCRIPT language=Javascript>
function removeAccount(id)
{
         if (confirm('<?=$i_CampusMail_New_alert_RemoveAccount?>'))
         {
             location.href = "removeaccount.php?type=1&target=<?=$UserType?>&ClassName=<?=$ClassName?>&uid="+id;
         }
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, 'index.php',$Lang['Gamma']['UserUsageRightsAndQuota'],'quotaindex.php',$i_LinuxAccount_DisplayQuota,'list.php',$lgroup->Title,'') ?>
<?= displayTag("head_imailsettings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><a class=iconLink href="batch_process.php?type=2&target=<?=$GroupID?>"><img src="<?=$image_path?>/admin/icon_setting.gif" border=0 hspace=1 vspace=0 align=absmiddle><?=$i_LinuxAccount_BatchProcess?></a></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle><?="$i_UserName ($i_UserLogin)"?></td>
<td class=tableTitle><?=$i_CampusMail_New_ExternalAllow?></td>
<?php 
if($plugin['imail_gamma']===true)
{
?>
<td class=tableTitle><?=$Lang['General']['Status']?></td>
<?php
}
?>
<td class=tableTitle><?=$i_LinuxAccount_Quota?> (Mbytes)</td>
<td class=tableTitle><?=$i_LinuxAccount_UsedQuota?> (Mbytes)</td>
<?php if($plugin['imail_gamma'] && $sys_custom['iMailPlus']['QuotaAlert']){ ?>
<td class="tableTitle"><?=$Lang['General']['Percentage']?></td>	
<?php } ?>
</tr>
<?
if($plugin['imail_gamma']===true){
	include_once("../../includes/imap_gamma.php");
	$IMap = new imap_gamma(true);
	
	$quota_alert = $IMap->getQuotaAlertSetting();
	
	$name_field = getNameFieldWithClassNumberByLang("b.");
	$allowed_type_list = implode(",",$webmail_identity_allowed);
	$sql = "SELECT a.UserID, b.UserLogin, $name_field, b.IMapUserEmail, c.ACL ".($sys_custom['iMailPlus']['EmailAliasName']?",b.ImapUserLogin":"")."  
	        FROM INTRANET_USERGROUP as a 
			LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID 
			LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as c ON a.UserID = c.UserID 
	        WHERE a.GroupID = $GroupID AND b.UserID IS NOT NULL
	                   AND b.RecordType IN ($allowed_type_list)
	        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
	$users = $lgroup->returnArray($sql,5);
	
	$user_logins = array();
	for($i=0;$i<sizeof($users);$i++){
		list($uid,$login,$name,$email,$acl) = $users[$i];
		//if(trim($email)!='')
		if($sys_custom['iMailPlus']['EmailAliasName'] && $users[$i]['ImapUserLogin']!=''){
			$user_logins[] = strtolower($users[$i]['ImapUserLogin'])."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		}else{
			$user_logins[] = strtolower($login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		}
	}
	//$quota = $IMap->Get_Quota_Info_List($user_logins);
	$quota = array();
	$userlogin_chunks = array_chunk($user_logins,200); // too many would wait long time to get function return, page will timeout
	for($i=0;$i<count($userlogin_chunks);$i++){
		$tmp_quota = $IMap->Get_Quota_Info_List($userlogin_chunks[$i]);
		$quota = array_merge($quota,$tmp_quota);
		echo " "; // give output to prevent page timeout
		ob_flush();
		flush();
	}
	
	for($i=0;$i<sizeof($users);$i++){
		list($uid,$login,$name,$email,$acl) = $users[$i];
		$IntranetLogin = $login;
		if($sys_custom['iMailPlus']['EmailAliasName'] && $users[$i]['ImapUserLogin']!=''){
			$login = strtolower($users[$i]['ImapUserLogin'])."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		}else{
			$login = strtolower($login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
		}
		$hasAccessRight = ($acl==1 || $acl==3);
		//$hasAccessRight = (trim($email)!='');
		$isAccountEnable = (trim($email)!='');
		
		$css = ($i%2? "":"2");
		if ($hasAccessRight)
	        $external_allowed = $Lang['General']['Yes'];
	    else $external_allowed = $Lang['General']['No'];
	    
	    if($isAccountEnable)
	    	$account_status = $Lang['Status']['Activate'];
	    else
	    	$account_status = $Lang['Status']['Suspend'];
	    
	    if($quota[$login]['AccountExist']){
	    	if($quota[$login]['QuotaTotal']==0){
	    		$user_quota = "$i_LinuxAccount_NoLimit";
	    		$percentage_used = $Lang['General']['EmptySymbol'];
	    	}else{
	    		$user_quota = $quota[$login]['QuotaTotal'];
	    		$percentage_used = sprintf("%.2f %%", $quota[$login]['QuotaUsed'] / $user_quota * 100);
	    		
	    		if($quota_alert != 0 && floatval($percentage_used) >= floatval($quota_alert)){
	    			$percentage_used = '<span style="color:red">'.$percentage_used.'</span>';
	    		}
	    	}
	    	$user_used = $quota[$login]['QuotaUsed'];
	    }else{
	    	$user_quota = "N/A";
	    	$user_used = "N/A";
	    	$percentage_used = "N/A";
	    }
	   	
	    $edit_link = "<a href=\"user_set.php?type=2&target=".$GroupID."&loginName=".$IntranetLogin."\">".$name." (".$login.") <img src=\"$image_path/edit_icon.gif\" border=0></a>";
	     $remove_link = "<a class=functionlink href=\"javascript:removeAccount($uid)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
	     if ($user_quota == "N/A")
	     {
	         echo "<tr class=tableContent$css><td>$edit_link $remove_link</td><td>$external_allowed</td><td>$account_status</td><td colspan=3 align=center>$i_LinuxAccount_NoAccount</td></tr>\n";
	     }
	     else
	     {
	         echo "<tr class=tableContent$css><td>$edit_link $remove_link</td><td>$external_allowed</td><td>$account_status</td><td>$user_quota</td><td>$user_used</td>".($sys_custom['iMailPlus']['QuotaAlert']?"<td>$percentage_used</td>":"")."</tr>\n";
	     }
	}
	
	if($IMap->IMapCache !== false) $IMap->IMapCache->Close_Connect();
}else{
# Retrieve Quota
$lwebmail = new libwebmail();
$list = $lwebmail->getQuotaTable("iMail");
for ($i=0; $i<sizeof($list); $i++)
{
     list($login,$used,$soft,$hard) = $list[$i];
     $quota[strtolower($login)] = array($used,$soft);
}
$name_field = getNameFieldWithClassNumberByLang("b.");
$allowed_type_list = implode(",",$webmail_identity_allowed);
$sql = "SELECT a.UserID, b.UserLogin, $name_field, c.ACL
        FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as c ON a.UserID = c.UserID
        WHERE a.GroupID = $GroupID AND b.UserID IS NOT NULL
                   AND b.RecordType IN ($allowed_type_list)
        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
$users = $lgroup->returnArray($sql,4);
for ($i=0; $i<sizeof($users); $i++)
{
     list($uid, $login, $name, $acl) = $users[$i];
     $IntranetLogin = $login;
     
     $login = strtolower($login);
     if ($lwebmail->login_type=="email" && $lwebmail->login_domain != "")
     {
	     $login = $login . "@" . $lwebmail->login_domain;
     }	
     
     $hasAccessRight = ($acl==1 || $acl==3);
          
     if($lwebmail->actype == "postfix"){
     	$user_used = round($quota[$login][0]/1024/1024,2);
     	$user_quota = round($quota[$login][1]/1024/1024,2);
     }else{
     	$user_used = $quota[$login][0];
     	$user_quota = $quota[$login][1];
     }
     
     if (!isset($quota[$login]))
     {
         $user_quota = "N/A";
     }
     else if ($user_quota == 0)
     {
         $user_quota = "$i_LinuxAccount_NoLimit";
     }
     if (!isset($quota[$login]))
     {
         $user_used = "N/A";
     }
     $css = ($i%2? "":"2");
     $en_login = urlencode($login);
     if ($hasAccessRight)
     {
         $external_allowed = "yes";
     }
     else $external_allowed = "no";
     
     $edit_link = "<a href=\"user_set.php?type=2&target=".$GroupID."&loginName=".$IntranetLogin."\">".$name." (".$login.") <img src=\"$image_path/edit_icon.gif\" border=0></a>";
     $remove_link = "<a class=functionlink href=\"javascript:removeAccount($uid)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
     if ($user_quota == "N/A")
     {
         echo "<tr class=tableContent$css><td>$edit_link $remove_link</td><td>$external_allowed</td><td colspan=2 align=center>$i_LinuxAccount_NoAccount</td></tr>\n";
     }
     else
     {
         echo "<tr class=tableContent$css><td>$edit_link $remove_link</td><td>$external_allowed</td><td>$user_quota</td><td>$user_used</td></tr>\n";
     }

}

}
?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</td>
</tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>