<?php
// editing by : 
/********************************* Change log *****************************************
 * 2018-05-25 (Carlos) : $sys_custom['iMailPlus']['ForceRemovalForiMailArchive'] open old iMail force removal for iMail plus client to clean up DB emails in order to release disk space. 
 * 2013-12-04 (Carlos) : iMail Settings - Added [Quota Alert Settings]
 * 2012-06-12 (Carlos) : Disable Days in trash/spam setting until server script is done
 * 2011-03-21 (Carlos) : added Batch Removal menu item
 **************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libwebmail.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

$filecontent = trim(get_file_content("$intranet_root/file/cm_removal.txt"));
$remove_allowed = ($filecontent == 1);

$filecontent = trim(get_file_content("$intranet_root/file/cm_removal_search.txt"));
$cond_search_allowed = ($filecontent == 1);

$lwebmail = new libwebmail();

$mail_server_quota_control = $special_feature['imail'] && $lwebmail->has_webmail && ($lwebmail->type==3);
$has_imail = $special_feature['imail'];
if ($has_imail)
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

?>

<?= displayNavTitle($i_adminmenu_fs, '', $mail_set_title, '') ?>
<?= displayTag("$img_tag", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<?if($plugin['imail_gamma']===true)
{
    echo displayOption( $Lang['Gamma']['GeneralSetting'], 'access.php',1,
    					$Lang['Gamma']['UserUsageRightsAndQuota'], 'quotaindex.php',1,
                        $Lang['Gamma']['BlockUserToSend'], 'ban.php', 1,
                        $i_CampusMail_New_BadWords_Settings, 'badwords.php',$imail_feature_allowed['bad_words_filter'],
                        $i_campusmail_recipient_selection_control,'recipient_mgnt.php',0,
                        $i_spam['Setting'],'spam/',$webmail_info['bl_spam'],
                        $i_CampusMail_Admin_DayInTrashSettings,'day_in_trash.php',1,
                        $i_campusmail_forceRemoval, 'force_remove.php', $remove_allowed,
                        $i_campusmail_forceRemoval.' ('.$Lang['Gamma']['iMailArchive'].')', 'remove.php', $sys_custom['iMailPlus']['ForceRemovalForiMailArchive'],
                        $Lang['Gamma']['BatchRemoval'],'batch_removal/',1,
                        $Lang['Gamma']['QuotaAlertSettings'], 'quota_alert_settings.php', $sys_custom['iMailPlus']['QuotaAlert'] 
                        //,'Clean Account Mails(Suggested feature, not released)','clean_account/',1
                        ) ;                        
}
else
{
	echo displayOption( $i_campusmail_usage_setting, 'usage.php',1,
                   		$Lang['Gamma']['GeneralSetting'], 'access.php', 1,
                   		$Lang['Gamma']['UserUsageRightsAndQuota'] , 'quotaindex.php',$has_imail ,
                        $i_campusmail_policy, 'policy.php', 1,
                        $Lang['Gamma']['BlockUserToSend'], 'ban.php', 1,
                        $i_campusmail_forceRemoval, 'remove.php', $remove_allowed,
                        $i_CampusMail_New_BadWords_Settings, 'badwords.php',$imail_feature_allowed['bad_words_filter'],
                        $i_CampusMail_New_RemoveBySearch,'cond_search.php',$cond_search_allowed,
                        $i_campusmail_recipient_selection_control,'recipient_mgnt.php',0,
                        $i_spam['Setting'],'spam/',$webmail_info['bl_spam'],
                        $i_CampusMail_Admin_DayInTrashSettings,'day_in_trash.php',$sys_custom['iMail_Admin_DayInTrashSettings']
                                ) ;
}           
?>                    
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>
