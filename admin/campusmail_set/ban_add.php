<?php
# editing by 
/********************************************** Change log *****************************************************
 * 2011-10-20 (Carlos): Added Identity Alumni
 ***************************************************************************************************************/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/fileheader_admin.php");
intranet_opendb();

# retrieve group category

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0>--{$button_select}--</option>\n";
$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByGroup'],ENT_QUOTES)."'>";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}
$x1 .= "</optgroup>";

$x1 .= "<optgroup label='".htmlspecialchars($Lang['iMail']['FieldTitle']['ByIdentity'],ENT_QUOTES)."'>";
$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
if($special_feature['alumni']) $x1 .= "<option value=-4 ".(($CatID==-4)?"SELECTED":"").">$i_identity_alumni</option>\n";
$x1 .= "</optgroup>";

if($plugin['imail_gamma'] === true){
	$sql = "SELECT s.MailBoxID, s.MailBoxName, IF(p.DisplayName IS NULL OR p.DisplayName = '',s.MailBoxName,p.DisplayName) as FinalDisplayName FROM MAIL_SHARED_MAILBOX as s LEFT JOIN MAIL_PREFERENCE as p ON p.MailBoxName = s.MailBoxName ORDER BY FinalDisplayName ";
	$SharedMailBoxes = $li->returnArray($sql);
	if(sizeof($SharedMailBoxes)>0){
		$x1 .= "<optgroup label='".$Lang['General']['Others']."'>";
		$x1 .= "<option value='-10' ".($CatID == -10?"SELECTED":"").">".$Lang['SharedMailBox']['SharedMailBox']."</option>";
		$x1 .= "</optgroup>";
	}
}

$x1 .= "</select>";

if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);
     $x2  = "<select name=ChooseGroupID[] size=10 multiple>\n";
     if(sizeof($row)>0){
	     for($i=0; $i<sizeof($row); $i++){
	          $GroupCatID = $row[$i][0];
	          $GroupCatName = $row[$i][1];
	          if ($GroupCatID != $GroupID)
	          {
	              $x2 .= "<option value=$GroupCatID";
	              for($j=0; $j<sizeof($ChooseGroupID); $j++){
	                  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
	              }
	              $x2 .= ">$GroupCatName</option>\n";
	          }
	     }
     }else{
     	$x2 .= "<option value=0>".$Lang['General']['NoRecordAtThisMoment']."</option>\n";
     }
     $x2 .= "</select>\n";
}

if($CatID == -10){
	$x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
    if(sizeof($SharedMailBoxes)>0){
     	for($i=0; $i<sizeof($SharedMailBoxes); $i++){
     		$mailboxname = $SharedMailBoxes[$i]['MailBoxName'];
     		if(stristr($mailboxname,"@")) $mailboxname = substr($mailboxname, 0, strpos($mailboxname, "@"));
     		$displayname = $SharedMailBoxes[$i]['FinalDisplayName'];
     		$x3 .= "<option value=".htmlspecialchars($mailboxname,ENT_QUOTES).">".htmlspecialchars($displayname,ENT_QUOTES)." (".htmlspecialchars($SharedMailBoxes[$i]['MailBoxName'],ENT_QUOTES).")"."</option>\n";
     	}
    }else{
     	$x3 .= "<option value=0>".$Lang['General']['NoRecordAtThisMoment']."</option>\n";
    }
    $x3 .= "</select>\n";
}else if($CatID < 0){
   # Return users with identity chosen
    $selectedUserType = 0-$CatID;
    
	if($selectedUserType==3){ // Parent
		if($plugin['imail_gamma'] === true)
			$imap_cond = " AND a.ImapUserEmail IS NOT NULL AND a.ImapUserEmail <> '' ";
		else
			$imap_cond = "";
		$NameField = "IF(c.EnglishName != '' OR c.EnglishName IS NOT NULL, IFNULL(CONCAT('(',c.ClassName,'-',c.ClassNumber,') ', ".getNameFieldByLang2('c.').",'".$Lang['iMail']['FieldTitle']['TargetParent']."',' (',".getNameFieldByLang2('a.').",')'),".getNameFieldByLang2('a.')."), ".getNameFieldByLang2('a.').")";
		$sql = " SELECT DISTINCT a.UserID as UserID, $NameField as UserName, a.UserLogin as UserLogin FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION AS b ON (a.UserID = b.ParentID) LEFT OUTER JOIN INTRANET_USER AS c ON (b.StudentID = c.UserID) WHERE a.RecordType = '3' AND a.RecordStatus = '1' $imap_cond ORDER BY IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
		$row = $li->returnArray($sql,3);
	}else{ // Teacher or Student
		if($plugin['imail_gamma'] === true)
			$imap_cond = " AND ImapUserEmail IS NOT NULL AND ImapUserEmail <> '' ";
		else
			$imap_cond = "";
		$NameField = getNameFieldWithClassNumberByLang();
		$sql = "SELECT DISTINCT UserID, $NameField as UserName, UserLogin FROM INTRANET_USER WHERE RecordType = '$selectedUserType' AND RecordStatus = '1' $imap_cond ORDER BY IFNULL(ClassName,''), IFNULL(ClassNumber,0), EnglishName ";
		$row = $li->returnArray($sql,3);
	}
	
    $x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
    if(sizeof($row)>0){
    	for($i=0; $i<sizeof($row); $i++)
   			$x3 .= "<option value=".htmlspecialchars($row[$i]['UserLogin'],ENT_QUOTES).">".$row[$i]['UserName']." (".htmlspecialchars($row[$i]['UserLogin'],ENT_QUOTES).")"."</option>\n";
    }else{
    	$x3 .= "<option value=0>".$Lang['General']['NoRecordAtThisMoment']."</option>\n";
    }
    $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID) && sizeof($ChooseGroupID)>0 && $ChooseGroupID[0]!=0) {
	 if($plugin['imail_gamma'] === true)
		$imap_cond = " AND a.ImapUserEmail IS NOT NULL AND a.ImapUserEmail <> '' ";
	 else
	 	$imap_cond = "";
     $sql = "SELECT DISTINCT a.UserID, ".getNameFieldWithClassNumberByLang("a.")." as UserName, a.UserLogin FROM INTRANET_USER AS a INNER JOIN INTRANET_USERGROUP AS b ON (a.UserID = b.UserID) WHERE a.RecordStatus = 1 AND b.GroupID  IN (".implode(",",$ChooseGroupID).") $imap_cond ORDER BY a.RecordType, IFNULL(a.ClassName,''), IFNULL(a.ClassNumber,0), a.EnglishName";
     $row = $li->returnArray($sql,3);
     
     $x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
     if(sizeof($row)>0){
     	for($i=0; $i<sizeof($row); $i++)
     		$x3 .= "<option value=".htmlspecialchars($row[$i]['UserLogin'],ENT_QUOTES).">".$row[$i]['UserName']." (".htmlspecialchars($row[$i]['UserLogin'],ENT_QUOTES).")"."</option>\n";
     }else{
     	$x3 .= "<option value=0>".$Lang['General']['NoRecordAtThisMoment']."</option>\n";
     }
     $x3 .= "</select>\n";
}
?>

<script language="JavaScript">
function Add()
{
	var target_field = window.opener.document.form1.elements['<?=$fieldname?>'];
	var selected_users = document.form1.elements['ChooseUserID[]'];
	var result = target_field.value;
	
	for(i=0;i<selected_users.length;i++){
		if(selected_users.options[i].value!=0 && selected_users.options[i].selected)
			result += selected_users.options[i].value + '\n';
	}
	target_field.value = result;
}

function SelectAll(name)
{
    var obj = document.form1.elements[name];
    for (i=0; i<obj.length; i++)
    {
      obj.options[i].selected = true;
    }
}

function expandGroup()
{
    var obj = document.form1;
    checkOption(obj.elements['ChooseGroupID[]']);
    obj.submit();
}

</script>

<form name="form1" action="ban_add.php?fieldname=<?=$fieldname?>" method="post">
	<?= displayNavTitle($Lang['iMail']['FieldTitle']['Choose'], '') ?>
	
	<p style="padding-left:20px">
		<table width="422" border="0" cellpadding="0" cellspacing="0">
			<tr><td><img src="<?=$PATH_WRT_ROOT?>images/admin/pop_head.gif" width="422" height="19" border=0></td></tr>
			<tr>
				<td style="background-image: url(<?=$PATH_WRT_ROOT?>images/admin/pop_bg.gif);" >
					<table width="422" border="0" cellpadding="0" cellspacing="0">
						<tr><td><img src="<?=$PATH_WRT_ROOT?>images/admin/pop_bar.gif" width="422" height="16" border="0"></td></tr>
					</table>
					<table width="422" border="0" cellpadding="10" cellspacing="0">
						<tr>
							<td>
								<p>
								<?php echo $i_frontpage_campusmail_select_category; ?>:
								<br>
								<?php echo $x1; ?>
								<?php if($CatID!=0 && $CatID > 0) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_group; ?>:
									<br>
									<?php echo $x2; ?>
									<a href="javascript:expandGroup()"><img src="<?=$PATH_WRT_ROOT?>images/admin/button/s_btn_expand_<?=$intranet_session_language?>.gif" border="0"></a>
									<a href="javascript:SelectAll('ChooseGroupID[]')"><img src="<?=$PATH_WRT_ROOT?>images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
									
								<?php
									if(isset($ChooseGroupID) && sizeof($ChooseGroupID)>0 && $ChooseGroupID[0]!=0) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_user; ?>:
									<br>
									<?php echo $x3; ?>
									<a href="javascript:Add()"><img src="<?=$PATH_WRT_ROOT?>images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
									<a href="javascript:SelectAll('ChooseUserID[]')"><img src="<?=$PATH_WRT_ROOT?>images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
							</td>
						</tr>
					</table>
	
				</td>
			</tr>
			<tr><td><img src="<?=$PATH_WRT_ROOT?>images/admin/pop_bottom.gif" width="422" height="18" border="0"></td></tr>
			<tr>
				<td align=center height="40" style="vertical-align:bottom">
					<a href="javascript:self.close()"><img src="<?=$PATH_WRT_ROOT?>images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
				</td>
			</tr>
		</table>
	<input type="hidden" name="GroupID" value="<?=$GroupID?>">
	<input type="hidden" name="fieldname" value="<?=$fieldname?>">
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/filefooter.php");
?>