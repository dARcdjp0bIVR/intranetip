<?php
// editing by 
/********************************************* Changes ***************************************************
 * 2012-08-14 (Carlos): iMail plus - Force removal
 **********************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libinterface.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../includes/imap_gamma.php");

// $i_Campusquota_basic_identity
$filecontent = trim(get_file_content("$intranet_root/file/cm_removal.txt"));
$remove_allowed = ($filecontent == 1);
if (!$remove_allowed)
{
     header("Location: index.php");
     exit();
}
if ($special_feature['imail'])
{
    $mail_set_title = $i_CampusMail_New_iMail_Settings;
    $img_tag = "head_imailsettings_$intranet_session_language.gif";
}
else
{
    $mail_set_title = $i_adminmenu_fs_campusmail;
    $img_tag = "head_campusmail_set_$intranet_session_language.gif";
}

$linterface = new interface_html();
$IMap = new imap_gamma($skipLogin = 1);
$access_right  = $IMap->access_right;

// Identity type
$usertype_select = '<SELECT name="TargetUserType">'."\n";
if($access_right[0]==1){
	$usertype_select .= '<OPTION value="1">'.$i_identity_teachstaff.'</OPTION>'."\n";
}
if($access_right[1]==1){
	$usertype_select .= '<OPTION value="2"">'.$i_identity_student.'</OPTION>'."\n";
}
if($access_right[2]==1){
	$usertype_select .= '<OPTION value="3">'.$i_identity_parent.'</OPTION>'."\n";
}
if($access_right[3]==1 && $special_feature['alumni']){
    $usertype_select .= '<OPTION value="4">'.$i_identity_alumni.'</OPTION>'."\n";
}
$usertype_select .= '</SELECT>';

include_once("../../templates/adminheader_setting.php");
?>
<script src="../../templates/jquery/jquery-1.3.2.min.js" type="text/JavaScript" language="JavaScript"></script>
<script src="../../templates/jquery/jquery.blockUI.js" type="text/JavaScript" language="JavaScript"></script>
<script src="../../templates/script.js" type="text/JavaScript" language="JavaScript"></script>
<script type="text/JavaScript" language="JavaScript">
function SubmitForm(formObj)
{
	var is_valid = true;
	var StartDate = $.trim($('input#StartDate').val());
	var EndDate = $.trim($('input#EndDate').val());
	$('input#StartDate').val(StartDate);
	$('input#EndDate').val(EndDate);
	
	if($('#ActionType2').is(':checked')){
		if(StartDate=='' || !check_date_without_return_msg(document.getElementById('StartDate'))){
			$('input#StartDate').focus();
			is_valid = false;
			alert('<?=$Lang['General']['InvalidDateFormat']?>');
			return false;
		}
		
		if(EndDate=='' || !check_date_without_return_msg(document.getElementById('EndDate'))){
			$('input#EndDate').focus();
			is_valid = false;
			alert('<?=$Lang['General']['InvalidDateFormat']?>');
			return false;
		}
		
		if(StartDate > EndDate){
			$('input#StartDate').focus();
			is_valid = false;
			alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			return false;
		}
	}
	if(is_valid){
		blockMsg = 'Removing mails, please wait...';
		Block_Element('ContentDiv');
		var bar = $('#progressbar');
		var barContainer = $('#progressbarContainer');
		var barText = $('#progressbarPercentText');
		var barImage = $('#progressbarPercentImage');
		var statusMsg =$('#statusMsg');
		var percent_done = 0;
		document.getElementById('iframeProcess').src = "force_remove_update.php";
		formObj.submit();
		barText.html(percent_done + '%');
		barImage.css('background-position','-'+(240-(240*(percent_done/100)))+' 50%');
		barImage.attr('alt',percent_done+'%');
		barImage.attr('title',percent_done+'%');
		bar.show();
		barText.show();
		statusMsg.hide();
	}
}

// callback function for force_remove_update.php
function UpdateProgress(percent_done)
{
	var bar = $('#progressbar');
	var barContainer = $('#progressbarContainer');
	var barText = $('#progressbarPercentText');
	var barImage = $('#progressbarPercentImage');
	var statusMsg =$('#statusMsg');
	
	barText.html(percent_done + '%');
	barImage.css('background-position','-'+(240-(240*(percent_done/100)))+' 50%');
	barImage.attr('alt',percent_done+'%');
	barImage.attr('title',percent_done+'%');
	if(percent_done >= 100){
		statusMsg.show();
		
		UnBlock_Element('ContentDiv');
		setTimeout(
			function(){
				bar.hide();
				barText.hide();
				statusMsg.hide();
			},
			1000
		);
	}
}
</script>
<form id="form1" name="form1" action="force_remove_update.php" method="post" target="iframeProcess" onsubmit="SubmitForm(this);return false;">
<?= displayNavTitle($i_admintitle_fs, '', $mail_set_title, 'index.php',$i_campusmail_forceRemoval,'') ?>
<?= displayTag("$img_tag", $msg) ?>
<table id="progressbarContainer" width="560" cellspacing="0" cellpadding="0" border="0" align="center" style="margin:0 0 16px 0;">
	<tbody>
		<tr>
			<td align="center">
            	<span id="progressbar" class="progressBar" style="display:none;">
					<img width="240px" id="progressbarPercentImage" src="/images/space.gif" style="width: 240px; height: 18px; background-image: url('/images/progress_bar_green.gif'); padding: 0pt; margin: 0pt; background-position: -0px 50%;" alt="0%" title="0%">
				</span>
				<span id="progressbarPercentText" style="display:none;">0%</span>
				<span id="statusMsg" style="display:none;color:green;"><?=$Lang['Gamma']['Done']?></span>
			</td>
		</tr>
	</tbody>
</table>
<div id="ContentDiv">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td>
			<blockquote>
				<p>
				<?=$i_LinuxAccount_SelectUserType.': '.$usertype_select?>
				<br><br>
				<?=$Lang['Gamma']['Action'].":"?><br><br>
				<input type="radio" name="ActionType" id="ActionType1" value="1" checked="checked" /><label for="ActionType1"><?=$Lang['Gamma']['CleanMailbox']?></label>
				<br>
				<input type="radio" name="ActionType" id="ActionType2" value="2" />
				<label for="ActionType2">
					<?=$i_campusmail_forceRemoval_instruction1?>:
				</label>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$i_campusmail_forceRemoval_Start?>:
				<input type="text" id="StartDate" name="StartDate" value="<?=date('Y-m-d')?>" size="10"> <span class="extraInfo">(yyyy-mm-dd)</span>
				<br>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$i_campusmail_forceRemoval_End?>:
				<input type="text" id="EndDate" name="EndDate" value="<?=date('Y-m-d')?>" size="10"> <span class="extraInfo">(yyyy-mm-dd)</span>
				<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$Lang['Gamma']['ForceRemovalRemarks']?>
			</BLOCKQUOTE>
		</td>
	</tr>
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1></td>
	</tr>
	<tr>
		<td align="right">
			<input type="image" src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border='0'>
 			<?= btnReset() ?>
		</td>
	</tr>
</table>
</div>
<iframe name="iframeProcess" id="iframeProcess" width="0" height="0" style="display:none" src=""></iframe>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>