<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

$lf = new libfilesystem();
$file_content = "";
if(isset($target))
{
	$file_content = implode(",", $target);
}

# Write to file
$li = new libfilesystem();
$location = $intranet_root."/file/award_scheme";
$li->folder_new($location);
$file = $location."/admin_user.txt";
$success = $li->file_write($file_content, $file);

intranet_closedb();
header("Location: admin_setting.php?msg=2");
?>
