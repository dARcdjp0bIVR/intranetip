<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");

if ($clearall)
{
    intranet_opendb();
    $ldb = new libdb();
    $sql = "DELETE FROM INTRANET_HOMEWORK";
    if($from!='' AND $to!='')
	    $sql .=" WHERE startdate between '$from' and '$to'";
    $ldb->db_db_query($sql);
    intranet_closedb();
}


$li = new libfilesystem();
$li->file_write(htmlspecialchars(trim($disable)), $intranet_root."/file/homework_disable.txt");
$li->file_write(htmlspecialchars(trim($fixstart)), $intranet_root."/file/homework_startdate.txt");
$li->file_write(htmlspecialchars(trim($prights)), "$intranet_root/file/homework_parent.txt");
$li->file_write(htmlspecialchars(trim($disablesubject)), "$intranet_root/file/homework_bysubject.txt");
$li->file_write(htmlspecialchars(trim($disableteacher)), "$intranet_root/file/homework_byteacher.txt");
$li->file_write(htmlspecialchars(trim($nonteach)), "$intranet_root/file/homework_nonteaching.txt");
$li->file_write(htmlspecialchars(trim($restrict)), "$intranet_root/file/homework_restrict.txt");
$li->file_write(htmlspecialchars(trim($disabletaught)), "$intranet_root/file/homework_taught.txt");
$li->file_write(htmlspecialchars(trim($subleader)), "$intranet_root/file/homework_leader.txt");
$li->file_write(htmlspecialchars(trim($allowexport)), "$intranet_root/file/homework_export.txt");
$li->file_write(htmlspecialchars(trim($pastallowed)), "$intranet_root/file/homework_past.txt");
$li->file_write(htmlspecialchars(trim($usehomeworktype)), "$intranet_root/file/homework_usetype.txt");
$li->file_write(htmlspecialchars(trim($usehomeworkhandin)), "$intranet_root/file/homework_usehandin.txt");
$li->file_write(htmlspecialchars(trim($useteachercollecthomework)), "$intranet_root/file/homework_teachercollects.txt");

header("Location: settings.php?msg=2");
?>