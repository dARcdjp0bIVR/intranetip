<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field !== "0") $field = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     default: $field = 1; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;

$sql  = "SELECT
               TypeName, DisplayOrder,
               CONCAT('<input type=checkbox name=TypeID[] value=', TypeID ,'>')
          FROM
               INTRANET_HOMEWORK_TYPE
          WHERE
               RecordStatus = $filter
          ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("TypeName", "DisplayOrder");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=60% class=tableTitle>".$li->column($pos++, $i_Homework_HomeworkType)."</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column($pos++, $i_general_DisplayOrder)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("TypeID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'homeworktype_new.php')\">".newIcon()."$button_new</a>\n";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'TypeID[]','homeworktype_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_general_inactive</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_general_active</option>\n";
$searchbar .= "</select>\n";
#$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
#$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_homework, 'settings.php', "$button_edit $i_Homework_HomeworkType",'' ) ?>
<?= displayTag("head_homework_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>