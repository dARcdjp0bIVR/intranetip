<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();

$sql = "DELETE FROM INTRANET_LOGIN_SESSION WHERE UNIX_TIMESTAMP(StartTime) < UNIX_TIMESTAMP('$RemoveFrom')+86399";
$li->db_db_query($sql);
intranet_closedb();
header("Location: index.php?msg=3");
?>