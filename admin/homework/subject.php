<?php
include("../../includes/global.php");
include("../../lang/email.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../includes/libdb.php");
include("../../includes/libdbtable.php");
include("../../templates/adminheader_setting.php");

?>
<script language="javascript">
function checkform(obj){
        return true;
}
</script>


<?php

intranet_opendb();
# TABLE SQL
$keyword = trim($keyword);
$field = 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;

$sql = "SELECT SubjectName,
        CONCAT('<input type=checkbox name=SubjectID[] value=',SubjectID,'>')
        FROM INTRANET_SUBJECT
        WHERE
        RecordStatus = $filter AND
        (SubjectName like '%$keyword%')
";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("SubjectName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_subject;
$li->column_array = array(0);
$li->IsColOff = 2;
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=100% class=tableTitle>".$li->column(0, $i_Subject_name)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("SubjectID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('new.php')\">".newIcon()."$button_new</a>";
$functionbar= "";
if ($filter==1)
{
    $functionbar .= "<a href=\"javascript:checkEdit(document.form1,'SubjectID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
    $functionbar .= "<a href=\"javascript:checkSuspend(document.form1,'SubjectID[]','suspend.php')\"><img src='/images/admin/button/t_btn_pause_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
}
else
{
    $functionbar .= "<a href=\"javascript:checkActivate(document.form1,'SubjectID[]','activate.php')\"><img src='/images/admin/button/t_btn_active_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
    $functionbar .= "<a href=\"javascript:checkRemove(document.form1,'SubjectID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_suspended</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_activated</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_homework, 'index.php', $i_admintitle_fs_homework_subject, '') ?>
<?= displayTag("head_homework_subject_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
</form>


<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>