<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");
intranet_opendb();

if (is_array($TypeID)) $TypeID = $TypeID[0];

$li = new libdb();
$sql = "SELECT TypeName, DisplayOrder, RecordStatus FROM INTRANET_HOMEWORK_TYPE WHERE TypeID = '$TypeID'";
$array = $li->returnArray($sql,3);
$TypeName = $array[0][0];
$DisplayOrder = $array[0][1];
$RecordStatus = $array[0][2];
$RecordStatus0 = ($RecordStatus == 0) ? "CHECKED" : "";
$RecordStatus1 = ($RecordStatus == 1) ? "CHECKED" : "";

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.TypeName, "<?php echo $i_alert_pleasefillin.$i_Homework_HomeworkType; ?>.")) return false;
}
</script>

<form name="form1" action="homeworktype_edit_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_homework, 'settings.php', "$button_edit $i_Homework_HomeworkType",'javascript:history.back()', $button_edit, '' ) ?>
<?= displayTag("head_homework_$intranet_session_language.gif", $msg) ?>

<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_Homework_HomeworkType; ?>:</td><td><input class=text type=text name=TypeName size=50 maxlength=100 value='<?=$TypeName?>'></td></tr>
<tr><td align=right><?php echo $i_general_DisplayOrder; ?>:</td><td><input class=text type=text name=DisplayOrder size=50 maxlength=100 value='<?=$DisplayOrder?>'></td></tr>
<tr><td align=right><?php echo $i_EventRecordStatus; ?>:</td><td><input class=text type=radio name=RecordStatus value=0 <?=$RecordStatus0?>><?=$i_general_inactive?> <input class=text type=radio name=RecordStatus value=1 <?=$RecordStatus1?>><?=$i_general_active?> </td></tr>
</table>
<br><br>
</blockquote>

<input type=hidden name=TypeID value="<?=$TypeID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>
