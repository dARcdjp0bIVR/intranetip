<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

if (is_array($SubjectID)) $SubjectID = $SubjectID[0];

$li = new libdb();
$sql = "SELECT SubjectName, RecordStatus FROM INTRANET_SUBJECT WHERE SubjectID = '$SubjectID'";
$array = $li->returnArray($sql,2);
$SubjectName = $array[0][0];
$RecordStatus = $array[0][1];

?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.SubjectName, "<?php echo $i_alert_pleasefillin.$i_Subject_name; ?>.")) return false;
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_fs, '', $i_admintitle_fs_homework, 'index.php', $i_admintitle_fs_homework_subject, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_homework_subject_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_Subject_name; ?>:</td><td><input class=text type=text name=SubjectName size=50 maxlength=100 value='<?=$SubjectName?>'></td></tr>
</table>
<br><br>
</blockquote>

<input type=hidden name=SubjectID value="<?=$SubjectID?>">
<input type=hidden name=RecordStatus value="<?=$filter?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?> 
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>