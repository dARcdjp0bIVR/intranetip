<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

# New Varialbes
$i_adminmenu_sc_eclass_update = "eClass Update";
$i_manual_update = "Manual Update";
$i_auto_update = "Auto Update";

?>


<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_group_setting_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>

Updated eClass to latest version<br>
Latest IntranetIP Version: intranetI-P-testing.tar.gz.v41.20090209_r5<br>
Current IntranetIP Version: intranetI-P-testing.tar.gz.v41.20090209_r5<br>
<br>
Latest eClass Version: eclass3-1.tar.gz.v41.20081208_r7<br>
Current eClass Version: eclass3-1.tar.gz.v41.20081208_r7<br>

<br><br>
1. Backup current program code: [<font color=green>Completed!</font>]<br>
2. Backup MySQL Database: [<font color=green>Completed!</font>]<br>
3. Extract latest program code: [<font color=green>Completed!</font>]<br>
4. Update eClass system: [<font color=green>Completed!</font>]<br>
5. Update eClass Database: [<font color=green>Completed!</font>]<br>
6. Finalization: [<font color=green>Completed!</font>]
<br><br>
<font color=green>All Done!</font>
<br><br>
<b>Important</b>: If you find any system error after eClass updates, please contact eClass Customer Service Center at +852 35204217 or support@broadlearning.com<br>


</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");

?>