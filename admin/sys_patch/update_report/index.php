<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

# New Varialbes
$i_adminmenu_sc_eclass_update = "eClass Update";
$i_manual_update = "Manual Update";
$i_auto_update = "Auto Update";
$i_update_report = "Update Report";

?>


<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_eclass_update, '', $i_update_report, '') ?>
<?= displayTag("head_group_setting_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>

Latest IntranetIP Version: intranetI-P-testing.tar.gz.v41.20090209_r5<br>
Current IntranetIP Version: <font color=red>intranetI-P-testing.tar.gz.v41.20081217_r22</font><br>
<br><br>
Latest eClass Version: eclass3-1.tar.gz.v41.20081208_r7<br>
Current eClass Version: <font color=green>eclass3-1.tar.gz.v41.20081208_r7</font><br>
<br><br>
Your system have been updated to latest version already. <-- If no new update available<br><br>
The Ref. ID can be used for reporting problem to our customer support center. <br><br>
You also can check the meaning of each Ref. ID through the following website:<br>
http://support.broadlearning.com/ref_id.php


<br><br>
<table width=100%>
<tr><td width=80>Date/Time</td><td>Events</td><td>Status</td><td>Ref. ID</td></tr>
<tr><td><font size=-2>2008-02-12 03:00</td><td><font size=-2>Auto Update: intranetI-P-testing.tar.gz.v41.20090209_r5 to intranetI-P-testing.tar.gz.v41.20090209_r6</td><td><font color="red">Fail</font></td><td>326</td></tr>
<tr><td><font size=-2>2008-02-11 03:00</td><td><font size=-2>Manual Update: intranetI-P-testing.tar.gz.v41.20090209_r4 to intranetI-P-testing.tar.gz.v41.20090209_r5</td><td><font color="green">Success</font></td><td>300</td></tr>

</table>


</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");

?>