<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libadminjob.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$ladminjob = new libadminjob();

?>

<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_adminjob, '') ?>
<?= displayTag("head_adminjob_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption($i_AdminJob_Announcement, 'announcement.php', $special_feature['announcement_approval'],
                  $i_Profile_AdminUserMgmt, 'stdprofile.php',0
                                ) ?>

</blockquote>
</td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>