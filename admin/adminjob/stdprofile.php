<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libadminjob.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$ladminjob = new libadminjob();
$toolbar = "<a class=iconLink href=\"std_profile_admin_new.php\">".newIcon()."$button_add</a>";

?>

<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_adminjob, 'index.php',$i_Profile_AdminUserMgmt,'') ?>
<?= displayTag("head_adminjob_$intranet_session_language.gif", $msg) ?>
<SCRIPT language=Javascript>
function removeAdminUser(id)
{
         if (confirm('<?=$i_AdminJob_Announcement_RemoveAdmin?>'))
         {
             location.href = "choose/remove.php?type=2&aid="+id;
         }
}
function editAdminUser(id)
{
         location.href = "std_profile_admin_edit.php?aid="+id;

}

</SCRIPT>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $toolbar; ?></td></tr>
<tr>
<td>
<?=$ladminjob->displayProfileAdminTable(0)?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>