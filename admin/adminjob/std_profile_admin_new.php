<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libadminjob.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$ladminjob = new libadminjob();
$lclass = new libclass();
$classList = $lclass->getClassList();

?>
<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_adminjob, 'index.php',$i_Profile_AdminUserMgmt,'javascript:history.back()',$button_new,'') ?>
<?= displayTag("head_adminjob_$intranet_session_language.gif", $msg) ?>

<SCRIPT language=Javascript>

</SCRIPT>
<form name=form1 action=std_profile_admin_new_update.php method=POST>
<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0 align=center>
<tr><td><?=$Lang['AdminJob_Description_SetAdmin']?></td></tr>
<tr><td><?=$ladminjob->displayNonProfileAdminUserInput()?></td></tr>
<tr><td><br><span class="subTitle"><?=$i_Profile_AdminACL?> : </span><br>
<input type=checkbox name=class_attend value=1 > <?=$i_Profile_Attendance?>
<input type=checkbox name=class_merit value=1 > <?=$i_Profile_Merit?>
<br>
<input type=checkbox name=class_service value=1 > <?=$i_Profile_Service?>
<input type=checkbox name=class_activity value=1 > <?=$i_Profile_Activity?>
<input type=checkbox name=class_award value=1 > <?=$i_Profile_Award?>
<br>
<input type=checkbox name=class_assessment value=1 > <?=$i_Profile_Assessment?>
<input type=checkbox name=class_file value=1 > <?=$i_Profile_Files?>

</td>
</tr>
<tr><td><br><span class="subTitle"><?=$i_AdminJob_AdminLevel?> : </span><br>
<input type=radio name=adminlevel value=0 CHECKED> <?=$i_AdminJob_AdminLevel_Normal." ($i_Profile_AdminLevel_Description_Normal)"?> <br>
<input type=radio name=adminlevel value=1> <?=$i_AdminJob_AdminLevel_Full. " ($i_Profile_AdminLevel_Description_Full)"?></td></tr>
<tr><td><br><span class="subTitle"><?=$Lang['General']['Class']?> : </span><br>
<?
$lastLvl = $classList[0][2];
for ($i=0; $i<sizeof($classList); $i++)
{
     list($class_id, $class_name, $class_lvl) = $classList[$i];

     if ($i!=0 && $lastLvl != $class_lvl)
     {
         echo "<br>\n";
         $lastLvl = $class_lvl;
     }
     echo "<input type=checkbox name=targetClass[] value=$class_id> $class_name &nbsp;";
}
?>
</td>
</tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="admin.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>


<?php
include_once("../../templates/adminfooter.php");
?>