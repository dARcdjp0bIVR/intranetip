<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$values_class = "";
$delim_class = "";
$values_acl = "";
$delim_acl = "";
$values = "";
$delim = "";
for ($i=0; $i<sizeof($targetUserID); $i++)
{
     # INTRANET_ADMIN_USER
     $target = $targetUserID[$i];
     $values .= "$delim('$target',2,'$adminlevel',now(),now())";
     $delim = ",";

     # INTRANET_PROFILE_ADMIN_ACL
     $values_acl .= "$delim_acl ('$target','$class_attend','$class_merit',
                     '$class_service','$class_activity','$class_award','$class_assessment',
                     '$class_file',now(),now())";
     $delim_acl = ",";
     # INTRANET_PROFILE_ADMIN_CLASS
     for ($j=0; $j<sizeof($targetClass); $j++)
     {
          $values_class .= "$delim_class('$target','".$targetClass[$j]."',now(),now())";
          $delim_class = ",";
     }
}
$sql = "INSERT IGNORE INTO INTRANET_ADMIN_USER (UserID, RecordType, AdminLevel, DateInput, DateModified)
               VALUES $values";
$li->db_db_query($sql);

$sql = "INSERT IGNORE INTO INTRANET_PROFILE_ADMIN_ACL (UserID, AllowedAttendance, AllowedMerit,
               AllowedService, AllowedActivity, AllowedAward, AllowedAssessment, AllowedFile,
               DateInput,DateModified) VALUES $values_acl";
$li->db_db_query($sql);

$sql = "INSERT IGNORE INTO INTRANET_PROFILE_ADMIN_CLASS (UserID,ClassID,DateInput,DateModified)
               VALUES $values_class";
$li->db_db_query($sql);

intranet_closedb();
header("Location: stdprofile.php?msg=1");
?>