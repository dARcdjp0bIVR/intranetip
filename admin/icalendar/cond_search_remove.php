<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

if (isset($eventID) && sizeof($eventID)>0) {
	$type = "event";
    $list = implode(",",$eventID);
	
    # delete event entry and all related data
    $sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID IN ($list)";
    $li->db_db_query($sql);

    $sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID IN ($list)";
    $li->db_db_query($sql);
    
    $sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID IN ($list)";
    $li->db_db_query($sql);
} else if (isset($calID) && sizeof($calID)>0) {
	$type = "calendar";
	$list = implode(",",$calID);
	
	 # delete calendar and all related data
    $sql = "DELETE FROM CALENDAR_CALENDAR WHERE CalID IN ($list)";
    $li->db_db_query($sql);

    $sql = "DELETE FROM CALENDAR_CALENDAR_VIEWER WHERE CalID IN ($list)";
    $li->db_db_query($sql);
    
    # retrieve all events belongs to the calendars
	$sql = "SELECT EventID FROM CALENDAR_EVENT_ENTRY WHERE CalID IN ($list)";
	$result = $li->returnVector($sql);
	
	if (sizeof($result) > 0) {
		$eventList = implode(",", $result);
	    
	    # delete event entry and all related data belongs to the deleted calendars
	    $sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID IN ($eventList)";
	    $li->db_db_query($sql);
	
	    $sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID IN ($eventList)";
	    $li->db_db_query($sql);
	    
	    $sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID IN ($eventList)";
	    $li->db_db_query($sql);
    }
}

header("Location: cond_search_result.php?msg=3&newsearch=1&type=$type&keyword=".str_replace('"', "&quot;", stripslashes($keyword)));
?>