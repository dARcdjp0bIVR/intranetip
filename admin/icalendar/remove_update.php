<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");

intranet_opendb();
$li = new libdb();

if (isset($start) && isset($end)) {
	$ts_start = strtotime($start);
	$ts_end = strtotime($end) + 24*60*60;
	
	$sql  = "SELECT EventID FROM CALENDAR_EVENT_ENTRY ";
	$sql .= "WHERE UNIX_TIMESTAMP(EventDate) >= '$ts_start' AND UNIX_TIMESTAMP(EventDate) < '$ts_end'";
	$result = $li->returnVector($sql);
	
	if (sizeof($result) > 0) {
		$list = implode(",", $result);
		
		$sql = "DELETE FROM CALENDAR_EVENT_ENTRY WHERE EventID IN ($list)";
		$li->db_db_query($sql);
		
		$sql = "DELETE FROM CALENDAR_REMINDER WHERE EventID IN ($list)";
		$li->db_db_query($sql);
		
		$sql = "DELETE FROM CALENDAR_EVENT_USER WHERE EventID IN ($list)";
		$li->db_db_query($sql);
	}
	intranet_closedb();
	header("Location: remove.php?msg=3");
} else {
	header("Location: remove.php");
}
?>