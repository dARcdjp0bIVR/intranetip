<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../lang/ical_lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();
include_once("../../includes/libicalendar.php");

$iCal = new libicalendar;
$systemSettings = $iCal->systemSettings;
?>
<script type="text/javascript">
	function checkform(form) {
		form.submit();
	}
</script>
<?= displayNavTitle($i_adminmenu_fs, '', $i_Calendar_Admin_Setting, 'index.php', $i_general_BasicSettings, '') ?>
<?= displayTag("$img_tag", $msg) ?>
<form name="form1" method="post" onsubmit="checkform(this)" action="settings_update.php">
	<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
		<tr>
			<td>
				<blockquote>
					<table border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td align="right" nowrap><?=$i_Calendar_Settings_PreferredView?>:</td>
							<td>
								<select name="prefer_view">
									<option value="monthly"<?php if($systemSettings["PreferredView"]=="monthly") echo " selected=\"selected\"" ?>><?=$iCalendar_NewEvent_Repeats_Monthly?></option>
									<option value="weekly"<?php if($systemSettings["PreferredView"]=="weekly") echo " selected=\"selected\"" ?>><?=$iCalendar_NewEvent_Repeats_Weekly?></option>
									<option value="daily"<?php if($systemSettings["PreferredView"]=="daily") echo " selected=\"selected\"" ?>><?=$iCalendar_NewEvent_Repeats_Daily?></option>
								</select>
							</td>
						</tr>
						<tr>
							<td align="right" nowrap><?=$i_Calendar_Settings_TimeFormat?>:</td>
							<td>
								<input type="radio" name="time_format" id="time_format1" value="12"<?php if($systemSettings["TimeFormat"]=="12") echo " checked=\"checked\"" ?>/> 
								<label for="time_format1"><?=$i_Calendar_Settings_12hr?></label>&nbsp;&nbsp;
								<input type="radio" name="time_format" id="time_format2" value="24"<?php if($systemSettings["TimeFormat"]=="24") echo " checked=\"checked\"" ?> /> 
								<label for="time_format2"><?=$i_Calendar_Settings_24hr?></label>
							</td>
						</tr>
						<tr>
							<td align="right" nowrap><?=$i_Calendar_Settings_WorkingHours?>:</td>
							<td>
								<?=$i_Profile_From?>
								<select name="working_hours_start">
									<?php
										foreach (range(0, 23) as $hour) {
									?>
									<option value="<?=$hour?>"<?php if($systemSettings["WorkingHoursStart"]=="$hour") echo " selected=\"selected\"" ?>><?=sprintf("%02d",$hour).":00"?></option>
									<?php
										}
									?>
								</select>
								<?=$i_Profile_To?>
								<select name="working_hours_end">
									<?php
										foreach (range(0, 23) as $hour) {
									?>
									<option value="<?=$hour?>"<?php if($systemSettings["WorkingHoursEnd"]=="$hour") echo " selected=\"selected\"" ?>><?=sprintf("%02d",$hour).":00"?></option>
									<?php
										}
									?>
								</select>
							</td>
						</tr>
						<tr>
							<td align="right" nowrap><?=$i_Calendar_Settings_DisableRepeat?>:</td>
							<td>
								<input type="radio" name="disable_repeat" id="disable_repeat1" value="yes"<?php if($systemSettings["DisableRepeat"]=="yes") echo " checked=\"checked\"" ?> /> 
								<label for="disable_repeat1"><?=$i_general_yes?></label>&nbsp;&nbsp;
								<input type="radio" name="disable_repeat" id="disable_repeat2" value="no"<?php if($systemSettings["DisableRepeat"]=="no") echo " checked=\"checked\"" ?> /> 
								<label for="disable_repeat2"><?=$i_general_no?></label>
							</td>
						</tr>
						<tr>
							<td align="right" nowrap><?=$i_Calendar_Settings_DisableGuest?>:</td>
							<td>
								<input type="radio" name="disable_guest" id="disable_guest1" value="yes"<?php if($systemSettings["DisableGuest"]=="yes") echo " checked=\"checked\"" ?> /> 
								<label for="disable_guest1"><?=$i_general_yes?></label>&nbsp;&nbsp;
								<input type="radio" name="disable_guest" id="disable_guest2" value="no"<?php if($systemSettings["DisableGuest"]=="no") echo " checked=\"checked\"" ?> /> 
								<label for="disable_guest2"><?=$i_general_no?></label>
							</td>
						</tr>
					</table>
				</blockquote>
			</td>
		</tr>
		<tr>
			<td height="22" style="vertical-align:bottom"><hr size=1></td>
		</tr>
		<tr>
			<td align="right">
				<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
	 			<?= btnReset() ?>
			</td>
		</tr>
	</table>
</form>
<?
include_once("../../templates/adminfooter.php");
intranet_closedb();
?>