<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../lang/ical_lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it
function calendarCallback1(date, month, year) {
	if (String(month).length == 1) {
		month = '0' + month;
	}
	if (String(date).length == 1) {
		date = '0' + date;
	}
	dateValue =year + '-' + month + '-' + date;
	document.forms['form1'].start.value = dateValue;
}

function calendarCallback2(date, month, year) {
	if (String(month).length == 1) {
		month = '0' + month;
	}
	if (String(date).length == 1) {
		date = '0' + date;
	}
	dateValue =year + '-' + month + '-' + date;
	document.forms['form1'].end.value = dateValue;
}
// -->
</script>
<?= displayNavTitle($i_adminmenu_fs, '', $i_Calendar_Admin_Setting, '', $i_Calendar_ForceRemoval, '') ?>
<?= displayTag("$img_tag", $msg) ?>
<form name="form1" action="remove_update.php" method="post">
<blockquote>
<table width="560" border="0" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td colspan="2"><?=$i_Calendar_ForceRemoval_Instruction?>:</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td align="right"><?=$i_general_startdate?>:</td>
		<td>
			<input type="text" name="start" value="<?=date('Y-m-d')?>" size="12" maxlength="10"> 
			<script language="JavaScript" type="text/javascript">
			<!--
			startCal = new dynCalendar('startCal', 'calendarCallback1', '/templates/calendar/images/');
			//-->
			</script>
			<span class="extraInfo">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr>
		<td align="right"><?=$i_general_enddate?>:</td>
		<td>
			<input type="text" name="end" value="<?=date('Y-m-d')?>" size="12" maxlength="10"> 
			<script language="JavaScript" type="text/javascript">
			<!--
			endCal = new dynCalendar('endCal', 'calendarCallback2', '/templates/calendar/images/');
			//-->
			</script>
			<span class="extraInfo">(yyyy-mm-dd)</span>
		</td>
	</tr>
	<tr><td colspan="2" height="22" style="vertical-align:bottom"><hr size="1"></td></tr>
	<tr>
		<td colspan="2" align="right">
			<input type="image" src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border='0'>
 			<?= btnReset() ?>
		</td>
	</tr>
</table>
</blockquote>
</form>
<?
include_once("../../templates/adminfooter.php");
?>