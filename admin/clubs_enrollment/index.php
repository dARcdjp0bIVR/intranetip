<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

# $li_menu = new libaccount()

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_adminmenu_sc_clubs_enrollment_settings, '') ?>
<?= displayTag("head_club_enrollment_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=300>
<blockquote>
<? if ($plugin['eEnrollment']) { ?>
<?= displayOption($i_Discipline_System_Control_UserACL, 'userset.php', 1) ?>
<? } else { ?>
<?= displayOption($i_ClubsEnrollment_BasicSettings, 'basic.php', 1,
                                $i_ClubsEnrollment_StudentRequirement, 'student.php', 1,
                                $i_ClubsEnrollment_QuotaSettings, 'group.php', 1) ?>
<? } ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>