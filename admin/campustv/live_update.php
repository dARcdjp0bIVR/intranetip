<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampustv.php");

$lcampustv = new libcampustv();

$special_feature['portal'] = true;

$content = "";
$i = 0;
while ($i < sizeof($range))
{
       if ($range[$i]=="" && $title[$i]=="")
           break;
       $content .= $range[$i].":::".$title[$i]."\n";
       $i++;
}

$li = new libfilesystem();
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($LiveURL))), $lcampustv->live_file);
$li->file_write(intranet_htmlspecialchars(trim(stripslashes($content))), $lcampustv->programme_file);

if ($special_feature['portal'])
{
	$live_show = ($live_show) ? "1" : "0";
	$live_auto_play = ($live_auto_play) ? "1" : "0";
	$live_width = (int) $live_width;
	if ($live_width<1)
	{
		$live_width = $lcampustv->live_width;
	}
	$live_height = (int) $live_height;
	if ($live_height<1)
	{
		$live_height = $lcampustv->live_height;
	}
	$live_portal_content =
"$live_show
$live_width
$live_height
$live_auto_play";
	$li->file_write(intranet_htmlspecialchars(trim(stripslashes($live_portal_content))), $lcampustv->live_portal_file);
}


header("Location: live.php?msg=2");
?>