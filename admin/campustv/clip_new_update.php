<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libftp.php");
intranet_opendb();

$li = new libdb();


$ClipName = intranet_htmlspecialchars(trim($ClipName));
$Description = intranet_htmlspecialchars(trim($Description));

if ($ClipType == 1)             # Link
{
    $URL = "$ClipURL";
    $secure = 0;
}
else
{
    $filepath = $ClipFile;
    $filename = $ClipFile_name;

    $lftp = new libftp();
    $lftp->host = $tv_ftp_host;
    if ($tv_host_type == "") $tv_host_type = "Windows";
    $lftp->host_type = $tv_host_type; #"Windows";

    if ($secure != 1) $secure = 0;
    $ftp_username = ($secure==1 && $tv_secure_ftp_user != ""? $tv_secure_ftp_user : $tv_ftp_user);
    $ftp_passwd = ($secure==1 && $tv_secure_ftp_user != ""? $tv_secure_ftp_passwd : $tv_ftp_passwd);

    if (!$lftp->connect($ftp_username,$ftp_passwd))
    {
         header("Location: clip_new.php");
         exit();
    }

    # Get random path
    $target_path = substr(md5(intranet_random_passwd(10)),0,8);


    $current_dir = $lftp->pwd();
    $lftp->mkdir($target_path);
    $lftp->upload($filepath,"$current_dir/$target_path",$filename,1);

    $base_path = ($secure == 1 && $tv_secure_ftp_user != "")? $tv_secure_base_path : $tv_base_path;

    $URL = "$base_path/$target_path/$filename";
    $lftp->close();

}

$sql = "INSERT INTO INTRANET_TV_MOVIECLIP (Title, Description,ClipURL,FilePath,FileName,Recommended, isFile,IsSecure, RecordType,RecordStatus,DateInput,DateModified)
        VALUES ('$ClipName','$Description','$URL','$target_path','$filename', '1', '$ClipType','$secure','$ChannelID','2',now(),now())";
#        echo "1. $sql\n";
$li->db_db_query($sql);

intranet_closedb();
header("Location: movieclip.php?msg=1");
?>