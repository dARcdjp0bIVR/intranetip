<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$list = implode(",",$PollingID);
$sql = "DELETE FROM INTRANET_TV_POLLING_RESULT WHERE PollingID IN ($list)";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_TV_POLLING_OPTION WHERE PollingID IN ($list)";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_TV_POLLING WHERE PollingID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: polling.php?msg=3");
?>