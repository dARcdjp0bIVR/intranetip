<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

for ($i=0; $i<$num; $i++)
{
     $id = ${"PollID$i"};
     if ($id != "")
         $pollIDs[] = $id;
}

if (sizeof($pollIDs)!=0)
{
    $sql = "INSERT INTO INTRANET_TV_POLLING (Question, Reference, DateStart, DateEnd, DateInput, DateModified)
            VALUES ('$PollingName','$Reference','$DateStart','$DateEnd',now(),now())";
    $li->db_db_query($sql);
    $pollingID = $li->db_insert_id();

    $values = "";
    $delimiter = "";
    $options_sql = "INSERT INTO INTRANET_TV_POLLING_OPTION (PollingID, OptionText, OptionOrder, DateInput, DateModified) VALUES ";

    for ($i=0; $i<sizeof($pollIDs); $i++)
    {
         $id = $pollIDs[$i];
         $pos = $i+1;
         $values .= "$delimiter ($pollingID, '$id','$pos',now(),now())";
         $delimiter = ",";
    }
    $options_sql .= $values;
    #echo $options_sql;
    $li->db_db_query($options_sql);

}


intranet_closedb();
header("Location: polling.php?msg=1");
?>