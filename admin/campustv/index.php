<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
?>

<form action="update.php" method="post" name="form1">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>
<blockquote>
<?= displayOption($i_CampusTV_BasicSettings, 'settings.php', 1,
                                $i_CampusTV_LiveBroadcastManagement, 'live.php', 1,
                                $i_CampusTV_ChannelManagement, 'channel.php', 1,
                                $i_CampusTV_VideoManagement, 'movieclip.php', 1,
                                $i_CampusTV_PollingManagement, 'polling.php', 1
                                ) ?>


</blockquote>
</td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>