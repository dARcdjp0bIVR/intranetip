<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

intranet_opendb();
# TABLE SQL
$keyword = trim($keyword);
if ($field == "") $field = 0;

$sql = "SELECT
              DATE_FORMAT(DateStart,'%Y-%m-%d'),
              DATE_FORMAT(DateEnd,'%Y-%m-%d'),
              Question, DateModified,
              CONCAT('<input type=checkbox name=PollingID[] value=',PollingID,'>')
        FROM INTRANET_TV_POLLING
        WHERE (Question like '%$keyword%'
               )";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("DateStart","DateEnd","Question","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array();
$li->IsColOff = 2;
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $i_general_startdate)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(1, $i_general_enddate)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(2, $i_CampusTV_PollingName)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(3, $i_LastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("PollingID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('polling_new.php')\">".newIcon()."$button_new</a>";
$functionbar= "";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'PollingID[]','polling_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_sc_campustv, 'index.php', $i_CampusTV_PollingManagement, '') ?>
<?= displayTag("head_campustv_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>