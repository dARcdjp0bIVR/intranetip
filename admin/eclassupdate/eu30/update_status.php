<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("includes/libpatchupdate.php");

# Preventing duplicate update process run at the same time.
if ($t=="1") {
echo time();exit;
}

session_start();

/*
# Case 1, run update in other tab by same session id
# Case 2, run update in other browser
$sql = "select * from ECLASS_PATCH_HISTORY where ReferenceID = '51' order by HistoryID desc limit 1";
intranet_opendb();
$libdb = new libdb();
$resultset = $libdb->returnArray($sql,1);
#print $resultset[0][Events];exit;
$last_puid = trim(str_replace("Called system update through web api, process id: ","",$resultset[0][Events]));
#print $last_puid;exit;
*/


#exec("ps aux|grep full_update_process.php|wc -l",$output,$return_var);
clearstatcache();
exec("ps aux > /tmp/pslist.txt;",$output,$return_var);
exec("cat /tmp/pslist.txt|grep full_update_process.php|wc -l",$output,$return_var);
#print_r($output[0]);exit;
#echo $output[0];exit;
#print_r($output);exit;

#echo "process id: ".$_SESSION['puid']."<br>";
#echo is_process_running($_SESSION['puid']);exit;

if ($output[0] < 1) {
$refid="15"; $event_desc="Debug: ".$output[0];
writePULog($event_desc,'0',$refid,$sid);
$confirm_update = "yes";
} else {
$refid="50"; $event_desc="Update process is running! Loading current status...";
#$refid="50"; $event_desc="Update process is running! Loading current status..., process id: ".$_SESSION['puid']." running status: ".$output[0];
writePULog($event_desc,'0',$refid,$sid);
};

if ($confirm_update == "yes") {
$command = "php full_update_process.php run_as_shell_script ".$_SERVER['SERVER_NAME'];
$ps = run_in_background($command);
$_SESSION['puid'] = $ps;
$refid="51"; $event_desc="Called system update through web api, process id: $ps";
writePULog($event_desc,'0',$refid,$sid);

}

############################################################################


function run_in_background($Command, $Priority = 0)
{
	if($Priority) {
           $PID = shell_exec("nohup nice -n $Priority $Command 2> /dev/null & echo $!");
     	}  //print $PID;}
       	else {
           $PID = shell_exec("nohup $Command > /dev/null 2> /dev/null & echo $!");
	}
        return($PID);
}

function is_process_running($PID)
{
if ($PID != "") {
       exec("ps $PID", $ProcessState);
       return(count($ProcessState) >= 2);
	   } else {return 0;}
	   
}

?>
<style type="text/css"> 
h1 {
font-family: arial;
font-size:16px;
}
p {
font-family: arial;
font-size:12px;
}

.style1 { 
  color:#FF0000; 
}

<!-- 
.popup { 
      background-color: #FFFFFF; 
      height: 300px; width: 500px; 
      #border: 5px solid #666; 
	  border: 1px solid #666; 
      position: absolute; visibility: hidden; 
      font-family: Verdana, Geneva, sans-serif; 
      font-size: small; text-align: justify; 
      padding: 5px; overflow: auto; 
      z-index: 2; 
} 
.popup_bg { 
      position: absolute; 
      visibility: hidden; 
      height: 100%; width: 100%; 
      left: 0px; top: 0px; 
      filter: alpha(opacity=70); /* for IE */ 
	  #opacity: 0.7; /* CSS3 standard */ 
      opacity: 0.9; /* CSS3 standard */ 
      #background-color: #999; 
	  background-color: #000; 
      z-index: 1; 
} 
.close_button { 
      font-family: Verdana, Geneva, sans-serif; 
      font-size: small; font-weight: bold; 
      #float: right; color: #666; 
	  float: right; color: #FFF; 
      display: block; text-decoration: none; 
      #border: 2px solid #666; 
	  border: 0px solid #666; 
      padding: 0px 3px 0px 3px; 
} 
body { margin: 0px; } 
--> 


</style>
<script language="JavaScript" src="jquery-1.7.2.min.js"></script>
<script language="JavaScript">
(function poll(){
    $.ajax({ url: "update_long_poll.php", success: function(data){
        
         $("div.current_status p").html(data);
    }, dataType: "html", complete: poll, timeout: 30000 });
})();

</script>

<script language="javascript"> 
function openpopup(id){ 
      //Calculate Page width and height 
      var pageWidth = window.innerWidth; 
      var pageHeight = window.innerHeight; 
      if (typeof pageWidth != "number"){ 
      if (document.compatMode == "CSS1Compat"){ 
            pageWidth = document.documentElement.clientWidth; 
            pageHeight = document.documentElement.clientHeight; 
      } else { 
            pageWidth = document.body.clientWidth; 
            pageHeight = document.body.clientHeight; 
      } 
      }  
      //Make the background div tag visible... 
      var divbg = document.getElementById('bg'); 
      divbg.style.visibility = "visible"; 
        
      var divobj = document.getElementById(id); 
      divobj.style.visibility = "visible"; 
      if (navigator.appName=="Microsoft Internet Explorer") 
      computedStyle = divobj.currentStyle; 
      else computedStyle = document.defaultView.getComputedStyle(divobj, null); 
      //Get Div width and height from StyleSheet 
      var divWidth = computedStyle.width.replace('px', ''); 
      var divHeight = computedStyle.height.replace('px', ''); 
      var divLeft = (pageWidth - divWidth) / 2; 
      var divTop = (pageHeight - divHeight) / 2; 
      //Set Left and top coordinates for the div tag 
      divobj.style.left = divLeft + "px"; 
      divobj.style.top = divTop + "px"; 
      //Put a Close button for closing the popped up Div tag 
      if(divobj.innerHTML.indexOf("closepopup('" + id +"')") < 0 ) 
      divobj.innerHTML = "<a href=\"#\" onclick=\"closepopup('" + id +"')\"><span class=\"close_button\"><img src=images/close_delete.png border=0></span></a>" + divobj.innerHTML; 
} 
function closepopup(id){ 
      var divbg = document.getElementById('bg'); 
      divbg.style.visibility = "hidden"; 
      var divobj = document.getElementById(id); 
      divobj.style.visibility = "hidden"; 
} 
</script> 



<a href="#" onclick="openpopup('popup1')">Status Here!</a> 
<div id="popup1" class="popup">
<br><br>
<table border=0 width=400 style="margin-left:auto;margin-right:auto;"><tr><td>
<h1>eClass System Update Progress:</h1>
<div class="current_status">
<p>Loading...</p>
</div>
</td></tr>
</table>
</div> 


<div id="bg" class="popup_bg"></div> 
