<?
# modifying : henry chow

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_opendb();

$li = new libfilesystem();


# update version in DB 
$result = updateEclassModuleVersion($module, $ver);


intranet_closedb();

$flag = ($result) ? 1 : 0;

header("Location: index.php?module=$module&msg=$flag");
	
?>