<? 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries

if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}


/*  Extract eclass source code /home/eclass/{eclass30,intranetIP} or /home/junior/junior20/{eclass30,intranetIP} */


// Define variables
session_start();
if( $systemType == "junior" )
	$extractEclassFilePath = substr($eclass_path,0,strpos($eclass_path,'junior20')-1);
else
	$extractEclassFilePath=$eclass_path;


$alertmessage = $_REQUEST["alertmessage"];

// Extract Source

if ( $doAction == "extractSource" )
{
	include_once("../../../templates/adminheader_setting.php");?>
	<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
	<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>
	<table width=590 border=0 cellpadding=0 cellspacing=0 align=center>
	<td class=tableContent height=300>
	<?

	// Import language files
	if( $systemType == "junior" )
        	include("../includes/header_big5.php"); // Include big5 header
	else 
        	include("../includes/header_utf8.php"); // Include utf-8 header

        echo "<script language=\"JavaScript\" type=\"text/javascript\">";
        echo "{ showHeader(4); }";
        echo "</script>";

	## Start Process
	echo "<BR>";
        echo $i_eu_extract_msg;
        writeEULog ("Upgrading eClass System ...");

        echo "<div id=extract-source-wrapper>";
	# Start to extract eclass source
        if (($upgrade_intranet) && ($upgrade_eclass)) {
	        echo "$i_eu_extract_msg2 <iframe src=\"extractIntranetSource.php?extractEclassFileName=$intranet_package_filename&extractEclassFilePath=$extractEclassFilePath\" name=\"Download\" scrolling=\"no\" frameborder=\"yes\" align=\"center\" height=\"40%\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
	} else if ($upgrade_intranet) {
		echo "$i_eu_extract_msg2 <iframe src=\"extractIntranetSource.php?extractEclassFileName=$intranet_package_filename&extractEclassFilePath=$extractEclassFilePath\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"40%\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
	} else if ($upgrade_eclass){
		// echo "Updating Eclass system source... <iframe src=\"extractEclassSource.php?extractEclassFileName=$eclass_package_filename&extractEclassFilePath=$extractEclassFilePath\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"40%\" width=\"100%\" marginheight=\"0\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px;\"></iframe>";
        } else {
		echo "No extraction!!";
	}
        echo "</div>";

}


/*
Update Sql
*/

if ( $doAction == "updateSql" )
{
	echo "<BR>";
        echo "<div id=extract-mysql-wrapper>";		
	# Start to update mysql
        if (($upgrade_intranet) && ($upgrade_eclass)) {
	           if (($IntranetSourceUpdateComplete == "true")) {
			echo "<iframe src=\"updateIntranetSql.php?backupFileName=$backupMysqlFileName&backupFilePath=$backupMysqlFilePath\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"80%\" width=\"100%\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px; marginwidth: 0px;\"></iframe>";
		        writeEULog ("Updating Intranet SQL...");
		}
        } else if ($upgrade_intranet) {
			echo "<BR><font face=arial size=-1><iframe src=\"updateIntranetSql.php?backupFileName=$backupMysqlFileName&backupFilePath=$backupMysqlFilePath\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"80%\" width=\"100%\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px; marginwidth: 0px; \"></iframe>";
		        writeEULog ("Updating Intranet SQL...");
        } else if ($upgrade_eclass) {
		echo "<BR><font face=arial size=-1>Update Eclass SQL... <iframe src=\"updateEclassSql.php?backupFileName=$backupMysqlFileName&backupFilePath=$backupMysqlFilePath\" name=\"Download\" scrolling=\"no\" frameborder=\"no\" align=\"center\" height=\"80%\" width=\"100%\" style=\"overflow-x: hidden; overflow-y: hidden; border-width: 0px; marginwidth: 0px; \"></iframe>";
	        writeEULog ("Updating Eclass SQL...");
        } else {
		echo "No Sql update!!";
	}
	echo "</div>";
}


// Redirect to final step

if ( $doAction == "extractFinish" )
{
	if (($upgrade_intranet) && ($upgrade_eclass)) {
        	if (($IntranetSqlUpdateComplete == true) && ($EclassSqlUpdateComplete == true)) {
			sleep(3);
	                echo '<script language="javascript">';
	                echo 'parent.parent.location.href = "final.php";';
        	        echo '</script>';
		}
	} else if (($upgrade_intranet) && (! $upgrade_eclass)) {
        	if (($IntranetSqlUpdateComplete == true) && ($EclassSqlUpdateComplete == true)) {
			sleep(3);
	                echo '<script language="javascript">';
			echo 'parent.parent.location.href = "final.php";';
        	        echo '</script>';
		}
	} else if ((! $upgrade_intranet) && ($upgrade_eclass)) {
        	if (($IntranetSqlUpdateComplete == "") && ($EclassSqlUpdateComplete == true)) {
                        sleep(3);
                        echo '<script language="javascript">';
	                echo 'parent.parent.location.href = "final.php";';
                        echo '</script>';
                }
        } else {
		echo "No update!!";
	}

	if (($IntranetSqlUpdateComplete == false) or ($EclassSqlUpdateComplete == false)) {
		     echo '<script language="javascript">';
	             echo 'parent.parent.location.href = "error.php?message=Upgrade SQL Fail";';
                     echo '</script>';
	}

	if ($IntranetSourceUpdateComplete == false) {
                        echo '<script language="javascript">';
                        echo 'parent.parent.location.href = "error.php?message=Update Source Fail";';
                        echo '</script>';
	}
	
}

if ($doAction!="updateSql" and $doAction!="extractFinish") { ?>
	</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");
}
?>
