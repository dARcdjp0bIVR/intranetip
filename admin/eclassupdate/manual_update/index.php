<?php
session_start();
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclassupdate.php");

// eclassupdate lib
include_once("../includes/global.php");
include_once("../includes/functions.php");
if( $systemType == "junior" ){
        include_once("../includes/lib/php4/XMLParser.php"); // Include XML library
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../includes/lib/php5/XMLParser.php"); // Include XML library
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}


// Define variable

?>
<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>


<?php
############################## START of HD SPACE INFRORMATION
if (function_exists("ep_hwinfo"))
{
	$hwinfo=ep_hwinfo($intranet_root, '/var/lib/mysql', false);
	//debug_r($hwinfo);
	$eClass_Use_percentage = round(100 * ($hwinfo["eclass_used_space"] / $hwinfo["eclass_total_space"]));
	$eClass_lef_alert = ($hwinfo["eclass_free_space"]>1073741824*2) ? "blue" : "red";
	$Mysql_Use_percentage = round(100 * ($hwinfo["mysql_used_space"] / $hwinfo["mysql_total_space"]));
	$Mysql_lef_alert = ($hwinfo["mysql_free_space"]>1073741824*2) ? "blue" : "red";
	$Tmp_Use_percentage = round(100 * ($hwinfo["tmp_used_space"] / $hwinfo["tmp_total_space"]));
	$Tmp_lef_alert = ($hwinfo["tmp_free_space"]>1073741824*2) ? "blue" : "red";
	?>
	
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="/templates/jquery/jquery.progressbar.min.js"></script> 
<script>
	$(document).ready(function() {
		$("#progressbarA").progressBar({barImage: '/images/progress_bar_green.gif'} );
		$("#progressbarB").progressBar({barImage: '/images/progress_bar_green.gif'} );
		$("#progressbarC").progressBar({barImage: '/images/progress_bar_green.gif'} );
	});
</script>
<style>
	#container { width: 80%; margin-left: 10%; margin-top: 30px;}
</style>

<table border="0" align="center" width="550" style="border:1px dotted orange;" bgcolor='#ffffBB'>
<tr>
<td nowrap="nowrap" align='right'><?=$Lang['eu']['eclass_space']?></td>
<td>
		<table width="240">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarA"><?=$eClass_Use_percentage?>%</span></td></tr>
		</table>
</td>
<td width="200"><b><font color="<?=$eClass_lef_alert?>"><?=ConvertBytes($hwinfo["eclass_free_space"])?></font>  <?=$Lang['eu']['space_left']?></b></td>
</tr>
<tr>
<td nowrap="nowrap" align='right'><?=$Lang['eu']['db_space']?></td>
<td>
		<table width="240">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarB"><?=$Mysql_Use_percentage?>%</span></td></tr>
		</table>
</td>
<td><b><font color="<?=$Mysql_lef_alert?>"><?=ConvertBytes($hwinfo["mysql_free_space"])?></font> <?=$Lang['eu']['space_left']?></b></td>
</tr>
<!--
<tr>
<td nowrap="nowrap">Temporary Space Used:</td>
<td>
		<table width="240">
			<tr><td width="240" align="center"><span class="progressBar" id="progressbarC"><?=$Tmp_Use_percentage?>%</span></td></tr>
		</table>
</td>
<td><b><font color="<?=$Tmp_lef_alert?>"><?=ConvertBytes($hwinfo["tmp_free_space"])?></font> <?=$Lang['eu']['space_left']?></b></td>
</tr>
-->
<tr>
	<td colspan="3"><font size="3" color="blue"><?=$Lang['eu']['notice']?></font></td>
</tr>
</table>
<br />
	<?php
}
############################## END of HD SPACE INFRORMATION
?>



<br />
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>
<?php


// Get machine code and verify whether can run update or not
writeEULog ("Checking access privilege..");
$machine_code = checkMachineCode();

$url = $package_api_root . "/accessright.php?";
$post = "mcode=" . trim($machine_code). "&intranet_session_language=" . $intranet_session_language;
$checkRight = send_post($url, $post);
//Set up the parser object

if ($debug=='1') {
	print "[Debug]<br>";
	print "url: $url<br>";
	print "post: $post<br>";
	print "checkRight: $checkRight<br>";
	$filename="scripts/zendid";
	$fileowner = posix_getpwuid(fileowner($filename));
	print "fileowner: ".$fileowner['name']."<br>";
	print "Machine Code: ".trim($machine_code)."<br>";
	echo "zendid permission: ".substr(sprintf('%o', fileperms('scripts/zendid')), -4)."<br>"; 

	print "<br><br>";
}

$clientinfo = new XMLParser($checkRight);

//Work the magic...
$clientinfo->Parse();
$status = $clientinfo->document->status[0]->tagData;
$allowupdate = $clientinfo->document->allowupdate[0]->tagData;
$branch = $clientinfo->document->branch[0]->tagData;
$needbackup = $clientinfo->document->needbackup[0]->tagData;

session_register('allowupdate');
session_register('branch');
if ($status == "") {$status='<font color=red>Unknown</font>'; }
print $i_eu_welcome_msg."<hr size=1 class=hr_sub_separator><br>";
print "$i_Maintenace_Status: ".nl2br($status)."<br><hr size=1 class=hr_sub_separator><br>";


intranet_opendb();
$eu = new libeclassupdate();
$currentversion=$eu->current_intranet_version($intranet_root);
$eu->clearhistory('2009-9-01','2009-11-19');


print "$i_System_Status: ";
include_once("../includes/getPackageVersion.php");

if (trim($intranet_package_version) != trim($currentversion)) {
	$message = "<font color=red>".$i_update_notlatest."</font>";


	if ($allowupdate == 'yes') {
		unset($intranet_package_title);
		unset($intranet_package_type);
		unset($intranet_package_version);
		unset($intranet_package_date);
		unset($intranet_package_md5);
		unset($intranet_package_filename);
		unset($intranet_package_url);

		session_register('current_intranet_version');
		session_register('current_eclass_version');
		session_register('upgrade_intranet');
		session_register('upgrade_eclass');
     
		$upgrade_intranet = true;
		$upgrade_eclass  = false;
		writeEULog("Run intranet update!!");
		print "<br><br>$message<br><br>$i_eu_submit_wizard";
	} else {
		echo $i_eu_not_activated;} 
} else { 
	print $message = "<br><br><font color=green>".$i_update_islatest."</font>";
}

?>    
</td></tr>
<?php if ($allowupdate == 'yes' and (trim($intranet_package_version) != trim($currentversion))) { ?>
<tr>
	<td align=right>
		<hr size=1 class="hr_sub_separator"><form name="form1" action="prechecking.php" method="post"><?= btnSubmit();?></form>
	</td>
</tr>
<?php }

?>
</table>
<?

include_once("../../../templates/adminfooter.php");
?>
