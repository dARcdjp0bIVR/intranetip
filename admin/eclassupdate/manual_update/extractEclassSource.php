<? 
include("functions.php");  // Import functions libraries


// Define variables
session_start();
session_register('EclassSourceUpdateComplete');
$progress_counter = 0;


// Call shell script to update source code
$command = "sudo sh scripts/update.sh updatesource " . $_REQUEST["extractEclassFileName"] . " " . $_REQUEST["extractEclassFilePath"];
$ps = run_in_background("$command");
writeEULog ($_REQUEST["extractEclassFileName"]);
writeEULog ($_REQUEST["extractEclassFilePath"]);

while(is_process_running($ps))
{
	$progress_counter++;

	if ( ($progress_counter % 30) == 0)
     		echo("#");
                echo str_repeat(" ", 4096);
	       	ob_flush(); 
		flush();
	        usleep(5000);
}


// Check whether tar file has errors of not
$command_1 = "tail Log/eclasstempfile | grep \"Not found in archive\"";
$command_2 = "tail Log/eclasstempfile | grep \"Unexpected EOF in archive\"";
$result1 = exec($command_1);
$result2 = exec($command_2);


if (( "$result1" == "" ) && ( "$result2" == "" )) {
	writeEULog ("Eclass Source extract Success");
        $alertmessage = "Eclass Source extract success";
	$EclassSourceUpdateComplete = true;
} else {
        Show_and_write_log ("Eclass Source extract fail");
        $alertmessage = "Eclass Source extract fail";
	$EclassSourceUpdateComplete = false;
}

echo '<script language="javascript">';
echo 'location.href = "extract.php?doAction=updateSql&alertmessage=' . $alertmessage . '";';
echo '</script>';

?>
