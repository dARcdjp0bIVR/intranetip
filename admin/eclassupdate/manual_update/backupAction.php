<? 
// Import functions libraries
include("../includes/functions.php"); 


// Define variables
$progress_counter = 0;
$makeBackup = $_REQUEST["backupPath"] . "/" .  $_REQUEST["backupFileName"];


// Call shell script to backup system
if( $systemType == "junior" ){
	$command = "scripts/update.sh backupsource " . $makeBackup . " \"" . $_REQUEST["backupFilePath"] . "\"";
}
else
	$command = "sudo -u eclass scripts/update.sh backupsource " . $makeBackup . " \"" . $_REQUEST["backupFilePath"] . "\"";

$ps = run_in_background("$command");

while(is_process_running($ps))
{
	$progress_counter++;

	if ( ($progress_counter % 100) == 0) {
		echo("#");
		echo str_repeat(" ", 4096);
	       	ob_flush(); 
		flush();
	        usleep(500000);//
	}
}


$alertmessage = "Backup eClass finish";
writeEULog ($alertmessage);

if ( $_REQUEST["backupIdentifier"] == "eclassSource" ) {
        echo '<script language="javascript">'; 
        echo 'location.href = "backup.php?doAction=verify&backupIdentifier=eclassSource&alertmessage=' . $alertmessage . '";'; 
        echo '</script>';
}

if ( $_REQUEST["backupIdentifier"] == "eclassMysql" ){
        echo '<script language="javascript">';
        echo 'location.href = "backup.php?doAction=verify&backupIdentifier=eclassMysql";';
        echo '</script>';
}

?>
