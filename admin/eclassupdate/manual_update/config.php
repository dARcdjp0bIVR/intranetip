<?php 
// Download eclass package
$cfg_downloadEclassPackage = 0;

// Verify downloaded eclass package
$cfg_verifyEclassPackage = 0;

// Download eclass package PID
$cfg_EclassPackagePID = 0;

// eClass update start time
$cfg_programStartTime = 0;

// #
$cfg_backupIdentifier = 0;

// Auto generate name of backup file
$cfg_backupEclassFileName = '';

// Auto generate name of Mysql file
$cfg_backupMysqlFileName = '';

// Path to save the backup
$cfg_backupEclassFilePath = '';

// Backup source code status: 1->Success ,
// 0->Fail
$cfg_doBackupSource = 0;

// Verify source code status: 1->Success ,
// 0->Fail
$cfg_verifyBackupSource = 0;

// Backup source code PID
$cfg_BackupSourcePID = 0;

// Backup MYSQL status: 1->Success ,
// 0->Fail
$cfg_doBackupSql = 0;

// Verify MYSQL status: 1->Success ,
// 0->Fail
$cfg_verifyBackupSql = 0;

// Update source code status: 1->Success ,
// 0->Fail
$cfg_updateSource = 0;

$cfg_updateSourcePID = 0;

// Update MYSQL status: 1->Success ,
// 0->Fail
$cfg_updateSql = 0;

?>
