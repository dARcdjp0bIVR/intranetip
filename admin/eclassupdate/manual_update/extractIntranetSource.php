<? 
include_once("../../../includes/global.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../lang/lang.$intranet_session_language.eu.php");
include_once("../includes/global.php");
include_once("../includes/functions.php");  // Import functions libraries


// Define variables
session_start();
session_register('IntranetSourceUpdateComplete');
print "<META http-equiv=Content-Type content='text/html; charset=UTF-8'>";
$progress_counter = 0;


// Call shell script to update source code
if( $systemType == "junior" )
	$command = "sudo -u junior scripts/update.sh updatesource " . $_REQUEST["extractEclassFileName"] . " " . $_REQUEST["extractEclassFilePath"];
else
	$command = "sudo -u eclass scripts/update.sh updatesource " . $_REQUEST["extractEclassFileName"] . " " . $_REQUEST["extractEclassFilePath"];

writeEULog ($_REQUEST["extractEclassFileName"]);
writeEULog ($_REQUEST["extractEclassFilePath"]);

$ps = run_in_background("$command");

while(is_process_running($ps))
{
	$progress_counter++;

	if ( ($progress_counter % 30) == 0)
     		echo("#");
                echo str_repeat(" ", 4096);
	       	ob_flush(); 
		flush();
	        usleep(5000);
}

// Check whether tar file has errors of not
$command_1 = "tail Log/tempfile | grep \"Not found in archive\"";
$command_2 = "tail Log/tempfile | grep \"Unexpected EOF in archive\"";

$result1 = exec($command_1);
$result2 = exec($command_2);

if (( "$result1" == "" ) && ( "$result2" == "" )) {
	// Delete smarty cache
	$smarty_cache_dir = $eclass_root."/files/smarty_cache";
	$command = "rm -f $smarty_cache_dir/*.tpl.php";
	exec($command, $return_arr, $return_var);
	print "<font face=arial size=-1>";
	echo "<br>[$i_eu_success]";
	writeEULog ("<br>Extraction Done!<br>");
	$alertmessage = "Intranet Source extract success";
	$IntranetSourceUpdateComplete = true;
	sleep(2);
} else {
        writeEULog ("<br>Intranet Source extract fail<br>");
        $alertmessage = "Intranet Source extract fail";
	$IntranetSourceUpdateComplete = false;
}

echo '<script language="javascript">';
echo 'location.href = "extract.php?doAction=updateSql&alertmessage=' . $alertmessage . '";';
echo '</script>';

?>
