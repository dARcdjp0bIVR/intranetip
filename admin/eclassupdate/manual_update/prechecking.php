<? 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclassupdate.php");
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries

if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}

session_start();

if ($_SESSION['allowupdate'] != 'yes') {redirectErrorPage("Error on system checking!");};

?>
<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>
<table width=590 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>
<?php
if( $systemType == "junior" )
        include("../includes/header_big5.php"); // Include big5 header
else
        include("../includes/header_utf8.php"); // Include utf-8 header


// Show the Javascript header
echo "<script language=\"JavaScript\" type=\"text/javascript\">";
echo "{ showHeader(1); }";
echo "</script>";


// Linux command that must be installed for eClassUpdate
$binaryexecute[] = "lynx";
$binaryexecute[] = "wget";
$binaryexecute[] = "tar";
$binaryexecute[] = "find";
$binaryexecute[] = "sudo";

$checkpath[]=$eclass_path."/intranetIP";
$checkpath[]=$eclass_path."/eclass40";
$checkpath[]=$eclass_path."/eclass30";
$checkpath[]=$eclass_path."/Smarty";


// Check Required system commands
$checkstatus = binaryexist($binaryexecute);
if ($checkstatus == '0') {
	$message = "Required system command have not been installed";
	print $message;
	redirectErrorPage($message);
	exit;
}


// Detect Port 443
$server_status = checkService("SSL",$update_server,443);
if($server_status != "Up") {
	print $i_eu_check_port;
	redirectErrorPage($i_eu_check_port);
	exit;
}


// Detect variable servername
if ( $HTTP_SERVER_VARS['SERVER_NAME'] == "" ){
	echo $i_eu_check_servername;
	exit;
}


// Log Variables
$showVars = "[systemType:" . $systemType . "|allowupdate:" . $_SESSION['allowupdate'] . "|SERVER_NAME:" . $HTTP_SERVER_VARS['SERVER_NAME'] . "]";
writeEULog("$showVars");


// Create related folders and assign permission
if( $systemType == "junior" ) {
	$command = "sudo -u junior scripts/update.sh createFolder " . $package_download_folder . " " . "777";
	$ps = run_in_background("$command");

	$command2 = "sudo -u junior scripts/update.sh createFolder ./Log 777";
	$ps2 = run_in_background("$command2");

        ### Change $package_download_folder permission to 777
        $result=checkfilepermission("$package_download_folder",777);
        if ( $result == 0 ) {
                $command2 = "sudo -u junior scripts/update.sh changeFolderPermission ./Log 777";
                $ps = run_in_background("$command2");
        }

} else {
	$command = "sudo -u eclass scripts/update.sh createFolder " . $package_download_folder . " " . "777";
	$ps = run_in_background("$command");

	$command2 = "sudo -u eclass scripts/update.sh createFolder ./Log 777";
	$ps2 = run_in_background("$command2");

        ### Change $package_download_folder permission to 777
        $result=checkfilepermission("$package_download_folder",777);
        if ( $result == 0 ) {
                $command2 = "sudo -u eclass scripts/update.sh changeFolderPermission ./Log 777";
                $ps = run_in_background("$command2");
        }

}


/* 
1. Detect system type
2. Change ./Log folder permission to 777
3. Scan folders ownership 
*/
if( $systemType == "junior" ) {
        $command = "sudo -u junior scripts/update.sh createFolder /tmp/juniorBackup 777";
        $ps = run_in_background("$command");

	### Change ./Log permission to 777 
	$result=checkfilepermission("./Log",777);
	if ( $result == 0 ) {
	        $command2 = "sudo -u junior scripts/update.sh changeFolderPermission ./Log 777";
        	$ps = run_in_background("$command2");
	}
} else {
        $command = "sudo -u eclass scripts/update.sh createFolder /tmp/eclassBackup 777";
        $ps = run_in_background("$command");

	### Change ./Log permission to 777 
	$result=checkfilepermission("./Log",777);
	if ( $result == 0 ) {
	        $command2 = "sudo -u eclass scripts/update.sh changeFolderPermission ./Log 777";
        	$ps = run_in_background("$command2");
	}


	# Change folder ownership to Eclass
	$checkstatus = checkfileownership($checkpath, "eclass");
	if ($checkstatus == '0') {
		print $i_eu_ownership_fail;
		redirectErrorPage($i_eu_ownership_fail);
		exit;
	}

}

// Define program start time as session variable

session_register('programStartTime');
$programStartTime = date("Ymd_His"); 
writeEULog("System analyzing...");

echo "<div id=prechecking>";

// Determine whether package exist or not
$download_temp = $intranet_package_url;


// Calcuate the eclass system
$intranet_system_size = GetFolderSize( $intranet_root );
$eclass_system_size = GetFolderSize( $eclass_root );


// Calcuate MYSQL
$mysql_total_size = GetFolderSize( $mysql_path );
	
writeEULog ("Intranet system total size: ".number_format($intranet_system_size/1024/1024, 2)."MB");
writeEULog ("eClass system total size: ".number_format($eclass_system_size/1024/1024, 2)."MB");
writeEULog ("Mysql total size: ".number_format($mysql_total_size/1024/1024, 2)."MB");
$total_size = ($intranet_system_size + $eclass_system_size)  + $mysql_total_size + (100*1024*1024);
writeEULog ("Space required for upgrade: ".number_format($total_size/1024/1024, 2)."MB<br>");


// Calucate the root partition space
$hd_space = disk_free_space($intranet_root);
writeEULog ("Diskspace left $backupEclassFilePath: ".number_format( $hd_space/1024/1024, 2)."MB<br>");

if ( $hd_space > $total_size ) {
	echo $i_eu_check_space_pass."<br><br>";
	writeEULog ("Checking for available storage space: Passed!");
		
	if (url_exists($download_temp)) {
		echo $i_eu_check_connection_pass."<br><br>";
		writeEULog("Checking connection to the update server: Connected!");
			             
		$eu = new libeclassupdate();
		$current_intranet_version=$eu->current_intranet_version($intranet_root);
		echo "<div class=upgradeinfo>";

		if ($upgrade_intranet) {
			echo "<U>$i_eu_now_will_up</U><BR>";
			echo "<br>$i_eu_from_ver" . $current_intranet_version;
			echo "<BR>$i_eu_to_ver" . $intranet_package_version;
			echo "<BR>";
		}

		echo "</div>";

		echo $i_eu_next_msg;
		echo "<form name=\"PreChecking\" action=\"download.php\" method=\"post\">";
		echo "<input id=\"submit\" type=\"submit\" Name=\"Submit\" Value=\"$i_eu_next_button\">";
		echo "<INPUT TYPE=HIDDEN NAME=\"doAction\" value=\"download\">";
		echo "</form>";

	} else {
	 	Show_and_write_log ("$i_eu_check_connection_fail<br><br>$i_eu_error2");
	}
}else {
	Show_and_write_log ("Not enough space for upgrade (please contact server administrator)");
	exit(0);
}

echo "</div>";


?>
</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");
?>
