#!/bin/bash
# Last Modified: 20150727
# Parameters
TIMESTAMP=`date +%Y%m%d_%H%M`
LOGDIRECTORY="Log"
PACKAGE_ROOT="/tmp/eclassupdate"
EU_TEMPFILE="Log/tempfile"
#echo $5 >> Log/ad-hoc.txt


#=====================
# Update Source
#=====================
touch $EU_TEMPFILE
#chown apache.apache $EU_TEMPFILE


if [ "$1" == "updatesource" ]; then
	echo "Extracting source..."
	> $EU_TEMPFILE
	tar -zxvf $PACKAGE_ROOT/$2 -C $3 2> $EU_TEMPFILE 
	
	#chown -R eclass.eclass $3
#	tar -zxvf $PACKAGE_ROOT/$2 -C $3 > $EU_TEMPFILE 2>&1
elif [ "$1" == "backupsource" ]; then
   #echo "backup src";
  	tar -zcvf $2 $3 ; echo $? > /tmp/`basename $2`"-checksum";
	#tar -zcvf $2 $3 2> /tmp/tar_error.txt; echo $? > /tmp/`basename $2`"-checksum";
  	
  	
  	
elif [ "$1" == "backupsql" ]; then
	echo "" > /tmp/mysqlVerify.txt
   #echo "backup src";
#  	mysqldump -u$2 -p$3 --databases $5 > $4
	mkdir $4
	for db in $5
	do
	        mysqldump -u$2 -p$3 --single-transaction --lock-tables=false --opt $db > "$4/$db.sql"

		if [ $? == 0 ]
		then
			echo "Database: $db [OK]" >> /tmp/mysqlVerify.txt
		else
			echo "Database: $db [FAIL]" >> /tmp/mysqlVerify.txt
			break
		fi

#	       	bzip2  "$BACKUP_LOCATION/$db.sql"
	done

elif [ "$1" == "backupsql-remote" ]; then
	echo "" > /tmp/mysqlVerify.txt

	mkdir $4
	for db in $6
	do
	        mysqldump -u$2 -p$3 -h$5 --single-transaction --lock-tables=false --opt $db > "$4/$db.sql"

		if [ $? == 0 ]
		then
			echo "Database: $db [OK]" >> /tmp/mysqlVerify.txt
		else
			echo "Database: $db [FAIL]" >> /tmp/mysqlVerify.txt
			break
		fi

#	       	bzip2  "$BACKUP_LOCATION/$db.sql"
	done

elif [ "$1" == "createFolder" ]; then
	mkdir -p $2
	chmod $3 $2
 
elif [ "$1" == "changeFolderPermission" ]; then
	chmod $3 $2
elif [ "$1" == "changeFolderOwnership" ]; then
	chown -R $3 $2
elif [ "$1" == "changeFilePermission" ]; then
        chmod $3 $2
elif [ "$1" == "clearEUfiles" ]; then
	rm -rf $2
elif [ "$1" == "overwriteFile" ]; then
	cp -rfp $2 $3
elif [ "$1" == "update_pl2" ]; then	
	
	if [ "$2" == "" ]
	then
	exit;
	fi	
	
	if [ "$3" == "" ]
	then
	exit;
	fi	
	
	local_package=$PACKAGE_ROOT/$2
	eclass_path=$3
	timestamp=`date +%Y%m%d-%H%M`
	tmp_folder=/tmp/pl2-update-$timestamp

	##############################################################
	mkdir $tmp_folder
	cd  $tmp_folder
	tar zxf $local_package

	rsync -rvlHpogDtS --delete --exclude '.crossbar' --exclude 'lib/powerlesson/config/settings.php' $tmp_folder/lib/powerlesson/ $eclass_path/lib/powerlesson/
	rsync -rvlHpogDtS --delete $tmp_folder/eclass40/src/includes/php/powerlesson/ $eclass_path/eclass40/src/includes/php/powerlesson/
	rsync -rvlHpogDtS --delete --exclude 'api/.htaccess' --exclude 'quickLogin/.htaccess' $tmp_folder/eclass40/src/powerlesson/ $eclass_path/eclass40/src/powerlesson/
	echo $tmp_folder >> /tmp/pl2-update.log
	
else
    echo "Verify"
fi


