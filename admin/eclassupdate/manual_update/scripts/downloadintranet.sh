#!/bin/sh

URL=$1
FILE=$2

DOWNLOAD_FILE="$URL"
LOG_FILE=$3

wget --progress=bar --limit-rate=1000K $DOWNLOAD_FILE -o $LOG_FILE -O $FILE
