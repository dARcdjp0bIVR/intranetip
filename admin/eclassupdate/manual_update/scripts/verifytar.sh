#!/bin/sh

TAR_FILE=$1

tar -tvf $TAR_FILE

#Check return code from tar
if [ $? == 0 ]  
then 
	echo "File ${TAR_FILE} verified OK."
	echo "Ok"
	exit 0
else
	echo "File ${TAR_FILE} failed to verify. Please check."
	echo "Fail"
   	exit 1
fi
