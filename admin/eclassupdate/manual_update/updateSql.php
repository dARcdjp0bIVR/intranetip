<? 
include_once("../../../includes/global.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../lang/lang.$intranet_session_language.eu.php");
include_once("../includes/global.php"); // Import system variables
include_once("../includes/functions.php"); // Import functions libraries
include_once("../includes/confedit.class.php");

// Import language files
if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}


// Define variables
session_start();
session_register('IntranetSqlUpdateComplete');
session_register('EclassSqlUpdateComplete');
$IntranetSqlUpdateComplete = "";
$eu_loginname = "archiweb";
$eu_password = "CS21363170es";
$EclassSqlUpdateComplete = true;


// Prevent invalid access
if (! $allowAccess){
        echo "You cannot access this file directly.";
        exit();
}


#20101119
// Creating an instance of ConfigEditor
$config = new ConfigEditor();

// Load configuration file
$config->LoadFromFile(CONFIG_FILE);
$vars = $config->GetVarNames();


// Call shell scrip to update intranet mysql
$progress_counter = 0;
if ( $eclassupdate_resolve == "" ){
        $servername = $HTTP_SERVER_VARS['SERVER_NAME'];
} else {
        $servername = $eclassupdate_resolve;
}


//if(stristr($intranet_root, "junior") === FALSE) {
if( $systemType == "junior" ) {
	$command = 'sudo -u junior scripts/updateIntranetSQL.sh "http://' . $servername .  '/addon/schema/?flag=2&flag2=1#toend" '."$eu_loginname $eu_password";
//	echo "update junior";
} else {
	$command = 'sudo -u eclass scripts/updateIntranetSQL.sh "http://' . $servername .  '/addon/ip25/?flag=1#toend" '."$eu_loginname $eu_password";
//	echo "update IP25 ";
}

writeEULog("Run scripts/updateIntranetSQL.sh");
$ps = run_in_background("$command");
print "<font face=arial size=-1>$i_eu_update: ";

while(is_process_running($ps))
{
        $progress_counter++;

        if ( ($progress_counter % 30) == 0) {
                echo("#");
                echo str_repeat(" ", 4096);
                ob_flush();
                flush();
                usleep(5000);
        }
}

// Check weather update process success or not
$command_2 = " tail Log/intranettempfile | grep \"Sqlupdate-ok\"";
$result = exec($command_2);

if (( "$result" == "Sqlupdate-ok" )) 	
{ // Detect Intranet db Start
	print "<font face=arial size=-1>";
	echo "[$i_eu_success]";
	writeEULog ("Intranet Mysql update success");
        $alertmessage = "Intranet Mysql update success";
	$IntranetSqlUpdateComplete = true;
	sleep(2);
	echo "<br>";


	// Call shell scrip to update eclass mysql
	$progress_counter = 0;

	if( $systemType == "eclass" ) {
		$command_eclass_1 = 'sudo -u eclass scripts/updateEclassSQL.sh "http://' .$servername.  '/eclass40/addon/schema/?flag=2&flag2=1#toend"'." $eu_loginname $eu_password";

		writeEULog("Run scripts/updateEclassSQL.sh");
		$ps_eclass = run_in_background("$command_eclass_1");
		print "<font face=arial size=-1>$i_eu_update2: </font>";

		while(is_process_running($ps_eclass))
		{
		        $progress_counter++;
			if ( ($progress_counter % 30) == 0) {
				echo("#");
		                echo str_repeat(" ", 4096);
		                ob_flush();
		                flush();
		                usleep(5000);
		        }
		}


		// Check weather update process success or not
		$command_eclass_2 = " tail Log/eclasstempfile | grep \"Sqlupdate-ok\"";
		$result_eclass = exec($command_eclass_2);


		if (( "$result_eclass" == "Sqlupdate-ok" )) {
			$config->SetVar('cfg_updateSql', 1, "#");
			$config_source = $config->Save(CONFIG_FILE);

			print "<font face=arial size=-1>";
			echo "[$i_eu_success]";
			writeEULog ("Eclass Mysql update success");
		        $alertmessage = "Eclass Mysql update success";
			$EclassSqlUpdateComplete = true;
			sleep(2);

			echo '<script language="javascript">';
			echo 'location.href = "extract.php?doAction=extractFinish&alertmessage=' . $alertmessage . '";';
			echo '</script>';

		} else {
			Show_and_write_log ("Intranet Mysql update success");
			$alertmessage = "Intranet Mysql update Database";
//			$EclassSqlUpdateComplete = false;

			echo '<script language="javascript">';
			echo 'location.href = "extract.php?doAction=extractFinish&alertmessage=' . $alertmessage . '";';
			echo '</script>';

		}



	} else {
		// Run junior settings.
		$config->SetVar('cfg_updateSql', 1, "#");
		$config_source = $config->Save(CONFIG_FILE);

                Show_and_write_log ("Eclass Mysql update successfully");
                $alertmessage = "Update Eclass Database successfully";

		echo '<script language="javascript">';
		echo 'location.href = "extract.php?doAction=extractFinish&alertmessage=' . $alertmessage . '";';
		echo '</script>';

	}

} else {	// Intranet DB End
	print "<font face=arial size=-1>";
	Show_and_write_log ("Mysql update fail");
	$alertmessage = "Fail to update Intranet database";
	$IntranetSqlUpdateComplete = false;

	echo '<script language="javascript">';
	echo 'location.href = "extract.php?doAction=reportError&alertmessage=' . $alertmessage . '";';
	echo '</script>';
}



?>
