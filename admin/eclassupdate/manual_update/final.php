<? 
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libeclassupdate.php");
include("../includes/global.php"); // Import system variables
include("../includes/functions.php"); // Import functions libraries
include_once("../../../templates/adminheader_setting.php"); 

// Import language files
if( $systemType == "junior" ){
        include_once("../lang/lang.$intranet_session_language.eu_big5.php");
}else{
        include_once("../lang/lang.$intranet_session_language.eu_utf8.php");
}

?>

<?= displayNavTitle($i_admintitle_sa, '', $i_adminmenu_sc_eclass_update, '', $i_manual_update, '') ?>
<?= displayTag("head_eclass_update_$intranet_session_language.gif", $msg) ?>

<table width=580 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300>
<?
if( $systemType == "junior" )
        include("../includes/header_big5.php"); // Include big5 header
else 
        include("../includes/header_utf8.php"); // Include utf-8 header

// Show the Javascript header
echo "<script language=\"JavaScript\" type=\"text/javascript\">";
echo "{ showHeader(5); }";
echo "</script>";

session_start();


// Send email with report to Broadlearning
writeEULog ("Send Email");
run_in_background(" php sendemail.php ");


// Update client details to update server API 
$machine_code = checkMachineCode();
writeEULog ("Update eclass update server status");
$url = $package_api_root . "/updateversion.php?";
$post = "mcode=".trim($machine_code)."&current_ver=".$current_intranet_version;
$foobar = send_post($url, $post);

echo "<BR>";
writeEULog ("eClass System has been upgraded from ".$current_intranet_version." to ".$intranet_package_version." latest version.", 'Success');

if( $systemType == "junior" )
	echo "";
else 
	echo "$i_update_islatest.<br><br> $i_eu_final<br><a href=http://version-dev.eclass.com.hk target=new>http://version-dev.eclass.com.hk</a><br><br>";
	
echo "<br>";
echo "<div id=final-wrapper>";
echo "<U>$i_eu_summary</U><BR>";

if (($upgrade_intranet) && ($upgrade_eclass)) {
	echo "eClass System upgrade <BR>$i_eu_from_ver: ";
	echo $current_intranet_version;
	echo "<BR>$i_eu_to_ver: " . $intranet_package_version;
	echo "<BR><BR>";
} else if ($upgrade_intranet) {
	echo "<BR>$i_eu_from_ver: ";
	echo $current_intranet_version;
	echo "<BR>$i_eu_to_ver: " . $intranet_package_version;
} else if ($upgrade_eclass) {
	echo "Eclass System upgrade <BR>From: ";
	echo $current_eclass_version;
	echo "<BR>To: " . $eclass_package_version;
}else {
	echo "";
}

echo "</div>";

$id = $school_code;
$events = "Finish";
$status = "Success";
$refid = "200";

// Change Folder ownership
if( $systemType == "junior" ){
        $command2 = "sudo -u junior scripts/update.sh changeFolderOwnership $eclass_path junior";
        $ps = run_in_background("$command2");
}else{
        $command2 = "sudo -u eclass scripts/update.sh changeFolderOwnership $eclass_path/intranetIP eclass";
        $ps = run_in_background("$command2");

        $command3 = "sudo -u eclass scripts/update.sh changeFolderOwnership $eclass_path/eclass30 eclass";
        $ps = run_in_background("$command3");

        $command4 = "sudo -u eclass scripts/update.sh changeFolderOwnership $eclass_path/eclass40 eclass";
        $ps = run_in_background("$command4");

        $command5 = "sudo -u eclass scripts/update.sh changeFolderOwnership $eclass_path/Smarty eclass";
        $ps = run_in_background("$command5");

        $command6 = "sudo -u eclass scripts/update.sh clearEUfiles Log/intranettempfile" ;
        $ps = run_in_background("$command6");

        $command7 = "sudo -u eclass scripts/update.sh clearEUfiles Log/eclasstempfile" ;
        $ps = run_in_background("$command7");

}


?>
</td></tr>
</table>
<?
include_once("../../../templates/adminfooter.php");
?>
