<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libqb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libfilesystem.php");

intranet_opendb();

$li = new libfilesystem();
/*
$access_list = get_file_content($qb_access_list_file);
if ($access_list=="")
    $access_list = "0";

$groupList = explode(",",$access_list);
if (!in_array($GroupID,$groupList) || !($type==1 || $type==2 || $type==3) )
{
     header("Location: index.php");
     exit();
}
*/
$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
     header ("Location: index.php");
     exit();
}
$lq = new libqb();

$sql = "SELECT EngName, ChiName, DisplayOrder FROM ".$lq->settingTable[$type]." WHERE RelatedGroupID = '".$GroupID."' ORDER BY DisplayOrder";
$result = $lq->returnArray($sql,3);

$file_data = "English Name, Chinese Name, Display Order\n";
for ($i=0; $i<sizeof($result); $i++)
{
     $file_data .= ($result[$i][0].",".$result[$i][1].",".$result[$i][2]."\n");
}

$target_url = "/file/export/qb-".session_id()."-".time().".csv";
$li->file_write($file_data,$intranet_root.$target_url);
header("Location: $target_url");

?>