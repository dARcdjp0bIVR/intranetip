<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libqb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$uniqueID = is_array($uniqueID)? $uniqueID[0]:$uniqueID;
$lq = new libqb();
/*
$access_list = get_file_content($qb_access_list_file);
if ($access_list=="")
    $access_list = "0";

$GroupID = $lq->returnGroupID($uniqueID,$type);

$groupList = explode(",",$access_list);
if (!in_array($GroupID,$groupList))
{
     header("Location: index.php");
     exit();
}
*/
$lq->retrieveData($uniqueID,$type);
$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
     header("Location: index.php");
     exit();
}

?>
<SCRIPT LANGUAGE=javascript>
function checkform(obj){
         if (obj.engName.value == ""  && obj.chiName.value == "") {alert("<?=$i_QB_NameCantEmpty?>"); return false;}
         if (!check_int(obj.displayOrder,<?=$lq->displayOrder?>,NonIntegerWarning)) return false;

         return true;
}
</SCRIPT>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_plugin_qb, 'index.php', $lg->Title, 'javascript:history.back()', $button_edit." ".$lq->tableTitle[$type], '') ?>
<?= displayTag("head_qb_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=450 border=0 cellpadding=5 cellspacing=0>
<tr><td align="right"><?=$i_QB_EngName?>:</td><td><input type=text size=40 name=engName value='<?=$lq->engName?>'></td></tr>
<tr><td align="right"><?=$i_QB_ChiName?>:</td><td><input type=text size=40 name=chiName value='<?=$lq->chiName?>'></td></tr>
<tr><td align="right"><?=$i_QB_DisplayOrder?>:</td><td><input type=text size=5 name=displayOrder value='<?=$lq->displayOrder?>'></td></tr>
</table>
<br>
</blockquote>
<input type=hidden name=type value=<?=$type?>>
<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=uniqueID value=<?=$uniqueID?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>