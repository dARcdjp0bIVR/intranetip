<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libqb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lq = new libqb();
/*
$access_list = get_file_content($qb_access_list_file);
if ($access_list=="")
    $access_list = "0";

$groupList = explode(",",$access_list);
if (!in_array($GroupID,$groupList))
{
     header("Location: index.php");
     exit();
}
*/

$lg = new libgroup($GroupID);
if (!$lg->isAccessQB())
{
     header("Location: index.php");
     exit();
}
include_once("../../templates/adminheader_setting.php");
$next_order = $lq->newDisplayOrder($GroupID, $type) + 1;
?>

<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_adminmenu_plugin_qb, 'index.php', $lg->Title, 'javascript:history.back()', $button_import." ".$lq->tableTitle[$type], '') ?>
<?= displayTag("head_qb_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?=$i_QB_SelectFile?>:</td><td><input type=file name=userfile size=40></td></tr>
<tr><td colspan=2><br><a class=functionlink_new href="/home/school/qb_admin/sample_<?=$lq->linkname[$type]?>.csv"><?=$i_QB_DownloadSettingSample?></a></td></tr>
</table>
<br>
</blockquote>
<input type=hidden name=type value=<?=$type?>>
<input type=hidden name=GroupID value=<?=$GroupID?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>