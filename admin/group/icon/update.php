<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
include("../../../includes/libgroup.php");
include("../../../includes/libfilesystem.php");
intranet_opendb();

$li = new libgroup($GroupID);
$lo = new libfilesystem();

if($lo->lfs_copy("$file_path/images/admin/icons/$icon", "$file_path/file/group/g".$li->GroupID."_".time().".gif")){
	$lo->lfs_remove("$file_path/file/group/g".$li->GroupID."_".$li->DateModifiedTimeStamp.".gif");
	$RecordStatus = ($li->RecordStatus == 1 || $li->RecordStatus == 3) ? 3 : 2;
	$sql = "UPDATE INTRANET_GROUP SET RecordStatus = $RecordStatus, DateModified = now() WHERE GroupID = $GroupID";
	$li->db_db_query($sql);
}

intranet_closedb();
?>
<body onLoad="opener.window.location=opener.window.location;self.close()">
</body>
