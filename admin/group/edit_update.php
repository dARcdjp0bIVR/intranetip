<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/liborganization.php");
intranet_opendb();

$li = new libdb();
$lo = new libgroup($GroupID);
$oldName = $lo->Title;

if (isset($grouptools) && sizeof($grouptools)!=0)
{
    $grouptools = array_unique($grouptools);
    $grouptools = array_values($grouptools);
}


if($lo->RecordType<>$RecordType){
     $sql = "UPDATE INTRANET_USERGROUP SET RoleID = NULL WHERE GroupID = $GroupID";
     $li->db_db_query($sql);
}

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$AnnounceAllowed = ($AnnounceAllowed==1? 1:0);
if (!is_numeric($Quota) || $Quota < 0) $Quota = 5;
if ($alltools == 1)
{
    $functionAccess = "NULL";
}
else
{
    # Change the tool rights to integer
    $sum = 0;
    for ($i=0; $i<sizeof($grouptools); $i++)
    {
         $target = intval($grouptools[$i]);
         if ($target == 5)        # Question bank is for academic only
         {
             if ($RecordType == 2)
             {
                 $sum += pow(2,$target);
             }
         }
         else
         {
             $sum += pow(2,$target);
         }
    }
    $functionAccess = "'$sum'";
}

$RecordType += 0;
$fieldname  = "Title = '$Title', ";
$fieldname .= "Description = '$Description', ";
$fieldname .= "RecordType = '$RecordType', ";
$fieldname .= "StorageQuota = '$Quota',";
$fieldname .= "AnnounceAllowed = '$AnnounceAllowed',";
$fieldname .= "FunctionAccess = $functionAccess,";
$fieldname .= "DateModified = now()";
$sql = "UPDATE INTRANET_GROUP SET $fieldname WHERE GroupID = $GroupID";
$li->db_db_query($sql);

$li->UpdateRole_UserGroup();

# Map to INTRANET_CLASS
$sql = "UPDATE INTRANET_CLASS SET GroupID = NULL WHERE ClassName = '$oldName'";
$li->db_db_query($sql);
$sql = "UPDATE INTRANET_CLASS SET GroupID = $GroupID WHERE ClassName = '$Title'";
$li->db_db_query($sql);

# INTRANET_ORPAGE_GROUP
$lorg = new liborganization();
if ($hide==1)
{
    $lorg->setGroupHidden($GroupID);
}
else
{
    $lorg->setGroupDisplay($GroupID);
}
intranet_closedb();
header("Location: index.php?filter=$RecordType&msg=2");
?>