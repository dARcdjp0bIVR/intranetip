<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
intranet_opendb();
if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

/*
switch ($filter){
        case 0: $filter = 0; break;
        case 1: $filter = 1; break;
        case 2: $filter = 2; break;
        case 3: $filter = 3; break;
        case 4: $filter = 4; break;
        case 5: $filter = 5; break;
        case 6: $filter = 6; break;
        default: $filter = 0; break;
}
<input type=hidden name=filter value="">
*/
$li = new libgroup($GroupID);
$GroupUsers = $li->getNumberGroupUsers();
$toolbar = "";
if ($li->RecordType != -1) {                
    $toolbar .= "<a class=iconLink href=javascript:newWindow('add.php?GroupID=".$li->GroupID."',2)>".importIcon()."$button_import</a>\n".toolBarSpacer();
}
$toolbar .= ($GroupUsers == 0) ? emailIcon()."$button_email" : "<a class=iconLink href=javascript:checkGet(document.form1,'email.php')>".emailIcon()."$button_email</a>\n";
$toolbar .= toolBarSpacer();
#$toolbar .= ($GroupUsers == 0) ? groupIcon()."$i_frontpage_schoolinfo_groupinfo_group_members" : "<a class=iconLink href=javascript:checkGet(document.form1,'export.php')>".groupIcon()."$i_frontpage_schoolinfo_groupinfo_group_members</a>\n";
$toolbar .= ($GroupUsers == 0) ? exportIcon()."$button_export" : "<a class=iconLink href=javascript:checkGet(document.form1,'export.php');document.form1.action='index.php'>".exportIcon()."$button_export</a>\n";
$row = $li->returnRoleType();
$functionbar = "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$functionbar .= "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$functionbar .= "</select>\n";
$functionbar .= "<a href=\"javascript:checkRole(document.form1,'UserID[]','role.php')\"><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= ($li->RecordType == -1) ? "&nbsp;" : "<a href=\"javascript:checkRemove(document.form1,'UserID[]','delete.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$groupadminfunctionbar = "";

$subleader = get_file_content("$intranet_root/file/homework_leader.txt");
if ($subleader == 1 && $li->RecordType == 3) {                # Only Class has
    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UserID[]','performance.php')\">";
    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UserID[]','subjectleader.php')\"><img src='/images/admin/button/t_btn_assignsubjectleader_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_assignsubjectleader'></a>&nbsp;";
}
$groupadminfunctionbar .= "<input type=hidden name=adminflag value=0>";
$groupadminfunctionbar .= "<a href=\"javascript:document.form1.adminflag.value='A'; checkEdit(document.form1, 'UserID[]','assignadmin.php')\"><img src='/images/admin/button/t_btn_set_admin_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$groupadminfunctionbar .= "<a href=\"javascript:document.form1.adminflag.value='0'; checkRole(document.form1, 'UserID[]','setadmin.php')\"><img src='/images/admin/button/t_btn_cancel_admin_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
if ($li->RecordType == 5) {                # Only ECA has
    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UserID[]','performance.php')\">";
    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UserID[]','performance.php')\"><img src='/images/admin/button/t_btn_update_performance_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_updateperformance'></a>&nbsp;";
}

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$li->order = $order;
$li->field = $field;
$li->keyword = $keyword;
?>

<form name=form1 method=get>
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, "../index.php?filter=$filter", $li->Title, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar($toolbar, $functionbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("", $groupadminfunctionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->displayGroupUsers(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td><font color=red>*</font> - <?= $i_GroupKey ?> </td></tr>
<?
if ($li->RecordType == 3) {                # Only Class has
?>
<tr><td><font color=green>#</font> - <?= $i_Homework_SubjectLeaderKey ?> <p><br></p></td></tr>
<? } ?>
</table>
<input type=hidden name=GroupID value=<?php echo $li->GroupID; ?>>
<input type=hidden name=field value="<?=$field?>">
<input type=hidden name=order value="<?=$order?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include("../../../templates/adminfooter.php");
?>