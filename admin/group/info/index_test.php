<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
class libgroup extends libdb {

        var $Group;
        var $GroupID;
        var $Title;
        var $Description;
        var $RecordType;
        var $RecordStatus;
        var $DateInput;
        var $DateModified;
        var $DateModifiedTimeStamp;
        var $URL;
        var $StorageQuota;
        var $AnnounceAllowed;
        var $no_of_rows;
        var $page_size;
        var $total;
        var $pagename;
        var $pagePrev;
        var $pageNext;
        var $pageNo;
        var $new_navigation;
        var $order;
        var $field;
        var $field_array;
        var $keyword;
        var $questionBankExist;
        var $FunctionAccess;
        var $isAdmin;
        var $AdminAccessRight;
        var $CurrentUserID;
        var $form_name;
		var $PublicStatus;
		var $GroupLogoLink;
		var $IndexAnnounceNo;
		
        function libgroup($GroupID=""){
                global $page_size, $list_total, $list_page, $list_prev, $list_next, $pageNo,$list_sortby;
                $this->page_size = $page_size; #5*$page_size;
                $this->pageNo = (trim($pageNo) == "") ? 1 : $pageNo;
                $this->total = (trim($list_total) == "") ? "Total" : $list_total;
                $this->pagename = (trim($list_page) == "") ? "Page" : $list_page;
                $this->pagePrev = (trim($list_prev) == "") ? "Prev" : $list_prev;
                $this->pageNext = (trim($list_next) == "") ? "Next" : $list_next;
                $this->new_navigation = false;
                $this->sortby = (trim($list_sortby) == "") ? "Sort By" : $list_sortby;
                $this->isAdmin = false;
                $this->AdminAccessRight = "NULL";
                $this->CurrentUserID = "";
                $this->form_name = "form1";
                $this->libdb();
                if($GroupID<>""){
                        $this->Group = $this->returnGroup($GroupID);
                        $this->GroupID = $this->Group[0][0];
                        $this->Title = $this->Group[0][1];
                        $this->Description = $this->Group[0][2];
                        $this->RecordType = $this->Group[0][3];
                        $this->RecordStatus = $this->Group[0][4];
                        $this->DateInput = $this->Group[0][5];
                        $this->DateModified = $this->Group[0][6];
                        $this->DateModifiedTimeStamp = $this->Group[0][7];
                        $this->URL = $this->Group[0][8];
                        $this->StorageQuota = $this->Group[0][9];
                        $this->AnnounceAllowed = $this->Group[0][10];
                        $this->FunctionAccess = $this->Group[0][11];
                        $this->PublicStatus = $this->Group[0][12];
                        $this->GroupLogoLink = $this->Group[0][13];
                        if ($this->StorageQuota == "") $this->StorageQuota = 5;
                        $this->IndexAnnounceNo = $this->Group[0][14];
                        
                }
        }

        function returnGroup($GroupID){
            $sql = "SELECT GroupID, Title, Description, RecordType, RecordStatus, DateInput, DateModified, UNIX_TIMESTAMP(DateModified), URL, StorageQuota, AnnounceAllowed, IF(FunctionAccess IS NULL,'ALL',REVERSE(BIN(FunctionAccess))), PublicStatus, GroupLogoLink, IndexAnnounceNo FROM INTRANET_GROUP WHERE GroupID = '".$GroupID."'";
                return $this->returnArray($sql,15);
        }

        # ------------------------------------------------------------------------------------

        function returnCategoryName($gid="")
        {
                 if ($gid=="") $gid = $this->GroupID;
                 if ($gid=="") return "";
                 $sql = "SELECT CategoryName FROM INTRANET_GROUP_CATEGORY WHERE GroupCategoryID = '".$this->RecordType."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        function returnMemberInfo ($mem_id)
        {
                 $sql = "SELECT b.Title, a.Performance
                         FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_ROLE as b
                         ON a.RoleID = b.RoleID WHERE UserID = $mem_id AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnArray($sql,2);
                 return $result[0];
        }

        function returnGroupUser($special_order=""){
                global $i_status_approved, $i_status_suspended, $i_status_pendinguser;
                $field  = "DISTINCT a.UserID, a.UserLogin, a.UserEmail, a.FirstName, a.LastName, ";
                $field .= "IF(a.RecordStatus='1','$i_status_approved',IF(a.RecordStatus='2','$i_status_pendinguser','$i_status_suspended')), ";
                $field .= "IFNULL(c.Title, '-'), a.UserPassword, a.URL, b.RecordType, a.ClassName, a.ClassNumber, a.EnglishName, a.ChineseName, b.Performance ";
                if ($this->RecordType == 3)
                    $field .= ", IF(d.LeaderID IS NULL,0,1) ";
                $sql  = "SELECT $field FROM INTRANET_USER AS a LEFT OUTER JOIN INTRANET_USERGROUP AS b ON a.UserID = b.UserID AND b.GroupID = '".$this->GroupID."' ";
                $sql .= "LEFT OUTER JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID ";
                if ($this->RecordType == 3)
                    $sql .= "LEFT OUTER JOIN INTRANET_SUBJECT_LEADER AS d ON a.UserID = d.UserID AND d.ClassID = '".$this->GroupID."' ";
                #$sql .= "WHERE b.GroupID = ".$this->GroupID;
                $sql .= "WHERE b.GroupID IS NOT NULL AND b.GroupID = '".$this->GroupID."'";
                $keyword = $this->keyword;
                $sql .= " AND (
                          (a.UserLogin LIKE '%$keyword%') OR
                          (a.UserEmail LIKE '%$keyword%') OR
                          (a.EnglishName LIKE '%$keyword%') OR
                          (a.ChineseName LIKE '%$keyword%') OR
                          (a.ClassName LIKE '%$keyword%') OR
                          (a.ClassNumber LIKE '%$keyword%') OR
                          (b.Performance LIKE '%$keyword%') )
                ";
                $sql .= $special_order;
                $cols = ($this->RecordType==3? 16: 15);
                echo "sql [".$sql."]<br>";
                return $this->returnArray($sql,$cols);
                
        }

        function returnGroupUser4Display($order=""){
                global $i_status_approved, $i_status_suspended, $i_status_pendinguser;
                $username_field = getNameFieldWithClassNumberByLang("a.");

                $field  = "a.UserID, a.UserLogin, a.UserEmail, $username_field, ";
                $field .= "IF(a.RecordStatus='1','$i_status_approved',IF(a.RecordStatus='2','$i_status_pendinguser','$i_status_suspended')), ";
                $field .= "IFNULL(c.Title, '-'), a.UserPassword, a.URL, b.RecordType, a.ClassName, a.ClassNumber, a.EnglishName, a.ChineseName ";
                $sql  = "SELECT $field FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b ";
                $sql .= "LEFT OUTER JOIN INTRANET_ROLE AS c ON b.RoleID = c.RoleID ";
                $sql .= "WHERE a.UserID = b.UserID AND a.RecordStatus IN (0,1,2) AND b.GroupID = ".$this->GroupID;
                $sql .= $order;
                return $this->returnArray($sql,13);
        }

        function returnGroupAnnouncement(){
            $sql  = "SELECT a.AnnouncementID, a.Title FROM INTRANET_ANNOUNCEMENT AS a, INTRANET_GROUPANNOUNCEMENT AS b WHERE a.RecordStatus = '1' AND a.AnnouncementID = b.AnnouncementID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,2);
        }

        function returnGroupEvent(){
            $sql  = "SELECT a.EventID, a.EventDate, a.Title FROM INTRANET_EVENT AS a, INTRANET_GROUPEVENT AS b WHERE a.RecordStatus = '1' AND a.EventID = b.EventID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,3);
        }

        function returnGroupPolling(){
            $sql  = "SELECT a.PollingID, a.Question FROM INTRANET_POLLING AS a, INTRANET_GROUPPOLLING AS b WHERE a.PollingID = b.PollingID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,2);
        }

        function returnGroupTimetable(){
            $sql  = "SELECT a.TimetableID, a.Title, a.Description, a.URL FROM INTRANET_TIMETABLE AS a, INTRANET_GROUPTIMETABLE AS b WHERE a.RecordStatus = '1' AND a.TimetableID = b.TimetableID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,4);
        }

        function returnGroupResource(){
            $sql  = "SELECT a.ResourceID, a.ResourceCode, a.Title FROM INTRANET_RESOURCE AS a, INTRANET_GROUPRESOURCE AS b WHERE a.RecordStatus = '1' AND a.ResourceID = b.ResourceID AND b.GroupID = '".$this->GroupID."'";
                return $this->returnArray($sql,3);
        }

        function returnGroupUsedStorageQuota (){
                 $sql = "SELECT SUM(Size) FROM INTRANET_FILE WHERE GroupID='".$this->GroupID."'";
                 $result = $this->returnArray($sql,1);
                 return $result[0][0];
        }

        function returnGroupsByCategory($category="")
        {
                 $conds = ($category==""? "" :" WHERE RecordType = $category");
                 $sql = "SELECT GroupID, Title FROM INTRANET_GROUP $conds ORDER BY Title";
                 return $this->returnArray($sql,2);
        }

        # ------------------------------------------------------------------------------------

        function getNumberGroupUsers(){
                return sizeof($this->returnGroupUser());
        }

        function getNumberGroupAnnouncement(){
                return sizeof($this->returnGroupAnnouncement());
        }

        function getNumberGroupEvent(){
                return sizeof($this->returnGroupEvent());
        }

        function getNumberGroupPolling(){
                return sizeof($this->returnGroupPolling());
        }

        function getNumberGroupTimetable(){
                return sizeof($this->returnGroupTimetable());
        }

        function getNumberGroupResource(){
                return sizeof($this->returnGroupResource());
        }

        # ------------------------------------------------------------------------------------

        function check($id){
                $x = "<input type=checkbox onClick=(this.checked)?setChecked(1,document.form1,'$id'):setChecked(0,document.form1,'$id')>";
                return $x;
        }

        function displayFunctionbar($a="", $b=""){
                return "<table width=100% border=0 cellpadding=1 cellspacing=0><tr><td>$a</td><td align=right>$b</td></tr></table>\n";
        }

        function column($field_index, $field_name){
                global $image_path;
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<img src='$image_path/desc.gif' hspace=2 border=0>";
                        if($this->order==0) $x .= "<img src='$image_path/asc.gif' hspace=2  border=0>";
                }
                $x .= "</a>";
                return $x;
        }
        
        function column_ip20($field_index, $field_name){
                global $image_path, $LAYOUT_SKIN; 
                $x = "";
                if($this->field==$field_index){
                        if($this->order==1) $x .= "<a class=\"tabletoplink\" href=javascript:sortPage(0,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                        if($this->order==0) $x .= "<a class=\"tabletoplink\" href=javascript:sortPage(1,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }else{
                        $x .= "<a class=\"tabletoplink\" href=javascript:sortPage($this->order,$field_index,document.form1) onMouseOver=\"window.status='".$this->sortby." $field_name';return true;\" onMouseOut=\"window.status='';return true;\">";
                }
                $x .= str_replace("_", " ", $field_name);
                if($this->field==$field_index){
					if($this->order==1) $x .= "<img name='sort_icon' id='sort_icon' src='$image_path/{$LAYOUT_SKIN}/icon_sort_a_off.gif' align='absmiddle' border='0' />";
					if($this->order==0) $x .= "<img name='sort_icon' id='sort_icon' onMouseOver=\"MM_swapImage('sort_asc','','$image_path/{$LAYOUT_SKIN}/icon_sort_d_on.gif',1)\" onMouseOut='MM_swapImgRestore()' src='$image_path/{$LAYOUT_SKIN}/icon_sort_d_off.gif' align='absmiddle'  border='0' />";
                }

                $x .= "</a>";
                return $x;
        }
        
        function navigation(){
                global $image_path;
                $x .= $this->record_range();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_prev();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->nav_next();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_page();
                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }
                return $x;
        }
        function navigation_ip20(){
                global $image_path;
                
                $x .= "<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
                $x .= "<tr><td class='tabletext' align='left'>";
                
                $x .= "<table cellspacing='0' cellpadding='0' border='0'><tr><td class='tabletext'>";
                $x .= $this->record_range();
                $x .= "</td></tr></table>";
                
                $x .= "</td><td align='right'>";
                
                $x .= "<table cellspacing='0' cellpadding='0' border='0'><tr><td>";
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= "</td><td class='tabletext'>".$this->nav_prev_ip20()."</td>";
                $x .= "<td class='tabletext'>".$this->nav_page()."</td>";                
                $x .= "<td class='tabletext'>".$this->nav_next_ip20()."</td>";
                
                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<td><img src=$image_path/space.gif width=10 height=10 border=0></td>\n";
                    $x .= "<td class='tabletext'>".$this->page_size_input()."</td>";
                }
                $x .= "</tr></table>";
                
                $x .= "</td>";
                $x .= "</tr></table>";
                
                return $x;
        }
        
        /*
        function navigation2(){
                global $image_path;
                $x .= $this->record_range();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_prev();
                $x .= "<img src=$image_path/space.gif width=5 height=10 border=0>\n";
                $x .= $this->nav_next();
                $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                $x .= $this->nav_page();
                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }
                return $x;
        }
        */
        # For Alumni
        function prev_n2(){
                global $image_path,$image_pre;
                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $previous_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_l.gif' width='13' height='13' align='absmiddle'>";
                else
                    $previous_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_l.gif' width='13' height='13' align='absmiddle'>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? "<span >".$previous_icon.$this->pagePrev."</span>\n" : "<a  href=javascript:"."gopage(".($n_page-1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }
        function next_n2(){
                global $image_path,$image_next;
                if ($this->IsColOff == 2 || $this->IsColOff == 5)
                    $next_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_r.gif' width='13' height='13' align='absmiddle'>";
                else
                    $next_icon = "<img border=0 hspace=2 vspace=0 align=middle src='$image_path/arrow_r.gif' width='13' height='13' align='absmiddle'>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->total_row/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? "<span >".$this->pageNext.$next_icon."</span>\n" : "<a href=javascript:"."gopage(".($n_page+1).",document.".$this->form_name.") onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }
        function navigation2(){
                global $image_path;

                                $x = "<table width='90%' border='0' cellspacing='0' cellpadding='0'>";
                                $x .= "<tr>";
                $x .= "<td width='10'><img src='$image_path/page_l.gif' width='10' height='34'></td>";
                $x .= "<td align='center' class='alumni_next_page_cellbg' valign='middle'>";
                $x .= $this->record_range();
                $x .= $this->prev_n2();
                $x .= $this->nav_page();
                $x .= $this->next_n2();

                global $pageSizeChangeEnabled;
                if ($pageSizeChangeEnabled)
                {
                    $x .= "<img src=$image_path/space.gif width=10 height=10 border=0>\n";
                    $x .= $this->page_size_input();
                }

                                $x .= "</td>";
                $x .= "<td width='10'><img src='$image_path/page_r.gif' width='10' height='34'></td></tr>";
                                $x .= "</table>";

                return $x;
        }
        function page_size_input ()
        {
                 global $i_general_EachDisplay ,$i_general_PerPage;

                 $x = "$i_general_EachDisplay <select name='num_per_page' onChange='this.form.pageNo.value=1;this.form.page_size_change.value=1;this.form.numPerPage.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                 $x .= "<option value=10 ".($this->page_size==10? "SELECTED":"").">10</option>\n";
                 $x .= "<option value=20 ".($this->page_size==20? "SELECTED":"").">20</option>\n";
                 $x .= "<option value=30 ".($this->page_size==30? "SELECTED":"").">30</option>\n";
                 $x .= "<option value=40 ".($this->page_size==40? "SELECTED":"").">40</option>\n";
                 $x .= "<option value=50 ".($this->page_size==50? "SELECTED":"").">50</option>\n";
                 $x .= "<option value=60 ".($this->page_size==60? "SELECTED":"").">60</option>\n";
                 $x .= "<option value=70 ".($this->page_size==70? "SELECTED":"").">70</option>\n";
                 $x .= "<option value=80 ".($this->page_size==80? "SELECTED":"").">80</option>\n";
                 $x .= "<option value=90 ".($this->page_size==90? "SELECTED":"").">90</option>\n";
                 $x .= "<option value=100 ".($this->page_size==100? "SELECTED":"").">100</option>\n";
                 $x .= "</select>$i_general_PerPage\n";
                 return $x;
        }

        function record_range(){
                $n_title = $this->title;
                $n_start = ($this->pageNo-1)*$this->page_size+1;
                $n_end = min($this->no_of_rows, ($this->pageNo*$this->page_size));
                $n_total = $this->no_of_rows;
                $x = "$n_title $n_start - $n_end, ".$this->total." $n_total\n";
                return $x;
        }

        function nav_prev(){
                global $image_path,$image_pre;
                if ($this->new_navigation)
                    $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
                else
                    $previous_icon = "<img src=$image_path/previous_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? $previous_icon.$this->pagePrev."\n" : "<a href=javascript:gopage(".($n_page-1).",document.form1) onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon.$this->pagePrev."</a>\n";
                return $x;
        }
        
        function nav_prev_ip20(){
                global $image_path,$image_pre, $LAYOUT_SKIN;
                if ($this->new_navigation)
                    $previous_icon = "<img src=$image_pre border=0 hspace=2 vspace=0 align=middle>";
                else
                    $previous_icon = "<img src=$image_path/$LAYOUT_SKIN/icon_prev_off.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==1) ? $previous_icon."\n" : "<a href=javascript:gopage(".($n_page-1).",document.form1) onMouseOver=\"window.status='".$this->pagePrev."';return true;\" onMouseOut=\"window.status='';return true;\">".$previous_icon."</a>\n";
                return $x;
        }

        function nav_next(){
                global $image_path,$image_next;
                if ($this->new_navigation)
                    $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
                else
                    $next_icon = "<img src=$image_path/next_icon.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? $this->pageNext.$next_icon."\n" : "<a href=javascript:gopage(".($n_page+1).",document.form1) onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$this->pageNext.$next_icon."</a>\n";
                return $x;
        }
        
        function nav_next_ip20(){
                global $image_path,$image_next, $LAYOUT_SKIN;
                if ($this->new_navigation)
                    $next_icon = "<img src=$image_next border=0 hspace=2 vspace=0 align=middle>";
                else
                    $next_icon = "<img src=$image_path/$LAYOUT_SKIN/icon_next_off.gif border=0 hspace=2 vspace=0 align=middle>";
                $n_page = $this->pageNo;
                $n_total = ceil($this->no_of_rows/$this->page_size);
                $n_size = $this->page_size;
                $x = ($n_page==$n_total) ? $next_icon."\n" : "<a href=javascript:gopage(".($n_page+1).",document.form1) onMouseOver=\"window.status='".$this->pageNext."';return true;\" onMouseOut=\"window.status='';return true;\">".$next_icon."</a>\n";
                return $x;
        }

        function nav_page(){
                $x  = $this->pagename;
                $x .= " <select name=pageNoSelect onChange='this.form.pageNo.value=this.options[this.selectedIndex].value;this.form.submit();'>\n";
                for($i=1; $i<=ceil($this->no_of_rows/$this->page_size); $i++)
                $x .= "<option value=$i ".(($this->pageNo==$i)?"SELECTED":"").">$i</option>\n";
                $x .= "</select>\n";
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function displayGroupUsers(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                global $i_UserEnglishName, $i_UserChineseName,$i_ClassNameNumber;
                global $i_ActivityPerformance;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","b.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "15%";
                    $width_login = "15%";
                    $width_eng = "30%";
                    $width_chi = "15%";
                    $width_class = "10%";
                    $width_perform = "15%";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "15%";
                    $width_login = "20%";
                    $width_eng = "30%";
                    $width_chi = "20%";
                    $width_class = "15%";
                    $total_col = 7;
                }
                $x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
                $x .= "<tr>";
                $x .= "<td class=tableTitle width=1>#</td>\n";
                $x .= "<td class=tableTitle width=$width_role>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td class=tableTitle width=$width_login>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td class=tableTitle width=$width_eng>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td class=tableTitle width=$width_chi>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td class=tableTitle width=$width_class>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                if ($this->RecordType == 5)
                {
                    $x .= "<td class=tableTitle width=$width_perform>".$this->column(5,$i_ActivityPerformance)."</td>\n";
                }
                $x .= "<td class=tableTitle width=1>".$this->check("UserID[]")."</td>\n";
                $x .= "</tr>\n";
                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=$total_col class=tableContent><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $UserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $isSubjectLeader = ($row[$i][15]==1?"<font color=green>#</font>":"");
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $EnglishName = ($EnglishName=="") ? "&nbsp;" : $EnglishName;
                                                $ChineseName = ($ChineseName=="") ? "&nbsp;" : $ChineseName;
                                                $x .= "<tr>";
                        $x .= "<td class=tableContent>".($i+1)."</td>\n";
                        $x .= "<td class=tableContent>$RoleTitle $isAdmin $isSubjectLeader</td>\n";
                        $x .= "<td class=tableContent>$UserLogin</td>\n";
                        $x .= "<td class=tableContent>$EnglishName</td>\n";
                        $x .= "<td class=tableContent>$ChineseName</td>\n";
                        $x .= "<td class=tableContent>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class=tableContent>$Performance</td>\n";
                        }
                        $x .= "<td class=tableContent><input type=checkbox name=UserID[] value=$UserID></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= (sizeof($row)==0) ? "" : "<tr><td style='vertical-align:middle' align=right colspan=$total_col>".$this->navigation()."</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=pageNo value=\"$this->pageNo\">";
                return $x;
        }
        
        function displayGroupUsers_ip20(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                global $i_UserEnglishName, $i_UserChineseName,$i_ClassNameNumber;
                global $i_ActivityPerformance;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","a.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "15%";
                    $width_login = "15%";
                    $width_eng = "30%";
                    $width_chi = "15%";
                    $width_class = "10%";
                    $width_perform = "15%";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "15%";
                    $width_login = "20%";
                    $width_eng = "30%";
                    $width_chi = "20%";
                    $width_class = "15%";
                    $total_col = 7;
                }
                $x .= "<table width='96%' border='0' cellpadding='4' cellspacing='0' align='center'>\n";
                $x .= "<tr class='tabletop'>";
                $x .= "<td class='tabletext' width='1'><span class='tabletoplink'>#</span></td>\n";
                $x .= "<td class='tabletext' width='$width_role'>".$this->column_ip20(0,$i_admintitle_role)."</td>\n";
                $x .= "<td class='tabletext' width='$width_login'>".$this->column_ip20(1,$i_UserLogin)."</td>\n";
                $x .= "<td class='tabletext' width='$width_eng'>".$this->column_ip20(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td class='tabletext' width='$width_chi'>".$this->column_ip20(3,$i_UserChineseName)."</td>\n";
                $x .= "<td class='tabletext' width='$width_class'>".$this->column_ip20(4,$i_ClassNameNumber)."</td>\n";
                if ($this->RecordType == 5) 
                {
                    $x .= "<td class='tableTitle' width='$width_perform'>".$this->column_ip20(5,$i_ActivityPerformance)."</td>\n";
                }
                $x .= "<td class='tableTitle' width='1'>".$this->check("UID[]")."</td>\n";
                $x .= "</tr>\n";
                $x .= (sizeof($row)==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $UID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $isSubjectLeader = ($row[$i][15]==1?"<font color=green>#</font>":"");
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $EnglishName = ($EnglishName=="") ? "&nbsp;" : $EnglishName;
                                                $ChineseName = ($ChineseName=="") ? "&nbsp;" : $ChineseName;
                                                $x .= "<tr class='tablerow".(($i % 2) + 1)."'>";
                        $x .= "<td class='tabletext'>".($i+1)."</td>\n";
                        $x .= "<td class='tabletext'>$RoleTitle $isAdmin $isSubjectLeader</td>\n";
                        $x .= "<td class='tabletext'>$UserLogin</td>\n";
                        $x .= "<td class='tabletext'>$EnglishName</td>\n";
                        $x .= "<td class='tabletext'>$ChineseName</td>\n";
                        $x .= "<td class='tabletext'>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class='tabletext'>$Performance</td>\n";
                        }
                        $x .= "<td class='tabletext'><input type='checkbox' name='UID[]' value='$UID'></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= (sizeof($row)==0) ? "" : "<tr><td style='vertical-align:middle' align='right' colspan='$total_col' class='tablebottom'>".$this->navigation_ip20()."</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type='hidden' name='pageNo' value=\"$this->pageNo\">";
                return $x;
        }

        function displayGroupUsersForAdmin(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus,$i_ClassNameNumber;
                global $i_UserEnglishName, $i_UserChineseName,$i_ActivityPerformance;
                global $image_path;
                $this->new_navigation = true;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","a.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "100";
                    $width_login = "90";
                    $width_eng = "140";
                    $width_chi = "110";
                    $width_class = "90";
                    $width_perform = "150";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "100";
                    $width_login = "100";
                    $width_eng = "205";
                    $width_chi = "185";
                    $width_class = "90";
                    $total_col = 7;
                }
                /*$x .= "<table width=100% border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0\">";
                $x .= "<tr align=left >";
                $x .= "<td bgcolor=#FCD5AE width=20 class=title_head align=center width=1>#</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_role class=title_head>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_login class=title_head>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_eng class=title_head>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_chi class=title_head>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td bgcolor=#FCD5AE width=$width_class class=title_head>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                */
                
                $x .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
                $x .= "<tr class=forumtablerow align=left >";
                $x .= "<td width=20 class=forumtabletop forumtabletoptext align=center width=1>#</td>\n";
                $x .= "<td width=$width_role class=forumtabletop forumtabletoptext>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td width=$width_login class=forumtabletop forumtabletoptext>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td width=$width_eng class=forumtabletop forumtabletoptext>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td width=$width_chi class=forumtabletop forumtabletoptext>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td width=$width_class class=forumtabletop forumtabletoptext>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                
                if ($this->RecordType == 5)
                {
                    $x .= "<td width=$width_perform class=forumtabletop forumtabletoptext>".$this->column(5,$i_ActivityPerformance)."</td>\n";
                }

                $x .= "<td width=20 class=forumtabletop forumtabletoptext>".$this->check("targetUserID[]")."</td>\n";
                $x .= "</tr>\n";

                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=$total_col class=body><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $UserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $x .= "<tr align=left valign=top>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>".($i+1)."</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$RoleTitle $isAdmin</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$UserLogin</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$EnglishName &nbsp;</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$ChineseName &nbsp;</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class='albumtablerow tabletext' align='center'>$Performance</td>\n";
                        }
 //                        $x .= "<td width=150 class=body>$UserEmail</td>\n";
                        $x .= "<td class='albumtablerow tabletext' align='center'><input type=checkbox name=targetUserID[] value=$UserID></td>\n";
#                        $x .= "<td class=h1 >&nbsp;</td>\n";
                        $x .= "</tr>\n";
                }

                $x .= (sizeof($row)==0) ? "" : "<tr><td colspan=3>&nbsp;</td><td bgcolor=#FFE6BC style='vertical-align:middle' align=center colspan=".($total_col-4).">".$this->navigation()."</td><td>&nbsp;</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=pageNo value=\"$this->pageNo\">";
                return $x;
        }
        function displayGroupUsersForAdmin1(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus,$i_ClassNameNumber;
                global $i_UserEnglishName, $i_UserChineseName,$i_ActivityPerformance;
                global $image_path;
                $this->new_navigation = true;
                $this->field_array = array("c.Title","a.UserLogin","a.EnglishName","a.ChineseName","a.ClassName,a.ClassNumber","a.Performance");
                if ((($this->field !== 0) && ($this->field == "")) || $this->field >= sizeof($this->field_array))
                {
                    $order_str = "a.ClassName, a.ClassNumber, a.EnglishName";
                }
                else
                {
                    $order_str = $this->field_array[$this->field];
                }
                $this->order += 0;
                if ($this->order == 0)
                {
                    $order_type = " DESC";
                }
                else
                {
                    $order_type = " ASC";
                }
                $order_str = str_replace(",","$order_type,",$order_str);
                $order_str .= $order_type;
                $row = $this->returnGroupUser(" ORDER BY $order_str");
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                if ($this->RecordType == 5)
                {
                    $width_role = "100";
                    $width_login = "90";
                    $width_eng = "140";
                    $width_chi = "110";
                    $width_class = "90";
                    $width_perform = "150";
                    $total_col = 8;
                }
                else
                {
                    $width_role = "100";
                    $width_login = "100";
                    $width_eng = "205";
                    $width_chi = "185";
                    $width_class = "90";
                    $total_col = 7;
                }
                $x .= "<table width=100% border=1 cellpadding=2 cellspacing=0 bordercolorlight=#5DA5C9 bordercolordark=#FFFFFF bgcolor=#FFFFFF class=13-black>";
                $x .= "<tr bgcolor=#E3DB9C>";

                $x .= "<table width=100% border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0\">";
                $x .= "<tr align=left >";
                $x .= "<td bgcolor=\"#E3DB9C\" width=20 class=13-black-bold align=center width=1>#</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_role class=13-black-bold>".$this->column(0,$i_admintitle_role)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_login class=13-black-bold>".$this->column(1,$i_UserLogin)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_eng class=13-black-bold>".$this->column(2,$i_UserEnglishName)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_chi class=13-black-bold>".$this->column(3,$i_UserChineseName)."</td>\n";
                $x .= "<td bgcolor=\"#E3DB9C\" width=$width_class class=13-black-bold>".$this->column(4,$i_ClassNameNumber)."</td>\n";
                if ($this->RecordType == 5)
                {
                    $x .= "<td bgcolor=\"#E3DB9C\" width=$width_perform class=13-black-bold>".$this->column(5,$i_ActivityPerformance)."</td>\n";
                }

                $x .= "<td bgcolor=\"#E3DB9C\" width=20 class=13-black-bold>".$this->check("targetUserID[]")."</td>\n";
                $x .= "</tr>\n";

                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=$total_col class=body><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $UserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RoleTitle = $row[$i][6];
                        $isAdmin = ($row[$i][9]=='A'? "<font color=red>*</font>":"");
                        $class = $row[$i][10];
                        $classno = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $ChineseName = $row[$i][13];
                        $Performance = $row[$i][14];
                        if ($Performance == "") $Performance = "-";
                        $class_display = ($class != "" && $classno != "")? "$class ($classno)":"-";
                        $css = ($i%2?"":"2");
                        $x .= "<tr class=tableContent$css align=left valign=top>\n";
                        $x .= "<td class=body align=center>".($i+1)."</td>\n";
                        $x .= "<td class=body>$RoleTitle $isAdmin</td>\n";
                        $x .= "<td class=body>$UserLogin</td>\n";
                        $x .= "<td class=body>$EnglishName &nbsp;</td>\n";
                        $x .= "<td class=body>$ChineseName &nbsp;</td>\n";
                        $x .= "<td class=body>$class_display</td>\n";
                        if ($this->RecordType == 5)
                        {
                            $x .= "<td class=body>$Performance</td>\n";
                        }
 //                        $x .= "<td width=150 class=body>$UserEmail</td>\n";
                        $x .= "<td ><input type=checkbox name=targetUserID[] value=$UserID></td>\n";
#                        $x .= "<td class=h1 >&nbsp;</td>\n";
                        $x .= "</tr>\n";
                }

                #$x .= (sizeof($row)==0) ? "" : "<tr><td colspan=3>&nbsp;</td><td bgcolor=#FFE6BC style='vertical-align:middle' align=center colspan=".($total_col-4).">".$this->navigation2()."</td><td>&nbsp;</td></tr>\n";
                $x .= "</table>\n";
                $x .= "<input type=hidden name=pageNo value=\"$this->pageNo\">";
                return $x;
        }
        # ------------------------------------------------------------------------------------

        function displayGroupAnnouncement(){
                return $this->displayRow($this->returnGroupAnnouncement());
        }

        function displayGroupEvent(){
                return $this->displayRow($this->returnGroupEvent());
        }

        function displayGroupPolling(){
                return $this->displayRow($this->returnGroupPolling());
        }

        function displayGroupTimetable(){
                return $this->displayRow($this->returnGroupTimetable());
        }

        function displayGroupResource(){
                return $this->displayRow($this->returnGroupResource());
        }

        # ------------------------------------------------------------------------------------

        function displayRow($row){
                for($i=0; $i<sizeof($row); $i++){
                        $x .= "".implode(":", $row[$i])."<br>\n";
                }
                return $x;
        }

        function displayGroupUsersEmailOption(){
                $row = $this->returnGroupUser(" ORDER BY a.UserEmail ASC");
                for($i=0; $i<sizeof($row); $i++){
                        $UserEmail = $row[$i][2];
                        $x .= "<option value=$UserEmail SELECTED>$UserEmail</option>\n";
                }
                return $x;
        }

        function displayGroupUsersPassword(){
                $row = $this->returnGroupUser(" ORDER BY a.UserLogin ASC");
                $x = "UserLogin ; EnglishName ; ChineseName ; ClassName ; ClassNumber ; UserEmail\n";
                for($i=0; $i<sizeof($row); $i++){
                        $UserLogin = $row[$i][1];
                        $UserEmail = $row[$i][2];
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $UserPassword = $row[$i][7];
                        $ClassName = $row[$i][10];
                        $ClassNumber = $row[$i][11];
                        $EnglishName = $row[$i][12];
                        $Chinesename = $row[$i][13];
                        $x .= "$UserLogin ; $EnglishName ; $ChineseName ; $ClassName ; $ClassNumber ; $UserEmail\n";
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------
/*
        function displayGroupImport(){
                global $i_no_record_exists_msg;
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                $row = $this->returnGroupUser();
                $this->no_of_rows = sizeof($row);
                $this->pageNo = min($this->pageNo, ceil($this->no_of_rows/$this->page_size));
                $start = max((($this->pageNo-1)*$this->page_size), 0);
                $end = min($start+$this->page_size, $this->no_of_rows);
                $x .= "<table width=95% border=0 cellpadding=2 cellspacing=1>\n";
                $x .= "<tr>";
                $x .= "<td class=tableTitle width=1>#</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_admintitle_role</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserLogin</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserFirstName</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserLastName</td>\n";
                $x .= "<td class=tableTitle width=25%>$i_UserEmail</td>\n";
                $x .= "<td class=tableTitle width=15%>$i_UserRecordStatus</td>\n";
                $x .= "<td class=tableTitle width=1>".$this->check("UserID[]")."</td>\n";
                $x .= "</tr>\n";
                $x .= (sizeof($row)==0) ? "<tr><td align=center colspan=8 class=tableContent><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
                for($i=$start; $i<$end; $i++){
                        $UserID = $row[$i][0];
                        $UserLogin = $row[$i][1];
                        $UserEmail = $this->convertAllLinks($row[$i][2]);
                        $FirstName = $row[$i][3];
                        $LastName = $row[$i][4];
                        $RecordStatus = $row[$i][5];
                        $RoleTitle = $row[$i][6];
                        $x .= "<tr>";
                        $x .= "<td class=tableContent>".($i+1)."</td>\n";
                        $x .= "<td class=tableContent>$RoleTitle</td>\n";
                        $x .= "<td class=tableContent>$UserLogin</td>\n";
                        $x .= "<td class=tableContent>$FirstName</td>\n";
                        $x .= "<td class=tableContent>$LastName</td>\n";
                        $x .= "<td class=tableContent>$UserEmail</td>\n";
                        $x .= "<td class=tableContent><select name=memberType_$UserID><option value=S>S</option><option value=T>T</option><option value=A>A</option></select></td>\n";
                        $x .= "<td class=tableContent><input type=checkbox name=UserID[] value=$UserID CHECKED></td>\n";
                        $x .= "</tr>\n";
                }
                $x .= (sizeof($row)==0) ? "" : "<tr><td style='vertical-align:middle' align=right colspan=8>".$this->navigation()."</td></tr>\n";
                $x .= "</table>\n";
                return $x;
        }
*/
        # ------------------------------------------------------------------------------------

        function returnRoleType(){
                $sql = "SELECT RoleID, Title, RecordStatus FROM INTRANET_ROLE WHERE RecordType = ".$this->RecordType;
                return $this->returnArray($sql,3);
        }
        
        function returnGroupRoleType(){
                $sql = "SELECT RoleID, Title, RecordStatus FROM INTRANET_ROLE WHERE RecordType = 5";
                return $this->returnArray($sql,3);
        }

        # ------------------------------------------------------------------------------------

        function displayDirectoryUsers(){
                global $i_admintitle_role, $i_UserLogin, $i_UserFirstName, $i_UserLastName, $i_UserEmail, $i_UserRecordStatus;
                global $hideEmail;                       # Display Option
                global $i_status_approved;
                $row = $this->returnGroupUser4Display(" ORDER BY c.title, a.ClassName, a.ClassNumber, a.EnglishName");
                $cols = 2;
                $width = 100/$cols;
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= $i_no_record_exists_msg."\n";
                }else{
                        $x .= "<table width=90% border=1 cellspacing=0 cellpadding=5 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5>\n";
                        $x .= "<tr>\n";
                        $row_count = 0;
                        for($i=0; $i<sizeof($row); $i++){
                                $UserID = $row[$i][0];
                                $UserLogin = $row[$i][1];
                                $UserEmail = $row[$i][2];
                                $Username = $row[$i][3];
                                $RecordStatus = $row[$i][4];
                                $RoleTitle = $row[$i][5];
                                $URL = $row[$i][7];
                                $convertedURL = $this->returnImageIconForURL($URL);

                                if ($RecordStatus != $i_status_approved)
                                {
                                    $Username = "$Username <i>($RecordStatus)</i>";
                                }

                                if ($RoleTitle!=$row[$i-1][5])
                                {
                                    if ($row_count % $cols)
                                    {
                                        $x .= "<td width=$width%>&nbsp;</td>";
                                    }
                                    $x .= "</tr>\n<tr>\n<td colspan=$cols><b>$RoleTitle</b></td>\n</tr>\n<tr>\n";
                                    $row_count = 0;
                                }
                                if ($i!=0 && $row_count%$cols==0)
                                {
                                    $x .= "</tr><tr>";
                                    $row_count = 0;
                                }
                                $row_count++;
                                if ($hideEmail)
                                {
                                    $x .= "<td width=$width%>$Username  $convertedURL </td>\n";
                                }
                                else
                                {
                                    $x .= "<td width=$width%><a href=mailto:$UserEmail>$Username </a> $convertedURL </td>\n";
                                }
                        }
                        if ($row_count % $cols)
                        {
                            $x .= "<td width=$width%>&nbsp;</td>\n";
                        }
                        $x .= "</tr>\n";
                        $x .= "</table>\n";
                }
                return $x;
        }

        # ------------------------------------------------------------------------------------

        function showGroupTimetable(){
                $row = $this->returnGroupTimetable();
                if(sizeof($row)==0){
                        global $i_no_record_exists_msg;
                        $x .= $i_no_record_exists_msg."\n";
                }else{
                        $x .= "<table width=90% border=0 cellpadding=2 cellspacing=1>\n";
                        for($i=0; $i<sizeof($row); $i++){
                                $TimetableID = $row[$i][0];
                                $Title = $row[$i][1];
                                $Description = nl2br($this->convertAllLinks2($row[$i][2]));
                                $Url = str_replace(" ","%20",$row[$i][3]);
                                if($this->validateURL($Url)){
                                        $Url = "<a href=".$Url.">$Title</a>\n";
                                } else if ($Url<>"") {
                                        $Url = "<a href=/file/timetable".$Url.">$Title</a>\n";
                                } else {
                                        $Url = $Title;
                                }
                                $x .= "<tr>";
                                $x .= "<td>$Url</td>\n";
                                $x .= "<td>$Description</td>\n";
                                $x .= "</tr>\n";
                        }
                        $x .= "</table>\n";
                }
                return $x;
        }

        function displayGroupIcon(){
                global $file_path, $image_path;
                $limit = 50;
                $icon = "/file/group/g".$this->GroupID."_".$this->DateModifiedTimeStamp.".gif";
                $default = "/images/admin/icons/default.gif";
                $x = file_exists($file_path.$icon) ? $icon : $default;
                $image_info = file_exists($file_path.$icon) ? getimagesize($file_path.$icon) : getimagesize($file_path.$default);
                $width = $image_info[0];
                $height = $image_info[1];
                $width = ($width <= $limit) ? $width: $limit;
                $height = ($height <= $limit) ? $height: $limit;
                $x = "<img src=$x width=$width height=$height border=0 hspace=5 vspace=5 align=absmiddle>";
                return $x;
        }

        function returnImageIconForURL ($target)
        {
                 global $i_image_home;
                 if ($target != "")
                     $x = "<A HREF=\"$target\" target=_blank> $i_image_home </a>";
                 else $x ="";
                 return $x;
        }

        function displayStorage ()
        {
                 global $i_Campusquota_max, $i_Campusquota_used,$i_Campusquota_left,$i_Campusquota_using1,$i_Campusquota_using2;
                 $used = $this->returnGroupUsedStorageQuota();
                 $total = $this->StorageQuota * 1000;
                 if ($used == "") $used = 0;
                 if ($total == 0)
                 {
                     $pused = 100;
                     $left = 0;
                 }
                 else
                 {
                     $pused = 100*($used/$total);
                     $left = $total - $used;
                 }

                 $title = "$i_Campusquota_max: $total Kb(s)\n$i_Campusquota_used: $used Kb(s)\n$i_Campusquota_left: $left Kb(s)";

               $total .= " Kb(s) ";
               $left .= " Kb(s) ";
               $used .= " Kb(s), $pused% ";
               $storage = "<TABLE width=100% align=left><tr><td style=\"font-size:10px;\">0%</td>";
               $storage .= "<td width=100% class=td_left_middle>
<table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td  title='$title' width=100% class=td_left_middle bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table>
</td>";
               $storage .= "<td style=\"font-size:10px;\">100%</td></tr>";
               $storage .= "</table>";
               $x .= "<table width=80% border=1 cellspacing=0 cellpadding=5 bordercolor=#555555 bgcolor=ACEFD5 class=decription>\n";
               $x .= "<tr><td><table width=100% border=0 cellspacing=1 cellpadding=0>";
               $x .= "<tr><td class=td_description align=right width=15%>$i_Campusquota_max </td><td class=td_description>:</td><td width=85% class=td_description> $total</td></tr>
                      <tr><td class=td_description align=right>$i_Campusquota_left</td><td class=td_description>:</td><td class=td_description> $left</td></tr>
                      <tr><td colspan=3 class=td_description>$storage </td></tr>
                      <tr><td class=td_description align=right>$i_Campusquota_used</td><td class=td_description>:</td><td class=td_description> $used</td></tr>
                      <tr><td colspan=3 class=td_description>$i_Campusquota_using1 $pused% $i_Campusquota_using2 </td></tr>
                         ";
               $x .= "</table></td></tr></table>\n";
               return $x;
        }
        function displayStorage2 ()
        {
                 global $i_Campusquota_max, $i_Campusquota_used,$i_Campusquota_left,$i_Campusquota_using1,$i_Campusquota_using2;
                 $used = $this->returnGroupUsedStorageQuota();
                 $total = $this->StorageQuota * 1000;
                 if ($used == "") $used = 0;
                 if ($total == 0)
                 {
                     $pused = 100;
                     $left = 0;
                 }
                 else
                 {
                     $pused = 100*($used/$total);
                     $left = $total - $used;
                 }

                 $title = "$i_Campusquota_max: $total Kb(s)\n$i_Campusquota_used: $used Kb(s)\n$i_Campusquota_left: $left Kb(s)";

               $total .= " Kb(s) ";
               $left .= " Kb(s) ";
               $used .= " Kb(s), $pused% ";

               $x .= "<table width=80% border=0 align=right cellpadding=2 cellspacing=0 bgcolor=#ACE5EF class=\"11-black\" bordercolorlight=\"#555555\" bordercolordark=\"#ACE5EF\">";
               $x .= "<tr class=11-black>
                <td class=11-black>$i_Campusquota_max : <span class=\"11-grey\">$total</span><br>
                  $i_Campusquota_left : <span class=\"11-grey\">$left</span></td>
                <td class=td_center_middle>
                <table width=100% cellpadding=0 cellspacing=0 border=0 align=center>
                <tr>
                <td class=11-black>0%</td><td width=130><table cellpadding=0 border=0 cellspacing=0 style=\"border: #104a7b
1px solid; padding:1px; padding-right: 0px; padding-left: 0px;\" width=100%>
<tr>
<td  title='$title' width=100%  bgcolor=white>
<div style=\"height:6px; width:$pused%; font-size:3px;
background-color:#CECFFF\"></div>
</td>
</tr>
</table></td><td class=11-black> 100%</td>
                </tr>
                </table>
                </td>
                <td align=right class=11-black>$i_Campusquota_used : <span class=\"11-grey\">$used</span><br>
                  $i_Campusquota_using1 $pused% $i_Campusquota_using2</td>
              </tr>
            </table>";

               return $x;
        }
        
        function displayStorage3()
        {
			global $image_path, $LAYOUT_SKIN, $i_Campusquota_used, $i_Campusquota_used, $eComm;
			
	        $used = $this->returnGroupUsedStorageQuota();
 			$total = $this->StorageQuota * 1000;
 			if ($used == "") $used = 0;
             if ($total == 0)
             {
                 $pused = 100;
                 $left = 0;
             }
             else
             {
                 $pused = 100*($used/$total);
                 $left = $total - $used;
             }
                 
			$x = "
				<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
					<tr>
					
					
					<td valign=\"bottom\">
						<div id=\"show_storage\" style=\"position:absolute; width:160px; height:50px; z-index:1; visibility: hidden;\" class=\"storage_border\">
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
						<tr>
						<td>
						<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
						<tr>
						<td align=\"left\" class=\"tabletext\">". $i_Campusquota_used ." : ". $used ." / ". $total ." Kb(s)&nbsp;</td>
						</tr>
						<tr>
						<td align=\"left\" class=\"tabletext\"><table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"imailusage\">
						<tr>
						<td valign=\"middle\"><div id=\"Layer1\" style=\"position:absolute;width:150px;height:14px;z-index:2;\">
						<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
						<tr>
						<td align=\"center\" class=\"imailusagetext\">". $pused ."% </td>
						</tr>
						</table>
						</div>
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/usage_bar.gif\" width=\"". ($pused*150/100) ."\" height=\"14\"></td>
						</tr>
						</table></td>
						</tr>
						</table></td>
						</tr>
						</table>
						</div> </td>
						<td nowrap><a href=\"#\" class=\"tabletool\" onMouseOver=\"MM_showHideLayers('show_storage','','show')\" onMouseOut=\"MM_showHideLayers('show_storage','','hidden')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_storage.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\"> ". $eComm['Storage'] ." : ". $pused ."% ". $i_Campusquota_used ." </a>&nbsp;</td>
					
					</tr>
					</table>
	        ";
	        
	        return $x;
        }
        
        
        # /home/school/index
        # Shown besides icons
        function returnNumberOfNewBulletin()
        {
                 global $UserID;
                 $sql = "SELECT COUNT(BulletinID) FROM INTRANET_BULLETIN WHERE locate(';$UserID;',ReadFlag)=0 AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnNumberOfNewLinks()
        {
                 global $UserID;
                 $sql = "SELECT COUNT(LinkID) FROM INTRANET_LINK WHERE locate(';$UserID;',ReadFlag)=0 AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }

        function returnNumberOfNewFiles()
        {
                 global $UserID;
                 $sql = "SELECT COUNT(FileID) FROM INTRANET_FILE WHERE locate(';$UserID;',ReadFlag)=0 AND GroupID = '".$this->GroupID."'";
                 $result = $this->returnVector($sql);
                 return $result[0];
        }
        # ------------------------------------------------------------------------------------

        # ----------------------------------------------------------------------------------
        # Admin/user/new.php
        function getSelectClass($js_event="")
        {
                 global $button_select;
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE RecordType = 3 AND RIGHT(Title,1) <> '*' AND LOCATE('-',Title) = 0 ORDER BY Title";
                 $result = $this->returnVector($sql);
                 $x = "<SELECT $js_event>\n";
                 $x .= "<OPTION> -- $button_select -- </OPTION>\n";
                 for ($i=0; $i < sizeof($result); $i++)
                 {
                      $name = $result[$i];
                      $x .= "<OPTION value='$name'>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;

        }

        function getSelectLevel($js_event="")
        {
                 global $button_select;
                 $sql = "SELECT Title FROM INTRANET_GROUP WHERE RecordType = 3 AND RIGHT(Title,1) = '*' ORDER BY Title";
                 $result = $this->returnVector($sql);

                 $x = "<SELECT $js_event>\n";
                 $x .= "<OPTION> -- $button_select -- </OPTION>\n";
                 for ($i=0; $i < sizeof($result); $i++)
                 {
                      $name = substr($result[$i],0,-1);
                      $x .= "<OPTION value='$name'>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;

        }

        # ----------------------------------------------------------------
        # Group tools settings
        # Field : FunctionAccess
        #         - Bit-1 (LSB) : timetable
        #         - Bit-2       : chat
        #         - Bit-3       : bulletin
        #         - Bit-4       : shared links
        #         - Bit-5       : shared files
        #         - Bit-6       : question bank
        #         - Bit-7       : Photo Album
        #         - Bit-8 (MSB) : Survey
        # For adding new tool, add to the array result of returnGroupAvailableFunctions() in lib.php
        # Add corresponding isAccess function
        # ------------------------------------------------------------------


        function getSelectAvailableFunctions ()
        {
                 $functions = returnGroupAvailableFunctions();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 for ($i=0; $i<sizeof($functions); $i++)
                 {
                      if ($functions[$i]!="")
                          $x .= "<tr class=\"textboxtext\"><td><input type=checkbox name=grouptools[] value=$i CHECKED DISABLED id=\"check{$i}\"></td><td><label for=\"check{$i}\">".$functions[$i]."</label></td></tr>\n";
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function isAccessAllTools()
        {
                 if ($this->GroupID != "" && $this->FunctionAccess == "ALL")
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
        }

        function isAccessTool($type)
        {
                 $available = returnGroupAvailableFunctions();
                 if ($this->isAccessAllTools()) return ($available[$type]!="");
                 $s = substr($this->FunctionAccess,$type,1);
                 return ($s == "1");
        }

        function isAccessTimeTable()
        {
                 return $this->isAccessTool(0);
        }
        function isAccessChatroom()
        {
                 return $this->isAccessTool(1);
        }
        function isAccessBulletin()
        {
                 return $this->isAccessTool(2);
        }
        function isAccessLinks()
        {
                 return $this->isAccessTool(3);
        }
        function isAccessFiles()
        {
                 return $this->isAccessTool(4);
        }
        function isAccessQB()
        {
                 if ($this->RecordType == 2)
                 {
                     return $this->isAccessTool(5);
                 }
                 return false;
        }
        function isAccessPhotoAlbum()
        {
                 return $this->isAccessTool(6);
        }
        function isAccessSurvey()
        {
                 return $this->isAccessTool(7);
        }
        function getSelectCurrentAvailableFunctions ()
        {
                 $option_status = ($this->isAccessAllTools()? "DISABLED": "");
                 $functions = returnGroupAvailableFunctions();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 for ($i=0; $i<sizeof($functions); $i++)
                 {
                      if ($functions[$i]!="")
                      {
                          $checked = ($this->isAccessTool($i)? "CHECKED":"");
                          $x .= "<tr class=\"tabletext\"><td><input type=checkbox name=grouptools[] value=$i $checked $option_status id=\"check{$i}\"></td><td><label for=\"check{$i}\">".$functions[$i]."</label></td></tr>\n";
                      }
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        # ----------------------------------------------------------------------------------

        # --------------------------------------------------------------------------------------
        # Group Admin access right settings
        #
        function returnAvailableGroupAdminFunction()
        {
                 if ($this->GroupID == "") return array();
                 global $i_GroupSettingsBasicInfo, $i_GroupSettingsMemberList,
                        $i_GroupAdminRightInternalAnnouncement, $i_GroupAdminRightAllAnnouncement,
                        $i_GroupAdminRightInternalEvent, $i_GroupAdminRightAllEvent,
                        $i_GroupAdminRightTimetable, $i_GroupAdminRightBulletin,
                        $i_GroupAdminRightLinks, $i_GroupAdminRightFiles,
                        $i_GroupAdminRightQB, $i_GroupAdminRightInternalSurvey,
                        $i_GroupAdminRightAllSurvey, $i_frontpage_schoolinfo_groupinfo_group_photoalbum;

                 $rights = array ($i_GroupSettingsBasicInfo,$i_GroupSettingsMemberList,$i_GroupAdminRightInternalAnnouncement);
                 if ($this->AnnounceAllowed == 1)
                 {
                     $rights[] = $i_GroupAdminRightAllAnnouncement;
                 }
                 else
                 {
                     $rights[] = "";
                 }

                 $rights[] = $i_GroupAdminRightInternalEvent;
                 if ($this->AnnounceAllowed == 1)
                 {
                     $rights[] = $i_GroupAdminRightAllEvent;
                 }
                 else
                 {
                     $rights[] = "";
                 }

                 if ($this->isAccessTimetable())
                 {
                     $rights[] = $i_GroupAdminRightTimetable;
                 }
                 else
                 {
                     $rights[] = "";
                 }

                 if ($this->isAccessBulletin())
                 {
                     $rights[] = $i_GroupAdminRightBulletin;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 if ($this->isAccessLinks())
                 {
                     $rights[] = $i_GroupAdminRightLinks;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 if ($this->isAccessFiles())
                 {
                     $rights[] = $i_GroupAdminRightFiles;
                 }
                 else
                 {
                     $rights[] = "";
                 }

                 if ($this->isAccessQB())
                 {
                     $rights[] = $i_GroupAdminRightQB;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 if ($this->isAccessSurvey())
                 {
                     $rights[] = $i_GroupAdminRightInternalSurvey;
                     if ($this->AnnounceAllowed == 1)
                     {
                         $rights[] = $i_GroupAdminRightAllSurvey;
                     }
                     else
                     {
                         $rights[] = "";
                     }
                 }
                 else
                 {
                     $rights[] = "";
                     $rights[] = "";
                 }
                 if ($this->isAccessPhotoAlbum())
                 {
                     $rights[] = $i_frontpage_schoolinfo_groupinfo_group_photoalbum;
                 }
                 else
                 {
                     $rights[] = "";
                 }
                 return $rights;
        }

        function getSelectAvailableAdminRights ()
        {
                 $rights = $this->returnAvailableGroupAdminFunction();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 for ($i=0; $i<sizeof($rights); $i++)
                 {
                      $name = $rights[$i];
                      if (trim($name)!= "")
                          $x .= "<tr><td><input type=checkbox name=grouptools[] value=$i CHECKED DISABLED></td><td>$name</td></tr>\n";
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function retrieveAdminRights($UserID)
        {
                 $sql = "SELECT RecordType, IF(AdminAccessRight IS NULL,'ALL',REVERSE(BIN(AdminAccessRight)))
                         FROM INTRANET_USERGROUP WHERE GroupID = ".$this->GroupID." AND UserID = $UserID";
                         
                 $result = $this->returnArray($sql,2);
                 
                 $this->CurrentUserID = $UserID;
                 list ($admin, $rights) = $result[0];
                 if ($admin == "A")
                 {
                     $this->isAdmin = true;
                     if ($rights == 0)
                     {
                         $this->AdminAccessRight = "ALL";
                     }
                     else
                     {
                         $this->AdminAccessRight = $rights;
                     }
                     
                 }
                 else
                 {
                     $this->isAdmin = false;
                     $this->AdminAccessRight = "NULL";
                 }
        }

        function isGroupAdmin($UserID)
        {
                 if ($this->GroupID == "") return false;
                 if ($this->CurrentUserID != $UserID)
                 {
                     $this->retrieveAdminRights($UserID);
                 }
                 return $this->isAdmin;
        }
        function hasAllAdminRights($UserID)
        {
                 if ($this->GroupID == "") return false;
                 if ($this->CurrentUserID != $UserID)
                 {
                     $this->retrieveAdminRights($UserID);
                 }
                 if ($this->AdminAccessRight == "ALL")
                 {
                     return true;
                 }
                 else
                 {
                     return false;
                 }
        }
        function hasAdminRight($UserID, $type)
        {
                 if (!$this->isGroupAdmin($UserID)) return false;
                 if ($this->hasAllAdminRights($UserID)) return true;
                 $s = substr($this->AdminAccessRight,$type,1);
                 return ($s == "1");
        }
        function hasAdminBasicInfo ($UserID)
        {
                 return $this->hasAdminRight($UserID, 0);
        }
        function hasAdminMemberList ($UserID)
        {
                 return $this->hasAdminRight($UserID, 1);
        }
        function hasAdminInternalAnnounce ($UserID)
        {
                 return $this->hasAdminRight($UserID, 2);
        }
        function hasAdminAllAnnounce ($UserID)
        {
                 return ($this->AnnounceAllowed==1 && $this->hasAdminRight($UserID, 3) );
        }
        function hasAdminInternalEvent ($UserID)
        {
                 return $this->hasAdminRight($UserID, 4);
        }
        function hasAdminAllEvent ($UserID)
        {
                 return ($this->AnnounceAllowed && $this->hasAdminRight($UserID, 5));
        }
        function hasAdminTimetable ($UserID)
        {
                 return $this->isAccessTimeTable() && $this->hasAdminRight($UserID, 6);
        }
        function hasAdminBulletin ($UserID)
        {
                 return $this->isAccessBulletin() && $this->hasAdminRight($UserID, 7);
        }
        function hasAdminLinks ($UserID)
        {
                 return $this->isAccessLinks() && $this->hasAdminRight($UserID, 8);
        }
        function hasAdminFiles ($UserID)
        {
                 return $this->isAccessFiles() && $this->hasAdminRight($UserID, 9);
        }
        function hasAdminQB ($UserID)
        {
                 return $this->isAccessQB() && $this->hasAdminRight($UserID, 10);
        }
        function hasAdminInternalSurvey ($UserID)
        {
                 return $this->isAccessSurvey() && $this->hasAdminRight($UserID, 11);
        }
        function hasAdminAllSurvey ($UserID)
        {
                 return ($this->AnnounceAllowed && $this->isAccessSurvey() && $this->hasAdminRight($UserID, 12));
        }
        function hasAdminPhotoAlbum ($UserID)
        {	        
                 return $this->isAccessPhotoAlbum() && $this->hasAdminRight($UserID, 13);
        }

        function getSelectCurrentAdminRights ($uid)
        {
                 $rights = $this->returnAvailableGroupAdminFunction();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 if ($this->hasAllAdminRights($uid))
                 {
                     $ch_str = "CHECKED";
                     $dis_str = "DISABLED";
                 }
                 else
                 {
                     $dis_str = "";
                 }
                 for ($i=0; $i<sizeof($rights); $i++)
                 {
                      $name = $rights[$i];
                      if (trim($name)!= "")
                      {
                          if ($this->hasAdminRight($uid,$i))
                          {
                              $ch_str = "CHECKED";
                          }
                          else
                          {
                              $ch_str = "";
                          }
                          $x .= "<tr><td><input type=checkbox name=grouprights[] value=$i $ch_str $dis_str></td><td>$name</td></tr>\n";
                      }
                 }
                 $x .= "</table>\n";
                 return $x;
        }
        
        function getSelectCurrentAdminRights_ip20 ($uid)
        {
                 $rights = $this->returnAvailableGroupAdminFunction();
                 $x = "<table border=0 cellspacing=0 cellpadding=0>\n";
                 if ($this->hasAllAdminRights($uid))
                 {
                     $ch_str = "CHECKED";
                     $dis_str = "DISABLED";
                 }
                 else
                 {
                     $dis_str = "";
                 }
                 for ($i=0; $i<sizeof($rights); $i++)
                 {
                      $name = $rights[$i];
                      if (trim($name)!= "")
                      {
                          if ($this->hasAdminRight($uid,$i))
                          {
                              $ch_str = "CHECKED";
                          }
                          else
                          {
                              $ch_str = "";
                          }
                          $x .= "<tr class='tabletext'><td><input type=checkbox name=grouprights[] value=$i $ch_str $dis_str id=\"check{$i}\"></td><td><label for=\"check{$i}\">$name</label></td></tr>\n";
                      }
                 }
                 $x .= "</table>\n";
                 return $x;
        }

        function arrayToHTMLSelect($array,$js_event,$selected)
        {
                 $x = "<SELECT $js_event>\n";
                 for ($i=0; $i<sizeof($array); $i++)
                 {
                      list ($id, $name) = $array[$i];
                      $op_sel = ($selected==$id? "SELECTED":"");
                      $x .= "<OPTION value=$id $op_sel>$name</OPTION>\n";
                 }
                 $x .= "</SELECT>\n";
                 return $x;
        }

        # Get drop-down menu according to tools
        function getSelectGroups($js_event,$selected)
        {
                 global $UserID;
                 $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.UserID = $UserID
                         ORDER BY a.RecordType+0, a.Title";
                 $result = $this->returnArray($sql,2);
                 return getSelectByArray($result,$js_event,$selected,0,1);
        }
        function getFirstSelectGroupsID()
        {
                 global $UserID;
                 $sql = "SELECT a.GroupID FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.UserID = $UserID
                         ORDER BY a.RecordType+0, a.Title";
                 $result = $this->returnArray($sql,1);
                 
                 return $result[0][0];
        }
        
        function getSelectOtherGroups($js_event,$selected)
        {
	        global $UserID, $PATH_WRT_ROOT;
	        
	        include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/libuser{$LAYOUT_SKIN}.php");
			
			$lu 		= new libuser($UserID);
			$lu2007 	= new libuser2007($UserID);

	        $mygroup_str = $lu2007->OtherGroupStr();

             $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a
                     WHERE 
                     '$mygroup_str' not like concat('%:',a.GroupID,':%') 
                     ORDER BY a.RecordType+0, a.Title";
             $result = $this->returnArray($sql,2);
             return getSelectByArray($result,$js_event,$selected,0,1);

        }
       
        
        function getSelectGroupsByTool ($tool, $js_event,$selected)
        {
                 global $UserID;
                 $sql = "SELECT a.GroupID, a.Title FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b WHERE a.GroupID = b.GroupID AND b.UserID = '$UserID' AND (a.FunctionAccess IS NULL OR SUBSTRING(REVERSE(BIN(a.FunctionAccess)),$tool,1)=1)";
                 $groups = $this->returnArray($sql,2);
                 return $this->arrayToHTMLSelect($groups,$js_event,$selected);
        }
        
        

        function getSelectTimetableGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(1,$js_event,$selected);
        }
        function getSelectChatGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(2,$js_event,$selected);
        }
        function getSelectBulletinGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(3,$js_event,$selected);
        }
        function getSelectLinksGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(4,$js_event,$selected);
        }
        function getSelectFilesGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(5,$js_event,$selected);
        }
        function getSelectOthersGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(5,$js_event,$selected);
        }
        function getSelectQBGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(6,$js_event,$selected);
        }
        function getSelectPhotoGroups($js_event,$selected="")
        {
                 return $this->getSelectGroupsByTool(7,$js_event,$selected);
        }

        function returnAdminToolMapping()            # Map admin right to corresponding tool bit position
        {
                 $adminToolMapping = array();
                 /*
                 $adminToolMapping[6] = 1;
                 $adminToolMapping[7] = 3;
                 $adminToolMapping[8] = 4;
                 $adminToolMapping[9] = 5;
                 $adminToolMapping[10] = 6;
                 $adminToolMapping[12] = 8;
                 $adminToolMapping[13] = 8;
                 */
                 $adminToolMapping[7] = 1;
                 $adminToolMapping[8] = 3;
                 $adminToolMapping[9] = 4;
                 $adminToolMapping[10] = 5;
                 $adminToolMapping[11] = 6;
                 $adminToolMapping[12] = 8;
                 $adminToolMapping[13] = 8;
                 $adminToolMapping[14] = 7;
                 return $adminToolMapping;

        }
        function getSelectGroupsByAdminRight($right,$js_event,$selected)
        {
                 global $UserID;
                 $mapping = $this->returnAdminToolMapping();
                 if ($mapping[$right]!="")
                 {
                     $conds = " AND (a.FunctionAccess IS NULL OR SUBSTRING(REVERSE(BIN(a.FunctionAccess)),".$mapping[$right].",1) = 1)";
                 }
                 $sql = "SELECT a.GroupID, a.Title
                         FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.RecordType='A' AND
                         b.UserID = $UserID AND
                         (b.AdminAccessRight IS NULL OR SUBSTRING(REVERSE(BIN(b.AdminAccessRight)),$right,1) )
                         $conds";
                 $groups = $this->returnArray($sql,2);
                 return $this->arrayToHTMLSelect($groups,$js_event,$selected);
        }
        function getSelectGroupsByAdminRightsArray($right_array,$js_event,$selected)
        {
                 global $UserID;
                 $conds = "";
                 $aconds = "";
                 $mapping = $this->returnAdminToolMapping();
                 for ($i=0; $i<sizeof($right_array); $i++)
                 {
                      $conds .= " OR SUBSTRING(REVERSE(BIN(b.AdminAccessRight)),".$right_array[$i].",1)=1";
                      $mapTool = $mapping[$right_array[$i]];
                      if ($mapTool != "")
                          $aconds = " OR SUBSTRING(REVERSE(BIN(a.FunctionAccess)),$mapTool,1) = 1";

                 }
                 if ($aconds != "")
                 {
                     $aconds = " AND (a.FunctionAccess IS NULL $aconds )";
                 }
                 $sql = "SELECT DISTINCT a.GroupID, a.Title
                         FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
                         WHERE a.GroupID = b.GroupID AND b.RecordType='A' AND
                         b.UserID = $UserID AND
                         (b.AdminAccessRight IS NULL $conds ) $aconds";
                 $groups = $this->returnArray($sql,2);
                 return $this->arrayToHTMLSelect($groups,$js_event,$selected);
        }
        function getSelectAdminBasicInfo($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRight(1,$js_event,$selected);
        }
        function getSelectAdminMember($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRight(2,$js_event,$selected);
        }
        function getSelectAdminQB($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRight(11,$js_event,$selected);
        }
        function getSelectAdminAnnounce($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(3,4),$js_event,$selected);
        }
        function getSelectAdminEvent($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(5,6),$js_event,$selected);
        }
        function getSelectAdminBulletin($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(8),$js_event,$selected);
        }
        function getSelectAdminShared($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(9,10),$js_event,$selected);
        }
        function getSelectAdminSurvey($js_event,$selected="")
        {
                 return $this->getSelectGroupsByAdminRightsArray(array(12,13),$js_event,$selected);
        }
        function getSelectAdminPhotoAlbum($js_event, $selected="")
        {
                 return $this->getSelectGroupsByAdminRight(14,$js_event,$selected);
        }
        function returnGroupEvents($filter)
        {
                 switch ($filter)
                 {
                         case 1: $conds = ""; break;
                         case 2: $conds = " AND b.EventDate >= CURDATE()"; break;
                         case 3: $conds = " AND b.EventDate < CURDATE()"; break;
                         default: $conds = " AND b.EventDate >= CURDATE()"; break;
                 }
                 $namefield = getNameFieldWithClassNumberByLang("d.");
                 $sql = "SELECT DISTINCT b.EventID, b.Title,DATE_FORMAT(b.EventDate, '%Y-%m-%d'), c.Title, $namefield,IF((LOCATE(';$UserID;',b.ReadFlag)=0 || b.ReadFlag Is Null), 1, 0)
                         FROM INTRANET_GROUPEVENT as a LEFT OUTER JOIN INTRANET_EVENT as b ON a.EventID = b.EventID
                              LEFT OUTER JOIN INTRANET_GROUP as c ON c.GroupID = b.OwnerGroupID
                              LEFT OUTER JOIN INTRANET_USER as d ON b.UserID = d.UserID
                         WHERE a.GroupID = ".$this->GroupID." $conds ORDER BY b.EventDate, b.Title";
                 return $this->returnArray($sql,6);
        }
        function displayGroupEventInGroup($filter)
        {
                 global $i_general_sysadmin,$i_EventTitle,$i_EventPoster,$i_EventDate;
                 $events = $this->returnGroupEvents($filter);
                $x .= "<table width=495 border=1 cellpadding=2 cellspacing=0 bordercolorlight=#FCEA88 bordercolordark=#FFFFFF bgcolor=#FCEA88>
                    <tr>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_EventDate</strong></td>
                      <td align=left bgcolor=#FFC800 class=body><strong>$i_EventTitle</strong></td>
                      <td width=120 align=left bgcolor=#FFC800 class=body><strong>$i_EventPoster</strong></td>
                    </tr>";
                if(sizeof($events)==0){
                        global $i_no_record_exists_msg;
                        $x .= "<tr><td align=center colspan=3 class=body>$i_no_record_exists_msg</td></tr>\n";
                }else{
                        for($i=0; $i<sizeof($events); $i++)
                        {
                            list ($EventID,$EventTitle,$EventDate,$ownerGroup,$poster,$IsNew) = $events[$i];
                            if ($ownerGroup == "")
                            {
                                $displayName = "$i_general_sysadmin";
                            }
                            else
                            {
                                $displayName = "$poster <br> $ownerGroup";
                            }
                            $newFlag = (($IsNew) ? "<img src=/images/new.gif hspace=2 align=absmiddle border=0>" : "&nbsp;");
                            $x .= "<tr>\n";
                            $x .= "<td valign=top class=body>
                                    <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                      <tr>
                                        <td width=30>$newFlag</td>
                                        <td>$EventDate</td>
                                      </tr>
                                    </table>
                                   </td>
                                   <td align=left valign=top class=bodylink><a class=bodylink href=javascript:fe_view_event($EventID)>$EventTitle</a></td>
                                   <td valign=top class=bodylink>
                                   <table width=118 border=0 cellpadding=0 cellspacing=0 class=body>
                                     <tr>
                                       <td>$displayName</td>
                                     </tr>
                                   </table>
                                  </td>\n";
                            $x .= "</tr>\n";
                        }
                }
                $x .= "</table>\n";
                return $x;
        }

        # INTRANET_COURSE
        function getCourseGroupSubjectID($CourseID)
        {
                 $sql = "SELECT ClassGroupID,SubjectID FROM INTRANET_COURSE WHERE CourseID = '$CourseID'";
                 $result = $this->returnArray($sql,2);
                 $record = $result[0];
                 return (sizeof($record)==0? false: $record);
        }
        function getSubjectLeaderSelection ($name1, $name2, $targetUserID)
        {
                 $ClassID = $this->GroupID;
                 # Check it is normal class or special class
                 $sql = "SELECT ClassID FROM INTRANET_CLASS WHERE GroupID = '$ClassID'";
                 $result = $this->returnVector($sql);
                 if ($result[0]!="")           # Class
                 {
                     $ClassTableID = $result[0];
                     $sql = "SELECT DISTINCT a.SubjectID, a.SubjectName, IF(b.LeaderID IS NULL,'0','1')
                             FROM INTRANET_SUBJECT_TEACHER as c
                                  LEFT OUTER JOIN INTRANET_SUBJECT as a ON c.SubjectID = a.SubjectID AND a.RecordStatus = 1
                                  LEFT OUTER JOIN INTRANET_SUBJECT_LEADER as b ON a.SubjectID = b.SubjectID AND b.ClassID = '$ClassID' AND b.UserID = '$targetUserID'
                             WHERE c.ClassID = '$ClassTableID'
                             ORDER BY a.SubjectName";
                 }
                 else                          # Special
                 {
                     $sql = "SELECT a.SubjectID, a.SubjectName, IF(b.LeaderID IS NULL,'0','1')
                             FROM INTRANET_SUBJECT as a
                                  LEFT OUTER JOIN INTRANET_SUBJECT_LEADER as b ON a.SubjectID = b.SubjectID AND b.ClassID = '$ClassID' AND b.UserID = '$targetUserID'
                             WHERE a.RecordStatus = 1
                             ORDER BY a.SubjectName";
                 }
                 #echo "1. $sql\n";
                 $result = $this->returnArray($sql,3);
                 $x = "<table width=100% border=0 cellpadding=5 cellspacing=0>\n";
                 $x .= "<tr><td class=tableContent width=50%>\n";
                 $x .= "<select name=$name1 size=10 multiple>\n";
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list($subjectID, $name, $leader) = $result[$i];
                      if ($leader == 1)
                      {
                          $x .= "<option value=$subjectID>$name</OPTION>\n";
                      }
                 }
                 $x .= "<option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>\n";
                 $x .= "</SELECT>\n";
                 $x .= "</td>\n";
                 $x .= "<td class=tableContent align=center>
<p><input type='image' src='/images/admin/button/s_btn_addto_b5.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"$name2\"],this.form.elements[\"$name1\"]);return false;'></p>
<p><input type='image' src='/images/admin/button/s_btn_deleteto_b5.gif' border='0' onClick='checkOptionTransfer(this.form.elements[\"$name1\"],this.form.elements[\"$name2\"]);return false;'></p>
                 </td>
<td class=tableContent width=50%>";
                 $x .= "<select name=$name2 size=10 multiple>\n";
                 for ($i=0; $i<sizeof($result); $i++)
                 {
                      list($subjectID, $name, $leader) = $result[$i];
                      if ($leader != 1)
                      {
                          $x .= "<option value=$subjectID>$name</OPTION>\n";
                      }
                 }
                 $x .= "<option>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>\n";
                 $x .= "</select>\n";
                 $x .= "</td></tr></table>\n";
                 return $x;
        }
        
        function getGroupsID()
        {
			global $UserID;
			$sql = "SELECT a.GroupID FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
				WHERE a.GroupID = b.GroupID AND b.UserID = $UserID";
			return $this->returnArray($sql,1);
			
			
        }
        
        function getTargetGroupsID($TarUserID)
        {
			global $UserID;
			$sql = "SELECT a.GroupID FROM INTRANET_GROUP as a, INTRANET_USERGROUP as b
				WHERE a.GroupID = b.GroupID AND b.UserID = $TarUserID";
				
			return $this->returnArray($sql,1);
			
			
        }
}

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

/*
switch ($filter){
        case 0: $filter = 0; break;
        case 1: $filter = 1; break;
        case 2: $filter = 2; break;
        case 3: $filter = 3; break;
        case 4: $filter = 4; break;
        case 5: $filter = 5; break;
        case 6: $filter = 6; break;
        default: $filter = 0; break;
}
<input type=hidden name=filter value="">
*/
$li = new libgroup($GroupID);
$GroupUsers = $li->getNumberGroupUsers();
$toolbar = "";
if ($li->RecordType != -1) {                
    $toolbar .= "<a class=iconLink href=javascript:newWindow('add.php?GroupID=".$li->GroupID."',2)>".importIcon()."$button_import</a>\n".toolBarSpacer();
}
$toolbar .= ($GroupUsers == 0) ? emailIcon()."$button_email" : "<a class=iconLink href=javascript:checkGet(document.form1,'email.php')>".emailIcon()."$button_email</a>\n";
$toolbar .= toolBarSpacer();
#$toolbar .= ($GroupUsers == 0) ? groupIcon()."$i_frontpage_schoolinfo_groupinfo_group_members" : "<a class=iconLink href=javascript:checkGet(document.form1,'export.php')>".groupIcon()."$i_frontpage_schoolinfo_groupinfo_group_members</a>\n";
$toolbar .= ($GroupUsers == 0) ? exportIcon()."$button_export" : "<a class=iconLink href=javascript:checkGet(document.form1,'export.php');document.form1.action='index.php'>".exportIcon()."$button_export</a>\n";
$row = $li->returnRoleType();
$functionbar = "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$functionbar .= "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$functionbar .= "</select>\n";
$functionbar .= "<a href=\"javascript:checkRole(document.form1,'UserID[]','role.php')\"><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= ($li->RecordType == -1) ? "&nbsp;" : "<a href=\"javascript:checkRemove(document.form1,'UserID[]','delete.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$groupadminfunctionbar = "";

$subleader = get_file_content("$intranet_root/file/homework_leader.txt");
if ($subleader == 1 && $li->RecordType == 3) {                # Only Class has
    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UserID[]','performance.php')\">";
    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UserID[]','subjectleader.php')\"><img src='/images/admin/button/t_btn_assignsubjectleader_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_assignsubjectleader'></a>&nbsp;";
}
$groupadminfunctionbar .= "<input type=hidden name=adminflag value=0>";
$groupadminfunctionbar .= "<a href=\"javascript:document.form1.adminflag.value='A'; checkEdit(document.form1, 'UserID[]','assignadmin.php')\"><img src='/images/admin/button/t_btn_set_admin_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$groupadminfunctionbar .= "<a href=\"javascript:document.form1.adminflag.value='0'; checkRole(document.form1, 'UserID[]','setadmin.php')\"><img src='/images/admin/button/t_btn_cancel_admin_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
if ($li->RecordType == 5) {                # Only ECA has
    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UserID[]','performance.php')\">";
    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UserID[]','performance.php')\"><img src='/images/admin/button/t_btn_update_performance_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_updateperformance'></a>&nbsp;";
}

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$li->order = $order;
$li->field = $field;
$li->keyword = $keyword;
?>

<form name=form1 method=get>
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, "../index.php?filter=$filter", $li->Title, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar($toolbar, $functionbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("", $groupadminfunctionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->displayGroupUsers(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td><font color=red>*</font> - <?= $i_GroupKey ?> </td></tr>
<?
if ($li->RecordType == 3) {                # Only Class has
?>
<tr><td><font color=green>#</font> - <?= $i_Homework_SubjectLeaderKey ?> <p><br></p></td></tr>
<? } ?>
</table>
<input type=hidden name=GroupID value=<?php echo $li->GroupID; ?>>
<input type=hidden name=field value="<?=$field?>">
<input type=hidden name=order value="<?=$order?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include("../../../templates/adminfooter.php");
?>