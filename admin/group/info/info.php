<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader_admin.php");
intranet_opendb();

$li = new libgroup($GroupID);
$GroupUsers = $li->getNumberGroupUsers();
$GroupAnnouncement = $li->getNumberGroupAnnouncement();
$GroupEvent = $li->getNumberGroupEvent();
$GroupTimetable = $li->getNumberGroupTimetable();
$GroupResource = $li->getNumberGroupResource();
?>
<?= displayNavTitle($li->Title, '') ?>

<form name=form1>
<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" align=center>

<table width=400 border=0 cellpadding=10 cellspacing=0>
<tr><td colspan=2 align=right><?php echo $i_admintitle_user; ?>: <?php echo $GroupUsers; ?></td></tr>
</table>
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_bar.gif" width=422 height=16 border=0></td></tr>
</table>
<table width=400 border=0 cellpadding=10 cellspacing=0>
<tr ><td># <?php echo $i_admintitle_announcement; ?>:</td><td align=right><?php echo $GroupAnnouncement; ?></td></tr>
<tr ><td># <?php echo $i_admintitle_event; ?>:</td><td align=right><?php echo $GroupEvent; ?></td></tr>
<tr ><td># <?php echo $i_admintitle_timetable; ?>:</td><td align=right><?php echo $GroupTimetable; ?></td></tr>
<tr ><td># <?php echo $i_admintitle_resource; ?>:</td><td align=right><?php echo $GroupResource; ?></td></tr>
</table>

</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>

</form>

<?php
intranet_closedb();
include_once("../../../templates/filefooter.php");
?>