<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
intranet_opendb();

$UserID = (is_array($UserID)? $UserID[0]:$UserID );
$li = new libgroup($GroupID);

$meminfo = $li->returnMemberInfo($UserID);
list($Role,$Performance) = $meminfo;

$lu = new libuser($UserID);
$semesterSelect = getSelectSemester("name=semester");
$lword = new libwordtemplates();
$performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");
?>

<script language="javascript">
function checkform(obj){
         return true;
}
</script>

<form name=form1 action=perform_update.php method=post onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, "../index.php?filter=$filter", $li->Title, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=right><?php echo $i_ActivityName; ?>:</td><td><?=$li->Title?></td></tr>
<tr><td align=right><?php echo $i_ActivityRole; ?>:</td><td><?=$Role?></td></tr>
<tr><td align=right><?php echo $i_ActivityPerformance; ?>:</td><td><input type=text size=30 name=performance value='<?=$Performance?>'><?=$performancelist?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=UserID value=<?=$UserID?>>
<input type=hidden name=GroupID value=<?=$GroupID?>>
<input type=hidden name=filter value=<?=$filter?>>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>