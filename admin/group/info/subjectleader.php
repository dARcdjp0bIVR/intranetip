<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libgroup.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
intranet_opendb();

if (is_array($UserID)) $UserID = $UserID[0];
$lu = new libuser($UserID);
$lg = new libgroup($GroupID);
$selection = $lg->getSubjectLeaderSelection("SubjectID[]","AvailableSubjectID[]",$UserID);
?>
<SCRIPT LANGUAGE=javascript>
function checkform(obj)
{
     checkOptionAll(obj.elements["SubjectID[]"]);
}
</SCRIPT>

<form name=form1 method=post action='subjectleader_update.php' onSubmit="return checkform(this)">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, "../index.php?filter=$filter", $lg->Title, 'javascript:history.back()', $button_assignsubjectleader, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=center colspan=2><?=$selection?></td></tr>
</table>
</blockquote>
<input type=hidden name=GroupID value="<?=$GroupID?>">
<input type=hidden name=UserID value="<?=$UserID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
intranet_closedb();
include("../../../templates/adminfooter.php");
?>