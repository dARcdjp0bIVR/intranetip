<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libclass.php");

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();

# uploaded file information
//$filepath = $HTTP_POST_FILES['userfile']['tmp_name'];
//$filename = $HTTP_POST_FILES['userfile']['name'];
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: import.php?filter=$RecordType");
} else {
		/*
        $ext = strtoupper($lo->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lo->file_read_csv($filepath);
                array_shift($data);                   # drop the title bar
        }
        */
        
        $data = $limport->GET_IMPORT_TXT($filepath);
		array_shift($data);		# drop the title bar

        $g_delimiter = "";
        $t_delimiter = "";
        $toTempValues = "";
        $toGroup_insert_values = "";


        for ($i=0; $i<sizeof($data); $i++)
        {
             list($userlogin,$grouptitle) = $data[$i];
             $userlogin = addslashes($userlogin);
             $grouptitle = addslashes($grouptitle);
             $toGroup_insert_values .= "$g_delimiter ('$grouptitle','$grouptitle',$RecordType,now(),now())";
             $toTempValues .= "$t_delimiter ('$userlogin','$grouptitle')";
             $g_delimiter = ",";
             $t_delimiter = ",";
        }

        # Insert new groups
        $fields_name = "Title,Description,RecordType,DateInput,DateModified";
        $sql = "INSERT IGNORE INTO INTRANET_GROUP ($fields_name) VALUES $toGroup_insert_values";
        $li->db_db_query($sql);

        # Create temp table
        $sql = "CREATE TEMPORARY TABLE TEMP_GROUP_IMPORT (
                       Login varchar(255) NOT NULL,
                       GroupName varchar(255) NOT NULL
               )";
        $li->db_db_query($sql);
        $fields_name = "Login,GroupName";
        $sql = "INSERT IGNORE INTO TEMP_GROUP_IMPORT ($fields_name) VALUES $toTempValues";
        $li->db_db_query($sql);

        $fields_name = "UserID,GroupID";
        $sql = "INSERT IGNORE INTO INTRANET_USERGROUP ( $fields_name )
                  SELECT a.UserID , b.GroupID
                  FROM INTRANET_USER as a, INTRANET_GROUP as b, TEMP_GROUP_IMPORT as c
                  WHERE a.UserLogin = c.Login AND b.Title = c.GroupName ";

        $li->db_db_query($sql);
        $li->UpdateRole_UserGroup();

        # Fix Class relation
        $lc = new libclass();
        $lc->fixClassGroupRelation();

        intranet_closedb();
        header("Location: index.php?filter=$RecordType&msg=1");
}

?>