<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
intranet_opendb();

$li = new libdb();
$year = "-".date('Y');
$update_fields = "Title=CONCAT(Title,'$year'), Description=CONCAT(Description,'$year')";
$conds = "GroupID IN (".implode(",", $GroupID).")";
$sql = "UPDATE INTRANET_GROUP SET $update_fields WHERE $conds";
$li->db_db_query($sql);

$lc = new libclass();
$lc->fixClassGroupRelation();

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=9");
?>