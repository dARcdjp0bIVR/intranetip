<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
if ($filter != "") $type_conds = "AND a.RecordType = $filter";
// 2nd row                        IF((a.RecordStatus='2' OR a.RecordStatus='3'),CONCAT('<a class=tableContent href=javascript:icon(', a.GroupID, ')>','<img src=/file/group/g', a.GroupID, '_', UNIX_TIMESTAMP(a.DateModified), '.gif width=41 height=26 vspace=0 hspace=4 border=0></a>'),CONCAT('<a class=tableContent href=javascript:icon(', a.GroupID, ')>','<img src=../../images/admin/icons/default.gif width=41 height=26 vspace=0 hspace=4 border=0></a>')),
$sql  = "SELECT
                        CONCAT('<a class=tableContentLink href=edit.php?GroupID[]=', a.GroupID, '>', a.Title, '</a>'),
                        ifnull(a.Description, '&nbsp;'),
                        CONCAT('<a class=tableContentLink href=javascript:user(', a.GroupID, ')>', IF(IsNull(b.GroupID), 0, COUNT(a.GroupID)), '</a>'),
                        CONCAT('<a class=tableContentLink href=javascript:info(', a.GroupID, ')><img src=../../images/admin/more_info.gif border=0></a>'),
                        a.DateModified,
                        IF((a.RecordStatus='1' OR a.RecordStatus='3'),'<img src=../../images/red_checkbox.gif vspace=3 hspace=4 border=0>',CONCAT('<input type=checkbox name=GroupID[] value=', a.GroupID ,'>')),
                        IF(IsNull(b.GroupID), 0, COUNT(a.GroupID)) AS Count
                FROM
                        INTRANET_GROUP AS a
                LEFT OUTER JOIN
                        INTRANET_USERGROUP AS b ON a.GroupID = b.GroupID
                WHERE
                        (a.Title like '%$keyword%' OR a.Description like '%$keyword%')
                        $type_conds
                GROUP BY a.GroupID
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Title", "a.Description", "Count", "a.GroupID", "a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_group;
$li->column_array = array(5,5,0,0,0);
$li->wrap_array = array(20,20,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $i_GroupTitle)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $i_GroupDescription)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column_image(2, "#", "<img src=../../images/group_icon.gif border=0>")."</td>\n";
$li->column_list .= "<td width=5% class=tableTitle><br></td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(4, $i_GroupDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("GroupID[]")."</td>\n";

// TABLE FUNCTION BAR
$lgc = new libgroupcategory();
$selection = $lgc->returnSelectCategory("name=filter onChange=this.form.submit()",false,1,$filter);
$filterbar = "$i_GroupCategoryName: $selection";
if ($filter !== "0")
{
    $toolbar = "<a class=iconLink href=\"javascript:checkPost(document.form1,'new.php')\">".newIcon()."$button_new</a>";
}
$functionbar = "";
if ($filter==3)
    $functionbar  = "<a href=javascript:checkArchive(document.form1,'GroupID[],'archive.php')><img src=$image_path/admin/button/t_btn_save_$intranet_session_language.gif alt='$button_archive' border=0></a>";
    #$functionbar  = "<input class=button type=button value='$button_archive' onClick=checkArchive(this.form,'GroupID[]','archive.php')>";
if ($filter != 3 && $filter !== "0")
    $toolbar .= "\n".toolBarSpacer()."<a class=iconLink href=\"javascript:checkPost(document.form1,'import.php')\">".importIcon()."$button_import</a>\n";

$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'GroupID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'GroupID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<script language="JavaScript1.2">
function icon(id){
        url = "icon/index.php?GroupID=" + id;
        newWindow(url,2);
}
function info(id){
        url = "info/info.php?GroupID=" + id;
        newWindow(url,2);
}
function user(id){
        f = document.form1.filter.value;
        url = "info/index.php?filter=" + f + "&GroupID=" + id;
        self.location.href = url;
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_group, '') ?>
<?= displayTag("head_group_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>