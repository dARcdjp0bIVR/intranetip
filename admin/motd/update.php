<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();
$li->file_write(htmlspecialchars(trim(stripslashes($motd))), $intranet_root."/file/motd.txt");

header("Location: index.php?msg=2");
?>
