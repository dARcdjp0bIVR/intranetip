<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();


$llunchbox = new liblunchbox();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;


if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV" || $ext == ".LOG") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                $title_row = array_shift($data);                   # drop the title bar
                if ($format==1)
                {
                    $file_format = array("Year","Month","Day","UserLogin");
                }
                else if ($format==2)
                {
                    $file_format = array("Year","Month","Day","ClassName","ClassNumber");
                }
                else
                {
                    header("Location: import.php?failed=1");
                    exit();
                }

                for ($i=0; $i<sizeof($file_format); $i++)
                {
                     if ($title_row[$i]!=$file_format[$i])
                     {
                         header("Location: import.php?failed=1");
                         exit();
                     }
                }
        }
        else
        {
            header("Location: import.php?failed=1");
            exit();
        }

        # Data used
        unset($tablenames);
        unset($error_data);
        $curr_year = date('Y');
        $curr_month = date('m');
        $curr_day = date('d');

        if ($format==1)
        {
            # Login assoc array
            $sql = "SELECT UserLogin, UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2)";
            $temp = $llunchbox->returnArray($sql,2);
            $students = build_assoc_array($temp);

            for ($i=0; $i<sizeof($data); $i++)
            {
                 list($t_year, $t_month, $t_day, $t_login) = $data[$i];

                 # Check date
                 if (!checkdate($t_month, $t_day, $t_year))
                 {
                      $error_data[] = array($i, $i_invalid_date);
                      continue;
                 }
                 $targetPassed = false;
                 if ($curr_year > $t_year)
                 {
                     $targetPassed = true;
                 }
                 else if ($curr_year == $t_year && $curr_month > $t_month)
                 {
                      $targetPassed = true;
                 }
                 else if ($curr_year == $t_year && $curr_month == $t_month && $curr_day > $t_day)
                 {
                      $targetPassed = true;
                 }

                 if ($targetPassed)
                 {
                     $error_data[] = array($i, $i_SmartCard_Lunchbox_Warning_CannotEditPrevious);
                     continue;
                 }

                 # Check student
                 $t_student_id = $students[$t_login];
                 if ($t_student_id == "")
                 {
                     $error_data[] = array($i, "Invalid Login");
                     continue;
                 }

                 if (strlen($t_month)<2)
                 {
                     $t_month = "0".$t_month;
                 }

                 # Retrieve ticket table name
                 $t_tablename = $tablenames["$t_year$t_month"];
                 if ($t_tablename == "")
                 {
                     $t_tablename = $llunchbox->createMonthlyTicketTable($t_year, $t_month);
                     $tablenames["$t_year$t_month"] = $t_tablename;
                 }

                 $sql = "INSERT INTO $t_tablename (StudentID, DayNumber, RecordType, RecordStatus, DateInput, DateModified)
                                VALUES ($t_student_id, $t_day, 0,0,now(),now())";
                 $llunchbox->db_db_query($sql);
            }
        }
        else if ($format == 2)
        {
             $sql = "SELECT ClassName, ClassNumber, UserID FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND TRIM(ClassName) != '' AND TRIM(ClassNumber) != ''";
             $temp = $llunchbox->returnArray($sql,3);
             for ($i=0; $i<sizeof($temp); $i++)
             {
                  list($t_class, $t_classnum , $t_userid) = $temp[$i];
                  if ($t_class != "" && $t_classnum != "")
                  {
                      $students[$t_class][$t_classnum] = $t_userid;
                  }
             }
             for ($i=0; $i<sizeof($data); $i++)
             {
                 list($t_year, $t_month, $t_day, $t_class, $t_classnum) = $data[$i];

                 # Check date
                 if (!checkdate($t_month, $t_day, $t_year))
                 {
                      $error_data[] = array($i, $i_invalid_date);
                      continue;
                 }
                 $targetPassed = false;
                 if ($curr_year > $t_year)
                 {
                     $targetPassed = true;
                 }
                 else if ($curr_year == $t_year && $curr_month > $t_month)
                 {
                      $targetPassed = true;
                 }
                 else if ($curr_year == $t_year && $curr_month == $t_month && $curr_day > $t_day)
                 {
                      $targetPassed = true;
                 }

                 if ($targetPassed)
                 {
                     $error_data[] = array($i, $i_SmartCard_Lunchbox_Warning_CannotEditPrevious);
                     continue;
                 }

                 # Check student
                 $t_student_id = $students[$t_class][$t_classnum];
                 if ($t_student_id == "")
                 {
                     $error_data[] = array($i, "Invalid ClassName, ClassNumber");
                     continue;
                 }

                 if (strlen($t_month)<2)
                 {
                     $t_month = "0".$t_month;
                 }

                 # Retrieve ticket table name
                 $t_tablename = $tablenames["$t_year$t_month"];
                 if ($t_tablename == "")
                 {
                     $t_tablename = $llunchbox->createMonthlyTicketTable($t_year, $t_month);
                     $tablenames["$t_year$t_month"] = $t_tablename;
                 }

                 $sql = "INSERT INTO $t_tablename (StudentID, DayNumber, RecordType, RecordStatus, DateInput, DateModified)
                                VALUES ($t_student_id, $t_day, 0,0,now(),now())";
                 $llunchbox->db_db_query($sql);
            }
        }

}


if (sizeof($error_data)!=0)
{
    include_once("../../../../templates/adminheader_setting.php");
    $error_display = $i_general_ImportFailed." <br>\n";
    $error_display .= "<table width=95% border=1 cellpadding=1 cellspacing=0>\n";
    $error_display .= "<tr class=tableTitle><td>Row</td><td>Reason</td></tr>\n";
    for ($i=0; $i<sizeof($error_data); $i++)
    {
         list($t_row, $t_msg) = $error_data[$i];
         $t_row++;
         $css = ($i%2?"":"2");
         if ($t_msg == "") $t_msg = "Unknown";
         $error_display .= "<tr class=tableContent$css><td>$t_row</td><td>$t_msg</td></tr>\n";
    }
    $error_display .= "</table>\n";
    $error_display .= "<br><br>\n";
    $error_display .= "<a class=functionlink_new href=\"import.php?targetDate=$targetDate\">Back to Import Page</a>\n";
    echo $error_display;
    include_once("../../../../templates/adminfooter.php");
}
else
{
    header("Location: index.php?targetDate=$targetDate");
    exit();
}

intranet_closedb();

?>