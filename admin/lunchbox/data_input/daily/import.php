<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

if ($failed == 1)
{
    $xmsg = $i_con_msg_import_failed;
}
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_DataInput, '../', $i_SmartCard_Lunchbox_DataInput_Day, 'index.php', $button_import, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>
<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this)">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile></td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
<input type=radio name=format value=1 CHECKED><?=$i_SmartCard_Lunchbox_Import_Format_Login?> <a class=functionlink href=format1.csv target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>  <br>
<input type=radio name=format value=2><?=$i_SmartCard_Lunchbox_Import_Format_ClassNumber?> <a class=functionlink href=format2.csv target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>        <br>
</td></tr>
</table>

</blockquote>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<br><br>
<input type=hidden name=targetDate value="<?=$targetDate?>"> 
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
