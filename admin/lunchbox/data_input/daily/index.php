<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$curr_date = date('Y-m-d');

$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');

if (isset($targetDate) && $targetDate != "")
{
    $targetSelected = true;
    $selectedDate = $targetDate;
}
else
{
    $targetSelected = false;
    $selectedDate = $curr_date;

}



?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_DataInput, '../', $i_SmartCard_Lunchbox_DataInput_Day, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form_change'].targetDate.value = dateValue;
          }
 // -->
 </script>

<form name="form_change" method="get">
<table width=300 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=right><?=$i_SmartCard_Lunchbox_DateField_Date?>:</td><td>
<input type=text name=targetDate value='<?=$selectedDate?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
<br><input type=image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0>
</td></tr>
</table>
</form>

<? if ($targetSelected) {
$llunchbox = new liblunchbox();
$lc = new libclass();

?>
<hr width=85% align=center>
<div align=center><?=$targetDate?></div>
<form name=form1 method=GET action="">
<?

$targetTS = strtotime($targetDate);
$targetYear = date('Y',$targetTS);
$targetMonth = date('m',$targetTS);
$targetDay = date('d',$targetTS);


$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month == $targetMonth && $curr_day > $targetDay)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_CannotEditPrevious</font></div>";
    $checkbox_string = "disabled";

}
else
{
/*
    $array_set_days = $llunchbox->getCalendarDates($targetYear, $targetMonth);
    if ($array_set_days===false || !is_array($array_set_days))
    {
        $hasData = false;
    }
    else
    {
        $hasData = true;
    }
    if (!$hasData)
    {
         echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_MonthlyCalendarNotExist</font></div>";
    }
    */
}

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;

# SQL stmt

$namefield = getNamefieldByLang("u.");
$ticket_table_name = $llunchbox->createMonthlyTicketTable($targetYear, $targetMonth);

if ($targetClassID != "")
{
    $targetClass = $lc->getClassName($targetClassID);
    $conds = " AND u.ClassName = '$targetClass'";
}

$sql  = "SELECT
               $namefield, u.ClassName, u.ClassNumber,
               IF(t.RecordStatus=1,'$i_SmartCard_Lunchbox_Status_Taken','$i_SmartCard_Lunchbox_Status_NotTaken')
               ". ($targetPassed? "":"
               , CONCAT('<input type=checkbox name=StudentID[] value=', u.UserID,'>')")."
                FROM
                    $ticket_table_name as t
                    LEFT OUTER JOIN INTRANET_USER as u ON t.StudentID = u.UserID AND u.RecordType = 2
                WHERE
                     t.DayNumber = '$targetDay' AND
                        (u.EnglishName like '%$keyword%' OR
                         u.ChineseName like '%$keyword%' OR
                         u.UserLogin like '%$keyword%' OR
                         u.ClassName like '%$keyword%' OR
                         u.ClassNumber like '%$keyword%'
                         )
                        $conds
                ";



# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("u.EnglishName", "u.ClassName", "u.ClassNumber", "t.RecordStatus");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
if (!$targetPassed)
{
     $li->no_col++;
}
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_general_status)."</td>\n";
if (!$targetPassed)
{
     $li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";
}


#if ($hasData)
#{
    $toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'new.php')\">".newIcon()."$i_SmartCard_Lunchbox_Action_AddStudent</a>";
    $toolbar .= "&nbsp;<a class=iconLink href=\"javascript:checkGet(document.form1,'import.php')\">".newIcon()."$button_import</a>";
#}

$classes = $lc->getClassList();
$select_class = getSelectByArray($classes,"name=targetClassID onChange=this.form.submit()",$targetClassID,1);

if (!$targetPassed)
{
     $functionbar .= " <a href=\"javascript:checkRemove(document.form1,'StudentID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}
$searchbar = $select_class;
$searchbar .= " <input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= " <a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=<?=$image_path?>/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>


<input type=hidden name=targetDate value="<?=$targetDate?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<? } ?>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
