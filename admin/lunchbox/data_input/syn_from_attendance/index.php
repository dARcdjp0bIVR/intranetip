<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../lang/lang.{$intranet_session_language}.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');

echo displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_DataInput, '../', $i_SmartCard_Lunchbox_Sync_From_Attendance, '');
echo displayTag("head_lunchbox_$intranet_session_language.gif", "");

$llunchbox = new liblunchbox();

$ticket_table_name = $llunchbox->createMonthlyTicketTable($curr_year, $curr_month);

$sql = "select count(*) FROM $ticket_table_name WHERE DayNumber = $curr_day";
$row = $llunchbox->returnVector($sql);
$NoOfStudentInLunchBox = (int) $row[0];

$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$curr_year."_".$curr_month;


$sql = "select count(*) from {$card_log_table_name} where AMstatus in (0, 2) AND DayNumber = $curr_day ";
$row = $llunchbox->returnVector($sql);
$NoOfPresent = (int) $row[0];
?>



<form name="form_change" method="get" action="syn_update.php">
<table width="300" align=center border="0" cellspacing="3" cellpadding="2" align="cetner">
<tr><td><?=$i_SmartCard_Lunchbox_DateField_Date?>:</td><td nowrap="nowrap"><?= date('Y-m-d') ?></td></tr>
<tr><td><?=$i_SmartCard_Lunchbox_Total_LunchBox?>:</td><td><?= $NoOfStudentInLunchBox ?></td></tr>
<tr><td><?=$i_SmartCard_Lunchbox_Total_Present?>:</td><td><?= $NoOfPresent ?></td></tr>
<tr><td align="center" colspan="2" style="border-top: 1px solid black;"><br />
<?php
if ($msg==2)
{
?>
<a href="../"><img src="<?=$image_path?>/admin/button/s_btn_back_<?=$intranet_session_language?>.gif" border=0 /></a>

<?php
} else
{
?>
<p><?= $i_SmartCard_Lunchbox_Sync_From_Attendance_Confirm ?></p>
<input type=image src="<?=$image_path?>/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border=0 />
<a href="../"><img src="<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border=0 /></a>
<?php
}
?>
</td></tr>
</table>
</form>


<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
