<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/liblunchbox.php");
intranet_opendb();


$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');

$llunchbox = new liblunchbox();


$ticket_table_name = $llunchbox->createMonthlyTicketTable($curr_year, $curr_month);


$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$curr_year."_".$curr_month;
$sql = "select UserID from {$card_log_table_name} where AMstatus in (0, 2) AND DayNumber={$curr_day}";
$StudentID = $llunchbox->returnVector($sql);
$student_list = implode(",",$StudentID);


$DefaultRecordStatus = ($plugin['Lunchbox_HCLMS']) ? "1" : "0";


# Remove old records (Status not taken)
$sql = "DELETE FROM $ticket_table_name WHERE RecordStatus = {$DefaultRecordStatus} AND DayNumber = $curr_day";
$llunchbox->db_db_query($sql);

# Insert new records
$values = "";
$delim = "";

for ($i=0; $i<sizeof($StudentID); $i++)
{
	$t_id = $StudentID[$i];
	$values .= "$delim ($t_id, $curr_day, 0, {$DefaultRecordStatus}, now(),now())";
	$delim = ",";
}
$sql = "INSERT IGNORE INTO $ticket_table_name (StudentID, DayNumber, RecordType, RecordStatus, DateInput, DateModified)
       VALUES $values";
$llunchbox->db_db_query($sql);

$llunchbox->addCalendarDate($curr_year, $curr_month, $curr_day);

header("Location: index.php?msg=2");
intranet_closedb();

?>
