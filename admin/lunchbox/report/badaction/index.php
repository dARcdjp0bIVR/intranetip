<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
include_once("../../../../cardapi/lunchbox/const.php");
intranet_opendb();

if (!isset($StartDate) || $StartDate == "")
{
     $StartDate = date('Y-m-d');
}
if (!isset($EndDate) || $EndDate == "")
{
     $EndDate = date('Y-m-d');
}


# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if (!isset($field)) $field = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     default: $field = 1; break;
}
$order = ($order == 1) ? 1 : 0;
if($ActionType == "") $ActionType = BAD_ACTION_NO_TICKET;
switch ($ActionType)
{
        case BAD_ACTION_NO_TICKET: break;
        case BAD_ACTION_TICKET_USED_ALREADY; break;
        default:
                $ActionType = BAD_ACTION_NO_TICKET;

}

$conds = " AND b.RecordType = $ActionType";

if ($StartDate!="")
{
    $conds .= " AND b.RecordDate >= '$StartDate'";
}
if ($EndDate != "")
{
    $conds .= " AND b.RecordDate <= '$EndDate'";
}

$namefield = getNameFieldWithClassNumberByLang("a.");
$sql = "SELECT $namefield, b.ActionTime
               FROM INTRANET_USER as a , CARD_STUDENT_LUNCH_BAD_LOG as b
               WHERE a.UserID = b.StudentID
                     $conds";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.EnglishName", "b.ActionTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0);
$li->IsColOff = 2;
// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column($pos++, $i_general_record_time)."</td>\n";

# Filtering Criteria
$select_type = "<SELECT name=ActionType>";
$select_type .= "<OPTION value=".BAD_ACTION_NO_TICKET." ".($ActionType==BAD_ACTION_NO_TICKET?"SELECTED":"").">$i_SmartCard_Lunchbox_Report_Type_BadAction_NoTicket</OPTION>";
$select_type .= "<OPTION value=".BAD_ACTION_TICKET_USED_ALREADY." ".($ActionType==BAD_ACTION_TICKET_USED_ALREADY?"SELECTED":"").">$i_SmartCard_Lunchbox_Report_Type_BadAction_TakeAgain</OPTION>";
$select_type .= "</SELECT>\n";

$filterbar = "
<table width=500 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=right>$i_general_startdate:</td><td>
<input type=text name=StartDate value='$StartDate' size=10><script language=\"JavaScript\" type=\"text/javascript\">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback_DateStart', '$intranet_httppath/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td align=right>$i_general_enddate:</td><td>
<input type=text name=EndDate value='$EndDate' size=10><script language=\"JavaScript\" type=\"text/javascript\">
    <!--
         endCal = new dynCalendar('endCal', 'calendarCallback_DateEnd', '$intranet_httppath/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td></td><td>$select_type</td></tr>
<tr><td></td><td>".btnSubmit()."</td></tr>
</table>
";

#$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'analysis.php')\">".newIcon()."$button_new</a>\n";

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_Report, '../', $i_SmartCard_Lunchbox_Report_BadAction , '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback_DateStart(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].StartDate.value = dateValue;
          }
          function calendarCallback_DateEnd(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].EndDate.value = dateValue;
          }
 // -->
 </script>
<form name="form1" method="get">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=<?=$image_path?>/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>