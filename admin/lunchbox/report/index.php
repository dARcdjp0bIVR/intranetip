<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$ShowBadAction = (isset($plugin['Lunchbox_HCLMS']) && $plugin['Lunchbox_HCLMS']) ? 0 : 1;

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../', $i_SmartCard_Lunchbox_Menu_Report, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>



<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_SmartCard_Lunchbox_Report_Daily ,'daily/',1,
                  $i_SmartCard_Lunchbox_Report_Month,'monthly/',1,
                  $i_SmartCard_Lunchbox_Report_BadAction, 'badaction/',$ShowBadAction
                                ) ?>


</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>