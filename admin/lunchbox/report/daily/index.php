<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libclass();
$classList = $lc->getClassList();

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../', $i_SmartCard_Lunchbox_Menu_Report, '../', $i_SmartCard_Lunchbox_Report_Daily , '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].targetDate.value = dateValue;
          }
 // -->

function setAll(isChecked, obj)
{
         element_name = "targetClass[]";
         val = isChecked;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name)
                {
                    obj.elements[i].checked=val;
                    obj.elements[i].disabled=val;
                }
        }
}
 </script>

<form name="form1" method="POST" action="daily_report.php" target=_blank>
<table width=500 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=right><?=$i_SmartCard_Lunchbox_DateField_Date?>:</td><td>
<input type=text name=targetDate value='<?=date('Y-m-d')?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td align=right><?=$i_SmartCard_Lunchbox_Report_Param_Type?></td><td>
<input type=radio name=ReportType value=1 checked onClick="this.form.isSplitClass.checked=false;this.form.isSplitClass.disabled=true;"><?=$i_SmartCard_Lunchbox_Report_Type_Summary?><br>
<input type=radio name=ReportType value=2 onClick="this.form.isSplitClass.disabled=false"><?=$i_SmartCard_Lunchbox_Report_Type_UntakenList?><br>
<input type=radio name=ReportType value=3 onClick="this.form.isSplitClass.disabled=false"><?=$i_SmartCard_Lunchbox_Report_Type_TakenList?><br>
<input type=radio name=ReportType value=4 onClick="this.form.isSplitClass.disabled=false"><?=$i_SmartCard_Lunchbox_Report_Type_StudentList?><br>
&nbsp;&nbsp;<input type=checkbox name=isSplitClass value=1 disabled><?=$i_SmartCard_Lunchbox_Report_Param_SplitClass?>
</td></tr>
<tr><td align=right><?=$i_SmartCard_Lunchbox_Report_Param_SelectClass?></td><td>
<input type=checkbox name=isAllClass CHECKED onClick="setAll(this.checked, this.form)"><?=$i_SmartCard_Lunchbox_Report_Lang_AllClasses?><br>
<?
$lastLvl = $classList[0][2];
for ($i=0; $i<sizeof($classList); $i++)
{
     list($class_id, $class_name, $class_lvl) = $classList[$i];

     if ($i!=0 && $lastLvl != $class_lvl)
     {
         echo "<br>\n";
         $lastLvl = $class_lvl;
     }
     echo "<input type=checkbox name=targetClass[] CHECKED DISABLED value=$class_id> $class_name &nbsp;";
}
?>
</td>
</tr>

</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
</td>
</tr>
</table>

</form>



<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
