<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/liblunchbox.php");
intranet_opendb();

# Checking
if (!isset($targetDate) || $targetDate == "")
{
    header("Location: index.php");
    exit();
}

if (!is_array($StudentID) || sizeof($StudentID)==0 || $targetChange==='')
{
     header("Location: index.php?targetDate=$targetDate");
     exit();
}


$curr_date = date('Y-m-d');

$curr_year = date('Y');
$curr_month = date('m');
$curr_day = date('d');

$llunchbox = new liblunchbox();

$targetTS = strtotime($targetDate);
$targetYear = date('Y',$targetTS);
$targetMonth = date('m',$targetTS);
$targetDay = date('d',$targetTS);


$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month == $targetMonth && $curr_day > $targetDay)
{
     $targetPassed = true;
}
/*
if ($targetPassed)
{
    header("Location: index.php?targetDate=$targetDate");
    exit();
}
else
{
}
*/
$ticket_table_name = $llunchbox->createMonthlyTicketTable($targetYear, $targetMonth);
$student_list = implode(",",$StudentID);

if ($targetChange == 1)
{
    $sql = "UPDATE $ticket_table_name SET RecordStatus = 1, RecordType = 2 , DateModified=now() WHERE DayNumber = $targetDay AND StudentID IN ($student_list) AND RecordStatus = 0";
}
else
{
    $sql = "UPDATE $ticket_table_name SET RecordStatus = 0, RecordType = 2 , DateModified=now() WHERE DayNumber = $targetDay AND StudentID IN ($student_list) AND RecordStatus = 1";
}
$llunchbox->db_db_query($sql);

header("Location: index.php?targetDate=$targetDate&msg=2");
intranet_closedb();
?>