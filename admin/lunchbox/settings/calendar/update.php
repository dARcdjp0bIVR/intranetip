<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/liblunchbox.php");
intranet_opendb();

$llunchbox = new liblunchbox();

$llunchbox->setCalendarDates($targetYear, $targetMonth, $hasLunch);

header ("Location: index.php?msg=2&targetYear=$targetYear&targetMonth=$targetMonth");
intranet_closedb();
?>
