<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libcal.php");
include_once("../../../../includes/libcycleperiods.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$curr_year = date('Y');
$curr_month = date('m');

if (isset($targetYear) && isset($targetMonth) && $targetYear != "" && $targetMonth != "")
{
    $targetSelected = true;
    $selectedYear = $targetYear;
    $selectedMonth = $targetMonth;
}
else
{
    $targetSelected = false;
    $selectedYear = $curr_year;
    $selectedMonth = $curr_month;
}

# Year Selection
$select_year = "<SELECT name=targetYear>\n";
for ($i=-1; $i<=1; $i++)
{
     $temp_year = $selectedYear + $i;
     $select_year .= "<OPTION value=\"$temp_year\" ".($i==0?"SELECTED":"").">$temp_year</OPTION>\n";
}
$select_year .= "</SELECT>\n";

$select_month = "<SELECT name=targetMonth>\n";
for ($i=0; $i<sizeof($i_general_MonthShortForm); $i++)
{
     $temp_value = $i+1;
     $temp_string = $i_general_MonthShortForm[$i];
     $select_month .= "<OPTION value=\"$temp_value\" ".($temp_value==$selectedMonth?"SELECTED":"").">$temp_string</OPTION>\n";
}
$select_month .= "</SELECT>\n";

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../',$i_SmartCard_Lunchbox_Menu_Settings,'../', $i_SmartCard_Lunchbox_Settings_Calendar, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>

<form name="form_change" method="get">
<table width=300 align=center border=0 cellspacing=3 cellpadding=2>
<tr><td align=center><?=$select_year?> <?=$select_month?> <input type=image src="<?=$image_path?>/admin/button/s_btn_continue_<?=$intranet_session_language?>.gif" border=0></td></tr>
</table>
</form>

<? if ($targetSelected) {
$lcal = new libcal();
$lcycleperiods = new libcycleperiods();
$llunchbox = new liblunchbox();
?>
<hr width=85% align=center>
<div align=center><?=$targetYear?> - <?=$i_general_MonthShortForm[$targetMonth-1]?></div>
<form name=form1 method=POST action="update.php">
<?
$targetPassed = false;
if ($curr_year > $targetYear)
{
    $targetPassed = true;
}
else if ($curr_year == $targetYear && $curr_month > $targetMonth)
{
     $targetPassed = true;
}

if ($targetPassed)
{
    echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_CannotEditPrevious</font></div>";
    $checkbox_string = "disabled";

}
else
{
    $tickets_num = $llunchbox->getMonthlyTicketNum($targetYear, $targetMonth);
    if ($tickets_num > 0)
    {
        echo "<div align=center><font color=red size=+1>$i_SmartCard_Lunchbox_Warning_AlreadyHasLunchTicket</font></div>";
    }
    $checkbox_string = "";
}

# Display Calendar
# Get Calendar Setted
$array_set_days = $llunchbox->getCalendarDates($targetYear, $targetMonth);
if ($array_set_days===false || !is_array($array_set_days))
{
    $hasData = false;
}
else
{
    $hasData = true;
    # Build assoc array
    unset($a_array_data);
    for ($i=0; $i<sizeof($array_set_days); $i++)
    {
         $a_array_data[$array_set_days[$i]] = 1;
    }
}

$table_calendar = "";
$table_calendar .= "<table width=90% align=center border=1 cellpadding=1 cellspacing=1>\n";
$table_calendar .= "<tr>\n";
$table_calendar .= "<td class=tableTitle width=1%>&nbsp;</td>";

for ($i=0; $i<sizeof($i_DayType0); $i++)
{
     $table_calendar .= "<td class=tableTitle width=14% align=center>".$i_DayType0[$i]."</td>\n";
}
$table_calendar .= "<td class=tableTitle width=1%>&nbsp;</td>";
$table_calendar .= "</tr>\n";
$array_ts = $lcal->returnCalendarArray($targetMonth, $targetYear);
$array_cycle_info = $lcycleperiods->getCycleInfoByYearMonth($targetYear, $targetMonth);
for ($i=0; $i<sizeof($array_ts); $i++)
{
     $table_calendar .= ($i%7==0?"<tr><td height=30>&nbsp;</td>":"");
     $target_ts = $array_ts[$i];
     if ($target_ts != "")
     {
         $day_value = date('j',$target_ts);
         $target_date = date('Y-m-d', $target_ts);
         $cycle_value = $array_cycle_info[$target_date][2];

         $cell_table = "<table width=100% border=0 cellspacing=1 cellpadding=1>\n";
         $cell_table .= "<tr><td align=left><i>$cycle_value</i></td><td align=right><input type=checkbox name=hasLunch[] value=\"$day_value\" ".($a_array_data[$day_value]==1?"CHECKED":"")." $checkbox_string></td></tr>\n";
         $cell_table .= "<tr><td colspan=2 align=center><font size=+1>$day_value</font></td></tr>\n";
         $cell_table .= "</table>";
     }
     else
     {
         $cell_table = "&nbsp;";
     }
     $table_calendar .= "<td>$cell_table</td>";
     $table_calendar .= ($i%7==6?"<td>&nbsp;</td></tr>":"");
}


$table_calendar .= "</table>\n";


?>
<?=$table_calendar?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<? if (!$targetPassed) { ?>
<input type='image' onClick="return confirm('<?=$i_SmartCard_Lunchbox_Warning_Save?>')" src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'>

<?=btnReset() ?>
<? } ?>
</td>
</tr>
</table>
<input type=hidden name=targetYear value="<?=$targetYear?>">
<input type=hidden name=targetMonth value="<?=$targetMonth?>">


</form>

<? } ?>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
