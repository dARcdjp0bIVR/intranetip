<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/liblunchbox.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$curr_year = date('Y');
$curr_month = date('m');

$llunchbox = new liblunchbox();
$functionbar = "<a href=\"javascript:checkRemove(document.form1,'YearMonth[]','remove.php')\"><img src='$image_path/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$data_set = $llunchbox->getMonthsTable();
$display_table = "";
$display_table .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$display_table .= "<tr class=tableTitle><td>$i_general_Year - $i_general_Month</td><td>#</td><td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'YearMonth[]'):setChecked(0,this.form,'YearMonth[]')></td></tr>\n";
for ($i=0; $i<sizeof($data_set); $i++)
{
     $string = $data_set[$i];
     $array = explode("_",$string);
     list($t_year, $t_month) = $array;
     $t_num = $llunchbox->getMonthlyTicketNum($t_year, $t_month);
     $css = ($i%2?"":"2");
     $display_table .= "<tr class=tableContent$css><td>$t_year - $t_month</td><td>$t_num</td><td><input type=checkbox name=YearMonth[] value=\"$string\"></td></tr>\n";
}
if (sizeof($data_set)==0)
{
    $display_table .= "<tr class=tableContent><td align=center colspan=2><br>$i_no_record_exists_msg<br><br></td></tr>\n";
}

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SmartCard_Lunchbox_System, '../../',$i_SmartCard_Lunchbox_Menu_Settings,'../', $i_SmartCard_Lunchbox_Settings_DataClearance, '') ?>
<?= displayTag("head_lunchbox_$intranet_session_language.gif", $msg) ?>
<form name=form1 method=POST action="clear.php">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu align=right><?php echo $functionbar; ?></td></tr>
<tr><td><img src=<?=$image_path?>/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?=$display_table?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>


</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
