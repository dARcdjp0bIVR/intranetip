<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdbtable.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

intranet_opendb();
# TABLE SQL
$keyword = trim($keyword);

$fieldname = getNameFieldWithClassNumberByLang("c.");
$sql = "SELECT
              c.UserLogin, $fieldname, a.UserType,
              CONCAT('<input type=checkbox name=AccountID[] value=',a.AdminUserID,'>')
        FROM SPORTS_ADMIN_USER_ACL as a
             LEFT OUTER JOIN INTRANET_USER as c ON a.AdminUserID = c.UserID
             ";

#echo $sql;

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("c.EnglishName","c.UserLogin","a.UserType");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0);
$li->IsColOff = "eSportAdmin";
// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column($pos++, $i_Sports_AdminConsole_AccountAllowedToUse)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Sports_AdminConsole_UserTypes)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("AccountID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('admin_new.php')\">".newIcon()."$button_new</a>";
//$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'AccountID[]','admin_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";



$select_admin_level = getSelectByAssoArray($i_Sports_AdminConsole_UserType, "name=userType","",0,1);
$functionbar .= $select_admin_level;
$functionbar .= "<a href=\"javascript:checkAlert(document.form1,'AccountID[]','admin_update.php','$i_Sports_AdminConsole_Alert_ChangeAdminLevel')\"><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'AccountID[]','admin_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_fs, '', $i_Sports_System, 'index.php',$i_Sports_AdminConsole_AccountAllowedToUse,'') ?>
<?= displayTag("head_sports_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>