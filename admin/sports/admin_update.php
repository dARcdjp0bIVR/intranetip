<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libsports.php");
include_once("../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lsports = new libsports();

if (!is_array($AccountID) || sizeof($AccountID) == 0) {
	header("Location: admin.php");
	exit();
}

if ($lsports->updateAdminUser($AccountID, $userType)) {
	$msg = 2;
} else {
	$msg = 14;
}

intranet_closedb();
header("Location: admin.php?msg=$msg");
?>
