<?php

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libannounce.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

function selectionList($arr){
	for ($i=0; $i<sizeof($arr); $i++)
	{
		$rx .= "<option value=\"".$arr[$i][0]."\">".$arr[$i][1]."</option>\n";
	}
	return $rx;
}

$li = new libdb();
$lannounce = new libannounce();

# get admin user
$AdminUser = $lannounce->GET_ADMIN_USER();

if(!empty($AdminUser))
{

	$sql = "select UserID, CONCAT(EnglishName, ' (', ChineseName, ')') from INTRANET_USER where UserID IN ($AdminUser) ORDER BY EnglishName, ChineseName ";
	$row = $li->returnArray($sql, 2);
	$AdminSelectionList = selectionList($row);
}

# get teaching staff
$cond = (!empty($AdminUser)) ? " AND UserID NOT IN ($AdminUser)" : "";
$sql = "select UserID, CONCAT(EnglishName, ' (', ChineseName, ')') from INTRANET_USER where recordtype=1 $cond ORDER BY EnglishName, ChineseName ";
$row = $li->returnArray($sql, 2);
$TeachingStaffList = selectionList($row);


?>
<?= displayNavTitle($i_adminmenu_fs, '', $i_adminmenu_im_announcement, 'settings.php', $i_ReportCard_System_Admin_User_Setting, '') ?>
<?= displayTag("head_announcement_$intranet_session_language.gif", $msg) ?>

<script language="javascript">
function checkform(obj){
	var target_obj = eval("obj.elements['target[]']");
	checkOption(target_obj);
	for(i=0; i<target_obj.length; i++)
	{
		target_obj.options[i].selected = true;
	}

        /*
	target_obj = eval("obj.elements['target_admin[]']");
	checkOption(target_obj);
	for(i=0; i<target_obj.length; i++)
	{
		target_obj.options[i].selected = true;
	}
*/
	return 0;
}
</script>

<form name="form1" method="post" action="admin_setting_update.php" onSubmit="return checkform(this);">
<table width=500 border=0 cellpadding=2 cellspacing=0 align="center">
<tr>
	<td colspan="3" nowrap><span class="extraInfo">[<span class=subTitle><?=$eReportCard["AdminUser"]?></span>]</span></td>
</tr>
<tr>
	<td width="30%" align="right">
        	<select name="target[]" class='inputfield' size=7 multiple>
        	<?=$AdminSelectionList?>
                <? if(sizeof($AdminSelectionList)==0) {?>
                <option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
                <? } ?>
        	</select>
	</td>
	<td align=center nowrap width="10%">
        	<p><input type='image' src='/images/admin/button/s_btn_addto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["source[]"],this.form.elements["target[]"]);return false;'></p>
        	<p><input type='image' src='/images/admin/button/s_btn_deleteto_<?=$intranet_session_language?>.gif' border='0' onClick='checkOptionTransfer(this.form.elements["target[]"],this.form.elements["source[]"]);return false;'></p>
	</td>
	<td width="50%">
        	<select class='inputfield' name="source[]" size=7 multiple>
        	<?=$TeachingStaffList?>
                <? if(sizeof($TeachingStaffList)==0) {?>
        	<option><? for($i = 0; $i < 30; $i++) echo "&nbsp;"; ?></option>
                <? } ?>
        	</select>
	</td>
</tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:window.location.reload();"><img src="/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="./settings.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<p><br></p>
</form>

<?php
intranet_closedb();

include("../../templates/adminfooter.php");
?>
