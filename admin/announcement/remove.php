<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();
$list =implode(",", $AnnouncementID);

$sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID IN ($list)";
$li->db_db_query($sql);

# Get attachment path
$sql = "SELECT AnnouncementID, Attachment FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
$array = $li->returnArray($sql,2);

# Delete attachment
$lf = new libfilesystem();
for ($i=0; $i<sizeof($array); $i++)
{
     if ($array[$i][1]==null || $array[$i][1]=="")
     {}
     else {
          $dir2del = "$file_path/file/announcement/".$array[$i][1].$array[$i][0];
          $lf->lfs_remove($dir2del);
     }
}

$sql = "DELETE FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=3");
?>