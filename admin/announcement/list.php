<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libannounce.php");
include("../../lang/lang.$intranet_session_language.php");
intranet_opendb();
$lo = new libannounce($AnnouncementID);
$read = $lo->returnReadCount();
$total = $lo->returnTargetCount();
$unread = $total - $read;

include("../../templates/fileheader.php");
?>

<?
$status_navigation = "<a href=read.php?AnnouncementID=$AnnouncementID>$i_AnnouncementViewReadStatus</a>";

if ($type == 1)
{
    $title = $i_AnnouncementReadList;
    $list = $lo->returnReadList();
    if ($unread != 0)
        $alt_navigation = "<a href=?AnnouncementID=$AnnouncementID&type=2>$i_AnnouncementUnreadList</a>";
    else
        $alt_navigation = "$i_AnnouncementUnreadList";
}
else
{
    $title = $i_AnnonucementUnreadList;
    $list = $lo->returnUnreadList();
    if ($read != 0)
        $alt_navigation = "<a href=?AnnouncementID=$AnnouncementID&type=1>$i_AnnouncementReadList</a>";
    else
        $alt_navigation = "$i_AnnouncementReadList";
}

#$x = "<table width=75% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=1>\n";
$x = "<table width=75% border=0 cellpadding=2 cellspacing=1>\n";
for ($i=0; $i<sizeof($list); $i++)
{
     list($id,$name, $type) = $list[$i];
     #$x .= "<tr><td>$name</td></tr>\n";
     $x .= "<tr><td>$name ";
     if ($type==3)
     {
         $x .= " <a title=\"$i_general_show_child\" href=javascript:newWindow(\"$intranet_httppath/admin/view_child.php?uid=$id\",2)><img src=\"$image_path/icon_parentlink.gif\" border=0></a>";
#        $x .= " <a href=javascript:newWindow('$intranet_httppath/admin/view_child.php?uid=$id',1)><img src=\"$image_path/icon_parentlink.gif\" border=0></a>";
#         $x .= " <a href=javascript:newWindow(\"$intranet_httppath/admin/view_child.php?uid=$id\",1)><img src=\"$image_path/icon_parentlink.gif\" border=0></a>";
     }
     $x .= "</td></tr>\n";

     $x .= "<tr><td><img src=\"$image_path/groupinfo/viewstatics_nameline.gif\" width=268 height=7></td></tr>\n";
}
$x .= "</table>\n";
#echo "<u>$i_AnnouncementTitle: $lo->Title </u><br>$title<br>";
#echo $x;
?>

<table width="330" border="0" cellspacing="0" cellpadding="0" align=center>
  <tr>
    <td width="10"><img src="<?=$image_path?>/groupinfo/viewstatics_top_l.gif" width="10" height="23"></td>
    <td width="310" bgcolor="#CCAD00"><img src="<?=$image_path?>/icon_viewstatics.gif" width="31" height="19" align="absmiddle">
      <font color="#FFFFFF" size="3"><span class="bodybold"><B><?=$title?></B></span></font></td>
    <td width="10"><img src="<?=$image_path?>/groupinfo/viewstatics_top_r.gif" width="10" height="23"></td>
  </tr>
  <tr>
    <td class=announcement_stat_cell_left>&nbsp;</td>
    <td bgcolor="#FBFCE5"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td class="bodybold"><font color="#003399"><?="$i_AnnouncementTitle: $lo->Title"?></font></td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/groupinfo/viewstatics_titleline.gif" width="310" height="3"></td>
        </tr>
        <tr>
          <td align="right"><table width="60%" border="0" cellpadding="0" cellspacing="0" class="body">
              <tr>
                <td align="right" bgcolor="#EDDA5C"> <div align="center"><b>|<span class="functionlink"><?=$status_navigation?></span>|<span class="functionlink"><?=$alt_navigation?></span>|</b></div></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="80%" border="0" cellpadding="0" cellspacing="0" class="body">
        <tr>
          <td><font color="#031BAC"><?=$title?></font></td>
        </tr>
        <tr>
          <td><img src="<?=$image_path?>/groupinfo/viewstatics_nameline.gif" width="268" height="7"></td>
        </tr>
        <tr>
          <td>
            <?=$x?>
          </td>
        </tr>
      </table></td>
    <td class=announcement_stat_cell_right>&nbsp;</td>
  </tr>
  <tr>
    <td><img src="<?=$image_path?>/groupinfo/viewstatics_btm_l.gif" width="10" height="10"></td>
    <td class=announcement_stat_cell_bottom><img src="<?=$image_path?>/groupinfo/viewstatics_cell_b.gif" width="10" height="10"></td>
    <td><img src="<?=$image_path?>/groupinfo/viewstatics_btm_r.gif" width="10" height="10"></td>
  </tr>
</table>


<?php
include("../../templates/filefooter.php");
intranet_closedb();
?>