<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../lang/email.php");
intranet_opendb();

$li = new libgrouping();

$Title = intranet_htmlspecialchars(trim($Title));
$Description = intranet_htmlspecialchars(trim($Description));
$AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
$EndDate = intranet_htmlspecialchars(trim($EndDate));

$startStamp = strtotime($AnnouncementDate);
$endStamp = strtotime($EndDate);
if (compareDate($startStamp,$endStamp)>0)   // start > end
{
    $valid = false;
}
/*
else if (compareDate($startStamp,time())<0)      // start < now
{
     $valid = false;
}*/
else
{
    $valid = true;
}

if ($valid)
{
     $fieldname  = "Title = '$Title', ";
     $fieldname .= "Description = '$Description', ";
     $fieldname .= "AnnouncementDate = '$AnnouncementDate', ";
     $fieldname .= "EndDate = '$EndDate',";
     $fieldname .= "RecordStatus = '$RecordStatus', ";
     $fieldname .= "RecordType = '$publicdisplay',";
     $fieldname .= "OwnerGroupID = NULL,";
     $fieldname .= "UserID = NULL,";
     $fieldname .= "Internal = NULL,";
     $fieldname .= "DateModified = now()";
     $sql = "UPDATE INTRANET_ANNOUNCEMENT SET $fieldname WHERE AnnouncementID = $AnnouncementID";
     $li->db_db_query($sql);

     $sql = "DELETE FROM INTRANET_GROUPANNOUNCEMENT WHERE AnnouncementID = $AnnouncementID";
     $li->db_db_query($sql);

     for($i=0; $i<sizeof($GroupID); $i++){
             $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES (".$GroupID[$i].", $AnnouncementID)";
             $li->db_db_query($sql);
     }

  	 # Process attachments
	     $lf = new libfilesystem();
	     $sql = "SELECT Attachment FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = $AnnouncementID";
	     $result = $li->returnArray($sql,1);
	     if($result[0][0]==''){
		     $resultAttachment = session_id()."_a";
		     $hasAttachment = false;
		 }else{
			 $resultAttachment = $result[0][0];
			 $hasAttachment = true;
		 }
	     $attachmentPath = "$file_path/file/announcement/".$resultAttachment.$AnnouncementID."/";
		 
	     # num of newly attached files
	     $attachment_size=$attachment_size==""?0:$attachment_size;	
		 
	     # Delete Files
	     $file2delete = array_filter(explode(":",$deleted_files));
	
	     if (sizeof($file2delete) != 0 && $hasAttachment)
	     {
	         for ($i=0; $i<sizeof($file2delete); $i++)
	         {
		         
	          	 $f = urldecode($file2delete[$i]);
		         if(trim($f)=='') continue;
	          	 $target = $attachmentPath.$f;
	             $lf->lfs_remove($target);
	         }
	         $size = $lf->folder_size($attachmentPath);
	         if ($size[1]==0)
	         {
	             $lf->folder_remove($attachmentPath);
	             $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = NULL WHERE AnnouncementID = $AnnouncementID";
	             $li->db_db_query($sql);
	             $hasAttachment = false;
	         }
	     }
	     
	     # Upload Files
	     $update_db = false;
	     for ($i=1; $i<=$attachment_size; $i++)
	     {
	          $key = "filea$i";
	          $loc = ${"filea$i"};
	          $file = stripslashes(${"hidden_userfile_name$i"});
	          #$file = ${"filea$i"."_name"};
	          $des = "$attachmentPath/$file";
	          if ($loc == "none" || $loc=="")
	          {} else
	          {
	             if (strpos($file,"."==0)){
	             } else
	             {
	                   if (!$hasAttachment)
	                   {
	                        //$lf->folder_new("$file_path/file/announcement");
	                        $lf->folder_new ($attachmentPath);
	                        $hasAttachment = true;
	                        $update_db = true;
	                   }
	                   $lf->lfs_copy($loc, $des);
	             }
	          }
	
	     } 
	
	     # Update attachment in DB
	     if ($update_db)
	     {
	          $sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$resultAttachment' WHERE AnnouncementID = $AnnouncementID";
	          $li->db_db_query($sql);
	     }

		if ($plugin['power_voice'])
		{  
			$folder = session_id()."_a";
			    			
  	 		# Process voice
	     	$lf = new libfilesystem();
	     	$sql = "SELECT VoiceFile FROM INTRANET_ANNOUNCEMENT WHERE AnnouncementID = $AnnouncementID";
	     	$result = $li->returnArray($sql,1);
	     	if($result[0][0]=='')
	     	{
				$resultVoiceAttachment = session_id()."_a";
		 	}
		 	else
		 	{
			 	$resultVoiceAttachment = $result[0][0];
		 	}
	        $VoiceFileName = basename($resultVoiceAttachment);    
	        $VoiceDirName1 = dirname($resultVoiceAttachment);
	        $VoiceDirName2 = basename($VoiceDirName1);
		 				     		
     		$path = "$file_path/file/announcement/".$VoiceDirName2."/";
			
	        if (($voiceFile!="")  && ($voicePath!=""))
	        {	        
				if (!is_dir($path))
				{
					$lf->folder_new ($path);
					$hasAttachment = true;
				}
				if ($is_empty_voice=="1")
				{	
					if (($path == $voicePath) || ($voicePath==$path."/") || ($path==$voicePath."/"))
					{
						$loc = $voicePath."/".$VoiceFileName;
						$des = $path."/".$voiceFile;						
					} 
					else
					{
						$loc = $voicePath."/".$voiceFile;
						$des = $path."/".$voiceFile;						
					}
				}				
				else
				{
					$loc = $voicePath."/".$voiceFile;
					$des = $path."/".$voiceFile;
				}
												
				if ($des != $loc)
				{
					if (is_file($loc) && (substr($des,-4)==".mp3"))
					{
												
						$lf->lfs_move($loc, $des);						     
												
						if (is_file($resultVoiceAttachment) && is_file($des))
						{
							$lf->file_remove($resultVoiceAttachment);
						}
					}
								
		         	$sql = 	"	
		         				UPDATE 
		         					INTRANET_ANNOUNCEMENT 
		         				SET 
		         					VoiceFile = '".addslashes($des)."' 
		         				WHERE 
		         					AnnouncementID = {$AnnouncementID}
		         				";
		         	$li->db_db_query($sql);
				}				
			} 
			else
			{
		         	$sql = 	"	
		         				UPDATE 
		         					INTRANET_ANNOUNCEMENT 
		         				SET 
		         					VoiceFile = NULL 
		         				WHERE 
		         					AnnouncementID = {$AnnouncementID}
		         				";
		         	$li->db_db_query($sql);				
			}
		}
	     
  	# End of Process attachments
   

     # Call sendmail function
     if($email_alert==1){
     $type = (sizeof($GroupID)==0? "School": "Group");
     $mailSubject = announcement_title_20($AnnouncementDate,$type); //stripslashes($AnnouncementDate." ".$Title);
     if (sizeof($GroupID)==0)
     {
         $mailBody = school_announcement_body($AnnouncementDate,$Title).email_footer();
     }
     else
     {
         $Groups = $li->returnGroupNames($GroupID);
         $mailBody = group_announcement_body($AnnouncementDate,$Title,$Groups).email_footer();
     }

//     $mailBody = announcement_body($Title, $type).email_footer(); //stripslashes($Description).email_footer();
/*
     $mailSubject = stripslashes($AnnouncementDate." ".$Title);
     $mailBody = stripslashes($Description).email_footer();
*/

	# 20081016 yat woon
	# trim the empty group which contain "&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"
	$tmpGroupID = implode(",", $GroupID);
	$tmpGroupID = str_replace("&#160;","",$tmpGroupID);
	$tmpGroupID = explode(",", $tmpGroupID);
	$GroupID = array_remove_empty($tmpGroupID);

     $mailTo = $li->returnUsersEmailGroup($GroupID);
     $lu = new libsendmail();
     $lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);
     }

     intranet_closedb();
     header("Location: index.php?filter=$RecordStatus&msg=2");
}
else
{
    header ("Location: edit.php?AnnouncementID[]=$AnnouncementID&invalid=1");
}
?>
