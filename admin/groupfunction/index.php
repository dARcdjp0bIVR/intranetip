<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

?>


<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_groupfunction, '') ?>
<?= displayTag("head_groupfunction_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption($i_adminmenu_im_timetable, '/admin/timetable/', $li_menu->is_access_timetable,
				$i_adminmenu_im_group_bulletin, '/admin/groupbulletin/', $li_menu->is_access_groupbulletin,
				$i_adminmenu_im_group_links, '/admin/grouplinks/', $li_menu->is_access_groupfiles,
				$i_adminmenu_im_group_files, '/admin/groupfiles/', $li_menu->is_access_grouplinks) ?>

</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>