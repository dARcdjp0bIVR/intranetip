<?php

########################### Change Log ###########################
/*
 *
 */
##################################################################


$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libcloudreport.php");
include_once($PATH_WRT_ROOT."includes/libcloudreport_ui.php");
include_once($PATH_WRT_ROOT."lang/cloud_report_lang.$intranet_session_language.php");

$image_path = $PATH_WRT_ROOT."images/cloud/";
$eclassCloudLogo = $image_path.'eclass-cloud-logo.png';

$libUi = new libcloudreport_ui();

$username= $config_school_code;
$password=$ecapi_password;
$APIver=1;
$schoolCode=$config_school_code;
$lib = new libcloudreport($username, $password, $APIver, $schoolCode);

if($lib->getAuthorizationCode()){
    $Accesstoken=$lib->getAccesstoken();
    $ClientRecordAry = $lib->getClientRecordAry($Accesstoken);
} else {
    echo('Invalid user access');
    die();
}

$eclassCloudIntro= $Lang['Cloud']['Report']['Intro'];




$currentPlan = $ClientRecordAry[plan];
$planExpiryDate = date("d M Y", strtotime($ClientRecordAry[expiry_date]));
$totalStorageUsage = $ClientRecordAry[disk_used];
$systemUsedAry = $ClientRecordAry['system_used'];
$RecommendationItemAry = array('recommendation');
$RecommendationItemAryindex=0;
foreach((array)$systemUsedAry as $items=>$itemsRecord){
    $RecommendationItemAryindex++;
    array_push($RecommendationItemAry, $items);
}

if($currentPlan)
$usedStoragePercentage= number_format($totalStorageUsage/$currentPlan*100, 2, '.', ''); // 2 decimal

/* setup warning messages */
$warningPercentage='90';
$reminderPercentage='75';
$warningMessage= str_replace('<!--usedStoragePercentage-->', $usedStoragePercentage, $Lang['Cloud']['Report']['StorageWarningMessage']);
$worningClassName='alert-warning';
$reminderClassName='alert-info';
if($usedStoragePercentage>=$warningPercentage){
   $alert= $libUi->getAlert($worningClassName, $warningMessage); 
}elseif($usedStoragePercentage>=$reminderPercentage){
    $alert= $libUi->getAlert($reminderClassName, $warningMessage);
}

$lib->logoutUser($Accesstoken);
?>
<head><?php echo $libUi->include_heeader()?></head>

  <body class="status-bg">
    <div id="main" class="container">
      <div class="cloud-shape"></div>
      <div class="cloud-shape big">
        <img src="<?php echo $eclassCloudLogo?>">
      </div>
<!--       <div id="user-area"> -->
<!--         <i class="far fa-user"></i> Chan Tai Man -->
<!--       </div> -->
      <div id="header-area">
        <h3><?php echo $Lang['Cloud']['Report']['Title']['Greeting']?></h3>
        <p><?php echo $eclassCloudIntro;?></p>
      </div>

      <!-- Alerts -->
      <div id="alert-area">
        <?php echo $alert; ?>
	</div>
      <!-- Package -->
      <div id="plan-area">
      	<h3><?php echo $Lang['Cloud']['Report']['CurrentPackage']; ?></h3>
        <?php  $libUi->displayCurrentPackage($currentPlan,$planExpiryDate);?>
      </div>
      <div id="storage-area">
      <h3 class="title"><?php echo $Lang['Cloud']['Report']['Title']['UsedStorage']?></h3>
        <?php $libUi->displaystorageInfo($currentPlan, $totalStorageUsage); ?>
        <!-- storage bar -->
        <div class="progress">
        <?php $libUi->diaplayUsageBar($systemUsedAry, $currentPlan);?>
        </div>
        <!-- legend -->
        <div class="recommend row">
            <div class="col-md-5 col-sm-12 left">
            	<div class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">
              		<?php $libUi->displaySystemUsageList($systemUsedAry);?>
              	</div>
            </div>
            <div class="col-md-7 col-sm-12">
              <div class="tab-content">
              	<?php $libUi->displayRecommendation($RecommendationItemAry); ?>
              	</div>
              </div>
          </div>
      </div>
    </div>
    <div id="footer" class="container">
      Powered by <img src="<?= $image_path?>eclass.png" width="60" height="25">
    </div>

  <?php echo $libUi->include_JS();?>
    <script>
      $(function () {
       $('[data-toggle="tooltip"]').tooltip()
      })
    </script>
  </body>

