<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$lc = new libcardstudentattend2();
$time_table_mode = $lc->retrieveTimeTableMode();

if (($time_table_mode == 0)||($time_table_mode == ''))
{
	$time_input_mode = 1;
	$time_session_mode = 0;
}
if ($time_table_mode == 1)
{
	$time_input_mode = 0;
	$time_session_mode = 1;
}
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_SmartCard_SystemSettings,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_general_BasicSettings,'basic/',1,
                  $i_SmartCard_TerminalSettings,'terminal/',1,
                  //$i_StudentAttendance_TimeSlotSettings,'slot/',(!$sys_custom['QualiEd_StudentAttendance']),
                  $i_StudentAttendance_TimeSlotSettings,'slot/',$time_input_mode,
                  $i_StudentAttendance_TimeSessionSettings,'slot_session/', $time_session_mode,
                  $i_StudentAttendance_TimeSlotSettings,'slot_q/',($sys_custom['QualiEd_StudentAttendance']),
                  $i_StudentAttendance_LunchSettings, 'lunch/',1
                                ) ?>


</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>