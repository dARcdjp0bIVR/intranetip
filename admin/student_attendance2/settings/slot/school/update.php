<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$sql = "INSERT INTO CARD_STUDENT_PERIOD_TIME
(DayType, DayValue, MorningTime, LunchStart, LunchEnd, LeaveSchoolTime, DateInput, DateModified)
VALUES (0,0,'$normal_am','$normal_lunch','$normal_pm','$normal_leave',now(),now())";

$li->db_db_query($sql);

if ($li->db_affected_rows()!=1)
{
    $sql = "UPDATE CARD_STUDENT_PERIOD_TIME
                   SET MorningTime = '$normal_am', LunchStart = '$normal_lunch',
                       LunchEnd = '$normal_pm', LeaveSchoolTime = '$normal_leave',
                       DateModified= now()
                   WHERE DayType=0 AND DayValue=0";
    $li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?msg=2");
?>