<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$normal_time_array = $lc->getTimeArray(0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave,$non_school_day) = $normal_time_array;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

$week_time_array = $lc->getTimeArrayList(1);
$cycle_time_array = $lc->getTimeArrayList(2);
$today = date('Y-m-d',time());
$sql="SELECT RecordDate,IF(ClassID IS NULL,0,ClassID),TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
   FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate>='$today' AND (ClassID='0' OR ClassID='')ORDER BY RecordDate";
$special_time_array= $lc->returnArray($sql,7);

if (sizeof($week_time_array)!= 0)
{
    $week_table = "<table width=400 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
    $week_table .= "<tr>";
    $week_table .= "<td class=tableTitle>$i_StudentAttendance_WeekDay</td>";
    if($hasAM)
            $week_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_AMStart</td>";
    if($hasLunch)
            $week_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_LunchStart</td>";
    if($hasPM)
            $week_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_PMStart</td>";
    $week_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_SchoolEnd</td>";
    $week_table .= "</tr>\n";
    for ($i=0; $i<sizeof($week_time_array); $i++)
    {
         list($weekday, $week_am, $week_lunch, $week_pm, $week_leave,$non_school_day) = $week_time_array[$i];
         $day = $i_DayType0[$weekday];
         $editlink = "<a class=functionlink href=edit.php?type=1&value=$weekday><img src=\"$image_path/icon_edit.gif\" border=0></a>";
         $editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(1,$weekday)\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
         $week_table .= "<tr>";
         $week_table .= "<td >$day $editlink</td>";
         if($non_school_day==1){
                 $week_table .="<td colspan=4>$i_StudentAttendance_NonSchoolDay</td>";
         }
         else{
                 if($hasAM)
                         $week_table .= "<td >$week_am</td>";
                 if($hasLunch)
                         $week_table .= "<td >$week_lunch</td>";
                 if($hasPM)
                         $week_table .= "<td >$week_pm</td>";
                 $week_table .= "<td >$week_leave</td>";
               }
                        $week_table .= "</tr>\n";
    }
    $week_table .= "</table>\n";
}
if (sizeof($cycle_time_array)!= 0)
{
    $cycle_table = "<table width=400 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
    $cycle_table .= "<tr>";
    $cycle_table .= "<td class=tableTitle>$i_StudentAttendance_CycleDay</td>";
    if($hasAM)
            $cycle_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_AMStart</td>";
    if($hasLunch)
            $cycle_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_LunchStart</td>";
    if($hasPM)
            $cycle_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_PMStart</td>";
    $cycle_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_SchoolEnd</td>";
    $cycle_table .= "</tr>\n";
    for ($i=0; $i<sizeof($cycle_time_array); $i++)
    {
         list($cycleday, $cycle_am, $cycle_lunch, $cycle_pm, $cycle_leave,$non_school_day) = $cycle_time_array[$i];

         $editlink = "<a class=functionlink href=edit.php?type=2&value=$cycleday><img src=\"$image_path/icon_edit.gif\" border=0></a>";
         $editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(2,'$cycleday')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
         $cycle_table .= "<tr>";
         $cycle_table .= "<td >$cycleday $editlink</td>";
         if($non_school_day==1){
                         $cycle_table .="<td colspan=4>$i_StudentAttendance_NonSchoolDay</td>";
         }
         else{
                 if($hasAM)
                         $cycle_table .= "<td >$cycle_am</td>";
                 if($hasLunch)
                         $cycle_table .= "<td >$cycle_lunch</td>";
                 if($hasPM)
                         $cycle_table .= "<td >$cycle_pm</td>";
                  $cycle_table .= "<td >$cycle_leave</td>";
         }
         $cycle_table .= "</tr>\n";

    }
    $cycle_table .= "</table>\n";
}
if (sizeof($special_time_array)!= 0)
{
    $special_table = "<table width=400 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
    $special_table .= "<tr>";
    $special_table .= "<td class=tableTitle>$i_StudentAttendance_Slot_Special</td>";
           if($hasAM)
            $special_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_AMStart</td>";
    if($hasLunch)
            $special_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_LunchStart</td>";
    if($hasPM)
            $special_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_PMStart</td>";
    $special_table .= "<td class=tableTitle>$i_StudentAttendance_SetTime_SchoolEnd</td>";
    $special_table .= "</tr>\n";
    for ($i=0; $i<sizeof($special_time_array); $i++)
    {
         list($specialday, $classid,$special_am, $special_lunch, $special_pm, $special_leave,$non_school_day) = $special_time_array[$i];

         $editlink = "<a class=functionlink href=edit.php?TargetDate=$specialday&ClassID=0&type=3><img src=\"$image_path/icon_edit.gif\" border=0></a>";
         $editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(3,'$specialday')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
         $special_table .= "<tr>";
         $special_table .= "<td >$specialday $editlink</td>";
         if($non_school_day==1){
                         $special_table .="<td colspan=4>$i_StudentAttendance_NonSchoolDay</td>";
         }
         else{
                 if($hasAM)
                         $special_table .= "<td >$special_am</td>";
                 if($hasLunch)
                         $special_table .= "<td >$special_lunch</td>";
                 if($hasPM)
                         $special_table .= "<td >$special_pm</td>";
                  $special_table .= "<td >$special_leave</td>";
         }
         $special_table .= "</tr>\n";

    }
    $special_table .= "</table>\n";
}
?>
<SCRIPT LANGUAGE=Javascript>
        <!--
function removeDaySetting(DayType, DayValue)
{

         if (confirm('<?=$i_Usage_RemoveConfirm?>'))
         {
                     location.href = "remove.php?type="+DayType+"&value="+DayValue;
         }
}
function removeSpecialDaySetting(targetDate){
         if (confirm('<?=$i_Usage_RemoveConfirm?>'))
         {
                     location.href = "special_date_remove.php?TargetDate="+targetDate;
         }

}
function checkForm(formObj){

        amObj = formObj.normal_am;
        lunchObj=formObj.normal_lunch;
        pmObj = formObj.normal_pm;
        leaveObj = formObj.normal_leave;

        if(amObj!=null){
                am = amObj.value;
                if(!isValidTimeFormat(am)){
                        amObj.focus();
                        return false;
                }
        }
        if(lunchObj!=null){
                lunch = lunchObj.value;
                if(!isValidTimeFormat(lunch)){
                        lunchObj.focus();
                        return false;
                }
        }
        if(pmObj!=null){
                pm = pmObj.value;
                if(!isValidTimeFormat(pm)){
                        pmObj.focus();
                        return false;
                }
        }
        if(leaveObj!=null){
                leave = leaveObj.value;
                if(!isValidTimeFormat(leave)){
                        leaveObj.focus();
                        return false;
                }
        }
        if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
                return true;
        return false;
}
function isValidTimeFormat(timeVal){
                // check if the timeVal is in the form of hh:mm
          var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
                if (!timeVal.match(re)) {
                                alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
                                return false;
                }
                return true;
}
function isValidValues(amObj,lunchObj,pmObj,leaveObj){
//                        amObj = formObj.normal_am;
//                        lunchObj=formObj.normal_lunch;
//                        pmObj = formObj.normal_pm;
//                        leaveObj = formObj.normal_leave;
                        if(amObj!=null){
                                        if(lunchObj!=null && amObj.value>=lunchObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
                                                amObj.focus();
                                                return false;
                                        }
                                        else if(pmObj!=null && amObj.value>=pmObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
                                                amObj.focus();
                                                return false;
                                        }
                                        else if(leaveObj!=null && amObj.value>=leaveObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
                                                amObj.focus();
                                                return false;
                                        }
                        }
                        if(lunchObj!=null){
                                        if(pmObj!=null && lunchObj.value>=pmObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
                                                lunchObj.focus();
                                                return false;
                                        }
                                        else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
                                                alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
                                                lunchObj.focus();
                                                return false;
                                        }
                        }
                        if(pmObj!=null){
                                        if(leaveObj!=null && pmObj.value>=leaveObj.value){
                                                        alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
                                                        pmObj.focus();
                                                        return false;
                                        }
                        }
                        return true;
}
// -->
</SCRIPT>
<form name="form1" method="POST" ACTION="update.php" onSubmit="return checkForm(this)">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_School,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_NormalDays?></span>]</span>
      <table width=400 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>
        <tr>
      <?
      ### Mode 1,3,4
      if ($hasAM) {
      ?>
          <td class=tableTitle><?=$i_StudentAttendance_SetTime_AMStart?></td>
      <? } ?>
      <?
      ### Mode 3,4
      if ($hasLunch) {
      ?>
          <td class=tableTitle><?=$i_StudentAttendance_SetTime_LunchStart?></td>
      <? } ?>
      <?
      ### Mode 2,3,4
      if ($hasPM) {
      ?>
          <td class=tableTitle><?=$i_StudentAttendance_SetTime_PMStart?></td>
      <? } ?>
      <?
      ### Mode 1,2,3,4
      ?>
          <td class=tableTitle><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
        </tr>
        <tr>
      <?
      ### Mode 1,3,4
      if ($hasAM) {
      ?>
          <td><input type=text size=5 maxlength=5 name=normal_am value="<?=$normal_am?>"></td>
      <? } ?>
      <?
      ### Mode 3,4
      if ($hasLunch) {
      ?>
          <td><input type=text size=5 maxlength=5 name=normal_lunch value="<?=$normal_lunch?>"></td>
      <? } ?>
      <?
      ### Mode 2,3,4
      if ($hasPM) {
      ?>
          <td><input type=text size=5 maxlength=5 name=normal_pm value="<?=$normal_pm?>"></td>
      <? } ?>
      <?
      ### Mode 1,2,3,4
      ?>
          <td><input type=text size=5 maxlength=5 name=normal_leave value="<?=$normal_leave?>"></td>
        </tr>
        <tr>
          <td colspan=4 align=right><input type=image src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif"></td>
        </tr>
      </table>
<br>
<p><?=$i_StudentAttendance_Slot_SettingsDescription?></p>
</blockquote>
</form>
<blockquote>
<a name='1'><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_Weekday_Specific?></span>]</span></a>
<a href=add.php?type=1><img  align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a>
<?=$week_table?>
</blockquote>
<blockquote>
<a name='2'><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_Cycleday_Specific?></span>]</span></a>
<a href=add.php?type=2><img  align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a>
<?=$cycle_table?>
</blockquote>
<BLOCKQUOTE>
<a name='3'><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_SpecialDay?></span>]</span></a>
<a href=add.php?type=3><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a>
<a class=iconLink href="details.php"><?=detailIcon().$i_StudentAttendance_ViewPastRecords?></a>
<?=$special_table?>
</blockquote>

<?
include_once("../../../../../templates/adminfooter.php");
intranet_closedb();
?>
