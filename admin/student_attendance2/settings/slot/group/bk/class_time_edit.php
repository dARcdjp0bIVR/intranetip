<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
/*
$normal_time_array = $lc->getTimeArray(0,0);
list($normal_am, $normal_lunch, $normal_pm, $normal_leave) = $normal_time_array;
*/
if($type==1 || $type==2||$type==0){
	$preset_time_array = $lc->getClassTimeArray($ClassID,$type,$value);
	list($preset_am, $preset_lunch, $preset_pm, $preset_leave,$non_school_day) = $preset_time_array;
}else{
	# if target day < today
	$TargetDate=$value;
	if($TargetDate<date('Y-m-d')){
		echo "<table border=0 width=560>";
		echo "<tr><td>$i_StudentAttendance_Warn_Cannot_Modify_Past_Records</td></tr>";
		echo "<tr><td><a href=\"class_edit.php?ClassID=$ClassID#$type\"><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a>";
		echo "</td></tr></table>";
		exit;
	}
	$preset_time_array=$lc->getClassSpecialTimeArray($ClassID,$value);
	list($record_date,$preset_am,$preset_lunch,$preset_pm,$preset_leave,$non_school_day)=$preset_time_array;
}
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

if ($type==2)  # Cycle
{
    $select_title = $i_StudentAttendance_CycleDay;
    $word = $value;
}
else if($type==1)  # Week
{
    $select_title = $i_StudentAttendance_WeekDay;
    $word = $i_DayType0[$value];
}
else if ($type==3)
{
    $select_title = "$i_StudentAttendance_TimeSlot_SpecialDay";
    $word = $value;
}


?>
<script language="javascript">
	<!--
	function setForm(formObj){
		nonSchoolDayObj = formObj.non_school_day;
		amObj = formObj.preset_am;
		lunchObj=formObj.preset_lunch;
		pmObj = formObj.preset_pm;
		leaveObj = formObj.preset_leave;
		if(nonSchoolDayObj!=null){
			if(nonSchoolDayObj.checked){
				if(amObj!=null) amObj.disabled=true;
				if(lunchObj!=null) lunchObj.disabled=true;
				if(pmObj!=null) pmObj.disabled=true;
				if(leaveObj!=null) leaveObj.disabled=true;
			}else{
				if(amObj!=null) amObj.disabled=false;
				if(lunchObj!=null) lunchObj.disabled=false;
				if(pmObj!=null) pmObj.disabled=false;
				if(leaveObj!=null) leaveObj.disabled=false;
			}
		}
	}
		function resetForm(formObj){
			formObj.reset();
			setForm(formObj);
	}
	function checkForm(formObj){
		if(formObj.non_school_day!=null && formObj.non_school_day.checked==true)
				return true;
	
		amObj = formObj.preset_am;
		lunchObj=formObj.preset_lunch;
		pmObj = formObj.preset_pm;
		leaveObj = formObj.preset_leave;
	
		if(amObj!=null){
			am = amObj.value;
			if(!isValidTimeFormat(am)){
				amObj.focus();
				return false;
			}
		}
		if(lunchObj!=null){
			lunch = lunchObj.value;
			if(!isValidTimeFormat(lunch)){
				lunchObj.focus();
				return false;
			}
		}
		if(pmObj!=null){
			pm = pmObj.value;
			if(!isValidTimeFormat(pm)){
				pmObj.focus();
				return false;
			}
		}
		if(leaveObj!=null){
			leave = leaveObj.value;
			if(!isValidTimeFormat(leave)){
				leaveObj.focus();
				return false;
			}
		}
	
		if(isValidValues(amObj,lunchObj,pmObj,leaveObj))
			return true;
		return false;
	}
	function isValidTimeFormat(timeVal){
  	var re = new RegExp("^(([0-1][0-9])|2[0-3]):[0-5][0-9]$");
		if (!timeVal.match(re)) {
				alert("<?=$i_StudentAttendance_Warn_Invalid_Time_Format?>");
				return false;
		}
		return true;
	}
	function isValidValues(amObj,lunchObj,pmObj,leaveObj){
			if(amObj!=null){
					if(lunchObj!=null && amObj.value>=lunchObj.value){ 
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart?>");
						amObj.focus();
						return false;
					}
					else if(pmObj!=null && amObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart?>");
						amObj.focus();
						return false;
					}
					else if(leaveObj!=null && amObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd?>");
						amObj.focus();
						return false;
					}
			}
			if(lunchObj!=null){
					if(pmObj!=null && lunchObj.value>=pmObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart?>");
						lunchObj.focus();
						return false;
					}
					else if(leaveObj!=null && lunchObj.value>=leaveObj.value){
						alert("<?=$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd?>");
						lunchObj.focus();
						return false;
					}
			}
			if(pmObj!=null){
					if(leaveObj!=null && pmObj.value>=leaveObj.value){
							alert("<?=$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd?>");
							pmObj.focus();
							return false;
					}
			}
			return true;
}
	// -->
	</script>
<form name="form1" method="POST" ACTION="class_time_edit_update.php" onSubmit="return checkForm(this)">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Class,'index.php',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
      <table width=500 border=0 cellpadding=2 cellspacing=0>
        <tr>
          <td ><?=$select_title?>:</td>
          <td><?=$word?></td>
        </tr>
      <?
      ### Mode 1,3,4
      if ($hasAM) {
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_AMStart?>:</td>
          <td><input type=text size=5 maxlength=5 <?=($non_school_day==1?"disabled=\"true\"":"")?> name=preset_am value="<?=$preset_am?>"></td>
        </tr>
      <? } ?>
      <?
      ### Mode 3,4
      if ($hasLunch) {
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_LunchStart?></td>
          <td><input type=text size=5 maxlength=5 <?=($non_school_day==1?"disabled=\"true\"":"")?> name=preset_lunch value="<?=$preset_lunch?>"></td>
        </tr>
      <? } ?>
      <?
      ### Mode 2,3,4
      if ($hasPM) {
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_PMStart?></td>
          <td><input type=text size=5 maxlength=5 <?=($non_school_day==1?"disabled=\"true\"":"")?> name=preset_pm value="<?=$preset_pm?>"></td>
        </tr>
      <? } ?>
      <?
      ### Mode 1,2,3,4
      ?>
        <tr>
          <td ><?=$i_StudentAttendance_SetTime_SchoolEnd?></td>
          <td><input type=text size=5 maxlength=5 <?=($non_school_day==1?"disabled=\"true\"":"")?> name=preset_leave value="<?=$preset_leave?>"></td>
        </tr>
                <tr>
          <td ><?=$i_StudentAttendance_NonSchoolDay?></td>
           <td><input type=checkbox onClick="setForm(this.form)" name="non_school_day" <?=($non_school_day==1?"CHECKED":"")?>></td>
        </tr>
      </table>
<br>
<p><?=$i_StudentAttendance_Slot_SettingsDescription?></p>
</blockquote>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit()?>
<a href="javascript:resetForm(document.form1)"><img src='/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=ClassID value="<?=$ClassID?>">
<input type=hidden name=DayType value="<?=$type?>">
<input type=hidden name=DayValue value="<?=$value?>">
</form>

<?
include_once("../../../../../templates/adminfooter.php");
intranet_closedb();
?>