<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libdbtable.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

$className = $lc->getClassName($ClassID);
include_once("../../../../../templates/adminheader_setting.php");

$field=$field==""?"RecordDate":$field;
$orderBy=$order==1?"asc":"desc";
$filter = trim($viewby);
$filter=$filter==""?0:$filter;
switch($filter){
		case 0: $condition = "";break;
		case 1: $condition = " AND RecordDate>='".date('Y-m-d',time())."'"; break;
		case 2: $condition = " AND RecordDate<'".date('Y-m-d',time())."'";break;
		default:$condition ="";
}

$FromDate=$FromDate==""?((date('Y')-1)."-09-01"):$FromDate;
$ToDate=$ToDate==""?(date('Y')."-08-31"):$ToDate;

$condition2= " AND RecordDate BETWEEN '$FromDate' AND '$ToDate' ";
$sql=" SELECT RecordDate,IF(NonSchoolDay=1,'--',MorningTime) AS MorningTime,IF(NonSchoolDay=1,'--',LunchStart) AS LunchStart,IF(NonSchoolDay=1,'--',LunchEnd) AS LunchEnd,IF(NonSchoolDay=1,'--',LeaveSchoolTime) AS LeaveSchoolTime,NonSchoolDay FROM CARD_STUDENT_SPECIFIC_DATE_TIME  WHERE ClassID='$ClassID' $condition $condition2 order by $field $orderBy";
//echo "<p>$sql</p>";
$specialResults = $lc->returnArray($sql,6);


$table_content = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$table_content.="<tr><td colspan=7><a class=iconLink href=javascript:openPrintPage()> <?php echo $i_PrinterFriendlyPage; ?></a></td></tr>\n";
$table_content.="<tr><td class=\"TableTitle\">#</td>\n";
	$table_content.="<td class=\"TableTitle\"><a href=javascript:sortByField('RecordDate') onMouseOver=\"window.status='$list_sortby $i_StaffAttendance_Field_RecordDate';return true;\" onMouseOut=\"window.status='';return true;\">$i_StaffAttendance_Field_RecordDate".($field=="RecordDate"?("<img src='/images/".($order==1?"desc":"asc").".gif' hspace=2 border=0>"):"")."</a></td>\n";
if($hasAM)
	$table_content.="<td class=\"TableTitle\"><a href=javascript:sortByField('MorningTime') onMouseOver=\"window.status='$list_sortby $i_StudentAttendance_SetTime_AMStart';return true;\" onMouseOut=\"window.status='';return true;\">$i_StudentAttendance_SetTime_AMStart".($field=="MorningTime"?("<img src='/images/".($order==1?"desc":"asc").".gif' hspace=2 border=0>"):"")."</a></td>\n";
if($hasLunch)
	$table_content.="<td class=\"TableTitle\"><a href=javascript:sortByField('LunchStart') onMouseOver=\"window.status='$list_sortby $i_StudentAttendance_SetTime_LunchStart';return true;\" onMouseOut=\"window.status='';return true;\">$i_StudentAttendance_SetTime_LunchStart".($field=="LunchStart"?("<img src='/images/".($order==1?"desc":"asc").".gif' hspace=2 border=0>"):"")."</a></td>\n";
if($hasPM)
	$table_content.="<td class=\"TableTitle\"><a href=javascript:sortByField('LunchEnd') onMouseOver=\"window.status='$list_sortby $i_StudentAttendance_SetTime_PMStart';return true;\" onMouseOut=\"window.status='';return true;\">$i_StudentAttendance_SetTime_PMStart".($field=="LunchEnd"?("<img src='/images/".($order==1?"desc":"asc").".gif' hspace=2 border=0>"):"")."</a></td>\n";
$table_content.="<td class=\"TableTitle\"><a href=javascript:sortByField('LeaveSchoolTime') onMouseOver=\"window.status='$list_sortby $i_StudentAttendance_SetTime_SchoolEnd';return true;\" onMouseOut=\"window.status='';return true;\">$i_StudentAttendance_SetTime_SchoolEnd".($field=="LeaveSchoolTime"?("<img src='/images/".($order==1?"desc":"asc").".gif' hspace=2 border=0>"):"")."</a></td>\n";


$table_content.="</tr>\n";

if (sizeof($specialResults) == 0){
		$table_content.="<tr><td colspan=6 align=center>$i_no_record_exists_msg</td></tr>\n";
}else{
		for($i=0;$i<sizeof($specialResults);$i++){
				list($recordDate,$am,$lunch,$pm,$leave,$off) = $specialResults[$i];
				$css = $i%2==1?2:"";
				$table_content.="<tr><td class=tableContent$css>".($i+1)."</td>\n";
				$table_content.="<td class=tableContent$css>$recordDate</td>\n";
				if($off==1){
						$table_content.="<td class=tableContent$css colspan=4>$i_StudentAttendance_NonSchoolDay</td></tr>\n";
						continue;
				}else{
						if($hasAM)
							$table_content.="<td class=tableContent$css>$am</td>\n";
						if($hasLunch)
							$table_content.="<td class=tableContent$css>$lunch</td>\n";
						if($hasPM)
							$table_content.="<td class=tableContent$css>$pm</td>\n";
						$table_content.="<td class=tableContent$css>$leave</td>\n";
				}
				$table_content.="</tr>\n";
		}
		$table_content.="</table>\n";
}
	
$select_view.="$i_StaffAttendance_IntermediateRecord_SelectView: <SELECT name=viewby onChange=\"javascript:viewBy(this.form)\">";
$select_view.="<OPTION value=0 ".($filter==0?"SELECTED":"").">$i_status_all</OPTION><OPTION value=1 ".($filter==1?"SELECTED":"").">$i_StudentAttendance_TodayOnward</OPTION><OPTION value=2 ".($filter==2?"SELECTED":"").">$i_StudentAttendance_Past</OPTION></SELECT>&nbsp;";
$class = "&nbsp;".$i_ClassName.":".$className;
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
	          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           		document.forms['form1'].FromDate.value = dateValue;
                           
          }
          function calendarCallback2(date, month, year)
          {								
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
		                           document.forms['form1'].ToDate.value = dateValue;

          }

	function viewBy(formObj){
			if(checkForm(formObj))
				formObj.submit();
	}
	function checkForm(formObj){
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			if(!checkDate(fromV)){
					//formObj.FromDate.focus();
					return false;
			}
			else if(!checkDate(toV)){
						//formObj.ToDate.focus();
						return false;
			}
				return true;
	}
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}
	function sortByField(f){
			orderOld = document.form1.order.value;
			fieldOld = document.form1.field.value;
			if(fieldOld ==f){
					orderNew = orderOld==1?2:1;
			}else orderNew = orderOld;
			document.form1.field.value = f;
			document.form1.order.value = orderNew;
			if(checkForm(document.form1))
				document.form1.submit();
	}
		function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
</script>
<form name="form1" method="GET" onSubmit="return checkForm(this)">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Class,'index.php',$className,'class_edit.php?ClassID='.$ClassID,$i_StudentAttendance_SpecialDay,'') ?>

<table border=0 width=560 align=center>
	<tr>
		<td nowrap class=tableContent><?=$i_Profile_SelectSemester?>:
		<br><?=$i_Profile_From?>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;
		 	<?=$i_Profile_To?>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 <a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr>
	<td class=admin_bg_menu>
<table border=0 width=560><tr>
	<td align=right></td>
	<td></td>
	<td align=right><?=$select_view?></td></tr>
	<tr><td align=right></td>
			<td></td><td>
</td></tr>
</table>

	</td>
<tr><td class=admin_bg_menu>&nbsp;</td></tr>
<tr><td class=admin_bg_menu><?=$class?></td></tr>
</table>
<?=$table_content?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<input type=hidden name=ClassID value=<?=$ClassID?>>
<input type=hidden name=field  value=<?=$field?>>
<input type=hidden name=order value=<?=$order?>>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
intranet_closedb();
?>