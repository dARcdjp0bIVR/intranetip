<?
// using kenneth hcung
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lclass = new libclass();

//if($SetOtherClasses == 1)
//{
	if(($selected_type == "") || ($selected_type == 0))
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				//list($class[$i]) = $class[$i];
								
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ($class[$i], 1, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = $class[$i]";
				$lclass->db_db_query($sql);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR VALUES ('', $class[$i], 0, 0, $SessionID, '', '', NOW(), NOW())";
				$lclass->db_db_query($sql);
				
				if ($lclass->db_affected_rows()==0)
				{
					$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $SessionID, DateModified = NOW() WHERE ClassID = $class[$i] AND DayType = 0 AND DayValue = 0";
					$lclass->db_db_query($sql);
				}
			}
		}
	}

	if(($selected_type == 1)||($selected_type == 2))
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				//list($class[$i]) = $class[$i];
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ($class[$i], 1, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = $class[$i]";
				$lclass->db_db_query($sql);
				
				$sql = "SELECT RecordID, ClassID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = $class[$i] AND DayType = $selected_type AND DayValue = $DayValue";
				$temp_class = $lclass->returnArray($sql,2);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_REGULAR (ClassID, DayType, DayValue, SessionID, DateInput, DateModified) VALUES ($class[$i], $selected_type, '$DayValue', $SessionID, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				if(sizeof($temp_class)>0)
				{
					for($j=0; $j<sizeof($temp_class); $j++)
					{
						list($r_id, $c_id) = $temp_class[$j];
						$sql = "UPDATE CARD_STUDENT_TIME_SESSION_REGULAR SET SessionID = $SessionID , DateModified = 'NOW()' WHERE RecordID = $r_id";
						$lclass->db_db_query($sql);
					}
				}
			}
		}
	}
	if($selected_type == 3)
	{
		$class_list = implode(",",$class);
		if(sizeof($class)>0)
		{
			for($i=0; $i<sizeof($class); $i++)
			{
				//list($class[$i]) = $class[$i];
				$sql = "INSERT IGNORE INTO CARD_STUDENT_CLASS_SPECIFIC_MODE (ClassID, Mode, DateInput, DateModified) VALUES ($class[$i], 1, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				$sql = "UPDATE CARD_STUDENT_CLASS_SPECIFIC_MODE SET Mode = 1, DateModified = 'NOW()' WHERE ClassID = $class[$i]";
				$lclass->db_db_query($sql);
				
				$sql = "SELECT RecordID, ClassID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = $class[$i] AND RecordDate = $TargetDate";
				$temp_class = $lclass->returnArray($sql,2);
				
				$sql = "INSERT IGNORE INTO CARD_STUDENT_TIME_SESSION_DATE (ClassID, RecordDate, SessionID, DateInput, DateModified) VALUES ($class[$i], '$TargetDate', $SessionID, 'NOW()', 'NOW()')";
				$lclass->db_db_query($sql);
				
				if(sizeof($temp_class)>0)
				{
					for($j=0; $j<sizeof($temp_class); $j++)
					{
						list($r_id, $c_id) = $temp_class[$j];
						$sql = "UPDATE CARD_STUDENT_TIME_SESSION_DATE SET SessionID = $SessionID, DateModified = 'NOW()' WHERE RecordID = $r_id";
						$lclass->db_db_query($sql);
					}
				}
			}
		}
	}
//}

Header ("Location: index.php?msg=2");
intranet_closedb();
?>