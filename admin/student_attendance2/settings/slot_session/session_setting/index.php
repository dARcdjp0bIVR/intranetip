<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");

intranet_opendb();
$lclass = new libclass();

//echo "session_setting/index.php";

$sql = "SELECT 
				SessionID, SessionName, RecordType, RecordStatus
		FROM
				CARD_STUDENT_TIME_SESSION
		ORDER BY
				SessionID";
//echo $sql;

$temp = $lclass->returnArray($sql,4);

$table_content = "";

if(sizeof($temp)>0)
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list ($s_id, $s_name, $record_type, $record_status) = $temp[$i];
		
		$table_content .= "<tr><td><a href=\"edit.php?s_id=$s_id\">$s_name</a></td>";
		
		if(($record_status == "")||($record_status == 0))
		{
			$table_content .= "<td align=center><input type=checkbox name=record_status[] value=$s_id></td></tr>";
		}
		if ($record_status == 1)
		{
			$table_content .= "<td align=center><input type=checkbox name=record_status[] value=$s_id checked></td></tr>";
		}

	}
}
if(sizeof($temp)==0)
{
	$table_content .= "<tr><td colspan=3 align=center>$i_no_record_exists_msg</td></tr>";
}

$new_bar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$update_bar = "<a href='javascript:submitForm()'><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
//$edit_bar .= "<a href=\"javascript:checkEdit(document.form1,'','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

?>
<SCRIPT Language="JavaScript">
function setChecked(val, obj, element_name){
        len=obj.elements.length;
        var i=0;
        var temp;
        
        if (val == 1)
        {
	        for( i=0 ; i<len ; i++) 
	        {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
            	}
            }
        }
        else
        {
	        for( i=0 ; i<len ; i++) 
	        {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
            	}
            }
        }
    }

function submitForm()
{
	document.form1.submit();
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Session_Setting,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="update.php" method=POST>
<table width=340 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan=2><img src="/images/admin/table_head0.gif" width="340" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu align=left><?=$new_bar?></td><td class=admin_bg_menu align=right><?=$update_bar?></td></tr>
</table>

<table width=340 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr><td class=tableTitle width=80%><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
<td class=tableTitle width=20%>&nbsp;<?=$i_general_active?>&nbsp;<input type=checkbox name=active onClick="(this.checked)?setChecked(1,this.form,'record_status[]'):setChecked(0,this.form,'record_status[]')"></td></tr>
<?=$table_content?>
</table>

<table width=340 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="340" height="16" border="0"></td></tr>
</table>

</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>