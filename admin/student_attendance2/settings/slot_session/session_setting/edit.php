<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();
$lclass = new libclass();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

if(($hasAM) && ($hasLunch) && ($hasPM))
{
	$mode = "3";
}
else if(($hasAM) && (!$hasLunch) && (!$hasPM))
{
	$mode = "1";
}
else if((!$hasAM) && (!$hasLunch) && ($hasPM))
{
	$mode = "2";
}
else
{
	$mode = "0";
}

$hours = array();
$minutes = array();

for($i=0; $i<24; $i++)
{
	if($i<10)
		$i='0'.$i;
	array_push($hours, $i);
}

for($j=0; $j<60; $j++)
{
	if($j<10)
		$j='0'.$j;
	array_push($minutes, $j);
}

if ($s_id!="")
{
	$sql = "SELECT SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay, RecordType, RecordStatus FROM CARD_STUDENT_TIME_SESSION WHERE SessionID = $s_id";
	$temp = $lclass->returnArray($sql,9);
	list ($s_id, $s_name, $morning_time, $lunch_start, $lunch_end, $leave_school, $non_school_day, $record_type, $record_status) = $temp[0];
	
	$table_content = "";
	$table_content 	.= "<input type=hidden name=s_id value=$s_id>";
	$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Slot_Session_Name:</td><td><input type=text name='s_name' value='$s_name'></td></tr>";
	if($hasAM)
	{
		$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Time_Session_MorningTime:</td>";
		if($non_school_day==0)
		{
			$temp1 = substr($morning_time,0,2);
			$temp2 = substr($morning_time,-2);
			$table_content .= "<td>".getSelectByValue($hours,'name=am_hours', $temp1, 0, 1).":".getSelectByValue($minutes,'name=am_minutes', $temp2, 0, 1)."</td></tr>";
		}
		else
		{
			$table_content .= "<td>".getSelectByValue($hours,'name=am_hours disabled', 0, 0, 1).":".getSelectByValue($minutes,'name=am_minutes disabled', 0, 0, 1)."</td></tr>";
		}
	}
	if($hasLunch)
	{
		$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Time_Session_LunchStartTime:</td>";
		if($non_school_day==0)
		{
			$temp1 = substr($lunch_start,0,2);
			$temp2 = substr($lunch_start,-2);
			$table_content .= "<td>".getSelectByValue($hours,'name=lunch_hours', $temp1, 0, 1).":".getSelectByValue($minutes,'name=lunch_minutes', $temp2, 0, 1)."</td></tr>";
		}
		else
		{
			$table_content .= "<td>".getSelectByValue($hours,'name=lunch_hours disabled', 0, 0, 1).":".getSelectByValue($minutes,'name=lunch_minutes disabled', 0, 0, 1)."</td></tr>";
		}
	}
	if($hasPM)
	{
		$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Time_Session_LunchEndTime:</td>";
		if($non_school_day==0)
		{
			$temp1 = substr($lunch_end,0,2);
			$temp2 = substr($lunch_end,-2);			
			$table_content .= "<td>".getSelectByValue($hours,'name=pm_hours', $temp1, 0, 1).":".getSelectByValue($minutes,'name=pm_minutes', $temp2, 0, 1)."</td></tr>";
		}
		else
		{
			$table_content .= "<td>".getSelectByValue($hours,'name=pm_hours disabled', 0, 0, 1).":".getSelectByValue($minutes,'name=pm_minutes disabled', 0, 0, 1)."</td></tr>";
		}
	}
	$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime:</td>";
	if($non_school_day==0)
	{		
		$temp1 = substr($leave_school,0,2);
		$temp2 = substr($leave_school,-2);
		$table_content .= "<td>".getSelectByValue($hours,'name=leave_hours', $temp1, 0, 1).":".getSelectByValue($minutes,'name=leave_minutes', $temp2, 0, 1)."</td></tr>";
	}
	else
	{
		$table_content .= "<td>".getSelectByValue($hours,'name=leave_hours disabled', 0, 0, 1).":".getSelectByValue($minutes,'name=leave_minutes disabled', 0, 0, 1)."</td></tr>";
	}
	if(($non_school_day==0)||($non_school_day==""))
	{
		$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Time_Session_NonSchoolDay:</td><td><input type=checkbox onClick=checkDisable() name='non_school_day' onClick='this.checked? this.value=1 : this.value=0'></td></tr>";
	}
	if($non_school_day==1)
	{
		$table_content .= "<tr><td align=right>$i_StudentAttendance_Menu_Time_Session_NonSchoolDay:</td><td><input type=checkbox onClick=checkDisable() name='non_school_day' onClick='this.checked? this.value=1 : this.value=0' checked></td></tr>";
	}
	//$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><SELECT name=SetOtherClasses onChange=\"this.form.submit(); document.form1.flag.value=1; \"><OPTION value=0 SELECTED>No</OPTION><OPTION value=1>Yes</OPTION></SELECT></td></tr>";
	$table_content .= "<tr><td></td><td><a class=functionlink_new href='multi_class.php?SessionID=$s_id'>$i_StudentAttendance_TimeSession_OtherClassUseSameSession</a></td></tr>";
	
	/*
	if($SetOtherClasses != '')
	{
		if($SetOtherClasses == 0)
			$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
		else
			$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
	}
	else
	{
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
	}
	
	if($SetOtherClasses == 1)
	{
		$temp_arr = array(array(1,"$i_StudentAttendance_Weekday_Specific"),array(2,"$i_StudentAttendance_Cycleday_Specific"),array(3,"$i_StudentAttendance_SpecialDay"));
		$result = $lclass->getClassList();
		$select_type = getSelectByArray($temp_arr, "name=selected_type onChange=\"document.form1.flag.value=1; this.form.submit();\"", $selected_type);
		
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_TypeSelection:</td>";
		$table_content .= "<td>$select_type</td></tr>";
		if($selected_type == 1)
		{
			$select_day = "<SELECT name=DayValue>\n";
		    $select_day .= "<OPTION value=''>-- $button_select --</OPTION>";
		    for ($i=0; $i<7; $i++)
		    {
		         $word = $i_DayType0[$i];
		         $select_day .= "<OPTION value='".$i."'>".$word."</OPTION>\n";
		    }
		    $select_day .= "</SELECT>\n";
			$table_content .= "<tr><td align=right>$i_StudentAttendance_WeekDay:</td><td>$select_day</td></tr>";
			$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_ClassSelection:</td>";
			if(sizeof($result)>0)
			{
				$table_content .= "<td>";
				for($i=0; $i<sizeof($result); $i++)
				{
					list($class_id, $class_name, $class_level) = $result[$i];
					
					if($temp_class_level != "")
						if($temp_class_level != $class_level)
							$table_content .= "<br>";
							
					$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
					
					$temp_class_level = $class_level; 
				}
			}
			$table_content .= "</td></tr>";
		}
		if($selected_type == 2)
		{
			$sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS";	
			$temp = $lc->returnVector($sql);
			$select_day = getSelectByValue($temp,"name=DayValue");
			$table_content .= "<tr><td align=right>$i_StudentAttendance_CycleDay:</td><td>$select_day</td></tr>";
			$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_ClassSelection:</td>";
			if(sizeof($result)>0)
			{
				$table_content .= "<td>";
				for($i=0; $i<sizeof($result); $i++)
				{
					list($class_id, $class_name, $class_level) = $result[$i];
					
					if($temp_class_level != "")
						if($temp_class_level != $class_level)
							$table_content .= "<br>";
							
					$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
					
					$temp_class_level = $class_level; 
				}
			}
			$table_content .= "</td></tr>";
		}
		if($selected_type == 3)
		{
			$select_day = "<input type=text name=TargetDate value='".date('Y-m-d')."' size=10>\n";
			$select_day .= "<script language=\"JavaScript\" type=\"text/javascript\">\n";
			$select_day .= "<!--\n";
		    $select_day .= "startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');\n";
		    $select_day .= "startCal.setLegend('$legend');\n";
		    $select_day .= "startCal.differentDisplay = true;\n";
		    $select_day .= "//-->\n";
			$select_day .= "</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>\n";
			$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSlot_SpecialDay:</td><td>$select_day</td></tr>";
			$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_ClassSelection:</td>";
			if(sizeof($result)>0)
			{
				$table_content .= "<td>";
				for($i=0; $i<sizeof($result); $i++)
				{
					list($class_id, $class_name, $class_level) = $result[$i];
					
					if($temp_class_level != "")
						if($temp_class_level != $class_level)
							$table_content .= "<br>";
							
					$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
					
					$temp_class_level = $class_level; 
				}
			}
			$table_content .= "</td></tr>";
		}
	}
	*/
}
?>

<SCRIPT language="JavaScript" src='/templates/tooltip.js'></SCRIPT>
<SCRIPT Language="JavaScript">
isMenu = true;
function checkDisable()
{
	var mode = <?=$mode;?>;
	if(mode == 3)
	{
		if(document.form1.non_school_day.checked)
		{
			document.form1.am_hours.disabled = true;
			document.form1.am_minutes.disabled = true;
			document.form1.lunch_hours.disabled = true;
			document.form1.lunch_minutes.disabled = true;
			document.form1.pm_hours.disabled = true;
			document.form1.pm_minutes.disabled = true;
			document.form1.leave_hours.disabled = true;
			document.form1.leave_minutes.disabled = true;
		}
		else
		{
			document.form1.am_hours.disabled = false;
			document.form1.am_minutes.disabled = false;
			document.form1.lunch_hours.disabled = false;
			document.form1.lunch_minutes.disabled = false;
			document.form1.pm_hours.disabled = false;
			document.form1.pm_minutes.disabled = false;
			document.form1.leave_hours.disabled = false;
			document.form1.leave_minutes.disabled = false;
		}	
	}
	if(mode == 2)
	{
		if(document.form1.non_school_day.checked)
		{
			document.form1.pm_hours.disabled = true;
			document.form1.pm_minutes.disabled = true;
			document.form1.leave_hours.disabled = true;
			document.form1.leave_minutes.disabled = true;
		}
		else
		{
			document.form1.pm_hours.disabled = false;
			document.form1.pm_minutes.disabled = false;
			document.form1.leave_hours.disabled = false;
			document.form1.leave_minutes.disabled = false;
		}	
	}
	if(mode == 1)
	{
		if(document.form1.non_school_day.checked)
		{
			document.form1.am_hours.disabled = true;
			document.form1.am_minutes.disabled = true;
			document.form1.leave_hours.disabled = true;
			document.form1.leave_minutes.disabled = true;
		}
		else
		{
			document.form1.am_hours.disabled = false;
			document.form1.am_minutes.disabled = false;
			document.form1.leave_hours.disabled = false;
			document.form1.leave_minutes.disabled = false;
		}
	}
}

function checkForm()
{
	var obj = document.form1;
	var cnt = 0;
	var mode = <?=$mode;?>
	
	if(mode == 3)
	{
		var temp_a1 = document.form1.am_hours.value;
		var temp_a2 = document.form1.am_minutes.value;
		var temp_b1 = document.form1.lunch_hours.value;
		var temp_b2 = document.form1.lunch_minutes.value;
		var temp_c1 = document.form1.pm_hours.value;
		var temp_c2 = document.form1.pm_minutes.value;
		var temp_d1 = document.form1.leave_hours.value;
		var temp_d2 = document.form1.leave_minutes.value;
		var temp_flag = document.form1.flag.value;
		
		if(obj.s_name.value == '')
		{
			alert ("<?=$i_StudentAttendance_TimeSessionSettings_SessionName_Warning?>");
			return false;
		}
		else
		{
			if(!document.form1.non_school_day.checked)
			{
				if((temp_a1<temp_b1) || ((temp_a1==temp_b1) && (temp_a2<temp_b2)))
				{
					if((temp_b1<temp_c1) || ((temp_b1==temp_c1) && (temp_b2<temp_c2)))
					{
						if((temp_c1<temp_d1) || ((temp_c1==temp_d1) && (temp_c2<temp_d2)))
						{
							cnt=1;
						}
						else
						{
							alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
							return false;
						}
					}
					else
					{
						alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
						return false;
					}
				}
				else
				{
					alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
					return false;
				}
			}
			else
			{
				cnt=1;
			}
		}
	}
	
	if(mode == 2)
	{
		var temp_c1 = document.form1.pm_hours.value;
		var temp_c2 = document.form1.pm_minutes.value;
		var temp_d1 = document.form1.leave_hours.value;
		var temp_d2 = document.form1.leave_minutes.value;
		var temp_flag = document.form1.flag.value;
		
		if(obj.s_name.value == '')
		{
			alert ("<?=$i_StudentAttendance_TimeSessionSettings_SessionName_Warning?>");
			return false;
		}
		else
		{
			if(!document.form1.non_school_day.checked)
			{
				if((temp_c1<temp_d1) || ((temp_c1==temp_d1) && (temp_c2<temp_d2)))
				{
					cnt=1;
				}
				else
				{
					alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
					return false;
				}
			}
			else
			{
				cnt=1;
			}
		}
	}
		
	if(mode == 1)
	{
		var temp_a1 = document.form1.am_hours.value;
		var temp_a2 = document.form1.am_minutes.value;
		var temp_d1 = document.form1.leave_hours.value;
		var temp_d2 = document.form1.leave_minutes.value;
		var temp_flag = document.form1.flag.value;
		
		if(obj.s_name.value == '')
		{
			alert ("<?=$i_StudentAttendance_TimeSessionSettings_SessionName_Warning?>");
			return false;
		}
		else
		{
			if(!document.form1.non_school_day.checked)
			{
				if((temp_a1<temp_d1) || ((temp_a1==temp_d1) && (temp_a2<temp_d2)))
				{
					cnt=1;
				}
				else
				{
					alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
					return false;
				}
			}
			else
			{
				cnt=1;
			}
		}
	}
	
	if(cnt==1)
	{
		document.form1.action = "edit_update.php";
		return true;
	}
}
</SCRIPT>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_card_no_data";
	css_array[1] = "dynCalendar_card_not_confirmed";
	css_array[2] = "dynCalendar_card_confirmed";
	var date_array = new Array;
	<?
     for ($i=0; $i<sizeof($records_with_data); $i++)
     {
          $date_string = $records_with_data[$i];
          
          //$isConfirmed = $confirmed_dates[$date_string];
          ?>
          date_array[<?=$date_string?>] = <?=($isConfirmed?2:1)?>;
          <?
     }
     ?>
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
	          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].TargetDate.value = dateValue;
          }
</script>
<SCRIPT Language="JavaScript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "Specialday_Info", o.responseText );
        }
}
// start AJAX
function retrieveSpecialdayInfo(DayType, ClassID)
{
        //FormObject.testing.value = 1;
        obj = document.form1;
        var myElement = document.getElementById("Specialday_Info");
        
        showMenu("Specialday_Info","<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td>Loading</td></tr></table>");
        YAHOO.util.Connect.setForm(obj);

        // page for processing and feedback
        var path = "getSpecialdayInfo.php?type="+DayType+"&c_id="+ClassID;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
-->
</SCRIPT>

<style type="text/css">
     #Specialday_Info{position:absolute; top:0px; left:0px; z-index:5; visibility:show; width:450px;}
</style>

<div id="Specialday_Info" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Session_Setting,'index.php', $button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method=POST onSubmit="return checkForm()";>
<table border=0 width=560 align=center>
<?=$table_content?>
</table>

<?
$normal_info = "";
$normal_info .= "<tr><td>";

### Get The Classes Which Are Using Session As Normal
$sql = "SELECT ClassID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE SessionID = $s_id AND DayType = 0 ORDER BY ClassID";
$result_normal = $lc->returnVector($sql);
if(sizeof($result_normal)>0)
{
	for($i=0; $i<sizeof($result_normal); $i++)
	{
		//list ($class_id) = $result_normal[$i];
		if($result_normal[$i] == 0)
			$normal_info .= $i_general_WholeSchool."&nbsp;&nbsp;&nbsp;";
		else
			$normal_info .= $lc->getClassName($result_normal[$i])."&nbsp;&nbsp;&nbsp;";
	}
}
if(sizeof($result_normal)==0)
{
	$normal_info .= $i_no_record_exists_msg."&nbsp;&nbsp;&nbsp;";
}
$normal_info .= "</td></tr>";
	
$weekly_info = "";
$weekly_info .= "<tr><td>";

### Get The Classes Which Are Using Session As Weekly
$sql = "SELECT DISTINCT ClassID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE SessionID = $s_id AND DayType = 1 ORDER BY ClassID";
$result_weekly = $lc->returnVector($sql);
if(sizeof($result_weekly)>0)
{
	for($i=0; $i<sizeof($result_weekly); $i++)
	{
		//list ($class_id) = $result_weekly[$i];
		if($result_weekly[$i] == 0)
			$weekly_info .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo(1, $result_weekly[$i])'>".$i_general_WholeSchool."</a>&nbsp;&nbsp;&nbsp;";
		else
			$weekly_info .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo(1, $result_weekly[$i])'>".$lc->getClassName($result_weekly[$i])."</a>&nbsp;&nbsp;&nbsp;";
	}
}
if(sizeof($result_weekly)==0)
{
	$weekly_info .= $i_no_record_exists_msg."&nbsp;&nbsp;&nbsp;";
}
$weekly_info .= "</td></tr>";

$cycle_info = "";
$cycle_info .= "<tr><td>";

### Get The Classes Which Are Using Session As Cycle
$sql = "SELECT DISTINCT ClassID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE SessionID = $s_id AND DayType = 2 ORDER BY ClassID";
$result_cycle = $lc->returnVector($sql);
if(sizeof($result_cycle)>0)
{
	for($i=0; $i<sizeof($result_cycle); $i++)
	{
		//list ($class_id) = $result_cycle[$i];
		if($result_cycle[$i] == 0)
			$cycle_info .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo(2, $result_cycle[$i])'>".
			general_WholeSchool."</a>&nbsp;&nbsp;&nbsp;";
		else
			$cycle_info .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo(2, $result_cycle[$i])'>".$lc->getClassName($result_cycle[$i])."</a>&nbsp;&nbsp;&nbsp;";
	}
}
if(sizeof($result_cycle)==0)
{
	$cycle_info .= $i_no_record_exists_msg."&nbsp;&nbsp;&nbsp;";
}
$cycle_info .= "</td></tr>";

$special_info = "";
$special_info .= "<tr><td>";

### Get The Classes Which Are Using Session As Special
$sql = "SELECT DISTINCT ClassID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE SessionID = $s_id ORDER BY ClassID";
$result_special = $lc->returnVector($sql);
if(sizeof($result_special)>0)
{
	for($i=0; $i<sizeof($result_special); $i++)
	{
		//list($class_id) = $result_special[$i];
		if($result_special[$i] == 0)
			$special_info .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo(3, $result_special[$i])'>".$i_general_WholeSchool."</a>&nbsp;&nbsp;&nbsp;";
		else
			$special_info .= "<a onMouseMove='overhere()' href='javascript:retrieveSpecialdayInfo(3, $result_special[$i])'>".$lc->getClassName($result_special[$i])."</a>&nbsp;&nbsp;&nbsp;";
	}
}
if(sizeof($result_special)==0)
{
	$special_info .= $i_no_record_exists_msg."&nbsp;&nbsp;&nbsp;";
}
$special_info .= "</td></tr>";

$info_table .= "<br>";
$info_table .= "<table border=1 width=360 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>";
$info_table .= "<tr class=tableTitle><td>$i_StudentAttendance_TimeSession_UsingAsNormalSetting</td></tr>";
$info_table .= $normal_info;
$info_table .= "<tr height=10px><td></td></tr>";
$info_table .= "</table>";
$info_table .= "<table border=1 width=360 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>";
$info_table .= "<tr class=tableTitle><td>$i_StudentAttendance_TimeSession_UsingAsWeeklySetting</td></tr>";
$info_table .= $weekly_info;
$info_table .= "<tr height=10px><td></td></tr>";
$info_table .= "<table>";
$info_table .= "<table border=1 width=360 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>";
$info_table .= "<tr class=tableTitle><td>$i_StudentAttendance_TimeSession_UsingAsCycleSetting</td></tr>";
$info_table .= $cycle_info;
$info_table .= "<tr height=10px><td></td></tr>";
$info_table .= "</table>";
$info_table .= "<table border=1 width=360 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>";
$info_table .= "<tr class=tableTitle><td>$i_StudentAttendance_TimeSession_UsingAsSpecialSetting</td></tr>";
$info_table .= $special_info;
$info_table .= "</table>";
?>

<?=$info_table?>

<table width=560 align=center>
<tr><td height=10></td></tr>
<tr><td align=center><?=$i_StudentAttendance_Slot_SettingsDescription?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr><td align=right><?= btnSubmit() ." ". btnReset() ?><a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
<input type=hidden name=flag value=0>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>