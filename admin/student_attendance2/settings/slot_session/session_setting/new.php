<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
//$lc->attendance_mode;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

$no_of_fields = $hasAM && $hasPM? 6:4;
//	$no_of_fields = 6;
$hours = array();
$minutes = array();

for($i=0; $i<24; $i++)
{
	if($i<10)
		$i='0'.$i;
	array_push($hours, $i);
}

for($j=0; $j<60; $j++)
{
	if($j<10)
		$j='0'.$j;
	array_push($minutes, $j);
}
?>

<SCRIPT Language='JavaScript'>
var no_of_time_session = <? echo $no_of_fields==""?6:$no_of_fields;?>;
var no_of_element = 1;

function add_field(){
	var table = document.getElementById("time_session");
	var row = table.insertRow(no_of_time_session);
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		x = '<tr><td></td>';
		y = '<td></td></tr>';
		cell1.height = 20;
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		x = '<tr><td align=right><?=$i_StudentAttendance_Menu_Slot_Session_Name?>:</td>';
		y = '<td><input type=text name="s_name_'+no_of_element+'"></td></tr>';
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	<?
	if($hasAM)
	{
	?>
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		x = '<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?>:</td>';
		y = '<td><SELECT name="am_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			y += '<OPTION value='+i+'>'+i+'</OPTION>';
		}
		y += '</SELECT>';
		y += ':';
		y += '<SELECT name="am_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			y += '<OPTION value='+j+'>'+j+'</OPTION>';
		}
		y += '</SELECT>';
		
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	<?
	}
	if($hasLunch)
	{
	?>
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		x = '<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?>:</td>';
		y = '<td><SELECT name="lunch_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			y += '<OPTION value='+i+'>'+i+'</OPTION>';
		}
		y += '</SELECT>';
		y += ':';
		y += '<SELECT name="lunch_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			y += '<OPTION value='+j+'>'+j+'</OPTION>';
		}
		y += '</SELECT>';
		
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	<?
	}
	if($hasPM)
	{
	?>
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		x = '<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?>:</td>';
		y = '<td><SELECT name="pm_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			y += '<OPTION value='+i+'>'+i+'</OPTION>';
		}
		y += '</SELECT>';
		y += ':';
		y += '<SELECT name="pm_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			y += '<OPTION value='+j+'>'+j+'</OPTION>';
		}
		y += '</SELECT>';
		
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	<?
	}
	?>
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		
		x = '<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?>:</td>';
		y = '<td><SELECT name="leave_hours_'+no_of_element+'">';
		for(i=0;i<24;i++)
		{
			if(i<10)
				i='0'+i;
			y += '<OPTION value='+i+'>'+i+'</OPTION>';
		}
		y += '</SELECT>';
		y += ':';
		y += '<SELECT name="leave_minutes_'+no_of_element+'">';
		for(j=0;j<60;j++)
		{
			if(j<10)
				j='0'+j;
			y += '<OPTION value='+j+'>'+j+'</OPTION>';
		}
		y += '</SELECT>';
		
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	if (document.all)
	{
		var row = table.insertRow(no_of_time_session);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);

		x = '<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?>:</td>';
		y = '<td><input type=checkbox onclick="checkDisable('+no_of_element+')" name="non_school_day_'+no_of_element+'" onClick="this.checked? this.value=1 : this.value=0;"></td></tr>';
		cell1.align = "right";
		cell1.innerHTML = x;
		cell2.innerHTML = y;
		
		no_of_time_session++;
	}
	no_of_element++;
	document.form1.no_of_element.value = no_of_element;
}

function checkForm()
{
	var cnt = 0;
	var checkTimeFormat = 0;
	
	for(i=0; i<no_of_element; i++)
	{
		var temp_a1 = eval("document.form1.am_hours_"+i+".value");
		var temp_a2 = eval("document.form1.am_minutes_"+i+".value");
		var temp_b1 = eval("document.form1.lunch_hours_"+i+".value");
		var temp_b2 = eval("document.form1.lunch_minutes_"+i+".value");
		var temp_c1 = eval("document.form1.pm_hours_"+i+".value");
		var temp_c2 = eval("document.form1.pm_minutes_"+i+".value");
		var temp_d1 = eval("document.form1.leave_hours_"+i+".value");
		var temp_d2 = eval("document.form1.leave_minutes_"+i+".value");
		
		if(eval("document.form1.s_name_"+i+".value") == "")
		{
			alert ("<?=$i_StudentAttendance_TimeSessionSettings_SessionName_Warning?>");
			return false;
		}
		else
		{
			if((temp_a1<temp_b1) || ((temp_a1==temp_b1) && (temp_a2<temp_b2)))
			{
				if((temp_b1<temp_c1) || ((temp_b1==temp_c1) && (temp_b2<temp_c2)))
				{
					if((temp_c1<temp_d1) || ((temp_c1==temp_d1) && (temp_c2<temp_d2)))
					{
						cnt++;
					}
					else
					{
						alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
						return false;
					}
				}
				else
				{
					alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
					return false;
				}
			}
			else
			{
				alert("<?=$i_StudentAttendance_Input_Correct_Time?>");
				return false;
			}
		}
		
	}
	if((cnt==no_of_element))
	{
		return true;
	}
}

function checkDisable(eid)
{
	var non_school_day = eval ('document.getElementById("non_school_day_'+eid+'")');
	var am_hours = eval ('document.getElementById("am_hours_'+eid+'")');
	var am_minutes = eval ('document.getElementById("am_minutes_'+eid+'")');
	var lunch_hours = eval ('document.getElementById("lunch_hours_'+eid+'")');
	var lunch_minutes = eval ('document.getElementById("lunch_minutes_'+eid+'")');
	var pm_hours = eval ('document.getElementById("pm_hours_'+eid+'")');
	var pm_minutes = eval ('document.getElementById("pm_minutes_'+eid+'")');
	var leave_hours = eval ('document.getElementById("leave_hours_'+eid+'")');
	var leave_minutes = eval ('document.getElementById("leave_minutes_'+eid+'")');
	
	if(non_school_day.checked)
	{
		am_hours.disabled = true;
		am_minutes.disabled = true;
		lunch_hours.disabled = true;
		lunch_minutes.disabled = true;
		pm_hours.disabled = true;
		pm_minutes.disabled = true;
		leave_hours.disabled = true;
		leave_minutes.disabled = true;
	}
	else
	{
		am_hours.disabled = false;
		am_minutes.disabled = false;
		lunch_hours.disabled = false;
		lunch_minutes.disabled = false;
		pm_hours.disabled = false;
		pm_minutes.disabled = false;
		leave_hours.disabled = false;
		leave_minutes.disabled = false;	
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Session_Setting,'index.php', $button_new,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action='new_update.php' method=POST onSubmit="return checkForm()">
<table id=time_session width=560 border=0 align=center>
<tr><td align=right><?=$i_StudentAttendance_Menu_Slot_Session_Name?>:</td><td><input type=text name="s_name_0"></td></tr>
<?
if($hasAM)
{
?>
<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?>:</td><td><?echo getSelectByValue($hours,'name=am_hours_0',0,0,1)?>:<?echo getSelectByValue($minutes,'name=am_minutes_0',0,0,1)?></td></tr>
<?
}
if($hasLunch)
{
?>
<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?>:</td><td><?echo getSelectByValue($hours,'name=lunch_hours_0',0,0,1)?>:<?echo getSelectByValue($minutes,'name=lunch_minutes_0',0,0,1)?></td></tr>
<?
}
if($hasPM)
{
?>
<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?>:</td><td><?echo getSelectByValue($hours,'name=pm_hours_0',0,0,1)?>:<?echo getSelectByValue($minutes,'name=pm_minutes_0',0,0,1)?></td></tr>
<?
}
?>
<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?>:</td><td><?echo getSelectByValue($hours,'name=leave_hours_0',0,0,1)?>:<?echo getSelectByValue($minutes,'name=leave_minutes_0',0,0,1)?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?>:</td><td><input type=checkbox onClick='checkDisable(0)' name="non_school_day_0" onClick='this.checked? this.value=1 : this.value=0' ></td></tr>
</table>

<table width=560 border=0 align=center>
<tr><td align=right><input type=button value=" + " onClick="add_field()"></td></tr>
<tr><td align=center><?=$i_StudentAttendance_Slot_SettingsDescription?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 align=center>
<tr><td height=10></td></tr>
<tr><td align=right><?= btnSubmit() ." ". btnReset() ?><a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
<input type=hidden name=no_of_element value=1>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>