<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();
$lclass = new libclass();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

//echo $SessionID;
/*
if($SetOtherClasses != '')
{
	if($SetOtherClasses == 0)
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
	else
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
}
else
{
	$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_OtherClassUseSameSession:</td><td><input type=radio name=SetOtherClasses value=0 checked onClick=\"document.form1.flag.value=1; this.form.submit();\">No <input type=radio name=SetOtherClasses value=1 onClick=\"document.form1.flag.value=1; this.form.submit();\">Yes </td></tr>";
}

if($SetOtherClasses == 1)
{
*/
	$sql = "SELECT NonSchoolDay FROM CARD_STUDENT_TIME_SESSION WHERE SessionID = $SessionID";
	$check_non_school_day = $lclass->returnVector($sql);

	$temp_arr = array(array(0,"$i_StudentAttendance_NormalDays"),array(1,"$i_StudentAttendance_Weekday_Specific"),array(2,"$i_StudentAttendance_Cycleday_Specific"),array(3,"$i_StudentAttendance_SpecialDay"));
	$result = $lclass->getClassList();
	$select_type = getSelectByArray($temp_arr, "name=selected_type onChange=\"document.form1.flag.value=1; this.form.submit();\"", $selected_type,0,1);

	$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_TypeSelection:</td>";
	$table_content .= "<td>$select_type</td></tr>";
	if(($selected_type == "") || ($selected_type == 0))
	{
		$table_content .= "<tr><td align=right>$i_ClassName:</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td>";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
	if($selected_type == 1)
	{
		$select_day = "<SELECT name=DayValue>\n";
	    $select_day .= "<OPTION value=''>-- $button_select --</OPTION>";
	    for ($i=0; $i<7; $i++)
	    {
	         $word = $i_DayType0[$i];
	         $select_day .= "<OPTION value='".$i."'>".$word."</OPTION>\n";
	    }
	    $select_day .= "</SELECT>\n";
		$table_content .= "<tr><td align=right>$i_StudentAttendance_WeekDay:</td><td>$select_day</td></tr>";
		$table_content .= "<tr><td align=right>$i_ClassName:</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td>";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
	if($selected_type == 2)
	{
		$sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS";	
		$temp = $lc->returnVector($sql);
		$select_day = getSelectByValue($temp,"name=DayValue");
		$table_content .= "<tr><td align=right>$i_StudentAttendance_CycleDay:</td><td>$select_day</td></tr>";
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_ClassSelection:</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td>";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
	if($selected_type == 3)
	{
		$select_day = "<input type=text name=TargetDate value='".date('Y-m-d')."' size=10>\n";
		$select_day .= "<script language=\"JavaScript\" type=\"text/javascript\">\n";
		$select_day .= "<!--\n";
	    $select_day .= "startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');\n";
	    $select_day .= "startCal.setLegend('$legend');\n";
	    $select_day .= "startCal.differentDisplay = true;\n";
	    $select_day .= "//-->\n";
		$select_day .= "</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>\n";
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSlot_SpecialDay:</td><td>$select_day</td></tr>";
		$table_content .= "<tr><td align=right>$i_StudentAttendance_TimeSession_ClassSelection:</td>";
		if(sizeof($result)>0)
		{
			$table_content .= "<td>";
			for($i=0; $i<sizeof($result); $i++)
			{
				list($class_id, $class_name, $class_level) = $result[$i];
				
				if($temp_class_level != "")
					if($temp_class_level != $class_level)
						$table_content .= "<br>";
						
				$table_content .= "<input type=checkbox name=class[] value=$class_id>$class_name&nbsp;&nbsp;&nbsp;";
				
				$temp_class_level = $class_level; 
			}
		}
		$table_content .= "</td></tr>";
	}
//}

?>

<SCRIPT Language="JavaScript">
function checkForm()
{
	var obj = document.form1;
	var temp = document.form1['class[]'];
	var checking = 0;
	
	if(obj.selected_type.value != "")
	{
		if(<?=$check_non_school_day[0]?> == 0)
		{
			if(obj.selected_type.value == 0)
			{
				for(i=0; i<temp.length; i++)
				{
					if(temp[i].checked)
						checking = 1;
				}
			}
		}
		else
		{
			alert("<?=$i_StudentAttendance_NonSchoolDaySetting_Warning?>");
			return false;
		}
			
		
		if((obj.selected_type.value == 1)||(obj.selected_type.value == 2))
		{
			if(obj.DayValue.value != "")
			{
				for(i=0; i<temp.length; i++)
				{
					if(temp[i].checked)
						checking = 1;
				}
			}
			else
			{
				if(obj.selected_type.value == 1)
				{
					alert("<?=$i_StudentAttendance_Warn_Please_Select_Weekday?>");
					return false;
				}
				if(obj.selected_type.value == 2)
				{
					alert("<?=$i_StudentAttendance_Warn_Please_Select_CycleDay?>");
					return false;
				}
			}
		}
		if(obj.selected_type.value == 3)
		{
			if(obj.TargetDate != "")
			{
				for(i=0; i<temp.length; i++)
				{
					if(temp[i].checked)
						checking = 1;
				}
			}
			else
			{
				alert("<?=$i_StudentAttendance_Warn_Please_Select_A_Day?>");
				return false;
			}
		}
		if(checking == 1)
		{
			obj.action = "multi_class_update.php?SessionID=<?=$SessionID?>";
			obj.submit();
		}
		else
		{
			alert("<?=$i_StudentAttendance_Class_Select_Instruction?>");
			return false;
		}
	}
	else
	{
		alert("<?=$i_StudentAttendance_TimeSession_TypeSelectionWarning?>");
		return false;
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../',$i_StudentAttendance_Menu_Slot_Session_Setting,'index.php', $button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method=POST onSubmit="return checkForm()";>
<table border=0 width=560 align=center>
<?=$table_content?>
</table>

<table width=560 align=center>
<tr><td height=10></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<tr><td align=right><?= btnSubmit() ." ". btnReset() ?><a href="edit.php?s_id=<?=$SessionID?>"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
<input type=hidden name=flag value=0>
</form>