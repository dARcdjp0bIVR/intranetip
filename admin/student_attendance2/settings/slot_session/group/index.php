<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
//include_once("../../../../../includes/libspecialgroup.php");
//include_once("../../../../../includes/libcardstudentattendgroup.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

//$lc = new libcardstudentattendgroup();
$lc = new libcardstudentattend2();

$_recordTypeID = 3;  #3 ==> class type)
//$lc->setSpecialGroupRecordType(3);
$classList = $lc->getGroupListMode($_recordTypeID);

$showAll = ($showAll =="") ? 0 : $showAll;
if($showAll == "1")
{
	$_SESSION['groupShowAll'] = "1";   
}
else if($showAll == "0")
{
	unset($_SESSION['groupShowAll']);
}

$table_content = " ";
$table_content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$table_content .= "<tr class=tableTitle><td>$i_GroupName</td><td>$i_StudentAttendance_GroupMode</td></tr>";
if(sizeof($classList)>0)
{
	for($i=0; $i<sizeof($classList); $i++)
	{
		list($class_id, $class_name, $mode) = $classList[$i];
		if ($mode == 2)
		{
			if($_SESSION['groupShowAll'] == "")
			{
			 //DEFAULT DO NOT SHOW "NO NEED TO TAKE ATTENDANCE GROUP"
			 continue; 
			}
			$word_mode = $i_StudentAttendance_GroupMode_NoNeedToTakeAttendance;			
		}
		else if ($mode == 1)
		{
			$word_mode = $i_StudentAttendance_GroupMode_UseGroupTimetable;
		}
		else
		{
			$word_mode = $i_StudentAttendance_GroupMode_UseSchoolTimetable;
			 if($_SESSION['groupShowAll'] == "")
			 {
				 //DEFAULT DO NOT SHOW USE SCHOOL TIME TABLE AS DEFAULT
				 continue; 
			 }
		}
		$css = ($i%2? "":"2");
     	$editlink = "<a class=functionlink href=group.php?ClassID=$class_id><img src=\"$image_path/icon_edit.gif\" border=0></a>";
     	$table_content .= "<tr class=tableContent$css>\n";
     	$table_content .= "<td>$class_name $editlink</td><td>$word_mode</td>";
     	$table_content .= "</tr>\n";
	}
}
$table_content .= "</table>";
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSessionSettings,'../',$i_StudentAttendance_Menu_Slot_Group,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<script>
function submitForm()
{
	//var selectValue = document.form1.showAll.options[document.form1.sltType.selectedIndex].value;
	 document.form1.submit();
}
</script>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td align="right">
<form name="form1" action="index.php" method="post">
<?
	$type_1_selected = ($showAll == 1)? $type_1_selected = "selected" : "";
	$type_0_selected = ($showAll == 0)? $type_0_selected = "selected" : "";
?>
<?=$i_StudentAttendance_Diplay_Mode?>&nbsp;:
<select onchange="submitForm()" name="showAll">
<option value="1" <?=$type_1_selected?>><?=$i_StudentAttendance_ShowALlGroup_Options?></option>
<option value="0" <?=$type_0_selected?>><?=$i_StudentAttendance_HiddenGroup_Options?></option>
</select>
</form>
</td>
</tr>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu></td></tr>
  <tr>
    <td><?=$table_content?></td>
  </tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<br><br>

<?
include_once("../../../../../templates/adminfooter.php");
intranet_closedb();
?>
