<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");

$li = new libdb();
$lc = new libcardstudentattend2();
$lc->retrieveSettings();

$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

intranet_opendb();

$sql = "SELECT 
				SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',Time_Format(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay=1, 'Y', 'N')
		FROM
				CARD_STUDENT_TIME_SESSION
		WHERE
				SessionID = $session_id
		";
		
$result = $li->returnArray($sql);

$layer_content = "<table border=1 width=450 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
$layer_content .= "<tr class=admin_bg_menu><td colspan=8 align=right><input type=button value=' X ' onClick=hideMenu('Specialday_Info')></td></tr>";
$layer_content .= "<tr class=tableTitle><td width=1>#</td><td>Session Name</td>";
if($hasAM)
	$layer_content .= "<td>$i_StudentAttendance_SetTime_AMStart</td>";
if($hasLunch)
	$layer_content .= "<td>$i_StudentAttendance_SetTime_LunchStart</td>";
if($hasPM)
	$layer_content .= "<td>$i_StudentAttendance_SetTime_PMStart</td>";
$layer_content .= "<td>$i_StudentAttendance_SetTime_SchoolEnd</td><td>$i_StudentAttendance_NonSchoolDay</td></tr>";

if (sizeof($result)==0)
	$layer_content .= "<tr><td colspan=8 align=center>$i_no_record_exists_msg</td></tr>";
else
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($s_name, $am_time, $lunch_time, $pm_time, $leave_time, $non_school_day) = $result[$i];
		$no = $i+1;
		$layer_content .= "<tr><td>$no</td><td>$s_name</td>";
		if($hasAM)
			$layer_content .= "<td>$am_time</td>";
		if($hasLunch)
			$layer_content .= "<td>$lunch_time</td>";
		if($hasPM)
			$layer_content .= "<td>$pm_time</td>";
		$layer_content .= "<td>$leave_time</td><td>$non_school_day</td></tr>";
	}
}

//$layer_content .= "<tr><td colspan=5><font color=red>* - $i_StudentGuardian_MainContent</font></td></tr>";
$layer_content .= "<tr><td colspan=8 align=center><font color=red>$i_StudentAttendance_TimeSession_GetSpecialdayInfo_Warning</font></td></tr>";
$layer_content .= "</table>";

$response = iconv("Big5","UTF-8",$layer_content);
echo $response;

intranet_closedb();
?>