<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
//include_once("../../../../../includes/libspecialgroup.php");
//include_once("../../../../../includes/libcardstudentattendgroup.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");

intranet_opendb();
$lclass = new libclass();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
//$lc->attendance_mode;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);

//$objSpecialGroup = new libcardstudentattendgroup();
//$classname = $objSpecialGroup->getSpecialGroupName($ClassID);
$classname = $lc->getSpecialGroupName($ClassID);

$normal_warning = 0;
$weekly_warning = 0;
$cycle_warning = 0;
$special_warning = 0;

### Weekday Table ###
### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";

$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

//$result = $lc->getSessionTimeArrayList(1,$ClassID);
//$result = $objSpecialGroup->getSessionTimeArrayList(1,$ClassID);
$result = $lc->getGroupSessionTimeArrayList(1,$ClassID);

$weekday_table_content = "";    
$weekday_table_content .= "<tr><td class=tableTitle>$i_BookingType_Weekdays</td>";
if($hasAM)
	$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($s_id, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_value) = $result[$i];
		$weekday = $i_DayType0[$day_value];
		$editlink = "<a class=\"functionlink\" href=\"group_session_edit.php?type=1&ClassID=$ClassID&value=$day_value\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(1,$ClassID,'$day_value')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$weekday_table_content .= "<tr bgcolor=red><td>$weekday $editlink</td>";
			$weekly_warning = 1;
		}
		else
		{
			$weekday_table_content .= "<tr><td>$weekday $editlink</td>";
		}
		if($non_school_day==1){
			$weekday_table_content .= "<td colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
		}
		else
		{
			if($hasAM)
				$weekday_table_content .= "<td>$am_time</td>";
			if($hasLunch)
				$weekday_table_content .= "<td>$lunch_time</td>";
			if($hasPM)
				$weekday_table_content .= "<td>$pm_time</td>";
			$weekday_table_content .= "<td>$leave_school_time</td>";		
		}	
	}
}
if(sizeof($result)==0)
{
	$weekday_table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Weekday Table ###

### Cycle Day Table ###
### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";

$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

//$result = $lc->getSessionTimeArrayList(2,$ClassID);
//$result = $objSpecialGroup->getSessionTimeArrayList(2,$ClassID);
$result = $lc->getGroupSessionTimeArrayList(2,$ClassID);

$cycle_table_content = "";    
$cycle_table_content .= "<tr><td class=tableTitle>$i_BookingType_Cycleday</td>";
if($hasAM)
	$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($s_id, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_value) = $result[$i];
		$cycleday = $day_value;
		$editlink = "<a class=functionlink href=\"group_session_edit.php?type=2&ClassID=$ClassID&value=$cycleday\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(2,$ClassID,'$day_value')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$cycle_table_content .= "<tr bgcolor=red><td>$cycleday $editlink</td>";
			$cycle_warning = 1;
		}
		else
		{
			$cycle_table_content .= "<tr><td>$cycleday $editlink</td>";
		}
		if($non_school_day==1){
                 $cycle_table_content .="<td colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
        }
        else
        {
			//$cycle_table_content .= "<tr><td>$cycleday $editlink</td>";
			if($hasAM)
				$cycle_table_content .= "<td>$am_time</td>";
			if($hasLunch)
				$cycle_table_content .= "<td>$lunch_time</td>";
			if($hasPM)
				$cycle_table_content .= "<td>$pm_time</td>";
			$cycle_table_content .= "<td>$leave_school_time</td></tr>";
		}
	}
}
if(sizeof($result)==0)
{
	$cycle_table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Cycle Day Table ###

### Special Day Table ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";

$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

$sql = "SELECT 
				a.RecordID, b.SessionID, b.SessionName, a.RecordDate ,TIME_FORMAT(b.MorningTime,'%H:%i'), TIME_FORMAT(b.LunchStart,'%H:%i'), TIME_FORMAT(b.LunchEnd,'%H:%i'), TIME_FORMAT(b.LeaveSchoolTime,'%H:%i'), b.NonSchoolDay
		FROM 
				CARD_STUDENT_TIME_SESSION_DATE_GROUP AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
		WHERE
				GROUPID = $ClassID
		ORDER BY	
				RecordDate DESC
		";

$temp = $lclass->returnArray($sql,9);

$special_day_table = " ";
$special_day_table = "<tr><td class=tabletitle>$i_StudentAttendance_Slot_Special</td>";
if($hasAM)
	$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($temp)>0)
{	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($record_id, $s_id, $s_name, $record_date, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day) = $temp[$i];
		$special_date = $record_date;
		$editlink = "<a class=functionlink href=\"group_session_edit.php?type=3&ClassID=$ClassID&value=$special_date\"><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeSpecialDaySetting($ClassID,'$special_date')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$special_day_table .= "<tr bgcolor=red><td>$record_date $editlink</td>";
			$special_warning = 1;
		}
		else
		{
			$special_day_table .= "<tr><td>$record_date $editlink</td>";
		}
		//$special_day_table .= "<tr><td>$record_date $editlink</td>";
		if($non_school_day==1){
			$special_day_table .= "<td colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
		}
		else
		{
			if($hasAM)
				$special_day_table .= "<td>$am_time</td>";
			if($hasLunch)
				$special_day_table .= "<td>$lunch_time</td>";
			if($hasPM)
				$special_day_table .= "<td>$pm_time</td>";
			$special_day_table .= "<td>$leave_school_time</td>";
		}
	}
}
if(sizeof($temp)==0)
{
	$special_day_table .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Special Day Table ###

### Start Of Normal Table ###
### Get The Current Using Session ###
$sql = "SELECT
				SessionID
		FROM
				CARD_STUDENT_TIME_SESSION_REGULAR_GROUP
		WHERE
				GROUPID = $ClassID AND DayType = 0 AND DayValue = 0
		ORDER BY
				SessionID";
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	list($using_session) = $result[0];
}

### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

### List ALL Sessions ###
if(!in_array($using_session,$InactiveSession))
{
	$sql = "SELECT 
					SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
			FROM 
					CARD_STUDENT_TIME_SESSION
			WHERE
					RecordStatus = 1 AND NonSchoolDay != 1
			ORDER BY
					SessionID";
	$temp = $lclass->returnArray($sql,7);
}
else
{
	$sql = "SELECT 
					SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
			FROM 
					CARD_STUDENT_TIME_SESSION
			WHERE
					(RecordStatus = 1 OR SessionID = $using_session) AND NonSchoolDay != 1
			ORDER BY
					SessionID";
	$temp = $lclass->returnArray($sql,7);
}

if(sizeof($temp)>0)
{
	$normal_table_content .= "";
	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list ($s_id, $s_name, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_type, $day_value) = $temp[$i];
		
		if(($using_session == $s_id)&&(in_array($using_session,$InactiveSession)))
		{
			$normal_table_content .= "<tr bgcolor=red><td>* $s_name</td>";
			$normal_warning = 1;
		}
		else
		{
			$normal_table_content .= "<tr><td>$s_name</td>";
		}
		if($hasAM)
			$normal_table_content .= "<td>$am_time</td>";
		if($hasLunch)
			$normal_table_content .= "<td>$lunch_time</td>";
		if($hasPM)
			$normal_table_content .= "<td>$pm_time</td>";
		$normal_table_content .= "<td>$leave_school_time</td>";

		$checked = " ";
		//if(($day_type!="")&&($day_value!="")&&($day_type==0)&&($day_value==0))
		if($s_id == $using_session)
		{
			$checked = "checked=1";
		}
		else
		{
			$checked = " ";
		}
		$normal_table_content .= "<td><input type=radio name=normal_session_id value=$s_id $checked></td></tr>";
		$normal_table_content .= "<input type=hidden name=ClassID value=$ClassID>";
	}
	
}
### End Of Normal Table ###
?>

<SCRIPT Language="JavaScript">
function removeDaySetting(Type, ClassID ,DayValue)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
		location.href = "group_session_remove.php?type="+Type+"&ClassID="+ClassID+"&value="+DayValue;
	}
}
function removeSpecialDaySetting(ClassID, targetDate)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
		location.href = "group_session_remove.php?ClassID="+ClassID+"&TargetDate="+targetDate;
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSessionSettings,'../',$i_StudentAttendance_Menu_Slot_Group,'index.php',$classname,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="group_edit_update.php" method=POST>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_NormalDays?></span>]</span></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
<?
if($hasAM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
<?
}
if($hasLunch)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
<?
}
if($hasPM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
<?
}
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_Using?></td></tr>
<?=$normal_table_content?>
</table>
<?
if($normal_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>
<table width=560 border=0 align=center>
<tr><td align=right><input type=image src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif"></td></tr>
</table>

<br><br>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_Weekday_Specific?></span>]</span><a href=group_add.php?type=1&ClassID=<?=$ClassID?>><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<?=$weekday_table_content?>
</table>
<?
if($weekly_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>

<br><br>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_Cycleday_Specific?></span>]</span><a href=group_add.php?type=2&ClassID=<?=$ClassID?>><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<?=$cycle_table_content?>
</table>
<?
if($cycle_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>

<br><br>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_SpecialDay?></span>]</span><a href=group_add.php?type=3&ClassID=<?=$ClassID?>><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<?=$special_day_table?>
</table>
<?
if($special_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>
</table>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>