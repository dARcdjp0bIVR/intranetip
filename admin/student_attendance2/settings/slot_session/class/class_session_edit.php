<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");

intranet_opendb();
$lclass = new libclass();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$days = $lc->getSessionDayValueWithoutSpecific($type);

if($type == 1)
{
	$select_title = $i_StudentAttendance_WeekDay;
    $select_day = $i_DayType0[$value];
    $sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = $ClassID AND DayType = $type AND DayValue = '$value'";
    $result = $lc->returnVector($sql);
    list($using_session) = $result;
    
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1"; 
    /*        
    $sql = "SELECT 
					a.SessionID, a.SessionName, TIME_FORMAT(a.MorningTime,'%H:%i'), TIME_FORMAT(a.LunchStart,'%H:%i'), TIME_FORMAT(a.LunchEnd,'%H:%i'), TIME_FORMAT(a.LeaveSchoolTime,'%H:%i'), IF(NonSchoolDay = 0, 'N', 'Y')
			FROM 
					CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.DayType NOT IN (0) AND b.DayType NOT IN (0))
			WHERE 
					a.RecordStatus = 1 AND a.NonSchoolDay = 0 
			GROUP BY 
					a.SessionID";
	*/
    $result = $lc->returnArray($sql,7);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day) = $result[$i];
		    $table_content .= "<tr><td>$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td>$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td>$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td>$PM_start_time</td>";
		    $table_content .= "<td>$leave_time</td><td>$non_school_day</td>";
		    
			if($using_session == $s_id)
			{
				$checked = "checked=1";
			}
			else
			{
				$checked = " ";
			}
		    
		    $table_content .= "<td><input type=radio name=weekday_session value=$s_id $checked></td></tr>";
	    }
	    $table_content .= "<input type=hidden name=type value=$type>";
	    $table_content .= "<input type=hidden name=value value=$value>";
	    $table_content .= "<input type=hidden name=ClassID value=$ClassID>";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
    }
}

if($type == 2)
{
	$select_title = $i_StudentAttendance_CycleDay;
    $select_day = $value;
    $sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_REGULAR WHERE ClassID = $ClassID AND DayType = $type AND DayValue = '$value'";
    $result = $lc->returnVector($sql);
    list($using_session) = $result;
    
    $sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
    /*
    $sql = "SELECT 
					a.SessionID, a.SessionName, TIME_FORMAT(a.MorningTime,'%H:%i'), TIME_FORMAT(a.LunchStart,'%H:%i'), TIME_FORMAT(a.LunchEnd,'%H:%i'), TIME_FORMAT(a.LeaveSchoolTime,'%H:%i'), IF(NonSchoolDay = 0, 'N', 'Y'), b.DayType, b.DayValue 
			FROM 
					CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.DayType NOT IN (0,1) AND b.DayType NOT IN (0,1))
			WHERE 
					a.RecordStatus = 1 AND a.NonSchoolDay = 0 
			GROUP BY 
					a.SessionID";
					echo $sql;
	*/
    $result = $lc->returnArray($sql,9);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $AM_start_time, $lunch_time, $PM_start_time, $leave_time, $non_school_day, $day_type, $day_value) = $result[$i];
		    //print_r($result);
		    $table_content .= "<tr><td>$s_name</td>";
		    if($hasAM)
		    	$table_content .= "<td>$AM_start_time</td>";
			if($hasLunch)
		    	$table_content .= "<td>$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td>$PM_start_time</td>";
		    $table_content .= "<td>$leave_time</td><td>$non_school_day</td>";
		    
			if($using_session == $s_id)
			{
				$checked = "checked=1";
			}
			else
			{
				$checked = " ";
			}
		    
		    $table_content .= "<td><input type=radio name=cycleday_session value=$s_id $checked></td></tr>";
	    }
	    $table_content .= "<input type=hidden name=type value=$type>";
	    $table_content .= "<input type=hidden name=value value=$value>";
	    $table_content .= "<input type=hidden name=ClassID value=$ClassID>";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
    }
}
if($type==3)
{
	$select_title = $i_StudentAttendance_TimeSlot_SpecialDay;
    $select_day = $value;
	
    $sql = "SELECT SessionID FROM CARD_STUDENT_TIME_SESSION_DATE WHERE ClassID = $ClassID AND RecordDate = '$value'";
	$result = $lc->returnVector($sql);
	list($using_session) = $result[0];
	
	$sql = "SELECT SessionID, SessionName, IF(NonSchoolDay=1,'-',TIME_FORMAT(MorningTime,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchStart,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LunchEnd,'%H:%i')), IF(NonSchoolDay=1,'-',TIME_FORMAT(LeaveSchoolTime,'%H:%i')), IF(NonSchoolDay = 0, 'N', 'Y') FROM CARD_STUDENT_TIME_SESSION WHERE RecordStatus = 1";
	$result = $lc->returnArray($sql,9);
    if(sizeof($result)>0)
    {
	    for($i=0; $i<sizeof($result); $i++)
	    {
		    list($s_id, $s_name, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_date) = $result[$i];
		    if($using_session == $s_id)
		    	$table_content .= "<tr bgcolor=red><td>$s_name</td>";
		    else
		    	$table_content .= "<tr><td>$s_name</td>";
			    
		    
		    if($hasAM)
		    	$table_content .= "<td>$am_time</td>";
			if($hasLunch)
		    	$table_content .= "<td>$lunch_time</td>";
		    if($hasPM)
		    	$table_content .= "<td>$pm_time</td>";
		    $table_content .= "<td>$leave_school_time</td><td>$non_school_date</td>";
		    
			if($using_session == $s_id)
			{
				$checked = "checked=1";
			}
			else
			{
				$checked = " ";
			}
		    
		    $table_content .= "<td><input type=radio name=specialday_session value=$s_id $checked></td></tr>";
	    }
	    $table_content .= "<input type=hidden name=type value=$type>";
	    $table_content .= "<input type=hidden name=value value=$value>";
	    $table_content .= "<input type=hidden name=ClassID value=$ClassID>";
	    $table_content .= "<tr class=tableTitle><td colspan=7><font color=red>red - now using</font></td></tr>";
    }
    if(sizeof($result)==0)
    {
	    $table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
    }
}
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSlotSettings,'../', $i_StudentAttendance_Menu_Slot_Class,'index.php', $button_edit, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="class_session_edit_update.php" method="POST" onSubmit="return checkForm()">

<table width=340 border=0 cellspacing=0 cellpadding=0 align=center>
<tr><td width=50% align=right><?=$select_title?>:</td><td><?=$select_day?></td></tr>
</table>
<br>
<table border=1 width=560 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr><tr>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
<?
if($hasAM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
<?
}
if($hasLunch)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
<?
}
if($hasPM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
<?
}
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_NonSchoolDay?></td>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_School_Use?></td>
</tr>
<?=$table_content?>
</table>
<table border=0 width=560 align=center>
<tr><td height=20></td></tr>
<tr><td><hr size=1></td></tr> 
<tr><td align=right><?=btnSubmit()?><?=btnReset()?><a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>