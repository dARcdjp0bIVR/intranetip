<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libcardstudentattend2();
$classname = $lc->getClassName($ClassID);

$class_mode = $lc->getClassAttendanceMode($ClassID);

$select_mode = "<SELECT name=mode>\n";
$select_mode .= "<OPTION value=0 ".($class_mode==0?"SELECTED":"").">$i_StudentAttendance_ClassMode_UseSchoolTimetable</OPTION>\n";
$select_mode .= "<OPTION value=1 ".($class_mode==1?"SELECTED":"").">$i_StudentAttendance_ClassMode_UseClassTimetable</OPTION>\n";
$select_mode .= "<OPTION value=2 ".($class_mode==2?"SELECTED":"").">$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</OPTION>\n";
$select_mode .= "</SELECT>\n";
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSessionSettings,'../',$i_StudentAttendance_Menu_Slot_Class,'index.php',$classname,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action='class_update.php' method=POST>
<table width=500 border=0 cellpadding=4 cellspacing=0 align=center>
<tr><td align=right><?php echo $i_ClassName; ?>:</td><td><?=$classname?></td></tr>
<tr><td align=right><?php echo $i_StudentAttendance_ClassMode; ?>:</td><td><?=$select_mode?></td></tr>
<? if ($class_mode == 1) { ?>
<tr><td align=right>&nbsp;</td><td><a class=functionlink_new href=class_edit.php?ClassID=<?=$ClassID?>><?=$i_StudentAttendance_ClassMode_Edit?></a></td></tr>
<? } ?>
</table>

<table border=0 width=560 align=center>
<tr><td><hr size=1></td></tr>
<tr><td align=right>
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>

<input type=hidden name=ClassID value=<?=$ClassID?>>

</form>