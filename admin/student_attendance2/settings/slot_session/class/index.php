<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lc = new libcardstudentattend2();
$classList = $lc->getClassListMode();

$table_content = " ";
$table_content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$table_content .= "<tr class=tableTitle><td>$i_ClassName</td><td>$i_StudentAttendance_ClassMode</td></tr>";
if(sizeof($classList)>0)
{
	for($i=0; $i<sizeof($classList); $i++)
	{
		list($class_id, $class_name, $mode) = $classList[$i];
		if ($mode == 2)
		{
			$word_mode = $i_StudentAttendance_ClassMode_NoNeedToTakeAttendance;
		}
		else if ($mode == 1)
		{
			$word_mode = $i_StudentAttendance_ClassMode_UseClassTimetable;
		}
		else
		{
			$word_mode = $i_StudentAttendance_ClassMode_UseSchoolTimetable;
		}
		$css = ($i%2? "":"2");
     	$editlink = "<a class=functionlink href=class.php?ClassID=$class_id><img src=\"$image_path/icon_edit.gif\" border=0></a>";
     	$table_content .= "<tr class=tableContent$css>\n";
     	$table_content .= "<td>$class_name $editlink</td><td>$word_mode</td>";
     	$table_content .= "</tr>\n";
	}
}
$table_content .= "</table>";
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSessionSettings,'../',$i_StudentAttendance_Menu_Slot_Class,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu></td></tr>
  <tr>
    <td><?=$table_content?></td>
  </tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<br><br>

<?
include_once("../../../../../templates/adminfooter.php");
intranet_closedb();
?>