<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");

include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");

if (!defined("LIBCARDSTUDENTATTEND2_DEFINED"))                  // Preprocessor directives
{
        define("LIBCARDSTUDENTATTEND2_DEFINED",true);

        # Constant definition
        define("CARD_STATUS_PRESENT",0);
        define("CARD_STATUS_ABSENT",1);
        define("CARD_STATUS_LATE",2);
        define("CARD_STATUS_OUTING",3);
        define("CARD_LEAVE_NORMAL",0);
        define("CARD_LEAVE_AM",1);
        define("CARD_LEAVE_PM",2);
        define("CARD_BADACTION_LUNCH_NOTINLIST",1);
        define("CARD_BADACTION_LUNCH_BACKALREADY",2);
        define("CARD_BADACTION_FAKED_CARD_AM",3);
        define("CARD_BADACTION_FAKED_CARD_PM",4);
        define("CARD_BADACTION_NO_CARD_ENTRANCE",5);
        define("PROFILE_DAY_TYPE_WD",1);
        define("PROFILE_DAY_TYPE_AM",2);
        define("PROFILE_DAY_TYPE_PM",3);
        define("PROFILE_TYPE_ABSENT",1);
        define("PROFILE_TYPE_LATE",2);
        define("PROFILE_TYPE_EARLY",3);

        class libcardstudentattend2 extends libclass {
                var $file_array;
                var $word_array;
                var $word_base_dir;

                # Settings
                var $attendance_mode;
                var $time_table_mode;
                var $disallow_input_in_profile;

                function libcardstudentattend2()
                {
                        $this->libclass();

                        # Make word templates array
                        global $intranet_root;
                        global $i_StudentAttendance_PresetWord_CardSite,
                               $i_StudentAttendance_PresetWord_OutingFromWhere,
                               $i_StudentAttendance_PresetWord_OutingLocation,
                               $i_StudentAttendance_PresetWord_OutingObjective,
                               $i_StudentAttendance_PresetWord_DetentionLocation,
                               $i_StudentAttendance_PresetWord_DetentionReason;
                        $this->file_array = array("site.txt",
                               "outing_from.txt",
                               "outing_loc.txt",
                               "outing_obj.txt",
                               "detent_loc.txt",
                               "detent_reason.txt"
                               );
                        $this->word_array = array($i_StudentAttendance_PresetWord_CardSite,
                               $i_StudentAttendance_PresetWord_OutingFromWhere,
                               $i_StudentAttendance_PresetWord_OutingLocation,
                               $i_StudentAttendance_PresetWord_OutingObjective,
                               $i_StudentAttendance_PresetWord_DetentionLocation,
                               $i_StudentAttendance_PresetWord_DetentionReason);
                        $this->word_base_dir = "$intranet_root/file/cardword/";
                        $this->attendance_mode = "NO";
                }

                function GET_ADMIN_USER()
                                {
                                        global $intranet_root;

                                        $lf = new libfilesystem();
                                        $AdminUser = trim($lf->file_read($intranet_root."/file/student_attendance2/admin_user.txt"));

                                        return $AdminUser;
                                }

                                function IS_ADMIN_USER($ParUserID)
                                {
                                        global $intranet_root, $plugin;

                                        $AdminUser = $this->GET_ADMIN_USER();
                                        $IsAdmin = 0;
                                        if(!empty($AdminUser))
                                        {
                                                $AdminArray = explode(",", $AdminUser);
                                                $IsAdmin = ($plugin['attendancestudent_eAdmin'] && in_array($ParUserID, $AdminArray)) ? 1 : 0;
                                        }

                                        return $IsAdmin;
                                }

                function retrieveTimeTableMode()
                {
                                global $intranet_root;
                                $time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
                                $time_table_mode = $time_table;
                                return $time_table_mode;
                }

                function retrieveSettings()
                {
                         global $intranet_root;
                         $content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
                         $attendance_mode = $content_basic;
                         $this->attendance_mode = $attendance_mode;

                         $this->disallowInputInProfile();

                }
                                                                // edited by Peter,14/07/2006 get TimeArray With NonScholDay field
                function getTimeArray($type,$value)
                {
                         if ($type==0)      # Normal
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                            FROM CARD_STUDENT_PERIOD_TIME WHERE DayType = 0 AND DayValue = 0";
                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                         else
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
                                            FROM CARD_STUDENT_PERIOD_TIME WHERE DayType = '$type' AND DayValue ='$value'";
                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                }

                                                                // edited by Peter, 14/07/2006 added NonScholDay field
                # Param: $type: 1 - Weekday, 2 - Cycle Day
                function getTimeArrayList($type)
                {
                         if ($type != 1 && $type != 2) return;
                         $sql = "SELECT DayValue, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_PERIOD_TIME
                                        WHERE DayType = '$type' ORDER BY DayValue";
                         return $this->returnArray($sql,6);
                }

                // Created By Ronald on 30 May 2007
                function getSessionTimeArrayList($type,$class_id)
                {
                         if ($type != 1 && $type != 2) return;
                         if ($class_id == 0)
                                         $cond = " b.ClassID = 0 ";
                         else
                                         $cond = " b.ClassID = $class_id ";

                         $sql = "SELECT
                                                         a.SessionID, TIME_FORMAT(a.MorningTime,'%H:%i'), TIME_FORMAT(a.LunchStart,'%H:%i'), TIME_FORMAT(a.LunchEnd,'%H:%i'), TIME_FORMAT(a.LeaveSchoolTime,'%H:%i'), a.NonSchoolDay, b.DayValue
                                 FROM
                                                 CARD_STUDENT_TIME_SESSION AS a INNER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID)
                                 WHERE
                                                 b.DayType = '$type' AND $cond ORDER BY b.DayValue";
echo "-- getSessionTimeArrayList ><br>".$sql."<br><111--<br>";
                         return $this->returnArray($sql,7);
                }

                // created by PEter, 14/07/2006   CARD_STUDENT_SPECIFIC_DATE_TIME
                function getSpecialTimeArrayList(){
                                $sql="SELECT RecordDate,IF(ClassID IS NULL,0,ClassID),TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_SPECIFIC_DATE_TIME ORDER BY RecordDate";
                          return $this->returnArray($sql,7);
                }

                function getDayValueWithoutSpecific($type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_PERIOD_TIME
                                        WHERE DayType = '$type' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }
                function getSessionDayValueWithoutSpecific($type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_TIME_SESSION_REGULAR
                                        WHERE DayType = '$type' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }
                function getDayValueWithoutSpecificForClass($ClassID,$type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_CLASS_PERIOD_TIME
                                        WHERE DayType = '$type' AND ClassID = '$ClassID' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }
                function getSessionDayValueWithoutSpecificForClass($ClassID,$type=1)
                {
                         $sql = "SELECT DayValue FROM CARD_STUDENT_TIME_SESSION_REGULAR
                                        WHERE DayType = '$type' AND ClassID = '$ClassID' ORDER BY DayValue";
                         $specific_dayvalues = $this->returnVector($sql);
                         if ($type==2)
                         {
                             $sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS ORDER BY TextShort";
                             $whole_set = $this->returnVector($sql);
                         }
                         else
                         {
                             $whole_set = array(0,1,2,3,4,5,6);
                         }
                         for ($i=0; $i<sizeof($whole_set); $i++)
                         {
                              $temp = $whole_set[$i];
                              if (!in_array($temp,$specific_dayvalues))
                              {
                                   $result[] = $temp;
                              }
                         }
                         return $result;
                }
                function getClassListMode()
                {
                         $sql = "SELECT a.ClassID, a.ClassName, b.Mode
                                        FROM INTRANET_CLASS as a
                                             LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                        WHERE a.RecordStatus = 1
                                        ORDER BY a.ClassName";
                         return $this->returnArray($sql,3);
                }
                function getClassAttendanceMode($ClassID)
                {
                         $sql = "SELECT Mode FROM CARD_STUDENT_CLASS_SPECIFIC_MODE
                                        WHERE ClassID = '$ClassID'";
                         $temp = $this->returnVector($sql);
                         return $temp[0]+0;
                }
                function getClassListToTakeAttendance()
                {
                         $sql = "SELECT a.ClassID, a.ClassName FROM INTRANET_CLASS as a
                                        LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                 WHERE a.RecordStatus = 1 AND (b.ClassID IS NULL OR b.Mode != 2)
                                 ORDER BY a.ClassName";
                         return $this->returnArray($sql,2);
                }

                function getClassListNotTakeAttendance()
                {
                         $sql = "SELECT a.ClassID, a.ClassName FROM INTRANET_CLASS as a
                                        LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                 WHERE a.RecordStatus = 1 AND b.Mode = 2
                                 ORDER BY a.ClassName";
                         return $this->returnArray($sql,2);
                }

                function isRequiredToTakeAttendance($classname)
                {

                         $sql = "SELECT b.Mode
                                       FROM INTRANET_CLASS as a
                                            LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                       WHERE a.ClassName = '$classname' AND a.RecordStatus = 1
                                       ";
                         $temp = $this->returnVector($sql);
                         if ($temp[0]==2) return false;
                         else return true;
                }



                # updated by Peter 2006/10/06, to support the checking of NonSchoolDay
                # used in:  /home/admin/smartcard/take/takeAM.php
                #           /home/admin/smartcard/take/takePM.php
                #           /home/profile/smartcard/list.php
                #           /admin/student_attendance2/dailyoperation/class/class_status.php
                #           /admin/student_attendance2/dailyoperation/class/view_student_AM.php
                #           /admin/student_attendance2/dailyoperation/class/view_student_AM_q.php
                #           /admin/student_attendance2/dailyoperation/class/view_student_PM.php
                #           /admin/student_attendance2/dailyoperation/class/view_student_PM_q.php
                function isRequiredToTakeAttendanceByDate($classname,$TargetDate="")
                {
                    if($TargetDate=="")
                            $TargetDate= date('Y-m-d');

                    $sql = "SELECT a.ClassID, a.ClassName, b.Mode
                                    FROM INTRANET_CLASS as a
                                         LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                    WHERE a.RecordStatus = 1 AND a.ClassName='$classname'
                                    ORDER BY a.ClassName";
                    $temp2 =$this->returnArray($sql,3);
                    //echo $temp2[0][2] . "<hr>";
                    if($temp2[0][2]==2) return false;

                    $ts_record = strtotime($TargetDate);
                    $txt_year = date('Y',$ts_record);
                    $txt_month = date('m',$ts_record);
                    $txt_day = date('j',$ts_record);
                    $day_of_week = date('w',$ts_record);
                    # select classes with non school day on TargetDate from CARD_STUDENT_SPECIFIC_DATE_TIME
                    $sqlSpecial = "SELECT ClassID,IF(NonSchoolDay=1,1,2) FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate='$TargetDate'";
                    $temp = $this->returnArray($sqlSpecial,2);
                    for($i=0;$i<sizeof($temp);$i++){
                            $resultSpecial[$temp[$i][0]]=$temp[$i][1];
                    }

                    # select classes with non school day on Target Cycle Day from CARD_STUDENT_CLASS_PERIOD_TIME
                    $sqlClassCycle ="select a.ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME AS a, INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
                    $temp=$this->returnArray($sqlClassCycle,2);

                    for($i=0;$i<sizeof($temp);$i++){
                            $resultClassCycle[$temp[$i][0]]=$temp[$i][1];
                    }

                    # select classes with non school day on Target Cycle Day from CARD_STUDENT_PERIOD_TIME
                    $sqlSchoolCycle ="select IF(a.NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME AS a,INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
                    $resultSchoolCycle=$this->returnVector($sqlSchoolCycle);

                    # select classes with non school day on Target Week Day from CARD_STUDENT_PERIOD_TIME
                    $sqlSchoolWeek ="select IF(NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
                    $resultSchoolWeek = $this->returnVector($sqlSchoolWeek);

                    # select classes with non school day on Target Week Day from CARD_STUDENT_CLASS_PERIOD_TIME
                    $sqlClassWeek ="select ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
                    $temp=$this->returnArray($sqlClassWeek,2);

                    for($i=0;$i<sizeof($temp);$i++){
                            $resultClassWeek[$temp[$i][0]]=$temp[$i][1];
                    }

                    $off=false;
                    $done=false;
                    $classID = $temp2[0][0];
                    $className = $temp2[0][1];
                    $classMode = $temp2[0][2];
                    $specialClassID= $classMode==0?0:$classID;

                    # check if NonSchoolDay for Speical Date
                    if($resultSpecial[$specialClassID]==1){
                                    //echo "<p>speical date off=$className</p>";
                                    $off=true;
                                    $done=true;
                    }else if($resultSpecial[$specialClassID]==2){
                                    //echo "<p>speical date on=$className</p>";
                                    $off=false;
                                    $done=true;
                    }

                    # check if NonSchoolDay for Cycle Day
                    if(!$done){
                                    if($classMode == 1){ # Class Cycle Day
                            if($resultClassCycle[$classID]==1){
                                //echo "<p>class cycle date off=$className</p>";
                                $off = true;
                                $done= true;
                            }else if($resultClassCycle[$classID]==2){
                                //echo "<p>class cycle date on=$className</p>";
                                $off = false;
                                $done= true;
                            }
                        }else if($classMode !=2){  # School Cycle Day
                            if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
                                //echo "<p>school cycle date off=$className</p>";
                                $off = true;
                                $done = true;
                            } else if($resultSchoolCycle[0]==2){
                                //echo "<p>school cycle date on=$className</p>";
                                $off = false;
                                $done = true;
                                }
                            }
                    }

                    # check if NonSchoolDay for Week Day
                    if(!$done){
                        if($classMode==1){ # Class Week Day
                            if($resultClassWeek[$classID]==1){
                                //echo "<p>class week date off=$className</p>";
                                $done = true;
                                $off = true;
                            }else if($resultClassWeek[$classID]==2){
                                //echo "<p>class week date on=$className</p>";
                                $done = true;
                                $off = false;
                            }

                        }else if($classMode!=2){ # School Week Day
                            if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
                                //echo "<p>school week date off=$className</p>";
                                $off = true;
                                $done=true;
                            } else if($resultSchoolWeek[0]==2){
                                //echo "<p>school week date on=$className</p>";
                                $off = false;
                                $done=true;
                                }
                            }
                    }

                    if($classMode==2 || $off){
                                    return false;
                    }else{
                            return true;
                    }
                }

                # updated by Peter 2006/10/09, to support the checking of NonSchoolDay
                # Used in: /home/admin/smartcard/take/index.php
                function getClassListToTakeAttendanceByDate($TargetDate)
                {
                                   $TargetDate==""?date('Y-m-d'):$TargetDate;
                                 $sql = "SELECT a.ClassID,a.ClassName FROM INTRANET_CLASS AS a WHERE a.RecordStatus=1 ORDER BY a.ClassName";
                         $classList = $this->returnArray($sql,2);
                         $need_take_attendance = array();
                         for($i=0;$i<sizeof($classList);$i++){
                                 list($class_id,$class_name) = $classList[$i];
                                 if($this->isRequiredToTakeAttendanceByDate($class_name,$TargetDate))
                                         $need_take_attendance[]=array($class_id,$class_name);
                             }
                             return $need_take_attendance;
                }

                # updated by Peter 2006/10/09, to support the checking of NonSchoolDay
                function getClassListNotTakeAttendanceByDate($TargetDate)
                {
                                 $TargetDate==""?date('Y-m-d'):$TargetDate;
                                $sql = "SELECT a.ClassID,a.ClassName FROM INTRANET_CLASS AS a WHERE a.RecordStatus=1 ORDER BY a.ClassName";
                         $classList = $this->returnArray($sql,2);
                         $not_take_attendance = array();
                         for($i=0;$i<sizeof($classList);$i++){
                                 list($class_id,$class_name) = $classList[$i];
                                 if(!$this->isRequiredToTakeAttendanceByDate($class_name,$TargetDate))
                                         $not_take_attendance[]=array($class_id,$class_name);
                             }
                             return $not_take_attendance;
                    }

                # edited by PeterHo 14/07/2006 add NonSchoolDay
                function getClassTimeArray($ClassID,$type,$value)
                {
                         if ($type==0)      # Normal
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                            FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType = 0 AND DayValue = 0 AND ClassID = '$ClassID'";
                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                         else
                         {
                             $sql = "SELECT TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                            FROM CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType = '$type' AND DayValue ='$value' AND ClassID = '$ClassID'";
                             $temp = $this->returnArray($sql,5);
                             return $temp[0];
                         }
                }
                # created by PeterHo 17/07/2006
                function getClassSpecialTimeArray($ClassID,$TargetDate){
                                                                        $sql="SELECT RecordDate,TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_SPECIFIC_DATE_TIME  WHERE ClassID='$ClassID' AND RecordDate='$TargetDate' ORDER BY RecordDate";
                                   $temp=$this->returnArray($sql,6);
                                   return $temp[0];

                }
                # created by PeterHo 14/07/2006
                function getClassSpecialTimeArrayList($ClassID){
                                                                        $sql="SELECT RecordDate,TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_SPECIFIC_DATE_TIME  WHERE ClassID='$ClassID' ORDER BY RecordDate";
                                   return $this->returnArray($sql,6);
                }
                // Edited by PeterHo 14/07/2006 add NonSchoolDay
                # Param: $type: 1 - Weekday, 2 - Cycle Day
                function getClassTimeArrayList($ClassID,$type)
                {
                         if ($type != 1 && $type != 2) return;
                         $sql = "SELECT DayValue, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'),NonSchoolDay
                                        FROM CARD_STUDENT_CLASS_PERIOD_TIME
                                        WHERE DayType = '$type' AND ClassID = '$ClassID' ORDER BY DayValue";
                         return $this->returnArray($sql,6);
                }

                function removeBadActionRecord($StudentID, $type, $date)
                {
                         if ($date == "")
                         {
                             //$date = date('Y-m-d');
                             return;
                         }

                         $sql =" DELETE FROM CARD_STUDENT_BAD_ACTION WHERE StudentID='$StudentID' AND RecordDate='$date' AND RecordType='$type'";
                         $this->db_db_query($sql);
                }
                function removeBadActionOutLunchTrial($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_LUNCH_NOTINLIST, $date);
                }
                function removeBadActionOutLunchAgain($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_LUNCH_BACKALREADY, $date);
                }
                function removeBadActionFakedCardAM($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_AM, $date);
                }
                function removeBadActionFakedCardPM($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_PM, $date);
                }
                function removeBadActionNoCardEntrance($StudentID, $date)
                {
                         $this->removeBadActionRecord($StudentID, CARD_BADACTION_NO_CARD_ENTRANCE, $date);
                }
                function addBadActionRecord($StudentID, $type, $date="", $time="")
                {
                         if ($date == "")
                         {
                             $date = date('Y-m-d');
                         }
                         $time = ($time == ""? "now()":"'$date $time'");

                         $sql = "INSERT INTO CARD_STUDENT_BAD_ACTION (StudentID, RecordDate, RecordTime, RecordType, DateInput, DateModified)
                                        VALUES ('$StudentID','$date', $time, '$type', now(), now())";
                         $this->db_db_query($sql);
                }

                function addBadActionOutLunchTrial($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_LUNCH_NOTINLIST, $date, $time);
                }
                function addBadActionOutLunchAgain($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_LUNCH_BACKALREADY, $date, $time);
                }
                function addBadActionFakedCardAM($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_AM, $date, $time);
                }
                function addBadActionFakedCardPM($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_FAKED_CARD_PM, $date, $time);
                }
                function addBadActionNoCardEntrance($StudentID, $date="", $time="")
                {
                         $this->addBadActionRecord($StudentID, CARD_BADACTION_NO_CARD_ENTRANCE, $date, $time);
                }

                function createTable_Card_Student_Daily_Log($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                         $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name(
                                  RecordID int(11) NOT NULL auto_increment,
                                  UserID int(11) NOT NULL,
                                  DayNumber int(11) NOT NULL,
                                  InSchoolTime time,
                                  InSchoolStation varchar(255),
                                  AMStatus int,
                                  LunchOutTime time,
                                  LunchOutStation varchar(255),
                                  LunchBackTime time,
                                  LunchBackStation varchar(255),
                                  PMStatus int,
                                  LeaveSchoolTime time,
                                  LeaveSchoolStation varchar(255),
                                  LeaveStatus int,
                                  ConfirmedUserID int,
                                  IsConfirmed int,
                                  RecordType int,
                                  RecordStatus int,
                                  DateInput datetime,
                                  DateModified datetime,
                                  PRIMARY KEY (RecordID),
                                  UNIQUE UserDay (UserID, DayNumber)
                                )";
                         $this->db_db_query($sql);
                }

                ### Created By Ronald Yeung On 27 Jul 2007 ###
                ### Create CARD_STUDENT_ENTRY_LOG If Not Exists
                function createTable_Card_Student_Entry_Log($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$year."_".$month;
                         $sql = "CREATE TABLE IF NOT EXISTS $card_log_table_name
                                                                        (
                                                                  RecordID int(11) NOT NULL auto_increment,
                                                                  UserID int(11) NOT NULL,
                                                                  DayNumber int(11) NOT NULL,
                                                                  RecordTime time,
                                                                  RecordType int(11),
                                                                  RecordStatus int(11),
                                                                  DateInput datetime,
                                                                  DateModified datetime,
                                                                  PRIMARY KEY (RecordID)
                                                                )";
                         $this->db_db_query($sql);
                }

                function createTable_Card_Student_Daily_Class_Confirm($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month;
                         $sql = "CREATE TABLE IF NOT EXISTS $card_student_daily_class_confirm (
                                  RecordID int(11) NOT NULL auto_increment,
                                  ClassID int(11) NOT NULL,
                                  ConfirmedUserID int(11) NOT NULL,
                                  DayNumber int(11) NOT NULL,
                                  DayType int(11) NOT NULL,
                                  RecordType int(11),
                                  RecordStatus int(11),
                                  DateInput datetime,
                                  DateModified datetime,
                                  PRIMARY KEY (RecordID),
                                  UNIQUE (ClassID,DayNumber,DayType)
                                )";
                         $this->db_db_query($sql);
                }
                function createEntry_Card_Student_Record_Storage($year="", $month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $sql = "INSERT INTO CARD_STUDENT_RECORD_DATE_STORAGE (Year, Month)
                                        VALUES ($year, $month)";
                         $this->db_db_query($sql);
                }
                function createTable_LogAndConfirm($year="",$month="")
                {
                         $year=($year=="")?date("Y"):$year;
                         $month=($month=="")?date("m"):$month;

                         $this->createTable_Card_Student_Daily_Log($year, $month);
                         $this->createTable_Card_Student_Daily_Class_Confirm($year, $month);
                         $this->createEntry_Card_Student_Record_Storage($year, $month);
                }

                # 0 - In School status (morning)
                # 1 - Lunch Status
                # 2 - After School Status
                function getSummaryCount($type)
                {
                         if ($this->attendance_mode=="NO")
                         {
                             $this->retrieveSettings();
                         }
                         /*
                         if ($this->attendance_mode!=2 && $type==2)
                         {
                             $type = 0;
                         }
                         */
                         if ($type != 0 && $type != 1 && $type!=2)
                         {
                             $type = 0;
                         }
                         $year=date("Y");
                         $month=date("m");
                         $day = date('d');

                         $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                         $status_field = ($this->attendance_mode==1?"PMStatus":"AMStatus");

                         if ($type == 0)  # Back School Time check
                         {

                             # Get Already back
                             // $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
                             //               WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR $status_field IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") )";
                             $sql = "SELECT UserID FROM $card_log_table_name
                                           WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR AMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE."))";
                             $temp = $this->returnVector($sql);
                             $back = sizeof($temp);
                             if($back>0){
                                     $back_student_list = implode(",",$temp);
                                     $back_conds = " AND a.UserID NOT IN($back_student_list) ";
                                 }

                             # Get not yet back
                             # Get no need to take attendance classes

                             /*
                             $sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
                                            LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                            WHERE b.Mode = 2";
                             $temp = $this->returnVector($sql);
                             */
                             $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
                             if (sizeof($temp)==0)
                             {
                                 $sql = "SELECT COUNT(a.UserID)
                                            FROM INTRANET_USER as a
                                                 LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                            WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $back_conds
                                                    ";
                             }
                             else
                             {
                                     for($j=0;$j<sizeof($temp);$j++){
                                             $t[] = $temp[$j][1];
                                         }
                                 $class_list = "'".implode("','",$t)."'";
                                 $sql = "SELECT COUNT(a.UserID)
                                            FROM INTRANET_USER as a
                                                 LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                            WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $back_conds
                                                     AND a.ClassName NOT IN ($class_list)";
                             }
                             $temp = $this->returnVector($sql);
                             $notyet = $temp[0]+0;


                            return array($back,$notyet);
                         }
                         else if ($type == 1) # Out for lunch check
                         {
                              # Gone out
                              //$sql = "SELECT COUNT(a.RecordID) FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON a.UserID = b.StudentID
                              //               WHERE DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND LeaveStatus IS NULL";
                              $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
                                             WHERE DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND LeaveStatus IS NULL AND (PMStatus IS NULL OR PMStatus IN (1,3))";

                              $temp = $this->returnVector($sql);
                              $gone_out = $temp[0]+0;
                              //echo "<BR>".$sql;
                              //echo "<BR>A:".sizeof($temp);

                              # back school
                              //$sql = "SELECT COUNT(a.RecordID) FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND a.DayNumber = '$day')
                              //               WHERE (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.LunchOutTime IS NOT NULL AND a.LunchBackTime IS NOT NULL AND LeaveStatus NOT IN (1)) OR (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (a.LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";
                                                          $sql = "SELECT COUNT(a.RecordID) FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND a.DayNumber = '$day')
                                             WHERE (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.LunchOutTime IS NOT NULL AND a.LunchBackTime IS NOT NULL AND LeaveStatus IS NULL) OR (a.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (a.LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";

                              $temp = $this->returnVector($sql);
                              //echo "<BR>".$sql;
                              $back = $temp[0]+0;
                              //echo "<BR>B:".sizeof($temp);

                              # Not gone out
                              # Can go out students - $gone_out - $back
                              global $intranet_root;
                              $content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
                              $lunch_misc_settings = explode("\n",$content_lunch_misc);
                              list($lunch_misc_no_record,$lunch_misc_lunch_once, $lunch_misc_all_allow) = $lunch_misc_settings;

                              if ($lunch_misc_all_allow)
                              {
                                  # Get no need to take attendance classes
                                  //$sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
                                  //               LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                  //               WHERE b.Mode = 2";
                                  //$temp = $this->returnVector($sql);

                                  $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
                                  if (sizeof($temp)==0)
                                  {
                                  /*
                                      $sql = "SELECT COUNT(a.UserID)
                                                     FROM INTRANET_USER as a
                                                          LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                                     WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1)
                                                           AND b.RecordID IS NULL";
                                                           */
                                      $sql = "SELECT COUNT(a.UserID)
                                                     FROM INTRANET_USER as a
                                                          LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                                     WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                                           AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.")";
                                  }
                                  else
                                  {
                                      $class_list = "'".implode("','",$temp)."'";
                                      $sql = "SELECT COUNT(a.UserID)
                                                     FROM INTRANET_USER as a
                                                          LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                                     WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                                           AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.")
                                                           AND a.ClassName NOT IN ($class_list)";
                                                           # b.RecordID IS NULL
                                  }
                              }
                              else
                              {
                                  # Get from Lunch outers list
                                  $sql = "SELECT COUNT(b.UserID)
                                                 FROM CARD_STUDENT_LUNCH_ALLOW_LIST as a
                                                      LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                                                           AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
                                                      LEFT OUTER JOIN $card_log_table_name as c ON a.StudentID = c.UserID AND c.DayNumber = '$day'
                                                 WHERE b.UserID IS NOT NULL
                                                       AND c.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2))";
                              }
                              //echo "<BR>".$sql;
                              $temp = $this->returnVector($sql);
                              $total = $temp[0]+0;
                              //echo $total;
                              return array($gone_out, $back, $total-$gone_out-$back);
                         }
                         else if ($type == 2)
                         {
                              //$sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
                              //               WHERE DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") )) OR (LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND DayNumber = '$day')";
                              //$sql = "  SELECT
                              //                                        COUNT(RecordID) FROM $card_log_table_name
                              //                        WHERE
                              //                                        DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.")))
                              //                                        OR (LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND DayNumber = '$day')
                              //                                        OR (LunchOutTime IS NULL AND LunchBackTime IS NULL AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND PMStatus IN(".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING."))";
                              $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
                                                      WHERE DayNumber = '$day' AND
                                                                                (DayNumber = '$day' AND LeaveSchoolTime IS NOT NULL OR LeaveStatus IS NOT NULL) OR
                                                                                (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING.") AND LeaveStatus IS NULL) OR
                                                                                (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IS NULL AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))";
                              //echo "<BR>".$sql;
                              $temp = $this->returnVector($sql);
                              $left = $temp[0]+0;

                              $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
                                                      WHERE DayNumber = '$day' AND
                                                                                (DayNumber = '$day' AND LeaveSchoolTime IS NOT NULL OR LeaveStatus IS NOT NULL) OR
                                                                                (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING.") AND LeaveStatus IS NULL) OR
                                                                                (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IS NULL AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))";
                              $result = $this->returnVector($sql);
                              if(sizeof($result)>0)
                              {
                                      $list = implode(",",$result);
                                      $cond = " AND UserID NOT IN ($list)";
                              }
                              else
                              {
                                      $cond = " ";
                              }
                              //$sql = "SELECT DISTINCT UserID from $card_log_table_name WHERE DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL OR AMStatus IN (0,2) OR PMStatus IN (0,2)) AND UserID NOT IN ($list)";
                              $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
                                             WHERE DayNumber = '$day' AND
                                             LeaveSchoolTime IS NULL AND
                                             LeaveStatus IS NULL AND
                                             (DayNumber = '$day' AND LeaveSchoolTime IS NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ")))
                                             $cond
                                             ";
                              //echo "<BR>".$sql;
                              $temp = $this->returnVector($sql);
                              $rest = $temp[0]+0;
                              return array($left,$rest);
                         }
                         else
                         {
                             return;
                         }
                }
                # 0 - In School status (morning)
                # 1 - Lunch Status
                # 2 - After School Status
                function getSummaryDetail($type)
                {
                         if ($this->attendance_mode=="NO")
                         {
                             $this->retrieveSettings();
                         }
                         /*
                         if ($this->attendance_mode!=2 && $type==2)
                         {
                             $type = 0;
                         }
                         */
                         if ($type != 0 && $type != 1 && $type!=2)
                         {
                             $type = 0;
                         }
                         $year=date("Y");
                         $month=date("m");
                         $day = date('d');

                         $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                         $status_field = ($this->attendance_mode==1?"PMStatus":"AMStatus");

                         if ($type == 0)  # Back School Time check
                         {
                             # Get Already back
                             // $sql = "SELECT COUNT(RecordID) FROM $card_log_table_name
                             //               WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR $status_field IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") )";
                             //$sql = "SELECT UserID FROM $card_log_table_name
                             //              WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR $status_field IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") )";
                             $sql = "SELECT UserID FROM $card_log_table_name
                                           WHERE DayNumber = '$day' AND (InSchoolTime IS NOT NULL OR AMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_PRESENT.", ".CARD_STATUS_LATE."))";
                             $temp = $this->returnVector($sql);
                             $back = $temp;
                             if(sizeof($back)>0){
                                     $back_student_list = implode(",",$temp);
                                     $back_conds = " AND a.UserID NOT IN($back_student_list) ";
                                 }

                             # Get not yet back
                             # Get no need to take attendance classes

                             /*
                             $sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
                                            LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                            WHERE b.Mode = 2";
                             $temp = $this->returnVector($sql);
                             */
                             $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
                             if (sizeof($temp)==0)
                             {
                                 $sql = "SELECT a.UserID
                                            FROM INTRANET_USER as a
                                                 LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                            WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $back_conds
                                                    ";
                             }
                             else
                             {
                                     for($j=0;$j<sizeof($temp);$j++){
                                             $t[] = $temp[$j][1];
                                         }
                                 $class_list = "'".implode("','",$t)."'";
                                 $sql = "SELECT a.UserID
                                            FROM INTRANET_USER as a
                                                 LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                            WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) $back_conds
                                                     AND a.ClassName NOT IN ($class_list)";
                             }
                             $notyet = $this->returnVector($sql);
                             return array($back,$notyet);
                         }
                         else if ($type == 1) # Out for lunch check
                         {
                              # Gone out
                              $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
                                             WHERE DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND LeaveStatus IS NULL AND (PMStatus IS NULL OR PMStatus IN (1,3))";
                              $gone_out = $this->returnVector($sql);

                              # back school
                              //$sql = "SELECT DISTINCT UserID FROM $card_log_table_name
                              //               WHERE DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NOT NULL";
                              //$sql = "SELECT DISTINCT UserID FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND DayNumber = '$day')
                              //               WHERE (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NOT NULL AND LeaveStatus NOT IN (1)) OR (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";
                                                          $sql = "SELECT DISTINCT UserID FROM $card_log_table_name AS a INNER JOIN CARD_STUDENT_LUNCH_ALLOW_LIST AS b ON (a.UserID = b.StudentID AND DayNumber = '$day')
                                             WHERE (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LunchOutTime IS NOT NULL AND LunchBackTime IS NOT NULL AND LeaveStatus IS NULL) OR (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2)))";
                              $back = $this->returnVector($sql);

                              # Not gone out
                              # Can go out students - $gone_out - $back
                              global $intranet_root;
                              $content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
                              $lunch_misc_settings = explode("\n",$content_lunch_misc);
                              list($lunch_misc_no_record,$lunch_misc_lunch_once, $lunch_misc_all_allow) = $lunch_misc_settings;

                              if ($lunch_misc_all_allow)
                              {
                                  # Get no need to take attendance classes
                                  //$sql = "SELECT a.ClassName FROM INTRANET_CLASS as a
                                  //               LEFT OUTER JOIN CARD_STUDENT_CLASS_SPECIFIC_MODE as b ON a.ClassID = b.ClassID
                                  //               WHERE b.Mode = 2";
                                  //$temp = $this->returnVector($sql);
                                  $temp = $this->getClassListNotTakeAttendanceByDate(date('Y-m-d'));
                                  $minus_cond = "";
                                  if (sizeof($back)!=0)
                                  {
                                      $minus_cond .= " AND a.UserID NOT IN (".implode(",",$back).")";
                                  }
                                  if (sizeof($gone_out)!=0)
                                  {
                                      $minus_cond .= " AND a.UserID NOT IN (".implode(",",$gone_out).")";
                                  }
                                  if (sizeof($temp)==0)
                                  {
                                      $sql = "SELECT a.UserID
                                                     FROM INTRANET_USER as a
                                                          LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                                     WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                                           AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND LeaveStatus IS NULL $minus_cond";

                                  }
                                  else
                                  {
                                      $class_list = "'".implode("','",$temp)."'";
                                      $sql = "SELECT a.UserID
                                                     FROM INTRANET_USER as a
                                                          LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                                     WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                                                           AND b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND a.ClassName NOT IN ($class_list) AND LeaveStatus IS NULL $minus_cond";
                                  }
                              }
                              else
                              {
                                  $minus_cond = "";
                                  if (sizeof($back)!=0)
                                  {
                                      $minus_cond .= " AND b.UserID NOT IN (".implode(",",$back).")";
                                  }
                                  if (sizeof($gone_out)!=0)
                                  {
                                      $minus_cond .= " AND b.UserID NOT IN (".implode(",",$gone_out).")";
                                  }
                                  # Get from Lunch outers list
                                  $sql = "SELECT b.UserID
                                                 FROM CARD_STUDENT_LUNCH_ALLOW_LIST as a
                                                      LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                                                           AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
                                                      LEFT OUTER JOIN $card_log_table_name as c ON a.StudentID = c.UserID AND c.DayNumber = '$day'
                                                 WHERE b.UserID IS NOT NULL AND c.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LeaveStatus IS NULL OR LeaveStatus IN (0,2)) $minus_cond";
                              }
                              $rest = $this->returnVector($sql);
                              return array($gone_out, $back, $rest);
                         }
                         else if ($type == 2)
                         {
                                         //$sql = "SELECT DISTINCT UserID FROM $card_log_table_name
                              //               WHERE DayNumber = '$day' AND LeaveSchoolTime IS NOT NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") ) OR (LunchOutTime IS NOT NULL AND LunchBackTime IS NULL AND Daynumber = '$day')";
                              $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
                                                      WHERE DayNumber = '$day' AND
                                                                                (DayNumber = '$day' AND LeaveSchoolTime IS NOT NULL OR LeaveStatus IS NOT NULL) OR
                                                                                (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_OUTING.") AND LeaveStatus IS NULL) OR
                                                                                (DayNumber = '$day' AND AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") AND PMStatus IS NULL AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))";
                              $left = $this->returnVector($sql);
                              if(sizeof($left)>0)
                              {
                                      $list = implode(",",$left);
                                      $cond = " AND UserID NOT IN ($list)";
                              }
                              else
                              {
                                      $cond = " ";
                              }
                              //$sql = "SELECT DISTINCT UserID from $card_log_table_name WHERE DayNumber = '$day' AND (LeaveSchoolTime IS NOT NULL OR AMStatus IN (0,2) OR PMStatus IN (0,2)) AND UserID NOT IN ($list)";
                              $sql = "SELECT DISTINCT UserID FROM $card_log_table_name
                                             WHERE DayNumber = '$day' AND
                                             LeaveSchoolTime IS NULL AND
                                             LeaveStatus IS NULL AND
                                             (DayNumber = '$day' AND LeaveSchoolTime IS NULL AND (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ") OR PMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE. ")))
                                             $cond
                                             ";
                              $rest = $this->returnVector($sql);
                              return array($left,$rest);
                         }
                         else
                         {
                             return;
                         }
                }
           function getWordList($type)
           {
                    $content = trim(get_file_content($this->word_base_dir.$this->file_array[$type]));
                    $array = explode("\n",$content);
                    for ($i=0; $i<sizeof($array); $i++)
                    {
                         $array[$i] = trim($array[$i]);
                    }
                    return $array;
           }
           function getRecordYear()
           {
                    $sql = "SELECT DISTINCT Year FROM CARD_STUDENT_RECORD_DATE_STORAGE ORDER BY Year";
                    return $this->returnVector($sql);
           }

           function retrieveMonthAttendanceByClass ($ClassName, $year, $month)
           {
                   $sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$ClassName' AND RecordType = 2 AND RecordStatus IN (0,1)";
                   $temp = $this->returnVector($sql);
                   $list = implode(",", $temp);

                   # Get Daily Records
                   $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                   $sql = "SELECT DayNumber, AMStatus, PMStatus
                                                           FROM $card_log_table_name
                                                           WHERE UserID IN ($list)
                                                           ORDER BY DayNumber";

                                $log_entries = $this->returnArray($sql, 3);
                                for ($i=0; $i<sizeof($log_entries); $i++)
                                {
                                        list($day, $am, $pm) = $log_entries[$i];
                                        $result[$am]++;
                                        $result[$pm]++;
                                }
                                return $result;
           }

           function retrieveClassMonthData($ClassName, $year, $month)
           {
                    # Get Student List
                    $sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$ClassName' AND RecordType = 2 AND RecordStatus IN (0,1,2)";
                    $temp = $this->returnVector($sql);
                    $list = implode(",",$temp);
                    # Get Daily Records
                    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                    $sql = "SELECT UserID, DayNumber, AMStatus, PMStatus, LeaveStatus
                                   FROM $card_log_table_name
                                   WHERE UserID IN ($list)
                                   ORDER BY DayNumber";
                    if($list!="")
                                   $log_entries = $this->returnArray($sql,5);

                    for ($i=0; $i<sizeof($log_entries); $i++)
                    {
                         list ($studentid, $day, $am, $pm, $leave) = $log_entries[$i];
                         $result[$studentid][$day] = array($am,$pm,$leave);
                    }
                    return $result;
           }
           function retrieveClassDayData($ClassName, $year, $month, $day)
           {
                    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                    $namefield = getNameFieldByLang("a.");
                    $sql = "SELECT a.UserID, $namefield, a.ClassName, a.ClassNumber,
                                   b.InSchoolTime, b.InSchoolStation, b.AMStatus,
                                   b.LunchOutTime, b.LunchOutStation, b.LunchBackTime,
                                   b.LunchBackStation, b.PMStatus, b.LeaveSchoolTime,
                                   b.LeaveSchoolStation, b.LeaveStatus
                                   FROM INTRANET_USER as a
                                        LEFT OUTER JOIN $card_log_table_name as b ON a.UserID = b.UserID AND b.DayNumber = '$day'
                                   WHERE a.ClassName = '$ClassName' AND a.RecordType = 2
                                         AND a.RecordStatus IN (0,1,2)
                                   ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
                    return $this->returnArray($sql,15);
           }
           function retrieveBadRecordsCountByClass($StartDate, $EndDate, $BadType)
           {
                    # Get Student List By Class
                    $sql = "SELECT ClassName, UserID FROM INTRANET_USER
                                   WHERE ClassName != '' AND RecordType = 2 AND RecordStatus IN (0,1,2)
                                   ORDER BY ClassName";
                    $students = $this->returnArray($sql,2);

                    # Group students into class
                    for ($i=0; $i<sizeof($students); $i++)
                    {
                         list($classname, $uid) = $students[$i];
                         $classes[$classname][] = $uid;
                    }

                    # For each class to get
                    if (sizeof($classes) > 0)
                    foreach ($classes as $classname => $id_array)
                    {
                        # Get Record Count
                        $id_list = implode(",",$id_array);
                        $sql = "SELECT COUNT(*) FROM CARD_STUDENT_BAD_ACTION
                                       WHERE StudentID IN ($id_list)
                                             AND RecordDate >= '$StartDate'
                                             AND RecordDate <= '$EndDate'
                                             AND RecordType = '$BadType'
                                             ";
                        $temp = $this->returnVector($sql);
                        $record_count = $temp[0]+0;
                        $student_count = sizeof($id_array);
                        $result[] = array($classname,$student_count,$record_count);
                    }
                    return $result;
           }
           function retrieveTopBadRecordsCountByStudent($StartDate, $EndDate, $BadType, $TopNumber)
           {
                    # Get the count first
                    $sql = "SELECT StudentID, COUNT(RecordID) as num
                                   FROM CARD_STUDENT_BAD_ACTION
                                   WHERE RecordDate >= '$StartDate'
                                         AND RecordDate <= '$EndDate'
                                         AND RecordType = '$BadType'
                                   GROUP BY StudentID
                                   ORDER BY num DESC
                                   LIMIT $TopNumber
                                   ";
                    $temp = $this->returnArray($sql,2);
                    $record_count = $temp[sizeof($temp)-1][1];
                    if ($record_count == 0) return;

                    $namefield = getNameFieldByLang("b.");
                    $sql = "SELECT a.StudentID, $namefield, b.ClassName, b.ClassNumber, COUNT(a.RecordID) as num
                                   FROM CARD_STUDENT_BAD_ACTION as a
                                        LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
                                   WHERE a.RecordDate >= '$StartDate'
                                         AND a.RecordDate <= '$EndDate'
                                         AND a.RecordType = '$BadType'
                                   GROUP BY a.StudentID
                                   HAVING num >= $record_count
                                   ORDER BY num DESC, b.ClassName, b.ClassNumber, b.EnglishName
                                   ";
                    return $this->returnArray($sql,5);
           }
           function retrieveYearMonthList()
           {
                    $sql = "SELECT Year, Month FROM CARD_STUDENT_RECORD_DATE_STORAGE
                                   ORDER BY Year, Month";
                    return $this->returnArray($sql,2);
           }
           function removeMonthData($year, $month)
           {
                    # Drop tables
                    $sql = "DROP TABLE CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                    $this->db_db_query($sql);
                    $sql = "DROP TABLE CARD_STUDENT_DAILY_CLASS_CONFIRM_".$year."_".$month;
                    $this->db_db_query($sql);
                    $sql = "DELETE FROM CARD_STUDENT_DAILY_DATA_CONFIRM
                                   WHERE Year(RecordDate) = '$year' AND Month(RecordDate) = '$month'";
                    $this->db_db_query($sql);
                    $sql = "DELETE FROM CARD_STUDENT_RECORD_DATE_STORAGE WHERE Year = '$year' AND Month = '$month'";
                    $this->db_db_query($sql);
           }

           function retrieveStudentDailyRecord($student_id, $target_date="")
           {
                    if ($target_date == "")
                    {
                        $target_date = date('Y-m-d');
                        $target_year = date('Y');
                        $target_month = date('m');
                        $target_day = date('d');
                    }
                    else
                    {
                        $ts = strtotime($target_date);
                        $target_year = date('Y', $ts);
                        $target_month = date('m', $ts);
                        $target_day = date('d', $ts);
                    }

                    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$target_year."_".$target_month;
                    $sql = "SELECT
                                  IF(InSchoolTime IS NULL, '-', InSchoolTime),
                                  IF(InSchoolStation IS NULL, '-', InSchoolStation),
                                  IF(AMStatus IS NOT NULL,
                                     AMStatus,
                                     CASE
                                         WHEN InSchoolTime IS NOT NULL THEN '".CARD_STATUS_PRESENT."'
                                         ELSE '".CARD_STATUS_ABSENT."'
                                     END
                                  ),
                                  IF(LunchOutTime IS NULL, '-', LunchOutTime),
                                  IF(LunchOutStation IS NULL, '-', LunchOutStation),
                                  IF(LunchBackTime IS NULL, '-', LunchBackTime),
                                  IF(LunchBackStation IS NULL, '-', LunchBackStation),
                                  IF(PMStatus IS NOT NULL,
                                     PMStatus,
                                     CASE
                                         WHEN (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LunchOutTime IS NULL OR LunchBackTime IS NOT NULL))
                                         THEN '".CARD_STATUS_PRESENT."'
                                         ELSE '".CARD_STATUS_ABSENT."'
                                     END
                                  ),
                                  IF(LeaveSchoolTime IS NULL, '-', LeaveSchoolTime),
                                  LeaveStatus
                                  FROM $card_log_table_name
                                  WHERE UserID = '$student_id' AND DayNumber = '$target_day'
                    ";
                    $temp = $this->returnArray($sql,10);
                    if (sizeof($temp)==0) return false;
                    return $temp[0];
           }

           function retrieveStudentMonthlyRecord ($student_id, $year, $month, $order=1)
           {
                    $this->createTable_Card_Student_Daily_Log($year, $month);
                    $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                    $sql = "SELECT
                                  DayNumber,
                                  IF(InSchoolTime IS NULL, '-', InSchoolTime),
                                  IF(InSchoolStation IS NULL, '-', InSchoolStation),
                                  IF(AMStatus IS NOT NULL,
                                     AMStatus,
                                     CASE
                                         WHEN InSchoolTime IS NOT NULL THEN '".CARD_STATUS_PRESENT."'
                                         ELSE '".CARD_STATUS_ABSENT."'
                                     END
                                  ),
                                  IF(LunchOutTime IS NULL, '-', LunchOutTime),
                                  IF(LunchOutStation IS NULL, '-', LunchOutStation),
                                  IF(LunchBackTime IS NULL, '-', LunchBackTime),
                                  IF(LunchBackStation IS NULL, '-', LunchBackStation),
                                  IF(PMStatus IS NOT NULL,
                                     PMStatus,
                                     CASE
                                         WHEN (AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND (LunchOutTime IS NULL OR LunchBackTime IS NOT NULL))
                                         THEN '".CARD_STATUS_PRESENT."'
                                         ELSE '".CARD_STATUS_ABSENT."'
                                     END
                                  ),
                                  IF(LeaveSchoolTime IS NULL, '-', LeaveSchoolTime),
                                  LeaveStatus
                                  FROM $card_log_table_name
                                  WHERE UserID = '$student_id'
                                  ORDER BY DayNumber
                    ";
                    if (!$order) $sql .= " DESC";
                    return $this->returnArray($sql,11);
           }
           function disallowInputInProfile(){
                                   global $intranet_root;
                                        $disallow_input_attendance_record = trim(get_file_content("$intranet_root/file/disallow_student_profile_input.txt"));
                                         $this->disallow_input_in_profile = $disallow_input_attendance_record;
                                         return $this->disallow_input_in_profile;
               }

    /*
        * Get MODULE_OBJ array
        */
        function GET_MODULE_OBJ_ARR(){

                global $PATH_WRT_ROOT, $ip20TopMenu, $CurrentPage, $LAYOUT_SKIN;

                # Menu Item Wordings
                global $i_SmartCard_SystemSettings, $i_general_BasicSettings, $i_SmartCard_TerminalSettings, $i_StudentAttendance_TimeSessionSettings, $i_StudentAttendance_TimeSlotSettings, $i_StudentAttendance_LunchSettings;
                global $i_StudentAttendance_Menu_ResponsibleAdmin,$i_SmartCard_Responsible_Admin_Class;
                global $i_StudentAttendance_Menu_DailyOperation,$i_SmartCard_DailyOperation_ViewClassStatus,$i_SmartCard_DailyOperation_ViewLateStatus,$i_SmartCard_DailyOperation_ViewAbsenceStatus,$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus,$i_SmartCard_DailyOperation_ViewLeftStudents, $i_SmartCard_DailyOperation_ImportOfflineRecords, $i_SmartCard_DailyOperation_ViewEntryLog, $i_SmartCard_DailyOperation_Preset_Absence;
                global $i_StudentAttendance_Menu_DataManagement,$i_StudentAttendance_Menu_DataMgmt_DataClear,$i_StudentAttendance_Menu_DataMgmt_BadLogs;
                global $i_StudentAttendance_Menu_OtherFeatures,$i_StudentAttendance_Menu_OtherFeatures_Outing,$i_StudentAttendance_Menu_OtherFeatures_Detention, $i_StudentAttendance_Menu_OtherFeatures_Reminder;
                global $i_StudentAttendance_Menu_DataExport, $i_StudentAttendance_Reminder, $i_StudentAttendance_StudentInformation, $i_StudentAttendance_OR_ExportStudentInfo;
                global $i_StudentAttendance_Menu_DataImport,$i_StudentAttendance_ImportCardID;
                global $i_StudentAttendance_Menu_Report, $i_StudentAttendance_Report_ClassMonth, $i_StudentAttendance_Report_ClassDaily, $i_StudentAttendance_Report_ClassBadRecords, $i_StudentAttendance_Report_StudentBadRecords, $button_find;

                # Current Page Information init
                $PageDailyOperation = 0;
                $PageDailyOperation_ViewClassStatus = 0;
                $PageDailyOperation_ViewLateStatus = 0;
                $PageDailyOperation_ViewAbsenceStatus = 0;
                $PageDailyOperation_ViewEarlyLeaveStatus = 0;
                $PageDailyOperation_ViewLeftStudents = 0;
                $PageDailyOperation_ImportOfflineRecords = 0;
                $PageDailyOperation_ViewEntryLog = 0;
                $PageDailyOperation_PresetAbsence = 0;

                $PageDataManagement = 0;
                $PageDataManagement_DataClear = 0;
                $PageDataManagement_BadLogs = 0;

                $PageOtherFeatures = 0;
                $PageOtherFeatures_Outing = 0;
                $PageOtherFeatures_Detention = 0;
                $PageOtherFeatures_Reminder = 0;

                $PageDataExport = 0;
                $PageDataExport_Reminder = 0;

                $PageDataImport = 0;
                $PageDataImport_CardID= 0;

                $PageReport = 0;
                $PageReport_ClassMonth = 0;
                $PageReport_ClassDaily = 0;
                $PageReport_ClassBadRecords = 0;
                $PageReport_StudentBadRecords = 0;
                $PageReport_Search = 0;

                $PageSystemSetting = 0;
                $PageSystemSetting_BasicSettings = 0;
                $PageSystemSetting_TerminalSettings = 0;
                # Either one: Slot or Session
                $PageSystemSetting_TimeSessionSettings = 0;
                $PageSystemSetting_TimeSlotSettings = 0;
                $PageSystemSetting_LunchSettings = 0;

                $PageAccountResponsibleAdmin = 0;
                $PageAccountResponsibleAdmin_ClassRepresentative = 0;

                switch ($CurrentPage) {
                        case "PageDailyOperation_ViewClassStatus":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ViewClassStatus = 1;
                                break;
                        case "PageDailyOperation_ViewLateStatus":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ViewLateStatus = 1;
                                break;
                        case "PageDailyOperation_ViewAbsenceStatus":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ViewAbsenceStatus = 1;
                                break;
                        case "PageDailyOperation_ViewEarlyLeaveStatus":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ViewEarlyLeaveStatus = 1;
                                break;
                        case "PageDailyOperation_ViewLeftStudents":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ViewLeftStudents = 1;
                                break;
                        case "PageDailyOperation_ImportOfflineRecords":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ImportOfflineRecords = 1;
                                break;
                        case "PageDailyOperation_ViewEntryLog":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_ViewEntryLog = 1;
                                break;
                        case "PageDailyOperation_PresetAbsence":
                                $PageDailyOperation= 1;
                                $PageDailyOperation_PresetAbsence = 1;
                                break;
                        case "PageDataManagement_DataClear":
                                $PageDataManagement = 1;
                                $PageDataManagement_DataClear = 1;
                                break;
                        case "PageDataManagement_BadLogs":
                                $PageDataManagement = 1;
                                $PageDataManagement_BadLogs = 1;
                                break;
                        case "PageOtherFeatures_Outing":
                                $PageOtherFeatures = 1;
                                $PageOtherFeatures_Outing = 1;
                                break;
                        case "PageOtherFeatures_Detention":
                                $PageOtherFeatures = 1;
                                $PageOtherFeatures_Detention = 1;
                                break;
                        case "PageOtherFeatures_Reminder":
                                $PageOtherFeatures = 1;
                                $PageOtherFeatures_Reminder = 1;
                                break;
                        case "PageDataExport_Reminder":
                                $PageDataExport = 1;
                                $PageDataExport_Reminder = 1;
                                break;
                        case "PageDataImport_CardID":
                                $PageDataImport = 1;
                                $PageDataImport_CardID = 1;
                                break;
                        case "PageReport_ClassMonth":
                                $PageReport = 1;
                                $PageReport_ClassMonth = 1;
                                break;
                        case "PageReport_ClassDaily":
                                $PageReport = 1;
                                $PageReport_ClassDaily = 1;
                                break;
                        case "PageReport_ClassBadRecords":
                                $PageReport = 1;
                                $PageReport_ClassBadRecords = 1;
                                break;
                        case "PageReport_StudentBadRecords":
                                $PageReport = 1;
                                $PageReport_StudentBadRecords = 1;
                                break;
                        case "PageReport_Search":
                                $PageReport = 1;
                                $PageReport_Search = 1;
                                break;
                        case "PageSystemSetting_BasicSettings":
                                $PageSystemSetting = 1;
                                $PageSystemSetting_BasicSettings = 1;
                                break;
                        case "PageSystemSetting_TerminalSettings":
                                $PageSystemSetting = 1;
                                $PageSystemSetting_TerminalSettings = 1;
                                break;
                        case "PageSystemSetting_TimeSessionSettings":
                                $PageSystemSetting = 1;
                                $PageSystemSetting_TimeSessionSettings = 1;
                                break;
                        case "PageSystemSetting_TimeSlotSettings":
                                $PageSystemSetting = 1;
                                $PageSystemSetting_TimeSlotSettings = 1;
                                break;
                        case "PageSystemSetting_LunchSettings":
                                $PageSystemSetting = 1;
                                $PageSystemSetting_LunchSettings = 1;
                                break;
                        case "PageAccountResponsibleAdmin_ClassRepresentative":
                                $PageAccountResponsibleAdmin = 1;
                                $PageAccountResponsibleAdmin_ClassRepresentative = 1;
                                break;
                }

                # Menu information
                $MenuArr["DailyOperation"] = array($i_StudentAttendance_Menu_DailyOperation, "", $PageDailyOperation);
                $MenuArr["DailyOperation"]["Child"]["ViewClassStatus"] = array($i_SmartCard_DailyOperation_ViewClassStatus, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/class/", $PageDailyOperation_ViewClassStatus);
                $MenuArr["DailyOperation"]["Child"]["ViewLateStatus"] = array($i_SmartCard_DailyOperation_ViewLateStatus, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/late/", $PageDailyOperation_ViewLateStatus);
                $MenuArr["DailyOperation"]["Child"]["ViewAbsenceStatus"] = array($i_SmartCard_DailyOperation_ViewAbsenceStatus, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/absence", $PageDailyOperation_ViewAbsenceStatus);
                $MenuArr["DailyOperation"]["Child"]["ViewEarlyLeaveStatus"] = array($i_SmartCard_DailyOperation_ViewEarlyLeaveStatus, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/early/", $PageDailyOperation_ViewEarlyLeaveStatus);
                $MenuArr["DailyOperation"]["Child"]["ViewLeftStudents"] = array($i_SmartCard_DailyOperation_ViewLeftStudents, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/left/summary.php", $PageDailyOperation_ViewLeftStudents);
                $MenuArr["DailyOperation"]["Child"]["ImportOfflineRecords"] = array($i_SmartCard_DailyOperation_ImportOfflineRecords, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/import/import.php", $PageDailyOperation_ImportOfflineRecords);
                $MenuArr["DailyOperation"]["Child"]["ViewEntryLog"] = array($i_SmartCard_DailyOperation_ViewEntryLog, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/entry_log/", $PageDailyOperation_ViewEntryLog);
                $MenuArr["DailyOperation"]["Child"]["PresetAbsence"] = array($i_SmartCard_DailyOperation_Preset_Absence, $PATH_WRT_ROOT."home/admin/attendance/dailyoperation/preset_absence/new.php", $PageDailyOperation_PresetAbsence);

                $MenuArr["DataManagement"] = array($i_StudentAttendance_Menu_DataManagement, "", $PageDataManagement);
                $MenuArr["DataManagement"]["Child"]["DataClear"] = array($i_StudentAttendance_Menu_DataMgmt_DataClear, $PATH_WRT_ROOT."home/admin/attendance/datamgmt/dataclear/", $PageDataManagement_DataClear);
                $MenuArr["DataManagement"]["Child"]["BadLogs"] = array($i_StudentAttendance_Menu_DataMgmt_BadLogs, $PATH_WRT_ROOT."home/admin/attendance/datamgmt/badlogs/browse.php?type=1", $PageDataManagement_BadLogs);

                $MenuArr["OtherFeatures"] = array($i_StudentAttendance_Menu_OtherFeatures, "", $PageOtherFeatures);
                $MenuArr["OtherFeatures"]["Child"]["Outing"] = array($i_StudentAttendance_Menu_OtherFeatures_Outing, $PATH_WRT_ROOT."home/admin/attendance/other_features/outing/", $PageOtherFeatures_Outing);
                $MenuArr["OtherFeatures"]["Child"]["Detention"] = array($i_StudentAttendance_Menu_OtherFeatures_Detention, $PATH_WRT_ROOT."home/admin/attendance/other_features/detention/", $PageOtherFeatures_Detention);
                $MenuArr["OtherFeatures"]["Child"]["Reminder"] = array($i_StudentAttendance_Menu_OtherFeatures_Reminder, $PATH_WRT_ROOT."home/admin/attendance/other_features/reminder/", $PageOtherFeatures_Reminder);

                $MenuArr["DataExport"] = array($i_StudentAttendance_Menu_DataExport, "", $PageDataExport);
                $MenuArr["DataExport"]["Child"]["StudentInformation"] = array($i_StudentAttendance_StudentInformation, $PATH_WRT_ROOT."home/admin/attendance/data_export/student.php", $PageDataExport_StudentInformation);
                $MenuArr["DataExport"]["Child"]["Reminder"] = array($i_StudentAttendance_Reminder, $PATH_WRT_ROOT."home/admin/attendance/data_export/reminder.php", $PageDataExport_Reminder);
                $MenuArr["DataExport"]["Child"]["ExportStudent"] = array($i_StudentAttendance_OR_ExportStudentInfo, $PATH_WRT_ROOT."home/admin/attendance/data_export/studentInfo_offline_reader_export_950e.php", $PageDataExport_ExportStudent);

                $MenuArr["DataImport"] = array($i_StudentAttendance_Menu_DataImport, "", $PageDataImport);
                $MenuArr["DataImport"]["Child"]["CardID"] = array($i_StudentAttendance_ImportCardID, $PATH_WRT_ROOT."home/admin/attendance/data_import/studentcard.php", $PageDataImport_CardID);

                $MenuArr["Report"] = array($i_StudentAttendance_Menu_Report, "", $PageReport);
                $MenuArr["Report"]["Child"]["ClassMonth"] = array($i_StudentAttendance_Report_ClassMonth, $PATH_WRT_ROOT."home/admin/attendance/report/class_month_full.php", $PageReport_ClassMonth);
                $MenuArr["Report"]["Child"]["ClassDaily"] = array($i_StudentAttendance_Report_ClassDaily, $PATH_WRT_ROOT."home/admin/attendance/report/class_day.php", $PageReport_ClassDaily);
                $MenuArr["Report"]["Child"]["ClassBadRecords"] = array($i_StudentAttendance_Report_ClassBadRecords, $PATH_WRT_ROOT."home/admin/attendance/report/class_bad.php", $PageReport_ClassBadRecords);
                $MenuArr["Report"]["Child"]["StudentBadRecords"] = array($i_StudentAttendance_Report_StudentBadRecords, $PATH_WRT_ROOT."home/admin/attendance/report/top_bad.php", $PageReport_StudentBadRecords);
                $MenuArr["Report"]["Child"]["Search"] = array($button_find, $PATH_WRT_ROOT."home/admin/attendance/report/search_report.php", $PageReport_Search);

                $MenuArr["SystemSetting"] = array($i_SmartCard_SystemSettings, "", $PageSystemSetting);
                $MenuArr["SystemSetting"]["Child"]["BasicSettings"] = array($i_general_BasicSettings, $PATH_WRT_ROOT."home/admin/attendance/settings/basic/", $PageSystemSetting_BasicSettings);
                $MenuArr["SystemSetting"]["Child"]["TerminalSettings"] = array($i_SmartCard_TerminalSettings, $PATH_WRT_ROOT."home/admin/attendance/settings/terminal/", $PageSystemSetting_TerminalSettings);

                $time_table_mode = $this->retrieveTimeTableMode();
                if (($time_table_mode == 0)||($time_table_mode == "")) {
                        $MenuArr["SystemSetting"]["Child"]["TimeSlotSettings"] = array($i_StudentAttendance_TimeSlotSettings, $PATH_WRT_ROOT."home/admin/attendance/settings/slot/school/", $PageSystemSetting_TimeSlotSettings);
                }
                if ($time_table_mode == 1) {
                        $MenuArr["SystemSetting"]["Child"]["TimeSessionSettings"] = array($i_StudentAttendance_TimeSessionSettings, $PATH_WRT_ROOT."home/admin/attendance/settings/slot_session/session_setting/", $PageSystemSetting_TimeSessionSettings);
                }
                $MenuArr["SystemSetting"]["Child"]["LunchSettings"] = array($i_StudentAttendance_LunchSettings, $PATH_WRT_ROOT."home/admin/attendance/settings/lunch/list/", $PageSystemSetting_LunchSettings);

                $MenuArr["AccountResponsibleAdmin"] = array($i_StudentAttendance_Menu_ResponsibleAdmin, "", $PageAccountResponsibleAdmin);
                $MenuArr["AccountResponsibleAdmin"]["Child"]["ClassRepresentative"] = array($i_SmartCard_Responsible_Admin_Class, $PATH_WRT_ROOT."home/admin/attendance/admin/class/list/", $PageAccountResponsibleAdmin_ClassRepresentative);

                # module information
                $MODULE_OBJ['title'] = $ip20TopMenu['eAttendance'];
                $MODULE_OBJ['logo'] = $PATH_WRT_ROOT."images/{$LAYOUT_SKIN}/leftmenu/icon_eAttendance.gif";
                $MODULE_OBJ['menu'] = $MenuArr;

                return $MODULE_OBJ;
        }


        function createTable_Card_Student_Lunch_Box_Option($year="",$month="")
        {
                 $year=($year=="")?date("Y"):$year;
                 $month=($month=="")?date("m"):$month;
                 $tablename = "CARD_STUDENT_ATTENDANCE_LIST_LUNCHBOX_OPTION_".$year."_".$month;

                 $sql = "CREATE TABLE IF NOT EXISTS $tablename (
                                RecordID int(11) NOT NULL auto_increment,
                                StudentID int(11) NOT NULL,
                                DayNumber int(11) NOT NULL,
                                CancelOption int(11) default 0,
                                RecordType int,
                                RecordStatus int,
                                DateInput datetime,
                                DateModified datetime,
                                PRIMARY KEY (RecordID),
                                UNIQUE StudentDay (StudentID, DayNumber),
                                INDEX StudentID (StudentID),
                                INDEX DayNumber (DayNumber)
                        )";
                 $this->db_db_query($sql);
                 return $tablename;

        }

        function generateMedicalReasonSummary($TargetDate)
        {
                 global $config_school_type, $BroadlearningClientName;

                 if ($config_school_type=="P")
                 {
                     $available_level = array("P1","P2","P3","P4","P5","P6");
                 }
                 else if ($config_school_type=="S")
                 {
                      $available_level = array("S1","S2","S3","S4","S5","S6","S7");
                 }
                 else
                 {
                     $available_level = array("P1","P2","P3","P4","P5","P6",
                                        "S1","S2","S3","S4","S5","S6","S7");
                 }

                 $ts = strtotime($TargetDate);
                 $year = date("Y",$ts);
                 $month = date("m",$ts);
                 $day = date("d",$ts);

                 # Retrieve Data
                 if ($this->attendance_mode=="NO")
                 {
                     $this->retrieveSettings();
                 }
                 # Retrieve Class Level List
                 $sql = "SELECT ClassName, WebSAMSLevel
                               FROM INTRANET_CLASS as a
                                    LEFT OUTER JOIN INTRANET_CLASSLEVEL as b ON a.ClassLevelID = b.ClassLevelID";
                 $classes = $this->returnArray($sql,2);
                 unset($class_data);
                 unset($result_count_total);
                 unset($result_data);
                 for ($i=0; $i<sizeof($classes); $i++)
                 {
                      list($t_classname, $t_webSamsClassLevel) = $classes[$i];
                      if (in_array($t_webSamsClassLevel, $available_level))
                      {
                          $class_data[$t_webSamsClassLevel][] = $t_classname;
                      }
                      else
                      {
                          $other_class[] = $t_classname;
                      }
                 }

#                 if ($this->attendance_mode==0 || $this->attendance_mode==1) # AM Only or PM Only
#                 {
                 foreach ($class_data as $t_webSamsLevel => $t_classArray)
                 {
                     $t_class_list = "'".implode("','",$t_classArray)."'";
                     $sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus IN (0,1,2) AND ClassName IN ($t_class_list)";
                     $temp = $this->returnVector($sql);
                     $result_count_total[$t_webSamsLevel] = $temp[0];
                 }
                 # Get a list of absence
                 $sql = "SELECT user.UserID, level.WebSAMSLevel, day_log.AMStatus, am_reason.MedicalReasonType,
                                    day_log.PMStatus, pm_reason.MedicalReasonType
                                    FROM CARD_STUDENT_DAILY_LOG_".$year."_".$month." as day_log
                                         LEFT OUTER JOIN INTRANET_USER as user ON day_log.UserID = user.UserID
                                         LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as am_reason
                                              ON day_log.UserID = am_reason.StudentID
                                                 AND am_reason.RecordDate = '$TargetDate'
                                                 AND am_reason.DayType = '".PROFILE_DAY_TYPE_AM."'
                                         LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as pm_reason
                                              ON day_log.UserID = pm_reason.StudentID
                                                 AND pm_reason.RecordDate = '$TargetDate'
                                                 AND pm_reason.DayType = '".PROFILE_DAY_TYPE_PM."'
                                         LEFT OUTER JOIN INTRANET_CLASS as class ON user.ClassName = class.ClassName
                                         LEFT OUTER JOIN INTRANET_CLASSLEVEL as level ON level.ClassLevelID = class.ClassLevelID
                                    WHERE day_log.DayNumber = '$day'
                                          AND (day_log.AMStatus='".CARD_STATUS_ABSENT."' OR day_log.PMStatus='".CARD_STATUS_ABSENT."')
                             ";
				
                 $abs_data = $this->returnArray($sql,6);
                 for ($i=0; $i<sizeof($abs_data); $i++)
                 {
                      list($t_userID, $t_webSamsLevel, $t_amStatus, $t_amReason, $t_pmStatus, $t_pmReason) = $abs_data[$i];
                      if ($this->attendance_mode==0)
                      {
                          if ($t_amStatus == CARD_STATUS_ABSENT)
                          {
                              if ($t_amReason == 1)                              # ILI/Resp
                              {
                                  $result_data[$t_webSamsLevel][1]++;
                              }
                              else if ($t_amReason == 2)                         # GI illness
                              {
                                  $result_data[$t_webSamsLevel][2]++;
                              }
                              else if ($t_amReason == 99)                        # Other illness
                              {
                                  $result_data[$t_webSamsLevel][99]++;
                              }
                              else if ($t_amReason == 999)                       # Not illness
                              {
                                  $result_data[$t_webSamsLevel][999]++;
                              }
                              else                                               # Undefined
                              {
                                  $result_data[$t_webSamsLevel][0]++;
                              }
                          }
                      }
                      else if ($this->attendance_mode==1)
                      {
                          if ($t_pmStatus == CARD_STATUS_ABSENT)
                          {
                              if ($t_pmReason == 1)                              # ILI/Resp
                              {
                                  $result_data[$t_webSamsLevel][1]++;
                              }
                              else if ($t_pmReason == 2)                         # GI illness
                              {
                                  $result_data[$t_webSamsLevel][2]++;
                              }
                              else if ($t_pmReason == 99)                        # Other illness
                              {
                                  $result_data[$t_webSamsLevel][99]++;
                              }
                              else if ($t_pmReason == 999)                       # Not illness
                              {
                                  $result_data[$t_webSamsLevel][999]++;
                              }
                              else                                               # Undefined
                              {
                                  $result_data[$t_webSamsLevel][0]++;
                              }
                          }
                      }
                      else if ($this->attendance_mode==2 || $this->attendance_mode==3)
                      {
                          if ($t_amStatus == CARD_STATUS_ABSENT && ($t_amReason != 0) && ($t_amReason != "") )  # AM Absent and with medical reason
                          {
	                          //echo "Part 1";
                              if ($t_amReason == 1)                              # ILI/Resp
                              {
                                  $result_data[$t_webSamsLevel][1]++;
                              }
                              else if ($t_amReason == 2)                         # GI illness
                              {
                                  $result_data[$t_webSamsLevel][2]++;
                              }
                              else if ($t_amReason == 99)                        # Other illness
                              {
                                  $result_data[$t_webSamsLevel][99]++;
                              }
                              else if ($t_amReason == 999)                       # Not illness
                              {
                                  $result_data[$t_webSamsLevel][999]++;
                              }
                              else                                               # Undefined
                              {
                                  $result_data[$t_webSamsLevel][0]++;
                              }
                          }
                          else if ($t_pmStatus==CARD_STATUS_ABSENT)  # PM Absent (As AM has not been set, just use PM medical reason)
                          {
	                          //echo "Part 2";
                              if ($t_pmReason == 1)                              # ILI/Resp
                              {
                                  $result_data[$t_webSamsLevel][1]++;
                              }
                              else if ($t_pmReason == 2)                         # GI illness
                              {
                                  $result_data[$t_webSamsLevel][2]++;
                              }
                              else if ($t_pmReason == 99)                        # Other illness
                              {
                                  $result_data[$t_webSamsLevel][99]++;
                              }
                              else if ($t_pmReason == 999)                       # Not illness
                              {
                                  $result_data[$t_webSamsLevel][999]++;
                              }
                              else                                               # Undefined
                              {
                                  $result_data[$t_webSamsLevel][0]++;
                              }
                          }
                          else if ($t_amStatus == CARD_STATUS_ABSENT) # AM Absent only, but no medical reason set
                          {
	                          //echo "Part 3";
                              $result_data[$t_webSamsLevel][0]++;
                          }
                          /*
                          # Check AM Reason is "Undefined" but PM Reason is Changed #
                          if (($t_amStatus == CARD_STATUS_ABSENT) && ($t_pmStatus == CARD_STATUS_ABSENT) && ($t_amReason == 0) && ($t_pmReason != 0))
	                      {
		                      if ($t_pmReason == 1)                              # ILI/Resp
                              {
                                  $result_data[$t_webSamsLevel][1]++;
                              }
                              else if ($t_pmReason == 2)                         # GI illness
                              {
                                  $result_data[$t_webSamsLevel][2]++;
                              }
                              else if ($t_pmReason == 99)                        # Other illness
                              {
                                  $result_data[$t_webSamsLevel][99]++;
                              }
                              else if ($t_pmReason == 999)                       # Not illness
                              {
                                  $result_data[$t_webSamsLevel][999]++;
                              }
                              else                                               # Undefined
                              {
                                  $result_data[$t_webSamsLevel][0]++;
                              }
                              $result_data[$t_webSamsLevel][0]--;				 # del 1 from the total no. of "Undefined"
	                      }
	                      */

                      }

                 }
                 # Combine to csv
                 $content = "";
                 $title_row = "\"Date\",\"Name of school\",\"Primary/Secondary\",\"Year/Form\",\"ILI/ resp illness\",\"GI illness\",\"Other illness\",\"Not illness\",\"Undefined\",\"Total no. of registered students\"";
                 $content = $title_row;

                 for ($i=0; $i<sizeof($available_level); $i++)
                 {
                      $content .= "\r\n";
                      $content .= "\"".$TargetDate."\",\"".$BroadlearningClientName."\"";
                      # Primary / Secondary
                      $sch_level = substr($available_level[$i], 0,1);
                      $form_level = substr($available_level[$i],1,1);
                      $content .= ",\"".$sch_level."\",\"".$form_level."\"";
                      $content .= ",\"".$result_data[$available_level[$i]][1]."\"";
                      $content .= ",\"".$result_data[$available_level[$i]][2]."\"";
                      $content .= ",\"".$result_data[$available_level[$i]][99]."\"";
                      $content .= ",\"".$result_data[$available_level[$i]][999]."\"";
                      $content .= ",\"".$result_data[$available_level[$i]][0]."\"";
                      $content .= ",\"".$result_count_total[$available_level[$i]]."\"";
                 }

                 return $content;
        }

        function sendMedicalReport($TargetDate)
        {
                 global $sys_custom_config, $BroadlearningClientName;

                 if ($sys_custom_config['hku_medical_research']['email']=="")
                 {
                     return false;
                 }
                 # Retrieve data content
                 $content = $this->generateMedicalReasonSummary($TargetDate);

                 # Prepare send email

                 # Fixed parameters
                 $type="application/octet-stream";
                 $priority = 1;
                 $charset = "big5";
                 $from = "support@broadlearning.com";
                 $to = $sys_custom_config['hku_medical_research']['email'];
                 $reply_address = $from;
                 $mime_boundary = "<<<:" . md5(uniqid(mt_rand(), 1));       # Boundary for multi-part
                 $fname = "attendance_report_".$TargetDate.".csv";

                 # Subject & Message
                 $subject = "School Attendance Report [$BroadlearningClientName - $TargetDate]";
                 $message = "Attachment";

                 # Mail Header
                 $headers='';
                 $headers .= "MIME-Version: 1.0\r\n";
                 $headers .= "X-Priority: $priority\r\n";
                 $headers .= "Content-Type: multipart/mixed; ";
                 $headers .= " boundary=\"" . $mime_boundary . "\"\r\n";

                 $headers .= "From: $from\r\n";
                 if ($reply_address != "")
                 {
                     $headers .= "Reply-To: $reply_address\r\n";
                 }
                 $mime = "This is a multi-part message in MIME format.\r\n";
                 $mime .= "\r\n";
                 $mime .= "--" . $mime_boundary . "\r\n";

                 # Message Body
                 $mime .= "Content-Transfer-Encoding: 7bit\r\n";
                 $mime .= "Content-Type: text/plain; charset=\"$charset\"\r\n";
                 $mime .= "\r\n";

                 $mime .= $message;
                 $mime .= "\r\n\r\n";
                 $mime .= "--" . $mime_boundary. "\r\n";

                 # Attachment
                 $mime .= "Content-Type: $type;\r\n";
                 #$mime .= "\tname=\"".base64_encode($fname)."\"\r\n";
                 $mime .= "\tname=\"".$fname."\"\r\n";
                 $mime .= "Content-Transfer-Encoding: base64\r\n";
                 $mime .= "Content-Disposition: attachment;\r\n ";
                 $mime .= "\tfilename=\"".$fname."\"\r\n\r\n";
                 #$mime .= "\tfilename=\"".base64_encode($fname)."\"\r\n\r\n";
                 $mime .= chunk_split(base64_encode($content));
                 $mime .= "\r\n\r\n";
                 $mime .= "--" . $mime_boundary;
                 $mime .= "--\r\n";

                 $result = mail($to, $subject, $mime, $headers, "-f $from");
                 return $result;


        }


 }


} // End of directives

intranet_opendb();
$lclass = new libclass();

$lc = new libcardstudentattend2();
$lc->retrieveSettings();
//$lc->attendance_mode;
$hasAM = ($lc->attendance_mode != 1);
$hasLunch = ($lc->attendance_mode != 0 && $lc->attendance_mode != 1);
$hasPM = ($lc->attendance_mode != 0);
$classname = $lc->getClassName($ClassID);

$normal_warning = 0;
$weekly_warning = 0;
$cycle_warning = 0;
$special_warning = 0;

### Weekday Table ###
### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

$result = $lc->getSessionTimeArrayList(1,$ClassID);

$weekday_table_content = "";    
$weekday_table_content .= "<tr><td class=tableTitle>$i_BookingType_Weekdays</td>";
if($hasAM)
	$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$weekday_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($s_id, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_value) = $result[$i];
		$weekday = $i_DayType0[$day_value];
		$editlink = "<a class=functionlink href=class_session_edit.php?type=1&ClassID=$ClassID&value=$day_value><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(1,$ClassID,'$day_value')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$weekday_table_content .= "<tr bgcolor=red><td>$weekday $editlink</td>";
			$weekly_warning = 1;
		}
		else
		{
			$weekday_table_content .= "<tr><td>$weekday $editlink</td>";
		}
		if($non_school_day==1){
			$weekday_table_content .= "<td colspan=5>$i_StudentAttendance_NonSchoolDay</td>";
		}
		else
		{
			if($hasAM)
				$weekday_table_content .= "<td>$am_time</td>";
			if($hasLunch)
				$weekday_table_content .= "<td>$lunch_time</td>";
			if($hasPM)
				$weekday_table_content .= "<td>$pm_time</td>";
			$weekday_table_content .= "<td>$leave_school_time</td>";		
		}	
	}
}
if(sizeof($result)==0)
{
	$weekday_table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Weekday Table ###

### Cycle Day Table ###
### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";
				
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

$result = $lc->getSessionTimeArrayList(2,$ClassID);
$cycle_table_content = "";    
$cycle_table_content .= "<tr><td class=tableTitle>$i_BookingType_Cycleday</td>";
if($hasAM)
	$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$cycle_table_content .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($s_id, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_value) = $result[$i];
		$cycleday = $day_value;
		$editlink = "<a class=functionlink href=class_session_edit.php?type=2&ClassID=$ClassID&value=$cycleday><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeDaySetting(2,$ClassID,'$day_value')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$cycle_table_content .= "<tr bgcolor=red><td>$cycleday $editlink</td>";
			$cycle_warning = 1;
		}
		else
		{
			$cycle_table_content .= "<tr><td>$cycleday $editlink</td>";
		}
		if($non_school_day==1){
                 $cycle_table_content .="<tr><td colspan=4>$i_StudentAttendance_NonSchoolDay</td></tr>";
        }
        else
        {
			//$cycle_table_content .= "<tr><td>$cycleday $editlink</td>";
			if($hasAM)
				$cycle_table_content .= "<td>$am_time</td>";
			if($hasLunch)
				$cycle_table_content .= "<td>$lunch_time</td>";
			if($hasPM)
				$cycle_table_content .= "<td>$pm_time</td>";
			$cycle_table_content .= "<td>$leave_school_time</td></tr>";
		}
	}
}
if(sizeof($result)==0)
{
	$cycle_table_content .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Cycle Day Table ###

### Special Day Table ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";
				
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

$sql = "SELECT 
				a.RecordID, b.SessionID, b.SessionName, a.RecordDate ,TIME_FORMAT(b.MorningTime,'%H:%i'), TIME_FORMAT(b.LunchStart,'%H:%i'), TIME_FORMAT(b.LunchEnd,'%H:%i'), TIME_FORMAT(b.LeaveSchoolTime,'%H:%i'), b.NonSchoolDay
		FROM 
				CARD_STUDENT_TIME_SESSION_DATE AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION AS b ON (a.SessionID = b.SessionID)
		WHERE
				ClassID = $ClassID
		";
echo "special --><br>".$sql."<br><111--<br>";
$temp = $lclass->returnArray($sql,9);

$special_day_table = " ";
$special_day_table = "<tr><td class=tabletitle>$i_StudentAttendance_Slot_Special</td>";
if($hasAM)
	$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_MorningTime</td>";
if($hasLunch)
	$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchStartTime</td>";
if($hasPM)
	$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LunchEndTime</td>";
$special_day_table .= "<td class=tableTitle>$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime</td></tr>";

if(sizeof($temp)>0)
{	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($record_id, $s_id, $s_name, $record_date, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day) = $temp[$i];
		$special_date = $record_date;
		$editlink = "<a class=functionlink href=class_session_edit.php?type=3&ClassID=$ClassID&value=$special_date><img src=\"$image_path/icon_edit.gif\" border=0></a>";
		$editlink .= "<a class=functionlink href=\"javascript:removeSpecialDaySetting($ClassID,'$special_date')\"><img src=\"$image_path/icon_erase.gif\" border=0></a>";
		if(in_array($s_id,$InactiveSession))
		{
			$special_day_table .= "<tr bgcolor=red><td>$record_date $editlink</td>";
			$special_warning = 1;
		}
		else
		{
			$special_day_table .= "<tr><td>$record_date $editlink</td>";
		}
		//$special_day_table .= "<tr><td>$record_date $editlink</td>";
		if($hasAM)
			$special_day_table .= "<td>$am_time</td>";
		if($hasLunch)
			$special_day_table .= "<td>$lunch_time</td>";
		if($hasPM)
			$special_day_table .= "<td>$pm_time</td>";
		$special_day_table .= "<td>$leave_school_time</td>";
	}
}
if(sizeof($temp)==0)
{
	$special_day_table .= "<tr><td colspan=7 align=center>$i_no_record_exists_msg</td></tr>";
}
### End Of Special Day Table ###

### Start Of Normal Table ###
### Get The Current Using Session ###
$sql = "SELECT
				SessionID
		FROM
				CARD_STUDENT_TIME_SESSION_REGULAR
		WHERE
				ClassID = $ClassID AND DayType = 0 AND DayValue = 0
		ORDER BY
				SessionID";
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	list($using_session) = $result[0];
}

### Get The Inactive Session BUT Using By Class ###
$InactiveSession = array();
$sql = "SELECT 
				a.SessionID
		FROM 
				CARD_STUDENT_TIME_SESSION AS a LEFT OUTER JOIN CARD_STUDENT_TIME_SESSION_REGULAR AS b ON (a.SessionID = b.SessionID AND b.ClassID = $ClassID AND b.DayType = 0)
		WHERE 
				a.RecordStatus = 0 AND a.NonSchoolDay = 0 
		GROUP BY 
				a.SessionID";
$result = $lclass->returnArray($sql,1);
if(sizeof($result)>0)
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($inactive_session) = $result[$i];
		array_push($InactiveSession,$inactive_session);
	}
}

### List ALL Sessions ###
if(!in_array($using_session,$InactiveSession))
{
	$sql = "SELECT 
					SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
			FROM 
					CARD_STUDENT_TIME_SESSION
			WHERE
					RecordStatus = 1 AND NonSchoolDay != 1
			ORDER BY
					SessionID";
	$temp = $lclass->returnArray($sql,7);
}
else
{
	$sql = "SELECT 
					SessionID, SessionName, TIME_FORMAT(MorningTime,'%H:%i'), TIME_FORMAT(LunchStart,'%H:%i'), TIME_FORMAT(LunchEnd,'%H:%i'), TIME_FORMAT(LeaveSchoolTime,'%H:%i'), NonSchoolDay
			FROM 
					CARD_STUDENT_TIME_SESSION
			WHERE
					(RecordStatus = 1 OR SessionID = $using_session) AND NonSchoolDay != 1
			ORDER BY
					SessionID";
	$temp = $lclass->returnArray($sql,7);
}
echo "normal --><br>".$sql."<br><111--<br>";
if(sizeof($temp)>0)
{
	$normal_table_content .= "";
	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list ($s_id, $s_name, $am_time, $lunch_time, $pm_time, $leave_school_time, $non_school_day, $day_type, $day_value) = $temp[$i];
		
		if(($using_session == $s_id)&&(in_array($using_session,$InactiveSession)))
		{
			$normal_table_content .= "<tr bgcolor=red><td>* $s_name</td>";
			$normal_warning = 1;
		}
		else
		{
			$normal_table_content .= "<tr><td>$s_name</td>";
		}
		if($hasAM)
			$normal_table_content .= "<td>$am_time</td>";
		if($hasLunch)
			$normal_table_content .= "<td>$lunch_time</td>";
		if($hasPM)
			$normal_table_content .= "<td>$pm_time</td>";
		$normal_table_content .= "<td>$leave_school_time</td>";

		$checked = " ";
		//if(($day_type!="")&&($day_value!="")&&($day_type==0)&&($day_value==0))
		if($s_id == $using_session)
		{
			$checked = "checked=1";
		}
		else
		{
			$checked = " ";
		}
		$normal_table_content .= "<td><input type=radio name=normal_session_id value=$s_id $checked></td></tr>";
		$normal_table_content .= "<input type=hidden name=ClassID value=$ClassID>";
	}
	
}
### End Of Normal Table ###
?>

<SCRIPT Language="JavaScript">
function removeDaySetting(Type, ClassID ,DayValue)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
		location.href = "class_session_remove.php?type="+Type+"&ClassID="+ClassID+"&value="+DayValue;
	}
}
function removeSpecialDaySetting(ClassID, targetDate)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
		location.href = "class_session_remove.php?ClassID="+ClassID+"&TargetDate="+targetDate;
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_TimeSessionSettings,'../',$i_StudentAttendance_Menu_Slot_Class,'index.php',$classname,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="class_edit_update.php" method=POST>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_NormalDays?></span>]</span></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Slot_Session_Name?></td>
<?
if($hasAM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_MorningTime?></td>
<?
}
if($hasLunch)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchStartTime?></td>
<?
}
if($hasPM)
{
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LunchEndTime?></td>
<?
}
?>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime?></td>
<td class=tableTitle><?=$i_StudentAttendance_Menu_Time_Session_Using?></td></tr>
<?=$normal_table_content?>
</table>
<?
if($normal_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>
<table width=560 border=0 align=center>
<tr><td align=right><input type=image src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif"></td></tr>
</table>

<br><br>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_Weekday_Specific?></span>]</span><a href=class_add.php?type=1&ClassID=<?=$ClassID?>><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<?=$weekday_table_content?>
</table>
<?
if($weekly_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>

<br><br>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_Cycleday_Specific?></span>]</span><a href=class_add.php?type=2&ClassID=<?=$ClassID?>><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<?=$cycle_table_content?>
</table>
<?
if($cycle_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>

<br><br>

<table border=0 width=560 cellspacing=0 cellpadding=0 align=center>
<tr><td><span class="extraInfo">[<span class=subTitle><?=$i_StudentAttendance_SpecialDay?></span>]</span><a href=class_add.php?type=3&ClassID=<?=$ClassID?>><img align=absmiddle border=0 src=<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif alt='<?=$button_add?>'></a></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<?=$special_day_table?>
</table>
<?
if($special_warning==1)
{
?>
<table width=560 border=0 align=center>
<tr><td><font color=red><?=$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning?></font></td></tr>
</table>
<?
}
?>
</table>
</form>

<?
include_once("../../../../../templates/adminfooter.php");
?>
?>