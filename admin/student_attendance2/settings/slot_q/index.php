<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();
$sql = "SELECT SlotID, DayType, SlotStart, SlotEnd
               FROM CARD_STUDENT_ATTENDANCE_CUSTOMIZED_1_TIMESLOT
               ORDER BY DayType, SlotID";
$temp = $li->returnArray($sql,4);
unset($array_time);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($t_id, $t_daytype, $t_start, $t_end) = $temp[$i];
     $array_time[$t_daytype][$t_id] = array($t_start, $t_end);
}

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_SmartCard_SystemSettings,'../',$i_StudentAttendance_TimeSlotSettings,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<!--
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
-->
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu align=center><?=$i_DayTypeAM?></td></tr>
  <tr>
    <td><table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>
<tr class=tableTitle>
  <td><?=$i_StudentAttendance_Slot?></td><td><?=$i_StudentAttendance_Slot_Start?></td>
  <td><?=$i_StudentAttendance_Slot_End?></td><td><?=$i_StudentAttendance_Status?></td>
  <td>&nbsp;</td>
</tr>
<tr class=tableContent2>
<td>1</td><td><?=$array_time[2][1][0]?></td><td><?=$array_time[2][1][1]?></td><td><?=$i_QualiEd_StudentAttendance_OnTime?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=1&DayType=2><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=tableContent>
<td>2</td><td><?=$array_time[2][2][0]?></td><td><?=$array_time[2][2][1]?></td><td><?=$i_QualiEd_StudentAttendance_LateType1?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=2&DayType=2><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=tableContent2>
<td>3</td><td><?=$array_time[2][3][0]?></td><td><?=$array_time[2][3][1]?></td><td><?=$i_QualiEd_StudentAttendance_LateType2?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=3&DayType=2><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=tableContent>
<td>4</td><td><?=$array_time[2][4][0]?></td><td><?=$array_time[2][4][1]?></td><td><?=$i_QualiEd_StudentAttendance_Absent?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=4&DayType=2><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=admin_bg_menu><td colspan=5 align=center><?=$i_DayTypePM?></td></tr>
<tr class=tableContent2>
<td>1</td><td><?=$array_time[3][1][0]?></td><td><?=$array_time[3][1][1]?></td><td><?=$i_QualiEd_StudentAttendance_OnTime?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=1&DayType=3><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=tableContent>
<td>2</td><td><?=$array_time[3][2][0]?></td><td><?=$array_time[3][2][1]?></td><td><?=$i_QualiEd_StudentAttendance_LateType1?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=2&DayType=3><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=tableContent2>
<td>3</td><td><?=$array_time[3][3][0]?></td><td><?=$array_time[3][3][1]?></td><td><?=$i_QualiEd_StudentAttendance_LateType2?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=3&DayType=3><img src="/images/icon_edit.gif" border=0></a></td>
</tr>
<tr class=tableContent>
<td>4</td><td><?=$array_time[3][4][0]?></td><td><?=$array_time[3][4][1]?></td><td><?=$i_QualiEd_StudentAttendance_Absent?></td>
<td align=right><a class=functionlink href=edit.php?SlotID=4&DayType=3><img src="/images/icon_edit.gif" border=0></a></td>
</tr>


</table>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<?
include_once("../../../../templates/adminfooter.php");
?>