<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

if(sizeOf($student_id)>0){
        for($i=0; $i<sizeOf($student_id); $i++){
                $fieldname = "StudentID, DateInput, DateModified";
                $fieldvalue = "'".$student_id[$i]."',NOW(),NOW()";
                $sql = "INSERT IGNORE INTO CARD_STUDENT_LUNCH_ALLOW_LIST ($fieldname) VALUES ($fieldvalue)";
                $li->db_db_query($sql);
        }
}
else if(sizeOf($class_name)>0){
        $list = "'" . implode("','",$class_name) . "'";
        $fieldname = "StudentID, DateInput, DateModified";
        $sub_sql = "SELECT UserID, now(), now() FROM INTRANET_USER WHERE ClassName IN ($list) AND RecordType=2 AND RecordStatus IN (0,1,2)";
        $sql = "INSERT IGNORE INTO CARD_STUDENT_LUNCH_ALLOW_LIST ($fieldname)
                       $sub_sql
                ";
        $li->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?msg=1");
?>