<?

include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libdbtable.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

if($update=="1"){
        $no_record = ($no_record=="1")?$no_record:"0";
        $out_once = ($out_once=="1")?$out_once:"0";
        $all_allow = ($all_allow=="1")?$all_allow:"0";

        $content = $no_record . "\n" . $out_once . "\n" . $all_allow;
        write_file_content($content, "$intranet_root/file/stattend_lunch_misc.txt");
}

$content_lunch_misc = trim(get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
$lunch_misc_settings = explode("\n",$content_lunch_misc);
list($lunch_misc_no_record,$lunch_misc_lunch_once, $lunch_misc_all_allow) = $lunch_misc_settings;

$checked_no_record = ($lunch_misc_no_record=="1")?"checked":"";
$checked_out_once = ($lunch_misc_lunch_once=="1")?"checked":"";
$checked_all_allow = ($lunch_misc_all_allow=="1")?"checked":"";

$select_no_record = "<input name=no_record type=checkbox $checked_no_record value=1>&nbsp;$i_SmartCard_Settings_Lunch_Misc_No_Record";
$select_out_once = "<input name=out_once type=checkbox $checked_out_once value=1>&nbsp;$i_SmartCard_Settings_Lunch_Misc_Out_Once";
$select_all_allow = "<input name=all_allow type=checkbox $checked_all_allow value=1>&nbsp;$i_SmartCard_Settings_Lunch_Misc_All_Allow";
?>

<form name="form1" method="get" action=''>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_SmartCard_SystemSettings,'../../',$i_StudentAttendance_LunchSettings,'../', $i_SmartCard_Settings_Lunch_Misc_Title, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<blockquote><blockquote>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_SmartCard_Settings_Lunch_Misc_No_Record?></td><td align=center><input name=no_record type=checkbox <?=$checked_no_record?> value=1></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_SmartCard_Settings_Lunch_Misc_Out_Once?></td><td align=center><input name=out_once type=checkbox <?=$checked_out_once?> value=1></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_SmartCard_Settings_Lunch_Misc_All_Allow?></td><td align=center><input name=all_allow type=checkbox <?=$checked_all_allow?> value=1></td></tr>
</table>
</blockquote></blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="index.php"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>

<input type=hidden name=update value=1>
</form>
<?
include_once("../../../../../templates/adminfooter.php");
?>