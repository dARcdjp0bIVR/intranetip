<?
// use by kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

$content_basic = $attendance_mode;
$time_table_mode = $time_table;

write_file_content($content_basic,"$intranet_root/file/stattend_basic.txt");
write_file_content($time_table_mode,"$intranet_root/file/time_table_mode.txt");
write_file_content($student_profile_input,"$intranet_root/file/disallow_student_profile_input.txt");
write_file_content($student_default_status,"$intranet_root/file/studentattend_default_status.txt");

header("Location: index.php?msg=2");
?>