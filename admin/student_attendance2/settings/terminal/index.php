<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$ips = get_file_content("$intranet_root/file/stattend_ip.txt");
$ignore_period = get_file_content("$intranet_root/file/stattend_ignore.txt");
$ignore_period += 0;

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_SmartCard_SystemSettings,'../',$i_SmartCard_TerminalSettings,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 method=POST action=update.php>
<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_SmartCard_Terminal_IPList; ?>:</td><td><textarea name=IPList COLS=40 ROWS=5><?=$ips?></textarea>
 <br><span class="extraInfo"><?=$i_SmartCard_Terminal_IPInput?></span>
 <br><span class="extraInfo"><?=$i_SmartCard_Terminal_YourAddress?>:<?=$HTTP_SERVER_VARS['REMOTE_ADDR']?></span></td></tr>
<tr><td align=right><?php echo $i_SmartCard_Terminal_IgnorePeriod; ?>:</td><td>
<input type=text name=ignore_period size=5 value="<?=$ignore_period?>"> <?=$i_SmartCard_Terminal_IgnorePeriod_unit?>
</td></tr>
</table>
<br>
<?=$i_SmartCard_Description_Terminal_IP_Settings?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>


</form>
<?
include_once("../../../../templates/adminfooter.php");
?>