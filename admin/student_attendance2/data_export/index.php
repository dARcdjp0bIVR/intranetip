<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_DataExport,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td class=tableContent height=150 align=left>


<blockquote>
<?
if(substr($module_version['StaffAttendance'],0,1) >= 2)
{
	echo displayOption(
						$i_StudentAttendance_StaffAndStudentInformation, 'staff_and_student.php',1
				 	  );
}
else
{
	echo displayOption(
						$i_StudentAttendance_StudentInformation, 'student.php',1
				 	  );
}
?>
<?= displayOption(
                  $i_StudentAttendance_Reminder, 'reminder.php',1,
                  #$i_StudentAttendance_OR_ExportStudentInfo, 'studentInfo_offline_reader_export.php',1,
                  $i_StudentAttendance_OR_ExportStudentInfo." (950e)", 'studentInfo_offline_reader_export_950e.php',1,
                  $i_MedicalReason_Export2,'medical.php',$sys_custom['hku_medical_research']
                                ) ?>
</blockquote>
</td></tr>
</table>
<br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<Tr><td class=tableContent ><?=$i_StudentAttendance_Export_Notice?></td></tr>
</table><Br>

<?
include_once("../../../templates/adminfooter.php");
?>