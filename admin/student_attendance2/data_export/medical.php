<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();

$lc = new libcardstudentattend2();





 # Retrieve Class Level List
 $sql = "SELECT ClassName, WebSAMSLevel
               FROM INTRANET_CLASS as a
                    LEFT OUTER JOIN INTRANET_CLASSLEVEL as b ON a.ClassLevelID = b.ClassLevelID
         ";
 $classes = $lc->returnArray($sql,2);
 
 
 $no_websamslevel = true;
 
$available_level = $lc->returnAvailableWebSamsLevel();
 
 for ($i=0; $i<sizeof($classes); $i++)
 {
      list($t_classname, $t_webSamsClassLevel) = $classes[$i];
      if (in_array($t_webSamsClassLevel, $available_level))
      {
          $no_websamslevel = false;
      }
 }


# default date range  = 1 Month
$currentMonth = date('n');
$FromDate = date('Y-m-d',mktime(0,0,0,$currentMonth,1));  // first day of current month
$ToDate   = date('Y-m-d',mktime(0,0,0,$currentMonth+1,0)); // last day of current month

?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
	      // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           		document.forms['form1'].FromDate.value = dateValue;
                           
          }
          function calendarCallback2(date, month, year)
          {								
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
		                           document.forms['form1'].ToDate.value = dateValue;

          }


	function checkForm(formObj){
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			if(!checkDate(fromV)){
					return false;
			}
			else if(!checkDate(toV)){
						return false;
			}
				return true;
	}
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}

	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
</script>
<form name="form1" method="GET" onSubmit="return checkForm(this)" action="medical_export.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_DataExport,'index.php',$i_MedicalReason_Export2,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table border=0 width=560 align=center>
<tr><td><?=$i_Profile_SelectSemester?>:</td></tr>
	<tr>
		<td nowrap class=tableContent>
		<br>&nbsp;&nbsp;<?=$i_Profile_From?>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;
		 	<?=$i_Profile_To?>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 </td></tr>
 <tr><td><?php 
 				if($no_websamslevel){
	 				echo "<Br><font color=red>$i_MedicalExportError</font><Br>";
	 			}
 		?></td></tr>
<tr><td><hr size=1 ></td></tr>
<tr><td align=right>
<?php if(!$no_websamslevel){?>
<a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a>
<?= btnReset() ?>
<?php } ?>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>