<?
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();

$li = new libdb();

$sql = "SELECT CardID, EnglishName, ChineseName, ClassName, ClassNumber, UserLogin
        FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN (0,1,2)
                ORDER BY ClassName, ClassNumber, EnglishName";

$result = $li->returnArray($sql,6);

$lexport = new libexporttext();

$exportColumn = array("CardID", "EnglishName", "ChineseName", "ClassName", "Class Number");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn, "###", "\r\n", ",", 6);


// Output the file to user browser
$filename = "studentinfo.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>
