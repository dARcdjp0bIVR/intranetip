<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Menu_OtherFeatures,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_StudentAttendance_Menu_OtherFeatures_Outing,'outing/',1,
                  $i_StudentAttendance_Menu_OtherFeatures_Detention,'detention/',1,
                  $i_StudentAttendance_Menu_OtherFeatures_Reminder,'reminder/',1,
                  $i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption, 'leave_option/',$sys_custom['SmartCardStudentAttend_LeaveSchoolOption']
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>