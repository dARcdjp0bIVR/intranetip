<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$DetentionID = (is_array($DetentionID)? $DetentionID[0]:$DetentionID);

$li = new libdb();

$user_field = getNameFieldByLang("b.");
$sql = "SELECT a.StudentID, a.RecordDate, IF(a.ArrivalTime IS NULL,'',a.ArrivalTime),
        IF(a.DepartureTime IS NULL,'',a.DepartureTime), a.Location,a.Reason,a.Remark,
        CONCAT($user_field,' (',b.ClassName,'-', b.ClassNumber,')')
        FROM CARD_STUDENT_DETENTION as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
        WHERE DetentionID = $DetentionID";
$result = $li->returnArray($sql,8);
list($uid,$recordDate,$arrivalTime,$departureTime,$location,$reason,$remark,$name) = $result[0];

$lcard = new libcardstudentattend();
$locationTemplate = getSelectByValue($lcard->getWordList(4),"onChange=\"this.form.Location.value=this.value\"",$location);
$reasonTemplate = getSelectByValue($lcard->getWordList(5),"onChange=\"this.form.Reason.value=this.value\"",$reason);

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Detention,'index.php',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="edit_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_UserStudentName?></td><td><?=$name?></td></tr>
<tr><td><?=$i_SmartCard_DetentionDate?></td><td><input type=text name=RecordDate size=10 maxlength=10 value="<?=$recordDate?>">(YYYY-MM-DD)</td></tr>
<tr><td><?=$i_SmartCard_DetentionArrivalTime?></td><td><input type=text name=ArrivalTime size=10 maxlength=10 value="<?=$arrivalTime?>">(HH:mm:ss 24-hr)</td></tr>
<tr><td><?=$i_SmartCard_DetentionDepartureTime?></td><td><input type=text name=DepartureTime size=10 maxlength=10 value='<?=$departureTime?>'>(HH:mm:ss 24-hr)</td></tr>
<tr><td><?=$i_SmartCard_DetentionLocation?></td><td><input type=text name=Location size=50 value='<?=$location?>'><?=$locationTemplate?></td></tr>
<tr><td><?=$i_SmartCard_DetentionReason?></td><td><input type=text name=Reason size=50 value='<?=$reason?>'><?=$reasonTemplate?></td></tr>
<tr><td><?=$i_SmartCard_Remark?></td><td><TEXTAREA rows=5 cols=50 name=Remark><?=$remark?></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=DetentionID value="<?=$DetentionID?>">
</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
