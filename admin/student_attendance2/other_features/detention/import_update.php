<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$format_array = array("ClassName","ClassNumber","Date","Arrival","Departure","Location","Reason","Remark");
$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
        exit();
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                //$data = $lf->file_read_csv($filepath);
                $data = $limport->GET_IMPORT_TXT($filepath);
                $toprow = array_shift($data);                   # drop the title bar
        }
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($toprow[$i] != $format_array[$i])
             {
                 header("Location: import.php?msg=13");
                 exit();
             }
        }
        $sql = "CREATE TEMPORARY TABLE TEMP_IMPORT_STUDENT_DETENTION (
                 ClassName varchar(20),
                 ClassNumber varchar(20),
                 RecordDate date,
                 Arrival varchar(20),
                 Departure varchar(20),
                 Location varchar(255),
                 Reason text,
                 Remark text
        )";
        $sql = $li->db_db_query($sql);
        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($class,$classnum,$recordDate,$arrival,$departure,$location,$reason,$remark) = $data[$i];
             if ($class=="" || $classnum=="") continue;
             if ($recordDate == "")
                 $recordDateStr = "CURDATE()";
             else
                 $recordDateStr = "'$recordDate'";
             if ($arrival == "")
                 $arrivalStr = "NULL";
             else
                 $arrivalStr = "'$arrival'";
             if ($departure == "")
                 $departureStr = "NULL";
             else
                 $departureStr = "'$departure'";

             $values .= "$delim ('$class','$classnum',$recordDateStr,$arrivalStr,$departureStr,'$location','$reason','$remark')";
             $delim = ",";
        }

        $sql = "INSERT INTO TEMP_IMPORT_STUDENT_DETENTION (ClassName,ClassNumber,RecordDate,Arrival,Departure,Location,Reason,Remark)
                       VALUES $values";
        $li->db_db_query($sql);

        # Insert to Reminder table
        $sql = "INSERT INTO CARD_STUDENT_DETENTION (StudentID, RecordDate, ArrivalTime, DepartureTime,
                            Location, Reason, Remark,DateInput, DateModified)
                       SELECT
                             b.UserID, a.RecordDate, a.Arrival, a.Departure, a.Location, a.Reason,
                               a.Remark, now(), now()
                       FROM TEMP_IMPORT_STUDENT_DETENTION as a
                            LEFT OUTER JOIN INTRANET_USER as b
                                 ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
                       WHERE b.UserID IS NOT NULL
                       ";
        $li->db_db_query($sql);
}
intranet_closedb();

header("Location: index.php?msg=1");
?>