<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$lc = new libcardstudentattend2();


$li = new libdb();

if ($filter == 1)
{
    $conds .= " AND a.DateOfReminder >= CURDATE()";
}
else if ($filter == 2)
{
     $conds .= " AND a.DateOfReminder < CURDATE()";
}

$sql = "SELECT b.ClassName, b.ClassNumber, a.DateOfReminder, c.UserLogin, a.Reason
        FROM CARD_STUDENT_REMINDER as a
             LEFT OUTER JOIN INTRANET_USER as b
                  ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
             LEFT OUTER JOIN INTRANET_USER as c
                  ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               )
               $conds
                  ";
                  
$result = $li->returnArray($sql,5);

$lexport = new libexporttext();

$exportColumn = array("ClassName", "ClassNumber", "Date", "Teacher", "Content");

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);

// Output the file to user browser
$filename = "reminders.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename, $export_content);
?>