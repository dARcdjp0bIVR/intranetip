<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

$limport = new libimporttext();
$linterface = new interface_html();

?>

<form name="form1" method="POST" action="import_update.php" enctype="multipart/form-data">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Reminder,'index.php',$button_import,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<tr><td align=right nowrap></td><td>
<br><?=$i_StudentAttendance_Reminder_ImportFileDescription?>
<br><br><a class=functionlink_new href="<?= GET_CSV("sample.csv")?>" target=_blank><?=$i_general_clickheredownloadsample?></a>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</blockquote>
</form>

<?
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
