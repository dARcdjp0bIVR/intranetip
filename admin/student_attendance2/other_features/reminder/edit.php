<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$ReminderID = (is_array($ReminderID)? $ReminderID[0]:$ReminderID);

$li = new libdb();
$user_field = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT a.StudentID, $user_field, a.DateOfReminder, a.TeacherID, a.Reason FROM CARD_STUDENT_REMINDER as a
        LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus = 1 WHERE ReminderID = $ReminderID";
$result = $li->returnArray($sql,5);
list($studentID, $name, $remindDate, $TeacherID, $Reason) = $result[0];

        $lcard = new libcardstudentattend();
        $lclass = new libclass();

        $namefield = getNameFieldByLang();
        $sql = "SELECT UserID,$namefield FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
        $staff = $lclass->returnArray($sql,2);
        $select_teacher = getSelectByArray($staff,"name=TeacherID","$TeacherID",1,1);

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Reminder,'index.php',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<script language="javascript">
<!--
function checkForm(obj){
	

		if(dateValidation(obj.RemindDate))
			return true;
		return false;
}
function dateValidation(dateObj){
	v = dateObj.value;
  	var re = new RegExp("^[1-9][0-9]{3}-(0[1-9]|1[0-2])-(0[1-9]|1[1-9]|2[1-9]|3[0-1])$");
	if (!v.match(re)) {
			alert("<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>");
			dateObj.focus();
			return false;
	}
	return true;
 }
//-->
</SCRIPT>
<form name=form1 action="edit_update.php" method=POST onsubmit="return checkForm(this)">
<table width=90% border=0 align=center>
<tr><td><?=$i_UserStudentName?></td><td><?=$name?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Date?></td><td><input type=text name=RemindDate size=10 maxlength=10 value="<?=$remindDate?>">(YYYY-MM-DD)</td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Teacher?></td><td><?=$select_teacher?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Content?></td><td><TEXTAREA rows=5 cols=50 name=Reason><?=$Reason?></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=ReminderID value="<?=$ReminderID?>">
</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
