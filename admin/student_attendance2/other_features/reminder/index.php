<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

if (isset($targetID) && $targetID != "")
{
    $conds = "AND a.UserID = $targetID";
}
$user_field = getNameFieldWithClassNumberByLang("b.");
$teacher_field = getNameFieldByLang("c.");

if ($filter == 1)
{
    $conds .= " AND a.DateOfReminder >= CURDATE()";
}
else if ($filter == 2)
{
     $conds .= " AND a.DateOfReminder < CURDATE()";
}
/*
$sql  = "SELECT
               a.DateOfReminder, $user_field, $teacher_field,a.Reason,
               CONCAT('<input type=checkbox name=ReminderID[] value=', a.ReminderID ,'>')
         FROM
             CARD_STUDENT_REMINDER as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus = 1
                                   LEFT OUTER JOIN INTRANET_USER as c ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               )
               $conds
                ";
*/
$sql  = "SELECT
               a.DateOfReminder, $user_field, $teacher_field,a.Reason,
               CONCAT('<input type=checkbox name=ReminderID[] value=', a.ReminderID ,'>')
         FROM
             CARD_STUDENT_REMINDER as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN(0,1,2)
                                   LEFT OUTER JOIN INTRANET_USER as c ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               )
               $conds
                ";


# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.DateOfReminder","b.EnglishName","c.EnglishName","a.Reason");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Reminder_Date)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Reminder_Teacher)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column($pos++, $i_StudentAttendance_Reminder_Content)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("ReminderID[]")."</td>\n";

/*
$namesql = "SELECT UserID, Name FROM CARD_USER ORDER BY Name";
$names = $li->returnArray($namesql,2);
$name_select = getSelectByArray($names,"name=targetID onChange=this.form.submit()",$targetID,1);
*/

$select_status = "<SELECT name=filter onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='' ".($filter==""?"SELECTED":"").">$i_status_all</OPTION>\n";
$select_status .= "<OPTION value='1' ".($filter=="1"?"SELECTED":"").">$i_StudentAttendance_Reminder_Status_Coming</OPTION>\n";
$select_status .= "<OPTION value='2' ".($filter=="2"?"SELECTED":"").">$i_StudentAttendance_Reminder_Status_Past</OPTION>\n";
$select_status .= "</SELECT>\n";

$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$toolbar .= toolBarSpacer()."<a class=iconLink href=javascript:checkNew('import.php')>".importIcon()."$button_import</a>";
$toolbar .= toolBarSpacer()."<a class=iconLink href=javascript:checkGet(document.form1,'export.php')>".exportIcon()."$button_export</a>";
$functionbar .= $select_status;
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'ReminderID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'ReminderID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Reminder,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
