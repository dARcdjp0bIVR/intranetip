<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$x = "";

# Print Out The Selected Student
if(($student_list == '') && (sizeof($student)>0))
{
	$student_list = implode(",",$student);
}
$namefield = getNameFieldWithClassNumberByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID IN ($student_list)";
$result = $lclass->returnArray($sql,2);
if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list ($user_id, $student_name) = $result[$i];
		$x .= $student_name."<BR>";
	}
}

# Generate Teacher Selection Box
$namefield = getNameFieldByLang();
$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND Teaching = 1";
$temp = $lclass->returnArray($sql,2);
$select_teacher = getSelectByArray($temp, "name=TargetTeacher", $TargetTeacher);


$repeat_type = array(
array(0,"$i_StudentAttendance_Reminder_RepeatSelection[0]"), #No Repeat
array(1,"$i_StudentAttendance_Reminder_RepeatSelection[1]"), #Daily
array(2,"$i_StudentAttendance_Reminder_RepeatSelection[2]"), #Weekly Day
array(3,"$i_StudentAttendance_Reminder_RepeatSelection[3]") #Cycle Day
);
$repeat = getSelectByArray($repeat_type,"name=repeat_status onChange=\"document.form1.flag.value=1; this.form.submit();\"",$repeat_status,0,1);

if(($repeat_status == 0)||($repeat_status == ''))
{
	$repeat_status = "";
}
if(($repeat_status != 0)||($repeat_status != ''))
{
	$repeat_selection = "<tr><td>$i_StudentAttendance_Reminder_FinishDate</td>";
	$repeat_selection .= "<td><input type=text name=ReminderEndDate value='$ReminderEndDate' size=10>\n";
	$repeat_selection .= "<script language=\"JavaScript\" type=\"text/javascript\">\n";
	$repeat_selection .= "<!--\n";
    $repeat_selection .= "endCal = new dynCalendar('endCal', 'calendarCallback_End', '/templates/calendar/images/');\n";
    $repeat_selection .= "endCal.setLegend('$legend');\n";
    $repeat_selection .= "endCal.differentDisplay = true;\n";
    $repeat_selection .= "//-->\n";
	$repeat_selection.="</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>\n</td></tr>";
	
	if($repeat_status == 2)
	{
		$days = array(0,1,2,3,4,5,6);
		$repeat_selection .= "<tr><td>$i_StudentAttendance_Reminder_WeekdaySelection</td><td>";
	    for ($i=0; $i<sizeof($days); $i++)
	    {
	         $word = $i_DayType0[$days[$i]];
	         $repeat_selection .= $word."<input type=checkbox name=DayValue[] value=$i>&nbsp;&nbsp;&nbsp;";
	         if($i==3)
	         	$repeat_selection .= "<BR>";
	    }
	    $repeat_selection .= "</td></tr>";
    }
	
	if($repeat_status == 3)
	{
		$sql = "SELECT DISTINCT TextShort FROM INTRANET_CYCLE_DAYS";
		$temp = $lclass->returnArray($sql,1);
		if(sizeof($temp)>0)
		{
			$repeat_selection .= "<tr><td>$i_StudentAttendance_Reminder_CycledaySelection</td><td>";
			for($i=0; $i<sizeof($temp); $i++)
			{
				list($cycle_day) = $temp[$i];
				$repeat_selection .= $cycle_day."<input type=checkbox name=DayValue[] value=$cycle_day>&nbsp;&nbsp;&nbsp;";
			}
			$repeat_selection .= "</td></tr>";
		}
	}
}

$back_button = "<a href=\"new.php\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
?>

<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
     var css_array = new Array;
     css_array[0] = "dynCalendar_card_no_data";
     css_array[1] = "dynCalendar_card_not_confirmed";
     css_array[2] = "dynCalendar_card_confirmed";
     var date_array = new Array;
     <?
     for ($i=0; $i<sizeof($records_with_data); $i++)
     {
          $date_string = $records_with_data[$i];
          $isConfirmed = $confirmed_dates[$date_string];
          ?>
          date_array[<?=$date_string?>] = <?=($isConfirmed?2:1)?>;
          <?
     }
     ?>
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
      // Calendar callback. When a date is clicked on the calendar
      // this function is called so you can do as you want with it
      function calendarCallback_Start(date, month, year)
      {
                       if (String(month).length == 1) {
                               month = '0' + month;
                       }

                       if (String(date).length == 1) {
                               date = '0' + date;
                       }
                       dateValue =year + '-' + month + '-' + date;
                       document.forms['form1'].ReminderStartDate.value = dateValue;
      }
      function calendarCallback_End(date, month, year)
      {
                       if (String(month).length == 1) {
                               month = '0' + month;
                       }

                       if (String(date).length == 1) {
                               date = '0' + date;
                       }
                       dateValue =year + '-' + month + '-' + date;
                       document.forms['form1'].ReminderEndDate.value = dateValue;
      }
// -->
</script>
<SCRIPT Language="JavaScript">
function checkForm()
{
	var obj = document.form1;
	var temp = document.form1['DayValue[]'];
	var checking = 0;
	
	if(obj.flag.value==0)
	{
		<? 
		if($repeat_status!=0) 
		{
		?>
			if(obj.ReminderStartDate.value!="") 
			{
				if(obj.ReminderEndDate.value!="")
				{
					if(obj.ReminderEndDate.value > obj.ReminderStartDate.value)
					{
						<?if(($repeat_status==2) || ($repeat_status==3)){?>
							for (i=0; i<temp.length; i++)
							{
								if(temp[i].checked)
									checking = 1;
							}
							if(checking == 1)
							{
								if(obj.TargetTeacher.value != "")
								{
									if(obj.Reason.value != "")
									{
										obj.action = "new_update.php";
										return true;
									}
									else
									{
										alert("<?=$i_StudentAttendance_Reminder_InputReasonWarning?>");
										return false;
									}
								}
								else
								{
									alert("<?=$i_StudentAttendance_Reminder_TeacherSelectionWarning?>");
									return false;
								}
							}
							else
							{
								<?if($repeat_status==2){?>
									alert ("<?=$i_StudentAttendance_Reminder_WeekdaySelectionWarning?>");
									return false;
								<?}?>
								<?if($repeat_status==3){?>
									alert ("<?=$i_StudentAttendance_Reminder_CycleDaySelectionWarning?>");
									return false;
								<?}?>
							}
							<?
							}
							else
							{
							?>
							if(obj.TargetTeacher.value != "")
							{
								if(obj.Reason.value != "")
								{
									obj.action = "new_update.php";
									return true;
								}
								else
								{
									alert("<?=$i_StudentAttendance_Reminder_InputReasonWarning?>");
									return false;
								}
							}
							else
							{
								alert("<?=$i_StudentAttendance_Reminder_TeacherSelectionWarning?>");
								return false;
							}
							<?
							}
							?>
					}
					else
					{
						alert("<?=$i_StudentAttendance_Reminder_WrongDateWarning?>");
						return false;
					}
				}
				else
				{
					alert("<?=$i_StudentAttendance_Reminder_FinishDateEmptyWarning?>");
					return false;
				}
			}
			else
			{
				alert("<?=$i_StudentAttendance_Reminder_StartDateEmptyWarning?>");
				return false;
			}
		<?
		}
		else
		{
		?>
			if(obj.ReminderStartDate.value!="")
			{
				obj.action = "new_update.php";
				return true;
			}
			else
			{
				alert("<?=$i_StudentAttendance_Reminder_StartDateEmptyWarning?>");
				return false;
			}
		<?
		}
		?>
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Reminder,'index.php',$button_new,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method="POST" onSubmit="return checkForm()">
<table border=0 width=80% align=center>
<tr><td><?=$i_UserStudentName?></td><td><?=$x?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_RepeatFrequency?></td><td><?=$repeat?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_StartDate?></td><td><input type=text name=ReminderStartDate value='<?=$ReminderStartDate?>' size=10>
<script language="JavaScript" type="text/javascript">
<!--
     startCal = new dynCalendar('startCal', 'calendarCallback_Start', '/templates/calendar/images/');
     startCal.setLegend('<?=$legend?>');
     startCal.differentDisplay = true;
//-->
</script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>

<?=$repeat_selection?>
<tr><td><?=$i_StudentAttendance_Reminder_Teacher?></td><td><?=$select_teacher?></td></tr>
<tr><td><?=$i_StudentAttendance_Reminder_Content?></td><td><TEXTAREA rows=5 cols=50 name=Reason><?=$Reason?></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"> <?=$back_button?></td></tr>
</table>
</tr>
<input type=hidden name=student_list value=<?=$student_list?>>
<input type=hidden name=flag value=0>
</table>
</form>


<?

include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>