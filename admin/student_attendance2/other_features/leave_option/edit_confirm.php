<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();

if($TargetClass != "")
{
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT
					a.UserID,
					$namefield
			FROM
					INTRANET_USER AS a 
			WHERE
					a.ClassName = '$TargetClass' AND 
					a.RecordType=2 AND 
					a.RecordStatus=1
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";
	//echo $sql;
	$result = $lclass->returnArray($sql,2);
	
	for($i=0; $i<sizeof($result); $i++)
	{
		list($user_id, $name) = $result[$i];
		$student_name[$user_id] = $name;
	}
	
	$table_content .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>\n";
	$table_content .= "<tr><td>$i_studentAttendance_ConfirmMsg</td></tr>";
	$table_content .= "</table>";
	$table_content .= "<br>";
	$table_content .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
	$table_content .= "<tr class=tableTitle>
							<td width=100>$i_UserStudentName</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[1]</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[2]</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[3]</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[4]</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[5]</td>
							<td width=60>$i_StudentAttendance_LeaveOption_Weekday[6]</td></tr>\n";
							
	if(sizeof($student)>0)
	{
		for($i=0; $i<sizeof($student); $i++)
		{
			$opt_1 = $attendst_leave_option[${"opt_$student[$i]_1"}][1];
			$opt_2 = $attendst_leave_option[${"opt_$student[$i]_2"}][1];
			$opt_3 = $attendst_leave_option[${"opt_$student[$i]_3"}][1];
			$opt_4 = $attendst_leave_option[${"opt_$student[$i]_4"}][1];
			$opt_5 = $attendst_leave_option[${"opt_$student[$i]_5"}][1];
			$opt_6 = $attendst_leave_option[${"opt_$student[$i]_6"}][1];
	
			$uid = $student[$i];
			$table_content .= "<tr><td>$student_name[$uid]</td>\n";
			$table_content .= "<td>$opt_1<input type=hidden name=opt_".$student[$i]."_1 value=".${"opt_$student[$i]_1"}."></td>\n";
			$table_content .= "<td>$opt_2<input type=hidden name=opt_".$student[$i]."_2 value=".${"opt_$student[$i]_2"}."></td>\n";
			$table_content .= "<td>$opt_3<input type=hidden name=opt_".$student[$i]."_3 value=".${"opt_$student[$i]_3"}."></td>\n";
			$table_content .= "<td>$opt_4<input type=hidden name=opt_".$student[$i]."_4 value=".${"opt_$student[$i]_4"}."></td>\n";
			$table_content .= "<td>$opt_5<input type=hidden name=opt_".$student[$i]."_5 value=".${"opt_$student[$i]_5"}."></td>\n";
			$table_content .= "<td>$opt_6<input type=hidden name=opt_".$student[$i]."_6 value=".${"opt_$student[$i]_6"}."></td>\n";
			$table_content .= "<input type=hidden name=student[] value=$student[$i]>";
		}
	}
	$table_content .= "</table>\n";
	$table_content .= "<table width=560 border=0 cellspacing=0 cellpadding=0 align=center>\n";
	$table_content .= "<tr><td height=10px></td></tr>\n";
	$table_content .= "<tr><td align=right>".btnSubmit()."&nbsp;<a href=\"edit.php?TargetClass=$TargetClass\"><img src=\"/images/admin/button/s_btn_cancel_$intranet_session_language.gif\" border=\"0\"></a></td></tr>\n";
	$table_content .= "</table>\n";
}
?>
<SCRIPT LANGUAGE="JavaScript">
function setAll(weekday,checked)
{
	if(checked == 1)
	{
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
	if(checked == 0)
	{
		<? 
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = 0;
		<?
		}
		?>
	}
}
function setAllUserOnly(user_id,checked)
{
	if(checked == 1)
	{
		for(i=1; i<7; i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
	if(checked == 0)
	{
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = 0;
		}
	}	
}
function changeAll(weekday)
{
	var temp = eval("document.form1.all_"+weekday);
	if(temp.checked)
	{
		<?
		for($i=0; $i<sizeof($result); $i++) 
		{
			list($uid, $student_name, $opt_mon, $opt_tue, $opt_wed, $opt_thur, $opt_fri, $opt_sat) = $result[$i];
		?>
			var temp = eval("document.form1.opt_"+<?=$uid?>+"_"+weekday);
			temp.value = eval("document.form1.opt_all_"+weekday+".value");
		<?
		}
		?>
	}
}
function changeAllUserOnly(user_id)
{
	var temp = eval("document.form1.all_"+user_id);
	if(temp.checked)
	{
		for(i=1;i<7;i++)
		{
			var temp = eval("document.form1.opt_"+user_id+"_"+i);
			temp.value = eval("document.form1.opt_"+user_id+"_all.value");
		}
	}
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../',$i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption,'index.php',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name="form1" action="edit_update.php" method="POST">
<?=$table_content?>
</form>

<?
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>	