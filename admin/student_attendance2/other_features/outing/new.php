<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_OtherFeatures,'../', $i_StudentAttendance_Menu_OtherFeatures_Outing,'index.php',$button_new,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);

if (!isset($ClassName) || $ClassName == "")
{
        $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()");
        $x = "<form name=form1 action='' method=GET>\n";
        $x .= "<table width=90% border=0 align=center><tr><td>\n";
        $x .= "$i_UserParentLink_SelectClass: $select_class";
        $x .= "</td></tr></table></form>\n";
        echo $x;
}
else
{
        $select_class = $lclass->getSelectClass("name=ClassName onChange=\"this.form.action='';this.form.method='GET';this.form.submit()\"",$ClassName);
        $select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID");

        $currentDate = date('Y-m-d');
        $currentTime = date('H:i:s');
        $lcard = new libcardstudentattend2();

        $fromWhereTemplate = getSelectByValue($lcard->getWordList(1),"onChange=\"this.form.FromWhere.value=this.value\"");
        $locationTemplate = getSelectByValue($lcard->getWordList(2),"onChange=\"this.form.Location.value=this.value\"");
        $objectiveTemplate = getSelectByValue($lcard->getWordList(3),"onChange=\"this.form.Objective.value=this.value\"");
        $namefield = getNameFieldForRecord();
        $sql = "SELECT $namefield FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
        $staff = $lclass->returnVector($sql);
        $picTemplate = getSelectByValue($staff,"onChange=\"addPIC(this.form,this.value)\"");

        # Get Time bounds
        $setting_file = "$intranet_root/file/attend_st_settings.txt";
        $file_content = get_file_content($setting_file);
        $lines = explode("\n",$file_content);
        for ($i=0; $i<sizeof($lines); $i++)
        {
             $settings[$i] = explode(",",$lines[$i]);
        }

?>
<script language="javascript">
function addPIC(obj, pic)
{
         if (obj.PIC.value != '')
         {
             obj.PIC.value += ', ';
         }
         obj.PIC.value += pic;
}
function checkform(obj)
{
         if (obj.StudentID.value=='')
         {
             alert('<?=$i_Profile_SelectUser?>');
             obj.StudentID.focus();
             return false;
         }
         if (!check_date(obj.OutingDate,'<?=$i_invalid_date?>'))  return false;
         return true;
}
</SCRIPT>
<form name=form1 action="new_update.php" method=POST ONSUBMIT="return checkform(this)">
<blockquote>
<table width=500 border=0 align=center>
<tr><td align=right width="100px"><?=$i_SmartCard_ClassName?></td><td width="400px"><?=$select_class?></td></tr>
<tr><td align=right width="100px"><?=$i_UserStudentName?></td><td width="400px"><?=$select_student?></td></tr>
<tr><td align=right width="100px"><?=str_replace("/","/<br />",$i_SmartCard_StudentOutingDate)."<br />"?></td><td width="400px"><input type=text name=OutingDate size=10 maxlength=10 value="<?=$currentDate?>">(YYYY-MM-DD)</td></tr>
<tr><td align=right width="100px"><?=str_replace("/","/<br />",$i_SmartCard_StudentOutingOutTime)."<br />"?></td><td width="400px"><input type=text name=OutTime size=10 maxlength=10 value="<?=$currentTime?>">(HH:mm:ss 24-hr)</td></tr>
<tr><td align=right width="100px"><?=$i_SmartCard_StudentOutingBackTime?></td><td width="400px"><input type=text name=BackTime size=10 maxlength=10>(HH:mm:ss 24-hr)</td></tr>
<tr><td align=right width="100px"><?=$i_SmartCard_StudentOutingPIC?></td><td width="400px"><input type=text name=PIC size=50><?=$picTemplate?></td></tr>
<tr><td align=right width="100px"><?=str_replace("/","/<br />",$i_SmartCard_StudentOutingFromWhere)."<br />"?></td><td width="400px"><input type=text name=FromWhere size=50><?=$fromWhereTemplate?></td></tr>
<tr><td align=right width="100px"><?=str_replace("/","/<br />",$i_SmartCard_StudentOutingLocation)."<br />"?></td><td width="400px"><input type=text name=Location size=50><?=$locationTemplate?></td></tr>
<tr><td align=right width="100px"><?=$i_SmartCard_StudentOutingReason?></td><td width="400px"><input type=text name=Objective size=50><?=$objectiveTemplate?></td></tr>
<tr><td align=right width="100px"><?=$i_SmartCard_Remark?></td><td width="400px"><TEXTAREA rows=5 cols=50 name=Remark></TEXTAREA></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>
<?
}
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
