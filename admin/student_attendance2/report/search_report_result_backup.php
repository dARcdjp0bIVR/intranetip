<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libwordtemplates.php");

if(!$sys_custom['SmartCardAttendance_Report_Search']){
	header("Location: index.php");
}

// include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$classes = explode(",",$selected_classes);
if(sizeof($classes)<=0)
	heaer("Location: search_report.php");
	
$selected_classes="";
for($i=0;$i<sizeof($classes);$i++)
	$selected_classes.="'".$classes[$i]."',";
if($selected_classes!="")
	$selected_classes=substr($selected_classes,0,strlen($selected_classes)-1);

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();

	
# process date range
$current_month=date('n');
$current_year =date('Y');

if($startStr=="" || $endStr==""){
	if($current_month>=9){
		$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
		$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
	}else{
		$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
		$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
	}
}else{
	$startDate = $startStr;
	$endDate = $endStr;
}

# get daily log tables within the date range 
$today = date('Y-m-d');
$target_months = array();
if($current_month>=9)
	$smallest_date = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
else $smallest_date = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));

if($startDate>$today)
	$startDate=$today;
if($startDate<$smallest_date)
	$startDate=$smallest_date;
if($endDate > $today)
	$endDate = $today; 			# the latest day should be today

	
$start_ts = strtotime($startDate);
$end_ts = strtotime($endDate);
	
if($startDate<=$today && $endDate<=$today){
	$temp_ts=$start_ts;
	while($temp_ts<=$end_ts){
		$target_months[] = date('Y_m',$temp_ts);
		$temp_ts=mktime(0,0,0,date('m',$temp_ts)+1,1,date('Y',$temp_ts));
	}
}

# session & attendance  type
$cond_day_type="";
$cond_session_attend="";

if($session==PROFILE_DAY_TYPE_AM){  # AM
	$cond_day_type = " AND c.DayType='".PROFILE_DAY_TYPE_AM."'";	
	if($attendance_type==CARD_STATUS_LATE){ # Late
		$cond_session_attend = " AND b.AMStatus='".CARD_STATUS_LATE."'";
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."'";

	}	
	else if($attendance_type==CARD_STATUS_ABSENT){ # Absent
		$cond_session_attend = " AND b.AMStatus='".CARD_STATUS_ABSENT."'";
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."'";
	}	
	else{ # All ( Late or Absent ) 
		$cond_session_attend = " AND (b.AMStatus='".CARD_STATUS_LATE."' OR b.AMStatus='".CARD_STATUS_ABSENT."')";
		$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."')";
	}
}
else if($session==PROFILE_DAY_TYPE_PM){ # PM
	$cond_day_type = " AND c.DayType='".PROFILE_DAY_TYPE_PM."'";	

	if($attendance_type==CARD_STATUS_LATE){  #Late
		$cond_session_attend = " AND b.PMStatus='".CARD_STATUS_LATE."'";
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."'";
	}	
	else if($attendance_type==CARD_STATUS_ABSENT){  # Absent 
		$cond_session_attend = " AND b.PMStatus='".CARD_STATUS_ABSENT."'";
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."'";
	}
	else{  # ALL(Late or Absent)
		$cond_session_attend = " AND (b.PMStatus='".CARD_STATUS_LATE."' OR b.PMStatus='".CARD_STATUS_ABSENT."')";
		$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."')";
	}
}
else{  # ALL (AM or PM)
	$cond_day_type = " AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."')";	
	
	if($attendance_type==CARD_STATUS_LATE){ # Late
				$cond_session_attend = " AND (b.AMStatus='".CARD_STATUS_LATE."' OR b.PMStatus='".CARD_STATUS_LATE."')";
				$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."'";

	}			
	else if($attendance_type==CARD_STATUS_ABSENT){ # Absent
				$cond_session_attend = " AND (b.AMStatus='".CARD_STATUS_ABSENT."' OR b.PMStatus='".CARD_STATUS_ABSENT."')";
				$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."'";

	}
	else{ # ALL (LATE or ABSENT)
				$cond_session_attend = " AND ( b.AMStatus='".CARD_STATUS_LATE."' OR b.AMStatus='".CARD_STATUS_ABSENT."' OR b.PMStatus='".CARD_STATUS_LATE."' OR b.PMStatus='".CARD_STATUS_ABSENT."')";
				$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."')";
	}
}

# reason 
$cond_reason ="";

if($match=="1"){
	$cond_reason=" AND c.Reason='".intranet_htmlspecialchars(addslashes($reason))."'";
}else{
	if($reason!="")
		$cond_reason =" AND c.Reason LIKE '%".intranet_htmlspecialchars(addslashes($reason))."%'";
}

# waived
$cond_waived="";
if($waived==1){
	$cond_waived=" AND c.RecordStatus=1";
}
else if($waived==2){
	$cond_waived=" AND c.RecordStatus!=1";
}
else{
	# all ( waived or not wavied)
}

//$class_number_list =array();

/*
# get students in the selected classes
$sql ="SELECT UserID FROM INTRANET_USER WHERE ClassName IN ($selected_classes) AND RecordType=2 AND RecordStatus IN(0,1)";
$t_users = $lcardattend->returnVector($sql);
if(sizeof($t_users)>0)
	$target_users = implode(",",$t_users);


$sql="SELECT a.Userlogin,".getNameFieldByLang("a.").",a.ClassName,a.ClassNumber,c.StudentID, c.RecordDate, c.RecordType, c.DayType, c.Reason, c.RecordStatus FROM CARD_STUDENT_PROFILE_RECORD_REASON AS c ,INTRANET_USER AS a
	WHERE c.RecordDate>='$startDate' AND c.RecordDate<='$endDate' AND c.StudentID IN ($target_users) $cond_day_type $cond_reason $cond_waived AND a.UserID=c.StudentID
	ORDER BY a.ClassName,a.ClassNumber,c.RecordDate 
	";
*/
	
$sql="SELECT a.Userlogin,".getNameFieldByLang("a.").",a.ClassName,a.ClassNumber,c.StudentID, c.RecordDate, c.RecordType, c.DayType, c.Reason, c.RecordStatus FROM CARD_STUDENT_PROFILE_RECORD_REASON AS c ,INTRANET_USER AS a
	WHERE c.RecordDate>='$startDate' AND c.RecordDate<='$endDate' AND a.ClassName IN ($selected_classes) AND a.RecordType=2 AND a.RecordStatus IN(0,1) AND c.StudentID=a.UserID $cond_day_type $cond_reason $cond_waived 
	ORDER BY a.ClassName,a.ClassNumber,c.RecordDate 
	";

$reason_result = $lcardattend->returnArray($sql,10);
//echo "<p>$sql</p>";
# process records retrieved from CARD_STUDENT_PROFILE_RECORD_REASON
for($i=0;$i<sizeof($reason_result);$i++){
		list($s_login,$s_name,$c_name1,$c_number,$s_id,$record_date,$attend_type,$session_type,$t_reason,$t_waived)=$reason_result[$i];
		$index = $c_name1.$c_number;
		$final_result[$index]['login']=$s_login;
		$final_result[$index]['sid'] = $sid;
		$final_result[$index]['name']=$s_name;
		$final_result[$index]['classname']=$c_name1;
		$final_result[$index]['classnumber']=$c_number;	
		//if(!in_array($index,$class_number_list))
		//	$class_number_list[]=$index;
			
		$str_type1 = $attend_type==CARD_STATUS_LATE?$i_Profile_Late:$i_Profile_Absent;
		
		//echo "<p>$s_id,$record_date,attendtype=$attendance_type,$session_type,$reason,$waived,CARD_STATUS_LATE=".CARD_STATUS_LATE.",CARD_STATUS_ABSENT=".CARD_STATUS_ABSENT.",str_type=$str_type</p>";
		if($session_type==PROFILE_DAY_TYPE_AM){
			$final_result[$index]['date'][$record_date]['AM']['type']=$str_type1;
			$final_result[$index]['date'][$record_date]['AM']['reason']=intranet_undo_htmlspecialchars($t_reason);
			$final_result[$index]['date'][$record_date]['AM']['waived']=$t_waived;
		}
		else{ 
			$final_result[$index]['date'][$record_date]['PM']['type']=$str_type1;
			$final_result[$index]['date'][$record_date]['PM']['reason']=intranet_undo_htmlspecialchars($t_reason);
			$final_result[$index]['date'][$record_date]['PM']['waived']=$t_waived;
		}
}

# process records in CARD_STUDENT_DAILY_LOG_{year}_{month}
if($reason=="" && $waived!=1){
//	echo "<p>process daily log table</p>";
	$temp_result=array();	
	for($i=0;$i<sizeof($target_months);$i++){
			$target_month = $target_months[$i];
			$target_table = "CARD_STUDENT_DAILY_LOG_".$target_month;
			
			$cond_date="";
			
			if($target_month==date('Y_m',$start_ts)){
				$txt_start_day = date('j',$start_ts);
				$cond_date=" AND b.DayNumber>='$txt_start_day' ";
			}
			else if($target_month==date('Y_m',$end_ts)){
				$txt_end_day = date('j',$end_ts);
				$cond_date =" AND b.DayNumber<='$txt_end_day' ";
			}
			$temp = explode("_",$target_month);
			$txt_year = $temp[0];
			$txt_month = $temp[1];
			
			$sql="SELECT a.UserID,
						 a.UserLogin,
						 ".getNameFieldByLang("a.")." as name,
						 a.ClassName,
						 a.ClassNumber,
						 b.AMStatus,
						 b.PMStatus,
		                 CONCAT('$txt_year-$txt_month-',IF(b.DayNumber<10,CONCAT('0',b.DayNumber),b.DayNumber)) AS day
				 FROM    $target_table AS b,
				 	     INTRANET_USER AS a
				 WHERE   a.ClassName IN ($selected_classes) AND a.RecordType=2 AND a.RecordStatus IN(0,1) AND a.UserID=b.UserID 
				 		 $cond_session_attend 
				 		 $cond_date 
				 		 ORDER BY a.ClassName,a.ClassNumber,b.DayNumber
			";
		//	echo "<p>$sql</p>";
			$temp_result[] = $lcardattend->returnArray($sql,8);
	}
	for($i=0;$i<sizeof($temp_result);$i++){
		$r = $temp_result[$i];
		for($j=0;$j<sizeof($r);$j++){
			list($u_id,$u_login,$u_name,$c_name,$c_num,$am_status,$pm_status,$target_date) = $r[$j];
	//		echo "<p>2=$u_id,$u_login,$u_name,$c_name,$c_num,$am_status,$pm_status,$target_date</p>";
			$index2=$c_name.$c_num;
			$final_result[$index2]['sid']=$u_id;
			$final_result[$index2]['login']=$u_login;
			$final_result[$index2]['name']=$u_name;
			$final_result[$index2]['classname']=$c_name;
			$final_result[$index2]['classnumber']=$c_num;
			// if(!in_array($index2,$class_number_list))
			//	 $class_number_list[]=$index2;
			
			# if selected session is NOT PM only, then check AMStatus
			if($session!=PROFILE_DAY_TYPE_PM){
				if($am_status==CARD_STATUS_LATE || $am_status==CARD_STATUS_ABSENT){
					if($attendance_type=="" || $attendance_type==$am_status)
						$str_type = $am_status==CARD_STATUS_LATE?$i_Profile_Late:$i_Profile_Absent;
					else $str_type="";
					$final_result[$index2]['date'][$target_date]['AM']['type']=$str_type;
				}
			}
			
			# if selected session is NOT AM only, then check PMStatus
			if($session!=PROFILE_DAY_TYPE_AM){
				if($pm_status==CARD_STATUS_LATE || $pm_status == CARD_STATUS_ABSENT){
					if($attendance_type=="" || $attendance_type==$pm_status)
						$str_type = $pm_status==CARD_STATUS_LATE?$i_Profile_Late:$i_Profile_Absent;
					else $str_type="";
					$final_result[$index2]['date'][$target_date]['PM']['type']=$str_type;
				}
			}
		}
	}
}	
//echo "<p>sizeof index=".sizeof($class_number_list)."</p>";
//if(sizeof($class_number_list)>0){
//	sort($class_number_list);
	//if(sizeof($final_result)>0)
	//	array_multisort($class_number_list,SORT_ASC,$final_result);
//}

if(sizeof($final_result)>0)
	ksort($final_result);


# csv
$csv_content="\"$i_Profile_Attendance ( $startDate $i_Profile_To $endDate )\"\n";
$csv_content.="\"$i_UserLogin\",\"$i_UserStudentName\",\"$i_ClassNameNumber\",\"$i_Attendance_Date\",\"$i_Attendance_attendance_type\",\"$i_Attendance_Reason\",\"$i_SmartCard_Frontend_Take_Attendance_Waived\"\n";

# DISPLAY
$display ="<link rel=\"stylesheet\" href=\"/templates/style.css\">";
$display.="<table width=95% border=0><tr><th>$i_Profile_Attendance ( $startDate $i_Profile_To $endDate )</th></tr></table>";
$display .="<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=1 cellspacing=0>
	<tr class=tableTitle>
	<th>$i_UserLogin</th>
	<th>$i_UserStudentName</th>
	<th>$i_ClassNameNumber</th>

	<th>$i_Attendance_Date</th>

	<th>$i_Attendance_attendance_type</th>
	<th>$i_Attendance_DayType</th>
	<th>$i_Attendance_Reason</th>
	<th>$i_SmartCard_Frontend_Take_Attendance_Waived</th>
	</tr>";
$css="2";
//echo "<p>final result=".sizeof($final_result)."</p>";
if(sizeof($final_result)>0){
	foreach($final_result as $index3 => $value){
		$login = $final_result[$index3]['login'];
		$name = $final_result[$index3]['name'];
		$class_name= $final_result[$index3]['classname'];
		$class_number= $final_result[$index3]['classnumber'];
	
		$date_list = $final_result[$index3]['date'];
		$count=1;
		$css=$css=="2"?"":"2";
		foreach($date_list as $date =>$v){
			$attend_am_type=$final_result[$index3]['date'][$date]['AM']['type'];
			$attend_pm_type=$final_result[$index3]['date'][$date]['PM']['type'];
			$am_reason = $final_result[$index3]['date'][$date]['AM']['reason'];
			$pm_reason = $final_result[$index3]['date'][$date]['PM']['reason'];
			$am_waived = $final_result[$index3]['date'][$date]['AM']['waived'];
			$pm_waived = $final_result[$index3]['date'][$date]['PM']['waived'];
			$am_waived = $am_waived==1?$i_general_yes:$i_general_no;
			$pm_waived = $pm_waived==1?$i_general_yes:$i_general_no;
			$am_row="";
			$pm_row="";
			$str_class =$class_number==""?$class_name:"$class_name($class_number)";
			if($attend_am_type!=""){
				$am_row="<tr>";
					if($count==1){
						$am_row.="
							<Td class=tableContent$css>$login</td>
							<Td class=tableContent$css>$name</td>
							<td class=tableContent$css>$str_class&nbsp;</td>
						";
						//$count=2;
					}
					else $am_row.="<td colspan=3>&nbsp;</td>";
							
				$am_row.="<Td class=tableContent$css>$date</td>
					<Td class=tableContent$css>$attend_am_type</td>
					<td class=tableContent$css>$i_StudentAttendance_Slot_AM</td>
					<Td class=tableContent$css>$am_reason&nbsp;</td>
					<Td class=tableContent$css>$am_waived</td>
					</tr>";
				 $display.=$am_row;
 				 $csv_content.="\"$login\",\"$name\",\"$str_class\",\"$date\",\"$attend_am_type\",\"$i_StudentAttendance_Slot_AM\",\"$am_reason\",\"$am_waived\"\n";
			}	
			if($attend_pm_type!=""){
				$pm_row="<tr>";
				if($count==1){
					$pm_row.="
						<Td class=tableContent$css>$login</td>
						<Td class=tableContent$css>$name</td>
						<td class=tableContent$css>$str_class&nbsp;</td>
					";
					//$count=2;
				}
				else $pm_row.="<td colspan=3>&nbsp;</td>";
				$pm_row.="
						<Td class=tableContent$css>$date</td>
						<Td class=tableContent$css>$attend_pm_type</td>
						<td class=tableContent$css>$i_StudentAttendance_Slot_PM</td>
						<Td class=tableContent$css>$pm_reason&nbsp;</td>
						<Td class=tableContent$css>$pm_waived</td>
						</tr>";
					$display.=$pm_row;
 				 $csv_content.="\"$login\",\"$name\",\"$str_class\",\"$date\",\"$attend_pm_type\",\"$i_StudentAttendance_Slot_PM\",\"$pm_reason\",\"$pm_waived\"\n";

			}
	    }
	}
}else{
	$display.="<tr><td colspan=8 align=center>$i_no_record_exists_msg</td></tr>";
}
$display.="</table>";

if($format==1){ # Web
	echo $display;
}
else if($format==2){ # CSV
	$file_name="attendance_report_$startDate_$endDate.csv";
    output2browser($csv_content,$file_name);
}
intranet_closedb();
?>
