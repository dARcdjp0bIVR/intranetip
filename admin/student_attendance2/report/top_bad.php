<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lcard = new libcardstudentattend2();
$select_type = "<SELECT name=BadType>\n";
$select_type .= "<OPTION value='".CARD_BADACTION_LUNCH_NOTINLIST."'>$i_StudentAttendance_BadLogs_Type_NotInLunchList</OPTION>\n";
$select_type .= "<OPTION value='".CARD_BADACTION_LUNCH_BACKALREADY."'>$i_StudentAttendance_BadLogs_Type_GoLunchAgain</OPTION>\n";
$select_type .= "<OPTION value='".CARD_BADACTION_FAKED_CARD_AM."'>$i_StudentAttendance_BadLogs_Type_FakedCardAM</OPTION>\n";
$select_type .= "<OPTION value='".CARD_BADACTION_FAKED_CARD_PM."'>$i_StudentAttendance_BadLogs_Type_FakedCardPM</OPTION>\n";
$select_type .= "<OPTION value='".CARD_BADACTION_NO_CARD_ENTRANCE."'>$i_StudentAttendance_BadLogs_Type_NoCardRecord</OPTION>\n";
$select_type .= "</SELECT>\n";

$array_top = array(1,5,10,20,50,100,200,300,400,500);
$select_top = getSelectByValue($array_top,"name=TopNumber",10,0,1);

?>
 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].StartDate.value = dateValue;
          }
          function calendarCallback2(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].EndDate.value = dateValue;
          }

 // -->
 </script>

 <?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_StudentBadRecords,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 target=_blank action=top_bad_report.php method=GET>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?=$i_StudentAttendance_BadLogs_Type?>:</td><td><?=$select_type?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_DisplayTop?>:</td><td><?=$select_top?></td></tr>
<tr><td align=right><?=$i_general_startdate?>:</td><td>
<input type=text name=StartDate value='<?=date('Y-m-d')?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td align=right><?=$i_general_enddate?>:</td><td>
<input type=text name=EndDate value='<?=date('Y-m-d')?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>