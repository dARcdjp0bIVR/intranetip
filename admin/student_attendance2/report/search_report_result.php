<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libwordtemplates.php");

if(!$sys_custom['SmartCardAttendance_Report_Search']){
	header("Location: index.php");
}

intranet_opendb();




$classes = explode(",",$selected_classes);
if(sizeof($classes)<=0)
	header("Location: search_report.php");


$selected_classes="";
for($i=0;$i<sizeof($classes);$i++)
	$selected_classes.="'".$classes[$i]."',";
if($selected_classes!="")
	$selected_classes=substr($selected_classes,0,strlen($selected_classes)-1);

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();


# process date range
//$current_month=date('n');
//$current_year =date('Y');

if($startStr=="" || $endStr==""){
	/*
	if($current_month>=9){
		$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year));
		$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year+1));
	}else{
		$startDate = date('Y-m-d',mktime(0, 0, 0, 9, 1, $current_year-1));
		$endDate   = date('Y-m-d',mktime(0, 0, 0, 8, 31,$current_year));
	}
	*/
	$ts = strtotime(date('Y-m-d'));
	$startDate = getStartOfAcademicYear($ts);
	$endDate = getEndOfAcademicYear($ts);
}else{
	$startDate = $startStr;
	$endDate = $endStr;
}


$cond_day_type="";

//$username_field = getNameFieldByLang("a.");
$username_field = "a.ChineseName,a.EnglishName";

if($session==PROFILE_DAY_TYPE_AM){  # AM
	$cond_day_type = " AND c.DayType='".PROFILE_DAY_TYPE_AM."'";
	if($attendance_type==CARD_STATUS_LATE){ # Late
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."'";

	}
	else if($attendance_type==CARD_STATUS_ABSENT){ # Absent
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."'";
	}
	else if($attendance_type==PROFILE_TYPE_EARLY){ # Early leave
		$cond_day_type.=" AND c.RecordType='".PROFILE_TYPE_EARLY."'";
	}
	else{ # All ( Late or Absent or Early leave)
		$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."' OR c.RecordType='".PROFILE_TYPE_EARLY."')";
	}
}
else if($session==PROFILE_DAY_TYPE_PM){ # PM
	$cond_day_type = " AND c.DayType='".PROFILE_DAY_TYPE_PM."'";

	if($attendance_type==CARD_STATUS_LATE){  #Late
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_LATE."'";
	}
	else if($attendance_type==CARD_STATUS_ABSENT){  # Absent
		$cond_day_type.=" AND c.RecordType='".CARD_STATUS_ABSENT."'";
	}
	else if($attendance_type==PROFILE_TYPE_EARLY){  # Early leave
		$cond_day_type.=" AND c.RecordType='".PROFILE_TYPE_EARLY."'";
	}
	else{  # ALL(Late or Absent or Early leave)
		$cond_day_type .=" AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."' OR c.RecordType='".PROFILE_TYPE_EARLY."')";
	}
}
else{  # ALL (AM or PM)

	if($attendance_type==CARD_STATUS_LATE){ # Late
				$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' )AND c.RecordType='".CARD_STATUS_LATE."'";

	}
	else if($attendance_type==CARD_STATUS_ABSENT){ # Absent
				$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' )AND c.RecordType='".CARD_STATUS_ABSENT."'";

	}
	else if($attendance_type==PROFILE_TYPE_EARLY){ # Early leave
				$cond_day_type =" AND (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' )AND c.RecordType='".PROFILE_TYPE_EARLY."'";

	}
	else{ # ALL (LATE or ABSENT or EARLY LEAVE)
				$cond_day_type =" AND ( (c.DayType='".PROFILE_DAY_TYPE_AM."' OR c.DayType='".PROFILE_DAY_TYPE_PM."' )AND (c.RecordType='".CARD_STATUS_LATE."' OR c.RecordType='".CARD_STATUS_ABSENT."' OR c.RecordType='".PROFILE_TYPE_EARLY."') )";
	}
}

# reason
$cond_reason ="";

if($match=="1"){
	$cond_reason=" AND c.Reason='".intranet_htmlspecialchars($reason)."'";
}else{
	if($reason!="")
		$cond_reason =" AND c.Reason LIKE '%".intranet_htmlspecialchars($reason)."%'";
}

# waived
$cond_waived="";
if($waived==1){
	$cond_waived=" AND c.RecordStatus=1";
}
else if($waived==2){
	$cond_waived=" AND c.RecordStatus!=1";
}
else{
	# all ( waived or not wavied)
}

	$join_profile_table =" LEFT OUTER JOIN PROFILE_STUDENT_ATTENDANCE AS d ON (c.ProfileRecordID = d.StudentAttendanceID) ";

	# two possibles:
	#  1. (Waived and No Record in PROFILE_STUDENT_ATTENDANCE ) OR
	#  2. (No Waived and Record exists in PROFILE_STUDENT_ATTENDANCE)
	$cond_profile_table =" AND (  (d.StudentAttendanceID IS NOT NULL AND c.RecordStatus!=1) OR (d.StudentAttendanceID IS NULL AND c.RecordStatus=1) ) ";

	$sql="
		SELECT
		a.UserID,
		a.UserLogin,
		$username_field,
		a.ClassName,
		a.ClassNumber,
		c.RecordDate,
		c.RecordType AS AttendanceType,
		c.DayType AS Session,
		c.Reason,
		c.RecordStatus
		FROM INTRANET_USER AS a,
		CARD_STUDENT_PROFILE_RECORD_REASON AS c
		$join_profile_table
		WHERE a.UserID=c.StudentID AND a.RecordType=2 AND a.RecordStatus IN(0,1,2) AND a.ClassName IN($selected_classes)
		AND c.RecordDate>='$startDate' AND c.RecordDate<='$endDate'
		$cond_waived
		$cond_reason
		$cond_day_type
		$cond_profile_table
		ORDER BY a.ClassName,a.ClassNumber,c.RecordDate

	";

$final_result = $lcardattend->returnArray($sql);

################# Build array of Attendance Data from Daily Log tables #################
# CAL target Year months
$year_month = array();
$start_ts = strtotime($startDate);
$end_ts = strtotime($endDate);

$temp_ts=$start_ts;
while($temp_ts<=$end_ts){
	$year = date('Y',$temp_ts);
	$month = date('m',$temp_ts);
	$lcardattend->createTable_Card_Student_Daily_Log($year,$month);
	$year_month[] = array($year,$month);
	$temp_ts=mktime(0,0,0,date('m',$temp_ts)+1,1,date('Y',$temp_ts));
}

##

//$target_status = $attendance_type==""? "'".CARD_STATUS_ABSENT."','".CARD_STATUS_LATE."','".PROFILE_TYPE_EARLY."'" : "'".$attendance_type."'";

if($session==""){
	$cond_session2 = "((AMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.") OR PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.")) OR ((AMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM.")) OR (PMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))))";
	//$cond_session2 = " ( b.AMStatus IN ($target_status) OR b.PMStatus IN ($target_status) )";
}
else if($session==PROFILE_DAY_TYPE_AM){
	//$cond_session2 = " b.AMStatus IN ($target_status) ";
	$cond_session2 = "((AMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.")) OR ((AMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))))";
}
else if($session==PROFILE_DAY_TYPE_PM){
	//$cond_session2 = " b.PMStatus IN ($target_status) ";
	$cond_session2 = "((PMStatus IN (".CARD_STATUS_ABSENT.",".CARD_STATUS_LATE.")) OR ((PMStatus = ".CARD_STATUS_PRESENT." AND LeaveStatus IN (".CARD_LEAVE_AM.",".CARD_LEAVE_PM."))))";
}


## get attendance record from DAILY LOG Tables
$attendance_data = array();
for($i=0;$i<sizeof($year_month);$i++){
		list($year,$month) = $year_month[$i];
		$table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
		$date_field = "CONCAT('$year','-','$month','-',IF(b.DayNumber<=9,CONCAT('0',b.DayNumber),b.DayNumber))";
		$sql="
				SELECT
					b.UserID,
					$date_field,
					b.AMStatus,
					b.PMStatus,
					b.LeaveStatus
				FROM
					$table_name AS b ,
					INTRANET_USER AS a
				WHERE
					$cond_session2 AND
					$date_field >='$startDate' AND
					$date_field <= '$endDate' AND
					a.ClassName IN ($selected_classes) AND
					a.UserID = b.UserID
				ORDER BY
					b.DayNumber
		";
		//echo $sql."<Br>";
		$temp = $lcardattend->returnArray($sql,4);
		for($j=0;$j<sizeof($temp);$j++){
			list($user_id,$record_date,$am_status,$pm_status,$leave_status) = $temp[$j];
			$attendance_data[$user_id][$record_date]['AM'] = $am_status;
			$attendance_data[$user_id][$record_date]['PM'] = $pm_status;
			if(($leave_status == 1) ||($leave_status == 2))
				$attendance_data[$user_id][$record_date]['EarlyLeave'] = 3;
		}
}
##################### end build array ###############


##################### output ######################

# csv
$csv_content="\"$i_Profile_Attendance ( $startDate $i_Profile_To $endDate )\",\"$i_general_report_creation_time: ".date('Y-m-d H:i:s')."\"\n";
//$csv_content.="\"$i_UserLogin\",\"$i_UserStudentName\",\"$i_ClassNameNumber\",\"$i_Attendance_Date\",\"$i_Attendance_attendance_type\",\"$i_Attendance_DayType\",\"$i_Attendance_Reason\",\"$i_SmartCard_Frontend_Take_Attendance_Waived\"\n";
$csv_content.="\"$i_UserLogin\",";
//$csv_content.="\"$i_UserStudentName\",";
switch($lcardattend->ReportDisplayStudentNameFormat())
{
	case 1:	
		$csv_content.="\"$i_UserChineseName\",";
		break;
	case 2:	
		$csv_content.="\"$i_UserEnglishName\",";
		break;
	default:
		$csv_content.="\"$i_UserChineseName\",";
		$csv_content.="\"$i_UserEnglishName\",";
		break;
}
$csv_content.="\"$i_ClassNameNumber\",\"$i_Attendance_Date\",\"$i_Attendance_attendance_type\",\"$i_Attendance_DayType\",\"$i_Attendance_Reason\",\"$i_SmartCard_Frontend_Take_Attendance_Waived\"\n";

# DISPLAY
$display ="<link rel=\"stylesheet\" href=\"/templates/style.css\">";
$display.="<table width=95% border=0><tr><th>$i_Profile_Attendance ( $startDate $i_Profile_To $endDate )&nbsp;&nbsp;$i_general_report_creation_time: ".date('Y-m-d H:i:s')."</th></tr></table>";
/*$display .="<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=1 cellspacing=0>
	<tr class=tableTitle>
	<th>$i_UserLogin</th>
	<th>$i_UserStudentName</th>
	<th>$i_ClassNameNumber</th>
*/

$display .="<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=1 cellspacing=0>
	<tr class=tableTitle>
	<th>$i_UserLogin</th>";
	
switch($lcardattend->ReportDisplayStudentNameFormat())
{
	case 1:	
		$display .= "<th>$i_UserChineseName</th>";
		break;
	case 2:	
		$display .= "<th>$i_UserEnglishName</th>";
		break;
	default:
		$display .= "<th>$i_UserChineseName</th>";
		$display .= "<th>$i_UserEnglishName</th>";
		break;
}
	
//$display .="<th>$i_UserStudentName</th>";
$display .="<th>$i_ClassNameNumber</th>	

	<th>$i_Attendance_Date</th>

	<th>$i_Attendance_attendance_type</th>
	<th>$i_Attendance_DayType</th>
	<th>$i_Attendance_Reason</th>
	<th>$i_SmartCard_Frontend_Take_Attendance_Waived</th>
	</tr>";
	
if(sizeof($final_result)>0){
	$css="2";
	for($i=0;$i<sizeof($final_result);$i++){
		//list($uid,$u_login,$sname,$cname,$cnum,$record_date,$attend_type,$t_session,$t_reason,$t_waived) = $final_result[$i];
		list($uid,$u_login,$sname_ch, $sname_en,$cname,$cnum,$record_date,$attend_type,$t_session,$t_reason,$t_waived) = $final_result[$i];
		if($sname_ch == "")
	     	$sname_ch = $sname_en;
	     if($sname_en == "")
	     	$sname_en = $sname_ch;
	     if($sname_ch == "")
	     	$sname_ch = "&nbsp;";
	     if($sname_en == "")
	     	$sname_en = "&nbsp;";
	     	
/*
		echo "Checking: ".$uid." ".$t_session." ".$attend_type."<BR>";
		## compare with daily log table, skip if they are different
		if($t_session==PROFILE_DAY_TYPE_AM && $attendance_data[$uid][$record_date]['AM'] != $attend_type)
		{
			echo $uid." ".$attendance_data[$uid][$record_date]['AM']." ".$attend_type."<BR>";
			continue;
		}
		if($t_session==PROFILE_DAY_TYPE_PM && $attendance_data[$uid][$record_date]['PM'] != $attend_type)
		{
			echo $uid." ".$attendance_data[$uid][$record_date]['PM']." ".$attend_type."<BR>";
			continue;
		}
		if(($attend_type != $attendance_data[$uid][$record_date]['EarlyLeave']) && (($t_session==PROFILE_DAY_TYPE_AM) || ($t_session==PROFILE_DAY_TYPE_PM)))
		{
			echo $uid." ".$attendance_data[$uid][$record_date]['EarlyLeave']." ".$attend_type."<BR>";
			continue;
		}
*/
		if($t_session==PROFILE_DAY_TYPE_AM){
			if($attendance_data[$uid][$record_date]['AM'] == $attend_type)
			{

			}else if($attendance_data[$uid][$record_date]['EarlyLeave'] == $attend_type){
				
			}
			else{
				continue;
			}
		}
		if($t_session==PROFILE_DAY_TYPE_PM){
			if($attendance_data[$uid][$record_date]['PM'] == $attend_type)
			{

			}else if($attendance_data[$uid][$record_date]['EarlyLeave'] == $attend_type){

			}
			else{
				continue;
			}
		}
		
		$str_session=$t_session==PROFILE_DAY_TYPE_AM?$i_StudentAttendance_Slot_AM:$i_StudentAttendance_Slot_PM;
		if ($attend_type == CARD_STATUS_LATE) {
			$str_attend_type = $i_Profile_Late;
		} elseif ($attend_type == CARD_STATUS_ABSENT) {
			$str_attend_type = $i_Profile_Absent;
		} else {
			$str_attend_type = $i_Profile_EarlyLeave;
		}
		$t_reason = intranet_undo_htmlspecialchars($t_reason);
		$str_waived = $t_waived==1?$i_general_yes:$i_general_no;
		$css=$css==""?"2":"";
	    $row ="	<Td class=tableContent$css>$u_login</td>";
	    
	    switch($lcardattend->ReportDisplayStudentNameFormat())
		{
			case 1:
				$row .= "<td  class=tableContent$css>$sname_ch</td>";
				break;
			case 2:
				$row .= "<td  class=tableContent$css>$sname_en</td>";
				break;
			default:
				$row .= "<td  class=tableContent$css>$sname_ch</td>";
				$row .= "<td  class=tableContent$css>$sname_en</td>";
				break;
		}
		
		//		<Td class=tableContent$css>$sname</td>
				
		$row .="<td class=tableContent$css>$cname".($cnum==""?"":"($cnum)")."&nbsp;</td>
				<Td class=tableContent$css>$record_date</td>
				<Td class=tableContent$css>$str_attend_type</td>
				<td class=tableContent$css>$str_session</td>
				<Td class=tableContent$css>$t_reason&nbsp;</td>
				<Td class=tableContent$css>$str_waived</td>
				</tr>";
		$display.=$row;
	 	//$csv_content.="\"$u_login\",\"$sname\",\"$cname".($cnum==""?"":"($cnum)")."\",\"$record_date\",\"$str_attend_type\",\"$str_session\",\"$t_reason\",\"$str_waived\"\n";
	 	$csv_content.="\"$u_login\",";
	 	//$csv_content.="\"$sname\",";
	 	switch($lcardattend->ReportDisplayStudentNameFormat())
		{
			case 1:
				$csv_content.="\"$sname_ch\",";
				break;
			case 2:
				$csv_content.="\"$sname_en\",";
				break;
			default:
				$csv_content.="\"$sname_ch\",";
				$csv_content.="\"$sname_en\",";
				break;
		}
	 	$csv_content.="\"$cname".($cnum==""?"":"($cnum)")."\",\"$record_date\",\"$str_attend_type\",\"$str_session\",\"$t_reason\",\"$str_waived\"\n";

	}
}else{
	$display.="<tr><td colspan=8 align=center>$i_no_record_exists_msg</td></tr>";
}
$display.="</table>";

if($format==1){ # Web
	include_once("../../../templates/fileheader.php");
	echo $display;
}
else if($format==2){ # CSV
	$file_name="attendance_report_$startDate_$endDate.csv";
    output2browser($csv_content,$file_name);
}
intranet_closedb();
?>
