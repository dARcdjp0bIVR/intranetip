<?
// using: kenneth chung
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libcal.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();
$lclass = new libclass();
if ($ClassID != "")
	$classes[0]['ClassName'] = $lclass->getClassName($ClassID);
else 
	$classes = $lclass->getClassList();

$months = $i_general_MonthShortForm;

// get count value for ABSENT
$filename="$intranet_root/file/std_profile_attend_count.txt";
$count_type = get_file_content($filename);
$count_value = $count_type==1?0.5:1;


$absent_count_value = $count_value;
$present_count_value = $count_value;
$late_count_value = 1;
$early_leave_count_value = 1;

//$notation_symbol = array($i_StudentAttendance_Symbol_Present, $i_StudentAttendance_Symbol_Absent,$i_StudentAttendance_Symbol_Late,$i_StudentAttendance_Symbol_Outing, $i_StudentAttendance_Symbol_EarlyLeave, $i_StudentAttendance_Symbol_SL, $i_StudentAttendance_Symbol_AR, $i_StudentAttendance_Symbol_LE, $i_StudentAttendance_Symbol_Truancy);

// 0 - Present
// 1 - Absent
// 2 - Late
// 3 - Outing
// 4 - Early Leave
// 5 - Sick Leave
// 6 - �ư�
// 7 - Late and Early Leave
// 8 - Truancy
// 9 - Waived
$notation_symbol[0] = $i_StudentAttendance_Symbol_Present;
$notation_symbol[1] = $i_StudentAttendance_Symbol_Absent;
$notation_symbol[2] = $i_StudentAttendance_Symbol_Late;
$notation_symbol[3] = $i_StudentAttendance_Symbol_Outing;
$notation_symbol[4] = $i_StudentAttendance_Symbol_EarlyLeave;
$notation_symbol[5] = $i_StudentAttendance_Symbol_SL;
$notation_symbol[6] = $i_StudentAttendance_Symbol_AR;
$notation_symbol[7] = $i_StudentAttendance_Symbol_LE;
$notation_symbol[8] = $i_StudentAttendance_Symbol_Truancy;
$notation_symbol[9] = $i_StudentAttendance_Symbol_Waived;

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

if ($Year == "")
{
	$Year = date('Y');
}
if ($Month == "")
{
	$Month = date('m');
}
if (strlen($Month)==1)
{
	$Month = "0".$Month;
}

$content = "";
for ($z=0; $z< sizeof($classes); $z++) {
	# Get Student List
	//$namefield = getNameFieldByLang();
	$sql = "SELECT UserID, ClassNumber, ChineseName, EnglishName, Gender FROM INTRANET_USER WHERE ClassName = '".$classes[$z]['ClassName']."' AND RecordStatus IN (0,1,2) AND RecordType = 2 ORDER BY ClassName, ClassNumber, EnglishName";
	$students = $lcard->returnArray($sql,5);
	//debug_r($students);
	
	$lcard->createTable_Card_Student_Daily_Log($Year,$Month);
	//$sql = "SELECT DISTINCT DayNumber FROM CARD_STUDENT_DAILY_LOG_".$Year."_".$Month;
	$sql="SELECT DISTINCT a.DayNumber FROM CARD_STUDENT_DAILY_LOG_".$Year."_".$Month." AS a LEFT OUTER JOIN INTRANET_USER AS b ON (a.UserID=b.UserID) WHERE b.RecordType=2 AND b.ClassName='".$classes[$z]['ClassName']."' AND b.RecordStatus IN(0,1,2)";
	$total_day = $lcard->returnArray($sql,1);
	
	/*
	$lcal = new libcal();
	$target_month = substr($Month,0,1)=="0"?substr($Month,1):$Month;
	$temp = $lcal->returnCalendarArray($target_month,$Year);
	$total_day = array();
	for($i=0;$i<sizeof($temp);$i++){
		if($temp[$i]=="") continue;
		$targetdate =$Year."-".$Month."-".date("d",$temp[$i]);
	
		if($lcard->isRequiredToTakeAttendanceByDate(".$classes[$z]['ClassName'].",$targetdate)){
			$total_day[] = $targetdate;
		}
	}
	*/
	
	if(sizeof($total_day)<=0){ # No records
		$content .=  "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
		$content .= "<tr class='tableTitle'><td>".$i_UserClassName.": ".$classes[$z]['ClassName']." (".$Year." ".$months[$Month-1].")</td></tr>";
		$content .= "<tr><td align=center>$i_StudentAttendance_Report_NoRecord</td></tr>";
		$content .= "</table><br>";
		
		continue;
	}
	
	$day_num = date("t",mktime(0,0,0,$Month,1,$Year));
	
	$col_span = 3;
	
	$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
	$content .= "<tr class='tableTitle'><td colspan=".$col_span.">".$i_UserClassName.": ".$classes[$z]['ClassName']." (".$Year." ".$months[$Month-1].")</td></tr>";
	$content .= "<tr>";
	$content .= "<td>";
	
	// main content
	$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
	
	//        menu header
	$content .= "<tr align=\"center\">";
	$content .= "<td rowspan=\"3\">$i_UserClassNumber</td>";
	switch($lcard->ReportDisplayStudentNameFormat())
	{
		case 1:	
			$content .= "<td rowspan=\"3\">$i_UserChineseName</td>";
			break;
		case 2:	
			$content .= "<td rowspan=\"3\">$i_UserEnglishName</td>";
			break;
		default:
			$content .= "<td rowspan=\"3\">$i_UserChineseName</td>";
			$content .= "<td rowspan=\"3\">$i_UserEnglishName</td>";
			break;
	}
	/*
	$content .= "<td rowspan=\"3\">$i_UserChineseName</td>";
	$content .= "<td rowspan=\"3\">$i_UserEnglishName</td>";
	*/
	$content .= "<td rowspan=\"3\">$i_UserGender</td>";
	$content .= "<td>$i_Attendance_Date</td>";
	for ($i=1;$i<=$day_num;$i++){
			$content .= "<td colspan=\"2\">".$i."</td>";
	}
	
	$content .= "<td rowspan=\"3\">";
	$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
	$content .= "<tr>";
	$content .= "<td colspan=\"4\">$i_StudentAttendance_FullReport_Days1".count($total_day)."$i_StudentAttendance_FullReport_Days2</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td rowspan=\"2\" width='25%'>$i_StudentAttendance_Status_Present</td>";
	$content .= "<td rowspan=\"2\" width='25%'>$i_StudentAttendance_Status_Absent</td>";
	$content .= "<td rowspan=\"2\" width='25%'>$i_StudentAttendance_Status_Late</td>";
	$content .= "<td rowspan=\"2\" width='25%'>$i_StudentAttendance_Status_EarlyLeave</td>";
	$content .= "</tr>";
	$content .= "</table>";
	$content .= "</td>";
	$content .= "</tr>";
	
	$content .= "<tr>";
	
	$content .= "<td rowspan=\"2\">$i_Attendance_Num_Of_Day</td>";
	for ($i=1;$i<=$day_num;$i++){
					$timestamp = mktime(0,0,0,$Month,$i,$Year);
					$content .= "<td colspan=\"2\" align=\"center\">".$i_DayType4[date("w",$timestamp)]."</td>";
	}
	$content .= "</tr>";
	
	$content .= "<tr align=\"center\">";
	for ($i=1;$i<=$day_num;$i++){
			$content .= "<td><font size=\"-2\">$i_DayTypeAM</font></td>";
			$content .= "<td><font size=\"-2\">$i_DayTypePM</font></td>";
	}
	$content .= "</tr>";
	
	#############
	# init month count
	# 0 - Present
	# 1 - Absent
	# 2 - Late
	# 3 - Truancy
	# 4 - EarlyLeave
	for ($i=1;$i<=$day_num;$i++){
		/*
			$monthly_report[$i]['0'] = 0;
			$monthly_report[$i]['0'] = 0;
			$monthly_report[$i]['1'] = 0;
			$monthly_report[$i]['2'] = 0;
			//$monthly_report[$i]['3'] = 0;
			$monthly_report[$i]['4'] = 0;
		*/
			$new_monthly_report[$i]['0']['am'] = 0;
			$new_monthly_report[$i]['0']['pm'] = 0;
			$new_monthly_report[$i]['1']['am'] = 0;
			$new_monthly_report[$i]['1']['pm'] = 0;
			$new_monthly_report[$i]['2']['am'] = 0;
			$new_monthly_report[$i]['2']['pm'] = 0;
			//$monthly_report[$i]['3'] = 0;
			$new_monthly_report[$i]['4']['am'] = 0;
			$new_monthly_report[$i]['4']['pm'] = 0;
	}
	
	$result = $lcard->retrieveClassMonthData($classes[$z]['ClassName'], $Year, $Month);
	
	for($i=0; $i<sizeof($students); $i++)
	{
			list($sid, $class_num, $cname, $ename, $gender) = $students[$i];
			$count_total_student++;
	
			#############
			# init total count
			# 0 - Present
			# 1 - Absent
			# 2 - Late
			# 3 - Truancy
			# 4 - EarlyLeave
			$s_stat[0] = 0;
			$s_stat[1] = 0;
			$s_stat[2] = 0;
			$s_stat[3] = 0;
			$s_stat[4] = 0;
	
			$css = ($i%2?"2":"");
			$content .= "<tr align=\"center\" class='tableContent$css'>";
			$content .= "<td>".$class_num."</td>";
			switch($lcard->ReportDisplayStudentNameFormat())
			{
				case 1:
					$content .= "<td>$cname</td>";
					break;
				case 2:
					$content .= "<td>$ename</td>";
					break;
				default:
					$content .= "<td>$cname</td>";
					$content .= "<td>$ename</td>";
					break;
			}
			
			//$content .= "<td>".$cname."</td>";
			//$content .= "<td>".$ename."</td>";
			$content .= "<td>".$gender."</td>";
			$content .= "<td>&nbsp;</td>";
	
			for($j=1; $j<=$day_num; $j++)
			{
					if(sizeof($result[$sid][$j]) != 0)
					{
							list($am, $pm, $leave, $am_late_waive, $pm_late_waive, $am_absent_waive, $pm_absent_waive, $am_early_waive, $pm_early_waive) = $result[$sid][$j];
	
							if($am == 3) // outing = > present
									$am = '0';
	
							if($pm == 3) // outing = > present
									$pm = '0';
	
							 $c = "";
							if($am != "")
							{
									//$content .= "<td>".$notation_symbol[$am]."</td>";
									$c .= $notation_symbol[$am];
	
								   	//$s_stat[$am]++; // count
									//$monthly_report[$j][$am] = $monthly_report[$j][$am] + $count_value;
	
									if($am=='0'){
	   	                                $s_stat[$am]+=$present_count_value;
				  	                    //$monthly_report[$j][$am] = $monthly_report[$j][$am] + $present_count_value;
				  	                    //$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + $present_count_value;
				  	                    $new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
				  	                    
			  	                    }else if($am=='1'){
		          	                    $s_stat[$am]+=$absent_count_value;
				  	                    //$monthly_report[$j][$am] = $monthly_report[$j][$am] + $absent_count_value;
				  	                    //$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + $absent_count_value;
				  	                    $new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
	
										if ($am_absent_waive == 1) {
											if($c!="") $c.="<br>";
											$c.= $notation_symbol[9];
											$s_stat[$am]-=$absent_count_value;
											//$monthly_report[$j][$am] = $monthly_report[$j][$am] - $absent_count_value;
											//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - $absent_count_value;
											$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - 1;
											$s_stat['0']+=$present_count_value;
											//$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
											//$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + $present_count_value;
											$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + 1;
										}
		          	                }else if($am=='2'){
	   		          	                $s_stat['0']+=$present_count_value;
	   		          	                $s_stat[$am]+=$late_count_value;
			          	                //$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
				  	                    //$monthly_report[$j][$am] = $monthly_report[$j][$am] + $late_count_value;
				  	                    //$new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + $present_count_value;
				  	                    //$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + $late_count_value;
				  	                    $new_monthly_report[$j]['0']['am'] = $new_monthly_report[$j]['0']['am'] + 1;
				  	                    $new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] + 1;
	
										if(!$am_late_waive){
											if($c!="") $c.="<br>";
											$c.= $notation_symbol[9];
											$s_stat[$am]-=$late_count_value;
											//$monthly_report[$j][$am] = $monthly_report[$j][$am] - $late_count_value;
											//$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - $late_count_value;
											$new_monthly_report[$j][$am]['am'] = $new_monthly_report[$j][$am]['am'] - 1;
										}
			          	            }
	
							}
							// eary leave info, added on 2008-03-13
							if($leave==1){
		                           if($c!="") $c.="<br>";
									$c.= $notation_symbol[4];
									$s_stat[4]+=$early_leave_count_value;
			  	                    //$monthly_report[$j][4] = $monthly_report[$j][4] + $early_leave_count_value;
			  	                    //$new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] + $early_leave_count_value;
			  	                    $new_monthly_report[$j][4]['am'] = $new_monthly_report[$j][4]['am'] + 1;
			  	                    
			  	                    if($am_early_waive == 1){
				  	                    $c.=$notation_symbol[9];
			  	                    }
		                    }
	
		                    if($c!=""){
			                    $content.="<Td>$c</td>";
			                }
							else
									$content .= "<td>&nbsp;</td>";
	
	
							$d ="";
							if($pm != "")
							{
									//$content .= "<td>".$notation_symbol[$pm]."</td>";
									$d .= $notation_symbol[$pm];
	
	
									//$s_stat[$pm]++;
									//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $count_value;
									if($pm=='0'){
		                                $s_stat[$pm]+=$present_count_value;
				  	                    //$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $present_count_value;
				  	                    //$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + $present_count_value;
				  	                    $new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
			  	                    }else if($pm=='1'){
		          	                    $s_stat[$pm]+=$absent_count_value;
				  	                    //$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $absent_count_value;
				  	                    //$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + $absent_count_value;
				  	                    $new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
	
										if ($pm_absent_waive == 1) {
											if($d!="") $d.="<br>";
											$d.= $notation_symbol[9];
											$s_stat[$pm]-=$absent_count_value;
											//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] - $absent_count_value;
											//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - $absent_count_value;
											$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - 1;
											$s_stat['0']+=$present_count_value;
											//$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
											//$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + $present_count_value;
											$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + 1;
										}
		          	                }else if($pm=='2'){
			          	                $s_stat['0']+=$present_count_value;
			          	                $s_stat[$pm]+=$late_count_value;
			          	                //$monthly_report[$j]['0'] = $monthly_report[$j]['0'] + $present_count_value;
				  	                    //$monthly_report[$j][$pm] = $monthly_report[$j][$pm] + $late_count_value;
				  	                    //$new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + $present_count_value;
				  	                    //$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + $late_count_value;
				  	                    $new_monthly_report[$j]['0']['pm'] = $new_monthly_report[$j]['0']['pm'] + 1;
				  	                    $new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] + 1;
	
										if(!$pm_late_waive){
											if($d!="") $d.="<br>";
											$d.= $notation_symbol[9];
											$s_stat[$pm]-=$late_count_value;
											//$monthly_report[$j][$pm] = $monthly_report[$j][$pm] - $late_count_value;
											//$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - $late_count_value;
											$new_monthly_report[$j][$pm]['pm'] = $new_monthly_report[$j][$pm]['pm'] - 1;
										}
			          	            }
							}
							// eary leave info, added on 2008-03-13
							if($leave==2){
		                        	if($d!="") $d.="<Br>";
									$d .= $notation_symbol[4];
								   	$s_stat[4]+=$early_leave_count_value;
			  	                    //$monthly_report[$j][4] = $monthly_report[$j][4] + $early_leave_count_value;
			  	                    //$new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] + $early_leave_count_value;
			  	                    $new_monthly_report[$j][4]['pm'] = $new_monthly_report[$j][4]['pm'] + 1;
			  	                    
			  	                    if($pm_early_waive == 1){
				  	                    $d.=$notation_symbol[9];
			  	                    }
		                    }
	
		                    if($d!=""){
			                    $content .="<Td>$d</td>";
			                }
							else
									$content .= "<td>&nbsp;</td>";
					}
					else
					{
							$content .= "<td>&nbsp;</td><td>&nbsp;</td>";
					}
	
			}
			$content .= "<td>";
			$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
			$content .= "<tr>";
			for($k=0; $k<sizeof($s_stat); $k++)
			{
		        if($k==3) continue;
		        $percent_width = sizeof($s_stat)-1>0? (100/sizeof($s_stat)-1)."%":'100%';
					$content .= "<td width='$percent_width'>".$s_stat[$k]."</td>";
			}
			$content .= "</tr>";
			$content .= "</table>";
			$content .= "</td>";
			$content .= "</tr>";
	
	}
	
	### Daily Statistic ###
	$content .= "<tr>";
	$content .= "<td rowspan=\"4\" colspan=\"4\" align=\"right\">$i_StudentAttendance_Daily_Stat</td>";
	$content .= "<td>$i_StudentAttendance_Stat_Present</td>";
	$totalPresent = 0;
	for ($i=1;$i<=$day_num;$i++){
			//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['0']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['0']['am']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['0']['pm']."</td>";
			$totalPresent = $totalPresent + $monthly_report[$i]['0'];
	}
	$content .= "</tr>";
	
	$content .= "<tr>";
	$content .= "<td>$i_StudentAttendance_Stat_Absent</td>";
	$totalAbsent = 0;
	for ($i=1;$i<=$day_num;$i++){
			//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['1']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['1']['am']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['1']['pm']."</td>";
			$totalAbsent = $totalAbsent + $monthly_report[$i]['1'];
	}
	$content .= "</tr>";
	
	$content .= "<tr>";
	$content .= "<td>$i_StudentAttendance_Stat_Late</td>";
	for ($i=1;$i<=$day_num;$i++){
			//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['2']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['2']['am']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['2']['pm']."</td>";
	}
	$content .= "</tr>";
	/*
	$content .= "<tr>";
	$content .= "<td>$i_StudentAttendance_Stat_Truancy</td>";
	for ($i=1;$i<=$day_num;$i++){
			$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['3']."</td>";
	}
	$content .= "</tr>";
	*/
	
	$content .= "<tr>";
	$content .= "<td>$i_StudentAttendance_Status_EarlyLeave</td>";
	for ($i=1;$i<=$day_num;$i++){
			//$content .= "<td colspan=\"2\" align=\"center\">".$monthly_report[$i]['4']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['4']['am']."</td>";
			$content .= "<td align=\"center\">".$new_monthly_report[$i]['4']['pm']."</td>";
	}
	$content .= "</tr>";
	### End of Daily Statistic ###
	
	$content .= "</table>";
	
	$content .= "</td>";
	
	$content .= "<td>";
	$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
	$content .= "<tr>";
	$content .= "<td rowspan=\"3\" algin=\"center\">";
	$content .= "$i_Discipline_System_general_remark";
	$content .= "</td>";
	$content .= "</tr>";
	$content .= "<tr></tr>";
	$content .= "<tr></tr>";
	$content .= "</table>";
	$content .= "</td>";
	
	$content .= "<td valign=\"top\">";
	$content .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=0 cellspacing=0>";
	$content .= "<tr>";
	$content .= "<td rowspan=\"3\">$i_StudentAttendance_Monthly_Level_Stat</td>";
	$content .= "</tr>";
	$content .= "</table><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$content .= "<tr>";
	$content .= "<td>".$i_StudentAttendance_Attendance_Number.": ".$count_total_student."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$i_StudentAttendance_Average_Present.": ".round(($totalPresent/count($total_day)),2)."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$i_StudentAttendance_Average_Absent.": ".round(($totalAbsent/count($total_day)),2)."</td>";
	$content .= "</tr>";
	$content .= "</table>";
	
	$content .= "<br>";
	$content .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
	$content .= "<tr>";
	$content .= "<td>$i_StudentAttendance_Symbol_Reference: </td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[0]." - ".$i_StudentAttendance_Status_Present."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[1]." - ".$i_StudentAttendance_Status_Absent."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[2]." - ".$i_StudentAttendance_Status_Late."</td>";
	$content .= "</tr>";
	//$content .= "<tr>";
	//$content .= "<td>".$notation_symbol[3]." - ".$i_StudentAttendance_Status_Outing."</td>";
	//$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[4]." - ".$i_StudentAttendance_Status_EarlyLeave."</td>";
	$content .= "</tr>";
	/*
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[5]." - ".$i_StudentAttendance_Status_SL."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[6]." - ".$i_StudentAttendance_Status_AR."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[7]." - ".$i_StudentAttendance_Status_LE."</td>";
	$content .= "</tr>";
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[8]." - ".$i_StudentAttendance_Status_Truancy."</td>";
	$content .= "</tr>";
	*/
	$content .= "<tr>";
	$content .= "<td>".$notation_symbol[9]." - ".$i_StudentAttendance_Status_Waived."</td>";
	$content .= "</tr>";
	$content .= "</table>";
	$content .= "</td>";
	$content .= "</tr>";
	$content .= "</table>";
	
	$content .= "</table>";
	$content .= "<br/>";
}

//echo $content;

if ($format == 1)     # Excel
{
	# Get template
	$template_content = get_file_content("$file_path/admin/student_attendance2/report/template.html");
	$output = str_replace("__MAIN_CONTENT__",$content,$template_content);
	$output_filename = "class_attend_".$classes[$z]['ClassName'].".xls";

	output2browser($output,$output_filename);
	flush();

} else {           # Web

$i_title = $i_StudentAttendance_Report_ClassMonth." (".$classes[$z]['ClassName'].")";
include_once("../../../templates/fileheader.php");
echo $content;
?>
<?php
}
intranet_closedb();
?>
