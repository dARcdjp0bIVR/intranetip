<?
// use by kenneth chung
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();


$lclass = new libclass();
if ($ClassID != "")
    $ClassName = $lclass->getClassName($ClassID);

$i_title = $i_StudentAttendance_Report_ClassDaily." (".$ClassName.")";
include_once("../../../templates/fileheader.php");

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

$ts = strtotime($TargetDate);
if ($ts==-1 || $TargetDate =="")
{
    $TargetDate = date('Y-m-d');
    $year = date('Y');
    $month = date('m');
    $day = date('d');
}
else
{
    $year = date('Y',$ts);
    $month = date('m',$ts);
    $day = date('d',$ts);
}

$need_to_take_attendance = $lcard->isRequiredToTakeAttendanceByDate($ClassName,$TargetDate);

if($need_to_take_attendance){
	$result = $lcard->retrieveClassDayData($ClassName, $year, $month, $day);

	$space = $intranet_session_language=="en"?" ":"";

	$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
	//$display .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$i_ClassNameNumber</td>\n";
	$display .= "<tr class='tableTitle'>";
	
	$display .= "<td>$i_UserClassNumber</td>\n";
	switch($lcard->ReportDisplayStudentNameFormat())
	{
		case 1:	
			$display .= "<td>$i_UserChineseName</td>";
			break;
		case 2:	
			$display .= "<td>$i_UserEnglishName</td>";
			break;
		default:
			$display .= "<td>$i_UserChineseName</td>";
			$display .= "<td>$i_UserEnglishName</td>";
			break;
	}
	
	$display .= "<td>$i_StudentAttendance_Field_InSchoolTime</td><td>$i_StudentAttendance_Field_CardStation</td>\n";
	if ($lcard->attendance_mode == 0 || $lcard->attendance_mode == 2 || $lcard->attendance_mode==3)
	{
	    $display .= "<td>$i_StudentAttendance_Slot_AM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
	}
	else
	{
	    $display .= "<td>$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
	}

	if ($lcard->attendance_mode==2)      # With Lunch Out
	{
	    $display .= "<td>$i_StudentAttendance_Field_LunchOutTime</td><td>$i_StudentAttendance_Field_CardStation</td>\n";
	    $display .= "<td>$i_StudentAttendance_Field_LunchBackTime</td><td>$i_StudentAttendance_Field_CardStation</td>\n";
	}
	if ($lcard->attendance_mode == 2 || $lcard->attendance_mode==3)
	{
	    $display .= "<td>$i_StudentAttendance_Slot_PM$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
	}
	$display .= "<td>$i_StudentAttendance_Field_LeaveTime</td><td>$i_StudentAttendance_Field_CardStation</td>\n";
	$display .= "<td>$i_StudentAttendance_Type_LeaveSchool$space$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>\n";
	$display .= "</tr>\n";

	for ($i=0; $i<sizeof($result); $i++)
	{
	     list($studentid, $student_name_ch, $student_name_en, $student_class, $classnum, 
	     		$inTime, $inStation, $am, 
	     		$lunchOutTime, $lunchOutStation, $lunchBackTime, 
	     		$lunchBackStation, $pm, $leaveSchoolTime, 
	     		$leaveSchoolStation, $leave, 
	     		$amLateWaive, $pmLateWaive, 
	     		$amAbsentWaive, $pmAbsentWaive, 
	     		$amEarlyWaive, $pmEarlyWaive) = $result[$i];
	          	          
	     if($student_name_ch == "")
	     	$student_name_ch = $student_name_en;
	     if($student_name_en == "")
	     	$student_name_en = $student_name_ch;
	     if($student_name_ch == "")
	     	$student_name_ch = "&nbsp;";
	     if($student_name_en == "")
	     	$student_name_en = "&nbsp;";
	     if($student_class == "")
	     	$student_class = "&nbsp;";
	     if($classnum == "")
	     	$classnum = "&nbsp;";
	     if($inTime == "")
	     	$inTime = "&nbsp;";
	     if($inStation == "")
	     	$inStation = "&nbsp;";
	     if($am == "")
	     	$am = "&nbsp;";
	     if($lunchOutTime == "")
	     	$lunchOutTime = "&nbsp;";
	     if($lunchOutStation == "")
	     	$lunchOutStation = "&nbsp;";
     	 if($lunchBackTime == "")
     	 	$lunchBackTime = "&nbsp;";
     	 if($lunchBackStation == "")
     	 	$lunchBackStation = "&nbsp;";
     	 if($pm == "")
     	 	$pm = "&nbsp;";
     	 if($leaveSchoolTime == "")
     	 	$leaveSchoolTime = "&nbsp;";
     	 if($leaveSchoolStation == "")
     	 	$leaveSchoolStation = "&nbsp;";
     	 if($leave == "")
     	 	$leave = "&nbsp;";
     	 $amLateWaiveStr = (trim($amLateWaive) != "") ? "" : " (". $i_SmartCard_Frontend_Take_Attendance_Waived .")";
     	 $pmLateWaiveStr = (trim($pmLateWaive) != "") ? "" : " (". $i_SmartCard_Frontend_Take_Attendance_Waived .")";
     	 $amAbsentWaiveStr = ($amAbsentWaive == 1) ? " (". $i_SmartCard_Frontend_Take_Attendance_Waived .")" : "";
     	 $pmAbsentWaiveStr = ($pmAbsentWaive == 1) ? " (". $i_SmartCard_Frontend_Take_Attendance_Waived .")" : "";
     	 $amEarlyWaiveStr = ($amEarlyWaive == 1) ? " (". $i_SmartCard_Frontend_Take_Attendance_Waived .")" : "";
     	 $pmEarlyWaiveStr = ($pmEarlyWaive == 1) ? " (". $i_SmartCard_Frontend_Take_Attendance_Waived .")" : "";

	     switch($am){
		     case "0" : $am = $i_StudentAttendance_Status_OnTime; break;
		     case "1" : $am = $i_StudentAttendance_Status_Absent . $amAbsentWaiveStr; break;
		     case "2" : $am = $i_StudentAttendance_Status_Late . $amLateWaiveStr; break;
		     case "3" : $am = $i_StudentAttendance_Status_Outing; break;
		     default : $am = $i_StudentAttendance_Status_Absent . $amAbsentWaiveStr;
		 }

		 switch($pm){
			 case "0" : $pm = $i_StudentAttendance_Status_OnTime; break;
			 case "1" : $pm = $i_StudentAttendance_Status_Absent . $pmAbsentWaiveStr; break;
			 case "2" : $pm = $i_StudentAttendance_Status_Late . $pmLateWaiveStr; break;
			 case "3" : $pm = $i_StudentAttendance_Status_Outing; break;
			 default : $pm = $i_StudentAttendance_Status_Absent . $pmAbsentWaiveStr;
		 }
		 switch($leave){
			 case "0" : $leave = $i_StudentAttendance_Status_Present; break;
			 case "1": $leave = $i_StudentAttendance_Status_EarlyLeave . $amEarlyWaiveStr; break;
			 case "2": $leave = $i_StudentAttendance_Status_EarlyLeave . $pmEarlyWaiveStr; break;
		 }

	     $css = ($i%2? "2":"");

	     //$display .= "<tr class='tableContent$css'><td NOWRAP>$student_name</td><td NOWRAP>$classnum</td>\n";

	     $display .= "<tr class='tableContent$css'>";
	     $display .= "<td NOWRAP>$classnum</td>\n";
	     switch($lcard->ReportDisplayStudentNameFormat())
		{
			case 1:
				$display .= "<td NOWRAP>$student_name_ch</td>";
				break;
			case 2:
				$display .= "<td NOWRAP>$student_name_en</td>";
				break;
			default:
				$display .= "<td NOWRAP>$student_name_ch</td>";
				$display .= "<td NOWRAP>$student_name_en</td>";
				break;
		}
		
	     $display .= "<td NOWRAP>$inTime</td><td NOWRAP>$inStation</td>\n";
	     if ($lcard->attendance_mode == 0 || $lcard->attendance_mode == 2 || $lcard->attendance_mode==3)
	     {
	         $display .= "<td>$am</td>\n";
	     }
	     else
	     {
	         $display .= "<td>$pm</td>\n";
	     }

	     if ($lcard->attendance_mode==2)      # With Lunch Out
	     {
	         $display .= "<td NOWRAP>$lunchOutTime</td><td NOWRAP>$lunchOutStation</td>\n";
	         $display .= "<td NOWRAP>$lunchBackTime</td><td NOWRAP>$lunchBackStation</td>\n";
	     }
	     if ($lcard->attendance_mode == 2 || $lcard->attendance_mode==3)
	     {
	         $display .= "<td>$pm</td>\n";
	     }
	     $display .= "<td NOWRAP>$leaveSchoolTime</td><td NOWRAP>$leaveSchoolStation</td>\n";
	     $display .= "<td>$leave</td>\n";
	     $display .= "</tr>\n";
	}
	$display .= "</table>\n";
}else{
	$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
	$display.="<tr><td align=center>$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td></tr>";
	$display.="</table>\n";
}

?>
<table width=560 border=0 cellpadding=2 cellspacing=1>
<tr><td colspan='2'><u><?=$i_title?></u></td></tr>
<? if ($ClassName != "") { ?>
<tr><td><?= $i_ClassName ?>:</td><td><?=$ClassName?></td></tr>
<? } ?>
<tr><td><?=$i_StudentAttendance_View_Date?>:</td><td><?php echo $TargetDate ?></td></tr>
</table>

<?=$display?>

<?
intranet_closedb();
?>
