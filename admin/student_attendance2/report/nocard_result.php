<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libwordtemplates.php");

if(!$sys_custom['SmartCardAttendance_Report_NoCard']){
        header("Location: index.php");
}

intranet_opendb();

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();


# process date range
if($startStr=="" || $endStr==""){
   die("Error in parameters");
}else{
        $startDate = $startStr;
        $endDate = $endStr;
}

# get months involved
$start_ts = strtotime($startDate);
$end_ts = strtotime($endDate);
$first_month = date('m',$start_ts);
$first_year = date('Y',$start_ts);
$first_day = date('d',$start_ts);
$end_month = date('m', $end_ts);
$end_year = date('Y',$end_ts);
$end_day = date('d', $end_ts);

if ($lcardattend->attendance_mode==1)      # PM
{
    $in_field = "LunchBackTime";
    $in_status = "PMStatus";
}
else
{
    $in_field = "InSchoolTime";
    $in_status = "AMStatus";
}

$out_field = "LeaveSchoolTime";

# Create temp table
$sql = "
CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_NO_TAB_TIME (
 UserID int(11),
 ClassName varchar(100),
 RecordDate date,
 RecordType char(5)
)";
$lcardattend->db_db_query($sql);

$sql = "DELETE FROM TEMP_NO_TAB_TIME";
$lcardattend->db_db_query($sql);

if ($first_month == $end_month && $first_year == $end_year) # special, only get 1 month table
{
    $tablename = "CARD_STUDENT_DAILY_LOG_".$first_year."_".$first_month;
        if ($_REQUEST['slot_in']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$first_year."-".$first_month."-01',INTERVAL (a.DayNumber-1) DAY),'IN'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE (a.DayNumber >= '$first_day' AND a.DayNumber <= '$end_day') AND
                                         a.$in_field IS NULL AND a.$in_status IN (0,2)";
            $lcardattend->db_db_query($sql);
        }
        if ($_REQUEST['slot_out']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$first_year."-".$first_month."-01',INTERVAL (a.DayNumber-1) DAY),'OUT'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE (a.DayNumber >= '$first_day' AND a.DayNumber <= '$end_day') AND
                                         a.$out_field IS NULL";
            $lcardattend->db_db_query($sql);
        }
}
else
{

    # Grab first month data
    $tablename = "CARD_STUDENT_DAILY_LOG_".$first_year."_".$first_month;
        if ($_REQUEST['slot_in']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$first_year."-".$first_month."-01',INTERVAL (a.DayNumber-1) DAY),'IN'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE (a.DayNumber >= '$first_day') AND
                                         a.$in_field IS NULL AND a.$in_status IN (0,2)";
            $lcardattend->db_db_query($sql);
        }
        if ($_REQUEST['slot_out']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$first_year."-".$first_month."-01',INTERVAL (a.DayNumber-1) DAY),'OUT'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE (a.DayNumber >= '$first_day') AND
                                         a.$out_field IS NULL";
            $lcardattend->db_db_query($sql);
        }



    # Grab all required year / month pair
    if ($first_month != "12")
    {
        $curr_year = $first_year;
        $curr_month = $first_month+1;
    }
    else
    {
        $curr_year = $first_year+1;
        $curr_month = 1;
    }
    while ($curr_year < $end_year || ($curr_year==$end_year && $curr_month < $end_month))
    {
           $curr_month_str = ($curr_month<10?"0".$curr_month:$curr_month);
           # Grab data
           $tablename = "CARD_STUDENT_DAILY_LOG_".$curr_year."_".$curr_month_str;
        if ($_REQUEST['slot_in']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$curr_year."-".$curr_month_str."-01',INTERVAL (a.DayNumber-1) DAY),'IN'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE a.$in_field IS NULL AND a.$in_status IN (0,2)";
            $lcardattend->db_db_query($sql);
        }
        if ($_REQUEST['slot_out']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$curr_year."-".$curr_month_str."-01',INTERVAL (a.DayNumber-1) DAY),'OUT'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE a.$out_field IS NULL";
            $lcardattend->db_db_query($sql);
        }

        if ($curr_month != "12")
        {
            $curr_year = $curr_year;
            $curr_month = $curr_month+1;
        }
        else
        {
            $curr_year = $curr_year+1;
            $curr_month = 1;
        }

    }



    # Grab last month data
    $tablename = "CARD_STUDENT_DAILY_LOG_".$end_year."_".$end_month;
        if ($_REQUEST['slot_in']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$end_year."-".$end_month."-01',INTERVAL (a.DayNumber-1) DAY),'IN'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE (a.DayNumber <= '$end_day') AND
                                         a.$in_field IS NULL AND a.$in_status IN (0,2)";
            $lcardattend->db_db_query($sql);
        }
        if ($_REQUEST['slot_out']=="1")
        {
            $sql = "INSERT IGNORE INTO TEMP_NO_TAB_TIME (UserID, ClassName, RecordDate, RecordType)
                           SELECT a.UserID, b.ClassName, DATE_ADD('".$end_year."-".$end_month."-01',INTERVAL (a.DayNumber-1) DAY),'OUT'
                                  FROM $tablename as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                                  WHERE (a.DayNumber <= '$end_day') AND
                                         a.$out_field IS NULL";
            $lcardattend->db_db_query($sql);
        }




}

$report_title = "$i_StudentAttendance_Report_NoCardTab - ";
if ($_REQUEST['targetType']=="Class")
{
    $report_title .= $i_general_class;
}
else if ($_REQUEST['targetType']=="Student")
{
     $report_title .= $i_identity_student;
}

$display = "$report_title\n<table width=95% border=1 cellpadding=1 cellspacing=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5>";
# Iterate temp table
if ($_REQUEST['targetType']=="Class")
{
     $csv_display = "\"$i_ClassName\",\"$i_general_count\"\r\n";
     $display .= "<tr class=\"tableTitle\"><td>$i_ClassName</td><td>$i_general_count</td>\n";
    if ($_REQUEST['orderby']=="name")
    {
        $order_by_ext = "ClassName ASC";
    }
    else           # Count assumed
    {
        $order_by_ext = "num DESC";
    }
    $sql = "SELECT ClassName, COUNT(*) as num FROM TEMP_NO_TAB_TIME GROUP BY ClassName ORDER BY $order_by_ext";
    $result = $lcardattend->returnArray($sql,2);

    $count_total = 0;
    for ($i=0; $i<sizeof($result); $i++)
    {
         list($t_name, $t_count) = $result[$i];
         $count_total += $t_count;
         $css = ($i%2?"":"2");
         $display .= "<tr class=\"tableContent$css\"><td>$t_name</td><td>$t_count</tr>\n";
         $csv_display .= "\"$t_name\",\"$t_count\"\r\n";
    }
    $display .= "<tr><td colspan=2></td></tr>\n";
    $display .= "<tr><td><b>$list_total</b></td><td><b>$count_total</b></td></tr>\n";
    $display .= "</table>\n";

}
else if ($_REQUEST['targetType']=="Student")
{
     $csv_display = "\"$i_UserStudentName\",\"$i_ClassName\",\"$i_ClassNumber\",\"$i_general_count\"";
     $display .= "<tr class=\"tableTitle\"><td>$i_UserStudentName</td><td>$i_ClassName</td><td>$i_ClassNumber</td><td>$i_general_count</td>";
     $colspan = 4;
     if ($_REQUEST['details']==1)
     {
         if ($_REQUEST['slot_in']==1)
         {
             $colspan++;
             $csv_display .= ",\"$i_StudentAttendance_InSchool\"";
             $display .= "<td>$i_StudentAttendance_InSchool</td>";
         }
         if ($_REQUEST['slot_out']==1)
         {
             $colspan++;
             $csv_display .= ",\"$i_StudentAttendance_LeaveSchool\"";
             $display .= "<td>$i_StudentAttendance_LeaveSchool</td>";
         }

     }

     $csv_display .= "\r\n";
     $display .= "</tr>\n";

    if ($_REQUEST['orderby']=="name")
    {
        $order_by_ext = "b.ClassName, b.ClassNumber, b.EnglishName ASC";
    }
    else           # Count assumed
    {
        $order_by_ext = "num DESC";
    }
    $namefield = getNameFieldByLang("b.");
     $sql = "SELECT a.UserID,$namefield,b.ClassName,b.ClassNumber, COUNT(*) as num FROM TEMP_NO_TAB_TIME as a
                    LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID AND b.RecordType = 2
                    GROUP BY a.UserID ORDER BY $order_by_ext";
     $result = $lcardattend->returnArray($sql,5);

     if ($_REQUEST['details']==1)         # Grab raw date record
     {
         $sql = "SELECT UserID, RecordDate, RecordType FROM TEMP_NO_TAB_TIME";
         $temp = $lcardattend->returnArray($sql,3);
         $records = array();

         # Build assoc array
         for ($i=0; $i<sizeof($temp); $i++)
         {
              list($t_userID, $t_recordDate, $t_recordType) = $temp[$i];
              $records[$t_userID][$t_recordType][] .= $t_recordDate;
         }
     }

     $count_total = 0;
     for ($i=0; $i<sizeof($result); $i++)
     {
          list($t_userID, $t_name,$t_class, $t_classnum, $t_count) = $result[$i];
          $count_total += $t_count;
          $css = ($i%2?"":"2");
          $display .= "<tr class=\"tableContent$css\"><td>$t_name</td><td>$t_class</td><td>$t_classnum</td><td>$t_count</td>";
          $csv_display .= "\"$t_name\",\"$t_class\",\"$t_classnum\",\"$t_count\"";

          if ($_REQUEST['details']==1)
          {
              # Get Date List
              if ($_REQUEST['slot_in']==1)
              {
                  if (is_array($records[$t_userID]["IN"]) && sizeof($records[$t_userID]["IN"])>0)
                  {
                      $t_datesInList = implode(",",$records[$t_userID]["IN"]);
                      #$t_datesInList_csv = "\"".implode("\", \"",$records[$t_userID]["IN"])."\"";
                      $t_datesInList_csv = implode(", ",$records[$t_userID]["IN"]);
                  }
                  $display .= "<td>$t_datesInList&nbsp;</td>";
                  $csv_display .= ",\"".$t_datesInList_csv."\"";
              }
              if ($_REQUEST['slot_out']==1)
              {
                  if (is_array($records[$t_userID]["OUT"]) && sizeof($records[$t_userID]["OUT"])>0)
                  {
                      $t_datesOutList = implode(",",$records[$t_userID]["OUT"]);
                      $t_datesOutList_csv = "\"".implode("\", \"",$records[$t_userID]["OUT"])."\"";
                  }
                  $display .= "<td>$t_datesOutList&nbsp;</td>";
                  $csv_display .= ",".$t_datesOutList;
              }

          }
          $display .= "</tr>\n";
          $csv_display .= "\r\n";

     }
     $display .= "<tr><td colspan='$colspan'>&nbsp;</td></tr>\n";
     $display .= "<tr><td><b>$list_total</b></td><td>&nbsp;</td><td>&nbsp;</td><td><b>$count_total</b></td>";
     if ($_REQUEST['details']==1 && $_REQUEST['slot_in']==1)
     {
         $display .= "<td>&nbsp;</td>";
     }
     if ($_REQUEST['details']==1 && $_REQUEST['slot_out']==1)
     {
         $display .= "<td>&nbsp;</td>";
     }
     $display .= "</tr>\n";
     $display .= "</table>\n";


}
else
{}

$display .= "<br><br><table width='95%' border='0'><tr><td align='center'><a href='javascript:window.print()'><img class=print_hide src='/images/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";

if($format=="print"){ # Web
   $i_title = $report_title;
   include_once("../../../templates/fileheader.php");
        echo $display;
        include_once("../../../templates/filefooter.php");
}
else if($format=="csv"){ # CSV
        $file_name="attendance_report_$startDate_$endDate.csv";
    output2browser($csv_display,$file_name);
}
else
{
    echo "Error in parameters";
}
intranet_closedb();
?>
