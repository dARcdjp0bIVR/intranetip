<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if (!$sys_custom['QualiEd_StudentAttendance'])
{
	die();
}

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

$flag = 1;

$Year1 = ($Year1 == "") ? date('Y') : $Year1;
$Year2 = ($Year2 == "") ? date('Y') : $Year2;
$Month1 = ($Month1 == "") ? date('m') : $Month1;
$Month2 = ($Month2 == "") ? date('m') : $Month2;

if ($Year1 == "" || $Year2 == "")
{
	$flag = 0;
}
if ($Month1 == "" || $Month2 == "")
{
	$flag = 0;
}
if (strlen($Month1)==1)
{
	$Month1 = "0".$Month1;
}
if (strlen($Month2) == 1)
{
	$Month2 = "0".$Month2;
}

/*
if ($flag)
{
	# Get Class List		
	$lclass = new libclass();
	$classes = $lclass->getClassList();		
	
	# Retrieve result
	$result = $lcard->retrieveClassMonthData($ClassName, $Year, $Month);
}
*/

# Get Year List
$years = $lcard->getRecordYear();
$select_year1 = getSelectByValue($years, "name=Year1",$Year1,0,1);
$select_year2 = getSelectByValue($years, "name=Year2",$Year2,0,1);

# Month List
$months = $i_general_MonthShortForm;
$select_month1 = "<SELECT name=Month1>\n";
for($i=0; $i<sizeof($months); $i++)
{
	$month_name = $months[$i];
	$string_selected = ($Month1-1 == $i) ? "SELECTED" : "";
	$select_month1 .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month1 .= "</SELECT>\n";
$select_month2 = "<SELECT name=Month2>\n";
for($i=0; $i<sizeof($months); $i++)
{
	$month_name = $months[$i];
	$string_selected = ($Month2-1 == $i) ? "SELECTED" : "";
	$select_month2 .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month2 .= "</SELECT>\n";
/*
$currMon = date('n')-1;
for ($i=0; $i<sizeof($months); $i++)
{
     $month_name = $months[$i];
     $string_selected = ($currMon==$i? "SELECTED":"");
     $select_month .= "<OPTION value=".($i+1)." $string_selected>".$month_name."</OPTION>\n";
}
$select_month .= "</SELECT>\n";
$select_month1 .= $select_month;
$select_month2 .= $select_month;
*/

?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj) {
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_TwoMonths,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="" method=GET>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr>
	<td align=center><?=$i_CycleNew_Field_Date?></td>
	<td align=left><?=$i_general_Year?>:<?=$select_year1?> <?=$i_general_Month?>:<?=$select_month1?></td>
</tr>
<tr>
	<td align=center><?=$i_CycleNew_Field_Date?></td>
	<td align=left><?=$i_general_Year?>:<?=$select_year2?> <?=$i_general_Month?>:<?=$select_month2?></td>
</tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
	# Display result	
	if ($flag == 1)
	{
		# Header
		echo "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
		echo "<tr>";
			echo "<td class=tableTitle>&nbsp;</td>";
			echo "<td class=tableTitle colspan=3 align=center>$i_general_Year : $Year1 $i_general_Month : $Month1</td>";
			echo "<td class=tableTitle colspan=3 align=center>$i_general_Year : $Year2 $i_general_Month : $Month2</td>";
		echo "<tr>";
			echo "<td class=tableTitle>$i_ClassName</td>";
			echo "<td class=tableTitle>$i_StudentAttendance_Status_Present</td>";
			echo "<td class=tableTitle>$i_StudentAttendance_Status_Absent</td>";
			echo "<td class=tableTitle>$i_StudentAttendance_Status_Late</td>";			
			echo "<td class=tableTitle>$i_StudentAttendance_Status_Present</td>";
			echo "<td class=tableTitle>$i_StudentAttendance_Status_Absent</td>";
			echo "<td class=tableTitle>$i_StudentAttendance_Status_Late</td>";
		echo "</tr>";	
		# End of Table Header		
		
		# Retrieve Class List
		$lclass = new libclass();
		$classes = $lclass->getClassList();				
		
		for ($i=0; $i<sizeof($classes); $i++)
		{
			$ClassName = $classes[$i][1];
			$result = $lcard->retrieveMonthAttendanceByClass($ClassName, $Year1, $Month1);						
						
			echo "<tr>";
				echo "<td>$ClassName</td>";
				
				$total = $result[0] + $result[1] + $result[2];
				$present = ($total == 0) ? 0 : round(($result[0]/$total*100), 2);
				$absent = ($total == 0) ? 0 : round(($result[1]/$total*100), 2);
				$late = ($total == 0) ? 0 : round(($result[2]/$total*100), 2);
				
				echo "<td>".($result[0]+0)." $present%</td>";
				echo "<td>".($result[1]+0)." $absent%</td>";
				echo "<td>".($result[2]+0)." $late%</td>";
				$result = $lcard->retrieveMonthAttendanceByClass($ClassName, $Year2, $Month2);			
				
				$total = $result[0] + $result[1] + $result[2];
				$present = ($total == 0) ? 0 : round(($result[0]/$total*100), 2);
				$absent = ($total == 0) ? 0 : round(($result[1]/$total*100), 2);
				$late = ($total == 0) ? 0 : round(($result[2]/$total*100), 2);

				echo "<td>".($result[0]+0)." $present%</td>";
				echo "<td>".($result[1]+0)." $absent%</td>";
				echo "<td>".($result[2]+0)." $late%</td>";
			echo "</tr>";
		}
		
		echo "</table>";
	}

?>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>