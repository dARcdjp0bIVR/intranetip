<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

switch ($BadType)
{
        case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
        case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
        case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
        case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
        case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
}


$i_title = $i_StudentAttendance_Report_ClassBadRecords." ($nav_title)";
include_once("../../../templates/fileheader.php");

$lcard = new libcardstudentattend2();
$lcard->retrieveSettings();

$ts = strtotime($StartDate);
if ($ts==-1 || $StartDate =="")
{
    $StartDate = date('Y-m-d');
}
$ts = strtotime($EndDate);
if ($ts==-1 || $EndDate =="")
{
    $EndDate = date('Y-m-d');
}


$result = $lcard->retrieveBadRecordsCountByClass($StartDate, $EndDate, $BadType);

$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class='tableTitle'><td>$i_SmartCard_ClassName</td>\n";
$display .= "<td>$i_StudentAttendance_ReportHeader_NumStudents</td>\n";
$display .= "<td>$i_StudentAttendance_ReportHeader_NumRecords</td>\n";
$display .= "</tr>\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list ($class_name, $count_student, $count_record) = $result[$i];
     $css = ($i%2? "2":"");
     $display .= "<tr class='tableContent$css'><td>$class_name</td><td>$count_student</td>\n";
     $display .= "<td>$count_record</td></tr>\n";
}
$display .= "</table>\n";

?>
<table width=560 border=0 cellpadding=2 cellspacing=1>
<tr><td colspan='2'><u><?=$i_title?></u></td></tr>
<tr><td><?= $i_general_startdate ?>:</td><td><?=$StartDate?></td></tr>
<tr><td><?=$i_general_enddate?>:</td><td><?=$EndDate?></td></tr>
</table>

<?=$display?>

<?
intranet_closedb();
?>
