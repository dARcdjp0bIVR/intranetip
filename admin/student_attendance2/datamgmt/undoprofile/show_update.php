<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");

intranet_opendb();

$ts_record = strtotime($TargetDate);
if ($ts_record == -1 || $TargetDate == "")
{
    header("Location: index.php");
    exit();
}


# class used
$li = new libdb();

# Remove Data Confirm Log
$sql = "DELETE FROM CARD_STUDENT_DAILY_DATA_CONFIRM WHERE RecordDate = '$TargetDate'";
$li->db_db_query($sql);

if ($allClass)
{
    # Remove Profile Attendance Records
    $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate = '$TargetDate'";
    $li->db_db_query($sql);
    # Remove Reason Records
    $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate = '$TargetDate'";
    $li->db_db_query($sql);
}
else
{
    # Get User List
    $list = implode(",",$ClassID);
    $sql = "SELECT a.UserID FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassName=b.ClassName
                   WHERE b.ClassID IN ($list)";
    $students = $li->returnVector($sql);
    if (sizeof($students)!=0)
    {
        $student_list = implode(",",$students);

        # Remove Profile Attendance Records
        $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate = '$TargetDate' AND UserID IN ($student_list)";
        $li->db_db_query($sql);
        # Remove Reason Records
        $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordDate = '$TargetDate' AND StudentID IN ($student_list)";
        $li->db_db_query($sql);
    }
}


header( "Location: index.php?msg=2");
intranet_closedb();
?>