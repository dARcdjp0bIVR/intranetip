<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lcardattend = new libcardstudentattend2();
$records = $lcardattend->retrieveYearMonthList();

$width = 400;
$functionbar = "<a href=\"javascript:checkRemove(document.form1,'Record[]','remove_month.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="post" action="remove_month.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DataManagement,'../',$i_StudentAttendance_Menu_DataMgmt_DataClear, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=<?=$width?> border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="<?=$width?>" height="13" border="0"></td></tr>
<tr><td align=right class=admin_bg_menu><?php echo  $functionbar; ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=<?=$width?> height=7 border=0></td></tr>
</table>
<table width=<?=$width?> border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr><td width=<?=$width-50?>><?="$i_general_Year-$i_general_Month"?></td><td width=50><?="$button_remove?"?></td></tr>
<?
for ($i=0; $i<sizeof($records); $i++)
{
     list ($year, $month) = $records[$i];
     if ($month <= 9) $month = "0".$month;
?>
<tr><td><?="$year-$month"?></td><td><input type=checkbox name=Record[] value='<?="$year-$month"?>'></td></tr>
<?
}
?>
</table>
<table width=<?=$width?> border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="<?=$width?>" height="16" border="0"></td></tr>
</table>
<br><br><?=$i_StudentAttendance_DataRemoval_Warning?>
</blockquote>

</form>
<?
include_once("../../../../templates/adminfooter.php");
?>