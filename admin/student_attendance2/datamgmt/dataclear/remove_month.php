<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");

intranet_opendb();

$LICS = new libcardstudentattend2();
for ($i=0; $i<sizeof($Record); $i++)
{
     $array = explode("-", $Record[$i]);
     list($year, $month) = $array;
     $LICS->removeMonthData($year, $month);
}
header( "Location: index.php?msg=2");
?>