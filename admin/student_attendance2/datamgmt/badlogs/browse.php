<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if ($type > 5 || $type < 1) $type = 1;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$user_field = getNameFieldByLang("b.");

if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM)
{
    $sql  = "SELECT
                   a.RecordDate, b.ClassName, b.ClassNumber, $user_field,
                   a.RecordTime,
                   CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
         FROM
             CARD_STUDENT_BAD_ACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2
         WHERE
              a.RecordType = '$type' AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               OR b.ClassName like '$keyword'
               OR b.ClassNumber like '%$keyword%'
               OR a.RecordDate = '$keyword'
               )
               $conds
                ";
}
else if ($type==CARD_BADACTION_NO_CARD_ENTRANCE)
{
    $sql  = "SELECT
                   a.RecordDate, b.ClassName, b.ClassNumber, $user_field,
                   CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
         FROM
             CARD_STUDENT_BAD_ACTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2
         WHERE
              a.RecordType = '$type' AND
              (b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               OR b.ClassName like '$keyword'
               OR b.ClassNumber like '%$keyword%'
               OR a.RecordDate = '$keyword'
               )
               $conds
                ";
}

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.RecordDate","b.ClassName","b.ClassNumber","b.EnglishName");
$name_width = 40;
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM)
{
    $li->field_array[] = "a.RecordTime";
    $name_width -= 20;
}
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_general_record_date)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=$name_width% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
if ($type==CARD_BADACTION_LUNCH_NOTINLIST || $type==CARD_BADACTION_LUNCH_BACKALREADY || $type==CARD_BADACTION_FAKED_CARD_AM || $type==CARD_BADACTION_FAKED_CARD_PM)
{
    $li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_general_record_time)."</td>\n";
}

$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

#$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
#$functionbar .= $name_select;
#$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'OutingID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
#$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'OutingID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1, 'export.php')\">".exportIcon()."$button_export</a>";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

switch ($type)
{
        case CARD_BADACTION_LUNCH_NOTINLIST: $nav_title = $i_StudentAttendance_BadLogs_Type_NotInLunchList; break;
        case CARD_BADACTION_LUNCH_BACKALREADY: $nav_title = $i_StudentAttendance_BadLogs_Type_GoLunchAgain; break;
        case CARD_BADACTION_FAKED_CARD_AM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardAM; break;
        case CARD_BADACTION_FAKED_CARD_PM: $nav_title = $i_StudentAttendance_BadLogs_Type_FakedCardPM; break;
        case CARD_BADACTION_NO_CARD_ENTRANCE: $nav_title = $i_StudentAttendance_BadLogs_Type_NoCardRecord; break;
}

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DataManagement,'../',$i_StudentAttendance_Menu_DataMgmt_BadLogs,'index.php',$nav_title,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=type value="<?=$type?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
