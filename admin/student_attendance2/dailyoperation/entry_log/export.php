<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();
$lword = new libwordtemplates();
$lclass = new libclass();

$lexport = new libexporttext();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

$card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$txt_year."_".$txt_month;
$namefield = getNameFieldWithClassNumberByLang("a.");
$cond = ($TargetClass == "") ? "" : " AND a.ClassName = '$TargetClass'";
/*
$sql = "SELECT
					a.UserID,
					$namefield,
					TIME_FORMAT(b.RecordTime, '%H:%i')
				FROM
					INTRANET_USER AS a
					LEFT OUTER JOIN $card_log_table_name AS b ON a.UserID = b.UserID AND b.DayNumber = '$txt_day'
				WHERE					
					a.RecordType = 2
					AND a.RecordStatus IN (0, 1)
					$cond
				ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";*/
/*
$sql = "SELECT
                                        a.UserID,
                                        $namefield,
                                        TIME_FORMAT(b.RecordTime, '%H:%i')
                                FROM
                                        INTRANET_USER AS a,
                                        $card_log_table_name AS b 
                                WHERE
                                        a.UserID=b.UserID AND a.RecordType = 2 AND b.DayNumber = '$txt_day'
                                        AND a.RecordStatus IN (0, 1,2)
                                        $cond
                                ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
*/
if ($TargetClass != "")
{
	$student_list = $lclass->returnStudentListByClass($TargetClass);
	$arr_student = array();
	for($i=0; $i<sizeof($student_list) ;$i++)
	{
		list($user_id, $student_name) = $student_list[$i];
		array_push($arr_student, $user_id);
	}
	$student_list = implode(",", $arr_student);
}


//$namefield = getNameFieldWithClassNumberByLang("a.");
$namefield = getNameFieldForRecord2("a.");
if($TargetClass == "")
{
	$sql = "SELECT 
					a.UserID,
          $namefield,
          a.ClassName, 
          a.ClassNumber,
          TIME_FORMAT(b.RecordTime, '%H:%i'),
          c.Reason
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN 
					CARD_STUDENT_ENTRY_LOG_REASON AS c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}
else
{
	//$TargetStudent = ($TargetClass == "") ? "" : " AND a.UserID IN ($student_list)";
	//$cond = ($TargetClass == "") ? "" : " AND a.ClassName = '$TargetClass'";
	
	$sql = "SELECT 
					a.UserID, $namefield, 
					a.ClassName, 
          a.ClassNumber,TIME_FORMAT(b.RecordTime, '%H:%i'), c.Reason
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN
					CARD_STUDENT_ENTRY_LOG_REASON as c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE 
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
					AND a.UserID IN ($student_list)
			ORDER BY 
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}

$data = $lcardattend->returnArray($sql,6);

// Format result
for($i=0; $i<sizeof($data); $i++)
{
	list($sid, $sname, $ClassName, $ClassNumber, $record_time, $reason) = $data[$i];
  $StudentName[$sid] = $sname;
  $StudentClassName[$sid] = $ClassName;
  $StudentClassNumber[$sid] = $ClassNumber;
  $time_array[$sid][] = $record_time;
  $EntryLogReason[$sid] = $reason;
}
/*
// Display name list for specific class
if ($TargetClass != "" && sizeof($data) == 0)
{
	$sql = "SELECT a.UserID, $namefield FROM INTRANET_USER AS a WHERE a.RecordType = 2 AND a.RecordStatus = 1 AND a.ClassName = '$TargetClass' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$student_list = $lcardattend->returnArray($sql, 2);
	
	for ($i=0; $i<sizeof($student_list); $i++)
	{
		list ($sid, $sname) = $student_list[$i];
		$StudentName[$sid] = $sname;
		$time_array[$sid][] = '';
	}
}
*/
$export_content = "";

// Table Header
$export_content .= "\"".$i_ClassName."\",\"".(($TargetClass == "") ? $i_StudentAttendance_AllStudentsWithRecords : $TargetClass)."\"\n";
$export_content .= "\"".$i_StudentAttendance_Field_Date."\",\"".$TargetDate."\"\n";
$export_content .= "\"".$i_UserStudentName."\",\"".$i_ClassName."\",\"".$i_ClassNumber."\",\"".$i_StaffAttendnace_Leave_TimeSlot."\",\"".$i_Attendance_Reason."\"\n";

$utf_export_content .= $i_ClassName."\t".(($TargetClass == "") ? $i_StudentAttendance_AllStudentsWithRecords : $TargetClass)."\r\n";
$utf_export_content .= $i_StudentAttendance_Field_Date."\t".$TargetDate."\r\n";
$utf_export_content .= $i_UserStudentName."\t".$i_ClassName."\t".$i_ClassNumber."\t".$i_StaffAttendnace_Leave_TimeSlot."\t".$i_Attendance_Reason."\r\n";


if (sizeof($StudentName) > 0)
{
	foreach ($StudentName AS $student_id => $student_name)
	{	
		$export_content .= "\"".$student_name."\",\"";
		$utf_export_content .= $student_name."\t";
		
		$export_content .= $StudentClassName[$student_id]."\",\"";
    $export_content .= $StudentClassNumber[$student_id]."\",\"";
    
    $utf_export_content .= $StudentClassName[$student_id]."\t";
    $utf_export_content .= $StudentClassNumber[$student_id]."\t";
		for($j=0; $j<sizeof($time_array[$student_id]); $j++)		
		{
			$export_content .= ($time_array[$student_id][$j]) ? "".$time_array[$student_id][$j]."  " : "" ." -  ". "";
			$utf_export_content .= ($time_array[$student_id][$j]) ? "".$time_array[$student_id][$j]."  " : "" ." -  ". "";
			
			if($time_array[$student_id][$j] != "")
				$cnt = 1;
			else
				$cnt = 0;
		}
		$export_content .= "\",\"";
		$utf_export_content .= "\t";
		if($cnt == 1) 
		{
			$export_content .= "$EntryLogReason[$student_id]";
			$utf_export_content .= "$EntryLogReason[$student_id]";
		}
		else
		{
			$export_content .= "" ." -  ". "";
			$utf_export_content .= "" ." -  ". "";
		}
		
		$export_content .= "\"\n";
		$utf_export_content .= "\r\n";
	}
}

$filename = "entry_log_".$txt_year."_".$txt_month.".csv";

if (!$g_encoding_unicode) {
	// Output the file to user browser
	output2browser($export_content, $filename);
} else {
	$lexport->EXPORT_FILE($filename, $utf_export_content);
}



?>