<?php
// using kenneth 
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libwordtemplates.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();
$lword = new libwordtemplates();
$lidb = new libdb();
$lclass = new libclass();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);


$lcardattend->createTable_Card_Student_Entry_Log($txt_year,$txt_month);

// Retrieve data
$card_log_table_name = "CARD_STUDENT_ENTRY_LOG_".$txt_year."_".$txt_month;

/*
if ($TargetClass!="")
{
	$student_list = $lclass->returnStudentListByClass($TargetClass);
	$arr_student = array();
	for($i=0; $i<sizeof($student_list) ;$i++)
	{
		list($user_id, $student_name) = $student_list[$i];
		//echo $student_name."<BR>";
		array_push($arr_student, $user_id);
	}
	$student_list = implode(",", $arr_student);
	//echo $student_list;
	
	$cond = " AND a.ClassName = '$TargetClass'";
	$namefield = getNameFieldWithClassNumberByLang("a.");
	
			
	$sql = "select a.UserID, $namefield, TIME_FORMAT(b.RecordTime, '%H:%i') from INTRANET_USER AS a LEFT OUTER JOIN $card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') WHERE a.UserID IN ($student_list) AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";
            
            
	echo $sql."<BR><BR>";
                    
	$data = $lcardattend->returnArray($sql,3);
	print_r($data);
}
*/
/*
$sql = "SELECT
                                        a.UserID,
                                        $namefield,
                                        TIME_FORMAT(b.RecordTime, '%H:%i')
                                FROM
                                        INTRANET_USER AS a
                                        LEFT OUTER JOIN $card_log_table_name AS b ON a.UserID = b.UserID AND b.DayNumber = '$txt_day'
                                WHERE
                                        a.RecordType = 2
                                        AND a.RecordStatus IN (0, 1)
                                        $cond
                                ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
*/
/*
$sql = "SELECT
                                        a.UserID,
                                        $namefield,
                                        TIME_FORMAT(b.RecordTime, '%H:%i')
                                FROM
                                        INTRANET_USER AS a,
                                        $card_log_table_name AS b 
                                WHERE
                                        a.UserID=b.UserID AND a.RecordType = 2 AND b.DayNumber = '$txt_day'
                                        AND a.RecordStatus IN (0, 1,2)
                                        $cond
                                ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
*/

if ($TargetClass != "")
{
	$student_list = $lclass->returnStudentListByClass($TargetClass);
	$arr_student = array();
	for($i=0; $i<sizeof($student_list) ;$i++)
	{
		list($user_id, $student_name) = $student_list[$i];
		array_push($arr_student, $user_id);
	}
	$student_list = implode(",", $arr_student);
}


//$namefield = getNameFieldWithClassNumberByLang("a.");
$namefield = getNameFieldForRecord2("a.");
if($TargetClass == "")
{
	$sql = "SELECT 
					a.UserID,
          $namefield,
          a.ClassName, 
          a.ClassNumber,
          TIME_FORMAT(b.RecordTime, '%H:%i'),
          c.Reason
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN 
					CARD_STUDENT_ENTRY_LOG_REASON AS c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}
else
{
	//$TargetStudent = ($TargetClass == "") ? "" : " AND a.UserID IN ($student_list)";
	//$cond = ($TargetClass == "") ? "" : " AND a.ClassName = '$TargetClass'";
	
	$sql = "SELECT 
					a.UserID, $namefield, 
					a.ClassName, 
          a.ClassNumber,TIME_FORMAT(b.RecordTime, '%H:%i'), c.Reason
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					$card_log_table_name AS b ON (a.UserID = b.UserID AND b.DayNumber = '$txt_day') LEFT OUTER JOIN
					CARD_STUDENT_ENTRY_LOG_REASON as c ON (a.UserID = c.StudentID AND c.RecordDate = '$TargetDate')
			WHERE 
					a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
					AND a.UserID IN ($student_list)
			ORDER BY 
					a.ClassName, a.ClassNumber, a.EnglishName
			";
}

//echo $sql;
$data = $lcardattend->returnArray($sql,6);


// Format result array
for($i=0; $i<sizeof($data); $i++)
{
        list($sid, $sname, $ClassName, $ClassNumber, $record_time, $reason) = $data[$i];
        $StudentName[$sid] = $sname;
        $StudentClassName[$sid] = $ClassName;
        $StudentClassNumber[$sid] = $ClassNumber;
        $time_array[$sid][] = $record_time;
        $EntryLogReason[$sid] = $reason;
}
/*
// Display name list for specific class
if ($TargetClass != "" && sizeof($data) == 0)
{
	$sql = "SELECT a.UserID, $namefield FROM INTRANET_USER AS a WHERE a.RecordType = 2 AND a.RecordStatus = 1 AND a.ClassName = '$TargetClass' ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$student_list = $lcardattend->returnArray($sql, 2);
	
	for ($i=0; $i<sizeof($student_list); $i++)
	{
		list ($sid, $sname) = $student_list[$i];
		$StudentName[$sid] = $sname;
		$time_array[$sid][] = '';
	}
}
*/

$confirm_button = "<a href=\"javascript:AlertPost(document.form1,'show_log_update.php','$i_SmartCard_Confirm_Update_Attend?')\"><img src='$image_path/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$back_button = "<a href=\"index.php\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$button_reset = "<a href='javascript:resetForm(document.form1)'><img src='$image_path/admin/button/s_btn_reset_$intranet_session_language.gif' border='0'></a>";

$table_attend = "";

// Table Header
$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>$i_UserStudentName</td>
<td>$i_ClassName</td>
<td>$i_ClassNumber</td>
<td>$i_StaffAttendnace_Leave_TimeSlot</td>
<td width=130>$i_Attendance_Reason</td>
</tr>\n";

$words = $lword->getWordListAttendance();
$hasWord = sizeof($words)!=0;
$reason_js_array = "";

$disable_color="#DFDFDF";  # background color for disabled text field
$enable_color="#FFFFFF";  # background color for enabled text field

$select_word = "";

// Display Result
if (sizeof($StudentName) > 0)
{
        foreach ($StudentName AS $student_id => $student_name)
        {
                $x .= "<tr class=tableContent>\n";
                        $x .= "<td>$student_name</td>\n";
                        $x .= "<td>$StudentClassName[$student_id]</td>\n";
                        $x .= "<td>$StudentClassNumber[$student_id]</td>\n";
                        for($j=0; $j<sizeof($time_array[$student_id]); $j++)
                        {
                                $font_color = ($j%2 == 0) ? "red" : "green";
                                $x .= ($j==0) ? "<td>" : "";
                                $x .= ($time_array[$student_id][$j]) ? "<font color=$font_color>".$time_array[$student_id][$j]."</font>" : "-";
                                $x .= (sizeof($time_array[$student_id]) > ($j+1)) ? " | " : "</td>\n";
                                if($time_array[$student_id][$j] != "")
                                		$cnt = 1;
                                else
                                		$cnt = 0;
                        }
                        if ($cnt == 1)
                        {
	                        $x .= "<input type=hidden name=record_time$student_id value=\"$cnt\">";
                        }
                        else
                        {
	                        $x .= "<input type=hidden name=record_time$student_id value=\"$cnt\">";
                        }
                       
                $x .= "<input type=hidden name=user_id[$student_id] value=\"$student_id\">\n";
                
				if($cnt == 1)
				{
                	$reason_comp = "<input type=text name=reason$student_id size=12 maxlength=255 value='$EntryLogReason[$student_id]' style='background:$enable_color'>";
            	}
                else
                {
                	$reason_comp = "-";
            	}
                	
		        if ($hasWord)
		        {
			        if($cnt == 1)
			        {
		            	$select_word = "<a onMouseMove='overhere()' href=javascript:showSelection($student_id,$cnt)><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a>";
	            	}
		            else
		            {
		            	$select_word = "";
	            	}
		            
		            $txtContent = "<table border=0 cellpadding=1 cellspacing=1>";
		            if($remark!=""){
			            $txtContent.="<tr><td><font color=red>[$i_Attendance_Others]</font></td></tr>";
		            	$txtContent.="<tr><td><table border=0 cellspacing=0 cellpadding=0 width=100%><tr><td valign=top width=8><font color=red> - </font></td><td align=left><a href=\\\"javascript:putBack(document.form1.reason$student_id,'$remark');\\\"><font color=red>$remark</font></a></td></tr></table><br></td></tr>";
		
		           	}
		           	$txtContent.="<tr><td><b>[$i_Attendance_Standard]</b></td></tr>";
		           	//$txtcontent.="<tr><td>--------------------</td></tr>";
		            for ($j=0; $j<sizeof($words); $j++)
		            {
		                 $temp = addslashes($words[$j]);
		                 $temp2 = addslashes($temp);
		                 $txtContent .= "<tr><td><table border=0 cellpaddin=0 cellspacing=0 width=100%><tr><td valign=top width=8> - </td><td align=left><a href=\\\"javascript:putBack(document.form1.reason$student_id,'$temp2');\\\">$temp</a></td></tr></table></td></tr>";
		            }
		            $txtContent .= "</table>";
		            //$reason_js_array .= "reasonSelection[$i] = \"<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table width=560 border=0 cellspacing=0 cellpadding=3><tr><td class=tipbg><input type=button value='+'></td></tr><tr><td class=tipbg valign=top><font size=-2>$txtContent</font></td></tr></table></td></tr></table>\";\n";
		            $reason_js_array .= "reasonSelection[$student_id] = \"<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table border=0 cellspacing=0 cellpadding=3><tr><Td class=tipbg><table border=0 cellspacing=0 cellpadding=0><tr><td><input type=button value=' X ' onClick=hideMenu('ToolMenu')></td></tr></table></td></tr><tr><td class=tipbg valign=top><font size=-2>$txtContent</font></td></tr></table></td></tr></table>\";\n";
		
		        }
		        $x .= "<td>".$reason_comp.$select_word."</td>";
                $x .= "</tr>\n";
        }
}
else
{
        $x .= "<tr class=tableContent><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>\n";
}
$x .= "</table>\n";
$table_attend .= $x;

?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">

     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     isMenu = true;
     </script>
     <div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>

<script language="JavaScript" type="text/javascript">
var reasonSelection = Array();
<?=$reason_js_array?>
<!--
function openPrintPage()
{
        newWindow("show_print.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>",4);
}
function showSelection(i, allowed)
{
	if (allowed == 1)
	{
		writeToLayer('ToolMenu',reasonSelection[i]);
				
		halfLayerHeight = parseInt(eval(doc + "ToolMenu" +".offsetHeight"))/2;

		if(mouse.y-halfLayerHeight >0)
			t = mouse.y-halfLayerHeight;
		else t = mouse.y-halfLayerHeight;
		
		l = mouse.x;
		moveToolTip2('ToolMenu', t, l);
		showLayer('ToolMenu');
	}
}
function moveToolTip2(lay, FromTop, FromLeft){

     if (tooltip_ns6) {

         var myElement = document.getElementById(lay);

          myElement.style.left = (FromLeft + 10) + "px";

          myElement.style.top = (FromTop + document.body.scrollTop) + "px";

     }

     else if(tooltip_ie4) {
          eval(doc + lay + sty + ".top = "  + (FromTop + document.body.scrollTop))
     }
 	 else if(tooltip_ns4){eval(doc + lay + sty + ".top = "  +  FromTop)}

     if (!tooltip_ns6) eval(doc + lay + sty + ".left = " + (FromLeft + 10));
   
}
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function setReasonComp(value, txtComp, hiddenFlag)
{
         if (value==1)
         {
             txtComp.disabled = false;
             hiddenFlag.value = 1;
         }
         else
         {
             txtComp.disabled = true;
             hiddenFlag.value = 0;
         }
}
function resetForm(formObj){
	formObj.reset();
  	hideMenu('ToolMenu');
	resetFields(formObj);	
}
function resetFields(formObj){
	status_list = document.getElementsByTagName('SELECT');
	if(status_list==null) return;
	
	for(i=0;i<status_list.length;i++){
		s = status_list[i];
		if(s==null) continue;
		reasonObj = eval('formObj.reason'+i);
		if(s.selectedIndex==1){
			if(reasonObj!=null){
				reasonObj.disabled=false;
				reasonObj.style.background='<?=$enable_color?>';
			}
		}else{
			if(reasonObj!=null){
				reasonObj.value="";
				reasonObj.disabled=true;
				reasonObj.style.background='<?=$disable_color?>';
			}
		}
	}
}

-->
</script>

<form name="form1" method="post" action="show_log_update.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewEntryLog, 'index.php',"$TargetDate (".(($TargetClass == "") ? $i_StudentAttendance_AllStudentsWithRecords : $TargetClass).")",'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<!--
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?=$table_confirm?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
-->

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
	<td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td>
</tr>
<tr>
<?$update_bar = "<a href='javascript:submitForm()'><img src='/images/admin/button/t_btn_update_$intranet_session_language.gif' border='0' align='absmiddle'></a>";?>
    <td class=admin_bg_menu align=left><a class=iconLink href=javascript:checkGet(document.form1,'export.php')><img src='/images/admin/icon_export.gif' border='0' align='absmiddle'><?php echo $button_export; ?></a></td>
</tr>
<tr><td class=tableContent align=center><?=$table_attend?></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td>&nbsp;</td></tr>
<?php  $button_reset = "<a href='javascript:resetForm(document.form1)'><img src='$image_path/admin/button/s_btn_reset_$intranet_session_language.gif' border='0'></a>"; ?>

<tr><td align=right><?=$confirm_button.toolBarSpacer().$button_reset.toolBarSpacer().$back_button?></td></tr>
</table>

<input type=hidden name=TargetDate value=<?php echo $TargetDate; ?>>
<input type=hidden name=TargetClass value=<?php echo $TargetClass; ?>>

</form>

<br><br>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
