<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../includes/libwordtemplates.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();

$lword = new libwordtemplates();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

# Get Confirmation status
$sql = "SELECT RecordID, LateConfirmed, LateConfirmTime FROM CARD_STUDENT_DAILY_DATA_CONFIRM
               WHERE RecordDate = '$TargetDate' AND RecordType = $DayType";
$temp = $lcardattend->returnArray($sql,3);
list ($recordID, $confirmed , $confirmTime) = $temp[0];


$col_width = 150;
$table_confirm = "";
$x = "<table width=100% border=0 cellpadding=2 cellspacing=0>\n";
$x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Field_Date:</td><td align=left>$TargetDate</td></tr>\n";
$x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Slot:</td><td align=left>$display_period</td></tr>\n";

if($confirmed==1)
{
        $x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Field_LastConfirmedTime:</td><td align=left>$confirmTime</td></tr>";
}
else
{
        $x .= "<tr><td width=$col_width></td><td align=left>$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
$x .= "</table>\n";
$table_confirm = $x;

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")." as name,
                a.ClassName,
                a.ClassNumber,
                b.InSchoolTime,
                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                c.Reason,
                c.LateType, c.DemeritWaived, c.DetentionWaived, c.DetentionRecordID
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_LATE."'
                WHERE b.DayNumber = '$txt_day' AND b.AMStatus = '".CARD_STATUS_LATE."'
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
}
/*
else if ($lcardattend->attendance_mode==1)    # Special for PM only
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")." as name,
                a.ClassName,
                a.ClassNumber,
                b.InSchoolTime,
                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                c.Reason
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_LATE."'
                WHERE b.DayNumber = '$txt_day' AND b.PMStatus = '".CARD_STATUS_LATE."'
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
}*/
else     # Check PM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")." as name,
                a.ClassName,
                a.ClassNumber,
                b.LunchBackTime,
                IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
                c.Reason,
                c.LateType, c.DemeritWaived, c.DetentionWaived, c.DetentionRecordID
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_LATE."'
                WHERE b.DayNumber = '$txt_day' AND b.PMStatus = '".CARD_STATUS_LATE."'
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1)
         ORDER BY c.LateType, a.ClassName, a.ClassNumber, a.EnglishName
";

}

$result = $lcardattend->returnArray($sql,12);

$table_attend = "";
# <td class=tableTitle>#</td>
$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>#</td><td>$i_UserName</td>
<td>$i_ClassName</td>
<td>$i_UserClassNumber</td>
<td>$i_StudentAttendance_Status</td>
<td>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td>
<td>$i_QualiEd_Waive_Demerit<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'WaiveDemerit[]'):setChecked(0,this.form,'WaiveDemerit[]')></td>
<td>$i_QualiEd_Waive_Detention<input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'WaiveDetention[]'):setChecked(0,this.form,'WaiveDetention[]')></td>
</tr>\n";
/*
<td width=130>$i_Attendance_Reason</td>
<td>$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td>
*/
$words = $lword->getWordListAttendance();
$hasWord = sizeof($words)!=0;
$reason_js_array = "";

$select_word = "";
for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name,$class_name,$class_number, $in_school_time,
             $in_school_station, $reason, $t_lateType, $t_demerit_waived,
             $t_detention_waived, $t_detention_id) = $result[$i];
        switch ($t_lateType)
        {
                case 1: $string_status = $i_QualiEd_StudentAttendance_LateType1; break;
                case 2: $string_status = $i_QualiEd_StudentAttendance_LateType2; break;
        }


        /*
        $select_status = "<SELECT name=drop_down_status[$i] onChange=\"setReasonComp(this.value, this.form.reason$i, this.form.editAllowed$i)\">\n";
        $select_status .= "<OPTION value=0>$i_StudentAttendance_Status_OnTime</OPTION>\n";
        $select_status .= "<OPTION value=2 SELECTED>$i_StudentAttendance_Status_Late</OPTION>\n";
        $select_status .= "</SELECT>\n";
        $select_status .= "<input name=record_id[$i] type=hidden value=\"$record_id\">\n";
        $select_status .= "<input name=user_id[$i] type=hidden value=\"$user_id\">\n";

        $reason_comp = "<input type=text name=reason$i size=17 maxlength=255 value='$reason'>";
        if ($hasWord)
        {
            $select_word = "<a onMouseMove='overhere()' href=javascript:showSelection($i,document.form1.editAllowed$i.value)><img src=\"$image_path/icon_alt.gif\" border=0 alt='$i_Profile_SelectReason'></a>";
            $txtContent = "<table width=100% border=0 cellpadding=1 cellspacing=1>";
            for ($j=0; $j<sizeof($words); $j++)
            {
                 $temp = addslashes($words[$j]);
                 $temp2 = addslashes($temp);
                 $txtContent .= "<tr><td> - <a href=\\\"javascript:putBack(document.form1.reason$i,'$temp2');\\\">$temp</a></td></tr>";
            }
            $txtContent .= "</table>";
            $reason_js_array .= "reasonSelection[$i] = \"<table border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table width=100% border=0 cellspacing=0 cellpadding=3><tr><td class=tipbg valign=top><font size=-2>$txtContent</font></td></tr></table></td></tr></table>\";\n";
        }
*/

#        $x .= "<tr class=$css><td >".($i+1)."</td><td >$name</td><td>$class_name</td><td >$class_number</td><td>$in_school_time</td><td >$in_school_station</td><td >$select_status</td><td>$reason_comp$select_word</td></tr>\n";
        $x .= "<tr class=$css><td >".($i+1)."</td><td >$name</td><td>$class_name</td>
                   <td >$class_number</td><td>$string_status<input type=hidden name=LateType_".$user_id." value=\"$t_lateType\"></td><td>$in_school_time</td>
                   <td><input type=checkbox name=WaiveDemerit[] value=\"$user_id\"".(($t_demerit_waived == 1) ? "CHECKED" : "")."></td>
                   <td><input type=checkbox name=WaiveDetention[] value=\"$user_id\"".(($t_detention_waived == 1) ? "CHECKED" : "")."><input type=hidden name=user_ids[] value=\"$user_id\"></td>                   
                   </tr>\n";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=tableContent><td colspan=8 align=center>$i_StudentAttendance_NoLateStudents</td></tr>\n";
}
$x .= "</table>\n";
$table_attend = $x;

$confirm_button = "<a href=\"javascript:AlertPost(document.form1,'showlate_update.php','$i_SmartCard_Confirm_Update_Attend?')\"><img src='$image_path/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$back_button = "<a href=\"index.php?period=$period\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     isMenu = true;
     </script>
     <div id="ToolMenu" style="position:absolute; width=200; height=100; visibility:hidden"></div>

<script language="JavaScript" type="text/javascript">
var reasonSelection = Array();
<?=$reason_js_array?>
<!--
function openPrintPage()
{
        newWindow("showlate_print.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>",4);
}
function showSelection(i, allowed)
{
         if (allowed == 1)
         {
             showMenu('ToolMenu',reasonSelection[i]);
         }
}
function putBack(obj, value)
{
         obj.value = value;
         hideMenu('ToolMenu');
}
function setReasonComp(value, txtComp, hiddenFlag)
{
         if (value==2)
         {
             txtComp.disabled = false;
             hiddenFlag.value = 1;
         }
         else
         {
             txtComp.disabled = true;
             hiddenFlag.value = 0;
         }
}
-->
</script>

<form name="form1" method="post" action="showlate_update.php" >
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewLateStatus, 'index.php',"$TargetDate ($display_period)",'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?=$table_confirm?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$toolbar?></td></tr>
<tr><td class=tableContent align=center><?=$table_attend?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align=right><?=$confirm_button.toolBarSpacer().btnReset().toolBarSpacer().$back_button?></td></tr>
</table>

<input name=period type=hidden value="<?=$period?>">
<input name=TargetDate type=hidden value="<?=$TargetDate?>">
<? for ($i=0; $i<sizeof($result); $i++) { ?>
<input type=hidden name=editAllowed<?=$i?> value=1>
<? } ?>
</form>
<br><br>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>