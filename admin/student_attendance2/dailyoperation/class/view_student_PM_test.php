<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

### class used
$LIDB = new libdb();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
if(!$lcardattend->isRequiredToTakeAttendanceByDate($class_name,$TargetDate)){
        header("Location: ../");
        exit();
}
include_once("../../../../templates/adminheader_setting.php");

$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = 3;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                break;
}

### build confirm table exists
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$sql = "SELECT
                                a.RecordID,
                                IF(a.ConfirmedUserID <> -1, ".getNameFieldWithClassNumberByLang("c.").", CONCAT('$i_general_sysadmin')),
                                a.DateModified
                                        FROM
                                                $card_student_daily_class_confirm as a
                                                LEFT OUTER JOIN INTRANET_CLASS as b ON (a.ClassID=b.ClassID)
                                                LEFT OUTER JOIN INTRANET_USER as c ON (a.ConfirmedUserID=c.UserID || a.ConfirmedUserID=-1)

                                                        WHERE b.ClassName = \"$class_name\" AND a.DayNumber = ".$txt_day." AND a.DayType = $DayType";
$result = $LIDB->returnArray($sql,3);
$col_width = 150;
$table_confirm = "";
$x = "<table width=100% border=0 cellpadding=2 cellspacing=0>\n";
$x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Field_Date:</td><td align=left>$TargetDate</td></tr>\n";
$x .= "<tr><td  align=right>$i_ClassName:</td><td align=left>$class_name</td></tr>\n";
$x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Slot:</td><td align=left>$display_period</td></tr>\n";

list($confirmed_id, $confirmed_user_name, $last_confirmed_date) = $result[0];
if($confirmed_id=="")
{
        $x .= "<tr><td width=$col_width></td><td align=left>$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record</td></tr>";
}
else
{
        $x .= "<tr><td width=$col_width align=right>$i_StudentAttendance_Field_LastConfirmedTime:</td><td align=left>$last_confirmed_date ($confirmed_user_name)</td></tr>";
}
$x .= "</table>\n";
$table_confirm = $x;


### check if Preset Absence exists
$sql="SELECT COUNT(*) FROM INTRANET_USER AS A INNER JOIN CARD_STUDENT_PRESET_LEAVE AS B ON(A.USERID = B.STUDENTID) WHERE A.RECORDTYPE=2 AND A.RECORDSTATUS IN(0,1,2) AND A.CLASSNAME='$class_name' AND B.RECORDDATE='$txt_year-$txt_month-$txt_day' AND B.DayPeriod=3";
$temp = $LIDB->returnVector($sql);
$preset_count = $temp[0];


### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($lcardattend->attendance_mode==1)
{
    $time_field = "InSchoolTime";
    $station_field = "InSchoolStation";
    $pm_expected_field = "CASE
                              WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LunchOutTime IS NULL) OR (b.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN b.AMStatus IS NULL AND c.OutingID IS NULL
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN b.AMStatus IS NULL AND c.OutingID IS NOT NULL
                                   THEN '".CARD_STATUS_OUTING."'
                              ELSE '".CARD_STATUS_ABSENT."'
                          END";
}
else
{
#                              WHEN (b.AMStatus = '".CARD_STATUS_PRESENT. "' OR b.AMStatus = '".CARD_STATUS_LATE."')

    $time_field = "LunchBackTime";
    $station_field = "LunchBackStation";
    $pm_expected_field = "CASE
                              WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LunchOutTime IS NULL) OR (b.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN (b.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND b.LunchOutTime IS NOT NULL) AND (b.LunchBackTime IS NULL)
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN (b.AMStatus IS NULL OR b.AMStatus = '".CARD_STATUS_ABSENT."') AND c.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
                              WHEN (b.AMStatus IS NULL OR b.AMStatus = '".CARD_STATUS_ABSENT."') AND c.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
                         END";
}

$sql  = "SELECT        b.RecordID,
                                                        a.UserID,
                                                        ".getNameFieldByLang("a.")."as name,
                                                        a.ClassNumber,
                                                        IF(b.$time_field IS NULL,
                                                                '-',
                                                                b.$time_field),
                                                        IF(b.$station_field IS NULL,
                                                                '-',
                                                                b.$station_field),
                                                        IF(b.PMStatus IS NOT NULL,
                                                                        b.PMStatus,
                                                                        $pm_expected_field
                                                                ),
                                                       IF(d.Remark IS NULL,'-',d.Remark),
                                                        IF(f.RecordID IS NOT NULL,1,0),
 								                        f.Reason,
                         								f.Remark                                                       
                FROM
                        INTRANET_USER AS a
                                                        LEFT OUTER JOIN $card_log_table_name AS b
                                                                ON        (
                                                                                        a.UserID=b.UserID AND
                                                                                        (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
                                                                                )
                                                        LEFT OUTER JOIN CARD_STUDENT_OUTING AS c ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate')
                                                        LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON(a.UserID=d.StudentID AND d.RecordDate='$txt_year-$txt_month-$txt_day')
                                                        LEFT OUTER JOIN CARD_STUDENT_PRESET_LEAVE AS f ON (a.UserID=f.StudentID AND f.DayPeriod=3 AND f.RecordDate='$txt_year-$txt_month-$txt_day')
                WHERE
                                                (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
                                                a.RecordType=2 AND
                                                a.RecordStatus IN (0,1,2) AND
                                                a.ClassName = \"$class_name\"
                                                        ORDER BY a.ClassNumber ASC
                ";

$result = $LIDB->returnArray($sql,11);
echo "sql ".$sql."<br>";
$table_attend = "";
$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>$i_UserClassNumber</td><td>$i_UserName</td><td>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td><td>$i_SmartCard_Frontend_Take_Attendance_In_School_Station</td><td>$i_SmartCard_Frontend_Take_Attendance_PreStatus</td>".($preset_count>0?"<td>&nbsp;</td>":"")."<td>$i_SmartCard_Frontend_Take_Attendance_Status</td><td>$i_Attendance_Remark</td></tr>\n";

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name,$class_number, $in_school_time, $in_school_station, $attend_status,$remark,$preset_record,$preset_reason,$preset_remark) = $result[$i];

        $attend_status += 0;
        $select_status = "<SELECT name=drop_down_status[]>\n";
        /*
        if ($attend_status == 0 || $attend_status == 2)
        {
            $present_status = $attend_status;
        }
        else
        {
            $present_status = 0;
        }
        */
        #$select_status .= "<OPTION value='$present_status' ".($attend_status=="0" || $attend_status=="2"? "SELECTED":"").">$i_StudentAttendance_Status_Present</OPTION>\n";
        $select_status .= "<OPTION value='".CARD_STATUS_PRESENT."' ".($attend_status==CARD_STATUS_PRESENT? "SELECTED":"").">$i_StudentAttendance_Status_OnTime</OPTION>\n";
        $select_status .= "<OPTION value='".CARD_STATUS_ABSENT."' ".($attend_status==CARD_STATUS_ABSENT? "SELECTED":"").">$i_StudentAttendance_Status_PreAbsent</OPTION>\n";
        $select_status .= "<OPTION value='".CARD_STATUS_LATE."' ".($attend_status==CARD_STATUS_LATE? "SELECTED":"").">$i_StudentAttendance_Status_Late</OPTION>\n";
        $select_status .= "<OPTION value='".CARD_STATUS_OUTING."' ".($attend_status==CARD_STATUS_OUTING? "SELECTED":"").">$i_StudentAttendance_Status_Outing</OPTION>\n";
        $select_status .= "</SELECT>\n";

        $select_status .= "<input name=record_id[] type=hidden value=\"$record_id\">\n";
        $select_status .= "<input name=user_id[] type=hidden value=\"$user_id\">\n";
        switch ($attend_status){
                case CARD_STATUS_ABSENT: $note = $i_StudentAttendance_Status_PreAbsent;
                          $css = "attendance_norecord";
                          break;
                case CARD_STATUS_LATE: $note = $i_StudentAttendance_Status_Late;
                          $css = "attendance_late";
                          break;
                case CARD_STATUS_OUTING: $note = $i_StudentAttendance_Status_Outing;
                          $css = "attendance_outing";
                          break;
                default: $note = "&nbsp;";
                         $css = "tableContent";
                         break;
        }
        # Preset Absence
        if($preset_record==1){
	        $preset_table = "<table width=200 border=0 cellspacing=0 cellpadding=1><tr><td class=tipborder><table border=0 cellspacing=0 cellpadding=3 width=100%><tr><Td class=tipbg><B>[$i_SmartCard_DailyOperation_Preset_Absence]</b></td></tr><tr><td class=tipbg valign=top><font size=2>$i_UserName: $name ($class_number)</font></td></tr><tr><td class=tipbg valign=top><font size=2>$i_Attendance_Date: ".date('Y-m-d')."</font></td></tr><tr><td class=tipbg valign=top><font size=2>$i_Attendance_DayType: $i_DayTypePM</font></td></tr>";
        	$preset_table .= "<tr><td class=tipbg valign=top><font size=2>$i_Attendance_Reason: $preset_reason</font></td></tr><tr><td class=tipbg valign=top><font size=2>$i_Attendance_Remark: $preset_remark</font></td></tr>";
	        $preset_table.="</table></td></tr></table>";
	        $preset_abs_info = "<img onMouseOut='hidePresetAbs();' onMouseOver=\"showPresetAbs(this,'$preset_table')\" src=/images/preset_abs.gif align=absmiddle>";

	    }else $preset_abs_info ="&nbsp;";
        
        
        $x .= "<tr class=$css><td>$class_number</td><td>$name</td><td>$in_school_time</td><td>$in_school_station</td><td>$note</td>".($preset_count>0?"<td>$preset_abs_info</td>":"")."<td>$select_status</td><td>$remark</td></tr>\n";

}
$x .= "</table>\n";
$table_attend = $x;

$confirm_button = "<a href=\"javascript:AlertPost(document.form1,'view_student_update.php','$i_SmartCard_Confirm_Update_Attend?')\"><img src='$image_path/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$back_button = "<a href=\"class_status.php?period=$period&TargetDate=$TargetDate\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";
$functionbar = "<a class=iconLink href=javascript:AbsToPre()>".importIcon().$i_StudentAttendance_Action_SetAbsentToOntime."</a>";
?>
<div id="tooltip3" style='position:absolute; left:100px; top:30px; z-index:1; visibility: hidden'></div>
<script language="Javascript" src='/templates/tooltip.js'></script>

<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
        newWindow("view_student_print.php?class_name=<?=$class_name?>&period=<?=$period?>&TargetDate=<?=$TargetDate?>",4);
}
function AbsToPre()
{
         obj = document.form1;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="drop_down_status[]")
                {
                    obj2 = obj.elements[i];
                    if (obj2.selectedIndex == 1)
                    {
                        obj2.selectedIndex = 0;
                    }
                }
        }
}
// for Preset Absence
function showPresetAbs(obj,reason){
			var pos_left = getPostion(obj,"offsetLeft");
			var pos_top  = getPostion(obj,"offsetTop");
			
			offsetX = (obj==null)?0:obj.width;
			//offsetY = (obj==null)?0:obj.height;
			offsetY =0;
			objDiv = document.getElementById('tooltip3');
			if(objDiv!=null){
				objDiv.innerHTML = reason;
				objDiv.style.visibility='visible';
				objDiv.style.top = pos_top+offsetY;
				objDiv.style.left = pos_left+offsetX;
				setDivVisible(true, "tooltip3", "lyrShim");
			}

}

// for preset absence
function hidePresetAbs(){
		obj = document.getElementById('tooltip3');
		if(obj!=null)
			obj.style.visibility='hidden';
		setDivVisible(false, "tooltip3", "lyrShim");
}
/*
function openPrintPage()
{
        newWindow("view_student_print.php?class_name=<?=$class_name?>&period=<?=$period?>&TargetDate=<?=$TargetDate?>",4);
}
function AbsToPre()
{
         obj = document.form1;
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="drop_down_status[]")
                {
                    obj2 = obj.elements[i];
                    if (obj2.selectedIndex == 1)
                    {
                        obj2.selectedIndex = 0;
                    }
                }
        }
}
*/
-->
</script>

<form name="form1" method="post" action="" method="post">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewClassStatus, 'index.php',$display_period,'class_status.php?period='.$period.'&TargetDate='.$TargetDate,$class_name,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<iframe id='lyrShim' src='javascript:false;' scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; display:none;'></iframe>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?=$table_confirm?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td colspan=2><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$toolbar?></td><td class=admin_bg_menu align=right><?=$functionbar?></td></tr>
<tr><td colspan=2 class=tableContent align=center><?=$table_attend?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
<tr><td>&nbsp;</td></tr>
<tr><td align=right><?=$confirm_button.toolBarSpacer().btnReset().toolBarSpacer().$back_button?></td></tr>
</table>

<input name=class_name type=hidden value="<?=$class_name?>">
<input name=period type=hidden value="<?=$period?>">
<input name=confirmed_id type=hidden value="<?=$confirmed_id?>">
<input name=TargetDate type=hidden value="<?=$TargetDate?>">

</form>
<br><br>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
