<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$header_filepath = "$intranet_root/templates/reportheader/viewstudentattendance.php";
$header_inclusion = is_file($header_filepath);

### internal function
function printHeader($period_slot,$target_date,$ClassName){
        global $intranet_root, $i_StudentAttendance_Menu_DailyOperation;
        global $i_StudentAttendance_System, $i_StudentAttendance_ViewTodayRecord;
        global $i_StudentAttendance_Slot, $i_StudentAttendance_Slot_AM, $i_StudentAttendance_Slot_PM, $i_StudentAttendance_Slot_AfterSchool;
        global $i_StudentAttendance_View_Date, $i_StudentAttendance_ToSchoolTime;
        global $i_ClassName, $i_UserStudentName, $i_SmartCard_Frontend_Take_Attendance_In_School_Time, $i_StudentAttendance_Status;
        global $header_inclusion,$header_filepath;
        global $i_Attendance_Remark;
        global $i_Payment_Receipt_Payment_StaffInCharge_Signature;
        global $i_SmartCard_Frontend_Take_Attendance_CancelLunch;

        if ($header_inclusion)
        {
            include($header_filepath);
        }
        $pageheader = "";
        $pageheader .= "<table width='560' border='0'>";
        $pageheader .= "<tr><td colspan='2'><u>$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)</u></td></tr>";

        $pageheader .= "<tr><td>$i_ClassName</td><td align=left>$ClassName</td></tr>";
        $pageheader .= "<tr><td>$i_StudentAttendance_Slot</td><td>";
        switch ($period_slot)
        {
                        case 1: $pageheader .= "$i_StudentAttendance_Slot_AM"; break;
                        case 2: $pageheader .= "$i_StudentAttendance_Slot_PM"; break;
                        default : $pageheader .= ""; break;
        }
        $pageheader .= "</tr>";
        $pageheader .= "<tr><td>$i_StudentAttendance_View_Date</td><td>$target_date</td></tr>";
        $pageheader .= "</table>";

        if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
        {
            $LunchBoxOption_header ="<td width=1%>$i_SmartCard_Frontend_Take_Attendance_CancelLunch</td>";
        }

        $pageheader .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>";
        $pageheader .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$i_SmartCard_Frontend_Take_Attendance_In_School_Time</td><td>$i_StudentAttendance_Status</td><td>$i_Attendance_Remark</td>$LunchBoxOption_header</tr>\n";
        echo $pageheader;
        #return $pageheader;
}

### class used
$li = new libdb();
$lcardattend = new libcardstudentattend2();

$page_breaker = "<P CLASS='breakhere'>";
$pagefooter = "</table>";
$prevClass = "";

$i_title = "$i_StudentAttendance_System ($i_StudentAttendance_Menu_DailyOperation)";
include_once("../../../../templates/fileheader.php");
?>
<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### build table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

$conds = "";

if($class_name<>"")
{
        $conds = " AND a.ClassName = \"$class_name\" ";
}

$db_status_field = ($period==2?"b.PMStatus":"b.AMStatus");

if ($option == 1)
{
}
else if ($option == 2)
{
     $conds .= " AND ($db_status_field = ".CARD_STATUS_ABSENT." OR ($db_status_field IS NULL AND b.InSchoolTime IS NULL AND c.OutingID IS NULL))";
}
else if ($option == 3)
{
     $conds .= " AND $db_status_field = ".CARD_STATUS_LATE;
}
else if ($option == 4)
{
     $conds .= " AND ($db_status_field = ".CARD_STATUS_LATE." OR $db_status_field = ".CARD_STATUS_ABSENT." OR ($db_status_field IS NULL AND b.InSchoolTime IS NULL AND c.OutingID IS NULL))";
}

if($period==1)        // AM
{
        $sql  = "SELECT        ".getNameFieldByLang("a.")."as name,
                                                                a.ClassName,
                                                                a.ClassNumber,
                                                                IF(b.InSchoolTime IS NULL, '-', b.InSchoolTime),
                                                                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                                                                IF(b.AMStatus IS NOT NULL,
                                                                                b.AMStatus,
                                                                                CASE
                                                                                        WHEN b.InSchoolTime IS NOT NULL THEN '".CARD_STATUS_PRESENT."'
                                                                                        WHEN b.InSchoolTime IS NULL AND c.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
                                                                                        WHEN b.InSchoolTime IS NULL AND c.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
                                                                                END
                                                                        ),
                                                                 IF(d.Remark IS NULL, '-',d.Remark),
                                                                 a.UserID
                                        FROM
                                                        INTRANET_USER AS a
                                                                LEFT OUTER JOIN $card_log_table_name AS b
                                                                        ON        (
                                                                                                a.UserID=b.UserID AND
                                                                                                (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
                                                                                        )
                                                                LEFT OUTER JOIN CARD_STUDENT_OUTING AS c
                                                                        ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate')
                                                                LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON(a.UserID=d.StudentID AND d.RecordDate='$TargetDate')
                                        WHERE
                                                        (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
                                                        a.RecordType=2 AND
                                                        a.RecordStatus IN (0,1,2) AND
                                                        a.ClassName IS NOT NULL AND a.ClassName !=\"\"
                                                        $conds
                                                                ORDER BY a.ClassName, a.ClassNumber, a.EnglishName

                                        ";
}
else if($period==2)         //PM
{
        $sql  = "SELECT        ".getNameFieldByLang("a.")."as name,
                                                                a.ClassName,
                                                                a.ClassNumber,
                                                                IF(b.LunchBackTime IS NULL,
                                                                        '-',
                                                                        b.LunchBackTime),
                                                                IF(b.LunchBackStation IS NULL,
                                                                        '-',
                                                                        b.LunchBackStation),
                                                                IF(b.PMStatus IS NOT NULL,
                                                                                b.PMStatus,
                                                                                CASE
                                                                                        WHEN        (b.InSchoolTime IS NOT NULL AND b.LunchOutTime IS NULL) OR
                                                                                                                (b.LunchBackTime IS NOT NULL)
                                                                                                                        THEN '".CARD_STATUS_PRESENT."'
                                                                                        WHEN b.InSchoolTime IS NULL AND c.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
                                                                                        WHEN b.InSchoolTime IS NULL AND c.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
                                                                                END
                                                                        ),
                                                                  IF(d.Remark IS NULL,'-',d.Remark),
                                                                  a.UserID
                                        FROM
                                                        INTRANET_USER AS a
                                                                LEFT OUTER JOIN $card_log_table_name AS b
                                                                        ON        (
                                                                                                a.UserID=b.UserID AND
                                                                                                (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL)
                                                                                        )
                                                                LEFT OUTER JOIN CARD_STUDENT_OUTING AS c
                                                                        ON (a.UserID=c.UserID AND c.RecordDate = '$TargetDate')
                                                                LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (a.UserID=d.StudentID AND d.RecordDate='$TargetDate')
                                        WHERE
                                                        (b.DayNumber = ".$txt_day." ||  b.DayNumber IS NULL) AND
                                                        a.RecordType=2 AND
                                                        a.RecordStatus IN(0,1,2) AND
                                                        a.ClassName IS NOT NULL AND a.ClassName !=\"\"
                                                        $conds
                                                                ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
                                        ";
}


$result = $li->returnArray($sql,8);
$background="#FFFFFF";
$textcolor = "#000000";
# Lunch cancellation option
if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
{
    $lunch_tablename = $lcardattend->createTable_Card_Student_Lunch_Box_Option($txt_year, $txt_month);
    $sql = "SELECT b.StudentID, b.CancelOption FROM
                   INTRANET_USER as a LEFT OUTER JOIN $lunch_tablename as b ON b.DayNumber = ".$txt_day." AND a.UserID = b.StudentID
                   WHERE
                   b.DayNumber = ".$txt_day." AND
                    a.RecordType=2 AND
                    a.RecordStatus IN (0,1,2) AND
                    a.ClassName IS NOT NULL AND a.ClassName !=\"\"
                    $conds
                   ";
    $temp = $lcardattend->returnArray($sql,2);
    $lunch_option_result = build_assoc_array($temp);
}


for ($i=0; $i<sizeof($result); $i++)
{
        list ($name,$ClassName,$ClassNumber,$InSchoolTime,$InSchoolStation,$attend_status,$remark, $t_studentID) = $result[$i];

        if ($i==0)
        {
                printHeader($period,$TargetDate ,$ClassName);
                echo "\n";
        }

        if ($i!=0 && $prevClass != $ClassName)
        {
                #$display_class_name = "<tr colspan=3><td>$ClassName</td></tr></table>\n";

                echo "$pagefooter\n$page_breaker\n";
                printHeader($period,$TargetDate,$ClassName);
                echo "\n";
                $prevClass = $ClassName;
        }
        $prevClass = $ClassName;

        switch ($attend_status)
        {
                        case CARD_STATUS_PRESENT : $note = $i_StudentAttendance_Status_Present;
                                          break;
                        case CARD_STATUS_ABSENT : $note = $i_StudentAttendance_Status_PreAbsent;
                                          break;
                        case CARD_STATUS_LATE : $note = $i_StudentAttendance_Status_Late;
                                          break;
                        case CARD_STATUS_OUTING : $note = $i_StudentAttendance_Status_Outing;
                                          break;
                        default: $note = "";
                                         break;
        }

     $class_str = ($ClassName != "" && $ClassNumber != "")? "($ClassName - $ClassNumber)":"";

     if ($sys_custom['SmartCardAttendance_LunchBoxOption'])
     {
         $lunch_cancel_status_text = "<td>".(($lunch_option_result[$t_studentID]==1)?"$button_cancel":"")."&nbsp;</td>";
     }
     else
     {
         $lunch_cancel_status_text = "";
     }
     echo "<tr bgcolor='$background'><td><font color='$textcolor'>$name $class_str</font></td><td><font color='$textcolor'>$InSchoolTime</font></td><td><font color='$textcolor'>$note</font></td><td>$remark</td>$lunch_cancel_status_text</tr>\n";
}
echo "</table>\n";

echo "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img class=print_hide src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";

intranet_closedb();
?>
