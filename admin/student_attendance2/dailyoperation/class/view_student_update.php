<?php
# using: kenneth chung
$PATH_WRT_ROOT = "../../../../";

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");


intranet_opendb();

$lcardattend = new libcardstudentattend2();

# check page load time 
if($lcardattend->isDataOutDated($user_id,$TargetDate,$PageLoadTime)){
	$error = 1;  // data outdated
	if($period=="1")
	        $return_page="view_student_AM.php";
	else if($period=="2")
	        $return_page = "view_student_PM.php";
	
	$return_page = $return_page."?class_name=$class_name&class_id=$class_id&period=$period&TargetDate=$TargetDate&error=$error";
	header("Location: $return_page");
	exit();	
}

# LOCK TABLES
//$lcardattend->lockTablesAdmin();


################################################
$directProfileInput = false;
################################################


# class used
$LIDB = new libdb();
$LICLASS = new libclass();

if ($plugin['Discipline']){
	include_once("../../../../includes/libdiscipline.php");

        $ldiscipline = new libdiscipline();
        $LateCtgInUsed = $ldiscipline->CategoryInUsed(1);
}


if ($plugin['Disciplinev12']){
	include_once("../../../../includes/libdisciplinev12.php");
        $ldisciplinev12 = new libdisciplinev12();
        $LateCtgInUsed = $ldisciplinev12->CategoryInUsed(PRESET_CATEGORY_LATE);
}

$confirmed_user_id = -1;

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}

$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);
$year = getCurrentAcademicYear();
$semester = getCurrentSemester();


### for student confirm
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;


### Bug tracing
if($bug_tracing['smartcard_student_attend_status']){
	include_once("../../../../includes/libfilesystem.php");
	if(!is_dir($file_path."/file/log_student_attend_status")){
		$lf = new libfilesystem();
		$lf->folder_new($file_path."/file/log_student_attend_status");
	}
	$log_filepath = "$file_path/file/log_student_attend_status/log_student_attend_status_".($txt_year.$txt_month.$txt_day).".txt";
}



for($i=0; $i<sizeOf($user_id); $i++)
{
        $my_user_id = $user_id[$i];
        $my_day = $txt_day;
        $my_drop_down_status = $drop_down_status[$i];
        $my_record_id = $record_id[$i];
        
        if( $period == "1")            # AM
        {
                # insert if not exist
                if($my_record_id=="")
                {
                        $sql = "INSERT INTO $card_log_table_name
                                                        (UserID,DayNumber,AMStatus,DateInput,DateModified) VALUES
                                                        ($my_user_id,$my_day,$my_drop_down_status,NOW(),NOW())
                                                ";
                        $LIDB->db_db_query($sql);
                        
                        ## Bug Tracing
                        if($bug_tracing['smartcard_student_attend_status']){
	                        $log_date = date('Y-m-d H:i:s');
	                        $log_target_date = $TargetDate;
	                        $log_student_id = $my_user_id;
	                        $log_old_status = "";
	                        $log_new_status = $my_drop_down_status;
	                        $log_sql = $sql;
	                        $log_admin_user = $PHP_AUTH_USER;
	                        
             				$log_page = 'view_student_update.php';
				            $log_content = get_file_content($log_filepath);
				            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
				            $log_content .= $log_entry;
				            write_file_content($log_content, $log_filepath);	                        
	                    }
                        
                        # Check Bad actions
                        if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
                        {
	                        $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
	                        
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
                        }
                        else if ($my_drop_down_status==CARD_STATUS_LATE)
                        {
   	                        $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
   	                        
                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");

                            #########################################################
                            # Add late to student profile
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list($targetClass, $targetClassNumber) = $temp[0];
                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                            $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $insert_id = $lcardattend->db_insert_id();

                            /*
                            # Calculate upgrade items
                            if ($plugin['Discipline'])
                            {
                                $student_id = $my_user_id;
                                $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                $ldiscipline->calculateUpgradeLateToDetention($student_id);
                            }
                            */
                            
                            # Calculate upgrade items
                             if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
								$dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $school_year;
								$semester = retrieveSemester($TargetDate);
								$dataAry['Semester'] = $semester;
								$dataAry['StudentAttendanceID'] = $insert_id;
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
								
	                    	} else if ($plugin['Discipline'] && $LateCtgInUsed)
                             {
                                     $student_id = $my_user_id;
                                     $t_date = $TargetDate;
                                     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date);
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                                     }
                                     else{
                                         $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                         $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                     }
                             }

                            if (!$insert_id)
                            {
                                 $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                            WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                            AND RecordType = '".PROFILE_TYPE_LATE."'
                                            AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                                 $temp = $lcardattend->returnVector($sql);
                                 $insert_id = $temp[0];
                            }

                            # Update to reason table
                            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                            $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
                            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                           VALUES ($fieldsvalues)";
                            $lcardattend->db_db_query($sql);
                            #########################################################
                                     
                            /*
                            if ($directProfileInput)
                            {
                                # Add late to student profile
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list($targetClass, $targetClassNumber) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                                $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $insert_id = $lcardattend->db_insert_id();

                                # Calculate upgrade items
                                if ($plugin['Discipline'])
                                {
                                    $student_id = $my_user_id;
                                    $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                    $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                }

                                if (!$insert_id)
                                {
                                     $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                AND RecordType = '".PROFILE_TYPE_LATE."'
                                                AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                                     $temp = $lcardattend->returnVector($sql);
                                     $insert_id = $temp[0];
                                }

                                # Update to reason table
                                $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                                $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
                                $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                               VALUES ($fieldsvalues)";
                                $lcardattend->db_db_query($sql);
                            }
                            else
                            {}
                            */
                        }else if ($my_drop_down_status==CARD_STATUS_ABSENT){ # No Daily Record and Absent
		                        //$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate); 
		                        //$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
	                    }else if ($my_drop_down_status==CARD_STATUS_OUTING){
		                    # 
		                }

                }
                else  # update if exist
                {
                     # Grab original status
                     $sql = "SELECT AMStatus, InSchoolTime, RecordID FROM $card_log_table_name WHERE RecordID = $my_record_id";
                     $temp = $LIDB->returnArray($sql,3);
                     list($old_status, $old_inTime, $old_record_id) = $temp[0];
                     
                     if ($old_status != $my_drop_down_status)
                     {
                         # Check bad actions
                         # Late / Present -> Absent
                         if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                         {
	                         $lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
	                         if($old_inTime!=""){
	                               # Has card record but not present in classroom
	                               $lcardattend->addBadActionFakedCardAM($my_user_id,$TargetDate,$old_inTime);
                             }
                         }
                         
                         # Absent -> Late / Present
                         if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
                         {
                               $lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
	                               # forgot to bring card
	                               $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,$old_inTime);
                         }
                         
                         # Absent -> Outing
                         if($old_status == CARD_STATUS_ABSENT && $my_drop_down_status==CARD_STATUS_OUTING){
                         
	                     }
                         
                         # Late / Present -> Outing
                         if( ($old_status == CARD_STATUS_PRESENT || $old_status == CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_OUTING){
	                     }
                         
                         # Outing -> Absent
                         if($old_status == CARD_STATUS_OUTING && $my_drop_down_status==CARD_STATUS_ABSENT){
                               //$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
                               //$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
	                     }
                         
                         # Outing -> Late/Present
                         if($old_status == CARD_STATUS_OUTING && ($my_drop_down_status==CARD_STATUS_PRESENT ||$my_drop_down_status==CARD_STATUS_LATE)){
                               //$lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
                               //$lcardattend->removeBadActionFakedCardAM($my_user_id,$TargetDate);
	                     }

                         # Update Daily table
                         $sql = "UPDATE $card_log_table_name SET AMStatus=$my_drop_down_status,  DateModified = NOW() WHERE RecordID=$my_record_id";
                         $LIDB->db_db_query($sql) or die(mysql_error());
                    
                         ## Bug Tracing
                        if($bug_tracing['smartcard_student_attend_status']){
	                        $log_date = date('Y-m-d H:i:s');
	                        $log_target_date = $TargetDate;
	                        $log_student_id = $my_user_id;
	                        $log_old_status = $old_status;
	                        $log_new_status = $my_drop_down_status;
	                        $log_sql = $sql;
	                        $log_admin_user = $PHP_AUTH_USER;
	                        
             				$log_page = 'view_student_update.php';
				            $log_content = get_file_content($log_filepath);
				            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
				            $log_content .= $log_entry;
				            write_file_content($log_content, $log_filepath);	                        
	                     }
                        
                         if ($old_status == CARD_STATUS_ABSENT)
                         {
                             # Remove Previous Absent Record
                             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                                  AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                             $lcardattend->db_db_query($sql);
                             $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                                  AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                             $lcardattend->db_db_query($sql);
                         }

                         if ($old_status!=CARD_STATUS_LATE && $my_drop_down_status==CARD_STATUS_LATE)
                         {
                             # Add late to student profile
                             $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($targetClass, $targetClassNumber) = $temp[0];
                             $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                             $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_AM."',now(),now(),'$targetClass','$targetClassNumber'";
                             $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                             $lcardattend->db_db_query($sql);
                             $insert_id = $lcardattend->db_insert_id();

                             # Calculate upgrade items
                             if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
								$dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $school_year;
								$semester = retrieveSemester($TargetDate);
								$dataAry['Semester'] = $semester;
								$dataAry['StudentAttendanceID'] = $insert_id;
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
	                    	}
	                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                             {
                                     $student_id = $my_user_id;
                                     $t_date = $TargetDate;
                                     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date);
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                                     }
                                     else{
                                         $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                         $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                     }
                             }

                             if (!$insert_id)
                             {
                                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                 WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'
                                                 AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                                  $temp = $lcardattend->returnVector($sql);
                                  $insert_id = $temp[0];
                             }
                             # Update to reason table
                             $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                             $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_AM."', now(), now() ";
                             $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                            VALUES ($fieldsvalues)";
                             $lcardattend->db_db_query($sql);
                         }
                         
                         if ($old_status==CARD_STATUS_LATE && $my_drop_down_status!=CARD_STATUS_LATE)
                         {
                             //# Remove Profile Record
                             $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE StudentID = '$my_user_id' AND RecordDate = '$TargetDate'
                                                  AND DayType = '".PROFILE_DAY_TYPE_AM."' AND RecordType = '".PROFILE_TYPE_LATE."'";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($tmp_record_id , $tmp_profile_id) = $temp[0];
                             
                             # $tmp_record_id <== RecordID [CARD_STUDENT_PROFILE_RECORD_REASON]
                             # $tmp_profile_id <== StudentAttendanceID [PROFILE_STUDENT_ATTENDANCE]
                             if ($tmp_record_id != '')
                             {
	                             # remove late reason [CARD_STUDENT_PROFILE_RECORD_REASON]
                                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$tmp_record_id'";
                                 $lcardattend->db_db_query($sql);
                                 
                                 /*
                                 # update attendance record as "delete"
                                 $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE set RecordStatus=-1 WHERE StudentAttendanceID = '$tmp_profile_id'";
                                 $lcardattend->db_db_query($sql);
                                 */
                                 if ($tmp_profile_id != '')
                                 {
                                     # Reset upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
											$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($tmp_profile_id);
								
                                     } else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                             $target_date = $TargetDate;
                                             $student_id = $my_user_id;
                                             if($ldiscipline->isUseAccumulativeLateSetting($target_date)){
			                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($student_id,$target_date);
			                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($student_id,$target_date);    
                                             }
                                             else{
                                                 $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
                                                 $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
                                             }
                                     }

                                     

                                     # Calculate upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
									{
										# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
			                    	}
			                    	else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                             $student_id = $my_user_id;
                                             $t_date = $TargetDate;
                                             if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                                 $ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date);
                                                 $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                                             }
                                             else{
                                                 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                                 $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                             }
                                     }
                                     
                                     # delete attendance record
                                     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$tmp_profile_id'";
                                     $lcardattend->db_db_query($sql);
                                 }
                             }
                         }

                         
                     }


                     # Try to remove profile records if applicable
                     if ($my_drop_down_status != CARD_STATUS_ABSENT)
                     {
                         # Remove Previous Absent Record
                         $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                              AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                         $lcardattend->db_db_query($sql);
                         $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                              AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_AM."'";
                         $lcardattend->db_db_query($sql);
                     }

                }
        }
        else if( $period == "2")	############################# PM
        {
	        
                # insert if not exist
                if($my_record_id=="")
                {
                        $sql = "INSERT INTO $card_log_table_name
                                                        (UserID,DayNumber,PMStatus,DateInput,DateModified) VALUES
                                                        ($my_user_id,$my_day,$my_drop_down_status,NOW(),NOW())
                                                ";
                        $LIDB->db_db_query($sql);
                        
                        
                        ## Bug Tracing
                        if($bug_tracing['smartcard_student_attend_status']){
	                        $log_date = date('Y-m-d H:i:s');
	                        $log_target_date = $TargetDate;
	                        $log_student_id = $my_user_id;
	                        $log_old_status ="";
	                        $log_new_status = $my_drop_down_status;
	                        $log_sql = $sql;
	                        $log_admin_user = $PHP_AUTH_USER;
	                        
             				$log_page = 'view_student_update.php';
				            $log_content = get_file_content($log_filepath);
				            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
				            $log_content .= $log_entry;
				            write_file_content($log_content, $log_filepath);	                        
	                        
	                        
	                        
	                    }                        
                        
                        
                        # Check Bad actions
                        if ($my_drop_down_status==CARD_STATUS_PRESENT)           # empty -> present
                        {
	                        $lcardattend->removeBadActionFakedCardPM($my_user_id,$TargetDate);

                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");
                        }
                        else if ($my_drop_down_status==CARD_STATUS_LATE)
                        {
   	                        $lcardattend->removeBadActionFakedCardPM($my_user_id,$TargetDate);

                            # Forgot to bring card
                            $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,"");

                            ##########################################################
                            # Add late to student profile
                            $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                            $temp = $lcardattend->returnArray($sql,2);
                            list($targetClass, $targetClassNumber) = $temp[0];
                            $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                            $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
                            $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                            $lcardattend->db_db_query($sql);
                            $insert_id = $lcardattend->db_insert_id();
                            # Calculate upgrade items
                            if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
								$dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $school_year;
								$semester = retrieveSemester($TargetDate);
								$dataAry['Semester'] = $semester;
								$dataAry['StudentAttendanceID'] = $insert_id;
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
	                    	} else if ($plugin['Discipline'] && $LateCtgInUsed)
                             {
                                     $student_id = $my_user_id;
                                     $t_date = $TargetDate;
                                     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date);
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                                     }
                                     else{
                                         $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                         $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                     }
                             }

                            if (!$insert_id)
                            {
                                 $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                AND RecordType = '".PROFILE_TYPE_LATE."'
                                                AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                                 $temp = $lcardattend->returnVector($sql);
                                 $insert_id = $temp[0];
                            }

                            # Update to reason table
                            $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                            $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
                            $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                           VALUES ($fieldsvalues)";
                            $lcardattend->db_db_query($sql);
                            ############################################################################
                                
                            /*
                            if ($directProfileInput)
                            {
                                # Add late to student profile
                                $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                                $temp = $lcardattend->returnArray($sql,2);
                                list($targetClass, $targetClassNumber) = $temp[0];
                                $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                                $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
                                $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                                $lcardattend->db_db_query($sql);
                                $insert_id = $lcardattend->db_insert_id();

                                # Calculate upgrade items
                                if ($plugin['Discipline'])
                                {
                                    $student_id = $my_user_id;
                                    $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                    $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                }

                                if (!$insert_id)
                                {
                                     $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                    WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                    AND RecordType = '".PROFILE_TYPE_LATE."'
                                                    AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                                     $temp = $lcardattend->returnVector($sql);
                                     $insert_id = $temp[0];
                                }

                                # Update to reason table
                                $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                                $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
                                $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                               VALUES ($fieldsvalues)";
                                $lcardattend->db_db_query($sql);
                            }
                            */
                        }



                }
                # update if exist
                else
                {
	                
                     # Grab original status
                     $sql = "SELECT PMStatus, InSchoolTime, LunchOutTime, LunchBackTime, RecordID FROM $card_log_table_name WHERE RecordID = $my_record_id";
                     $temp = $LIDB->returnArray($sql,5);
                     list($old_status, $old_inTime,$old_lunchOutTime, $old_lunchBackTime, $old_record_id) = $temp[0];
                     
                     ## get old time
			        if ($lcardattend->attendance_mode==1){ # PM only
						$bad_action_time_field = $old_inTime;
					}else{
						$bad_action_time_field = $old_lunchBackTime;
					}
                     if ($old_status != $my_drop_down_status)
                     {
                         
                         if ( ($old_status==CARD_STATUS_PRESENT || $old_status==CARD_STATUS_LATE) && $my_drop_down_status==CARD_STATUS_ABSENT)
                         {
	                         
                               $lcardattend->removeBadActionNoCardEntrance($my_user_id,$TargetDate);
								if($bad_action_time_field!=""){ # has time but absent
	                               # Has card record but not present in classroom
	                               $lcardattend->addBadActionFakedCardPM($my_user_id,$TargetDate,$bad_action_time_field);
                               }
                         }
                         
                         # Absent -> Late / Present
                         if ( $old_status==CARD_STATUS_ABSENT && ($my_drop_down_status==CARD_STATUS_PRESENT || $my_drop_down_status==CARD_STATUS_LATE))
                         {
                               $lcardattend->removeBadActionFakedCardPM($my_user_id,$TargetDate);
                               # forgot to bring card
                               $lcardattend->addBadActionNoCardEntrance($my_user_id,$TargetDate,$bad_action_time_field);
                         }
                         
                         
						
                         # Update Daily table
                         $sql = "UPDATE $card_log_table_name SET PMStatus=$my_drop_down_status, DateModified = now() WHERE RecordID=$my_record_id";
                         $LIDB->db_db_query($sql);
                         
                        ## Bug Tracing
                        if($bug_tracing['smartcard_student_attend_status']){
	                        $log_date = date('Y-m-d H:i:s');
	                        $log_target_date = $TargetDate;
	                        $log_student_id = $my_user_id;
	                        $log_old_status = $old_status;
	                        $log_new_status = $my_drop_down_status;
	                        $log_sql = $sql;
	                        $log_admin_user = $PHP_AUTH_USER;
	                        
             				$log_page = 'view_student_update.php';
				            $log_content = get_file_content($log_filepath);
				            $log_entry = "\"$log_page\",\"$log_student_id\",\"$log_target_date\",\"$log_old_status\",\"$log_new_status\",\"$log_sql\",\"$log_admin_user\",\"$log_date\"\n";
				            $log_content .= $log_entry;
				            write_file_content($log_content, $log_filepath);	                        
	                        
	                        
	                        
	                    }                         
                         

                         // Remove old late/absent records
                         if ($old_status==CARD_STATUS_LATE)
                         {
	                         /*
                             # Remove Previous Absent Record
                             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                            WHERE RecordType = '".PROFILE_TYPE_LATE."' AND UserID = '$my_user_id'
                                                  AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                             $lcardattend->db_db_query($sql);
                             
                             $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE RecordType = '".PROFILE_TYPE_LATE."' AND StudentID = '$my_user_id'
                                                  AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                             $lcardattend->db_db_query($sql);
                             */
                         }
                         else if ($old_status == CARD_STATUS_ABSENT)
                         {
                             # Remove Previous Absent Record
                             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                                  AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                             $lcardattend->db_db_query($sql);
                             $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                                  AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                             $lcardattend->db_db_query($sql);
                         }


                     }

                     if ($my_drop_down_status != CARD_STATUS_ABSENT)
                     {
                         # Remove Previous Absent Record
                         $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND UserID = '$my_user_id'
                                              AND AttendanceDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                         $lcardattend->db_db_query($sql);
                         $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                        WHERE RecordType = '".PROFILE_TYPE_ABSENT."' AND StudentID = '$my_user_id'
                                              AND RecordDate = '$TargetDate' AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                         $lcardattend->db_db_query($sql);
                     }


                         if ($old_status!=CARD_STATUS_LATE && $my_drop_down_status==CARD_STATUS_LATE)
                         {
                             # Add late to student profile
                             $sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = $my_user_id";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($targetClass, $targetClassNumber) = $temp[0];
                             $fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, DateInput,DateModified,ClassName,ClassNumber";
                             $fieldvalue = "'$my_user_id','$TargetDate','$year','$semester','".PROFILE_TYPE_LATE."','".PROFILE_DAY_TYPE_PM."',now(),now(),'$targetClass','$targetClassNumber'";
                             $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";
                             $lcardattend->db_db_query($sql);
                             $insert_id = $lcardattend->db_insert_id();

                             # Calculate upgrade items
                             if ($plugin['Disciplinev12'] && $LateCtgInUsed)
							{
								$dataAry = array();
								$dataAry['StudentID'] = $my_user_id;
								$dataAry['RecordDate'] = $TargetDate;
								$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($TargetDate);
								$dataAry['Year'] = $school_year;
								$semester = retrieveSemester($TargetDate);
								$dataAry['Semester'] = $semester;
								$dataAry['StudentAttendanceID'] = $insert_id;
								$ldisciplinev12->INSERT_LATE_MISCONDUCT_RECORD($dataAry);
	                    	} else if ($plugin['Discipline'] && $LateCtgInUsed)
                             {
                                     $student_id = $my_user_id;
                                     $t_date = $TargetDate;
                                     if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date);
                                         $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                                     }
                                     else{
                                         $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                         $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                     }
                             }

                             if (!$insert_id)
                             {
                                  $sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE
                                                 WHERE UserID = $my_user_id AND AttendanceDate = '$TargetDate'
                                                 AND RecordType = '".PROFILE_TYPE_LATE."'
                                                 AND DayType = '".PROFILE_DAY_TYPE_PM."'";
                                  $temp = $lcardattend->returnVector($sql);
                                  $insert_id = $temp[0];
                             }
                             # Update to reason table
                             $fieldname = "RecordDate, StudentID, ProfileRecordID, RecordType, DayType, DateInput, DateModified";
                             $fieldsvalues = "'$TargetDate', '$my_user_id', '$insert_id', '".PROFILE_TYPE_LATE."', '".PROFILE_DAY_TYPE_PM."', now(), now() ";
                             $sql = "INSERT INTO CARD_STUDENT_PROFILE_RECORD_REASON ($fieldname)
                                            VALUES ($fieldsvalues)";
                             $lcardattend->db_db_query($sql);
                         }
                         
                         if ($old_status==CARD_STATUS_LATE && $my_drop_down_status!=CARD_STATUS_LATE)
                         {
                             # Remove Profile Record
                             $sql = "SELECT RecordID, ProfileRecordID FROM CARD_STUDENT_PROFILE_RECORD_REASON
                                            WHERE StudentID = '$my_user_id' AND RecordDate = '$TargetDate'
                                                  AND DayType = '".PROFILE_DAY_TYPE_PM."' AND RecordType = '".PROFILE_TYPE_LATE."'";
                             $temp = $lcardattend->returnArray($sql,2);
                             list($tmp_record_id , $tmp_profile_id) = $temp[0];
                             if ($tmp_record_id != '')
                             {
                                 $sql = "DELETE FROM CARD_STUDENT_PROFILE_RECORD_REASON WHERE RecordID = '$tmp_record_id'";
                                 $lcardattend->db_db_query($sql);
                                 
                                 # update attendance record as "delete"
                                 $sql = "UPDATE PROFILE_STUDENT_ATTENDANCE set RecordStatus=-1 WHERE StudentAttendanceID = '$tmp_profile_id'";
                                 $lcardattend->db_db_query($sql);
                                 
                                 if ($tmp_profile_id != '')
                                 {
                                  # Reset upgrade items
                                  if ($plugin['Disciplinev12'] && $LateCtgInUsed)
                                     {
                                        $ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($tmp_profile_id);
                                     } else if ($plugin['Discipline'] && $LateCtgInUsed)
                                 {
                                         $target_date = $TargetDate;
                                         $student_id = $my_user_id;
                                         if($ldiscipline->isUseAccumulativeLateSetting($target_date)){
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDemerit($student_id,$target_date);
		                                     $ldiscipline->resetAccumulativeUpgradeLateToDetention($student_id,$target_date);
											}
                                         else{
                                             $ldiscipline->resetUpgradeLateToDemerit($student_id,$target_date);
                                             $ldiscipline->resetUpgradeLateToDetention($student_id,$target_date);
                                         }
                                 }

                                     # Calculate upgrade items
                                     if ($plugin['Disciplinev12'] && $LateCtgInUsed)
									{
										# calculation flow done in function DELETE_LATE_MISCONDUCT_RECORD
			                    	} else if ($plugin['Discipline'] && $LateCtgInUsed)
                                     {
                                             $student_id = $my_user_id;
                                             $t_date = $TargetDate;
                                             if($ldiscipline->isUseAccumulativeLateSetting($t_date)){
                                                 $ldiscipline->calculateAccumulativeUpgradeLateToDemerit($student_id,$t_date);
                                                 $ldiscipline->calculateAccumulativeUpgradeLateToDetention($student_id,$t_date);
                                             }
                                             else{
                                                 $ldiscipline->calculateUpgradeLateToDemerit($student_id);
                                                 $ldiscipline->calculateUpgradeLateToDetention($student_id);
                                             }
                                     }
                                     
                                     $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID = '$tmp_profile_id'";
                                      $lcardattend->db_db_query($sql);
                                      
                                 }
                             }
                         }


                }
        }
}

### for class confirm
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

if( $confirmed_id <> "" )
{
        # update if record exist
        $sql = "UPDATE $card_student_daily_class_confirm SET ConfirmedUserID=$confirmed_user_id, DateModified = NOW() WHERE RecordID = '$confirmed_id'";
        $LIDB->db_db_query($sql);
        $msg = 2;
}
else
{
        # insert if record not exist
        $class_id = $LICLASS->getClassID($class_name);

        switch($period)
        {
                case "1": $DayType = 2; break;
                case "2": $DayType = 3; break;
                default : $DayType = 2; break;
        }

        $sql = "INSERT INTO $card_student_daily_class_confirm
                                        (
                                                ClassID,
                                                ConfirmedUserID,
                                                DayNumber,
                                                DayType,
                                                DateInput,
                                                DateModified
                                        ) VALUES
                                        (
                                                '$class_id',
                                                '$confirmed_user_id',
                                                '$txt_day',
                                                '$DayType',
                                                NOW(),
                                                NOW()
                                        )
                                        ";
        $LIDB->db_db_query($sql);
        $msg = 1;
}


## UNLOCK TABLES
//$lcardattend->unlockTables();


if($period=="1")
        $display_period="AM";
else if($period=="2")
        $display_period="PM";

$return_url = "view_student_$display_period.php?class_name=$class_name&class_id=$class_id&period=$period&TargetDate=$TargetDate&msg=$msg";

header( "Location: $return_url");
?>
