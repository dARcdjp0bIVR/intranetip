<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdiscipline.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

if ($msg == 1)
{
    $response_msg = $i_Discipline_System_alert_RecordAddSuccessful;
}
else if ($msg == 2)
{
     $response_msg = $i_Discipline_System_alert_RecordWaitForApproval;
}
$warning_table = "";
if ( (isset($WarningStudentID) && is_array($WarningStudentID) && sizeof($WarningStudentID)!=0)
     || (isset($WarningStudentID_Subscore_1) && is_array($WarningStudentID_Subscore_1) && sizeof($WarningStudentID_Subscore_1)!=0)
     )
{
    intranet_opendb();
    $ldiscipline = new libdiscipline();

/*
        if($ldiscipline->allowAccessMerit() || $ldiscipline->allowAccessDemerit() || $ldiscipline->allowAccessApproval())
        {

        }
        else
        {
                echo "You have no priviledge to access this page.";
                exit();
        }
*/
		if($ldiscipline->isUseAccumulativeLateSetting($TargetDate)){
			$temp = $ldiscipline->returnAccumulativePeriodInfoByDate($TargetDate);
			list($period_id,$start_date,$end_date,$target_year,$target_semester) = $temp;
			$year = $target_year;
			$semester = $target_semester;
		}else{
		        if ($year == "")
		        {
		            $year = getCurrentAcademicYear();
		        }
		        if ($semester == "")
		        {
		            $semester = getCurrentSemester();
		        }
		}
        # Conduct Score checking
        $warning_table = "";
        if (sizeof($WarningStudentID))
        {
            # Get Student info
            $list = implode(",", $WarningStudentID);
            $namefield = getNameFieldWithClassNumberByLang("a.");
            $sql = "SELECT a.UserID, $namefield, b.ConductScore FROM INTRANET_USER as a
                           LEFT OUTER JOIN DISCIPLINE_STUDENT_CONDUCT_BALANCE as b
                                ON a.UserID = b.StudentID AND b.Year = '$year' AND b.Semester = '$semester'
                           WHERE a.UserID IN ($list)";
            $student_info = $ldiscipline->returnArray($sql,3);

            $warning_table .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src=\"$image_path/admin/table_head0.gif\" width=\"560\" height=\"13\" border=\"0\"></td></tr>
</table>
";
            $warning_table .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=\"center\">";
            $warning_table .= "<tr class=tableTitle><td>$i_UserStudentName</td><td>$i_Discipline_System_Conduct_DroppedPassWarningPoint</td><td>$i_Discipline_System_Conduct_UpdatedScore</td></tr>\n";

            for ($i=0; $i<sizeof($student_info); $i++)
            {
                 list($t_id, $t_name, $t_conduct_score) = $student_info[$i];
                 $css = ($i%2?"":"2");
                 $t_warning_level = $ldiscipline->getDroppedConductWarningLevel($t_conduct_score);
                 $warning_table .= "<tr class=tableContent$css>";
                 $warning_table .= "<td>$t_name</td><td>$t_warning_level</td><td>$t_conduct_score</td>";
                 $warning_table .= "</tr>\n";
                 $warning_table .= "<input type=hidden name=WarningStudentID[] value=\"$t_id\">\n";
            }

            $warning_table .= "</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=\"center\">
<tr><td><img src=\"$image_path/admin/table_bottom.gif\" width=\"560\" height=\"16\" border=\"0\"></td></tr>
</table>";
        }


        # Sub Score Type 1 checking
        $warning_table_subscore_1 = "";
        if (sizeof($WarningStudentID_Subscore_1))
        {
            # Get Student info
            $list = implode(",", $WarningStudentID_Subscore_1);
            $namefield = getNameFieldWithClassNumberByLang("a.");
            $sql = "SELECT a.UserID, $namefield, b.SubScore FROM INTRANET_USER as a
                           LEFT OUTER JOIN DISCIPLINE_STUDENT_SUBSCORE_BALANCE as b
                           ON a.UserID = b.StudentID AND b.Year = '$year' AND b.Semester = '$semester'
                           AND b.RecordType = 1
                    WHERE a.UserID IN ($list)";
            $student_info = $ldiscipline->returnArray($sql,3);

            $warning_table_subscore_1 .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src=\"$image_path/admin/table_head0.gif\" width=\"560\" height=\"13\" border=\"0\"></td></tr>
</table>
";
            $warning_table_subscore_1 .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=\"center\">";
            $warning_table_subscore_1 .= "<tr class=tableTitle><td>$i_UserStudentName</td><td>$i_Discipline_System_Conduct_DroppedPassWarningPoint</td><td>$i_Discipline_System_Subscore1_UpdatedScore</td></tr>\n";

            for ($i=0; $i<sizeof($student_info); $i++)
            {
                 list($t_id, $t_name, $t_sub_score) = $student_info[$i];
                 $css = ($i%2?"":"2");
                 $t_warning_level = $ldiscipline->getDroppedSubscoreWarningLevel(1,$t_sub_score);
                 $warning_table_subscore_1 .= "<tr class=tableContent$css>";
                 $warning_table_subscore_1 .= "<td>$t_name</td><td>$t_warning_level</td><td>$t_sub_score</td>";
                 $warning_table_subscore_1 .= "</tr>\n";
                 $warning_table_subscore_1 .= "<input type=hidden name=WarningStudentID_Subscore_1[] value=\"$t_id\">\n";
            }

            $warning_table_subscore_1 .= "</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=\"center\">
<tr><td><img src=\"$image_path/admin/table_bottom.gif\" width=\"560\" height=\"16\" border=\"0\"></td></tr>
</table>";
        }
     intranet_closedb();
}
#############################
#include_once("../../../../templates/fileheader.php");
include_once("../../../../templates/adminheader_setting.php");

?>
<SCRIPT LANGUAGE=Javascript>
function showNotice()
{
         checkPost(document.form1, '<?=$intranet_httppath?>/home/admin/discipline/notice/shownotice_conduct.php');
}
function showNotice_subscore_1()
{
         checkPost(document.form_subscore_1, '../notice/shownotice_subscore_1.php');
}

</SCRIPT>


<table width=100% border=0 cellspacing=0 cellpadding=0 height=100%>
<tr><td align=center valign=middle><font color=green><?=$response_msg?></font><br>

<?php
if (isset($WarningStudentID) && is_array($WarningStudentID) && sizeof($WarningStudentID)!=0)
{?>
        <form name=form1 action="" METHOD=post target=_blank><br><br><hr width=90% align=center><br><span class=discipline_submenu_title><u><?=$i_Discipline_System_Conduct_List_ConductScore_Dropped?></u></span><br>
        <?=$warning_table?>
        <hr width=50% align=center>
        <div align=center class=discipline_submenu_title><u><?=$i_Discipline_System_Conduct_ShowNotice?></u></div>
        <table width=400 align=center border=0 cellspacing=1 cellpadding=1>
          <tr>
                <td width=30%><?=$i_Discipline_System_Notice_AttachItem?> :</td><td><input type=checkbox name=AttachItem value=1></td>
          </tr>
          <tr>
                <td><?=$i_general_Format?>: </td><td><select name=format>
                  <option value=0>Web</option>
                  <option value=1>Word</option>
                                   </select>
                </td>
          </tr>
          <tr>
                <td>&nbsp;</td><td><a href=javascript:showNotice()><image src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a></td>
          </tr>
        </table>
        <hr width=50% align=center>
        <input type=hidden name=year value="<?=$year?>">
        <input type=hidden name=semester value="<?=$semester?>">

        </form>
<?php
}?>
<?php
/*
if (isset($WarningStudentID_Subscore_1) && is_array($WarningStudentID_Subscore_1) && sizeof($WarningStudentID_Subscore_1)!=0)
{?>
        <form name=form_subscore_1 action="" METHOD=post target=_blank><br><br><hr width=90% align=center><br><span class=discipline_submenu_title><u><?=$i_Discipline_System_Subscore1_List_SubScore_Dropped?></u></span><br>
        <?=$warning_table_subscore_1?>
        <hr width=50% align=center>
        <div align=center class=discipline_submenu_title><u><?=$i_Discipline_System_Conduct_ShowNotice?></u></div>
        <table width=400 align=center border=0 cellspacing=1 cellpadding=1>
          <tr>
                <td width=30%><?=$i_Discipline_System_Notice_AttachItem?> :</td><td><input type=checkbox name=AttachItem value=1></td>
          </tr>
          <tr>
                <td><?=$i_general_Format?>: </td><td><select name=format>
                  <option value=0>Web</option>
                  <option value=1>Word</option>
                                   </select>
                </td>
          </tr>
          <tr>
                <td>&nbsp;</td><td><a href=javascript:showNotice_subscore_1()><image src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a></td>
          </tr>
        </table>
        <hr width=50% align=center>
        <input type=hidden name=year value="<?=$year?>">
        <input type=hidden name=semester value="<?=$semester?>">
        </form>
<?php
}
*/ ?>
<a class=functionlink_new href="showlate.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>"><?=$button_continue?></a>
</td></tr>
</table>
<?


include_once("../../../../templates/adminfooter.php");

?>