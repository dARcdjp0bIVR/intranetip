<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$hasLunchTime = ($lcardattend->attendance_mode == 2);

?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewLeftStudents, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_StudentAttendance_LeftStatus_Type_InSchool,'summary.php?type=0',1,
                  $i_StudentAttendance_LeftStatus_Type_Lunch,'summary.php?type=1',$hasLunchTime,
                  $i_StudentAttendance_LeftStatus_Type_AfterSchool,'summary.php?type=2',1,
                  $i_StudentAttendance_LeftStatus_Type_LeavingTime,'summary.php?type=3',1
                                ) ?>
</blockquote>
</td></tr>
</table>

<?
include_once("../../../../templates/adminfooter.php");
?>