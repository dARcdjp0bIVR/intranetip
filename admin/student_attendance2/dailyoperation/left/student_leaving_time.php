<?php

if($targetClass == "")
{
	header("location: summary.php?type=3");
	exit();
}
	
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LIDB = new libdb();
$LICS = new libcardstudentattend2();
$LICS->retrieveSettings();
$lclass = new libclass();
$attendanceMode=$LICS->attendance_mode;

# Get class list
$select_class = $lclass->getSelectClass("name='targetClass'",$targetClass,1);

$sql="SELECT UserID FROM INTRANET_USER WHERE ClassName IN ('$targetClass') AND RecordType=2 AND RecordStatus IN (0,1,2)";
$temp = $LIDB->returnVector($sql);
if(sizeof($temp)>0)
$student_list = implode(",",$temp);

### Get Student Name List
$temp_name_list = $lclass -> getStudentNameListByClassName($targetClass);

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
//$LICS->createTable_LogAndConfirm($txt_year,$txt_month);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                $link_page = "AM";
                                                $confirm_page="AM";
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = 3;
                                                //if($attendanceMode==2||$attendanceMode==3){
                                                //        $confirm_page="PM_S";
                                                //}else{
                                                        $confirm_page="PM";
                                                //}
                                                       $link_page="PM";
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                $link_page = "AM";
                                                $confirm_page="AM";
                                                break;
}

### build student table
//$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;
$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

/*
$sql = "SELECT DISTINCT
				a.ClassID,
                a.ClassName,
                IF(b.ConfirmedUserID IS NULL,
                  	'-',
                   	IF(b.ConfirmedUserID <> -1, ".getNameFieldByLang("c.").", CONCAT('$i_general_sysadmin'))
                  ),
                IF(b.DateModified IS NULL, '-', b.DateModified)
		FROM 
				INTRANET_CLASS as a
				LEFT OUTER JOIN $card_student_daily_class_confirm as b
				ON (a.ClassID=b.ClassID AND
					(b.DayNumber = ".$txt_day." || b.DayNumber IS NULL) AND
					(b.DayType = $DayType || b.DayType IS NULL))
				LEFT OUTER JOIN INTRANET_USER as c ON (b.ConfirmedUserID=c.UserID || b.ConfirmedUserID=-1) WHERE a.RecordStatus=1
		ORDER BY 
				a.ClassName
		";
*/

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($OrderBy != '')
{
	if($OrderBy == '1')
	{
		$order_by = $default_order_by ;
	}
	else
	{
		$order_str= " ASC ";

		$order_by = "ISNULL(b.LeaveSchoolTime), b.LeaveSchoolTime $order_str, a.ClassName, a.ClassNumber, a.EnglishName";
	}
}
else
{
	$order_by = $default_order_by ;
}

/*		
if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " DESC ";

	$order_by = "b.LeaveSchoolTime $order_str";
}
*/

/*
# order info
$order_icon = "<img src='/images/schoolrecord/btn_set.gif' border=0 align=absmiddle>";
$order_by_time_link = "<a class=iconLink a href='javascript:orderByTime(1)' onMouseOver=\"window.status='$i_SmartCard_DailyOperation_OrderBy_InSchoolTime';return true;\" onMouseOut=\"window.status='';return true;\">".$order_icon.$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</a>";
$order_by_class_name_number_link = "<a class=iconLink href='javascript:orderByTime(0)' onMouseOver=\"window.status='$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber';return true;\" onMouseOut=\"window.status='';return true;\">".$order_icon.$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</a>";

$order_link = $order_by_time==1?$order_by_class_name_number_link:$order_by_time_link;

$order_toolbar.="<br><table width=560 border=0 cellpadding=1 cellspacing=1 align=center>";
$order_toolbar.="<tr>";
$order_toolbar.="<td align=right>".$order_link."&nbsp;</td>";
$order_toolbar.="</tr>";
$order_toolbar.="</table>";
*/

$select_order_by = "$i_LinkSortBy :</td>";
$select_order_by .= "<td><SELECT name='OrderBy'>";
if($OrderBy != '')
{
	if($OrderBy == '1')
	{
		$select_order_by .= "<OPTION value='1' SELECTED>".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
		$select_order_by .= "<OPTION value='2'>".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
	}
	if($OrderBy == '2')
	{
		$select_order_by .= "<OPTION value='1'>".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
		$select_order_by .= "<OPTION value='2' SELECTED>".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
	}
}
else
{
	$select_order_by .= "<OPTION value='1'>".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
	$select_order_by .= "<OPTION value='2'>".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
}
$select_order_by .= "</SELECT>";

/*
$sql = "SELECT
				a.ClassNumber,
				b.LeaveSchoolTime,
				b.LeaveSchoolStation,
				b.LeaveStatus
		FROM 
				INTRANET_USER AS a LEFT OUTER JOIN
				$card_student_daily_log AS b ON (a.UserID = b.UserID || a.UserID = '')
		WHERE
				b.DayNumber = $txt_day AND 
				a.UserID IN ($student_list)
		ORDER BY
				$order_by
		";
*/
		
$sql = "SELECT
				".getNameFieldByLang("a.").",
				a.ClassName,
				a.ClassNumber,
				b.LeaveSchoolTime,
				IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
				b.LeaveStatus
		FROM
				INTRANET_USER AS a LEFT OUTER JOIN
				$card_student_daily_log AS b ON (a.UserID = b.UserID)
		WHERE
				b.DayNumber = $txt_day AND
				a.ClassName = '$targetClass' AND
				a.RecordType = 2 AND
				a.RecordStatus IN (0,1,2)
		ORDER BY
				$order_by
		";
		
$result = $LIDB->returnArray($sql, 6);

$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td align=center>$i_UserName</td>";
$x .= "<td>$i_ClassName</td>";
$x .= "<td>$i_UserClassNumber</td>";
$x .= "<td align=center>$i_StudentAttendance_Time_Departure</td>";
$x .= "<td align=center>$i_StudentAttendance_Field_CardStation</td>";
$x .= "<td align=center>$i_StudentAttendance_Status</td></tr>\n";

/*
for($i=0; $i<sizeof($result); $i++)
{
	list($ClassNumber, $leave_school_time, $leave_school_station, $leave_status) = $result[$i];
	$LeaveSchool[$targetClass][$ClassNumber]['LeaveSchoolTime'] = $leave_school_time;
	$LeaveSchool[$targetClass][$ClassNumber]['LeaveSchoolStation'] = $leave_school_station;
	$LeaveSchool[$targetClass][$ClassNumber]['LeaveStatus'] = $leave_status;
}

for($i=0; $i<sizeof($temp_name_list); $i++)
{
	$css = ($i%2==0? "":"2");
	list($user_id, $student_name, $class_number) = $temp_name_list[$i];
	
	if($LeaveSchool[$targetClass][$class_number]['LeaveSchoolTime'] == "")
	{
		$str_leave_school_time = "-";
	}
	else
	{
		$str_leave_school_time = $LeaveSchool[$targetClass][$class_number]['LeaveSchoolTime'];
	}
	
	if($LeaveSchool[$targetClass][$class_number]['LeaveSchoolStation'] == "")
	{
		$leave_school_station = "-";
	}
	else
	{
		$leave_school_station = $leave_school_station;
	}
	
	if($LeaveSchool[$targetClass][$class_number]['LeaveStatus'] == "")
	{
		$str_leave_status = "-";
	}
	else
	{
		switch ($LeaveSchool[$targetClass][$class_number]['LeaveStatus'])
	    {
		    case 0:
		    		$str_leave_status = $i_StudentAttendance_Leave_Status_Normal;
		    		break;
		    case 1:
		    		$str_leave_status = $i_StudentAttendance_Leave_Status_Early_Leave_AM;
		    		break;
		    case 2:
		    		$str_leave_status = $i_StudentAttendance_Leave_Status_Early_Leave_PM;
		    		break;
		    default:
		    		break;
	    }
	}

	$x .= "<tr class=tableContent$css><td>$student_name</td>";
	$x .= "<td>$targetClass</td>";
    $x .= "<td>$class_number</td>";
    $x .= "<td>-</td>";
    $x .= "<td align=center>$str_leave_school_time</td>";
    $x .= "<td align=center>$str_leave_status</td></tr>";
}

if(sizeof($temp_name_list)<=0)
{
	$x .= "<tr><td>$i_no_record_searched_msg</td></tr>";
}
*/

for($i=0; $i<sizeof($result); $i++)
{
	list($student_name, $class_name, $class_number, $leave_school_time, $leave_school_station, $leave_status) = $result[$i];
	
	$x .= "<tr class=tableContent$css><td>$student_name</td>";
	$x .= "<td>$class_name</td>";
    $x .= "<td>$class_number</td>";
    
    if($leave_school_time != "")
    {
	    $x .= "<td align=center>$leave_school_time</td>";
    }
    else
    {
	    $x .= "<td align=center>-</td>";
    }
    
    if($leave_school_station != "")
    {
	    $x .= "<td align=center>$leave_school_station</td>";
    }
    else
    {
	    $x .= "<td align=center>-</td>";
    }
    
    if($leave_status != "")
    {
	    switch($leave_status)
	    {
		    case 0:
		    		$x .= "<td align=center>$i_StudentAttendance_Leave_Status_Normal</td>";
		    		break;
		    case 1:
		    		$x .= "<td align=center>$i_StudentAttendance_Leave_Status_Early_Leave_AM</td>";
		    		break;
		    case 2:
		    		$x .= "<td align=center>$i_StudentAttendance_Leave_Status_Early_Leave_PM</td>";
		    		break;
		    default:
		    		break;
    	}
    }
    else
    {
	    $x .= "<td align=center>-</td>";
    }
}

if(sizeof($result)<=0)
{
	$x .= "<tr><td colspan = 6 align=center>$i_no_record_exists_msg</td></tr>";
}

$x .= "</table>\n";


#$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";


$data = Array(
				Array("1",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll),
                Array("2",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs),
                Array("3",$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate),
                Array("4",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate)
			);
$toolbar = "$i_SmartCard_DailyOperation_viewClassStatus_PrintOption:&nbsp;".getSelectByArray($data, " name=print_option", $print_option,0,1);
$toolbar .= "&nbsp;<a class=iconLink href=javascript:openPrintPage()><img src='/images/admin/button/s_btn_print_".$intranet_session_language.".gif' border=0 align=absmiddle></a>";


$absent_count="<a class=iconLink href=\"blind_confirmation_".$confirm_page.".php?TargetDate=$TargetDate&period=$period\"><img src=\"$image_path/admin/icon_revise.gif\" border=0 align=absmiddle> $i_SmartCard_DailyOperation_BlindConfirmation</a>";
?>

<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
	// Calendar callback. When a date is clicked on the calendar
	// this function is called so you can do as you want with it
	function calendarCallback(date, month, year)
	{
		if (String(month).length == 1) {
			month = '0' + month;
		}

		if (String(date).length == 1) {
			date = '0' + date;
		}
		dateValue =year + '-' + month + '-' + date;
		document.forms['form1'].TargetDate.value = dateValue;
	}

	function openPrintPage()
	{
		var option = document.form1.print_option.value;
		if(option=="")
		{
			alert("<?=$i_alert_pleaseselect?>");
		}
		else
		{
			newWindow("view_student_print.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>&option="+option,4);
		}
	}
	function orderByTime(v)
	{
		fObj = document.form1;
		if(fObj==null ) return;
		orderByTimeObj = fObj.order_by_time;
		if(orderByTimeObj==null) return;
		orderByTimeObj.value = v;
		fObj.action="";
		fObj.submit();
	}
// -->
</script>


<form name="form1" method="post" action="">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewLeftStudents, 'index.php', $i_StudentAttendance_LeftStatus_Type_LeavingTime,'summary.php?type=3', $targetClass,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=1 cellspacing=1 align="center">
<!--
<tr><td align=left><?="&nbsp;".$i_StudentAttendance_Field_Date.":&nbsp;".$TargetDate?></td><td align=right><input type=text name=TargetDate value='<?=$TargetDate?>' size=10><script language="JavaScript" type="text/javascript">

	<tr>
		<td colspan=3><?=$toolbar?></td>
	</tr>
	<tr>
		<td colspan=3><hr size=1 class="hr_sub_separator"></td>
	</tr>
		-->
	<tr>
		<td align=left><?=$i_StudentAttendance_Field_Date?> : </td><td><input type=text name=TargetDate value='<?=$TargetDate?>' size=10>&nbsp;<script language="JavaScript" type="text/javascript">
		<!--
		startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
		-->
		</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span></td>
	</tr>
	<tr>
		<td align=left><?=$i_ClassName?> :</td>
		<td><?=$select_class?></td>
	</tr>
	<tr>
		<td><?=$select_order_by?></td>
		<td align=center><?=btnSubmit();?>&nbsp;<?=btnReset();?></td>
	</tr>
	<tr>
		<td colspan=3><hr size=1 class="hr_sub_separator"></td>
	</tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>

<tr><td class=admin_bg_menu></td></tr>
<tr><td class=tableContent align=center>
<?=$x?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<input name=order type=hidden value="<?=$order?>">
<input name=order_by_time type=hidden value="<?=$order_by_time?>">

</form>
<?																																																																																																	
include_once("../../../../templates/adminfooter.php");
?>

