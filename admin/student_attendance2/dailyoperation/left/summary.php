<?php
if($targetClass <> "" && $TargetDate <> "" && $OrderBy <> "")
{
	//echo $period . " " . $TargetDate;
	$URL = "?TargetDate=$TargetDate&targetClass=".$targetClass."&OrderBy=$OrderBy";
	header("Location: student_leaving_time.php".$URL);
	exit();
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LICS = new libcardstudentattend2();
$type += 0;
$summary = $LICS->getSummaryCount($type);
$display = "";

/*
$LICS->retrieveSettings();
$attendanceMode=$LICS->attendance_mode;
switch($attendanceMode){
		case 0 : $data = Array(Array("1",$i_DayTypeAM));break;
		case 1 : $data = Array(Array("2",$i_DayTypePM));break;
		default: $data = Array(Array("1",$i_DayTypeAM),Array("2",$i_DayTypePM));break;
}
$selection_period = getSelectByArray($data, " name=period ", $period, 0, 1);
*/
# Get class list
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name='targetClass'",$targetClass,1);

switch($type)
{
       case 1:
            $type_string = $i_StudentAttendance_LeftStatus_Type_Lunch;
            list($gone_out, $back, $not_out) = $summary;

            $link1 = ($gone_out != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=0',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link2 = ($back != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=1',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link3 = ($not_out != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=2',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");

            $display = "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_Lunch_GoneOut $link1</td><td style='vertical-align:middle' align=right>$gone_out</td></tr>\n";
            $display .= "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_Lunch_Back $link2</td><td style='vertical-align:middle' align=right>$back</td></tr>\n";
            $display .= "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_Lunch_NotOutYet $link3</td><td style='vertical-align:middle' align=right>$not_out</td></tr>\n";

            break;
       case 2:
            $type_string = $i_StudentAttendance_LeftStatus_Type_AfterSchool;
            list($left, $stay) = $summary;

            $link1 = ($left != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=0',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link2 = ($stay != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=1',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");

			$display = "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_AfterSchool_Left $link1</td><td style='vertical-align:middle' align=right>$left</td></tr>\n";
            $display .= "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_AfterSchool_Stay $link2</td><td style='vertical-align:middle' align=right>$stay</td></tr>\n";
            break;
       case 3:
       		$type_string = $i_StudentAttendance_LeftStatus_Type_LeavingTime;       		
       		$date_str = date('Y-m-d');
       		$display = "<tr><td align=right>$i_StudentAttendance_Field_Date : </td>";
       		$display .= "<td><input type=text name=TargetDate value='$date_str' size=10> <script language=\"JavaScript\" type=\"text/javascript\">";
         	$display .= "startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');";
    		$display .= "</script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>";
			$display .= "</td></tr>";
			$display .= "<tr><td align=right>$i_ClassName :</td><td>$select_class</td></tr>";
			$display .= "<tr><td align=right>$i_LinkSortBy :</td>";
			
			$display .= "<td><SELECT name='OrderBy'>";
			$display .= "<OPTION value='1'>".$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber."</option>";
			$display .= "<OPTION value='2'>".$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime."</option>";
			$display .= "</SELECT>";
			$display .= "</td></tr>";
			
			if ($OrderBy != '')
				echo $OrderBy;
				
			$display .= "<table width=500 border=0 cellpadding=0 cellspacing=0 align=\"center\">";
			$display .= "<tr><td><hr size=1></td></tr>";
			$display .= "<tr><td align='right'>";
			$display .= btnSubmit()." ".btnReset();
			$display .= "<a href=\"javascript:history.back()\"><img src='/images/admin/button/s_btn_cancel_".$intranet_session_language.".gif' border='0'></a>";
			$display .= "</td></tr>";
			$display .= "</table>";
       		break;
       default:
            $type_string = $i_StudentAttendance_LeftStatus_Type_InSchool;
            list($back, $not_yet) = $summary;

            $link1 = ($back != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=0',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");
            $link2 = ($not_yet != 0? "<a class=functionlink_new href=javascript:newWindow('list.php?type=$type&subtype=1',12)>[$i_StudentAttendance_ViewStudentList]</a>":"");

            $display = "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_InSchool_HaveBeenToSchool $link1</td><td style='vertical-align:middle' align=right>$back</td></tr>\n";
            $display .= "<tr><td height=30 style='vertical-align:middle' class=tableTitle_new>$i_StudentAttendance_InSchool_NotBackYet $link2</td><td style='vertical-align:middle' align=right>$not_yet</td></tr>\n";
            break;
}

?>

<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>

<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script type="text/javascript">
<!--
	// Calendar callback. When a date is clicked on the calendar
	// this function is called so you can do as you want with it
	function calendarCallback(date, month, year)
	{
		if (String(month).length == 1) {
			month = '0' + month;
		}

		if (String(date).length == 1) {
			date = '0' + date;
		}
		dateValue =year + '-' + month + '-' + date;
		document.forms['form1'].TargetDate.value = dateValue;
	}
// -->
</script>

<form name="form1" method="post" action="">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewLeftStudents, 'index.php',$type_string,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1 align=center>
<?=$display?>
</table>
</blockquote>

</form>
<?
include_once("../../../../templates/adminfooter.php");
?>