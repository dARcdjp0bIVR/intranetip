<?php
/**
 * For audit log
 * 
 * 
 * 
 * @author Fai
 * @version 0.1
 * @package 
 */
include_once("auditLogConfig.php");
if (!defined("LIBAUDITLOG_DEFINED"))                  // Preprocessor directives
{
	define("LIBAUDITLOG_DEFINED",true);
	
	class libauditlog extends libdb {

		var $recordId;
		var $userId;
		var $program;
		var $action;		
		var $audit_log_table_name;
		var $operationSQL;

		var $table;
		var $module;

		var $details;
		var $remark;

		var $modifyTime;	

		#Handle Error
		var $hasError = false;
		var $aryErrorMsg = array();

		# use config value
		var $aduitLogAction = array();	
		var $aduitLogProgram = array();
		

		function libauditlog()
		{
			global $cfg_aduitLogAction, $cfg_aduitLogProgram;  # use config value

			$this->libdb();
			$this->aduitLogAction = $cfg_aduitLogAction;
			$this->aduitLogProgram = $cfg_aduitLogProgram;
			$i_auditLogPrefix = "AUDIT_LOG_";
			$TargetDate= date('Y-m-d');
			$ts_record = strtotime($TargetDate);
			$txt_year = date('Y',$ts_record);
			$txt_month = date('m',$ts_record);
			### for student confirm
			$this->audit_log_table_name = $i_auditLogPrefix.$txt_year."_".$txt_month;

			$sql = "CREATE TABLE IF NOT EXISTS ".$this->audit_log_table_name."(
						recordID int(11) NOT NULL auto_increment,
						userid int(11) NOT NULL default '0',
						module varchar(255) default NULL,
						action varchar(50) default NULL,
						program text default NULL,
						actionTable varchar(50) default NULL,
						details text default NULL,
						remark text default NULL,						
						SQL text default NULL,
						dateModified TIMESTAMP(14) ,
						PRIMARY KEY (RecordID)
						) 
					";
			$this->db_db_query($sql);

			//UPDATE A TABLE TO STORE ALL THE audit LOG TABLE
			$sql = "INSERT IGNORE INTO AUDIT_LOG(logTable)values('".$this->audit_log_table_name."')";
			$this->db_db_query($sql);
		}

		function setRecordId($id)
		{
			$this->recordid=$id;
		}

		function setUserId($id)
		{
			$this->userId = $id;
		}

		function setProgram($name)
		{
			if($this->aduitLogProgram[$name] == "")
			{
				$this->hasError = true;
				array_push($this->aryErrorMsg,"Program [".$name."] does not exist in config file [auditLogConfig.php]");
			}
			else
			{
				$this->program = $name;
				$this->module= $this->aduitLogProgram[$name];
			}

		}
		function setSQL($_sql)
		{
			$this->operationSQL = intranet_htmlspecialchars($_sql);
		}
		function setAction($_action)
		{	
			$actionValue = strtolower($_action);

			if($this->aduitLogAction[$actionValue] == "")
			{
				$this->hasError = true;
				array_push($this->aryErrorMsg,"Action [".$_action."] does not exist in config file [auditLogConfig.php]");
			}
			else
			{
				$this->action = $this->aduitLogAction[$actionValue];
			}
		}
		function setRemark($_remark)
		{
			$this->remark = intranet_htmlspecialchars($_remark);
		}
		function setDetails($_details)
		{
			$this->details = intranet_htmlspecialchars($_details);
		}
		function  setTable($name)
		{
			$this->table= $name;
		}

		function setModule($name)
		{
			$this->module= $name;
		}

		function setModifyTime($_time)
		{
			$this->modifyTime = $_time;
		}

		function toString() //DEBUG
		{

			echo "<br>libauditlog.toString() --> recordId[<b>".$this->recordId."</b>] userId[<b>".$this->userId."</b>] program [<b>".$this->program."</b>] action [<b>".$this->action."</b>] remark [<b>".$this->remark."</b>] table [<b>".$this->table."</b>] module [<b>".$this->module."</b>] modifyTime [<b>".$this->modifyTime."</b>] sql [<b>".$this->operationSQL."</b>]<br><br>";
			if($this->hasError)
			{
				$this->printError();
			}
		}

		function recordLog()
		{	
			if($this->hasError)
			{
				$this->printError();
			}
			else
			{
				$sql = "insert into ".$this->audit_log_table_name." (userid,module,action,program,actionTable,details,remark,SQL) values(".$this->userId.",'".$this->module."','".$this->action."','".$this->program."','".$this->table."','".$this->details."','".$this->remark."','".$this->operationSQL."')";
				$this->db_db_query($sql);
			}
		}
		function resetError()
		{
			$this->hasError = false;
			$this->aryErrorMsg = array(); 
		}
		function printError()
		{
			$errorNo = 0;
			echo "<font color=\"#FF0000\">CANNOT WRITE AUDIT LOG</font><br>";
			echo "Reason :<br>";
			for($i =0 ; $i< sizeof($this->aryErrorMsg);$i++)
			{
				$errorNo = $i+1;
			echo $errorNo.") ".$this->aryErrorMsg[$i]."<br>";
			}
			echo "<hr size=\"-1\"/><br>";
		}
	} //close class libauditlog extends libdb 
}
?>
