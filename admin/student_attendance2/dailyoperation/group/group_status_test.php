<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

#class used
$LIDB = new libdb();
$LICS = new libcardstudentattend2();
$LICS->retrieveSettings();
$attendanceMode=$LICS->attendance_mode;
### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

### create table if needed
$LICS->createTable_LogAndConfirm($txt_year,$txt_month);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                $link_page = "AM";
                                                $confirm_page="AM";
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = 3;
                                                //if($attendanceMode==2||$attendanceMode==3){
                                                //        $confirm_page="PM_S";
                                                //}else{
                                                        $confirm_page="PM";
                                                //}
                                                       $link_page="PM";
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                $link_page = "AM";
                                                $confirm_page="AM";
                                                break;
}

### build student table
$card_student_daily_class_confirm = "CARD_STUDENT_DAILY_CLASS_CONFIRM_".$txt_year."_".$txt_month;

$sql = "SELECT DISTINCT
                                a.GROUPID,
                                a.Title,e.mode,
                                IF(b.ConfirmedUserID IS NULL,
                                                '-',
                                                IF(b.ConfirmedUserID <> -1, ".getNameFieldByLang("c.").", CONCAT('$i_general_sysadmin'))
                                        ),
                                IF(b.DateModified IS NULL, '-', b.DateModified)
                                        FROM INTRANET_GROUP as a 
												left outer join INTRANET_CLASS as d 
														on a.groupid = d.groupid
                                                LEFT OUTER JOIN $card_student_daily_class_confirm as b
                                                        ON (                a.groupid=b.ClassID AND
                                                                                (b.DayNumber = ".$txt_day." || b.DayNumber IS NULL) AND
                                                                                (b.DayType = $DayType || b.DayType IS NULL)        )
                                                LEFT OUTER JOIN INTRANET_USER as c 
														ON (b.ConfirmedUserID=c.UserID || b.ConfirmedUserID=-1) 
												LEFT OUTER JOIN CARD_STUDENT_ATTENDANCE_GROUP as e
														on (e.groupid = a.groupid)
												WHERE 
												a.RecordType = 3 
												AND d.groupid is null
                                                ORDER BY a.Title
                        ";

echo "sql [".$sql."]<br>";
$result = $LIDB->returnArray($sql, 5);

$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td align=center>$i_GroupName</td><td>$i_StudentAttendance_Field_ConfirmedBy</td><td>$i_StudentAttendance_Field_LastConfirmedTime</td></tr>\n";
for($i=0; $i<sizeOf($result); $i++)
{
        list($class_id, $class_name, $mode ,$confirmed_username, $confirmed_date) = $result[$i];
		if ($mode == 2) # mode equal to 2(no need to take attandace), don't show this record;
		{
			continue;
		}
        if($LICS->isRequiredToTakeAttendanceByDate($class_name,$TargetDate)){
	        $class_link = (!$sys_custom['QualiEd_StudentAttendance']) ? "<a class=functionlink_new href=\"view_student_$link_page.php?class_name=$class_name&class_id=$class_id&TargetDate=$TargetDate&period=$period\">$class_name</a>" : "<a class=functionlink_new href=\"view_student_{$link_page}_q.php?class_name=$class_name&class_id=$class_id&TargetDate=$TargetDate&period=$period\">$class_name</a>";
	        $x .= "<tr><td class=tableTitle align=center>$class_link</td><td>$confirmed_username</td><td>$confirmed_date</td></tr>\n";
        }else{
	        $class_link = "<a class=functionlink_new>$class_name</a>";
	        $x .="<tr><td class=tableTitle align=center>$class_link</td><td colspan=2>$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance</td>";
	    }
}
$x .= "</table>\n";


#$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";

$data = Array(
                                                        Array("1",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll),
                                                        Array("2",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs),
                                                        Array("3",$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate),
                                                        Array("4",$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate)

												);
//DISABLE THIS FUNCTION IN GROUP
//$toolbar = "$i_SmartCard_DailyOperation_viewClassStatus_PrintOption:&nbsp;".getSelectByArray($data, " name=print_option", $print_option,0,1);

/*$toolbar .= "&nbsp;<a class=iconLink href=javascript:openPrintPage()><img src='/images/admin/button/s_btn_print_".$intranet_session_language.".gif' border=0 align=absmiddle></a>"; */
$toolbar = "";
//$absent_count="<a class=iconLink href=\"blind_confirmation_".$confirm_page.".php?TargetDate=$TargetDate&period=$period\"><img src=\"$image_path/admin/icon_revise.gif\" border=0 align=absmiddle> $i_SmartCard_DailyOperation_BlindConfirmation</a>";
$absent_count = "";  #DISABLE THIS FUNCTION IN GROUP
?>

<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
        var option = document.form1.print_option.value;
        if(option=="")
        {
                alert("<?=$i_alert_pleaseselect?>");
        }
        else
        {
            newWindow("view_student_print.php?period=<?=$period?>&TargetDate=<?=$TargetDate?>&option="+option,4);
        }
}
-->
</script>

<form name="form1" method="post" action="">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewGroupStatus, 'index.php', $display_period,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?="&nbsp;".$i_StudentAttendance_Field_Date.":&nbsp;".$TargetDate?></td><td align=right></td></tr>
<!--<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>-->
<tr><td colspan=2><?=$toolbar?></td></tr>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu></td></tr>
<tr><td class=admin_bg_menu>&nbsp;<?=$absent_count?></td></tr>
<tr><td class=tableContent align=center>
<?=$x?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>
<?
include_once("../../../../templates/adminfooter.php");
?>
