<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

### Set Date from previous page

$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $ts_record = strtotime(date('Y-m-d'));
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('j',$ts_record);
$day_of_week = date('w',$ts_record);

# create confirm log table if not exists
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
$lcardattend->createTable_LogAndConfirm($txt_year, $txt_month);
$daily_log_table_name="CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;
$attendanceMode = $lcardattend->attendance_mode;

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = 3;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = 2;
                                                break;
}


# select classes with non school day on TargetDate from CARD_STUDENT_SPECIFIC_DATE_TIME
	$sqlSpecial = "SELECT ClassID,IF(NonSchoolDay=1,1,2) FROM CARD_STUDENT_SPECIFIC_DATE_TIME WHERE RecordDate='$TargetDate'";
	$temp = $lcardattend->returnArray($sqlSpecial,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultSpecial[$temp[$i][0]]=$temp[$i][1];
	}

# select classes with non school day on Target Cycle Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassCycle ="select a.ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME AS a, INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$temp=$lcardattend->returnArray($sqlClassCycle,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassCycle[$temp[$i][0]]=$temp[$i][1];
	}

# select classes with non school day on Target Cycle Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolCycle ="select IF(a.NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME AS a,INTRANET_CYCLE_DAYS AS b WHERE a.DayType=2 and a.DayValue=b.TextShort AND b.RecordDate='$TargetDate'";
	$resultSchoolCycle=$lcardattend->returnVector($sqlSchoolCycle);

# select classes with non school day on Target Week Day from CARD_STUDENT_PERIOD_TIME
  $sqlSchoolWeek ="select IF(NonSchoolDay=1,1,2) from CARD_STUDENT_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$resultSchoolWeek = $lcardattend->returnVector($sqlSchoolWeek);

# select classes with non school day on Target Week Day from CARD_STUDENT_CLASS_PERIOD_TIME
  $sqlClassWeek ="select ClassID,IF(NonSchoolDay=1,1,2) from CARD_STUDENT_CLASS_PERIOD_TIME WHERE DayType=1 and DayValue='$day_of_week'";
	$temp=$lcardattend->returnArray($sqlClassWeek,2);
	for($i=0;$i<sizeof($temp);$i++){
			$resultClassWeek[$temp[$i][0]]=$temp[$i][1];
	}
	
# select the timetable mode for each class
	$resultClassMode = $lcardattend->getClassListMode();
	$count=0;

	$off_class_list = array();

# determine the mode for each class 
	for($i=0;$i<sizeof($resultClassMode);$i++){
		$off=false;
		$done=false;
		$classID = $resultClassMode[$i][0];
		$className = $resultClassMode[$i][1];
		$classMode = $resultClassMode[$i][2];
		$specialClassID= $classMode==0?0:$classID;
		# check if NonSchoolDay for Speical Date
		if($resultSpecial[$specialClassID]==1){
				$off=true;
				$done=true;
		}else if($resultSpecial[$specialClassID]==2){
				$off=false;
				$done=true;
		}
		# check if NonSchoolDay for Cycle Day 
		if(!$done){
					if($classMode == 1){ # Class Cycle Day
							if($resultClassCycle[$classID]==1){
									$off = true;
									$done= true;
							}else if($resultClassCycle[$classID]==2){
									$off = false;
									$done= true;
							}
					}else if($classMode !=2){  # School Cycle Day
							if(is_array($resultSchoolCycle) &&$resultSchoolCycle[0]==1){
											$off = true;
											$done = true;
							} else if($resultSchoolCycle[0]==2){
											$off = false;
											$done = true;
							}
					} 
		}
		# check if NonSchoolDay for Week Day
		if(!$done){
				if($classMode==1){ # Class Week Day
							if($resultClassWeek[$classID]==1){
									$done = true;
									$off = true;
							}else if($resultClassWeek[$classID]==2){
									$done = true;
									$off = false;
							}


				}else if($classMode!=2){ # School Week Day
							if(is_array($resultSchoolWeek) && $resultSchoolWeek[0]==1){
											$off = true;
											$done=true;
							} else 	if($resultSchoolWeek[0]==2){
											$off = false;
											$done=true;
							} 

				}
		}
		if($classMode==2 || $off){
				$off_class_list[] = $classID;
		}
	}

# determining the pm status 
if($lcardattend->attendance_mode==2 || $lcardattend->attendance_mode==3){
    $pm_expected_field = "CASE
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NOT NULL) AND (d.LunchBackTime IS NULL)
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NULL THEN '".CARD_STATUS_ABSENT."'
                              WHEN (d.AMStatus IS NULL OR d.AMStatus = '".CARD_STATUS_ABSENT."') AND f.OutingID IS NOT NULL THEN '".CARD_STATUS_OUTING."'
                         END";			
}else{
	
	$pm_expected_field = "CASE
                              WHEN (d.AMStatus IN (".CARD_STATUS_PRESENT.",".CARD_STATUS_LATE.") AND d.LunchOutTime IS NULL) OR (d.LunchBackTime IS NOT NULL)
                                   THEN '".CARD_STATUS_PRESENT."'
                              WHEN d.AMStatus IS NULL AND f.OutingID IS NULL
                                   THEN '".CARD_STATUS_ABSENT."'
                              WHEN d.AMStatus IS NULL AND f.OutingID IS NOT NULL
                                   THEN '".CARD_STATUS_OUTING."'
                              ELSE '".CARD_STATUS_ABSENT."'
                          END";
}

  $sqlStudentList = "SELECT c.UserID,$pm_expected_field FROM 
  					INTRANET_USER AS c LEFT JOIN 
  					$daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),
  					INTRANET_CLASS AS e
					LEFT JOIN CARD_STUDENT_OUTING AS f ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate')
  					WHERE c.ClassName=e.ClassName AND ($pm_expected_field='".CARD_STATUS_ABSENT."' AND d.PMStatus IS NULL)";

  $sqlOutingList = "SELECT c.UserID,$pm_expected_field FROM 
  					INTRANET_USER AS c LEFT JOIN 
  					$daily_log_table_name AS d ON(c.UserID=d.UserID AND d.DayNumber='$txt_day'),
  					INTRANET_CLASS AS e
					LEFT JOIN CARD_STUDENT_OUTING AS f ON (c.UserID=f.UserID AND f.RecordDate = '$TargetDate')
  					WHERE c.ClassName=e.ClassName AND ($pm_expected_field='".CARD_STATUS_OUTING."' AND d.PMStatus IS NULL)";  					  					
  		
  if(sizeof($off_class_list)>0){
  		$sqlStudentList .=" AND e.ClassID NOT IN(".implode(",",$off_class_list).")";
  		$sqlOutingList.=" AND e.ClassID NOT IN(".implode(",",$off_class_list).")";
  }
  $sqlStudentList.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1,2) AND c.ClassName IS NOT NULL";
  $sqlOutingList.=" AND e.RecordStatus=1 AND c.RecordType=2 AND c.RecordStatus IN(0,1,2) AND c.ClassName IS NOT NULL AND f.RecordDate='$TargetDate'";

 $studentList = $lcardattend->returnVector($sqlStudentList,2);                         
 
 # count the no. of absent student and outing student
 if(sizeof($studentList)>0){
		$temp= $lcardattend->returnVector($sqlOutingList,2);
		$outingCount=sizeof($temp)+0;
		$absentCount = sizeof($studentList);
	}else{
		 $outingCount=0;
		 $absentCount=0;
	}

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewClassStatus, 'index.php', $display_period,'class_status.php?TargetDate='.$TargetDate.'&period='.$period,$i_SmartCard_DailyOperation_BlindConfirmation,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="GET" action="blind_confirmation_update_PM.php">
<blockquote>
<u><strong><?=$i_StudentPromotion_Summary?></strong></u>
<table width=<?=$table_width+300?> border=1 cellpadding=3 cellspacing=0 align=center >
<tr><td align=right nowrap><?php echo $i_StaffAttendance_IntermediateRecord_Date; ?>:</td><td><?=$TargetDate?></td></tr>
<tr><td align=right nowrap><?php echo $i_Attendance_DayType; ?>:</td><td><?=$display_period?></td></tr>
<tr><td align=right width=<?=$table_width?> nowrap><?php echo $i_SmartCard_DailyOperation_Number_Of_Absent_Student; ?>:</td><td width=100><?=$absentCount?></td></tr>
<tr><td align=right width=<?=$table_width?> nowrap><?php echo $i_SmartCard_DailyOperation_Number_Of_Outing_Student; ?>:</td><td width=100><?=$outingCount?></td></tr>

</table><br>
<?=$i_SmartCard_DailyOperation_BlindConfirmation_Notes?><br><Br>

</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
&nbsp;<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=TargetDate value="<?=$TargetDate?>">
<input type=hidden name=period value="<?=$period?>">

</form>
</form>
<?
include_once("../../../../templates/adminfooter.php");
?>