<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();


$return_url = $return_url==""?$HTTP_SERVER_VARS['HTTP_REFERER']:$return_url;

if(sizeof($RecordID)!=1){
	header("Location: $return_url");
}

if(is_array($RecordID))
	$RecordID=$RecordID[0];

$namefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT $namefield,a.RecordDate,a.DayPeriod,a.Reason,a.Remark FROM CARD_STUDENT_PRESET_LEAVE AS a LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE RecordID='$RecordID'";

$li = new libcardstudentattend2();

$temp = $li->returnArray($sql,5);
list($studentname,$recorddate,$datetype,$reason,$remark) = $temp[0];
if($datetype==2)
	$datetype = $i_DayTypeAM;
else if($datetype==3)
	$datetype = $i_DayTypePM;
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_Preset_Absence,'index.php',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>
function submitForm(){
	document.form1.submit();
}
</script>
<form name="form1" method="GET" action='edit_update.php'>

<br />

<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<tr><td align=right><?=$i_UserStudentName?>:&nbsp;</td><td><?=$studentname?><br>&nbsp;</td></tr>
<tr><td align=right><?=$i_Attendance_Date?>:&nbsp;</td><td><?=$recorddate?><br>&nbsp;</td></tr>
<tr><td align=right><?=$i_Attendance_DayType?>:&nbsp;</td><td><?=$datetype?><br>&nbsp;</td></tr>
<tr><td align=right><?=$i_Attendance_Reason?>:&nbsp;</td><td><textarea cols=60 rows=5 name='reason'><?=$reason?></textarea><br>&nbsp;</td></tr>
<tr><td align=right><?=$i_Attendance_Remark?>:&nbsp;</td><td><textarea cols=60 rows=5 name='remark'><?=$remark?></textarea><br>&nbsp;</td></tr>
</table>
<br><br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a href="javascript:submitForm()"><img src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a> 
<a href='<?=$return_url?>'><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>
<br />
<input type=hidden name=RecordID value='<?=$RecordID?>'>
<input type=hidden name=return_url value='<?=$return_url?>'>
</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>