<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();
$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;


if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename)) {
                # read file into array
                # return 0 if fail, return csv array if success
                //$data = $lf->file_read_csv($filepath);
                $data = $limport->GET_IMPORT_TXT($filepath);
                if ($format != 3)
                {
                    array_shift($data);                   # drop the title bar
                }
        }

        $sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
        $li->db_db_query($sql);
        $sql = "CREATE TABLE TEMP_CARD_STUDENT_LOG (
                 RecordDate date,
                 RecordedTime datetime,
                 UserID int,
                 CardID varchar(255),
                 ClassName varchar(100),
                 ClassNumber varchar(100),
                 SiteName varchar(255)
                )";
        $li->db_db_query($sql);

        # Get Student and Time table data
        if ($format == 2)
        {
            # ClassName, ClassNumber -> UserID
            $sql = "SELECT UserID, ClassName, ClassNumber FROM INTRANET_USER
                           WHERE RecordType = 2 AND RecordStatus IN (0,1)
                                 AND ClassName != '' AND ClassNumber != ''";
            $temp = $li->returnArray($sql,3);
            for ($i=0; $i<sizeof($temp); $i++)
            {
                 list($targetUserID, $targetClass, $targetNum) = $temp[$i];
                 $students[$targetClass][$targetNum] = $targetUserID;
            }
        }
        else if ($format == 3) # Reader 950e
        {
             # UserID -> ClassName
             /*
             $sql = "SELECT UserID, ClassName FROM INTRANET_USER
                            WHERE RecordType = 2 AND RecordStatus IN (0,1)
                                  AND ClassName != '' AND ClassNumber != ''";
             $temp = $li->returnArray($sql,2);
             $students = build_assoc_array($temp);
             */
        }
        else
        {
            # CardID -> ClassName
            # CardID -> UserID
            $sql = "SELECT CardID, UserID, ClassName FROM INTRANET_USER
                           WHERE RecordType = 2 AND RecordStatus IN (0,1)
                                 AND ClassName != '' AND ClassNumber != ''
                                 AND CardID != ''";
            $temp = $li->returnArray($sql,3);
            for ($i=0; $i<sizeof($temp); $i++)
            {
                 list($targetCardID, $targetUserID, $targetClass) = $temp[$i];
                 $students[$targetCardID] = $targetUserID;
                 $classes[$targetCardID] = $targetClass;
            }
        }

        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             if ($format == 2)
             {
                 list($time,$site,$class,$classnum) = $data[$i];
                 $ts = strtotime($time);
                 $date = date('Y-m-d',$ts);
                 $targetUserID = $students[$class][$classnum];
                 if ($targetUserID != '')
                 {
                     $values .= "$delim('$date','$time','$targetUserID','$site','$class','$classnum')";
                     $delim = ",";
                 }
             }
             else if ($format == 3) # Reader 950e
             {
                  // for offline reader format
                  // format:   time,site,student id
                  // e.g:      2004-11-03 11:26:55,Office,21013
                  if (is_array($data[$i]))
                  {
                      list($time,$site,$targetUserID) = $data[$i];
                      if ($time!="" && $targetUserID != "")
                      {
                          $ts = strtotime($time);
                          $date = date('Y-m-d',$ts);
                          $values .= "$delim('$date','$time','$site','$targetUserID')";
                          $delim = ",";
                      }
                  }
             }
             else
             {
                 list($time,$site,$cardid) = $data[$i];
                 $ts = strtotime($time);
                 $date = date('Y-m-d',$ts);
                 $targetUserID = $students[$cardid];
                 if ($targetUserID != '')
                 {
                     $values .= "$delim('$date','$time','$site','$cardid','$targetUserID')";
                     $delim = ",";
                 }
             }
        }

        if ($format==2)
        {
            $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordDate, RecordedTime,UserID,SiteName,ClassName,ClassNumber) VALUES $values";
        }
         else if ($format==3)
                {
            $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordDate,RecordedTime,SiteName,UserID) VALUES $values";
                }
        else
        {
            $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordDate,RecordedTime,SiteName,CardID, UserID) VALUES $values";
        }
        $li->db_db_query($sql);

}

$name_field = getNameFieldByLang("b.");
$sql = "SELECT a.RecordDate, DATE_FORMAT(a.RecordedTime,'%H:%i:%s'), a.SiteName, IF(a.CardID IS NULL, b.CardID, a.CardID), $name_field, b.ClassName, b.ClassNumber
        FROM TEMP_CARD_STUDENT_LOG as a
             LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND
                  a.UserID = b.UserID
                  ";
$result = $li->returnArray($sql,7);

$display = "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>\n";
$display .= "<tr><td><img src='$image_path/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>";
#$display .= "<tr><td class=admin_bg_menu>$toolbar</td></tr>";
$display .= "<tr><td>";

$display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class=admin_bg_menu><td>$i_SmartCard_DateRecorded</td><td>$i_SmartCard_TimeRecorded</td><td>$i_SmartCard_Site</td><td>$i_SmartCard_CardID</td><td>$i_UserStudentName</td><td>$i_UserClassName</td><td>$i_UserClassNumber</td></tr>";

for ($i=0; $i<sizeof($result); $i++)
{
     list ($date,$time,$site,$cardid,$name,$class,$classnum) = $result[$i];
     $display .= "<tr><td>$date</td><td>$time</td><td>$site</td><td>$cardid</td><td>$name</td><td>$class</td><td>$classnum</td></tr>\n";
}
$display .= "</table>\n";
$display .= "</td></tr></table>\n";

$display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
$display .= "<tr><td><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr>";
$display .= "</table><br>\n";

$sql = "SELECT DISTINCT RecordDate FROM TEMP_CARD_STUDENT_LOG";
$temp = $li->returnVector($sql);
$oneDayOnly = (sizeof($temp)==1);

include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ImportOfflineRecords, 'index.php',$button_import,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="POST" action="import_update_confirm.php">
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr>
<td>
<? if ($oneDayOnly) { ?>
<?=$i_StudentAttendance_ImportConfirm?><br>
<?=$i_StudentAttendance_ImportCancel?><br>
<input type=hidden name=RecordDate value="<?=$temp[0]?>">
<? }
else
{
?>
<?=$i_StudentAttendance_Import_Warning_OneDayOnly?>
<?
}
?>

</td></tr>
<tr><td align="right">
<a href=javascript:history.back()><img src="<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border=0></a>
<? if ($oneDayOnly) { ?>
<input type=image src="<?=$image_path?>/admin/button/s_btn_import_<?=$intranet_session_language?>.gif">
<? } ?>
</td>
</tr>
</table>
<input type=hidden name=datatype value="<?=$datatype?>">
</form>
<?

intranet_closedb();

include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>