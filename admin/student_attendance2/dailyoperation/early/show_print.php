<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

# order information
$default_order_by = " a.ClassName, a.ClassNumber, a.EnglishName";

if($order_by_time!=1){
	$order_by = $default_order_by ;
}
else{
	#$order_str= $order== 1?" DESC ":" ASC ";
	$order_str= " ASC ";

	$order_by = "b.LeaveSchoolTime $order_str";
	
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Early Leave
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LeaveSchoolTime,
                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
                c.Reason,
                c.RecordStatus
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_EARLY."'
                WHERE b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_AM."'
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY $order_by
";
}
else     # Check PM Early Leave
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LeaveSchoolTime,
                IF(b.LeaveSchoolStation IS NULL, '-', b.LeaveSchoolStation),
                c.Reason,
                c.RecordStatus
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_EARLY."'
                WHERE b.DayNumber = '$txt_day' AND b.LeaveStatus = '".CARD_LEAVE_PM."'
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY $order_by
";

}

$result = $lcardattend->returnArray($sql,9);

$table_attend = "";
# <td class=tableTitle>#</td>
$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>#</td><td>$i_UserName</td>
<td>$i_ClassName</td>
<td>$i_UserClassNumber</td>
<td>$i_StudentAttendance_LeaveSchoolTime</td>
<td>$i_StudentAttendance_Field_CardStation</td>
<td>$i_SmartCard_Frontend_Take_Attendance_Status</td>
<td>$i_SmartCard_Frontend_Take_Attendance_Waived</td>
<td width=30%>$i_Attendance_Reason</td>
</tr>\n";

for($i=0; $i<sizeOf($result); $i++)
{
        list($record_id, $user_id, $name,$class_name,$class_number, $leave_time, $leave_station, $reason,$record_status) = $result[$i];
        $str_status = $i_StudentAttendance_Status_EarlyLeave;
        $str_reason = $reason;
        $waived_option = $record_status == 1? $i_general_yes:$i_general_no;
        $x .= "<tr class=$css><td >".($i+1)."</td><td >$name</td><td>$class_name</td><td >$class_number</td><td>$leave_time</td><td >$leave_station</td><td >$str_status</td><td>$waived_option</td><td>$str_reason&nbsp;</td></tr>\n";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=tableContent><td colspan=9 align=center>$i_StudentAttendance_NoEarlyStudents</td></tr>\n";
}
$x .= "</table>\n";
$table_attend = $x;

$i_title = "$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus $TargetDate ($display_period)";
include_once("../../../../templates/fileheader.php");
echo $i_title;
echo $table_attend;
include_once("../../../../templates/filefooter.php");
intranet_closedb();

?>