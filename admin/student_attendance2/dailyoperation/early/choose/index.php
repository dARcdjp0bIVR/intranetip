<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libdiscipline.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/fileheader.php");

intranet_opendb();


$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=targetClass onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
{
    //$select_students = $lclass->getStudentSelectByClass($targetClass,"size=25 multiple name=targetID[]");
    
    $ts_record = strtotime($TargetDate);
	if ($ts_record == -1)
	{
	    $TargetDate = date('Y-m-d');
	    $ts_record = strtotime($TargetDate);
	}
	$txt_year = date('Y',$ts_record);
	$txt_month = date('m',$ts_record);
	$txt_day = date('d',$ts_record);
	
	$card_student_daily_log = "CARD_STUDENT_DAILY_LOG_";
	$card_student_daily_log .= $txt_year."_";
	$card_student_daily_log .= $txt_month;
	
    switch ($period)
	{
        case "1": $InSchool = " AND b.AMStatus IN (0,2) ";
        		  $LeaveStatus = " ";
                                                break;
        case "2": $InSchool = " AND b.PMStatus IN (0,2) ";
        		  $LeaveStatus = " AND (b.LeaveStatus IS NULL or b.LeaveStatus =0) ";
                                                break;
        default : $InSchool = " AND b.AMStatus IN (0,2) ";
        		  $LeaveStatus = " ";
                                                break;
	}
	
	$name_field = getNameFieldWithClassNumberByLang("a.");    
	$sql = "SELECT 
					a.UserID, $name_field 
			FROM 
					INTRANET_USER AS a INNER JOIN 
					$card_student_daily_log AS b ON (a.UserID = b.UserID) 
			WHERE 
					b.DayNumber = $txt_day
					$InSchool
					$LeaveStatus
					AND a.ClassName = '$targetClass' AND a.RecordType = 2 
					AND a.RecordStatus IN (0,1,2)
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName
			";

	$list = $lclass->returnArray($sql,2);
	$select_students = getSelectByArray($list, "size=25 multiple name=targetID[]");
}
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.submitForm();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name=form1 action="index.php" method=post>
<table border=0 cellpadding=0 cellspacing=0 align=center width=100%>
<tr>
<td align=center>
<br>
<table border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 width=60%><tr><td bgcolor=#FFFFFF>


<table width=100% border=0 cellpadding=5 cellspacing=1>
<tr><td class=tableTitle><?php echo $i_StudentAttendance_Class_Select_Instruction; ?></td></tr>
<tr><td><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
<tr><td><?php echo $select_class; ?></td></tr>
<?php if($targetClass != "")
{ ?>
<tr><td width=100%><hr width=90%></td></tr>
<tr><td class=tableTitle><?php echo $i_StudentAttendance_Student_Select_Instruction; ?></td></tr>
<tr><td><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
<tr><td>
        <table width=100% cellpadding=0 cellspacing=0>
        <tr><td>
        <?php echo $select_students; ?></td>
        <td style="vertical-align:bottom">
        <input type=image src="<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" alt='<?=$button_add?>' onClick=checkOption(this.form.elements["targetID[]"]);AddOptions(this.form.elements["targetID[]"])>&nbsp;<input type=image src="<?=$image_path?>/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" onClick="SelectAll(this.form.elements['targetID[]']); return false;" alt="<?=$button_select_all?>"></td></tr>
        </table>
</td></tr>
<?php
} ?>
</table>
</td></tr></table>
<br>
<a href=javascript:self.close()><img src="<?=$image_path?>/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" alt="<?php echo $button_close; ?>" border=0></a>
</td>
</tr>
</table>
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php
include_once("../../../../../templates/filefooter.php");
intranet_closedb();
?>