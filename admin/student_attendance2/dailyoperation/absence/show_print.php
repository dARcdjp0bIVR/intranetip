<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

### class used
$lcardattend = new libcardstudentattend2();

### Set Date from previous page
$ts_record = strtotime($TargetDate);
if ($ts_record == -1)
{
    $TargetDate = date('Y-m-d');
    $ts_record = strtotime($TargetDate);
}
$txt_year = date('Y',$ts_record);
$txt_month = date('m',$ts_record);
$txt_day = date('d',$ts_record);

###period
switch ($period)
{
        case "1": $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
        case "2": $display_period = $i_DayTypePM;
                                                $DayType = PROFILE_DAY_TYPE_PM;
                                                break;
        default : $display_period = $i_DayTypeAM;
                                                $DayType = PROFILE_DAY_TYPE_AM;
                                                break;
}

### build card log table
$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$txt_year."_".$txt_month;

if ($DayType==PROFILE_DAY_TYPE_AM)               # Check AM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.InSchoolTime,
                IF(b.InSchoolStation IS NULL, '-', b.InSchoolStation),
                b.AMStatus,
                c.Reason,
                c.RecordStatus,
                d.Remark
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_ABSENT."'
                   LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate')
                WHERE b.DayNumber = '$txt_day'
                      AND (b.AMStatus = '".CARD_STATUS_ABSENT."' OR b.AMStatus = '".CARD_STATUS_OUTING."')
                      AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";
}
else     # Check PM Late
{
$sql  = "SELECT b.RecordID, a.UserID,
                ".getNameFieldByLang("a.")."as name,
                a.ClassName,
                a.ClassNumber,
                b.LunchBackTime,
                IF(b.LunchBackStation IS NULL, '-', b.LunchBackStation),
                b.PMStatus,
                c.Reason,
                c.RecordStatus,
                d.Remark
                FROM
                $card_log_table_name as b
                   LEFT OUTER JOIN INTRANET_USER as a ON b.UserID = a.UserID
                   LEFT OUTER JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
                        ON c.StudentID = a.UserID AND c.RecordDate = '$TargetDate' AND c.DayType = $DayType
                           AND c.RecordType = '".PROFILE_TYPE_ABSENT."'
                   LEFT OUTER JOIN CARD_STUDENT_DAILY_REMARK AS d ON (b.UserID = d.StudentID AND d.RecordDate='$TargetDate')
                WHERE b.DayNumber = '$txt_day'
                      AND (b.PMStatus = '".CARD_STATUS_ABSENT."' OR b.PMStatus = '".CARD_STATUS_OUTING."')
                AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
         ORDER BY a.ClassName, a.ClassNumber, a.EnglishName
";

}

$result = $lcardattend->returnArray($sql,11);

$table_attend = "";

$x = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr class=tableTitle><td>#</td><td>$i_UserName</td>
<td>$i_ClassName</td>
<td>$i_UserClassNumber</td>
<td>$i_SmartCard_Frontend_Take_Attendance_Status</td>
<td>$i_SmartCard_Frontend_Take_Attendance_Waived</td>
<td width=30%>$i_Attendance_Reason</td><td>".$i_StudentGuardian['MenuInfo']."</td>";

if($sys_custom['hku_medical_research'])
{
   $x .= "<td>$i_MedicalReasonTitle</td>";
   # Retrieve Stored Reason
   # $DayType
   $sql = "SELECT a.UserID, b.MedicalReasonType
                 FROM INTRANET_USER as a
                      LEFT OUTER JOIN SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON as b
                           ON b.RecordDate = '$TargetDate' AND b.DayType = '$DayType' AND a.UserID = b.StudentID
                 ";
   $temp = $lcardattend->returnArray($sql,2);
   $data_medical = build_assoc_array($temp);
   $col_count = 9;
}
else
{
    $col_count = 8;
}


$x .= "</tr>\n";

for($i=0; $i<sizeOf($result); $i++)
{
    list($record_id, $user_id, $name,$class_name,$class_number, $in_school_time, $in_school_station, $old_status, $reason,$record_status, $remark) = $result[$i];
    $str_status = ($old_status==CARD_STATUS_ABSENT?$i_StudentAttendance_Status_Absent:$i_StudentAttendance_Status_Outing);

    $str_reason = ($reason==""? $remark:$reason);
    $waived_option = $record_status == 1? $i_general_yes:$i_general_no;

    $x .= "<tr class=$css><td >".($i+1)."</td><td >$name</td><td>$class_name</td><td >$class_number&nbsp;</td>";
    $x .= "<td >$str_status</td><td>$waived_option</td><td>$str_reason&nbsp;</td><td>";
    $layer_content = "";

    // Get guardian information

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$sql = "SELECT
                                $main_content_field,
                                Relation,
                                Phone,
                                EmPhone,
                                IsMain
                FROM
                                $eclass_db.GUARDIAN_STUDENT
                WHERE
                                UserID = $user_id
                ORDER BY
                                IsMain DESC, Relation ASC
                ";
$temp = $lcardattend->returnArray($sql,4);



#$layer_content = "<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
#$layer_content .= "<tr class=tableTitle><td width=1>#</td><td width=40%>$i_UserName</td><td width=20%>$ec_iPortfolio[relation]</td><td width=20%>$i_StudentGuardian_Phone</td><td width=20%>$i_StudentGuardian_EMPhone</td></tr>";


if (sizeof($temp)==0)
    $layer_content .= "--";
#        $layer_content .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>";
else
{
        for($j=0; $j<sizeof($temp); $j++)
        {
                list($name, $relation, $phone, $em_phone) = $temp[$j];
                $no = $j+1;
                $layer_content .= "$name ($ec_guardian[$relation]) ($i_StudentGuardian_Phone) $phone ($i_StudentGuardian_EMPhone) $em_phone <br>\n";
                /*
                if ($i==0)
                {
#                        $layer_content .= "<tr><td><font color=red>$no</font></td><td><font color=red>$name</font></td><td><font color=red>$ec_guardian[$relation]</font></td><td><font color=red>$phone</font></td><td><font color=red>$em_phone</font></td></tr>";
                }
                else
                {
                        $layer_content .= "<tr><td>$no</td><td>$name</td><td>$ec_guardian[$relation]</td><td>$phone</td><td>$em_phone</td></tr>";
                }
                */
        }
}

#$layer_content .= "<tr><td colspan=5><font color=red>* - $i_StudentGuardian_MainContent</font></td></tr>";
#$layer_content .= "</table>";

    //

    $x .= $layer_content;



    $x .= "</td>";
    if($sys_custom['hku_medical_research'])
    {
       $x .= "<td>";
       for ($j=0; $j<sizeof($i_MedicalReasonName); $j++)
       {
          list($t_reasonType, $t_reasonName) = $i_MedicalReasonName[$j];
          $t_selected_value = $data_medical[$user_id];
          if ($t_selected_value==$t_reasonType) {
                  $x .= $t_reasonName;
                  break;
          }
       }
       $x .= "</td>";
    }
    $x .= "</tr>\n";
}
if (sizeof($result)==0)
{
    $x .= "<tr class=tableContent><td colspan='$col_count' align=center>$i_StudentAttendance_NoAbsentStudents</td></tr>\n";
}
$x .= "</table>\n";
$table_attend = $x;

$i_title = "$i_SmartCard_DailyOperation_ViewAbsenceStatus $TargetDate ($display_period)";
include_once("../../../../templates/fileheader.php");
echo $i_title;
echo $table_attend;
echo "* - $i_StudentGuardian_MainContent";
include_once("../../../../templates/filefooter.php");
intranet_closedb();

?>
