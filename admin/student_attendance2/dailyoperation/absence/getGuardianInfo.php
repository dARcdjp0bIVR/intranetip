<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

$lidb = new libdb();

intranet_opendb();

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$sql = "SELECT 
				$main_content_field,
				Relation,
				Phone, 
				EmPhone,
				IsMain
		FROM
				$eclass_db.GUARDIAN_STUDENT
		WHERE 
				UserID = $UserID
		ORDER BY 
				IsMain DESC, Relation ASC
		";

$temp = $lidb->returnArray($sql,4);

$layer_content = "<table border=1 width=300 bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
$layer_content .= "<tr class=admin_bg_menu><td colspan=5 align=right><input type=button value=' X ' onClick=hideMenu2('ToolMenu2')></td></tr>";
$layer_content .= "<tr class=tableTitle><td width=1>#</td><td width=40%>$i_UserName</td><td width=20%>$ec_iPortfolio[relation]</td><td width=20%>$i_StudentGuardian_Phone</td><td width=20%>$i_StudentGuardian_EMPhone</td></tr>";


if (sizeof($temp)==0)
	$layer_content .= "<tr><td colspan=5 align=center>$i_no_record_exists_msg</td></tr>";
else
{
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($name, $relation, $phone, $em_phone) = $temp[$i];
		$no = $i+1;
		if($i==0)
			$layer_content .= "<tr><td><font color=red>$no</font></td><td><font color=red>$name</font></td><td><font color=red>$ec_guardian[$relation]</font></td><td><font color=red>$phone</font></td><td><font color=red>$em_phone</font></td></tr>";
		else
			$layer_content .= "<tr><td>$no</td><td>$name</td><td>$ec_guardian[$relation]</td><td>$phone</td><td>$em_phone</td></tr>";
	}
}

$layer_content .= "<tr><td colspan=5><font color=red>* - $i_StudentGuardian_MainContent</font></td></tr>";
$layer_content .= "</table>";


$response = iconv("Big5","UTF-8",$layer_content);

echo $response;
intranet_closedb();
?>