<?php
# 20061113 (Kenneth): Add confirmation indicator

if($period<>"" && $TargetDate <> "" && $flag==1)
{
        $URL = "?TargetDate=$TargetDate&period=".$period;
        header("Location: show.php".$URL);
        exit();
}

include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libcardstudentattend2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();
$lcardattend = new libcardstudentattend2();
$lcardattend->retrieveSettings();
if ($TargetDate == "")
{
    $TargetDate = date('Y-m-d');
}
if ($lcardattend->attendance_mode != 1)
{
    $data[] = array("1",$i_DayTypeAM);
}
if ($lcardattend->attendance_mode != 0)
{
    $data[] = array("2",$i_DayTypePM);
}
if ($lcardattend->attendance_mode == 0)
{
    $period = 1;
}
else if ($lcardattend->attendance_mode == 1)
{
     $period = 2;
}
else if ($period != 2)
{
     $period = 1;
}
$selection_period = getSelectByArray($data, " name=period onChange=\"this.form.flag.value=0;this.form.submit();\"", $period,0,1);
# Count 3 months
if ($period == 1)
{
    $field_status = "AMStatus";
    $confirm_conds = " AND RecordType = ".PROFILE_DAY_TYPE_AM;
}
else
{
    $field_status = "PMStatus";
    $confirm_conds = " AND RecordType = ".PROFILE_DAY_TYPE_PM;
}
# Status of dates
$records_with_data = array();
$ts = time();
$iteration = 0;

while($iteration < 12)
{

      $year = date('Y',$ts);
      $month = date('m',$ts);
      $table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
      $sql = "SELECT DISTINCT DayNumber FROM $table_name
                     WHERE $field_status = '".CARD_STATUS_ABSENT."'
                     ORDER BY DayNumber";
      $temp = @$lcardattend->returnVector($sql);
      for($i=0; $i<sizeof($temp); $i++)
      {
          $day = $temp[$i];
          $day = ($day < 10? "0".$day : $day);
          $entry_date = $year.$month.$day; #$year."-".$month."-".$day;
          $records_with_data[] = $entry_date;
      }
      $ts = mktime(0,0,0,$month-1,1,$year);
      $iteration++;
}

# Get Confirmed date
$date_string = $year."-".$month."-01";
$sql = "SELECT DISTINCT(DATE_FORMAT(RecordDate,'%Y%m%d')) FROM CARD_STUDENT_DAILY_DATA_CONFIRM
               WHERE AbsenceConfirmed = 1 AND RecordDate >= '$date_string'
               $confirm_conds ORDER BY RecordDate";
$temp = $lcardattend->returnVector($sql);
for ($i=0; $i<sizeof($temp); $i++)
{
     $confirmed_dates[$temp[$i]] = 1;
}

$legend = "<table width=100% border=0><tr><td align=center class=dynCalendar_card_not_confirmed>$i_StudentAttendance_CalendarLegend_NotConfirmed</td></tr><tr><td align=center class=dynCalendar_card_confirmed>$i_StudentAttendance_CalendarLegend_Confirmed</td></tr>";


?>

 <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
 <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_card_no_data";
         css_array[1] = "dynCalendar_card_not_confirmed";
         css_array[2] = "dynCalendar_card_confirmed";
         var date_array = new Array;
         <?
         for ($i=0; $i<sizeof($records_with_data); $i++)
         {
              $date_string = $records_with_data[$i];
              $isConfirmed = $confirmed_dates[$date_string];
              ?>
              date_array[<?=$date_string?>] = <?=($isConfirmed?2:1)?>;
              <?
         }
         ?>
 </script>

 <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
 <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
 <script type="text/javascript">
 <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           document.forms['form1'].TargetDate.value = dateValue;
          }
 // -->
 </script>

<form name="form1" method="post" action="">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../',$i_StudentAttendance_Menu_DailyOperation,'../',$i_SmartCard_DailyOperation_ViewAbsenceStatus, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_StudentAttendance_Field_Date; ?>:</td><td>
<input type=text name=TargetDate value='<?=$TargetDate?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
         startCal.setLegend('<?=$legend?>');
         startCal.differentDisplay = true;
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
</td></tr>
<tr><td align=right><?php echo $i_StudentAttendance_Slot; ?>:</td><td><?=$selection_period?></td></tr>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="../"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=flag value=1>

</form>
<?
include_once("../../../../templates/adminfooter.php");
?>
