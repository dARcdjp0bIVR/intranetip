<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libdiscipline.php");
include_once("../../../includes/libsmsv2.php");

intranet_opendb();

# class used
$LIDB = new libdb();
$lsms = new libsmsv2();

if ($plugin['Discipline']){
        $ldiscipline = new libdiscipline();
}

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
$namefield2 		= $lsms->getSMSGuardianUserNameField("");

$sms_ary	= array();
$no_sms_ary = array();
$sms_info 	= "";
for($i=0; $i<sizeOf($user_id); $i++)
{
	$student_id	= $user_id[$i];
	$send_sms 	= (${"sms_".$student_id} == '') ? 0 : 1;
	
	if($send_sms)
	{
		$lu = new libuser($student_id);
		
		$sql = "
			SELECT 
					$main_content_field,
					Relation,
					Phone, 
					EmPhone,
					IsMain,
					$namefield2
			FROM
					$eclass_db.GUARDIAN_STUDENT
			WHERE 
					UserID = $student_id and 
					isSMS = 1
			ORDER BY 
					IsMain DESC, Relation ASC
		";
		$temp = $LIDB->returnArray($sql);
	
		// guardian info
		for($j=0;$j<sizeof($temp);$j++)
		{
			# phone / emergency Phone 
			$p  		= $lsms->isValidPhoneNumber($temp[$j][2]) ? $temp[$j][2] : "";
			$e  		= $lsms->isValidPhoneNumber($temp[$j][3]) ? $temp[$j][3] : "";
			$sms_phone 	= ($p!="") ? $p : $e;
			if(!$sms_phone)		
			{	
				$no_sms_ary[$i]['reason'] = $i_SMS_Error_NovalidPhone;
				$mobile 	= ($temp[$j][2]!="") ? $temp[$j][2] : $temp[$j][3];
				$guardianName	= "";
			}
			else
			{
				$no_sms_ary[$i]['reason'] = "";
				$mobile 	= ($p!="") ? $p : $e;
				$guardianName	= $temp[$j][5];
				break;
			}
		}
		
		//Check Guardian
		if(sizeof($temp)==0)	
		{
			$no_sms_ary[$i]['reason'] = $i_SMS_Error_No_Guardian;
			$mobile					= "";
		}
		
		if($no_sms_ary[$i]['reason'])	# in $no_sms_ary
			{
					$no_sms_ary[$i]['name'] 		= $lu->UserName();
					$no_sms_ary[$i]['classname'] 	= $lu->ClassName;
					$no_sms_ary[$i]['classnumber'] 	= $lu->ClassNumber;
					$no_sms_ary[$i]['mobile'] 		= $mobile;
			}
			else
			{
					$sms_ary[$i]['guardian'] 	= $temp[$j][0];
					$sms_ary[$i]['relation'] 	= $temp[$j][1];	
					$sms_ary[$i]['name'] 		= $lu->UserName();
					$sms_ary[$i]['classname'] 	= $lu->ClassName;
					$sms_ary[$i]['classnumber']	= $lu->ClassNumber;
					$sms_ary[$i]['mobile'] 		= $mobile;
					$sms_ary[$i]['guardianName']= $guardianName;
					$sms_info .= $student_id .",".$mobile.",".$guardianName.";";
			}
	}
}
############################################################
## no_sms_ary
############################################################
$invalid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
$invalid_table.= "<tr><td>$i_SMS_Warning_Cannot_Send_To_Users</td></tr>";
$invalid_table.= "</table>";
$table_content = "";
$x = "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>\n";
$x .= "<tr class=tableTitle>
<td width=25%>$i_UserName</td>
<td width=20%>$i_ClassName</td>
<td width=5%>$i_ClassNumber</td>
<td>$i_SMS_MobileNumber</td>
<td>$i_Attendance_Reason</td></tr>\n";

$no_sms_ary_no = 0;
foreach($no_sms_ary as $this_ary)
{
	//if(trim($this_ary['name'])=="")	continue;
	$x .= "<tr class=$css>";
	$x .= "<td>". $this_ary['name'] ."</td>";
	$x .= "<td>". $this_ary['classname'] ."</td>";
	$x .= "<td>". $this_ary['classnumber'] ."</td>";
	$x .= "<td>". $this_ary['mobile'] ."</td>";
	$x .= "<td>". $this_ary['reason'] ."</td>";
	$x .= "</tr>\n";
	$no_sms_ary_no++;
}
$x .= "</table>\n";
$table_content = $invalid_table.$x;

############################################################
## sms_ary
############################################################
$valid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
$valid_table.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
$table_content1 = "";
$x = "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>\n";
$x .= "<tr class=tableTitle>
<td width=25%>$i_UserName</td>
<td width=20%>$i_ClassName</td>
<td width=5%>$i_ClassNumber</td>
<td>$i_SMS_MobileNumber</td>
<td>$i_StudentGuardian_GuardianName</td>
</tr>\n";

$sms_ary_no = 0;
foreach($sms_ary as $this_ary)
{
	//if(trim($this_ary['mobile'])=="")	continue;
	$x .= "<tr class=$css>";
	$x .= "<td>". $this_ary['name'] ."</td>";
	$x .= "<td>". $this_ary['classname'] ."</td>";
	$x .= "<td>". $this_ary['classnumber'] ."</td>";
	$x .= "<td>". $this_ary['mobile'] ."</td>";
	$x .= "<td>". $this_ary['guardianName'] ."</td>";
	$x .= "</tr>\n";
	$sms_ary_no++;
}
$x .= "</table>\n";
$table_content1 = $valid_table.$x;

$back_button = "<a href=\"javascript:history.back();\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$send_button = "<a href=\"javascript:document.form1.submit();\"><img src='$image_path/admin/button/s_btn_send_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

$Content = $lsms->returnSystemMsg($TemplateCode);
?>


<form name="form1" method="post" action="confirm_send_sms_update.php" >
<?=stripslashes($this_page_title)?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<? if($sms_ary_no>0) {?>
	<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>
	<tr>
		<td><?=$i_SMS_MessageContent?> : </td>
		<td><TEXTAREA NAME=Content ROWS=6 COLS=50><?=$Content?></TEXTAREA></td>
	</tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
	<tr><td><hr size=1></td></tr>
	<tr><td align='right'><?=$send_button?>&nbsp;<?=$back_button?></td></tr></table>
<? } ?>

<? if($no_sms_ary_no!=0) {?>
<br />
<?=$table_content?>
<? } ?>

<? if($sms_ary_no!=0) {?>
<br />
<?=$table_content1?>
<? } ?>


<? if($sms_ary_no==0) {?>
<br />
	<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
	<tr><td><hr size=1></td></tr>
	<tr><td align='right'><?=$back_button?></td></tr></table>
<? } ?>


<input type="hidden" name="this_page" value="<?=$this_page?>">
<input type="hidden" name="sms_info" value="<?=$sms_info?>">
<input type="hidden" name="TargetDate" value="<?=$TargetDate?>">
<input type="hidden" name="period" value="<?=$period?>">
<input type="hidden" name="order_by_time" value="<?=$order_by_time?>">

</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
