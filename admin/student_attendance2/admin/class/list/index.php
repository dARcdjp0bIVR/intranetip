<?

if ($page_size_change == 1){
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libdbtable.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

# TABLE SQL
$keyword = trim($keyword);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 3;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        default: $field = 3; break;
}

$conds = "";

if($select_class<>""){
	$conds = "AND b.ClassName = \"$select_class\" ";
}

$sql  = "SELECT	
							b.UserLogin,
							b.EnglishName,
							b.ChineseName,
							b.ClassName,
							b.ClassNumber,
							CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>')
                FROM
                        CARD_STUDENT_HELPER_STUDENT AS a 
							LEFT OUTER JOIN INTRANET_USER  AS b ON (a.StudentID=b.UserID)
                WHERE
                        (b.UserLogin like '%$keyword%' OR
                                b.UserEmail like '%$keyword%' OR
                                b.EnglishName like '%$keyword%' OR
                                b.ChineseName like '%$keyword%' OR
                                b.ClassName like '%$keyword%'
                                )
                        $conds
                ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array(	"b.UserLogin","b.EnglishName","b.ChineseName","b.ClassName","b.ClassNumber");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_user;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = 2;

# TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=18% class=tableTitle>".$li->column(0, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_UserChineseName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle NOWRAP>".$li->column(3, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

# filter
$LIDB = new libdb();

$sql = "SELECT DISTINCT b.ClassName 
				FROM 
					CARD_STUDENT_HELPER_STUDENT as a 
					LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID=b.UserID)
					WHERE b.ClassName IS NOT NULL
				";
$class_list = $LIDB->returnVector($sql);

$x = "<SELECT name=select_class onChange=\"submit()\">\n";
$x .= "<OPTION value=\"\"> -- $i_status_all -- </OPTION>\n";
for($i=0; $i<sizeOf($class_list); $i++){
	$selected = ($class_list[$i]==$select_class)?"SELECTED":"";
	$x .= "<OPTION value=\"".$class_list[$i]."\" $selected>".$class_list[$i]."</OPTION>\n";
}
$x .= "<SELECT>\n";
$searchbar = $x . toolBarSpacer() ;

$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";

?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, 
'../../../',$i_StudentAttendance_Menu_ResponsibleAdmin,'../../', $i_SmartCard_Responsible_Admin_Class, '../', $i_SmartCard_Responsible_Admin_Settings_List, '') ?>

<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar2, $functionbar); ?></td></tr>
<tr><td><img src=../../../../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br><br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>



</form>
<?
include_once("../../../../../templates/adminfooter.php");
?>