<?

include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libcardstudentattend2.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

# class used
$LIDB = new libdb();

$sql = "SELECT
                                                b.ClassName,
                                                COUNT(c.StudentID) as AdminNumber
                FROM
                                                INTRANET_CLASS AS a
                                                        LEFT OUTER JOIN INTRANET_USER AS b ON (a.ClassName=b.ClassName)
                                                        LEFT OUTER JOIN CARD_STUDENT_HELPER_STUDENT AS c ON (c.StudentID=b.UserID)
                                                WHERE a.RecordStatus = 1 AND b.ClassName IS NOT NULL
                                                GROUP BY b.ClassName
                                                        ";

$result = $LIDB->returnArray($sql, 2);
$lcardattend = new libcardstudentattend2();
$temp = $lcardattend->getClassListNotTakeAttendance();
for ($i=0; $i<sizeof($temp); $i++)
{
     list($id,$name) = $temp[$i];
     if ($name!="")
     {
         $no_need_array[$name] = $id;
     }
}

$table_width = "300";

$x = "<table width=$table_width border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=30% class=tableTitle>$i_UserClassName</td>
<td width=70% class=tableTitle>$i_SmartCard_Settings_Admin_Number_Students</td>
</tr>
";

for($i=0; $i<sizeOf($result); $i++){
        list($class_name, $admin_number) = $result[$i];
        if ($no_need_array[$class_name]=="")
        {
            $link = "<a class=functionlink_new href=\"edit.php?class_name=$class_name\">$class_name</a>";
            $x .= "<tr><td class=tableContent>$link</td><td class=tableContent>$admin_number</td></tr>\n";
        }
        else
        {
            $x .= "<tr><td class=tableContent>$class_name</td><td class=tableContent>$i_StudentAttendance_NoNeedTakeAttendance</td></tr>\n";
        }
}

$x .= "</table>\n";


$toolbar  = "<a class=iconLink href=javascript:checkGet(document.form1,'new.php')>".newIcon()."$button_new</a>\n".toolBarSpacer();


?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../../../',$i_StudentAttendance_Menu_ResponsibleAdmin,'../../', $i_SmartCard_Responsible_Admin_Class, '../', $i_SmartCard_Responsible_Admin_Settings_Manage, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=<?=$table_width?> border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="<?=$table_width?>" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=<?=$table_width?> height=7 border=0></td></tr>
<tr><td class=tableContent align=center>
<?=$x?>
</td></tr>
</table>

<table width=<?=$table_width?> border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="<?=$table_width?>" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../../../templates/adminfooter.php");
?>