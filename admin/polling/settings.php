<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_adminmenu_im_polling, '') ?>
<?= displayTag("head_polling_$intranet_session_language.gif", $msg) ?>

<table width='560' border='0' cellpadding='0' cellspacing='0' align='center'>
<td class='tableContent' height='300' align='left'>
<blockquote>
<?php if (trim($warning_message) != "") { ?>
<div><?=$warning_message.'<br><br>'?></div>
<?php } ?>
<?php
	echo displayOption($i_ReportCard_System_Admin_User_Setting, 'admin_setting.php',1);
?>	

</blockquote>
</td></tr>
</table>

<?php
intranet_closedb();

include_once("../../templates/adminfooter.php");
?>