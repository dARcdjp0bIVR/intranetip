<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "DELETE FROM INTRANET_GROUPPOLLING WHERE PollingID IN (".implode(",", $PollingID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_POLLINGRESULT WHERE PollingID IN (".implode(",", $PollingID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_POLLING WHERE PollingID IN (".implode(",", $PollingID).")";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&msg=3");
?>
