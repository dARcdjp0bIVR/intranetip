<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();
?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.DateStart, "<?php echo $i_alert_pleasefillin.$i_PollingDateStart; ?>.")) return false;
     if(!check_date(obj.DateStart, "<?php echo $i_invalid_date; ?>.")) return false;
     if(!check_text(obj.DateEnd, "<?php echo $i_alert_pleasefillin.$i_PollingDateEnd; ?>.")) return false;
     if(!check_date(obj.DateEnd, "<?php echo $i_invalid_date; ?>.")) return false;
     if(!check_text(obj.Question, "<?php echo $i_alert_pleasefillin.$i_PollingQuestion; ?>.")) return false;
     if(!check_text(obj.AnswerA, "<?php echo $i_alert_pleasefillin.$i_PollingAnswerA; ?>")) return false;
     if(!check_text(obj.AnswerB, "<?php echo $i_alert_pleasefillin.$i_PollingAnswerB; ?>")) return false;
     return checkalldate(obj.DateStart.value, obj.DateEnd.value);
}
function checkalldate(t1, t2)
{
        today = new Date();
        smonth = today.getMonth()+1;
        sdate = today.getDate();
        if (smonth < 10)
        {
                smonth = "0"+smonth;
        }
        if (sdate < 10)
        {
                sdate = "0"+sdate;
        }
        stoday = today.getFullYear() + '-' + smonth + '-' + sdate;
        if (compareDate(t1, stoday) < 0) {alert("<?php echo $i_PollingWrontStart; ?>"); return false;}
        if (compareDate(t2, t1) < 0) {alert ("<?php echo $i_PollingWrongEnd; ?>"); return false;}

        return true;
}

function compareDate(s1,s2)
{
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_adminmenu_im_polling, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_polling_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_PollingDateStart; ?>:</td><td><input class=text type=text name=DateStart size=10 maxlength=10 value="<?php echo date("Y-m-d"); ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_PollingDateEnd; ?>:</td><td><input class=text type=text name=DateEnd size=10 maxlength=10 value="<?php echo date("Y-m-d", mktime(0,0,0,date("m"),date("d")+7,date("Y"))); ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_PollingDateRelease; ?>:</td><td><input class=text type=text name=DateRelease size=10 maxlength=10 value="<?php echo date("Y-m-d", mktime(0,0,0,date("m"),date("d")+7,date("Y"))); ?>"> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_PollingQuestion; ?>:</td><td><input class=text type=text name=Question size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerA; ?>:</td><td><input class=text type=text name=AnswerA size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerB; ?>:</td><td><input class=text type=text name=AnswerB size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerC; ?>:</td><td><input class=text type=text name=AnswerC size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerD; ?>:</td><td><input class=text type=text name=AnswerD size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerE; ?>:</td><td><input class=text type=text name=AnswerE size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerF; ?>:</td><td><input class=text type=text name=AnswerF size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerG; ?>:</td><td><input class=text type=text name=AnswerG size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerH; ?>:</td><td><input class=text type=text name=AnswerH size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerI; ?>:</td><td><input class=text type=text name=AnswerI size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingAnswerJ; ?>:</td><td><input class=text type=text name=AnswerJ size=40 maxlength=255></td></tr>
<tr><td align=right><?php echo $i_PollingReference; ?>:</td><td><input class=text type=text name=Reference size=40 maxlength=255 value="http://"></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>