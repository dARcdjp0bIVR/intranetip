<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     default: $field = 0; break;
}
$sql  = "SELECT
               DATE_FORMAT(DateStart, '%Y-%m-%d'),
               DATE_FORMAT(DateEnd, '%Y-%m-%d'),
               CONCAT('<a class=tableContentLink href=view.php?PollingID=', PollingID, '>', Question, '</a>'),
               DateModified,
               CONCAT('<input type=checkbox name=PollingID[] value=', PollingID ,'>')
          FROM
               INTRANET_POLLING
          WHERE
               (Question like '%$keyword%')";


# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("DateStart", "DateEnd", "Question", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_polling;
$li->column_array = array(0,0,5,0);
$li->wrap_array = array(0,0,25,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_PollingDateStart)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_PollingDateEnd)."</td>\n";
$li->column_list .= "<td width=40% class=tableTitle>".$li->column(2, $i_PollingQuestion)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(3, $i_PollingDateModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("PollingID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'PollingID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'PollingID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_adminmenu_im_polling, '') ?>
<?= displayTag("head_polling_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>