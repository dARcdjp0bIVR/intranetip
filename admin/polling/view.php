<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libpolling.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libpolling($PollingID);
$now = time();
$today = mktime(0,0,0,date("m",$now),date("d",$now),date("Y",$now));
$start = strtotime($li->DateStart);
if ($start > $today)
    $edit_btn = "<input type='image' src='/images/admin/button/s_btn_edit_$intranet_session_language.gif' border='0'>";
else
    $edit_btn = "";
?>

<form action="edit.php" method="get" name="form1">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_adminmenu_im_polling, 'javascript:history.back()', $li->Question, '') ?>
<?= displayTag("head_polling_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=382 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src=../../images/frontpage/poll_t.gif border=0 width=382 height=11></td></tr>
<tr><td class=poll_m_bg align=center><u><?=$i_PollingDateRelease?>: <?=($li->DateRelease==""? $li->DateStart: $li->DateRelease)?></u>
<?php echo $li->displayResult(); ?></td></tr>
<tr><td><img src=../../images/frontpage/poll_b.gif border=0 width=382 height=11></td></tr>
</table>
</blockquote>
<input type=hidden name=PollingID[] value="<?php echo $li->PollingID; ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?=$edit_btn?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>