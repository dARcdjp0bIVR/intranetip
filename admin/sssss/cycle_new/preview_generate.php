<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycleperiods.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lc = new libcycleperiods();

# Clear Preview table
$lc->clearPreview();

# Get Periods
$periods = $lc->returnPeriods();

# Handle each period
for ($i=0; $i<sizeof($periods); $i++)
{
     list($PeriodID, $PeriodStart, $PeriodEnd, $PeriodType, $CycleType, $PeriodDays, $FirstDay, $SaturdayCounted) = $periods[$i];
     if ($PeriodDays < 1) continue;
     if ($PeriodType == 1)         # Calculation
     {
         if ($CycleType == 1)
         {
             $txtArray = $lc->array_alphabet;
         }
         else if ($CycleType == 2)
         {
              $txtArray = $lc->array_roman;
         }
         else
         {
             $txtArray = $lc->array_numeric;
         }

         # Get Non-Cycle Days
         $temp = $lc->getNonCycleDays($PeriodStart,$PeriodEnd);
         for ($j=0; $j<sizeof($temp); $j++)
         {
              $ts = strtotime($temp[$j]);
              $non_cycle_days[$ts] = "1";
         }
         $ts_start = strtotime($PeriodStart);
         $ts_end = strtotime($PeriodEnd);
         $ts_current = $ts_start;
         $current_weekday = date("w",$ts_current);
         $current_cycleday = $FirstDay;
         $values = "";
         $delim = "";
         while ($ts_current <= $ts_end)
         {
                #$recordDate = date("Y-m-d",$ts_current);

                # Check is non-cycle day
                if ($non_cycle_days[$ts_current]==1) # Skip
                {
                    #echo "$recordDate -> Non cycle<br>\n";
                    $current_weekday = ($current_weekday+1)%7;
                    $ts_current += 86400;  # 1 Day
                    continue;
                }
                # Check is weekend
                if ($current_weekday==0 || ($current_weekday==6 && $SaturdayCounted!=1))
                {
                    #echo "$recordDate -> Weekend<br>\n";
                    $ts_current += 86400;  # 1 Day
                    $current_weekday = ($current_weekday+1)%7;
                    continue;
                }
                # Put in Database
                $recordDate = date("Y-m-d",$ts_current);
                $txtCycle = $txtArray[$current_cycleday];
                $values .= "$delim ('$recordDate','$i_CycleNew_Prefix_Eng$txtCycle','$i_CycleNew_Prefix_Chi$txtCycle','$txtCycle')";
                $delim = ",";

                # Next iteration
                $current_weekday = ($current_weekday+1)%7;
                $current_cycleday = ($current_cycleday+1)%$PeriodDays;
                $ts_current += 86400;  # 1 Day
         }
         $sql = "INSERT INTO INTRANET_CYCLE_TEMP_DAYS_VIEW (RecordDate, TextEng, TextChi, TextShort)
                        VALUES $values";
         $lc->db_db_query($sql);
     }
     else if ($PeriodType == 2)      # File import
     {
          $sql = "INSERT IGNORE INTO INTRANET_CYCLE_TEMP_DAYS_VIEW (RecordDate,TextEng,TextChi,TextShort)
                         SELECT RecordDate, TextEng, TextChi, TextShort FROM INTRANET_CYCLE_IMPORT_RECORD
                                WHERE RecordDate >= '$PeriodStart' AND RecordDate <= '$PeriodEnd'";
          $lc->db_db_query($sql);
     }
     else
     {
         # Nothing to do
     }
}
intranet_closedb();
header("Location: index.php?step=2");
?>