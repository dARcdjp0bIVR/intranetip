<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");

intranet_opendb();
$li = new libdb();
$ts_start = strtotime($DateStart);
$ts_end = strtotime($DateEnd);
if ($ts_end < $ts_start)
{
    header("Location: edit.php?error=1&pid=$PeriodID");
    exit();
}
# Check date
$sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD
               WHERE ('$ts_start' BETWEEN UNIX_TIMESTAMP(PeriodStart) AND UNIX_TIMESTAMP(PeriodEnd)
                      OR '$ts_end' BETWEEN UNIX_TIMESTAMP(PeriodStart) AND UNIX_TIMESTAMP(PeriodEnd)
                     ) AND PeriodID != $PeriodID
                      ";
$temp = $li->returnVector($sql);
if ($temp[0]!="")
{
    header("Location: edit.php?error=2&pid=$PeriodID");
    exit();
}

if ($PeriodType==1)
{
    $fields = "PeriodStart = '$DateStart'";
    $fields .= ",PeriodEnd = '$DateEnd'";
    $fields .= ",CycleType = '$CycleType'";
    $fields .= ",PeriodDays = '$PeriodDays'";
    $fields .= ",FirstDay = '$FirstDay'";
    $fields .= ",SaturdayCounted = '$satCount'";
    $fields .= ",DateModified = now()";
    $sql = "UPDATE INTRANET_CYCLE_GENERATION_PERIOD SET $fields WHERE PeriodID = $PeriodID";
    $li->db_db_query($sql);
}
else if ($PeriodType==2)
{
     # Check file format
     $file_format = array("Date","Chi","Eng","Short");
     $lo = new libfilesystem();
     $filepath = $userfile;
     $filename = $userfile_name;

     if($filepath=="none" || $filepath == "")          # import failed
     {
        header("Location: edit.php?pid=$PeriodID");
        exit();
     }
     else
     {
         $ext = strtoupper($lo->file_ext($filename));
         if($ext == ".CSV")
         {
            # read file into array
            # return 0 if fail, return csv array if success
            $data = $lo->file_read_csv($filepath);
            $col_name = array_shift($data);                   # drop the title bar

            # Check file format
            $format_wrong = false;
            for ($i=0; $i<sizeof($file_format); $i++)
            {
                 if ($col_name[$i]!=$file_format[$i])
                 {
                     $format_wrong = true;
                     break;
                 }
            }
            if ($format_wrong)
            {
                include_once("../../../../lang/lang.$intranet_session_language.php");
                $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
                $display .= "<table width=100 border=1>\n";
                for ($i=0; $i<sizeof($file_format); $i++)
                {
                     $display .= "<tr><td>".$file_format[$i]."</td></tr>\n";
                }
                $display .= "</table></blockquote>\n";
            }
            else
            {
                # Handle File
                $sql = "DELETE FROM INTRANET_CYCLE_IMPORT_RECORD WHERE UNIX_TIMESTAMP(RecordDate) BETWEEN '$ts_start' AND '$ts_end'";
                $li->db_db_query($sql);
                $values = "";
                $delim = "";
                for ($i=0; $i<sizeof($data); $i++)
                {
                     list($recordDate, $chiTxt, $engTxt, $shortTxt) = $data[$i];
                     $recordDate = trim($recordDate);
                     if ($recordDate == "") break;
                     $chiTxt = addslashes($chiTxt);
                     $engTxt = addslashes($engTxt);
                     $shortTxt = addslashes($shortTxt);
                     $ts_record = strtotime($recordDate);
                     if ($ts_record < $ts_start)
                     {
                         $recordValid[$i] = false;
                         $reason[$i] = "$recordDate Earlier than Date Start";
                         continue;
                     }
                     if ($ts_record > $ts_end)
                     {
                         $recordValid[$i] = false;
                         $reason[$i] = "$recordDate Later than Date End";
                         continue;
                     }
                     $values .= "$delim ('$recordDate','$chiTxt','$engTxt','$shortTxt')";
                     $delim = ",";
                     $recordValid[$i] = true;
                }
                $sql = "INSERT IGNORE INTO INTRANET_CYCLE_IMPORT_RECORD (RecordDate, TextChi, TextEng, TextShort)
                               VALUES $values";
                $li->db_db_query($sql);

                $sql = "UPDATE INTRANET_CYCLE_GENERATION_PERIOD SET PeriodStart = '$DateStart',
                               PeriodEnd = '$DateEnd', DateModified = now() WHERE PeriodID = $PeriodID";
                $li->db_db_query($sql);

                # Mark Reason to display
                for ($i=0; $i<sizeof($recordValid); $i++)
                {
                     if (!$recordValid[$i])
                     {
                          $display .= "<tr><td>Line ".($i+1)."</td><td>".$reason[$i]."</td></tr>\n";
                     }
                }
                if ($display .= "")
                {
                    $display = "<table width=90% align=center border=1 cellspacing=0 cellpadding=0>\n$display\n</table>\n";
                }

            }
            if ($display != '')
            {
                include_once("../../../../lang/lang.$intranet_session_language.php");
                include_once("../../../../templates/adminheader_setting.php");
                echo displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, 'index.php', $button_import, '');
                echo displayTag("head_merit_$intranet_session_language.gif", $msg);
                $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
                echo $display;
                include_once("../../../../templates/adminfooter.php");
            }
         }
         else
         {
             header("Location: edit.php?pid=$PeriodID");
             exit();
         }
     }
}
else # PeriodType == 0
{
    $fields = "PeriodStart = '$DateStart'";
    $fields .= ",PeriodEnd = '$DateEnd'";
    $fields .= ",DateModified = now()";
    $sql = "UPDATE INTRANET_CYCLE_GENERATION_PERIOD SET $fields WHERE PeriodID = $PeriodID";
    $li->db_db_query($sql);
}

header("Location: index.php?msg=2");
intranet_closedb();
?>