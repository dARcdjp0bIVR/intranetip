<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libcycleperiods.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();
$lc = new libcycleperiods();
$toolbar = "<a class=iconLink href=\"new.php\">".newIcon()."$button_new</a>";
$functionbar = "<a href=javascript:removeAll()><img src=\"$image_path/admin/button/t_btn_delete_all_$intranet_session_language.gif\" border=0></a>";
?>
<SCRIPT LANGUAGE=Javascript>
function removeAll()
{
         if (confirm('<?=$i_CycleNew_Alert_RemoveAllConfirm?>'))
         {
             location.href = 'removeall.php';
         }
}
</SCRIPT>
<?= displayNavTitle($i_admintitle_sc, '',$i_adminmenu_sc_school_settings,'../../', $i_admintitle_sc_cycle, '../',$i_CycleNew_Menu_DefinePeriods,'') ?>
<?= displayTag("head_cycle_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><table width=100% border=0 cellspacing=0 cellpadding=0>
<tr><td align=left><?=$toolbar?></td><td align=right><?=$functionbar?></td></tr>
</table>
</td></tr>
<tr>
<td>
<?=$lc->displayPeriodsTableForEdit()?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../../../templates/adminfooter.php");
?>