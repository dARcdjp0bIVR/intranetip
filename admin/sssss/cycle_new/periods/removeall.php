<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libcycleperiods.php");

intranet_opendb();
$lc = new libcycleperiods();

$sql = "DELETE FROM INTRANET_CYCLE_IMPORT_RECORD";
$lc->db_db_query($sql);
$sql = "DELETE FROM INTRANET_CYCLE_GENERATION_PERIOD";
$lc->db_db_query($sql);


header("Location: index.php?msg=3");
intranet_closedb();
?>