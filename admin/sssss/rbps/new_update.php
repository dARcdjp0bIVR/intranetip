<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$BatchTitle = intranet_htmlspecialchars($BatchTitle);
$sql = "INSERT INTO INTRANET_SLOT_BATCH (Title, RecordStatus) values ('$BatchTitle', 0)";
$li->db_db_query($sql);
$count = $li->db_affected_rows();
if ($count > 0)
{
    $BatchID = $li->db_insert_id();
    $query_str = "?BatchID=$BatchID&msg=1";
}
else
{
    $query_str = "?fail=1";
}

header("Location: index.php$query_str");
intranet_closedb();
?>