<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libbatch.php");
intranet_opendb();

$li = new libbatch();

$schoolTimeTable = $li->BatchID;
if ($schoolTimeTable == $BatchID)
{
    # Cannot delete school timetable, need to set another first
    header ("Location: index.php?BatchID=$BatchID");
    exit();
}



# Archive booking records

    # Retrieve item list first
    $sql = "SELECT ResourceID FROM INTRANET_RESOURCE WHERE TimeSlotBatchID = $BatchID";
    $items = $li->returnVector($sql);
    $itemlist = implode(",",$items);

    # Archive
    $conds = "d.TimeSlotBatchID = $BatchID";
    $sql = "INSERT INTO INTRANET_BOOKING_ARCHIVE
            (BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
            SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
            CONCAT(b.EnglishName, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ),
            d.ResourceCategory, d.Title, d.ResourceCode,
            a.RecordStatus, a.TimeApplied, a.LastAction
            FROM INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d
            WHERE a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq
            AND a.BookingDate < CURDATE() AND $conds";
    $li->db_db_query($sql);

    # Remove Booking records
    $cond2 = "ResourceID IN ($itemlist)";
    $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE $cond2";
    $li->db_db_query($sql);


# Update the items using batch to be deleted to school timetable
$sql = "UPDATE INTRANET_RESOURCE SET TimeSlotBatchID = $schoolTimeTable WHERE TimeSlotBatchID = $BatchID";
$li->db_db_query($sql);

# Remove time scheme
$sql = "DELETE FROM INTRANET_SLOT_BATCH WHERE BatchID = $BatchID";
$li->db_db_query($sql);

# Remove time slots
$sql = "DELETE FROM INTRANET_SLOT WHERE BatchID = $BatchID";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=3");
?>