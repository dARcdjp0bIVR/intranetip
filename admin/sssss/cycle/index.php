<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycle.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$li = new libfilesystem();
/*
$cycleID = $li->file_read($intranet_root."/file/cycle.txt");
$cycleID += 0;
// bordercolordark="black" bordercolorlight="black"
$cycleTime = ($cycleID == 0) ? "" : date("Y-m-d", $li->file_get_modification_time($intranet_root."/file/cycle.txt"));
*/

$lc = new libcycle();
$cycleID = $lc->CycleID;
$cycleTime = date('Y-m-d',$lc->DateStart);

# Special Arrangement
$specialfile = $li->file_read("$intranet_root/file/specialDates.txt");
$specialDates = explode("\n",$specialfile);
$specialStart = $specialDates[0];
$specialEnd = $specialDates[1];

?>

<script language="javascript">
function checkform(obj){
     if(returnChecked(obj, "cycleID")!=0){
          if(!check_date(obj.cycleTime, "<?php echo $i_invalid_date; ?>.")) return false;
     }
}
</script>

<form name="form1" action="update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings,'/admin/school_settings/',$i_admintitle_sc_cycle, '') ?>
<?= displayTag("head_cycle_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<p><br>
<u><?=$i_CycleSelect?></u>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td style="vertical-align:middle"><?=$i_CycleWeek?></td><td><input type=radio name=cycleID value=0 <?php if($cycleID==0) echo "CHECKED"; ?>></td>
</tr>
<tr>
<td><?=$i_CycleCycle?></td><td>
<table width=260 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>
<tr>
<td class="tableTitle_new"><?=$i_CycleType?></td>
<td align=center class="tableTitle_new"><?=$i_Cycle5Day?></td>
<td align=center class="tableTitle_new"><?=$i_Cycle6Day?></td>
<td align=center class="tableTitle_new"><?=$i_Cycle7Day?></td>
<td align=center class="tableTitle_new"><?=$i_Cycle8Day?></td>
</tr>
<tr>
<td>1,2,3 ... </td>
<td align=center><input type=radio name=cycleID value=10 <?php if($cycleID==10) echo "CHECKED"; ?>></td>
<td align=center><input type=radio name=cycleID value=1 <?php if($cycleID==1) echo "CHECKED"; ?>></td>
<td align=center><input type=radio name=cycleID value=2 <?php if($cycleID==2) echo "CHECKED"; ?>></td>
<td align=center><input type=radio name=cycleID value=3 <?php if($cycleID==3) echo "CHECKED"; ?>></td>
</tr>
<tr >
    <td>A,B,C...</td>
    <td align=center><input type=radio name=cycleID value=11 <?php if($cycleID==11) echo "CHECKED"; ?>></td>
    <td align=center><input type=radio name=cycleID value=4 <?php if($cycleID==4) echo "CHECKED"; ?>></td>
    <td align=center><input type=radio name=cycleID value=5 <?php if($cycleID==5) echo "CHECKED"; ?>></td>
    <td align=center><input type=radio name=cycleID value=6 <?php if($cycleID==6) echo "CHECKED"; ?>></td>
</tr>
<tr >
    <td >I,II,III...</td>
    <td align=center><input type=radio name=cycleID value=12 <?php if($cycleID==12) echo "CHECKED"; ?>></td>
    <td align=center><input type=radio name=cycleID value=7 <?php if($cycleID==7) echo "CHECKED"; ?>></td>
    <td align=center><input type=radio name=cycleID value=8 <?php if($cycleID==8) echo "CHECKED"; ?>></td>
    <td align=center><input type=radio name=cycleID value=9 <?php if($cycleID==9) echo "CHECKED"; ?>></td>
</tr>
<tr >
    <td ><?php echo $i_CycleStart; ?></td>
    <td align=center colspan=4><input class=text type=text name=cycleTime size=10 maxlength=10 value="<?php echo $cycleTime; ?>"> <span class="extraInfo">(yyyy-mm-dd)</span></td>
</tr>
</table>
</td>
</tr>
<tr><td colspan="2"><p class="extraInfo"><?php echo $i_CycleMsg; ?></p></td></tr>
</table>

<hr size=1 class="hr_sub_separator">
<p>
<span class="extraInfo">[<span class=subTitle><?=$i_CycleSpecialArrangment?></span>]</span>
<br><?=$i_CycleSpecialForSatCounting?>:
<br><?=$i_CycleSpecialStart?>:
<input type=text name=specialstart value='<?=$specialStart?>' size=10> <span class="extraInfo">(yyyy-mm-dd)</span>
<br><?=$i_CycleSpecialEnd?>:
<input type=text name=specialend value='<?=$specialEnd?>' size=10> <span class="extraInfo">(yyyy-mm-dd)</span>
<p><b><?=$i_CycleSpecialNotes?><b></p>
</blockquote>

</td>
</tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>