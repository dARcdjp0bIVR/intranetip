<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$linterface = new interface_html();
$limport = new libimporttext();

$lclass = new libclass();
?>

<form name="form1" action="import_update.php" enctype="multipart/form-data" method="post">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, 'javascript:history.back()', $button_import." "."$i_ClassName / $i_ClassLevel", '') ?>
<?= displayTag("head_school_class_$intranet_session_language.gif", $msg) ?>

<blockquote>
<?= $i_select_file ?>: <input type=file name=classfile><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>

<blockquote>
<?php
	if($special_feature['websams_attendance_export']){
		echo $i_WebSAMS_Class_ImportInstruction;
		$sample_file = GET_CSV("sample_class_websams.csv");
	}else{
		echo $i_Class_ImportInstruction;
		$sample_file = GET_CSV("sample_class.csv");
	}
?>
</blockquote>
<p><a class=functionlink_new href="<?=$sample_file?>"><?=$i_Class_DownloadSample?></a></p>
</blockquote>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>