<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if (is_array($ClassLevelID)) $ClassLevelID = $ClassLevelID[0];

$li = new libdb();
$sql = "SELECT LevelName, RecordStatus,WebSAMSLevel FROM INTRANET_CLASSLEVEL WHERE ClassLevelID = $ClassLevelID";
$array = $li->returnArray($sql,3);
$LevelName = $array[0][0];
$RecordStatus = $array[0][1];
$WebSamsLevel = $array[0][2];

$select_websams_level = "<SELECT name='WebSamsLevel'>\n";
$select_websams_level.= "<OPTION VALUE=''> -- $button_select -- </OPTION>";
$select_websams_level.= "<OPTION VALUE='K1'".($WebSamsLevel=='K1'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_K1</OPTION>";
$select_websams_level.= "<OPTION VALUE='K2'".($WebSamsLevel=='K2'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_K2</OPTION>";
$select_websams_level.= "<OPTION VALUE='K3'".($WebSamsLevel=='K3'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_K3</OPTION>";
$select_websams_level.= "<OPTION VALUE='LP'".($WebSamsLevel=='LP'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_LP</OPTION>";
$select_websams_level.= "<OPTION VALUE='P1'".($WebSamsLevel=='P1'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_P1</OPTION>";
$select_websams_level.= "<OPTION VALUE='P2'".($WebSamsLevel=='P2'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_P2</OPTION>";
$select_websams_level.= "<OPTION VALUE='P3'".($WebSamsLevel=='P3'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_P3</OPTION>";
$select_websams_level.= "<OPTION VALUE='P4'".($WebSamsLevel=='P4'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_P4</OPTION>";
$select_websams_level.= "<OPTION VALUE='P5'".($WebSamsLevel=='P5'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_P5</OPTION>";
$select_websams_level.= "<OPTION VALUE='P6'".($WebSamsLevel=='P6'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_P6</OPTION>";
$select_websams_level.= "<OPTION VALUE='PP'".($WebSamsLevel=='PP'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_PP</OPTION>";
$select_websams_level.= "<OPTION VALUE='PR'".($WebSamsLevel=='PR'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_PR</OPTION>";
$select_websams_level.= "<OPTION VALUE='S1'".($WebSamsLevel=='S1'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S1</OPTION>";
$select_websams_level.= "<OPTION VALUE='S2'".($WebSamsLevel=='S2'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S2</OPTION>";
$select_websams_level.= "<OPTION VALUE='S3'".($WebSamsLevel=='S3'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S3</OPTION>";
$select_websams_level.= "<OPTION VALUE='S4'".($WebSamsLevel=='S4'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S4</OPTION>";
$select_websams_level.= "<OPTION VALUE='S5'".($WebSamsLevel=='S5'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S5</OPTION>";
$select_websams_level.= "<OPTION VALUE='S6'".($WebSamsLevel=='S6'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S6</OPTION>";
$select_websams_level.= "<OPTION VALUE='S7'".($WebSamsLevel=='S7'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_S7</OPTION>";
$select_websams_level.= "<OPTION VALUE='SJ'".($WebSamsLevel=='SJ'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_SJ</OPTION>";
$select_websams_level.= "<OPTION VALUE='SS'".($WebSamsLevel=='SS'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_SS</OPTION>";
$select_websams_level.= "<OPTION VALUE='UP'".($WebSamsLevel=='UP'?" SELECTED ":"").">$i_WebSAMS_AttendanceCode_Option_ClassLevel_UP</OPTION>";
$select_websams_level .= "</SELECT><br>  <span class=extraInfo>($i_WebSAMS_Notice_AttendanceCode)</span>\n";
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.LevelName, "<?php echo $i_alert_pleasefillin.$i_ClassLevel; ?>.")) return false;
}
</script>

<form name="form1" action="level_edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, 'javascript:history.back()', $button_edit." ".$i_ClassLevel, '') ?>
<?= displayTag("head_school_class_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_ClassLevel; ?>:</td><td><input class=text type=text name=LevelName size=50 maxlength=100 value='<?=$LevelName?>'></td></tr>

<?if($special_feature['websams_attendance_export']){?>
	<tr><td align=right><?php echo $i_WebSAMS_ClassLevel; ?>:</td><td><?=$select_websams_level?></td></tr>
<?php } ?>

</table>
<br><br>
</blockquote>
<input type=hidden name=ClassLevelID value="<?=$ClassLevelID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>