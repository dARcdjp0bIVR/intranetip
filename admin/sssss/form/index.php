<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

# TABLE SQL
$keyword = trim($keyword);
if ($filter == "") $filter = 1;
if ($status == "") $status = 1;
switch ($filter){
        case 0: $filter = 0; break;
        case 1: $filter = 1; break;
        default: $filter = 1; break;
}
switch ($status){
        case 0: $status = 0; break;
        case 1: $status = 1; break;
        default: $status = 1; break;
}

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;

$sql = "SELECT CONCAT('<a href=javascript:viewFormTemplate(',a.FormID,')>',a.FormName,'</a>'),
               a.Description,
               CASE (a.RecordType)
                     WHEN 0 THEN '$i_Form_Assessment'
                     WHEN 1 THEN '$i_Form_Activity'
                     END,
               a.DateModified,
               CONCAT('<input type=checkbox name=FormID[] value=', a.FormID ,'>')
        FROM INTRANET_FORM as a
        WHERE a.FormType = $filter AND a.RecordStatus = $status
              AND (a.FormName LIKE '%$keyword%'
                   OR a.Description LIKE '%$keyword%')
               ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.FormName", "a.Description", "a.RecordType","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(20,20,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $i_Form_Name)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $i_Form_Description)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $i_Form_Type)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_Form_LastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("FormID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=javascript:checkPost(document.form1,'new.php')>".newIcon()."$button_new</a>";
$functionbar = "";

$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'FormID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'FormID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


$templates_string = ($filter==0?"SELECTED" :"");
$form_string = ($filter==1?"SELECTED" :"");
$typeSelect = "<SELECT name=filter onChange=this.form.submit()>\n<OPTION value=0 $templates_string>$i_Form_Templates</OPTION>
<OPTION value=1 $form_string>$i_Form_Form</OPTION>\n</SELECT>\n";
$suspend_string = ($status==0?"SELECTED":"");
$approve_string = ($status==1?"SELECTED":"");
$statusSelect = "<SELECT name=status onChange=this.form.submit()>\n<OPTION value=0 $suspend_string>$i_Form_Suspended</OPTION>
<OPTION value=1 $approve_string>$i_Form_Approved</OPTION>\n</SELECT>\n";

$searchbar = $typeSelect.$statusSelect;
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>
<SCRIPT LANGUAGE=javascript>
function viewFormTemplate(id)
{
         newWindow("view.php?FormID="+id,1);
}
</SCRIPT>


<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Forms, '') ?>
<?= displayTag("head_school_form_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?php
include_once("../../../templates/adminfooter.php");
?>