<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libgroupcategory.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();

$content = get_file_content("$intranet_root/file/orpage.txt");
if ($content != "")
{
    $data = explode("\n",$content);
    $chk_member = ($data[0]==1? "CHECKED":"");
    $chk_email = ($data[1]==1? "CHECKED":"");
    $default_cat = ($data[2]==""? 5:$data[2]);
}
else
{
    $chk_member = "";
    $chk_email = "";
    $default_cat = 5;
}

$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnCatsForOrPage(1);
$select_cat = getSelectByArray($cats,"name=DefaultCat",$default_cat,1);
?>

<script language="javascript">
function checkform(obj){
     return true;
}
</script>

<form action="update.php" name="form1">
<?= displayNavTitle($i_admintitle_sc, '',$i_adminmenu_sc_school_settings,'/admin/school_settings/', $i_OrganizationPage_Settings, '') ?>
<?= displayTag("head_organization_set_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<br>
<table width=300 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_OrganizationPage_NotDisplayMemberList?></td><td align=center><input type=checkbox name=member value=1 <?=$chk_member?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_OrganizationPage_NotDisplayEmailAddress?></td><td align=center><input type=checkbox name=email value=1 <?=$chk_email?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_OrganizationPage_DefaultCat?></td><td align=center><?=$select_cat?></td></tr>
</table>

</blockquote>
</td></tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type='image' src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'>
<?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>