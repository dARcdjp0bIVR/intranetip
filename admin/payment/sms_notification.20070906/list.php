<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libsmsv2.php");
include_once("../../../includes/libpayment.php");

include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$lclass = new libclass();
$ldb 	= new libdb();
$lsms 	= new libsmsv2();
$lp		= new libpayment();

$stu_ary = array();
for($i=0;$i<sizeof($ClassID);$i++)
{
	$this_class = $ClassID[$i];
	$stu_ary = array_merge($stu_ary, $lclass->getClassStudentList($this_class));
}

$stu_ary = array_unique ($stu_ary);

$sms_ary	= array();
$no_sms_ary = array();
$sms_info 	= "";
$sms_no		= 0;
$no_sms_no	= 0;
$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";
$namefield2 		= $lsms->getSMSGuardianUserNameField("");

for($i=0;$i<sizeof($stu_ary);$i++)
{
	$student_id	= $stu_ary[$i];
	if($student_id=="")	continue;
	
	//Need Check Balance or not?
	if($check_bal)
	{
		$this_bal = $lp->checkBalance($student_id);
		if($this_bal[0] >= $bal_less_than) continue;
	}
	
		$lu = new libuser($student_id);
		
		$sql = "
			SELECT 
					$main_content_field,
					Relation,
					Phone, 
					EmPhone,
					IsMain,
					$namefield2
			FROM
					$eclass_db.GUARDIAN_STUDENT
			WHERE 
					UserID = $student_id and 
					isSMS = 1
			ORDER BY 
					IsMain DESC, Relation ASC
		";
		$temp = $ldb->returnArray($sql);
	
		// guardian info
		for($j=0;$j<sizeof($temp);$j++)
		{
			# phone / emergency Phone 
			$p  		= $lsms->isValidPhoneNumber($temp[$j][2]) ? $temp[$j][2] : "";
			$e  		= $lsms->isValidPhoneNumber($temp[$j][3]) ? $temp[$j][3] : "";
			$sms_phone 	= ($p!="") ? $p : $e;
			
			if(!$sms_phone)		
			{	
				$no_sms_ary[$i]['reason'] = $i_SMS_Error_NovalidPhone;
				$mobile 	= ($temp[$j][2]!="") ? $temp[$j][2] : $temp[$j][3];
				$guardianName	= "";
			}
			else
			{
				$no_sms_ary[$i]['reason'] = "";
				$mobile 	= ($p!="") ? $p : $e;
				$guardianName	= $temp[$j][5];
				break;
			}
		}
		
		//Check Guardian
		if(sizeof($temp)==0)	
		{
			$no_sms_ary[$i]['reason'] = $i_SMS_Error_No_Guardian;
			$mobile					= "";
		}
		
		if($no_sms_ary[$i]['reason'])	# in $no_sms_ary
			{
					$no_sms_ary[$i]['name'] 		= $lu->UserName();
					$no_sms_ary[$i]['classname'] 	= $lu->ClassName;
					$no_sms_ary[$i]['classnumber'] 	= $lu->ClassNumber;
					$no_sms_ary[$i]['mobile'] 		= $mobile;
					$no_sms_no++;
			}
			else
			{
					$sms_ary[$i]['guardian'] 	= $temp[$j][0];
					$sms_ary[$i]['relation'] 	= $temp[$j][1];	
					$sms_ary[$i]['name'] 		= $lu->UserName();
					$sms_ary[$i]['classname'] 	= $lu->ClassName;
					$sms_ary[$i]['classnumber']	= $lu->ClassNumber;
					$sms_ary[$i]['mobile'] 		= $mobile;
					$sms_ary[$i]['guardianName']= $guardianName;
					
					$sms_info .= $student_id .",".$mobile.",".$guardianName.";";
					$sms_no++;
			}
}

############################################################
## no_sms_ary
############################################################
$invalid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
$invalid_table.= "<tr><td>$i_SMS_Warning_Cannot_Send_To_Users</td></tr>";
$invalid_table.= "</table>";
$table_content = "";
$x = "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>\n";
$x .= "<tr class=tableTitle>
<td width=25%>$i_UserName</td>
<td width=20%>$i_ClassName</td>
<td width=5%>$i_ClassNumber</td>
<td>$i_SMS_MobileNumber</td>
<td>$i_Attendance_Reason</td></tr>\n";

$no_sms_ary_no = 0;
foreach($no_sms_ary as $this_ary)
{
	if(trim($this_ary['name'])=="")	continue;
	$x .= "<tr class=$css>";
	$x .= "<td>". $this_ary['name'] ."</td>";
	$x .= "<td>". $this_ary['classname'] ."</td>";
	$x .= "<td>". $this_ary['classnumber'] ."</td>";
	$x .= "<td>". $this_ary['mobile'] ."</td>";
	$x .= "<td>". $this_ary['reason'] ."</td>";
	$x .= "</tr>\n";
	$no_sms_ary_no++;
}
$x .= "</table>\n";
$table_content = $invalid_table.$x;

############################################################
## sms_ary
############################################################
$valid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
$valid_table.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
$table_content1 = "";
$x = "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>\n";
$x .= "<tr class=tableTitle>
<td width=25%>$i_UserName</td>
<td width=20%>$i_ClassName</td>
<td width=5%>$i_ClassNumber</td>
<td>$i_SMS_MobileNumber</td>
<td>$i_StudentGuardian_GuardianName</td>
</tr>\n";

$sms_ary_no = 0;
foreach($sms_ary as $this_ary)
{
	if(trim($this_ary['name'])=="")	continue;
	$x .= "<tr class=$css>";
	$x .= "<td>". $this_ary['name'] ."</td>";
	$x .= "<td>". $this_ary['classname'] ."</td>";
	$x .= "<td>". $this_ary['classnumber'] ."</td>";
	$x .= "<td>". $this_ary['mobile'] ."</td>";
	$x .= "<td>". $this_ary['guardianName'] ."</td>";
	$x .= "</tr>\n";
	$sms_ary_no++;
}
$x .= "</table>\n";
$table_content1 = $valid_table.$x;

$back_button = "<a href=\"javascript:history.back();\"><img src='$image_path/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$send_button = "<a href=\"javascript:document.form1.submit();\"><img src='$image_path/admin/button/s_btn_send_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
?>


<form name="form1" method="post" action="send_sms.php" >
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_SMS_Notification,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>


<? if($sms_ary_no>0) {?>
	<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>
	<tr>
		<td width="110"><?=$i_SMS_MessageContent?> : </td>
		<td><?=$Message?></td>
	</tr>
	<? if($check_bal) { ?>
	<tr>
		<td><?=$i_SMS_Balance_Lass_Than?> : </td>
		<td>$ <?=$bal_less_than?></td>
	</tr>
	<? } ?>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
	<tr><td><hr size=1></td></tr>
	<tr><td align='right'><?=$send_button?>&nbsp;<?=$back_button?></td></tr></table>
<? } ?>

<? if($no_sms_ary_no!=0) {?>
<br />
<?=$table_content?>
<? } ?>

<? if($sms_ary_no!=0) {?>
<br />
<?=$table_content1?>
<? } ?>


<? if($sms_ary_no==0) {?>
<br />
	<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
	<tr><td><hr size=1></td></tr>
	<tr><td align='right'><?=$back_button?></td></tr></table>
<? } ?>

<br>
<input type="hidden" name="sms_info" value="<?=$sms_info?>">
<input type="hidden" name="Message" value="<?=$Message?>">
</form>


<?

/*
$namefield = getNameFieldByLang("a.");
$namefield2= getNameFieldWithClassNumberByLang("a.");
$cond="";
$cond_keyword="";

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

$delim="";
for($i=0;$i<sizeof($ClassID);$i++){
	$class_str .= $delim."'".$ClassID[$i]."'";
	$delim=",";
}
$cond = " AND c.ClassName IN ($class_str) ";
if($keyword!=""){
		$cond_keyword=" AND ($namefield2 LIKE '%$keyword%' OR a.UserLogin LIKE '%$keyword%') ";
}





$main_content_field = "IF(b.EnName IS NOT NULL AND b.EnName !='' AND b.ChName IS NOT NULL AND b.ChName !='', CONCAT(b.EnName,' / ',b.ChName), IF(b.EnName IS NOT NULL AND b.EnName!='',b.EnName,b.ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$smsphone_field = "IF(d.Phone IS NOT NULL AND d.Phone !='' AND d.EmPhone IS NOT NULL AND d.EmPhone !='', CONCAT(d.Phone,' / ',d.EmPhone),IF(d.Phone IS NOT NULL AND d.Phone!='',d.Phone,d.EmPhone)) ";
$smsphone_field = "IF($smsphone_field='' OR $smsphone_field IS NULL,'-',$smsphone_field)";

$sql="SELECT CONCAT('<a href=\"javascript:editGuardian(',a.UserID,')\">',$namefield,'</a>'),
			a.ClassName,
			a.ClassNumber,
			$main_content_field,
			".($plugin['sms']?"$smsphone_field,":"")."
			CONCAT('<input type=checkbox name=StudentID[] value=',a.UserID,'>')
			FROM INTRANET_USER as a LEFT OUTER JOIN
				$eclass_db.GUARDIAN_STUDENT as b ON (a.UserID=b.UserID AND b.IsMain=1),
				INTRANET_CLASS as c LEFT OUTER JOIN
				$eclass_db.GUARDIAN_STUDENT as d ON (a.UserID=d.UserID AND d.IsSMS=1) 
			WHERE a.RecordType=2 AND a.RecordStatus IN (0,1,2) AND a.ClassName = c.ClassName AND c.RecordStatus=1 
				 $cond
				 $cond_keyword
		";
	$li = new libdbtable($field, $order, $pageNo);
	
	
	if($plugin['sms'])
		$li->field_array = array("$namefield","a.ClassName","a.ClassNumber","$main_content_field","$smsphone_field");
	else $li->field_array = array("$namefield","a.ClassName","a.ClassNumber","$main_content_field");
	
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->column_array = array(0,0,0,0,0);
	$li->wrap_array = array(0,0,0,0,0);
	$li->IsColOff = 2;

	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width=5% class=tableTitle>#</td>\n";
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassName)."</td>\n";
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_ClassNumber)."</td>\n";
	$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_StudentGuardian_MainContent)."</td>\n";
	if($plugin['sms'])
		$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_StudentGuardian_SMSPhone)."</td>\n";
	$li->column_list .= "<td width=5% class=tableTitle>".$li->check("StudentID[]")."</td>\n";

	$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'StudentID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

	
if(sizeof($ClassID))	
	$title = implode(",",$ClassID);	
?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentGuardian['MenuInfo'], 'index.php',$button_search,'search.php') ?>
<?= displayTag("head_guardian_$intranet_session_language.gif", $msg) ?>

<script language='javascript'>
function editGuardian(studentid){
	//document.form1.action = 'edit.php?SID='+studentid;
	document.form1.SID.value=studentid;
	document.form1.action='edit.php';
	document.form1.submit();
}
</script>
<form name="form1" method="GET">

	<table  width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td width=100><?=$i_StudentGuardian_Searching_Area?>:</td></tr>
	<tr><td><?=$title?></td></tr>
	<tr><td height=20></td></tr>
	</table>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?></td></tr>
	<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
	</table>
	<?php echo $li->display(); ?>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>

<br><br>
<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."'>";
}

?>
<input type=hidden name=SID value=''>
<input type=hidden name=keyword value="<?=$keyword?>">
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?php
*/
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>