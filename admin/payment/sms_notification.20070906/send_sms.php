<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend2.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libdiscipline.php");
include_once("../../../includes/libsmsv2.php");

intranet_opendb();

$ldbsms 	= new libsmsv2();
$msg 		= $Message;
$temp = split(";",$sms_info);
$sms_ary = array();

for($i=0;$i<sizeof($temp);$i++)
{
	list($student_id, $sms_phone, $gname)	= split(",", $temp[$i]);
	if($student_id=="")	continue;
	
	# get msg content
	$this_msg = $ldbsms->replace_content($student_id, $msg);
	$recipientData[] = array($sms_phone, $student_id, $gname, $this_msg);
}

	# send sms
    $sms_message = $msg;
	$targetType = 3;  
	$picType = 1;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = "";
	$frModule= "";
	$deliveryTime = "";
	$isIndividualMessage=true;
	$ldbsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, $userPIC, $frModule, $deliveryTime,$isIndividualMessage);
	
	intranet_closedb();
	
	header("Location: index.php?sent=1");

?>
