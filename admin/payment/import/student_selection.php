<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");
intranet_opendb();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=targetClass onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"size=25 multiple name=targetID[]");

?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
		
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
     {
          addtext = obj.options[i].text;
          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }

     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.submitForm();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name=form1 action="" method=post>
<table border=0 cellpadding=0 cellspacing=0 align=center width=100%>
<tr>
<td align=center>
<br>
<table border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 width=60%><tr><td bgcolor=#FFFFFF>


<table width=100% border=0 cellpadding=5 cellspacing=1>
<tr><td class=tableTitle><?php echo $i_Discipline_System_Class_Select_Instruction;?></td></tr>
<tr><td><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
<tr><td><?php echo $select_class;?></td></tr>
<?php if($targetClass != "")
{ ?>
<tr><td width=100%><hr width=90%></td></tr>
<tr><td class=tableTitle><?php echo $i_Discipline_System_Student_Select_Instruction; ?></td></tr>
<tr><td><img src=../../../images/space.gif border=0 width=1 height=3></td></tr>
<tr><td>
        <table width=100% cellpadding=0 cellspacing=0>
        <tr><td>
        <?php echo $select_students; ?></td>
        <td style="vertical-align:bottom">
        <input type=image src="<?=$image_path?>/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" alt='<?=$button_add?>' onClick=checkOption(this.form.elements["targetID[]"]);AddOptions(this.form.elements["targetID[]"])>&nbsp;<input type=image src="<?=$image_path?>/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" onClick="SelectAll(this.form.elements['targetID[]']); return false;" alt="<?=$button_select_all?>"></td></tr>
        </table>
</td></tr>
<?php
} ?>
</table>
</td></tr></table>
<br>
<a href=javascript:self.close()><img src="<?=$image_path?>/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" alt="<?php echo $button_close; ?>" border=0></a>
</td>
</tr>
</table>
<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>