<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
$user_field = getNameFieldByLang("b.");

$sql = "SELECT
               b.ClassName, b.ClassNumber, b.UserLogin, $user_field, a.PPSAccountNo,
               CONCAT('<input type=checkbox name=StudentID[] value=', a.StudentID ,'>')
         FROM
             PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
              PPSAccountNo IS NOT NULL AND PPSAccountNo != ''
              AND b.UserID IS NOT NULL AND
                        (b.ChineseName like '%$keyword%' OR
                        b.EnglishName like '%$keyword%' OR
                                b.ClassName like '%$keyword%' OR
                                b.ClassNumber like '%$keyword%' OR
                                b.UserLogin like '%$keyword%' OR
                                a.PPSAccountNo like '%$keyword%'
                                )";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("b.ClassName","b.ClassNumber","b.UserLogin",$user_field,"a.PPSAccountNo");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(2, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(3, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_Payment_Field_PPSAccountNo)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";
$toolbar .= "<a class=iconLink href=javascript:checkNew('pps_account.php')>".ImportIcon()."$button_import</a>&nbsp;";
$toolbar .= "<a class=iconLink href=javascript:checkNew('pps_account_export.php')>".ExportIcon()."$button_export_all</a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'StudentID[]','pps_account_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

?>

<form name="form1" method="GET" action="">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataImport,'index.php',$i_Payment_Menu_Import_PPSLink,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
