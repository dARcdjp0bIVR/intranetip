<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");


intranet_opendb();

$li = new libdb();

# Check charging
$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));
$temp = explode("\n",$pps_charge_from_file);
$pps_auto_charge = ($temp[0]==1);

if ($PPSFileType==1) # Counter bill
{
    $handling_charge = LIBPAYMENT_PPS_CHARGE_COUNTER_BILL;
}
else
{
    $handling_charge = LIBPAYMENT_PPS_CHARGE;
}

$trans_count = 0;

if ($confirm == 1)
{
    if ($FileDate != "")
    {
        $PPSFileType += 0;
        $sql = "INSERT INTO PAYMENT_CREDIT_FILE_LOG (FileDate, FileType) VALUES ('$FileDate','$PPSFileType')";
        $success = $li->db_db_query($sql);
        if (!$success)
        {
             header("Location: pps_log.php?dup=1");
             exit();
        }
    }
    else
    {
        header("Location: pps_log.php?dup=1");
        exit();
    }

    $sql = "LOCK TABLES
                 PAYMENT_CREDIT_TRANSACTION WRITE
                 ,TEMP_PPS_LOG WRITE
                 ,TEMP_PPS_LOG as a WRITE
                 ,PAYMENT_ACCOUNT as b WRITE
                 ,PAYMENT_ACCOUNT WRITE
                 ,PAYMENT_OVERALL_TRANSACTION_LOG WRITE
                 ,PAYMENT_MISSED_PPS_BILL WRITE";
    $li->db_db_query($sql);

    # Insert to Credit transaction table
    # RecordStatus = 1 means in progress
    $sql = "INSERT IGNORE INTO PAYMENT_CREDIT_TRANSACTION (StudentID, Amount,RecordType ,RecordStatus,PPSAccountNo, TransactionTime, PPSType)
               SELECT b.StudentID, a.Amount,1, 1, a.PPSAccount,a.InputTime, '$PPSFileType'
                      FROM TEMP_PPS_LOG as a LEFT OUTER JOIN PAYMENT_ACCOUNT as b ON a.PPSAccount = b.PPSAccountNo
               WHERE b.StudentID IS NOT NULL";
    $li->db_db_query($sql);


    # Insert missed link
    $sql = "INSERT INTO PAYMENT_MISSED_PPS_BILL (PPSAccount, Amount, InputTime, PPSType)
               SELECT  a.PPSAccount,a.Amount,a.InputTime, '$PPSFileType'
                      FROM TEMP_PPS_LOG as a LEFT OUTER JOIN PAYMENT_ACCOUNT as b ON a.PPSAccount = b.PPSAccountNo
               WHERE b.StudentID IS NULL";
    $li->db_db_query($sql);

    # Update Ref Code and Admin Name
    $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
               RefCode = CONCAT('PPS',TransactionID), AdminInCharge = '$PHP_AUTH_USER'
               WHERE RecordStatus = 1 AND RecordType = 1";
    $li->db_db_query($sql);


    $sql = "SELECT TransactionID, StudentID, Amount, DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i'),RefCode
            FROM PAYMENT_CREDIT_TRANSACTION WHERE RecordStatus = 1 AND RecordType = 1 ORDER BY TransactionTime";
    $transactions = $li->returnArray($sql,5);

    $trans_count = sizeof($transactions);
    
    $values = "";
    $delim = "";
    # For pps charge input
    $values2 = "";
    $delim2 = "";
    for ($i=0; $i<sizeof($transactions); $i++)
    {
         list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
         $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = $sid";
         $temp = $li->returnVector($sql);
         if ($pps_auto_charge)
         {
             $balanceAfter = $temp[0]+$amount;
             $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','$refCode')";
             $delim = ",";

             $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount - $handling_charge, LastUpdateByAdmin = '$PHP_AUTH_USER', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = $sid";
             $li->db_db_query($sql);

             $balanceAfter2 = $balanceAfter - $handling_charge;
             $values2 .= "$delim2 ('$sid',8,'$handling_charge','$tid','$balanceAfter2',now(),'$i_Payment_PPS_Charge','')";
             $delim2 = ",";

         }
         else
         {
             $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '$PHP_AUTH_USER', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = $sid";
             $li->db_db_query($sql);
             $balanceAfter = $temp[0]+$amount;
             # Change transaction time on Overall Transaction to now()
             #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailPPS','$refCode')";
             $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','$refCode')";
             $delim = ",";
         }
    }
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES $values";
    $li->db_db_query($sql);

    if ($pps_auto_charge)
    {
        $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES $values2";
        $li->db_db_query($sql);
    }


    # Update in progress to finished status
    $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
               RecordStatus = 0,
               DateInput = now() WHERE RecordStatus = 1 AND RecordType = 1 ";
    $li->db_db_query($sql);

    $sql = "DELETE FROM TEMP_PPS_LOG";
    $li->db_db_query($sql);

    $sql = "UNLOCK TABLES";
    $li->db_db_query($sql);
    $msg = 2;
}
else
{
    $msg = "";
}
if($trans_count<=0){
	$no_trans="&no_trans=1";
}
intranet_closedb();
header("Location: pps_log.php?msg=$msg".$no_trans);
?>
