<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$linterface = new interface_html();
$limport = new libimporttext();
$lclass = new libclass();
$li = new libdb();

$hours = array();
$minutes = array();

for($i=0; $i<24; $i++)
{
	if($i<10)
		$i='0'.$i;
	array_push($hours, $i);
}

for($j=0; $j<60; $j++)
{
	if($j<10)
		$j='0'.$j;
	array_push($minutes, $j);
}

if($TargetDate=="")
	$TargetDate = date('Y-m-d');
	
if(sizeof($student)>0)
{
	$student_list = implode(",",$student);
	$namefield = getNameFieldWithClassNumberByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE UserID IN ($student_list) ORDER BY ClassName, ClassNumber, EnglishName";
	$result = $lclass->returnArray($sql,2);
	
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($uid, $name) = $result[$i];
			$table_content .= "<tr>";
			$table_content .= "<input type=hidden name=student[] value=$uid>";
			$table_content .= "<td>$name</td>";
			$table_content .= "<td><input type=text name=amount_$uid size=8></td>";
			$table_content .= "<td><input type=text name=ref_$uid size=8></td>";
			$table_content .= "<td><input type=text name=TargetDate_$uid size=10>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span></td>";
			$curr_hour = date("H");
			$curr_min = date("i");
			$table_content .= "<td>".getSelectByValue($hours,"name=hour_$uid",0,0,1).":".getSelectByValue($minutes,"name=min_$uid",0,0,1)."</td>";
			$table_content .= "</tr>";
		}
	}
}
?>

<SCRIPT Language="JavaScript">
function setAllDate(checking)
{
	if(checking == 1)
	{
		temp_value = document.form1.TargetDate_SetAll.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp = eval("document.form1.TargetDate_"+<?=$u_id?>);
			temp.value = temp_value;
		<?
		}
		?>
	}
	if(checking == 0)
	{
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp = eval("document.form1.TargetDate_"+<?=$u_id?>);
			temp.value = '';
		<?
		}
		?>
	}
}
function setAllTime(checking)
{
	if(checking == 1)
	{
		selected_hour = document.form1.all_hour.value;
		selected_min = document.form1.all_min.value;
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp_hour = eval("document.form1.hour_"+<?=$u_id?>);
			temp_min = eval("document.form1.min_"+<?=$u_id?>);
			temp_hour.value = selected_hour;
			temp_min.value = selected_min;
		<?
		}
		?>
	}
	if(checking == 0)
	{
		<?
		for($i=0; $i<sizeof($result); $i++)
		{
			list($u_id, $u_name) = $result[$i];
		?>
			temp_hour = eval("document.form1.hour_"+<?=$u_id?>);
			temp_min = eval("document.form1.min_"+<?=$u_id?>);
			temp_hour.value = '00';
			temp_min.value = '00';
		<?
		}
		?>
	}
}
function checkForm()
{
	var checking=0;
	
	<?
	for($i=0; $i<sizeof($result); $i++)
	{
		list($u_id, $u_name) = $result[$i];
	?>
		temp_date 	=	eval("document.form1.TargetDate_"+<?=$u_id?>);
		temp_hour 	= 	eval("document.form1.hour_"+<?=$u_id?>);
		temp_min 	= 	eval("document.form1.min_"+<?=$u_id?>);
		temp_amount = 	eval("document.form1.amount_"+<?=$u_id?>);
		temp_ref 	=	eval("document.form1.ref_"+<?=$u_id?>);
		year		=	temp_date.value.substr(0,4);
		month		=	temp_date.value.substr(5,2);
		day			=	temp_date.value.substr(8,2);
		
		if(temp_amount.value!='')
		{
			if(!isNaN(temp_amount.value))
			{
				if(temp_amount.value > 0)
				{
					if(temp_ref.value!='')
					{
						if(temp_date.value!='')
						{
							if((!isNaN(year))&&(!isNaN(month))&&(!isNaN(day)))
							{
								if(year%4==0)
								{
									switch(month)
									{
										case '01':
										case '03':
										case '05':
										case '07':
										case '08':
										case '10':
										case '12':
											if(day<=31)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										
										case '04':
										case '06':
										case '09':
										case '11':
											if(day<=30)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										case '02':
											if(day<=29)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										default:
											break;
									}
								}
								else
								{
									switch(month)
									{
										case '01':
										case '03':
										case '05':
										case '07':
										case '08':
										case '10':
										case '12':
											if(day<=31)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										
										case '04':
										case '06':
										case '09':
										case '11':
											if(day<=30)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										case '02':
											if(day<=28)
											{
												checking = 1;
											}
											else
											{
												alert("<?=$i_Payment_DepositWrongDateWarning?>");
												return false;
											}
											break;
										default:
											break;
									}
								}
							}
						}
						else
						{
							alert("<?=$i_Payment_DepositDateWarning?>");
							return false;
						}
					}
					else
					{
						alert("<?=$i_Payment_RefCodeWarning?>");
						return false;
					}
				}
				else
				{
					alert("<?=$i_Payment_DepositAmountWarning?>");
					return false;
				}
			}
			else
			{
				alert("<?=$i_Payment_DepositAmountWarning?>");
				return false;
			}
		}
		else
		{
			alert("<?=$i_Payment_AmountWarning?>");
			return false;
		}
	<?
	}
	?>
if(checking == 1)
{
	return true;
}	
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataImport,'index.php',$i_Payment_Menu_ManualCashDeposit,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="POST" action="manual_cash_log_update.php" onSubmit="return checkForm()">
<table width=95% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align=center>
<tr class=tableTitle>
	<td width=10% style="vertical-align:bottom"><?=$i_UserName?></td>
	<td style="vertical-align:bottom"><?=$i_Payment_Field_Amount?></td>
	<td style="vertical-align:bottom"><?=$i_Payment_Field_RefCode?></td>
	<td style="vertical-align:bottom">
		<?=$i_Payment_CashDepositDate?><br>
		<input type=text name=TargetDate_SetAll size=10 value=<?=$TargetDate?>>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>&nbsp;<input type=checkbox name=SetAllDate onClick="this.checked?setAllDate(1):setAllDate(0)"><br>
	</td>
	<td style="vertical-align:bottom">
		<?=$i_Payment_CashDepositTime?><br>
		<?=getSelectByValue($hours,"name=all_hour",$curr_hour,0,1)?>:<?=getSelectByValue($minutes,"name=all_min",$curr_min,0,1)?><input type=checkbox name=SetAllTime onClick="this.checked?setAllTime(1):setAllTime(0)">
	</td>
</tr>
<?=$table_content?>
</table>
<table border=0>
<tr><td height=10px></td></tr>
</table>
<table width=90% border=0 align=center>
<tr><td align=right><?=btnSubmit()?><?=btnReset()?><a href="manual_cash_log.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
