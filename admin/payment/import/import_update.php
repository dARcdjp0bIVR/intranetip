<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$file_format = array("Year","Semester","Date","Class","Class Number","Award Name","Remark");
$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;


if ($confirm != 1){          # import failed
        header("Location: import.php");
} else {
        //$ext = strtoupper($lo->file_ext($filename));
        //if($limport->CHECK_FILE_EXT($filename))
        //{
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
               $display .= "<table width=100 border=1>\n";
               for ($i=0; $i<sizeof($file_format); $i++)
               {
                    $display .= "<tr><td>".$file_format[$i]."</td></tr>\n";
               }
               $display .= "</table></blockquote>\n";
           }
           else
           {
               $t_delimiter = "";
               $toTempValues = "";
               $currentYear = getCurrentAcademicYear();
               $currentSem = getCurrentSemester();
               $defaultUnit = 1;
               for ($i=0; $i<sizeof($data); $i++)
               {
                    list($year,$sem,$date,$class,$classnum,$award,$remark) = $data[$i];
                    $date = (($date == "")? "NULL": "'$date'");
                    $year = ($year == ""? $currentYear : $year);
                    $sem = ($sem == ""? $currentSem : $sem);
                    $class = trim(addslashes($class));
                    $classnum = trim(addslashes($classnum));
                    $award = trim(addslashes($award));
                    $remark = trim(addslashes($remark));
                    if ($class != "" && $classnum != "")
                    {
                        $toTempValues .= "$t_delimiter ($date,'$year','$sem','$class','$classnum','$award','$remark')";
                        $t_delimiter = ",";
                    }
               }

               # Create temp table
               $sql = "CREATE TEMPORARY TABLE TEMP_AWARD_IMPORT (
                              AwardDate datetime,
                              Year char(20),
                              Semester char(20),
                              ClassName varchar(255),
                              ClassNumber varchar(255),
                              AwardName text,
                              Remark text
                              )";
               $li->db_db_query($sql);
               $fields_name = "AwardDate, Year,Semester, ClassName, ClassNumber, AwardName, Remark";
               $sql = "INSERT IGNORE INTO TEMP_AWARD_IMPORT ($fields_name) VALUES $toTempValues";
               $li->db_db_query($sql);

               $fields_name = "UserID, AwardDate, Year, Semester, AwardName,Remark, DateInput, DateModified,ClassName,ClassNumber";
               $sql = "INSERT IGNORE INTO PROFILE_STUDENT_AWARD ($fields_name)
                              SELECT a.UserID, b.AwardDate, b.Year,b.Semester,
                                     b.AwardName,
                                     b.Remark,now(),now(),
                                     a.ClassName,a.ClassNumber
                              FROM INTRANET_USER as a, TEMP_AWARD_IMPORT as b
                                   WHERE a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber
                                         AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)
                      ";
               $li->db_db_query($sql);
               header("Location: index.php?msg=10");
           }
           if ($display != '')
           {
               include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
               echo displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Award, 'index.php', $button_import, '');
               echo displayTag("head_award_$intranet_session_language.gif", $msg);
               $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                               <tr><td><hr size=1></td></tr>
                               <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
               echo $display;
               include_once($PATH_WRT_ROOT."templates/adminfooter.php");
           }
        //}
        /*else
        {
            header("Location: import.php");
        }*/


}

intranet_closedb();
?>