<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$user_field = getNameFieldByLang("b.");

$sql = "SELECT
				b.ClassName, b.ClassNumber, b.UserLogin, $user_field, a.PPSAccountNo
         FROM
         		PAYMENT_ACCOUNT as a LEFT OUTER JOIN 
         		INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
         		PPSAccountNo IS NOT NULL AND PPSAccountNo != '' AND 
         		b.UserID IS NOT NULL
         ORDER BY
         		b.ClassName, b.ClassNumber";

$arr_result = $li->returnArray($sql,5);


$content .= "\"$i_UserClassName\",";
$content .= "\"$i_UserClassNumber\",";
$content .= "\"$i_UserLogin\",";
$content .= "\"$i_UserStudentName\",";
$content .= "\"$i_Payment_Field_PPSAccountNo\",";
$content .= "\n";


$rows[] = array($i_UserClassName, 
				$i_UserClassNumber, 
				$i_UserLogin, 
				$i_UserStudentName, 
				$i_Payment_Field_PPSAccountNo);

if(sizeof($arr_result)>0) {
	for($i=0; $i<sizeof($arr_result); $i++){
		list($class_name, $class_number, $user_login, $user_name, $user_pps) = $arr_result[$i];
		
		$content .= "\"$class_name\",";
		$content .= "\"$class_number\",";
		$content .= "\"$user_login\",";
		$content .= "\"$user_name\",";
		$content .= "\"$user_pps\",";
		$content .= "\n";
		
		$rows[] = array($class_name, $class_number, $user_login, $user_name, $user_pps);
	}
}

intranet_closedb();

$display="\"$i_Payment_Menu_Import_PPSLink\"\n";
$display.="\n";

$utf_display="$i_Payment_Menu_Import_PPSLink\r\n";
$utf_display.="\r\n";

$timestamp = time();
$filename = "student_pps_linkage_".$timestamp.".csv";
$file_content .= $content;

if($g_encoding_unicode){
	$export_content = $utf_display.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
}else{
	output2browser($display.$file_content,$filename);
}


?>