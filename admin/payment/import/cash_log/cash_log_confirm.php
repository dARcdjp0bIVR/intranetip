<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$lpayment = new libpayment();
$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;



$format=$format==1?1:2;
if($format==1)
	$file_format = array ("ClassName","ClassNumber","Amount","Time","RefCode");
if($format==2)
	$file_format = array ("UserLogin","Amount","Time","RefCode");

if($filepath=="none" || $filepath == "" || !$limport->CHECK_FILE_EXT($filename)){ 	# import failed
        header("Location: cash_log.php?error=1");
        exit();
} else {
    	    
			# read file into array
           # return 0 if fail, return csv array if success
           $data = $limport->GET_IMPORT_TXT($filepath);
		   $col_name = array_shift($data);

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: cash_log.php?error=1");
               exit();
           }
           
           
      	   # build student array
    	   $t_sql ="SELECT UserID,UserLogin,ClassName,ClassNumber,RecordType FROM INTRANET_USER WHERE (RecordType=2 OR RecordType=1) AND RecordStatus IN(0,1,2)";
       	   $t_result = $li->returnArray($t_sql,5);
       	   for($i=0;$i<sizeof($t_result);$i++){
	       	   list($u_id,$u_login,$u_class,$u_classnum,$type) = $t_result[$i];
	       	   if($format==1){
		       	   $students[$u_class][$u_classnum]['uid']=$u_id;
		       	   $students[$u_class][$u_classnum]['login']=$u_login;
		       }
	       	   else if($format==2){
		       	   $students[$u_login]['uid'] = $u_id;
		       	   $students[$u_login]['class'] = $u_class;
		       	   $students[$u_login]['classnumber']=$u_classnum;
		       	   $students[$u_login]['type']=$type;

		       }
	       }
          
           $error = false;
           
           # Update TEMP_CASH_DEPOSIT
           $values = "";
           $delim = "";
           for ($i=0; $i<sizeof($data); $i++)
           {
				# check for empty line
				$test = trim(implode("",$data[$i])); 
				if($test=="") continue;
				
	           if($format==1){ # Format 1
                	list($class, $classnum, $amount, $tran_time, $refCode) = $data[$i];
                	$class = trim($class);
                	$classnum = trim($classnum);
                	if ($class != "" && $classnum != "" && $amount > 0)
                	{
	                	if($students[$class][$classnum]['uid']==""){ # no match student
	                		$error = true;
	                    	$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","<i>$class</i>","<i>$classnum</i>","","$amount","<i>$tran_time</i>","<i>$refCode</i>");
	                    }else{
		                    $user_login = $students[$class][$classnum]['login'];
		                    $user_type=2;
		                }
		            }
                }
                else if($format==2){ # Format 2

                	list($user_login, $amount, $tran_time, $refCode) = $data[$i];
                	
                	if($students[$user_login]['uid']==""){ # no matched student
                		$error = true;
                    	$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","","","<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>");
                	}
	               	else if($user_login!=""){
	                  	$user_type = $students[$user_login]['type'];
	                   	$class = $user_type==1?"-" :$students[$user_login]['class'];
	                   	$classnum = $user_type==1?"-" :$students[$user_login]['classnumber'];
                   	}
	            }
	            if (!$error && $amount>0){
		            if($user_type==1 ||  ($user_type==2 && $class != "" && $classnum != "")){
	                    if ($tran_time != "")
	                        $time_str = "'$tran_time'";
	                    else $time_str = "now()";
	                    $values .= "$delim ('$user_login','$class','$classnum','$amount',$time_str,'$refCode')";
	                    $delim = ",";
                	}
                }
              
           }
           if(!$error){
           	   $sql = "INSERT INTO TEMP_CASH_DEPOSIT (UserLogin,ClassName,ClassNumber,Amount,InputTime,RefCode) VALUES $values";
          		$li->db_db_query($sql);
        	}
        		
		/*
		$ext = strtoupper($lo->file_ext($filename));
        if($ext == ".CSV")
        {
           # read file into array
           # return 0 if fail, return csv array if success
           $data = $lo->file_read_csv($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
           if ($format_wrong)
           {
               header ("Location: cash_log.php?error=1");
               exit();
           }
           
           
      	   # build student array
    	   $t_sql ="SELECT UserID,UserLogin,ClassName,ClassNumber,RecordType FROM INTRANET_USER WHERE (RecordType=2 OR RecordType=1) AND RecordStatus IN(0,1,2)";
       	   $t_result = $li->returnArray($t_sql,5);
       	   for($i=0;$i<sizeof($t_result);$i++){
	       	   list($u_id,$u_login,$u_class,$u_classnum,$type) = $t_result[$i];
	       	   if($format==1){
		       	   $students[$u_class][$u_classnum]['uid']=$u_id;
		       	   $students[$u_class][$u_classnum]['login']=$u_login;
		       }
	       	   else if($format==2){
		       	   $students[$u_login]['uid'] = $u_id;
		       	   $students[$u_login]['class'] = $u_class;
		       	   $students[$u_login]['classnumber']=$u_classnum;
		       	   $students[$u_login]['type']=$type;

		       }
	       }
          
           $error = false;
           
           # Update TEMP_CASH_DEPOSIT
           $values = "";
           $delim = "";
           for ($i=0; $i<sizeof($data); $i++)
           {
				# check for empty line
				$test = trim(implode("",$data[$i])); 
				if($test=="") continue;
				
	           if($format==1){ # Format 1
                	list($class, $classnum, $amount, $tran_time, $refCode) = $data[$i];
                	$class = trim($class);
                	$classnum = trim($classnum);
                	if ($class != "" && $classnum != "" && $amount > 0)
                	{
	                	if($students[$class][$classnum]['uid']==""){ # no match student
	                		$error = true;
	                    	$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","<i>$class</i>","<i>$classnum</i>","","$amount","<i>$tran_time</i>","<i>$refCode</i>");
	                    }else{
		                    $user_login = $students[$class][$classnum]['login'];
		                    $user_type=2;
		                }
		            }
                }
                else if($format==2){ # Format 2

                	list($user_login, $amount, $tran_time, $refCode) = $data[$i];
                	
                	if($students[$user_login]['uid']==""){ # no matched student
                		$error = true;
                    	$error_entries[] = array("&nbsp;&nbsp;<font color=red><i>*</i></font>","","","<i>$user_login</i>","$amount","<i>$tran_time</i>","<i>$refCode</i>");
                	}
	               	else if($user_login!=""){
	                  	$user_type = $students[$user_login]['type'];
	                   	$class = $user_type==1?"-" :$students[$user_login]['class'];
	                   	$classnum = $user_type==1?"-" :$students[$user_login]['classnumber'];
                   	}
	            }
	            if (!$error && $amount>0){
		            if($user_type==1 ||  ($user_type==2 && $class != "" && $classnum != "")){
	                    if ($tran_time != "")
	                        $time_str = "'$tran_time'";
	                    else $time_str = "now()";
	                    $values .= "$delim ('$user_login','$class','$classnum','$amount',$time_str,'$refCode')";
	                    $delim = ",";
                	}
                }
              
           }
           if(!$error){
           	   $sql = "INSERT INTO TEMP_CASH_DEPOSIT (UserLogin,ClassName,ClassNumber,Amount,InputTime,RefCode) VALUES $values";
          		$li->db_db_query($sql);
        	}
        }
        else
        {
            header ("Location: cash_log.php?error=1");
            exit();
        }
        */
}

if(!$error){
	$namefield = getNameFieldByLang("b.");
	if($format==1){
		$sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin, a.Amount, a.InputTime, a.RefCode
        	FROM TEMP_CASH_DEPOSIT as a LEFT OUTER JOIN INTRANET_USER as b
            	 ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)";
    }else if($format ==2){
   		$sql = "SELECT $namefield, a.ClassName, a.ClassNumber, b.UserLogin, a.Amount, a.InputTime, a.RefCode
        	FROM TEMP_CASH_DEPOSIT as a LEFT OUTER JOIN INTRANET_USER as b
            	 ON a.UserLogin = b.UserLogin AND b.RecordType IN(1,2) AND b.RecordStatus IN (0,1,2)";
	}
		$result = $li->returnArray($sql,7);
}else{
	$result = $error_entries;
}

$display = "<tr>
<td class=tableTitle width=130>$i_Payment_Field_Username</td>
<td class=tableTitle width=60>$i_UserClassName</td>
<td class=tableTitle width=60>$i_UserClassNumber</td>
<td class=tableTitle width=60>$i_UserLogin</td>
<td class=tableTitle width=80>$i_Payment_Field_CreditAmount</td>
<td class=tableTitle width=90>$i_Payment_Field_TransactionTime</td>
<td class=tableTitle width=80>$i_Payment_Field_RefCode</td>
</tr>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($name,$class,$classnum,$login,$amount,$tran_time,$refcode) = $result[$i];
     $css = ($i%2? "":"2");
     $display .= "<tr class=tableContent$css><td>$name</td><td>$class</td><td>$classnum</td><td>$login</td><td>".$lpayment->getWebDisplayAmountFormat($amount)."</td><td>$tran_time</td><td>$refcode&nbsp;</td></tr>\n";
}
include_once("../../../../templates/adminheader_setting.php");

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataImport,'../',$Lang['Payment']['CashDeposit'],'index.php',$Lang['Payment']['CSVImportDeposit'],'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name="form1" method="GET" action="cash_log_update.php">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<?=($error?($format==1?$i_Payment_Import_NoMatch_Entry:$i_Payment_Import_NoMatch_Entry2):$i_Payment_Import_Confirm)?>
</td></tr></table><br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$display?>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<table width=560 cellspacing=0 cellpadding=0>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?php if(!$error){?>
	<input type="image" alt="<?=$button_confirm?>" src="/images/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border='0'>
<?php } ?>
<a href=cash_log.php?clear=1><img alt="<?=$button_cancel?>" border=0 src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirm value=1>
<input type=hidden name=format value="<?=$format?>">

</form>

<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>
