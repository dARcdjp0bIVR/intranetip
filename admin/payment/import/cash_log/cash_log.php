<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$linterface = new interface_html();
$limport = new libimporttext();

$li = new libdb();

if ($clear == 1)
{
    //$sql = "DELETE FROM TEMP_CASH_DEPOSIT";
    $sql ="DROP TABLE TEMP_CASH_DEPOSIT";
    $li->db_db_query($sql);
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_CASH_DEPOSIT (
 UserLogin varchar(20),
 ClassName varchar(255),
 ClassNumber varchar(255),
 Amount float,
 InputTime datetime,
 RefCode varchar(255)
)";
$li->db_db_query($sql);


$sql = "SELECT COUNT(*) FROM TEMP_CASH_DEPOSIT";
$temp = $li->returnVector($sql);
$count = $temp[0];

if($error==1)
	$sysMsg = $i_StaffAttendance_import_invalid_entries;
//else $sysMsg = $lpayment->getResponseMsg($msg);

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataImport,'../',$Lang['Payment']['CashDeposit'],'index.php',$Lang['Payment']['CSVImportDeposit'],'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<?


if ($count != 0)
{
?>
<form name="form1" method="GET" action="">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?=$i_Payment_Import_ConfirmRemoval?></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Import_ConfirmRemoval2?></td></tr>
<tr><td><input type=image src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=clear value=1>
</form>
<?
}
else
{
?>

<form name="form1" method="POST" action="cash_log_confirm.php" enctype="multipart/form-data">
<blockquote>
<?php if($sysMsg!=""){?>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td align=right><font color=red><?=$sysMsg?></font></td></tr>
</table>
<?php } ?>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td>
		<table border=0 cellpadding=0 cellspacing=0><tr><Td>
	<input type=radio name=format value=1 CHECKED></td><td valign=middle><?=$i_Payment_Import_CashDeposit_Instruction?><Br>&nbsp;<a class=functionlink_new href="<?= GET_CSV("sample_cashdep.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a></td></tr>
		<tr><td colspan='2'>&nbsp;<Br>&nbsp;</td></tr>
	<tr><td><input type=radio name=format value=2></td><td><?=$i_Payment_Import_CashDeposit_Instruction2?><br>&nbsp;<a class=functionlink_new href="<?= GET_CSV("sample_cashdep2.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>
	</td></tr></table><br>&nbsp;
</td></tr>
</table>
<!--
<blockquote>
<?=$i_Payment_Import_CashDeposit_Instruction?>
</blockquote>

<p><a class=functionlink_new href="sample_cashdep.csv"><?=$i_general_clickheredownloadsample?></a></p>-->
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?
}
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>
