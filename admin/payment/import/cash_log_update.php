<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();

if($format=="")
	header("Location: cash_log.php");

if ($confirm == 1)
{
    $sql = "LOCK TABLES
                 PAYMENT_CREDIT_TRANSACTION WRITE
                 ,TEMP_CASH_DEPOSIT WRITE
                 ,TEMP_CASH_DEPOSIT as a WRITE
                 ,INTRANET_USER as b READ
                 ,PAYMENT_ACCOUNT as WRITE
                 ,PAYMENT_OVERALL_TRANSACTION_LOG as WRITE";
    $li->db_db_query($sql);

    if($format==1){
    	$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (StudentID, Amount,RecordType, RecordStatus, TransactionTime, RefCode)
               SELECT b.UserID, a.Amount,2, 1,a.InputTime, a.RefCode
                      FROM TEMP_CASH_DEPOSIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus = 1
               WHERE b.UserID IS NOT NULL";
	}else if($format==2){
	    $sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (StudentID, Amount,RecordType, RecordStatus, TransactionTime, RefCode)
	               SELECT b.UserID, a.Amount,2, 1,a.InputTime, a.RefCode
	                      FROM TEMP_CASH_DEPOSIT as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserLogin = b.UserLogin AND b.RecordType IN (1,2) AND b.RecordStatus = 1
	               WHERE b.UserID IS NOT NULL";
    }
    $li->db_db_query($sql);

    # Update Ref Code
    $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
               RefCode = CONCAT('CSH',TransactionID), AdminInCharge = '$PHP_AUTH_USER'
               WHERE RecordStatus = 1 AND RecordType = 2 AND (RefCode IS NULL OR RefCode = '')";
              
    $li->db_db_query($sql);


    $sql = "SELECT TransactionID, StudentID, Amount, DATE_FORMAT(TransactionTime,'%Y-%m-%d %H:%i'),RefCode
            FROM PAYMENT_CREDIT_TRANSACTION WHERE RecordStatus = 1 AND RecordType = 2 ORDER BY TransactionTime";
    $transactions = $li->returnArray($sql,5);

    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($transactions); $i++)
    {
         list($tid, $sid, $amount, $tran_time, $refCode) = $transactions[$i];
         $sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = $sid";
         $temp = $li->returnVector($sql);
         $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount, LastUpdateByAdmin = '$PHP_AUTH_USER', LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = $sid";
         $li->db_db_query($sql);
         $balanceAfter = $temp[0]+$amount;
         # Change transaction time on overall trans to now()
         #$values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter','$tran_time','$i_Payment_TransactionDetailCashDeposit','$refCode')";
         $values .= "$delim ('$sid',1,'$amount','$tid','$balanceAfter',now(),'$i_Payment_TransactionDetailCashDeposit','$refCode')";
         $delim = ",";
    }
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
                   (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES $values";
    $li->db_db_query($sql);

    # Update in progress to finished status
    $sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
               RecordStatus = 0,
               DateInput = now() WHERE RecordStatus = 1 AND RecordType = 2 ";
    $li->db_db_query($sql);

    $sql = "DELETE FROM TEMP_CASH_DEPOSIT";
    $li->db_db_query($sql);

    $sql = "UNLOCK TABLES";
    $li->db_db_query($sql);
    $msg = 2;
}
else
{
    $msg = "";
}

intranet_closedb();
header("Location: cash_log.php?msg=$msg");
?>
