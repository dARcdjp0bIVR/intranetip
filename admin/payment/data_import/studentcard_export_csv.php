<?
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$count = 0;
if ($_GET['format']==1)
{
    $sql = "SELECT UserLogin, CardID FROM INTRANET_USER WHERE RecordType = 2 ORDER BY UserLogin";
    $count = 2;
    $title_row = "\"UserLogin\",\"CardID\"\r\n";
    $exportColumn = array("UserLogin","CardID");
}
else if ($_GET['format']==2)
{
     $sql = "SELECT ClassName, ClassNumber, CardID FROM INTRANET_USER WHERE RecordType = 2 AND ClassName != '' ORDER BY ClassName, ClassNumber";
     $count = 3;
     $title_row = "\"ClassName\",\"ClassNumber\",\"CardID\"\r\n";
     $exportColumn = array("ClassName","ClassNumber","CardID");
}
if ($count < 1)
{
    header("Location: studentcard_export.php");
    exit();
}

$result = $li->returnArray($sql,$count);

$x = $title_row;
for ($i=0; $i<sizeof($result); $i++)
{
     $delim = "";
     for ($j=0; $j<$count; $j++)
     {
          $x .= $delim . "\"".$result[$i][$j]."\"";
          $delim = ",";
     }
     $x .= "\r\n";
}

// Output the file to user browser
$filename = "studentcard_id_$format.csv";
$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($x, $filename);
intranet_closedb();
?>
