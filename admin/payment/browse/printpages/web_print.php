<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();

$lpayment = new libpayment();

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 45;

//Default width of a specific field
$defaultFieldWidth1 = 53;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;

# Page layout details
$page_breaker = "<P CLASS='breakhere'>";
$page_header_linefeed = 8;
$intermediate_pageheader = "<P CLASS='breakhere'>";
$intermediate_linefeed = 5;
?>

<STYLE TYPE="text/css">
     P.breakhere {page-break-before: always}
</STYLE>

<?
if(sizeof($UserType)>0)
	$temp_UserType = implode(",",$UserType);
if(sizeof($UserStatus)>0)
	$temp_UserStatus = implode(",",$UserStatus);

if($temp_UserStatus!="")
	$cond = " AND RecordStatus IN ($temp_UserStatus)";
else
	$cond = " ";
	
$sql = "SELECT * FROM INTRANET_USER WHERE RecordType IN ($temp_UserType) $cond";

$target_user = $lpayment->returnVector($sql);

if(sizeof($target_user)>0)
{
	$user_list = implode(",",$target_user);
	
	$namefield = getNameFieldWithClassNumberByLang("a.");
	
	$sql = "SELECT 
					a.UserID, $namefield, IF(b.PPSAccountNo IS NULL, ' -- ', b.PPSAccountNo), CONCAT('$',IF(b.Balance>=0,FORMAT(b.Balance,1),'0.0') ), if(b.LastUpdated IS NULL, ' -- ', b.LastUpdated)
			FROM 
					INTRANET_USER AS a LEFT OUTER JOIN 
					PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE
					a.UserID IN ($user_list) 
			ORDER BY
					a.ClassName, a.ClassNumber, a.EnglishName";

	$payment_account_result = $lpayment->returnArray($sql, 5);
	
	if(sizeof($payment_account_result)>0)
	{
		$lineRemain = $defaultNumOfLine - $page_header_linefeed;
		
		for($i=0; $i<sizeof($payment_account_result); $i++)
		{
			list($u_id, $name, $pps, $bal, $last_modified) = $payment_account_result[$i];

			$sql2  = "SELECT
	               DATE_FORMAT(a.TransactionTime,'%Y-%m-%d %H:%i'),
	               IF(a.TransactionType=1,DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),' -- '),
	               CASE a.TransactionType
	                    WHEN 1 THEN '$i_Payment_TransactionType_Credit'
	                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
	                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
	                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
	                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
	                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
	                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
	                    ELSE '$i_Payment_TransactionType_Other' END,
	               IF(a.TransactionType IN (1,5,6),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
	               IF(a.TransactionType IN (2,3,4,7),CONCAT('$',FORMAT(a.Amount,1)),' -- '),
	               a.Details, CONCAT('$',IF(a.BalanceAfter>=0,FORMAT(a.BalanceAfter,1),'0.0') ),
	               IF(a.RefCode IS NULL, ' -- ', a.RefCode)
	         FROM
	               PAYMENT_OVERALL_TRANSACTION_LOG as a
	               LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as b ON a.RelatedTransactionID = b.TransactionID
	         WHERE
	               a.StudentID =$u_id";
	    
			$lineRemain -= 8;
			
			if ($lineRemain <0)
			{
	            $table_content .= "$intermediate_pageheader";
	            $lineRemain = $defaultNumOfLine - 8;
			}
			
	        $table_content .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>";
			$table_content .= "<tr><td><b>$i_Payment_Field_Username:</b> $name<br>";
			$table_content .= "<b>$i_Payment_Field_Balance:</b> $bal<br>";
			$table_content .= "<b>$i_Payment_Field_LastUpdated:</b> $last_modified<br>";
			$table_content .= "<b>$i_Payment_Field_PPSAccountNo:</b> $pps</td></tr>";
			$table_content .= "<tr><td><table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
			$table_content .= "<tr class=tableTitle><td>$i_Payment_Field_TransactionTime</td>";
			$table_content .= "<td>$i_Payment_Field_TransactionFileTime</td>";
			$table_content .= "<td>$i_Payment_Field_TransactionType</td>";
			$table_content .= "<td>$i_Payment_TransactionType_Credit</td>";
			$table_content .= "<td>$i_Payment_TransactionType_Debit</td>";
			$table_content .= "<td>$i_Payment_Field_TransactionDetail</td>";
			$table_content .= "<td>$i_Payment_Field_BalanceAfterTransaction</td>";
			$table_content .= "<td>$i_Payment_Field_RefCode</td></tr>";
			       
			$payment_detail_result = $lpayment->returnArray($sql2,7);
			
			if(sizeof($payment_detail_result)>0)
			{
				for ($z=0; $z<sizeof($payment_detail_result); $z++)
				{

		            list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$z];
		
		            $currentLineNeed = ceil ( strlen($transaction_time.$credit_transaction_time.$transaction_type.$credit_amount.$debit_amount.$transaction_detail.$balance_after.$ref_no) / $defaultFieldWidth1 );
		
		            if ($fieldLineNeed < $currentLineNeed)
		                    $fieldLineNeed = $currentLineNeed;
		
				}
				$lineRemain = $lineRemain - $fieldLineNeed * sizeof($payment_detail_result) ;
				
				for($j=0; $j<sizeof($payment_detail_result); $j++)
				{
					list($transaction_time, $credit_transaction_time, $transaction_type, $credit_amount, $debit_amount, $transaction_detail, $balance_after, $ref_no) = $payment_detail_result[$j];
					
					$table_content .= "<tr><td>$transaction_time</td>";
					$table_content .= "<td>$credit_transaction_time</td>";
					$table_content .= "<td>$transaction_type</td>";
					$table_content .= "<td>$credit_amount</td>";
					$table_content .= "<td>$debit_amount</td>";
					$table_content .= "<td>$transaction_detail</td>";
					$table_content .= "<td>$balance_after</td>";
					$table_content .= "<td>$ref_no</td></tr>";
				}
			}
			if(sizeof($payment_detail_result)==0)
			{
				$lineRemain = $lineRemain - 1;
				$table_content .= "<tr><td colspan=8 align=center>$i_no_record_exists_msg</td></tr>";
			}
			$table_content .= "</table></td></tr>";
			$table_content .= "</table>";
			$table_content .= "<table border=0>";
			$table_content .= "<tr><td height=20px></td></tr>";
			$table_content .= "</table>";
		}
	}
}
?>
<?=$table_content?>