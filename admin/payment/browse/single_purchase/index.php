<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();
if ($start == "" || $end == "") # Default current month
{
    $start = date("Y-m-d",mktime(0,0,0,date('m'),1,date('Y')));
    $end = date("Y-m-d",mktime(0,0,0,date('m')+1,-1,date('Y')));
}

$info = $lpayment->returnSinglePurchaseStatsByUserType($start,$end,$user_type);
$x = "";
for ($i=0; $i<sizeof($info); $i++)
{
     list($item, $sum, $count) = $info[$i];
     $sum = $lpayment->getWebDisplayAmountFormat($sum);
     $css = ($i%2? "":"2");

     $x .= "<tr class=tableContent$css>\n";
     $x .= "<td><a href=detail.php?user_type=$user_type&item=".urlencode($item)."&start=".urlencode($start)."&end=".urlencode($end).">$item</a></td>\n";
     $x .= "<td>$sum</td>\n";
     $x .= "<td>$count</td>\n";
     $x .= "</tr>\n";
}
if(sizeof($info)<=0){
	$x .="<tr class=tableContent2><td colspan=3 align=center>$i_no_record_exists_msg</td></tr>";
}

$select_user = "<SELECT name='user_type'>";
$select_user.= "<OPTION value='2'".($user_type==2?"SELECTED":"").">$i_identity_student</OPTION>";
$select_user.= "<OPTION value='1' ".($user_type==1?"SELECTED":"").">$i_identity_teachstaff</OPTION>";
$select_user.= "<OPTION value='' ".($user_type==""?"SELECTED":"").">-- $i_Payment_All --</OPTION>";
$select_user.= "</SELECT>&nbsp;&nbsp;";

#$toolbar = "$i_From <input type=text name=start value='$start' size=10> $i_To <input type=text name=end value='$end' size=10> (YYYY-MM-DD) <input type=image src='/images/admin/button/s_btn_find_$intranet_session_language.gif' align='absmiddle'>";
$toolbar = "<table border=0 widith=60% cellpadding=1 cellspacing=5>
	<tr><td align=right>$i_From:</td><td><input type=text name=start value='$start' size=10> (YYYY-MM-DD)</td></tr>
	<tr><td align=right>$i_To:</td><td><input type=text name=end value='$end' size=10> (YYYY-MM-DD)</td></tr>
	<tr><td align=right>$i_Payment_UserType:</td><td>$select_user</td></tr>
	<tr><td></td><Td ><a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_find_$intranet_session_language.gif' align='absmiddle' border=0></a></td></tr>
</table>";
$toolbar2 = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_PurchasingRecords,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<SCRIPT LANGUAGE=Javascript>
function openPrintPage()
{
         newWindow('index_print.php?user_type=<?=$user_type?>&start=<?=$start?>&end=<?=$end?>',6);
}
</SCRIPT>
<form name=form1 ACTION="" method=get>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?=$toolbar?></td></tr>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $toolbar2; ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td>
<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>
<tr>
<td class=tableTitle><?=$i_Payment_Field_SinglePurchase_Unit?></td>
<td class=tableTitle><?=$i_Payment_Field_SinglePurchase_TotalAmount?></td>
<td class=tableTitle><?=$i_Payment_Field_SinglePurchase_Count?></td>
</tr>
<?=$x?>
</table>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>


</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
