<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$RecordID = (is_array($RecordID)? $RecordID[0]:$RecordID);

$li = new libdb();

$lpayment = new libpayment();

$sql = "SELECT PPSAccount, Amount, InputTime, MappedStudentID,PPSType FROM PAYMENT_MISSED_PPS_BILL WHERE RecordID = $RecordID";
$result = $li->returnArray($sql,5);
list($ppsaccount, $amount, $inputtime,$studentid, $PPSType) = $result[0];

if ($studentid != "")
{
    header("Location: index.php?mapped=1");
    exit();
}

include_once("../../../../templates/adminheader_setting.php");
if (!isset($searchlength) || intval($searchlength)==0 )
{
     $searchlength = 4;
}

# Get substrings
$stringlength = strlen($ppsaccount);
if ($searchlength > $stringlength) $searchlength = $stringlength;
for ($i=0; $i<$stringlength-$searchlength+1; $i++)
{
     $temp  = substr($ppsaccount, $i, $searchlength);
     $substrings[] = $temp;
}
if ($searchlength == $stringlength)
{
    $substrings[] = $ppsaccount;
}

# Search
$namefield = getNameFieldByLang("b.");
$conds = "";
$delim = "";
for ($i=0; $i<sizeof($substrings); $i++)
{
     $conds .= "$delim a.PPSAccountNo LIKE '%".$substrings[$i]."%' ";
     $delim = " OR ";
}
/*
$sql = "SELECT a.StudentID, a.PPSAccountNo, a.Balance, $namefield, b.ClassName, b.ClassNumber
               FROM PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2
               WHERE ($conds)
               ";
*/
$sql = "SELECT a.StudentID, a.PPSAccountNo, a.Balance, $namefield, b.ClassName, b.ClassNumber
               FROM PAYMENT_ACCOUNT as a INNER JOIN INTRANET_USER as b ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus IN (0,1,2)
               WHERE ($conds)
               ";
$result = $li->returnArray($sql,6);

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_MissedPPSBill,'index.php',$i_Payment_action_handle,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<form name=form1 action="" method=POST>
<blockquote>
<?=$i_Payment_Field_RecordedAccountNo?> : <?=$ppsaccount?><br>
<?=$i_Payment_Field_CreditAmount?> : <?=$lpayment->getWebDisplayAmountFormat($amount)?><br>
<?=$i_Payment_Field_TransactionTime?> : <?=$inputtime?><br>
<?
# Check previously stored value of ChargeDeduction
$file_content = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));
$temp = explode("\n",$file_content);
$pps_charge_from_file = $temp[0];

if ($pps_charge_from_file==1)
{
    if ($PPSType==1)
    {
        $handling_charge = LIBPAYMENT_PPS_CHARGE_COUNTER_BILL;
    }
    else
    {
        $handling_charge = LIBPAYMENT_PPS_CHARGE;
    }
?>
<font color=red><?=$i_Payment_PPS_Charge?>: <?=$lpayment->getWebDisplayAmountFormat($handling_charge)?></font><br>
<?  } ?>

<?=$i_Payment_SearchResult?><br>
<table width=80% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>
<?
if (sizeof($result)!=0)
{
?>
  <tr class=tableTitle>
    <td><?=$i_Payment_Field_PPSAccountNo?></td>
    <td><?=$i_UserStudentName?></td>
    <td><?=$i_UserClassName?></td>
    <td><?=$i_UserClassNumber?></td>
    <td><?=$i_Payment_Field_Balance?></td>
    <td>&nbsp;</td>
  </tr>
<?
    for ($i=0; $i<sizeof($result); $i++)
    {
         list($res_sid, $res_pps, $res_balance, $res_name, $res_class, $res_classnum) = $result[$i];
         ?>
  <tr>
    <td><?=$res_pps?></td>
    <td><?=$res_name?></td>
    <td><?=$res_class?></td>
    <td><?=$res_classnum?></td>
    <td><?=number_format($res_balance,1)?></td>
    <td><input type=radio name=TargetStudentID value='<?=$res_sid?>' <?=($i==0?"CHECKED":"")?>></td>
  </tr>
         <?
    }
}
else
{
?>
<tr><td align=center><?=$i_no_search_result_msg?></td></tr>
<?
}
?>
</table>
<?
if (sizeof($result)!=0)
{
?>
<table width=80% border=0 cellpadding=2 cellspacing=0>
<tr><td align=right><input type=image src="<?=$image_path?>/admin/button/s_btn_select_std_<?=$intranet_session_language?>.gif" onClick="JavaScript: AlertPost(document.form1, 'edit_update.php', '<?=$i_Payment_UpdateRecordConfirm?>')"></td></tr>
</table>
<?
}
?>
<br>
<hr width=80%>
<a class=functionlink_new href="?RecordID=<?=$RecordID?>&searchlength=<?=($searchlength-1)?>"><?=$i_Payment_IncreaseSensitivity?></a><br>
<a class=functionlink_new href="?RecordID=<?=$RecordID?>&searchlength=<?=($searchlength+1)?>"><?=$i_Payment_DecreaseSensitivity?></a><br>
<a class=functionlink_new href="manualselect.php?RecordID=<?=$RecordID?>"><?=$i_Payment_ManualSelectStudent?></a><br>
</blockquote>
<input type=hidden name=RecordID value="<?=$RecordID?>">
</form>
<?
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
