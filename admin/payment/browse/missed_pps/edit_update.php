<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();

if($TargetStudentID != "")
{
        $sql = "SELECT UserID FROM INTRANET_USER WHERE UserID = $TargetStudentID";
        $result = $li->returnArray($sql,1);
        if(sizeof($result)==0)
        {
                header("location: index.php?mapped=1");
                exit();
        }
}

# Check previously stored value of ChargeDeduction
$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));
$temp = explode("\n",$pps_charge_from_file);
$pps_auto_charge = ($temp[0]==1);

$sql = "SELECT PPSAccount, Amount, InputTime, MappedStudentID, PPSType FROM PAYMENT_MISSED_PPS_BILL WHERE RecordID = $RecordID";
$result = $li->returnArray($sql,5);
list($ppsaccount, $amount, $inputtime,$studentid, $PPSType) = $result[0];
$PPSType += 0;
if ($PPSType==1)
{
    $handling_charge = LIBPAYMENT_PPS_CHARGE_COUNTER_BILL;
}
else
{
    $handling_charge = LIBPAYMENT_PPS_CHARGE;
}

if ($studentid != "")
{
    header("Location: index.php?mapped=1");
    exit();
}

$sql = "UPDATE PAYMENT_MISSED_PPS_BILL set MappedStudentID = '$TargetStudentID' WHERE RecordID = '$RecordID'";
$li->db_db_query($sql);

$sql = "SELECT PPSAccountNo FROM PAYMENT_ACCOUNT WHERE StudentID = '$TargetStudentID'";
$temp = $li->returnVector($sql);
$actual_pps = $temp[0];

# RecordType: 1 - PPS
# RecordStatus: 0 - Completed , 1 - Not completed
$sql = "INSERT INTO PAYMENT_CREDIT_TRANSACTION (StudentID, Amount, RecordType, RecordStatus, PPSAccountNo, TransactionTime, DateInput, PPSType)
               VALUES ('$TargetStudentID','$amount',1,0,'$actual_pps','$inputtime',now(), '$PPSType')";
$li->db_db_query($sql);
$trans_id = $li->db_insert_id();

# Update Ref Code and Admin Name
$sql = "UPDATE PAYMENT_CREDIT_TRANSACTION SET
        RefCode = CONCAT('PPS',TransactionID), AdminInCharge = '$PHP_AUTH_USER'
        WHERE TransactionID = $trans_id";
$li->db_db_query($sql);

$sql = "SELECT Balance FROM PAYMENT_ACCOUNT WHERE StudentID = $TargetStudentID";
$temp = $li->returnVector($sql);
$balanceAfter = $temp[0]+$amount;
if ($pps_auto_charge)
{
    $balanceAfter2 = $balanceAfter - $handling_charge;
    $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount - $handling_charge,
               LastUpdateByAdmin = '$PHP_AUTH_USER',
               LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = $TargetStudentID";
    $li->db_db_query($sql);

    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ($TargetStudentID, 1, '$amount','$trans_id','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','PPS$trans_id')";
    $li->db_db_query($sql);

    # PPS Charge
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ($TargetStudentID, 8, '$handling_charge','$trans_id','$balanceAfter2',now(),'$i_Payment_PPS_Charge','')";
    $li->db_db_query($sql);

}
else
{
    $sql = "UPDATE PAYMENT_ACCOUNT SET Balance = Balance + $amount,
               LastUpdateByAdmin = '$PHP_AUTH_USER',
               LastUpdateByTerminal= '', LastUpdated=now() WHERE StudentID = $TargetStudentID";
    $li->db_db_query($sql);
    $sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ($TargetStudentID, 1, '$amount','$trans_id','$balanceAfter',now(),'$i_Payment_TransactionDetailPPS','PPS$trans_id')";
    $li->db_db_query($sql);
}


/*
$sql = "INSERT INTO PAYMENT_OVERALL_TRANSACTION_LOG
               (StudentID,TransactionType,Amount,RelatedTransactionID,BalanceAfter,
                   TransactionTime,Details,RefCode) VALUES
               ($TargetStudentID, 1, '$amount','$trans_id','$balanceAfter','$inputtime','$i_Payment_TransactionDetailPPS','PPS$trans_id')";
*/

header ("Location: index.php?msg=2");
intranet_closedb();
?>
