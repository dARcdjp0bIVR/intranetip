<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libattendance.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$now = time();
$today = date('Y-m-d',$now);
$ts_monthstart = mktime(0,0,0,date('m',$now)-1,1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now),1,date('Y',$now));

$start = date('Y-m-d',$ts_monthstart);
$end = date('Y-m-d',$ts_monthend);

$functionbar = "<table width=560 border=0 cellpadding=2 cellspacing=0 align='center'>\n";
$functionbar .= "<tr><td><input type='radio' name='search_by' value='0'  ".($search_by!=1?"checked":"").">$i_Payment_Search_By_PostTime&nbsp;&nbsp;<input type='radio' name='search_by' value='1' ".($search_by==1?"checked":"").">$i_Payment_Search_By_TransactionTime</td></tr>";
$functionbar .= "<tr><td>$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To ";
$functionbar .= "<input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) </span></td></tr>";
$functionbar .= "</table>\n";

//echo $functionbar;

$select_1 = "<input type='checkbox' name=status1 value='1' checked> $i_Payment_PPS_Export_Choices_1";
$select_2 = "<input type='checkbox' name=status2 value='1'> $i_Payment_PPS_Export_Choices_2";
$select_3 = "<input type='checkbox' name=status3 value='1'> $i_Payment_PPS_Export_Choices_3";


?>


<script language='javascript'>
function checkform(){
	obj = document.form1;
	objStatus1 = obj.status1;
	objStatus2 = obj.status2;
	objStatus3 = obj.status3;
	
	if(objStatus1.checked || objStatus2.checked || objStatus3.checked)
		obj.submit();
	else alert('<?=$i_Payment_PPS_Export_Warning?>');
	
	
}
</script>
<form name="form1" method="get" action="pps_export.php">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '/admin/payment/', $i_Payment_Menu_DataBrowsing, '../', $i_Payment_Menu_Browse_CreditTransaction, 'index.php',$button_export_pps,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=5 align="center">
<tr><td><?=$functionbar?></td></tr>
<tr><td><?=$i_Payment_PPS_Export_Select_Student?>:</td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$select_1?></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$select_2?></td></tr>
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?=$select_3?></td></tr>
<tr><td><?=$i_Payment_Note_PPSCostExport?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=5 align="center">
<tr><td><div><a href='javascript:checkform()'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></div></td></tr>
</table>
<?php
include_once("../../../../templates/adminfooter.php");

?>