<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$lpayment = new libpayment();

$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;

if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3 && $RecordStatus != 4) $RecordStatus = 1;

$namefield = getNameFieldByLang2("b.");
$conds = "";
if ($ClassName != "")
{
    $conds .= "AND b.ClassName = '$ClassName'";
}


switch($user_type){
	case 1: $user_cond = " AND b.RecordType=1"; break;
	case 2: $user_cond = " AND b.RecordType=2"; break;
	default : $user_cond = " AND b.RecordType IN (1,2) "; break;
}

####### get total outstanding balance of the target users ##
$year_left_cond = "";
if($RecordStatus == 3 || $RecordStatus == 4) {
	if($YearOfLeft != "") {
		$year_left_cond = " AND b.YearOfLeft = '$YearOfLeft' ";
	}		
}

if($RecordStatus != 4){
	$sql = "SELECT COUNT(*),SUM(a.Balance) FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN INTRANET_USER AS b ON a.StudentID = b.UserID
	        WHERE
	              b.RecordStatus = $RecordStatus  $user_cond $conds AND
	              (
	               b.EnglishName LIKE '%$keyword%' OR
	               b.ChineseName LIKE '%$keyword%' OR
	               b.ClassName LIKE '%$keyword%' OR
	               b.ClassNumber LIKE '%$keyword%'
	              )
	              $year_left_cond
	        ";
} else {
	$sql = "SELECT COUNT(*),SUM(a.Balance) FROM INTRANET_ARCHIVE_USER AS b LEFT OUTER JOIN PAYMENT_ACCOUNT AS a ON a.StudentID = b.UserID
	        WHERE
	              (
	               b.EnglishName LIKE '%$keyword%' OR
	               b.ChineseName LIKE '%$keyword%' OR
	               b.ClassName LIKE '%$keyword%' OR
	               b.ClassNumber LIKE '%$keyword%'
	              )
	              $year_left_cond
	              $user_cond $conds
	        ";
}
$ldb = new libdb();
$temp = $ldb->returnArray($sql,2);
if(sizeof($temp)>0){
	$no_of_users = $temp[0][0];
	$total_balance = $temp[0][1];
}else{
	$no_of_users = 0;
	$total_balance = 0;
}
$infobar = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br><br>\n";
$infobar .= "$i_Payment_Total_Users: $no_of_users<br><Br>\n";
$infobar .= "$i_Payment_Total_Balance: ".$lpayment->getWebDisplayAmountFormat($total_balance)."<br>";


### list users
if($RecordStatus != 4){
	$sql  = "SELECT
	               CONCAT('<a href=view.php?StudentID=',a.StudentID,'&RecordStatus=',$RecordStatus,'>',$namefield,'</a>')
	               , b.ClassName, b.ClassNumber,
	               IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."),
	               IF(a.LastUpdated IS NULL,'&nbsp',DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i')),
	               IF(
	                  a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
	                     CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
	                     IF(
	                        a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
	                        CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
	                        '$i_Payment_Credit_TypeUnknown'
	                     )
	               ),
	               CONCAT('<input type=checkbox name=StudentID[] value=', a.StudentID ,'>')
	         FROM
	             PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
	         WHERE
	              b.RecordStatus = $RecordStatus  $user_cond $conds AND
	              (
	               b.EnglishName LIKE '%$keyword%' OR
	               b.ChineseName LIKE '%$keyword%' OR
	               b.ClassName LIKE '%$keyword%' OR
	               b.ClassNumber LIKE '%$keyword%'
	              )
	              $year_left_cond
	                ";
} else {
	$sql  = "SELECT
	               CONCAT('<a href=view.php?StudentID=',b.UserID,'&RecordStatus=',$RecordStatus,'>',$namefield,'</a>')
	               , b.ClassName, b.ClassNumber,
	               IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."),
	               IF(a.LastUpdated IS NULL,'&nbsp',DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i')),
	               IF(
	                  a.LastUpdateByAdmin IS NOT NULL AND a.LastUpdateByAdmin != '',
	                     CONCAT(a.LastUpdateByAdmin,' [$i_Payment_Field_AdminHelper]'),
	                     IF(
	                        a.LastUpdateByTerminal IS NOT NULL AND a.LastUpdateByTerminal != '',
	                        CONCAT(a.LastUpdateByTerminal,' [$i_Payment_Field_TerminalUser]'),
	                        '$i_Payment_Credit_TypeUnknown'
	                     )
	               ),
	               CONCAT('')
	         FROM
	             INTRANET_ARCHIVE_USER as b LEFT OUTER JOIN PAYMENT_ACCOUNT as a ON a.StudentID = b.UserID
	         WHERE
	              (
	               b.EnglishName LIKE '%$keyword%' OR
	               b.ChineseName LIKE '%$keyword%' OR
	               b.ClassName LIKE '%$keyword%' OR
	               b.ClassNumber LIKE '%$keyword%'
	              )
	              $year_left_cond
	              $user_cond $conds
	                ";
}
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("b.EnglishName","b.ClassName","b.ClassNumber","a.Balance","a.LastUpdated");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Balance)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Payment_Field_LastUpdated)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>". $i_Payment_Field_LastUpdatedBy."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";

$select_user = "<SELECT name='user_type' onChange='filterByUser(this.form)'>";
$select_user.= "<OPTION value='2'".($user_type==2?"SELECTED":"").">$i_identity_student</OPTION>";
$select_user.= "<OPTION value='1' ".($user_type==1?"SELECTED":"").">$i_identity_teachstaff</OPTION>";
$select_user.= "<OPTION value='' ".($user_type==""?"SELECTED":"").">$i_Payment_All</OPTION>";
$select_user.= "</SELECT>&nbsp;&nbsp;";

$select_status = "<SELECT name=RecordStatus onChange=this.form.submit()>\n";
$select_status .= "<OPTION value=0 ".($RecordStatus==0?" SELECTED":"").">$i_status_suspended</OPTION>\n";
$select_status .= "<OPTION value=1 ".($RecordStatus==1?" SELECTED":"").">$i_status_approved</OPTION>\n";
$select_status .= "<OPTION value=2 ".($RecordStatus==2?" SELECTED":"").">$i_status_pendinguser</OPTION>\n";
$select_status .= "<OPTION value=3 ".($RecordStatus==3?" SELECTED":"").">$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp</OPTION>\n";
$select_status .= "<OPTION value=4 ".($RecordStatus==4?" SELECTED":"").">$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived</OPTION>\n";
$select_status .= "</SELECT>\n";
$function_refund .= "<a href=\"javascript:checkAlert(document.form1,'StudentID[]','refund.php','$i_Payment_alert_refund')\"><img src='/images/admin/button/t_btn_refund_$intranet_session_language.gif' border='0' align='absmiddle' alt='$i_Payment_action_refund'></a>";
$function_refund .= "<a href=\"javascript:checkAlert(document.form1,'StudentID[]','donate.php','".$Lang['Payment']['DonateBalanceWarning']."')\"><img src='/images/admin/button/s_btn_donate_to_school_$intranet_session_language.gif' border='0' align='absmiddle' alt='".$Lang['Payment']['DonateBalance']."'></a>";

#################################################
#			added on 20080821 by Ronald			#
#-----------------------------------------------#
# 	Drop down box to select school year left	#
#	Only show when select "LEFT"				#
#################################################
if($RecordStatus != 4){
	$sql = "SELECT DISTINCT YearOfLeft FROM INTRANET_USER WHERE YearOfLeft IS NOT NULL AND YearOfLeft != '' ORDER BY YearOfLeft";
}else{
	$sql = "SELECT DISTINCT YearOfLeft FROM INTRANET_ARCHIVE_USER WHERE YearOfLeft IS NOT NULL AND YearOfLeft != '' ORDER BY YearOfLeft";
}
$arr_year_left = $lpayment->returnVector($sql);

$select_year_left .= "<SELECT name=\"YearOfLeft\" onChange=\"this.form.submit()\">\n";
$empty_selected = ($selected == '')? "SELECTED":"";
$select_year_left .= "<OPTION value='' $empty_selected>-- $i_Profile_DataLeftYear --</OPTION>\n";
if(sizeof($arr_year_left)>0) {
	for($i=0; $i<sizeof($arr_year_left); $i++) {
		$name = $arr_year_left[$i];
		$sel_str = ($YearOfLeft == $name? "SELECTED":"");
		$select_year_left .= "<OPTION value='$name' $sel_str> $name </OPTION>\n";
	}
}
$select_year_left .= "</SELECT>\n";

$lclass = new libclass();
$temp = $button_select;
$button_select = $i_status_all;
$select_class = $lclass->getSelectClass("name=ClassName onChange='filterByClass(this.form)'",$ClassName);
$button_select = $temp;

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$functionbar = "$function_refund";
$searchbar = $select_user.$select_class;
$searchbar .= "$select_status";
if($RecordStatus == 3 || $RecordStatus == 4) {
	$searchbar .= $select_year_left;
}
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

if ($refund == 1)
{
    if ($count > 0)
    {
        $xmsg = "<font color=green>$count $i_Payment_Students$i_Payment_Refunded</font>";
    }
    else
    {
        $xmsg = "$i_Payment_con_RefundFailed";
    }
}

if ($donate == 1) {
	if ($count > 0) {
		$xmsg = "<font color=green>$count $i_Payment_Students ".$Lang['Payment']['Donated']."</font>";
	}
	else {
		$xmsg = $Lang['Payment']['DonateFailed'];
	}
}
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_DataBrowsing,'../',$i_Payment_Menu_Browse_StudentBalance,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<SCRIPT LANGUAGE=Javascript>
function openPrintPage()
{
         newWindow('list_print.php?user_type=<?=$user_type?>&ClassName=<?=$ClassName?>&RecordStatus=<?=$RecordStatus?>&keyword=<?=$keyword?>',6);
}
function filterByUser(formObj){
	if(formObj==null) return;
	classNameObj = formObj.ClassName;
	userTypeObj = formObj.user_type;
	if(classNameObj==null || userTypeObj ==null) return;
	v = userTypeObj.options[userTypeObj.selectedIndex].value;
	if(v=='' || v==1)
		classNameObj.options[0].selected = true;
	formObj.submit();
}
function filterByClass(formObj){
	if(formObj==null) return;
	classNameObj = formObj.ClassName;
	userTypeObj = formObj.user_type;
	if(classNameObj==null || userTypeObj ==null) return;
	v = classNameObj.options[userTypeObj.selectedIndex].value;
	if(v!='')
		userTypeObj.options[0].selected = true;
	formObj.submit();
}

</SCRIPT>
<form name="form1" method="get">
<!-- summary -->
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $infobar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
<Tr><td><?=$i_Payment_Refund_Warning?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
