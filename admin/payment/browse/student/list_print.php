<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");
intranet_opendb();

$lpayment = new libpayment();

if ($RecordStatus == "") $RecordStatus = 1;
if ($RecordStatus != 0 && $RecordStatus != 2 && $RecordStatus != 3) $RecordStatus = 1;

$namefield = getNameFieldByLang("b.");
$conds = "";
if ($ClassName != "")
{
    $conds .= "AND b.ClassName = '$ClassName'";
}

switch($user_type){
	case 1: $user_cond = " AND b.RecordType=1"; break;
	case 2: $user_cond = " AND b.RecordType=2"; break;
	default : $user_cond = " AND b.RecordType IN (1,2) "; break;
}

####### get total outstanding balance of the target users ##
$sql = "SELECT COUNT(*),SUM(a.Balance) FROM PAYMENT_ACCOUNT AS a LEFT OUTER JOIN INTRANET_USER AS b ON a.StudentID = b.UserID
        WHERE
              b.RecordStatus = $RecordStatus  $user_cond $conds AND
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%'
              )
        ";
$ldb = new libdb();
$temp = $ldb->returnArray($sql,2);
if(sizeof($temp)>0){
	$no_of_users = $temp[0][0];
	$total_balance = $temp[0][1];
}else{
	$no_of_users = 0;
	$total_balance = 0;
}
$infobar = "<span class='$css_text'><u><b>$i_Payment_ItemSummary</b></u></span><br>\n";
$infobar .= "$i_Payment_Total_Users: $no_of_users<br>\n";
$infobar .= "$i_Payment_Total_Balance: ".$lpayment->getWebDisplayAmountFormat($total_balance)."<br>";

$sql  = "SELECT
               $namefield, b.ClassName, b.ClassNumber,
               IF(a.Balance>=0,".$lpayment->getWebDisplayAmountFormatDB("a.Balance").",".$lpayment->getWebDisplayAmountFormatDB("0")."),
               DATE_FORMAT(a.LastUpdated,'%Y-%m-%d %H:%i')
         FROM
             PAYMENT_ACCOUNT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
              b.RecordStatus = $RecordStatus  $user_cond $conds AND
              (
               b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%'
              )
         ORDER BY b.ClassName, b.ClassNumber, b.EnglishName
                ";
$li = new libdb();
$result = $li->returnArray($sql,5);

$display = "<table width=95% border=0  cellpadding=2 cellspacing=0 class='$css_table'>";
$display .= "<tr class='$css_table_title'><td class='$css_table_title'>$i_Payment_Field_Username</td><td class='$css_table_title'>$i_UserClassName</td><td class='$css_table_title'>$i_UserClassNumber</td><td class='$css_table_title'>$i_Payment_Field_Balance</td><td class='$css_table_title'>$i_Payment_Field_LastUpdated</td></tr>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     //$css = ($i%2? "":"2");
     	$css =$i%2==0?$css_table_content:$css_table_content."2";
     list($studentname, $classname, $classnum, $balance, $lastupdated) = $result[$i];
     if ($lastupdated == "") $lastupdated = "--";
     $display .= "<tr class='$css'><td class='$css'>$studentname&nbsp;</td><td class='$css'>$classname&nbsp;</td><td class='$css'>$classnum&nbsp;</td><td class='$css'>$balance</td><td class='$css'>$lastupdated</td></tr>\n";

}
if(sizeof($result)<=0){
	$display.="<tr class='$css_table_content'><td colspan=5 align=center  class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}

$display .= "</table>";
echo $infobar."<Br>";
echo $display;
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
