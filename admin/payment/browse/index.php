<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_DataBrowsing,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(
                  $i_Payment_Menu_Browse_TerminalLog,'terminal/',1,
                  $i_Payment_Menu_Browse_CreditTransaction,'credit/',1,
                  $i_Payment_Menu_Browse_StudentBalance,'student/',1,
                  $i_Payment_Menu_Browse_PurchasingRecords,'single_purchase/',1,
                  $i_Payment_Menu_Browse_MissedPPSBill , 'missed_pps/',1
                                ) ?>


</blockquote>
</td></tr>
</table>

<?
include_once("../../../templates/adminfooter.php");
?>