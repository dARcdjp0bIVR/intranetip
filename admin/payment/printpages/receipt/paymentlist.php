<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$button_select = $i_status_all;
$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.action='';this.form.target='';this.form.submit()",$ClassName);

?>

<form name="form1" method="GET" action="">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PrintPage,'../',$i_Payment_Menu_PrintPage_Receipt,'index.php',$i_Payment_Menu_PrintPage_Receipt_PaymentItemList,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><?=$select_class?></td></tr>
<? if ($ClassName != "") {
$select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID");
?>
<tr><td align=right nowrap><?php echo $i_UserStudentName; ?>:</td><td><?=$select_student?></td></tr>
<? } ?>
<tr><td align=right nowrap><?=$i_From?>:</td><td><input type=text name=date_from size=10 value=''>(YYYY-MM-DD)</td></tr>
<tr><td align=right nowrap><?=$i_To?>:</td><td><input type=text name=date_to size=10 value=''>(YYYY-MM-DD)</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" onClick="this.form.target='_blank';this.form.action='paymentlist_print.php'"></td></tr>

</table>
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
