<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();
$lclass = new libclass();

$usertype = array(
array(1,$i_identity_teachstaff),
array(2,$i_identity_student));

$teacherstatus = array(
array(1,$i_status_suspended),
array(2,$i_status_approved),
array(3,$i_status_pendinguser));

$studentstatus = array(
array(1,$i_status_suspended),
array(2,$i_status_approved),
array(3,$i_status_pendinguser),
array(4,$i_status_graduate));

$format_array = array(
array(0,"Web"),
array(1,"CSV"));

$UserType_Selection = getSelectByArray($usertype,"name=UserType onChange=\"this.form.flag.value=0; javascript:checkForm();\"",$UserType,1,0,0);

if($UserType==2)
{
	$class_list = $lclass->getClassList();
	$Class_Selection = "<tr><td align=right>$i_ClassName:</td>";
	$Class_Selection .= "<td>".getSelectByArray($class_list,'name=TargetClass',0,1,0,0)."</td></tr>";
}

if($UserType==1)
	$UserStatus_Selection = "<input type=checkbox name=UserStatus[] value=0>$i_status_suspended <input type=checkbox name=UserStatus[] value=1>$i_status_approved <input type=checkbox name=UserStatus[] value=2>$i_status_pendinguser";
else
	$UserStatus_Selection = "<input type=checkbox name=UserStatus[] value=0>$i_status_suspended <input type=checkbox name=UserStatus[] value=1>$i_status_approved <input type=checkbox name=UserStatus[] value=2>$i_status_pendinguser <input type=checkbox name=UserStatus[] value=3>$i_status_graduate";
$File_Type = getSelectByArray($format_array,"name=FileFormat",0,0,1);

?>

<SCRIPT Language="JavaScript">
function checkForm()
{
	var obj1 = document.form1.UserType;
	var obj2 = this.form1.elements["UserStatus[]"];
	var checking1 = 0;
	
	for(j=0; j<obj2.length; j++)
		if(obj2[j].checked)
			checking1 = 1;
			
	if(document.form1.flag.value == 2)
	{
		if(checking1 == 1)
		{
			document.form1.target = "_blank";
			if(document.form1.FileFormat.value==0)
			{
				document.form1.action='web_print.php';
				return true;
			}
			else
			{
				document.form1.action='csv_print.php';
				return true;
			}
		}
		else
		{
			alert("<?=$i_Payment_PrintPage_Browse_UserStatusWarning?>");
			return false;
		}
	}
	if((document.form1.flag.value == 0)||(document.form1.flag.value == 1))
	{
		document.form1.target = '';
		document.form1.action='';
		document.form1.submit();
	}
}
</SCRIPT>

<?=displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PrintPage,'../',$i_Payment_Menu_Browse_TransactionRecord,'')?>
<?=displayTag("head_payment_$intranet_session_language.gif", $msg)?>

<form name=form1 action="" method="post">
<table width=400 border=0 align=center>
<tr><td align=right><?=$i_identity?>:</td><td><?=$UserType_Selection?></td></tr>
<tr><td align=right></td><td><?=$Class_Selection?></td></tr>
<tr><td align=right><?=$i_general_status?>:</td><td><?=$UserStatus_Selection?></td></tr>
<tr><td align=right><?=$i_general_Format?>:</td><td><?=$File_Type?></td></tr>
</table>

<table width=560 border=0 align=center>
<tr><td height=10px></td></tr>
<tr><td colspan=2><hr size=1></td></tr>
</table>

<table width=340 border=0 align=center>
<tr><td colspan=2 align=right><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" onClick="document.form1.flag.value=2; javascript:checkForm();"><?=" ".btnReset()?></td></tr>
</table>
<input type=hidden name=flag value=0>
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>