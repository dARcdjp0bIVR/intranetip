<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libsmsv2.php");
include_once("../../../lang/lang.$intranet_session_language.php");

if(!($plugin['sms'] and $bl_sms_version >= 2))
{
		header("Location: /admin/");
		exit;
}

include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$sql="SELECT a.ClassName,b.LevelName,b.ClassLevelID FROM INTRANET_CLASS AS a LEFT OUTER JOIN INTRANET_CLASSLEVEL AS b ON (a.ClassLevelID = b.ClassLevelID) WHERE a.RecordStatus=1 ORDER BY b.ClassLevelID,a.ClassName ";
$temp = $lclass->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($name,$level,$levelid)=$temp[$i];
	if($levelid=="")
		$no_level[] = $name;
		
	else{
		$result[$levelid]['name']=$level;
		$result[$levelid]['class'][]=$name;
	}
}

$table = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$table.="<tr><td class='tableTitle'><b>$i_ClassLevel</b></td><td class='tableTitle'><B>$i_ClassName</b></td></tr>";
if(sizeof($result)>0){
	foreach($result as $key =>$values){
		$level_name = $values['name'];
		$classes = $values['class'];
		$table.="<tr><Td><input type=checkbox name='LevelID[]' value='$key' onClick='selectByClassLevel(this)'> $level_name</td><td>";
		for($i=0;$i<sizeof($classes);$i++){
			$class_name = $classes[$i];
			$table.="<input type=checkbox name='ClassID[]' value='$class_name' id='ClassID_".$key."_".$i."'> $class_name&nbsp;&nbsp;";
		}
		$table.="<input type=hidden name='$key' value='$i'>";
		$table.="</td></tr>";
	}
}
if(sizeof($no_level)>0){
	$table.="<tr><td>&nbsp;</td><td>";
	for($i=0;$i<sizeof($no_level);$i++){
		$name = $no_level[$i];
		$table.="<input type=checkbox name='ClassID[]' value='$name'> $name";
	}
	$table.="</td></tr>";
}
$table.="</table>";

$lsms = new libsmsv2();
$TemplateCode = array("STUDENT_MAKE_PAYMENT","BALANCE_REMINDER");
$select_template = "<SELECT name=\"TemplateCode\" onChange=\"toggle();this.form.Message.value=this.value; this.form.Message.focus()\">\n";
$select_template .= "<OPTION value=''> -- $button_select $i_SMS_MessageTemplate -- </OPTION>\n";
for($i=0; $i<sizeof($TemplateCode); $i++)
{
	$content = $lsms->returnSystemMsg($TemplateCode[$i]);
	$status = $lsms->returnTemplateStatus("",$TemplateCode[$i]);
	if($content!="" and $status)
		$select_template .= "<OPTION value='". $lsms->returnSystemMsg($TemplateCode[$i]) ."'>". $lsms->returnTemplateName($TemplateCode[$i]) ."</OPTION>\n";
}
$select_template .= "</SELECT>\n";


if ($sent == 1)
{
    $xmsg = "$i_SMS_MessageSent";
}


?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_SMS_Notification,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<script language='javascript'>
function toggle()
{
	with(document.form1)
	{
		if(TemplateCode.options[TemplateCode.selectedIndex].text == '<?=$lsms->returnTemplateName("BALANCE_REMINDER")?>')
			bal_tr.style.display = "inline";
		else
			bal_tr.style.display = "none";
	}
	
}
function selectByClassLevel(obj){
	classid = obj.value;
	check = obj.checked;
	objLevels = document.getElementsByName(classid);
	objLevel = objLevels[0];
	no_of_class = objLevel.value;
		
	for(i=0;i<no_of_class;i++){
		obj = document.getElementById('ClassID_'+classid+'_'+i);
		if(typeof(obj)!='undefined')
			obj.checked = check;
	}
	
}
function checkform(obj)
{
	class_flag = false;
	classes = document.getElementsByName('ClassID[]');
	for(i=0;i<classes.length;i++){
		if(classes[i].checked) class_flag = true;
	}
	if(!class_flag)	{ alert("<?=$i_StudentGuardian_warning_PleaseSelectClass?>"); return false;}
	
	if (!check_text(obj.Message,'<?=$i_SMS_AlertMessageContent?>')) return false;
	
	if(bal_tr.style.display=="inline")
	{
		if (!check_text(obj.bal_less_than,'<?=$i_Homework_Error_Empty?>')) return false;
		if (!check_positive_int(obj.bal_less_than,'<?=$i_Payment_Warning_InvalidAmount?>')) return false;
		
		obj.check_bal.value=1;
	}

}
function selectAll(obj){
	classes = document.getElementsByName('ClassID[]');
	levels = document.getElementsByName('LevelID[]');
	check = obj.checked;
	for(i=0;i<classes.length;i++){
		classes[i].checked = check;
	}
	for(i=0;i<levels.length;i++){
		levels[i].checked = check;
	}

	
}
</script>
<form name=form1 method='post' action='list.php' onsubmit='return checkform(this)'>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><input type=checkbox name='all' value=1 onClick='selectAll(this)'> <?=$i_StudentGuardian_all_students?></td></tr>
<tr><td><?=$table?></td></tr>

<tr><td>&nbsp;</td></tr>

<tr>
	<td>
		<table>
		<tr>
			<td width="120"><?=$i_SMS_MessageContent?>: </td>
			<td><?=$select_template?></td>
		</tr>
		</tr>
			<td>&nbsp;</td>
			<td><TEXTAREA NAME="Message" COLS="40" ROWS="4" WRAP="virtual"></TEXTAREA><FONT>&nbsp;</FONT></td>
		</tr>	
		<tr id="bal_tr" style="display: none">
			<td><?=$i_SMS_Balance_Lass_Than?></td> 
			<td>$<input type="text" name="bal_less_than" value=""></td>
		</tr>
		</table>
	</td>
</tr>	

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="<?=$image_path?>/admin/button/s_btn_find_<?=$intranet_session_language?>.gif" border='0'>
<a href='javascript:document.form1.reset()'><img src='<?=$image_path?>/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a></td>
</tr>
</table>

<input type="hidden" name="check_bal" value="">
</form>
<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>