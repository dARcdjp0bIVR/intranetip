<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
intranet_opendb();


if(sizeof($PaymentID)<=0){
	header("Location: list.php?ItemID".$ItemID);
}

$li = new libdb();

$list = implode(",",$PaymentID);
$sql="SELECT PaymentID,StudentID,Amount,SubsidyAmount FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ($list)";
$temp = $li->returnArray($sql,4);

$lock_sql="LOCK TABLES PAYMENT_PAYMENT_ITEMSTUDENT as WRITE";
$li->db_db_query($lock_sql);

for($i=0;$i<sizeof($temp);$i++){
	list($payment_id,$sid,$old_amount,$old_sub_amount) = $temp[$i];
	$old_sub_amount+=0;
	$old_amount+=0;
	$amount = round(trim(${'new_amount_'.$sid}),2);
	
	$sub = round(trim(${'new_subsidy_amount_'.$sid}),2);
	$unit_id=trim(${'unitid_'.$sid});
	
	$sql=" UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET ";
	$sql.=" Amount='$amount' ";
	$sql.=" ,RecordType=1 ";
	$sql.=" ,ProcessingTerminalUser=NULL ";
	$sql.=" ,ProcessingTerminalIP=NULL ";
	$sql.=" ,ProcessingAdminUser='$PHP_AUTH_USER' ";
	$sql.=" ,DateModified=NOW() ";
	if($unit_id!="")
		$sql.=" ,SubsidyUnitID='$unit_id' ";
	else $sql.=" ,SubsidyUnitID=NULL ";

	if($sub!=$old_sub_amount){
		$sql.=" ,SubsidyPICAdmin='$PHP_AUTH_USER' ";
		$sql.=" ,SubsidyAmount ='$sub' ";
	}
	
	$sql.=" WHERE PaymentID = $payment_id AND RecordStatus = 0";

	$li->db_db_query($sql);
}
$unlock_sql="UNLOCK TABLES ";
$li->db_db_query($unlock_sql);

/*
$amount = trim($amount);

if ($amount != "")
{
    $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount = '$amount', RecordType = 1, ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='$PHP_AUTH_USER', DateModified=NOW() WHERE PaymentID = $PaymentID AND RecordStatus = 0";
}
$li->db_db_query($sql);
*/
header ("Location: list.php?ItemID=$ItemID&ClassName=$ClassName&pageNo=$pageNo&order=$order&field=$field&msg=2");
intranet_closedb();
?>
