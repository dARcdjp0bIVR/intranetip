<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

if($ItemID=="" || sizeof($ItemID)<=0)
	header("Location: index.php");

include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();



$li = new libdb();

$list = implode(",",$ItemID);

/*
$sql = "DELETE FROM PAYMENT_PAYMENT_ITEM WHERE ItemID IN ($list)";
$li->db_db_query($sql);

$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ($list)";
$li->db_db_query($sql);
*/

$allow_delete = true;

$sql ="SELECT ItemID,RecordStatus FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ($list) ORDER BY ItemID";
$temp = $li->returnArray($sql,2);
for($i=0;$i<sizeof($temp);$i++){
	list($itemid,$isPaid) = $temp[$i];
	
	if($data[$itemid]['paid']=="")
		$data[$itemid]['paid']=0;
	if($data[$itemid]['unpaid']=="")
		$data[$itemid]['unpaid']=0;
	
	if($isPaid==1){
		$data[$itemid]['paid'] +=1;
		$allow_delete = false;
	}
	else $data[$itemid]['unpaid'] +=1;

}

$sql=" 
		SELECT 
			a.ItemID,a.Name, b.Name,a.StartDate,a.EndDate 
		FROM 
			PAYMENT_PAYMENT_ITEM AS a  LEFT OUTER JOIN
			PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
	 	WHERE a.ItemID IN ($list)
	 ";
$temp = $li->returnArray($sql,5);

$display_table="<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$display_table.="<tr class='tableTitle'>
			<td>$i_Payment_Field_PaymentItem</td>
			<Td>$i_Payment_Field_PaymentCategory</td>
			<td>$i_Payment_PresetPaymentItem_PaidStudentCount</td>
			<Td>$i_Payment_PresetPaymentItem_UnpaidStudentCount</td></tr>";
	//		<td>$i_general_startdate</td>
	//		<td>$i_general_enddate</td>
	//		</tr>
	///";
for($i=0;$i<sizeof($temp);$i++){
	list($item_id,$item_name,$cat_name,$start_date,$end_date) = $temp[$i];
	$paid = $data[$item_id]['paid'];
	$unpaid=$data[$item_id]['unpaid'];
	
	$paid = $paid==""?0:$paid;
	$paid = $paid > 0? "<font color=red><b>$paid</b></font>":$paid;
	$unpaid = $unpaid==""?0:$unpaid;
	
	$css = $i%2==0?"tableContent":"tableContent2";
	$display_table.="<tr class='$css'>";
	$display_table.="<td>$item_name</td><td>$cat_name</td><td>$paid</td><td>$unpaid</td>";
	//<td>$start_date</td><td>$end_date</td>";
	$display_table.="</tr>";
}
$display_table.="</table>";

if($allow_delete){
	$del_btn = "<a href='javascript:submitform()'><img src='/images/admin/button/s_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
}
$cancel_btn = "<a href='index.php'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0' align='absmiddle'></a>";

$btn_table="<table width=560 border=0 align=center><Tr><td align=center><hr size=1></td></tr><tr><Td align=right>$del_btn&nbsp;$cancel_btn</td></tr></table>";

$display_table.=$btn_table;

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$button_remove,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<script language='javascript'>
function submitform(){
	obj = document.form1;
	if(obj!=null)
	  if(confirm(globalAlertMsg3))	      
		obj.submit();
}
</script>
<form name=form1 action="remove_update.php" method=POST>
<table width=560 border=0 align=center>
<tr><Td><?=($allow_delete?$i_Payment_Warning_Item_No_Has_Paid_Student:$i_Payment_Warning_Item_Has_Paid_Student)?></td></tr>
</table>
<?=$display_table?>
<?php
for($i=0;$i<sizeof($ItemID);$i++){
echo "<input type=hidden name='ItemID[]' value='".$ItemID[$i]."'>\n";
}
?>
</form>

<?

include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>

