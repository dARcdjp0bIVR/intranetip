<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$ItemName = intranet_htmlspecialchars($ItemName);
$Description = intranet_htmlspecialchars($Description);
$DisplayOrder = intranet_htmlspecialchars($DisplayOrder);
$PayPriority = intranet_htmlspecialchars($PayPriority);

$sql = "INSERT INTO PAYMENT_PAYMENT_ITEM
               (Name, CatID,DisplayOrder, PayPriority,Description, StartDate, EndDate,
                DefaultAmount, RecordStatus,ProcessingTerminalUser,ProcessingTerminalIP,ProcessingAdminUser,
                DateInput, DateModified)
        VALUES ('$ItemName','$CatID','$DisplayOrder','$PayPriority','$Description','$StartDate','$EndDate',
                 '$Amount',0, NULL,NULL,'$PHP_AUTH_USER',
                 now(),now())";
                 
$li->db_db_query($sql);
$ItemID = $li->db_insert_id();

# Insert each student's payment record
if (sizeof($ClassID)!=0)
{
    $classList = implode(",",$ClassID);
    $sql = "
    INSERT IGNORE INTO PAYMENT_PAYMENT_ITEMSTUDENT
           (ItemID, StudentID, Amount,RecordType, RecordStatus, DateInput, DateModified)
    SELECT $ItemID, a.UserID, '$Amount',0,0,now(),now()
            FROM INTRANET_USER as a LEFT OUTER JOIN INTRANET_CLASS as b ON a.ClassName = b.ClassName
            WHERE b.ClassID IN ($classList) AND a.RecordType = 2 AND a.RecordStatus IN (0,1,2)";
    $li->db_db_query($sql);
}

header ("Location: index.php?msg=1");
intranet_closedb();
?>
