<?php
// page used by: kenneth chung
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

if ($ItemID == "")
{
    header("Location: index.php");
    exit();
}

include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();

$itemStatus = $lpayment->returnPaymentItemStatus($ItemID);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;


$student_status_cond="";
if($StudentStatus==1){ ## Removed Student
	$student_status_cond =" AND c.UserID IS NOT NULL ";
}else if($StudentStatus==2){ ## Not Removed Student
	$student_status_cond = " AND b.UserID IS NOT NULL ";
}
else{ ## All Student
	$student_status_cond ="";
}

$conds = "";
if ($RecordStatus != "")
{
    $conds .= " AND a.RecordStatus = $RecordStatus";
}
if ($ClassName != "")
{
    $conds .= " AND (b.Classname = '$ClassName' OR c.Classname='$ClassName')";
}
$namefield = getNameFieldWithClassNumberByLang("b.");

if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";

$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";



$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield),
                ".$lpayment->getWebDisplayAmountFormatDB("a.Amount")." AS Amount,
                IF(a.SubsidyAmount IS NULL,".$lpayment->getWebDisplayAmountFormatDB("0").",".$lpayment->getWebDisplayAmountFormatDB("a.SubsidyAmount").") AS SubsidyAmount,
                IF(d.UnitName IS NULL,'&nbsp;',d.UnitName),
               IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid'),
			   IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,
			   		IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,'&nbsp;')),
               IF(a.PaidTime IS NULL,'&nbsp;',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s')),
               CONCAT('<input type=checkbox name=PaymentID[] value=', a.PaymentID ,'>')
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
              (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              $conds $student_status_cond
                ";



/*
$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassName,'</i>'),b.ClassName),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<i>',c.ClassNumber,'</i>'),b.ClassNumber),
			    IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('<font color=red>*</font><i>',$archive_namefield,'</i>'), $namefield),
                CONCAT('$', FORMAT(a.Amount, 1)) AS Amount,
                CONCAT('$', IF(a.SubsidyAmount IS NULL,FORMAT(0,1),FORMAT(a.SubsidyAmount,1))) AS SubsidyAmount,
                IF(d.UnitName IS NULL,'&nbsp;',d.UnitName),
               IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid'),
			   IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,
			   		IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,'&nbsp;')),
               IF(a.PaidTime IS NULL,'&nbsp;',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s')),
               CONCAT('<input type=checkbox name=PaymentID[] value=', a.PaymentID ,'>')
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
              (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              $conds
                ";
*/
$ldb = new libdb();
# Grab Class list
$class_sql = "SELECT DISTINCT b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			  WHERE
					a.ItemID = $ItemID AND
					b.ClassName IS NOT NULL
			  ORDER BY b.ClassName";
$classes = $ldb->returnVector($class_sql);
$select_class = getSelectByValue($classes,"name=ClassName onChange=this.form.submit()",$ClassName,1);

# Grab Summary
$summary_sql = "SELECT a.Amount, a.SubsidyAmount,a.SubsidyUnitID,a.RecordStatus FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER as c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
               (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              
              $conds $student_status_cond
 ";
$count_paid = 0;
$count_unpaid = 0;
$sum_paid = 0;
$sum_unpaid = 0;
$sum_subsidy_total=0;
$count_subsidy_total=0;
$temp = $ldb->returnArray($summary_sql,4);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($amount,$sub_amount,$sub_unit_id,$status) = $temp[$i];
     if ($status == 1)
     {
         $count_paid++;
         $sum_paid += $amount;
     }
     else
     {
         $count_unpaid++;
         $sum_unpaid += $amount;
     }
     if($sub_unit_id>0){
       	$count_subsidy_total++;
	 }
     if($sub_amount>0){
     	$sum_subsidy_total+=$sub_amount;
     }
}
$sum_total = $sum_paid + $sum_unpaid;
$count_total = $count_paid + $count_unpaid;
$sum_paid = $lpayment->getWebDisplayAmountFormat($sum_paid);
$sum_unpaid = $lpayment->getWebDisplayAmountFormat($sum_unpaid);
$sum_total = $lpayment->getWebDisplayAmountFormat($sum_total);
$sum_subsidy_total = $lpayment->getWebDisplayAmountFormat($sum_subsidy_total);

$infobar = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br>\n";
$infobar .= "<b>$i_Payment_ItemPaid:</b> $sum_paid ($count_paid $i_Payment_Students)<br>\n";
$infobar .= "<b>$i_Payment_ItemUnpaid:</b> $sum_unpaid ($count_unpaid $i_Payment_Students)<br>\n";
$infobar .= "<B>$i_Payment_Field_Total_Chargeable_Amount:</b> $sum_total ($count_total $i_Payment_Students)<br>";
$infobar .= "<B>$i_Payment_Subsidy_Total_Subsidy_Amount:</b> $sum_subsidy_total ($count_subsidy_total $i_Payment_Students)<br>";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
//$li->field_array = array("b.ClassName","b.ClassNumber","b.EnglishName","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$li->field_array = array("IF(b.UserID IS NULL,c.EnglishName,b.EnglishName)","a.Amount","a.SubsidyAmount","d.UnitName","a.RecordStatus","$last_updated_field","a.PaidTime");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,);
$li->wrap_array = array(0,0,0,0,0,0,0,);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
//$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
//$li->column_list .= "<td width=5% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
//$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_Chargeable_Amount)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Amount)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Subsidy_Unit)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_general_status)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_general_last_modified_by)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_TransactionTime)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("PaymentID[]")."</td>\n";

$student_status = "<SELECT name=StudentStatus onChange=this.form.submit()>\n";
$student_status .= "<OPTION value='-1' ".($StudentStatus=="-1"? "SELECTED":"").">$i_status_all</OPTION>\n";
$student_status .= "<OPTION value='1' ".(isset($StudentStatus) && $StudentStatus=="1"? "SELECTED":"").">$i_Payment_StudentStatus_Removed</OPTION>\n";
$student_status .= "<OPTION value='2' ".(isset($StudentStatus) && $StudentStatus=="2"? "SELECTED":"").">$i_Payment_StudentStatus_NonRemoved</OPTION>\n";
$student_status .= "</SELECT>\n";

$select_status = "<SELECT name=RecordStatus onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='' ".(!isset($RecordStatus) || $RecordStatus==""? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_status .= "<OPTION value=0 ".(isset($RecordStatus) && $RecordStatus=="0"? "SELECTED":"").">$i_Payment_PaymentStatus_Unpaid</OPTION>\n";
$select_status .= "<OPTION value=1 ".(isset($RecordStatus) && $RecordStatus==1? "SELECTED":"").">$i_Payment_PaymentStatus_Paid</OPTION>\n";
$select_status .= "</SELECT>\n";

if($itemStatus!=1){
	$toolbar = "<a class=iconLink href=javascript:checkNew('import.php?ItemID=$ItemID')>".importIcon()."$button_import</a>";
}else $toolbar="";
$toolbar .= toolbarSpacer()."<a class=iconLink href=javascript:newWindowExport(document.form1,'export.php?ItemID=$ItemID')>".exportIcon()."$button_export</a>";
$toolbar .= toolbarSpacer()."<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";

//$toolbar .= "<br><a class=iconLink href=javascript:newWindow(document.form1,'export.php?ItemID=$ItemID')>".newIcon()."$button_new</a>";
if($itemStatus!=1){
	$toolbar2 = "<a class=iconLink href=javascript:checkNew('insert.php?ItemID=$ItemID')>".newIcon()."$button_new</a>";
	$function_pay_all = "<a href=\"javascript:AlertPost(document.form1,'bulk_student_pay.php?ItemID=".$ItemID."','$i_Payment_alert_forcepayall')\"><img src='/images/admin/button/s_btn_payall_$intranet_session_language.gif' border='0' align='absmiddle' alt='$i_Payment_action_batchpay'></a>";
	$function_pay = "<a href=\"javascript:checkAlert(document.form1,'PaymentID[]','student_pay.php','$i_Payment_alert_forcepay')\"><img src='/images/admin/button/t_btn_pay_$intranet_session_language.gif' border='0' align='absmiddle' alt='$i_Payment_action_batchpay'></a>";
	$function_undo = "<a href=\"javascript:checkAlert(document.form1,'PaymentID[]','student_undopay.php','$i_Payment_alert_undopay')\"><img src='/images/admin/button/t_btn_undopay_$intranet_session_language.gif' border='0' align='absmiddle' alt='$i_Payment_action_undo'></a>";
	$function_edit = "<a href=\"javascript:checkEdit(document.form1,'PaymentID[]','student_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
	$function_remove = "<a href=\"javascript:checkRemove(document.form1,'PaymentID[]','student_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
}else{
	$toolbar2="";
	$function_pay_all = "";
	$function_pay="";
	$function_undo="";
	$function_edit="";
	$function_remove="";
}

$functionbar = "$select_status\n&nbsp;";
if (!isset($RecordStatus) || $RecordStatus == "")    # ALL
{
     $functionbar .= "$function_pay_all $function_pay $function_undo $function_edit";
}
else if ($RecordStatus == 1)
{
     $functionbar .= "$function_undo";
}
else
{
     $functionbar .= "$function_pay_all $function_pay $function_edit $function_remove";
}

$searchbar = "$select_class&nbsp;$student_status<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

if ($paid==1)
{
    $xmsg = "$i_Payment_con_PaidNoEdit";
}

$itemName = $lpayment->returnPaymentItemName($ItemID);


if ($pay == 1)
{
    $xmsg = "$i_Payment_con_PaymentProcessed";
}
else if ($pay == 2)
{
     $xmsg = "$i_Payment_con_PaymentProcessFailed";
}
else if ($pay == 3)
{
     $xmsg = "$i_Payment_con_PaymentUndo";
}


## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<SCRIPT LANGUAGE=Javascript>
function newWindowExport(obj,url)
{
         obj.target = "_blank";
         checkPost(obj,'export.php?ItemID=<?=$ItemID?>');
         obj.target = "";
         obj.action = "";
         obj.method = "GET";
}
function openPrintPage()
{
	general_values = "&keyword=<?=$keyword?>&pageNo=<?=$li->pageNo?>&order=<?=$li->order?>&field=<?=$li->field?>&numPerPage=<?=$li->page_size?>";
	page_values = "ItemID=<?=$ItemID?>&RecordStatus=<?=$RecordStatus?>&StudentStatus=<?=$StudentStatus?>&itemStatus=<?=$itemStatus?>&ClassName=<?=$ClassName?>";
	url="print.php?"+page_values+general_values; 
    newWindow(url,8);
}
// override the common one
function checkEdit(obj,element,page){
        if(countChecked(obj,element)>=1) {
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg2);
        }
}
</SCRIPT>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $infobar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"><BR>&nbsp;</td></tr>
<tr><td><?= $i_Payment_Note_StudentRemoved ?></td></tr>
</table>


<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
