<?php
// page used by: kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

@set_time_limit(0);

$ItemID = $_REQUEST['ItemID'];

$lpayment = new libpayment();

$sql = "SELECT a.PaymentID
        FROM 
        	PAYMENT_PAYMENT_ITEMSTUDENT as a 
        	INNER JOIN 
        	INTRANET_USER as b 
        	ON 
        	a.StudentID = b.UserID AND b.RecordType = 2 
        	AND a.ItemID = $ItemID
        	AND b.RecordStatus  IN (0,1,2) 
        	AND a.RecordStatus = 0 
        ";
$result = $lpayment->returnArray($sql,1);

?>
<form name="BulkPayment" method="POST" action="student_pay.php">
<input type="hidden" name="BulkPayment" value="1" >
<input type="hidden" name="ItemID" value="<?=$ItemID?>" >
<?
for ($i=0; $i<sizeof($result); $i++) {
?>
	<input type="hidden" name="PaymentID[]" value="<?=$result[$i][0]?>" >
<?
}
?>
</form>
<?
intranet_closedb();
?>
<script>
document.BulkPayment.submit();
</script>
