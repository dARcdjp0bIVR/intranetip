<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lpayment = new libpayment();


if (!isset($order)) $order = 1;
if (!isset($field)) $field = 2;
$order = ($order == 1) ? 1 : 0;

if ($CatID != "")
{
    $conds = " AND a.CatID = $CatID";
}

if ($itemPaidStatus <> ""){
	if ($itemPaidStatus == 0)
		$conds .= " AND (c.countPaid <> c.TotalCount OR c.TotalCount = 0) ";
	else if ($itemPaidStatus == 1)
		$conds .= " AND c.countPaid = c.TotalCount AND c.TotalCount <> 0 ";
	else if($itemPaidStatus == 2 ){ # Not Yet Started
		$today = date('Y-m-d');
		$conds .=" AND DATE_FORMAT(a.StartDate,'%Y-%m-%d') > '$today' ";
	}
}

$itemStatus=$itemStatus==""?0:$itemStatus;

	if($itemStatus==1){
		$conds2 =" AND a.RecordStatus=1 ";
	}else{
		$conds2 =" AND a.RecordStatus=0 ";
	}


	# date range
	$today_ts = strtotime(date('Y-m-d'));
	if($FromDate=="")
		$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
	if($ToDate=="")
		$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

#$datetype=$datetype!=2?1:2;
#if($datetype==2)
#	$date_conds =" AND DATE_FORMAT(a.EndDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate' ";
#else 
	$date_conds =" AND( (DATE_FORMAT(a.StartDate,'%Y-%m-%d') <= '$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>= '$FromDate' )) ";


//+++ get payment info of each payment item
// - get the payment id of all requested payment item
$ldb = new libdb();

$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$ldb->db_db_query($temp_table_sql);

$temp_table_sql = "CREATE TABLE TEMP_PAYMENT_ITEM_SUMMARY (
				pid int,
				sumUnpaid varchar(255),
				sumPaid varchar(255),
				TotalPayment varchar(255),
				countUnpaid int,
				countPaid int,
				TotalCount int
			)";
$ldb->db_db_query($temp_table_sql);

$pValue="";

$all_pid_sql  = "SELECT a.ItemID 
				 FROM
					 PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON a.CatID = b.CatID
				 WHERE
					  (a.Name LIKE '%$keyword%' OR
					   a.Description LIKE '%$keyword%'
					  )
                ";

$allPaymentID = $ldb->returnVector($all_pid_sql);
// - get the summay of payment :
// - Sum of unpaid amount, Sum of paid amount, Count of unpaid student, Count of paid student
if (sizeOf($allPaymentID)>0)
	for ($i=0; $i<sizeOf($allPaymentID); $i++){
		$allPaymentIDSummary[$i] = $lpayment->returnPaymentPaidSummary($allPaymentID[$i]);

		if ($i>0) $pValue .= ","; 
		$pValue .=
							"(".$allPaymentID[$i].",'"
							.number_format($allPaymentIDSummary[$i][0],1,".",",")."','"
							.number_format($allPaymentIDSummary[$i][1],1,".",",")."','"
							.number_format(($allPaymentIDSummary[$i][0]+$allPaymentIDSummary[$i][1]),1,".",",")."',"
							.$allPaymentIDSummary[$i][2].",".$allPaymentIDSummary[$i][3]."," 
							.($allPaymentIDSummary[$i][2]+$allPaymentIDSummary[$i][3]).")";
	}

$insert_temp_sql = "INSERT INTO TEMP_PAYMENT_ITEM_SUMMARY ( pid, sumUnpaid, sumPaid, TotalPayment, countUnpaid, countPaid,TotalCount ) VALUES $pValue";
$ldb->db_db_query($insert_temp_sql);
//+++ end of getting payment


$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";


$sql  = "SELECT
               a.Name,
               b.Name, a.DisplayOrder, a.PayPriority, 
			   CONCAT(c.countPaid,'(',c.TotalCount,')') as countInfo,
			   IF(a.Description IS NULL OR a.Description='','',a.Description),
			   IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,
			   		IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,'')),
               DATE_FORMAT(a.StartDate,'%Y-%m-%d'),
               DATE_FORMAT(a.EndDate,'%Y-%m-%d')
         FROM
             PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) LEFT OUTER JOIN TEMP_PAYMENT_ITEM_SUMMARY as c ON (c.pid = a.ItemID)
         WHERE
              (a.Name LIKE '%$keyword%' OR
               a.Description LIKE '%$keyword%'
              )
              $conds $date_conds $conds2
                ";
                
                
# TABLE INFO
$field_array = array("a.Name","b.Name","a.DisplayOrder","a.PayPriority","countInfo","a.Description","$last_updated_field","a.StartDate","a.EndDate");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();


$temp = $li->returnArray($sql, sizeof($field_array));


$csv="";
$csv.="\"$i_Payment_Field_PaymentItem\",";
$csv.="\"$i_Payment_Field_PaymentCategory\",";
$csv.="\"$i_Payment_Field_DisplayOrder\",";
$csv.="\"$i_Payment_Field_PayPriority\",";
$csv.="\"$i_Payment_Field_PaidCount ($i_Payment_Field_TotalPaidCount)\",";
$csv.="\"$i_general_description\",";
$csv.="\"$i_general_last_modified_by\",";
$csv.="\"$i_general_startdate\",";
$csv.="\"$i_general_enddate\"\n";

$csv_utf="";
$csv_utf="$i_Payment_Field_PaymentItem\t";
$csv_utf.="$i_Payment_Field_PaymentCategory\t";
$csv_utf.="$i_Payment_Field_DisplayOrder\t";
$csv_utf.="$i_Payment_Field_PayPriority\t";
$csv_utf.="$i_Payment_Field_PaidCount ($i_Payment_Field_TotalPaidCount)\t";
$csv_utf.="$i_general_description\t";
$csv_utf.="$i_general_last_modified_by\t";
$csv_utf.="$i_general_startdate\t";
$csv_utf.="$i_general_enddate\r\n";



for($i=0;$i<sizeof($temp);$i++){
	list($item_name,$cat_name,$display_order,$pay_priority,$count_info,$desc,$last_update,$start_date,$end_date)=$temp[$i];
	$csv.="\"$item_name\",\"$cat_name\",\"$display_order\",\"$pay_priority\",\"$count_info\",\"$desc\",\"$last_update\",\"$start_date\",\"$end_date\"\n";
	$csv_utf.="$item_name\t$cat_name\t$display_order\t$pay_priority\t$count_info\t$desc\t$last_update\t$start_date\t$end_date\r\n";

}


if($CatID!=''){
	$sql="SELECT Name FROM PAYMENT_PAYMENT_CATEGORY WHERE CatID = '$CatID'";
	$temp = $li->returnVector($sql);
	$cat_name = $temp[0];
}else{
	$cat_name = $i_status_all;
}

$str_item_paid_status="";
switch($itemPaidStatus){
	case "": $str_item_paid_status = $i_status_all; break;
	case 0 : $str_item_paid_status = $i_Payment_Field_NotAllPaid; break;
	case 1 : $str_item_paid_status = $i_Payment_Field_AllPaid; break;
	case 2 : $str_item_paid_status = $i_Payment_PaymentStatus_NotStarted; break;
	//default :  $str_item_paid_status = $i_status_all;
}

$str_item_status="";
switch($itemStatus){
	case 1 : $str_item_status = $i_Payment_Menu_Settings_PaymentItem_ArchivedRecord; break;
	default :  $str_item_status = $i_Payment_Menu_Settings_PaymentItem_ActiveRecord;
}

$keyword=$keyword==""?"--":$keyword;




$csv_title="\"$i_Payment_SchoolAccount_PresetItem ( $FromDate $i_Profile_To $ToDate )\"\n";
$csv_title.="\"$i_Payment_Field_PaymentCategory\",\"$cat_name\"\n";
$csv_title.="\"$i_general_status\",\"$str_item_paid_status\"\n";
$csv_title.="\"$i_Payment_Class_Item_Type\",\"$str_item_status\"\n";
$csv_title.="\"$i_Payment_Field_PaymentItem\",\"$keyword\"\n";
if($itemStatus==1){
	$csv_title.="\"$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2\"\n";
}
$csv = $csv_title.$csv;

$csv_utf_title="$i_Payment_SchoolAccount_PresetItem ( $FromDate $i_Profile_To $ToDate )\r\n";
$csv_utf_title.="$i_Payment_Field_PaymentCategory\t$cat_name\r\n";
$csv_utf_title.="$i_general_status\t$str_item_paid_status\r\n";
$csv_utf_title.="$i_Payment_Class_Item_Type\t$str_item_status\r\n";
$csv_utf_title.="$i_Payment_Field_PaymentItem\t$keyword\r\n";
if($itemStatus==1){
	$csv_utf_title.="$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2\r\n";
}
$csv_utf = $csv_utf_title.$csv_utf;


# remove the temp table
$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$ldb->db_db_query($temp_table_sql);

intranet_closedb();

$filename = "payment_item.csv";
if ($g_encoding_unicode) {
        $lexport->EXPORT_FILE($filename, $csv_utf);
} else {
        output2browser($csv,$filename);
}

?>