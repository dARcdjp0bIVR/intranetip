<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libuser.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lpayment = new libpayment();


if($ItemID=="" || sizeof($PaymentID)<=0 || $PaymentID==""){
	header("Location: index.php");
}



$itemName = $lpayment->returnPaymentItemName($ItemID);

$namefield = getNameFieldWithClassNumberByLang("b.");

$list = is_array($PaymentID)? implode(",",$PaymentID):$PaymentID;

$sql = "SELECT a.PaymentID,a.RecordStatus, a.Amount,a.SubsidyAmount,a.StudentID,$namefield,a.SubsidyUnitID FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE PaymentID IN ($list)";
$payment_infos = $lpayment->returnArray($sql,7);



### Get Subsidy Unit info
$subsidy = array();
$available_sub = array();
$sql="SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ($list) AND RecordStatus=0";
$temp = $lpayment->returnVector($sql);

$target_list = implode(",",$temp);
$sql="SELECT a.UnitID, a.UnitName,a.TotalAmount, SUM(b.SubsidyAmount),a.RecordStatus FROM PAYMENT_SUBSIDY_UNIT  AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON ( a.UnitID = b.SubsidyUnitID AND b.PaymentID NOT IN ($target_list)) GROUP BY a.UnitID ORDER BY a.UnitID";
$temp = $lpayment->returnArray($sql,5);
for($i=0;$i<sizeof($temp);$i++){
	list($sub_unit_id,$sub_name,$sub_total,$sub_used,$r_status)=$temp[$i];
	$subsidy[$sub_unit_id]['name'] = $sub_name;
	$subsidy[$sub_unit_id]['total'] = $sub_total;
	$subsidy[$sub_unit_id]['left'] = $sub_total - $sub_used > 0 ? $sub_total - $sub_used : 0;
	$subsidy[$sub_unit_id]['status']=$r_status;
	//if($subsidy[$sub_unit_id]['left']>0)
		$available_sub[]=array($sub_unit_id,$sub_name,$r_status);
}


include_once("../../../../templates/adminheader_setting.php");

//echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,"list.php?ItemID=$ItemID","$button_edit $i_Payment_Field_Amount",'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,"list.php?ItemID=$ItemID","$button_edit $i_Payment_Field_Amount",'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<script language='javascript'>

function getLeftAmount(){
	amount = new Array();
	<?php
	foreach($subsidy as $sub_id => $values){
		echo "\tamount.push(new Array($sub_id,".$values['left']."));\n";
	}
	?>
	return amount;
}
function checkAmount(obj){

	if(!check_numeric(obj,obj.value,'<?=$i_Payment_Warning_InvalidAmount?>')) return false;
	return true;

}


function recalculateAmount(sid){
	obj = document.form1;
	objCurrentAmount = getObj('old_amount_'+sid);
	objNewAmount = getObj('new_amount_'+sid);
	objNewSubsidy = getObj('new_subsidy_amount_'+sid);
	
	if(!checkAmount(objNewAmount)) return;
	
	current_amount = parseFloat(objCurrentAmount.value,10);
	
	new_amount = parseFloat(objNewAmount.value,10);
	
	left = current_amount - new_amount;
	
	new_subsidy_amount = left>0?left:0;
	new_subsidy_amount = Math.round(new_subsidy_amount*100)/100;
	
	objNewSubsidy.value = new_subsidy_amount;
	
}
function getObj(objName){
	obj = document.getElementsByName(objName);
	return obj[0];
}
function checkform(obj){
	students = document.getElementsByName('StudentIDs[]');
	left = getLeftAmount();
	for(x=0;x<students.length;x++){
		sid = students[x].value;
		
		objNewAmount = getObj('new_amount_'+sid);
		objNewSubsidy = getObj('new_subsidy_amount_'+sid);
		objUnitID = getObj('unitid_'+sid);
		
		if(!checkAmount(objNewAmount)) return false;
		if(!checkAmount(objNewSubsidy)) return false;
		
		selectedUnit = objUnitID.options[objUnitID.selectedIndex].value;
		
		if(selectedUnit=='' && objNewSubsidy.value>0){
			alert('<?=$i_Payment_Subsidy_Warning_Select_SubsidyUnit?>');
			objUnitID.focus();
			return false;
		}
		if(selectedUnit!='' && objNewSubsidy.value<=0){
			alert('<?=$i_Payment_Subsidy_Warning_Enter_New_SubsidyAmount?>');
			objNewSubsidy.focus();
			return false;
		}
		newSubsidyAmount = objNewSubsidy.value;
		if(newSubsidyAmount>0){
			for(j=0;j<left.length;j++){
				if(left[j][0] == selectedUnit){
					if(left[j][1] >=newSubsidyAmount){
						left[j][1] -= newSubsidyAmount;
					}else{
						alert('<?=$i_Payment_Subsidy_Warning_NotEnough_Amount?>');
						objNewSubsidy.focus();
						return false;
					}
				}
			}
		}
		
	}
	if(confirm('<?=$i_Payment_Subsidy_Edit_Confirm?>')){
		return true;
	}
	return false;
}
</script>

<form name=form1 action="student_edit_update.php" method=POST onSubmit='return checkform(this)'>
<table width=560 border=0 align=center>
<tr><td><b><?=$i_Payment_Field_PaymentItem?>:</b> &nbsp;<?=$itemName?></td></tr>
</table>
<table width=560  border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=3 cellspacing=0 align='center'>
<!--<tr class='tableTitle'><td><?=$i_UserName?></td><td><?=$i_Payment_Subsidy_Current_Amount?></td><td><?=$i_Payment_Subsidy_Current_Subsidy_Amount?></td><td><?=$i_Payment_Subsidy_New_Amount?></td><td><?=$i_Payment_Subsidy_New_Subsidy?></td><td><?=$i_Payment_Subsidy_Item?></td></tr>-->
<tr class='tableTitle'><td width='30%'><?=$i_UserName?></td><td>&nbsp;</td></tr>

<?php
// print_r($payment_infos);
$valid_count=0;
$students=array();
for($i=0;$i<sizeof($payment_infos);$i++){
	list($payment_id,$paid_status, $t_amount,$t_subsidy,$student_id,$student_name,$t_unit_id) = $payment_infos[$i];
	
	$t_subsidy+=0;
	$t_amount+=0;
	?>	
	<tr>
		<td><?=$student_name?></td>
		<?php 
				if($paid_status==1){
					echo "<td align=center><font color=red>$i_Payment_Import_PayAlready</font></td>";
				}
				else{
					$valid_count++;
					$students[] = $student_id;
					$select_unit ="<select name='unitid_$student_id'>";
					$select_unit .= "<OPTION value=''>$i_Payment_Subsidy_Option_No</OPTION>";
					
					for($j=0;$j<sizeof($available_sub);$j++){
						list($unit_id,$unit_name,$unit_status)=$available_sub[$j];
						if($unit_id==$t_unit_id || $unit_status==1)
							$select_unit .="<OPTION value='$unit_id' ".($unit_id==$t_unit_id?" SELECTED ":"").">$unit_name".($unit_status==1?"":" ($i_general_inactive)")."</OPTION>\n";
					}
					$select_unit.="</select>";
					
					echo "<td><table border=0 cellspacing=0 cellpadding=3>";
					echo "<tr><Td align=right>$i_Payment_Field_Current_Chargeable_Amount:</td><td>".$lpayment->getWebDisplayAmountFormat($t_amount)."<input type='hidden' name='old_amount_$student_id' value='".($t_amount+$t_subsidy)."'></td></tr>";
					echo "<tr><Td align=right>$i_Payment_Subsidy_Current_Subsidy_Amount:<td>".$lpayment->getWebDisplayAmountFormat($t_subsidy)."</td></td></tr>";
					echo "<tr><td align=right>$i_Payment_Field_New_Chargeable_Amount:</td><td><input type='text' name='new_amount_$student_id' value='$t_amount' onblur='recalculateAmount($student_id)'></td></td></tr>";
					echo "<tr><td align=right>$i_Payment_Subsidy_New_Subsidy:</td><td><input type='text' name='new_subsidy_amount_$student_id' value='$t_subsidy'></td></tr>";
					echo "<tr><td align=right>$i_Payment_Subsidy_Unit:</td><td>$select_unit</td></tr>";
					echo "</table>";
					echo "<input type='hidden' name='PaymentID[]' value='$payment_id'>\n";
					echo "<input type='hidden' name='StudentIDs[]' value='$student_id'>\n";
					echo "</td>";
				 }
		?>
	</tr>
<?}?>
</table><BR>
<table border=0 width=560 align='center'>
<tr><td align=center>
<?php
if($valid_count>0){?>
	<input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif">&nbsp;&nbsp;
<? }?>
<a href='list.php?ItemID=<?=$ItemID?>'><img border=0 align='absmiddle' src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif"></a></td></tr>
</table>
<input type=hidden name=ItemID value="<?=$ItemID?>">
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
</form>
<?

include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
