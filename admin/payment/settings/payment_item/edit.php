<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$ItemID = (is_array($ItemID)? $ItemID[0] : $ItemID);
$lpayment = new libpayment();
$info = $lpayment->returnPaymentItemInfo($ItemID);
list ($name, $CatID, $displayOrder, $desp, $start, $end, $PayPriority) = $info;

$amount = $lpayment->returnDefaultPaymentAmount($ItemID);

$cats = $lpayment->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name=CatID",$CatID,0,1);

## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentCategory,'index.php',$button_edit,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$button_edit,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<script language='javascript'>
function checkform(obj){
        if(obj==null) return false;
        objName = obj.ItemName;
        objStartDate = obj.StartDate;
        objEndDate = obj.EndDate;
        objDispOrder = obj.DisplayOrder;
        objPriority = obj.PayPriority;
        objChangeAmount = obj.ChangeAmount;
        if(!check_text(objName,'<?=$i_Payment_Warning_Enter_PaymentItemName?>')) return false;
        if(!check_date(objStartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
        if(!check_date(objEndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
        if(!check_numeric(objDispOrder,'','<?=$i_Payment_Warning_Invalid_DisplayOrder?>')) return false;
        if(!check_numeric(objPriority,'','<?=$i_Payment_Warning_Invalid_PayPriority?>')) return false;
        if (compareDate(objStartDate.value, objEndDate.value) > 0)
        {
            alert ("<?php echo $i_con_msg_date_startend_wrong_alert; ?>"); return false;
        }
        if(objChangeAmount.checked){
                objAmount = obj.Amount;
                //if(!check_text(objAmount,'<?=$i_Payment_Warning_Enter_NewDefaultAmount?>')) return false;
                if(!check_numeric(objAmount,'<?=$amount?>','<?=$i_Payment_Warning_InvalidAmount?>')) return false;
        }

        return true;

}
</script>
<form name=form1 action="edit_update.php" method=POST onsubmit='return checkform(this)'>
<table width=90% border=0 align=center>
<tr><td align=right nowrap><?=$i_Payment_Field_PaymentCategory?>:</td><td><?=$select_cat?></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_PaymentItem?>:</td><td><input type=text name=ItemName size=50 MAXLENGTH=255 value='<?=$name?>'></td></tr>
<tr><td align=right nowrap><?=$i_general_startdate?>:</td><td><input type=text name=StartDate size=10 MAXLENGTH=10 value="<?=$start?>"></td></tr>
<tr><td align=right nowrap><?=$i_general_enddate?>:</td><td><input type=text name=EndDate size=10 MAXLENGTH=10 value="<?=$end?>"></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_DisplayOrder?>:</td><td><input type=text name=DisplayOrder size=10 maxlength=10 value="<?=$displayOrder?>"></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_PayPriority?>:</td><td><input type=text name=PayPriority size=10 maxlength=10 value="<?=$PayPriority?>">(<?=$i_Payment_Note_Priority?>)</td></tr>
<tr><td align=right nowrap><?=$i_general_description?>:</td><td><TEXTAREA rows=5 cols=50 name=Description><?=$desp?></TEXTAREA></td></tr>
<tr><td align=right nowrap><?=$i_Payment_ChangeDefaultAmount?>:</td><td><input type=checkbox name=ChangeAmount value=1 ONCLICK="this.form.Amount.disabled=!this.checked"></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Field_NewDefaultAmount?>:</td><td><input type=text name=Amount size=10 maxlength=10 DISABLED value="<?=$amount?>"></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>
<?

include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
