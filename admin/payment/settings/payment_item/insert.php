<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lidb = new libdb();
$lclass = new libclass();
$lpayment = new libpayment();

$itemName = $lpayment->returnPaymentItemName($ItemID);


$sql = "
			SELECT 
					IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.UserID,IF((b.UserID IS NOT NULL AND c.UserID IS NULL),b.UserID,''))
			FROM 
					PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN 
					INTRANET_USER as b ON a.StudentID = b.UserID LEFT OUTER JOIN 
					INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID
			WHERE 
					a.ItemID = $ItemID
		";

$result = $lidb->returnVector($sql);

if(sizeof($result)>0)
	$student_list = implode(",", $result);	# Student Who Are In The Payment List Already


$sql = "
			SELECT
					DefaultAmount
			FROM
					PAYMENT_PAYMENT_ITEM
			WHERE
					ItemID = $ItemID
		";

$temp = $lidb->returnVector($sql);
if(sizeof($temp)>0)
	list($default_amount) = $temp;
	
	
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $lclass->returnArray($sql,2);

    //if(sizeof($student)==1)
   	//	$allow_item_number=true;
}
?>


<SCRIPT LANGUAGE=Javascript>
function removeStudent(){
		 checkOptionRemove(document.form1.elements["student[]"]);
 		 submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'insert_update.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;
        var cnt = 0;

        if(obj.elements["student[]"].length != 0)
        {
        		cnt++;
    	}
        else
        {
        		alert("<?=$i_SmartCard_Payment_Student_Select_Instruction?>");
        		return false;
    	}
        
        if(obj.amount.value != '')
        {
        		cnt++;
    	}
        else
        {
	    		alert("<?=$i_SmartCard_Payment_Input_Amount_Instruction?>");
	    		return false;
		}
		    
       	if(cnt==2)
                return formSubmit(obj);
        else
        {
                alert('<?=$i_SmartCard_Payment_Student_Select_Instruction?>');
                return false;
        }
}

</SCRIPT>


<?
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,"list.php?ItemID=".$ItemID."'", $button_new, '');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>


<br>
<form name="form1" action="" method="POST" onsubmit="return checkForm();">
<table width=450 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr><td width=30%><?=$i_Profile_SelectStudent?></td>
	<td><table width=100% border=0 cellspacing=0 cellpadding=0>
		<tr>
			<td><SELECT name="student[]" size=5 multiple>
				<?
				for ($i=0; $i<sizeof($array_students); $i++)
				{
				     list ($id, $name) = $array_students[$i];
				?>
				<OPTION value='<?=$id?>'><?=$name?></OPTION>
				<?
				}
				?>
				</SELECT></td>
			<td>
			<?	
				if($student_list != ""){
					$arr_student = explode(",",$student_list);
				}
				if(sizeof($arr_student)>0)
				{
					for($i=0; $i<sizeof($arr_student); $i++)
					{
						$selected_student = $arr_student[$i];
			?>
						<input type="hidden" name="selected_student[]" value="<?=$selected_student?>">
			<?
					}
				}
			?>
			<!--<a href='javascript:newWindow("choose/index.php?fieldname=student[]&student_list=<?=$student_list?>", 9)'><img src="<?=$image_path?>/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border=0></a>-->
			<a href='javascript:newWindow("choose/index.php?fieldname=student[]", 9)'><img src="<?=$image_path?>/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border=0></a>
			<br>
			<a href='javascript:removeStudent();'><img src="<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border=0></a>
			</td>
		</tr>
	</table>
</td></tr>
<tr><td><?=$i_Payment_Field_Amount?></td><td><input type="text" name="amount" value=<?=$default_amount?>></td></tr>
<tr><td colspan=2><input type=image onClick="this.form.flag.value=1" src="<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>


<input type=hidden name=flag value=0>
<input type=hidden name="ItemID" value=<?=$ItemID?>>
<input type=hidden name="student_list" value=<?=$student_list?>>



</form>


<?																																																																																																	
include_once("../../../../templates/adminfooter.php");
?>