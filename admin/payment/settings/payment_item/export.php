<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lpayment = new libpayment();

$lexport = new libexporttext();


$li = new libdb();
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 0;
$field += 0;

$sql="SELECT a.Name,b.Name,a.DefaultAmount,a.StartDate,a.EndDate,a.RecordStatus FROM PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,6);
list($itemName,$catName,$defaultAmount,$startDate,$endDate,$itemStatus)=$temp[0];
$defaultAmount = $lpayment->getExportAmountFormat($defaultAmount);


$student_status_cond="";
if($StudentStatus==1){ ## Removed Student
	$student_status_cond =" AND c.UserID IS NOT NULL ";
}else if($StudentStatus==2){ ## Not Removed Student
	$student_status_cond = " AND b.UserID IS NOT NULL ";
}
else{ ## All Student
	$student_status_cond ="";
}

if ($RecordStatus != "")
{
    $conds .= " AND a.RecordStatus = $RecordStatus";
}
if ($ClassName != "")
{
    $conds .= " AND b.Classname = '$ClassName'";
}


$namefield = getNameFieldByLang("b.");
if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
	$archive_namefield = "c.ChineseName";
}else $archive_namefield = "c.EnglishName";

$lastmodified_field = "IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";

$target_name_field = "IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.EnglishName, b.EnglishName)";


$sql  = "SELECT
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),CONCAT('*',c.ClassName),b.ClassName),
				IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.ClassNumber,b.ClassNumber),
			    IF((b.UserID IS NULL AND c.UserID IS NOT NULL),$archive_namefield,$namefield),
			    IF((b.UserID IS NULL AND c.UserID IS NOT NULL),c.UserLogin,b.UserLogin),
                ".$lpayment->getExportAmountFormatDB("a.Amount").",
                ".$lpayment->getExportAmountFormatDB("a.SubsidyAmount").",
                d.UnitName, 
               IF(a.RecordStatus = 1,'$i_Payment_PaymentStatus_Paid','$i_Payment_PaymentStatus_Unpaid'),
                $lastmodified_field,
               IF(a.PaidTime IS NULL,'',DATE_FORMAT(a.PaidTime,'%Y-%m-%d %H:%i:%s'))
         FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
              (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              $conds $student_status_cond
              
                           ";
                           
                           
# Grab Summary
$summary_sql = "SELECT a.Amount, a.SubsidyAmount,a.SubsidyUnitID,a.RecordStatus FROM
             PAYMENT_PAYMENT_ITEMSTUDENT as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
             LEFT OUTER JOIN INTRANET_ARCHIVE_USER as c ON a.StudentID = c.UserID LEFT OUTER JOIN PAYMENT_SUBSIDY_UNIT as d ON (a.SubsidyUnitID=d.UnitID)
         WHERE
              a.ItemID = $ItemID AND
               (b.EnglishName LIKE '%$keyword%' OR
               b.ChineseName LIKE '%$keyword%' OR
               b.ClassName LIKE '%$keyword%' OR
               b.ClassNumber LIKE '%$keyword%' OR
               c.EnglishName LIKE '%$keyword%' OR
               c.ChineseName LIKE '%$keyword%' OR
               c.ClassName LIKE '%$keyword%' OR
               c.ClassNumber LIKE '%$keyword%' OR
               d.UnitName LIKE '%$keyword%'

              )
              
              $conds $student_status_cond
 ";
$count_paid = 0;
$count_unpaid = 0;
$sum_paid = 0;
$sum_unpaid = 0;
$sum_subsidy_total=0;
$count_subsidy_total=0;
$temp = $li->returnArray($summary_sql,4);
for ($i=0; $i<sizeof($temp); $i++)
{
     list($amount,$sub_amount,$sub_unit_id,$status) = $temp[$i];
     if ($status == 1)
     {
         $count_paid++;
         $sum_paid += $amount;
     }
     else
     {
         $count_unpaid++;
         $sum_unpaid += $amount;
     }
     if($sub_unit_id>0){
       	$count_subsidy_total++;
	 }
     if($sub_amount>0){
     	$sum_subsidy_total+=$sub_amount;
     }
}
$sum_total = $sum_paid + $sum_unpaid;
$count_total = $count_paid + $count_unpaid;
$sum_paid = $lpayment->getExportAmountFormat($sum_paid);
$sum_unpaid = $lpayment->getExportAmountFormat($sum_unpaid);
$sum_total = $lpayment->getExportAmountFormat($sum_total);
$sum_subsidy_total = $lpayment->getExportAmountFormat($sum_subsidy_total);

$infobar="";
//$infobar = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br>\n";
//$infobar .= "<b>$i_Payment_Field_PaymentItem:</b> $itemName<br>\n";
$infobar .= "\"$itemName\"\n";
$infobar .= "\"$i_Payment_Field_PaymentCategory\",\"$catName\"\n";
$infobar .= "\"$i_Payment_PresetPaymentItem_PaymentPeriod\",\"$startDate $i_Profile_To $endDate\"\n";
$infobar .= "\"$i_Payment_Field_PaymentDefaultAmount\",\"$defaultAmount\"\n";
$infobar .= "\"$i_Payment_ItemPaid\",\"$sum_paid ($count_paid $i_Payment_Students)\"\n";
$infobar .= "\"$i_Payment_ItemUnpaid\",\"$sum_unpaid ($count_unpaid $i_Payment_Students)\"\n";
$infobar .= "\"$i_Payment_Field_Total_Chargeable_Amount\",\"$sum_total ($count_total $i_Payment_Students)\"\n";
$infobar .= "\"$i_Payment_Subsidy_Total_Subsidy_Amount\",\"$sum_subsidy_total ($count_subsidy_total $i_Payment_Students)\"\n";
  
$infobar_utf="";
//$infobar_utf = "<font color=green><u><b>$i_Payment_ItemSummary</b></u></font><br>\n";
//$infobar_utf .= "<b>$i_Payment_Field_PaymentItem:</b> $itemName<br>\n";
$infobar_utf .= "$itemName\r\n";
$infobar_utf .= "$i_Payment_Field_PaymentCategory\t$catName\r\n";
$infobar_utf .= "$i_Payment_PresetPaymentItem_PaymentPeriod\t$startDate $i_Profile_To $endDate\r\n";
$infobar_utf .= "$i_Payment_Field_PaymentDefaultAmount\t$defaultAmount\r\n";
$infobar_utf .= "$i_Payment_ItemPaid\t$sum_paid ($count_paid $i_Payment_Students)\r\n";
$infobar_utf .= "$i_Payment_ItemUnpaid\t$sum_unpaid ($count_unpaid $i_Payment_Students)\r\n";
$infobar_utf .= "$i_Payment_Field_Total_Chargeable_Amount\t$sum_total ($count_total $i_Payment_Students)\r\n";
$infobar_utf .= "$i_Payment_Subsidy_Total_Subsidy_Amount\t$sum_subsidy_total ($count_subsidy_total $i_Payment_Students)\r\n";
                         
                           
# use for ordering the results                        
$field_array = array($target_name_field,"a.Amount","a.SubsidyAmount","d.UnitName",$lastmodified_field,"a.RecordStatus","a.PaidTime");


$sql .= " ORDER BY ".$field_array[$field].( ($order==0) ? " DESC" : " ASC");

$result = $li->returnArray($sql,10);
$x = $infobar."\n";
$x .= "\"$i_ClassName\",\"$i_ClassNumber\",\"$i_UserStudentName\",\"$i_UserLogin\",\"$i_Payment_Field_Chargeable_Amount\",\"$i_Payment_Subsidy_Amount\",\"$i_Payment_Subsidy_Unit\",\"$i_general_status\",\"$i_general_last_modified_by\",\"$i_Payment_Field_TransactionTime\"\n";
$utf_x = $infobar_utf."\r\n";
$utf_x .= $i_ClassName."\t".$i_ClassNumber."\t".$i_UserStudentName."\t".$i_UserLogin."\t".$i_Payment_Field_Chargeable_Amount."\t".$i_Payment_Subsidy_Amount."\t".$i_Payment_Subsidy_Unit."\t".$i_general_status."\t".$i_general_last_modified_by."\t".$i_Payment_Field_TransactionTime."\r\n";
//$x = "\"Class Name\",\"Class Number\",\"Student Name\",\"User Login\",\"Amount\",\"Subsidy Amount\",\"Subsidy Unit\",\"Status\",\"Last Modified By\",\"Paid Time\"\n";


for($i=0;$i<sizeof($result);$i++){
	list($class_name,$class_num,$student_name,$userlogin,$amount,$sub_amount,$sub_unit,$status,$lastmodifed,$paidtime)=$result[$i];
	$x.="\"$class_name\",\"$class_num\",\"$student_name\",\"$userlogin\",\"$amount\",\"$sub_amount\",\"$sub_unit\",\"$status\",\"$lastmodifed\",\"$paidtime\"\n";
	$utf_x.=$class_name."\t".$class_num."\t".$student_name."\t".$userlogin."\t".$amount."\t".$sub_amount."\t".$sub_unit."\t".$status."\t".$lastmodifed."\t".$paidtime."\r\n";
}
$x.="\"$i_Payment_Note_StudentRemoved2\"\n";
$utf_x.= $i_Payment_Note_StudentRemoved2."\r\n";

// Output the file to user browser
$filename = "studentlist.csv";



intranet_closedb();

$utf_content = $utf_x;
$content = $x;
if ($g_encoding_unicode) {
        $lexport->EXPORT_FILE($filename, $utf_content);
} else {
        output2browser($content,$filename);
}

?>
