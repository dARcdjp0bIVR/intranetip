<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
intranet_opendb();

/*
$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
} 
else {  # update database

        $ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                array_shift($data);                   # drop the title bar
        }
        else
        {
            header("Location: import.php?failed=2");
            exit();
        }

*/
if($confirm==1){
		$li = new libdb();
		$sql = "SELECT ClassName,ClassNumber,Amount FROM TEMP_PAYMENT_ITEM_IMPORT";
		$data = $li->returnArray($sql,3);
        $sql = "LOCK TABLES
                     INTRANET_USER READ,
                     TEMP_PAYMENT_ITEM_IMPORT WRITE
                     ,PAYMENT_PAYMENT_ITEMSTUDENT WRITE
                     ";
        $li->db_db_query($sql);

        # Get Existing Students
        $sql = "SELECT ClassName, ClassNumber, UserID
                FROM INTRANET_USER
                WHERE RecordType = 2 AND RecordStatus = 1
                      AND ClassName != '' AND ClassNumber != ''
                ORDER BY ClassName, ClassNumber";
        $result = $li->returnArray($sql,3);
        for ($i=0; $i<sizeof($result); $i++)
        {
             list($class,$classnum,$uid) = $result[$i];
             $students[$class][$classnum] = $uid;
        }

        # Get Existing non-paid StudentID
        $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = $ItemID AND RecordStatus = 0";
        $unpaid = $li->returnVector($sql);

        # Get Existing paid StudentID
        $sql = "SELECT StudentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = $ItemID AND RecordStatus = 1";
        $paid = $li->returnVector($sql);
        # Update the amount
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($class, $classnum, $amount) = $data[$i];
             $targetUID = $students[$class][$classnum];
             
             if ($targetUID == "") continue;
             if (in_array($targetUID,$paid)) continue;    # Paid records remain unchanged
             
             if (in_array($targetUID, $unpaid))       # Existing record
             {
                 if ($amount == "")
                 {
                     $sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT
                             WHERE ItemID = $ItemID AND RecordStatus = 0 AND StudentID = $targetUID";
                 }
                 else
                 {
                     $sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount = '$amount', RecordType = 1, ProcessingTerminalUser=NULL,ProcessingTerminalIP=NULL, ProcessingAdminUser='$PHP_AUTH_USER', DateModified=NOW()
                             WHERE ItemID = $ItemID AND RecordStatus = 0 AND StudentID = $targetUID";
                 }
             }
             else            # New record
             {
                 $sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT
                                (ItemID, StudentID, Amount, RecordType, RecordStatus, ProcessingTerminalUser, ProcessingAdminUser, ProcessingTerminalIP, DateInput, DateModified)
                         VALUES
                                ('$ItemID','$targetUID','$amount',1,0,NULL,'$PHP_AUTH_USER',NULL,now(),now())";
             }
             $li->db_db_query($sql);
        }
    $sql = "DELETE FROM TEMP_PAYMENT_ITEM_IMPORT";
    $li->db_db_query($sql);
    $sql = "UNLOCK TABLES";
    $li->db_db_query($sql);
	header ("Location: list.php?ItemID=$ItemID&msg=1&clear=1");
}
else 
	header("Location: import.php?ItemID=$ItemID&clear=1");

intranet_closedb();
?>
