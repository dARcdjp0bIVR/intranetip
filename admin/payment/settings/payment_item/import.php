<?php
// kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$linterface = new interface_html();
$limport = new libimporttext();

$lpayment = new libpayment();
if($clear==1){
	$sql="DROP TABLE TEMP_PAYMENT_ITEM_IMPORT";
	$lpayment->db_db_query($sql);
}

$sql = "CREATE TABLE IF NOT EXISTS TEMP_PAYMENT_ITEM_IMPORT (
                                 ClassName varchar(20),
                                 ClassNumber varchar(20),
                                 Amount float
                                )";
$lpayment->db_db_query($sql);


$itemName = $lpayment->returnPaymentItemName($ItemID);
$count = $lpayment->returnPaymentItemStudentCount($ItemID);

## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'javascript:history.back()',$button_import,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'index.php',$itemName,'javascript:history.back()',$button_import,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);

$sql = "SELECT COUNT(*) FROM TEMP_PAYMENT_ITEM_IMPORT";
$temp = $lpayment->returnVector($sql);
$lcount = $temp[0];

if ($lcount != 0)
{
?>
<form name="form1" method="GET" action="">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td><?=$i_Payment_Import_ConfirmRemoval?></td></tr>
<tr><td><?=$i_Payment_Import_ConfirmRemoval2?></td></tr>
<tr><td><input type=image src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=ItemID value="<?=$ItemID?>">
<input type=hidden name=clear value=1>
</form>
<?
}
else
{
?>
<form name="form1" method="POST" action="import_confirm.php" enctype="multipart/form-data">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_Payment_Field_PaymentItem; ?>:</td><td><?=$itemName?></td></tr>
<tr><td align=right nowrap><?php echo $i_Payment_Payment_PaidCount; ?>:</td><td><?=$count[1]?></td></tr>
<tr><td align=right nowrap><?php echo $i_Payment_Payment_UnpaidCount; ?>:</td><td><?=$count[0]?></td></tr>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile><br />
<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td></tr>
<!--<tr><td align=right nowrap>&nbsp;</td><td>
<br><?=$i_Payment_Payment_StudentImportFileDescription1?>
<br><a class=functionlink_new href=sample.csv><?=$i_general_clickheredownloadsample?></a>
<br>
</td></tr>
<tr><td align=right nowrap>&nbsp;</td><td>
<br><?=$i_Payment_Payment_StudentImportFileDescription2?>
<br><a class=functionlink_new href=sample2.csv><?=$i_general_clickheredownloadsample?></a>
<br>
<br><?=$i_Payment_Payment_StudentImportNotice?>
</td></tr>-->
<tr><td align=right nowrap><?=$i_general_Format?>:</td><td valign=top>
	<table border=0 cellpadding=0 cellspacing=0><tr><Td>
<input type=radio name=format value=1 CHECKED></td><td><?=$i_Payment_Payment_StudentImportFileDescription1?><Br>&nbsp;<a class=functionlink_new href="<?= GET_CSV("sample.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a></td></tr>
	<tr><td colspan='2'>&nbsp;</td></tr>
<tr><td><input type=radio name=format value=2></td><td><?=$i_Payment_Payment_StudentImportFileDescription2?><br>&nbsp;<a class=functionlink_new href="<?= GET_CSV("sample2.csv")?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>
</td></tr></table></td></tr>
<tr><td nowrap colspan='2'>&nbsp;<Br>
<?=$i_Payment_Payment_StudentImportNotice?><BR>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
<input type=hidden name=ItemID value="<?=$ItemID?>">
</form>


<?
}
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
