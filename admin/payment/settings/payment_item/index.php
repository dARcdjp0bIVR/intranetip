<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 2;
$order = ($order == 1) ? 1 : 0;

if ($CatID != "")
{
    $conds = " AND a.CatID = $CatID";
}

if ($itemPaidStatus <> ""){
	if ($itemPaidStatus == 0)
		$conds .= " AND (c.countPaid <> c.TotalCount OR c.TotalCount = 0) ";
	else if ($itemPaidStatus == 1)
		$conds .= " AND c.countPaid = c.TotalCount AND c.TotalCount <> 0 ";
	else if($itemPaidStatus == 2 ){ # Not Yet Started
		$today = date('Y-m-d');
		$conds .=" AND DATE_FORMAT(a.StartDate,'%Y-%m-%d') > '$today' ";
	}
}

$itemStatus=$itemStatus==""?0:$itemStatus;

	if($itemStatus==1){
		$conds2 =" AND a.RecordStatus=1 ";
	}else{
		$conds2 =" AND a.RecordStatus=0 ";
	}


	# date range
	$today_ts = strtotime(date('Y-m-d'));
	if($FromDate=="")
		$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
	if($ToDate=="")
		$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

#$datetype=$datetype!=2?1:2;
#if($datetype==2)
#	$date_conds =" AND DATE_FORMAT(a.EndDate,'%Y-%m-%d') BETWEEN '$FromDate' AND '$ToDate' ";
#else 
	$date_conds =" AND( (DATE_FORMAT(a.StartDate,'%Y-%m-%d') <= '$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>= '$FromDate' )) ";


//+++ get payment info of each payment item
// - get the payment id of all requested payment item
$ldb = new libdb();

$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$ldb->db_db_query($temp_table_sql);

$temp_table_sql = "CREATE TABLE TEMP_PAYMENT_ITEM_SUMMARY (
				pid int,
				sumUnpaid varchar(255),
				sumPaid varchar(255),
				TotalPayment varchar(255),
				countUnpaid int,
				countPaid int,
				TotalCount int
			)";
$ldb->db_db_query($temp_table_sql);

$pValue="";

$all_pid_sql  = "SELECT a.ItemID 
				 FROM
					 PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON a.CatID = b.CatID
				 WHERE
					  (a.Name LIKE '%$keyword%' OR
					   a.Description LIKE '%$keyword%'
					  )
                ";

$allPaymentID = $ldb->returnVector($all_pid_sql);
// - get the summay of payment :
// - Sum of unpaid amount, Sum of paid amount, Count of unpaid student, Count of paid student
if (sizeOf($allPaymentID)>0)
	for ($i=0; $i<sizeOf($allPaymentID); $i++){
		$allPaymentIDSummary[$i] = $lpayment->returnPaymentPaidSummary($allPaymentID[$i]);

		if ($i>0) $pValue .= ","; 
		$pValue .=
							"(".$allPaymentID[$i].",'"
							.number_format($allPaymentIDSummary[$i][0],1,".",",")."','"
							.number_format($allPaymentIDSummary[$i][1],1,".",",")."','"
							.number_format(($allPaymentIDSummary[$i][0]+$allPaymentIDSummary[$i][1]),1,".",",")."',"
							.$allPaymentIDSummary[$i][2].",".$allPaymentIDSummary[$i][3]."," 
							.($allPaymentIDSummary[$i][2]+$allPaymentIDSummary[$i][3]).")";
	}

$insert_temp_sql = "INSERT INTO TEMP_PAYMENT_ITEM_SUMMARY ( pid, sumUnpaid, sumPaid, TotalPayment, countUnpaid, countPaid,TotalCount ) VALUES $pValue";
$ldb->db_db_query($insert_temp_sql);
//+++ end of getting payment


$last_updated_field="IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,''))";


$sql  = "SELECT
               CONCAT('<a href=list.php?ItemID=',a.ItemID,'>',a.Name,'</a>')
               ,b.Name, a.DisplayOrder, a.PayPriority, 
			   CONCAT(c.countPaid,'<BR>(',c.TotalCount,')') as countInfo,
			   IF(a.Description IS NULL OR a.Description='','&nbsp;',a.Description),
			   IF(a.ProcessingAdminUser IS NOT NULL AND a.ProcessingAdminUser!='',a.ProcessingAdminUser,
			   		IF(a.ProcessingTerminalUser IS NOT NULL AND a.ProcessingTerminalUser!='',a.ProcessingTerminalUser,'&nbsp;')),
               DATE_FORMAT(a.StartDate,'%Y-%m-%d'),
               DATE_FORMAT(a.EndDate,'%Y-%m-%d'),
               CONCAT('<input type=checkbox name=ItemID[] value=', a.ItemID ,'>')
         FROM
             PAYMENT_PAYMENT_ITEM as a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY as b ON (a.CatID = b.CatID) LEFT OUTER JOIN TEMP_PAYMENT_ITEM_SUMMARY as c ON (c.pid = a.ItemID)
         WHERE
              (a.Name LIKE '%$keyword%' OR
               a.Description LIKE '%$keyword%'
              )
              $conds $date_conds $conds2
                ";
                
                
# TABLE INFO

/*
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Name","b.Name","a.DisplayOrder","a.PayPriority","a.Description","a.StartDate","a.EndDate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;
*/
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Name","b.Name","a.DisplayOrder","a.PayPriority","countInfo","a.Description","$last_updated_field","a.StartDate","a.EndDate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_PaymentItem)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_PaymentCategory)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_DisplayOrder)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Payment_Field_PayPriority)."</td>\n";

$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Payment_Field_PaidCount."<br>(".$i_Payment_Field_TotalPaidCount.")")."</td>\n";

$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_general_description)."</td>\n";
$li->column_list .= "<td class=tableTitle>".$li->column($pos++, $i_general_last_modified_by)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_general_startdate)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_general_enddate)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("ItemID[]")."</td>\n";

$cats = $lpayment->returnPaymentCats();
$select_cat = getSelectByArray($cats,"name=CatID onChange=this.form.submit()",$CatID,1);

$select_item_paid_status = "<SELECT name=itemPaidStatus onChange=this.form.submit()>\n";
$select_item_paid_status .= "<OPTION value='' ".(!isset($itemPaidStatus) || $itemPaidStatus==""? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_item_paid_status .= "<OPTION value=2 ".(isset($itemPaidStatus) && $itemPaidStatus=="2"? "SELECTED":"").">$i_Payment_PaymentStatus_NotStarted</OPTION>\n";
$select_item_paid_status .= "<OPTION value=1 ".(isset($itemPaidStatus) && $itemPaidStatus==1? "SELECTED":"").">$i_Payment_Field_AllPaid</OPTION>\n";
$select_item_paid_status .= "<OPTION value=0 ".(isset($itemPaidStatus) && $itemPaidStatus=="0"? "SELECTED":"").">$i_Payment_Field_NotAllPaid</OPTION>\n";
$select_item_paid_status .= "</SELECT>\n";


$select_item_status="<SELECT name=itemStatus onChange=this.form.submit()>\n";
//$select_item_status.= "<OPTION value='' ".($itemStatus==""? "SELECTED":"").">$i_status_all</OPTION>\n";
$select_item_status.="<OPTION value=0 ".($itemStatus=="0"?" SELECTED":"").">$i_Payment_Menu_Settings_PaymentItem_ActiveRecord</OPTION>";
$select_item_status.="<OPTION value=1 ".($itemStatus=="1"?" SELECTED":"").">$i_Payment_Menu_Settings_PaymentItem_ArchivedRecord</OPTION>";
$select_item_status.="</SELECT>";

if($itemStatus!=1){
	$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>&nbsp;";
}else $toolbar="";

$toolbar .= "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar.= "&nbsp;<a class=iconLink href=javascript:exportPage(document.form1,'item_export.php')>".exportIcon()."$button_export</a>";


$functionbar .="$select_item_status&nbsp;";
$functionbar .= "$select_item_paid_status";

$functionbar .= "$select_cat&nbsp;";
if($itemStatus!=1){
	$functionbar .= "<a href=\"javascript:checkArchive(document.form1,'ItemID[]','archive.php')\"><img src='/images/admin/button/t_btn_archive_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
	$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'ItemID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
	$functionbar .= "<a href=\"javascript:checkRemove2(document.form1,'ItemID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
}else{
	$functionbar.="<a href=\"javascript:checkUndoArchive(document.form1,'ItemID[]','archive_undo.php')\"><img src='/images/admin/button/t_btn_restore_$intranet_session_language.gif' border=0 align='absmiddle'></a>&nbsp;\n";
}
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";


## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentItem,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings_PaymentItem,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
	          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           		document.forms['form1'].FromDate.value = dateValue;
                           
          }
          function calendarCallback2(date, month, year)
          {								
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
		                           document.forms['form1'].ToDate.value = dateValue;

          }
	function checkForm(formObj){
		if(formObj==null)return false;
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			if(!checkDate(fromV)){
					//formObj.FromDate.focus();
					return false;
			}
			else if(!checkDate(toV)){
						//formObj.ToDate.focus();
						return false;
			}
				return true;
	}
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}
	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}
	function checkRemove2(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
        }
}
function checkArchive(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
	        if(confirm('<?=$i_Payment_Menu_Settings_PaymentItem_Archive_Warning?>')){
                obj.action=page;                
                obj.method="POST";
                obj.submit();	
            }			             
        }
}
function checkUndoArchive(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
	        if(confirm('<?=$i_Payment_Menu_Settings_PaymentItem_UndoArchive_Warning?>')){
                obj.action=page;                
                obj.method="POST";
                obj.submit();	
            }			             
        }	
}
function openPrintPage()
{
	 q_str = "FromDate=<?=$FromDate?>&ToDate=<?=$ToDate?>&CatID=<?=$CatID?>&itemStatus=<?=$itemStatus?>&itemPaidStatus=<?=$itemPaidStatus?>&keyword=<?=$keyword?>&pageNo=<?=$li->pageNo?>&order=<?=$li->order?>&field=<?=$li->field?>&numPerPage=<?=$li->page_size?>";
	 url="item_print.php?"+q_str; 
     newWindow(url,8);
}
function exportPage(obj,url){
		old_url = obj.action;
        obj.action=url;
        obj.submit();
        obj.action = old_url;
        
}

</script>
<form name="form1" method="get">
<!-- date range -->
<table border=0 width=560 align=center>
	<tr>
		<td nowrap class=tableContent><?=$i_Payment_Menu_Settings_PaymentItem_SelectDateRange?>:
		<br><?=$i_Profile_From?>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;
		 	<?=$i_Profile_To?>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 </td></tr>
<!-- <tr><Td><input type=radio name=datetype value='1' <?=($datetype==1?"checked":"")?>><?=$i_general_startdate?>&nbsp;&nbsp;<input type=radio name=datetype value='2'  <?=($datetype==2?"checked":"")?>><?=$i_general_enddate?></td></tr>-->
 <tr><td><a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<BR>
<?php if($itemStatus==1){?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<?=$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2?>
</td></tr>
</table>
<?php } ?>
<BR>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");

# remove the temp table
$temp_table_sql = "DROP TABLE TEMP_PAYMENT_ITEM_SUMMARY";
$ldb->db_db_query($temp_table_sql);

intranet_closedb();
?>
