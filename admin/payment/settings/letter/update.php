<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

$letter_head_file = "$intranet_root/file/pay_out_letter_head.txt";
$letter_head2_file = "$intranet_root/file/pay_out_letter_head2.txt";
$letter_foot_file = "$intranet_root/file/pay_out_letter_foot.txt";
$letter_signature_option_file = "$intranet_root/file/pay_out_letter_signature_option.txt";

//$header = intranet_htmlspecialchars($header);
//$footer = intranet_htmlspecialchars($footer);


write_file_content($header,$letter_head_file);

write_file_content($header2,$letter_head2_file);

write_file_content($footer,$letter_foot_file);


write_file_content($signature_disabled, $letter_signature_option_file);

header("Location: index.php?msg=2");
?>
