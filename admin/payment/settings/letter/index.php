<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$letter_head_file = "$intranet_root/file/pay_out_letter_head.txt";
$letter_head2_file = "$intranet_root/file/pay_out_letter_head2.txt";
$letter_foot_file = "$intranet_root/file/pay_out_letter_foot.txt";
$letter_signature_option_file = "$intranet_root/file/pay_out_letter_signature_option.txt";

$letter_head = get_file_content($letter_head_file);
$letter_head2 = get_file_content($letter_head2_file);
$letter_foot = get_file_content($letter_foot_file);
$letter_signature_option = get_file_content($letter_signature_option_file);

$letter_head = stripslashes($letter_head);
$letter_head2 = stripslashes($letter_head2);
$letter_foot = stripslashes($letter_foot);
$letter_signature_option = trim($letter_signature_option);

?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_Letter,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>
<form name=form1 action="update.php" method=post>
<table width=560 border=0 cellpadding=2 cellspacing=2 align=center>
<tr><td><?=$i_Payment_Menu_Settings_Letter_Header?>:<Br>
<textarea name='header' cols=60 rows=10><?=$letter_head?></textarea>
<br>&nbsp;
</td></tr>
<tr><td><?=$i_Payment_Menu_Settings_Letter_Header2?>:<Br>
<textarea name='header2' cols=60 rows=10><?=$letter_head2?></textarea>
<br>&nbsp;
</td></tr>
<tr><td><?=$i_Payment_Menu_Settings_Letter_Footer?>:<Br>
<textarea name='footer' cols=60 rows=10><?=$letter_foot?></textarea>
</td></tr>
<tr><td><input type="checkbox" name="signature_disabled" value="1" <?=($letter_signature_option==1?"CHECKED":"")?>> <?=$i_Payment_Menu_Settings_Letter_Disable_Signature?></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="../index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
include_once("../../../../templates/adminfooter.php");
?>
