<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$ips = get_file_content("$intranet_root/file/payment_ip.txt");
$st_expiry = trim(get_file_content("$intranet_root/file/payment_expiry.txt"));
$authfile = get_file_content("$intranet_root/file/payment_auth.txt");
$auth = explode("\n",$authfile);
$payment_no_auth = ($auth[0]==1);
$purchase_no_auth = ($auth[1]==1);

if ($st_expiry=="" || !is_numeric($st_expiry))
{
    $st_expiry = $payment_expiry;
    write_file_content($st_expiry,"$intranet_root/file/payment_expiry.txt");
}



?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_TerminalIP,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name=form1 method=POST action=update.php>
<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_SmartCard_Terminal_IPList; ?>:</td><td><textarea name=IPList COLS=40 ROWS=5><?=$ips?></textarea>
 <br><span class="extraInfo"><?=$i_SmartCard_Terminal_IPInput?></span>
 <br><span class="extraInfo"><?=$i_SmartCard_Terminal_YourAddress?>:<?=$HTTP_SERVER_VARS['REMOTE_ADDR']?></span></td></tr>
<tr><td align=right><?php echo $i_Payment_Auth_PaymentRequired; ?>:</td><td><input type=checkbox name=payment_auth value=1 <?=($payment_no_auth?"CHECKED":"")?>></td></tr>
<tr><td align=right><?php echo $i_Payment_Auth_PurchaseRequired; ?>:</td><td><input type=checkbox name=purchase_auth value=1 <?=($purchase_no_auth?"CHECKED":"")?>></td></tr>
<tr><td align=right><?php echo $i_SmartCard_Terminal_ExpiryTime; ?>:</td><td><input type=text size=7 name=st_expiry value="<?=$st_expiry?>"></td></tr>
</table>
<br>
<?=$i_SmartCard_Description_Terminal_IP_Settings?>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>


</form>
<?
include_once("../../../../templates/adminfooter.php");
?>