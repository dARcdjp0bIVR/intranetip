<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();


$select_status = "<SELECT name=Status>\n";
$select_status .= "<OPTION value=1 >$i_general_active</OPTION>";
$select_status .= "<OPTION value='' >$i_general_inactive</OPTION>";
$select_status .= "</SELECT>\n";


## echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentCategory,'index.php',$button_new,'');
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Subsidy_Setting,'index.php',$button_new,'');

echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<script language='javascript'>
function checkform(){
	obj = document.form1;
	if(obj==null) return;
	
	objName = obj.UnitName;
	objAmount = obj.TotalAmount;
	if(!check_text(objName,'<?=$i_Payment_Subsidy_Warning_Enter_UnitName?>')) return;
	if(!check_numeric(objAmount,'','<?=$i_Payment_Warning_InvalidAmount?>')) return;

	obj.submit();

}
function resetform(){
	obj = document.form1;
	if(obj==null) return;
	obj.reset();
}
</script>
<form name=form1 action="new_update.php" method=POST onSubmit='return checkform(this)'>
<table width=90% border=0 align=center>
<tr><td align=right nowrap><?=$i_Payment_Subsidy_UnitName?>:</td><td><input type='text' name='UnitName' size=50  value=''></td></tr>
<tr><td align=right nowrap><?=$i_Payment_Subsidy_Total_Amount?>:</td><td><input type='text' name='TotalAmount' value=''></td></tr>
<tr><td align=right nowrap><?=$i_general_status?>:</td><td><?=$select_status?></td></tr>
</td></tr>
</table>
<BR>
<table width=90% border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<a href="javascript:checkform()"><img src='<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a> 
<a href='index.php'><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td></tr>
</table>
</form>
<?

include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
