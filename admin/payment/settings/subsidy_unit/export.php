<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lpayment = new libpayment();
$lexport = new libexporttext();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;



if($status==""){
	$conds="";
}
else if($status==0){
	$conds = " AND a.RecordStatus!=1 ";
}
else if($status==1){
	$conds = " AND a.RecordStatus=1";
}



$sql="SELECT a.UnitName,
	".$lpayment->getExportAmountFormatDB("SUM(b.SubsidyAmount)")." AS abc,
	".$lpayment->getExportAmountFormatDB("a.TotalAmount")." 
	FROM PAYMENT_SUBSIDY_UNIT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.UnitID = b.SubsidyUnitID)
	WHERE (a.UnitName LIKE '%$keyword%' OR
               a.TotalAmount LIKE '%$keyword%' OR
               a.UnitCode LIKE '%$keyword%'
    )
    $conds
    GROUP BY a.UnitID
";
 
$field_array = array("a.UnitName","abc","a.TotalAmount");

$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();

$temp = $li->returnArray($sql,3);



$x="\"$i_Payment_Subsidy_Unit\",";
$x.="\"$i_Payment_Subsidy_Used_Amount\",";
$x.="\"$i_Payment_Subsidy_Total_Amount\"\n";

$utf_x=$i_Payment_Subsidy_Unit."\t";
$utf_x.=$i_Payment_Subsidy_Used_Amount."\t";
$utf_x.=$i_Payment_Subsidy_Total_Amount."\t\r\n";


if(sizeof($temp)<=0){
        $x.="\"$i_no_record_exists_msg\"\n";
        $utf_x.=$i_no_record_exists_msg."\r\n";
}else{
	for($i=0;$i<sizeof($temp);$i++){
		list($unit_name,$used_amount,$total_amount)=$temp[$i];
		$x.="\"$unit_name\",\"$used_amount\",\"$total_amount\"\n";
		$utf_x.=$unit_name."\t".$used_amount."\t".$total_amount."\r\n";
	}
}

$content = $x;

$utf_content=$utf_x;


intranet_closedb();

$filename = "subsidy_unit.csv";
if ($g_encoding_unicode) {
        $lexport->EXPORT_FILE($filename, $utf_content);
} else {
        output2browser($content,$filename);
}


?>
