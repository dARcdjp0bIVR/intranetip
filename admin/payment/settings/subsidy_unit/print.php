<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();


$lpayment = new libpayment();

if (!isset($order)) $order = 1;
if (!isset($field)) $field = 0;
$order = ($order == 1) ? 1 : 0;



if($status==""){
	$conds="";
}
else if($status==0){
	$conds = " AND a.RecordStatus!=1 ";
}
else if($status==1){
	$conds = " AND a.RecordStatus=1";
}



$sql="SELECT a.UnitName,
	".$lpayment->getWebDisplayAmountFormatDB("SUM(b.SubsidyAmount)")." AS abc,
	IF(a.TotalAmount IS NULL,".$lpayment->getWebDisplayAmountFormatDB("0").",".$lpayment->getWebDisplayAmountFormatDB("a.TotalAmount").")
	FROM PAYMENT_SUBSIDY_UNIT AS a LEFT OUTER JOIN PAYMENT_PAYMENT_ITEMSTUDENT AS b ON (a.UnitID = b.SubsidyUnitID)
	WHERE (a.UnitName LIKE '%$keyword%' OR
               a.TotalAmount LIKE '%$keyword%' OR
               a.UnitCode LIKE '%$keyword%'
    )
    $conds
    GROUP BY a.UnitID
";

    
$field_array = array("a.UnitName","abc","a.TotalAmount");
$sql .= " ORDER BY ";
$sql .= (count($field_array)<=$field) ? $field_array[0] : $field_array[$field];
$sql .= ($order==0) ? " DESC" : " ASC";
$li = new libdb();



$temp = $li->returnArray($sql,3);

$x="<table width=90% border=0 cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x.="<Tr class='$css_table_title'>";
$x.="<td class='$css_table_title'>#</td>";
$x.="<td width=40%  class='$css_table_title'>$i_Payment_Subsidy_Unit</td><td width=30%  class='$css_table_title'>$i_Payment_Subsidy_Used_Amount</td><td width=30%  class='$css_table_title'>$i_Payment_Subsidy_Total_Amount</td>";
$x.="</tr>";
for($i=0;$i<sizeof($temp);$i++){
	list($unit_name,$used_amount,$total_amount)=$temp[$i];
	//$css =$i%2==0?"tableContent":"tableContent2";
	$css =$i%2==0?$css_table_content:$css_table_content."2";
	$x.="<tr class='$css'>";
	$x.="<td class='$css'>".($i+1)."</td>";
	$x.="<td class='$css'>$unit_name</td><Td class='$css'>$used_amount</td><td class='$css'>$total_amount</td>";
	$x.="</tr>";
}
if(sizeof($temp)<=0){
        $x.="<tr><td colspan=4 align=center class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$x.="</table>";

$display=$x;

include_once("../../../../templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
?>
<table border=0 width=90% align=center>
<tr><td><b><? /* echo $i_Payment_Subsidy_Setting */ ?></b></td></tr>
</table>
<?=$display?>
<BR>
<table border=0 width=90% align=center>
<tr><td align=center><a href='javascript:window.print()'><img src='/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif' border=0 align='absmiddle'></a></td></tr>
</table>
<BR><BR>
<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
