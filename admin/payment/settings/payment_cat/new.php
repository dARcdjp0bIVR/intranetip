<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lpayment = new libpayment();
$nextOrder = $lpayment->getPaymentCatNextDisplayOrder();

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_PaymentCategory,'index.php',$button_new,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);

?>
<form name=form1 action="new_update.php" method=POST>
<table width=90% border=0 align=center>
<tr><td><?=$i_Payment_Field_PaymentCategory?></td><td><input type=text name=CatName size=50 maxlength=255></td></tr>
<tr><td><?=$i_Payment_Field_DisplayOrder?></td><td><input type=text name=DisplayOrder size=10 maxlength=10 value="<?=$nextOrder?>"></td></tr>
<tr><td><?=$i_general_description?></td><td><TEXTAREA rows=5 cols=50 name=Description></TEXTAREA></td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</form>
<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
