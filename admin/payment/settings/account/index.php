<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

$sql  = "SELECT
               Username
               , IF(PaymentAllowed=1,'<img src=$image_path/frontpage/campusmail/icon_notification.gif>','&nbsp;')
               , IF(PurchaseAllowed=1,'<img src=$image_path/frontpage/campusmail/icon_notification.gif>','&nbsp;')
               , LastLogin, DateInput,
               CONCAT('<input type=checkbox name=TerminalUserID[] value=', TerminalUserID ,'>')
         FROM
             PAYMENT_TERMINAL_USER
                ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("Username","PaymentAllowed","PurchaseAllowed","LastLogin","DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(0, $i_Payment_Field_Username)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_Payment_Field_PaymentAllowed)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_Payment_Field_PurchaseAllowed)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(3, $i_Payment_Field_LastLogin)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_Payment_Field_AccountCreation)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("TerminalUserID[]")."</td>\n";

$toolbar = "<a class=iconLink href=javascript:checkNew('new.php')>".newIcon()."$button_new</a>";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'TerminalUserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TerminalUserID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_Settings,'../',$i_Payment_Menu_Settings_TerminalAccount,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
