<?php
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");

intranet_opendb();

$lclass = new libclass();
$lidb = new libdb();

$type_selection = array(
array(1,$i_ClassLevel),
array(2,$i_ClassName),
array(3,$i_SmartCard_Payment_photoCopier_UserType)
);

$add_type = getSelectByArray($type_selection,"name=targetType onChange=\"this.form.submit()\"",$targetType,1);


if($targetType == "")
{
	$table_content .= "<tr><td>";
	$table_content .= "<table width=60% border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
	$table_content .= "<tr><td width=20%>$i_SmartCard_Payment_PhotoCopier_TotalQuota</td><td><input type=text name=total_quota></td></tr>";
	$table_content .= "</table>";
	$table_content .= "<table width=60% border=0 align=center>";
	//$table_content .= "<tr><td align=right><input type=image onClick=\"this.form.flag.value=0; checkForm()\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	//$table_content .= "<tr><td align=right><img src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\" onClick=\"checkForm()\"></a></td></tr>";
	$table_content .= "<input type=hidden name=flag value=0>";
	$table_content .= "<input type=hidden name=add_type value=0>";
	$table_content .= "</table>";
	$table_content .= "</td></tr>";
}

if($targetType == 1)
{
	$sql = "SELECT DISTINCT a.ClassLevelID, a.LevelName FROM INTRANET_CLASSLEVEL AS a INNER JOIN INTRANET_CLASS AS b ON (a.ClassLevelID = b.ClassLevelID) WHERE b.RecordStatus = 1";
	$classLevel = $lidb->returnArray($sql,2);
	
	$table_content .= "<tr><td>";
	$table_content .= "<table width=60% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
	$table_content .= "<tr class=tabletitle><td width=20%>$i_ClassLevel</td><td>$i_SmartCard_Payment_PhotoCopier_TotalQuota</td><td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'LevelID[]'):setChecked(0,this.form,'LevelID[]')></td></tr>";	
	for($i=0; $i<sizeof($classLevel); $i++)
	{
		list($levelID, $levelName) = $classLevel[$i];
		$table_content .= "<tr><td>$levelName</td>";
		$table_content .= "<td><input type=text name=total_quota_$levelID disabled=true style='background-color:#CCCCCC'></td>";
		$table_content .= "<td><input type=checkbox name=LevelID[] value=$levelID onClick='this.checked? checkClicked(1, $levelID) : checkClicked(0, $levelID)'></td></tr>";
	}
	
	$table_content .= "</table>";
	$table_content .= "<table width=60% border=0 align=center>";
	//$table_content .= "<tr><td width=20%></td><td colspan=2 align=right><input type=image onClick=\"this.form.flag.value=1; checkForm(document.form1,'LevelID[]')\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	//$table_content .= "<tr><td width=20%></td><td colspan=2 align=right><input type=image onClick=\"checkForm(document.form1,'LevelID[]')\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	$table_content .= "<input type=hidden name=flag value=1>";
	$table_content .= "<input type=hidden name=add_type value=1>";
	$table_content .= "</table>";
	$table_content .= "</td></tr>";
}

if($targetType == 2)
{
	$lclass = new libclass();
	$classes = $lclass->getClassList();
	$currLvl = $classes[0][2];
	
	$table_content .= "<tr><td>";
	$table_content .= "<table width=60% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
	$table_content .= "<tr class=tabletitle><td width=20%>$i_ClassLevel</td><td>$i_SmartCard_Payment_PhotoCopier_TotalQuota</td><td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'ClassID[]'):setChecked(0,this.form,'ClassID[]')></td></tr>";
	
	for ($i=0; $i<sizeof($classes); $i++)
	{
	     list($cid, $cname, $clvl) = $classes[$i];
	     if ($currLvl != $clvl)
	     {
	         $currLvl = $clvl;
	     }
		$table_content .= "<tr><td>$cname</td>";
		$table_content .= "<td><input type=text name=total_quota_$cid disabled=true style='background-color:#CCCCCC'></td>";
		$table_content .= "<td><input type=checkbox name=ClassID[] value=$cid onClick='this.checked? checkClicked(1, $cid) : checkClicked(0, $cid)'></td></tr>";
	}

	$table_content .= "</table>";
	$table_content .= "<table width=60% border=0 align=center>";
	//$table_content .= "<tr><td align=right><input type=image onClick=\"this.form.flag.value=2; checkForm(document.form1,'ClassID[]')\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	//$table_content .= "<tr><td align=right><input type=image onClick=\"checkForm(document.form1,'ClassID[]')\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	$table_content .= "<input type=hidden name=flag value=2>";
	$table_content .= "<input type=hidden name=add_type value=2>";
}

if($targetType == 3)
{
	$usertype = array(
	array(1,$i_identity_teachstaff),
	array(2,$i_identity_student),
	);

	$table_content .= "<tr><td>";
	$table_content .= "<table width=60% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
	$table_content .= "<tr class=tabletitle><td width=20%>$i_SmartCard_Payment_photoCopier_UserType</td><td>$i_SmartCard_Payment_PhotoCopier_TotalQuota</td><td><input type=checkbox onClick=(this.checked)?setChecked(1,this.form,'TypeID[]'):setChecked(0,this.form,'TypeID[]')></td></tr>";
	
	for($i=0; $i<sizeof($usertype); $i++)
	{
		list($type_index, $type_name) = $usertype[$i];
		$table_content .= "<tr><td>$type_name</td>";
		$table_content .= "<td><input type=text name=total_quota_$type_index disabled=true style='background-color:#CCCCCC'></td>";
		$table_content .= "<td><input type=checkbox name=TypeID[] value=$type_index onClick='this.checked? checkClicked(1, $type_index) : checkClicked(0, $type_index)'></td></tr>";
	}
	
	$table_content .= "</table>";
	$table_content .= "<table width=60% border=0 align=center>";
	//$table_content .= "<tr><td align=right><input type=image onClick=\"this.form.flag.value=3; checkForm(document.form1,'TypeID[]')\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	//$table_content .= "<tr><td align=right><input type=image onClick=\"checkForm(document.form1,'TypeID[]')\" src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	$table_content .= "<input type=hidden name=flag value=3>";
	$table_content .= "<input type=hidden name=add_type value=3>";
}

?>

<SCRIPT Language="JavaScript">
function setChecked(val, obj, element_name){
        len=obj.elements.length;
        var i=0;
        var temp;
        
        if (val == 1)
        {
	        for( i=0 ; i<len ; i++) 
	        {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
            	}
            }
            <?
            if(sizeof($classes)>0)
            {
		            
	            for ($i=0; $i<sizeof($classes); $i++)
	            {
		            list($cid, $cname, $clvl) = $classes[$i];
			?>
	            	temp = eval("document.form1.total_quota_"+<?=$cid;?>);
	            	temp.disabled=false;
		            temp.value="0";
	            	temp.style.backgroundColor="";
            <?
        		}
        	}
        	if(sizeof($classLevel)>0)
        	{
	        	for($i=0; $i<sizeof($classLevel); $i++)
	        	{
		        	list($levelID, $levelName) = $classLevel[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$levelID;?>);
	            	temp.disabled=false;
		            temp.value="0";
	            	temp.style.backgroundColor="";
	        <?
        		}
    		}
    		if(sizeof($usertype)>0)
        	{
	        	for($i=0; $i<sizeof($usertype); $i++)
	        	{
		        	list($type_index, $type_name) = $usertype[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$type_index;?>);
	            	temp.disabled=false;
		            temp.value="0";
	            	temp.style.backgroundColor="";
	        <?
        		}
    		}
        	?>            
        }
        else
        {
	        for( i=0 ; i<len ; i++) 
	        {
                if (obj.elements[i].name==element_name)
                {
                	obj.elements[i].checked=val;
            	}
            }
            <?
            if(sizeof($classes)>0)
            {
		            
	            for ($i=0; $i<sizeof($classes); $i++)
	            {
		            list($cid, $cname, $clvl) = $classes[$i];
			?>
	            	temp = eval("document.form1.total_quota_"+<?=$cid;?>);
	            	temp.disabled=true; 
					temp.value="";
					temp.style.backgroundColor="#CCCCCC";
            <?
        		}
        	}
        	if(sizeof($classLevel)>0)
        	{
	        	for($i=0; $i<sizeof($classLevel); $i++)
	        	{
		        	list($levelID, $levelName) = $classLevel[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$levelID;?>);
	            	temp.disabled=true; 
					temp.value="";
					temp.style.backgroundColor="#CCCCCC";
	        <?
        		}
    		}
    		if(sizeof($usertype)>0)
        	{
	        	for($i=0; $i<sizeof($usertype); $i++)
	        	{
		        	list($type_index, $type_name) = $usertype[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$type_index;?>);
	            	temp.disabled=true; 
					temp.value="";
					temp.style.backgroundColor="#CCCCCC";
	        <?
        		}
    		}
        	?>
		}
}


function checkClicked(cnt, id)
{
	var temp;
	temp = eval("document.form1.total_quota_"+id);
	if (cnt == 1)
	{
		temp.disabled=false; 
		temp.value="0";
		temp.style.backgroundColor="";
	}
	else
	{
		temp.disabled=true; 
		temp.value="";
		temp.style.backgroundColor="#CCCCCC";
	}
}

function checkForm(obj,element)
{
	if(document.form1.flag.value == 0)
	{
		if(document.form1.total_quota.value == "")
		{
			alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
			return false;
		}
		else
		{
			if ((isNaN(document.form1.total_quota.value)) && (document.form1.total_quota.value != ""))
			{
				alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
				return false;
			}
				else
			{
				cnt = 1;
			}
		}
	}
	
	if((document.form1.flag.value==1)||(document.form1.flag.value==2)||(document.form1.flag.value==3))
	{
		var obj = document.form1;
		if(obj.flag.value==1)
		{
			var element = 'LevelID[]';
		}
		if(obj.flag.value==2)
		{
			var element = 'ClassID[]';
		}
		if(obj.flag.value==3)
		{
			var element = 'TypeID[]';
		}
		
	    if(countChecked(obj,element)>=1)
	    {
		    <?
        	if(sizeof($classLevel)>0)
        	{
	        	for($i=0; $i<sizeof($classLevel); $i++)
	        	{
		        	list($levelID, $levelName) = $classLevel[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$levelID;?>);
	            	if((temp.disabled==false)&&(temp.value==""))
					{
						alert ('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
						return false;
					}
					else 
					{
						if ((temp.disabled==false)&&(temp.value!="")&&(isNaN(temp.value)))
						{
							alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
							return false;
						}
						else
						{
							cnt = 1;	
						}
					}
	        <?
        		}
    		}
            if(sizeof($classes)>0)
            {
	            for ($i=0; $i<sizeof($classes); $i++)
	            {
		            list($cid, $cname, $clvl) = $classes[$i];
			?>
	            	temp = eval("document.form1.total_quota_"+<?=$cid;?>);
	            	if((temp.disabled==false)&&(temp.value==""))
					{
						alert ('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
						return false;
					}
					else 
					{
						if ((temp.disabled==false)&&(temp.value!="")&&(isNaN(temp.value)))
						{
							alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
							return false;
						}
						else
						{
							cnt = 1;	
						}
					}
            <?
        		}
        	}
    		if(sizeof($usertype)>0)
        	{
	        	for($i=0; $i<sizeof($usertype); $i++)
	        	{
		        	list($type_index, $type_name) = $usertype[$i];
            ?>
            		temp = eval("document.form1.total_quota_"+<?=$type_index;?>);
	            	if((temp.disabled==false)&&(temp.value==""))
					{
						alert ('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction?>');
						return false;
					}
					else 
					{
						if ((temp.disabled==false)&&(temp.value!="")&&(isNaN(temp.value)))
						{
							alert('<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>');
							return false;
						}
						else
						{
							cnt = 1;
						}
					}
	        <?
        		}
    		}
        	?>
	    } 
	    else 
		{
			alert(globalAlertMsg1);
	    }
    }
    if (cnt == 1)
	{
		document.form1.action = "add_update.php";
		return true;
	}
    
}

</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../../',$i_Payment_Menu_PhotoCopier_Quota_Management,'../', $button_add, '') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<!--<form name=form1 ACTION="" method=POST onsubmit="return checkForm();">-->
<form name=form1 ACTION="" method=POST onSubmit="return checkForm()">
<table border=0 width=560 align=center>
<tr><td td width=20% height=40px><?=$i_alert_pleaseselect?></td><td><?=$add_type?></td></tr>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>
<tr><td colspan=2 height=10px></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td>
<?=$table_content?>
</td></tr>
</table>
<table width=340 border=0 align=center>
<tr><td align=right><?=btnSubmit()?></td></tr>
</table>
<!--<input type=hidden name=flag value=0>-->
</form>

<?php
include_once("../../../../../templates/filefooter.php");
intranet_closedb();
?>
