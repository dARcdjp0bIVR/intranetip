<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libpayment.php");
intranet_opendb();

$lpayment = new libpayment();
$lidb = new libdb();
if($add_type==0)
{
	$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType IN (1,2) AND RecordStatus IN (0,1,2)";
	$temp = $lidb->returnVector($sql);
	$user_list = implode(",",$temp);
	
	$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified) SELECT UserID, 0, 0, '', '', NOW(), NOW() FROM INTRANET_USER WHERE UserID IN ($user_list)";
	$lidb->db_db_query($sql);

	$updatePrintingQuotaLogCondition = "where userID in (".$user_list.")";
	$lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($total_quota,1,$updatePrintingQuotaLogCondition,$lidb);	

	$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = TotalQuota + $total_quota, DateModified = NOW() WHERE UserID IN ($user_list)";
	$lidb->db_db_query($sql);
}

if($add_type==1)
{
	if(sizeof($LevelID)>0)
	{
		for($i=0; $i<sizeof($LevelID); $i++)
		{
			$sql = "SELECT UserID FROM INTRANET_USER AS a INNER JOIN INTRANET_CLASS AS b ON (a.ClassName = b.ClassName) INNER JOIN INTRANET_CLASSLEVEL AS c ON (b.ClassLevelID = c.ClassLevelID) WHERE a.RecordType = 2 AND a.RecordStatus IN (0,1,2) AND c.ClassLevelID = $LevelID[$i]";
			$temp = $lidb->returnVector($sql);
			$user_list = implode(",",$temp);
			$user_arr[$LevelID[$i]] = $user_list; ### Update List ###
		}
		
		if(sizeof($user_arr)>0)
		{
			for($i=0; $i<sizeof($LevelID); $i++)
			{
				$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified) SELECT UserID, 0, 0, '', '', Now(), Now() FROM INTRANET_USER WHERE UserID IN (".$user_arr[$LevelID[$i]].")";
				$lidb->db_db_query($sql);
			}
		}
	}
	if(sizeof($user_arr)>0)
	{
		for($i=0; $i<sizeof($user_arr); $i++)
		{
			$totalQuota = ${"total_quota_".$LevelID[$i]};

			$updatePrintingQuotaLogCondition = "where userID in (".$user_arr[$LevelID[$i]].")";
			$lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,1,$updatePrintingQuotaLogCondition,$lidb);	


			$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = TotalQuota + $totalQuota, DateModified = NOW() WHERE UserID IN (".$user_arr[$LevelID[$i]].")";
			$lidb->db_db_query($sql);
		}
	}
}

if($add_type==2)
{
	if(sizeof($ClassID)>0)
	{
		for($i=0; $i<sizeof($ClassID); $i++)
		{
			$sql = "SELECT a.UserID FROM INTRANET_USER AS a INNER JOIN INTRANET_CLASS AS b ON (a.ClassName = b.ClassName) WHERE b.ClassID = $ClassID[$i] AND a.RecordType IN (1,2) AND a.RecordStatus IN (0,1,2)";
			$temp = $lidb->returnVector($sql);
			$user_list = implode(",",$temp);
			$user_arr[$ClassID[$i]] = $user_list; ### Update List ###
		}
		
		if(sizeof($user_arr)>0)
		{
			for($i=0; $i<sizeof($ClassID); $i++)
			{
				$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified) SELECT UserID, 0, 0, '', '', Now(), Now() FROM INTRANET_USER WHERE UserID IN (".$user_arr[$ClassID[$i]].")";
				$lidb->db_db_query($sql);
			}
		}
	}
	if(sizeof($user_arr)>0)
	{
		for($i=0; $i<sizeof($user_arr); $i++)
		{
			$totalQuota = ${"total_quota_$ClassID[$i]"};

			$updatePrintingQuotaLogCondition = "where userID in(".$user_arr[$ClassID[$i]].")";
			$lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,1,$updatePrintingQuotaLogCondition,$lidb);	

			$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET PAYMENT_PRINTING_QUOTA.TotalQuota = (PAYMENT_PRINTING_QUOTA.TotalQuota + $totalQuota), DateModified = NOW() WHERE UserID IN (".$user_arr[$ClassID[$i]].")";
			$lidb->db_db_query($sql);
		}
	}
}

if($add_type==3)
{
	if(sizeof($TypeID) > 0)
	{
		for($i=0; $i<sizeof($TypeID); $i++)
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = $TypeID[$i] AND RecordStatus IN (0,1,2)";
			$temp = $lidb->returnVector($sql);
			$user_list = implode(",",$temp);
			$user_arr[$TypeID[$i]] = $user_list;
		}
		if(sizeof($user_arr)>0)
		{
			for($i=0; $i<sizeof($TypeID); $i++)
			{
				$sql = "INSERT IGNORE INTO PAYMENT_PRINTING_QUOTA (UserID, TotalQuota, UsedQuota, RecordType, RecordStatus, DateInput, DateModified) SELECT UserID, 0, 0, '', '', Now(), Now() FROM INTRANET_USER WHERE UserID IN (".$user_arr[$TypeID[$i]].")";
				$lidb->db_db_query($sql);
			}
		}
	}
	if(sizeof($user_arr)>0)
	{
		for($i=0; $i<sizeof($user_arr); $i++)
		{
			$totalQuota = ${"total_quota_$TypeID[$i]"};

			$updatePrintingQuotaLogCondition = "where userID in(".$user_arr[$TypeID[$i]].")";
			$lpayment->update_PAYMENT_PRINTING_QUOTA_CHANGE_LOG($totalQuota,1,$updatePrintingQuotaLogCondition,$lidb);	

			$sql = "UPDATE PAYMENT_PRINTING_QUOTA SET TotalQuota = TotalQuota + $totalQuota, DateModified = NOW() WHERE UserID IN (".$user_arr[$TypeID[$i]].")";
			$lidb->db_db_query($sql);
		}
	}
}

header ("Location: add.php?msg=2");
intranet_closedb();


?>
