<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lidb = new libdb();

if(isset($user_id))
{
	$user_list = implode(",", $user_id);
	
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$sql = "SELECT a.UserID, $namefield, b.TotalQuota, b.UsedQuota FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID) WHERE a.UserID IN ($user_list) AND a.RecordStatus IN (0,1,2) ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
	$result = $lidb->returnArray($sql,4);
	
	$table_content = "";
	$table_content .= "<form name=form1 action='edit_update.php' method=POST onSubmit='return checkQuota()'>";
	$table_content .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
	$table_content .= "<tr class=tableTitle><td>$i_UserName</td><td>$i_SmartCard_Payment_PhotoCopier_TotalQuota</td><td>$i_SmartCard_Payment_PhotoCopier_UsedQuota</td></tr>";
	
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($uid, $name, $total_quota, $used_quota) = $result[$i];
			$table_content .= "<input type=hidden name=user_id[] value=$uid>";
			$table_content .= "<tr><td>$name</td>";
			$table_content .= "<td><input type=text name=total_quota_$uid value=". ($total_quota == "" ? 0 : $total_quota) ."></td>";
			$table_content .= "<td>". ($used_quota == "" ? 0 : $used_quota) ."</td></tr>";
		}
	}
	
	$table_content .= "</table>";
	$table_content .= "<table width=560 border=0>";
	$table_content .= "<tr><td align=right><input type=image src=\"$image_path/admin/button/s_btn_submit_$intranet_session_language.gif\"></td></tr>";
	$table_content .= "</table></form>";
}

?>

<SCRIPT Language='JavaScript'>
function checkQuota()
{
	var cnt = 0;
	<?
	for($i=0; $i<sizeof($result); $i++)
	{
		list($uid, $name, $total_quota, $used_quota) = $result[$i];
	?>
		var element = eval("document.form1.total_quota_"+<?=$uid?>);
		if (element.value < <?=$used_quota?>)
		{
			cnt = 1;
		}
	<?
	}
	?>
	if(cnt==1)
	{
		alert ("<?=$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid?>");
		return false;
	}
	else
	{
		return true;
	}
	
}
</SCRIPT>

<?
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../',$i_Payment_Menu_PhotoCopier_Quota_Management,'index.php',$button_edit,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<?=$table_content?>
<?
include_once("../../../../../templates/filefooter.php");
intranet_closedb();
?>