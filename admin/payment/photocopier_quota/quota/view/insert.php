<?
include_once("../../../../../includes/global.php");
include_once("../../../../../includes/libdb.php");
include_once("../../../../../includes/libclass.php");
include_once("../../../../../includes/libfilesystem.php");
include_once("../../../../../includes/libdbtable.php");
include_once("../../../../../includes/libaccount.php");
include_once("../../../../../includes/libpayment.php");
include_once("../../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../../templates/adminheader_setting.php");
intranet_opendb();

$lidb = new libdb();
$lclass = new libclass();
$lpayment = new libpayment();

$itemName = $lpayment->returnPaymentItemName($ItemID);

$sql = "
			SELECT 
					a.UserID
			FROM 
					INTRANET_USER AS a INNER JOIN
					PAYMENT_PRINTING_QUOTA AS b ON (a.UserID = b.UserID)
			WHERE 
					a.RecordType IN (1,2)
		";
$result = $lidb->returnVector($sql);

if(sizeof($result)>0)
	$student_list = implode(",", $result);	# Student Who Are In The Payment List Already
	
### Student Selection ###
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType IN (1,2) AND UserID IN ($list)";
    $array_students = $lclass->returnArray($sql,2);
}
?>


<SCRIPT LANGUAGE=Javascript>
function removeStudent(){
		 checkOptionRemove(document.form1.elements["student[]"]);
 		 submitForm();
}
function submitForm(){
         obj = document.form1;
         obj.flag.value =0;
         generalFormSubmitCheck(obj);
}
function finishSelection()
{
         obj = document.form1;
         obj.action = 'insert_update.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
}
function formSubmit(obj)
{
         if (obj.flag.value == 0)
         {
             obj.flag.value = 1;
             generalFormSubmitCheck(obj);
             return true;
         }
         else
         {
             return finishSelection();
         }
}
function checkForm()
{
        obj = document.form1;
        var cnt = 0;

        if(obj.elements["student[]"].length != 0)
        {
        		cnt++;
    	}
        else
        {
        		alert("<?=$i_SmartCard_Payment_Student_Select_Instruction?>");
        		return false;
    	}
        
        if(obj.total_quota.value != '')
        {
        		cnt++;
    	}
        else
        {
	    		alert("<?=$i_SmartCard_Payment_Input_Quota_Instruction?>");
	    		return false;
		}
		    
       	if(cnt==2)
                return formSubmit(obj);
        else
        {
                alert('<?=$i_SmartCard_Payment_Student_Select_Instruction?>');
                return false;
        }
}

</SCRIPT>


<?
echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../',$i_Payment_Menu_PhotoCopier_Quota_Management,'index.php',$button_new,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>


<br>
<form name="form1" action="" method="POST" onsubmit="return checkForm();">
<table width=450 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr><td width=30%><?=$i_SmartCard_Payment_PhotoCopier_SelectUser?></td>
	<td><table width=100% border=0 cellspacing=0 cellpadding=0>
		<tr>
			<td><SELECT name="student[]" size=5 multiple>
				<?
				for ($i=0; $i<sizeof($array_students); $i++)
				{
				     list ($id, $name) = $array_students[$i];
				?>
				<OPTION value='<?=$id?>'><?=$name?></OPTION>
				<?
				}
				?>
				</SELECT></td>
			<td>
			<a href='javascript:newWindow("choose/index.php?fieldname=student[]&student_list=<?=$student_list?>", 9)'><img src="<?=$image_path?>/admin/button/s_btn_edit_<?=$intranet_session_language?>.gif" border=0></a>
			<br>
			<a href='javascript:removeStudent();'><img src="<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border=0></a>
			</td>
		</tr>
	</table>
</td></tr>
<tr><td><?=$i_SmartCard_Payment_PhotoCopier_TotalQuota?></td><td><input type="text" name="total_quota"></td></tr>
<tr><td colspan=2><input type=image onClick="this.form.flag.value=1" src="<?=$image_path?>/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr></table>

<input type=hidden name=flag value=0>
<input type=hidden name="ItemID" value=<?=$ItemID?>>
<input type=hidden name="student_list" value=<?=$student_list?>>
</form>


<?																																																																																																	
include_once("../../../../../templates/adminfooter.php");
?>