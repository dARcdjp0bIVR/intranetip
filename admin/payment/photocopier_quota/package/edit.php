<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();

$table_content = "";

if(sizeof($record_id) > 0)
{
	$table_content = "";
	
	$edit_list = implode(",",$record_id);
	
	//echo $edit_list;
	
	$sql = "SELECT
					RecordID, NumOfQuota, Price, PurchasingItemName, RecordType 
			FROM
					PAYMENT_PRINTING_PACKAGE
			WHERE
					RecordID IN ($edit_list)
			ORDER BY
					NumOfQuota, Price
			";
			
	$temp = $li->returnArray($sql,5);
	
	for($i=0; $i<sizeof($temp); $i++)
	{
		list($r_id, $num_of_quota, $price, $name, $record_type) = $temp[$i];
		$j++;
		$table_content .= "<tr><td>$j</td><td>$name</td>";
		$table_content .= "<td><input type=text name=num_of_quota_$r_id value=$num_of_quota></td>";
		$table_content .= "<td><input type=text name=price_$r_id value=$price></td></tr>";
		$table_content .= "<input type=hidden name=edit_list[] value=$r_id>";
	}
}
?>

<SCRIPT Language="JavaScript">
function checkForm()
{
	var cnt_1 = 0;
	var cnt_2 = 0;
	var obj = document.form1;
	<?
	for($i=0; $i<sizeof($record_id); $i++)
	{
	?>
		temp_num_of_quota = eval("obj.num_of_quota_"+<?=$record_id[$i]?>+".value");
		temp_price = eval("obj.price_"+<?=$record_id[$i]?>+".value");
		if(temp_num_of_quota != "")
		{
			if(temp_price != "")
			{
				if(!isNaN(temp_num_of_quota))
				{
					if(temp_num_of_quota >= 0)
					{
						cnt_1=cnt_1+1;
					}
					else
					{
						alert("Invalid quota");
					}
				}
				else
				{
					alert("Invalid value");
				}
				if(!isNaN(temp_price))
				{
					if(temp_price >= 0)
					{
						cnt_2=cnt_2+1;
					}
					else
					{
						alert("Invalid Price");
					}
				}
				else
				{
					alert("Invalid Price");
				}
			}
			else
			{
				alert ("Price Empty");
			}
		}
		else
		{
			alert ("Quota Empty");
		}
	<?
	}
	?>
	if((cnt_1 == <?=sizeof($record_id);?>)&&(cnt_2 == <?=sizeof($record_id);?>))
	{
		return true;
	}
	else
	{
		return false;
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../',$i_Payment_Menu_PhotoCopier_Package_Setting,'index.php',$button_edit,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="edit_update.php" method=POST onSubmit="return checkForm()">
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="16" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width="560" height="7" border="0"></td></tr>
</table>
<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr class=tableTitle><td>#</td>
<td><?=$i_Payment_Menu_PhotoCopier_TitleName?></td>
<td><?=$i_Payment_Field_Quota?></td>
<td><?=$i_Payment_Field_Amount?></td></tr>
<?=$table_content?>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table> 
<table width=560 border=0 align=center>
<tr
<tr><td align=right><?= btnSubmit() ." ". btnReset() ?><a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>