<?
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

?>

<SCRIPT LANGUAGE=Javascript>
function checkForm()
{
	var obj = document.form1;
	if(obj.item_name.value != '')
	{
		if(obj.num_of_quota.value != '')
		{
			if(!isNaN(obj.num_of_quota.value))
			{
				if(obj.num_of_quota.value > 0)
				{
					if(obj.price.value != '')
					{
						if(!isNaN(obj.num_of_quota.value))
						{
							if(obj.price.value > 0)
							{
								return true;
							}
							else
							{
								alert("<?=$i_Payment_PhotocopierQuotaPriceNotValid_Warning?>");
								return false;
							}
						}
						else
						{
							alert ("<?=$i_Payment_PhotocopierQuotaPriceNotValid_Warning?>");
							return false;
						}
					}
					else
					{
						alert("<?=$i_SmartCard_Payment_Input_Amount_Instruction?>");
						return false;
					}
				}
				else
				{
					alert ("<?=$i_Payment_PhotocopierQuotaNotValid_Warning?>");
					return false;
				}
			}
			else
			{
				alert ("<?=$i_Payment_PhotocopierQuotaNotValid_Warning?>");
				return false;
			}
		}
		else
		{
			alert ("<?=$i_SmartCard_Payment_Input_Quota_Instruction?>");
			return false;
		}
	}
	else
	{
		alert ("<?=$i_Payment_PhotocopierQuotaPackageName_Warning?>");
		return false;
	}
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../',$i_Payment_Menu_PhotoCopierQuotaSetting,'../',$i_Payment_Menu_PhotoCopier_Package_Setting,'../package/', $button_new,'') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<center>
<form name=form1 action="insert_update.php" method=POST OnSubmit="return checkForm()">
<table border=0 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr><td width=90><?=$i_Payment_Menu_PhotoCopier_TitleName?></td><td><input type=text name=item_name></td></tr>
<td><?=$i_Payment_Field_Quota?></td><td><input type=text name=num_of_quota></td></tr>
<td><?=$i_Payment_Field_Amount?></td><td><input type=text name=price></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align=right><?= btnSubmit() ." ". btnReset() ?><a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a></td></tr>
</table>
</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>