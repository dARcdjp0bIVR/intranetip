<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");


?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '') ?>
<?= displayTag("head_payment_$intranet_session_language.gif", $msg) ?>

<script>
function PromptSMS()
{
	alert("<?=$i_SMS['jsWarning']['NoPluginWarning']?>");
}
</script>

<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<tr><td class="tableContent" height="300" align="left">
<blockquote>
<?
# Check SMS v2

        //check sms_notification
        if ($bl_sms_version>=2)
        {
            include_once("../../includes/libdb.php");
            include_once("../../includes/libsmsv2.php");
            intranet_opendb();

            //checkout the status of the payment related template
            $status = 0;
            $lsms                = new libsmsv2();
            if($lsms->returnTemplateStatus("","STUDENT_MAKE_PAYMENT") and $lsms->returnSystemMsg("STUDENT_MAKE_PAYMENT"))        $status = 1;
            if($lsms->returnTemplateStatus("","BALANCE_REMINDER") and $lsms->returnSystemMsg("BALANCE_REMINDER"))        $status = 1;
        }
        else
        {
            $status = 0;
        }

?>
<? if($intranet_version=="2.0") {?>
<?php
	
	# Show SMS warning if the client did not purchase the sms plugin
	if (!$plugin['sms'])
	{
		echo "<font color='red'>".$i_SMS['NoPluginWarning']."</font><br /><br />";
	}

        echo displayOption(
                $i_Payment_System_Admin_User_Setting ,'admin_setting.php',$plugin['payment_eAdmin'],
                $i_Payment_Menu_Settings ,'settings/',1,
                $i_Payment_Menu_DataImport,'import/',1,
                $i_Payment_Menu_Settings_PaymentItem,'settings/payment_item/',1,
                $i_Payment_Subsidy_Setting,'settings/subsidy_unit/',1,
                $i_Payment_Menu_DataBrowsing,'browse/',1,
                $i_Payment_Menu_PhotoCopierQuotaSetting,'photocopier_quota/',$plugin['payment_printing_quota'],
                $i_Payment_Menu_PrintPage, 'printpages/',1,
                $i_Payment_Menu_StatisticsReport, 'report/',1,
                $i_StudentAttendance_Menu_DataImport,'data_import/',!$plugin['attendancestudent']
        );
        
        # Show SMS selection or not
        if (!$plugin['sms'])
		{
			echo displayOption(
				$i_SMS_Notification, 'javascript:PromptSMS()', 1
	        );
		}
		else
		{
			echo displayOption(
				$i_SMS_Notification,'sms_notification/', ($plugin['sms'] && $plugin['payment'] && $bl_sms_version>=2 && $status)
	        );
		}
	
?>
<? } else {?>
<?php

	if (!$plugin['sms'])
	{
		echo "<font color='red'>".$i_SMS['NoPluginWarning']."</font><br /><br />";
	}
	
        echo displayOption(
                $i_Payment_Menu_Settings ,'settings/',1,
                $i_Payment_Menu_DataImport,'import/',1,
                $i_Payment_Menu_Settings_PaymentItem,'settings/payment_item/',1,
                $i_Payment_Subsidy_Setting,'settings/subsidy_unit/',1,
                $i_Payment_Menu_DataBrowsing,'browse/',1,
                $i_Payment_Menu_PhotoCopierQuotaSetting,'photocopier_quota/',$plugin['payment_printing_quota'],
                $i_Payment_Menu_PrintPage, 'printpages/',1,
                $i_Payment_Menu_StatisticsReport, 'report/',1,
                $i_StudentAttendance_Menu_DataImport,'data_import/',!$plugin['attendancestudent']
        );
        
    # Show SMS selection or not
    if (!$plugin['sms'])
	{
		echo displayOption(
			$i_SMS_Notification, 'javascript:PromptSMS()', 1
        );
	}
	else
	{
		echo displayOption(
			$i_SMS_Notification,'sms_notification/', ($plugin['sms'] && $bl_sms_version>=2 && $status)
        );
	}
    
?>
<? } ?>

</blockquote>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>
