<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../includes/libexporttext.php");

if($ClassName=="" || $FromDate=="" || $ToDate=="" || $format=="")
	header("Location: index.php");

if($format!=1)
	include_once("../../../../templates/fileheader.php");
	
	
define('MAX_COLUMN',256);  // max. column 256 in excel, minus 2 columns ( username , balance)
if($intranet_session_language=="en")
	define('MAX_STUDENT_PER_PAGE',20);	
else 
	define('MAX_STUDENT_PER_PAGE',45);	
	
intranet_opendb();

$li = new libpayment();

$lexport = new libexporttext();


## get target students data
$order_student = array();

if($display_mode!=1){
	$namefield = getNameFieldWithClassNumberByLang("a.");
	
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$archive_namefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.ChineseName,' (',a.ClassName,'-',a.ClassNumber,')'),a.ChineseName)";
	}else $archive_namefield = "IF(a.ClassName IS NOT NULL AND a.ClassName<>'' AND a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.EnglishName,' (',a.ClassName,'-',a.ClassNumber,')'),a.EnglishName)";
}else{
	$break = $format==1?"":"<BR>";
	$namefield = "CONCAT(".getNameFieldByLang("a.").", IF(a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT('$break',a.ClassNumber),''))";
	
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$archive_namefield = "IF(a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.ChineseName,'$break',a.ClassNumber),a.ChineseName)";
	}else $archive_namefield = "IF(a.ClassNumber IS NOT NULL AND a.ClassNumber<>'',CONCAT(a.EnglishName,'$break',a.ClassNumber),a.EnglishName)";
	
	/*
	$namefield = getNameFieldByLang("a.");
	
	if($intranet_session_language=="b5"||$intranet_session_language=="gb"){
		$archive_namefield = "a.ChineseName";
	}else $archive_namefield = "a.EnglishName";
	*/
}

//$sql="SELECT a.UserID, $namefield, b.Balance FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName='$ClassName' AND a.RecordType=2 AND a.RecordStatus IN (0,1,2) ORDER BY a.ClassNumber";
/*
$sql="
	SELECT 
		IF(a.UserID IS NOT NULL,a.UserID, b.UserID),
		IF(a.UserID IS NOT NULL,$namefield,$namefield2),
		IF(c.Balance
	FROM
		PAYMENT_ACCOUNT AS c LEFT OUTER JOIN
		INTRANET_USER AS a  ON (c.StudentID = a.UserID AND a.RecordType=2 ) LEFT OUTER JOIN
		INTRANET_ARCHIVE_USER AS b ON (c.StudentID = b.UserID AND b.RecordType=2  AND b.Class)
	WHERE 
		a.ClassName='$ClassName' OR b.ClassName='$ClassName'
	ORDER BY
		a.ClassNumber,b.ClassNumber
	
";
*/

/* developing by Ronald on 20080930
if((in_array(1, $DataRange)) && (in_array(3, $DataRange))){
	$curr_user_status = "1,2,3,4";
} else if(in_array(1, $DataRange)){
	$curr_user_status = "1,2,3";
} else if(in_array(3, $DataRange)){
	$curr_user_status = "4";
} else {
	$curr_user_status = "";
}
echo $user_status;

if((in_array(2, $DataRange)) 
{
	
}
*/

if ($StudentTypesCURRENT)
{
	// get student list from INTRANET_USER
	$sql="SELECT a.UserID, $namefield, b.Balance FROM INTRANET_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName='$ClassName' AND a.RecordType=2 ORDER BY a.ClassNumber";
	$temp = $li->returnArray($sql,3);
}

if ($StudentTypesREMOVED)
{
	// get student list from INTRANET_ARCHIVE_USER
	$sql="SELECT a.UserID, $archive_namefield, b.Balance FROM INTRANET_ARCHIVE_USER AS a LEFT OUTER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID) WHERE a.ClassName='$ClassName' AND a.RecordType=2 ORDER BY a.ClassNumber";
	$temp2 = $li->returnArray($sql,3);
}

for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$student_name,$balance) = $temp[$i];
	$student_data[$student_id]['Name'] = $student_name;
	$student_data[$student_id]['Balance'] = $balance;
	$student_data[$student_id]['archived']=0;
	if(!in_array($student_id,$order_student))
		$order_student[] = $student_id;
}
for($i=0;$i<sizeof($temp2);$i++){
	list($student_id,$student_name,$balance) = $temp2[$i];
	$student_data[$student_id]['Name'] = $student_name;
	$student_data[$student_id]['archived']=1;
	$student_data[$student_id]['Balance'] = $balance;
	
	if(!in_array($student_id,$order_student))
		$order_student[] = $student_id;
}


$list = implode(",",$order_student);

$sql="
	SELECT
		c.StudentID,
		d.Name,
		d.DefaultAmount,
		c.ItemID,
		IF(c.RecordStatus=1,'$i_Payment_PresetPaymentItem_PaidCount','$i_Payment_PresetPaymentItem_UnpaidCount')
	FROM 
		PAYMENT_PAYMENT_ITEMSTUDENT AS c LEFT OUTER JOIN
		PAYMENT_PAYMENT_ITEM AS d ON (c.ItemID = d.ItemID )
	WHERE 
		c.StudentID IN ($list) AND 
		d.StartDate<='$ToDate' AND 
		d.EndDate>='$FromDate'
	ORDER BY
		d.StartDate
";

$temp = $li->returnArray($sql,5);

$order_item = array();
for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$item_name,$default_amount,$item_id,$paid_status) = $temp[$i];
	$payment_item[$item_id]['Name'] = $item_name;
	$payment_item[$item_id]['DefaultAmount'] = $default_amount;
	$student_data[$student_id][$item_id]['PaidStatus'] = $paid_status;
	
	if(!in_array($item_id,$order_item))
		$order_item[] = $item_id;
}

if($format==1 && sizeof($order_item)>MAX_COLUMN){ // too many columns
	include_once("../../../../templates/fileheader.php");
	echo "<table border=0 align=center>";
	echo "<tr><td align=center class='$css_text'>$i_Payment_Class_Payment_Report_Warning<Br><br><a href='javascript:window.close()'><img src='/images/admin/button/s_btn_close_$intranet_session_language.gif' border='0' align='absmiddle'></td></tr>";
	echo "</table>";
	include_once("../../../../templates/filefooter.php");
	exit();
}

############## Built Display Table / CSV content #############
//$table="<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$header="<table width=100% border=0 cellpadding=0 cellspacing=0 align=center>
	<tr><td class='$css_title'>$i_Payment_Class_Payment_Report</td></tr>
	<Tr><Td class='$css_text'><B>$i_ClassName</b>: $ClassName</td></tr>
	<tr><Td class='$css_text'><B>$i_Payment_Receipt_Date</b>: $FromDate <span class='$css_text'><b>$i_Profile_To</b></span> $ToDate</td></tr>
	<tr><Td>&nbsp;</td></tr>
	<tr><td class='$css_text'><font style='font-family:Symbol;'>&Ouml;</font> : ".$i_Payment_PresetPaymentItem_PaidCount." &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;X : $i_Payment_PresetPaymentItem_UnpaidCount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-- : $i_Payment_Class_Payment_Report_NoNeedToPay</td>
	<td class=\"$css_text\" align=\"right\">$i_general_report_creation_time : ".date('Y-m-d H:i:s')."</td></tr>
	<tr><Td>&nbsp;</td></tr>
	</table>
	<table width=100% border=0 cellpadding=0 cellspacing=0 align=\"center\">
	<tr><td class='$css_text'>$i_Payment_Note_StudentRemoved2</td></tr>
	</table>
";



if($display_mode!=1){ # Y: Student Name / X: Item Name
	$table="<table width=100% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
	# table header
	$table.="<tr class='$css_table_title'><td width=20% class='$css_table_title'>&nbsp</td><td width=10% style='vertical-align:bottom;horizontal-align:center'  class='$css_table_title'>$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
	$csv ="\"\",\"$i_Payment_PrintPage_Outstanding_AccountBalance\",";
	for($i=0;$i<sizeof($order_item);$i++){
		$item_id = $order_item[$i];
		$item_name = $payment_item[$item_id]['Name'];
		$default_amount = $payment_item[$item_id]['DefaultAmount'];
		$str_default_amount = $li->getWebDisplayAmountFormat($default_amount);
		$table.="<td style='vertical-align:bottom;horizontal-align:center' class='$css_table_title'>$item_name<Br>$str_default_amount</td>";
		$csv.="\"$item_name ".$li->getExportAmountFormat($default_amount)."\"";
		if($i<sizeof($order_item)-1)
			$csv .=",";
	}
	$table.="</tr>";
	$csv.="\n";
	
	# table content
	for($i=0;$i<sizeof($order_student);$i++){
		$student_id   = $order_student[$i];
		$student_name = $student_data[$student_id]['Name'];
		if($format!=1 && $student_data[$student_id]['archived']==1){
			$student_name = "<font color=black>*</font> <i>$student_name</i>";
		}
		$balance      = $student_data[$student_id]['Balance'];
		$str_balance  = $li->getWebDisplayAmountFormat($balance);
		
		//$css = $i%2==0?"tableContent":"tableContent2";
		$css = $i%2==0?$css_table_content:$css_table_content2;

		
		$table.="<tr class='$css'><td class='$css'>$student_name</td><td class='$css'>$str_balance</td>";
		$csv .="\"$student_name\",\"".$li->getExportAmountFormat($balance)."\",";
		for($j=0;$j<sizeof($order_item);$j++){
			$item_id = $order_item[$j];
			$paid_status  = $student_data[$student_id][$item_id]['PaidStatus'];
			
			if($paid_status == ""){
				 $paid_status = $i_Payment_PresetPaymentItem_NoNeedToPay; 
				 $str_paid_status = $paid_status;
			}
			/*
			if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
				$str_paid_status = "<font color=blue>$paid_status</font>";
			else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
				$str_paid_status = "<font color=red>$paid_status</font>";
			*/
				if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
					$str_paid_status ="<font style='font-family:Symbol;'>&Ouml;</font>";
				else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
					$str_paid_status ="X";				
			$table.="<td align=center class='$css'>$str_paid_status</td>";	
			$csv .="\"$paid_status\"";
			if($j<sizeof($order_item)-1)
				$csv.=",";
		
		}
		$table.="</tr>";
		$csv.="\n";
	}
	$table.="</table>";
	$display = $header.$table;
}else{
	
	   # Y: Item Name / X: Student Name
		$header.="<table width=100% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
		$page_breaker="<P class='breakhere'></P>";
		
		$page = $page==""?0:$page;
		
		$pos_start = 0;
		$pos_end = sizeof($order_student);
	
		if($format!=1){ ## cut students
			if(sizeof($order_student)>MAX_STUDENT_PER_PAGE){
				$temp_size = sizeof($order_student);
				$pages = ceil($temp_size / MAX_STUDENT_PER_PAGE);
				$next_page = $page+1 < $pages ?$page+1:-1;
				$prev_page = $page>0?$page-1:-1;
				$pos_start = MAX_STUDENT_PER_PAGE * $page;
				$pos_end  = MAX_STUDENT_PER_PAGE * ($page+1);
				$pos_end = $pos_end > $temp_size ? $temp_size : $pos_end;
			}else{
				$next_page = -1;
				$prev_page = -1;
			}
		}
//	echo "page=$page,prevpage=$prev_page,next_page=$next_page,pos start=$pos_start,end=$pos_end<Br>";	
		$header.="<tr class='$css_table_content3'><td  class='$css_table_content3'>&nbsp;</td>";
		$csv="\"\",";
		$csv_bal ="\"$i_Payment_PrintPage_Outstanding_AccountBalance\",";
		$table_bal="<tr class='$css_table_content'><td class='$css_table_content'>$i_Payment_PrintPage_Outstanding_AccountBalance</td>";
		for($i=$pos_start;$i<$pos_end;$i++){
			$student_id   = $order_student[$i];
			$student_name = $student_data[$student_id]['Name'];
			if($format!=1 && $student_data[$student_id]['archived']==1){
				$student_name = "<font color=black>*</font><br><i>$student_name</i>";
			}			
			$balance      = $student_data[$student_id]['Balance'];
			
			$header .= "<td  class='$css_table_content3' style='vertical-align:bottom;horizontal-align:center'>$student_name</td>";
			$table_bal.="<td class='$css_table_content'>".$li->getWebDisplayAmountFormat($balance)."</td>";
			$csv.="\"$student_name\"";
			$csv_bal.="\"".$li->getExportAmountFormat($balance)."\"";
			if($i<$pos_end-1){
				$csv .=",";
				$csv_bal.=",";
			}
		}
		
		$header.="</tr>";
		$table_bal.="</tr>";
		//$header.=$table_bal;
		
		$csv .="\n";
		$csv_bal.="\n";
		//$csv.=$csv_bal;
		
		$table="";
		$display="";
		
		///$table.="<tr>";
		for($i=0;$i<sizeof($order_item);$i++){
			//$css = $i%2==0?"tableContent2":"tableContent";
			//$css = $i%2==0?$css_table_content:$css_table_content2;
			$css=$css_table_content3;
			
			if($ItemPerPage>0 && $i>0 && $i%$ItemPerPage==0){ ## page break;
				$table.="</table>";
				$display.=$header.$table.$page_breaker;
				$table="";
			}
			
			
			$item_id = $order_item[$i];
			$item_name = $payment_item[$item_id]['Name'];
			$default_amount = $payment_item[$item_id]['DefaultAmount'];			

			$table.="<tr class='$css'><td class='$css'>$item_name (".$li->getWebDisplayAmountFormat($default_amount).")</td>";
			$csv.="\"$item_name ".$li->getExportAmountFormat($default_amount)."\",";
			for($j=$pos_start;$j<$pos_end;$j++){
				$student_id =  $order_student[$j];
				$paid_status  = $student_data[$student_id][$item_id]['PaidStatus'];
				if($paid_status == ""){
					 $paid_status = $i_Payment_PresetPaymentItem_NoNeedToPay; 
					 $str_paid_status = $paid_status;
				}
				
				if($paid_status == $i_Payment_PresetPaymentItem_PaidCount)
					//$str_paid_status = "<font color=blue>$paid_status</font>";
					$str_paid_status ="<font style='font-family:Symbol;'>&Ouml;</font>";
				else if($paid_status == $i_Payment_PresetPaymentItem_UnpaidCount)
					//$str_paid_status = "<font color=red>$paid_status</font>";
					$str_paid_status ="X";

				
				$table.="<td class='$css' style='vertical-align:middle;horizontal-align:center'>$str_paid_status</td>";					
				$csv .="\"$paid_status\"";
				if($j<$pos_end -1)
					$csv.=",";
			}
			$table.="</tr>";
			$csv.="\n";	
			
		}
		$table.="</table>";
		$display.=$header.$table;
		
		$nav_button = "<table width=100% border=0>";
		$nav_button.="<tr><td><table border=0 class=print_hide align=right><tr><td style='vertical-align:top;horizontal-align:right'>";
		if($prev_page!=-1)
			$nav_button.="<a href='class_report.php?FromDate=$FromDate&ToDate=$ToDate&ClassName=$ClassName&page=$prev_page&format=$format&display_mode=$display_mode&ItemPerPage=$ItemPerPage'><img src='$image_path/admin/button/prev.gif' border=0></a> ";
		if($next_page!=-1)
			$nav_button.=" <a href='class_report.php?FromDate=$FromDate&ToDate=$ToDate&ClassName=$ClassName&page=$next_page&format=$format&display_mode=$display_mode&ItemPerPage=$ItemPerPage'><img src='$image_path/admin/button/next.gif' border=0></a>";
		$nav_button.="</td></tr></table></td></tr>";
		$nav_button.="<tr><Td style='vertical-align:top;horizontal-align:right'>$display</td></tr>";
		$nav_button.="</table>";
		
		$display=$nav_button;
}


intranet_closedb();

if($format!=1){
	
	?>
	<style type='text/css' media='print'>
 .print_hide {display:none;}
  P.breakhere {page-break-before: always}
</style>
<?php
	echo $display;
	include_once("../../../../templates/filefooter.php");
}else{
	$filename = "class_payment_report_".$ClassName.".csv";
	if ($g_encoding_unicode) {
		$csv = str_replace("\"","",$csv);
		$csv = str_replace(",","\t",$csv);
		$csv = str_replace("\n","\r\n",$csv);
		$lexport->EXPORT_FILE($filename, $csv);		
	}
	else output2browser($csv,$filename);
}
?>