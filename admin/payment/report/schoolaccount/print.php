<?php
// kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();

        # date range
        $today_ts = strtotime(date('Y-m-d'));
        if($FromDate=="")
                $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
        if($ToDate=="")
                $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate'";
        $sql="SELECT a.Amount,
                        a.Details,
                        a.TransactionType,
                        c.RecordType
                        FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a
                             LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
                              WHERE $date_cond ORDER BY a.Details, c.RecordType";

$li = new libpayment();
$temp = $li->returnArray($sql,4);


$ary_types = array(1,2,3,6,7,8,9,10);

$x="<table width=90% border=0  cellpadding=1 cellspacing=0 align='center' class='$css_table'>";
$x.="<Tr class='$css_table_title'>";
$x.="<Td rowspan=2 class='$css_table_title' width=40% align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_Detailed_Transaction</td>";
$x.="<td rowspan=2 class='$css_table_title' widht=15% align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_Deposit</td>";
$x.="<td colspan=3 class='$css_table_title' align=center width=45% style='vertical-align:middle'>$i_Payment_SchoolAccount_Expenses</td>";
$x.="</tr>";
$x.="<Tr><td class='$css_table_title' align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_PresetItem</td><Td class='$css_table_title' align=center style='vertical-align:middle'>$i_Payment_SchoolAccount_SinglePurchase</td><td class='$css_table_title' align=center style='vertical-align:middle' width=12%>$i_Payment_SchoolAccount_Other</td></tr>";


$income=0;
$expense_item=0;
$expense_single=0;
$expense_other=0;


for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type,$credit_transaction_type) = $temp[$i];
        if(in_array($type,$ary_types)){
                #$result[$type]["$detail"] += $amount+0;
                switch($type){
                        case 1        :
                                       $result[$type][$credit_transaction_type] += $amount;
                                       $income+=$amount+0;
                                       break;

                        case 2        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $expense_item +=$amount+0;
                                       break;

                        case 3        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $expense_single+=$amount+0;
                                       break;

                        case 6        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $income+=$amount+0;
                                       break;

                        case 7        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;

                        case 8        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                                       
                        case 9        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                         
                        case 10        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;

                        default        : break;
                }

        }

}

if(sizeof($result)<=0){
        $x.="<tr><td colspan=5 align=center class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg<td></tr>";
}else{
        $count=0;
		
        # deposit
        $list_item = $result[1];
        if(sizeof($list_item)>0){
                foreach($list_item as $credit_type => $t_total){
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        $css=$count%2==0?$css_table_content:$css_table_content."2";
                        $count++;
                        switch ($credit_type)
                        {
                                case 1: $name = $i_Payment_Credit_TypePPS; break;
                                case 2: $name = $i_Payment_Credit_TypeCashDeposit; break;
                                case 3: $name = $i_Payment_Credit_TypeAddvalueMachine; break;
                                default : $name = $i_Payment_Credit_TypeUnknown . "(Type=$credit_type)";

                        }

                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
                //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";
        }

        # payment cancellation
        $list_item = $result[6];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        $css=$count%2==0?$css_table_content:$css_table_content."2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
        }
        //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";

        # payment item
        $list_item = $result[2];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        $css=$count%2==0?$css_table_content:$css_table_content."2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td></tr>";
                }
        }

        # single purchase
        $list_item = $result[3];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        $css=$count%2==0?$css_table_content:$css_table_content."2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td><td class='$css'>&nbsp;</td></tr>";
                }
        }
        //$x.="<tr><td colspan=4 class='tableContent'>&nbsp;</td></tr>";

        # refund
        $list_item = $result[7];
        if(sizeof($list_item)>0){
                  $item_name = $i_Payment_action_refund;
                  //$css =$count%2==0?"tableContent":"tableContent2";
                  $css=$count%2==0?$css_table_content:$css_table_content."2";
                foreach($list_item as $name => $t_total){
                        $count++;
                        $x.="<tr><td class='$css'>$item_name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
                }
        }

        # pps charges
        $list_item = $result[8];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        //$css =$count%2==0?"tableContent":"tableContent2";
                        $css=$count%2==0?$css_table_content:$css_table_content."2";
                        $count++;
                        $x.="<tr><td class='$css'>$i_Payment_PPS_Charge&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
                }
        }
				
				# Cancel Deposit Charge
        $list_item = $result[9];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['CancelDepositDescription'];
          $s_total =0;
          $css=$count%2==0?$css_table_content:$css_table_content."2";
          foreach($list_item as $name => $t_total){
                  $s_total+=$t_total;
          }
          
          $x.="<tr><td class='$css'>$item_name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
          /*foreach($list_item as $name => $t_total){
                  $css =$count%2==0?"tableContent":"tableContent2";
                  $count++;
                  $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
          }*/
        }
        
        # Donation to school
        $list_item = $result[10];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['DonateBalanceDescription'];
          $s_total =0;
          $css=$count%2==0?$css_table_content:$css_table_content."2";
          foreach($list_item as $name => $t_total){
                  $s_total+=$t_total;
          }
          
          $x.="<tr><td class='$css'>$item_name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($s_total)."</td></tr>";
          /*      foreach($list_item as $name => $t_total){
                        $css =$count%2==0?"tableContent":"tableContent2";
                        $count++;
                        $x.="<tr><td class='$css'>$name&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($t_total)."</td></tr>";
                }*/
        }
				
        # Total
        $total_income = $income+0;

        //$css =$count%2==0?"tableContent":"tableContent2";
        //$count++;
        //$css = "tableTitle2";
        $css = $css_table_title;
        $x.="<tr><td class='$css_table_content' colspan='5'><img src='$image_path/space.gif' border=0 height=1></td></tr>";
        $x.="<tr><td align=right class='$css'>$i_Payment_SchoolAccount_Total</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($total_income)."</td><Td class='$css_table_content'>".$li->getWebDisplayAmountFormat($expense_item)."</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($expense_single)."</td><Td class='$css_table_content'>".$li->getWebDisplayAmountFormat($expense_other)."</td></tr>";

        # Summary
        $total_expense = $expense_item + $expense_single + $expense_other+0;
        $net_income = $total_income - $total_expense;
        $net_income_str = ($net_income>0)? $li->getWebDisplayAmountFormat($net_income):"<font color=red>(".$li->getWebDisplayAmountFormat($net_income).")</font>";
        $y="<table width=90% border=0 cellpadding=0 cellspacing=0 align=center><tr><TD>";
        $y.="<table border=0>";
        $y.="<tr><td align=right class='$css_text'><B>$i_Payment_SchoolAccount_TotalIncome:</b></td><td class='$css_text'>".$li->getWebDisplayAmountFormat($total_income)."</td></tr>";
        $y.="<tr><td align=right class='$css_text'><B>$i_Payment_SchoolAccount_TotalExpense:</b></td><td class='$css_text'>".$li->getWebDisplayAmountFormat($total_expense)."</td></tr>";
        $y.="<tr><td align=right class='$css_text'><B>$i_Payment_SchoolAccount_NetIncomeExpense:</b></td><td class='$css_text'>$net_income_str</td></tr>";
        $y.="</table></td></tr></table><Br>";

}

$x.="</table>";

$display=$x;

include_once("../../../../templates/fileheader.php");
//echo displayNavTitle($i_Payment_Menu_Report_SchoolAccount,'');
?>
<table border=0 width=90% align=center>
<tr><td class='<?=$css_title?>'><?=$i_Payment_Menu_Report_SchoolAccount?> (<?="$FromDate $i_Profile_To $ToDate"?>)</td></tr>
</table>
<?=$y?>
<?=$display?>
<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>
<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
