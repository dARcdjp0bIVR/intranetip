<?php
// kenneth chung
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lexport = new libexporttext();

$lpayment = new libpayment();


# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
        $FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
        $ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$date_cond =" DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.TransactionTime,'%Y-%m-%d')<='$ToDate'";
        $sql="SELECT a.Amount,
                        a.Details,
                        a.TransactionType,
                        c.RecordType
                        FROM PAYMENT_OVERALL_TRANSACTION_LOG AS a
                             LEFT OUTER JOIN PAYMENT_CREDIT_TRANSACTION as c ON a.RelatedTransactionID = c.TransactionID
                             WHERE $date_cond ORDER BY a.Details, c.RecordType";

$li = new libpayment();
$temp = $li->returnArray($sql,4);


$ary_types = array(1,2,3,6,7,8,9,10);


$x="\"$i_Payment_SchoolAccount_Detailed_Transaction\",";
$x.="\"$i_Payment_SchoolAccount_Deposit\",";
$x.="\"\",";
$x.="\"$i_Payment_SchoolAccount_Expenses\"\n";

$x.="\"\",\"\",";
$x.="\"$i_Payment_SchoolAccount_PresetItem\",\"$i_Payment_SchoolAccount_SinglePurchase\",\"$i_Payment_SchoolAccount_Other\"\n";

$utf_x=$i_Payment_SchoolAccount_Detailed_Transaction."\t";
$utf_x.=$i_Payment_SchoolAccount_Deposit."\t";
$utf_x.="\t";
$utf_x.=$i_Payment_SchoolAccount_Expenses."\t\r\n";

$utf_x.="\t\t";
$utf_x.=$i_Payment_SchoolAccount_PresetItem."\t".$i_Payment_SchoolAccount_SinglePurchase."\t".$i_Payment_SchoolAccount_Other."\r\n";



$income=0;
$expense_item=0;
$expense_single=0;
$expense_other=0;


for($i=0;$i<sizeof($temp);$i++){
        list($amount,$detail,$type, $credit_transaction_type) = $temp[$i];
        if(in_array($type,$ary_types)){
                #$result[$type]["$detail"] += $amount+0;
                switch($type){
                        case 1        :
                                       $result[$type][$credit_transaction_type] += $amount;
                                       $income+=$amount+0;
                                       break;

                        case 2        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $expense_item +=$amount+0;
                                       break;

                        case 3        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $expense_single+=$amount+0;
                                       break;

                        case 6        :
                                       $result[$type]["$detail"] += $amount+0;
                                       $income+=$amount+0;
                                       break;

                        case 7        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;

                        case 8        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                        
                        case 9        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                         
                        case 10        :
                                       $result[$type]["all"] += $amount;
                                       $expense_other +=$amount+0;
                                       break;
                        default        : break;
                }

        }

}

if(sizeof($result)<=0){
        $x.="\"$i_no_record_exists_msg\"\n";
        $utf_x.=$i_no_record_exists_msg."\r\n";
}else{

        # deposit
        $list_item = $result[1];
        if(sizeof($list_item)>0){
                foreach($list_item as $credit_type => $t_total){
                        switch ($credit_type)
                        {
                                case 1: $name = $i_Payment_Credit_TypePPS; break;
                                case 2: $name = $i_Payment_Credit_TypeCashDeposit; break;
                                case 3: $name = $i_Payment_Credit_TypeAddvalueMachine; break;
                                default : $name = $i_Payment_Credit_TypeUnknown . "(Type=$credit_type)";

                        }
                        $name = intranet_undo_htmlspecialchars($name);
                        $x.="\"$name\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
                        $utf_x.=$name."\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
                }
        }

        # payment cancellation
        $list_item = $result[6];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $name = intranet_undo_htmlspecialchars($name);
                        $x.="\"$name\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
                        $utf_x.=$name."\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
                }
        }

        # payment item
        $list_item = $result[2];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $name = intranet_undo_htmlspecialchars($name);
                        $x.="\"$name\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
                        $utf_x.=$name."\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
                }
        }

        # single purchase
        $list_item = $result[3];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $name = intranet_undo_htmlspecialchars($name);
                        $x.="\"$name\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
                        $utf_x.=$name."\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
                }
        }

        # refund
        $list_item = $result[7];
        if(sizeof($list_item)>0){
                $item_name = $i_Payment_action_refund;
                foreach($list_item as $name => $t_total){
                        $item_name = intranet_undo_htmlspecialchars($item_name);
                        $x.="\"$item_name\",\"\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
                        $utf_x.=$item_name."\t\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
                }
        }

        # pps charges
        $list_item = $result[8];
        if(sizeof($list_item)>0){
                foreach($list_item as $name => $t_total){
                        $name = intranet_undo_htmlspecialchars($i_Payment_PPS_Charge);
                        $x.="\"$name\",\"\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
                        $utf_x.=$name."\t\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
                }
        }
        
        # Cancel Deposit Charge
        $list_item = $result[9];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['CancelDepositDescription'];
          $s_total =0;
          foreach($list_item as $name => $t_total){
                  $s_total+=$t_total;
          }
          
          $item_name = intranet_undo_htmlspecialchars($item_name);
          $x.="\"$item_name\",\"\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
          $utf_x.=$item_name."\t\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
        }
        
        # Donation to school
        $list_item = $result[10];
        if(sizeof($list_item)>0){
        	$item_name = $Lang['Payment']['DonateBalanceDescription'];
          $s_total =0;
          foreach($list_item as $name => $t_total){
                  $s_total+=$t_total;
          }
          
          $item_name = intranet_undo_htmlspecialchars($item_name);
          $x.="\"$item_name\",\"\",\"\",\"\",\"".$lpayment->getExportAmountFormat($t_total)."\"\n";
          $utf_x.=$item_name."\t\t\t\t".$lpayment->getExportAmountFormat($t_total)."\r\n";
        }

        # Total
        $total_income = $income+0;

        $x.="\"$i_Payment_SchoolAccount_Total\",\"".$lpayment->getExportAmountFormat($total_income)."\",\"".$lpayment->getExportAmountFormat($expense_item)."\",\"".$lpayment->getExportAmountFormat($expense_single)."\",\"".$lpayment->getExportAmountFormat($expense_other)."\"\n";
        $utf_x.=$i_Payment_SchoolAccount_Total."\t".$lpayment->getExportAmountFormat($total_income)."\t".$lpayment->getExportAmountFormat($expense_item)."\t".$lpayment->getExportAmountFormat($expense_single)."\t".$lpayment->getExportAmountFormat($expense_other)."\r\n";

        # Summary
        $total_expense = $expense_item + $expense_single + $expense_other+0;
        $net_income = $total_income - $total_expense;
        $net_income_str = $lpayment->getExportAmountFormat($net_income);
        $y ="\"$i_Payment_SchoolAccount_TotalIncome\",\"".$lpayment->getExportAmountFormat($total_income)."\"\n";
        $y.="\"$i_Payment_SchoolAccount_TotalExpense\",\"".$lpayment->getExportAmountFormat($total_expense)."\"\n";
        $y.="\"$i_Payment_SchoolAccount_NetIncomeExpense\",\"$net_income_str\"\n\n";


        $utf_y =$i_Payment_SchoolAccount_TotalIncome."\t".$lpayment->getExportAmountFormat($total_income)."\r\n";
        $utf_y.=$i_Payment_SchoolAccount_TotalExpense."\t".$lpayment->getExportAmountFormat($total_expense)."\r\n";
        $utf_y.=$i_Payment_SchoolAccount_NetIncomeExpense."\t".$net_income_str."\r\n\r\n";

}


$content = "\"$i_Payment_Menu_Report_SchoolAccount ( $FromDate $i_Profile_To $ToDate )\"\n\n";
$content.=$y.$x;
$content.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";


$utf_content = $i_Payment_Menu_Report_SchoolAccount." (".$FromDate." ".$i_Profile_To." ".$ToDate.")\r\n\r\n";

$utf_content.=$utf_y.$utf_x;
$utf_content.=$i_general_report_creation_time."\t".date('Y-m-d H:i:s')."\r\n";


intranet_closedb();

$filename = "school_account.csv";
if ($g_encoding_unicode) {
        $lexport->EXPORT_FILE($filename, $utf_content);
} else {
        output2browser($content,$filename);
}


?>
