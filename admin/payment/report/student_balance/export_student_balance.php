<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../includes/libexporttext.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();
$lpayment = new libpayment();
$lclass = new libclass();
$lexport = new libexporttext();

if(isset($TargetStudentID)) {
	
	$user_list = implode(",",$TargetStudentID);
	
} else if(isset($TargetClassID)) {
	
	$class_list = implode(",",$TargetClassID);
	
	$sql = "SELECT a.UserID FROM INTRANET_USER AS a INNER JOIN INTRANET_CLASS AS b ON (a.ClassName = b.ClassName) WHERE b.ClassID IN ($class_list)";
	$arr_user_list = $lpayment->returnVector($sql);
	
	if(sizeof($arr_user_list)>0) {
		$user_list = implode(",",$arr_user_list);
	}
	
} else {
	
	$class_level_name = $TargetClassLevel;
	
	$sql = "SELECT a.UserID FROM 
			INTRANET_USER AS a INNER JOIN 
			INTRANET_CLASS AS b ON (a.ClassName = b.ClassName) INNER JOIN 
			INTRANET_CLASSLEVEL AS c ON b.ClassLevelID = c.ClassLevelID AND c.LevelName = '$class_level_name'";
	$arr_user_list = $lpayment->returnVector($sql);
	
	if(sizeof($arr_user_list)>0) {
		$user_list = implode(",",$arr_user_list);
	}
}

if($user_list != "") {
	
	$arr_target_user = explode(",",$user_list);
	
	if(!empty($TargetStartDate) && !empty($TargetEndDate))
	{
		$start_day = substr($TargetStartDate,8,2);
		$start_month = substr($TargetStartDate,5,2);
		$start_year = substr($TargetStartDate,0,4);
		$StartDate = date("Y-m-d H:i:s",mktime(0,0,0,$start_month,$start_day,$start_year));
		
		$end_day = substr($TargetEndDate,8,2);
		$end_month = substr($TargetEndDate,5,2);
		$end_year = substr($TargetEndDate,0,4);
		$EndDate = date("Y-m-d H:i:s",mktime(23,59,59,$end_month,$end_day,$end_year));
		
		$date_range_cond = " AND (b.TransactionTime BETWEEN '$StartDate' AND '$EndDate') ";
	}
	
	if(sizeof($arr_target_user)>0) {
		for($i=0; $i<sizeof($arr_target_user); $i++) {
			$user_id = $arr_target_user[$i];
			
			$sql  = "SELECT
							a.UserLogin,
							a.ClassName,
							a.ClassNumber,
							a.ChineseName,
							a.EnglishName,
							DATE_FORMAT(b.TransactionTime,'%Y-%m-%d %H:%i'),
							IF(b.TransactionType=1,DATE_FORMAT(c.TransactionTime,'%Y-%m-%d %H:%i'),'--'),
							CASE b.TransactionType
								WHEN 1 THEN '$i_Payment_TransactionType_Credit'
			                    WHEN 2 THEN '$i_Payment_TransactionType_Payment'
			                    WHEN 3 THEN '$i_Payment_TransactionType_Purchase'
			                    WHEN 4 THEN '$i_Payment_TransactionType_TransferTo'
			                    WHEN 5 THEN '$i_Payment_TransactionType_TransferFrom'
			                    WHEN 6 THEN '$i_Payment_TransactionType_CancelPayment'
			                    WHEN 7 THEN '$i_Payment_TransactionType_Refund'
			                    WHEN 8 THEN '$i_Payment_PPS_Charge'
			                    ELSE '$i_Payment_TransactionType_Other' END,
							IF(b.TransactionType IN (1,5,6),".$lpayment->getWebDisplayAmountFormatDB("b.Amount").",' '),
							IF(b.TransactionType IN (2,3,4,7,8,9,10),".$lpayment->getWebDisplayAmountFormatDB("b.Amount").",' '),
							b.Details, 
							IF(b.BalanceAfter>=0,".$lpayment->getWebDisplayAmountFormatDB("b.BalanceAfter").",".$lpayment->getWebDisplayAmountFormatDB("0")." ),
							b.RefCode
					FROM
							INTRANET_USER AS a INNER JOIN 
							PAYMENT_OVERALL_TRANSACTION_LOG as b ON (a.UserID = b.StudentID) LEFT OUTER JOIN 
							PAYMENT_CREDIT_TRANSACTION as c ON (b.RelatedTransactionID = c.TransactionID)
					WHERE
							b.StudentID IN ($user_id) AND
							(
								b.Details LIKE '%$keyword%'
							)
							$date_range_cond
			                ";
			
			$arr_result = $lpayment->returnArray($sql,13);
			
			$content.="\"$i_UserLogin\",";
			$content.="\"$i_UserClassName\",";
			$content.="\"$i_UserClassNumber\",";
			$content.="\"$i_UserChineseName\",";
			$content.="\"$i_UserEnglishName\",";
			$content.="\"$i_Payment_Field_TransactionTime\",";
			$content.="\"$i_Payment_Field_TransactionFileTime\",";
			$content.="\"$i_Payment_Field_TransactionType\",";
			$content.="\"$i_Payment_TransactionType_Credit\",";
			$content.="\"$i_Payment_TransactionType_Debit\",";
			$content.="\"$i_Payment_Field_TransactionDetail\",";
			$content.="\"$i_Payment_Field_BalanceAfterTransaction\",";
			$content.="\"$i_Payment_Field_RefCode\",";
			$content.="\n";
			
			$rows[] = array($i_UserLogin, 
						$i_UserClassName, 
						$i_UserClassNumber, 
						$i_UserChineseName, 
						$i_UserEnglishName, 
						$i_Payment_Field_TransactionTime, 
						$i_Payment_Field_TransactionFileTime, 
						$i_Payment_Field_TransactionType, 
						$i_Payment_TransactionType_Credit, 
						$i_Payment_TransactionType_Debit,
						$i_Payment_Field_TransactionDetail,
						$i_Payment_Field_BalanceAfterTransaction,
						$i_Payment_Field_RefCode);
						
			if(sizeof($arr_result)>0) {
				for($j=0; $j<sizeof($arr_result); $j++) {
					list($user_login, $class_name, $class_number, $chi_name, $eng_name, $tran_time, $tran_file_time, $tran_type, $credit_amount, $debit_amount, $tran_details, $bal_after, $ref_code) = $arr_result[$j];
			
					$content.="\"$user_login\",";
					$content.="\"$class_name\",";
					$content.="\"$class_number\",";
					$content.="\"$chi_name\",";
					$content.="\"$eng_name\",";
					$content.="\"$tran_time\",";
					$content.="\"$tran_file_time\",";
					$content.="\"$tran_type\",";
					$content.="\"$credit_amount\",";
					$content.="\"$debit_amount\",";
					$content.="\"$tran_details\",";
					$content.="\"$bal_after\",";
					$content.="\"$ref_code\",";
					$content.="\n";
					
					$rows[] = array($user_login,$class_name,$class_number,$chi_name,$eng_name,$tran_time,
									$tran_file_time,$tran_type,$credit_amount,$debit_amount,$tran_details,
									$bal_after,$ref_code);
					
				}
			} else {
				
				$sql = "SELECT 
								UserLogin,
								ClassName,
								ClassNumber,
								ChineseName,
								EnglishName
						FROM 
								INTRANET_USER
						WHERE
								UserID = $user_id";
				
				$arr_no_record_user = $lpayment->returnArray($sql,5);
				
				if(sizeof($arr_no_record_user)>0){
					list($user_login, $class_name, $class_number, $chi_name, $eng_name) = $arr_no_record_user[0];
					$content .= "\"$user_login\",";
					$content .= "\"$class_name\",";
					$content .= "\"$class_number\",";
					$content .= "\"$chi_name\",";
					$content .= "\"$eng_name\",";
					$content .= "\"$i_no_record_exists_msg\",";
					$content .= "\n";
					$rows[] = array($user_login,$class_name,$class_number,$chi_name,$eng_name,$i_no_record_exists_msg);
				}
				
			}
			
			$content .= "\n\n";
			$rows[] = "";
			$rows[] = "";
		}
	}
}
intranet_closedb();
		

$display="\"$i_Payment_Menu_Report_StudentBalance\"\n";
$display.="\"$i_general_startdate\",\"$TargetStartDate\"\n";
$display.="\"$i_general_enddate\",\"$TargetEndDate\"\n";
$display.="\n\n";
$utf_display="$i_Payment_Menu_Report_StudentBalance\r\n";
$utf_display.="$i_general_startdate\t$TargetStartDate\r\n";
$utf_display.="$i_general_enddate\t$TargetEndDate\r\n";
$utf_display.="\r\n\r\n";


$timestamp = time();
$filename = "student_balance_report_".$timestamp.".csv";
$file_content .= $content;

if($g_encoding_unicode){
	$export_content = $utf_display.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
}else{
	output2browser($display.$file_content,$filename);
}

?>
