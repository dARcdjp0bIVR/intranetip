<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();

$li = new libpayment();

$x="<table width=95% border=0  cellpadding=2 cellspacing=0 align='center' class='$css_table'>";
$x.="<Tr class='$css_table_title'>";
$x.="<td width=15% class='$css_table_title'>$i_Payment_Field_PaymentItem</td>";
$x.="<td width=15% class='$css_table_title'>$i_Payment_Field_PaymentCategory</td>";
$x.="<td width=12% class='$css_table_title'>$i_general_startdate</td>";
$x.="<td width=12% class='$css_table_title'>$i_general_enddate</td>";
$x.="<td width=10% class='$css_table_title'>$i_general_status</td>";
$x.="<td class='$css_table_title'>$i_Payment_Field_TotalPaidCount</td>";
$x.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_PaidStudentCount</td>";
$x.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_PaidAmount</td>";
$x.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_UnpaidStudentCount</td>";
$x.="<td class='$css_table_title'>$i_Payment_PresetPaymentItem_UnpaidAmount</td>";

$x.="</tr>";




# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$today = date('Y-m-d');
	
$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$FromDate'";		

$cat_cond = $CatID==""?"":" AND a.CatID='$CatID' ";

$sql="SELECT a.ItemID FROM PAYMENT_PAYMENT_ITEM AS a WHERE $date_cond $cat_cond AND a.Name LIKE '%$keyword%' ";
$temp = $li->returnVector($sql);
if(sizeof($temp)>0){
	$item_list = implode(",",$temp);
	$sql="SELECT a.ItemID, 
				a.Name,
				b.Name,
				a.StartDate,
				a.EndDate,
				IF('$today'<DATE_FORMAT(a.StartDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_NotStarted',
				IF('$today'>DATE_FORMAT(a.EndDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_Ended','$i_Payment_PresetPaymentItem_Progress')) AS Status
				FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
				WHERE a.ItemID IN($item_list) ORDER BY a.EndDate DESC";
	$temp = $li->returnArray($sql,6);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name,$cat_name,$start_date,$end_date,$status)=$temp[$i];
		$result[$item_id]['name']		= $item_name;
		$result[$item_id]['catname']	= $cat_name;
		$result[$item_id]['startdate']	= $start_date;
		$result[$item_id]['enddate']	= $end_date;
		$result[$item_id]['status']		= $status;
		$result[$item_id]['paid']	= 0;
		$result[$item_id]['unpaid']= 0;
		$result[$item_id]['paidcount']	= 0;
		$result[$item_id]['unpaidcount']= 0;
		
	}
	$sql="SELECT b.ItemID,
				b.RecordStatus,
				b.Amount FROM PAYMENT_PAYMENT_ITEMSTUDENT AS b WHERE b.ItemID IN ($item_list) ORDER BY b.ItemID,b.RecordStatus";	
	$temp = $li->returnArray($sql,3);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$record_status,$amount) = $temp[$i];
		if($record_status==1){ # Paid
			$result[$item_id]['paid']+=($amount>0?$amount:0);
			$result[$item_id]['paidcount']++; 
		}else{
			$result[$item_id]['unpaid']+=($amount>0?$amount:0);
			$result[$item_id]['unpaidcount']++; 
		}
	}
	
	if(sizeof($result)>0){
		$j=0;
		foreach($result as $item_id =>$values){
			//$css = $j%2==0?"tableContent":"tableContent2";
			$css=$j%2==0?$css_table_content:$css_table_content."2";
			$j++;
			$item_name = $values['name'];
			$cat_name  = $values['catname'];
			$start_date= $values['startdate'];
			$end_date  = $values['enddate'];
			$status    = $values['status'];
			$paid	   = $values['paid'];
			$unpaid	   = $values['unpaid'];
			$paidcount = $values['paidcount'];
			$unpaidcount= $values['unpaidcount'];
			$total  = $paidcount + $unpaidcount;
			
			$x.="<tr>";
			$x.="<td class='$css'>$item_name</td>";
			$x.="<td class='$css'>$cat_name</td>";
			$x.="<td class='$css'>$start_date</td>";
			$x.="<td class='$css'>$end_date</td>";
			$x.="<td class='$css'>$status</td>";
			$x.="<td class='$css'>$total</td>";
			$x.="<td class='$css'>$paidcount</td>";
			$x.="<td class='$css'>".$li->getWebDisplayAmountFormat($paid)."</td>";
			$x.="<td class='$css'>$unpaidcount</td>";
			$x.="<td class='$css'>".$li->getWebDisplayAmountFormat($unpaid)."</td>";
			$x.="</tr>";
		}
	}
	
	
}else{ # no record
	$x.="<tr><td colspan=10 align=center class='$css_table_content' height=40 style='vertical-align:middle'>$i_no_record_exists_msg</td></tr>";
}


$x.="</table>";

$display=$x;

//$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
//$toolbar2 = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate&ToDate=$ToDate')>".exportIcon()."$button_export</a>";

//echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_PresetItem,'');


if($CatID!=''){
	$sql="SELECT Name FROM PAYMENT_PAYMENT_CATEGORY WHERE CatID = '$CatID'";
	$temp = $li->returnVector($sql);
	$cat_name = $temp[0];
}else{
	$cat_name = $i_status_all;
}
$keyword= $keyword==""?"--":$keyword;
?>
<form name="form1" method="get" action=''>
<!-- date range -->
<table border=0 width=95% align=center>
<tr><td class='<?=$css_title?>'><b><?=$i_Payment_Menu_Report_PresetItem?> (<?="$FromDate $i_Profile_To $ToDate"?>)</b></td></tr>
</table>
<table border=0 width=95% align=center>
<tr><td class='<?=$css_text?>'><B><?=$i_Payment_Field_PaymentCategory?></B>: <?=$cat_name?></td></tr>
<tr><td class='<?=$css_text?>'><B><?=$i_Payment_Field_PaymentItem?></B>: <?=$keyword?></td></tr>
</table>
</form>
<?=$y?>

<?=$display?>
<table width=95% border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>

<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
