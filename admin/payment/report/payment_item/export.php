<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lpayment = new libpayment();

$lexport = new libexporttext();

$x.="\"$i_Payment_Field_PaymentItem\",";
$x.="\"$i_Payment_Field_PaymentCategory\",";
$x.="\"$i_general_startdate\",";
$x.="\"$i_general_enddate\",";
$x.="\"$i_general_status\",";
$x.="\"$i_Payment_Field_TotalPaidCount\",";
$x.="\"$i_Payment_PresetPaymentItem_PaidStudentCount\",";
$x.="\"$i_Payment_PresetPaymentItem_PaidAmount\",";
$x.="\"$i_Payment_PresetPaymentItem_UnpaidStudentCount\",";
$x.="\"$i_Payment_PresetPaymentItem_UnpaidAmount\"";
$x.="\n";

$exportColumn = array($i_Payment_Field_PaymentItem, 
						$i_Payment_Field_PaymentCategory, 
						$i_general_startdate, 
						$i_general_enddate, 
						$i_general_status, 
						$i_Payment_Field_TotalPaidCount, 
						$i_Payment_PresetPaymentItem_PaidStudentCount, 
						$i_Payment_PresetPaymentItem_PaidAmount, 
						$i_Payment_PresetPaymentItem_UnpaidStudentCount, 
						$i_Payment_PresetPaymentItem_UnpaidAmount);


# date range
$today_ts = strtotime(date('Y-m-d'));
if($FromDate=="")
	$FromDate = date('Y-m-d',getStartOfAcademicYear($today_ts));
if($ToDate=="")
	$ToDate = date('Y-m-d',getEndOfAcademicYear($today_ts));

$today = date('Y-m-d');
	
$date_cond =" DATE_FORMAT(a.StartDate,'%Y-%m-%d')<='$ToDate' AND DATE_FORMAT(a.EndDate,'%Y-%m-%d')>='$FromDate'";		

$cat_cond = $CatID==""?"":" AND a.CatID='$CatID' ";

$sql="SELECT a.ItemID FROM PAYMENT_PAYMENT_ITEM AS a WHERE $date_cond $cat_cond AND a.Name LIKE '%$keyword%' ";


//$sql="SELECT a.ItemID FROM PAYMENT_PAYMENT_ITEM AS a WHERE $date_cond ";
$temp = $lpayment->returnVector($sql);
if(sizeof($temp)>0){
	$item_list = implode(",",$temp);
	$sql="SELECT a.ItemID, 
				a.Name,
				b.Name,
				a.StartDate,
				a.EndDate,
				IF('$today'<DATE_FORMAT(a.StartDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_NotStarted',
				IF('$today'>DATE_FORMAT(a.EndDate,'%Y-%m-%d'),'$i_Payment_PaymentStatus_Ended','$i_Payment_PresetPaymentItem_Progress')) AS Status
				FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID)
				WHERE a.ItemID IN($item_list) ORDER BY a.EndDate DESC";
	$temp = $lpayment->returnArray($sql,6);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$item_name,$cat_name,$start_date,$end_date,$status)=$temp[$i];
		$result[$item_id]['name']		= $item_name;
		$result[$item_id]['catname']	= $cat_name;
		$result[$item_id]['startdate']	= $start_date;
		$result[$item_id]['enddate']	= $end_date;
		$result[$item_id]['status']		= $status;
		$result[$item_id]['paid']	= 0;
		$result[$item_id]['unpaid']= 0;
		$result[$item_id]['paidcount']	= 0;
		$result[$item_id]['unpaidcount']= 0;
		
	}
	$sql="SELECT b.ItemID,
				b.RecordStatus,
				b.Amount FROM PAYMENT_PAYMENT_ITEMSTUDENT AS b WHERE b.ItemID IN ($item_list) ORDER BY b.ItemID,b.RecordStatus";	
	$temp = $lpayment->returnArray($sql,3);
	
	for($i=0;$i<sizeof($temp);$i++){
		list($item_id,$record_status,$amount) = $temp[$i];
		if($record_status==1){ # Paid
			$result[$item_id]['paid']+=($amount>0?$amount:0);
			$result[$item_id]['paidcount']++; 
		}else{
			$result[$item_id]['unpaid']+=($amount>0?$amount:0);
			$result[$item_id]['unpaidcount']++; 
		}
	}
	
	if(sizeof($result)>0){
		$j=0;
		foreach($result as $item_id =>$values){
			$css = $j%2==0?"tableContent":"tableContent2";
			$j++;
			$item_name = $values['name'];
			$cat_name  = $values['catname'];
			$start_date= $values['startdate'];
			$end_date  = $values['enddate'];
			$status    = $values['status'];
			$paid	   = $values['paid'];
			$unpaid	   = $values['unpaid'];
			$paidcount = $values['paidcount'];
			$unpaidcount= $values['unpaidcount'];
			$total  = $paidcount + $unpaidcount;
			
			$x.="";
			$x.="\"$item_name\",";
			$x.="\"$cat_name\",";
			$x.="\"$start_date\",";
			$x.="\"$end_date\",";
			$x.="\"$status\",";
			$x.="\"$total\",";
			$x.="\"$paidcount\",";
			$x.="\"".$lpayment->getExportAmountFormat($paid)."\",";
			$x.="\"$unpaidcount\",";
			$x.="\"".$lpayment->getExportAmountFormat($unpaid)."\"";
			$x.="\n";
			
			$rows[] = array($item_name, $cat_name, $start_date, $end_date, $status, $total,
							$paidcount, $lpayment->getExportAmountFormat($paid), $unpaidcount, $lpayment->getExportAmountFormat($unpaid)
							);
		}
	}
	
	
}else{ # no record
	$x.="\"$i_no_record_exists_msg\"\n";
	$rows[] = $i_no_record_exists_msg;
}

$x.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$rows[] = array($i_general_report_creation_time, date('Y-m-d H:i:s'));



if($CatID!=''){
	$sql="SELECT Name FROM PAYMENT_PAYMENT_CATEGORY WHERE CatID = '$CatID'";
	$temp = $lpayment->returnVector($sql);
	$cat_name = $temp[0];
}else{
	$cat_name = $i_status_all;
}
$keyword= $keyword==""?"--":$keyword;


$display="\"$i_Payment_Menu_Report_PresetItem ($FromDate $i_Profile_To $ToDate)\"\n";
$display.="\"$i_Payment_Field_PaymentCategory\",\"$cat_name\"\n";
$display.="\"$i_Payment_Field_PaymentItem\",\"$keyword\"\n";
$utf_display="$i_Payment_Menu_Report_PresetItem ($FromDate $i_Profile_To $ToDate)\r\n";
$utf_display.="$i_Payment_Field_PaymentCategory\t$cat_name\r\n";
$utf_display.="$i_Payment_Field_PaymentItem\t$keyword\r\n";

//$display.=$x;


intranet_closedb();

$filename = "payment_item_report.csv";
//output2browser($display,$filename);

if($g_encoding_unicode){
	$export_content = $utf_display.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
}else{
	output2browser($display.$x,$filename);
}


?>
