<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$lpayment = new libpayment();
$lexport = new libexporttext();


# Get Item Name , Category Name, Start Date , End Date
$sql=" SELECT a.Name,b.Name,a.StartDate,a.EndDate FROM PAYMENT_PAYMENT_ITEM AS a LEFT OUTER JOIN PAYMENT_PAYMENT_CATEGORY AS b ON (a.CatID = b.CatID) WHERE a.ItemID='$ItemID'";
$temp = $lpayment->returnArray($sql,4);
list($item_name,$cat_name,$start_date,$end_date) = $temp[0];
$x="\"$i_Payment_PresetPaymentItem_ClassStat\"\n";
$x.="\"$i_Payment_Field_PaymentItem\",\"$item_name\"\n";
$x.="\"$i_Payment_Field_PaymentCategory\",\"$cat_name\"\n";
$x.="\"$i_Payment_PresetPaymentItem_PaymentPeriod\",\"$start_date $i_To $end_date\"\n";

$preheader = $i_Payment_PresetPaymentItem_ClassStat."\r\n";
$preheader .= $i_Payment_Field_PaymentItem."\t".$item_name."\r\n";
$preheader .= $i_Payment_Field_PaymentCategory."\t".$cat_name."\r\n";
$preheader .= $i_Payment_PresetPaymentItem_PaymentPeriod."\t".$start_date." ".$i_To." ".$end_date."\r\n";


/*	
$sql=" SELECT a.Amount, a.RecordStatus, b.ClassName FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
		LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) WHERE a.ItemID='$ItemID' AND b.ClassName IS NOT NULL  ORDER BY b.ClassName";
*/
$sql=" SELECT a.Amount, IF(a.SubsidyUnitID IS NOT NULL AND a.SubsidyUnitID <>'' , IF(a.SubsidyAmount>0,a.SubsidyAmount,0),0) AS SubAmount,
				a.RecordStatus, 
				IF(b.UserID IS NULL AND c.UserID IS NOT NULL, c.ClassName,b.ClassName) 
		FROM PAYMENT_PAYMENT_ITEMSTUDENT AS a 
				LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
				LEFT OUTER JOIN INTRANET_ARCHIVE_USER AS c ON (a.StudentID = c.UserID)
		WHERE a.ItemID='$ItemID'  ORDER BY b.ClassName,c.ClassName";
	

$temp = $lpayment->returnArray($sql,4);


$y="\"$i_ClassName\","; 
$y.="\"$i_Payment_Field_TotalPaidCount\","; 
$y.="\"$i_Payment_PresetPaymentItem_PaidStudentCount\","; 
$y.="\"$i_Payment_PresetPaymentItem_UnpaidStudentCount\","; 
$y.="\"$i_Payment_PresetPaymentItem_PaidAmount\","; 
$y.="\"$i_Payment_PresetPaymentItem_UnpaidAmount\","; 
$y.="\"$i_Payment_Subsidy_Amount\",";
$y.="\"$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid\"";
$y.="\n";

$exportColumn = array($i_ClassName, $i_Payment_Field_TotalPaidCount, $i_Payment_PresetPaymentItem_PaidStudentCount, $i_Payment_PresetPaymentItem_UnpaidStudentCount, $i_Payment_PresetPaymentItem_PaidAmount, $i_Payment_PresetPaymentItem_UnpaidAmount,$i_Payment_Subsidy_Amount,$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid);

if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		list($amount,$sub_amount,$isPaid,$class_name) = $temp[$i];
		$result["$class_name"]['total']++;
		if($isPaid){
			$result["$class_name"]['paidcount']++;
			$result["$class_name"]['paid']+=$amount+0;
		}else{
			$result["$class_name"]['unpaidcount']++;
			$result["$class_name"]['unpaid']+=$amount+0;
		}
		$result["$class_name"]['subsidy'] +=$sub_amount+0;
	}
	foreach($result as $class => $values){
		
		$total = $values['total'];
		$paid_count = $values['paidcount'];
		$unpaid_count = $values['unpaidcount'];
		$paid = $values['paid'];
		$unpaid=$values['unpaid'];
		$sub_amount = $values['subsidy'];				
		
		$overall_total +=$total+0;
		$overall_paidcount+=$paid_count;
		$overall_unpaidcount+=$unpaid_count;
		$overall_paid +=$paid;
		$overall_unpaid+=$unpaid;
		$overall_subsidy+=$sub_amount;
		$overall_paid_subsidy+=$sub_amount+$paid;		
						
		$y.="\"$class\",";
		$y.="\"$total\",";
		$y.="\"".($paid_count==""?0:$paid_count)."\",";
		$y.="\"".($unpaid_count==""?0:$unpaid_count)."\",";
		$y.="\"".$lpayment->getExportAmountFormat($paid)."\",";
		$y.="\"".$lpayment->getExportAmountFormat($unpaid)."\",";
		$y.="\"".$lpayment->getExportAmountFormat($sub_amount)."\",";
		$y.="\"".$lpayment->getExportAmountFormat($sub_amount+$paid)."\"";		
		$y.="\n";
		$rows[] = array($class, $total, ($paid_count==""?0:$paid_count), ($unpaid_count==""?0:$unpaid_count), $lpayment->getExportAmountFormat($paid), $lpayment->getExportAmountFormat($unpaid),$lpayment->getExportAmountFormat($sub_amount),$lpayment->getExportAmountFormat($sub_amount+$paid));
	}
	$y.="\"$i_Payment_SchoolAccount_Total\",\"$overall_total\",\"$overall_paidcount\",\"$overall_unpaidcount\",\"".$lpayment->getExportAmountFormat($overall_paid)."\",\"".$lpayment->getExportAmountFormat($overall_unpaid)."\",\"".$lpayment->getExportAmountFormat($overall_subsidy)."\",\"".$lpayment->getExportAmountFormat($overall_paid_subsidy)."\"\n";
	$rows[] = array($i_Payment_SchoolAccount_Total, $overall_total, $overall_paidcount, $overall_unpaidcount, $lpayment->getExportAmountFormat($overall_paid), $lpayment->getExportAmountFormat($overall_unpaid),$lpayment->getExportAmountFormat($overall_subsidy),$lpayment->getExportAmountFormat($overall_paid_subsidy));

	
}else{ # no record
	$y.="\"$i_no_record_exists_msg\"\n";
	$rows[] = $i_no_record_exists_msg;
}


$display=$x.$y;
$display.="\"$i_general_report_creation_time\",\"".date('Y-m-d H:i:s')."\"\n";
$rows[] = array($i_general_report_creation_time, date('Y-m-d H:i:s'));

intranet_closedb();

$filename = "class_stats.csv";
//output2browser($display,$filename);

if($g_encoding_unicode){
	$export_content = $preheader.$lexport->GET_EXPORT_TXT($rows, $exportColumn);
//$filename = "eclass-user-".session_id()."-".time().".csv";
	$lexport->EXPORT_FILE($filename, $export_content);
}else output2browser($display,$filename);

?>
