<?php
// using kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

intranet_opendb();

$li = new libpayment();
$lb = new libdbtable("","","");

# 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
# 2 - Payment Items
# 3 - Single Purchase
# 4 - Transfer FROM
# 5 - Transfer TO
# 6 - Cancel Payment
# 7 - Refund
# 8 - PPS Charges
# 9 - Cancel Cash Deposit
# 10 - Donation to school
$target_type=array(1,2,3,6,7,8,9,10);

$target_type_name[1]=$i_Payment_ValueAddedRecord;
$target_type_name[2]=$i_Payment_SchoolAccount_PresetItem;
$target_type_name[3]=$i_Payment_SchoolAccount_SinglePurchase;
$target_type_name[6]=$i_Payment_TransactionType_CancelPayment;
$target_type_name[7]=$i_Payment_SchoolAccount_Other;
//$target_type_name[8]=$i_Payment_SchoolAccount_Other;


$type_cond = " TransactionType IN (".implode(",",$target_type).")";

$today = date('Y-m-d');
$FromDate = $FromDate==""?$today:$FromDate;
	
$date_cond =" DATE_FORMAT(TransactionTime,'%Y-%m-%d')='$FromDate'";		

$sql =	"SELECT 
					IF(TransactionType=8 OR TransactionType=9 OR TransactionType=10,7,TransactionType),
					Amount,
					Details 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG 
				WHERE 
					$date_cond AND $type_cond 
				ORDER BY TransactionType,Details";
$temp = $li->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($tran_type,$amount,$detail)=$temp[$i];
	$result[$tran_type]["$detail"]+=$amount+0;
}
$x="";
$x.="<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=0 cellspacing=0 align='center'>";
foreach($target_type_name as $type => $name){
	$x.="<tr><td class='tableTitle' height=20 style='vertical-align:middle' width='80%'><u>$name</u></td><td height=20 style='vertical-align:middle' class='tableTitle' width='20%'>$i_Payment_Field_Amount</td></tr>";
	$values = $result[$type];
	if(sizeof($values)>0){
		$j=0;
		$total=0;
		foreach($values as $detail => $amount){
			$css =$j%2==0?"tableContent":"tableContent2";
			$total+=$amount+0;
			$x.="<tr><td class='$css'>$detail&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($amount)."</td></tr>";
			$j++;
	 	}
		$css ='tableTitle2';
	 	$x.="<tr><td class='$css' align='right'>$i_Payment_SchoolAccount_Total:&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($total)."</td></tr>";
	 	//$x.="<tr><td colspan='2' class='tableContent' height=>&nbsp;</td></tr>";
	}
	else $x.="<tr><td align=center class=tableContent height=40 style='vertical-align:middle' colspan=2>$i_no_record_exists_msg<td></tr>";
}
$x.="</table>";
$display=$x;

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon()."$i_PrinterFriendlyPage</a>";
$toolbar2 = "<a class=iconLink href=javascript:exportPage(document.form1,'export.php?FromDate=$FromDate')>".exportIcon()."$button_export</a>";

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_TodayTransaction,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
        var css_array = new Array;
        css_array[0] = "dynCalendar_free";
        css_array[1] = "dynCalendar_half";
        css_array[2] = "dynCalendar_full";
        var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
                  // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].FromDate.value = dateValue;

          }

        function checkForm(formObj){
                if(formObj==null)return false;
                        fromV = formObj.FromDate;
                        toV= formObj.ToDate;
                        if(!checkDate(fromV)){
                                        //formObj.FromDate.focus();
                                        return false;
                        }
                                return true;
        }
        function checkDate(obj){
                         if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                        return true;
        }
        function submitForm(obj){
                if(checkForm(obj))
                        obj.submit();
        }
        function openPrintPage()
	{
         newWindow("print.php?FromDate=<?=$FromDate?>",8);
	}
		function exportPage(obj,url){
			old_url = obj.action;
	        obj.action=url;
	        obj.submit();
	        obj.action = old_url;
	        
	}
</script>
<form name="form1" method="get" action=''>
<!-- date range -->
<table border=0 width=560 align=center>
	<tr>
		<td nowrap class=tableContent>
		<?=$i_Payment_Menu_PrintPage_AddValueReport_Date?>:
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 <a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

</form>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar","")?></td></tr>
<tr><td class=admin_bg_menu><?=$lb->displayFunctionbar("-", "-", "$toolbar2","")?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>

<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
