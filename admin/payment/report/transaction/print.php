<?php
// using kenneth chung
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();

$li = new libpayment();


# 1 - Add Value ( Cash Deposit, PPS, Add Value Machine )
# 2 - Payment Items
# 3 - Single Purchase
# 4 - Transfer FROM
# 5 - Transfer TO
# 6 - Cancel Payment
# 7 - Refund
# 8 - PPS Charges
# 9 - Cancel Cash Deposit
# 10 - Donation to school
$target_type=array(1,2,3,6,7,8,9,10);

$target_type_name[1]=$i_Payment_ValueAddedRecord;
$target_type_name[2]=$i_Payment_SchoolAccount_PresetItem;
$target_type_name[3]=$i_Payment_SchoolAccount_SinglePurchase;
$target_type_name[6]=$i_Payment_TransactionType_CancelPayment;
$target_type_name[7]=$i_Payment_SchoolAccount_Other;
//$target_type_name[8]=$i_Payment_SchoolAccount_Other;


$type_cond = " TransactionType IN (".implode(",",$target_type).")";

$today = date('Y-m-d');
$FromDate = $FromDate==""?$today:$FromDate;
	
$date_cond =" DATE_FORMAT(TransactionTime,'%Y-%m-%d')='$FromDate'";		

$sql =	"SELECT 
					IF(TransactionType=8 OR TransactionType=9 OR TransactionType=10,7,TransactionType),
					Amount,
					Details 
				FROM 
					PAYMENT_OVERALL_TRANSACTION_LOG 
				WHERE 
					$date_cond AND $type_cond 
				ORDER BY TransactionType,Details";
$temp = $li->returnArray($sql,3);

for($i=0;$i<sizeof($temp);$i++){
	list($tran_type,$amount,$detail)=$temp[$i];
	$result[$tran_type]["$detail"]+=$amount+0;
}
$x="";
$x.="<table width=90% border=0  cellpadding=0 cellspacing=0 align='center' class='$css_table'>";
foreach($target_type_name as $type => $name){
	$x.="<tr><td class='$css_table_title' height=20 style='vertical-align:middle' width='80%'><u>$name</u></td><td height=20 style='vertical-align:middle'  width='20%'  class='$css_table_title'>$i_Payment_Field_Amount</td></tr>";
	$values = $result[$type];
	if(sizeof($values)>0){
		$j=0;
		$total=0;
		foreach($values as $detail => $amount){
			//$css =$j%2==0?"tableContent":"tableContent2";
			$css = $j%2==0?$css_table_content:$css_table_content."2";
			$total+=$amount+0;
			$x.="<tr><td class='$css'>$detail&nbsp;</td><td class='$css'>".$li->getWebDisplayAmountFormat($amount)."</td></tr>";
			$j++;
	 	}
		//$css ='tableTitle2';
		$x.="<tr><td class='$css_table_content' colspan='2'><img src='$image_path/space.gif' border=0 height=1></td></tr>";
		$css=$css_table_title;
	 	$x.="<tr><td class='$css' align='right'>$i_Payment_SchoolAccount_Total:&nbsp;</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($total)."</td></tr>";
	 	//$x.="<tr><td colspan='2' class='tableContent' height=>&nbsp;</td></tr>";
	}
	else $x.="<tr><td align=center class='$css_table_content' height=40 style='vertical-align:middle' colspan=2>$i_no_record_exists_msg<td></tr>";
}
$x.="</table>";
$display=$x;


//echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_Report_TodayTransaction,'');
?>
<!-- date range -->
<table border=0 width=90% align=center>
<tr><td class='<?=$css_title?>'><?=$i_Payment_Menu_Report_TodayTransaction?> (<?="$FromDate"?>)</td></tr>
</table>


<?=$display?>
<table width=90% border=0 cellpadding=0 cellspacin=0 align=center>
<tr><td align=right class='<?=$css_text?>'>
<?="$i_general_report_creation_time : ".date('Y-m-d H:i:s')?>
</td></tr>
</table>
<BR><BR>

<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>
