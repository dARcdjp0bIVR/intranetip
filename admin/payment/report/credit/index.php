<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

# date range ( 1st of current month till the last day of current month)
$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;

$select_filter = "<SELECT name='valueadd_type'>";
$select_filter.="<OPTION value=''>$i_status_all</OPTION>";
$select_filter.="<OPTION value='1'>$i_Payment_Credit_TypePPS</OPTION>";
$select_filter.="<OPTION value='2'>$i_Payment_Credit_TypeCashDeposit</OPTION>";
$select_filter.="<OPTION value='3'>$i_Payment_Credit_TypeAddvalueMachine</OPTION>";
$select_filter.="</SELECT>";

$search_by=1;

$format_array = array(
                      array(0,"Web"),
                      array(1,"CSV"));
$select_format = getSelectByArray($format_array, "name=format",0,0,1);

echo displayNavTitle($i_adminmenu_plugin, '', $i_Payment_System, '../../',$i_Payment_Menu_StatisticsReport,'../index.php',$i_Payment_Menu_PrintPage_AddValueReport,'');
echo displayTag("head_payment_$intranet_session_language.gif", $msg);
?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
        var css_array = new Array;
        css_array[0] = "dynCalendar_free";
        css_array[1] = "dynCalendar_half";
        css_array[2] = "dynCalendar_full";
        var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
                  // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].FromDate.value = dateValue;

          }
          function calendarCallback2(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                                           document.forms['form1'].ToDate.value = dateValue;

          }
        function checkForm(formObj){
                if(formObj==null)return false;
                        fromV = formObj.FromDate;
                        toV= formObj.ToDate;
                        if(!checkDate(fromV)){
                                        //formObj.FromDate.focus();
                                        return false;
                        }
                        else if(!checkDate(toV)){
                                                //formObj.ToDate.focus();
                                                return false;
                        }
                                return true;
        }
        function checkDate(obj){
                         if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
                        return true;
        }
        function submitForm(obj){
                if(checkForm(obj))
                        obj.submit();
        }
</script>
<form name="form1" method="get" action='addvalue_report.php' target='_blank'>
<!-- date range -->
<table border=0 width=560 align=center>
<tr><Td></td><td ><input type='radio' name='search_by' value='0'  <?=($search_by!=1?"checked":"")?>><?=$i_Payment_Search_By_PostTime?>&nbsp;&nbsp;<input type='radio' name='search_by' value='1' <?=($search_by==1?"checked":"")?>><?=$i_Payment_Search_By_TransactionTime?></td></tr>
        <tr>
                <td nowrap align=right><?=$i_Payment_Menu_Browse_CreditTransaction_DateRange?>:</td>
                <td><input type=text name=FromDate value="<?=$FromDate?>" size=10>
                                <script language="JavaScript" type="text/javascript">
                                        <!--
                                                startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
                                        //-->
                                </script>&nbsp;&nbsp;
                         <?=$i_Profile_To?>
                         <input type=text name=ToDate value="<?=$ToDate?>" size=10>
                                <script language="JavaScript" type="text/javascript">
                                        <!--
                                                startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
                                        //-->
                                </script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
                                
                                 

        </td></tr>
        <tr><td align=right><?=$i_Payment_Credit_Method?>:</td><td><?=$select_filter?></td></tr>
        <tr><Td align=right><?=$i_general_Format?>:</td><td><?=$select_format?></td></tr>
        <tr><td align=right colspan='2'><hr></td></tr>
 <tr><td></td><td><a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a>
 </td></tr>

</table>

</form>
<?php
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>
