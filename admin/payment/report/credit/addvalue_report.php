<?php

include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libpayment.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
if($format!=1){
	include_once("../../../../templates/fileheader.php");
}


intranet_opendb();



# date range 
# default : FromDate = 1st of current month 
#			ToDate = last day of current month
$FromDate=$FromDate==""?((date('Y-m'))."-01"):$FromDate;
$ToDate=$ToDate==""?(date('Y-m-d',mktime(0, 0, 0, date("m")+1, 0,  date("Y")))):$ToDate;

if($search_by!=1){
	$search_time ="DateInput";
	$date_cond = " AND (DATE_FORMAT(DateInput,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(DateInput,'%Y-%m-%d')<='$ToDate') ";
}else{ 
	$search_time="TransactionTime";
	$date_cond = " AND (DATE_FORMAT(TransactionTime,'%Y-%m-%d') >= '$FromDate' AND DATE_FORMAT(TransactionTime,'%Y-%m-%d')<='$ToDate') ";
}

if($valueadd_type=='')
	$conds = " RecordType IN (1,2,3)";
else $conds = " RecordType= '$valueadd_type'";

$sql = "SELECT 
		DATE_FORMAT($search_time,'%Y/%m/%d') AS TransactionDay,
		SUM(Amount) AS TotalAmount,
		COUNT(TransactionID) AS TransactionsCount 
		FROM PAYMENT_CREDIT_TRANSACTION 
		WHERE $conds $date_cond
		GROUP BY DATE_FORMAT($search_time,'%Y-%m-%d') 
		";
		
$li = new libpayment();
$temp = $li->returnArray($sql,3);
$total_amount = 0;
$total_count  = 0;
for($i=0;$i<sizeof($temp);$i++){
	list($date,$amount,$count)=$temp[$i];
	$amount = $amount+0;
	$result[$date]['amount']=$amount;
	$result[$date]['count'] =$count;
	$total_amount+=$amount;
	$total_count +=$count;
}

$display="<table width=90% border=0 cellpadding=1 cellspacing=0 align=center class='$css_table'>";
$display.="<tr class='$css_table_title'>";
$display.="<td class='$css_table_title' width=30%>$i_Payment_Menu_PrintPage_AddValueReport_Date</td>";
$display.="<td class='$css_table_title' width=30%>$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal</td>";
$display.="<td class='$css_table_title' width=30%>$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount</td>";
$display.="</tr>";

$csv = "\"$i_Payment_Menu_PrintPage_AddValueReport_Date\",\"$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal\",\"$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount\"\n";
$csv_utf = "$i_Payment_Menu_PrintPage_AddValueReport_Date\t$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal\t$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount\r\n";

		
$start_ts =strtotime($FromDate);
$end_ts = strtotime($ToDate);
$i=0;
while($start_ts<=$end_ts){
	$target_date = date('Y/m/d',$start_ts);
	$target_amount = $result[$target_date]['amount']==""?"--":$result[$target_date]['amount'];
	$target_amount_csv = $li->getExportAmountFormat($target_amount);

	$target_amount = $target_amount=="--"?$target_amount:$li->getWebDisplayAmountFormat($target_amount);
	$target_count = $result[$target_date]['count']==""?"--":$result[$target_date]['count'];
	$start_ts += 60*60*24;
	$i++;
	$cn = $i%2==0?"2":"";
	$display.="<tr>";
	$display.="<td class='".($css_table_content.$cn)."'>$target_date</td>";
	$display.="<td class='".($css_table_content.$cn)."'>$target_amount</td>";
	$display.="<td class='".($css_table_content.$cn)."'>$target_count</td>";
	$display.="</tr>";
	
	$csv.="\"$target_date\",\"$target_amount_csv\",\"$target_count\"\n";
	$csv_utf.="$target_date\t$target_amount_csv\t$target_count\r\n";	
}
if($i==0){
	$display.="<tr><Td colspan=3 class='$css_table_content' align=center>$i_StaffAttendance_Status_NoRecord</td></tr>";
}else{
	$display.="<tr><td class='$css_table_content' colspan='3'><img src='$image_path/space.gif' border=0 height=1></td></tr>";
	$display.="<tr><td class='$css_table_title'>$list_total</td><td class='$css_table_content'>".$li->getWebDisplayAmountFormat($total_amount)."</td><td class='$css_table_content'>$total_count</td></tr>";
	$csv.="\"$list_total\",\"".$li->getExportAmountFormat($total_amount)."\",\"$total_count\"\n";
	$csv_utf.="$list_total\t".$li->getExportAmountFormat($total_amount)."\t$total_count\r\n";
}
$display.="</table>";

switch($valueadd_type){
	case '1' : $value_add_type_name=$i_Payment_Credit_TypePPS;break;
	case '2' : $value_add_type_name=$i_Payment_Credit_TypeCashDeposit;break;
	case '3' : $value_add_type_name=$i_Payment_Credit_TypeAddvalueMachine;break;
	default :  $value_add_type_name=$i_status_all;
}
$header_table="<table width=90% border=0 cellpadding=1 cellspacing=0 align=center>";
$header_table.="<tr><td class='$css_title'>$i_Payment_Menu_PrintPage_AddValueReport</td></tr>";
$header_table.="<tr><td class='$css_text'><B>".($search_by==1?$i_Payment_Field_TransactionTime:$i_Payment_Field_PostTime).":</b> $FromDate $i_Profile_To $ToDate</td></tr>";
$header_table.="<tr><td class='$css_text'><B>".$i_Payment_Credit_Method.":</b> ".$value_add_type_name."</td></tr>";
$header_table.="</table>";

$header_csv.="\"$i_Payment_Menu_PrintPage_AddValueReport\"\n";
$header_csv.="\"".($search_by==1?$i_Payment_Field_TransactionTime:$i_Payment_Field_PostTime)."\",\"$FromDate $i_Profile_To $ToDate\"\n";
$header_csv.="\"".$i_Payment_Credit_Method."\",\"$value_add_type_name\"\n\n";

$header_csv_utf.=$i_Payment_Menu_PrintPage_AddValueReport."\r\n";
$header_csv_utf.=($search_by==1?$i_Payment_Field_TransactionTime:$i_Payment_Field_PostTime)."\t$FromDate $i_Profile_To $ToDate\r\n";
$header_csv_utf.=$i_Payment_Credit_Method."\t$value_add_type_name\r\n\r\n";


$display = $header_table.$display;
$csv = $header_csv.$csv;
$csv_utf = $header_csv_utf.$csv_utf;

if ($format == 1)     # Excel
{
    # Get template
    //$template_content = get_file_content("template.html");
    //$output = str_replace("__MAIN_CONTENT__",$display,$template_content);
    //$output_filename = "addvalue_report.xls";
	$output_filename = "addvalue.csv";
	if($g_encoding_unicode){
			$lexport->EXPORT_FILE($output_filename, $csv_utf);
			flush();
	}    
	else{ 
		output2browser($csv,$output_filename);
	    flush();
	
	}

}
else{
	//include_once("../../../../templates/fileheader.php");

?>
<form name="form1" method="get" action='addvalue_report.php'>
<?=$display?>
</form>

<?
}
intranet_closedb();
?>
