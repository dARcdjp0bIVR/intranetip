<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();
$file_format = array("UserLogin","Subject","Classes");

$limport = new libimporttext();

# Open the input data
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: teaching_import.php");
        exit();
} else {
        $ext = strtoupper($lo->file_ext($filename));
        $format_wrong = true;
        if($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
        }
		
        if ($format_wrong)
        {
			$display .= "<tr><td>";
            $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
            $display .= "<table width='90%' border='1' bordercolor='#CCCCCC' align='center' cellpadding=0 cellspacing=0>\n";
            for ($i=0; $i<sizeof($file_format); $i++)
            {
                 $display .= "<tr><td>".$file_format[$i]."</td></tr>\n";
            }
            $display .= "</table></blockquote>\n";
			$display .= "</td></tr>";
        }
        else
        {
            # Retrieve valid teachers, classes and subjects
            $sql = "SELECT UserLogin,UserID FROM INTRANET_USER WHERE RecordType = 1 AND Teaching = 1";
            $temp = $li->returnArray($sql,2);
            $teachers = build_assoc_array($temp);

            $sql = "SELECT ClassName, ClassID FROM INTRANET_CLASS WHERE RecordStatus = 1";
            $temp = $li->returnArray($sql,2);
            $classes = build_assoc_array($temp);
			
			$sql = "SELECT EN_SNAME, RecordID FROM {$eclass_db}.ASSESSMENT_SUBJECT WHERE CMP_CODEID IS NULL ORDER BY DisplayOrder";
			$temp = $li->returnArray($sql,2);
            $subjects = build_assoc_array($temp);
			
            # Browse the input data, convert to array
            for ($i=0; $i<sizeof($data); $i++)
            {
				 $row_result = array();
                 $row = $data[$i];
                 $login = trim($row[0]);
                 if ($login=="") break;  # Assume End of file
                 $recordValid[$i] = true;

                 # Check login name
                 $teacher_id = $teachers[$login];
                 if ($teacher_id == "")
                 {
                     $recordValid[$i] = false;
                     $reason[$i] = "Invalid Login ($login)";
                     continue;
                 }
				
				# get subject id
				$subject_name = trim($row[1]);
				$subject_id = $subjects[$subject_name];
				if ($subject_id=="")
                {
					$recordValid[$i] = false;
                    $reason[$i] = "Invalid Subject name of teaching data ($subject_name)";
                }
				else 
				{
					$class_name_list = trim($row[2]);
					$class_name_array = explode(";", $class_name_list);
					$subj_temp = array();
					$ValidClassCount = 0;
					for($k=0; $k<sizeof($class_name_array); $k++)
					{
						$class_name = $class_name_array[$k];
						$class_id = $classes[$class_name];
						if($class_id!="")
						{
							$ValidClassCount++;
							$refined_data[] = array($teacher_id, $class_id, $subject_id);
						}
					}
					if($ValidClassCount==0)
					{
						$recordValid[$i] = false;
						$reason[$i] = "Invalid Class name of class teacher data (".$class_name_list.")";
					}
				}

                if ($recordValid[$i])
                {
					$sql = "DELETE FROM RC_SUBJECT_TEACHER WHERE UserID = '$teacher_id' AND SubjectID = '$subject_id'";
					$li->db_db_query($sql);
                }
            }

            # Process Refined Data
            $st_values = "";
            $st_delim = "";
            for ($i=0; $i<sizeof($refined_data); $i++)
            {
                 list($teacher_id, $class_id, $subject_id) = $refined_data[$i];

                 $st_values .= "$st_delim ('$teacher_id', '$class_id', '$subject_id')";
                 $st_delim = ",";
            }
			$sql = "INSERT IGNORE INTO RC_SUBJECT_TEACHER (UserID, ClassID, SubjectID) VALUES $st_values";
			$li->db_db_query($sql);
        }

        # Mark Reason to display
		$FailedExist = 0;
        for ($i=0; $i<sizeof($recordValid); $i++)
        {
             if (!$recordValid[$i])
             {
				 if($FailedExist==0)
				 {
					 $FailedExist = 1;
					 $display .= "<blockquote>".$i_Teaching_ImportFailed."</blockquote>\n";
				 }
                  $display .= "<tr><td>Line ".($i+1)."</td><td>".$reason[$i]."</td></tr>\n";
             }
        }
        if ($display != "")
        {
			$table_border_style = ($format_wrong==1) ? "border=0" : "border=1 bordercolor='#CCCCCC'";
            $display = "<table width=90% align=center {$table_border_style} cellspacing=0 cellpadding=4>\n$display\n</table>\n";
        }

}
intranet_closedb();

if ($display != '')
{
    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
    include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
    echo displayNavTitle($i_adminmenu_fs, '', $i_ReportCard_System, 'index.php', $eReportCard["TeachingAppointmentSettings"],  'javascript:history.back()', $button_import, '');
    echo displayTag("head_teaching_$intranet_session_language.gif", $msg);
    if (!$format_wrong)
    {
         $display .= "<table width='90%' border=0 cellpadding=0 cellspacing=0 align='center'>
                        <tr><td><hr size=1></td></tr>
                        <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
    }
    else
    {
         $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                        <tr><td><hr size=1></td></tr>
                        <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_back_$intranet_session_language.gif' border='0'></a></td></tr></table>";
    }
    echo $display;
    include_once($PATH_WRT_ROOT."templates/adminfooter.php");

}
else
	header("Location: teaching.php?msg=2");

?>