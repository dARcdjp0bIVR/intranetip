<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

$lf = new libfilesystem();
if (is_array($target) && sizeof($target)>0)
{
	$target = str_replace("&#160;", "", $target);
	$file_content = implode(",", $target);
}

// added by Andy on 16:50 2008/4/11
// remove empty and non-numeric entries from the array so that no space will be written in the text file
$newTarget = array();
for($i=0; $i<sizeof($target); $i++) {
	if(trim($target[$i]) != "" && is_numeric($target[$i])) {
		$newTarget[] = $target[$i];
	}
}
$target = $newTarget;
//str_replace("&#160;", "", $file_content);
$file_content = trim($file_content, ",");

# Write to file
$li = new libfilesystem();

$location = $intranet_root."/file/reportcard";
$li->folder_new($location);
$file = $location."/admin_user.txt";
$success = $li->file_write($file_content, $file);

# Write to file of eReportCard 1.2 also
$location = $intranet_root."/file/reportcard2008";
$li->folder_new($location);
$file = $location."/admin_user.txt";
$success = $li->file_write($file_content, $file);

intranet_closedb();
header("Location: admin_setting.php?msg=2");
?>