<?php
# using: henry chow

/******************************************
 *  modification log:
 *
 *	20100528 YatWoon
 *		update set_row_array(), maybe import student csv is without CardID
 *
 * ****************************************/
 
@SET_TIME_LIMIT(600);

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libimport.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfilesams.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();

$page = "import.php?";



# process file - libfilesystem.php, libfilesams.php
# process data array - libdb, libimport.php
$li = new libimport();
$lo = new libfilesams();

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

$hasSmartCard = ($plugin['attendancestudent'] || $plugin['payment'] || $plugin['Lunchbox']);
$hasTeacherSmartCard = ($module_version['StaffAttendance'] || $plugin['payment'] );
/*
if ($TabID == 1)	# teacher/staff
{	
	if($hasTeacherSmartCard)
	{
    	$file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","TitleEnglish","TitleChinese","CardID");
  }
	else{
    	$file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","TitleEnglish","TitleChinese");
	}
}
else if ($TabID == 2)	# student
{
    # if ($plugin['attendancestudent'])
    # {
    #     $file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","ClassNumber","ClassName","CardID");
    # }
    if($hasSmartCard)
    {
    	$file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","DOB","WebSAMSRegNo","CardID");
	}
	else
	{
		$file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","DOB","WebSAMSRegNo");
	}
}
else if ($TabID == 3)	# parent
{
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3");
}
elseif ($TabID == 4)	# alumni
{
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","FirstName","LastName","ChineseName","Title","Gender","Mobile","YearOfLeft");
}


if($special_feature['ava_hkid'])
{
	$file_format = array_merge($file_format, array("HKID"));
}

if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4))
{
	$file_format = array_merge($file_format, array("STRN"));
}
*/
if ($TabID == 1)	# teacher/staff
{	
	if($hasTeacherSmartCard)
		$file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Remarks","TitleEnglish","TitleChinese","CardID");
  	else
		$file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Remarks","TitleEnglish","TitleChinese");
}
else if ($TabID == 2)	# student
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","Fax","Remarks","DOB","WebSAMSRegNo");
else if ($TabID == 3)	# parent
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","Gender","Mobile","Fax","Remarks");
/*    
else if ($TabID == TYPE_ALUMNI)	# alumni
    $file_format = array ("UserLogin","Password","UserEmail","EnglishName","ChineseName","NickName","Gender","Mobile","YearOfLeft");
*/

if($TabID == 2 && $hasSmartCard)
	$file_format = array_merge($file_format, array("CardID"));
if($special_feature['ava_hkid']) {
	$file_format = array_merge($file_format, array("HKID"));
	if($TabID==3)
		$file_format = array_merge($file_format, array("StudentLogin1","StudentEngName1","StudentLogin2","StudentEngName2","StudentLogin3","StudentEngName3"));
}

if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4))
	$file_format = array_merge($file_format, array("STRN"));


if($filepath=="none"){
   header("Location: ".$page."TabID=$TabID&filter=1&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&format=$format");
   
} else {
        # Compare format of CSV
        if ($format == 1)
        {
            //$fp = fopen($filepath,"r");
            //$data = fgetcsv($fp,filesize($filepath));
            
            $row = $limport->GET_IMPORT_TXT($filepath);
		    		$col_name = array_shift($row);
		    		
            $formatCorrect = true;
            for ($i=0; $i<sizeof($file_format); $i++)
            {
                 if ($file_format[$i]!=$col_name[$i])
                 {
                     $formatCorrect = false;
                     break;
                 }
            }
        }

        if ($formatCorrect && $TabID != 4)
        {        
            # read file into array
            # return 0 if fail, return csv array if success
            //$row = $lo->get_file_array($format, $filepath, $filename);
            
            # process array
            # return result
            if (is_array($row))
            {
	            /*
        		# special character conversion
        		if(isset($ClassNameCol))
        		{
            		for($i=0; $i<sizeof($row); $i++)
            		{
						$row[$i][$ClassNameCol] = htmlspecialchars(trim($row[$i][$ClassNameCol]));
					}
				}
				*/
                $li->open_webmail = ($open_webmail==1);
                $li->open_file_account= ($open_file==1);
                $li->teaching = $teaching;
                $li->set_format($format);
                $li->process_data($row);
                
# Send welcome campusmail
/*
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
$lcampusmail = new libcampusmail();
$lcampusmail->sendWelcomeMail($li->new_account_userids);
*/
                # SchoolNet integration
                if ($plugin['SchoolNet'])
                {
                    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
                    $lschoolnet = new libschoolnet();
                    if (sizeof($li->new_account_userids)!=0)
                    {
                        $userlist = implode(",",$li->new_account_userids);
                        if ($TabID == 1)
                        {
                            $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '".IntegerSafe($UserID)."'";
                            $data = $li->returnArray($sql,11);
                            $lschoolnet->addStaffUser($data);
                        }
                        else if ($TabID == 2)
                        {
                            $sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '".IntegerSafe($UserID)."'";
                             $data = $li->returnArray($sql,12);
                             $lschoolnet->addStudentUser($data);
                        }
                        else if ($TabID == 3)
                        {
                        }
                    }
                }
                 
                
                $error_msg = $li->getErrorMsg();
                
                if ($error_msg=="")
                {
	                # msg=10  >>> "Import success." 
                    header("Location: ".$page."msg=10&TabID=$TabID&filter=1&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&format=$format");
                }
                else
                {
                    include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
                    echo "$i_import_error <br>\n $error_msg\n";
                    echo  "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";

                    //echo $li->queries;
                    include_once($PATH_WRT_ROOT."templates/adminfooter.php");
                }
            }
        }
        else if ($TabID==4 && $formatCorrect)    # Handle alumni import
        {
            # read file into array
            # return 0 if fail, return csv array if success
            //$row = $lo->get_file_array($format, $filepath, $filename);
            if (is_array($row))
            {
                for ($i=0; $i<sizeof($row); $i++)
                {
                     list($login,$password,$useremail,$engname,$first,$last,$chiname,$title,$gender,$mobile,$classnum,$classname,$yearofleft) = $row[$i];
                     if($li->validateLogin($login) && ($useremail == "" ||$li->validateEmail($useremail) ))
                     {
                        //$target_email = ($useremail==""? $login . "@".$HTTP_SERVER_VARS["SERVER_NAME"] : $useremail);
                        $target_email = ($useremail==""? $login . "@".($intranet_email_generation_domain==""? $HTTP_SERVER_VARS["SERVER_NAME"]:$intranet_email_generation_domain) : $useremail);
                        
                        $target_email = substr($target_email,0,50);
                        $target_password = ($password ==""? intranet_random_passwd(8): $password);
                        $title = strtoupper(str_replace(".","",$title));
                        switch($title)
                        {
                               case "MR":    $title = 0; break;
                               case "MISS":  $title = 1; break;
                               case "MRS":   $title = 2; break;
                               case "MS":    $title = 3; break;
                               case "DR":    $title = 4; break;
                               case "PROF":  $title = 5; break;
                               default:      $title = '';
                        }
                        # Try insert
                        $sql = "INSERT INTO INTRANET_USER (UserLogin, UserEmail,
                                        UserPassword, FirstName, LastName, EnglishName,
                                        ChineseName, Title, Gender,
                                        MobileTelNo, 
                                        RecordStatus, RecordType, DateInput, DateModified,
                                        YearOfLeft)
                                        VALUES ('".$login."', '".$target_email."'
                                        , '".$target_password."', '".$first."'
                                        , '".$last."', '".$engname."'
                                        , '".$chiname."'
                                        , '".$title."', '".$gender."'
                                        , '".$mobile."'
                                        , '9',$TabID, now(), now(),
                                        '".$yearofleft."')";
                        $li->db_db_query($sql);
                        if ($li->db_affected_rows()!=1)
                        {
                            # insert failed try update
                            $update_fields = "";
                            $update_fields .= "FirstName = '$first',";
                            $update_fields .= "LastName = '$last',";
                            $update_fields .= "ChineseName = '$chiname',";
                            $update_fields .= "EnglishName = '$engname',";
                            $update_fields .= "Title = '$title',";
                            $update_fields .= "Gender = '$gender',";
                            $update_fields .= "MobileTelNo = '$mobile',";
                            $update_fields .= "RecordStatus = '9',";
                            $update_fields .= "DateModified = now()";
                            # update email if not empty
                            if ($useremail != "")
                            {
                                $update_fields .= ",UserEmail = '$useremail'";
                            }
                            if ($password !="")
                            {
                                $update_fields .= ",UserPassword = '$target_password'";
                            }
                            if ($yearofleft != "")
                            {
                                $update_fields .= ",YearOfLeft = '$yearofleft'";
                            }

                            $conds = "UserLogin = '".$login."'";

                            $sql = "UPDATE INTRANET_USER SET $update_fields WHERE $conds";
                            $li->db_db_query($sql);
                        }
                     }
                }
                # Put user into alumni Group
                if ($alumni_GroupID != "")
                {
                    $sql = "INSERT IGNORE INTO INTRANET_USERGROUP (GroupID, UserID)
                            SELECT '$alumni_GroupID', UserID FROM INTRANET_USER WHERE RecordType = 4 AND RecordStatus = 9";
                    $li->db_db_query($sql);
                }

                # Update status
                $sql  = "UPDATE INTRANET_USER SET RecordStatus = 1 WHERE RecordType = 4 AND RecordStatus = 9";
                $li->db_db_query($sql);

                # Update Group role
                $li->UpdateRole_UserGroup();

                $page = "index.php?msg=10&";
                $error_msg = $li->getErrorMsg();
                if ($error_msg=="")
                {
                    header("Location: ".$page."TabID=$TabID&filter=1&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&format=$format");
                }
                else
                {
                    include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
                    echo "$i_import_error <br>\n $error_msg\n";
                    echo  "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";

                    //echo $li->queries;
                    include_once($PATH_WRT_ROOT."templates/adminfooter.php");
                }


            }
        }
        else
        {
            include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
                        echo displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.go(-2)', $button_import, 'javascript:history.back()');
            echo "<blockquote><br>$i_import_invalid_format <br>\n";
            echo "<table width=100 border=1>\n";
            for ($i=0; $i<sizeof($file_format); $i++)
            {
                 echo "<tr><td>".$file_format[$i]."</td></tr>\n";
            }
            echo "</table></blockquote>\n";
                        echo "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                        <tr><td><hr size=1></td></tr>
                                        <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";


            //echo $li->queries;
            include_once($PATH_WRT_ROOT."templates/adminfooter.php");
        }


}

intranet_closedb();

//header("Location: ".$page."TabID=$TabID&filter=1&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&format=$format");
?>
