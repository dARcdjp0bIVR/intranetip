<?php
# using: henry

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
$keyword = addslashes(trim($keyword));

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field == "") $field = 5;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 5; break;
}
if($filter == "") $filter = 1;
switch ($filter){
        case 0: $filter = 0; break;
        case 1: $filter = 1; break;
        case 2: $filter = 2; break;
        case 3: $filter = 3; break;
        default: $filter = 1; break;
}
$conds = ($TabID != ""? "AND a.RecordType = $TabID":"");

$sql  = "SELECT
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.UserLogin, '</a>'),
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.EnglishName, '</a>'),
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.ChineseName, '</a>'),
                        (".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN').") as ClassName,
                        if(ycu.ClassNumber ='0','', ycu.ClassNumber) As ClassCount, 
                        a.LastUsed,
                        CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>')
                FROM
                        INTRANET_USER AS a LEFT OUTER JOIN
						YEAR_CLASS_USER ycu ON (ycu.UserID=a.UserID) LEFT OUTER JOIN
						YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=".Get_Current_Academic_Year_ID().")
                WHERE
                        (a.UserLogin like '%$keyword%' OR
                                a.UserEmail like '%$keyword%' OR
                                a.EnglishName like '%$keyword%' OR
                                a.ChineseName like '%$keyword%' OR
                                a.ClassName like '%$keyword%' OR
                                a.WebSamsRegNo like '%$keyword%'
                                ) AND
                        a.RecordStatus = $filter 
                        $conds
				GROUP BY a.UserID
                ";

$sql  = "SELECT
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.UserLogin, '</a>'),
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.EnglishName, '</a>'),
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.ChineseName, '</a>'),
                        a.ClassName,
                        if(a.ClassNumber ='0','', a.ClassNumber) As ClassCount, 
                        a.LastUsed,
                        CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>')
                FROM
                        INTRANET_USER AS a
                WHERE
                        (a.UserLogin like '%$keyword%' OR
                                a.UserEmail like '%$keyword%' OR
                                a.EnglishName like '%$keyword%' OR
                                a.ChineseName like '%$keyword%' OR
                                a.ClassName like '%$keyword%' OR
                                a.WebSamsRegNo like '%$keyword%'
                                ) AND
                        a.RecordStatus = $filter 
                        $conds
				GROUP BY a.UserID
                ";

if ($TabID == 3)
{
	if ($parentLink!="")
	{
		$conds .= " AND b.ParentID IS ".($parentLink==1? "NOT NULL": "NULL");
	}
    $sql  = "SELECT
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.UserLogin, '</a>'),
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.EnglishName, '</a>'),
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '>', a.ChineseName, '</a>'),
                        a.ClassName,
                        CONCAT('<a class=tableContentLink href=edit.php?TabID=$TabID&order=$order&field=$field&pageNo=$pageNo&UserID[]=', a.UserID, '#childrenlink >', count(b.StudentID), '</a>'),
                        a.LastUsed,
                        CONCAT('<input type=checkbox name=UserID[] value=', a.UserID ,'>'),
						count(b.StudentID) As ClassCount
                FROM
                        INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION as b
                                      ON a.UserID = b.ParentID
                WHERE
                        (a.UserLogin like '%$keyword%' OR
                                a.UserEmail like '%$keyword%' OR
                                a.EnglishName like '%$keyword%' OR
                                a.ChineseName like '%$keyword%' OR
                                a.ClassName like '%$keyword%' OR
                                a.WebSamsRegNo like '%$keyword%'
                                ) AND
                        a.RecordStatus = $filter
                        $conds
                GROUP BY a.UserID
                ";
}

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.UserLogin", "a.EnglishName", "a.ChineseName", "a.ClassName","ClassCount", "a.LastUsed");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_user;
$li->column_array = array(0,0,0,0,0);
$li->IsColOff = 2;

# TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=16% class=tableTitle>".$li->column(0, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=22% class=tableTitle>".$li->column(1, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width=22% class=tableTitle>".$li->column(2, $i_UserChineseName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(3, $i_UserClassName)."</td>\n";
if ($TabID == 3)
{
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column(4, $i_UserParentLink)."</td>\n";
} else
{
	$li->column_list .= "<td width=10% class=tableTitle>".$li->column(4, $i_UserClassNumber)."</td>\n";
}
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_UserLastUsed)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("UserID[]")."</td>\n";



# student license
if (isset($account_license_student) && $account_license_student>0)
{
	$license_student_used = $li->getLicenseUsed();
	$license_student_left = $account_license_student - $license_student_used;
	$student_license_text = str_replace("<!--license_total-->", $account_license_student, $i_license_sys_msg);
	$student_license_text = str_replace("<!--license_used-->", $license_student_used, $student_license_text);
	$student_license_text = str_replace("<!--license_left-->", $license_student_left, $student_license_text);
	$student_license_text .= "<br />";
}
if ($plugin['iPortfolio'])
{
	$student_license_text .= $i_UserWarningiPortfolio;
}
//$i_license_sys_msg;

// TABLE FUNCTION BAR
$usertype = array(
array(1,$i_identity_teachstaff),
array(2,$i_identity_student),
array(3,$i_identity_parent)
);
if ($special_feature['alumni'])
{
    $usertype[] = array(4,$i_identity_alumni);
}
$selection = "$button_select $i_identity:".getSelectByArray($usertype,"name=TabID onChange=this.form.submit()",$TabID,1);
/*
$toolbar  = "<a class=iconLink href=javascript:checkGet(document.form1,'new.php')>".newIcon()."$button_new</a>\n".toolBarSpacer();
$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'import.php')>".importIcon()."$button_import</a>\n".toolBarSpacer();
*/
$toolbar  = "<a class=iconLink href=javascript:displayUiAlert()>".newIcon()."$button_new</a>\n".toolBarSpacer();
$toolbar .= "<a class=iconLink href=javascript:displayUiAlert()>".importIcon()."$button_import</a>\n".toolBarSpacer();

$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'export.php')>".exportIcon()."$button_export</a>\n".toolBarSpacer();
$toolbar .= "<a class=iconLink href=javascript:checkGet(document.form1,'email.php')>".emailIcon()."$button_email</a>\n";
if ($intranet_authentication_method!="HASH")
{
	$toolbar2 = "<a class=iconLink href=javascript:confirmSendPassword(document.form1,'email_all.php')><img src=$image_path/admin/icon_send_password.gif border=0 hspace=1 vspace=0 align=absmiddle>$button_emailpassword</a>\n";
}
#$toolbar2 .= "<a class=iconLink href=javascript:checkGet(document.form1,'importgrad.php')>".importIcon()."$i_UserImportGraduate</a>\n".toolBarSpacer();

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'UserID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
if ($TabID==2 && $filter==3)
{
}
else
{
    $functionbar .= ($filter==1) ? "<a href=\"javascript:checkSuspend(document.form1,'UserID[]','suspend.php')\"><img src='/images/admin/button/t_btn_pause_$intranet_session_language.gif' border='0' align='absmiddle'></a>" : "<a href=\"javascript:checkApprove(document.form1,'UserID[]','approve.php')\"><img src='/images/admin/button/t_btn_permit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
}
$functionbar .= ($TabID==2 && $filter==3)? "<a href=\"javascript:checkEdit(document.form1,'UserID[]','special_promote.php')\"><img src='/images/admin/button/t_btn_special_promote_$intranet_session_language.gif' border='0' align='absmiddle' alt='$i_StudentPromotion_SpecialPromote'></a>" : "";
$functionbar .= ($TabID==2 && $filter!=3)? "<a href=\"javascript:checkEdit(document.form1,'UserID[]','markleft.php')\"><img src='/images/admin/button/t_btn_markleft_$intranet_session_language.gif' border='0' align='absmiddle' alt='$i_StudentPromotion_SetToLeft'></a>" : "";
/*
if ($intranet_authentication_method=='LDAP')
{
    include_once("../../includes/libldap.php");
    $lldap = new libladp();
    if ($lldap->isAccountControlNeeded())
    {
        $confirmedNeeded = true;
    }
}
*/
if ($confirmedNeeded)
{
    $remove_all_page = 'remove_all_confirm.php';
    $remove_page = 'remove_confirm.php';
}
else
{
    $remove_all_page = 'remove_all.php';
    $remove_page = 'remove.php';
}

#$functionbar .= "<a href=\"javascript:checkRemoveAll(document.form1,'$remove_all_page')\"><img src='/images/admin/button/t_btn_delete_all_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'UserID[]','$remove_page')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar  = "<select name=filter onChange=\"this.form.pageNo.value=1;this.form.submit();\">\n";
$searchbar .= "<option value=0 ".(($filter==0)?"selected":"").">$i_status_suspended</option>\n";
$searchbar .= "<option value=1 ".(($filter==1)?"selected":"").">$i_status_approved</option>\n";
$searchbar .= "<option value=2 ".(($filter==2)?"selected":"").">$i_status_pendinguser</option>\n";
if ($TabID == 2)
    $searchbar .= "<option value=3 ".(($filter==3)?"selected":"").">$i_status_graduate</option>\n";
$searchbar .= "</select>\n";
$searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes(stripslashes($keyword))."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
if ($TabID == 3)
{
    $select_parent_type = "<SELECT name=parentLink onChange=this.form.submit()>";
    $select_parent_type .= "<OPTION value='' ".($parentLink==""? "SELECTED":"").">$i_status_all</OPTION>\n";
    $select_parent_type .= "<OPTION value='1' ".($parentLink=="1"? "SELECTED":"").">$i_UserParentWithLink</OPTION>\n";
    $select_parent_type .= "<OPTION value='2' ".($parentLink=="2"? "SELECTED":"").">$i_UserParentWithNoLink</OPTION>\n";
    $select_parent_type .= "</SELECT>\n";
}
?>
<SCRIPT>
function confirmSendPassword(obj, url)
{
         if (confirm(globalAlertMsgSendPassword))
             checkGet(obj, url);
}

function displayUiAlert() {
	alert("<?=$Lang['AdminConsole']['jsMovedToFrontEndUiAlertMsg']?>");
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td align=left><?= $selection ?></td><td align=right><?=$select_parent_type?></td></tr>
<tr><td colspan=2><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<? if ($student_license_text!="") { ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?=$student_license_text?></td></tr>
</table><? } ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar2, $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br><br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
