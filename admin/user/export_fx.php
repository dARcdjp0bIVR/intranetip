<?php
// editing by 
ini_set("memory_limit","300M");
set_time_limit(24*60*60);
$PATH_WRT_ROOT = "../../";
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbdump.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

if ($TabID == "")
{
    $tab_conds = "";
}
else
{
    $tab_conds = "RecordType = $TabID AND ";
}
$conds = "WHERE $tab_conds RecordStatus IN (0,1,2)";
$sql = "SELECT UserLogin,ClassName,ClassNumber,'',CardID,UserEmail,EnglishName,'','',ChineseName FROM
               INTRANET_USER
               $conds ORDER BY ClassName, ClassNumber, EnglishName
               ";
$col_count = 10;
$li = new libdb();
$row = $li->returnArray($sql,$col_count);
$x = "\"UserLogin\",\"ClassName\",\"ClassNumber\",\"Password\",\"CardID\",\"UserEmail\",\"EnglishName\",\"FirstName\",\"LastName\",\"ChineseName\"\r\n";

$row_size = sizeof($row);
$column_size = sizeof($row[0]);
for($i=0; $i<$row_size; $i++)
{
    $delim = "";

    for($j=0; $j<$column_size; $j++)
    {
		$tmp_data = iconv("UTF-8", "Big5//IGNORE", $row[$i][$j]);
        $row[$i][$j] = "\"".intranet_undo_htmlspecialchars($tmp_data)."\"";
        $x .= $delim.$row[$i][$j];
        $delim = ",";
    }
    $x .="\r\n";
}

$export_content = $x;

// Output the file to user browser
$filename = "eclass-user-fx.csv";
output2browser($export_content, $filename);

intranet_closedb();
?>