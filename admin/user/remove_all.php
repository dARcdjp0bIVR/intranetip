<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libwebmail.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libform.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");

intranet_opendb();

$lu = new libuser();
if ($parentLink=="")
{
    $sql  = "SELECT
               a.UserID
                FROM
                        INTRANET_USER AS a
                WHERE
                        (a.UserLogin like '%$keyword%' OR
                                a.UserEmail like '%$keyword%' OR
                                a.EnglishName like '%$keyword%' OR
                                a.ChineseName like '%$keyword%' OR
                                a.ClassName like '%$keyword%'
                                ) AND
                        a.RecordStatus = $filter";
    if ($TabID >= 1 && $TabID <= 4)
        $sql .= " AND a.RecordType = $TabID ";
}
else
{
    $conds = "a.RecordType = 3 AND b.ParentID IS ".($parentLink==1? "NOT NULL": "NULL");
    $sql  = "SELECT
                   a.UserID
                FROM
                        INTRANET_USER AS a LEFT OUTER JOIN INTRANET_PARENTRELATION as b
                                      ON a.UserID = b.ParentID
                WHERE
                        (a.UserLogin like '%$keyword%' OR
                                a.UserEmail like '%$keyword%' OR
                                a.EnglishName like '%$keyword%' OR
                                a.ChineseName like '%$keyword%' OR
                                a.ClassName like '%$keyword%'
                                ) AND
                        a.RecordStatus = $filter
                        $conds
                GROUP BY a.UserID
                ";
}

$result = $lu->returnVector($sql);
if (sizeof($result)==0)     # No users
{
    header("Location: index.php");
    exit();
}
$list = implode(",",$result);


$lu->prepareUserRemoval($list);
$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
$students = $lu->returnVector($sql);

if (sizeof($students)!=0)
{
    $lsp = new libstudentpromotion();
    $studentList = implode(",",$students);
    $lsp->addClassHistory($studentList);
    $lsp->archiveStudentsInfo($studentList);
    if ($special_feature['alumni'] && $alumni_system_type==1)
    {
        $lu->setToAlumni($studentList);
    }
    else
    {
        $lu->removeUsers($studentList);
    }
}

$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType != 2 AND UserID IN ($list)";
$others = $lu->returnVector($sql);
if (sizeof($others)!=0)
{
    $otherList = implode(",",$others);
    $lu->removeUsers($otherList);
}


intranet_closedb();
header("Location: index.php?TabID=$TabID&filter=$filter&order=$order&field=$field&msg=3");
?>