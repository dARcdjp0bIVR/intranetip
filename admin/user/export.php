<?php
# using: YAT

######### Change Log [Start] ###########
#
#	Date:	2010-04-19 YatWoon
#			- for "LaiShan" project, add parent/guardian name to student export csv (with flag checking: $sys_custom['LaiShanStudentExport'])
#			- display checkbox instead of multiple selection (with flag checking: $sys_custom['UserExpoertWithCheckbox'])
#
######### Change Log [End] ###########

include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");


if ($sys_custom['yyc_pda_question'])
{
	$ExportOffline  = "<tr>
								<td align=\"left\">".$ec_yyc['yyc_export_usage']." (school_info)
								<a href=\"javascript:jEXPORT(1)\" >
								<img src=\"/images/admin/button/s_btn_export_".$intranet_session_language.".gif\" border=\"0\" align=\"absmiddle\" >
								</a></td>
								
								<td align=\"left\">".$ec_yyc['yyc_export_usage']." (account_info)
								<a href=\"javascript:jEXPORT(2)\" >
								<img src=\"/images/admin/button/s_btn_export_".$intranet_session_language.".gif\" border=\"0\" align=\"absmiddle\" >
								</a>
								</td>								
							</tr>
							<tr>
								<td height=\"22\" style=\"vertical-align:bottom\" colspan=\"2\" ><hr size=\"1\" /></td>
							</tr>
							";
}
?>

<script language="javascript">
function checkform(obj){ 
	<? if(!$sys_custom['UserExpoertWithCheckbox']) {?>
     if(countOption(obj.elements["Fields[]"])==0){ alert(globalAlertMsg18); return false; }
     <? } else { ?>
     if(countChecked(obj, "Fields[]")==0){ alert(globalAlertMsg18); return false; }
     <? } ?>
}

function jEXPORT(jNum)
{
	if (jNum == 2)
	{
		document.form4.submit();
	}
	else 
	{
		document.form3.submit();
	}
}

function click_print()
{
	var obj=document.form1;
	var print_ok = 1;
	<? if(!$sys_custom['UserExpoertWithCheckbox']) {?>
     if(countOption(obj.elements["Fields[]"])==0){ alert(globalAlertMsg18); print_ok=0; }
     <? } else { ?>
     if(countChecked(obj, "Fields[]")==0){ alert(globalAlertMsg18); print_ok=0; }
     <? } ?>
     
     if(print_ok==1)
     {
		document.form1.action='print.php';
		document.form1.target='blank';
		document.form1.submit();
		 	
		document.form1.action='export_update.php';
		document.form1.target='_self';
	}
}

</script>

<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $button_export, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>
<form name="form3" action="export_update_yyc.php" method="post">
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<?=$ExportOffline?>
</table>
</form>
<form name="form4" action="export_update_yyc_account.php" method="post">
</form>

<form name="form2" action="export_update.php" method="post">
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
<?php if(trim($TabID)!=""){ ?>
	<td align="right"><?=$i_UserDefaultExport?>
	<input type="image" src="/images/admin/button/s_btn_export_<?=$intranet_session_language?>.gif" border="0">
	</td>
<?php 
	}else{  # All
		echo "<td align=center>$i_UserDefaultExportWarning</td>";
	}
?>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
</table>
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=keyword value="<?php echo $keyword; ?>">
<input type=hidden name=filter value="<?php echo $filter; ?>">
<input type=hidden name=TabID value="<?php echo $TabID; ?>">
<input type=hidden name=parentLink  value="<?php echo $parentLink;?>">

<input type=hidden name=default value=1>
</form>

<form name="form1" action="export_update.php" method="post" onSubmit="return checkform(this);">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><?php echo $i_export_msg2; ?>:<br>
<? if($sys_custom['UserExpoertWithCheckbox']) {?>
	<input type="checkbox" name="Fields[]" value="UserLogin" id="UserLogin" checked> <label for="UserLogin"><?php echo $i_UserLogin; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="UserEmail" id="UserEmail" checked> <label for="UserEmail"><?php echo $i_UserEmail; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="EnglishName" id="EnglishName" checked> <label for="EnglishName"><?php echo $i_UserEnglishName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="FirstName" id="FirstName"> <label for="FirstName"><?php echo $i_UserFirstName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="LastName" id="LastName"> <label for="LastName"><?php echo $i_UserLastName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="ChineseName" id="ChineseName" checked> <label for="ChineseName"><?php echo $i_UserChineseName; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="IF(Title IS NULL OR TRIM(Title) = '', '', CASE Title WHEN 0 THEN 'Mr.' WHEN 1 THEN 'Miss' WHEN 2 THEN 'Mrs.' WHEN 3 THEN 'Ms.' WHEN 4 THEN 'Dr.' WHEN 5 THEN 'Prof.' ELSE '' END) AS Title " id="Title"> <label for="Title"><?php echo $i_UserTitle; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="Gender" id="Gender"> <label for="Gender"><?php echo $i_UserGender; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="MobileTelNo" id="MobileTelNo"> <label for="MobileTelNo"><?php echo $i_UserMobileTelNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="ClassNumber" id="ClassNumber"> <label for="ClassNumber"><?php echo $i_UserClassNumber; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="IF(DateOfBirth IS NULL OR date_format(DateOfBirth,'%Y-%m-%d') = '0000-00-00', '', DateOfBirth) AS DateOfBirth " id="DateOfBirth"> <label for="DateOfBirth"><?php echo $i_UserDateOfBirth; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="HomeTelNo" id="HomeTelNo"> <label for="HomeTelNo"><?php echo $i_UserHomeTelNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="OfficeTelNo" id="OfficeTelNo"> <label for="OfficeTelNo"><?php echo $i_UserOfficeTelNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="FaxNo" id="FaxNo"> <label for="FaxNo"><?php echo $i_UserFaxNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="ICQNo" id="ICQNo"> <label for="ICQNo"><?php echo $i_UserICQNo; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="Address" id="Address"> <label for="Address"><?php echo $i_UserAddress; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="Country" id="Country"> <label for="Country"><?php echo $i_UserCountry; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="CardID" id="CardID"> <label for="CardID"><?php echo $i_SmartCard_CardID; ?></label> <br>
	<input type="checkbox" name="Fields[]" value="WebSamsRegNo" id="WebSamsRegNo"> <label for="WebSamsRegNo"><?php echo $i_WebSAMS_Registration_No; ?></label> <br>
	<? if($special_feature['ava_hkid']) {?>
		<input type="checkbox" name="Fields[]" value="HKID" id="HKID"> <label for="HKID"><?php echo $i_HKID; ?></label> <br>
	<? } ?>
	<? if($special_feature['ava_strn']) {?>
		<input type="checkbox" name="Fields[]" value="STRN" id="STRN"> <label for="STRN"><?php echo $i_STRN; ?></label> <br>
	<? } ?>
	
	<? if($sys_custom['LaiShanStudentExport'] && $TabID==2) {?>
		<input type="checkbox" name="Fields[]" value="Parent" id="Parent"> <label for="Parent"><?php echo $Lang['Identity']['Parent']; ?></label> <br>
		<input type="checkbox" name="Fields[]" value="Guardian" id="Guardian"> <label for="Guardian"><?php echo $Lang['Identity']['Guardian']; ?></label> <br>
	<? } ?>
<? } else {?>
	<select name=Fields[] size=10 multiple>
	<option value=UserLogin SELECTED><?php echo $i_UserLogin; ?></option>
	<option value=UserEmail SELECTED><?php echo $i_UserEmail; ?></option>
	<option value=EnglishName SELECTED><?php echo $i_UserEnglishName; ?></option>
	<option value=FirstName ><?php echo $i_UserFirstName; ?></option>
	<option value=LastName ><?php echo $i_UserLastName; ?></option>
	<option value=ChineseName SELECTED><?php echo $i_UserChineseName; ?></option>
	<option value="IF(Title IS NULL OR TRIM(Title) = '', '', CASE Title WHEN 0 THEN 'Mr.' WHEN 1 THEN 'Miss' WHEN 2 THEN 'Mrs.' WHEN 3 THEN 'Ms.' WHEN 4 THEN 'Dr.' WHEN 5 THEN 'Prof.' ELSE '' END) AS Title "><?php echo $i_UserTitle; ?></option>
	<option value=Gender><?php echo $i_UserGender; ?></option>
	<option value=MobileTelNo><?php echo $i_UserMobileTelNo; ?></option>
	<option value=ClassNumber><?php echo $i_UserClassNumber; ?></option>
	<option value=ClassName><?php echo $i_UserClassName; ?></option>
	<option value="IF(DateOfBirth IS NULL OR date_format(DateOfBirth,'%Y-%m-%d') = '0000-00-00', '', DateOfBirth) AS DateOfBirth "><?php echo $i_UserDateOfBirth; ?></option>
	<option value=HomeTelNo><?php echo $i_UserHomeTelNo; ?></option>
	<option value=OfficeTelNo><?php echo $i_UserOfficeTelNo; ?></option>
	<option value=FaxNo><?php echo $i_UserFaxNo; ?></option>
	<option value=ICQNo><?php echo $i_UserICQNo; ?></option>
	<option value=Address><?php echo $i_UserAddress; ?></option>
	<option value=Country><?php echo $i_UserCountry; ?></option>
	<option value=CardID><?php echo $i_SmartCard_CardID; ?></option>
	<option value=WebSamsRegNo><?php echo $i_WebSAMS_Registration_No; ?></option>
	<? if($special_feature['ava_hkid']) {?>
	<option value=HKID><?php echo $i_HKID; ?></option>
	<? } ?>
	<? if($special_feature['ava_strn']) {?>
	<option value='STRN'><?php echo $i_STRN; ?></option>
	<? } ?>
	
	<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option>
	</select>
<? } ?>
</blockquote>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<? if($sys_custom['UserExportWithPrint']) {?>
<a href="javascript:click_print();" border="0"><img src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0"></a> 
<? } ?>
<input type="image" src="/images/admin/button/s_btn_export_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
<a href="javascript:history.back()"> <img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=keyword value="<?php echo $keyword; ?>">
<input type=hidden name=filter value="<?php echo $filter; ?>">
<input type=hidden name=TabID value="<?php echo $TabID; ?>">
<input type=hidden name=parentLink  value="<?php echo $parentLink;?>">
</form>

<?php
include("../../templates/adminfooter.php");
?>