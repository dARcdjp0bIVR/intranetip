<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgrouping.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();
?>
<form name="form1" action="remove.php" method="post">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $button_remove, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>
<blockquote>
<?=$i_UserRemoveConfirm?>
<blockquote>
<?
if ($plugin['personalfile'] && ($personalfile_type == 'FTP' || $personalfile_type == 'LOCAL_FTP' || $personalfile_type=='REMOTE_FTP'))
{
    include_once("../../includes/libftp.php");
    $lftp = new libftp();
    if ($lftp->isAccountManageable && $lftp->isLocal)
    {
        $withLinuxAccount = true;
    }
}
if (!$withLinuxAccount && $plugin['webmail'])
{
     include_once("../../includes/libwebmail.php");
     $lwebmail = new libwebmail();
     if ($lwebmail->isAccountManageable())
     {
         $withLinuxAccount = true;
     }
}
if ($withLinuxAccount)
{
    echo "$i_UserLinuxAccount <br>\n";
}

if ($intranet_authentication_method=='LDAP')
{
    include_once("../../includes/libldap.php");
    $lldap = new libladp();
    if ($lldap->isAccountControlNeeded())
    {
        echo "$i_UserLDAPAccount <br>\n";
    }
}

?>
<?
?>
</blockquote>
</blockquote>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border='0'>
 <a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<?
for ($i=0; $i<sizeof($UserID); $i++)
{
     echo "<input type=hidden name=UserID[] value=".$UserID[$i].">\n";
}
?>
<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=TabID value="<?=$TabID?>">
<input type=hidden name=keyword value="<?=$keyword?>">
</form>

<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>