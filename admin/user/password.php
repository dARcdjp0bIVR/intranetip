<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libuser.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/fileheader_admin.php");
intranet_opendb();

$li = new libuser($uid);
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (!check_text(obj.password1,"<?="$i_alert_pleasefillin $i_frontpage_myinfo_password_new"?>")) return false;
         if (!check_text(obj.password2,"<?="$i_alert_pleasefillin $i_frontpage_myinfo_password_retype"?>")) return false;
         if (obj.password1.value != obj.password2.value)
         {
             alert ("<?=$i_frontpage_myinfo_password_mismatch?>");
             obj.password1.value = '';
             obj.password2.value = '';
             return false;
         }
         return true;
}
</SCRIPT>

<form name="form1" action="password_update.php" method="post" ONSUBMIT="return checkform(this)">
<?= displayNavTitle($i_UserResetPassword, '') ?>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../images/admin/pop_bg.gif);" >

<blockquote><blockquote>
<br>
<p><?=$i_UserLogin?> : <?=$li->UserLogin?></p>
<p><?=$i_frontpage_myinfo_password_new?> : <input type=password length=20 MAXLENGTH=20 name=password1>
<p><?=$i_frontpage_myinfo_password_retype?> : <input type=password length=20 MAXLENGTH=20 name=password2>
<p><input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border="0">
</blockquote></blockquote>

</td></tr>
<tr><td><img src="../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>

<input type=hidden name=uid value="<?php echo $li->UserID; ?>">
</form>

<?php
intranet_closedb();
include("../../templates/filefooter.php");
?>