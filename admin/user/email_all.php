<?php
# using: 

########## Change Log [Start] ############
#
#	Date:	2010-04-09 YatWoon
#			Email content missing to assign the $Title, bug: display 0, 1, 2, 3, 4, 5 in the email content, it should be Ms, Mr ....
#
########## Change Log [End] ############

@SET_TIME_LIMIT(600);
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../lang/email.php");
intranet_opendb();

$li = new libdb();
// Original, send to users who have not login yet
//$sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, a.UserEmail, a.FirstName, a.LastName FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b WHERE a.LastUsed Is Null AND a.RecordStatus = 1 AND a.UserID = b.UserID AND b.GroupID = $TabID";
if ($TabID != "")
{
    $conds = " AND RecordType = '$TabID'";
}
#$sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, a.UserEmail, a.EnglishName, a.Title FROM INTRANET_USER AS a, INTRANET_USERGROUP AS b WHERE a.RecordStatus = 1 AND a.UserID = b.UserID AND b.GroupID = $TabID";
$sql = "SELECT UserID, UserLogin, UserPassword, UserEmail, EnglishName, Title FROM INTRANET_USER
        WHERE
                        (UserLogin like '%$keyword%' OR
                                UserEmail like '%$keyword%' OR
                                EnglishName like '%$keyword%' OR
                                ChineseName like '%$keyword%' OR
                                ClassName like '%$keyword%'
                                ) AND
                        RecordStatus = $filter
                        $conds
";
$row = $li->returnArray($sql, 6);
for($i=0; $i<sizeof($row); $i++){
     $UserID = $row[$i][0];
     $UserLogin = $row[$i][1];
     $UserPassword = $row[$i][2];
     $UserEmail = $row[$i][3];
     $EnglishName = $row[$i][4];
     $title = $row[$i][5];
     
     switch ($title)
     {
	     case 0:	$title_name = $i_title_mr;		break;
	     case 1:	$title_name = $i_title_miss;	break;
	     case 2:	$title_name = $i_title_mrs;		break;
	     case 3:	$title_name = $i_title_ms;		break;
	     case 4:	$title_name = $i_title_dr;		break;
	     case 5:	$title_name = $i_title_prof;	break;
	     default:	$title_name = ""; 				break;
     }
     
     # Call sendmail function
     $mailSubject = registration_title();
     $mailBody = registration_body($UserEmail, $UserLogin, $UserPassword, $title_name,$EnglishName);
     $mailTo = $UserEmail;
     $lu = new libsendmail();
     $headers = "From: $webmaster\r\n";
     $lu->SendMail( $mailTo, $mailSubject, $mailBody,"$headers");
}

intranet_closedb();
header("Location: index.php?TabID=$TabID&filter=$filter&keyword=$keyword&pageNo=$pageNo&order=$order&field=$field&msg=4");
?>