<?php
# Using By : yat
##-------------------------------- Important --------------------------------##
##
##	Before marking any changes on this page, 
##	please mark sure your text editor is in / support utf-8 format. 
##	Thanks.
##
##---------------------------------------------------------------------------##

############### Change Log 
# - 2010-08-27 YatWoon
#	hide Title
##############################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
intranet_opendb();


if ($special_feature['alumni'])
{
    $i_identity_array[4] = $i_identity_alumni;
}

if ($TabID == "")
{
?>
<form name="form1" action="" method="get">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo "$button_select $i_identity"; ?>:</td><td>
<SELECT name=TabID onChange=this.form.submit()>
<OPTION><?=" -- $button_select -- "?></OPTION>
<OPTION value=1><?=$i_identity_teachstaff?></OPTION>
<OPTION value=2><?=$i_identity_student?></OPTION>
<OPTION value=3><?=$i_identity_parent?></OPTION>
<?
if ($special_feature['alumni'])
{
?>
<OPTION value=4><?=$i_identity_alumni?></OPTION>
<?
}
?>
</SELECT>
</td></tr>
</table>
</form>

<?

}
else
{

$lo = new libgrouping();
/*
$lg = new libgroup();
$selection = $lg->getSelectClass("onChange=\"this.form.ClassName.value=this.value\"");
$selection2 = $lg->getSelectLevel("onChange=\"this.form.ClassLevel.value=this.value\"");
*/
$lclass = new libclass();
$selection = $lclass->getSelectClass("name=ClassName");
#$selection2 = $lclass->getSelectLevel("name=ClassLevel");

if ($TabID==2)
{
	$license_student_left = 9999;
	if (isset($account_license_student) && $account_license_student>0)
	{
		$license_student_used = $lclass->getLicenseUsed();
		$license_student_left = $account_license_student - $license_student_used;
	}
	
	if ($license_student_left<=0)
	{
		# no quota left
		echo "<table width=560  border=0 cellpadding=0 cellspacing=0 align='center'>
		<tr><td align='center'><br /><br />
		{$i_license_sys_msg_none}
		</td></tr>
		</table>";
		die();
	}
}

if ($TabID == 1)
{
    $teachingStr = "<tr><td align=right nowrap></td><td><input type=radio name=teaching value=1 checked>$i_teachingStaff <input type=radio name=teaching value=0>$i_nonteachingStaff \n<br><span class=extraInfo>$i_teachingDifference</span></td>\n";
}
else
{
    $teachingStr = "";
}

/*
<tr><td align=right nowrap><?php echo $i_UserClassLevel; ?>:</td><td><?=$selection2?>(<?=$i_ifapplicable?>)</td></tr>
*/

if ($TabID==1)
{
    if ($intranet_session_language!="gb")
    {
        $chi_title_array = array("校長","副校","主任");
    }
    else
    {
        $chi_title_array = array();
    }
    $sql = "SELECT DISTINCT TitleChinese FROM INTRANET_USER WHERE TitleChinese IS NOT NULL AND TitleChinese !='' ORDER BY TitleChinese";
    $temp = $lclass->returnVector($sql);
    if (sizeof($temp)!=0)
    {
        $chi_title_array = array_merge($chi_title_array, $temp);
        $chi_title_array = array_unique($chi_title_array);
        $chi_title_array = array_values($chi_title_array);
    }
    $sql = "SELECT DISTINCT TitleEnglish FROM INTRANET_USER WHERE TitleEnglish IS NOT NULL AND TitleEnglish !='' ORDER BY TitleEnglish";
    $eng_title_array = $lclass->returnVector($sql);
    if (sizeof($eng_title_array)!=0)
    {
        $select_eng_title = getSelectByValue($eng_title_array, "onChange=this.form.TitleEnglish.value=this.value");
    }
    else
    {
        $select_eng_title = "";
    }
    $select_chi_title = getSelectByValue($chi_title_array, "onChange=this.form.TitleChinese.value=this.value");
}

?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.UserLogin, "<?php echo $i_alert_pleasefillin.$i_UserLogin; ?>.")) return false;
     if(!check_text(obj.Password, "<?php echo $i_alert_pleasefillin.$i_UserPassword; ?>.")) return false;
     if (obj.Password.value != obj.RePassword.value)
     {
         alert("<?=$i_frontpage_myinfo_password_mismatch?>");
         return false;
     }
     if (obj.UserEmail.value != "")
     {
         if(!validateEmail(obj.UserEmail, "<?php echo $i_invalid_email; ?>.")) return false;
     }
     if(obj.ChineseName.value == "")
     {
        if(!check_text(obj.EnglishName, "<?php echo $i_alert_pleasefillin.$i_UserEnglishName; ?>.")) return false;
     }

     <? if ($TabID != 4) { ?>
     checkOptionAll(obj.elements["GroupID[]"]);
     <? } ?>

     <? if ($TabID == 3) { ?>
     checkOptionAll(obj.elements["child[]"]);
     <? } ?>

}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserLogin; ?>:</td><td><input class=text type=text name=UserLogin size=20 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserPassword; ?>:</td><td><input class=text type=password name=Password size=20 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_frontpage_myinfo_password_retype; ?>:</td><td><input class=text type=password name=RePassword size=20 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserEmail; ?>:</td><td><input class=text type=text name=UserEmail size=30 maxlength=50></td></tr>
<tr><td align=right nowrap><?php echo $i_UserEnglishName; ?>:</td><td><input class=text type=text name=EnglishName size=20 maxlength=100></td></tr>
<tr><td align=right nowrap><?php echo $i_UserChineseName; ?>:</td><td><input class=text type=text name=ChineseName size=20 maxlength=100></td></tr>
<tr><td align=right nowrap><?php echo $i_UserRecordStatus; ?>:</td><td><input type=radio name=RecordStatus value=1 CHECKED> <?php echo $i_status_approve; ?> <input type=radio name=RecordStatus value=2> <?php echo $i_status_pendinguser; ?></td></tr>
<tr><td align=right nowrap><?php echo $i_identity; ?>:</td><td><?=$i_identity_array[$TabID]?><input type=hidden name=identity value=<?=$TabID?>></td></tr>
<?=$teachingStr?>
<tr><td colspan=2><br></td></tr>
<tr><td align=right nowrap><?php echo $i_UserNickName; ?>:</td><td><input class=text type=text name=NickName size=20 maxlength=100></td></tr>
<?/*?><tr><td align=right nowrap><?php echo $i_UserTitle; ?>:</td><td><input type=radio name=Title value=0 CHECKED> <?php echo $i_title_mr; ?> <input type=radio name=Title value=1> <?php echo $i_title_miss; ?> <input type=radio name=Title value=2> <?php echo $i_title_mrs; ?> <input type=radio name=Title value=3> <?php echo $i_title_ms; ?> <input type=radio name=Title value=4> <?php echo $i_title_dr; ?> <input type=radio name=Title value=5> <?php echo $i_title_prof; ?> </td></tr><?*/?>
<? if ($TabID == 1) { ?>
<tr><td align=right nowrap><?php echo $i_UserDisplayTitle_Chinese; ?>:</td><td><input class=text type=text name=TitleChinese size=20 maxlength=100 ><?=$select_chi_title?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserDisplayTitle_English; ?>:</td><td><input class=text type=text name=TitleEnglish size=20 maxlength=100 ><?=$select_eng_title?></td></tr>
<? } ?>
<tr><td align=right nowrap><?php echo $i_UserGender; ?>:</td><td><input type=radio name=Gender value=M CHECKED> <?php echo $i_gender_male; ?> <input type=radio name=Gender value=F> <?php echo $i_gender_female; ?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserDateOfBirth; ?>:</td><td><input class=text type=text name=DateOfBirth size=10 maxlength=10> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right nowrap><?php echo $i_UserHomeTelNo; ?>:</td><td><input class=text type=text name=HomeTelNo size=15 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserOfficeTelNo; ?>:</td><td><input class=text type=text name=OfficeTelNo size=15 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserMobileTelNo; ?>:</td><td><input class=text type=text name=MobileTelNo size=15 maxlength=20><? if (isset($plugin['sms']) && $plugin['sms']){ echo $i_UserMobileSMSNotes; }?></td></tr>
<tr><td align=right nowrap><?php echo $i_UserFaxNo; ?>:</td><td><input class=text type=text name=FaxNo size=15 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserICQNo; ?>:</td><td><input class=text type=text name=ICQNo size=15 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserAddress; ?>:</td><td><input class=text type=text name=Address size=30 maxlength=255></td></tr>
<tr><td align=right nowrap><?php echo $i_UserCountry; ?>:</td><td><input class=text type=text name=Country size=20 maxlength=50></td></tr>
<tr><td align=right nowrap><?php echo $i_UserInfo; ?>:</td><td><textarea name=Info cols=30 rows=5></textarea></td></tr>
<tr><td align=right nowrap><?php echo $i_UserRemark; ?>:</td><td><textarea name=Remark cols=30 rows=5></textarea></td></tr>
<!-- Teacher Card ID -->
<?php
   if ($TabID == 1 && ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) ){
?>
	<tr><td align=right nowrap><?php echo $i_SmartCard_CardID; ?>:</td><td><input class=text type=text name=CardID size=20 maxlength=50></td></tr>
<?php } ?>
<? if ($TabID == 2 && ((isset($plugin['attendancestudent']) && $plugin['attendancestudent']) ||(isset($plugin['payment'])&& $plugin['payment']))) { ?>
<tr><td align=right nowrap><?php echo $i_SmartCard_CardID; ?>:</td><td><input class=text type=text name=CardID size=20 maxlength=50></td></tr>
<? } ?>
<? if($TabID == 2){ ?>
<tr><td align=right nowrap><?php echo $i_WebSAMS_Registration_No; ?>:</td><td><input class=text type=text name=WebSamsRegNo size=20 maxlength=20> <span class=extraInfo>(<?=$i_ifapplicable?>)</span></td></tr>
<? } ?>
<? if($special_feature['ava_hkid']) {?>
<tr><td align=right nowrap><?php echo $i_HKID; ?>:</td><td><input class=text type=text name=HKID size=10 maxlength=10></td></tr>
<? } ?>
<? if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4)) {?>
<tr><td align='right' nowrap><?php echo $i_STRN; ?>:</td><td><input class='text' type='text' name='STRN' size='10' maxlength='8'></td></tr>
<? } ?>

<tr><td colspan=2><br></td></tr>
<?
if ($TabID == 4) {
?>
<tr><td align=right nowrap><?php echo $i_Profile_DataLeftYear; ?>:</td><td><input class=text type=text name=YearOfLeft size=10 maxlength=20></td></tr>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><input class=text type=text name=ClassName size=10 maxlength=20></td></tr>
<? }
else
{
?>
<? /* IP25 - this setting should be set in School Settings  ?>
<tr><td align=right nowrap><?php echo $i_UserClassName; ?>:</td><td><?=$selection?></td></tr>
<? */ ?>
<? } ?>
<? /* IP25 - this setting should be set in School Settings  ?>
<tr><td align=right nowrap><?php echo $i_UserClassNumber; ?>:</td><td><input class=text type=text name=ClassNumber size=10 maxlength=20></td></tr>
<? */ ?>

<?php
if ($TabID != 4 && isset($plugin['webmail']) && $plugin['webmail'] && in_array($TabID, $webmail_identity_allowed))
{
    if ($webmail_SystemType == 2)
    {
         $mail_title = $i_Mail_LinkWebmail;
    }
    else if ($webmail_SystemType == 3)
    {
         $mail_title = $i_Mail_AllowSendReceiveExternalMail;
    }
    else $mail_title = $i_Mail_OpenWebmailAccount;
?>
<tr><td align=right nowrap></td><td><input type=checkbox name=open_webmail value=1 checked> <?php echo $mail_title; ?></td></tr>
<?php
}
?>
<?php
if ($TabID != 4 && isset($plugin['personalfile']) && $plugin['personalfile'] && in_array($TabID,$personalfile_identity_allowed) )
{
    if ($personalfile_type=='FTP')
    {
        include_once($PATH_WRT_ROOT."includes/libftp.php");
        $lftp = new libftp();
        if ($lftp->isAccountManageable)
        {
            $file_title = $i_Files_OpenAccount;
        }
        else
        {
            $file_title = $i_Files_LinkFTPAccount;
        }
    }
    else if ($personalfile_type=='AERO')
    {
         $file_title = $i_Files_LinkToAero;
    }
    ?>
<tr><td align=right nowrap></td><td><input type=checkbox name=open_file value=1 checked> <?php echo $file_title; ?></td></tr>
    <?
}
?>
<? if ($TabID != 4) { ?>
<tr><td align="right"><?=$i_admintitle_group?>:</td>
<td><?php echo $lo->displayUserGroups(); ?></td></tr>
<? } ?>
<? if ($TabID == 3) { ?>
<tr><td align="right"><?=$i_UserParentLink?>:</td>
<td>
<table width=100% border=0 cellspacing=1 cellpadding=1>
<tr><td width=1>
<select name=child[] size=4 multiple>
<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select>
</td><td>
<a href="javascript:newWindow('choose/index.php?fieldname=child[]',2)"><img src="/images/admin/button/s_btn_select_std_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['child[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</td></tr>
<? } ?>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
}
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>                     