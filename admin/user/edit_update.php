<?php
# Using By : 

################ Change Log [Start] #######################
# - 2011-03-29 (Henry Chow)
#	Assign to corresponding identity group for staff account 
# - 2010-08-27 YatWoon
#	hide Title
# -	2010-02-27 Ivan
#	If the system is using eEnrol and have the term-based club enhancement, do not delete the ECA Groups of the users
# -	2009-12-08 YatWoon
#	Only UserType=4 (alumni) student can update YearOfLeft
################ Change Log [End] #######################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
#include_once($PATH_WRT_ROOT."includes/libwebmail.php");
#include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once("$eclass_filepath/src/includes/php/lib-portfolio.php");
intranet_opendb();

# check if iportfolio activated
$iportfolio_activated = false;

if($plugin['iPortfolio']){
	$lportfolio = new portfolio();
	$data = $lportfolio->returnAllActivatedStudent();
	if($TabID==2){
		for($i=0;$i<sizeof($data);$i++){
			if($UserID==$data[$i][0]){
				$iportfolio_activated = true;
				break;
			}
		}
	}
}

$li = new libdb();
$lu = new libuser($UserID);
$lc = new libeclass();



$EnglishName = intranet_htmlspecialchars(trim($EnglishName));
$ChineseName = intranet_htmlspecialchars(trim($ChineseName));
$NickName = intranet_htmlspecialchars(trim($NickName));
// $Title = intranet_htmlspecialchars(trim($Title));
$Gender = intranet_htmlspecialchars(trim($Gender));
$DateOfBirth = intranet_htmlspecialchars(trim($DateOfBirth));
$HomeTelNo = intranet_htmlspecialchars(trim($HomeTelNo));
$OfficeTelNo = intranet_htmlspecialchars(trim($OfficeTelNo));
$MobileTelNo = intranet_htmlspecialchars(trim($MobileTelNo));
$FaxNo = intranet_htmlspecialchars(trim($FaxNo));
$ICQNo = intranet_htmlspecialchars(trim($ICQNo));
$Address = intranet_htmlspecialchars(trim($Address));
$Country = intranet_htmlspecialchars(trim($Country));
$Info = intranet_htmlspecialchars(trim($Info));
$Remark = intranet_htmlspecialchars(trim($Remark));
/*
$ClassNumber = intranet_htmlspecialchars(trim($ClassNumber));
$ClassName = intranet_htmlspecialchars(trim($ClassName));
*/
#$ClassLevel = intranet_htmlspecialchars(trim($ClassLevel));
$CardID = intranet_htmlspecialchars(trim($CardID));
if($TabID==4)
{
	$YearOfLeft = intranet_htmlspecialchars(trim($YearOfLeft));
}
$WebSamsRegNo = intranet_htmlspecialchars(trim($WebSamsRegNo));


$fieldname  = "EnglishName = '$EnglishName', ";
$fieldname .= "ChineseName = '$ChineseName', ";
$fieldname .= "NickName = '$NickName', ";
// $fieldname .= "Title = '$Title', ";
$fieldname .= "Gender = '$Gender', ";
$fieldname .= "DateOfBirth = '$DateOfBirth', ";
$fieldname .= "HomeTelNo = '$HomeTelNo', ";
$fieldname .= "OfficeTelNo = '$OfficeTelNo', ";
$fieldname .= "MobileTelNo = '$MobileTelNo', ";
$fieldname .= "FaxNo = '$FaxNo', ";
$fieldname .= "ICQNo = '$ICQNo', ";
$fieldname .= "Address = '$Address', ";
$fieldname .= "Country = '$Country', ";
$fieldname .= "Info = '$Info', ";
$fieldname .= "Remark = '$Remark', ";
//$fieldname .= "ClassNumber = '$ClassNumber', ";
//$fieldname .= "ClassName = '$ClassName', ";
#$fieldname .= "ClassLevel = '$ClassLevel', ";
$fieldname .= "RecordStatus = '$RecordStatus', ";
$fieldname .= "Teaching = '$teaching',";

$error_msg="";

if ($TabID == 1 && ((isset($module_version['StaffAttendance'])&&$module_version['StaffAttendance']!="" ) || (isset($plugin['payment'])&& $plugin['payment'])) ){
    $fieldname .= "CardID = '$CardID',";
}

if ($TabID==2) # Change CardID for Student ONLY
{
    $fieldname .= "CardID = '$CardID',";
    if($WebSamsRegNo!=""){
	    if(substr(trim($WebSamsRegNo),0,1)!="#")
			$WebSamsRegNo = "#".$WebSamsRegNo;
    }
    if(!$iportfolio_activated){
		   	### check if websams regno exists ####
		   	if($WebSamsRegNo!=""){
				$sql="SELECT UserID FROM INTRANET_USER WHERE WebSamsRegNo='".$WebSamsRegNo."' AND UserID!=$UserID AND RecordType=2";
				$temp = $li->returnVector($sql);
				if(sizeof($temp)!=0){ ## WebSamsRegNo exists
					$error_msg = "WebSAMS Registration Number has been used by other user.";
				}	
		    }
			$fieldname.="WebSAMSRegNo = '$WebSamsRegNo',";

    }
}

if($CardID!=""){
	$sql="SELECT UserID FROM INTRANET_USER WHERE CardID='$CardID' AND UserID!=$UserID ";
	$temp = $li->returnVector($sql);
	if(sizeof($temp)!=0){ ## card id exists
		$error_msg = "Smart Card ID has been used by other user.";
	}	
}

# check HKID is existing or not (if include HKID field)
if($special_feature['ava_hkid'])
{
	$HKID = intranet_htmlspecialchars(trim($HKID));
	if($HKID != "")
	{
		$sql="SELECT UserID FROM INTRANET_USER WHERE HKID='".$HKID."' AND UserID!=$UserID ";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)!=0){ ## HKID exists
			$error_msg ="HKID has been used by other user.";
		}
	}
}

# check STRN is existing or not (if include STRN field)
if($special_feature['ava_strn'] && ($TabID==2 || $TabID==4))
{
	$STRN = intranet_htmlspecialchars(trim($STRN));
	if($STRN != "")
	{
		$sql="SELECT UserID FROM INTRANET_USER WHERE STRN='".$STRN."' AND UserID!=$UserID ";
		$temp = $li->returnVector($sql);
		if(sizeof($temp)!=0){ ## STRN exists
			$error_msg ="STRN has been used by other user.";
		}
	}
}


if($error_msg!=""){
		        include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
		        echo " <br>\n $error_msg \n";
		        echo  "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
		                    <tr><td><hr size=1></td></tr>
		                    <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_cancel_$intranet_session_language.gif' border='0'></a></td></tr></table>";
		
		        include_once($PATH_WRT_ROOT."templates/adminfooter.php");
		        exit();

}


if($TabID==4)
{
	$fieldname .= "YearOfLeft = '$YearOfLeft',";
}
if ($TabID == 1)
{
    $TitleEnglish = intranet_htmlspecialchars($TitleEnglish);
    $TitleChinese = intranet_htmlspecialchars($TitleChinese);
    $fieldname .= "TitleEnglish = '$TitleEnglish',";
    $fieldname .= "TitleChinese = '$TitleChinese',";
}
else
{
    $fieldname .= "TitleEnglish = NULL,";
    $fieldname .= "TitleChinese = NULL,";
}

if($special_feature['ava_hkid'])
{
	$fieldname .= "HKID = '$HKID',";
}

if($special_feature['ava_strn'])
{
	$fieldname .= "STRN = '$STRN',";
}

$fieldname .= "DateModified = now()";
$sql = "UPDATE INTRANET_USER SET $fieldname WHERE UserID = $UserID";
$li->db_db_query($sql);

if ($TabID != 4)
{
	# Update Groups
	
	# Remove unrequired groups
	# Do not remove ECA Groups (20100227 Ivan)
	# Hide ECA Groups if eEnrolment is updated to use Year-based Term-based Club (20090904 Ivan)
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$cond_ECA_GroupID = "";
	if ($plugin['eEnrollment'] && $libenroll->isUsingYearTermBased)
	{
		$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = '5'";
		$ECA_GroupIDArr = $li->returnVector($sql);
		
		$cond_ECA_GroupID = '';
		if (is_array($ECA_GroupIDArr) && count($ECA_GroupIDArr) > 0)
		{
			$ECA_GroupIDList = implode(',', $ECA_GroupIDArr);
			$cond_ECA_GroupID = " AND GroupID NOT IN (".$ECA_GroupIDList.") ";
		}
	}
	
	if(!empty($GroupID))
	{
		$tempGroupID = array();
		foreach($GroupID as $k=>$d)
		{
			if(is_numeric($d))	
			{
				$tempGroupID[] = $d;
			}
		}
		$GroupID = $tempGroupID;
	}
	else
	{
		$GroupID = array();
	}
	
	
	# Remove unrequired groups
	if(sizeof($GroupID) > 0){
	     $sql  = "DELETE FROM INTRANET_USERGROUP WHERE GroupID NOT IN (".implode(",",$GroupID).") AND GroupID NOT IN (1,2,3,4) AND UserID = $UserID";
	     $sql .= $cond_ECA_GroupID;
	     $li->db_db_query($sql);		
		 
	}
	else
	{
	    $sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID NOT IN (1,2,3,4) AND UserID = $UserID";
	    $sql .= $cond_ECA_GroupID;
	    $li->db_db_query($sql);
	}
	
	for($i=0; $i<sizeof($GroupID); $i++){
		# check duplicate
		$dup_sql = "select count(*) from INTRANET_USERGROUP where GroupID=".$GroupID[$i]." and UserID=$UserID";
		$dup = $li->returnVector($dup_sql);
		if($dup[0] == 0)
		{
			$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (".$GroupID[$i].", $UserID, now(), now())";
			$li->db_db_query($sql);
		}
	}
	
	# assign to group depends on the identity
	if($TabID==1) {
		if ($teaching == 1) {   # Teaching staff -> Teacher group
		    $target_idgroup = 1;
		} else {                   # Non-teaching staff -> Admin staff group
		    $target_idgroup = 3;
		}
		$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID='$UserID' AND GroupID IN (1,3)";
		$lu->db_db_query($sql);
		
		$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES ($target_idgroup, $UserID, now(), now())";
		$lu->db_db_query($sql);		
	}
	
	# update iCalendar
	$GroupID = empty($GroupID)?Array():$GroupID;
		$sql = "delete from CALENDAR_CALENDAR_VIEWER where 
			GroupID not in ('".implode("','",$GroupID)."') and GroupType = 'E' and UserID = $UserID";
		$lu->db_db_query($sql);
		
		$sql = "select GroupID from CALENDAR_CALENDAR_VIEWER where GroupType = 'E' and UserID = $UserID";
		$existing = $lu->returnVector($sql);
		
		$existing = empty($existing)? array():$existing;	
	
		//$newGroup = array_diff($GroupID,$existing);
		//if (!empty($existing)){
		$sql = "insert into CALENDAR_CALENDAR_VIEWER 
				(CalID, UserID, GroupID, GroupType, Access, Color, Visible)
				select g.CalID, '$UserID', g.GroupID, 'E', 'R', '2f75e9', '1' 
				from INTRANET_GROUP as g inner join INTRANET_USERGROUP as u 
				on g.GroupID = u.GroupID and u.UserID = $UserID
				where g.GroupID not in ('".implode("','",$existing)."')";
		$lu->db_db_query($sql);
		
	
	$li->UpdateRole_UserGroup();
	#$lc->eClassUserUpdateProfile12($lu->UserEmail, $Title, $EnglishName,$NickName, "", $ClassName,$ClassNumber, "",$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,"",$Info);
	$lc->eClassUserUpdateInfoIP($lu->UserEmail, $Title, $EnglishName,$ChineseName, $lu->FirstName,$lu->LastName,$NickName, "", $ClassName,$ClassNumber, "",$Gender, $ICQNo,$HomeTelNo,$FaxNo,$DateOfBirth,$Address,$Country,"",$Info);
	
	if ($TabID==3)             # Parent Student relation linkage
	{
	    $values = "";
	    $delimiter = "";
	    for ($i=0; $i<sizeof($child); $i++)
	    {
	         $childID = $child[$i];
	         $values .= "$delimiter ($UserID,$childID)";
	         $delimiter = ",";
	    }
	    $sql = "DELETE FROM INTRANET_PARENTRELATION WHERE ParentID = $UserID";
	    $li->db_db_query($sql);
	    if ($values != "")
	    {
	        $sql = "INSERT IGNORE INTO INTRANET_PARENTRELATION (ParentID, StudentID) VALUES $values";
	        $li->db_db_query($sql);
	    }
	}
	/*
	$acl_field = 0;
	
	if ($open_webmail && $open_file)
	{
	    $lwebmail = new libwebmail();
	    $lftp = new libftp();
	
	    if ($lwebmail->has_webmail)
	    {
	        #$lwebmail->open_account($UserLogin,$UserPassword);
	        $acl_field += 1;
	    }
	    $lftp->initAccount($lu->UserLogin,$lu->UserPassword);       # Account already Open in mail part
	    $acl_field += 2;
	}
	else if ($open_webmail) {
	    // Open webmail account
	    $lwebmail = new libwebmail();
	    if ($lwebmail->has_webmail && $lwebmail->type != 2)
	    {
	        #$mail_failed = ($lwebmail->open_account($UserLogin,$UserPassword)? 0 : 1);
	        $acl_field += 1;
	    }
	}
	else if ($open_file)
	{
	    if ($personalfile_type=='LOCAL_FTP' || $personalfile_type == 'REMOTE_FTP')
	    {
	        #$lftp = new libftp();
	        #$file_failed = ($lftp->openAccount($UserLogin,$UserPassword)? 0:1);
	    }
	    $acl_field += 2;
	}
	
	if (!$open_webmail && !$open_file)
	{
	     $lwebmail = new libwebmail();
	     $lwebmail->delete_account($lu->UserLogin);
	     $acl_field = 0;
	}
	
	$sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = $acl_field WHERE UserID = $UserID";
	$li->db_db_query($sql);
	if ($li->db_affected_rows()==0)
	{
	    $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($UserID, $acl_field)";
	    $li->db_db_query($sql);
	}
	
	*/
	
	# SchoolNet
	if ($plugin['SchoolNet'])
	{
	    include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
	    $lschoolnet = new libschoolnet();
	    if ($TabID == 1)
	    {
	        # Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
	        $sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '".IntegerSafe($UserID)."'";
	        $data = $li->returnArray($sql,11);
	        $lschoolnet->addStaffUser($data);
	    }
	    else if ($TabID == 2)
	    {
	         #($userlogin, $password, $classname, $classnum, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email)
	        $sql = "SELECT UserLogin, UserPassword,ClassName, ClassNumber, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail FROM INTRANET_USER WHERE UserID = '".IntegerSafe($UserID)."'";
	         $data = $li->returnArray($sql,12);
	         $lschoolnet->addStudentUser($data);
	    }
	    else if ($TabID == 3)
	    {
	    }
	}

}          # End of if (TabID!=4)


$lc->addTitleToCourseUser($lu->UserEmail,$TitleEnglish,$TitleChinese,$EnglishName,$ChineseName); 
intranet_closedb();
header("Location: index.php?TabID=$TabID&filter=$RecordStatus&order=$order&field=$field&pageNo=$pageNo&msg=2");
//header("Location: index.php?TabID=".$lu->RecordType."&filter=$RecordStatus&order=$order&field=$field&msg=2");
?>