<?php

// Modifing by 
/*
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
*/
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libftp.php");
include_once($PATH_WRT_ROOT."includes/libldap.php");
include_once($PATH_WRT_ROOT."includes/libstudentpromotion.php");
include_once($PATH_WRT_ROOT."includes/libaccountmgmt.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_opendb();

$lsp = new libstudentpromotion();
$lu = new libuser();

/*
$sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID = '$targetStudentID'";
$temp = $lsp->returnArray($sql,2);
list($ClassName, $ClassNumber) = $temp[0];

$yearOfLeft = date('Y');
$MonthOfLeft = date('m');

// Modified by key [2008-12-11]: If the day is after 1st Sept, change the year of left to next year
//if($MonthOfLeft >= 9)
//$yearOfLeft += 1;

# Insert class history record
$lsp->addClassHistory($targetStudentID);

$sql = "UPDATE INTRANET_USER SET RecordStatus = 3, YearOfLeft = '$yearOfLeft' WHERE UserID = '$targetStudentID'";
$lsp->db_db_query($sql);
*/

##### The following code is copied from frontend (Account Mgmt > Student Mgmt > Not In Class > Archive) ###
### [Start] ###

$CurrentYear = getCurrentAcademicYear();
$lsp->setAcademicYear($CurrentYear);

$list = $targetStudentID;

# Archive Student Information
$lsp->archiveStudentsInfo($list);
# Archive Process of remove users
$lu->prepareUserRemoval($list);

# If alumni auto , change record type and add to alumni
# else remove from INTRANET_USER
if ($special_feature['alumni'] && $alumni_system_type==1)
{
    $lu->setToAlumni($list);
}
else
{
    $lu->removeUsers($list);
}
### [End] ###

intranet_closedb();
header("Location: index.php?TabID=$TabID&filter=$filter&order=$order&field=$field&msg=2");

?>