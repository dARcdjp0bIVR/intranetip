<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
intranet_opendb();

$lo = new libgrouping();
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.subject, "<?php echo $i_alert_pleasefillin.$i_email_subject; ?>.")) return false;
        if(!check_text(obj.message, "<?php echo $i_alert_pleasefillin.$i_email_message; ?>.")) return false;
        return (confirm("<?php echo $i_email_sendemail; ?>?")) ? true : false;
}
</script>

<form name="form1" action="email_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_am, '', $i_admintitle_am_user, 'javascript:history.back()', $button_email, '') ?>
<?= displayTag("head_user_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<blockquote>
<table width=400 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?php echo $i_email_to; ?>:</td><td><select name=UserEmail[] size=5 multiple><?php echo $lo->displayUsersEmailOption($filter,$keyword,$TabID); ?></select></td></tr>
<tr><td align=right><?php echo $i_email_subject; ?>:</td><td><input class=text type=text name=subject size=35></td></tr>
<tr><td><br></td><td><textarea name=message cols=55 rows=15 wrap=virtual></textarea></td></tr>
</table>
</blockquote>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?> 
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=keyword value="<?php echo $keyword; ?>">
<input type=hidden name=filter value="<?php echo $filter; ?>">
<input type=hidden name=TabID value="<?php echo $TabID; ?>">
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>