<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$user_field = getNameFieldByLang("b.");
$sql  = "SELECT
               a.ClassName,a.ClassNumber
               a.RecordDate,
               $user_field,
               a.ArrivalTime,
               a.DepartureTime,
               a.Location,a.Reason, a.Remark
         FROM
             CARD_STUDENT_DETENTION as a LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
         WHERE
              (a.RecordDate = '$keyword' OR
              b.ChineseName like '%$keyword%'
               OR b.EnglishName like '%$keyword%'
               OR a.Location LIKE '%$keyword%'
               OR a.Remark LIKE '%$keyword%')
                ";

$result = $li->returnArray($sql,9);
$filecontent = "ClassName,ClassNumber,Date,StudentName,Arrival,Departure,Location,Reason,Remark\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($class,$classnum,$recordDate,$studentname,$arrival,$departure,$location,$reason,$remark) = $result[$i];
     $filecontent .= "\"$class\",\"$classnum\",\"$recordDate\",\"$studentname\",\"$arrival\",\"$departure\",\"$location\",\"$reason\"\"$remark\",\n";
}

// Output the file to user browser
$filename = "detention_records.csv";

header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($filecontent));
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $filecontent;

intranet_closedb();

?>