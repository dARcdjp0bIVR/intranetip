<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
intranet_opendb();


$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if ($format == 1)
    $format_array = array("UserLogin","CardID");
else
    $format_array = array("ClassName","ClassNumber","CardID");


if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: studentcard_import.php?failed=2");
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                $header_row = array_shift($data);                   # drop the title bar
        }

        # Check Format
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($header_row[$i] != $format_array[$i])
             {
                 $format_wrong = true;
             }
        }

        if ($format_wrong)
        {
            $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
            $display .= "<table width=100 border=1>\n";
            for ($i=0; $i<sizeof($format_array); $i++)
            {
                 $display .= "<tr><td>".$format_array[$i]."</td></tr>\n";
            }
            $display .= "</table></blockquote>\n";
        }
        else
        {
            for ($i=0; $i<sizeof($data); $i++)
            {
                 if ($format==1)
                 {
                     list($login, $cardid) = $data[$i];
                     $sql = "UPDATE INTRANET_USER SET CardID = '$cardid' WHERE UserLogin = '$login' AND RecordType = 2";
                     $li->db_db_query($sql);
                     $affected = $li->db_affected_rows();
                     if ($affected == 0)
                     {
                         # Check whether this record exists
                         $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$login' AND RecordType = 2";
                         $temp = $li->db_db_query($sql);
                         if ($temp[0]=="")
                         {
                             $recordValid[$i] = false;
                             $reason[$i] = "UserLogin ($login) does not exist.";
                         }
                         else
                         {
                             $recordValid[$i] = true;
                         }
                     }
                     else
                     {
                         $recordValid[$i] = true;
                     }
                 }
                 else
                 {
                     list($class,$classnum,$cardid) = $data[$i];
                     $sql = "UPDATE INTRANET_USER SET CardID = '$cardid' WHERE ClassName = '$class' AND ClassNumber = '$classnum' AND RecordType = 2";
                     $li->db_db_query($sql);
                     $affected = $li->db_affected_rows();
                     if ($affected == 0)
                     {
                         # Check whether this record exists
                         $sql = "SELECT UserID FROM INTRANET_USER WHERE ClassName = '$class' AND ClassNumber = '$classnum' AND RecordType = 2";
                         $temp = $li->db_db_query($sql);
                         if ($temp[0]=="")
                         {
                             $recordValid[$i] = false;
                             $reason[$i] = "($class - $classnum) does not exist.";
                         }
                         else
                         {
                             $recordValid[$i] = true;
                         }
                     }
                     else
                     {
                         $recordValid[$i] = true;
                     }
                 }
            }

            # Mark Reason to display
            for ($i=0; $i<sizeof($recordValid); $i++)
            {
                 if (!$recordValid[$i] && $reason[$i]!="")
                 {
                      $display .= "<tr><td>Line ".($i+1)."</td><td>".$reason[$i]."</td></tr>\n";
                 }
            }
            if ($display != "")
            {
                $display = "<table width=90% align=center border=1 cellspacing=0 cellpadding=0>\n$display\n</table>\n";
            }
        }

        if ($display != '')
        {
            include_once("../../../lang/lang.$intranet_session_language.php");
            include_once("../../../templates/adminheader_setting.php");
            echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_ImportCardID,'studentcard.php',$button_import,'');
            echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);
            if (!$format_wrong)
            {
                 $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                                <tr><td><hr size=1></td></tr>
                                <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
                 echo "$i_general_ImportFailed<br>";
            }
            else
            {
                $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                               <tr><td><hr size=1></td></tr>
                               <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
            }
            echo $display;
            include_once("../../../templates/adminfooter.php");
        }
        else
        {
            header("Location: studentcard.php?msg=2");
        }

}

intranet_closedb();

?>