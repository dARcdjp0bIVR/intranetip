<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;

if (isset($targetClass) && $targetClass != "")
{
    $conds = "AND a.ClassName = '$targetClass'";
}
$user_field = getNameFieldByLang("a.");

$sql = "SELECT $user_field, a.ClassName, a.ClassNumber, a.CardID,
               CONCAT('<input type=checkbox name=StudentID[] value=', a.UserID ,'>')
               FROM INTRANET_USER as a WHERE a.RecordType = 2 AND a.RecordStatus != 3 $conds
               ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.EnglishName","a.ClassName","a.ClassNumber","a.CardID");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassName)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_UserClassNumber)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_SmartCard_CardID)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentID[]")."</td>\n";

$lc = new libclass();
$select_classes = $lc->getSelectClass("name=targetClass onChange='this.form.submit()'", $targetClass);
/*
$classes = $lc->getClassList();
$temp = $button_select;
$button_select = $i_status_all;
$select_classes = getSelectByArray($classes,"name=targetClass onChange='this.form.submit()'");
$button_select = $temp;
*/
/*
$select_status = "<SELECT name=filter onChange=this.form.submit()>\n";
$select_status .= "<OPTION value='' ".($filter==""?"SELECTED":"").">$i_status_all</OPTION>\n";
$select_status .= "<OPTION value='1' ".($filter=="1"?"SELECTED":"").">$i_StudentAttendance_Reminder_Status_Coming</OPTION>\n";
$select_status .= "<OPTION value='2' ".($filter=="2"?"SELECTED":"").">$i_StudentAttendance_Reminder_Status_Past</OPTION>\n";
$select_status .= "</SELECT>\n";
*/

$toolbar .= toolBarSpacer()."<a class=iconLink href=javascript:checkNew('studentcard_import.php')>".importIcon()."$button_import</a>";
$toolbar .= toolBarSpacer()."<a class=iconLink href=javascript:checkGet(document.form1,'studentcard_export.php')>".exportIcon()."$button_export</a>";
#$functionbar .= $select_status;
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'StudentID[]','studentcard_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$searchbar = $select_classes;
#$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
#$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_ImportCardID,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);
?>
<form name="form1" method="get">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
