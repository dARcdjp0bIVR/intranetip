<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libuser.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$StudentID = (is_array($StudentID)? $StudentID[0]: $StudentID);
$lu = new libuser($StudentID);

?>

<form name="form1" method="POST" action="studentcard_edit_update.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_ImportCardID,'studentcard.php',$button_edit,'')?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_UserStudentName; ?>:</td><td><?=$lu->UserNameClassNumber()?></td></tr>
<tr><td align=right nowrap><?php echo $i_SmartCard_CardID; ?>:</td><td><input type=text name=CardID value="<?=$lu->CardID?>" size=20></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

<input type=hidden name=pageNo value="<?php echo $pageNo; ?>">
<input type=hidden name=order value="<?php echo $order; ?>">
<input type=hidden name=field value="<?php echo $field; ?>">
<input type=hidden name=targetClass value="<?=$targetClass?>">
<input type=hidden name=StudentID value="<?=$StudentID?>">
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>