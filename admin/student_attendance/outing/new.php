<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();

echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Outing,'index.php',$button_new,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);

if (!isset($ClassName) || $ClassName == "")
{
        $select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()");
        $x = "<form name=form1 action='' method=GET>\n";
        $x .= "<table width=90% border=0 align=center><tr><td>\n";
        $x .= "$i_UserParentLink_SelectClass: $select_class";
        $x .= "</td></tr></table></form>\n";
        echo $x;
}
else
{
        $select_class = $lclass->getSelectClass("name=ClassName onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
        $select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID");

        $currentDate = date('Y-m-d');
        $currentTime = date('H:i:s');
        $lcard = new libcardstudentattend();

        $fromWhereTemplate = getSelectByValue($lcard->getWordList(1),"onChange=\"this.form.FromWhere.value=this.value\"");
        $locationTemplate = getSelectByValue($lcard->getWordList(2),"onChange=\"this.form.Location.value=this.value\"");
        $objectiveTemplate = getSelectByValue($lcard->getWordList(3),"onChange=\"this.form.Objective.value=this.value\"");
        $namefield = getNameFieldForRecord();
        $sql = "SELECT $namefield FROM INTRANET_USER WHERE RecordType = 1 ORDER BY EnglishName";
        $staff = $lclass->returnVector($sql);
        $picTemplate = getSelectByValue($staff,"onChange=\"addPIC(this.form,this.value)\"");

        # Get Time bounds
        $setting_file = "$intranet_root/file/attend_st_settings.txt";
        $file_content = get_file_content($setting_file);
        $lines = explode("\n",$file_content);
        for ($i=0; $i<sizeof($lines); $i++)
        {
             $settings[$i] = explode(",",$lines[$i]);
        }

?>
<script language="javascript">
function addPIC(obj, pic)
{
         if (obj.PIC.value != '')
         {
             obj.PIC.value += ', ';
         }
         obj.PIC.value += pic;
}
</SCRIPT>
<form name=form1 action="new_update.php" method=POST>
<blockquote>
<table width=500 border=0 align=center>
<tr><td align=right nowrap><?=$i_SmartCard_ClassName?></td><td><?=$select_class?></td></tr>
<tr><td align=right nowrap><?=$i_UserStudentName?></td><td><?=$select_student?></td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingDate?></td><td><input type=text name=OutingDate size=10 maxlength=10 value="<?=$currentDate?>">(YYYY-MM-DD)</td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingOutTime?></td><td><input type=text name=OutTime size=10 maxlength=10 value="<?=$currentTime?>">(HH:mm:ss 24-hr)</td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingBackTime?></td><td><input type=text name=BackTime size=10 maxlength=10>(HH:mm:ss 24-hr)</td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingPIC?></td><td><input type=text name=PIC size=50><?=$picTemplate?></td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingFromWhere?></td><td><input type=text name=FromWhere size=50><?=$fromWhereTemplate?></td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingLocation?></td><td><input type=text name=Location size=50><?=$locationTemplate?></td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_StudentOutingReason?></td><td><input type=text name=Objective size=50><?=$objectiveTemplate?></td></tr>
<tr><td align=right nowrap><?=$i_SmartCard_Remark?></td><td><TEXTAREA rows=5 cols=50 name=Remark></TEXTAREA></td></tr>
<? if ($settings[0][0] == 1) { ?>
<tr><td align=right><?php echo "$i_StudentAttendance_SimCard ($i_StudentAttendance_Slot_AM)"; ?>:</td><td><input type=text name=Time1 size=10 maxlength=10 onClick="if (this.value=='') this.value='<?=$settings[0][3]?>'">(hh:mm:ss)</td></tr>
<? } ?>
<? if ($settings[1][0] == 1) { ?>
<tr><td align=right><?php echo "$i_StudentAttendance_SimCard ($i_StudentAttendance_Slot_PM)"; ?>:</td><td><input type=text name=Time2 size=10 maxlength=10 onClick="if (this.value=='') this.value='<?=$settings[1][3]?>'">(hh:mm:ss)</td></tr>
<? } ?>
<? if ($settings[2][0] == 1) { ?>
<tr><td align=right><?php echo "$i_StudentAttendance_SimCard ($i_StudentAttendance_Slot_AfterSchool)"; ?>:</td><td><input type=text name=Time3 size=10 maxlength=10 onClick="if (this.value=='') this.value='<?=$settings[2][3]?>'">(hh:mm:ss)</td></tr>
<? } ?>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>
<?
}
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
