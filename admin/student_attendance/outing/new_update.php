<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$Location = intranet_htmlspecialchars($Location);
$Remark = intranet_htmlspecialchars($Remark);
$Objective = intranet_htmlspecialchars($Objective);
$PIC = intranet_htmlspecialchars($PIC);

$outTimeStr = ($OutTime ==""? "NULL":"'$OutTime'");
$backTimeStr = ($BackTime ==""? "NULL":"'$BackTime'");

$sql = "INSERT INTO CARD_STUDENT_OUTING (UserID, RecordDate,OutTime,BackTime,Location,FromWhere,Objective,Detail,PIC,DateInput,DateModified)
        VALUES ('$StudentID','$OutingDate',$outTimeStr,$backTimeStr,'$Location','$FromWhere','$Objective','$Remark','$PIC',now(),now())";
$li->db_db_query($sql);
header ("Location: index.php?msg=1");
intranet_closedb();
?>