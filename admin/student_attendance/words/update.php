<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");

intranet_opendb();
$lcard = new libcardstudentattend();
$lf = new libfilesystem();
$base_dir = $lcard->word_base_dir;
if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$file_array = $lcard->file_array;
if ($type >= sizeof($file_array) || $type < 0)
{
    header ("Location: index.php");
}
else
{
    $file_target = $file_array[$type];
    $data = stripslashes($data);
    $lf->file_write($data,"$base_dir$file_target");
    header("Location: index.php?type=$type&msg=2");
}
intranet_closedb();
?>