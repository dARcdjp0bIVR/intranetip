<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();

$lcard = new libcardstudentattend();
$base_dir = $lcard->word_base_dir;
$lf = new libfilesystem();

if (!is_dir($base_dir))
{
     $lf->folder_new($base_dir);
}

$file_array = $lcard->file_array;
$word_array = $lcard->word_array;


$type +=0;

$type_select = "<SELECT name=type onChange=\"location='index.php?type='+this.value\">";
for ($i=0; $i<sizeof($file_array); $i++)
{
     $selected_str = ($type==$i? "SELECTED":"");
     $type_select .= "<OPTION value=$i $selected_str>".$word_array[$i]."</OPTION>\n";
}
$type_select .= "</SELECT>\n";

$data = get_file_content($base_dir.$file_array[$type]);

?>

<script language="javascript">
function checkform(obj){
     return true;
}
</script>

<form action="update.php" name="form1">
<?= displayNavTitle($i_admintitle_sc, '',$i_StudentAttendance_System,'../', $i_StudentAttendance_WordTemplates, '') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<br>
<?=$i_wordtemplates_select?> <?=$type_select?><br>
<textarea name=data COLS=60 ROWS=20>
<?=$data?>
</textarea>
<br><span class="extraInfo"><?=$i_wordtemplates_instruction?></span>
</blockquote>
</td></tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type='image' src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'>
<?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>