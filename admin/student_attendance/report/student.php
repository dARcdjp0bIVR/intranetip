<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();

$setting_file = "$intranet_root/file/attend_st_settings.txt";
if (!is_file($setting_file))
{
     $default_content = "1,06:00,09:00,07:40\n1,12:30,14:30,13:30\n1,15:00,18:00,16:00";
     write_file_content($default_content,$setting_file);
}
$file_content = get_file_content($setting_file);
$lines = explode("\n",$file_content);
for ($i=0; $i<sizeof($lines); $i++)
{
     $settings[$i] = explode(",",$lines[$i]);
}

# Preset Date Range to current month
if (!isset($year) || $year == "")
{
     $year = date('Y');
}
if (!isset($month) || $month == "")
{
     $month = date('m');
}

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";

include_once("../../../templates/fileheader.php");
echo displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'./', $i_StudentAttendance_Report_Student,'');
echo displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg);

if (!isset($ClassName) || $ClassName == "")
{
/*
        $sql = "SELECT DISTINCT ClassName FROM INTRANET_USER WHERE ClassName != '' ORDER BY ClassName";
        $classes = $li->returnVector($sql);
        $select_class = getSelectByValue($classes,"name=ClassName onChange=this.form.submit()");
        */
        $lclass = new libclass();
        $select_class = $lclass->getSelectClass("name=ClassName onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
        $x = "<form name=form1 action='' method=GET>\n";
        $x .= "<table width=90% border=0 align=center><tr><td>\n";
        $x .= "$i_UserParentLink_SelectClass: $select_class";
        $x .= "</td></tr></table></form>\n";
        echo $x;
}
else
{
        $lclass = new libclass();
        $select_class = $lclass->getSelectClass("name=ClassName onChange=\"this.form.action='';this.form.submit()\"",$ClassName);
        $select_student = $lclass->getStudentSelectByClass($ClassName,"name=StudentID",$StudentID);
?>
        <script language="JavaScript" type="text/javascript">
        <!--
        function openPrintPage()
        {
                 newWindow("studentprint.php?<?php echo $HTTP_SERVER_VARS['QUERY_STRING'] ?>",4);
        }
        -->
        </script>

        <form action="" method=get>
        <input name="order" type="hidden" value="1">
        <table width="90%" border="0" align="center">
                <tr><td><?php echo $i_SmartCard_ClassName?></td><td><?=$select_class?></td></tr>
                <tr><td><?php echo $i_UserStudentName?></td><td><?=$select_student?></td></tr>
                <tr><td><?php echo $i_SmartCard_ReportMonth ?></td><td><input name="year" type="text" value="<?=$year?>" maxlength="4" size="4"> <input name="month" type="text" value="<?=$month?>" maxlength="2" size="2"> (<?php echo $i_general_Year ?> / <?php echo $i_general_Month ?>)</td></tr>
                <tr><td><?php echo $i_StudentAttendance_Report_SelectSlot?></td>
                        <td><? if ($settings[0][0]==1) { ?>
                                <input type=checkbox name=slot[] value=1 CHECKED><?=$i_StudentAttendance_Slot_AM?>
                                <? } ?>
                                <? if ($settings[1][0]==1) { ?>
                                <input type=checkbox name=slot[] value=2 CHECKED><?=$i_StudentAttendance_Slot_PM?>
                                <? } ?>
                                <? if ($settings[2][0]==1) { ?>
                                <input type=checkbox name=slot[] value=3 CHECKED><?=$i_StudentAttendance_Slot_AfterSchool?>
                                <? } ?>
                        </td>
                </tr>
                <tr><td colspan="2"><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
        </table>
        </form>
<?
        if (isset($StudentID) && $StudentID <> "")
        {
                $i=0;
                $lcard = new libcardstudentattend();
                $results = $lcard->retrieveStudentMonthlyRecord($StudentID, $year, $month, $slot, $order);
                $outing = $lcard->retrieveStudentMonthlyOutingRecord($StudentID,$year,$month);
                $x = '';

                if (sizeof($results) > 0) {
                        foreach ($results AS $key => $result)
                        {
                                $css = ($i++%2==0? "":"2");
                                if ($outing[$key]!="")
                                {
                                    $outLink = "<a href=javascript:newWindow('outing_view.php?StudentID=$StudentID&targetDate=$key',2)>[$i_StudentAttendance_Outing]</a>";
                                }
                                else
                                {
                                    $outLink = "";
                                }
                                $x .= "<tr class=tableContent$css><td>$key $outLink</td>";

                                for ($j=0; $j<sizeof($slot); $j++) {
                                        if ($result[$slot[$j]][1] === 0)
                                        {
                                                $background = "#FFFFFF";
                                                $textcolor = "#000000";
                                                $str = $result[$slot[$j]][0];
                                        }
                                        elseif ($result[$slot[$j]][1] === 1)
                                        {
                                                $background = "#D9F0C2";
                                                $textcolor = "#3E7608";
                                                $str = $i_StudentAttendance_Status_Absent;
                                        }
                                        elseif ($result[$slot[$j]][1] === 2)
                                        {
                                                $background = "#FDC2B4";
                                                $textcolor = "#C62E2E";
                                                $str = $result[$slot[$j]][0]." ($i_StudentAttendance_MinLate: ".$result[$slot[$j]][2].")";
                                        }
                                        elseif ($result[$slot[$j]][1] === 3)
                                        {
                                                $background = "#B4D5FD";
                                                $textcolor = "#036AD3";
                                                $str = $result[$slot[$j]][0]." ($i_StudentAttendance_MinEarly: ".$result[$slot[$j]][2].")";
                                        }
                                        else
                                        {
                                                $background = "#E5D8CA";
                                                $textcolor = "#797979";
                                                $str = "$i_StudentAttendance_Report_NoRecord";
                                        }

                                        $x .= "<td bgcolor='$background'><font color='$textcolor'>$str</font></td>";
                                }

                                $x .= "</tr>";
                        }
                } else {
                        $x .= "<tr><td colspan=".(sizeof($slot)+1)." align=center>$i_no_record_exists_msg</td></tr>";
                }

                $display = "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>\n";
                $display .= "<tr><td><img src='$image_path/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>";
                $display .= "<tr><td class=admin_bg_menu>$toolbar</td></tr>";
                $display .= "<tr><td>";

                $display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
                $display .= "<tr class='tableTitle'><td>$i_SmartCard_DateRecorded</td>";
                for ($i=0; $i<sizeof($slot); $i++) {
                        if ($slot[$i] == 1)
                        {
                                $display .= "<td>$i_StudentAttendance_Slot_AM</td>";
                        }
                        if ($slot[$i] == 2)
                        {
                                $display .= "<td>$i_StudentAttendance_Slot_PM</td>";
                        }
                        if ($slot[$i] == 3)
                        {
                                $display .= "<td>$i_StudentAttendance_Slot_AfterSchool</td>";
                        }
                }
                $display .= "</tr>";

                $display .= $x;
                $display .= "</table>\n";

                $display .= "</td></tr></table>\n";

                $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
                $display .= "<tr><td><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr>";
                $display .= "</table><br>\n";

                echo $display;
        }
}
intranet_closedb();
include_once("../../../templates/filefooter.php");
?>
