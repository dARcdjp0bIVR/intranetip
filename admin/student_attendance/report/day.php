<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$setting_file = "$intranet_root/file/attend_st_settings.txt";
if (!is_file($setting_file))
{
     $default_content = "1,06:00,09:00,07:40\n1,12:30,14:30,13:30\n1,15:00,18:00,16:00";
     write_file_content($default_content,$setting_file);
}
$file_content = get_file_content($setting_file);
$lines = explode("\n",$file_content);
for ($i=0; $i<sizeof($lines); $i++)
{
     $settings[$i] = explode(",",$lines[$i]);
}

$lclass = new libclass();
$classes = $lclass->getClassList();
$select_class = getSelectByArray($classes,"name=ClassID","",1);

?>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj) {
         if (countChecked(obj,'slot[]')==0)
         {
             alert('<?=$i_StudentAttendance_Report_PlsSelectSlot?>');
             return false;
         }
         if (!check_date(obj.targetDate,'<?=$i_invalid_date?>')) return false;
         return true;
}
</SCRIPT>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_Daily,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<form action=day_report.php method=GET ONSUBMIT="return checkform(this)">
<table width=95% border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?=$i_StudentAttendance_Report_SelectSlot?>:</td><td>
<? if ($settings[0][0]==1) { ?>
<input type=checkbox name=slot[] value=1 CHECKED><?=$i_StudentAttendance_Slot_AM?>
<? } ?>
<? if ($settings[1][0]==1) { ?>
<input type=checkbox name=slot[] value=2 CHECKED><?=$i_StudentAttendance_Slot_PM?>
<? } ?>
<? if ($settings[2][0]==1) { ?>
<input type=checkbox name=slot[] value=3 CHECKED><?=$i_StudentAttendance_Slot_AfterSchool?>
<? } ?>
</td></tr>
<tr><td align=right><?="$button_select $i_ClassName"?>:</td><td><?=$select_class?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_View_Date?>:</td><td><input type=text name=targetDate size=10 value='<?=date('Y-m-d')?>'></td></tr>
        <tr>
          <td colspan=2 align=right>
          <?=btnSubmit()?> <?=btnReset()?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
        </tr>

</table>
</form>
<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>