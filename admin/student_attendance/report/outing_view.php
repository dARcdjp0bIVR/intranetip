<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

#$i_title = $i_StudentAttendance_System.$i_StudentAttendance_Report.'('.$i_StudentAttendance_Report_Daily.')';
include_once("../../../templates/fileheader.php");

$luser = new libuser($StudentID);
$StudentName = $luser->UserNameClassNumber();

$lcard = new libcardstudentattend();
$result = $lcard->returnOutingRecord($StudentID,$targetDate);
                    $sql = "SELECT OutingID, OutTime, BackTime, Location, FromWhere, Objective, PIC, Detail
                            FROM CARD_STUDENT_OUTING WHERE UserID = $StudentID AND RecordDate = '$targetDate'";

$display = "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class='tableTitle'>
<td>$i_SmartCard_StudentOutingOutTime</td><td>$i_SmartCard_StudentOutingBackTime</td>
<td>$i_SmartCard_StudentOutingPIC</td>
<td>$i_SmartCard_StudentOutingFromWhere</td>
<td>$i_SmartCard_StudentOutingLocation</td>
<td>$i_SmartCard_StudentOutingReason</td>
<td>$i_SmartCard_Remark</td></tr>
";

for ($i=0; $i<sizeof($result); $i++)
{
        $css = ($i%2==0? "":"2");
        list($OutingID, $OutTime, $BackTime, $Location, $FromWhere, $Objective, $PIC, $Detail) = $result[$i];
     $display .= "<tr class=tableContent$css><td>$OutTime</td><td>$BackTime</td>
                   <td>$Location</td>
                   <td>$FromWhere</td>
                   <td>$Objective</td>
                   <td>$PIC</td>
                   <td>$Detail</td></tr>
                   ";
     $display .= "</tr>\n";
}
$display .= "</table>\n";

//$display .= "<br><table width='560' border='0'><tr><td align='center'><a href='javascript:window.print()'><img src='$image_path/admin/button/s_btn_print_$intranet_session_language.gif' border='0'></a></td></tr></table>";
?>
<table width=560 border=0 cellpadding=2 cellspacing=1>
<tr><td colspan='2'><u><?= $i_StudentAttendance_Outing?></u></td></tr>
<tr><td><?=$i_UserStudentName?>:</td><td><?=$StudentName?></td></tr>
<tr><td><?=$i_SmartCard_DateRecorded?>:</td><td><?php echo $targetDate ?></td></tr>
</table>

<?=$display?>

<?
intranet_closedb();
?>
