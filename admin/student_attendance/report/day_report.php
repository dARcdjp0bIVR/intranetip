<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libcardstudentattend.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();


$lclass = new libclass();
$classes = $lclass->getClassList();
$select_class = getSelectByArray($classes,"name=ClassID",$ClassID,1);

$lcard = new libcardstudentattend();

$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";

$result = $lcard->retrieveDayClassRecord($ClassID,$targetDate,$slot);
$outing = $lcard->retrieveClassDateOutingRecord($ClassID,$targetDate);

$display = "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>\n";
$display .= "<tr><td><img src='$image_path/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>";
$display .= "<tr><td class=admin_bg_menu>$toolbar</td></tr>";
$display .= "<tr><td>";

$display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$i_ClassNameNumber</td>";
for ($i=0; $i<sizeof($slot); $i++)
{
     if ($slot[$i] == 1)
     {
         $display .= "<td>$i_StudentAttendance_Slot_AM</td>";
     }
     if ($slot[$i] == 2)
     {
         $display .= "<td>$i_StudentAttendance_Slot_PM</td>";
     }
     if ($slot[$i] == 3)
     {
         $display .= "<td>$i_StudentAttendance_Slot_AfterSchool</td>";
     }
}


for ($i=0; $i<sizeof($result); $i++)
{
        $css = ($i%2==0? "":"2");

     list ($studentID, $studentName, $className, $classNumber, $time1, $type1, $min1, $time2, $type2, $min2, $time3, $type3, $min3) = $result[$i];
     if ($outing[$studentID] != "")
     {
         $outLink = "<a href=javascript:newWindow('outing_view.php?StudentID=$studentID&targetDate=$targetDate',2)>[$i_StudentAttendance_Outing]</a>";
     }
     else
     {
         $outLink = "";
     }
     $display .= "<tr class=tableContent$css><td>$studentName $outLink</td><td>($className - $classNumber)</td>";
     for ($j=1; $j<=sizeof($slot); $j++)
     {
          $time = ${"time$j"};
          $type = ${"type$j"};
          $min = ${"min$j"};

          if ($type === "No record")
          {
                                $background = "#E5D8CA";
                                $textcolor = "#797979";
                                $str = "$i_StudentAttendance_Report_NoRecord";
          }
          else if ($type === 0)
          {
                                $background = "#FFFFFF";
                                $textcolor = "#000000";
                                $str = "$time";
          }
          else if ($type === 1)
          {
                                $background = "#D9F0C2";
                                $textcolor = "#3E7608";
                                $str = "$i_StudentAttendance_Status_Absent";
          }
          else if ($type === 2)
          {
                                $background = "#FDC2B4";
                                $textcolor = "#C62E2E";
                                $str = "$time ($i_StudentAttendance_MinLate: $min)";
          }
          else if ($type === 3)
          {
                                $background = "#B4D5FD";
                                $textcolor = "#036AD3";
                                $str = "$time ($i_StudentAttendance_MinEarly: $min)";
          }
          else
          {
                                $background = "#E5D8CA";
                                $textcolor = "#797979";
                                $str = "$i_StudentAttendance_Report_NoRecord";
          }

/*
          switch ($type)
          {
                  case 0:      # On time
                        $str = "$time";
                        break;
                  case 1:      # Absent
                        $str = "$i_StudentAttendance_Status_Absent";
                        break;
                  case 2:      # Late
                        $str = "$time ($i_StudentAttendance_MinLate: $min)";
                        break;
                  case 3:      # Early leave
                        $str = "$time ($i_StudentAttendance_MinEarly: $min)";
                        break;
                  case "NO": # No record
                        $str = "$i_StudentAttendance_Report_NoRecord";
                        break;
                  default:
                        $str = "default";
          }
*/
          $display .= "<td bgcolor='$background'><font color='$textcolor'>$str</font></td>";
     }
     $display .= "</tr>\n";
}
$display .= "</table>\n";

$display .= "</td></tr></table>\n";

$display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
$display .= "<tr><td><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr>";
$display .= "</table><br>\n";

?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Report,'index.php',$i_StudentAttendance_Report_Daily,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
             newWindow("day_reportprint.php?<?php echo $HTTP_SERVER_VARS['QUERY_STRING'] ?>",4);
}
-->
</script>

<form action=day_report.php method=GET>
<table width=95% border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?="$button_select $i_ClassName"?>:</td><td><?=$select_class?></td></tr>
<tr><td align=right><?=$i_StudentAttendance_View_Date?>:</td><td><input type=text name=targetDate size=10 value='<?=$targetDate?>'></td></tr>
        <tr>
          <td colspan=2 align=right>
          <?=btnSubmit()?> <?=btnReset()?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
        </tr>
</table>

<?=$display?>

<? for ($i=0; $i<sizeof($slot); $i++) {
?>
<input type=hidden name=slot[] value="<?=$slot[$i]?>">
<? } ?>
</form>
<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
