<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycle.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$li = new libdb();
$SpecialSlotID = (is_array($SpecialSlotID)? $SpecialSlotID[0]:$SpecialSlotID);
$sql = "SELECT DayValue, SlotStart, SlotEnd, SlotBoundary FROM CARD_STUDENT_SLOT_SPECIAL WHERE SpecialSlotID = $SpecialSlotID";
$temp = $li->returnArray($sql,4);
list($DayValue , $SlotStart, $SlotEnd, $SlotBound) = $temp[0];

$lc = new libcycle();
$cycle_days = $lc->CycleNum;

if ($DayValue <= 6)
{
    $dayTypeStr = $i_DayType0[$DayValue];
}
else
{
    $cycle_name_array = ${"i_DayType".$lc->CycleType};
    $dayTypeStr = $cycle_name_array[$DayValue-7];
}


$slotname = "";
switch ($SlotID)
{
        case 1: $slotname = $i_StudentAttendance_Slot_AM; break;
        case 2: $slotname = $i_StudentAttendance_Slot_PM; break;
        case 3: $slotname = $i_StudentAttendance_Slot_AfterSchool; break;
}

?>
<form name="form1" method="POST" action="special_edit_update.php">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_TimeSlotSettings,'index.php',$i_StudentAttendance_Slot_Special." ($slotname)",'javascript:history.back()',$button_edit,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_StudentAttendance_Slot_Special; ?>:</td><td><?=$dayTypeStr?></td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_Start?>:</td><td><input type=text name=SlotStart size=10 value='<?=$SlotStart?>'>(HH:mm:ss - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_End?>:</td><td><input type=text name=SlotEnd size=10 value='<?=$SlotEnd?>'>(HH:mm:ss - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_Boundary?>:</td><td><input type=text name=SlotBound size=10 value='<?=$SlotBound?>'>(HH:mm:ss - 24 hrs)</td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=SpecialSlotID value="<?=$SpecialSlotID?>">
<input type=hidden name=SlotID value="<?=$SlotID?>">

</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
