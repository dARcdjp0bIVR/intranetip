<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$sql = "SELECT SUBSTRING(CardID,3) , UserID,UserLogin
        FROM INTRANET_USER
                                        WHERE
                                                RecordType = 2 AND RecordStatus = 1
                                                        AND CardID IS NOT NULL
                                                        AND UserID IS NOT NULL
                                                        AND UserLogin IS NOT NULL
                                                ORDER BY ClassName, ClassNumber, EnglishName
                ";

//$x = "Card ID,User ID,User Login\n";
$content = "";
$result = $li->returnArray($sql,3);
for ($i=0; $i<sizeof($result); $i++)
{
     $content .= ($i+1)."";
     $delim = ",";
     for ($j=0; $j<sizeof($result[$i]); $j++)
     {
          $content .= $delim.$result[$i][$j];
     }
     $content .= "\r\n";
}

// Output the file to user browser
$filename = "studentinfo_950e.csv";
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($content) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $content;
intranet_closedb();
?>