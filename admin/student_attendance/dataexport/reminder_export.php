<?
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();
$namefield = getNameFieldForRecord("c.");
$sql = "SELECT b.CardID, a.DateOfReminder, $namefield, a.Reason
        FROM CARD_STUDENT_REMINDER as a
             LEFT OUTER JOIN INTRANET_USER as b
                  ON a.StudentID = b.UserID AND b.RecordType = 2 AND b.RecordStatus = 1
             LEFT OUTER JOIN INTRANET_USER as c
                  ON a.TeacherID = c.UserID AND c.RecordType = 1 AND c.RecordStatus = 1
         WHERE
              b.UserID IS NOT NULL AND c.UserID IS NOT NULL AND
               a.DateOfReminder >= '$startdate' AND a.DateOfReminder <= '$enddate'
                  ";
$result = $li->returnArray($sql,5);

$x = "CardID,Date,Teacher,Reason\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list($cardid,$remindDate,$teacher,$reason) = $result[$i];
     $x .= "$cardid###$remindDate###$teacher###$reason\n";
}

// Output the file to user browser
$filename = "reminder_pc.csv";
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($x) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");

echo $x;
intranet_closedb();
?>
