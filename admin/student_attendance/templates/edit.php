<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$TemplateID = (is_array($TemplateID)? $TemplateID[0]:$TemplateID);

$li = new libdb();
$sql = "SELECT Title, Content FROM INTRANET_SMS_TEMPLATE WHERE TemplateID = $TemplateID";
$result = $li->returnArray($sql,2);
list($Title, $Content) = $result[0];

?>

<script language="javascript">
function checkform(obj){
     if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateName; ?>.")) return false;
     if(!check_text(obj.Content, "<?php echo $i_alert_pleasefillin.$i_SMS_TemplateContent; ?>.")) return false;
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_MessageTemplate,'javascript:history.back()',$button_edit,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_SMS_TemplateName; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100 value='<?=$Title?>'></td></tr>
<tr><td align=right><?php echo $i_SMS_TemplateContent; ?>:</td><td><TEXTAREA NAME=Content ROWS=5 COLS=30><?=$Content?></TEXTAREA></td></tr>
</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=TemplateID value="<?=$TemplateID?>">
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>