<?
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");

write_file_content($IPList,"$intranet_root/file/stattend_ip.txt");
$st_expiry = trim($st_expiry);
if ($st_expiry=="" || !is_numeric($st_expiry))
{
    $st_expiry = 60;
}
write_file_content($st_expiry,"$intranet_root/file/stattend_expiry.txt");

header("Location: index.php?msg=2");
?>