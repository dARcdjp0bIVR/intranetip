<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
intranet_opendb();

$format_array = array("ClassName","ClassNumber","Date","Teacher","Reason");

$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;
if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
        exit();
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);
                $toprow = array_shift($data);                   # drop the title bar
        }
        for ($i=0; $i<sizeof($format_array); $i++)
        {
             if ($toprow[$i] != $format_array[$i])
             {
                 header("Location: import.php?msg=13");
                 exit();
             }
        }
        $sql = "CREATE TEMPORARY TABLE TEMP_IMPORT_STUDENT_REMINDER (
                 ClassName varchar(20),
                 ClassNumber varchar(20),
                 DateOfReminder date,
                 Teacher varchar(100),
                 Reason text
        )";
        $sql = $li->db_db_query($sql);
        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             list($class,$classnum,$remindDate,$teacher,$reason) = $data[$i];
             if ($class=="" || $classnum=="" || $teacher=="") continue;
             if ($remindDate == "")
             {
                 $remindDateStr = "ADDDATE(CURDATE() , INTERVAL 1 DAY)";
             }
             else
             {
                 $remindDateStr = "'$remindDate'";
             }
             $values .= "$delim ('$class','$classnum',$remindDateStr,'$teacher','$reason')";
             $delim = ",";
        }

        $sql = "INSERT INTO TEMP_IMPORT_STUDENT_REMINDER (ClassName,ClassNumber,DateOfReminder,Teacher,Reason)
                       VALUES $values";
        $li->db_db_query($sql);

        # Insert to Reminder table
        $sql = "INSERT INTO CARD_STUDENT_REMINDER (StudentID, TeacherID, DateOfReminder, Reason, DateInput, DateModified)
                       SELECT
                             b.UserID, c.UserID, a.DateOfReminder, a.Reason, now(), now()
                       FROM TEMP_IMPORT_STUDENT_REMINDER as a
                            LEFT OUTER JOIN INTRANET_USER as b
                                 ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND b.RecordType = 2 AND b.RecordStatus = 1
                            LEFT OUTER JOIN INTRANET_USER as c
                                 ON a.Teacher = c.UserLogin AND c.RecordType = 1 AND c.RecordStatus = 1
                       WHERE b.UserID IS NOT NULL AND c.UserID IS NOT NULL
                       ";
        $li->db_db_query($sql);
}
intranet_closedb();

header("Location: index.php?msg=1");
?>