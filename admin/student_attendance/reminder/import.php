<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");


?>

<form name="form1" method="POST" action="import_update.php" enctype="multipart/form-data">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Reminder,'index.php',$button_import,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_select_file; ?>:</td><td><input type=file size=50 name=userfile></td></tr>
<tr><td align=right nowrap></td><td>
<br><?=$i_StudentAttendance_Reminder_ImportFileDescription?>
<br><br><a class=functionlink_new href=sample.csv target=_blank><?=$i_general_clickheredownloadsample?></a>
</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif"></td></tr>
</table>
</blockquote>
</form>

<?
include_once("../../../templates/adminfooter.php");
?>
