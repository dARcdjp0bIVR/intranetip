<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libcycle.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

#$lclass = new libclass();
#$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()",$ClassName);

if ($slot != 2 && $slot != 3) $slot = 1;

$setting_file = "$intranet_root/file/attend_st_settings.txt";
if (!is_file($setting_file))
{
     $default_content = "1,06:00,09:00,07:40\n1,12:30,14:30,13:30\n1,15:00,18:00,16:00";
     write_file_content($default_content,$setting_file);
}
$file_content = get_file_content($setting_file);
$lines = explode("\n",$file_content);
for ($i=0; $i<sizeof($lines); $i++)
{
     $settings[$i] = explode(",",$lines[$i]);
}
if ($settings[0][0] != 1 && $slot == 1)
{
    $slot = 2;
}
if ($settings[1][0] != 1 && $slot == 2)
{
    $slot = 3;
}
if ($settings[2][0] != 1 && $slot == 3)
{
    $slot = 1;
}
$select_slot = "<SELECT name=slot onChange=this.form.submit()>\n";
if ($settings[0][0]==1)
$select_slot .= "<OPTION value=1 ".($settings[0][0]==1 && $slot==1? "SELECTED":"").">$i_StudentAttendance_Slot_AM</OPTION>\n";
if ($settings[1][0]==1)
$select_slot .= "<OPTION value=2 ".($settings[1][0]==1 && $slot==2? "SELECTED":"").">$i_StudentAttendance_Slot_PM</OPTION>\n";
if ($settings[2][0]==1)
$select_slot .= "<OPTION value=3 ".($settings[2][0]==1 && $slot==3? "SELECTED":"").">$i_StudentAttendance_Slot_AfterSchool</OPTION>\n";
$select_slot .= "</SELECT>\n";

$lc = new libcycle();
$lc->DateToday = strtotime($date);
$cycle_days = $lc->CycleNum;
$weekday = date("w",$lc->DateToday);
$conds = " DayValue = '$weekday'";
if ($cycle_days != 0)       # Count cycle first
{
    $cycleday = $lc->getCycleNumber($cycle_days);
    $conds .= " OR DayValue = '".($cycleday+7)."'";
}
$li = new libdb();
$sql = "SELECT DayValue, SlotStart, SlotEnd, SlotBoundary FROM CARD_STUDENT_SLOT_SPECIAL WHERE SlotID = $slot AND $conds ORDER BY DayValue DESC";
$temp = $li->returnArray($sql,4);
list($temp_dayvalue , $temp_start,$temp_end,$temp_bound) = $temp[0];
if ($temp_start != "" && $temp_end != "" && $temp_bound != "")
{
    $start = $temp_start;
    $end = $temp_end;
    $bound = $temp_bound;
    if ($temp_dayvalue <= 6)
    {
        $dayTypeStr = $i_DayType0[$temp_dayvalue];
    }
    else
    {
        $cycle_name_array = ${"i_DayType".$lc->CycleType};
        $dayTypeStr = $cycle_name_array[$temp_dayvalue-7];
    }
    $special_str = "<tr><td align=right nowrap></td><td><font color=green>$i_StudentAttendance_Slot_Special_Today1 $dayTypeStr $i_StudentAttendance_Slot_Special_Today2</font></td></tr>\n";
}
else
{
    $start = $settings[$slot-1][1];
    $end = $settings[$slot-1][2];
    $bound = $settings[$slot-1][3];
}

?>

<script language="javascript">
function submit2update(){
        document.form1.action='archive_update.php';
        parent.intranet_admin_menu.runStatusWin("/admin/pop_processing.php");
        return;
}
</script>


<form name="form1" method="GET" action="">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_ArchiveTodayRecord,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td align=right nowrap><?php echo $i_StudentAttendance_Slot; ?>:</td><td><?=$select_slot?></td></tr>
<?=$special_str?>
<tr><td align=right nowrap><?=$i_StudentAttendance_Archive_Date?>:</td><td><?=$date?><input type=hidden name=date value="<?=$date?>"></td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_Start?>:</td><td><input type=text name=start size=10 value='<?=$start?>'>(HH:mm - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=$i_StudentAttendance_Slot_End?>:</td><td><input type=text name=end size=10 value='<?=$end?>'>(HH:mm - 24 hrs)</td></tr>
<tr><td align=right nowrap><?=($slot==3?"$i_StudentAttendance_LeaveSchoolTime":"$i_StudentAttendance_ToSchoolTime")?>:</td><td><input type=text name=bound size=10 value='<?=$bound?>'>(HH:mm - 24 hrs)</td></tr>
<tr><td></td><td><input type=image src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" onClick="submit2update()"></td></tr>
</table>
</form>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>