<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();


$li = new libdb();
$lf = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

$sql = "DROP TABLE TEMP_CARD_STUDENT_LOG";
$li->db_db_query($sql);
$sql = "CREATE TABLE TEMP_CARD_STUDENT_LOG (
 RecordedTime datetime,
 SiteName varchar(255),
 CardID varchar(255),
 ClassName varchar(100),
 ClassNumber varchar(100),
 UserID varchar(100)
)";
$li->db_db_query($sql);



if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php?failed=2");
} else {
        $ext = strtoupper($lf->file_ext($filename));
        if($ext == ".CSV") {
                # read file into array
                # return 0 if fail, return csv array if success
                $data = $lf->file_read_csv($filepath);

                        array_shift($data);                   # drop the title bar
        }

        $values = "";
        $delim = "";
        for ($i=0; $i<sizeof($data); $i++)
        {
             if ($format == 2)
             {
                 list($time,$site,$class,$classnum) = $data[$i];
                 $values .= "$delim('$time','$site','$class','$classnum')";
                 $delim = ",";
             }
              else if ($format == 3){
                                // for offline reader format
                                // format:                time,site,student id
                                // e.g:                        2004-11-03 11:26:55,Office,21013
                                if ($data[$i]<>""){
                                         if ($data[$i][0]<>NULL AND $data[$i][1]<>NULL AND $data[$i][2]<>NULL){
                                                 list($time,$site,$UserID) = $data[$i];

                                                 $values .= "$delim('$time','$site','$UserID')";
                                                 $delim = ",";
                                         }
                                }
                        }
             else
             {
                 list($time,$site,$cardid) = $data[$i];
                 $values .= "$delim('$time','$site','$cardid')";
                 $delim = ",";
             }
        }

        if ($format==2)
        {
            $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordedTime,SiteName,ClassName,ClassNumber) VALUES $values";
        }
         else if ($format==3)
                {
            $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordedTime,SiteName,UserID) VALUES $values";
                }
        else
        {
            $sql = "INSERT INTO TEMP_CARD_STUDENT_LOG (RecordedTime,SiteName,CardID) VALUES $values";
        }
        $li->db_db_query($sql);

}

$name_field = getNameFieldByLang("b.");
$sql = "SELECT a.RecordedTime, a.SiteName, IF(a.CardID IS NULL, b.CardID, a.CardID), $name_field, b.ClassName, b.ClassNumber
        FROM TEMP_CARD_STUDENT_LOG as a
             LEFT OUTER JOIN INTRANET_USER as b ON b.RecordType = 2 AND
                  ((a.CardID IS NOT NULL AND a.CardID = b.CardID) OR (a.CardID IS NULL AND a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber) OR (a.UserID IS NOT NULL AND a.UserID = b.UserID  ) )";
$result = $li->returnArray($sql,6);

$display = "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>\n";
$display .= "<tr><td><img src='$image_path/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>";
#$display .= "<tr><td class=admin_bg_menu>$toolbar</td></tr>";
$display .= "<tr><td>";

$display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr><td>$i_SmartCard_TimeRecorded</td><td>$i_SmartCard_Site</td><td>$i_SmartCard_CardID</td><td>$i_UserStudentName</td><td>$i_UserClassName</td><td>$i_UserClassNumber</td></tr>";
for ($i=0; $i<sizeof($result); $i++)
{
     list ($time,$site,$cardid,$name,$class,$classnum) = $result[$i];
     $display .= "<tr><td>$time</td><td>$site</td><td>$cardid</td><td>$name</td><td>$class</td><td>$classnum</td></tr>\n";
}
$display .= "</table>\n";
$display .= "</td></tr></table>\n";

$display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
$display .= "<tr><td><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr>";
$display .= "</table><br>\n";

include_once("../../../templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_CardLog,'index.php',$button_import,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<form name="form1" method="POST" action="import_update_confirm.php">
<?=$display?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr>
<td><?=$i_StudentAttendance_ImportConfirm?><br>
<?=$i_StudentAttendance_ImportCancel?><br>
</td></tr>
<tr><td align="right">
<a href=javascript:history.back()><img src="<?=$image_path?>/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border=0></a>
<input type=image src="<?=$image_path?>/admin/button/s_btn_import_<?=$intranet_session_language?>.gif">
</td>
</tr>
</table>

</form>
<?

intranet_closedb();

include_once("../../../templates/adminfooter.php");
?>