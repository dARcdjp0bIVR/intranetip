<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libclass.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

#$lclass = new libclass();
#$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.submit()",$ClassName);

if ($slot != 2 && $slot != 3) $slot = 1;
$target = $date;
$targetStart = "$target $start";
$targetEnd = "$target $end";
#$targetBound = "$target $bound";
#$boundtime = strtotime($targetBound);

$currentAcademicYear = getCurrentAcademicYear();
$currentSemester = getCurrentSemester();

#$toolbar = "<a class=iconLink href=javascript:openPrintPage()>".printIcon().$i_PrinterFriendlyPage."</a>";

if ($slot == 3)
{
    $sql_func = "MAX";
    $time_str = "$i_StudentAttendance_Time_Departure";
    $min_str = "$i_StudentAttendance_MinEarly";
    #$sql_min = "CEILING(($boundtime - UNIX_TIMESTAMP(b.RecordedTime))/60)";
    $lateEarlyType = 3;
    $eclassSlot = 1;               # Assume WHOLE DAY if early leave
    $RecordType = 2;               # Leave school
    $lateEL_status_str = "$i_StudentAttendance_Status_EarlyLeave";
}
else
{
    $sql_func = "MIN";
    $time_str = "$i_StudentAttendance_Time_Arrival";
    $min_str = "$i_StudentAttendance_MinLate";
    #$sql_min = "CEILING((UNIX_TIMESTAMP(b.RecordedTime) - $boundtime)/60)";
    $lateEarlyType = 2;
    $RecordType = 1;               # Go to school
    if ($slot==1)
    {
        $eclassSlot = 2;
    }
    else if ($slot==2)
    {
         $eclassSlot = 3;
    }
    $lateEL_status_str = $i_StudentAttendance_Status_Late;
}
$li = new libdb();

$list = implode(",",$StudentID);

if ($targetClass != "")
{
    $class_cond = " AND a.ClassName = '$targetClass'";
}
else
{
    $class_cond = "";
}
$namefield = getNameFieldByLang("a.");
$sql = "SELECT a.UserID, $namefield, a.ClassName, a.ClassNumber FROM INTRANET_USER as a WHERE a.UserID IN ($list) $class_cond ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";
$students = $li->returnArray($sql,4);
$values = "";
$delim = "";
for ($i=0; $i<sizeof($students); $i++)
{
     list($uid, $name, $class, $classnum) = $students[$i];
     $logTime = ${"marktime$uid"};
     $boundTime = ${"bound$uid"};
     $boundts = strtotime("$date $boundTime");
     if ($RecordType == 1)
     {
         $min_differ = $logTime - $boundts;
         if ($min_differ > 0)
             $min_differ = ceil($min_differ/60);
         else $min_differ = 0;
     }
     else     # Leave
     {
         $min_differ = $boundts - $logTime;
         if ($min_differ > 0)
             $min_differ = ceil($min_differ/60);
         else $min_differ = 0;
     }
     $strLogTime = ($logTime==''? "NULL":"'".date('Y-m-d H:i:s',$logTime)."'");

     # INSERT to DAILY RECORD
     $sql = "INSERT INTO CARD_STUDENT_DAILYSLOTRECORD (UserID, RecordDate, SlotID, RecordedTime, MinDiffer, DateInput, DateModified)
             VALUES ('$uid','$date','$slot',$strLogTime,'$min_differ',now(),now())";
     $li->db_db_query($sql);
     if ($li->db_affected_rows()!=1)
     {
         # Call Update
         $sql = "UPDATE CARD_STUDENT_DAILYSLOTRECORD SET RecordedTime = $strLogTime,MinDiffer='$min_differ',DateModified=now()
                 WHERE UserID = $uid AND RecordDate = '$date' AND SlotID = '$slot'";
         $li->db_db_query($sql);
     }

     if ($logTime == "")
     {
         # Mark Absence
         if ($slot!=3)
         {
             $sql = "SELECT COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE WHERE Year = '$currentAcademicYear' AND Semester = '$currentSemester' AND AttendanceDate = '$date' AND UserID = $uid AND RecordType = 1 AND DayType = $eclassSlot";
             $result = $li->returnVector($sql);
             if ($result[0]==0)
             {
                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE (UserID, AttendanceDate,Year, Semester, DayType, RecordType, DateInput, DateModified)
                         VALUES ($uid, '$date','$currentAcademicYear','$currentSemester',$eclassSlot, 1,now(),now())";
                 $li->db_db_query($sql);
             }
             else
             {
                 # No work to do for already exists
             }
             # Remove Late or Early leave record
             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate = '$date' AND DayType='$eclassSlot' AND RecordType != 1 AND UserID = '$uid'";
             $li->db_db_query($sql);
         }
     }
     else
     {
         if ($min_differ > 0)
         {
             $status = $lateEL_status_str;
             # Late/Early Leave
             $sql = "SELECT COUNT(StudentAttendanceID) FROM PROFILE_STUDENT_ATTENDANCE WHERE Year = '$currentAcademicYear' AND Semester = '$currentSemester' AND AttendanceDate = '$date' AND UserID = $uid AND RecordType = $lateEarlyType AND DayType = $eclassSlot";
             $result = $li->returnVector($sql);
             if ($result[0]==0)
             {
                 $sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE (UserID, AttendanceDate,Year,Semester,DayType, RecordType, DateInput, DateModified)
                         VALUES ($uid, '$date','$currentAcademicYear','$currentSemester' ,$eclassSlot,$lateEarlyType,now(),now())";
                 $li->db_db_query($sql);
             }
             else
             {
                 # No work to do for already exists
             }
             # Remove Absence Record
             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE AttendanceDate = '$date' AND DayType='$eclassSlot' AND RecordType = 1 AND UserID = '$uid'";
             $li->db_db_query($sql);
         }
         else     # On time
         {
             $status = "$i_StudentAttendance_Status_OnTime";
             # Remove Records
             $sql = "DELETE FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID = $uid AND Year = '$currentAcademicYear' AND Semester = '$currentSemester' AND DayType = $eclassSlot";
             $li->db_db_query($sql);
         }
     }


}



$namefield = getNameFieldByLang("a.");
$sql = "SELECT a.UserID, $namefield, a.CardID,a.ClassName,a.ClassNumber,
        IF(b.RecordedTime IS NULL,'--',b.RecordedTime),
        b.MinDiffer
        FROM INTRANET_USER as a LEFT OUTER JOIN CARD_STUDENT_DAILYSLOTRECORD as b ON a.UserID = b.UserID AND b.RecordDate = '$date' AND b.SlotID = '$slot'
        WHERE a.RecordType = 2 AND a.UserID IN ($list)
             ORDER BY a.ClassName, a.ClassNumber, a.EnglishName";


$dailyrecords = $li->returnArray($sql,7);
$x = "";
for ($i=0; $i<sizeof($dailyrecords); $i++)
{
        $css = ($i%2==0? "":"2");
        list($uid,$name,$cardid,$ClassName,$ClassNumber, $inTime, $minLate) = $dailyrecords[$i];

     if ($minLate < 0 || $minLate == "") $minLate = 0;
     $status = "";
     if ($inTime == "--")
     {
                $status = "$i_StudentAttendance_Status_Absent";
                $background = "#D9F0C2";
                $textcolor = "#3E7608";
     }
     if ($minLate > 0)
     {
                if ($RecordType==1){
                $status = "$i_StudentAttendance_Status_Late";
                        $background = "#FDC2B4";
                        $textcolor = "#C62E2E";
                } else {
                $status = "$i_StudentAttendance_Status_EarlyLeave";
                        $background = "#B4D5FD";
                        $textcolor = "#036AD3";
                }
     }
     if ($status == "")
     {
                $status = "$i_StudentAttendance_Status_OnTime";
                $background = "#FFFFFF";
                $textcolor = "#000000";
     }
     $displayMinLate = ($minLate == 0? "--":round($minLate));

     $dbInTime = ($inTime == "--"? "NULL":"'$inTime'");

     $class_str = ($ClassName != "" && $ClassNumber != "")? "($ClassName - $ClassNumber)":"";
     $x .= "<tr bgcolor='$background'><td><font color='$textcolor'>$name $class_str</font></td><td><font color='$textcolor'>$inTime</font></td><td><font color='$textcolor'>$status</font></td><td><font color='$textcolor'>$displayMinLate</font></td></tr>\n";
}

$display = "<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>\n";
$display .= "<tr><td><img src='$image_path/admin/table_head0.gif' width='560' height='13' border='0'></td></tr>";
$display .= "<tr><td class=admin_bg_menu>$toolbar</td></tr>";
$display .= "<tr><td>";

$display .= "<table width=100% border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0>\n";
$display .= "<tr class='tableTitle'><td>$i_UserStudentName</td><td>$time_str</td><td>$i_StudentAttendance_Status</td><td>$min_str</td></tr>\n";

$display .= $x;
$display .= "</table>\n";

$display .= "</td></tr></table>\n";

$display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>";
$display .= "<tr><td><img src='/images/admin/table_bottom.gif' width='560' height='16' border='0'></td></tr>";
$display .= "</table><br>\n";
?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_Action_SpecialArchive,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>
<font color=green><?=$i_StudentAttendance_con_RecordArchived?></font>
<p></p>
<?=$display?>

<script language="JavaScript" type="text/javascript">
<!--
function openPrintPage()
{
             newWindow("archive_updateprint.php?date=<?=$date?>&start=<?=$start?>&end=<?=$end?>&bound=<?=$bound?>&slot=<?=$slot?>&targetClass=<?=$targetClass?>",4);
}

// close status window
//parent.intranet_admin_menu.runStatusWin("");
-->
</script>

<?
include_once("../../../templates/adminfooter.php");
intranet_closedb();
?>
