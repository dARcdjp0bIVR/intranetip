<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();


?>
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
     </script>

     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['remove'].RemoveFrom.value = dateValue;
          }
          function calendarCallback2(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['remove'].RemoveTo.value = dateValue;
          }
     // -->
     </script>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_StudentAttendance_System, '../',$i_StudentAttendance_CardLog,'index.php',$i_StudentAttendance_RemoveByDateRange,'') ?>
<?= displayTag("head_smartcardattendence_$intranet_session_language.gif", $msg) ?>

<form name="remove" method="post" action="range_remove.php" ONSUBMIT="return checkRemoveForm(this)">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<script LANGUAGE=Javascript>
function checkRemoveForm(obj)
{

         if (!check_date(obj.RemoveFrom,"<?php echo $i_invalid_date; ?>")) return false;
         if (!check_date(obj.RemoveTo,"<?php echo $i_invalid_date; ?>")) return false;
         if (obj.action_type[0].checked) return true;
         return confirm('<?=$i_Usage_RemoveConfirm?>');
}
</script>
<?=$i_SmartCard_Instruction_RemoveLog?> <br>
<?=$i_From?>: <input type=text name=RemoveFrom value='<?=$specialStart?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span><br>
<?=$i_To?>: <input type=text name=RemoveTo value='<?=$specialStart?>' size=10><script language="JavaScript" type="text/javascript">
    <!--
         endCal = new dynCalendar('endCal', 'calendarCallback2', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
<br>
<input type=radio name=action_type value=1 CHECKED><?=$i_SmartCard_DownloadOnly?>
<input type=radio name=action_type value=2><?=$i_SmartCard_RemoveOnly?>
<input type=radio name=action_type value=3><?=$i_SmartCard_DownloadRemove?>
<br><br>

<ol style="text-color: green">
<li><?=$i_SmartCard_FromToIncluded?></li>
<li><?=$i_SmartCard_DownloadLogFormat?></li>
</ol>

</font>
</blockquote>
</td>
</tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif" border="0">
<a href="javascript:document.remove.reset()"><img src='/images/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>