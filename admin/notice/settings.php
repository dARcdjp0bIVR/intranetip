<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libnotice.php");
include_once("../../includes/libgroup.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lgroup = new libgroup();
$lnotice = new libnotice();

$disableChecked = ($lnotice->disabled==1? "CHECKED":"");
$adminGroups = $lgroup->returnGroupsByCategory(1);
$fullGroupSelect = getSelectByArray($adminGroups,"name=fullGroup",$lnotice->fullAccessGroupID,2,0);
$normalGroupSelect = getSelectByArray($adminGroups,"name=normalGroup",$lnotice->normalAccessGroupID,2,0);
$classChecked = ($lnotice->isClassTeacherEditDisabled==1? "CHECKED":"");
$allAllowChecked = ($lnotice->isAllAllowed==1? "CHECKED":"");
$defaultNum = $lnotice->defaultNumDays;
$enableViewAllChecked = ($lnotice->showAllEnabled==1?"CHECKED":"");

# 20090306
$DisciplineGroupSelect = getSelectByArray($adminGroups,"name=DisciplineGroup",$lnotice->DisciplineGroupID,2,0);
?>
<form name="form1" action="update.php" method="post">
<?= displayNavTitle($i_admintitle_fs, '', $i_Notice_ElectronicNoticeSettings, '') ?>
<?= displayTag("head_notice_$intranet_session_language.gif", $msg) ?>
<blockquote><blockquote>
<table width=400 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=3>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Disable?></td><td align=center><input type=checkbox name=disabled value=1 <?=$disableChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_NormalControlGroup?></td><td align=center><?=$normalGroupSelect?><br>(<?=$i_Notice_Setting_AdminGroupOnly?>)</td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_FullControlGroup?></td><td align=center><?=$fullGroupSelect?><br>(<?=$i_Notice_Setting_AdminGroupOnly?>)</td></tr>

<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_DisciplineGroup?></td><td align=center><?=$DisciplineGroupSelect?>(<?=$i_Notice_Setting_AdminGroupOnly?>)</td></tr>

<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_DisableClassTeacher?></td><td align=center><input type=checkbox name=classteacher value=1 <?=$classChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_AllHaveRight?></td><td align=center><input type=checkbox name=allallow value=1 <?=$allAllowChecked?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_DefaultNumDays?></td><td align=center><input type=text name=numDays value=<?=$defaultNum?>></td></tr>
<tr><td style="vertical-align:middle" class=tableTitle_new><?=$i_Notice_Setting_ParentStudentCanViewAll?></td><td align=center><input type=checkbox name=enableViewAll value=1 <?=$enableViewAllChecked?>></td></tr>
</table>
</blockquote></blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="index.php"><img src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border="0"></a>
</td>
</tr>
</table>
</form>


<?php
include("../../templates/adminfooter.php");
?>