<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();
$FormID = array_unique($FormID);
$FormID = array_values($FormID);
for ($i=0; $i<sizeof($FormID); $i++)
{
     $x .= "<input type=hidden name=FormID[] value=".$FormID[$i].">\n";
}
?>

<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../index.php', $i_SettingsSchool_Forms, 'javascript:history.back()', $button_remove, '') ?>
<?= displayTag("head_school_form_$intranet_session_language.gif", $msg) ?>

<form name=form1 action=remove_update.php method=post>
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=0>
<tr><td><?=$i_Form_RemovalAlert?></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="35" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type=image src='/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif' border='0'>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<?=$x?>
<input type=hidden name=filter value=<?=$filter?>>
<input type=hidden name=status value=<?=$status?>>
</form>


<?php
include_once("../../../templates/adminfooter.php");
?>