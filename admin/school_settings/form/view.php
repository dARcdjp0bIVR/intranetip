<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libform.php");
include_once("../../../lang/lang.$intranet_session_language.php");
$body_tags = "bgcolor='#FFFFFF'";
include_once("../../../templates/fileheader.php");

intranet_opendb();
$lform = new libform($FormID);
#$queString = $lform->queString;
#$queString = str_replace('"','&quot;',$queString);
$queString = $lform->getConvertedQuestion();
?>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/_form_edit.js"></script>
<form name="ansForm" method="post" action="update.php">
        <input type=hidden name="qStr" value="">
        <input type=hidden name="aStr" value="">
</form>
<script language="Javascript">
answer_sheet="網上報名表";
answersheet_template="範本";
answersheet_header="問題 / 標題";
answersheet_no="題數";
//answersheet_order="";
answersheet_type="問題種類";
//answersheet_mark="";
answersheet_maxno="問題數目已超越已選擇指數極限: 英文字母為 'a - z';  羅馬數字為 'i - x'!";
no_options_for = "不用為此類問題選擇數目!";
pls_specify_type = "請選定問題種類!";
pls_fill_in = "請輸入內容!";
chg_title = "更改問題/標題:";

answersheet_tf="是非項";
answersheet_mc="多項選擇";
answersheet_mo="可選多項";
answersheet_sq="自由填寫";
//answersheet_on="Likert Score";
answersheet_option="選項數目";

button_submit="呈送";
button_quit="離開";
button_add="增加";


background_image = "/images/layer_bg.gif";

var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString="<?=$queString?>";
//edit submitted application
sheet.mode=1;
sheet.answer=sheet.sheetArr();
//sheet.templates=form_templates;
document.write(editPanel());
</script>
<hr width=90%>
<form>

<table width=500 border=0>
<tr><td align=center><a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a></td></tr>
</table>
</form>
<?php
include_once("../../../templates/filefooter.php");
?>