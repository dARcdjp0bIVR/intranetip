<?php

#########################################################
#
#	Date:	2011-08-23	YatWoon
#			"form_edit.js" is updated as IP25 version so that admin console doesn't support, and need changed to use old version .js
#
#########################################################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();

$FormID = (is_array($FormID)? $FormID[0]: $FormID);
$lf = new libform($FormID);
$form_templates = $lf->getTemplatesInJS();
$typeSelect = $lf->getTypeSelect("name=type",$lf->recordType);
$formtypeSelect = $lf->getFormTypeSelect("formtype",$lf->formType);
$statusSelect = $lf->getStatusSelect("status",$lf->recordStatus);
?>
<SCRIPT LANGUAGE=javascript SRC=/templates/forms/_form_edit.js></SCRIPT>
<SCRIPT LANGUAGE=javascript SRC=/templates/forms/layer.js></SCRIPT>

<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../index.php', $i_SettingsSchool_Forms, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_school_form_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=450 border=0 cellpadding=3 cellspacing=0>
<form name=ansForm>
<tr><td align=right nowrap><?php echo $i_Form_Name; ?>:</td><td><input class=text type=text name=formname size=30 maxlength=100 value="<?=$lf->FormName?>"><BR><span class=extraInfo><?=$i_Form_FormNameMustBeUnique?></span></td></tr>
<tr><td align=right nowrap><?php echo $i_Form_Description; ?>:</td><td><textarea name=description cols=30 rows=5><?=$lf->description?></textarea></td></tr>
<input type=hidden name="qStr" value="<?=$lf->getConvertedQuestion()?>">
<input type=hidden name="aStr" value="">
</form>
<tr><td align=right nowrap><?=$i_Form_ConstructForm?>:</td>
<?php
if ($lf->isFormEditable())
{
?>
<td bgcolor=white>
<script language="Javascript">
<?=$form_templates?>
<?=$lf->getWordsInJS()?>

background_image = "/images/layer_bg.gif";

var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString=document.ansForm.qStr.value;
sheet.mode=0;        // 0:edit 1:fill in application
sheet.answer=sheet.sheetArr();
sheet.templates=form_templates;
document.write(editPanel());
</script>
<? }
else
{
echo "&nbsp;</td><td>$i_Form_FormEditNotAllow";
}
?>
</td></tr>
<form name=form1 action=edit_update.php method=post>
<tr><td align=right><?php echo $i_Form_Type; ?>:</td><td><?=$typeSelect?></td></tr>
<tr><td align=right></td><td><?=$formtypeSelect?></td></tr>
<tr><td align=right></td><td><?=$statusSelect?></td></tr>

<input type=hidden name="qStr" value="">
<input type=hidden name="aStr" value="">
<input type=hidden name="formname" value="">
<input type=hidden name="description" value="">
<input type=hidden name="FormID" value="<?=$FormID?>">
</form>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="35" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<a href="javascript:grabInfo();"><image src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'></a>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>


</blockquote>
<SCRIPT LANGUAGE=javascript>
function grabInfo()
{
         if (!check_text(document.ansForm.formname,'<?="$i_alert_pleasefillin$i_Form_Name"?>')) return;
<?php
if ($lf->isFormEditable())
{
?>
         finish();
<? } ?>
         document.form1.qStr.value = document.ansForm.qStr.value;
         document.form1.aStr.value = document.ansForm.aStr.value;
         document.form1.formname.value = document.ansForm.formname.value;
         document.form1.description.value = document.ansForm.description.value;
         document.form1.submit();
}
</SCRIPT>
<?php
include_once("../../../templates/adminfooter.php");
?>