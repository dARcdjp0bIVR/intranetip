<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$formname = intranet_htmlspecialchars(trim($formname));
$description = intranet_htmlspecialchars(trim($description));


$field_name = "FormName, Description, RecordType, FormType, RecordStatus,QueString,DateInput,DateModified";
$field_values = "'$formname','$description','$type','$formtype','$status','$qStr',now(),now()";
$sql = "INSERT INTO INTRANET_FORM ($field_name) VALUES ($field_values)";
$li->db_db_query($sql);

if ($li->db_affected_rows()==0)
{
    $msg = 10;
}
else
{
    $msg = 1;
}
intranet_closedb();
header("Location: index.php?filter=$formtype&status=$status&msg=$msg");
?>