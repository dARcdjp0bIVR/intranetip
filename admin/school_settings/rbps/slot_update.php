<?php
/*
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *
 */

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

# Archive records
if ($reset_booking == 1)
{
//    # Retrieve item list first
//    $sql = "SELECT ResourceID FROM INTRANET_RESOURCE WHERE TimeSlotBatchID = $BatchID";
//    $items = $li->returnVector($sql);
//    $itemlist = implode(",",$items);
//
//    # Archive
//    $conds = "d.TimeSlotBatchID = $BatchID";
//    $sql = "INSERT INTO INTRANET_BOOKING_ARCHIVE
//            (BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
//            SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
//            CONCAT(b.EnglishName, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ),
//            d.ResourceCategory, d.Title, d.ResourceCode,
//            a.RecordStatus, a.TimeApplied, a.LastAction
//            FROM INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d
//            WHERE a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq
//            AND a.BookingDate < CURDATE() AND $conds";
//    $li->db_db_query($sql);
//
//    # Remove
//    $cond2 = "ResourceID IN ($itemlist)";
//    $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE $cond2";
//    $li->db_db_query($sql);

	include_once("../../../includes/librb.php");
	$librb = new librb();
	$librb->Archive_Booking_Record('', $BatchID);
}


$sql = "DELETE FROM INTRANET_SLOT WHERE BatchID = '$BatchID'";
$li->db_db_query($sql);
$insert_values = "";
$i = 0;
while ($i < sizeof($range))
{
       if ($range[$i]=="" && $title[$i]=="")
           break;
       if ($i != 0)
       {
           $insert_values .= ",";
       }
       $slotseq = $i+1;
       $insert_values .= "('".$title[$i]."','".$range[$i]."',$slotseq,'$BatchID'".")";
       $i++;
}
$sql = "INSERT INTO INTRANET_SLOT (Title, TimeRange, SlotSeq, BatchID) values $insert_values";
$li->db_db_query($sql);

$sql = "UPDATE INTRANET_SLOT_BATCH SET Description = '$description' WHERE BatchID = '$BatchID'";
$li->db_db_query($sql);


intranet_closedb();
header("Location: index.php?msg=2&BatchID=$BatchID");
?>