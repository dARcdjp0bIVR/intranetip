<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libbatch.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lb = new libbatch($BatchID);
$BatchID = $lb->BatchID;
$slots = $lb->slots;
$x = "";
for ($i=0; $i<sizeof($slots); $i++)
{
     list($id,$title,$range,$seq) = $slots[$i];
     $x .= "<tr><td align=center><input type=text name=range[] value=\"$range\"></td><td>&nbsp; </td><td align=center><input type=text name=title[] value=\"$title\"></td></tr>\n";
}
$remaining = max(5,15-sizeof($slots));
for ($i=0; $i<$remaining; $i++)
{
     $x .= "<tr><td align=center><input type=text name=range[]></td><td>&nbsp; </td><td align=center><input type=text name=title[]></td></tr>\n";
}

if ($fail == 1)
{
    $update_msg = "<font color=red>$i_Batch_TitleDuplicated</font>";
}
?>

<script language="javascript">

function NewBatch ()
{
                 var obj = document.batchform;
         title = prompt(globalAlertMsgNewBatch,"");
         if (title == "")
         {
             alert ("<?=$i_Batch_TitleNotNull?>");
             return;
         }
         if (title != null && title != "")
         {
             obj.BatchTitle.value = title;
             obj.action = "new_update.php";
             obj.submit();
         }

}

function EditBatch ()
{
                 var obj = document.batchform;
         title = prompt(globalAlertMsgNewBatch,obj.BatchTitle.value);
         if (title == "")
         {
             alert ("<?=$i_Batch_TitleNotNull?>");
             return;
         }
         if (title != null && title != "")
         {
             obj.BatchTitle.value = title;
             obj.action = "edit_update.php";
             obj.submit();
         }
}

function RemoveBatch ()
{
                 var obj = document.batchform;
         if (confirm(globalAlertMsg3))

         {
             obj.action = "remove.php";
             obj.submit();
         }
}

function SetBatch ()
{
                 var obj = document.batchform;
         if (confirm(globalAlertMsgSetBatch))
         {
             obj.action = "set_update.php";
             obj.submit();
         }
}

function EditBookingPeriodID(obj){
     if(check_select(obj.BookingPeriodID, globalAlertMsg18, "")){
          BookingPeriodValue = obj.BookingPeriodID.options[obj.BookingPeriodID.selectedIndex].text;
          BookingPeriodValue = prompt(globalAlertMsg19, BookingPeriodValue);
          if(BookingPeriodValue!=null){
               obj.BookingPeriodTitle.value = BookingPeriodValue;
               obj.action = "new_update.php";
               obj.submit();
          }
     }
}

function RemoveBookingPeriodID(obj){
     if(check_select(obj.BookingPeriodID, globalAlertMsg18, "")){
          if(confirm(globalAlertMsg3)){
               obj.action = "delete.php";
               obj.submit();
          }
     }
}

function checkform(obj)
{
         var msg = (obj.reset_booking.checked? '<?=$i_ResourceSlotChangeAlert?>': '<?=$i_Slot_NotArchiveAlert?>');
         return confirm (msg);
}

function saveform() {
        document.form1.submit();
}
</script>
<?=$update_msg?>

<?= displayNavTitle($i_admintitle_sc, '',$i_adminmenu_sc_school_settings,'/admin/school_settings/', $i_admintitle_sc_period, '') ?>
<?= displayTag("head_rbps_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<p><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr><td colspan=3>
<form name="batchform" action="" method=get>
<?php echo $lb->returnBatchSelect("name=BatchID onChange=\"this.form.submit();\"", $BatchID); ?>
 <a href="javascript:EditBatch()"><img src="/images/admin/button/s_btn_rename_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a>
 <!-- <input type=button class=button value="<?=$button_rename?>" onClick="EditBatch(this.form)"> -->
<?php if ($lb->RecordStatus != 1) {?>
 <a href="javascript:SetBatch()"><img src="/images/admin/button/s_btn_set_default_period_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a>
 <!-- <input type=button class=button value="<?=$i_Batch_SetDefault?>" onClick="SetBatch(this.form)">
 <input type=button class=button value="<?=$button_remove?>" onClick="RemoveBatch(this.form)"> -->
 <a href="javascript:RemoveBatch()"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a>
<?php } ?>
 <a href="javascript:NewBatch()"><img src="/images/admin/button/s_btn_new_<?=$intranet_session_language?>.gif" border="0" align="absmiddle"></a>
 <!-- <input type=button class=button value="<?=$button_new?>" onClick="NewBatch(this.form)"> -->
<input type=hidden name=BatchTitle value="<?=$lb->BatchName?>">
<br>
<span class="extraInfo"><?=$i_Batch_DefaultDescription?></span>
</form>
</td></tr>

<tr><td class=tableTitle_new align=center><u><?=$i_Slot_TimeRange?></u></td><td class=tableTitle_new>&nbsp; </td><td class=tableTitle_new align=center><u><?=$i_Slot_Title?></u></td></tr>
<form name="form1" action="slot_update.php" method="post" ONSUBMIT="return checkform(this)">
<input type=hidden name=BatchID value="<?=$BatchID?>">
<?php echo $x; ?>
<tr><td colspan=3>
<p><br><?=$i_Batch_Description?><br>
<textarea rows=6 cols=70 name=description><?=$lb->Description?></textarea>
</p></td></tr>
<tr><td colspan=3><p><br>
<table border=0 cellpadding=2 cellspacing=0><tr><td><input type=checkbox name=reset_booking value=1></td><td width=100%><?=$i_Slot_ChangeArchiveRecords?></td></tr></table>
</p></td></tr>
</form>
</table>
</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<a href="javascript:saveform()"><img src='/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif' border='0'></a>
<?= btnReset() ?>
<p></p>
</td>
</tr>
</table>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>