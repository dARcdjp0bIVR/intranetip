<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

if ($t!="")
    echo "<p> $i_con_msg_date_wrong </p>\n";
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.SubjectName, "<?php echo $i_alert_pleasefillin.$i_Subject_name; ?>.")) return false;
}
</script>

<form name="form1" action="new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_fs, '', $i_adminmenu_sc_school_settings, 'index.php', $i_admintitle_fs_homework_subject, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_homework_subject_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_Subject_name; ?>:</td><td><input class=text type=text name=SubjectName size=50 maxlength=100></td></tr>
</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>