<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycleperiods.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lc = new libcycleperiods();

# Clear Production table
$lc->clearProduction();

# Copy to production
$lc->makeProduction();

intranet_closedb();
header("Location: index.php?step=3");
?>