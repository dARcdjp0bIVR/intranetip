<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libcycleperiods.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$linterface = new interface_html();
$limport = new libimporttext();

$lc = new libcycleperiods();

$select_periodtype = "<SELECT name=PeriodType onChange=checkPeriodType(this.value)>";
$select_periodtype .= "<OPTION value=0>$i_CycleNew_PeriodType_NoCycle</OPTION>";
$select_periodtype .= "<OPTION value=1>$i_CycleNew_PeriodType_Generation</OPTION>";
$select_periodtype .= "<OPTION value=2>$i_CycleNew_PeriodType_FileImport</OPTION>";
$select_periodtype .= "</SELECT>";

$select_cycletype = "<SELECT name=CycleType onChange=changeFirstDaySelect()>\n";
$select_cycletype .= "<OPTION value=0>$i_CycleNew_CycleType_Numeric</OPTION>";
$select_cycletype .= "<OPTION value=1>$i_CycleNew_CycleType_Alphabetic</OPTION>";
$select_cycletype .= "<OPTION value=2>$i_CycleNew_CycleType_Roman</OPTION>";
$select_cycletype .= "</SELECT>";

$select_days = "<SELECT name=PeriodDays onChange=changeFirstDaySelect()>";
for ($i=1; $i<=26; $i++)
{
     $select_days .= "<OPTION value=$i ".($i==6?"SELECTED":"").">$i</OPTION>";
}
$select_days.= "</SELECT>";

$select_firstday = "<SELECT name=FirstDay>";
for ($i=0; $i<6; $i++)
{
     #$string = $lc->array_numeric[$i]."/".$lc->array_alphabet[$i]."/".$lc->array_roman[$i];
     $string = $lc->array_numeric[$i];
     $select_firstday .= "<OPTION value=$i>$string</OPTION>";
}
$select_firstday .= "</SELECT>";

$txtCalculation = "<table width=100% border=0 cellspacing=0 cellpadding=4>\n";
$txtCalculation .= "<tr><td align=right>$i_CycleNew_Field_CycleType:</td><td>$select_cycletype</td></tr>";
$txtCalculation .= "<tr><td align=right>$i_CycleNew_Field_PeriodDays:</td><td>$select_days</td></tr>";
$txtCalculation .= "<tr><td align=right>$i_CycleNew_Field_FirstDay:</td><td>$select_firstday</td></tr>";
$txtCalculation .= "<tr><td align=right>$i_CycleNew_Field_SaturdayCounted:</td><td><input type=checkbox name=satCount value=1></td></tr>";
$txtCalculation .= "</table>\n";
$txtCalculation = str_replace("\n","<br>",$txtCalculation);
$txtCalculation = str_replace("\r","",$txtCalculation);

$i_CycleNew_Description_FileImport = str_replace("\n","",$i_CycleNew_Description_FileImport);
$i_CycleNew_Description_FileImport = str_replace("\r","",$i_CycleNew_Description_FileImport);
$txtFileImport = "<table width=100% border=0 cellspacing=0 cellpadding=4>\n";
$txtFileImport .= "<tr><td align=right>&nbsp;</td><td><input type=file size=40 name=userfile><br>
". str_replace("'", "\'",$linterface->GET_IMPORT_CODING_CHKBOX()) ."
$i_CycleNew_Description_FileImport
<br>$i_CycleNew_Warning_FileImport<br>
<a class=functionlink_new href=\"".GET_CSV("importsample.csv")."\">$i_general_clickheredownloadsample</a>
</td></tr>";
$txtFileImport .= "</table>\n";
$txtFileImport = str_replace("\n","<br>",$txtFileImport);
$txtFileImport = str_replace("\r","",$txtFileImport);

switch ($error)
{
        case 1: $xmsg = "<font color=red>$i_Booking_EndDateWrong</font>"; break;
        case 2: $xmsg = "<font color=red>$i_CycleNew_Prompt_DateRangeOverlapped</font>"; break;
}
?>
     <link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
     <script LANGUAGE="javascript">
         var css_array = new Array;
         css_array[0] = "dynCalendar_free";
         css_array[1] = "dynCalendar_half";
         css_array[2] = "dynCalendar_full";
         var date_array = new Array;
     </script>

     <script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
     <script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
     <script type="text/javascript">
     <!--
          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['form1'].DateStart.value = dateValue;
          }
          function calendarCallback2(date, month, year)
          {
                   if (String(month).length == 1) {
                       month = '0' + month;
                   }

                   if (String(date).length == 1) {
                       date = '0' + date;
                   }
                   dateValue =year + '-' + month + '-' + date;
                   document.forms['form1'].DateEnd.value = dateValue;
          }
     // -->
     </script>
<script LANGUAGE=Javascript>
function checkform(obj)
{

         if (!check_date(obj.DateStart,"<?php echo $i_invalid_date; ?>")) return false;
         if (!check_date(obj.DateEnd,"<?php echo $i_invalid_date; ?>")) return false;
         if (compareDate(obj.DateStart.value,obj.DateEnd.value)==1)
         {
             alert('<?=$i_Booking_EndDateWrong?>');
             return false;
         }
}
function checkPeriodType(value)
{
         msg = document.getElementById('otherInfo');
         if (value==0)
         {
             msg.innerHTML = '';
         }
         else if (value==1)
         {
             msg.innerHTML = '<?=$txtCalculation?>';
         }
         else if (value==2)
         {
              msg.innerHTML = '<?=$txtFileImport?>';
         }
}
var data_firstday_by_type = new Array();
data_firstday_by_type[0] = new Array();
data_firstday_by_type[1] = new Array();
data_firstday_by_type[2] = new Array();
<?
for ($i=0; $i<26; $i++)
{
     ?>
     data_firstday_by_type[0][data_firstday_by_type[0].length] = Array(<?=$i?>,"<?=$lc->array_numeric[$i]?>");
     data_firstday_by_type[1][data_firstday_by_type[1].length] = Array(<?=$i?>,"<?=$lc->array_alphabet[$i]?>");
     data_firstday_by_type[2][data_firstday_by_type[2].length] = Array(<?=$i?>,"<?=$lc->array_roman[$i]?>");
     <?
}
?>

function changeFirstDaySelect()
{
         type = document.form1.CycleType.value;
         max_num = document.form1.PeriodDays.value;
         obj = document.form1.FirstDay;
         var current = obj.options.length;
         
         for (var j=current;j>0;j--) obj.options[j-1] = null;
         for (var i=0;i<max_num;i++)
         {
              obj.options[obj.options.length] = new Option(data_firstday_by_type[type][i][1],data_firstday_by_type[type][i][0]);
         }

}
</script>

<form name="form1" action="new_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">

<?= displayNavTitle($i_admintitle_sc, '',$i_adminmenu_sc_school_settings,'../../', $i_admintitle_sc_cycle, '../',$i_CycleNew_Menu_DefinePeriods,'index.php',$button_new,'') ?>
<?= displayTag("head_cycle_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_CycleNew_Field_PeriodStart; ?>:</td><td>
<input type=text name=DateStart value='' size=10><script language="JavaScript" type="text/javascript">
    <!--
         startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(YYYY-MM-DD)</span>
</td></tr>
<tr><td align=right><?php echo $i_CycleNew_Field_PeriodEnd; ?>:</td><td>
<input type=text name=DateEnd value='' size=10><script language="JavaScript" type="text/javascript">
    <!--
         endCal = new dynCalendar('endCal', 'calendarCallback2', '/templates/calendar/images/');
    //-->
    </script> &nbsp;<span class=extraInfo>(YYYY-MM-DD)</span>
</td></tr>
<tr><td align=right><?php echo $i_CycleNew_Field_PeriodType; ?>:</td><td><?=$select_periodtype?></td></tr>
<tr><td align=center colspan=2><hr width=80% align=center size=1><br><div name=otherInfo id=otherInfo></div></td></tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../../templates/adminfooter.php");
?>