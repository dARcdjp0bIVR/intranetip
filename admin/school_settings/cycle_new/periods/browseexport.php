<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();

intranet_opendb();
$lc = new libcycleperiods();
$data = $lc->retrieveImportRecords($PeriodStart,$PeriodEnd);
$content = "\"Date\",\"Chi\",\"Eng\",\"Short\"\n";
$exportColumn = array("Date","Chi","Eng","Short");
for ($i=0; $i<sizeof($data); $i++)
{
     list($id,$date,$txtEng,$txtChi,$txtShort) = $data[$i];
     $content .= "\"$date\",\"$txtChi\",\"$txtEng\",\"$txtShort\"\n";
     $rows[] = array($date, $txtChi, $txtEng, $txtShort);
}
//$content = trim($content);
$filename = "download_$PeriodStart"."to$PeriodEnd.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

//output2browser($content,$filename);
?>