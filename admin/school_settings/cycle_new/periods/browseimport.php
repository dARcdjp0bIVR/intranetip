<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libcycleperiods.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");

intranet_opendb();
$lc = new libcycleperiods();
$data = $lc->retrieveImportRecords($PeriodStart,$PeriodEnd);
?>
<a class=functionlink_new href=browseexport.php?PeriodStart=<?=$PeriodStart?>&PeriodEnd=<?=$PeriodEnd?>><?=$i_CycleNew_Export_FileImport?></a>
<br><br>
<table width=90% border=1 cellpadding=1 cellspacing=0 align=center>
<tr class=tableTitle>
  <td>#</td><td><?=$i_CycleNew_Field_Date?></td><td><?=$i_CycleNew_Field_TxtEng?></td><td><?=$i_CycleNew_Field_TxtChi?></td><td><?=$i_CycleNew_field_TxtShort?></td>
</tr>
<?
for ($i=0; $i<sizeof($data); $i++)
{
     $css = ($i%2?"":"2");
     list($id,$date,$txtEng,$txtChi,$txtShort) = $data[$i];
?>
<tr class=tableContent<?=$css?>>
<td><?=$i+1?></td><td><?=$date?></td><td><?=$txtEng?></td><td><?=$txtChi?></td><td><?=$txtShort?></td>
</tr>
<?
}
?>
</table>

<?php
include_once("../../../../templates/filefooter.php");
?>