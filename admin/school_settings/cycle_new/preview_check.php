<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libcycleperiods.php");
include_once("../../../lang/lang.$intranet_session_language.php");
intranet_opendb();

$lc = new libcycleperiods();
if ($ts == "")
{
    $month = date("m");
    $year = date("y");
}
else
{
    $month = date("m",$ts);
    $year = date("y",$ts);
}
if ($num < 1)
{
    $num = 4;
}

$prev_ts = mktime(0,0,0,$month-$num,1,$year);
$next_ts = mktime(0,0,0,$month+$num,1,$year);
?>
<script language="Javascript" src='<?=$intranet_httppath?>/templates/tooltip.js'></script>
<style type="text/css">
     #ToolTip{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
     </style>

     <script language="JavaScript">
     //isMenu = true;
     isToolTip = true;
     </script>
     <div id="ToolTip"></div>
<?
$body_tags = "onMouseMove=\"overhere()\"";
include_once("../../../templates/fileheader.php");

?>


<form name=form1 action="" method=get>
<table width=90% border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td colspan=2 align=center><?=$i_CycleNew_NumOfMonth?>: <input type=text name=num size=2 maxlength=2 value="<?=$num?>">
<?=btnSubmit()?>
</td></tr>
<tr><td align=left><a class=functionlink_new href="?ts=<?=$prev_ts?>&num=<?=$num?>"><img src="<?=$image_path?>/previous_icon.gif" border=0><?="$list_prev"?></a></td>
<td align=right><a class=functionlink_new href="?ts=<?=$next_ts?>&num=<?=$num?>"><?="$list_next"?><img src="<?=$image_path?>/next_icon.gif" border=0></a></td></tr>
<?
for ($i=0; $i<$num; $i++)
{
     if ($i%2==0)
     {
         echo "<tr>\n";
     }
     $temp_ts = mktime(0,0,0,$month+$i,1,$year);
     $m = date("m",$temp_ts);
     $y = date("Y",$temp_ts);
     ?>
     <td align=center><?=$lc->displayMonth($y,$m)?><br></td>
     <?
     if ($i%2 != 0)
     {
         echo "</tr>\n";
     }
}
if ($num % 2 ==1)
{
?>
<td>&nbsp;</td></tr>
<?
}
?>
</table>
<input type=hidden name=ts value="<?=$ts?>">
</form>
<?
include_once("../../../templates/filefooter.php");
intranet_closedb();
?>