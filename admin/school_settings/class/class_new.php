<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$lclass = new libclass();
$level_selection = $lclass->getSelectLevel("name=ClassLevel") . "<span class=extraInfo>($i_ifapplicable)</span>";


?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.ClassName, "<?php echo $i_alert_pleasefillin.$i_ClassName; ?>.")) return false;
}
</script>

<form name="form1" action="class_new_update.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, 'javascript:history.back()', $button_new." ".$i_ClassName, '') ?>
<?= displayTag("head_school_class_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_ClassName; ?>:</td><td><input class=text type=text name=ClassName size=50 maxlength=100></td></tr>
<tr><td align=right><?php echo $i_ClassLevel; ?>:</td><td><?=$level_selection?></td></tr>
<?if($special_feature['websams_attendance_export']){?>
	<tr><td align=right><?php echo $i_WebSAMS_ClassCode; ?>:</td><td><input class=text type=text name=WebSamsClassCode size=10 maxlength=5 value=''><br>  <span class=extraInfo>(<?=$i_WebSAMS_Notice_AttendanceCode?>)</span>
</td></tr>
<?php } ?>
</table>
<br><br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>