<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");

intranet_opendb();

$special_pagesize = 100;

$field = $field==""?0:$field;
$order = $order==""?1:$order;


if($special_feature['websams_attendance_export']){
		$sql = "SELECT LevelName,WebSAMSLevel,
        CONCAT('<input type=checkbox name=ClassLevelID[] value=',ClassLevelID,'>')
        FROM INTRANET_CLASSLEVEL
        WHERE RecordStatus = 1
        ";
        $field_ary = "LevelName,WebSAMSLevel";
        $col_count =3;
}
else{
	$sql = "SELECT LevelName,
        CONCAT('<input type=checkbox name=ClassLevelID[] value=',ClassLevelID,'>')
        FROM INTRANET_CLASSLEVEL
        WHERE RecordStatus = 1
        ";
      $field_ary = "LevelName";
      $col_count = 2;
}
$li = new libdbtable($field, $order, $pageNo);
$li->sql = $sql;
$li->field_array = array($field_ary);
$li->no_col = sizeof($li->field_array)+$col_count;
$li->title = $i_ClassLevel;
$li->column_array = array(0);
$li->IsColOff = 2;
$li->page_size = $special_pagesize;
$li->form_name = "levelform";



// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";

if($special_feature['websams_attendance_export']){  # with Websams
	$li->column_list .= "<td width=50% class=tableTitle>$i_ClassLevelName</td>\n";
	$li->column_list .= "<td width=50% class=tableTitle>$i_WebSAMS_ClassLevelName</td>\n";
}
else{
	$li->column_list .= "<td width=100% class=tableTitle>$i_ClassLevelName</td>\n";
}
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("ClassLevelID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=javascript:checkNew('level_new.php')>".newIcon()."$button_new</a>";
$toolbar .= " <a class=iconLink href=javascript:checkNew('import.php')>".importIcon()."$button_import</a>";
$functionbar= "";
$functionbar .= "<a href=\"javascript:checkEdit(document.levelform,'ClassLevelID[]','level_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.levelform,'ClassLevelID[]','level_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar = "";


if($special_feature['websams_attendance_export']){

	$sql2 = "SELECT CONCAT('<a href=class_edit.php?ClassID[]=',a.ClassID,'>',a.ClassName,'</a>'),
	         IF (b.LevelName IS NULL, '$i_notapplicable',b.LevelName),
	         a.WebSAMSClassCode,
	        CONCAT('<input type=checkbox name=ClassID[] value=',a.ClassID,'>')
	        FROM INTRANET_CLASS as a LEFT OUTER JOIN INTRANET_CLASSLEVEL as b ON a.ClassLevelID = b.ClassLevelID
	        WHERE a.RecordStatus = 1
	        ";
   	        $field_ary2="a.ClassName,b.LevelName,a.WebSAMSClassCode";
	        $col_count2=4;

}
else{
		$sql2 = "SELECT CONCAT('<a href=class_edit.php?ClassID[]=',a.ClassID,'>',a.ClassName,'</a>'),
	         IF (b.LevelName IS NULL, '$i_notapplicable',b.LevelName),
	        CONCAT('<input type=checkbox name=ClassID[] value=',a.ClassID,'>')
	        FROM INTRANET_CLASS as a LEFT OUTER JOIN INTRANET_CLASSLEVEL as b ON a.ClassLevelID = b.ClassLevelID
	        WHERE a.RecordStatus = 1
	        ";
	        $field_ary2="a.ClassName,b.LevelName";
	        $col_count2=3;
}
$field2 = $field2==""?0:$field2;
$order2 = $order2==""?1:$order2;

$li2 = new libdbtable($field2, $order2, $pageNo2);
$li2->field_array = array($field_ary2);
$li2->sql = $sql2;
$li2->no_col = sizeof($li2->field_array)+$col_count2;
$li2->title = $i_ClassName;
$li2->column_array = array(0);
$li2->IsColOff = 2;
$li2->page_size = $special_pagesize;
$li2->form_name = "classform";
$li2->pageNo_name = "pageNo2";
// TABLE COLUMN
$li2->column_list .= "<td width=1 class=tableTitle>#</td>\n";

if($special_feature['websams_attendance_export']){
	$li2->column_list .= "<td width=34% class=tableTitle>$i_ClassName</td>\n";
	$li2->column_list .= "<td width=33% class=tableTitle>$i_ClassLevel</td>\n";
	$li2->column_list .= "<td width=33% class=tableTitle>$i_WebSAMS_ClassCode</td>\n";
}else{
	$li2->column_list .= "<td width=50% class=tableTitle>$i_ClassName</td>\n";
	$li2->column_list .= "<td width=50% class=tableTitle>$i_ClassLevel</td>\n";
}
$li2->column_list .= "<td width=1 class=tableTitle>".$li2->check("ClassID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar2 = "<a class=iconLink href=javascript:checkNew('class_new.php')>".newIcon()."$button_new</a>";
$toolbar2 .= " <a class=iconLink href=javascript:checkNew('import.php')>".importIcon()."$button_import</a>";
$functionbar2= "";
$functionbar2 .= "<a href=\"javascript:checkEdit(document.classform,'ClassID[]','class_edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar2 .= "<a href=\"javascript:checkRemove(document.classform,'ClassID[]','class_remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
$searchbar2 = "";
?>

<script language="javascript">
function checkform(obj){
     return true;
}
</script>

<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../', $i_SettingsSchool_Class, '') ?>
<?= displayTag("head_school_class_$intranet_session_language.gif", $msg) ?>

<form name="levelform" method="post">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<br>

<form name="classform" method="post">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li2->displayFunctionbar("-", "-", $toolbar2, $functionbar2); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li2->display(); ?>
<input type=hidden name=pageNo2 value="<?php echo $li2->pageNo; ?>">
<input type=hidden name=order2 value="<?php echo $li2->order; ?>">
<input type=hidden name=field2 value="<?php echo $li2->field; ?>">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>


<?php
include_once("../../../templates/adminfooter.php");
?>