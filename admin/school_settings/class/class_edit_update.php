<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
intranet_opendb();

$li = new libclass();

$ClassName = htmlspecialchars(trim($ClassName));
$ClassLevel = htmlspecialchars(trim($ClassLevel));

$levelID = $li->getLevelID($ClassLevel);
if ($levelID == "") $levelID = 'NULL';

# Check group
$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = 3 AND Title = '$ClassName'";
$result = $li->returnVector($sql);
$GroupID = $result[0];
if ($GroupID == "" || $GroupID == 0)
{
    $oldName = $li->getClassName($ClassID);
    $sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = 3 AND Title = '$oldName'";
    $result = $li->returnVector($sql);
    $GroupID = $result[0];
    if ($GroupID == "" || $GroupID == 0)
    {
        $sql = "INSERT INTO INTRANET_GROUP (Title,RecordType,StorageQuota,DateInput,DateModified) VALUES ('$ClassName',3,5,now(),now())";
        $li->db_db_query($sql);
        $GroupID = $li->db_insert_id();
    }
    else
    {
        # Update Group Name
        $sql = "UPDATE INTRANET_GROUP SET Title = '$ClassName' WHERE GroupID = $GroupID";
        $li->db_db_query($sql);
    }
}


$sql = "UPDATE INTRANET_CLASS SET ClassName = '$ClassName', ClassLevelID = '$levelID',WebSAMSClassCode='$WebSamsClassCode'
        ,GroupID = $GroupID WHERE ClassID = $ClassID";

$li->db_db_query($sql);
intranet_closedb();
header("Location: class.php?msg=2");

?>