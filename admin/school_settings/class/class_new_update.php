<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$ClassName = htmlspecialchars(trim($ClassName));

if ($ClassLevel != '')
{
    $sql = "SELECT ClassLevelID FROM INTRANET_CLASSLEVEL WHERE LevelName = '$ClassLevel'";
    $result = $li->returnVector($sql);
    $levelID = $result[0];
}
else
{
    $levelID = "NULL";
}

# Check group
$sql = "SELECT GroupID FROM INTRANET_GROUP WHERE RecordType = 3 AND Title = '$ClassName'";
$result = $li->returnVector($sql);
$GroupID = $result[0];
if ($GroupID == "" || $GroupID == 0)
{
    $sql = "INSERT INTO INTRANET_GROUP (Title,RecordType,StorageQuota,DateInput,DateModified) VALUES ('$ClassName',3,5,now(),now())";
    $li->db_db_query($sql);
    $GroupID = $li->db_insert_id();
}

$sql = "INSERT INTO INTRANET_CLASS (ClassName, RecordStatus, ClassLevelID,GroupID,WebSAMSClassCode) VALUES ('$ClassName','1','$levelID','$GroupID','$WebSamsClassCode')";
$li->db_db_query($sql);

intranet_closedb();
header("Location: class.php?msg=1");
?>