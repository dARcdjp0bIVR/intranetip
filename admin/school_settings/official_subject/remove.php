<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$RecordIDList = (is_array($RecordID)) ? implode(",", $RecordID) : $RecordID;
$sql = "DELETE FROM {$eclass_db}.ASSESSMENT_SUBJECT WHERE RecordID IN ($RecordIDList)";
$li->db_db_query($sql);


intranet_closedb();
header("Location: index.php?msg=3&field=$field&order=$order");
?>