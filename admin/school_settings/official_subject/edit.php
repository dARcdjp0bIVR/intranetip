<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();

$RecordID = (is_array($RecordID)) ? $RecordID[0] : $RecordID;

$li = new libdb();


# Get Subject Info
$sql = "SELECT 
			CONCAT(EN_DES, '&nbsp;', CH_DES), 
			CONCAT(EN_ABBR, '&nbsp;', CH_ABBR),
			CONCAT(EN_SNAME, '&nbsp;', CH_SNAME), 
			DisplayOrder 
		FROM 
			{$eclass_db}.ASSESSMENT_SUBJECT 
		WHERE 
				RecordID = '$RecordID'";
$row = $li->returnArray($sql, 4);
list($description, $abbrName, $shortName, $displayOrder) = $row[0];

# Find previous subject record id
$sql = "SELECT RecordID FROM {$eclass_db}.ASSESSMENT_SUBJECT where DisplayOrder < '{$displayOrder}' ORDER BY DisplayOrder DESC LIMIT 0,1";
$row = $li->returnVector($sql);
$prev_recordID = (sizeof($row)!=0) ? $row[0] : 0;

# Create Subject Selection List
$namefield = ($intranet_session_language=="eb") ? "EN_DES" : "CH_DES";

$sql = "SELECT RecordID, $namefield FROM {$eclass_db}.ASSESSMENT_SUBJECT ORDER BY DisplayOrder";
$row = $li->returnArray($sql, 2);

$subject_selection_list = "<SELECT name='prev_recordID' class='inputfield' onChange='checkSelf(this, $RecordID)'>";
$subject_selection_list .= "<OPTION value='0'>/</OPTION>";
for($i=0; $i<sizeof($row); $i++)
{
	list($rid, $name) = $row[$i];

	$style = ($rid==$RecordID) ? "style=\"color:'#0054A6'\"" : "";
	$selected = ($rid==$prev_recordID) ? "SELECTED" : $selected = "";

	$subject_selection_list .= "<OPTION value='$rid' {$selected} {$style}>".$name."</OPTION>";
}
$subject_selection_list .= "</SELECT>";

?>

<script language="JavaScript">
function checkSelf(obj, recordID){

	var new_id = obj.value;
	var curr = obj.selectedIndex;

	if(new_id==recordID)
	{
		obj.selectedIndex = curr-1;
	}
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_fs, '', $i_adminmenu_sc_school_settings, '../index.php', $i_admintitle_fs_official_subject, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_AddOn_Subject_Settings_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align="right" nowrap class="chi_content_12"><?=$i_SAMS_description?>�G</td><td><?=$description?></td>
</tr>
<tr>
	<td align="right" nowrap class="chi_content_12"><?=$i_SAMS_abbr_name?>�G</td><td><?=$abbrName?></td>
</tr>
<tr>
   <td align="right" nowrap class="chi_content_12"><?=$i_SAMS_short_name?>�G</td><td><?=$shortName?></td>
</tr>
<tr>
	<td align="right" nowrap class="chi_content_12"><?=$i_general_DisplayOrder?>�G</td><td><?=$subject_selection_list."&nbsp;".$i_SAMS_notes_after?></td>
</tr>
</table>
<br>
</blockquote>

<input type=hidden name=RecordID value="<?=$RecordID?>">
<input type=hidden name=field value="<?=$field?>">
<input type=hidden name=order value="<?=$order?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>