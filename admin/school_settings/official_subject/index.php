<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../lang/email.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../templates/adminheader_setting.php");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

intranet_opendb();
# TABLE SQL
if ($order=="") $order=1;
if ($field=="") $field=0;

if (isset($_GET['keyword']))
{
	$keyword = trim(addslashes(htmlspecialchars($_GET['keyword'])));
	$display_keyword = stripslashes(htmlspecialchars($_GET['keyword']));
	$conds = " WHERE 
				EN_SNAME LIKE '%{$keyword}%' 
				OR CH_SNAME LIKE '%{$keyword}%' 
				OR EN_DES LIKE '%{$keyword}%' 
				OR CH_DES LIKE '%{$keyword}%'
				OR CODEID LIKE '%{$keyword}%'
				OR CMP_CODEID LIKE '%{$keyword}%'
			";
}	

// TABLE SQL
$fieldname  = "CODEID AS CodeID, ";
$fieldname .= "if(CMP_CODEID IS NULL OR CMP_CODEID='','-',CMP_CODEID) AS CmpCodeID, ";
$fieldname .= "CONCAT(EN_DES, '<br>', CH_DES) AS Description, ";
$fieldname .= "if((EN_ABBR IS NULL OR EN_ABBR='') AND (CH_ABBR IS NULL OR CH_ABBR=''),'-',CONCAT(EN_ABBR, '<br>', CH_ABBR)) AS AbbrName, ";
$fieldname .= "CONCAT(EN_SNAME, '<br>', CH_SNAME) AS ShortName, ";
$fieldname .= "DisplayOrder, ";
$fieldname .= "CONCAT('<input type=checkbox name=RecordID[] value=',RecordID,'>')";

$sql = "SELECT $fieldname FROM {$eclass_db}.ASSESSMENT_SUBJECT $conds";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("CODEID", "CMP_CODEID", "EN_DES", "EN_ABBR", "EN_SNAME", "DisplayOrder");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_subject;
$li->column_array = array(0);
$li->IsColOff = 2;
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width='10%' class='tableTitle' bgcolor='#CFE6FE'>".$li->column(0, $i_SAMS_code, 1)."</td>\n";
$li->column_list .= "<td width='10%' class='tableTitle' bgcolor='#CFE6FE'>".$li->column(1, $i_SAMS_cmp_code, 1)."</td>\n";
$li->column_list .= "<td width='35%' class='tableTitle' bgcolor='#CFE6FE'>".$li->column(2, $i_SAMS_description, 1)."</td>\n";
$li->column_list .= "<td width='25%' class='tableTitle' bgcolor='#CFE6FE'>".$li->column(3, $i_SAMS_abbr_name, 1)."</td>\n";
$li->column_list .= "<td width='20%' class='tableTitle' bgcolor='#CFE6FE'>".$li->column(4, $i_SAMS_short_name, 1)."</td>\n";
$li->column_list .= "<td width='10%' class='tableTitle' bgcolor='#CFE6FE' nowrap='nowrap'>".$li->column(5, $i_general_DisplayOrder, 1)."</td>\n";

$li->column_list .= "<td width=1 class=tableTitle>".$li->check("RecordID[]")."</td>\n";

// TABLE FUNCTION BAR
$toolbar = "<a class=iconLink href=\"javascript:checkNew('import.php')\">".importIcon()."$button_import</a>";
$functionbar= "";
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'RecordID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'RecordID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".$display_keyword."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_school_settings, '../index.php', $i_admintitle_fs_official_subject, '') ?>
<?= displayTag("head_AddOn_Subject_Settings_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>


<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>