<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");

# Error Code
define ("ERROR_TYPE_DUPLICATED_SNAME", 1);
unset($error_data);
$count_new = 0;
$count_updated = 0;

# uploaded file information
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == "" || !is_uploaded_file($filepath))
{
   header("Location: import.php");
} 
else 
{
	include_once("../../../templates/adminheader_setting.php");
	intranet_opendb();

	$li = new libdb();
	$li->db = $intranet_db;
	$lf = new libfilesystem();

	$ext = strtoupper($lf->file_ext($filename));
    if ($ext == ".CSV")
    {
		$data = $lf->file_read_csv($filepath);
		$header_row = array_shift($data);                   # drop the title bar
    }

    # Check Title Row
    $format_wrong = false;
		$file_format1 = array("CODE_ID","EN_SNAME","CH_SNAME","EN_DES","CH_DES","EN_ABBR","CH_ABBR");
		$file_format2 = array("SBJ_CODE","CODE_ID","EN_SNAME","CH_SNAME","EN_DES","CH_DES");
		$format_type = ($header_row[0]=="SBJ_CODE") ? 1 : 0;

	if($format_type==1)
	{
		for ($i=0; $i<sizeof($file_format2); $i++)
		{			
			 if ($header_row[$i]!=$file_format2[$i])
			 {
				 $format_wrong = true;
				 break;
			 }
		}
	}
	else
	{
		for ($i=0; $i<sizeof($file_format1); $i++)
		{
			 if ($header_row[$i]!=$file_format1[$i])
			 {
				 $format_wrong = true;
				 break;
			 }
		}
	}
	
	if ($format_wrong)
    {
		$correct_format = "<table width='50%' border='0' cellpadding='5' cellspacing='0'>\n";
		$correct_format_array = ($format_type==1) ? $file_format2 : $file_format1;
        for ($i=0; $i<sizeof($correct_format_array); $i++)
        {
             $correct_format .= "<tr><td>".$correct_format_array[$i]."</td></tr>\n";
        }
        $correct_format .= "</table>\n";

		$wrong_format = "<table width='50%' border='0' cellpadding='5' cellspacing='0'>\n";
        for ($i=0; $i<sizeof($header_row); $i++)
        {
			$field_title = ($header_row[$i]!=$file_format[$i]) ? "<u>".$header_row[$i]."</u>" : $header_row[$i];
			$wrong_format .= "<tr><td>".$field_title."</td></tr>\n";
        }
        $wrong_format .= "</table>\n";
		
		$display_content = "<table width='90%' border=0 cellpadding=5 cellspacing=1><tr><td align=center>".$i_SAMS_import_error_wrong_format."</td></tr></table>\n";
        $display_content .= "<table width='90%' align=center border=0 cellpadding=5 cellspacing=1 style='border: 1px solid #000000;'>\n";
		$display_content .= "<tr><td valign='top'>{$correct_format}</td><td width='50%' align='center'>VS</td><td valign='top'>{$wrong_format}</td></tr>\n";
        $display_content .= "</table>\n";
    }
	else
    {
		# Get existing CodeID
        $sql = "SELECT CodeID, CMP_CODEID FROM {$eclass_db}.ASSESSMENT_SUBJECT WHERE CodeID IS NOT NULL AND CodeID <> ''";
        $temp = $li->returnArray($sql);

        unset($array_subj_codeid);
        for ($i=0; $i<sizeof($temp); $i++)
        {
			list($target, $target_cmp) = $temp[$i];
			if (trim($target)!="")
			{
				if($target_cmp=="")
					$array_subj_codeid[$target]["main"] = 1;
				else
					$array_subj_codeid[$target][$target_cmp] = 1;
			}
        }

		# Get existing
        # find the max DisplayOrder of existing records
		$sql = "	SELECT
						MAX(DisplayOrder)
					FROM
						{$eclass_db}.ASSESSMENT_SUBJECT
				";
		$row = $li->returnVector($sql);
		$NextDisplayOrder = $row[0] + 1;
		
		if($format_type==1) 
        {
			for ($i=0; $i<sizeof($data); $i++)
			{
				list ($t_codeID, $t_cmp_codeID, $t_en_sname, $t_ch_sname, $t_en_des, $t_ch_des) = $data[$i];
				if($t_en_sname=="")
					continue;

				$t_en_sname = addslashes($t_en_sname);
				$t_ch_sname = addslashes($t_ch_sname);
				$t_en_des = addslashes($t_en_des);
				$t_ch_des = addslashes($t_ch_des);
				
				# Check existing
				 if ($array_subj_codeid[$t_codeID][$t_cmp_codeID])
				 {
					 # update
					 $sql = "UPDATE 
								{$eclass_db}.ASSESSMENT_SUBJECT 
							SET
								EN_SNAME = '$t_en_sname', 
								CH_SNAME = '$t_ch_sname',
								EN_DES = '$t_en_des', 
								CH_DES = '$t_ch_des'
							WHERE 
								CODEID = '$t_codeID'
								AND CMP_CODEID = '$t_cmp_codeID'
						";
					 $li->db_db_query($sql);
					 if ($li->db_affected_rows()==-1)
					 {
						# Failed
						 $error_data[] = array($i, ERROR_TYPE_DUPLICATED_SNAME, array($t_codeID, $t_cmp_codeID, $t_en_sname));
					 }
					 else
					 {
						 # Update success
						 $count_updated ++;
					 }
				 }
				 else # Add new
				 {
						$sql = "INSERT 
									{$eclass_db}.ASSESSMENT_SUBJECT 
									(CODEID, CMP_CODEID, EN_SNAME, CH_SNAME, 
										EN_DES, CH_DES, DisplayOrder)
								VALUES
									('$t_codeID','$t_cmp_codeID','$t_en_sname','$t_ch_sname',
										'$t_en_des','$t_ch_des','$NextDisplayOrder')";
						$li->db_db_query($sql);
						if ($li->db_affected_rows()==1)
						{
							$count_new++;
							$array_subj_codeid[$t_codeID][$t_cmp_codeID] = 1;
							$NextDisplayOrder ++;
						}
						else
						{
							# failed
							$error_data[] = array($i, ERROR_TYPE_DUPLICATED_SNAME, array($t_codeID, $t_cmp_codeID, $t_en_sname) );
						}
				 }
			}
		}
		else
		{
			for ($i=0; $i<sizeof($data); $i++)
			{
				list ($t_codeID, $t_en_sname, $t_ch_sname, $t_en_des, $t_ch_des, $t_en_abbr, $t_ch_abbr) = $data[$i];
				if($t_en_sname=="")
					continue;

				$t_en_sname = addslashes($t_en_sname);
				$t_ch_sname = addslashes($t_ch_sname);
				$t_en_des = addslashes($t_en_des);
				$t_ch_des = addslashes($t_ch_des);
				$t_en_abbr = addslashes($t_en_abbr);
				$t_ch_abbr = addslashes($t_ch_abbr);

				 # Check existing
				 if ($array_subj_codeid[$t_codeID]["main"])
				 {
					 # update
					 $sql = "UPDATE 
								{$eclass_db}.ASSESSMENT_SUBJECT 
							SET
								EN_SNAME = '$t_en_sname', 
								CH_SNAME = '$t_ch_sname',
								EN_DES = '$t_en_des', 
								CH_DES = '$t_ch_des',
								EN_ABBR = '$t_en_abbr', 
								CH_ABBR = '$t_ch_abbr'
							WHERE 
								CODEID = '$t_codeID'
								AND (CMP_CODEID IS NULL OR CMP_CODEID = '')
							";
					 $li->db_db_query($sql);

					 if ($li->db_affected_rows()==-1)
					 {
						# Failed
						 $error_data[] = array($i, ERROR_TYPE_DUPLICATED_SNAME, array($t_codeID, "", $t_en_sname));
					 }
					 else
					 {
						 # Update success
						 $count_updated ++;
					 }
				 }
				 else # Add new
				 {
						$sql = "INSERT 
									{$eclass_db}.ASSESSMENT_SUBJECT 
									(CODEID, EN_SNAME, CH_SNAME, EN_DES, CH_DES, EN_ABBR, CH_ABBR, DisplayOrder)
								VALUES
									('$t_codeID','$t_en_sname','$t_ch_sname','$t_en_des','$t_ch_des','$t_en_abbr','$t_ch_abbr', '$NextDisplayOrder')";
						$li->db_db_query($sql);

						if ($li->db_affected_rows()==1)
						{
							$count_new++;
							$array_subj_codeid[$t_codeID]["main"] = 1;
							$NextDisplayOrder ++;
						}
						else
						{
							# failed
							$error_data[] = array($i, ERROR_TYPE_DUPLICATED_SNAME, array($t_codeID, "", $t_en_sname) );
						}
				 }
			}
		}
	}
}

if(!$format_wrong)
{
	# Display import stats
	$display_content = "<table border='0' cellpadding='8' cellspacing='0'>";
	$display_content .= "<tr><td class='chi_content_15'>".$i_SAMS_import_add_no." : </td><td class='chi_content_15'><b>".$count_new."</b></td></tr>\n";
	$display_content .= "<tr><td class='chi_content_15'>".$i_SAMS_import_update_no." : </td><td class='chi_content_15'><b>".$count_updated."</b></td></tr>\n";
	$display_content .= "</table>\n";
}

if (sizeof($error_data)!=0)
{
	$error_table = "<br><table width='90%' align=center border=0 cellpadding=5 cellspacing=1>\n";
    $error_table .= "<tr bgcolor='#CFE6FE'><td>".$i_SAMS_import_error_row."</td><td>".$i_SAMS_import_error_reason."</td><td>".$i_SAMS_import_error_detail."</td></tr>\n";

	for ($i=0; $i<sizeof($error_data); $i++)
	{
		list ($t_row, $t_type, $t_data) = $error_data[$i];
		$css_color = ($i%2==0) ? "#FFFFFF" : "#F3F3F3";
		$error_table .= "<tr bgcolor='$css_color'><td>$t_row</td><td>";
		$reason_string = $i_SAMS_import_error_unknown;
		switch ($t_type)
		{
			case ERROR_TYPE_DUPLICATED_SNAME:
				$reason_string = $i_SAMS_SAMS_short_name_duplicate;
				break;
			default:
				$reason_string = $i_SAMS_import_error_unknown;
			break;
		}
		$error_table .= $reason_string;
		$error_table .= "</td><td>".implode(",",$t_data)."</td></tr>\n";
	}
	$error_table .= "</table>\n";
	$display_content .= $error_table;
}
?>
<?= displayNavTitle($i_admintitle_fs, '', $i_adminmenu_sc_school_settings, '../index.php', $i_admintitle_fs_official_subject, 'index.php', $button_import, '') ?>
<?= displayTag("head_AddOn_Subject_Settings_$intranet_session_language.gif", $msg) ?>

<blockquote>
<?= $display_content ?>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_back_<?=$intranet_session_language?>.gif" border="0" onClick="self.location='index.php'"></td>
</tr>
</table>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
