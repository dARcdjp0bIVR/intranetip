<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_setting.php");
intranet_opendb();
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.userfile, "<?php echo $i_alert_pleasefillin.$i_SAMS_CSV_file; ?>.")) return false;
}
</script>

<form name="form1" enctype="multipart/form-data" action="import_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_fs, '', $i_adminmenu_sc_school_settings, '../index.php', $i_admintitle_fs_official_subject, 'javascript:history.back()', $button_import, '') ?>
<?= displayTag("head_AddOn_Subject_Settings_$intranet_session_language.gif", $msg) ?>


<blockquote>
<br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td width='140' height='40' nowrap='nowrap'><?php echo $i_SAMS_CSV_file; ?>:</td><td><input class=file type=file name=userfile size=25></td></tr>
<tr>
	<td colspan="2"><a href="<?= GET_CSV("subject_import_sams_sample.csv")?>" target="_blank" class="functionlink_new"><img src="../../../images/2007a/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$i_SAMS_download_subject_csv?></a></td>
</tr>
<tr>
	<td colspan="2"><a href="<?= GET_CSV("cmp_subject_import_sams_sample.csv")?>" target="_blank" class="functionlink_new"><img src="../../../images/2007a/icon_files/xls.gif" border="0" align="absmiddle" hspace="3"><?=$i_SAMS_download_cmp_subject_csv?></a></td>
</tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="javascript:self.location='index.php'"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>