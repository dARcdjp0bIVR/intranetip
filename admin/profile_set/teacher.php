<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
// $li_menu from adminheader_intranet.php

$lstudentprofile = new libstudentprofile();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, 'index.php',$i_Profile_settings_display,'') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript>
function class_clicked(obj)
{
         if (!obj.class_teacher_level[0].checked)
         {
             obj.class_attend.disabled = false;
             obj.class_merit.disabled = false;
             obj.class_service.disabled = false;
             obj.class_activity.disabled = false;
             obj.class_award.disabled = false;
             obj.class_assessment.disabled = false;
             obj.class_file.disabled = false;
         }
         else
         {
             obj.class_attend.disabled = true;
             obj.class_merit.disabled = true;
             obj.class_service.disabled = true;
             obj.class_activity.disabled = true;
             obj.class_award.disabled = true;
             obj.class_assessment.disabled = true;
             obj.class_file.disabled = true;
         }
}
function subj_clicked(obj)
{
         if (!obj.subj_teacher_level[0].checked)
         {
             obj.subj_attend.disabled = false;
             obj.subj_merit.disabled = false;
             obj.subj_service.disabled = false;
             obj.subj_activity.disabled = false;
             obj.subj_award.disabled = false;
             obj.subj_assessment.disabled = false;
             obj.subj_file.disabled = false;
         }
         else
         {
             obj.subj_attend.disabled = true;
             obj.subj_merit.disabled = true;
             obj.subj_service.disabled = true;
             obj.subj_activity.disabled = true;
             obj.subj_award.disabled = true;
             obj.subj_assessment.disabled = true;
             obj.subj_file.disabled = true;
         }
}
</SCRIPT>
<form name=form1 action="teacher_update.php" method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td>
<blockquote>
<span class="subTitle"><?=$i_Teaching_ClassTeacher?>:</span>
<input type=radio name=class_teacher_level value=0 <?=!$lstudentprofile->is_class_allowed? "CHECKED":""?> onclick="class_clicked(this.form)">
<?=$i_Profile_Admin_NotAllowed?>
<input type=radio name=class_teacher_level value=1 <?=$lstudentprofile->is_class_allowed && $lstudentprofile->class_adminlevel!=1? "CHECKED":""?> onclick="class_clicked(this.form)">
<?=$i_AdminJob_AdminLevel_Normal?>
<input type=radio name=class_teacher_level value=2 <?=$lstudentprofile->is_class_allowed && $lstudentprofile->class_adminlevel==1? "CHECKED":""?> onclick="class_clicked(this.form)">
<?=$i_AdminJob_AdminLevel_Full?>
<br>
<input type=checkbox name=class_attend value=1 <?=$lstudentprofile->is_class_attend_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Attendance?>
<input type=checkbox name=class_merit value=1 <?=$lstudentprofile->is_class_merit_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Merit?>
<br>
<input type=checkbox name=class_service value=1 <?=$lstudentprofile->is_class_service_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Service?>
<input type=checkbox name=class_activity value=1 <?=$lstudentprofile->is_class_activity_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Activity?>
<input type=checkbox name=class_award value=1 <?=$lstudentprofile->is_class_award_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Award?>
<br>
<input type=checkbox name=class_assessment value=1 <?=$lstudentprofile->is_class_assessment_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Assessment?>
<input type=checkbox name=class_file value=1 <?=$lstudentprofile->is_class_file_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_class_allowed? "DISABLED":""?>> <?=$i_Profile_Files?>
<br><br>
<span class="subTitle"><?=$i_general_subject_teacher?>:</span>
<input type=radio name=subj_teacher_level value=0 <?=!$lstudentprofile->is_subj_allowed? "CHECKED":""?> onclick="subj_clicked(this.form)">
<?=$i_Profile_Admin_NotAllowed?>
<input type=radio name=subj_teacher_level value=1 <?=$lstudentprofile->is_subj_allowed && $lstudentprofile->subj_adminlevel!=1? "CHECKED":""?> onclick="subj_clicked(this.form)">
<?=$i_AdminJob_AdminLevel_Normal?>
<input type=radio name=subj_teacher_level value=2 <?=$lstudentprofile->is_subj_allowed && $lstudentprofile->subj_adminlevel==1? "CHECKED":""?> onclick="subj_clicked(this.form)">
<?=$i_AdminJob_AdminLevel_Full?>
<br>
<input type=checkbox name=subj_attend value=1 <?=$lstudentprofile->is_subj_attend_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Attendance?>
<input type=checkbox name=subj_merit value=1 <?=$lstudentprofile->is_subj_merit_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Merit?>
<br>
<input type=checkbox name=subj_service value=1 <?=$lstudentprofile->is_subj_service_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Service?>
<input type=checkbox name=subj_activity value=1 <?=$lstudentprofile->is_subj_activity_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Activity?>
<input type=checkbox name=subj_award value=1 <?=$lstudentprofile->is_subj_award_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Award?>
<br>
<input type=checkbox name=subj_assessment value=1 <?=$lstudentprofile->is_subj_assessment_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Assessment?>
<input type=checkbox name=subj_file value=1 <?=$lstudentprofile->is_subj_file_allowed?"CHECKED":""?> <?=!$lstudentprofile->is_subj_allowed? "DISABLED":""?>> <?=$i_Profile_Files?>

</blockquote>
</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>

</table>
</form>
<?
include_once("../../templates/adminfooter.php");

?>
