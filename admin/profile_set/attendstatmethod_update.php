<?php
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$SettingList['ProfileAttendCount'] = (empty($_REQUEST['AttendanceCountMethod'])) ? 0 : $_REQUEST['AttendanceCountMethod'];
$GeneralSetting = new libgeneralsettings();

$GeneralSetting->Save_General_Setting('StudentAttendance',$SettingList);

intranet_closedb();
header("Location: attendstatmethod.php?msg=2");
?>
