<?php
# using: yat

############## Change Log Start ##########################
#
#	Date:	2010-02-18 YatWoon
#			add back intranet_opendb(); !!!
#
############## Change Log End ##########################

include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
// $li_menu from adminheader_intranet.php

intranet_opendb();

$lstudentprofile = new libstudentprofile();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, 'index.php',$i_Profile_settings_merit_type_select,'') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="merittype_update.php" method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td>
<blockquote>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td colspan=2 class=tableTitle_new><?=$i_Profile_settings_select_fields_to_disable?></td>
</tr>
<tr>
<td ><input type=checkbox name=is_merit_disabled <?=($lstudentprofile->is_merit_disabled? "CHECKED":"")?> value=1><?=$i_Merit_Merit?></td>
<td ><input type=checkbox name=is_black_disabled <?=($lstudentprofile->is_black_disabled? "CHECKED":"")?> value=1><?=$i_Merit_BlackMark?></td>
</tr>
<tr>
<td ><input type=checkbox name=is_min_merit_disabled <?=($lstudentprofile->is_min_merit_disabled? "CHECKED":"")?> value=1><?=$i_Merit_MinorCredit?></td>
<td ><input type=checkbox name=is_min_demer_disabled <?=($lstudentprofile->is_min_demer_disabled? "CHECKED":"")?> value=1><?=$i_Merit_MinorDemerit?></td>
</tr>
<tr>
<td ><input type=checkbox name=is_maj_merit_disabled <?=($lstudentprofile->is_maj_merit_disabled? "CHECKED":"")?> value=1><?=$i_Merit_MajorCredit?></td>
<td ><input type=checkbox name=is_maj_demer_disabled <?=($lstudentprofile->is_maj_demer_disabled? "CHECKED":"")?> value=1><?=$i_Merit_MajorDemerit?></td>
</tr>
<tr>
<td ><input type=checkbox name=is_sup_merit_disabled <?=($lstudentprofile->is_sup_merit_disabled? "CHECKED":"")?> value=1><?=$i_Merit_SuperCredit?></td>
<td ><input type=checkbox name=is_sup_demer_disabled <?=($lstudentprofile->is_sup_demer_disabled? "CHECKED":"")?> value=1><?=$i_Merit_SuperDemerit?></td>
</tr>
<tr>
<td ><input type=checkbox name=is_ult_merit_disabled <?=($lstudentprofile->is_ult_merit_disabled? "CHECKED":"")?> value=1><?=$i_Merit_UltraCredit?></td>
<td ><input type=checkbox name=is_ult_demer_disabled <?=($lstudentprofile->is_ult_demer_disabled? "CHECKED":"")?> value=1><?=$i_Merit_UltraDemerit?></td>
</tr>

</table>
</blockquote>
</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>

</table>
</form>
<?
include_once("../../templates/adminfooter.php");

?>
