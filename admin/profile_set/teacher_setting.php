<?php
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
// $li_menu from adminheader_intranet.php

intranet_opendb();

$SettingList[] = "'TeacheriAccountProfileAllowEdit'";
$GeneralSetting = new libgeneralsettings();
$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance',$SettingList);
$setting = $Settings['TeacheriAccountProfileAllowEdit'];

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, 'index.php',$i_Profile_settings_AttednanceReason,'') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="teacher_setting_update.php" method="POST">

<table border=0 width=250 align=center cellpadding=5 cellspacing=0>
<tr><td class=tableTitle_new style="vertical-align:bottom" colspan=2 align=center><?=$i_Profile_settings_AttednanceReason?></td></tr>
<?
if(($setting=="")||($setting==0))
{
?>
<tr><td style="vertical-align:bottom" align=right><?=$i_Profile_settings_AttednanceReasonNotEditable?></td><td align=left width=50%><input name="teacher_profile_setting" type=radio value=0 CHECKED></td></tr>
<tr><td style="vertical-align:bottom" align=right><?=$i_Profile_settings_AttednanceReasonEditable?></td><td align=left width=50%><input name="teacher_profile_setting" type=radio value=1></td></tr>
<?
}
if($setting==1)
{
?>
<tr><td style="vertical-align:bottom" align=right><?=$i_Profile_settings_AttednanceReasonNotEditable?></td><td align=left width=50%><input name="teacher_profile_setting" type=radio value=0></td></tr>
<tr><td style="vertical-align:bottom" align=right><?=$i_Profile_settings_AttednanceReasonEditable?></td><td align=left width=50%><input name="teacher_profile_setting" type=radio value=1 CHECKED></td></tr>
<?
}
?>
</table>

<table border=0 width=250 align=center>
<tr><td><hr size=1></td></tr>
<tr><td align=right><?=btnSubmit();?><?=btnReset();?></td></tr>
</table>

</form>

<?
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
intranet_closedb();
?>