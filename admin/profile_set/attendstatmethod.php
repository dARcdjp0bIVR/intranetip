<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
// $li_menu from adminheader_intranet.php

intranet_opendb();

$lstudentprofile = new libstudentprofile();

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, 'index.php',$i_Profile_settings_attend_stat_method,'') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>

<form name=form1 action="attendstatmethod_update.php" method=POST>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td>
<blockquote>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
        <td>
                <select name="AttendanceCountMethod">
                <option value=0 <?=(($lstudentprofile->attendance_count_method == 0) ? "SELECTED"
                 : "") ?>><?=$i_Profile_AttendanceStatistic_Method1?></option>
                <option value=1 <?=(($lstudentprofile->attendance_count_method == 1) ? "SELECTED"
                 : "") ?>><?=$i_Profile_AttendanceStatistic_Method2?></option>
                </select>
        </td>
</tr>

</table>
</blockquote>
</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>

</table>
</form>
<?
include_once("../../templates/adminfooter.php");
intranet_closedb();
?>
