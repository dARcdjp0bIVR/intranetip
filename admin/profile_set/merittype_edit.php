<?php
# using: yat

########### Change Log			
#
#	Date:	2010-09-20	YatWoon
#			can update "Warning" wording 
#
##############################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
intranet_opendb();
// $li_menu from adminheader_intranet.php

$lstudentprofile = new libstudentprofile();

if ($intranet_session_language == "en")
{
	include($PATH_WRT_ROOT."lang/lang.b5.php");
	$i_Merit_Merit_b5 = $i_Merit_Merit;
	$i_Merit_MinorCredit_b5 = $i_Merit_MinorCredit;
	$i_Merit_MajorCredit_b5 = $i_Merit_MajorCredit;
	$i_Merit_SuperCredit_b5 = $i_Merit_SuperCredit;
	$i_Merit_UltraCredit_b5 = $i_Merit_UltraCredit;
		
	$i_Merit_BlackMark_b5 = $i_Merit_BlackMark;
	$i_Merit_MinorDemerit_b5 = $i_Merit_MinorDemerit;
	$i_Merit_MajorDemerit_b5 = $i_Merit_MajorDemerit;
	$i_Merit_SuperDemerit_b5 = $i_Merit_SuperDemerit;
	$i_Merit_UltraDemerit_b5 = $i_Merit_UltraDemerit;
	
	$i_Merit_Warning_b5 = $Lang['eDiscipline']['Warning'];
	
	include($PATH_WRT_ROOT."lang/lang.en.php");
	$i_Merit_Merit_en = $i_Merit_Merit;
	$i_Merit_MinorCredit_en = $i_Merit_MinorCredit;
	$i_Merit_MajorCredit_en = $i_Merit_MajorCredit;
	$i_Merit_SuperCredit_en = $i_Merit_SuperCredit;
	$i_Merit_UltraCredit_en = $i_Merit_UltraCredit;
		
	$i_Merit_BlackMark_en = $i_Merit_BlackMark;
	$i_Merit_MinorDemerit_en = $i_Merit_MinorDemerit;
	$i_Merit_MajorDemerit_en = $i_Merit_MajorDemerit;
	$i_Merit_SuperDemerit_en = $i_Merit_SuperDemerit;
	$i_Merit_UltraDemerit_en = $i_Merit_UltraDemerit;
	
	$i_Merit_Warning_en = $Lang['eDiscipline']['Warning'];
	
}
else if ($intranet_session_language == "b5")
{
	include($PATH_WRT_ROOT."lang/lang.en.php");
	$i_Merit_Merit_en = $i_Merit_Merit;
	$i_Merit_MinorCredit_en = $i_Merit_MinorCredit;
	$i_Merit_MajorCredit_en = $i_Merit_MajorCredit;
	$i_Merit_SuperCredit_en = $i_Merit_SuperCredit;
	$i_Merit_UltraCredit_en = $i_Merit_UltraCredit;
		
	$i_Merit_BlackMark_en = $i_Merit_BlackMark;
	$i_Merit_MinorDemerit_en = $i_Merit_MinorDemerit;
	$i_Merit_MajorDemerit_en = $i_Merit_MajorDemerit;
	$i_Merit_SuperDemerit_en = $i_Merit_SuperDemerit;
	$i_Merit_UltraDemerit_en = $i_Merit_UltraDemerit;
	
	$i_Merit_Warning_en = $Lang['eDiscipline']['Warning'];
	
	include($PATH_WRT_ROOT."lang/lang.b5.php");
	$i_Merit_Merit_b5 = $i_Merit_Merit;
	$i_Merit_MinorCredit_b5 = $i_Merit_MinorCredit;
	$i_Merit_MajorCredit_b5 = $i_Merit_MajorCredit;
	$i_Merit_SuperCredit_b5 = $i_Merit_SuperCredit;
	$i_Merit_UltraCredit_b5 = $i_Merit_UltraCredit;
		
	$i_Merit_BlackMark_b5 = $i_Merit_BlackMark;
	$i_Merit_MinorDemerit_b5 = $i_Merit_MinorDemerit;
	$i_Merit_MajorDemerit_b5 = $i_Merit_MajorDemerit;
	$i_Merit_SuperDemerit_b5 = $i_Merit_SuperDemerit;
	$i_Merit_UltraDemerit_b5 = $i_Merit_UltraDemerit;
	
	$i_Merit_Warning_b5 = $Lang['eDiscipline']['Warning'];
}

?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, 'index.php',$i_Profile_settings_merit_type_edit,'') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>

<form name="form1" action="merittype_edit_update.php" method="POST">
<table width="560" border="0" cellpadding="0" cellspacing="0" align="center">
<tr>
<td>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="5"><b><?=$i_Discipline_System_Merit?></b></td>
	</tr>
	<tr>
		<td class="tableTitle_new" valign="bottom"><b><?=$i_general_level?></b></td>
		<td colspan="2" class="tableTitle_new"><b><?=$i_general_english?></b></td>
		<td colspan="2" class="tableTitle_new"><b><?=$i_general_chinese?></b></td>
	</tr>
	<tr>
		<td class="tableTitle_new">&nbsp;</td>
		<td class="tableTitle_new"><?=$eEnrollment['current_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['new_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['current_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['new_title']?></td>
	</tr>
	<tr>
		<td><?=$i_general_lowest?></td>
		<td><?=$i_Merit_Merit_en?></td>
		<td><input class="text" type="text" name="merit_en" size="20" maxlength="128" value="<?=$i_Merit_Merit_en?>"></td>
		<td><?=$i_Merit_Merit_b5?></td>
		<td><input class="text" type="text" name="merit_b5" size="20" maxlength="128" value="<?=$i_Merit_Merit_b5?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_MinorCredit_en?></td>
		<td><input class="text" type="text" name="minorCredit_en" size="20" maxlength="128" value="<?=$i_Merit_MinorCredit_en?>"></td>
		<td><?=$i_Merit_MinorCredit_b5?></td>
		<td><input class="text" type="text" name="minorCredit_b5" size="20" maxlength="128" value="<?=$i_Merit_MinorCredit_b5?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_MajorCredit_en?></td>
		<td><input class="text" type="text" name="majorCredit_en" size="20" maxlength="128" value="<?=$i_Merit_MajorCredit_en?>"></td>
		<td><?=$i_Merit_MajorCredit_b5?></td>
		<td><input class="text" type="text" name="majorCredit_b5" size="20" maxlength="128" value="<?=$i_Merit_MajorCredit_b5?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_SuperCredit_en?></td>
		<td><input class="text" type="text" name="superCredit_en" size="20" maxlength="128" value="<?=$i_Merit_SuperCredit_en?>"></td>
		<td><?=$i_Merit_SuperCredit_b5?></td>
		<td><input class="text" type="text" name="superCredit_b5" size="20" maxlength="128" value="<?=$i_Merit_SuperCredit_b5?>"></td>
	</tr>
	<tr>
		<td><?=$i_general_highest?></td>
		<td><?=$i_Merit_UltraCredit_en?></td>
		<td><input class="text" type="text" name="ultraCredit_en" size="20" maxlength="128" value="<?=$i_Merit_UltraCredit_en?>"></td>
		<td><?=$i_Merit_UltraCredit_b5?></td>
		<td><input class="text" type="text" name="ultraCredit_b5" size="20" maxlength="128" value="<?=$i_Merit_UltraCredit_b5?>"></td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr><td><hr size="1" class="hr_sub_separator"></td></tr>
</table>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="5"><b><?=$Lang['eDiscipline']['Warning']?></b></td>
	</tr>
	<tr>
		<td class="tableTitle_new" valign="bottom"> </td>
		<td colspan="2" class="tableTitle_new"><b><?=$i_general_english?></b></td>
		<td colspan="2" class="tableTitle_new"><b><?=$i_general_chinese?></b></td>
	</tr>
	<tr>
		<td class="tableTitle_new">&nbsp;</td>
		<td class="tableTitle_new"><?=$eEnrollment['current_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['new_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['current_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['new_title']?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_Warning_en?></td>
		<td><input class="text" type="text" name="warning_en" size="20" maxlength="128" value="<?=$i_Merit_Warning_en?>"></td>
		<td><?=$i_Merit_Warning_b5?></td>
		<td><input class="text" type="text" name="warning_b5" size="20" maxlength="128" value="<?=$i_Merit_Warning_b5?>"></td>
	</tr>
</table>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr><td><hr size="1" class="hr_sub_separator"></td></tr>
</table>

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td colspan="5"><b><?=$i_Discipline_System_Demerit?></b></td>
	</tr>
	<tr>
		<td class="tableTitle_new" valign="bottom"><b><?=$i_general_level?></b></td>
		<td colspan="2" class="tableTitle_new"><b><?=$i_general_english?></b></td>
		<td colspan="2" class="tableTitle_new"><b><?=$i_general_chinese?></b></td>
	</tr>
	<tr>
		<td class="tableTitle_new">&nbsp;</td>
		<td class="tableTitle_new"><?=$eEnrollment['current_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['new_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['current_title']?></td>
		<td class="tableTitle_new"><?=$eEnrollment['new_title']?></td>
	</tr>
	<tr>
		<td><?=$i_general_lowest?></td>
		<td><?=$i_Merit_BlackMark_en?></td>
		<td><input class="text" type="text" name="blackMark_en" size="20" maxlength="128" value="<?=$i_Merit_BlackMark_en?>"></td>
		<td><?=$i_Merit_BlackMark_b5?></td>
		<td><input class="text" type="text" name="blackMark_b5" size="20" maxlength="128" value="<?=$i_Merit_BlackMark_b5?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_MinorDemerit_en?></td>
		<td><input class="text" type="text" name="minorDemerit_en" size="20" maxlength="128" value="<?=$i_Merit_MinorDemerit_en?>"></td>
		<td><?=$i_Merit_MinorDemerit_b5?></td>
		<td><input class="text" type="text" name="minorDemerit_b5" size="20" maxlength="128" value="<?=$i_Merit_MinorDemerit_b5?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_MajorDemerit_en?></td>
		<td><input class="text" type="text" name="majorDemerit_en" size="20" maxlength="128" value="<?=$i_Merit_MajorDemerit_en?>"></td>
		<td><?=$i_Merit_MajorDemerit_b5?></td>
		<td><input class="text" type="text" name="majorDemerit_b5" size="20" maxlength="128" value="<?=$i_Merit_MajorDemerit_b5?>"></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?=$i_Merit_SuperDemerit_en?></td>
		<td><input class="text" type="text" name="superDemerit_en" size="20" maxlength="128" value="<?=$i_Merit_SuperDemerit_en?>"></td>
		<td><?=$i_Merit_SuperDemerit_b5?></td>
		<td><input class="text" type="text" name="superDemerit_b5" size="20" maxlength="128" value="<?=$i_Merit_SuperDemerit_b5?>"></td>
	</tr>
	<tr>
		<td><?=$i_general_highest?></td>
		<td><?=$i_Merit_UltraDemerit_en?></td>
		<td><input class="text" type="text" name="ultraDemerit_en" size="20" maxlength="128" value="<?=$i_Merit_UltraDemerit_en?>"></td>
		<td><?=$i_Merit_UltraDemerit_b5?></td>
		<td><input class="text" type="text" name="ultraDemerit_b5" size="20" maxlength="128" value="<?=$i_Merit_UltraDemerit_b5?>"></td>
	</tr>
</table>



</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>

</table>
</form>
<?
$linterface = new interface_html();
echo $linterface->FOCUS_ON_LOAD("form1.merit_en");
include_once("../../templates/adminfooter.php");

?>
