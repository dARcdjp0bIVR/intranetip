<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
// $li_menu from adminheader_intranet.php
# $i_general_BasicSettings, 'settings.php',1,
#                        $i_Profile_settings_acl, 'teacher.php',1
?>

<?= displayNavTitle($i_adminmenu_fs, '', $i_Profile_settings, '') ?>
<?= displayTag("head_profile_set_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr>
<td>
<blockquote>
<? 
	echo displayOption(

                        $i_Profile_settings_display, 'display.php',1,
                        # added on 9 Sept 2008
                        $i_Profile_settings_merit_type_edit, 'merittype_edit.php',1,
                        
                        $i_Profile_settings_merit_type_select, 'merittype.php',1
                        // setting moved to front end attendance basic setting - by kenneth chung 2090826
                        // setting reactive in case client don't have student attendance module but using student profile
                        ,
                        $i_Profile_settings_attend_stat_method, 'attendstatmethod.php', (!$plugin['attendancestudent']),
                        $i_Profile_settings_AttednanceReason, 'teacher_setting.php', ($intranet_version=='2.5' && !$plugin['attendancestudent'])
	);

?>
</blockquote>
</td></tr>
</table>
<?
include_once("../../templates/adminfooter.php");

?>
