<?php
########### Change Log			
#
#	Date:	2010-09-20	YatWoon
#			can update "Warning" wording 
#
##############################

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_opendb();
$lf = new libfilesystem();

# get new title
$merit_en = stripslashes(intranet_htmlspecialchars(trim($merit_en)));
$merit_b5 = stripslashes(intranet_htmlspecialchars(trim($merit_b5)));
$minorCredit_en = stripslashes(intranet_htmlspecialchars(trim($minorCredit_en)));
$minorCredit_b5 = stripslashes(intranet_htmlspecialchars(trim($minorCredit_b5)));
$majorCredit_en = stripslashes(intranet_htmlspecialchars(trim($majorCredit_en)));
$majorCredit_b5 = stripslashes(intranet_htmlspecialchars(trim($majorCredit_b5)));
$superCredit_en = stripslashes(intranet_htmlspecialchars(trim($superCredit_en)));
$superCredit_b5 = stripslashes(intranet_htmlspecialchars(trim($superCredit_b5)));
$ultraCredit_en = stripslashes(intranet_htmlspecialchars(trim($ultraCredit_en)));
$ultraCredit_b5 = stripslashes(intranet_htmlspecialchars(trim($ultraCredit_b5)));

$blackMark_en = stripslashes(intranet_htmlspecialchars(trim($blackMark_en)));
$blackMark_b5 = stripslashes(intranet_htmlspecialchars(trim($blackMark_b5)));
$minorDemerit_en = stripslashes(intranet_htmlspecialchars(trim($minorDemerit_en)));
$minorDemerit_b5 = stripslashes(intranet_htmlspecialchars(trim($minorDemerit_b5)));
$majorDemerit_en = stripslashes(intranet_htmlspecialchars(trim($majorDemerit_en)));
$majorDemerit_b5 = stripslashes(intranet_htmlspecialchars(trim($majorDemerit_b5)));
$superDemerit_en = stripslashes(intranet_htmlspecialchars(trim($superDemerit_en)));
$superDemerit_b5 = stripslashes(intranet_htmlspecialchars(trim($superDemerit_b5)));
$ultraDemerit_en = stripslashes(intranet_htmlspecialchars(trim($ultraDemerit_en)));
$ultraDemerit_b5 = stripslashes(intranet_htmlspecialchars(trim($ultraDemerit_b5)));

$warning_b5 = stripslashes(intranet_htmlspecialchars(trim($warning_b5)));
$warning_en = stripslashes(intranet_htmlspecialchars(trim($warning_en)));

# set destinating file
$file_en = "$intranet_root/file/merit.en.customized.txt";
$file_b5 = "$intranet_root/file/merit.b5.customized.txt";

# construct file contents
$lines_en[0] = $merit_en;
$lines_en[1] = $minorCredit_en;
$lines_en[2] = $majorCredit_en;
$lines_en[3] = $superCredit_en;
$lines_en[4] = $ultraCredit_en;
$lines_en[5] = $blackMark_en;
$lines_en[6] = $minorDemerit_en;
$lines_en[7] = $majorDemerit_en;
$lines_en[8] = $superDemerit_en;
$lines_en[9] = $ultraDemerit_en;
$lines_en[10] = $warning_en;

$lines_b5[0] = $merit_b5;
$lines_b5[1] = $minorCredit_b5;
$lines_b5[2] = $majorCredit_b5;
$lines_b5[3] = $superCredit_b5;
$lines_b5[4] = $ultraCredit_b5;
$lines_b5[5] = $blackMark_b5;
$lines_b5[6] = $minorDemerit_b5;
$lines_b5[7] = $majorDemerit_b5;
$lines_b5[8] = $superDemerit_b5;
$lines_b5[9] = $ultraDemerit_b5;
$lines_b5[10] = $warning_b5;

# write contents to the files
$updatedcontent_en = implode("\n",$lines_en);
$lf->file_write($updatedcontent_en,$file_en);

$updatedcontent_b5 = implode("\n",$lines_b5);
$lf->file_write($updatedcontent_b5,$file_b5);


header("Location: merittype_edit.php?msg=2");


?>
