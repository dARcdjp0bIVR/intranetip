<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");

$li = new libfilesystem();

if (is_uploaded_file($slsfile))
{
    $li->file_copy($slsfile,$mapping_file_path);
    header("Location: index.php?upload=1");
}
else
{
    header("Location: index.php");
}
?>