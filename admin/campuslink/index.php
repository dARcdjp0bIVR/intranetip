<?php
include("../../includes/global.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");

$li = new libfilesystem();
$links = explode("\n", $li->file_read($intranet_root."/file/campuslink.txt"));
$num = sizeof($links)/2;
$max = max($num+5, 10);
?>

<form name="form1" action="update.php" method="post">
<?= displayNavTitle($i_adminmenu_adm, '',$i_adminmenu_im, '/admin/info/', $i_admintitle_sc_campuslink, '') ?>
<?= displayTag("head_campuslink_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr>
<td>
<blockquote>
<?=$i_CampusLinkDescription?>
<p><br>
<table border=0 cellpadding=5 cellspacing=0>
<tr>
<td class=tableTitle_new><u><?= $i_CampusLinkTitle ?></u></td>
<td class=tableTitle_new><u><?= $i_CampusLinkURL ?></u></td>
</tr>
<?php for($i=0; $i<$max; $i++) { ?>
<tr>
<td class=tableContent><input class=text type=text name=link_title[] size=30 maxlength=250 value="<?php echo $links[(2*$i)]; ?>"></td>
<td class=tableContent><input class=text type=text name=link_url[] size=30 maxlength=250 value="<?php echo $links[(2*$i)+1]; ?>"></td>
</tr>
<?php } ?>
</table>
</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>