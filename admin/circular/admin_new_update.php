<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$values = "";
$delim = "";
for ($i=0; $i<sizeof($targetUserID); $i++)
{
     $target = $targetUserID[$i];
     $values .= "$delim('$target',1,'$adminlevel')";
     $delim = ",";
}
$sql = "INSERT IGNORE INTO INTRANET_ADMIN_USER (UserID, RecordType, AdminLevel)
               VALUES $values";
$li->db_db_query($sql);
intranet_closedb();
header("Location: admin.php?msg=1");
?>