<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libcircular.php");

$lcircular = new libcircular();
$setting_file = $lcircular->setting_file;


$content = "";
$content .= "$disabled\n";
$content .= "$helpsign\n";
$content .= "$late\n";
$content .= "$resign\n";
$content .= "$numDays\n";
$content .= "$enableViewAll";

$li = new libfilesystem();
$li->file_write($content, $setting_file);
header("Location: settings.php?msg=2");
?>