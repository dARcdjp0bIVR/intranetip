<?php
// editing by 
/**************************************** Changes log *********************************************
 * 2014-11-07 (Carlos): Generate a random password for users that do not have encrypted password logged.
 * 						Password would be auto sync when user access iFolder. 
 * 2014-07-03 (Carlos): Use new approach to retrieve password if using hash password mechanism
 * 2014-05-07 (Carlos): Added suspend/unsuspend api call
 * 2013-08-01 (Carlos): set default_socket_timeout to a large value to prevent fopen timeouted 
 * 2011-10-19 (Carlos): Added Alumni
 * 2011-09-30 (Carlos): Cater schools that only use hashed password problem
 **************************************************************************************************/
ini_set('default_socket_timeout',60*60);
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libftp.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libpwm.php");
intranet_opendb();

$li = new libdb();
$lftp = new libftp();
$allowed_type_list = implode(",",$personalfile_identity_allowed);

if ($type==1)                # User Type
{
    if ($target == 1 || $target == 3 || $target == 4)
    {
        $sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, b.ACL, s.EncPassword FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID 
				   LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = a.UserID 
                   WHERE a.RecordType = $target
                         AND a.RecordType IN ($allowed_type_list)
                   ";
    }
    else
    {
        $sql = "SELECT a.UserID, a.UserLogin, a.UserPassword, b.ACL, s.EncPassword FROM INTRANET_USER as a
                   LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as b ON a.UserID = b.UserID 
				   LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = a.UserID 
                   WHERE a.RecordType = $target AND a.ClassName = '$ClassName'
                         AND a.RecordType IN ($allowed_type_list)
                   ";
    }
}
else if ($type == 2)
{
     $sql = "SELECT a.UserID, b.UserLogin, b.UserPassword, c.ACL, s.EncPassword 
        FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        LEFT OUTER JOIN INTRANET_SYSTEM_ACCESS as c ON a.UserID = c.UserID 
		LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID = b.UserID 
        WHERE a.GroupID = $target AND b.UserID IS NOT NULL
              AND b.RecordType IN ($allowed_type_list)
        ";
}
else
{
    header("Location: index.php");
    exit();
}

$quota += 0;
$users = $li->returnArray($sql,5);

if($intranet_authentication_method == 'HASH'){
	$libpwm = new libpwm();
	$uid_ary = Get_Array_By_Key($users,'UserID');
	$uidToPw = $libpwm->getData($uid_ary);
}

for ($i=0; $i<sizeof($users); $i++)
{
     list($uid, $userlogin, $password, $acl, $encPassword) = $users[$i];
     $userlogin = strtolower($userlogin);
     $password = trim($password);
     /*
     $encPassword = trim($encPassword);
     if($password == "" && $encPassword != ""){
     	$password = GetDecryptedPassword($encPassword);
     }
     */
     if($intranet_authentication_method == 'HASH'){
     	$password = $uidToPw[$uid];
     }
     
     if($password == ""){
		// generate a random password if password is not available
		$password = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'),0,10);
	 }
     
     if ($batchoption==1)              # Create accounts and link up
     {
         if ($lftp->isAccountExist($userlogin))
         {
	         $lftp->setTotalQuota($userlogin,$quota,"iFolder");
	         $lftp->setUnsuspendUser($userlogin,"iFolder",$password);
         }
         else
         {
             $lftp->open_account($userlogin, $password);
             $lftp->setTotalQuota($userlogin,$quota,"iFolder");
         }
         if ($acl == "")
         {
             $sql = "INSERT INTO INTRANET_SYSTEM_ACCESS (UserID, ACL) VALUES ($uid,2)";
         }
         else
         {
             $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL + 2 WHERE UserID = $uid AND ACL IN (0,1)";
         }
         $li->db_db_query($sql);
     }
     else if ($batchoption==2)   # unlink
     {
          $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 2 WHERE UserID = $uid AND ACL IN (2,3)";
          $li->db_db_query($sql);
          
          $lftp->setSuspendUser($userlogin,"iFolder");
     }
     else if ($batchoption==3)   # remove accounts
     {
          $sql = "UPDATE INTRANET_SYSTEM_ACCESS SET ACL = ACL - 2 WHERE UserID = $uid AND ACL IN (2,3)";
          $li->db_db_query($sql);
          $lftp->removeAccount($userlogin);
     }

}

if ($type==1)       # From Type List
{
    $url = "list_type.php?UserType=$target&ClassName=$ClassName&msg=2";
}
else if ($type==2)    # From Group List
{
    $url = "list_group.php?GroupID=$target&msg=2";
}
else     # From Login name input
{
    $url = "index.php";
}

intranet_closedb();
header("Location: $url");
?>