<?php
// editing by 
/***************************************** Change log *************************************************
 * 2011-10-19 (Carlos): added quota for Alumni
 ******************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
intranet_opendb();

$content = "$quota0\n$quota1\n$quota2";
if($special_feature['alumni']) $content .= "\n$quota3";

$lf = new libfilesystem();
$lf->file_write($content,"$intranet_root/file/account_file_quota.txt");

header("Location: default.php?msg=2");
?>