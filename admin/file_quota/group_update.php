<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libftp.php");
intranet_opendb();

$lftp = new libftp();
$li = new libdb();

if ($quota != "")
{
# Retrieve Quota
$list = $lftp->getQuotaTable("iFolder");
for ($i=0; $i<sizeof($list); $i++)
{
     //list($login,$delimiter,$used,$soft,$hard) = $list[$i];
     list($login,$used,$soft,$hard) = $list[$i];
     
     $current_quota[$login] = array($used,$soft);
}

if ($type == 0)
{
    $sql = "SELECT UserLogin FROM INTRANET_USER WHERE RecordType = $UserType ORDER BY ClassName, ClassNumber, EnglishName";
}
else if ($type == 1)
{
    $sql = "SELECT b.UserLogin
        FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        WHERE a.GroupID = $GroupID AND b.UserID IS NOT NULL
        ORDER BY b.ClassName, b.ClassNumber, b.EnglishName";
}
else
{
    header("Location: group.php");
    exit();
}
#print_r($current_quota);
$users = $li->returnVector($sql);
for ($i=0; $i<sizeof($users); $i++)
{
     $loginName = strtolower($users[$i]);
     $user_used = round($current_quota[$loginName][0],2);
     $user_quota = round($current_quota[$loginName][1],2);
     
     if (!isset($current_quota[$loginName]))
     {
          #echo "No Account\n";
     }
     else
     {
         if ( ($force == 1) || ($user_quota < $quota && $user_quota != 0) )
         {
               $succeed = $lftp->setTotalQuota($loginName,$quota,"iFolder");
         }
         else
         {
             #echo "Set $loginName to $quota (old: $user_quota , not set)\n";
             $new_quota = $quota + $user_quota;
			 $succeed = $lftp->setTotalQuota($loginName,$new_quota,"iFolder");
         }
     }
}


}

header("Location: group.php?msg=2");
?>