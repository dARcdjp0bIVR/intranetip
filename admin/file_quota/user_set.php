<?php
// editing by 
/************************************************** Change log **************************************************
 * 2011-10-19 (Carlos): Added alumni
 ****************************************************************************************************************/
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libftp.php");
intranet_opendb();
$lftp = new libftp();
$li = new libdb();

$sql = "SELECT UserID, RecordType FROM INTRANET_USER WHERE UserLogin = '$loginName'";
$temp = $li->returnArray($sql,2);
list($uid, $utype) = $temp[0];
if ($uid == "")
{
    header("Location: user.php?error=1");
    intranet_closedb();
    exit();
}

if (!in_array($utype,$personalfile_identity_allowed))
{
     header("Location: user.php?error=2");
     intranet_closedb();
     exit();
}
$loginName = strtolower($loginName);
$account_exists = $lftp->isAccountExist($loginName);

if ($account_exists)
{
     $quota = $lftp->getTotalQuota($loginName,"iFolder");
}
else
{
    $file_content = get_file_content($intranet_root."/file/account_file_quota.txt");
    if ($file_content == "")
    {
        $userquota = array(10,10,10,10);
    }
    else
    {
        $userquota = explode("\n", $file_content);
        if($userquota[3]=="") $userquota[3] = 10; // Alumni is lately added, may not set any quota
    }
    $quota = $userquota[$utype-1];
}
#$used = $lwebmail->getUsedQuota($loginName);
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function checkform(obj)
{
         if (obj.quota.value != '0' && !check_int(obj.quota,0,'<?=$i_LinuxAccount_Alert_QuotaMissing?>')) return false;
         return true;
}
</SCRIPT>


<form name="form1" action="user_set_update.php" method="post" onSubmit="return checkform(this)">
<?= displayNavTitle($i_admintitle_fs, '', $i_LinuxAccount_Folder_QuotaSetting, 'index.php',$i_LinuxAccount_SetUserQuota,'') ?>
<?= displayTag("head_storagequota_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><br>
<table width=400 border=0>
<tr><td align=right><?=$i_LinuxAccount_AccountName?>:</td><td><?=$loginName?></td></tr>
<? if ($account_exists) { ?>
<tr><td align=right><?=$i_LinuxAccount_CurrentQuota?>:</td><td><?=$quota?></td></tr>
<? } else { ?>
<tr><td colspan=2 align=center><font COLOR=red><?=$i_LinuxAccount_NewCreation?></font></td></tr>
<? } ?>
<tr><td align=right><?=$i_LinuxAccount_Quota?>:</td><td><input type=text size=5 name=quota value=<?=$quota?>> MBytes </td></tr>
<?
	$sql = "SELECT ACL FROM INTRANET_SYSTEM_ACCESS WHERE UserID = $uid";
	$arr_linked = $li->returnVector($sql);

	if(sizeof($arr_linked)>0){
		$linked_val = $arr_linked[0];
	}

	if($linked_val == 2 || $linked_val == 3)
		$linked_checked = " CHECKED ";
	else
		$linked_checked = "";

?>
<tr><td align=right><?=$i_Files_LinkIfolder?>:</td><td><input type=checkbox name=linked value=1 <?=$linked_checked;?>>
<br>
<font color=red><?=$i_LinuxAccount_UserSetNote?></font>
</td></tr>
</table>
</BLOCKQUOTE>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
</td>
</tr>
</table>
<input type=hidden name=loginName value="<?=$loginName?>">
<input type=hidden name=oldQuota value="<?=$quota?>">
<input type=hidden name=type value="<?=$type?>">
<input type=hidden name=target value="<?=$target?>">
<input type=hidden name=ClassName value="<?=$ClassName?>">
<input type=hidden name=uid value="<?=$uid?>">
<? if (!$account_exists) { ?>
<input type=hidden name=newAccount value=1>
<? } ?>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>