<?php
include("../../includes/global.php");
include("../../lang/email.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");
$li = new libfilesystem();
$LangID = $li->file_read($intranet_root."/file/language.txt");
$LangID += 0;

$personal_info_heading = array(
                            $i_UserTitle,
                            $i_UserNickName,
                            $i_UserPhoto,
                            $i_UserGender,
                            $i_UserDateOfBirth
                          );
                          
$contact_info_heading = array(
                            $i_UserHomeTelNo,
                            $i_UserOfficeTelNo,
                            $i_UserMobileTelNo,
                            $i_UserFaxNo,
                            $i_UserICQNo,
                            $i_UserAddress,
                            $i_UserCountry,
                            $i_UserURL,
                            $i_UserEmail
                          );
                          
$contact_info_disable = array_fill(0, count($contact_info_heading), 0);
if(isset($plugin['sms']) && $plugin['sms']){
  $contact_info_disable[2] = 1;
}

$message_heading = array(
                            $i_UserProfileMessage
                          );
                          
$password_heading = array(
                            $i_UserProfileLogin
                          );
                          
$display_heading = array(
                            $i_UserTitle,
                            $i_UserEmail,
                            $i_UserICQNo,
                            $i_UserHomeTelNo,
                            $i_UserFaxNo,
                            $i_UserDateOfBirth,
                            $i_UserAddress,
                            $i_UserCountry
                          );
for($i=0; $i<count($display_heading); $i++)
  $display_disable[$i] = array_fill(1, 3, 0);
$display_disable[0][2] = 1;
for($i=1; $i<count($display_heading); $i++)
  $display_disable[$i][3] = 1;

for($j=1; $j<=3; $j++)
{
  if(file_exists($intranet_root."/file/user_info_settings_".$j.".txt")){
    $misc = $li->file_read($intranet_root."/file/user_info_settings_".$j.".txt");
    
    $line = explode("\n",$misc);
    
    $personal_info_allow[$j] = explode(",",$line[0]);
    $contact_info_allow[$j] = explode(",",$line[1]);
    $message_allow[$j] = explode(",",$line[2]);
    $password_allow[$j] = explode(",",$line[3]);
    $display_allow[$j] = explode(",",$line[4]);
    
    array_shift($personal_info_allow[$j]);
    array_shift($contact_info_allow[$j]);
    array_shift($message_allow[$j]);
    array_shift($password_allow[$j]);
    array_shift($display_allow[$j]);
    
//    $title_checked[$j] = ($other_settings[$j][0]==1? "CHECKED":"");
  }
}

if(!isset($contact_info_allow))
{
  $misc = $li->file_read($intranet_root."/file/basic_misc.txt");
  $misca = explode("\n",$misc);
  $contact_info_allow[1][8] = $misca[0];
  $contact_info_allow[2][8] = $misca[0];
  $contact_info_allow[3][8] = $misca[0];
  $display_allow[1][0] = $misca[1];
  $display_allow[3][0] = $misca[1];
  $contact_info_allow[1][0] = $misca[2];
  $contact_info_allow[2][0] = $misca[2];
  $contact_info_allow[3][0] = $misca[2];
//  $email_checked = ($email_allowed==1? "CHECKED":"");
//  $display_settings[1] = ($title_disabled==1? "CHECKED":"");
//  $title_checked[3] = ($title_disabled==1? "CHECKED":"");
//  $home_tel_checked = ($home_tel_allowed==1? "CHECKED":"");
}

$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
if ($imgfile != "")
{
    $badge_checked = "CHECKED";
}

$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];
$school_org = $school_data[1];

$academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");
if ($academic_yr == "") $academic_yr = date("Y");

$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$number = sizeof($semester_data);
$number = ($number > 4 ? $number+2 : 6);
$semester_table = "<table width=100% border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1>\n";
$semester_table .= "<tr><td align='center' nowrap class='tableTitle_new'><u>$i_SettingsCurrentSemester</u></td><td align='center' class='tableTitle_new'><u>$i_SettingsSemesterList</u></td></tr>\n";
for ($i=0; $i<$number; $i++)
{
     if ($semester_data[$i] != "")
     {
         $linedata = split("::",$semester_data[$i]);
     }
     else
     {
         $linedata = array("","");
     }
     if ($linedata[1]==1)
     {
         $selected_str = "CHECKED";
     }
     else
     {
         $selected_str = "";
     }
     $semester_table .= "<tr><td align='center'><input type=radio name=currentsem value=$i $selected_str></td><td align='center'><input type=text name=semester[] length=40 value=\"".$linedata[0]."\"></td></tr>\n";
}
$semester_table .= "</table>\n";
$current_daytype = get_file_content("$intranet_root/file/schooldaytype.txt");
if ($current_daytype != 2 && $current_daytype != 3)
    $current_daytype = 1;

$daySelect = getSelectSchoolDayType("name=DayType",$current_daytype);


$popup_file = $li->file_read($intranet_root."/file/popupdate.txt");
$dates = explode("\n",$popup_file);

?>

<form name="form1" action="update_all.php" method="post" enctype="multipart/form-data">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_user_info_settings, '') ?>
<?= displayTag("head_user_info_settings_$intranet_session_language.gif", $msg) ?>

<!-- Number of fields for each category: -->
<input type="hidden" name="user_info_fields[]" value="<?=count($personal_info_heading)?>" />
<input type="hidden" name="user_info_fields[]" value="<?=count($contact_info_heading)?>" />
<input type="hidden" name="user_info_fields[]" value="<?=count($message_heading)?>" />
<input type="hidden" name="user_info_fields[]" value="<?=count($password_heading)?>" />
<input type="hidden" name="user_info_fields[]" value="<?=count($display_heading)?>" />

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_UserProfilePersonal?></span>]</span>
<table width=90% border=0 cellpadding=3 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_1?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_2?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_3?></div></td>
  </tr>
<?php
  for($i=0; $i<count($personal_info_heading); $i++)
  {
    echo "<tr>\n";
    echo "<td width=25%><div>".$personal_info_heading[$i]."</div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=personal_info_allowed_1[] value=".$i;
    echo $personal_info_allow[1][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";

    echo "<td width=25% align=\"center\"><div><input type=checkbox name=personal_info_allowed_2[] value=".$i;
    echo $personal_info_allow[2][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=personal_info_allowed_3[] value=".$i;
    echo $personal_info_allow[3][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";

    echo "</tr>\n";
  }
?>
</table>
</p>

<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_UserProfileContact?></span>]</span>
<table width=90% border=0 cellpadding=3 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_1?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_2?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_3?></div></td>
  </tr>
<?php
  for($i=0; $i<count($contact_info_heading); $i++)
  {
    echo "<tr>\n";
    echo "<td width=25%><div>".$contact_info_heading[$i]."</div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=contact_info_allowed_1[] value=".$i;
    echo $contact_info_disable[$i] == 1?" DISABLED":"";
    echo $contact_info_allow[1][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=contact_info_allowed_2[] value=".$i;
    echo $contact_info_disable[$i] == 1?" DISABLED":"";
    echo $contact_info_allow[2][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=contact_info_allowed_3[] value=".$i;
    echo $contact_info_disable[$i] == 1?" DISABLED":"";
    echo $contact_info_allow[3][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "</tr>\n";
  }
?>
</table>
</p>

<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_UserProfileMessage?></span>]</span>
<table width=90% border=0 cellpadding=3 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_1?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_2?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_3?></div></td>
  </tr>
<?php
  for($i=0; $i<count($message_heading); $i++)
  {
    echo "<tr>\n";
    echo "<td width=25%><div>".$message_heading[$i]."</div></td>\n";
      
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=message_allowed_1[] value=".$i;
    echo $message_allow[1][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";

    echo "<td width=25% align=\"center\"><div><input type=checkbox name=message_allowed_2[] value=".$i;
    echo $message_allow[2][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";

    echo "<td width=25% align=\"center\"><div><input type=checkbox name=message_allowed_3[] value=".$i;
    echo $message_allow[3][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";

    echo "</tr>\n";
  }
?>
</table>
</p>

<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_UserProfileLogin?></span>]</span>
<table width=90% border=0 cellpadding=3 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_1?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_2?></div></td>
    <td><div><?=$i_adminmenu_user_info_settings_disable_3?></div></td>
  </tr>
<?php
  for($i=0; $i<count($password_heading); $i++)
  {
    echo "<tr>\n";
    echo "<td width=25%><div>".$password_heading[$i]."</div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=password_allowed_1[] value=".$i;
    echo $password_allow[1][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=password_allowed_2[] value=".$i;
    echo $password_allow[2][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=password_allowed_3[] value=".$i;
    echo $password_allow[3][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";

    echo "</tr>\n";
  }
?>
</table>
</p>
<!--
<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_UserProfileLogin?></span>]</span>
<div><input type=checkbox name=email_allowed value=1 <?=$email_checked?>> <?=$i_adminmenu_basic_settings_email?></div>
<div><input type=checkbox name=home_tel_allowed value=1 <?=$home_tel_checked?>> <?=$i_adminmenu_basic_settings_home_tel?></div>
</p>
-->
<hr size=1 class="hr_sub_separator">

<p>
<span class="extraInfo">[<span class=subTitle><?=$i_adminmenu_user_info_settings_display?></span>]</span>
<table width=90% border=0 cellpadding=3 cellspacing=0>
  <tr>
    <td>&nbsp;</td>
    <td align="center"><div><?=$i_adminmenu_user_info_settings_display_disable_1?></div></td>
    <td align="center"><div><?=$i_adminmenu_user_info_settings_display_disable_2?></div></td>
    <td align="center"><div><?=$i_adminmenu_user_info_settings_display_disable_3?></div></td>
  </tr>
<?php
  for($i=0; $i<count($display_heading); $i++)
  {
    echo "<tr>\n";
    echo "<td width=25%><div>".$display_heading[$i]."</div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=display_allowed_1[] value=".$i;
    echo $display_disable[$i][1] == 1?" DISABLED":"";
    echo $display_allow[1][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=display_allowed_2[] value=".$i;
    echo $display_disable[$i][2] == 1?" DISABLED":"";
    echo $display_allow[2][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "<td width=25% align=\"center\"><div><input type=checkbox name=display_allowed_3[] value=".$i;
    echo $display_disable[$i][3] == 1?" DISABLED":"";
    echo $display_allow[3][$i] == 1?" CHECKED":" "; 
    echo "/></div></td>\n";
    
    echo "</tr>\n";
  }
?>
</table>
</p>

</blockquote>
</td>
</tr>

<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
</td>
</tr>
</table>
</form>

<?php
include("../../templates/adminfooter.php");
?>
