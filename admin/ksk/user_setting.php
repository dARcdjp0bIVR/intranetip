<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
include_once("../../includes/libksk.php");
intranet_opendb();

$ksk = new ksk();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
$order = ($order == 1) ? 1 : 0;
$cond = ($keyword=='')?"":" and (u.EnglishName like '%".$keyword."%' or u.ChineseName like '%".$keyword."%')";

$sql  = "SELECT ".getNamefieldByLang('u.')." as DisplayName, concat(u.ClassName, '-',u.ClassNumber),
		case subject 
			when 1 then concat('通識')
			when 2 then concat('數學')
			when 3 then concat('通識 & 數學')
		END as Subject,
		concat('<input type=\"checkbox\" name=\"UserIDs[]\" value=\"',u.UserID,'\">')	  
                FROM
                        INTRANET_USER AS u
                INNER JOIN 
                		KSK_USER AS ksk_u on u.UserID = ksk_u.UserID
                WHERE 1  $cond ";
          
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("DisplayName", "u.ClassName,u.ClassNumber","Subject");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_admintitle_group;
$li->wrap_array = array(20,20,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=25% class=tableTitle>".$li->column(0, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(1, $i_ClassNameNumber)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(2, $Lang['SysMgr']['FormClassMapping']['Subject'])."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("UserIDs[]")."</td>\n";


//if(!$ksk->hasUsedUpAllLicense() && $ksk->isInLicencePeriod()) 
{
	$toolbar = "<a class=iconLink href=\"javascript:newWindow('user_add.php',2)\">".newIcon()."$button_new</a>";
	//$toolbar .= "\n".toolBarSpacer()."<a class=iconLink href=\"javascript:checkPost(document.form1,'import.php')\">".importIcon()."$button_import</a>\n";
}

$functionbar = "<a href=\"javascript:checkEdit(document.form1,'UserIDs[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'UserIDs[]','user_delete_update.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
?>

<script language="JavaScript1.2">
function icon(id){
        url = "icon/index.php?GroupID=" + id;
        newWindow(url,2);
}
function info(id){
        url = "info/info.php?GroupID=" + id;
        newWindow(url,2);
}
function user(id){
        f = document.form1.filter.value;
        url = "info/index.php?filter=" + f + "&GroupID=" + id;
        self.location.href = url;
}
</script>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_fs, '', $Lang['kskgsmath']['kskgsmath'],'index.php',$i_LSLP['user_license_setting'],'') ?>



<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src=../../images/admin/table_head1.gif width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<br>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
