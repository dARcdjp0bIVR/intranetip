<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/fileheader_admin.php");
include_once($PATH_WRT_ROOT."includes/libksk.php");
intranet_opendb();

$ksk = new ksk();



$lo = new libgroup($GroupID);

$liuser = new libuser($UserID);

# retrieve group category

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
for ($i=0; $i<sizeof($cats); $i++)
{
     list($id,$name) = $cats[$i];
     if ($id!=0)
     {
         $x1 .= "<option value=$id ".(($CatID==$id)?"SELECTED":"").">$name</option>\n";
     }
}

$x1 .= "<option value=0>";
for($i = 0; $i < 20; $i++)
$x1 .= "_";
$x1 .= "</option>\n";
$x1 .= "<option value=0></option>\n";

$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";


$x1 .= "<option value=0></option>\n";
$x1 .= "</select>";

if($CatID==-1) // teacher
{
	$subject_type = '<br/><br/>'.$Lang['kskgsmath']['ManageSubject'].'<br/>
					<select id="Subject" name="Subject">
					<option value="1">通識</option>
					<option value="2">數學</option>
					<option value="3">通識 & 數學</option>
					</select>';
}

if($CatID!=0 && $CatID > 0) {
     $row = $li->returnCategoryGroups($CatID);

     $x2  = "<select name=ChooseGroupID[] size=5 multiple>\n";
     for($i=0; $i<sizeof($row); $i++){
          $GroupCatID = $row[$i][0];
          $GroupCatName = $row[$i][1];
          if ($GroupCatID != $GroupID)
          {
              $x2 .= "<option value=$GroupCatID";
              for($j=0; $j<sizeof($ChooseGroupID); $j++){
                  $x2 .= ($GroupCatID == $ChooseGroupID[$j]) ? " SELECTED" : "";
              }
              $x2 .= ">$GroupCatName</option>\n";
          }
     }
     $x2 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x2 .= "&nbsp;";
     $x2 .= "</option>\n";
     $x2 .= "</select>\n";
}

//selection of all classes
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;

   //if ($selectedUserType == 2 || $selectedUserType == 3)
   if ($selectedUserType == 3)	# 20081103 yat woon - no need class selection drop down box for Student option
   {
	   $x3 = $select_class;

	   if (isset($targetClass) && $targetClass!="")
	   {
		   if ($selectedUserType == 3)	//parent
		   {
			   	// get all students from the class
			   	$username_field = getNameFieldWithClassNumberByLang("");
                $sql = "SELECT UserID,$username_field
                		FROM INTRANET_USER
                		WHERE RecordType = 2
				                AND ClassName = '$targetClass'
                		ORDER BY ClassName, ClassNumber, TRIM(EnglishName)";
                $row = $li->returnArray($sql,2);

			    $parentID2DArr = array();
			    $parentIDArr = array();
			    $memberIDArr = array();
				//get all parent whose children is in that class
				for ($i=0; $i<sizeof($row); $i++)
				{
					$sql = "SELECT ParentID FROM INTRANET_PARENTRELATION WHERE StudentID = '".$row[$i][0]."'";
					$parentID2DArr[] = $li->returnVector($sql);
				}
				//change 2D array -> string list for SQL later
				foreach ($parentID2DArr as $list) {
				    $parentIDArr = array_merge($parentIDArr,$list);
				    $parentList = implode(",", $parentIDArr);
				}

				//get all club members
				$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID = '$GroupID'";
				$memberIDArr = $li->returnVector($sql);
				if (sizeof($memberIDArr))
				{
					$memberList = implode(",", $memberIDArr);
					$member_conds = " AND UserID NOT IN ($memberList) ";
				}

				//get all parents whose children is in the class and the parent himself has not joined the club yet
				$name_field = getNameFieldByLang("");
				$sql = "SELECT UserID, $name_field
						FROM INTRANET_USER
						WHERE UserID IN ($parentList) $member_conds ";

				$row = $li->returnArray($sql,2);
		   }
		   else
		   {
				//get all students in the class except those have joined the club already
		   		$row = $li->returnUserForTypeExcludeGroup(2,$GroupID, "AND ClassName = '$targetClass'");
		   }

		   $x4  = "<select name=ChooseUserID[] size=5 multiple>\n";
		   for($i=0; $i<sizeof($row); $i++)
		   		$x4 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
		   $x4 .= "<option>";
		   for($i = 0; $i < 40; $i++)
		   		$x4 .= "&nbsp;";
		   $x4 .= "</option>\n";
		   $x4 .= "</select>\n";
	   }
   }
   else
   {
	   $sql = "select UserID from KSK_USER";
		$user_obj = $li->returnVector($sql);
		if(sizeof($user_obj)>0)
		{
			$ksk_userid = implode(",",$user_obj);
			$extra_cond = " and UserID NOT IN (".$ksk_userid.")";
		}

	   $row = $li->returnUserForTypeExcludeGroup($selectedUserType,$GroupID,$extra_cond);

	   $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
	   for($i=0; $i<sizeof($row); $i++)
	   		$x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
	   $x3 .= "<option>";
	   for($i = 0; $i < 40; $i++)
	   		$x3 .= "&nbsp;";
	   $x3 .= "</option>\n";
	   $x3 .= "</select>\n";
   }


}
else if(isset($ChooseGroupID)) {

	# get ksk userid and exclude them
	$sql = "select UserID from KSK_USER";
	$ksk_user_obj = $li->returnVector($sql);

	# get teachers, parents and alumni userid and exclude them
	$sql = "select UserID from INTRANET_USER where RecordType in (1,3,4)";
	$teacher_user_obj = $li->returnVector($sql);

	$exclude_id_arr = array_merge($teacher_user_obj,$ksk_user_obj);

	if(is_array($exclude_id_arr))
		$exclude_id = implode(",",$exclude_id_arr);

	$row = $li->returnGroupUsersExcludeGroup($ChooseGroupID, $GroupID,'',$exclude_id);
    $x3  = "<select name=ChooseUserID[] size=5 multiple>\n";
    for($i=0; $i<sizeof($row); $i++)
    $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
    $x3 .= "<option>";
    for($i = 0; $i < 40; $i++)
    	$x3 .= "&nbsp;";
    $x3 .= "</option>\n";
    $x3 .= "</select>\n";
}
?>


<script language="JavaScript1.2">
function add_role(obj){
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          obj.role.value = role_name;
          obj.action = "add_role.php";
          obj.submit();
     }
}
function add_user(obj){
     obj.action = "user_add_update.php";
     obj.submit();
}

function import_update(){
        var obj = document.form1;
        checkOption(obj.elements["ChooseUserID[]"]);
        obj.action = "user_add_update.php";
        obj.submit();
}

function SelectAll()
{
        var obj = document.form1.elements['ChooseUserID[]'];
        for (i=0; i<obj.length; i++)
        {
          obj.options[i].selected = true;
        }
}

function expandGroup()
{
        var obj = document.form1;
        checkOption(obj.elements['ChooseGroupID[]']);
        obj.submit();
}

</script>

<form name="form1" action="user_add.php" method="post">
	<?= displayNavTitle($i_LSLP['user_list'], '',  $button_new, '') ?>

	<p style="padding-left:20px">
		<table width="422" border="0" cellpadding="0" cellspacing="0">
			<tr><td><img src="../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
			<tr>
				<td style="background-image: url(../../../images/admin/pop_bg.gif);" >

					<table width="422" border="0" cellpadding="0" cellspacing="0">
						<tr><td><img src="../../../images/admin/pop_bar.gif" width="422" height="16" border="0"></td></tr>
					</table>
					<table width="422" border="0" cellpadding="10" cellspacing="0">
						<tr>
							<td>
								<p>
								<?php echo $i_frontpage_campusmail_select_category; ?>:
								<br>
								<?php echo $x1; ?>
								<?php if($CatID!=0 && $CatID > 0) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_group; ?>:
									<br>
									<?php echo $x2; ?>
									<a href="javascript:expandGroup()"><img src="/images/admin/button/s_btn_expand_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
								<?=$subject_type?>
								<?php

								if(isset($ChooseGroupID)) { ?>
									<p>
									<?php echo $i_frontpage_campusmail_select_user; ?>:
									<br>
									<?php echo $x3; ?>
									<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
									<a href="javascript:SelectAll()"><img src="/images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
								<?php } ?>
							</td>
						</tr>
					</table>

				</td>
			</tr>
			<tr><td><img src="../../../images/admin/pop_bottom.gif" width="422" height="18" border="0"></td></tr>
			<tr>
				<td align=center height="40" style="vertical-align:bottom">
					<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
				</td>
			</tr>
		</table>
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/filefooter.php");
?>