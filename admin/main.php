<?php

################## Change Log [Start] ###################
# 
#	Date:	2014-009-16	[YatWoon]
#			Requested by Solution-Tony, update the eclassupdate link to /admin/eclassupdate/eu30/
#
#	Date:	2010-05-04	[YatWoon]
#			If "admin" login, then auto direct to eClass Update page
#
################## Change Log [End] ###################

include("../includes/global.php");
include("../lang/lang.$intranet_session_language.php");
include("../templates/adminheader.php");
?>
<br><br><br><br><br>
<TABLE WIDTH=100%>
<TR>
<TD ALIGN=CENTER><IMG SRC="/images/backend/admin_welcome_<?=$intranet_session_language?>.gif"></TD>
</TR>
</TABLE>
<br><br><br><br><br>
<?
include("../templates/adminfooter.php");
?>

<? if($PHP_AUTH_USER=="admin" && $special_feature['eclass_update']) {?>
<script language="javascript">
<!--
//top.intranet_admin_main.location.href = "/admin/eclassupdate/manual_update/index.php";
top.intranet_admin_main.location.href = "/admin/eclassupdate/eu30/index.php";
if(top.intranet_admin_menu.document.imgSetting != undefined)
{
	top.intranet_admin_menu.document.imgSetting.src = "/images/admin/header/tab_intranetsetting_h_<?=$intranet_session_language?>.gif";
}
//-->
</script>
<? } ?>
