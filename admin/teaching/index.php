<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libteaching.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");

intranet_opendb();

$lteaching = new libteaching();
$toolbar = "<a class=iconLink href=\"import.php\">".importIcon()."$button_import</a>";
?>

<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_teaching, '') ?>
<?= displayTag("head_teaching_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan="2"><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu colspan="2"><?php echo $toolbar; ?></td></tr>
<tr><td colspan="2"><?=$lteaching->displayAllTeachingTable(0)?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>