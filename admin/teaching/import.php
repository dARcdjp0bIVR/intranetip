<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_opendb();

$linterface = new interface_html();
?>

<script language="javascript">
function checkform(obj){
         return true;
}
</script>

<form name="form1" action="import_update.php" method="post" enctype="multipart/form-data" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_teaching,  'javascript:history.back()', $button_import, '') ?>
<?= displayTag("head_teaching_$intranet_session_language.gif", $msg) ?>

<blockquote>
<?= $i_select_file ?>: <input type=file name=userfile> <br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>

<blockquote>
<?=$i_Teaching_ImportInstruction?>
</blockquote>
<p><a class=functionlink_new href="<?= GET_CSV("sample_teaching.csv")?>"><?=$i_general_clickheredownloadsample?></a></p>
<p><a class=functionlink_new href="javascript:newWindow('classlist.php',1)"><?=$i_Teaching_ClassList?></a></p>
<p><a class=functionlink_new href="javascript:newWindow('subjectlist.php',1)"><?=$i_Teaching_SubjectList?></a></p>
</blockquote>


<p>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_import_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</p>
</form>

<?php
intranet_closedb();
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>