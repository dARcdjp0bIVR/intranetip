<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

# Update Class Teacher
$sql = "DELETE FROM INTRANET_CLASSTEACHER WHERE UserID = $tid";
$li->db_db_query($sql);
if ($classT != 0)
{
    $sql = "INSERT INTO INTRANET_CLASSTEACHER (UserID, ClassID) VALUES ($tid,$classT)";
    $li->db_db_query($sql);
}

# Update Subjects
$sql = "DELETE FROM INTRANET_SUBJECT_TEACHER WHERE UserID = $tid";
$li->db_db_query($sql);
for ($i=0; $i<$size; $i++)
{
     $classID = ${"class$i"};
     $subjectID = ${"subject$i"};
     if ($classID != 0 && $subjectID != 0)
     {
         $sql = "INSERT INTO INTRANET_SUBJECT_TEACHER (UserID, ClassID, SubjectID)
                 VALUES ($tid,$classID,$subjectID)";
         $li->db_db_query($sql);
     }
}

intranet_closedb();
header("Location: index.php?msg=2");

?>