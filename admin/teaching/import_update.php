<?php

$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

$li = new libdb();
$file_format = array("UserLogin","ClassTeacher","Class","Subject");

$limport = new libimporttext();

# Open the input data
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
        exit();
} else {
        $ext = strtoupper($lo->file_ext($filename));
        $format_wrong = true;
        if($limport->CHECK_FILE_EXT($filename))
        {
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   # drop the title bar

           #Check file format
           $format_wrong = false;
           for ($i=0; $i<sizeof($file_format); $i++)
           {
                if ($col_name[$i]!=$file_format[$i])
                {
                    $format_wrong = true;
                    break;
                }
           }
        }

        if ($format_wrong)
        {
            $display .= "<blockquote><br>$i_import_invalid_format <br>\n";
            $display .= "<table width='90%' border='1' bordercolor='#CCCCCC' align='center' cellpadding=0 cellspacing=0>\n";
            for ($i=0; $i<sizeof($file_format); $i++)
            {
                 $display .= "<tr><td>".$file_format[$i]."</td></tr>\n";
            }
            $display .= "</table></blockquote>\n";
        }
        else
        {

            # Retrieve valid teachers, classes and subjects
            $sql = "SELECT UserLogin,UserID FROM INTRANET_USER WHERE RecordType = 1 AND Teaching = 1";
            $temp = $li->returnArray($sql,2);
            $teachers = build_assoc_array($temp);

            $sql = "SELECT ClassName, ClassID FROM INTRANET_CLASS WHERE RecordStatus = 1";
            $temp = $li->returnArray($sql,2);
            $classes = build_assoc_array($temp);

            $sql = "SELECT SubjectName, SubjectID FROM INTRANET_SUBJECT WHERE RecordStatus = 1";
            $temp = $li->returnArray($sql,2);
            $subjects = build_assoc_array($temp);

            # Browse the input data, convert to array
            for ($i=0; $i<sizeof($data); $i++)
            {
                 $row_result = array();
                 $row = $data[$i];
                 $login = trim($row[0]);
                 if ($login=="") break;  # Assume End of file
                 $recordValid[$i] = true;
                 # Check login name
                 $teacher_id = $teachers[$login];
                 if ($teacher_id == "")
                 {
                     $recordValid[$i] = false;
                     $reason[$i] = "Invalid Login ($login)";
                     continue;
                 }

                 $class_teacher = trim($row[1]);
                 if ($class_teacher != "")
                 {
                     $class_teacher_id = $classes[$class_teacher];
                     if ($class_teacher_id == "")
                     {
                         $recordValid[$i] = false;
                         $reason[$i] = "Invalid Class name of class teacher data ($class_teacher)";
                         continue;
                     }
                 }
                 $row_result = array($teacher_id, $class_teacher_id);

                 $j = 2;
                 $subj_temp = array();
                 while (trim($row[$j])!="" && trim($row[$j+1])!="")
                 {
                        $class_name = trim($row[$j]);
                        $subject_name = trim($row[$j+1]);
                        $class_id = $classes[$class_name];
                        $subject_id = $subjects[$subject_name];
                        if ($class_id == "")
                        {
                            $recordValid[$i] = false;
                            $reason[$i] = "Invalid Class name of teaching data ($class_name)";
                            break;
                        }
                        if ($subject_id == "")
                        {
                            $recordValid[$i] = false;
                            $reason[$i] = "Invalid Subject name of teaching data ($subject_name)";
                            break;
                        }
                        $subj_temp[] = array($class_id, $subject_id);
                        $j += 2;
                 }
                 if ($recordValid[$i])
                 {
                     $row_result[] = $subj_temp;
                     $refined_data[] = $row_result;
                     $valid_teachers[] = $teacher_id;
                 }
            }

            # Remove current settings
            if (sizeof($valid_teachers)>0)
            {
                $teacher_list = implode(",",$valid_teachers);
                $sql = "DELETE FROM INTRANET_CLASSTEACHER WHERE UserID IN ($teacher_list)";
                $li->db_db_query($sql);
                $sql = "DELETE FROM INTRANET_SUBJECT_TEACHER WHERE UserID IN ($teacher_list)";
                $li->db_db_query($sql);
            }

            # Process Refined Data
            $ct_values = "";
            $ct_delim = "";
            $st_values = "";
            $st_delim = "";
            for ($i=0; $i<sizeof($refined_data); $i++)
            {
                 list($teacher_id, $class_id, $subjects_teaching) = $refined_data[$i];
                 # Insert Class Teacher
                 if ($class_id != "")
                 {
                     $ct_values .= "$ct_delim ($teacher_id, $class_id)";
                     $ct_delim = ",";
                 }
                 # Insert Subject Teacher
                 for ($j=0; $j<sizeof($subjects_teaching); $j++)
                 {
                      list($sub_class, $sub_subject) = $subjects_teaching[$j];
                      $st_values .= "$st_delim ($teacher_id, $sub_class, $sub_subject)";
                      $st_delim = ",";
                 }
            }
            # Execute SQL
            if($ct_values!=""){
	            $sql = "INSERT IGNORE INTO INTRANET_CLASSTEACHER (UserID, ClassID) VALUES $ct_values";
	            $li->db_db_query($sql);
            }
            if($st_values!=""){
	            $sql = "INSERT IGNORE INTO INTRANET_SUBJECT_TEACHER (UserID, ClassID, SubjectID) VALUES $st_values";
	            $li->db_db_query($sql);
            }

        }

        # Mark Reason to display
        for ($i=0; $i<sizeof($recordValid); $i++)
        {
             if (!$recordValid[$i])
             {
                  $display .= "<tr><td>Line ".($i+1)."</td><td>".$reason[$i]."</td></tr>\n";
             }
        }
        if ($display != "")
        {
            $display = "<table width=90% align=center border=0 cellspacing=0 cellpadding=0><tr><td>\n$display\n</td></tr></table>\n";
        }

}
intranet_closedb();

if ($display != '')
{
    include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
    include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
    echo displayNavTitle($i_adminmenu_sc, '', $i_adminmenu_adm_teaching,  'javascript:history.back()', $button_import, '');
    echo displayTag("head_teaching_$intranet_session_language.gif", $msg);
    if (!$format_wrong)
    {
         $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                        <tr><td><hr size=1></td></tr>
                        <tr><td align='right'><a href='javascript:history.go(-2)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
         echo "$i_Teaching_ImportFailed<br>";
    }
    else
    {
         $display .= "<table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
                        <tr><td><hr size=1></td></tr>
                        <tr><td align='right'><a href='javascript:history.go(-1)'><img src='/images/admin/button/s_btn_continue_$intranet_session_language.gif' border='0'></a></td></tr></table>";
    }
    echo $display;
    include_once($PATH_WRT_ROOT."templates/adminfooter.php");

}
else
	header("Location: index.php?msg=2");

?>