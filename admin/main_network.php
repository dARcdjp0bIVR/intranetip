<?php
include_once("../includes/global.php");
include_once("../lang/email.php");
include_once("../lang/lang.$intranet_session_language.php");
include_once("../templates/adminheader.php");

$network_conf_file = "$intranet_root/plugins/network_conf.php";
if (is_file($network_conf_file))
{
    include_once($network_conf_file);
}
//echo "<p><br><br><blockquote>Redirect to PHP Web Admin ...</blockquote></p>";

function displayNetOption()
{
        $x = "";
        $j = 0;

        $x .= "<table width='495' border='0' cellspacing='0' cellpadding='0'>\n";
        $numargs = func_num_args();
        for ($i=0; $i<$numargs; $i++) {
                $title = func_get_arg($i);
                $link = func_get_arg(++$i);
                $allow_access = func_get_arg(++$i);
                if ($allow_access)
                {
                        if ($i!=2)
                        {
                                $x .= "<tr><td><img src='/images/admin/net_menu_line.gif' height='10' width='495' border='0'></td></tr>\n";
                        }
                        $x .= "<tr><td align=center height='52' style=\"vertical-align:middle; background-image:url('/images/admin/net_menu.gif')\">".((trim($link)=="") ? $title : "<a class=functionlink_new href='$link'>$title</a>")."</td></tr>\n";
                }
        }
        $x .= "</table>\n";

        return $x;
}

function displayNetOptionArray($data)
{
        $x = "";
        $j = 0;

        $x .= "<table width='495' border='0' cellspacing='0' cellpadding='0'>\n";
        for ($i=0; $i<sizeof($data); $i++)
        {
                list($title,$link) = $data[$i];
                $x .= "<tr><td align=center height='52' style=\"vertical-align:middle; background-image:url('/images/admin/net_menu.gif')\">".((trim($link)=="") ? $title : "<a class=functionlink_new target=_blank href='$link'>$title</a>")."</td></tr>\n";
                if ($i!=sizeof($data))
                {
                    $x .= "<tr><td><img src='/images/admin/net_menu_line.gif' height='10' width='495' border='0'></td></tr>\n";
                }
        }
        $x .= "</table>\n";

        return $x;
}
?>

<script language="javascript">
function openNetwork(num)
{
        var win_name = "";
        var win_size = "";

        if (num==0)
        {
                url = "/images/admin/tmp/server.html";
                win_name = "ip_popup" + num;
                win_size = "resizable,scrollbars,top=40,left=40,width=850,height=460";
        } else if (num==1)
        {
                url = "/images/admin/tmp/firewall.html";
                win_name = "ip_popup" + num;
                win_size = "resizable,scrollbars,top=40,left=40,width=800,height=600";
        } else if (num==2)
        {
                url = "/images/admin/tmp/virusscan.html";
                win_name = "ip_popup" + num;
                win_size = "resizable,scrollbars,top=40,left=40,width=800,height=600";
        }


        window.open (url, win_name, win_size);
}
</script>

<br>
<br>
<br>
<br>
<center>
<?php
/*
displayNetOption("伺服器系統管理", "javascript:openNetwork(0)", 1,
                                "防火牆系統管理", "javascript:openNetwork(1)", 1,
                                "電腦病毒掃瞄系統管理", "javascript:openNetwork(2)", 1)
*/
if (sizeof($network_links)!=0)
{
    echo displayNetOptionArray($network_links);
}
else
{
    echo $Lang['SysMgr']['NetWorkContact']; 
}

?>
</center>

<?php
include_once("../templates/adminfooter.php");
?>