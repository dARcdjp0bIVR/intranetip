<?php
# using: 

/************************************
 *  Modification log
 *
 *	2020-04-30 Henry
 *	- added this setting
 * **********************************/

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libauth.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libgeneralsettings.php");
intranet_opendb();


$la = new libaccount();
$la->is_access_function($PHP_AUTH_USER);
if (!$la->is_access_system_security)
{
	die ("E001 :: Access denied! ");
}


// access IP from post
$AllowAccessIP = trim($_POST['AllowAccessIP']);

$lgs = new libgeneralsettings();

// get any saved access IP
$currentIP = $lgs->Get_General_Setting("AdminConsole", array("'AllowAccessIP'"));

// update
if(sizeof($currentIP)>0){
	$sql = "UPDATE GENERAL_SETTING 
				SET SettingValue='$AllowAccessIP', DateModified=NOW(), ModifiedBy='".$_SESSION['UserID']."' 
			WHERE Module='AdminConsole' AND SettingName='AllowAccessIP'";
}
// insert
else{
	$sql = "INSERT INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput, InputBy)
				VALUES ('AdminConsole', 'AllowAccessIP', '$AllowAccessIP', NOW(), '".$_SESSION['UserID']."') ";
}
$success = $lgs->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=2");
?>