<?php

$PATH_WRT_ROOT = "../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();

if(isset($GroupID)){
     $url = "/file/timetable/g".$GroupID."_timetable_special.csv";
     if (!file_exists($intranet_root.$url))
     {
          $url = "/file/timetable/g".$GroupID."_timetable.csv";
          if (!file_exists($intranet_root.$url))
          {
               $li = new libfilesystem();
               $li->file_write("\n", $intranet_root.$url);
          }
     }
}else{
     $url = "index.php";
}

if (is_file($intranet_root.$url))
{
	$handle = fopen($intranet_root.$url, "r");
	$contents = fread($handle, filesize($intranet_root.$url));
	
	$temp = explode("\n", $contents);
	
	for ($i = 0; $i < sizeof($temp); $i++) {
		$temp2[] = explode(",", $temp[$i]);
	}
	
	for ($i = 1; $i < sizeof($temp); $i++) {
		$result[] = explode(",", $temp[$i]);
	}
	
	
	$export_content = $lexport->GET_EXPORT_TXT($result, $temp2[0]);
	$filename = "g".$GroupID."_timetable.csv";
	$lexport->EXPORT_FILE($filename, $export_content);
	
	fclose($handle);
}


//header("Location: $url");
?>