<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/libgrouping.php");
include("../../includes/libfilesystem.php");
include("../../includes/libtimetablegroup.php");
include("../../includes/libbatch.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_intranet.php");
include("../../includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();
$li = new libgrouping();
$GroupID = (isset($GroupID)) ? $GroupID : $li->returnFirstGroup();

$lb = new libbatch();
$slots = $lb->slots;
for ($i=0; $i<sizeof($slots); $i++)
     $row[$i] = $slots[$i][2]."<br>\n".$slots[$i][1];

$lu = new libtimetablegroup();
$lu->setGroupID($GroupID);
$lu->setRow($row);

// TABLE FUNCTION BAR
$toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1,'import.php')\">".importIcon()."$button_import</a>\n".toolBarSpacer();
$toolbar .= "<a class=iconLink href=\"javascript:checkGet(document.form1,'export.php')\">".exportIcon()."$button_export</a>\n".toolBarSpacer();
$selection = "<select name=GroupID onChange=checkGet(document.form1,'index.php')>\n";
$selection .= $li->displayGroupsSelection($GroupID);
$selection .= "</select>\n";
$functionbar .= "<a href=\"javascript:checkGet(document.form1,'edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:if(confirm(globalAlertMsg3)){checkGet(document.form1,'remove.php')}\"><img src='/images/admin/button/t_btn_clear_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_gm, '', $i_adminmenu_gm_groupfunction, '/admin/groupfunction/', $i_admintitle_im_timetable, '') ?>
<?= displayTag("head_group_timetable_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $selection ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td colspan=2><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $toolbar; ?></td><td class=admin_bg_menu align=right><?php echo $functionbar; ?></td></tr>
<tr><td colspan=2><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td colspan=2 class=admin_bg_menu><?php echo $lu->display(); ?></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>