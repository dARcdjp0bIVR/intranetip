<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

intranet_opendb();

$lc = new libclubsenrol();
$info = $lc->getGroupInfoList();

$x .= "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>\n";
$x .= "<tr>
<td class=tableTitle width=300>$i_ClubsEnrollment_GroupName</td>
<td class=tableTitle width=70>$i_ClubsEnrollment_Quota</td>
<td class=tableTitle width=70>$i_ClubsEnrollment_StatusApproved</td>
<td class=tableTitle width=70>$i_ClubsEnrollment_SpaceLeft</td>
</tr>\n";
for ($i=0; $i<sizeof($info); $i++)
{
     list ($id,$name,$quota,$approved) = $info[$i];
     if ($quota == 0)
     {
         $quota = "$i_ClubsEnrollment_NoQuota";
         $left = "-";
     }
     else
     {
         $left = $quota - $approved;
         if ($left < 0) $left = 0;
     }
         $css = ($i%2) ? "2" : "";
     $x .= "<tr>
             <td class=tableContent$css><a class=functionlink href=\"member.php?GroupID=$id\">$name</a></td>
             <td class=tableContent$css>$quota</td>
             <td class=tableContent$css>$approved</td>
             <td class=tableContent$css>$left</td>
            </tr>\n";
}
if (sizeof($info)==0)
{
     $x .= "<tr>
             <td colspan=4 align=center class=tableContent$css>$i_no_record_exists_msg</td>
            </tr>\n";
}
$x .= "</table>\n";

$toolbar = "";
$toolbar = "<a href=\"javascript:clear2Next()\"><img src=\"$image_path/admin/button/s_btn_proceed2next_$intranet_session_language.gif\" border=0 alt=\"$i_ClubsEnrollment_Proceed2NextRound\"></a>";
if ($lc->mode==2)
{
    $toolbar .= " <a href=\"javascript:goLottery()\"><img src=\"$image_path/admin/button/s_btn_draw_$intranet_session_language.gif\" border=\"0\" alt=\"$i_ClubsEnrollment_GoLottery\"></a>";
    $toolbar .= " <a href=\"javascript:confirmFinal()\"><img src=\"$image_path/admin/button/s_btn_confirm_finalized_$intranet_session_language.gif\" border=\"0\" alt=\"$i_ClubsEnrollment_FinalConfirm\"></a>";
}

?>
<SCRIPT LANGUAGE=Javascript>
function clear2Next()
{
         if (confirm('<?=$i_ClubsEnrollment_Confirm_Clear2Next?>'))
         {
             document.form1.action = 'clear2next.php';
             document.form1.submit();
         }
}
function confirmFinal()
{
         if (confirm('<?=$i_ClubsEnrollment_Confirm_FinalConfirm?>'))
         {
             document.form1.action = 'confirm.php';
             document.form1.submit();
         }
}
function goLottery()
{
         if (confirm('<?=$i_ClubsEnrollment_Confirm_GoLottery?>'))
         {
             newWindow('lottery.php',1);
         }
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_enrollment_approval, 'index.php', $i_adminmenu_adm_enrollment_group, '') ?>
<?= displayTag("head_enrollment_$intranet_session_language.gif", $msg) ?>
<? if ($plugin['enrollment'] && !$plugin['eEnrollment']) { ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?=$toolbar?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<? } ?>
<form name=form1 action=''>
</form>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><a class=iconLink href="javascript:newWindow('allgroup.php',1)"><?=printIcon().$i_ClubsEnrollment_AllMemberList?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<?=$x?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<?php
include_once("../../templates/adminfooter.php");
?>