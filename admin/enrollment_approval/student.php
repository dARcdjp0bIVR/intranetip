<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");

intranet_opendb();

$lc = new libclubsenrol();
$enrolInfo = $lc->getEnrollmentInfo();
list($students,$received,$notSchool,$notOwn,$own) = $enrolInfo;
if ($students != $received)
{
    $linkNotHandin = "<a class=functionlink href=\"javascript:openList(1)\">$i_ClubsEnrollment_NotHandinList</a>";
}
else
{
    $linkNotHandin = "";
}
if ($notSchool != 0)
{
    $linkNotSchool = "<a class=functionlink href=\"javascript:openList(2)\">$i_ClubsEnrollment_NameList</a>";
}
else
{
    $linkNotSchool = "";
}
if ($notOwn != 0)
{
    $linkNotOwn = "<a class=functionlink href=\"javascript:openList(3)\">$i_ClubsEnrollment_NameList</a>";
}
else
{
    $linkNotOwn = "";
}
if ($own != 0)
{
    $linkSat = "<a class=functionlink href=\"javascript:openList(4)\">$i_ClubsEnrollment_NameList</a>";
}
else
{
    $linkSat = "";
}


$x .= "<table width=500 border=1 bordercolor='#F7F7F9' cellspacing=1 cellpadding=1 align=center>\n";
$x .= "<tr>
<td height=30 style='vertical-align:middle' class=tableTitle_new>$i_ClubsEnrollment_NumOfStudent </td><td style='vertical-align:middle' align=right>$students </td>
</tr>
<tr>
<td height=30 style='vertical-align:middle' class=tableTitle_new>$i_ClubsEnrollment_NumOfReceived $linkNotHandin</td><td style='vertical-align:middle' align=right>$received</td>
</tr>
<tr>
<td height=30 style='vertical-align:middle' class=tableTitle_new>$i_ClubsEnrollment_NumOfNotFulfilSchool $linkNotSchool</td><td style='vertical-align:middle' align=right>$notSchool</td>
</tr>
<tr>
<td height=30 style='vertical-align:middle' class=tableTitle_new>$i_ClubsEnrollment_NumOfNotFulfilOwn $linkNotOwn</td><td style='vertical-align:middle' align=right>$notOwn</td>
</tr>
<tr>
<td height=30 style='vertical-align:middle' class=tableTitle_new>$i_ClubsEnrollment_NumOfSatisfied $linkSat</td><td style='vertical-align:middle' align=right>$own</td>
</tr>\n";
$x .= "</table>\n";

?>
<SCRIPT LANGUAGE=Javascript>
function openList(type)
{
         newWindow('list.php?type='+type,1);
}
</SCRIPT>

<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_enrollment_approval, 'index.php', $Lang['eEnrolment']['StudentEnrolmentStatus'], '') ?>
<?= displayTag("head_enrollment_$intranet_session_language.gif", $msg) ?>


<?=$x?>

<?php
include_once("../../templates/adminfooter.php");
?>