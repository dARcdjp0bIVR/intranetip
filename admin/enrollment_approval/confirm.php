<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libaccess.php");
include_once("../../includes/libcampusmail.php");
include_once("../../lang/lang.$intranet_default_lang.php");      # Use default lang

intranet_opendb();

$lc = new libcampusmail();
$sql = "INSERT IGNORE INTO INTRANET_USERGROUP (UserID,GroupID)
        SELECT StudentID,GroupID FROM INTRANET_ENROL_GROUPSTUDENT
               WHERE RecordStatus = 2";
$lc->db_db_query($sql);

# Send Mail to notify students the result
$status_array = array($i_ClubsEnrollment_StatusWaiting,$i_ClubsEnrollment_StatusRejected,$i_ClubsEnrollment_StatusApproved);

# Send Mail for notifying
$laccess = new libaccess();
if ($laccess->retrieveAccessCampusmailForType(2))     # if can access campusmail
{

    $sql = "SELECT StudentID, Max, Approved, DateModified FROM INTRANET_ENROL_STUDENT";
    $students = $lc->returnArray($sql,4);

    for ($i=0; $i<sizeof($students); $i++)
    {
         list($sid,$max,$approved,$dateMod) = $students[$i];
         $sql = "SELECT a.Choice, b.Title, a.RecordStatus
                 FROM INTRANET_ENROL_GROUPSTUDENT as a
                      LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
                 WHERE a.StudentID = $sid ORDER BY Choice";
         $choices = $lc->returnArray($sql,3);

         $info = "";
         $info .= "$i_ClubsEnrollment_MailConfirm1\n";
         for ($j=0; $j<sizeof($choices); $j++)
         {
              list($cho,$name,$status) = $choices[$j];
              $info .= "$name ($i_ClubsEnrollment_MailChoice ".($j+1).") : ".$status_array[$status]."\n";
         }
         $info .= "$i_ClubsEnrollment_MailConfirm2\n$i_ClubsEnrollment_MailConfirm3\n";
    }
    $recipient = array($sid);
    $lc->sendMail($recipient,$i_ClubsEnrollment_MailConfirmSubject,$info);
}
intranet_closedb();
header("Location: group.php?msg=16");
?>