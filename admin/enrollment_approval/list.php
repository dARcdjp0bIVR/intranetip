<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");

intranet_opendb();

$lc = new libclubsenrol();
switch ($type)
{
        case 1:
             $studentList = $lc->returnNoHandinList();
             $heading = $i_ClubsEnrollment_HeadingNotHandinList;
             break;
        case 2:
             $studentList = $lc->returnNotFulfilSchoolList();
             $heading = $i_ClubsEnrollment_HeadingNotFulfilSchool;
             break;
        case 3:
             $studentList = $lc->returnNotFulfilOwnList();
             $heading = $i_ClubsEnrollment_HeadingNotFulfilOwn;
             break;
        case 4:
             $studentList = $lc->returnSatisfiedList();
             $heading = $i_ClubsEnrollment_HeadingSatisfied;
             break;
        default:
             $studentList = array();
             $heading = "Not Implemented";
             break;
}

$x .= "<table width=560 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>\n";
$x .= "<tr>
<td width=10 class=tableTitle>#</td>
<td class=tableTitle>$i_UserClassName</td>
<td class=tableTitle>$i_UserClassNumber </td>
<td class=tableTitle>$i_UserStudentName </td>
<td class=tableTitle>$i_UserLogin </td>
</tr>\n";
for ($i=0; $i<sizeof($studentList); $i++)
{
     $count = $i+1;
     $css = ($i%2==0? "":"2");
     list($id,$class,$classnumber,$name,$login) = $studentList[$i];
     $x .= "<tr class=tableContent$css><td width=10>$count</td><td>$class</td><td>$classnumber</td><td>$name</td><td>$login</td></tr>\n";
}
$x .= "</table>\n";

?>
<p><?=$heading?><br>
<br>
<?=$x?>
<input type=button class=button value="<?=$button_print?>" onClick="window.print()">
<?php
include_once("../../templates/filefooter.php");
?>