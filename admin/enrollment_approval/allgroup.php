<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");

intranet_opendb();

$lc = new libclubsenrol();
$groups = $lc->returnAllGroupMemberList();

$header .= "<table width=560 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>\n";
$header .= "<tr>
<td width=10 class=tableTitle>#</td>
<td class=tableTitle>$i_UserClassName</td>
<td class=tableTitle>$i_UserClassNumber </td>
<td class=tableTitle>$i_UserStudentName </td>
<td class=tableTitle>$i_UserLogin </td>
</tr>\n";
for ($i=0; $i<sizeof($groups); $i++)
{
     list($gid,$name,$quota,$members) = $groups[$i];
     if ($quota==0) $quota = $i_ClubsEnrollment_NoQuota;
     $size = sizeof($members);
     $x .= "$i_ClubsEnrollment_GroupName: $name \n<br>$i_ClubsEnrollment_Quota: $quota <br>
            $i_ClubsEnrollment_CurrentSize: $size";
     $x .= "$header";
     for ($j=0; $j<sizeof($members); $j++)
     {
          $count = $j+1;
          $css = ($j%2==0? "":"2");
          list($gid,$class,$classnumber,$name,$login) = $members[$j];
          $x .= "<tr class=tableContent$css><td width=10>$count</td><td>$class</td><td>$classnumber</td><td>$name</td><td>$login</td></tr>\n";
     }
     if (sizeof($members)==0)
     {
         $x .= "<tr><td colspan=5 align=center>$i_ClubsEnrollment_NoMember</td></tr>\n";
     }
     $x .= "</table>\n<br>\n";
}

?>
<br>
<?=$x?>
<input type=button class=button value="<?=$button_print?>" onClick="window.print()">
<?php
include_once("../../templates/filefooter.php");
?>