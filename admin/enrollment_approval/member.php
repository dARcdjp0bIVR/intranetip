<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libclubsenrol.php");
include_once("../../includes/libgroup.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
if ($GroupID == "")
{
    header ("Location: index.php");
    exit();
}
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$lg = new libgroup($GroupID);
$lclubsenrol = new libclubsenrol();

         $quotaDisplay = "$i_ClubsEnrollment_GroupName: ".$lg->Title."<br>\n";
         $groupQuota = $lclubsenrol->getQuota($GroupID);
         $approvedCount = $lclubsenrol->getApprovedCount($GroupID);
         if ($groupQuota == 0)
         {
             $quotaDisplay .= "$i_ClubsEnrollment_GroupQuotaNo";
         }
         else
         {
             $quotaDisplay .= "$i_ClubsEnrollment_GroupQuota1 : $groupQuota";
         }
         $quotaDisplay .= "<br>\n$i_ClubsEnrollment_GroupApprovedCount : $approvedCount";

switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
if ($filter == "") $filter = 2;
if ($filter != -1)
{
    $conds = "AND b.RecordStatus = $filter";
}
else
{
    $conds = "";
}
$sql = "SELECT
              a.UserLogin, a.EnglishName, a.ChineseName,
              IF(a.ClassNumber IS NULL OR a.ClassNumber = '', '-',
                 CONCAT(a.ClassName,' (',a.ClassNumber,')')) as Class,
              CASE b.RecordStatus
                   WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
                   WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
                   WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
              END,
              b.DateModified
        FROM INTRANET_USER as a, INTRANET_ENROL_GROUPSTUDENT as b
        WHERE a.UserID = b.StudentID AND b.GroupID = $GroupID $conds
        ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.UserLogin","a.EnglishName","a.ChineseName","Class","b.RecordStatus","b.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=18% class=tableTitle>".$li->column(0, $i_UserLogin)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(1, $i_UserEnglishName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(2, $i_UserChineseName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_ClassNameNumber)."</td>\n";
$li->column_list .= "<td width=12% class=tableTitle>".$li->column(4, $i_ClubsEnrollment_Status)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_ClubsEnrollment_LastSubmissionTime)."</td>\n";

$filter_array = array (
                       array (-1,$i_status_all),
                       array (0,$i_ClubsEnrollment_StatusWaiting),
                       array (1,$i_ClubsEnrollment_StatusRejected),
                       array (2,$i_ClubsEnrollment_StatusApproved)
                       );
$toolbar = getSelectByArray($filter_array,"name=filter onChange=this.form.submit()",$filter);
?>

<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_enrollment_approval, 'index.php', $i_adminmenu_adm_enrollment_group, 'javascript:history.back()') ?>
<?= displayTag("head_enrollment_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?=$quotaDisplay?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<form name="form1" ACTION="" METHOD="GET">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?=$li->display()?>
<input type=hidden name=field value="<?=$field?>">
<input type=hidden name=order value="<?=$order?>">
<input type=hidden name=pageNo value="<?=$pageNo?>">
<input type=hidden name=GroupID value="<?=$GroupID?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>