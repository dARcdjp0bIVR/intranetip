<?php
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/librole.php");
include("../../includes/libfilesystem.php");
include("../../includes/libaccount.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../templates/adminheader_setting.php");
intranet_opendb();

$li = new librole();
?>

<form action=set_update.php method=post>
<p class=admin_head><?php echo $i_admintitle_sc.displayArrow().$i_adminmenu_sc_group_settings.displayArrow(); ?><a href=javascript:history.back()><?php echo $i_admintitle_am_role; ?></a><?php echo displayArrow();?><?php echo $i_RoleDefault; ?></p>
<blockquote>
<?php echo $li->displaySetDefaultRole(); ?>
<input class=submit type=submit value="<?php echo $button_save; ?>"><input class=reset type=reset value="<?php echo $button_reset; ?>"><input class=button type=button value="<?php echo $button_cancel; ?>" onClick=history.back()>
</blockquote>
</form>

<?php
intranet_closedb();
include("../../templates/adminfooter.php");
?>