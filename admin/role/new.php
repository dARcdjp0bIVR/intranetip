<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();
$lgc = new libgroupcategory();
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_RoleTitle; ?>.")) return false;
        if(obj.RecordType.value == '') { alert('<?=$i_alert_pleaseselect.$i_GroupRecordType?>'); return false; }
}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_group_settings, '/admin/group_settings/', $i_admintitle_am_role, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_group_role_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_RoleTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100></td></tr>
<tr><td align=right><?php echo $i_RoleDescription; ?>:</td><td><textarea name=Description cols=30 rows=5></textarea></td></tr>
<tr><td align=right><?php echo $i_RoleRecordType; ?>:</td><td><?php echo $lgc->returnSelectCategory("name=RecordType",false,0,$filter); ?></td></tr>
</table>
<br>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../templates/adminfooter.php");
?>