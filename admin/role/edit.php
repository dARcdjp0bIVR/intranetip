<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/librole.php");
include_once("../../includes/libgroupcategory.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_setting.php");
intranet_opendb();

$li = new librole($RoleID[0]);
$lgc = new libgroupcategory($li->RecordType);
?>

<script language="javascript">
function checkform(obj){
        if(!check_text(obj.Title, "<?php echo $i_alert_pleasefillin.$i_RoleTitle; ?>.")) return false;
        if(obj.RecordType.value == '') { alert('<?=$i_alert_pleaseselect.$i_GroupRecordType?>'); return false; }
}
</script>

<form name="form1" action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_admintitle_sc, '', $i_adminmenu_sc_group_settings, '/admin/group_settings/', $i_admintitle_am_role, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_group_role_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right><?php echo $i_RoleTitle; ?>:</td><td><input class=text type=text name=Title size=30 maxlength=100 value="<?php echo $li->Title; ?>"></td></tr>
<tr><td align=right><?php echo $i_RoleDescription; ?>:</td><td><textarea name=Description cols=30 rows=5><?php echo $li->Description; ?></textarea></td></tr>
<tr><td align=right><?php echo $i_RoleRecordType; ?>:</td><td><?php echo (($li->RecordStatus==1) ? $lgc->CategoryName." <input type=hidden name=RecordType value='".$li->RecordType."'>" : $lgc->returnSelectCategory("name=RecordType",false,0,$li->RecordType) ); ?></td></tr>
</table>
<br>
</blockquote>
<input type=hidden name=RoleID value="<?php echo $li->RoleID; ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'>
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>