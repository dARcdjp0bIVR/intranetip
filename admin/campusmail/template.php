<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libdbtable.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if($field=="") $field = 3;
$RecordType = 1;
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.IsImportant", "a.IsNotification", "a.IsAttachment", "a.DateModified", "a.Subject");
$sql  = "SELECT
               IF(a.IsImportant=1,'$i_frontpage_campusmail_icon_important','<br>'),
               IF(a.IsNotification=1,'$i_frontpage_campusmail_icon_notification','<br>'),
               IF(a.IsAttachment=1,'$i_frontpage_campusmail_icon_attachment','<br>'),
               DATE_FORMAT(a.DateModified, '%Y-%m-%d %H:%i'),
               CONCAT('<a class=tableContentLink href=\"template_edit.php?CampusMailID=', a.CampusMailID, '\">', a.Subject, '</a>'),
               CONCAT('<input type=checkbox name=CampusMailID[] value=', a.CampusMailID ,'>')
          FROM INTRANET_CAMPUSMAIL AS a
          WHERE
               a.UserID = 0 AND
               a.UserFolderID = 1 AND
               a.Deleted!=1
               AND
               (a.Subject like '%$keyword%' OR a.Message like '%$keyword%')
          ";
# TABLE INFO
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = 2;
$li->title = "";
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=50 class=tableTitle>".$li->column_image(0, $i_frontpage_campusmail_important, $i_frontpage_campusmail_icon_important)."</td>\n";
$li->column_list .= "<td width=50 class=tableTitle>".$li->column_image(1, $i_frontpage_campusmail_notification, $i_frontpage_campusmail_icon_notification)."</td>\n";
$li->column_list .= "<td width=50 class=tableTitle>".$li->column_image(2, $i_frontpage_campusmail_attachment, $i_frontpage_campusmail_icon_attachment)."</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(3, $i_frontpage_campusmail_date)."</td>\n";
$li->column_list .= "<td width=70% class=tableTitle>".$li->column(4, $i_frontpage_campusmail_subject)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("CampusMailID[]")."</td>\n";
$li->column_array = array(0,0,0,0,0);
// TABLE FUNCTION BAR
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'CampusMailID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
# $searchbar .= "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
# $searchbar .= "<input class=submit type=submit value='$button_search'>\n";

$toolbar = "<a class=iconLink href=\"javascript:checkNew('compose.php')\">".newIcon()."$i_admintitle_im_campusmail_compose</a>";

$mail_index = (isset($mail_index)) ? $mail_index : 1;
$selectedOption[$mail_index] = "selected";
$selection = "<select onchange=\"window.location=this.options[this.selectedIndex].value+'?mail_index='+this.selectedIndex\">
<option value='outbox.php' {$selectedOption[0]}>$i_admintitle_im_campusmail_outbox</option>
<option value='template.php' {$selectedOption[1]}>$i_admintitle_im_campusmail_template</option>
<option value='trash.php' {$selectedOption[2]}>$i_admintitle_im_campusmail_trash</option>
</select>";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_admintitle_im, '/admin/info/', $i_admintitle_im_campusmail, '') ?>
<?= displayTag("head_campusmail_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $selection ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>
<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>

<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>