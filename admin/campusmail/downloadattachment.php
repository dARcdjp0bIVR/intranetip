<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libcampusmail.php");
intranet_opendb();

$valid = true;
if ($PartID == "")
{
    $valid = false;
}
else
{
    $li = new libdb();
    $sql = "SELECT AttachmentPath, FileName
                  FROM INTRANET_IMAIL_ATTACHMENT_PART as a
                       LEFT OUTER JOIN INTRANET_CAMPUSMAIL as b ON (a.CampusMailID = b.CampusMailID OR a.CampusMailID = b.CampusMailFromID)  AND b.UserID = 0
                  WHERE PartID = $PartID AND b.UserID IS NOT NULL
                       ";
    $files = $li->returnArray($sql,2);
    if (sizeof($files)==0)
    {
        $valid = false;
    }
}

intranet_closedb();

if ($valid)
{
    list($attachment_path, $filename) = $files[0];
    $target_filepath = "$intranet_root/file/mail/$attachment_path/$filename";
    $content = get_file_content($target_filepath);

    $encoded_name = urlencode($filename);
    if ($encoded_name == $filename && false)
    {
        $url = "/file/mail/$attachment_path/$filename";
    ?>
<html>
<head>
        <meta http-equiv="REFRESH" content="0; URL=<?=$url?>">
        <title><?=$filename?></title>
</head>
</html>
    <?
    }
    else
    {
        output2browser($content, $filename);

        /*
        // Output the file to user browser
        header("Content-type: $mime_type");
        header("Content-Length: ".strlen($content) );
        header("Content-Disposition: $content_disposition; filename=\"".$filename."\"");
        echo $content;
        */

     }

}
else
{
    echo "Bad Attachment/Mail already removed";
}
?>