<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libmerit.php");
include_once("../../../includes/libstudentprofile.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lmerit = new libmerit();
$class_name = $lmerit->getClassName($classid);
$lsp = new libstudentprofile();
?>

<script language="javascript">
function checkform(obj){
     if(countOption(obj.elements["Fields[]"])==0){ alert(globalAlertMsg18); return false; }
}
</script>

<form name="form1" action="classexport_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, 'index.php', $class_name, 'javascript:history.back()', $button_export, '') ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>
<blockquote>
<p><?php echo $i_export_msg2; ?>:<br>
<select name=Fields[] size=<?=$lsp->col_merit+$lsp->col_demerit?> multiple>
<? if (!$lsp->is_merit_disabled) { ?><option value=merit SELECTED><?= $i_Merit_Short_Merit ?></option><? } ?>
<? if (!$lsp->is_min_merit_disabled) { ?><option value=minorC SELECTED><?= $i_Merit_Short_MinorCredit ?></option><? } ?>
<? if (!$lsp->is_maj_merit_disabled) { ?><option value=majorC SELECTED><?= $i_Merit_Short_MajorCredit ?></option><? } ?>
<? if (!$lsp->is_sup_merit_disabled) { ?><option value=superC SELECTED><?= $i_Merit_Short_SuperCredit ?></option><? } ?>
<? if (!$lsp->is_ult_merit_disabled) { ?><option value=ultraC SELECTED><?= $i_Merit_Short_UltraCredit ?></option><? } ?>
<? if (!$lsp->is_black_disabled) { ?><option value=black SELECTED><?= $i_Merit_Short_BlackMark ?></option><? } ?>
<? if (!$lsp->is_min_demer_disabled) { ?><option value=minorD SELECTED><?= $i_Merit_Short_MinorDemerit ?></option><? } ?>
<? if (!$lsp->is_maj_demer_disabled) { ?><option value=majorD SELECTED><?= $i_Merit_Short_MajorDemerit ?></option><? } ?>
<? if (!$lsp->is_sup_demer_disabled) { ?><option value=superD SELECTED><?= $i_Merit_Short_SuperDemerit ?></option><? } ?>
<? if (!$lsp->is_sup_demer_disabled) { ?><option value=ultraD SELECTED><?= $i_Merit_Short_UltraDemerit ?></option><? } ?>
<option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option>
</select>
</blockquote>
</td></tr></table>

<input type=hidden name=start value="<?= $start ?>">
<input type=hidden name=end value="<?= $end ?>">
<input type=hidden name=reason value="<?= $reason ?>">
<input type=hidden name=classid value="<?= $classid ?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_export_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>

</form>

<?php
include("../../../templates/adminfooter.php");
?>