<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$reason = intranet_htmlspecialchars(trim($reason));
$remark = intranet_htmlspecialchars(trim($remark));
$meritdate = intranet_htmlspecialchars(trim($meritdate));
$qty = intranet_htmlspecialchars(trim($qty));
// $year = intranet_htmlspecialchars(trim($year));
// $semester = intranet_htmlspecialchars(trim($semester));

list($AcademicYearID, $year, $YearTermID, $semester) = getAcademicYearInfoAndTermInfoByDate($meritdate);

$update_fields = "";
$update_fields .= "MeritDate = '$meritdate'";
$update_fields .= ",Year = '$year'";
$update_fields .= ",Semester = '$semester'";
$update_fields .= ",NumberOfUnit = '$qty'";
$update_fields .= ",Reason = '$reason'";
$update_fields .= ",Remark = '$remark'";
$update_fields .= ",RecordType = '$type'";
$update_fields .= ",DateModified = now()";

# PersonInCharge
if (isset($PersonInCharge) && $PersonInCharge!='')
{
    $update_fields .= ",PersonInCharge = '$PersonInCharge'";
}
else
{
    $update_fields .= ",PersonInCharge = NULL";
}


$sql = "UPDATE PROFILE_STUDENT_MERIT SET $update_fields WHERE StudentMeritID = $StudentMeritID";
$li->db_db_query($sql);

intranet_closedb();
if ($page_from == "detail.php")
{
    header("Location: detail.php?class=$class&yrfilter=$yrfilter&pic=$pic&reason=$prev_reason&datetype=$datetype&date_from=$date_from&date_to=$date_to&msg=2");
}
else
{
    header("Location: studentview.php?studentid=$studentid&msg=2&classid=$classid");
}
?>