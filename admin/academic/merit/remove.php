<?php
// using: 
/******************************************
 * Date: 2013-03-18 (Rita)
 * Details: Add remove msg
 ******************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$list = implode(",",$StudentMeritID);
$sql = "DELETE FROM PROFILE_STUDENT_MERIT WHERE StudentMeritID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
if ($page_from == "detail.php")
{
    header("Location: detail.php?msg=3&class=$class&yrfilter=$yrfilter&pic=$pic&reason=$reason&datetype=$datetype&date_from=$date_from&date_to=$date_to");
}
else
{
    header("Location: studentview.php?classid=$classid&studentid=$studentid&msg=3");
}
?>