<?
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libassessment.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# +++ function for toolbar
function getYearOfLeft($attrib,$selected=""){
        global $button_select;
                   $li = new libdb();
        $sql = "SELECT DISTINCT YearOfLeft
                                        FROM INTRANET_ARCHIVE_USER
                                                WHERE YearOfLeft<>''";
        $result = $li->returnVector($sql);

        $x = "<SELECT $attrib>\n";
        $empty_selected = ($selected == '')? "SELECTED":"";

        $x .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
        for ($i=0; $i < sizeof($result); $i++)
        {
             $name = $result[$i];
             $selected_str = ($name==$selected? "SELECTED":"");
             $x .= "<OPTION value='$name' $selected_str>$name</OPTION>\n";
        }
        $x .= "</SELECT>\n";
        return $x;
}
function getClassOfFinalYear($YearOfLeft, $attrib,$selected=""){
        global $button_select;
                   $li = new libdb();
        $sql = "SELECT DISTINCT ClassName
                                        FROM INTRANET_ARCHIVE_USER
                                                WHERE YearOfLeft=\"$YearOfLeft\" AND ClassName<>\"\"
                                                ORDER BY ClassName ASC";
        $result = $li->returnVector($sql);

        $x = "<SELECT $attrib>\n";
        $empty_selected = ($selected == '')? "SELECTED":"";

        $x .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
        for ($i=0; $i < sizeof($result); $i++)
        {
             $name = $result[$i];
             $selected_str = ($name==$selected? "SELECTED":"");
             $x .= "<OPTION value='$name' $selected_str>$name</OPTION>\n";
        }
        $x .= "</SELECT>\n";
        return $x;
}
function getStudentName($YearOfLeft, $selectedClass, $attrib, $lang, $selected=""){
        global $button_select;
                   $li = new libdb();

        $sql = "SELECT UserID, ";
                                       if ( $lang=="en") $sql.= "CONCAT(EnglishName, ' (', ClassName, '-', ClassNumber, ')') ";
                                       if ( $lang=="b5") $sql.= "CONCAT(ChineseName, ' (', ClassName, '-', ClassNumber, ')') ";
        $sql .=        "FROM INTRANET_ARCHIVE_USER
                                                WHERE YearOfLeft=\"$YearOfLeft\" AND ClassName=\"$selectedClass\"
                                                ORDER BY ClassNumber ASC";

        $result = $li->returnArray($sql, 2);

        $x = "<SELECT $attrib>\n";
        $empty_selected = ($selected == '')? "SELECTED":"";

        $x .= "<OPTION value='' $empty_selected> -- $button_select -- </OPTION>\n";
        for ($i=0; $i < sizeof($result); $i++)
        {
             $UserId = $result[$i][0];
             $name = $result[$i][1];
             $selected_str = ($UserId==$selected? "SELECTED":"");
             $x .= "<OPTION value='$UserId' $selected_str>$name</OPTION>\n";
        }
        $x .= "</SELECT>\n";
        return $x;
}
# +++ end of function for toobar

# remove all archive record of NON-removed student
if ( $removeAll == "1" ){
        $li = new libstudentpromotion();
        $li->removeAllArchiveRecord();
        $removedMsg = $i_Profile_SucceedRemoveMsg;
}

$selectYOLBar = getYearOfLeft( "name=selectedYOL onChange=this.form.submit()", "$selectedYOL");

# control the class & student bar
$showClassBar=true;
$showStudentBar=true;

if ( $currentClassLevel<>$selectedYOL  ){
        $selectedClass="";
        $selectedStudent="";

        $showStudentBar=false;
}

if ( $currentClassName<>$selectedClass )
        $selectedStudent="";

if ( isset($selectedYOL))
        $selectClassBar = getClassOfFinalYear( $selectedYOL, "name=selectedClass onChange=this.form.submit()", "$selectedClass" );

if ( isset($selectedClass))
        $selectStudentBar = getStudentName( $selectedYOL, $selectedClass ,"name=selectedStudent onChange=this.form.submit()", $intranet_session_language, "$selectedStudent" );

# display student info
if ($selectedStudent != "" ){

                $now = time();
                $yearstart = date('Y-m-d',getStartOfAcademicYear($now));
                $yearend = date('Y-m-d',getEndOfAcademicYear($now));
                $currentYear = getCurrentAcademicYear();

                $lattend = new libattendance();
                $lmerit = new libmerit();
                $lact = new libactivity();
                $lservice = new libservice();
                $laward = new libaward();
                $lassess = new libassessment();
                $lfiles = new libstudentfiles();
                $lstudentprofile = new libstudentprofile();

                $lu = new libuser();
                $studentNameClassNumber = "$i_UserStudentName: ".$lu->ArchiveUserNameClassNumber($selectedStudent);

                /*
                $year_attend = $lattend->returnArchiveYears($selectedStudent);
                $year_merit = $lmerit->returnArchiveYears($selectedStudent);
                $year_act = $lact->returnYears($selectedStudent);
                $year_service = $lservice->returnYears($selectedStudent);
                $year_award = $laward->returnYears($selectedStudent);
                $year_assess = $lassess->returnYears($selectedStudent);
                $year_files = $lfiles->returnYears($selectedStudent);

                $year_array = array_merge($year_attend,$year_merit,$year_act,$year_service,$year_award,$year_access,$year_files);
                $year_array[] = $currentYear;
                $year_array = array_unique($year_array);
                rsort($year_array);

                $year_array = array_values($year_array);
                */

                /*
                if (!isset($year)){
                     $year = $currentYear;
                }
                */
                //$select_year = getSelectByValue($year_array,"name=year onChange=this.form.submit()",$year,1);

}

#general print tool bar
if ($selectedStudent != "" )
                $printBar .= "$general_select_year $general_select_sem<a class=iconLink href=\"javascript:openPrintPage()\">".printIcon()."$i_PrinterFriendlyPage</a>";
else if ($selectedYOL!= "" )
            $printBar .= "$general_select_year $general_select_sem<a class=iconLink href=\"javascript:openPrintAllPage()\">".printIcon()."$i_PrinterFriendlyPage</a>\n";

if ($intranet_session_language=="en")        $margin="30%";
else if ($intranet_session_language=="b5")        $margin="20%";
else        $margin="20%";

# remove all record link
if ( $selectedYOL == "" )
        $displayRemoveLink = "<a class=\"functionlink_warn\" href=\"javascript:checkArchiveRemove(document.form1, 'searchArchiveByYear.php') \">$i_Profile_ConfirmRemoveLink</a>";

if ( $selectedYOL=="")
        $showClassBar = false;

#^^^ display search bar ^^^#
$toolBar="";

# display search method
if ($searchMethod){

                $toolBar .= "<tr><td>$i_Profile_SearchMethod:</td><td><select name=searchMethod onChange=\"checkPost(document.form1, document.form1.searchMethod.value)\">".
                                                                "<option value=\"searchArchiveIndex.php\"> --  $button_select --</option>\n";

                if ( $searchMethod == "searchArchiveByYear.php")
                        $toolBar .= "<option value=\"searchArchiveByYear.php\" selected>$i_Profile_DataLeftYear, $i_Profile_ClassOfFinalYear</option>";
                else
                        $toolBar .= "<option value=\"searchArchiveByYear.php\" >$i_Profile_DataLeftYear, $i_Profile_ClassOfFinalYear</option>";

                if ( $searchMethod == "searchArchiveByName.php")
                        $toolBar .= "<option value=\"searchArchiveByName.php\" selected>$i_Profile_StudentName</option>";
                else
                        $toolBar .= "<option value=\"searchArchiveByName.php\">$i_Profile_StudentName</option>";


                $toolBar .="</select></td></tr>";

                $toolBar .="<tr><td colspan=2><hr size=1 class='hr_sub_separator'></td></tr>\n";
}

$toolBar .= "<tr><td width='$margin'></td><td></td>";

if ( $selectedStudent == "" && $printBar<>"")
        $toolBar .= "<tr><td width='$margin'></td><td align=right class=admin_bg_menu>$printBar</td></tr>\n";

$toolBar .="<tr><td>$i_Profile_DataLeftYear :</td><td>$selectYOLBar</td></tr>\n";

if ( isset($selectedYOL) && ($showClassBar==true))
        $toolBar .="<tr><td>$i_Profile_ClassOfFinalYear :</td><td>$selectClassBar</td></tr>\n";

if ( isset($selectedClass) && ($showStudentBar==true))
        $toolBar .="<tr><td >$i_Profile_SelectStudent :</td><td>$selectStudentBar</td></tr>\n";

if ( $removedMsg )
        $toolBar .="<tr><td colspan=2 align=right>$removedMsg</td></tr>\n";

$toolBar .="<tr><td colspan=2><hr size=1 class='hr_sub_separator'></td></tr>\n";

if ( $selectedYOL=="" )
        $toolBar .="<tr><td colspan=2 align=right>$displayRemoveLink</td></tr>\n";

if ( $selectedStudent != "" && $selectedYOL!= "" )
        $toolBar .="<tr><td align=right class=admin_bg_menu colspan=2>$printBar</td></tr>\n";

?>

<SCRIPT LANGUAGE=Javascript>
function viewAttend(id)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewMerit(id)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function openPrintPage()
{
         newWindow("archivestudentprint.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&class=<?=$selectedClass?>&selectedYOL=<?=$selectedYOL?>&year=<?=$year?>&sem=<?=$generalSem?>",4);
}
function openPrintAllPage()
{
         newWindow("archivestudentallprint.php?class=<?=$selectedClass?>&generalYear="+document.form1.selectedYOL.value,4);
}
function viewFormAns(id)
{
         newWindow('/admin/academic/assessment/view.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&RecordID='+id,1);
}
function viewAttendByYear(id,year)
{
         newWindow("/admin/academic/view_attendance.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewMeritByYear(id, year)
{
         newWindow("/admin/academic/view_merit.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewFile(id)
{
         newWindow("/admin/academic/view_file.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&RecordID="+id,11);
}
function checkArchiveRemove(obj,url){
        if(confirm("<?=$i_Profile_ConfirmRemoveAllArchiveRecord?>")){
               obj.removeAll.value=1;
           obj.action=url;
           obj.method = "get";
           obj.submit();
        }
}
</SCRIPT>

<form name="form1" action="">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, 'index.php', $i_Profile_DataLeftStudent, '') ?>
<!-- diplay icon--><?= displayTag("head_studentview_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<?=$toolBar?>

<!--display student info-->
<tr><td colspan=2 class=tableContent height=300 align=left>
<?php if ($selectedStudent != "") { ?>
<?=$studentNameClassNumber?>
<br>
<? if ($year == "") {
$lclass = new libclass();
?>
<?=$i_Profile_ClassHistory?>
<?=$lclass->displayLeftClassHistoryAdmin($selectedStudent)?>
<br>
<? } ?>
<br>
<? if ($li_menu->is_access_attendance) { ?>
<?=$i_Profile_Attendance?>
<?=$lattend->displayArchiveStudentRecordByYearAdmin($selectedStudent,"","")?>
<br>
<? }
   if ($li_menu->is_access_merit) {
?>
<?=$i_Profile_Merit?>
<?=$lmerit->displayArchiveStudentRecordByYearAdmin($selectedStudent,"","")?>
<br>
<? }
   if ($li_menu->is_access_service) {
?>
<?=$i_Profile_Service?>
<?=$lservice->displayArchiveServiceAdmin($selectedStudent,"","")?>
<br>
<? }
   if ($li_menu->is_access_activity) {
?>
<?=$i_Profile_Activity?>
<?=$lact->displayArchiveActivityAdmin($selectedStudent,"","")?>
<br>
<? }
   if ($li_menu->is_access_award) {
?>
<?=$i_Profile_Award?>
<?=$laward->displayArchiveAwardAdmin($selectedStudent,"","")?>
<br>
<? }
   if ($li_menu->is_access_assessment) {
?>
<?=$i_Profile_Assessment?>
<?=$lassess->displayArchiveAssessmentAdmin($selectedStudent,"","")?>
<br>
<? }
   if ($li_menu->is_access_student_files) {
?>
<?=$i_Profile_Files?>
<?=$lfiles->displayArchiveFileListAdmin($selectedStudent,"","")?>
<? } ?>
<br>
<br>
<?php } ?>
</td></tr>

</table>

<input name=currentClassLevel type=hidden value='<?=$selectedYOL?>'>
<input name=currentClassName type=hidden value='<?=$selectedClass?>'>
<input type=hidden name=removeAll value="">
<!--input type=hidden name=currentSearchMethod value=<?=$searchMethod?>-->
</form>

<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
