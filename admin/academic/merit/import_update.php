<?php
// Editing by 
#################################################
#	Date:	2017-12-05  Carlos
#			Fixed record type mapping convertion. 
#	Date:	2017-08-16  Simon
#			Update sql to get the targetYearClassName and targetYearClassNumber by select the TEMP_MERIT_IMPORT table
#
#	Date:	2013-07-05	YatWoon
#			Improved: no need to check student status [Case#2013-0704-1159-08073]
#
#################################################
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

// if($format==1)
// {
//                $fields_name = "UserID, MeritDate, Year, Semester, RecordType,NumberOfUnit, Reason,Remark, DateInput, DateModified,ClassName,ClassNumber,PersonInCharge";
//                $sql = "INSERT IGNORE INTO PROFILE_STUDENT_MERIT ($fields_name)
//                               SELECT a.UserID, b.MeritDate, b.Year,b.Semester,
//                                      CASE b.RecordType
//                                           WHEN 'C1' THEN '1'
//                                           WHEN 'C2' THEN '2'
//                                           WHEN 'C3' THEN '3'
//                                           WHEN 'C4' THEN '4'
//                                           WHEN 'C5' THEN '5'
//                                           WHEN 'D1' THEN '-1'
//                                           WHEN 'D2' THEN '-2'
//                                           WHEN 'D3' THEN '-3'
//                                           WHEN 'D4' THEN '-4'
//                                           WHEN 'D5' THEN '-5'
//                                           END,
//                                      b.Unit,b.Reason,b.Remark,now(),now(),a.ClassName,a.ClassNumber, c.UserID
//                                      FROM TEMP_MERIT_IMPORT as b LEFT OUTER JOIN INTRANET_USER as a ON a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND a.RecordType = 2 
//                                           LEFT OUTER JOIN INTRANET_USER as c ON b.PICLogin = c.UserLogin AND c.RecordType = 1 AND c.RecordStatus IN (0,1,2)
//                                      ";
// }
// if($format==2){
	
// 	$fields_name = "UserID, MeritDate, Year, Semester, RecordType,NumberOfUnit, Reason,Remark, DateInput, DateModified,ClassName,ClassNumber,PersonInCharge";
	
// 	               $sql = "INSERT IGNORE INTO PROFILE_STUDENT_MERIT ($fields_name)
//                               SELECT a.UserID, b.MeritDate, b.Year,b.Semester,
//                                      CASE b.RecordType
//                                           WHEN 'C1' THEN '1'
//                                           WHEN 'C2' THEN '2'
//                                           WHEN 'C3' THEN '3'
//                                           WHEN 'C4' THEN '4'
//                                           WHEN 'C5' THEN '5'
//                                           WHEN 'D1' THEN '-1'
//                                           WHEN 'D2' THEN '-2'
//                                           WHEN 'D3' THEN '-3'
//                                           WHEN 'D4' THEN '-4'
//                                           WHEN 'D5' THEN '-5'
//                                           END,
//                                      b.Unit,b.Reason,b.Remark,now(),now(),a.ClassName,a.ClassNumber, c.UserID
//                                      FROM TEMP_MERIT_IMPORT as b LEFT OUTER JOIN INTRANET_USER as a ON a.websamsregno = b.websamsregno AND a.RecordStatus IN (0,1,2)  AND a.RecordType = 2 
//                                           LEFT OUTER JOIN INTRANET_USER as c ON b.PICLogin = c.UserLogin AND c.RecordType = 1 AND c.RecordStatus IN (0,1,2)
//                                      ";
// }

if($format==1)
{
	$fields_name = "UserID, MeritDate, Year, Semester, RecordType,NumberOfUnit, Reason,Remark, DateInput, DateModified,ClassName,ClassNumber,PersonInCharge";
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_MERIT ($fields_name)
	SELECT  UserID, MeritDate, Year, Semester, 
			CASE RecordType 
	           WHEN 'C1' THEN '1'
	           WHEN 'C2' THEN '2'
	           WHEN 'C3' THEN '3'
	           WHEN 'C4' THEN '4'
	           WHEN 'C5' THEN '5'
	           WHEN 'D1' THEN '-1'
	           WHEN 'D2' THEN '-2'
	           WHEN 'D3' THEN '-3'
	           WHEN 'D4' THEN '-4'
	           WHEN 'D5' THEN '-5'
	        END, 
			Unit, Reason, Remark, now(), now(), targetYearClassName, targetYearClassNumber, PICLogin
	FROM TEMP_MERIT_IMPORT";

}
if($format==2){

	$fields_name = "UserID, MeritDate, Year, Semester, RecordType,NumberOfUnit, Reason,Remark, DateInput, DateModified,ClassName,ClassNumber,PersonInCharge";
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_MERIT ($fields_name)
	SELECT  UserID, MeritDate, Year, Semester, 
			CASE RecordType 
	           WHEN 'C1' THEN '1'
	           WHEN 'C2' THEN '2'
	           WHEN 'C3' THEN '3'
	           WHEN 'C4' THEN '4'
	           WHEN 'C5' THEN '5'
	           WHEN 'D1' THEN '-1'
	           WHEN 'D2' THEN '-2'
	           WHEN 'D3' THEN '-3'
	           WHEN 'D4' THEN '-4'
	           WHEN 'D5' THEN '-5'
	        END, 
			Unit, Reason, Remark, now(), now(), targetYearClassName, targetYearClassNumber, PICLogin
	FROM TEMP_MERIT_IMPORT";


}


	
$li->db_db_query($sql);

//debug_r($sql);
	
header("Location: index.php?msg=10");

intranet_closedb();

?>