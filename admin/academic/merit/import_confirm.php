<?php
# Under Development : 20091130
## Using By : 
##########################################################
## Modification Log
## 2017-08-17: Simon
## update sql to get the UserId
##
## 2017-08-16: Simon
## map the class name and class number with the student referring to academic year when import records
## includes libuser.php
##
## 2009-12-08: Max (200912081142)
## -Compare import file with English only
##########################################################
/**
 * Type			: Enhancement
 * Date 		: 200911301532
 * Description	: 1C) Add checking on Year and Semester
 * By			: Max Wong
 * C=CurrentIssue
 * Case Number	: 200911301532MaxWong
 */
$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once("../function.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_opendb();

$limport = new libimporttext();

$file_format1 = array("Year","Semester","Date","Class","Class Number","Type","Unit","Reason","Remark","PIC Login");
$file_format2 = array("Year","Semester","Date","WebSAMSRegNo","Type","Unit","Reason","Remark","PIC Login");

$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

/*************Override the wrong function existed in lib.php******************/
//function getCurrentAcademicYear2()
//{
//		global $intranet_root, $intranet_session_language;
//
//		//[PLEASE RELEASE THIS ONE WHEN TRYING TO PUT BACK TO LIB]include_once("libdb.php");
//		$li = new libdb();
//
//		$year = Get_Current_Academic_Year_ID();
//		$sql = "SELECT * FROM ACADEMIC_YEAR WHERE AcademicYearID=$year";
//		$academicInfo = $li->returnArray($sql);
//
//		//return Get_Lang_Selection($academicInfo[0]['YearNameB5'], $academicInfo[0]['YearNameEN']);
//		return ( $intranet_session_language == "en" ? $academicInfo[0]['YearNameEN'] :$academicInfo[0]['YearNameB5'] );
//
//         /*
//         ### commented by henry (20090727)
//         ### original coding in IP20 (START) ###
//
//         $academic_yr = get_file_content("$intranet_root/file/academic_yr.txt");
//         if ($academic_yr == "") $academic_yr = date("Y");
//         return $academic_yr;
//
//         ### original coding in IP20 (END) ###
//         */
//}
function getCurrentSemesterEN() {
	global $PATH_WRT_ROOT;
	$TermInfoArr = getCurrentAcademicYearAndYearTerm();
	$curYearTermID = $TermInfoArr['YearTermID'];

	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$objTerm = new academic_year_term($curYearTermID, false);
	$termNameEN = $objTerm->YearTermNameEN;
	return $termNameEN;
}

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {
		$SystemYearTermMapping = getAllAcademicYear_Term();

        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename))
        {
	       $error_code1 = false;
	       $error_code2 = false;
	       $error_code3 = false;
	       $error_code4 = false;
	       
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   

           #Check file format
           $format_wrong = false;
           if ($format==1)
           {
	           for ($i=0; $i<sizeof($file_format1); $i++)
	           {
	                if ($col_name[$i]!=$file_format1[$i])
	                {
	                    $format_wrong = true;
	                    break;
	                }
	           }
           }
           if ($format==2)
           {
	           for ($i=0; $i<sizeof($file_format2); $i++)
	           {
	                if ($col_name[$i]!=$file_format2[$i])
	                {
	                    $format_wrong = true;
	                    break;
	                }
	           }
           }
           if ($format_wrong)
           {
               //header ("Location: import.php");
               $error=1;
               $error_code=1;
               $error_code1 = true;
               
           }
           else
           {
               $t_delimiter = "";
               $toTempValues = "";
               $currentYear = getCurrentAcademicYear();
               //$currentSem = getCurrentSemester();
               $currentSem = getCurrentSemesterEN();
               $defaultUnit = 1;
               
               for ($i=0; $i<sizeof($data); $i++)
               {
	               if(is_array($data[$i]) && sizeof($data[$i])>0)
	               {
		               if ($format==1)
		               list($year,$sem,$date,$class,$classnum,$type,$unit,$reason,$remark,$pic) = $data[$i];
	
	                   if ($format==2)
	                   list($year,$sem,$date,$websamsregno,$type,$unit,$reason,$remark,$pic) = $data[$i];
	                    
	                   // echo "This is the year -> $year<br>This is the sem -> $sem<hr>";
	                
	                    $date = (($date == "")? "NULL": "'$date'");   
	                    $year = ($year == ""? $currentYear : $year);
	                    $sem = ($sem == ""? $currentSem : $sem);
	                    $class = trim(addslashes($class));
	                    $classnum = trim(addslashes($classnum));
	                    // trim any "#" at the beginning of the WebSam Reg. No.
	                    $websamsregno = ltrim(trim(addslashes($websamsregno)), "#");
	                    if (!empty($websamsregno))
	                    	$websamsregno = "#".$websamsregno;
	                    $unit = ($unit == ""? $defaultUnit : $unit);
	                    $reason = trim(addslashes($reason));
	                    $remark = trim(addslashes($remark));
	                    $pic = trim(addslashes($pic));
	                    
	                    # Get Record target Year Student ClassName, ClassNum
	                    $_classname = '';
	                    $_classnumber = '';
	                    $studentUserId	= '';
	                    $yearresult = '';
	                    $sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE YearNameEN = '$year' ";
	                    $yearresult = $li->returnVector($sql);
	                     
	                    $sqlresult = '';
	                    if(!empty($yearresult)){
	                    	$_academicYearID = $yearresult[0];
	                    }
	                    if($format==1){
	                    	$sql = "select userid from INTRANET_USER where classname= '".$class."' and classnumber=".$classnum." and RecordType = 2 AND RecordStatus IN (0,1,2)";
	                    	// add sql to get the userId where UserLogin = pic value
	                    	$sql1 = "select userid, englishname from INTRANET_USER where UserLogin = '".$pic."'";
	                    }
	                    else if ($format==2){
	                    	$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo = '".$websamsregno."' OR WebSAMSRegNo = '".ltrim($websamsregno, "#")."' AND RecordType = '2' AND RecordStatus IN ('0','1','2')";
	                    	// add sql to get the userId where UserLogin = pic value
	                    	$sql1 = "select userid, englishname from INTRANET_USER where UserLogin = '".$pic."'";
	                    }
	                    $sqlresult = $li->returnVector($sql);
	                    $studentUserId = $sqlresult[0];
	                    // return the userId value
	                    $returnResult = $li->returnVector($sql1);
	                    $userId = $returnResult[0];
	                     
	                    if(!empty($sqlresult)){
	                    	$libuser = new libuser($studentUserId);
	                    	$classinfoAry = $libuser->getStudentInfoByAcademicYear($_academicYearID);
	                    	$_classname = $classinfoAry[0]['ClassNameEn'];
	                    	$_classnumber = $classinfoAry[0]['ClassNumber'];
	                    }
	                    
	                     
	                    $targetYearClassName = $_classname;
	                    $targetYearClassNumber = $_classnumber;
	                    $picLogin = $userId;
	                    
	                    
	                    if ( $format==1 && ($class == "" || $classnum == "" || $type == ""))
	                	{
	                    	$error=1;
	                		$error_code=4;
	            		}
	                		
	                	if ($format==2 && ($websamsregno=="" || $type=="")	)
	                	{
	                    	$error=1;
	                		$error_code=4;
	                	}
	                	
	                	if ($date == "NULL")
							$date = "now()";
		                	
	                    if ($format==1){
	
		                   	 	$toTempValues .= "$t_delimiter ('$studentUserId',$date,'$year','$sem','$class','$classnum','$targetYearClassName','$targetYearClassNumber','$type','$unit','$reason','$remark','$picLogin')";
		                        $t_delimiter = ",";
	                    
	                 	}
	                 	if ($format==2){
	
		                 		$toTempValues .= "$t_delimiter ('$studentUserId',$date,'$year','$sem','$websamsregno','$targetYearClassName','$targetYearClassNumber','$type','$unit','$reason','$remark','$picLogin')";
		                        $t_delimiter = ",";
	
	                 	}	
                 	}
               }
				
               # Create temp table
               $sql = "DROP TABLE TEMP_MERIT_IMPORT";
			   $li->db_db_query($sql);
			   if ($format==1) {
	               $sql = "CREATE TABLE TEMP_MERIT_IMPORT (
	               			  UserID int(11),
                              MeritDate datetime,
                              Year char(20),
                              Semester varchar(255),
                              curClassName varchar(255),
                              curClassNumber varchar(255),
               				  targetYearClassName varchar(255),
                              targetYearClassNumber varchar(255),
                              RecordType char(2),
                              Unit int(11),
                              Reason text,
                              Remark text,
                              PICLogin varchar(255)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                }
                
                if ($format==2) {
                	$sql = "CREATE TABLE TEMP_MERIT_IMPORT (
                			  UserID int(11),
                              MeritDate datetime,
                              Year char(20),
                              Semester varchar(255),
                              WebSAMSRegNo varchar(100),
                			  targetYearClassName varchar(255),
                              targetYearClassNumber varchar(255),
                              RecordType char(2),
                              Unit int(11),
                              Reason text,
                              Remark text,
                              PICLogin varchar(255)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                }
               $li->db_db_query($sql);
               if ($format==1)
               $fields_name = "UserID, MeritDate, Year, Semester, curClassName, curClassNumber, targetYearClassName, targetYearClassNumber, RecordType, Unit, Reason, Remark, PICLogin";
               		
               if ($format==2)
               $fields_name = "UserID, MeritDate, Year,Semester, WebSAMSRegNo, targetYearClassName, targetYearClassNumber, RecordType, Unit,Reason,Remark,PICLogin";
               
               $sql = "INSERT IGNORE INTO TEMP_MERIT_IMPORT ($fields_name) VALUES $toTempValues";

               $li->db_db_query($sql);
               
               $sql ="SELECT ".$fields_name." FROM TEMP_MERIT_IMPORT";
               
               if($format==1)
//                		$original = $li->returnArray($sql,9);
               		$original = $li->returnArray($sql);
               		
               if($format==2)
//                		$original = $li->returnArray($sql,8);
               		$original = $li->returnArray($sql);
               		
	       	   //debug_r($original);
    		    $display = "<tr>
				<td class=tableTitle width=130>$i_Profile_Year</td>
				<td class=tableTitle width=60>$i_Profile_Semester</td>
				<td class=tableTitle width=60>$i_Merit_Date</td>";
				if ($format==2)
					$display .= "<td class=tableTitle width=60>$i_WebSAMS_Registration_No</td>";
				if ($format==1)
					{$display .= "<td class=tableTitle width=60>$i_ClassName</td>";
					$display .= "<td class=tableTitle width=80>$i_ClassNumber</td>";}
				$display .= "<td class=tableTitle width=90>$i_Merit_Type</td>
				<td class=tableTitle width=80>$i_Merit_Unit</td>
				<td class=tableTitle width=80>$i_Merit_Reason</td>
				<td class=tableTitle width=80>$i_Merit_Remark</td>
				<td class=tableTitle width=80>$i_Profile_PersonInCharge</td>
	
				<td class=tableTitle width=80>".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>
				</tr>\n";
               for ($i=0; $i<sizeof($original); $i++) {
               	$error_code1_this = false;
		        $error_code2_this = false;
		        $error_code3_this = false;
		        $error_code4_this = false;
               	$css = ($i%2? "":"2");
			   	$display .= "<tr class=tableContent$css><td>";
			   
					//CHECK USER INPUT YEAR AND SEM ACTUALLY MAP WITH SYSTEM SETTING (ACADEMIC_YEAR , ACADEMIC_YEAR_TERM)
				
					$yearSemIsValid = checkYearSemMapWithSystem($original[$i]['Year'],$original[$i]['Semester'],$SystemYearTermMapping);
//debug_r($SystemYearTermMapping);
					///////////////////////////////////
				if ($format==1) {
// 					list($date,$year,$sem,$class,$classnum,$type,$unit,$reason,$remark,$pic) = $original[$i];
					list($studentUserId,$date,$year,$sem,$class,$classnum,$targetYearClassName,$targetYearClassNumber,$type,$unit,$reason,$remark,$pic) = $original[$i];
// 			   		$sql = "select userid from INTRANET_USER where classname= '".$class."' and classnumber=".$classnum." and 
// 					RecordType = 2 AND RecordStatus IN (0,1,2)";			
// 					$result = $li->returnArray($sql,1);
						
			   		if ($type=="") {
			   			#$type="<font color=red><i>*</i></font>";
			   			$error_code4_this = true;
		   			}
			   		$display .=$year."</td><td>$sem&nbsp;</td><td>$date&nbsp;</td><td>$class&nbsp;</td><td>$classnum&nbsp;</td><td>$type&nbsp;</td><td>$unit&nbsp;</td><td>$reason&nbsp;</td><td>$remark&nbsp;</td><td>$pic&nbsp;</td>\n";
			   		if ($yearSemIsValid>-1) {
			    	} else {
						$error = 1;
					   	$error_code5_this=true;
					   	$error_num = "@ ";
					}
					if ($studentUserId== null || $studentUserId== '' || $studentUserId== '0'){
// 			    	if ($result==null){
						$error=1;
				   		$error_code=2;
				   		$error_code2_this=true;
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		if ($error_code2_this)
				   			$error_num .= "# ";
				   		$error_num = rtrim($error_num, " ");
			   			$display .="<td><font color=red>$error_num</font></td></tr>";
			   		} else {
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		$display .="<td>".(isset($error_num)?"<font color=red>$error_num</font>":"-")."</td></tr>";
			   		}
				}
				
				if ($format==2) {
// 					list($date,$year,$sem,$websamsregno,$type,$unit,$reason,$remark,$pic) = $original[$i];
					list($studentUserId,$date,$year,$sem,$websamsregno,$targetYearClassName,$targetYearClassNumber,$type,$unit,$reason,$remark,$pic) = $original[$i];
// 			   		$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo = '".$websamsregno."' OR WebSAMSRegNo = '".ltrim($websamsregno, "#")."' AND RecordType = '2' AND RecordStatus IN ('0','1','2')";			
// 					$result = $li->returnArray($sql,2);
					#echo "<pre>";
					#print_r($result);
					#echo "</pre>";
			   		#exit;
					if ($type=="" || $websamsregno=="") {
			   			$error_code4_this = true;
		   			}
					$display .=$year."</td><td>$sem&nbsp;</td><td>$date&nbsp;</td><td>$websamsregno&nbsp;</td><td>$type&nbsp;</td><td>$unit&nbsp;</td><td>$reason&nbsp;</td><td>$remark&nbsp;</td><td>$pic&nbsp;</td>\n";
					if ($yearSemIsValid>-1) {
			    	} else {
						$error = 1;
					   	$error_code5_this=true;
					   	$error_num = "@ ";
					}
					if ($studentUserId == null || $studentUserId == '' || $studentUserId== '0') {
// 			   		if ($result==null) {
				   		$error=1;
				   		$error_code=3;
				   		$error_code3_this=true;
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		if ($error_code3_this)
				   			$error_num .= "^ ";
				   		$error_num = rtrim($error_num, " ");
			   			$display .="<td><font color=red>$error_num</font></td></tr>";
			   		} else {
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		if ($error_code3_this)
				   			$error_num .= "^ ";
				   		$display .="<td>".(isset($error_num)?"<font color=red>$error_num</font>":"-")."</td></tr>";
			   		}
				}
				if ($error_code2_this)
					$error_code2=true;
				if ($error_code3_this)
					$error_code3=true;
				if ($error_code4_this)
					$error_code4=true;
				if ($error_code5_this)
					$error_code5=true;
				unset($error_num);
		   	}
           }
    
        }
        else
        {
            header("Location: import.php");
        }
}

include_once("../../../templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Merit, 'index.php', $button_import, '') ?>
<?= displayTag("head_merit_$intranet_session_language.gif", $msg) ?>


<form name="form1" method="GET" action="import_update.php">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>

<!--<?=($error?($format==1?$i_Merit_Import_NoMatch_Entry1:$i_Merit_Import_NoMatch_Entry2):$i_Payment_Import_Confirm)?>-->
<?
	if ($error==1)
	{
		if ($error_code5)
			$msg.="<font color=red>@</font> ".$i_Profile_Import_NoMatch_Entry[5]."<br />";
		if ($error_code4)
			$msg.="<font color=red>*</font> ".$i_Profile_Import_NoMatch_Entry[4]."<br />";
		if ($error_code1)
			$msg.="<font color=red>!</font> ".$i_Profile_Import_NoMatch_Entry[1]."<br />";
		if ($error_code2)
			$msg.="<font color=red>#</font> ".$i_Profile_Import_NoMatch_Entry[2]."<br />";
		if ($error_code3)
			$msg.="<font color=red>^</font> ".$i_Profile_Import_NoMatch_Entry[3]."<br />";
		#$msg=$i_Merit_Import_NoMatch_Entry[$error_code];
		
	}
	else
		$msg=$i_Payment_Import_Confirm;
?>
<?=$msg?>
</td></tr></table><br>
<?php if (!$error_code1) { ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$display?>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<?php } ?>
<table width=560 cellspacing=0 cellpadding=0>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?php if(!$error){?>
	<input type="image" alt="<?=$button_confirm?>" src="/images/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border='0'>
<?php } ?>
<a href=import.php?clear=1><img alt="<?=$button_cancel?>" border=0 src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirm value=1>
<input type=hidden name=format value=<?=$format?>>



</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
