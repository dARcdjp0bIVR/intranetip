<?php
include_once("../../includes/global.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

# remove all archive record of NON-removed student
if ( $removeAll == "1" ){
        $li = new libstudentpromotion();
        $li->removeAllArchiveRecord();
        $removedMsg = $i_Profile_SucceedRemoveMsg;
}

$searchMethodBar = "<select name=searchMethod onChange=\"checkPost(document.form1, document.form1.searchMethod.value)\">
                                                                                        <option value=0> --  $button_select --</option>
                                                                                        <option value=\"searchArchiveByYear.php\">$i_Profile_DataLeftYear, $i_Profile_ClassOfFinalYear</option>
                                                                                        <option value=\"searchArchiveByName.php\">$i_Profile_StudentName</option>
                                                                                </select>";

?>

<SCRIPT LANGUAGE=Javascript>
function checkArchiveRemove(obj,url){
        if(confirm("<?=$i_Profile_ConfirmRemoveAllArchiveRecord?>")){
               obj.removeAll.value=1;
           obj.action=url;
           obj.method = "get";
           obj.submit();
        }
}
</SCRIPT>

<form name="form1" action="">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, 'index.php', $i_Profile_DataLeftStudent, '') ?>
<!-- diplay icon--><?= displayTag("head_studentview_$intranet_session_language.gif", $msg) ?>

<table border=0 cellpadding=0 cellspacing=0 align="left">
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><?=$i_Profile_SearchMethod?>:</td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td><?=$searchMethodBar?></td></tr>
</table>
<br><br>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<?php if ( $removedMsg ) ?>
<tr><td align=right><?=$removedMsg?></td></tr>
<? php ?>
<tr><td><hr size=1 class='hr_sub_separator'></td></tr>
<tr><td align=right><a class="functionlink_warn" href="javascript:checkArchiveRemove(document.form1, 'searchArchiveIndex.php')"><?=$i_Profile_ConfirmRemoveLink?></a></td></tr>

</table>

<input type=hidden name=removeAll value="">
</form>
<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
