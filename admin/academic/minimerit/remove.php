<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libminimerit.php");
intranet_opendb();

/*
$li = new libdb();

$list = implode(",",$StudentMeritID);
$sql = "DELETE FROM PROFILE_STUDENT_MINI_MERIT WHERE StudentMeritID IN ($list)";
$li->db_db_query($sql);
*/

$lmerit = new libminimerit();

for($i=0; $i<sizeOf($StudentMeritID); $i++)
	$lmerit->removeOneStatus($StudentMeritID[$i]);

intranet_closedb();
if ($page_from == "detail.php")
{
    header("Location: detail.php?class=$class&yrfilter=$yrfilter&pic=$pic&reason=$reason&datetype=$datetype&date_from=$date_from&date_to=$date_to");
}
else
{
    header("Location: studentview.php?classid=$classid&studentid=$studentid&msg=3");
}
?>