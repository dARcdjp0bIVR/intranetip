<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libminimerit.php");
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;

$reason = intranet_htmlspecialchars(trim($reason));
$remark = intranet_htmlspecialchars(trim($remark));
$meritdate = intranet_htmlspecialchars(trim($meritdate));
$qty = intranet_htmlspecialchars(trim($qty));
$year = intranet_htmlspecialchars(trim($year));
$semester = intranet_htmlspecialchars(trim($semester));
$fieldname = "UserID, Year ,Semester, MeritDate, RecordType, Reason,Remark, DateInput,DateModified,ClassName,ClassNumber, RecordStatus";
$fieldvalue = "'$studentid','$year','$semester','$meritdate','$type','$reason','$remark',now(),now(),'$classname','$classnumber', 1";
# PersonInCharge
if (isset($PersonInCharge) && $PersonInCharge!='')
{
     $fieldname .= ",PersonInCharge";
     $fieldvalue .= ",'$PersonInCharge'";
}

$sql = "INSERT INTO PROFILE_STUDENT_MINI_MERIT ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

# update minor, major
$lmerit = new libminimerit();
$lmerit->synAllGradeUp($studentid);

intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
?>