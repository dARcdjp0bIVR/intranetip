<?php

$PATH_WRT_ROOT = "../../../";

include($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

#include_once($PATH_WRT_ROOT."includes/libmerit.php");
include_once($PATH_WRT_ROOT."includes/libminimerit.php");

include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$lexport = new libexporttext();

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? "\"".$school_data[0]."\"\n\n" : "";
$utf_school_name = (trim($school_data[0])!="") ? $school_data[0]."\r\n\r\n" : "";


$lmerit = new libminimerit();
$lword = new libwordtemplates();
$meritReasons = $lword->getWordListMerit();

$now = time();

if ($classid == "")
{
    $sql = "SELECT ClassID,ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
    $result = $lmerit->returnArray($sql,2);
    $classid = $result[0][0];
}

//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

/*
if ($start == "" || $end == "")
{
    $start = $yearstart;
    $end = $yearend;
}
*/
$class_name = $lmerit->getClassName($classid);
$result = $lmerit->getDetailedMeritListByClass($class_name,$start,$end,$reason);

$x_header = "";
for ($i=0; $i<sizeof($Fields); $i++)
{
	switch ($Fields[$i]) {
		/*
		case "merit": $x_header .= ",\"".$i_Merit_Short_Merit."\""; $f_merit=true; break;
		case "minorC": $x_header .= ",\"".$i_Merit_Short_MinorCredit."\""; $f_minorC=true; break;
		case "majorC": $x_header .= ",\"".$i_Merit_Short_MajorCredit."\""; $f_majorC=true; break;
		case "superC": $x_header .= ",\"".$i_Merit_Short_SuperCredit."\""; $f_superC=true; break;
		case "ultraC": $x_header .= ",\"".$i_Merit_Short_UltraCredit."\""; $f_ultraC=true; break;
		case "black": $x_header .= ",\"".$i_Merit_Short_BlackMark."\""; $f_black=true; break;
		case "minorD": $x_header .= ",\"".$i_Merit_Short_MinorDemerit."\""; $f_minorD=true; break;
		case "majorD": $x_header .= ",\"".$i_Merit_Short_MajorDemerit."\""; $f_majorD=true; break;
		case "superD": $x_header .= ",\"".$i_Merit_Short_SuperDemerit."\""; $f_superD=true; break;
		case "ultraD": $x_header .= ",\"".$i_Merit_Short_UltraDemerit."\""; $f_ultraD=true; break;
		*/

		case "warningD": 
			$x_header .= ",\""."Warning"."\""; 
			$utf_x_header .= "\t"."Warning"; 
			$f_warningD=true; 
			break;
		case "minorD": 
			$x_header .= ",\""."Minor Mistake"."\""; 
			$utf_x_header .= "\t"."Minor Mistake"; 
			$f_minorD=true; 
			break;
		case "majorD": 
			$x_header .= ",\""."Major Mistake"."\""; 
			$utf_x_header .= "\t"."Major Mistake"; 
			$f_majorD=true; 
			break;

	}
}
$x = "\"".$i_UserClassNumber."\",\"".$i_UserEnglishName."\"".$x_header."\n";
$utf_x = $i_UserClassNumber."\t".$i_UserEnglishName.$utf_x_header."\r\n";

for ($i=0; $i<sizeof($result); $i++)
{
     #list($id,$name,$classnumber,$merit,$minorC,$majorC,$superC,$ultraC,$black,$minorD,$majorD,$superD,$ultraD) = $result[$i];

     list($id,$name,$classnumber,$warningD,$minorD,$majorD) = $result[$i];

     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
	 $x .= "\"".$classnumber."\"";
	 $x .= ",\"".$name."\"";

	 /*
	 $x .= ($f_merit) ? ",".$merit : "";
	 $x .= ($f_minorC) ? ",".$minorC : "";
	 $x .= ($f_majorC) ? ",".$majorC : "";
	 $x .= ($f_superC) ? ",".$superC : "";
	 $x .= ($f_ultraC) ? ",".$ultraC : "";
	 $x .= ($f_black) ? ",".$black : "";
	 $x .= ($f_minorD) ? ",".$minorD : "";
	 $x .= ($f_majorD) ? ",".$majorD : "";
	 $x .= ($f_superD) ? ",".$superD : "";
	 $x .= ($f_ultraD) ? ",".$ultraD : "";
	 */

 	 $x .= ($f_warningD) ? ",".$warningD : "";
	 $x .= ($f_minorD) ? ",".$minorD : "";
	 $x .= ($f_majorD) ? ",".$majorD : "";

	 $x .= "\n";
	 
	 
	 $utf_x .= ($f_warningD) ? "\t".$warningD : "";
	 $utf_x .= ($f_minorD) ? "\t".$minorD : "";
	 $utf_x .= ($f_majorD) ? "\t".$majorD : "";

	 $utf_x .= "\r\n";
}

$functionbar = "\"$i_UserClassName:\",\"$class_name\"\n";
$utf_functionbar = "$i_UserClassName: \t$class_name\r\n";
$functionbar .= "\"$i_Attendance_Date:\",\"$i_Profile_From $start $i_Profile_To $end\"\n";
$utf_functionbar .= "$i_Attendance_Date: \t$i_Profile_From $start $i_Profile_To $end\r\n";
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$functionbar .= "\"$i_Attendance_Reason:\",\"$reasonSelect\"\n\n\n";
$utf_functionbar .= "$i_Attendance_Reason: \t$reasonSelect\r\n\r\n\r\n";

/*
if (!$g_encoding_unicode) {

	$url = "/file/export/eclass-user-".session_id()."-".time().".csv";
	$lo = new libfilesystem();
	$sExcel = $school_name.$functionbar.$x;
	$lo->file_write($sExcel, $intranet_root.$url);

	intranet_closedb();
	header("Location: $url");
} else {
	*/
	$filename = "eclass-user-".session_id()."-".time().".csv";
	$utf_sExcel = $utf_school_name.$utf_functionbar.$utf_x;
	$lexport->EXPORT_FILE($filename, $utf_sExcel);

	intranet_closedb();
//}


?>