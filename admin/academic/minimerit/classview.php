<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");

#include_once("../../../includes/libmerit.php");
include_once("../../../includes/libminimerit.php");

include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lmerit = new libminimerit();

### need to implement later
/*
$lword = new libwordtemplates();
$meritReasons = array_merge($lword->getWordListMerit(), $lword->getWordListDemerit());
*/

$now = time();

if ($classid == "")
{
    $sql = "SELECT ClassID,ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
    $result = $lmerit->returnArray($sql,2);
    $classid = $result[0][0];
}

//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

/*
if ($start == "" || $end == "")
{
    $start = $yearstart;
    $end = $yearend;
}
*/
$class_name = $lmerit->getClassName($classid);
$result = $lmerit->getDetailedMeritListByClass($class_name,$start,$end,$reason);

$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle rowspan=2>$i_UserClassNumber</td>
<td width=30% class=tableTitle rowspan=2>$i_UserEnglishName</td>
<td class=tableTitle align=center colspan=3>$i_Merit_Award</td>
</tr>
<tr>
<td class=tableTitle>"."Warning"."</td>
<td class=tableTitle>"."Minor Punish"."</td>
<td class=tableTitle>"."Major Punish"."</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
      list($id,$name,$classnumber,$warning, $minorP, $majorP) = $result[$i];
     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
     $display = $name;
     $link = "<a class=functionlink href=studentview.php?studentid=$id&classid=$classid>$display</a>";
	 $css = ($i%2) ? "2" : "";
     $x .= "<tr>
             <td class=tableContent$css>$classnumber</td>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$warning</td>
             <td class=tableContent$css>$minorP</td>
             <td class=tableContent$css>$majorP</td>
            </tr>\n";
}
$x .= "</table>\n";

/*
$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td class=tableTitle rowspan=2>$i_UserClassNumber</td>
<td width=15% class=tableTitle rowspan=2>$i_UserEnglishName</td>
<td class=tableTitle align=center colspan=5>$i_Merit_Award</td>
<td class=tableTitle align=center colspan=5>$i_Merit_Punishment</td>
</tr>
<tr>
<td class=tableTitle>$i_Merit_Short_Merit</td>
<td class=tableTitle>$i_Merit_Short_MinorCredit</td>
<td class=tableTitle>$i_Merit_Short_MajorCredit</td>
<td class=tableTitle>$i_Merit_Short_SuperCredit</td>
<td class=tableTitle>$i_Merit_Short_UltraCredit</td>
<td class=tableTitle>$i_Merit_Short_BlackMark</td>
<td class=tableTitle>$i_Merit_Short_MinorDemerit</td>
<td class=tableTitle>$i_Merit_Short_MajorDemerit</td>
<td class=tableTitle>$i_Merit_Short_SuperDemerit</td>
<td class=tableTitle>$i_Merit_Short_UltraDemerit</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$merit,$minorC,$majorC,$superC,$ultraC,$black,$minorD,$majorD,$superD,$ultraD) = $result[$i];
#    $display = ($classnumber != ""? "$classnumber. $name":"$name");
     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
     $display = $name;
     $link = "<a class=functionlink href=studentview.php?studentid=$id&classid=$classid>$display</a>";
	 $css = ($i%2) ? "2" : "";
     $x .= "<tr>
             <td class=tableContent$css>$classnumber</td>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$merit</td>
             <td class=tableContent$css>$minorC</td>
             <td class=tableContent$css>$majorC</td>
             <td class=tableContent$css>$superC</td>
             <td class=tableContent$css>$ultraC</td>
             <td class=tableContent$css>$black</td>
             <td class=tableContent$css>$minorD</td>
             <td class=tableContent$css>$majorD</td>
             <td class=tableContent$css>$superD</td>
             <td class=tableContent$css>$ultraD</td>
            </tr>\n";
}
$x .= "</table>\n";
*/

$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = $lmerit->getSelectClassID("name=classid onChange=\"this.form.submit();\"",$classid);

$daySelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$daySelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$daySelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$daySelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$daySelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$daySelect .= "</SELECT>\n";

$functionbar = "<table border=0>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectClass</td><td>$classSelect</td></tr>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectSemester:</td><td>$daySelect<br>$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To ";
$functionbar .= "<input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) </span> <a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a></td></tr>";
$reasonSelect = getSelectByValue($meritReasons,"name=reason onChange=this.form.submit()",$reason,1);
$functionbar .= "<tr><td nowrap>$i_Profile_SelectReason:</td><td>$reasonSelect</td></tr>\n";
$functionbar .= "</table>\n";

?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}
function openPrintPage()
{
	newWindow("classprint.php?classid=<?=$classid?>&start=<?=$start?>&end=<?=$end?>&reason=<?=$reason?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', "Mini Merit", 'index.php', $class_name, '') ?>
<?= displayTag("head_merit_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
<a class=iconLink href=import.php><?=importIcon().$button_import?></a>
<a class=iconLink href="javascript:checkGet(document.form1,'classexport.php')"><?=exportIcon().$button_export?></a>
<a class=iconLink href="javascript:openPrintPage()"><?=printIcon().$i_PrinterFriendlyPage?></a>
</td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>