<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libminimerit.php");
intranet_opendb();

$li = new libdb();
$lmerit = new libminimerit();

$reason = intranet_htmlspecialchars(trim($reason));
$remark = intranet_htmlspecialchars(trim($remark));
$meritdate = intranet_htmlspecialchars(trim($meritdate));
$year = intranet_htmlspecialchars(trim($year));
$semester = intranet_htmlspecialchars(trim($semester));

$update_fields = "";
$update_fields .= "MeritDate = '$meritdate'";
$update_fields .= ",Year = '$year'";
$update_fields .= ",Semester = '$semester'";
$update_fields .= ",Reason = '$reason'";
$update_fields .= ",Remark = '$remark'";
$update_fields .= ",RecordType = $type";
$update_fields .= ",DateModified = now()";

if ($originalType<>$type){
	$update_fields .= ",RecordStatus = 1";
	$update_fields .= ",ToID = NULL";
	$update_fields .= ",byUpgrade = 0";
}

# PersonInCharge
if (isset($PersonInCharge) && $PersonInCharge!=''){
    $update_fields .= ",PersonInCharge = '$PersonInCharge'";
}
else{
    $update_fields .= ",PersonInCharge = NULL";
}

$sql = "UPDATE PROFILE_STUDENT_MINI_MERIT SET $update_fields WHERE StudentMeritID = $StudentMeritID";
$li->db_db_query($sql);

# downgrade, upgrade minor, major
$lmerit->synAllGradeDown($studentid);
$lmerit->synAllGradeUp($studentid);

intranet_closedb();
?>

<script language="javascript">
function lastAction1(){
	window.opener.location.reload();
	self.close();
}
function lastAction2(){
	window.opener.location.href = "/admin/academic/minimerit/studentview.php?studentid=<?=$studentid?>";
	self.close();
}
</script>


<? if ($originalType==$type) { ?>
<body onLoad=javascript:lastAction1()>
<? } 
	  else{
?>
<body onLoad=javascript:lastAction2()>
<? } ?>
