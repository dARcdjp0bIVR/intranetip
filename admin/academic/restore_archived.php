<?
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
if ($StudentID == "")
{
    header("Location: searchArchiveIndex.php");
    exit();
}
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();



$lu = new libuser();
$archived_info = $lu->ReturnArchiveStudentInfo($StudentID);
list($student_name,$login,$class,$classnum,$yearOfLeft) = $archived_info;

$lc = new libclass();
$classes = $lc->getClassList();
$select_class = getSelectByArray($classes,"name=NewClassID","",0,1);

# Check login
$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE UserLogin = '$login'";
$temp = $lu->returnVector($sql);
$isDuplicated = ($temp[0]!=1);
?>

<SCRIPT LANGUAGE=Javascript>
function checkArchiveRemove(obj,url){
        if(confirm("<?=$i_Profile_ConfirmRemoveAllArchiveRecord?>")){
               obj.removeAll.value=1;
           obj.action=url;
           obj.method = "get";
           obj.submit();
        }
}
</SCRIPT>

<form name="form1" action="restore_archived_update.php" method=POST>
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, 'index.php', $i_Profile_DataLeftStudent, 'javascript:history.back()',$button_restore,'') ?>
<?= displayTag("head_studentview_$intranet_session_language.gif", $msg) ?>
<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?=$i_UserStudentName?>:</td><td><?=$student_name?></td></tr>
<tr><td align=right><?=$i_UserLogin?>:</td><td><?=$login?></td></tr>
<tr><td align=right><?=$i_ClassNameNumber?>:</td><td><?="$class - $classnum"?></td></tr>
<tr><td align=right><?=$i_StudentPromotion_NewClass?>:</td><td><?=$select_class?></td></tr>
<tr><td align=right><?=$i_StudentPromotion_NewClassNumber?>:</td><td><input type=text name=NewClassNumber size=10></td></tr>
<? if ($isDuplicated) { ?>
<tr><td align=right><?=$i_StudentPromotion_RestoreLogin?>:</td><td><input type=text name=NewLogin size=20 maxlength=50 value="<?=$login?>"></td></tr>
<? } else { ?>
<input type=hidden name=NewLogin value="<?=$login?>">
<? } ?>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
