<?php

################################################
#
#	Date:	2016-07-20 	Bill
#			Replace deprecated split() by explode() for PHP 5.4
#
#	Date:	2012-11-23	YatWoon
#			fixed: Failed to display school logo in file name with space [Case#2012-1122-1536-19132]
#
################################################

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");
// $li_menu from adminheader_intranet.php
intranet_opendb();

#######################################
# Customization (Not display last 2 cols) (0 - Show, 1 - Hide)
#
# Column1 - Year, Column2 - Merit, Column3 - Min_Merit, Column4 - Maj_Merit, Column5 - Sup_Merit
# Column6 - Ult_Merit, Column7 - Demerit, Column8 - Min_Demerit, Column9 - Maj_Demerit, Column10 - Sup_Demerit
# Column11 - Ult_Demerit
//$merit_col_not_display = array(0,0,0,0,0,0,0,0,0,1,1);
#######################################

#######################################
# Customization - Hide School Info Or Not (false - Show, true - Hide)
//$HideSchoolInfo = true;
#######################################

# Parameters of page
//Default value of number of line in each page
$defaultNumOfLine = 40;

//Default width of a specific field
$defaultFieldWidth1 = 53;

//Default width of table
$defaultTableWidth = 560;

//current number of line remaining
$lineRemain = $defaultNumOfLine;

//how many line need
$fieldLineNeed = 1;

# Page layout details
$page_breaker = "<P CLASS='breakhere'>";
$page_header_linefeed = 6;
$intermediate_pageheader = "<div STYLE=\"page-break-before: always\">&nbsp;</div>";
$intermediate_linefeed = 5;
?>

<STYLE TYPE="text/css">
	p.breakhere {page-break-before: always}
</STYLE>

<?

function GET_LINE_NEEDED($ParArray)
{
	global $defaultTableWidth, $defaultFieldWidth1;

	$length = 0;
	for ($i=0; $i<sizeof($ParArray); $i++)
		$length += strlen($ParArray[$i]);

	$ReturnVal = ceil($length / $defaultFieldWidth1);

	return $ReturnVal;
}


// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src='/file/$imgfile' width=120 height=60><br>\n" : "&nbsp;";

//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if($sys_custom['student_profile_report_no_header']) # For Customization Used Only
{
	#######################################
	# Customization (Not display school badge)
	$school_info = "<table border=0 width=650 height=60>\n";
	$school_info .= "<tr><td align=center>&nbsp;</td></tr>\n";
	$school_info .= "</table>\n<br>\n";
	#######################################
	
	# Move first row to top
	$school_info = "";
}
else
{
	if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
	{
		$school_info = "<table border=0 width=650>\n";
		$school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
		$school_info .= "</table>\n<br>\n";
	}
}

$li_menu = new libaccount();
$li_menu->is_access_function($PHP_AUTH_USER);
$lstudentprofile = new libstudentprofile();

# settings controling show/hide columns in merit/demerit table
$field_disabled_array = array(0,$lstudentprofile->is_merit_disabled,$lstudentprofile->is_min_merit_disabled,
        $lstudentprofile->is_maj_merit_disabled, $lstudentprofile->is_sup_merit_disabled,
        $lstudentprofile->is_ult_merit_disabled, $lstudentprofile->is_black_disabled,
        $lstudentprofile->is_min_demer_disabled, $lstudentprofile->is_maj_demer_disabled,
        $lstudentprofile->is_sup_demer_disabled, $lstudentprofile->is_ult_demer_disabled);

# Customarization for UCCKE to hide "homework" and "bad name" in print preview
# 6: homework column
# 7: badname column
$field_disabled_array[6] = $sys_custom['student_profile_report_print_no_homework']?true:$field_disabled_array[6];
$field_disabled_array[7] = $sys_custom['student_profile_report_print_no_badname']?true:$field_disabled_array[7];
								
$lclass = new libclass();
$lu = new libuser();
$case = 0;

if($class == ""){
	$result = $lu->ReturnArchiveStudentInfo($StudentID);
	$class = $result[2];
}

$toolbar = "$i_Profile_ClassOfFinalYear: $class <br>$i_PrinterFriendly_StudentName: ".$lu->ArchiveUserNameClassNumber($StudentID);
if ($StudentID != "")
{
	$now = time();
	$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
	$yearend = date('Y-m-d',getEndOfAcademicYear($now));
	
	$lattend = new libattendance();
	$lmerit = new libmerit();
	$lact = new libactivity();
	$lservice = new libservice();
	$laward = new libaward();
	
	if ( $selectedYOL == "" ){
		$result = $lu->ReturnArchiveStudentInfo($StudentID);
		$selectedYOL = $result[4];
	}
	
	$year_display = $selectedYOL;
}



?>
<SCRIPT LANGUAGE=Javascript>
function viewAttend(id)
{
	newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewMerit(id)
{
	newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function openPrintPage()
{
	newWindow("printpage.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewAttendByYear()
{
	return;
}
function viewMeritByYear()
{
	return;
}
</SCRIPT>
<!--
<form name=form1 action="">
<?=$school_info?>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td>
<?php
if($year_display != "")
	echo "$i_Profile_DataLeftYear: ".$year_display;
?><br>
<?php
	echo $toolbar;
?></td></tr>
<tr><td><br></td></tr>
<tr>
<td class=tableContent height=300 align=left>
<?//php if ($StudentID != "") {?>

<?=$i_Profile_ClassHistory?>
<?=$lclass->displayLeftClassHistoryAdmin($StudentID)?>
<br>
-->
<? 
	if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden) {
		//=$i_Profile_Attendance
		//=$lattend->displayArchiveStudentRecordByYearAdmin($StudentID,"","")
		$dataAttendance = $lattend->returnArchiveStudentRecordByYear($StudentID,$year,"");
	}
	if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden) {
		//=$i_Profile_Merit
		//=$lmerit->displayArchiveStudentRecordByYearAdmin($StudentID,"","")
		$dataMerit = $lmerit->returnArchiveStudentRecordByYear($StudentID,$year,"");
	}
	if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden) {
		//=$i_Profile_Service
		//=$lservice->displayArchiveServiceAdmin($StudentID,"","")
		$dataService = $lservice->getArchiveServiceByStudent($StudentID,$year,"");
	}
	if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden) {
		//=$i_Profile_Activity
		//=$lact->displayArchiveActivityAdmin($StudentID,"","")
		$dataActivity = $lact->getArchiveActivityByStudent($StudentID,$year,"");
	}
	if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden) {
		//=$i_Profile_Award
		//=$laward->displayArchiveAwardAdmin($StudentID,"","")
		$dataAward = $laward->getArchiveAwardByStudent($StudentID,$year,"");
	}
//} 
?>

<?
	$lineRemain = $defaultNumOfLine - $page_header_linefeed;
	echo "$school_info\n";
	echo "<table width=".$defaultTableWidth." border=0 cellpadding=0 cellspacing=0>\n";
	###################################
	echo "<tr><td>\n";
	if($year_display != "")
	{
		echo "$i_Profile_DataLeftYear: ".$year_display;
		echo "</td></tr>\n";
		echo "<tr><td>\n";
	}
	echo $toolbar;
	echo "</td></tr>\n";
	echo "<tr><td><br></td></tr><tr><td class=tableContent height=300 align=left>\n";
        
	$x = "";
	$x .= "$i_Profile_ClassHistory";
	$classtable = $lclass->displayLeftClassHistoryAdmin($StudentID, $year);
	$x .= $classtable;
	$x .= "<br>";
	$lineRemain -= 5;

	###########################################################################
	# Attendance
	###########################################################################
	if ($li_menu->is_access_attendance && !$lstudentprofile->is_printpage_attendance_hidden)
	{
		# Attendance Title
		$x .= $i_Profile_Attendance;
		$lineRemain--;

		# Attendance Table Header
		$title_array = array($i_Attendance_Year,$i_Profile_Absent,$i_Profile_Late,$i_Profile_EarlyLeave);
		$AttendanceTableHeader = "<table width=".$defaultTableWidth." border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
		$AttendanceTableHeader .= "<tr>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			$AttendanceTableHeader .= "<td class=tableTitle_new align=center>".$title_array[$i]."</td>";
		}
		$headerLineNeed = GET_LINE_NEEDED($title_array);
		$AttendanceTableHeader .= "</tr>\n";
		
		# If no records
		if (sizeof($dataAttendance)==0)
		{
			$x .= $AttendanceTableHeader;
			$lineRemain = $lineRemain - $headerLineNeed;
			$x .= "<tr><td colspan=4 align=center>$i_no_record_exists_msg</td></tr>\n";
			$lineRemain--;
		}
		else
		{
			$AttendanceTableStart = true;
			$AttendanceTableStarted = false;
			for ($i=0; $i<sizeof($dataAttendance); $i++)
			{
				# Determine page break
				list($dataAttendanceYear, $dataAttendanceAbs, $dataAttendanceLate, $dataAttendanceEarly)
				        = $dataAttendance[$i];
				$currentLineNeed = GET_LINE_NEEDED($dataAttendance[$i]);

				if($lineRemain - $currentLineNeed < 0)
				{
					if($AttendanceTableStarted)
					{
						$x .= "</table>\n";
						$AttendanceTableStarted = false;
					}
					$x .= $intermediate_pageheader;
					$lineRemain = $defaultNumOfLine - $intermediate_linefeed;
					$AttendanceTableStart = true;
				}
				
				if($AttendanceTableStart)
				{
					$x .= $AttendanceTableHeader;
					$lineRemain = $lineRemain - $headerLineNeed;
					$AttendanceTableStart = false;
					$AttendanceTableStarted = true;
				}
	
				# Data Row		
				$x .= "<tr>\n";
				for ($j=0; $j<sizeof($dataAttendance[$i]); $j++)
				{
					$data = $dataAttendance[$i][$j];
					$x .= "<td align=center>$data</td>";
				}
				$x .= "</tr>\n";
				$lineRemain = $lineRemain - $currentLineNeed;
			}
		}
		$x .= "</table>\n";
		
		$x .= "<br>\n";
		$lineRemain--;
	}
	###########################################################################
	# End of Attendance
	###########################################################################
        
	###########################################################################
	# Merit
	###########################################################################
	if ($li_menu->is_access_merit && !$lstudentprofile->is_printpage_merit_hidden)
	{
		# Merit Title
		$x .= $i_Profile_Merit;
		$lineRemain--;
		
		# Merit Table Header
		$MeritTableHeader = "<table width=".$defaultTableWidth." border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
		$title_array = array($i_Profile_Year);
		$title_array = array_merge($title_array,$i_Merit_TypeArray);
		$MeritTableHeader .= "<tr>\n";
		for ($i=0; $i<sizeof($title_array); $i++)
		{
			if ($merit_col_not_display[$i]==1) continue;
			if (!$field_disabled_array[$i])
			{
				$MeritTableHeader .= "<td class=tableTitle_new align=center>".$title_array[$i]."</td>";
				$column_cnt++;
			}
		}
		$headerLineNeed = GET_LINE_NEEDED($title_array);
		$MeritTableHeader .= "</tr>\n";
		
		# If no record
		if (sizeof($dataMerit)==0)
		{
			$x .= $MeritTableHeader;
			$lineRemain = $lineRemain - $headerLineNeed;
			$x .= "<tr><td colspan=".$column_cnt." align=center>$i_no_record_exists_msg</td></tr>\n";
			$lineRemain--;
		}
		else
		{
			$MeritTableStart = true;
			$MeritTableStarted = false;
			for ($i=0; $i<sizeof($dataMerit); $i++)
			{
				$currentLineNeed = GET_LINE_NEEDED($dataMerit[$i]);
				
				if($lineRemain - $currentLineNeed < 0)
				{
					if($MeritTableStarted)
					{
						$x .= "</table>\n";
						$MeritTableStarted = false;
					}
					$x .= $intermediate_pageheader;
					$lineRemain = $defaultNumOfLine - $intermediate_linefeed;
					$MeritTableStart = true;
				}
				
				if($MeritTableStart)
				{
					$x .= $MeritTableHeader;
					$lineRemain = $lineRemain - $headerLineNeed;
					$MeritTableStart = false;
					$MeritTableStarted = true;
				}
				
				# Data Row
				$x .= "<tr>\n";
				for ($j=0; $j<sizeof($dataMerit[$i]); $j++)
				{
					if ($merit_col_not_display[$j]==1) continue;
					if (!$field_disabled_array[$j])
					{
						$data = $dataMerit[$i][$j];
						$x .= "<td align=center>$data</td>";
					}
				}
				$x .= "</tr>\n";
				$lineRemain = $lineRemain - $currentLineNeed;
			}
		}
		$x .= "</table>\n";
		
		$x .= "<br>\n";
		$lineRemain--;
	}
	###########################################################################
	# End of Merit
	###########################################################################
        
	###########################################################################
	# Service
	###########################################################################
	if ($li_menu->is_access_service && !$lstudentprofile->is_printpage_service_hidden)
	{
		# Service Title
		$x .= $i_Profile_Service;
		$lineRemain--;
	
		# Service Table Header
		$ServiceTableHeader = "
													<table width=".$defaultTableWidth." border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
													<tr>
													<td width=70 align=center class=tableTitle_new>$i_ServiceYear</td>
													<td width=110 align=center class=tableTitle_new>$i_ServiceSemester</td>
													<td width=90 align=center class=tableTitle_new>$i_ServiceDate</td>
													<td width=110 align=center class=tableTitle_new>$i_ServiceName</td>
													<td width=90 align=center class=tableTitle_new>$i_ServiceRole</td>
													<td width=90 align=center class=tableTitle_new>$i_ServicePerformance</td>
													</tr>\n";
		$headerLineNeed = GET_LINE_NEEDED(array($i_ServiceYear, $i_ServiceSemester, $i_ServiceDate, $i_ServiceName, $i_ServiceRole, $i_ServicePerformance));

		# If no record									
		if (sizeof($dataService)==0)
		{
			$x .= $ServiceTableHeader;
			$lineRemain = $lineRemain - $headerLineNeed;
			$x .= "<tr><td colspan=6 align=center>$i_ServiceNoRecord</td></tr>\n";
			$lineRemain--;
		}
		else
		{
			$ServiceTableStart = true;
			$ServiceTableStarted = false;
			for ($i=0; $i<sizeof($dataService); $i++)
			{
				$currentLineNeed = GET_LINE_NEEDED($dataService[$i]);

				if($lineRemain - $currentLineNeed < 0)
				{
					if($ServiceTableStarted)
					{
						$x .= "</table>\n";
						$ServiceTableStarted = false;
					}
					$x .= $intermediate_pageheader;
					$lineRemain = $defaultNumOfLine - $intermediate_linefeed;
					$ServiceTableStart = true;
				}
				
				if($ServiceTableStart)
				{
					$x .= $ServiceTableHeader;
					$lineRemain = $lineRemain - $headerLineNeed;
					$ServiceTableStart = false;
					$ServiceTableStarted = true;
				}
			
				# Data Row
				list($year,$sem,$sDate,$service,$role,$performance) = $dataService[$i];
				$x .= "
								<tr>
								<td align=center>$year</td>
								<td align=center>$sem</td>
								<td align=center>$sDate</td>
								<td align=center>$service</td>
								<td align=center>$role&nbsp;</td>
								<td align=center>$performance&nbsp;</td>
								</tr>\n";
				$lineRemain = $lineRemain - $currentLineNeed;
			}
		}
		$x .= "</table>\n";
	
		$x .= "<br>\n";
		$lineRemain--;
	}
	###########################################################################
	# End of Service
	###########################################################################
        
	###########################################################################
	# Activity
	###########################################################################
	if ($li_menu->is_access_activity && !$lstudentprofile->is_printpage_activity_hidden)
	{
		# Activity Title
		$x .= $i_Profile_Activity;
		$lineRemain--;
		
		# Activity Table Header
		$ActivityTableHeader = "
														<table width=".$defaultTableWidth." border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
														<tr>
														<td width=70 align=center class=tableTitle_new>$i_ActivityYear</td>
														<td width=110 align=center class=tableTitle_new>$i_ActivitySemester</td>
														<td width=150 align=center class=tableTitle_new>$i_ActivityName</td>
														<td width=110 align=center class=tableTitle_new>$i_ActivityRole</td>
														<td width=120 align=center class=tableTitle_new>$i_ActivityPerformance</td>
														</tr>\n";
		$headerLineNeed = GET_LINE_NEEDED(array($i_ActivityYear, $i_ActivitySemester, $i_ActivityName, $i_ActivityRole, $i_ActivityPerformance));

		# If no record
		if (sizeof($dataActivity)==0)
		{
			$x .= $ActivityTableHeader;
			$lineRemain = $lineRemain - $headerLineNeed;
			$x .= "<tr><td colspan=5 align=center>$i_ActivityNoRecord</td></tr>\n";
			$lineRemain--;
		}
		else
		{
			$ActivityTableStart = true;
			$ActivityTableStarted = false;
			for ($i=0; $i<sizeof($dataActivity); $i++)
			{
				$currentLineNeed = GET_LINE_NEEDED($dataActivity[$i]);
				
				if($lineRemain - $currentLineNeed < 0)
				{
					if($ActivityTableStarted)
					{
						$x .= "</table>\n";
						$ActivityTableStarted = false;
					}
					$x .= $intermediate_pageheader;
					$lineRemain = $defaultNumOfLine - $intermediate_linefeed;
					$ActivityTableStart = true;
				}
				
				if($ActivityTableStart)
				{
					$x .= $ActivityTableHeader;
					$lineRemain = $lineRemain - $headerLineNeed;
					$ActivityTableStart = false;
					$ActivityTableStarted = true;
				}
			
				# Data Row
				list($year,$sem,$actName,$role,$performance) = $dataActivity[$i];
				$x .= "
								<tr>
								<td align=center>$year</td>
								<td align=center>$sem</td>
								<td align=center>$actName</td>
								<td align=center>$role&nbsp;</td>
								<td align=center>$performance&nbsp;</td>
								</tr>\n";
				$lineRemain = $lineRemain - $currentLineNeed;
			}
		}
		$x .= "</table>\n";
		
		$x .= "<br>\n";
		$lineRemain--;
	}
	###########################################################################
	# End of Activity
	###########################################################################
        
	###########################################################################
	# Award
	###########################################################################
	if ($li_menu->is_access_award && !$lstudentprofile->is_printpage_award_hidden)
	{
/*
	for ($j=0; $j<sizeof($dataAward); $j++){
	$length = 0;
	for ($pos=0; $pos < sizeof($dataAward[$j]); $pos++)
	{
	$length += strlen($dataAward[$j][$pos]);
	}
	$currentLineNeed = ceil ( $length / $defaultFieldWidth1 );
	
	if ($fieldLineNeed < $currentLineNeed)
	$fieldLineNeed = $currentLineNeed;
	
	}
	$lineRemain=$lineRemain - $fieldLineNeed*sizeof($dataAward) ;
	
	if ($lineRemain <0){
	$x .= "$intermediate_pageheader";
	$lineRemain = $defaultNumOfLine - $fieldLineNeed*sizeof($dataAward);
	}
*/	
		# Award Title
		$x .= $i_Profile_Award;
		$lineRemain--;
		
		# Award Table Header
		$AwardTableHeader = "
													<table width=".$defaultTableWidth." border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>
													<tr>
													<td width=70 align=center class=tableTitle_new>$i_AwardYear</td>
													<td width=110 align=center class=tableTitle_new>$i_AwardSemester</td>
													<td width=90 align=center class=tableTitle_new>$i_AwardDate</td>
													<td width=170 align=center class=tableTitle_new>$i_AwardName</td>
													<td width=120 align=center class=tableTitle_new>$i_AwardRemark</td>
													</tr>\n";
		$headerLineNeed = GET_LINE_NEEDED(array($i_AwardYear, $i_AwardSemester, $i_AwardDate, $i_AwardName, $i_AwardRemark));

		# If no record
		if (sizeof($dataAward)==0)
		{
			$x .= $AwardTableHeader;
			$x .= "<tr><td colspan=5 align=center>$i_AwardNoRecord</td></tr>\n";
		}
		else
		{
			$AwardTableStart = true;
			$AwardTableStarted = false;
			for ($i=0; $i<sizeof($dataAward); $i++)
			{
				$currentLineNeed = GET_LINE_NEEDED($dataAward[$i]);
				
				if($lineRemain - $currentLineNeed < 0)
				{
					if($AwardTableStarted)
					{
						$x .= "</table>\n";
						$AwardTableStarted = false;
					}
					$x .= $intermediate_pageheader;
					$lineRemain = $defaultNumOfLine - $intermediate_linefeed;
					$AwardTableStart = true;
				}
				
				if($AwardTableStart)
				{
					$x .= $AwardTableHeader;
					$lineRemain = $lineRemain - $headerLineNeed;
					$AwardTableStart = false;
					$AwardTableStarted = true;
				}
			
				# Data Row
				list($year,$sem,$aDate,$award,$remark) = $dataAward[$i];
				$x .= "
							<tr>
							<td align=center>$year</td>
							<td align=center>$sem</td>
							<td align=center>$aDate</td>
							<td align=center>$award</td>
							<td align=center>$remark&nbsp;</td>
							</tr>\n";
				$lineRemain = $lineRemain - $currentLineNeed;
			}
		}
		$x .= "</table>\n";
	}
	###########################################################################
	# End of Award
	###########################################################################

	echo $x;
	echo "</td></tr></table>";
	//if ($iter < sizeof($students)-1)
	//    echo "$page_breaker";
?>

</td></tr>
</table>
<!---
<table border=0 width=550>
<tr><td align=center>
<a href="javascript:window.print()"><img src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
--->
<?
include_once("../../templates/filefooter.php");

?>
