<?php

######################################
#
#	Date:	2012-11-19	YatWoon
#			hidden the link of "Press here to remove all records"
#
######################################

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libassessment.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libstudentpromotion.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

$li = new libclass();

if ($searchStuName){
        if ($li->isRemovedArchiveStudentExist($searchStuName)){
                $stuID=$li->returnRemovedStudentID($searchStuName);
                for ($i=0; $i<sizeOf($stuID); $i++ )
                {
                     list($studentID, $studentengname, $studentchiname) = $stuID[$i];

                      $stuArchiveInfo[$i]=array($studentID,$studentengname,$studentchiname,$li->returnRemovedStudentArchiveClassHistory($studentID));
                }
        }
        else
                $errMsg= "\"$searchStuName\"" . " - " . $i_Profile_StudentNotFound;
}
# remove all record link
// $displayRemoveLink = "<a class=\"functionlink_warn\" href=\"javascript:checkArchiveRemove(document.form1, 'searchArchiveByName.php') \">$i_Profile_ConfirmRemoveLink</a>";

#        display search bar
$displaySearchBar = "";

# display search method
if ($searchMethod){

                $displaySearchBar .= "$i_Profile_SearchMethod&nbsp;:&nbsp;<select name=searchMethod onChange=\"checkPost(document.form1, document.form1.searchMethod.value)\">".
                                                                "<option value=\"searchArchiveIndex.php\"> --  $button_select --</option>\n";


                if ( $searchMethod == "searchArchiveByYear.php")
                        $displaySearchBar .= "<option value=\"searchArchiveByYear.php\" selected>$i_Profile_DataLeftYear, $i_Profile_ClassOfFinalYear</option>";
                else
                        $displaySearchBar .= "<option value=\"searchArchiveByYear.php\" >$i_Profile_DataLeftYear, $i_Profile_ClassOfFinalYear</option>";

                if ( $searchMethod == "searchArchiveByName.php")
                        $displaySearchBar .= "<option value=\"searchArchiveByName.php\" selected>$i_Profile_StudentName</option>";
                else
                        $displaySearchBar .= "<option value=\"searchArchiveByName.php\">$i_Profile_StudentName</option>";

                $displaySearchBar .="</select><br>";

                if ( $searchStuName=="")
                        $selectedStudent="";
}

$displaySearchBar .= $i_Profile_StudentName . "&nbsp;:&nbsp;<input name=searchStuName type=text length=256 value=\"$searchStuName\">\n";
$displaySearchBar .= "<a href='javascript:document.form1.submit()'><img src='$image_path/admin/button/s_btn_find_$intranet_session_language.gif' border='0' align='absmiddle' alt='searchButton'></a>";

# display class history for entered search name
$displayClassHistory = "";
$displayClassHistory .= "<table width=560 border=1 cellpadding=0 cellspacing=0 bordercolorlight=#FEEEFD bordercolordark=#BEBEBE class=body>\n";
$displayClassHistory .= "<tr><td class=tableTitle_new>$i_Profile_StudentName</td><td class=tableTitle_new>$i_Profile_Year</td></tr>";


for ( $i=0; $i<sizeOf($stuArchiveInfo); $i++ ){
         list ($studentID, $studentengname, $studentchiname, $classhistory) = $stuArchiveInfo[$i];
                $linkToDetail = "<a href=\"searchArchiveByName.php?searchMethod=" . $searchMethod . "&selectedStudent=$studentID&searchStuName=" . $searchStuName . "#profile\">$studentengname $studentchiname</a>";
                $displayClassHistory .= "<tr><td>". $linkToDetail."</td><td>";
                for ( $j=0; $j<sizeOf($classhistory); $j++)
                                $displayClassHistory .= $classhistory[$j][2] . ":" . $classhistory[$j][3] . "-" . $classhistory[$j][4]."<br>";

                $displayClassHistory .= "</td>\n";
}
$displayClassHistory .= "</table>\n";

# display student info
if ($selectedStudent != "" ){
                $now = time();
                $yearstart = date('Y-m-d',getStartOfAcademicYear($now));
                $yearend = date('Y-m-d',getEndOfAcademicYear($now));
                $currentYear = getCurrentAcademicYear();

                $lattend = new libattendance();
                $lmerit = new libmerit();
                $lact = new libactivity();
                $lservice = new libservice();
                $laward = new libaward();
                $lassess = new libassessment();
                $lfiles = new libstudentfiles();
                $lstudentprofile = new libstudentprofile();

                $lu = new libuser();
                $studentNameClassNumber = "$i_UserStudentName: ".$lu->ArchiveUserNameClassNumber($selectedStudent);
                
                $year_attend = array();
                $year_merit = array();
                $year_act = array();
                $year_service = array();
                $year_award = array();
                $year_assess = array();
                $year_files = array();
                
                $year_attend = $lattend->returnArchiveYears($selectedStudent);
                $year_merit = $lmerit->returnArchiveYears($selectedStudent);
                $year_act = $lact->returnArchiveYears($selectedStudent);
                $year_service = $lservice->returnArchiveYears($selectedStudent);
                $year_award = $laward->returnArchiveYears($selectedStudent);
                $year_assess = $lassess->returnArchiveYears($selectedStudent);
                $year_files = $lfiles->returnArchiveYears($selectedStudent);
                $year_array = array_merge((array)$year_attend,(array)$year_merit,(array)$year_act,(array)$year_service,(array)$year_award,(array)$year_access,(array)$year_files);
                //$year_array[] = $currentYear;
                $year_array = array_unique($year_array);
                rsort($year_array);

                $year_array = array_values($year_array);
                
				##### global variable called by : 
				#	- libattendance.php
				#	- libmerit.php
				$all_distinct_years = $year_array;

				for($i=0; $i<count($year_array); $i++)
				{
					$record_year_arr[] = array($year_array[$i], $year_array[$i]);
				}
				$selectRecordYear = getSelectByArray($record_year_arr, "name=\"year\"  onChange=\"this.form.selectedStudent.value='".$selectedStudent."';this.form.submit()\"", $year, 1, 0, "", 2);
				$toolBar ="<tr><td >$i_Profile_Year : $selectRecordYear</td></tr>\n";

}


# remove all archive record of removed student
if ( $removeAll == "1" ){
        $lir = new libstudentpromotion();
        $lir->removeAllArchiveRecord();
        $removedMsg = $i_Profile_SucceedRemoveMsg;
}

?>

<SCRIPT LANGUAGE=Javascript>
function viewAttend(id)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewMerit(id)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function openPrintPage()
{
         newWindow("archivestudentprint.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&class=<?=$selectedClass?>&selectedYOL=<?=$selectedYOL?>&year=<?=$year?>&sem=<?=$generalSem?>",4);
}
function openPrintAllPage()
{
         newWindow("archivestudentallprint.php?class=<?=$selectedClass?>&generalYear="+document.form1.selectedYOL.value,4);
}
function viewFormAns(id)
{
         newWindow('/admin/academic/assessment/view.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&RecordID='+id,1);
}
function viewAttendByYear(id,year)
{
         newWindow("/admin/academic/view_attendance.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewMeritByYear(id, year)
{
         newWindow("/admin/academic/view_merit.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewFile(id)
{
         newWindow("/admin/academic/view_file.php?StudentType=Archive&StudentID=<?=$selectedStudent?>&RecordID="+id,11);
}

function checkArchiveRemove(obj,url){
        if(confirm("<?=$i_Profile_ConfirmRemoveAllArchiveRecord?>")){
               obj.removeAll.value=1;
           obj.action=url;
           obj.method = "get";
           obj.submit();
        }
}

</SCRIPT>


<form name="form1" action="">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, 'index.php', $i_Profile_DataLeftStudent, '') ?>
<!-- icon --><?= displayTag("head_studentview_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td class=tableContent align=left>
<?php
        if (!$searchStuName) {
                echo $displaySearchBar ;
        }
        else if ( !$errMsg )
                echo $displaySearchBar . "<br><br>" .$displayClassHistory  ;
        else if ( $errMsg )
                echo $errMsg . "<br><br>" . $displaySearchBar;
?>
</td><tr>
<?=$toolBar?>
<tr><td colspan=2 align=right><?=$removedMsg?></td></tr>
<tr><td colspan=2><hr size=1 class='hr_sub_separator'></td></tr>
<?php if (!$searchStuName || $errMsg) { ?>
<tr><td align=right><?=$displayRemoveLink?></td></tr>
<? } ?>
</table>

<!--display student info-->
<?php if ($selectedStudent != "") { ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td id="profile" align=right class=admin_bg_menu><a class=iconLink href="javascript:openPrintPage()"><?=printIcon()?><?=$i_PrinterFriendlyPage?></a></td></tr>
<tr><td class=tableContent height=300 align=left>
<?=$studentNameClassNumber?>

<br>
<? if ($year == "") { ?>
<br>
<?=$i_Profile_ClassHistory?>
<?=$li->displayLeftClassHistoryAdmin($selectedStudent, $year)?>
<br>
<? } ?>
<? if ($li_menu->is_access_attendance) { ?>
<?=$i_Profile_Attendance?>
<?=$lattend->displayArchiveStudentRecordByYearAdmin($selectedStudent,$year,"")?>
<br>
<? }
   if ($li_menu->is_access_merit) {
?>
<?=$i_Profile_Merit?>
<?=$lmerit->displayArchiveStudentRecordByYearAdmin($selectedStudent,$year,"")?>
<br>
<? }
   if ($li_menu->is_access_service) {
?>
<?=$i_Profile_Service?>
<?=$lservice->displayArchiveServiceAdmin($selectedStudent,$year,"")?>
<br>
<? }
   if ($li_menu->is_access_activity) {
?>
<?=$i_Profile_Activity?>
<?=$lact->displayArchiveActivityAdmin($selectedStudent,$year,"")?>
<br>
<? }
   if ($li_menu->is_access_award) {
?>
<?=$i_Profile_Award?>
<?=$laward->displayArchiveAwardAdmin($selectedStudent,$year,"")?>
<br>
<? }
   if ($li_menu->is_access_assessment) {
?>
<?=$i_Profile_Assessment?>
<?=$lassess->displayArchiveAssessmentAdmin($selectedStudent,$year,"")?>
<br>
<? }
   if ($li_menu->is_access_student_files) {
?>
<?=$i_Profile_Files?>
<?=$lfiles->displayArchiveFileListAdmin($selectedStudent,$year,"")?>
<? } ?>
<br>
<br>
</td></tr>
</table>
<?  } ?>
<!-- end of student info -->

<input type=hidden name=removeAll value="">
<input type=hidden name=selectedStudent >
</form>
<?
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
