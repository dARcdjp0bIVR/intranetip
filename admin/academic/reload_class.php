<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_opendb();

$lclass = new libclass();

$AcademicYearID = stripslashes($_REQUEST['AcademicYearID']);
$attrib = stripslashes($_REQUEST['attrib']);
$selected_class = stripslashes($_REQUEST['selected_class']);

if($AcademicYearID)
	$returnString = $lclass->getSelectClass($attrib,$selected_class, "", "", $AcademicYearID);

echo $returnString;

intranet_closedb();
	
?>