<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$list = implode(",",$AssessmentID);
$sql = "DELETE FROM PROFILE_STUDENT_ASSESSMENT WHERE AssessmentID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: studentview.php?classid=$classid&studentid=$studentid&msg=3");
?>