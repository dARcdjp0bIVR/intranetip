<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libassessment.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$lu = new libuser($studentid);
# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     default: $field = 0; break;
}
if ($yrfilter!="")
{
    $conds = " AND a.Year = '$yrfilter'";
}
else
{
    $conds = "";
}

$name_field = getNameFieldByLang("b.");
$sql = "SELECT
              CONCAT('<a href=javascript:viewFormAns(',a.AssessmentID,')>',a.Year,'</a>'),
              a.Semester,
              DATE_FORMAT(a.AssessmentDate,'%Y-%m-%d'),
              $name_field AS UserName, c.FormName,
              a.DateModified,
              CONCAT('<input type=checkbox name=AssessmentID[] value=', a.AssessmentID ,'>')
        FROM PROFILE_STUDENT_ASSESSMENT as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.AssessBy = b.UserID
             LEFT OUTER JOIN INTRANET_FORM as c ON a.FormID = c.FormID
        WHERE a.UserID = $studentid
              AND (a.AssessmentDate like '%$keyword%'
                   OR a.Year like '%$keyword%'
                   OR a.Semester like '%$keyword%'
                   OR c.FormName like '%$keyword%'
                   OR $name_field like '%$keyword%'
                   ) $conds
              ";

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("a.Year","a.Semester","a.AssessmentDate", "UserName","c.FormName","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(0, $i_Assessment_Year)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(1, $i_Assessment_Semester)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(2, $i_Assessment_Date)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(3, $i_Assessment_By)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(4, $i_Form_Name)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(5, $i_Assessment_LastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("AssessmentID[]")."</td>\n";

$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'new.php')\">".newIcon()."$button_new</a>";

// TABLE FUNCTION BAR
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'AssessmentID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'AssessmentID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$name = $lu->UserNameLang();

$lassess = new libassessment();
$years = $lassess->returnYears($studentid);
$filterbar = "$button_select $i_Profile_Year: ".getSelectByValue($years,"name=yrfilter onChange='this.form.submit()'",$yrfilter,1);

?>
<SCRIPT LANGUAGE=javascript>
function viewFormAns(id)
{
         newWindow('view.php?AssessmentID='+id,1);
}
</SCRIPT>

<form name="form1" method="post">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Assessment, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, '') ?>
<?= displayTag("head_assessment_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$li->display()?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=studentid value="<?=$studentid?>">
<input type=hidden name=classid value="<?=$classid?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
</form>
<?
include_once("../../../templates/adminfooter.php");

?>