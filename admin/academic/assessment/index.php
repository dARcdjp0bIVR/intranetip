<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libassessment.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lassessment = new libassessment();
$lform = new libform();

$forms = $lform->returnAssessmentForm();
$selection = getSelectByArray($forms,"name=FormID onChange=this.form.submit()",$FormID,1);
$result = $lassessment->getClassAssessmentListByForm($FormID);

$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=70% class=tableTitle>$i_UserClassName</td>
<td width=30% class=tableTitle>$i_Form_AssessmentDone</td>
</tr>
";

$lvlArray = array();
$lvlFormsCount = array();
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$formsCount,$lvlID) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlFormsCount[$j] += $formsCount;
              break;
          }
     }
     $link = "<a class=functionlink href=classview.php?classid=$id&FormID=$FormID>$name</a>";
	 $css = ($i%2) ? "2" : "";
     $x .= "<tr>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$formsCount</td>
            </tr>\n";
}
$x .= "</table>\n";


$lvls = $lassessment->getLevelList();
$y = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=70% class=tableTitle>$i_UserClassLevel</td>
<td width=30% class=tableTitle>$i_Form_AssessmentDone</td>
</tr>
";
for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $count = $lvlFormsCount[$i];

     $css = ($i%2) ? "2" : "";
	 $y .= "<tr>
             <td class=tableContent$css>$name</td>
             <td class=tableContent$css>$count</td>
            </tr>\n";
}
$y .= "</table>\n";

/*
$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT><br>\n";
$functionbar = "$classSelect $i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To";
$functionbar .= "<input type=text name=end size=10 value='$end'> (YYYY-MM-DD)<br><input type=submit class=submit value='$button_submit'>";
*/

$functionbar = "$i_Assessment_SelectForm: $selection";
?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Form_Assessment, '') ?>
<?= displayTag("head_assessment_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
<tr><td><br><br></td></tr>
<tr><td class=tableContent>
<?=$y?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>