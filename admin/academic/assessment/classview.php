<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libassessment.php");
include_once("../../../includes/libform.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lassessment = new libassessment();
$class_name = $lassessment->getClassName($classid);
$lform = new libform();

$forms = $lform->returnAssessmentForm();
$selection = getSelectByArray($forms,"name=FormID onChange=this.form.submit()",$FormID,1);

$result = $lassessment->getAssessmentListByClassForm($classid,$FormID);

$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=10% class=tableTitle>$i_UserClassNumber</td>
<td width=60% class=tableTitle>$i_UserEnglishName</td>
<td width=30% class=tableTitle>$i_Form_AssessmentDone</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$count) = $result[$i];
#    $display = ($classnumber != ""? "$classnumber. $name":"$name");
     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
     $display = $name;
     $link = "<a class=functionlink href=studentview.php?studentid=$id&classid=$classid>$display</a>";
	 $css = ($i%2) ? "2" : "";
     $x .= "<tr>
             <td class=tableContent$css>$classnumber</td>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$count</td>
            </tr>\n";
}
$x .= "</table>\n";

$classSelect = $lassessment->getSelectClassID("name=classid onChange=\"this.form.submit()\"",$classid);
$functionbar = "<table border=0>\n";
$functionbar .= "<tr><td nowrap>$i_Assessment_SelectForm:</td><td>$selection</td></tr>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectClass</td><td>$classSelect</td></tr>\n";
$functionbar .= "</table>\n";

?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Assessment, 'index.php', $class_name, '') ?>
<?= displayTag("head_assessment_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>