<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();
$limport = new libimporttext();
$linterface = new interface_html();

$lu = new libuser($studentid);
$lattend = new libattendance();
$lword = new libwordtemplates();
$typeSelect = $lattend->getSelectType("name=type");
$dayTypeSelect = getSelectSchoolDayType("name=daytype");
$wordlist = $lword->getSelectAttendance("onChange=\"this.form.reason.value=this.value\"");
?>

<script language="javascript">
function checkform(obj){
         return true;
}
</script>

<form action=import_update.php enctype=multipart/form-data method=post onSubmit="return checkform(this);">
<p class=admin_head><?php echo $i_adminmenu_adm.displayArrow(); ?><a href=javascript:history.back()><?php echo $i_adminmenu_adm_academic_record.displayArrow().$i_Profile_Attendance; ?></a><?php echo displayArrow();?><?php echo $button_import; ?></p>
<blockquote>
<table width=500 border=0 cellpadding=2 cellspacing=1>
<tr><td align=right><?php echo $i_Attendance_ImportSelect; ?>:</td>
	<td><input type=file name=userfile><br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?></td>
</tr>
<tr><td><br></td><td><input class=submit type=submit value="<?php echo $button_submit; ?>"><input class=reset type=reset value="<?php echo $button_reset; ?>"><input class=button type=button value="<?php echo $button_cancel; ?>" onClick=history.back()></td></tr>
</table>
<table width=450 align=center border=0 bgcolor=white cellpadding=3 cellspacing=3>
<tr><td>
<?=$i_Attendance_ImportInstruction?></td></tr>
</table><br>
<p><a class=functionlink href="<?= GET_CSV("sample_attendance.csv")?>"><?=$i_Attendance_DownloadSample?></a></p>
</blockquote>
<input type=hidden name=studentid value=<?=$studentid?>>
</form>

<?php
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>