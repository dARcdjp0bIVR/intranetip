<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libstudentfiles.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lfiles = new libstudentfiles();
$class_name = $lfiles->getClassName($classid);

$result = $lfiles->getClassFilesListByClass($classid);

$x = "<table width=560 border=1 bordercolordark=#D0DBC4 bordercolorlight=#F1FCE5 cellpadding=2 cellspacing=0>
<tr>
<td width=10% class=tableTitle>$i_UserClassNumber</td>
<td width=60% class=tableTitle>$i_UserEnglishName</td>
<td width=30% class=tableTitle>$i_ReferenceFiles_Qty</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$count) = $result[$i];
#    $display = ($classnumber != ""? "$classnumber. $name":"$name");
     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
     $display = $name;
     $css = ($i%2) ? "2" : "";
     $link = "<a class=functionlink href=studentview.php?studentid=$id&classid=$classid>$display</a>";
     $x .= "<tr>
             <td class=tableContent$css>$classnumber</td>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$count</td>
            </tr>\n";
}
$x .= "</table>\n";
/*
<p class=admin_head><?php echo $i_adminmenu_adm.displayArrow()."<a href=/admin/academic/>$i_adminmenu_adm_academic_record</a>".displayArrow()."<a href=index.php>$i_Profile_Assessment</a>".displayArrow().$class_name; ?></p>
*/
?>
<form name=form1 method=get>
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Files, '/admin/academic/student_files/',$class_name,'') ?>
<?= displayTag("head_referencefile_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<input type=hidden name=classid value=<?=$classid?>>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>