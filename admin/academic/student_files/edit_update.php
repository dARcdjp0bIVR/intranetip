<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libdb();

// $year = intranet_htmlspecialchars(trim($year));
$title = intranet_htmlspecialchars(trim($title));
$filetype = intranet_htmlspecialchars(trim($filetype));
$semester = intranet_htmlspecialchars(trim($semester));
$description = intranet_htmlspecialchars(trim($description));

$academic_year_term = new academic_year_term($YearTermID);
$year = $academic_year_term->YearNameEN;
$semester = $academic_year_term->YearTermNameEN;

$update_value = "Year = '$year'";
$update_value.= ",Semester = '$semester'";
$update_value.= ",FileType = '$filetype'";
$update_value.= ",Title = '$title'";
$update_value.= ",Description = '$description'";
$update_value.= ",DateModified = now()";

$sql = "UPDATE PROFILE_STUDENT_FILES SET $update_value WHERE FileID = $FileID";
$li->db_db_query($sql);

intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=2");
?>