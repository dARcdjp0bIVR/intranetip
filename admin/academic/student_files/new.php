<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();
$lu = new libuser($studentid);
$semesterSelect = getSelectSemester("name=semester");
$name = $lu->UserNameLang();
/*
<p class=admin_head><?php echo $i_adminmenu_adm.displayArrow()."<a href=/admin/academic/>$i_adminmenu_adm_academic_record</a>".displayArrow()."<a href=index.php>$i_Form_Assessment</a>".displayArrow()."<a href=classview.php?classid=$classid>".$lu->ClassName."</a>".displayArrow()?><a href=javascript:history.back()><?=$name?><?=displayArrow().$i_Assessment_SelectForm?></a><?=displayArrow().$i_Assessment_FillForm?></p>
*/

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$select_academicYear = getSelectAcademicYear("AcademicYearID", "onChange=\"js_Reload_Term_Selection()\"",1,"",$AcademicYearID);

?>
<script type="text/javascript" src="../../../templates/jquery/jquery-1.3.2.min.js"></script>

<script language="javascript">
function checkform(obj){
         if(!check_text(obj.year, "<?php echo $i_alert_pleasefillin.$i_Profile_Year; ?>.")) return false;
         if(!check_text(obj.title, "<?php echo $i_alert_pleasefillin.$i_ReferenceFiles_Title; ?>.")) return false;
         if(!check_text(obj.userfile, "<?php echo $i_alert_pleaseselect.$i_ReferenceFiles_FileName; ?>.")) return false;
         return true;
}

function js_Reload_Term_Selection()
{
	AcademicYearID = document.form1.AcademicYearID.value;
	
	$('#semester_div').load(
		'../reload_semester.php', 
		{
			AcademicYearID: AcademicYearID
		}
	); 
}

</script>

<form name=form1 action=new_update.php enctype=multipart/form-data method=post onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Files, 'index.php', $lu->ClassName, 'classview.php?classid='.$classid, $name, 'javascript:history.back()',$i_ReferenceFiles_NewFile,'') ?>
<?= displayTag("head_referencefile_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=right><?php echo $i_Profile_Year; ?>:</td><td><?=$select_academicYear?></td></tr>
<tr><td align=right><?php echo $i_Profile_Semester; ?>:</td><td><div id="semester_div"><?=$semesterSelect?></div></td></tr>
<tr><td align=right><?php echo $i_ReferenceFiles_Title; ?>:</td><td><input type=text name=title size=50 maxlength=100></td></tr>
<tr><td align=right><?php echo $i_ReferenceFiles_Description; ?>:</td><td><textarea name=description rows=5 cols=50></textarea></td></tr>
<tr><td align=right><?php echo $i_ReferenceFiles_FileType; ?>:</td><td><input type=text name=filetype size=50 maxlength=100></td></tr>
<tr><td align=right><?php echo $i_ReferenceFiles_SelectFile; ?>:</td><td><input type=file name=userfile size=40></td></tr>
</table>
</blockquote>

<input type=hidden name="classid" value="<?=$classid?>">
<input type=hidden name="studentid" value="<?=$studentid?>">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<script language="javascript">
<!--
js_Reload_Term_Selection();
//-->
</script>

<?php
include_once("../../../templates/adminfooter.php");
?>