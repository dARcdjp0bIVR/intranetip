<?php
// page used by : yat
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libassessment.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
include_once("../../includes/form_class_manage.php");
// $li_menu from adminheader_intranet.php
intranet_opendb();

$lclass = new libclass();

$lu = new libuser();
$lstudentprofile = new libstudentprofile();
$case = 0;
 
$general_select_year = getSelectAcademicYear("AcademicYearID", "onChange=\"js_Reload_Term_Selection('','')\"",1,"",$AcademicYearID);
$StudentID = $getFirstStudent ? "" : $StudentID;

if ($class !="")
{
    $student_selection = "<tr><td>$i_Profile_SelectStudent:</td><td>".$lclass->getStudentSelectByClass($class,"name=StudentID onChange=this.form.getFirstStudent.value=0;this.form.submit()",$StudentID,"","","","1")."</td></tr>";
    $additional_js = "this.form.StudentID.selectedIndex=0;";
    
    if($StudentID=="")
    {
	    # found the first student
	    $studnetlist = $lclass->returnStudentListByClass($class,"","1");
	    if($studnetlist)
	    {
	    	$StudentID = $studnetlist[0]['UserID'];
		}
	}
}
$class_attribStr = "name=class onChange=\"$additional_js this.form.getFirstStudent.value=1;this.form.submit()\"";
//$class_selection = "<tr><td>$i_Profile_SelectClass</td><td><div id=class_div>".$lclass->getSelectClass("name=class onChange=\"".$additional_js."this.form.getFirstStudent.value=1;this.form.submit()\"",$class)."</div></td></tr>";
$class_selection = "<tr><td>$i_Profile_SelectClass</td><td><div id=class_div>".$lclass->getSelectClass("name=class onChange=\"this.form.getFirstStudent.value=1;this.form.submit();\"",$class)."</div></td></tr>";
//$class_selection = "<tr><td>$i_Profile_SelectClass</td><td><div id=class_div></div></td></tr>";

if ($StudentID == "" )
{
    $toolbar = "<table border=0 width=100% cellpadding=2 cellspacing=0>\n";
    $toolbar .= "<tr><td>&nbsp;</td><td align=right class=admin_bg_menu>$general_select_year</td>";
    $toolbar .= "<td class=admin_bg_menu><div id=semester_div>$general_select_sem</div></td><td class=admin_bg_menu><a class=iconLink href=\"javascript:openPrintAllPage()\">".printIcon()."$i_PrinterFriendlyPage</a></td></tr>\n";
}
else
{
    $toolbar = "<table border=0>\n";
}
$toolbar .= "$class_selection\n $student_selection\n";
$toolbar .= "</table>\n";
if ($StudentID != "")
{

$now = time();
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));

if($changeYear)		# data pass from inner page
{
	$academic_year = new academic_year($AcademicYearID);
	$currentYear = $academic_year->YearNameEN;
}
else
{
	if($YearTermID)
	{
		$academic_year_term = new academic_year_term($YearTermID);
		$generalSem = $academic_year_term->YearTermNameEN;
		$currentYear = $academic_year_term->YearNameEN;
	}
	else
	{
		$academic_year = new academic_year($AcademicYearID);
		$currentYear = $academic_year->YearNameEN;
	}
}

$year = $currentYear;
$onChangeSubmitStr = "this.form.submit();";
$select_year = getSelectAcademicYear("AcademicYearID", "onChange=\"document.form1.changeYear.value=1; this.form.submit()\"",1,"",$AcademicYearID, 1);


$lattend = new libattendance();
$lmerit = new libmerit();
$lact = new libactivity();
$lservice = new libservice();
$laward = new libaward();
$lassess = new libassessment();
$lfiles = new libstudentfiles();

/*
$year_array = array();
$year_attend = array();
$year_merit = array();
$year_act = array();
$year_service = array();
$year_award = array();
$year_assess = array();
$year_files = array();

$year_attend = $lattend->returnYears($StudentID);
$year_merit = $lmerit->returnYears($StudentID);
$year_act = $lact->returnYears($StudentID);
$year_service = $lservice->returnYears($StudentID);
$year_award = $laward->returnYears($StudentID);
$year_assess = $lassess->returnYears($StudentID);
$year_files = $lfiles->returnYears($StudentID);

$year_array = array_merge((array)$year_attend,(array)$year_merit,(array)$year_act,(array)$year_service,(array)$year_award,(array)$year_access,(array)$year_files);
$year_array[] = $currentYear;
$year_array = array_unique($year_array);
rsort($year_array);

$year_array = array_values($year_array);

##### global variable called by : 
#	- libattendance.php
#	- libmerit.php
$all_distinct_years = $year_array;
if (!isset($year))
{
     $year = $currentYear;
}
$select_year = getSelectByValue($year_array,"name=year onChange=this.form.submit()",$year,1);
*/

}


?>
<script type="text/javascript" src="../../../templates/jquery/jquery-1.3.2.min.js"></script>

<SCRIPT LANGUAGE=Javascript>
function viewAttend(id)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewMerit(id)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function openPrintPage()
{
         newWindow("studentprint.php?StudentID=<?=$StudentID?>&class=<?=$class?>&year=<?=$year?>&sem=<?=$generalSem?>",4);
}
function openPrintAllPage()
{
         newWindow("studentallprint.php?class=<?=$class?>&generalYear="+document.form1.generalYear.value+"&generalSem="+document.form1.generalSem.value,4);
}
function viewFormAns(id)
{
         newWindow('/admin/academic/assessment/view.php?AssessmentID='+id,1);
}
function viewAttendByYear(id,year)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewMeritByYear(id, year)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewFile(id)
{
         newWindow("/admin/academic/view_file.php?FileID="+id,11);
}


function js_Reload_Term_Selection(SelectedYearTermID)
{
 	AcademicYearID = document.form1.AcademicYearID.value;
 	
	$('#semester_div').load(
		'reload_semester.php', 
		{
			AcademicYearID: AcademicYearID,
			SelectedYearTermID: SelectedYearTermID,
			onChangeSubmitStr: '<?=$onChangeSubmitStr?>',
			displayAll: 1
		}
	); 
	
	$('#class_div').load(
		'reload_class.php', 
		{
			AcademicYearID: AcademicYearID,
			selected_class: '<?=$class?>',
			attrib: '<?=$class_attribStr?>'
		}
	); 
	
	

}
</SCRIPT>

<form name="form1" action="">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, 'index.php', $i_Profile_OverallStudentView, '') ?>
<?= displayTag("head_studentview_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $toolbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<?php if ($StudentID != "") { ?>
<tr><td class=admin_bg_menu align=right><?=$select_year?></td><td class=admin_bg_menu><div id=semester_div><?=$general_select_sem?></div></td><td class=admin_bg_menu><a class=iconLink href="javascript:openPrintPage()"><?=printIcon().$i_PrinterFriendlyPage?></a>
</td></tr>
<?php } ?>


<tr><td class=tableContent height=300 align=left colspan="3">
<?php if ($StudentID != "") { ?>
<br>
<? if ($year == "") { ?>
<?=$i_Profile_ClassHistory?>
<?=$lclass->displayClassHistoryAdmin($StudentID)?>
<br>
<? } ?>
<? if ($li_menu->is_access_attendance) { ?>
<?=$i_Profile_Attendance?>

<?=$lattend->displayStudentRecordByYearAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_merit) {
?>
<?=$i_Profile_Merit?>
<?=$lmerit->displayStudentRecordByYearAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_service) {
?>
<?=$i_Profile_Service?>
<?=$lservice->displayServiceAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_activity) {
?>
<?=$i_Profile_Activity?>
<?=$lact->displayActivityAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_award) {
?>
<?=$i_Profile_Award?>
<?=$laward->displayAwardAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_assessment) {
?>
<?=$i_Profile_Assessment?>
<?=$lassess->displayAssessmentAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_student_files) {
?>
<?=$i_Profile_Files?>
<?=$lfiles->displayFileListAdmin($StudentID,$year,$generalSem)?>
<? } ?>
<br>
<br>
<?php } ?>
</td></tr>
</table>

<input type="hidden" name="changeYear" value="0">
<input type="hidden" name="getFirstStudent" value="0">
</form>


<script language="javascript">
<!--
js_Reload_Term_Selection(<?=$YearTermID?>);
//-->
</script>
<?
include_once("../../templates/adminfooter.php");
?>