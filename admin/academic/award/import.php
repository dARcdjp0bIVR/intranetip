<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();
$limport = new libimporttext();
$linterface = new interface_html();
?>

<script language="javascript">
function checkform(obj){
         return true;
}
</script>

<form name="form1" action="import_confirm.php" enctype="multipart/form-data" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Award, 'index.php', $button_import, '') ?>
<?= displayTag("head_award_$intranet_session_language.gif", $msg) ?>


<blockquote>
<?= $i_select_file ?>: <input type=file name=userfile><br><?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>

<blockquote>
<input type=radio name=format value=1 CHECKED><?=$i_Award_ImportInstruction1?>
<p><a class=functionlink_new href="<?= GET_CSV("sample_award.csv")?>"><?=$i_general_clickheredownloadsample?></a></p>
</blockquote>
<blockquote>
<input type=radio name=format value=2 ><?=$i_Award_ImportInstruction2?>
<p><a class=functionlink_new href="<?= GET_CSV("sample_award2.csv")?>"><?=$i_general_clickheredownloadsample?></a></p>
</blockquote>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>