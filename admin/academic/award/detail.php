<?php
###############################
#
#	Date:	2011-03-07	YatWoon
#			- Improved: display "Whole Year" is semester is empty
#
###############################

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libaward.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$lu = new libuser($studentid);

# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     case 6: $field = 6; break;
     default: $field = 0; break;
}
$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($AwardName != "")
{
    $conds .= " AND UPPER(a.AwardName) like UPPER('%$AwardName%')";
}
if ($AwardRemark != "")
{
    $conds .= " AND UPPER(a.Remark) like UPPER('%$AwardRemark%')";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.AwardDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");
$sql = "SELECT
							$studentnamefield, 
              a.Year,
               IF (a.Semester='','". $Lang['General']['WholeYear'] ."',a.Semester) as Semester,
              IF (a.AwardDate IS NULL,'-',a.AwardDate),
              a.AwardName,
              a.Remark,
              a.DateModified,
              CONCAT('<input type=checkbox name=StudentAwardID[] value=', a.StudentAwardID ,'>')
        FROM 
        			PROFILE_STUDENT_AWARD as a
        			INNER JOIN 
        			INTRANET_USER as b
        			ON a.UserID = b.UserID 
        WHERE 
        			(b.ChineseName like '%$keyword%' 
        			 OR b.EnglishName like '%$keyword%' 
        			 OR a.AwardName like '%$keyword%'
               OR a.Year like '%$keyword%'
               OR a.Semester like '%$keyword%'
               OR a.Remark like '%$keyword%'
              ) 
              $conds
              ";
             
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("b.EnglishName","a.Year", "Semester", "a.AwardDate", "a.AwardName", "a.Remark", "a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

$pos = 0;
// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_AwardYear)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_AwardSemester)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_AwardDate)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_AwardName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_AwardRemark)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_AwardLastModified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentAwardID[]")."</td>\n";

$toolbar = "<a class=iconLink href=\"javascript:checkGet(document.form1,'detail_export.php');document.form1.action=''\">".exportIcon()."$button_export</a>";

// TABLE FUNCTION BAR
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'StudentAwardID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>\n";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'StudentAwardID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$name = ($intranet_session_language=="en"? $lu->EnglishName: $lu->ChineseName);

$laward = new libaward();
$years = $laward->returnYears();

$select_year = getSelectByValue($years,"name=yrfilter",$yrfilter,1);

$lword = new libwordtemplates();
$awardlist = $lword->getSelectAwardName("onChange=\"this.form.AwardName.value=this.value\"");

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=class",$class);

$filterbar = "<table width=100% border=0 cellpadding=1 cellspacing=1>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_Year: </td><td>$select_year<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_UserClassName: </td><td>$select_class<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_AwardName: </td><td><input type=text name=AwardName size=30 maxlength=30 value='$AwardName'> $awardlist<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_AwardRemark: </td><td><input type=text name=AwardRemark size=30 maxlength=30 value='$AwardRemark'><td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_SearchByDate: </td><td>";
$filterbar .= "<input type=radio name=datetype value=0 ".($datetype==0?"CHECKED":"")."> $i_Profile_NotSearchByDate";
$filterbar .= "&nbsp;<input type=radio name=datetype value=1 ".($datetype==1?"CHECKED":"")."> $i_Profile_ByRecordDate";
$filterbar .= "&nbsp;<input type=radio name=datetype value=2 ".($datetype==2?"CHECKED":"")."> $i_Profile_ByLastModified";
$filterbar .= "<br>\n$i_From : <input type=text name=date_from size=10 maxlength=10 value='$date_from'> $i_To : <input type=text name=date_to size=10 maxlength=10 value='$date_to'> (YYYY-MM-DD)<br>\n";
$filterbar .= "<input type=image src=\"$image_path/admin/button/s_btn_find_$intranet_session_language.gif\"><td></tr>";
$filterbar .= "</table>\n";

// debug_pr($li->built_sql());

################################## here
?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}
function openAnalysisPage()
{
        newWindow("studentanalysis.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
function openPrintPage()
{
        newWindow("studentprint.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Award, 'index.php', $i_general_ViewDetailRecords, 'detail.php') ?>
<?= displayTag("head_award_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="100%" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $printAnalyze, $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width="100%" height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$li->display()?>
</td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=studentid value="<?=$studentid?>">
<input type=hidden name=classid value="<?=$classid?>">
<input type=hidden name=page_from value="<?="detail.php"?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>