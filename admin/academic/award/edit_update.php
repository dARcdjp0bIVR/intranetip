<?php
//using: rita
/*****************************************
 * Date: 	2013-03-15 (Rita)
 * Details: amend redirection
 *****************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libdb();

// $year = intranet_htmlspecialchars(trim($year));
$awardDate = intranet_htmlspecialchars(trim($awardDate));
$award = intranet_htmlspecialchars(trim($award));
$remark = intranet_htmlspecialchars(trim($remark));
$awardDate = ($awardDate==""? "NULL":"'$awardDate'");
$organization = intranet_htmlspecialchars(trim($organization));
$subjectarea = intranet_htmlspecialchars(trim($subjectarea));

$academic_year = new academic_year($AcademicYearID);
$year = $academic_year->YearNameEN;
$academic_year_term = new academic_year_term($YearTermID);
$semester = $academic_year_term->YearTermNameEN;

$update_fields = "";
$update_fields = "Year = '$year'";
$update_fields .= ",Semester = '$semester'";
$update_fields .= ",AwardDate = $awardDate";
$update_fields .= ",AwardName = '$award'";
$update_fields .= ",Remark = '$remark'";
$update_fields .= ",Organization = '$organization'";
$update_fields .= ",SubjectArea = '$subjectarea'";
$update_fields .= ",DateModified = now()";

$sql = "UPDATE PROFILE_STUDENT_AWARD SET $update_fields WHERE StudentAwardID = $StudentAwardID";
$li->db_db_query($sql);

intranet_closedb();
if ($page_from){
	header("Location: $page_from?msg=2&yrfilter=$yrfilter&class=$class&AwardName=$filterAwardName&AwardRemark=$filterAwardRemark&datetype=$datetype&date_from=$date_from&date_to=$date_to");
}
else{
	header("Location: studentview.php?studentid=$studentid&msg=2&classid=$classid");
}
?>
