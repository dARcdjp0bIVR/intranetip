<?php
##	Modifying by: 
#########################################
##	Modification Log:
##	2015-01-08	Omas
##	- Fixed : Insert student get award 's academic year classname, classnumber into db
##
##	2013-07-05	YatWoon
##	- Fixed: only import record for student (USERTYPE_STUDENT) [Case#2013-0704-1159-08073]
##
##	2011-02-08	YAtWoon
##	- Fix the bug that the insertion do not cater the DateInput ($format == 2)
##
##	2010-02-02: Max (201002011347)
##	- Fix the bug that the insertion do not cater the DateInput
#########################################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

		
//if($format==1)
//{
//	
//	$fields_name = "UserID, AwardDate, Year, Semester, ClassName,ClassNumber, AwardName,Remark,Organization,SubjectArea,DateInput,DateModified";	
//	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_AWARD ($fields_name) 
//	SELECT  b.UserID, a.AwardDate,a.Year, a.Semester, a.ClassName, a.ClassNumber, a.AwardName, a.Remark,a.Organization,a.SubjectArea,now(),now() 
//	FROM TEMP_AWARD_IMPORT as a LEFT JOIN INTRANET_USER as b ON a.ClassName = b.ClassName AND
//	a.ClassNumber = b.ClassNumber ";
//	$sql .= " and b.RecordType=".USERTYPE_STUDENT;
//}
//if($format==2){
//	
//	$fields_name = "UserID, AwardDate, Year, Semester, ClassName, ClassNumber, AwardName,Remark,Organization,SubjectArea,DateInput,DateModified";	
//	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_AWARD ($fields_name) 
//	SELECT  b.UserID, a.AwardDate,a.Year, a.Semester, b.ClassName, b.ClassNumber, a.AwardName, a.Remark,a.Organization,a.SubjectArea,now(),now() 
//	FROM TEMP_AWARD_IMPORT as a LEFT JOIN INTRANET_USER as b ON a.websamsregno = b.websamsregno ";
//	$sql .= " and b.RecordType=".USERTYPE_STUDENT;
//	
//}

if($format==1)
{
	
	$fields_name = "UserID, AwardDate, Year, Semester, ClassName,ClassNumber, AwardName,Remark,Organization,SubjectArea,DateInput,DateModified";	
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_AWARD ($fields_name) 
	SELECT  UserID, AwardDate,Year, Semester,targetYearClassName, targetYearClassNumber,AwardName, Remark,Organization,SubjectArea,now(),now() 
	FROM TEMP_AWARD_IMPORT";

}
if($format==2){
	
	$fields_name = "UserID, AwardDate, Year, Semester, ClassName, ClassNumber, AwardName,Remark,Organization,SubjectArea,DateInput,DateModified";	
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_AWARD ($fields_name) 
	SELECT  UserID, AwardDate,Year, Semester, targetYearClassName, targetYearClassNumber, AwardName, Remark,Organization,SubjectArea,now(),now() 
	FROM TEMP_AWARD_IMPORT";

	
}


	
$li->db_db_query($sql);

header("Location: index.php?msg=10");

intranet_closedb();

?>
