<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libuser.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/fileheader.php");

intranet_opendb();
$lfiles = new libstudentfiles($FileID);

# Student information
$lu = new libuser($lfiles->UserID);
$StudentName = $lu->UserNameLang();
$StudentClassName = $lu->ClassName;
$StudentClassNumber = $lu->ClassNumber;
$StudentLogin = $lu->UserLogin;

$download_path = "$intranet_httppath/file/studentfiles/".$lfiles->path."/".$lfiles->FileName;

?>
<p><?=$i_Profile_Files?></p>
<p>
<table width=95% border=0 cellspacing=5 cellpadding=4>
<tr><td align=right><?=$i_PrinterFriendly_StudentName?>:</td><td><?=$StudentName?></td></tr>
<tr><td align=right><?=$i_UserClassName?>:</td><td><?=$StudentClassName?></td></tr>
<tr><td align=right><?=$i_UserClassNumber?>:</td><td><?=$StudentClassNumber?></td></tr>
<tr><td align=right><?=$i_UserLogin?>:</td><td><?=$StudentLogin?></td></tr>
<tr><td align=right><?=$i_Profile_Year?>:</td><td><?=$lfiles->year?></td></tr>
<tr><td align=right><?=$i_Profile_Semester?>:</td><td><?=$lfiles->semester?></td></tr>
<tr><td align=right><?=$i_ReferenceFiles_Title?>:</td><td><?=$lfiles->Title?></td></tr>
<tr><td align=right><?=$i_ReferenceFiles_FileType?>:</td><td><?=$lfiles->FileType?></td></tr>
<tr><td align=right><?=$i_ReferenceFiles_Description?>:</td><td><?=$lfiles->Description?></td></tr>
<tr><td align=right><?=$i_LastModified?>:</td><td><?=$lfiles->DateModified?></td></tr>
<tr><td align=right><?=$i_ReferenceFiles_FileName?>:</td><td>
<a class=functionlink_new href=<?=$download_path?>>
<?=$lfiles->FileName?></a><?=" (".$lfiles->FileSize." kb)"?></td></tr>
<tr><td align=right>&nbsp;</td><td><input type=button onClick="window.close()" class=button value="<?=$button_close?>"></td></tr>
</table>
</p>
<hr>
<?php
include_once("../../templates/filefooter.php");
?>