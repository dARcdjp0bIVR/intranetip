<?php
// editing by 
/********************* Changes ****************************
 * 2011-07-22 (Carlos): Added duplication checking
 **********************************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$reason = intranet_htmlspecialchars(trim($reason));
$attenddate = intranet_htmlspecialchars(trim($attenddate));
// $year = intranet_htmlspecialchars(trim($year));
// $semester = intranet_htmlspecialchars(trim($semester));

list($AcademicYearID, $year, $YearTermID, $semester) = getAcademicYearInfoAndTermInfoByDate($attenddate);

## Duplication checking
if($type == 1){ // Absent
	$cond_type = " RecordType IN (1,2) ";
}else if($type == 2){ // Late
	$cond_type = " RecordType IN (1,2) ";
}else{ // Earlyleave = 3 
	$cond_type = " RecordType IN (1,3) ";
}
if($daytype == 1){ // WD
	$cond_daytype = " DayType IN (1,2,3) ";
}else if($daytype == 2){ // AM
	$cond_daytype = " DayType IN (1,2) ";
}else if($daytype == 3){ // PM
	$cond_daytype = " DayType IN (1,3) ";
}
$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE WHERE StudentAttendanceID <> $StudentAttendanceID AND UserID = '$studentid' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d') = '$attenddate' AND $cond_daytype AND $cond_type ";
$profileids = $li->returnVector($sql);
if(count($profileids) >= 1){
	intranet_closedb();
	header("Location: edit.php?StudentAttendanceID=$StudentAttendanceID&studentid=$studentid&classid=$classid&attenddate=$attenddate&type=$type&daytype=$daytype=&xxmsg=".urlencode($i_con_msg_update_failed));
}
## end checking

$update_value = "AttendanceDate = '$attenddate',
                 Year = '$year',Semester = '$semester',                DayType='$daytype',Reason='$reason',DateModified=now()";
$sql = "UPDATE PROFILE_STUDENT_ATTENDANCE SET $update_value WHERE StudentAttendanceID = $StudentAttendanceID";


$li->db_db_query($sql);

intranet_closedb();
if ($page_from == "detail.php")
{
    header("Location: detail.php?class=$class&yrfilter=$yrfilter&reason=$prev_reason&datetype=$datetype&date_from=$date_from&date_to=$date_to&msg=2");
}
else
{
    header("Location: studentview.php?studentid=$studentid&msg=2&classid=$classid");
}

#header("Location: studentview.php?studentid=$studentid&msg=2&classid=$classid");
?>