<?php
// Editing by 
#################################################
#	Date:	2017-12-05  Carlos
#			Fixed the mapping value convertion for RecordType and Slot. 
#	Date:	2017-08-16	Simon
#			Update sql to get the targetYearClassName and targetYearClassNumber by select the TEMP_ATTENDANCE_IMPORT table
#
#	Date:	2013-07-05	YatWoon
#			Improved: no need to check student status [Case#2013-0704-1159-08073]
#
#################################################
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

// if($format==1)
// {
//                $fields_name = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, Reason, DateInput, DateModified, ClassName, ClassNumber";
//                $sql = "INSERT IGNORE INTO PROFILE_STUDENT_ATTENDANCE ($fields_name)
//                               SELECT a.UserID, b.AttendanceDate, b.Year, b.Semester,
//                                      CASE b.RecordType
//                                           WHEN 'A' THEN 1
//                                           WHEN 'L' THEN 2
//                                           WHEN 'E' THEN 3 END,
//                                      CASE b.Slot
//                                           WHEN 'WD' THEN 1
//                                           WHEN 'AM' THEN 2
//                                           WHEN 'PM' THEN 3 END,
//                                      b.Reason,now(),now(),
//                                      a.ClassName, a.ClassNumber
//                                      FROM INTRANET_USER as a, TEMP_ATTENDANCE_IMPORT as b
//                                           WHERE a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber
//                                           AND a.RecordType = 2
//                                      ";
// }
// if($format==2){
	
// 	           $fields_name = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, Reason, DateInput, DateModified, ClassName, ClassNumber";
//                $sql = "INSERT IGNORE INTO PROFILE_STUDENT_ATTENDANCE ($fields_name)
//                               SELECT a.UserID, b.AttendanceDate, b.Year, b.Semester,
//                                      CASE b.RecordType
//                                           WHEN 'A' THEN 1
//                                           WHEN 'L' THEN 2
//                                           WHEN 'E' THEN 3 END,
//                                      CASE b.Slot
//                                           WHEN 'WD' THEN 1
//                                           WHEN 'AM' THEN 2
//                                           WHEN 'PM' THEN 3 END,
//                                      b.Reason,now(),now(),
//                                      a.ClassName, a.ClassNumber
//                                      FROM INTRANET_USER as a, TEMP_ATTENDANCE_IMPORT as b
//                                           WHERE a.websamsregno = b.websamsregno
//                                           AND a.RecordType = 2
//                                      ";
	
// }


if($format==1)
{

	$fields_name = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, Reason, DateInput, DateModified, ClassName, ClassNumber";
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_ATTENDANCE ($fields_name)
	SELECT  UserID, AttendanceDate, Year, Semester, 
			CASE RecordType
               WHEN 'A' THEN 1
               WHEN 'L' THEN 2
               WHEN 'E' THEN 3 
			END,
          	CASE Slot 
               WHEN 'WD' THEN 1
               WHEN 'AM' THEN 2
               WHEN 'PM' THEN 3 
			END, 
			Reason, now(), now(), targetYearClassName, targetYearClassNumber
	FROM TEMP_ATTENDANCE_IMPORT";

}
if($format==2){

	$fields_name = "UserID, AttendanceDate, Year, Semester, RecordType, DayType, Reason, DateInput, DateModified, ClassName, ClassNumber";
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_ATTENDANCE ($fields_name)
	SELECT  UserID, AttendanceDate, Year, Semester,
			CASE RecordType
               WHEN 'A' THEN 1
               WHEN 'L' THEN 2
               WHEN 'E' THEN 3 
			END,
          	CASE Slot 
               WHEN 'WD' THEN 1
               WHEN 'AM' THEN 2
               WHEN 'PM' THEN 3 
			END, 
			Reason, now(), now(), targetYearClassName, targetYearClassNumber
	FROM TEMP_ATTENDANCE_IMPORT";


}

	
$li->db_db_query($sql);
	
header("Location: index.php?msg=10");

intranet_closedb();

?>