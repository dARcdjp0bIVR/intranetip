<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libattendance.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$lexport = new libexporttext();

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? "\"".$school_data[0]."\"\n\n" : "";
$school_name = (trim($school_data[0])!="") ? "\"".$school_data[0]."\"\n\n" : "";


$lattend = new libattendance();
$lword = new libwordtemplates();
$reasons = $lword->getWordListAttendance();

$class_name = $lattend->getClassName($classid);
$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

$sql = "SELECT ClassID FROM INTRNAET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";

$result = $lattend->getAttendanceListByClass($class_name,$start,$end,$reason);

$x_header = "";
for ($i=0; $i<sizeof($Fields); $i++)
{
	switch ($Fields[$i]) {
		case "absent": 
			$x_header .= ",\"".$i_Profile_Absent."\""; 
			$utf_x_header .= "\t".$i_Profile_Absent; 
			$f_absent=true; 
			break;
		case "late": 
			$x_header .= ",\"".$i_Profile_Late."\""; 
			$utf_x_header .= "\t".$i_Profile_Late; 
			$f_late=true; 
			break;
		case "earlyleave": 
			$x_header .= ",\"".$i_Profile_EarlyLeave."\""; 
			$utf_x_header .= "\t".$i_Profile_EarlyLeave; 
			$f_earlyleave=true; 
			break;
	}
}
$x = "\"".$i_UserClassNumber."\",\"".$i_UserEnglishName."\"".$x_header."\n";
$utf_x = $i_UserClassNumber."\t".$i_UserEnglishName.$utf_x_header."\r\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$absence,$late,$earlyleave) = $result[$i];
#    $display = ($classnumber != ""? "$classnumber. $name":"$name");
     $classnumber = ($classnumber==""? "&nbsp;":$classnumber);
	 $x .= "\"".$classnumber."\"";
	 $x .= ",\"".$name."\"";
	 $x .= ($f_absent) ? ",".$absence : "";
	 $x .= ($f_late) ? ",".$late : "";
	 $x .= ($f_earlyleave) ? ",".$earlyleave : "";
	 $x .= "\n";
	 
	 $utf_x .= $classnumber;
	 $utf_x .= "\t".$name;
	 $utf_x .= ($f_absent) ? "\t".$absence : "";
	 $utf_x .= ($f_late) ? "\t".$late : "";
	 $utf_x .= ($f_earlyleave) ? "\t".$earlyleave : "";
	 $utf_x .= "\r\n";
}

$functionbar = "\"$i_UserClassName:\",\"$class_name\"\n";
$utf_functionbar = "$i_UserClassName: \t$class_name\r\n";
$functionbar .= "\"$i_Attendance_Date:\",\"$i_Profile_From $start $i_Profile_To $end\"\n";
$utf_functionbar .= "$i_Attendance_Date: \t$i_Profile_From $start $i_Profile_To $end\n";
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$functionbar .= "\"$i_Attendance_Reason:\",\"$reasonSelect\"\n\n\n";
$utf_functionbar .= "$i_Attendance_Reason: \t$reasonSelect\r\n\r\n\r\n";

/*
if (!$g_encoding_unicode) {

	$url = "/file/export/eclass-user-".session_id()."-".time().".csv";
	$lo = new libfilesystem();
	$sExcel = $school_name.$functionbar.$x;
	$lo->file_write($sExcel, $intranet_root.$url);

	intranet_closedb();
	header("Location: $url");
} else {
	*/
	$filename = "eclass-user-".session_id()."-".time().".csv";
	$utf_sExcel = $utf_school_name.$utf_functionbar.$utf_x;
	$lexport->EXPORT_FILE($filename, $utf_sExcel);
	
	intranet_closedb();

//}

?>