<?php
// editing by 
/******************************** Changes *******************************
 * 2011-07-22 (Carlos): Added duplication checking failure message
 ************************************************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libattendance.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
include_once("../../../includes/libcardstudentattend2.php");


intranet_opendb();
$StudentAttendanceID = (is_array($StudentAttendanceID)? $StudentAttendanceID[0]: $StudentAttendanceID);
$lattend = new libattendance($StudentAttendanceID);
$lu = new libuser($lattend->UserID);
$lword = new libwordtemplates();
//$typeSelect = $lattend->getSelectType("name=type",$lattend->RecordType);
$currentType = $i_AttendanceTypeArray[$lattend->RecordType];
$dayTypeSelect = getSelectSchoolDayType("name=daytype",$lattend->DayType);
$wordlist = $lword->getSelectAttendance("onChange=\"this.form.reason.value=this.value\"");
$name = $lu->UserNameLang();

// $select_sem = getSelectSemester("name=semester",$lattend->Semester);
// $year_field = "<input type=text name=year value='$lattend->Year'>";
$date_field = "<input type=text name=attenddate value='$lattend->AttendanceDate' size=10 maxlength=10> <span class=extraInfo>(yyyy-mm-dd)</span>";


### allow input attendace record through student profile
$li = new libcardstudentattend2();
$disallow_input_attendace_record = $li->disallowInputInProfile();




## special setting for Late Record
if($lattend->RecordType==2 || $disallow_input_attendace_record==1){
	$select_sem = "$lattend->Semester<input type=hidden name=semester value='$lattend->Semester'>";
	$year_field = "$lattend->Year<input type=hidden name=year value='$lattend->Year'>";
	$date_field = "$lattend->AttendanceDate<input type=hidden name=attenddate value='$lattend->AttendanceDate'>";
     for ($i=1; $i<sizeof($i_DayTypeArray); $i++)
     {
          if($i==$lattend->DayType){
	          $str_daytype= $i_DayTypeArray[$i];
	      }
          
     }
	$dayTypeSelect = "$str_daytype<input type=hidden name=daytype value='$lattend->DayType'>";

}

?>

<script language="javascript">
function checkform(obj){
         if(!check_date(obj.attenddate,"<?="$i_alert_pleasefillin$i_Attendance_Date"?>")) return false;
         if(!check_text(obj.year,"<?="$i_alert_pleasefillin$i_Profile_Year"?>")) return false;
         return true;
}
</script>

<form action="edit_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Attendance, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, 'javascript:history.back()', $button_edit, '') ?>
<?= displayTag("head_attendance_$intranet_session_language.gif", $msg, ($xxmsg!=""?urldecode($xxmsg):"")) ?>

<blockquote>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<!--
<tr><td align=right><?php echo $i_Attendance_Year; ?>:</td><td><?=$year_field?></td></tr>
<tr><td align=right><?php echo $i_Profile_Semester; ?>:</td><td><?=$select_sem?></td></tr>
//-->
<tr><td align=right><?php echo $i_Attendance_Date; ?>:</td><td><?=$date_field?></td></tr>
<tr><td align=right><?php echo $i_Attendance_Type; ?>:</td><td><?=$currentType?></td></tr>
<tr><td align=right><?php echo $i_Attendance_DayType; ?>:</td><td><?=$dayTypeSelect?></td></tr>
<tr><td align=right><?php echo $i_Attendance_Reason; ?>:</td><td><input TYPE=text NAME=reason size=30 value="<?=$lattend->Reason?>"><?=$wordlist?></td></tr>
</table>
</blockquote>
<input type="hidden" name="StudentAttendanceID" value=<?=$StudentAttendanceID?>>
<input type="hidden" name="studentid" value=<?=$studentid?>>
<input type="hidden" name="classid" value=<?=$classid?>>
<input type="hidden" name="page_from" value="<?=$page_from?>">
<input type="hidden" name="class" value="<?=$class?>">
<input type="hidden" name="yrfilter" value="<?=$yrfilter?>">
<input type="hidden" name="pic" value="<?=$pic?>">
<input type="hidden" name="prev_reason" value="<?=$reason?>">
<input type="hidden" name="datetype" value="<?=$datetype?>">
<input type="hidden" name="type" value="<?=$lattend->RecordType?>">
<input type="hidden" name="date_from" value="<?=$date_from?>">
<input type="hidden" name="date_to" value="<?=$date_to?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="field" value="<?=$field?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>