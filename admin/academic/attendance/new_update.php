<?php
// editing by 
/********************* Changes ****************************
 * 2011-07-22 (Carlos): Added duplication checking
 **********************************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;

$reason = intranet_htmlspecialchars(trim($reason));
$attenddate = intranet_htmlspecialchars(trim($attenddate));
//$year = intranet_htmlspecialchars(trim($year));
//$semester = intranet_htmlspecialchars(trim($semester));

list($AcademicYearID, $year, $YearTermID, $semester) = getAcademicYearInfoAndTermInfoByDate($attenddate);

## Duplication checking
if($type == 1){ // Absent
	$cond_type = " RecordType IN (1,2) ";
}else if($type == 2){ // Late
	$cond_type = " RecordType IN (1,2) ";
}else{ // Earlyleave = 3 
	$cond_type = " RecordType IN (1,3) ";
}

if($daytype == 1){ // WD
	$cond_daytype = " DayType IN (1,2,3) ";
}else if($daytype == 2){ // AM
	$cond_daytype = " DayType IN (1,2) ";
}else if($daytype == 3){ // PM
	$cond_daytype = " DayType IN (1,3) ";
}
$sql = "SELECT StudentAttendanceID FROM PROFILE_STUDENT_ATTENDANCE WHERE UserID = '$studentid' AND DATE_FORMAT(AttendanceDate,'%Y-%m-%d') = '$attenddate' AND $cond_daytype AND $cond_type ";
$profileids = $li->returnVector($sql);
if(count($profileids) >= 1){
	intranet_closedb();
	header("Location: new.php?studentid=$studentid&classid=$classid&attenddate=$attenddate&type=$type&daytype=$daytype=&xxmsg=".urlencode($i_con_msg_update_failed));
}
## end checking

$fieldname = "UserID, AttendanceDate,Year, Semester, RecordType, DayType, Reason, DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "'$studentid','$attenddate','".$li->Get_Safe_Sql_Query($year)."','".$li->Get_Safe_Sql_Query($semester)."','$type','$daytype','".$li->Get_Safe_Sql_Query($reason)."',now(),now(),'".$li->Get_Safe_Sql_Query($classname)."','$classnumber'";
$sql = "INSERT INTO PROFILE_STUDENT_ATTENDANCE ($fieldname) VALUES ($fieldvalue)";

$li->db_db_query($sql);

$warning_student_ids = array();

if ($plugin['Discipline'] && $type == 2)
{
	include_once("../../../includes/libdiscipline.php");
                $ldiscipline = new libdiscipline();
                
                if($ldiscipline->isUseAccumulativeLateSetting($attenddate)){
	                 
	                  ## Accumulative Punishment , Added on 2007-07-25
	                    if ($ldiscipline->calculateAccumulativeUpgradeLateToDemerit($studentid,$attenddate))
                        {
                            $warning_student_ids[] = $studentid;
                        }
                        $ldiscipline->calculateAccumulativeUpgradeLateToDetention($studentid,$attenddate);
                        
	            }
                else{
	                if($ldiscipline->calculateUpgradeLateToDemerit($studentid))
	                {
	                   $warning_student_ids[] = $studentid;
	                   echo "warning required";
	                }
	                $ldiscipline->calculateUpgradeLateToDetention($studentid);
	            }
}
intranet_closedb();

if (sizeof($warning_student_ids)!=0)
{
        $body_tags = "onLoad=document.form1.submit()";
        include_once("../../../templates/fileheader.php");
        ?>
        <form name=form1 action=shownotice_msg_prompt.php method=POST>
        <?
        for ($i=0; $i<sizeof($warning_student_ids); $i++)
        {
             ?>
             <input type=hidden name=WarningStudentID[] value="<?=$warning_student_ids[$i]?>">
             <?
        }
        ?>
        <input type=hidden name=studentid value="<?=$studentid?>">
        <input type=hidden name=classid value="<?=$classid?>">
        <input type=hidden name=TargetDate value="<?=$attenddate?>">
        </form>
        <?
        include_once("../../../templates/filefooter.php");

}
else
{
    header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
}
?>