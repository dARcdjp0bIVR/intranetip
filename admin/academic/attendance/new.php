<?php
# editing by 
/******************************** Changes *******************************
 * 2011-07-22 (Carlos): Added duplication checking failure message
 ************************************************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libattendance.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();
$lu = new libuser($studentid);
$lattend = new libattendance();
$lword = new libwordtemplates();
$typeSelect = $lattend->getSelectType("name=type", $type);
$dayTypeSelect = getSelectSchoolDayType("name=daytype", $daytype);
$wordlist = $lword->getSelectAttendance("onChange=\"this.form.reason.value=this.value\"");
$name = $lu->UserNameLang();
// $select_sem = getSelectSemester("name=semester");

?>

<script language="javascript">
function checkform(obj){
	if(!check_date(obj.attenddate,"<?="$i_alert_pleasefillin$i_Attendance_Date"?>")) return false;
	if(!check_text(obj.year,"<?="$i_alert_pleasefillin$i_Profile_Year"?>")) return false;
	return true;
}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Attendance, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_attendance_$intranet_session_language.gif", $msg, ($xxmsg!=""?urldecode($xxmsg):"")) ?>


<blockquote>
<font color="red">
	<?=$Lang['StudentAttendance']['AdminConsole']['ImportNewLinkageNote1']?>
	<? if($plugin['Disciplinev12']) echo $Lang['StudentAttendance']['AdminConsole']['ImportNewLinkageNote2']?>
	<br>
	<?=$Lang['StudentAttendance']['AdminConsole']['ImportNewLinkageNote3']?>
</font>
<br><br>
<table width=500 border=0 cellpadding=5 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<!--
<tr><td align=right><?php echo $i_Profile_Year; ?>:</td><td><?=$select_academicYear?></td></tr>
<tr><td align=right><?php echo $i_Profile_Semester; ?>:</td><td><div id="semester_div"><?=$select_sem?></div></td></tr>
//-->
<tr><td align=right><?php echo $i_Attendance_Date; ?>:</td><td><input type=text name=attenddate value="<?=($attenddate!=""?$attenddate:date("Y-m-d"))?>" size=10 maxlength=10> <span class=extraInfo>(yyyy-mm-dd)</span></td></tr>
<tr><td align=right><?php echo $i_Attendance_Type; ?>:</td><td><?=$typeSelect?></td></tr>
<tr><td align=right><?php echo $i_Attendance_DayType; ?>:</td><td><?=$dayTypeSelect?></td></tr>
<tr><td align=right><?php echo $i_Attendance_Reason; ?>:</td><td><input TYPE=text NAME=reason size=30><?=$wordlist?></td></tr>
</table>
</blockquote>
<input type=hidden name=studentid value=<?=$studentid?>>
<input type=hidden name=classid value=<?=$classid?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<?php
include_once("../../../templates/adminfooter.php");
?>