<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libattendance.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/fileheader.php");
//include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$imgfile = $li->file_read($intranet_root."/file/schoolbadge.txt");
$schoolbadge = ($imgfile!="") ? "<img src='/file/$imgfile' width=120 height=60><br>\n" : "&nbsp;";

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? $school_data[0] : "&nbsp;";

if ($schoolbadge!="&nbsp;" || $school_name!="&nbsp;")
{
	$school_info = "<table border=0 width=650>\n";
	$school_info .= "<tr><td align=center>$schoolbadge <font size=+0><b>$school_name</b></font></td></tr>\n";
	$school_info .= "</table>\n<br>\n";
}

$lvlArray = array();
$lvlAbsence = array();
$lvlLate = array();
$lvlEarlyLeave = array();

$lattend = new libattendance();
$lword = new libwordtemplates();
$reasons = $lword->getWordListAttendance();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

$sql = "SELECT ClassID FROM INTRNAET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
$result = $lattend->getClassAttendanceList($start,$end,$reason);

$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=25% class=tableTitle>$i_UserClassName</td>
<td width=25% class=tableTitle>$i_Profile_Absent</td>
<td width=25% class=tableTitle>$i_Profile_Late</td>
<td width=25% class=tableTitle>$i_Profile_EarlyLeave</td>
</tr>
";
$en_reason = urlencode($reason);

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$lvlID,$absence,$late,$earlyleave) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlAbsence[$j] += $absence;
              $lvlLate[$j] += $late;
              $lvlEarlyLeave[$j] += $earlyleave;
              break;
          }
     }
     $link = "$name";
     $x .= "<tr>
             <td class=tableContent>$link</td>
             <td class=tableContent>$absence</td>
             <td class=tableContent>$late</td>
             <td class=tableContent>$earlyleave</td>
            </tr>\n";
}
$x .= "</table>\n";


$lvls = $lattend->getLevelList();
$y = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=25% class=tableTitle>$i_UserClassLevel</td>
<td width=25% class=tableTitle>$i_Profile_Absent</td>
<td width=25% class=tableTitle>$i_Profile_Late</td>
<td width=25% class=tableTitle>$i_Profile_EarlyLeave</td>
</tr>
";
for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $absence = $lvlAbsence[$i];
     $late = $lvlLate[$i];
     $earlyleave = $lvlEarlyLeave[$i];

     $y .= "<tr>
             <td class=tableContent>$name</td>
             <td class=tableContent>$absence</td>
             <td class=tableContent>$late</td>
             <td class=tableContent>$earlyleave</td>
            </tr>\n";
}
$y .= "</table>\n";

$functionbar = "&nbsp; $i_Attendance_Date: $i_Profile_From $start $i_Profile_To $end";
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$functionbar .= "<br>&nbsp; $i_Attendance_Reason: $reasonSelect";

?>


<form name="form1" method="get">
<?=$school_info?>
<table width=560 border=0 cellpadding=0 cellspacing=0>
<tr><td><font size='+0'><u><?=$i_Profile_Attendance ?></u></font></td></tr>
<tr><td><?= $functionbar ?></td></tr>
<tr><td class=tableContent><br><br>
<?=$x?>
</td></tr>
<tr><td><br><br></td></tr>
<tr><td class=tableContent>
<?=$y?>
</td></tr>
</table>
<br>


<table border=0 width=550>
<tr><td align=center>
<a href="javascript:window.print()"><img class="print_hide" src="/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</form>



<?
include_once("../../../templates/filefooter.php");
?>