<?php
# using: yat

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libdbtable.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libattendance.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
include_once("../../../includes/libcardstudentattend2.php");

intranet_opendb();


### allow input attendace record through student profile
$li = new libcardstudentattend2();
$disallow_input_attendace_record = $li->disallowInputInProfile();



if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

#$lu = new libuser($studentid);
$conds = "";
if ($yrfilter != "")
{
    $conds .= " AND a.Year = '$yrfilter'";
}
/*
if ($semester != "")
{
    $conds .= " AND a.Semester = '$semester'";
}
*/
if ($class != "")
{
    $conds .= " AND b.ClassName = '$class'";
}
if ($RecordType != "")
{
    $conds .= " AND a.RecordType = '$RecordType'";
}
if ($reason != "")
{
    $conds .= " AND a.Reason = '$reason'";
}
if ($datetype != 0)
{
    $date_field = "";
    if ($datetype == 1)
    {
        $date_field = "a.AttendanceDate";
    }
    else if ($datetype == 2)
    {
         $date_field = "a.DateModified";
    }
    if ($date_field != "")
    {
        if ($date_from != "")
        {
            $conds .= " AND $date_field >= '$date_from'";
        }
        if ($date_to != "")
        {
            $conds .= " AND $date_field <= '$date_to 23:59:59'";
        }
    }
}

# TABLE SQL
$keyword = trim($keyword);
if($field=="") $field = 0;
if($order=="") $order = 1;
switch ($field){
     case 0: $field = 0; break;
     case 1: $field = 1; break;
     case 2: $field = 2; break;
     case 3: $field = 3; break;
     case 4: $field = 4; break;
     case 5: $field = 5; break;
     case 6: $field = 6; break;
     default: $field = 0; break;
}

$studentnamefield = getNameFieldWithClassNumberByLang("b.");

$sql = "SELECT
              $studentnamefield,
              DATE_FORMAT(a.AttendanceDate,'%Y-%m-%d'),
              a.Year,
              CASE a.RecordType
                   WHEN 1 THEN '$i_Profile_Absent'
                   WHEN 2 THEN '$i_Profile_Late'
                   WHEN 3 THEN '$i_Profile_EarlyLeave'
                   ELSE '-' END,
              CASE a.DayType
                   WHEN 1 THEN '$i_DayTypeWholeDay'
                   WHEN 2 THEN '$i_DayTypeAM'
                   WHEN 3 THEN '$i_DayTypePM'
                   ELSE '-' END,
              a.Reason,
              a.DateModified,
              CONCAT('<input type=checkbox name=StudentAttendanceID[] value=', a.StudentAttendanceID ,'>')
        FROM PROFILE_STUDENT_ATTENDANCE as a
             LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
        WHERE b.RecordStatus IN (0,1,2) AND b.UserID IS NOT NULL AND (a.Reason like '%$keyword%'
                   OR b.EnglishName like '%$keyword%'
                   OR b.ChineseName like '%$keyword%'
                   OR b.UserLogin like '%$keyword%'
                   OR b.ClassNumber like '%$keyword%'
              ) $conds
              ";
# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
$li->field_array = array("b.EnglishName","a.AttendanceDate","a.Year","a.RecordType", "a.DayType","a.Reason","a.DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0,0,0,0,0,0);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_UserStudentName)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Attendance_Date)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Attendance_Year)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Attendance_Type)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column($pos++, $i_Attendance_DayType)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column($pos++, $i_Attendance_Reason)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_Attendance_Modified)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("StudentAttendanceID[]")."</td>\n";

#$toolbar = "<a class=iconLink href=\"javascript:checkPost(document.form1,'new.php')\">".newIcon()."$button_new</a>";
$toolbar = "<a class=iconLink href=\"javascript:void(0);\" onclick=\"javascript:checkGet(document.form1,'detail_export.php');document.form1.action=''\">".exportIcon()."$button_export</a>";
// TABLE FUNCTION BAR
$functionbar .= "<a href=\"javascript:checkEdit(document.form1,'StudentAttendanceID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
if($disallow_input_attendace_record!=1){
	$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'StudentAttendanceID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
}
$searchbar = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

#$printAnalyze = "<a class=iconLink href='javascript:openAnalysisPage()'>".analysisIcon()."$i_Profile_Attendance_Analysis</a>\n";
#$printAnalyze .= "<a class=iconLink href='javascript:openPrintPage()'>".printIcon()."$i_PrinterFriendlyPage</a>\n";
#$name = $lu->UserNameLang();

$lattend = new libattendance();
$years = $lattend->returnYears();
$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=class",$class);
$select_year = getSelectByValue($years,"name=yrfilter",$yrfilter,1);
$lword = new libwordtemplates();
$attendReasons = $lword->getWordListAttendance();
$reasonSelect = getSelectByValue($attendReasons,"name=reason",$reason,1);
$filterbar = "<table width=100% border=0 cellpadding=1 cellspacing=1>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_Year: </td><td>$select_year<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_UserClassName: </td><td>$select_class<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_SelectReason: </td><td>$reasonSelect<td></tr>\n";
$filterbar .= "<tr><td width=20% align=right>$i_Profile_SearchByDate: </td><td>";
$filterbar .= "<input type=radio name=datetype value=0 ".($datetype==0?"CHECKED":"")."> $i_Profile_NotSearchByDate";
$filterbar .= "&nbsp;<input type=radio name=datetype value=1 ".($datetype==1?"CHECKED":"")."> $i_Profile_ByRecordDate";
$filterbar .= "&nbsp;<input type=radio name=datetype value=2 ".($datetype==2?"CHECKED":"")."> $i_Profile_ByLastModified";
$filterbar .= "<br>\n$i_From : <input type=text name=date_from size=10 maxlength=10 value='$date_from'> $i_To : <input type=text name=date_to size=10 maxlength=10 value='$date_to'> (YYYY-MM-DD)<br>\n";
$filterbar .= "<input type=image src=\"$image_path/admin/button/s_btn_find_$intranet_session_language.gif\"><td></tr>";
$filterbar .= "</table>\n";

#$filterbar = "$button_select $i_Profile_Year: ".getSelectByValue($years,"name=yrfilter onChange='this.form.submit()'",$yrfilter,1);


?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function openAnalysisPage()
{
        newWindow("studentanalysis.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
function openPrintPage()
{
        newWindow("studentprint.php?studentid=<?=$studentid?>&keyword=<?=$keyword?>&yrfilter=<?=$yrfilter?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Attendance, 'index.php', $i_general_ViewDetailRecords, '') ?>
<?= displayTag("head_attendance_$intranet_session_language.gif", $msg) ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $filterbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent>
<?=$li->display()?>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=studentid value="<?=$studentid?>">
<input type=hidden name=classid value="<?=$classid?>">
<input type=hidden name=page_from value="<?="detail.php"?>">
</form>
<?
include_once("../../../templates/adminfooter.php");
?>