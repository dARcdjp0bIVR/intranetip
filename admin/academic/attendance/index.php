<?php
// Editing by 
/*
 * 2014-05-27 (Carlos): added time components to datetime variables $start and $end to correctly wrap a date range
 */
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libattendance.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");
include_once("../../../includes/libcardstudentattend2.php");


intranet_opendb();

### allow input attendace record through student profile
$li = new libcardstudentattend2();
$disallow_input_attendace_record = $li->disallowInputInProfile();
 
$lvlArray = array();
$lvlAbsence = array();
$lvlLate = array();
$lvlEarlyLeave = array();

$lattend = new libattendance();
$lword = new libwordtemplates();
$reasons = $lword->getWordListAttendance();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));

$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}
$start_datetime = $start." 00:00:00";
$end_datetime = $end." 23:59:59";

$sql = "SELECT ClassID FROM INTRNAET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
$result = $lattend->getClassAttendanceList($start_datetime,$end_datetime,$reason);

$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=25% class=tableTitle>$i_UserClassName</td>
<td width=25% class=tableTitle>$i_Profile_Absent</td>
<td width=25% class=tableTitle>$i_Profile_Late</td>
<td width=25% class=tableTitle>$i_Profile_EarlyLeave</td>
</tr>
";
$en_reason = urlencode($reason);

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$lvlID,$absence,$late,$earlyleave) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlAbsence[$j] += $absence;
              $lvlLate[$j] += $late;
              $lvlEarlyLeave[$j] += $earlyleave;
              break;
          }
     }
         $css = ($i%2) ? "2" : "";
     $link = "<a class=functionlink href=classview.php?classid=$id&start=$start&end=$end&reason=$en_reason&datetype=$datetype>$name</a>";
     $x .= "<tr>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$absence</td>
             <td class=tableContent$css>$late</td>
             <td class=tableContent$css>$earlyleave</td>
            </tr>\n";
}
$x .= "</table>\n";


$lvls = $lattend->getLevelList();
$y = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=25% class=tableTitle>$i_UserClassLevel</td>
<td width=25% class=tableTitle>$i_Profile_Absent</td>
<td width=25% class=tableTitle>$i_Profile_Late</td>
<td width=25% class=tableTitle>$i_Profile_EarlyLeave</td>
</tr>
";
for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $absence = $lvlAbsence[$i];
     $late = $lvlLate[$i];
     $earlyleave = $lvlEarlyLeave[$i];

         $css = ($i%2) ? "2" : "";
     $y .= "<tr>
             <td class=tableContent$css>$name</td>
             <td class=tableContent$css>$absence</td>
             <td class=tableContent$css>$late</td>
             <td class=tableContent$css>$earlyleave</td>
            </tr>\n";
}
$y .= "</table>\n";

$datetype += 0;
$selected[$datetype] = "SELECTED";

$classSelect = "<SELECT name=datetype onChange=\"changeDateType(this.form)\">\n";
$classSelect .= "<OPTION value=0 ".$selected[0].">$i_Profile_Today</OPTION>\n";
$classSelect .= "<OPTION value=1 ".$selected[1].">$i_Profile_ThisWeek</OPTION>\n";
$classSelect .= "<OPTION value=2 ".$selected[2].">$i_Profile_ThisMonth</OPTION>\n";
$classSelect .= "<OPTION value=3 ".$selected[3].">$i_Profile_ThisAcademicYear</OPTION>\n";
$classSelect .= "</SELECT> \n";
$functionbar = "<table border=0>\n";
$functionbar .= "<tr><td nowrap>$i_Profile_SelectSemester:</td><td>$classSelect<br>$i_Profile_From <input type=text name=start size=10 value='$start'> $i_Profile_To ";
$functionbar .= "<input type=text name=end size=10 value='$end'> <span class=extraInfo>(yyyy-mm-dd) </span> <a href='javascript:document.form1.submit()'><img src='/images/admin/button/s_btn_submit_$intranet_session_language.gif' border='0' align='absmiddle'></a></td></tr>";
$reasonSelect = getSelectByValue($reasons,"name=reason onChange=this.form.submit()",$reason,1);
$functionbar .= "<tr><td nowrap>$i_Profile_SelectReason:</td><td>$reasonSelect</td></tr>\n";
$functionbar .= "</table>\n";

?>
<SCRIPT LANGUAGE=JAVASCRIPT>
function changeDateType(obj)
{
         switch (obj.datetype.value)
         {
                 case '0':
                      obj.start.value = '<?=$today?>';
                      obj.end.value = '<?=$today?>';
                      break;
                 case '1':
                      obj.start.value = '<?=$weekstart?>';
                      obj.end.value = '<?=$weekend?>';
                      break;
                 case '2':
                      obj.start.value = '<?=$monthstart?>';
                      obj.end.value = '<?=$monthend?>';
                      break;
                 case '3':
                      obj.start.value = '<?=$yearstart?>';
                      obj.end.value = '<?=$yearend?>';
                      break;
         }
         obj.submit();
}

function openPrintPage()
{
         newWindow("indexprint.php?start=<?=$start?>&end=<?=$end?>&reason=<?=$en_reason?>",4);
}
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Attendance, '') ?>
<?= displayTag("head_attendance_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>

<?php 
	## allow input attendance record through student profile
	if($disallow_input_attendace_record!=1){
?>
		<a class=iconLink href=import.php><?=importIcon().$button_import?></a>
<?php }?>
<a class=iconLink href="javascript:checkGet(document.form1,'export.php')"><?=exportIcon().$button_export?></a>
<a class=iconLink href=javascript:openPrintPage()><?=printIcon().$i_PrinterFriendlyPage?></a>
<a class=iconLink href="detail.php"><?=detailIcon().$i_general_ViewDetailRecords?></a>
</td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
<tr><td class=tableContent align=center>
<?=$x?>
</td></tr>
<tr><td><br><br></td></tr>
<tr><td class=tableContent align=center>
<?=$y?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>