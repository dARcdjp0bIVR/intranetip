<?php
//using: 
/*****************************************
 * Date:	2013-04-24 (YatWoon)
 *			Fixed: "role" data is overwided by session "role" data [Case#2013-0423-1113-49071]
 *****************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;

//$year = intranet_htmlspecialchars(trim($year));
$academic_year = new academic_year($AcademicYearID);
$year = $academic_year->YearNameEN;
$academic_year_term = new academic_year_term($YearTermID);
$semester = $academic_year_term->YearTermNameEN;

$serviceDate = intranet_htmlspecialchars(trim($serviceDate));
$service = intranet_htmlspecialchars(trim($service));
$service_role = intranet_htmlspecialchars(trim($service_role));
$performance = intranet_htmlspecialchars(trim($performance));
$organization = intranet_htmlspecialchars(trim($organization));
$remark = intranet_htmlspecialchars(trim($remark));
$serviceDate = ($serviceDate==""? "NULL":"'$serviceDate'");

$fieldname = "UserID, Year, Semester,ServiceDate, ServiceName,Role,Performance,Organization,Remark, DateInput,DateModified,ClassName,ClassNumber";
$fieldvalue = "'$studentid','$year','$semester',$serviceDate,'$service','$service_role','$performance','$organization','$remark',now(),now(),'$classname','$classnumber'";
$sql = "INSERT INTO PROFILE_STUDENT_SERVICE ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
?>
