<?php
# using: 

/**
#	2017-08-16 	Simon
#	map the class name and class number with the student referring to academic year when import records
#	includes libuser.php
#
#	2011-03-02	YatWoon
#	Add display error msg if "Year" not found
#	
#	2011-02-08	YatWoon
#	Allow import term with term name "0", that mean the record is for "Whole Year" (Case: #2011-0207-1142-51067)
#	
#	2011-01-19	YatWoon
#	update getCurrentSemester(), force to retrieve English semester title for import
#

 * Type			: Enhancement
 * Date 		: 200911301532
 * Description	: 1C) Add checking on Year and Semester
 * By			: Max Wong
 * C=CurrentIssue
 * Case Number	: 200911301532MaxWong
 */

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once("../function.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_opendb();

$limport = new libimporttext();

$file_format1 = array("Year","Semester","Date","Class","Class Number","Service Name","Role","Performance","Remark","Organization");
$file_format2 = array("Year","Semester","Date","WebSAMSRegNo","Service Name","Role","Performance","Remark","Organization");

$li = new libdb();
$lo = new libfilesystem();
$filepath = $userfile;
$filename = $userfile_name;

if($filepath=="none" || $filepath == ""){          # import failed
        header("Location: import.php");
} else {

		$SystemYearTermMapping = getAllAcademicYear_Term();
	
        $ext = strtoupper($lo->file_ext($filename));
        if($limport->CHECK_FILE_EXT($filename))
        {
	       $error_code1 = false;
	       $error_code2 = false;
	       $error_code3 = false;
	       $error_code4 = false;
	       
           # read file into array
           # return 0 if fail, return csv array if success
           //$data = $lo->file_read_csv($filepath);
           $data = $limport->GET_IMPORT_TXT($filepath);
           $col_name = array_shift($data);                   
		   
           #Check file format
           $format_wrong = false;
           if ($format==1)
           {
	           for ($i=0; $i<sizeof($file_format1); $i++)
	           {
	                if ($col_name[$i] != $file_format1[$i])
	                {
	                    $format_wrong = true;
	                    break;
	                }
	           }
           }
           if ($format==2)
           {
	           for ($i=0; $i<sizeof($file_format2); $i++)
	           {
	                if ($col_name[$i]!=$file_format2[$i])
	                {
	                    $format_wrong = true;
	                    break;
	                }
	           }
           }
           if ($format_wrong)
           {
               //header ("Location: import.php");
               $error=1;
               $error_code=1;
               $error_code1 = true;
               
           }
           else
           {
               $t_delimiter = "";
               $toTempValues = "";
               
               $currentYear = getCurrentAcademicYear();
               $currentSem = getCurrentSemester('EN');
               $defaultUnit = 1;
               
               for ($i=0; $i<sizeof($data); $i++)
               {
	               if(is_array($data[$i]) && sizeof($data[$i])>0)
	               {
		               if ($format==1)
		                list($year,$sem,$date,$class,$classnum,$service,$role,$perform,$remark,$organization) = $data[$i];
	                   if ($format==2)
	                    list($year,$sem,$date,$websamsregno,$service,$role,$perform,$remark,$organization) = $data[$i];
	        
	                    $year = ($year == ""? $currentYear : $year);
	                    
	                    ##### 2011-02-08 YatWoon: If term=0, that mean the record is "whole year" record
	                    if(strlen($sem) && $sem=="0")
	                    {
		                    $sem = "";
	                    }
	                    else
	                    {
	                    	$sem = ($sem == ""? $currentSem : $sem);
                    	}
                    	
	                    $date = (($date == "")? "NULL": "'$date'");
	                    // trim any "#" at the beginning of the WebSam Reg. No.
	                    $websamsregno = ltrim(trim(addslashes($websamsregno)), "#");
	                    if (!empty($websamsregno))
	                    	$websamsregno = "#".$websamsregno;
	                    $class = trim(addslashes($class));
	                    $classnum = trim(addslashes($classnum));
	                    $service = trim(addslashes($service));
	                    $role = trim(addslashes($role));
	                    $perform = trim(addslashes($perform));
	                    $remark = trim(addslashes($remark));
	                    $organization = trim(addslashes($organization));
	                    
	                    # Get Record target Year Student ClassName, ClassNum
	                    $_classname = '';
	                    $_classnumber = '';
	                    $studentUserId	= '';
	                    $yearresult = '';
	                    $sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR WHERE YearNameEN = '$year' ";
	                    $yearresult = $li->returnVector($sql);
	                    
	                    
	                    $sqlresult = '';
	                    if(!empty($yearresult)){
	                    	$_academicYearID = $yearresult[0];
	                    }
	                    if($format==1){
	                    	$sql = "select userid from INTRANET_USER where classname= '".$class."' and classnumber=".$classnum." and RecordType = 2 AND RecordStatus IN (0,1,2)";
	                    }
	                    else if ($format==2){
	                    	$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo = '".$websamsregno."' OR WebSAMSRegNo = '".ltrim($websamsregno, "#")."' AND RecordType = '2' AND RecordStatus IN ('0','1','2')";
	                    }
	                    $sqlresult = $li->returnVector($sql);
	                    $studentUserId = $sqlresult[0];
	                    
	                    if(!empty($sqlresult)){
	                    	$libuser = new libuser($studentUserId);
	                    	$classinfoAry = $libuser->getStudentInfoByAcademicYear($_academicYearID);
	                    	$_classname = $classinfoAry[0]['ClassNameEn'];
	                    	$_classnumber = $classinfoAry[0]['ClassNumber'];
	                    }
	                    
	                    $targetYearClassName = $_classname;
	                    $targetYearClassNumber = $_classnumber;
	                    
// 	                    
	     				if ( $format==1 && ($class == "" || $classnum == "" || $service == ""))
	                	{
	                    	$error=1;
	                		$error_code=4;
	            		}
	                		
	                	if ($format==2 && ($websamsregno=="" || $service=="")	)
	                	{
	                    	$error=1;
	                		$error_code=4;
	                	}
	                		
						# 6/24/2008 Yat Woon, leave the date empty if the import data is empty.
// 		               if ($date == "NULL")
// 		                	$date = "now()"; 
		                	
	                    if ($format==1){
		                    	$toTempValues .= "$t_delimiter ('$studentUserId',$date,'$year','$sem','$class','$classnum','$targetYearClassName','$targetYearClassNumber','$service','$role','$perform','$remark','$organization')";
		                        $t_delimiter = ",";
	                   
	                 	}
	                 	if ($format==2){
	
		                 		$toTempValues .= "$t_delimiter ('$studentUserId',$date,'$year','$sem','$websamsregno','$targetYearClassName','$targetYearClassNumber','$service','$role','$perform','$remark','$organization')";
		                        $t_delimiter = ",";
	
	                 	}	
                 	}
               }
				
               
               # Create temp table
               $sql = "DROP TABLE TEMP_SERVICE_IMPORT";
			   $li->db_db_query($sql);
			   if ($format==1) {
	               $sql = "CREATE TABLE TEMP_SERVICE_IMPORT (
	               			  UserID int(11),
                              ServiceDate datetime,
                              Year char(20),
                              Semester varchar(255),
                              curClassName varchar(255),
                              curClassNumber varchar(255),
               				  targetYearClassName varchar(255),
                              targetYearClassNumber varchar(255),
                              ServiceName text,
                              Role char(100),
                              Performance text,
                              Remark text,
                              Organization varchar(200)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                }
                
                if ($format==2) {
                	$sql = "CREATE TABLE TEMP_SERVICE_IMPORT (
                              UserID int(11),  
                			  ServiceDate datetime,
                              Year char(20),
                              Semester varchar(255),
                              WebSAMSRegNo varchar(100),
                			  targetYearClassName varchar(255),
                              targetYearClassNumber varchar(255),
                              ServiceName text,
                              Role char(100),
                              Performance text,
                              Remark text,
                              Organization varchar(200)
                              ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
                }
               $li->db_db_query($sql);
               if ($format==1)
               		$fields_name = "UserID, ServiceDate, Year,Semester, curClassName, curClassNumber, targetYearClassName, targetYearClassNumber, ServiceName, Role,Performance,Remark,Organization";
               		
               if ($format==2)
               		$fields_name = "UserID, ServiceDate, Year,Semester, WebSAMSRegNo, targetYearClassName, targetYearClassNumber, ServiceName, Role,Performance,Remark,Organization";
               
               $sql = "INSERT IGNORE INTO TEMP_SERVICE_IMPORT ($fields_name) VALUES $toTempValues";

               $li->db_db_query($sql);
               
               $sql ="SELECT ".$fields_name." FROM TEMP_SERVICE_IMPORT";
               
               if($format==1)
               		//$original = $li->returnArray($sql,9);
               		$original = $li->returnArray($sql);
               if($format==2)
//                		$original = $li->returnArray($sql,8);
               		$original = $li->returnArray($sql);
	       	   
    		    $display = "<tr>
				<td class=tableTitle width=130>$i_ServiceYear</td>
				<td class=tableTitle width=60>$i_ServiceSemester</td>
				<td class=tableTitle width=60>$i_ServiceDate</td>";
				if ($format==2)
					$display .= "<td class=tableTitle width=60>$i_WebSAMS_Registration_No</td>";
				if ($format==1)
					{$display .= "<td class=tableTitle width=60>$i_ClassName</td>";
					$display .= "<td class=tableTitle width=80>$i_ClassNumber</td>";}
					
				$display .= "<td class=tableTitle width=90>$i_ServiceName</td>
				<td class=tableTitle width=80>$i_ServiceRole</td>
				<td class=tableTitle width=80>$i_ServicePerformance</td>
				<td class=tableTitle width=80>$i_ServiceRemark</td>
				<td class=tableTitle width=80>$i_ServiceOrganization</td>
				<td class=tableTitle width=80>".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>
				</tr>\n";

               for ($i=0; $i<sizeof($original); $i++) {
               	$error_code1_this = false;
		        $error_code2_this = false;
		        $error_code3_this = false;
		        $error_code4_this = false;
               	$css = ($i%2? "":"2");
			   	$display .= "<tr class=tableContent$css><td>";
			   
					//CHECK USER INPUT YEAR AND SEM ACTUALLY MAP WITH SYSTEM SETTING (ACADEMIC_YEAR , ACADEMIC_YEAR_TERM)
					$yearSemIsValid = checkYearSemMapWithSystem($original[$i]['Year'],$original[$i]['Semester'],$SystemYearTermMapping);

					///////////////////////////////////
				if ($format==1) {
// 					list($date, $year,$sem,$class,$classnum,$service,$role,$perform,$remark,$organization) = $original[$i];
					list($studentUserId, $date, $year,$sem,$class,$classnum,$targetYearClassName,$targetYearClassNumber,$service,$role,$perform,$remark,$organization) = $original[$i];
// 			   		$sql = "select userid from INTRANET_USER where classname= '".$class."' and classnumber=".$classnum." and 
// 					RecordType = 2 AND RecordStatus IN (0,1,2)";			
// 					$result = $li->returnArray($sql,1);
						
			   		if ($service=="") {
			   			#$service="<font color=red><i>*</i></font>";
			   			$error_code4_this = true;
		   			}
		   			
		   			$display_sem = $sem ? $sem : $Lang['General']['WholeYear'];
			   		$display .=$year."</td><td>$display_sem&nbsp;</td><td>$date&nbsp;</td><td>$class&nbsp;</td><td>$classnum&nbsp;</td><td>$service&nbsp;</td><td>$role&nbsp;</td><td>$perform&nbsp;</td><td>$remark&nbsp;</td><td>$organization&nbsp;</td>\n";
			   		
			   		##### 2011-02-08 YatWoon: If term=0, that mean the record is "whole year" record
			   		##### no need to check the term is valid or not
			   		if (($yearSemIsValid==-1 && $original[$i]['Semester']) || $yearSemIsValid==-2)
			    	{
						$error = 1;
					   	$error_code5_this=true;
					   	$error_num = "@ ";
					}
					
					if ($studentUserId== null || $studentUserId== '' || $studentUserId== '0'){
// 			    	if ($result==null){
						$error=1;
				   		$error_code=2;
				   		$error_code2_this=true;
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		if ($error_code2_this)
				   			$error_num .= "# ";
				   		$error_num = rtrim($error_num, " ");
			   			$display .="<td><font color=red>$error_num</font></td></tr>";
			   		} else {
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		$display .="<td>".(isset($error_num)?"<font color=red>$error_num</font>":"-")."</td></tr>";
			   		}
				}
				
				if ($format==2) {
// 					list($date,$year,$sem,$websamsregno,$service,$role,$perform,$remark,$organization) = $original[$i];
					list($studentUserId,$date,$year,$sem,$websamsregno,$targetYearClassName,$targetYearClassNumber,$service,$role,$perform,$remark,$organization) = $original[$i];
// 			   		$sql = "SELECT UserID FROM INTRANET_USER WHERE WebSAMSRegNo = '".$websamsregno."' OR WebSAMSRegNo = '".ltrim($websamsregno, "#")."' AND RecordType = '2' AND RecordStatus IN ('0','1','2')";			
// 					$result = $li->returnArray($sql,2);
					#echo "<pre>";
					#print_r($result);
					#echo "</pre>";
			   		#exit;
					if ($service=="" || $websamsregno=="") {
			   			$error_code4_this = true;
		   			}
					
		   			$display_sem = $sem ? $sem : $Lang['General']['WholeYear'];
		   			$display .=$year."</td><td>$display_sem&nbsp;</td><td>$date&nbsp;</td><td>$websamsregno&nbsp;</td><td>$service&nbsp;</td><td>$role&nbsp;</td><td>$perform&nbsp;</td><td>$remark&nbsp;</td><td>$organization&nbsp;</td>\n";
			   		
		   			if (($yearSemIsValid==-1 && $original[$i]['Semester']) || $yearSemIsValid==-2)
		   			{
						$error = 1;
					   	$error_code5_this=true;
					   	$error_num = "@ ";
					}
					if ($studentUserId== null || $studentUserId== '' || $studentUserId== '0'){
// 			   		if ($result==null) {
				   		$error=1;
				   		$error_code=3;
				   		$error_code3_this=true;
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		if ($error_code3_this)
				   			$error_num .= "^ ";
				   		$error_num = rtrim($error_num, " ");
			   			$display .="<td><font color=red>$error_num</font></td></tr>";
			   		} else {
				   		if ($error_code4_this)
				   			$error_num .= "* ";
				   		if ($error_code3_this)
				   			$error_num .= "^ ";
				   		$display .="<td>".(isset($error_num)?"<font color=red>$error_num</font>":"-")."</td></tr>";
			   		}
				}
				if ($error_code2_this)
					$error_code2=true;
				if ($error_code3_this)
					$error_code3=true;
				if ($error_code4_this)
					$error_code4=true;
				if ($error_code5_this)
					$error_code5=true;
				unset($error_num);
		   	}
           }
    
        }
        else
        {
            header("Location: import.php");
        }
}

include_once("../../../templates/adminheader_setting.php");
?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Service, 'index.php', $button_import, '') ?>
<?= displayTag("head_service_$intranet_session_language.gif", $msg) ?>


<form name="form1" method="GET" action="import_update.php">
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td>

<!--<?=($error?($format==1?$i_Service_Import_NoMatch_Entry1:$i_Service_Import_NoMatch_Entry2):$i_Payment_Import_Confirm)?>-->
<?
	if ($error==1)
	{
		if ($error_code5)
			$msg.="<font color=red>@</font> ".$i_Profile_Import_NoMatch_Entry[5]."<br />";
		if ($error_code4)
			$msg.="<font color=red>*</font> ".$i_Profile_Import_NoMatch_Entry[4]."<br />";
		if ($error_code1)
			$msg.="<font color=red>!</font> ".$i_Profile_Import_NoMatch_Entry[1]."<br />";
		if ($error_code2)
			$msg.="<font color=red>#</font> ".$i_Profile_Import_NoMatch_Entry[2]."<br />";
		if ($error_code3)
			$msg.="<font color=red>^</font> ".$i_Profile_Import_NoMatch_Entry[3]."<br />";
		#$msg=$i_Service_Import_NoMatch_Entry[$error_code];
		
	}
	else
		$msg=$i_Payment_Import_Confirm;
?>
<?=$msg?>
</td></tr></table><br>
<?php if (!$error_code1) { ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>

<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<?=$display?>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
<?php } ?>
<table width=560 cellspacing=0 cellpadding=0>
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?php if(!$error){?>
	<input type="image" alt="<?=$button_confirm?>" src="/images/admin/button/s_btn_confirm_<?=$intranet_session_language?>.gif" border='0'>
<?php } ?>
<a href=import.php?clear=1><img alt="<?=$button_cancel?>" border=0 src="/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif" border='0'></a>
</td>
</tr>
</table>
<input type=hidden name=confirm value=1>
<input type=hidden name=format value=<?=$format?>>



</form>

<?
intranet_closedb();
include_once("../../../templates/adminfooter.php");
?>
