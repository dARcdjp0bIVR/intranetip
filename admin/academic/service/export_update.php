<?php

$PATH_WRT_ROOT = "../../../";

include($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libmerit.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

// get school information (name + badge)
$li = new libfilesystem();
$lexport = new libexporttext();

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = (trim($school_data[0])!="") ? "\"".$school_data[0]."\"\n\n" : "";
$utf_school_name = (trim($school_data[0])!="") ? $school_data[0]."\r\n\r\n" : "";


$lvlArray = array();
$lvlMerit = array();
$lvlMinorC = array();
$lvlMajorC = array();
$lvlSuperC = array();
$lvlUltraC = array();
$lvlBlack = array();
$lvlMinorD = array();
$lvlMajorD = array();
$lvlSuperD = array();
$lvlUltraD = array();

$lmerit = new libmerit();
$lword = new libwordtemplates();
$meritReasons = $lword->getWordListMerit();

$now = time();
//$now = mktime(0,0,0,4,1,2003);
$today = date('Y-m-d',$now);
$ts_weekstart = mktime(0,0,0,date('m',$now),date('d',$now)-date('w',$now),date('Y',$now));
$ts_weekend = mktime(0,0,-1,date('m',$ts_weekstart),date('d',$ts_weekstart)+7,date('Y',$ts_weekstart));
$ts_monthstart = mktime(0,0,0,date('m',$now),1,date('Y',$now));
$ts_monthend = mktime(0,0,-1,date('m',$now)+1,1,date('Y',$now));


$weekstart = date('Y-m-d',$ts_weekstart);
$weekend = date('Y-m-d',$ts_weekend);
$monthstart = date('Y-m-d',$ts_monthstart);
$monthend = date('Y-m-d',$ts_monthend);
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));


if ($start == "" || $end == "")
{
    $start = $today;
    $end = $today;
}

$result = $lmerit->getClassMeritList($start,$end,$reason);

$x_header = "";
for ($i=0; $i<sizeof($Fields); $i++)
{
	switch ($Fields[$i]) {
		case "merit": 
			$x_header .= ",\"".$i_Merit_Short_Merit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_Merit; 
			$f_merit=true; 
			break;
		case "minorC": 
			$x_header .= ",\"".$i_Merit_Short_MinorCredit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_MinorCredit; 
			$f_minorC=true; 
			break;
		case "majorC": 
			$x_header .= ",\"".$i_Merit_Short_MajorCredit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_MajorCredit; 
			$f_majorC=true; 
			break;
		case "superC": 
			$x_header .= ",\"".$i_Merit_Short_SuperCredit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_SuperCredit; 
			$f_superC=true; 
			break;
		case "ultraC": 
			$x_header .= ",\"".$i_Merit_Short_UltraCredit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_UltraCredit; 
			$f_ultraC=true; 
			break;
		case "black": 
			$x_header .= ",\"".$i_Merit_Short_BlackMark."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_BlackMark; 
			$f_black=true; 
			break;
		case "minorD": 
			$x_header .= ",\"".$i_Merit_Short_MinorDemerit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_MinorDemerit; 
			$f_minorD=true; 
			break;
		case "majorD": 
			$x_header .= ",\"".$i_Merit_Short_MajorDemerit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_MajorDemerit; 
			$f_majorD=true; 
			break;
		case "superD": 
			$x_header .= ",\"".$i_Merit_Short_SuperDemerit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_SuperDemerit; 
			$f_superD=true; 
			break;
		case "ultraD": 
			$x_header .= ",\"".$i_Merit_Short_UltraDemerit."\""; 
			$utf_x_header .= "\t".$i_Merit_Short_UltraDemerit; 
			$f_ultraD=true; 
			break;
	}
}
$x = "\"".$i_UserClassName."\"".$x_header."\n";
$utf_x = $i_UserClassName.$utf_x_header."\r\n";

$en_reason = urlencode($reason);

for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$lvlID,$merit,$minorC,$majorC,$superC,$ultraC,$black,$minorD,$majorD,$superD,$ultraD) = $result[$i];
     if (!in_array($lvlID,$lvlArray))
     {
          $lvlArray[] = $lvlID;
     }
     for ($j=0; $j<sizeof($lvlArray); $j++)
     {
          if ($lvlArray[$j]==$lvlID)
          {
              $lvlMerit[$j] += $merit;
              $lvlMinorC[$j] += $minorC;
              $lvlMajorC[$j] += $majorC;
              $lvlSuperC[$j] += $superC;
              $lvlUltraC[$j] += $ultraC;
              $lvlBlack[$j] += $black;
              $lvlMinorD[$j] += $minorD;
              $lvlMajorD[$j] += $majorD;
              $lvlSuperD[$j] += $superD;
              $lvlUltraD[$j] += $ultraD;
              break;
          }
     }
	 $x .= "\"".$name."\"";
	 $x .= ($f_merit) ? ",".$merit : "";
	 $x .= ($f_minorC) ? ",".$minorC : "";
	 $x .= ($f_majorC) ? ",".$majorC : "";
	 $x .= ($f_superC) ? ",".$superC : "";
	 $x .= ($f_ultraC) ? ",".$ultraC : "";
	 $x .= ($f_black) ? ",".$black : "";
	 $x .= ($f_minorD) ? ",".$minorD : "";
	 $x .= ($f_majorD) ? ",".$majorD : "";
	 $x .= ($f_superD) ? ",".$superD : "";
	 $x .= ($f_ultraD) ? ",".$ultraD : "";
	 $x .= "\n";
	 
	 $utf_x .= $name;
	 $utf_x .= ($f_merit) ? "\t".$merit : "";
	 $utf_x .= ($f_minorC) ? "\t".$minorC : "";
	 $utf_x .= ($f_majorC) ? "\t".$majorC : "";
	 $utf_x .= ($f_superC) ? "\t".$superC : "";
	 $utf_x .= ($f_ultraC) ? "\t".$ultraC : "";
	 $utf_x .= ($f_black) ? "\t".$black : "";
	 $utf_x .= ($f_minorD) ? "\t".$minorD : "";
	 $utf_x .= ($f_majorD) ? "\t".$majorD : "";
	 $utf_x .= ($f_superD) ? "\t".$superD : "";
	 $utf_x .= ($f_ultraD) ? "\t".$ultraD : "";
	 $utf_x .= "\r\n";
}


$lvls = $lmerit->getLevelList();
$y = "\"".$i_UserClassLevel."\"".$x_header."\n";
$utf_y = $i_UserClassLevel.$utf_x_header."\r\n";

for ($i=0; $i<sizeof($lvlArray); $i++)
{
     $id = $lvlArray[$i];
     $name = $lvls[$id];
     $merit = $lvlMerit[$i];
     $minorC = $lvlMinorC[$i];
     $majorC = $lvlMajorC[$i];
     $superC = $lvlSuperC[$i];
     $ultraC = $lvlUltraC[$i];
     $black = $lvlBlack[$i];
     $minorD = $lvlMinorD[$i];
     $majorD = $lvlMajorD[$i];
     $superD = $lvlSuperD[$i];
     $ultraD = $lvlUltraD[$i];

	 $y .= "\"".$name."\"";
	 $y .= ($f_merit) ? ",".$merit : "";
	 $y .= ($f_minorC) ? ",".$minorC : "";
	 $y .= ($f_majorC) ? ",".$majorC : "";
	 $y .= ($f_superC) ? ",".$superC : "";
	 $y .= ($f_ultraC) ? ",".$ultraC : "";
	 $y .= ($f_black) ? ",".$black : "";
	 $y .= ($f_minorD) ? ",".$minorD : "";
	 $y .= ($f_majorD) ? ",".$majorD : "";
	 $y .= ($f_superD) ? ",".$superD : "";
	 $y .= ($f_ultraD) ? ",".$ultraD : "";
	 $y .= "\n";
	 
	 $utf_y .= $name;
	 $utf_y .= ($f_merit) ? "\t".$merit : "";
	 $utf_y .= ($f_minorC) ? "\t".$minorC : "";
	 $utf_y .= ($f_majorC) ? "\t".$majorC : "";
	 $utf_y .= ($f_superC) ? "\t".$superC : "";
	 $utf_y .= ($f_ultraC) ? "\t".$ultraC : "";
	 $utf_y .= ($f_black) ? "\t".$black : "";
	 $utf_y .= ($f_minorD) ? "\t".$minorD : "";
	 $utf_y .= ($f_majorD) ? "\t".$majorD : "";
	 $utf_y .= ($f_superD) ? "\t".$superD : "";
	 $utf_y .= ($f_ultraD) ? "\t".$ultraD : "";
	 $utf_y .= "\r\n";
}

$functionbar = "\"$i_Attendance_Date:\",\"$i_Profile_From $start $i_Profile_To $end\"\n";
$utf_functionbar = "$i_Attendance_Date: \t$i_Profile_From $start $i_Profile_To $end\r\n";
$reasonSelect = ($reason!="") ? $reason : $i_status_all;
$functionbar .= "\"$i_Attendance_Reason:\",\"$reasonSelect\"\n\n\n";
$utf_functionbar .= "$i_Attendance_Reason: \t$reasonSelect\r\n\r\n\r\n";

/*
if (!$g_encoding_unicode) {
	$url = "/file/export/eclass-user-".session_id()."-".time().".csv";
	$lo = new libfilesystem();
	$sExcel = $school_name.$functionbar.$x."\n\n".$y;
	$lo->file_write($sExcel, $intranet_root.$url);
	
	intranet_closedb();
	header("Location: $url");
} else {	
	*/
	$filename = "eclass-user-".session_id()."-".time().".csv";
	$utf_sExcel = $utf_school_name.$utf_functionbar.$utf_x."\r\n\r\n".$utf_y;	
	$lexport->EXPORT_FILE($filename, $utf_sExcel);	
	intranet_closedb();
//}


?>