<?php
/*****************************************
 * Date:	2013-04-24 (YatWoon)
 *			Fixed: "role" data is overwided by session "role" data [Case#2013-0423-1113-49071]
 *****************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();
$lu = new libuser($studentid);
$semesterSelect = getSelectSemester("name=semester");
$lword = new libwordtemplates();
$servicelist = $lword->getSelectServiceName("onChange=\"this.form.service.value=this.value\"");
$rolelist = $lword->getSelectServiceRole("onChange=\"this.form.service_role.value=this.value\"");
$performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");
$name = $lu->UserNameLang();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$select_academicYear = getSelectAcademicYear("AcademicYearID", "onChange=\"js_Reload_Term_Selection()\"",1,"",$AcademicYearID);

?>
<script type="text/javascript" src="../../../templates/jquery/jquery-1.3.2.min.js"></script>

<script language="javascript">
function checkform(obj){
         if (obj.serviceDate.value != "")
         {
             if(!check_date(obj.serviceDate, "<?php echo $i_invalid_date; ?>.")) return false;
         }
         if(!check_text(obj.year, "<?php echo $i_alert_pleasefillin.$i_ServiceYear; ?>.")) return false;
         if(!check_text(obj.service, "<?php echo $i_alert_pleasefillin.$i_ServiceName; ?>.")) return false;

         return true;
}

function js_Reload_Term_Selection()
{
	AcademicYearID = document.form1.AcademicYearID.value;
	
	$('#semester_div').load(
		'../reload_semester.php', 
		{
			AcademicYearID: AcademicYearID
		}
	); 
}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Service, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_service_$intranet_session_language.gif", $msg) ?>


<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=right><?php echo $i_ServiceYear; ?>:</td><td><?=$select_academicYear?></td></tr>
<tr><td align=right><?php echo $i_ServiceSemester; ?>:</td><td><div id="semester_div"><?=$semesterSelect?></div></td></tr>
<tr><td align=right><?php echo $i_ServiceDate; ?>:</td><td><input type=text size=10 maxlength=10 name=serviceDate> <span class=extraInfo>(yyyy-mm-dd)<br> <?=$i_ServiceDateCanBeIgnored?></span></td></tr>
<tr><td align=right><?php echo $i_ServiceName; ?>:</td><td><input type=text size=30 name=service><?=$servicelist?></td></tr>
<tr><td align=right><?php echo $i_ServiceRole; ?>:</td><td><input type=text size=30 name=service_role><?=$rolelist?></td></tr>
<tr><td align=right><?php echo $i_ServicePerformance; ?>:</td><td><input type=text size=30 name=performance><?=$performancelist?></td></tr>
<tr><td align=right><?php echo $i_ServiceOrganization; ?>:</td><td><input type=text name=organization></td></tr>
<tr><td align=right><?php echo $i_ServiceRemark; ?>:</td><td><TEXTAREA name=remark cols=50 rows=5></TEXTAREA></td></tr>
</table>
</blockquote>
<input type=hidden name=studentid value=<?=$studentid?>>
<input type=hidden name=classid value=<?=$classid?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
<?= btnSubmit() ." ". btnReset() ?>
<a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<script language="javascript">
<!--
js_Reload_Term_Selection();
//-->
</script>
<?php
include_once("../../../templates/adminfooter.php");
?>
