<?php
//using: yat
/*****************************************
 * Date:	2013-04-24 (YatWoon)
 *			Fixed: "role" data is overwided by session "role" data [Case#2013-0423-1113-49071]
 * Date: 	2013-03-15 (Rita)
 * Details: amend redirection
 *****************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libdb();

// $year = intranet_htmlspecialchars(trim($year));
$serviceDate = intranet_htmlspecialchars(trim($serviceDate));
$service = intranet_htmlspecialchars(trim($service));
$service_role = intranet_htmlspecialchars(trim($service_role));
$performance = intranet_htmlspecialchars(trim($performance));
$organization = intranet_htmlspecialchars(trim($organization));
$remark = intranet_htmlspecialchars(trim($remark));
$serviceDate = ($serviceDate==""? "NULL":"'$serviceDate'");

$academic_year = new academic_year($AcademicYearID);
$year = $academic_year->YearNameEN;
$academic_year_term = new academic_year_term($YearTermID);
$semester = $academic_year_term->YearTermNameEN;

$update_fields = "";
$update_fields = "Year = '$year'";
$update_fields .= ",Semester = '$semester'";
$update_fields .= ",ServiceDate = $serviceDate";
$update_fields .= ",ServiceName = '$service'";
$update_fields .= ",Role = '$service_role'";
$update_fields .= ",Performance = '$performance'";
$update_fields .= ",Organization = '$organization'";
$update_fields .= ",Remark = '$remark'";
$update_fields .= ",DateModified = now()";

$sql = "UPDATE PROFILE_STUDENT_SERVICE SET $update_fields WHERE StudentServiceID = $StudentServiceID";
$li->db_db_query($sql);

intranet_closedb();
if ($page_from){
	header("Location: $page_from?msg=2&yrfilter=$yrfilter&class=$class&ServiceName=$ServiceName&role=$role&performance=$filterPerformance&datetype=$datetype&date_from=$date_from&date_to=$date_to");
}else{
	header("Location: studentview.php?studentid=$studentid&msg=2&classid=$classid");
}
?>
