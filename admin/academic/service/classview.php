<?php
###################################
#	Date:	2013-12-19	YatWoon
#			Fixed: incorrect classname in navigation title [Case#B56879]
###################################
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libclass.php");
include_once("../../../includes/libservice.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lservice = new libservice();
if ($classid == "")
{
	$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
    $sql = "SELECT 
    			YearClassID,
    			ClassTitleEN 
    		FROM 
    			YEAR_CLASS 
    		ORDER BY 
    			Sequence, ClassTitleEN";
    $result = $lservice->returnArray($sql,2);
    $classid = $result[0][0];
}
$class_name = $lservice->getClassName($classid);
$result = $lservice->getServiceListByClass($class_name);
$x = "<table width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>
<tr>
<td width=20% class=tableTitle>$i_UserClassNumber</td>
<td width=60% class=tableTitle>$i_UserEnglishName</td>
<td width=20% class=tableTitle>$i_Service_Count</td>
</tr>
";
for ($i=0; $i<sizeof($result); $i++)
{
     list($id,$name,$classnumber,$count) = $result[$i];
     $link = "<a class=functionlink href=studentview.php?studentid=$id&classid=$classid>$name</a>";
     $css = ($i%2) ? "2" : "";
         $x .= "<tr>
             <td class=tableContent$css>$classnumber&nbsp;</td>
             <td class=tableContent$css>$link</td>
             <td class=tableContent$css>$count</td>
            </tr>\n";
}
$x .= "</table>\n";

$classSelect = $lservice->getSelectClassID("name=classid onChange=\"this.form.submit()\"",$classid);
$functionbar = "$i_Profile_SelectClass $classSelect";

?>
<SCRIPT LANGUAGE=JAVASCRIPT>
</SCRIPT>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Service, '', $class_name, '') ?>
<?= displayTag("head_service_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $functionbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu>
<a class=iconLink href="import.php"><?=importIcon().$button_import?></a>
<a class=iconLink href="detail.php"><?=detailIcon().$i_general_ViewDetailRecords?></a>
</td></tr>
<tr><td class=tableContent>
<?=$x?>
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");
?>