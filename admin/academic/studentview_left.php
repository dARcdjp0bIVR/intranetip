<?php
// Modifing by 

######### Change Log [Start]
#   Date    :   2019-04-26 AnnA   
#               Added IntegerSafe for studentID
#
#	Date	:	2016-07-20 	Bill
#				Replace deprecated split() by explode() for PHP 5.4
#
#	Date	:	2013-10-09	YatWoon
#				Improved: Display student without classes as well [Case#2013-1009-0907-30156]
#				Improved: Combine NULL and empty classname
#				Improved: Sort by EnglishName
#
#	Date	:	2010-06-17 YatWoon
#				Fixed: tyop of $year_access, changed to $year_assess (CRM Ref No.: 2010-0617-1032)
#
######### Change Log [End]

include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libuser.php");
include_once("../../includes/libclass.php");
include_once("../../includes/libattendance.php");
include_once("../../includes/libmerit.php");
include_once("../../includes/libservice.php");
include_once("../../includes/libactivity.php");
include_once("../../includes/libaward.php");
include_once("../../includes/libassessment.php");
include_once("../../includes/libstudentfiles.php");
include_once("../../includes/libstudentprofile.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
// $li_menu from adminheader_intranet.php
intranet_opendb();

$lclass = new libclass();
$lu = new libuser();
$lstudentprofile = new libstudentprofile();
$case = 0;
$general_select_year = "<SELECT name=generalYear>\n";
$general_select_year .= "<OPTION value=0>$i_SettingsCurrentAcademicYear</OPTION>\n";
$general_select_year .= "<OPTION value=1>$i_status_all</OPTION>\n";
$general_select_year .= "</SELECT>\n";

//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$semester_data = explode("\n",get_file_content("$intranet_root/file/semester.txt"));
for ($i=0; $i<sizeof($semester_data); $i++)
{
     if ($semester_data[$i] != "")
     {
         //$linedata = split("::",$semester_data[$i]);
         $linedata = explode("::",$semester_data[$i]);
     }

     if ($linedata[1]==1)
     {
         $currentSemester = $linedata[0];
     }
     $generalSemester[] = $linedata[0];
}
if (!isset($generalSem)) $generalSem = $currentSemester;
$general_select_sem = getSelectByValue($generalSemester,"name=generalSem",$generalSem,1);
$select_sem = getSelectByValue($generalSemester,"name=generalSem onChange=this.form.submit()",$generalSem,1);
$StudentID = IntegerSafe($_GET['StudentID']);
if ($class !="")
{
	$class_con = " ClassName = '$class'"; 
}
else
{
	$class_con = " (ClassName is NULL or ClassName = '') ";
}
    # Get Students in Left Status of class $class
    $namefield = getNameFieldWithClassNumberByLang();
    //$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordStatus = 3 AND RecordType = 2 AND ClassName = '$class'";
    $sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordStatus = 3 AND RecordType = 2 AND ". $class_con;
    $sql .= " order by EnglishName";
    $temp_students = $lclass->returnArray($sql,2);
    $select_students = getSelectByArray($temp_students,"name=StudentID onChange=this.form.submit()",$StudentID);
    $student_selection = "<tr><td>$i_Profile_SelectStudent:</td><td>$select_students</td></tr>";
    $additional_js = "this.form.StudentID.selectedIndex=0;";

# Get Classes
//$sql = "SELECT DISTINCT ClassName FROM INTRANET_USER WHERE RecordStatus = 3 AND RecordType = 2 ORDER BY ClassName";
$sql = "SELECT DISTINCT IFNULL(ClassName, '') FROM INTRANET_USER WHERE RecordStatus = 3 AND RecordType = 2 ORDER BY ClassName";
$temp_classes = $lclass->returnVector($sql);
$select_classes = getSelectByValue($temp_classes, "name=class onChange=this.form.submit()",$class);
$class_selection = "<tr><td>$i_Profile_SelectClass</td><td>$select_classes</td></tr>";
if ($StudentID == "" )
{
    $toolbar = "<table border=0 width=100%>\n";
    $toolbar .= "<tr><td>&nbsp;</td><td align=right class=admin_bg_menu>$general_select_year $general_select_sem<a class=iconLink href=\"javascript:openPrintAllPage()\">".printIcon()."$i_PrinterFriendlyPage</a></td></tr>\n";
}
else
{
    $toolbar = "<table border=0>\n";
}
$toolbar .= "$class_selection\n $student_selection\n";
$toolbar .= "</table>\n";

if ($StudentID != "")
{

$now = time();
$yearstart = date('Y-m-d',getStartOfAcademicYear($now));
$yearend = date('Y-m-d',getEndOfAcademicYear($now));
$currentYear = getCurrentAcademicYear();

$lattend = new libattendance();
$lmerit = new libmerit();
$lact = new libactivity();
$lservice = new libservice();
$laward = new libaward();
$lassess = new libassessment();
$lfiles = new libstudentfiles();

$year_attend = $lattend->returnYears($StudentID);
$year_merit = $lmerit->returnYears($StudentID);
$year_act = $lact->returnYears($StudentID);
$year_service = $lservice->returnYears($StudentID);
$year_award = $laward->returnYears($StudentID);
$year_assess = $lassess->returnYears($StudentID);
$year_files = $lfiles->returnYears($StudentID);

$year_array = array_merge($year_attend,$year_merit,$year_act,$year_service,$year_award,$year_assess,$year_files);
# Modified by Key (2008-09-26): left student are no need to add current year 
//$year_array[] = $currentYear;
$year_array = array_unique($year_array);
rsort($year_array);

$year_array = array_values($year_array);

##### global variable called by : 
#	- libattendance.php
#	- libmerit.php
$all_distinct_years = $year_array;

if (!isset($year))
{
	# Modified by Key (2008-09-26) : use the latest history year
    // $year = $currentYear;
    $year = $year_array[0];
}
$select_year = getSelectByValue($year_array,"name=year onChange=this.form.submit()",$year,1);

}

?>
<SCRIPT LANGUAGE=Javascript>
function viewAttend(id)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function viewMerit(id)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id,1);
}
function openPrintPage()
{
         newWindow("studentprint.php?StudentID=<?=$StudentID?>&class=<?=$class?>&year=<?=$year?>&sem=<?=$generalSem?>&comeFrom=left",4);
}
function openPrintAllPage()
{
         newWindow("studentallprint.php?left=1&class=<?=$class?>&generalYear="+document.form1.generalYear.value+"&generalSem="+document.form1.generalSem.value,4);
}
function viewFormAns(id)
{
         newWindow('/admin/academic/assessment/view.php?AssessmentID='+id,1);
}
function viewAttendByYear(id,year)
{
         newWindow("/admin/academic/view_attendance.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewMeritByYear(id, year)
{
         newWindow("/admin/academic/view_merit.php?StudentID=<?=$StudentID?>&class=<?=$class?>&type="+id+"&year="+year,1);
}
function viewFile(id)
{
         newWindow("/admin/academic/view_file.php?FileID="+id,11);
}
</SCRIPT>

<form name="form1" action="">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, 'index.php', $i_Profile_OverallStudentViewLeft, '') ?>
<?= displayTag("head_studentview_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $toolbar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<?php if ($StudentID != "") { ?>
<tr><td class=admin_bg_menu align=right><?=$select_year?> <?=$select_sem?><a class=iconLink href="javascript:openPrintPage()"><?=printIcon().$i_PrinterFriendlyPage?></a>
</td></tr>
<?php } ?>
<tr><td class=tableContent height=300 align=left>
<?php if ($StudentID != "") { ?>
<br>
<? if ($year == "") { ?>
<?=$i_Profile_ClassHistory?>
<?=$lclass->displayClassHistoryAdmin($StudentID)?>
<br>
<? } ?>
<? if ($li_menu->is_access_attendance) { ?>
<?=$i_Profile_Attendance?>
<?=$lattend->displayStudentRecordByYearAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_merit) {
?>
<?=$i_Profile_Merit?>
<?=$lmerit->displayStudentRecordByYearAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_service) {
?>
<?=$i_Profile_Service?>
<?=$lservice->displayServiceAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_activity) {
?>
<?=$i_Profile_Activity?>
<?=$lact->displayActivityAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_award) {
?>
<?=$i_Profile_Award?>
<?=$laward->displayAwardAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_assessment) {
?>
<?=$i_Profile_Assessment?>
<?=$lassess->displayAssessmentAdmin($StudentID,$year,$generalSem)?>
<br>
<? }
   if ($li_menu->is_access_student_files) {
?>
<?=$i_Profile_Files?>
<?=$lfiles->displayFileListAdmin($StudentID,$year,$generalSem)?>
<? } ?>
<br>
<br>
<?php } ?>
</td></tr>
</table>

<?
include_once("../../templates/adminfooter.php");
?>