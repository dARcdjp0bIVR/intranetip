<?php
// Using: 

#################################################
#
#   Date:   2018-02-27 (Bill)   [2018-0227-1029-29207]
#           Fixed: Update Activity Record Type (Internal / External)
#
#	Date:   2017-08-15 (Simon)
#			Update sql to get the targetYearClassName and targetYearClassNumber by select the TEMP_ACTIVITY_IMPORT table
#
# 	Date:	2015-12-07 (Bill)	[2015-1008-1356-44164]
#			Improved: added field Activity Date
#
#	Date:	2013-07-05	YatWoon
#			Improved: no need to check student status [Case#2013-0704-1159-08073]
#
#################################################

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../includes/libpayment.php");
include_once("../../../lang/lang.$intranet_session_language.php");

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();

$filepath = $userfile;
$filename = $userfile_name;
		
// if($format==1)
// {
// 	$fields_name = "UserID, Year, Semester, ActivityName,Role, Performance,Organization,Remark, DateInput, DateModified,ClassName,ClassNumber,ActivityDate";
// 	if ($special_feature['activity_internal_external'])
// 	{
// 		$fields_name .= ",RecordType";
// 	}
// 	$fields_value = "a.UserID, b.Year,b.Semester, b.ActivityName, b.Role,b.Performance,b.Organization,b.Remark,now(),now(), a.ClassName,a.ClassNumber,b.ActivityDate";
// 	if ($special_feature['activity_internal_external'])
// 	{
// 		$fields_value .= ",b.RecordType";
// 	}
	
// 	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_ACTIVITY ($fields_name)
//     SELECT $fields_value
//     FROM INTRANET_USER as a, TEMP_ACTIVITY_IMPORT as b
//     WHERE a.ClassName = b.ClassName AND a.ClassNumber = b.ClassNumber AND a.RecordType = 2
//     ";
// }
// if($format==2){
	
// 	$fields_name = "UserID, Year, Semester, ActivityName,Role, Performance,Organization,Remark, DateInput, DateModified,ClassName,ClassNumber,ActivityDate";
// 	if ($special_feature['activity_internal_external'])
// 	{
// 		$fields_name .= ",RecordType";
// 	}
// 	$fields_value = "a.UserID, b.Year,b.Semester, b.ActivityName, b.Role,b.Performance,b.Organization,b.Remark,now(),now(), a.ClassName,a.ClassNumber,b.ActivityDate";
// 	if ($special_feature['activity_internal_external'])
// 	{
// 		$fields_value .= ",b.RecordType";
// 	}
// 	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_ACTIVITY ($fields_name)
//     SELECT $fields_value
//     FROM INTRANET_USER as a, TEMP_ACTIVITY_IMPORT as b
//     WHERE a.websamsregno = b.websamsregno AND a.RecordType = 2
//     ";
// }

if($format==1)
{
    $fields_name = "UserID, Year, Semester, ActivityName, Role, Performance, Organization, Remark, DateInput, DateModified, ClassName, ClassNumber, ActivityDate";
	if ($special_feature['activity_internal_external'])
	{
		$fields_name .= ", RecordType";
	}
	$fields_value = "UserID, Year, Semester, ActivityName, Role, Performance, Organization, Remark, now(), now(), targetYearClassName, targetYearClassNumber, ActivityDate";
	if ($special_feature['activity_internal_external'])
	{
		$fields_value .= ", RecordType";
	}
	
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_ACTIVITY ($fields_name) SELECT ".$fields_value." FROM TEMP_ACTIVITY_IMPORT";
}

if($format==2)
{
    $fields_name = "UserID, Year, Semester, ActivityName, Role, Performance, Organization, Remark, DateInput, DateModified, ClassName, ClassNumber, ActivityDate";
	if ($special_feature['activity_internal_external'])
	{
		$fields_name .= ",RecordType";
	}
	$fields_value = "UserID, Year, Semester, ActivityName, Role, Performance, Organization, Remark, now(), now(), targetYearClassName, targetYearClassNumber, ActivityDate";
	if ($special_feature['activity_internal_external'])
	{
	    $fields_value .= ", RecordType";
	}
	
	$sql = "INSERT IGNORE INTO PROFILE_STUDENT_ACTIVITY ($fields_name) SELECT ".$fields_value." FROM TEMP_ACTIVITY_IMPORT";
}

$li->db_db_query($sql);

header("Location: index.php?msg=10");

intranet_closedb();
?>