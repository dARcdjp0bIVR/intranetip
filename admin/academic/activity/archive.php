<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$current_year = getCurrentAcademicYear();
$current_sem = getCurrentSemester();

?>

<form name="form1" method="get">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Activity, 'index.php', $i_ActivityArchive, '') ?>
<?= displayTag("head_activity_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td class=tableContent>
<blockquote>
<p><?=$i_ActivityArchiveDescription?><br>
<?="$i_ActivityYear : <font color=red>$current_year</font>"?><br>
<?="$i_ActivitySemester : <font color=red>$current_sem</font>"?><br>
<p><?=$i_ActivityArchiveWrongData?>
</blockquote>
</td></tr>
</table>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
 <a href="archive_perform.php"><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0'></a>
 <a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>
<?
include_once("../../../templates/adminfooter.php");

?>