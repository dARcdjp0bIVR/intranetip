<?php
// using: 
/*******************************************
#
# 	Date:	2015-12-07 (Bill)	[2015-1008-1356-44164]
#			Improved: added field Activity Date
#
#	Date:	2013-12-04	YatWoon
#			Improved: enforce $_POST for retreive data [Case#2013-1126-1605-00071]
#
#	Date:	2013-09-13	YatWoon
#			Fixed: update pass filterRole intead of role [Case#2013-0911-1531-08073]

 * Date: 	2013-03-14 Rita
 * Details:	amend redirect path, add filters' variables
 *******************************************/
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libdb();

// $year = intranet_htmlspecialchars(trim($year));
$activity = intranet_htmlspecialchars(trim($_POST['activity']));
$role = intranet_htmlspecialchars(trim($_POST['role']));
$performance = intranet_htmlspecialchars(trim($_POST['performance']));
$organization = intranet_htmlspecialchars(trim($_POST['organization']));
$remark = intranet_htmlspecialchars(trim($_POST['remark']));

$academic_year = new academic_year($AcademicYearID);
$year = $academic_year->YearNameEN;
$academic_year_term = new academic_year_term($YearTermID);
$semester = $academic_year_term->YearTermNameEN;

// [2015-1008-1356-44164]
$activityDate = intranet_htmlspecialchars(trim($activityDate));
$activityDate = ($activityDate==""? "NULL":"'$activityDate'");

$update_fields = "";
$update_fields = "Year = '$year'";
$update_fields .= ",Semester = '$semester'";
$update_fields .= ",ActivityName = '$activity'";
$update_fields .= ",Role = '$role'";
$update_fields .= ",Performance = '$performance'";
$update_fields .= ",Organization = '$organization'";
$update_fields .= ",Remark = '$remark'";
$update_fields .= ",DateModified = now()";
// [2015-1008-1356-44164]
$update_fields .= ",ActivityDate = $activityDate";

if ($special_feature['activity_internal_external']) 
{
	$recordType = ($type=="internal")? 1 : 2;
	$update_fields .= ",RecordType = '$recordType'";
}

$sql = "UPDATE PROFILE_STUDENT_ACTIVITY SET $update_fields WHERE StudentActivityID = $StudentActivityID";
$li->db_db_query($sql);

intranet_closedb();
if ($page_from){
	header("Location: $page_from?msg=2&yrfilter=$yrfilter&class=$class&ActivityName=$ActivityName&role=$filterRole&datetype=$datetype&date_from=$date_from&date_to=$date_to&performance=$filterPerformance");
}
else{
	header("Location: studentview.php?studentid=$studentid&msg=2&classid=$classid");
}
?>
