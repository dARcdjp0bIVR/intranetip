<?php
// Using: 

/*****************************************
 *  Date:   2018-03-16 (Bill)   [DM#3383]
 *      - Improved: added checking for input date not in selected school year
 * 	Date:	2015-12-07 (Bill)	[2015-1008-1356-44164]
 *		- Improved: added field Activity Date
 *****************************************/

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/libfilesystem.php");
include_once("../../../includes/libwordtemplates.php");
include_once("../../../includes/libaccount.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../templates/adminheader_intranet.php");

intranet_opendb();

$lu = new libuser($studentid);
$name = $lu->UserNameLang();

$semesterSelect = getSelectSemester("name=semester");

$lword = new libwordtemplates();
$actlist = $lword->getSelectActivityName("onChange=\"this.form.activity.value=this.value\"");
$performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$select_academicYear = getSelectAcademicYear("AcademicYearID", "onChange=\"js_Reload_Term_Selection()\"",1,"",$AcademicYearID);
?>

<script type="text/javascript" src="../../../templates/jquery/jquery-1.3.2.min.js"></script>
<script language="javascript">
function checkform(obj)
{
	event.preventDefault();
	var allowSubmit = false;
	
	var activityDate = obj.activityDate.value;
	if (activityDate != "")
    {
    	if(check_date(obj.activityDate, "<?php echo $i_invalid_date; ?>."))
    	{
        	var academicYearId = obj.AcademicYearID.value;
    		$.ajax({
    			type: "POST",
    			url: "../ajax_check.php", 
    			data:
    			{
    				academicYearID : academicYearId,
    				checkYearDate : activityDate
    			},
    			success: function(result)
    			{
    				if(result == '1')
    				{
    					obj.submit();
    				}
    				else
    				{
        				alert("<?php echo $Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSelectedSchoolYear']; ?>.");
    				}
    			}
    		});
    	}
    }
	else
	{
		obj.submit();
	}
}

function js_Reload_Term_Selection()
{
	var AcademicYearID = document.form1.AcademicYearID.value;
	$('#semester_div').load(
		'../reload_semester.php', 
		{
			AcademicYearID: AcademicYearID
		}
	); 
}
</script>

<form name="form1" action="new_update.php" method="post" onSubmit="return checkform(this);">
<?= displayNavTitle($i_adminmenu_adm, '', $i_adminmenu_adm_academic_record, '/admin/academic/', $i_Profile_Activity, 'index.php', $lu->ClassName, "classview.php?classid=$classid", $name, 'javascript:history.back()', $button_new, '') ?>
<?= displayTag("head_activity_$intranet_session_language.gif", $msg) ?>

<blockquote>
<table width=500 border=0 cellpadding=4 cellspacing=0>
<tr><td align=right <?=($intranet_session_language=="en"?"":"nowrap")?>><?php echo $i_UserLogin; ?>:</td><td><?=$lu->UserLogin?></td></tr>
<tr><td align=right><?php echo $i_UserEnglishName; ?>:</td><td><?=$lu->EnglishName?></td></tr>
<tr><td align=right><?php echo $i_UserChineseName; ?>:</td><td><?=$lu->ChineseName?></td></tr>
<tr><td align=right><?php echo $i_UserClassName; ?>:</td><td><?=$lu->ClassName?></td></tr>
<tr><td align=right><?php echo $i_UserClassNumber; ?>:</td><td><?=$lu->ClassNumber?></td></tr>
<tr><td align=right><?php echo $i_ActivityYear; ?>:</td><td><?=$select_academicYear?></td></tr>
<tr><td align=right><?php echo $i_ActivitySemester; ?>:</td><td><div id="semester_div"><?=$semesterSelect?></div></td></tr>
<tr><td align=right><?php echo $Lang['AdminConsole']['Activity']['ActivityDate']; ?>:</td><td><input type=text size=10 maxlength=10 name=activityDate> <span class=extraInfo>(yyyy-mm-dd)<br> <?=$Lang['AdminConsole']['Activity']['ActivityDateCanBeIgnored']?></span></td></tr>
<tr><td align=right><?php echo $i_ActivityName; ?>:</td><td><input type=text size=30 name=activity><?=$actlist?></td></tr>
<tr><td align=right><?php echo $i_ActivityRole; ?>:</td><td><input type=text size=30 name=role></td></tr>
<tr><td align=right><?php echo $i_ActivityPerformance; ?>:</td><td><input type=text size=30 name=performance><?=$performancelist?></td></tr>
<tr><td align=right><?php echo $i_ActivityOrganization; ?>:</td><td><input type=text name=organization></td></tr>
<tr><td align=right><?php echo $i_ActivityRemark; ?>:</td><td><TEXTAREA name=remark cols=50 rows=5></TEXTAREA></td></tr>

<? if ($special_feature['activity_internal_external']) { ?>
	<tr>
		<td align=right><?php echo $i_general_Type; ?>:</td>
		<td>
			<input type="radio" id="typeInternal" name="type" value="internal" checked><label for="typeInternal"><?=$i_ServiceMgmt_System_Hours_Type1_Name?></label>
			<?= toolBarSpacer() ?>
			<input type="radio" id="typeExternal" name="type" value="external"><label for="typeExternal"><?=$i_ServiceMgmt_System_Hours_Type2_Name?></label>
		</td>
	</tr>
<? } ?>

</table>
</blockquote>

<input type=hidden name=studentid value=<?=$studentid?>>
<input type=hidden name=classid value=<?=$classid?>>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><hr size=1></td></tr>
<tr><td align="right">
    <?= btnSubmit() ." ". btnReset() ?>
    <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
</form>

<script language="javascript">
<!--
js_Reload_Term_Selection();
//-->
</script>

<?php
include_once("../../../templates/adminfooter.php");
?>