<?php
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$year = getCurrentAcademicYear();
$sem = getCurrentSemester();

$ECA_groupCategory = 5;
$type_student = 2;

# Extract current records
$sql = "SELECT a.UserID, b.Title, c.Title, a.Performance,d.ClassName,d.ClassNumber
        FROM INTRANET_USERGROUP as a LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
             LEFT OUTER JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
             LEFT OUTER JOIN INTRANET_USER as d ON a.UserID = d.UserID
        WHERE b.RecordType = $ECA_groupCategory AND d.RecordType = $type_student";
$result = $li->returnArray($sql,6);
//$x = "<table>\n";
for ($i=0; $i<sizeof($result); $i++)
{
     list ($uid,$group,$role,$perform,$classname,$classnumber) = $result[$i];

     # Try to find matched record first
     $conds = "UserID = $uid AND ActivityName = '$group' AND Year = '$year' AND Semester = '$sem'";
     $sql = "SELECT StudentActivityID FROM PROFILE_STUDENT_ACTIVITY WHERE $conds";
     $temp = $li->returnVector($sql);
     $act_id = $temp[0]+0;

//     echo "$sql \n$act_id\n<br>";

     if ($act_id==0)     # No matched record
     {
         # Perform insert
         $sql = "INSERT INTO PROFILE_STUDENT_ACTIVITY (UserID,Year,Semester,ActivityName,Role,Performance,DateInput,DateModified,ClassName,ClassNumber)
                 VALUES ($uid,'$year','$sem','$group','$role','$perform',now(),now(),'$classname','$classnumber')";
//         echo "$sql\n";
         $li->db_db_query($sql);
     }
     else
     {
         # update
         //$act_id = $temp[0];
         $fields = "Role = '$role', Performance = '$perform'";
         $sql = "UPDATE PROFILE_STUDENT_ACTIVITY SET $fields WHERE StudentActivityID = $act_id";
//         echo "$sql\n";
         $li->db_db_query($sql);
     }

//     $x .= "<tr><td>$id</td><td>$group</td><td>$role</td><td>$perform</td></tr>\n";
}
//$x .= "</table>\n";

intranet_closedb();
header("Location: index.php?msg=2");
?>