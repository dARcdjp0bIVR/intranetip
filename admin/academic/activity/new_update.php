<?php
###################################
#
# 	Date:	2015-12-07 (Bill)	[2015-1008-1356-44164]
#			Improved: added field Activity Date
#
#	Date:	2013-12-04	YatWoon
#			Improved: enforce $_POST for retreive data [Case#2013-1126-1605-00071]
#
###################################
include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libuser.php");
include_once("../../../includes/form_class_manage.php");
intranet_opendb();

$li = new libuser($studentid);
$classname = $li->ClassName;
$classnumber = $li->ClassNumber;

// $year = intranet_htmlspecialchars(trim($year));
$activity = intranet_htmlspecialchars(trim($_POST['activity']));
$role = intranet_htmlspecialchars(trim($_POST['role']));
$performance = intranet_htmlspecialchars(trim($_POST['performance']));
$organization = intranet_htmlspecialchars(trim($_POST['organization']));
$remark = intranet_htmlspecialchars(trim($_POST['remark']));

$academic_year = new academic_year($AcademicYearID);
$year = $academic_year->YearNameEN;
$academic_year_term = new academic_year_term($YearTermID);
$semester = $academic_year_term->YearTermNameEN;

// [2015-1008-1356-44164]
$activityDate = intranet_htmlspecialchars(trim($activityDate));
$activityDate = ($activityDate==""? "NULL":"'$activityDate'");

$fieldname = "UserID, Year, Semester,ActivityName,Role,Performance,Organization,Remark, DateInput,DateModified,ClassName,ClassNumber,ActivityDate";
$fieldvalue = "'$studentid','$year','$semester','$activity','$role','$performance','$organization','$remark',now(),now(),'$classname','$classnumber',$activityDate";

if ($special_feature['activity_internal_external']) 
{
	$recordType = ($type=="internal")? 1 : 2;
	$fieldname .= ",RecordType";
	$fieldvalue .= ",'$recordType'";
}

$sql = "INSERT INTO PROFILE_STUDENT_ACTIVITY ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: studentview.php?studentid=$studentid&msg=1&classid=$classid");
?>
