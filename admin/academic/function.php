<?php
# Under Development : 20091208
## Using By : Max
##########################################################
## Modification Log
## 2009-12-08: Max (200912081142)
## -Change the "description of import file"
## -Compare import file with English only
##########################################################
function getAllAcademicYear_Term()
{
	global $intranet_db, $intranet_session_language;
	$academicYear_term = array();
	$li = new libdb();
	$sql = " select 
				y.AcademicYearID,y.YearNameEN as 'YearNameEN',y.YearNameB5  as 'YearNameB5', t.YearTermNameEN as 'YearTermNameEN', t.YearTermNameB5 as 'YearTermNameB5'
			from 
				".$intranet_db.".ACADEMIC_YEAR as y
			left join 
				".$intranet_db.".ACADEMIC_YEAR_TERM t
				on t.AcademicYearID = y.AcademicYearID
			";
	$resultArray = $li->returnArray($sql);
	$_totalRecord = sizeof($resultArray);
	for($i = 0 ;$i < $_totalRecord; $i++)
	{
//		$_year = ($intranet_session_language == "en" ? $resultArray[$i]["YearNameEN"] : $resultArray[$i]["YearNameB5"]);
//		$_term = ($intranet_session_language == "en" ? $resultArray[$i]["YearTermNameEN"] : $resultArray[$i]["YearTermNameB5"]);
		$_year = $resultArray[$i]["YearNameEN"];
		$_term = $resultArray[$i]["YearTermNameEN"];
		$academicYear_term[$_year][] = $_term;
	}
	return $academicYear_term;
	
}
function checkYearSemMapWithSystem($year,$sem,$SystemYearTermMapping)
{
	if (array_key_exists($year, $SystemYearTermMapping)) {
		if (in_array($sem, $SystemYearTermMapping[$year])) {
			//match with year and sem	
			return 1;
		} else {
			//Match Year but Sem not find
			return -1;
		}
	} else {
		//Year not Match
		return -2;
	}
}
?>
