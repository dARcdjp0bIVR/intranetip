<?php
# Modification Log:
#	**** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
#	Date:	2016-08-03 Ivan [ip.2.5.7.9.1]
#			added CompanyName for macau pui ching (but treat as general improvement)
#	**** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");
include_once("../../includes/libaccount.php");
include_once("../../includes/libclass.php");
include_once("../../lang/lang.$intranet_session_language.php");
include_once("../../templates/adminheader_intranet.php");
intranet_opendb();

if($ClassList!="")
{
	$ClassID = explode(",", $ClassList);
}

if($SID!="")
	$student_id = $SID;
else if(sizeof($StudentID)==1){
	$student_id = $StudentID[0];
}

$li = new libdb();

$namefield = getNameFieldWithClassNumberByLang();
$sql="SELECT $namefield FROM INTRANET_USER WHERE UserID = '$student_id' AND RecordType=2 AND RecordStatus IN (0,1,2)";
$temp = $li->returnVector($sql);
$student_name = $temp[0];

$sql="	SELECT  
		b.RecordID,
		b.ChName,
		b.EnName,
		b.Phone,
		b.EmPhone,
		b.Address Full_Address,
		b.Relation,
		b.IsMain,
		b.IsSMS,
		gse1.IsLiveTogether,
		gse1.IsEmergencyContact,
		gse1.Occupation,
		IFNULL(gse1.AreaCode, 'O' ) AreaCode,
		gse1.Road,
		gse1.Address,
		gse1.CompanyName
	FROM $eclass_db.GUARDIAN_STUDENT AS b 
	LEFT OUTER JOIN $eclass_db.GUARDIAN_STUDENT_EXT_1 AS gse1
		ON b.RecordID = gse1.RecordID
	WHERE b.UserID = '$student_id' ORDER By b.RecordID ";


$temp = $li->returnArray($sql,9);

$table_content="<table id='guardians' width=560 border=1 bordercolordark=#DBD6C4 bordercolorlight=#FCF7E5 cellpadding=2 cellspacing=0 align='center'>";
$table_content.="<tr>";

$table_content.="<td class='tableTitle' width=70%>&nbsp;</td>";
$table_content.="<Td class='tableTitle' width=18%>$i_StudentGuardian_MainContent</td>";

$table_content.="<Td class='tableTitle' >$i_StudentGuardian_EmergencyContact</td>";
if($plugin['sms'])
	$table_content.="<Td class='tableTitle'>$i_StudentGuardian_SMS</td>";
	
//if(sizeof($temp)>0)
	$table_content.="<td class='tableTitle'>$button_remove</td>";
	
$table_content.="</tr>";

if(sizeof($temp)>0){
	for($i=0;$i<sizeof($temp);$i++){
		//$add_area,$add_road,$add_address are for StudentRegistryPlugin only
		list($record_id,$ch_name,$en_name,$phone,$emphone,$address,$relation,$isMain,$isSMS,
			$isLiveTogether, $isEmergencyContact, $occupation,$add_area,$add_road,$add_address, $companyName)=$temp[$i];
		//$css = $i%2==0?"tableContent":"tableContent2";
		$css ="tableContent";
		$record_ids[]=$record_id;
		
		$select_relation ="<SELECT name='relation_$record_id'>";
		$select_relation.="<OPTION value=''>-- $button_select --</OPTION>";
		foreach($ec_guardian AS $key =>$relation_name)
			$select_relation.="<OPTION value='$key'".($key==$relation?" SELECTED ":"").">$relation_name</OPTION>\n";
		$select_relation.="</SELECT>\n";
		
		$table_content.="<tr id='r_$record_id'>";
		
		$table = "<table border=0 cellpadding=0 cellspacing=0 width=100%>";
		$table.="<tr><td align=right>$i_UserChineseName :&nbsp;</td><td ><input type=text name='chname_$record_id' value='$ch_name' size=10></td></tr>";
		$table.="<tr><td align=right>$i_UserEnglishName :&nbsp;</td><td ><input type=text name='enname_$record_id' value='$en_name' size=20></td></tr>";
		$table.="<tr><td align=right>$i_StudentGuardian_Occupation :&nbsp;</td><td ><input type=text name='occupation_$record_id' value='$occupation' size=20 maxlength=60></td></tr>";
		$table.="<tr><td align=right>".$Lang['AccountMgmt']['NameOfCompany/Organization']." :&nbsp;</td><td ><input type=text name='companyName_$record_id' value='$companyName' size=20 maxlength=255></td></tr>";
		$table.="<tr><td align=right>".$ec_iPortfolio['relation']." :&nbsp;</td><td>$select_relation</td></tr>";
		$table.="<tr><td align=right>$i_StudentGuardian_Phone :&nbsp;</td><td ><input type=text name='phone_$record_id' value='$phone' size=10></td></tr>";
		$table.="<tr><td align=right>$i_StudentGuardian_EMPhone :&nbsp;</td><td ><input type=text name='emphone_$record_id' value='$emphone' size=10></td></tr>";
		$table.="<tr><td align=right>$i_StudentGuardian_LiveTogether :&nbsp;</td><td ><input type=radio name='liveTogether_$record_id' value='1'".($isLiveTogether==1?" CHECKED ":"")." id='liveTogether_".$record_id."_1'/><label for='liveTogether_".$record_id."_1'>".$i_general_yes."</label> <input type=radio name='liveTogether_$record_id' value='0'".($isLiveTogether!=1?" CHECKED ":"")." id='liveTogether_".$record_id."_0'/><label for='liveTogether_".$record_id."_0'>".$i_general_no."</label></td></tr>";

		if($plugin['StudentRegistry']){
			$AreaCode_Select = "<select name='add_area_$record_id'>";
			if(!empty($i_StudentRegistry_AreaCode)){
				foreach($i_StudentRegistry_AreaCode as $v=>$d){
					$AreaCode_Select .= "<option value='$v' ".($add_area==$v?"selected ":"")."/>".$d;
				}
			}
			$AreaCode_Select .= "</select>";

			$table.="<tr><td align=right>$i_UserAddress_Area :&nbsp;</td><td >$AreaCode_Select</td></tr>";
			$table.="<tr><td align=right>$i_UserAddress_Road :&nbsp;</td><td ><input type=text name='add_road_$record_id' value='$add_road' size=20 maxlength='50' /></td></tr>";
			$table.="<tr><td align=right>$i_UserAddress_Address :&nbsp;</td><td ><input type=text name='add_address_$record_id' value='$add_address' size=20 maxlength='50' /></td></tr>";
		}else{
			$table.="<tr><td align=right>$i_UserAddress :&nbsp;</td><td ><textarea cols=30 rows=3 name='address_$record_id'>$address</textarea></td></tr>";
		}
		$table.="</table>";
		
		$table_content.="<td class='$css'>$table</td>";
		$table_content.="<td class='$css'><input type=radio name='main[]' value='$record_id'".($isMain==1?" CHECKED ":"")."></td>";
		$table_content.="<td class='$css'><input type=radio name='ec[]' value='$record_id'".($isEmergencyContact==1?" CHECKED ":"")."></td>";
		if($plugin['sms'])
			$table_content.="<td class='$css'><input type=radio name='sms[]' value='$record_id'".($isSMS==1?" CHECKED ":"")."></td>";
		//$table_content.="<td class='$css'><a href='remove.php?RecordID=$record_id'><img src='$image_path/admin/button/s_btn_delete_$intranet_session_language.gif' border=0 ></a></td>";
		//$table_content.="<td class='$css'><a href=\"javascript:removeGuardian('r_$record_id')\"><img src='$image_path/admin/button/s_btn_delete_$intranet_session_language.gif' border=0 ></a></td>";
		$table_content.="<td class='$css'><input type=checkbox name='Delete[]' value='$record_id' onClick=\"javascript:checkRemoveGuardian(this);\"></td>";

		$table_content.="</tr>";
	}
	$table_content.="</table>";

}else{
	

	$table_content.="</table>\n";
	$table_content.="<script language=javascript>\n";
	$table_content.="addGuardian();\n";
	$table_content.="</script>\n";
	

	
}

## blank js relation selection
$js_select_relation ="<SELECT name='relation_new_\"+row.id+\"'>";
$js_select_relation.="<OPTION value=''>-- $button_select --</OPTION>";
foreach($ec_guardian AS $key =>$relation_name)
	$js_select_relation.="<OPTION value='$key'>$relation_name</OPTION>";
$js_select_relation.="</SELECT>";

?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentGuardian['MenuInfo'], 'index.php',$student_name,'') ?>
<?= displayTag("head_guardian_$intranet_session_language.gif", $msg) ?>


<script language='javascript'>
// remove existing guardian
function checkRemoveGuardian(obj){
	if(obj.checked==false) return;
	guardian_id = obj.value;
	objMains = document.getElementsByName('main[]');
	for(i=0;i<objMains.length;i++){
		if(objMains[i].value == guardian_id){
			if(objMains[i].checked){
				alert('<?=$i_StudentGuardian_warning_Delete_Main_Guardian?>');
			}
			break;
		}
	}

	objECs = document.getElementsByName('ec[]');
	for(i=0;i<objECs.length;i++){
		if(objECs[i].value == guardian_id){
			if(objECs[i].checked){
				alert('<?=$i_StudentGuardian_warning_Delete_EmergencyContact?>');
			}
			break;
		}
	}
	<?php if($plugin['sms']){?>
		objSms  = document.getElementsByName('sms[]');
	
		for(i=0;i<objSms.length;i++){
			if(objSms[i].value == guardian_id){
				if(objSms[i].checked){
					alert('<?=$i_StudentGuardian_warning_Delete_SMS?>');
				}
				break;
			}
		}
	<?php } ?>
}

function checkGuardian(objChName,objEnName,objPhone,objEmPhone){
	if(objChName.value == '' && objEnName.value==''){
		alert('<?=$i_StudentGuardian_warning_enter_chinese_english_name?>');
		objChName.focus();
		return false;
	}
	if(objPhone.value=='' && objEmPhone.value==''){
		alert('<?=$i_StudentGuardian_warning_enter_phone_emphone?>');
		objPhone.focus();
		return false;
	}
	return true;
}

// add new guardian
function addGuardian(){
	objTable = document.getElementById("guardians");
	tableRows = objTable.rows;
	
	rowStyle ='tableContent';
	row = objTable.insertRow(-1);
	row.id = parseInt(document.form1.tsize.value,10)+1;
	
	document.form1.tsize.value = row.id;
	document.form1.newids.value+=document.form1.newids.value!=""?","+row.id:row.id;
	
	cell0 = "<table border=0 cellpadding=0 cellspacing=0 width=100%>";
	cell0+="<tr><td align=right><?=$i_UserChineseName?> :&nbsp;</td><td><input type=text name='chname_new_"+row.id+"' value='' size=10></td></tr>";
	cell0+="<tr><td align=right><?=$i_UserEnglishName?> :&nbsp;</td><td><input type=text name='enname_new_"+row.id+"' value='' size=20></td></tr>";
	cell0+="<tr><td align=right><?=$i_StudentGuardian_Occupation?> :&nbsp;</td><td><input type=text name='occupation_new_"+row.id+"' value='' size=20 maxlength=60></td></tr>";
	cell0+="<tr><td align=right><?=$Lang['AccountMgmt']['NameOfCompany/Organization']?> :&nbsp;</td><td><input type=text name='companyName_new_"+row.id+"' value='' size=20 maxlength=255></td></tr>";
	cell0+="<tr><td align=right><?=$ec_iPortfolio['relation']?> :&nbsp;</td><td><?=$js_select_relation?></td></tr>";
	cell0+="<tr><td align=right><?=$i_StudentGuardian_Phone?> :&nbsp;</td><td><input type=text name='phone_new_"+row.id+"' value='' size=10></td></tr>";
	cell0+="<tr><td align=right><?=$i_StudentGuardian_EMPhone?> :&nbsp;</td><td><input type=text name='emphone_new_"+row.id+"' value='' size=10></td></tr>";
	cell0+="<tr><td align=right><?=$i_StudentGuardian_LiveTogether?> :&nbsp;</td><td><input type=radio name='liveTogether_new_"+row.id+"' value='1' id='liveTogether_new_"+row.id+"_1' checked/><label for='liveTogether_new_"+row.id+"_1'><?=$i_general_yes?></label> <input type=radio name='liveTogether_new_"+row.id+"' value='0' id='liveTogether_new_"+row.id+"_0'/><label for='liveTogether_new_"+row.id+"_0'><?=$i_general_no?></label></td></tr>";

	<?php if($plugin['StudentRegistry']){ 
		$js_select_area ="<SELECT name='add_area_new_\"+row.id+\"'>";
		foreach($i_StudentRegistry_AreaCode as $v=>$d)
		        $js_select_area .="<OPTION value='$v'/>$d";
		$js_select_area.="</SELECT>";

?>

		cell0+="<tr><td align=right><?=$i_UserAddress_Area?> :&nbsp;</td><td ><?=$js_select_area?></td></tr>";
		cell0+="<tr><td align=right><?=$i_UserAddress_Road?> :&nbsp;</td><td ><input type=text name='add_road_new_"+row.id+"' value='' size=20 maxlength='50' /></td></tr>";
		cell0+="<tr><td align=right><?=$i_UserAddress_Address?> :&nbsp;</td><td ><input type=text name='add_address_new_"+row.id+"' value='' size=20 maxlength='50' /></td></tr>";


	
	<?php } else{?>
		cell0+="<tr><td align=right><?=$i_UserAddress?> :&nbsp;</td><td><textarea cols=30 rows=3 name='address_new_"+row.id+"'></textarea></td></tr>";
	<?php } ?>
	cell0+="</table>";
	
	cell1="<input type=radio name='main[]' value='new_"+row.id+"'>";

	cell12 = "<input type=radio name='ec[]' value='new_"+row.id+"'>";
	<?php if($plugin['sms']){ ?>
			cell2 = "<input type=radio name='sms[]' value='new_"+row.id+"'>";
			//cell3 = "<a href=\"javascript:removeGuardian('"+row.id+"')\"><img src='<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif' border=0></a>";
			cell3 = "<input type='button' value=' - ' onClick=\"javascript:removeGuardian('"+row.id+"')\">";
	<?php } else{?>
			// cell2 = "<a href=\"javascript:removeGuardian('"+row.id+"')\"><img src='<?=$image_path?>/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif' border=0></a>";
			cell2 = "<input type='button' value=' - ' onClick=\"javascript:removeGuardian('"+row.id+"')\">";

	<?php } ?>
	
	
	cell = row.insertCell(0);
	cell.className = rowStyle;
	cell.innerHTML = cell0;
	
	cell = row.insertCell(1);
	cell.className = rowStyle;
	cell.innerHTML = cell1;

	cell = row.insertCell(2);
	cell.className = rowStyle;
	cell.innerHTML = cell12;
	
	cell = row.insertCell(3);
	cell.className = rowStyle;
	cell.innerHTML = cell2;

	<?php if($plugin['sms']){?>
		cell = row.insertCell(4);
		cell.className = rowStyle;
		cell.innerHTML = cell3;
	<?php } ?>

}

// to remove new guardian
function removeGuardian(rid){

	objTable = document.getElementById("guardians");
	tableRows = objTable.rows;
	for(i=0;i<tableRows.length;i++)
		if(tableRows[i].id==rid){
			objTable.deleteRow(i);
			if(rid.indexOf("r_")>-1){
				//document.form1.deletedids.value+=document.form1.deletedids.value!=""?","+rid.substring(2):rid.substring(2);
			}else{

				newids = document.form1.newids.value;
				if(newids==rid) newids="";
				newids = newids.replace(","+rid,"");
				newids = newids.replace(rid+",","");
				document.form1.newids.value = newids;
			}
			break;
		}
			
}

function checkform(obj){

	if(obj==null) return false;
	
	deletedids='';
	delim ='';
	objsDelete = document.getElementsByName('Delete[]');
	for(j=0;j<objsDelete.length;j++){
			if(objsDelete[j].checked){
				deletedids+=delim+objsDelete[j].value;
				delim=',';
			}
		
	}
	ids = obj.recordids.value.split(",");
	if(typeof(ids)!='undefined' && ids!=''){
		for(i=0;i<ids.length;i++){
			
			// deleted
			if(deletedids.indexOf(ids[i])>-1) continue;
				
			objChName = getElement('chname_'+ids[i]);
			objEnName = getElement('enname_'+ids[i]);
			objPhone   = getElement('phone_'+ids[i]);
			objEmPhone = getElement('emphone_'+ids[i]);
			
			if(objChName==null || objEnName==null || objPhone==null || objEmPhone==null) return false;
			if(!checkGuardian(objChName,objEnName,objPhone,objEmPhone)) return false;

		}
	}else{
	}
	
	// check new 
	ids = document.form1.newids.value.split(",");
	if(typeof(ids)!='undefined' && ids!=''){
		for(i=0;i<ids.length;i++){
				
			objChName = getElement('chname_new_'+ids[i]);
			objEnName = getElement('enname_new_'+ids[i]);
			objPhone   = getElement('phone_new_'+ids[i]);
			objEmPhone = getElement('emphone_new_'+ids[i]);
			
			if(objChName==null || objEnName==null || objPhone==null || objEmPhone==null) continue;
			if(!checkGuardian(objChName,objEnName,objPhone,objEmPhone)) return false;

		}
	}
/*	
	hasMain = false;
	hasSMS = false;
	objsMain = document.getElementsByName('main[]');
	objsSms  = document.getElementsByName('sms[]');
	objsDelete = document.getEleme
	for(i=0;i<objsMain.length;i++)
		if(objsMain[i].checked && deletedids.indexOf(objsMain[i].value)==-1){
			hasMain = true;
			break;
		}
	if(!hasMain){
		alert('no main');
	}
	
	<?php if($plugin['sms']){?>
			for(i=0;i<objsSms.length;i++){
				if(objsSms[i].checked && deletedids.indexOf(objsSms[i].value)==-1){
					hasSMS = true;
					break;
				}
				
			}
			if(!hasSMS){
				alert('no sms');
			}
	<?php	} ?>
*/	
	return true;
}
function getElement(str_name){
		objs = document.getElementsByName(str_name);
		return objs[0];
}
function submitform(formObj){
	if(checkform(formObj))
		formObj.submit();
		
}
function resetform(formObj){
	formObj.action='';
	formObj.submit();
}
function back(){
	document.form1.action='search_result.php';
	document.form1.submit();
}
</script>
<form name=form1 method='get' action='edit_update.php'>
<table border=0 cellpadding=0 cellspacing=0 align=center width=560>
<tr><td align=right><img src="<?=$image_path?>/schoolrecord/btn_set.gif" align=absMiddle hspace=1 border=0><a class='iconLink' href='javascript:back()'><?=$i_StudentGuardian_Back_To_Search_Result?></a></td></tr>
<tr><td><?=$i_UserName?>&nbsp;:&nbsp;<?=$student_name?></td></tr>
</table>
<table border=0 cellpadding=0 cellspacing=0 align=center width=560>
<tr><td>
<input type=hidden name='tsize' value=0>
<input type=hidden name='newids' value=''>
<?=$table_content?>
</td></tr>
<tr><td>
<input type="button" value=" + " onClick="addGuardian()">
</td></tr>
</table>
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<a href='javascript:submitform(document.form1)'><img src="<?=$image_path?>/admin/button/s_btn_save_<?=$intranet_session_language?>.gif" border='0'></a>
<a href='javascript:resetform(document.form1)'><img src='<?=$image_path?>/admin/button/s_btn_reset_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>
</table>
<?php
for($i=0;$i<sizeof($ClassID);$i++){
	echo "<input type=hidden name=ClassID[] value='".$ClassID[$i]."'>";
}

?>
<input type=hidden name='recordids' value='<?=(sizeof($record_ids)>0?implode(",",$record_ids):"")?>'>
<input type=hidden name='StudentID[]' value='<?=$student_id?>'>
<input type=hidden name=keyword value="<?=$keyword?>">
</form>
<?php
intranet_closedb();
include_once("../../templates/adminfooter.php");
?>
