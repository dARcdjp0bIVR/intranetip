<?php
# using: 
/***************
 *  * **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
 * 2019-07-02 Henry [ip.2.5.10.10.1] fixed suspected eval problem
 * 2016-09-06 Carlos [ip.2.5.7.9.1] Fixed import format header for macau pui ching.
 * 2016-08-03 Ivan [ip.2.5.7.9.1]
 *  - added CompanyName for macau pui ching (but treat as general improvement)
 * **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
 * 2016-07-14 Carlos - Improved to clear guardian info for each student only one time to allow import multiple guardians for the same student. 
 * 2016-02-16 Kenneth
 * 	- Change import file (with class name & no, userLogin and RegNo)
 * Date: 2015-11-04 (Cameron)
 * Detail: check against UserLogin if $sys_custom['using_userlogin_for_guardian_import'] is set
 * Date: 2013-11-12 (Carlos)
 * Detail: force value of isMain, isSMS, isEmergencyContact to be either "1" or "0" 
 * Date: 2013-01-31 (Rita)
 * Details:	change import msg
 * 
 ***************/
set_time_limit(2*60*60); 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lo = new libfilesystem();
$luser = new libuser();
$filepath = $userfile;
$filename = $userfile_name;
$importMsg = '';

$valid_booleans = array("1","0");

function EXCLUSIVE_FIELD($UserIDs, $FieldToExclusive, $Table)
{
	global $eclass_db, $li;
	
	# Select all the records having field value 1
	$sql =	"
				SELECT
					UserID, RecordID
				FROM
					".$eclass_db.".".$Table."
				WHERE
					UserID IN (".implode(",",$UserIDs).") AND
					".$FieldToExclusive." = 1
				ORDER BY
					UserID, RecordID
			";
	$ExclusiveFieldArr = $li->returnArray($sql);

	$CurrentUserID = "";
	$PrevRecordID = "";
	for($i=0; $i<count($ExclusiveFieldArr); $i++)
	{
		# Current ID != $ExclusiveFieldArr[$i]["UserID"]
		# => Finish processing one user
		if($CurrentUserID != $ExclusiveFieldArr[$i]["UserID"])
		{
			$CurrentUserID = $ExclusiveFieldArr[$i]["UserID"];
			$PrevRecordID = $ExclusiveFieldArr[$i]["RecordID"];
		}
		# Current ID = $ExclusiveFieldArr[$i]["UserID"]
		# => Previous record has field value 1, need to change to 0 
		# => Keep in an array for one-off process
		else
		{
			$ToBeUpdatedRecordsArr[] = $PrevRecordID;
			$PrevRecordID = $ExclusiveFieldArr[$i]["RecordID"];
		}
	}
	
	# Modify field value of all the entries
	if(is_array($ToBeUpdatedRecordsArr))
	{
		$sql =	"
					UPDATE
						".$eclass_db.".".$Table."
					SET
						".$FieldToExclusive." = 0
					WHERE
						RecordID IN (".implode(",", $ToBeUpdatedRecordsArr).")
				";
		$li->db_db_query($sql);
	}

}

# csv format no need to check is $plugin['StudentRegistry'] or not (20081014-yatwoon)
/*
if($plugin['StudentRegistry']){
	if($plugin['sms']){
		# sample 4
		$file_format = array ("RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsSMS", "IsEmergencyContact", "Occupation", "IsLiveTogether", "Area", "Road", "Address");
		$var_feeding = 'list($regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$issms, $isec, $occupation,$islivetogether, $add_area, $add_road, $add_address)=$data[$i];';
	}
	else{
		# sample 3
		$file_format = array ("RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsEmergencyContact", "Occupation", "IsLiveTogether", "Area", "Road", "Address");
		$var_feeding = 'list($regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$isec, $occupation,$islivetogether, $add_area, $add_road, $add_address)=$data[$i]; $issms="";';
	}
}
else{
	if($plugin['sms']){
		# sample 2
		$file_format = array ("RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsSMS", "IsEmergencyContact");
		$var_feeding = 'list($regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$issms, $isec)=$data[$i]; $occupation = ""; $islivetogether = 1; $add_area = ""; $add_road = ""; $add_address = "";';
	}
	else{
		# sample 1
		$file_format = array ("RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsEmergencyContact");
		$var_feeding = 'list($regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$isec)=$data[$i]; $issms="";$occupation = ""; $islivetogether = 1; $add_area = ""; $add_road = ""; $add_address = "";';
	}
}
*/

//if ($sys_custom['using_userlogin_for_guardian_import']) {
//	$firstField = "UserLogin";
//	$firstFieldVar = "userlogin";	
//}
//else {
//	$firstField = "RegNo";
//	$firstFieldVar = "regno";	
//}

if($plugin['sms']){
	# sample 4 (RegNo) or sample 6 (UserLogin)
	$firstField = $sys_custom['using_userlogin_for_guardian_import']? 'UserLogin' : 'RegNo';
	$firstFieldVar = $sys_custom['using_userlogin_for_guardian_import']? '$userlogin' : '$regno';
	$file_format = array ($firstField,"EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsSMS", "IsEmergencyContact", "Occupation", "CompanyName", "IsLiveTogether", "Area", "Road", "Address");
	$var_feeding = 'list('.$firstFieldVar.',$enname,$chname,$phone,$emphone,$relation,$ismain,$issms, $isec, $occupation, $companyName, $islivetogether, $add_area, $add_road, $add_address)=$data[$i];';
}
else if($plugin['StudentRegistry']){
	# sample 7
	$file_format = array ("ClassName","ClassNumber","UserLogin","RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsEmergencyContact", "Occupation", "CompanyName", "IsLiveTogether", "Area", "Road", "Address");
	$var_feeding = 'list($classname,$classnumber,$userlogin,$regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$isec, $occupation, $companyName,$islivetogether, $add_area, $add_road, $add_address)=$data[$i]; $issms="";';
}else{
	# sample 8
	$file_format = array ("ClassName","ClassNumber","UserLogin","RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain", "IsEmergencyContact", "Occupation", "CompanyName", "IsLiveTogether", "Address");
	$var_feeding = 'list($classname,$classnumber,$userlogin,$regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$isec, $occupation, $companyName,$islivetogether,  $add_address)=$data[$i]; $issms=""; $add_area=""; $add_road="";';
}

$errorRows = array();

# Check If No File -> Back To Import Page
if($filepath=="none" || $filepath == ""){          # import failed
//	header("Location: import.php");
//	exit();

	$importMsg = 'noFileFound';
	header("Location: import.php?importMsg=$importMsg");
	exit();
}
else {
	//$ext = strtoupper($lo->file_ext($filename));
	if(!$limport->CHECK_FILE_EXT($filename))
	{
		header ("Location: import.php?msg=13");
		exit();
	}
		
	# read file into array
	# return 0 if fail, return csv array if success
	//$data = $lo->file_read_csv($filepath);
	$data = $limport->GET_IMPORT_TXT($filepath);
	$col_name = array_shift($data);                   # drop the title bar
	#Check file format
	$format_wrong = false;
	for ($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			//debug($col_name[$i]."---".$file_format[$i]);
			break;
		}
	}

	if ($format_wrong)
	{
		header ("Location: import.php?msg=13");
		exit();
	} 
       
	$li = new libdb();

	$error = false;  
//var_dump($data);
//die;
	$resetCache = array(); // StudentID as key
	$numOfRecords = count($data);
	$importSuccess = 0;
	for($i=0;$i<$numOfRecords;$i++){
		# check for empty line
		$test = trim(implode("",$data[$i])); 
		if($test=="") continue;

		$data[$i] = sanitizeEvalInput($data[$i]);
//		list($regno,$enname,$chname,$phone,$emphone,$relation,$ismain,$issms)=$data[$i];
		eval($var_feeding);
		$classname= addslashes($classname);
		$classnumber= addslashes($classnumber);
		$userlogin= addslashes($userlogin);

		$chname = addslashes($chname);
		$occupation = addslashes($occupation);
		$companyName = addslashes(trim($companyName));
		$add_road = addslashes($add_road);
		$add_address = addslashes($add_address);
		$relation = str_pad($relation, 2, "0", STR_PAD_LEFT);
		
		if($issms=="") $issms = 0;
		
		if(!in_array($ismain,$valid_booleans)){
			$ismain = "0";
		}
		if(!in_array($issms,$valid_booleans)){
			$issms = "0";
		}
		if(!in_array($isec,$valid_booleans)){
			$isec = "0";
		}
		if(!in_array($islivetogether,$valid_booleans)) {
			$islivetogether = "0";
		}
		
		##############################################
		#
		#	Check classname classnumber / Userlogin
		#	(priority:  userlogin >  regno > Class name / no)
		##############################################
		
		/*
		 * Case 1 : ClassName and Classnumber Exist, UserLogin, RegNo is empty
		 */
		$regoniseUserIdBy ='';
		
		if($userlogin!=''){
			$regoniseUserIdBy = 'UserLogin';
		}else if($regno!=''){
			$regoniseUserIdBy = 'WebSAMSRegNo';
		}else if($classname!=''&&$classnumber!=''){
			$regoniseUserIdBy = 'class';
		}else{
			header ("Location: import.php?msg=13");
			exit();
		}
		
		
		#Get UserId
		$filter="";
		$innerJoin="";
		if($regoniseUserIdBy=='UserLogin'){
			$StudentID = $luser->getUserIDByUserLogin($userlogin);
		}else if($regoniseUserIdBy=='WebSAMSRegNo'){
			$StudentID = $luser->getUserIDByWebSamsRegNo($regno);
		}else if($regoniseUserIdBy=='class'){
			$returnArr = $luser->GET_USER_BY_CLASS_CLASSNO($classname,$classnumber);
			$StudentID = $returnArr[0]['UserID'];
		}

		# remove all guardians of current student from database
		if($StudentID!='' && !isset($resetCache[$StudentID])){
					
			# remove all guardians of current student from database
			$sql =	"
					DELETE FROM
						$eclass_db.GUARDIAN_STUDENT
					WHERE
						UserID='$StudentID'
					";
			$li->db_db_query($sql);
			
			if($plugin['StudentRegistry']){
				$sql =	"
						DELETE FROM
							$eclass_db.GUARDIAN_STUDENT_EXT_1
						WHERE
							UserID='$StudentID'
						";
				$li->db_db_query($sql);
			}
			$resetCache[$StudentID] = $StudentID;
		}	
		
//		# get userid
//		if($guardians[$$firstFieldVar]['userid']=="")
//		{
//			$StudentID = '';
//			if(trim($$firstFieldVar)!=''){
//				
//				$filter = $sys_custom['using_userlogin_for_guardian_import'] ? "UserLogin='".$$firstFieldVar."'" : "WebSAMSRegNo='".$$firstFieldVar."'";
//					
//				$sql =	"
//						SELECT
//							UserID
//						FROM
//							INTRANET_USER
//						WHERE
//							$filter
//						";
//				$temp = $li->returnVector($sql);
//				$StudentID = $temp[0];
//				
//				if($StudentID!='')
//				{
//					$guardians[$$firstFieldVar]['userid']=$StudentID;
//					
//					# remove all guardians of current student from database
//					$sql =	"
//							DELETE FROM
//								$eclass_db.GUARDIAN_STUDENT
//							WHERE
//								UserID='$StudentID'
//							";
//					$li->db_db_query($sql);
//					
//					if($plugin['StudentRegistry'])
//					{
//						$sql =	"
//								DELETE FROM
//									$eclass_db.GUARDIAN_STUDENT_EXT_1
//								WHERE
//									UserID='$StudentID'
//								";
//						$li->db_db_query($sql);
//					}
//				}			
//			}
//		}
				
		if($ec_guardian[$relation]==""){ ## unknown relation
			# not handling 
		}
//		$guardians[$regno]['guardian'][] = array($enname,$chname,$phone,$emphone,$relation,$ismain,$issms);

//		if($plugin['StudentRegistry']){
			if($add_area == "" || $add_area == "O"){
				$address = "";
			}
			else{
				$address = $i_StudentRegistry_AreaCode[$add_area]."\n";
			}
			$address .= trim($add_road)."\n";
			$address .= trim($add_address);
// 		}
// 		else{
// 			$address = '';
// 		}
		
		if($StudentID!='')
		{
			
			$sql =	"
					INSERT INTO
						$eclass_db.GUARDIAN_STUDENT
						(UserID,EnName,ChName,Phone,EmPhone,Relation,Address,IsMain,IsSMS,InputDate,ModifiedDate)
					VALUES
						('$StudentID','$enname','$chname','$phone','$emphone','$relation','$address','$ismain','$issms',NOW(),NOW())
					";
	// 						debug_r($sql);
							
			$result_gs = $li->db_db_query($sql);
			$record_id = mysql_insert_id();
	
	
	// 		if($plugin['StudentRegistry'])
	// 		{
				# insert the extend record
				$sql =	"
							INSERT INTO
								$eclass_db.GUARDIAN_STUDENT_EXT_1
									(UserID, RecordID, IsLiveTogether, IsEmergencyContact, Occupation, CompanyName,AreaCode, Road, Address, InputDate, ModifiedDate)
							VALUES
								('$StudentID','$record_id', '$islivetogether', '$isec', '$occupation', '$companyName', '$add_area','$add_road', '$add_address', NOW(), NOW())
						";
				$result_gse1 = $li->db_db_query($sql);
	// 		}
	
			## Count No of Records Imported
			if($result_gs && $result_gse1){
				$importSuccess ++;	
			}else{
				$errorRows[] = '#'.($i+2);
			}

			# Eric Yip: Ensure at most one 1 for each student in IsMain, IsSMS, IsEmergencyContact
			# Step 1: Get ID of all the involved students
			if(!is_array($StudentIDArr) || !in_array($StudentID, $StudentIDArr)){
				$StudentIDArr[] = $StudentID;
			}
		}else{
			$errorRows[] = '#'.($i+2);
		}
		
		if($i % 10 == 0) usleep(1);
	}


	
	# Eric Yip: Ensure at most one 1 for each student in IsMain, isSMS, IsEmergencyContact
	if(is_array($StudentIDArr))
	{
		
		# Step 2: Run function to modify field values
		EXCLUSIVE_FIELD($StudentIDArr, "IsMain", "GUARDIAN_STUDENT");
		EXCLUSIVE_FIELD($StudentIDArr, "isSMS", "GUARDIAN_STUDENT");
		EXCLUSIVE_FIELD($StudentIDArr, "IsEmergencyContact", "GUARDIAN_STUDENT_EXT_1");

	}



/*
//sequence changed to done at above
	if(sizeof($guardians)>0){
		$v = "";
		$delim = "";
		foreach($guardians as $key => $values){
			$StudentID = $values['userid'];
			$guardian  = $values['guardian'];


			for($i=0;$i<sizeof($guardian);$i++){
				list($enname,$chname,$phone,$emphone,$relation,$ismain,$issms)=$guardian[$i];

				$v .= $delim."('$StudentID','$enname','$chname','$phone','$emphone','$relation','$ismain','$issms',NOW(),NOW())";
				$delim = ",";
			}
		}
		$sql="INSERT INTO $eclass_db.GUARDIAN_STUDENT (
		UserID,EnName,ChName,Phone,EmPhone,Relation,IsMain,IsSMS,InputDate,ModifiedDate
		) VALUES $v";
		$li->db_db_query($sql);
		//echo $sql."<Br>";
	}
*/
}           
intranet_closedb();

if($numOfRecords!=0){
	if($importSuccess!=0 && $importSuccess == $numOfRecords){		
		$importMsg = 'allRecordsAreImported';
	}elseif($importSuccess>0 && $importSuccess<$numOfRecords){
		$importMsg = 'someRecordsAreImported';
		if(count($errorRows)>0){
			$_SESSION['ADMIN_GUARDIAN_IMPORT_ERROR'] = implode(', ',$errorRows);
		}
		//debug_pr($importMsg);
	}elseif($importSuccess == 0 && $numOfRecords!=0){
		$importMsg = 'allRecordsCannotBeImported';
	}
}else{
	$importMsg = 'noRecordFound';
}

header("Location: import.php?importMsg=$importMsg");	

?>
