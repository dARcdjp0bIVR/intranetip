<?php
// Using: 
/***************
 * **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [start] ****
 * 2016-08-03 Ivan [ip.2.5.7.9.1]
 *  - added CompanyName for macau pui ching (but treat as general improvement)
 * **** MUST UPDATE SCHEMA WITH THIS IMPROVEMENT BEFORE ip.2.5.7.9.1 [end] ****
 * 
 * 2016-02-16 Kenneth
 * 	- Change import file (with class name & no, userLogin and RegNo)
 * 
 * 2015-11-04 [Cameron]
 * 	- add $sys_custom['using_userlogin_for_guardian_import'] and change corresponding header in sample files
 * 			
 * Date: 2013-01-31 (Rita)
 * Details:	change import msg
 * 
 ***************/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_intranet.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

$linterface = new interface_html();
$limport = new libimporttext();


# csv format no need to check is $plugin['StudentRegistry'] or not (20081014-yatwoon)
/*
$filename = $plugin['StudentRegistry']? ($plugin['sms'] ? 
					'sample4.csv' : 
					'sample3.csv' ) : 
				($plugin['sms'] ? 
					'sample2.csv' : 
					'sample.csv');
//					$filename = "sample2.csv";
$the_remarks_src = 'guardian_import_remind';
$plugin['sms'] ? $the_remarks_src .= '_sms' : '';
$plugin['StudentRegistry'] ? $the_remarks_src .= '_studentregistry' : '';
*/

if ($plugin['sms'] ) {
	$filename = $sys_custom['using_userlogin_for_guardian_import'] ? 'sample6.csv' : 'sample4.csv'; 
}else
//else {
//	$filename = $sys_custom['using_userlogin_for_guardian_import'] ? 'sample5.csv' : 'sample3.csv'; 
//}

###############################################################
#
#	FILE WITH CLASS & CLASS NO & USERLOGIN & REGNO
#
###############################################################
if($plugin['StudentRegistry']){
	$filename = 'sample7.csv';
}else{
	$filename = 'sample8.csv';
}


if ($sys_custom['using_userlogin_for_guardian_import']) {
	$the_remarks = $plugin['sms'] ? $Lang['AdminConsole']['GuardianInfo']['import_remind_studentregistry_use_userlogin_sms'] : $Lang['AdminConsole']['GuardianInfo']['import_remind_studentregistry_use_userlogin'];
}
else {
	$the_remarks_src = 'guardian_import_remind';
	$plugin['sms'] ? $the_remarks_src .= '_sms' : '';
	$plugin['StudentRegistry']?$the_remarks_src .= '_studentregistry':'';
	
	$the_remarks = $ec_iPortfolio[$the_remarks_src];

}

## Import Message 
$displayMsg = '';
if($importMsg!=''){
	if($importMsg =='allRecordsAreImported')
	{
		$displayMsg = '<font color="green">' . $Lang['AdminConsole']['ImportMessage']['AllRecordsAreImported'] .'</font>';
	}
	elseif($importMsg =='noFileFound')
	{	
		$displayMsg = '<font color="red">' . $Lang['AdminConsole']['ImportMessage']['NoFileUploaded'] .'</font>';
	}
	elseif($importMsg =='noRecordFound')
	{
		$displayMsg = '<font color="red">' . $Lang['General']['NoRecordFound']  .'</font>';
	}
	elseif($importMsg =='someRecordsAreImported')
	{
		if(isset($_SESSION['ADMIN_GUARDIAN_IMPORT_ERROR']))
		{
			$error_result = $_SESSION['ADMIN_GUARDIAN_IMPORT_ERROR'];
			$error_string = '<br />'.str_replace('<!--RowNumber-->',$error_result,"Error occurs in row <!--RowNumber-->.");
			unset($_SESSION['ADMIN_GUARDIAN_IMPORT_ERROR']);
		}
		$displayMsg = '<font color="red">' . $Lang['AdminConsole']['ImportMessage']['SomeRecordsAreImported'] .$error_string.'</font>';
	}
	elseif($importMsg =='allRecordsCannotBeImported')
	{
		$displayMsg = '<font color="red">' . $Lang['AdminConsole']['ImportMessage']['AllRecordsCannotBeImported'] .'</font>';
	}
}

?>
<?= displayNavTitle($i_adminmenu_adm, '', $i_StudentGuardian['MenuInfo'], 'index.php',$button_import,'') ?>
<?= displayTag("head_guardian_$intranet_session_language.gif", $msg, $displayMsg) ?>


<form name="form1" method="POST" action="import_update.php" enctype="multipart/form-data">
<blockquote>
<table width=500 border=0 cellpadding=3 cellspacing=2>
	<tr>
		<td align=right nowrap><?=$i_select_file ?>:</td>
		<td>
			<input type=file size=50 name=userfile><br />
			<?=$linterface->GET_IMPORT_CODING_CHKBOX() ?>
		</td>
	</tr>
	<tr>
		<td colspan=2>&nbsp;<br />&nbsp;</td>
	</tr>
	<tr>
		<td colspan=2>
			<?=$the_remarks?><br />&nbsp;
			<a class=functionlink_new href="<?=GET_CSV($filename)?>" target=_blank>[<?=$i_general_clickheredownloadsample?>]</a>
		</td>
	</tr>
</table>
</blockquote>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr>
		<td height="22" style="vertical-align:bottom"><hr size=1 /></td>
	</tr>
	<tr>
		<td align="right">
			<?=btnSubmit() ?>
			<a href="index.php"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0' /></a>
		</td>
	</tr>
</table>
</form>

<?php
include_once($PATH_WRT_ROOT."templates/adminfooter.php");
?>
