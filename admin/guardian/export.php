<?php
/**
 * Change Log:
 * 
 * 2016-02-16 Kenneth
 * 	- Change export file format to match import file (with class name & no, userLogin and RegNo)
 * 
 */
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libexporttext.php");
intranet_opendb();

$li = new libdb();
$lexport = new libexporttext();

$namefield = getNameFieldByLang("a.");

$delim="";
for($i=0;$i<sizeof($ClassID);$i++){
	$class_str .= $delim."'".$ClassID[$i]."'";
	$delim=",";
}
$additionalStatment = "";
if($plugin['StudentRegistry']){
	$additionalStatment="c.AreaCode,c.Road,";
	$exportColumn = array("StudentName","ClassName","ClassNumber","UserLogin","RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain","IsEmergencyContact","Occupation","IsLiveTogether","Area","Road","Address");
}else{
	$exportColumn = array("StudentName","ClassName","ClassNumber","UserLogin","RegNo","EnName","ChName","Phone","EmPhone","Relation","IsMain","IsEmergencyContact","Occupation","IsLiveTogether","Address");
}
$sql="	SELECT  
			$namefield as student_name,
			a.ClassName,
			a.ClassNumber,
			a.UserLogin,		
			a.WebSAMSRegNo,
			b.EnName, 
			b.ChName,
			b.Phone,
			b.EmPhone,
			b.Relation,
			b.IsMain,
			c.IsEmergencyContact,
			c.Occupation,
			c.IsLiveTogether,
			$additionalStatment
			c.Address				
		FROM 
			INTRANET_USER as a 
			LEFT OUTER JOIN	$eclass_db.GUARDIAN_STUDENT as b ON (a.UserID=b.UserID)
			INNER JOIN $eclass_db.GUARDIAN_STUDENT_EXT_1 as c ON (b.RecordID=c.RecordID)		
		WHERE 
			a.RecordType=2 AND 
			a.RecordStatus IN (0,1,2) AND 
			a.ClassName IN ($class_str) and
			b.UserID is not null
		";		
$result = $li->returnArray($sql) or die(mysql_error());
//debug_pr($result);		

$utf_rows = array();
foreach($result as $k=>$d)
{
	$tempAry = array();
	$delim = "";
	for($i=0;$i<=count($exportColumn);$i++)
	{
		$tempAry[] = intranet_undo_htmlspecialchars($d[$i]);
	}
	
	$utf_rows[] = $tempAry;
}

$export_content = $lexport->GET_EXPORT_TXT($utf_rows, $exportColumn);	

$filename = "eclass-guardian.csv";
$lexport->EXPORT_FILE($filename, $export_content);


intranet_closedb();
?>