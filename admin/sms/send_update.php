<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
if ($sms_vendor == "ORANGE")
{
    include_once("../../includes/liborangesms.php");
    $lsms = new liborangesms();
}
else
{
    header("Location: index.php");
    exit();
}
$message = $Message;

$sms_message = intranet_htmlspecialchars(trim($message));
if ($scdate != "" && $sctime != "")
{
    $time_send = strtotime("$scdate $sctime");
}
else $time_send = "";

$numbers = explode(";",$recipient);
for ($i=0; $i<sizeof($numbers); $i++)
{
     $numbers[$i] = trim($numbers[$i]);
}

$logsent = $lsms->sendSMSAction_v2($numbers,$sms_message, $time_send);
/*
    $records = $lsms->sendSMS($numbers,$sms_message, $time_send);
    $logsent = $lsms->sendLogRecords($records);

    $values = "";
    $delim = "";
    for ($i=0; $i<sizeof($records); $i++)
    {
         $jobid = $records[$i]->jobid;
         $mobile = $records[$i]->recipient;
         $values .= "$delim('$jobid','$mobile','$sms_message',1,now(),now())";
         $delim = ",";
    }
    $li = new libdb();
    $sql = "INSERT IGNORE INTO INTRANET_SMS_LOG (JobID, MobileNumber,Message,RecordType,DateInput,DateModified) VALUES $values";
    $li->db_db_query($sql);
*/
intranet_closedb();
header("Location: index.php?sent=1");

?>