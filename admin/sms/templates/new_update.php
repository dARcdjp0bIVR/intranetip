<?php
include("../../../includes/global.php");
include("../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();

$Title = intranet_htmlspecialchars(trim($Title));
$Content = intranet_htmlspecialchars(trim($Content));

$fieldname = "Title,Content,DateInput,DateModified";
$fieldvalue = "'$Title','$Content',now(),now()";

$sql = "INSERT INTO INTRANET_SMS_TEMPLATE ($fieldname) VALUES ($fieldvalue)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=1");
?>