<?php
/*
 * 	Date: 2013-03-20	Ivan [2012-0928-1438-14066]
 * 		- added scheduled send date time JS validation
 */
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");

intranet_opendb();
$li = new libdb();
$libsms = new libsmsv2();

$sql = "SELECT Title, Content FROM INTRANET_SMS_TEMPLATE ORDER BY Title";
$templates = $li->returnArray($sql,2);

if (sizeof($templates)!=0)
{
    $select_template = "<SELECT onChange=\"this.form.Message.value=this.value; this.form.Message.focus()\">\n";
    $select_template .= "<OPTION value=''> -- $button_select $i_SMS_MessageTemplate -- </OPTION>\n";
    for($i=0; $i<sizeof($templates); $i++)
    {
        list ($title,$content) = $templates[$i];
        $select_template .= "<OPTION value='$content'>$title</OPTION>\n";
    }
    $select_template .= "</SELECT>\n";
}

?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, 'index.php',$i_SMS_SendTo_Student_Guardian,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript src=../sms.js></SCRIPT>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         if (!check_text(obj.Message,'<?=$i_SMS_AlertMessageContent?>')) return false;
         if(obj.elements["Recipient[]"].length==0){ alert("<?php echo "$button_select $i_SMS_Recipient"; ?>"); return false; }
         
         <? if ($sms_vendor != "TACTUS") { ?>
	         if (obj.scdate.value != '')
	         {
	             if (!check_date(obj.scdate,'<?=$i_invalid_date?>')) return false;
	         }
	         if (obj.sctime.value != '')
	         {
	             if (!check_time(obj.sctime, '<?=$i_SMS_InvalidTime?>')) return false;
	         }
	         
	         if (obj.scdate.value != '' && obj.sctime.value != '') {
		     	var curTs = new Date().getTime();
		     	
		     	var scheduledDateText = obj.scdate.value;
		     	var scheduledDateAry = scheduledDateText.split('-');
		     	var scheduledYear = scheduledDateAry[0];
		     	var scheduledMonth = scheduledDateAry[1] - 1;
		     	var scheduledDate = scheduledDateAry[2];
		     	
		     	var scheduledTime = obj.sctime.value;
		     	var scheduledTimeAry = scheduledTime.split(':');
		     	var scheduledHour = scheduledTimeAry[0];
		     	var scheduledMin = scheduledTimeAry[1];
		     	var scheduledSec = scheduledTimeAry[2];
		     	
				var scheduledDateObj = new Date(scheduledYear, scheduledMonth, scheduledDate, scheduledHour, scheduledMin, scheduledSec);
				var scheduledDateTs = scheduledDateObj.getTime();
				
				if (scheduledDateTs <= curTs) {
					// scheduled date has past already
					alert('<?=$Lang['SMS']['ScheduledDateIsPast']?>');
					return false;
				}
		     }
         <? } ?>
         checkOptionAll(obj.elements["Recipient[]"]);
}
</SCRIPT>

<form name=form1 action=send_guardian_update.php method=POST  ONSUBMIT="return checkform(this)">
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<table width=100% border=0>
<tr><td align=right nowrap><?=$i_SMS_Recipient?>:</td><td>
<select name=Recipient[] size=4 multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td>
<td width=50%>
<a href="javascript:newWindow('choose_guardian/index.php?fieldname=Recipient[]',2)"><img src="/images/admin/button/s_btn_choose_recipient_<?=$intranet_session_language?>.gif" border="0"></a>
<br>
<a href="javascript:checkOptionRemove(document.form1.elements['Recipient[]'])"><img src="/images/admin/button/s_btn_delete_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
<tr><td colspan='2'>&nbsp;</td></tr>
<tr><td align=right nowrap><?=$i_SMS_MessageContent?>:</td><td><?=$select_template?><br><TEXTAREA onFocus="startTimer('<?=$langStr?>');" onChange="countit(this.form,0,'<?=$langStr?>');" onBlur="stopTimer();" NAME="Message" COLS="40" ROWS="4" WRAP="virtual"></TEXTAREA><FONT>&nbsp;</FONT><INPUT TYPE=TEXT NAME="Size" VALUE="160" SIZE="3" MAXLENGTH="3"></td></tr>
<tr><td colspan='3'><br><?=$libsms->Get_SMS_Limitation_Note_Table()?></td></tr>

<? if ($sms_vendor != "TACTUS") { ?>
	<tr><td colspan=2>&nbsp;</td></tr>
	<tr><td align=right nowrap="nowrap"><?=$i_SMS_SendDate?>:</td><td colspan=2><input type=text size=10 maxlength=10 name=scdate> (YYYY-MM-DD)</td></tr>
	<tr><td align=right nowrap="nowrap"><?=$i_SMS_SendTime?>:</td><td colspan=2><input type=text size=10 maxlength=10 name=sctime> (HH:mm:ss)</td></tr>
	<tr><td align=right></td><td colspan=2><font color=green><?=$i_SMS_NoteSendTime?></font></td></tr>
<? } ?>

</table>

</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border="0" >
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>

</table>
<INPUT TYPE=HIDDEN NAME='options' VALUE='0'>

<INPUT TYPE=HIDDEN NAME='chinesechar' VALUE=''>

<INPUT TYPE=HIDDEN NAME='Textsize' VALUE=''>
</form>
<?
include_once("../../../templates/adminfooter.php");
?>