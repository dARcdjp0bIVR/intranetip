<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libsmsv2.php"); 

if ($sms_vendor == "KANHAN")
{
     include_once("$intranet_root/includes/libkanhansms.php");
     $lobject_vendor = new libkanhansms();
}
else if ($sms_vendor == "CTM")
{
     include_once("$intranet_root/includes/libctmsms.php");
     $lobject_vendor = new libctmsms();
}
else if ($sms_vendor == "CLICKATELL")
{
     include_once("$intranet_root/includes/libclickatellsms.php");
     $lobject_vendor = new libclickatellsms();
}
else if ($sms_vendor == "TACTUS")
{
     include_once("$intranet_root/includes/libtactussms.php");
     $lobject_vendor = new libtactussms();
}
else
{
	die("Configuration of SMS is wrong. Please contact eClass Technical Support &lt;support@broadlearning.com&gt;");
}

intranet_opendb();

$lsms2 = new libsmsv2();


$year 	= $year=="" ? date("Y") : $year;
$month 	= $month=="" ? date("m") : $month;

if($month>0)		//Refresh month
{
	$start_date = date("Y-m-d H:i:s", mktime(0, 0, 0, $month, 1, $year));
	$end_date 	= date("Y-m-t H:i:s", mktime(23, 59, 59, $month, 1, $year));
	
	$count 		= $lobject_vendor->retrieveMessageCount($start_date, $end_date); 
	$lsms2->updateMessageCountRecord($year, $month, $count);
	
}
else				//Refresh Year
{
	for($i=1;$i<=12;$i++)
	{
		$start_date = date("Y-m-d H:i:s", mktime(0, 0, 0, $i, 1, $year));
		$end_date 	= date("Y-m-t H:i:s", mktime(23, 59, 59, $i, 1, $year));
		
		$count 		= $lobject_vendor->retrieveMessageCount($start_date, $end_date); 
		$lsms2->updateMessageCountRecord($year, $i, $count);
		
	}
}					

intranet_closedb();
header("Location: index.php?refresh=1");
?>