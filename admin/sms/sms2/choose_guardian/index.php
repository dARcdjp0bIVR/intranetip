<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libclass.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
intranet_opendb();
include_once("../../../../templates/fileheader_admin.php");

$li = new libclass();


## get class list with 1 or more guardians
//$sql = "SELECT ClassName,ClassID FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
/*
$sql="
		SELECT 
			DISTINCT a.ClassName,a.ClassID 
		FROM 
			INTRANET_CLASS AS a, 
			INTRANET_USER AS b, 
			$eclass_db.GUARDIAN_STUDENT AS c 
		WHERE 
			a.ClassName = b.ClassName AND
			b.UserID = c.UserID AND
			a.RecordStatus = 1
		ORDER BY a.ClassName
	";
*/
$curAcademicYearID = Get_Current_Academic_Year_ID();
$sql="
		SELECT 
			DISTINCT a.ClassTitleEn as ClassName, a.YearClassID as ClassID
		FROM 
			YEAR_CLASS AS a, 
			INTRANET_USER AS b, 
			$eclass_db.GUARDIAN_STUDENT AS c 
		WHERE 
			a.ClassTitleEn = b.ClassName AND
			a.AcademicYearID = '$curAcademicYearID' AND
			b.UserID = c.UserID
		ORDER BY a.ClassTitleEn
	";
$result = $li->returnArray($sql,2);

$classlist = "<SELECT multiple name='ClassID[]' style='height=100px'>\n";
for ($i=0; $i < sizeof($result); $i++)
{
     $name = $result[$i][0];
     $classid  = $result[$i][1];
     $selected_str = (is_array($ClassID) && in_array($classid,$ClassID))?" SELECTED " : "";
     $classlist .= "<OPTION value='$classid' $selected_str>$name</OPTION>\n";
}
$classlist .= "</SELECT>\n";

if(sizeof($ClassID)>0){
	$namefield = getNameFieldWithClassNumberByLang("a.");
	$list = implode(",",$ClassID);
/*
	$sql="
			SELECT 
				a.UserID, $namefield 
			FROM 
				INTRANET_USER AS a , 
				INTRANET_CLASS AS b
			WHERE 
				a.ClassName = b.ClassName AND 
				b.ClassID IN ($list) AND
				a.RecordType=2 AND 
				a.RecordStatus IN (0,1,2) 
			ORDER BY 
				a.ClassName,a.ClassNumber
		";
*/

/*
	$sql="
			SELECT 
				DISTINCT a.UserID, $namefield 
			FROM 
				INTRANET_USER AS a , 
				INTRANET_CLASS AS b, 
				$eclass_db.GUARDIAN_STUDENT AS c  
			WHERE 
				a.ClassName = b.ClassName AND 
				b.ClassID IN ($list) AND 
				c.UserID = a.UserID AND
				a.RecordType=2 AND 
				a.RecordStatus IN (0,1,2) 
			ORDER BY 
				a.ClassName,a.ClassNumber
		";
*/
	$sql="
			SELECT 
				DISTINCT a.UserID, $namefield 
			FROM 
				INTRANET_USER AS a , 
				YEAR_CLASS AS b, 
				$eclass_db.GUARDIAN_STUDENT AS c  
			WHERE 
				a.ClassName = b.ClassTitleEn AND 
				b.YearClassID IN ($list) AND 
				c.UserID = a.UserID AND
				a.RecordType=2 AND 
				a.RecordStatus IN (0,1,2) 
			ORDER BY 
				a.ClassName,a.ClassNumber
		";
		
	$students = $li->returnArray($sql,2);
	$select_students ="<SELECT name='StudentID[]' multiple style='height=150px'>\n";
	for($i=0;$i<sizeof($students);$i++){
		list($userid,$name) = $students[$i];
		$select_students.="<OPTION value='$userid'>$name</OPTION>\N";
	}
	$select_students.="</SELECT>";
}

?>

<script language="javascript">
function Trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}

function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
     x = (obj.name == "ClassID[]") ? "G" : "U";
     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;
     
     var browserAgent = navigator.userAgent;
	 var browser = browserAgent.match(/MSIE/);
     
     if (browser != 'MSIE' && Trim(parObj.options[0].value) == '')
	    parObj.remove(0);
	    
     while(i!=-1){
          par.checkOptionAdd(parObj, obj.options[i].text+"<?=$i_general_targetGuardian?>", x + obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}
function SelectAll()
{
        var obj = document.form1.elements['StudentID[]'];
        for (i=0; i<obj.length; i++)
        {
          obj.options[i].selected = true;
        }
}
</script>

<form name="form1" action="index.php" method="post">
<?= displayNavTitle($i_frontpage_campusmail_choose, '') ?>

<p style="padding-left:20px">
<table width=422 border=0 cellpadding=0 cellspacing=0>
<tr><td><img src="../../../../images/admin/pop_head.gif" width=422 height="19" border=0></td></tr>
<tr><td style="background-image: url(../../../../images/admin/pop_bg.gif);" >


<table width=100% border=0 cellpadding=10 cellspacing=0>
<tr>
<td>
<?php echo $i_UserParentLink_SelectClass; ?><br><img src=../../../../images/space.gif border=0 width=1 height=3><br>
<?php echo $classlist; ?>
<a href="javascript:checkOption(document.form1.elements['ClassID[]']);AddOptions(document.form1.elements['ClassID[]'])"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
<input type="image" onClick="checkOption(document.form1.elements['ClassID[]'])" src="/images/admin/button/s_btn_expand_<?=$intranet_session_language?>.gif" border="0">
</td></tr>
<?php if(sizeof($ClassID)>0) { ?>
<tr><td>
<?php echo $i_general_select_guardian_from_student; ?><br><span class=extraInfo>(<?=$i_SMS_Notice_Show_Student_With_Guardian_Only?>)</span><br><img src=../../../../images/space.gif border=0 width=1 height=3><br>
<?php echo $select_students; ?>
<a href="javascript:SelectAll()"><img src="../../../../images/admin/button/s_btn_select_all_<?=$intranet_session_language?>.gif" border="0"></a>
<a href="javascript:checkOption(document.form1.elements['StudentID[]']);AddOptions(document.form1.elements['StudentID[]'])"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>

</td></tr>
<?php } ?>
</table>

</td></tr>
<tr><td><img src="../../../../images/admin/pop_bottom.gif" width=422 height=18 border=0></td></tr>
<tr><td align=center height="40" style="vertical-align:bottom">
<a href="javascript:self.close()"><img src="/images/admin/button/s_btn_close_<?=$intranet_session_language?>.gif" border="0"></a>
</td></tr>
</table>
</p>

<input type=hidden name=fieldname value="<?php echo $fieldname; ?>">
</form>

<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>