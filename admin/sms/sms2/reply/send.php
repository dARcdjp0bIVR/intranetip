<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();
$li = new libdb();
$sql = "SELECT Title, Content FROM INTRANET_SMS_TEMPLATE ORDER BY Title";
$templates = $li->returnArray($sql,2);

if (sizeof($templates)!=0)
{
    $select_template = "<SELECT onChange=\"this.form.Message.value=this.value; this.form.Message.focus()\">\n";
    $select_template .= "<OPTION value=''> -- $button_select $i_SMS_MessageTemplate -- </OPTION>\n";
    for($i=0; $i<sizeof($templates); $i++)
    {
        list ($title,$content) = $templates[$i];
        $select_template .= "<OPTION value='$content'>$title</OPTION>\n";
    }
    $select_template .= "</SELECT>\n";
}

if ($intranet_session_language == "en")
{
    $langStr = "English";
}
else
{
    $langStr = "Chinese";
}

?>

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../index.php',$i_SMS_Reply_Message,'index.php',$i_SMS_Send_Raw,'') ?>

<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<SCRIPT LANGUAGE=Javascript src=../../sms.js></SCRIPT>
<SCRIPT LANGUAGE=Javascript>
function checkform(obj)
{
         if (!check_text(obj.Message,'<?=$i_SMS_AlertMessageContent?>')) return false;
         if (!check_text(obj.recipient,'<?=$i_SMS_AlertRecipient?>')) return false;
         if (obj.scdate.value != '')
         {
             if (!check_date(obj.scdate,'<?=$i_invalid_date?>')) return false;
         }
         if (obj.sctime.value != '')
         {
             if (!check_time(obj.sctime, '<?=$i_SMS_InvalidTime?>')) return false;
         }
}
</SCRIPT>
<form action=send_update.php method=POST ONSUBMIT="return checkform(this)">
<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<table width=100% border=0>
<tr><td align=right><?=$i_SMS_Recipient?>:</td><td><TEXTAREA name=recipient rows=5 cols=50></TEXTAREA><br>
<font color=green><?=$i_SMS_SeparateRecipient?></font></td></tr>
<!--
<tr><td align=right><?=$i_SMS_MessageContent?>:</td><td><?=$select_template?><br><TEXTAREA name=message rows=5 cols=50></TEXTAREA>
--->
<tr><td align=right><?=$i_SMS_MessageContent?>:</td><td><?=$select_template?><br><TEXTAREA onFocus="startTimer('<?=$langStr?>');" onChange="countit(this.form,0,'<?=$langStr?>');" onBlur="stopTimer();" NAME="Message" COLS="40" ROWS="4" WRAP="virtual"></TEXTAREA><FONT>&nbsp;</FONT><INPUT TYPE=TEXT NAME="Size" VALUE="160" SIZE="3" MAXLENGTH="3">

<br><p><?=$i_SMS_Cautions?>
<?php
   echo "<table border=0 width=90% align=left cellpadding=1 cellspacing=1>";
   for ($i=1; $i<=6; $i++)
   {
        $stmt = ${"i_SMS_Limitation$i"};
        echo "<tr><td>$i.</td><td>$stmt</td></tr>\n";
   }
   echo "</table>";
?>
</td></tr>
<tr><td colspan=2>&nbsp;</td></tr>
<tr><td align=right><?=$i_SMS_SendDate?>:</td><td><input type=text size=10 maxlength=10 name=scdate> (YYYY-MM-DD)</td></tr>
<tr><td align=right><?=$i_SMS_SendTime?>:</td><td><input type=text size=10 maxlength=10 name=sctime> (HH:mm:ss <?=$i_SMS_24Hr?>) </td></tr>
<tr><td align=right></td><td><font color=green><?=$i_SMS_NoteSendTime?></font></td></tr>
</table>

</td></tr>
<tr><td height="22" style="vertical-align:bottom"><hr size=1></td></tr>
<tr><td align="right">
<input type="image" src="/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif" border="0">
 <?= btnReset() ?>
 <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
</td>
</tr>

</table>

<INPUT TYPE=HIDDEN NAME='options' VALUE='0'>

<INPUT TYPE=HIDDEN NAME='chinesechar' VALUE=''>

<INPUT TYPE=HIDDEN NAME='Textsize' VALUE=''>

</form>
<?
include_once("../../../../templates/adminfooter.php");
?>