<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libsmsv2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");

intranet_opendb();

if(sizeof($Recipient)<=0 &&sizeof($usertype)<=0 ){
    header("Location: index.php");
    exit();
	
}

$ldbsms = new libsmsv2();

$message = $Message;
$sms_message = intranet_htmlspecialchars(trim($message));

if(sizeof($Recipient)>0){
	$Recipient = array_unique($Recipient);
	$Recipient = array_values($Recipient);
	$RecipientID = implode(",", $Recipient);
}
#$li = new libcampusmail();
$actual_recipient_array = $ldbsms->returnRecipientUserIDArray($RecipientID);


$namefield = $ldbsms->getSMSIntranetUserNameField("");

if(sizeof($usertype)>0){
	$usertype_list = implode(",",$usertype);
	if($usertype_list==2)
		$cond = " AND RecordStatus IN (0,1,2)";
	// $sql="SELECT UserID FROM INTRANET_USER WHERE RecordType IN ($usertype_list) AND TRIM(MobileTelNo) <> '' AND MobileTelNo IS NOT NULL ";
	$sql="SELECT UserID FROM INTRANET_USER WHERE RecordType IN ($usertype_list) $cond ";
	$temp = $ldbsms->returnVector($sql);
}

if (sizeof($actual_recipient_array)==0 && sizeof($temp)<=0)
{
    header("Location: index.php?nouser=1");
    exit();
}

if ($scdate != "" && $sctime != "")
{
    $time_send = "$scdate $sctime";
}
else $time_send = "";


# Grab mobile numbers and UserIDs
//$delim = "";
//$mobileList = "";
$mobileList=array();
for ($i=0; $i<sizeof($actual_recipient_array); $i++)
{
     //$mobileList .= "$delim".$actual_recipient_array[$i][0];
     //$delim = ",";
     $mobileList[] = $actual_recipient_array[$i][0];
}
for($i=0;$i<sizeof($temp);$i++){
	if(!in_array($temp[$i],$mobileList)){
		$mobileList[] = $temp[$i];
	}
}

$mobile_list = implode(",",$mobileList);
/*
$sql = "
		SELECT 
			UserID, 
			TRIM(MobileTelNo), 
			$namefield 
		FROM 
			INTRANET_USER 
		WHERE 
			TRIM(MobileTelNo) <> '' AND 
			MobileTelNo IS NOT NULL AND 
			UserID IN ($mobile_list) 
		ORDER BY 
			ClassName,
			ClassNumber,
			EnglishName
	";
	*/
$sql = "
		SELECT 
			UserID, 
			TRIM(MobileTelNo), 
			ClassName,
			ClassNumber,
			$namefield 
		FROM 
			INTRANET_USER 
		WHERE 
			UserID IN ($mobile_list) 
		ORDER BY 
			ClassName,
			ClassNumber,
			EnglishName
	";
$users = $ldbsms->returnArray($sql,5);
if (sizeof($users)==0)
{
    header("Location: index.php?nouser=1");
    exit();
}

$error_list=array();
$valid_list=array();
for($i=0;$i<sizeof($users);$i++){
	list($userid,$mobile,$classname,$classnumber,$name,) = $users[$i];
	if($ldbsms->isValidPhoneNumber($mobile)){
		$recipientData[] = array($mobile,$userid,addslashes($name),"");
		$valid_list[] = array($userid,$mobile,$name,$classname,$classnumber);
	}else{
		$reason =$i_SMS_Error_NovalidPhone;
		$error_list[] = array($userid,$mobile,$name,$classname,$classnumber,$reason);
	}
}


if(sizeof($recipientData)>0 && $send==1){
	$targetType = 3;
	$picType = 1;
	$adminPIC =$PHP_AUTH_USER;
	$userPIC = "";
	$frModule= "";
	$deliveryTime = $time_send;
	$isIndividualMessage=false;
	$isReplyMessage = true;
	$ldbsms->sendSMS($sms_message,$recipientData, $targetType, $picType, $adminPIC, "","",$deliveryTime,$isIndividualMessage,$isReplyMessage);
	$isSent = 1;
}


# ----------- valid list-------- #
$valid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
$valid_table.= "<tr><td>$i_SMS_Notice_Send_To_Users</td></tr></table>";
$valid_table.= "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>";
$valid_table.= "<tr class='tableTitle'><td width=30%>$i_UserName</td><td width=20%>$i_ClassName</td><td width=5%>$i_ClassNumber</td><td>$i_SMS_MobileNumber</td></tr>";
for($i=0;$i<sizeof($valid_list);$i++){
	list($userid,$mobile,$name,$classname,$classnumber)=$valid_list[$i];
    $css = $i%2==1?"tableContent":"tableContent2";
    $valid_table.="<tr class='$css'><td>".($name==""?"-":$name)."</td><td width=15%>".($classname==""?"-":$classname)."</td><Td width=5%>".($classnumber==""?"-":$classnumber)."</td><td>".($mobile==""?"-":$mobile)."</td></tr>";
	
}	
$valid_table.="</table>";


# ----------- error list -------------- #
$invalid_table = "<table width=560 border=0 cellpadding=2 cellspacing=1 align='center'>";
$invalid_table.= "<tr><td>$i_SMS_Warning_Cannot_Send_To_Users</td></tr>";
$invalid_table.= "</table>";
$invalid_table.= "<table width=560 border=1 cellpadding=2 cellspacing=1 align='center'>";
$invalid_table.= "<tr class='tableTitle'><td width=30%>$i_UserName</td><td width=20%>$i_ClassName</td><td width=5%>$i_ClassNumber</td><td>$i_SMS_MobileNumber</td><td width=30%>$i_Attendance_Reason</td></tr>";
for($i=0;$i<sizeof($error_list);$i++){
    list($sid,$mobile,$name,$classname,$classnumber,$reason)=$error_list[$i];
    $css = $i%2==0?"tableContent":"tableContent2";
    $invalid_table.="<tr class='$css'><td>".($name==""?"-":$name)."</td><td>".($classname==""?"-":$classname)."</td><Td>".($classnumber==""?"-":$classnumber)."</td><td>".($mobile==""?"-":$mobile)."</td><td>".($reason==""?"-":$reason)."</td></tr>";
}
$invalid_table.="</table>";
                    
if($isSent!=1){
		include_once("../../../../templates/adminheader_intranet.php");
		echo displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../index.php',$i_SMS_Reply_Message,'index.php',$i_SMS_Send_SelectUser,'');
		echo displayTag("head_sms_$intranet_session_language.gif", $msg);
		?>
		<script language='javascript'>
			function send(){
				obj = document.form1;
				if(document.form1==null) return;
				
				sendObj = obj.send;
				if(sendObj==null) return;
				sendObj.value = 1;
				
				document.form1.submit();
				
			}
		</script>
		<form name=form1 action='user_update.php' method=post>
		<input type=hidden name="scdate" value="<?=$scdate?>">
		<input type=hidden name="sctime" value="<?=$sctime?>">
		<input type=hidden name="Message" value="<?=stripslashes($sms_message)?>">
		<?php 
			for($i=0;$i<sizeof($Recipient);$i++)
				echo "<input type=hidden name='Recipient[]' value='".$Recipient[$i]."'>\n";
			for($i=0;$i<sizeof($usertype);$i++)
				echo "<input type=hidden name='usertype[]' value='".$usertype[$i]."'>\n";

		?>
		
		<input type=hidden name="send" value="">
		<?=$invalid_table?>
		<Br>
		<?=$valid_table?>
        <table width=560 border=0 cellpadding=0 cellspacing=0 align='center'>
           <tr><td><hr size=1></td></tr>
           <tr><td align='right'>
           <?php if(sizeof($valid_list)>0){?><a href='javascript:send()'><img src='/images/admin/button/s_btn_send_<?=$intranet_session_language?>.gif' border='0'></a><?php } ?>
           &nbsp;
               <a href="javascript:history.back()"><img src='/images/admin/button/s_btn_cancel_<?=$intranet_session_language?>.gif' border='0'></a>
            </td></tr></table>
        </form>
        <?php
        intranet_closedb();
		include_once("../../../../templates/adminfooter.php");
}else{
	intranet_closedb();
	header("Location: index.php?sent=".$isSent);
}

?>