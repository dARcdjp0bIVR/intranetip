<?php

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libsmsv2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

if(!$sms_use_reply_message){
	header("Location :index.php");
}

$sms = new libsmsv2();

$today = date('Y-m-d');
$FromDate=$FromDate==""?$today:$FromDate;
$ToDate=$ToDate==""?$today:$ToDate;


##################  Retrieve Reply Message From Service Provider ################
if($refresh==1){
	$from_time=$FromDate." 00:00:00";
	$to_time =$today." 23:59:59";
	$sms->retrieveReplyMessage($from_time,$to_time);
}
#################################################################################


################# Create Temp. Summary Table #####################

$date_cond =" AND DATE_FORMAT(a.DateInput,'%Y-%m-%d')>='$FromDate' AND DATE_FORMAT(a.DateInput,'%Y-%m-%d') <='$ToDate' ";
//$date_cond =" AND a.DateInput >='$FromDate' AND a.DateInput<='$ToDate' ";

// GET replied count for non individual message
$message_list_a = array();
$sql="SELECT a.SourceID,b.MessageCode,a.Content,COUNT(b.ReplyID),a.DateInput FROM INTRANET_SMS2_SOURCE_MESSAGE AS a LEFT OUTER JOIN
	INTRANET_SMS2_REPLY_MESSAGE AS b ON (a.SourceID = b.MessageID AND b.RecordStatus=1) 
 WHERE a.RecordType=1 AND a.IsIndividualMessage <> 1 $date_cond
 GROUP BY a.SourceID
 ORDER BY a.SourceID
";

$temp = $sms->returnArray($sql,5);

// GET replied count for individual message
$message_list_b = array();
$sql="SELECT b.SourceID,a.RecordID,a.Message,COUNT(c.ReplyID),a.DateInput FROM 
	INTRANET_SMS2_MESSAGE_RECORD AS a LEFT OUTER JOIN 
	INTRANET_SMS2_SOURCE_MESSAGE AS b ON (a.SourceMessageID = b.SourceID) LEFT OUTER JOIN 
	INTRANET_SMS2_REPLY_MESSAGE AS c ON (a.RecordID = c.MessageCode AND c.RecordStatus=1)
 WHERE b.RecordType=1 AND b.IsIndividualMessage=1 $date_cond
 GROUP BY a.RecordID
 ORDER BY a.RecordID
";

$temp2 = $sms->returnArray($sql,5);

$sql_a="DROP TEMPORARY TABLE TEMP_SMS2_REPLY_MESSAGE_COUNT";
$sql_b="
CREATE TEMPORARY TABLE TEMP_SMS2_REPLY_MESSAGE_COUNT(
	SourceID int(11),
	MessageCode int(11),
	Message varchar(255), 
	IsIndividualMessage int(11),
	NumOfReplied int(11),
	DeliveryTime datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$sql_c="INSERT INTO TEMP_SMS2_REPLY_MESSAGE_COUNT (SourceID,MessageCode,Message,IsIndividualMessage,NumOfReplied,DeliveryTime) VALUES";	

$sms->db_db_query($sql_a);
$sms->db_db_query($sql_b);

$message_list = array();
for($i=0;$i<sizeof($temp);$i++){
	list($source_id,$message_code,$content,$reply_count,$delivery_time) = $temp[$i];	
	$message_list[] = "('$source_id','','$content',0,'$reply_count','$delivery_time')";
}
for($i=0;$i<sizeof($temp2);$i++){
	list($source_id,$message_code,$content,$reply_count,$delivery_time) = $temp2[$i];	
	$message_list[] = "('','$message_code','$content',1,'$reply_count','$delivery_time')";
}
if(sizeof($message_list)>0){
	$sql_c.=implode(",",$message_list);

	$sms->db_db_query($sql_c);
}


######################## End Create Temp. Summary Table #######################

######################## Display ################
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
if ($field == "") $field = 2;


$message_id_field =" IF(a.SourceID IS NULL,'',a.SourceID) ";
$message_code_field=" IF(a.MessageCode IS NULL,'',a.MessageCode) ";

$sql="SELECT 
	CONCAT('<a href=\'list_reply.php?MessageID=',$message_id_field,'&MessageCode=',$message_code_field,'&IsIndividualMsg=',a.IsIndividualMessage,'\'>',a.Message,'</a>'),
	CONCAT('<a href=\'list_reply.php?MessageID=',$message_id_field,'&MessageCode=',$message_code_field,'&IsIndividualMsg=',a.IsIndividualMessage,'\'>',a.NumOfReplied,'/',IF(a.IsIndividualMessage=1,'1',b.RecipientCount),'</a>'),
	a.DeliveryTime FROM TEMP_SMS2_REPLY_MESSAGE_COUNT AS a LEFT OUTER JOIN INTRANET_SMS2_SOURCE_MESSAGE AS b ON (a.SourceID = b.SourceID) ";
	$li = new libdbtable($field, $order, $pageNo);
	$li->field_array = array("a.Message","a.NumOfReplied","a.DeliveryTime");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = "";
	$li->column_array = array(0,0,0);
	$li->wrap_array = array(0,0,0);
	$li->IsColOff = 2;
	
	// TABLE COLUMN
	$pos = 0;
	$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
	$li->column_list .= "<td width=50% class=tableTitle>".$li->column($pos++, $i_SMS_Reply_Message_Message)."</td>\n";
	$li->column_list .= "<td width=15% class=tableTitle>".$li->column($pos++, $i_SMS_Reply_Message_Replied_MessageCount)."</td>\n";
	$li->column_list .= "<td width=30% class=tableTitle>".$li->column($pos++, $i_SMS_Reply_Message_Replied_Message_DeliveryTime)."</td>\n";
	
	
	//$toolbar2="<input type=button onClick='javascript:refresh_list()' value='Refresh'>";
	$toolbar2="<a href='javascript:refresh_list()' class='iconLink'><img src=$image_path/admin/icon_revise.gif border=0 align='absmiddle'>$i_SMS_Reply_Message_Retrieve</a>";
	
	
?>
<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../index.php',$i_SMS_Reply_Message,'index.php',$i_SMS_Reply_Message_View_Sent_Message,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>
<link rel="stylesheet" href="/templates/calendar/dynCalendar.css" type="text/css" media="screen">
<script LANGUAGE="javascript">
	var css_array = new Array;
	css_array[0] = "dynCalendar_free";
	css_array[1] = "dynCalendar_half";
	css_array[2] = "dynCalendar_full";
	var date_array = new Array;
</script>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script src="/templates/calendar/dynCalendar.js" type="text/javascript" language="javascript"></script>
<script language='javascript'>
	          // Calendar callback. When a date is clicked on the calendar
          // this function is called so you can do as you want with it
          function calendarCallback(date, month, year)
          {
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
                           		document.forms['form1'].FromDate.value = dateValue;
                           
          }
          function calendarCallback2(date, month, year)
          {								
                           if (String(month).length == 1) {
                                   month = '0' + month;
                           }

                           if (String(date).length == 1) {
                                   date = '0' + date;
                           }
                           dateValue =year + '-' + month + '-' + date;
		                           document.forms['form1'].ToDate.value = dateValue;

          }
	function checkForm(formObj){
		if(formObj==null)return false;
			fromV = formObj.FromDate;
			toV= formObj.ToDate;
			if(!checkDate(fromV)){
					//formObj.FromDate.focus();
					return false;
			}
			else if(!checkDate(toV)){
						//formObj.ToDate.focus();
						return false;
			}
				return true;
	}
	function checkDate(obj){
 			if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
			return true;
	}
	function submitForm(obj){
		if(checkForm(obj))
			obj.submit();	
	}          
function refresh_list(){
	document.form1.refresh.value=1;
	document.form1.submit();
}
</script>
<form name="form1" method="GET">
<!-- date range -->
<table border=0 width=560 align=center>
	<tr>
		<td nowrap class=tableContent><?=$i_SMS_Reply_Message_DateRange_Msg?>:
		<br><?=$i_Profile_From?>
			<input type=text name=FromDate value="<?=$FromDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal = new dynCalendar('startCal', 'calendarCallback', '/templates/calendar/images/');
					//-->
				</script>&nbsp;
		 	<?=$i_Profile_To?>
		 	<input type=text name=ToDate value="<?=$ToDate?>" size=10>
				<script language="JavaScript" type="text/javascript">
					<!--
						startCal2 = new dynCalendar('startCal2', 'calendarCallback2', '/templates/calendar/images/');
					//-->
				</script>&nbsp;<span class=extraInfo>(yyyy-mm-dd)</span>
 </td></tr>
<!-- <tr><Td><input type=radio name=datetype value='1' <?=($datetype==1?"checked":"")?>><?=$i_general_startdate?>&nbsp;&nbsp;<input type=radio name=datetype value='2'  <?=($datetype==2?"checked":"")?>><?=$i_general_enddate?></td></tr>-->
 <tr><td><a href='javascript:submitForm(document.form1)'><img src='/images/admin/button/s_btn_submit_<?=$intranet_session_language?>.gif' border='0' align='absmiddle'></a></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<BR>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar", $searchbar); ?>&nbsp;</td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "$toolbar2", $functionbar); ?>&nbsp;</td></tr>
	<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
	</table>
	<?php echo $li->display(); ?>
	<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
	</table>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">
<input type=hidden name=refresh value=0>
</form>
<?
intranet_closedb();
include_once("../../../../templates/adminfooter.php");
?>