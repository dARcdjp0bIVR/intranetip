<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");

if(!$sms_use_reply_message){
	header("Location: ../");
	exit();
}

if ($sent == 1)
{
    $xmsg = "$i_SMS_MessageSent";
}
if ($nouser==1)
{
    $xmsg = $i_SMS_con_NoUser;
}
?>


<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_Reply_Message,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>


<table width=560 border=0 cellpadding=0 cellspacing=0 align=center>
<td class=tableContent height=300 align=left>
<blockquote>
<?= displayOption(              $i_SMS_Send_SelectUser, 'user.php', 1,
                                $i_SMS_Send_Raw, 'send.php', 1,
                                $i_SMS_File_Send, 'filesend.php', 1,
                                $i_SMS_SendTo_Student_Guardian,'send_guardian.php',1,
                                $i_SMS_Reply_Message_View_Sent_Message,'list.php',1
                                ) ?>

</blockquote>
</td></tr>
</table>

<?
include_once("../../../../templates/adminfooter.php");
?>