<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libinterface.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/fileheader.php");
intranet_opendb();

$li = new libdb();


if($MessageCode>0){
	$sql="SELECT Message FROM INTRANET_SMS2_MESSAGE_RECORD WHERE RecordID='$MessageCode'";
	$temp = $li->returnVector($sql);
	$message = $temp[0];
}
else if($MessageID>0){
	$sql="SELECT Content FROM INTRANET_SMS2_SOURCE_MESSAGE WHERE SourceID='$MessageID'";
	$temp = $li->returnVector($sql);
	$message = $temp[0];
}
$message = nl2br($message)."<BR><BR>";


if($MessageCode>0 && $IsIndividualMsg==1){
	$cond = " MessageCode='$MessageCode' ";
}else if($MessageID>0 && $IsIndividualMsg!=1){
	$cond =" MessageID = '$MessageID' ";
}	
$cond.=" AND RecordStatus=1 ";

$sql="SELECT COUNT(ReplyID),TRIM(ReplyMessage) FROM INTRANET_SMS2_REPLY_MESSAGE WHERE $cond GROUP BY TRIM(ReplyMessage) ORDER BY ReplyMessage";
$temp = $li->returnArray($sql,2);


$total=0;
for($i=0;$i<sizeof($temp);$i++){
	$total+=$temp[$i][0]+0;
}
for($i=0;$i<sizeof($temp);$i++){
	list($ans_count,$ans)=$temp[$i];
	$str = "$ans (".number_format(($ans_count/$total)*100,0)."%)";
	$DataArr[] = array($str,$ans_count);
}



$interface = new interface_html();

$ConfArr['IsHorizontal'] = 1; 
//$ConfArr['IsVertical'] = 1; 
$ConfArr['ChartX'] = "";
$ConfArr['ChartY'] = "";
$ConfArr['MaxWidth'] = 200;
$ConfArr['MaxHeight'] = 20;
?>
<style type='text/css'>
td{
	vertical-align:middle;
}
</style>
<table width=560 cellpadding=3 cellspacing=0 align=center>
<tr><td><b><?=$i_SMS_Reply_Message_Replied_Message_Stat?></b></td></tr>
<tr><Td align=center><table width="88%" border="0" cellspacing="0" cellpadding="5"><tr><td><?=$message?></td></tr></td></tR>
<tr><td>
<?=$interface->GEN_CHART($ConfArr, $DataArr)?>
</td></tr>
<tr><td>&nbsp;</td></tr>
<?php if($total>0){?>
<tr><td align=center><a href='javascript:window.print()'><img src='/images/admin/button/s_btn_print_<?=$intranet_session_language?>.gif' border=0 align='absmiddle'></a></td></tr>
<?php } ?>
</table>

<?php
include_once("../../../../templates/filefooter.php");
intranet_closedb();
?>