<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libsmsv2.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/adminheader_setting.php");
intranet_opendb();

if ($retrival==1)
{
    # Retrieve from BL Server
    $xmsg = $i_SMS_con_DataRetrieved;
}


### Refresh SMS status
$libsms = new libsmsv2();
$isRefresh = $_GET['isRefresh'];
if ($isRefresh)
	$libsms->Refresh_SMS_Status();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field=="") $field = 3;
if ($order=="") $order = 0;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        case 3: $field = 3; break;
        case 4: $field = 4; break;
        case 5: $field = 5; break;
        default: $field = 5; break;
}
$order = ($order == 1) ? 1 : 0;
if($filter == "") $filter = 1;
$filter = ($filter == 1) ? 1 : 0;

$namefield = getNameFieldByLang("b.");

#$content_field = " IF(a.IsIndividualMessage <>1, a.Content, IF(a.Content IS NOT NULL AND TRIM(a.Content)<>'', a.Content, IF(a.RecipientCount=1, c.Message, '<i>[$i_SMS_MultipleMessage]</i>')))";
$content_field = " IF(a.IsIndividualMessage <>1, a.Content, IF(a.Content IS NOT NULL AND TRIM(a.Content)<>'', a.Content, '<i>[$i_SMS_MultipleMessage]</i>'))";

$StatusField = '';
if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
{
	$StatusField = $libsms->Get_SMS_Status_Field('ismr');
	$StatusFieldSQL = ", IF (a.RecipientCount > 1 And (ismr.ReferenceID is Not Null Or ismr.ReferenceID != ''), 
							'<i>[".$i_SMS['MultipleStatus']."]</i>', 
							$StatusField) 
						as RecordStatus";
}


$sql = "
                SELECT
                		Distinct(a.SourceID),
                        concat('<a href=detail.php?SourceID=', a.SourceID,'>',$content_field,'</a>'),
                        concat('<a href=detail.php?SourceID=', a.SourceID,'>',a.RecipientCount,'</a>'),
                        IF (a.PICType=1, a.AdminPIC, if(a.PICType=2, $namefield, '". $i_SMS_System_Message ."')) as PIC,
                        a.DateInput
                        $StatusFieldSQL
                FROM 
                		INTRANET_SMS2_SOURCE_MESSAGE as a
                		INNER JOIN
                		INTRANET_SMS2_MESSAGE_RECORD as ismr
                		ON (a.SourceID = ismr.SourceMessageID)
             			LEFT OUTER JOIN 
             			INTRANET_USER as b 
             			ON (a.UserPIC = b.UserID)
        WHERE (
               $content_field LIKE '%$keyword%'
               )
               Group By
               			a.SourceID
";
#             LEFT OUTER JOIN INTRANET_SMS2_MESSAGE_RECORD as c ON (a.SourceID = c.SourceMessageID)

//debug_r($sql);

# TABLE INFO
$li = new libdbtable($field, $order, $pageNo);
if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
	$li->field_array = array("a.Content", "a.RecipientCount", "PIC", "a.DateInput", "RecordStatus");
else
	$li->field_array = array("a.Content", "a.RecipientCount", "PIC", "a.DateInput");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = "";
$li->column_array = array(5,0,0,0);
$li->wrap_array = array(25,0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = 'adminSMSViewSentRecords';

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(0, $i_SMS_MessageContent)."</td>\n";
$li->column_list .= "<td width=10% class=tableTitle>".$li->column(1, $i_SMS_RecipientCount)."</td>\n";
$li->column_list .= "<td width=15% class=tableTitle>".$li->column(2, $i_SMS_PIC)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle nowrap>".$li->column(3, $i_SMS_TargetSendTime)."</td>\n";
if (($sms_vendor == 'CTM') || ($sms_vendor == 'TACTUS'))
	$li->column_list .= "<td width=15% class=tableTitle>".$li->column(4, $i_general_status)."</td>\n";

$toolbar  = "<a class=iconLink href=javascript:checkPost(document.form1,'clear.php')>".newIcon()."$i_SMS_ClearRecords</a>".toolBarSpacer();
$toolbar  .= "<br><a class=iconLink href=javascript:checkPost(document.form1,'export.php')>".exportIcon()."$button_export</a>".toolBarSpacer();

$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";
$searchbar .= "<br />";
$searchbar .= "<a href='javascript:refresh();' class=iconLink><img src='/images/admin/icon_refresh.gif' border='0' align='absmiddle'>". $i_SMS['RefreshAllStatus'] ."</a>&nbsp;\n";


$sentSql = "SELECT COUNT(*) FROM INTRANET_SMS_LOG WHERE RecordStatus = 2 OR RecordStatus = 0 OR RecordStatus IS NULL";
$result = $li->returnVector($sentSql);
$sent = $result[0]+0;

$quota = get_file_content("$intranet_root/admin/sms/limit.txt");
$removed = get_file_content("$intranet_root/admin/sms/removed.txt");
$removed += 0;
$sent += $removed;

$left = $quota - $sent;

$left = ($left<0? "<font color=red>$left</font>":"<font color=green>$left</font>");

$infobar = "$i_SMS_MessageSentSuccessfully: $sent<br>$i_SMS_MessageQuotaLeft: $left";
?>
<script language="javascript">
<!--
        function view_detail(sid)
        {
                with(document.form1)
                {
                        SourceID.value = sid;
                        submit();
                }
        }
        
        function refresh()
		{
			document.getElementById('isRefresh').value = 1;
			document.form1.submit();
		}
//-->
</script>
<form name="form1" method="get">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_SentRecords,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<? /* ?>
<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><?= $infobar ?></td></tr>
<tr><td><hr size=1 class="hr_sub_separator"></td></tr>
</table>
<? */ ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
	<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $searchbar); ?></td></tr>
	<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
	<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="SourceID" value="">
<input type="hidden" name="isRefresh" value="">


<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>