<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
intranet_opendb();
$li = new libdb();
$status_check_required = false;

if ($status_check_required)
{
    $count_sql_cond = "";
    $delete_sql_cond = "WHERE RecordStatus IS NOT NULL";
}
else
{
    $count_sql_cond = "RecordStatus IS NULL OR";
    $delete_sql_cond = "";
}

/*
$sql = "SELECT SUM(RecipientCount) FROM INTRANET_SMS2_SOURCE_MESSAGE WHERE $count_sql_cond RecordStatus = 2 OR RecordStatus = 0";
$result = $li->returnVector($sql);
$count = $result[0]+0;

if ($count != 0)
{
    $targetFile = "$intranet_root/admin/sms/removed.txt";
    if (is_file($targetFile))
    {
        $original = get_file_content($targetFile);
    }
    $original += 0;
    $new = $original + $count;
    write_file_content($new,$targetFile);
}
*/

# INTRANET_SMS2_SOURCE_MESSAGE
$sql = "DELETE FROM INTRANET_SMS2_SOURCE_MESSAGE $delete_sql_cond";
$li->db_db_query($sql);

#INTRANET_SMS2_MESSAGE_RECORD
$sql = "DELETE FROM INTRANET_SMS2_MESSAGE_RECORD $delete_sql_cond";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?msg=3");
?>