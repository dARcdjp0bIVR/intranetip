<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
include_once("../../../../includes/global.php");
include_once("../../../../includes/libfilesystem.php");
include_once("../../../../includes/libdb.php");
include_once("../../../../includes/libdbtable.php");
include_once("../../../../includes/libaccount.php");
include_once("../../../../includes/libsmsv2.php");
include_once("../../../../lang/lang.$intranet_session_language.php");
include_once("../../../../templates/adminheader_setting.php");
intranet_opendb();

$lsms		= new libsmsv2();

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$keyword = trim($keyword);
if ($field=="") $field = 0;
if ($order=="") $order = 1;
switch ($field){
        case 0: $field = 0; break;
        case 1: $field = 1; break;
        case 2: $field = 2; break;
        default: $field = 0; break;
}
$order = ($order == 1) ? 1 : 0;
$TemplateType = $TemplateType ? $TemplateType : 1;


if($TemplateType==1)
{
	$sql = "SELECT
              CONCAT('<a href=edit.php?TemplateType=1&TemplateID=',TemplateID,'>',Title,'</a>'), Content, DateModified,
              CONCAT('<input type=checkbox name=TemplateID[] value=', TemplateID ,'>')
        FROM INTRANET_SMS_TEMPLATE
        WHERE (
               Title LIKE '%$keyword%' OR
               Content LIKE '%$keyword%'
               )
        ";
        
	$field_array 	= array("Title", "Content","DateModified");
	$col0 			= $i_SMS_TemplateName;
}
else
{
	$TemplateAry = $lsms->TemplateAry();
	
	$t = "CASE TemplateCode ";
	$ava_template = "";
	foreach ($TemplateAry as $tempAry) 
	{
		$ava_template .= ":".$tempAry[0].":";
		$t .= " WHEN '{$tempAry[0]}' THEN '{$tempAry[1]}' ";
	}
	$t .= " ELSE '' END";
	
	$sql = "SELECT
              CONCAT('<a href=edit.php?TemplateType=2&TemplateID=',TemplateID,'>', $t ,'</a>'), 
              Content, 
              DateModified,
              case RecordStatus when 1 then '". $i_general_active."' else '". $i_general_inactive."' end,
              CONCAT('<input type=checkbox name=TemplateID[] value=', TemplateID ,'>')
        FROM INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE
        WHERE (
               TemplateCode LIKE '%$keyword%' OR
               Content LIKE '%$keyword%'
               )
          		and '".$ava_template."' like concat('%:',TemplateCode,':%')           
        ";	
        
	$field_array 	= array("TemplateCode", "Content","DateModified","RecordStatus");
	$col0 			= $i_SMS_Personalized_Template_SendCondition;
}

# TABLE INFO

$li = new libdbtable($field, $order, $pageNo);
$li->field_array = $field_array;
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,5,0);
$li->wrap_array = array(0,25,0);
$li->IsColOff = 2;

// TABLE COLUMN
$li->column_list .= "<td width=1 class=tableTitle>#</td>\n";
$li->column_list .= "<td width=30% class=tableTitle>".$li->column(0, $col0)."</td>\n";
$li->column_list .= "<td width=50% class=tableTitle>".$li->column(1, $i_SMS_TemplateContent)."</td>\n";
$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_LastModified)."</td>\n";
if($TemplateType==2)
	$li->column_list .= "<td width=20% class=tableTitle>".$li->column(2, $i_general_status)."</td>\n";
$li->column_list .= "<td width=1 class=tableTitle>".$li->check("TemplateID[]")."</td>\n";

if($plugin['sms'] and $bl_sms_version >= 2)
	$selection 	= $lsms->getTemplateTypeSelect("name=TemplateType onChange=this.form.submit()",$TemplateType);

if($lsms->returnNonExistsTemplateAry(1) or $TemplateType==1)
	$toolbar  = "<a class=iconLink href=javascript:checkGet(document.form1,'new.php')>".newIcon()."$button_new</a>\n".toolBarSpacer();
$searchbar  = "<input class=text type=text name=keyword size=10 maxlength=50 value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\">\n";
$searchbar .= "<a href='javascript:document.form1.submit()'><img src='/images/admin/button/t_btn_find_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;\n";

$functionbar  = "<a href=\"javascript:checkEdit(document.form1,'TemplateID[]','edit.php')\"><img src='/images/admin/button/t_btn_edit_$intranet_session_language.gif' border='0' align='absmiddle'></a>";
$functionbar .= "<a href=\"javascript:checkRemove(document.form1,'TemplateID[]','remove.php')\"><img src='/images/admin/button/t_btn_delete_$intranet_session_language.gif' border='0' align='absmiddle'></a>&nbsp;";
?>

<form name="form1" method="get">

<?= displayNavTitle($i_adminmenu_plugin, '', $i_SMS_SMS, '../',$i_SMS_MessageTemplate,'') ?>
<?= displayTag("head_sms_$intranet_session_language.gif", $msg) ?>

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="/images/admin/table_head0.gif" width="560" height="13" border="0"></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", $toolbar, $selection . $searchbar); ?></td></tr>
<tr><td class=admin_bg_menu><?php echo $li->displayFunctionbar("-", "-", "", $functionbar); ?></td></tr>
<tr><td><img src="/images/admin/table_head1.gif" width=560 height=7 border=0></td></tr>
</table>
<?php echo $li->display(); ?>

<input type=hidden name=pageNo value="<?php echo $li->pageNo; ?>">
<input type=hidden name=order value="<?php echo $li->order; ?>">
<input type=hidden name=field value="<?php echo $li->field; ?>">
<input type=hidden name=page_size_change value="">
<input type=hidden name=numPerPage value="<?=$li->page_size?>">

<table width=560 border=0 cellpadding=0 cellspacing=0 align="center">
<tr><td><img src="<?=$image_path?>/admin/table_bottom.gif" width="560" height="16" border="0"></td></tr>
</table>

</form>

<?
include_once("../../../../templates/adminfooter.php");
intranet_closedb();
?>