<?php
include_once("../../../../includes/global.php");
include_once("../../../../includes/libdb.php");
intranet_opendb();

$li = new libdb();
$list = implode(",",$TemplateID);

$table = ($TemplateType==1) ? "INTRANET_SMS_TEMPLATE" : "INTRANET_SMS2_SYSTEM_MESSAGE_TEMPLATE";

$sql = "DELETE FROM $table WHERE TemplateID IN ($list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?TemplateType=$TemplateType&msg=3");
?>