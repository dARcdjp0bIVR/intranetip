<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libsms.php");
intranet_opendb();
if ($sms_vendor == "ORANGE")
{
    include_once("../../includes/liborangesms.php");
    $lsms = new liborangesms();
}
else
{
    header("Location: index.php");
    exit();
}

$message = $Message;
$sms_message = intranet_htmlspecialchars(trim($message));

$Recipient = array_unique($Recipient);
$Recipient = array_values($Recipient);
$RecipientID = implode(",", $Recipient);

$ldbsms = new libsms();

#$li = new libcampusmail();

$actual_recipient_array = $ldbsms->returnRecipientUserIDArray($RecipientID);
if (sizeof($actual_recipient_array)==0)
{
    header("Location: index.php?nouser=1");
    exit();
}

if ($scdate != "" && $sctime != "")
{
    $time_send = strtotime("$scdate $sctime");
}
else $time_send = "";


# Grab mobile numbers and UserIDs
$delim = "";
$mobileList = "";
for ($i=0; $i<sizeof($actual_recipient_array); $i++)
{
     $mobileList .= "$delim".$actual_recipient_array[$i][0];
     $delim = ",";
}

$sql = "SELECT UserID, TRIM(MobileTelNo) FROM INTRANET_USER WHERE TRIM(MobileTelNo) <> '' AND MobileTelNo IS NOT NULL AND UserID IN ($mobileList)";

$users = $ldbsms->returnArray($sql,2);
if (sizeof($users)==0)
{
    header("Location: index.php?nouser=1");
    exit();
}

$numbers = array();
for ($i=0; $i<sizeof($users); $i++)
{
     list ($uid,$mobileno) = $users[$i];
     if (!in_array($mobileno,$numbers))
     {
          $userIDs[$mobileno] = $uid;
          $numbers[] = $mobileno;
     }
}

$records = $lsms->sendSMS($numbers,$sms_message, $time_send);
$logsent = $lsms->sendLogRecords($records);

$values = "";
$delim = "";
for ($i=0; $i<sizeof($records); $i++)
{
     $jobid = $records[$i]->jobid;
     $mobile = $records[$i]->recipient;
     $uid = $userIDs[$mobile];
     $values .= "$delim('$jobid','$mobile','$uid','$sms_message',3,now(),now())";
     $delim = ",";
}
$sql = "INSERT IGNORE INTO INTRANET_SMS_LOG (JobID, MobileNumber,ReceiverID,Message,RecordType,DateInput,DateModified) VALUES $values";
$ldbsms->db_db_query($sql);

intranet_closedb();
header("Location: index.php?sent=1");

?>