<?php
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
intranet_opendb();
if ($sms_vendor == "ORANGE")
{
    include_once("../../includes/liborangesms.php");
    $lsms = new liborangesms();
}
else
{
    header("Location: index.php");
    exit();
}
$li = new libdb();

$message = $Message;

$sms_message = intranet_htmlspecialchars(trim($message));
if ($scdate != "" && $sctime != "")
{
    $time_send = strtotime("$scdate $sctime");
}
else $time_send = "";


# Grab mobile numbers and UserIDs
$typeList = implode(",",$usertype);
$sql = "SELECT UserID, TRIM(MobileTelNo) FROM INTRANET_USER WHERE TRIM(MobileTelNo) <> '' AND MobileTelNo IS NOT NULL AND RecordType IN ($typeList)";
$users = $li->returnArray($sql,2);
$numbers = array();
for ($i=0; $i<sizeof($users); $i++)
{
     list ($uid,$mobileno) = $users[$i];
     if (!in_array($mobileno,$numbers))
     {
          $userIDs[$mobileno] = $uid;
          $numbers[] = $mobileno;
     }
}

$records = $lsms->sendSMS($numbers,$sms_message, $time_send);
$logsent = $lsms->sendLogRecords($records);

$values = "";
$delim = "";
for ($i=0; $i<sizeof($records); $i++)
{
     $jobid = $records[$i]->jobid;
     $mobile = $records[$i]->recipient;
     $uid = $userIDs[$mobile];
     $values .= "$delim('$jobid','$mobile','$uid','$sms_message',2,now(),now())";
     $delim = ",";
}
$sql = "INSERT IGNORE INTO INTRANET_SMS_LOG (JobID, MobileNumber,ReceiverID,Message,RecordType,DateInput,DateModified) VALUES $values";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?sent=1");

?>