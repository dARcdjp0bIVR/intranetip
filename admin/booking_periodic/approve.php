<?php
/*
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *      
 */
include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/librb.php");
include("../../lang/lang.$intranet_session_language.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../includes/libwebmail.php");
include("../../lang/email.php");
intranet_opendb();

$li = new libdb();
$lr = new librb();

$status_reserved = 4;
$status_cancel = 1;
$status_reject = 5;
$status_pending = 0;
$approved_status = "2,3,4";
$ignore_status = "1,5";

$cancelList = "0";
$cancelMailSubject = periodic_booking_cancel_title();
$cancelSingleMailSubject = booking_title();
$approveMailSubject = periodic_booking_approve_title();

//$query_lock = "LOCK TABLES INTRANET_BOOKING_RECORD WRITE, INTRANET_BOOKING_RECORD as a WRITE, INTRANET_PERIODIC_BOOKING as b WRITE,INTRANET_PERIODIC_BOOKING WRITE";
$query_lock = "LOCK TABLES INTRANET_BOOKING_RECORD WRITE, INTRANET_BOOKING_RECORD as a WRITE, INTRANET_PERIODIC_BOOKING as b WRITE, INTRANET_PERIODIC_BOOKING WRITE, INTRANET_RESOURCE as c READ, INTRANET_SLOT as b READ, INTRANET_USER as d READ, INTRANET_USER as a READ";
$query_release = "UNLOCK TABLES";
# Set status -> approve if original is pending
//$sql = "UPDATE INTRANET_BOOKING SET RecordStatus = '$status_approve', DateModified = now() WHERE BookingID IN (".implode(",", $BookingID).")";
//$li->db_db_query($sql);

# For each record approving
# if that time slot is reserved, set record to cancel and send cancel email if update count =1
# not reserved: update to approved if original is pending
# if count = 0, continue
# else send approval mail
# for each pending record,
#  - send cancel mail, store those bookingID
# end 2 for
#
# else Set status -> cancel if pending and in cancel bookingID
for($i=0; $i<sizeof($PeriodicBookingID); $i++){
     $li->db_db_query($query_lock);
     $pid = $PeriodicBookingID[$i];

     # Get Resource ID
     $sql = "SELECT ResourceID FROM INTRANET_PERIODIC_BOOKING WHERE PeriodicBookingID = '$pid'";
     $temp = $li->returnVector($sql);
     $t_resource_id = $temp[0];
     if ($t_resource_id == "") continue;

     # Get all the dates in this periodic booking
     $sql = "SELECT DISTINCT a.BookingDate FROM INTRANET_BOOKING_RECORD as a WHERE a.PeriodicBookingID = '$pid' AND a.BookingDate >= CURDATE() AND a.RecordStatus = '$status_pending'";
     $whole = $li->returnVector($sql);
     $whole_dates_list = implode(",",$whole);

     # Get time slots of this periodic booking
     $sql = "SELECT b.TimeSlots FROM INTRANET_PERIODIC_BOOKING as b WHERE b.PeriodicBookingID = '$pid'";
     $tmp_res = $li->returnVector($sql);
     $slots_list = $tmp_res[0];

     # Get all crashed dates
     $sql = "SELECT DISTINCT a.BookingDate FROM INTRANET_BOOKING_RECORD as a WHERE a.RecourceID = '$t_resource_id' AND a.BookingDate IN ($whole_dates_list) AND a.TimeSlot IN ($slots_list) AND RecordStatus IN ($approved_status)";
     $reserved = $li->returnVector($sql);

     # Compute available records
     $target = array();
     if (sizeof($reserved)!=0)
     {
         $count = 0;
         for ($j=0; $j<sizeof($whole); $j++)
         {
              if (!in_array($whole[$j],$reserved))
              {
                   $target[$count++] = $whole[$j];
              }
         }
     }
     else
     {
         $target = $whole;
     }

     $dates = "'".implode("','",$target)."'";
     if (sizeof($target)!=0)
     {
         # Update status to approved
         $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$status_reserved', LastAction = now()  WHERE PeriodicBookingID = '$pid' AND BookingDate IN ($dates)";
         $li->db_db_query($sql);

         $sql = "UPDATE INTRANET_PERIODIC_BOOKING SET RecordStatus = '$status_reserved', LastAction = now()  WHERE PeriodicBookingID = '$pid'";
         $li->db_db_query($sql);
     }
     else
     {
         # No slots approved
         # Check any records already approved
         $sql = "SELECT COUNT(*) FROM INTRANET_BOOKING_RECORD WHERE RecordStatus IN ($approved_status) AND PeriodicBookingID = '$pid'";
         $temp = $li->returnVector($sql);
         if ($temp[0]=="" || $temp[0] == 0)
         {
             $temp_status = $status_reject;
             if ($dev_log_required)
             {
                 $sql = "SELECT BookingID FROM INTRANET_BOOKING_RECORD WHERE RecordStatus IN ($approved_status) AND PeriodicBookingID = '$pid'";
                 $temp = $li->returnVector($sql);
                 $dev_log_filepath = "$file_path/file/log_resource.txt";
                 # Time, $PHP_AUTH_USER, $HTTP_SERVER_VARS['REMOTE_ADDR'],$BookingID
                 $temp_time = date("Y-m-d H:i:s");
                 $temp_user = $PHP_AUTH_USER;
                 $temp_IP = $HTTP_SERVER_VARS['REMOTE_ADDR'];
                 $temp_list = implode(",",$temp);
                 $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_IP\",\"Periodic Approved->Cancel1\",\"$temp_list\"\n";
                 $temp_content = get_file_content($dev_log_filepath);
                 $temp_content .= $temp_logentry;
                 write_file_content($temp_content, $dev_log_filepath);
             }
         }
         else
         {
             $temp_status = $status_reserved;
         }
         $sql = "UPDATE INTRANET_PERIODIC_BOOKING SET RecordStatus = '$temp_status', LastAction = now()  WHERE PeriodicBookingID = '$pid'";
         $li->db_db_query($sql);
     }

     # Check other pending records crashed
     $sql = "SELECT a.BookingID FROM INTRANET_BOOKING_RECORD as a WHERE a.ResourceID = '$t_resource_id' AND a.BookingDate IN ($dates) AND a.TimeSlot IN ($slots_list) AND a.RecordStatus = '$status_pending'";
     $crashed = $li->returnVector($sql);
     if (sizeof($crashed)!=0)
     {
         # Reject crashed records
         $crashed_list = implode(",",$crashed);
         if ($dev_log_required)
         {
             $dev_log_filepath = "$file_path/file/log_resource.txt";
             # Time, $PHP_AUTH_USER, $HTTP_SERVER_VARS['REMOTE_ADDR'],$BookingID
             $temp_time = date("Y-m-d H:i:s");
             $temp_user = $PHP_AUTH_USER;
             $temp_IP = $HTTP_SERVER_VARS['REMOTE_ADDR'];
             $temp_list = $crashed_list;
             $temp_logentry = "\"$temp_time\",\"$temp_user\",\"$temp_IP\",\"Periodic Approved->Cancel2\",\"$temp_list\"\n";
             $temp_content = get_file_content($dev_log_filepath);
             $temp_content .= $temp_logentry;
             write_file_content($temp_content, $dev_log_filepath);
         }
         $sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$status_reject', LastAction = now()  WHERE BookingID IN ($crashed_list)";
         $li->db_db_query($sql);
     }

     $li->db_db_query($query_release);

     # Grab information and send approval mail
     $fields_name = "a.BookingStartDate,a.BookingEndDate, a.Remark, c.ResourceCategory, c.ResourceCode, c.Title, d.EnglishName, d.ChineseName, d.UserEmail, a.TimeSlots, a.RecordType, a.TypeValues, c.TimeSlotBatchID, d.UserID";
     $db_tables = "INTRANET_PERIODIC_BOOKING as a, INTRANET_RESOURCE as c, INTRANET_USER as d";
     $join_conds = "c.ResourceID = a.ResourceID AND a.UserID = d.UserID";
     $conds = "a.PeriodicBookingID = '$pid'";
     $sql = "SELECT $fields_name FROM $db_tables WHERE $join_conds AND $conds";
     $row = $li->returnArray($sql,14);
     $start = $row[0][0];
     $end = $row[0][1];
     $Remark = $row[0][2];
     $ResourceCategory = $row[0][3];
     $ResourceCode = $row[0][4];
     $Title = $row[0][5];
     $EnglishName = $row[0][6];
     $ChineseName = $row[0][7];
     $UserEmail = $row[0][8];
     $slots = $row[0][9];
     $recordType = $row[0][10];
     $typeValues = $row[0][11];
     $batchID = $row[0][12];
     $UID = $row[0][13];
     $type_str = $lr->returnTypeString($recordType,$typeValues);
     $slot_str = $lr->returnSlotsString($slots,$batchID,"\n");
     $dates_str = implode(",\n",$target);
     
     if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
     {
     	$mailBody = periodic_booking_approve_body($ResourceCategory, $ResourceCode, $Title, $start,$end,$slot_str,$type_str, $dates_str, $EnglishName, $ChineseName,$Remark);
     	//$mailTo = $UserEmail;
     	//$lu = new libsendmail();
     	//$lu->do_sendmail($webmaster, "", $mailTo, "", $approveMailSubject, $mailBody);
     	$lwebmail = new libwebmail();
     	$SuccessToArray = array();
     	$SuccessToArray[] = $UID;
     	$lwebmail->sendModuleMail($SuccessToArray,$approveMailSubject,$mailBody,1);
     }
      

     if (sizeof($crashed)!= 0)
     {
	     # Grab information and send reject mail
	     $fields_name = "a.BookingDate, b.Title, a.Remark, c.ResourceCategory, c.ResourceCode, c.Title, d.EnglishName, d.ChineseName, d.UserEmail, d.UserID";
	     $db_tables = "INTRANET_BOOKING_RECORD as a, INTRANET_SLOT as e, INTRANET_RESOURCE as c, INTRANET_USER as d";
	     $join_conds = "a.TimeSlot = e.SlotSeq AND e.BatchID = c.TimeSlotBatchID AND c.ResourceID = a.ResourceID AND a.UserID = d.UserID";
	     $conds = "BookingID IN ($crashed_list) AND PeriodicBookingID <> '$pid'";
	     $sql = "SELECT $fields_name FROM $db_tables WHERE $join_conds AND $conds";
	     $crashedRecords = $li->returnArray($sql,10);
	     for ($j=0; $j<sizeof($crashedRecords); $j++)
	     {
	          $bdate = $crashedRecords[$j][0];
	          $slot = $crashedRecords[$j][1];
	          $Remark = $crashedRecords[$j][2];
	          $ResourceCategory = $crashedRecords[$j][3];
	          $ResourceCode = $crashedRecords[$j][4];
	          $Title = $crashedRecords[$j][5];
	          $EnglishName = $crashedRecords[$j][6];
	          $ChineseName = $crashedRecords[$j][7];
	          $UserEmail = $crashedRecords[$j][8];
	          $UID = $crashedRecords[$j][9];
	          
	          if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
	          {
	          	$mailBody = booking_cancel_body($bdate, $slot, $Remark, $ResourceCategory, $ResourceCode, $Title, $EnglishName, $ChineseName);
	          	//$mailTo = $UserEmail;
	          	//$lu = new libsendmail();
	          	//$lu->do_sendmail($webmaster, "", $mailTo, "", $cancelSingleMailSubject, $mailBody);
	          	$lwebmail = new libwebmail();
	          	$CancelToArray = array();
	          	$CancelToArray[] = $UID; 
	          	$lwebmail->sendModuleMail($CancelToArray, $cancelSingleMailSubject, $mailBody, 1);	
	          }
	     }
     }
}
intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&pageNo=$pageNo&filter2=$filter2&filter3=$filter3&msg=2");
?>