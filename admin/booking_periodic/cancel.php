<?php
/*
 *  2019-05-14 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 *
 */

include("../../includes/global.php");
include("../../includes/libdb.php");
include("../../includes/librb.php");
include("../../includes/libemail.php");
include("../../includes/libsendmail.php");
include("../../includes/libwebmail.php");
include("../../lang/email.php");
intranet_opendb();

$li = new libdb();
$lr = new librb();

$RecordStatus = 5;
$list = implode(",",$PeriodicBookingID);
$sql = "UPDATE INTRANET_PERIODIC_BOOKING SET RecordStatus = '$RecordStatus', LastAction = now() WHERE PeriodicBookingID IN ($list)";
$li->db_db_query($sql);

$sql = "UPDATE INTRANET_BOOKING_RECORD SET RecordStatus = '$RecordStatus', LastAction = now() WHERE PeriodicBookingID IN ($list) AND BookingDate >= CURDATE() AND RecordStatus NOT IN(5)";
$li->db_db_query($sql);

$mailSubject = periodic_booking_cancel_title();
# Call sendmail function
for($i=0; $i<sizeof($PeriodicBookingID); $i++){
     $pid = $PeriodicBookingID[$i];
     $fields_name = "a.BookingStartDate, a.BookingEndDate, a.Remark, c.ResourceCategory, c.ResourceCode, c.Title, d.EnglishName, d.ChineseName, d.UserEmail, a.TimeSlots, a.RecordType, a.TypeValues, c.TimeSlotBatchID, d.UserID";
     $db_tables = "INTRANET_PERIODIC_BOOKING as a, INTRANET_RESOURCE as c, INTRANET_USER as d";
     $join_conds = "c.ResourceID = a.ResourceID AND a.UserID = d.UserID";
     $conds = "a.PeriodicBookingID = '$pid'";
     $sql = "SELECT $fields_name FROM $db_tables WHERE $join_conds AND $conds";
     $row = $li->returnArray($sql,14);
     $start = $row[0][0];
     $end = $row[0][1];
     $Remark = $row[0][2];
     $ResourceCategory = $row[0][3];
     $ResourceCode = $row[0][4];
     $Title = $row[0][5];
     $EnglishName = $row[0][6];
     $ChineseName = $row[0][7];
     $UserEmail = $row[0][8];
     $slots = $row[0][9];
     $recordType = $row[0][10];
     $typeValues = $row[0][11];
     $batchID = $row[0][12];
     $UID = $row[0][13];
     $sql = "SELECT DISTINCT BookingDate FROM INTRANET_BOOKING_RECORD WHERE PeriodicBookingID = '$pid' AND BookingDate >= CURDATE()";
     $dates = $li->returnVector($sql);
     $dates_str = implode(",\n",$dates);
     $type_str = $lr->returnTypeString($recordType,$typeValues);
     $slot_str = $lr->returnSlotsString($slots,$batchID,"\n");
     
     if( (isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation']) && !$sys_custom['ResourceBooking']['DisableEmailNoticifcation']) || (!isset($sys_custom['ResourceBooking']['DisableEmailNoticifcation'])) )
     {
     	$mailBody = periodic_booking_cancel_body($ResourceCategory, $ResourceCode, $Title, $start,$end,$slot_str,$type_str, $dates_str, $EnglishName, $ChineseName,$Remark);
     	//$mailTo = $UserEmail;
     	//$lu = new libsendmail();
     	//$lu->do_sendmail($webmaster, "", $mailTo, "", $mailSubject, $mailBody);
     	$lwebmail = new libwebmail();
     	$CancelToArray = array();
	 	$CancelToArray[] = $UID; 
	 	$lwebmail->sendModuleMail($CancelToArray, $mailSubject, $mailBody, 1);
     }
}

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&pageNo=$pageNo&filter2=$filter2&filter3=$filter3&msg=2");
?>