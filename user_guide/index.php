<?php
include("../includes/global.php");
include("../includes/libdb.php");
include("../includes/libuser.php");
$setting40 = "../../eclass40/system/settings/settings.php";
if (file_exists($setting40)) {
    include($setting40);
}

$special_feature['eclass_backup'] = true;


$prodect = array();
// $plugin["platform"] = "KIS";

if (isset($plugin["platform"]) && $plugin["platform"] = "KIS") {
    $prodect[] = array("platform", $plugin["platform"]);
}

$prodect[] = array("ver", Get_IP_Version());

if (isset($plugin['eClassApp']) && $plugin['eClassApp']) {
    $prodect[] = array("eClassApp", $plugin['eClassApp']);
}

if (isset($plugin['eClassTeacherApp']) && $plugin['eClassTeacherApp']) {
    $prodect[] = array("eClassTeacherApp", $plugin['eClassTeacherApp']);
}

if (isset($plugin['eClassStudentApp']) && $plugin['eClassStudentApp']) {
    $prodect[] = array("eClassStudentApp", $plugin['eClassStudentApp']);
}

if (isset($sys_custom['HideeClass']) && $sys_custom['HideeClass']) {
    $prodect[] = array("HideeClass", $sys_custom['HideeClass']);
}

if (isset($plugin['FlippedChannels']) && $plugin['FlippedChannels']) {
    $prodect[] = array("FlippedChannels", $plugin['FlippedChannels']);
}

if (isset($plugin['power_board']) && $plugin['power_board']) {
    $prodect[] = array("power_board", $plugin['power_board']);
}

if (isset($plugin['power_concept']) && $plugin['power_concept']) {
    $prodect[] = array("power_concept", $plugin['power_concept']);
}

if (isset($sys_custom['enable_minigame_engine']) && $sys_custom['enable_minigame_engine']) {
    $prodect[] = array("enable_minigame_engine", $sys_custom['enable_minigame_engine']);
}

if (isset($plugin['power_speech']) && $plugin['power_speech']) {
    $prodect[] = array("power_speech", $plugin['power_speech']);
}

if (isset($plugin['power_speech_expire']) && $plugin['power_speech_expire'] >= $today) {
    $prodect[] = array("power_speech_expire", $plugin['power_speech_expire']);
}

if (isset($plugin['power_voice']) && $plugin['power_voice']) {
    $prodect[] = array("power_voice", $plugin['power_voice']);
}

if (isset($plugin_expiry['power_voice']) && $plugin_expiry['power_voice'] >= $today) {
    $prodect[] = array("power_voice_expiry", $plugin_expiry['power_voice']);
}

if (isset($plugin['power_rubrics']) && $plugin['power_rubrics']) {
    $prodect[] = array("power_rubrics", $plugin['power_rubrics']);
}

if (isset($plugin['power_peermarking']) && $plugin['power_peermarking']) {
    $prodect[] = array("power_peermarking", $plugin['power_peermarking']);
}

if (isset($plugin['power_movie']) && $plugin['power_movie']) {
    $prodect[] = array("power_movie", $plugin['power_movie']);
}

if (isset($plugin['power_rubrics']) && $plugin['power_rubrics']) {
    $prodect[] = array("power_rubrics", $plugin['power_rubrics']);
}

if (isset($plugin['digital_archive']) && $plugin['digital_archive']) {
    $prodect[] = array("digital_archive", $plugin['digital_archive']);
}

if (isset($plugin['DigitalChannels']) && $plugin['DigitalChannels']) {
    $prodect[] = array("DigitalChannels", $plugin['DigitalChannels']);
}

if (isset($plugin['DocRouting']) && $plugin['DocRouting']) {
    $prodect[] = array("DocRouting", $plugin['DocRouting']);
}

if (isset($plugin['attendancestaff']) && $plugin['attendancestaff']) {
    $prodect[] = array("attendancestaff", $plugin['attendancestaff']);
}

if (isset($plugin['attendancestudent']) && $plugin['attendancestudent']) {
    $prodect[] = array("attendancestudent", $plugin['attendancestudent']);
}

if (isset($plugin['eBooking']) && $plugin['eBooking']) {
    $prodect[] = array("eBooking", $plugin['eBooking']);
}

if (isset($plugin['Disciplinev12']) && $plugin['Disciplinev12']) {
    $prodect[] = array("Discipline", $plugin['Disciplinev12']);
}

if (isset($plugin['eEnrollment']) && $plugin['eEnrollment']) {
    $prodect[] = array("eEnrollment", $plugin['eEnrollment']);
}

if (isset($plugin['Inventory']) && $plugin['Inventory']) {
    $prodect[] = array("Inventory", $plugin['Inventory']);
}

if (isset($plugin['eLib']) && $plugin['eLib']) {
    $prodect[] = array("eLib", $plugin['eLib']);
}

if (isset($plugin['library_management_system']) && $plugin['library_management_system']) {
    $prodect[] = array("library_management_system", $plugin['library_management_system']);
}

if (isset($plugin['payment']) && $plugin['payment']) {
    $prodect[] = array("payment", $plugin['payment']);
}

if (isset($plugin['ePOS']) && $plugin['ePOS']) {
    $prodect[] = array("ePOS", $plugin['ePOS']);
}

if (isset($plugin['ePCM']) && $plugin['ePCM']) {
    $prodect[] = array("ePCM", $plugin['ePCM']);
}

if (isset($plugin['ReportCard']) && $plugin['ReportCard']) {
    $prodect[] = array("ReportCard", $plugin['ReportCard']);
}

if (isset($plugin['ReportCard2008']) && $plugin['ReportCard2008']) {
    $prodect[] = array("ReportCard2008", $plugin['ReportCard2008']);
}

if (isset($plugin['ReportCard_Rubrics']) && $plugin['ReportCard_Rubrics']) {
    $prodect[] = array("ReportCard_Rubrics", $plugin['ReportCard_Rubrics']);
}

if (isset($plugin['ReportCardKindergarten']) && $plugin['ReportCardKindergarten']) {
    $prodect[] = array("ReportCardKindergarten", $plugin['ReportCardKindergarten']);
}

if (isset($plugin['Sports']) && $plugin['Sports']) {
    $prodect[] = array("Sports", $plugin['Sports']);
}

if (isset($plugin['attendancelesson']) && $plugin['attendancelesson']) {
    $prodect[] = array("attendancelesson", $plugin['attendancelesson']);
}

if (isset($plugin['EmailMerge']) && $plugin['EmailMerge']) {
    $prodect[] = array("EmailMerge", $plugin['EmailMerge']);
}

if (isset($plugin['RepairSystem']) && $plugin['RepairSystem']) {
    $prodect[] = array("RepairSystem", $plugin['RepairSystem']);
}

if (isset($plugin['StudentDataAnalysisSystem']) && $plugin['StudentDataAnalysisSystem']) {
    $prodect[] = array("StudentDataAnalysisSystem", $plugin['StudentDataAnalysisSystem']);
}

if (isset($plugin['iPortfolio']) && $plugin['iPortfolio']) {
    $prodect[] = array("iPortfolio", $plugin['iPortfolio']);
}

if (isset($iportfolio_lp_version) && $iportfolio_lp_version) {
    $prodect[] = array("iportfolio_lp_version", $iportfolio_lp_version);
}

if (isset($plugin['eAdmission']) && $plugin['eAdmission']) {
    $prodect[] = array("eAdmission", $plugin['eAdmission']);
}

if ((isset($sys_custom['iPf']['JUPAS']) && $sys_custom['iPf']['JUPAS'])) {
    $prodect[] = array("iPf_jupas", $sys_custom['iPf']['JUPAS']);
}

if (isset($special_feature['eclass_backup']) && $special_feature['eclass_backup']) {
    $prodect[] = array("eclass_backup", $special_feature['eclass_backup']);

    if (intranet_phpversion_compare('5.4') == "LATER") {
        $prodect[] = array("version", "LATER");
    } else {
        $prodect[] = array("version", "SAME");
    }
}

if (isset($sys_custom['eClassApp']['enableReprintCard']) && $sys_custom['eClassApp']['enableReprintCard']) {
    $prodect[] = array("ReprintCard", $sys_custom['eClassApp']['enableReprintCard']);
}

if (isset($plugin['WebSAMS']) && $plugin['WebSAMS']) {
    $prodect[] = array("WebSAMS", $plugin['WebSAMS']);
}

if (isset($plugin['eSchoolPad_MDM']) && $plugin['eSchoolPad_MDM']) {
    $prodect[] = array("eSchoolPad_MDM", $plugin['eSchoolPad_MDM']);
}

if (isset($sys_custom['KIS_iCalendar']['EnableModule']) && $sys_custom['KIS_iCalendar']['EnableModule']) {
    $prodect[] = array("KIS_iCalendar", $sys_custom['KIS_iCalendar']['EnableModule']);
}

// debug_pr($prodect);
intranet_opendb();
$lu = new libuser($UserID);
$client = $lu->sessionKey;

$http = checkHttpsWebProtocol() ? 'https' : 'http';
intranet_closedb();
?>
<script>
    window.onload = function () {
        document.form1.submit();
    }
</script>

<form name="form1" method="POST" action="https://centralsupport.broadlearning.com/doc/help/portal/iresources/">

    <?php foreach ($prodect as $value) { ?>
        <input type="hidden" name="prodect[]" value="<?= $value[0] ?>">
        <input type="hidden" name="active[]" value="<?= $value[1] ?>">
    <?php } ?>
    <input type="hidden" name="web" value="<?= $http . "://" . $_SERVER["HTTP_HOST"] ?>">
    <input type="hidden" name="client" value="<?= $client ?>">
</form>