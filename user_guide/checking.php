<?php
include("../includes/global.php");
if (!class_exists('JSON_obj')) {
    include_once("../includes/json.php");
}

$_json = new JSON_obj;

$json = file_get_contents('php://input');

$json_data = $_json->decode($json);

if (substr(Get_IP_Version(), 0, 2) != "ej") {
    include("../../eclass40/system/settings/settings.php");
}

if ($json_data["product"] == substr(Get_IP_Version(), 0, 2) || $json_data["product"] == 'ip_4.1' || $json_data["product"] == 'ej_4.1') {
    $match = 1;
} else {
    $match = 0;
}

if ($json_data["product"] == "KIS" || $json_data["product"] == "Year_Transition_kis") {
    if (isset($plugin["platform"]) && $plugin["platform"] = "KIS") {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eClassParentApp" || $json_data["product"] == "eClassParentApp_kis") {
    if (isset($plugin['eClassApp']) && $plugin['eClassApp']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eClassTeacherApp" || $json_data["product"] == "eClassTeacherApp_kis") {
    if (isset($plugin['eClassTeacherApp']) && $plugin['eClassTeacherApp']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eClassStudentApp") {
    if (isset($plugin['eClassStudentApp']) && $plugin['eClassStudentApp']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "FlippedChannels") {
    if (isset($plugin['FlippedChannels']) && $plugin['FlippedChannels']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_board") {
    if (isset($plugin['power_board']) && $plugin['power_board']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_concept") {
    if (isset($plugin['power_concept']) && $plugin['power_concept']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "enable_minigame_engine") {
    if (isset($sys_custom['enable_minigame_engine']) && $sys_custom['enable_minigame_engine']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_speech") {
    if (isset($plugin['power_speech']) && $plugin['power_speech']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_voice") {
    if (isset($plugin['power_voice']) && $plugin['power_voice']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_rubrics") {
    if (isset($plugin['power_rubrics']) && $plugin['power_rubrics']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_peermarking") {
    if (isset($plugin['power_peermarking']) && $plugin['power_peermarking']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_movie") {
    if (isset($plugin['power_movie']) && $plugin['power_movie']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "power_rubrics") {
    if (isset($plugin['power_rubrics']) && $plugin['power_rubrics']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "digital_archive") {
    if (isset($plugin['digital_archive']) && $plugin['digital_archive']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "DigitalChannels" || $json_data["product"] == "DigitalChannels_kis") {
    if (isset($plugin['DigitalChannels']) && $plugin['DigitalChannels']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "DocRouting") {
    if (isset($plugin['DocRouting']) && $plugin['DocRouting']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "attendancestaff" || $json_data["product"] == "attendancestaff_kis") {
    if (isset($plugin['attendancestaff']) && $plugin['attendancestaff']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "attendancestudent" || $json_data["product"] == "attendancestudent_kis") {
    if (isset($plugin['attendancestudent']) && $plugin['attendancestudent']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eBooking" || $json_data["product"] == "eBooking_kis") {
    if (isset($plugin['eBooking']) && $plugin['eBooking']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "Discipline" || $json_data["product"] == "eDiscipline") {
    if (isset($plugin['Disciplinev12']) && $plugin['Disciplinev12']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eEnrollment") {
    if (isset($plugin['eEnrollment']) && $plugin['eEnrollment']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "Inventory" || $json_data["product"] == "barcode_reader" || $json_data["product"] == "Inventory_kis") {
    if (isset($plugin['Inventory']) && $plugin['Inventory']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

// if($json_data["product"] == "Inventory"){
//     if(isset($plugin['eLib']) && $plugin['eLib']){
//         $prodect[] = array("eLib", $plugin['eLib']);
//     }
// }

if ($json_data["product"] == "eLib+" || $json_data["product"] == "eLib+_kis") {
    if (isset($plugin['library_management_system']) && $plugin['library_management_system']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "payment" || $json_data["product"] == "card_reader" || $json_data["product"] == "ePayment_3" || $json_data["product"] == "ePayment_2" || $json_data["product"] == "payment_kis") {
    if (isset($plugin['payment']) && $plugin['payment']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "ePOS" || $json_data["product"] == "ePOS_program" || $json_data["product"] == "ePOS_kis") {
    if (isset($plugin['ePOS']) && $plugin['ePOS']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "ePCM") {
    if (isset($plugin['ePCM']) && $plugin['ePCM']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "ReportCard") {
    if (isset($plugin['ReportCard']) && $plugin['ReportCard']) {
        $match = 1;
    } else if (isset($plugin['ReportCard2008']) && $plugin['ReportCard2008']) {
        $match = 1;
    } else if (isset($plugin['ReportCard_Rubrics']) && $plugin['ReportCard_Rubrics']) {
        $match = 1;
    } else if (isset($plugin['ReportCardKindergarten']) && $plugin['ReportCardKindergarten']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "Sports") {
    if (isset($plugin['Sports']) && $plugin['Sports']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "attendancelesson") {
    if (isset($plugin['attendancelesson']) && $plugin['attendancelesson']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "EmailMerge") {
    if (isset($plugin['EmailMerge']) && $plugin['EmailMerge']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "RepairSystem") {
    if (isset($plugin['RepairSystem']) && $plugin['RepairSystem']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "SDAS") {
    if (isset($plugin['StudentDataAnalysisSystem']) && $plugin['StudentDataAnalysisSystem']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "iPortfolio" || $json_data["product"] == "iPortfolio_tools" || $json_data["product"] == "iPortfolio_kis") {
    if (isset($plugin['iPortfolio']) && $plugin['iPortfolio']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eAdmission") {
    if (isset($plugin['eAdmission']) && $plugin['eAdmission']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "iPortfolio_tools_jupas") {
    if ((isset($sys_custom['iPf']['JUPAS']) && $sys_custom['iPf']['JUPAS'])) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "ip_backup" || $json_data["product"] == "ej_backup") {
    if (isset($special_feature['eclass_backup']) && $special_feature['eclass_backup']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "reprint_card") {
    if (isset($plugin['payment']) && $plugin['payment'] && isset($sys_custom['eClassApp']['enableReprintCard']) && $sys_custom['eClassApp']['enableReprintCard']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "WebSAMS_Queries" || $json_data["product"] == "WebSAMS_eClass") {
    if (isset($plugin['WebSAMS']) && $plugin['WebSAMS']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eSchoolPad_MDM" || $json_data["product"] == "eSchoolPad_MDM_DEP") {
    if (isset($plugin['eSchoolPad_MDM']) && $plugin['eSchoolPad_MDM']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

if ($json_data["product"] == "eAttendance") {
    if (isset($plugin['attendancestudent']) && $plugin['attendancestudent'] && isset($plugin['attendancestaff']) && $plugin['attendancestaff']) {
        $match = 1;
    } else {
        $match = 0;
    }
}

// if(isset($sys_custom['KIS_iCalendar']['EnableModule']) && $sys_custom['KIS_iCalendar']['EnableModule']){
//     $prodect[] = array("KIS_iCalendar", $sys_custom['KIS_iCalendar']['EnableModule']);
// }

echo $match;
?>