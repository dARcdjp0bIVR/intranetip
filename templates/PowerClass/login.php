<?php 
## Using By :   

/********************** Change Log ***********************/
#
#   Date:       2020-05-06 Tommy
#               added flag $sys_custom['PowerClass_GWP'] checking for google login 
#
#   Date:       2020-01-16
#               Redirect to StemPL2Client login page if $sys_custom['StemPL2Client'] is true
#
#	Date:		2017-04-06
#				LivingHomeopathy Login page
/********************** Change Log ***********************/
include_once("../../includes/global.php");
include_once("../../includes/SecureToken.php");
if (is_file("$intranet_root/servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{
	header("Location: ../servermaintenance.php");
	exit();
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'en';
}
$PATH_WRT_ROOT_ABS = "../../";

if (!$sys_custom["PowerClass"]) {
	header("Location: ../../login.php");
	exit;
} elseif($sys_custom['StemPL2Client']) {
    header("Location: ../StemPL2Client/login.php");
    exit;
}
$customLangVar = $sys_custom['Project_Label'];
if (file_exists($PATH_WRT_ROOT_ABS . "lang/lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT_ABS . "lang/lang.$intranet_session_language.php");
if (file_exists($PATH_WRT_ROOT_ABS . "lang/" . $customLangVar. "/pc_lang.$intranet_session_language.php")) include_once($PATH_WRT_ROOT_ABS . "lang/" . $customLangVar. "/pc_lang.$intranet_session_language.php");

intranet_opendb();
$SecureToken = new SecureToken();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $Lang["PageTitle"]; ?></title>
	<link href="css/main.css" rel="stylesheet" type="text/css">
	<link href="css/login.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/custom.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" src="<?php echo $PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.4.4.min.js"></script>
	<script language=JavaScript src="<?php echo $PATH_WRT_ROOT_ABS?>lang/script.<?=$title_lang?>.js"></script>
	<script language=JavaScript1.2 src="<?php echo $PATH_WRT_ROOT_ABS?>templates/script.js"></script>
	<script src="js/script.js"></script>
	<link id="page_favicon" href="/templates/PowerClass/images/favicon.gif?v=3" rel="shortcut icon" type="image/gif" />
	<meta name="viewport" content="width=device-width">
	<script language="javascript">
	function signInWithGoogle(){
		location.href = "../login.googlesso.php";
	}
	
	var ForgotPasswordMsg = "<?php echo $Lang[$customLangVar]["Login"]["ForgotPasswordMsg"]; ?>";
	
	function checkLoginForm(){
		$( "#login_btn" ).attr( "disabled", true );
		$('.invaidMsg').css('display','none');
		var obj = document.form1;
		var pass=1;
		
		if(!checkLoginInput(document.getElementById('UserLogin'), "<?php echo $Lang[$customLangVar]["Login"]["LoginID_alert"]; ?>", 'lblInvaidId'))
			pass  = 0;
		else if(!checkLoginInput(document.getElementById('UserPassword'), "<?php echo $Lang[$customLangVar]["Login"]["Password_alert"]; ?>", 'lblInvaidPw'))
			pass = 0;
		
		if(pass)	
		{
			$('span#loading').show();
			return true;
		}
		$( "#login_btn" ).attr( "disabled", false );
		return false;
	}
	
	function checkForgetForm(){
	     obj = document.form2;
	     tmp = prompt(ForgotPasswordMsg, "");
	     if(tmp!=null && Trim(tmp)!=""){
	          obj.UserLogin.value = tmp;
	          obj.submit();
	     }
	}

	function checkLoginInput(f, msg, t){
		 if(Trim(f.value)==""){
             $('#'+t).html(msg);
             $('#'+t).css('display','block');
             f.value="";
             f.focus();
             return false;
	     }else{
	         return true;
	     }
	}
	</script>
</head>
<body class="font-medium  CustCommon CustMainContent PowerClassPortal">
<div id="blkContainer">
	<span id="blkLoginWrapper">
		<form name="form1" action="../../login.php" method="post" onSubmit="return checkLoginForm();">
		<?=$SecureToken->WriteFormToken()?>
		<img alt="logo" src="images/logo.png">
		<div id="blkLogin">
			<?php if($err==1 || $err==2) { ?><div class="invaidMsg" id="lblInvaidId">
			<?php echo $Lang[$customLangVar]["Login"]["LoginID_err"]; ?>
			</div><?php } ?>
			<div class="marginB10">
				<?php echo $Lang[$customLangVar]['Login']['LoginID']; ?>
				<span class="invaidMsg" id="lblInvaidId" style="display:none;">
					<?=$Lang['PowerClass']["Login"]["LoginID_err"]?>
				</span>
			</div>			
			<input class="user_loginname" type="text" name="UserLogin" id="UserLogin"/>
			
			<div class="marginB10">
				<?php echo $Lang[$customLangVar]["Login"]["Password"]; ?>
				<span class="invaidMsg" id="lblInvaidPw" style="display:none;">
				</span>
			</div>
			<input class="user_loginname" type="password" name="UserPassword" id="UserPassword" />
			
			<input name="submit1"  type="submit" class="login_button pointer" id="login_btn" value="<?php echo $Lang[$customLangVar]["Login"]["Login"]; ?>" class="login_button"/>
                <input type="hidden" name="url" value="/templates/index.php?err=1&DirectLink=<?=rawurlencode($DirectLink)?>">
			<input type="hidden" name="AccessLoginPage" value="<?=$AccessLoginPage?>">
				
				<?php if($msg==1) { ?><div class="done_msg"><?=$Lang['LoginPage']['RequestSent']?></div>
               		<?php } else { ?><a href="javascript:checkForgetForm();" class="forgot_password"><?php echo $Lang[$customLangVar]['Login']['ForgotPassword']; ?></a><br><?php } ?>
		</div>
		<div id="lbleClass"><span id="lblBL">BroadLearning</span><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="images/eClassLogo.png"></a></div>
		<?php if($ssoservice["Google"]["Valid"] && !$sys_custom['PowerClass_GWP']){ ?>
		<div id="blkLoginDivider">
			<span></span><span>or</span><span></span>
		</div>		
      		<input id="btnGoogleSignin" type="button" value="" onclick="signInWithGoogle()" />
      	<?php } ?>
		</form>
	</span>
</div>
<form name=form2 action=../../forget.php method=post>
<input type=hidden name=UserLogin>
<input type=hidden name=url_success value="/templates/<?php echo $customLangVar; ?>/login.php?msg=1">
<input type=hidden name=url_fail value="/templates/<?php echo $customLangVar; ?>/login.php?err=2">
<?=$SecureToken->WriteFormToken()?>
</form>
	
<SCRIPT language=javascript>
function openForgetWin () {
win_size = "resizable,scrollbars,status,top=40,left=40,width=650,height=600";
url = null;
if (url != null && url != "") 
{
    var forgetWin = window.open(url, 'forget_password', win_size);
    if (navigator.appName=="Netscape" && navigator.appVersion >= "3") forgetWin.focus();
}
}

document.getElementById('UserLogin').focus();
<?php if($msg==1) { ?>openForgetWin();<?php } ?>
</script>
</body>
</html>
