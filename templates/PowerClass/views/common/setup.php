<?php
## Using By : 

/********************** Change Log ***********************/
/*
 *  Date:       2020-05-18 Tommy
 *              hide Timetable, eClassroom, Subject when $sys_custom['PowerClass_GWP'] is on
 * 
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *
 */

/********************** Change Log ***********************/

$order_no = 0;

include_once('iframeHead.php');	
?>
<body>
	<div class=" CustCommon CustMainContent PowerCLassPortal">
	<div class="pageHeader">
						<div class="backButton font-light pointer" onclick="window.history.go(-1); return false;">
							<?php echo $Lang["PoserClass"]["BtnBack"];?>
						</div>
						<div class="pageSubject font-bold">
							<?php echo $Lang['Header']['Menu']['SystemSetting']?>
						</div>
					</div>
		<div id="blkModuleContainer" class="<?= $menuBgcolorClass?>">
			<?php 
			if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]){
				$order_no++;
			?>
			<div class="accordion">
				<div id="lblPart1"><?=$order_no?></div>
				<div class="accordion-subject"><?php echo $Lang["PowerClass"]["School"]; ?></div>
			</div>
			<div class="accordion-page">
				<div class="portalIconsContainer1">
				<?php if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])){?>
					<span id="btnSchool-SchoolYear" class="portalIcon">
						<a class="portalIcon_img" href='/home/system_settings/school_calendar/academic_year.php'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href='/home/system_settings/school_calendar/academic_year.php'><?php echo $Lang["PowerClass"]["SchoolYearAndTerm"]; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]){?><span
					id="btnSchool-Form" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/form_class_management/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/form_class_management/"><?php echo $Lang["PowerClass"]["Class"]; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) && !$plugin["Disable_Subject"]){?><span
					id="btnSchool-Subject" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/subject_class_mapping/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/subject_class_mapping/"><?php echo $Lang["PowerClass"]["Subject"]; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]){?><span
					id="btnSchool-Group" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/group/?clearCoo=1"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/group/?clearCoo=1"><?php echo $Lang["PowerClass"]["Group"]; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php /*<span
					id="btnSchool-Role" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/role_management/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/role_management/"><?php echo $Lang["PowerClass"]["Role"]; ?></a></div>
							</span>
						</div>
					</span> */ ?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $sys_custom["SchoolCalendarWhenNotAdminReadOnly"]){?><span
					id="btnSchool-Events" class="portalIcon">
						<a class="portalIcon_img" href='/home/system_settings/school_calendar/'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href='/home/system_settings/school_calendar/'><?php echo $Lang["PowerClass"]["HolidayAndEvents"]; ?><br>(<?php echo $Lang["PowerClass"]["SchoolCalendar"]; ?>)</a></div>
							</span>
						</div>
					</span><?php }?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){?><span
					id="btnSchool-Security" class="portalIcon">
						<a class="portalIcon_img" href='/home/system_settings/security/password_policy.php'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href='/home/system_settings/security/password_policy.php'><?php echo $Lang["PowerClass"]["SchoolSecurity"]; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] || $sys_custom["SchoolCalendarWhenNotAdminReadOnly"]){?><span
					id="btnSchool-Timezone" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/school_calendar/time_zone.php"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/school_calendar/time_zone.php"><?php echo $Lang['SysMgr']['CycleDay']['SettingTitle'] ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) && !$plugin["Disable_Timetable"]){?><span
					id="btnSchool-Timetable" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/timetable/index.php"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/timetable/index.php"><?php echo $Lang['SysMgr']['Timetable']['ModuleTitle']; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){?><span
					id="btnSchool-Role" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/role_management/index.php"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/role_management/index.php"><?php echo $Lang['Header']['Menu']['Role']; ?></a></div>
							</span>
						</div>
					</span><?php }?><?php if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]){?><span
					id="btnSchool-Campus" class="portalIcon">
						<a class="portalIcon_img" href="/home/system_settings/location/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/system_settings/location/"><?php echo $Lang['Header']['Menu']['Site']; ?></a></div>
							</span>
						</div>
					</span> <?php }?>
				</div>
			</div>
			<?php
			}
			?>
			<?php 
			if($_SESSION['SSV_USER_ACCESS']['eAdmin-AccountMgmt']){
				$order_no++;
			?>
			<div class="accordion">
				<div id="lblPart2"><?=$order_no?></div>
				<div class="accordion-subject"><?php echo $Lang["PowerClass"]["Account"]; ?></div>
			</div>
			<div class="accordion-page">
				<div class="portalIconsContainer1">
					<span id="btnAccount-Staff" class="portalIcon">
						<a class="portalIcon_img" href='/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1'></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1"><?php echo $Lang["PowerClass"]["Staff"]; ?></a></div>
								<div id="lblParentNo" class="portalIcon_data"><?php if ($NumberOfStaff > 0) echo $NumberOfStaff; ?></div>
							</span>
						</div>
					</span><span
					id="btnAccount-Student" class="portalIcon">
						<a class="portalIcon_img" href="/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1"><?php echo $Lang["PowerClass"]["Student"]; ?></a></div>
								<div id="lblStudentNo" class="portalIcon_data">
									<?php /* if ($NumberOfStudent > 0) { ?><span id="lblStudentNo" class="portalIcon_data"><?php echo $NumberOfStudentAvailTotal; ?></span><span class="portalIcon_subInfo">&nbsp;<!-- /&nbsp;<span id="lblStudentQuota"><?php echo $NumberOfStudent; ?></span> --></span><?php } */?>
									<?php echo $NumberOfStudentUsed; ?>
								</div>
							</span>
						</div>
					</span><span
					id="btnAccount-StudentPhotos" class="portalIcon">
						<a class="portalIcon_img" href="/home/eAdmin/AccountMgmt/StudentMgmt/photo/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/StudentMgmt/photo/"><?php echo $Lang["PowerClass"]["StudentApp_OfficalPhoto"]; ?></a></div>
							</span>
						</div>
					</span><span
					id="btnAccount-Parent" class="portalIcon">
						<a class="portalIcon_img" href="/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1"><?php echo $Lang["PowerClass"]["Parent"]; ?></a></div>
								<div id="lblStaffNo" class="portalIcon_data"><?php if ($NumberOfParent > 0) echo $NumberOfParent; ?></div>
							</span>
						</div>
					</span>
				</div>
			</div>
			<?php 
			}
			?>
			<?php
			if($_SESSION['SSV_USER_ACCESS']["eLearning-eClass"] && !$plugin["Disable_eClassroom"]){
				$order_no++;
			?>
			<div class="accordion">
				<div id="lblPart3"><?=$order_no?></div>
				<div class="accordion-subject"><?php echo $Lang["PowerClass"]["eClass"]; ?></div>
			</div>
			<div class="accordion-page">
				<div class="portalIconsContainer1">
					<span id="btnClass-eClassroom" class="portalIcon">
						<a class="portalIcon_img" href="/home/eLearning/eclass/organize/"></a>
						<div class="portalIcon_text">
							<span>
								<div><a href="/home/eLearning/eclass/organize/"><?php echo $Lang["PowerClass"]["eClassroom"]; ?></a></div>
							</span>
						</div>
					</span>
				</div>
			</div>
			<?php
			}
			?>
		</div>
		<?php echo $footerHtml?>
	</div>
</html>