<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $Lang["PageTitle"]; ?></title>
	<link href="<?php echo $source_path; ?>css/main.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portal.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portalIcon.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $powerClass_source_path; ?>css/common.css" rel="stylesheet" type="text/css">
	<?php if($project_identify!="PowerClass"){ ?> 
	    <link href="<?php echo $powerClass_source_path ; ?>css/customTemplate.css" rel="stylesheet" type="text/css">
	<?php } ?>
	<script src="/templates/jquery/jquery-3.3.1.min.js"></script>
	<script src="/templates/jquery/jquery-ui-1.12.1.js"></script>
	<script src="<?php echo $source_path; ?>js/script.js"></script>
	<link rel="shortcut icon" href="<?php echo $source_path; ?>images/favicon.png" type="image/x-icon">
</head>