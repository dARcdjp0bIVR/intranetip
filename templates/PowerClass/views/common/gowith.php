<?php 
## Using By : 

/********************** Change Log ***********************/
/*
 *  Date:       2020-05-06 Tommy
 *              created this as the common template file
 *
 */
/********************** Change Log ***********************/

$_SESSION['PowerClass_PAGE'] = "gowith";

include_once('iframeHead.php');	
?>
<body>
	<div class="container">
		<div class="CustCommon CustMainContent">
					<!--
			<div id="blkTab" class="font-light">
				<div>
					<ul>
						<li>
							<a href="App.html"><span class="tab_current">App</span></a>
						</li><li>
							<a><span>PowerLesson</span></a>
						</li><li>
							<a href="Analytics.html"><span>Analytics</span></a>
						</li><li id="blkApps">
							<span>
								<img src="../../images/icon-iCal.png">
							</span><span id="btnGoogleApps">
								<img src="../../images/icon-apps.png">
							</span>
						</li>
					</ul>
				</div>
			</div>
			-->
			<div class="wrapper">
				<!--
					Replace #custFrameBody with an iFrame
					Place .pageHeader, #blkModuleContainer & #lbleClass inside the iFrame
					Place module in #blkModuleContainer
				-->
				<div id="custFrameBody">
					<div id="blkModuleContainer" class="bgColor-2">
						<!-- Go With message -->
						<div id="blkGoWith">
							<div id="blkGoWith-Img">
								<div id="blkGoWith-Change">
									<img id="imgGoWith-Change" src="/templates/PowerClass/images/goWith-change.png" alt="">
								</div>
								<div id="blkGoWith-App">
									<img id="imgGoWith-App" src="/templates/PowerClass/images/goWith-app.png" alt="">
									<?php if($ParentOrStudent == 1){?>
									<img id="imgGoWith-ParentApp" src="/templates/PowerClass/images/appIcon-parent.png" alt="">
									<?php }else{?>
									<img id="imgGoWith-StudentApp" src="/templates/PowerClass/images/appIcon-student.png" alt="">
									<?php }?>
								</div>
							</div>
							<div><?=$msg?></div>
							<!-- Show below span in App Webview / mobile browsers only -->
						<!-- <span class="link"><span>打開 Parent App</span><span class="fa fa-arrow-right"></span></span><!-- 打開 Student App --> 
						</div>

					</div>
					<div id="lbleClass"><span id="lblBL">BroadLearning</span><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="/templates/PowerClass/images/eClassLogo.png"></a></div>
				</div>
			</div>
			<!--Google Apps-->
			<div id="blkGoogleApps" class="font-light hide">
				<ul>
					<li class="shortcut-google-gmail">
						<img src="/templates/PowerClass/images/google/gmail.png">
						<div><a>Gmail</a></div>
					</li><li class="shortcut-google-drive">
						<img src="/templates/PowerClass/images/google/drive.png">
						<div><a>Drive</a></div>
					</li><li class="shortcut-google-classroom">
						<img src="/templates/PowerClass/images/google/classroom.png">
						<div><a>Classroom</a></div>
					</li><li class="shortcut-google-docs">
						<img src="/templates/PowerClass/images/google/docs.png">
						<div><a>Docs</a></div>
					</li><li class="shortcut-google-sheets">
						<img src="/templates/PowerClass/images/google/sheets.png">
						<div><a>Sheets</a></div>
					</li><li class="shortcut-google-slides">
						<img src="/templates/PowerClass/images/google/slides.png">
						<div><a>Slides</a></div>
					</li>
				</ul>
				<a class="font-light" href="">More</a>
			</div>

			<script>
			$(document).ready(function()
			{
				$("#btnGoogleApps").click(function()
				{
					$("#blkGoogleApps").toggleClass("hide");
				});
			});
			</script>
		</div>
	</div>
</body>
