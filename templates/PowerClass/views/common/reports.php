<?php
## Using By :

/********************** Change Log ***********************/
/*
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *              
 *  Date:       2017-12-20 Cameron
 * 		        set overflow:visible for eInventory so that it won't hide partial title name
 *
 */

/********************** Change Log ***********************/

	session_start();
	$_SESSION['PowerClass_PAGE'] = "reports";
	
	include_once('iframeHead.php');	
?>

<body>
	<div class="CustCommon CustMainContent PowerCLassPortal">
		<div id="blkModuleContainer" class="bgColor-1">
		<!--3 in a row, use .portalIconsContainer1-->
		<!--2 in a row, use .portalIconsContainer3-->
			<div class="portalIconsContainer<?= $containerNo ?>">
				<?php
				foreach($app_details as $key=>$app){
					if(!is_array($app))continue;
				?>
				<span id="" class="portalIcon portalIcon-<?=$app['icon_class']?>">
					<div class="portalIcon_img">
					</div>
					<div class="portalIcon_text" <?=($app['icon_class'] == 'eInventory') ? 'style="overflow:visible"' : ''?>>					
						<span>
							<div><a href="<?=$app['href']?>"><?=$app['Title']?></a></div>
						</span>
					</div>
				</span>
				<?php
				}
				?>
			</div>
		</div>
		<?php echo $footerHtml?>
	</div>
</body>
</html>