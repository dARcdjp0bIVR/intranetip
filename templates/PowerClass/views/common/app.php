<?php 
## Using By :

/********************** Change Log ***********************/
/*
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *
 */

/********************** Change Log ***********************/

	session_start();
	$_SESSION['PowerClass_PAGE'] = "app";
	
	include_once('iframeHead.php');	
?>
<body>
		<div id="blkModuleContainer" class="bgColor-2 CustCommon CustMainContent PowerCLassPortal">
			<table id="tblApp">
				<tr>
					<td>
						<div id="blkAppIconsHeader" class="bgColor-1">
							<a id="btnAppSettings" class="pointer" href='app_settings.php'>
								<?php echo $Lang["PowerClass"]["Settings"]; ?>
							</a>
						</div>
						<div class="portalIconsContainer2">
							<?php 
							foreach($app_details as $key => $app){
								if(!is_array($app))continue;
							?>
							<span class="portalIcon portalIcon-<?=$app['icon_class']?>">
								<a class="portalIcon_img" href="<?=$app['home_href']?>"></a>
								<div class="portalIcon_text">
									<span>
										<div><a href="<?=$app['home_href']?>"><?=$app['Title']?></a></div>
										<?php 
										if($app['Add']['isGroup']){
										?>
										<div class="portalIcon_subInfo">
											<div class="addButton-group">
												<div class="addButton">
													<?php echo $app['Add']['Top']; ?>
												</div>
												<?php 
												foreach($app['Add']['List'] as $appAddBtn){
												?>
												<div class="addButton collapse">
													<a href="<?=$appAddBtn['Link']?>"><?php echo $appAddBtn["Lang"]; ?></a>
												</div>
												<?php
												}?>
											</div>
										</div>
										<?php 
										}else{
										?>
										<div class="addButton">
											<a href="<?=$app['Add']['Link']?>"><?=$app['Add']['Lang']?></a>
										</div>
										<?php 
										}
										?>	
									</span>
								</div>
							</span>
							<?php	
							}
							?>
						</div>
					</td>
					<td id="tblApp-divider">
					</td>
					<td>
						<div id="blkAppTab" class='hasChart'>
						<ul>
							<li><a href="#blkAppTab-Today"><?php echo $Lang["PowerClass"]["cht_Today"]; ?></a></li>
							<li><a href="#blkAppTab-Yesterday"><?php echo $Lang["PowerClass"]["cht_Yesterday"]; ?></a></li>
							<li><a href="#blkAppTab-Week"><?php echo $Lang["PowerClass"]["cht_Week"]; ?></a></li>
							<li><a href="#blkAppTab-Month"><?php echo $Lang["PowerClass"]["cht_Month"]; ?></a></li>
						</ul>
						<div id="blkAppTab-Today">
							<div class='chrt_body'>
								<div id='graph' class='graph_body'></div>
							</div>
						</div>
						<div id="blkAppTab-Yesterday">
							<div class='chrt_body'>
								<div id='yesterday_graph' class='graph_body'></div>
							</div>
						</div>
						<div id="blkAppTab-Week">
							<div class='chrt_body'>
								<div id='week_graph' class='graph_body'></div>
						  	</div>
						</div>
						<div id="blkAppTab-Month">
							<div class='chrt_body'>
								<div id='month_graph' class='graph_body'></div>
						  	</div>
						</div>
					</td>
				</tr>
			</table>
			<?php echo $footerHtml?>
		</div>
		<!-- <div id="lbleClass"><span id="lblBL">BroadLearning</span><span><?php echo $Lang['Footer']['PowerBy']; ?></span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="<?php echo $source_path; ?>images/eClassLogo.png"></a></div> -->
<script language="javascript">
var $lang = [];
$lang["graphTitle"] = "<?php echo $Lang["PowerClass"]["cht_loginrecord"]; ?>";
$lang["NumberOfAccess"] = "<?php echo $Lang["PowerClass"]["NumberOfAccess"]; ?>";
$lang["BtnBack"] = "<?php echo $Lang["PowerClass"]["BtnBack"]; ?>";
$lang["label"] = [];
$lang["label"][0] = "<?php echo $Lang["PowerClass"]["cht_Today"]; ?>";
$lang["label"][1] = "<?php echo $Lang["PowerClass"]["cht_Yesterday"]; ?>";
var data = [];
data[0] = [];
<?php if (isset($dhl_statInfo["today"]["categories"])) {?>
data[0]["categories"] = ['<?php echo implode("', '", $dhl_statInfo["today"]["categories"]); ?>'];
data[0]["data"] = [<?php echo implode(", ", $dhl_statInfo["today"]["data"]); ?>];
<?php } else { ?>
data[0]["categories"] = [];
data[0]["data"] = [];
<?php } ?>
data[1] = [];
<?php if (isset($dhl_statInfo["yesterday"]["categories"])) {?>
data[1]["categories"] = ['<?php echo implode("', '", $dhl_statInfo["yesterday"]["categories"]); ?>'];
data[1]["data"] = [<?php echo implode(", ", $dhl_statInfo["yesterday"]["data"]); ?>];
<?php } else { ?>
data[1]["categories"] = [];
data[1]["data"] = [];
<?php } ?>
<?php 
if (isset($dhl_statInfo["weekday"]["categories"])) {
	$strDate = "'" . implode("', '", $dhl_statInfo["weekday"]["categories"]) . "'";
	$strValue = implode(", ", $dhl_statInfo["weekday"]["data"]);
} else {
	$strDate = "";
	$strValue = "";
}
?>
data[2] = [];
data[2]["categories"] = [<?php echo $strDate; ?>];
data[2]["data"] = [<?php echo $strValue; ?>];
<?php 
if (isset($dhl_statInfo["month"]["categories"])) {
	$strDate = "'" . implode("', '", $dhl_statInfo["month"]["categories"]) . "'";
	$strValue = implode(", ", $dhl_statInfo["month"]["data"]);
} else {
	$strDate = "";
	$strValue = "";
}
?>
data[3] = [];
data[3]["categories"] = [<?php echo $strDate; ?>];
data[3]["data"] = [<?php echo $strValue; ?>];

chartJS.cfn.setLang($lang);
chartJS.cfn.createChart(data);
chartJS.cfn.init();
</script>
</body>
</html>