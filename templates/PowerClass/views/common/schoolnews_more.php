<?php 
## Using By :

/********************** Change Log ***********************/
/*
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *
 */

/********************** Change Log ***********************/


	session_start();
	$_SESSION['PowerClass_PAGE'] = "schoolnews";
	
	include_once('iframeHead.php');	
?>
<body>
	<div class="CustCommon CustMainContent PowerCLassPortal" id="eclass_main_frame_table">
		<div class="pageHeader" id="header">
			<div class="header_title">
				<span id="module_title" class="menu_opened "><?php echo $Lang["PowerClass"]["SchoolNews"]; ?></span>
			</div>
		</div>
		<div id="blkModuleContainer" class="bgColor-2">
			<div id="schoolNewsContent" class="bgColor-1">
			<table width="99%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                    <td height="27"></td>
                    <td height="33" valign="middle" style="padding-left: 5px">
                      	
                    	<span class="contenttitle" style="vertical-align: bottom; "><?php echo $Lang["PowerClass"]["SchoolNews"]; ?></span>
                      	
                    </td>
                	<td height="27"></td>
                </tr>
				<tr>
					<td class="bgColor-1" width="1%"></td>
					<td class="main_content">
					<SCRIPT LANGUAGE=Javascript>
		
						function removeCat(obj,element,page){
								var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
						        if(countChecked(obj,element)==0)
						                alert(globalAlertMsg2);
						        else{
						                if(confirm(alertConfirmRemove)){
						                obj.action=page;
						                obj.method="post";
						                obj.submit();
						                }
						        }
							}
							
							function reloadForm() {
								document.form1.keyword.value = "";
								document.form1.pageNo.value = 1;
								document.form1.submit();
							}
						
							function viewNews(id)
							{
									 newWindow('view.php?AnnouncementID='+id,1);
							}
						
							function showRead(id)
							{
									 newWindow('read.php?AnnouncementID='+id,6);
							}
							
							function copy_news(id)
							{
								document.form1.action="copy.php";
								document.form1.copy_from.value = id;
								document.form1.submit();
							}
							
							function Check_Go_Search(evt)
							{
								var key = evt.which || evt.charCode || evt.keyCode;
								
								if (key == 13) // enter
								{
									document.form1.submit();
								}
								else
									return false;
							}
						</SCRIPT>
						
						<form name="form1" method="post" action="">
						
						<div class="content_top_tool">
							<div class="Conntent_search"><?=$searchTag?></div>
							<br style="clear:both" />
						</div>
						
						<div class="table_board">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td valign="bottom">
									<!-- Status //-->
									<div class="table_filter">
										<?=$selectedStatus?>
									</div>
								</td>
							</tr>
							</table>
						
						<?=$ltable->display()?>
						
						</div>
						
						<input type="hidden" name="pageNo" value="<?php echo $ltable->pageNo; ?>"/>
						<input type="hidden" name="order" value="<?php echo $ltable->order; ?>"/>
						<input type="hidden" name="field" value="<?php echo $ltable->field; ?>"/>
						<input type="hidden" name="numPerPage" value="<?=$ltable->page_size?>"/>
						<input type="hidden" name="page_size_change" value=""/>
						<input type="hidden" name="cid" value="<?=$childrenID?>"/>
						<input type="hidden" name="sid" value="<?=$subjectID?>"/>
					</form>
					</td>
					<td  class="bgColor-1"  width="1%"></td>
					</tr>
					<tr  class="bgColor-1">
						<td style="height:50px"></td>
						<td style="height:50px"></td>
						<td style="height:50px"></td>
					</tr>
				</table>
			</div>
			<?php echo $footerHtml;?>			
		</div>
	</div>	
</body>
</html>