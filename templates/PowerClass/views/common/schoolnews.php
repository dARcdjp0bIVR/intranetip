<?php 
## Using By :

/********************** Change Log ***********************/
/*
 *  Date:       2019-01-22 Isaac
 *              created this as the common template file
 *
 */

/********************** Change Log ***********************/


	session_start();
	$_SESSION['PowerClass_PAGE'] = "schoolnews";
	
include_once('iframeHead.php');	
?>
<body>
	<div class="CustCommon CustMainContent PowerCLassPortal">
		<div class="wrapper">
					
    				<!--
    					Replace #custFrameBody with an iFrame
    					Place .pageHeader, #blkModuleContainer & #lbleClass inside the iFrame
    					Place module in #blkModuleContainer
    				-->
    		<div id="custFrameBody">
					<div id="blkModuleContainer" class="<?= $containerBGColorClass ?>">
						<div id="lblSchoolNews" class="font-bold"><?php echo $Lang["PowerClass"]["SchoolNews"]; ?></div>
						<ul>
							<?php foreach($schoolnews as $news){ ?>
							<li class="schoolNews-item">
								<img src="<?php echo $source_path; ?>images/icon-news.png">
								<div>
									<div>
										<?php if($news[3]==1){?>
										<div class="update-label">New</div>
										<?php }?>
										<a class="schoolNews-subject" href="/home/view_announcement.php?AnnouncementID=<?=$news[0]?>" target="_blank"><?php echo $news[2]; ?></a>
									</div>
									<div class="schoolNews-content">
										<?php echo htmlspecialchars_decode($news[7]); ?>
									</div>
									<div>
										<?=$Lang['SysMgr']['SchoolNews']['StartDate']?>:<span class="dateTime-label"><?php echo $news[1]; ?></span>
									</div>
								</div>
							</li>
							<?php }?>
						</ul>
    						<div id="blkMoreSchoolNews"><a href='schoolnews_more.php'><?=$Lang['General']['More']?></a></div>
    			</div>
    			<?php echo $footerHtml;?>
			</div>
		</div>		
	</div>
</body>
</html>