<?php
## Using By : Isaac

/********************** Change Log ***********************/
#   Date:       2019-01-17 Isaac
#               Added common.css & customTemplate.css
#
#   Date:       2018-08-08 Isaac
#               remove ?v=3 from favicon's path to have icon shown properly 
#
#	Date:		2017-04-10
#				LivingHomeopathy header control
/********************** Change Log ***********************/

if (!class_exists("libpowerclass_ui", false)) {
	include_once($intranet_root."/includes/PowerClass/libpowerclass_ui.php");
}

$libpowerclass_ui= new libpowerclass_ui();
$urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];
$showCustomProjectHeader = !strstr($urlSrc, "/home/eAdmin/ResourcesMgmt/DigitalChannels") && $libpowerclass_ui->isModuleOn();
// $showCustomProjectHeader= false;

if ($showCustomProjectHeader) {
	$home_header_no_EmulateIE7 = true;
	$home_header_with_EmulateIE9 = true;
	$showViewportWithDeviceWidth = true;
	
	$project_identify = $libpowerclass_ui->getProjectIdentify();
	
	$hide_menu_now = "style=\"display:none\"";
	$lang_file = dirname(dirname(dirname(__FILE__))) . "/lang/" . $project_identify . "/pc_lang.$intranet_session_language.php";
	if (file_exists($lang_file)) {
		include_once($lang_file);
	}
	$template_path = "/templates/";
	$source_path = $template_path . $project_identify . "/";
	$powerClass_source_path = $template_path ."PowerClass/";
	$powerClass_common_template_source_path = $powerClass_source_path.'views/common/';
	// $customk_favicon = $source_path . 'images/favicon.gif';
	/******************************************************************/
	$dhl_css = array();
	// $dhl_css[] = $source_path . "css/main.css";
	$dhl_css[] = $source_path . "css/portal.css";
	$dhl_css[] = $source_path . "css/portalIcon.css";
	$dhl_css[] = $source_path . "css/font-awesome.min.css";
	$dhl_css[] = $source_path . "css/zOverwrite.css";
	$dhl_css[] = $source_path . "css/custom.css?t=" . time();
	$dhl_css[] = $powerClass_source_path . "css/common.css";
	if($project_identify!="PowerClass"){ 
	    $dhl_css[] = $powerClass_source_path . "css/customTemplate.css";
	}
	$dhl_js = array();
	if (!$showHeader) {
		$dhl_js[] = $template_path. "highchart/highcharts.src.js";
		// $dhl_js[] = $source_path . "js/morris.min.js";
	}
	// $dhl_js[] = $source_path . "js/script.js";
	$dhl_js[] = $source_path . "js/init.js";
// 	$customk_favicon = $source_path . 'images/favicon.gif?v=3';
	$customk_favicon = $source_path . 'images/favicon.gif';
	/******************************************************************/
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$li_hp = new libuser($UserID);
	/*
	if ($li->PhotoLink !="" && $_SESSION['UserType']==USERTYPE_STUDENT)
	{
		if (is_file($intranet_root.$li_hp->PhotoLink) && $photo_link != $li_hp->PhotoLink)
		{
			$photo_link = $li_hp->PhotoLink;
		}
	}
	########## Personal Photo
	$personal_photo_link = "/images/myaccount_personalinfo/samplephoto.gif";
	if ($li_hp->PersonalPhotoLink !="")
	{
		if (is_file($intranet_root.$li_hp->PersonalPhotoLink))
		{
			$personal_photo_link = $li_hp->PersonalPhotoLink;
		}
	}
	$myPersonalPhoto = $personal_photo_link;
	if (empty($myPersonalPhoto)) {
		$myPersonalPhoto = $photo_link;
	}
	########## Personal Photo
	/******************************************************************/

	
	
	$args = array(
				"source_path" => $source_path,
				"myPersonalPhoto" => $myPersonalPhoto,
	            "powerClass_source_path"=> $powerClass_source_path,
	            "powerClass_common_template_source_path"=>$powerClass_common_template_source_path,
				"UserIdentity" => $UserIdentity,
				"UserName" => ($intranet_session_language == "en") ? $_SESSION["EnglishName"] : $_SESSION["ChineseName"],
				"Lang" => $Lang
			);
	if (empty($args["UserName"])) $args["UserName"] = $_SESSION["EnglishName"];
	$topMenuCustomization = $libpowerclass_ui->getTopMenuHTML($args);
}
$hideCountDown = false;
if (!$showHeader) {
	$topMenuCustomization = "";
	$hideCountDown = true;
}
?>