<?php
include_once("../../includes/global.php");

ob_start();

$image_path = (strstr($image, "http://".$eclass_httppath) || substr($image, 0, 1)=="/") ? $image : "";
if (substr($image, 0, 1)=="/")
{
	$image_path = $intranet_root . $image;
} else
{
	$image_path = str_replace("http://".$eclass_httppath, $eclass_filepath, $image_path);
}

if ($image_path!="" && file_exists($image_path))
{
	# try to get thumbnail
	//$phpver_check = phpversion_compare("4.2.0");
	$ext = strtolower(substr($image_path, strlen($image_path)-3));
	//if (($phpver_check=="LATER" || $phpver_check=="SAME") && ($ext=="jpg" || $ext=="jpeg" || $ext=="jpe"))
	if ($ext=="jpg" || $ext=="jpeg" || $ext=="jpe")
	{
		$thumbnail = @exif_thumbnail($image_path);
	}

	# read original image
	if (!$thumbnail)
	{
		$thumbnail = readfile($image_path);
	}
} else
{
	$thumbnail = false;
}


if ($thumbnail)
{
   header('Content-type: image/jpeg');
   echo $thumbnail;
}

?>