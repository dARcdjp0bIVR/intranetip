(function($) {
	$.fn.overlayTextarea = function(options) {

	 	// build main options before element iteration
		var opts = $.extend({}, $.fn.overlayTextarea.defaults, options);
	 	
		// append a textarea wrapper div at the end of the body
		$(document.body).append("<div id='overlayTextarea_TAWrapper' style='position:absolute; background-color:#ccc; z-index:100; display:none;'></div>");
	 
	 	$ObjList = this;
	 
	 	// iterate and reformat each matched element
	 	return this.each(function() {
	 	
			$this = $(this);
			
			// build element specific options
			var o = $.meta ? $.extend({}, opts, $this.data()) : opts;

			$this.focus(function(){
				$thisTA = $(this);
				$.fn.overlayTextarea.cloneTextarea($thisTA, o, $ObjList);
			});

		});
	};
	
	// check whether shift key is being held down 
	$.fn.overlayTextarea.isShiftPress = false;
	$(document).keydown(function(event){
		// indicate shift press on shift key down
		if(event.which==16)
			$.fn.overlayTextarea.isShiftPress = true;
		
	}).keyup(function(event){
		// unset shift press indicator on shift key up
		if(event.which==16)
			$.fn.overlayTextarea.isShiftPress = false;
	});
		


	$.fn.overlayTextarea.cloneTextarea = function($Obj, opts, $ObjList) {
		var trigger = false;

		if($ObjList.index($Obj)>=0)
		{
			if(opts.triggerWidthLimit == "self" && $Obj.width()<opts.width)
				trigger = true;
			else if($Obj.width() < opts.triggerWidthLimit)
				trigger = true;
		}
		else 
			$Obj.focus();
			
		
		if(trigger)
		{
			$.fn.overlayTextarea.relocateTAWrapper($Obj, opts)
			$.fn.overlayTextarea.showCloneTA($Obj, opts, $ObjList)
		}
	};
	
	$.fn.overlayTextarea.relocateTAWrapper = function($Obj, opts) {
		
		var pos = $Obj.position();
		var left = pos.left;
		var top = pos.top;		
		var w = opts.width;
		var h = opts.height;
		var sw = $(window).width(); 
		var sh = $(window).height();
		var scrollTop = $(window).scrollTop();
		 
		// x pos
		var l = (left+w>sw)?sw-w:left;

		// y pos
		var t = ((top-scrollTop)+h>sh)?scrollTop+sh-h:top;

		$("div#overlayTextarea_TAWrapper").css({
			"position":"absolute",
			"opacity":opts.opacity,
			"width":w,
			"height":h,
			"top":t,
			"left":l			
		})	

	};	

	$.fn.overlayTextarea.showCloneTA = function($Obj, opts, $ObjList) {
	
		// build clone Textarea
		var CloneTA = "<textarea style='margin:2px'>\n"+$Obj.val()+"</textarea>";
		$("div#overlayTextarea_TAWrapper").html(CloneTA).show();
		
		$CloneTAObj = $("div#overlayTextarea_TAWrapper>textarea");
		$CloneTAObj.css({
			"width":opts.width-10,
			"height":opts.height-10
		}).focus();
		
		// select all text on focus
		if(opts.selectTextOnFocus)
			$CloneTAObj.select()

		// highlight parent
		if(opts.highlightParent)
			$Obj.parent().css("background-color","#fea");
		
		// setup onblur action
		$CloneTAObj.blur(function(){
			$.fn.overlayTextarea.blurCloneTA($Obj, $(this), opts);
		})
		
		// handle pressing tab/shift+tab to next textarea
		$CloneTAObj.keydown(function(event){
			
			if(event.which==9)
			{
				// blur current clone textarea
				$.fn.overlayTextarea.blurCloneTA($Obj, $(this), opts);
				
		 		// find and focus  the index of current
		 		$TabbableList = $("textarea, button, input, select");  
		 		var idx = $TabbableList.index($Obj);

				// if shift is being held down, go back, else go next 
		 		var nextprevidx = $.fn.overlayTextarea.isShiftPress?idx-1:(idx+1)%$TabbableList.length;
		 		if(nextprevidx<0)	nextprevidx = $TabbableList.length-1;

				$TabbableList.eq(nextprevidx).focus()
		 		$.fn.overlayTextarea.cloneTextarea($TabbableList.eq(nextprevidx), opts, $ObjList)

				// avoid original tab action
		 		return false;		
			}
		});
		
	};	

	$.fn.overlayTextarea.blurCloneTA = function($Obj, $CloneObj, opts) {
		$Obj.text($CloneObj.val());
		$CloneObj.remove();
		$("div#overlayTextarea_TAWrapper").hide();
		
		if(opts.highlightParent)
			$Obj.parent().css("background-color",opts.dehighlightColor);
	};	

	$.fn.overlayTextarea.defaults = {
		width: 450,
		height: 150,
		highlightParent: false,
		highlightColor: "#fea",
		dehighlightColor: "#fff",
		opacity: 1,
		triggerWidthLimit: "self",
		selectTextOnFocus: true 
	};

})(jQuery);