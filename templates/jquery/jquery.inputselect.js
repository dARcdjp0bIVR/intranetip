/*
 *	Input Selection for jQuery
 *	by Jason Lam - Copyright © Broadlearning Education (Asia) Limit. All Rights Reserved
 *	utf8
 *	
 *	Note:
 *		Input element must contain attirbute "id"
 *		Attribute "autosuggest" is used to control the availability of auto suggestion according to the input keyword
 *		
 *	Option: 
 *	@Params		ajax_script 	: null - script for retrieving suggested items
 * 	@Params		image_path		: null - relative path of image if using
 *	@Params		js_lang_alert	: Array - js lang use only
 */

(function($) {
	
	/*
	var ... = '';
	*/
	var ajax_script = '';
	var lyr_opened = 0;
	var image_path = '';
	var js_lang_alert = {};

	$.fn.inputselect = function(options) {

		var opts = $.extend({}, $.fn.inputselect.defaults, options);
		ajax_script = opts.ajax_script;
		image_path = opts.image_path;
		ajax_callback = opts.ajax_callback;
		js_lang_alert = opts.js_lang_alert;
		
		if($(this).length > 0){
			$(this).each(function() {
				var inputObj = $(this);
				var input_id = inputObj.attr("id");
				
				if(input_id == '')
					return;
			
				var input_w = inputObj.width();
				var lyr_id = input_id+'_selection_list';
		
				// set input obj - width , border
				//var new_width = parseInt(input_w) - 35;
				//inputObj.width(new_width+'px');
				inputObj.width('84%');
				inputObj.css('border', 'none');
			
				// create a container
				inputObj.wrap('<div id="'+input_id+'_inputselect_div" class="sl_container"></div>');
		
				var divObj = $('#'+input_id+'_inputselect_div');
			
				// add arrow button
				var spanObj = $('<span/>').attr('id', input_id+'_arrow').addClass('sl_link');
				var iconObj = $('<a/>').attr('id', input_id+'_link')
					.click(function(ev){ 
						// Stop event handling in non-IE browsers:
						ev.preventDefault();
						ev.stopPropagation();
						
						opts.callback(input_id);
						
						// Stop event handling in IE
						return false; 
					});
				var iconImg = '<img src="'+opts.image_path+'selectbox_arrow.gif" border="0">';
				iconObj.append(iconImg);
				spanObj.append(iconObj);
				divObj.append(spanObj);
				
				// add selection layer
				var div_w = divObj.width();
				divObj.parent().append('<div id="'+lyr_id+'" class="sl_lyr" style="width:'+div_w+'px;"></div>');
				/*
				divObj.unbind("mouseover").bind("mouseover", function(){
					if($('#'+input_id+'_arrow').length > 0){
						$('#'+input_id+'_arrow').show();
					}
				});
				*/
				/*
				divObj.unbind("mouseout").bind("mouseout", function(){
					if($('#'+input_id+'_arrow').length > 0){
						if(inputObj.is(":focus") == false && $('#'+input_id+'_selection_list').css('display') != 'none'){
							//$('#'+input_id+'_arrow').hide();
							$('#'+lyr_id).hide();
							set_arrow_image(input_id, 0);
							lyr_opened = 0;
						}
					}
				});
				*/
				/*
				inputObj.unbind("focusout").bind("focusout", function(){
					if($('#'+input_id+'_arrow').length > 0){
						//$('#'+input_id+'_arrow').hide();
						$('#'+lyr_id).hide();
						set_arrow_image(input_id, 0);
						lyr_opened = 0;
					}
				});
				inputObj.unbind("focusin").bind("focus", function(){
					if($('#'+input_id+'_arrow').length > 0){
						$('#'+input_id+'_arrow').show();
					}
				});
				*/
				
				// IE does not work with this
				if(navigator.userAgent.indexOf("MSIE")==-1){
					$("body").bind("focusin", "document", function(ev){
						// Stop event handling in non-IE browsers:
						ev.preventDefault();
						ev.stopPropagation();
						
						$('#'+lyr_id).hide();
						set_arrow_image(input_id, 0);
					});
				}
			});
	  	}
	}
	
	$.fn.inputselect.defaults = {
		image_path: '', 
		callback: function(id){
			get_input_selection_list(id);
		},
		ajax_callback: function(id){}, 
		js_lang_alert: {'no_records' : 'No record(s) at this moment.' }
	};
	
	/*
	function ...(){}
	*/
	function get_input_selection_list(id){
		ajax_script = ajax_script || '';
		if(ajax_script == '')
			return; 
			
		var lyr = id + '_selection_list';
		var lyr_show = ($('#'+id+'_selection_list').css('display') == 'none') ? 0 : 1;
		if(lyr_show == 1){
			$('#'+lyr).html('').hide();
			lyr_opened = 0;
			set_arrow_image(id, 0);
		} else {
			var param = {};
			param['action'] = 'get_'+id;
			//$.post('ajax_get_selection_list.php', param, function(response){
			$.post(ajax_script, param, function(response){
				$('#'+lyr).html('');
				if(response.length > 0){
					for(var i=0; i < response.length; i++) {
					
						var rowObj = $('<div/>')
							.addClass('sl_row')
							.mouseover(function(ev){ 
								// Stop event handling in non-IE browsers:
								ev.preventDefault();
								ev.stopPropagation();
								$(this).addClass('sl_selected'); 
							})
							.mouseout(function(ev){ 
								// Stop event handling in non-IE browsers:
								ev.preventDefault();
								ev.stopPropagation(); 
								$(this).removeClass('sl_selected');
							})
							.click(function(ev){
								// Stop event handling in non-IE browsers:
								ev.preventDefault();
								ev.stopPropagation();
								
								$('#'+id).val($(this).text());
								//$('#'+id+'_arrow').hide();
								$('#'+lyr).hide(); 
								set_arrow_image(id, 0);
		        				ajax_callback(id);
		        				
								// Stop event handling in IE
								return false;
							})
							.html(response[i].title);
						$('#'+lyr).append(rowObj);
			        }
				} else {
					var rowObj = $('<div/>').addClass('sl_row').text(js_lang_alert['no_records']);
					$('#'+lyr).append(rowObj);
				}
		        $('#'+lyr).show();
				lyr_opened = 1;
				set_arrow_image(id, 1);
			}, 'json');
		}
	}
	
	function set_arrow_image(id, type){
		if(type == 1){
			var img = 'selectbox_arrow_on';
		} else {
			var img = 'selectbox_arrow';
		}
		var iconImg = '<img src="'+image_path+img+'.gif" border="0">';
		$('#'+id+'_link').html(iconImg);
	}
	
})(jQuery);