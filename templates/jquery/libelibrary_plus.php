<?
class elibrary_plus extends elibrary
{
    private $book_id;
    private $physical_book_id;
    public $physical_book_init_value;
    public $physical_book_type_code;
    private $user_id;
    private $book_formats="";
    private $libmsDB;
    private $range;
    
    public function elibrary_plus($book_id=0, $user_id=0, $book_type="type_all", $range){//note that using user = affected user
	
	global $intranet_session_language, $eclass_prefix, $junior_mck;


	$this->libmsDB = $eclass_prefix . "eClass_LIBMS";
	
	include_once("libelibrary_install.php");
        include_once("liblibrarymgmt.php");
         
	$this->elibrary();
        
        $this->physical_book_init_value = 10000000;	
	$this->physical_book_type_code = "physical";	
	$this->book_id=$book_id;
        $this->physical_book_id=$book_id-$this->physical_book_init_value;
	$this->user_id=$user_id;
	$this->range=$range;
        $this->convert_utf8 = $junior_mck;
        
        
	switch ($book_type){
	    case "type_physical":
		$this->book_formats="AND BookFormat = '".$this->physical_book_type_code."' ";
	    break;
	    case "type_pc":
		$this->book_formats="AND (BookFormat = 'ePub2' OR BookFormat IS NULL)";
	    break;
	    case "type_ipad":
		$this->book_formats="AND BookFormat = 'ePub2' ";
	    break;
	}

    }
           
    public function getRecommendedClassLevels(){
        
    
        $recommends = $this->getBookRecommend(array("BookID"=>$this->book_id));
       
        if (empty($recommends)){
            $returnArray = array('content'=>'', 'isEdit'=>false);
            $recommended_classes=array();
        }else{
            $returnArray = array('content'=>$recommends[0]['Description'], 'isEdit'=>'true');
            $recommended_classes=explode('|',$recommends[0]['RecommendGroup']);
        }
        
        
        
        if ($this->convert_utf8){
            $this->dbConnectionLatin1();
            $sql = "select ClassLevelID as class_level_id ,LevelName as class_level_name from INTRANET_CLASSLEVEL order by LevelName";
            $class_levels = $this->returnArray($sql);
            foreach ($class_levels as &$class_level){
                $class_level['class_level_name']=convert2Unicode($class_level['class_level_name']);
                $class_level['class_level_recommended'] = in_array($class_level['class_level_id'],$recommended_classes);
            }
            $this->dbConnectionUTF8(); 
        }else{
            $sql = "select YearID as class_level_id, YearName as class_level_name from YEAR order by Sequence";
            $class_levels = $this->returnArray($sql);
            foreach ($class_levels as &$class_level){
                $class_level['class_level_recommended'] = in_array($class_level['class_level_id'],$recommended_classes);
            }
        }
        $returnArray['class_levels']=$class_levels;
        
        return $returnArray;
        
    }
    public function getWeekOpeningHours(){
        
        $libms = new liblms();
        return $libms->GET_OPEN_TIME();
    }
    public function getUserPermission(){
        $libms = new liblms();
        $libms->get_current_user_right();
        $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'] = $this->IS_ADMIN_USER($this->user_id);
        
        return array(
            'admin' => (boolean)$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'],
            'circulation'=>$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']
                || in_array(true,(array)$_SESSION['LIBMS']['admin']['current_right']['circulation management']),
            'setting'=>$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']
                || in_array(true,(array)$_SESSION['LIBMS']['admin']['current_right']['admin'])
                || in_array(true,(array)$_SESSION['LIBMS']['admin']['current_right']['reports']),
            'reserve'=>$_SESSION['LIBMS']['admin']['current_right']['service']['reserve'],
            'renew'=>$_SESSION['LIBMS']['admin']['current_right']['service']['renew'],
        );
    }

    public function getRecommendBooks($limit=5){
                                  
        $books = $this->GET_RECOMMEND_BOOK($limit);
	
	$returnArray=array();
	foreach ($books as $book){
            
            $sql = "SELECT a.*, ifnull(round(avg(Rating),1),'--') as rating
                    FROM INTRANET_ELIB_BOOK a
                    LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID
                    WHERE a.BookID=".$book['BookID']." ".$this->book_formats."
                    GROUP BY a.BookID";//get all detaild from id
            
            $detail=current($this->returnArray($sql));
            if ($detail){
                $entry=$this->getBooksArrayByFormat($detail);
                $entry['reason']=$book['Description'];
                $entry['rating']=$detail['rating'];
                $returnArray[]=$entry;
            }
	}
	
	return $returnArray;
    }
    public function getNewBooks($limit=5){
        $sql = "SELECT a.*, ifnull(round(avg(Rating),1),'--') as rating
                FROM INTRANET_ELIB_BOOK a
                LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID
                WHERE a.Publish=1 ".$this->book_formats."
                GROUP BY a.BookID
                ORDER BY a.DateModified DESC
                LIMIT $limit";
        
        $books = $this->returnArray($sql);
	
	$returnArray=array();
	foreach ($books as $book){
            $entry=$this->getBooksArrayByFormat($book);
            $entry['rating']=$book['rating'];
	    $returnArray[]=$entry; 
	}
        
        return $returnArray;
    }
    public function getBookList($options, $offset=0, $amount=30, $sortby='title', $order='asc'){
	
	$search="";
        if (isset($options['language']) && $options['language']!='others') $search.=" AND Language = '".$options['language']."' ";
	if (isset($options['category'])) $search.=" AND Category = '".$options['category']."' ";
	if (isset($options['subcategory'])) $search.=" AND SubCategory = '".$options['subcategory']."' ";
	if (isset($options['keyword'])) $search.=" AND (Title like '%".$options['keyword']."%' OR Author like '%".$options['keyword']."%' OR Publisher like '%".$options['keyword']."%' OR Category like '%".$options['keyword']."%' OR SubCategory like '%".$options['keyword']."%' OR TagName like '%".$options['keyword']."%')";
	if (isset($options['tag_id'])) $search.=" AND d.TagID=".$options['tag_id'];

	$limit = $offset*$amount.','.$amount;
	$sql = "SELECT a.*, ifnull(round(avg(Rating),1),'--') as rating
                FROM INTRANET_ELIB_TAG c 
                INNER JOIN INTRANET_ELIB_BOOK_TAG d on d.TagID=c.TagID
                RIGHT JOIN INTRANET_ELIB_BOOK a on a.BookID=d.BookID 
                LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID
                WHERE 1 $search
                AND Publish = 1
                ".$this->book_formats."
                GROUP BY a.BookID
                ORDER BY $sortby $order 
                LIMIT $limit";
	
	$books = $this->returnArray($sql);

	$returnArray=array();
	foreach ($books as $book){
            $entry=$this->getBooksArrayByFormat($book);
            $entry['rating']=$book['rating'];
	    $returnArray[]=$entry;
	}
	
	return $returnArray;
    }
    
    public function getBookDetail(){
	
	$sql = "SELECT *
                FROM 
                INTRANET_ELIB_BOOK 
                WHERE BookID = ".$this->book_id;
                
        $books = $this->returnArray($sql);
    			
	$returnArray=$this->getBooksArrayByFormat($books[0]);
        
        $sql = "SELECT t.TagName as tag_name, t.TagID as tag_id FROM INTRANET_ELIB_TAG t, INTRANET_ELIB_BOOK_TAG bt WHERE bt.BookID='".$this->book_id."' AND t.TagID=bt.TagID";
	$returnArray['tags']=$this->returnArray($sql);

	return $returnArray;
	
    }
    private function getPhysicalBookLoanCount($book_id){
        $sql = "SELECT count(*)
                    from ".$this->libmsDB.".LIBMS_BORROW_LOG a
                    WHERE BookID=".($book_id-$this->physical_book_init_value);
        return current($this->returnVector($sql));
                	    
    }
    
    private function getBooksArrayByFormat($book){
	global $eLib_plus;
	$elib_install=new elibrary_install();
	
	$returnArray=array();

	if ($book['BookFormat']==$this->physical_book_type_code){
	    $physicalBook = $this->GET_PHYSICAL_BOOK_INFO($book['BookID']);
	    $title = $physicalBook['BookTitle'];
	    $author = $physicalBook['ResponsibilityBy1'].' '.$physicalBook['ResponsibilityBy2'].' '.$physicalBook['ResponsibilityBy3'];
	    $publisher = $physicalBook['Publisher'];
	    $image = $physicalBook['CoverImage'];
	    $status = $physicalBook['OpenBorrow'];
	    $description = $physicalBook['Introduction'];
	    $language = $physicalBook['Language'];
	    $edition = $physicalBook['Edition'];
	    $publish_year = $physicalBook['PublishYear'];
	    $location = $physicalBook['Location'];
	    //$available_count = $physicalBook['NoOfCopyAvailable'];
            $loan_count=$this->getPhysicalBookLoanCount($book['BookID']);
            
	}else{
			   
	    $title = $book['Title']; 
	    $author = $book['Author'];
	    $publisher = $book['Publisher'];
	    $image = $this->getBookImage($book['BookID']);
	    $description = $book['Preface'];
	    $url = $elib_install->get_book_reader($book['BookID'],$book['BookFormat'],$book['IsTLF'], $this->user_id);
	    $language = $eLib_plus["html"][$book['language']];
	    $hit_rate=(int)$book['HitRate'];
	
	}
	$returnArray=array(
	    'id'=>$book['BookID'],
	    'title'=>$title,
	    'author'=> !trim($author) ? '--': $author,
	    'publisher'=> empty($publisher)? '--': $publisher,
	    'image'=>$image,
	    'url'=>$url,
	    'status' => $status,
	    'description' => nl2br($description),
	    'location' => $location,
	    'hit_rate' => $hit_rate,
            'loan_count' => $loan_count,
	    'category' => empty($book['Category'])? "--" : $book['Category'],
	    'subcategory' => $book['SubCategory'],
	    'language' => $language,
	    'edition' => $edition,
	    //'available_count' => $available
	);
        return $returnArray;
    }
    
    private function getBookImage($book_id){
	global $PATH_WRT_ROOT;
	$image = '/file/elibrary/content/'.$book_id.'/image/cover.jpg';
	
	return file_exists($PATH_WRT_ROOT.$image) ? $image : null;
    }
    public function getBookStat(){
	
	$sql = "SELECT ifnull(round(avg(Rating),1),'--') as rating, count(distinct(ReviewID)) as count FROM INTRANET_ELIB_BOOK_REVIEW WHERE BookID = '".$this->book_id."' ";
	$reviews = $this->returnArray($sql, 5);
		
	$returnArray['rating']=$reviews[0]['rating'];
	$returnArray['review_count']=$reviews[0]['count'];
	
	$sql = "SELECT HitRate as hit_count, BookFormat FROM INTRANET_ELIB_BOOK WHERE BookID = '".$this->book_id."'";   
        $book = $this->returnArray($sql);
	
	if ($book[0]['BookFormat']==$this->physical_book_type_code){
            $returnArray['loan_count'] = $this->getPhysicalBookLoanCount($this->book_id);
	    $returnArray['status']=current($this->returnVector("SELECT OpenReservation from ".$this->libmsDB.".LIBMS_BOOK WHERE BookID=".$this->physical_book_id));
	}else{
            $returnArray['hit_rate'] =  $book[0]['hit_count'];
        }
        
	$returnArray['is_reserved']=$this->isReservedByUser();
	$returnArray['is_favourite']=$this->isFavourite();
	return $returnArray;
		
    }
    public function isFavourite(){
		
	return $this->IS_MY_FAVOURITE_BOOK(array("BookID"=>$this->book_id, "currUserID"=>$this->user_id));
    }
    public function isReservedByUser(){
	
        $sql = "SELECT count(*) from ".$this->libmsDB.".LIBMS_RESERVATION_LOG
                where BookID=".$this->physical_book_id.' AND UserID='.$this->user_id;
        
        return current($this->returnVector($sql));

    }    
    public function getBookReviews(){
    	
	$sql = "SELECT a.ReviewID as id, a.UserID as user_id, a.Rating as rating, a.Content as content,
                datediff(now(),a.DateModified) as days, a.DateModified as date,
                count(c.ReviewCommentID) as likes
                FROM  INTRANET_ELIB_BOOK_REVIEW a
                left join INTRANET_ELIB_BOOK_REVIEW_HELPFUL c on a.ReviewID=c.ReviewID AND c.Choose=1
                WHERE a.BookID = ".$this->book_id."
                GROUP BY a.ReviewID
                ORDER BY a.ReviewID DESC";

	$reviews = $this->returnArray($sql);echo mysql_error();
        foreach ($reviews as &$review){
            $review['content']=strip_tags($review['content']);
            list($review['name'],$review['class'],$review['image'])=$this->getUserInfo($review['user_id']);
        }
        
	return $reviews;
    }
    public function removeReviews($review_id){
        
        return $this->REMOVE_BOOK_REVIEW(false, $review_id);
        
    }
    public function addReview($content, $rate){
        
        return $this->addBookReview(array('Rating'=>$rate, "BookID"=>$this->book_id, "Content"=>htmlspecialchars($content)));
        
    }
    public function addOrRemoveReservation(){
	
	
	$sql = "DELETE FROM ".$this->libmsDB.".LIBMS_RESERVATION_LOG
                where BookID=".$this->physical_book_id.' AND UserID='.$this->user_id;
		
	$this->db_db_query($sql);
        $isRemoved = $this->db_affected_rows();
	
        $sql = "SELECT b.NoOfCopyAvailable-count(a.ReservationID) from ".$this->libmsDB.".LIBMS_BOOK b
                LEFT JOIN ".$this->libmsDB.".LIBMS_RESERVATION_LOG a on a.BookID=b.BookID 
                where b.BookID=".$this->physical_book_id." 
                GROUP BY b.BookID";//see if any no of reservation > no of book copies
        $remainCopy=current($this->returnVector($sql));
                
	if (!$isRemoved){//add reservation
            
            $physicalBook = $this->GET_PHYSICAL_BOOK_INFO($this->book_id);
	    if(!$physicalBook['OpenReservation']) return -1;
          
            $status=($remainCopy>0) ? 'READY' : 'WAITING'; //get next reservation status
            
            $sql = "INSERT INTO ".$this->libmsDB.".LIBMS_RESERVATION_LOG
                    (BookID, UserID, ReserveTime, RecordStatus)
                    VALUES
                    ('".$this->physical_book_id."', '".$this->user_id."', now(), '$status')";
                    
            $this->db_db_query($sql);
            return 1;

	}else{//update queuing reservation
            
            $status=($remainCopy>=0) ? 'READY' : 'WAITING'; //get current reservation status
             
            $sql = "UPDATE ".$this->libmsDB.".LIBMS_RESERVATION_LOG SET 
                    RecordStatus = '$status', DateModified = now()
                    WHERE BookID = '".$this->physical_book_id."' AND RecordStatus='WAITING'
                    ORDER BY ReserveTime ASC
                    LIMIT 1";
                        
            $this->db_db_query($sql);
            return 0;
        }
	
    }
    public function addOrRemoveFavourite(){
	
	
	$sql = "DELETE FROM INTRANET_ELIB_BOOK_MY_FAVOURITES
		WHERE BookID=".$this->book_id." AND UserID=".$this->user_id;
		
	$this->db_db_query($sql);
		
	if ($this->db_affected_rows()==0){
	    $sql = "INSERT INTO INTRANET_ELIB_BOOK_MY_FAVOURITES
		    (BookID, UserID, DateModified)
                    VALUES
		    ('".$this->book_id."', '".$this->user_id."', now())";
		    
	    $this->db_db_query($sql);
	    
	    return true;
	}
	
	return false;
	
    }
    public function likeOrUnlikeReview($reviewID){
		
	$sql = "DELETE FROM INTRANET_ELIB_BOOK_REVIEW_HELPFUL
		WHERE BookID=".$this->book_id." AND UserID=".$this->user_id." and ReviewID=$reviewID";
		
	$this->db_db_query($sql);
		
	if ($this->db_affected_rows()==0){
	    
	    $this->addBookReviewHelpful(array('ReviewID'=>$reviewID, "BookID"=>$this->book_id, "Choose"=>1));
	    
	    return true;
	}
	
	return false;
	
    }
    public function addOrEditRecommend($class_level_ids, $content, $mode){
      
        $parArr=array('Description'=>$content, 'ClassLevelID'=>implode('|',$class_level_ids), 'currUserID'=>$this->user_id, 'BookID'=>$this->book_id);
        $rows = $this->editBookRecommend($parArr);
        if ($this->db_affected_rows()==0){
            $this->addBookRecommend($parArr);
            return true;
        }else{
            return false;
        }
    }
    public function removeRecommend(){
        
        $sql = "DELETE FROM
		INTRANET_ELIB_BOOK_RECOMMEND
		WHERE
		BookID = ".$this->book_id;
                
        return $this->db_db_query($sql);
    }
    public function renewBook($borrow_id){
        
        $renew_quota = $this->getUserRenewQuota();
        $return_duration = $this->getUserReturnDuration();
        
        $sql = "UPDATE ".$this->libmsDB.".LIBMS_BORROW_LOG
                SET
                DueDate=CURDATE()+INTERVAL $return_duration DAY,
                RenewalTime=RenewalTime+1,
                DateModified=NOW(),
                LastModifiedBy=".$this->user_id."
                WHERE 
                BorrowLogID = $borrow_id AND
                RenewalTime < $renew_quota AND
                RecordStatus = 'BORROWED'
                ";
                
        $this->db_db_query($sql);
        
        return $this->db_affected_rows();
        
    }
    public function getCategoryBookCount($options){
	
	$search="";
        if (isset($options['language']) && $options['language']!='others') $search.=" AND Language = '".$options['language']."' ";
	if (isset($options['category'])) $search.=" AND Category = '".$options['category']."' ";
	if (isset($options['subcategory'])) $search.=" AND SubCategory = '".$options['subcategory']."' ";
	if (isset($options['keyword'])) $search.=" AND (Title like '%".$options['keyword']."%' OR Author like '%".$options['keyword']."%' OR Publisher like '%".$options['keyword']."%' OR Category like '%".$options['keyword']."%' OR SubCategory like '%".$options['keyword']."%' OR TagName like '%".$options['keyword']."%')";
	if (isset($options['tag_id'])) $search.=" AND c.TagID=".$options['tag_id'];

	$sql = "SELECT COUNT(distinct(a.BookID))
                FROM
                INTRANET_ELIB_TAG c 
                INNER JOIN INTRANET_ELIB_BOOK_TAG b on b.TagID=c.TagID
                RIGHT JOIN INTRANET_ELIB_BOOK a on a.BookID=b.BookID 
                WHERE 1 $search AND Publish = 1 ".$this->book_formats;
                
	return current($this->returnVector($sql));
    }
    public function getBookTags(){
       
        $sql = "SELECT t.TagName as title, t.TagID as tag_id, COUNT(bt.TagID) AS count
                FROM INTRANET_ELIB_TAG t
                LEFT JOIN INTRANET_ELIB_BOOK_TAG bt ON t.TagID=bt.TagID
                LEFT JOIN INTRANET_ELIB_BOOK AS b ON bt.BookID=b.BookID
                WHERE Publish = 1 ".$this->book_formats."
                GROUP BY t.TagID ORDER BY title";
                
	return $this->returnArray($sql);
        
    }
    public function getBookCatrgories(){
	
        $sql = " 
		SELECT Language, Category, SubCategory, Count(SubCategory) as count_subcategory
		FROM INTRANET_ELIB_BOOK
		WHERE Publish = 1  ".$this->book_formats."
		GROUP BY category, SubCategory
		ORDER BY category
		";
		                
	$subcategories = $this->returnArray($sql);
	
	$returnArray=array(
	    'chi'=>array(
		'count'=>0,
		'categories'=>array()
	    ),
	    'eng'=>array(
		'count'=>0,
		'categories'=>array()
	    )					    
	);
	
	foreach ($subcategories as $subcategory){
	    
	    $l=$subcategory['Language'];
	    $c=$subcategory['Category'];
	    
	    if ($l!='chi' && $l!='eng') $l='others';
	    
	    if (empty($returnArray[$l]['categories'][$c])){
		
		$returnArray[$l]['categories'][$c]=array(
		    'title'=>$c,
		    'count'=>0,
		    'subcategories'=>array()
		);
	    }
	    
	    $returnArray[$l]['categories'][$c]['subcategories'][]=array(
		  'title'=>$subcategory['SubCategory'],
		  'count'=>$subcategory['count_subcategory'],
	    );
	    
	    $returnArray[$l]['categories'][$c]['count']+=$subcategory['count_subcategory'];
	    $returnArray[$l]['count']+=$subcategory['count_subcategory'];
	         
	}
	
	return $returnArray;
    }
    public function getUserNotes(){
        
        $elib_install=new elibrary_install();
	
        $sql = "SELECT 
		a.*, 
		b.NoteType as type,b.NoteID as note_id,b.Category as category, b.Content as content, b.DateModified as last_modified
		FROM
		INTRANET_ELIB_BOOK a
                INNER JOIN INTRANET_ELIB_USER_MYNOTES b
		ON b.UserID = ".$this->user_id."
                WHERE a.BookID = b.BookID
		AND a.Publish = 1
                ORDER by last_modified desc";
                
        $books = $this->returnArray($sql);
	$returnArray=array();
	foreach($books as $book){
	    $entry=$this->getBooksArrayByFormat($book);
            $entry['book_id']=$entry['id'];
            $entry['type']=$book['type'];
	    $entry['note_id']=$book['note_id'];
            $entry['category']=$book['category'];
            $entry['content']=$book['content'];
            $entry['note_url']= str_replace('?BookID='.$entry['id'],'?BookID='.$entry['id']."&NoteType=".$book['type']."&NoteID=".$book['note_id'],$elib_install->get_book_reader($entry['id'],$book['BookFormat'],$book['IsTLF'], $this->user_id));
            $entry['last_modified']=$book['last_modified'];
	    $returnArray[]=$entry;
	}
	
	return $returnArray;
                
    }
    public function getUserInfo($user_id=false){
        global $eLib_plus, $userPhotoField, $intranet_session_language;
        
        $userNameField=$intranet_session_language == 'en' ? 'EnglishName' : 'ChineseName';
        if (!$user_id) $user_id=$this->user_id;
		
	$sql = "SELECT 
                ".$userNameField." as name,
                concat('(',ClassName,'-',ClassNumber,')') as class,
                ".$userPhotoField." as image
                FROM INTRANET_USER
                WHERE UserID = ".$user_id;
                
        if ($this->convert_utf8){
            $this->dbConnectionLatin1();
            $user=current($this->returnArray($sql));
            $this->dbConnectionUTF8();
    
            $returnArray=array(
                empty($user['name'])? $eLib_plus["html"]["anonymous"] : convert2Unicode($user['name']),
                empty($user['class'])? '(--)' : convert2Unicode($user['class']),
                empty($user['image'])? '/images/myaccount_personalinfo/samplephoto.gif' : convert2Unicode($user['image'])
            );
        }else{
            $user=current($this->returnArray($sql));
    
            $returnArray=array(
                empty($user['name'])? $eLib_plus["html"]["anonymous"] : $user['name'],
                empty($user['class'])? '(--)' : $user['class'],
                empty($user['image'])? '/images/myaccount_personalinfo/samplephoto.gif' : $user['image']
            );
        }
        
	return $returnArray;
    }
    public function getUserFavourites(){
	
	$sql = "SELECT c.*
                FROM INTRANET_ELIB_BOOK c
                INNER JOIN INTRANET_ELIB_BOOK_MY_FAVOURITES a on a.BookID=c.BookID and a.UserID=".$this->user_id;
	
	$books = $this->returnArray($sql);
	$returnArray=array();
	foreach($books as $book){
	    $returnArray[]=$this->getBooksArrayByFormat($book);
	}
	
	return $returnArray;
    }
    public function getUserReviews($user_id=false, $class_name=false){
        
        if (!$user_id) {
            if (!$class_name){
                $filter = "b.UserID = ".$this->user_id;
            }else{
                $filter = "b.ClassName = '".$class_name."'";
            }
            
        }else{
            $filter = "b.UserID = ".$user_id;
        }
        $day_a = $this->getDayRangeCondition("a.DateModified");
	
	$sql = "SELECT a.UserID as user_id,
                a.Rating as rating, a.Content as content,
                datediff(now(),a.DateModified) as days,
                a.DateModified as date,
                c.*,
                count(d.ReviewCommentID) as likes,
                a.ReviewID as review_id
                FROM  INTRANET_ELIB_BOOK_REVIEW a
                inner join INTRANET_USER b on a.UserID = b.UserID
                inner join INTRANET_ELIB_BOOK c on a.BookID=c.BookID
                left join INTRANET_ELIB_BOOK_REVIEW_HELPFUL d on a.ReviewID=d.ReviewID AND d.Choose=1
                
                WHERE $filter $day_a
                GROUP BY a.ReviewID
                ORDER BY a.ReviewID DESC";
	
	$books = $this->returnArray($sql);
	$returnArray=array();
	foreach($books as $book){
	    $entry=$this->getBooksArrayByFormat($book);
            $entry['book_id']=$entry['id'];
            $entry['review_id']=$book['review_id'];
	    $entry['user_id']=$book['user_id'];
            list($entry['name'],$entry['class'],$entry['user_image'])=$this->getUserInfo($entry['user_id']);
	    $entry['rating']=$book['rating'];
	    $entry['content']=strip_tags($book['content']);
	    $entry['days']=$book['days'];
	    $entry['date']=$book['date'];
	    $entry['likes']=$book['likes'];
	    $returnArray[]=$entry;
	}
	
	return $returnArray;
        
    }
    private function getDayRangeCondition($field){
        
        switch($this->range){
            case "week":
                return "AND $field between (CURDATE()-INTERVAL DAYOFWEEK(CURDATE())+1 DAY) and NOW()";
            case "month":
                return "AND $field between (CURDATE()-INTERVAL DAYOFMONTH(CURDATE())+1 DAY) and NOW()";
            case "year":
                return "AND $field between (CURDATE()-INTERVAL DAYOFYEAR(CURDATE())+1 DAY) and NOW()";
            default:
                return "";
        }
    }
    private function getBestRateOrMostHitBooks($options){//basically 2 queries are same except order
        
        $elib_install=new elibrary_install();
        
        $day_b = $this->getDayRangeCondition("b.DateModified");
        $day_c = $this->getDayRangeCondition("c.DateModified");
	
	$sql = "SELECT a.*, ifnull(round(avg(b.Rating),1),'--') as rating, count(distinct(b.ReviewID)) as review_count, count(distinct(c.HistoryID)) as hit_rate
                FROM INTRANET_ELIB_BOOK a 
                LEFT JOIN INTRANET_ELIB_BOOK_REVIEW b on a.BookID=b.BookID $day_b
                LEFT JOIN INTRANET_ELIB_BOOK_HISTORY c on a.BookID=c.BookID $day_c
                WHERE a.Publish=1 ".$this->book_formats." 
                GROUP BY a.BookID
                $options";

	$books = $this->returnArray($sql);echo mysql_error();
	$returnArray=array();
	foreach($books as $book){
	    $entry=$this->getBooksArrayByFormat($book);
	    $entry['rating']=$book['rating'];
	    $entry['review_count']=$book['review_count'];
            if ($book['BookFormat']!=$this->physical_book_type_code){
                $entry['hit_rate']=$book['hit_rate'];
                $entry['url']=$elib_install->get_book_reader($book['BookID'],$book['BookFormat'],$book['IsTLF'], $this->user_id);
            }
	    $returnArray[]=$entry;
	    
	}
	
	return $returnArray;
    }
    public function getBestRateBooks($limit=3){ return $this->getBestRateOrMostHitBooks("HAVING review_count>0 ORDER BY rating DESC, review_count DESC, hit_rate DESC LIMIT $limit");}
    
    public function getMostHitBooks($limit=3){ return $this->getBestRateOrMostHitBooks("HAVING hit_rate>0 ORDER BY hit_rate DESC, rating DESC, review_count DESC LIMIT $limit");}
   
    public function getMostLoanBooks($limit=10){
        
        $day_a = $this->getDayRangeCondition("a.BorrowTime");
        $day_c = $this->getDayRangeCondition("c.DateModified");
        
	$sql = "SELECT a.BookID+".$this->physical_book_init_value." as BookID,
                COUNT(DISTINCT(a.BorrowLogID)) as loan_count,
                COUNT(DISTINCT(c.ReviewID)) as review_count,
                ifnull(round(avg(c.Rating),1),'--') as rating
                from ".$this->libmsDB.".LIBMS_BORROW_LOG a
                INNER JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID = b.BookID $day_a
                LEFT JOIN INTRANET_ELIB_BOOK_REVIEW c on a.BookID+".$this->physical_book_init_value."=c.BookID $day_c
                GROUP BY a.BookID
                HAVING loan_count>0 
                ORDER BY loan_count DESC, rating DESC
                LIMIT $limit";
                
        $books = $this->returnArray($sql); 
        $returnArray=array();
        foreach ($books as &$book){
            
            $book['BookFormat']=$this->physical_book_type_code;
            $entry=$this->getBooksArrayByFormat($book);//forced BookFormat so that to get physical book detail
            $entry['loan_count'] = $book['loan_count'];
            $entry['review_count'] = $book['review_count'];
            $entry['rating'] = $book['rating'];
            $returnArray[]=$entry;
        }
        
        return $returnArray;
	
    }
    public function getMostActiveUsers($limit=10){
	
        $day = $this->getDayRangeCondition("a.DateModified");
		
	$sql = "SELECT 
		a.UserID as user_id,
		count(distinct(a.ReviewID)) as review_count
		FROM INTRANET_USER b
                INNER JOIN INTRANET_ELIB_BOOK_REVIEW a on a.UserID = b.UserID $day
                INNER JOIN INTRANET_ELIB_BOOK c where a.BookID=c.BookID
		GROUP BY 
		a.UserID 
		ORDER BY 
		review_count desc
                
		Limit $limit";//ensure INTRANET_ELIB_BOOK has book
				
        $users=$this->returnArray($sql);
        foreach ($users as &$user){
            list($user['name'],$user['class'],$user['image'])=$this->getUserInfo($user['user_id']);
            
        }
        
	return $users;
		
    }
    public function getMostActiveUsersWithReviews($limit=10){
	
        $users = $this->getMostActiveUsers($limit);
        
        foreach ($users as &$user){
            $user['reviews']=$this->getUserReviews($user['user_id']);
        }
	return $users;
		
    }
    public function getMostActiveClasses($limit=10){
	
        
        $day = $this->getDayRangeCondition("a.DateModified");
        
        
	$sql = "SELECT 
		b.ClassName as class,
		count(a.ReviewID) as review_count
		FROM 
		INTRANET_ELIB_BOOK_REVIEW a
		INNER JOIN INTRANET_USER b ON a.UserID = b.UserID
                INNER JOIN INTRANET_ELIB_BOOK c on a.BookID=c.BookID
		WHERE b.ClassName IS NOT NULL AND b.ClassName<>'' $day
		GROUP BY 
		b.ClassName
		ORDER BY 
		review_count desc
		Limit $limit";//ensure INTRANET_ELIB_BOOK has book
				
        if ($this->convert_utf8){
            
            $this->dbConnectionLatin1();
            $classes=$this->returnArray($sql);
            foreach ($classes as &$class){
              
                $class['class']=convert2Unicode($class['class']);
            }
            
            $this->dbConnectionUTF8();
            $classes_raw=$this->returnArray($sql);//get an unconverted classname for getting reviews
            foreach ($classes_raw as $i=>&$class_raw){
              
                $classes[$i][0]=$class_raw['class'];
            }
        }else{
            $classes=$this->returnArray($sql);
        }
        
	return $classes;

    }
    public function getMostActiveClassesWithReviews($limit=10){
	
        $classes = $this->getMostActiveClasses($limit);
       
        foreach ($classes as &$class){

            $class['reviews']=$this->getUserReviews(false, $class[0]);
        }
	return $classes;
		
    }
    public function getMostBorrowUsers($limit=10){
        
        $day = $this->getDayRangeCondition("a.BorrowTime");
       
	$sql = "SELECT a.UserID as user_id, 
                count(a.BookID) as loan_count 
                from ".$this->libmsDB.".LIBMS_BORROW_LOG a 
                INNER JOIN INTRANET_USER b ON a.UserID=b.UserID 
                $day 
                GROUP BY a.UserID 
                ORDER BY loan_count DESC";
                
        $users=$this->returnArray($sql);
        foreach ($users as &$user){
            list($user['name'],$user['class'],$user['image'])=$this->getUserInfo($user['user_id']);
        }
                
        return $users;
                
    }
    public function getMostUsefulReviews($limit=10){
	
        $day = $this->getDayRangeCondition("d.DateModified");	

	$sql = "SELECT 
		c.*,
                a.ReviewID as review_id,
                d.UserID as user_id,
                d.Content as content,
                datediff(now(),d.DateModified) as days,
                d.DateModified as date,
		count(a.ReviewCommentID) likes,
                d.Rating as rating
		FROM INTRANET_ELIB_BOOK_REVIEW_HELPFUL a
                LEFT OUTER JOIN INTRANET_ELIB_BOOK_REVIEW d ON a.ReviewID = d.ReviewID
		LEFT JOIN INTRANET_USER b ON d.UserID = b.UserID
		INNER JOIN INTRANET_ELIB_BOOK c ON c.BookID = a.BookID
		WHERE a.Choose = 1
		AND c.Publish = 1
                $day
		GROUP BY a.ReviewID
		ORDER BY likes desc, rating desc
		Limit $limit";
	$books=$this->returnArray($sql);
            
        $returnArray=array();
	foreach($books as $book){
	    $entry=$this->getBooksArrayByFormat($book);
            $entry['book_id']=$entry['id'];
            $entry['review_id']=$book['review_id'];
	    $entry['user_id']=$book['user_id'];
	    $entry['content']=strip_tags($book['content']);
	    $entry['days']=$book['days'];
            $entry['date']=$book['date'];
	    $entry['likes']=$book['likes'];
            $entry['rating']=$book['rating'];
            list($entry['name'],$entry['class'],$entry['user_image'])=$this->getUserInfo($book['user_id']);
	    $returnArray[]=$entry;
	    
	}
      
	return $returnArray;
	    
    }
    public function getReadingProgress(){

	$elib_install=new elibrary_install();
	
	$sql = "SELECT a.BookID as id, a.Title as title, a.Author as author, a.IsTLF, a.BookFormat, b.Percentage as percentage, max(c.DateModified) as last_accessed
                FROM INTRANET_ELIB_BOOK a
                RIGHT JOIN INTRANET_ELIB_BOOK_HISTORY c on a.BookID=c.BookID
                LEFT JOIN INTRANET_ELIB_USER_PROGRESS b on b.BookID=c.BookID and  b.UserID=c.UserID
                WHERE a.BookFormat<>'".$this->physical_book_type_code."' AND a.BookID IS NOT NULL AND c.UserID=".$this->user_id."
                GROUP BY id
                ORDER BY last_accessed desc
                LIMIT 100";
	
	$books=$this->returnArray($sql);
	foreach($books as &$book){
	    $book['url']=$elib_install->get_book_reader($book['id'],$book['BookFormat'],$book['IsTLF'], $this->user_id);
	}
	
	return $books;
	
    }
    public function getNotificationCount(){
	return "0";
    }
    private function getUserLoanBooks($status, $dateorder){
        $sql = "SELECT b.BookTitle as title,
                a.BookID+".$this->physical_book_init_value." as id,
                a.BorrowLogID as borrow_id,
                a.ReturnedTime as return_date,
                a.BorrowTime as borrow_date,
                a.DueDate as due_date,
                DATEDIFF(now(), a.DueDate) as overdue_days,
                a.RenewalTime as renew_count
                from ".$this->libmsDB.".LIBMS_BORROW_LOG a
                INNER JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID=b.BookID
                where a.UserID=".$this->user_id." and a.RecordStatus='$status'
                ORDER BY borrow_date $dateorder, title asc";
                
                
        if ($this->convert_utf8){
            $this->dbConnectionLatin1();
            $books=$this->returnArray($sql);
            $this->dbConnectionUTF8();
            
        }else{
            
            $books=$this->returnArray($sql);
        }
        
        return $books;
    }
    
    public function getUserLoanBooksCurrent(){ return $this->getUserLoanBooks('BORROWED', 'asc');}
    
    public function getUserLoanBooksHistory(){return $this->getUserLoanBooks('RETURNED', 'desc');}
    
    private function getUserPenalty($status, $dateorder){
        
        
        $sql = "SELECT b.BookTitle as title,
                    a.BookID+".$this->physical_book_init_value." as id,
                    a.DueDate as due_date,
                    if(a.ReturnedTime=0,'--',a.ReturnedTime) as return_date,
                    c.Payment as penalty_total,
                    c.Payment-IFNULL(sum(d.Amount),0) as penalty_owing,
                    IFNULL(c.DaysCount, 'LOST') as penalty_reason
                from ".$this->libmsDB.".LIBMS_OVERDUE_LOG c
                LEFT JOIN ".$this->libmsDB.".LIBMS_TRANSACTION d ON c.OverDueLogID=d.OverDueLogID
                INNER JOIN ".$this->libmsDB.".LIBMS_BORROW_LOG a ON c.BorrowLogID=a.BorrowLogID
                INNER JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID=b.BookID
                where a.UserID=".$this->user_id." and c.RecordStatus='$status' 
                GROUP BY c.OverDueLogID
                ORDER BY return_date $dateorder, title asc";
        
        if ($this->convert_utf8){
            $this->dbConnectionLatin1();
            $books=$this->returnArray($sql);
            $this->dbConnectionUTF8();
            
        }else{
            
            $books=$this->returnArray($sql);
        }
        
        return $books;
    }
    
    public function getUserPenaltyCurrent(){ return $this->getUserPenalty('OUTSTANDING', 'asc');}
    
    public function getUserPenaltyHistory(){ return $this->getUserPenalty('SETTLED', 'desc');}
    
    public function getUserReservedBooks(){
        
        $sql = "SELECT b.BookTitle as title, a.BookID+".$this->physical_book_init_value." as id, a.ReserveTime as reserve_date, a.RecordStatus as status
                from ".$this->libmsDB.".LIBMS_RESERVATION_LOG a
                INNER JOIN ".$this->libmsDB.".LIBMS_BOOK b ON a.BookID=b.BookID
                where a.UserID=".$this->user_id."
                ORDER BY status asc, reserve_date asc";
        $books=$this->returnArray($sql);
        
        if ($this->convert_utf8){
            $this->dbConnectionLatin1();
            $books=$this->returnArray($sql);
            $this->dbConnectionUTF8();
            
        }else{
            
            $books=$this->returnArray($sql);
        }
        return $books;
    }
    private function getUserLimitations($fields){
        
        $sql = "SELECT $fields FROM ".$this->libmsDB.".LIBMS_GROUP_CIRCULATION a INNER JOIN ".$this->libmsDB.".LIBMS_GROUP_USER b ON a.GroupID=b.GroupID WHERE b.UserID=".$this->user_id;
        return current($this->returnVector($sql));
    }
    public function getUserLoanQuota(){ return $this->getUserLimitations("LimitBorrow");}
    
    public function getUserReserveQuota(){ return $this->getUserLimitations("LimitReserve");}
    
    public function getUserRenewQuota(){ return $this->getUserLimitations("LimitRenew");}
    
    public function getUserReturnDuration(){ return $this->getUserLimitations("ReturnDuration");}
    
}
?>