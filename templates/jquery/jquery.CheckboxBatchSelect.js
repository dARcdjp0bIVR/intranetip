(function($) {

	$.fn.CheckboxBatchSelect = function(options) {

		//!!!!!!!!!!!!!!!!!!!! temporarily disabled !!!!!!!!!!!!!!!
		return false; 
		
	 	// build main options before element iteration
		var opts = $.extend({}, $.fn.CheckboxBatchSelect, options);
	 	
	 	var $ObjList = this.filter("input:checkbox");

	 	// iterate and reformat each matched element
	 	return this.filter("input:checkbox").each(function() {
	 	
			$this = $(this);
			
			// build element specific options
			var o = $.meta ? $.extend({}, opts, $this.data()) : opts;

			$this.click(function(){
				$thisCheckbox = $(this);
				$.fn.CheckboxBatchSelect.handleClickAction($thisCheckbox, o, $ObjList);
			});

		});
	};
	
	// check whether shift key is being held down 
	$.fn.CheckboxBatchSelect.isShiftPress = false;
	$(document).keydown(function(event){
		// indicate shift press on shift key down
		if(event.which==16)
			$.fn.CheckboxBatchSelect.isShiftPress = true;
		
	}).keyup(function(event){
		// unset shift press indicator on shift key up
		if(event.which==16)
			$.fn.CheckboxBatchSelect.isShiftPress = false;
	});

	$.fn.CheckboxBatchSelect.LastSelectedCheckbox = null;
	$.fn.CheckboxBatchSelect.LastAction = null;
	
	$.fn.CheckboxBatchSelect.handleClickAction = function($Obj, opts, $ObjList) {
		var thisIsShiftPress = $.fn.CheckboxBatchSelect.isShiftPress;
		var thisLastSelectedCheckbox = $.fn.CheckboxBatchSelect.LastSelectedCheckbox;
		var thisLastAction = $.fn.CheckboxBatchSelect.LastAction;
		var thisSelectedCheckbox = $ObjList.index($Obj);

		if(thisIsShiftPress)
		{
			if(thisLastSelectedCheckbox!=null && thisLastSelectedCheckbox != thisSelectedCheckbox)
			{
				if(thisLastSelectedCheckbox> thisSelectedCheckbox)
				{
					var StartIdx = thisSelectedCheckbox;
					var EndIdx = thisLastSelectedCheckbox;
				}
				else	
				{
					var StartIdx = thisLastSelectedCheckbox;
					var EndIdx = thisSelectedCheckbox;
				}
			
				$ObjList.filter(":lt("+EndIdx+")").filter(":gt("+StartIdx+")").attr("checked", thisLastAction)
				$Obj.attr("checked", thisLastAction)
			}
			
		}
		
		$.fn.CheckboxBatchSelect.LastSelectedCheckbox = $ObjList.index($Obj);
		$.fn.CheckboxBatchSelect.LastAction = $Obj.attr("checked");
	};
	
	$.fn.CheckboxBatchSelect.defaults = {

	};

})(jQuery);