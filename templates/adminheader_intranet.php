<?php
#Modify by: yat
session_unregister_intranet("UserID");
$xmsg = "";
switch($msg){
        case 1: $xmsg = $i_con_msg_add; break;
        case 2: $xmsg = $i_con_msg_update; break;
        case 3: $xmsg = $i_con_msg_delete; break;
        case 4: $xmsg = $i_con_msg_email; break;
        case 5: $xmsg = $i_con_msg_password1; break;
        case 6: $xmsg = $i_con_msg_password2; break;
        case 7: $xmsg = $i_con_msg_email1; break;
        case 8: $xmsg = $i_con_msg_email2; break;
        case 9: $xmsg = $i_con_msg_archive; break;
        case 10: $xmsg = $i_con_msg_import_success; break;
        case 11: $xmsg = $i_con_msg_admin_modified; break;
        case 12: $xmsg = $i_con_msg_cannot_edit_poll; break;
        case 13: $xmsg = $i_con_msg_import_failed; break;
        case 14: $xmsg = $i_con_msg_user_add_failed; break;
        case 15: $xmsg = $i_con_msg_enrollment_next; break;
        case 16: $xmsg = $i_con_msg_enrollment_confirm; break;
        case 17: $xmsg = $i_con_msg_enrollment_lottery_completed; break;
        case 18: $xmsg = $i_con_msg_subject_leader_updated; break;
        case 19: $xmsg = $i_con_msg_import_success_with_update; break;
        case 20: $xmsg = $i_con_msg_record_not_found[0]; break;
        case 21: $xmsg = $i_con_msg_record_not_found[1]; break;
        case 22: $xmsg = $i_con_msg_promo_not_deleted; break;
        case 23: $xmsg = $Lang['ErrorMsg']['InvalidRegNo']; break;
        case 24: $xmsg = $i_con_msg_update_failed; break;
}
$li_menu = new libaccount();
$charset = "UTF-8";
session_unregister_intranet("SSV_PRIVILEGE");

?>


<html>
<head><title><?php echo $i_admin_title; ?></title>
<META http-equiv=Content-Type content='text/html; charset=<?=$charset?>'></head>
<meta http-equiv="Content-Language" content="zh-CN" />
<script language=JavaScript src=/lang/script.<?php echo $intranet_session_language; ?>.js></script>
<script language=JavaScript1.2 src=/templates/script.js></script>
<link rel=stylesheet href=/templates/style.css>
<body topmargin=0 leftmargin=0 marginheight=0 marginwidth=0 bgcolor="#F0FAFA">
<table width=780 border=0 cellpadding=0 cellspacing=0>
<tr>
<td style="background-image:url(/images/admin/menu/menu_bg.gif)" width="175"><?php echo $li_menu->display_menu_intranet($PHP_AUTH_USER); ?></td>
<td bgcolor="#FFFFFF" width="605">