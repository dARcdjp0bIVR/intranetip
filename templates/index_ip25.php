<?php
# using: 

#############################################################
#	Date:	2018-08-08 Carlos
#			- Do js checking on XFS(Cross Frame Scripting). Can use $sys_custom['Allow_embed_eClass_in_iframe'] to exclude for those clients that have used iframe approach.
#
#	Date:	2017-08-14 Paul
#			- add mechanism to decide the use of login UI page also on the project sub-theme flag
#
#	Date:	2017-03-29 Carlos 
#			- remove including of jquery script as security scanner report it has security risk. 
#
#	Date:	2016-10-03 Jason [ip.2.5.7.10.1]
#			- add google button to support login by Google Account, $ssoservice["Google"]["Valid"]
#
#	Date:	2016-05-24 Paul [ip.2.5.7.7.1]
#			- Redirect to PowerFlip login entrance if PowerFlip Lite is using
#
#	Date:	2016-01-13 Jason [ip.2.5.7.1.1]
#			- change the year of copyright from 2009 to 2016
#
#	Date:	2015-10-30 Bill
#			- support API server for HKEdCity EdConnect
#
#	Date:	2015-10-02 Bill
#			- added checking for $ssoservice["HKEdCity"]["Valid"] to display HKEdCity Login button
#
#	Date:	2015-07-16 Carlos
#			- add secure token to prevent CSRF.
#
#	Date:	2015-05-18 Cameron
#			- pass $_GET parameters to customized login page 
#
#	Date:	2015-03-20 Cameron
#			- Change login_title css width from 233px to 253px so that full picture can be shown		
#
#	Date:	2015-03-11 Cameron
#			- Add flag $sys_custom['eLibraryPlus']['eLibraryPlusOnly'] to control showing IP image or eLib+ image
#
#	Date:	2015-01-16	Omas	[ip.2.5.6.3.1]
#			Disable login btn after submit and show a loading image
#			comment style.css , login_form.png
#
#	Date:	2014-09-29	Carlos [ip.2.5.5.10.1]
#			Set $DirectLink from $_SESSION['REDIRECT_URL'] before session destroyed
#
#	Date:	2013-09-26	YatWoon
#			improved: language will depends on client's default language settings [Case#2013-0909-0940-06071]
#
#	Date:	2013-09-09	Ivan [2013-0906-1440-29177]
#			change wording "forgot password" to "Reset Password"
#
#	Date:	2013-05-08	Siuwan
#			add css "title_en" and checking for login_title css with different $intranet_default_lang
#
#	Date:	2013-05-07	Siuwan
#			Comment out two jquery include files 
#
#	Date:	2013-04-26	Siuwan
#			Add meta name="viewport" for handling scale of mobile browser layout 
#
#	Date:	2013-03-12	YatWoon
#			Revised ui
#
#	Date:	2013-01-16	Yuen
#			enable PowerLesson login page (to replace standard login to Intranet) and all users access PL by default
#
#	Date:	2012-12-20	YatWoon
#			Add $AccessLoginPage, allow user to still access the login page during the maintenance
#
#	Date:	2012-10-26	YatWoon
#			Add session_destroy(); [Case#2012-0924-1721-00073]
#
#	Date:	2012-10-19	Yuen
#			Show the link of Parent App Online Form according to $sys_custom["show_parentapp_form"]
#
#	Date:	2012-06-21	YatWoon
#			Change the prompt msg for "forgot password"
#
#	Date:	2011-09-08 YatWoon
#			add LDAP checking <== should be no "forgot password" like
#
#	Date:	20110-08-22	YatWoon
#			change the Login button to "button" from "image" [Case: 2011-0819-0955-43073]
#
#############################################################
@session_start();

/*
 * The session variable 'REDIRECT_URL' remembers the attempted request url before login. It is set at /includes/lib.php intranet_auth().
 * Here assign it to $DirectLink for auto redirect. 
 */
if(isset($_SESSION['REDIRECT_URL']) && $_SESSION['REDIRECT_URL'] != ''){
	$DirectLink = $_SESSION['REDIRECT_URL'];
}


/*** come from SSO ***/
if (isset($_SESSION["ECLASS_SSO"]["client_credentials"]))
{
    /* remove DSI SSO on this Login Page at 20190103 */
    /*
    // 5 min expired
    $expireAfter = 5;
    $secondsInactive = time() - $_SESSION["ECLASS_SSO"]["client_credentials"]["expiredTime"];
    $expireAfterSeconds = $expireAfter * 60;
    if($secondsInactive <= $expireAfterSeconds)
    {
        $client_credentials = $_SESSION["ECLASS_SSO"]["client_credentials"];
    }
    */
}

session_destroy();
if (is_file("../servermaintenance.php") && !$iServerMaintenance && !$AccessLoginPage)
{	
    header("Location: ../servermaintenance.php");
    exit();
} elseif (is_file("login.customized.php"))
{
	if (count($_GET)>0) {
		$para = '?';
		foreach($_GET as $k=>$v) {
			if ($v) {
				$para .= "$k=$v&"; 
			}
		}
		$para = substr($para,0,-1);	// remove last '&'
	}
	else {
		$para = '';
	}
    header("Location: login.customized.php".$para);
    exit();
}
include_once("../includes/global.php");
include_once("../includes/SecureToken.php");
if ($sys_custom['Project_Custom']) {
	if($sys_custom['Project_Use_Sub']){
		if (file_exists($sys_custom['Project_Sub_Label'] ."/login.php") && $sys_custom[$sys_custom['Project_Sub_Label']])
		{
			if (count($_GET)>0) {
				$para = "?" . $_SERVER['QUERY_STRING'];
			}
			else {
				$para = '';
			}
			header("Location: " . $sys_custom['Project_Sub_Label'] . "/login.php".$para);
			exit();
		}
	}
	if (file_exists($sys_custom['Project_Label'] ."/login.php") && $sys_custom[$sys_custom['Project_Label']])
	{
		if (count($_GET)>0) {
			$para = "?" . $_SERVER['QUERY_STRING'];
		}
		else {
			$para = '';
		}
		header("Location: " . $sys_custom['Project_Label'] . "/login.php".$para);
		exit();
	}
}

intranet_opendb();
$SecureToken = new SecureToken();

if ($plugin["platform"]=="KIS")
{
    header("Location: ../kis/");
    exit();
} elseif (isset($plugin['PowerLesson_Login']) && $plugin['PowerLesson_Login'] && is_file("../pl/index.php"))
{
    header("Location: ../pl/");
    exit();	
}elseif (isset($plugin['PowerFlip_lite']) && $plugin['PowerFlip_lite'] && is_file("../pf/index.php"))
{
    header("Location: ../pf/");
    exit();	
}
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'b5';
}
include_once("../lang/lang.$intranet_session_language.php");

$title_image = ($sys_custom['eLibraryPlus']['eLibraryPlusOnly']) ? "title_library_b5.png" : "title_b5.png";

if ($sys_custom['eClassApp']['showLoginPageDownloadApkLink']) {
	$eClassAppApkDownloadLink = '<span class="text_other" style="font-size:12px;"><a href="http://www.eclass.com.hk/apk/eclass_app_apk.php" target="_epaform">Download eClass App apk for Android</a></span>';
}

//debug_pr(Get_IP_Version());
$platformVersion = Get_IP_Version();
$platformVersionAry = explode('.', $platformVersion);
$copyRightYear = 2009 + $platformVersionAry[3];

$is_from_SSO = false;
/* remove DSI SSO on this Login Page at 20190103 */
/*
if (isset($client_credentials))
{
    session_start();
    $is_from_SSO = true;
    $_SESSION["ECLASS_SSO"]["client_credentials"] = $client_credentials;
}
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=yes">
<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<title>eClass Integrated Platform 2.5</title>
<style type="text/css">
<!--
html {}
body { margin:0; padding:0; min-height:600px; background: url(../images/bg.jpg) fixed center;  font-family: Verdana, "微軟正黑體"; font-size:12px;}
p.spacer{clear:both; display:block; line-height:0px; font-size:0px; padding:0; margin:0; height:0px}
a { color:#0066CC; text-decoration:none;}
a:hover { color:#FF0000}
#wrapper { display:block; width:670px; margin:0 auto;}
.main_container {position:absolute; top:50%; height:320px; margin-top:-160px; width:670px;/* negative half of the height */}
.login_page { width:100%; height:320px; background:url(../images/board.png); position:relative}
#login_title {	background-repeat:no-repeat;	background-position:left top;	width:253px;	height: 67px;	display:block;	position:absolute;	left:235px;	top:23px;}
.title_b5 { background-image:url(../images/<?=$title_image?>)}
.title_en { background-image:url(../images/<?=$title_image?>)}
.title_gb { background-image:url(../images/title_gb.png)}
.title_comm { background-image:url(../images/title_comm.png)}
.title_1 { background-image:url(../images/title_1.png)}
<?php /* .login_form {display:block;	position:absolute;	background:url(../images/login/login_form.png) no-repeat left top;	width:420px;	height: 165px;	left: 235px;	top: 85px;} */ ?>
.login_form {display:block;	position:absolute;	width:420px;	height: 165px;	left: 235px;	top: 85px;}
.login_form span, .login_form input, .login_form a {	position:absolute;	font-family:Verdana, Arial, Helvetica, sans-serif;	display:block;}
span.text_login {	width:100px;	left:35px;	top:40px;}
span.text_password {	width:100px;	left:35px;	top:67px;}
span.text_other {	left:35px;	width:270px;	top:132px;	height:30px;	font-weight:bold}
.login_form input.user_loginname {	width:220px;	padding:1px 5px;	height:18px;	border: 1px solid #999999;	left:140px;	top:38px}
.login_form input.user_password {	width:220px;	height:18px;	padding:1px 5px;	border: 1px solid #999999;	left:140px;	top:66px}
.login_form .login_btn{	display:block;	border:0;	background-image:url(../images/btn_bg.gif);	height:26px;	color:#666666;	line-height:26px;	font-weight:bold;	text-align:center;	width:61px;	left:310px;	top:107px}
.login_form .login_btn:hover {background-image:url(../images/btn_bg_on.gif);color:#FF0000;}
span#loading {left:380px; top:112px;}
a.forgot_password {	width:175px;	font-size:0.9em;	height:20px;	left:35px;	top:105px;	color: #3bac24;	text-decoration: none;}
a.forgot_password:hover {	color: #FF0000;}
.error_msg {	display:block;	position:absolute;	left: 36px;	top: 6px;	width: 330px;	height: 30px;	font-size:0.9em;	color: #FF0000;	font-weight: bold;}
div.Go_link {	display:block;	position:absolute;	right:23px;	height:25px;	width:172px;	top:61px;	text-align:right;}
div.Go_link a{		font-size: 11px;	color: #003399;}
div.Go_link a:hover{	color: #FF0000;}
#footer {	position:absolute;	top:252px;	right:15px;width: 590px; line-height:20px; color:#FFFFFF;font-size:0.9em; text-align:right; }
#footer a { color:#fff }
#footer a:hover {text-decoration:underline}
.support_info {	color: #000;	font-size:0.9em;	position:absolute;	right:15px;	bottom:25px;	width: 535px;	height: 23px;	text-align:right;}
.support_info a{ color: #003399; text-decoration:none;}
.support_info a:hover {text-decoration:underline}
.done_msg {display:block; position:absolute; left: 36px; top: 6px; width: 330px; height: 30px; font-size:0.9em; color: #009900; font-weight: bold;}

<?php if($ssoservice["HKEdCity"]["Valid"]){ ?>
.login_form .hkedcity {	display:block; border:0; background-image:url(../images/EdConnect_login_btn_text_<?=$title_lang?>.png); background-color:transparent; line-height:35px; font-weight:bold; text-align:center; height:35px; width:168px; left:246px; top:-74px }
.login_form .hkedcity:hover { background-image:url(../images/EdConnect_login_btn_text_<?=$title_lang?>_on.png); }
a.forgot_password {	width:96px;	font-size:0.9em; height:20px; left:35px; top:105px;	color: #3bac24;	text-decoration: none;}
<?php } ?>

<?php if($ssoservice["Google"]["Valid"]){ ?>
.login_form .ssogoogle {	display:block; border:0; background-image:url(../images/btn_google_signin_light_normal_web.png); background-color:transparent; line-height:35px; font-weight:bold; text-align:center; height:46px; width:191px; left:225px; top:202px }
.login_form .ssogoogle:hover { background-image:url(../images/btn_google_signin_light_focus_web.png); cursor:pointer; }
a.forgot_password {	width:96px;	font-size:0.9em; height:20px; left:35px; top:105px;	color: #3bac24;	text-decoration: none;}
<?php } ?>


<?php if ($is_from_SSO) { ?>
.sso_title { color: #3bac24; font-weight:bold; padding-top:10px; padding-left:35px; }
.error_msg { top:110px; width:250px; }
<?php } ?>
?>
-->
</style>
<? /* ?>

    <script type="text/javascript" src="http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js"></script>
    <script type="text/javascript" src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>

<? */ ?> 
<script language=JavaScript src=/lang/script.<?=$title_lang?>.js></script>
<script language=JavaScript1.2 src=/templates/script.js></script>
<script language="javascript">
function signInWithGoogle(){
	<? if($ssoservice["Google"]['Enable_SubDomain']['blers'] == true){ ?>
	var GoogleSSOMsg = 'Please enter your google email address';
	tmp = prompt(GoogleSSOMsg, ".broadlearning.com");
	if(tmp!=null && Trim(tmp)!=""){
		location.href = "./login.googlesso.php?gmail="+tmp;
	}
	<? } else { ?>
	location.href = "./login.googlesso.php";
	<? } ?>
}

function checkLoginForm(){
	var login_btn = document.getElementById('login_btn');
	login_btn.disabled = true;
	var obj = document.form1;
	var pass=1;
	
	if(!check_text(document.getElementById('UserLogin'), "Please enter the user login."))
		pass  = 0;
	else if(!check_text(document.getElementById('UserPassword'), "Please enter the password."))
		pass = 0;
	
	if(pass)	
	{
		document.getElementById('loading').style.display = '';
		return true;
	}
	login_btn.disabled = false;
	return false;
}

function checkForgetForm(){
     var obj = document.form2;
     tmp = prompt(ForgotPasswordMsg, "");
     if(tmp!=null && Trim(tmp)!=""){
          obj.UserLogin.value = tmp;
          obj.submit();
     }
}
</script>    
</head>

<body>
<form name="form1" action="../login.php" method="post" onSubmit="return checkLoginForm();">
<div id="wrapper">
	<div class="main_container">
     <div class="login_page">  
          <div id="login_title" class="title_<?=$title_lang?>"></div>
            <div class="login_form">
            	<?php if ($is_from_SSO) { ?>
            		<div class="sso_title">Sign in with your eClass Account</div>
            		<input type='hidden' name='_SsoFAS_' value='<?php echo $_SESSION["ECLASS_SSO"]["client_credentials"]["client_id"]; ?>'>
            	<?php } ?>
         			<span class="text_login"><?=$Lang['AccountMgmt']['LoginID']?></span>
                    <span class="text_password"><?=$Lang['Gamma']['Password']?></span>
                    
                    <? if ($sys_custom["show_parentapp_form"]) { ?>
                    <span class="text_other"><a href="http://eclass.com.hk/parentapp/" target="_epaform">eClass Parent App Order Form</a></span>
                    <? } ?>
                    <?=$eClassAppApkDownloadLink?>
                    
              <input class="user_loginname" type="text" name="UserLogin" id="UserLogin"/>
              <input class="user_password" type="password" name="UserPassword" id="UserPassword"/>
              		<span id="loading" style="display:none"><img src="/images/<?=$LAYOUT_SKIN?>/indicator.gif"></span>
              		<?php if($ssoservice["HKEdCity"]["Valid"] && !$is_from_SSO){ ?>
              		<input class="hkedcity" type="button" value="" onclick="document.form3.submit();" />
              		<?php } ?>
              		<?php if($ssoservice["Google"]["Valid"] && !$is_from_SSO){ ?>
              		<input class="ssogoogle" type="button" value="" onclick="signInWithGoogle()" />
              		<?php } ?>
                    <input name="submit" type="submit" class="login_btn" id="login_btn" value="<?=$Lang['LoginPage']['Login']?>" />
                    
                    <? if (!($intranet_authentication_method=="LDAP") && !$is_from_SSO) {?>
                   	<!--a href="javascript:checkForgetForm();" class="forgot_password">forgot password?</a-->
                   	<a href="javascript:checkForgetForm();" class="forgot_password"><?=$Lang['ForgotHashedPassword']['ResetPassword']?></a> 
                   	<? } ?>
                   	
                   	<?php if($err==1) { ?><div class="error_msg"><?=$Lang['LoginPage']['IncorrectLogin']?></div><?php } ?>
					<?php /* if($msg==1) { ?><div class="done_msg"><?=$intranet_authentication_method == 'HASH'?"Password resetting request has been sent.":"Password has been sent."?></div><?php } */ ?>
					<?php /* if($msg==1) { ?><div class="done_msg"><?=$intranet_authentication_method == 'HASH'?"Password resetting request has been sent.":"Reset password email has been sent."?></div><?php } */ ?>
					<?php if($msg==1) { ?><div class="done_msg"><?=$Lang['LoginPage']['RequestSent']?></div><?php } ?>
					<?php if($err==2) { ?><div class="error_msg"><?=$Lang['LoginPage']['IncorrectLogin']?></div><?php } ?>
					<?php if($ssoservice["HKEdCity"]["Valid"] && $err==5) { ?><div class="error_msg"><?=$Lang['HKEdCity']['NotLinkToHKEdCityAcct']?></div><?php } ?>
						   
	   </div>
	   
	   <? if(($plugin['attendancestudent'] && $plugin['ppc_attendance']) || $plugin['ppc_eclass'] || ($plugin['Disciplinev12'] && $plugin['ppc_disciplinev12'])){?>
      <!-- <div class="Go_link">&nbsp;<a href="/ppc/">PocketPC Version<img src="../images/icon_palm.gif" width="26" height="22" border="0" align="absmiddle"></a></div> -->
       <? } ?>
	   
       <div id="footer">
   		  <span>Copyright © <?php echo $copyRightYear ?> BroadLearning Education (Asia) Limited. All rights reserved. </span>          </div>
    		<div class="support_info"><a href="http://www.google.com/chrome" target="_blank"><strong>Chrome</strong></a> and 1024x768 or above resolution is recommended.</div>
     </div>
 
     
     
    
	</div>
	<!-- end main_container -->
</div><!-- end wrapper -->

<input type="hidden" name="url" value="/templates/index.php?err=1&DirectLink=<?=rawurlencode($DirectLink)?>">
<input type="hidden" name="AccessLoginPage" value="<?=htmlspecialchars($AccessLoginPage)?>">
<input type="hidden" name="DirectLink" value="<?=htmlspecialchars($DirectLink)?>">
<?=$SecureToken->WriteFormToken()?>
<?php //<input type="hidden" name="target_url" value="/home/eLearning/eclass/"> ?>
</form>

<form name=form2 action=../forget.php method=post>
<input type=hidden name=UserLogin>
<input type=hidden name=url_success value="/templates/index.php?msg=1">
<input type=hidden name=url_fail value="/templates/index.php?err=2">
<?=$SecureToken->WriteFormToken()?>
</form>

<?php if($ssoservice["HKEdCity"]["Valid"]){
	include_once("../includes/libhkedcity.php");
	$lhkedcity = new libhkedcity();
?>
<form name="form3" action="<?=$lhkedcity->getCentralServerPath()?>" method="post">
<input type="hidden" name="HKEdCity" value="<?=$lhkedcity->getSendHKEdCityData()?>">
</form>
<?php } ?>

</body>

<SCRIPT language=javascript>
function openForgetWin () {
win_size = "resizable,scrollbars,status,top=40,left=40,width=650,height=600";
url = null;
if (url != null && url != "") 
{
    var forgetWin = window.open (url, 'forget_password', win_size);
    if (navigator.appName=="Netscape" && navigator.appVersion >= "3") forgetWin.focus();
}
}

document.getElementById('UserLogin').focus();
<?php if($msg==1) { ?>openForgetWin();<?php } ?>

<?php if(!$sys_custom['Allow_embed_eClass_in_iframe']){ ?>
if (top != self){
	top.location = self.location;
}
<?php } ?>
</script>

</html>
<?php
intranet_closedb();
?>