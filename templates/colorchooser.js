     var perline =7;
     var divSet = false;
     var curId;
	 var colorLevels = Array('F','C','9','6','3','0');
     var colorLevels2 = Array('F','C','9','6','3','0');
     var colorLevels3 = Array('F','C','9','6','3','0');

     var colorArray = Array();
     var ie = false;
     var nocolor = 'none';
	 if (document.all) { ie = true; nocolor = ''; }
	 function getObj(id) {
		//if (ie) { return document.all[id]; } 
		//else {	return document.getElementById(id);	}
		
		return document.getElementById(id)
	 }

	 
	 function chargeColor(curId,color){
     	var link = getObj(curId);
     	var field = getObj(curId + 'Field');
 
     	field.value = color;
     	if (color == '') {
	     	document.getElementById(curId+'Div').style.background =nocolor;
     	} 
     	else {
		    if (color.substr(0, 1) != '#')
		    {
		        color = '#'+color;
		        field.value=color;
		    }

		//check length of the color
		    if (color.length == 4)
		    {
		        r = color.substr(1, 1);
		        r += r;
		        g = color.substr(2, 1);
		        g += g;
		        b = color.substr(3, 1);
		        b += b;
			    r = parseInt(r, 16);
			    g = parseInt(g, 16);
			    b = parseInt(b, 16);
			    if (isNaN(r) || isNaN(g) || isNaN(b))
			    {
			       document.getElementById(curId+'Div').style.background =nocolor;
			    }
			    else{			    
			    	document.getElementById(curId+'Div').style.background = color;
		    	}
		    }
		    else if (color.length == 7)
		    {
		        r = color.substr(1, 2);
		        g = color.substr(3, 2);
		        b = color.substr(5, 2);
			    r = parseInt(r, 16);
			    g = parseInt(g, 16);
			    b = parseInt(b, 16);
			    if (isNaN(r) || isNaN(g) || isNaN(b))
			    {
			       document.getElementById(curId+'Div').style.background =nocolor;
			    }
			    else{			    
			    	document.getElementById(curId+'Div').style.background = color;
		    	}
		    }
		    else{
			    r='';
			    g='';
			    b='';
			    document.getElementById(curId+'Div').style.background =nocolor;
		    }




	    }

	    eval(getObj(curId + 'Field').title);		 
	 }
     
     function setColor(color) {
     	var link = getObj(curId);
     	
     	//var field = getObj('hidden_'+curId);
     	var picker = getObj('colorpicker');
     	//field.value = color;
     	if (color == '') {
	     //	link.style.background = nocolor;
	     //	link.style.color = nocolor;
	     //	color = nocolor;
	     	document.getElementById(curId+'Div').style.background =nocolor;
     	} else {
	     //	link.style.background = color;
	     //	link.style.color = color;
	     document.getElementById(curId+'Div').style.background =color;
	    }
     	picker.style.display = 'none';
//	    eval(getObj('hidden_'+curId).title);
	   // preview();
	    if (setColor.preview_callback) setColor.preview_callback(color);	    
     }
        
     function setDiv() { 
     	if (!document.createElement) { return; }
        var elemDiv = document.createElement('div');
        if (typeof(elemDiv.innerHTML) != 'string') { return; }
        //genColors();
        elemDiv.id = 'colorpicker';
        elemDiv.className= 'selectbox_layer';
	    elemDiv.style.position = 'absolute';
        elemDiv.style.display = 'none';
        elemDiv.style.visibility = 'visible';
        elemDiv.style.border = '#000000 1px solid';
        elemDiv.style.background = '#FFFFFF';
     
    /*   
		elemDiv.innerHTML =  '<span style="font-family:Verdana; font-size:11px;"> Pick Color'
	//	+ '(<a href="javascript:setColor(\'\');">Transparent</a>)<br>'
		+ getColorTable() 
		+ '</span>';
	*/
		
		var tableCode = '';
		
		elemDiv.innerHTML =  '<table border="0" cellspacing="1" cellpadding="1">'
		+ '<tr><td colspan="6" style="font-family:Verdana; font-size:11px">Color Picker</td>'
		+ getColorTable() 
		+ '<tr><td colspan="7" style="padding-top:4px"><input onfocus="colorPickerMsglive($(this))" type="text" id="colorPickerTextBox1" style="font-family:Verdana; width:100%; font-size:11px; display:inline-block" tabindex="0" onkeyup="colorChangelive($(this),event)" value="#FFFFFF"></td></tr>'
		+ '</table>';

        document.body.appendChild(elemDiv);
        divSet = true;
        
     }
     function colourNameToHex(colour)
	 {
	 	var colours = {"aliceblue":"#f0f8ff","antiquewhite":"#faebd7","aqua":"#00ffff","aquamarine":"#7fffd4","azure":"#f0ffff",
	 	"beige":"#f5f5dc","bisque":"#ffe4c4","black":"#000000","blanchedalmond":"#ffebcd","blue":"#0000ff","blueviolet":"#8a2be2","brown":"#a52a2a","burlywood":"#deb887",
	 	"cadetblue":"#5f9ea0","chartreuse":"#7fff00","chocolate":"#d2691e","coral":"#ff7f50","cornflowerblue":"#6495ed","cornsilk":"#fff8dc","crimson":"#dc143c","cyan":"#00ffff",
	 	"darkblue":"#00008b","darkcyan":"#008b8b","darkgoldenrod":"#b8860b","darkgray":"#a9a9a9","darkgreen":"#006400","darkkhaki":"#bdb76b","darkmagenta":"#8b008b","darkolivegreen":"#556b2f",
	 	"darkorange":"#ff8c00","darkorchid":"#9932cc","darkred":"#8b0000","darksalmon":"#e9967a","darkseagreen":"#8fbc8f","darkslateblue":"#483d8b","darkslategray":"#2f4f4f","darkturquoise":"#00ced1",
	 	"darkviolet":"#9400d3","deeppink":"#ff1493","deepskyblue":"#00bfff","dimgray":"#696969","dodgerblue":"#1e90ff",
	 	"firebrick":"#b22222","floralwhite":"#fffaf0","forestgreen":"#228b22","fuchsia":"#ff00ff",
	 	"gainsboro":"#dcdcdc","ghostwhite":"#f8f8ff","gold":"#ffd700","goldenrod":"#daa520","gray":"#808080","green":"#008000","greenyellow":"#adff2f",
	 	"honeydew":"#f0fff0","hotpink":"#ff69b4",
	 	"indianred ":"#cd5c5c","indigo ":"#4b0082","ivory":"#fffff0","khaki":"#f0e68c",
	 	"lavender":"#e6e6fa","lavenderblush":"#fff0f5","lawngreen":"#7cfc00","lemonchiffon":"#fffacd","lightblue":"#add8e6","lightcoral":"#f08080","lightcyan":"#e0ffff","lightgoldenrodyellow":"#fafad2",
	 	"lightgrey":"#d3d3d3","lightgreen":"#90ee90","lightpink":"#ffb6c1","lightsalmon":"#ffa07a","lightseagreen":"#20b2aa","lightskyblue":"#87cefa","lightslategray":"#778899","lightsteelblue":"#b0c4de",
	 	"lightyellow":"#ffffe0","lime":"#00ff00","limegreen":"#32cd32","linen":"#faf0e6",
	 	"magenta":"#ff00ff","maroon":"#800000","mediumaquamarine":"#66cdaa","mediumblue":"#0000cd","mediumorchid":"#ba55d3","mediumpurple":"#9370d8","mediumseagreen":"#3cb371","mediumslateblue":"#7b68ee",
	 	"mediumspringgreen":"#00fa9a","mediumturquoise":"#48d1cc","mediumvioletred":"#c71585","midnightblue":"#191970","mintcream":"#f5fffa","mistyrose":"#ffe4e1","moccasin":"#ffe4b5",
	 	"navajowhite":"#ffdead","navy":"#000080",
	 	"oldlace":"#fdf5e6","olive":"#808000","olivedrab":"#6b8e23","orange":"#ffa500","orangered":"#ff4500","orchid":"#da70d6",
	 	"palegoldenrod":"#eee8aa","palegreen":"#98fb98","paleturquoise":"#afeeee","palevioletred":"#d87093","papayawhip":"#ffefd5","peachpuff":"#ffdab9","peru":"#cd853f","pink":"#ffc0cb","plum":"#dda0dd","powderblue":"#b0e0e6","purple":"#800080",
	 	"red":"#ff0000","rosybrown":"#bc8f8f","royalblue":"#4169e1",
	 	"saddlebrown":"#8b4513","salmon":"#fa8072","sandybrown":"#f4a460","seagreen":"#2e8b57","seashell":"#fff5ee","sienna":"#a0522d","silver":"#c0c0c0","skyblue":"#87ceeb","slateblue":"#6a5acd","slategray":"#708090","snow":"#fffafa","springgreen":"#00ff7f","steelblue":"#4682b4",
	 	"tan":"#d2b48c","teal":"#008080","thistle":"#d8bfd8","tomato":"#ff6347","turquoise":"#40e0d0",
	 	"violet":"#ee82ee",
	 	"wheat":"#f5deb3","white":"#ffffff","whitesmoke":"#f5f5f5",
	 	"yellow":"#ffff00","yellowgreen":"#9acd32"};
	 	
	 	if (typeof colours[colour.toLowerCase()] != 'undefined')
	 		return colours[colour.toLowerCase()];
	 		
	 	return false;
	 }
     function colorChangelive(obj,e){
     	objContent = $.trim(obj.val());
     	if(objContent.match(/(^#[0-9A-Fa-f]{6}$)|(^#[0-9A-Fa-f]{3}$)/)||colourNameToHex(objContent)){
	     	objA = $('#colorPickerShowBox1');
	     	objA.css('background', objContent);
	     	objA.css('color', objContent);
	     	objA.attr('title', objContent);
	     	objA.attr('href', "javascript:setColor('"+objContent+"')");
	     	if(e.keyCode==13){
	     		javascript:setColor(objContent);
	     	}
     	}
     }
     function colorPickerMsglive(obj){
     	objContent = $.trim(obj.val());
     	if($('#colorPickerShowBox1').attr('notchanged')==1){
     		$('#colorPickerShowBox1').removeAttr('notchanged');
     		obj.val('#');
     	}else if(objContent==$('#colorPickerShowBox1').attr('title')){
     	
     	}else {
     		obj.val('#');
     	}
     }
     
     function pickColor(id) {
     	//if (!divSet) { setDiv();  }
     	var picker = getObj('colorpicker');
     	
		if (id == curId && picker.style.display == 'block') {
			picker.style.display = 'none';
			return;
		}
     	curId = id;
		curColor = $('#'+curId+'Div').css('backgroundColor');
		if(curColor.match(/^rgb[() ,0-9]+/)){
			curColor = curColor.replace(/[rgb() ]/gi,'');
			curColor = curColor.split(",");
			curColor[0] = parseInt(curColor[0]).toString(16);
			curColor[1] = parseInt(curColor[1]).toString(16);
			curColor[2] = parseInt(curColor[2]).toString(16);
			curColor = '#'+(curColor[0]+curColor[0]).slice(-2)+(curColor[1]+curColor[1]).slice(-2)+(curColor[2]+curColor[2]).slice(-2);
		} 
		curColor = curColor.toUpperCase();
		objA = $('#colorPickerShowBox1');
     	objA.attr('notchanged','1');
     	objA.css('background', curColor);
     	objA.css('color', curColor);
     	objA.attr('title', curColor);
     	objA.attr('href', "javascript:setColor('"+curColor+"')");
		$('#colorPickerTextBox1').val(curColor);
		

     //	var thelink = getObj(id);
     	var thelink = document.getElementById(id+'Div');
     	picker.style.top = getAbsoluteOffsetTop(thelink)+20 ;
     	picker.style.left = getAbsoluteOffsetLeft(thelink)-120;     
		picker.style.display = 'block';
		
		
     }
     

     function setColorPattern(){
	    var i,j,k;
		var l=0;
		var colorArray = new Array();
	/*	for (i in colorLevels){
			for (j in colorLevels2){
				for (k in colorLevels3){
					if(k !="in_array" && i != "in_array" && j != "in_array"){
						cur_color = '#'+colorLevels[i]+colorLevels[i]+colorLevels2[j]+colorLevels2[j]+colorLevels3[k]+colorLevels3[k];
						
						if(cur_color == "#FF0000" || cur_color=="#6600FF" || cur_color== "#3300FF" || cur_color=="#009966"){}
						
						colorArray[l] = '#'+colorLevels[i]+colorLevels[i]+colorLevels2[j]+colorLevels2[j]+colorLevels3[k]+colorLevels3[k];						
						l++;
					}
					
				}
			}	
		}*/
		
		// dark grey to light grey
		colorArray[colorArray.length] = "#FFFFFF";
		colorArray[colorArray.length] = "#D9D9D9";
		colorArray[colorArray.length] = "#BBBBBB";
		colorArray[colorArray.length] = "#AAAAAA";
		colorArray[colorArray.length] = "#808080";
		colorArray[colorArray.length] = "#666666";
		colorArray[colorArray.length] = "#000000";
		
		colorArray[colorArray.length] = "#FF0000";	// red
		colorArray[colorArray.length] = "#FF8800";	// orange
		colorArray[colorArray.length] = "#000066";	// dark blue
		colorArray[colorArray.length] = "#3300FF";	// light blue
		colorArray[colorArray.length] = "#6600FF";	// purple
		colorArray[colorArray.length] = "#006600";	// dark green
		colorArray[colorArray.length] = "#00CC00";	// light green
		
		colorArray[colorArray.length] = "#FF6666";
		colorArray[colorArray.length] = "#FFDD44";
		colorArray[colorArray.length] = "#3366CC";
		colorArray[colorArray.length] = "#33CCEE";
		colorArray[colorArray.length] = "#CCAAEE";
		colorArray[colorArray.length] = "#009900";
		colorArray[colorArray.length] = "#66FF99";
		
		
		
	     return colorArray;
     }


     
     function getColorTable() {
		// var colors = colorArray;
		var colors = setColorPattern();						    

		tableCode = '';
        // tableCode += '<table border="0" cellspacing="1" cellpadding="1" >\n';
      	 tableCode += '<td valign="bottom"><a id="colorPickerShowBox1" onmouseover="$(\'#colorPickerTextBox1\').val($(this).attr(\'title\'))" style="border: 1px solid #000000; color: ' 
      	  + colorArray[0] + '; background: ' + colorArray[0] + ';font-size: 10px;" notchanged="1" title="' 
      	  + colorArray[0] + '" href="javascript:setColor(\'' + colorArray[0] + '\');">&nbsp;&nbsp;&nbsp;&nbsp;</a></td></tr>';
         for (i = 0; i < colors.length; i++) {

              if (i % perline == 0) { tableCode += '<tr>'; }
              tableCode += '<td ><a onmouseover="$(\'#colorPickerTextBox1\').val(\''+colors[i]+'\');$(\'#colorPickerShowBox1\').css(\'background\',\''+colors[i]+'\')" onmouseout="$(\'#colorPickerTextBox1\').val($(\'#colorPickerShowBox1\').attr(\'title\'));$(\'#colorPickerShowBox1\').css(\'background\',$(\'#colorPickerShowBox1\').attr(\'title\'))" style="border: 1px solid #000000; color: ' 
              	  + colors[i] + '; background: ' + colors[i] + ';font-size: 10px;" title="' 
              	  + colors[i] + '" href="javascript:setColor(\'' + colors[i] + '\');">&nbsp;&nbsp;&nbsp;&nbsp;</a></td>\n';
              if (i % perline == perline - 1) { tableCode += '</tr>'; }
         }
         if (i % perline != 0) { tableCode += '</tr>'; }
       //  tableCode += '</table>';
      	 return tableCode;
     }
     function relateColor(id, color) {
     	var link = getObj(id);
     	if (color == '') {
	     	//link.style.background = nocolor;
	     	//link.style.color = nocolor;
	     	//color = nocolor;
	     	
     	} else {
	     	link.style.background = color;
	     	link.style.color = color;
	    }
	    eval(getObj(id + 'Field').title);
     }
     function getAbsoluteOffsetTop(obj) {
		
     	var top = obj.offsetTop;
     	var parent = obj.offsetParent;

     	while (parent != document.body) {
     		top += parent.offsetTop;
     		parent = parent.offsetParent;
     	}
     	return top;
     }
     
     function getAbsoluteOffsetLeft(obj) {
     	var left = obj.offsetLeft;
     	var parent = obj.offsetParent;
     	while (parent != document.body) {
     		left += parent.offsetLeft;
     		parent = parent.offsetParent;
     	}
     	return left;
     }

   

