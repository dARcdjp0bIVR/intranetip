// common function for going to specific scheme
function doScheme(jParSchemeID){
	if (!document.form1){
		alert("Related Infomation (form) cannot be found! Cannot perform action.");
	}
	else
	{
		if(document.form1.scheme_id)
		{
			document.form1.scheme_id.value = jParSchemeID;
			document.form1.action = "index.php";
			document.form1.submit();		
		}
		else{
			alert("Related Infomation (scheme id) cannot be found! Cannot perform action.");
		}

	}
}

function goToStage(scheme_id, stage_id) {

	document.form1.scheme_id.value = scheme_id;
	document.form1.stage_id.value = stage_id;
	document.form1.action = "stage.php";
	document.form1.submit();
}

function goToCoverPage(schemeID,stageID)
{
	var goURL = "coverpage.php?scheme_id="+schemeID+"&stage_id="+stageID;
	window.location = goURL; 
}

function insertTemplateAtCursor(id, myValue) {
	myField = document.getElementById(id);
	//IE support
	if (document.selection) {		
		myField.focus();
		sel = document.selection.createRange();	
		sel.text = myValue;		
		sel.moveStart('character', -myValue.length);
		sel.select();
	}
	//FF 
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
						+ myValue
						+ myField.value.substring(endPos, myField.value.length);
		
		myField.selectionStart = startPos;
		myField.selectionEnd = startPos + myValue.length;
		myField.focus();
	} 
	//another
	else {
		myField.value += myValue;
		myField.focus();
	}
}

function goOtherStudent(ParKey) {
	$targetPage = $("form[name=form1]");
	$("input[name=key]").val(ParKey);
	$targetPage.attr("action","");
	$targetPage.submit();
}
function goGroupingList() {
	$targetPage = $("form[name=form1]");
	// $targetPage.attr("action","questionnaireGroupingList.php");
	$('input#mod').val('survey');
	$('input#task').val('questionnaireGroupingList');
	$targetPage.submit();
}

function goGrouping() {
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","questionnaireGrouping.php");
	$targetPage.submit();
}

function goDisplay() {
	$targetPage = $("form[name=form1]");
	//$targetPage.attr("action","questionnaireDisplay.php");
	$('input#mod').val('survey');
	$('input#task').val('questionnaireDisplay');
	$targetPage.submit();
}

function goManagement() {
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","management.php");
	$targetPage.submit();
}

function goDiscoveryList() {
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","questionnaire_discovery_list.php");
	$targetPage.submit();
}
function goNewAnalysis(){
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","questionnaireGroupingNew.php");
	$targetPage.submit();
}