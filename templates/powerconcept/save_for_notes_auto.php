<?php
/* Modify by  */
/**
 * Log:
 * 
 * 2011-05-13 Yuen:
 * 		- addopt the settings for default file permission (default: R)
 * 
 */

include_once('../../../system/settings/global.php');
include_once("../../includes/php/lib-db.php");
include_once('../../includes/php/lib-user.php');
include_once('../../includes/php/lib-filemanager.php');
include_once('../../includes/php/lib-courseinfo.php');
include_once('../../../system/settings/lang/'.$ck_default_lang.'.php');

opendb();


# adopting default file permission

$lc = new courseinfo(0);

list($sfunction, $afunction, $gfunction, $setControls) = $lc->getRightAndSettings();
$new_file_right1 = strstr($setControls,'new_file_right:1');
$new_file_right2 = strstr($setControls,'new_file_right:2');
$new_file_right3 = strstr($setControls,'new_file_right:3');
if (!$new_file_right1 && !$new_file_right2 && !$new_file_right3)
{
	$new_file_right2 = true;
}
if ($new_file_right2)
{
	$DefaultFilePermission = "AR";
} elseif ($new_file_right3)
{
	$DefaultFilePermission = "AW";
}
# end of adopting default file permission


$categoryID = empty($categoryID)?0:$categoryID;
$fm = new fileManager($ck_course_id, 0, "");

# check if new Concept Map exists, otherwise, no need to save
$pc_file = $file_path."/".$fm->db."/tmp/$ck_user_id/$fileName";

if (!file_exists($pc_file))
{
	header("Location: ../../course/contents/new.php?fromCC=1&fileID=$fileID&fileName=$pc_filename&isFileNew=$isFileNew&name=$name");				
	//die("<script language='JavaScript'>\n self.close();\n </script>");
	die();
}

$defaul_folder = "DIY_NOTES";

$sql = "SELECT fileID FROM eclass_file
		WHERE title='$defaul_folder' AND VirPath IS NULL AND IsDir=1 AND Category=$categoryID";

$row = $fm->returnVector($sql);
if (($folderID=$row[0])=="")
{
	$lu = new libuser($ck_user_id, $ck_course_id);

	$fm = new fileManager($ck_course_id, 0, "");
	$fm->user_name = addslashes($lu->user_name());
	$fm->memberType = "T";
	$fm->permission = "AW";
	$folderID = $fm->createFolderDB($defaul_folder, "", "", 0);
}
$Title = ".epc";

$cur_status = getStatus($file_create_file, "");

$title = HTMLtoDB($title);
$description = HTMLtoDB($description);

?>
<html>
<head>
<title></title>
<meta http-equiv='content-type' content='text/html; charset=big5'>
</head>
<form name="form1" action="../../course/resources/files/edit_update.php" method="post"  >
<input type="hidden" name="file_body">
<input type="hidden" name="categoryID" value="<?=$categoryID?>">
<input type="hidden" name="folderID" value="<?=$folderID?>">
<input type="hidden" name="isFromEditor" value="1">
<input type="hidden" name="access_all[]" value="<?=$DefaultFilePermission?>">
<input type="hidden" name="fileID" value="<?=$fileID?>">
<input type="hidden" name="pc_filename" value="<?=$fileName?>">
<input type="hidden" name="fromCC" value="1">
<input type="hidden" name="isFileNew" value="<?=$isFileNew?>">

<input type="hidden" name="filename" value="<?=$fileName?>" >
<input type="hidden" name="autoSubmit" value="1" >
<input type="hidden" name="referPage" value="<?=$referPage?>" >

<input type="hidden" name="title" value="<?=stripslashes($title)?>" >
<input type="hidden" name="description" value="<?=stripslashes($description)?>" >
<input type="hidden" name="m" value="<?=$m?>" >
<input type="hidden" name="chapter" value="<?=$chapter?>" >
<input type="hidden" name="status" value="<?=$status?>" >
<input type="hidden" name="starttime" value="<?=$starttime?>" >
<input type="hidden" name="sh" value="<?=$sh?>" >
<input type="hidden" name="sm" value="<?=$sm?>" >
<input type="hidden" name="endtime" value="<?=$endtime?>" >
<input type="hidden" name="eh" value="<?=$sh?>" >
<input type="hidden" name="em" value="<?=$sm?>" >
<input type="hidden" name="add_to_schedule" value="<?=$add_to_schedule?>" >
<input type="hidden" name="email_alert" value="<?=$email_alert?>" >
<input type="hidden" name="campusmail_alert" value="<?=$campusmail_alert?>" >

</form>
<script language="javascript" >
	document.form1.submit();
</script>
</body>
</html>