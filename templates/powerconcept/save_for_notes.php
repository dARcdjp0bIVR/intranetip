<?php
/* Modify by  */
/**
 * Log:
 * 
 * 2011-05-13 Yuen:
 * 		- addopt the settings for default file permission (default: R)
 * 
 */
 
include_once('../../../system/settings/global.php');
include_once("../../includes/php/lib-db.php");
include_once('../../includes/php/lib-user.php');
include_once('../../includes/php/lib-filemanager.php');
include_once('../../includes/php/lib-courseinfo.php');
include_once('../../../system/settings/lang/'.$ck_default_lang.'.php');

opendb();

# adopting default file permission

$lc = new courseinfo(0);

list($sfunction, $afunction, $gfunction, $setControls) = $lc->getRightAndSettings();
$new_file_right1 = strstr($setControls,'new_file_right:1');
$new_file_right2 = strstr($setControls,'new_file_right:2');
$new_file_right3 = strstr($setControls,'new_file_right:3');
if (!$new_file_right1 && !$new_file_right2 && !$new_file_right3)
{
	$new_file_right2 = true;
}
if ($new_file_right2)
{
	$DefaultFilePermission = "AR";
} elseif ($new_file_right3)
{
	$DefaultFilePermission = "AW";
}
# end of adopting default file permission

$categoryID = empty($categoryID)?0:$categoryID;

$fm = new fileManager($ck_course_id, $categoryID, "");
$vPath = stripslashes($fm->getVirtualPath());

# check if new Concept Map exists, otherwise, no need to save
$pc_file = $file_path."/".$fm->db."/tmp/$ck_user_id/$fileName";
if (!file_exists($pc_file))
{
	die("<script language='JavaScript'>\n self.close();\n </script>");
}

if (!empty($task_folder)){
	include_once('../../includes/php/lib-assessmentFactory.php');
	$factory = &AssessmentFactory::getFactory();
	$data = array();
	$data['task_folder'] = stripslashes($task_folder);
	$task = $factory->load('task',$data);
	$folderID = $task->createDBTaskFolder();
	$defaul_folder = $task_folder;
}
else{
	$defaul_folder = "DIY_NOTES";

	$sql = "SELECT fileID FROM eclass_file
			WHERE title='$defaul_folder' AND VirPath IS NULL AND IsDir=1 AND Category=$categoryID";

	$row = $fm->returnVector($sql);
	if (($folderID=$row[0])=="")
	{
		$lu = new libuser($ck_user_id, $ck_course_id);

		$fm = new fileManager($ck_course_id, $categoryID, "");
		$fm->user_name = addslashes($lu->user_name());
		$fm->memberType = "T";
		$fm->permission = "AW";
		$folderID = $fm->createFolderDB($defaul_folder, "", "", $categoryID);
	}
}
$Title = ".epc";

$cur_status = getStatus($file_create_file, "");

head(4);
?>


<script language="javascript">
function checkform(obj){
	if(!check_text(obj.filename, "<?=$classfiles_alertMsgFile18 ?>")) return false;
	if(obj.filename.value==".epc")
	{
		alert("<?=$classfiles_alertMsgFile18 ?>");
		obj.filename.focus();
		return false;
	}
}
</script>


<form name="form1" action="../../resource/eclass_files/files/edit_update.php" method="post" onSubmit="return checkform(this);">
<?php displayStatusAndMenu($cur_status, "%&%"); ?>
<br><br><br>
<?php
    getBorder2_s('85%', 'center');
?>
<td class="bodycolor2">
<br>
<?php if ($isDenied && $name!="")
{
	echo "&nbsp; '$name'<br>&nbsp; <font color='orange'>$file_msg10</font><br>";
}
?>
<br>

<table width=55% border=0 cellpadding=5 cellspacing=0 align="center">
<tr>
<td width=20% align=right nowrap><span class="title"><?php echo $file_location; ?>:</span>&nbsp;</td>
<td><?=$vPath.$defaul_folder?>/</td>
</tr>

<tr>
<td width=20% align=right valign=top nowrap><span class="title"><?= $file_name ?>:</span>&nbsp;</td>
<td><input type=text name="filename" class="inputfield" size=30 maxlength=255 value="<?=$Title?>" ><?=$guide?></td>
</tr>
</table>
<br><br>

<?php
$buttons =  "&nbsp;&nbsp;&nbsp;<input class=button type=submit value='$button_submit'>&nbsp;&nbsp;\n";
$buttons .= "<input class=button type=reset value='$button_reset'>&nbsp;&nbsp;\n";
$buttons .= "<input class=button type=button value='$button_cancel' onClick=\"self.close()\">&nbsp;&nbsp;\n";
getBorder2_e('center',$buttons);
?>

<script language="JavaScript">
window.resizeTo(500, 350);
window.moveTo(250, 150);
document.form1.filename.focus();
</script>

<input type="hidden" name="file_body">
<input type="hidden" name="categoryID" value="<?=$categoryID?>">
<input type="hidden" name="folderID" value="<?=$folderID?>">
<input type="hidden" name="isFromEditor" value="1">
<input type="hidden" name="access_all[]" value="<?=$DefaultFilePermission?>">
<input type="hidden" name="fileID" value="<?=$fileID?>">
<input type="hidden" name="pc_filename" value="<?=$fileName?>">
<input type="hidden" name="fromCC" value="<?=$fromCC?>">
<input type="hidden" name="isFileNew" value="<?=$isFileNew?>">
<input type="hidden" name="sec_index" value="<?=$sec_index?>">
</form>

</body>
</html>