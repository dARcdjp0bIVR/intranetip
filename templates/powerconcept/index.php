<?php
# using : Thomas
# This page is used to Detect Flash Player (7.0 or higher)

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");

# Initialize Variable
$PowerConceptID = isset($PowerConceptID) && $PowerConceptID!=''? $PowerConceptID:"";

# Build Query String
$query_str = urlencode("?PowerConceptID=$PowerConceptID");
$query_str .= !empty($r_comeFrom)?urlencode("&r_comeFrom=".$r_comeFrom):"";

# Define Error Page
$error_page = "Get_flash_player_$intranet_session_language.htm";

# Show Error Message if user is using iPad
if ($userBrowser->platform=="iPad")
{
	die ($Lang['iPad']['PowerConceptNoFlash']);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="refresh" content="3;url=<?=$error_page?>" />
<title>PowerConcept</title>
</head>
<body>
<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=4,0,0,0" width="80" height="80">
<param name="movie" value="flash_detection.swf?flashContentURL=pc_edit.php<?=$query_str?>&altContentURL=<?=$error_page?>&contentVersion=7&contentMajorRevision=0&contentMinorRevision=0&allowFlashAutoInstall=false" />
<param name="quality" value="low" />
<embed src="flash_detection.swf?flashContentURL=pc_edit.php<?=$query_str?>&altContentURL=<?=$error_page?>&contentVersion=7&contentMajorRevision=0&contentMinorRevision=0&allowFlashAutoInstall=false" quality="low" pluginspage="http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash" type="application/x-shockwave-flash" width="80" height="80" />
</object>
</body>
</html>
