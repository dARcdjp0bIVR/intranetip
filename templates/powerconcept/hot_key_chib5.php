<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=big5">
<title>PowerConcept 快速鍵</title>

<link href="../../includes/css/powerconcept_hot_key.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF">

<center>
<h3><img src="../../../images/logo_powerconcept.gif" width="292" height="40"> 快速鍵</h3>
<table width="580" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td><p><span class="textcontent">為使更方便快捷地製作概念圖，用戶可利用快速鍵以加快完成概念圖的效率，以下是一些快速鍵的使用方法：</span><br>
    </p>
    </td>
  </tr>
  <tr>
    <td><table width="100%" border=0 cellpadding=0 cellspacing=0 class="helptable">
      <tr class="helptableheader">
        <th width=167 valign=top><strong>動作</strong></th>
        <th width="208" valign=top><strong>說明</strong></th>
        <th width=181 valign=top><strong>快速鍵</strong></th>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top ><strong>插入元件</strong></td>
        <td valign=top >在已選擇的元件下插入一個元件</td>
        <td width=181 valign=top >按任何字母或數字<br>
      [a-z, A-Z, 0-9] </td>
      </tr>
      <tr >
        <td width=167 valign=top><strong>刪除元件/線段</strong></td>
        <td  valign=top>刪除已選擇的元件/線段</td>
        <td  width=181 valign=top>按 “Delete” 鍵 (不能刪除標題這個元件)</td>
      </tr>
      <tr class="tablerow" >
        <td width=167 valign=top ><strong>修改元件/線段<br>
          名稱/格式</strong></td>
        <td  valign=top>修改已選擇的元件/線段</td>
        <td  width=181 valign=top>連按元件/線段兩下</td>
      </tr>
      <tr>
        <td width=167 valign=top><strong>移動元件</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr >
        <td  width=167 valign=top><li>一個元件</td>
        <td  valign=top>拖拉一個元件到適當的位置</td>
        <td  width=181 valign=top>滑鼠拖拉</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>元件及它以下的元件</td>
        <td valign=top>拖拉一組元件到適當的位置</td>
        <td width=181 valign=top>按著 “Ctrl”鍵 + 滑鼠拖拉</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>所有元件</td>
        <td valign=top>拖拉所有元件到適當的位置</td>
        <td width=181 valign=top>按著 “Space Bar” + 滑鼠拖拉</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><strong>檢視</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向上檢視</td>
        <td valign=top>把概念圖向上移動</td>
        <td width=181 valign=top>按 “上” 鍵</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向下檢視</td>
        <td valign=top>把概念圖向下移動</td>
        <td width=181 valign=top>按 “下” 鍵</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向左檢視</td>
        <td valign=top>把概念圖向左移動</td>
        <td width=181 valign=top>按 “左” 鍵</td>
      </tr>
      <tr class="tablerow">
        <td width=167 valign=top><li>向右檢視</td>
        <td valign=top>把概念圖向右移動</td>
        <td width=181 valign=top>按 “右” 鍵</td>
      </tr>
      <tr>
        <td width=167 valign=top><strong>變焦</strong></td>
        <td valign=top>&nbsp;</td>
        <td width=181 valign=top>&nbsp;</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>放大</td>
        <td valign=top>放大概念圖</td>
        <td width=181 valign=top>按 “Page Up” 鍵</td>
      </tr>
      <tr>
        <td width=167 valign=top><li>縮小</td>
        <td valign=top>縮小概念圖</td>
        <td width=181 valign=top>按 “Page Down” 鍵</td>
      </tr>
      <tr class="tablerow">
        <td  width=167 valign=top><strong>復原</strong></td>
        <td  valign=top>復原動作包括︰插入、刪除、排列、修改、移動、連線等…</td>
        <td width=181 valign=top>按著 “Ctrl”鍵 + “z” 鍵</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center"><a href="javascript:window.close();">關閉</a></td>
  </tr>
</table>

</center>

</body>
</html>
