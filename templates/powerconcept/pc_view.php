<?php
# using : Paul

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Initialize Variable
$PowerConceptID = isset($PowerConceptID) && $PowerConceptID!=''? $PowerConceptID : '';

# Initialize Object
$libdb = new libdb();

# Die if no PowerConceptID 

if(!$PowerConceptID){
	echo "No PowerConceptID";
	die();
}

# Define the swf file use
switch($intranet_session_language){
	case 'en': $url_swf = "PowerConcept-Viewer.2.0.Secondary.eng.swf"; break;
	case 'b5': $url_swf = "PowerConcept-Viewer.2.0.Secondary.chib5.swf"; break;
}

# Generate Gateway Path
$src_folder = "/tool/powerconcept/";
$src_folder_referer = "/tool/powerconcept/";
$ecGatewayPath = getAMFphpGateWay($src_folder_referer, $src_folder);

# Define UserKey
$ecUserKey = "{$ck_login_session_id}SeP{$ck_user_id}SeP".$row[0]."SeP".$PowerconceptID."SeP".$pc_type."SeP".$fileID."SeP".$isFileNew."SeP".$fileName."SeP".$categoryID;

# Define ItemID
$ecItemID = "{$intranet_db}SeP{$ck_user_id}SeP{$PowerConceptID}SeP{$fileID}SeP{$isFileNew}";

# Define the Flash Parameter
$param_send .= "ecGatewayPath=$ecGatewayPath";
$param_send .= "&ecUserKey=$ecUserKey";
$param_send .= "&ecItemID=$ecItemID";
?>
<html>
	<head>
		<title>eClass</title>
		<meta http-equiv='content-type' content='text/html; charset=UTF-8'>
		<script language="JavaScript" type="text/javascript" src="http://<?=$eclass40_httppath?>src/includes/js/script.js"></script>
		<script language="JavaScript">
			<!--
			var isInternetExplorer = navigator.appName.indexOf("Microsoft") != -1;
			// Handle all the FSCommand messages in a Flash movie.
			function PowerConcept_DoFSCommand(command, args) {
				var PowerConceptObj = isInternetExplorer ? document.all.PowerConcept : document.PowerConcept;
				switch (command) {
					case "VIEW_FILE":
						viewFile(args);
						break;
			
					case "EDIT":
						document.form1.submit();
						break;
			
					case "EXIT":
						top.window.close();
						break;
				}
			}
			// Hook for Internet Explorer.
			if (navigator.appName && navigator.appName.indexOf("Microsoft") != -1 && navigator.userAgent.indexOf("Windows") != -1 && navigator.userAgent.indexOf("Windows 3.1") == -1) {
				document.write('<script language=\"VBScript\"\>\n');
				document.write('On Error Resume Next\n');
				document.write('Sub PowerConcept_FSCommand(ByVal command, ByVal args)\n');
				document.write('	Call PowerConcept_DoFSCommand(command, args)\n');
				document.write('End Sub\n');
				document.write('</script\>\n');
			}
			// open file from eClass
			function viewFile(myFilePath){
				var url = "<?=$admin_url_path?>/src/resource/eclass_files/files/open.php?categoryID=<?=$categoryID?>&fpath=" + myFilePath;
				newWindow(url,9);
			}
			//-->
		</script>
	</head>
	<BODY bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<center>
			<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" id="PowerConcept" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" WIDTH="100%" HEIGHT="100%" align="middle">
				<param name="allowScriptAccess" value="sameDomain" />
				<param name="loop" value="false" />
				<param name="menu" value="false" />
				<param name="salign" value="lt" />
				<param name="movie" value="<?=$url_swf?>" />
				<param name="quality" value="high" />
				<param name="flashvars" value="<?=$param_send?>" />
				<param name="wmode" value="transparent" />
				<param name="scale" value="showall" />
				<embed src="<?=$url_swf?>" quality="high" scale="showall" flashvars="<?=$param_send?>" wmode="transparent" WIDTH="100%" HEIGHT="100%" name="PowerConcept" align="middle" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
			</object>
		</center>
	</BODY>
</html>