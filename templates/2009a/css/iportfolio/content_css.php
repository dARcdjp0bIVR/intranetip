<?php
$PATH_WRT_ROOT = "../../../../";
$image_path = "images/";
$LAYOUT_SKIN = "2009a";
?>

/***********************************************************************************************************************/
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONTENT.CSS END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/***********************************************************************************************************************/

/*#####################################################################################################################*/
/*##################### Common Elements ###############################################################################*/
/*#####################################################################################################################*/
html, body{
	background-color:#faefd2;
	scrollbar-face-color:#F7F7F7;
	scrollbar-base-color:#F7F7F7;
	scrollbar-arrow-color:888888;
	scrollbar-track-color:#E2DFDF;
	scrollbar-shadow-color:#F7F7F7;
	scrollbar-highlight-color:#F7F7F7;
	scrollbar-3dlight-color:#C6C6C6;
	scrollbar-darkshadow-Color:#C6C6C6;

}

table {
 font-size:12px; 
 font-family:Arial, Helvetica, sans-serif
 
}

select {
	border: 1px solid #CCCCCC;
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	padding: 1px 2px;
}


.file {
font-family: "Arial", "Helvetica", "sans-serif";
font-size: 12px;
width: 300px;
}
.systemmsg {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	border: 1px dashed #FF0000;
}
.dotline {
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #999999;
}

.title {
	font-family: Verdana;
	font-size: 18px;
	font-weight: bold;
	color: #000000;
}
.contenttitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #3c3c3c;
	text-decoration: none;
}
.contenttitle:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #3c3c3c;
	text-decoration: none;
}
.contenttitle:hover {
	color: #FF0000;

}

.navigation {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-style: normal;
	color: #000000;
	font-weight: normal;
	text-decoration: none;
}
.navigation a{
	color: #077C9E;
	text-decoration: underline;
}

.navigation a:hover {
	color: #FF0000;
	text-decoration: none;
}

.sectiontitle {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 14px;
	font-weight: bold;
	color: #006600;
	padding-right: 3px;
	padding-left: 3px;
	padding-top: 5px;
	padding-bottom: 5px;


}

.popuptitle {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #00000;
}
/*#####################################################################################################################*/
/*##################### Index #########################################################################################*/
/*#####################################################################################################################*/

.indexscrolltext {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #DD7400;
	font-weight: bold;
	padding-right: 3px;
	padding-left: 3px;
}



.indexwelcome {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #134899;
}
.indextvtitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #757474;
}
.indextvbtn {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #FFFFFF;
	text-decoration: none;
}
.indextvbtn:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #CCFF00;
	text-decoration: none;
}
.indextvplaying {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #FFFFFF;
	padding-bottom: 3px;
}
.indextvlink {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #666666;
	text-decoration: none;
}
.indextvlink:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}


/*================  Quick Button =============================================*/
.indexquicklink {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #003399;
	text-decoration: none;
}

.indexquickbtn {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #196d99;
	text-decoration: none;
}
.indexquickbtn:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}
/*================Tab (eClass, HW, Community =============================================*/
.indextabon {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #0d3aa0;
}
.indextaboff {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #333333;
	text-decoration: none;
}
.indextaboff:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #FF0000;
	text-decoration: none;
}


.indextabwhiterow {
	background-color: #ffffff;
}
.indextabclassiconoff {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #999999;
}


.indextabbluerow {
	background-color: #f0f8fe;
}

.indextabgreenrow {
	background-color: #f5fdf1;
}
.indextaborangerow {
	background-color: #fdf9f1;
}
.indextabclassicon {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #5588b8;
	text-decoration: none;
}

.indextabclassicon:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indextabclasslist {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #006699;
	text-decoration: none;
	font-weight: bold;
}

.indextabclasslist:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexhomelist {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #006699;
	text-decoration: none;
}
.indexhomelist:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
/*============================== POP =============================================*/
.indexpopsubtitlet {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #6699CC;
}
.indexpoplist {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #666666;
	text-decoration: none;
}
.indexpoplist:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}

.indexpopsubtitle {
	background-color: #FFFFFF;
	padding: 4px;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC;
}
.indexpoptitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #135694;
}
.indexpopsubtitlehday {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
}
.indexpopsubtitleschevent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #4f9313;
}
.indexpopsubtitleacevent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #0d98b6;
}

.indexpopsubtitlegroupevent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #ff8401;
}
.indexpop {

	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #000000;
	text-decoration: none;
}
.indexpop a{

	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #006699;
	text-decoration: none;
}
.indexpop a:hover{

	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #FF0000;
	text-decoration: none;
}
/*============================== Calendar ====================================*/
.indexcalendardaylink {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #2152EB;
	text-decoration: none;
}

.indexcalendardaylink:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: underline;
}
.indexcalendarholidaylink {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}
.indexcalendarholidaylink:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #000000;
	text-decoration: underline;
}
.indexcalendartable {
	border-top-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: none;
	border-bottom-style: none;
	border-left-style: solid;
	border-top-color: #bed1e8;
	border-left-color: #bed1e8;
}
.indexcalendartabletd {
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-right-style: solid;
	border-bottom-style: solid;
	border-right-color: #bed1e8;
	border-bottom-color: #bed1e8;
}
.indexcalendarweeks {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #ff0000;
	background-color: #bed1e8;
	height: 15px;
	width: 25px;
}
.indexcalendarweek {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #02186a;
	background-color: #bed1e8;
	height: 15px;
	width: 25px;
}
.indexcalendaracaevent {
	background-color:#D0E5FF;
}
.indexcalendarschevent {
	background-color:#D2F384;
}
.indexcalendarbothevent{
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/index/calendar/ca_botheventbg.gif);
}
.indexcalendarday{
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 11px;
	color: #333333;
}
.indexcalendarhday {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif","�啁敦�𡡞�";
	font-size: 11px;
	color: #FF0000;
}
.indexcalendarremark{
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 10px;
	color: #999999;
}
.indexcalendartoday {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #0082C8;}
.indexcalendarstoday{
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
}
.indexcalendarmonth {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 14px;
	font-weight: normal;
	color: #0079C6;

}
.indexcalendarpaneltext {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #888888;
	text-decoration: none;
	height: 12px;
}
.indexcalendarpaneltext:hover {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 9px;
	color: #FF0000;
	text-decoration: none;
	height: 12px;
}


/*========================Events =============================*/
.indexeventtitleon {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	font-weight: bold;
	color: #0082c8;
}
.indexeventlist {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #0a3fb6;
}
.indexeventlink1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff8401;
	text-decoration: none;
}
.indexeventlink1:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexeventlink2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #4f9313;
	text-decoration: none;
}
.indexeventlink2:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexeventlink3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #0d98b6;
	text-decoration: none;
}
.indexeventlink3:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexeventlink4 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexeventlink4:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexeventlink5 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #ff0000;
	text-decoration: none;
}
.indexeventlink5:hover {
	color: #ff0000;

}
.indexeventtitleoff {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #575757;
	text-decoration: none;
	padding-top: 2px;
	padding-bottom: 1px;
}
.indexeventtitleoff:hover {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #FF0000;
	text-decoration: none;
}
/*======================== What's News ===================================*/
.indexnewstitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #379504;

}
.indexnewslisttitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #429416;
}
.indexnewslist {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #4ead1a;
	text-decoration: none;
}
.indexnewslist:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #ff0000;
	text-decoration: none;
}
.indexnewslistgroup {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #126CB6;
	text-decoration: none;
}
.indexnewslistgrouptitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #126CB6;
}
.indexnewslistgroup:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: normal;
	color: #ff0000;
	text-decoration: none;
}
.indexnewsdesc {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #020202;
}



/*#####################################################################################################################*/
/*##################### Table Comment Elements ########################################################################*/
/*#####################################################################################################################*/
.contenttool {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #9966CC;
	text-decoration: none;
}
.contenttool:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.tabletool {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #2F7510;
	text-decoration: none;
}
.tabletool:hover  {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.tabletoplink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF;
	text-decoration: none;
}
.tabletoplink:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #BF0808;
	text-decoration: none;
}
.tabletopnolink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #FFFFFF;
}
.tablesubtitle {
	padding-top: 15px;
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 13px;
	font-weight: bold;
	color: #003399;
	border-bottom-width: 1px;
	border-bottom-style: dotted;
	border-bottom-color: #666666;
	font-style: italic;

}

.tablesubtitle2 {
	padding-top: 10px;
	border-bottom-width: 1px;
	border-bottom-style: double;
	border-bottom-color: #666666;
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #666666;
	background-color: #FFFFFF;
}

.tabletextremark {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #999999;
}

.tabletext {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #000000;
}
.tablename {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 13px;
	font-weight: bold;
	color: #333333;
	padding-right: 3px;
	padding-left: 3px;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #999999;
	border-left-color: #CCCCCC;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #CCCCCC;
	padding-top: 5px;
	padding-bottom: 5px;


}
.tablebottomlink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #006600;
	text-decoration: none;
}
.tablebottomlink:hover  {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.tabletextrequire {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
}
.tabletextrequire2 {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #008000;
}
.tablerow_underline{
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-style: solid;
	border-bottom-color: #CDCDCD;

}

/*#####################################################################################################################*/
/*########################### Table Grey ##############################################################################*/
/*#####################################################################################################################*/
.tabletop {
	background-color: #A6A6A6;
}
.tablerow1 {
	background-color: #FFFFFF;
}
.tablerow2 {

	background-color: #F5F5F5;
}
.tablebottom {

	background-color: #D2D2D2;
}
.tablelink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #2286C5;
	text-decoration: none;
}
.tablelink:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.tablelist{
	background-color: #D2D2D2;
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #FFFFFF;
}

/*#####################################################################################################################*/
/*########################### Table Blue ##############################################################################*/
/*#####################################################################################################################*/
.tablebluetop {
	background-color: #1B91AF;
}

.tablebluerow1 {
	background-color: #FFFFFF;
}
.tablebluerow2 {

background-color: #EBF6FA;
}
.tablebluebottom {

	background-color: #BAD1D8;
}

.tablebluelink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #8447BD;
	text-decoration: none;
}
.tablebluelink:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.tablebluelist{
	background-color: #BAD1D8;
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #FFFFFF;
}

/*#####################################################################################################################*/
/*########################### Table Orange ############################################################################*/
/*#####################################################################################################################*/
.tableorangetop {
	background-color: #BB8444;
}
.tableorangerow1 {
	background-color: #FFFFFF;
}
.tableorangerow2 {

	background-color: #FFFAEF;
}
.tableorangebottom {

	background-color: #E2D0BA;
}
.tableorangelink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #A15A06;
	text-decoration: none;
}
.tableorangelink:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
/*#####################################################################################################################*/
/*########################## Table Green ##############################################################################*/
/*#####################################################################################################################*/
.tablegreentop {
	background-color: #71AA2F;
}
.tablegreenrow1 {
	background-color: #FFFFFF;
}

.tablegreenrow2 {

	background-color: #F7FEEF;
}
.tablegreenrow3 {

	background-color: #bfedba;
		border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}
.tablegreenbottom {

	background-color: #CAE1BF;
}
.tablegreenlink {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #038A9E;
	text-decoration: none;
}
.tablegreenlink:hover {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
/*#####################################################################################################################*/
/*############################ Form Elements###########################################################################*/
/*#####################################################################################################################*/
.textboxhighlight {
		background-color: #FFCD71;
		color: #000000;
}
.textboxtext {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #000000;
	width: 99%;
}
.textboxtext2 {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #000000;
	width: 95%;
}
.textboxnum {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #000000;
	width: 100px;
}
.formfieldtitle {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #EEEEEE;
}
.formbutton{
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 13px;
	cursor:pointer;
	border:1px outset #82A4BC;
	color:#384C95;
	font-weight:bold;
	padding: 2px 3px;
	background-color:#BBDBF2;
    Filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr='#ffFFFFFF',EndColorStr='#BBDBF2');

}
.formbuttonon{
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 13px;
	cursor:pointer;
	border:1px outset #82A4BC;
	color:#0084CF;
	font-weight:bold;
	padding: 2px 3px;
	background-color:#D4E5F1;
    Filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr='#ffFFFFFF',EndColorStr='#D4E5F1');
}

.formtextbox{
	border: 1px solid #CCCCCC;
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	padding: 1px 2px;
}
.formsubbutton{
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	cursor:pointer;
	border:1px outset #CCCCCC;
	color:#666666;
	font-weight:bold;
	padding: 1px 2px;
	background-color:#D7D7D7;
    Filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr='#ffFFFFFF',EndColorStr='#D7D7D7');

}
.formsubbuttonon{
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	cursor:pointer;
	border:1px outset #CCCCCC;
	color:#9E9FA0;
	font-weight:bold;
	padding: 1px 2px;
	background-color:#EFEFEF;
    Filter:progid:DXImageTransform.Microsoft.Gradient(GradientType=0,StartColorStr='#ffFFFFFF',EndColorStr='#EFEFEF');

}

/*#####################################################################################################################*/
/*############################ Step ###################################################################################*/
/*#####################################################################################################################*/
.steptable {
	border: 1px dashed #32C617;
}
.steptitlebg {
	background-color: #EDFFE0;
	padding: 5px;

}

.steptitletext {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #188107;
}
.stepoff {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FFFFFF;
}
.stepon {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 14px;
	font-weight: bold;
	color: #FFFFFF;
}
.stepofftext {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #C4C4C4;
}
.stepofftext a{
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #C4C4C4;
	text-decoration: none;
}
.stepofftext a:hover{

	color: #FF0000;
	text-decoration: none;
}
/*#####################################################################################################################*/
/*############################ Calendar (Date Selection)###############################################################*/
/*#####################################################################################################################*/
.calendartitle {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 10px;
	color: #0079C6;
}
.calendarholiday {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 9px;
	color: #FF0000;
	text-decoration: none;
}
.calendarholiday:hover {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 9px;
	color: #FF0000;
	text-decoration: underline;
}
.calendarday {
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 9px;
	color: #6A6A6A;
	text-decoration: none;
}
.calendarday:hover{
	font-family: "Verdana", "Arial", "Helvetica", "sans-serif";
	font-size: 9px;
	color: #FF0000;
	text-decoration: underline;
}
.calendartop {
	background-color: #D7D7D7;
}
.calendarselect {
	background-color: #FFE87A;
}
.calendartoday {
	background-color: #D1F9B0;
}
.calendarlayer{
visibility: hidden;
position: absolute;
top: 1px;
left: 1px;
}
/*#####################################################################################################################*/
/*############################ Pre-set List############################################################################*/
/*#####################################################################################################################*/
.presetlist {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #4288E8;
	text-decoration: none;
}

.presetlist:hover{
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}

/*#####################################################################################################################*/
/*############################ eSports ################################################################################*/
/*#####################################################################################################################*/
.eSporttableevent {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #183998;
}
.eSporthouse {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #000000;
	/*padding: 2px;
	background-color:#FFFFFF;
	height:20; 
	filter:glow(color=#FFFFFF, strength=4);*/

}
.eSporthousetd {
	font-weight: bold;
	padding: 2px;
}

/*#####################################################################################################################*/
/*############################ iMail ##################################################################################*/
/*#####################################################################################################################*/

.imailpagetitleunread:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}
.imailusage {
	background-color: #f6f6f6;
	border: 1px solid #aeaeae;
	width: 150px;
}
.imailusagetext {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #333333;
	font-weight: none;
}
.iMailrow {
	background-color: #f5f5f5;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}
.iMailrowunread {
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}

.iMailsender {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
}
.iMailsenderunread {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
	font-weight: bold;
}
.iMailsubject {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #2253ae;
	text-decoration: none;
}
.iMailsubject:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
	text-decoration: none;
}
.iMailsubjectunread {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #2253ae;
	font-weight: bold;
	text-decoration: none;
}
.iMailsubjectunread:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FF0000;
	font-weight: bold;
	text-decoration: none;
}
.iMailrecipientgroup {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #2B51A0;
}
.imailpagetitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 15px;
	font-weight: bold;
	color: #3c3c3c;
}
.imailpagetitleunread {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #2253ae;
	text-decoration: none;
}
.iMailReadtable {
	border: 1px solid #EEEEEE;
}
.iMailComposefieldtitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #000000;
}
.iMailReadnotification {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	color: #2D8800;
}
/*#####################################################################################################################*/
/*############################ SmartCard ##############################################################################*/
/*#####################################################################################################################*/
.tableline {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d2d2d2;
}

.attendancein {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d2d2d2;
	background-color: #fffbe3;
}
.attendanceintop {
	background-color: #fdf298;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #ffffff;
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #ffffff;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #666666;
}

.attendanceout {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d2d2d2;
	background-color: #f0fde0;
}
.attendanceouttop {
	background-color: #cee7aa;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #ffffff;
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #ffffff;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #666666;
}

.attendancelate {
	background-color: #EBF6FA;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}
.attendancestudentphoto {
	background-color: #FFFFFF;
	border: 1px solid #666666;
}
.paymenttitle {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #660000;
}


.attendancepresent {
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}
.attendanceabsent {
	background-color: #ffe0e1;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}
.attendanceouting {
	background-color: #F7FEEF;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #b8b8b8;
}
/*#####################################################################################################################*/
/*############################ eNotice ##############################################################################*/
/*#####################################################################################################################*/

.eNoticereplytitle {
	/*background-color: #F4FEE9;*/
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #336600;
}

/*#####################################################################################################################*/
/*############################ eClass ##############################################################################*/
/*#####################################################################################################################*/

.iconlist {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #969595;
}
.eclassiconlink {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #2469b1;
	text-decoration: none;
}
.eclassiconlink:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #FF0000;
	text-decoration: none;
}
.eclassicon {

	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	color: #2469b1;
	text-decoration: none;
}

/*#####################################################################################################################*/
/*############################ iCalendar ##############################################################################*/
/*#####################################################################################################################*/
.icalendar_title {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #033e96;
}
.icalendartabon {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
	color: #033e96;
	text-decoration: none;
}
.icalendar_weektitle {
	background-color: #d2d2d2;
	padding: 2px;
	text-align: center;
	height: 23px;
}
.icalendar_weektitle_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #3b3c3c;
	padding: 0px;
	text-align: center;
	width: 14%;

}

.icalendar_weektitle_today {
	background-color: #a4a4a4;
	padding: 2px;
	text-align: center;
	height: 23px;

}
.icalendar_weektitle_text_today {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
	padding: 0px;
	text-align: center;
	width: 14%;
	font-weight: bold;

}
.icalendar_sepline {
	padding: 2px;
	height: 1px;
}
.icalendar_day_border {
	border: 1px solid #CCCCCC;
	float: none;
	position: relative;
	z-index: auto;
}
.icalendar_day_border_over {
	border: 1px solid #000000;
	background-color: #d1f2f2;
}
.icalendar_today_border {
	border: 1px solid #5e5f5e;
	background-color: #f8f4a5;
}
.icalendartabon:hover {
	color: #FF0000;

}
.icalendartaboff {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: normal;
	color: #777878;
	text-decoration: none;
}
.icalendartaboff:hover {
	color: #FF0000;

}
.icalendartabon:hover {
	color: #FF0000;

}
#iCalendar_logo {
	position:absolute;
	z-index:2;
	left: 0;
	top: 0;
	width: 310;
}
.icalendar_eventlist_title {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #FFFFFF;
}
.icalendar_event_holiday{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #d31b1a;
	text-decoration: none;
}
.icalendar_event_holiday:hover{
	color: #FF0000;
}
.icalendar_event_school{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #5a8b35;
	text-decoration: none;
}
.icalendar_event_school:hover{
	color: #FF0000;
}
.icalendar_event_personal{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #6c2a8b;
	text-decoration: none;
	}
.icalendar_event_personal:hover{
	color: #FF0000;
}
.icalendar_event_group{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #b1721f;
	text-decoration: none;
}
.icalendar_event_group:hover{
	color: #FF0000;
}
.icalendar_event_academic{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #6583bc;
	text-decoration: none;
}
.icalendar_event_academic:hover{
	color: #FF0000;
}
#normal_day_number
{
	width: 100%;
	clear: both;
	float: left;

}
#normal_day_number span{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #313232;
	background-color: #f0f0f0;
	padding: 2px;
	width: 20px;
	text-align: center;
}
#today_number
{
	width: 100%;
	clear: both;
	float: left;

}
#today_number span{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
	background-color: #f5c01a;
	padding: 2px;
	width: 20px;
	text-align: center;
	font-weight: bold;
}

#holiday_day_number_noevent
{
	width: 100%;
	clear: both;
	float: left;

}
#holiday_day_number_noevent span.numbertext{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #b41d23;
	background-color: #f9d6d6;
	padding: 2px;
	width: 20px;
	text-align: center;
}
#holiday_day_number2
{
	width: 100%;
	clear: both;
	float: left;
	background-color: #f9d6d6;

}
#holiday_day_number2 span.numbertext{
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #b41d23;
    width: 20px;
	padding: 2px;
	text-align: center;
}
#event_holiday{
	width: 100%;
	clear: both;
	float: left;

/*	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/icalendar/icon_event_holiday.gif);
	background-repeat: no-repeat;
	background-position: left 20px;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	*/	
}


#event_holiday a {
	font-family: Arial, Helvetica, sans-serif;
	float:left;
	font-size: 12px;
	margin:0;
	display:block;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	
	text-decoration:none;
	color: #b41d23;
	background-color: #f9d6d6;
	width: 100%;
	/*border: 1px solid #f9d6d6;*/
}

#event_holiday a:hover {
	color: #000000;
	background-color: #ea9c9c;
	/*border: 1px solid #000000;*/

}
/*#event_holiday a span{
	padding-left: 15px;

	display: block;
}*/


#event_personal{
	width: 100%;
	clear: both;
	float: left;

	/*background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/icalendar/icon_event_personal.gif);
	background-repeat: no-repeat;
	background-position: left 5px;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;*/		

}
#event_personal a {

	font-family: Arial, Helvetica, sans-serif;
	float:left;
	font-size: 12px;
	margin:0px;
	display:block;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	
	text-decoration:none;
	color: #6c2a8b;
	background-color: #f1e2ee;
	width: 100%;
	/*border: 1px solid #f1e2ee;*/
}/*
#event_personal a span{
	padding-left: 15px;
	display: block;

}*/

#event_personal a:hover {
	color: #000000;
	background-color: #d0bae2;
	/*border: 1px solid #000000;*/

}


#event_school{
	width: 100%;
	clear: both;
	float: left;

	/*background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/icalendar/icon_event_school.gif);
	background-repeat: no-repeat;
	background-position: left 5px;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	*/	
}


#event_school a {
	font-family: Arial, Helvetica, sans-serif;
	float:left;
	font-size: 12px;
	margin:0;
	display:block;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	
	text-decoration:none;
	color: #598a35;
	background-color: #deecc3;
	width: 100%;
	/*border: 1px solid #deecc3;*/
}
/*#event_school a span{
	padding-left: 15px;

	display: block;
}*/
#event_school a:hover {
	color: #000000;
	background-color: #bfe673;
	/*border: 1px solid #000000;*/

}




#event_group{
	width: 100%;
	clear: both;
	float: left;
	/*padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;*/	
}


#event_group a {
	font-family: Arial, Helvetica, sans-serif;
	float:left;
	font-size: 12px;
	margin:0;
	display:block;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	
	text-decoration:none;
	color: #b1721f;
	background-color: #fcf3e0;
	width: 100%;
	/*border: 1px solid #fcf3e0;*/
}
/*#event_group a span{
	padding-left: 15px;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/icalendar/icon_event_group.gif);
	background-repeat: no-repeat;
	background-position: left top;
	display: block;
}*/
#event_group a:hover {
	color: #000000;
	background-color: #ffdd94;
	/*border: 1px solid #000000;*/
}

#event_academic{

	width: 100%;
	clear: both;
	float: left;
	/*background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/icalendar/icon_event_academic.gif);
	background-repeat: no-repeat;
	background-position: left 5px;
		padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;	*/
}

#event_academic a {
	font-family: Arial, Helvetica, sans-serif;
	float:left;
	font-size: 12px;
	margin:0;
	display:block;
	text-decoration:none;
	color: #3a5d9d;
	width: 100%;
	background-color: #dff1fa;
	padding-top: 2px;
	padding-right: 0px;
	padding-bottom: 2px;
	padding-left: 0px;		
	/*border: 1px solid #dff1fa;*/
}
/*#event_academic a span{
	padding-left: 15px;

	display: block;
}

#event_academic a span {float:none;}
/* End IE5-Mac hack */
#event_academic a:hover {
	color: #000000;
 	background-color: #80d5ff;
	/*border: 1px solid #000000;*/


}
#add_event_box {
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-right-style: solid;
	border-bottom-style: solid;
	border-top-color: #8a8a8a;
	border-right-color: #8a8a8a;
	border-bottom-color: #8a8a8a;
	border-left-color: #8a8a8a;
	position:absolute;


	z-index: auto;

}
#add_new_event_box
 {
	border: 1px solid #e5e5e5;
	background-color: #FFFFFF;
	width:300px;
}
#add_event_box_new_title
 {
	background-color: #e5e5e5;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #727272;
	width: 98%;
	padding:2px;
	clear:both;
	display: block;
}
#add_event_box_personal {
	border: 1px solid #d2b1d3;
	background-color: #FFFFFF;
	width:200px;
}
#add_event_box_personal_title {
	background-color: #f3e4f0;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #6c2a8b;
	width: 98%;
	padding:2px;
	clear:both;
	display: block;
}
#add_event_box_group {
	border: 1px solid #b1721f;
	background-color: #FFFFFF;
	width:200px;
}
#add_event_box_group_title {
	background-color: #fcf3e0;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #b1721f;
	width: 98%;
	padding:2px;
	clear:both;
}
#add_event_box_school {
	border: 1px solid #598a35;
	background-color: #FFFFFF;
	width:200px;
}
#add_event_box_school_title {
	background-color: #deecc3;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #598a35;
	width: 98%;
	padding:2px;
}
#add_event_box_holiday {
	border: 1px solid #b41d23;
	background-color: #FFFFFF;
	width:200px;
}
#add_event_box_holiday_title {
	background-color: #f9d6d6;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #b41d23;
	width: 98%;
	padding:2px;
}
#add_event_box_academic {
	border: 1px solid #3a5d9d;
	background-color: #FFFFFF;
	width:200px;
}
#add_event_box_academic_title {
	background-color: #dff1fa;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-weight: bold;
	color: #3a5d9d;
	width: 98%;
	padding:2px;
}
#add_event_box_content {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #000000;
	line-height: 1.5;
	clear:both;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 5px;
}
#box_title {

	float: left;
}
#box_btn {

	float: right;
}

.icalendar_time_text {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #818181;
}
.icalendar_time_row1 {
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #FFFFFF;
	padding: 2px;
	background-color: #e6e4e4;

}
.icalendar_time_row2 {
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #FFFFFF;
	border-bottom-color: #FFFFFF;
	padding: 2px;
	background-color: #e6e4e4;

}
.icalendar_week_today
{
	background-color: #fffca5;
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #bbbbbb;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #828282;
	border-left-color: #828282;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #fffca5;
}
.icalendar_week_today_first
{
	background-color: #fffca5;
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #bbbbbb;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #828282;
	border-left-color: #828282;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #828282;
}
.icalendar_week_today_last
{
	background-color: #fffca5;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #828282;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #828282;
	border-left-color: #828282;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #fffca5;
}
.icalendar_week
{
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #bbbbbb;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #d4d4d4;
	border-left-color: #d4d4d4;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #FFFFFF;
}
.icalendar_week_first
{
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #bbbbbb;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #d4d4d4;
	border-left-color: #d4d4d4;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #d4d4d4;
}
.icalendar_week_last
{
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d4d4d4;
	border-right-width: 1px;
	border-left-width: 1px;
	border-right-style: solid;
	border-left-style: solid;
	border-right-color: #d4d4d4;
	border-left-color: #d4d4d4;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #FFFFFF;
}

.icalendar_week_over
{
	background-color: #eaefff;
	border: 1px solid #0066c5;
}

/*#####################################################################################################################*/
/*############################ Warning ###################################################################################*/
/*#####################################################################################################################*/
.Warningtable {
	border: 1px dashed #ffb9b9;
}
.Warningtitlebg {
	background-color: #ffe0e0;
	padding: 5px;

}

.Warningtitletext {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #c40303;
}

/*#####################################################################################################################*/
/*############################ Warning ###################################################################################*/
/*#####################################################################################################################*/
.Warningtable {
	border: 1px dashed #ffb9b9;
}
.Warningtitlebg {
	background-color: #ffe0e0;
	padding: 5px;

}

.Warningtitletext {
	font-family: "Arial", "Helvetica", "sans-serif";
	font-size: 12px;
	font-weight: bold;
	color: #c40303;
}

/*#####################################################################################################################*/
/*############################ sub tabs ###################################################################################*/
/*#####################################################################################################################*/

.tab_table{
padding-top: 5px;
padding-bottom: 5px;

}

.shadetabs{
	border-bottom: 1px solid #b1bfdb;
	/* width: 90%; width of menu. Uncomment to change to a specific width */
	margin-bottom: 5pt;
}

.shadetabs ul{
padding: 3px 0;
margin-left: 0;
margin-top: 1px;
margin-bottom: 0;
font:  12px Arial;
list-style-type: none;
text-align: left; /*set to left, center, or right to align the menu as desired*/
}

.shadetabs li{
display: inline;
margin: 0;
}

.shadetabs li a{
	text-decoration: none;
	padding: 3px 7px;
	margin-right: 3px;
	color: #a5a5a5;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 0px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #d9d9d9;
	border-right-color: #d9d9d9;
	border-bottom-color: #d9d9d9;
	border-left-color: #d9d9d9;/*background: white url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/shade.gif) top left repeat-x;*/
}


.shadetabs li a:hover{
text-decoration: none;
color: #FF0000;
}

.shadetabs li.selected{
position: relative;
color: #2b64b2;
top: 1px;
}

.shadetabs li.selected a{ /*selected main tab style */
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/shade.gif);
	color: #2b64b2;
	font-weight: bold ;
	border-bottom-color: white;
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 1px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #b1bfdb;
	border-right-color: #b1bfdb;
	border-left-color: #b1bfdb;
}

.shadetabs li.selected a:hover{ /*selected main tab style */
text-decoration: none;
}/***********************************************************************************************************************/
/*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CONTENT.CSS END <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/
/***********************************************************************************************************************/
