// Timetable index & view page common functions

function MM_showHideLayers() { //v9.0
	var i,p,v,obj,args=MM_showHideLayers.arguments;
	for (i=0; i<(args.length-2); i+=3)
	{
		with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { 
			v=args[i+2];
			if (obj.style) { 
				obj=obj.style;
				v=(v=='show')? 'visible' : (v=='hide')? 'hidden' : v; 
			}
			obj.visibility = v;
		}
	}
}

function js_Show_Hide_Filter_Div(ActionType)
{
	if (ActionType == null)
	{
		var layer_visibility = document.getElementById('FilterSelectionDiv').style.visibility;
		if (layer_visibility=='' || layer_visibility=='hidden')
		{
			MM_showHideLayers('FilterSelectionDiv','','show');
			MM_showHideLayers('DisplayOptionDiv','','hide');
		}
		else
		{
			MM_showHideLayers('FilterSelectionDiv','','hide');
		}
	}
	else if (ActionType == 'hide')
	{
		MM_showHideLayers('FilterSelectionDiv','','hide');
	}
	else if (ActionType == 'show')
	{
		MM_showHideLayers('FilterSelectionDiv','','show');
	}
}

function js_Show_Hide_Display_Option_Div()
{
	var layer_visibility = document.getElementById('DisplayOptionDiv').style.visibility;
	if (layer_visibility=='' || layer_visibility=='hidden')
	{
		MM_showHideLayers('DisplayOptionDiv','','show');
		MM_showHideLayers('FilterSelectionDiv','','hide');
	}
	else
	{
		MM_showHideLayers('DisplayOptionDiv','','hide');
	}
}

function js_Update_Timetable_Display(jsForPrint)
{
	jsForPrint = jsForPrint || '';
	
	// Show Hide Subject Teacher Name
	var jsChkObj = (jsForPrint == 1)? $('input#ShowSubjectTeacherNameChk', opener.document) : $('input#ShowSubjectTeacherNameChk');
	if (jsChkObj.attr('checked'))
		$('div.StaffNameDisplayDiv').show();
	else
		$('div.StaffNameDisplayDiv').hide();
	
	// Show Hide Subject Teacher UserLogin
	var jsChkObj = (jsForPrint == 1)? $('input#ShowSubjectTeacherUserLoginChk', opener.document) : $('input#ShowSubjectTeacherUserLoginChk');
	if (jsChkObj.attr('checked'))
		$('div.StaffUserLoginDisplayDiv').show();
	else
		$('div.StaffUserLoginDisplayDiv').hide();
		
	// Show Hide Subject Title Info
	var jsChkObj = (jsForPrint == 1)? $('input#ShowSubjectTitleChk', opener.document) : $('input#ShowSubjectTitleChk');
	if (jsChkObj.attr('checked'))
		$('span.SubjectNameDisplaySpan').show();
	else
		$('span.SubjectNameDisplaySpan').hide();
		
	// Show Hide Number of Student Info
	var jsChkObj = (jsForPrint == 1)? $('input#ShowNumOfStudentChk', opener.document) : $('input#ShowNumOfStudentChk');
	if (jsChkObj.attr('checked'))
		$('span.SubjectGroupNumOfStudentDisplaySpan').show();
	else
		$('span.SubjectGroupNumOfStudentDisplaySpan').hide();
		
	// Show Hide Building Info
	var jsChkObj = (jsForPrint == 1)? $('input#ShowBuildingChk', opener.document) : $('input#ShowBuildingChk');
	if (jsChkObj.attr('checked'))
		$('span.BuildingNameDisplaySpan').show();
	else
		$('span.BuildingNameDisplaySpan').hide();
		
	// Show Hide Floor Info
	var jsChkObj = (jsForPrint == 1)? $('input#ShowFloorChk', opener.document) : $('input#ShowFloorChk');
	if (jsChkObj.attr('checked'))
		$('span.FloorNameDisplaySpan').show();
	else
		$('span.FloorNameDisplaySpan').hide();
		
	// Show Hide Room Info
	var jsChkObj = (jsForPrint == 1)? $('input#ShowRoomChk', opener.document) : $('input#ShowRoomChk');
	if (jsChkObj.attr('checked'))
		$('span.RoomNameDisplaySpan').show();
	else
		$('span.RoomNameDisplaySpan').hide();
}


function js_Update_Select_All_Display_Option(chkAllID)
{
	var checkedValue = $('input#' + chkAllID).attr('checked');
	$('input.DisplayOptionChkBox').attr('checked', checkedValue);
}


// return '' if the value is undefined
function js_Check_Object_Defined(obj)
{
	if (obj == null)
		return '';
	else
		return obj;
}

function js_Select_All_None_Subject_Group()
{
	var isChecked = $('input#DisplayOption_All').attr('checked');
	//$('.DisplayOptionChkBox').each( function () { $(this).attr('checked', isChecked); });
	
	$('input.DisplayOptionChkBox').attr('checked', isChecked);
}

function js_Unselect_All_Subject_Group(thisObjID)
{
	var isChecked = $('input#' + thisObjID).attr('checked');
	
	if (!isChecked)
		$('input#DisplayOption_All').attr('checked', false);
}
