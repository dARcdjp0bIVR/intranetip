var LessonAttendFlashVars;

function Refresh_Lesson_Attendance_Flash() {
	// if is pop up, it means it start from admin page, and will close the pop up window after save complete
	if (window.opener) {
		window.opener.Scroll_To_Top();
		window.opener.Get_Return_Message(LessonAttendReturnMessage);
		window.opener.View_Lesson_Daily_Overview();
		window.close();
	}
	else {
		Scroll_To_Top();
		Get_Return_Message(LessonAttendReturnMessage);
		
		Block_Element("LoadingLayer");
		document.getElementById('LoadingLayer').innerHTML = '<div id="LessonAttendanceFlashLayer" name="LessonAttendanceFlashLayer" width="100%" style="height=800px;"></div>';
		embedFlash("LessonAttendanceFlashLayer", "/home/smartcard/attendance/lesson_attendance/main1.swf", LessonAttendFlashVars, "#869ca7", "100%", "900px", "9.0.0");
		UnBlock_Element("LoadingLayer");
	}
}