//expand collapse
function initBtnTableExpand(){
	$(".btn-table-expand").click(function() {
	//$(document).on("click",".btn-table-expand", function () {
	    var this_class = $(this).attr('class');
	    if(this_class != '') {
	        var classArr = this_class.replace('btn-table-expand ', '').replace(' collapsed', '').split(' ');
	
	        if(classArr[0] == 'master') {
	            $('tr.level-1').toggleClass("show");
	
	            var is_show = $('tr.level-1').hasClass("show");
	            $('tr.level-2').each(function(){
	                if($(this).hasClass("show") == is_show) {
	                    // do nothing
	                } else {
	                    $(this).toggleClass("show");
	                }
	            });
	        } else {
	            if(classArr.length == 1) {
	                var selectorStr = "tr.level-1." + classArr[0];
	                $(selectorStr).toggleClass("show");
	
	                var is_show = $(selectorStr).hasClass("show");
	                $('tr.level-2.' + classArr[0]).each(function(){
	                    if($(this).hasClass("show") == is_show) {
	                        // do nothing
	                    } else {
	                        $(this).toggleClass("show");
	                    }
	                });
	            } else {
	                var selectorStr = "tr." + classArr[0] + "." + classArr[1];
	                $(selectorStr).toggleClass("show");
	            }
	        }
	    }
	
	    $(this).toggleClass("collapsed");
	});
}