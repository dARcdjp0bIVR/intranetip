/**
* Filename.......: list.js
* Project........: Popup List
* Last Modified..: $Date: 2008/04/14 18:17:05 $
* CVS Revision...: $Revision: 1.2 $
* Copyright......: 2001, 2002 Richard Heyes
*/

/**
* Global variables
*/
	dynList_layers          = new Array();
	dynList_mouseoverStatus = false;
	dynList_mouseX          = 0;
	dynList_mouseY          = 0;

/**
* The list constructor
*
* @access public
* @param string objName      Name of the object that you create
* @param string callbackFunc Name of the callback function
* @param string OPTIONAL     Optional layer name
* @param string OPTIONAL     Optional images path
*/
	function dynList(objName, callbackFunc)
	{
		/**
        * Properties
        */
		this.objName        = objName;
		
		this.callbackFunc   = callbackFunc;
		this.imagesPath     = arguments[2] ? arguments[2] : 'images/';
		this.layerID        = arguments[3] ? arguments[3] : 'dynList_layer_' + dynList_layers.length;

		this.offsetX        = 0;
		this.offsetY        = 0;
		this.listArray      = "a";
		this.targetObj      = null;

		// [eCorp]
		this.pos_top	  = null;
		this.pos_left	  = null;

		/**
        * Public Methods
        */
		this.show              = dynList_show;
		this.writeHTML         = dynList_writeHTML;
		this.trace 			   = dynList_trace;

		// Accessor methods
		this.setOffset         = dynList_setOffset;
		this.setOffsetX        = dynList_setOffsetX;
		this.setOffsetY        = dynList_setOffsetY;
		this.setImagesPath     = dynList_setImagesPath;
		this.setListArray      = dynList_setListArray;
		this.setTargetObj      = dynList_setTargetObj;
		
		// [eCorp]
		this.setPosition	     = dynList_setPosition;

		/**
        * Private methods
        */
		// Layer manipulation
		this._getLayer         = dynList_getLayer;
		this._hideLayer        = dynList_hideLayer;
		this._showLayer        = dynList_showLayer;
		this._setLayerPosition = dynList_setLayerPosition;
		this._setHTML          = dynList_setHTML;

		// Miscellaneous
		this._mouseover        = dynList_mouseover;

		/**
        * Constructor type code
        */
		dynList_layers[dynList_layers.length] = this;
		this.writeHTML();
	}

/**
* Shows the list, or updates the layer if
* already visible.
*
* @access public
*/
	function dynList_show()
	{
		// Variable declarations to prevent globalisation
		var htmlCal;
		var arrObj = eval(this.listArray);

		// Main body of list
		htmlCal = '<table width="130" border="0" cellpadding="0" cellspacing="0">';
		htmlCal += '<tr>';
		htmlCal += '<td height="19">';
		
		htmlCal += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmlCal += '<tr>';
		htmlCal += '<td width="5" height="19"><img src="' + this.imagesPath + 'can_board_01.gif" width="5" height="19"></td>';
		htmlCal += '<td height="19" background="' + this.imagesPath + 'can_board_02.gif">';
		
		htmlCal += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmlCal += '<tr>';
		htmlCal += '<td width="12"></td>';
		htmlCal += '<td align="center" nowrap="nowrap"><span class="calendartitle"></td>';
		htmlCal += '<td width="12"></td>';
		htmlCal += '</tr>';
		htmlCal += '</table>';
		
		htmlCal += '</td>';
		htmlCal += '<td width="19" height="19"><a href="javascript:' + this.objName + '._hideLayer()"><img src="' + this.imagesPath + 'can_board_close_off.gif" name="can_close" width="19" height="19" border="0" id="can_close" onMouseOver="MM_swapImage(\'can_close\',\'\',\'' + this.imagesPath + 'can_board_close_on.gif\',1)" onMouseOut="MM_swapImgRestore()" /></a></td>';
		htmlCal += '</tr>';
		htmlCal += '</table>';
		htmlCal += '</td>';
		htmlCal += '</tr>';

		htmlCal += '<tr>';
		htmlCal += '<td>';
		
		htmlCal += '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
		htmlCal += '<tr>';
		htmlCal += '<td width="5" background="' + this.imagesPath + 'can_board_04.gif"><img src="' + this.imagesPath + 'can_board_04.gif" width="5" height="19"></td>';
		htmlCal += '<td bgcolor="#FFFFF7">';
		
	    htmlCal += '<div style="overflow: auto; height: 100px; width: 130px">';
		htmlCal += '<table width="100%" border="0" cellspacing="0" cellpadding="1">';
		
		for (var i = 0; i < arrObj.length; i++)
		{
			for (var j = 0; j < arrObj[i].length; j++)
			{
				fieldValue = document.getElementById(this.targetObj).value.replace(/&/g, "&amp;").replace(/'/g, "&#96;").replace(/`/g, "&#96;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

				htmlCal += "<tr><td class=\"presetlist\">";
				//htmlCal += arrObj[i][j];
				//jOtherFunctionTxt = (jOtherFunction=="jPRESET_ELE()") ? "if(typeof(jPresetELE)!='undefined'){jPRESET_ELE('"+i+"', '"+j+"');}" : "";
				//htmlCal += "<a href=\"javascript:void(0)\" onClick=\"document.getElementById('" + this.targetObj + "').value = '" + arrObj[i][j] + "' ;document.getElementById('" + this.objName + "').style.display= 'none';setDivVisible(false, '"+this.objName+"', 'lyrShim');"+"\" title=\"" + arrObj[i][j] + "\">" + arrObj[i][j] + "</a>";
				htmlCal += "<a href=\"javascript:void(0)\" onClick=\"document.getElementById('" + this.targetObj + "').value = '" + arrObj[i][j] + "' ;setDivVisible(false, '"+this.layerID+"', 'lyrShim'); \" + title=\"" + arrObj[i][j] + "\">" + arrObj[i][j] + "</a>";
				htmlCal += "</td></tr>";
			}
		}
		
		htmlCal += '</table>';
		htmlCal += '</div>';
		
		htmlCal += '</td>';
		htmlCal += '<td width="6" background="' + this.imagesPath + 'can_board_06.gif"><img src="' + this.imagesPath + 'can_board_06.gif" width="6" height="6"></td>';
		htmlCal += '</tr>';
		htmlCal += '<tr>';
		htmlCal += '<td width="5" height="6"><img src="' + this.imagesPath + 'can_board_07.gif" width="5" height="6"></td>';
		htmlCal += '<td height="6" background="' + this.imagesPath + 'can_board_08.gif"><img src="' + this.imagesPath + 'can_board_08.gif" width="5" height="6"></td>';
		htmlCal += '<td width="6" height="6"><img src="' + this.imagesPath + 'can_board_09.gif" width="6" height="6"></td>';
		htmlCal += '</tr>';
		htmlCal += '</table>';
		htmlCal += '</td>';
		htmlCal += '</tr>';
		htmlCal += '</table>';
	
		this._setHTML(htmlCal);
		
		if (!arguments[0])
		{
			this._showLayer();
			this._setLayerPosition();
			setDivVisible(true, this.layerID, "lyrShim");
		}
	
	}


/**
* Writes HTML to document for layer
*
* @access public
*/
	function dynList_writeHTML()
	{
		if (is_ie5up || is_nav6up || is_gecko) {
			document.write('<div class="calendarlayer" id="' + this.layerID + '" onmouseover="' + this.objName + '._mouseover(true)" onmouseout="' + this.objName + '._mouseover(false)" style="display:none;z-index:100;"></div>');
		}
	}

/**
* Sets the offset to the mouse position
* that the calendar appears at.
*
* @access public
* @param integer Xoffset Number of pixels for vertical
*                        offset from mouse position
* @param integer Yoffset Number of pixels for horizontal
*                        offset from mouse position
*/
	function dynList_setOffset(Xoffset, Yoffset)
	{
		this.setOffsetX(Xoffset);
		this.setOffsetY(Yoffset);
	}

/**
* Sets the X offset to the mouse position
* that the calendar appears at.
*
* @access public
* @param integer Xoffset Number of pixels for horizontal
*                        offset from mouse position
*/
	function dynList_setOffsetX(Xoffset)
	{
		this.offsetX = Xoffset;
	}

/**
* Sets the Y offset to the mouse position
* that the calendar appears at.
*
* @access public
* @param integer Yoffset Number of pixels for vertical
*                        offset from mouse position
*/
	function dynList_setOffsetY(Yoffset)
	{
		this.offsetY = Yoffset;
	}

/**
* Sets the images path
*
* @access public
* @param string path Path to use for images
*/
	function dynList_setImagesPath(path)
	{
		this.imagesPath = path;
	}

	// [eCorp]
	function dynList_setPosition(left, top)
	{
		this.pos_top = top;
		this.pos_left = left;
	}


/**
* Sets List array
*
* @access public
* @param integer Yoffset Number of pixels for vertical
*                        offset from mouse position
*/
	function dynList_setListArray(arrName)
	{
		this.listArray = arrName;
	}	
	
/**
* Sets target object
*
* @access public
* @param integer Yoffset Number of pixels for vertical
*                        offset from mouse position
*/
	function dynList_setTargetObj(objName)
	{
		this.targetObj = objName;
	}	

/**
* Returns the layer object
*
* @access private
*/
	function dynList_getLayer()
	{
		var layerID = this.layerID;

		if (document.getElementById(layerID)) {

			return document.getElementById(layerID);

		} else if (document.all(layerID)) {
			return document.all(layerID);
		}
	}

/**
* Hides the calendar layer
*
* @access private
*/
	function dynList_hideLayer()
	{
		this._getLayer().style.visibility = 'hidden';
		setDivVisible(false, this.layerID, "lyrShim");
	}

/**
* Shows the calendar layer
*
* @access private
*/
	function dynList_showLayer()
	{
		this._getLayer().style.visibility = 'visible';
	}

/**
* Sets the layers position
*
* @access private
*/
	function dynList_setLayerPosition()
	{
		this._getLayer().style.top  = (this.pos_top!=null) ? this.pos_top + 'px' : (dynList_mouseY + this.offsetY) + 'px';
		this._getLayer().style.left = (this.pos_left!=null) ? this.pos_left + 'px' : (dynList_mouseX + this.offsetX) + 'px';
		
		//alert(this._getLayer().style.top + " " + this._getLayer().style.left);
	}

/**
* Sets the innerHTML attribute of the layer
*
* @access private
*/
	function dynList_setHTML(html)
	{
		this._getLayer().innerHTML = html;
	}

/**
* onMouse(Over|Out) event handler
*
* @access private
* @param boolean status Whether the mouse is over the
*                       calendar or not
*/
	function dynList_mouseover(status)
	{
		dynList_mouseoverStatus = status;
		return true;
	}

/**
* onMouseMove event handler
*/
	dynList_oldOnmousemove = document.onmousemove ? document.onmousemove : new Function;

	document.onmousemove = function ()
	{
		if (is_ie5up || is_nav6up || is_gecko) {
			if (arguments[0]) {
				dynList_mouseX = arguments[0].pageX;
				dynList_mouseY = arguments[0].pageY;
			} else {
				dynList_mouseX = event.clientX + document.body.scrollLeft;
				dynList_mouseY = event.clientY + document.body.scrollTop;
				arguments[0] = null;
			}

			dynList_oldOnmousemove();
		}
	}

/**
* Callbacks for document.onclick
*/
	dynList_oldOnclick = document.onclick ? document.onclick : new Function;

	document.onclick = function ()
	{
		if (is_ie5up || is_nav6up || is_gecko) {
			if(!dynList_mouseoverStatus){
				for(i=0; i<dynList_layers.length; ++i){
					dynList_layers[i]._hideLayer();
				}
			}

			dynList_oldOnclick(arguments[0] ? arguments[0] : null);
		}
	}

	
/**
* Sets the innerHTML attribute of the layer
*
* @access public
*/
	function dynList_trace(str)
	{
		alert(str);
	}