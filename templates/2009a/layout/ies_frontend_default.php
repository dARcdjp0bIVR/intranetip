{{{TEMPLATE_HOME_HEADER}}}
<?php
echo "===>".$PATH_WRT_ROOT;
?>
<link href="web_root_path/templates/2009a/css/IES.css" rel="stylesheet" type="text/css">

<table id="eclass_main_frame_table" name="eclass_main_frame_table" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top">
	<!-- blue-->
	<div style="border:solid thick blue">

<div class="student_stage1"><div class="theme"> <!-- stage 1 background -->
  <div class="top_left_btn"> <!-- logo , scheme, stage btn -->
    <div class="IES_logo"><div></div></div> <!-- IES logo -->
    <div class="scheme_current">
      <!--Scheme Selection-->
      <ul class="scheme_option">
        <li><a href="#">專題 000002</a></li>
        <li><a href="#">專題 000003</a></li>
        <li><a href="#">專題 000004</a></li>
      </ul>
      專題 001</div>
      <a href="#" class="scheme_select" title="選擇其他專題">&nbsp;</a>
      <ul class="stage">
        <li class="current"><a href="#" class="s1">階段 I</a></li>
        <li class="line"></li>
        <li><a href="#" class="s2">階段 II</a></li>
        <li class="line"></li>
        <li><a href="#" class="s3">階段 III</a></li>
      </ul>
    </div> <!--class "top_left_btn" end -->    
    <div class="clear"></div>
    <div class="student_tools"><div> <!-- student tool -->
    <ul>
      <li><a href="#" class="stu_files"><span>文件夾</span></a></li>
      <li><a href="#" class="bibi"><span>文獻記錄</span></a></li>
      <li><a href="#" class="reflect_note"><span>反思手記</span></a></li>
      <li><a href="#" class="worksheet"><span>工作紙</span></a></li>
      <li><a href="#" class="faq"><span>常見問題</span></a></li>
    </ul>
  </div></div> <!-- student tool end-->

  <div class="clear"></div>

  <div class="main_paper_top"><div><div></div></div></div>
  <div class="main_paper_left"><div class="main_paper_right">
    <div class="main_paper_body_left"><div class="main_paper_body_right"><div class="main_paper_body ">
      <div class="stage1_title"><span class="title">探究計劃書</span><span class="deadline">呈交限期 : 10-10-2010</span><div class="clear"></div></div>
      <div class="stage1_paper"> <!-- class "stage1_paper" making a single line paper-->
      {{{HTML_BODY}}}
      </div> <!--class "stage1_paper" end -->
    </div></div></div>
  </div></div>
  <div class="main_paper_bottom"><div><div></div></div></div>        

</div></div> <!-- stage 1 background end -->

</div>
	<!-- blue-->
      
    </td>
  </tr>
  <tr>
    <td>{{{TEMPLATE_IES_FRONTEND_FOOTER}}}</td>
  </tr>
</table>
</td>
</tr>
</table>
</body>
</html>