<?php
/*
 *  // Using: 
 *  
 * 	Log
 * 
 *  2019-01-11 [Cameron]
 *      - change open-sans fonts link from google to local directory (case #D155052)
 *
 *  2018-11-14 [Cameron]
 *      - should disable timeout logic for OPAC (case X152669)
 *      
 * 	2017-09-27 [Cameron]
 * 		- add font-awesome.min.css (for showing trophy in my friends function)
 *  
 * 	2016-12-22 [Cameron] 
 * 		- add <!DOCTYPE html> and $home_header_EmulateIE9 to support CKEditor in IE (version >= IE9) [case #T110678]
 *  
 * 	2016-05-18 [Cameron] create this file
 * 
 */
 
/* Cache miss! Use ob_start to catch all the output that comes next*/   
ob_start();

## $PATH_WRT_ROOT is global in libinterface.LAYOUT_START
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
//include_once($PATH_WRT_ROOT."includes/elibplus2/elibplusConfig.inc.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus.php");
include_once($PATH_WRT_ROOT."includes/elibplus2/libelibplus_ui.php");

## because this file is called in function libinterface.LAYOUT_START, should define following variables as global
global $special_feature, $hideTopMenuPart, $home_header_EmulateIE9;
		

$libelibplus_ui = new libelibplus_ui();

$ControlTimeOutByJS = $special_feature['TimeOutByJS'];


if ($ControlTimeOutByJS)
{
	$TimeOutWarningSecond = $special_feature['TimeOutWarningSecond'] ? $special_feature['TimeOutWarningSecond'] : 120;
	# session max life time in second
	$SessionLifeTime = ini_get('session.gc_maxlifetime') - $TimeOutWarningSecond;
	# below is just for quick testing
	// $SessionLifeTime = 2;
}

?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta Http-Equiv="Cache-Control" Content="no-cache">

<? if ($home_header_EmulateIE9){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<? } else {?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<? }?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<title>eLibrary plus</title>

<link href="/templates/2009a/css/bootstrap.min.css" rel="stylesheet">
<link href="/templates/2009a/css/elibplus2.css" rel="stylesheet">
<!--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>-->
<link href="/templates/2009a/css/open_sans.css" rel="stylesheet" type="text/css">
<link href="/templates/2009a/css/font-awesome.min.css" rel="stylesheet" />
<script src="/templates/jquery/jquery-1.12.1.min.js"></script>
<script src="/templates/jquery/bootstrap.min.js"></script>
<script src="/templates/jquery/jquery.ellipsis.js" type="text/javascript"></script>
<script language="JavaScript" src="/lang/script.<?= $intranet_session_language?>.js"></script>
<script src="/templates/script.js"></script>

<link rel="stylesheet" href="/templates/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css">
<script src="/templates/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
  
<link href="/templates/jquery/jquery.fancybox.css" rel="stylesheet" media="screen"/>
<script src="/templates/jquery/jquery.fancybox.pack.js"></script>
  
<!--favicons-->
<link rel="apple-touch-icon" sizes="57x57" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/2009a/eLibplus2/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/x-icon" href="/images/2009a/eLibplus2/favicons/favicon.ico" />
<link rel="icon" type="image/png" href="/images/2009a/eLibplus2/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/images/2009a/eLibplus2/favicons/favicon-194x194.png" sizes="194x194">
<link rel="icon" type="image/png" href="/images/2009a/eLibplus2/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/images/2009a/eLibplus2/favicons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/images/2009a/eLibplus2/favicons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/images/2009a/eLibplus2/favicons/manifest.json">
<link rel="mask-icon" href="/images/2009a/eLibplus2/favicons/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">


<script language="javascript">
function logout() {
	if(confirm(globalAlertMsg16)) {
        window.location="/logout.php";
	}
}
</script>

<?php
if ($ControlTimeOutByJS)
{
	$bubbleMsgBlockSysTimeout =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
	<span id="ajaxMsgBlockTimeOut">
				<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">
					<tr>
					<td bgcolor="orange" align="center">
					<table width="620" height="400" border="0" cellpadding="12" cellspacing="0">
						<tr>
						<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>
						&nbsp;
						</td>
						</tr>
					</table>
					</td>
					</tr>
				</table>
				</span>
	</div>

sysTimeout;
	
?>
<script language="JavaScript" src="/templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="/templates/ajax_connection.js"></script>
<script language="JavaScript">
var LogoutCountValue = <?=$TimeOutWarningSecond?>;
var SecondLogout = LogoutCountValue;
var ToLogout=false;
var SecondLeftOriginal=<?=$SessionLifeTime?>;
var SecondLeft=<?=$SessionLifeTime?>;

function CountingDown(){
	if (SecondLeft>0)
	{
	   SecondLeft -= 1;
	   setTimeout("CountingDown()",1000);
	} else
	{
		// query server to see if there is no any response from user (e.g. using AJAX)
		path = "/get_last_access.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft);

	}
}

<?php if (isset($_SESSION['UserID'])):?>
	CountingDown();
<?php endif;?>


var callback_timeleft =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);

		if (TimeLeft>0)
		{
			SecondLeft = TimeLeft;
			CountingDown();
		} else
		{
			//alert("testing timeout method!")
			// notify user about the timeout
			// user can press "Continue" to cancel the timeout; if no action taken, system will (call logout page and) direct to timeout result page
			MM_showHideLayers('sub_layer_sys_timeout','','show');		
			bubble_block = document.getElementById("sub_layer_sys_timeout");
			bubble_block.style.left = "0px";
			bubble_block.style.top = '0px';
			bubble_block.focus();
		
			ToLogout = true;
			CountDownToLogout();
		}
	}
}


function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}


function CountDownToLogout()
{
	if (ToLogout)
	{
		if (SecondLogout>0)
		{
			SecondLogout -= 1;

			var browserWidth = f_clientWidth();
			var browserHeight = f_clientHeight(); 
			
			jsTimeOutTxt2 = '<table class="timeoutAlert"  width="'+(browserWidth)+'" height="'+(browserHeight)+'"><tr><td align="center">';
			jsTimeOutTxt2 += '<div class="contentContainer">';
			jsTimeOutTxt2 += '<div class="textTitle"><span class="timeLeft">&nbsp;<?=$Lang['SessionTimeout']['ReadyToTimeout_Header']?></span></div>';
			jsTimeOutTxt2 += '<div class="textContent"><div class="line"><span><?=$Lang['SessionTimeout']['ReadyToTimeout_str1']?> <U> '+SecondLogout+' </U><?=$Lang['SessionTimeout']['ReadyToTimeout_str2']?><br><?=$Lang['SessionTimeout']['ReadyToTimeout_str3']?> </span> <input type="button"  value="<?=$Lang['SessionTimeout']['Continue']?>" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()"> ';
			jsTimeOutTxt2 += '<span><?=$Lang['SessionTimeout']['ReadyToTimeout_str4']?></span></div> ';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</td></tr></table>';
			jsTimeOutTxt2 += '	</table>';
			
			document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt2; 

			setTimeout("CountDownToLogout()",1000);
			
			// update message
		} else
		{
			// redirect to logout page
			self.location = "/logout.php?IsTimeout=1";
		}
	} else
	{
		// reset logout timer
		SecondLogout = LogoutCountValue;
	}
}



var callback_timeleft_trival =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);
	}
}



function CancelTimeOut()
{
	// stop redirect function
	SecondLogout = LogoutCountValue;
	ToLogout = false;
	alert("<?=$Lang['SessionTimeout']['CancelTimeout']?>");

	// restart counting down for time out
	SecondLeft = SecondLeftOriginal;
	CountingDown();
	
	var path = "/update_last_access.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft_trival);
}


function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}


</script>
<?php
}
?>

</head>
<body id="elib" <?=(!isset($_SESSION['UserID']) ? 'class="opac"' : '')?>>
<? if (!$hideTopMenuPart): ?>
<?=(isset($_SESSION['UserID']) ? $libelibplus_ui->getPortalTopPart() : $libelibplus_ui->getOpacPortalTopPart())?>
<? endif;?>
<?=$bubbleMsgBlockSysTimeout?>
