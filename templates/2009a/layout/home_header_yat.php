<?
# using: yat
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

global $linterface;

# Basic Information
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
if ($intranet_default_lang == "gb")
	($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
else	
	($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];

# eClass eCommunity data
$cs_path = "$intranet_httppath/home/support.php";

# user identity
switch($_SESSION['UserType'])
{
	case 1:
		$UserIdentity = $_SESSION['isTeaching']==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
		$UserIdentityIcon = $_SESSION['isTeaching']==1 ? "icon_teacher.gif" : "icon_nonteacher.gif";
		break;
	case 2:
		$UserIdentity = $Lang['Identity']['Student'];
		$UserIdentityIcon = "icon_student.gif";
		break;
	case 3:
		$UserIdentity = $Lang['Identity']['Parent'];
		$UserIdentityIcon = "icon_parent.gif";
		break;
}

# Build Menu
unset($FullMenuArr['topMenu']);
unset($FullMenuArr['subMenu']);

# Check menu focus 
# Home
$CurrentPageArr['Home'] = 1;

# eService

# eLearning

# eAdmin

# School Settings
if ($CurrentPageArr['Location'] || $CurrentPageArr['Role'] || $CurrentPageArr['Subjects'] || $CurrentPageArr['Class'] || $CurrentPageArr['Group'] || $CurrentPageArr['SchoolCalendar'] || $CurrentPageArr['Timetable'] || $CurrentPageArr['Period'])
{
	$CurrentPageArr['Home'] = 0;
	$CurrentPageArr['SchoolSettings'] = 1;
}

# === $MenuArr : array fields description====
#	1 - menu id 
#	2 - path / #  
#	3 - section name
#	4 - section focus flag
#	5 - css (will change the icon)
$FullMenuArr[0] = array(0, "/home/index.php", $Lang['Header']['Menu']['Home'], $CurrentPageArr['Home'], 'home');

$FullMenuArr[1][0] = array(1, "#", $Lang['Header']['Menu']['eService'], $CurrentPageArr['eService']);
$FullMenuArr[1][1][] = array(11, "#", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['PageCircular']);
$FullMenuArr[1][1][] = array(12, "#", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eHomework']);
$FullMenuArr[1][1][] = array(13, "#", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard']);
$FullMenuArr[1][1][] = array(14, "#", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eBooking']);
$FullMenuArr[1][1][] = array(15, "#", $Lang['Header']['Menu']['CampusTV'], $CurrentPageArr['eServiceCampusTV']);
$FullMenuArr[1][1][] = array(16, "#", $Lang['Header']['Menu']['Organization'], $CurrentPageArr['PageSchoolInfoOrganization']);

$FullMenuArr[2][0] = array(2, "#", $Lang['Header']['Menu']['eLearning'], $CurrentPageArr['eLearning']);
$FullMenuArr[2][1][] = array(21, "#", $Lang['Header']['Menu']['eClass'], $CurrentPageArr['PageeClasseClassMy']);
$FullMenuArr[2][1][] = array(22, "#", $Lang['Header']['Menu']['eLC'], $CurrentPageArr['eLC']);
$FullMenuArr[2][1][] = array(23, "#", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib']);

$FullMenuArr[3][0] = array(3, "#", $Lang['Header']['Menu']['eAdmin'], $CurrentPageArr['eAdmin']);
$FullMenuArr[3][1][0][0] = array(31, "#", $Lang['Header']['Menu']['StaffManagement'], $CurrentPageArr['StaffManagement']);
$FullMenuArr[3][1][0][1][] = array(311, "#", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StaffeAttednance']);
$FullMenuArr[3][1][0][1][] = array(312, "#", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eCircular']);
$FullMenuArr[3][1][1][0] = array(32, "#", $Lang['Header']['Menu']['StudentManagement'], $CurrentPageArr['StudentManagement']);
$FullMenuArr[3][1][1][1][] = array(322, "#", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eDiscipline']);
$FullMenuArr[3][1][1][1][] = array(323, "#", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard']);
$FullMenuArr[3][1][1][1][] = array(324, "#", $Lang['Header']['Menu']['eEnrolment'], $CurrentPageArr['eEnrolment']);
$FullMenuArr[3][1][1][1][] = array(325, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eSports']);
$FullMenuArr[3][1][1][1][] = array(326, "#", $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eNotice']);
$FullMenuArr[3][1][2][0] = array(32, "#", $Lang['Header']['Menu']['ResourcesManagement'], $CurrentPageArr['ResourcesManagement']);
$FullMenuArr[3][1][2][1][] = array(331, "#", $Lang['Header']['Menu']['eInventory'], $CurrentPageArr['eInventory']);
$FullMenuArr[3][1][2][1][] = array(332, "#", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eBooking']);

$FullMenuArr[4][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting');
$FullMenuArr[4][1][] = array(41, $PATH_WRT_ROOT."home/system_settings/location/", $Lang['Header']['Menu']['Location'], $CurrentPageArr['Location']);
$FullMenuArr[4][1][] = array(42, $PATH_WRT_ROOT."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role']);
$FullMenuArr[4][1][] = array(43, $PATH_WRT_ROOT."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subjects'], $CurrentPageArr['Subjects']);
$FullMenuArr[4][1][] = array(44, $PATH_WRT_ROOT."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class']);
$FullMenuArr[4][1][] = array(45, "#", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group']);
$FullMenuArr[4][1][] = array(46, $PATH_WRT_ROOT."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar']);
$FullMenuArr[4][1][] = array(47, $PATH_WRT_ROOT."home/system_settings/timetable/", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable']);
$FullMenuArr[4][1][] = array(48, $PATH_WRT_ROOT."home/system_settings/period/", $Lang['Header']['Menu']['Period'], $CurrentPageArr['Period']);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?=$Lang['Header']['HomeTitle']?></title>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function support()
{
         newWindow("<?=$cs_path?>",10);
}

//-->
</script>

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
		<td class="top_banner">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="120" rowspan="2" valign="middle" align="center">
					<div class="school_logo">&nbsp;</div>
					<img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/topbar_25/eclass_logo.gif">
				</td>
				<td height="38" valign="top" nowrap>
					<div class="school_title"> <?= $school_name ?> </div>
					<div class="user_login">
						<img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/topbar_25/<?=$UserIdentityIcon?>" align="absmiddle"> <?=$UserIdentity?> 
						<span>|</span> <a href="javascript:support()" title="<?=$Lang['Header']['eClassCommunity']?>">?</a>
						
						<a href="/lang.php?lang=<?= $lang_to_change?>" title="<?=$Lang['Header']['ChangeLang']?>"><?=$change_lang_to?></a>
						
						<a href="/logout.php" onClick="if(confirm(globalAlertMsg16)){return true;}else{return false;}" title="Logout">X</a>					
					</div>
				</td>
			</tr>
				<tr>
					<td valign="top" nowrap>
						<div id="main_menu" class="mainmenu">
							<?= GEN_TOP_MENU_IP25($FullMenuArr);?>
						</div>
						<div class="itool_menu"> 
						<ul><li><a href="#" class="itool_imail" title="iMail"></a></li> 
							<li><a href="#" class="itool_icalendar" title="iCalendar">&nbsp;</a></li>
							<li><a href="#" class="itool_iaccount" title="iAccount">&nbsp;</a></li>
							<li><a href="#" class="itool_ifolder" title="iFolder">&nbsp;</a></li>
							<li><a href="#" class="itool_iportfolio" title="iPortfolio">&nbsp;</a></li>
							<li><span>|</span></li>
							<li><a href="#" class="itool_campus_link" title="Campus Link">&nbsp;</a></li>
							<li><a href="#" class="itool_rss" title="RSS">&nbsp;</a></li>
							</ul>
						</div>
					</td>
				</tr>
		</table></td>
  	</tr>
</table>


<?
############### IP25 Top Menu #############################################
	# === $MenuArr : array fields description====
	#	1 - menu id 
	#	2 - path / #  (if #, sub-menu will be displayed)
	#	3 - section name
	#	4 - section focus flag
	#	5 - css (will change the icon)
	###########################################################################
// 	function GEN_TOP_MENU($Arr = array())
// 	{
// 		//global $FullMenuArr;
// 		
// 		$ReturnStr = "";
// 		
// 		//GEN_TOP_MENU_SUB();
// 		$ReturnStr .= GEN_TOP_MENU_SUB($FullMenuArr);
// 		
// 		return $ReturnStr;
// 	}
	
	function GEN_TOP_MENU_IP25($Arr = array())
	{
		$x = "";
		if(sizeof($Arr))
		{
			$x .= "<ul>";
			for($i=0;$i<sizeof($Arr);$i++)
			{
				if(is_array($Arr[$i][0]))
				{
					$x .= GEN_TOP_MENU_li($Arr[$i][0]);
					$x .= GEN_TOP_MENU_IP25($Arr[$i][1]);
				}
				else
				{
					$x .= GEN_TOP_MENU_li($Arr[$i]);
				}
			}
			$x .= "</ul>";
		}
		return $x;
	}
	
	function GEN_TOP_MENU_li($Arr = array())
	{
		$x = "";
		list($menuid, $link, $name, $focus, $css) = $Arr;
		$this_css = $css ? $css : "";
		$focus_css = $focus ? ($this_css ? $this_css."_current" : "current_module") : $this_css;
		$x .= "<li><a href='$link' class='$focus_css'>$name</a></li>";
		
		return $x;
	}

?>