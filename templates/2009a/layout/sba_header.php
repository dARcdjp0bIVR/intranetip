<?
global $id, $sba_h_scheme_select, $sba_h_stage_select, $sba_h_student_toolbar, $scheme_id, $favicon_image;

$PATH_WRT_ROOT_ABS = "/";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<title>eClass IP 2.5 :: SBAIES ::</title>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

//-->
</script>
<!--<link href="css/text" rel="stylesheet" type="text/css">-->
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/sba/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/sba/SBAIES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/sba/SBAIES_font_L.css" rel="stylesheet" type="text/css">

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jqueryslidemenu.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-latest.js"></script>
<script type="text/javaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/thickbox.js"></script>
<script type="text/javaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/ui.core.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/ui.draggable.js"></script>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/js/sba/SBAIES.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/js/sba/sba.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>lang/script.<?=$intranet_session_language?>.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js"></script>




<link rel="stylesheet" href="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/sba/SBAIES_override.css" type="text/css">
<!--<script type="tet/javaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>-->
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="SBA">
<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr> 
  	<td valign="top">
	<div class="student_stage1"><div class="theme"> <!-- stage background -->
    
    	<div class="top_left_btn"> <!-- logo , scheme, stage btn -->
        	<div class="SBA_logo"><div><a href="/home/eLearning/sba/index.php?mod=content&task=scheme_list&scheme_id=<?=$scheme_id?>">&nbsp;</a></div></div> <!-- SBA logo -->
			<?=$sba_h_scheme_select?>
			
            <?=$sba_h_stage_select?>
            
        </div> <!--class "top_left_btn" end -->    
            
        <div class="clear"></div>
        
        
        <?=$sba_h_student_toolbar?> 
        <!--
        <div class="clear"></div>
        -->
        <!-- Start of message --> 
        
        <div id="system_message_box" class="SystemReturnMessage" style="display:block; visibility: hidden;"> 
			<div class="msg_board_left">
		  	<div class="msg_board_right" id="message_body"> 
		  	
		  	</div>
			</div>
		</div>
        <!-- End of message -->  
        
        
        <div class="IES_sheet_wrap" style="padding-right:1px"> <!-- IES_sheet_wrap end -->      
        
        <div class="IES_sheet"> <!-- IES_sheet end -->      
        
        
        
        