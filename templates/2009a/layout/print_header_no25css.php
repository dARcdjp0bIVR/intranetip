<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html style="background-color: #ffffff">
<head>
<meta http-equiv='pragma' content='no-cache' />
<?= returnHtmlMETA() ?>
<title>eClass IP</title>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/print.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/print_25.css" rel="stylesheet" type="text/css" />

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<style type='text/css'> 
 P.breakhere {page-break-before: always}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff; background-image: none">