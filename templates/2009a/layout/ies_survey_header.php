<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="<?=$html_ies_survey_body_class?>">
<head>
<?= returnHtmlMETA() ?>
<title><?= $i_home_title ?></title>

<link href="css/text" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/IES_font_L.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/IES_q_pop.css" rel="stylesheet" type="text/css">

<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jqueryslidemenu.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/ies.js"></script>

</head>

<body class="<?=$html_ies_survey_body_class?>">
  <div class="form_board">
    <div class="m_clip"></div>
    <div class="q_top"><div><div></div></div></div>
    <div class="q_left"><div class="q_right">
      <div class="q_wrapper">