<?php 
## Using By :

/********************** Change Log ***********************/
/*
 *  Date:       2019-01-22 Isaac
 *              Changed the file from static to relative file calling the common template file
 *
 */

/********************** Change Log ***********************/
	session_start();
	$_SESSION['PowerClass_PAGE'] = "schoolnews";
	
	$containerBGColorClass = "bgColor-2";
	$this->loadFooter($source_path, 'custom');
	
	if(file_exists("../..".$powerClass_common_template_source_path."schoolnews.php")){
	    include_once ("../..".$powerClass_common_template_source_path."schoolnews.php");
	}
?>