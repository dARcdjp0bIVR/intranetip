// using by : 

////////////////////// Change Log ////
//
//	Date:	2018-09-28 Bill
//		support new payment question type - Must Pay
//
//	Date:	2017-01-18 Bill
//		modified viewForm(), analysisData(), fixed js error when reply slip has long question
//
//	Date:	2011-02-16 YatWoon
//		add DisplayQuestionNumber checking, if DisplayQuestionNumber==1 display question number before the question
//
////////////////////////////////////


function viewForm(txtStr, txtAns){
	//alert(txtAns);
	var txtArr=txtStr.split("#QUE#");
	ansArr=txtAns.split("#ANS#");
	var tmpArr = null;
	var txtStr = null;

	txtStr='<Form name="answersheet">';
        txtStr+='<table width="100%" border="0" cellpadding="0" cellspacing="5">';
	for (var x=1; x<txtArr.length; x++){
		j = x - 1;
		tmpArr = txtArr[x].split("||");
		type_no = tmpArr[0].split(",");			//0:type, 1:number of options
		question = tmpArr[1];
		opts = tmpArr[2];

		if(type_no[0]!=10) {
			tmpArr = opts.split("#OPT#");
		}

        txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
		txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
		txtStr+='</td></tr>\n';
                
		txtStr+='<table border="0" width="100%"><tr><td class="tabletext" colspan="2">\n';
		txtStr+=question+'</td></tr>\n';
                txtStr+='<td class="tabletext">&nbsp;</td>';
		txtStr+='<td width="95%" class="tabletext">';

		switch (type_no[0]){
			case "1":
			case "2":
			case "12":
				txtStr += (isNaN(parseInt(ansArr[x]))) ? "--" : tmpArr[parseInt(ansArr[x])+1];
				break;
			case "3":
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];					    
						c_i_i = 1;
					}
				}
				break;
			/*
			case "4":
			case "5":
				txtStr += ansArr[x];
				break;
			case "6":
				break;
			case "7":
			case "8":
			*/
			case "9":
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];					    
						c_i_i = 1;
					}
				}
				break;
		}
		txtStr+=(type_no[0]=="6") ? '</td>' : '</td>';
		txtStr+='</table>\n';
                txtStr+='</td></tr>';
                
	}

        txtStr+='</table>';
	txtStr+='</Form>';

	return txtStr;
}

function viewFormPreview(txtStr, txtAns, txtCountAmount, txtAmountTotal){

	var txtArr=txtStr.split("#QUE#");
	ansArr=txtAns.split("#ANS#");
	//var nonPayItemArr = txtStr.split("#NonPaymentItem#");

	var tmpArr = null;
	var txtStr = null;
	var coumtAmtArr = new Array();
	var countAmtTotal = new Array();
	var nonPayItem;
	var ct = 0;
	var pos = 0;

	countAmtArr = txtCountAmount.split(",");
	countAmtTotal = txtAmountTotal.split(",");


	txtStr='<Form name="answersheet">';
    txtStr+='<table width="100%" border="0" cellpadding="0" cellspacing="5">';
	for (var x=1; x<txtArr.length; x++){	// for each question
		j = x - 1;
		tmpArr = txtArr[x].split("||");
		type_no = tmpArr[0].split(",");			//0:type, 1:number of options
		question = tmpArr[1];
		opts = tmpArr[2];
		//nonPayItem = nonPayItemArr[x][0];

		if(type_no[0]!=10) {
			tmpArr = opts.split("#OPT#");
		}
		
        txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
		txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
		txtStr+='</td></tr>\n';
                
		txtStr+='<table border="0" width="100%"><tr><td class="tabletext" colspan="2">\n';
		if(typeof(DisplayQuestionNumber)!='undefined')
		{
			if(DisplayQuestionNumber==1)
						{
							txtStr += (x) + '. ';
						}
		}
		switch (type_no[0]){
			case "1": 
			case "12": 
				txtStr += (countAmtTotal[pos]!=undefined && nonPayItem==0) ? question+' ($'+countAmtTotal[pos]+')'+'</td></tr>\n' : (nonPayItem==1 ? question+'</td></tr>\n' : question+' ($'+countAmtTotal[pos]+')</td></tr>\n');
		        txtStr+='<td class="tabletext" width=5%>&nbsp;</td>';
				txtStr+='<td width="95%" class="tabletext">';
				txtStr += (isNaN(parseInt(ansArr[x]))) ? "--" : tmpArr[parseInt(ansArr[x])+1];
				//txtStr += " ($"+countAmtTotal[pos]+")";
				pos += parseInt(countAmtArr[ct]);
				ct++;
				break;
			case "2":
				txtStr+=question+'</td></tr>\n';
		                txtStr+='<td class="tabletext" width=5%>&nbsp;</td>';
				txtStr+='<td width="95%" class="tabletext">';
				txtStr += (isNaN(parseInt(ansArr[x]))) ? "--" : (nonPayItem==0 ? "$"+tmpArr[parseInt(ansArr[x])+1] : "$"+tmpArr[parseInt(ansArr[x])+1]);
				break;
			case "3":
				txtStr+=question+'</td></tr>\n';
		                txtStr+='<td class="tabletext" width=5%>&nbsp;</td>';
				txtStr+='<td width="95%" class="tabletext">';
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				//alert(checkAns.length);
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];		
						txtStr += " ($"+countAmtTotal[pos+parseInt(checkAns[m])]+")";			    
						c_i_i = 1;
					}
					//pos++;
				}
				pos += parseInt(countAmtArr[ct]);
				ct++;
				break;
			/*
			case "4":
			case "5":
				txtStr += ansArr[x];
				break;
			case "6":
				break;
			case "7":
			case "8":
			*/
			case "9":
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				txtStr+=question+'</td></tr>\n';
		                txtStr+='<td class="tabletext" width=5%>&nbsp;</td>';
				txtStr+='<td width="95%" class="tabletext">';
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];					    
						txtStr += " ($"+countAmtTotal[pos+parseInt(checkAns[m])]+")";			    
						c_i_i = 1;
					}
				}
				
				pos += parseInt(countAmtArr[ct]);
				
				ct++;
				break;
			case "10":
				txtStr+=question+'</td></tr>\n';
				txtStr += '<td class="tabletext" width=5%>&nbsp;</td>\n';
				txtStr += '<td width=95%>'+ansArr[x];
				break;
		}

		//alert(pos);
		txtStr+=(type_no[0]=="6") ? '</td>' : '</td>';
		txtStr+='</table>\n';
                txtStr+='</td></tr>';
                
	}

        txtStr+='</table>';
	txtStr+='</Form>';

	return txtStr;
}

function switchLayer(layer_name, j){
	var hide = "hidden";
	var show = "visible";

	if (!document.all)
		return;

	var co=(document.getElementById && !document.all)? document.getElementById(layer_name).style : document.all[layer_name].style;

	if (co.visibility==show){
		co.visibility = hide;
		co.position  = "absolute";
        eval("document.all.sb"+j+".innerHTML='" + w_show_details+"';");
        co.top = -50000;
	}
	else {
		co.visibility = show;
		co.position  = "relative";
        eval("document.all.sb"+j+".innerHTML='" + w_hide_details+"';");
        co.top = 0;
	}
}



/*################################## STATISTICS ####################################*/
function Statistics()
{
	if (typeof(_statistics_prototype_called) == 'undefined')
	{
		 _statistics_prototype_called = true;
		 Statistics.prototype.qString = null;
		 Statistics.prototype.total = 0;
		 Statistics.prototype.answer = new Array();
		 Statistics.prototype.data = new Array();
		 Statistics.prototype.analysisData = analysisData;
		 Statistics.prototype.getStats = getStats;
		 Statistics.prototype.existPos = existPos;
	}

	function getStats() {
		var resultArr = new Array();
		var txtArr = this.qString.split("#QUE#");
		var dataArr = this.data;
		var tmpArr = null;
		var txtStr = null;

		txtStr='<Form name="answersheet">';
                txtStr+='<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">\n';
                txtStr+='<tr><td align="left" class="eNoticereplytitle">'+w_total_no+' : '+this.total+'</td></tr>\n';
                txtStr+='</table>';
                
                txtStr+='<table width="96%" border="0" cellpadding="0" cellspacing="5" align="center">\n';
		for (var x=1; x<txtArr.length; x++){
			j = x - 1;
			tmpArr = txtArr[x].split("||");
			type_no = tmpArr[0].split(",");			//0:type, 1:number of options
			question = tmpArr[1];
		
            txtStr+='<tr><td align="center" bgcolor="#EFEFEF">\n';
			txtStr+='<table width="100%" border="0" cellspacing="2" cellpadding="2">';
                        
                        txtStr+='<tr>';
                        txtStr+='<td class="tabletext">';
                        if(typeof(DisplayQuestionNumber)!='undefined')
						{
							if(DisplayQuestionNumber==1)
							{
								txtStr += (x) + '. ';
							}
						}
						txtStr+=question+'</td>';
						
                        if (type_no[0]==4 || type_no[0]==5) {
                        		txtStr+='<td width="15%" nowrap>';
                                        txtStr+="<a class=\"tablelink\" href=\"javaScript:switchLayer('l_g"+j+"', "+ j +");\">"+w_view_details_img+ "<span id=\"sb"+ j +"\">"+ w_show_details + "</span></a>";
                                        txtStr+='</td>';
				}
                        
                        txtStr+='</tr>\n';

			if (type_no[0]!=6) {
				txtStr+='<tr>';
				txtStr+='<td align="center" class="tabletext" colspan="2">';

                                if (type_no[0]==4 || type_no[0]==5) {
					txtStr+="<div id='l_g"+j+"' style='position:absolute; left:0px; top:-50000px; width:100%; z-index:1; visibility: hidden'>";
				}
                                txtStr+='<table width="100%" border="0" cellspacing="2" cellpadding="2">';
				for (var i=0; i<dataArr[j].length; i++) {
					percent = (dataArr[j][i][1]==0) ? 0 : Math.round(dataArr[j][i][1]*100/this.total);
					txtStr+='<tr><td width="80%" bgcolor="#FFFFFF" class="tabletext">'+dataArr[j][i][0] + '</td>';
                                        txtStr+='<td nowrap bgcolor="#FFFFFF" class="tabletext">'+dataArr[j][i][1]+' ('+percent+'%)';
					txtStr+=(type_no[0]==4 && i==dataArr[j].length-1) ? '</td></tr>' : '</td></tr>';
				}
				txtStr+='</table>';
				if (type_no[0]==4 || type_no[0]==5)
					txtStr+="</div>";
				txtStr+='</td></tr>';		    
			} else {
			    txtStr+='<tr><td colspan=3>&nbsp;</td></tr>';
			}
			
			txtStr+='</table></td></tr>\n';
		}
                
		txtStr+='</table>\n';
		txtStr+='</Form>';

		return txtStr;
	}


	function analysisData() {
		var resultArr = new Array();
		var txtArr=this.qString.split("#QUE#");

		// each application
		var ii=0;
		for (var i=0; i<this.answer.length; i++) {
			ansArr=this.answer[i].split("#ANS#");
			if(this.answer[i]=="")	
			{
				continue;
			}
			var tmpArr = null;

			// each question
			for (var x=1; x<txtArr.length; x++){
				j = x - 1;
				tmpArr = txtArr[x].split("||");
				type_no = tmpArr[0].split(",");			//0:type, 1:number of options
				question = tmpArr[1];
				opts = tmpArr[2];
				
				if(type_no[0]!=10) {
					tmpArr = opts.split("#OPT#");
				}
				
				if (i==0 || ii==0) {
				    resultArr[j] = new Array();
				}
				
				switch (type_no[0]){
					case "1":
					case "2":
					case "12":
						if (i==0) {
							opt_no = (type_no[0]=="1") ? 2 : type_no[1];
							opt_no = (type_no[0]=="12") ? 1 : opt_no;
							for (var k=0; k<opt_no; k++) {
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						if (!isNaN(parseInt(ansArr[x]))) {
	                        if(parseInt(ansArr[x]) >= resultArr[j].length)
	                        {
								resultArr[j][(resultArr[j].length-1)][1] ++;
	                        }
	                        else
	                        {
								resultArr[j][parseInt(ansArr[x])][1] ++;
	                        }
						}						
						break;
						
					case "3":
						if (ii==0 || i==0) {
							opt_no = type_no[1];
							for (var k=0; k<opt_no; k++) {
 								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						checkAns = ansArr[x].split(",");
						for (var m=0; m<checkAns.length; m++) {
							if (!isNaN(parseInt(checkAns[m]))) {
								if(parseInt(checkAns[m])>=resultArr[j].length)		continue;
							    resultArr[j][parseInt(checkAns[m])][1] ++;
							}
						}
						break;
						
					case "4":
					case "5":
						var answer_pos = this.existPos(resultArr[j], ansArr[x]);
						isArrExpand = (answer_pos==resultArr[j].length);

						if (isArrExpand) {
							resultArr[j][answer_pos] = new Array;
							resultArr[j][answer_pos][0] = ansArr[x];
							resultArr[j][answer_pos][1] = 0;
						}
						resultArr[j][answer_pos][1] ++ ;
						break;
					case "6":
						break;
					case "7":
						break;
					case "8":
						break;	
					case "9":
						if (ii==0 || i==0) {
							opt_no = type_no[1];
							for (var k=0; k<opt_no; k++) {
 								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						checkAns = ansArr[x].split(",");
						for (var m=0; m<checkAns.length; m++) {
							if (!isNaN(parseInt(checkAns[m]))) {
								if(parseInt(checkAns[m])>=resultArr[j].length)		continue;
							    resultArr[j][parseInt(checkAns[m])][1] ++;
							}
						}
						break;
				}
				ii++;
			}
		}

		this.total = this.answer.length;
		this.data = resultArr;
	}

	function existPos(reArr, value) {
		for (var i=0; i<reArr.length; i++) {
			if (reArr[i][0].toUpperCase()==value.toUpperCase())
				return i;
		}
		return reArr.length;
	}

}
