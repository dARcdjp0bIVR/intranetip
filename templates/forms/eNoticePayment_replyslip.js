// Editing by 
/*
 * Require jQuery and jQuery TableDnD.
 */
function Answersheet(paramOptions)
{
	var self = this;
	var options = paramOptions? paramOptions : {};
	var questions = []; // question objects
	var skip_question_indices = []; // question index number to skip
	
	var EDIT_MODE = '0';
	var FILLIN_ANSWER_MODE = '1';
	var PREVIEW_MODE = '2';
	
	var QUE_SEP = '#QUE#';
	var ITEM_SEP = '||';
	var OPT_SEP = '#OPT#';
	var TAR_SEP = '#TAR#';
	var MSUB_SEP = '#MSUB#';
	var CATEGORYID_SEP = '#CATEGORYID#';
	var PAYMENTITEMID_SEP = '#PAYMENTITEMID#';
	var AMT_SEP = '#AMT#';
	var ANS_SEP = '#ANS#';
	var SUBSIDY_SEP = '#SUBSIDY#';
	var IDENTITIES_SEP = '#IDENTITIES#'; // subsidy identities
	var IDENTITY_SEP = '#IDENTITY#';
	var STUDENTS_SEP = '#STUDENTS#'; // individual subsidy students amounts
	var STUDENT_SEP = '#STUDENT#';
	var SUBSIDY_PART_SEP = '_'; // separate subsidy enable status, identities and students
	var QUANTITY_SEP = '#QUANTITY#';
	
	var QTYPE_MUST_PAY = '12';
	var QTYPE_WHETHER_TO_PAY = '1';
	var QTYPE_ONE_PAY_ONE_CAT = '2';
	var QTYPE_ONE_PAY_FEW_CAT = '9';
	var QTYPE_FEW_PAY_FEW_CAT = '3';
	var QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY = '17';
	var QTYPE_TEXT_LONG = '10';
	var QTYPE_TEXT_SHORT = '11';
	var QTYPE_TRUE_OR_FALSE = '13';
	var QTYPE_MC_SINGLE = '14';
	var QTYPE_MC_MULTIPLE = '15';
	var QTYPE_NOT_APPLICABLE = '16';
	
	var require_options_qtypes = [QTYPE_ONE_PAY_ONE_CAT,QTYPE_ONE_PAY_FEW_CAT,QTYPE_FEW_PAY_FEW_CAT,QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY,QTYPE_MC_SINGLE,QTYPE_MC_MULTIPLE];
	var require_submit_qtypes = [QTYPE_WHETHER_TO_PAY,QTYPE_ONE_PAY_ONE_CAT,QTYPE_ONE_PAY_FEW_CAT,QTYPE_FEW_PAY_FEW_CAT,QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY,QTYPE_TEXT_LONG,QTYPE_TEXT_SHORT,QTYPE_TRUE_OR_FALSE,QTYPE_MC_SINGLE,QTYPE_MC_MULTIPLE];
	var go_to_question_msg_color = 'orange';
	var skip_question_msg_color = 'orange';
	var info_msg_style = 'border-radius:5px;background:#74DF00;color:white;text-shadow:0px 0px;font-size:16px;vertical-align:text-top;padding:2px;';
	
	function recurReplace(exp, reby, txt)
	{
	    while (txt.search(exp)!=-1) {
	        txt = txt.replace(exp, reby);
	    }
	    return txt;
	};
	
	function escapeHtml(text)
	{
		  var map = {
		    '&': '&amp;',
		    '<': '&lt;',
		    '>': '&gt;'
		  };
		  
		  text += '';
		  return text.replace(/[&<>]/g, function(m) { return map[m]; });
	};
	
	function escapeDoubleQuotes(text)
	{
		text += '';
		return text.replace(/"/g,'&quot;');
	};
	
	function escapeJQueryId(id) 
	{
	    return id.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" );
	};
	
	function replaceSpecialChars(text)
	{
		var is_ej = self.getOption('IsEJ') == '1'?true:false;
		var map = {
		    '#': '&num;',
		    '|': '&vert;',
		    '"': '&quot;',
		    '\'': '&apos;',
		    '>': '&gt;',
		    '<': '&lt;',
		    '\n': '<br />'
		};
		
		text += '';
		text = text.replace(/[#|"'><\n]/g, function(m) { return map[m]; });
		if(is_ej){
			text = ' ' + $.trim(text) + ' ';
		}
		return text;
	};
	
	function reverseReplaceSpecialChars(text)
	{
		var is_ej = self.getOption('IsEJ') == '1'?true:false;
	
		text += '';
		text = text.replace(/&num;/g,'#').replace(/&vert;/g,'|').replace(/&quot;/g,'"').replace(/&apos;/g,'\'').replace(/&gt;/g,'>').replace(/&lt;/g,'<').replace(/<br \/>/g,'\n');
		if(is_ej){
			text = $.trim(text);
		}
		return text;
	};
	
	function nl2br(text)
	{
		text += '';
		return text.replace(/\n/g,'<br />');
	};
	
	function convertToFloat(value)
	{
		var converted_value = parseFloat(value);
		if(isNaN(converted_value)){
			return 0;
		}else{
			return converted_value;
		}
	};
	
	function convertToInteger(value)
	{
		var converted_value = parseInt(value);
		if(isNaN(converted_value)){
			return 0;
		}else{
			return converted_value;
		}
	};
	
	function convertToString(value)
	{
		return new String(value).toString();
	};
	
	function convertToEmptyStringOrInteger(value)
	{
		if($.trim(value) == ''){
			return '';
		}
		var converted_value = parseInt(value);
		if(isNaN(converted_value)){
			return '';
		}else{
			return converted_value;
		}
	};
	
	function formatDisplayNumber(value)
	{
		return convertToFloat(value).toFixed(2);
	}
	
	function extractNumberText(value)
	{
		var parts = value.match(/^(\d+\.?\d*)(.*)$/);
		if(parts){
			return [convertToFloat(parts[1]),parts[2]];
		}else{
			return [0,''];
		}
	}
	
	function initDnDTable(callback)
	{
		var timerId = null;
		$('.DragAndDropTable').tableDnD({
		    onDrop: function(table, row) {
		    	if(timerId){
		    		clearTimeout(timerId);
		    		timerId = null;
		    	}
		        timerId = setTimeout(function(){callback(table, row);},200);
			},
			dragHandle: "Dragable",
			onDragClass: "move_selected"
		});
	}
	
	function createSelection(data,attributes,selectedValue)
	{
		var html = '<select '+attributes+'>\n';
		var is_array = typeof(selectedValue) == 'object' && selectedValue.length;
		for(var i=0;i<data.length;i++){
			var is_selected = selectedValue? (is_array && selectedValue.indexOf(data[i][1]) != -1? 'selected' : (selectedValue == data[i][1]? 'selected' : '')) : '';
			html+= '<option value="'+escapeDoubleQuotes(data[i][1])+'"'+is_selected+'>'+escapeHtml(data[i][0])+'</option>\n';
		}
			html+= '</select>\n';
		
		return html;
	};
	
	function makeRangeDataForSelection(min,max)
	{
		var ary = [];
		for(var i=min;i<=max;i++){
			ary.push([i,i]);
		}
		
		return ary;
	};
	
	self.getOption = function(key)
	{
		return (options[key])? options[key] : '';
	};
	
	self.setOption = function(key,val)
	{
		options[key] = val;
	};
	
	self.generateTemplatesSelection = function()
	{
		var templates_ary = self.getOption('TemplatesAry');
		var str_templates = self.getOption('StrTemplates');
		var data = [];
		data.push(['- '+str_templates+' -','']);
		for(var i=0;i<templates_ary.length;i++){
			data.push(templates_ary[i]);
		}
		
		var html = createSelection(data,' id="Template" name="Template" ');
		return html;
	};
	
	self.generateQuestionTypeSelection = function()
	{
		var str_non_payment_item = ' ('+self.getOption('StrNonPaymentItem')+')';
	
		var data = [];
		data.push(['- '+self.getOption('StrFormat')+' -','']);
		data.push([self.getOption('StrMustPay'),QTYPE_MUST_PAY]);
		data.push([self.getOption('StrWhetherToPay'),QTYPE_WHETHER_TO_PAY]);
		data.push([self.getOption('StrOnePayOneCat'),QTYPE_ONE_PAY_ONE_CAT]);
		data.push([self.getOption('StrOnePayFewCat'),QTYPE_ONE_PAY_FEW_CAT]);
		data.push([self.getOption('StrFewPayFewCat'),QTYPE_FEW_PAY_FEW_CAT]);
		data.push([self.getOption('StrFewPayFewCatWithInputQuantity'),QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY]);
		data.push([self.getOption('StrTextLong')+str_non_payment_item,QTYPE_TEXT_LONG]);
		data.push([self.getOption('StrTextShort')+str_non_payment_item,QTYPE_TEXT_SHORT]);
		data.push([self.getOption('StrTrueOrFalse')+str_non_payment_item,QTYPE_TRUE_OR_FALSE]);
		data.push([self.getOption('StrMCSingle')+str_non_payment_item,QTYPE_MC_SINGLE]);
		data.push([self.getOption('StrMCMultiple')+str_non_payment_item,QTYPE_MC_MULTIPLE]);
		data.push([self.getOption('StrNotApplicable')+str_non_payment_item,QTYPE_NOT_APPLICABLE]);
		
		var html = createSelection(data,' id="qType" name="qType" ');
		return html;
	};
	
	self.generateAnswerOptionsSelection = function(minOptionValue)
	{
		var data = [];
		var min_no = minOptionValue? minOptionValue : 2;
		var max_no = parseInt(self.getOption('MaxOptionNo'));
		data.push(['- '+self.getOption('StrOptions')+' -','']);
		for(var i=min_no;i<=max_no;i++){
			data.push([i,i]);
		}
		
		var html = createSelection(data,' id="oNum" name="oNum" ');
		return html;
	};
	
	self.generatePaymentCategorySelection = function(id,name,selectedValue)
	{
		var category_ary = self.getOption('PaymentCategoryAry');
		var data = [];
		data.push(['- '+self.getOption('StrPaymentCategory')+' -','']);
		for(var i=0;i<category_ary.length;i++){
			data.push([category_ary[i]['Name'],category_ary[i]['CatID']]);
		}
		
		var html = createSelection(data,(id!=''?' id="'+escapeDoubleQuotes(id)+'" ':'')+' name="'+escapeDoubleQuotes(name)+'" ',selectedValue);
		return html;
	};
	
	self.generateGoToQuestionSelection = function(id,name,selectedValue,startIndex,endIndex)
	{
		var data = [];
		var str_go_to_question = self.getOption('StrGoToQuestion');
		var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
		data.push(['- '+self.getOption('StrNA')+' -','']);
		for(var i=startIndex;i<=endIndex;i++){
			data.push([str_go_to_question+' '+(i+1),i]);
		}
		data.push([str_go_to_end_of_replyslip,-1]);
		
		var html = createSelection(data,(id!=''?' id="'+escapeDoubleQuotes(id)+'" ':'')+' name="'+escapeDoubleQuotes(name)+'" class="jump-question-selection" ',selectedValue);
		return html;
	};
	
	self.renderEditPanel = function(containerId)
	{
		self.setOption('Mode',EDIT_MODE);
		
		var html = '';
		
		var is_debug = self.getOption('Debug');
		
		var all_fields_req = self.getOption('AllFieldsReq');
		var display_question_number = self.getOption('DisplayQuestionNumber');
		
		var str_basic_settings = self.getOption('StrBasicSettings');
		var str_all_require_to_fill = self.getOption('StrAllRequireToFill');
		var str_display_question_number = self.getOption('StrDisplayQuestionNumber');
		var str_question_setting = self.getOption('StrQuestionSetting');
		var str_reply_slip = self.getOption('StrReplySlip');
		var str_reply_slip_questions = self.getOption('StrReplySlipQuestions');
		var str_templates = self.getOption('StrTemplates');
		var str_topic_title = self.getOption('StrTopicTitle');
		var str_format = self.getOption('StrFormat');
		var str_options = self.getOption('StrOptions');
		var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
		var str_add = self.getOption('StrAdd');
		
		var templates_selection_html = self.generateTemplatesSelection();
		var qtype_selection_html = self.generateQuestionTypeSelection();
		var options_selection_html = self.generateAnswerOptionsSelection();
		
		html +=	'<div>\n';
		html +=		'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
		html +=			'<tr>\n';
		html +=				'<td> &nbsp; <span class="sectiontitle_v30">'+str_basic_settings+'</span></td>\n';
		html += 		'</tr>\n';
		html += 		'<tr>\n';
		html +=				'<td>\n';
		html +=					'<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">\n';
		html +=						'<tr>\n';
		html +=							'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_all_require_to_fill+'</span></td>\n';
		html +=							'<td class="tabletext"><input type="checkbox" id="AllFieldsReq" name="AllFieldsReq" value="1" '+(all_fields_req?'checked':'')+' /></td>\n';
		html +=						'</tr>\n';
		html +=						'<tr>\n';
		html +=							'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_display_question_number+'</span></td>\n';
		html +=							'<td class="tabletext"><input type="checkbox" id="DisplayQuestionNumber" name="DisplayQuestionNumber" value="1" '+(display_question_number?'checked':'')+' /></td>\n';
		html +=						'</tr>\n';
		html +=					'</table>\n';
		html +=				'</td>\n';
		html +=			'</tr>\n';
		html +=		'</table>\n';
		html +=	'</div>\n';
		
		html += '<div id="blockDiv">\n';
		html += 	'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n';
		html += 		'<tr>\n';
		html += 			'<td> &nbsp; <span class="sectiontitle_v30">'+str_question_setting+'</span></td>\n';
		html += 		'</tr>\n';
		html += 		'<tr>\n';
		html += 			'<td>\n';
		html += 				'<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">\n';
		html += 					'<tr>\n';
		html += 						'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_templates+'</span></td>';
		html += 						'<td class="tabletext">'+templates_selection_html+'</td>';
		html += 					'</tr>';
		html += 					'<tr>';
		html += 						'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_topic_title+'</span></td>';
		html += 						'<td class="tabletext"><textarea id="secDesc" name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td>';
		html += 					'</tr>';
		html += 					'<tr>';
		html += 						'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_format+'</span></td>';
		html += 						'<td class="tabletext">'+qtype_selection_html+'</td>';
		html += 					'</tr>';
		html += 					'<tr id="optionTr" name="optionTr" style="display:none">';
		html += 						'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_options+'</span></td>';
		html += 						'<td class="tabletext">'+options_selection_html+'</td>';
		html += 					'</tr>';
		html += 					'<tr id="submitTr" name="submitTr" style="display:none">';
		html += 						'<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext">'+str_question_need_to_reply+'</span></td>';
		html += 						'<td class="tabletext"><input type="checkbox" id="mustSubmitOption" name="mustSubmitOption" value="1" '+(all_fields_req?'checked':'')+' /></td>';
		html += 					'</tr>';
		html += 				'</table>';
		html += 			'</td>';
		html += 		'</tr>';
		html += 		'<tr>';
		html += 			'<td>';
		html += 				'<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">';
		html += 					'<tr>';
		html += 						'<td class="dotline" colspan="2"><img src="/images/2009a/10x10.gif" width="10" height="1" /></td>';
		html += 					'</tr>';
		html += 				'</table>';
		html += 			'</td>';
		html += 		'</tr>';
		html += 		'<tr>';
		html += 			'<td colspan="2" align="center">';
		html += 				'<input type="button" class="formsubbutton" name="add_btn" id="add_btn" value="'+str_add+'" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
		if(is_debug){
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_btn" id="debug_btn" value="Debug" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_construct_question_btn" id="debug_construct_question_btn" value="Construct Question" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_parse_question_btn" id="debug_parse_question_btn" value="Parse Question" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
		}
		html += 			'</td>';
		html += 		'</tr>';
    	html += 	'</table>';
		html += '</div>';
		
		html += '<div id="blockInput" style="background-color:transparent;width:100%; visibility:visible;">\n';
		html += '<form name="answersheet">\n';
		html +=		'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
		html +=			'<tbody>\n';
		html +=				'<tr>\n';
		html +=					'<td align="center">'+str_reply_slip+'</td>\n';
		html +=				'</tr>\n';
		html +=				'<tr>\n';
		html +=					'<td><span class="sectiontitle_v30">'+str_reply_slip_questions+'</span></td>\n';
		html +=				'</tr>\n';
		html +=				'<tr>\n'
		html +=					'<td>\n';
		html +=						'<div id="replySlipDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;">\n';
		html +=							'<table width="100%" border="0" cellpadding="0" cellspacing="5" id="ContentTable" class="DragAndDropTable">\n';	
		html +=								'<tbody>\n';
		html +=								'</tbody>\n';
		html +=							'</table>\n';
		html +=						'</div>\n';
		html +=					'</td>\n';
		html +=				'</tr>\n';
		html +=			'</tbody>\n';
		html +=		'</table>\n';
		html += '</form>\n';
		html += '</div>\n';
		
		$('#'+escapeJQueryId(containerId)).html(html);
		self.initEditPanel();
		questions = self.parseQuestionString(self.getOption('QuestionString'));
		self.writeSheet();
		self.initQuestionSubmitStatus();
	};
	
	self.initEditPanel = function()
	{
		var is_debug = self.getOption('Debug');
		var qtype_id = 'qType';
		var qtitle_id = 'secDesc';
		var qoption_id = 'oNum';
		var tr_option_id = 'optionTr';
		var tr_require_submit_id = 'submitTr';
		var add_btn_id = 'add_btn'; 
		var qtype_obj = $('#'+escapeJQueryId(qtype_id));
		var add_btn_obj = $('#'+escapeJQueryId(add_btn_id));
		qtype_obj.unbind('change').bind('change',function(){
			var selected_val = $(this).val();
			var answer_option_selection = self.generateAnswerOptionsSelection();
			if(selected_val == QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY){
				answer_option_selection = self.generateAnswerOptionsSelection(1);
			}
			$('#oNum').replaceWith(answer_option_selection);
			if(require_options_qtypes.indexOf(selected_val) != -1){
				$('#'+tr_option_id).show();
			}else{
				$('#'+tr_option_id).hide();
			}
			if(selected_val == '' || require_submit_qtypes.indexOf(selected_val)==-1){
				$('#'+tr_require_submit_id).hide();
			}else{
				$('#'+tr_require_submit_id).show();
			}
		});
		
		add_btn_obj.unbind('click').bind('click',function(){
			var qtitle_obj = $('#'+qtitle_id);
			var qtitle_val = $.trim(qtitle_obj.val());
			var qtype_val = qtype_obj.val();
			var qoption_val = $('#'+qoption_id).val();
			var must_submit = $('#mustSubmitOption').is(':checked')?1:0; 
			
			if(qtitle_val == ''){
				alert(self.getOption('StrPleaseFillInTitle'));
				return;
			}
			
			if(qtype_val == ''){
				alert(self.getOption('StrPleaseSpecifyType'));
				return;
			}
			
			if(require_options_qtypes.indexOf(qtype_val) != -1 && qoption_val == ''){
				alert(self.getOption('StrPleaseSpecifyOptionNum'));
				return;
			}
			
			var q = self.generateQuestionByType(qtype_val,qtitle_val,qoption_val,must_submit);
			questions.push(q);
			self.writeSheet();
		});
		
		$('#AllFieldsReq').unbind('click').bind('click',function(){
			var checked = $(this).is(':checked');
			if(checked){
				self.setOption('AllFieldsReq',1);
			}else{
				self.setOption('AllFieldsReq',0);
			}
			$('input#mustSubmitOption').attr('checked',checked);
			$('.must-submit-checkbox').each(function(i){
				var cb = $(this);
				cb.attr('checked',checked?true:false);
				
				var asterisk_span = $(cb.closest('.question-row').find('.must-submit-asterisk'));
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
				//cb.change();
				questions[i].MustSubmit = checked?1:0;
			});
			if(window.opener && window.opener.document.form1 && window.opener.document.form1['AllFieldsReq']){
				window.opener.document.form1['AllFieldsReq'].checked = checked;
			}
		});

		$('#DisplayQuestionNumber').unbind('click').bind('click',function(){
			var checked = $(this).is(':checked');
			if(checked){
				self.setOption('DisplayQuestionNumber',1);
				$('.question-no').show();
			}else{
				self.setOption('DisplayQuestionNumber',0);
				$('.question-no').hide();
			}
			if(window.opener && window.opener.document.form1 && window.opener.document.form1['DisplayQuestionNumber']){
				window.opener.document.form1['DisplayQuestionNumber'].checked = checked;
			}
		});
		
		$('select#Template').unbind('change').bind('change',function(){
			var selected_template_value = $.trim($(this).val());
			var is_template = selected_template_value != '';
			self.setOption('IsTemplate',is_template);
			questions = self.parseQuestionString(selected_template_value);
			self.writeSheet();
		});
		
		if(is_debug){
			$('#debug_btn').unbind('click').bind('click',function(){
				console.log(questions);
			});
			$('#debug_construct_question_btn').unbind('click').bind('click',function(){
				var qstr = self.constructQuestionString();
				self.setOption('QuestionString',qstr);
				console.log(qstr);
			});
			$('#debug_parse_question_btn').unbind('click').bind('click',function(){
				console.log(self.parseQuestionString(self.getOption('QuestionString')));
			});
		}

        $(document).bind('click', '', function(event) {
			var event_target_obj = event.target;

            // Reply Slip Questions - Question Option - "Required to be answered"
            if(event_target_obj.className != null && event_target_obj.className == 'must-submit-checkbox')
            {
                // uncheck each Question Option - "Required to be answered" > uncheck Basic Settings Option - "All questions are required to be answered"
                var checked = event_target_obj.checked;
                if(!checked)
                {
                    self.setOption('AllFieldsReq', 0);

                    $('input#AllFieldsReq').attr('checked', checked);
                    $('input#mustSubmitOption').attr('checked', checked);

                    if(window.opener && window.opener.document.form1 && window.opener.document.form1['AllFieldsReq']) {
                        window.opener.document.form1['AllFieldsReq'].checked = checked;
                    }
                }
            }
            // Question Settings Option - "Required to be answered"
            else if(event_target_obj.id == 'mustSubmitOption') {
                var checked = $('#AllFieldsReq').is(':checked');
                if(checked){
                    $('#mustSubmitOption').attr('checked', checked);
                }
            }
        });
	};
	
    self.initQuestionSubmitStatus = function()
    {
		var checked = $('#AllFieldsReq').is(':checked');
		if(checked) {
			self.setOption('AllFieldsReq',1);
		} else {
			self.setOption('AllFieldsReq',0);
		}
		$('input#mustSubmitOption').attr('checked', checked);

        if(checked) {
			$('.must-submit-checkbox').each(function(i) {
				var cb = $(this);
				cb.attr('checked', checked ? true : false);

				var asterisk_span = $(cb.closest('.question-row').find('.must-submit-asterisk'));
				if(checked) {
					asterisk_span.show();
				} else {
					asterisk_span.hide();
				}
				questions[i].MustSubmit = checked ? 1 : 0;
			});
        }

		if(window.opener && window.opener.document.form1 && window.opener.document.form1['AllFieldsReq']) {
			window.opener.document.form1['AllFieldsReq'].checked = checked;
		}
    }

	self.renderFillInPanel = function(containerId)
	{
		self.setOption('Mode',FILLIN_ANSWER_MODE);
		
		var is_debug = self.getOption('Debug');
		var str_reply_slip = self.getOption('StrReplySlip');
		var str_payment_item_name = self.getOption('StrPaymentItemName');
		var str_account_balance = self.getOption('StrAccountBalance');
		var str_total = self.getOption('StrTotal');
		var account_balance = self.getOption('AccountBalance');
		var do_not_show_balance = self.getOption('DoNotShowBalance')==1?true:false;
		
		var html = '';
		
		html += '<div id="blockInput" style="background-color:transparent;width:100%; visibility:visible;">\n';
		html += '<form name="answersheet">\n';
		if(is_debug){			
			html += '<input type="button" class="formsubbutton" name="debug_btn" id="debug_btn" value="Debug" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_construct_answer_btn" id="debug_construct_answer_btn" value="Construct Answer" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_parse_answer_btn" id="debug_parse_answer_btn" value="Parse Answer" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
		}
		html +=		'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
		html +=			'<tbody>\n';
		//html +=				'<tr>\n';
		//html +=					'<td align="center">'+str_reply_slip+'</td>\n';
		//html +=				'</tr>\n';
		html +=				'<tr>\n'
		html +=					'<td>\n';
		html +=						'<div id="replySlipDiv" style="width:100%;overflow-x:auto;overflow-y:hidden;">\n';
		html +=							'<table width="100%" border="0" cellpadding="0" cellspacing="5" id="ContentTable">\n';	
		html +=								'<tbody>\n';
		html +=								'</tbody>\n';
		html +=							'</table>\n';
		html +=						'</div>\n';
		html +=					'</td>\n';
		html +=				'</tr>\n';
		html +=			'</tbody>\n';
		html +=		'</table>\n';
		html += 	'<br />\n';
		html += 	'<div style="margin:0px 12px;">\n';
		html += 	'<span class="tabletextremark"><font color="red">#</font> '+str_payment_item_name+'</span>\n';
		html += 	'<p><span class="tabletext">'+str_total+' : $<span id="TotalAmountToPay">'+formatDisplayNumber(0)+'</span></span></p>\n';
		if(!do_not_show_balance){
			html += '<p><span class="tabletext">'+str_account_balance+' : $'+formatDisplayNumber(account_balance)+'</span></p>';
		}
		html += 	'</div>\n';
		html += '</form>\n';
		html += '</div>\n';
		
		$('#'+escapeJQueryId(containerId)).html(html);
		questions = self.parseQuestionString(self.getOption('QuestionString'));
		self.applySubsidySettingsToQuestions();
		self.parseAnswerString(self.getOption('AnswerString'));
		self.writeSheet();
		self.calculateAndDisableSkippedQuestions();
		self.calculateAmountTotal();
		
		if(is_debug){
			$('#debug_btn').unbind('click').bind('click',function(){
				console.log(questions);
			});
			$('#debug_construct_answer_btn').unbind('click').bind('click',function(){
				var astr = self.constructAnswerString();
				self.setOption('AnswerString',astr);
				console.log(astr);
			});
			$('#debug_parse_answer_btn').unbind('click').bind('click',function(){
				self.parseAnswerString(self.getOption('AnswerString'))
				console.log(questions);
			});
		}
	};
	
	self.calculateAmountTotal = function()
	{
		var selected_rbs = $('input[type=radio].answer:checked');
		var selected_cbs = $('input[type=checkbox].answer:checked');
		var total_to_pay = 0;
		for(var i=0;i<selected_rbs.length;i++){
			if($(selected_rbs[i]).attr('disabled')){
				continue;
			}
			var data_amount = $(selected_rbs[i]).attr('data-amount');
			if(data_amount){
				total_to_pay += convertToFloat(data_amount);
			}
		}
		for(var i=0;i<selected_cbs.length;i++){
			if($(selected_cbs[i]).attr('disabled')){
				continue;
			}
			var data_amount = $(selected_cbs[i]).attr('data-amount');
			if(data_amount){
				var data_quantity = $(selected_cbs[i]).attr('data-quantity');
				if(data_quantity){
					total_to_pay += convertToFloat(data_amount) * convertToInteger(data_quantity);
				}else{
					total_to_pay += convertToFloat(data_amount);
				}
			}
		}
		var container = $('#TotalAmountToPay');
		if(container.length > 0){
			container.html(formatDisplayNumber(total_to_pay));
		}
		var external_amount_total_element_id = self.getOption('ExternalAmountTotalFieldID');
		if(external_amount_total_element_id != ''){
			var external_amount_total_element_obj = $('#'+escapeJQueryId(external_amount_total_element_id));
			if(external_amount_total_element_obj.length > 0){
				external_amount_total_element_obj.val(formatDisplayNumber(total_to_pay));
			}
		}
	}
	
	self.calculateAndDisableSkippedQuestions = function()
	{
		skip_question_indices = [];
		
		for(var i=0;i<questions.length;i++){
			if(skip_question_indices.indexOf(i) != -1){
				// skip checking already disabled questions
				continue;
			}
			var q = questions[i];
			switch(q.Type)
			{
				case QTYPE_WHETHER_TO_PAY:
				case QTYPE_TRUE_OR_FALSE:
					if(q.Answer == '0' && q.OptionYesJumpToQuestion != ''){
						var jump_start_index = i + 1;
						var jump_end_index = jump_start_index;
						if(q.OptionYesJumpToQuestion == -1){
							jump_end_index = questions.length;
						}else if(q.OptionYesJumpToQuestion > i){
							jump_end_index = convertToInteger(q.OptionYesJumpToQuestion);
						}
						for(var j=jump_start_index;j<jump_end_index;j++){
							if(skip_question_indices.indexOf(j) == -1){
								skip_question_indices.push(j);
							}
						}
					}
					if(q.Answer == '1' && q.OptionNoJumpToQuestion != ''){
						var jump_start_index = i + 1;
						var jump_end_index = jump_start_index;
						if(q.OptionNoJumpToQuestion == -1){
							jump_end_index = questions.length;
						}else if(q.OptionNoJumpToQuestion > i){
							jump_end_index = convertToInteger(q.OptionNoJumpToQuestion);
						}
						for(var j=jump_start_index;j<jump_end_index;j++){
							if(skip_question_indices.indexOf(j) == -1){
								skip_question_indices.push(j);
							}
						}
					}
					
				break;
				case QTYPE_ONE_PAY_ONE_CAT:
				case QTYPE_ONE_PAY_FEW_CAT:
				case QTYPE_MC_SINGLE:
					if(q.Answer != '' && q.Answer >= 0){
						if(q.Options[q.Answer] && q.Options[q.Answer].JumpToQuestion != ''){
							var jump_start_index = i + 1;
							var jump_end_index = jump_start_index;
							if(q.Options[q.Answer].JumpToQuestion == -1){
								jump_end_index = questions.length;
							}else if(q.Options[q.Answer].JumpToQuestion > i){
								jump_end_index = convertToInteger(q.Options[q.Answer].JumpToQuestion);
							}
							for(var j=jump_start_index;j<jump_end_index;j++){
								if(skip_question_indices.indexOf(j) == -1){
									skip_question_indices.push(j);
								}
							}
						}
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT:
				case QTYPE_MC_MULTIPLE:
					if(q.Answer.length > 0){
						var jump_start_index = i + 1;
						var jump_end_index = jump_start_index;
						for(var j=0;j<q.Answer.length;j++){
							if(q.Answer[j]!='' && q.Options[q.Answer[j]].JumpToQuestion != ''){
								if(q.Options[q.Answer[j]].JumpToQuestion == -1){
									jump_end_index = questions.length;
								}else if(q.Options[q.Answer[j]].JumpToQuestion > i){
									var tmp_jump_end_index = convertToInteger(q.Options[q.Answer[j]].JumpToQuestion);
									if(tmp_jump_end_index > jump_end_index){
										jump_end_index = tmp_jump_end_index;
									}
								}
							}
						}
						for(var j=jump_start_index;j<jump_end_index;j++){
							if(skip_question_indices.indexOf(j) == -1){
								skip_question_indices.push(j);
							}
						}
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					if(q.Answer.length > 0){
						var jump_start_index = i + 1;
						var jump_end_index = jump_start_index;
						for(var j=0;j<q.Answer.length;j++){
							if((q.Answer[j]['Option']+'') != '' && q.Options[q.Answer[j]['Option']].JumpToQuestion != ''){
								if(q.Options[q.Answer[j]['Option']].JumpToQuestion == -1){
									jump_end_index = questions.length;
								}else if(q.Options[q.Answer[j]['Option']].JumpToQuestion > i){
									var tmp_jump_end_index = convertToInteger(q.Options[q.Answer[j]['Option']].JumpToQuestion);
									if(tmp_jump_end_index > jump_end_index){
										jump_end_index = tmp_jump_end_index;
									}
								}
							}
						}
						for(var j=jump_start_index;j<jump_end_index;j++){
							if(skip_question_indices.indexOf(j) == -1){
								skip_question_indices.push(j);
							}
						}
					}
				break;
				case QTYPE_MUST_PAY:
				case QTYPE_TEXT_LONG:
				case QTYPE_TEXT_SHORT:
					if(q.Answer != '' && q.JumpToQuestion != ''){
						var jump_start_index = i + 1;
						var jump_end_index = jump_start_index;
						if(q.JumpToQuestion == -1){
							jump_end_index = questions.length;
						}else if(q.JumpToQuestion > i){
							jump_end_index = convertToInteger(q.JumpToQuestion);
						}
						for(var j=jump_start_index;j<jump_end_index;j++){
							if(skip_question_indices.indexOf(j) == -1){
								skip_question_indices.push(j);
							}
						}
					}
				break;
			}
		}
		
		$('.answer').attr('disabled',false);
		$('.info').hide();
		$('.question-row').removeClass('disabled_q');
		if(skip_question_indices.length > 0)
		{
			skip_question_indices.sort();
			for(var i=0;i<skip_question_indices.length;i++){
				var skip_row = skip_question_indices[i];
				var skip_row_obj = $('#row'+skip_row);
				//if($.mobile){
				//	skip_row_obj.find('.answer').attr('disabled',true).attr('checked',false).checkboxradio('refresh');
				//}else{
				//	skip_row_obj.find('.answer').attr('disabled',true).attr('checked',false);
				//}
				var q = questions[skip_row];
				switch(q.Type)
				{
					case QTYPE_TEXT_LONG: // textarea
					case QTYPE_TEXT_SHORT: // input text
						skip_row_obj.find('.answer').attr('disabled',true);
						$('#row'+skip_row).find('.answer').val('');
					break;
					case QTYPE_MUST_PAY: // radio button
					case QTYPE_WHETHER_TO_PAY: // radio button
					case QTYPE_ONE_PAY_ONE_CAT: // radio button
					case QTYPE_ONE_PAY_FEW_CAT: // radio button
					case QTYPE_FEW_PAY_FEW_CAT: // checkbox
					case QTYPE_TRUE_OR_FALSE: // radio button
					case QTYPE_MC_SINGLE: // radio button
					case QTYPE_MC_MULTIPLE: // checkboxes
					case QTYPE_NOT_APPLICABLE:
						if($.mobile){
							// pass skip_row_obj to disableQuestionInWebview()
							self.disableQuestionInWebview(skip_row_obj);

							// added timeout to prevent JS error
                            //setTimeout(function() {
                            //    skip_row_obj.find('.answer').attr('disabled',true).attr('checked',false).checkboxradio('refresh');
                            //}, 10);
						}else{
							skip_row_obj.find('.answer').attr('disabled',true).attr('checked',false);
						}
					break;
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY: // checkbox
						if($.mobile){
							// pass skip_row_obj to disableQuestionInWebview()
							self.disableQuestionInWebview(skip_row_obj);

                            // added timeout to prevent JS error
                            //setTimeout(function() {
							//	skip_row_obj.find('.answer').attr('disabled',true).attr('checked',false).checkboxradio('refresh');
                            //}, 10);
						}else{
							skip_row_obj.find('.answer').attr('disabled',true).attr('checked',false);
						}
						var answer_checkboxes = skip_row_obj.find('.answer');
						for(var j=0;j<answer_checkboxes.length;j++){
							var answer_checkbox_id = answer_checkboxes[j].id;
							var answer_quantity_container_id = answer_checkbox_id.replace('OptionChecked','AnswerQuantityContainer');
							$('#'+answer_quantity_container_id).hide();
						}
					break;
				}
				if(typeof(questions[skip_row].Answer)=='string'){
					questions[skip_row].Answer = '';
				}else{
					questions[skip_row].Answer = [];
				}
				skip_row_obj.addClass('disabled_q');
				$('#Info'+skip_row).show();
			}
		}
	};
	
	self.applySubsidySettingsToQuestions = function()
	{
		var subsidy_student_id = self.getOption('SubsidyStudentID');
		var subsidy_identity_id = self.getOption('SubsidyIdentityID');
		
		if( (subsidy_student_id && subsidy_student_id != '') || (subsidy_identity_id && subsidy_identity_id != '') )
		{
			for(var i=0;i<questions.length;i++){
				var q = questions[i];
				if(q.Subsidy){
					switch(q.Type)
					{
						case QTYPE_MUST_PAY:
						case QTYPE_WHETHER_TO_PAY:
							if(q.Subsidy[0]['Enable'] == 1){
								var applied = false;
								if(q.Subsidy[0]['SetStudent'] == 1 && subsidy_student_id){
									if(typeof(q.Subsidy[0]['Students'][subsidy_student_id]) != "undefined"
									   && q.Subsidy[0]['Students'][subsidy_student_id] != null 
									   && q.Subsidy[0]['Students'][subsidy_student_id] != ''){
										q.Amount = q.Subsidy[0]['Students'][subsidy_student_id];
										applied = true;
									}
								}
								if(!applied && q.Subsidy[0]['SetIdentity'] == 1 && subsidy_identity_id){
									if(typeof(q.Subsidy[0]['Identities'][subsidy_identity_id]) != "undefined" 
									   && q.Subsidy[0]['Identities'][subsidy_identity_id] != null 
									   && q.Subsidy[0]['Identities'][subsidy_identity_id] != ''){
										q.Amount = q.Subsidy[0]['Identities'][subsidy_identity_id];
										applied = true;
									}
								}
							}
						break;
						case QTYPE_ONE_PAY_ONE_CAT:
						case QTYPE_ONE_PAY_FEW_CAT:
						case QTYPE_FEW_PAY_FEW_CAT:
						case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
							for(var j=0;j<q.Options.length;j++){
								if(q.Subsidy[j]['Enable'] == 1){
									var applied = false;
									if(q.Subsidy[j]['SetStudent'] == 1 && subsidy_student_id){
										if(typeof(q.Subsidy[j]['Students'][subsidy_student_id]) != "undefined" 
										   && q.Subsidy[j]['Students'][subsidy_student_id] != null 
										   && q.Subsidy[j]['Students'][subsidy_student_id] != ''){
											q.Options[j]['Amount'] = q.Subsidy[j]['Students'][subsidy_student_id];
											applied = true;
										}
									}
									if(!applied && q.Subsidy[j]['SetIdentity'] == 1 && subsidy_identity_id){
										if(typeof(q.Subsidy[j]['Identities'][subsidy_identity_id]) != "undefined" 
										   && q.Subsidy[j]['Identities'][subsidy_identity_id] != null 
										   && q.Subsidy[j]['Identities'][subsidy_identity_id] != ''){
											q.Options[j]['Amount'] = q.Subsidy[j]['Identities'][subsidy_identity_id];
											applied = true;
										}
									}
								}
							}
						break;
					}
				}
			}
		}
	};
	
	self.renderPreviewPanel = function(containerId)
	{
		self.setOption('Mode',PREVIEW_MODE);
		
		var is_debug = self.getOption('Debug');
		var str_reply_slip = self.getOption('StrReplySlip');
		var str_payment_item_name = self.getOption('StrPaymentItemName');
		var str_account_balance = self.getOption('StrAccountBalance');
		var str_total = self.getOption('StrTotal');
		var account_balance = self.getOption('AccountBalance');
		var do_not_show_balance = self.getOption('DoNotShowBalance')==1?true:false;
		
		var html = '';
		
		html += '<div id="blockInput" style="background-color:transparent;width:100%; visibility:visible;">\n';
		html += '<form name="answersheet">\n';
		if(is_debug){			
			html += '<input type="button" class="formsubbutton" name="debug_btn" id="debug_btn" value="Debug" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_construct_answer_btn" id="debug_construct_answer_btn" value="Construct Answer" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
			html += '&nbsp;<input type="button" class="formsubbutton" name="debug_parse_answer_btn" id="debug_parse_answer_btn" value="Parse Answer" onMouseOver="this.className=\'formsubbutton\'" onMouseOut="this.className=\'formsubbutton\'" />';
		}
		html +=		'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">';
		html +=			'<tbody>\n';
		html +=				'<tr>\n';
		html +=					'<td align="center">'+str_reply_slip+'</td>\n';
		html +=				'</tr>\n';
		html +=				'<tr>\n'
		html +=					'<td>\n';
		html +=						'<div id="replySlipDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;">\n';
		html +=							'<table width="100%" border="0" cellpadding="0" cellspacing="5" id="ContentTable">\n';	
		html +=								'<tbody>\n';
		html +=								'</tbody>\n';
		html +=							'</table>\n';
		html +=						'</div>\n';
		html +=					'</td>\n';
		html +=				'</tr>\n';
		html +=			'</tbody>\n';
		html +=		'</table>\n';
		html += 	'<br />\n';
		html += 	'<div style="margin:0px 12px;">\n';
		//html += 	'<span class="tabletextremark"><font color="red">#</font> '+str_payment_item_name+'</span>\n';
		//html += 	'<p><span class="tabletext">'+str_total+' : $<span id="TotalAmountToPay">'+formatDisplayNumber(0)+'</span></span></p>\n';
		if(!do_not_show_balance){
			html += '<p><span class="tabletext">'+str_account_balance+' : $'+formatDisplayNumber(account_balance)+'</span></p>';
		}
		html += 	'</div>\n';
		html += '</form>\n';
		html += '</div>\n';
		
		$('#'+escapeJQueryId(containerId)).html(html);
		questions = self.parseQuestionString(self.getOption('QuestionString'));
		self.applySubsidySettingsToQuestions();
		self.parseAnswerString(self.getOption('AnswerString'));
		self.writeSheet();
		self.calculateAndDisableSkippedQuestions();
		self.calculateAmountTotal();
		
		if(is_debug){
			$('#debug_btn').unbind('click').bind('click',function(){
				console.log(questions);
			});
			$('#debug_construct_answer_btn').unbind('click').bind('click',function(){
				var astr = self.constructAnswerString();
				self.setOption('AnswerString',astr);
				console.log(astr);
			});
			$('#debug_parse_answer_btn').unbind('click').bind('click',function(){
				self.parseAnswerString(self.getOption('AnswerString'))
				console.log(questions);
			});
		}
	};
	
	self.writeSheet = function()
	{
		$('#ContentTable').html('');
		for(var i=0;i<questions.length;i++){
			var html = self.generateQuestionEditableHtml(questions[i],i);
			$('#ContentTable').append(html);
			self.bindQuestionData(questions[i],i);
		}
		if(self.getOption('Mode') == EDIT_MODE){
			initDnDTable(function(table, row){
				self.reorderQuestions();
			});
		}
	};
	
	self.reorderQuestions = function()
	{
		var rows = $('.question-row');
		var new_questions = [];
		for(var i=0;i<rows.length;i++){
			var id = rows[i].id;
			var id_number = convertToInteger(id.replace('row',''));
			new_questions.push(questions[id_number]);
		}
		questions = new_questions;
		
		self.writeSheet();
		$('select.jump-question-selection').trigger('change');
	};
	
	self.generateQuestionByType = function(type,title,optionNo,mustSubmit)
	{
		var q = {};
		switch(type)
		{
			case QTYPE_MUST_PAY:
				q = {"Type":type,"Title":title,"Option":"","JumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":"","Subsidy":[{"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}}]};
			break;
			case QTYPE_WHETHER_TO_PAY:
				q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":mustSubmit,"Answer":"","Subsidy":[{"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}}]};
			break;
			case QTYPE_ONE_PAY_ONE_CAT:
				var option_ary = [];
				var subsidy_ary = [];
				for(var i=0;i<optionNo;i++){
					option_ary.push({"Amount":"","Option":"","JumpToQuestion":""});
					subsidy_ary.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
				}
				
				q = {"Type":type,"Title":title,"CategoryID":"","PaymentItemID":"","Options":option_ary,"MustSubmit":mustSubmit,"Answer":"","Subsidy":subsidy_ary};
			break;
			case QTYPE_ONE_PAY_FEW_CAT:
				var option_ary = [];
				var subsidy_ary = [];
				for(var i=0;i<optionNo;i++){
					option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					subsidy_ary.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
				}
				q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":"","Subsidy":subsidy_ary};
			break;
			case QTYPE_FEW_PAY_FEW_CAT:
				var option_ary = [];
				var subsidy_ary = [];
				for(var i=0;i<optionNo;i++){
					option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					subsidy_ary.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
				}
				q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[],"Subsidy":subsidy_ary};
			break;
			case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
				var option_ary = [];
				var subsidy_ary = [];
				for(var i=0;i<optionNo;i++){
					option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":"","MinQuantity":1,"MaxQuantity":100});
					subsidy_ary.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
				}
				q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[],"Subsidy":subsidy_ary};
			break;
			case QTYPE_TEXT_LONG:
				q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
			break;
			case QTYPE_TEXT_SHORT:
				q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
			break;
			case QTYPE_TRUE_OR_FALSE:
				q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
			break;
			case QTYPE_MC_SINGLE:
				var option_ary = [];
				for(var i=0;i<optionNo;i++){
					option_ary.push({"Option":"","JumpToQuestion":""});
				}
				q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
			break;
			case QTYPE_MC_MULTIPLE:
				var option_ary = [];
				for(var i=0;i<optionNo;i++){
					option_ary.push({"Option":""});
				}
				q = {"Type":type,"Title":title,"Options":option_ary,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":[]};
			break;
			case QTYPE_NOT_APPLICABLE:
				q = {"Type":type,"Title":title,"MustSubmit":0,"Answer":""};
			break;
		}
		
		return q;
	};
	
	self.generateSubsidyTableHtml = function(q, que_index, ans_index)
	{
		var str_apply_subsidy_settings = self.getOption('StrApplySubsidySettings');
		var str_set_subsidy_by_identity = self.getOption('StrSetSubsidyByIdentity');
		var str_set_subsidy_by_individual_student = self.getOption('StrSetSubsidyByIndividualStudent');
		var str_subsidy_identity = self.getOption('StrSubsidyIdentity');
		var str_student = self.getOption('StrStudent');
		var str_amount = self.getOption('StrPaymentAmount');
		var str_class = self.getOption('StrClass');
		
		var subsidy_identities = self.getOption('SubsidyIdentityAry');
		var target_students = self.getOption('TargetStudentAry');
		var classname_selection_data = self.getOption('ClassNameSelectionData');
		
		var enabled = q.Subsidy[ans_index]['Enable'] == '1';
		var set_identity = q.Subsidy[ans_index]['SetIdentity'] == '1';
		var set_student = q.Subsidy[ans_index]['SetStudent'] == '1';
		var identities_obj = q.Subsidy[ans_index]['Identities'];
		var students_obj = q.Subsidy[ans_index]['Students'];
		
		var classname_selection = createSelection(classname_selection_data,' id="SubsidyStudentClass_'+que_index+'_'+ans_index+'" name="SubsidyStudentClass_'+que_index+'_'+ans_index+'" ','');
		
		var html = '';
		html += '<table id="SubsidyTable_'+que_index+'_'+ans_index+'" class="common_table_list_v30"'+(enabled?'':' style="display:none;"')+'>\n';
		html += '<thead>\n';
			html += '<tr>\n';
				html += '<th colspan="2"><label><input type="checkbox" id="SetIdentity_'+que_index+'_'+ans_index+'" name="SetIdentity_'+que_index+'_'+ans_index+'" value="1" '+(set_identity?'checked':'')+' />'+str_set_subsidy_by_identity+'</label></th><th colspan="2"><label><input type="checkbox" id="SetStudent_'+que_index+'_'+ans_index+'" name="SetStudent_'+que_index+'_'+ans_index+'" value="1" '+(set_student?'checked':'')+' />'+str_set_subsidy_by_individual_student+'</label><span style="float:right;">'+str_class+': '+classname_selection+'</span></th>\n';
			html += '</tr>\n';
		html += '</thead>\n';
		html += '<tbody>\n';
			html += '<tr>\n';
				html += '<td colspan="2" style="padding:0;">\n';
					html += '<table id="SubsidyIdentityTable_'+que_index+'_'+ans_index+'" class="common_table_list_v30"'+(set_identity?'':' style="display:none;"')+'>\n';
						html += '<tr>\n';
							html += '<th>'+str_subsidy_identity+'</th><th>'+str_amount+'<div class="table_row_tool" style="float:none;">$<input type="number" id="AssignAllIdentityAmount_'+que_index+'_'+ans_index+'" name="AssignAllIdentityAmount_'+que_index+'_'+ans_index+'" value="" min="0" onchange="this.value=Math.max(0,this.value);" /><a class="icon_batch_assign" href="javascript:;" id="AssignAllIdentityAmountBtn_'+que_index+'_'+ans_index+'"></a></div></th>\n';
						html += '</tr>\n';
						for(var i=0;i<subsidy_identities.length;i++){
							var amount = typeof(identities_obj[subsidy_identities[i]['IdentityID']])!="undefined"? identities_obj[subsidy_identities[i]['IdentityID']] : '';
							html += '<tr>\n';
								html += '<td>'+escapeHtml(subsidy_identities[i]['IdentityName'])+'</td><td>$<input type="number" class="identity-amount" id="IdentityAmount_'+que_index+'_'+ans_index+'_'+subsidy_identities[i]['IdentityID']+'" name="IdentityAmount_'+que_index+'_'+ans_index+'_'+subsidy_identities[i]['IdentityID']+'" value="'+escapeDoubleQuotes(amount)+'" data-identityid="'+escapeDoubleQuotes(subsidy_identities[i]['IdentityID'])+'" data-queindex="'+que_index+'" data-ansindex="'+ans_index+'" data-rowindex="'+i+'" min="0" /><span class="table_row_tool" style="display:inline-block;float:none;vertical-align:bottom;"><a class="icon_batch_assign" style="transform:rotate(-90deg);" href="javascript:;" id="AssignIdentityAmountToStudentBtn_'+que_index+'_'+ans_index+'_'+subsidy_identities[i]['IdentityID']+'"></a></span></td>\n';
							html += '</tr>\n';
						}
					html += '</table>\n';
				html += '</td>\n';
				html += '<td colspan="2" style="padding:0;">\n';
					html += '<table id="SubsidyStudentTable_'+que_index+'_'+ans_index+'" class="common_table_list_v30"'+(set_student?'':' style="display:none;"')+'>\n';
						html += '<tr>\n';
							html += '<th>'+str_student+'</th><th>'+str_amount+'<div class="table_row_tool" style="float:none;">$<input type="number" id="AssignAllStudentAmount_'+que_index+'_'+ans_index+'" name="AssignAllStudentAmount_'+que_index+'_'+ans_index+'" value="" min="0" onchange="this.value=Math.max(0,this.value);" /><a class="icon_batch_assign" href="javascript:;" id="AssignAllStudentAmountBtn_'+que_index+'_'+ans_index+'"></a></div></th>\n';
						html += '</tr>\n';
						for(var i=0;i<target_students.length;i++){
							var amount = typeof(students_obj[target_students[i]['UserID']])!="undefined"? students_obj[target_students[i]['UserID']] : '';
							html += '<tr class="row-class'+(target_students[i]['ClassName']!=''? ' class-'+escapeJQueryId(escapeDoubleQuotes(target_students[i]['ClassName'])) : '')+'">\n';
								html += '<td>'+escapeHtml(target_students[i]['StudentName']+(target_students[i]['IdentityName']!=''?'('+target_students[i]['IdentityName']+')':''))+'</td><td>$<input type="number" class="student-amount'+(target_students[i]['IdentityName']==''?' no-subsidy':'')+'" id="StudentAmount_'+que_index+'_'+ans_index+'_'+target_students[i]['UserID']+'" name="StudentAmount_'+que_index+'_'+ans_index+'_'+target_students[i]['UserID']+'" value="'+escapeDoubleQuotes(amount)+'" data-userid="'+escapeDoubleQuotes(target_students[i]['UserID'])+'" data-queindex="'+que_index+'" data-ansindex="'+ans_index+'" data-rowindex="'+i+'" min="0" /></td>\n';
							html += '</tr>\n';
						}
					html += '</table>\n';
				html += '</td>\n';
			html += '</tr>\n';
			
		html += '</tbody>\n';
		html += '</table>\n';
		
		return html;
	};
	
	self.generateSubsidyContainerHtml = function(q, que_index, ans_index)
	{
		var str_apply_subsidy_settings = self.getOption('StrApplySubsidySettings');
		var str_set_subsidy_by_identity = self.getOption('StrSetSubsidyByIdentity');
		var str_set_subsidy_by_individual_student = self.getOption('StrSetSubsidyByIndividualStudent');
		var str_subsidy_identity = self.getOption('StrSubsidyIdentity');
		var str_student = self.getOption('StrStudent');
		var str_amount = self.getOption('StrPaymentAmount');
		var str_class = self.getOption('StrClass');
		
		var subsidy_identities = self.getOption('SubsidyIdentityAry');
		var target_students = self.getOption('TargetStudentAry');
		var classname_selection_data = self.getOption('ClassNameSelectionData');
		
		var enabled = q.Subsidy[ans_index]['Enable'] == '1';
		var set_identity = q.Subsidy[ans_index]['SetIdentity'] == '1';
		var set_student = q.Subsidy[ans_index]['SetStudent'] == '1';
		var identities_obj = q.Subsidy[ans_index]['Identities'];
		var students_obj = q.Subsidy[ans_index]['Students'];
		
		var html = '';
		html += '<div style="width:100%;max-height:480px;overflow-y:auto;">\n';
		html += '<div id="SubsidyError_'+que_index+'_'+ans_index+'" class="error tabletextrequire" style="display:none;"></div>\n';
		html += '<label><input type="checkbox" id="EnableSubsidy_'+que_index+'_'+ans_index+'" name="EnableSubsidy_'+que_index+'_'+ans_index+'" value="1" '+(enabled?'checked':'')+' />'+str_apply_subsidy_settings+'</label>\n';
		html += '<div id="SubsidyTableContainer_'+que_index+'_'+ans_index+'">\n';
		if(q.Subsidy[ans_index]['Enable'] == 1){
			html += self.generateSubsidyTableHtml(q, que_index, ans_index);
		}
		html += '</div>\n';
		html += '</div>\n';
		
		return html;
	};
	
	/*
	self.generateSubsidyHtml = function(q, que_index, ans_index)
	{
		var str_apply_subsidy_settings = self.getOption('StrApplySubsidySettings');
		var str_set_subsidy_by_identity = self.getOption('StrSetSubsidyByIdentity');
		var str_set_subsidy_by_individual_student = self.getOption('StrSetSubsidyByIndividualStudent');
		var str_subsidy_identity = self.getOption('StrSubsidyIdentity');
		var str_student = self.getOption('StrStudent');
		var str_amount = self.getOption('StrPaymentAmount');
		var str_class = self.getOption('StrClass');
		
		var subsidy_identities = self.getOption('SubsidyIdentityAry');
		var target_students = self.getOption('TargetStudentAry');
		var classname_selection_data = self.getOption('ClassNameSelectionData');
		
		var enabled = q.Subsidy[ans_index]['Enable'] == '1';
		var set_identity = q.Subsidy[ans_index]['SetIdentity'] == '1';
		var set_student = q.Subsidy[ans_index]['SetStudent'] == '1';
		var identities_obj = q.Subsidy[ans_index]['Identities'];
		var students_obj = q.Subsidy[ans_index]['Students'];
		
		var classname_selection = createSelection(classname_selection_data,' id="SubsidyStudentClass_'+que_index+'_'+ans_index+'" name="SubsidyStudentClass_'+que_index+'_'+ans_index+'" ','');
		
		var html = '';
		html += '<div style="width:100%;max-height:480px;overflow-y:auto;">\n';
		html += '<div id="SubsidyError_'+que_index+'_'+ans_index+'" class="error tabletextrequire" style="display:none;"></div>\n';
		html += '<label><input type="checkbox" id="EnableSubsidy_'+que_index+'_'+ans_index+'" name="EnableSubsidy_'+que_index+'_'+ans_index+'" value="1" '+(enabled?'checked':'')+' />'+str_apply_subsidy_settings+'</label>\n';
		html += '<table id="SubsidyTable_'+que_index+'_'+ans_index+'" class="common_table_list_v30"'+(enabled?'':' style="display:none;"')+'>\n';
		html += '<thead>\n';
			html += '<tr>\n';
				html += '<th colspan="2"><label><input type="checkbox" id="SetIdentity_'+que_index+'_'+ans_index+'" name="SetIdentity_'+que_index+'_'+ans_index+'" value="1" '+(set_identity?'checked':'')+' />'+str_set_subsidy_by_identity+'</label></th><th colspan="2"><label><input type="checkbox" id="SetStudent_'+que_index+'_'+ans_index+'" name="SetStudent_'+que_index+'_'+ans_index+'" value="1" '+(set_student?'checked':'')+' />'+str_set_subsidy_by_individual_student+'</label><span style="float:right;">'+str_class+': '+classname_selection+'</span></th>\n';
			html += '</tr>\n';
		html += '</thead>\n';
		html += '<tbody>\n';
			html += '<tr>\n';
				html += '<td colspan="2" style="padding:0;">\n';
					html += '<table id="SubsidyIdentityTable_'+que_index+'_'+ans_index+'" class="common_table_list_v30"'+(set_identity?'':' style="display:none;"')+'>\n';
						html += '<tr>\n';
							html += '<th>'+str_subsidy_identity+'</th><th>'+str_amount+'<div class="table_row_tool" style="float:none;">$<input type="number" id="AssignAllIdentityAmount_'+que_index+'_'+ans_index+'" name="AssignAllIdentityAmount_'+que_index+'_'+ans_index+'" value="" min="0" onchange="this.value=Math.max(0,this.value);" /><a class="icon_batch_assign" href="javascript:;" id="AssignAllIdentityAmountBtn_'+que_index+'_'+ans_index+'"></a></div></th>\n';
						html += '</tr>\n';
						for(var i=0;i<subsidy_identities.length;i++){
							var amount = typeof(identities_obj[subsidy_identities[i]['IdentityID']])!="undefined"? identities_obj[subsidy_identities[i]['IdentityID']] : '';
							html += '<tr>\n';
								html += '<td>'+escapeHtml(subsidy_identities[i]['IdentityName'])+'</td><td>$<input type="number" class="identity-amount" id="IdentityAmount_'+que_index+'_'+ans_index+'_'+subsidy_identities[i]['IdentityID']+'" name="IdentityAmount_'+que_index+'_'+ans_index+'_'+subsidy_identities[i]['IdentityID']+'" value="'+escapeDoubleQuotes(amount)+'" data-identityid="'+escapeDoubleQuotes(subsidy_identities[i]['IdentityID'])+'" data-queindex="'+que_index+'" data-ansindex="'+ans_index+'" data-rowindex="'+i+'" min="0" /><span class="table_row_tool" style="display:inline-block;float:none;vertical-align:bottom;"><a class="icon_batch_assign" style="transform:rotate(-90deg);" href="javascript:;" id="AssignIdentityAmountToStudentBtn_'+que_index+'_'+ans_index+'_'+subsidy_identities[i]['IdentityID']+'"></a></span></td>\n';
							html += '</tr>\n';
						}
					html += '</table>\n';
				html += '</td>\n';
				html += '<td colspan="2" style="padding:0;">\n';
					html += '<table id="SubsidyStudentTable_'+que_index+'_'+ans_index+'" class="common_table_list_v30"'+(set_student?'':' style="display:none;"')+'>\n';
						html += '<tr>\n';
							html += '<th>'+str_student+'</th><th>'+str_amount+'<div class="table_row_tool" style="float:none;">$<input type="number" id="AssignAllStudentAmount_'+que_index+'_'+ans_index+'" name="AssignAllStudentAmount_'+que_index+'_'+ans_index+'" value="" min="0" onchange="this.value=Math.max(0,this.value);" /><a class="icon_batch_assign" href="javascript:;" id="AssignAllStudentAmountBtn_'+que_index+'_'+ans_index+'"></a></div></th>\n';
						html += '</tr>\n';
						for(var i=0;i<target_students.length;i++){
							var amount = typeof(students_obj[target_students[i]['UserID']])!="undefined"? students_obj[target_students[i]['UserID']] : '';
							html += '<tr class="row-class'+(target_students[i]['ClassName']!=''? ' class-'+escapeJQueryId(escapeDoubleQuotes(target_students[i]['ClassName'])) : '')+'">\n';
								html += '<td>'+escapeHtml(target_students[i]['StudentName']+(target_students[i]['IdentityName']!=''?'('+target_students[i]['IdentityName']+')':''))+'</td><td>$<input type="number" class="student-amount'+(target_students[i]['IdentityName']==''?' no-subsidy':'')+'" id="StudentAmount_'+que_index+'_'+ans_index+'_'+target_students[i]['UserID']+'" name="StudentAmount_'+que_index+'_'+ans_index+'_'+target_students[i]['UserID']+'" value="'+escapeDoubleQuotes(amount)+'" data-userid="'+escapeDoubleQuotes(target_students[i]['UserID'])+'" data-queindex="'+que_index+'" data-ansindex="'+ans_index+'" data-rowindex="'+i+'" min="0" /></td>\n';
							html += '</tr>\n';
						}
					html += '</table>\n';
				html += '</td>\n';
			html += '</tr>\n';
			
		html += '</tbody>\n';
		html += '</table>\n';
		html += '</div>\n';
		
		return html;
	};
	*/
	self.bindSubsidyData = function(q, que_index, ans_index)
	{
		var index = que_index;
		var i = ans_index;
		var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
		
		$('#SetIdentity_'+index+'_'+i).data('index',i);
		$('#SetIdentity_'+index+'_'+i).unbind('click').bind('click',function(){
			var checked = $(this).is(':checked');
			var ans_index = $(this).data('index');
			q.Subsidy[ans_index]['SetIdentity'] = checked?'1':'0';
			if(checked){
				$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
			}else{
				$('#SubsidyIdentityTable_'+index+'_'+ans_index).hide();
			}
		});
		
		$('#SetStudent_'+index+'_'+i).data('index',i);
		$('#SetStudent_'+index+'_'+i).unbind('click').bind('click',function(){
			var checked = $(this).is(':checked');
			var ans_index = $(this).data('index');
			q.Subsidy[ans_index]['SetStudent'] = checked?'1':'0';
			if(checked){
				$('#SubsidyStudentTable_'+index+'_'+ans_index).show();
			}else{
				$('#SubsidyStudentTable_'+index+'_'+ans_index).hide();
			}
		});
		
		$('#SubsidyIdentityTable_'+index+'_'+i+' input.identity-amount').unbind('change').bind('change',function(){
			var ans_index = $(this).attr('id').split('_')[2];
			var value = Math.max(0, formatDisplayNumber($(this).val()));
			$(this).val(value);
			var identity_id = $(this).attr('data-identityid');
			q.Subsidy[ans_index]['Identities'][identity_id] = value;
			
			//var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
			//for(var j=0;j<subsidy_identity_ary.length;j++){
			//	if(subsidy_identity_ary[j]['IdentityID'] == identity_id){
			//		var student_ids = subsidy_identity_ary[j]['StudentIDs'];
			//		for(var k=0;k<student_ids.length;k++){
			//			var tmp_obj = $('#StudentAmount_'+index+'_'+ans_index+'_'+student_ids[k]);
			//			if(tmp_obj && tmp_obj.val() == ''){
			//				tmp_obj.val(value);
			//				tmp_obj.change();
			//			}
			//		}
			//		break;
			//	}
			//}
			
		});
		
		$('#SubsidyStudentTable_'+index+'_'+i+' input.student-amount').unbind('change').bind('change',function(){
			var ans_index = $(this).attr('id').split('_')[2];
			var value = Math.max(0, formatDisplayNumber($(this).val()));
			$(this).val(value);
			var student_id = $(this).attr('data-userid');
			q.Subsidy[ans_index]['Students'][student_id] = value;
		});
		
		$('#AssignAllIdentityAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
			var ans_index = $(this).attr('id').split('_')[2];
			var amount = Math.max(0, formatDisplayNumber($('#AssignAllIdentityAmount_'+index+'_'+ans_index).val()));
			$('#AssignAllIdentityAmount_'+index+'_'+ans_index).val(amount);
			$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').val(amount);
			$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').change();
		});
		
		$('#AssignAllStudentAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
			var ans_index = $(this).attr('id').split('_')[2];
			var amount = Math.max(0, formatDisplayNumber($('#AssignAllStudentAmount_'+index+'_'+ans_index).val()));
			$('#AssignAllStudentAmount_'+index+'_'+ans_index).val(amount);
			$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').val(amount);
			$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').change();
		});
		
		for(var j=0;j<subsidy_identity_ary.length;j++){
			var identity_id = subsidy_identity_ary[j]['IdentityID'];
			$('#AssignIdentityAmountToStudentBtn_'+index+'_'+i+'_'+identity_id).unbind('click').bind('click',function(){
				var target_student_ary = self.getOption('TargetStudentAry');
				var id_parts = $(this).attr('id').split('_');
				var que_index = id_parts[1];
				var ans_index = id_parts[2];
				var identity_id = id_parts[3];
				var amount = Math.max(0, formatDisplayNumber($('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val()));
				$('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val(amount);
				for(var k=0;k<target_student_ary.length;k++){
					if(target_student_ary[k]['IdentityID'] == identity_id){
						var tmp_obj = $('input#StudentAmount_'+que_index+'_'+ans_index+'_'+target_student_ary[k]['UserID']);
						if(tmp_obj){
							tmp_obj.val(amount).change();
						}
					}
				}
			});
		}
		
		$('#SubsidyStudentClass_'+index+'_'+i).unbind('change').bind('change',function(){
			var ans_index = $(this).attr('id').split('_')[2];
			var selected_value = $(this).val();
			if(selected_value == ''){
				$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').show();
			}else{
				$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').hide();
				$('#SubsidyStudentTable_'+index+'_'+ans_index+' .class-'+escapeJQueryId(selected_value)).show();
			}
		});
	};
	
	// render html for each question type
	self.generateMustPayQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_payment_title = self.getOption('StrPaymentTitle');
			var str_payment_category = self.getOption('StrPaymentCategory');
			var str_payment_amount = self.getOption('StrPaymentAmount');
			var str_payment_description = self.getOption('StrPaymentDescription');
			var str_must_pay = self.getOption('StrMustPay');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var option = q.Option;
			var category_id = q.CategoryID;
			var payment_item_id = q.PaymentItemID;
			var amount = q.Amount;
			var must_submit = q.MustSubmit == 1;
			var jump_to_question = q.JumpToQuestion;
			
			var category_selection_html = self.generatePaymentCategorySelection('CategoryID'+index,'CategoryID'+index,category_id);
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			var go_to_question_selection = self.generateGoToQuestionSelection('JumpToQuestion'+index,'JumpToQuestion'+index,jump_to_question,jump_start_index,jump_end_index);	
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table>\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td><u>'+str_payment_title+'</u></td>\n';
			html +=										'<td><u>'+str_payment_category+'</u></td>\n';
			html +=										'<td><u>'+str_payment_amount+'</u></td>\n';
			html +=									'</tr>\n';
			html +=									'<tr>\n';
			html +=										'<td><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea></td>\n';
			html +=										'<td>'+category_selection_html+'</td>\n';
			html +=										'<td>$<input type="number" id="Amount'+index+'" name="Amount'+index+'" value="'+escapeDoubleQuotes(amount)+'" min="0" /></td>\n';
			html +=									'</tr>\n';
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<u>'+str_payment_description+'</u> : <br>\n';
			html +=							'<input type="radio" value="0" id="OptionChecked'+index+'" name="OptionChecked'+index+'" checked />\n';
			html +=							'<input type="text" value="'+escapeDoubleQuotes(option)+'" id="Option'+index+'" name="Option'+index+'" size="25" class="tabletext" /> ('+str_must_pay+')\n';
			html += 						self.generateSubsidyContainerHtml(q,index,0);
			html +=						'</td>\n';
			html += 					'<td align="right" valign="top">\n';
			html += 						go_to_question_selection+'\n';					
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+' ($'+formatDisplayNumber(q.Amount)+')';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							(ans == ''? '' : escapeHtml(q.Option))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			var jump_to_question_msg = '';
			//if(q.JumpToQuestion == '-1'){
			//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(q.JumpToQuestion > 0){
			//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(q.JumpToQuestion)+1) +')</span>';
			//}
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<span class="tabletextrequire">#</span>'+escapeHtml(q.Title)+' ($'+formatDisplayNumber(q.Amount)+')';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<label for="OptionChecked'+index+'"><input type="radio" class="answer" value="0" id="OptionChecked'+index+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(q.Amount)+'" '+(ans == '0'?'checked':'')+' />\n';
			html +=							''+escapeHtml(q.Option)+jump_to_question_msg+'</label>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateWhetherToPayQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_payment_title = self.getOption('StrPaymentTitle');
			var str_payment_category = self.getOption('StrPaymentCategory');
			var str_payment_amount = self.getOption('StrPaymentAmount');
			var str_payment_description = self.getOption('StrPaymentDescription');
			var str_pay = self.getOption('StrPaymentPay');
			var str_not_paying = self.getOption('StrPaymentNotPaying');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var desc = q.Description;
			var category_id = q.CategoryID;
			var payment_item_id = q.PaymentItemID;
			var amount = q.Amount;
			var option_yes = q.OptionYes;
			var option_no = q.OptionNo;
			var option_yes_jump_to_question = q.OptionYesJumpToQuestion;
			var option_no_jump_to_question = q.OptionNoJumpToQuestion;
			var must_submit = q.MustSubmit == 1;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			var category_selection_html = self.generatePaymentCategorySelection('CategoryID'+index,'CategoryID'+index,category_id);
			var option_yes_go_to_question_selection = self.generateGoToQuestionSelection('OptionYesJumpToQuestion'+index,'OptionYesJumpToQuestion'+index,option_yes_jump_to_question,jump_start_index,jump_end_index);
			var option_no_go_to_question_selection = self.generateGoToQuestionSelection('OptionNoJumpToQuestion'+index,'OptionNoJumpToQuestion'+index,option_no_jump_to_question,jump_start_index,jump_end_index);		
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="4" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table>\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td><u>'+str_payment_title+'</u></td>\n';
			html +=										'<td><u>'+str_payment_category+'</u></td>\n';
			html +=										'<td><u>'+str_payment_amount+'</u></td>\n';
			html +=									'</tr>\n';
			html +=									'<tr>\n';
			html +=										'<td><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea></td>\n';
			html +=										'<td>'+category_selection_html+'</td>\n';
			html +=										'<td>$<input type="number" id="Amount'+index+'" name="Amount'+index+'" value="'+escapeDoubleQuotes(amount)+'" min="0" /></td>\n';
			html +=									'</tr>\n';
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="20%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<u>'+str_payment_description+'</u> : \n';
			html +=						'</td>\n';
			html +=						'<td>&nbsp;</td>\n';
			html +=						'<td>&nbsp;</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="20%" class="tabletext" bgcolor="#EFEFEF" valign="top">\n';
			html +=							'<input type="radio" value="0" id="OptionYesChecked'+index+'" name="OptionChecked'+index+'" />\n';
			html +=							'<input type="text" value="'+escapeDoubleQuotes(option_yes)+'" id="OptionYes'+index+'" name="OptionYes'+index+'" size="25" class="tabletext" /> ('+str_pay+')<br />\n';
			html +=						'</td>\n';
			html +=						'<td class="tabletext" bgcolor="#EFEFEF" valign="top">\n';
			html += 						self.generateSubsidyContainerHtml(q,index,0);
			html +=						'</td>\n';
			html += 					'<td align="right" valign="top">\n';
			html += 						option_yes_go_to_question_selection+'<br />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="20%" class="tabletext" bgcolor="#EFEFEF" valign="top">\n';
			html +=							'<input type="radio" value="1" id="OptionNoChecked'+index+'" name="OptionChecked'+index+'" />\n';
			html +=							'<input type="text" value="'+escapeDoubleQuotes(option_no)+'" id="OptionNo'+index+'" name="OptionNo'+index+'" size="25" class="tabletext" /> ('+str_not_paying+')\n';
			html +=						'</td>\n';
			html +=						'<td class="tabletext" bgcolor="#EFEFEF">&nbsp;</td>\n';
			html += 					'<td align="right" valign="top">\n';
			html += 						option_no_go_to_question_selection;						
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="3" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+' ($'+formatDisplayNumber(q.Amount)+')\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			if(ans == '0'){
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			//html +=							'<input type="radio" class="answer" value="0" id="OptionYesChecked'+index+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(q.Amount)+'" '+(ans=='0'?'checked':'')+'/>\n';
			html +=							'<p>'+escapeHtml(q.OptionYes)+'</p>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			}
			if(ans == '1'){
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			//html +=							'<input type="radio" class="answer" value="1" id="OptionNoChecked'+index+'" name="OptionChecked'+index+'" data-amount="0" '+(ans=='1'?'checked':'')+'/>\n';
			html +=							'<p>'+escapeHtml(q.OptionNo)+'</p>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
			
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var option_yes_jump_to_question = q.OptionYesJumpToQuestion;
			var option_no_jump_to_question = q.OptionNoJumpToQuestion;
			var must_submit = q.MustSubmit == '1';
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var option_yes_jump_to_question_msg = '';
			var option_no_jump_to_question_msg = '';
			
			//if(option_yes_jump_to_question == '-1'){
			//	option_yes_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(option_yes_jump_to_question > 0){
			//	option_yes_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+ str_go_to_question + (convertToInteger(option_yes_jump_to_question)+1) +')</span>';
			//}
			//if(option_no_jump_to_question == '-1'){
			//	option_no_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(option_no_jump_to_question > 0){
			//	option_no_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+ str_go_to_question + (convertToInteger(option_no_jump_to_question)+1) +')</span>';
			//}
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="3" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<span class="tabletextrequire">#</span>'+escapeHtml(q.Title)+' ($'+formatDisplayNumber(q.Amount)+')\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<label for="OptionYesChecked'+index+'"><input type="radio" class="answer" value="0" id="OptionYesChecked'+index+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(q.Amount)+'" '+(ans=='0'?'checked':'')+'/>\n';
			html +=							''+escapeHtml(q.OptionYes)+option_yes_jump_to_question_msg+'</label>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<label for="OptionNoChecked'+index+'"><input type="radio" class="answer" value="1" id="OptionNoChecked'+index+'" name="OptionChecked'+index+'" data-amount="0" '+(ans=='1'?'checked':'')+'/>\n';
			html +=							''+escapeHtml(q.OptionNo)+option_no_jump_to_question_msg+'</label>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateOnePayOneCatQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_payment_title = self.getOption('StrPaymentTitle');
			var str_payment_category = self.getOption('StrPaymentCategory');
			var str_payment_amount = self.getOption('StrPaymentAmount');
			var str_payment_description = self.getOption('StrPaymentDescription');
			var str_amount_description = self.getOption('StrAmountDescription');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var desc = q.Description;
			var category_id = q.CategoryID;
			var payment_item_id = q.PaymentItemID;
			var amount = q.Amount;
			var must_submit = q.MustSubmit == 1;
			var options = q.Options;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			var category_selection_html = self.generatePaymentCategorySelection('CategoryID'+index,'CategoryID'+index,category_id);
			
			var br = '';
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(options.length+2)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table>\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td><u>'+str_payment_title+'</u></td>\n';
			html +=										'<td><u>'+str_payment_category+'</u></td>\n';
			html +=									'</tr>\n';
			html +=									'<tr>\n';
			html +=										'<td><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea></td>\n';
			html +=										'<td>'+category_selection_html+'</td>\n';
			html +=									'</tr>\n';
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top" colspan="2">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<u>'+str_payment_amount+'</u> : <br>\n';
			html +=						'</td>\n';
			html +=						'<td class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<u>'+str_amount_description+'</u> : <br>\n';
			html +=						'</td>\n';
			html +=						'<td>&nbsp;</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++){
				html +=				'<tr>\n';
				html +=					'<td width="20%" class="tabletext" bgcolor="#EFEFEF" valign="top">\n';
				html +=						'<input type="radio" value="'+i+'" id="Option'+index+'_'+i+'" name="Option'+index+'" />\n';
				html +=						'$<input type="number" value="'+escapeDoubleQuotes(options[i].Amount)+'" id="Amount'+index+'_'+i+'" name="Amount'+index+'_'+i+'" size="10" class="tabletext" style="width:80%;" min="0" />\n';
				html +=					'</td>\n';
				html +=					'<td width="20%" class="tabletext" bgcolor="#EFEFEF" valign="top">\n';
				html +=						'<input type="text" value="'+escapeDoubleQuotes(options[i].Option)+'" id="Description'+index+'_'+i+'" name="Description'+index+'_'+i+'" size="25" class="tabletext" style="width:100%;" />\n';
				html +=					'</td>\n';
				html +=					'<td width="50%" class="tabletext" bgcolor="#EFEFEF" valign="top">\n';
				html += 					self.generateSubsidyContainerHtml(q,index,i);
				html +=					'</td>\n';
				html += 				'<td align="right" valign="top">\n';
				html += 					self.generateGoToQuestionSelection('JumpToQuestion'+index+'_'+i,'JumpToQuestion'+index+'_'+i,options[i].JumpToQuestion,jump_start_index,jump_end_index)+'\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				if(ans==convertToString(i))
				{
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				//html +=						'<input type="radio" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(options[i].Amount)+'" '+(ans==convertToString(i)?'checked':'')+'/>\n';
				html +=						'<p>$'+formatDisplayNumber(options[i].Amount)+ ' ' + escapeHtml(options[i].Option) +'</p>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
				}
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
			
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(q.Options.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<span class="tabletextrequire">#</span>'+escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				var jump_to_question_msg = '';
				//if(options[i].JumpToQuestion == '-1'){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
				//}else if(options[i].JumpToQuestion > 0){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(options[i].JumpToQuestion) + 1) +')</span>';
				//}
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				html +=						'<label for="OptionChecked'+index+'_'+i+'"><input type="radio" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(options[i].Amount)+'" '+(ans==convertToString(i)?'checked':'')+'/>\n';
				html +=						'$'+formatDisplayNumber(options[i].Amount)+' '+escapeHtml(options[i].Option)+jump_to_question_msg+'</label>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateOnePayFewCatQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_payment_title = self.getOption('StrPaymentTitle');
			var str_payment_category = self.getOption('StrPaymentCategory');
			var str_payment_amount = self.getOption('StrPaymentAmount');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var must_submit = q.MustSubmit == 1;
			var options = q.Options;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit==1?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<u>'+str_topic_title+'</u><br /><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table width="100%">\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td width="1%">&nbsp;</td>';
			html +=										'<td width="15%"><u>'+str_payment_title+'</u></td>\n';
			html +=										'<td width="15%"><u>'+str_payment_category+'</u></td>\n';
			html +=										'<td width="15%"><u>'+str_payment_amount+'</u></td>\n';
			html +=										'<td width="44%">&nbsp;</td>\n';
			html +=										'<td width="10%">&nbsp;</td>';
			html +=									'</tr>\n';
			for(var i=0;i<options.length;i++){
				var category_selection_html = self.generatePaymentCategorySelection('CategoryID'+index+'_'+i,'CategoryID'+index+'_'+i,options[i].CategoryID);
				html +=								'<tr>\n';
				html += 								'<td valign="top"><input type="radio" value="0" name="OptionChecked'+index+'" id="OptionChecked'+index+'_'+i+'"></td>';
				html +=									'<td valign="top"><input type="text" id="Option'+index+'_'+i+'" name="Option'+index+'_'+i+'" value="'+escapeDoubleQuotes(options[i].Option)+'" size="25" class="tabletext" /></td>\n';
				html +=									'<td valign="top">'+category_selection_html+'</td>\n';
				html +=									'<td valign="top">$<input type="number" value="'+escapeDoubleQuotes(options[i].Amount)+'" id="Amount'+index+'_'+i+'" name="Amount'+index+'_'+i+'" size="25" class="tabletext" min="0" /></td>\n';
				html += 								'<td valign="top">'+self.generateSubsidyContainerHtml(q,index,i)+'</td>\n';
				html+= 									'<td valign="top" align="right">'+self.generateGoToQuestionSelection('JumpToQuestion'+index+'_'+i,'JumpToQuestion'+index+'_'+i,options[i].JumpToQuestion,jump_start_index,jump_end_index)+'</td>\n';
				html +=								'</tr>\n';
			}
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				if(ans==convertToString(i))
				{
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				//html +=						'<input type="radio" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(options[i].Amount)+'" '+(ans==convertToString(i)?'checked':'')+'/>\n';
				html +=						'<p>'+escapeHtml(options[i].Option)+' ($'+formatDisplayNumber(options[i].Amount)+')</p>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
				}
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(q.Options.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				var jump_to_question_msg = '';
				//if(options[i].JumpToQuestion == '-1'){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
				//}else if(options[i].JumpToQuestion > 0){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(options[i].JumpToQuestion)+1) +')</span>';
				//}
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				html +=						'<label for="OptionChecked'+index+'_'+i+'"><input type="radio" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" data-amount="'+formatDisplayNumber(options[i].Amount)+'" '+(ans==convertToString(i)?'checked':'')+'/>\n';
				html +=						'<span class="tabletextrequire">#</span>'+escapeHtml(options[i].Option)+' ($'+formatDisplayNumber(options[i].Amount)+jump_to_question_msg+')</label>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateFewPayFewCatQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_payment_title = self.getOption('StrPaymentTitle');
			var str_payment_category = self.getOption('StrPaymentCategory');
			var str_payment_amount = self.getOption('StrPaymentAmount');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var must_submit = q.MustSubmit == 1;
			var options = q.Options;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<u>'+str_topic_title+'</u><br /><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table width="100%">\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td width="1%">&nbsp;</td>';
			html +=										'<td width="15%"><u>'+str_payment_title+'</u></td>\n';
			html +=										'<td width="15%"><u>'+str_payment_category+'</u></td>\n';
			html +=										'<td width="15%"><u>'+str_payment_amount+'</u></td>\n';
			html +=										'<td width="44%">&nbsp;</td>';
			html +=										'<td width="10%">&nbsp;</td>';
			html +=									'</tr>\n';
			for(var i=0;i<options.length;i++){
				var category_selection_html = self.generatePaymentCategorySelection('CategoryID'+index+'_'+i,'CategoryID'+index+'_'+i,options[i].CategoryID);
				html +=								'<tr>\n';
				html += 								'<td valign="top"><input type="checkbox" value="0" name="OptionChecked'+index+'[]" id="OptionChecked'+index+'_'+i+'"></td>';
				html +=									'<td valign="top"><input type="text" id="Option'+index+'_'+i+'" name="Option'+index+'_'+i+'" value="'+escapeDoubleQuotes(options[i].Option)+'" size="25" class="tabletext" /></td>\n';
				html +=									'<td valign="top">'+category_selection_html+'</td>\n';
				html +=									'<td valign="top">$<input type="number" value="'+escapeDoubleQuotes(options[i].Amount)+'" id="Amount'+index+'_'+i+'" name="Amount'+index+'_'+i+'" size="25" class="tabletext" min="0" /></td>\n';
				html += 								'<td valign="top">'+self.generateSubsidyContainerHtml(q,index,i)+'</td>';
				html += 								'<td valign="top" align="right">'+self.generateGoToQuestionSelection('JumpToQuestion'+index+'_'+i,'JumpToQuestion'+index+'_'+i,options[i].JumpToQuestion,jump_start_index,jump_end_index)+'</td>\n';
				html +=								'</tr>\n';
			}
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(ans.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				if(ans.indexOf(i)!=-1)
				{
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				//html +=						'<input type="checkbox" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'[]" data-amount="'+formatDisplayNumber(options[i].Amount)+'" '+(ans.indexOf(i)!=-1?'checked':'')+'/>\n';
				html +=						'<p>'+escapeHtml(options[i].Option)+' ($'+formatDisplayNumber(options[i].Amount)+')</p>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
				}
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
			
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(q.Options.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				var jump_to_question_msg = '';
				//if(options[i].JumpToQuestion == '-1'){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
				//}else if(options[i].JumpToQuestion > 0){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(options[i].JumpToQuestion)+1) +')</span>';
				//}
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				html +=						'<label for="OptionChecked'+index+'_'+i+'"><input type="checkbox" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'[]" data-amount="'+formatDisplayNumber(options[i].Amount)+'" '+(ans.indexOf(i)!=-1?'checked':'')+'/>\n';
				html +=						'<span class="tabletextrequire">#</span>'+escapeHtml(options[i].Option)+' ($'+formatDisplayNumber(options[i].Amount)+jump_to_question_msg+')</label>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};

	// helper function for FewPayFewCatWithInputQuantityQuestion
	self.isOptionIndexInAnswers = function(index,answers)
	{
		for(var i=0;i<answers.length;i++){
			if(answers[i]['Option'] == index){
				return true;
				break;
			}
		}
		
		return false;
	};
	
	// helper function for FewPayFewCatWithInputQuantityQuestion
	self.getAnswerByIndex = function(index,answers)
	{
		for(var i=0;i<answers.length;i++){
			if(answers[i]['Option'] == index){
				return answers[i];
				break;
			}
		}
		
		return null;
	};
	
	self.generateFewPayFewCatWithInputQuantityQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_payment_title = self.getOption('StrPaymentTitle');
			var str_payment_category = self.getOption('StrPaymentCategory');
			var str_payment_amount = self.getOption('StrPaymentAmount');
			var str_purchase_quantity = self.getOption('StrPurchaseQuantity');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var must_submit = q.MustSubmit == 1;
			var options = q.Options;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<u>'+str_topic_title+'</u><br /><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table width="100%">\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td width="1%">&nbsp;</td>\n';
			html +=										'<td width="14%"><u>'+str_payment_title+'</u></td>\n';
			html +=										'<td width="12%"><u>'+str_payment_category+'</u></td>\n';
			html +=										'<td width="13%"><u>'+str_payment_amount+'</u></td>\n';
			html += 									'<td width="10%"><u>'+str_purchase_quantity+'</u></td>\n';
			html +=										'<td width="40%">&nbsp;</td>\n';
			html +=										'<td width="10%">&nbsp;</td>\n';
			html +=									'</tr>\n';
			for(var i=0;i<options.length;i++){
				var category_selection_html = self.generatePaymentCategorySelection('CategoryID'+index+'_'+i,'CategoryID'+index+'_'+i,options[i].CategoryID);
				html +=								'<tr>\n';
				html += 								'<td valign="top"><input type="checkbox" value="0" name="OptionChecked'+index+'[]" id="OptionChecked'+index+'_'+i+'"></td>\n';
				html +=									'<td valign="top"><input type="text" id="Option'+index+'_'+i+'" name="Option'+index+'_'+i+'" value="'+escapeDoubleQuotes(options[i].Option)+'" size="25" class="tabletext" /></td>\n';
				html +=									'<td valign="top">'+category_selection_html+'</td>\n';
				html +=									'<td valign="top">$<input type="number" value="'+escapeDoubleQuotes(options[i].Amount)+'" id="Amount'+index+'_'+i+'" name="Amount'+index+'_'+i+'" size="25" class="tabletext" min="0" /></td>\n';
				html += 								'<td valign="top"><input type="number" value="'+options[i]['MinQuantity']+'" name="MinQuantity'+index+'_'+i+'" id="MinQuantity'+index+'_'+i+'" min="1" style="width:40%;" /> - <input type="number" value="'+options[i]['MaxQuantity']+'" name="MaxQuantity'+index+'_'+i+'" id="MaxQuantity'+index+'_'+i+'" min="1" style="width:40%;" /></td>\n';
				html += 								'<td valign="top">'+self.generateSubsidyContainerHtml(q,index,i)+'</td>\n';
				html += 								'<td valign="top" align="right">'+self.generateGoToQuestionSelection('JumpToQuestion'+index+'_'+i,'JumpToQuestion'+index+'_'+i,options[i].JumpToQuestion,jump_start_index,jump_end_index)+'</td>\n';
				html +=								'</tr>\n';
			}
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(ans.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				if(self.isOptionIndexInAnswers(i,ans))
				{
					var answer_obj = self.getAnswerByIndex(i,ans);
					var answer_quantity = answer_obj? answer_obj['Quantity'] : 0;
					html +=				'<tr>\n';
					html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
					//html +=						'<input type="checkbox" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'[]" data-amount="'+formatDisplayNumber(options[i].Amount * answer_quantity)+'" '+('checked')+'/>\n';
					html +=						'<p>'+escapeHtml(options[i].Option)+' ($'+formatDisplayNumber(options[i].Amount)+' x '+answer_quantity+')</p>\n';
					html +=					'</td>\n';
					html +=				'</tr>\n';
				}
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
			
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var str_purchase_quantity = self.getOption('StrPurchaseQuantity');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(q.Options.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				var jump_to_question_msg = '';
				//if(options[i].JumpToQuestion == '-1'){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
				//}else if(options[i].JumpToQuestion > 0){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(options[i].JumpToQuestion)+1) +')</span>';
				//}
				var answer_obj = self.getAnswerByIndex(i,ans);
				var is_answer_checked = answer_obj? true:false;
				var answer_quantity = answer_obj? convertToInteger(answer_obj['Quantity']) : convertToInteger(options[i]['MinQuantity']);
				var quantity_selection_data = makeRangeDataForSelection(convertToInteger(options[i]['MinQuantity']),convertToInteger(options[i]['MaxQuantity']));
				var quantity_selection = createSelection(quantity_selection_data,' class="answer-quantity" id="AnswerQuantity'+index+'_'+i+'" name="AnswerQuantity'+index+'[]" ',answer_quantity);
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				html +=						'<label for="OptionChecked'+index+'_'+i+'"><input type="checkbox" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'[]" data-amount="'+formatDisplayNumber(options[i].Amount)+'" data-quantity="'+answer_quantity+'" '+(is_answer_checked?'checked':'')+'/>\n';
				html +=						'<span class="tabletextrequire">#</span>'+escapeHtml(options[i].Option)+' ($'+formatDisplayNumber(options[i].Amount)+jump_to_question_msg+')</label>\n';
				//html += 					'<span id="AnswerQuantityContainer'+index+'_'+i+'"'+(is_answer_checked?'':' style="display:none"')+'><label for="AnswerQuantity'+index+'_'+i+'">'+str_purchase_quantity + ': </label><input type="number" class="answer-quantity" value="'+answer_quantity+'" id="AnswerQuantity'+index+'_'+i+'" name="AnswerQuantity'+index+'[]" min="1" /></span>\n';
				html += 					'<span id="AnswerQuantityContainer'+index+'_'+i+'"'+(is_answer_checked?'':' style="display:none"')+'><label for="AnswerQuantity'+index+'_'+i+'">'+str_purchase_quantity + ': </label>'+quantity_selection+'</span>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateTextLongQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_payment_description = self.getOption('StrPaymentDescription');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var jump_to_question = q.JumpToQuestion;
			var must_submit = q.MustSubmit == 1;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			var go_to_question_selection = self.generateGoToQuestionSelection('JumpToQuestion'+index,'JumpToQuestion'+index,jump_to_question,jump_start_index,jump_end_index);		
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<u>'+str_topic_title+'</u>: <br /><textarea cols="30" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<textarea cols="50" rows="3" id="Answer'+index+'" name="Answer'+index+'"></textarea>\n';
			html +=						'</td>\n';
			html += 					'<td align="right" valign="top">\n';
			html += 						go_to_question_selection+'\n';					
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td width="100%" bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'<br /><p>'+escapeHtml(ans)+'</p>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			var jump_to_question_msg = '';
			//if(q.JumpToQuestion == '-1'){
			//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(q.JumpToQuestion > 0){
			//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(q.JumpToQuestion)+1) +')</span>';
			//}
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td width="100%" bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+jump_to_question_msg+'<br /><textarea class="answer" cols="50" rows="3" id="Answer'+index+'" name="Answer'+index+'">'+escapeHtml(ans)+'</textarea>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateTextShortQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_payment_description = self.getOption('StrPaymentDescription');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var jump_to_question = q.JumpToQuestion;
			var must_submit = q.MustSubmit == 1;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			var go_to_question_selection = self.generateGoToQuestionSelection('JumpToQuestion'+index,'JumpToQuestion'+index,jump_to_question,jump_start_index,jump_end_index);		
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<u>'+str_topic_title+'</u>: <br /><textarea cols="30" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<input type="text" id="Answer'+index+'" name="Answer'+index+'" value="" />\n';
			html +=						'</td>\n';
			html += 					'<td align="right" valign="top">\n';
			html += 						go_to_question_selection+'\n';					
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td width="100%" bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'<br /><p>'+escapeHtml(ans)+'</p>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			var jump_to_question_msg = '';
			//if(q.JumpToQuestion == '-1'){
			//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(q.JumpToQuestion > 0){
			//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(q.JumpToQuestion)+1) +')</span>';
			//}
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td width="100%" bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+jump_to_question_msg+'<br /><input type="text" class="tabletext answer" id="Answer'+index+'" name="Answer'+index+'" value="'+escapeDoubleQuotes(ans)+'"/>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateTrueOrFalseQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var option_yes = q.OptionYes;
			var option_no = q.OptionNo;
			var option_yes_jump_to_question = q.OptionYesJumpToQuestion;
			var option_no_jump_to_question = q.OptionNoJumpToQuestion;
			var must_submit = q.MustSubmit == 1;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			var option_yes_go_to_question_selection = self.generateGoToQuestionSelection('OptionYesJumpToQuestion'+index,'OptionYesJumpToQuestion'+index,option_yes_jump_to_question,jump_start_index,jump_end_index);
			var option_no_go_to_question_selection = self.generateGoToQuestionSelection('OptionNoJumpToQuestion'+index,'OptionNoJumpToQuestion'+index,option_no_jump_to_question,jump_start_index,jump_end_index);		
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<table>\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea></td>\n';
			html +=									'</tr>\n';
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<input type="radio" value="0" id="OptionYesChecked'+index+'" name="OptionChecked'+index+'" />\n';
			html +=							'<input type="text" value="'+escapeDoubleQuotes(option_yes)+'" id="OptionYes'+index+'" name="OptionYes'+index+'" size="25" class="tabletext" /><br />\n';
			html +=							'<input type="radio" value="1" id="OptionNoChecked'+index+'" name="OptionChecked'+index+'" />\n';
			html +=							'<input type="text" value="'+escapeDoubleQuotes(option_no)+'" id="OptionNo'+index+'" name="OptionNo'+index+'" size="25" class="tabletext" />\n';
			html +=						'</td>\n';
			html += 					'<td align="right" valign="top">\n';
			html += 						option_yes_go_to_question_selection+'<br />\n';
			html += 						option_no_go_to_question_selection;						
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="3" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			if(ans=='0'){
			//html +=							'<input type="radio" class="answer" value="0" id="OptionYesChecked'+index+'" name="OptionChecked'+index+'" '+(ans=='0'?'checked':'')+' />\n';
			html +=							'<p>'+escapeHtml(q.OptionYes)+'</p>\n';
			}
			if(ans=='1'){
			//html +=							'<input type="radio" class="answer" value="1" id="OptionNoChecked'+index+'" name="OptionChecked'+index+'" '+(ans=='1'?'checked':'')+' />\n';
			html +=							'<p>'+escapeHtml(q.OptionNo)+'</p>\n';
			}
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
			
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var option_yes_jump_to_question = q.OptionYesJumpToQuestion;
			var option_no_jump_to_question = q.OptionNoJumpToQuestion;
			var must_submit = q.MustSubmit == '1';
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var option_yes_jump_to_question_msg = '';
			var option_no_jump_to_question_msg = '';
			
			//if(option_yes_jump_to_question == '-1'){
			//	option_yes_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(option_yes_jump_to_question > 0){
			//	option_yes_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+ str_go_to_question + (convertToInteger(option_yes_jump_to_question)+1) +')</span>';
			//}
			//if(option_no_jump_to_question == '-1'){
			//	option_no_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
			//}else if(option_no_jump_to_question > 0){
			//	option_no_jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+ str_go_to_question + (convertToInteger(option_no_jump_to_question)+1) +')</span>';
			//}
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="3" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			html +=							'<label for="OptionYesChecked'+index+'"><input type="radio" class="answer" value="0" id="OptionYesChecked'+index+'" name="OptionChecked'+index+'" '+(ans=='0'?'checked':'')+' />\n';
			html +=							''+escapeHtml(q.OptionYes)+option_yes_jump_to_question_msg+'</label>';
			html +=							'<label for="OptionNoChecked'+index+'"><input type="radio" class="answer" value="1" id="OptionNoChecked'+index+'" name="OptionChecked'+index+'" '+(ans=='1'?'checked':'')+' />\n';
			html +=							''+escapeHtml(q.OptionNo)+option_no_jump_to_question_msg+'</label>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		return html;
	};
	
	self.generateMCSingleQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var must_submit = q.MustSubmit == 1;
			var options = q.Options;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			
			var br = '';
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<table>\n';
			html +=								'<tbody>\n';
			html +=									'<tr>\n';
			html +=										'<td><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea></td>\n';
			html +=									'</tr>\n';
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=					'<tr>\n';
			html +=						'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
			br = '';
			for(var i=0;i<options.length;i++){
				html +=						br+'<input type="radio" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" />\n';
				html +=						'<input type="text" value="'+escapeDoubleQuotes(options[i].Option)+'" id="Option'+index+'_'+i+'" name="Option'+index+'_'+i+'" size="25" class="tabletext" />\n';
				br = '<br />';
			}
			html +=						'</td>\n';
			html += 					'<td align="right" valign="top">\n';
			br = '';
			for(var i=0;i<options.length;i++){
				html += br+self.generateGoToQuestionSelection('JumpToQuestion'+index+'_'+i,'JumpToQuestion'+index+'_'+i,options[i].JumpToQuestion,jump_start_index,jump_end_index)+'\n';
				br = '<br />';
			}
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				if(ans==convertToString(i)){
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				//html +=						'<input type="radio" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" '+(ans==convertToString(i)?'checked':'')+'/>\n';
				html +=						'<p>'+escapeHtml(options[i].Option)+')</p>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
				}
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(q.Options.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				var jump_to_question_msg = '';
				//if(options[i].JumpToQuestion == '-1'){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
				//}else if(options[i].JumpToQuestion > 0){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(options[i].JumpToQuestion)+1) +')</span>';
				//}
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				html +=						'<label for="OptionChecked'+index+'_'+i+'"><input type="radio" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'" '+(ans==convertToString(i)?'checked':'')+'/>\n';
				html +=						''+escapeHtml(options[i].Option)+jump_to_question_msg+'</label>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};
	
	self.generateMCMultipleQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_question_need_to_reply = self.getOption('StrQuestionNeedToReply');
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			var must_submit = q.MustSubmit == 1;
			var options = q.Options;
			
			var jump_start_index = index+1;
			var jump_end_index = questions.length-1;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<u>'+str_topic_title+'</u><br /><textarea cols="20" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html += 					'<td class="tabletext" align="right" valign="top">\n';
			html += 						'<span>'+str_question_need_to_reply+'</span>\n';
			html +=							'<input type="checkbox" class="must-submit-checkbox" value="1" name="MustSubmit'+index+'" id="MustSubmit'+index+'" '+(must_submit?'checked':'')+' />\n';
			html += 					'</td>\n';
			html +=					'</tr>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" colspan="2" class="tabletext">\n';
			html += 						'<table width="100%">\n';
			html +=								'<tbody>\n';
			for(var i=0;i<options.length;i++){
				html +=								'<tr>\n';
				html += 								'<td style="width:10px"><input type="checkbox" value="0" name="OptionChecked'+index+'[]" id="OptionChecked'+index+'_'+i+'"></td>';
				html +=									'<td><input type="text" id="Option'+index+'_'+i+'" name="Option'+index+'_'+i+'" value="'+escapeDoubleQuotes(options[i].Option)+'" size="25" class="tabletext" /></td>\n';
				html+= 									'<td align="right" valign="top">'+self.generateGoToQuestionSelection('JumpToQuestion'+index+'_'+i,'JumpToQuestion'+index+'_'+i,options[i].JumpToQuestion,jump_start_index,jump_end_index)+'</td>\n';
				html +=								'</tr>\n';
			}
			html +=								'</tbody>\n';
			html +=							'</table>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else if(mode == PREVIEW_MODE){
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(ans.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				if(ans.indexOf(i)!=-1)
				{
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				//html +=						'<input type="checkbox" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'[]" '+(ans.indexOf(i)!=-1?'checked':'')+'/>\n';
				html +=						'<p>'+nl2br(escapeHtml(options[i].Option))+'</p>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
				}
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
			
		}else{
			var str_no_need_to_answer = self.getOption('StrNoNeedToAnswerThisQuestion');
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			var must_submit = q.MustSubmit == '1';
			var options = q.Options;
			var ans = q.Answer;
			
			var str_go_to_question = self.getOption('StrGoToQuestion');
			var str_go_to_end_of_replyslip = self.getOption('StrGoToEndOfReplySlip');
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="'+(q.Options.length+1)+'" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire must-submit-asterisk"'+(must_submit?'':' style="display:none;"')+'>*</span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						escapeHtml(q.Title)+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			for(var i=0;i<options.length;i++)
			{
				var jump_to_question_msg = '';
				//if(options[i].JumpToQuestion == '-1'){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_end_of_replyslip+')</span>';
				//}else if(options[i].JumpToQuestion > 0){
				//	jump_to_question_msg = '<span style="color:'+go_to_question_msg_color+'"> ('+str_go_to_question + (convertToInteger(options[i].JumpToQuestion)+1) +')</span>';
				//}
				html +=				'<tr>\n';
				html +=					'<td width="100%" class="tabletext" bgcolor="#EFEFEF">\n';
				html +=						'<label for="OptionChecked'+index+'_'+i+'"><input type="checkbox" class="answer" value="'+i+'" id="OptionChecked'+index+'_'+i+'" name="OptionChecked'+index+'[]" '+(ans.indexOf(i)!=-1?'checked':'')+'/>\n';
				html +=						''+escapeHtml(options[i].Option)+jump_to_question_msg+'</label>\n';
				html +=					'</td>\n';
				html +=				'</tr>\n';
			}
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Info'+index+'" class="info" style="'+info_msg_style+'display:none;margin:8px 24px;">'+str_no_need_to_answer+'</div>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		return html;
	};

	self.generateNotApplicableQuestionEditableHtml = function(q, index)
	{
		var mode = self.getOption('Mode');
		var html = '';
		if(mode == EDIT_MODE)
		{
			var str_topic_title = self.getOption('StrTopicTitle');
			var str_delete = self.getOption('StrDelete');
			var str_move_question = self.getOption('StrMoveQuestion');
			
			var title = q.Title;
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no">'+(index+1)+'. </span>\n';
			html +=							'<span class="tabletextrequire"></span>\n';
			html +=						'</td>';
			html += 					'<td bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						'<textarea cols="30" rows="2" id="Title'+index+'" name="Title'+index+'">'+escapeHtml(title)+'</textarea>\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html += 		'<div id="Error'+index+'" class="error tabletextrequire" style="display:none;margin:8px 24px;"></div>';
			html +=		'</td>\n';
			html +=		'<td class="Dragable" bgcolor="#EFEFEF" width="48px" valign="top">\n';
			html +=			'<div class="table_row_tool" align="center">&nbsp;\n';
			html +=				'<span class="table_row_tool">\n';
			html +=					'<a href="javascript:;" id="move_btn'+index+'" class="move_order_dim" title="'+str_move_question+'"></a>\n';
			html +=				'</span>\n';
			html +=				'<span class="table_row_tool row_content_tool">\n';
			html +=					'<a href="javascript:;" id="delete_btn'+index+'" class="delete_dim" title="'+str_delete+'"></a>\n';
			html +=				'</span>\n';
			html +=			'</div>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}else{
			var display_question_number = self.getOption('DisplayQuestionNumber') == '1';
			
			html += '<tr id="row'+index+'" class="question-row">\n';
			html +=		'<td bgcolor="#EFEFEF">\n';
			html += 		'<table border="0" width="100%" align="center" bgcolor="#EFEFEF">\n';
			html +=				'<tbody>\n';
			html += 				'<tr>\n';
			html += 					'<td bgcolor="#EFEFEF" rowspan="2" class="tabletext" style="vertical-align:top;padding:5px;width:12px;">\n';
			html += 						'<span class="question-no"'+(display_question_number?'':' style="display:none;"')+'>'+(index+1)+'. </span>\n';
			html +=						'</td>';
			html += 					'<td width="100%" bgcolor="#EFEFEF" class="tabletext">\n';
			html += 						nl2br(escapeHtml(q.Title))+'\n';
			html +=						'</td>\n';
			html +=					'</tr>\n';
			html +=				'</tbody>\n';
			html +=			'</table>\n';
			html +=		'</td>\n';
			html +=	'</tr>\n';
		}
		
		return html;
	};	
	
	// bind events to data objects
	self.bindMustPayQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
			
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#CategoryID'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.CategoryID = value;
			});
			
			$('#Amount'+index).unbind('change').bind('change',function(){
				var value = Math.max(0, formatDisplayNumber($(this).val()));
				
				$(this).val(value);
				q.Amount = value;
				
				$('#SubsidyStudentTable_'+index+'_0 input.no-subsidy').val(value).change();
			});
			
			$('#Option'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Option = value;
			});
			
			$('#JumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.JumpToQuestion = value;
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
			
			$('#EnableSubsidy_'+index+'_0').unbind('click').bind('click',function(){
				var checked = $(this).is(':checked');
				q.Subsidy[0]['Enable'] = checked? '1':'0';
				if(checked){
					if($('#SubsidyTable_'+index+'_0').length == 0){
						$('#SubsidyTableContainer_'+index+'_0').html(self.generateSubsidyTableHtml(q,index,0));
						self.bindSubsidyData(q, index, 0);
					}
					$('#SubsidyTable_'+index+'_0').show();
					if(q.Subsidy[0]['SetIdentity'] != '1' && q.Subsidy[0]['SetStudent'] != '1'){
						q.Subsidy[0]['SetIdentity'] = '1';
						$('#SetIdentity_'+index+'_0').attr('checked',true);
						$('#SubsidyIdentityTable_'+index+'_0').show();
					}
					$('#Amount'+index).change();
				}else{
					//$('#SubsidyTable_'+index+'_0').hide();
					$('#SubsidyTable_'+index+'_0').remove();
				}
			});
			
			if(q.Subsidy[0]['Enable'] == 1){
				self.bindSubsidyData(q, index, 0);
			}
			
			/*
			$('#SetIdentity_'+index+'_0').unbind('click').bind('click',function(){
				var checked = $(this).is(':checked');
				q.Subsidy[0]['SetIdentity'] = checked? '1':'0';
				if(checked){
					$('#SubsidyIdentityTable_'+index+'_0').show();
				}else{
					$('#SubsidyIdentityTable_'+index+'_0').hide();
				}
			});
			
			$('#SetStudent_'+index+'_0').unbind('click').bind('click',function(){
				var checked = $(this).is(':checked');
				q.Subsidy[0]['SetStudent'] = checked? '1':'0';
				if(checked){
					$('#SubsidyStudentTable_'+index+'_0').show();
				}else{
					$('#SubsidyStudentTable_'+index+'_0').hide();
				}
			});
			
			$('#SubsidyIdentityTable_'+index+'_0 input.identity-amount').unbind('change').bind('change',function(){
				var value = Math.max(0, formatDisplayNumber($(this).val()));
				$(this).val(value);
				var identity_id = $(this).attr('data-identityid');
				q.Subsidy[0]['Identities'][identity_id] = value;
				
				var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
				for(var j=0;j<subsidy_identity_ary.length;j++){
					if(subsidy_identity_ary[j]['IdentityID'] == identity_id){
						var student_ids = subsidy_identity_ary[j]['StudentIDs'];
						for(var k=0;k<student_ids.length;k++){
							var tmp_obj = $('#StudentAmount_'+index+'_0_'+student_ids[k]);
							if(tmp_obj && tmp_obj.val() == ''){
								tmp_obj.val(value);
								tmp_obj.change();
							}
						}
						break;
					}
				}
			});
			
			$('#SubsidyStudentTable_'+index+'_0 input.student-amount').unbind('change').bind('change',function(){
				var value = Math.max(0, formatDisplayNumber($(this).val()));
				$(this).val(value);
				var student_id = $(this).attr('data-userid');
				q.Subsidy[0]['Students'][student_id] = value;
			});
			
			$('#AssignAllIdentityAmountBtn_'+index+'_0').unbind('click').bind('click',function(){
				var amount = Math.max(0, formatDisplayNumber($('#AssignAllIdentityAmount_'+index+'_0').val()));
				$('#AssignAllIdentityAmount_'+index+'_0').val(amount);
				$('#SubsidyIdentityTable_'+index+'_0 input.identity-amount').val(amount);
				$('#SubsidyIdentityTable_'+index+'_0 input.identity-amount').change();
			});
			
			$('#AssignAllStudentAmountBtn_'+index+'_0').unbind('click').bind('click',function(){
				var amount = Math.max(0, formatDisplayNumber($('#AssignAllStudentAmount_'+index+'_0').val()));
				$('#AssignAllStudentAmount_'+index+'_0').val(amount);
				$('#SubsidyStudentTable_'+index+'_0 tr.row-class:visible input.student-amount').val(amount);
				$('#SubsidyStudentTable_'+index+'_0 tr.row-class:visible input.student-amount').change();
			});
			
			for(var j=0;j<subsidy_identity_ary.length;j++){
				var identity_id = subsidy_identity_ary[j]['IdentityID'];
				$('#AssignIdentityAmountToStudentBtn_'+index+'_0_'+identity_id).unbind('click').bind('click',function(){
					var target_student_ary = self.getOption('TargetStudentAry');
					var id_parts = $(this).attr('id').split('_');
					var que_index = id_parts[1];
					var ans_index = id_parts[2];
					var identity_id = id_parts[3];
					var amount = Math.max(0, formatDisplayNumber($('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val()));
					$('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val(amount);
					for(var k=0;k<target_student_ary.length;k++){
						if(target_student_ary[k]['IdentityID'] == identity_id){
							var tmp_obj = $('input#StudentAmount_'+que_index+'_'+ans_index+'_'+target_student_ary[k]['UserID']);
							if(tmp_obj){
								tmp_obj.val(amount).change();
							}
						}
					}
				});
			}
			
			$('#SubsidyStudentClass_'+index+'_0').unbind('change').bind('change',function(){
				var selected_value = $(this).val();
				if(selected_value == ''){
					$('#SubsidyStudentTable_'+index+'_0 .row-class').show();
				}else{
					$('#SubsidyStudentTable_'+index+'_0 .row-class').hide();
					$('#SubsidyStudentTable_'+index+'_0 .class-'+escapeJQueryId(selected_value)).show();
				}
			});
			*/
		}else{
			$('#OptionChecked'+index).unbind('click').bind('click',function(){
				var is_checked = $(this).is(':checked');
				q.Answer = is_checked?$(this).val():'';
				
				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
		}
	};
	
	self.bindWhetherToPayQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
		
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#CategoryID'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.CategoryID = value;
			});
			
			$('#Amount'+index).unbind('change').bind('change',function(){
				var value = Math.max(0, formatDisplayNumber($(this).val()));
				
				$(this).val(value);
				q.Amount = value;
				
				$('#SubsidyStudentTable_'+index+'_0 input.no-subsidy').val(value).change();
			});
			
			$('#OptionYes'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.OptionYes = value;
			});
			
			$('#OptionNo'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.OptionNo = value;
			});
			
			$('#OptionYesJumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.OptionYesJumpToQuestion = value;
			});
			
			$('#OptionNoJumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.OptionNoJumpToQuestion = value;
			});
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
			
			$('#EnableSubsidy_'+index+'_0').unbind('click').bind('click',function(){
				var checked = $(this).is(':checked');
				q.Subsidy[0]['Enable'] = checked? '1':'0';
				if(checked){
					if($('#SubsidyTable_'+index+'_0').length == 0){
						$('#SubsidyTableContainer_'+index+'_0').html(self.generateSubsidyTableHtml(q,index,0));
						self.bindSubsidyData(q, index, 0);
					}
					$('#SubsidyTable_'+index+'_0').show();
					if(q.Subsidy[0]['SetIdentity'] != '1' && q.Subsidy[0]['SetStudent'] != '1'){
						q.Subsidy[0]['SetIdentity'] = '1';
						$('#SetIdentity_'+index+'_0').attr('checked',true);
						$('#SubsidyIdentityTable_'+index+'_0').show();
					}
					$('#Amount'+index).change();
				}else{
					//$('#SubsidyTable_'+index+'_0').hide();
					$('#SubsidyTable_'+index+'_0').remove();
				}
			});
			
			if(q.Subsidy[0]['Enable'] == 1){
				self.bindSubsidyData(q, index, 0);
			}
			
			/*
			$('#SetIdentity_'+index+'_0').unbind('click').bind('click',function(){
				var checked = $(this).is(':checked');
				q.Subsidy[0]['SetIdentity'] = checked? '1':'0';
				if(checked){
					$('#SubsidyIdentityTable_'+index+'_0').show();
				}else{
					$('#SubsidyIdentityTable_'+index+'_0').hide();
				}
			});
			
			$('#SetStudent_'+index+'_0').unbind('click').bind('click',function(){
				var checked = $(this).is(':checked');
				q.Subsidy[0]['SetStudent'] = checked? '1':'0';
				if(checked){
					$('#SubsidyStudentTable_'+index+'_0').show();
				}else{
					$('#SubsidyStudentTable_'+index+'_0').hide();
				}
			});
			
			$('#SubsidyIdentityTable_'+index+'_0 input.identity-amount').unbind('change').bind('change',function(){
				var value = Math.max(0, formatDisplayNumber($(this).val()));
				$(this).val(value);
				var identity_id = $(this).attr('data-identityid');
				q.Subsidy[0]['Identities'][identity_id] = value;
				
				var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
				for(var j=0;j<subsidy_identity_ary.length;j++){
					if(subsidy_identity_ary[j]['IdentityID'] == identity_id){
						var student_ids = subsidy_identity_ary[j]['StudentIDs'];
						for(var k=0;k<student_ids.length;k++){
							var tmp_obj = $('#StudentAmount_'+index+'_0_'+student_ids[k]);
							if(tmp_obj && tmp_obj.val() == ''){
								tmp_obj.val(value);
								tmp_obj.change();
							}
						}
						break;
					}
				}
			});
			
			$('#SubsidyStudentTable_'+index+'_0 input.student-amount').unbind('change').bind('change',function(){
				var value = Math.max(0, formatDisplayNumber($(this).val()));
				$(this).val(value);
				var student_id = $(this).attr('data-userid');
				q.Subsidy[0]['Students'][student_id] = value;
			});
			
			$('#AssignAllIdentityAmountBtn_'+index+'_0').unbind('click').bind('click',function(){
				var amount = Math.max(0, formatDisplayNumber($('#AssignAllIdentityAmount_'+index+'_0').val()));
				$('#AssignAllIdentityAmount_'+index+'_0').val(amount);
				$('#SubsidyIdentityTable_'+index+'_0 input.identity-amount').val(amount);
				$('#SubsidyIdentityTable_'+index+'_0 input.identity-amount').change();
			});
			
			$('#AssignAllStudentAmountBtn_'+index+'_0').unbind('click').bind('click',function(){
				var amount = Math.max(0, formatDisplayNumber($('#AssignAllStudentAmount_'+index+'_0').val()));
				$('#AssignAllStudentAmount_'+index+'_0').val(amount);
				$('#SubsidyStudentTable_'+index+'_0 tr.row-class:visible input.student-amount').val(amount);
				$('#SubsidyStudentTable_'+index+'_0 tr.row-class:visible input.student-amount').change();
			});
			
			for(var j=0;j<subsidy_identity_ary.length;j++){
				var identity_id = subsidy_identity_ary[j]['IdentityID'];
				$('#AssignIdentityAmountToStudentBtn_'+index+'_0_'+identity_id).unbind('click').bind('click',function(){
					var target_student_ary = self.getOption('TargetStudentAry');
					var id_parts = $(this).attr('id').split('_');
					var que_index = id_parts[1];
					var ans_index = id_parts[2];
					var identity_id = id_parts[3];
					var amount = Math.max(0, formatDisplayNumber($('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val()));
					$('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val(amount);
					for(var k=0;k<target_student_ary.length;k++){
						if(target_student_ary[k]['IdentityID'] == identity_id){
							var tmp_obj = $('input#StudentAmount_'+que_index+'_'+ans_index+'_'+target_student_ary[k]['UserID']);
							if(tmp_obj){
								tmp_obj.val(amount).change();
							}
						}
					}
				});
			}
			
			$('#SubsidyStudentClass_'+index+'_0').unbind('change').bind('change',function(){
				var selected_value = $(this).val();
				if(selected_value == ''){
					$('#SubsidyStudentTable_'+index+'_0 .row-class').show();
				}else{
					$('#SubsidyStudentTable_'+index+'_0 .row-class').hide();
					$('#SubsidyStudentTable_'+index+'_0 .class-'+escapeJQueryId(selected_value)).show();
				}
			});
			*/
		}else{
			$('#OptionYesChecked'+index).unbind('click').bind('click',function(){
				var is_checked = $(this).is(':checked');
				q.Answer = is_checked?$(this).val():'';

				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
			
			$('#OptionNoChecked'+index).unbind('click').bind('click',function(){
				var is_checked = $(this).is(':checked');
				q.Answer = is_checked?$(this).val():'';
				
				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
		}
	};
	
	self.bindOnePayOneCatQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
			
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#CategoryID'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.CategoryID = value;
			});
			
			for(var i=0;i<q.Options.length;i++){
				
				$('#Amount'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Amount'+index+'_'+i).unbind('change').bind('change',function(){
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Amount = value;
					
					$('#SubsidyStudentTable_'+index+'_'+array_index+' input.no-subsidy').val(value).change();
				});
				
				$('#Description'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Description'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $.trim($(this).val());
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Option = value;
				});
				
				$('#JumpToQuestion'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#JumpToQuestion'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].JumpToQuestion = value;
				});
				
				$('#EnableSubsidy_'+index+'_'+i).data('index',i);
				$('#EnableSubsidy_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['Enable'] = checked? '1':'0';
					if(checked){
						if($('#SubsidyTable_'+index+'_'+ans_index).length == 0){
							$('#SubsidyTableContainer_'+index+'_'+ans_index).html(self.generateSubsidyTableHtml(q,index,ans_index));
							self.bindSubsidyData(q, index, ans_index);
						}
						$('#SubsidyTable_'+index+'_'+ans_index).show();
						if(q.Subsidy[ans_index]['SetIdentity'] != '1' && q.Subsidy[ans_index]['SetStudent'] != '1'){
							q.Subsidy[ans_index]['SetIdentity'] = '1';
							$('#SetIdentity_'+index+'_'+ans_index).attr('checked',true);
							$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
						}
						$('#Amount'+index+'_'+ans_index).change();
					}else{
						//$('#SubsidyTable_'+index+'_'+ans_index).hide();
						$('#SubsidyTable_'+index+'_'+ans_index).remove();
					}
				});
				
				if(q.Subsidy[i]['Enable'] == 1){
					self.bindSubsidyData(q, index, i);
				}
				
				/*
				$('#SetIdentity_'+index+'_'+i).data('index',i);
				$('#SetIdentity_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['SetIdentity'] = checked? '1':'0';
					if(checked){
						$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
					}else{
						$('#SubsidyIdentityTable_'+index+'_'+ans_index).hide();
					}
				});
				
				$('#SetStudent_'+index+'_'+i).data('index',i);
				$('#SetStudent_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['SetStudent'] = checked? '1':'0';
					if(checked){
						$('#SubsidyStudentTable_'+index+'_'+ans_index).show();
					}else{
						$('#SubsidyStudentTable_'+index+'_'+ans_index).hide();
					}
				});
				
				$('#SubsidyIdentityTable_'+index+'_'+i+' input.identity-amount').unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					$(this).val(value);
					var identity_id = $(this).attr('data-identityid');
					q.Subsidy[ans_index]['Identities'][identity_id] = value;
					
					//var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
					//for(var j=0;j<subsidy_identity_ary.length;j++){
					//	if(subsidy_identity_ary[j]['IdentityID'] == identity_id){
					//		var student_ids = subsidy_identity_ary[j]['StudentIDs'];
					//		for(var k=0;k<student_ids.length;k++){
					//			var tmp_obj = $('#StudentAmount_'+index+'_'+ans_index+'_'+student_ids[k]);
					//			if(tmp_obj && tmp_obj.val() == ''){
					//				tmp_obj.val(value);
					//				tmp_obj.change();
					//			}
					//		}
					//		break;
					//	}
					//}
					
				});
				
				$('#SubsidyStudentTable_'+index+'_'+i+' input.student-amount').unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					$(this).val(value);
					var student_id = $(this).attr('data-userid');
					q.Subsidy[ans_index]['Students'][student_id] = value;
				});
				
				$('#AssignAllIdentityAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var amount = Math.max(0, formatDisplayNumber($('#AssignAllIdentityAmount_'+index+'_'+ans_index).val()));
					$('#AssignAllIdentityAmount_'+index+'_'+ans_index).val(amount);
					$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').val(amount);
					$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').change();
				});
				
				$('#AssignAllStudentAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var amount = Math.max(0, formatDisplayNumber($('#AssignAllStudentAmount_'+index+'_'+ans_index).val()));
					$('#AssignAllStudentAmount_'+index+'_'+ans_index).val(amount);
					$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').val(amount);
					$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').change();
				});
				
				for(var j=0;j<subsidy_identity_ary.length;j++){
					var identity_id = subsidy_identity_ary[j]['IdentityID'];
					$('#AssignIdentityAmountToStudentBtn_'+index+'_'+i+'_'+identity_id).unbind('click').bind('click',function(){
						var target_student_ary = self.getOption('TargetStudentAry');
						var id_parts = $(this).attr('id').split('_');
						var que_index = id_parts[1];
						var ans_index = id_parts[2];
						var identity_id = id_parts[3];
						var amount = Math.max(0, formatDisplayNumber($('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val()));
						$('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val(amount);
						for(var k=0;k<target_student_ary.length;k++){
							if(target_student_ary[k]['IdentityID'] == identity_id){
								var tmp_obj = $('input#StudentAmount_'+que_index+'_'+ans_index+'_'+target_student_ary[k]['UserID']);
								if(tmp_obj){
									tmp_obj.val(amount).change();
								}
							}
						}
					});
				}
				
				$('#SubsidyStudentClass_'+index+'_'+i).unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var selected_value = $(this).val();
					if(selected_value == ''){
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').show();
					}else{
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').hide();
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .class-'+escapeJQueryId(selected_value)).show();
					}
				});
				*/
			}
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			for(var i=0;i<q.Options.length;i++){
				
				//$('#OptionChecked'+index+'_'+i).data('index',i);
				$('#OptionChecked'+index+'_'+i).unbind('click').bind('click',function(){
					//var array_index = $(this).data('index');
					var is_checked = $(this).is(':checked');
					q.Answer = is_checked?$(this).val():'';
					
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
			}
		}
	};
	
	self.bindOnePayFewCatQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
		
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			for(var i=0;i<q.Options.length;i++){
				
				$('#Option'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Option'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $.trim($(this).val());
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Option = value;
				});
				
				$('#CategoryID'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#CategoryID'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].CategoryID = value;
				});
				
				$('#Amount'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Amount'+index+'_'+i).unbind('change').bind('change',function(){
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Amount = value;
					
					$('#SubsidyStudentTable_'+index+'_'+array_index+' input.no-subsidy').val(value).change();
				});
				
				$('#JumpToQuestion'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#JumpToQuestion'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].JumpToQuestion = value;
				});
				
				$('#EnableSubsidy_'+index+'_'+i).data('index',i);
				$('#EnableSubsidy_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['Enable'] = checked?'1':'0';
					if(checked){
						if($('#SubsidyTable_'+index+'_'+ans_index).length == 0){
							$('#SubsidyTableContainer_'+index+'_'+ans_index).html(self.generateSubsidyTableHtml(q,index,ans_index));
							self.bindSubsidyData(q, index, ans_index);
						}
						$('#SubsidyTable_'+index+'_'+ans_index).show();
						if(q.Subsidy[ans_index]['SetIdentity'] != '1' && q.Subsidy[ans_index]['SetStudent'] != '1'){
							q.Subsidy[ans_index]['SetIdentity'] = '1';
							$('#SetIdentity_'+index+'_'+ans_index).attr('checked',true);
							$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
						}
						$('#Amount'+index+'_'+ans_index).change();
					}else{
						//$('#SubsidyTable_'+index+'_'+ans_index).hide();
						$('#SubsidyTable_'+index+'_'+ans_index).remove();
					}
				});
				
				if(q.Subsidy[i]['Enable'] == 1){
					self.bindSubsidyData(q, index, i);
				}
				
				/*
				$('#SetIdentity_'+index+'_'+i).data('index',i);
				$('#SetIdentity_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['SetIdentity'] = checked?'1':'0';
					if(checked){
						$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
					}else{
						$('#SubsidyIdentityTable_'+index+'_'+ans_index).hide();
					}
				});
				
				$('#SetStudent_'+index+'_'+i).data('index',i);
				$('#SetStudent_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['SetStudent'] = checked?'1':'0';
					if(checked){
						$('#SubsidyStudentTable_'+index+'_'+ans_index).show();
					}else{
						$('#SubsidyStudentTable_'+index+'_'+ans_index).hide();
					}
				});
				
				$('#SubsidyIdentityTable_'+index+'_'+i+' input.identity-amount').unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					$(this).val(value);
					var identity_id = $(this).attr('data-identityid');
					q.Subsidy[ans_index]['Identities'][identity_id] = value;
					
					//var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
					//for(var j=0;j<subsidy_identity_ary.length;j++){
					//	if(subsidy_identity_ary[j]['IdentityID'] == identity_id){
					//		var student_ids = subsidy_identity_ary[j]['StudentIDs'];
					//		for(var k=0;k<student_ids.length;k++){
					//			var tmp_obj = $('#StudentAmount_'+index+'_'+ans_index+'_'+student_ids[k]);
					//			if(tmp_obj && tmp_obj.val() == ''){
					//				tmp_obj.val(value);
					//				tmp_obj.change();
					//			}
					//		}
					//		break;
					//	}
					//}
					
				});
				
				$('#SubsidyStudentTable_'+index+'_'+i+' input.student-amount').unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					$(this).val(value);
					var student_id = $(this).attr('data-userid');
					q.Subsidy[ans_index]['Students'][student_id] = value;
				});
				
				$('#AssignAllIdentityAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var amount = Math.max(0, formatDisplayNumber($('#AssignAllIdentityAmount_'+index+'_'+ans_index).val()));
					$('#AssignAllIdentityAmount_'+index+'_'+ans_index).val(amount);
					$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').val(amount);
					$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').change();
				});
				
				$('#AssignAllStudentAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var amount = Math.max(0, formatDisplayNumber($('#AssignAllStudentAmount_'+index+'_'+ans_index).val()));
					$('#AssignAllStudentAmount_'+index+'_'+ans_index).val(amount);
					$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').val(amount);
					$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').change();
				});
				
				for(var j=0;j<subsidy_identity_ary.length;j++){
					var identity_id = subsidy_identity_ary[j]['IdentityID'];
					$('#AssignIdentityAmountToStudentBtn_'+index+'_'+i+'_'+identity_id).unbind('click').bind('click',function(){
						var target_student_ary = self.getOption('TargetStudentAry');
						var id_parts = $(this).attr('id').split('_');
						var que_index = id_parts[1];
						var ans_index = id_parts[2];
						var identity_id = id_parts[3];
						var amount = Math.max(0, formatDisplayNumber($('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val()));
						$('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val(amount);
						for(var k=0;k<target_student_ary.length;k++){
							if(target_student_ary[k]['IdentityID'] == identity_id){
								var tmp_obj = $('input#StudentAmount_'+que_index+'_'+ans_index+'_'+target_student_ary[k]['UserID']);
								if(tmp_obj){
									tmp_obj.val(amount).change();
								}
							}
						}
					});
				}
				
				$('#SubsidyStudentClass_'+index+'_'+i).unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var selected_value = $(this).val();
					if(selected_value == ''){
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').show();
					}else{
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').hide();
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .class-'+escapeJQueryId(selected_value)).show();
					}
				});
				*/
			}
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			for(var i=0;i<q.Options.length;i++){
				
				//$('#OptionChecked'+index+'_'+i).data('index',i);
				$('#OptionChecked'+index+'_'+i).unbind('click').bind('click',function(){
					//var array_index = $(this).data('index');
					var is_checked = $(this).is(':checked');
					q.Answer = is_checked?$(this).val():'';
					
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
			}
		}
	};
	
	self.bindFewPayFewCatQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
		
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			for(var i=0;i<q.Options.length;i++){
				
				$('#Option'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Option'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $.trim($(this).val());
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Option = value;
				});
				
				$('#CategoryID'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#CategoryID'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].CategoryID = value;
				});
				
				$('#Amount'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Amount'+index+'_'+i).unbind('change').bind('change',function(){
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Amount = value;
					
					$('#SubsidyStudentTable_'+index+'_'+array_index+' input.no-subsidy').val(value).change();
				});
				
				$('#JumpToQuestion'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#JumpToQuestion'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].JumpToQuestion = value;
				});
				
				$('#EnableSubsidy_'+index+'_'+i).data('index',i);
				$('#EnableSubsidy_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['Enable'] = checked?'1':'0';
					if(checked){
						if($('#SubsidyTable_'+index+'_'+ans_index).length == 0){
							$('#SubsidyTableContainer_'+index+'_'+ans_index).html(self.generateSubsidyTableHtml(q,index,ans_index));
							self.bindSubsidyData(q, index, ans_index);
						}
						$('#SubsidyTable_'+index+'_'+ans_index).show();
						if(q.Subsidy[ans_index]['SetIdentity'] != '1' && q.Subsidy[ans_index]['SetStudent'] != '1'){
							q.Subsidy[ans_index]['SetIdentity'] = '1';
							$('#SetIdentity_'+index+'_'+ans_index).attr('checked',true);
							$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
						}
						$('#Amount'+index+'_'+ans_index).change();
					}else{
						//$('#SubsidyTable_'+index+'_'+ans_index).hide();
						$('#SubsidyTable_'+index+'_'+ans_index).remove();
					}
				});
				
				if(q.Subsidy[i]['Enable'] == 1){
					self.bindSubsidyData(q, index, i);
				}
				
				/*
				$('#SetIdentity_'+index+'_'+i).data('index',i);
				$('#SetIdentity_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['SetIdentity'] = checked?'1':'0';
					if(checked){
						$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
					}else{
						$('#SubsidyIdentityTable_'+index+'_'+ans_index).hide();
					}
				});
				
				$('#SetStudent_'+index+'_'+i).data('index',i);
				$('#SetStudent_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['SetStudent'] = checked?'1':'0';
					if(checked){
						$('#SubsidyStudentTable_'+index+'_'+ans_index).show();
					}else{
						$('#SubsidyStudentTable_'+index+'_'+ans_index).hide();
					}
				});
				
				$('#SubsidyIdentityTable_'+index+'_'+i+' input.identity-amount').unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					$(this).val(value);
					var identity_id = $(this).attr('data-identityid');
					q.Subsidy[ans_index]['Identities'][identity_id] = value;
					
					//var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
					//for(var j=0;j<subsidy_identity_ary.length;j++){
					//	if(subsidy_identity_ary[j]['IdentityID'] == identity_id){
					//		var student_ids = subsidy_identity_ary[j]['StudentIDs'];
					//		for(var k=0;k<student_ids.length;k++){
					//			var tmp_obj = $('#StudentAmount_'+index+'_'+ans_index+'_'+student_ids[k]);
					//			if(tmp_obj && tmp_obj.val() == ''){
					//				tmp_obj.val(value);
					//				tmp_obj.change();
					//			}
					//		}
					//		break;
					//	}
					//}
					
				});
				
				$('#SubsidyStudentTable_'+index+'_'+i+' input.student-amount').unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					$(this).val(value);
					var student_id = $(this).attr('data-userid');
					q.Subsidy[ans_index]['Students'][student_id] = value;
				});
				
				$('#AssignAllIdentityAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var amount = Math.max(0, formatDisplayNumber($('#AssignAllIdentityAmount_'+index+'_'+ans_index).val()));
					$('#AssignAllIdentityAmount_'+index+'_'+ans_index).val(amount);
					$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').val(amount);
					$('#SubsidyIdentityTable_'+index+'_'+ans_index+' input.identity-amount').change();
				});
				
				$('#AssignAllStudentAmountBtn_'+index+'_'+i).unbind('click').bind('click',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var amount = Math.max(0, formatDisplayNumber($('#AssignAllStudentAmount_'+index+'_'+ans_index).val()));
					$('#AssignAllStudentAmount_'+index+'_'+ans_index).val(amount);
					$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').val(amount);
					$('#SubsidyStudentTable_'+index+'_'+ans_index+' tr.row-class:visible input.student-amount').change();
				});
				
				for(var j=0;j<subsidy_identity_ary.length;j++){
					var identity_id = subsidy_identity_ary[j]['IdentityID'];
					$('#AssignIdentityAmountToStudentBtn_'+index+'_'+i+'_'+identity_id).unbind('click').bind('click',function(){
						var target_student_ary = self.getOption('TargetStudentAry');
						var id_parts = $(this).attr('id').split('_');
						var que_index = id_parts[1];
						var ans_index = id_parts[2];
						var identity_id = id_parts[3];
						var amount = Math.max(0, formatDisplayNumber($('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val()));
						$('#IdentityAmount_'+que_index+'_'+ans_index+'_'+identity_id).val(amount);
						for(var k=0;k<target_student_ary.length;k++){
							if(target_student_ary[k]['IdentityID'] == identity_id){
								var tmp_obj = $('input#StudentAmount_'+que_index+'_'+ans_index+'_'+target_student_ary[k]['UserID']);
								if(tmp_obj){
									tmp_obj.val(amount).change();
								}
							}
						}
					});
				}
				
				$('#SubsidyStudentClass_'+index+'_'+i).unbind('change').bind('change',function(){
					var ans_index = $(this).attr('id').split('_')[2];
					var selected_value = $(this).val();
					if(selected_value == ''){
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').show();
					}else{
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .row-class').hide();
						$('#SubsidyStudentTable_'+index+'_'+ans_index+' .class-'+escapeJQueryId(selected_value)).show();
					}
				});
				*/
			}
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			for(var i=0;i<q.Options.length;i++){
				
				//$('#OptionChecked'+index+'_'+i).data('index',i);
				$('#OptionChecked'+index+'_'+i).unbind('click').bind('click',function(){
					//var array_index = $(this).data('index');
					var is_checked = $(this).is(':checked');
					var value = $(this).val();
					var find_index = q.Answer.indexOf(value);
					if(is_checked){
						if(find_index == -1){
							q.Answer.push(value);
						}
					}else{
						if(find_index != -1){
							q.Answer.splice(find_index,1);
						}
					}
					q.Answer.sort();
					
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
			}
		}
	};

	self.bindFewPayFewCatWithInputQuantityQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			var subsidy_identity_ary = self.getOption('SubsidyIdentityAry');
		
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			for(var i=0;i<q.Options.length;i++){
				
				$('#Option'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Option'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $.trim($(this).val());
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Option = value;
				});
				
				$('#CategoryID'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#CategoryID'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].CategoryID = value;
				});
				
				$('#Amount'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Amount'+index+'_'+i).unbind('change').bind('change',function(){
					var value = Math.max(0, formatDisplayNumber($(this).val()));
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Amount = value;
					
					$('#SubsidyStudentTable_'+index+'_'+array_index+' input.no-subsidy').val(value).change();
				});
				
				$('#MinQuantity'+index+'_'+i).data('que-index',index);
				$('#MinQuantity'+index+'_'+i).data('ans-index',i);
				$('#MinQuantity'+index+'_'+i).unbind('change').bind('change',function(){
					var que_index = $(this).data('que-index');
					var ans_index = $(this).data('ans-index');
					var max_value = $('#MaxQuantity'+que_index+'_'+ans_index).val();
					var value = Math.max(1, formatDisplayNumber($(this).val()));
					value = Math.min(value, max_value);
					$(this).val(value);
					q.Options[ans_index]['MinQuantity'] = value;
				});
				
				$('#MaxQuantity'+index+'_'+i).data('que-index',index);
				$('#MaxQuantity'+index+'_'+i).data('ans-index',i);
				$('#MaxQuantity'+index+'_'+i).unbind('change').bind('change',function(){
					var que_index = $(this).data('que-index');
					var ans_index = $(this).data('ans-index');
					var min_value = $('#MinQuantity'+que_index+'_'+ans_index).val();
					var value = Math.max(1, formatDisplayNumber($(this).val()));
					value = Math.max(value, min_value);
					$(this).val(value);
					q.Options[ans_index]['MaxQuantity'] = value;
				});
				
				$('#JumpToQuestion'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#JumpToQuestion'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].JumpToQuestion = value;
				});
				
				$('#EnableSubsidy_'+index+'_'+i).data('index',i);
				$('#EnableSubsidy_'+index+'_'+i).unbind('click').bind('click',function(){
					var checked = $(this).is(':checked');
					var ans_index = $(this).data('index');
					q.Subsidy[ans_index]['Enable'] = checked?'1':'0';
					if(checked){
						if($('#SubsidyTable_'+index+'_'+ans_index).length == 0){
							$('#SubsidyTableContainer_'+index+'_'+ans_index).html(self.generateSubsidyTableHtml(q,index,ans_index));
							self.bindSubsidyData(q, index, ans_index);
						}
						$('#SubsidyTable_'+index+'_'+ans_index).show();
						if(q.Subsidy[ans_index]['SetIdentity'] != '1' && q.Subsidy[ans_index]['SetStudent'] != '1'){
							q.Subsidy[ans_index]['SetIdentity'] = '1';
							$('#SetIdentity_'+index+'_'+ans_index).attr('checked',true);
							$('#SubsidyIdentityTable_'+index+'_'+ans_index).show();
						}
						$('#Amount'+index+'_'+ans_index).change();
					}else{
						//$('#SubsidyTable_'+index+'_'+ans_index).hide();
						$('#SubsidyTable_'+index+'_'+ans_index).remove();
					}
				});
				
				if(q.Subsidy[i]['Enable'] == 1){
					self.bindSubsidyData(q, index, i);
				}
				
			}
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			for(var i=0;i<q.Options.length;i++){
				
				$('#OptionChecked'+index+'_'+i).data('que-index',index);
				$('#OptionChecked'+index+'_'+i).data('ans-index',i);
				$('#OptionChecked'+index+'_'+i).unbind('click').bind('click',function(){
					var que_index = convertToInteger($(this).data('que-index'));
					var ans_index = convertToInteger($(this).data('ans-index'));
					var is_checked = $(this).is(':checked');
					var value = $(this).val();
					var quantity = Math.max(1, convertToInteger($('#AnswerQuantity'+que_index+'_'+ans_index).val()));
					var new_answer_obj = [];
					var is_in_answer = false;
					for(var j=0;j<q.Answer.length;j++){
						if(q.Answer[j]['Option'] == ans_index){
							is_in_answer = true;
							q.Answer[j]['Quantity'] = quantity;
						}else{
							new_answer_obj.push(q.Answer[j]);
						}
					}
					if(!is_in_answer && is_checked){
						new_answer_obj.push({"Option":ans_index,"Quantity":quantity});
					}
					new_answer_obj.sort(function(a,b){
						return a['Option'] - b['Option'];
					});
					q.Answer = new_answer_obj;
					if(is_checked){
						$('#AnswerQuantityContainer'+que_index+'_'+ans_index).show();
					}else{
						$('#AnswerQuantityContainer'+que_index+'_'+ans_index).hide();
					}
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
				$('#AnswerQuantity'+index+'_'+i).data('que-index',index);
				$('#AnswerQuantity'+index+'_'+i).data('ans-index',i);
				$('#AnswerQuantity'+index+'_'+i).unbind('change').bind('change',function(){
					var que_index = $(this).data('que-index');
					var ans_index = $(this).data('ans-index');
					var quantity = Math.max(1, convertToInteger($(this).val()));
					$('#AnswerQuantity'+que_index+'_'+ans_index).val(quantity);
					$('#OptionChecked'+que_index+'_'+ans_index).attr('data-quantity',quantity);
					for(var j=0;j<q.Answer.length;j++){
						if(q.Answer[j]['Option'] == ans_index){
							q.Answer[j]['Quantity'] = quantity;
						}
					}
					
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
			}
		}
	};
	
	self.bindTextLongQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#JumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.JumpToQuestion = value;
			});
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			$('#Answer'+index).unbind('change').bind('change',function(){
				q.Answer = $.trim($(this).val());
				$(this).val(q.Answer);
				
				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
		}
	};
	
	self.bindTextShortQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#JumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.JumpToQuestion = value;
			});
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			$('#Answer'+index).unbind('change').bind('change',function(){
				q.Answer = $.trim($(this).val());
				$(this).val(q.Answer);

				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
		}
	};
	
	self.bindTrueOrFalseQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#OptionYes'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.OptionYes = value;
			});
			
			$('#OptionNo'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.OptionNo = value;
			});
			
			$('#OptionYesJumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.OptionYesJumpToQuestion = value;
			});
			
			$('#OptionNoJumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.OptionNoJumpToQuestion = value;
			});
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			$('#OptionYesChecked'+index).unbind('click').bind('click',function(){
				var is_checked = $(this).is(':checked');
				q.Answer = is_checked?$(this).val():'';

				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
			
			$('#OptionNoChecked'+index).unbind('click').bind('click',function(){
				var is_checked = $(this).is(':checked');
				q.Answer = is_checked?$(this).val():'';
				
				self.calculateAndDisableSkippedQuestions();
				self.calculateAmountTotal();
			});
		}
	};
	
	self.bindMCSingleQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			for(var i=0;i<q.Options.length;i++){
				
				$('#Option'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Option'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $.trim($(this).val());
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Option = value;
				});
				
				$('#JumpToQuestion'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#JumpToQuestion'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $(this).val();
					var array_index = $(this).data('index');
					q.Options[array_index].JumpToQuestion = value;
				});
			}
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			for(var i=0;i<q.Options.length;i++){
				
				//$('#OptionChecked'+index+'_'+i).data('index',i);
				$('#OptionChecked'+index+'_'+i).unbind('click').bind('click',function(){
					//var array_index = $(this).data('index');
					var is_checked = $(this).is(':checked');
					q.Answer = is_checked?$(this).val():'';
					
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
			}
		}
	};
	
	self.bindMCMultipleQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			for(var i=0;i<q.Options.length;i++){
				
				$('#Option'+index+'_'+i).data('index',i); // i is not block scoped and changing, need to bind to somewhere to save the current value
				$('#Option'+index+'_'+i).unbind('change').bind('change',function(){
					var value = $.trim($(this).val());
					var array_index = $(this).data('index');
					$(this).val(value);
					q.Options[array_index].Option = value;
				});
			}
			
			$('#JumpToQuestion'+index).unbind('change').bind('change',function(){
				var value = $(this).val();
				q.JumpToQuestion = value;
			});
			
			$('#MustSubmit'+index).unbind('change').bind('change',function(){
				var checked = $(this).is(':checked');
				var asterisk_span = $($(this).closest('.question-row').find('.must-submit-asterisk'));
				q.MustSubmit = checked? '1':'0';
				if(checked){
					asterisk_span.show();
				}else{
					asterisk_span.hide();
				}
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
			for(var i=0;i<q.Options.length;i++){
				
				//$('#OptionChecked'+index+'_'+i).data('index',i);
				$('#OptionChecked'+index+'_'+i).unbind('click').bind('click',function(){
					//var array_index = $(this).data('index');
					var is_checked = $(this).is(':checked');
					var value = $(this).val();
					var find_index = q.Answer.indexOf(value);
					if(is_checked){
						if(find_index == -1){
							q.Answer.push(value);
						}
					}else{
						if(find_index != -1){
							q.Answer.splice(find_index,1);
						}
					}
					q.Answer.sort();
					
					self.calculateAndDisableSkippedQuestions();
					self.calculateAmountTotal();
				});
				
			}
		}
	};
	
	self.bindNotApplicableQuestionData = function(q, index)
	{
		var mode = self.getOption('Mode');
		if(mode == EDIT_MODE)
		{
			$('#Title'+index).unbind('change').bind('change',function(){
				var value = $.trim($(this).val());
				$(this).val(value);
				q.Title = value;
			});
			
			$('#delete_btn'+index).unbind('click').bind('click',function(){
				questions.splice(index,1);
				self.writeSheet();
			});
		}else{
		
		}
	};
	
	self.generateQuestionEditableHtml = function(q, index)
	{
		var generators = {};
		generators[QTYPE_MUST_PAY] = self.generateMustPayQuestionEditableHtml;
		generators[QTYPE_WHETHER_TO_PAY] = self.generateWhetherToPayQuestionEditableHtml;
		generators[QTYPE_ONE_PAY_ONE_CAT] = self.generateOnePayOneCatQuestionEditableHtml;
		generators[QTYPE_ONE_PAY_FEW_CAT] = self.generateOnePayFewCatQuestionEditableHtml;
		generators[QTYPE_FEW_PAY_FEW_CAT] = self.generateFewPayFewCatQuestionEditableHtml;
		generators[QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY] = self.generateFewPayFewCatWithInputQuantityQuestionEditableHtml;
		generators[QTYPE_TEXT_LONG] = self.generateTextLongQuestionEditableHtml;
		generators[QTYPE_TEXT_SHORT] = self.generateTextShortQuestionEditableHtml;
		generators[QTYPE_TRUE_OR_FALSE] = self.generateTrueOrFalseQuestionEditableHtml;
		generators[QTYPE_MC_SINGLE] = self.generateMCSingleQuestionEditableHtml;
		generators[QTYPE_MC_MULTIPLE] = self.generateMCMultipleQuestionEditableHtml;
		generators[QTYPE_NOT_APPLICABLE] = self.generateNotApplicableQuestionEditableHtml;
		
		var html = generators[q.Type](q,index);
		return html;
	};
	
	self.bindQuestionData = function(q, index)
	{
		var generators = {};
		generators[QTYPE_MUST_PAY] = self.bindMustPayQuestionData;
		generators[QTYPE_WHETHER_TO_PAY] = self.bindWhetherToPayQuestionData;
		generators[QTYPE_ONE_PAY_ONE_CAT] = self.bindOnePayOneCatQuestionData;
		generators[QTYPE_ONE_PAY_FEW_CAT] = self.bindOnePayFewCatQuestionData;
		generators[QTYPE_FEW_PAY_FEW_CAT] = self.bindFewPayFewCatQuestionData;
		generators[QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY] = self.bindFewPayFewCatWithInputQuantityQuestionData;
		generators[QTYPE_TEXT_LONG] = self.bindTextLongQuestionData;
		generators[QTYPE_TEXT_SHORT] = self.bindTextShortQuestionData;
		generators[QTYPE_TRUE_OR_FALSE] = self.bindTrueOrFalseQuestionData;
		generators[QTYPE_MC_SINGLE] = self.bindMCSingleQuestionData;
		generators[QTYPE_MC_MULTIPLE] = self.bindMCMultipleQuestionData;
		generators[QTYPE_NOT_APPLICABLE] = self.bindNotApplicableQuestionData;
		
		generators[q.Type](q,index);
	};

	self.constructSubsidyPartString = function(subsidy_ary)
	{
		var str = '';
		for(var i=0;i<subsidy_ary.length;i++)
		{
			var subsidy_obj = subsidy_ary[i];
			str += SUBSIDY_SEP;
			str += subsidy_obj.Enable + ',' + subsidy_obj.SetIdentity + ',' + subsidy_obj.SetStudent + SUBSIDY_PART_SEP;
			str += IDENTITIES_SEP;
			for(var key in subsidy_obj.Identities){
				if(subsidy_obj.Identities.hasOwnProperty(key)){
					str += IDENTITY_SEP + key + ',' + subsidy_obj.Identities[key];
				}
			}
			
			str += SUBSIDY_PART_SEP;
			str += STUDENTS_SEP;
			for(var key in subsidy_obj.Students){
				if(subsidy_obj.Students.hasOwnProperty(key)){
					str += STUDENT_SEP + key + ',' + subsidy_obj.Students[key];
				}
			}
		}
		
		return str;
	};
	
	self.constructQuestionString = function()
	{
		var qstr = '';
		for(var i=0;i<questions.length;i++){
			var q = questions[i];
			switch(q.Type)
			{
				case QTYPE_MUST_PAY:
					//q = {"Type":type,"Title":title,"Option":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":""};
					qstr += QUE_SEP + q.Type + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP + OPT_SEP + replaceSpecialChars(q.Option) + TAR_SEP + q.JumpToQuestion + ITEM_SEP + PAYMENTITEMID_SEP + q.PaymentItemID + ITEM_SEP + CATEGORYID_SEP + q.CategoryID + ITEM_SEP + AMT_SEP + formatDisplayNumber(q.Amount) + ITEM_SEP + MSUB_SEP + q.MustSubmit + ITEM_SEP + self.constructSubsidyPartString(q.Subsidy);
				break;
				case QTYPE_WHETHER_TO_PAY:
					//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":mustSubmit,"Answer":""};
					qstr += QUE_SEP + q.Type + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP + OPT_SEP + replaceSpecialChars(q.OptionYes) + TAR_SEP + q.OptionYesJumpToQuestion + OPT_SEP + replaceSpecialChars(q.OptionNo) + TAR_SEP + q.OptionNoJumpToQuestion + ITEM_SEP + PAYMENTITEMID_SEP + q.PaymentItemID + ITEM_SEP + CATEGORYID_SEP + q.CategoryID + ITEM_SEP + AMT_SEP + formatDisplayNumber(q.Amount) + ITEM_SEP + MSUB_SEP + q.MustSubmit + ITEM_SEP + self.constructSubsidyPartString(q.Subsidy);
				break;
				case QTYPE_ONE_PAY_ONE_CAT:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Amount":"","Option":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"CategoryID":"","PaymentItemID":"","Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					qstr += QUE_SEP + q.Type + ',' + q.Options.length + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += OPT_SEP + formatDisplayNumber(q.Options[j].Amount) + ' ' + replaceSpecialChars(q.Options[j].Option) + TAR_SEP + q.Options[j].JumpToQuestion;
					}
					qstr += ITEM_SEP + PAYMENTITEMID_SEP + q.PaymentItemID + ITEM_SEP + CATEGORYID_SEP + q.CategoryID + ITEM_SEP + MSUB_SEP + q.MustSubmit + ITEM_SEP + self.constructSubsidyPartString(q.Subsidy);
				break;
				case QTYPE_ONE_PAY_FEW_CAT:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[]};
					qstr += QUE_SEP + q.Type + ',' + q.Options.length + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += OPT_SEP + replaceSpecialChars(q.Options[j].Option) + TAR_SEP + q.Options[j].JumpToQuestion;
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += PAYMENTITEMID_SEP + q.Options[j].PaymentItemID;
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += CATEGORYID_SEP + q.Options[j].CategoryID;
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += AMT_SEP + formatDisplayNumber(q.Options[j].Amount);
					}
					qstr += ITEM_SEP + MSUB_SEP + q.MustSubmit + ITEM_SEP + self.constructSubsidyPartString(q.Subsidy);
				break;
				case QTYPE_FEW_PAY_FEW_CAT:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[]};
					qstr += QUE_SEP + q.Type + ',' + q.Options.length + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += OPT_SEP + replaceSpecialChars(q.Options[j].Option) + TAR_SEP + q.Options[j].JumpToQuestion;
					}
					qstr += ITEM_SEP + PAYMENTITEMID_SEP;
					var comma = '';
					for(var j=0;j<q.Options.length;j++){
						qstr += comma + q.Options[j].PaymentItemID;
						comma = ',';
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += CATEGORYID_SEP + q.Options[j].CategoryID;
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += AMT_SEP + formatDisplayNumber(q.Options[j].Amount);
					}
					qstr += ITEM_SEP + MSUB_SEP + q.MustSubmit + ITEM_SEP + self.constructSubsidyPartString(q.Subsidy);
				break;
				case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					qstr += QUE_SEP + q.Type + ',' + q.Options.length + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += OPT_SEP + replaceSpecialChars(q.Options[j].Option) + TAR_SEP + q.Options[j].JumpToQuestion;
					}
					qstr += ITEM_SEP + PAYMENTITEMID_SEP;
					var comma = '';
					for(var j=0;j<q.Options.length;j++){
						qstr += comma + q.Options[j].PaymentItemID;
						comma = ',';
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += CATEGORYID_SEP + q.Options[j].CategoryID;
					}
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += AMT_SEP + formatDisplayNumber(q.Options[j].Amount);
					}
					qstr += ITEM_SEP + MSUB_SEP + q.MustSubmit + ITEM_SEP + self.constructSubsidyPartString(q.Subsidy);
					qstr += ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += QUANTITY_SEP + q.Options[j]['MinQuantity'] + ',' + q.Options[j]['MaxQuantity'];
					}
				break;
				case QTYPE_TEXT_LONG:
					//q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
					qstr += QUE_SEP + q.Type + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP + TAR_SEP + q.JumpToQuestion + ITEM_SEP + MSUB_SEP + q.MustSubmit;
				break;
				case QTYPE_TEXT_SHORT:
					//q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
					qstr += QUE_SEP + q.Type + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP + TAR_SEP + q.JumpToQuestion + ITEM_SEP + MSUB_SEP + q.MustSubmit;
				break;
				case QTYPE_TRUE_OR_FALSE:
					//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
					qstr += QUE_SEP + q.Type + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP + OPT_SEP + replaceSpecialChars(q.OptionYes) + TAR_SEP + q.OptionYesJumpToQuestion + OPT_SEP + replaceSpecialChars(q.OptionNo) + TAR_SEP + q.OptionNoJumpToQuestion + ITEM_SEP + MSUB_SEP + q.MustSubmit;
				break;
				case QTYPE_MC_SINGLE:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					qstr += QUE_SEP + q.Type + ',' + q.Options.length + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += OPT_SEP + replaceSpecialChars(q.Options[j].Option) + TAR_SEP + q.Options[j].JumpToQuestion;
					} 
					qstr += ITEM_SEP + MSUB_SEP + q.MustSubmit;
				break;
				case QTYPE_MC_MULTIPLE:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":[]};
					qstr += QUE_SEP + q.Type + ',' + q.Options.length + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP;
					for(var j=0;j<q.Options.length;j++){
						qstr += OPT_SEP + replaceSpecialChars(q.Options[j].Option);
					} 
					qstr += ITEM_SEP + TAR_SEP + q.JumpToQuestion + ITEM_SEP + MSUB_SEP + q.MustSubmit;
				break;
				case QTYPE_NOT_APPLICABLE:
					//q = {"Type":type,"Title":title,"MustSubmit":0};
					qstr += QUE_SEP + q.Type + ITEM_SEP + replaceSpecialChars(q.Title) + ITEM_SEP + MSUB_SEP + q.MustSubmit;
				break;
			}
		}
		
		return qstr;
	};
	
	self.constructAnswerString = function()
	{
		var astr = '';
		for(var i=0;i<questions.length;i++){
			var q = questions[i];
			if(skip_question_indices.indexOf(i) != -1){
				astr += ANS_SEP + ''; // leave answer blank for skipped questions
			}else{
				switch(q.Type)
				{
					case QTYPE_MUST_PAY:
					case QTYPE_WHETHER_TO_PAY:
					case QTYPE_ONE_PAY_ONE_CAT:
					case QTYPE_ONE_PAY_FEW_CAT:
					case QTYPE_TEXT_LONG:
					case QTYPE_TEXT_SHORT:
					case QTYPE_TRUE_OR_FALSE:
					case QTYPE_MC_SINGLE:
					case QTYPE_NOT_APPLICABLE:
						astr += ANS_SEP + $.trim(replaceSpecialChars(q.Answer));
					break;
					case QTYPE_FEW_PAY_FEW_CAT:
					case QTYPE_MC_MULTIPLE:
						astr += ANS_SEP + $.trim(replaceSpecialChars(q.Answer.join(',')));
					break;
					case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
						astr += ANS_SEP;
						var ans_opt_sep = '';
						for(var j=0;j<q.Answer.length;j++){
							astr += ans_opt_sep + q.Answer[j]['Option'] + '_' + q.Answer[j]['Quantity'];
							ans_opt_sep = ',';
						}
					break;
				}
			}
		}
		
		return astr;
	};
	
	self.parseSubsidyPartString = function(str)
	{
		var tmp_subsidy_ary = str.split(SUBSIDY_SEP);
		tmp_subsidy_ary.shift();
		
		var subsidy_ary = [];
		for(var k=0;k<tmp_subsidy_ary.length;k++){
			var subsidy_obj = {"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}};
			var tmp_subsidy_parts = tmp_subsidy_ary[k].split(SUBSIDY_PART_SEP);
			var tmp_enable_parts = tmp_subsidy_parts[0].split(',');
			subsidy_obj.Enable = tmp_enable_parts[0];
			subsidy_obj.SetIdentity = tmp_enable_parts[1];
			subsidy_obj.SetStudent = tmp_enable_parts[2];
			var identities_parts = tmp_subsidy_parts[1].split(IDENTITIES_SEP);
			if(identities_parts.length > 0){
				identities_parts.shift();
			}
			var students_parts = tmp_subsidy_parts[2].split(STUDENTS_SEP);
			if(students_parts.length > 0){
				students_parts.shift();
			}
			
			for(var i=0;i<identities_parts.length;i++){
				var identity_ary = identities_parts[i].split(IDENTITY_SEP);
				if(identity_ary.length > 0){
					identity_ary.shift();
					for(var j=0;j<identity_ary.length;j++){
						var identity_id_amount = identity_ary[j].split(',');
						var tmp_id = convertToEmptyStringOrInteger(identity_id_amount[0]);
						var tmp_amount = formatDisplayNumber(identity_id_amount[1]);
						subsidy_obj.Identities[tmp_id] = tmp_amount;
					}
				}
			}
			
			for(var i=0;i<students_parts.length;i++){
				var student_ary = students_parts[i].split(STUDENT_SEP);
				if(student_ary.length > 0){
					student_ary.shift();
					for(var j=0;j<student_ary.length;j++){
						var student_id_amount = student_ary[j].split(',');
						var tmp_id = convertToEmptyStringOrInteger(student_id_amount[0]);
						var tmp_amount = formatDisplayNumber(student_id_amount[1]);
						subsidy_obj.Students[tmp_id] = tmp_amount;
					}
				}
			}
			
			subsidy_ary.push(subsidy_obj);
		}
		
		return subsidy_ary;
	};
	
	self.parseQuestionString = function(qstr)
	{
		var is_template = self.getOption('IsTemplate');
		var questions_ary = [];
		
		var ques = qstr.split(QUE_SEP);
		if(ques.length > 0){
			ques.shift();
		}
		for(var i=0;i<ques.length;i++){
			var parts = ques[i].split(ITEM_SEP);
			if(parts.length==0){
				continue;
			}
			var type_opt_num = parts[0].split(',');
			var type = type_opt_num[0];
			switch(type)
			{
				case QTYPE_MUST_PAY:
					var q = {"Type":type,"Title":"","Option":"","JumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":"","Subsidy":[{"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}}]};
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_option = parts[2].split(OPT_SEP);
					tmp_option.shift();
					var opt_tar = tmp_option[0].split(TAR_SEP);
					q.Option = reverseReplaceSpecialChars(opt_tar[0]);
					if(opt_tar.length > 1){
						q.JumpToQuestion = opt_tar[1];
					}
					var tmp_paymentitemid = parts[3].split(PAYMENTITEMID_SEP);
					tmp_paymentitemid.shift();
					q.PaymentItemID = is_template? '' : convertToEmptyStringOrInteger(tmp_paymentitemid[0]);
					var tmp_categoryid = parts[4].split(CATEGORYID_SEP);
					tmp_categoryid.shift();
					q.CategoryID = convertToEmptyStringOrInteger(tmp_categoryid[0]);
					var tmp_amount = parts[5].split(AMT_SEP);
					tmp_amount.shift();
					q.Amount = formatDisplayNumber(tmp_amount[0]);
					if(parts.length > 6 && $.trim(parts[6]) != '')
					{
						var tmp_msub = parts[6].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					if(parts.length > 7 && $.trim(parts[7]) != '')
					{
						q.Subsidy = self.parseSubsidyPartString(parts[7]);						
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_WHETHER_TO_PAY:
					var q = {"Type":type,"Title":"","OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":"","Subsidy":[{"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}}]};
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					var yes_opt_tar = tmp_options[0].split(TAR_SEP);
					q.OptionYes = reverseReplaceSpecialChars(yes_opt_tar[0]);
					if(yes_opt_tar.length > 1){
						q.OptionYesJumpToQuestion = yes_opt_tar[1];
					}
					var no_opt_tar = tmp_options[1].split(TAR_SEP);
					q.OptionNo = reverseReplaceSpecialChars(no_opt_tar[0]);
					if(no_opt_tar.length > 1){
						q.OptionNoJumpToQuestion = no_opt_tar[1];
					}
					var tmp_paymentitemid = parts[3].split(PAYMENTITEMID_SEP);
					tmp_paymentitemid.shift();
					q.PaymentItemID = is_template? '' : convertToEmptyStringOrInteger(tmp_paymentitemid[0]);
					var tmp_categoryid = parts[4].split(CATEGORYID_SEP);
					tmp_categoryid.shift();
					q.CategoryID = convertToEmptyStringOrInteger(tmp_categoryid[0]);
					var tmp_amount = parts[5].split(AMT_SEP);
					tmp_amount.shift();
					q.Amount = formatDisplayNumber(tmp_amount[0]);
					if(parts.length > 6 && $.trim(parts[6]) != '')
					{
						var tmp_msub = parts[6].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					if(parts.length > 7 && $.trim(parts[7]) != '')
					{
						q.Subsidy = self.parseSubsidyPartString(parts[7]);						
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_ONE_PAY_ONE_CAT:
					var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Amount":"","Option":"","JumpToQuestion":""});
					//}
					var q = {"Type":type,"Title":"","CategoryID":"","PaymentItemID":"","Options":[],"MustSubmit":1,"Answer":"","Subsidy":[]};
					var num_of_opt = type_opt_num[1];
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					for(var j=0;j<tmp_options.length;j++){
						var tmp_opt_tar = tmp_options[j].split(TAR_SEP);
						var tmp_number_text = extractNumberText(tmp_opt_tar[0]);
						var tmp_opt_obj = {"Amount":formatDisplayNumber(tmp_number_text[0]),"Option":$.trim(reverseReplaceSpecialChars(tmp_number_text[1])),"JumpToQuestion":""};
						if(tmp_opt_tar.length > 1){
							tmp_opt_obj.JumpToQuestion = tmp_opt_tar[1];
						}
						option_ary.push(tmp_opt_obj);
					}
					q.Options = option_ary;
					var tmp_paymentitemid = parts[3].split(PAYMENTITEMID_SEP);
					tmp_paymentitemid.shift();
					q.PaymentItemID = is_template? '' : convertToEmptyStringOrInteger(tmp_paymentitemid[0]);
					var tmp_categoryid = parts[4].split(CATEGORYID_SEP);
					tmp_categoryid.shift();
					q.CategoryID = convertToEmptyStringOrInteger(tmp_categoryid[0]);
					if(parts.length > 5 && $.trim(parts[5]) != '')
					{
						var tmp_msub = parts[5].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					if(parts.length > 6 && $.trim(parts[6]) != '')
					{
						q.Subsidy = self.parseSubsidyPartString(parts[6]);						
					}else{
						for(var j=0;j<q.Options.length;j++){
							q.Subsidy.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
						}
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_ONE_PAY_FEW_CAT:
					var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					//}
					var q = {"Type":type,"Title":"","Options":[],"MustSubmit":1,"Answer":"","Subsidy":[]};
					var num_of_opt = type_opt_num[1];
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					var tmp_paymentitemid = parts[3].split(PAYMENTITEMID_SEP);
					tmp_paymentitemid.shift();
					var tmp_categoryid = parts[4].split(CATEGORYID_SEP);
					tmp_categoryid.shift();
					var tmp_amount = parts[5].split(AMT_SEP);
					tmp_amount.shift();
					for(var j=0;j<tmp_options.length;j++){
						var tmp_opt_tar = tmp_options[j].split(TAR_SEP);
						var tmp_opt_obj = {"Option":reverseReplaceSpecialChars(tmp_opt_tar[0]),"CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""};
						if(tmp_opt_tar.length > 1){
							tmp_opt_obj.JumpToQuestion = tmp_opt_tar[1];
						}
						tmp_opt_obj.PaymentItemID = is_template? '' : convertToEmptyStringOrInteger(tmp_paymentitemid[j]);
						tmp_opt_obj.CategoryID = convertToEmptyStringOrInteger(tmp_categoryid[j]);
						tmp_opt_obj.Amount = formatDisplayNumber(tmp_amount[j]);
						option_ary.push(tmp_opt_obj);
					}
					q.Options = option_ary;
					if(parts.length > 6 && $.trim(parts[6]) != '')
					{
						var tmp_msub = parts[6].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					if(parts.length > 7 && $.trim(parts[7]) != '')
					{
						q.Subsidy = self.parseSubsidyPartString(parts[7]);						
					}else{
						for(var j=0;j<q.Options.length;j++){
							q.Subsidy.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
						}
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_FEW_PAY_FEW_CAT:
				case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					//}
					var q = {"Type":type,"Title":"","Options":[],"MustSubmit":1,"Answer":[],"Subsidy":[]};
					var num_of_opt = type_opt_num[1];
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					var tmp_paymentitemid = parts[3].split(PAYMENTITEMID_SEP);
					tmp_paymentitemid.shift();
					var tmp_paymentitemid_ary = tmp_paymentitemid[0].split(',');
					var tmp_categoryid = parts[4].split(CATEGORYID_SEP);
					tmp_categoryid.shift();
					var tmp_amount = parts[5].split(AMT_SEP);
					tmp_amount.shift();
					for(var j=0;j<tmp_options.length;j++){
						var tmp_opt_tar = tmp_options[j].split(TAR_SEP);
						var tmp_opt_obj = {"Option":reverseReplaceSpecialChars(tmp_opt_tar[0]),"CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""};
						if(tmp_opt_tar.length > 1){
							tmp_opt_obj.JumpToQuestion = tmp_opt_tar[1];
						}
						tmp_opt_obj.PaymentItemID = is_template? '' : convertToEmptyStringOrInteger(tmp_paymentitemid_ary[j]);
						tmp_opt_obj.CategoryID = convertToEmptyStringOrInteger(tmp_categoryid[j]);
						tmp_opt_obj.Amount = formatDisplayNumber(tmp_amount[j]);
						option_ary.push(tmp_opt_obj);
					}
					q.Options = option_ary;
					if(parts.length > 6 && $.trim(parts[6]) != '')
					{
						var tmp_msub = parts[6].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0] == '1'? '1' : '0';
					}
					if(parts.length > 7 && $.trim(parts[7]) != '')
					{
						q.Subsidy = self.parseSubsidyPartString(parts[7]);						
					}else{
						for(var j=0;j<q.Options.length;j++){
							q.Subsidy.push({"Enable":0,"SetIdentity":0,"SetStudent":0,"Identities":{},"Students":{}});
						}
					}
					if(type == QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY)
					{ 
						if(parts.length > 8 && $.trim(parts[8]) != ''){
							var tmp_quantity = parts[8].split(QUANTITY_SEP);
							tmp_quantity.shift();
							for(var j=0;j<q.Options.length;j++){
								var tmp_quantity_range = tmp_quantity[j].split(',');
								q.Options[j]['MinQuantity'] = tmp_quantity_range[0];
								q.Options[j]['MaxQuantity'] = tmp_quantity_range[1];
							}
						}else{
							for(var j=0;j<q.Options.length;j++){
								q.Options[j]['MinQuantity'] = 1;
								q.Options[j]['MaxQuantity'] = 100;
							}
						}
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_TEXT_LONG:
				case QTYPE_TEXT_SHORT:
					var q = {"Type":type,"Title":"","JumpToQuestion":"","MustSubmit":1,"Answer":""};
					q.Title = reverseReplaceSpecialChars(parts[1]);
					if(parts.length > 2){
						var tmp_tar = parts[2].split(TAR_SEP);
						tmp_tar.shift();
						q.JumpToQuestion = tmp_tar[0];
					}
					if(parts.length > 3 && $.trim(parts[3]) != '')
					{
						var tmp_msub = parts[3].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_TRUE_OR_FALSE:
					var q = {"Type":type,"Title":"","OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","MustSubmit":1,"Answer":""};
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					var yes_opt_tar = tmp_options[0].split(TAR_SEP);
					q.OptionYes = reverseReplaceSpecialChars(yes_opt_tar[0]);
					q.OptionYesJumpToQuestion = yes_opt_tar[1];
					var no_opt_tar = tmp_options[1].split(TAR_SEP);
					q.OptionNo = reverseReplaceSpecialChars(no_opt_tar[0]);
					q.OptionNoJumpToQuestion = no_opt_tar[1];
					if(parts.length > 3 && $.trim(parts[3]) != '')
					{
						var tmp_msub = parts[3].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_MC_SINGLE:
					var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","JumpToQuestion":""});
					//}
					var q = {"Type":type,"Title":"","Options":[],"MustSubmit":1,"Answer":""};
					var num_of_opt = type_opt_num[1];
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					for(var j=0;j<tmp_options.length;j++){
						var tmp_opt_tar = tmp_options[j].split(TAR_SEP);
						var tmp_opt_obj = {"Option":reverseReplaceSpecialChars(tmp_opt_tar[0]),"JumpToQuestion":""};
						if(tmp_opt_tar.length > 1){
							tmp_opt_obj.JumpToQuestion = tmp_opt_tar[1];
						}
						option_ary.push(tmp_opt_obj);
					}
					q.Options = option_ary;
					if(parts.length > 3 && $.trim(parts[3]) != '')
					{
						var tmp_msub = parts[3].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_MC_MULTIPLE:
					var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":""});
					//}
					var q = {"Type":type,"Title":"","Options":[],"JumpToQuestion":"","MustSubmit":1,"Answer":[]};
					var num_of_opt = type_opt_num[1];
					q.Title = reverseReplaceSpecialChars(parts[1]);
					var tmp_options = parts[2].split(OPT_SEP);
					tmp_options.shift();
					for(var j=0;j<tmp_options.length;j++){
						var tmp_opt_obj = {"Option":reverseReplaceSpecialChars(tmp_options[j])};
						option_ary.push(tmp_opt_obj);
					}
					q.Options = option_ary;
					if(parts.length > 3){
						var tmp_tar = parts[3].split(TAR_SEP);
						tmp_tar.shift();
						q.JumpToQuestion = tmp_tar[0];
					}
					if(parts.length > 4 && $.trim(parts[4]) != '')
					{
						var tmp_msub = parts[4].split(MSUB_SEP);
						tmp_msub.shift();
						q.MustSubmit = tmp_msub[0];
					}
					questions_ary.push(q);
					
				break;
				case QTYPE_NOT_APPLICABLE:
					var q = {"Type":type,"Title":"","MustSubmit":0,"Answer":""};
					q.Title = reverseReplaceSpecialChars(parts[1]);
					
					questions_ary.push(q);
					
				break;				
			}
		}
		
		return questions_ary;
	};
	
	self.parseAnswerString = function(astr)
	{
		var answers = astr.split(ANS_SEP);
		if(answers.length > 0){
			answers.shift();
		}
		for(var i=0;i<questions.length;i++){
			var q = questions[i];
			
			switch(q.Type)
			{
				case QTYPE_MUST_PAY:
				case QTYPE_WHETHER_TO_PAY:
				case QTYPE_ONE_PAY_ONE_CAT:
				case QTYPE_ONE_PAY_FEW_CAT:
				case QTYPE_TEXT_LONG:
				case QTYPE_TEXT_SHORT:
				case QTYPE_TRUE_OR_FALSE:
				case QTYPE_MC_SINGLE:
				case QTYPE_NOT_APPLICABLE:
					if(answers[i]){
						q.Answer = reverseReplaceSpecialChars(answers[i]);
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT:
				case QTYPE_MC_MULTIPLE:
					if(answers[i]){
						q.Answer = answers[i].split(',');
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					if(answers[i]){
						var ans_opt_ary = answers[i].split(',');
						q.Answer = [];
						for(var j=0;j<ans_opt_ary.length;j++){
							var ans_opt_ary2 = ans_opt_ary[j].split('_');
							q.Answer.push({"Option":ans_opt_ary2[0],"Quantity":ans_opt_ary2[1]});
						}
					}
				break;
			}
		}
	};
	
	self.validateEditForm = function(){
		var is_all_valid = true;
		var str_please_complete_this_question = self.getOption('StrPleaseCompleteThisQuestion');
		var str_please_complete_all_questions = self.getOption('StrPleaseCompleteAllQuestions');
		var str_please_complete_this_subsidy_settings = self.getOption('StrPleaseCompleteThisSubsidySettings');
		var str_please_input_valid_quantity = self.getOption('StrPleaseInputValidQuantity');
		$('.error').html('').hide();
		//$('input.identity-amount').change();
		//$('input.student-amount').change();
		var identity_amount_elements = $('input.identity-amount');
		for(var i=0;i<identity_amount_elements.length;i++){
			var elem_id = identity_amount_elements[i].id;
			var parts = elem_id.split('_');
			var que_index = parts[1];
			var ans_index = parts[2];
			var identity_id = parts[3];
			var amount_value = identity_amount_elements[i].value == ''? '' : Math.max(0, formatDisplayNumber(identity_amount_elements[i].value));
			questions[que_index]['Subsidy'][ans_index]['Identities'][identity_id] = amount_value;
			identity_amount_elements[i].value = amount_value;
		}
		var student_amount_elements = $('input.student-amount');
		for(var i=0;i<student_amount_elements.length;i++){
			var elem_id = student_amount_elements[i].id;
			var parts = elem_id.split('_');
			var que_index = parts[1];
			var ans_index = parts[2];
			var student_id = parts[3];
			var amount_value = student_amount_elements[i].value == ''? '' : Math.max(0, formatDisplayNumber(student_amount_elements[i].value));
			questions[que_index]['Subsidy'][ans_index]['Students'][student_id] = amount_value;
			student_amount_elements[i].value = amount_value;
		}
		for(var i=0;i<questions.length;i++){
			var q = questions[i];
			
			switch(q.Type)
			{
				case QTYPE_MUST_PAY:
					//q = {"Type":type,"Title":title,"Option":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":1,"Answer":""};
					if(q.Title == '' || q.Option == '' || q.CategoryID == '' || q.Amount === ''){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
					if(q.Subsidy[0]['Enable'] == 1){
						if(q.Subsidy[0]['SetIdentity'] != 1 && q.Subsidy[0]['SetStudent'] != 1){
							is_all_valid = false;
							$('#Error'+i).html(str_please_complete_this_subsidy_settings).show();
						}
						if(q.Subsidy[0]['SetIdentity'] == 1){
							var subsidy_settings_not_completed = false;
							for(var key in q.Subsidy[0]['Identities']){
								if(q.Subsidy[0]['Identities'].hasOwnProperty(key)){
									if(q.Subsidy[0]['Identities'][key] === ''){
										subsidy_settings_not_completed = true;
										break;
									}
								}
							}
							if(subsidy_settings_not_completed){
								is_all_valid = false;
								$('#SubsidyError_'+i+'_0').html(str_please_complete_this_subsidy_settings).show();
							}
						}
						if(q.Subsidy[0]['SetStudent'] == 1){
							var subsidy_settings_not_completed = false;
							for(var key in q.Subsidy[0]['Students']){
								if(q.Subsidy[0]['Students'].hasOwnProperty(key)){
									if(q.Subsidy[0]['Students'][key] === ''){
										subsidy_settings_not_completed = true;
										break;
									}
								}
							}
							if(subsidy_settings_not_completed){
								is_all_valid = false;
								$('#SubsidyError_'+i+'_0').html(str_please_complete_this_subsidy_settings).show();
							}
						}
					}
				break;
				case QTYPE_WHETHER_TO_PAY:
					//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","CategoryID":"","PaymentItemID":"","Amount":0,"MustSubmit":mustSubmit,"Answer":""};
					if(q.Title == '' || q.OptionYes == '' || q.OptionNo == '' || q.CategoryID == '' || q.Amount === ''){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
					if(q.Subsidy[0]['Enable'] == 1){
						if(q.Subsidy[0]['SetIdentity'] != 1 && q.Subsidy[0]['SetStudent'] != 1){
							is_all_valid = false;
							$('#Error'+i).html(str_please_complete_this_subsidy_settings).show();
						}
						if(q.Subsidy[0]['SetIdentity'] == 1){
							var subsidy_settings_not_completed = false;
							for(var key in q.Subsidy[0]['Identities']){
								if(q.Subsidy[0]['Identities'].hasOwnProperty(key)){
									if(q.Subsidy[0]['Identities'][key] === ''){
										subsidy_settings_not_completed = true;
										break;
									}
								}
							}
							if(subsidy_settings_not_completed){
								is_all_valid = false;
								$('#SubsidyError_'+i+'_0').html(str_please_complete_this_subsidy_settings).show();
							}
						}
						if(q.Subsidy[0]['SetStudent'] == 1){
							var subsidy_settings_not_completed = false;
							for(var key in q.Subsidy[0]['Students']){
								if(q.Subsidy[0]['Students'].hasOwnProperty(key)){
									if(q.Subsidy[0]['Students'][key] === ''){
										subsidy_settings_not_completed = true;
										break;
									}
								}
							}
							if(subsidy_settings_not_completed){
								is_all_valid = false;
								$('#SubsidyError_'+i+'_0').html(str_please_complete_this_subsidy_settings).show();
							}
						}
					}
				break;
				case QTYPE_ONE_PAY_ONE_CAT:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Amount":"","Option":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"CategoryID":"","PaymentItemID":"","Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					var is_all_options_valid = true;
					for(var j=0;j<q.Options.length;j++){
						if(q.Options[j].Amount === ''){
							is_all_options_valid = false;
						}
					}
					if(q.Title == '' || q.CategoryID == '' || !is_all_options_valid){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
					for(var j=0;j<q.Options.length;j++){
						if(q.Subsidy[j]['Enable'] == 1){
							if(q.Subsidy[j]['SetIdentity'] != 1 && q.Subsidy[j]['SetStudent'] != 1){
								is_all_valid = false;
								$('#Error'+i).html(str_please_complete_this_subsidy_settings).show();
							}
							if(q.Subsidy[j]['SetIdentity'] == 1){
								var subsidy_settings_not_completed = false;
								for(var key in q.Subsidy[j]['Identities']){
									if(q.Subsidy[j]['Identities'].hasOwnProperty(key)){
										if(q.Subsidy[j]['Identities'][key] === ''){
											subsidy_settings_not_completed = true;
											break;
										}
									}
								}
								if(subsidy_settings_not_completed){
									is_all_valid = false;
									$('#SubsidyError_'+i+'_'+j).html(str_please_complete_this_subsidy_settings).show();
								}
							}
							if(q.Subsidy[j]['SetStudent'] == 1){
								var subsidy_settings_not_completed = false;
								for(var key in q.Subsidy[j]['Students']){
									if(q.Subsidy[j]['Students'].hasOwnProperty(key)){
										if(q.Subsidy[j]['Students'][key] === ''){
											subsidy_settings_not_completed = true;
											break;
										}
									}
								}
								if(subsidy_settings_not_completed){
									is_all_valid = false;
									$('#SubsidyError_'+i+'_'+j).html(str_please_complete_this_subsidy_settings).show();
								}
							}
						}
					}
				break;
				case QTYPE_ONE_PAY_FEW_CAT:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					var is_all_options_valid = true;
					for(var j=0;j<q.Options.length;j++){
						if(q.Options[j].Option == '' || q.Options[j].CategoryID == '' || q.Options[j].Amount === ''){
							is_all_options_valid = false;
						}
					}
					if(q.Title == '' || !is_all_options_valid){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
					for(var j=0;j<q.Options.length;j++){
						if(q.Subsidy[j]['Enable'] == 1){
							if(q.Subsidy[j]['SetIdentity'] != 1 && q.Subsidy[j]['SetStudent'] != 1){
								is_all_valid = false;
								$('#Error'+i).html(str_please_complete_this_subsidy_settings).show();
							}
							if(q.Subsidy[j]['SetIdentity'] == 1){
								var subsidy_settings_not_completed = false;
								for(var key in q.Subsidy[j]['Identities']){
									if(q.Subsidy[j]['Identities'].hasOwnProperty(key)){
										if(q.Subsidy[j]['Identities'][key] === ''){
											subsidy_settings_not_completed = true;
											break;
										}
									}
								}
								if(subsidy_settings_not_completed){
									is_all_valid = false;
									$('#SubsidyError_'+i+'_'+j).html(str_please_complete_this_subsidy_settings).show();
								}
							}
							if(q.Subsidy[j]['SetStudent'] == 1){
								var subsidy_settings_not_completed = false;
								for(var key in q.Subsidy[j]['Students']){
									if(q.Subsidy[j]['Students'].hasOwnProperty(key)){
										if(q.Subsidy[j]['Students'][key] === ''){
											subsidy_settings_not_completed = true;
											break;
										}
									}
								}
								if(subsidy_settings_not_completed){
									is_all_valid = false;
									$('#SubsidyError_'+i+'_'+j).html(str_please_complete_this_subsidy_settings).show();
								}
							}
						}
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT:
				case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","CategoryID":"","PaymentItemID":"","Amount":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":[]};
					var is_all_options_valid = true;
					for(var j=0;j<q.Options.length;j++){
						if(q.Options[j].Option == '' || q.Options[j].CategoryID == '' || q.Options[j].Amount === ''){
							is_all_options_valid = false;
						}
					}
					if(q.Title == '' || !is_all_options_valid){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
					for(var j=0;j<q.Options.length;j++){
						if(q.Subsidy[j]['Enable'] == 1){
							if(q.Subsidy[j]['SetIdentity'] != 1 && q.Subsidy[j]['SetStudent'] != 1){
								is_all_valid = false;
								$('#Error'+i).html(str_please_complete_this_subsidy_settings).show();
							}
							if(q.Subsidy[j]['SetIdentity'] == 1){
								var subsidy_settings_not_completed = false;
								for(var key in q.Subsidy[j]['Identities']){
									if(q.Subsidy[j]['Identities'].hasOwnProperty(key)){
										if(q.Subsidy[j]['Identities'][key] === ''){
											subsidy_settings_not_completed = true;
											break;
										}
									}
								}
								if(subsidy_settings_not_completed){
									is_all_valid = false;
									$('#SubsidyError_'+i+'_'+j).html(str_please_complete_this_subsidy_settings).show();
								}
							}
							if(q.Subsidy[j]['SetStudent'] == 1){
								var subsidy_settings_not_completed = false;
								for(var key in q.Subsidy[j]['Students']){
									if(q.Subsidy[j]['Students'].hasOwnProperty(key)){
										if(q.Subsidy[j]['Students'][key] === ''){
											subsidy_settings_not_completed = true;
											break;
										}
									}
								}
								if(subsidy_settings_not_completed){
									is_all_valid = false;
									$('#SubsidyError_'+i+'_'+j).html(str_please_complete_this_subsidy_settings).show();
								}
							}
						}
						
						if(q.Type == QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY){
							if(q.Subsidy[j]['MinQuantity'] == '' || q.Subsidy[j]['MaxQuantity'] == '' || q.Subsidy[j]['MinQuantity'] <=0 || q.Subsidy[j]['MaxQuantity'] <= 0 || q.Subsidy[j]['MinQuantity'] > q.Subsidy[j]['MaxQuantity']){
								is_all_valid = false;
								$('#Error'+i).html(str_please_input_valid_quantity).show();
							}
						}
					}
				break;
				case QTYPE_TEXT_LONG:
				case QTYPE_TEXT_SHORT:
					//q = {"Type":type,"Title":title,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
					if(q.Title == ''){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}else{
						$('#Error'+i).html('').hide();
					}
				break;
				case QTYPE_TRUE_OR_FALSE:
					//q = {"Type":type,"Title":title,"OptionYes":"","OptionYesJumpToQuestion":"","OptionNo":"","OptionNoJumpToQuestion":"","MustSubmit":mustSubmit,"Answer":""};
					if(q.Title == '' || q.OptionYes == '' || q.OptionNo == ''){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
				break;
				case QTYPE_MC_SINGLE:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":"","JumpToQuestion":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"MustSubmit":mustSubmit,"Answer":""};
					var is_all_options_valid = true;
					for(var j=0;j<q.Options.length;j++){
						if(q.Options[j].Option == ''){
							is_all_options_valid = false;
						}
					}
					if(q.Title == '' || !is_all_options_valid){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
				break;
				case QTYPE_MC_MULTIPLE:
					//var option_ary = [];
					//for(var i=0;i<optionNo;i++){
					//	option_ary.push({"Option":""});
					//}
					//q = {"Type":type,"Title":title,"Options":option_ary,"JumpToQuestion":"","MustSubmit":mustSubmit,"Answer":[]};
					var is_all_options_valid = true;
					for(var j=0;j<q.Options.length;j++){
						if(q.Options[j].Option == ''){
							is_all_options_valid = false;
						}
					}
					if(q.Title == '' || !is_all_options_valid){
						is_all_valid = false;
						$('#Error'+i).html(str_please_complete_this_question).show();
					}
				break;
				case QTYPE_NOT_APPLICABLE:
					//q = {"Type":type,"Title":title,"MustSubmit":0};
				break;
			}
		}
		
		return is_all_valid;
	};
	
	self.validateFillInForm = function()
	{
		var is_all_valid = true;
		var str_please_answer_this_question = self.getOption('StrPleaseAnswerThisQuestion');
		var str_please_input_valid_quantity = self.getOption('StrPleaseInputValidQuantity');
		$('.error').html('').hide();
		self.calculateAndDisableSkippedQuestions();
		self.calculateAmountTotal();
		for(var i=0;i<questions.length;i++){
			var q = questions[i];
			if(skip_question_indices.indexOf(i) != -1){
				// do not check skipped question
				continue;
			}
			switch(q.Type)
			{
				case QTYPE_MUST_PAY:
				case QTYPE_WHETHER_TO_PAY:
				case QTYPE_ONE_PAY_ONE_CAT:
				case QTYPE_ONE_PAY_FEW_CAT:
				case QTYPE_TEXT_LONG:
				case QTYPE_TEXT_SHORT:
				case QTYPE_TRUE_OR_FALSE:
				case QTYPE_MC_SINGLE:
				case QTYPE_NOT_APPLICABLE:
					if(q.MustSubmit == 1 && q.Answer == ''){
						is_all_valid = false;
						$('#Error'+i).html(str_please_answer_this_question).show();
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT:
				case QTYPE_MC_MULTIPLE:
					if(q.MustSubmit == 1 && q.Answer.length == 0){
						is_all_valid = false;
						$('#Error'+i).html(str_please_answer_this_question).show();
					}
				break;
				case QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY:
					if(q.MustSubmit == 1 && q.Answer.length == 0){
						is_all_valid = false;
						$('#Error'+i).html(str_please_answer_this_question).show();
					}
					if(q.Answer.length > 0){
						for(var j=0;j<q.Answer.length;j++){
							if(q.Answer[j]['Quantity'] <= 0){
								is_all_valid = false;
								$('#Error'+i).html(str_please_input_valid_quantity).show();
							}
						}
					}
				break;
			}
		}
		
		return is_all_valid;
	};

	self.disableQuestionInWebview = function(row_obj)
	{
		// apply timeout to prevent JS error related to checkboxradio('refresh')
		setTimeout(function() {
			row_obj.find('.answer').attr('disabled', true).attr('checked', false).checkboxradio('refresh');
			self.calculateAmountTotal();
		}, 10);
	};
}