// using by : 

////////////////////// Change Log ////
//
//	Date:	2018-09-28 Bill
//		support new payment question type - Must Pay
//
//	Date:	2017-01-18 Bill
//		modified viewForm(), analysisData(), fixed js error when reply slip has long question
//
//	Date:	2011-02-24 YatWoon
//		add DisplayQuestionNumber checking, if DisplayQuestionNumber==1 display question number before the question
//
////////////////////////////////////

function viewForm(txtStr, txtAns){
	var txtArr=txtStr.split("#QUE#");
	ansArr=txtAns.split("#ANS#");
	var tmpArr = null;
	var txtStr = null;

	txtStr='<Form name="answersheet">';
        txtStr+='<table width="100%" border="0" cellpadding="0" cellspacing="5">';
	for (var x=1; x<txtArr.length; x++){
		j = x - 1;
		tmpArr = txtArr[x].split("|");
		type_no = tmpArr[0].split(",");			//0:type, 1:number of options
		question = tmpArr[1];
		opts = tmpArr[2];

		if(type_no[0]!=10) {
			tmpArr = opts.split("#OPT#");
		}

        txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
		txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
		txtStr+='</td></tr>\n';
                
		txtStr+='<table border="0" width="100%"><tr><td class="tabletext" colspan="2">\n';
		txtStr+=question+'</td></tr>\n';
                txtStr+='<td class="tabletext">&nbsp;</td>';
		txtStr+='<td width="95%" class="tabletext">';

		switch (type_no[0]){
			case "1":
			case "2":
			case "12":
				txtStr += (isNaN(parseInt(ansArr[x]))) ? "--" : tmpArr[parseInt(ansArr[x])+1];
				break;
			case "3":
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];					    
						c_i_i = 1;
					}
				}
				break;
			case "4":
			case "5":
				txtStr += ansArr[x];
				break;
			case "6":
				break;
			case "7":
				break;	
			case "8":
				break;
			case "9":
				checkAns = ansArr[x].split(",");
				c_i_i = 0
				for (var m=0; m<checkAns.length; m++) {

					if (isNaN(parseInt(checkAns[m]))) {
						txtStr += "--";
					}
					else
					{
						txtStr += (c_i_i>0) ? "<br><br>"+tmpArr[parseInt(checkAns[m])+1] : tmpArr[parseInt(checkAns[m])+1];					    
						c_i_i = 1;
					}
				}
				break;
			
		}
		txtStr+=(type_no[0]=="6") ? '</td>' : '</td>';
		txtStr+='</table>\n';
                txtStr+='</td></tr>';
                
	}

        txtStr+='</table>';
	txtStr+='</Form>';

	return txtStr;
}


function switchLayer(layer_name, j){
	var hide = "hidden";
	var show = "visible";

	if (!document.all)
		return;

	var co=(document.getElementById && !document.all)? document.getElementById(layer_name).style : document.all[layer_name].style;

	if (co.visibility==show){
		co.visibility = hide;
		co.position  = "absolute";
                eval("document.all.sb"+j+".innerHTML='" + w_show_details+"';");
	}
	else {
		co.visibility = show;
		co.position  = "relative";
                eval("document.all.sb"+j+".innerHTML='" + w_hide_details+"';");
	}
}



/*################################## STATISTICS ####################################*/
function Statistics()
{
	if (typeof(_statistics_prototype_called) == 'undefined')
	{
		 _statistics_prototype_called = true;
		 Statistics.prototype.qString = null;
		 Statistics.prototype.total = 0;
		 Statistics.prototype.answer = new Array();
		 Statistics.prototype.data = new Array();
		 Statistics.prototype.analysisData = analysisData;
		 Statistics.prototype.getStats = getStats;
		 Statistics.prototype.existPos = existPos;
	}

	function getStats() {
		var resultArr = new Array();
		var txtArr = this.qString.split("#QUE#");
		var dataArr = this.data;
		var tmpArr = null;
		var txtStr = null;

		txtStr='<Form name="answersheet">';
                
                txtStr+='<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">\n';
                txtStr+='<tr><td align="left" class="eSportprinttitle">'+w_total_no+' : '+this.total+'</td></tr>\n';
                txtStr+='</table>';
                
                txtStr+='<table width="90%" border="0" cellpadding="0" cellspacing="5" align="center">\n';
		for (var x=1; x<txtArr.length; x++){
			j = x - 1;
			tmpArr = txtArr[x].split("||");
			type_no = tmpArr[0].split(",");			//0:type, 1:number of options
			question = tmpArr[1];
			if(question=="")	question = "&nbsp;";
		
                	txtStr+='<tr><td align="center">\n';
			txtStr+='<table width="100%" border="0" cellspacing="0" cellpadding="2" class="eSporttableborder">';
                        
                        txtStr+='<tr>';
                        txtStr+='<td colspan="2" class="eSporttdborder eSportprinttabletitle">';
                        if(typeof(DisplayQuestionNumber)!='undefined')
						{
							if(DisplayQuestionNumber==1)
							{
								txtStr += (x) + '. ';
							}
						}
						txtStr+=question+'</td>';
						
                        txtStr+='</tr>\n';

			if (type_no[0]!=6) {
				for (var i=0; i<dataArr[j].length; i++) {
					percent = (dataArr[j][i][1]==0) ? 0 : Math.round(dataArr[j][i][1]*100/this.total);
					txtStr+='<tr><td width="80%" class="eSporttdborder eSportprinttext">'+dataArr[j][i][0] + '&nbsp;</td>';
                    txtStr+='<td nowrap class="eSporttdborder eSportprinttext">'+dataArr[j][i][1]+' ('+percent+'%)';
                    txtStr+='</td></tr>';
				}
			} else {
			    txtStr+='<tr><td colspan=2 class="eSporttdborder eSportprinttext">&nbsp;</td></tr>';
			}
			
			txtStr+='</table></td></tr>\n';
		}
                
		txtStr+='</table>\n';
		txtStr+='</Form>';

		return txtStr;
	}


	function analysisData() {
		var resultArr = new Array();
		var txtArr=this.qString.split("#QUE#");

		// each application
		for (var i=0; i<this.answer.length; i++) {
			ansArr=this.answer[i].split("#ANS#");
			var tmpArr = null;

			// each question
			for (var x=1; x<txtArr.length; x++){
				j = x - 1;
				tmpArr = txtArr[x].split("||");
				type_no = tmpArr[0].split(",");			//0:type, 1:number of options
				question = tmpArr[1];
				opts = tmpArr[2];

				if(type_no[0]!=10) {
					tmpArr = opts.split("#OPT#");
				}

				if (i==0) {
				    resultArr[j] = new Array();
				}

				switch (type_no[0]){
					case "1":
					case "2":
					case "12":
						if (i==0) {
							opt_no = (type_no[0]=="1") ? 2 : type_no[1];
							opt_no = (type_no[0]=="12") ? 1 : opt_no;
							for (var k=0; k<opt_no; k++) {
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						if (!isNaN(parseInt(ansArr[x]))) {
						    resultArr[j][parseInt(ansArr[x])][1] ++;
						}						
						break;
					case "3":
						if (i==0) {
							opt_no = type_no[1];
							for (var k=0; k<opt_no; k++) {
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						checkAns = ansArr[x].split(",");
						for (var m=0; m<checkAns.length; m++) {
							if (!isNaN(parseInt(checkAns[m]))) {
							    resultArr[j][parseInt(checkAns[m])][1] ++;
							}
						}
						break;
					case "4":
					case "5":
						var answer_pos = this.existPos(resultArr[j], ansArr[x]);
						isArrExpand = (answer_pos==resultArr[j].length);

						if (isArrExpand) {
							resultArr[j][answer_pos] = new Array;
							resultArr[j][answer_pos][0] = ansArr[x];
							resultArr[j][answer_pos][1] = 0;
						}
						resultArr[j][answer_pos][1] ++ ;
						break;
					case "6":
						break;
					case "7":
						break;
					case "8":
						break;
					case "9":
						if (i==0) {
							opt_no = type_no[1];
							for (var k=0; k<opt_no; k++) {
								resultArr[j][k] = new Array();
								resultArr[j][k][0] = tmpArr[k+1];
								resultArr[j][k][1] = 0;
							}
						}
						checkAns = ansArr[x].split(",");
						for (var m=0; m<checkAns.length; m++) {
							if (!isNaN(parseInt(checkAns[m]))) {
							    resultArr[j][parseInt(checkAns[m])][1] ++;
							}
						}
						break;
				}
			}
		}

		this.total = this.answer.length;
		this.data = resultArr;
	}

	function existPos(reArr, value) {
		for (var i=0; i<reArr.length; i++) {
			if (reArr[i][0].toUpperCase()==value.toUpperCase())
				return i;
		}
		return reArr.length;
	}

}
