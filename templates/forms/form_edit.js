// using: 

////////////////////// Change Log ////
//  Date:2020-08-24 Bill
//      Modified sheetArr(), to remove quotes in opts if convertQuoteInOpts = true
//	Date:2017-04-05 Frankie
//		Modified getAns() - check value with $.trim function 
//	Date:2015-12-16 Pun
//		Modified writeSheet(), sheetArr(), getAns(),
//			finish(), handleEmptyCase(), drop()
//		Added number of answer for ordering type 
//	Date:2015-11-13 Bill
//		edit editPanel(), sheetArr(), getAns()
//		edit appendTxt(), writeSheet(), move(), finish()
//		for Question must be submitted option
//	Date:2015-03-23 Jason
//		edit editPanel() to support new type - ordering
//	Date:2014-02-10 Kin
//	update writeSheet(), case 2/3
//	Date: 2012-09-12 YatWoon
//	updae editPanel(), option value min set to 2
//
//	Date: 2011-03-09	YatWoon
//	update changeTemplate()
//
//	Date:	2011-02-24	YatWoon
//		add MaxReplySlipOption
//
//	Date:	2011-02-09 YatWoon
//		add DisplayQuestionNumber checking, if DisplayQuestionNumber==1 display question number before the question
//
////////////////////////////////////

var formAllFilled = true; var orderingAllFilled = true; 
// define a Answer Sheet class
function Answersheet()
{
  if (typeof(_answersheet_prototype_called) == 'undefined')
  {
        _answersheet_prototype_called = true;
        Answersheet.prototype.qString = null;
        Answersheet.prototype.aString = null;
        Answersheet.prototype.counter = 0;
        Answersheet.prototype.answer = new Array();
        Answersheet.prototype.valueTmp = new Array();
        Answersheet.prototype.templates = new Array();
        Answersheet.prototype.selects = new Array();
        Answersheet.prototype.sheetArr = sheetArr;
        Answersheet.prototype.writeSheet = writeSheet;
        Answersheet.prototype.stuffAns = stuffAns;
        Answersheet.prototype.move = move;
        Answersheet.prototype.chgNum = chgNum;
        Answersheet.prototype.secDes = secDes;
        Answersheet.prototype.mode = 0;                                // 0:create; 1:for fill-in
        Answersheet.prototype.templateNo = 0;                        // template list menu
        Answersheet.prototype.mustSubmit = 0;						 // Question must be submitted option
        Answersheet.prototype.mustSubmitValues = new Array();
        Answersheet.prototype.convertQuoteInOpts = false;
     //Answersheet.prototype.convertType = convertType;
		
		if(typeof(MaxOptionNo)!='undefined' && MaxOptionNo>0)
			Answersheet.prototype.ansQty = MaxOptionNo;
		else
			Answersheet.prototype.ansQty = 50;							// default answer quantity [20090310 yat] (default = 15)
  }

        function writeSheet(withSpace){
				withSpace = withSpace || '1';
                var txtArr = this.answer;
                var valueArr = this.valueTmp;
                var selArr = this.selects;
                var arr=txtArr;
                var strTowrite='';
							
                if (this.mode==0) {
                	// Display Order
                        strTowrite+='<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
						+'<tr><td valign=top>'+(typeof(Part2)!='undefined'?Part2:'&nbsp;')+'</td>'
						 +'<td nowrap><form name="editForm">&nbsp; <select name="sList">';
                        for (x=0; x<arr.length; x++){
                                temTxt=arr[x][0];
                                strTowrite+='<option value="">'+cutStrLen(temTxt, 35);
                        }
                        strTowrite+='</select><br>'
                        +'&nbsp; <input type=button value="&uarr; '+ MoveUpBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'up\');writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&darr; '+ MoveDownBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'down\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&Chi; '+ DeleteBtn +'" onclick="sheet.move(this.form.sList.selectedIndex, \'out\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&harr; '+ EditBtn +'" onclick="sheet.secDes(this.form.sList.selectedIndex, \'out\'); "> </form>'
						+'</td></tr>\n'
                        
						+'<tr><td colspan=2>'	
                        +'<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                        +'</td></tr>'
                        
                 	+'</td></tr></table>'
                }
                
                var ansNum=1;
                txtStr=strTowrite+'<Form name="answersheet">\n';
				
                txtStr+='<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">'			
							+'<tr><td align=center>'+ replyslip +'</td></tr>'
							+'<tr><td>'+(typeof(Part3)!='undefined'?Part3:'&nbsp;')+'</td></tr>'  +'<tr><td>'
							+'<table width="100%" border="0" cellpadding="0" cellspacing="5">';
                		
                        for (x=0; x<txtArr.length; x++){
                        tit=recurReplace(">", "&gt;", txtArr[x][0]);
                        tit = recurReplace("<", "&lt;", tit);
                        tit = recurReplace("\n", "<br>", tit);

                    	txtStr+= "<input id=\"typeOf"+ansNum+"\" type=\"hidden\" value=\""+txtArr[x][1][0]+"\" sum=\""+txtArr[x][1][2]+"\" />";
                    	
                    	// Display * for must be submitted question
                    	if(this.mustSubmit){
                    		var canSubmit = this.mustSubmitValues[x]==1;
                    		if(canSubmit){
                    			tit = '<span class="tabletextrequire">*</span>' + tit;
                    			txtStr+= "<input id=\"mustSubmitOption"+ansNum+"\" type=\"hidden\" value=\""+canSubmit+"\" sum=\""+canSubmit+"\" />";
                    		}
                    	}
                    	
                        if(txtArr[x][1][0] == 7){
                        	txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
                            
                            txtStr+='<table border="0" style="min-width:340px;padding: 10px;" align="left" bgcolor="#EFEFEF"><tr><td style="white-space: nowrap;width:40px;text-align: center;font-weight: bold;padding:3px;"  bgcolor="#EFEFEF" colspan="1" class="tabletext">\n';
                            if(typeof(DisplayQuestionNumber)!='undefined')
    						{
    							if(DisplayQuestionNumber==1)
    							{
    								txtStr += (x+1) + '. ';
    							}
    						}    						
                            if(tit.indexOf("|") > -1){
                            	res = tit.split("|");
                            }else{
                            	res = [order_lang,tit];
                            }
                            
    						txtStr+=res[0]+'</td><td style="text-align: center;font-weight: bold;padding:3px;width:300px;">'+res[1]+'</td>';
    						if(this.mode!=0)
    							txtStr+='<td style="text-align: center;font-weight: bold;padding:3px" >'+choice_lang+'</td></tr>\n';
    						else
    							txtStr+='</tr>\n';
    						
                        }else{
                        	txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
                        	
                            txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
                            if(typeof(DisplayQuestionNumber)!='undefined')
    						{
    							if(DisplayQuestionNumber==1)
    							{
    								txtStr += (x+1) + '. ';
    							}
    						}
    						
    						txtStr+=tit+'</td></tr>\n';
                        }
                        
                        
						
                        for (y=1; y<txtArr[x].length; y++){
                                queArr=txtArr[x][y][0].split(",");
		
                                switch (queArr[0]){
                                		case "7": 
                                			break;
                                		default:
                                            txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                			break;
                                }
                                
                                switch (queArr[0]){
                                        case "1":
                                                preValue0 = (ansNum<=this.counter) ? valueArr[ansNum-1][0] : "";
                                                preValue1 = (ansNum<=this.counter) ? valueArr[ansNum-1][1] : "";
                                                r_check0 = "";
                                                r_check1 = "";

                                                if (selArr.length>0) {
                                                    eval("r_check"+selArr[ansNum-1]+"='checked'");
                                                }
                                                
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 40; // total str length
                                                txtStr+='<input type="radio" value="0" '+r_check0+' name="F'+ansNum+'" id="FF0'+ansNum+'">';
                                                txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue0+'" name="FD'+ansNum+'_0" size="25" class="tabletext">' : '<label for="FF0'+ansNum+'">'+preValue0+'</label>';
												
                                                if (withSpace == '1'){
													txtStr+=(this.mode==0 || strLen>=40) ? '<br>' : ' &nbsp; &nbsp; ';
												}
                                                
                                                txtStr+='<input type="radio" value="1" '+r_check1+' name="F'+ansNum+'" id="FF1'+ansNum+'">';
                                                txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue1+'" name="FD'+ansNum+'_1" size="25" class="tabletext">' : '<label for="FF1'+ansNum+'">'+preValue1+'</label>';
                                                break;
                                        case "2":
                                                r_check_i = (selArr.length>0) ? parseInt(selArr[ansNum-1]) : "";
                                                
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                                for (m=0; m<txtArr[x][y][1]; m++){
                                                    preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
                                                    if (r_check_i=='' && r_check_i!='0')
                                                    {
                                                        txtStr+='<input type="radio" value="'+m+'" name="F'+ansNum+'" id="FF0'+ansNum+'_'+m+'">';
                                                    }
                                                    else
                                                    {
                                                        txtStr+=(r_check_i==m) ? '<input type="radio" value="'+m+'" checked name="F'+ansNum+'" id="FF0'+ansNum+'_'+m+'">' : '<input type="radio" value="'+m+'" name="F'+ansNum+'" id="FF0'+ansNum+'_'+m+'">';
                                                    }
                                                    txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" id="FF0'+ansNum+'" size="25" class="tabletext"><br>' : "<label for=\"FF0"+ansNum+'_'+m+"\">" + preValue + "</label>";
													if (withSpace == '1'){
														if (this.mode==1) {
															txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
														}
													}
                                                }
                                                break;
                                        case "3":
                                                s_checkArr = null;
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                                if (selArr.length>0) {
                                                        s_checks = selArr[ansNum-1];
                                                        s_checkArr = s_checks.split(",");
                                                }
                                                for (m=0; m<txtArr[x][y][1]; m++){
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
                                                        c_check = "";
                                                        if (s_checkArr!=null) {
                                                                for (var kk=0; kk<s_checkArr.length; kk++) {
                                                                        if (s_checkArr[kk]==m) {
                                                                                c_check = "checked";
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        txtStr+= '<input type="checkbox" '+c_check+' name="F'+ansNum+'" value="'+m+'" id="FF0'+ansNum+'_'+m+'">';
                                                        txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext"><br>' : "<label for=\"FF0"+ansNum+'_'+m+"\">" + preValue + "</label>";
                                                        if (withSpace == '1'){
															if (this.mode==1) {
																txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																	txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
															}
														}
                                                }
                                                break;
                                        case "4":
                                                t_filled = (selArr.length>0) ? selArr[ansNum-1] : "";
                                                txtStr+='<input type="text" name="F'+ansNum+'" value="'+t_filled+'" class="tabletext">';
                                                break;
                                        case "5":
                                                t_filled = (selArr.length>0) ? selArr[ansNum-1] : "";
                                                txtStr+='<textarea name="F'+ansNum+'" cols="50" rows="3">'+recurReplace("<br>", "\n", t_filled)+'</textarea>';
                                                break;
                                        case "6":
                                                break;
                                        case "7":
	                                        	var optionCount = txtArr[x][y][1];
	                                        	var selectedChoiceInt = txtArr[x][y][2] || optionCount;
            	
                                        		if(selArr[ansNum-1]){
                                            		var ans_empty = "" == $.trim(selArr[ansNum-1]).replace(/,/gi,"");    
                                        		}else{
                                        			var ans_empty = false;
                                        		}
                                        		
	                                            if(selArr.length>0 && !ans_empty){
	                                            	var orderArr = $.trim(selArr[ansNum-1]).split(",");
                                                	
	                                            	var current_valueArr = [];
	                                            	for(var a = 0 ; a < orderArr.length; a++){
	                                            		current_valueArr[a] = valueArr[ansNum-1][orderArr[a]];
	                                            	}
	                                            	
	                                            	var not_selected_valueArr = [];
	                                            	var not_selected_valueArrId = [];
	                                            	for(var a = 0 ; a < valueArr[ansNum-1].length ; a++){
	                                            		if( $.inArray(''+a, orderArr) == -1 ){
	                                            			not_selected_valueArr.push( valueArr[ansNum-1][a] );
	                                            			not_selected_valueArrId.push(''+a);
	                                            		}
	                                            	}
	                                            	
	                                            }else{
		                                            var current_valueArr = valueArr[ansNum-1];
	                                            	var not_selected_valueArr = [];
	                                            }
	                                            
	                                            for (m=0; m<optionCount; m++){
	                                                preValue = (ansNum<=this.counter) ? current_valueArr[m] : "";
	                                                not_selected_preValue = (ansNum<=this.counter) ? not_selected_valueArr[m] : "";
	                                                var current_m = m + 1;
	                                                txtStr+='<td class="tabletext" style="text-align: center;padding: 6px 3px 0px 3px; vertical-align:top;" bgcolor="#EFEFEF">' + ( ( (this.mode==0) || (current_m <= selectedChoiceInt) )? (current_m+'.') : '&nbsp;') +'</td>';
	                                                
	                                                var tag = checkIE9() == 9 ? "a" : "span";
	                                                
	                                                if(this.mode==0){
	                                                	txtStr+='<td style="padding: 0px 3px 0px 3px" class="tabletext" bgcolor="#EFEFEF">';
	                                                	txtStr+='<input style="min-width:300px" type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" from="F" id="FF0'+ansNum+'" size="25" class="tabletext"><br>';
		                                                txtStr+='</td>';
	                                                }else{
	                                                	if(selArr.length>0&& !ans_empty){
	                                                		txtStr+='<td style="padding: 0px 3px 0px 3px; vertical-align: top;" class="tabletext" bgcolor="#EFEFEF">';

		                                                	if(m<selectedChoiceInt){
			                                                	txtStr+= "<div  ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\">" 
				                                                			+ "<"+tag+"  href=\"javascript:void(0)\" style=\"color:#000;text-decoration:none;cursor:move;text-align: center;border: 1px solid #000000;padding:5px;min-width:300px;display: inline-block;min-height: 15px;\" range=\""+m+"\" " 
				                                                			+ " class=\"no-select\" sum=\""+txtArr[x][y][1]+"\" sumAns=\""+txtArr[x][y][2]+"\" ansNum=\""+ansNum+"\" from=\"F\" id=\"FF0"+ansNum+'_'+m+"\" draggable=\"true\" ondragstart=\"drag(event)\" >" 
				                                                			+ "<input name=\"F"+ansNum+"\" type=\"hidden\" value=\""+((selArr.length>0 && !ans_empty) ? orderArr[m] : m)+"\">" +  preValue 
				                                                			+ "</"+tag+"> </div>";
		                                                	}
		                                                	
		                                                	txtStr+='</td><td style="padding: 0px 3px 0px 3px; vertical-align: top;" class="tabletext" bgcolor="#EFEFEF">';
		                                                	
		                                                	txtStr+= "<div  ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\">" 
			                                                			+ "<"+tag+" href=\"javascript:void(0)\" style=\"color:#000;cursor:move;text-align: center;border: 1px dashed #000000;padding:5px;width:300px;display: inline-block;min-height: 15px;\" range=\""+m+"\" " 
			                                                			+ " class=\"no-select\" sum=\""+txtArr[x][y][1]+"\" sumAns=\""+txtArr[x][y][2]+"\" ansNum=\""+ansNum+"\" from=\"C\" id=\"CF0"+ansNum+'_'+m+"\" draggable=\"true\" ondragstart=\"drag(event)\" >";
		                                                	if(m < not_selected_valueArrId.length){
		                                                		txtStr+= "<input name=\"F"+ansNum+"\" type=\"hidden\" value=\""+(not_selected_valueArrId[m])+"\">" +  not_selected_preValue;
		                                                	}
		                                                	txtStr+= "</"+tag+"></div>";
			                                                txtStr+='</td>';
	                                                	}else{

		                                                	txtStr+='<td style="padding: 0px 3px 0px 3px; vertical-align: top;" class="tabletext" bgcolor="#EFEFEF">';

		                                                	if(m<selectedChoiceInt){
			                                                	txtStr+= "<div  ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\">" 
				                                                			+ "<"+tag+" href=\"javascript:void(0)\" style=\"color:#000;cursor:move;text-align: center;border: 1px solid #000000;padding:5px;width:300px;display: inline-block;min-height: 15px;\" range=\""+m+"\" " 
				                                                			+ " class=\"no-select\" sum=\""+txtArr[x][y][1]+"\" sumAns=\""+txtArr[x][y][2]+"\" ansNum=\""+ansNum+"\" from=\"F\" id=\"FF0"+ansNum+'_'+m+"\" draggable=\"true\" ondragstart=\"drag(event)\" >" 
				                                                			+ "</"+tag+"></div>";
		                                                	}

		                                                	txtStr+='</td><td style="padding: 0px 3px 0px 3px; vertical-align: top;" class="tabletext" bgcolor="#EFEFEF">';
		                                                	
		                                                	txtStr+= "<div  ondrop=\"drop(event)\" ondragover=\"allowDrop(event)\">" 
			                                                			+ "<"+tag+" href=\"javascript:void(0)\" style=\"color:#000;cursor:move;text-align: center;border: 1px dashed #000000;padding:5px;width:300px;display: inline-block;min-height: 15px;\" range=\""+m+"\" " 
			                                                			+ " class=\"no-select\" sum=\""+txtArr[x][y][1]+"\" sumAns=\""+txtArr[x][y][2]+"\" ansNum=\""+ansNum+"\" from=\"C\" id=\"CF0"+ansNum+'_'+m+"\" draggable=\"true\" ondragstart=\"drag(event)\" >" 
			                                                			+ "<input name=\"F"+ansNum+"\" type=\"hidden\" value=\""+((selArr.length>0 && !ans_empty) ? orderArr[m] : m)+"\">" 
			                                                			+ preValue 
			                                                			+ "</"+tag+"> </div>";
			                                                txtStr+='</td>';
	                                                	}     
	                                                } 	                                                
	                                                
	                                                txtStr+='<tr>';
	                                            }
                                                
	                                            /******** Select number of answer UI START ********/
                                                if(this.mode==0){ // Show only in edit mode
                                                	var selectChoiceHTML = '';
                                                	var selected = '';
                                                	
                                                	selectChoiceHTML += '<select name="FD'+ansNum+'_choice">';
                                                	for(var i=0;i<optionCount;i++){
                                                		selected = '';
                                                		if(i == (selectedChoiceInt-1) ){
                                                			selected = ' selected';
                                                		}
                                                		selectChoiceHTML += '<option value="'+(i+1)+'"'+selected+'>'+(i+1)+'</option>';
                                                	}
                                                	selectChoiceHTML += '</select>';
                                                	
                                                	txtStr += '<tr><td colspan="2">';
                                                	txtStr += '<span>'+num_choice+': '+selectChoiceHTML+'</span>';
                                                	txtStr += '</td></tr>';
                                                }
	                                            /******** Select number of answer UI END ********/
                                                
                                        		break;
                                } // End switch (queArr[0])
                                tArr=txtArr[x][y];                                
                                
                                if(queArr[0]!="7")
                                	txtStr+= (queArr[0]=="2" || queArr[0]=="3") ? '</td>' : '<br></td>';
                                
                                
                                ansNum++;
                        }
                        txtStr+='</table>\n';
                        
                        txtStr+='</td></tr>\n';
                }
                
                txtStr+='</td></tr></table>';
                txtStr+='</td></tr></table>';
                this.counter = ansNum-1;
                txtStr+='</Form>\n';
//                txtStr+='<table>'+
//                    '<tr><td><div><a id="one" draggable="true" href="#">one</a></div></td></tr>'+
//                    '<tr><td><div><a id="two" draggable="true" href="#">two</a></div></td></tr>'+
//                    '<tr><td><div><a id="three" draggable="true" href="#">three</a></div></td></tr>'+
//                    '<tr><td><div><a id="four" draggable="true" href="#">four</a></div></td></tr>'+
//                    '<tr><td><div><a id="five" draggable="true" href="#">five</a></div></td></tr>'+
//                  '</table>';
                return txtStr;
        }



/* internal function for conversion and operations */
        function pushUp(arr){
                for (x=0; x<arr.length-1; x++){
                        arr[x]=arr[x+1];
                }
                arr.length=arr.length-1;
                return arr;
        }


        //initialization
        function sheetArr(){
                var txtStr = this.qString;
                var txtArr = txtStr.split("#QUE#");
                var tmpArr = null;
                var resultArr = new Array();
                var valueTemp = new Array();
                var submitArr = new Array();
                var submitTemp = new Array();

                txtArr = pushUp(txtArr);
                this.counter = txtArr.length;
				var containMustSubmit = txtStr.indexOf('#MSUB#')!==-1;
				
                for (var x=0; x<txtArr.length; x++){
                        tmpArr = txtArr[x].split("||");
                        type_no = tmpArr[0].split(",");
                        question = tmpArr[1];
                        opts = tmpArr[2];
                        if(this.convertQuoteInOpts != false) {
                            opts = opts.replace(/"/g, '&quot;');
                        }

                        // get Question Options & Settings
						if(this.mustSubmit || containMustSubmit) {
							submitTemp = opts.split("#MSUB#");
							opts = submitTemp[0];
							mustS = submitTemp[1];
						}
						
                        var j = x;

                        // question
                        resultArr[j] = new Array();
                        resultArr[j][0] = recurReplace("<br>", "\n", question);
                        resultArr[j][1] = new Array();
                        resultArr[j][1][0] = type_no[0];
                        if (type_no.length>1) {
                            resultArr[j][1][1] = type_no[1];
                            
                        }
	                    if(type_no[0] == "7"){ // For type "Ordering"
	                    	resultArr[j][1][2] = type_no[2];
	                    }
                        
                        // option values
                        valueTemp[j] = new Array();
                        tmpArr = opts.split("#OPT#");
                        if (tmpArr.length>1) {
                                for (var m=1; m<tmpArr.length; m++){
                                        valueTemp[j][m-1] = tmpArr[m];
                                }
                        }
                        
                        // question submit settings
                        if(this.mustSubmit){
                        	if(mustS!=1){
                        		mustS = 0;
                        	} else{
                        		mustS = 1;
                        	}
                        	submitArr[j] = mustS;
                        }
                }

                if (this.mode==1 && this.aString!=null) {
                    this.stuffAns();
                }

                this.valueTmp = valueTemp;
                if(this.mustSubmit){
                	this.mustSubmitValues = submitArr;
                }
                
                return resultArr;
          }


        function stuffAns(){
                var ansStr=this.aString;
                ansArr=ansStr.split("#ANS#");
                this.selects = pushUp(ansArr);
          }


        function move(i, dir){
                var arr = this.answer;
                var temArr = new Array();

                retainValues();
                var valueArr = this.valueTmp;
                var tvArr = new Array();
                
                var submitArr = this.mustSubmitValues;
                var tsArr = new Array();

                switch (dir){
                        case "up":
                                if (i!=0 && i<arr.length){
                                        //swap question
                                        temArr = arr[i];
                                        arr[i] = arr[i-1];
                                        arr[i-1] = temArr;

                                        //swap current fill-in-value
                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i-1];
                                        valueArr[i-1] = temArr;
                                        
                                        //swap question submit setting
                                        if(this.mustSubmit){
                                        	tsArr = submitArr[i];
                                        	submitArr[i] = submitArr[i-1];
                                        	submitArr[i-1] = tsArr;
                                        }
                                };
                                break;
                        case "down":
                                if (i!=(arr.length-1)&&i>=0){
                                        temArr = arr[i];
                                        arr[i] = arr[i+1];
                                        arr[i+1] = temArr;

                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i+1];
                                        valueArr[i+1] = temArr;
                                        
                                        if(this.mustSubmit){
                                        	tsArr = submitArr[i];
                                        	submitArr[i] = submitArr[i+1];
                                        	submitArr[i+1] = tsArr;
                                        }
                                };
                                break;
                        case "out":
								if(arr.length>0)
								{
									for (x=i; x<arr.length; x++) {
											arr[x]=arr[x+1];
											valueArr[x]=valueArr[x+1];
	                                        if(this.mustSubmit){
	                                        	submitArr[i] = submitArr[i+1];
	                                        }
									}
									arr.length = arr.length-1;
									valueArr.length = valueArr.length-1;
                                    if(this.mustSubmit){
                                    	submitArr.length = submitArr.length-1;
                                    }
								}
                                break;
                }
                this.answer = arr;
                this.valueTmp = valueArr;
                if(this.mustSubmit){
                	this.mustSubmitValues = submitArr;
                }
        }


        function secDes(i){
                var arr=this.answer;

				if(typeof(arr[i])!="undefined")
				{    
					var form_pop_up = window.open("", "form_pop_up", "toolbar=no,location=no,status=no,menubar=no,resizable,width=400,height=200,top=100,left=100");

					var JSfunction = "<script language='javascript'>";
					JSfunction += "function updateDesc(ind, fobj) {";
					JSfunction += "var arrD = window.opener.sheet.answer;";
					JSfunction += "arrD[ind][0] = fobj.new_desc.value;";
					JSfunction += "window.opener.sheet.answer = arrD;";
					JSfunction += "window.opener.retainValues();";
					JSfunction += "window.opener.writetolayer('blockInput',window.opener.sheet.writeSheet());";
					JSfunction += "self.close();";
					JSfunction += "} </script>";
				
					var desc_form = "<HTML><head><title>"+chg_title+"</title></head><body bgcolor='#EFECE7'><form name='form1'>";
					desc_form += "<br>"+JSfunction+"<table border='0' align='center'>";
					desc_form += "<tr><td><textarea name='new_desc' rows='5' cols='40'>"+arr[i][0]+"</textarea></td></tr>";
					desc_form += "<tr><td align='right'><input type='button' onClick='updateDesc("+i+", this.form);' value='"+button_update+"'> <input type='button' onClick='self.close()' value='"+button_cancel+"'></td></tr>";
					desc_form += "</table></form></body></HTML>";
					form_pop_up.document.write(desc_form);
				}

                return;
        }


        function chgNum(i){
                arr=this.answer;
                num=arr[i].length-1;
                var changeNum = prompt ("Change the number of questions from "+num+" to:","");
                if (changeNum){
                    if (isInteger(changeNum) && changeNum>0){
                            if (changeNum>num){
                                    for (n=num; n<changeNum; n++)
                                             arr[i][arr[i].length]=arr[i][arr[i].length-1];
                            }
                            if (changeNum<num){
                                    diff=num-changeNum;
                                    for (n=0; n<diff; n++)
                                        arr[i].length--;
                            }
                     }
                     else
                             alert("your input is not a valid number");
        }
        }


}

function editPanel(withSpace){
		withSpace = withSpace || '1';
        answer = "";
		
		var ordering_option = "";
		if(typeof enableOrderingQuestion != "undefined"){
			ordering_option =  (enableOrderingQuestion == true) ? '<option>'+answersheet_order+'\n' : "";
		}
		
        if (sheet.mode==0) {
        		// Question must be submitted option
				var mustSubmitOption = "";
				if(sheet.mustSubmit) {
					mustSubmitOption = "<tr>"
										  +"<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answer_sheet_mustsubmit+"</span></td>"
		                				  +'<td class="tabletext">\n'
		                				  +'<input type="checkbox" name="mustSubmitOption" />'
		                				  +'</td>'
		                				+'</tr>\n';
				}
				
                answer += '<DIV ID="blockDiv">\n'
                +'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
				+'<tr><td>'+(typeof(Part1)!='undefined'?Part1:'&nbsp;')+'</td></tr>\n'
                +'<tr><td><table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                +'<form name="addForm">\n'

                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_template+"</span></td>"
                +'<td class="tabletext">'+getTemplate()+'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_header+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_type+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==2||this.selectedIndex==3||this.selectedIndex==7) this.form.oNum.selectedIndex=1; else this.form.oNum.selectedIndex=0">\n'
                +'                <option>- '+answersheet_type+' -\n'
                +'                <option>'+answersheet_tf+'\n'
                +'                <option>'+answersheet_mc+'\n'
                +'                <option>'+answersheet_mo+'\n'
                +'                <option>'+answersheet_sq1+'\n'
                +'                <option>'+answersheet_sq2+'\n'
                +'                <option>'+answersheet_not_applicable+'\n'
                + ordering_option
                +'                </select>\n'
                +'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_option+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=2&&this.form.qType.selectedIndex!=3&&this.form.qType.selectedIndex!=7){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option>- '+answersheet_option+' -\n';
                
                // [20090310 yat]
				// [20120912 yat]
                for(qi=2;qi<=sheet.ansQty;qi++)
                	answer +='                <option value='+qi+'>'+qi+'\n';
                
                answer +='  </select>\n'
                +'</td></tr>\n'
                
                +mustSubmitOption
                
                +'</table></td></tr>\n'
                +'<tr><td>'
				+ '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                + '</td></tr>'
				+ '<tr><td colspan="2" align="center">'+(typeof(add_btn)!='undefined'?add_btn:'&nbsp;')+'</td></tr>\n'
                + '        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet("'+withSpace+'"));\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}

// Added by Kelvin Ho on 10 Feb 09.
function editPaneleDiscipline(){
        answer = "";
        
		var ordering_option = "";
		if(typeof enableOrderingQuestion != "undefined"){
			ordering_option =  (enableOrderingQuestion == true) ? '<option>'+answersheet_order+'\n' : "";
		}
        
        if (sheet.mode==0) {
                answer += '<DIV ID="blockDiv">\n'
                +'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
				+'<tr><td>'+(typeof(Part1)!='undefined'?Part1:'&nbsp;')+'</td></tr>\n'
                +'<tr><td><table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                +'<form name="addForm">\n'

                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_template+"</span></td>"
                +'<td class="tabletext">'+getTemplate()+'</td></tr>\n'
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_header+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_type+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==2||this.selectedIndex==3||this.selectedIndex==7) this.form.oNum.selectedIndex=3; else this.form.oNum.selectedIndex=0">\n'
                +'                <option>- '+answersheet_type_selection+' -\n'
                +'                <option>'+answersheet_tf+'\n'
                +'                <option>'+answersheet_mc+'\n'
                +'                <option>'+answersheet_mo+'\n'
                +'                <option>'+answersheet_sq1+'\n'
                +'                <option>'+answersheet_sq2+'\n'
                +'                <option>'+answersheet_not_applicable+'\n'
                +					ordering_option
                +'                </select>\n'
                +'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_option+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=2&&this.form.qType.selectedIndex!=3&&this.form.qType.selectedIndex!=7){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option>- '+answersheet_selection+' -\n';
                
                // [20090310 yat]
                for(qi=3;qi<=sheet.ansQty;qi++)
                	answer +='                <option value='+qi+'>'+qi+'\n';
                	
                	/*
                +'                <option value=3>3\n'
                +'                <option value=4>4\n'
                +'                <option value=5>5\n'
                +'                <option value=6>6\n'
                +'                <option value=7>7\n'
                +'                <option value=8>8\n'
                +'                <option value=9>9\n'
                +'                <option value=10>10\n'
                +'                <option value=11>11\n'
                +'                <option value=12>12\n'
                +'                <option value=13>13\n'
                +'                <option value=14>14\n'
                +'                <option value=15>15\n'
                */
                
                answer +='  </select>\n'
                +'</td></tr></table></td></tr>\n'
                +'<tr><td>'
				+ '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                + '</td></tr>'
				+ '<tr><td colspan="2" align="center">'+(typeof(add_btn)!='undefined'?add_btn:'&nbsp;')+'</td></tr>\n'
                + '        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet());\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}



function getTemplate() {
        var tmpArr = sheet.templates;
    var xStr = '<select name="fTemplate" onchange="if (confirm(chg_template)) {changeTemplate(this.form.fTemplate.selectedIndex); this.form.secDesc.focus();} else {this.form.fTemplate.selectedIndex=sheet.templateNo;}">';

        xStr += '<option value="">-'+answersheet_template+'-</option>\n';
        for (var i=0; i<tmpArr.length; i++) {
            xStr += '<option value="'+i+'">'+tmpArr[i][0]+'</option>\n';
        }
        xStr += '</select>';

        return xStr;
}


function changeTemplate(index) {
        if (index!=0) {
                sheet.qString = sheet.templates[index-1][1];
                sheet.answer = sheet.sheetArr();
        } else {
            sheet.qString = "";
                //sheet.answer = "";
				sheet.answer = sheet.sheetArr();
        }
        sheet.templateNo = index;
        writetolayer("blockInput",sheet.writeSheet());
}


function appendTxt(formName){
        var formObj=eval("document."+formName);
        var arr=new Array();
        arr[0]=formObj.secDesc.value;

        /* check empty tocie/title */
        if(formObj.secDesc.value=='')
        {
	        alert(pls_fill_in_title);
			formObj.secDesc.focus();
			return false;
        }
        
        qtype=String(formObj.qType.selectedIndex);
        if (qtype==0){
                alert(pls_specify_type);
                formObj.qType.focus();
                return false;
        }

        for (x=0; x<1; x++){
                y=x+1;
                arr[y]=new Array();
                arr[y][0]=qtype;
                if (qtype=="2" ||qtype=="3"||qtype=="7")
                        arr[y][1]=String(formObj.oNum.options[formObj.oNum.selectedIndex].value);
        }
        
        if(sheet.mustSubmit && formObj.mustSubmitOption){
        	var submitval = 0;
        	if(formObj.mustSubmitOption.checked){
        		submitval = 1;
        	}
        	sheet.mustSubmitValues[sheet.answer.length]=submitval;
        }
        
        sheet.answer[sheet.answer.length]=arr;
        return true;
}

if (need2checkform==undefined)
{
    var need2checkform = false;
}
if(need2checkformSQ==undefined)
{
	var need2checkformSQ = false;
}

function getAns(i){//20150324
        var eleObj = eval("document.answersheet.F"+i);
        var strAns = null;
        var tempAns = null;

		var mustSubmitArr = sheet.mustSubmitValues;

        if (eleObj==undefined)
        {
            return '';
        }
        if (eleObj.length){
                switch(eleObj[0].type){
                        case "radio":
                                for (p=0; p<eleObj.length; p++){
                                        if (eleObj[p].checked)
                                        {
                                                tempAns += p;
                                        }
                                }
                                if (need2checkform && tempAns == null )
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                if(sheet.mustSubmit && need2checkformSQ && mustSubmitArr[i-1]==1 && tempAns == null){
                                    formAllFilled = false;
                            		return false;
                                }
                                if (tempAns != null)
                                {
                                    strAns = tempAns;
                                }
                                break;

                        case "checkbox":
                                for (p=0; p<eleObj.length; p++) {
                                        if (eleObj[p].checked)
                                        {
                                            if (tempAns != null)
                                            {
                                                tempAns += ","+p;
                                            }
                                            else
                                            {
                                                tempAns = p;
                                            }
                                        }
                                }
                                if (need2checkform && tempAns == null)
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                if(sheet.mustSubmit && need2checkformSQ && mustSubmitArr[i-1]==1 && tempAns == null){
                                    formAllFilled = false;
                            		return false;
                                }
                                strAns = tempAns;
                                break;

                        case "text":
                                for (p=0; p<eleObj.length; p++){
                                        tempAns += recurReplace('"', '&quot;', eleObj.value);
                                }
                                if (need2checkform && (tempAns == null || $.trim(tempAns)==''))
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                if(sheet.mustSubmit && need2checkformSQ && mustSubmitArr[i-1]==1 && (tempAns == null || $.trim(tempAns)=='')){
                                    formAllFilled = false;
                            		return false;
                                }
                                strAns = tempAns;
                                break;
                                
                        case "hidden":
                        		var current_type = $("#typeOf"+i).attr("value");
                        		var current_sum =  $("#typeOf"+i).attr("sum");
                        		var emptyIndexCount = 0;
                        		
                        		if(current_type == 7){
                        			var emptyChoiceIndex = checkIfNextEmpty(0,current_sum,"CF0",i);                        			
                        			var emptyIndex = checkIfNextEmpty(0,current_sum,"FF0",i);
    	                        	
                        			if(emptyIndex != -1){
                            			formAllFilled = false; emptyIndexCount++;
                            			if(need2checkform)
                            				return false;
		                                if(sheet.mustSubmit && need2checkformSQ && mustSubmitArr[i-1]==1){
		                            		return false;
		                                }
                        			}
                        		}
                        		tempAns = "";var emptyAns = "";
	                        	for (p=0; p<eleObj.length; p++) {	     	                        		
	                        		if($(eleObj[p]).parent().attr('id').indexOf('CF') == 0){
	                        			continue;
	                        		}
	                        		var current_p = eleObj[p].value;	   
	                        		tempAns += tempAns != "" ?  ","+current_p : current_p;	      
	                        		emptyAns += ",";
		                        }
	                        	
	                        	if (!formAllFilled && !need2checkform){
	                        		if(emptyChoiceIndex == -1){
	                        			formAllFilled = true;
		                        		orderingAllFilled = true;
		                        		tempAns = emptyAns;
	                        		}else{
	                        			orderingAllFilled = false;
	                        		}
	                        	}else {
	                        		orderingAllFilled = true;
	                        	}
	                        	
		                        strAns = tempAns;
		                        break;                      	
                }
        } else
        {
                tempAns = recurReplace('"', '&quot;', eleObj.value);
                if (need2checkform && (tempAns == null || $.trim(tempAns)==''))
                {
                    formAllFilled = false;
                    return false;
                }
                if(sheet.mustSubmit && need2checkformSQ && mustSubmitArr[i-1]==1 && (tempAns == null || $.trim(tempAns)=='')){
                    formAllFilled = false;
            		return false;
                }
                strAns = tempAns;
        }
        return strAns;
}


function finish(){
        var txtStr="";
        var ansStr="";
        var arr=sheet.answer;
        var submitArr=sheet.mustSubmitValues;
        var ansNum=1;
        var temp = null;
        formAllFilled = true;
        
        if (sheet.mode==0) {
                // get questions
                for (var x=0; x<arr.length; x++){
                        //txtStr+="#QUE#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //append options
                                queArr = arr[x][y][0].split(",");
                                switch (queArr[0]){
                                        case "1":        myLen=2; break;
                                        case "4":
                                        case "5":
                                        case "6":        myLen=0; break;
                                        default:        myLen=arr[x][y][1]; break;
                                }

                                if(queArr[0] == "7" && arr[x][y].length > 2){
                                	arr[x][y].pop();
                                }
                                
                                txtStr+="#QUE#";
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr += (z>0) ? ","+arr[x][y][z] : arr[x][y][z];
                                }
                                
	                            if(y==1 && queArr[0] == "7"){ // Question type "Ordering"
	                            	var choice = eval("document.answersheet.FD"+ansNum+"_choice.value");
	                            	txtStr += ","+choice;
	                            }
                            
                                txtStr+="||"+arr[x][0]+"||";


                                for (var m=0; m<myLen; m++){
                                        optDescription = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        /* check form for null input => return
                                        if (optDescription=="") {
                                                alert(pls_fill_in);
                                                eval("document.answersheet.FD"+ansNum+"_"+m+".focus()");
                                                return false;
                                        }
                                        */
                                        txtStr+="#OPT#"+optDescription;
                                }
                                
                                if(sheet.mustSubmit){
                                	txtStr+="#MSUB#"+submitArr[x];
                                }
                                
                                ansNum++;
                        }
                }
                document.ansForm.qStr.value=txtStr;
        } else if (sheet.mode==1) {

                // get answers
            for (var x=0; x<arr.length; x++){
                    queArr = arr[x][1][0].split(",");
                    temp = getAns(x+1);
                    if (formAllFilled==false)
                    {
                        return false;
                    }
                    txtStr+=(queArr[0]=="6") ? "#ANS#" : "#ANS#"+temp; //getAns(x+1);
            }
            document.ansForm.aStr.value=txtStr;
        }
/*
        if (sheet.mode==1) {
                document.write(document.ansForm.aStr.value);
        } else {
            document.write(document.ansForm.qStr.value);
        }
*/
}


function retainValues(){
                var txtStr="";
                var ansStr="";
                var arr=sheet.answer;
                var ansNum=1;
                var valueTemp = new Array();

                for (var x=0; x<arr.length; x++){
                        valueTemp[x] = new Array();
                        //txtStr+="#SEC#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //txtStr+="#QUE#";
                                /*
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr+=arr[x][y][z]+"||";
                                }
                                */
                                //append options
                                queArr = arr[x][y][0].split(",");
                                switch (queArr[0]){
                                        case "1":        myLen=2; break;
                                        case "4":
                                        case "5":
                                        case "6":        myLen=0; break;
                                        default:        myLen=arr[x][y][1]; break;
                                }
                                for (var m=0; m<myLen; m++){
                                        tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        valueTemp[x][m] = recurReplace('"', '&quot;', tmpVal);
                                }
                                ansNum++;
                        }
                }
                sheet.valueTmp = valueTemp;
}


function recurReplace(exp, reby, txt) {
    while (txt.search(exp)!=-1) {
        txt = txt.replace(exp, reby);
    }
        return txt;
}




// the following three functions are added for integer validation



function isInteger(s){
        var i;
    if (isEmpty(s))
                if (isInteger.arguments.length == 1) return false;
                else return (isInteger.arguments[1] == true);
    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}



function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}


function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}

function getStrLen(tmpArr) {
        var tmpV=0;
        if (tmpArr.length<7) {
                for (var i=0; i<tmpArr.length; i++) {
                        for (var j=0; j<tmpArr[i].length; j++) {
                                tmpV += (tmpArr[i].charCodeAt(j)>1000) ? 2 : 1;
                        }
                }
        } else {
                tmpV = 60;
        }

        return tmpV;
}

function cutStrLen(xStr, xLen) {
        var tmpArr = new Array(xStr);
        var tmpStr = "";
        var xCount = 0;

    if (getStrLen(tmpArr)>xLen) {
                for (var j=0; j<xStr.length; j++) {
                        xCount += (xStr.charCodeAt(j)>1000) ? 2 : 1;
                        tmpStr += xStr.charAt(j);
                        if (xCount>xLen-3) {
                            break;
                        }
                }
        xStr = tmpStr+"...";
    }
        return xStr;
}

function allowDrop(ev) {
    ev.preventDefault();
}
var drag_from = "";
var drag_from_value = "";
function drag(ev) {
	drag_from = ev.target;
    ev.dataTransfer.setData("text", drag_from.id);
    drag_from_value = drag_from.attributes.from.value;
//    ev.dataTransfer.setData("from", drag_from.attributes.from.value);
}

function checkIfNextEmpty(end_range,sum,pre,ansnum){
	var emptyIndex = -1;
	for(var i = end_range; i < sum; i ++  ){
		if(document.getElementById(pre+ansnum+"_"+i).childNodes.length == 0){
			emptyIndex = i;
			break;
		}
	}
	return emptyIndex;
}

function checkIfIntermediateEmpty(start_range,end_range,sum,pre,ansnum){
	var emptyIndex = -1;
	for(var i = start_range+1; i < end_range; i ++  ){
		if(document.getElementById(pre+ansnum+"_"+i).childNodes.length == 0){
			emptyIndex = i;
			break;
		}
	}
	return emptyIndex;
}

function handleEmptyCase(end_range,sum,pre,ansnum){
	var emptyIndex = checkIfNextEmpty(end_range,sum,pre,ansnum);
	if(emptyIndex != -1){ // Move option down
		for(var i = end_range; i < emptyIndex ; i++){
			var j = i+ 1;      
   			document.getElementById(pre+ansnum+"_"+j).appendChild(document.getElementById(pre+ansnum+"_"+i).childNodes[1]);
   			document.getElementById(pre+ansnum+"_"+j).appendChild(document.getElementById(pre+ansnum+"_"+i).childNodes[0]);
		}
	}else{ // Move option up/replace
		/******** Check if there are empty box in this column START ********/
		var empty_box_index = 0;
		for(var i = end_range; i > 0 ; i--){
			var j = i- 1;
			var isNearestEmpty = false; 
			if(document.getElementById(pre+ansnum+"_"+j).childNodes.length == 0){
				isNearestEmpty = true;
				empty_box_index = j;
				break;
			}
		}
		/******** Check if there are empty box in this column END ********/
		
		/******** Move action START ********/
		if(isNearestEmpty){
			/**** Move all items up until the empty box is filled START ****/
			for(var i = end_range; i > empty_box_index ; i--){
				var j = i- 1;
	   			document.getElementById(pre+ansnum+"_"+j).appendChild(document.getElementById(pre+ansnum+"_"+i).childNodes[1]);
	   			document.getElementById(pre+ansnum+"_"+j).appendChild(document.getElementById(pre+ansnum+"_"+i).childNodes[0]);
			}
			/**** Move all items up until the empty box is filled END ****/
		}else{
			/**** Replace items START ****/
			$('[id^="CF0'+ansnum+'_"]').each(function(index, element){
				if(element.childNodes.length == 0){
					element.appendChild(document.getElementById(pre+ansnum+"_"+end_range).childNodes[1]);
					element.appendChild(document.getElementById(pre+ansnum+"_"+end_range).childNodes[0]);
					return false;
				}
			});
			/**** Replace items END ****/
		}
		/******** Move action END ********/
	}
}
$(document).ready(function(){
	$(".no-select").css("text-decoration", "none");
})

function checkIE9(){
	var undef,
	    v = 3,
	    div = document.createElement('div'),
	    all = div.getElementsByTagName('i');
	while (
	    div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
	        all[0]
	    );
	    return v > 4 ? v : undef;
};

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var from  = drag_from_value;
    var to = ev.target.attributes.from.value; // F: select, C: not select
    
    if(document.getElementById(data).childNodes.length == 0 || document.getElementById(data).getAttribute("ansnum") != ev.target.getAttribute("ansnum")){
    	return false;
    }
    
    var ansnum = document.getElementById(data).getAttribute("ansnum");
   	var start_range = parseInt(document.getElementById(data).getAttribute("range"));
   	var end_range = parseInt(ev.target.getAttribute("range"));
   	var sum;
   	if(to == 'F'){
   		sum = document.getElementById(data).getAttribute("sumAns"); // Total number of choise
   	}else{
   		sum = document.getElementById(data).getAttribute("sum"); // Total number of choise
   	}
   
    var pre = to+"F0";
    var end_childNode_length = document.getElementById(pre+ansnum+"_"+end_range).childNodes.length; // Check the target box has item or not
    
    ev.target.appendChild(document.getElementById(data).childNodes[1]);
    ev.target.appendChild(document.getElementById(data).childNodes[0]);
//    console.log([end_childNode_length, start_range, end_range]);
    if(from == to){ // Just ordering in same column
    	if(end_childNode_length == 0){    	
    		//DO Nothing	
    	}
    	else{
    		var emptyIndex = start_range > end_range ? checkIfIntermediateEmpty(end_range,start_range,sum,pre,ansnum) : checkIfIntermediateEmpty(start_range,end_range,sum,pre,ansnum);
    		
    		if(emptyIndex != -1){     				
//				console.log("A");    		
    			handleEmptyCase(end_range,sum,pre,ansnum);
    		}else if(start_range > end_range){    				
//				console.log("B");
    			for(var i = end_range; i < start_range; i ++  ){
    				var j = i+ 1;       		    
    				if(end_range == start_range - 1){       			
    					if(ev.target.childNodes.length == 0) break;
    					drag_from.appendChild(ev.target.childNodes[1]);
    					drag_from.appendChild(ev.target.childNodes[0]);
    				}else{
//    					if(document.getElementById(pre+ansnum+"_"+j).childNodes.length == 0) {		
//    						break;
//    					}
    		   			document.getElementById(pre+ansnum+"_"+j).appendChild(document.getElementById(pre+ansnum+"_"+i).childNodes[1]);
    		   			document.getElementById(pre+ansnum+"_"+j).appendChild(document.getElementById(pre+ansnum+"_"+i).childNodes[0]);
    				}
    			}
    		}else if (start_range < end_range){         				
//				console.log("C");    		
    			for(var i = start_range; i < end_range; i ++  ){
    				var j = i+ 1;       
    				if(start_range == end_range + 1){
    					if(ev.target.childNodes.length == 0) break;
    					drag_from.appendChild(ev.target.childNodes[1]);
    					drag_from.appendChild(ev.target.childNodes[0]);
    				}else{
    					if(document.getElementById(pre+ansnum+"_"+j).childNodes.length == 0) {
    						break;
    					}
    		   			document.getElementById(pre+ansnum+"_"+i).appendChild(document.getElementById(pre+ansnum+"_"+j).childNodes[1]);
    		   			document.getElementById(pre+ansnum+"_"+i).appendChild(document.getElementById(pre+ansnum+"_"+j).childNodes[0]);
    				}
    			}
    		}
    	}   	
    }else{
    	if(ev.target.childNodes.length > 2){
			handleEmptyCase(end_range,sum,pre,ansnum);
    	}else{
    		//DO Nothing
    	}    	
    }   	
}
