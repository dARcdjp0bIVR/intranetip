var ie4 = (document.all) ? true : false;
var ns4 = (document.layers) ? true : false;
var ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var is_ie5_5up = false;
var sliding = {c1:false, c2:false, c3:false, c4:false}
var hide = "hidden";
var show = "visible";

function hidelayer(lay) {
	if (ie4) {document.all[lay].style.visibility = "hidden";}
	if (ns4) {document.layers[pa].document.layers[lay].visibility = "hide";}
	if (ns6) {document.getElementById([lay]).style.display = "none";}
}


function showlayer(lay) {
	if (ie4) {document.all[lay].style.visibility = "visible";}
	if (ns4) {document.layers[pa].document.layers[lay].visibility = "show";}
	if (ns6) {document.getElementById([lay]).style.display = "block";}
}


function writetolayer(lay,txt) {
	showlayer(lay);
	if (ie4) {
		document.all[lay].innerHTML = txt;
	}
	if (ns4) {
		var lyr = document.layers[pa].document[lay].document;
		lyr.open();
		lyr.write(txt);
		lyr.close();
	}
	if (ns6) {
		over = document.getElementById([lay]);
		range = document.createRange();
		range.setStartBefore(over);
		domfrag = range.createContextualFragment(txt);
		while (over.hasChildNodes()) {
			over.removeChild(over.lastChild);
		}
		over.appendChild(domfrag);
	}
}

function switchLayer(layer_name, imgObj, alt_s, alt_h){
    var srcImg = imgObj.src
    var tn = srcImg.split("/")
    var imgName = tn[tn.length-1]

	if (!document.all)
		return;

	//var co=ns6? document.getElementById(layer_name).style : document.all[layer_name].style;
	var co=document.all[layer_name].style

	if (co.visibility==show){
		co.visibility = hide;
		co.position  = "absolute";
	}
	else {
		co.visibility = show;
		co.position  = "relative";
	}

}

function switchAll(layer_pre, total, objself, txt_show, txt_hide) {
	if (!document.all)
		return;

	var isShow = (objself.value == txt_show) ? true : false;

	for (var i=0; i<total; i++) {
		lay = layer_pre + i;
		var co=document.all[lay].style;
		if (!isShow){
			co.visibility = hide;
			co.position  = "absolute";
			objself.value = txt_show;
		}
		else {
			co.visibility = show;
			co.position  = "relative";
			objself.value = txt_hide;
		}
	}

}

function movelayer(lay,h){
	if (ie4) {heig=document.all[lay].style.top;}
	if (ns4) {heig=document.layers[pa].document.layers[lay].top;}
	if (ns6) {heig=document.getElementById([lay]).style.top;}
	//h=eval(heig.substr(0,heig.length-2))+h;
	ih = parseInt(heig) + h;
	if (ie4) {document.all[lay].style.top=ih+'px';}
	if (ns4) {document.layers[pa].document.layers[lay].top = ih;}
	if (ns6) {document.getElementById([lay]).style.top=ih+'px';}
}


function slide(lay, h, speed, up) {
	if (ie4) {
		heig=document.all[lay].style.top; it=4;
		if (is_ie5_5up)
			it = 4;
	}
	if (ns4) {heig=document.layers[pa].document.layers[lay].top; it=6;}
	if (ns6) {heig=document.getElementById([lay]).style.top; it=10;}
	ih = parseInt(heig);

	if (up == 0){
		if (ih != h)
			movelayer(lay, it);
		if (ih < h) {
			setSliding(lay, 1);
			setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
		} else
			setSliding(lay, 0);
	} else {
		if (ih != 0)
			movelayer(lay, -it);
		if (ih > 0) {
			setSliding(lay, 1);
			setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
		} else
			setSliding(lay, 0);
	}
}


function setSliding(lay, on_off) {
	is_sliding = "sliding." + lay;
	is_sliding += (on_off==0) ? "=false" : "=true";
	eval (is_sliding);
}


function getSliding(lay) {
	is_sliding = "sliding." + lay;
	return eval(is_sliding);
}


function move(lay,h){
	if (getSliding(lay))
		return;
	if (ie4) {
		heig=document.all[lay].style.top; speed=1;
		if (is_ie5_5up) {
			speed=1;
			document.all[pa].focus();
		}
	}
	if (ns4) {heig=document.layers[pa].document.layers[lay].top; speed=1;}
	if (ns6) {heig=document.getElementById([lay]).style.top;speed=1;}
	ih = parseInt(heig);

	if (ih<=0)
		setTimeout("slide('"+lay+"',"+h+","+speed+",0)",1);
	else if (ih>=h)
		setTimeout("slide('"+lay+"',"+h+","+speed+",1)",1);
}


function init(max_options){
	ie4 = (document.all) ? true : false;
	ns4 = (document.layers) ? true : false;
	ns6 = (document.getElementById && !document.all) ? true : false;
	var is_major = parseInt(navigator.appVersion);
	var is_minor = parseFloat(navigator.appVersion);
	var agt=navigator.userAgent.toLowerCase();
	var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
    var is_ie3    = (is_ie && (is_major < 4));
    var is_ie4    = (is_ie && (is_major == 4) && (agt.indexOf("msie 4")!=-1) );
    var is_ie5    = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
    is_ie5_5up =(is_ie && !is_ie3 && !is_ie4 && !is_ie5);

	var thin="padding-left:0.1cm; border-color:grey; border-style:solid; border-top-width:0px; border-left-width:1px; border-right-width:1px; border-bottom-width:0px; font-size: 8pt";
	var thick="padding-left: 0.1cm; border-style: solid; border-top-width: 0px; border-left-width: 0px; border-right-width: 0px; border-bottom-width: 0px; font-size: 10pt";

	max_options += 1;
	var i = 0;

	if (ns4){

		for (y=0; y<4; y++){
			if (isEmpty(y, max_options)) {
				i++;
				//lef=y*130+190;
				str='<table width="130" border="0" cellspacing="0" cellpadding="0"><tr><td><img src="../../images/space.gif" width="2" height="1" alt="" border="0"></td></tr>\n';
				str+='<tr><td background="../../images/bar_bg0.gif" height="19"><img src="../../images/space.gif" width="2" height="1" alt="" border="0"><font face="Verdana" size="2"><a style="text-decoration: none" href="javascript:move(\'c'+i+'\',40)"><b>'+menu[y][0]+'</b></a></font></td></tr>\n';
				ii = 0;
				for (x=1; x<max_options; x++){
					if (menu[y][x] != "") {
						str+='<tr><td background="../../images/menu_bg.gif"><img src="../../images/space.gif" width="5" height="1" alt="" border="0"><font face="Verdana" size="1"><a style="text-decoration: none" href="'+url[y][x]+'" target=eClassMain>'+menu[y][x]+'</a></font></td></tr>\n';
						ii ++;
					}
				}
				for (x=ii; x<max_options; x++) {
			    	str+='<tr><td background="../../images/menu_bg.gif">&nbsp;</td></tr>\n';
				}
				str+='<tr><td background="../../images/menu_bg.gif">&nbsp;</td></tr>\n';
				str+='</table>';

				cmd='writetolayer("c'+i+'",str)';
				eval(cmd);
			}
		}

	}else {

		for (y=0; y<4; y++){
			if (isEmpty(y, max_options)) {
				i++;
				//lef=y*130+190;
				str='<table width="130" border="0" cellspacing="0" cellpadding="0"><tr><td colspan=3><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td></tr>\n';
				str+='<tr><td colspan=3 style="'+thick+'" background="../images/bar_bg.gif" height="19"><a class="menuHead" href="javascript:move(\'c'+i+'\',40)"><b>'+menu[y][0]+'</b></font></a></td></tr>\n';
				ii = 0;
				for (x=1; x<max_options; x++){
					if (menu[y][x] != "") {
						str+='<tr><td><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td><td style="'+thin+'" bgcolor=white><a href="'+url[y][x]+'" target=eClassMain>'+menu[y][x]+'</a></td><td><img src="../images/space.gif" width="1" height="1" alt="" border="0"></td></tr>\n';
						ii ++;
					}
				}
				for (x=ii; x<max_options; x++) {
			    	str+='<tr><td><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td><td style="'+thin+'" bgcolor=white>&nbsp;</td><td><img src="../images/space.gif" width="1" height="1" alt="" border="0"></td></tr>\n';
				}
				str+='<tr><td><img src="../images/space.gif" width="2" height="1" alt="" border="0"></td><td style="'+thin+'" bgcolor=white>&nbsp;</td><td><img src="../images/space.gif" width="1" height="1" alt="" border="0"></td></tr>\n';
				str+='</table>';

				cmd='writetolayer("c'+i+'",str)';
				eval(cmd);
			}
		}

	}

}

function isEmpty(y, max_options) {
    for (x=1; x<max_options; x++){
		if (menu[y][x] != "")
			return true;
	}
	return false;
}
/* ########  TEXT VERSION  #############

Browser					Transparent Menu
---------------------------------------
IE 5 or above			Yes
NS 6					Yes
NS 4					Partial

##################################### */