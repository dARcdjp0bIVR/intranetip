// using by : henry
var formAllFilled = true;
// define a Answer Sheet class
function Answersheet()
{
  if (typeof(_answersheet_prototype_called) == 'undefined')
  {
        _answersheet_prototype_called = true;
        Answersheet.prototype.qString = null;
        Answersheet.prototype.aString = null;
        Answersheet.prototype.counter = 0;
        //Answersheet.prototype.changeCategory = "";
        Answersheet.prototype.answer = new Array();
        Answersheet.prototype.valueTmp = new Array();
        Answersheet.prototype.catIDTemp = new Array();
        Answersheet.prototype.amtTemp = new Array();
        Answersheet.prototype.templates = new Array();
        Answersheet.prototype.selects = new Array();
        Answersheet.prototype.sheetArr = sheetArr;
        Answersheet.prototype.writeSheet = writeSheet;
        Answersheet.prototype.stuffAns = stuffAns;
        Answersheet.prototype.move = move;
        Answersheet.prototype.chgNum = chgNum;
        Answersheet.prototype.secDes = secDes;
        Answersheet.prototype.mode = 0;                                // 0:create; 1:for fill-in
        Answersheet.prototype.templateNo = 0;                        // template list menu
     //Answersheet.prototype.convertType = convertType;
     	Answersheet.prototype.ansQty = 30;							// default answer quantity [20090310 yat] (default = 15)
  }

        function writeSheet(){
                var txtArr = this.answer;
                var valueArr = this.valueTmp;
                var valueCatArr = this.catIDTemp;
                var valueAmtArr = this.amtTemp;
                var selArr = this.selects;
                var arr=txtArr;
                var strTowrite='';
                var assignScript = "";
                

                if (this.mode==0) {
                	// Display Order
                        strTowrite+='<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
						+'<tr><td>'+(typeof(Part2)!='undefined'?Part2:'&nbsp;')+'</td></tr>\n'
                        +'<tr><td>'
                     
                        +'<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                        +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+(typeof(order_name)!="undefined"?order_name:"&nbsp;")+"</span></td>"
                        +'<td nowrap><form name="editForm">&nbsp; <select name="sList">';
                        for (x=0; x<arr.length; x++){
                                temTxt=arr[x][0];
                                strTowrite+='<option value="">'+cutStrLen(temTxt, 35);
                        }
                        strTowrite+='</select>'
                        +'&nbsp; <input type=button value="&uarr;" onclick="sheet.move(this.form.sList.selectedIndex, \'up\');writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&darr;" onclick="sheet.move(this.form.sList.selectedIndex, \'down\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&Chi;" onclick="sheet.move(this.form.sList.selectedIndex, \'out\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&harr;" onclick="sheet.secDes(this.form.sList.selectedIndex, \'out\'); "> </form></td></tr></table>'

                        +'<tr><td>'
                        +'<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                        +'</td></tr>'
                        
                 	+'</td></tr></table>'
                }
                
                var ansNum=1;
                //alert(paymentOptAry);
                txtStr=strTowrite+'<Form name="answersheet">\n';
				
                txtStr+='<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">'
			+'<tr><td>'+(typeof(Part3)!='undefined'?Part3:'&nbsp;')+'</td></tr>'
                        +'<tr><td>'
			+'<table width="100%" border="0" cellpadding="0" cellspacing="5">';
                	
                        for (x=0; x<txtArr.length; x++){
	                        
                        tit=recurReplace(">", "&gt;", txtArr[x][0]);
                                                tit = recurReplace("<", "&lt;", tit);
                                                tit = recurReplace("\n", "<br>", tit);
                        
                        
                        txtStr+='<tr><td align="center" bgcolor="#EFEFEF">';
                        
                        txtStr+='<table border="0" width="95%" align="center" bgcolor="#EFEFEF"><tr><td bgcolor="#EFEFEF" colspan="3" class="tabletext">\n';
                        //txtStr+=tit+'</td></tr>\n';
                        
                        for (y=1; y<txtArr[x].length; y++){
	                        
                                queArr=txtArr[x][y][0].split(",");
                              
                                //txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                switch (queArr[0]){
                                        case "1":		// True or False
	                                
                                        	
                                                preValue0 = (ansNum<=this.counter) ? valueArr[ansNum-1][0] : "";
                                                
                                                preValue1 = (ansNum<=this.counter) ? valueArr[ansNum-1][1] : "";
                                                //preValueCatID = (ansNum<=this.counter) ? valueCatArr[ansNum-1][0] : "";
                                                //alert(preValueCatID);
                                                /*
                                                preValuePaymentID = (ansNum<=this.counter) ? valueArr[ansNum-1][2] : "";
                                                preValueAmt = (ansNum<=this.counter) ? valueArr[ansNum-1][4] : "";
                                                */
                                                r_check0 = "";
                                                r_check1 = "";

                                                if (selArr.length>0) {

                                                    eval("r_check"+selArr[ansNum-1]+"='checked'");
                                                }
												strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 40; // total str length
						
												txtStr+=tit;
												if(this.mode==0)
												{
													selection = '<SELECT name="CatID'+ansNum+'" id="CatID'+ansNum+'">'+paymentOptions;
													for(k=0; k<paymentOptIDAry.length; k++) {
														selectFlag = (paymentOptIDAry[k]==valueCatArr[ansNum-1]) ? " selected" : "";
														selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
													}
													selection += '</SELECT>';
													txtStr+=selection;
												}
						                        
						                        //txtStr+=' <SELECT name="CatID'+ansNum+'" id="CatID'+ansNum+'">'+ paymentOptions +'</SELECT>'+valueCatArr[ansNum-1];
						                        if(this.mode == 0) {
						                        	txtStr+=' <input type="text" name="amt'+ansNum+'" id="amt'+ansNum+'" value="'+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+'">';
						                        } else {
							                     	txtStr += " ($"+((valueAmtArr[ansNum-1]!=undefined)?valueAmtArr[ansNum-1]:"")+")";
						                        }
						                        //txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentItemID+'">';
						                        txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentIDAry[ansNum-1]+'">';
						                        txtStr+='</td></tr>\n';
												txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
												
                                                txtStr+='<input type="radio" value="0" '+r_check0+' name="F'+ansNum+'" id="FF0'+ansNum+'">';
                                                txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue0+'" name="FD'+ansNum+'_0" size="25" class="tabletext">' : '<label for="FF0'+ansNum+'">'+preValue0+'</label>';
                                                txtStr+=(this.mode==0 || strLen>=40) ? '<br>' : ' &nbsp; &nbsp; ';
                                                txtStr+='<input type="radio" value="1" '+r_check1+' name="F'+ansNum+'" id="FF1'+ansNum+'">';
                                                txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue1+'" name="FD'+ansNum+'_1" size="25" class="tabletext">' : '<label for="FF1'+ansNum+'">'+preValue1+'</label>';
                                                //alert(paymentIDAry[ansNum-1]);
                                               	
                                                break;
                                        case "2":		// MC (single), i.e. radio button
                                                r_check_i = (selArr.length>0) ? parseInt(selArr[ansNum-1]) : "";
                                                
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                                
                                                txtStr+=tit;
                                                //txtStr+=' <SELECT name="CatID'+ansNum+'" id="CatID'+ansNum+'" id="CatID'+ansNum+'" id="CatID'+ansNum+'">'+ paymentOptions +'</SELECT>'+valueCatArr[ansNum-1];
                                                if(this.mode==0)
												{
													selection = '<SELECT name="CatID'+ansNum+'" id="CatID'+ansNum+'">'+paymentOptions;
													for(k=0; k<paymentOptIDAry.length; k++) {
														//tmpCat = (valueCatArr.length>ansNum && valueCatArr[ansNum-1][m]!=undefined) ? valueCatArr[ansNum-1][m] : "";
														selectFlag = (paymentOptIDAry[k]==valueCatArr[ansNum-1]) ? " selected" : "";
														selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
													}
													selection += '</SELECT>';
	
													txtStr+=selection;
												}
                                                //txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentItemID+'">';
                                                txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentIDAry[ansNum-1]+'">';
                                                txtStr+='</td></tr>\n';
                                                txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';

                                                for (m=0; m<txtArr[x][y][1]; m++){
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
                                                        if (r_check_i=='' && r_check_i!='0')
                                                        {

                                                            txtStr+='<input type="radio" value="'+m+'" name="F'+ansNum+'">';
                                                        }
                                                        else
                                                        {
                                                            txtStr+=(r_check_i==m) ? '<input type="radio" value="'+m+'" checked name="F'+ansNum+'">' : '<input type="radio" value="'+m+'" name="F'+ansNum+'">';
                                                        }
                                                        txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext"><br>' : preValue;
                                                        if (this.mode==1) {
                                                            txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
                                                                txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
                                                        }
                                                }
                                                break;
                                        case "3":		// MC Multiple , i.e. checkbox + payment category + amount field
                                        
                                                s_checkArr = null;
						                       	
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                               
						                        txtStr+=tit+'</td></tr>\n';
						                        txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                                if (selArr.length>0) {
                                                        s_checks = selArr[ansNum-1];
                                                        s_checkArr = s_checks.split(",");
                                                }
                                                
                                                for (m=0; m<txtArr[x][y][1]; m++){
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";

                                                        c_check = "";
                                                        if (s_checkArr!=null) {
                                                                for (var kk=0; kk<s_checkArr.length; kk++) {
                                                                        if (s_checkArr[kk]==m) {
                                                                                c_check = "checked";
                                                                                break;
                                                                        }
                                                                }
                                                        }
                                                        
                                                        
                                                        txtStr+= '<input type="checkbox" '+c_check+' name="F'+ansNum+'" value="'+m+'">';
                                                        txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext">' : preValue;
                                                        //txtStr+=' <SELECT name="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'" id="CatID'+ansNum+'">'+ paymentOptions +'</SELECT>'+valueCatArr[ansNum-1][m];
                                                        
                                                        if(this.mode==0)
														{
															selection = '<SELECT name="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'">'+paymentOptions;
															for(k=0; k<paymentOptIDAry.length; k++) {
																tmpCat = (valueCatArr.length>ansNum || valueCatArr[ansNum-1]!=undefined) ? valueCatArr[ansNum-1][m] : "";
																// Change by Ronald
																//tmpCat = (valueCatArr.length<=ansNum && valueCatArr[ansNum-1][m]!=undefined) ? valueCatArr[ansNum-1][m] : "";
																selectFlag = (paymentOptIDAry[k]==tmpCat) ? " selected" : "";
																selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
															}
															selection += '</SELECT>';
			
															txtStr+=selection;
														}
                                                        tmpAmt = (valueAmtArr.length>ansNum || valueAmtArr[ansNum-1]!=undefined) ? valueAmtArr[ansNum-1][m] : "";
                                                       	if(this.mode == 0)
														{
                                                        	// Change by Ronald
                                                       		//tmpAmt = (valueAmtArr.length<=ansNum && valueAmtArr[ansNum-1][m]!=undefined) ? valueAmtArr[ansNum-1][m] : "";
                                                        	txtStr+=' <input type="text" name="amt'+ansNum+'_'+m+'" id="amt'+ansNum+'_'+m+'" value="'+tmpAmt+'" size="10"><br>';
                                                        } else {
	                                                     	txtStr += ' ($'+tmpAmt+')';   
                                                        }
                                                        //alert(ansNum+'/'+m);
                                                        if (this.mode==1) {
                                                            txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
                                                                txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
                                                        }
                                                }
                                                //txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentItemID+'"><br>';
                                                tmpPaymentItemID = (paymentIDAry[ansNum-1]!=undefined) ? paymentIDAry[ansNum-1] : "";
                                                txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+tmpPaymentItemID+'">';
                                                break;
                                                /*
                                        case "4":
                                                t_filled = (selArr.length>0) ? selArr[ansNum-1] : "";
                                                
                                                txtStr+=tit+'</td></tr>\n';
                                                txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                                
                                                txtStr+='<input type="text" name="F'+ansNum+'" value="'+t_filled+'" class="tabletext">';
                                                break;
                                        case "5":
                                                t_filled = (selArr.length>0) ? selArr[ansNum-1] : "";
                                                
                                                txtStr+=tit+'</td></tr>\n';
                                                txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';
                                                
                                                txtStr+='<textarea name="F'+ansNum+'" cols="50" rows="3">'+recurReplace("<br>", "\n", t_filled)+'</textarea>';
                                                break;
                                        case "6":
                                                break;
                                                */
                                        case "9":		// MC Single, i.e. radio + payment category + amount field
                                        
                                                r_check_i = (selArr.length>0) ? parseInt(selArr[ansNum-1]) : "";
                                                
                                                strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
                                                
                                                txtStr+=tit+'</td></tr>\n';
                                                txtStr+='<td width="100%" class="tabletext" bgcolor="#EFEFEF">';

                                                for (m=0; m<txtArr[x][y][1]; m++){
	                                                
                                                        preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
                                                        if (r_check_i=='' && r_check_i!='0')
                                                        {

                                                            txtStr+='<input type="radio" value="'+m+'" name="F'+ansNum+'">';
                                                        }
                                                        else
                                                        {
                                                            txtStr+=(r_check_i==m) ? '<input type="radio" value="'+m+'" checked name="F'+ansNum+'">' : '<input type="radio" value="'+m+'" name="F'+ansNum+'">';
                                                        }
                                                        
                                                        txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="25" class="tabletext">' : preValue;
                                                        //txtStr+=' <SELECT name="CatID'+ansNum+'" id="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'">'+ paymentOptions +'</SELECT>'+valueCatArr[ansNum-1][m];
                                                        
                                                        if(this.mode==0)
														{
															selection = '<SELECT name="CatID'+ansNum+'_'+m+'" id="CatID'+ansNum+'_'+m+'">'+paymentOptions;
															for(k=0; k<paymentOptIDAry.length; k++) {
																tmpCat = (valueCatArr[ansNum-1]!=undefined && valueCatArr[ansNum-1][m]!='') ? valueCatArr[ansNum-1][m] : "";
																selectFlag = (paymentOptIDAry[k]==tmpCat) ? " selected" : "";
																selection += '<option value="'+paymentOptIDAry[k]+'" '+selectFlag+'>'+paymentOptNameAry[k]+'</option>';	
															}
															selection += '</SELECT>';
															txtStr+=selection;
														}
														
                                                        tmpAmt = (valueAmtArr[ansNum-1]!=undefined) ? valueAmtArr[ansNum-1][m] : "";
														if(this.mode == 0) {
                                                        	txtStr+=' <input type="text" name="amt'+ansNum+'_'+m+'" id="amt'+ansNum+'_'+m+'" value="'+tmpAmt+'" size="10"><br>';
                                                        } else {
	                                                        txtStr += ' ($'+tmpAmt+')';
                                                        }
                                                        
                                                        if (this.mode==1) {
                                                            txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
                                                                txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
                                                        }
                                                }
                                                //txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+paymentItemID+'">';
                                                tmpPaymentItemID = (paymentIDAry[ansNum-1]!=undefined) ? paymentIDAry[ansNum-1] : "";
                                                txtStr+=' <input type="hidden" name="paymentItemID'+ansNum+'" id="paymentItemID'+ansNum+'" value="'+tmpPaymentItemID+'">';
                                                break;
                                }
                                tArr=txtArr[x][y];
                                txtStr+= (queArr[0]=="2" || queArr[0]=="3" || queArr[0]=="6") ? '</td>' : '<br></td>';
                                ansNum++;
                        }
                        txtStr+='</table>\n';
                        txtStr+='</td></tr>\n';
                }
                
                txtStr+='</td></tr></table>';
                txtStr+='</td></tr></table>';
                this.counter = ansNum-1;
                txtStr+='</Form>\n';
                
                return txtStr;
        }

        

/* internal function for conversion and operations */
        function pushUp(arr){
                for (x=0; x<arr.length-1; x++){
                        arr[x]=arr[x+1];
                }
                arr.length=arr.length-1;
                return arr;
        }


        //initialization
        function sheetArr(){
                var txtStr = this.qString;
                var txtArr = txtStr.split("#QUE#");
                var tmpArr = null;
                var resultArr = new Array();
                var valueTemp = new Array();
                var catIDTemp = new Array();
                var amtTemp = new Array();

                txtArr=pushUp(txtArr);
                this.counter = txtArr.length;

                for (var x=0; x<txtArr.length; x++){
                        tmpArr = txtArr[x].split("||");
	                    //if(tmpArr.length!=0) {
	                        type_no = tmpArr[0].split(",");
	                        question = tmpArr[1];
	                        opts = tmpArr[2];
	                        itemIDs = tmpArr[3];
	                        catIDs = tmpArr[4];
	                        amts = tmpArr[5];
	                    /*} else {
	                        type_no = "";
	                        question = "";
	                        opts = "";
	                        itemIDs = "";
	                        catIDs = "";
	                        amts = "";
	                    }*/
                        
                        var j = x;

                        // question
                        resultArr[j] = new Array();
                        resultArr[j][0] = recurReplace("<br>", "\n", question);
                        resultArr[j][1] = new Array();
                        resultArr[j][1][0] = type_no[0];
                        if (type_no.length>1) {
                            resultArr[j][1][1] = type_no[1];
                        }

                        // option values
                        valueTemp[j] = new Array();
                        tmpArr = opts.split("#OPT#");
                        //alert(tmpArr);
                        if (tmpArr.length>1) {
                                for (var m=1; m<tmpArr.length; m++){
                                        valueTemp[j][m-1] = tmpArr[m];
                                }
                        }
                        // payment category ID values
                        catIDTemp[j] = new Array();
                        tmpArr = catIDs.split("#CATEGORYID#");
                        //alert(tmpArr[0]+'/'+tmpArr[1]);
                        if (tmpArr.length>1) {
                                for (var m=1; m<tmpArr.length; m++){
                                        catIDTemp[j][m-1] = tmpArr[m];
                                        //alert(j+'/'+(m-1)+'/'+tmpArr[m]);
                                }
                        }
                        
						
                        // amount values
                        amtTemp[j] = new Array();
                        if(amts!=undefined) {		// some reply slip format maybe not have "amount" option
	                        tmpArr = amts.split("#AMT#");
	                        if (tmpArr.length>1) {
	                                for (var m=1; m<tmpArr.length; m++){
	                                        amtTemp[j][m-1] = tmpArr[m];
	                                }
	                        }
	                    } else {
		                 	amtTemp[j][0] = "";   
	                    }
	                    

                }

                if (this.mode==1 && this.aString!=null) {
                    this.stuffAns();
                }

                this.valueTmp = valueTemp;
                this.catIDTemp = catIDTemp;
                this.amtTemp = amtTemp;
                
                return resultArr;
          }


        function stuffAns(){
                var ansStr=this.aString;
                ansArr=ansStr.split("#ANS#");
                this.selects = pushUp(ansArr);
          }


        function move(i, dir){
                var arr = this.answer;
                var temArr = new Array();

                retainValues();
                var valueArr = this.valueTmp;
                
                var tvArr = new Array();

                switch (dir){
                        case "up":
                                if (i!=0 && i<arr.length){
                                        //swap question
                                        temArr = arr[i];
                                        arr[i] = arr[i-1];
                                        arr[i-1] = temArr;

                                        //swap current fill-in-value
                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i-1];
                                        valueArr[i-1] = temArr;
                                };
                                break;
                        case "down":
                                if (i!=(arr.length-1)&&i>=0){
                                        temArr = arr[i];
                                        arr[i] = arr[i+1];
                                        arr[i+1] = temArr;

                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i+1];
                                        valueArr[i+1] = temArr;
                                };
                                break;
                        case "out":
								if(arr.length>0)
								{
									for (x=i; x<arr.length; x++) {
											arr[x]=arr[x+1];
											valueArr[x]=valueArr[x+1];
									}
									arr.length = arr.length-1;
									valueArr.length = valueArr.length-1;
								}
                                break;
                }
                this.answer = arr;
                this.valueTmp = valueArr;
        }


        function secDes(i){
                var arr=this.answer;

				if(typeof(arr[i])!="undefined")
				{    
					var form_pop_up = window.open("", "form_pop_up", "toolbar=no,location=no,status=no,menubar=no,resizable,width=400,height=200,top=100,left=100");

					var JSfunction = "<script language='javascript'>";
					JSfunction += "function updateDesc(ind, fobj) {";
					JSfunction += "var arrD = window.opener.sheet.answer;";
					JSfunction += "arrD[ind][0] = fobj.new_desc.value;";
					JSfunction += "window.opener.sheet.answer = arrD;";
					JSfunction += "window.opener.retainValues();";
					JSfunction += "window.opener.writetolayer('blockInput',window.opener.sheet.writeSheet());";
					JSfunction += "self.close();";
					JSfunction += "} </script>";
				
					var desc_form = "<HTML><head><title>"+chg_title+"</title></head><body bgcolor='#EFECE7'><form name='form1'>";
					desc_form += "<br>"+JSfunction+"<table border='0' align='center'>";
					desc_form += "<tr><td><textarea name='new_desc' rows='5' cols='40'>"+arr[i][0]+"</textarea></td></tr>";
					desc_form += "<tr><td align='right'><input type='button' onClick='updateDesc("+i+", this.form);' value='"+button_update+"'> <input type='button' onClick='self.close()' value='"+button_cancel+"'></td></tr>";
					desc_form += "</table></form></body></HTML>";
					form_pop_up.document.write(desc_form);
				}

                return;
        }


        function chgNum(i){
                arr=this.answer;
                num=arr[i].length-1;
                var changeNum = prompt ("Change the number of questions from "+num+" to:","");
                if (changeNum){
                    if (isInteger(changeNum) && changeNum>0){
                            if (changeNum>num){
                                    for (n=num; n<changeNum; n++)
                                             arr[i][arr[i].length]=arr[i][arr[i].length-1];
                            }
                            if (changeNum<num){
                                    diff=num-changeNum;
                                    for (n=0; n<diff; n++)
                                        arr[i].length--;
                            }
                     }
                     else
                             alert("your input is not a valid number");
        		}
        }

}
/*
function changeCatItem() {
	return sheet.changeCategory;
}
*/
function editPanel(){
        answer = "";
        if (sheet.mode==0) {
                answer += '<DIV ID="blockDiv">\n'
                +'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
				+'<tr><td>'+(typeof(Part1)!='undefined'?Part1:'&nbsp;')+'</td></tr>\n'
                +'<tr><td><table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                +'<form name="addForm">\n'

                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_template+"</span></td>"
                +'<td class="tabletext">'+getTemplate()+'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_header+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_type+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==2||this.selectedIndex==3||this.selectedIndex==4) this.form.oNum.selectedIndex=3; else this.form.oNum.selectedIndex=0">\n'
                +'                <option>- '+answersheet_type+' -\n'
                +'                <option value="1">'+answersheet_tf+'\n'
                +'                <option value="2">'+answersheet_mc+'\n'
                +'                <option value="9">'+answersheet_mc2+'\n'
                +'                <option value="3">'+answersheet_mo+'\n'
                +'                </select>\n'
                +'</td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_option+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=2&&this.form.qType.selectedIndex!=3&&this.form.qType.selectedIndex!=4){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option>- '+answersheet_option+' -\n';
                
                // [20090310 yat]
                for(qi=3;qi<=sheet.ansQty;qi++)
                	answer +='                <option value='+qi+'>'+qi+'\n';
                	/*
                +'                <option value=3>3\n'
                +'                <option value=4>4\n'
                +'                <option value=5>5\n'
                +'                <option value=6>6\n'
                +'                <option value=7>7\n'
                +'                <option value=8>8\n'
                +'                <option value=9>9\n'
                +'                <option value=10>10\n'
                +'                <option value=11>11\n'
                +'                <option value=12>12\n'
                +'                <option value=13>13\n'
                +'                <option value=14>14\n'
                +'                <option value=15>15\n'
                */
                answer +='  </select>\n'
                +'</td></tr></table></td></tr>\n'
                +'<tr><td>'
				+ '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                + '</td></tr>'
				+ '<tr><td colspan="2" align="center">'+(typeof(add_btn)!='undefined'?add_btn:'&nbsp;')+'</td></tr>\n'
                + '        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet());\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}
/*
// Added by Kelvin Ho on 10 Feb 09.
function editPaneleDiscipline(){
        answer = "";
        if (sheet.mode==0) {
                answer += '<DIV ID="blockDiv">\n'
                +'<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">\n'
				+'<tr><td>'+(typeof(Part1)!='undefined'?Part1:'&nbsp;')+'</td></tr>\n'
                +'<tr><td><table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">'
                +'<form name="addForm">\n'

                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_template+"</span></td>"
                +'<td class="tabletext">'+getTemplate()+'</td></tr>\n'
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_header+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <textarea name="secDesc" class="tabletext" rows="3" cols="30"></textarea></td></tr>\n'
                
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_type+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==2||this.selectedIndex==3) this.form.oNum.selectedIndex=3; else this.form.oNum.selectedIndex=0">\n'
                +'                <option>- '+answersheet_type_selection+' -\n'
                +'                <option>'+answersheet_tf+'\n'
                +'                <option>'+answersheet_mc+'\n'
                +'                <option>'+answersheet_mo+'\n'
                +'                <option>'+answersheet_mc2+'\n'
                +'                </select>\n'
                +'</td></tr>\n'
                +"<tr><td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle'><span class='tabletext'>"+answersheet_option+"</span></td>"
                +'<td class="tabletext">\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=2&&this.form.qType.selectedIndex!=3){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option>- '+answersheet_selection+' -\n';
                
                // [20090310 yat]
                for(qi=3;qi<=sheet.ansQty;qi++)
                	answer +='                <option value='+qi+'>'+qi+'\n';
                	
                
                answer +='  </select>\n'
                +'</td></tr></table></td></tr>\n'
                +'<tr><td>'
				+ '<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center"><tr><td class="dotline" colspan="2">'+(typeof(space10)!='undefined'?space10:'&nbsp;')+'</td></tr></table>'
                + '</td></tr>'
				+ '<tr><td colspan="2" align="center">'+(typeof(add_btn)!='undefined'?add_btn:'&nbsp;')+'</td></tr>\n'
                + '        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet());\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}
*/


function getTemplate() {
        var tmpArr = sheet.templates;
    var xStr = '<select name="fTemplate" onchange="if (confirm(chg_template)) {changeTemplate(this.form.fTemplate.selectedIndex); this.form.secDesc.focus();} else {this.form.fTemplate.selectedIndex=sheet.templateNo;}">';

        xStr += '<option value="">-'+answersheet_template+'-</option>\n';
        for (var i=0; i<tmpArr.length; i++) {
            xStr += '<option value="'+i+'">'+tmpArr[i][0]+'</option>\n';
        }
        xStr += '</select>';

        return xStr;
}


function changeTemplate(index) {
        if (index!=0) {
                sheet.qString = sheet.templates[index-1][1];
                sheet.answer = sheet.sheetArr();
        } else {
            sheet.qString = "";
                sheet.answer = "";
        }
        sheet.templateNo = index;
        writetolayer("blockInput",sheet.writeSheet());
}


function appendTxt(formName){
        var formObj=eval("document."+formName);
        var arr=new Array();
        arr[0]=formObj.secDesc.value;

        /* check empty tocie/title */
        if(formObj.secDesc.value=='')
        {
	        alert(pls_fill_in_title);
			formObj.secDesc.focus();
			return false;
        }
        
        //qtype=String(formObj.qType.selectedIndex);
        qtype = String(formObj.qType.value);
        if (qtype==0){
                alert(pls_specify_type);
                formObj.qType.focus();
                return false;
        }

        for (x=0; x<1; x++){
                y=x+1;
                arr[y]=new Array();
                arr[y][0]=qtype;
                if (qtype=="2" || qtype=="3" || qtype=="9") 
                	arr[y][1]=String(formObj.oNum.options[formObj.oNum.selectedIndex].value);
        }
        sheet.answer[sheet.answer.length]=arr;

        return true;
}

if (need2checkform==undefined)
{
    var need2checkform = false;
}

function getAns(i){
        var eleObj = eval("document.answersheet.F"+i);
        var strAns = null;
        var tempAns = null;

        if (eleObj==undefined)
        {
            return '';
        }

        if (eleObj.length){
                switch(eleObj[0].type){
                        case "radio":
                                for (p=0; p<eleObj.length; p++){
                                        if (eleObj[p].checked)
                                        {
                                                tempAns += p;
                                        }
                                }
                                if (need2checkform && tempAns == null )
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                if (tempAns != null)
                                {
                                    strAns += tempAns;
                                }
                                break;

                        case "checkbox":
                                for (p=0; p<eleObj.length; p++) {
                                        if (eleObj[p].checked)
                                        {
                                            if (tempAns != null)
                                            {
                                                tempAns += ","+p;
                                            }
                                            else
                                            {
                                                tempAns = p;
                                            }
                                        }
                                }
                                if (need2checkform && tempAns == null)
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                strAns = tempAns;
                                break;

                        case "text":
                                for (p=0; p<eleObj.length; p++){
                                        tempAns += recurReplace('"', '&quot;', eleObj.value);
                                }
                                if (need2checkform && (tempAns == null || tempAns==''))
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                strAns += tempAns;
                                break;
                }
        } else
        {
                tempAns = recurReplace('"', '&quot;', eleObj.value);
                if (need2checkform && (tempAns == null || tempAns==''))
                {
                    formAllFilled = false;
                    return false;
                }
                strAns = tempAns;
        }
        return strAns;
}


function finish(){
        var txtStr="";
        var ansStr="";
        var arr=sheet.answer;
        var ansNum=1;
        var temp = null;
		var result = true;
        formAllFilled = true;
        
        if (sheet.mode==0) {
                // get questions
                
                for (var x=0; x<arr.length; x++){
                        //txtStr+="#QUE#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //append options
                                queArr = arr[x][y][0].split(",");
                                switch (queArr[0]){
                                        case "1":        myLen=2;break;
                                        case "4":
                                        case "5":
                                        case "6":        myLen=0;break;
                                        default:		myLen=arr[x][y][1]; break;
                                }
                                txtStr+="#QUE#";
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr += (z>0) ? ","+arr[x][y][z] : arr[x][y][z];
                                        //alert(arr[x][y][z]);
                                }
                                txtStr+="||"+arr[x][0]+"||";
                                for (var m=0; m<myLen; m++){
                                        optDescription = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        if(eval("document.answersheet.FD"+ansNum+"_"+m+".value")=="") result = false;
                                        txtStr+="#OPT#"+optDescription;
                                }
								// add payment category ID and amount to the string
								switch(queArr[0]) {
									case "1":	
											//alert(eval("document.answersheet.paymentItemID"+ansNum+".value"));
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!='undefined') ? eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											txtStr+='#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+".value");
											txtStr+='||#AMT#'+eval("document.answersheet.amt"+ansNum+".value");
											if(eval("document.answersheet.CatID"+ansNum+".value")=="") result = false;
											if(eval("document.answersheet.amt"+ansNum+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+".value")) || eval("document.answersheet.amt"+ansNum+".value")<0)) result = false;
											break;
									case "2":
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!='undefined') ? eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											txtStr+='#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+".value");
											if(eval("document.answersheet.CatID"+ansNum+".value")=="") result = false;
											break;
									case "3":
											var catString = "";
											var amtString = "";
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!="") ? eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											for (var m=0; m<myLen; m++){
												catString += '#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+"_"+m+".value");
												amtString += '#AMT#'+eval("document.answersheet.amt"+ansNum+"_"+m+".value");
												if(eval("document.answersheet.CatID"+ansNum+"_"+m+".value")=="") result = false;
												if(eval("document.answersheet.amt"+ansNum+"_"+m+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_"+m+".value")) || eval("document.answersheet.amt"+ansNum+"_"+m+".value")<0)) result = false;
											}
											txtStr+=catString+"||"+amtString;
											break;
									case "9":
											var catString = "";
											var amtString = "";
											txtStr += (eval("document.answersheet.paymentItemID"+ansNum+".value")!="") ? eval("document.answersheet.paymentItemID"+ansNum+".value")+"||" : "||#PAYMENTITEMID#1||";
											for (var m=0; m<myLen; m++){
												catString += '#CATEGORYID#'+eval("document.answersheet.CatID"+ansNum+"_"+m+".value");
												amtString += '#AMT#'+eval("document.answersheet.amt"+ansNum+"_"+m+".value");
												if(eval("document.answersheet.CatID"+ansNum+"_"+m+".value")=="") result = false;
												if(eval("document.answersheet.amt"+ansNum+"_"+m+".value")=="" || (isNaN(eval("document.answersheet.amt"+ansNum+"_"+m+".value")) || eval("document.answersheet.amt"+ansNum+"_"+m+".value")<0)) result = false;
											}
											txtStr+=catString+"||"+amtString;
											break;
									
									default	:	break;
												
								}
								
								
                                ansNum++;
                        }
                }
                document.ansForm.qStr.value=txtStr;
        } else if (sheet.mode==1) {

                // get answers
            for (var x=0; x<arr.length; x++){
                        queArr = arr[x][1][0].split(",");
                        temp = getAns(x+1);
                        if (formAllFilled==false)
                        {
                            return false;
                        }
                        txtStr+=(queArr[0]=="6") ? "#ANS#" : "#ANS#"+temp; //getAns(x+1);
                }
                document.ansForm.aStr.value=txtStr;
        }
        
        return result;

}


function retainValues(){
                var txtStr="";
                var ansStr="";
                var arr=sheet.answer;
                var ansNum=1;
                var valueTemp = new Array();

                for (var x=0; x<arr.length; x++){
                        valueTemp[x] = new Array();
                        //txtStr+="#SEC#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //txtStr+="#QUE#";
                                /*
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr+=arr[x][y][z]+"||";
                                }
                                */
                                //append options
                                queArr = arr[x][y][0].split(",");

                                switch (queArr[0]){
                                        case "1":        myLen=2; break;
                                        case "4":
                                        case "5":
                                        case "6":        myLen=0; break;
                                        default:        myLen=arr[x][y][1]; break;
                                }
                                //alert(queArr[0]+'/'+myLen);
                                for (var m=0; m<myLen; m++){
                                        tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        valueTemp[x][m] = recurReplace('"', '&quot;', tmpVal);
                                }
                                ansNum++;
                        }
                }
                sheet.valueTmp = valueTemp;
}


function recurReplace(exp, reby, txt) {
    while (txt.search(exp)!=-1) {
        txt = txt.replace(exp, reby);
    }
        return txt;
}




// the following three functions are added for integer validation



function isInteger(s){
        var i;
    if (isEmpty(s))
                if (isInteger.arguments.length == 1) return false;
                else return (isInteger.arguments[1] == true);
    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}



function isEmpty(s)
{   return ((s == null) || (s.length == 0))
}


function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}

function getStrLen(tmpArr) {
        var tmpV=0;
        if (tmpArr.length<7) {
                for (var i=0; i<tmpArr.length; i++) {
                        for (var j=0; j<tmpArr[i].length; j++) {
                                tmpV += (tmpArr[i].charCodeAt(j)>1000) ? 2 : 1;
                        }
                }
        } else {
                tmpV = 60;
        }

        return tmpV;
}

function cutStrLen(xStr, xLen) {
        var tmpArr = new Array(xStr);
        var tmpStr = "";
        var xCount = 0;

    if (getStrLen(tmpArr)>xLen) {
                for (var j=0; j<xStr.length; j++) {
                        xCount += (xStr.charCodeAt(j)>1000) ? 2 : 1;
                        tmpStr += xStr.charAt(j);
                        if (xCount>xLen-3) {
                            break;
                        }
                }
        xStr = tmpStr+"...";
    }
        return xStr;
}