/* Customize Fucntion for fckeditor [function name is the same of old HTML editor] */
/* Omas Move from EJ 2015-06-09 */

/* ---------------------------------------------------------------------- *\
  Function    : editor_getHTML
  Description : Get the HTML content of the editor
\* ---------------------------------------------------------------------- */
function editor_getHTML(instanceName){
	var oEditor = FCKeditorAPI.GetInstance(instanceName) ;
	return oEditor.GetXHTML(true);
}

/* ---------------------------------------------------------------------- *\
  Function    : editor_setHTML
  Description : Set the HTML content of the editor
\* ---------------------------------------------------------------------- */
function editor_setHTML(instanceName, text){
	var oEditor = FCKeditorAPI.GetInstance(instanceName) ;
	oEditor.SetHTML(text);
}

/* ---------------------------------------------------------------------- *\
  Function    : word_count
  Description : count Chinese letters and English words
\* ---------------------------------------------------------------------- */
function word_count(instanceName){
	//var editor_obj  = document.all["_" +objname+  "_editor"];
	//var editdoc 	= editor_obj.contentWindow.document;
    //var sRange  	= editdoc.selection.createRange();
    //var sHtml   	= sRange.text;
    //sHtml 			= (typeof(sHtml)!="undefined") ? sHtml : " ";

	//var wStr 	  	= (sHtml!="") ? sHtml : editor_getText(objname);
	var wStr 	  	= editor_getHTML(instanceName);
	wStr 		  	= wStr.replace( /<[^<|>]+?>/gi,'' );
	wStr 		  	= wStr.replace(/\r\n|\n|\r/g, ' ');
	var tmpA 	  	= wStr.split(" ");
	var total 	  	= 0;
	var total_sym 	= 0;
	var isEngWord 	= false;

	for (var i=0; i<tmpA.length; i++)
	{
		tmpV = tmpA[i];
		isEngWord = false;
		for (var j=0; j<tmpV.length; j++)
		{
			charCode = tmpV.charCodeAt(j);
			if (charCode==39 || charCode==45 || charCode==95 || (charCode>=48 && charCode<=57) || (charCode>=65 && charCode<=90) || (charCode>=97 && charCode<=122) || (charCode>=65296 && charCode<=65305)  || (charCode>=65313 && charCode<=65338) || (charCode>=65345 && charCode<=65370))
			{
				if (!isEngWord)
				{
					isEngWord = true;
					total ++;
				}
			} else if (charCode>255)
			{
				isEngWord = false;
				if (!(charCode>=65070 && charCode<=65375) && !(charCode>=12288 && charCode<=12345) && !(charCode>=8204 && charCode<=8231))
				{
					total ++;
				} else
				{
					total_sym ++;
				}
			} else
			{
				isEngWord = false;
				total_sym ++;
			}
		}
	}
	//alert("total # of words:\n     " + total + "\n" + String.fromCharCode(65296,65410,65305));
	//var tmpa = "-_";
	//alert(tmpa.charCodeAt(1));
	var ro = new Object();
	ro.total = total;
	ro.symbol = total_sym;
	return ro;
}