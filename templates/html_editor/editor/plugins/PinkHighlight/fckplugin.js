function PinkHighlight()
{
}
PinkHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'pink');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
PinkHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('PinkHighlight', new PinkHighlight());

// Add the button.
var item = new FCKToolbarButton('PinkHighlight', 'Pink Highlight');
item.IconPath = FCKConfig.PluginsPath + 'PinkHighlight/remark_highlight_icon_10_on.png';
FCKToolbarItems.RegisterItem('PinkHighlight', item);
