// Our method which is called during initialization of the toolbar.
function NoticeTemplateIcon()
{
}

// Disable button toggling.
NoticeTemplateIcon.prototype.GetState = function()
{
	return FCK_TRISTATE_OFF;
}

// Our method which is called on button click.
NoticeTemplateIcon.prototype.Execute = function()
{
	window.open('http://www.kindblad.com');
}

// Register the command.
FCKCommands.RegisterCommand('NoticeTemplateIcon', new NoticeTemplateIcon());

// Add the button.
var item = new FCKToolbarButton('NoticeTemplateIcon', 'Open URL');
item.IconPath = FCKPlugins.Items['NoticeTemplateIcon'].Path + 'openurl.gif';
FCKToolbarItems.RegisterItem('NoticeTemplateIcon', item);