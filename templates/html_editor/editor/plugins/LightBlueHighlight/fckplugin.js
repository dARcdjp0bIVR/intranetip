function LightBlueHighlight()
{
}
LightBlueHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'lightblue');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
LightBlueHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('LightBlueHighlight', new LightBlueHighlight());

// Add the button.
var item = new FCKToolbarButton('LightBlueHighlight', 'Light Blue Highlight');
item.IconPath = FCKConfig.PluginsPath + 'LightBlueHighlight/remark_highlight_icon_06_on.png';
FCKToolbarItems.RegisterItem('LightBlueHighlight', item);
