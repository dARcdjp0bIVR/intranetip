function YellowHighlight()
{
}
YellowHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'yellow');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
YellowHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('YellowHighlight', new YellowHighlight());

// Add the button.
var item = new FCKToolbarButton('YellowHighlight', 'Yellow Highlight');
item.IconPath = FCKConfig.PluginsPath + 'YellowHighlight/remark_highlight_icon_03_on.png';
FCKToolbarItems.RegisterItem('YellowHighlight', item);
