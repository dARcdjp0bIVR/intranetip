function PurpleHighlight()
{
}
PurpleHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'purple');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
PurpleHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('PurpleHighlight', new PurpleHighlight());

// Add the button.
var item = new FCKToolbarButton('PurpleHighlight', 'Purple Highlight');
item.IconPath = FCKConfig.PluginsPath + 'PurpleHighlight/remark_highlight_icon_09_on.png';
FCKToolbarItems.RegisterItem('PurpleHighlight', item);
