function RedHighlight()
{
}
RedHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	parent.GetSelectedText_Highlight(oFCKeditor, 'red');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;

}
RedHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('RedHighlight', new RedHighlight());

// Add the button.
var item = new FCKToolbarButton('RedHighlight', 'Red Highlight');
item.IconPath = FCKConfig.PluginsPath + 'RedHighlight/remark_highlight_icon_01_on.png';
FCKToolbarItems.RegisterItem('RedHighlight', item);
