/*
 * Customized by broadlearning
 * Created on 2009-9-4 by Jason Lam
 * Modified on 2011-04-20 by Jason to fix problem of specialchars including in the path
 * Modified on 2009-9-8 by Shing to make it work.
 * Aim: add an insert image function by flash 
*/

var relative_path = "../../";
var return_image ;
var cur_height;
var cur_width;
// This function is called by the flash after updated the image.
function flashUploadResult(result, msg, path, type, size, createDate, modDate, creator, fileW, fileH){
	//alert("result: "+result+" / msg: "+msg+" / path: "+path+" / type: "+type+" / size: "+size+" / createion date: "+createDate+" / modification date: "+modDate+" / creator: "+creator+"/ fileW : "+fileW+" / fileH : "+fileH);
	
	if (result) {

		/* Implementation note: change "../../" replace path and /eclass40/ */
		// Change path from "RELATIVE" --> "ABSOLUTE"
		path = path.replace(relative_path, '');
		//path = "/eclass40/"+path;
		path = "/"+path;
		UpdateImageSize(path);		
		// Calculate the height & width according to the maximum height & width specified
/*		var aryDimension = calculate_file_height(225,300,fileW,fileH);
		fileW =  aryDimension['width'];
		fileH =  aryDimension['height'];
*/				
		setTimeout(
				function(){				
					
					//encodeURI - encode Chinese words, 2nd replace function to escape special characters
					//|~|\=|\[|\]|\|
					var encode_path = encodeURI(path).replace(/'|~|@|&|;|\(|\)|!|#|\$|~|\=|\[|\]|\|/g, function(s) {
						return escape(s);
					})
					.replace('%27', "'");
					
					FCK.InsertHtml('<img src="' + encode_path + '" width="'+cur_width+'" height="'+cur_height+'" border="0" />');					
				}, 1000
		); //End Timeout
		
	}//End if 
}
function UpdateImageSize(path){
	return_image = document.createElement( 'IMG' ) ;	// new Image() ;
	return_image.src = path;
	return_image.onload = function()
	{
		var aryDimension = calculate_file_height(225,300,return_image.width,return_image.height);
		cur_width =  aryDimension['width'];
		cur_height =  aryDimension['height'];
	}
	

	
}
function calculate_file_height(max_h, max_w, fileW, fileH){
	var aryDimension = new Array();
	var max_h = parseInt(225);
	var max_w = parseInt(300);
	
	fileW = parseInt(fileW);
	fileH = parseInt(fileH);
	
	if(fileW > max_w){
		fileH = (max_w/fileW) * fileH;
		fileW = max_w;
	}

	if(fileH > max_h){
		fileW = (max_h / fileH)*fileW;
		fileH = max_h;
	}
	
	aryDimension['height'] 	= fileH;
	aryDimension['width'] 	= fileW;
	return aryDimension;
}


// Take reference of FCKToolbarButton, FCKToolbarButtonUI.
function InsertImageFlash()
{
}

// Just insert the flash object to the toolbar.
InsertImageFlash.prototype.Create = function( targetElement )
{
	


	var oDoc = FCKTools.GetElementDocument( targetElement ) ;
	
	// Create the Main Element.
	var oMainElement = this.MainElement = oDoc.createElement( 'DIV' ) ;
	oMainElement.setAttribute('id', 'flashuploadDIV');
	targetElement.appendChild( oMainElement ) ;
	
	// Include a Javascript for embedding flash.
	// The following may cause problem in Firefox, so just include swfobject.js in fckeditor.html.
	//var script = oDoc.createElement('script');
	//script.setAttribute('src', '/eclass40/src/includes/js/swfobject.js');
	//oDoc.appendChild(script);
	
	// Embed the flash to the element.
	// Calling embedFlash directly seems not work.
	
	//var text_imgDir = relative_path+FCKConfig.FlashImageInsertPath;
	var text_imgDir = FCKConfig.FlashImageInsertPath;
	
	setTimeout(
		function(){
			var flashvars = {
				// It seems that imgDir has two overloaded uses here:
				// 1. Used for saving image. Absolute path is a file system path. Relative path is relative to the directory of uploadFlashImage.php, which seems in the same directory of flashupload.swf.
				// 2. Used for displaying image. Absolute path is a web path. Relative path is relative to the directory of the page using FCKeditor when editing. Relative path is relative to the directory of the page displaying the image.
				// Therefore, absolute path is normally not OK; Using relative path needs to be careful.
				imgDir: text_imgDir//'files/tmp/flashupload/'
				//imgDir: relative_path+'files/tmp/flashupload/'
			};
			
			if (parent.imgdir) {
				// Override the image directory if requested.
				flashvars.imgDir = parent.imgdir;
			}
			var params = {
				play: "true",
				loop: "true",
				menu: "true",
				quality: "high",
				//scale: "showall",
				scale: "noscale",
				wmode: "transparent", 
				bgcolor: "#ffffff",
				//devicefont: "false",
				allowscriptaccess: "sameDomain",
				allowfullscreen: "false"
			};
			var attributes = {
				id: "flashupload",
				name: "flashupload",
				align: "middle"
			};

			//swfobject.embedSWF("<?=$PATH_WRT_ROOT?>src/tool/flashupload/flashupload.swf", "flashuploadDIV", "10px", "10px", "9.0.0", "<?=$PATH_WRT_ROOT?>src/includes/js/expressInstall.swf", flashvars, params, attributes);
			// Please put flashupload.swf and uploadFlashImage.php into the same directory as fckeditor.html.
			//swfobject.embedSWF("flashupload.swf", "flashuploadDIV", "14px", "14px", "9.0.0", "/eclass40/src/includes/js/expressInstall.swf", flashvars, params, attributes);
			// Not really, put flashupload.swf and uploadFlashImage.php into the same directory level as those pages which make use of FCKeditor.
			//swfobject.embedSWF("/eclass40/src/tool/flashupload/flashupload.swf", "flashuploadDIV", "14px", "14px", "9.0.0", "/eclass40/src/includes/js/expressInstall.swf", flashvars, params, attributes);
			swfobject.embedSWF("/templates/flashupload/flashupload_image.swf", "flashuploadDIV", "14px", "14px", "9.0.0", "/templates/swf_object/expressInstall.swf", flashvars, params, attributes);
		}, 0
	);
}

// Not sure whether the following methods will be called.
InsertImageFlash.prototype.RefreshState = function() {}
InsertImageFlash.prototype.Click = function() {}
InsertImageFlash.prototype.Enable = function() {}
InsertImageFlash.prototype.Disable = function() {}

// Add the button.
FCKToolbarItems.RegisterItem('InsertImageFlash', new InsertImageFlash());

