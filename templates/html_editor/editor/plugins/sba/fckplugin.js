function SbaCommand(){
    this.Name = window.parent.lang_insertSbaElement;
    this.Execute = function(action){       
        window.parent.loadInsertMenu(FCK.Name); 
    }
    this.GetState = function(){
        return FCK_TRISTATE_OFF; // FCK_TRISTATE_OFF or FCK_TRISTATE_ON
    }
}


FCKCommands.RegisterCommand( 'sba' , new SbaCommand() ) ;
FCKToolbarItems.RegisterItem('sba' , new FCKToolbarButton('sba', window.parent.lang_insertSbaElement, window.parent.lang_insertSbaElement, FCK_TOOLBARITEM_ONLYTEXT) ) ;
