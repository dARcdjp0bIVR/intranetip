function DarkBlueHighlight()
{
}
DarkBlueHighlight.prototype.Execute = function(){ 
	// Take an undo snapshot before changing the document
	FCKUndo.SaveUndoStep() ;
	
	var oFCKeditor = FCK;
	
	parent.GetSelectedText_Highlight(oFCKeditor, 'darkblue');
	
	FCKUndo.SaveUndoStep() ;
	
	FCK.Focus() ;
	FCK.Events.FireEvent( 'OnSelectionChange' ) ;
}
DarkBlueHighlight.prototype.GetState = function(){ 
      return FCK_TRISTATE_OFF;
}

// Register the command.
FCKCommands.RegisterCommand('DarkBlueHighlight', new DarkBlueHighlight());

// Add the button.
var item = new FCKToolbarButton('DarkBlueHighlight', 'Dark Blue Highlight');
item.IconPath = FCKConfig.PluginsPath + 'DarkBlueHighlight/remark_highlight_icon_08_on.png';
FCKToolbarItems.RegisterItem('DarkBlueHighlight', item);
