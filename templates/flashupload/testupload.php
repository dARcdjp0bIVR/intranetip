<?php

// using: 

$PATH_WRT_ROOT = "../../../";

$imgDir = $PATH_WRT_ROOT."src/tool/flashupload/files/";
	
?>

<html>
<head>
<title>test flash upload</title>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>src/includes/js/swfobject.js"></script>	

<script language="javascript">
	var imgdir = "<?=$imgDir?>";

	function thisMovie(movieName) {
		if (navigator.appName.indexOf("Microsoft") != -1) {
			return window[movieName];
		} else {
			return document[movieName];
		}
	}

	function flashUploadResult(result, msg, path, type, size, createDate, modDate, creator, fileW, fileH){
		alert("result: "+result+" / msg: "+msg+" / path: "+path+" / type: "+type+" / size: "+size+" / createion date: "+createDate+" / modification date: "+modDate+" / creator: "+creator+" / file width: "+fileW+" / height: "+fileH);
		
		var displayDIV = document.getElementById("displayImg");
		while(displayDIV.hasChildNodes()){
			displayDIV.removeChild(displayDIV.firstChild);
		}
		var fimg = document.createElement("img");
		fimg.setAttribute("src",path);
		displayDIV.appendChild(fimg);

	}

	//function flashUploadDimension(result, msg, fileW, fileH){
	//	alert("result: "+result+" / msg: "+msg+" / image width: "+fileW+" / height: "+fileH);
	//}

	function embedFlash(){
		var areaFlash = document.getElementById("flashuploadDIV");
		while(areaFlash.hasChildNodes()){
			areaFlash.removeChild(areaFlash.firstChild);
		}

		var flashvars = {
			imgDir: imgdir
		};
		var params = {
			play: "true",
			loop: "true",
			menu: "true",
			quality: "high",
			scale: "showall",
			//scale: "noscale",
			wmode: "window", 
			bgcolor: "#ffffff",
			//devicefont: "false",
			allowscriptaccess: "sameDomain",
			allowfullscreen: "false"
		};
		var attributes = {
			id: "flashupload",
			name: "flashupload",
			align: "middle"
		};

		//swfobject.embedSWF("<?=$PATH_WRT_ROOT?>src/tool/flashupload/flashupload.swf", "flashuploadDIV", "10px", "10px", "9.0.0", "<?=$PATH_WRT_ROOT?>src/includes/js/expressInstall.swf", flashvars, params, attributes);
		swfobject.embedSWF("flashupload.swf", "flashuploadDIV", "16px", "16px", "9.0.0", "<?=$PATH_WRT_ROOT?>src/includes/js/expressInstall.swf", flashvars, params, attributes);
	}

</script>
</head>

<body onload="javascript:embedFlash();">

<!--
<div>
<a href="#" onClick="javascript:thisMovie('flashupload').uploadImage(imgdir)">UPLOAD IMAGE</a> 
</div>
-->

<!--////////////////// flash file /////////////////////////////////-->
<div id="flashuploadDIV">no flash?</div>
<!--///////////////////////////////////////////////////////////////-->

<div id="displayImg"></div>


</body>
<?php

?>