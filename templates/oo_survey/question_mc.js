// MC Question Base Constructor
function question_mc()
{
}

question_mc.prototype = new question_base();
question_mc.prototype.qOptNum = 0;
question_mc.prototype.qOpt = new Array();
question_mc.prototype.genOptionNumSelection = genOptionNumSelection;
question_mc.prototype.genQuestionString = genQuestionString;
question_mc.prototype.setOptionNum = setOptionNum;
question_mc.prototype.setOption = setOption;
question_mc.prototype.getOptionNum = getOptionNum;
question_mc.prototype.parseQuestionString = parseQuestionString;
question_mc.prototype.checkDuplicateOption = checkDuplicateOption;

/**********************************************************************
 * Methods of this class
 **********************************************************************/
function genOptionNumSelection() {
  var x = "";
  
  for(var i=2; i<=10; i++)
  {
    x += "<option value=\""+i+"\">"+i+"</option>";
  }
  
  return x;
}
  
function genQuestionString() {
  var x = "#QUE#";
  x += this.qType+","+this.qOptNum+"||"+this.q+"||";
  for(var i=0; i<this.qOptNum; i++)
  {
    var _opt = this.qOpt[i];
  
    x += "#OPT#"+_opt;
  }
  
  return x;
}

function setOptionNum(num) {
  this.qOptNum = parseInt(num);
}

function setOption(opt) {
  this.qOpt = opt;
}

function getOptionNum() {
  return parseInt(this.qOptNum);
}

function parseQuestionString(rawQStr) {
  var qType = rawQStr.split("||")[0].split(",")[0];
  var qOptNum = rawQStr.split("||")[0].split(",")[1];
  var q = rawQStr.split("||")[1];
  var qOpt = rawQStr.split("||")[2].split("#OPT#");
  qOpt.shift();
  
  this.qType = qType;
  this.qOptNum = parseInt(qOptNum);
  this.q = q;
  this.qOpt = qOpt;
}

function checkDuplicateOption() {
  var qOpt = this.qOpt;
  
	for(var i=0; i<qOpt.length; i++) {
    for (var j=i+1; j<qOpt.length; j++) { // inner loop only compares items j at i+1 to n
      if (qOpt[i]==qOpt[j])
      {
        return true;
      }
    }
	}
	return false;
}