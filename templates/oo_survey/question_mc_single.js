// Single Answer MC Question Constructor
function question_mc_single()
{
}

question_mc_single.prototype = new question_mc();
question_mc_single.prototype.genQuestionDisplay = genQuestionDisplay;
question_mc_single.prototype.genAnsQuestionDisplay = genAnsQuestionDisplay;

/**********************************************************************
 * Methods of this class
 **********************************************************************/
function genQuestionDisplay() {
	var displayQOrder = (this.qOrder == null) ? "" : this.qOrder+". ";
  var x = "";
  
  x = "<div class=\"ies_q_box\" id=\""+this.qHashStr+"\">"
      + "<div class=\"q_box_manage\">"
      + "<a name=\"moveUpQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_up\">"+Lang["IES"]["MoveUp"]+"</a>"
      + "<a name=\"moveDownQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_down\">"+Lang["IES"]["MoveDown"]+"</a>"
      + "<a name=\"removeQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"q_cancel\">"+Lang['IES']['Delete']+"</a>"
      + "</div>"
      
      + "<div class=\"\">"
      + "<table class=\"form_table\">"
      + "<tr>"
      + "<td class=\"q_title\">"+displayQOrder+Lang['IES']['QuestionTitle']+" <input name=\"q\" type=\"text\" value=\""+this.q+"\" style=\"width:80%\"/></td>"
      + "</tr>"
      
      + "<tr>"
      + "<td class=\"q_option\">";
      
  for(var i=0; i<this.qOptNum; i++)
  {
    var qOpt = (this.qOpt[i] || "");
  
    x += "<span class=\"q_option_list\"><input type=\"radio\" /> <input name=\"qOpt\" type=\"text\" value=\""+qOpt+"\"/></span><!--<a href=\"#\" class=\"deselect\">&nbsp;</a>--><br class=\"clear\" />";
  }
  
  x +=  "<a name=\"addOpt\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"add_more\">"+Lang['IES']['AddRow']+"</a>"
        + "</td>"
        + "</tr>"
        + "</table>"
        + "</div></div>";

  return x;
}

function genAnsQuestionDisplay(num){
	
	var x = "<div class=\"ies_q_box\"><table class=\"form_table\">";
	x += "<input type=\"hidden\" name=\"ans["+num+"][size]\" value=\"1\">";
	x += "<tr><td class=\"q_title\">"+(num+1)+". "+this.q+"</td></tr><tr><td class=\"q_option\">";
	for(var i=0; i<this.qOptNum; i++)
  {
    var qOpt = (this.qOpt[i] || "");
//    if(i==0){
//    	x += "<input type=\"radio\" name=\"ans["+num+"][0][0]\" id=\"ans["+num+"][0]["+i+"]\" value=\""+i+"\" CHECKED/><label for=\"ans["+num+"][0]["+i+"]\">"+qOpt+"</label><br />";
//    }
//    else{
    	x += "<input type=\"radio\" name=\"ans["+num+"][0][0]\" id=\"ans["+num+"][0]["+i+"]\" value=\""+i+"\"/><label for=\"ans["+num+"][0]["+i+"]\">"+qOpt+"</label><br />";
//    }	
  }
  x +=  "</td></tr>"
        + "</table></div>";
  
  return x;
}
