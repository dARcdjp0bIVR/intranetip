<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");

$this_langFile = '';

$this_langFile = ($_SESSION['sba_SchemeLang']['currentSessionLang'] == '')? $intranet_session_language :$_SESSION['sba_SchemeLang']['currentSessionLang'];
$this_langFile = (trim($this_langFile) == '') ? 'en' : $this_langFile;

include_once($PATH_WRT_ROOT."lang/lang.$this_langFile.php");
include_once($intranet_root."/lang/sba_lang.{$this_langFile}.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/sba/libies.php");
// Survey type config by IES (may change later)


$li = new libies();

$json = new JSON_obj();
$json_str = $json->encode($ies_cfg["Questionnaire"]["Question"]);



$dir_path = dirname($_SERVER["PHP_SELF"]);

header("Content-Type: text/xml");
$XML =  $li->generateXML(
          array(
            array("BasePath", $dir_path),
            array("QTypeJson", $json_str),
          ),
          false
        );
echo $XML;

?>