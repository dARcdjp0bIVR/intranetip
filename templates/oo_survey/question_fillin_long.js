// Fill-in short Question Constructor
function question_fillin_long()
{
}

question_fillin_long.prototype = new question_base();
question_fillin_long.prototype.genQuestionDisplay = genQuestionDisplay;
question_fillin_long.prototype.genQuestionString = genQuestionString;
question_fillin_long.prototype.parseQuestionString = parseQuestionString;
question_fillin_long.prototype.genAnsQuestionDisplay = genAnsQuestionDisplay;

/**********************************************************************
 * Methods of this class
 **********************************************************************/ 
function genQuestionDisplay() {
	var displayQOrder = (this.qOrder == null) ? "" : this.qOrder+". ";
  var x = "";
  
  x = "<div class=\"ies_q_box\" id=\""+this.qHashStr+"\">"
      + "<div class=\"q_box_manage\">"
      + "<a name=\"moveUpQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_up\">"+Lang["IES"]["MoveUp"]+"</a>"
      + "<a name=\"moveDownQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"move_down\">"+Lang["IES"]["MoveDown"]+"</a>"
      + "<a name=\"removeQ\" href=\"javascript:;\" hashID=\""+this.qHashStr+"\" class=\"q_cancel\">"+Lang['IES']['Delete']+"</a>"
      + "</div>"
      + "<div class=\"\">"
      + "<table class=\"form_table\">"
      + "<tr>"
      + "<td class=\"q_title\">"+displayQOrder+Lang['IES']['QuestionTitle']+" <input name=\"q\" type=\"text\" value=\""+this.q+"\" style=\"width:80%\"/></td>"
      + "</tr>"
      + "<tr><td><textarea style=\"width:100%; height:50px\"></textarea></td></tr>"
      + "</table>"
      + "</div></div>";
  
  return x;
}

function genAnsQuestionDisplay(num){
	var x = "";
	  
  x = "<div class=\"ies_q_box\"><table class=\"form_table\">"
      + "<input type=\"hidden\" name=\"ans["+num+"][size]\" value=\"1\">"
      + "<tr>"
      + "<td class=\"q_title\">"+(num+1)+". "+this.q+"</td>"
      + "</tr>"
      + "<tr><td><textarea name=\"ans["+num+"][0][0]\"></textarea></td></tr>"
      + "</table></div>";
  
  return x;
}

function genQuestionString() {
  var x = "#QUE#";
  
  x += this.qType+"||"+this.q+"||";
  
  return x;
}

function parseQuestionString(rawQStr) {
  var qType = rawQStr.split("||")[0];
  var q = rawQStr.split("||")[1];
  
  this.qType = qType;
  this.q = q;
}