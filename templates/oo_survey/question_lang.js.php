<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($intranet_root."/lang/ies_lang.".(empty($_SESSION['IES_CURRENT_LANG'])?$intranet_session_language:$_SESSION['IES_CURRENT_LANG']).".php");

?>

var Lang = new Array();
Lang["IES"] = new Array();

Lang['IES']['AddRow'] = "<?=$Lang['IES']['AddRow']?>";
Lang['IES']['Answer'] = "<?=$Lang['IES']['Answer']?>";

Lang['IES']['Delete'] = "<?=$Lang['IES']['Delete']?>";
Lang['IES']['DuplicateOptions'] = "<?=$Lang['IES']['DuplicateOptions']?>";
Lang['IES']['DuplicateQuestions'] = "<?=$Lang['IES']['DuplicateQuestions']?>";

Lang['IES']['MoveDown'] = "<?=$Lang['IES']['MoveUp']?>";
Lang['IES']['MoveUp'] = "<?=$Lang['IES']['MoveDown']?>";

Lang['IES']['Question'] = "<?=$Lang['IES']['Question']?>";
Lang['IES']['QuestionTitle'] = "<?=$Lang['IES']['QuestionTitle']?>";