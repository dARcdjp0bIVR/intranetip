// Using By :     

/*
# Modification Log
#   Date:   2019-02-22 (Isaac)
#           added function convertOverflowNumber() to add '+' to overflow numbers.
#
#  2018-01-03 (Carlos)
#	added function CheckPasswordCriteria(password,userlogin,minlength) for checking user account password.
#
#  2017-07-27 (Cameron)
#	add parameter jRowHeight to jSelectPresetTextECLASS() so that the row can be shown when Chrome is set to normal display (100%)
#	add function resizeTextArea(), appendRow2TextArea()
#
#  2017-07-25 (Cameron)
#	add parameter jAppend to jSelectPresetTextECLASS() so that the preset text can be appended to specific field
#
#  2017-07-03 (Carlos)
#	Improved Get_Selection_Value(SelectionID,ReturnType,IgnoreSelectedStatus), cater selection id is not same as selection name situation.
#
#  2016-07-18 (Paul)
#	Added flag to control the checking of refresh
#
#  2016-06-28 (Paul)
#	Added general checking for the number of times the refresh was done within a minute
#
#  2016-01-20 (Carlos)
#	Added validateIPAddress() for checking ip address.
#
#  2015-10-29 (Omas)
#  Added focusNext() for a group of textbox
#
# 2015-09-30 (Ivan)
#	modified function isObjectValueEmpty() to support input password type
#
# 2014-12-05 (Omas)
#	add jQuery function initInsertAtTextarea() to insert text to cursor position in textarea 
#
# 2014-08-28 (Bill)
#	modified jSelectPresetTextECLASS() to allow setting the width of window
#
# 2014-06-12 (Ivan)
#	modified function limitText added param callBackFunc
#
# 2013-09-05 (Ivan)
#	added isFormChanged() to check if the form values are changed or not
#
# 2013-05-02 (YatWoon)
#	create is_int() for check the value is positive integer or not
#
# 2012-10-24 Rita()
# copy 	saveFormValues(), loadFormValues(), js_Hide_Option_Div(), js_Show_Option_Div() from EJ
#
#  2012-05-18 YatWoon
#  add newWindowWithSize()
# 2012-06-06 YatWoon
# - add zeroPad()
# 2012-03-29 Ivan
# - added function show_dyn_thickbox_ip, get_dyn_thickbox_dimension_ip, load_dyn_size_thickbox_ip from /home/web/eclass40/eclass40/js/common.js
#
# 2012-03-26 Marcus
# - modified function compareTime, toTimeStr, Cater additional sec field
#
# 2012-01-26 Marcus
# - added function js_Delay_Action() , for key event ajax checking, delay sending ajax request in order to reduce number of request sent.
#
# 2011-11-29 Ivan
# - added function isObjectValueEmpty()
#
# 2011-11-21 Ivan
# - added function isIncludeChineseChar()
#
# 2011-09-01 Ivan
# - add function checkApprove2(), checkReject2(), exec a function instead of post a form after checking
#
# 2011-08-10 YatWoon
# - add check_date_30()
#
# 2011-08-10 Thomas
# - add fe_itextbook() for opening iTextbook special room
#
# 2011-08-08 YatWoon
# - add isFloat(), check_positive_float_30(), check_date_allow_null_30()
#
# 2011-08-01 Marcus
# - add js_Show_ThickBox, show thickbox in js with specifi size
#
# 2011-05-24 Marcus
# - add is_positive_int, to check positive int
#
# 2011-04/21 Henry Chow
# - Modified validateFilename(), customize return message "msg"
#
# 2011-03-28 Ivan
# - added function js_Get_Word_Count() to count the number of words (can be both Chinese and English)
# 2011-03-14 Marcus
# - modified function Block_Element, UnBlock_Element
#		fixed Block_Element will automatically add a style(position:relative) to the element and may caused ui problem.
#		add logic to rollback the style (position) in UnBlock_Element
# 2011-03-01 Marcus
# - modified function js_Select_All, js_Select_All_With_OptGroup
#		improved js_Select_All to support selection with OptGroup, 
#		modified js_Select_All_With_OptGroup, directly call js_Select_All in the function as js_Select_All support optgroup
# 2011-02-11 Kenneth Chung
# - add function Remove_All_Selection_Option(), to remove all selection's options with defined value parameter
#
# 2010-01-21 Marcus
# - add function checkEdit2(), exec a function instead of post a form after checking
#
# 2011-01-03 YatWoon
# - update check_positive_int_30(), add f.value!=value checking
#
# 2010-12-30 Marcus
# - add function checkRemove2(), exec a function instead of post a form after checking
#
# 2010-12-29 YatWoon
# - add function Click_SearchBox()
#
# 2010-12-13 YatWoon
# - add check_positive_int_30() for IP25 standard
#
# 2010-11-25 YatWoon
# - add check_select_30() for  IP25 standard form checking error message display
#
# 2010-11-5 Marcus
# - modified js_Select_All_With_OptGroup, call js_Select_All at the beginning of the function in order to select options without optgroup  
#
# 2010-09-20 Ivan
# - added fixSuperTableHeight() to adjust the height of the table if the table is shorter than expected (so that the scroll bar will not be too low) 
#
# 2010-08-30 Ivan
# - added js_Hide_All_Option_Layer(), js_Clicked_Option_Layer(), and js_Clicked_Option_Layer_Button() for export / import layer handling
#
# 2010-08-20 Marcus
# - added LPAD (pad 0 or oters on the left), valid_code (valid a str is start with letters)  
#
# 2010-08-18 Sandy
# - added init_textToSpeach(), for PowerSpeech
#
# 2010-08-17 Marcus
# - added noEsc(), limitText(limitField, limitNum) to limit textarea textlength
#
# 2010-08-13 Ivan
# - modified Block_Document() add parameter "jsMyBlockMsg" for customized blocking message
#
# 2010-08-05 YaTWoon
# - Add check_text_30() for IP25 standard form checking error message display
#
# 2010-04-09: Kelvin
# - Added function switchOnlineHelp(method,m_function)
#
# 2010-03-29: kenneth chung
# - Added function check_time_without_return_msg(TimeString)
#
# 2010-03-23: Marcus
# - modified Get_Form_Values() add parameter to skip a specific field.
#
# 2010-03-08: Marcus
# - added open source function htmlspecialchars ,htmlspecialchars_decode (equivalent to PHP function).
#
# 2010-02-26: Marcus
# - added trim function String.prototype.Trim,String.prototype.LTrim,String.prototype.RTrim.
#
# 2010-02-05: Shing
# - Updated jSelectPresetTextECLASS() to prevent a possible popup display problem on first load in IE7.
#
# 2010-01-22: Ivan
# - added function Create_IFrame(FrameName)
#
# 2010-01-08: Henry 
# - add check_date_allow_null(), same as check_date() but allow null input
# 
# 2009-12-08: Max (200912081532)
# - change the document.all in jAddMoreFileField to document.getElementsByTagName('*') to support both I.E. and Firefox
*/

// open window functions
//dim on 20080331 by user comment
/*function showMonthSelect() {
	var i=0;
	var obj = document.getElementsByName('SelectYear')[0];
	var MonthObj = document.getElementsByName('SelectMonth');
	var yearSelectedValue = obj.options.value;
	
	for (i=0; i< MonthObj.length; i++) {
		MonthObj[i].style.visibility = 'hidden';
		MonthObj[i].disabled = true;
	}
	
	
	document.getElementById(yearSelectedValue).style.visibility = 'visible';
	document.getElementById(yearSelectedValue).disabled = false;
}*/

/*To Provide Block for continuous F5*/
var doNotCheckF5 = false;
if(document.addEventListener && doNotCheckF5==false)
{
	document.addEventListener("keydown", keyDownBlockF5, false);
	function keyDownBlockF5(e){
		e = window.event || e;
		var keycode = e.keyCode || e.which;
		if( keycode == 116){
			var rootPath = window.location.protocol+"//"+window.location.hostname;
			if(location.port!='0' && location.port !=''){
				rootPath = rootPath+":"+location.port;
			} 
			var cur_uri = window.location.href.replace(window.location.protocol+"//"+window.location.hostname, "");
			if(location.port!='0' && location.port !=''){
				cur_uri = cur_uri.replace(":"+location.port, "");
			} 
			var PathData = "";
			var name = encodeURIComponent(cur_uri) + "=";
			var ca = document.cookie.split(";");
			for(var i = 0; i < ca.length; i++) {
				var c = ca[i];
			    while (c.charAt(0) == ' ') {
				    c = c.substring(1);
				}
			    if (c.indexOf(name) == 0) {
			        PathData = c.substring(name.length, c.length);
			    }
			}
			var curTime = new Date().getTime();
			var timeOfF5 = 0;
			if(PathData){
				if (typeof (JSON) != 'undefined') { 
					var root = JSON.parse(window.atob(PathData));
				}else{
					var data = PathData.split("@_@");
					var root = {
						"refresh_times" : data[0],
						"last_time"		: data[1]
					};
				}	
				timeOfF5 = parseInt(root.refresh_times);
				timeOfF5 += 1;
				var timestamp = parseInt(root.last_time);
			}else{
				timeOfF5 = 1;
				var timestamp = parseInt(curTime);
			}
			if(curTime - timestamp <= 60000){
				if(timeOfF5 == 50){
					e.preventDefault();
					scriptURL = rootPath +"/home/record_abuse_ajax.php";
					alert(JSLang['RefreshOverload']);	
					if(typeof (jQuery) === 'undefined'){
						var xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (xhttp.readyState == 4 && xhttp.status == 200) {
						      return false;
						    }
						};
						xhttp.open("POST", scriptURL, true);
						xhttp.send("action=jsLog&cur_uri="+cur_uri+"&tapNo="+timeOfF5);
					}else{
						param = {
							"action": "jsLog",
							"cur_uri": cur_uri,
							"tapNo": timeOfF5
						};
						$.post(scriptURL, param, function(){
							return false;
						});
					}						
				}else if(timeOfF5 > 50){
					e.preventDefault();
					alert(JSLang['RefreshOverload']);	
					return false;					
				}
			}else{
				timeOfF5 = 0;
				timestamp = curTime;
			}
			nowDate = new Date();
			nowDate.setTime(nowDate.getTime() + 1000*60);
			if (typeof (JSON) != 'undefined') { 
				var jsonData = {
					"refresh_times" : timeOfF5,
					"last_time"		: curTime
				};
				var cookieStr = JSON.stringify(jsonData);
				document.cookie= encodeURIComponent(cur_uri) + "=" + window.btoa(cookieStr) + "; path=/; expires=" + nowDate.toGMTString();		
			}else{
				var cookieStr = timeOfF5+"@_@"+curTime;
				document.cookie= encodeURIComponent(cur_uri) + "=" + cookieStr + "; path=/; expires=" + nowDate.toGMTString();		
			}						    
		}
	};
}

var newWin;
var ip_dyn_tb_h = "";
var ip_dyn_tb_w = "";

function newWindow(url, winType) 
{

        win_name = "intranet_popup";
        win_size = "resizable,scrollbars,status,top=40,left=40,width=400,height=400";
        if(winType==0){
                win_name = "intranet_popup0";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=400,height=400";
        }
        if(winType==1){
                win_name = "intranet_popup1";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==2){
                win_name = "intranet_popup2";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=500,height=400";
        }
        if(winType==3){
                win_name = "intranet_popup3";
                win_size = "toolbar,location,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==4){
                win_name = "intranet_popup4";
                win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==5){
                win_name = "intranet_popup5";
                win_size = "menubar,toolbar,resizable,scrollbars,status,top=40,left=40,width=620,height=400";
        }
        if(winType==6){
                win_name = "intranet_popup6";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=500,height=350";
        }
        if(winType==7){
                win_name = "intranet_popup7";
                win_size = "toolbar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==8){
                win_name = "intranet_popup8";
                win_size = "menubar,toolbar,resizable,scrollbars,status,top=40,left=40,width=800,height=500";
        }
        if(winType==9){
                win_name = "intranet_popup9";
                win_size = "resizable,scrollbars,status,top=40,left=500,width=500,height=500";
        }
        if(winType==10){
                win_name = "intranet_popup10";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=800,height=600";
        }
        if(winType==11){
                win_name = "intranet_popup11";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=500,height=500";
        }
        if(winType==12){
                win_name = "intranet_popup12";
                win_size = "resizable,scrollbars,status,top=40,left=40,width=700,height=500";
        }
		if(winType==13){
                                // backend status window
                win_name = "intranet_popup13";
                win_size = "scrollbars=no,top=140,left=140,width=400,height=300";
        }
		if(winType==14){
				win_name = "intranet_popup14";
				win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=630,height=600";
        }
		if(winType==15){
				win_name = "intranet_popup15";
				win_size = "resizable,scrollbars,status,top=40,left=40,width=500,height=550";
        }
        if(winType==16)
        {	        
                win_name = "intranet_popup16";
                win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=600,height=540";
        }
        if(winType==17)
        {	        
			//For powervoice use	        
			win_name = "intranet_popup17";
			win_size = "menubar=no,resizable,scrollbars,status,top=40,left=40,width=580,height=310";
        }
        if(winType==18)
        {	        
			//For powervoice use	        
			win_name = "intranet_popup18";
			win_size = "menubar=no,resizable,scrollbars=no,status,top=40,left=40,width=380,height=200";
        }
        if(winType==19)
        {	        
			//For Merit Summary iportfolio v2.5	        
			win_name = "intranet_popup19";
			win_size = "resizable,scrollbars,status=0,top=40,left=40,width=670,height=540";
        }
			if(winType==21){
		//for HTML editor page
			win_name = "ec_popup21";
			win_size = "status,top=30,left=130,width=750,height=520";

				}
				if(winType==22){
			//for iPortfolio Assessment Statistic Report Pop-up
			win_name = "ec_popup22";
			win_size = "toolbar,location,resizable,scrollbars,status,top=10,left=10,width=700,height=500";
				}
				if(winType==23){
		// for printing
		win_name = "ec_popup23";
		win_size = "menubar,resizable,scrollbars,top=40,left=40,width=640,height=480";
				}
		if(winType==24){
			//for print mode of attendance
            win_name = "intranet_popup24";
            win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=700";
        }
				if(winType==27){
		//for iPortfolio SAMS import
		win_name = "ec_popup27";
		win_size = "resizable,scrollbars=1,top=110,left=150,width=560,height=410";
		}
		if(winType==28){
		//for iPortfolio report printing
		win_name = "ec_popup28";
		win_size = "menubar,resizable,scrollbars=1,top=20,left=20,width=800,height=570";
		}
				if(winType==93){
			//for iPortfolio Learning Portfolio
			win_name = "ec_popup93";
			win_size = "toolbar,location,resizable,scrollbars,status,top=10,left=10,width=800,height=600";
				}
				if(winType==94){
			//for iPortfolio Learning Portfolio
			win_name = "ec_popup94";
			win_size = "toolbar,location,resizable,scrollbars,status,top=10,left=10,width=800,height=300";
				}
				if(winType==95){
			//for full screen
			win_name = "ec_popup95";
			win_size = "toolbar,location,resizable,scrollbars,status,fullscreen=yes,top=0,left=0";
				}
		if(winType==30){
			// iCalendar 
            win_name = "intranet_popup30";
            win_size = "menubar,toolbar,resizable,scrollbars,status,top=40,left=40,width=1000,height=500";
        }
		if (winType==31){
			win_name = "intranet_popup31";
            win_size = "menubar,toolbar,resizable,scrollbars,status,top=40,left=40,width="+screen.width+",height="+screen.height;
		}
		if (winType==32){
			win_name = "intranet_popup31";			
            win_size = "resizable,scrollbars,status,top=40,left=40,width="+screen.width+",height="+screen.height;
		}
		// For popup file output
		if (winType==33){
			win_name = "intranet_popup33";
            win_size = "width=100,height=100";
		}
		// For external site logout popup
		if (winType==34){
			win_name = "intranet_popup34";
            win_size = "top=1,left=1,width=10,height=10";
		}
		if(winType==35){
	        win_name = "intranet_popup35";
	        win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=800,height=600";
	    }
		if(winType==36){
			win_name = "intranet_popup36";
			win_size = "resizable,scrollbars,status,top=40,left=40,width="+screen.width+",height="+screen.height;
		}
		// for printing A4 size 
		if(winType==37){
	        win_name = "intranet_popup37";
	        win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=880,height=600";
	    }
		if(winType==38){
			win_name = "intranet_popup38";
			win_size = "resizable,scrollbars,status,top=40,left=40,width=800,height=535";
        }
        // alert(screen.width+" "+screen.height);
//	if (newWin!=null && (navigator.appName=="Netscape")) newWin.close();
        newWin = window.open (url, win_name, win_size);
	if ((navigator.appName=="Netscape") || (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion.indexOf("MSIE")>1))
		 newWin.focus();

}

function newWindowWithSize(url, is_resizable, is_scroll, this_width, this_height) {
        win_name = "intranet_popup";
        win_size = "resizable="+is_resizable+",scrollbars="+is_scroll+",status=0,top=40,left=40,width="+this_width+",height="+this_height;
			
		if (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion >="4")
		{
			if (newWin!=null) newWin.close();
		}
        newWin = window.open (url, win_name, win_size);
        if (navigator.appName=="Netscape" && navigator.appVersion >= "3") newWin.focus();
}

function newWindowNotClose(url, winType,name) {
        var newWindow;
        win_name = name;
        win_size = "resizable,scrollbars,status,top=40,left=40,width=400,height=400";
        if(winType==0){
                win_size = "resizable,scrollbars,status,top=40,left=40,width=400,height=400";
        }
        if(winType==1){
                win_size = "resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==2){
                win_size = "resizable,scrollbars,status,top=40,left=40,width=500,height=400";
        }
        if(winType==3){
                win_size = "toolbar,location,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==4){
                win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==5){
                win_size = "menubar,toolbar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==6){
                win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=500,height=350";
        }
        if(winType==7){
                win_size = "toolbar,resizable,scrollbars,status,top=40,left=40,width=600,height=400";
        }
        if(winType==8){
                win_size = "menubar,toolbar,resizable,scrollbars,status,top=40,left=40,width=800,height=500";
        }
        if(winType==9){
                win_size = "resizable,scrollbars,status,top=40,left=500,width=500,height=500";
        }
        if(winType==10){
                win_size = "resizable,scrollbars,status,top=40,left=40,width=800,height=600";
        }
        if(winType==11){
                win_size = "resizable,scrollbars,status,top=40,left=40,width=500,height=500";
        }
        if(winType==12){
                win_size = "menubar,resizable,scrollbars,status,top=40,left=40,width=630,height=400";
        }
        if(winType==13){
		win_size = "status,top=30,left=130,width=720,height=520";
		}
		if(winType==14){
		win_size = "menubar,resizable,scrollbars,top=40,left=40,width=640,height=480";
		}
//        if (navigator.appName=="Microsoft Internet Explorer" && navigator.appVersion >="4")
        newWindow = window.open (url, win_name, win_size);
        if (navigator.appName=="Netscape" && navigator.appVersion >= "3") newWindow.focus();
}

// trim functions
function LTrim(str){
        var whitespace = new String(" \t\n\r");
        var s = new String(str);
        if (whitespace.indexOf(s.charAt(0)) != -1) {
                var j=0, i = s.length;
                while (j < i && whitespace.indexOf(s.charAt(j)) != -1)
                        j++;
                s = s.substring(j, i);
        }
        return s;
}
function RTrim(str){
        var whitespace = new String(" \t\n\r");
        var s = new String(str);
        if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
                var i = s.length - 1;       // Get length of string
                while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1)
                        i--;
                s = s.substring(0, i+1);
        }
        return s;
}
function Trim(str){
        return RTrim(LTrim(str));
}

// form validation functions
function setChecked(val, obj, element_name, checkIdOnly){
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
        	if(checkIdOnly==1) {
                if (obj.elements[i].id==element_name && !obj.elements[i].disabled)
                obj.elements[i].checked=val;
            } else {
            	if (obj.elements[i].name==element_name && !obj.elements[i].disabled)
                obj.elements[i].checked=val;
            }
        }
}
function countChecked(obj, element_name){
        len=obj.elements.length;
        var i=0;
        var j=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name && obj.elements[i].checked)
                j=j+1;
        }
        return j;
}
function returnChecked(obj, element_name){
        len=obj.elements.length;
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name && obj.elements[i].checked)
				{
					//alert(obj.elements[i].value);
                return obj.elements[i].value;
				}
        }
        return null;
}
function check_text(f, msg){
        if(Trim(f.value)==""){
                alert(msg);
                f.value="";
                f.focus();
                return false;
        }else{
                return true;
        }
}

function check_text_30(f, msg, div_name){
        if(Trim(f.value)==""){
			eval("document.getElementById('" + div_name +"').innerHTML = '<font color=red>"+ msg +"</font>';"); 
                return false;
        }else{
                return true;
        }
}

function check_select(f, msg, g){
        if(Trim(f.options[f.selectedIndex].value)==g){
                alert(msg);
                f.focus();
                return false;
        }else{
                return true;
        }
}

function check_select_30(f, msg, g,div_name){
		if(Trim(f.options[f.selectedIndex].value)==g){
                eval("document.getElementById('" + div_name +"').innerHTML = '<font color=red>"+ msg +"</font>';"); 
                return false;
        }else{
                return true;
        }
}

function check_checkbox(obj,element){
        if(countChecked(obj,element)==0){
                return false;
        } else {
                return true;
        }
}

function check_int(f,d,msg){
        if(isNaN(parseInt(f.value)) || parseInt(f.value)==0){
                alert(msg);
                f.value=d;
                f.focus();
                return false;
        }else{
                f.value=parseInt(f.value);
                return true;
        }
}

function check_positive_int (f,msg,d,rollback)
{
     value = parseInt(f.value);
     if (isNaN(value) || value < 0)
     {
         alert(msg);
         f.focus();
         if (rollback==1)
         {
         	f.value=d;
     	}
         return false;
     }
     else
     {
         f.value = value;
         return true;
     }
}
function check_positive_int_30(f,msg,d,rollback,div_name)
{
     value = parseInt(f.value);

     if (isNaN(value) || value < 0 || f.value!=value)
     {
    	 eval("document.getElementById('" + div_name +"').innerHTML = '<font color=red>"+ msg +"</font>';"); 
         if (rollback==1)
         {
         	f.value=d;
     	}
         return false;
     }
     else
     {
         f.value = value;
         return true;
     }
}
function check_positive_nonzero_int (f,msg)
{
     value = parseInt(f.value);
     if (isNaN(value) || value <= 0)
     {
         alert(msg);
         f.focus();
         return false;
     }
     else
     {
         f.value = value;
         return true;
     }
}
function check_percentage (f,msg)
{
	value = parseFloat(f.value);
	
	if (value == null || isNaN(value) || value < 0 || value > 100 || !IsNumeric(f.value))
	{
		alert(msg);
		f.focus();
		return false;
	}
	else
	{
		f.value = value;
        return true;
	}
}

function isFloat(val) {
    if(!val || (typeof val != "string" || val.constructor != String)) {
      return(false);
    }
    var isNumber = !isNaN(new Number(val));
    if(isNumber) {
      if(val.indexOf('.') != -1) {
        return(true);
      } else {
        return(false);
      }
    } else {
      return(false);
    }
  }
  
function check_positive_float_30(f,msg, div_name)
{
	if(f.value <0)
	{
		eval("document.getElementById('" + div_name +"').innerHTML = '<font color=red>"+ msg +"</font>';"); 
		return false;
	}
	
	if(!isFloat(f.value))
	{
		if(IsNumeric(f.value))
		{
			dot_no = f.value.split(".").length-1;
			if(dot_no>1)
			{
				eval("document.getElementById('" + div_name +"').innerHTML = '<font color=red>"+ msg +"</font>';"); 
				return false;
			}
		}
		else
		{
			eval("document.getElementById('" + div_name +"').innerHTML = '<font color=red>"+ msg +"</font>';"); 
			return false;
		}
	}
	
	return true;
}




function check_date(obj, msg){
        var err = 0;
        d_a = obj.value;
        if (d_a.length != 10) err = 1;
        // commented by kenneth chung 2010-04-09, to prevent chinese character pass through to checking
        /*d_b = parseInt(d_a.substring(0, 4),10)        // year
        d_c = d_a.substring(4, 5)                                // '-'
        d_d = parseInt(d_a.substring(5, 7),10)        // month
        d_e = d_a.substring(7, 8)                                // '-'
        d_f = parseInt(d_a.substring(8, 10),10)        // day*/
        d_b = d_a.substring(0, 4);        // year
        d_c = d_a.substring(4, 5);                                // '-'
        d_d = d_a.substring(5, 7);        // month
        d_e = d_a.substring(7, 8);                                // '-'
        d_f = d_a.substring(8, 10);        // day
        // basic error checking
        if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
        if(d_c != '-') err = 1;
        if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
        if(d_e != '-') err = 1;
        if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
        // advanced error checking
        // months with 30 days
        if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
        // february, leap year
        if(d_d==2){ // feb
                var d_g = parseInt(d_b/4)
                if(isNaN(d_g)) err = 1;
                if(d_f>29) err = 1;
                if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
        }
        if(err==1){
                alert(msg);
                obj.focus();
                return false;
        }else{
                return true;
        }
}

function check_date_30(obj, msg){
        var err = 0;
        d_a = obj.value;
        if (d_a.length != 10) err = 1;
       
        d_b = d_a.substring(0, 4);        // year
        d_c = d_a.substring(4, 5);                                // '-'
        d_d = d_a.substring(5, 7);        // month
        d_e = d_a.substring(7, 8);                                // '-'
        d_f = d_a.substring(8, 10);        // day
        // basic error checking
        if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
        if(d_c != '-') err = 1;
        if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
        if(d_e != '-') err = 1;
        if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
        // advanced error checking
        // months with 30 days
        if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
        // february, leap year
        if(d_d==2){ // feb
                var d_g = parseInt(d_b/4)
                if(isNaN(d_g)) err = 1;
                if(d_f>29) err = 1;
                if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
        }
        if(err==1){
                return false;
        }else{
                return true;
        }
}

function check_date_allow_null(obj, msg){
    var err = 0;
    d_a = obj.value;
	if(d_a.length!=0) {
	        if (d_a.length != 10) err = 1;
	        // commented by kenneth chung 2010-04-09, to prevent chinese character pass through to checking
        /*d_b = parseInt(d_a.substring(0, 4),10)        // year
        d_c = d_a.substring(4, 5)                                // '-'
        d_d = parseInt(d_a.substring(5, 7),10)        // month
        d_e = d_a.substring(7, 8)                                // '-'
        d_f = parseInt(d_a.substring(8, 10),10)        // day*/
        d_b = d_a.substring(0, 4);        // year
        d_c = d_a.substring(4, 5);                                // '-'
        d_d = d_a.substring(5, 7);        // month
        d_e = d_a.substring(7, 8);                                // '-'
        d_f = d_a.substring(8, 10);        // day
	        // basic error checking
	        if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
	        if(d_c != '-') err = 1;
	        if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
	        if(d_e != '-') err = 1;
	        if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
	        // advanced error checking
	        // months with 30 days
	        if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
	        // february, leap year
	        if(d_d==2){ // feb
	                var d_g = parseInt(d_b/4)
	                if(isNaN(d_g)) err = 1;
	                if(d_f>29) err = 1;
	                if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
	        }
	        if(err==1){
	                alert(msg);
	                obj.focus();
	                return false;
	        }else{
	                return true;
	        }
	} else {
		return true;
	}
}

function check_date_allow_null_30(obj){
    var err = 0;
    d_a = obj.value;
	
	if(d_a.length!=0) {
	        if (d_a.length != 10) err = 1;
	        
        d_b = d_a.substring(0, 4);        // year
        d_c = d_a.substring(4, 5);                                // '-'
        d_d = d_a.substring(5, 7);        // month
        d_e = d_a.substring(7, 8);                                // '-'
        d_f = d_a.substring(8, 10);        // day
	        // basic error checking
	        if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
	        if(d_c != '-') err = 1;
	        if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
	        if(d_e != '-') err = 1;
	        if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
	        // advanced error checking
	        // months with 30 days
	        if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
	        // february, leap year
	        if(d_d==2){ // feb
	                var d_g = parseInt(d_b/4)
	                if(isNaN(d_g)) err = 1;
	                if(d_f>29) err = 1;
	                if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
	        }
	
	        if(err==1){
	                return false;
	        }else{
	                return true;
	        }
	} else {
		return true;
	}
}

function check_date_without_return_msg(obj){
        var err = 0;
        d_a = obj.value;
        if (d_a.length != 10) err = 1;
        // commented by kenneth chung 2010-04-09, to prevent chinese character pass through to checking
        /*d_b = parseInt(d_a.substring(0, 4),10)        // year
        d_c = d_a.substring(4, 5)                                // '-'
        d_d = parseInt(d_a.substring(5, 7),10)        // month
        d_e = d_a.substring(7, 8)                                // '-'
        d_f = parseInt(d_a.substring(8, 10),10)        // day*/
        d_b = d_a.substring(0, 4);        // year
        d_c = d_a.substring(4, 5);                                // '-'
        d_d = d_a.substring(5, 7);        // month
        d_e = d_a.substring(7, 8);                                // '-'
        d_f = d_a.substring(8, 10);        // day
        //alert(d_f);
        // basic error checking
        if(d_b<0 || d_b>3000 || isNaN(d_b)) err = 1;
        if(d_c != '-') err = 1;
        if(d_d<1 || d_d>12 || isNaN(d_d)) err = 1;
        if(d_e != '-') err = 1;
        if(d_f<1 || d_f>31 || isNaN(d_f)) err = 1;
        // advanced error checking
        // months with 30 days
        if((d_d==4 || d_d==6 || d_d==9 || d_d==11) && (d_f==31)) err = 1;
        // february, leap year
        if(d_d==2){ // feb
                var d_g = parseInt(d_b/4)
                if(isNaN(d_g)) err = 1;
                if(d_f>29) err = 1;
                if(d_f==29 && ((d_b/4)!=parseInt(d_b/4))) err = 1;
        }
        if(err==1){
                return false;
        }else{
                return true;
        }
}

function check_time(obj,msg)
{
        var err = 0;
        d_a = obj.value;
        if (d_a.length != 8) err = 1;
        d_b = parseInt(d_a.substring(0, 2),10)        // hr
        d_c = d_a.substring(2, 3)                                // ':'
        d_d = parseInt(d_a.substring(3, 5),10)        // min
        d_e = d_a.substring(5, 6)                                // ':'
        d_f = parseInt(d_a.substring(6, 8),10)        // sec
        // basic error checking
        if(d_b<0 || d_b>23 || isNaN(d_b)) err = 1;
        if(d_c != ':') err = 1;
        if(d_d<0 || d_d>59 || isNaN(d_d)) err = 1;
        if(d_e != ':') err = 1;
        if(d_f<0 || d_f>59 || isNaN(d_f)) err = 1;
        if(err==1){
                alert(msg);
                obj.focus();
                return false;
        }else{
                return true;
        }
}

function check_time2(obj,msg)
{
        var err = 0;
        d_a = obj.value;
        if (d_a.length != 5) err = 1;
        
        if(isNaN(d_a.substring(0, 2)) || isNaN(d_a.substring(3, 5)))	err=1;
        
        if(err!=1)
        {
	        d_b = parseInt(d_a.substring(0, 2),10)        // hr
	        d_c = d_a.substring(2, 3)                                // ':'
	        d_d = parseInt(d_a.substring(3, 5),10)        // min
	        // basic error checking
	        if(d_b<0 || d_b>23 || isNaN(d_b)) err = 1;
	        if(d_c != ':') err = 1;
	        if(d_d<0 || d_d>59 || isNaN(d_d)) err = 1;
		}
	
        if(err==1){
                alert(msg);
                obj.focus();
                return false;
        }else{
                return true;
        }
}

function check_time_without_return_msg(TimeString){
  var err = 0;
  d_a = TimeString;
  if (d_a.length != 8) err = 1;
  d_b = parseInt(d_a.substring(0, 2),10)        // hr
  d_c = d_a.substring(2, 3)                                // ':'
  d_d = parseInt(d_a.substring(3, 5),10)        // min
  d_e = d_a.substring(5, 6)                                // ':'
  d_f = parseInt(d_a.substring(6, 8),10)        // sec
  // basic error checking
  if(d_b<0 || d_b>23 || isNaN(d_b)) err = 1;
  if(d_c != ':') err = 1;
  if(d_d<0 || d_d>59 || isNaN(d_d)) err = 1;
  if(d_e != ':') err = 1;
  if(d_f<0 || d_f>59 || isNaN(d_f)) err = 1;
  if(err==1){
    return false;
  }else{
    return true;
  }
}

function compareDate(s1,s2)
{		
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);

        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;


}

function compareDate2(s1,s2)
{		
        y1 = parseInt(s1.substring(0,4),10);
        y2 = parseInt(s2.substring(0,4),10);
        m1 = parseInt(s1.substring(5,7),10);
        m2 = parseInt(s2.substring(5,7),10);
        d1 = parseInt(s1.substring(8,10),10);
        d2 = parseInt(s2.substring(8,10),10);
        
        if (y1 > y2)
        {
                return 1;
        }
        else if (y1 < y2)
        {
                return -1;
        }
        else if (m1 > m2)
        {
                return 1;
        }
        else if (m1 < m2)
        {
                return -1;
        }
        else if (d1 > d2)
        {
                return 1;
        }
        else if (d1 < d2)
        {
                return -1;
        }
        return 0;
}

function checkRegEx(str,msg){
        var RE1 = new RegExp("[^0-9a-zA-Z]");
        if (RE1.test(str)){
                alert(msg);
                return false;
        }else{
                return true;
        }
}

function checkRegEx2(str,msg){
        var RE1 = new RegExp("[^0-9a-zA-Z_]");
        if (RE1.test(str)){
                alert(msg);
                return false;
        }else{
                return true;
        }
}

function validateEmail(obj,msg){
        //var re = /^[\w_.-]+@[\w-]+(\.\w+)+$/;
        //var re = /^.+@.+\..{2,3}$/;
        //var re = /^[\w_\.\-]+@[\w_\-]+\.[\w]{2,3}$/;
        var re = /^([\w_\.\-])+\@(([\w_\.\-])+\.)+([\w]{2,4})+$/;	
        if (re.test(obj.value)) {
                return true;
        }else{
                alert(msg);
                obj.focus();
                return false;
        }
}
function validateFilename(str, msg){
        var re = /[:<>\\\/\|\*\?"]/;
        if(msg=='undefined') msg=globalAlertMsg25;
        
        if (re.test(str)) {
                alert(msg);
                return false;
        }else{
                return true;
        }
}
function validateURL(obj,msg){
        var re = /http(s?):\/\/[A-Za-z0-9\.-]{3,}\.[A-Za-z]{3}/;
        if (re.test(obj.value)) {
                return true;
        }else{
                alert(msg);
                obj.focus();
                return false;
        }
}

// page functions
function gopage(page, obj){
        obj.pageNo.value=page;
        obj.submit();
}
function sortPage(a, b, obj){
	    obj.order.value=a;
        obj.field.value=b;
        obj.pageNo.value=1;
        obj.submit();
}

// menu bar functions
function checkNew(page){
        self.location.href=page;
}
function checkPost(obj,url){
		var PreAction = obj.action;
        obj.action=url;
        obj.method = "post";
        obj.submit();
        obj.action=PreAction;
}
function checkGet(obj,url){
        obj.action=url;
        obj.method = "get";
        obj.submit();
}
function AlertPost (obj,url,msg){
        if(confirm(msg)){
           obj.action=url;
           obj.method = "post";
           obj.submit();
        }
}
function checkRemoveAll(obj,url){
        if(confirm(globalAlertMsgRemoveAll)){
           obj.action=url;
           obj.method = "get";
           obj.submit();
        }
}

function checkEdit(obj,element,page){
        if(countChecked(obj,element)==1) {
                obj.action=page;
                obj.submit();
        } else {
                alert(globalAlertMsg1);
        }
}

function checkEdit2(obj,element,func){
        if(countChecked(obj,element)==1) {
                eval(func)
        } else {
                alert(globalAlertMsg1);
        }
}

function checkEditMultiple(obj,element,page){
        if(countChecked(obj,element)==0) {
                alert(globalAlertMsg2);
        } else {
                obj.action=page;
                obj.method="POST";
                obj.submit();
        }
}

function checkEditMultiple2(obj,element,func){
        if(countChecked(obj,element)==0) {
                alert(globalAlertMsg2);
        } else {
                eval(func)
        }
}


function checkRemove(obj,element,page,confirmMsg){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
        		if (confirmMsg == null || confirmMsg == '')
        			confirmMsg = globalAlertMsg3;
        			
                if(confirm(confirmMsg)){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();				             
                }
        }
}
function checkRemove2(obj,element,func,confirmMsg){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
        		if (confirmMsg == null || confirmMsg == '')
        			confirmMsg = globalAlertMsg3;
        			
                if(confirm(confirmMsg)){	            
               		eval(func)				             
                }
        }
}
function checkReject(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsgReject)){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}
function checkReject2(obj,element,func,confirmMsg){
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
    		if (confirmMsg == null || confirmMsg == '')
    			confirmMsg = globalAlertMsgReject;
    			
            if(confirm(confirmMsg)){	            
           		eval(func)				             
            }
    }
}
function checkRejectNoDel(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsgRejectNoDel)){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}
function checkRestore(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.submit();
        }
}
function checkApprove(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg4)){
                obj.action=page;
                obj.submit();
                }
        }
}
function checkApprove2(obj,element,func,confirmMsg){
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
    		if (confirmMsg == null || confirmMsg == '')
    			confirmMsg = globalAlertMsg4;
    			
            if(confirm(confirmMsg)){	            
           		eval(func)				             
            }
    }
}
function checkActivate(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertActivate)){
                obj.action=page;
                obj.submit();
                }
        }
}

function checkSuspend(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg5)){
                obj.action=page;
                obj.submit();
                }
        }
}

function checkPublish(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg26)){
                obj.action=page;
                obj.submit();
                }
        }
}

function checkUnpublish(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(globalAlertMsg27)){
                obj.action=page;
                obj.submit();
                }
        }
}

function checkBlankFirstPage(obj,element,page){
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
            if(confirm(globalAlertMsg28)){
            obj.action=page;
            obj.submit();
            }
    }
}

function checkUnblankFirstPage(obj,element,page){
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
            if(confirm(globalAlertMsg29)){
            obj.action=page;
            obj.submit();
            }
    }
}

function checkLeftToRight(obj,element,page){
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
            if(confirm(globalAlertMsg31)){
            obj.action=page;
            obj.submit();
            }
    }
}

function checkRightToLeft(obj,element,page){
    if(countChecked(obj,element)==0)
            alert(globalAlertMsg2);
    else{
            if(confirm(globalAlertMsg30)){
            obj.action=page;
            obj.submit();
            }
    }
}

function checkSwap(obj,element,page){
        if(countChecked(obj,element)!=2)
                alert(globalAlertMsg6);
        else{
                if(confirm(globalAlertMsg7)){
                obj.action=page;
                obj.submit();
                }
        }
}
function checkImport(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.submit();
        }
}
function checkIdentity(obj,element){
        if(countChecked(obj,element)==0){
                alert(globalAlertMsg14);
                return false;
        }else{
                return true;
        }
}
function checkRole(obj,element,page){
        if(countChecked(obj,element)==0){
                alert(globalAlertMsg2);
        }else{
        		obj.action=page;
                obj.submit();
                //obj.target="_self";
				obj.action="";
	    }
}


function checkArchive(obj,element,page,confirmMsg){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
        	if (confirmMsg == null || confirmMsg == '')
    			confirmMsg = globalAlertMsg21;
    			
            if(confirm(confirmMsg)){	         
            	obj.action=page;
                obj.submit();
            }
        }
}

function checkAlert(obj,element,page,msg){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(msg)){
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}
function checkPrompt(obj,element,page,msg,data,defaultmsg){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                var inputdata = prompt(msg,defaultmsg);
                if (inputdata==null) return;
                else {
                data.value = inputdata;
                obj.action=page;
                obj.method="POST";
                obj.submit();
                }
        }
}

function countOption(obj){
        checkOption(obj);
        var j = 0;
        for(i=0; i<obj.length; i++){
                if(obj.options[i].selected == true){
                        j = j+1;
                }
        }
        return j;
}

function checkOption(obj){
	for(i=0; i<obj.length; i++){
		str = obj.options[i].value.replace(/^(\s)*/, '');
		str = str.replace(/(\s)*$/, '');
		if(str==''){
			obj.options[i] = null;
		}
	}
}

function checkOptionAll(obj){
        checkOption(obj);
        for(i=0; i<obj.length; i++){
                obj.options[i].selected = true;
        }
}

function checkOptionNone(obj){
        for(i=0; i<obj.length; i++){
                obj.options[i].selected = false;
        }
}

function checkOptionAdd(obj, text, value){
        obj.options[obj.length] = new Option(text, value, false, false);
}

function checkOptionRemove(obj){
        checkOption(obj);
        i = obj.selectedIndex;
        while(i!=-1){
                obj.options[i] = null;
                i = obj.selectedIndex;
        }
}

function checkOptionClear(obj){
        i = obj.length;
        while(i!=-1){
                obj.options[i] = null;
                i = i - 1;;
        }
}

function checkOptionTransfer(from,to){
        checkOption(from);
        checkOption(to);
        i = from.selectedIndex;
        while(i!=-1){
                to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
                from.options[i] = null;
                i = from.selectedIndex;
        }
}

function jGET_MULTIPLE_OPTIONS_VALUE(obj)
{
	var jReturnArray = new Array();
	var jCount = 0;
	for (var i=0; i<obj.options.length; i++)
	{
		if (obj.options[i].selected)
		{
			//if(obj.options[i].value>0)
			if(parseInt(obj.options[i].value))
			{
				jReturnArray[jCount] = obj.options[i].value;
				jCount++;
			}
		}
	}

	return jReturnArray;
}

// browser checker
function Browser() {
        var b=navigator.appName;
        if (b=="Netscape") this.b="ns";
        else if ((b=="Opera") || (navigator.userAgent.indexOf("Opera")>0)) this.b = "opera";
        else if (b=="Microsoft Internet Explorer") this.b="ie";
        if (!b) alert('Unidentified browser./nThis browser is not supported,');
        this.version=navigator.appVersion;
        this.v=parseInt(this.version);
        this.ns=(this.b=="ns" && this.v>=4);
        this.ns4=(this.b=="ns" && this.v==4);
        this.ns6=(this.b=="ns" && this.v==5);
        this.ie=(this.b=="ie" && this.v>=4);
        this.ie4=(this.version.indexOf('MSIE 4')>0);
        this.ie5=(this.version.indexOf('MSIE 5')>0);
        this.ie55=(this.version.indexOf('MSIE 5.5')>0);
        this.opera=(this.b=="opera");
        this.dom=(document.createElement && document.appendChild && document.getElementsByTagName)?true:false;
        this.def=(this.ie||this.dom); // most used browsers, for faster if loops
        var ua=navigator.userAgent.toLowerCase();
        if (ua.indexOf("win")>-1) this.platform="win32";
        else if (ua.indexOf("mac")>-1) this.platform="mac";
        else this.platform="other";
}
is = new Browser();
if(is.ie) {
document.writeln("<link rel=stylesheet href=/templates/ie.css>");
document.writeln("<META HTTP-EQUIV='Pragma' CONTENT='no-cache'>");
}

// filesystem functions
function fs_view(url){
        newWindow(url,6);
}
function fs_openfolder(folder, obj, page){
        obj.path.value = (folder == "..") ? obj.path.value.substring(0,obj.path.value.lastIndexOf("/")) : obj.path.value + "/" + folder;
        obj.action = page;
        obj.submit();
}
function fs_newfolder(obj, page){
		var folder = "";
        folder = prompt(globalAlertMsg8, "New_Folder");
        if(folder!=null && Trim(folder)!=""){
                if(validateFilename(folder)){
                        obj.folderName.value = folder;
                        obj.action = page;
                        obj.submit();
                }
        }
}
function fs_fileupload(obj, page){
        if(check_int(obj.no_file, 1, globalAlertMsg9)){
                obj.action = page;
                obj.submit();
        }
}
function fs_attachfile(obj, element, target){
        var x = "";
        for(var i=0;i<obj.elements.length;i++){
                if (obj.elements[i].name==element && obj.elements[i].checked){
                        if (x!='') x = x + " : ";
                        x = x + obj.path.value + '/' + obj.elements[i].value
                }
        }
        target.value = x;
        self.close();
}
function fs_rename(obj, element, page){
        var flag=0;
        if(countChecked(obj,element)==1) {
                filename = prompt(globalAlertMsg10, returnChecked(obj, element))
                if(filename!=null && Trim(filename)!=""){
                        if(validateFilename(filename)){
                                for(var i=0 ; i<obj.elements.length ; i++) {
                                        if (obj.elements[i].name==element && obj.elements[i].value==filename)
                                        flag=1;
                                }
                                if(flag==1){
                                        alert(globalAlertMsg11);
                                }else{
                                        obj.newName.value = filename;
                                        obj.action = page;
                                        obj.submit();
                                }
                        }
                }
        } else {
                alert(globalAlertMsg1);
        }
}

function fs_unzip(obj, element, page){
        var flag=0;
        if(countChecked(obj,element)==1) {
                filename = returnChecked(obj,element);
                filename = filename.toLowerCase();
                if(filename.indexOf(".zip")!=-1){
                        if(confirm(globalAlertMsg12)){
                                obj.action = page;
                                obj.submit();
                        }
                }else{
                        alert(filename + globalAlertMsg13);
                }
        } else {
                alert(globalAlertMsg1);
        }
}

function fs_move(obj, element, page){
        if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
        else{
                obj.action = page;
                obj.submit();
        }
}

function fs_copy(obj, element, page){
        if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
        else{
                obj.action = page;
                obj.submit();
        }
}

// front end view functions
function fe_view_announcement(id, ct){
        newWindow('/home/view_announcement.php?AnnouncementID='+id+'&ct='+ct,12);
}

function fe_view_event(id, NoBack){
        newWindow('/home/view_event.php?EventID='+id+'&NoBack='+NoBack,6);
}

function fe_view_event_by_date(ts){
        newWindow('/home/view_event_by_date.php?ts='+ts,6);
}

function fe_view_event_by_type(tp){
        newWindow('/home/view_event_by_type.php?tp='+tp,6);
}

function fe_view_polling(id){
        newWindow('/home/view_pollresult.php?PollingID='+id,2);
}

function fe_eclass(id){
        newWindow('/home/eLearning/login.php?uc_id='+id,8);
}

function fe_eclass_guest(id){
        newWindow('/home/eLearning/eclass/guest.php?c_id='+id,8);
}

function fe_directory(id){
        newWindow('view.php?GroupID='+id,1);
}

function fe_pastpoll(){
        newWindow('/home/view_pastpoll.php',1);
}

function fe_view_timetable(id){
        newWindow('/home/school/timetable/index.php?GroupID='+id,10);
}

function fe_view_chat(id){
        newWindow('/home/school/chat/index.php?GroupID='+id,10);
}

function fe_view_bulletin(id){
        newWindow('/home/school/bulletin/index.php?GroupID='+id,10);
}

function fe_view_links(id){
        newWindow('/home/eCommunity/files/index2.php?GroupID='+id+'&FileType=W',10);
}

function fe_view_files(id){
		// this function should not be used anymore
        newWindow('/home/school/files/index.php?GroupID='+id,10);
}

function fe_view_settings(id){
         newWindow('/home/eCommunity/group/settings.php?GroupID='+id,10);         
}

function fe_view_homework_detail(type, id){
		if(type==1)
			newWindow('/home/eAdmin/StudentMgmt/homework/management/homeworklist/view.php?hid='+id,1);
		else
			newWindow('/home/eService/homework/management/homeworklist/view.php?hid='+id,1);
}

function fe_view_day_rb(ts){
         newWindow('/home/eService/ResourcesBooking/dayview.php?ts='+ts,1);
}
function fe_view_qb(id){
         newWindow('/home/school/qb/index.php?GroupID='+id,10);
}

function fe_view_all_event(){
         newWindow('/home/allevent.php',9);
}

function fe_itextbook(id, BookID){
    newWindow('/home/eLearning/iTextbook/login.php?uc_id='+id+'&BookID='+BookID,8);
}

function fe_itextbook2(id, BookID, childID){
    newWindow('/home/eLearning/iTextbook/login.php?uc_id='+id+'&BookID='+BookID+'&isParent=1&childID='+childID,8);
}

function open_tv()
{
         tv_window = window.open('/home/plugin/campustv/','tv_window','scrollbars=0,toolbar=0,menubar=0,resizable=1,dependent=0,status=0,width=780,height=550,left=25,top=25')
}

function open_bulletin_msg(id)
{
         newWindowNotClose('/home/school/bulletin/message.php?BulletinID='+id,10,"intranet_bulletin");
}

function qb_open_source_document(id, isEng)
{
         newWindowNotClose('/home/school/qb/view_source.php?qid='+id+'&isEng='+isEng,10,"intranet_qb_source");
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function check_numeric(f,d,msg){
        if(f.value=='' || !IsNumeric(f.value)){
                alert(msg);
                f.value=d;
                f.focus();
                return false;
        }else{
                return true;
        }
}

function IsNumeric(sText)
{
   var ValidChars = "0123456789.";
   var Char;

   for (i = 0; i < sText.length; i++)
   {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            return false;
        }
   }
   return true;

}

function displayTable(tableID, myStyle){
	//block, none, inline
	//var currentStyle = document.all[tableID].style.display;
	var currentStyle = document.getElementById(tableID).style.display;
	var newStyle = "none";
	
	if (typeof(myStyle)!="undefined")
	{
		newStyle = myStyle;
	} else
	{
		newStyle = (currentStyle=="none") ? "" : "none";
	}

	//document.all[tableID].style.display = newStyle;
	document.getElementById(tableID).style.display = newStyle;

	return;
}

// Return today in the format of YYYY-MM-DD
function getToday()
{
	// Array of month Names
	var month=new Array(12);
	month[0]="01";
	month[1]="02";
	month[2]="03";
	month[3]="04";
	month[4]="05";
	month[5]="06";
	month[6]="07";
	month[7]="08";
	month[8]="09";
	month[9]="10";
	month[10]="11";
	month[11]="12";

	var now = new Date();
	var day = now.getDate();
	var today = now.getFullYear() + "-" + month[now.getMonth()] + "-" + (day < 10 ? ('0'+day) : day);

	return today;
}



function setCookie(name, value)
{
	var argv = setCookie.arguments;
	var argc = setCookie.arguments.length;
	var expires = (argc > 2) ? argv[2] : null;
	var path = (argc > 3) ? argv[3] : null;
	var domain = (argc > 4) ? argv[4] : null;
	var secure = (argc > 5) ? argv[5] : false;

	document.cookie = name + "=" + escape (value) +
	((expires == null) ? "" : ("; expires=" + expires.toGMTString())) +
	((path == null) ? "" : ("; path=" + path)) +
	((domain == null) ? "" : ("; domain=" + domain)) +
	((secure == true) ? "; secure" : "");
}

function getCookie(name) {
	var bikky = document.cookie;
	var index = bikky.indexOf(name + "=");
	if (index == -1) return null;
	index = bikky.indexOf("=", index) + 1;
	// first character
	var endstr = bikky.indexOf(";", index);
	if (endstr == -1) endstr = bikky.length;
	// last character
	return unescape(bikky.substring(index, endstr));

}

function isCookieEnabled() {
	if (document.all)
	{
		if (!navigator.cookieEnabled)
		{
			return false;
		}
		else
			return true;
	} else
	{
		setCookie('temp','temp');
		var temp = getCookie('temp');
		if (!temp)
		{
			return false;
	    } else
			return true;
	}

}


/* Used in :
	/staff2/data/daily/staff_status_in.php
	/staff2/data/daily/staff_status_in_list.php
	/staff2/data/daily/staff_status_out.php
	/staff2/data/daily/staff_status_out_list.php
*/
function setDivVisible(state, lay, lay2)
{
	var DivRef = document.getElementById(lay);
	var lay_used = (lay2!="") ? lay2 : "lyrShim";
	var IfrRef = document.getElementById(lay_used);
	if (state)
	{
		DivRef.style.display = 'block';
		IfrRef.style.width = DivRef.offsetWidth;
		IfrRef.style.height = DivRef.offsetHeight;
		IfrRef.style.top = DivRef.style.top;
		IfrRef.style.left = DivRef.style.left;
		IfrRef.style.zIndex = DivRef.style.zIndex - 1;
		IfrRef.style.display = 'block';
	} else
	{
		DivRef.style.display = 'none';
		IfrRef.style.display = 'none';
	}
}


/* Used in :
	/staff2/data/daily/staff_status_in.php
	/staff2/data/daily/staff_status_in_list.php
	/staff2/data/daily/staff_status_out.php
	/staff2/data/daily/staff_status_out_list.php
*/
function getPostion(obj, direction){
	return getPosition(obj, direction);
}
function getPosition(obj, direction){
	var objStr = "obj";
	var pos_value = 0;
	
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML") {
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function getObjectPosition(obj){
	this.pos_left = getPosition(obj, "offsetLeft");
	this.pos_top = getPosition(obj, "offsetTop") + obj.offsetHeight - document.body.scrollTop;

	return this;
}

function changeLayerPosition(jsClickedObjID, jsLayerDivID, jsLeftAdjustment, jsTopAdjustment) {
	
	var jsOffsetLeft, jsOffsetTop;
	jsLeftAdjustment = jsLeftAdjustment || 0;
	jsTopAdjustment = jsTopAdjustment || 0;
	
	jsOffsetLeft = $('div#' + jsClickedObjID).width();
	jsOffsetTop = 0;
	//jsOffsetTop = $('div#' + jsClickedObjID).height();
	
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') + jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + jsOffsetTop;
	
	document.getElementById(jsLayerDivID).style.left = posleft + jsLeftAdjustment + "px";
	document.getElementById(jsLayerDivID).style.top = postop + jsTopAdjustment + "px";
}


/*
Floating Menu script-  Roy Whittle (http://www.javascript-fx.com/)
Script featured on/available at http://www.dynamicdrive.com/
This notice must stay intact for use
*/
function JSFX_FloatTopDiv(SpecY)
{
	var startX = 0,	
	startY = 19;
	
	if (typeof(SpecY) != "undefined")
	{
		startY = SpecY;
	}
	
	var ns = (navigator.appName.indexOf("Netscape") != -1);
	var d = document;
	function ml(id)
	{
		var el=d.getElementById?d.getElementById(id):d.all?d.all[id]:d.layers[id];
		if(d.layers)el.style=el;
		el.sP=function(x,y){this.style.left=x;this.style.top=y;};
		el.x = startX;
		if (verticalpos=="fromtop")
		{
		el.y = startY;
		}
		else{
		el.y = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;
		el.y -= startY;
		}
		return el;
	}
	window.stayTopLeft=function()
	{
		if (verticalpos=="fromtop"){
		var pY = ns ? pageYOffset : document.body.scrollTop;
		ftlObj.y += (pY + startY - ftlObj.y)/8;
		}
		else{
		var pY = ns ? pageYOffset + innerHeight : document.body.scrollTop + document.body.clientHeight;
		ftlObj.y += (pY - startY - ftlObj.y)/8;
		}

		if (parseInt(ftlObj.y)>parseInt(getPostion(document.getElementById('hidden_image'), "offsetTop")))
		{
			ftlObj.sP(ftlObj.x, ftlObj.y);
		} else
		{
			ftlObj.sP(ftlObj.x, parseInt(getPostion(document.getElementById('hidden_image'), "offsetTop")));
		}
		setTimeout("stayTopLeft()", 15);
	}
	ftlObj = ml("divStayTopLeft");
	stayTopLeft();
}


function jChangeContent( objID, new_content ) 
{
	var obj = document.getElementById(objID);
	if (obj!=null)
	{
		obj.innerHTML = new_content;
	}
}

/*
* function for valid numeric strings
*/
function jIS_NUMERIC(jParString)	
{
	var strValidChars = "0123456789.-";
	var strChar;
	var blnResult = true;

	if (jParString.length == 0) return false;

	//  test jParString consists of valid characters listed above
	for (i = 0; i < jParString.length && blnResult == true; i++)
	{
		strChar = jParString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1)
		{
			blnResult = false;
		}
	}
	return blnResult;
}

/*
* function to check whether value is in a array
*/
function jIN_ARRAY(jParArray, jParString)	
{
	var len = jParArray.length;
	for ( var x = 0 ; x <= len ; x++ ) {
		if ( jParArray[x] == jParString ) return true;
	}
	return false;
}

function findPos(obj) {
	var curleft = curtop = 0;
	if (obj.offsetParent)
	{
		curleft = obj.offsetLeft;
		curtop = obj.offsetTop;
		while (obj = obj.offsetParent)
		{
			curleft += obj.offsetLeft;
			curtop += obj.offsetTop;
		}
	}
	return [curleft,curtop];
}

function strReplaceAll(s, t, f){
	var tmp1=s;
	var tmp2="";

	while (tmp1.indexOf(t)!=-1)
	{
		tmp2=tmp2+tmp1.substring(0,tmp1.indexOf(t))+f;
		tmp1=tmp1.substring(tmp1.indexOf(t)+t.length);
	}
	tmp2=tmp2+tmp1;

	return tmp2;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function checkPick(obj,element,page,type){
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2)
	else{
		if (page == 'public.php' || page == 'private.php') {
			switch (type) {
			    case 1 :
			        msg = globalAlertActivate; break;
			    case 2 :
			        msg = globalAlertDeactivate; break;
			    default :
			        msg = "Are you sure?" ; break;
			}
			if(confirm(msg)){
				obj.action=page;
				obj.submit();
			}
		}
		else
		{
			obj.action=page;
			obj.submit();
		}
	}
}

/*
* Modified by Key 2008-08-01
* Updated the checkPick Fucntion , not only to check the page name instead of alert variable
*/
function jCheckPick(obj,element,page,type, isAlert)
{
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else
	{
		if (isAlert == 1)
		{
			switch (type) 
			{
			    case 1 :
			        msg = globalAlertActivate; break;
			    case 2 :
			        msg = globalAlertDeactivate; break;
			    default :
			        msg = "Are you sure?" ; break;
			}
			if(confirm(msg))
			{
				obj.action=page;
				obj.submit();
			}
		}
		else
		{
			obj.action=page;
			obj.submit();
		}
	}
} // end function check pick

// Compare Start Time & End Time and Compare Start Date & End Date in hr/min selection box
function compareTime(date_s, hr_s, min_s, date_e, hr_e, min_e, sec_s, sec_e) {
	var time1 = toTimeStr(date_s, hr_s, min_s, sec_s);
	var time2 = toTimeStr(date_e, hr_e, min_e, sec_e);

	if (time1>time2) {
	    return false;
	}else {
		return true;
	}
}

// Combine Date and Time 
function toTimeStr(date_o, hr_o, min_o, sec_o) {
    var hr_v = parseInt(hr_o.options[hr_o.selectedIndex].value);
	if (hr_v<10) {
	    hr_v = "0"+hr_v;
	}
	min_v = parseInt(min_o.options[min_o.selectedIndex].value);
	if (min_v<10) {
	    min_v = "0"+min_v;
	}

	var time1 = date_o.value + hr_v + min_v;

	if(sec_o)
	{
		sec_v = parseInt(sec_o.options[sec_o.selectedIndex].value);
		if (sec_v<10) {
		    sec_v = "0"+sec_v;
		    time1 = time1+sec_v;
		}
	}
	
	return time1;
}

function compareTimeByObjId(dateStartId, hourStartId, minuteStartId, secondStartId, dateEndId, hourEndId, minuteEndId, secondEndId) {
	var dateStart = (dateStartId=='')? '0000-00-00' : $('#' + dateStartId).val();
	var hourStart = (hourStartId=='')? '00' : str_pad($('#' + hourStartId).val(), 2);
	var minStart = (minuteStartId=='')? '00' : str_pad($('#' + minuteStartId).val(), 2);
	var secStart = (secondStartId=='')? '00' : str_pad($('#' + secondStartId).val(), 2);
	var timeTextStart = dateStart + hourStart + minStart + secStart;
	
	var dateEnd = (dateEndId=='')? '0000-00-00' : $('#' + dateEndId).val();
	var hourEnd = (hourEndId=='')? '00' : str_pad($('#' + hourEndId).val(), 2);
	var minEnd = (minuteEndId=='')? '00' : str_pad($('#' + minuteEndId).val(), 2);
	var secEnd = (secondEndId=='')? '00' : str_pad($('#' + secondEndId).val(), 2);
	var timeTextEnd = dateEnd + hourEnd + minEnd + secEnd;
	
	return (timeTextStart > timeTextEnd)? false : true;
}

function checkTimeRangeOverlappedByObjId(dateStartId1, hourStartId1, minuteStartId1, secondStartId1, dateEndId1, hourEndId1, minuteEndId1, secondEndId1, dateStartId2, hourStartId2, minuteStartId2, secondStartId2, dateEndId2, hourEndId2, minuteEndId2, secondEndId2) {
	var dateStart1 = (dateStartId1=='')? '0000-00-00' : $('#' + dateStartId1).val();
	var hourStart1 = (hourStartId1=='')? '00' : str_pad($('#' + hourStartId1).val(), 2);
	var minStart1 = (minuteStartId1=='')? '00' : str_pad($('#' + minuteStartId1).val(), 2);
	var secStart1 = (secondStartId1=='')? '00' : str_pad($('#' + secondStartId1).val(), 2);
	var timeTextStart1 = dateStart1 + hourStart1 + minStart1 + secStart1;
	
	var dateEnd1 = (dateEndId1=='')? '0000-00-00' : $('#' + dateEndId1).val();
	var hourEnd1 = (hourEndId1=='')? '00' : str_pad($('#' + hourEndId1).val(), 2);
	var minEnd1 = (minuteEndId1=='')? '00' : str_pad($('#' + minuteEndId1).val(), 2);
	var secEnd1 = (secondEndId1=='')? '00' : str_pad($('#' + secondEndId1).val(), 2);
	var timeTextEnd1 = dateEnd1 + hourEnd1 + minEnd1 + secEnd1;
	
	var dateStart2 = (dateStartId2=='')? '0000-00-00' : $('#' + dateStartId2).val();
	var hourStart2 = (hourStartId2=='')? '00' : str_pad($('#' + hourStartId2).val(), 2);
	var minStart2 = (minuteStartId2=='')? '00' : str_pad($('#' + minuteStartId2).val(), 2);
	var secStart2 = (secondStartId2=='')? '00' : str_pad($('#' + secondStartId2).val(), 2);
	var timeTextStart2 = dateStart2 + hourStart2 + minStart2 + secStart2;
	
	var dateEnd2 = (dateEndId2=='')? '0000-00-00' : $('#' + dateEndId2).val();
	var hourEnd2 = (hourEndId2=='')? '00' : str_pad($('#' + hourEndId2).val(), 2);
	var minEnd2 = (minuteEndId2=='')? '00' : str_pad($('#' + minuteEndId2).val(), 2);
	var secEnd2 = (secondEndId2=='')? '00' : str_pad($('#' + secondEndId2).val(), 2);
	var timeTextEnd2 = dateEnd2 + hourEnd2 + minEnd2 + secEnd2;
	
	return ((timeTextStart1 < timeTextEnd2) && (timeTextEnd1 > timeTextStart2))? true : false;
}

// Compare Start Time & End Time and Compare Start Date & End Date in hr/min input box
function compareTimeInput(date_s, hr_s, min_s, date_e, hr_e, min_e) {
	var time1 = toTimeStrByInput(date_s, hr_s, min_s);
	var time2 = toTimeStrByInput(date_e, hr_e, min_e);

	if (time1>time2) {
	    return false;
	}else {
		return true;
	}
}

// Combine Date and Time by input box
function toTimeStrByInput(date_o, hr_o, min_o) {
    var hr_v = parseInt(hr_o.value);
	if (hr_v<10) {
	    hr_v = "0"+hr_v;
	}
	min_v = parseInt(min_o.value);
	if (min_v<10) {
	    min_v = "0"+min_v;
	}
	var time1 = date_o.value + hr_v + min_v;
	return time1;
}


// Submit to Pop-up Windows for Ip v2.5
function checkEditContent(obj,element,page){
        if(countChecked(obj,element)==1) {
                obj.action=page;
                obj.target='_blank';
                obj.submit();
                obj.target='';
        } else {
                alert(globalAlertMsg1);
        }
}

//Add more files 
function jAddMoreFileField(jFormObj, jTableName, jFileNumberName, jFileFieldName)
{
	// prepare td contents
	// manipulate jInsertTableRow()

	if (document.getElementsByTagName('*') || document.getElementById)
	{
		var table = document.getElementsByTagName('*') ? document.getElementsByTagName('*')[jTableName] : document.getElementById(jTableName);
		var file_no = parseInt(document.getElementById(jFileNumberName).value);

		if (document.getElementsByTagName('*'))
		{
			var td = "<input class=file type=file name='" + jFileFieldName + (file_no + 1) + "' size='40' /><input type=hidden name='" + jFileFieldName + (file_no + 1) + "_hidden' />";
			jInsertTableRow(table, file_no, td);
			document.getElementById(jFileNumberName).value = file_no + 1;
		}
	}
}

function jInsertTableRow(jTableName, jFileNumber){
	// first argument is jTableName
	// other arguments after will be td contents

	var row = jTableName.insertRow(jFileNumber);

	for(var i=2; i<arguments.length; i++){
        cell = row.insertCell(i-2);
		cell.innerHTML = arguments[i];
    }
}

// generate options from jArrData and bold selected option
function jSelectPresetTextECLASS(jArrData, jTarget, jMenu, jPos, jOtherFunction, jWidth, jAppend, jRowHeight)
{
	var listStr = "";
	var fieldValue;
	var count = 0;
	var btn_hide = (typeof(js_btn_hide)!="undefined") ? js_btn_hide : "";
	jWidth = jWidth || 160;

	var box_height = (jArrData.length>10) ? "200" : "105";
	listStr += "<div style='background-color:#ffffff; border:1px solid #FFFFFF;'>";
	listStr += "	<div style='background-color:#CCCCCC; border-left:1px solid #CCCCCC; border-top:1px solid #CCCCCC; border-right:1px solid #CCCCCC'>&nbsp;<a href=\"javascript:void(0)\" onClick=\"document.getElementById('" + jMenu + "').style.display= 'none';setDivVisible(false, '"+jMenu+"', 'lyrShim')\" title=\""+btn_hide+"\" ><font face='arial' color='#FFFFFF'><b>x</b></font></a></div>";
	listStr += "	<div style='border-left:1px solid #CCCCCC; border-bottom:1px solid #CCCCCC; border-right:1px solid #CCCCCC'>";
	listStr += "		<div style=\"overflow: auto; height: " + box_height + "px; width: " + jWidth + "px; \">";
	listStr += "			<table width=\"90%\" border='0' align='center' class='tbclip'>";

	if (typeof(jArrData)=="undefined" || jArrData.length==0)
	{
		if (typeof(js_preset_no_record)!="undefined")
		{
			listStr += "<tr><td class='guide' align='center'>"+js_preset_no_record+"</td></tr>";
		}
	} else
	{
		for (var i = 0; i < jArrData.length; i++)
		{
			for (var j = 0; j < jArrData[i].length; j++)
			{
				fieldValue = document.getElementById(jTarget).value.replace(/&/g, "&amp;").replace(/'/g, "&#96;").replace(/`/g, "&#96;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");

				listStr += "<tr><td style=\"border-bottom:1px #CCCCCC solid; background-color: ";
				if (fieldValue == jArrData[i][j])
				{
					listStr += "#FFF2C3;";
				} else
				{
					listStr += "#FFFFFF;";
				}
				if (jRowHeight != "") {
					listStr += " height:" + jRowHeight + "px;";
				}
				listStr += "\">";

				if (fieldValue == jArrData[i][j])
				{
					listStr += "<b>";
				}
				jOtherFunctionTxt = (jOtherFunction=="jPRESET_ELE()") ? "if(typeof(jPresetELE)!='undefined'){jPRESET_ELE('"+i+"', '"+j+"');}" : "";
				if (jAppend == 1) {
					listStr += "<a href=\"javascript:void(0)\" onClick=\"appendRow2TextArea('" + jTarget + "', '" + jArrData[i][j].replace(/&#039;/g, "\\'") + "', '" + jMenu + "');"+jOtherFunctionTxt+"\" title=\"" + jArrData[i][j] + "\">" + jArrData[i][j] + "</a>";
				}
				else {
					listStr += "<a href=\"javascript:void(0)\" onClick=\"document.getElementById('" + jTarget + "').value = '" + jArrData[i][j].replace(/&#039;/g, "\\'") + "' ;document.getElementById('" + jMenu + "').style.display= 'none';setDivVisible(false, '"+jMenu+"', 'lyrShim');"+jOtherFunctionTxt+"\" title=\"" + jArrData[i][j] + "\">" + jArrData[i][j] + "</a>";					
				}
				//listStr += "<a href=\"javascript:void(0)\" onClick=\"document.getElementById('" + jTarget + "').value = \\\"" + jArrData[i][j] + "\\\" ;document.getElementById('" + jMenu + "').style.display= 'none';setDivVisible(false, '"+jMenu+"', 'lyrShim');"+jOtherFunctionTxt+"\" title=\"" + jArrData[i][j] + "\">" + jArrData[i][j] + "</a>";
				if (fieldValue == jArrData[i][j])
				{
					listStr += "</b>";
				}

				listStr += "</td></tr>";
				count ++;
			}
		}
	}

	listStr += "</table></div></div></div>";

	document.getElementById(jMenu).innerHTML = listStr;

	document.getElementById(jMenu).style.position = "absolute";
	document.getElementById(jMenu).style.zIndex = "9999999";
	document.getElementById(jMenu).style.textalign = "left";

	var pos = findPos(document.getElementById("posimg" + jPos));

	document.getElementById(jMenu).style.left = (pos[0] + 0) + "px";
	document.getElementById(jMenu).style.top = pos[1] + "px";
	document.getElementById(jMenu).style.display= 'block';

	// Re-assign the innerHTML to workaround a possible problem in IE7 that the popup may appear taller on first load.
	document.getElementById(jMenu).innerHTML = listStr;

	setDivVisible(true, jMenu, "lyrShim");

}

function Big5FileUploadHandler(jFormObj, jFileName, jFileNo) {

	for (var i = 1; i <= jFileNo; i++)
	{
		chk_undefined = "typeof(jFormObj." + jFileName + i + ")!='undefined'";
		chk_empty = "jFormObj." + jFileName + i + ".value != ''";
		if (eval(chk_undefined))
		{
			if (eval(chk_empty))
			{
				eval("var Ary = jFormObj." + jFileName + i + ".value.split('\\\\')");
				eval("jFormObj." + jFileName + i + "_hidden.value=Ary[Ary.length-1]");
			}
		}
	}

	return true;
}

function check(from, to)
{
	checkOption(from);
	checkOption(to);
	i = from.selectedIndex;
	while(i!=-1)
	{
		to.options[to.length] = new Option(from.options[i].text, from.options[i].value, false, false);
		from.options[i] = null;
		i = from.selectedIndex;
	}
}

function postInstantForm(obj, url, method, winType, winName) {
	winType = (typeof(winType)=="number") ? winType.toString() : winType;
	var aWin = (winType.indexOf(",")!=-1) ? eval("newWindow('',"+winType+")") : newWindow("",winType);
	var frmTargetOrg = obj.target;
	var frmActionOrg = obj.action;
	var frmMethodOrg = obj.method;

	obj.target = winName;
	if (method!="" && typeof(obj.method)!="object")
	{
		obj.method = method;
	}
	obj.action = url;
	obj.submit();

	obj.target = frmTargetOrg;
	obj.action = frmActionOrg;
	if (typeof(obj.method)!="object")
	{
		obj.method = frmMethodOrg;
	}

	return;
}

function getFileExtension(f) {
		
	var filename = f.value;
	if(filename.length == 0)
		return "";
		
	var dot = filename.lastIndexOf(".");
	if( dot == -1 )
		return "";
		
	var extension = filename.substr(dot,filename.length);
	
	return extension;
}

function Select_All_Selection_Option(SelectObj,SelectValue) {
	var SelectIndex = 0;
	
	if (SelectValue == "") {
		return true;
	}
	else {
		for (var j=0; j< SelectObj[0].length; j++) {
			if (SelectObj[0].options[j].value == SelectValue) {
				SelectIndex = j;
				break;
			}
		}
		
		for (var i=0; i< SelectObj.length; i++) {
			SelectObj[i].selectedIndex = SelectIndex;
		}
		
		return true;
	}
}

// select all options for multipule selection box
function Select_All_Options(SelectID,SelectType) {
	var SelectList = document.getElementById(SelectID);
	for (var j=0; j< SelectList.length; j++) {
		SelectList.options[j].selected = SelectType;
	}
}

function Get_Form_Values(fobj, excluded) {
   var str = "";
   var valueArr = null;
   var val = "";
   var cmd = "";
   for(var i = 0;i < fobj.elements.length;i++) {
   	
   	 if(excluded == fobj.elements[i].name)
   	 	continue;
   	 
   	 //alert(fobj.elements[i].name + ": " + fobj.elements[i].type);
     switch(fobj.elements[i].type) {
       case "text":
       case "textarea":
            str += fobj.elements[i].name +
             "=" + encodeURIComponent(fobj.elements[i].value) + "&";
            break;
       case "select-one":
            str += fobj.elements[i].name +
            "=" + fobj.elements[i].options[fobj.elements[i].selectedIndex].value + "&";
            break;
       case "hidden":
       			str += fobj.elements[i].name +
             "=" + encodeURIComponent(fobj.elements[i].value) + "&";
             break;
       case "checkbox":
       case "radio":
       			if (fobj.elements[i].checked) {
       				str += fobj.elements[i].name + "=" + encodeURIComponent(fobj.elements[i].value) + "&";
       			}
       			 break;
       case "select-multiple":
       			for (var j=0; j< fobj.elements[i].length; j++) {
       				if (fobj.elements[i].options[j].selected) {
       					str += fobj.elements[i].name +
            		"=" + fobj.elements[i].options[j].value + "&";
       				}
       			}
       			 break;
     }
   }
   str = str.substr(0,(str.length - 1));
   
   return str;
}

function Get_Check_Box_Value(CheckBoxID,ReturnType,IgnoreSelectedStatus) {
	var IgnoreSelectedStatus = IgnoreSelectedStatus || false;
	var ReturnType = ReturnType || "Array";
	var ValueArray = Array();
	if (document.getElementsByName(CheckBoxID)) {
		var CheckboxElement = document.getElementsByName(CheckBoxID);
		var ValueStr = '';
		for (var i=0; i< CheckboxElement.length; i++) {
			if (CheckboxElement[i].checked || IgnoreSelectedStatus) 
				ValueStr += CheckboxElement[i].value + ',';
		}
		
		if (ValueStr != "" && ValueStr.lastIndexOf(',') != -1) {
			ValueStr = ValueStr.substring(0,ValueStr.lastIndexOf(','));
			
			ValueArray = ValueStr.split(',');
		}	
	}
	
	if (ReturnType == "Array") {
		return ValueArray;
	}
	// form/ ajax post query string
	else {
		var ValueStr = "";
		for (var i=0; i< ValueArray.length; i++) {
			ValueStr += CheckBoxID + "=" + ValueArray[i] + '&';
		}
		
		return ValueStr;
	}
}

function Get_Checkbox_Value_By_Class(CheckboxClass)
{
	var jsValueArr = Array();
	$('input.' + CheckboxClass + ':checked').each( function() {
		jsValueArr[jsValueArr.length] = $(this).val();
	});
	return jsValueArr.join(',');
}

function Get_Radio_Value(RadioID) {
	if (document.getElementsByName(RadioID)) {
		var RadioElement = document.getElementsByName(RadioID);
		var ValueStr = '';
		for (var i=0; i< RadioElement.length; i++) {
			if (RadioElement[i].checked) {
				return RadioElement[i].value;
				break;
			}
		}
	}
	
	return "";
}

function Get_Selection_Value(SelectionID,ReturnType,IgnoreSelectedStatus) {
	var ReturnType = ReturnType || "Array";
	var IgnoreSelectedStatus = IgnoreSelectedStatus || false;
	var ValueArray = Array();
	var str = '';
	
	if (document.getElementById(SelectionID)) 
	{
		var SelectionObj = document.getElementById(SelectionID);
		switch(SelectionObj.type)
		{
    		case "select-one":
    		str += SelectionObj.options[SelectionObj.selectedIndex].value; 
	      //str += fobj.elements[i].name +
	      //"=" + SelectionObj.options[SelectionObj.selectedIndex].value + "&";
	      break;
      		case "select-multiple":
				for (var j=0; j< SelectionObj.length; j++) {
					if (SelectionObj.options[j].selected || IgnoreSelectedStatus) {
	 					str += SelectionObj.options[j].value + ",";
	 				}
	 			}
	 			break;
  		}
	  	if (str.lastIndexOf(',') != -1) 
	  		str = str.substring(0,str.lastIndexOf(','));
	  		
	  	if (str != "") 
	  		ValueArray = str.split(',');
	}else if (document.getElementsByName(SelectionID) && document.getElementsByName(SelectionID).length > 0) 
	{
		var SelectionObj = document.getElementsByName(SelectionID)[0];
		switch(SelectionObj.type)
		{
    		case "select-one":
    		str += SelectionObj.options[SelectionObj.selectedIndex].value; 
	      //str += fobj.elements[i].name +
	      //"=" + SelectionObj.options[SelectionObj.selectedIndex].value + "&";
	      break;
      		case "select-multiple":
				for (var j=0; j< SelectionObj.length; j++) {
					if (SelectionObj.options[j].selected || IgnoreSelectedStatus) {
	 					str += SelectionObj.options[j].value + ",";
	 				}
	 			}
	 			break;
  		}
  		
	  	if (str.lastIndexOf(',') != -1) 
	  		str = str.substring(0,str.lastIndexOf(','));
	  		
	  	if (str != "") 
	  		ValueArray = str.split(',');
	}
		
	if (ReturnType == "Array") {
		return ValueArray;
	}
	// normal string -- onlu for select-one element
	else if (ReturnType == "String") {
		return str;
	}
	// form/ ajax post query string
	else {
		str = "";
		for (var i=0; i< ValueArray.length; i++) {
			str += SelectionID + "=" + ValueArray[i] + '&';
		}
		
		return str;
	}
}

function Set_Checkbox_Value(CheckboxName,CheckStatus) {
	var obj = document.getElementsByName(CheckboxName);
	
	for (var i=0; i< obj.length; i++) {
		obj[i].checked = CheckStatus;
	}
}

function Uncheck_SelectAll(jsSelectAllObjID, jsChecked)
{
	if (jsChecked == false)
		$('input#' + jsSelectAllObjID).attr('checked', jsChecked);
}

function Check_All_Options_By_Class(jsOptionClass, jsChecked, jsEnabledOnly)
{
	jsEnabledOnly = jsEnabledOnly || 0;
	var jsEnableConds = '';
	if (jsEnabledOnly == 1) {
		jsEnableConds = ':enabled';
	}
	
	$('.' + jsOptionClass + jsEnableConds).attr('checked', jsChecked);
}

function Apply_Value_To_All(jsClass, jsValue)
{
	$('.' + jsClass).val(jsValue);
}

// function for return message
{
var TimeoutObj;
function Get_Return_Message(Message, MsgDivID) {
	var clearMsg = globalClearMsgForUpdateStatus;
	if(Trim(Message) != ''){
		clearTimeout(TimeoutObj);
		var ReturnMessage = Message.split('|=|');
		
		if (MsgDivID == null)
			MsgDivID = "div.SystemReturnMessage";
		else	
			MsgDivID = '#' + MsgDivID;
			
		var MessageLayer = $(MsgDivID);
		
		if (ReturnMessage[0] == "0") 
			MessageLayer.addClass("warning_box");
		else 
			MessageLayer.removeClass("warning_box");
		




		var Temp = ReturnMessage[1];
		Temp += ' <a href="#" onclick="$(\'div.SystemReturnMessage\').css(\'visibility\',\'hidden\'); return false;">['+clearMsg+']</a>';
		$("div.msg_board_right").html(Temp);
		MessageLayer.css("visibility",'visible').css("display",'block');
		TimeoutObj = setTimeout("Hide_Return_Message();",1000*5);
	}
}

function Hide_Return_Message() {
	$("div.SystemReturnMessage").css("visibility","hidden").css("display",'none');
	clearTimeout(TimeoutObj);
}

function Show_jEditable_Edit_Background(Obj, js_PATH_WRT_ROOT, js_LAYOUT_SKIN) {
	Obj.style.backgroundImage = "url(" + js_PATH_WRT_ROOT + "/images/" + js_LAYOUT_SKIN + "/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_jEditable_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

function js_Hide_ThickBox()
{
	tb_remove();
}

function initThickBox(){   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}

function js_Show_ThickBox(Caption, Height, Width, imageGroup)
{
	var imageGroup = imageGroup || '';
	var Caption = Caption || '';
	var Height = Height || '400';
	var Width = Width || '600';
 	tb_show(Caption,"#TB_inline?height="+Height+"&width="+Width+"&inlineId=FakeLayer",imageGroup);
}
}

// block ui functions
{
var isBlocking = false;
var blockMsg = "Loading (載入中..)";
function Block_Thickbox(){
	if (!isBlocking) {
		//$('#TB_window').block(blockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
		$('#TB_window').block({message:blockMsg, overlayCSS: {backgroundColor:'#ccc' ,border:'3px solid #ccc' }});
	}
}

function UnBlock_Thickbox() {
	$('#TB_window').unblock();
	isBlocking = false;
}

function Block_Document(jsMyBlockMsg, jsWidth) {
	if (!isBlocking) {
		
		var TargetBlockMsg;
		if (jsMyBlockMsg == null || jsMyBlockMsg == '')
			TargetBlockMsg = blockMsg;
		else
			TargetBlockMsg = jsMyBlockMsg;
		
		if (jsWidth == null || jsWidth == '')
			jsWidth = '250px';
		else
			jsWidth = jsWidth;
		
		//$('body').block(TargetBlockMsg, { color:'#ccc' , border:'3px solid #ccc', width:jsWidth });
		$('body').block({message:TargetBlockMsg, overlayCSS: {backgroundColor:'#ccc' ,border:'3px solid #ccc' }, width:jsWidth });
	}
}

function UnBlock_Document() {
	$('body').unblock();
	isBlocking = false;
}

var ElementPositionArr = new Object();
function Block_Element(ElementID, jsMyBlockMsg) {

	ElementPositionArr[ElementID] = $('#'+ElementID).css("position")
	
	var TargetBlockMsg;
	if (jsMyBlockMsg == null || jsMyBlockMsg == '')
		TargetBlockMsg = blockMsg;
	else
		TargetBlockMsg = jsMyBlockMsg;
	
	//$('#'+ElementID).block(TargetBlockMsg, { color:'#ccc' ,border:'3px solid #ccc' });
	$('#'+ElementID).block({message:TargetBlockMsg, overlayCSS: {backgroundColor:'#ccc' ,border:'3px solid #ccc' }});

}

function UnBlock_Element(ElementID) {
	if(ElementPositionArr[ElementID])
		var RollbackPosition = ElementPositionArr[ElementID];
		
	if ($('#'+ElementID+' > .blockUI').length) {
		$('#'+ElementID).unblock();
		
	}
	$('#'+ElementID).css("position",RollbackPosition);
	
}
}

// Reload location selection
function js_Change_Floor_Selection(js_PATH_WRT_ROOT, BuildingSelID, OthersOptTbID, 
									FloorSelTrID, FloorSelDivID, FloorSelID, FloorSelFirstTitle, 
									RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange, RoomSelIsMultiple,
									WithoutSubmitBtn)
{
	var SelectedBuildingID = $('select#' + BuildingSelID).val();
	
	if (RoomSelIsMultiple == null)
		RoomSelIsMultiple = '';
		
	if (WithoutSubmitBtn == null)
		WithoutSubmitBtn = '';
	
	// Hide Row and Div of Floor and Room
	if (FloorSelTrID != '')
		$('tr#' + FloorSelTrID).hide();
	$('div#' + FloorSelDivID).hide();
	
	if (RoomSelTrID != '')
		$('tr#' + RoomSelTrID).hide();
	$('div#' + RoomSelDivID).hide();
	
	if (OthersOptTbID != '')
		$('input#' + OthersOptTbID).hide();
	
	// Hide the Submit Btn
	if (WithoutSubmitBtn == '')
		$("div#SubmitBtnDiv").hide();
	
	if (SelectedBuildingID != "")
	{
		if (SelectedBuildingID == "-1")
		{
			// Show input box for user to add customized location
			$('input#' + OthersOptTbID).show();
			$('input#' + OthersOptTbID).focus();
			if (OthersOptTbID != '')
				$("div#SubmitBtnDiv").show();
		}
		else
		{
			// Refresh Floor Selection and Room Selection
			$('div#' + FloorSelDivID).load(
				js_PATH_WRT_ROOT + "home/system_settings/location/ajax_reload_selection.php", 
				{ 
					RecordType: 'Reload_Floor_Selection',
					js_PATH_WRT_ROOT: js_PATH_WRT_ROOT,
					BuildingID: SelectedBuildingID,
					FloorSelID: FloorSelID,
					FloorSelFirstTitle: FloorSelFirstTitle,
					RoomSelTrID: RoomSelTrID,
					RoomSelDivID: RoomSelDivID,
					RoomSelID: RoomSelID,
					RoomSelFirstTitle: RoomSelFirstTitle,
					RoomSelOnchange: RoomSelOnchange,
					RoomSelIsMultiple: RoomSelIsMultiple
				},
				function(ReturnData)
				{
					// Display Floor Selection
					if (FloorSelTrID != '')
						$('tr#' + FloorSelTrID).show();
						
					$('div#' + FloorSelDivID).show();
					
					// Reload Room Selection
					js_Change_Room_Selection(js_PATH_WRT_ROOT, FloorSelID, RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange, RoomSelIsMultiple);
					
					//$('#IndexDebugArea').html(ReturnData);
					//$('#debugArea').html(ReturnData);
				}
			);
		}
	}
}

function js_Change_Room_Selection(js_PATH_WRT_ROOT, FloorSelID, RoomSelTrID, RoomSelDivID, RoomSelID, RoomSelFirstTitle, RoomSelOnchange, RoomSelIsMultiple)
{
	var SelectedLocationLevelID = $('#' + FloorSelID).val();
	
	if (RoomSelIsMultiple == null)
		RoomSelIsMultiple = '';
	
	if (RoomSelTrID != '')
		$('tr#' + RoomSelTrID).hide();
	$('div#' + RoomSelDivID).hide();
	
	if (SelectedLocationLevelID != "")
	{
		// Refresh Room Selection
		$('div#' + RoomSelDivID).load(
			js_PATH_WRT_ROOT + "home/system_settings/location/ajax_reload_selection.php", 
			{ 
				RecordType: 'Reload_Room_Selection',
				LocationLevelID: SelectedLocationLevelID,
				RoomSelID: RoomSelID,
				RoomSelFirstTitle: RoomSelFirstTitle,
				RoomSelOnchange: RoomSelOnchange,
				RoomSelIsMultiple: RoomSelIsMultiple
			},
			function(ReturnData)
			{
				// Display Room Selection
				if (RoomSelIsMultiple)
				{
					js_Select_All(RoomSelID, 1);
					$("div#SubmitBtnDiv").show();
				}
				
				if (RoomSelTrID != '')
					$('tr#' + RoomSelTrID).show();
				
				$('div#' + RoomSelDivID).show();
				
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
}

function js_Select_All(SelectionObjID, ScrollToTop, FunctionEval)
{
	ScrollToTop = ScrollToTop || false;
	
	SelectionObjID = SelectionObjID.replace('[', '\\[').replace(']', '\\]');
	//$('select#' + SelectionObjID + ' > option[selected!=true]').attr("selected","selected");
	$('select#' + SelectionObjID).find('option[selected!=true]').attr("selected","selected");
	
	if (ScrollToTop==1)
		$('select#' + SelectionObjID).scrollTo(0,800,{queue:true});	// scroll back to top
	
	if (FunctionEval == null || FunctionEval == '') {
		// do nth
	}
	else {
		eval(FunctionEval);
	}
}

function js_Select_All_With_OptGroup(SelectionObjID, ScrollToTop, FunctionEval)
{
	js_Select_All(SelectionObjID, ScrollToTop, FunctionEval);
/*
	ScrollToTop = ScrollToTop || false;
	
	js_Select_All(SelectionObjID);
	
	SelectionObjID = SelectionObjID.replace('[', '\\[').replace(']', '\\]');
	$('select#' + SelectionObjID + ' > optgroup > option[selected!=true]').attr("selected","selected");
	
	if (ScrollToTop==1)
		$('select#' + SelectionObjID).scrollTo(0,800,{queue:true});	// scroll back to top
	
	if (FunctionEval == null || FunctionEval == '') {
		// do nth
	}
	else {
		eval(FunctionEval);
	}
*/
}

function js_Is_Input_Blank(TargetInputID, WarningDivID, WarningMsg)
{
	if (Trim($('input#' + TargetInputID).val()) == '')
	{
		$('div#' + WarningDivID).html(WarningMsg);
		$('div#' + WarningDivID).show();
		return 1;
	}
	else
	{
		$('div#' + WarningDivID).hide();
		return 0;
	}
}

function js_Add_KeyUp_Blank_Checking(TargetInputID, WarningDivID, WarningMsg)
{
	$('input#' + TargetInputID).keyup( function() {
		js_Is_Input_Blank(TargetInputID, WarningDivID, WarningMsg);
	});
}



function Scroll_To_Element(DomID,containerID) {
	containerID = containerID || '';
	
	var targetObj;
	if (containerID=='') {
		targetObj = $(window);
	}
	else {
		targetObj = $('#'+containerID);
	}
	 
	targetObj.scrollTo('#'+DomID,800,{queue:true});
}

function Scroll_To_Top() {
$(window).scrollTo( 0, 800, {queue:true} );
}

function unset_checkall(obj, form_obj)
{
	if(!obj.checked)
		form_obj.checkmaster.checked=false;
}

function embedFlash(areaFlashID, swfPath, flashVarsStr, flashColor, flashWidth, flashHeight, flashVersion, IsTransparent){
	var IsTransparent = IsTransparent || "YES";
	var areaFlash = document.getElementById(areaFlashID);
	while(areaFlash.hasChildNodes()){
		areaFlash.removeChild(areaFlash.firstChild);
	}
	
	var flashvars = flashVarsStr;
	
	if (IsTransparent == "YES") {
		WindowMode = 'transparent';
	}
	else {
		WindowMode = 'window';
	}
	var params = {
		play: "true",
		loop: "true",
		menu: "true",
		quality: "high",
		//scale: "showall",
		scale: "noscale",
		//wmode:"transparent",
		wmode: WindowMode,
		bgcolor: flashColor,
		//devicefont: "false",
		allowscriptaccess: "sameDomain",
		allowfullscreen: "true"
	};
	var attributes = {
		id: areaFlashID,
		name: areaFlashID,
		align: "middle"
	};
	
	swfobject.embedSWF(swfPath, areaFlashID, flashWidth, flashHeight, flashVersion, "expressInstall.swf", flashvars, params, attributes);
}

// javascript simulate of PHP in_array function
Array.prototype.in_array = function(p_val) {
	for(var i = 0; i = this.length; i++) {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}


// function to replace document.getElementsByName(name) as this getElementsByName only works for form element (E.G. <input>) in IE
function Browser_Proof_Get_Elements_By_Name(name,tagName) {   
	var returns = document.getElementsByName(name);   
	if(returns.length > 0) return returns;   
	
	returns = new Array();   
	var e = document.getElementsByTagName(tagName);   
	for(i = 0; i < e.length; i++) {   
		if(e[i].getAttribute("name") == name) {   
		returns[returns.length] = e[i];   
		}   
	}   
	return returns;   
}

// javascript simulate of PHP var_dump function 
function var_dump(data, addwhitespace, safety, level) {
    var rtrn = '';
    var dt, it, spaces = '';

    if (!level) {
        level = 1;
    }

    for (var i=0; i<level; i++) {
        spaces += '   ';
    } //end for i<level

    if (typeof(data) != 'object') {
        dt = data;

        if (typeof(data) == 'string') {
            if (addwhitespace == 'html') {
                dt = dt.replace(/&/g,'&amp;');
                dt = dt.replace(/>/g,'&gt;');
                dt = dt.replace(/</g,'&lt;');
            } //end if addwhitespace == html
            dt = dt.replace(/\"/g,'\"');
            dt = '"' + dt + '"';
        } //end if typeof == string

        if (typeof(data) == 'function' && addwhitespace) {
            dt = new String(dt).replace(/\n/g,"\n"+spaces);
            if (addwhitespace == 'html') {
                dt = dt.replace(/&/g,'&amp;');
                dt = dt.replace(/>/g,'&gt;');
                dt = dt.replace(/</g,'&lt;');
            } //end if addwhitespace == html
        } //end if typeof == function

        if (typeof(data) == 'undefined') {
            dt = 'undefined';
        } //end if typeof == undefined

        if (addwhitespace == 'html') {
            if (typeof(dt) != 'string') {
                dt = new String(dt);
            } //end typeof != string
            dt = dt.replace(/ /g,"&nbsp;").replace(/\n/g,"<br>");
        } //end if addwhitespace == html
        return dt;
    } //end if typeof != object && != array

    for (var x in data) {
        if (safety && (level > safety)) {
            dt = '*RECURSION*';
        } else {
            try {
                dt = var_dump(data[x],addwhitespace,safety,level+1);
            } catch (e) {
                continue;
            }
        } //end if-else level > safety
        it = var_dump(x,addwhitespace,safety,level+1);
        rtrn += it + ':' + dt + ',';
        if (addwhitespace) {
            rtrn += '\n'+spaces;
        } //end if addwhitespace
    } //end for...in

    if (addwhitespace) {
        rtrn = '{\n' + spaces + rtrn.substr(0,rtrn.length-(2+(level*3))) + '\n' + spaces.substr(0,spaces.length-3) + '}';
    } else {
        rtrn = '{' + rtrn.substr(0,rtrn.length-1) + '}';
    } //end if-else addwhitespace

    if (addwhitespace == 'html') {
        rtrn = rtrn.replace(/ /g,"&nbsp;").replace(/\n/g,"<br>");
    } //end if addwhitespace == html

    return rtrn;
} //end function var_dump

function isInteger(val)
{
    if(val==null)
    {
        return false;
    }
    if (val.length==0)
    {
        return false;
    }
    for (var i = 0; i < val.length; i++) 
    {
        var ch = val.charAt(i)
        if (i == 0 && ch == "-")
        {
            continue
        }
        if (ch < "0" || ch > "9")
        {
            return false
        }
    }
    return true
}

function is_int(value){ 
   for (i = 0 ; i < value.length ; i++) { 
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) {return false;}
    } 
   return true; 
}

function Create_IFrame(FrameName){
	var Iframe;
	var Hide = true;
	
	if (!document.getElementById(FrameName)) {
		var Iframe;
		try {
			Iframe = document.createElement('<iframe name="'+FrameName+'">');
		}
		catch (ex) {
			Iframe = document.createElement('iframe');
			Iframe.name=FrameName;
		}
		
		Iframe.id = FrameName;
		if (Hide) {
			Iframe.style.height = '0px';
			Iframe.style.width = '0px';
			Iframe.style.border = '0px';
			Iframe.style.visibility = 'hidden';
			Iframe.style.display = 'none';
		}
		else {
			// for debug purpose
			Iframe.style.height = '200px';
			Iframe.style.width = '300px';
			Iframe.style.border = '0px';
			Iframe.style.visibility = 'visible';
			Iframe.style.display = 'block';
		}
		document.body.appendChild(Iframe);
	}
}

function Get_File_Name(FileName) {
	return FileName.substring(FileName.lastIndexOf("\\")+1);
}

function Get_File_Ext(FileName) {
	if (FileName.lastIndexOf(".") == -1) {
		return false;
	}
	else {
		return FileName.substring(FileName.lastIndexOf(".")+1).toLowerCase();
	}
}

function Get_KeyNum(e)
{
	var keynum;
		
	if(window.event) // IE
	{
		e = window.event;
		keynum = e.keyCode;
	}
	else if(e.which) // Netscape/Firefox/Opera
		keynum = e.which;
		
	return keynum;
}

function Check_Pressed_Enter(e)
{
	var jsFlag = false;
	
	if (Get_KeyNum(e) == 13) {
		jsFlag = true;
	}
	
	return jsFlag;
}

String.prototype.Trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}

String.prototype.LTrim = function() {
	return this.replace(/^\s+/,"");
}

String.prototype.RTrim = function() {
	return this.replace(/\s+$/,"");
}

function htmlspecialchars_decode (string, quote_style) {
 
    var hash_map = {}, symbol = '', tmp_str = '', entity = '';
    tmp_str = string.toString();
    
    if (false === (hash_map = this.get_html_translation_table('HTML_SPECIALCHARS', quote_style))) {
        return false;
    }
 
    for (symbol in hash_map) {
        entity  = hash_map[symbol];
        tmp_str = tmp_str.split(entity).join(symbol);
    }
    tmp_str = tmp_str.split('&#039;').join("'");
    
    return tmp_str;
}

function get_html_translation_table (table, quote_style) {
    var entities = {}, hash_map = {}, decimal = 0, symbol = '';
    var constMappingTable = {}, constMappingQuoteStyle = {};
    var useTable = {}, useQuoteStyle = {};
    
    // Translate arguments
    constMappingTable[0]      = 'HTML_SPECIALCHARS';
    constMappingTable[1]      = 'HTML_ENTITIES';
    constMappingQuoteStyle[0] = 'ENT_NOQUOTES';
    constMappingQuoteStyle[2] = 'ENT_COMPAT';
    constMappingQuoteStyle[3] = 'ENT_QUOTES';
 
    useTable       = !isNaN(table) ? constMappingTable[table] : table ? table.toUpperCase() : 'HTML_SPECIALCHARS';
    useQuoteStyle = !isNaN(quote_style) ? constMappingQuoteStyle[quote_style] : quote_style ? quote_style.toUpperCase() : 'ENT_COMPAT';
 
    if (useTable !== 'HTML_SPECIALCHARS' && useTable !== 'HTML_ENTITIES') {
        throw new Error("Table: "+useTable+' not supported');
        // return false;
    }
 
    entities['38'] = '&amp;';
    if (useTable === 'HTML_ENTITIES') {
        entities['160'] = '&nbsp;';
        entities['161'] = '&iexcl;';
        entities['162'] = '&cent;';
        entities['163'] = '&pound;';
        entities['164'] = '&curren;';
        entities['165'] = '&yen;';
        entities['166'] = '&brvbar;';
        entities['167'] = '&sect;';
        entities['168'] = '&uml;';
        entities['169'] = '&copy;';
        entities['170'] = '&ordf;';
        entities['171'] = '&laquo;';
        entities['172'] = '&not;';
        entities['173'] = '&shy;';
        entities['174'] = '&reg;';
        entities['175'] = '&macr;';
        entities['176'] = '&deg;';
        entities['177'] = '&plusmn;';
        entities['178'] = '&sup2;';
        entities['179'] = '&sup3;';
        entities['180'] = '&acute;';
        entities['181'] = '&micro;';
        entities['182'] = '&para;';
        entities['183'] = '&middot;';
        entities['184'] = '&cedil;';
        entities['185'] = '&sup1;';
        entities['186'] = '&ordm;';
        entities['187'] = '&raquo;';
        entities['188'] = '&frac14;';
        entities['189'] = '&frac12;';
        entities['190'] = '&frac34;';
        entities['191'] = '&iquest;';
        entities['192'] = '&Agrave;';
        entities['193'] = '&Aacute;';
        entities['194'] = '&Acirc;';
        entities['195'] = '&Atilde;';
        entities['196'] = '&Auml;';
        entities['197'] = '&Aring;';
        entities['198'] = '&AElig;';
        entities['199'] = '&Ccedil;';
        entities['200'] = '&Egrave;';
        entities['201'] = '&Eacute;';
        entities['202'] = '&Ecirc;';
        entities['203'] = '&Euml;';
        entities['204'] = '&Igrave;';
        entities['205'] = '&Iacute;';
        entities['206'] = '&Icirc;';
        entities['207'] = '&Iuml;';
        entities['208'] = '&ETH;';
        entities['209'] = '&Ntilde;';
        entities['210'] = '&Ograve;';
        entities['211'] = '&Oacute;';
        entities['212'] = '&Ocirc;';
        entities['213'] = '&Otilde;';
        entities['214'] = '&Ouml;';
        entities['215'] = '&times;';
        entities['216'] = '&Oslash;';
        entities['217'] = '&Ugrave;';
        entities['218'] = '&Uacute;';
        entities['219'] = '&Ucirc;';
        entities['220'] = '&Uuml;';
        entities['221'] = '&Yacute;';
        entities['222'] = '&THORN;';
        entities['223'] = '&szlig;';
        entities['224'] = '&agrave;';
        entities['225'] = '&aacute;';
        entities['226'] = '&acirc;';
        entities['227'] = '&atilde;';
        entities['228'] = '&auml;';
        entities['229'] = '&aring;';
        entities['230'] = '&aelig;';
        entities['231'] = '&ccedil;';
        entities['232'] = '&egrave;';
        entities['233'] = '&eacute;';
        entities['234'] = '&ecirc;';
        entities['235'] = '&euml;';
        entities['236'] = '&igrave;';
        entities['237'] = '&iacute;';
        entities['238'] = '&icirc;';
        entities['239'] = '&iuml;';
        entities['240'] = '&eth;';
        entities['241'] = '&ntilde;';
        entities['242'] = '&ograve;';
        entities['243'] = '&oacute;';
        entities['244'] = '&ocirc;';
        entities['245'] = '&otilde;';
        entities['246'] = '&ouml;';
        entities['247'] = '&divide;';
        entities['248'] = '&oslash;';
        entities['249'] = '&ugrave;';
        entities['250'] = '&uacute;';
        entities['251'] = '&ucirc;';
        entities['252'] = '&uuml;';
        entities['253'] = '&yacute;';
        entities['254'] = '&thorn;';
        entities['255'] = '&yuml;';
    }
 
    if (useQuoteStyle !== 'ENT_NOQUOTES') {
        entities['34'] = '&quot;';
    }
    if (useQuoteStyle === 'ENT_QUOTES') {
        entities['39'] = '&#39;';
    }
    entities['60'] = '&lt;';
    entities['62'] = '&gt;';
 
 
    // ascii decimals to real symbols
    for (decimal in entities) {
        symbol = String.fromCharCode(decimal);
        hash_map[symbol] = entities[decimal];
    }
    
    return hash_map;
}

function htmlspecialchars (string, quote_style, charset, double_encode) {
    // Convert special characters to HTML entities  
    var optTemp = 0, i = 0, noquotes= false;
    if (typeof quote_style === 'undefined' || quote_style === null) {        quote_style = 2;
    }
    string = string.toString();
    if (double_encode !== false) { // Put this first to avoid double-encoding
        string = string.replace(/&/g, '&amp;');    }
    string = string.replace(/</g, '&lt;').replace(/>/g, '&gt;');
 
    var OPTS = {
        'ENT_NOQUOTES': 0,        'ENT_HTML_QUOTE_SINGLE' : 1,
        'ENT_HTML_QUOTE_DOUBLE' : 2,
        'ENT_COMPAT': 2,
        'ENT_QUOTES': 3,
        'ENT_IGNORE' : 4    };
    if (quote_style === 0) {
        noquotes = true;
    }
    if (typeof quote_style !== 'number') { // Allow for a single string or an array of string flags        quote_style = [].concat(quote_style);
        for (i=0; i < quote_style.length; i++) {
            // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
            if (OPTS[quote_style[i]] === 0) {
                noquotes = true;            }
            else if (OPTS[quote_style[i]]) {
                optTemp = optTemp | OPTS[quote_style[i]];
            }
        }        quote_style = optTemp;
    }
    if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
        string = string.replace(/'/g, '&#039;');
    }    if (!noquotes) {
        string = string.replace(/"/g, '&quot;');
    }
 
    return string;
}

function addslashes (str) {
    // Escapes single quote, double quotes and backslash characters in a string with backslashes  
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}

function stripslashes (str) {
    return (str + '').replace(/\\(.?)/g, function (s, n1) {
        switch (n1) {
        case '\\':
            return '\\';
        case '0':
            return '\u0000';
        case '':
            return '';
        default:
            return n1;
        }
    });
}

function js_ShowHide_Layer(jsDivID)
{
	if (document.getElementById(jsDivID).style.visibility == 'visible')
		MM_showHideLayers(jsDivID, '', 'hide');
	else
		MM_showHideLayers(jsDivID, '', 'show');
}

//function js_Hide_Option_Div(jsDivID)
//{
//	var jsShowSpanID = "spanShowOption_" + jsDivID;
//	var jsHideSpanID = "spanHideOption_" + jsDivID;
//	
//	$('div#' + jsDivID).slideUp('slow', 'linear');
//	$('span#' + jsShowSpanID).slideDown('slow');
//	$('span#' + jsHideSpanID).slideUp('slow');
//}

//function js_Show_Option_Div(jsDivID)
//{
//	var jsShowSpanID = "spanShowOption_" + jsDivID;
//	var jsHideSpanID = "spanHideOption_" + jsDivID;
//	
//	$('div#' + jsDivID).slideDown('slow', 'linear');
//	$('span#' + jsShowSpanID).slideUp('slow');
//	$('span#' + jsHideSpanID).slideDown('slow');
//}

//Copy from EJ
function js_Hide_Option_Div(jsDivID, jsTdId)
{
	jsTdId = jsTdId || '';
	
	var jsShowSpanID = "spanShowOption_" + jsDivID;
	var jsHideSpanID = "spanHideOption_" + jsDivID;
	
//	$('div#' + jsDivID).slideUp('slow', 'linear');
//	$('span#' + jsShowSpanID).slideDown('slow');
//	$('span#' + jsHideSpanID).slideUp('slow');
	$('div#' + jsDivID).hide();
	$('span#' + jsShowSpanID).show();
	$('span#' + jsHideSpanID).hide();
	
	if (jsTdId != '') {
		$('td#' + jsTdId).addClass('report_show_option');
	}
}

function js_Show_Option_Div(jsDivID, jsTdId)
{
	jsTdId = jsTdId || '';
	
	var jsShowSpanID = "spanShowOption_" + jsDivID;
	var jsHideSpanID = "spanHideOption_" + jsDivID;
	
//	$('div#' + jsDivID).slideDown('slow', 'linear');
//	$('span#' + jsShowSpanID).slideUp('slow');
//	$('span#' + jsHideSpanID).slideDown('slow');
	$('div#' + jsDivID).show();
	$('span#' + jsShowSpanID).hide();
	$('span#' + jsHideSpanID).show();
	
	if (jsTdId != '') {
		$('td#' + jsTdId).removeClass('report_show_option');
	}
}

function js_Hide_All_Option_Layer(jsExceptDivID)
{
	var jsTempDivID;
	
	$('a.option_layer').each( function() {
		$(this).removeClass('parent_btn');
	});
	
	$('div.btn_option_layer').each( function() {
		jsTempDivID = $(this).attr('id');
		
		if (jsTempDivID != jsExceptDivID)
			MM_showHideLayers(jsTempDivID, '', 'hide');
	});
}

function js_Clicked_Option_Layer(jsOptionLayerDivID, jsOptionLayerBtnID)
{
	js_Hide_All_Option_Layer(jsOptionLayerDivID); 
	js_ShowHide_Layer(jsOptionLayerDivID);
			
	if ($('div#' + jsOptionLayerDivID).css('visibility') == 'visible')
		$('a#' + jsOptionLayerBtnID).addClass('parent_btn');
	else
		$('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
}

function js_Clicked_Option_Layer_Button(jsOptionLayerDivID, jsOptionLayerBtnID)
{
	MM_showHideLayers(jsOptionLayerDivID, '', 'hide');
	$('a#' + jsOptionLayerBtnID).removeClass('parent_btn');
}

function switchOnlineHelp(m_function,evt){
	
	var slide_speed = 500;
	if($('#online_'+m_function+'_help_board').is(":hidden"))
	{
		$('#online_'+m_function+'_help_btn').onclick =  function() {switchOnlineHelp(m_function);}; 
		if (document.compatMode == 'BackCompat' ){
			$('#online_'+m_function+'_help_board').show(200);
		}
		else{
			$('#online_'+m_function+'_help_board').slideDown(slide_speed);
		}
		
		var pos = findPos(document.getElementById('online_'+m_function+'_help_btn'));
	 	 
  		$('#online_'+m_function+'_help_board').css('left', (pos[0]-60) + "px");
  		$('#online_'+m_function+'_help_board').css('top',  (pos[1]+25) + "px");
	}
	else
	{
		if (document.compatMode == 'BackCompat' ){
			$('#online_'+m_function+'_help_board').hide(200);
		}
		else{
			$('#online_'+m_function+'_help_board').slideUp(slide_speed);
		}
    	$('#online_'+m_function+'_help_btn').onclick =  function() {switchOnlineHelp(m_function);};
	}
}

function AssignCookies()
{
	var jsTempID;
	for(i=0; i<arrCookies.length; i++)
	{
		jsTempID = arrCookies[i].replace('[', '\\[').replace(']', '\\]');
		
		if($('#' + jsTempID).length != 0)
		{
			var jsTempVal = $('#' + jsTempID).val();
			if (jsTempVal == null) {
				jsTempVal = '';
			}
			
			//alert("A: "+$('#'+arrCookies[i]).val());
			//$.cookies.set(arrCookies[i], $('#' + jsTempID).val().toString());
			$.cookies.set(arrCookies[i], jsTempVal.toString());
			//alert(arrCookies[i] + ': ' + $('#'+arrCookies[i]).val());
		}
		else
		{
			//alert("B: "+$.cookies.get(arrCookies[i]));
			$.cookies.set(arrCookies[i], $.cookies.get(arrCookies[i]));
		}
	}
}

function SetCookies()
{
	var jsTempID;
	for(i=0; i<arrCookies.length; i++)
	{
		if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
		{
			jsTempID = arrCookies[i].replace('[', '\\[').replace(']', '\\]');
			$('#' + jsTempID).cookieBind();
		}
	}
}

function Blind_Cookies_To_Object()
{
	SetCookies();
}

function Get_Screen_Width()
{
	var de = document.documentElement;
	var w = window.innerWidth || self.innerWidth || (de&&de.clientWidth) || document.body.clientWidth;
	return w;
}

function Get_Screen_Height()
{
	var de = document.documentElement;
	var h = window.innerHeight || self.innerHeight || (de&&de.clientHeight) || document.body.clientHeight;
	return h;
}

function limitText(limitField, limitNum, checkChinese, chineseCharCount, callBackFunc) {
	checkChinese = checkChinese || 0;
	chineseCharCount = chineseCharCount || 1;
	callBackFunc = callBackFunc || '';
	
	if (checkChinese == 1) {
		var jsText = limitField.value;
		var jsTextLength = jsText.length;
		var jsReturnText = '';
		var i = 0;
		var jsCountVal = 0;
		for (i=0; i<jsTextLength; i++) {
			//var jsThisChar = jsText[i]; <== not work in IE
			var jsThisChar = jsText.charAt(i);
			if (isIncludeChineseChar(jsThisChar)) {	// Chinese characters count as 3 English characters for JUPAS
				jsCountVal += chineseCharCount;
			}
			else {
				jsCountVal += 1;
			}
			if (parseInt(jsCountVal) > parseInt(limitNum)) {
				limitField.value = jsReturnText;
				break;
			}
			else {
				jsReturnText += jsThisChar;
			}
		}
	}
	else {
		if (limitField.value.length > limitNum) {
			limitField.value = limitField.value.substring(0, limitNum);
			
			if (callBackFunc == null || callBackFunc == '') {
				// do nth
			}
			else {
				eval(callBackFunc);
			}
		}
	}
}

function noEsc() {
	return !(window.event && window.event.keyCode == 27);
}


function init_textToSpeach(ignoreEmpty){
	
	var ignoreEmpty = ignoreEmpty || false;
	  var img_loading = IMAGE_PREFIX+"loading_s.gif";
	  
	  if($(".textToSpeech").length > 0){
	    $(".textToSpeech").click(function (){	    	
	      if($(this).attr("src") != img_loading){
	        $(this).attr("src", img_loading);  
	        TTS_Play($(this));
	      }
	    });
	  }
	  
	  if($(".TTS_PlayButton").length > 0){
		  
	    $(".TTS_PlayButton").each(function(){
	    	
	    	var target = $(this).attr("ttsTarget");	    	
	    	if(ignoreEmpty || Trim($("#"+target).text()) != ""){
	    		
		      $(this).html(
		        TTS_PlayButton(
		        $(this).attr("ttsTarget"),
		        $(this).attr("ttsPlayer"),
		        $(this).attr("ttsLang"),
		        $(this).attr("ttsPlayerType"),
		        $(this).attr("ttsButtonFull"),
		        $(this).attr("ttsButtonHighLight"),
		        $(this).attr("ttsButtonSetting"),
		        $(this).attr("ttsExpired")	        
		        )
		      );
	    	}
	    });
	  }
}

function valid_code(val)
{
	var patt1=/^[a-z]/i;
	return val.Trim().match(patt1)?true:false;  
}

function LPAD(num, totalChars, padWith) 
{
	num = num + "";
	padWith = (padWith) ? padWith : "0";
	if (num.length < totalChars) 
	{
		while (num.length < totalChars) 
		{
		num = padWith + num;
		}
	} else {}
	
	if (num.length > totalChars) 
	{ //if padWith was a multiple character string and num was overpadded
		num = num.substring((num.length - totalChars), totalChars);
	} else {}
		
	return num;
}

function reloadResultTable(curPage, numPerPage, sortColumnIndex, sortOrder, submitMode, submitFormID, submitHref, submitFunctionName, submitExtraPara) {
	if (sortColumnIndex == '' || sortColumnIndex == null) {
		sortColumnIndex = $('input#sortColumnIndex').val();
	}
	if (sortOrder == '' || sortOrder == null) {
		sortOrder = $('input#sortOrder').val();
	}
	
	$('input#curPage').val(curPage);
	$('input#numPerPage').val(numPerPage);
	$('input#sortColumnIndex').val(sortColumnIndex);
	$('input#sortOrder').val(sortOrder);
	
	submitMode = parseInt(submitMode);
	switch (submitMode) {
		case 1:
			$('form#' + submitFormID).submit();
			break;
		case 2:
			var href = '';
			href += submitHref;
			href += (submitExtraPara=='')? '?' : submitExtraPara + '&';
			href += 'curPage=' + curPage + '&numPerPage=' + numPerPage + '&sortColumnIndex=' + sortColumnIndex + '&sortOrder=' + sortOrder;
			
			window.location = href; 
			break;
		case 3:
			eval(submitFunctionName + '();');
			break;
	}
}

//function fixSuperTableHeight(jsTableID, jsMaxHeight)
//{
//	var tbodyHeight = $("table#" + jsTableID + " > tbody").height() + $("table#" + jsTableID + " > thead").height() + $("table#" + jsTableID + " > tfoot").height();
//	var adjustTo = tbodyHeight > jsMaxHeight? jsMaxHeight : tbodyHeight + 20;
//	$("table#" + jsTableID + " > div.sData").css("height", adjustTo) ;
//	$("div#" + jsTableID + "_box").css("height", adjustTo + $("table#" + jsTableID + " > div.sHeader").height()) ;
//}
function fixSuperTableHeight(jsTableID, jsMaxHeight)
{
	var tbodyHeight = $("table#" + jsTableID + " > tbody").height() + $("table#" + jsTableID + " > thead").height() + $("table#" + jsTableID + " > tfoot").height();
	var adjustTo = tbodyHeight > jsMaxHeight? jsMaxHeight : tbodyHeight + 20;
	$("table#" + jsTableID + " div.sData").height(adjustTo) ;
	//$("table#" + jsTableID + " > div.sData").height($("table#" + jsTableID + " > div.sFData").height());
	
	$("div#" + jsTableID + "_box div.sBase").height(adjustTo + $("table#" + jsTableID + " div.sHeader").height()) ;
	$("div#" + jsTableID + "_box").height(adjustTo + $("table#" + jsTableID + " div.sHeader").height()) ;
	
	$("div#" + jsTableID + "_box div.sData").height($("div#" + jsTableID + "_box div.sBase").height() - $("div#" + jsTableID + "_box div.sHeader").height());
}

// temporary for iPad use only
function fixSuperTableWidth(jsTableID, jsMaxWidth)
{

	var tbodyWidth = $("table#" + jsTableID + " > tbody").width();
	var adjustTo = tbodyWidth > jsMaxWidth? jsMaxWidth : tbodyWidth + 20;
	adjustTo = adjustTo / 2;
	$("table#" + jsTableID + " > div.sData").css("width", adjustTo) ;
	$("div#" + jsTableID + "_box").css("width", adjustTo + $("table#" + jsTableID + " > div.sHeader").width()) ;
}

function Click_SearchBox(evt, obj)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		obj.submit();
	else
		return false;
}

function remove_selection_option(obj_name, is_all){
	var is_all = is_all || false;
	var obj = document.getElementById(obj_name);
	if(obj.length > 0){
		for(var i=(obj.length-1) ; i>=0 ; i--){
			if(is_all == true){
				obj[i] = null;
			} else {
				if(obj[i].selected == true){
					obj[i] = null;
				}
			}
		}
	}
}

function Remove_All_Selection_Option(SelectObj,SelectValue) {
	var SelectIndex = 0;
	
	for (var j=0; j< SelectObj.length; j++) {
		if (SelectObj.options[j].value == SelectValue) {
			SelectObj.options[j] = null;
		}
	}
}

function js_Disable_Button(jsBtnID)
{
	$('input#' + jsBtnID).attr("disabled", true).addClass("formbutton_disable formbutton_disable_v30").removeClass("formbutton formbutton_v30");
}

function js_Enable_Button(jsBtnID)
{
	$('input#' + jsBtnID).attr("disabled", false).removeClass("formbutton_disable formbutton_disable_v30").addClass("formbutton formbutton_v30");
}

// ref: http://stackoverflow.com/questions/2315488/using-javascript-how-can-i-count-a-mix-of-asian-characters-and-english-words
function js_Get_Word_Count(jsText) {
	
	if (jsText == '' || jsText == null) {
		return 0;
	}
	
	// This matches all CJK ideographs.
	var cjkRegEx = /[\u3400-\u4db5\u4e00-\u9fa5\uf900-\ufa2d]/;
	
	// This matches all characters that "break up" words.
	var wordBreakRegEx = /\W/;
	
	var wordCount = 0;
	var inWord = false;
	var length = jsText.length;
	for (var i = 0; i < length; i++) {
		var curChar = jsText.charAt(i);
		if (cjkRegEx.test(curChar)) {
			// Character is a CJK ideograph.
			// Count it as a word.
			wordCount += inWord ? 2 : 1;
			inWord = false;
		} else if (wordBreakRegEx.test(curChar)) {
			// Character is a "word-breaking" character.
			// If a word was started, increment the word count.
			if (inWord) {
				wordCount += 1;
			}
			inWord = false;
		} else {
			// All other characters are "word" characters.
			// Indicate that a word has begun.
			inWord = true;
		}
	}
	
	// If the text ended while in a word, make sure to count it.
	if (inWord) {
		wordCount += 1;
	}
	
	return wordCount;
}

function js_Get_Character_Count(jsText, jsChineseCount, jsLineBreakCount) {
	jsChineseCount = jsChineseCount || 1;
	jsLineBreakCount = jsLineBreakCount || 1;
	
	var jsCountVal = 0;
	var jsTextLength = jsText.length;
	var i = 0;
	for (i=0; i<jsTextLength; i++) {
		//var jsThisChar = jsText[i]; <== not work in IE
		var jsThisChar = jsText.charAt(i);
		
		if (jsThisChar == "\n") {
			jsCountVal += jsLineBreakCount;
		}
		else if (isIncludeChineseChar(jsThisChar)) {
			jsCountVal += jsChineseCount;
		}
		else {
			jsCountVal += 1;
		}
	}
	
	return jsCountVal;
}

function strip_tags(input, allowed) {
	if (input == '' || input == null) {
		return '';
	}
	
    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
}

function is_positive_int(val)
{
	var value = parseInt(val)
	if (isNaN(val) || val < 0 || value!=val)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function getEditorValue(instanceName) {
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	
	// Get the editor contents as XHTML.
	return oEditor.GetXHTML( true ) ; // "true" means you want it formatted.
}

// reference: 	http://www.kidslovepc.com/javascript/javascript-detect-chinese-input.shtml
//				http://ubuntu-rubyonrails.blogspot.com/2009/06/unicode.html
function isIncludeChineseChar(str) {
	var re1 = new RegExp("^[\u4E00-\u9FFF]*$"); //Chinese character range
	var re2 = new RegExp("^[\uE7C7-\uE7F3]*$"); //Chinese character range
	var re3 = new RegExp("^[\u2014\u2026\u3001\u3002\u300C\u300D\u300E\u300F\uFB00-\uFFFD]*$"); //Chinese character symbol range 
	/*
	 * ����\u2014
	 * ����\u2026
	 * ����\u3001
	 * ����\u3002
	 * ����	\u300C
	 * ����	\u300D
	 * ����	\u300E
	 * ����	\u300F
	 */
	
	var includeChinese = true;
	if (str=='' || (!((re1.test(str) || re3.test(str)) && (!re2.test(str))))) {
		includeChinese = false;
	}
	
	return includeChinese;
}

function isObjectValueEmpty(objId) {
	var isEmpty = false;
	objId = getJQuerySaveId(objId);
	
	switch ($('#' + objId).get(0).tagName.toUpperCase()) {
		case 'INPUT':
			var objectType = $('#' + objId).attr('type').toUpperCase();
			switch (objectType) {
				case 'TEXT':
				case 'PASSWORD':
					if ($('#' + objId).val().Trim() == '') {
						isEmpty = true;
					}
					break;
				case 'RADIO':
				case 'CHECKBOX':
					if (!$('#' + objId).attr('checked')) {
						isEmpty = true;
					}
					break;
			}
			break;
		case 'TEXTAREA':
			if ($('#' + objId).val().Trim() == '') {
				isEmpty = true;
			}
			break;
		case 'SELECT':
			if ($('#' + objId).attr('multiple')) {
				if ($('#' + objId + ' option:selected').length == 0) {
					isEmpty = true;
				}
			}
			else {
				if ($('#' + objId).val().Trim() == '') {
					isEmpty = true;
				}
			}
			break;
	}
	return isEmpty;
}

function getAjaxLoadingMsg(withMsg, custLang) {
	withMsg = withMsg || 1;
	custLang = custLang || '';
	
	var html = '';
	html += '<img src="/images/2009a/indicator.gif">';
	if(withMsg) {
		var msg = (custLang=='')? JSLang['Loading'] : custLang;
		html += ' <span class="tabletextremark">' + msg + '</span>';
	}
	
	return html;
}

function getJQuerySaveId(objId) {
	objId = objId.replace(/\[/g, '\\[').replace(/\]/g, '\\]');
	return objId;
}
function getJQuerySafeId(objId) {
	return getJQuerySaveId(objId);
}


function richTxt_IsCompatibleBrowser()
{
	var sAgent = navigator.userAgent.toLowerCase() ;
	
	if (sAgent.indexOf( 'ipad' )>1)
		return false;

	// Internet Explorer 5.5+
	if ( /*@cc_on!@*/false && sAgent.indexOf("mac") == -1 )
	{
		var sBrowserVersion = navigator.appVersion.match(/MSIE (.\..)/)[1] ;
		return ( sBrowserVersion >= 5.5 ) ;
	}

	// Gecko (Opera 9 tries to behave like Gecko at this point).
	if ( navigator.product == "Gecko" && navigator.productSub >= 20030210 && !( typeof(opera) == 'object' && opera.postError ) )
		return true ;

	// Opera 9.50+
	if ( window.opera && window.opera.version && parseFloat( window.opera.version() ) >= 9.5 )
		return true ;

	// Adobe AIR
	// Checked before Safari because AIR have the WebKit rich text editor
	// features from Safari 3.0.4, but the version reported is 420.
	if ( sAgent.indexOf( ' adobeair/' ) != -1 )
		return ( sAgent.match( / adobeair\/(\d+)/ )[1] >= 1 ) ;	// Build must be at least v1

	// Safari 3+
	if (sAgent.indexOf( ' applewebkit/' ) != -1 )
		return ( sAgent.match( / applewebkit\/(\d+)/ )[1] >= 522 ) ;	// Build must be at least 522 (v3)

	return false ;

}

// for keyup ajax checking, delay sending ajax request in order to reduce number of request sent. 
var js_Delay_Action_Timer;
function js_Delay_Action(jsAction, jsDelay)
{
	var delay = jsDelay || 250;
	clearTimeout(js_Delay_Action_Timer);
   	js_Delay_Action_Timer=setTimeout(jsAction,delay);
}

/*
 * text 			: thickbox top bar message
 * url  			: the link would pass to
 * extraparams 		: contains some get data format as 'sid=13&vid=34'
 * isUse			: turn on the dynamic dimension setting or thickbox or not
 * defaultHeight 	: define the default height of thickbox if not use or define
 * defaultWidth 	: define the default width of thickbox if not use or define
 * tbType 			: define the type of thickbox : 1-iframe, 2-inline, ...
 */
function show_dyn_thickbox_ip(text, url, extraparams, isUse, defaultHeight, defaultWidth, tbType, inlineID, callBackFunc){
	extraparams = extraparams || '';
	inlineID = inlineID || 'fakeDiv';
	isUse = isUse || 0;
	defaultWidth = defaultWidth || 690;
	defaultHeight = defaultHeight || 470;
	tbType = tbType || 1;		// 1-iframe ; 2-inline
	var cust_wh = (isUse == 1) ? 1 : 0;
	callBackFunc = callBackFunc || '';
	
	get_dyn_thickbox_dimension_ip();
	
	// handle any extraparams is already attached with the url
	if(url.indexOf("?") != -1){
		var tmp = url.split("?");
		if(tmp.length > 1){
			url = tmp[0];
			if(tmp[1] != ''){
				extraparams = tmp[1] + '&' + extraparams;
			}
		}
	}
	
	var tbparams = (tbType == 1) ? '&TB_iframe=true' : '';
	var tb_h = (cust_wh) ? ip_dyn_tb_h : defaultHeight;
	var tb_w = (cust_wh) ? ip_dyn_tb_w : defaultWidth;
	var params = 'tb_h='+tb_h+'&tb_w='+tb_w;
	params += (extraparams != '') ? '&'+extraparams : '';
	
	var tb_str = (tbType == 2) ? '#TB_inline' : url; 
	tb_str += '?' + params + tbparams + '&amp;height='+tb_h+'&amp;width='+tb_w;
	tb_str += (tbType == 2) ? '&amp;inlineId=' + inlineID : '';
	
	tb_show(text, tb_str);
	if (callBackFunc == null || callBackFunc == '') {
		// do nth
	}
	else {
		eval(callBackFunc);
	}
}

function get_dyn_thickbox_dimension_ip(){
	ip_dyn_tb_w = ip_dyn_tb_w || '';
	ip_dyn_tb_h = ip_dyn_tb_h || '';
	
	if(document.all){
		var h = document.documentElement.clientHeight;
		var w = document.documentElement.clientWidth;
	} else {
		var h = window.innerHeight;
		var w = window.innerWidth;
	}
	
	ip_dyn_tb_h = (parseInt(h) > parseInt(250)) ? parseInt(h) - parseInt(100) : '';
	ip_dyn_tb_w = (parseInt(w) > parseInt(250)) ? parseInt(w) - parseInt(100) : '';
}

function load_dyn_size_thickbox_ip(title, callBackFunc, inlineID, defaultHeight, defaultWidth, extraparams, tbType, url) {
	callBackFunc = callBackFunc || '';
	inlineID = inlineID || 'fakeDiv';
	defaultHeight = defaultHeight || '';
	defaultWidth = defaultWidth || '';
	extraparams = extraparams || '';
	tbType = tbType || 2;
	url = url || '';
	
	var useDynSize = false;
	if (defaultHeight == '' && defaultWidth == '') {
		useDynSize = true;
	}
	
	show_dyn_thickbox_ip(title, url, extraparams, useDynSize, defaultHeight, defaultWidth, tbType, inlineID, callBackFunc);
}

function adjust_dyn_size_thickbox_content_height_ip(containerDivId, contentDivId, bottomDivId) {
	var thickboxHeightAjax = $('div#TB_ajaxContent').height();
	var thickboxHeightIFrame = $('iframe#TB_iframeContent').height();
	var thickboxHeight;
	
	if (thickboxHeightAjax) {
		thickboxHeight = thickboxHeightAjax;
	}
	else {
		thickboxHeight = thickboxHeightIFrame;
	}
	
	var bottomDivHeight = $('div#' + bottomDivId).height();
	var contentDivHeight = thickboxHeight - bottomDivHeight - 30;
	
	
	$('div#' + contentDivId).height(contentDivHeight);
	$('div#' + containerDivId).height(thickboxHeight);
}

function str_pad(number, length) {
	var str = '' + number;
	while (str.length < length) {
		str = '0' + str;
	}
	return str;
}

function zeroPad(num,count)
{
	var numZeropad = num + '';
	while(numZeropad.length < count) {
		numZeropad = "0" + numZeropad;
	}
	return numZeropad;
}

function saveFormValues() {
	$('form#form1 input:text, form#form1 select').each( function() {
		if ($(this).attr('multiple')) {
			var _elementId = $(this).attr('id');
			$('select#' + _elementId + ' option').attr('selected', 'selected');
		}
		$('body').data($(this).attr('id'), $(this).val());
		if ($(this).attr('multiple')) {
			var _elementId = $(this).attr('id');
			$('select#' + _elementId + ' option').attr('selected', '');
		}
	});
	$('form#form1 input:radio, form#form1 input:checkbox').each( function() {
		$('body').data($(this).attr('id'), $(this).attr('checked'));
	});
	$('form#form1 textarea').each( function() {
		$('body').data($(this).attr('id'), $(this).html());
	});
}

function loadFormValues() {
	$('form#form1 input:text, form#form1 select').each( function() {
		$(this).val($('body').data($(this).attr('id')));
	});
	$('form#form1 input:radio, form#form1 input:checkbox').each( function() {
		$(this).attr('checked', $('body').data($(this).attr('id')));
	});
	$('form#form1 textarea').each( function() {
		$(this).html($('body').data($(this).attr('id')));
	});
}


// should call saveFormValues() in initiation to record the form values first before calling this function
function isFormChanged() {
	var isChanged = false;
	
	$('form#form1 input:text, form#form1 select').each( function() {
		var initialValue = $('body').data($(this).attr('id'));
		if (initialValue != null && initialValue != '') {
			initialValue = initialValue.toString();
		}
		
		
		if ($(this).attr('multiple')) {
			var _elementId = $(this).attr('id');
			$('select#' + _elementId + ' option').attr('selected', 'selected');
		}
		var curValue = $(this).val();
		if (curValue != null && curValue != '') {
			curValue = curValue.toString();
		}
		if ($(this).attr('multiple')) {
			var _elementId = $(this).attr('id');
			$('select#' + _elementId + ' option').attr('selected', '');
		}
		
		if (initialValue != curValue) {
			isChanged = true;
			return false;	// break
		}
	});
	
	if (!isChanged) {
		$('form#form1 input:radio, form#form1 input:checkbox').each( function() {
			var initialValue = $('body').data($(this).attr('id'));
			if (initialValue != $(this).attr('checked')) {
				isChanged = true;
				return false;	// break
			}
		});
	}
	
	if (!isChanged) {
		$('form#form1 textarea').each( function() {
			var initialValue = $('body').data($(this).attr('id'));
			if (initialValue != $(this).html()) {
				isChanged = true;
				return false;	// break
			}
		});
	}
	
	return isChanged;
}

// return false if from date is greater than to date
// Parse date must be in one of these format: yyyy/mm/dd, yyyy-mm-dd, yyyy.mm.dd,yyyymmdd
function compare_date(fromDateObj, toDateObj, msg) {
	if (fromDateObj.value > toDateObj.value) {
		alert(msg);
		fromDateObj.focus();
		return false;
	}
	else {		
		return true;
	}	
}

function initInsertAtTextarea() {
	jQuery.fn.extend({
		insertAtCaret: function(myValue){
		  return this.each(function(i) {
		    if (document.selection) {
		      //For browsers like Internet Explorer
		      this.focus();
		      var sel = document.selection.createRange();
		      sel.text = myValue;
		      this.focus();
		    }
		    else if (this.selectionStart || this.selectionStart == '0') {
		      //For browsers like Firefox and Webkit based
		      var startPos = this.selectionStart;
		      var endPos = this.selectionEnd;
		      var scrollTop = this.scrollTop;
		      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
		      this.focus();
		      this.selectionStart = startPos + myValue.length;
		      this.selectionEnd = startPos + myValue.length;
		      this.scrollTop = scrollTop;
		    } else {
		      this.value += myValue;
		      this.focus();
		    }
		  });
		}
	});
}

// focus function
function focusNext(from, to, len){
	if (from.value.length==len) {
		to.focus();
	}
}

/*
 * Validate IPv4 address, allow
 * 1) Exact IP Address (e.g. 192.168.0.101)
 * 2) IP Address Range (e.g. 192.168.0.[10-100])
 * 3) CISCO Style Subnet Range (e.g. 192.168.0.0/24)
 */
function validateIPAddress(ip)
{
	var val = ip.toString();
	var regex = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3}|\[\d{1,3}\-\d{1,3}\]|\d{1,3}\/\d{1,3})$/;
	var valid = true;
	
	valid = regex.test(ip);
	if(valid){
		var ip_parts = val.split('.');
		var part0 = parseInt(ip_parts[0]);
		var part1 = parseInt(ip_parts[1]);
		var part2 = parseInt(ip_parts[2]);
		var part3 = parseInt(ip_parts[3]);
		
		if(part0 < 0 || part0 > 255) valid = false;
		if(part1 < 0 || part1 > 255) valid = false;
		if(part2 < 0 || part2 > 255) valid = false;
		if(ip_parts[3].indexOf('/') != -1){
			var part3_parts = ip_parts[3].split('/');
			if(part3_parts.length == 2){
				var subnetmask = parseInt(part3_parts[1]);
				if(subnetmask < 1 || subnetmask > 32) valid = false;
				var subnet = part3_parts[0];
				if(subnet < 0 || subnet > 255) valid = false;
			}else{
				valid = false;
			}
		}else if(ip_parts[3].indexOf('-') != -1 && ip_parts[3].indexOf('[') != -1 && ip_parts[3].indexOf(']') != -1){
			var part3_parts = ip_parts[3].replace('[','').replace(']','').split('-');
			if(part3_parts.length == 2){
				var from_range = parseInt(part3_parts[0]);
				var to_range = parseInt(part3_parts[1]);
				if(from_range >= to_range || from_range < 0 || from_range > 255 || to_range < 0 || to_range > 255){
					valid = false;
				}
			}else{
				valid = false;
			}
		}else if(part3 < 0 || part3 > 255){
			valid = false;
		}
	}
	
	return valid;
}

function resizeTextArea(obj) {
	obj.style.height = 'auto';
	obj.style.height = obj.scrollHeight + 'px';
}	

function appendRow2TextArea(jTarget, new_val, jMenu) {
	var org_val = document.getElementById(jTarget).value;
	if (org_val.indexOf(new_val) == -1) {
		org_val = org_val + ((org_val != '') ? '\n' : '') + new_val;
		document.getElementById(jTarget).value = org_val; 
		resizeTextArea(document.getElementById(jTarget));
	} 
	document.getElementById(jMenu).style.display= 'none';
	setDivVisible(false, jMenu, 'lyrShim');
}

function CheckPasswordCriteria(password,userlogin,minlength)
{
	var banned_chars = ['"','\'', ' ', '$', '&', '<', '>', '+', '\\'];
	var bad_passwords = [userlogin,"123","abc",
						"123456","password","12345678","1234","pussy","12345","dragon","qwerty","696969","mustang",
						"letmein","baseball","master","michael","football","shadow","monkey","abc123","pass","fuckme",
						"6969","jordan","harley","ranger","iwantu","jennifer","hunter","fuck","2000","test",
						"batman","trustno1","thomas","tigger","robert","access","love","buster","1234567","soccer",
						"hockey","killer","george","sexy","andrew","charlie","superman","asshole","fuckyou","dallas",
						"jessica","panties","pepper","1111","austin","william","daniel","golfer","summer","heather",
						"hammer","yankees","joshua","maggie","biteme","enter","ashley","thunder","cowboy","silver","richard",
						"fucker","orange","merlin","michelle","corvette","bigdog","cheese","matthew","121212","patrick",
						"martin","freedom","ginger","blowjob","nicole","sparky","yellow","camaro","secret","dick","falcon",
						"taylor","111111","131313","123123","bitch","hello","scooter","please",
						"porsche","guitar","chelsea","black","diamond","nascar","jackson","cameron","654321","computer",
						"amanda","wizard","xxxxxxxx","money","phoenix","mickey","bailey","knight","iceman","tigers",
						"purple","andrea","horny","dakota","aaaaaa","player","sunshine","morgan","starwars","boomer",
						"cowboys","edward","charles","girls","booboo","coffee","xxxxxx","bulldog","ncc1701","rabbit",
						"peanut","john","johnny","gandalf","spanky","winter","brandy","compaq","carlos","tennis","james",
						"mike","brandon","fender","anthony","blowme","ferrari","cookie","chicken","maverick","chicago",
						"joseph","diablo","sexsex","hardcore","666666","willie","welcome","chris","panther","yamaha",
						"justin","banana","driver","marine","angels","fishing","david","maddog","hooters","wilson",
						"butthead","dennis","fucking","captain","bigdick","chester","smokey","xavier","steven","viking",
						"snoopy","blue","eagles","winner","samantha","house","miller","flower","jack",
						"firebird","butter","united","turtle","steelers","tiffany","zxcvbn","tomcat","golf","bond007",
						"bear","tiger","doctor","gateway","gators","angel","junior","thx1138","porno","badboy","debbie",
						"spider","melissa","booger","1212","flyers","fish","porn","matrix","teens","scooby","jason","walter",
						"cumshot","boston","braves","yankee","lover","barney","victor","tucker","princess","mercedes",
						"5150","doggie","zzzzzz","gunner","horney","bubba","2112","fred","johnson","xxxxx","tits","member",
						"boobs","donald","bigdaddy","bronco","penis","voyager","rangers","birdie","trouble","white","topgun",
						"bigtits","bitches","green","super","qazwsx","magic","lakers","rachel","slayer","scott","2222","asdf",
						"video","london","7777","marlboro","srinivas","internet","action","carter","jasper","monster","teresa",
						"jeremy","11111111","bill","crystal","peter","pussies","cock","beer","rocket","theman","oliver",
						"prince","beach","amateur","7777777","muffin","redsox","star","testing","shannon","murphy","frank",
						"hannah","dave","eagle1","11111","mother","nathan","raiders","steve","forever","angela","viper","ou812",
						"jake","lovers","suckit","gregory","buddy","whatever","young","nicholas","lucky","helpme","jackie","monica",
						"midnight","college","baby","cunt","brian","mark","startrek","sierra","leather","232323","4444","beavis",
						"bigcock","happy","sophie","ladies","naughty","giants","booty","blonde","fucked","golden","0","fire",
						"sandra","pookie","packers","einstein","dolphins","0","chevy","winston","warrior","sammy","slut","8675309",
						"zxcvbnm","nipples","power","victoria","asdfgh","vagina","toyota","travis","hotdog","paris","rock","xxxx",
						"extreme","redskins","erotic","dirty","ford","freddy","arsenal","access14","wolf","nipple","iloveyou",
						"alex","florida","eric","legend","movie","success",
						"rosebud","jaguar","great","cool","cooper","1313","scorpio","mountain","madison","987654","brazil",
						"lauren","japan","naked","squirt","stars","apple","alexis","aaaa","bonnie","peaches","jasmine","kevin",
						"matt","qwertyui","danielle","beaver","4321","4128","runner","swimming","dolphin","gordon","casper","stupid",
						"shit","saturn","gemini","apples","august","3333","canada","blazer","cumming","hunting","kitty","rainbow",
						"112233","arthur","cream","calvin","shaved","surfer","samson","kelly","paul","mine","king","racing","5555",
						"eagle","hentai","newyork","little","redwings","smith","sticky","cocacola","animal","broncos","private","skippy",
						"marvin","blondes","enjoy","girl","apollo","parker","qwert","time","sydney","women","voodoo","magnum","juice",
						"abgrtyu","777777","dreams","maxwell","music","rush2112","russia","scorpion","rebecca","tester","mistress",
						"phantom","billy","6666","albert"];
						
	var result = [];
	
	if(password.length < minlength){
		result.push(-1); // Password does not fulfil minimum required length.
	}
	
	for(var i=0;i<banned_chars.length;i++){
		if(password.indexOf(banned_chars[i]) != -1){
			result.push(-2); // Password contains one or more of the restricted characters " ' space $ & < > + \.
		}
	}
	
	if(bad_passwords.indexOf(password) != -1){
		result.push(-3); // Password is too simple.
	}
	
	if(password.match(/.*\d.*/g) == null || password.match(/.*[a-zA-Z.*]/g) == null){
		result.push(-4); // Password should contains both alphabets and digits.
	}
	
	if(result.length == 0){
		result.push(1); // Valid and OK
	}
	
	return result;
}

function convertOverflowNumber(number, digitlimit){
	 var maxNum = Math.pow(10, digitlimit) -1;
	 var convertedNumber;
	    if(number > maxNum) 
	    { 
	    	convertedNumber = maxNum +'+';
	    } else {
	    	convertedNumber = number;
	    }
	    return convertedNumber;
}
