<?
## Using By : thomas

## IMPORTANT IMPORTANT IMPORTANT:
##			If you add a new menu, please go to "lighten current menu (for cached method)" to add your newly added menu to lighten the parent menu.
##-------------------------------------------------------------------------------------------------------------------------------------------------

/********************** Change Log ***********************/
#       Date    :       2012-11-06 [Mick]
#                               added $home_header_no_EmulateIE7 to cancel meta emulateIE7
#                               also $home_header_no_EmulateIE7 will cancel the caching of header
#	Date	: 	2012-10-30 [Jason]
#				improved JS toScrabble() to pass user_type via Post method
#
#	Date 	: 	2012-10-08 [Rita]
#				added Document Routing menu
#
# 	Date	: 	2012-09-20 [Yuen]
#				added eLib + (should replace eBook when it is completed)
#
# 	Date	: 	2012-09-06 [YatWoon]  >>>> should be fixed on 2011-06-21 [Henry Chow]
#				display "eHomework" in menu when current user is eHomework Admin, even eHomework module is "disabled"
#
#	Date	: 	2012-09-04 [Ivan]
#				modified the link of "School Settings > Group" to add param "?clearCoo=1"
#
#	Date	:	2012-09-03 [YatWoon]
#				comment out those "/templates/jquery/plupload/" js setting
#
#	Date	:	2012-08-31 [Ivan]
#				modified meta: enabled back <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> for parent account mgmt step 2 yahoo ajax
#
#	Date	:	2012-08-31 [YatWoon]
#				add eService>eNotice parent checking (if no child, hide the menu)
#
#	Date	:	2012-08-30 [Carlos]
#				added "eService > Staff Attendance System"
#
#	Date	:	2012-08-14 [Ivan] (2012-0814-1021-26073)
#				fixed system did not cater the case of student as an eEnrol admin
#
#	Date	:	2012-08-14 [Carlos]
#				added "eService > Class Diary"
#				added "eAdmin > Student Management > Class Diary"
#
#	Date	:	2012-07-14 [Jason]
#				added "eLearning > Wong Wha San eLearning Project"
#				added PageMenuKey 'WWSeLearningProject' for $CurrentPageArr
#
#	Date	:	2012-06-19 [Henry Chow]
#				added "eLearning > SBA"
#
#	Date	:	2012-05-21 [Ivan] [2012-0516-1551-30066]
#				Improved: hide "eService > eBooking" if the user has no right for booking
#
#	Date	:	2012-05-18 [YatWoon]
#				Improved: add checking for China client, hidden "support ?"
#
#	Date	:	2012-04-17 [YatWoon]
#				Fixed: failed to display "General Mgmt" if user only got "Message Center" [Case#2012-0417-1518-17066]
#
#	Date	:	2012-03-29 [Carlos]
#				Illiminate webmail connection if using imail gamma because new mail will be counted by ajax
#
#	Date	:	2012-03-19 [Yuen]
#				Update access time once after user cancells the logout prompt
#
#	Date	:	2012-02-20 [fai]
#				Update chuenyuen access right , relate to sys_custom['chuen_yuen_award_scheme']
#
#	Date	:	2012-02-17 [YatWoon]
#				Improved: eBook & Reading scheme, only allow student and staff access only
#
#	Date	:	2012-02-10 [YatWoon]
#				update ehomework access checking
#
#	Date	:	2012-02-09 [YatWoon]
#				edit the eCommunity link to redirect.php
#
#	Date	:	2012-02-09 [Ivan]
#				only staff and student accounts are accessible to "eLearning > Writing" now
#
#	Date	:	2011-11-30 [Yuen]
#				$plugin['DisablePoll'] = true; #Disable Poll on request
#				$plugin['DisableSurvey'] = true; #Disable eSurvey on request
#				$plugin['DisableTimeTable'] = true; #Disable TimeTable of eService on request
#				$plugin['DisableiCalendar'] = true; #Disable iCalendar on request
#				$plugin['DisableeCommunity'] = true; #Disable eCommunity on request
#
#	Date	:	2011-11-24 [Ivan]
#				added "eLearning > Writing"
#
#	Date	:	2011-11-23 [Henry Chow]
#				hide "eLearning > Subject References"
#
#	Date	:	2011-11-16 [YatWoon]
#				Add a config for TimeoutWarningSecond (default 120s) [Case#2011-1111-1405-16066]
#
#	Date	:	2011-11-04 [YatWoon]
#				remove ( && $_SESSION['UserType']!=USERTYPE_ALUMNI) checking for iMail
#
#	Date	:	2011-11-01 [Kelvin]
#				added "KSK" to menu
#
#	Date	:	2011-10-31 [Henry Chow]
#				modified "eService" > "Digital Archive" to "Subject Reference"
#
#	Date	:	2011-10-07 [Henry Chow]
#				modified "Digital Archive" link, default open "Admin Doc" for staff
#
#	Date	:	2011-09-28 [Ivan]
#				modified "JUPAS Ready" icon show for HK client only
#
#	Date	:	2011-09-28 [Yuen]
#				added eLC modules (ELP, SSR, ReadingRoom & SFC) to system menu and changed eLC to full title - eClass Learning Centre
#
#	Date	:	2011-09-26 [Fai]
#				added $customizationModuleMgmtIn_eAdmin, $customizationModuleMgmtIn_eAdmin_StudentMgmt to control customization module display in top menu bar
#
#	Date	:	2011-09-26 [Ivan]
#				added "JUPAS Ready" icon until 2012-03-01
#
#	Date	:	2011-09-17 [YatWoon]
#				add alumni user access right checking
#
#	Date	:	2011-09-08 [YatWoon]
#				add eEnrolment (lite version) checking
#
#	Date	:	2011-09-06 [Henry Chow]
#				allow "Viewer Group" member of Invoice Mgmt System can see the option in menu
#
#	Date	:	2011-08-23 [YaTWoon]
#				Add alumni user checking ( $_SESSION['UserType']==USERTYPE_ALUMNI  <=== alumni)
#				Hidden top menu for alumni user
#				change school logo link ($school_logo_link)
#
# 	Date	: 	2011-08-18 [Marcus]
#				Added Header Menu eAdmin > General Management > Message Center
#
# 	Date	: 	2011-07-08 [Henry Chow]
#				eHomework Admin not allow to view "eService > eHomework"
#
# 	Date	: 	2011-06-24 [Yuen]
#				allow to disable caching by setting $special_feature['CacheDisabled'] to true in settings.php
#
# 	Date	: 	2011-06-21 [Henry Chow]
#				display "eHomework" in menu when current user is eHomework Admin, even eHomework module is "disabled"
#
# 	Date	: 	2011-06-10 [Henry Chow]
#				update lightening of "eAdmin > Resources Management"
#
# 	Date	: 	2011-06-08 [Yuen]
#				Handled the update of iMail new message in cache mode
#
#	Date	:	2011-06-03 [Henry Chow]
#				modified display of "eAdmin > eHomework" , consider the parameter of $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]
#
#	Date	:	2011-06-02 [Henry Chow]
#				split the entrance for student & teacher of Digital Archive
#
#	Date	:	2011-05-31 [Thomas]
#				Added teh menu of eLearning > iTextbook
#
# 	Date	: 	2011-05-17 [Yuen]
#				Introduce caching (Cache Lite) to save time for better performance (now, 0.11s is saved!)
#
# 	Date	: 	2011-04-20 [Henry Chow]
#				Display of "Digital Archive" (only check $plugin['digital_archive'] at this moment)
#
# 	Date	: 	2011-04-11 [Henry Chow]
#				checking on $plugin['eDisciplinev12'] is the MUST while determine display of "eAdmin > eDiscipline"
#
# 	Date	: 	2011-04-08 [Carlos]
#				removed $header_onload_js from <body> onLoad event, moved it to be executed in /home/index.php $(document).ready()
#
#	Date	:	2011-03-23 [Yuen]
#				disabled to call returnNumNewMessage() for counting new mails if iMail Gamma/Plus is active
#
#	Date	:	2011-03-21 [Henry Chow]
#				revised checking on "Offical Photo" access right from $_SESSION
#
#	Date	:	2011-03-17 [Yuen]
#				added the menu of Digital Archive
#
#	Date	:	2011-03-17 [Yuen]
#				relocated eLibrary to "eLibrary -> eBook", Reading Scheme to "eLibrary -> Reading Scheme"
#
#	Date	:	2011-03-10 [Henry Chow]
#				allow System Admin to view "School News"
#
#	Date	:	2011-03-10 [ Yuen]
#				add Video Tutorials to ?
#
#	Date	:	2011-03-02 [Fai]
#				Add a checking , if it is a standalone iPorfolio , home/index page ($indexPage) should change to iPortfolio index page
#
#	Date	:	2011-02-24 [ YatWoon]
#				add $special_feature['portal_new_features_bubble'] to on/off the new features bubble
#
#	Date	:	2011-02-14 [Henry Chow]
#				change javascript function "loadContent" in order to display the layer according to window width (handle the problem of resize window)
#
#	Date	:	2011-02-11 [Henry Chow]
#				Split "Account Mgmt" right into 3 different rights (Parent, Staff & Student)
#
#	Date	:	2011-02-10 [Henry Chow]
#				Changed: add "New Features" on top right corner in order to display bubble of "Update Message"
#
#	Date	:	2011-01-25	Ivan
#				Changed: add Admin Group logic for "eAdmin > Studen Mgmt > eReportCard (Rubrics)" menu
#
#	Date	:	2011-01-20	Kenneth Chung
#				Changed: add style for class "print_hide"
#
#	Date	:	2011-01-05	YatWoon
#				Improved: "System admin" can access Account Management as well
#
#	Date	:	2010-12-30	Marcus
#				added: eLearning > Reading Scheme
#
#	Date	:	2010-12-21	YatWoon
#				Fixed: eAdmin > StudentMgmt > eHomework access right checking
#
#	Date	: 	2010-12-10 [Ivan]
#				add eAdmin > Student Management > eReportCard (Rubrics), added Class Teacher and Subject Teacher access right
#
#	Date	: 	2010-12-08 [Carlos]
#				add eAdmin > Account Management > SharedmailBox
#
#	Date	:	2010-11-29 [FAI]
#				update standalone iPortfolio , if login with hardcode login "ab_iportfolio_cd" and password "abc" , then the standalone iportfolio will be turned on
#
#	Date	:	2010-11-05 [Yuen]
#				update the layout of ? menu and show Help and Documentations
#
#	Date	:	2010-11-02 [Ronald]
#				add one more flag - $plugin['DisableResourcesBookingOnRequest'] to control the resource booking
#
#	Date	:	2010-10-19 [Henry Chow]
#				Fixed the "eAdmin > Repair System", admin can view Repair System even not in mgmt group
#
#	Date	:	2010-10-14 [Ivan]
#				Fixed the eService > eEnrollment permission for Attendance Helper who is a Staff
#
#	Date	:	2010-09-20 [Ivan]
#				Added eRC1.0 to the eAdmin > StudentMgmt menu
#
#	Date	:	2010-09-20 [Ivan]
#				Added eEnrolment Trial Period Logic
#
#	Date	:	2010-09-02 [Carlos]
#				Get iMail Gamma identity access right from lib
#
# 	Date 	: 	2010-08-25 [Sandy]
#				Power tool icon
#
#	Date	:	2010-08-19 [Carlos]
#				Add checking on identity and access right to control showing iMail Gamma icon
#
#	Date	:	2010-08-13 [Carlos]
#				change checking flags of iMail and iMail Gamma to show only one mail icon
#
#	Date	:	2010-08-12 [Ivan]
#				For Timetable link, added "index.php?clearCoo=1" to clear the cookies when first entering the page
#
#	Date	:	2010-08-12 [YatWoon]
#				comment "ajaxMsgBlock2" to hide the red box in the "?"
#				update $eClassVersion, if the version with sub-version number (e.g. ip.2.5.1.8.1.3), then only display ip.2.5.1.8.1
#
#	Date	:	2010-07-16 [Henry Chow]
#	Detail	:	add access right checking on display of "eAdmin > Account Mgmt > Student Registry"
#
#	Date	:	2010-07-13 [Henry Chow]
#	Detail	:	add the checking on "eAdmin > Account Mgmt > Student Registry"
#
#	Date	:	2010-07-08 [YAtWoon]
#				add $special_option['hide_lang_selectioin'] checking for hide lang selection
#
#	Date	:	2010-07-07 [Yuen]
#	Detail	:	support product version change log of different regions
#               e.g. setting.php : $intranet_version_url = "tw-version.eclass.com.hk";
#
#	Date	:	2010-06-25 [Henry Chow]
#	Detail	:	add the checking on "eLearning > LSIES" (check both $plugin & access right)
#
#	Date	:	2010-06-22 [Henry Chow]
#	Detail	:	change the checking of access to "eService>eDiscipline" using $_SESSION
#
#	Date	:	2010-06-21 [Ivan]
#	Detail	:	add flag to show module eReportCard (Rubrics)
#
#	Date	:	2010-06-10 [Marcus Leung]
#	Detail	:	add flag to hide imail
#
#	Date	:	2010-06-10 [Henry Chow]
#	Detail	:	Staff (Teacher) also can see "eService>eDiscipline" to view his/her own class (as class teacher or subject teacher)
#
#	Date	:	2010-06-09 [Eric]
#	Detail	:	add "Chuen Yuen Award Scheme" eService -> Chuen Yuen Award Scheme / eAdmin -> StudentManagement -> Chuen Yuen Award Scheme
#
#	Date	:	2010-06-04 [Ivan]
#	Detail	:	check admin access right in function UPDATE_CONTROL_VARIABLE() in lib.php for eEnrol and eRC now
#
#	Date	:	2010-05-20 [Henry Chow]
#	Detail	:	add access right checking on display of  "eAdmin > Account Management"
#
#	Date	:	2010-05-13 [Kenneth Chung]
#	Detail	:	include js brwsniff.js also
#
#	Date	:	2010-04-27 [Henry]
#	Detail	:	modified "eAdmin > Account Management" link
#
#	Date	:	2010-04-15 [Ivan]
#	Detail	:	modified "eService > eBooking" link
#
#	Date	:	2010-04-15 [Henry]
#	Detail	:	modified "Repair System" display checking
#
#	Date	:	2010-04-07 [YatWoon]
#	Detail	:	update session timeout layout
#
#	Date	:	2010-03-30 [YatWoon]
#	Detail	:	Add "Repair System" menu eAdmin>ResourceMgmt>Repair System
#
#	Date	:	2010-03-23 [Fai]
#	Detail	:	IP25 Support with standalone iPortfolio $stand_alone['iPortfolio']
#
# 	Date	:	2010-02-25 [Yuen]
#	Details	:	complete function of the timeout control using AJAX and JS (UI to be revised later)
#
# 	Date	:	2010-02-19 [YatWoon]
#	Details	:	add $intranet_default_lang = $_SESSION['intranet_default_lang'];
#
# 	Date	:	2009-12-15 [YatWoon]
#	Details	:	Apply top menu access checking for School Settings
#
# 	Date	:	2009-12-10 [Yuen]
#	Details	:	improve webmail checking logic to avoid checking if once fails to login to email server
#               (avoid slow performance)
#
# 	Date	:	2009-12-05 [Yuen]
#	Details	:	testing the timeout control using AJAX and JS (to be continued... for UI)
#
/******************* End Of Change Log *******************/

// $PATH_WRT_ROOT = "../../../";
# check the page can access or not

if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
	$cur_page = $_SERVER["REQUEST_URI"];
	$AllowAccess = array();
	$AllowAccess[] = "/home/eCommunity/";
	$AllowAccess[] = "/home/iaccount/account/";
	$AllowAccess[] = "/home/iaccount/login_record/";
	$AllowAccess[] = "/home/imail/";
	$AllowAccess[] = "/home/imail_gamma/";

	$hasAssessRight = 0;
	foreach($AllowAccess as $k=>$d)
	{
		$pos = strpos($cur_page, $d);
		if ($pos === false) continue;
		$hasAssessRight = 1;
	}
	if(!$hasAssessRight)	header("Location: /" . $_SESSION['AlumniURL']);

}

$IsHeaderCaching = ($special_feature['CacheDisabled']) ? false : true ;
$IsHeaderCaching = $IsHeaderCaching && !$home_header_no_EmulateIE7; // if cancel EmulateIE7 in meta, disable cache

if ($CurrentPageArr['LIBMS'])
{
	$CurrentPageArr['eLibPlus'] = true;

}

if (is_file("$intranet_root/templates/2020a/layout/home_header.customized.php"))
{
   include_once ("$intranet_root/templates/2020a/layout/home_header.customized.php");

}
else
{

	//$iNewCampusMail = "";
	//unset($iNewCampusMail);

	include_once($PATH_WRT_ROOT."includes/cache_lite/Lite.php");

	/* Set a few options */
	$cached_hh_id = "U".$_SESSION['UserID']."T".$_SESSION['UserType']."P".$_SESSION['eclass_session_password']."L".strlen($intranet_root);


	$options = array('cacheDir' => '/tmp/', 'lifeTime'=>7200);
	$Cache_Lite = new Cache_Lite($options);
	$Cache_Lite->clean();
	/* Test if there is a valid cache-entry for this key */
	if ($IsHeaderCaching && $cached_hh_data = $Cache_Lite->get($cached_hh_id)) {

		/* Cache hit! We've got the cached content stored in $data! */
		//debug("using Cache Lite :: loaded from cached with ID=".$cached_hh_id);
		# reset/clear the cached (for testing only!)
		//$Cache_Lite->remove($cached_hh_id);

	} else {

		/* Cache miss! Use ob_start to catch all the output that comes next*/
		ob_start();

	$PATH_WRT_ROOT_ABS = "/";



#############################################
#####  home_header main codes
#############################################
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");



if (is_file($PATH_WRT_ROOT."includes/version.php"))
{
	/*
	$JustWantVersionData = true;
	include_once($PATH_WRT_ROOT."includes/version.php");

	$eClassVersion = $versions[0][0];
	*/
	$eClassVersion = Get_IP_Version();

	$pos = strrpos($eClassVersion, ".");
	if($pos>10)	$eClassVersion = substr($eClassVersion, 0, $pos);

} else
{
	$eClassVersion = "--";
}
global $linterface, $special_feature, $module_version,$stand_alone, $home_header_no_EmulateIE7;

if (!isset($linterface))
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html("");
}
$linterface->IsHeaderCaching = $IsHeaderCaching;
if (!isset($lu))
{
	$lu = new libuser($UserID);
}

# protect for eClass logo!
/* Yuen: not yet effective
$eClassLogoData = md5(get_file_content($PATH_WRT_ROOT."images/2020a/logo_eclass_footer.gif"));
if ($eClassLogoData!="980442a2a7b4cb596f011830d5afd907")
{
	die("System cannot run due to a very import file for eClass logo is modified illegally!");
}
*/


$ControlTimeOutByJS = $special_feature['TimeOutByJS'];

//$ControlTimeOutByJS = true;
if ($ControlTimeOutByJS || $special_feature['TimeOutByJS'])
{
	$TimeOutWarningSecond = $special_feature['TimeOutWarningSecond'] ? $special_feature['TimeOutWarningSecond'] : 120;
	# session max life time in second
	$SessionLifeTime = ini_get('session.gc_maxlifetime') - $TimeOutWarningSecond;
	# below is just for quick testing
	// $SessionLifeTime = 2;
}


# Basic Information
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
// $intranet_default_lang = $_SESSION['intranet_default_lang'];
$intranet_default_lang = $_SESSION['intranet_default_lang'] ? $_SESSION['intranet_default_lang'] : $intranet_default_lang;

if ($intranet_default_lang == "gb")
	($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
else
	($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];

if (isset($_SESSION["SSV_PRIVILEGE"]["school"]["logo"]))
{
        $logo_img = $_SESSION["SSV_PRIVILEGE"]["school"]["logo"];
}
else
{
        $imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
        $logo_img = ($imgfile == "" ?"{$image_path}/2007a/topbar/eclass_logo.gif" : "/file/$imgfile");
        $_SESSION["SSV_PRIVILEGE"]["school"]["logo"] = $logo_img;
}
if (!strstr($logo_img, "topbar_25/eclass_logo.gif"))
{
	$logo_fixed_height = " height='55' ";
}

$icalendar_name = $plugin["iCalendarFull"]?$iCalendar_iCalendar :$iCalendar_iCalendarLite;


# get the URL of this site
$PageURLArr = split("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}

# eClass eCommunity data
$cs_path = "$intranet_httppath/home/support.php";

# product version change log
//$intranet_version_url = "tw-version.eclass.com.hk";
if (isset($intranet_version_url) && $intranet_version_url!="")
{
	$version_url = $intranet_version_url;
} else
{
	# by default
	$version_url = "hke-version.eclass.com.hk";
}
$products_path = "http://{$version_url}/?eclasskey=".$lu->sessionKey."&eClassURL=".$eClassURL;

$onlinehelpcenter_path = "http://support.broadlearning.com/doc/help/intranet/";

$doccenter_path = "http://support.broadlearning.com/doc/help/portal/";

$videocenter_path = "http://www.youtube.com/user/BroadLearning#grid/user/98D7F2448C1E4667";

$cs_faq = "http://support.broadlearning.com/doc/help/faq/";

# user identity
switch($_SESSION['UserType'])
{
	case USERTYPE_STAFF:
		$UserIdentity = $_SESSION['isTeaching']==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
		$UserIdentityIcon = $_SESSION['isTeaching']==1 ? "icon_teacher.gif" : "icon_nonteacher.gif";
		break;
	case USERTYPE_STUDENT:
		$UserIdentity = $Lang['Identity']['Student'];
		$UserIdentityIcon = "icon_student.gif";
		break;
	case USERTYPE_PARENT:
		$UserIdentity = $Lang['Identity']['Parent'];
		$UserIdentityIcon = "icon_parent.gif";
		break;
	case USERTYPE_ALUMNI:
		$UserIdentity = $Lang['Identity']['Alumni'];
		$UserIdentityIcon = "icon_student.gif";
		break;
	default:
		$UserIdentity = "";
		$UserIdentityIcon = "10x10.gif";
		break;
}

# Build Menu
unset($FullMenuArr);

# Check menu focus
# Home
$CurrentPageArr['Home'] = 1;

# eService
if($CurrentPageArr['eServiceCircular'] || $CurrentPageArr['eServiceHomework'] ||
	$CurrentPageArr['eServiceNotice'] || $CurrentPageArr['eServiceeEnrolment'] ||
	$CurrentPageArr['eServiceeDisciplinev12'] || $CurrentPageArr['eServiceeSurvey'] ||
	$CurrentPageArr['eServiceRepairSystem'] || $CurrentPageArr['eServiceeBooking'] ||
	$CurrentPageArr['eServiceTimetable'] || $CurrentPageArr['PagePolling'] ||
	$CurrentPageArr['SLSLibrary'] || $CurrentPageArr['eServiceClassDiary'] ||
	$CurrentPageArr['eServiceStaffAttendance']
	)
{
	$CurrentPageArr['Home'] = 0;
	$CurrentPageArr['eService'] = 1;
}


# eLearning
if($CurrentPageArr['eClass'] ||
	$CurrentPageArr['eLC'] ||
	$CurrentPageArr['eLib'] ||
	$CurrentPageArr['eLibPlus'] ||
	$CurrentPageArr['IES'] ||
	$CurrentPageArr['iTextbook'] ||
	($CurrentPageArr['DigitalArchive_SubjectReference'] && $_SESSION['UserType']==USERTYPE_STUDENT) ||
	$CurrentPageArr['SBA'] ||
	$CurrentPageArr['WWSeLearningProject']
) {
  $CurrentPageArr['eLearning'] = 1;
	$CurrentPageArr['Home'] = 0;
}


# eAdmin
# ---------- Account Management
if ($CurrentPageArr['StudentMgmt'] || $CurrentPageArr['StaffMgmt'] || $CurrentPageArr['ParentMgmt'] || $CurrentPageArr['StudentRegistry'] || $CurrentPageArr['SharedMailBox'] || $CurrentPageArr['AlumniMgmt'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	# GeneralManagement
	$CurrentPageArr['AccountManagement'] = 1;
}

# ---------- Genearl Management
if ($CurrentPageArr['ePolling'] || $CurrentPageArr['ePayment'] || $CurrentPageArr['schoolNews'] || $CurrentPageArr['eAdmineSurvey'] || $CurrentPageArr['ePOS'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	# GeneralManagement
	$CurrentPageArr['GeneralManagement'] = 1;
}


# ---------- Staff Management
if ($CurrentPageArr['eAdminCircular'] || $CurrentPageArr['eAdminStaffAttendance'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	# Staff Management
	$CurrentPageArr['StaffManagement'] = 1;
}

# ---------- Student Management
#eSports, Swimming Gala
if ($CurrentPageArr['eSports'] || $CurrentPageArr['eAdminNotice'] || $CurrentPageArr['eAdminHomework'] || $CurrentPageArr['StudentAttendance'] || $CurrentPageArr['eReportCard'] || $CurrentPageArr['eEnrolment'] || $CurrentPageArr['eDisciplinev12'] || $CurrentPageArr['eReportCard_Rubrics'] || $CurrentPageArr['eReportCard1.0'] || $CurrentPageArr['ClassDiary'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	$CurrentPageArr['StudentManagement'] = 1;
}

# ---------- Resources Management
if($CurrentPageArr['eInventory'] || $CurrentPageArr['eBooking'] || $CurrentPageArr['eAdminRepairSystem'] || ($CurrentPageArr['DigitalArchive'] || $CurrentPageArr['DocRouting'] && $_SESSION['UserType']==USERTYPE_STAFF))
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	$CurrentPageArr['ResourcesManagement'] = 1;
}

if($CurrentPageArr['eServiceeSports'])
{
	$CurrentPageArr['eService'] = 1;
	$CurrentPageArr['Home'] = 0;
}

# School Settings
if ($CurrentPageArr['Location'] || $CurrentPageArr['Role'] || $CurrentPageArr['Subjects'] || $CurrentPageArr['Class'] || $CurrentPageArr['Group'] || $CurrentPageArr['SchoolCalendar'] || $CurrentPageArr['Timetable'] || $CurrentPageArr['Period'])
{
	$CurrentPageArr['Home'] = 0;
	$CurrentPageArr['SchoolSettings'] = 1;
}

//IF IT IS A STAND ALONE IPORFOLIO , SET THE HOME PAGE TO IPORTFOLIO PAGE
if (isset($_SESSION['SSV_isiPortfolio_standalone']) && $_SESSION['SSV_isiPortfolio_standalone'] && $stand_alone['iPortfolio']){
	$indexPage = "home/portfolio/";
}else{
	$indexPage = "home/index.php";
}


if($CurrentPageArr['eLearningReadingGarden'] || $CurrentPageArr['eLib'] || $CurrentPageArr['eLibPlus'] )
{
	$CurrentPageArr['eLearning'] = 1;
	$CurrentPageArr['Home'] = 0;

}

//debug_r($CurrentPageArr);
# === $MenuArr : array fields description====
#	1 - menu id
#	2 - path / #
#	3 - section name
#	4 - section focus flag
#	5 - css (will change the icon)

if($_SESSION['UserType']!=USERTYPE_ALUMNI)
{
	### Home
	$FullMenuArr[0] = array(0, "/".$indexPage, $Lang['Header']['Menu']['Home'], $CurrentPageArr['Home'], 'home', "Home");

	### eService
	$eServiceMenu_i = 1;
	$eService_i = 0;

	### eService -> Campus TV
	if($plugin['tv'])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(15, "javascript:newWindow(\"/home/plugin/campustv/info.php\",8)", $Lang['Header']['Menu']['CampusTV'], $CurrentPageArr['eServiceCampusTV'], "", "eServiceCampusTV");
		$eService_i++;
	}


	/* for demo
	if($plugin['digital_archive'])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(20, "/home/eService/digital_archive/", $Lang['Header']['Menu']['DigitalArchive'], $CurrentPageArr['eServiceDigitalArchive']);
		$eService_i++;
	}
	*/


	### eService -> eBooking
	if($plugin['eBooking'] && $_SESSION["eBooking"]["canAccesseService"])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(14, "/home/eService/eBooking/", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eServiceeBooking'], "", "eServiceeBooking");
		$eService_i++;
	}

	### eService -> eCircular
	if(!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION['UserType']==USERTYPE_STAFF)
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(11, "/home/eService/circular/", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eServiceCircular'], "", "eServiceCircular");
		$eService_i++;
	}

	### eService -> eDiscipline

	if(
		(($_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT )&& $plugin['Disciplinev12'] &&(!empty($_SESSION['SESSION_ACCESS_RIGHT'])))
		||
		($plugin['Disciplinev12'] && $_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["canAccess_eServive"])
	  )
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(19, "/home/eService/disciplinev12", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eServiceeDisciplinev12'], "", "eServiceeDisciplinev12");
		$eService_i++;
	}

	### eService -> eEnrolment
	$eEnrolTrialPast = false;
	if ($plugin['eEnrollment_trial'] != '' && date('Y-m-d') > $plugin['eEnrollment_trial'])
		$eEnrolTrialPast = true;

	// 2012-0814-1021-26073: student and parent can be eEnrol admin now
	//if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]))
	if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || $_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT))
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(18, "/home/eService/enrollment/", $Lang['Header']['Menu']['eEnrolment'], $CurrentPageArr['eServiceeEnrolment'],"", "eServiceeEnrolment");
		$eService_i++;
	}

	### eService -> eHomework
	//if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
		if((!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"]))))	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(12, "/home/eService/homework/index.php", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eServiceHomework'], "", "eServiceHomework");
		$eService_i++;
	}

	### eService -> eNotice
	if($plugin['notice'] && !$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(11, "/home/eService/notice/", $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eServiceNotice'], "", "eServiceNotice");
		$eService_i++;
	}
	### eService -> eRC
	if($plugin['ReportCard'])
	{
		if ($_SESSION['UserType']==USERTYPE_STAFF)
		{
			$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/reportcard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard'], "", "eServiceeReportCard");
			$eService_i++;
		}
	}

	if($plugin['ReportCard2008'])
	{
		/* 20100604 Ivan: Now, do checking in lib.php, function UPDATE_CONTROL_VARIABLE()
		include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
		$libreportcard = new libreportcard();

		// check if the user is class/subject teacher in the ACTIVE year of eRC
		$Is_ClassTeacher = $libreportcard->Is_Class_Teacher($_SESSION['UserID']);
		$Is_SubjectTeacher = $libreportcard->Is_Subject_Teacher($_SESSION['UserID']);
		*/

		$Is_ClassTeacher = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"];
		$Is_SubjectTeacher = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_teacher"];

		if ($Is_ClassTeacher || $Is_SubjectTeacher || $_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)
		{
			$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard'],"","eServiceeReportCard");
			$eService_i++;
		}
	}

	### eService -> eSports
	if($_SESSION["UserType"]==2 && ($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"]||$_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"]))
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i][0] = array(18, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eServiceeSports']);
		$eSports_i = 0;
		if($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"])
		{
			$FullMenuArr[$eServiceMenu_i][1][$eService_i][1][$eSports_i] = array(181, "/home/eService/eSports/sports/", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['eServiceSportDay'], "", "eServiceSportDay");
			$eSports_i ++;
		}
		if($_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"])
		{
			$FullMenuArr[$eServiceMenu_i][1][$eService_i][1][$eSports_i] = array(182, "/home/eService/eSports/swimming_gala/", $Lang['Header']['Menu']['SwimmingGala'], $CurrentPageArr['eServiceSwimmingGala'],"", "eServiceSwimmingGala");
			$eSports_i ++;
		}
		$eService_i++;
	}

	### eService -> eSurvey
	if (!$plugin['DisableSurvey'])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(17, "/home/eService/eSurvey/index.php", $Lang['Header']['Menu']['eSurvey'], $CurrentPageArr['eServiceeSurvey'],"","eServiceeSurvey");
		$eService_i++;
	}

	### eService -> Poll
	if($_SESSION['UserType']!=USERTYPE_PARENT && !$plugin['DisablePoll'])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(17, "/home/eService/polling/index.php", $Lang['Header']['Menu']['ePolling'], $CurrentPageArr['PagePolling'],"","PagePolling");
		$eService_i++;
	}

	### eService -> Repair System
	if($plugin['RepairSystem'])
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(17, "/home/eService/RepairSystem/", $Lang['Header']['Menu']['RepairSystem'], $CurrentPageArr['eServiceRepairSystem'],"","eServiceRepairSystem");
		$eService_i++;
	}


	### eService -> Resource Booking
	if($plugin['ResourcesBooking'] && (!$plugin['DisableResourcesBookingOnRequest'] || !isset($plugin['DisableResourcesBookingOnRequest'])) )
	{
		$access2eServiceeBooking = false;
		if (isset($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"]) and $_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
		{
			$access2eServiceeBooking = true;
		}
		else
		{
			if($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
			{
				$access2eServiceeBooking = true;
			}
		}
		if($access2eServiceeBooking){
			$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(183, "javascript: newWindow(\"/home/eService/ResourcesBooking\",\"8\");", $Lang['Header']['Menu']['ResourcesBooking'], $CurrentPageArr['ResourcesBooking'],"","ResourcesBooking");
			$eService_i++;
		}
	}

	# eService -> SLS Library
	if(isset($_SESSION["SSV_PRIVILEGE"]["library"]["access"]) && $_SESSION["SSV_PRIVILEGE"]["library"]["access"])
	{
	        $header_library_access = $_SESSION["SSV_PRIVILEGE"]["library"]["access"];
	}
	else
	{
	        if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]["library"]) && $_SESSION["SSV_PRIVILEGE"]["plugin"]["library"])
	        {
	                    global $mapping_file_path;

						if (!isset($_SESSION['sls_library_access']))
	             		 {
		                    $content = get_file_content("$mapping_file_path");
		                    $records = explode("\n",$content);

		                    //$header_lu = new libuser($UserID);
		                    //$login = $header_lu->UserLogin;
		                    $login = $lu->UserLogin;


		                    $header_library_access = false;

		                    for ($i=0; $i<sizeof($records); $i++)
		                    {
		                         $data = split("::",$records[$i]);
		                         if (strtolower($login) == strtolower($data[0]))
		                         {
		                             $header_library_access = true;
		                             break;
		                         }
		                    }
		                    $_SESSION['sls_library_access'] = $header_library_access;
	           		   } else
	           		   {
	           		   		$header_library_access = $_SESSION['sls_library_access'];
	           		   }
	        }
	        else
	        {
	            $header_library_access = false;
	        }

	        $_SESSION["SSV_PRIVILEGE"]["library"]["access"] = $header_library_access;


	}
	if($header_library_access)
	{
		//$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(20, "/home/eService/sls_library_index.php", $Lang['SLSLibrary'], $CurrentPageArr['SLSLibrary'],"","SLSLibrary");
		$FullMenuArr[$eServiceMenu_i][1][$eService_i][0] = array(20, "#", $Lang['SLSLibrary'], $CurrentPageArr['SLSLibrary'],"","SLSLibrary");
		$FullMenuArr[$eServiceMenu_i][1][$eService_i][1][0] = array(201, "javascript:newWindow(\"/home/plugin/sls/library.php\",8)", $Lang['SLSSearch'], $CurrentPageArr['SLSLibrary'], "", "SLSLibrary");
		$FullMenuArr[$eServiceMenu_i][1][$eService_i][1][1] = array(202, "javascript:newWindow(\"/home/plugin/sls/library_info.php\",8)", $Lang['SLSInfo'], $CurrentPageArr['SLSLibrary'], "", "SLSLibrary");

		$eService_i++;
	}
	# end of SLS Library

	# eService -> Chuen Yuen Award Scheme
	if($sys_custom['chuen_yuen_award_scheme'] && $_SESSION['UserType'] == USERTYPE_STUDENT){
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(21, "/home/award_scheme/", $Lang['customization']['award_scheme'], '');
		$eService_i++;
	}

	# eService -> Timetable
	if ($_SESSION['UserType']==USERTYPE_STAFF && !$plugin['DisableTimeTable']) {
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(22, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/view.php?clearCoo=1&From_eService=1", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['eServiceTimetable'], "", "eServiceTimetable");
		$eService_i++;
	}


	# eService -> Class Diary
	if($plugin['ClassDiary'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)) {
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(23, $PATH_WRT_ROOT_ABS."home/eService/classdiary/", $Lang['Header']['Menu']['ClassDiary'], $CurrentPageArr['eServiceClassDiary'], "", "eServiceClassDiary");
		$eService_i++;
	}

	# eService -> Staff Attendance System
	if($_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] && ($module_version['StaffAttendance'] == 3.0) && ($_SESSION['UserType'] == USERTYPE_STAFF) && ($sys_custom['StaffAttendance']['DailyAttendanceRecord'] || $sys_custom['StaffAttendance']['DoctorCertificate']) )
	{
		$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(24, $PATH_WRT_ROOT_ABS."home/eService/StaffAttendance/", $Lang['StaffAttendance']['StaffAttendance'], $CurrentPageArr['eServiceStaffAttendance'], "", "eServiceStaffAttendance");
		$eService_i++;
	}


	if ($eService_i<=0)
	{
		$eServiceMenu_i = 0;
	} else
	{
		$FullMenuArr[$eServiceMenu_i][0] = array(1, "#", $Lang['Header']['Menu']['eService'], $CurrentPageArr['eService'], 'common', "eService");
	}

	$eClassMenu_i = $eServiceMenu_i + 1;

	### eLearning
	$FullMenuArr[$eClassMenu_i][0] = array(2, "#", $Lang['Header']['Menu']['eLearning'], $CurrentPageArr['eLearning'], 'common', "eLearning" );


	$pos = 0;

	# eLearning > Digital Archive (Student/Staff entrance)

	/*
	if($sys_custom['subject_resources'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF)) {

		$FullMenuArr[$eClassMenu_i][1][$pos] = array(29, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'],"","DigitalArchive");
		$pos++;
	}
	*/


	/* change to first level menu on 25 Sept 2012
	if (($plugin['eLib'] || $plugin['library_management_system'] || $plugin['ReadingScheme'] || $sys_custom['subject_resources']) && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF))
	{
		$CurrentPageArr['DRC'] = ($CurrentPageArr['eLib'] || $CurrentPageArr['eLibPLus'] || $CurrentPageArr['eLearningReadingGarden'] || $CurrentPageArr['DigitalArchive_SubjectReference']);
		$FullMenuArr[$eClassMenu_i][1][$pos][0] = array(210, "#", $Lang['Header']['Menu']['DRC'], $CurrentPageArr['DRC'], "", "DRC");
		$drc_i = 0;
		if ($plugin['eLib'] || $plugin['library_management_system'])
		{
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(211, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib'], "", "eLib");
			$drc_i ++;
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(212, "/home/eLearning/elibplus/", $Lang['Header']['Menu']['eLibraryPlus'], $CurrentPageArr['eLibPLus'], "", "eLibPLus");
			$drc_i ++;
		}
		if ($plugin['ReadingScheme'])
		{
			if($_SESSION['UserType']==USERTYPE_STUDENT)
				$ReadingSchemeLink = "/home/eLearning/reading_garden/student_login.php";
			else
				$ReadingSchemeLink = "/home/eLearning/reading_garden/?clearCoo=1";

			$FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(213, $ReadingSchemeLink, $Lang['Header']['Menu']['ReadingScheme'], $CurrentPageArr['eLearningReadingGarden'], "", "eLearningReadingGarden");
			$drc_i ++;
		}
		if ($sys_custom['subject_resources'])
		{
			//$FullMenuArr[$eClassMenu_i][1][$pos] = array(29, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'],"","DigitalArchive");
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(214, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'], "", "DigitalArchive_SubjectReference");
			$drc_i ++;
		}
		$pos++;
	}
	*/

	//$FullMenuArr[$eClassMenu_i][1][0] = array(21, "/home/eLearning/eclass/", $Lang['Header']['Menu']['eClass'], $CurrentPageArr['eClass'],"","eClass");
	$FullMenuArr[$eClassMenu_i][1][$pos][0] = array(22, "/home/eLearning/eclass/", $Lang['Header']['Menu']['eClass'], $CurrentPageArr['eClass'],"","eClass");
	$pos++;
	if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2SpecialRm"])
	{

		//$FullMenuArr[$eClassMenu_i][1][$pos][0] = array(22, "/home/eLearning/elc/", $Lang['Header']['Menu']['eLC'], $CurrentPageArr['eLC'],"","eLC");
		$FullMenuArr[$eClassMenu_i][1][$pos][0] = array(23, "#", $Lang['Header']['Menu']['eLC'], $CurrentPageArr['eLC'],"","eLC");
		$elc_i = 0;
		if ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Elprm"])
		{
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(231, "javascript:newWindow(\"/home/eLearning/elc/index_elp.php\",8)", $Lang['Header']['Menu']['ELP'], $CurrentPageArr['ELP'], "", "srELP");
			$elc_i ++;
		}

		if ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Readingrm"])
		{
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(232, "javascript:newWindow(\"/home/eLearning/elc/readingrm.php\",8)", $Lang['Header']['Menu']['ReadingRoom'], $CurrentPageArr['ELP'], "", "srELP");
			$elc_i ++;
		}
		if ($plugin['ScrabbleFC'])
		{
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(233, "javascript:toScrabble()", $Lang['Header']['Menu']['SFC'], $CurrentPageArr['ELP'], "", "srELP");
			$elc_i ++;
		}
		if ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Ssrm"])
		{
			$FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(234, "javascript:newWindow(\"/home/eLearning/elc/index_ssr.php\",8)", $Lang['Header']['Menu']['SSR'], $CurrentPageArr['ELP'], "", "srELP");
			$elc_i ++;
		}
		$pos ++;
	}
	/*
	if ($plugin['eLib'])
	{
		$FullMenuArr[$eClassMenu_i][1][] = array(23, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib']);
	}
	*/



	if ($plugin['eLib'] && !$plugin['library_management_system'])
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(20, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib'], "", "eLib");
		$pos ++;
	}

	# may hide only for admin to view
	if ($plugin['library_management_system'] && (!$sys_custom['block_access_temporily'] || ($sys_custom['block_access_temporily'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])))
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(20, "/home/eLearning/elibplus/", $Lang['Header']['Menu']['eLibraryPlus'], $CurrentPageArr['eLibPLus'], "", "eLibPlus");
		$pos ++;
	}


	if($plugin['ePost'])
	{
		include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
		$ePost = new libepost();

		if(!$ePost->check_is_expired()){
			$FullMenuArr[$eClassMenu_i][1][$pos] = array(21, "javascript:newWindow(\"/home/ePost/index.php\", 36)", $Lang['ePost']['ePost'], $CurrentPageArr['ePost'],"","ePost");
			$pos ++;
		}
	}

	if ($plugin['iTextbook'] && ($_SESSION["SSV_USER_ACCESS"]["eLearning-iTextbook"]))
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(27, "/home/eLearning/iTextbook/", $Lang['itextbook']['itextbook'], $CurrentPageArr['iTextbook'],"","iTextbook");
		$pos ++;
	}


	if ($plugin['lslp'])
	{
		include_once ($PATH_WRT_ROOT."includes/liblslp.php");
		$ls = new lslp();
		# to be improved to control on users (i.e. particular students, teachers)
		if($ls->hasLSLPAccess() && $ls->isInLicencePeriod())
		{
			$FullMenuArr[$eClassMenu_i][1][$pos] = array(24,"javascript:open_lslp();", $Lang['lslp']['lslp'], $CurrentPageArr['LSLP'],"","LSLP");
			$pos ++;
		}
	}
	if ($plugin['IES'] && ($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"]))
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(25, "/home/eLearning/ies/", $Lang['Header']['Menu']['IES'], $CurrentPageArr['IES'],"","IES");
		$pos ++;
	}

	if ($plugin['ReadingScheme'])
	{
		if($_SESSION['UserType']==USERTYPE_STUDENT)
			$ReadingSchemeLink = "/home/eLearning/reading_garden/student_login.php";
		else
			$ReadingSchemeLink = "/home/eLearning/reading_garden/?clearCoo=1";

		$FullMenuArr[$eClassMenu_i][1][$pos] = array(213, $ReadingSchemeLink, $Lang['Header']['Menu']['ReadingScheme'], $CurrentPageArr['eLearningReadingGarden'], "", "eLearningReadingGarden");
		$pos ++;
	}

	if ($sys_custom['subject_resources'])
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(214, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'], "", "DigitalArchive_SubjectReference");
		$pos ++;
	}

	// with SBA plugin
	if (count($plugin['SBA']) > 0 && ($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"] || $_SESSION["SSV_PRIVILEGE"]["SBA"]["isTeacher"] || $_SESSION["SSV_PRIVILEGE"]["SBA"]["isStudent"]))
	{
		if($_SESSION['UserType']==USERTYPE_STUDENT) {
			$FullMenuArr[$eClassMenu_i][1][$pos] = array(43, "javascript:newWindow(\"/home/eLearning/sba/\", 32)", $Lang['Header']['Menu']['SBA'], $CurrentPageArr['SBA'],"","SBA");
			$pos ++;
		} else if($_SESSION['UserType']==USERTYPE_STAFF) {
			$FullMenuArr[$eClassMenu_i][1][$pos] = array(43, "/home/eLearning/sba/", $Lang['Header']['Menu']['SBA'], $CurrentPageArr['SBA'],"","SBA");
			$pos ++;
		}
	}
	/*
	if ($plugin['ReadingScheme'])
	{
		if($_SESSION['UserType']==USERTYPE_STUDENT)
			$ReadingSchemeLink = "/home/eLearning/reading_garden/student_login.php";
		else
			$ReadingSchemeLink = "/home/eLearning/reading_garden/?clearCoo=1";

		$FullMenuArr[$eClassMenu_i][1][] = array(26, $ReadingSchemeLink, $Lang['Header']['Menu']['ReadingScheme'], $CurrentPageArr['eLearningReadingGarden']);
	}
	*/

	if ($plugin['W2'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF))
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(28, "javascript:newWindow(\"/home/eLearning/w2/\", 32)", $Lang['Header']['Menu']['Writing'], $CurrentPageArr['W2'],"","W2");
		$pos ++;
	}


	if($plugin['WWS_eLearningProject'])
	{
		$FullMenuArr[$eClassMenu_i][1][$pos] = array(797, "/home/eLearning/eclass2/?roomType=13", $Lang['wws']['eLearningProject'], $CurrentPageArr['WWSeLearningProject'],"","WWSeLearningProject");
		$pos ++;
	}

	if ($plugin['kskgsmath'])
	{
		include_once ($PATH_WRT_ROOT."includes/libksk.php");
		$ksk = new ksk();
		# to be improved to control on users (i.e. particular students, teachers)
		if($ksk->hasKSKAccess())
		{
			if($lu->isStudent()){
				$FullMenuArr[$eClassMenu_i][1][$pos] = array(26, "javascript:open_kskgs(1);", $Lang['kskgsmath']['kskgsmath'].' - '.$Lang['kskgsmath']['kskgsmath_gs'], $CurrentPageArr['KSK_GSMATH_GS'],"","KSK_GSMATH_GS");
				$pos ++;
				$FullMenuArr[$eClassMenu_i][1][$pos] = array(26, "javascript:open_kskgs(2);", $Lang['kskgsmath']['kskgsmath'].' - '.$Lang['kskgsmath']['kskgsmath_math'], $CurrentPageArr['KSK_GSMATH_MATH'],"","KSK_GSMATH_MATH");
				$pos ++;
			}
			else{
				$FullMenuArr[$eClassMenu_i][1][$pos] = array(26, "javascript:open_kskgs();", $Lang['kskgsmath']['kskgsmath'], $CurrentPageArr['KSK_GSMATH'],"","KSK_GSMATH");
				$pos ++;
			}

		}
	}
}
### eAdmin
###### check display Account Management
/*
include_once($intranet_root."/includes/libaccountmgmt.php");
$laccount = new libaccountmgmt();
$officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();
if(sizeof($officalPhotoMgmtMember) && in_array($UserID, $officalPhotoMgmtMember))
	$canAccessOfficalPhotoSetting = 1;
*/
$canAccessOfficalPhotoSetting = $_SESSION["SSV_PRIVILEGE"]["canAccessOfficalPhotoSetting"];

if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || ($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY']))) || ($plugin['imail_gamma']==true && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"]) || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)
	$AccountMgmt = 1;

###### check display General Management
if(
	($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"]) ||
	($plugin['payment'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"]) ||
	($plugin['payment'] && $plugin['ePOS'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) ||
	($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"]) ||
	($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"]) ||
	($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) ||
	$_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] ||
	$_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"] ||
	$_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"]
	)
	$GeneralMgmt = 1;
###### check display Staff Management
if(
	($special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]))) ||
	($module_version['StaffAttendance'] == 3.0 && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"]))
	)
	//($plugin['attendancestaff'])
	$StaffMgmt = 1;

###### check display Student Management


## Special checking for eEnrolment
$show_eAdmin_eEnrol = false;
if ($plugin['eEnrollment'] && !$eEnrolTrialPast)
{
	/* 20100604 Ivan: Now, get admin status in lib.php, function UPDATE_CONTROL_VARIABLE()
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();

	if (isset($header_lu) == false)
	{
		include_once ($PATH_WRT_ROOT."/includes/libuser.php");
		$header_lu = new libuser($_SESSION['UserID']);
	}
	*/

	if ($_SESSION['UserType']!=USERTYPE_STUDENT && $_SESSION['UserType']!=USERTYPE_PARENT)
	{
		if ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_admin"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_master"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"])
			$show_eAdmin_eEnrol = true;
	}
}
## End Special checking for eEnrolment

$show_eAdmin_ReportCardRubrics = false;
if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard_Rubrics"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"] || count((array)$_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["PageAccessRightArr"]) > 0)
	$show_eAdmin_ReportCardRubrics = true;

include_once($PATH_WRT_ROOT."/includes/libhomework.php");
include_once($PATH_WRT_ROOT."/includes/libhomework2007a.php");
$lhomework = new libhomework2007();

if (
		($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) ||
		($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) ||
		($plugin['Disciplinev12'] && (($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) || $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"])) ||
		($plugin['eEnrollment'] && $show_eAdmin_eEnrol) ||

		(
			(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] &&
			((
				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
				$_SESSION['isTeaching'] ||
				($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
				$lhomework->isViewerGroupMember($UserID)
			) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework']))
		   ) ||

		($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) ||
		($plugin['ReportCard'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"]) ||
		($plugin['ReportCard2008'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"]) ||
		($plugin['ReportCard_Rubrics'] && $show_eAdmin_ReportCardRubrics) ||
		($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) ||
		($plugin['swimmming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"]) ||
		($plugin['ClassDiary'] && $_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION['isTeaching']==1 /*&& $_SESSION["SSV_USER_ACCESS"]["eAdmin-ClassDiary"]*/)
	)
	$StudentMgmt = 1;

	//($plugin['RepairSystem'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"])
###### check display Resources Management

if (isset($plugin['RepairSystem']) && $plugin['RepairSystem'])
{
	include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
	$lrepairsystem = new librepairsystem();
}

if($sys_custom['Invoice2Inventory'] && $plugin['Inventory']) {
	include_once($PATH_WRT_ROOT."includes/libinvoice.php");
	$linvoice = new libinvoice();
}

if(
	($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER" )) ||
	($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")) ||
	($plugin['RepairSystem'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"] || $lrepairsystem->userInMgmtGroup($UserID)>0)) ||
	($plugin['digital_archive'] && $_SESSION['UserType']==USERTYPE_STAFF) ||
	(($sys_custom['Invoice2Inventory'] && $plugin['Inventory']) && ($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || $linvoice->isViewerGroupMember))
){
	$ResourcesMgmt = 1;
}

$customizationModuleMgmtIn_eAdmin = false; //flag to display eAdmin menu , default = false
$customizationModuleMgmtIn_eAdmin_StudentMgmt = false; //flag to display eAdmin >> student mgmt menu , default = false
$canAccess_CY_awardScheme =false;
if($sys_custom['chuen_yuen_award_scheme'] && $_SESSION['UserType'] == USERTYPE_STAFF){
	//case 2012-0216-1210-32073 , once with this plugin and is a USERTYPE_STAFF , client can access this module
	$canAccess_CY_awardScheme = true;

	//may skip this checking
//	include_once($intranet_root."/includes/libawardscheme.php");
//	$objCY_awardScheme = new libawardscheme();
//	$adminUserList = $objCY_awardScheme->GET_ADMIN_USER();
//	if(trim($adminUserList) != ''){
//		$cy_adminAry = explode(',',$adminUserList);
//		if(in_array($UserID,(array)$cy_adminAry)){
//			$canAccess_CY_awardScheme = true;
//		}
//	}

	if($canAccess_CY_awardScheme){
		$customizationModuleMgmtIn_eAdmin= true;
		$customizationModuleMgmtIn_eAdmin_StudentMgmt = true;
	}
}

if($AccountMgmt || $GeneralMgmt || $StaffMgmt || $StudentMgmt || $ResourcesMgmt || $customizationModuleMgmtIn_eAdmin)
{
	$eAdminMenu_i = $eClassMenu_i + 1;
	$FullMenuArr[$eAdminMenu_i][0] = array(3, "#", $Lang['Header']['Menu']['eAdmin'], $CurrentPageArr['eAdmin'], 'common',"eAdmin");

	$eAdmin_i = 0;

	### eAdmin -> Account Management
	if($AccountMgmt) {

		$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(38, "#", $Lang['Header']['Menu']['AccountMgmt'], $CurrentPageArr['AccountManagement'],"","AccountManagement");
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Alumni"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting) {

			if($special_feature['alumni'])
			{
				if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]  || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
					$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(386, "/home/eAdmin/AccountMgmt/AlumniMgmt/?clearCoo=1", $Lang['Header']['Menu']['AlumniAccount'], $CurrentPageArr['AlumniMgmt'],"","AlumniMgmt");
			}

			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
				$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(383, "/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1", $Lang['Header']['Menu']['ParentAccount'], $CurrentPageArr['ParentMgmt'],"","ParentMgmt");
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
				$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(382, "/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1", $Lang['Header']['Menu']['StaffAccount'], $CurrentPageArr['StaffMgmt'],"","StaffMgmt");
			if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)
				$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(381, "/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1", $Lang['Header']['Menu']['StudentAccount'], $CurrentPageArr['StudentMgmt'],"","StudentMgmt");

		}

		if($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY'])))
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(384, "/home/eAdmin/AccountMgmt/StudentRegistry/", $Lang['Header']['Menu']['StudentRegistry'], $CurrentPageArr['StudentRegistry'],"","StudentRegistry");
		if($plugin['imail_gamma']==true && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"])
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(385, "/home/eAdmin/AccountMgmt/SharedMailBox/", $Lang['Header']['Menu']['SharedMailBox'], $CurrentPageArr['SharedMailBox'],"","SharedMailBox");
		$eAdmin_i++;
	}

	### eAdmin -> GeneralManagement
	if($GeneralMgmt)
	{
		$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(34, "#", $Lang['Header']['Menu']['GeneralManagement'], $CurrentPageArr['GeneralManagement'],"","GeneralManagement");

		### eAdmin -> GeneralManagement -> ePayment
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $plugin['payment'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(344, "/home/eAdmin/GeneralMgmt/payment/", $Lang['Header']['Menu']['ePayment'], $CurrentPageArr['ePayment'],"","ePayment");
		}

		### eAdmin -> GeneralManagement -> ePayment
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $plugin['ePOS'] && $plugin['payment'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(345, "/home/eAdmin/GeneralMgmt/pos/", $Lang['Header']['Menu']['ePOS'], $CurrentPageArr['ePOS'],"","ePOS");
		}

		### eAdmin -> GeneralManagement -> eSurvey
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(343, "/home/eAdmin/GeneralMgmt/eSurvey/", $Lang['Header']['Menu']['eSurvey'] , $CurrentPageArr['eAdmineSurvey'],"","eAdmineSurvey");
		}

		### eAdmin -> GeneralManagement -> MessageCenter
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
		{
			if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/SMS/";
			} elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"])
			{
				$mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/";
			}
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(346, $mc_first_page, $Lang['Header']['Menu']['MessageCenter'], $CurrentPageArr['eAdmineMassMailing'],"","eAdmineMassMailing");
		}

		### eAdmin -> GeneralManagement -> ePolling
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(341, "/home/eAdmin/GeneralMgmt/polling/", $Lang['Header']['Menu']['ePolling'], $CurrentPageArr['ePolling'],"","ePolling");
		}

		### eAdmin -> GeneralManagement -> schoolNews
		if($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(342, "/home/eAdmin/GeneralMgmt/schoolnews/", $Lang['Header']['Menu']['schoolNews'], $CurrentPageArr['schoolNews'],"","schoolNews");
		}


		$eAdmin_i++;
	}

	### eAdmin -> StaffManagement
	if($StaffMgmt)
	{
		$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(31, "#", $Lang['Header']['Menu']['StaffManagement'], $CurrentPageArr['StaffManagement'],"","StaffManagement");

		/*
		if($plugin['attendancestaff'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(311, "#", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StaffeAttednance']);
		}
		*/

		// staff attendance v3
		if ($module_version['StaffAttendance'] == 3.0 && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"])) {
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(313, "/home/eAdmin/StaffMgmt/attendance/", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['eAdminStaffAttendance'],"","eAdminStaffAttendance");
		}

		if($special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(312, "/home/eAdmin/StaffMgmt/circular/", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eAdminCircular'],"", "eAdminCircular");
		}

		$eAdmin_i++;
	}

	### eAdmin -> StudentManagement
	if ($StudentMgmt || $customizationModuleMgmtIn_eAdmin_StudentMgmt) {
		$eAdmin_StudentMgmt_i = 0;
		$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(32, "#", $Lang['Header']['Menu']['StudentManagement'], $CurrentPageArr['StudentManagement'],"","StudentManagement");

		### eAdmin -> StudentManagement -> eAttendance
		if (($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) || ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(321, "/home/eAdmin/StudentMgmt/attendance/", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StudentAttendance'],"","StudentAttendance");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eDis
		if($plugin['Disciplinev12'] && (($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) || $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"]))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(322, "/home/eAdmin/StudentMgmt/disciplinev12/overview", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eDisciplinev12'],"","eDisciplinev12");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eEnrolment
		if($plugin['eEnrollment'])
		{
			if ($show_eAdmin_eEnrol)
			{
				$eEnrolmentTitle = $plugin['eEnrollmentLite'] ? $Lang['Header']['Menu']['eEnrolmentLite'] : $Lang['Header']['Menu']['eEnrolment'];
				$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(323, "/home/eAdmin/StudentMgmt/enrollment/", $eEnrolmentTitle, $CurrentPageArr['eEnrolment'],"","eEnrolment");
				$eAdmin_StudentMgmt_i++;
			}
		}

		### eAdmin -> StudentManagement -> eHomework
		//if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"])))
		//if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION['isTeaching'])))
		//echo $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"].'/';
		if(
			(
				$_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
				$_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
				$_SESSION['isTeaching'] ||
				($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
				$lhomework->isViewerGroupMember($UserID)
			) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(324, "/home/eAdmin/StudentMgmt/homework/index.php", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eAdminHomework'],"","eAdminHomework");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eNotice
		if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(325, "/home/eAdmin/StudentMgmt/notice/", $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eAdminNotice'],"","eAdminNotice");
			//$FullMenuArr[$eAdminMenu_i][1][1][1][4][1][] = array(3276, "#", "test sub sub sub menu XD", $CurrentPageArr['eNotice']);	# pls don't delete, ref usage
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eRC 1.0
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"] && $plugin['ReportCard'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(326, "/home/eAdmin/StudentMgmt/reportcard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard1.0'],"","eReportCard1.0");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eRC
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"] && $plugin['ReportCard2008'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(327, "/home/redirect.php?mode=1&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard'],"","eReportCard");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eRC (Rubrics)
		if($show_eAdmin_ReportCardRubrics && $plugin['ReportCard_Rubrics'])
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(328, "/home/eAdmin/StudentMgmt/reportcard_rubrics/index.php?from_eAdmin=1", $Lang['Header']['Menu']['eReportCard_Rubrics'], $CurrentPageArr['eReportCard_Rubrics'],"","eReportCard_Rubrics");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eSports
		if(($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) || ($plugin['swimmming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"]))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][0] = array(329, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eSports'],"","eSports");
			if($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"])
				$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3291, "/home/eAdmin/StudentMgmt/eSports/sports/index.php", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['SportDay'],"","SportDay");
			if($plugin['swimming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"])
				$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3292, "/home/eAdmin/StudentMgmt/eSports/swimming_gala/index.php", $Lang['Header']['Menu']['SwimmingGala'], $CurrentPageArr['SwimmingGala'], "", "SwimmingGala");
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> Chuen Yuen Award Scheme
		if($canAccess_CY_awardScheme && $sys_custom['chuen_yuen_award_scheme'] && $_SESSION['UserType'] == USERTYPE_STAFF)
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(330, "/home/award_scheme/", $Lang['customization']['award_scheme'], '');
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> Class Diary
		if($plugin['ClassDiary'] && $_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION['isTeaching']==1 /*&& $_SESSION["SSV_USER_ACCESS"]["eAdmin-ClassDiary"]*/)
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(331, "/home/eAdmin/StudentMgmt/classdiary/", $Lang['Header']['Menu']['ClassDiary'], $CurrentPageArr['ClassDiary'] ,"", "ClassDiary");
			$eAdmin_StudentMgmt_i++;
		}

		$eAdmin_i++;
	}

	### eAdmin -> ResourcesManagement
	if($ResourcesMgmt)
	{
		$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(32, "#", $Lang['Header']['Menu']['ResourcesManagement'], $CurrentPageArr['ResourcesManagement'],"","ResourcesManagement");

		### eAdmin -> ResourcesManagement -> eBooking
		if($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER"))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/eBooking", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eBooking'],"","eBooking");
		}

		if($plugin['digital_archive'] && $_SESSION['UserType']==USERTYPE_STAFF) {
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/", $Lang['Header']['Menu']['DigitalArchive'], $CurrentPageArr['DigitalArchive'],"","DigitalArchive");
		}

		//if($plugin['DocRouting'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-DocRouting']) && $_SESSION['UserType']== USERTYPE_STAFF){
		if($plugin['DocRouting'] && $_SESSION['UserType']== USERTYPE_STAFF){
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/DocRouting/", $Lang['DocRouting']['DocRouting'], $CurrentPageArr['DocRouting'], "", "DocRouting");
		}

		### eAdmin -> ResourcesManagement -> eInventory
		if($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != ""))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(331, "/home/eAdmin/ResourcesMgmt/eInventory", $Lang['Header']['Menu']['eInventory'], $CurrentPageArr['eInventory'],"","eInventory");
		}

		### eAdmin -> ResourcesManagement -> Invoice Mgmt System (customization for Fukien Secondary School)
		//include_once($PATH_WRT_ROOT."includes/libinvoice.php");
		//$linvoice = new libinvoice();
		if($plugin['Inventory'] && $sys_custom['Invoice2Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "" || $linvoice->isViewerGroupMember))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(333, "/home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem", $Lang['Header']['Menu']['InvoiceMgmtSystem'], $CurrentPageArr['InvoiceMgmtSystem'],"","InvoiceMgmtSystem");
		}

		### eAdmin -> ResourcesManagement -> Repair System
		//if($plugin['RepairSystem'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"]))
		if (isset($plugin['RepairSystem']) && $plugin['RepairSystem'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] || $lrepairsystem->userInMgmtGroup($UserID)>0))
		{
			$FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(331, "/home/eAdmin/ResourcesMgmt/RepairSystem", $Lang['Header']['Menu']['RepairSystem'], $CurrentPageArr['eAdminRepairSystem'],"","eAdminRepairSystem");
		}
	}

	$SchoolSettingsMenu_i = $eAdminMenu_i + 1;
}
else
	$SchoolSettingsMenu_i = $eClassMenu_i;


### School Settings
if(	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]
)
{
	$FullMenuArr[$SchoolSettingsMenu_i][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting', "SchoolSettings");
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(41, $PATH_WRT_ROOT_ABS."home/system_settings/location/", $Lang['Header']['Menu']['Site'], $CurrentPageArr['Location'],"","Location");
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(44, $PATH_WRT_ROOT_ABS."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class'],"","Class");
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(45, $PATH_WRT_ROOT_ABS."home/system_settings/group/?clearCoo=1", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group'],"","Group");
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT_ABS."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role'],"","Role");
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT_ABS."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar'],"","SchoolCalendar");
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(43, $PATH_WRT_ROOT_ABS."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subject'], $CurrentPageArr['Subjects'],"","Subjects");
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/index.php?clearCoo=1", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable'],"","Timetable");
	}
	//$FullMenuArr[4][1][] = array(48, $PATH_WRT_ROOT_ABS."home/system_settings/period/", $Lang['Header']['Menu']['Period'], $CurrentPageArr['Period']);
}

if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] && !isset($iNewCampusMail))
{
    # check any new message from DB
    include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
    if (!isset($header_lc))
    {
    	$header_lc = new libcampusmail();
    }

	# no need to count for iMail Gamma/Plus
	if (!$plugin['imail_gamma'])
	{
	    if ($special_feature['imail'])
	        $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
	    else
	    	$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);
	}

	# only check webmail if there is no new mail from DB!
	if ($iNewCampusMail<=0 && !$plugin['imail_gamma'])
	{
		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
		if (!isset($lwebmail))
		{
			$lwebmail = new libwebmail();
		}

	    # Check preference of check email
	    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
	    $temp = $header_lc->returnArray($sql,1);
	    $optionSkipCheckEmail = $temp[0][0];
	    if ($optionSkipCheckEmail)
	    {
	        $skipCheck = true;
	    }
	    else
	    {
	        $skipCheck = false;
	    }

		if (!$skipCheck && $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] && $lwebmail->type==3 && !$_SESSION["EmailLoginFailure".$lu->UserLogin])
		{
		    if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
		    {
		        $exmail_count = $lwebmail->checkMailCount();
		        $lwebmail->close();
		        $iNewCampusMail += $exmail_count;
		    } else
		    {
		    	# mark the user's email authenticate
		    	# and don't connect to mail server next time (to avoid slow performance)
		    	$_SESSION["EmailLoginFailure".$lu->UserLogin] = true;
		    }
		}
	}
}
# determine iMail icon for display
$iMailClass = ($iNewCampusMail>0) ? "itool_imail_new" : "itool_imail";


### Check any iFolder ###
$access2iFile = false;
if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]['aerodrive']) && $_SESSION["SSV_PRIVILEGE"]["plugin"]['aerodrive'])
{
        $access2iFile = true;
}

if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]['personalfile']) && $_SESSION["SSV_PRIVILEGE"]["plugin"]['personalfile'])
{
    if ($personalfile_type == 'AERO')
    {
        #$moving_layer_text .= $link_personalFiles;
        #$layer_length -= 25;
    }
    else
    {
        include_once("$intranet_root/includes/libftp.php");
        include_once ($PATH_WRT_ROOT."includes/libsystemaccess.php");
        $header_lsysacc = new libsystemaccess($UserID);
        $header_lftp = new libftp();
        if ($header_lsysacc->hasFilesRight())
        {
            $access2iFile = true;
        }
    }
}



### UI preparation
$bubbleMsgBlock =<<<campusLink

	<div class="sub_layer_board" id="sub_layer_campus_link" style="visibility:hidden;z-index:100">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br  style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_campus_link','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a></div>
			</div>
			<div class="bubble_board_03">
				<div class="bubble_board_04">
					<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock"></span>
					</div>
					<div id="bubble_board_content">

					</div>
				</div>
			</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>
	<div class="sub_layer_board" id="sub_layer_power_tool" style="visibility:hidden;z-index:100;width:180px;">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br  style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_power_tool','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a></div>
			</div>
			<div class="bubble_board_03">
				<div class="bubble_board_04">
					<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock"></span>
					</div>
					<div id="bubble_board_content">
						<div class="powertool_list">
					<!--<a class="tool_powervoice" href="#">PowerVoice</a>-->
					<a onclick="newWindow('/home/plugin/powerspeech',16)" class="tool_powerspeech" href="#">{$Lang['Header']['Menu']['PowerSpeech']}</a>
                    <!--<a class="tool_powerconcept" href="#">PowerConcept</a>
                    <a class="tool_powerboard" href="#">PowerBoard</a>-->                                    
                 </div>
					</div>
				</div>
			</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>
campusLink;




### UI preparation for about system
if(in_array(get_client_region(), array('zh_HK', 'zh_MO')) && $_SESSION['UserType']==USERTYPE_STAFF){
	$reprintcard_link = '<a href="/home/eClassStore/redirect.php" target="_blank" class="help_reprintcard" title="'.$Lang['Header']['ReprintCardSystem'].'">'.$Lang['Header']['ReprintCardSystem'].'</a>';
	//$iportbuy_link = '<a href="/home/eClassStore/redirect.php?module=iPortfolio" target="_blank" class="help_iportbuy" title="'.$Lang['Header']['iPortbuy'].'">'.$Lang['Header']['iPortbuy'].'</a>';
}

$bubbleMsgBlockSysAbout =<<<sysAbout

	<div class="sub_layer_board" id="sub_layer_sys_about" style="visibility:hidden;z-index:100;width:259px">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<span style="width:37px;float:right">&nbsp;</span>
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_sys_about','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
			<? /* ?>
				<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock2"></span>
				</div>
			<? */ ?>
			
			<h1>{$i_eu_current_ver}<br />
                        <a href="#"><img src="/images/2020a/logo_eclass_footer.gif" align="absmiddle" border="0" /> <a href="javascript:products()"> {$eClassVersion}</a></a><br />
                        </h1>
               <div class="eclasshelp_list">
						<a href="javascript:support()" class="help_community"> {$Lang['Header']['eClassCommunity']}</a>
						<a href="javascript:onlinehelpcenter()" class="help_onlinehelp"> {$Lang['Header']['OnlineHelp']}</a>
                        <a href="javascript:doccenter()" class="help_document"> {$Lang['Header']['OnlineDocs']}</a>
                        <a href="javascript:videocenter()" class="help_video"> {$Lang['Header']['VideoTutorial']}</a>
                        {$reprintcard_link}
                        {$iportbuy_link}
                        <a href="javascript:faq()" class="help_faq"> {$Lang['Header']['FAQ']}</a>            
				</div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>

sysAbout;



### UI preparation for system timeout
if ($ControlTimeOutByJS)
{
$bubbleMsgBlockSysTimeout =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
	<span id="ajaxMsgBlockTimeOut">
				<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">
					<tr>
					<td bgcolor="orange" align="center">
					<table width="620" height="400" border="0" cellpadding="12" cellspacing="0">
						<tr>
						<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>
						&nbsp;
						</td>
						</tr>
					</table>
					</td>
					</tr>
				</table>
				</span>
	</div>

sysTimeout;



$bubbleMsgBlockSysTimeout2 =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<span style="width:37px;float:right">&nbsp;</span>
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<table width="620" height="5" border="0">
					<tr>
					<td>&nbsp;</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
				<div style="position: absolute;"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock3"></span>
				</div>
				<div id="bubble_board_content3" style="text-align:center" >
				<table width="620" height="400" border="0">
					<tr>
					<td><font size="5"><b>
					<!--System is about to time out in 60 seconds. You can press <a href="javascript:MM_showHideLayers('sub_layer_sys_timeout','','hide');CancelTimeOut()">CONTINUE</a> to cancel the timeout and continue your use in eClass.-->
					<font color="red">System is about to time out in 60 seconds.</font><br />You can press <input style="font-size:23px" type="button" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers('sub_layer_sys_timeout','','hide');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.
					</b>
					</font>
					</td>
					</tr>
				</table>
				</div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"><table width="620" height="5" border="0">
					<tr>
					<td>&nbsp;</td>
					</tr>
				</table></div>
		</div>
	</div>

sysTimeout;
}

//FOR DEVELOPMENT , HARDCODE A LOGIN WITH "xx_iportfolio_xx" SHOULD BE IPORTFOLIO STANDALONE
# MENU FOR STANDALONE VERSION OF IPORTFOLIO
if (($lu->UserLogin == "ab_iportfolio_cd" && $lu->UserPassword == "123") || (isset($stand_alone['iPortfolio']) && $stand_alone['iPortfolio']))
{

	unset($FullMenuArr);
	$SchoolSettingsMenu_i = 0;

	if($stand_alone['iesWithIPortfolio'] &&
			($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"])){

		$FullMenuArr[$SchoolSettingsMenu_i][0] = array(2, "#", $Lang['Header']['Menu']['eLearning'], $CurrentPageArr['eLearning'], 'common', "eLearning" );
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(25, "/home/eLearning/ies/", $Lang['Header']['Menu']['IES'], $CurrentPageArr['IES'],"","IES");
			$SchoolSettingsMenu_i = $SchoolSettingsMenu_i + 1;
	}



	# pls just provide school settings (without role menu) if the user is the school admin
	### School Settings
	if(	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]
	)
	{

		### eAdmin -> Account Management
	if($AccountMgmt)
	{
		$FullMenuArr[$SchoolSettingsMenu_i][0] = array(3, "#", $Lang['Header']['Menu']['eAdmin'], $CurrentPageArr['eAdmin'], 'common',"eAdmin");
		$eAdmin_i = 0;
		$FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][0] = array(38, "#", $Lang['Header']['Menu']['AccountMgmt'], $CurrentPageArr['AccountManagement']);
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(383, "/home/eAdmin/AccountMgmt/ParentMgmt/", $Lang['Header']['Menu']['ParentAccount'], $CurrentPageArr['ParentMgmt'],"","ParentMgmt");

			$FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(382, "/home/eAdmin/AccountMgmt/StaffMgmt/", $Lang['Header']['Menu']['StaffAccount'], $CurrentPageArr['StaffMgmt'],"","StaffMgmt");

			$FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(381, "/home/eAdmin/AccountMgmt/StudentMgmt/", $Lang['Header']['Menu']['StudentAccount'], $CurrentPageArr['StudentMgmt'],"","StudentMgmt");
		}
		/*
		if($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY'])))
			$FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(384, "/home/eAdmin/AccountMgmt/StudentRegistry/", $Lang['Header']['Menu']['StudentRegistry'], $CurrentPageArr['StudentRegistry']);
		*/
				$SchoolSettingsMenu_i = $SchoolSettingsMenu_i + 1;
	}


		$FullMenuArr[$SchoolSettingsMenu_i][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting',"", "SchoolSettings");
		/*
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(41, $PATH_WRT_ROOT_ABS."home/system_settings/location/", $Lang['Header']['Menu']['Site'], $CurrentPageArr['Location']);
		}
		*/
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(44, $PATH_WRT_ROOT_ABS."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class'],"","Class");
		}
		/*
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(45, $PATH_WRT_ROOT_ABS."home/system_settings/group/", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group']);
		}
		*/

		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT_ABS."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role'],"","Role");
		}

		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT_ABS."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar'],"","SchoolCalendar");
		}

		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(43, $PATH_WRT_ROOT_ABS."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subject'], $CurrentPageArr['Subjects'],"","Subjects");
		}

		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable'],"","Timetable");
		}
	}
	$menuWithIMail = false;
	$menuWithECommnuity = false;
}

$school_logo_link = $PATH_WRT_ROOT_ABS.$indexPage;

if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
 	include($PATH_WRT_ROOT."/plugins/alumni_conf.php");
	$school_logo_link = "/home/eCommunity/group/index.php?GroupID=$alumni_GroupID";
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  />
<META Http-Equiv="Cache-Control" Content="no-cache">

<? if (!$home_header_no_EmulateIE7): ?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<? endif; ?>

<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css?301_3" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css?301_1" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css?301_1" rel="stylesheet" type="text/css">

<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/brwsniff.js"></script>
<?
// code to include newer jquery library if is going to staff attendance 3 pages for case: 2010-1216-0911-56073
if (stristr($_SERVER['REQUEST_URI'],'StaffMgmt/attendance') !== false) {?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.4.4.min.js"></script>
<?}else {?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>
<?}?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.blockUI.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/js/SpryValidationTextField.js"></script>

<? /* ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.gears.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.silverlight.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.flash.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.browserplus.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.html4.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.html5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/jquery.plupload.queue/jquery.plupload.queue.php"></script>
<link rel=stylesheet href="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css">
<? */ ?>

<?php
if ($ControlTimeOutByJS)
{
?>
<script language="JavaScript">
var LogoutCountValue = <?=$TimeOutWarningSecond?>;
var SecondLogout = LogoutCountValue;
var ToLogout=false;
var SecondLeftOriginal=<?=$SessionLifeTime?>;
var SecondLeft=<?=$SessionLifeTime?>;

function CountingDown(){
	if (SecondLeft>0)
	{
	   SecondLeft -= 1;
	   setTimeout("CountingDown()",1000);
	} else
	{
		// query server to see if there is no any response from user (e.g. using AJAX)
		path = "/get_last_access.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft);

	}
}
CountingDown();


var callback_timeleft =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);

		if (TimeLeft>0)
		{
			SecondLeft = TimeLeft;
			CountingDown();
		} else
		{
			//alert("testing timeout method!")
			// notify user about the timeout
			// user can press "Continue" to cancel the timeout; if no action taken, system will (call logout page and) direct to timeout result page
			MM_showHideLayers('sub_layer_sys_timeout','','show');
			bubble_block = document.getElementById("sub_layer_sys_timeout");
			bubble_block.style.left = "0px";
			bubble_block.style.top = '0px';
			bubble_block.focus();

			ToLogout = true;
			CountDownToLogout();
		}
	}
}

function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}


function CountDownToLogout()
{
	if (ToLogout)
	{
		if (SecondLogout>0)
		{
			SecondLogout -= 1;

			var browserWidth = f_clientWidth();
			var browserHeight = f_clientHeight();

			/*
			jsTimeOutTxt = '<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">';
			jsTimeOutTxt += '		<tr>';
			jsTimeOutTxt += '		<td bgcolor="orange" align="center">';
			jsTimeOutTxt += '		<table width="'+(browserWidth-60)+'" height="'+(browserHeight-60)+'" border="0" cellpadding="12" cellspacing="0">';
			jsTimeOutTxt += '			<tr>';
			jsTimeOutTxt += '			<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>';
			jsTimeOutTxt += '			<img src="/images/2020a/alert_signal.gif" /><br /><font size="5"><b><font color="red">System is about to time out in '+SecondLogout+' seconds.</font><br />You can press <input type="button" style="font-size:23px" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.</b></font>';
			jsTimeOutTxt += '			</td></tr>';
			jsTimeOutTxt += '		</table>';
			jsTimeOutTxt += '		</td></tr>';
			jsTimeOutTxt += '	</table>';
			*/

			jsTimeOutTxt2 = '<table class="timeoutAlert"  width="'+(browserWidth)+'" height="'+(browserHeight)+'"><tr><td align="center">';
			jsTimeOutTxt2 += '<div class="contentContainer">';
			jsTimeOutTxt2 += '<div class="textTitle"><span class="timeLeft">&nbsp;<?=$Lang['SessionTimeout']['ReadyToTimeout_Header']?></span></div>';
			jsTimeOutTxt2 += '<div class="textContent"><div class="line"><span><?=$Lang['SessionTimeout']['ReadyToTimeout_str1']?> <U> '+SecondLogout+' </U><?=$Lang['SessionTimeout']['ReadyToTimeout_str2']?><br><?=$Lang['SessionTimeout']['ReadyToTimeout_str3']?> </span> <input type="button"  value="<?=$Lang['SessionTimeout']['Continue']?>" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()"> ';
			jsTimeOutTxt2 += '<span><?=$Lang['SessionTimeout']['ReadyToTimeout_str4']?></span></div> ';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</td></tr></table>';
			jsTimeOutTxt2 += '	</table>';

			document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt2;

			setTimeout("CountDownToLogout()",1000);

			// update message
		} else
		{
			// redirect to logout page
			self.location = "/logout.php?IsTimeout=1";
		}
	} else
	{
		// reset logout timer
		SecondLogout = LogoutCountValue;
	}
}



var callback_timeleft_trival =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);
	}
}



function CancelTimeOut()
{
	// stop redirect function
	SecondLogout = LogoutCountValue;
	ToLogout = false;
	alert("<?=$Lang['SessionTimeout']['CancelTimeout']?>");

	// restart counting down for time out
	SecondLeft = SecondLeftOriginal;
	CountingDown();

	var path = "/update_last_access.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft_trival);
}



</script>
<?php
}
?>

<script language="JavaScript">
<!--
function support()
{
	newWindow("<?=$cs_path?>",10);
}

function toScrabble()
{
	document.scrabble_form.method = "post";
	document.scrabble_form.action = "http://sfc.broadlearner.com/main.php?schoolName=<?=$BroadlearningClientName?>&schoolType=<?=$config_school_type?>";
	document.scrabble_form.target = "scrabbleNewWin";

	window.open("", "scrabbleNewWin", "menubar=0,toolbar=0,resizable,scrollbars,status,top=40,left=40,width=800,height=500");
	var a = window.setTimeout("document.scrabble_form.submit();", 500);
}

function products()
{
	newWindow("<?=$products_path?>",10);
}

function onlinehelpcenter()
{
	newWindow("<?=$onlinehelpcenter_path?>",24);
}

function doccenter()
{
	newWindow("<?=$doccenter_path?>",24);
}


function faq()
{
	newWindow("<?=$cs_faq?>",24);
}

function videocenter()
{
	newWindow("<?=$videocenter_path?>",10);
}
//-->
</script>

<script language="Javascript">
<!--
// slide tool
var slide_ie4 = (document.all) ? true : false;
var slide_ns4 = (document.layers) ? true : false;
var slide_ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var sliding = {MyTools:false}
var hide = "hidden";
var show = "visible";

function open_lslp()
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
	newWindow('/home/asp/lslp.php',32);
}
function open_kskgs(subject)
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
 	if(typeof(subject)!='undefined')
 		var url = '/home/asp/ksk.php?subject='+subject;
 	else
 		var url = '/home/asp/ksk.php';
 	newWindow(url,32);
}


function logout()
{
	if(confirm(globalAlertMsg16))
	{

		<?if($plugin['lslp']): ?>

                logoutscript = document.createElement('script');
                logoutscript.type = 'text/javascript';
                logoutscript.src = '<?= $lslp_httppath ?>/logout.php?action=redirect';
                document.getElementsByTagName('body')[0].appendChild(logoutscript);

                setTimeout(function(){window.location="/logout.php";}, 3000); <? //force logout if LSLP cannot load in 3 seconds?>

                <? else: ?>
                window.location="/logout.php";
                <? endif; ?>

	}
}
function slide(lay, h, speed, up) {
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                it=4;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; it=6;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left; it=10;}
        ih = parseInt(heig);

        if (up == 0){
                if (ih != h)
                        movelayer(lay, it);
                if (ih < h) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
                } else
                        setSliding(lay, 0);
        } else {
                if (ih != 25)
                        movelayer(lay, -it);
                if (ih > 25) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
                } else
                        setSliding(lay, 0);
        }
}


function setSliding(lay, on_off) {
        is_sliding = "sliding." + lay;
        is_sliding += (on_off==0) ? "=false" : "=true";
        eval (is_sliding);
}


function getSliding(lay) {
        is_sliding = "sliding." + lay;
        return eval(is_sliding);
}


function move(lay,h){
        if (getSliding(lay))
                return;
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                speed=1;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; speed=1;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left;speed=1;}
        ih = parseInt(heig);
        if (ih<=25)
                slide(lay,h,speed,0);
        else if (ih>=h)
                slide(lay,h,speed,1);
}
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function loadContent(evt,contentType){
	if (contentType == 'campus_link'){
		bubble_block = document.getElementById("sub_layer_campus_link");
		//link = document.getElementById('campusLinkico');
		//alert(document.defaultView.getComputedStyle(link,null).getPropertyValue('left'));

		bubble_block.style.left = (evt.clientX-230)+"px";

		bubble_block.style.top = '65px';
		document.getElementById("ajaxMsgBlock").innerHTML = "<?=$ip20_loading?> ...";
		document.getElementById("ajaxMsgBlock").style.display = "block";
		serverURL = "/home/campus_link/campus_link_load_content.php";
		$('div#sub_layer_campus_link div#bubble_board_content').load(serverURL,{loadType: "whole"},			function(){
			document.getElementById("ajaxMsgBlock").style.display = "none";
		});
	} else if (contentType == 'sys_about') {
		bubble_block = document.getElementById("sub_layer_sys_about");

						//var windowWidth = $(window).width();
						if( typeof( window.innerWidth ) == 'number' ) {
							//Non-IE
							myWidth = window.innerWidth;
						} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
							//IE 6+ in \'standards compliant mode\'
							myWidth = document.documentElement.clientWidth;
						} else if( document.body && ( document.body.clientWidth) ) {
							//IE 4 compatible
							myWidth = document.body.clientWidth;
						}

						//alert(myWidth);

		//bubble_block.style.left = (evt.clientX-200)+"px";
		bubble_block.style.left = (myWidth-265)+"px";

		bubble_block.style.top = '15px';
// 		document.getElementById("ajaxMsgBlock2").style.display = "block";

		//MM_showHideLayers('sub_layer_sys_about','','show');
		document.getElementById('sub_layer_sys_about').style.visibility = 'visible';


	} else if(contentType == "power_tool"){

		bubble_block = document.getElementById("sub_layer_power_tool");
		bubble_block.style.left = (evt.clientX-160)+"px";
		bubble_block.style.right = "122px";
		bubble_block.style.top = '65px';
	}
}

function emptyContent(layername)
{
	document.getElementById(layername).innerHTML = "";
}

$(window).resize(function() {
	if(document.getElementById('sub_layer_sys_about').style.visibility=="visible")
		$('#about_eclass').click();

});

//-->
</script>

<title><?=$Lang['Header']['HomeTitle']?></title>
<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
</head>
<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<?=$bubbleMsgBlock?>

<?=$bubbleMsgBlockSysAbout?>

<?=$bubbleMsgBlockSysTimeout?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<Td height="75">
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
			<td class="top_banner" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="120" rowspan="2" valign="middle" align="center">
						<div class="school_logo">&nbsp;</div>
						<a href="<?=$school_logo_link?>"><img src="<?= $logo_img?>" border="0" <?=$logo_fixed_height?>></a>
					</td>
					<td height="38" valign="top" nowrap>
						<div class="school_title"> <?= $school_name ?> </div>
						<div class="user_login">
							<img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/topbar_25/<?=$UserIdentityIcon?>" align="absmiddle"> <?=$UserIdentity?>
							<span>|</span>

							<? if($showNewFeature) {?>
							<span id="newFeatureSpan"><a title="<?=$Lang['General']['NewFeatures']?>" class="new_features"><?=$Lang['General']['NewFeatures']?></a><span>|</span></span>
							<? } ?>

							<!--<a href="javascript:support()" title="<?=$Lang['Header']['eClassCommunity']?>">?</a>-->
<?php if (get_client_region()!="zh_CN" && !$special_feature['hide_support'] && $_SESSION['UserType']==USERTYPE_STAFF) {?>
							<a href="javascript:void(0)" id="about_eclass" onclick="loadContent(event,'sys_about')" >?</a>
<?php } ?>
							<? if(!$sys_custom['hide_lang_selectioin'][$_SESSION['UserType']]) {?>
								<a href="/lang.php?lang=<?= $lang_to_change?>" title="<?=$Lang['Header']['ChangeLang']?>"><?=$change_lang_to?></a>
							<? } ?>
							<a href="javascript:logout()" title="Logout">X</a>
						</div>
					</td>
				</tr>
					<tr>
						<td valign="top" nowrap>
<?php
if (!$_SESSION['SSV_eBooking_standalone'])
{

?>
							<div id="main_menu" class="mainmenu">
								<?= $linterface->GEN_TOP_MENU_IP25($FullMenuArr);?>
							</div>
							<div class="itool_menu">
							<ul>
							<?if($menuWithIAccount !== false){?>
									<li><a href="/home/iaccount/account/" class="itool_iaccount" title="<?=$Lang['Header']['Menu']['iAccount']?>">&nbsp;</a></li>
							<?}?>
							<?if(!$plugin['DisableiCalendar'] && $menuWithICalendar !== false){?>
									<li><a href="javascript:newWindow('/home/iCalendar/',31)" class="itool_icalendar" title="<?=$icalendar_name?>">&nbsp;</a></li>
							<?}?>
							<? if($access2iFile && $_SESSION['UserType']!=USERTYPE_ALUMNI) { ?>
									<li><a href="#" class="itool_ifolder" title="<?=$Lang['Header']['Menu']['iFolder']?>" onClick="newWindow('/home/iFolder/',10); return false;">&nbsp;</a></li>
							<? } ?>
							<?
							global $SYS_CONFIG;
							if($menuWithIMail !== false){?>

									<?if($special_feature['imail']===true && $plugin['imail_gamma']!==true){?>
										<?
											$access2campusmail = $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"];
											if($access2campusmail) {
										?>
											<li><a href="/home/imail/" class="<?=$iMailClass?>" id="iMailIcon" title="<?=$Lang['Header']['Menu']['iMail']?>"></a></li>
										<?
											}
										?>
									<?}?>
									<? if($plugin['imail_gamma'] && !empty($_SESSION['SSV_EMAIL_LOGIN']) && !empty($_SESSION['SSV_LOGIN_EMAIL'])) {
										$gamma_access_right = Get_Gamma_Identity_Access_Rights();
										if(($lu->isTeacherStaff() && $gamma_access_right[0]==1) || ($lu->isStudent() && $gamma_access_right[1]==1) || ($lu->isParent() && $gamma_access_right[2]==1) || ($lu->isAlumni() && $gamma_access_right[3]==1)){
									?>
									<li><a href="/home/imail_gamma/index.php" class="itool_imail" title="<?=$Lang['Header']['Menu']['iMailGamma']?>"></a></li>
									<?}
									}?>
							<? } ?>

							<?php
								  $showJupasReadyIcon = false;
								  if (get_client_region()=='zh_HK' && $_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"] && $_SESSION['UserType']==USERTYPE_STAFF && date('Y-m-d') < '2012-03-01' && $sys_custom['iPf']['JUPASArr']['HideJupasReadyIcon']==false) {
								  		$showJupasReadyIcon = true;
								  }
							?>
								    <li>
										<?php if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"] && $_SESSION['UserType']!=USERTYPE_ALUMNI) { ?>
											<a id="iPortfolioLink" href="/home/portfolio/" class="itool_iportfolio" title="<?=$Lang['Header']['Menu']['iPortfolio']?>">&nbsp;</a>
										<?php } ?>
										<?php if ($showJupasReadyIcon) { ?>
											<img id="jupasReadyIcon" src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/topbar_25/icon_jupasready.png" align="absmiddle" style="position:absolute;display:none;">
										<?php } ?>
									</li>

							<? if(($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] || $_SESSION["SSV_PRIVILEGE"]["plugin"]['payment'] || isset($module_version['StaffAttendance']))  && $_SESSION['UserType']!=USERTYPE_ALUMNI) { ?>
									<li><a href="/home/smartcard/" class="itool_ismartcard" title="<?=$Lang['Header']['Menu']['ismartcard']?>">&nbsp;</a></li>
							<? } ?>

							<?
							/*if(in_array(get_client_region(), array('zh_HK', 'zh_MO')) && $_SESSION['UserType']==USERTYPE_STAFF && time()>= strtotime("2012-04-18")){
							?>
									<li><a href="/home/eClassStore/redirect.php" target="_blank" class="itool_reprintcard" title="����������踝蕭����������踝蕭��質�����鳴蕭����������踝蕭&nbsp;</a></li>
							<?
							}*/
							?>

							<? if($_SESSION['UserType']!=USERTYPE_ALUMNI) {?>
							<li><span>|</span></li>
							<? } ?>

							<?if(!$plugin['DisableeCommunity'] && $menuWithECommnuity !== false && $_SESSION['UserType']!=USERTYPE_ALUMNI){?>
									<li><a href="/home/eCommunity/redirect.php" class="itool_ecommunity" title="<?=$Lang['Header']['Menu']['eCommunity']?>">&nbsp;</a></li>
									<li><span>|</span></li>
							<? } ?>

							<?php
							## 2010-08-25 Sandy , Power Speech
							if($_SESSION['ss_intranet_plugin']['power_speech'] && $_SESSION['UserType']!=USERTYPE_ALUMNI) { ?>
									<li><a href="javascript:void(0)" onclick="MM_showHideLayers('sub_layer_power_tool','','show');loadContent(event,'power_tool');" class="itool_powertool" title="<?=$Lang['Header']['Menu']['PowerTool']?>">&nbsp;</a></li>
									<li><span>|</span></li>
							<?php } ?>

							<? if($_SESSION['UserType']!=USERTYPE_ALUMNI) {?>
								<li><a href="javascript:void(0)" onclick="MM_showHideLayers('sub_layer_campus_link','','show');loadContent(event,'campus_link')" class="itool_campus_link" title="<?=$Lang['Header']['Menu']['CampusLinks']?>">&nbsp;</a></li>
							<? } ?>

							</ul>
							</div>
<?php
}
?>
						</td>
					</tr>
			</table></td>
	  	</tr>
	</table>
	</td>
</tr>
<tr>
<td valign="top">

<?php
		/* We've got fresh content stored in $data! */
		$cached_hh_data = ob_get_contents();
		if ($IsHeaderCaching)
		{
			$Cache_Lite->save($cached_hh_data, $id);

		}
		ob_get_clean();
	}

	echo $cached_hh_data;
}

################################################################################################# IMPORTANT
# lighten current menu (for cached method)
# [based on $CurrentPageArr], you have to add only if you have added new sub-menu!!!

if ($IsHeaderCaching)
{
	global $linterface, $special_feature, $module_version,$stand_alone;
	if (is_array($CurrentPageArr) && sizeof(($CurrentPageArr)>0))
	{
		foreach($CurrentPageArr as $PageMenuKey => $MenuEnabled)
		{
			if ($MenuEnabled && $PageMenuKey!="eService" && $PageMenuKey!="DRC" && $PageMenuKey!="eLearning" && $PageMenuKey!="eAdmin" && $PageMenuKey!="SchoolSettings")
			{
				$jsCommands = "";

				# lighten sService
				if ($PageMenuKey=="eServiceCampusTV" || $PageMenuKey=="eServiceeBooking" || $PageMenuKey=="eServiceCircular" || $PageMenuKey=="eServiceeDisciplinev12"
					 || $PageMenuKey=="eServiceeEnrolment"  || $PageMenuKey=="eServiceHomework"  || $PageMenuKey=="eServiceNotice"
					  || $PageMenuKey=="eServiceeReportCard"  || $PageMenuKey=="eServiceSportDay"  || $PageMenuKey=="eServiceSwimmingGala"
					  || $PageMenuKey=="eServiceeSurvey"  || $PageMenuKey=="PagePolling"  || $PageMenuKey=="eServiceRepairSystem"
					  || $PageMenuKey=="ResourcesBooking"  || $PageMenuKey=="SLSLibrary" || $PageMenuKey=="eServiceTimetable")
				{
					$jsCommands .= "changeClass(\"eService\", \"common_current\");\n";
				}


				# lighten eLibrary
				if ($PageMenuKey=="eLib" || $PageMenuKey=="eLibPlus" || $PageMenuKey=="eLearningReadingGarden"  || $PageMenuKey=="DigitalArchive_SubjectReference")
				{
					$jsCommands .= "changeClass(\"DRC\", \"current_module\");\n";
					$flagOn['DRC'] = true;
				}

				# lighten eLearning
				if ($flagOn['DRC'] || $PageMenuKey=="eClass" || $PageMenuKey=="eLC" || $PageMenuKey=="IES" || $PageMenuKey=="iTextbook" ||
					($PageMenuKey=="DigitalArchive_SubjectReference" && $_SESSION['UserType']==USERTYPE_STUDENT) || $PageMenuKey=="SBA" ||
					$PageMenuKey == "WWSeLearningProject" )
				{
					$jsCommands .= "changeClass(\"eLearning\", \"common_current\");\n";
				}


				# lighten Account Management
				if ($PageMenuKey=="ParentMgmt" || $PageMenuKey=="StaffMgmt" || $PageMenuKey=="StudentMgmt"
					 || $PageMenuKey=="StudentRegistry"  || $PageMenuKey=="SharedMailBox" || $PageMenuKey=="AlumniMgmt")
				{
					$jsCommands .= "changeClass(\"AccountManagement\", \"current_module\");\n";
					$flagOn['AccountManagement'] = true;
				}

				# lighten General Management
				if ($PageMenuKey=="ePayment" || $PageMenuKey=="ePOS" || $PageMenuKey=="eAdmineSurvey"
					 || $PageMenuKey=="ePolling"  || $PageMenuKey=="schoolNews" || $PageMenuKey=="eAdmineMassMailing")
				{
					$jsCommands .= "changeClass(\"GeneralManagement\", \"current_module\");\n";
					$flagOn['GeneralManagement'] = true;
				}

				# lighten Staff Management
				if ($PageMenuKey=="eAdminStaffAttendance" || $PageMenuKey=="eAdminCircular")
				{
					$jsCommands .= "changeClass(\"StaffManagement\", \"current_module\");\n";
					$flagOn['StaffManagement'] = true;
				}

				# lighten eSports
				if ($PageMenuKey=="SportDay" || $PageMenuKey=="SwimmingGala")
				{
					$jsCommands .= "changeClass(\"eSports\", \"current_module\");\n";
					$flagOn['eSports'] = true;
				}

				# lighten Student Management
				if ($flagOn['eSports'] || $PageMenuKey=="StudentAttendance" || $PageMenuKey=="eDisciplinev12" || $PageMenuKey=="eEnrolment"
					 || $PageMenuKey=="eAdminHomework"  || $PageMenuKey=="eAdminNotice" || $PageMenuKey=="eReportCard"  || $PageMenuKey=="eReportCard_Rubrics")
				{
					$jsCommands .= "changeClass(\"StudentManagement\", \"current_module\");\n";
					$flagOn['StudentManagement'] = true;
				}

				# lighten Resources Management
				if ($PageMenuKey=="eBooking" || ($PageMenuKey=="DigitalArchive" && $_SESSION['UserType']==USERTYPE_STAFF) ||  ($PageMenuKey=="DocRouting" && $_SESSION['UserType']==USERTYPE_STAFF) || $PageMenuKey=="eInventory"
					 || $PageMenuKey=="eAdminRepairSystem"
					 || $PageMenuKey=="InvoiceMgmtSystem"
					)
				{
					$jsCommands .= "changeClass(\"ResourcesManagement\", \"current_module\");\n";
					$flagOn['ResourcesManagement'] = true;
				}

				# lighten eAdmin
				if ($flagOn['AccountManagement'] || $flagOn['GeneralManagement'] || $flagOn['StaffManagement']
					 || $flagOn['StudentManagement']  || $flagOn['ResourcesManagement'])
				{
					$jsCommands .= "changeClass(\"eAdmin\", \"common_current\");\n";
				}


				# lighten School Settings
				if ($PageMenuKey=="Location" || $PageMenuKey=="Class" || $PageMenuKey=="Group" || $PageMenuKey=="Role"
					 || $PageMenuKey=="SchoolCalendar"  || $PageMenuKey=="Subjects"  || $PageMenuKey=="Timetable")
				{
					$jsCommands .= "changeClass(\"SchoolSettings\", \"setting_current\");\n";
				}

				if ($PageMenuKey=="Home")
				{
					$ClassNameChangeTo = "home_current";
				} elseif ($PageMenuKey=="SchoolSettings")
				{
					$ClassNameChangeTo = "setting_current";
				} else
				{
					$ClassNameChangeTo = "current_module";
				}

				$jsCommands .= "changeClass(\"".$PageMenuKey."\", \"".$ClassNameChangeTo."\");\n";
			}
		}
	}
	if (!$CurrentPageArr['Home'])
	{
		$jsCommands .= "changeClass(\"Home\", \"home\");\n";
	}


	# update iMail alert icon
	if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] && !isset($iNewCampusMail))
	{
	    # check any new message from DB
	    include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
	    if (!isset($header_lc))
	    {
	    	$header_lc = new libcampusmail();
	    }

		# no need to count for iMail Gamma/Plus
		if (!$plugin['imail_gamma'])
		{
		    if ($special_feature['imail'])
		        $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
		    else
		    	$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);
		}

		# only check webmail if there is no new mail from DB!
		if ($iNewCampusMail<=0 && !$plugin['imail_gamma'])
		{
			include_once($PATH_WRT_ROOT."includes/libwebmail.php");
			if (!isset($lwebmail))
			{
				$lwebmail = new libwebmail();
			}

		    # Check preference of check email
		    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
		    $temp = $header_lc->returnArray($sql,1);
		    $optionSkipCheckEmail = $temp[0][0];
		    if ($optionSkipCheckEmail)
		    {
		        $skipCheck = true;
		    }
		    else
		    {
		        $skipCheck = false;
		    }

			if (!$skipCheck && $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] && $lwebmail->type==3 && !$_SESSION["EmailLoginFailure".$lu->UserLogin])
			{
			    if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
			    {
			        $exmail_count = $lwebmail->checkMailCount();
			        $lwebmail->close();
			        $iNewCampusMail += $exmail_count;
			    } else
			    {
			    	# mark the user's email authenticate
			    	# and don't connect to mail server next time (to avoid slow performance)
			    	$_SESSION["EmailLoginFailure".$lu->UserLogin] = true;
			    }
			}
		}
	}

	if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] || isset($iNewCampusMail))
	{
		$iMailClass = ($iNewCampusMail>0) ? "itool_imail_new" : "itool_imail";
		$jsCommands .= "changeClass(\"iMailIcon\", \"{$iMailClass}\");\n";
	}

}

?>

<script language="javascript" defer="1">
$(document).ready( function() {
	<?php if ($showJupasReadyIcon) { ?>
		$(window).resize(function() {
			relocateJupasReadyIcon();
		});

		relocateJupasReadyIcon();
	<?php } ?>
});

function relocateJupasReadyIcon() {
	var ipf_link_pos = $('a#iPortfolioLink').position();
	var ipf_link_height = $('a#iPortfolioLink').height();
	$('img#jupasReadyIcon').css('top', ipf_link_pos.top - ipf_link_height + 6).css('left', ipf_link_pos.left - 28).show();
}

function changeClass(objID, classname)
{
	if ($('#'+objID).length>0)
	{
		$('#'+objID).attr("class",classname);
	}
}
<?=$jsCommands?>
<?php

if (!$_SESSION['Is_First_Login'])
{
?>
if ($('#'+'newFeatureSpan').length>0)
{
	$('#'+'newFeatureSpan').hide();
}
<?php
}
?>
</script>