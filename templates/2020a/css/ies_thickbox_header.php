<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv='pragma' content='no-cache' charset="utf-8" />
<?= returnHtmlMETA() ?>

<title><?= $i_home_title ?></title>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2020a/css/IES.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>templates/2020a/css/IES_font_L.css" rel="stylesheet" type="text/css">

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/IES.js"></script>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">