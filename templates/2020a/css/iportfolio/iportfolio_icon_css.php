<?php
$PATH_WRT_ROOT = "../../../../";
$image_path = "images/";
$LAYOUT_SKIN = "2020a";
?>

@charset "utf-8";
/* CSS Document */
.icon_stu_acc{
	color: #000000;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_acc_off.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}
.icon_stu_acc_on{
	color: #000000;
	font-weight:bold;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_acc_on.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}

.icon_stu_acc:hover{
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_stu_acc_on.gif);
}

.icon_LP{
	color: #2b8105;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_LP_off.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}
.icon_LP_on{
	color: #2b8105;
	font-weight:bold;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_LP_on.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}

.icon_addon{
	color: #000000;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_addon_off.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}

.icon_addon_on{
	color: #000000;
	font-weight:bold;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_addon_on.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}

.icon_addon:hover{
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_addon_on.gif);

}

.icon_LP:hover{
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_LP_on.gif);
}

.icon_scheme{
	color: #9b3b07;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_scheme_off.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}
.icon_scheme_on{
	color: #9b3b07;
	font-weight:bold;
	display: block;
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_scheme_on.gif);
	background-repeat: no-repeat;
	background-position: 0px 0px;
}

.icon_scheme:hover{
	background-image: url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/icon_scheme_on.gif);

}

.scheme_act_btn_activate{
	background-image:url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/btn_activate.gif);

}

.scheme_act_btn_deactivate{
	background-image:url(<?=$PATH_WRT_ROOT?><?=$image_path?><?=$LAYOUT_SKIN?>/iPortfolio/btn_deactivate.gif);

}