<?
## Using By : yat

/********************** Change Log ***********************/
#
#	Date	:	2010-03-30 [YatWoon]
#	Detail	:	Add "Repair System" menu eAdmin>ResourceMgmt>Repair System
#
#	Date	:	2010-03-23 [Fai]
#	Detail	:	IP25 Support with standalone iPortfolio $stand_alone['iPortfolio']
#
# 	Date	:	2010-02-25 [Yuen]
#	Details	:	complete function of the timeout control using AJAX and JS (UI to be revised later)
#
# 	Date	:	2010-02-19 [YatWoon]
#	Details	:	add $intranet_default_lang = $_SESSION['intranet_default_lang'];
#
# 	Date	:	2009-12-15 [YatWoon]
#	Details	:	Apply top menu access checking for School Settings
#
# 	Date	:	2009-12-10 [Yuen]
#	Details	:	improve webmail checking logic to avoid checking if once fails to login to email server
#               (avoid slow performance)
#
# 	Date	:	2009-12-05 [Yuen]
#	Details	:	testing the timeout control using AJAX and JS (to be continued... for UI)
#
/******************* End Of Change Log *******************/

// $PATH_WRT_ROOT = "../../../";
if (is_file("$intranet_root/templates/2020a/layout/home_header.customized.php"))
{
   include_once ("$intranet_root/templates/2020a/layout/home_header.customized.php");
}
else
{

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libportal.php");
if (is_file($PATH_WRT_ROOT."includes/version.php"))
{
	$JustWantVersionData = true;
	include_once($PATH_WRT_ROOT."includes/version.php");
	$eClassVersion = $versions[0][0];
} else
{
	$eClassVersion = "--";
}
global $linterface, $special_feature, $module_version,$stand_alone;
$lu = new libuser($UserID);


# Yuen: temp
$ControlTimeOutByJS = $special_feature['TimeOutByJS'];

//$ControlTimeOutByJS = true;
if ($ControlTimeOutByJS || $special_feature['TimeOutByJS'])
{
	# session max life time in second
	//$SessionLifeTime = ini_get('session.gc_maxlifetime')-120;
	# below is just for quick testing
	$SessionLifeTime = ini_get('session.gc_maxlifetime')-1435;
}


# Basic Information
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
$intranet_default_lang = $_SESSION['intranet_default_lang'];
if ($intranet_default_lang == "gb")
	($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
else
	($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];

if (isset($_SESSION["SSV_PRIVILEGE"]["school"]["logo"]))
{
        $logo_img = $_SESSION["SSV_PRIVILEGE"]["school"]["logo"];
}
else
{
        $imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
        $logo_img = ($imgfile == "" ?"{$image_path}/2007a/topbar/eclass_logo.gif" : "/file/$imgfile");
        $_SESSION["SSV_PRIVILEGE"]["school"]["logo"] = $logo_img;
}
if (!strstr($logo_img, "topbar_25/eclass_logo.gif"))
{
	$logo_fixed_height = " height='55' ";
}

$icalendar_name = $plugin["iCalendarFull"]?$iCalendar_iCalendar :$iCalendar_iCalendarLite;


# get the URL of this site
$PageURLArr = split("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}

# eClass eCommunity data
$cs_path = "$intranet_httppath/home/support.php";

$products_path = "http://version-dev.eclass.com.hk/?eclasskey=".$lu->sessionKey."&eClassURL=".$eClassURL;

/* YuEn: temp
global $TestNow;
if ($TestNow)
{
	$TimeNow = time();
	debug(date("Y-m-d-H", $TimeNow));
	debug(date("Y-m-d-H", $TimeNow-3600));
	debug(date("Y-m-d-H", $TimeNow+3600));
	$products_path = "http://version-dev.eclass.com.hk/?eclasskey=".md5(date("Y-m-d-H", $TimeNow))."&eClassURL=".$eClassURL."&eClassCheck=1";
}
*/

# user identity
switch($_SESSION['UserType'])
{
	case USERTYPE_STAFF:
		$UserIdentity = $_SESSION['isTeaching']==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
		$UserIdentityIcon = $_SESSION['isTeaching']==1 ? "icon_teacher.gif" : "icon_nonteacher.gif";
		break;
	case USERTYPE_STUDENT:
		$UserIdentity = $Lang['Identity']['Student'];
		$UserIdentityIcon = "icon_student.gif";
		break;
	case USERTYPE_PARENT:
		$UserIdentity = $Lang['Identity']['Parent'];
		$UserIdentityIcon = "icon_parent.gif";
		break;
	default:
		$UserIdentity = "";
		$UserIdentityIcon = "10x10.gif";
		break;
}


# Build Menu
unset($FullMenuArr);

# Check menu focus
# Home
$CurrentPageArr['Home'] = 1;

# eService
if($CurrentPageArr['eServiceCircular'] || $CurrentPageArr['eServiceHomework'] ||
	$CurrentPageArr['eServiceNotice'] || $CurrentPageArr['eServiceeEnrolment'] ||
	$CurrentPageArr['eServiceeDisciplinev12'] || $CurrentPageArr['eServiceeSurvey'] ||
	$CurrentPageArr['eServiceRepairSystem']
	)
{
	$CurrentPageArr['Home'] = 0;
	$CurrentPageArr['eService'] = 1;
}

if ($CurrentPageArr['PagePolling']){
	$CurrentPageArr['eService'] = 1;
	$CurrentPageArr['Home'] = 0;
}

if ($CurrentPageArr['SLSLibrary']){
	$CurrentPageArr['eService'] = 1;
	$CurrentPageArr['Home'] = 0;
}

# eLearning
if($CurrentPageArr['eClass'] || $CurrentPageArr['eLC'] || $CurrentPageArr['eLib']){
  $CurrentPageArr['eLearning'] = 1;
	$CurrentPageArr['Home'] = 0;
}

# eAdmin
# ---------- Genearl Management
if ($CurrentPageArr['ePolling'] || $CurrentPageArr['ePayment'] || $CurrentPageArr['schoolNews'] || $CurrentPageArr['eAdmineSurvey'] || $CurrentPageArr['ePOS'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	# GeneralManagement
	$CurrentPageArr['GeneralManagement'] = 1;
}


# ---------- Staff Management
if ($CurrentPageArr['eAdminCircular'] || $CurrentPageArr['eAdminStaffAttendance'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	# Staff Management
	$CurrentPageArr['StaffManagement'] = 1;
}

# ---------- Student Management
#eSports, Swimming Gala
if ($CurrentPageArr['eSports'] || $CurrentPageArr['eAdminNotice'] || $CurrentPageArr['eAdminHomework'] || $CurrentPageArr['StudentAttendance'] || $CurrentPageArr['eReportCard'] || $CurrentPageArr['eEnrolment'] || $CurrentPageArr['eDisciplinev12'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	$CurrentPageArr['StudentManagement'] = 1;
}

# ---------- Resources Management
if($CurrentPageArr['eInventory'] || $CurrentPageArr['eBooking'] || $CurrentPageArr['eAdminRepairSystem'])
{
	$CurrentPageArr['eAdmin'] = 1;
	$CurrentPageArr['Home'] = 0;

	$CurrentPageArr['ResourcesManagement'] = 1;
}

if($CurrentPageArr['eServiceeSports'])
{
	$CurrentPageArr['eService'] = 1;
	$CurrentPageArr['Home'] = 0;
}

# School Settings
if ($CurrentPageArr['Location'] || $CurrentPageArr['Role'] || $CurrentPageArr['Subjects'] || $CurrentPageArr['Class'] || $CurrentPageArr['Group'] || $CurrentPageArr['SchoolCalendar'] || $CurrentPageArr['Timetable'] || $CurrentPageArr['Period'])
{
	$CurrentPageArr['Home'] = 0;
	$CurrentPageArr['SchoolSettings'] = 1;
}


# === $MenuArr : array fields description====
#	1 - menu id
#	2 - path / #
#	3 - section name
#	4 - section focus flag
#	5 - css (will change the icon)

### Home
$FullMenuArr[0] = array(0, "/home/index.php", $Lang['Header']['Menu']['Home'], $CurrentPageArr['Home'], 'home');

### eService
$FullMenuArr[1][0] = array(1, "#", $Lang['Header']['Menu']['eService'], $CurrentPageArr['eService'], 'common');
$eService_i = 0;

### eService -> Campus TV
if($plugin['tv'])
{
	$FullMenuArr[1][1][$eService_i] = array(15, "javascript:newWindow(\"/home/plugin/campustv/info.php\",8)", $Lang['Header']['Menu']['CampusTV'], $CurrentPageArr['eServiceCampusTV']);
	$eService_i++;
}

### eService -> eBooking
if($plugin['eBooking'])
{
	$FullMenuArr[1][1][$eService_i] = array(14, "#", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eBooking']);
	$eService_i++;
}

### eService -> eCircular
if(!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION['UserType']==USERTYPE_STAFF)
{
	$FullMenuArr[1][1][$eService_i] = array(11, "/home/eService/circular/", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eServiceCircular']);
	$eService_i++;
}

### eService -> eDiscipline
if(($_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT )&& $plugin['Disciplinev12'] &&!empty($_SESSION['SESSION_ACCESS_RIGHT']))
{
	$FullMenuArr[1][1][$eService_i][0] = array(19, "/home/eService/disciplinev12", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eServiceeDisciplinev12']);
	$eService_i++;
}

### eService -> eEnrolment
if($plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT))
{
	$FullMenuArr[1][1][$eService_i] = array(19, "/home/eService/enrollment/", $Lang['Header']['Menu']['eEnrolment'], $CurrentPageArr['eServiceeEnrolment']);
	$eService_i++;
}

### eService -> eHomework
//if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
	if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"])))	{
	$FullMenuArr[1][1][$eService_i] = array(12, "/home/eService/homework/index.php", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eServiceHomework']);
	$eService_i++;
}

### eService -> eNotice
if($plugin['notice'] && !$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"])
{
	$FullMenuArr[1][1][$eService_i] = array(11, "/home/eService/notice/", $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eServiceNotice']);
	$eService_i++;
}

### eService -> eRC
if($plugin['ReportCard2008'])
{
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	$libreportcard = new libreportcard();

	// check if the user is class/subject teacher in the ACTIVE year of eRC
	$Is_ClassTeacher = $libreportcard->Is_Class_Teacher($_SESSION['UserID']);
	$Is_SubjectTeacher = $libreportcard->Is_Subject_Teacher($_SESSION['UserID']);

	if ($Is_ClassTeacher || $Is_SubjectTeacher || $_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)
	{
		$FullMenuArr[1][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard']);
		$eService_i++;
	}
}

### eService -> eSports
if($_SESSION["UserType"]==2 && ($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"]||$_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"]))
{
	$FullMenuArr[1][1][$eService_i][0] = array(18, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eServiceeSports']);
	if($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"])
		$FullMenuArr[1][1][$eService_i][1][] = array(181, "/home/eService/eSports/sports/", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['eServiceSportDay']);
	if($_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"])
		$FullMenuArr[1][1][$eService_i][1][] = array(182, "/home/eService/eSports/swimming_gala/", $Lang['Header']['Menu']['SwimmingGala'], $CurrentPageArr['eServiceSwimmingGala']);
	$eService_i++;
}

### eService -> eSurvey
	$FullMenuArr[1][1][$eService_i] = array(17, "/home/eService/eSurvey/index.php", $Lang['Header']['Menu']['eSurvey'], $CurrentPageArr['eServiceeSurvey']);
	$eService_i++;

### eService -> Poll
if($_SESSION['UserType']!=USERTYPE_PARENT)
{
	$FullMenuArr[1][1][$eService_i] = array(17, "/home/eService/polling/index.php", $Lang['Header']['Menu']['ePolling'], $CurrentPageArr['PagePolling']);
	$eService_i++;
}

### eService -> Repair System
if($plugin['RepairSystem'])
{
	$FullMenuArr[1][1][$eService_i] = array(17, "/home/eService/RepairSystem/", $Lang['Header']['Menu']['RepairSystem'], $CurrentPageArr['eServiceRepairSystem']);
	$eService_i++;
}


### eService -> Resource Booking
if($plugin['ResourcesBooking'])
{
	$access2eServiceeBooking = false;
	if (isset($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"]) and $_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
	{
		$access2eServiceeBooking = true;
	}
	else
	{
		if($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
		{
			$access2eServiceeBooking = true;
		}
	}
	if($access2eServiceeBooking){
		$FullMenuArr[1][1][$eService_i] = array(183, "javascript: newWindow(\"/home/eService/ResourcesBooking\",\"8\");", $Lang['Header']['Menu']['ResourcesBooking'], $CurrentPageArr['ResourcesBooking']);
		$eService_i++;
	}
}

# eService -> SLS Library
if(isset($_SESSION["SSV_PRIVILEGE"]["library"]["access"]) && $_SESSION["SSV_PRIVILEGE"]["library"]["access"])
{
        $header_library_access = $_SESSION["SSV_PRIVILEGE"]["library"]["access"];
}
else
{
        if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]["library"]) && $_SESSION["SSV_PRIVILEGE"]["plugin"]["library"])
        {
                    global $mapping_file_path;

                    $content = get_file_content("$mapping_file_path");
                    $records = explode("\n",$content);
					include_once ($PATH_WRT_ROOT."/includes/libuser.php");

                    $header_lu = new libuser($UserID);
                    $login = $header_lu->UserLogin;

                    $header_library_access = false;

                    for ($i=0; $i<sizeof($records); $i++)
                    {
                         $data = split("::",$records[$i]);
                         if (strtolower($login) == strtolower($data[0]))
                         {
                             $header_library_access = true;
                             break;
                         }
                    }
        }
        else
        {
            $header_library_access = false;
        }

        $_SESSION["SSV_PRIVILEGE"]["library"]["access"] = $header_library_access;


}
if($header_library_access){
	$FullMenuArr[1][1][$eService_i] = array(20, "/home/eService/sls_library_index.php", $Lang['SLSLibrary'], $CurrentPageArr['SLSLibrary']);
	$eService_i++;
}
# end of SLS Library

### eLearning
$FullMenuArr[2][0] = array(2, "#", $Lang['Header']['Menu']['eLearning'], $CurrentPageArr['eLearning'], 'common');
$FullMenuArr[2][1][] = array(21, "/home/eLearning/eclass/", $Lang['Header']['Menu']['eClass'], $CurrentPageArr['eClass']);
if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2SpecialRm"])
{
  $FullMenuArr[2][1][] = array(22, "/home/eLearning/elc/", $Lang['Header']['Menu']['eLC'], $CurrentPageArr['eLC']);
}
if ($plugin['eLib'])
{
	$FullMenuArr[2][1][] = array(23, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib']);
}
if ($plugin['lslp'])
{
	include_once ($PATH_WRT_ROOT."includes/liblslp.php");
	$ls = new lslp();
	# to be improved to control on users (i.e. particular students, teachers)
	if($ls->hasLSLPAccess() && $ls->isInLicencePeriod())
	{
		$FullMenuArr[2][1][] = array(24,"javascript:open_lslp();", $Lang['lslp']['lslp'], $CurrentPageArr['LSLP']);
	}
}


### eAdmin
###### check display General Management
if(
	($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"]) ||
	($plugin['payment'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"]) || 
	($plugin['payment'] && $plugin['ePOS'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) || 
	($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"]) ||
	($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"]) 
	)
	$GeneralMgmt = 1;
###### check display Staff Management
if(
	($special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]))) ||
	($module_version['StaffAttendance'] == 3.0 && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"]))
	)
	//($plugin['attendancestaff'])
	$StaffMgmt = 1;

###### check display Student Management

## Special checking for eEnrolment
$show_eAdmin_eEnrol = false;
if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();

	if (isset($header_lu) == false)
	{
		include_once ($PATH_WRT_ROOT."/includes/libuser.php");
		$header_lu = new libuser($_SESSION['UserID']);
	}

	if ($header_lu->isStudent() == false && $header_lu->isParent() == false)
	{
		if ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']) || $libenroll->IS_CLUB_PIC() || $libenroll->IS_EVENT_PIC())
			$show_eAdmin_eEnrol = true;
	}
}
## End Special checking for eEnrolment
if (
		($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) ||
		($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) ||
		($plugin['Disciplinev12'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) || $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"]) ||
		($plugin['eEnrollment'] && $show_eAdmin_eEnrol) ||
		($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION['isTeaching']))) ||
		($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) ||
		($plugin['ReportCard2008'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"]) ||
		($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) ||
		($plugin['swimmming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"])
	)
	$StudentMgmt = 1;

	//($plugin['RepairSystem'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"])
###### check display Resources Management
if(
	($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] && $_SESSION["eBooking"]["role"] != "ADMIN")) ||
	($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")) ||
	($plugin['RepairSystem'])
){
	$ResourcesMgmt = 1;
}

if($GeneralMgmt || $StaffMgmt || $StudentMgmt || $ResourcesMgmt)
{
	$FullMenuArr[3][0] = array(3, "#", $Lang['Header']['Menu']['eAdmin'], $CurrentPageArr['eAdmin'], 'common');

	$eAdmin_i = 0;

	### eAdmin -> GeneralManagement
	if($GeneralMgmt)
	{
		$FullMenuArr[3][1][$eAdmin_i][0] = array(34, "#", $Lang['Header']['Menu']['GeneralManagement'], $CurrentPageArr['GeneralManagement']);

		### eAdmin -> GeneralManagement -> ePayment
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $plugin['payment'])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(344, "/home/eAdmin/GeneralMgmt/payment/", $Lang['Header']['Menu']['ePayment'], $CurrentPageArr['ePayment']);
		}
		
		### eAdmin -> GeneralManagement -> ePayment
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $plugin['ePOS'] && $plugin['payment'])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(345, "/home/eAdmin/GeneralMgmt/pos/", $Lang['Header']['Menu']['ePOS'], $CurrentPageArr['ePOS']);
		}

		### eAdmin -> GeneralManagement -> eSurvey
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(343, "/home/eAdmin/GeneralMgmt/eSurvey/", $Lang['Header']['Menu']['eSurvey'] , $CurrentPageArr['eAdmineSurvey']);
		}

		### eAdmin -> GeneralManagement -> ePolling
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(341, "/home/eAdmin/GeneralMgmt/polling/", $Lang['Header']['Menu']['ePolling'], $CurrentPageArr['ePolling']);
		}

		### eAdmin -> GeneralManagement -> schoolNews
		if($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"]){
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(342, "/home/eAdmin/GeneralMgmt/schoolnews/", $Lang['Header']['Menu']['schoolNews'], $CurrentPageArr['schoolNews']);
		}



		$eAdmin_i++;
	}

	### eAdmin -> StaffManagement
	if($StaffMgmt)
	{
		$FullMenuArr[3][1][$eAdmin_i][0] = array(31, "#", $Lang['Header']['Menu']['StaffManagement'], $CurrentPageArr['StaffManagement']);

		/*
		if($plugin['attendancestaff'])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(311, "#", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StaffeAttednance']);
		}
		*/

		if($special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])))
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(312, "/home/eAdmin/StaffMgmt/circular/", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eAdminCircular']);
		}

		// staff attendance v3
		if ($module_version['StaffAttendance'] == 3.0 && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"])) {
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(313, "/home/eAdmin/StaffMgmt/attendance/", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['eAdminStaffAttendance']);
		}

		$eAdmin_i++;
	}

	### eAdmin -> StudentManagement
	if ($StudentMgmt) {
		$eAdmin_StudentMgmt_i = 0;
		$FullMenuArr[3][1][$eAdmin_i][0] = array(32, "#", $Lang['Header']['Menu']['StudentManagement'], $CurrentPageArr['StudentManagement']);

		### eAdmin -> StudentManagement -> eAttendance
		if (($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) || ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(321, "/home/eAdmin/StudentMgmt/attendance/", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StudentAttendance']);
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eDis
		if($plugin['Disciplinev12'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) || $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(322, "/home/eAdmin/StudentMgmt/disciplinev12/overview", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eDisciplinev12']);
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eEnrolment
		if($plugin['eEnrollment'])
		{
			if ($show_eAdmin_eEnrol)
			{
				$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(324, "/home/eAdmin/StudentMgmt/enrollment/", $Lang['Header']['Menu']['eEnrolment'], $CurrentPageArr['eEnrolment']);
				$eAdmin_StudentMgmt_i++;
			}
		}

		### eAdmin -> StudentManagement -> eHomework
		//if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"])))
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION['isTeaching'])))
		{
			$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(325, "/home/eAdmin/StudentMgmt/homework/index.php", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eAdminHomework']);
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eNotice
		if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
		{
			$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(327, "/home/eAdmin/StudentMgmt/notice/", $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eAdminNotice']);
			//$FullMenuArr[3][1][1][1][4][1][] = array(3276, "#", "test sub sub sub menu XD", $CurrentPageArr['eNotice']);	# pls don't delete, ref usage
			$eAdmin_StudentMgmt_i++;
		}

		### eAdmin -> StudentManagement -> eRC
		if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"] && $plugin['ReportCard2008'])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(323, "/home/redirect.php?mode=1&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard']);
			$eAdmin_StudentMgmt_i++;
		}
		### eAdmin -> StudentManagement -> eSports
		if(($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) || ($plugin['swimmming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"]))
		{
			$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][0] = array(326, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eSports']);
			if($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"])
				$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3261, "/home/eAdmin/StudentMgmt/eSports/sports/index.php", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['SportDay']);
			if($plugin['swimming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"])
				$FullMenuArr[3][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3262, "/home/eAdmin/StudentMgmt/eSports/swimming_gala/index.php", $Lang['Header']['Menu']['SwimmingGala'], $CurrentPageArr['SwimmingGala']);
			$eAdmin_StudentMgmt_i++;
		}
		$eAdmin_i++;
	}

	### eAdmin -> ResourcesManagement
	if($ResourcesMgmt)
	{
		$FullMenuArr[3][1][$eAdmin_i][0] = array(32, "#", $Lang['Header']['Menu']['ResourcesManagement'], $CurrentPageArr['ResourcesManagement']);

		### eAdmin -> ResourcesManagement -> eBooking
		if($plugin['eBooking'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/eBooking", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eBooking']);
		}

		### eAdmin -> ResourcesManagement -> eInventory
		if($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != ""))
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(331, "/home/eAdmin/ResourcesMgmt/eInventory", $Lang['Header']['Menu']['eInventory'], $CurrentPageArr['eInventory']);
		}
		
		### eAdmin -> ResourcesManagement -> Repair System
		//if($plugin['RepairSystem'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"]))
		if($plugin['RepairSystem'])
		{
			$FullMenuArr[3][1][$eAdmin_i][1][] = array(331, "/home/eAdmin/ResourcesMgmt/RepairSystem", $Lang['Header']['Menu']['RepairSystem'], $CurrentPageArr['eAdminRepairSystem']);
		}
	}

	$SchoolSettingsMenu_i = 4;
}
else
	$SchoolSettingsMenu_i = 3;

### School Settings
if(	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
	$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]
)
{
	$FullMenuArr[$SchoolSettingsMenu_i][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting');
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(41, $PATH_WRT_ROOT."home/system_settings/location/", $Lang['Header']['Menu']['Site'], $CurrentPageArr['Location']);
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(44, $PATH_WRT_ROOT."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class']);
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(45, $PATH_WRT_ROOT."home/system_settings/group/", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group']);
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role']);
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar']);
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(43, $PATH_WRT_ROOT."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subject'], $CurrentPageArr['Subjects']);
	}
	if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"])
	{
		$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT."home/system_settings/timetable/", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable']);
	}
	//$FullMenuArr[4][1][] = array(48, $PATH_WRT_ROOT."home/system_settings/period/", $Lang['Header']['Menu']['Period'], $CurrentPageArr['Period']);
}

if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] && !isset($iNewCampusMail))
{
    # check any new message from DB
    include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
    $header_lc = new libcampusmail();

    if ($special_feature['imail'])
        $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
    else
    	$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);

	# only check webmail if there is no new mail from DB!
	if ($iNewCampusMail<=0)
	{
		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
		$lwebmail = new libwebmail();

	    # Check preference of check email
	    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
	    $temp = $header_lc->returnArray($sql,1);
	    $optionSkipCheckEmail = $temp[0][0];
	    if ($optionSkipCheckEmail)
	    {
	        $skipCheck = true;
	    }
	    else
	    {
	        $skipCheck = false;
	    }

		if (!$skipCheck && $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] && $lwebmail->type==3 && !$_SESSION["EmailLoginFailure".$lu->UserLogin])
		{
		    if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
		    {
		        $exmail_count = $lwebmail->checkMailCount();
		        $lwebmail->close();
		        $iNewCampusMail += $exmail_count;
		    } else
		    {
		    	# mark the user's email authenticate
		    	# and don't connect to mail server next time (to avoid slow performance)
		    	$_SESSION["EmailLoginFailure".$lu->UserLogin] = true;
		    }
		}
	}
}
# determine iMail icon for display
$iMailClass = ($iNewCampusMail>0) ? "itool_imail_new" : "itool_imail";


### Check any iFolder ###
$access2iFile = false;
if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]['aerodrive']) && $_SESSION["SSV_PRIVILEGE"]["plugin"]['aerodrive'])
{
        $access2iFile = true;
}

if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]['personalfile']) && $_SESSION["SSV_PRIVILEGE"]["plugin"]['personalfile'])
{
    if ($personalfile_type == 'AERO')
    {
        #$moving_layer_text .= $link_personalFiles;
        #$layer_length -= 25;
    }
    else
    {
        include_once("$intranet_root/includes/libftp.php");
        include_once ($PATH_WRT_ROOT."includes/libsystemaccess.php");
        $header_lsysacc = new libsystemaccess($UserID);
        $header_lftp = new libftp();
        if ($header_lsysacc->hasFilesRight())
        {
            $access2iFile = true;
        }
    }
}

### UI preparation
$bubbleMsgBlock =<<<campusLink

	<div class="sub_layer_board" id="sub_layer_campus_link" style="visibility:hidden;z-index:100">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br  style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_campus_link','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a></div>
			</div>
			<div class="bubble_board_03">
				<div class="bubble_board_04">
					<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock"></span>
					</div>
					<div id="bubble_board_content">

					</div>
				</div>
			</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>

campusLink;


### UI preparation for about system
$bubbleMsgBlockSysAbout =<<<sysAbout

	<div class="sub_layer_board" id="sub_layer_sys_about" style="visibility:hidden;z-index:100;width:259px">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<span style="width:37px;float:right">&nbsp;</span>
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_sys_about','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
				<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock2"></span>
				</div>
				<div id="bubble_board_content2" style="text-align:center">
					{$i_eu_current_ver}: <a href="javascript:products()">{$eClassVersion}</a><br><br>
					<a href="javascript:support()">{$Lang['Header']['eClassCommunity']}</a><br>
				</div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>

sysAbout;



### UI preparation for system timeout
if ($ControlTimeOutByJS)
{
$bubbleMsgBlockSysTimeout =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
	<span id="ajaxMsgBlockTimeOut">
				<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">
					<tr>
					<td bgcolor="orange" align="center">
					<table width="620" height="400" border="0" cellpadding="12" cellspacing="0">
						<tr>
						<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>
						&nbsp;
						</td>
						</tr>
					</table>
					</td>
					</tr>
				</table>
				</span>
	</div>

sysTimeout;



$bubbleMsgBlockSysTimeout2 =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<span style="width:37px;float:right">&nbsp;</span>
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<table width="620" height="5" border="0">
					<tr>
					<td>&nbsp;</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
				<div style="position: absolute;"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock3"></span>
				</div>
				<div id="bubble_board_content3" style="text-align:center" >
				<table width="620" height="400" border="0">
					<tr>
					<td><font size="5"><b>
					<font color="red">System is about to time out in 60 seconds.</font><br />You can press <input style="font-size:23px" type="button" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers('sub_layer_sys_timeout','','hide');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.
					</b>
					</font>
					</td>
					</tr>
				</table>
				</div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"><table width="620" height="5" border="0">
					<tr>
					<td>&nbsp;</td>
					</tr>
				</table></div>
		</div>
	</div>

sysTimeout;
}

# MENU FOR STANDALONE VERSION OF IPORTFOLIO
if (isset($stand_alone['iPortfolio']) && $stand_alone['iPortfolio'])
{

	unset($FullMenuArr);
	$SchoolSettingsMenu_i = 0;
	# pls just provide school settings (without role menu) if the user is the school admin 
	### School Settings
	if(	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
		$_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]
	)
	{
		$FullMenuArr[$SchoolSettingsMenu_i][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting');
		/*
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(41, $PATH_WRT_ROOT."home/system_settings/location/", $Lang['Header']['Menu']['Site'], $CurrentPageArr['Location']);
		}
		*/
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(44, $PATH_WRT_ROOT."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class']);
		}
		/*
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(45, $PATH_WRT_ROOT."home/system_settings/group/", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group']);
		}
		*/
		/*
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role']);
		}
		*/
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar']);
		}
		/*
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(43, $PATH_WRT_ROOT."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subject'], $CurrentPageArr['Subjects']);
		}
		*/
		if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"])
		{
			$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT."home/system_settings/timetable/", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable']);
		}
		//$FullMenuArr[4][1][] = array(48, $PATH_WRT_ROOT."home/system_settings/period/", $Lang['Header']['Menu']['Period'], $CurrentPageArr['Period']);
	}
	$menuWithIMail = false;
	$menuWithECommnuity = false;
}

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/js/SpryValidationTextField.js"></script>

<?php
if ($ControlTimeOutByJS)
{
?>
<script language="JavaScript">
var LogoutCountValue = 120;
var SecondLogout = LogoutCountValue;
var ToLogout=false;
var SecondLeftOriginal=<?=$SessionLifeTime?>;
var SecondLeft=<?=$SessionLifeTime?>;

function CountingDown(){
	if (SecondLeft>0)
	{
	   SecondLeft -= 1;
	   setTimeout("CountingDown()",1000);
	} else
	{
		// query server to see if there is no any response from user (e.g. using AJAX)
		//path = "/get_last_access.php";
		path = "/get_last_access_yat2.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft);

	}
}
CountingDown();


var callback_timeleft =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);
		
		if (TimeLeft>0)
		{
			SecondLeft = TimeLeft;
			CountingDown();
		} else
		{
			//alert("testing timeout method!")
			// notify user about the timeout
			// user can press "Continue" to cancel the timeout; if no action taken, system will (call logout page and) direct to timeout result page
			MM_showHideLayers('sub_layer_sys_timeout','','show');		
			bubble_block = document.getElementById("sub_layer_sys_timeout");
			bubble_block.style.left = "0px";
			bubble_block.style.top = '0px';
			bubble_block.focus();
		
			ToLogout = true;
			CountDownToLogout();
		}
	}
}

function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}


function CountDownToLogout()
{
	if (ToLogout)
	{
		if (SecondLogout>0)
		{
			SecondLogout -= 1;

			var browserWidth = f_clientWidth();
			var browserHeight = f_clientHeight(); 
			
			/*
			jsTimeOutTxt = '<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">';
			jsTimeOutTxt += '		<tr>';
			jsTimeOutTxt += '		<td bgcolor="orange" align="center">';
			jsTimeOutTxt += '		<table width="'+(browserWidth-60)+'" height="'+(browserHeight-60)+'" border="0" cellpadding="12" cellspacing="0">';
			jsTimeOutTxt += '			<tr>';
			jsTimeOutTxt += '			<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>';
			jsTimeOutTxt += '			<img src="/images/2020a/alert_signal.gif" /><br /><font size="5"><b><font color="red">System is about to time out in '+SecondLogout+' seconds.</font><br />You can press <input type="button" style="font-size:23px" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.</b></font>';
			jsTimeOutTxt += '			</td></tr>';
			jsTimeOutTxt += '		</table>';
			jsTimeOutTxt += '		</td></tr>';
			jsTimeOutTxt += '	</table>';
			*/
			
			jsTimeOutTxt2 = '<table class="timeoutAlert"  width="'+(browserWidth)+'" height="'+(browserHeight)+'"><tr><td align="center">';
			jsTimeOutTxt2 += '<div class="contentContainer">';
			jsTimeOutTxt2 += '<div class="textTitle"><span class="timeLeft"><?=$Lang['SessionTimeout']['ReadyToTimeout_Header']?></span></div>';
			jsTimeOutTxt2 += '<div class="textContent"><div class="line"><span><?=$Lang['SessionTimeout']['ReadyToTimeout_str1']?> <U> '+SecondLogout+' </U><?=$Lang['SessionTimeout']['ReadyToTimeout_str2']?><br><?=$Lang['SessionTimeout']['ReadyToTimeout_str3']?> </span> <input type="button"  value="<?=$Lang['SessionTimeout']['Continue']?>" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()"> ';
			jsTimeOutTxt2 += '<span><?=$Lang['SessionTimeout']['ReadyToTimeout_str4']?></span></div> ';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</td></tr></table>';
			jsTimeOutTxt2 += '	</table>';
			
			//document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt; 
			document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt2; 

			setTimeout("CountDownToLogout()",1000);
			// update message
		} else
		{
			// redirect to logout page
			self.location = "/logout.php?IsTimeout=1&lg=<?=$intranet_session_language?>";
		}
	} else
	{
		// reset logout timer
		SecondLogout = LogoutCountValue;
	}
}

function CancelTimeOut()
{
	// stop redirect function
	SecondLogout = LogoutCountValue;
	ToLogout = false;
	alert("<?=$Lang['SessionTimeout']['CancelTimeout']?>");

	// restart counting down for time out
	SecondLeft = SecondLeftOriginal;
	CountingDown();
}
</script>
<?php
}
?>

<script language="JavaScript" type="text/JavaScript">
<!--
function support()
{
	newWindow("<?=$cs_path?>",10);
}

function products()
{
	newWindow("<?=$products_path?>",10);
}
//-->
</script>

<script language="Javascript">
<!--
// slide tool
var slide_ie4 = (document.all) ? true : false;
var slide_ns4 = (document.layers) ? true : false;
var slide_ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var sliding = {MyTools:false}
var hide = "hidden";
var show = "visible";

function open_lslp()
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
	newWindow('/home/asp/lslp.php',32);
}
function logout()
{
	if(confirm(globalAlertMsg16))
	{
		<?if($plugin['lslp']) {?>
		newWindow('<?=$lslp_httppath?>/logout.php',31);
		<?}?>
		window.location="/logout.php";
	}
}
function slide(lay, h, speed, up) {
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                it=4;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; it=6;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left; it=10;}
        ih = parseInt(heig);

        if (up == 0){
                if (ih != h)
                        movelayer(lay, it);
                if (ih < h) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
                } else
                        setSliding(lay, 0);
        } else {
                if (ih != 25)
                        movelayer(lay, -it);
                if (ih > 25) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
                } else
                        setSliding(lay, 0);
        }
}


function setSliding(lay, on_off) {
        is_sliding = "sliding." + lay;
        is_sliding += (on_off==0) ? "=false" : "=true";
        eval (is_sliding);
}


function getSliding(lay) {
        is_sliding = "sliding." + lay;
        return eval(is_sliding);
}


function move(lay,h){
        if (getSliding(lay))
                return;
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                speed=1;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; speed=1;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left;speed=1;}
        ih = parseInt(heig);
        if (ih<=25)
                slide(lay,h,speed,0);
        else if (ih>=h)
                slide(lay,h,speed,1);
}
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function loadContent(evt,contentType){
	if (contentType == 'campus_link'){
		bubble_block = document.getElementById("sub_layer_campus_link");
		//link = document.getElementById('campusLinkico');
		//alert(document.defaultView.getComputedStyle(link,null).getPropertyValue('left'));

		bubble_block.style.left = (evt.clientX-230)+"px";

		bubble_block.style.top = '65px';
		document.getElementById("ajaxMsgBlock").innerHTML = "<?=$ip20_loading?> ...";
		document.getElementById("ajaxMsgBlock").style.display = "block";
		serverURL = "/home/campus_link/campus_link_load_content.php";
		$('div#sub_layer_campus_link div#bubble_board_content').load(serverURL,{loadType: "whole"},			function(){
			document.getElementById("ajaxMsgBlock").style.display = "none";
		});
	} else if (contentType == 'sys_about') {
		bubble_block = document.getElementById("sub_layer_sys_about");

		bubble_block.style.left = (evt.clientX-200)+"px";

		bubble_block.style.top = '15px';
		document.getElementById("ajaxMsgBlock2").style.display = "block";
	}
}
//-->
</script>

<title><?=$Lang['Header']['HomeTitle']?></title>
<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="<?=$header_onload_js?>">
<?=$bubbleMsgBlock?>

<?=$bubbleMsgBlockSysAbout?>

<?=$bubbleMsgBlockSysTimeout?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<Td height="75">
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr>
			<td class="top_banner" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="120" rowspan="2" valign="middle" align="center">
						<div class="school_logo">&nbsp;</div>
						<a href="<?=$PATH_WRT_ROOT?>home/index.php"><img src="<?= $logo_img?>" border="0" <?=$logo_fixed_height?>></a>
					</td>
					<td height="38" valign="top" nowrap>
						<div class="school_title"> <?= $school_name ?> </div>
						<div class="user_login">
							<img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/topbar_25/<?=$UserIdentityIcon?>" align="absmiddle"> <?=$UserIdentity?>
							<span>|</span>
							<!--<a href="javascript:support()" title="<?=$Lang['Header']['eClassCommunity']?>">?</a>-->
							<a href="javascript:void(0)" onclick="MM_showHideLayers('sub_layer_sys_about','','show');loadContent(event,'sys_about')" >?</a>
							<a href="/lang.php?lang=<?= $lang_to_change?>" title="<?=$Lang['Header']['ChangeLang']?>"><?=$change_lang_to?></a>
							<a href="javascript:logout()" title="Logout">X</a>
						</div>
					</td>
				</tr>
					<tr>
						<td valign="top" nowrap>
							<div id="main_menu" class="mainmenu">
								<?= $linterface->GEN_TOP_MENU_IP25($FullMenuArr);?>
							</div>
							<div class="itool_menu">
							<ul>
							<?if($menuWithIAccount !== false){?>
									<li><a href="/home/iaccount/account/" class="itool_iaccount" title="<?=$Lang['Header']['Menu']['iAccount']?>">&nbsp;</a></li>
							<?}?>
							<?if($menuWithICalendar !== false){?>
									<li><a href="javascript:newWindow('/home/iCalendar/',31)" class="itool_icalendar" title="<?=$icalendar_name?>">&nbsp;</a></li>
							<?}?>
							<? if($access2iFile) { ?>
									<li><a href="#" class="itool_ifolder" title="<?=$Lang['Header']['Menu']['iFolder']?>" onClick="newWindow('/home/iFolder/',10); return false;">&nbsp;</a></li>
							<? } ?>
							<?
							if($menuWithIMail !== false){?>
									<li><a href="/home/imail/" class="<?=$iMailClass?>" title="<?=$Lang['Header']['Menu']['iMail']?>"></a></li>
									<? if($plugin['imail_gamma'] && !empty($_SESSION['SSV_EMAIL_LOGIN']) && !empty($_SESSION['SSV_LOGIN_EMAIL'])) { ?>
									<li><a href="/home/imail_gamma/index.php" class="itool_imail" title="<?=$Lang['Header']['Menu']['iMailGamma']?>"></a></li>
									<?}?>
							<? } ?>

							<?php if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"]) { ?>
									<li><a href="/home/portfolio/" class="itool_iportfolio" title="<?=$Lang['Header']['Menu']['iPortfolio']?>">&nbsp;</a></li>
							<?php } ?>
							<? if($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] || $_SESSION["SSV_PRIVILEGE"]["plugin"]['payment'] || isset($module_version['StaffAttendance'])) { ?>
									<li><a href="/home/smartcard/" class="itool_ismartcard" title="<?=$Lang['Header']['Menu']['ismartcard']?>">&nbsp;</a></li>
							<? } ?>
								<li><span>|</span></li>
							<?if($menuWithECommnuity !== false){?>
									<li><a href="/home/eCommunity/" class="itool_ecommunity" title="<?=$Lang['Header']['Menu']['eCommunity']?>">&nbsp;</a></li>
									<li><span>|</span></li>
							<? } ?>

								<li><a href="javascript:void(0)" onclick="MM_showHideLayers('sub_layer_campus_link','','show');loadContent(event,'campus_link')" class="itool_campus_link" title="<?=$Lang['Header']['Menu']['CampusLinks']?>">&nbsp;</a></li>
							</ul>
							</div>
						</td>
					</tr>
			</table></td>
	  	</tr>
	</table>
	</td>
</tr>
<tr>
<td valign="top">

<? } ?>