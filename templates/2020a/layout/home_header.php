<?php
## Using By :

## IMPORTANT IMPORTANT IMPORTANT:
##			If you add a new menu, please go to "lighten current menu (for cached method)" to add your newly added menu to lighten the parent menu.
##-------------------------------------------------------------------------------------------------------------------------------------------------

/********************** Change Log ***********************/
#
#   Date:       2020-09-07 [Bill]   [2020-0604-1821-16170]
#                   - seperate issue right checking for pages display
#
#   Date:       2020-06-16 [Cameron]
#                   - show Organization Info if $sys_custom['SchoolSettings']['OrganizationInfo'] is true or $intranet_version > 2.5
#
#   Date:       2020-05-12 [Cameron]
#                   - fix to set top menu focus to eAdmin > Medical Caring System for medical module when staff login
#
#   Date:       2020-04-17 [Cameron]
#                   - don't show organization_info menu for Amway, Oaks and CEM because they are already customized before
#
#   Date:       2020-03-26 [Cameron]
#                   - consolidate Amway, Oaks, CEM to use StandardLMS
#                   - add menu item: organization_info
#
#   Date:       2020-02-07 [Sam]
#                   - hide eClass Community [2020-0130-1356-59066]
#
#   Date:       2019-11-18 [Henry]
#                   set $hide_menu_now = true if the page is digital channels admin settings in powerclass
#
#   Date:       2019-10-31 [Cameron]
#                   customize for LivingHomeopathy [case #P166116]:
#                   - include jquery-3.3.1.min.js, exclude jquery.blockUI.js for LivingHomeopathy portal page
#                   - add meta tag for device viewport
#
#	Date:		2019-10-18 [Philips]
#					- allow class teacher access eAdmin -> GeneralManagement -> eClass App
#
#   Date:       2019-10-16 [Henry]
#                   - add flag $sys_custom['ePOS']['enablePurchaseAndPickup'] to control showing ePOS in eService or not
#
#   Date:       2019-10-10 [Cameron]
#                   - add flag $sys_custom['ePOS']['exclude_webview'] to control showing ePOS in eService or not
#
#   Date:       2019-09-02 [Ray]
#                   - add IMail CKEditor header
#
#   Date:       2019-08-28 [Henry]
#                   - changed $doccenter_path
#
#   Date:       2019-08-20 [Cameron]
#                   - add ePOS in eService
#
#   Date:       2019-08-16 [Henry]
#               - added School Settings > System Security for KIS client
#
#   Date:       2019-08-14 [Cameron]
#               - allow class teacher and subject teacher to view corresponding student's SEN record even they are not granted right in permission settings [case #M158130]
#
#   Date:       2019-08-02 [Bill]
#               - added special handling for Pui Ching eRC KG ($isPageExcludeCaching)
#
#   Date:       2019-07-18 [Henry]
#               - $sys_custom['KIS_SchoolSettings']['DisableCampus'] - for School Settings > Campus in KIS
#               - $sys_custom['KIS_SchoolSettings']['DisableGroup'] - for School Settings > Group in KIS
#               - $sys_custom['KIS_SchoolSettings']['DisableSchoolCalendar'] - for School Settings > School Calendar in KIS
#               - $sys_custom['KIS_SchoolSettings']['DisableTimetable'] - for School Settings > Timetable in KIS
#
#   Date:       2019-05-30 [Bill]
#               - added $sys_custom['eSports']['StudentReviewEnroledEvents'] > always show "eService > eSports > Sports Day"
#
#   Date:       2019-05-28 [Bill]
#               add 2 eReportCard KG pages to use EmulateIE9
#
#   Date:       2019-03-25 [Cameron]
#               add student registry linking to $KIS_MENUS [Y140316]
#
#	Date:		For IE11 and at iMail+ compose page, output <meta http-equiv="X-UA-Compatible" content="IE=Edge" /> when using CKEditor.
#				For IE11 and at iMail+ compose page, output <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" /> when using fckeditor.
#
#   Date:       2019-03-18 [Philips]
#               - added $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_admin_group_member"]
#
#   Date:       2019-01-03 [Isaac]
#               - removed LSLP cust logout redirect
#
#   Date:       2019-01-02 [Anna]
#               - ADDED $sys_custom['eEnrolment']['HideEnrolForParentView']> hide enrollment button for parent
#
#	Date: 	    2018-12-20 [Pun]
#				- Fixed Flipped Channels always show in '?'
#
#	Date: 	    2018-12-11 [Bill]
#				- eService > eReportCard (Kindergarten), added Parent access right
#
#	Date:		2018-12-06 [Carlos]
#				$sys_custom['HideiSmartCardForUserTypes'] - hide iSmartCard icon button at portal header for certain user types.
#
#   Date:       2018-11-12 [Cameron]
#               - consolidate Amway, Oaks, CEM to use the same customized flag control ($sys_custom['project']['CourseTraining']['IsEnable'])
#               - add case $sys_custom['project']['CourseTraining']['CEM']
#
#   Date:       2018-09-04 [Bill]   [2018-0628-1634-01096]
#               - eAdmin > Student Mgmt > eSports > Sports Day, allow Kao Yip Class Teacher access  ($_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"])
#
#   Date:       2018-08-31 [Cameron] [ip.2.5.9.10.1]
#               map System Security linking to login_records.php for HKPF
#
#   Date:       2018-08-27 [Ivan] [X145096] [ip.2.5.9.10.1]:
#               added customization $sys_custom['eEnrolment']['ClassTeacherView'] to let class teacher enter eEnrolment
#
#   Date:       2018-08-13 [Cameron] [ip.2.5.9.10.1]
#               show eEnrol Admin for HKPF general staff so that they can manage self-schedule
#
#   Date:       2018-07-11 [Ivan] [ip.2.5.9.7.1]:
#               applied flag $sys_custom['ePost']['hiddenForParent'] and $sys_custom['subject_resources_hidden_for_parent'] to hide ePost and Subject eResources for parent account
#
#   Date:       2018-06-25 [Cameron]:
#               use newer jquery version for library_sys/reports/frequency_report, [case #S139804 scroll bar cannot move back to top for select all]
#
#	Date: 	    2018-06-11 [Bill]
#				- eAdmin > Student Management > eReportCard (Kindergarten), added Class Teacher and Subject Teacher access right
#
#   Date:       2018-05-10 (Isaac)
#               Added $sys_custom['PowerClass'] to $IsHeaderCaching
#
#	Date:		2018-04-24 (Pun)
#				Added $special_feature['hide_iAccount']
#
#	Date:		2018-03-27 (Pun)
#				Hide customize '?' for Flipped Channels
#
#	Date:		2017-12-18 (Cameron)
#				customize menu for Oaks if $sys_custom['project']['Oaks'] == true
#
#	Date:		2017-12-06 (Pun)
#				Added hide Parent account manage for NCS cust
#
#	Date:		2017-11-03 (Cameron)
#				re-position eGuidance in the menu according to the Alphabet order of module name (English)
#
#	Date:		2017-10-04 (Carlos)
#				$sys_custom['StudentAttendance']['HostelAttendance'] - added hostel attendance admin for taking all groups attendance.
#
#	Date:		2017-09-26 (Anna)
#				added eAdmin access right for enrollment helper
#
#	Date:		2017-09-13 (Bill)
#				$sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] - for School Settings > Subject in KIS
#
#	Date:		2017-09-04 (Bill)
#				$sys_custom['eNotice']['HideParenteNotice'] - for eAdmin > student Mgmt > eNotice - redirect to Student Notice
#
#	Date:		2017-09-01 (Carlos)
#				$sys_custom['StudentAttendance']['HostelAttendance'] added [eService > Hostel Attendance] for non-student attendance admin staff users.
#
#	Date:		2017-08-08 (Paul)
#				Add Scrabble Fun form to make it accessible in all IP pages
#
#	Date:		2017-08-01 (Carlos)
#				Make the Campus Location Setting menu generally available. (open for KIS)
#
#	Date:		2017-06-29 (Paul)
#				added $Lang['Header']['Menu']['AuditLog'] for AccountMgmt
#
#	Date:		2017-06-07 (Pun)
#				added $sys_custom['iAccount']['editPasswordOnly'] for iAccount url
#
#	Date:		2017-05-22 (Omas) #X110568
#				revise url for https on nginx
#
#	Date:		2017-05-15 (Bill)	[2017-0502-1024-21096]
#				Allow View Group Member to access eAdmin > Student Mgmt > eReportCard
#
#	Date:		2017-03-22 (Paul)
#				Display KIS-used header for NCS project
#
#	Date:		2017-03-20 (Cameron)
#				Add module eGuidance in Student Management
#
#	Date:		2017-03-06 (Frankie Leung)
#				Add customized $plugin['eAppraisal'] eAppraisal menu
#
#	Date:		2016-12-19 (Frankie Leung)
#				Add customized $plugin['TeacherPortfolio'] teacher portfolio menu
#
#	Date:		2016-12-09 (Ivan) [ip.2.5.8.1.1]
#				Add customized $sys_custom['eRC']['Settings']['DisableVerificationForParent'] to disable eRC menu for parent account
#
#	Date:		2016-12-05 (Ivan) [A109140] [ip.2.5.8.1.1]
#				Add customized flag $sys_custom['eNotice']['eServiceDisableParent'] and $sys_custom['eNotice']['eServiceDisableStudent'] to disable parents and students enter eNotice
#
#	Date:		2016-12-02 (Cameron)
#				Add customized flag $sys_custom['RepairSystem']['GrantedUserIDArr'] to hardcode user for accessing eService -> Repair System
#
#	Date:		2016-11-11 (Cameron)
#				Add customized flag $sys_custom['RepairSystemBanAccessForStudent'] to ban access of eService -> Repair System for student
#
#	Date:		2016-10-17 [Cameron] [ip.2.5.7.10.1]
#				- add $plugin['eLibraryPlusOne'], link to elibplus2 (new windows tab) if it's not set, else link to elibplus
#
#	Date:		2016-10-12 [Ivan] [ip.2.5.7.10.1]
#				- added $plugin['eAppraisal'] teacher appraisal menu
#
#	Date:		2016-09-27 [Omas] [M104893 ] [ip.2.5.7.10.1]
#				- hide eCommunity if $plugin['DisableeCommunityForParent']
#
#	Date:		2016-08-04 [Ivan] [F99388] [ip.2.5.7.9.1]
#				- fixed menu generation logic for $special_feature['lite_for_eAdmin']
#
#	Date:		2016-07-29 [Kenneth] [ip.2.5.7.9.1]
#				- added module eSchoolBus
#
#	Date:		2016-05-16 [Ivan] [ip.2.5.7.7.1]
#				- added module SLRS in Staff Mgmt menu
#
#	Date:		2016-04-15 [Siuwan] [ip.2.5.7.4.1]
#				- remove UserType_Staff checking for PowerFlip
#
#	Date:		2016-04-01 [Omas]
#				- added eProcurement
#
#	Date:		2015-12-15 [Cameron]
#				- shift left menu to top if $sys_custom['project']['Amway'] is set
#				- create AmwayModuleMenu
#
#	Date:		2015-12-11 [Paul] [ip.2.5.7.1.1]
#				added new checking on powervoice expiration
#
#	Date:		2015-12-11 [Cameron]
#				add $sys_custom['project']['Amway'] to hide top bar menu
#
#	Date:		2015-10-28 [Ivan] [ip.2.5.7.1.1]
#				added subject panel access right for eService > eReportCard
#
#	Date:		2015-09-16 [Ivan] [ip.2.5.6.10.1]
#				add Timetable settings for KIS with eBooking module
#
#	Date:		2015-08-21 [Siuwan] [ip.2.5.6.9.1]
#				add eLearning > PowerFlip
#
#	Date:		2015-08-05 [Cameron]
#				add variable $sys_custom['HideTopBarMenu'] to hide top bar menu
#
#	Date:		2015-06-10 [Siuwan] [ip.2.5.6.7.1]
#				add eLearning > Flipped Channels
#
#	Date:		2015-05-15 [Yuen]
#				add customized banner of system header
#
#	Date:		2015-03-20 [Cameron]
#				Add flag $sys_custom['eAdmin']['AcctMgmt']['DisableParentModule'] to hide Parent Account management if it's set
#
#	Date:		2015-03-19 [Cameron]
#				Add flag $sys_custom['HideeClass'] to hide eClass module if it's set
#
#	Date:		2015-03-05 [Pun]
#				Fixed teacher without general management cannot see cust MSSCH_printing module
#
#	Date:		2014-12-11 [Ivan]
#				Added app message group admin access right
#				Deploy: ip.2.5.6.1.1
#
#	Date:		2014-11-17 [Henry]
#				Modified permission access to Digital Channels (deploy: IPv12.1)
#
#	Date:		2014-11-14 [Pun]
#				Move printing module by alphabetical order
#
#	Date:		2014-11-11 [Pun]
#				added cust printing module for mssch in eAdmin
#
#	Date:		2014-10-23 [Roy]
#				added checking for class teacher / club & activity PIC send push message right to message center
#
#	Date:		2014-09-15 [Ryan]
#				switch Stuent/Parent AccountManagement into sis mode if $sys_custom['SISUserManagement'] enabled
#
# 	Date	: 	2014-10-17 [Henry Chan]
#				Display of "Digital Channels" (only check $plugin['DigitalChannels'] at this moment) deploy: IPv10.1
#
#	Date:		2014-09-19 [Adam]
#				added a new name for Writing 2.0 (easyWriting) if the flag $plugin['W2_oldName'] is disabled
#
#	Date:		2014-09-17 [YatWoon]
#				fixed: typo swimming_gala as swimmming_gala, so that failed to display menu [Case#X68106]
#				deploy: IPv10.1
#
#	Date:		2014-08-08 [YatWoon]
#				added checking for $stand_alone['eAdmission'], display "School calendar" only for KIS platform
#
#	Date:		2014-07-25 [Pun]
#				added checking for "eService > Medical" for student Log parent view version 3
#
#	Date:		2014-07-21 [Pun]
#				added checking for "eService > Medical" for student Log parent view version 2
#
#	Date:		2014-07-18 [Pun]
#				added checking for "eService > Medical" for student Log parent view
#
#	Date:		2014-07-15 [Ivan]
#				added eClass Teacher App access right for "General Mgmt > eClass App" menu display
#
#	Date:		2014-07-08 [Ivan]
#				added "Teacher App push message" access right for message center menu display
#
#	Date:		2014-07-08 [Yuen]
#				cater SLS encoding (UCS-2LE) with a new config $sls_mapping_encoding which can be set in plugins/sls_conf.php

#	Date:		2014-04-24 [Ivan]
#				added "eClass App" into "eAdmin > General Mgmt"
#
#	Date:		2014-03-05 [YatWoon]
#				Improved: add checking for get_client_region()=="zh_MY" || "zh_CN", select lang optioin: Eng/SC
#
#	Date:		2014-02-27 [YatWoon]
#				Fixed: display eService > eDiscipline missing to check only for eDiscipline access right
#
#	Date:		2014-02-12 [Fai]
#				Display Medical module in eAdmin
#
#	Date:		2014-01-26 [Cameron]
#				add Medical Module in eService
#
#	Date:		2013-12-18 [YatWoon]
#				change eclass default school logo
#
#	Date	:	2013-12-07 [Fai]
#				add Medical Module eAdmin > Student Management > "Medical"
#
#	Date	:	2013-10-08 [Ivan]
#				Fixed: in eAdmin > Resources Management, show in sequence Digital Archive > Document Routing > eBooking now
#	Date	:	2013-08-31 [Yuen]
#				Generate KIS module menus in another way (i.e. don't follow IP 2.5!)  IMPORTANT~~~!!!!!!!!~~~~~
#				Disable cache for KIS !
#	Date	:	2013-08-23 [Carlos]
#				display iSmartcard icon if have Lesson Attendance access right
#
#	Date	:	2013-07-29 [Mick]
#				add conditions for hiding "School Settings > Campus" , "School Settings > Subjects" if platform is KIS
#
#	Date	:	2013-07-03 [YatWoon]
#				improve top menu display checking, cater with UCCKE online reg logic
#
#	Date	:	2013-06-07 [Yuen]
#				only show eAdmin and School Settings to System Admin for KIS version
#				IMPORTANT: only support account management and school settings
#
#	Date	:	2013-04-30 [YatWoon]
#				add checking for the UCCKE parent online reg process, force user redirect to the online reg index
#
#	Date	:	2013-04-29 [YatWoon]
#				add parent_reg.css for UCCKE online reg
#
#	Date	:	2013-04-26 [Ivan] (2013-0425-1702-48156)
#				fixed cannot show Document Routing module button if the user has no eAdmin menu originally
#
#	Date	:	2013-04-19 [Yuen]
#				add eSurvey icon for alumni (the icon is similiar to iMail 1.2 icon)
#
#	Date	:	2013-04-16 [YatWoon]
#				add eLib+ flag checking ($plugin['library_management_system']) to check the eLib+ permission
#
#	Date	:	2013-04-02 [YatWoon]
#				add checking for $sys_custom['OnlineRegistry'], hide menu if within the online reg period
#
#	Date	: 	2013-03-26 [Mick]
#				Hide eLibrary Plus is it is 'Closed' in eLibrary Plus settings
#	Date	: 	2013-03-21 [yuen]
#				support KIS by hidding the menu and use "eClass" as HTML title
#
#	Date	: 	2013-02-05 [yuen]
#				improve for better cache ID to cater src code update, language and also concurrent logins!
#
#	Date	: 	2013-02-05 [Rita]
#				add access setting (verification period) checking for eService - eReportCard
#
#	Date	:	2013-01-25 [Yuen]
#				allow School Admin to change UI language and other users can't
#
#	Date	:	2013-01-17 [Thomas]
#				Display back to PowerLesson button if the user is login from PowerLesson Login Page
#
#   	Date    :       2012-11-27 [Yuen]
#                               use another PHP to redirect to version release note site
#
#   	Date    :       2012-11-06 [Mick]
#                               added $home_header_no_EmulateIE7 to cancel meta emulateIE7
#                               also $home_header_no_EmulateIE7 will cancel the caching of header
#	Date	: 	2012-10-30 [Jason]
#				improved JS toScrabble() to pass user_type via Post method
#
#	Date 	: 	2012-10-08 [Rita]
#				added Document Routing menu
#
# 	Date	: 	2012-09-20 [Yuen]
#				added eLib + (should replace eBook when it is completed)
#
# 	Date	: 	2012-09-06 [YatWoon]  >>>> should be fixed on 2011-06-21 [Henry Chow]
#				display "eHomework" in menu when current user is eHomework Admin, even eHomework module is "disabled"
#
#	Date	: 	2012-09-04 [Ivan]
#				modified the link of "School Settings > Group" to add param "?clearCoo=1"
#
#	Date	:	2012-09-03 [YatWoon]
#				comment out those "/templates/jquery/plupload/" js setting
#
#	Date	:	2012-08-31 [Ivan]
#				modified meta: enabled back <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" /> for parent account mgmt step 2 yahoo ajax
#
#	Date	:	2012-08-31 [YatWoon]
#				add eService>eNotice parent checking (if no child, hide the menu)
#
#	Date	:	2012-08-30 [Carlos]
#				added "eService > Staff Attendance System"
#
#	Date	:	2012-08-14 [Ivan] (2012-0814-1021-26073)
#				fixed system did not cater the case of student as an eEnrol admin
#
#	Date	:	2012-08-14 [Carlos]
#				added "eService > Class Diary"
#				added "eAdmin > Student Management > Class Diary"
#
#	Date	:	2012-07-14 [Jason]
#				added "eLearning > Wong Wha San eLearning Project"
#				added PageMenuKey 'WWSeLearningProject' for $CurrentPageArr
#
#	Date	:	2012-06-19 [Henry Chow]
#				added "eLearning > SBA"
#
#	Date	:	2012-05-21 [Ivan] [2012-0516-1551-30066]
#				Improved: hide "eService > eBooking" if the user has no right for booking
#
#	Date	:	2012-05-18 [YatWoon]
#				Improved: add checking for China client, hidden "support ?"
#
#	Date	:	2012-04-17 [YatWoon]
#				Fixed: failed to display "General Mgmt" if user only got "Message Center" [Case#2012-0417-1518-17066]
#
#	Date	:	2012-03-29 [Carlos]
#				Illiminate webmail connection if using imail gamma because new mail will be counted by ajax
#
#	Date	:	2012-03-19 [Yuen]
#				Update access time once after user cancells the logout prompt
#
#	Date	:	2012-02-20 [fai]
#				Update chuenyuen access right , relate to sys_custom['chuen_yuen_award_scheme']
#
#	Date	:	2012-02-17 [YatWoon]
#				Improved: eBook & Reading scheme, only allow student and staff access only
#
#	Date	:	2012-02-10 [YatWoon]
#				update ehomework access checking
#
#	Date	:	2012-02-09 [YatWoon]
#				edit the eCommunity link to redirect.php
#
#	Date	:	2012-02-09 [Ivan]
#				only staff and student accounts are accessible to "eLearning > Writing" now
#
#	Date	:	2011-11-30 [Yuen]
#				$plugin['DisablePoll'] = true; #Disable Poll on request
#				$plugin['DisableSurvey'] = true; #Disable eSurvey on request
#				$plugin['DisableTimeTable'] = true; #Disable TimeTable of eService on request
#				$plugin['DisableiCalendar'] = true; #Disable iCalendar on request
#				$plugin['DisableeCommunity'] = true; #Disable eCommunity on request
#
#	Date	:	2011-11-24 [Ivan]
#				added "eLearning > Writing"
#
#	Date	:	2011-11-23 [Henry Chow]
#				hide "eLearning > Subject References"
#
#	Date	:	2011-11-16 [YatWoon]
#				Add a config for TimeoutWarningSecond (default 120s) [Case#2011-1111-1405-16066]
#
#	Date	:	2011-11-04 [YatWoon]
#				remove ( && $_SESSION['UserType']!=USERTYPE_ALUMNI) checking for iMail
#
#	Date	:	2011-11-01 [Kelvin]
#				added "KSK" to menu
#
#	Date	:	2011-10-31 [Henry Chow]
#				modified "eService" > "Digital Archive" to "Subject Reference"
#
#	Date	:	2011-10-07 [Henry Chow]
#				modified "Digital Archive" link, default open "Admin Doc" for staff
#
#	Date	:	2011-09-28 [Ivan]
#				modified "JUPAS Ready" icon show for HK client only
#
#	Date	:	2011-09-28 [Yuen]
#				added eLC modules (ELP, SSR, ReadingRoom & SFC) to system menu and changed eLC to full title - eClass Learning Centre
#
#	Date	:	2011-09-26 [Fai]
#				added $customizationModuleMgmtIn_eAdmin, $customizationModuleMgmtIn_eAdmin_StudentMgmt to control customization module display in top menu bar
#
#	Date	:	2011-09-26 [Ivan]
#				added "JUPAS Ready" icon until 2012-03-01
#
#	Date	:	2011-09-17 [YatWoon]
#				add alumni user access right checking
#
#	Date	:	2011-09-08 [YatWoon]
#				add eEnrolment (lite version) checking
#
#	Date	:	2011-09-06 [Henry Chow]
#				allow "Viewer Group" member of Invoice Mgmt System can see the option in menu
#
#	Date	:	2011-08-23 [YaTWoon]
#				Add alumni user checking ( $_SESSION['UserType']==USERTYPE_ALUMNI  <=== alumni)
#				Hidden top menu for alumni user
#				change school logo link ($school_logo_link)
#
# 	Date	: 	2011-08-18 [Marcus]
#				Added Header Menu eAdmin > General Management > Message Center
#
# 	Date	: 	2011-07-08 [Henry Chow]
#				eHomework Admin not allow to view "eService > eHomework"
#
# 	Date	: 	2011-06-24 [Yuen]
#				allow to disable caching by setting $special_feature['CacheDisabled'] to true in settings.php
#
# 	Date	: 	2011-06-21 [Henry Chow]
#				display "eHomework" in menu when current user is eHomework Admin, even eHomework module is "disabled"
#
# 	Date	: 	2011-06-10 [Henry Chow]
#				update lightening of "eAdmin > Resources Management"
#
# 	Date	: 	2011-06-08 [Yuen]
#				Handled the update of iMail new message in cache mode
#
#	Date	:	2011-06-03 [Henry Chow]
#				modified display of "eAdmin > eHomework" , consider the parameter of $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]
#
#	Date	:	2011-06-02 [Henry Chow]
#				split the entrance for student & teacher of Digital Archive
#
#	Date	:	2011-05-31 [Thomas]
#				Added teh menu of eLearning > iTextbook
#
# 	Date	: 	2011-05-17 [Yuen]
#				Introduce caching (Cache Lite) to save time for better performance (now, 0.11s is saved!)
#
# 	Date	: 	2011-04-20 [Henry Chow]
#				Display of "Digital Archive" (only check $plugin['digital_archive'] at this moment)
#
# 	Date	: 	2011-04-11 [Henry Chow]
#				checking on $plugin['eDisciplinev12'] is the MUST while determine display of "eAdmin > eDiscipline"
#
# 	Date	: 	2011-04-08 [Carlos]
#				removed $header_onload_js from <body> onLoad event, moved it to be executed in /home/index.php $(document).ready()
#
#	Date	:	2011-03-23 [Yuen]
#				disabled to call returnNumNewMessage() for counting new mails if iMail Gamma/Plus is active
#
#	Date	:	2011-03-21 [Henry Chow]
#				revised checking on "Offical Photo" access right from $_SESSION
#
#	Date	:	2011-03-17 [Yuen]
#				added the menu of Digital Archive
#
#	Date	:	2011-03-17 [Yuen]
#				relocated eLibrary to "eLibrary -> eBook", Reading Scheme to "eLibrary -> Reading Scheme"
#
#	Date	:	2011-03-10 [Henry Chow]
#				allow System Admin to view "School News"
#
#	Date	:	2011-03-10 [ Yuen]
#				add Video Tutorials to ?
#
#	Date	:	2011-03-02 [Fai]
#				Add a checking , if it is a standalone iPorfolio , home/index page ($indexPage) should change to iPortfolio index page
#
#	Date	:	2011-02-24 [ YatWoon]
#				add $special_feature['portal_new_features_bubble'] to on/off the new features bubble
#
#	Date	:	2011-02-14 [Henry Chow]
#				change javascript function "loadContent" in order to display the layer according to window width (handle the problem of resize window)
#
#	Date	:	2011-02-11 [Henry Chow]
#				Split "Account Mgmt" right into 3 different rights (Parent, Staff & Student)
#
#	Date	:	2011-02-10 [Henry Chow]
#				Changed: add "New Features" on top right corner in order to display bubble of "Update Message"
#
#	Date	:	2011-01-25	Ivan
#				Changed: add Admin Group logic for "eAdmin > Studen Mgmt > eReportCard (Rubrics)" menu
#
#	Date	:	2011-01-20	Kenneth Chung
#				Changed: add style for class "print_hide"
#
#	Date	:	2011-01-05	YatWoon
#				Improved: "System admin" can access Account Management as well
#
#	Date	:	2010-12-30	Marcus
#				added: eLearning > Reading Scheme
#
#	Date	:	2010-12-21	YatWoon
#				Fixed: eAdmin > StudentMgmt > eHomework access right checking
#
#	Date	: 	2010-12-10 [Ivan]
#				add eAdmin > Student Management > eReportCard (Rubrics), added Class Teacher and Subject Teacher access right
#
#	Date	: 	2010-12-08 [Carlos]
#				add eAdmin > Account Management > SharedmailBox
#
#	Date	:	2010-11-29 [FAI]
#				update standalone iPortfolio , if login with hardcode login "ab_iportfolio_cd" and password "abc" , then the standalone iportfolio will be turned on
#
#	Date	:	2010-11-05 [Yuen]
#				update the layout of ? menu and show Help and Documentations
#
#	Date	:	2010-11-02 [Ronald]
#				add one more flag - $plugin['DisableResourcesBookingOnRequest'] to control the resource booking
#
#	Date	:	2010-10-19 [Henry Chow]
#				Fixed the "eAdmin > Repair System", admin can view Repair System even not in mgmt group
#
#	Date	:	2010-10-14 [Ivan]
#				Fixed the eService > eEnrollment permission for Attendance Helper who is a Staff
#
#	Date	:	2010-09-20 [Ivan]
#				Added eRC1.0 to the eAdmin > StudentMgmt menu
#
#	Date	:	2010-09-20 [Ivan]
#				Added eEnrolment Trial Period Logic
#
#	Date	:	2010-09-02 [Carlos]
#				Get iMail Gamma identity access right from lib
#
# 	Date 	: 	2010-08-25 [Sandy]
#				Power tool icon
#
#	Date	:	2010-08-19 [Carlos]
#				Add checking on identity and access right to control showing iMail Gamma icon
#
#	Date	:	2010-08-13 [Carlos]
#				change checking flags of iMail and iMail Gamma to show only one mail icon
#
#	Date	:	2010-08-12 [Ivan]
#				For Timetable link, added "index.php?clearCoo=1" to clear the cookies when first entering the page
#
#	Date	:	2010-08-12 [YatWoon]
#				comment "ajaxMsgBlock2" to hide the red box in the "?"
#				update $eClassVersion, if the version with sub-version number (e.g. ip.2.5.1.8.1.3), then only display ip.2.5.1.8.1
#
#	Date	:	2010-07-16 [Henry Chow]
#	Detail	:	add access right checking on display of "eAdmin > Account Mgmt > Student Registry"
#
#	Date	:	2010-07-13 [Henry Chow]
#	Detail	:	add the checking on "eAdmin > Account Mgmt > Student Registry"
#
#	Date	:	2010-07-08 [YAtWoon]
#				add $special_option['hide_lang_selectioin'] checking for hide lang selection
#
#	Date	:	2010-07-07 [Yuen]
#	Detail	:	support product version change log of different regions
#               e.g. setting.php : $intranet_version_url = "tw-version.eclass.com.hk";
#
#	Date	:	2010-06-25 [Henry Chow]
#	Detail	:	add the checking on "eLearning > LSIES" (check both $plugin & access right)
#
#	Date	:	2010-06-22 [Henry Chow]
#	Detail	:	change the checking of access to "eService>eDiscipline" using $_SESSION
#
#	Date	:	2010-06-21 [Ivan]
#	Detail	:	add flag to show module eReportCard (Rubrics)
#
#	Date	:	2010-06-10 [Marcus Leung]
#	Detail	:	add flag to hide imail
#
#	Date	:	2010-06-10 [Henry Chow]
#	Detail	:	Staff (Teacher) also can see "eService>eDiscipline" to view his/her own class (as class teacher or subject teacher)
#
#	Date	:	2010-06-09 [Eric]
#	Detail	:	add "Chuen Yuen Award Scheme" eService -> Chuen Yuen Award Scheme / eAdmin -> StudentManagement -> Chuen Yuen Award Scheme
#
#	Date	:	2010-06-04 [Ivan]
#	Detail	:	check admin access right in function UPDATE_CONTROL_VARIABLE() in lib.php for eEnrol and eRC now
#
#	Date	:	2010-05-20 [Henry Chow]
#	Detail	:	add access right checking on display of  "eAdmin > Account Management"
#
#	Date	:	2010-05-13 [Kenneth Chung]
#	Detail	:	include js brwsniff.js also
#
#	Date	:	2010-04-27 [Henry]
#	Detail	:	modified "eAdmin > Account Management" link
#
#	Date	:	2010-04-15 [Ivan]
#	Detail	:	modified "eService > eBooking" link
#
#	Date	:	2010-04-15 [Henry]
#	Detail	:	modified "Repair System" display checking
#
#	Date	:	2010-04-07 [YatWoon]
#	Detail	:	update session timeout layout
#
#	Date	:	2010-03-30 [YatWoon]
#	Detail	:	Add "Repair System" menu eAdmin>ResourceMgmt>Repair System
#
#	Date	:	2010-03-23 [Fai]
#	Detail	:	IP25 Support with standalone iPortfolio $stand_alone['iPortfolio']
#
# 	Date	:	2010-02-25 [Yuen]
#	Details	:	complete function of the timeout control using AJAX and JS (UI to be revised later)
#
# 	Date	:	2010-02-19 [YatWoon]
#	Details	:	add $intranet_default_lang = $_SESSION['intranet_default_lang'];
#
# 	Date	:	2009-12-15 [YatWoon]
#	Details	:	Apply top menu access checking for School Settings
#
# 	Date	:	2009-12-10 [Yuen]
#	Details	:	improve webmail checking logic to avoid checking if once fails to login to email server
#               (avoid slow performance)
#
# 	Date	:	2009-12-05 [Yuen]
#	Details	:	testing the timeout control using AJAX and JS (to be continued... for UI)
#
/******************* End Of Change Log *******************/

// $PATH_WRT_ROOT = "../../../";
if($sys_custom['OnlineRegistry'] && $_SESSION['ParentFillOnlineReg'])
{
    if($_SESSION['ParentFillOnlineRegMode'] == "new")
    {
        $parent_fill_online_reg_url = "/home/eService/StudentRegistry/online_reg/index.php";
        $pos = strpos($_SERVER['REQUEST_URI'], "/home/eService/StudentRegistry/online_reg/");
        if($pos===false && !$_SESSION['ParentFillOnline_Intranet_Allowed'])
        {
            header("Location: $parent_fill_online_reg_url");
            exit;
        }
    }
}

# check the page can access or not
if($_SESSION['UserType']==USERTYPE_ALUMNI)
{
    $cur_page = $_SERVER["REQUEST_URI"];
    $AllowAccess = array();
    $AllowAccess[] = "/home/eCommunity/";
    $AllowAccess[] = "/home/iaccount/account/";
    $AllowAccess[] = "/home/iaccount/login_record/";
    $AllowAccess[] = "/home/imail/";
    $AllowAccess[] = "/home/eService/eSurvey/";
    $AllowAccess[] = "/home/imail_gamma/";

    $hasAssessRight = 0;
    foreach($AllowAccess as $k=>$d)
    {
        $pos = strpos($cur_page, $d);
        if ($pos === false) continue;
        $hasAssessRight = 1;
    }
    if(!$hasAssessRight)	header("Location: /" . $_SESSION['AlumniURL']);

}


global $home_header_no_EmulateIE7, $special_feature, $favicon_image;

$isIE11 = strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0;') !== false /* && strpos($_SERVER['HTTP_USER_AGENT'], 'rv:11.0') !== false */;

$IsHeaderCaching = ($special_feature['CacheDisabled'] || $_SESSION["platform"]=="KIS" || $sys_custom['PowerClass'] || $sys_custom['project']['CourseTraining']['IsEnable'] || isset($_SESSION["ncs_role"])) ? false : true ;
$IsHeaderCaching = $IsHeaderCaching && !$home_header_no_EmulateIE7 && !$isIE11; // if cancel EmulateIE7 in meta, disable cache

// for Pui Ching eRC KG Special Handling
$isPageExcludeCaching = ((stristr($_SERVER['REQUEST_URI'],'reportcard_kindergarten/index.php?task=mgmt.subject_topic_score.score') !== false) ||
                         (stristr($_SERVER['REQUEST_URI'],'reportcard_kindergarten/index.php?task=mgmt.language_behavior_score.score') !== false) ||
                         (stristr($_SERVER['REQUEST_URI'],'reportcard_kindergarten/index.php?task=settings.teaching_tool.list') !== false));
if($isPageExcludeCaching) {
    $IsHeaderCaching = false;
}

if ($CurrentPageArr['LIBMS'])
{
    $CurrentPageArr['eLibPlus'] = true;

}

if (is_file("$intranet_root/templates/2020a/layout/home_header.customized.php"))
{
    include_once ("$intranet_root/templates/2020a/layout/home_header.customized.php");

}
else
{
    //$iNewCampusMail = "";
    //unset($iNewCampusMail);

    # ATTENTION: if you change the cache, related codes should be updated in /lang.php and /logout.php !!!
    include_once($PATH_WRT_ROOT."includes/cache_lite/Lite.php");

    /* Set a few options */
    //$cached_hh_id = "U".$_SESSION['UserID']."T".$_SESSION['UserType']."P".$_SESSION['eclass_session_password']."L".strlen($intranet_root);
    $cached_hh_id = CacheUniqueID($_SESSION['UserID']);

    if ($_SESSION['UserType']==USERTYPE_ALUMNI)
    {
        #  New Survey ?
        include_once($PATH_WRT_ROOT."includes/libportal.php");
        $lportal = new libportal();
        $NewSurvey = sizeof($lportal->returnSurvey())>0;
        $eSurveyAlumniClass = ($NewSurvey>0) ? "itool_esurvey_new" : "itool_esurvey";
    }


    $options = array('cacheDir' => '/tmp/', 'lifeTime'=>7200);
    $Cache_Lite = new Cache_Lite($options);
    
    /* Test if there is a valid cache-entry for this key */
    if ($IsHeaderCaching && $cached_hh_data = $Cache_Lite->get($cached_hh_id)) {

        /* Cache hit! We've got the cached content stored in $data! */
        //debug("using Cache Lite :: loaded from cached with ID=".$cached_hh_id);
        # reset/clear the cached (for testing only!)
        //$Cache_Lite->remove($cached_hh_id);

    } else {

        /* Cache miss! Use ob_start to catch all the output that comes next*/
        ob_start();
        $PATH_WRT_ROOT_ABS = "/";



        #############################################
        #####  home_header main codes
        #############################################
        include_once($PATH_WRT_ROOT."includes/global.php");
        include_once($PATH_WRT_ROOT."includes/libuser.php");
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        if (is_file($PATH_WRT_ROOT."includes/version.php"))
        {
            /*
             $JustWantVersionData = true;
             include_once($PATH_WRT_ROOT."includes/version.php");

             $eClassVersion = $versions[0][0];
             */
            $eClassVersion = Get_IP_Version();

            $pos = strrpos($eClassVersion, ".");
            if($pos>10)	$eClassVersion = substr($eClassVersion, 0, $pos);

        } else
        {
            $eClassVersion = "--";
        }
        global $linterface, $special_feature, $module_version,$stand_alone;

        if (!isset($linterface))
        {
            include_once($PATH_WRT_ROOT."includes/libinterface.php");
            $linterface = new interface_html("");
        }
        $linterface->IsHeaderCaching = $IsHeaderCaching;
        if (!isset($lu))
        {
            $lu = new libuser($UserID);
        }

        # protect for eClass logo!
        /* Yuen: not yet effective
         $eClassLogoData = md5(get_file_content($PATH_WRT_ROOT."images/2020a/logo_eclass_footer.gif"));
         if ($eClassLogoData!="980442a2a7b4cb596f011830d5afd907")
         {
         die("System cannot run due to a very import file for eClass logo is modified illegally!");
         }
         */


        $ControlTimeOutByJS = $special_feature['TimeOutByJS'];

        //$ControlTimeOutByJS = true;
        if ($ControlTimeOutByJS || $special_feature['TimeOutByJS'])
        {
            $TimeOutWarningSecond = $special_feature['TimeOutWarningSecond'] ? $special_feature['TimeOutWarningSecond'] : 120;
            # session max life time in second
            $SessionLifeTime = ini_get('session.gc_maxlifetime') - $TimeOutWarningSecond;
            # below is just for quick testing
            // $SessionLifeTime = 2;
        }


        # Basic Information
        $school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
        // $intranet_default_lang = $_SESSION['intranet_default_lang'];
        $intranet_default_lang = $_SESSION['intranet_default_lang'] ? $_SESSION['intranet_default_lang'] : $intranet_default_lang;

        if ($intranet_default_lang == "gb" || get_client_region()=="zh_MY" || get_client_region()=="zh_CN")
            ($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
            else
                ($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
                $change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];

                if (isset($_SESSION["SSV_PRIVILEGE"]["school"]["logo"]))
                {
                    $logo_img = $_SESSION["SSV_PRIVILEGE"]["school"]["logo"];
                }
                else
                {
                    $imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
                    $logo_img = $imgfile == "" ? (($_SESSION["platform"]=="KIS")? "{$image_path}/kis/logo_kis.png" : "{$image_path}/2020a/topbar_25/eclass_logo.png") : "/file/$imgfile";
                    $_SESSION["SSV_PRIVILEGE"]["school"]["logo"] = $logo_img;
                }

                if (!strstr($logo_img, "topbar_25/eclass_logo.gif"))
                {
                    $logo_fixed_height = " height='55' ";
                }

                $icalendar_name = $plugin["iCalendarFull"]?$iCalendar_iCalendar :$iCalendar_iCalendarLite;

                # get the URL of this site
                $PageURLArr = explode("//", curPageURL());
                if (sizeof($PageURLArr)>1)
                {
                    $strPath = $PageURLArr[1];
                    $eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
                }

                # eClass eCommunity data
                $cs_path = "$intranet_httppath/home/support.php";

                # product version change log
                //$intranet_version_url = "tw-version.eclass.com.hk";
                if (isset($intranet_version_url) && $intranet_version_url!="")
                {
                    $version_url = $intranet_version_url;
                } else
                {
                    # by default
                    $version_url = "hke-version.eclass.com.hk";
                }
                $products_path = "http://{$version_url}/?eclasskey=".$lu->sessionKey."&eClassURL=".$eClassURL;

                $onlinehelpcenter_path = "http://support.broadlearning.com/doc/help/intranet/";

//                $doccenter_path = "http://support.broadlearning.com/doc/help/portal/";
//                $doccenter_path = "http://support.broadlearning.com/doc/help/portal/iresources/index.html";

    $http = checkHttpsWebProtocol()?'https':'http';
    $doccenter_path = $http."://".$_SERVER["HTTP_HOST"]."/user_guide/";

                $videocenter_path = "http://www.youtube.com/user/BroadLearning#grid/user/98D7F2448C1E4667";

                $cs_faq = "http://support.broadlearning.com/doc/help/faq/";

                # user identity
                switch($_SESSION['UserType'])
                {
                    case USERTYPE_STAFF:
                        $UserIdentity = $_SESSION['isTeaching']==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
                        $UserIdentityIcon = $_SESSION['isTeaching']==1 ? "icon_teacher.gif" : "icon_nonteacher.gif";
                        break;
                    case USERTYPE_STUDENT:
                        $UserIdentity = $Lang['Identity']['Student'];
                        $UserIdentityIcon = "icon_student.gif";
                        break;
                    case USERTYPE_PARENT:
                        $UserIdentity = $Lang['Identity']['Parent'];
                        $UserIdentityIcon = "icon_parent.gif";
                        break;
                    case USERTYPE_ALUMNI:
                        $UserIdentity = $Lang['Identity']['Alumni'];
                        $UserIdentityIcon = "icon_student.gif";
                        break;
                    default:
                        $UserIdentity = "";
                        $UserIdentityIcon = "10x10.gif";
                        break;
                }

                if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
					$hostel_attendance_menu = ($sys_custom['StudentAttendance']['HostelAttendance'] && $_SESSION['UserType']==USERTYPE_STAFF);
				} else {
					$hostel_attendance_menu = ($sys_custom['StudentAttendance']['HostelAttendance'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $_SESSION['UserType']==USERTYPE_STAFF);
                }

                if($hostel_attendance_menu) {
                    include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
                    $lcardstudentattend2 = new libcardstudentattend2();
                    $hostel_attendance_admin_users = $lcardstudentattend2->getHostelAttendanceAdminUsers(array('OnlyUserID'=>1));
                    $hostel_attendance_groups = $lcardstudentattend2->getHostelAttendanceGroupsConfirmedRecords(array('AcademicYearID'=>Get_Current_Academic_Year_ID(),'RecordDate'=>date("Y-m-d"),'CategoryID'=>$lcardstudentattend2->HostelAttendanceGroupCategory,'IsGroupStaff'=>true,'GroupUserID'=>$_SESSION['UserID']));
                }

                # Build Menu
                unset($FullMenuArr);

                # Check menu focus
                # Home
                $CurrentPageArr['Home'] = 1;

                # eService
                if($CurrentPageArr['eServiceCircular'] || $CurrentPageArr['eServiceHomework'] ||
                    $CurrentPageArr['eServiceNotice'] || $CurrentPageArr['eServiceeEnrolment'] ||
                    $CurrentPageArr['eServiceeDisciplinev12'] || $CurrentPageArr['eServiceeSurvey'] ||
                    $CurrentPageArr['eServiceRepairSystem'] || $CurrentPageArr['eServiceeBooking'] ||
                    $CurrentPageArr['eServiceTimetable'] || $CurrentPageArr['PagePolling'] ||
                    $CurrentPageArr['SLSLibrary'] || $CurrentPageArr['eServiceClassDiary'] ||
                    $CurrentPageArr['eServiceStaffAttendance'] || ($CurrentPageArr['medical'] && $plugin['medical'] && ($_SESSION['UserType']==USERTYPE_PARENT))
                    || $CurrentPageArr['eServiceDigitalChannels']||$CurrentPageArr['eServiceSchoolBus']
                    || ($CurrentPageArr['eServiceeAppraisal'] && $_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["hasAppraisal"])
                    || $CurrentPageArr['eServiceeForm']
                    || ($sys_custom['StudentAttendance']['HostelAttendance'] && $CurrentPageArr['eServiceHostelAttendance'])
                    || ($CurrentPageArr['eServicePOS'] && $plugin['ePOS'] && !$sys_custom['ePOS']['exclude_webview'] && $sys_custom['ePOS']['enablePurchaseAndPickup'])
                    )
                {
                    $CurrentPageArr['Home'] = 0;
                    $CurrentPageArr['eService'] = 1;
                }


                # eLearning
                if($CurrentPageArr['eClass'] ||
                    $CurrentPageArr['eLC'] ||
                    $CurrentPageArr['eLib'] ||
                    $CurrentPageArr['eLibPlus'] ||
                    $CurrentPageArr['IES'] ||
                    $CurrentPageArr['iTextbook'] ||
                    ($CurrentPageArr['DigitalArchive_SubjectReference'] && $_SESSION['UserType']==USERTYPE_STUDENT) ||
                    $CurrentPageArr['SBA'] ||
                    $CurrentPageArr['WWSeLearningProject']
                    ) {
                        $CurrentPageArr['eLearning'] = 1;
                        $CurrentPageArr['Home'] = 0;
                    }


                    # eAdmin
                    # ---------- Account Management
                    if ($CurrentPageArr['StudentMgmt'] || $CurrentPageArr['StaffMgmt'] || $CurrentPageArr['ParentMgmt'] || $CurrentPageArr['StudentRegistry'] || $CurrentPageArr['SharedMailBox'] || $CurrentPageArr['AlumniMgmt'])
                    {
                        $CurrentPageArr['eAdmin'] = 1;
                        $CurrentPageArr['Home'] = 0;

                        # GeneralManagement
                        $CurrentPageArr['AccountManagement'] = 1;
                    }

                    # ---------- Genearl Management
                    if ($CurrentPageArr['ePolling'] || $CurrentPageArr['ePayment'] || $CurrentPageArr['schoolNews'] ||$CurrentPageArr['mssch_printing'] || $CurrentPageArr['eAdmineSurvey'] || $CurrentPageArr['ePOS'] || $CurrentPageArr['eClassApp'])
                    {
                        $CurrentPageArr['eAdmin'] = 1;
                        $CurrentPageArr['Home'] = 0;

                        # GeneralManagement
                        $CurrentPageArr['GeneralManagement'] = 1;
                    }


                    # ---------- Staff Management
                    if ($CurrentPageArr['eAdminCircular'] || $CurrentPageArr['eAdminStaffAttendance'] || $CurrentPageArr['eAdminSLRS'] || $CurrentPageArr['eAdminTeacherPortfolio'] || $CurrentPageArr['eAdmineAppraisal'] || $CurrentPageArr['eAdmineForm'])
                    {
                        $CurrentPageArr['eAdmin'] = 1;
                        $CurrentPageArr['Home'] = 0;

                        # Staff Management
                        $CurrentPageArr['StaffManagement'] = 1;
                    }

                    # ---------- Student Management
                    #eSports, Swimming Gala
                    if ($CurrentPageArr['eSports'] || $CurrentPageArr['eAdminNotice'] || $CurrentPageArr['eAdminHomework'] || $CurrentPageArr['StudentAttendance'] || $CurrentPageArr['eReportCard'] || $CurrentPageArr['eEnrolment'] || $CurrentPageArr['eDisciplinev12'] || $CurrentPageArr['eReportCard_Rubrics'] || $CurrentPageArr['eReportCard1.0'] || $CurrentPageArr['ClassDiary']||$CurrentPageArr['eSchoolBus'] || $CurrentPageArr['eReportCardKindergarten_eAdmin'] || $CurrentPageArr['eGuidance'] || ($CurrentPageArr['medical'] && $plugin['medical'] && ($_SESSION['UserType']==USERTYPE_STAFF)))
                    {
                        $CurrentPageArr['eAdmin'] = 1;
                        $CurrentPageArr['Home'] = 0;

                        $CurrentPageArr['StudentManagement'] = 1;
                    }

                    # ---------- Resources Management
                    if( $CurrentPageArr['eInventory'] || $CurrentPageArr['eBooking'] || $CurrentPageArr['eAdminRepairSystem'] || (($CurrentPageArr['DigitalArchive'] || $CurrentPageArr['DocRouting'] || $CurrentPageArr['DigitalChannels'] || $CurrentPageArr['ePCM']) && $_SESSION['UserType']==USERTYPE_STAFF))
                    {
                        $CurrentPageArr['eAdmin'] = 1;
                        $CurrentPageArr['Home'] = 0;

                        $CurrentPageArr['ResourcesManagement'] = 1;
                    }


                    if($CurrentPageArr['eServiceeSports'])
                    {
                        $CurrentPageArr['eService'] = 1;
                        $CurrentPageArr['Home'] = 0;
                    }

                    # School Settings
                    if ($CurrentPageArr['Location'] || $CurrentPageArr['Role'] || $CurrentPageArr['Subjects'] || $CurrentPageArr['Class'] || $CurrentPageArr['Group'] || $CurrentPageArr['SchoolCalendar'] || $CurrentPageArr['Timetable'] || $CurrentPageArr['Period'] || $CurrentPageArr['SystemSecurity'] || $CurrentPageArr['OrganizationInfo'])
                    {
                        $CurrentPageArr['Home'] = 0;
                        $CurrentPageArr['SchoolSettings'] = 1;
                    }

                    //IF IT IS A STAND ALONE IPORFOLIO , SET THE HOME PAGE TO IPORTFOLIO PAGE
                    if (isset($_SESSION['SSV_isiPortfolio_standalone']) && $_SESSION['SSV_isiPortfolio_standalone'] && $stand_alone['iPortfolio']){
                        $indexPage = "home/portfolio/";
                    }else{
                        $indexPage = "home/index.php";
                    }


                    if($CurrentPageArr['eLearningReadingGarden'] || $CurrentPageArr['eLib'] || $CurrentPageArr['eLibPlus'] )
                    {
                        $CurrentPageArr['eLearning'] = 1;
                        $CurrentPageArr['Home'] = 0;

                    }

                    //debug_r($CurrentPageArr);
                    # === $MenuArr : array fields description====
                    #	1 - menu id
                    #	2 - path / #
                    #	3 - section name
                    #	4 - section focus flag
                    #	5 - css (will change the icon)

                    if($_SESSION['UserType']!=USERTYPE_ALUMNI)
                    {
                        ### Home
                        $FullMenuArr[0] = array(0, "/".$indexPage, $Lang['Header']['Menu']['Home'], $CurrentPageArr['Home'], 'home', "Home");

                        ### eService
                        $eServiceMenu_i = 1;
                        $eService_i = 0;

                        ### eService -> Campus TV
                        if($plugin['tv'])
                        {
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(15, "javascript:newWindow(\"/home/plugin/campustv/info.php\",8)", $Lang['Header']['Menu']['CampusTV'], $CurrentPageArr['eServiceCampusTV'], "", "eServiceCampusTV");
                            $eService_i++;
                        }


                        /* for demo
                         if($plugin['digital_archive'])
                         {
                         $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(20, "/home/eService/digital_archive/", $Lang['Header']['Menu']['DigitalArchive'], $CurrentPageArr['eServiceDigitalArchive']);
                         $eService_i++;
                         }
                         */

                        # eService -> Digital Channels
                        if($plugin['DigitalChannels']){
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i]  = array(26, $PATH_WRT_ROOT_ABS."home/eAdmin/ResourcesMgmt/DigitalChannels/?clearCoo=1&From_eService=1", $Lang['Header']['Menu']['DigitalChannels'], $CurrentPageArr['eServiceDigitalChannels'], "", "DigitalChannels");
                            $eService_i++;
                        }

                        ### eService -> eAppraisal
                        if($plugin['eAppraisal'] && $_SESSION["SSV_PRIVILEGE"]["eAppraisal"]["hasAppraisal"])
                        {
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(15, "/home/eAdmin/StaffMgmt/appraisal/?init=eservice", $Lang['Header']['Menu']['eAppraisal'], $CurrentPageArr['eServiceeAppraisal'], "", "eServiceeAppraisal");
                            $eService_i++;
                        }

                        ### eService -> eBooking
                        if($plugin['eBooking'] && $_SESSION["eBooking"]["canAccesseService"])
                        {
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(14, "/home/eService/eBooking/", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eServiceeBooking'], "", "eServiceeBooking");
                            $eService_i++;
                        }

                        ### eService -> eCircular
                        if(!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION['UserType']==USERTYPE_STAFF)
                        {
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(11, "/home/eService/circular/", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eServiceCircular'], "", "eServiceCircular");
                            $eService_i++;
                        }

                        ### eService -> eDiscipline
                        if(
                            (($_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT )&& $plugin['Disciplinev12'] &&(!empty($_SESSION['SESSION_ACCESS_RIGHT']['DISCIPLINE'])))
                            ||
                            ($plugin['Disciplinev12'] && $_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["canAccess_eServive"])
                            )
                        {
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(19, "/home/eService/disciplinev12/", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eServiceeDisciplinev12'], "", "eServiceeDisciplinev12");
                            $eService_i++;
                        }

                        ### eService -> eForm
                        if(
                            ($_SESSION['UserType'] == USERTYPE_STAFF ) && $plugin['eForm']
                            )
                        {
                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(19, "/home/eService/eForm/", $Lang['Header']['Menu']['eForm'], $CurrentPageArr['eServiceeForm'], "", "eServiceeForm");
                            $eService_i++;
                        }
                        ### eService -> eEnrolment
                        $eEnrolTrialPast = false;
                        if ($plugin['eEnrollment_trial'] != '' && date('Y-m-d') > $plugin['eEnrollment_trial'])
                            $eEnrolTrialPast = true;

                            // 2012-0814-1021-26073: student and parent can be eEnrol admin now
                            //if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]))
                            //if($plugin['eEnrollment'] && !$eEnrolTrialPast && (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || $_SESSION['UserType'] == USERTYPE_STUDENT || $_SESSION['UserType'] == USERTYPE_PARENT))
                            if($plugin['eEnrollment'] && !$eEnrolTrialPast)
                            {
                                $showButton = true;
                                if ($_SESSION['UserType'] == USERTYPE_STAFF) {
                                    $showButton = false;

                                    if ($sys_custom['eEnrolment']['ClassTeacherView'] && $_SESSION["USER_BASIC_INFO"]["is_class_teacher"]) {
                                        $showButton = true;
                                    }
                                }
                                else if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]) {
                                    $showButton = false;
                                }

                                if($_SESSION['UserType'] == USERTYPE_PARENT && $sys_custom['eEnrolment']['HideEnrolForParentView']){
                                    $showButton = false;
                                }

                                if ($showButton) {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(18, "/home/eService/enrollment/?fromService=1", $Lang['Header']['Menu']['eEnrolment'], $CurrentPageArr['eServiceeEnrolment'],"", "eServiceeEnrolment");
                                    $eService_i++;
                                }
                            }

                            ### eService -> eHomework
                            //if(!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"] && !$_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"]){
                            if((!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION['UserType']==USERTYPE_STUDENT || ($_SESSION['UserType']==USERTYPE_PARENT && $_SESSION["SSV_PRIVILEGE"]["homework"]["parentAllowed"]))))	{
                                $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(12, "/home/eService/homework/index.php", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eServiceHomework'], "", "eServiceHomework");
                                $eService_i++;
                            }

                            ### eService -> eNotice
                            if ($sys_custom['eNotice']['eServiceDisableParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
                                $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
                            }
                            if ($sys_custom['eNotice']['eServiceDisableStudent'] && $_SESSION['UserType']==USERTYPE_STUDENT) {
                                $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"] = false;
                            }
                            if($plugin['notice'] && !$_SESSION["SSV_PRIVILEGE"]["notice"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["notice"]["canAccess"])
                            {
                                $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(11, "/home/eService/notice/", $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eServiceNotice'], "", "eServiceNotice");
                                $eService_i++;
                            }

                            if($plugin['ePOS'] && $sys_custom['ePOS']['enablePurchaseAndPickup'] && !$sys_custom['ePOS']['exclude_webview'] && ($_SESSION['UserType'] == USERTYPE_PARENT || $_SESSION['UserType'] == USERTYPE_STAFF || $_SESSION['UserType'] == USERTYPE_STUDENT))
                            {
                                $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(11, "javascript:newWindow(\"/home/eClassApp/common/ePOS/index.php?FromService=1\",8)", $Lang['Header']['Menu']['ePOS'], $CurrentPageArr['eServicePOS'], "", "eServicePOS");
                                $eService_i++;
                            }

                            ### eService -> eRC
                            if($plugin['ReportCard'])
                            {
                                if ($_SESSION['UserType']==USERTYPE_STAFF)
                                {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/reportcard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard'], "", "eServiceeReportCard");
                                    $eService_i++;
                                }
                            }
                            if($plugin['ReportCard2008'])
                            {
                                /* 20100604 Ivan: Now, do checking in lib.php, function UPDATE_CONTROL_VARIABLE()
                                 include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
                                 $libreportcard = new libreportcard();

                                 // check if the user is class/subject teacher in the ACTIVE year of eRC
                                 $Is_ClassTeacher = $libreportcard->Is_Class_Teacher($_SESSION['UserID']);
                                 $Is_SubjectTeacher = $libreportcard->Is_Subject_Teacher($_SESSION['UserID']);
                                 */

                                $Is_ClassTeacher = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_class_teacher"];
                                $Is_SubjectTeacher = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_teacher"];
                                $Is_SubjectPanel = $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_subject_panel"];

                                //		if ($Is_ClassTeacher || $Is_SubjectTeacher || $_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)
                                //		{
                                //			$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard'],"","eServiceeReportCard");
                                    //			$eService_i++;
                                    //		}

                                    # Check Access Setting (verification period) checking 2013-02-05


                                    if ($Is_ClassTeacher || $Is_SubjectTeacher || $Is_SubjectPanel)
                                    {
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard'],"","eServiceeReportCard");
                                        $eService_i++;
                                    }
                                    else if(($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT) && $_SESSION["SSV_PRIVILEGE"]["reportcard"]["EnableVerificationPeriod"]==1)
                                    {
                                        $showMenu = true;
                                        if ($sys_custom['eRC']['Settings']['DisableVerificationForParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
                                            $showMenu = false;
                                        }

                                        if ($showMenu) {
                                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/redirect.php?mode=0&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eServiceeReportCard'],"","eServiceeReportCard");
                                            $eService_i++;
                                        }
                                    }
                                }

                                ### eService -> eRC (Kindergarten)
                                if($plugin['ReportCardKindergarten'])
                                {
                                    //if(($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT) && $_SESSION["SSV_PRIVILEGE"]["reportcard"]["EnableVerificationPeriod"]==1)
                                    if($_SESSION['UserType']==USERTYPE_PARENT)
                                    {
                                        $showMenu = true;
//                                         if ($sys_custom['eRC']['Settings']['DisableVerificationForParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
//                                             $showMenu = false;
//                                         }

                                        if ($showMenu) {
                                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(13, "/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php", $Lang['Header']['Menu']['eReportCardKindergarten'], $CurrentPageArr['eServiceeReportCardKindergarten'],"","eServiceeReportCardKindergarten");
                                            $eService_i++;
                                        }
                                    }
                                }

                                ### eService -> eSports
                                if($_SESSION["UserType"]==2 && ($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"] || $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"] || $sys_custom['eSports']['StudentReviewEnroledEvents']))
                                {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i][0] = array(18, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eServiceeSports']);
                                    $eSports_i = 0;

                                    if($_SESSION["SSV_PRIVILEGE"]["eSports"]["inEnrolmentPeriod"] || $sys_custom['eSports']['StudentReviewEnroledEvents'])
                                    {
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i][1][$eSports_i] = array(181, "/home/eService/eSports/sports/", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['eServiceSportDay'], "", "eServiceSportDay");
                                        $eSports_i ++;
                                    }
                                    if($_SESSION["SSV_PRIVILEGE"]["swimminggala"]["inEnrolmentPeriod"])
                                    {
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i][1][$eSports_i] = array(182, "/home/eService/eSports/swimming_gala/", $Lang['Header']['Menu']['SwimmingGala'], $CurrentPageArr['eServiceSwimmingGala'],"", "eServiceSwimmingGala");
                                        $eSports_i ++;
                                    }
                                    $eService_i++;
                                }

                                ### eService -> eSurvey
                                if (!$plugin['DisableSurvey'])
                                {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(17, "/home/eService/eSurvey/index.php", $Lang['Header']['Menu']['eSurvey'], $CurrentPageArr['eServiceeSurvey'],"","eServiceeSurvey");
                                    $eService_i++;
                                }

                                ### eService -> Poll
                                if($_SESSION['UserType']!=USERTYPE_PARENT && !$plugin['DisablePoll'])
                                {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(17, "/home/eService/polling/index.php", $Lang['Header']['Menu']['ePolling'], $CurrentPageArr['PagePolling'],"","PagePolling");
                                    $eService_i++;
                                }

                                ### eService -> Repair System
                                //if($plugin['RepairSystem'])
                                if($plugin['RepairSystem'] && $_SESSION['UserType'] != USERTYPE_ALUMNI && ($_SESSION['UserType'] != USERTYPE_PARENT || $sys_custom['RepairSystemAccessForParent']) && ($_SESSION['UserType'] != USERTYPE_STUDENT || !$sys_custom['RepairSystemBanAccessForStudent']) && (isset($sys_custom['RepairSystem']['GrantedUserIDArr'])?(in_array($_SESSION['UserID'],(array)$sys_custom['RepairSystem']['GrantedUserIDArr'])?1:0):1) )
                                {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(17, "/home/eService/RepairSystem/", $Lang['Header']['Menu']['RepairSystem'], $CurrentPageArr['eServiceRepairSystem'],"","eServiceRepairSystem");
                                    $eService_i++;
                                }


                                ### eService -> Resource Booking
                                if($plugin['ResourcesBooking'] && (!$plugin['DisableResourcesBookingOnRequest'] || !isset($plugin['DisableResourcesBookingOnRequest'])) )
                                {
                                    $access2eServiceeBooking = false;
                                    if (isset($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"]) and $_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
                                    {
                                        $access2eServiceeBooking = true;
                                    }
                                    else
                                    {
                                        if($_SESSION["SSV_PRIVILEGE"]["resource"]["is_access"])
                                        {
                                            $access2eServiceeBooking = true;
                                        }
                                    }
                                    if($access2eServiceeBooking){
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(183, "javascript: newWindow(\"/home/eService/ResourcesBooking\",\"8\");", $Lang['Header']['Menu']['ResourcesBooking'], $CurrentPageArr['ResourcesBooking'],"","ResourcesBooking");
                                        $eService_i++;
                                    }
                                }

                                # eService -> SLS Library
                                if(isset($_SESSION["SSV_PRIVILEGE"]["library"]["access"]) && $_SESSION["SSV_PRIVILEGE"]["library"]["access"])
                                {
                                    $header_library_access = $_SESSION["SSV_PRIVILEGE"]["library"]["access"];
                                }
                                else
                                {
                                    if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]["library"]) && $_SESSION["SSV_PRIVILEGE"]["plugin"]["library"])
                                    {
                                        global $mapping_file_path;

                                        if (!isset($_SESSION['sls_library_access']))
                                        {
                                            $content = get_file_content("$mapping_file_path");
                                            if ($sls_mapping_encoding=="UCS-2LE")
                                            {
                                                $content = iconv("UCS-2LE", "UTF-8", $content);
                                            }
                                            $records = explode("\n",$content);

                                            //$header_lu = new libuser($UserID);
                                            //$login = $header_lu->UserLogin;
                                            $login = $lu->UserLogin;


                                            $header_library_access = false;

                                            for ($i=0; $i<sizeof($records); $i++)
                                            {
                                                $data = explode("::",$records[$i]);
                                                if (strtolower($login) == strtolower($data[0]))
                                                {
                                                    $header_library_access = true;
                                                    break;
                                                }
                                            }
                                            $_SESSION['sls_library_access'] = $header_library_access;
                                        } else
                                        {
                                            $header_library_access = $_SESSION['sls_library_access'];
                                        }
                                    }
                                    else
                                    {
                                        $header_library_access = false;
                                    }

                                    $_SESSION["SSV_PRIVILEGE"]["library"]["access"] = $header_library_access;


                                }
                                if($header_library_access)
                                {
                                    //$FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(20, "/home/eService/sls_library_index.php", $Lang['SLSLibrary'], $CurrentPageArr['SLSLibrary'],"","SLSLibrary");
                                    if ($sls_mapping_encoding!="UCS-2LE")
                                    {
                                        # before SLS upgrades
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i][0] = array(20, "#", $Lang['SLSLibrary'], $CurrentPageArr['SLSLibrary'],"","SLSLibrary");
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i][1][0] = array(201, "javascript:newWindow(\"/home/plugin/sls/library.php\",8)", $Lang['SLSSearch'], $CurrentPageArr['SLSLibrary'], "", "SLSLibrary");
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i][1][1] = array(202, "javascript:newWindow(\"/home/plugin/sls/library_info.php\",8)", $Lang['SLSInfo'], $CurrentPageArr['SLSLibrary'], "", "SLSLibrary");

                                    } else
                                    {
                                        $FullMenuArr[$eServiceMenu_i][1][$eService_i][0] = array(20, "javascript:newWindow(\"/home/plugin/sls/library.php\",8)", $Lang['SLSLibrary'], $CurrentPageArr['SLSLibrary'],"","SLSLibrary");
                                    }

                                    $eService_i++;
                                }
                                # end of SLS Library

                                # eService -> Chuen Yuen Award Scheme
                                if($sys_custom['chuen_yuen_award_scheme'] && $_SESSION['UserType'] == USERTYPE_STUDENT){
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(21, "/home/award_scheme/", $Lang['customization']['award_scheme'], '');
                                    $eService_i++;
                                }

                                # eService -> Timetable
                                if ($_SESSION['UserType']==USERTYPE_STAFF && !$plugin['DisableTimeTable']) {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(22, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/view.php?clearCoo=1&From_eService=1", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['eServiceTimetable'], "", "eServiceTimetable");
                                    $eService_i++;
                                }


                                # eService -> Class Diary
                                if($plugin['ClassDiary'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_PARENT)) {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(23, $PATH_WRT_ROOT_ABS."home/eService/classdiary/", $Lang['Header']['Menu']['ClassDiary'], $CurrentPageArr['eServiceClassDiary'], "", "eServiceClassDiary");
                                    $eService_i++;
                                }

                                # eService -> Staff Attendance System
                                if($_SESSION['SSV_PRIVILEGE']['plugin']['attendancestaff'] && ($module_version['StaffAttendance'] == 3.0) && ($_SESSION['UserType'] == USERTYPE_STAFF) && ($sys_custom['StaffAttendance']['DailyAttendanceRecord'] || $sys_custom['StaffAttendance']['DoctorCertificate']) )
                                {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(24, $PATH_WRT_ROOT_ABS."home/eService/StaffAttendance/", $Lang['StaffAttendance']['StaffAttendance'], $CurrentPageArr['eServiceStaffAttendance'], "", "eServiceStaffAttendance");
                                    $eService_i++;
                                }


                                # eService -> Medical
                                if($plugin['medical']) {
                                    include_once($PATH_WRT_ROOT."includes/cust/medical/libMedical.php");
                                    $objMedical = new libMedical();

                                    if ($objMedical->getParentViewAccess('bowel') || $objMedical->getParentViewAccess('studentLog')){
                                        $studentList = $objMedical->getStudentInfoByParent($_SESSION['UserID']);
                                        if (count($studentList) > 0)	// Parent must have child
                                        {
                                            $FullMenuArr[$eServiceMenu_i][1][$eService_i] = array(25, $PATH_WRT_ROOT_ABS."home/eAdmin/StudentMgmt/medical/?t=index.index", $Lang['Header']['Menu']['Medical'], $CurrentPageArr['medical'], "", "medical");
                                            $eService_i++;
                                        }
                                    }
                                }

                                # eService -> eSchoolBus
                                if($plugin['eSchoolBus']){
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i]  = array(27, $PATH_WRT_ROOT_ABS."home/eService/eSchoolBus/",$Lang['Header']['Menu']['eSchoolBus'], $CurrentPageArr['eServiceSchoolBus'], "", "eServiceSchoolBus");
                                    $eService_i++;
                                }

                                if($sys_custom['StudentAttendance']['HostelAttendance_Reason']) {
									$hostel_attendance_menu = ($sys_custom['StudentAttendance']['HostelAttendance'] && $_SESSION['UserType']==USERTYPE_STAFF && in_array($_SESSION['UserID'],$hostel_attendance_admin_users));
								} else {
                                    $hostel_attendance_menu = ($sys_custom['StudentAttendance']['HostelAttendance'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] && $_SESSION['UserType']==USERTYPE_STAFF && (count($hostel_attendance_groups)>0 || in_array($_SESSION['UserID'],$hostel_attendance_admin_users)));
								}

                                if($hostel_attendance_menu) {
                                    $FullMenuArr[$eServiceMenu_i][1][$eService_i]  = array(28, $PATH_WRT_ROOT_ABS."home/eAdmin/StudentMgmt/attendance/dailyoperation/hostel/",$Lang['StudentAttendance']['HostelAttendance'], $CurrentPageArr['eServiceHostelAttendance'], "", "eServiceHostelAttendance");
                                    $eService_i++;
                                }

                                if ($eService_i<=0 || $special_feature['lite_for_eLearning'])
                                {
                                    $eServiceMenu_i = 0;
                                    unset($FullMenuArr[1]);
                                } else
                                {
                                    $FullMenuArr[$eServiceMenu_i][0] = array(1, "#", $Lang['Header']['Menu']['eService'], $CurrentPageArr['eService'], 'common', "eService");
                                }

                                if ($special_feature['lite_for_eAdmin'])
                                {
                                    //F99388
                                    //$eClassMenu_i = $eServiceMenu_i;  // no need to increament, set it for being used in next menu, i.e. eAdmin
                                    $eClassMenu_i = $eServiceMenu_i + 1;  // NEED to increament and run the eClass logic normally, remove the menu at last. Otherwise, strange behaviour if enabled some eAdmin modules

                                } else
                                {
                                    $eClassMenu_i = $eServiceMenu_i + 1;

                                    ### eLearning
                                    $FullMenuArr[$eClassMenu_i][0] = array(2, "#", $Lang['Header']['Menu']['eLearning'], $CurrentPageArr['eLearning'], 'common', "eLearning" );
                                }

                                $pos = 0;

                                # eLearning > Digital Archive (Student/Staff entrance)

                                /*
                                 if($sys_custom['subject_resources'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF)) {

                                 $FullMenuArr[$eClassMenu_i][1][$pos] = array(29, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'],"","DigitalArchive");
                                 $pos++;
                                 }
                                 */


                                /* change to first level menu on 25 Sept 2012
                                 if (($plugin['eLib'] || $plugin['library_management_system'] || $plugin['ReadingScheme'] || $sys_custom['subject_resources']) && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF))
                                 {
                                 $CurrentPageArr['DRC'] = ($CurrentPageArr['eLib'] || $CurrentPageArr['eLibPLus'] || $CurrentPageArr['eLearningReadingGarden'] || $CurrentPageArr['DigitalArchive_SubjectReference']);
                                 $FullMenuArr[$eClassMenu_i][1][$pos][0] = array(210, "#", $Lang['Header']['Menu']['DRC'], $CurrentPageArr['DRC'], "", "DRC");
                                 $drc_i = 0;
                                 if ($plugin['eLib'] || $plugin['library_management_system'])
                                 {
                                 $FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(211, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib'], "", "eLib");
                                 $drc_i ++;
                                 $FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(212, "/home/eLearning/elibplus/", $Lang['Header']['Menu']['eLibraryPlus'], $CurrentPageArr['eLibPLus'], "", "eLibPLus");
                                 $drc_i ++;
                                 }
                                 if ($plugin['ReadingScheme'])
                                 {
                                 if($_SESSION['UserType']==USERTYPE_STUDENT)
                                 $ReadingSchemeLink = "/home/eLearning/reading_garden/student_login.php";
                                 else
                                 $ReadingSchemeLink = "/home/eLearning/reading_garden/?clearCoo=1";

                                 $FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(213, $ReadingSchemeLink, $Lang['Header']['Menu']['ReadingScheme'], $CurrentPageArr['eLearningReadingGarden'], "", "eLearningReadingGarden");
                                 $drc_i ++;
                                 }
                                 if ($sys_custom['subject_resources'])
                                 {
                                 //$FullMenuArr[$eClassMenu_i][1][$pos] = array(29, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'],"","DigitalArchive");
                                 $FullMenuArr[$eClassMenu_i][1][$pos][1][$drc_i] = array(214, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'], "", "DigitalArchive_SubjectReference");
                                 $drc_i ++;
                                 }
                                 $pos++;
                                 }
                                 */

                                 if ($plugin['W2'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF) && !$plugin['W2_oldName'])
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(28, "javascript:newWindow(\"/home/eLearning/w2/\", 32)", $Lang['Header']['Menu']['easyWriting'], $CurrentPageArr['W2'],"","W2");
                                     $pos ++;
                                 }


                                 if ($plugin['CENTRAL_Power_Lesson'] && ($_SESSION['UserType']==USERTYPE_STAFF || $_SESSION['UserType']==USERTYPE_STUDENT))
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(29, "javascript:Go2CentralPL()", "PowerLesson", $CurrentPageArr['CENTRAL_PL'],"","CENTRAL_PL");
                                     $pos ++;
                                 }

                                 //$FullMenuArr[$eClassMenu_i][1][0] = array(21, "/home/eLearning/eclass/", $Lang['Header']['Menu']['eClass'], $CurrentPageArr['eClass'],"","eClass");
                                 if (!$sys_custom['HideeClass']) {
                                     $FullMenuArr[$eClassMenu_i][1][$pos][0] = array(22, "/home/eLearning/eclass/", $Lang['Header']['Menu']['eClass'], $CurrentPageArr['eClass'],"","eClass");
                                     $pos++;
                                 }

                                 if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2SpecialRm"] || $sys_integration['bookflix_school_access'])
                                 {


                                     //$FullMenuArr[$eClassMenu_i][1][$pos][0] = array(22, "/home/eLearning/elc/", $Lang['Header']['Menu']['eLC'], $CurrentPageArr['eLC'],"","eLC");
                                     $FullMenuArr[$eClassMenu_i][1][$pos][0] = array(23, "#", $Lang['Header']['Menu']['eLC'], $CurrentPageArr['eLC'],"","eLC");
                                     $elc_i = 0;



                                     if (isset($sys_integration['bookflix_school_access']) && $sys_integration['bookflix_school_access']!="" && isset($sys_integration['bookflix_school_expiry']))
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(230, "javascript:openBookFlix()", "BookFlix", $CurrentPageArr['BookFlix'], "", "BookFlix");
                                         $elc_i ++;
                                     }

                                     if ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Elprm"])
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(231, "javascript:newWindow(\"/home/eLearning/elc/index_elp.php\",8)", $Lang['Header']['Menu']['ELP'], $CurrentPageArr['ELP'], "", "srELP");
                                         $elc_i ++;
                                     }

                                     if ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Readingrm"])
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(232, "javascript:newWindow(\"/home/eLearning/elc/readingrm.php\",8)", $Lang['Header']['Menu']['ReadingRoom'], $CurrentPageArr['ELP'], "", "srELP");
                                         $elc_i ++;
                                     }
                                     if ($plugin['ScrabbleFC'])
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(233, "javascript:toScrabble()", $Lang['Header']['Menu']['SFC'], $CurrentPageArr['ELP'], "", "srELP");
                                         $elc_i ++;
                                     }
                                     if ($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Ssrm"])
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos][1][$elc_i] = array(234, "javascript:newWindow(\"/home/eLearning/elc/index_ssr.php\",8)", $Lang['Header']['Menu']['SSR'], $CurrentPageArr['ELP'], "", "srELP");
                                         $elc_i ++;
                                     }
                                     $pos ++;
                                 }
                                 /*
                                  if ($plugin['eLib'])
                                  {
                                  $FullMenuArr[$eClassMenu_i][1][] = array(23, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib']);
                                  }
                                  */



                                 if ($plugin['eLib'] && (!$plugin['library_management_system'] || $sys_custom['eLib']['CoexistWithPlus']))
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(20, "/home/eLearning/elibrary/", $Lang['Header']['Menu']['eLibrary'], $CurrentPageArr['eLib'], "", "eLib");
                                     $pos ++;
                                 }

                                 if($plugin['library_management_system'])
                                 {
                                     include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
                                     $permission = elibrary_plus::getUserPermission($UserID);

                                     # may hide only for admin to view
                                     //if ($plugin['library_management_system'] && (!$sys_custom['block_access_temporily'] || ($sys_custom['block_access_temporily'] && $_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem'])))
                                     if ($plugin['library_management_system'] && ($permission['elibplus_opened'] || $_SESSION['UserType']==USERTYPE_STAFF))
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos] = array(20, $plugin['eLibraryPlusOne']?"/home/eLearning/elibplus/":"javascript:newWindow(\"/home/eLearning/elibplus2/\",8)", $Lang['Header']['Menu']['eLibraryPlus'], $CurrentPageArr['eLibPLus'], "", "eLibPlus");
                                         $pos ++;
                                     }
                                 }


                                 $showePost = true;
                                 if ($sys_custom['ePost']['hiddenForParent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
                                     $showePost = false;
                                 }
                                 if($plugin['ePost'] && $showePost)
                                 {
                                     include_once($PATH_WRT_ROOT."includes/ePost/libepost.php");
                                     $ePost = new libepost();

                                     if(!$ePost->check_is_expired()){
                                         $FullMenuArr[$eClassMenu_i][1][$pos] = array(21, "javascript:newWindow(\"/home/ePost/index.php\", 36)", $Lang['ePost']['ePost'], $CurrentPageArr['ePost'],"","ePost");
                                         $pos ++;
                                     }
                                 }

                                 if ($plugin['FlippedChannels'] && ($_SESSION['UserType']==USERTYPE_STAFF))
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(270, "javascript:newWindow(\"/home/asp/flipchan.php\", 36)", $Lang['General']['FlippedChannels'], $CurrentPageArr['FlippedChannels'],"","FlippedChannels");
                                     $pos ++;
                                 }
                                 if ($plugin['PowerFlip'])
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(270, "javascript:newWindow(\"/home/pl/index.php\", 36)", $Lang['General']['PowerFlip'], $CurrentPageArr['PowerFlip'],"","PowerFlip");
                                     $pos ++;
                                 }
                                 if ($plugin['iTextbook'] && ($_SESSION["SSV_USER_ACCESS"]["eLearning-iTextbook"]))
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(27, "/home/eLearning/iTextbook/", $Lang['itextbook']['itextbook'], $CurrentPageArr['iTextbook'],"","iTextbook");
                                     $pos ++;
                                 }


                                 if ($plugin['lslp'])
                                 {
                                     include_once ($PATH_WRT_ROOT."includes/liblslp.php");
                                     $ls = new lslp();
                                     # to be improved to control on users (i.e. particular students, teachers)
                                     if($ls->hasLSLPAccess() && $ls->isInLicencePeriod())
                                     {
                                         $FullMenuArr[$eClassMenu_i][1][$pos] = array(24,"javascript:open_lslp();", $Lang['lslp']['lslp'], $CurrentPageArr['LSLP'],"","LSLP");
                                         $pos ++;
                                     }
                                 }
                                 if ($plugin['IES'] && ($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"]))
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(25, "/home/eLearning/ies/", $Lang['Header']['Menu']['IES'], $CurrentPageArr['IES'],"","IES");
                                     $pos ++;
                                 }

                                 if ($plugin['ReadingScheme'])
                                 {
                                     if($_SESSION['UserType']==USERTYPE_STUDENT)
                                         $ReadingSchemeLink = "/home/eLearning/reading_garden/student_login.php";
                                         else
                                             $ReadingSchemeLink = "/home/eLearning/reading_garden/?clearCoo=1";

                                             $FullMenuArr[$eClassMenu_i][1][$pos] = array(213, $ReadingSchemeLink, $Lang['Header']['Menu']['ReadingScheme'], $CurrentPageArr['eLearningReadingGarden'], "", "eLearningReadingGarden");
                                             $pos ++;
                                 }

                                 $showSubjectResources = true;
                                 if ($sys_custom['subject_resources_hidden_for_parent'] && $_SESSION['UserType']==USERTYPE_PARENT) {
                                     $showSubjectResources = false;
                                 }
                                 if ($sys_custom['subject_resources'] && $showSubjectResources)
                                 {
                                     $FullMenuArr[$eClassMenu_i][1][$pos] = array(214, "/home/eAdmin/ResourcesMgmt/DigitalArchive/", $Lang['Header']['Menu']['SubjectReference'], $CurrentPageArr['DigitalArchive_SubjectReference'], "", "DigitalArchive_SubjectReference");
                                     $pos ++;
                                 }

                                 // with SBA plugin
                                 if (count($plugin['SBA']) > 0 && ($_SESSION["SSV_USER_ACCESS"]["eLearning-SBA"] || $_SESSION["SSV_PRIVILEGE"]["SBA"]["isTeacher"] || $_SESSION["SSV_PRIVILEGE"]["SBA"]["isStudent"]))
                                 {
                                     if($_SESSION['UserType']==USERTYPE_STUDENT) {
                                         $FullMenuArr[$eClassMenu_i][1][$pos] = array(43, "javascript:newWindow(\"/home/eLearning/sba/\", 32)", $Lang['Header']['Menu']['SBA'], $CurrentPageArr['SBA'],"","SBA");
                                         $pos ++;
                                     } else if($_SESSION['UserType']==USERTYPE_STAFF) {
                                         $FullMenuArr[$eClassMenu_i][1][$pos] = array(43, "/home/eLearning/sba/", $Lang['Header']['Menu']['SBA'], $CurrentPageArr['SBA'],"","SBA");
                                         $pos ++;
                                     }
                                 }
                                 /*
                                  if ($plugin['ReadingScheme'])
                                  {
                                  if($_SESSION['UserType']==USERTYPE_STUDENT)
                                  $ReadingSchemeLink = "/home/eLearning/reading_garden/student_login.php";
                                  else
                                  $ReadingSchemeLink = "/home/eLearning/reading_garden/?clearCoo=1";

                                  $FullMenuArr[$eClassMenu_i][1][] = array(26, $ReadingSchemeLink, $Lang['Header']['Menu']['ReadingScheme'], $CurrentPageArr['eLearningReadingGarden']);
                                  }
                                  */

                                  if ($plugin['W2'] && ($_SESSION['UserType']==USERTYPE_STUDENT || $_SESSION['UserType']==USERTYPE_STAFF) && $plugin['W2_oldName'])
                                  {
                                      $FullMenuArr[$eClassMenu_i][1][$pos] = array(28, "javascript:newWindow(\"/home/eLearning/w2/\", 32)", $Lang['Header']['Menu']['Writing'], $CurrentPageArr['W2'],"","W2");
                                      $pos ++;
                                  }


                                  if($plugin['WWS_eLearningProject'])
                                  {
                                      $FullMenuArr[$eClassMenu_i][1][$pos] = array(797, "/home/eLearning/eclass2/?roomType=13", $Lang['wws']['eLearningProject'], $CurrentPageArr['WWSeLearningProject'],"","WWSeLearningProject");
                                      $pos ++;
                                  }

                                  if ($plugin['kskgsmath'])
                                  {
                                      include_once ($PATH_WRT_ROOT."includes/libksk.php");
                                      $ksk = new ksk();
                                      # to be improved to control on users (i.e. particular students, teachers)
                                      if($ksk->hasKSKAccess())
                                      {
                                          if($lu->isStudent()){
                                              $FullMenuArr[$eClassMenu_i][1][$pos] = array(26, "javascript:open_kskgs(1);", $Lang['kskgsmath']['kskgsmath'].' - '.$Lang['kskgsmath']['kskgsmath_gs'], $CurrentPageArr['KSK_GSMATH_GS'],"","KSK_GSMATH_GS");
                                              $pos ++;
                                              $FullMenuArr[$eClassMenu_i][1][$pos] = array(26, "javascript:open_kskgs(2);", $Lang['kskgsmath']['kskgsmath'].' - '.$Lang['kskgsmath']['kskgsmath_math'], $CurrentPageArr['KSK_GSMATH_MATH'],"","KSK_GSMATH_MATH");
                                              $pos ++;
                                          }
                                          else{
                                              $FullMenuArr[$eClassMenu_i][1][$pos] = array(26, "javascript:open_kskgs();", $Lang['kskgsmath']['kskgsmath'], $CurrentPageArr['KSK_GSMATH'],"","KSK_GSMATH");
                                              $pos ++;
                                          }

                                      }
                                  }

                                  if ($plugin['PaGamO'])
                                  {
                                      include_once ($PATH_WRT_ROOT."includes/pagamo/libpagamo.php");
                                      $libpi = new pagamo();
                                      if($libpi->hasPagamoAccess()){
                                          $FullMenuArr[$eClassMenu_i][1][$pos] = array(270, "javascript:newWindow(\"/home/eLearning/LTI/Pagamo/index.php\", 36)", "eClass x PaGamO", $CurrentPageArr['PaGamO'],"","PaGamO");
                                          $pos ++;
                                      }
                                  }

                                  //F99388
                                  if ($special_feature['lite_for_eAdmin']) {
                                      unset($FullMenuArr[$eClassMenu_i]);  // remove the eClass menu for eAdmin lite version
                                      $eClassMenu_i = $eClassMenu_i - 1;	 // roll back the menu index for eAdmin use
                                  }
                    }
                    ### eAdmin
                    ###### check display Account Management
                    /*
                     include_once($intranet_root."/includes/libaccountmgmt.php");
                     $laccount = new libaccountmgmt();
                     $officalPhotoMgmtMember = $laccount->getOfficalPhotoMgmtMemberID();
                     if(sizeof($officalPhotoMgmtMember) && in_array($UserID, $officalPhotoMgmtMember))
                     $canAccessOfficalPhotoSetting = 1;
                     */
                     $canAccessOfficalPhotoSetting = $_SESSION["SSV_PRIVILEGE"]["canAccessOfficalPhotoSetting"];

                     if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || ($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY']))) || ($plugin['imail_gamma']==true && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"]) || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)
                         $AccountMgmt = 1;

                         ###### check display General Management
                         if(
                             ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"]) ||
                             ($plugin['payment'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"]) ||
                             ($plugin['payment'] && $plugin['ePOS'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"]) ||
                             ($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] && !$plugin['DisableNews']) ||
                             ($plugin['mssch_printing_module'] && $_SESSION['UserType'] == USERTYPE_STAFF) ||
                             ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"]) ||
                             ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]  && !$plugin['DisableNews']) ||
                             $_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] ||
                             $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"] ||
                             $_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] ||
                             ($plugin['eClassApp'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"])) ||
                             ($plugin['eClassTeacherApp'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"])) ||
                             ($plugin['eClassStudentApp'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"] || $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"])) ||
                             $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] ||
                             $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"]
                             )
                             $GeneralMgmt = 1;
                             ###### check display Staff Management
                             if(
                                 ($special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]))) ||
                                 ($module_version['StaffAttendance'] == 3.0 && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"])) ||
                                 ($plugin['SLRS'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SLRS"]) ||
                                 ($plugin['TeacherPortfolio'] && ( $_SESSION["SSV_USER_ACCESS"]["other-TeacherPortfolio"] || $_SESSION["SSV_PRIVILEGE"]["TeacherPortfolio"]["isViewerUser"])) ||
                                 ($plugin['eAppraisal'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAppraisal"]) ||
                                 ($plugin['eForm'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eForm"])
                                 )
                                 //($plugin['attendancestaff'])
                                 $StaffMgmt = 1;
                                 ###### check display Student Management

                                 ## Special checking for eEnrolment
                                 $show_eAdmin_eEnrol = false;
                                 if ($plugin['eEnrollment'] && !$eEnrolTrialPast)
                                 {
                                     /* 20100604 Ivan: Now, get admin status in lib.php, function UPDATE_CONTROL_VARIABLE()
                                      include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
                                      $libenroll = new libclubsenrol();

                                      if (isset($header_lu) == false)
                                      {
                                      include_once ($PATH_WRT_ROOT."/includes/libuser.php");
                                      $header_lu = new libuser($_SESSION['UserID']);
                                      }
                                      */

                                     if ($_SESSION['UserType']!=USERTYPE_STUDENT && $_SESSION['UserType']!=USERTYPE_PARENT)
                                     {
                                         if ($_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_admin"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_enrol_master"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_pic"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_helper"] || $_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_activity_helper"] || ($sys_custom['project']['HKPF'] && $_SESSION['UserType']==USERTYPE_STAFF))
                                             $show_eAdmin_eEnrol = true;
                                     }
                                 }
                                 ## End Special checking for eEnrolment

                                 $show_eAdmin_ReportCardRubrics = false;
                                 if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard_Rubrics"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"] || count((array)$_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["PageAccessRightArr"]) > 0)
                                     $show_eAdmin_ReportCardRubrics = true;

                                     $show_eAdmin_ReportCardKindergarten = false;
                                     if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCardKindergarten"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_class_teacher"] || $_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_subject_teacher"]||$_SESSION["SSV_PRIVILEGE"]["reportcard_kindergarten"]["is_admin_group_member"]){
                                         $show_eAdmin_ReportCardKindergarten = true;
                                     }

                                     include_once($PATH_WRT_ROOT."/includes/libhomework.php");
                                     include_once($PATH_WRT_ROOT."/includes/libhomework2007a.php");
                                     $lhomework = new libhomework2007();
                                     if (
                                         ($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) ||
                                         ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"]) ||
                                         ($plugin['Disciplinev12'] && (($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) || $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"])) ||
                                         ($plugin['eEnrollment'] && $show_eAdmin_eEnrol) ||
                                         (
                                             (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] &&
                                                 ((
                                                     $_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
                                                     $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
                                                     $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
                                                     $_SESSION['isTeaching'] ||
                                                     ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
                                                     $lhomework->isViewerGroupMember($UserID)
                                                     ) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework']))
                                             ) ||

                                         ($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"])) ||
                                         ($plugin['ReportCard'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"]) ||
                                         ($plugin['ReportCard2008'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"]) ||
                                         ($plugin['ReportCard_Rubrics'] && $show_eAdmin_ReportCardRubrics) ||
                                         ($plugin['ReportCardKindergarten'] && $show_eAdmin_ReportCardKindergarten) ||
                                         ($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) ||
                                         ($plugin['Sports'] && $sys_custom['eSports']['KaoYipRelaySettings'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"]) ||
                                         ($plugin['swimming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"]) ||
                                         ($plugin['medical'] && (($_SESSION["SSV_PRIVILEGE"]["medical"]["isMedicalUser"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-medical"])))	||
                                         ($plugin['ClassDiary'] && $_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION['isTeaching']==1 /*&& $_SESSION["SSV_USER_ACCESS"]["eAdmin-ClassDiary"]*/)||
                                         ($plugin['eGuidance'] && $_SESSION['UserType'] == USERTYPE_STAFF && ($_SESSION['SSV_USER_ACCESS']['eAdmin-eGuidance'] || $_SESSION['eGuidance']['current_right'] || $_SESSION['eGuidance']['settings']['isClassTeacher'] || $_SESSION['eGuidance']['settings']['isSubjectTeacher'])) ||
                                         ($plugin['eSchoolBus']&& $_SESSION['UserType'] == USERTYPE_STAFF)
                                         )
                                         $StudentMgmt = 1;

                                         //($plugin['RepairSystem'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"])
                                         ###### check display Resources Management

                                         if (isset($plugin['RepairSystem']) && $plugin['RepairSystem'])
                                         {
                                             include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
                                             $lrepairsystem = new librepairsystem();
                                         }

                                         if($sys_custom['Invoice2Inventory'] && $plugin['Inventory']) {
                                             include_once($PATH_WRT_ROOT."includes/libinvoice.php");
                                             $linvoice = new libinvoice();
                                         }

                                         if($plugin['DigitalChannels']) {
                                             include_once($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
                                             $ldigitalchannels = new libdigitalchannels();
                                         }

                                         if(
                                             ($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER" )) ||
                                             ($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")) ||
                                             ($plugin['RepairSystem'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"] || $lrepairsystem->userInMgmtGroup($UserID)>0)) ||
                                             ($plugin['digital_archive'] && $_SESSION['UserType']==USERTYPE_STAFF) ||
                                             (($sys_custom['Invoice2Inventory'] && $plugin['Inventory']) && ($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || $linvoice->isViewerGroupMember)) ||
                                             ($plugin['DocRouting'] && $_SESSION['UserType']==USERTYPE_STAFF) ||
                                             ($plugin['ePCM'] && ($_SESSION['ePCM']['isAdmin'] != '' || $_SESSION['ePCM']['isUser'] != '') && $_SESSION['UserType']==USERTYPE_STAFF) ||
                                             ($plugin['DigitalChannels'] && $ldigitalchannels->isAlbumEditable() && $_SESSION['UserType']== USERTYPE_STAFF)
                                             ){
                                                 $ResourcesMgmt = 1;
                                         }
                                         $customizationModuleMgmtIn_eAdmin = false; //flag to display eAdmin menu , default = false
                                         $customizationModuleMgmtIn_eAdmin_StudentMgmt = false; //flag to display eAdmin >> student mgmt menu , default = false
                                         $canAccess_CY_awardScheme =false;
                                         if($sys_custom['chuen_yuen_award_scheme'] && $_SESSION['UserType'] == USERTYPE_STAFF){
                                             //case 2012-0216-1210-32073 , once with this plugin and is a USERTYPE_STAFF , client can access this module
                                             $canAccess_CY_awardScheme = true;

                                             //may skip this checking
                                             //	include_once($intranet_root."/includes/libawardscheme.php");
                                             //	$objCY_awardScheme = new libawardscheme();
                                             //	$adminUserList = $objCY_awardScheme->GET_ADMIN_USER();
                                             //	if(trim($adminUserList) != ''){
                                             //		$cy_adminAry = explode(',',$adminUserList);
                                             //		if(in_array($UserID,(array)$cy_adminAry)){
                                             //			$canAccess_CY_awardScheme = true;
                                             //		}
                                             //	}

                                             if($canAccess_CY_awardScheme){
                                                 $customizationModuleMgmtIn_eAdmin= true;
                                                 $customizationModuleMgmtIn_eAdmin_StudentMgmt = true;
                                             }
                                         }

                                         if($AccountMgmt || $GeneralMgmt || $StaffMgmt || $StudentMgmt || $ResourcesMgmt || $customizationModuleMgmtIn_eAdmin)
                                         {
                                             $eAdminMenu_i = $eClassMenu_i + 1;

                                             # standard menu for IP 2.5
                                             $FullMenuArr[$eAdminMenu_i][0] = array(3, "#", $Lang['Header']['Menu']['eAdmin'], $CurrentPageArr['eAdmin'], 'common',"eAdmin");

                                             $eAdmin_i = 0;

                                             ### eAdmin -> Account Management
                                             if($AccountMgmt) {

                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(38, "#", $Lang['Header']['Menu']['AccountMgmt'], $CurrentPageArr['AccountManagement'],"","AccountManagement");
                                                 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Alumni"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting) {

                                                     if($special_feature['alumni'])
                                                     {
                                                         if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"]  || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(386, "/home/eAdmin/AccountMgmt/AlumniMgmt/?clearCoo=1", $Lang['Header']['Menu']['AlumniAccount'], $CurrentPageArr['AlumniMgmt'],"","AlumniMgmt");
                                                     }

                                                     if ($sys_custom['SISUserManagement'])
                                                     {
                                                         if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(383, "/home/eAdmin/AccountMgmt/ParentMgmt/index_sis.php?clearCoo=1", $Lang['Header']['Menu']['ParentAccount'], $CurrentPageArr['ParentMgmt'],"","ParentMgmt");
                                                     }else{
                                                         if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Parent"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$sys_custom['eAdmin']['AcctMgmt']['DisableParentModule'])
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(383, "/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1", $Lang['Header']['Menu']['ParentAccount'], $CurrentPageArr['ParentMgmt'],"","ParentMgmt");
                                                     }

                                                     if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Staff"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(382, "/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1", $Lang['Header']['Menu']['StaffAccount'], $CurrentPageArr['StaffMgmt'],"","StaffMgmt");

                                                         if ($sys_custom['SISUserManagement'])
                                                         {
                                                             if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting)
                                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(381, "/home/eAdmin/AccountMgmt/StudentMgmt/index_sis.php?clearCoo=1", $Lang['Header']['Menu']['StudentAccount'], $CurrentPageArr['StudentMgmt'],"","StudentMgmt");
                                                         }else{
                                                             if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_Student"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $canAccessOfficalPhotoSetting) && !$sys_custom['eAdmin']['AcctMgmt']['DisableStudentModule'])
                                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(381, "/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1", $Lang['Header']['Menu']['StudentAccount'], $CurrentPageArr['StudentMgmt'],"","StudentMgmt");
                                                         }

                                                 }

                                                 if($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY'])))
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(384, "/home/eAdmin/AccountMgmt/StudentRegistry/", $Lang['Header']['Menu']['StudentRegistry'], $CurrentPageArr['StudentRegistry'],"","StudentRegistry");
                                                     if($plugin['imail_gamma']==true && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SharedMailBox"])
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(385, "/home/eAdmin/AccountMgmt/SharedMailBox/", $Lang['Header']['Menu']['SharedMailBox'], $CurrentPageArr['SharedMailBox'],"","SharedMailBox");
                                                         $eAdmin_i++;
                                             }

                                             ### eAdmin -> GeneralManagement
                                             if($GeneralMgmt)
                                             {

                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(34, "#", $Lang['Header']['Menu']['GeneralManagement'], $CurrentPageArr['GeneralManagement'],"","GeneralManagement");

                                                 ### eAdmin -> GeneralManagement -> eClass App
                                                 if(
                                                     ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] && $plugin['eClassApp'])	// eClass App admin
                                                     || ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] && $plugin['eClassTeacherApp'])	// Teacher App admin
                                                     || ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] && $plugin['eClassStudentApp'])	// Student App admin
                                                     || (($plugin['eClassApp'] || $plugin['eClassTeacherApp']) && $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"])	// Normal user but message group admin
                                                 	 || ($plugin['eClassApp'] && $_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher']) // Class Teacher Only
                                                 	 )
                                                 {
                                                     //			if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] && $plugin['eClassApp']) {
                                                     //				$appType = 'P';
                                                     //			}
                                                     //			else if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] && $plugin['eClassTeacherApp']) {
                                                     //				$appType = 'T';
                                                         //			}
                                                         //			else if (($plugin['eClassApp'] || $plugin['eClassTeacherApp']) && $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"]) {
                                                         //				$appType = 'C';
                                                         //			}

                                                         if ($sys_custom['eClassApp']['SFOC']) {
                                                             // go to school info page directly for SFOC project
                                                             $eclassAppPage = '/home/eAdmin/GeneralMgmt/eClassApp/?appType=C&task=commonFunction/school_info/index';

                                                             if($_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"]){
                                                             	$eclassAppPage = '/home/eAdmin/GeneralMgmt/eClassApp/';
                                                             }
                                                         }
                                                         else {
                                                             $eclassAppPage = '/home/eAdmin/GeneralMgmt/eClassApp/';
                                                             if($_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"]){
                                                             	$eclassAppPage = '/home/eAdmin/GeneralMgmt/eClassApp/';
                                                             }
                                                         }
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(347, $eclassAppPage, $Lang['Header']['Menu']['eClassApp'], $CurrentPageArr['eClassApp'],"","eClassApp");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> ePayment
                                                 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePayment"] && $plugin['payment'])
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(344, "/home/eAdmin/GeneralMgmt/payment/", $Lang['Header']['Menu']['ePayment'], $CurrentPageArr['ePayment'],"","ePayment");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> ePayment
                                                 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] && $plugin['ePOS'] && $plugin['payment'])
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(345, "/home/eAdmin/GeneralMgmt/pos/", $Lang['Header']['Menu']['ePOS'], $CurrentPageArr['ePOS'],"","ePOS");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> eSurvey
                                                 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(343, "/home/eAdmin/GeneralMgmt/eSurvey/", $Lang['Header']['Menu']['eSurvey'] , $CurrentPageArr['eAdmineSurvey'],"","eAdmineSurvey");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> MessageCenter
                                                 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"]
                                                     || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] || $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"])
                                                 {
                                                     if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-EmailMerge"])
                                                     {
                                                         $mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/MassMailing/";
                                                     } elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-SMS"])
                                                     {
                                                         $mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/SMS/";
                                                     } elseif ($plugin['eClassApp'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-ParentAppNotify"] ||
                                                         $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_classTeacher"] ||
                                                         $_SESSION["SSV_PRIVILEGE"]["MessageCenter"]["allowSendPushMessage_clubAndActivityPIC"]))
                                                     {
                                                         $mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/ParentNotification/";
                                                     } elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-TeacherAppNotify"])
                                                     {
                                                         $mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/TeacherNotification/";
                                                     }
                                                     elseif ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAppNotify"])
                                                     {
                                                         $mc_first_page = "/home/eAdmin/GeneralMgmt/MessageCenter/StudentNotification/";
                                                     }
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(346, $mc_first_page, $Lang['Header']['Menu']['MessageCenter'], $CurrentPageArr['eAdmineMassMailing'],"","eAdmineMassMailing");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> ePolling
                                                 if($_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"])
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(341, "/home/eAdmin/GeneralMgmt/polling/", $Lang['Header']['Menu']['ePolling'], $CurrentPageArr['ePolling'],"","ePolling");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> printing
                                                 if (($_SESSION['UserType'] == USERTYPE_STAFF) && $plugin['mssch_printing_module']) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(348, "/home/eAdmin/GeneralMgmt/MsschPrinting/", $Lang['Header']['Menu']['msschPrinting'], $CurrentPageArr['mssch_printing'],"","msschPrinting");
                                                 }

                                                 ### eAdmin -> GeneralManagement -> schoolNews
                                                 if (($_SESSION["SSV_USER_ACCESS"]["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]) && !$plugin['DisableNews']) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(342, "/home/eAdmin/GeneralMgmt/schoolnews/", $Lang['Header']['Menu']['schoolNews'], $CurrentPageArr['schoolNews'],"","schoolNews");
                                                 }
                                                 if ($sys_custom['eClassApp']['SFOC']) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(342, "/home/eAdmin/GeneralMgmt/MedalList/management/", $Lang['Header']['Menu']['MedalList'], $CurrentPageArr['medalList'],"","medalList");
                                                 }

                                                 $eAdmin_i++;
                                             }

                                             ### eAdmin -> StaffManagement
                                             if($StaffMgmt)
                                             {
                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(31, "#", $Lang['Header']['Menu']['StaffManagement'], $CurrentPageArr['StaffManagement'],"","StaffManagement");

                                                 /*
                                                  if($plugin['attendancestaff'])
                                                  {
                                                  $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(311, "#", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StaffeAttednance']);
                                                  }
                                                  */

                                                 // staff attendance v3
                                                 if ($module_version['StaffAttendance'] == 3.0 && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-StaffAttendance"] || $_SESSION["SSV_PRIVILEGE"]["StaffAttendance3"]["has_access_right"])) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(313, "/home/eAdmin/StaffMgmt/attendance/", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['eAdminStaffAttendance'],"","eAdminStaffAttendance");
                                                 }

                                                 if($special_feature['circular'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"] || (!$_SESSION["SSV_PRIVILEGE"]["circular"]["disabled"] && $_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"])))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(312, "/home/eAdmin/StaffMgmt/circular/", $Lang['Header']['Menu']['eCircular'], $CurrentPageArr['eAdminCircular'],"", "eAdminCircular");
                                                 }

                                                 if ($plugin['eForm'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eForm"]) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(317, "/home/eAdmin/StaffMgmt/eForm/", $Lang['Header']['Menu']['eForm'], $CurrentPageArr['eAdmineForm'],"", "eAdmineForm");
                                                 }

                                                 if ($plugin['SLRS'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-SLRS"]) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(314, "/home/eAdmin/StaffMgmt/slrs/", $Lang['Header']['Menu']['SLRS'], $CurrentPageArr['eAdminSLRS'],"", "eAdminSLRS");
                                                 }

                                                 if ($plugin['eAppraisal'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eAppraisal"]) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(315, "/home/eAdmin/StaffMgmt/appraisal/?init=eadmin", $Lang['Header']['Menu']['eAppraisal'], $CurrentPageArr['eAdmineAppraisal'],"", "eAdmineAppraisal");
                                                 }

                                                 if ($plugin['TeacherPortfolio'] && ($_SESSION["SSV_USER_ACCESS"]["other-TeacherPortfolio"] || $_SESSION["SSV_PRIVILEGE"]["TeacherPortfolio"]["isViewerUser"])) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(316, 'javascript:newWindow("/home/eAdmin/StaffMgmt/teacher_portfolio/",8);', $Lang['Header']['Menu']['TeacherPortfolio'], $CurrentPageArr['eAdminTeacherPortfolio'],"", "eAdminTeacherPortfolio");
                                                 }

                                                 $eAdmin_i++;
                                             }

                                             ### eAdmin -> StudentManagement
                                             if (($StudentMgmt || $customizationModuleMgmtIn_eAdmin_StudentMgmt) ) {
                                                 $eAdmin_StudentMgmt_i = 0;
                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(32, "#", $Lang['Header']['Menu']['StudentManagement'], $CurrentPageArr['StudentManagement'],"","StudentManagement");

                                                 ### eAdmin -> StudentManagement -> award scheme ( in medical module)
                                                 if($plugin['medical'] && $plugin['medical_module']['awardscheme'] && ($_SESSION["SSV_PRIVILEGE"]["medical"]["isMedicalUser"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-medical"]))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(320, "/home/eAdmin/StudentMgmt/medical/?t=management.award_list&clearCoo=1", $Lang['Header']['Menu']['AwardScheme'], $CurrentPageArr['awardscheme'],"",'awardscheme');
                                                     $eAdmin_StudentMgmt_i++;
                                                 }

                                                 ### eAdmin -> StudentManagement -> eAttendance
                                                 if (($plugin['attendancestudent'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"]) || ($plugin['attendancelesson'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-LessonAttendance"])) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(321, "/home/eAdmin/StudentMgmt/attendance/", $Lang['Header']['Menu']['eAttednance'], $CurrentPageArr['StudentAttendance'],"","StudentAttendance");
                                                     $eAdmin_StudentMgmt_i++;
                                                 }

                                                 ### eAdmin -> StudentManagement -> eDis
                                                 if($plugin['Disciplinev12'] && (($_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) || $_SESSION["SSV_PRIVILEGE"]["disciplinev12"]["has_access_right"]))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(322, "/home/eAdmin/StudentMgmt/disciplinev12/overview/", $Lang['Header']['Menu']['eDiscipline'], $CurrentPageArr['eDisciplinev12'],"","eDisciplinev12");
                                                     $eAdmin_StudentMgmt_i++;
                                                 }

                                                 ### eAdmin -> StudentManagement -> eEnrolment
                                                 if($plugin['eEnrollment'])
                                                 {
                                                     if ($show_eAdmin_eEnrol)
                                                     {
                                                         $eEnrolmentTitle = $plugin['eEnrollmentLite'] ? $Lang['Header']['Menu']['eEnrolmentLite'] : $Lang['Header']['Menu']['eEnrolment'];
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(323, "/home/eAdmin/StudentMgmt/enrollment/", $eEnrolmentTitle, $CurrentPageArr['eEnrolment'],"","eEnrolment");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }
                                                 }

                                                 ### eAdmin -> StudentManagement -> eGuidance
                                                 if($plugin['eGuidance'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-eGuidance'] || $_SESSION['eGuidance']['current_right'] || $_SESSION['eGuidance']['settings']['isClassTeacher'] || $_SESSION['eGuidance']['settings']['isSubjectTeacher']))
                                                 {
                                                     if(true){
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(336, "/home/eAdmin/StudentMgmt/eGuidance/", $Lang['Header']['Menu']['eGuidance'], $CurrentPageArr['eGuidance'],"",'eGuidance');
                                                         $eAdmin_StudentMgmt_i++;
                                                     }
                                                 }

                                                 ### eAdmin -> StudentManagement -> eHomework
                                                 //if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_access"])))
                                                 //if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] || (!$_SESSION["SSV_PRIVILEGE"]["homework"]["disabled"] && ($_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] || $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] || $_SESSION['isTeaching'])))
                                                     //echo $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"].'/';
                                                     if ($plugin['DisableHomework']) {
                                                         // hide homework button
                                                     }
                                                     else if(
                                                         (
                                                             $_SESSION["SSV_USER_ACCESS"]["eAdmin-eHomework"] ||
                                                             $_SESSION["SSV_PRIVILEGE"]["homework"]["is_admin"] ||
                                                             $_SESSION["SSV_PRIVILEGE"]["homework"]["is_subject_leader"] ||
                                                             $_SESSION['isTeaching'] ||
                                                             ($_SESSION['UserType']==USERTYPE_STAFF && !$_SESSION['isTeaching'] && $_SESSION["SSV_PRIVILEGE"]["homework"]["nonteachingAllowed"]) ||
                                                             $lhomework->isViewerGroupMember($UserID)
                                                             ) || $_SESSION['SSV_USER_ACCESS']['eAdmin-eHomework'])
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(324, "/home/eAdmin/StudentMgmt/homework/index.php", $Lang['Header']['Menu']['eHomework'], $CurrentPageArr['eAdminHomework'],"","eAdminHomework");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> eNotice
                                                     if($plugin['notice'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasIssueRight"]))
                                                     {
                                                         // [2020-0604-1821-16170] seperate issue right checking
                                                         //$AccessNoticeAdminLink = $sys_custom['eNotice']['HideParenteNotice']? "/home/eAdmin/StudentMgmt/notice/student_notice/" : "/home/eAdmin/StudentMgmt/notice/";
                                                         if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eNotice"] || $_SESSION["SSV_PRIVILEGE"]["notice"]["hasSchoolNoticeIssueRight"]) {
                                                             $AccessNoticeAdminLink = $sys_custom['eNotice']['HideParenteNotice'] ? "/home/eAdmin/StudentMgmt/notice/student_notice/" : "/home/eAdmin/StudentMgmt/notice/";
                                                         } else {
                                                             $AccessNoticeAdminLink = "/home/eAdmin/StudentMgmt/notice/payment_notice/paymentNotice.php";
                                                         }
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(325, $AccessNoticeAdminLink, $Lang['Header']['Menu']['eNotice'], $CurrentPageArr['eAdminNotice'],"","eAdminNotice");
                                                         //$FullMenuArr[$eAdminMenu_i][1][1][1][4][1][] = array(3276, "#", "test sub sub sub menu XD", $CurrentPageArr['eNotice']);	# pls don't delete, ref usage
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> eRC 1.0
                                                     if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard1.0"] && $plugin['ReportCard'])
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(326, "/home/eAdmin/StudentMgmt/reportcard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard1.0'],"","eReportCard1.0");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> eRC
                                                     if(($_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard"] || ($sys_custom['eRC']['Settings']['ViewGroupUserType'] && $_SESSION["SSV_PRIVILEGE"]["reportcard"]["is_view_group_memeber"])) && $plugin['ReportCard2008'])
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(327, "/home/redirect.php?mode=1&url=/home/eAdmin/StudentMgmt/eReportCard/", $Lang['Header']['Menu']['eReportCard'], $CurrentPageArr['eReportCard'],"","eReportCard");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> eRC (Rubrics)
                                                     if($show_eAdmin_ReportCardRubrics && $plugin['ReportCard_Rubrics'])
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(328, "/home/eAdmin/StudentMgmt/reportcard_rubrics/index.php?from_eAdmin=1", $Lang['Header']['Menu']['eReportCard_Rubrics'], $CurrentPageArr['eReportCard_Rubrics'],"","eReportCard_Rubrics");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> eRC (Kindergarten)
                                                     if($plugin['ReportCardKindergarten'] && $show_eAdmin_ReportCardKindergarten) {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(335, "/home/eAdmin/StudentMgmt/reportcard_kindergarten/index.php?from_eAdmin=1", $Lang['Header']['Menu']['eReportCardKindergarten'], $CurrentPageArr['eReportCardKindergarten_eAdmin'],"","eReportCardKindergarten");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> eSchoolBus
                                                     if($plugin['eSchoolBus'])
                                                     {
                                                         if(true){
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(337, "/home/eAdmin/StudentMgmt/eSchoolBus/", $Lang['Header']['Menu']['eSchoolBus'], $CurrentPageArr['eSchoolBus'],"",'eSchoolBus');
                                                             $eAdmin_StudentMgmt_i++;
                                                         }
                                                     }

                                                     ### eAdmin -> StudentManagement -> eSports
                                                     if(($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) || ($plugin['Sports'] && $sys_custom['eSports']['KaoYipRelaySettings'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"]) ||
                                                            ($plugin['swimming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"]))
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][0] = array(329, "#", $Lang['Header']['Menu']['eSports'], $CurrentPageArr['eSports'],"","eSports");
                                                         if($plugin['Sports'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["isAdminOrHelper"]) {
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3291, "/home/eAdmin/StudentMgmt/eSports/sports/index.php", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['SportDay'],"","SportDay");
                                                         } else if ($plugin['Sports'] && $sys_custom['eSports']['KaoYipRelaySettings'] && $_SESSION["SSV_PRIVILEGE"]["eSports"]["is_class_teacher"]) {
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3291, "/home/eAdmin/StudentMgmt/eSports/sports/arrange/class_relay_enrol.php", $Lang['Header']['Menu']['SportDay'], $CurrentPageArr['SportDay'],"","SportDay");
                                                         }
                                                         if($plugin['swimming_gala'] && $_SESSION["SSV_PRIVILEGE"]["swimminggala"]["isAdminOrHelper"]) {
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i][1][] = array(3292, "/home/eAdmin/StudentMgmt/eSports/swimming_gala/index.php", $Lang['Header']['Menu']['SwimmingGala'], $CurrentPageArr['SwimmingGala'], "", "SwimmingGala");
                                                         }
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> Chuen Yuen Award Scheme
                                                     if($canAccess_CY_awardScheme && $sys_custom['chuen_yuen_award_scheme'] && $_SESSION['UserType'] == USERTYPE_STAFF)
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(330, "/home/award_scheme/", $Lang['customization']['award_scheme'], '');
                                                         $eAdmin_StudentMgmt_i++;
                                                     }

                                                     ### eAdmin -> StudentManagement -> Class Diary
                                                     if($plugin['ClassDiary'] && $_SESSION['UserType'] == USERTYPE_STAFF && $_SESSION['isTeaching']==1 /*&& $_SESSION["SSV_USER_ACCESS"]["eAdmin-ClassDiary"]*/)
                                                     {
                                                         $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(331, "/home/eAdmin/StudentMgmt/classdiary/", $Lang['Header']['Menu']['ClassDiary'], $CurrentPageArr['ClassDiary'] ,"", "ClassDiary");
                                                         $eAdmin_StudentMgmt_i++;
                                                     }
                                                     ### eAdmin -> StudentManagement -> medical
                                                     if($plugin['medical'])
                                                     {
                                                         if($_SESSION["SSV_PRIVILEGE"]["medical"]["isMedicalUser"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-medical"]){
                                                             $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][$eAdmin_StudentMgmt_i] = array(338, "/home/eAdmin/StudentMgmt/medical/?t=index.index", $Lang['Header']['Menu']['Medical'], $CurrentPageArr['medical'],"",'medical');
                                                             $eAdmin_StudentMgmt_i++;
                                                         }
                                                     }


                                                     $eAdmin_i++;
                                             }

                                             ### eAdmin -> ResourcesManagement
                                             if ($ResourcesMgmt)
                                             {
                                                 $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][0] = array(32, "#", $Lang['Header']['Menu']['ResourcesManagement'], $CurrentPageArr['ResourcesManagement'],"","ResourcesManagement");

                                                 if($plugin['digital_archive'] && $_SESSION['UserType']==USERTYPE_STAFF) {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/", $Lang['Header']['Menu']['DigitalArchive'], $CurrentPageArr['DigitalArchive'],"","DigitalArchive");
                                                 }

                                                 # Digital Channels
                                                 if($plugin['DigitalChannels'] && $ldigitalchannels->isAlbumEditable() && $_SESSION['UserType']== USERTYPE_STAFF){
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/DigitalChannels/?clearCoo=1", $Lang['Header']['Menu']['DigitalChannels'], $CurrentPageArr['DigitalChannels'], "", "DigitalChannels");
                                                 }

                                                 # Dcoument Routing
                                                 //if($plugin['DocRouting'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-DocRouting']) && $_SESSION['UserType']== USERTYPE_STAFF){
                                                 if($plugin['DocRouting'] && $_SESSION['UserType']== USERTYPE_STAFF){
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/DocRouting/", $Lang['DocRouting']['DocRouting'], $CurrentPageArr['DocRouting'], "", "DocRouting");
                                                 }

                                                 ### eAdmin -> ResourcesManagement -> eBooking
                                                 if($plugin['eBooking'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eBooking"] || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER"))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(332, "/home/eAdmin/ResourcesMgmt/eBooking/", $Lang['Header']['Menu']['eBooking'], $CurrentPageArr['eBooking'],"","eBooking");
                                                 }

                                                 ### eAdmin -> ResourcesManagement -> eInventory
                                                 if($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != ""))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(331, "/home/eAdmin/ResourcesMgmt/eInventory/", $Lang['Header']['Menu']['eInventory'], $CurrentPageArr['eInventory'],"","eInventory");
                                                 }

                                                 ### eAdmin -> ResourcesManagement -> eProcurement
                                                 if ($plugin['ePCM'] && ($_SESSION['ePCM']['isAdmin'] != '' || $_SESSION['ePCM']['isUser'] != ''))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(334, "/home/eAdmin/ResourcesMgmt/eProcurement/", $Lang['Header']['Menu']['ePCM'], $CurrentPageArr['ePCM'],"","ePCM");
                                                 }

                                                 ### eAdmin -> ResourcesManagement -> Invoice Mgmt System (customization for Fukien Secondary School)
                                                 //include_once($PATH_WRT_ROOT."includes/libinvoice.php");
                                                 //$linvoice = new libinvoice();
                                                 if($plugin['Inventory'] && $sys_custom['Invoice2Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "" || $linvoice->isViewerGroupMember))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(333, "/home/eAdmin/ResourcesMgmt/InvoiceMgmtSystem/", $Lang['Header']['Menu']['InvoiceMgmtSystem'], $CurrentPageArr['InvoiceMgmtSystem'],"","InvoiceMgmtSystem");
                                                 }

                                                 ### eAdmin -> ResourcesManagement -> Repair System
                                                 //if($plugin['RepairSystem'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-RepairSystem"]))
                                                 if (isset($plugin['RepairSystem']) && $plugin['RepairSystem'] && ($_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] || $lrepairsystem->userInMgmtGroup($UserID)>0))
                                                 {
                                                     $FullMenuArr[$eAdminMenu_i][1][$eAdmin_i][1][] = array(331, "/home/eAdmin/ResourcesMgmt/RepairSystem/", $Lang['Header']['Menu']['RepairSystem'], $CurrentPageArr['eAdminRepairSystem'],"","eAdminRepairSystem");
                                                 }
                                                 }

                                                 $SchoolSettingsMenu_i = $eAdminMenu_i + 1;
                                             }
                                             else
                                                 $SchoolSettingsMenu_i = $eClassMenu_i;


                                                 if ($plugin['DisableTimeTable'])
                                                 {
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"] = false;
                                                 }

                                                 ### School Settings
                                                 if(	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"] ||
                                                     $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"]
                                                     )
                                                 {
                                                     $FullMenuArr[$SchoolSettingsMenu_i][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting', "SchoolSettings");
                                                     if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]) /* && ($_SESSION["platform"]!="KIS" || $plugin['Inventory']) */ )
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(41, $PATH_WRT_ROOT_ABS."home/system_settings/location/", $Lang['Header']['Menu']['Site'], $CurrentPageArr['Location'],"","Location");
                                                     }
                                                     if(!$plugin['DisableClass'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"]))
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(44, $PATH_WRT_ROOT_ABS."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class'],"","Class");
                                                     }
                                                     if(!$plugin['DisableGroup'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"]))
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(45, $PATH_WRT_ROOT_ABS."home/system_settings/group/?clearCoo=1", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group'],"","Group");
                                                     }
                                                     if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT_ABS."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role'],"","Role");
                                                     }
                                                     if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT_ABS."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar'],"","SchoolCalendar");
                                                     }
                                                     if(!$sys_custom['project']['CourseTraining']['Amway'] && !$sys_custom['project']['CourseTraining']['Oaks'] && !$sys_custom['project']['CourseTraining']['CEM'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-OrganizationInfo"]))
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(49, $PATH_WRT_ROOT_ABS."home/system_settings/organization_info/", $Lang['Header']['Menu']['OrganizationInfo'], $CurrentPageArr['OrganizationInfo'],"","OrganizationInfo");
                                                     }
                                                     if(!$plugin['DisableSubject'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) )
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(43, $PATH_WRT_ROOT_ABS."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subject'], $CurrentPageArr['Subjects'],"","Subjects");
                                                     }
                                                     if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                                     {
                                                         if ($sys_custom['project']['HKPF']) {
                                                             $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(48, $PATH_WRT_ROOT_ABS."home/system_settings/security/login_records.php", $Lang['SysMgr']['Security']['Title'], $CurrentPageArr['SystemSecurity'],"","SystemSecurity");
                                                         }
                                                         else {
                                                             $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(48, $PATH_WRT_ROOT_ABS."home/system_settings/security/password_policy.php", $Lang['SysMgr']['Security']['Title'], $CurrentPageArr['SystemSecurity'],"","SystemSecurity");
                                                         }
                                                     }
                                                     if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) && !$plugin['DisableTimeTable'])
                                                     {
                                                         $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/index.php?clearCoo=1", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable'],"","Timetable");
                                                     }
                                                     //$FullMenuArr[4][1][] = array(48, $PATH_WRT_ROOT_ABS."home/system_settings/period/", $Lang['Header']['Menu']['Period'], $CurrentPageArr['Period']);
                                                 }

                                                 if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] && !isset($iNewCampusMail))
                                                 {
                                                     # check any new message from DB
                                                     include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
                                                     if (!isset($header_lc))
                                                     {
                                                         $header_lc = new libcampusmail();
                                                     }

                                                     # no need to count for iMail Gamma/Plus
                                                     if (!$plugin['imail_gamma'])
                                                     {
                                                         if ($special_feature['imail'])
                                                             $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
                                                             else
                                                                 $iNewCampusMail = $header_lc->returnNumNewMessage($UserID);
                                                     }

                                                     # only check webmail if there is no new mail from DB!
                                                     if ($iNewCampusMail<=0 && !$plugin['imail_gamma'])
                                                     {
                                                         include_once($PATH_WRT_ROOT."includes/libwebmail.php");
                                                         if (!isset($lwebmail))
                                                         {
                                                             $lwebmail = new libwebmail();
                                                         }

                                                         # Check preference of check email
                                                         $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
                                                         $temp = $header_lc->returnArray($sql,1);
                                                         $optionSkipCheckEmail = $temp[0][0];
                                                         if ($optionSkipCheckEmail)
                                                         {
                                                             $skipCheck = true;
                                                         }
                                                         else
                                                         {
                                                             $skipCheck = false;
                                                         }

                                                         if (!$skipCheck && $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] && $lwebmail->type==3 && !$_SESSION["EmailLoginFailure".$lu->UserLogin])
                                                         {
                                                             if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
                                                             {
                                                                 $exmail_count = $lwebmail->checkMailCount();
                                                                 $lwebmail->close();
                                                                 $iNewCampusMail += $exmail_count;
                                                             } else
                                                             {
                                                                 # mark the user's email authenticate
                                                                 # and don't connect to mail server next time (to avoid slow performance)
                                                                 $_SESSION["EmailLoginFailure".$lu->UserLogin] = true;
                                                             }
                                                         }
                                                     }
                                                 }
                                                 # determine iMail icon for display
                                                 $iMailClass = ($iNewCampusMail>0) ? "itool_imail_new" : "itool_imail";



                                                 ### Check any iFolder ###
                                                 $access2iFile = false;
                                                 if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]['aerodrive']) && $_SESSION["SSV_PRIVILEGE"]["plugin"]['aerodrive'])
                                                 {
                                                     $access2iFile = true;
                                                 }

                                                 if (isset($_SESSION["SSV_PRIVILEGE"]["plugin"]['personalfile']) && $_SESSION["SSV_PRIVILEGE"]["plugin"]['personalfile'])
                                                 {
                                                     if ($personalfile_type == 'AERO')
                                                     {
                                                         #$moving_layer_text .= $link_personalFiles;
                                                         #$layer_length -= 25;
                                                     }
                                                     else
                                                     {
                                                         include_once("$intranet_root/includes/libftp.php");
                                                         include_once ($PATH_WRT_ROOT."includes/libsystemaccess.php");
                                                         $header_lsysacc = new libsystemaccess($UserID);
                                                         $header_lftp = new libftp();
                                                         if ($header_lsysacc->hasFilesRight())
                                                         {
                                                             $access2iFile = true;
                                                         }
                                                     }
                                                 }



                                                 ### UI preparation
                                                 $bubbleMsgBlock =<<<campusLink

	<div class="sub_layer_board" id="sub_layer_campus_link" style="visibility:hidden;z-index:100">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br  style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_campus_link','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a></div>
			</div>
			<div class="bubble_board_03">
				<div class="bubble_board_04">
					<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock"></span>
					</div>
					<div id="bubble_board_content">

					</div>
				</div>
			</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>
	<div class="sub_layer_board" id="sub_layer_power_tool" style="visibility:hidden;z-index:100;width:180px;">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br  style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_power_tool','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a></div>
			</div>
			<div class="bubble_board_03">
				<div class="bubble_board_04">
					<div style="position: absolute;width:225px"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock"></span>
					</div>
					<div id="bubble_board_content">
						<div class="powertool_list">
					<!--<a class="tool_powervoice" href="#">PowerVoice</a>-->
					<a onclick="newWindow('/home/plugin/powerspeech',16)" class="tool_powerspeech" href="#">{$Lang['Header']['Menu']['PowerSpeech']}</a>
                    <!--<a class="tool_powerconcept" href="#">PowerConcept</a>
                    <a class="tool_powerboard" href="#">PowerBoard</a>-->
                 </div>
					</div>
				</div>
			</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>
campusLink;




                                                 ### UI preparation for about system
                                                 if(in_array(get_client_region(), array('zh_HK', 'zh_MO')) && $_SESSION['UserType']==USERTYPE_STAFF){
                                                     $reprintcard_link = '<a href="/home/eClassStore/redirect.php" target="_blank" class="help_reprintcard" title="'.$Lang['Header']['ReprintCardSystem'].'"><span></span>'.$Lang['Header']['ReprintCardSystem'].'</a>';
                                                     if($plugin['iPortfolio'])
                                                     {
                                                         $iportbuy_link = '<a href="/home/eClassStore/redirect.php?module=iPortfolio" target="_blank" class="help_iportbuy" title="'.$Lang['Header']['iPortbuy'].'"><span></span>'.$Lang['Header']['iPortbuy'].'</a>';
                                                     }
                                                 }

                                                 if($_SERVER['HTTP_HOST'] == '192.168.0.171:31002' && $_SESSION['UserType']==USERTYPE_STAFF){
                                                     $flipchan_path = "/home/asp/flipchan.php?OnlyCsChannels=1";
                                                     $flipped_channels_link = '<a href="javascript:newWindow(\''.$flipchan_path.'\', 36);" target="_blank" class="help_cschannel" title="'.$Lang['General']['FlippedChannels'].'"><span></span>'.$Lang['General']['FlippedChannels'].'</a>';
                                                 }

                                                 $arrow_pos = $intranet_session_language=="en" ? 42 : 57;
                                                 $bubbleMsgBlockSysAbout =<<<sysAbout

	<div class="sub_layer_board" id="sub_layer_sys_about" style="visibility:hidden;z-index:100;width:246px">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<span style="width:{$arrow_pos}px;float:right">&nbsp;</span>
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<a href="#" title="{$Lang['Btn']['Close']}" onclick="MM_showHideLayers('sub_layer_sys_about','','hide')"><img src="/images/2020a/addon_tools/close_layer.gif" border="0" style="float:right"/></a>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
			<h1>{$i_eu_current_ver}<br />
                        <a href="#"><img src="/images/2020a/logo_eclass_footer.gif" align="absmiddle" border="0" /> <a href="javascript:products()"> {$eClassVersion}</a></a><br />
                        </h1>
               <div class="eclasshelp_list">
						<!--<a href="javascript:support()" class="help_community"><span></span> {$Lang['Header']['eClassCommunity']}</a>-->
						<!--<a href="javascript:onlinehelpcenter()" class="help_onlinehelp"><span></span> {$Lang['Header']['OnlineHelp']}</a>-->
                        <a href="javascript:doccenter()" class="help_document"><span></span> {$Lang['Header']['OnlineDocs']}</a>
                        <!--<a href="javascript:videocenter()" class="help_video"><span></span> {$Lang['Header']['VideoTutorial']}</a>-->
                        {$reprintcard_link}
                        {$iportbuy_link}
                        {$flipped_channels_link}
                        <!--<a href="javascript:faq()" class="help_faq"><span></span> {$Lang['Header']['FAQ']}</a>-->
				</div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"></div>
		</div>
	</div>

sysAbout;



                        ### UI preparation for system timeout
                        if ($ControlTimeOutByJS)
                        {
                            $bubbleMsgBlockSysTimeout =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
	<span id="ajaxMsgBlockTimeOut">
				<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">
					<tr>
					<td bgcolor="orange" align="center">
					<table width="620" height="400" border="0" cellpadding="12" cellspacing="0">
						<tr>
						<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>
						&nbsp;
						</td>
						</tr>
					</table>
					</td>
					</tr>
				</table>
				</span>
	</div>

sysTimeout;



                            $bubbleMsgBlockSysTimeout2 =<<<sysTimeout

	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">
		<div class="bubble_board_01">
			<div class="bubble_board_02">
				<span style="width:37px;float:right">&nbsp;</span>
				<img style="float:right" src="/images/2020a/addon_tools/sub_layer_arrow.gif" width="16" height="12" />
				<br style="clear:both;" />
				<table width="620" height="5" border="0">
					<tr>
					<td>&nbsp;</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="bubble_board_03">
			<div class="bubble_board_04">
				<div style="position: absolute;"><span class="tabletext" style="float:right; background-color:red; color:white;display:none; padding:2px" id="ajaxMsgBlock3"></span>
				</div>
				<div id="bubble_board_content3" style="text-align:center" >
				<table width="620" height="400" border="0">
					<tr>
					<td><font size="5"><b>
					<!--System is about to time out in 60 seconds. You can press <a href="javascript:MM_showHideLayers('sub_layer_sys_timeout','','hide');CancelTimeOut()">CONTINUE</a> to cancel the timeout and continue your use in eClass.-->
					<font color="red">System is about to time out in 60 seconds.</font><br />You can press <input style="font-size:23px" type="button" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers('sub_layer_sys_timeout','','hide');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.
					</b>
					</font>
					</td>
					</tr>
				</table>
				</div>
			</div>
		</div>
		<div class="bubble_board_05">
			<div class="bubble_board_06"><table width="620" height="5" border="0">
					<tr>
					<td>&nbsp;</td>
					</tr>
				</table></div>
		</div>
	</div>

sysTimeout;
                        }

                        //FOR DEVELOPMENT , HARDCODE A LOGIN WITH "xx_iportfolio_xx" SHOULD BE IPORTFOLIO STANDALONE
                        # MENU FOR STANDALONE VERSION OF IPORTFOLIO
                        if (($lu->UserLogin == "ab_iportfolio_cd" && $lu->UserPassword == "123") || (isset($stand_alone['iPortfolio']) && $stand_alone['iPortfolio']))
                        {

                            unset($FullMenuArr);
                            $SchoolSettingsMenu_i = 0;

                            if($stand_alone['iesWithIPortfolio'] &&
                                ($_SESSION["SSV_USER_ACCESS"]["eLearning-IES"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isTeacher"] || $_SESSION["SSV_PRIVILEGE"]["IES"]["isStudent"])){

                                    $FullMenuArr[$SchoolSettingsMenu_i][0] = array(2, "#", $Lang['Header']['Menu']['eLearning'], $CurrentPageArr['eLearning'], 'common', "eLearning" );
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(25, "/home/eLearning/ies/", $Lang['Header']['Menu']['IES'], $CurrentPageArr['IES'],"","IES");
                                    $SchoolSettingsMenu_i = $SchoolSettingsMenu_i + 1;
                            }



                            # pls just provide school settings (without role menu) if the user is the school admin
                            ### School Settings
                            if(	$_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]
                                )
                            {

                                ### eAdmin -> Account Management
                                if($AccountMgmt)
                                {
                                    $FullMenuArr[$SchoolSettingsMenu_i][0] = array(3, "#", $Lang['Header']['Menu']['eAdmin'], $CurrentPageArr['eAdmin'], 'common',"eAdmin");
                                    $eAdmin_i = 0;
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][0] = array(38, "#", $Lang['Header']['Menu']['AccountMgmt'], $CurrentPageArr['AccountManagement']);
                                    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"])
                                    {
                                        $FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(383, "/home/eAdmin/AccountMgmt/ParentMgmt/", $Lang['Header']['Menu']['ParentAccount'], $CurrentPageArr['ParentMgmt'],"","ParentMgmt");

                                        $FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(382, "/home/eAdmin/AccountMgmt/StaffMgmt/", $Lang['Header']['Menu']['StaffAccount'], $CurrentPageArr['StaffMgmt'],"","StaffMgmt");

                                        $FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(381, "/home/eAdmin/AccountMgmt/StudentMgmt/", $Lang['Header']['Menu']['StudentAccount'], $CurrentPageArr['StudentMgmt'],"","StudentMgmt");
                                    }
                                    /*
                                     if($plugin['AccountMgmt_StudentRegistry'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] || isset($_SESSION['SESSION_ACCESS_RIGHT']['STUDENTREGISTRY'])))
                                     $FullMenuArr[$SchoolSettingsMenu_i][1][$eAdmin_i][1][] = array(384, "/home/eAdmin/AccountMgmt/StudentRegistry/", $Lang['Header']['Menu']['StudentRegistry'], $CurrentPageArr['StudentRegistry']);
                                     */
                                     $SchoolSettingsMenu_i = $SchoolSettingsMenu_i + 1;
                                }


                                $FullMenuArr[$SchoolSettingsMenu_i][0] = array(4, "#", $Lang['Header']['Menu']['SchoolSettings'], $CurrentPageArr['SchoolSettings'], 'setting',"", "SchoolSettings");
                                /*
                                 if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"])
                                 {
                                 $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(41, $PATH_WRT_ROOT_ABS."home/system_settings/location/", $Lang['Header']['Menu']['Site'], $CurrentPageArr['Location']);
                                 }
                                 */
                                if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
                                {
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(44, $PATH_WRT_ROOT_ABS."home/system_settings/form_class_management/", $Lang['Header']['Menu']['Class'], $CurrentPageArr['Class'],"","Class");
                                }
                                /*
                                 if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
                                 {
                                 $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(45, $PATH_WRT_ROOT_ABS."home/system_settings/group/", $Lang['Header']['Menu']['Group'], $CurrentPageArr['Group']);
                                 }
                                 */

                                if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                {
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT_ABS."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role'],"","Role");
                                }

                                if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"])
                                {
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT_ABS."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar'],"","SchoolCalendar");
                                }

                                if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"])
                                {
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(43, $PATH_WRT_ROOT_ABS."home/system_settings/subject_class_mapping/", $Lang['Header']['Menu']['Subject'], $CurrentPageArr['Subjects'],"","Subjects");
                                }

                                if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"])
                                {
                                    $FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable'],"","Timetable");
                                }
                            }
                            $menuWithIMail = false;
                            $menuWithECommnuity = false;
                        }

                        $school_logo_link = $PATH_WRT_ROOT_ABS.$indexPage;

                        if($_SESSION['UserType']==USERTYPE_ALUMNI)
                        {
                            include($PATH_WRT_ROOT."/plugins/alumni_conf.php");
                            $school_logo_link = "/home/eCommunity/group/index.php?GroupID=$alumni_GroupID";
                        }

                        $hide_KIS_menu = " style='display:none' ";
                        $hide_StandardLMS_menu = " style='display:none' ";

						$urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];

                        if ($sys_custom['PowerClass'] && strstr($urlSrc, "/home/eAdmin/ResourcesMgmt/DigitalChannels") || $_SESSION["platform"]=="KIS" || (isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=''))
                        {
                            $KISSchoolName = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

                            $hide_menu_now = "style=\"display:none\"";

                            function GenerateKISModuleMenu($KIS_MENUS)
                            {
                                if (sizeof($KIS_MENUS)<1)
                                {
                                    return "&nbsp;";
                                }

                                $ReturnRX = "";
                                for ($i=0; $i<sizeof($KIS_MENUS); $i++)
                                {
                                    list($MenuTitle, $MenuPath, $MenuIsOn) = $KIS_MENUS[$i];
                                    $MenuID = MD5($MenuTitle."_".$MenuPath);
                                    $MenuClass = ($MenuIsOn) ? "common_current" : "common";
                                    $ReturnRX .= "<li style=\"z-index: 9999;\"><a id=\"{$MenuID}\" href=\"{$MenuPath}\" class=\"{$MenuClass}\">{$MenuTitle}</a></li>\n";
                                }

                                return $ReturnRX;
                            }

                            # only show corresponding module's sub-modules in menu

                            $KIS_MODULE_MENUS = "";

                            $urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];

                            # Account management
                            if (($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])  && strstr($urlSrc, "home/eAdmin/AccountMgmt"))
                            {
                                $KIS_MENUS[] = array($Lang['Header']['Menu']['StaffAccount'], "/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1", $CurrentPageArr['StaffMgmt']);

                                $KIS_MENUS[] = array($Lang['Header']['Menu']['StudentAccount'], "/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1", $CurrentPageArr['StudentMgmt']);

                                if(!$sys_custom['project']['NCS']){
                                    $KIS_MENUS[] = array($Lang['Header']['Menu']['ParentAccount'], "/home/eAdmin/AccountMgmt/ParentMgmt/?clearCoo=1", $CurrentPageArr['ParentMgmt']);
                                }

                                if((isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']=='A')){
                                    $KIS_MENUS[] = array($Lang['Header']['Menu']['AuditLog'], "/home/eAdmin/AccountMgmt/AuditLog/?clearCoo=1", $CurrentPageArr['AuditLog']);
                                }
                            }

                            if ($plugin['AccountMgmt_StudentRegistry'] && $sys_custom['StudentRegistry']['Kentville'] && $_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && strstr($urlSrc, "home/eAdmin/AccountMgmt")) {
                                $KIS_MENUS[] = array($Lang['Header']['Menu']['StudentRegistry'], "/home/eAdmin/AccountMgmt/StudentRegistry/", $CurrentPageArr['StudentRegistry']);
                            }


                            # School Settings
                            if (($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"] ||
                                $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) && strstr($urlSrc, "home/system_settings"))
                            {
                                if(!$sys_custom['KIS_SchoolSettings']['DisableCampus'] && !$stand_alone['eAdmission'] && !(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=''))
                                {
                                    if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Campus"]) /* && $plugin['Inventory'] */)
                                    {
                                        $KIS_MENUS[] = array($Lang['Header']['Menu']['Site'], "/home/system_settings/location/", $CurrentPageArr['Location']);
                                    }
                                }

                                if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Class"])
                                {
                                    $KIS_MENUS[] = array($Lang['Header']['Menu']['Class'], "/home/system_settings/form_class_management/", $CurrentPageArr['Class']);
                                }

                                if(!$stand_alone['eAdmission'])
                                {
                                    if(!$sys_custom['KIS_SchoolSettings']['DisableGroup'] && !(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!='')){
                                        if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Group"])
                                        {
                                            $KIS_MENUS[] = array($Lang['Header']['Menu']['Group'], "/home/system_settings/group/?clearCoo=1", $CurrentPageArr['Group']);
                                        }
                                    }
                                    if($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])
                                    {
                                        //$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(42, $PATH_WRT_ROOT_ABS."home/system_settings/role_management/", $Lang['Header']['Menu']['Role'], $CurrentPageArr['Role'],"","Role");
                                        $KIS_MENUS[] = array($Lang['Header']['Menu']['Role'], "/home/system_settings/role_management/", $CurrentPageArr['Role']);
                                    }
                                }

                                if(!$sys_custom['KIS_SchoolSettings']['DisableSchoolCalendar'] && ($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-SchoolCalendar"]))
                                {
                                    //$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(46, $PATH_WRT_ROOT_ABS."home/system_settings/school_calendar/", $Lang['Header']['Menu']['SchoolCalendar'], $CurrentPageArr['SchoolCalendar'],"","SchoolCalendar");
                                    if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=''){
                                        $KIS_MENUS[] = array($Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'], "/home/system_settings/school_calendar/academic_year.php", $CurrentPageArr['SchoolCalendar']);
                                    }else{
                                        $KIS_MENUS[] = array($Lang['Header']['Menu']['SchoolCalendar'], "/home/system_settings/school_calendar/", $CurrentPageArr['SchoolCalendar']);
                                    }
                                }

                                if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Subject"]) && $sys_custom['KIS_SchoolSettings']['EnableSubjectSetting'] && !(isset($_SESSION["ncs_role"]) && $_SESSION["ncs_role"]!=""))
                                {
                                    $KIS_MENUS[] = array($Lang['Header']['Menu']['Subject'], "/home/system_settings/subject_class_mapping/", $CurrentPageArr['Subjects']);
                                }

                                if(!$sys_custom['KIS_SchoolSettings']['DisableTimetable'] && !(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!='')){
                                    if(($_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || $_SESSION["SSV_USER_ACCESS"]["SchoolSettings-Timetable"]) && $plugin['eBooking'])
                                    {
                                        //$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(47, $PATH_WRT_ROOT_ABS."home/system_settings/timetable/", $Lang['Header']['Menu']['Timetable'], $CurrentPageArr['Timetable'],"","Timetable");
                                        $KIS_MENUS[] = array($Lang['Header']['Menu']['Timetable'], "/home/system_settings/timetable/", $CurrentPageArr['Timetable']);
                                    }
                                }
                                if(false && $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"]){
                                	//$FullMenuArr[$SchoolSettingsMenu_i][1][] = array(48, $PATH_WRT_ROOT_ABS."home/system_settings/security/password_policy.php", $Lang['SysMgr']['Security']['Title'], $CurrentPageArr['SystemSecurity'],"","SystemSecurity");
                                	$KIS_MENUS[] = array($Lang['SysMgr']['Security']['Title'], "/home/system_settings/security/password_policy.php", $CurrentPageArr['SystemSecurity']);
                                }
                            }

                            # eInventory
                            if (($plugin['Inventory'] && ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"] || $_SESSION["inventory"]["role"] != "")) && strstr($urlSrc, "home/eAdmin/ResourcesMgmt/eInventory"))
                            {
                                # it is quite strange to show only one menu button...
                                //$KIS_MENUS[] = array($Lang['Header']['Menu']['eInventory'], "/home/eAdmin/ResourcesMgmt/eInventory", $CurrentPageArr['eInventory']);
                            }

                            if (sizeof($KIS_MENUS)>0)
                            {
                                $KIS_MODULE_MENUS = GenerateKISModuleMenu($KIS_MENUS);
                                # show the menu
                                $hide_KIS_menu = "";
                                $KIS_TopPx = 8;
                            } else
                            {
                                $KIS_TopPx = 4;
                            }
                        }


                        if ($sys_custom['project']['CourseTraining']['IsEnable']) {
                            $hide_menu_now = "style=\"display:none\"";

                            # only show corresponding module's sub-modules in menu

                            $StandardLMS_MODULE_MENUS = "";

                            $urlSrc = (getenv("SCRIPT_NAME")!="") ? getenv("SCRIPT_NAME") : $_SERVER["SCRIPT_NAME"];

                            # Account management
                            if (($_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])  && strstr($urlSrc, "home/eAdmin/AccountMgmt"))
                            {
                                $StandardLMS_MENUS[] = array($Lang['Header']['Menu']['StaffAccount'], "/home/eAdmin/AccountMgmt/StaffMgmt/?clearCoo=1", $CurrentPageArr['StaffMgmt']);

                                $StandardLMS_MENUS[] = array($Lang['Header']['Menu']['StudentAccount'], "/home/eAdmin/AccountMgmt/StudentMgmt/?clearCoo=1", $CurrentPageArr['StudentMgmt']);
                            }

                            if (sizeof($StandardLMS_MENUS)>0)
                            {
                                $StandardLMS_MODULE_MENUS = $linterface->getCustomizedModuleMenu($StandardLMS_MENUS);
                                # show the menu
                                $hide_StandardLMS_menu = "";
                                $KIS_TopPx = 8;
                            } else
                            {
                                $KIS_TopPx = 4;		// for scroll left menu to top
                            }
                        }


                        if ($sys_custom['DHL'] || $sys_custom['LivingHomeopathy'] || $sys_custom['PowerClass']) {
                            include_once($PATH_WRT_ROOT . "templates/" . $sys_custom['Project_Label'] . "/home_header.php");
                        }

                        if ($sys_custom['HideTopBarMenu']) {
                            $hide_menu_now = "style=\"display:none\"";
                        }
                        ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  />
<META Http-Equiv="Cache-Control" Content="no-cache">

<?php if (stristr($_SERVER['REQUEST_URI'],'/home/eAdmin/ResourcesMgmt/DigitalArchive/admin_doc/') !== false){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<?php } elseif ($isIE11 && (stristr($_SERVER['REQUEST_URI'],'/home/imail_gamma/compose_email.php') !== false || stristr($_SERVER['REQUEST_URI'],'/home/imail_gamma/pref_signature.php') !== false)){ ?>
	<?php if($sys_custom['iMailPlus']['CKEditor']){ ?>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
	<?php }else{ ?>
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
	<?php } ?>
<?php } elseif ($isIE11 && (stristr($_SERVER['REQUEST_URI'],'/home/imail/compose.php') !== false || stristr($_SERVER['REQUEST_URI'],'/home/imail/pref_signature.php') !== false)){ ?>
	<?php if($sys_custom['iMail']['CKEditor']){ ?>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <?php } else { ?>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <?php } ?>
<?php } elseif ((stristr($_SERVER['REQUEST_URI'],'reportcard_kindergarten/index.php?task=mgmt.subject_topic_score.score') !== false) ||
                    (stristr($_SERVER['REQUEST_URI'],'reportcard_kindergarten/index.php?task=mgmt.language_behavior_score.score') !== false)) { ?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<?php } elseif (!$home_header_no_EmulateIE7){ ?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<?php } elseif ($home_header_with_EmulateIE9) {?>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<?php } ?>
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css" rel="stylesheet" type="text/css">
<?if($sys_custom['OnlineRegistry']) {?>
	<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/parent_reg.css" rel="stylesheet" type="text/css">
<? } ?>
<?php if ($showCustomProjectHeader) {
	if (count($dhl_css) > 0) {
		foreach ($dhl_css as $kk => $vv) {
			echo '<link href="' . $vv . '" rel="stylesheet" type="text/css">' . "\n";
		}
	}
} ?>
<?php if ($showViewportWithDeviceWidth) { ?>
<meta name="viewport" content="width=device-width">
<?php } ?>
<?php if ($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'],'templates/LivingHomeopathy/portal.php') !== false):?>
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
<?php endif;?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/brwsniff.js"></script>
<?php
// code to include newer jquery library if is going to staff attendance 3 pages for case: 2010-1216-0911-56073
if ((stristr($_SERVER['REQUEST_URI'],'StaffMgmt/attendance') !== false) || (stristr($_SERVER['REQUEST_URI'],'library_sys/reports/frequency_report') !== false)) { ?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.4.4.min.js"></script>
<? } else if (stristr($_SERVER['REQUEST_URI'],'reportcard_kindergarten/index.php?task=settings.teaching_tool.list') !== false) { ?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.12.4.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-migrate-1.4.1.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery.lazy/jquery.lazy.min.js"></script>
<? } else { ?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>
<?}?>
<?php if ($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'],'templates/LivingHomeopathy/portal.php') !== false):?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-3.3.1.min.js"></script>
<?php endif;?>
<?php if ($showCustomProjectHeader) {
	if (count($dhl_js) > 0) {
		foreach ($dhl_js as $kk => $vv) {
			echo '<script language="JavaScript" src="' . $vv . '"></script>' . "\n";
		}
	}
} ?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js?t=<?=date('Ymd')?>"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>lang/script.<?= $intranet_session_language?>.js"></script>
<?php if (!($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'],'templates/LivingHomeopathy/portal.php') !== false)):?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.blockUI.js"></script>
<?php endif;?>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/js/SpryValidationTextField.js"></script>

<?php if (isset($sys_custom['layout']['top_banner']) && $sys_custom['layout']['top_banner']!="") { ?>
<style type="text/css">
.top_banner {
  background-image: url(/images/customization/biba_bg.gif);
  height: 75px;
}
</style>
<?php } ?>

<? /* ?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.gears.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.silverlight.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.flash.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.browserplus.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.html4.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/plupload.html5.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/jquery.plupload.queue/jquery.plupload.queue.php"></script>
<link rel=stylesheet href="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/plupload/jquery.plupload.queue/css/jquery.plupload.queue.css">
<? */ ?>

<?php
if ($ControlTimeOutByJS)
{
?>
<script language="JavaScript">
var LogoutCountValue = <?=$TimeOutWarningSecond?>;
var SecondLogout = LogoutCountValue;
var ToLogout=false;
var SecondLeftOriginal=<?=$SessionLifeTime?>;
var SecondLeft=<?=$SessionLifeTime?>;

function CountingDown(){
	if (SecondLeft>0)
	{
	   SecondLeft -= 1;
	   setTimeout("CountingDown()",1000);
	} else
	{
		// query server to see if there is no any response from user (e.g. using AJAX)
		path = "/get_last_access.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft);

	}
}
<?php if (!$hideCountDown) { ?>
CountingDown();
<?php } ?>

var callback_timeleft =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);

		if (TimeLeft>0)
		{
			SecondLeft = TimeLeft;
			CountingDown();
		} else
		{
			//alert("testing timeout method!")
			// notify user about the timeout
			// user can press "Continue" to cancel the timeout; if no action taken, system will (call logout page and) direct to timeout result page
			MM_showHideLayers('sub_layer_sys_timeout','','show');
			bubble_block = document.getElementById("sub_layer_sys_timeout");
			bubble_block.style.left = "0px";
			bubble_block.style.top = '0px';
			bubble_block.focus();

			ToLogout = true;
			CountDownToLogout();
		}
	}
}

function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}


function CountDownToLogout()
{
	if (ToLogout)
	{
		if (SecondLogout>0)
		{
			SecondLogout -= 1;

			var browserWidth = f_clientWidth();
			var browserHeight = f_clientHeight();

			/*
			jsTimeOutTxt = '<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">';
			jsTimeOutTxt += '		<tr>';
			jsTimeOutTxt += '		<td bgcolor="orange" align="center">';
			jsTimeOutTxt += '		<table width="'+(browserWidth-60)+'" height="'+(browserHeight-60)+'" border="0" cellpadding="12" cellspacing="0">';
			jsTimeOutTxt += '			<tr>';
			jsTimeOutTxt += '			<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>';
			jsTimeOutTxt += '			<img src="/images/2020a/alert_signal.gif" /><br /><font size="5"><b><font color="red">System is about to time out in '+SecondLogout+' seconds.</font><br />You can press <input type="button" style="font-size:23px" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.</b></font>';
			jsTimeOutTxt += '			</td></tr>';
			jsTimeOutTxt += '		</table>';
			jsTimeOutTxt += '		</td></tr>';
			jsTimeOutTxt += '	</table>';
			*/

			jsTimeOutTxt2 = '<table class="timeoutAlert"  width="'+(browserWidth)+'" height="'+(browserHeight)+'"><tr><td align="center">';
			jsTimeOutTxt2 += '<div class="contentContainer">';
			jsTimeOutTxt2 += '<div class="textTitle"><span class="timeLeft">&nbsp;<?=$Lang['SessionTimeout']['ReadyToTimeout_Header']?></span></div>';
			jsTimeOutTxt2 += '<div class="textContent"><div class="line"><span><?=$Lang['SessionTimeout']['ReadyToTimeout_str1']?> <U> '+SecondLogout+' </U><?=$Lang['SessionTimeout']['ReadyToTimeout_str2']?><br><?=$Lang['SessionTimeout']['ReadyToTimeout_str3']?> </span> <input type="button"  value="<?=$Lang['SessionTimeout']['Continue']?>" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()"> ';
			jsTimeOutTxt2 += '<span><?=$Lang['SessionTimeout']['ReadyToTimeout_str4']?></span></div> ';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</td></tr></table>';
			jsTimeOutTxt2 += '	</table>';

			document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt2;

			setTimeout("CountDownToLogout()",1000);

			// update message
		} else
		{
			// redirect to logout page
			self.location = "/logout.php?IsTimeout=1";
		}
	} else
	{
		// reset logout timer
		SecondLogout = LogoutCountValue;
	}
}



var callback_timeleft_trival =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);
	}
}



function CancelTimeOut()
{
	// stop redirect function
	SecondLogout = LogoutCountValue;
	ToLogout = false;
	alert("<?=$Lang['SessionTimeout']['CancelTimeout']?>");

	// restart counting down for time out
	SecondLeft = SecondLeftOriginal;
	CountingDown();

	var path = "/update_last_access.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft_trival);
}



</script>
<?php
}
?>

<script language="JavaScript">
<!--
function support()
{
	newWindow("<?=$cs_path?>",10);
}

function toScrabble()
{
	document.scrabble_form.method = "post";
	document.scrabble_form.action = "http://sfc.broadlearner.com/main.php?schoolName=<?=$BroadlearningClientName?>&schoolType=<?=$config_school_type?>";
	document.scrabble_form.target = "scrabbleNewWin";

	window.open("", "scrabbleNewWin", "menubar=0,toolbar=0,resizable,scrollbars,status,top=40,left=40,width=800,height=500");
	var a = window.setTimeout("document.scrabble_form.submit();", 500);
}


function products()
{
	<?php if (!file_exists("$PATH_WRT_ROOT/home/support_version.php")) { ?>
	newWindow("<?=$products_path?>",10);
	<?php } else { ?>
	newWindow("/home/support_version.php",10);
	<?php } ?>
}

function onlinehelpcenter()
{
	newWindow("<?=$onlinehelpcenter_path?>",24);
}

function doccenter()
{
	newWindow("<?=$doccenter_path?>",24);
}


function faq()
{
	newWindow("<?=$cs_faq?>",24);
}

function videocenter()
{
	newWindow("<?=$videocenter_path?>",10);
}
//-->
</script>

<script language="Javascript">
<!--
// slide tool
var slide_ie4 = (document.all) ? true : false;
var slide_ns4 = (document.layers) ? true : false;
var slide_ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var sliding = {MyTools:false}
var hide = "hidden";
var show = "visible";

function open_lslp()
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
	newWindow('/home/asp/lslp.php',32);
}
function open_kskgs(subject)
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
 	if(typeof(subject)!='undefined')
 		var url = '/home/asp/ksk.php?subject='+subject;
 	else
 		var url = '/home/asp/ksk.php';
 	newWindow(url,32);
}


function logout()
{
	if(confirm(globalAlertMsg16))
	{

		<?/* if($plugin['lslp']): */?>

//                 logoutscript = document.createElement('script');
//                 logoutscript.type = 'text/javascript';
//                logoutscript.src = '<?= $lslp_httppath ?>/logout.php?action=redirect';
//                 document.getElementsByTagName('body')[0].appendChild(logoutscript);

//                setTimeout(function(){window.location="/logout.php";}, 3000); <? //force logout if LSLP cannot load in 3 seconds?>

                <?/* else: */?>
                window.location="/logout.php";
                <?/* endif; */?>

	}
}
function slide(lay, h, speed, up) {
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                it=4;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; it=6;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left; it=10;}
        ih = parseInt(heig);

        if (up == 0){
                if (ih != h)
                        movelayer(lay, it);
                if (ih < h) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
                } else
                        setSliding(lay, 0);
        } else {
                if (ih != 25)
                        movelayer(lay, -it);
                if (ih > 25) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
                } else
                        setSliding(lay, 0);
        }
}


function setSliding(lay, on_off) {
        is_sliding = "sliding." + lay;
        is_sliding += (on_off==0) ? "=false" : "=true";
        eval (is_sliding);
}


function getSliding(lay) {
        is_sliding = "sliding." + lay;
        return eval(is_sliding);
}


function move(lay,h){
        if (getSliding(lay))
                return;
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                speed=1;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; speed=1;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left;speed=1;}
        ih = parseInt(heig);
        if (ih<=25)
                slide(lay,h,speed,0);
        else if (ih>=h)
                slide(lay,h,speed,1);
}
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function loadContent(evt,contentType){
	if (contentType == 'campus_link'){
		bubble_block = document.getElementById("sub_layer_campus_link");
		//link = document.getElementById('campusLinkico');
		//alert(document.defaultView.getComputedStyle(link,null).getPropertyValue('left'));

		bubble_block.style.left = (evt.clientX-230)+"px";

		bubble_block.style.top = '65px';
		document.getElementById("ajaxMsgBlock").innerHTML = "<?=$ip20_loading?> ...";
		document.getElementById("ajaxMsgBlock").style.display = "block";
		serverURL = "/home/campus_link/campus_link_load_content.php";
		$('div#sub_layer_campus_link div#bubble_board_content').load(serverURL,{loadType: "whole"},			function(){
			document.getElementById("ajaxMsgBlock").style.display = "none";
		});
	} else if (contentType == 'sys_about') {
		bubble_block = document.getElementById("sub_layer_sys_about");

						//var windowWidth = $(window).width();
						if( typeof( window.innerWidth ) == 'number' ) {
							//Non-IE
							myWidth = window.innerWidth;
						} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
							//IE 6+ in \'standards compliant mode\'
							myWidth = document.documentElement.clientWidth;
						} else if( document.body && ( document.body.clientWidth) ) {
							//IE 4 compatible
							myWidth = document.body.clientWidth;
						}

						//alert(myWidth);

		//bubble_block.style.left = (evt.clientX-200)+"px";
		bubble_block.style.left = (myWidth-265)+"px";

		bubble_block.style.top = '15px';
// 		document.getElementById("ajaxMsgBlock2").style.display = "block";

		//MM_showHideLayers('sub_layer_sys_about','','show');
		document.getElementById('sub_layer_sys_about').style.visibility = 'visible';


	} else if(contentType == "power_tool"){

		bubble_block = document.getElementById("sub_layer_power_tool");
		bubble_block.style.left = (evt.clientX-160)+"px";
		bubble_block.style.right = "122px";
		bubble_block.style.top = '65px';
	}
}

function emptyContent(layername)
{
	document.getElementById(layername).innerHTML = "";
}

$(window).resize(function() {
	if(document.getElementById('sub_layer_sys_about').style.visibility=="visible")
		$('#about_eclass').click();

});

function openBookFlix()
{
	<?php if (date("Y-m-d")>$sys_integration['bookflix_school_expiry'])	{ ?>
		alert("<?=str_replace('<expiry_date>', $sys_integration['bookflix_school_expiry'], $Lang['bookflix_expired'])?>");
	<?php } else { ?>
		newWindow("/home/bookflix.php",8);
	<?php }  ?>
}


<?php
if ($plugin['CENTRAL_Power_Lesson'] && ($_SESSION['UserType']==USERTYPE_STAFF || $_SESSION['UserType']==USERTYPE_STUDENT))
	{
?>

function Go2CentralPL()
{
	newWindow('/api/single_sign_on_ip25.php',8);
	return;
}
<?php
	}
?>
//-->
</script>
<?php
if (!empty($customk_favicon)) {
?>
<link id="page_favicon" href="<?php echo $customk_favicon; ?>" rel="shortcut icon" type="image/gif" />
<?php } else { ?>
<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<?php } ?>
<title><?=$Lang['Header']['HomeTitle']." ".$KISSchoolName?></title>
<!--[if lte IE 7]>
<style type="text/css">
html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
</style>
<![endif]-->
</head>
<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<?=$bubbleMsgBlock?>

<?=$bubbleMsgBlockSysAbout?>

<?=$bubbleMsgBlockSysTimeout?>

<?php
if ($plugin['ScrabbleFC'])
{
	# for scrabble fun corner
	if($_SESSION['UserType'] == 1)
		$scrabble_user_type = 'Teacher';
	else if($_SESSION['UserType'] == 2)
		$scrabble_user_type = 'Student';
?>
<form name="scrabble_form" method="post">
	<input type="hidden" name="user_type" value="<?=$scrabble_user_type?>" />
</form>
<?php
}
?>

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
<?php if (!isset($hide_KIS_menu) || empty($hide_KIS_menu)) { ?>
<tr <?=$hide_KIS_menu?>>
	<td>
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
	  <tr>
			<td class="top_banner" style="height:28px; background-position:0px -39px;" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="150">&nbsp;</td>
					<td><div id="main_menu" class="mainmenu">
								<ul><?=$KIS_MODULE_MENUS?></ul>
							</div></td>
				</tr>
			</table></td>
	  	</tr>
	</table>
	</td>
</tr>
<?php } ?>
<?php if (!isset($hide_StandardLMS_menu) || empty($hide_StandardLMS_menu)) { ?>
<tr <?=$hide_StandardLMS_menu?>>

	<td>
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
	  <tr>
			<td class="top_banner" style="height:28px; background-position:0px -39px;" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="150">&nbsp;</td>
					<td><div id="main_menu" class="mainmenu">
								<ul><?=$StandardLMS_MODULE_MENUS?></ul>
							</div></td>
				</tr>
			</table></td>
	  	</tr>
	</table>
	</td>
</tr>
<?php } ?>
<?php if (!isset($hide_menu_now) || empty($hide_menu_now)) { ?>
<tr <?=$hide_menu_now?> >
	<Td height="75">
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
	  <tr>
			<td class="top_banner" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="120" rowspan="2" valign="middle" align="center">
						<a href="<?=$school_logo_link?>" class="school_logo"><img src="<?= $logo_img?>" border="0" <?=$logo_fixed_height?>></a>
					</td>
					<td height="38" valign="top" nowrap>
						<div class="school_title"> <?= $school_name ?> </div>
						<div class="user_login">
							<img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/topbar_25/<?=$UserIdentityIcon?>" align="absmiddle"> <?=$UserIdentity?>
							<span>|</span>

							<? if($_SESSION['ck_from_pl']){ ?>
							<a class="btn_to_pl" href="/home/pl"><?=$Lang['PowerLessonAppWeb']['BackToPowerLesson']?></a><span>|</span>
							<? } ?>

							<? if($showNewFeature) {?>
							<span id="newFeatureSpan"><a title="<?=$Lang['General']['NewFeatures']?>" class="new_features"><?=$Lang['General']['NewFeatures']?></a><span>|</span></span>
							<? } ?>

							<!--<a href="javascript:support()" title="<?=$Lang['Header']['eClassCommunity']?>">?</a>-->
<?php if (get_client_region()!="zh_CN" && !$special_feature['hide_support'] && $_SESSION['UserType']==USERTYPE_STAFF) {?>
							<a href="javascript:void(0)" id="about_eclass" onclick="loadContent(event,'sys_about')" >?</a>
<?php } ?>
							<? if(!$sys_custom['hide_lang_selectioin'][$_SESSION['UserType']] || ($sys_custom['hide_lang_selectioin_except_school_admin'] && $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {?>
								<a href="/lang.php?lang=<?= $lang_to_change?>" title="<?=$Lang['Header']['ChangeLang']?>"><?=$change_lang_to?></a>
							<? } ?>
							<a href="javascript:logout()" title="Logout">X</a>
						</div>
					</td>
				</tr>
					<tr>
						<td valign="top" nowrap>
<?php
$online_reg_for_parent = false;
if($sys_custom['OnlineRegistry'])
{
	include_once($PATH_WRT_ROOT."includes/libstudentregistry.php");
	$lsr = new libstudentregistry();
	$online_reg_for_parent = $lsr->checkParentFillOnlineReg();
}

//if (!$_SESSION['SSV_eBooking_standalone'] && !$online_reg_for_parent)
if ($_SESSION["platform"]!="KIS" && !$_SESSION['SSV_eBooking_standalone'] && (!$online_reg_for_parent || ($online_reg_for_parent && $_SESSION['ParentFillOnlineRegMode']=="current")))
{
?>
							<div id="main_menu" class="mainmenu">
								<?= $linterface->GEN_TOP_MENU_IP25($FullMenuArr);?>
							</div>
							<div class="itool_menu">
							<ul>
							<?if($menuWithIAccount !== false && $_SESSION["platform"]!="KIS" && !$special_feature['hide_iAccount']){?>
								<?php
							    $iAccountURL = '/home/iaccount/account/';
								if($sys_custom['iAccount']['editPasswordOnly']){
								    $iAccountURL = '/home/iaccount/account/login_password.php';
								}
								?>
									<li><a href="<?=$iAccountURL ?>" class="itool_iaccount" title="<?=$Lang['Header']['Menu']['iAccount']?>">&nbsp;</a></li>
							<?}?>
							<?if($_SESSION["SSV_PRIVILEGE"]["plugin"]["TeacherPortfolio"] && $_SESSION["UserType"]=="1" && $_SESSION["isTeaching"]=="1" ){?>
									<li><a href="#" onClick="javascript:newWindow('/home/eAdmin/StaffMgmt/teacher_portfolio/teacher.php',8); return false;" class="itool_teacher_portfolio" title="<?=$Lang['Header']['Menu']['TeacherPortfolio']?>">&nbsp;</a></li>
							<?}?>
							<?if(!$plugin['DisableiCalendar'] && $menuWithICalendar !== false){?>
									<li><a href="javascript:newWindow('/home/iCalendar/',31)" class="itool_icalendar" title="<?=$icalendar_name?>">&nbsp;</a></li>
							<?}?>
							<? if($access2iFile && $_SESSION['UserType']!=USERTYPE_ALUMNI) { ?>
									<li><a href="#" class="itool_ifolder" title="<?=$Lang['Header']['Menu']['iFolder']?>" onClick="newWindow('/home/iFolder/',10); return false;">&nbsp;</a></li>
							<? } ?>
							<?
							global $SYS_CONFIG;
							if($menuWithIMail !== false && $_SESSION["platform"]!="KIS" && !$special_feature['hide_iMail']){?>

									<?if($special_feature['imail']===true && $plugin['imail_gamma']!==true){?>
										<?
											$access2campusmail = $_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"];
											if($access2campusmail) {
										?>
											<li><a href="/home/imail/" class="<?=$iMailClass?>" id="iMailIcon" title="<?=$Lang['Header']['Menu']['iMail']?>"></a></li>
										<?
											}
										?>
									<?}?>
									<? if($plugin['imail_gamma'] && !empty($_SESSION['SSV_EMAIL_LOGIN']) && !empty($_SESSION['SSV_LOGIN_EMAIL'])) {
										$gamma_access_right = Get_Gamma_Identity_Access_Rights();
										if(($lu->isTeacherStaff() && $gamma_access_right[0]==1) || ($lu->isStudent() && $gamma_access_right[1]==1) || ($lu->isParent() && $gamma_access_right[2]==1) || ($lu->isAlumni() && $gamma_access_right[3]==1)){
									?>
									<li><a href="/home/imail_gamma/index.php" class="itool_imail" title="<?=$Lang['Header']['Menu']['iMailGamma']?>"></a></li>
									<?}
									}?>
							<? } ?>
							<?php if ($_SESSION['UserType']==USERTYPE_ALUMNI) { ?>
									<li><a href="/home/eService/eSurvey/" class="<?=$eSurveyAlumniClass?>" id="eSurveyAlumniIcon" title="<?=$Lang['Header']['Menu']['eSurvey']?>"></a></li>
							<?php } ?>
							<?php
								  $showJupasReadyIcon = false;
								  if (get_client_region()=='zh_HK' && $_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"] && $_SESSION['UserType']==USERTYPE_STAFF && date('Y-m-d') < '2012-03-01' && $sys_custom['iPf']['JUPASArr']['HideJupasReadyIcon']==false) {
								  		$showJupasReadyIcon = true;
								  }
							?>
								    <li>
										<?php if($_SESSION["SSV_PRIVILEGE"]["eclass"]["Access2Portfolio"] && $_SESSION['UserType']!=USERTYPE_ALUMNI) { ?>
											<a id="iPortfolioLink" href="/home/portfolio/" class="itool_iportfolio" title="<?=$Lang['Header']['Menu']['iPortfolio']?>">&nbsp;</a>
										<?php } ?>
										<?php if ($showJupasReadyIcon) { ?>
											<img id="jupasReadyIcon" src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/topbar_25/icon_jupasready.png" align="absmiddle" style="position:absolute;display:none;">
										<?php } ?>
									</li>

							<? if(($_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancestudent'] || $_SESSION["SSV_PRIVILEGE"]["plugin"]['payment'] || isset($module_version['StaffAttendance']) || $_SESSION["SSV_PRIVILEGE"]["plugin"]['attendancelesson'])  && $_SESSION['UserType']!=USERTYPE_ALUMNI && !(isset($sys_custom['HideiSmartCardForUserTypes']) && in_array($_SESSION['UserType'],$sys_custom['HideiSmartCardForUserTypes']))) { ?>
									<li><a href="/home/smartcard/" class="itool_ismartcard" title="<?=$Lang['Header']['Menu']['ismartcard']?>">&nbsp;</a></li>
							<? } ?>

							<?
							/*if(in_array(get_client_region(), array('zh_HK', 'zh_MO')) && $_SESSION['UserType']==USERTYPE_STAFF && time()>= strtotime("2012-04-18")){
							?>
									<li><a href="/home/eClassStore/redirect.php" target="_blank" class="itool_reprintcard" title="����������踝蕭����������踝蕭��質�����鳴蕭����������踝蕭&nbsp;</a></li>
							<?
							}*/
							?>

							<? if($_SESSION['UserType']!=USERTYPE_ALUMNI) {?>
							<li><span>|</span></li>
							<? } ?>

							<?if( !($plugin['DisableeCommunityForParent'] && $_SESSION['UserType']==USERTYPE_PARENT) && !$plugin['DisableeCommunity'] && $menuWithECommnuity !== false && $_SESSION['UserType']!=USERTYPE_ALUMNI){?>
									<li><a href="/home/eCommunity/redirect.php" class="itool_ecommunity" title="<?=$Lang['Header']['Menu']['eCommunity']?>">&nbsp;</a></li>
									<li><span>|</span></li>
							<? } ?>
							<?php
							## 2010-08-25 Sandy , Power Speech
							if($_SESSION['ss_intranet_plugin']['power_speech'] && $_SESSION['UserType']!=USERTYPE_ALUMNI && check_powerspeech_is_expired()!=2) { ?>
									<li><a href="javascript:void(0)" onclick="MM_showHideLayers('sub_layer_power_tool','','show');loadContent(event,'power_tool');" class="itool_powertool" title="<?=$Lang['Header']['Menu']['PowerTool']?>">&nbsp;</a></li>
									<li><span>|</span></li>
							<?php } ?>

							<? if($_SESSION['UserType']!=USERTYPE_ALUMNI && $_SESSION["platform"]!="KIS" && !$plugin['DisableCampusLink']) {?>
								<li><a href="javascript:void(0)" onclick="MM_showHideLayers('sub_layer_campus_link','','show');loadContent(event,'campus_link')" class="itool_campus_link" title="<?=$Lang['Header']['Menu']['CampusLinks']?>">&nbsp;</a></li>
							<? } ?>

							</ul>
							</div>
<?php
}
?>
						</td>
					</tr>
			</table></td>
	  	</tr>
	</table>
	</td>
</tr>
<?php } ?>
<?php
if ($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'], 'templates/LivingHomeopathy/portal.php') !== false) {
    echo '</table>';
    echo '<div id="container">';
}

// for DHL, LivingHomeopathy
if ($showCustomProjectHeader) {
	echo $topMenuCustomization;
} ?>

<?php if (!($sys_custom['LivingHomeopathy'] && stristr($_SERVER['REQUEST_URI'], 'templates/LivingHomeopathy/portal.php') !== false)):?>
<tr>
<td valign="top">
<?php endif;?>

<?php
		/* We've got fresh content stored in $data! */
		$cached_hh_data = ob_get_contents();
		if ($IsHeaderCaching)
		{
			$Cache_Lite->save($cached_hh_data, $id);

		}
		ob_get_clean();
	}

	echo $cached_hh_data;
}

################################################################################################# IMPORTANT
# lighten current menu (for cached method)
# [based on $CurrentPageArr], you have to add only if you have added new sub-menu!!!

if ($IsHeaderCaching)
{
	global $linterface, $special_feature, $module_version,$stand_alone;
	if (is_array($CurrentPageArr) && sizeof(($CurrentPageArr)>0))
	{
		foreach($CurrentPageArr as $PageMenuKey => $MenuEnabled)
		{
			if ($MenuEnabled && $PageMenuKey!="eService" && $PageMenuKey!="DRC" && $PageMenuKey!="eLearning" && $PageMenuKey!="eAdmin" && $PageMenuKey!="SchoolSettings")
			{
				$jsCommands = "";

				# lighten sService
				if ($PageMenuKey=="eServiceCampusTV" || $PageMenuKey=="eServiceeBooking" || $PageMenuKey=="eServiceCircular" || $PageMenuKey=="eServiceeDisciplinev12"
					 || $PageMenuKey=="eServiceeEnrolment"  || $PageMenuKey=="eServiceHomework"  || $PageMenuKey=="eServiceNotice"
                     || $PageMenuKey=="eServiceeReportCard" || $PageMenuKey=="eServiceeReportCardKindergarten" || $PageMenuKey=="eServiceSportDay"  || $PageMenuKey=="eServiceSwimmingGala"
					  || $PageMenuKey=="eServiceeSurvey"  || $PageMenuKey=="PagePolling"  || $PageMenuKey=="eServiceRepairSystem"
					  || $PageMenuKey=="ResourcesBooking"  || $PageMenuKey=="SLSLibrary" || $PageMenuKey=="eServiceTimetable" || $PageMenuKey=="eServiceDigitalChannels"||$PageMenuKey=="eServiceSchoolBus"||$PageMenuKey=="eServiceeAppraisal")
				{
					$jsCommands .= "changeClass(\"eService\", \"common_current\");\n";
				}


				# lighten eLibrary
				if ($PageMenuKey=="eLib" || $PageMenuKey=="eLibPlus" || $PageMenuKey=="eLearningReadingGarden"  || $PageMenuKey=="DigitalArchive_SubjectReference")
				{
					$jsCommands .= "changeClass(\"DRC\", \"current_module\");\n";
					$flagOn['DRC'] = true;
				}

				# lighten eLearning
				if ($flagOn['DRC'] || $PageMenuKey=="eClass" || $PageMenuKey=="eLC" || $PageMenuKey=="IES" || $PageMenuKey=="iTextbook" ||
					($PageMenuKey=="DigitalArchive_SubjectReference" && $_SESSION['UserType']==USERTYPE_STUDENT) || $PageMenuKey=="SBA" ||
					$PageMenuKey == "WWSeLearningProject" )
				{
					$jsCommands .= "changeClass(\"eLearning\", \"common_current\");\n";
				}


				# lighten Account Management
				if ($PageMenuKey=="ParentMgmt" || $PageMenuKey=="StaffMgmt" || $PageMenuKey=="StudentMgmt"
					 || $PageMenuKey=="StudentRegistry"  || $PageMenuKey=="SharedMailBox" || $PageMenuKey=="AlumniMgmt")
				{
					$jsCommands .= "changeClass(\"AccountManagement\", \"current_module\");\n";
					$flagOn['AccountManagement'] = true;
				}

				# lighten General Management
				if ($PageMenuKey=="ePayment" || $PageMenuKey=="ePOS" || $PageMenuKey=="eAdmineSurvey"
					 || $PageMenuKey=="ePolling"  || $PageMenuKey=="schoolNews" || $PageMenuKey=="msschPrinting" || $PageMenuKey=="eAdmineMassMailing" || $PageMenuKey=="eClassApp")
				{
					$jsCommands .= "changeClass(\"GeneralManagement\", \"current_module\");\n";
					$flagOn['GeneralManagement'] = true;
				}

				# lighten Staff Management
				if ($PageMenuKey=="eAdminStaffAttendance" || $PageMenuKey=="eAdminCircular" || $PageMenuKey=="eAdminSLRS" || $PageMenuKey=="eAdmineAppraisal" || $PageMenuKey=="eAdmineForm")
				{
					$jsCommands .= "changeClass(\"StaffManagement\", \"current_module\");\n";
					$flagOn['StaffManagement'] = true;
				}

				# lighten eSports
				if ($PageMenuKey=="SportDay" || $PageMenuKey=="SwimmingGala")
				{
					$jsCommands .= "changeClass(\"eSports\", \"current_module\");\n";
					$flagOn['eSports'] = true;
				}

				# lighten Student Management
				if ($flagOn['eSports'] || $PageMenuKey=="StudentAttendance" || $PageMenuKey=="eDisciplinev12" || $PageMenuKey=="eEnrolment"
					 || $PageMenuKey=="eAdminHomework"  || $PageMenuKey=="eAdminNotice" || $PageMenuKey=="eReportCard"  || $PageMenuKey=="eReportCard_Rubrics" || $PageMenuKey=='eSchoolBus' || $PageMenuKey=="eReportCardKindergarten")
				{
					$jsCommands .= "changeClass(\"StudentManagement\", \"current_module\");\n";
					$flagOn['StudentManagement'] = true;
				}

				# lighten Resources Management
				if ($PageMenuKey=="eBooking" || ($PageMenuKey=="DigitalArchive" && $_SESSION['UserType']==USERTYPE_STAFF) ||  ($PageMenuKey=="DocRouting" && $_SESSION['UserType']==USERTYPE_STAFF) ||  ($PageMenuKey=="DigitalChannels" && $_SESSION['UserType']==USERTYPE_STAFF) || $PageMenuKey=="eInventory"
					 || $PageMenuKey=="eAdminRepairSystem"
					 || $PageMenuKey=="InvoiceMgmtSystem"
					 || $PageMenuKey=="ePCM"
					)
				{
					$jsCommands .= "changeClass(\"ResourcesManagement\", \"current_module\");\n";
					$flagOn['ResourcesManagement'] = true;
				}

				# lighten eAdmin
				if ($flagOn['AccountManagement'] || $flagOn['GeneralManagement'] || $flagOn['StaffManagement']
					 || $flagOn['StudentManagement']  || $flagOn['ResourcesManagement'])
				{
					$jsCommands .= "changeClass(\"eAdmin\", \"common_current\");\n";
				}


				# lighten School Settings
				if ($PageMenuKey=="Location" || $PageMenuKey=="Class" || $PageMenuKey=="Group" || $PageMenuKey=="Role"
					 || $PageMenuKey=="SchoolCalendar"  || $PageMenuKey=="Subjects"  || $PageMenuKey=="Timetable")
				{
					$jsCommands .= "changeClass(\"SchoolSettings\", \"setting_current\");\n";
				}

				if ($PageMenuKey=="Home")
				{
					$ClassNameChangeTo = "home_current";
				} elseif ($PageMenuKey=="SchoolSettings")
				{
					$ClassNameChangeTo = "setting_current";
				} else
				{
					$ClassNameChangeTo = "current_module";
				}

				$jsCommands .= "changeClass(\"".$PageMenuKey."\", \"".$ClassNameChangeTo."\");\n";
			}
		}
	}
	if (!$CurrentPageArr['Home'])
	{
		$jsCommands .= "changeClass(\"Home\", \"home\");\n";
	}


	# update iMail alert icon
	if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] && !isset($iNewCampusMail))
	{
	    # check any new message from DB
	    include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
	    if (!isset($header_lc))
	    {
	    	$header_lc = new libcampusmail();
	    }

		# no need to count for iMail Gamma/Plus
		if (!$plugin['imail_gamma'])
		{
		    if ($special_feature['imail'])
		        $iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
		    else
		    	$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);
		}

		# only check webmail if there is no new mail from DB!
		if ($iNewCampusMail<=0 && !$plugin['imail_gamma'])
		{
			include_once($PATH_WRT_ROOT."includes/libwebmail.php");
			if (!isset($lwebmail))
			{
				$lwebmail = new libwebmail();
			}

		    # Check preference of check email
		    $sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
		    $temp = $header_lc->returnArray($sql,1);
		    $optionSkipCheckEmail = $temp[0][0];
		    if ($optionSkipCheckEmail)
		    {
		        $skipCheck = true;
		    }
		    else
		    {
		        $skipCheck = false;
		    }

			if (!$skipCheck && $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] && $lwebmail->type==3 && !$_SESSION["EmailLoginFailure".$lu->UserLogin])
			{
			    if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
			    {
			        $exmail_count = $lwebmail->checkMailCount();
			        $lwebmail->close();
			        $iNewCampusMail += $exmail_count;
			    } else
			    {
			    	# mark the user's email authenticate
			    	# and don't connect to mail server next time (to avoid slow performance)
			    	$_SESSION["EmailLoginFailure".$lu->UserLogin] = true;
			    }
			}
		}
	}

	if ($_SESSION["SSV_PRIVILEGE"]["campusmail"]["is_access"] || isset($iNewCampusMail))
	{
		$iMailClass = ($iNewCampusMail>0) ? "itool_imail_new" : "itool_imail";
		$jsCommands .= "changeClass(\"iMailIcon\", \"{$iMailClass}\");\n";
	}


	if ($_SESSION['UserType']==USERTYPE_ALUMNI)
	{
		$eSurveyAlumniClass = ($NewSurvey>0) ? "itool_esurvey_new" : "itool_esurvey";
		$jsCommands .= "changeClass(\"eSurveyAlumniIcon\", \"{$eSurveyAlumniClass}\");\n";
	}

}


if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role'] != ""){

	# check any new message from DB
	include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
	if (!isset($header_lc))
	{
		$header_lc = new libcampusmail();
	}

	# no need to count for iMail Gamma/Plus
	if (!$plugin['imail_gamma'])
	{
		if ($special_feature['imail'])
			$iNewCampusMail = $header_lc->returnNumNewMessage_iMail($UserID);
			else
				$iNewCampusMail = $header_lc->returnNumNewMessage($UserID);
	}

	# only check webmail if there is no new mail from DB!
	if ($iNewCampusMail<=0 && !$plugin['imail_gamma'])
	{
		include_once($PATH_WRT_ROOT."includes/libwebmail.php");
		if (!isset($lwebmail))
		{
			$lwebmail = new libwebmail();
		}

		# Check preference of check email
		$sql ="SELECT SkipCheckEmail FROM INTRANET_IMAIL_PREFERENCE WHERE UserID='$UserID'";
		$temp = $header_lc->returnArray($sql,1);
		$optionSkipCheckEmail = $temp[0][0];
		if ($optionSkipCheckEmail)
		{
			$skipCheck = true;
		}
		else
		{
			$skipCheck = false;
		}

		if (!$skipCheck && $_SESSION["SSV_PRIVILEGE"]["campusmail"]["has_webmail"] && $lwebmail->type==3 && !$_SESSION["EmailLoginFailure".$lu->UserLogin])
		{
			if ($lwebmail->openInbox($lu->UserLogin, $lu->UserPassword))
			{
				$exmail_count = $lwebmail->checkMailCount();
				$lwebmail->close();
				$iNewCampusMail += $exmail_count;
			} else
			{
				# mark the user's email authenticate
				# and don't connect to mail server next time (to avoid slow performance)
				$_SESSION["EmailLoginFailure".$lu->UserLogin] = true;
			}
		}
	}
}
?>
<script language="javascript" defer="1">
$(document).ready( function() {
<?php if ($showJupasReadyIcon) { ?>
		$(window).resize(function() {
			relocateJupasReadyIcon();
		});

		relocateJupasReadyIcon();
	<?php } ?>
<?php if ($sys_custom['PowerClass'] && strstr($urlSrc, "/home/eAdmin/ResourcesMgmt/DigitalChannels") || ($_SESSION["platform"]=="KIS") || ($sys_custom['project']['CourseTraining']['IsEnable'])) { ?>
	$("#leftmenu").animate({
	  top: "<?=$KIS_TopPx?>px", opacity: 1
	}, { duration: 1000, queue: false });
<?php } ?>
	// style="position:absolute; z-index:1; left: 0px; top: 90px;">
});

function relocateJupasReadyIcon() {
	var ipf_link_pos = $('a#iPortfolioLink').position();
	var ipf_link_height = $('a#iPortfolioLink').height();
	$('img#jupasReadyIcon').css('top', ipf_link_pos.top - ipf_link_height + 6).css('left', ipf_link_pos.left - 28).show();
}

function changeClass(objID, classname)
{
	if ($('#'+objID).length>0)
	{
		$('#'+objID).attr("class",classname);
	}
}
<?=$jsCommands?>
<?php

if (!$_SESSION['Is_First_Login'])
{
?>
if ($('#'+'newFeatureSpan').length>0)
{
	$('#'+'newFeatureSpan').hide();
}
<?php
}
?>
</script>