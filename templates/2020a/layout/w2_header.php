<?php
// Using: Cameron 
// defined in /home/eLearning/w2/index.php
/*
 * 	Log
 * 	Date:	2015-02-11 [Cameron] change the order of thickbox.js, place it after script.{lang}.js because it uses global var defined in lang js file
 */
global $w2_thisUserType, $w2_thisIsTeaching, $r_contentCode, $w2_new_template_header 
	,$isMain, $isFirstLoginToday, $special_feature, $plugin; // for new layout animation

$PATH_WRT_ROOT_ABS = "/";

### Get School Name
$schoolName = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];


### Get User Identity Icon
switch($w2_thisUserType)
{
	case USERTYPE_STAFF:
		$userIdentity = $w2_thisIsTeaching==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
		$userIdentityClass = $w2_thisIsTeaching==1 ? "teacher" : "teaching_assistant";
		break;
	case USERTYPE_STUDENT:
		$userIdentity = $Lang['Identity']['Student'];
		$userIdentityClass = "student";
		break;
//	case USERTYPE_PARENT:
//		$userIdentity = $Lang['Identity']['Parent'];
//		$userIdentityClass = "icon_parent.gif";
//		break;
//	case USERTYPE_ALUMNI:
//		$userIdentity = $Lang['Identity']['Alumni'];
//		$userIdentityClass = "icon_student.gif";
//		break;
	default:
		$userIdentity = "";
		$userIdentityClass = "10x10.gif";
		break;
}

### Get User Name
//debug_pr($_SESSION['intranet_session_language']);
$userName = Get_Lang_Selection($_SESSION['ChineseName'], $_SESSION['EnglishName']);
### Get Lang Info
$intranet_default_lang = $_SESSION['intranet_default_lang'] ? $_SESSION['intranet_default_lang'] : $intranet_default_lang;
if ($intranet_default_lang == "gb") {
	$lang_to_change = ($intranet_session_language=="en")? "gb" : "en";
}
else {
	$lang_to_change = ($intranet_session_language=="en")? "b5" : "en";
}
$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];
$langCss = 'lang_'.$lang_to_change;


### 20140613 Timeout Start
$ControlTimeOutByJS = $special_feature['TimeOutByJS'];
//$ControlTimeOutByJS = false;

if ($ControlTimeOutByJS || $special_feature['TimeOutByJS'])
{
	$TimeOutWarningSecond = $special_feature['TimeOutWarningSecond'] ? $special_feature['TimeOutWarningSecond'] : 120;
	# session max life time in second
	$SessionLifeTime = ini_get('session.gc_maxlifetime') - $TimeOutWarningSecond;
	# below is just for quick testing
	// $SessionLifeTime = 2;
}
### 20140613 Timeout End

### 20140917 New Logo Start
//$plugin['W2_Old_Name'] = false;

if($plugin['W2_Old_Name']){
	$contentPageLogoClass = 'class_title_190';
	$projectTitle = '190 Project - Writing 2.0';
}
else{
	$contentPageLogoClass = 'class_title_190_easyWriting';
	$projectTitle = 'eClass easyWriting';
}

### 20140917 New Logo End

?>
<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'> 
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<title><?=$projectTitle?></title>

<style>
.sub_layer_board div{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
}

.sub_layer_board{
	clear: both;
	float: right;
	position: absolute;
	visibility: hidden;
	text-align: left;
	
}
.sub_layer_board .bubble_board_01{
	font-size: 1px;
	background-repeat: no-repeat;
	background-position: left top;
	display: block;
	height: 30px;
	width: 100%;
	margin:0;
	padding-left: 2px;
	clear: both;
	background-image: url(../../../images/2020a/addon_tools/sub_layer_bubble01.gif);
}

.sub_layer_board .bubble_board_02{
	font-size: 1px;
	background-repeat: no-repeat;
	display: block;
	background-position: right top;
	height: 30px;
	margin:0;
	margin-left: 12px;
	padding-right: 12px;
	background-image: url(../../../images/2020a/addon_tools/sub_layer_bubble02.gif);
	text-align:left;
}
.sub_layer_board .bubble_board_03{
	background-repeat: no-repeat;
	background-position: left top;
	display: block;
	width: 100%;
	margin:0;
	padding-left: 2px;
	clear: both;
	background-image: url(../../../images/2020a/addon_tools/sub_layer_bubble03.gif);
}

.sub_layer_board .bubble_board_04{
	background-repeat: no-repeat;
	background-position: right top;
	display: block;
	margin:0;
	margin-left: 12px;
	padding-right: 12px;
	background-image: url(../../../images/2020a/addon_tools/sub_layer_bubble04.gif);
	color: #000000;
}

.sub_layer_board .bubble_board_05{
	font-size: 1px;
	background-repeat: no-repeat;
	background-position: left top;
	display: block;
	height: 20px;
	width: 100%;
	margin:0;
	padding-left: 2px;
	clear: both;
	background-image: url(../../../images/2020a/addon_tools/sub_layer_bubble05.gif);
}

.sub_layer_board .bubble_board_06{
	font-size: 1px;
	background-repeat: no-repeat;
	display: block;
	background-position: right top;
	height: 20px;
	margin:0;
	margin-left: 12px;
	padding-right: 12px;
	background-image: url(../../../images/2020a/addon_tools/sub_layer_bubble06.gif);
}

/** Session timeout **/
.timeoutAlert {vertical-align: middle; background: url(../../../images/2020a/session_timeout/bg_transparentwhite.png) repeat;}
.timeoutAlert .contentContainer { width: 550px; height: 210px; position: relative; display: block; background: url(../../../images/2020a/session_timeout/time_bg.png) center; margin: 0 auto; padding: 0;}
.timeoutAlert .textTitle { top: 23px; left: 115px; height: 30px; position: absolute; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; color:#FFFFFF; width: 300px;  text-align: left;}
.timeoutAlert .textTitle .timeLeft { color: #FFFF33; font-size: 20px; }
.timeoutAlert .textContent { line-height:20px; top: 70px; left: 124px; position: absolute; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 13px; color:#000; width: 380px; text-align: left;}
.timeoutAlert .textContent u{ text-decoration:none; color:#FF0000; font-weight:bold; font-size:1.2em }
.textContent input {font-family:Verdana, Arial, Helvetica, sans-serif; font-weight:bold;font-size:13px; color:green; margin-top:10px;}
/** Session timeout **/
</style>
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/common.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/writing20.css" rel="stylesheet" type="text/css" />
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/writing20_extended.css" rel="stylesheet" type="text/css" />

<!--<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/skin_watercolor.css" rel="stylesheet" type="text/css" /> -->
<!--<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/skin_beach.css" rel="stylesheet" type="text/css" /> -->

<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/skin_camp.css" rel="stylesheet" type="text/css" />

<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/thickbox.css" rel="stylesheet" type="text/css" media="screen" />

<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/w2/writing20_print.css" rel="stylesheet" type="text/css" media="print" />
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-ui-1.7.3.custom.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/brwsniff.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/thickbox.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.highlight.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/js/w2/w2.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/json2.js"></script>
<?php if ($ControlTimeOutByJS): ?>	
			
<script language="JavaScript">
var LogoutCountValue = <?=$TimeOutWarningSecond?>;
var SecondLogout = LogoutCountValue;
var ToLogout=false;
var SecondLeftOriginal=<?=$SessionLifeTime?>;
var SecondLeft=<?=$SessionLifeTime?>;

function CountingDown(){
	if (SecondLeft>0)
	{
	   SecondLeft -= 1;
	   setTimeout("CountingDown()",1000);
	} else
	{
		// query server to see if there is no any response from user (e.g. using AJAX)
		path = "/get_last_access.php";
		$.ajax({
			url : path,
			type : "POST",
			success : function(msg){
				TimeLeft = parseInt(msg);
				
				if (TimeLeft>0)
				{
					SecondLeft = TimeLeft;
					CountingDown();
				} else
				{
					//alert("testing timeout method!")
					// notify user about the timeout
					// user can press "Continue" to cancel the timeout; if no action taken, system will (call logout page and) direct to timeout result page
					MM_showHideLayers('sub_layer_sys_timeout','','show');		
					bubble_block = document.getElementById("sub_layer_sys_timeout");
					bubble_block.style.left = "0px";
					bubble_block.style.top = '0px';
					bubble_block.focus();
					ToLogout = true;
					CountDownToLogout();
				}
			},
			
		});	
	}
}
CountingDown();
function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}

function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}

function CountDownToLogout()
{
	if (ToLogout)
	{
		if (SecondLogout>0)
		{
			SecondLogout -= 1;

			var browserWidth = f_clientWidth();
//			var browserHeight = f_clientHeight(); 
			var browserHeight = $(window).height();
			
			/*
			jsTimeOutTxt = '<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">';
			jsTimeOutTxt += '		<tr>';
			jsTimeOutTxt += '		<td bgcolor="orange" align="center">';
			jsTimeOutTxt += '		<table width="'+(browserWidth-60)+'" height="'+(browserHeight-60)+'" border="0" cellpadding="12" cellspacing="0">';
			jsTimeOutTxt += '			<tr>';
			jsTimeOutTxt += '			<td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>';
			jsTimeOutTxt += '			<img src="/images/2020a/alert_signal.gif" /><br /><font size="5"><b><font color="red">System is about to time out in '+SecondLogout+' seconds.</font><br />You can press <input type="button" style="font-size:23px" id="TimeOutContinue" value="CONTINUE" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()" /> to cancel the timeout and continue your use in eClass.</b></font>';
			jsTimeOutTxt += '			</td></tr>';
			jsTimeOutTxt += '		</table>';
			jsTimeOutTxt += '		</td></tr>';
			jsTimeOutTxt += '	</table>';
			*/
			
			jsTimeOutTxt2 = '<table class="timeoutAlert"  width="'+(browserWidth)+'" height="'+(browserHeight)+'"><tr><td align="center">';
			jsTimeOutTxt2 += '<div class="contentContainer">';
			jsTimeOutTxt2 += '<div class="textTitle"><span class="timeLeft">&nbsp;<?=$Lang['SessionTimeout']['ReadyToTimeout_Header']?></span></div>';
			jsTimeOutTxt2 += '<div class="textContent"><div class="line"><span><?=$Lang['SessionTimeout']['ReadyToTimeout_str1']?> <U> '+SecondLogout+' </U><?=$Lang['SessionTimeout']['ReadyToTimeout_str2']?><br><?=$Lang['SessionTimeout']['ReadyToTimeout_str3']?> </span> <input type="button"  value="<?=$Lang['SessionTimeout']['Continue']?>" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()"> ';
			jsTimeOutTxt2 += '<span><?=$Lang['SessionTimeout']['ReadyToTimeout_str4']?></span></div> ';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</td></tr></table>';
			jsTimeOutTxt2 += '	</table>';
			
			document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt2; 

			setTimeout("CountDownToLogout()",1000);
			
			// update message
		} else
		{
			// redirect to logout page
			self.location = "/logout.php?IsTimeout=1";//
		}
	} else
	{
		// reset logout timer
		SecondLogout = LogoutCountValue;//
	}
}


function CancelTimeOut()
{
	// stop redirect function
	SecondLogout = LogoutCountValue;
	ToLogout = false;
	alert("<?=$Lang['SessionTimeout']['CancelTimeout']?>");

	// restart counting down for time out
	SecondLeft = SecondLeftOriginal;
	CountingDown();
	
	var path = "/update_last_access.php";
	$.ajax({
			url : path,
			type : "POST",
			success : function(msg){
				TimeLeft = parseInt(msg);
			},
		});	
}



</script>
<?php endif; ?>

</head>

<body id="<?php echo $isMain?'bgmain':''; ?>">
	<?php if ($ControlTimeOutByJS): ?>
	<div class="sub_layer_board" id="sub_layer_sys_timeout" style="visibility:hidden;z-index:9999999;width:600px">	
		<span id="ajaxMsgBlockTimeOut">
			<table border="0" cellpadding="0" cellspacing="0" style="border:2px solid red;">
				<tr><td bgcolor="orange" align="center">
					<table width="620" height="400" border="0" cellpadding="12" cellspacing="0">
						<tr><td bgcolor="yellow" cellpadding="20" align="center"><font size="5"><b>&nbsp;</td></tr>
					</table>
				</td></tr>
			</table>
		</span>
	</div>
	<?php endif; ?>	
	
	<div id="Page_container">
	
	
		<?php if($isMain): ?>
		<!-- New Layout Design Cloud Start -->
		<div class="layer_cloud">
			<div class="cloud_con" style="left:640px; top:55px">
				<div class="sky_cloud cloud_anim_1"></div>
			</div>
			<div class="cloud_con flip" style="left:130px; top:10px">
				<div class="sky_cloud cloud_anim_3 cloud_size_3"></div>
			</div>
			<div class="cloud_con flip" style="left:-40px; top:-20px">
				<div class="sky_cloud cloud_anim_3 cloud_size_3"></div>
			</div>
			<div class="cloud_con" style="left:850px; top:-20px">
				<div class="sky_cloud cloud_anim_3 cloud_size_3"></div>
			</div>
			<div class="cloud_con flip" style="left:750px; top:0px">
				<div class="sky_cloud cloud_anim_3 cloud_size_3"></div>
			</div>
			<div class="cloud_con flip" style="left:0px; top:40px">
				<div class="sky_cloud cloud_anim_2r cloud_size_2"></div>
			</div>
			<div class="cloud_con flip" style="left:210px; top:5px">
				<div class="sky_cloud cloud_anim_2 cloud_size_2"></div>
			</div>
		</div>
		<!-- New Layout Design Cloud End -->
		<?php endif ?>

		<!-- ************ Head banner Start ************  -->
		<div id="topbanner">
			<div class="topouter_190">
				<div class="topinner">
					<div id="topleft_190">
						<?php if(!$isMain): ?>
						<a class="class_title_190_link" href="?"></a>
						<?php endif; ?>
						<div id="<?php echo $isMain?'class_title_190_nologo':$contentPageLogoClass; ?>" class="<?=$r_contentCode?>"> <?=$schoolName?> </div>
					</div>
				</div>
				<div id="topmiddle_190" class="print_hide">
					<div id="user_name_tool"> 
	                    <!--<a href="#" class="logout" title="<?=$Lang['Header']['Logout']?>"></a>--> 
	                    <!--<a href="/lang.php?lang=<?=$lang_to_change?>" class="<?=$langCss?>" title="<?=$Lang['Header']['ChangeLang']?>"></a>--><!--<a href="#" class="lang_en"></a>-->
	                    <!--<a href="#" class="help" title="<?=$Lang['Header']['OnlineHelp']?>"></a>--><!-- <a href="setting_basic_info.htm" class="tool" title="Setting"></a>-->
	                    <a href="index.php" class="home" title="<?=	$Lang['Header']['Menu']['Home']?>"></a>
	                    <span class="st">|</span>
	                    <span class="<?=$userIdentityClass?>" title="<?=$userIdentity?>"> <?=$userIdentity?> <?=$userName?> </span> 
                	</div>
                </div>
				<br style="clear:both"/>
			</div>
		</div>
		<!-- main menu  end -->
		<!-- ************ Head banner end ************  -->
		
				
			