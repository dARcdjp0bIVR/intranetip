<?
/*
 * 	Log
 * 	
 * 	@Purpose:	echo elibplus path for used in libms_circulation.html
 * 
 * 	2016-10-17 [Cameron]
 * 		- create this file
 */
if ($plugin['eLibraryPlusOne']) {
	echo 'elibplus';	
}
else {
	echo 'elibplus2';
}
?>