<? 
## Using By :  

## IMPORTANT IMPORTANT IMPORTANT:
##			If you add a new menu, please go to "lighten current menu (for cached method)" to add your newly added menu to lighten the parent menu. 
##-------------------------------------------------------------------------------------------------------------------------------------------------

/********************** Change Log ***********************/

/******************* End Of Change Log *******************/


global $home_header_no_EmulateIE7, $special_feature, $favicon_image;

$PATH_WRT_ROOT_ABS = "/";
#############################################
#####  home_header main codes
#############################################	
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

	
if (is_file($PATH_WRT_ROOT."includes/version.php"))
{
	/*
	$JustWantVersionData = true;
	include_once($PATH_WRT_ROOT."includes/version.php");
	
	$eClassVersion = $versions[0][0];
	*/
	$eClassVersion = Get_IP_Version();
	
	$pos = strrpos($eClassVersion, ".");
	if($pos>10)	$eClassVersion = substr($eClassVersion, 0, $pos);

} else
{
	$eClassVersion = "--";
}
global $linterface, $special_feature, $module_version,$stand_alone;

if (!isset($linterface))
{
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	$linterface = new interface_html("");
}

if (!isset($lu))
{
	$lu = new libuser($UserID);	
}

# protect for eClass logo!
/* Yuen: not yet effective
$eClassLogoData = md5(get_file_content($PATH_WRT_ROOT."images/2020a/logo_eclass_footer.gif"));
if ($eClassLogoData!="980442a2a7b4cb596f011830d5afd907")
{
	die("System cannot run due to a very import file for eClass logo is modified illegally!");
}
*/


$ControlTimeOutByJS = $special_feature['TimeOutByJS'];

//$ControlTimeOutByJS = true;
if ($ControlTimeOutByJS || $special_feature['TimeOutByJS'])
{
	$TimeOutWarningSecond = $special_feature['TimeOutWarningSecond'] ? $special_feature['TimeOutWarningSecond'] : 120;
	# session max life time in second
	$SessionLifeTime = ini_get('session.gc_maxlifetime') - $TimeOutWarningSecond;
	# below is just for quick testing
	// $SessionLifeTime = 2;
}


# Basic Information
$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];
// $intranet_default_lang = $_SESSION['intranet_default_lang'];
$intranet_default_lang = $_SESSION['intranet_default_lang'] ? $_SESSION['intranet_default_lang'] : $intranet_default_lang;

if ($intranet_default_lang == "gb" || get_client_region()=="zh_MY" || get_client_region()=="zh_CN")
	($intranet_session_language=="en") ? $lang_to_change = "gb" : $lang_to_change = "en";
else
	($intranet_session_language=="en") ? $lang_to_change = "b5" : $lang_to_change = "en";
$change_lang_to = $intranet_session_language=="en" ? $Lang['Header']['B5'] : $Lang['Header']['ENG'];

if (isset($_SESSION["SSV_PRIVILEGE"]["school"]["logo"]))
{ 
        $logo_img = $_SESSION["SSV_PRIVILEGE"]["school"]["logo"];
}
else
{
        $imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
        $logo_img = $imgfile == "" ? (($_SESSION["platform"]=="KIS")? "{$image_path}/kis/logo_kis.png" : "{$image_path}/2020a/topbar_25/eclass_logo.png") : "/file/$imgfile";
        $_SESSION["SSV_PRIVILEGE"]["school"]["logo"] = $logo_img;
}

if (!strstr($logo_img, "topbar_25/eclass_logo.gif"))
{
	$logo_fixed_height = " height='55' ";
}

$icalendar_name = $plugin["iCalendarFull"]?$iCalendar_iCalendar :$iCalendar_iCalendarLite;




# get the URL of this site
$PageURLArr = split("//", curPageURL());
if (sizeof($PageURLArr)>1)
{
	$strPath = $PageURLArr[1];
	$eClassURL = $PageURLArr[0] . "//" . substr($strPath, 0, strpos($strPath, "/"));
}

# eClass eCommunity data
$cs_path = "$intranet_httppath/home/support.php";

# product version change log
//$intranet_version_url = "tw-version.eclass.com.hk";
if (isset($intranet_version_url) && $intranet_version_url!="")
{
	$version_url = $intranet_version_url;
} else
{
	# by default
	$version_url = "hke-version.eclass.com.hk";
}
$products_path = "http://{$version_url}/?eclasskey=".$lu->sessionKey."&eClassURL=".$eClassURL;

$onlinehelpcenter_path = "http://support.broadlearning.com/doc/help/intranet/";

$doccenter_path = "http://support.broadlearning.com/doc/help/portal/";

$videocenter_path = "http://www.youtube.com/user/BroadLearning#grid/user/98D7F2448C1E4667";

$cs_faq = "http://support.broadlearning.com/doc/help/faq/";

# user identity
switch($_SESSION['UserType'])
{
	case USERTYPE_STAFF:	
		$UserIdentity = $_SESSION['isTeaching']==1 ? $Lang['Identity']['TeachingStaff'] : $Lang['Identity']['NonTeachingStaff'];
		$UserIdentityIcon = $_SESSION['isTeaching']==1 ? "icon_teacher.gif" : "icon_nonteacher.gif";
		break;
	case USERTYPE_STUDENT:
		$UserIdentity = $Lang['Identity']['Student'];
		$UserIdentityIcon = "icon_student.gif";
		break;
	case USERTYPE_PARENT:
		$UserIdentity = $Lang['Identity']['Parent'];
		$UserIdentityIcon = "icon_parent.gif";
		break;
	case USERTYPE_ALUMNI:
		$UserIdentity = $Lang['Identity']['Alumni'];
		$UserIdentityIcon = "icon_student.gif";
		break;
	default:
		$UserIdentity = "";
		$UserIdentityIcon = "10x10.gif";
		break;
}

# Build Menu
unset($FullMenuArr);

# Check menu focus
# Home
$CurrentPageArr['Home'] = 1;

$arrow_pos = $intranet_session_language=="en" ? 42 : 57;
### UI preparation for system timeout
$school_logo_link = $PATH_WRT_ROOT_ABS.$indexPage;

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- meta to tell IE8 to run backward compatible mode -->
<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"  /> 
<META Http-Equiv="Cache-Control" Content="no-cache">

<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/content_30.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/css/ereportcard.css" rel="stylesheet" type="text/css">
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/brwsniff.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery-1.3.2.min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/2007script.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/ajax_connection.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/swf_object/swfobject.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>lang/script.<?= $intranet_session_language?>.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.blockUI.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jquery.scrollTo-min.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/jquery/jqueryslidemenu.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT_ABS?>templates/<?=$LAYOUT_SKIN?>/js/SpryValidationTextField.js"></script>



<?php
if ($ControlTimeOutByJS)
{
?>
<script language="JavaScript">
var LogoutCountValue = <?=$TimeOutWarningSecond?>;
var SecondLogout = LogoutCountValue;
var ToLogout=false;
var SecondLeftOriginal=<?=$SessionLifeTime?>;
var SecondLeft=<?=$SessionLifeTime?>;

function CountingDown(){
	if (SecondLeft>0)
	{
	   SecondLeft -= 1;
	   setTimeout("CountingDown()",1000);
	} else
	{
		// query server to see if there is no any response from user (e.g. using AJAX)
		path = "/get_last_access.php";
		var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft);

	}
}
CountingDown();


var callback_timeleft =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);

		if (TimeLeft>0)
		{
			SecondLeft = TimeLeft;
			CountingDown();
		} else
		{
			//alert("testing timeout method!")
			// notify user about the timeout
			// user can press "Continue" to cancel the timeout; if no action taken, system will (call logout page and) direct to timeout result page
			MM_showHideLayers('sub_layer_sys_timeout','','show');		
			bubble_block = document.getElementById("sub_layer_sys_timeout");
			bubble_block.style.left = "0px";
			bubble_block.style.top = '0px';
			bubble_block.focus();
		
			ToLogout = true;
			CountDownToLogout();
		}
	}
}

function f_filterResults(n_win, n_docel, n_body) {
	var n_result = n_win ? n_win : 0;
	if (n_docel && (!n_result || (n_result > n_docel)))
		n_result = n_docel;
	return n_body && (!n_result || (n_result > n_body)) ? n_body : n_result;
}


function f_clientWidth() {
	return f_filterResults (
		window.innerWidth ? window.innerWidth : 0,
		document.documentElement ? document.documentElement.clientWidth : 0,
		document.body ? document.body.clientWidth : 0
	);
}
function f_clientHeight() {
	return f_filterResults (
		window.innerHeight ? window.innerHeight : 0,
		document.documentElement ? document.documentElement.clientHeight : 0,
		document.body ? document.body.clientHeight : 0
	);
}


function CountDownToLogout()
{
	if (ToLogout)
	{
		if (SecondLogout>0)
		{
			SecondLogout -= 1;

			var browserWidth = f_clientWidth();
			var browserHeight = f_clientHeight(); 
			
			jsTimeOutTxt2 = '<table class="timeoutAlert"  width="'+(browserWidth)+'" height="'+(browserHeight)+'"><tr><td align="center">';
			jsTimeOutTxt2 += '<div class="contentContainer">';
			jsTimeOutTxt2 += '<div class="textTitle"><span class="timeLeft">&nbsp;<?=$Lang['SessionTimeout']['ReadyToTimeout_Header']?></span></div>';
			jsTimeOutTxt2 += '<div class="textContent"><div class="line"><span><?=$Lang['SessionTimeout']['ReadyToTimeout_str1']?> <U> '+SecondLogout+' </U><?=$Lang['SessionTimeout']['ReadyToTimeout_str2']?><br><?=$Lang['SessionTimeout']['ReadyToTimeout_str3']?> </span> <input type="button"  value="<?=$Lang['SessionTimeout']['Continue']?>" onClick="MM_showHideLayers(\'sub_layer_sys_timeout\',\'\',\'hide\');CancelTimeOut()"> ';
			jsTimeOutTxt2 += '<span><?=$Lang['SessionTimeout']['ReadyToTimeout_str4']?></span></div> ';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</div>';
			jsTimeOutTxt2 += '</td></tr></table>';
			jsTimeOutTxt2 += '	</table>';
			
			document.getElementById("ajaxMsgBlockTimeOut").innerHTML = jsTimeOutTxt2; 

			setTimeout("CountDownToLogout()",1000);
			
			// update message
		} else
		{
			// redirect to logout page
			self.location = "/logout.php?IsTimeout=1";
		}
	} else
	{
		// reset logout timer
		SecondLogout = LogoutCountValue;
	}
}



var callback_timeleft_trival =
{
	success: function (o)
	{
		TimeLeft = parseInt(o.responseText);
	}
}



function CancelTimeOut()
{
	// stop redirect function
	SecondLogout = LogoutCountValue;
	ToLogout = false;
	alert("<?=$Lang['SessionTimeout']['CancelTimeout']?>");

	// restart counting down for time out
	SecondLeft = SecondLeftOriginal;
	CountingDown();
	
	var path = "/update_last_access.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest('POST', path, callback_timeleft_trival);
}



</script>
<?php
}
?>

<script language="JavaScript">
<!--
function support()
{
	newWindow("<?=$cs_path?>",10);
}

function toScrabble()
{
	document.scrabble_form.method = "post";
	document.scrabble_form.action = "http://sfc.broadlearner.com/main.php?schoolName=<?=$BroadlearningClientName?>&schoolType=<?=$config_school_type?>";
	document.scrabble_form.target = "scrabbleNewWin";
	
	window.open("", "scrabbleNewWin", "menubar=0,toolbar=0,resizable,scrollbars,status,top=40,left=40,width=800,height=500");
	var a = window.setTimeout("document.scrabble_form.submit();", 500);
}


function products()
{
	<?php if (!file_exists("$PATH_WRT_ROOT/home/support_version.php")) { ?>
	newWindow("<?=$products_path?>",10);
	<?php } else { ?>
	newWindow("/home/support_version.php",10);
	<?php } ?>
}

function onlinehelpcenter()
{
	newWindow("<?=$onlinehelpcenter_path?>",24);
}

function doccenter()
{
	newWindow("<?=$doccenter_path?>",24);
}


function faq()
{
	newWindow("<?=$cs_faq?>",24);
}

function videocenter()
{
	newWindow("<?=$videocenter_path?>",10);
}
//-->
</script>

<script language="Javascript">
<!--
// slide tool
var slide_ie4 = (document.all) ? true : false;
var slide_ns4 = (document.layers) ? true : false;
var slide_ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var sliding = {MyTools:false}
var hide = "hidden";
var show = "visible";

function open_lslp()
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
	newWindow('/home/asp/lslp.php',32);
}
function open_kskgs(subject)
{
	var intWidth = screen.width;
 	var intHeight = screen.height;
 	if(typeof(subject)!='undefined')
 		var url = '/home/asp/ksk.php?subject='+subject;
 	else
 		var url = '/home/asp/ksk.php';
 	newWindow(url,32);		
}		


function logout()
{
	if(confirm(globalAlertMsg16))
	{
           
		<?if($plugin['lslp']): ?>
                
                logoutscript = document.createElement('script');
                logoutscript.type = 'text/javascript';
                logoutscript.src = '<?= $lslp_httppath ?>/logout.php?action=redirect';
                document.getElementsByTagName('body')[0].appendChild(logoutscript);

                setTimeout(function(){window.location="/logout.php";}, 3000); <? //force logout if LSLP cannot load in 3 seconds?>
                
                <? else: ?>
                window.location="/logout.php";
                <? endif; ?>
		
	}
}
function slide(lay, h, speed, up) {
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                it=4;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; it=6;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left; it=10;}
        ih = parseInt(heig);

        if (up == 0){
                if (ih != h)
                        movelayer(lay, it);
                if (ih < h) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
                } else
                        setSliding(lay, 0);
        } else {
                if (ih != 25)
                        movelayer(lay, -it);
                if (ih > 25) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
                } else
                        setSliding(lay, 0);
        }
}


function setSliding(lay, on_off) {
        is_sliding = "sliding." + lay;
        is_sliding += (on_off==0) ? "=false" : "=true";
        eval (is_sliding);
}


function getSliding(lay) {
        is_sliding = "sliding." + lay;
        return eval(is_sliding);
}


function move(lay,h){
        if (getSliding(lay))
                return;
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                speed=1;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; speed=1;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left;speed=1;}
        ih = parseInt(heig);
        if (ih<=25)
                slide(lay,h,speed,0);
        else if (ih>=h)
                slide(lay,h,speed,1);
}
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function loadContent(evt,contentType){
	if (contentType == 'campus_link'){
		bubble_block = document.getElementById("sub_layer_campus_link");
		//link = document.getElementById('campusLinkico');
		//alert(document.defaultView.getComputedStyle(link,null).getPropertyValue('left'));

		bubble_block.style.left = (evt.clientX-230)+"px";

		bubble_block.style.top = '65px';
		document.getElementById("ajaxMsgBlock").innerHTML = "<?=$ip20_loading?> ...";
		document.getElementById("ajaxMsgBlock").style.display = "block";
		serverURL = "/home/campus_link/campus_link_load_content.php";
		$('div#sub_layer_campus_link div#bubble_board_content').load(serverURL,{loadType: "whole"},			function(){
			document.getElementById("ajaxMsgBlock").style.display = "none";
		});
	} else if (contentType == 'sys_about') {
		bubble_block = document.getElementById("sub_layer_sys_about");
					
						//var windowWidth = $(window).width();
						if( typeof( window.innerWidth ) == 'number' ) {
							//Non-IE
							myWidth = window.innerWidth;
						} else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
							//IE 6+ in \'standards compliant mode\'
							myWidth = document.documentElement.clientWidth;
						} else if( document.body && ( document.body.clientWidth) ) {
							//IE 4 compatible
							myWidth = document.body.clientWidth;
						}

						//alert(myWidth);
					
		//bubble_block.style.left = (evt.clientX-200)+"px";
		bubble_block.style.left = (myWidth-265)+"px";

		bubble_block.style.top = '15px';
// 		document.getElementById("ajaxMsgBlock2").style.display = "block";

		//MM_showHideLayers('sub_layer_sys_about','','show');
		document.getElementById('sub_layer_sys_about').style.visibility = 'visible';

		
	} else if(contentType == "power_tool"){
		
		bubble_block = document.getElementById("sub_layer_power_tool");
		bubble_block.style.left = (evt.clientX-160)+"px";
		bubble_block.style.right = "122px";			
		bubble_block.style.top = '65px';
	}
}

function emptyContent(layername)
{
	document.getElementById(layername).innerHTML = "";
}

$(window).resize(function() {
	if(document.getElementById('sub_layer_sys_about').style.visibility=="visible")
		$('#about_eclass').click();
	
});

function openBookFlix()
{
	<?php if (date("Y-m-d")>$sys_integration['bookflix_school_expiry'])	{ ?>
		alert("<?=str_replace('<expiry_date>', $sys_integration['bookflix_school_expiry'], $Lang['bookflix_expired'])?>");
	<?php } else { ?>
		newWindow("/home/bookflix.php",8);
	<?php }  ?>
}

//-->
</script>

<link id="page_favicon" href="/images/<?=$favicon_image?>" rel="icon" type="image/x-icon" />
<title><?=$Lang['Header']['HomeTitle']?></title>

</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >

<tr>
	<Td height="75">
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
	  <tr>
			<td class="top_banner" valign="top">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="120" rowspan="2" valign="middle" align="center">
						<div class="school_logo">&nbsp;</div>
						<a href="<?=$school_logo_link?>"><img src="<?= $logo_img?>" border="0" <?=$logo_fixed_height?>></a>
					</td>
					<td height="38" valign="top" nowrap>
						<div class="school_title"> <?= $school_name ?> </div>
						<div class="user_login">
							<img src="<?=$PATH_WRT_ROOT_ABS?>images/<?=$LAYOUT_SKIN?>/topbar_25/<?=$UserIdentityIcon?>" align="absmiddle"> <?=$UserIdentity?>
							<span>|</span>
							
							<? if(!$sys_custom['hide_lang_selectioin'][$_SESSION['UserType']] || ($sys_custom['hide_lang_selectioin_except_school_admin'] && $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])) {?>
								<a href="/lang.php?lang=<?= $lang_to_change?>" title="<?=$Lang['Header']['ChangeLang']?>"><?=$change_lang_to?></a>
							<? } ?>
							<a href="javascript:logout()" title="Logout">X</a>
						</div>
					</td>
				</tr>
					<tr>
						<td valign="top" nowrap>

						</td>
					</tr>
			</table></td>
	  	</tr>
	</table>
	</td>
</tr>
<tr>
<td valign="top">

