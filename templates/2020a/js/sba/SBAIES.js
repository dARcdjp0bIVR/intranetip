/* Modified by */
/*
 * Modification Log
 *
 * 2012-08-14 Bill:
 *		- comment out student tool
 *		- add loadReflectNote for handling Reflect Note
 * 
 * 2012-06-22 Thomas:
 * 		- Comment out the definition of $("a.task_edit").click()
 * */


// all JS for your reference only 


$(document).ready(function(){
	
	


	//show hide scheme option					   
			
		$("ul.scheme_option").hide(); 
		$("a.scheme_select").click( 
			function () {
				$("ul.scheme_option").slideToggle("normal");
				return false;
			}
		);




	//tool list action			   


        $('table.tool_record_top').toggle(
          function(event) {
			$(this).parent().addClass('selected_list');
          },
          function(event) {
            $(this).parent().removeClass('selected_list');
          }
        );



	//selection box action			   


        $('.selectAnsList ul li').toggle(
          function(event) {
            $(this).addClass('active'); 
          },
          function(event) {
            $(this).removeClass('active');
          }
        );

	/*Turn off right tool and padding on 14-08-2012
	//student tool
        $('a.reflect_note').toggle(
          function(event) {
            $('.IES_sheet_wrap').css('padding-right', '310px'); // 310px to 360px 20-05-2010
			$('.IES_right_tool').css('display', 'block');
			$('li.reflect_note').addClass('current');
          },
          function(event) {
            $('.IES_sheet_wrap').css('padding-right', '1px');
			$('.IES_right_tool').css('display', 'none');
			$('li.reflect_note').removeClass('current');
          }
        );
     */   		

	//show task_index_edit btn	
        /* Comment out by Thomas on 2012-06-22
		$("a.task_edit").click( 
			function () {
				$("div.task_index_edit").css("display" , "block");
				$("a.task_edit").css("display" , "none");
				return false;
			}
		);
		*/
		
		

	//show hide ans box	

		//$("a.show_intro").toggle( 
			//function (event) {
				//$(this).parent().find("div.IES_ans_box").css("display" , "none");
				//$(this).parent().find("div.task_intro_box").css("display" , "block");
				//$(this).removeClass("show_intro").addClass("hide_intro");
				
			//},
			//function (event) {
				//$(this).parent().find("div.IES_ans_box").css("display" , "block");
				//$(this).parent().find("div.task_intro_box").css("display" , "none");
				//$(this).removeClass("hide_intro").addClass("show_intro");
				
			//}
		//);

});



// common function for going to specific scheme
function doScheme(jParSchemeID){
	if (!document.form1){
		alert("Related Infomation (form) cannot be found! Cannot perform action.");
	}
	else
	{
		if(document.form1.scheme_id)
		{
			document.form1.scheme_id.value = jParSchemeID;
			document.form1.action = "index.php";
			document.form1.submit();		
		}
		else{
			alert("Related Infomation (scheme id) cannot be found! Cannot perform action.");
		}

	}
}

function goToStage(scheme_id, stage_id) {

	document.form1.scheme_id.value = scheme_id;
	document.form1.stage_id.value = stage_id;
	document.form1.action = "stage.php";
	document.form1.submit();
}

function goToCoverPage(schemeID,stageID)
{
	var goURL = "coverpage.php?scheme_id="+schemeID+"&stage_id="+stageID;
	window.location = goURL; 
}

function insertTemplateAtCursor(id, myValue) {
	myField = document.getElementById(id);
	//IE support
	if (document.selection) {		
		myField.focus();
		sel = document.selection.createRange();	
		sel.text = myValue;		
		sel.moveStart('character', -myValue.length);
		sel.select();
	}
	//FF 
	else if (myField.selectionStart || myField.selectionStart == '0') {
		var startPos = myField.selectionStart;
		var endPos = myField.selectionEnd;
		myField.value = myField.value.substring(0, startPos)
						+ myValue
						+ myField.value.substring(endPos, myField.value.length);
		
		myField.selectionStart = startPos;
		myField.selectionEnd = startPos + myValue.length;
		myField.focus();
	} 
	//another
	else {
		myField.value += myValue;
		myField.focus();
	}
}

function goOtherStudent(ParKey) {
	$targetPage = $("form[name=form1]");
	$("input[name=key]").val(ParKey);
	$targetPage.attr("action","");
	$targetPage.submit();
}
function goGroupingList() {
	$targetPage = $("form[name=form1]");
//	$targetPage.attr("action","questionnaireGroupingList.php");
	$('input#mod').val('survey');
	$('input#task').val('questionnaireGroupingList');
	$targetPage.submit();
}

function goGrouping() {
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","questionnaireGrouping.php");
	$targetPage.submit();
}

function goDisplay() {
	$targetPage = $("form[name=form1]");
	//$targetPage.attr("action","questionnaireDisplay.php");
	$('input#mod').val('survey');
	$('input#task').val('questionnaireDisplay');
	$targetPage.submit();

}

function goManagement() {
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","management.php");
	$targetPage.submit();
}

function goDiscoveryList() {
	$targetPage = $("form[name=form1]");
	$targetPage.attr("action","questionnaire_discovery_list.php");
	$targetPage.submit();
}
function goNewAnalysis(){
	$targetPage = $("form[name=form1]");
//	$targetPage.attr("action","questionnaireGroupingNew.php");
	$('input#mod').val('survey');
	$('input#task').val('questionnaireGroupingNew');
	$targetPage.submit();
}

function loadReflectNote(contentType, scheme_id, stage_id, note_id, note_Content)
{
	if(typeof(note_Content)==="undefined")
		note_Content = '';
		
	if(contentType==="update_note"){
		data_transmit = "type=update_note&scheme_id="+scheme_id+"&stage_id="+stage_id+"&noteid="+note_id+"&noteContent="+note_Content;
	}else{
		data_transmit = "type="+contentType+"&scheme_id="+scheme_id+"&stage_id="+stage_id+"&noteid="+note_id;
	}
	
	$.ajax({
			url		: "index.php?mod=ajax&task=ajax_coursework_toolBar_handler",
			type	: "POST",
			data	: data_transmit,
			success : function(data){
						$('#reflect_note').html(data);
						$('#reflect_note').show();
				     }
	});
}

function loadContent(contentType, scheme_id)
{
	$.ajax({
			url		: "index.php?mod=ajax&task=ajax_coursework_toolBar_handler",
			type	: "POST",
			data	: "type="+contentType+"&scheme_id="+scheme_id,
			success : function(data){
						$('#'+contentType).html(data);
						$('#'+contentType).show();
				     }
	});
}

function editPowerConcept(scheme_id, stage_id, task_id, step_id, answer_id, PowerConceptID){
	newWindow('',10); // open a blank popup
	
	var obj = document.getElementById('sba_step_handinForm_'+step_id);
	obj.method = "POST";
	obj.target = "intranet_popup10";
	obj.action = "index.php?mod=content&task=stepEditPowerConceptInterface&scheme_id="+scheme_id+"&stage_id="+stage_id+"&task_id="+task_id+"&step_id="+step_id+"&r_answer_id="+answer_id+"&r_powerconcept_id="+PowerConceptID;
	obj.submit();
}

function viewPowerConcept(PowerConceptID){
	newWindow('/templates/powerconcept/view.php?PowerConceptID='+PowerConceptID,10); // open a blank popup
}