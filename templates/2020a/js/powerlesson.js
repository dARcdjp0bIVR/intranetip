function loadPowerLessonList(evt){
	bubble_block = document.getElementById("sub_layer_powerlesson");
	bubble_block.style.left = (evt.clientX-280) + "px";
	bubble_block.style.top = (evt.clientY+10) + "px";
	document.getElementById("ajaxMsgBlock").innerHTML = "Loading ...";
	document.getElementById("ajaxMsgBlock").style.display = "block";
	var serverURL = "powerlesson_ajax.php";
	var params = {};
	params["ajax_action"] = "get_active_lesson_list";
	$('div#sub_layer_powerlesson div#bubble_board_content').load(
		serverURL, params, function(){
			document.getElementById("ajaxMsgBlock").style.display = "none";
	});
}


var Lesson = (function(){
	var lesson_instance;
	return {
		login_lesson: function (user_course_id,session_id){
			var allcookies = document.cookie;
			if (allcookies.search('having_lesson') != -1 && lesson_instance != null){
				if (lesson_instance != null)
					lesson_instance.focus();
				//window.location.assign('eclass/index.php');
				return;
			}
			//window.location.assign('/home/eLearning/login.php?uc_id='+user_course_id+'&session_id='+session_id+'&classLogin=1');
			lesson_instance = newWindow('/home/eLearning/login.php?uc_id='+user_course_id+'&session_id='+session_id+'&classLogin=1', 8);
		}
	}
})();

