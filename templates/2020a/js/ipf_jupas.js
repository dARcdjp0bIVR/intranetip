function js_Onkeyup_Info_Textarea(jsTextObjID, jsDisplaySpanID, jsMaxWordCount, jsMaxCharCount, jsCountType) {
	//limitText(document.getElementById(jsTextObjID), jsMaxCharCount, 1, 3);
	
	//js_Update_Word_Count($('#' + jsTextObjID).val(), jsDisplaySpanID, jsMaxWordCount);
	var jsMaxCount = (jsCountType=='char')? jsMaxCharCount : jsMaxWordCount;
	js_Update_Text_Counter($('#' + jsTextObjID).val(), jsDisplaySpanID, jsMaxCount, jsCountType);
}

function js_Update_Text_Counter(jsText, jsDisplaySpanID, jsMaxCount, jsCountType) {
	var jsCountVal = 0;
	if (jsCountType=='char') {
//		var jsTextLength = jsText.length;
//		var i = 0;
//		for (i=0; i<jsTextLength; i++) {
//			//var jsThisChar = jsText[i]; <== not work in IE
//			var jsThisChar = jsText.charAt(i);
//			
//			if (isIncludeChineseChar(jsThisChar)) {	// Chinese characters count as 3 English characters for JUPAS
//				jsCountVal += 3;
//			}
//			else {
//				jsCountVal += 1;
//			}
//		}
		jsCountVal = js_Get_Jupas_Character_Count(jsText);
	}
	else {
		jsCountVal = js_Get_Word_Count(jsText);
	}
	jsCountVal = (jsCountVal=='' || jsCountVal==null)? 0 : jsCountVal;
	
	$('span#' + jsDisplaySpanID).html(jsCountVal);
	
	// mark the Word Count as red if exceeded the maximum
	if (jsMaxCount > 0 && jsCountVal > jsMaxCount) {
		$('span#' + jsDisplaySpanID).addClass('tabletextrequire');
	}
	else {
		$('span#' + jsDisplaySpanID).removeClass('tabletextrequire');
	}
}

//function js_Update_Word_Count(jsText, jsDisplaySpanID, jsMaxWordCount) {
//	var jsWordCount = js_Get_Word_Count(jsText);
//	jsWordCount = (jsWordCount=='' || jsWordCount==null)? 0 : jsWordCount;
//	$('span#' + jsDisplaySpanID).html(jsWordCount);
//	
//	// mark the Word Count as red if exceeded the maximum
//	if (jsMaxWordCount > 0 && jsWordCount > jsMaxWordCount) {
//		$('span#' + jsDisplaySpanID).addClass('tabletextrequire');
//	}
//	else {
//		$('span#' + jsDisplaySpanID).removeClass('tabletextrequire');
//	}
//}

function js_Get_Content_Exceed_Maximum_Warning_Msg(jsWarningText, jsMaxWordCount) {
	return jsWarningText.replace(/<!--MaxWordCount-->/i, jsMaxWordCount);
}

function js_Get_Jupas_Character_Count(jsText, jsLineBreakCount) {
	jsLineBreakCount = jsLineBreakCount || 1;
	
	var jsCountVal = 0;
	var jsTextLength = jsText.length;
	var i = 0;
	for (i=0; i<jsTextLength; i++) {
		//var jsThisChar = jsText[i]; <== not work in IE
		var jsThisChar = jsText.charAt(i);
		
		if (jsThisChar == "\n") {
			jsCountVal += jsLineBreakCount;
		}
		else if (isIncludeChineseChar(jsThisChar)) {	// Chinese characters count as 3 English characters for JUPAS
			jsCountVal += 3;
		}
		else {
			jsCountVal += 1;
		}
	}
	
	return jsCountVal;
}