//function showModelAns(ansNum) {
//	$('#modelAnsSpan_' + ansNum).show();
//	
//	checkEnableSubmitBtn();
//}

function showModelAns(ansCode, ansType) {
	// Show answer only if the student has type / choose the answer
	var hasAnswered = checkHasAnswer(ansCode, ansType);
	if (hasAnswered) {
		var modelAnsSpanId = getJQuerySaveId('modelAnsSpan_' + ansCode);
		$('#' + modelAnsSpanId).show();
		checkEnableSubmitBtn();
	}	
}

function saveIndividualAns(ansCode, ansType) {
	var hasAnswered = checkHasAnswer(ansCode, ansType);
	if (hasAnswered) {
		saveStepAns(0, '', ansCode);
	}
}

function showAndSaveModelAns(ansCode, ansType) {
	showModelAns(ansCode, ansType);
	saveIndividualAns(ansCode, ansType);
}

function checkHasAnswer(ansCode, ansType) {
	var studentAnsInputName = getJQuerySaveId('r_question[' + ansType + '][' + ansCode + ']');
	
	var hasAnswered = false;
	switch (ansType.toUpperCase()) {
		case 'TEXT':
		case 'TEXTAREA':
		case 'SELECT':
			if (Trim($('[name="' + studentAnsInputName + '"]').val()) != '') {
				hasAnswered = true;
			}
			break;
		case 'RADIO':
			if ($('input[name="' + studentAnsInputName + '"]:checked').length > 0) {
				hasAnswered = true;
			}
			break;
		case 'CHECKBOX':
			if ($('input[name="' + studentAnsInputName + '\\[\\]"]:checked').length > 0) {
				hasAnswered = true;
			}
			break;
	}
	
	return hasAnswered;
}

function checkEnableSubmitBtn() {
	var allAnswered = true;
	
	// Check if all model answers are clicked
	$('.modelans').each( function() {
		if ($(this).is(':hidden')) {
			allAnswered = false;
		}
	});
	
	// Check if all answer has input
//	$('.w2_ansInput').each( function() {
//		if ($(this).val().Trim() == '') {
//			allAnswered = false;
//		}
//	});
//	
//	$('.w2_ansInput').each( function() {
//		switch ($(this).attr('type').toUpperCase()) {
//			case 'RADIO':
//				if (parseInt($('input:radio[name="' + getJQuerySaveId($(this).attr('name')) + '"]:checked').length) == 0) {
//					allAnswered = false;
//				}
//				break;
//			default:
//				if (isObjectValueEmpty($(this).attr('id'))) {
//					allAnswered = false;
//				}
//		}
//	});
	
	if (allAnswered) {
		$('#saveButton').attr('disabled', '');
	}
	else {
		$('#saveButton').attr('disabled', 'disabled');
	}
}

function checkEnableSubmitBtnByInfoxbox() {
	var isAllViewed = true;
	$('input.w2_infoxBoxStatusHidden').each( function() {
		if ($(this).val() != 3) {	// $w2_cfg["DB_W2_STEP_HANDIN_ANSWER"]["infoboxHasSubmitted"] = 3
			isAllViewed = false;
		}
	});
	
	if (isAllViewed) {
		$('input#saveButton').attr('disabled', '');
	}
	else {
		$('input#saveButton').attr('disabled', 'disabled');
	}
}

function updateOthersTextboxStatus() {
	$("input.othersChk:enabled").each( function() {
		if ($(this).attr('checked')) {
			$(this).next('.othersTb:input').attr('disabled', '');
		}
		else {
			$(this).next('.othersTb:input').attr('disabled', 'disabled').val('');
		}
	});
}

//function triggerInfoBoxContent(infoBoxCode) {
//	var infoBoxDivId = 'infobox_' + infoBoxCode;
//	var infoBoxContentDivId = 'infobox_content_' + infoBoxCode;
//	
//	if ($('div#' + infoBoxContentDivId).is(':visible')) {
//		// show -> hide
//		$('div#' + infoBoxContentDivId).hide();
//		$('div#' + infoBoxDivId).removeClass('infobox_open');
//		
//	}
//	else {
//		// hide -> show
//		$('div#' + infoBoxContentDivId).show();
//		$('div#' + infoBoxDivId).addClass('infobox_open');
//	}
//}

function triggerInfoBoxContent(linkObj) {
	var infoboxContentObj = $(linkObj).next('div.w2_infoxboxContentDiv');
	
	if ($(infoboxContentObj).is(':visible')) {
		// show -> hide
		$(infoboxContentObj).hide();
		//$(infoboxContentObj).removeClass('infobox_open');
	}
	else {
		// hide -> show
		$(infoboxContentObj).show();
		//$(infoboxContentObj).addClass('infobox_open');
	}
}

function saveInfoboxHandling(infoboxCode, targetInfoboxStatus, targetStepStatus) {
	var infoboxDivId = 'infobox_' + infoboxCode;
	
	// all answers are answered => process save
	if ($('div#' + infoboxDivId + ' span.w2_modelAnsSpan:hidden').length == 0) {
		// change the hidden field value of the infobox status
		$('input#infoboxStatus_' + infoboxCode).val(targetInfoboxStatus);
				
		// save the step status as draft and infoxbox status as submitted
		saveStepAns(0, targetStepStatus, infoboxCode);
		
		// disable the view button
		var infoboxViewBtnId = 'infoboxViewBtn_' + infoboxCode;
		if ($('div#' + infoboxDivId + ' span.modelans:hidden').length == 0) {
			$('input#' + infoboxViewBtnId).attr('disabled', true);
		}
		
		// change the done / notdone display of the title
		$('div#' + infoboxDivId + ' span.done').show();
		$('div#' + infoboxDivId + ' span.notdone').hide();
		
		// enable the next button if all infoxboxes have been viewed
		checkEnableSubmitBtnByInfoxbox();
	}
}

function editorOnDocumentReadyHandling(editorSource, iFrameSource) {
	if (editorSource == 'ECLASS') {
		// form post to eclass php to genereate the editor
		var jsForm = document.getElementById('handinForm');
		jsForm.action = iFrameSource;
		jsForm.target = 'editorIFrame';
		jsForm.submit();
		
		// restore form value after submit (not affect others form post action)
		jsForm.action = 'index.php';
		jsForm.target = '_self';
	}
}

function editorIndividualAnsHandling(editorName) {
	var ansValue = getEditorValue(editorName);
	$('#' + getJQuerySaveId(editorName)).val(ansValue);
}