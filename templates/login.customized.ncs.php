<?php 
include_once("../includes/global.php");
include_once("../includes/SecureToken.php");

intranet_opendb();
$SecureToken = new SecureToken();
if(isset($intranet_default_lang)){
	$title_lang = $intranet_default_lang;
}else{
	$title_lang = 'b5';
}

switch($title_lang){
	case 'b5':
		$display_lang = '中文';
		break;
	case 'en':
		$display_lang = 'English';
		break;
	default:
		$display_lang = '';
		break;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>C-for-Chinese @ JC | 友趣學中文</title>
	<link href="polyu_ncs/css/main.css" rel="stylesheet" type="text/css">
	<link href="polyu_ncs/css/login.css" rel="stylesheet" type="text/css">
	<link href="polyu_ncs/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="shortcut icon" href="polyu_ncs/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="polyu_ncs/images/favicon.ico" type="image/x-icon">
	<script language=JavaScript src=/lang/script.<?=$title_lang?>.js></script>
	<script language=JavaScript1.2 src=/templates/script.js></script>
	<script language=JavaScript src="polyu_ncs/js/common.js"></script>

	<style>
		/* Test Show & Hide Effect */
		#blkMenu.hide
		{
			display: none;
		}
	</style>
	<script>
	var currLang = "<?=$title_lang?>";
	var forgetPwdMsg = ForgetPwdMsg_lang[currLang];
	$(document).ready(function() {
		updateDisplayLang();
	});
		
	function checkForgetForm(){
	     obj = document.form2;
	     tmp = prompt(forgetPwdMsg, "");
	     if(tmp!=null && Trim(tmp)!=""){
	          obj.UserLogin.value = tmp;
	          obj.submit();
	     }
	}
	function checkLoginForm(){
		$(".invaidMsg").css('display','none');
		$( ".login" ).attr( "disabled", true );
		var obj = document.form1;
		var pass=1;
		
		if(Trim(document.getElementById('UserLogin').value)==""){
			pass  = 0;
			$('#lblInvaidId').css('display','block');
		}else if(Trim(document.getElementById('UserPassword').value)==""){
			pass = 0;
			$('#lblPwd').css('display','block');
		}
		if(pass)	
		{
			return true;
		}
		$( ".login" ).attr( "disabled", false );
		return false;
	}
	function changeLang(toLang){
		$('#display_lang').html(display_lang[toLang]);
		$('.lang_btn').css('display','block');
		$('#lang_btn_'+toLang).css('display','none');
		$('#DirectLink').val("/ncs/index.php?portal_lang="+toLang);
		currLang = toLang;
		forgetPwdMsg = ForgetPwdMsg_lang[toLang];
		updateDisplayLang();
		$('#blkMenu').attr('class','hide');
	}
	
	function updateDisplayLang(){
		$('#lblInvaidId').html(lblInvaidId_lang[currLang]);
		$('#lblId').html(lblId_lang[currLang]);
		$('#lblInvaidPw').html(lblInvaidPw_lang[currLang]);
		$('#lblPwd').html(lblPwd_lang[currLang]);
		$('#login_btn1').val(BtnLogin_lang[currLang]);
		$('#lblForgetPwd').html(lblForgetPwd_lang[currLang]);
	}
	</script>
</head>

<body>
	<!-- Full width Canvas with min-width 1024px -->
	<div id="blkCanvas">
		<div id="blkContainer" class="absolute fill">
			<div id="blkUpper">
				<span id="blkLoginWrapper">
					<div id="blkLogin">
						<form name="form1" action="../login.php" method="post" onSubmit="return checkLoginForm();">
							<img alt="logo" class="marginB20" src="polyu_ncs/images/logo.png">
							<div class="invaidMsg" id="lblInvaidId" <?php if($err!=1) { ?>style="display:none;"<?php }?>>
							</div>
							<div class="marginB10" id="lblId"></div>
							<input type="text" name="UserLogin" id="UserLogin" autofocus/>
							<div class="invaidMsg" id="lblInvaidPw"  <?php if($err!=2) { ?>style="display:none;"<?php }?>>
							</div>
							<div class="marginB10" id="lblPwd"></div>
							<input type="password" name="UserPassword" id="UserPassword"/>
							<input type="hidden" name="DirectLink" id="DirectLink" value="/ncs/index.php?portal_lang=<?=$title_lang?>">
							<input name="submit" type="submit" id="login_btn" class="login" style="display:none"/>
							<!-- <input type="hidden" name="target_url" value="/polyu_ncs/portal.php"> -->
							<?=$SecureToken->WriteFormToken()?>
						</form>	
						<input name="submit1" type="button" id="login_btn1" value="" class="login_button pointer login" onclick="document.form1.submit.click();"/>
						<a href="javascript:checkForgetForm();" class="forgot_password"><span style="overflow:hidden;" id="lblForgetPwd"></span></a>
						<div id="lbleClass"><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="polyu_ncs/images/eClassLogo.png"></a></div>
						
					</div>
				</span>
				<!--Test Show & Hide Effect-->
				<script>
				$(document).ready(function()
				{
					var $btnLang = $("#btnLang");
					var $blkMenu = $("#blkMenu");

					$btnLang.click(function()
					{
						$blkMenu.toggleClass("hide");
					});
				});
				</script>
				<!--Test Show & Hide Effect-->
				<div id="btnLangContainer">
					<div id="btnLang">
						<span class="fa fa-globe"></span><span id="display_lang"><?=$display_lang?></span><span class="fa fa-angle-down"></span>
					</div>
					<div id="blkMenu" class="hide">
						<div class="menu_button lang_btn" id="lang_btn_en" onclick="changeLang('en')" <?php if($title_lang=='en'){?>style="display:none"<?php }?>>English</div>
						<div class="menu_button lang_btn" id="lang_btn_b5" onclick="changeLang('b5')"<?php if($title_lang=='b5'){?>style="display:none"<?php }?>>中文</div>
					</div>
				</div>
			</div>
			<div id="blkLower">
					<img src="polyu_ncs/images/logos.png">
			</div>
			<form name=form2 action=../forget.php method=post>
				<input type=hidden name=UserLogin>
				<input type=hidden name=url_success value="/templates/index.php?msg=1">
				<input type=hidden name=url_fail value="/templates/index.php?err=2">
				<?=$SecureToken->WriteFormToken()?>
			</form>
		</div>
	</div>
</body>
</html>
<?php
intranet_closedb();
?>