CKEDITOR.plugins.add( 'ReplyTemplate', {
	// jscs:disable maximumLineLength
	lang: 'en,zh', // %REMOVE_LINE_CORE%
	// jscs:enable maximumLineLength
	init: function( editor ) {
		editor.addCommand( 'ReplyTemplate', new CKEDITOR.dialogCommand( 'ReplyTemplate', {
				exec: function( editor ) {
	            	var diagObj;
	            	window.open('/templates/ckeditor/plugins/ReplyTemplate/dialogs/ReplyTemplate.php?editorName='+editor.name, 'ReplyTemplate',"menubar,toolbar,top=40,left=40,width=800,height=600");
	
	            }		
		} ) );
		editor.ui.addButton && editor.ui.addButton( 'ReplyTemplate', {
			label: editor.lang.ReplyTemplate.title,
			command: 'ReplyTemplate',
			toolbar: 'insert,50'
		} );
	}
} );

function addTemplatesToEditor(editorName,html){
	var oEditor = CKEDITOR.instances[editorName];
	oEditor.insertHtml(html);
}