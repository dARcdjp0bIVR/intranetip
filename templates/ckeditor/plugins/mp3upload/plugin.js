// Modifying by : Paul
// Modification Log:
// 2019-05-17 (Paul) [ip.2.5.10.6.1]
//	- update to ensure categoryId is always an integer
// 2019-05-15 (Paul) [ip.2.5.10.6.1]
//	- Using ckeditor config of moduleCategory to be the category ID first and then parent category, as parent.category is not reliable 
( function() {
	CKEDITOR.plugins.add( 'mp3upload', {
		lang: 'en,zh',
	    icons: 'ed_mp3upload.gif',
		hidpi: false,
	    init: function( editor ) {
	    	
	    	editor.addCommand( 'insertmp3upload', {
	            exec: function( editor ) {
	            	var categoryId = editor.config.moduleCategory;
	            	var diagObj;
	            	window.open('/eclass40/src/tool/misc/html_editor/popups/insert_voice_for_ckeditor.php?editorName='+editor.name+'&categoryID='+categoryId, 'mp3upload',"menubar,toolbar,top=40,left=40,width=580,height=310");
	
	            }
	        });
	    	
	        editor.ui.addButton( 'mp3upload', {
	            label: editor.lang.mp3upload.mp3upload,
	            command: 'insertmp3upload',
	            toolbar: 'insert'
	        });
	        
	    },
	    onLoad: function() {
	        CKEDITOR.addCss(
	            '.pv_image{' + 
	        	'   background-image: url('+this.path+'images/pv_logo.png);' + 
	        	'   background-position: center center;' + 
			    '   background-repeat: no-repeat;' + 
			    '   border: 1px solid #a9a9a9;' + 
			    '   width: 80px;' + 
			    '   height: 80px;' + 
			    '}'
	        );
	    }
	});
})();

function saveMP3InCkEditor(editorName,fileId,url){
	var oEditor = CKEDITOR.instances[editorName];
	// Update UI
	var categoryId = oEditor.config.moduleCategory;
	var pvIndex = new Date().getTime();
	var x="";
	var fileName = url.replace(/^.*(\\|\/|\:)/, '');
	// Build outout UI
	
	x += '<div id="fck_pv_container_'+pvIndex+'" style="border:2px orange dotted;padding:10px;width:300px;float:left;">'; 
	x += '<div id="pv_name_'+pvIndex+'">'+fileName+'</div>';
	x += '<div type="fck_pv" id="tmp_pv_'+pvIndex+'" fck_filepath="'+url+'" file_id="'+fileId+'" fck_pvcat="'+categoryId+'"></div>';	
	x += '</div>';
	x += '<BR style="clear:both;" /> ';
	
	//x += '<div id="fck_pv_container_'+pvIndex+'" title="'+fileName+'"><p>'+fileName+'</p><audio id="fck_pv" controls controlsList="nodownload"><source src="'+url+'"></audio></div>';
	var newElement = CKEDITOR.dom.element.createFromHtml( x, oEditor.document );
	oEditor.insertElement( newElement );


}
function browseFileLink(myAtt, dObj, dCat, dFile){
	diagObj = dObj;
	diagFile = dFile;
	var folderID = typeof(parent.folderID)=='undefined'?'':parent.folderID;
	var attachment = typeof(parent.attachment)=='undefined'?'':parent.attachment;
	var categoryId = dCat;
	var url = "/eclass40/src/resource/eclass_files/files/index.php?attachment="+myAtt+"&fieldname=html_editor_diagObj&isFromCk=1&attach=1&attachment="+attachment+"&folderID="+folderID+"&category="+categoryId+"&isImageOnly=0";
	window.open(url,'intranet_popup1','resizable,scrollbars,status,top=40,left=40,width=870,height=543');
}
function getFilePath(rPath){
	diagObj.value = rPath;
	return;
}
function getFileID(value){
	diagFile.value = value;
	return;
}
if(typeof file_exists != 'function'){
	function file_exists(editorName){
		var oEditor = CKEDITOR.instances[editorName];
		alert(oEditor.lang.powervoice.alertfilenameexist);
	}
}