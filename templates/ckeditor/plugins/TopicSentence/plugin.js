( function() {

	CKEDITOR.plugins.add( 'TopicSentence', {
		lang: 'en,zh',
		icons: 'ed_topicsentence', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.TopicSentence.title;
	        editor.addCommand( 'TopicSentence', {
	            exec: function( editor ) {
	            		var result = topic_sentence(editor);
						alert(result);				
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'TopicSentence', {
	            label: editor.lang.TopicSentence.title,
	            command: 'TopicSentence',
	            toolbar: 'TopicSentence,40'
	        });
	    }
	});

})();
function topic_sentence(objname){
	var hStr =  objname.GetXHTML( true ) ;
	var wStr = hStr.replace(/<[^>]*>/g, "");
	var tmpA = wStr.split("\n");
	var rx = "";
	var tmpS = "";
	var counter = 0;

	var CSymbolFS = "\。";
	var CSymbolEM = "\！";
	var CSymbolQM = "\？";

	if (typeof(TopicSentenceSymbolFS)!="undefined" && TopicSentenceSymbolFS!="")
	{
		CSymbolFS = TopicSentenceSymbolFS;
	}
	if (typeof(TopicSentenceSymbolEM)!="undefined" && TopicSentenceSymbolEM!="")
	{
		CSymbolEM = TopicSentenceSymbolEM;
	}
	if (typeof(TopicSentenceSymbolQM)!="undefined" && TopicSentenceSymbolQM!="")
	{
		CSymbolQM = TopicSentenceSymbolQM;
	}

	// get first & last sentences
	for (var i=0; i<tmpA.length; i++)
	{
		tmps = tmpA[i].replace(/\r\n|\n|\r/g, ' ');
		fullstop_first = tmps.indexOf("\.");
		fullstop_last = tmps.lastIndexOf("\.");
		questionmark_first = tmps.indexOf("\?");
		questionmark_last = tmps.lastIndexOf("\?");
		exclamationmark_first = tmps.indexOf("\!");
		exclamationmark_last = tmps.lastIndexOf("\!");
		cfullstop_first = tmps.indexOf(CSymbolFS);
		cfullstop_last = tmps.lastIndexOf(CSymbolFS);
		cquestionmark_first = tmps.indexOf(CSymbolQM);
		cquestionmark_last = tmps.lastIndexOf(CSymbolQM);
		cexclamationmark_first = tmps.indexOf(CSymbolEM);
		cexclamationmark_last = tmps.lastIndexOf(CSymbolEM);


		// find the first occurence of the symbol
		first_pos = [fullstop_first, questionmark_first, exclamationmark_first, cfullstop_first, cquestionmark_first, cexclamationmark_first];
		first_pos = first_pos.sort(sortNumbers);
		var fpos = -1;
		for (var j=0; j<first_pos.length; j++)
		{
			if (first_pos[j]>1)
			{
				fpos = first_pos[j];
				break;
			}
		}

		// find the second last occurence of the symbol
		last_pos = [fullstop_last, questionmark_last, exclamationmark_last, cfullstop_last, cquestionmark_last, cexclamationmark_last];
		last_pos = last_pos.sort(sortNumbers);

		var lpos = -1;
		if (last_pos[5]>1)
		{
			var startAt = last_pos[5]-1;
			fullstop_last = tmps.lastIndexOf("\.", startAt);
			questionmark_last = tmps.lastIndexOf("\?", startAt);
			exclamationmark_last = tmps.lastIndexOf("\!", startAt);
			cfullstop_last = tmps.lastIndexOf("\。", startAt);
			cquestionmark_last = tmps.lastIndexOf("\？", startAt);
			cexclamationmark_last = tmps.lastIndexOf("\！", startAt);
			last_pos = [fullstop_last, questionmark_last, exclamationmark_last, cfullstop_last, cquestionmark_last, cexclamationmark_last];
			last_pos = last_pos.sort(sortNumbers);

			for (var j=last_pos.length-1; j>=0; j--)
			{
				if (last_pos[j]>=fpos)
				{
					lpos = last_pos[j];
					break;
				}
			}
		}

		// put to final output
		var isLast = (lpos>=fpos);
		if (fpos>1)
		{
			counter ++;
			rx += counter + (isLast ? ".1" : "") + " " + tmps.substr(0, fpos+1)+"\n";
		}
		if (lpos>1)
		{
			rx += counter + ".2 " + tmps.substr(lpos+1, tmps.length-lpos)+"\n";
		}
	}

	return rx;
}
function sortNumbers(a, b) { return a - b}