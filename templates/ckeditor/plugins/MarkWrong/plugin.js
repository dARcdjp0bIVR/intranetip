( function() {

	CKEDITOR.plugins.add( 'MarkWrong', {
		lang: 'en,zh',
		icons: 'ed_incorrect', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.MarkWrong.title;
	        editor.addCommand( 'MarkWrong', {
	            exec: function( editor ) {
	            		var eclassVersion = window.parent.location.pathname.split('/')[1];			
						var html = "<img src='"+window.parent.location.origin+"/"+eclassVersion+"/images/html_editor/markwrong.gif' border='0' align='absmiddle'>";
						editor.InsertHtml(parent.Trim(html));					
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'MarkWrong', {
	            label: editor.lang.MarkWrong.title,
	            command: 'MarkWrong',
	            toolbar: 'MarkWrong,40'
	        });
	    }
	});

})();