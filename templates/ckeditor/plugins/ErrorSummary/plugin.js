( function() {

	CKEDITOR.plugins.add( 'ErrorSummary', {
		lang: 'en,zh',
		icons: 'ed_errorsummary', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.ErrorSummary.title;
	        editor.addCommand( 'ErrorSummary', {
	            exec: function( editor ) {
	            		var result = error_summary(editor);
						alert(result);			
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'ErrorSummary', {
	            label: editor.lang.ErrorSummary.title,
	            command: 'ErrorSummary',
	            toolbar: 'ErrorSummary,40'
	        });
	    }
	});

})();

function error_summary(objname){
	var JSMarkingCode = parent.JSMarkingCode;
	if (typeof(JSMarkingCode)=="undefined")
	{
		return;
	}
	var hStr =  objname.GetXHTML( true ) ;
	var wStr = hStr.replace(/<[^>]*>/g, "");
	wStr = wStr.replace(/\r\n|\n|\r/g, " ");
	var rx = "";
	var ry = "";
	var tmp = null;
	var total = 0;

	for (var i=0; i<JSMarkingCode.length; i++)
	{
		// get
		tmp = wStr.split("["+JSMarkingCode[i]+"]");
		total = tmp.length - 1;
		total = (parseInt(total)>0) ? total : 0;
		ry += (total!=0) ? JSMarkingCode[i] + "\t:\t" + total + "\n" : "";
	}
	rx += (ry!="") ? ry : "No error found!";

	return rx;
}