( function() {

	CKEDITOR.plugins.add( 'MarkCorrect', {
		lang: 'en,zh',
		icons: 'ed_correct', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.MarkCorrect.title;
	        editor.addCommand( 'MarkCorrect', {
	            exec: function( editor ) {
	            		var eclassVersion = window.parent.location.pathname.split('/')[1];			
						var html = "<img src='"+window.parent.location.origin+"/"+eclassVersion+"/images/html_editor/markcorrect.gif' border='0' align='absmiddle'>";
						editor.InsertHtml(Trim(html));					
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'MarkCorrect', {
	            label: editor.lang.MarkCorrect.title,
	            command: 'MarkCorrect',
	            toolbar: 'MarkCorrect,40'
	        });
	    }
	});

})();
