( function() {

	CKEDITOR.plugins.add( 'MarkPartRight', {
		lang: 'en,zh',
		icons: 'ed_partlycorrect', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.MarkPartRight.title;
	        editor.addCommand( 'MarkPartRight', {
	            exec: function( editor ) {
	            		var eclassVersion = window.parent.location.pathname.split('/')[1];			
						var html = "<img src='"+window.parent.location.origin+"/"+eclassVersion+"/images/html_editor/markpartright.gif' border='0' align='absmiddle'>";
						editor.InsertHtml(parent.Trim(html));					
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'MarkPartRight', {
	            label: editor.lang.MarkPartRight.title,
	            command: 'MarkPartRight',
	            toolbar: 'MarkPartRight,40'
	        });
	    }
	});

})();

