/*
 * For FCKeditor 2.3
 * 
 * 
 * File Name: en.js
 * 	English language file for plugin "FlippedChannels".
 * 
 * File Authors:
 * 		Broadlearning Education (Asia) Limited
 */
CKEDITOR.plugins.setLang('InsertFlippedChannels','en', {
    title: 'Insert Flipped Channels'
} );
