( function() {

	CKEDITOR.plugins.add( 'Appreciate', {
		lang: 'en,zh',
		icons: 'ed_appreciate', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.Appreciate.title;
	        editor.addCommand( 'Appreciate', {
	            exec: function( editor ) {
	            		var eclassVersion = window.parent.location.pathname.split('/')[1];			
						var html = "<img src='"+window.parent.location.origin+"/"+eclassVersion+"/images/html_editor/appreciate.gif' border='0' align='absmiddle'>";
						editor.InsertHtml(parent.Trim(html));					
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'Appreciate', {
	            label: editor.lang.Appreciate.title,
	            command: 'Appreciate',
	            toolbar: 'Appreciate,40'
	        });
	    }
	});

})();