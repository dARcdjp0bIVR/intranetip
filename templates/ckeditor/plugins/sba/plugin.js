CKEDITOR.plugins.add( 'sba', {
	icons: null,
	lang: 'en,zh,zh-cn',
    init: function( editor ) {
    	this.Name = window.parent.lang_insertSbaElement;
        editor.addCommand( 'SbaCommand', {
            exec: function( editor ) {
                  window.parent.loadInsertMenu(editor.name); 
            }
        });
        editor.ui.addButton( 'sba', {
            label: editor.lang.sba.title,
            command: 'SbaCommand',
            toolbar: 'sba,100'
        });
    }
});