( function() {

	CKEDITOR.plugins.add( 'Missing', {
		lang: 'en,zh',
		icons: 'ed_insertmissing', // %REMOVE_LINE_CORE%
		hidpi: false, 
	    init: function( editor ) {
	    	this.Name = editor.lang.Missing.title;
	        editor.addCommand( 'Missing', {
	            exec: function( editor ) {			
						var html = "<sub><font size='5' color='red'>^</font></sub><sup><font size='3' color='red'>( )</font></sup>&nbsp;";
						editor.InsertHtml(parent.Trim(html));						
	            },
	            GetState: function(){
	            	return CKEDITOR.TRISTATE_OFF;
	            }
	        });
	        editor.ui.addButton( 'Missing', {
	            label: editor.lang.Missing.title,
	            command: 'Missing',
	            toolbar: 'Missing,40'
	        });
	    }
	});

})();