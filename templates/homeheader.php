
<?php
include_once ("$intranet_root/includes/libaccess.php");
include_once ("$intranet_root/includes/libdb.php");
include_once ("$intranet_root/includes/libuser.php");
include_once ("$intranet_root/includes/libhomework.php");
include_once ("$intranet_root/includes/libwebmail.php");
include_once ("$intranet_root/includes/libsurvey.php");
include_once ("$intranet_root/includes/libteaching.php");
include_once ("$intranet_root/includes/libsystemaccess.php");
include_once ("$intranet_root/includes/libnotice.php");
include_once ("$intranet_root/includes/libeclass.php");
include_once ("$intranet_root/includes/libadminjob.php");
include_once ("$intranet_root/includes/libcircular.php");
include_once ("$intranet_root/includes/libstudentprofile.php");

intranet_opendb();

$default_css = ($header_default_style!="") ? $header_default_style : "/templates/style.css";
?>

<META http-equiv=Content-Type content='text/html; charset=utf-8'>


<SCRIPT LANGUAGE=Javascript>
<!--
// slide tool
var slide_ie4 = (document.all) ? true : false;
var slide_ns4 = (document.layers) ? true : false;
var slide_ns6 = (document.getElementById && !document.all) ? true : false;
var pa = "parent";
var sliding = {MyTools:false}
var hide = "hidden";
var show = "visible";

function movelayer(lay,h){
        if (slide_ie4) {heig=document.all[lay].style.left;}
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left;}
        ih = parseInt(heig) + h;
        if (slide_ie4) {document.all[lay].style.left=ih+'px';}
        if (slide_ns4) {document.layers[pa].document.layers[lay].left = ih;}
        if (slide_ns6) {document.getElementById([lay]).style.left=ih+'px';}
}


function slide(lay, h, speed, up) {
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                it=4;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; it=6;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left; it=10;}
        ih = parseInt(heig);

        if (up == 0){
                if (ih != h)
                        movelayer(lay, it);
                if (ih < h) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",0)",speed);
                } else
                        setSliding(lay, 0);
        } else {
                if (ih != 25)
                        movelayer(lay, -it);
                if (ih > 25) {
                        setSliding(lay, 1);
                        setTimeout("slide('"+lay+"',"+h+","+speed+",1)",speed);
                } else
                        setSliding(lay, 0);
        }
}


function setSliding(lay, on_off) {
        is_sliding = "sliding." + lay;
        is_sliding += (on_off==0) ? "=false" : "=true";
        eval (is_sliding);
}


function getSliding(lay) {
        is_sliding = "sliding." + lay;
        return eval(is_sliding);
}


function move(lay,h){
        if (getSliding(lay))
                return;
        if (slide_ie4) {
                heig=document.all[lay].style.left;
                speed=1;
        }
        if (slide_ns4) {heig=document.layers[pa].document.layers[lay].left; speed=1;}
        if (slide_ns6) {heig=document.getElementById([lay]).style.left;speed=1;}
        ih = parseInt(heig);
        if (ih<=25)
                slide(lay,h,speed,0);
        else if (ih>=h)
                slide(lay,h,speed,1);
}

function openFileWindow()
{
         newWindow('/home/profile/cabinet/',10);
}
function open_readingrm()
{
         newWindow('/home/eclass/readingrm.php',8);
}
// --->
</SCRIPT>
<html>
<head><title><?php echo $Lang['Header']['HomeTitle']; ?></title></head>
<link rel=stylesheet href="/templates/style.css" />
<script language="Javascript" src="/templates/tooltip.js"></script>
<script language="JavaScript" src="/lang/script.<?php echo $intranet_session_language; ?>.js"></script>
<script language="JavaScript1.2" src="/templates/script.js"></script>
<script LANGUAGE="Javascript1.2">
function support()
{
         newWindow('<?=$cs_path?>',10);
}
function intranet_PreloadImages()
{
         <?php
         $preload_images[] = "$image_path/header/moving/ma_studentid_o.gif";
         $preload_images[] = "$image_path/header/moving/ma_schoolrecord_o.gif";
         $preload_images[] = "$image_path/header/moving/ma_sls_o.gif";
         $preload_images[] = "$image_path/header/moving/ma_participation_o.gif";
         $preload_images[] = "$image_path/header/moving/ma_campusmail_o.gif";
         $preload_images[] = "$image_path/header/moving/ma_webmail_o.gif";
         $preload_images[] = "$image_path/header/moving/ma_personalprofile_o.gif";
         $preload_list = "'".implode("','",$preload_images)."'";
         ?>
         MM_preloadImages(<?=$preload_list?>);
}
</script>
<!--<script language=javascript1.2 src=/templates/menu/style.js></script>-->
<!--<script language=javascript1.2 src=/templates/menu/menu.js></script>-->