<?php
// modifying : 
################# Change Log [Start] #####
#
#	Date	:	2015-12-14	Omas
# 			$HTTP_REFERER is replaced by $_SERVER['HTTP_REFERER'] for php5.4
#
################## Change Log [End] ######

	$PATH_WRT_ROOT = "../";
	include_once($PATH_WRT_ROOT."includes/global.php");

	if (!$_GET["file"]) {
		header ("Location: /");
		exit();
	}
	
	$filename = basename($_GET["file"]);
	
	$ref = $_SERVER['HTTP_REFERER'];
	$url_format = parse_url($ref);
	
	if (!isset($path) || $path=='')
		$path = $PATH_WRT_ROOT.str_replace(basename($url_format["path"]), "", $url_format["path"]).$filename;
	else
		$path = $path.$filename;
	
	$parts = explode(".", $filename);
	if (is_array($parts) && count($parts) > 1)
	    $extension = strtoupper(end($parts));
	
	# Only allow plain text files on the server to be read
	$allow_ext = array("CSV", "TXT", "LOG");
	
	$handle = @fopen($path,"r");
	
	if ($handle && in_array($extension, $allow_ext)) {
		$data = fread($handle, filesize($path));
		
		header('Pragma: public');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	
		header('Content-type: application/octet-stream');
		header('Content-Length: '.strlen($data));
	
		header('Content-Disposition: attachment; filename="'.$filename.'";');
	
		print $data;
	} else {

		header ("Location: /");
		exit();
	}
?>