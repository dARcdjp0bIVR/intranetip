var formAllFilled = true;

// define a Answer Sheet class
function Answersheet()
{
	if (typeof(_answersheet_prototype_called) == 'undefined')
	{
		_answersheet_prototype_called = true;
		Answersheet.prototype.qString = null;
		Answersheet.prototype.aString = null;
		Answersheet.prototype.counter = 0;
		Answersheet.prototype.counter_scale = 0;
		Answersheet.prototype.answer = new Array();
		Answersheet.prototype.valueTmp = new Array();
		Answersheet.prototype.scaleTmp = new Array();
		Answersheet.prototype.templates = new Array();
		Answersheet.prototype.selects = new Array();
		Answersheet.prototype.sheetArr = sheetArr;
		Answersheet.prototype.writeSheet = writeSheet;
		Answersheet.prototype.stuffAns = stuffAns;
		Answersheet.prototype.move = move;
		Answersheet.prototype.chgNum = chgNum;
		Answersheet.prototype.secDes = secDes;
		Answersheet.prototype.mode = 0;                                // 0:create; 1:for fill-in
		Answersheet.prototype.displaymode = 0;                         // 0: question and answer in separate lines; 1: in same line
		Answersheet.prototype.templateNo = 0;                        // template list menu
	}

        function writeSheet(){
                var txtArr = this.answer;
                var valueArr = this.valueTmp;
               	var scaleArr = this.scaleTmp;
                var selArr = this.selects;
                var arr=txtArr;
                var strTowrite='';
                var lastIndexVal = "8";
                
				if (this.mode==0)
                {
                        strTowrite+='<form name="editForm"><table border="0" width="95%" align="center"><tr><td><select name="sList">';
                        for (x=0; x<arr.length; x++){
                                temTxt=arr[x][0];
                                strTowrite+='<option value="">'+cutStrLen(temTxt, 50);
                        }
                        strTowrite+='</select><br>'
                        +'&nbsp; <input type=button value="&uarr;" title="'+((typeof(operator_move_up)!="undefined")?operator_move_up:'')+'" onclick="sheet.move(this.form.sList.selectedIndex, \'up\');writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&darr;" title="'+((typeof(operator_move_down)!="undefined")?operator_move_down:'')+'" onclick="sheet.move(this.form.sList.selectedIndex, \'down\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&Chi;" title="'+((typeof(operator_delete)!="undefined")?operator_delete:'')+'" onclick="sheet.move(this.form.sList.selectedIndex, \'out\'); writetolayer(\'blockInput\',sheet.writeSheet()); ">'
                        +'<input type=button value="&harr;" title="'+((typeof(operator_rename)!="undefined")?operator_rename:'')+'" onclick="sheet.secDes(this.form.sList.selectedIndex, \'out\'); "><br><hr width=100%></td></tr></table></form>';
                }

                var ansNum=1;
                var onclick_handler = (typeof(fill_in_block)!="undefined") ? 'onClick="alert(fill_in_block)"' : "";
				var submit_handler = (typeof(submit_action)!="undefined") ? submit_action : "";
				var view_only = (typeof(display_answer_form)!="undefined") ? 1 : 0;
				var table_style = (typeof(print_report)!="undefined") ? ' width="100%" ' : ' style="border-left:2px #CDCDCD solid; border-top:2px #CDCDCD solid; border-right:2px #CDCDCD solid; border-bottom:2px #CDCDCD solid; " width="95%" ';

				var circle_img = (typeof(image_path)!="undefined") ? '<img src="'+image_path+'/icon/circle.jpg" border="0" hspace="3" align="absmiddle">' : '';
				var circle_tick_img = (typeof(image_path)!="undefined") ? '<img src="'+image_path+'/icon/circle_tick.jpg" border="0" hspace="3" align="absmiddle">' : '';
				var checkbox_img = (typeof(image_path)!="undefined") ? '<img src="'+image_path+'/icon/checkbox.jpg" border="0" hspace="3" align="absmiddle">' : '';
				var checkbox_tick_img = (typeof(image_path)!="undefined") ? '<img src="'+image_path+'/icon/checkbox_tick.jpg" border="0" hspace="3" align="absmiddle">' : '';

				txtStr = strTowrite+'<Form '+ submit_handler +' name="answersheet" id="answersheet" '+ onclick_handler +'>\n';
                txtStr += '<table border="0" align="center" bgcolor="white" '+ table_style +' cellpadding="10" cellspacing="0"><tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0">\n';
                if (typeof(form_description)!="undefined")
                {
					description_html = '<p><table border="0" cellpadding="0" cellspacing="0"><tr><td>'+form_description+'</td></tr></table></p>';
                } else
                {
                		description_html = '';
                }
                if (typeof(form_reference)!="undefined")
                {
                		description_html += '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td align="right">' + form_reference + '</td></tr></table>';
                }
				if (typeof(form_user_photo)!="undefined")
				{
					description_html += '<table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td align="center">' + form_user_photo + '</td></tr></table>';
				}
                if (typeof(form_title_main)!="undefined")
                {
                		if (form_title_main!="" || description_html!="")
                		{
							txtStr += '<tr><td align="center" colspan="2" class="form_title">'+form_title_main+'<br>'+description_html;
//							txtStr += '<hr style="border:dashed 3px #999999;" >';
							txtStr += '<br/></td></tr>';
						}
                }
				
				if (this.displaymode==2)
				{
					txtStr+='<tr><td>' + txtArr[0] + '</td></tr>\n';
				}
				else
				{
					for (x=0; x<txtArr.length; x++)
					{
							tit = recurReplace(">", "&gt;", txtArr[x][0]);
							tit = recurReplace("<", "&lt;", tit);
							tit = recurReplace("\n", "<br>", tit);
							queArr=txtArr[x][1][0].split(",");
						
							if (queArr[0]==lastIndexVal)
							{
								if (x>0)
								{
									txtStr += '<tr><td colspan="2"><hr color="#E0F2BB" size=5></td></tr>';
								}
								tit = '<span class="16Green">' + tit + '</span>';
								title_align = '';
							} else
							{
								title_align = 'align="right"';
							}

							if (this.displaymode==0 || queArr[0]==lastIndexVal)
							{
								txtStr += '<tr><td colspan=2>\n';
								txtStr += tit+'</td></tr>\n';
							} 
							else if (this.displaymode==1)
							{
								  txtStr += (tit!='') ? '<tr><td valign="top" '+title_align+' >'+tit+'</td><td width="80%"><table width="100%" border="0" cellpadding="0" cellspaing="0"><tr>' : '<tr><td valign="top" colspan=2><table width="100%" border="0" cellpadding="0" cellspaing="0"><tr>';
							}

							for (y=1; y<txtArr[x].length; y++)
							{
								queArr=txtArr[x][y][0].split(",");
									if (this.displaymode==0 || queArr[0]==lastIndexVal)
								  {
										txtStr += '<tr><td>&nbsp;</td>';
										txtStr += '<td width="95%">';
								  } else if (this.displaymode==1)
								  {
										txtStr += '<td>';
								  }
								
									switch (queArr[0])
									{
											case "1":
													preValue0 = (ansNum<=this.counter) ? valueArr[ansNum-1][0] : "";
													preValue1 = (ansNum<=this.counter) ? valueArr[ansNum-1][1] : "";
													r_check0 = "";
													r_check1 = "";
													if (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined')
													{
														eval("r_check"+selArr[ansNum-1]+"='checked'");
													}
													strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 40; // total str length
													if(view_only==1)
													{
														r_check0_img = (r_check0=='checked') ? circle_tick_img : circle_img;
														r_check1_img = (r_check1=='checked') ? circle_tick_img : circle_img;
														txtStr+='<table border=0 cellpaddin="10" cellspacing="0">';
														txtStr+='<tr><td>'+ r_check0_img + preValue0 +'</td>';
														txtStr+=(this.mode==0 || strLen>=40) ? '</tr><tr>' : '<td width="25">&nbsp;</td>';
														txtStr+='<td>'+ r_check1_img + preValue1 +'</td></tr>';
														txtStr+='</table>';
													}
													else
													{
													
														txtStr+='<input type="radio" value="0" '+r_check0+' name="F'+ansNum+'">';
														txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue0+'" name="FD'+ansNum+'_0" size="80">' : preValue0;
														txtStr+=(this.mode==0 || strLen>=40) ? '<br>' : ' &nbsp; &nbsp; ';
														txtStr+='<input type="radio" value="1" '+r_check1+' name="F'+ansNum+'">';
														txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue1+'" name="FD'+ansNum+'_1" size="80">' : preValue1;
													}
													break;

											case "2":
													r_check_i = (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined') ? parseInt(selArr[ansNum-1]) : -1;

													strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
													if(view_only==1)
													{
														txtStr+='<table border=0 cellpaddin="10" cellspacing="0">';
														txtStr+='<tr>';
														for (m=0; m<txtArr[x][y][1]; m++)
														{
															preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
															r_check_img = (r_check_i==m) ? circle_tick_img : circle_img;
															txtStr+='<td>'+ r_check_img + preValue +'</td>';
															txtStr+=(strLen<60) ? '<td width="25">&nbsp;</td>' : '</tr><tr>';
														}
														txtStr+='</tr>';
														txtStr+='</table>';
													}
													else
													{
														for (m=0; m<txtArr[x][y][1]; m++)
														{
															preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
															if (r_check_i==-1)
															{
																txtStr+='<input type="radio" value="'+m+'" name="F'+ansNum+'">';
															} else
															{
																txtStr+=(r_check_i==m) ? '<input type="radio" value="'+m+'" checked name="F'+ansNum+'">' : '<input type="radio" value="'+m+'" name="F'+ansNum+'">';
															}
															txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="80"><br>' : preValue;
															if (this.mode==1)
															{
																txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
															}
														}
													}
													break;

											case "3":
													s_checkArr = null;
													strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
													if (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined') {
															s_checks = selArr[ansNum-1];
															s_checkArr = s_checks.split(",");
													}
													if(view_only==1)
													{
														txtStr+='<table border=0 cellpaddin="10" cellspacing="0">';
														txtStr+='<tr>';
														for (m=0; m<txtArr[x][y][1]; m++){
															preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
															c_check = "";
															if (s_checkArr!=null) {
																	for (var kk=0; kk<s_checkArr.length; kk++) {
																			if (s_checkArr[kk]==m) {
																					c_check = "checked";
																					break;
																			}
																	}
															}
															r_check_img = (c_check=="checked") ? checkbox_tick_img : checkbox_img;
															txtStr+='<td>'+ r_check_img + preValue +'</td>';
															txtStr+=(strLen<60) ? '<td width="25">&nbsp;</td>' : '</tr><tr>';
														}
														txtStr+='</tr>';
														txtStr+='</table>';
													}
													else
													{
														for (m=0; m<txtArr[x][y][1]; m++){
																preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
																c_check = "";
																if (s_checkArr!=null) {
																		for (var kk=0; kk<s_checkArr.length; kk++) {
																				if (s_checkArr[kk]==m) {
																						c_check = "checked";
																						break;
																				}
																		}
																}
																txtStr+= '<input type="checkbox" '+c_check+' name="F'+ansNum+'" value="'+m+'">';
																txtStr+=(this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="80"><br>' : preValue;
																if (this.mode==1) {
																	txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																		txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
																}
														}
													}
													break;

											case "4":
													t_filled = (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined') ? selArr[ansNum-1] : "";
													if(view_only==1)
													{	
														t_filled = (t_filled=="") ? "&nbsp;" : t_filled;
														txtStr+='<table width="98%" style="border-left:1px #CDCDCD solid; border-top:1px #CDCDCD solid; border-right:1px #CDCDCD solid; border-bottom:1px #CDCDCD solid; ">';
														txtStr+='<tr><td style="line-height:150%">'+t_filled+'</td></tr>';
														txtStr+='</table>';
													}
													else
													{
														txtStr+='<input type="text" name="F'+ansNum+'" size="80" value="'+t_filled+'">';
													}
													break;

											case "5":
													t_filled = (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined') ? selArr[ansNum-1] : "";
													if(view_only==1)
													{							
														t_filled = (t_filled=="") ? "&nbsp;" : t_filled;
														txtStr+='<table width="98%" style="border-left:1px #CDCDCD solid; border-top:1px #CDCDCD solid; border-right:1px #CDCDCD solid; border-bottom:1px #CDCDCD solid; ">';
														txtStr+='<tr><td style="line-height:150%">'+t_filled+'</td></tr>';
														txtStr+='</table>';
													}
													else
													{
														txtStr+='<textarea name="F'+ansNum+'" cols="80" rows="5">'+recurReplace("<br>", "\n", t_filled)+'</textarea>';
													}
													break;

											case "6":
											// likert scale
					
													s_checkArr = null;
													strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
													if (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined')
													{
															s_checks = selArr[ansNum-1];
															s_checkArr = s_checks.split(",");
													}
												
													if(false)
													{
														//view mode
														//alert(view_only);
														var myAnsArr = new Array();
														if(selArr != undefined && selArr != "")
														var myAnsArr = selArr[x].split(",");
														
														txtStr+='<table border=0 cellpaddin="10" cellspacing="0">';
														txtStr+='<tr>';
														//for (m=0; m<txtArr[x][y][1]; m++)
														{
															//preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
															
															/*
															c_check = "";
															if (s_checkArr!=null) {
																	for (var kk=0; kk<s_checkArr.length; kk++) {
																			if (s_checkArr[kk]==m) {
																					c_check = "checked";
																					break;
																			}
																	}
															}
															r_check_img = (c_check=="checked") ? checkbox_tick_img : checkbox_img;	
															txtStr+='<td>'+ r_check_img + preValue +'</td>';
															*/
															//txtStr+=(strLen<60) ? '<td width="25">&nbsp;</td>' : '</tr><tr>';
														}
														txtStr+='</tr>';
														txtStr+='</table>';
													}
													else
													{
														// edit mode
														//alert(txtArr[x][y][1]); // # of question
														//alert(txtArr[x][y][2]); // # of option
														var myAnsArr = new Array();
														
														if(selArr[x] != undefined && selArr[x] != "")
														var myAnsArr = selArr[x].split(",");
														
														tableName = "TB_" + ansNum;
														table_type = queArr[0];
														//row = parseInt(txtArr[x][y][1]) + 2;
														//col = parseInt(txtArr[x][y][2]) + 3;
														 
														txtStr += '<table border="0" align="left" cellpadding="5" cellspacing="5" id="'+tableName+'" name="'+tableName+'">';
														   
														// Header row
														txtStr += '<tr>';
														txtStr += '<td></td>';
														if (this.mode==0){
															txtStr += '<td>'+question_scale+'</td>';
														}
														else{
															txtStr += '<td></td>';
														}
														mm = parseInt(txtArr[x][y][1]);
														for (n=0; n<txtArr[x][y][2]; n++)
														{
															preScale = (ansNum<=this.counter) ? valueArr[ansNum-1][mm+n] : "";
															if(preScale == undefined) 
																preScale = "";
															if (this.mode==0)
															txtStr += '<td align="center"><input type="text" value="'+preScale+'" name="SC'+ansNum+'_'+n+'" size="10"></td>'; // scale input
															else
															txtStr += '<td align="center" nowrap>'+preScale+'</td>'; // scale input
														}
														if (this.mode==0)
														{
														txtStr += '<td>';
														txtStr += '<input type="button" name="AC'+ansNum+'" value="&nbsp;+&nbsp;" onClick="add_scale_column(\''+tableName+'\','+table_type+','+x+','+y+');" />'; // add column
														txtStr += '<input type="button" name="RC'+ansNum+'" value="&nbsp;-&nbsp;" onClick="remove_scale_column(\''+tableName+'\','+table_type+','+x+','+y+');" />'; // remove column
														txtStr += '</td>';
														}
														txtStr += '</tr>';
														
														// Question rows
														for (m=0; m<txtArr[x][y][1]; m++)
														{
																preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
																txtStr += '<tr>';
																txtStr += '<td align="left">'+ (m + 1) + '.</td>';
																txtStr += '<td>';
																txtStr += (this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="10"><br>' : preValue;
																if (this.mode==1) 
																{
																	txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																		txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
																}
																txtStr += '</td>';
																
																for (n=0; n<txtArr[x][y][2]; n++)
																{
																	if(selArr != undefined && selArr != "")
																	{
																		if(myAnsArr[m] == n)
																		{
																			txtStr += '<td align="center"><input type="radio" name="RS'+ ansNum + '_' + m + '" checked /></td>';
																		}
																		else
																		{
																			txtStr += '<td align="center"><input type="radio" name="RS'+ ansNum + '_' + m + '" /></td>';
																		}
																	}
																	else
																	{
																		txtStr += '<td align="center"><input type="radio" name="RS'+ ansNum + '_' + m + '" /></td>';
																	}
																}
																
																if (this.mode==0)
																txtStr += '<td><input type="button" value="&nbsp;&nbsp;X&nbsp;&nbsp;" name="RM'+ ansNum + '_' + m + '" onClick="remove_sub_question(\''+tableName+'\','+m+','+table_type+','+x+','+y+');" /></td>';
																
																txtStr += '</tr>';
	
														} // end for
														
														if (this.mode==0)
														{
																// row for add button
																txtStr += '<tr>';
																txtStr += '<td align="left"><input name="AR'+ansNum+'" type="button" value="&nbsp;&nbsp;+&nbsp;&nbsp;" onClick="add_sub_question(\''+tableName+'\','+table_type+','+null+','+null+','+x+','+y+');" /></td>'; // add row
																txtStr += '<td>&nbsp;</td>';
																for (n=0; n<txtArr[x][y][2]; n++)
																{
																	txtStr += '<td>&nbsp;</td>';
																}
																txtStr += '<td>&nbsp;</td>';
																txtStr += '</tr>';
														}
														
														txtStr += '</table>';
													}
													
													break;
													
											case "7":
											// table like format
													s_checkArr = null;
													strLen = (ansNum<=this.counter) ? getStrLen(valueArr[ansNum-1]) : 60; // total str length
													if (selArr.length>0 && typeof(selArr[ansNum-1])!='undefined')
													{
															s_checks = selArr[ansNum-1];
															s_checkArr = s_checks.split(",");
													}
													
													//if(view_only==1)
													if(false)
													{
														// view mode
														//alert(view_only);
														txtStr+='<table border=0 cellpaddin="10" cellspacing="0">';
														txtStr+='<tr>';
														//for (m=0; m<txtArr[x][y][1]; m++)
														//{
															//preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
															
															/*
															c_check = "";
															if (s_checkArr!=null) {
																	for (var kk=0; kk<s_checkArr.length; kk++) {
																			if (s_checkArr[kk]==m) {
																					c_check = "checked";
																					break;
																			}
																	}
															}
															r_check_img = (c_check=="checked") ? checkbox_tick_img : checkbox_img;	
															txtStr+='<td>'+ r_check_img + preValue +'</td>';
															*/
															//txtStr+=(strLen<60) ? '<td width="25">&nbsp;</td>' : '</tr><tr>';
														//}
														txtStr+='</tr>';
														txtStr+='</table>';
													}
													else
													{
														// edit mode
														//alert(txtArr[x][y][1]); // # of question
														//alert(txtArr[x][y][2]); // # of option
														var myAnsArr = new Array();
										
//														if(selArr != undefined && selArr != "")
														if(selArr[x] != undefined && selArr[x] != "")
														var myAnsArr = selArr[x].split(",");
														
														//alert(myAnsArr);
														if(myAnsArr[0] != undefined)
														var txtAnsArr = myAnsArr[0].split("#MQA#");
														//alert(txtAnsArr);
														
														tableName = "TB_" + ansNum;
														table_type = queArr[0];
														//row = parseInt(txtArr[x][y][1]) + 2;
														//col = parseInt(txtArr[x][y][2]) + 3;
														 
														txtStr += '<table border="0" align="left" cellpadding="5" cellspacing="5" id="'+tableName+'" name="'+tableName+'">';
														   
														// Header row
														txtStr += '<tr>';
														
														if (this.mode==0){
															txtStr += '<td>#</td>';
															txtStr += '<td>'+question_question+'</td>';
														}
														else{
															txtStr += '<td></td>';
															txtStr += '<td></td>';
														}
														mm = parseInt(txtArr[x][y][1]);
														for (n=0; n<txtArr[x][y][2]; n++)
														{
															preScale = (ansNum<=this.counter) ? valueArr[ansNum-1][mm+n] : "";
															if (this.mode==0)
															txtStr += '<td align="center"><input type="text" value="'+preScale+'" name="SC'+ansNum+'_'+n+'" size="10"></td>'; // scale input
															else
															txtStr += '<td align="center">'+preScale+'</td>'; // scale input
														}
														if (this.mode==0)
														{
														txtStr += '<td>';
														txtStr += '<input type="button" name="AC'+ansNum+'" value="&nbsp;+&nbsp;" onClick="add_scale_column(\''+tableName+'\','+table_type+','+x+','+y+');" />'; // add column
														txtStr += '<input type="button" name="RC'+ansNum+'" value="&nbsp;-&nbsp;" onClick="remove_scale_column(\''+tableName+'\','+table_type+','+x+','+y+');" />'; // remove column
														txtStr += '</td>';
														}
														txtStr += '</tr>';
														
														// Question rows
														for (m=0; m<txtArr[x][y][1]; m++)
														{
																preValue = (ansNum<=this.counter) ? valueArr[ansNum-1][m] : "";
																txtStr += '<tr>';
																txtStr += '<td align="left">'+ (m + 1) + '.</td>';
																txtStr += '<td>';
																txtStr += (this.mode==0) ? '<input type="text" value="'+preValue+'" name="FD'+ansNum+'_'+m+'" size="10"><br>' : preValue;
																if (this.mode==1) 
																{
																	txtStr+=(strLen<60) ? ' &nbsp; ' : '<br>';
																		txtStr+=(m==txtArr[x][y][1]-1 && strLen<60) ? '<br>' : '';
																}
																txtStr += '</td>';
																
																if(txtArr != undefined)
																{
																for (n=0; n<txtArr[x][y][2]; n++)
																{
//																	if(selArr != undefined && selArr != "")
																	if(selArr[x] != undefined && selArr[x] != "")
																	{
																		
																		ansIndex = m * txtArr[x][y][2] + n + 1;
																		
																		if(txtAnsArr[ansIndex] != undefined){
																			tmpAnsTxt = txtAnsArr[ansIndex];
																		}
																		else{
																			tmpAnsTxt = '';
																		}
																		
																		txtStr += '<td align="center"><input type="text" name="RS'+ ansNum + '_' + m + '_' + n + '" value="'+tmpAnsTxt+'" size="10" /></td>';
																	}
																	else
																	{
									
																		txtStr += '<td align="center"><input type="text" name="RS'+ ansNum + '_' + m + '_' + n + '" value="" size="10" /></td>';
																	}
																} // end for loop answer
																}
																
																if (this.mode==0)
																txtStr += '<td><input type="button" value="&nbsp;&nbsp;X&nbsp;&nbsp;" name="RM'+ ansNum + '_' + m + '" onClick="remove_sub_question(\''+tableName+'\','+m+','+table_type+','+x+','+y+');" /></td>';
																
																txtStr += '</tr>';
	
														} // end for
														
														if (this.mode==0)
														{
																// row for add button
																txtStr += '<tr>';
																txtStr += '<td align="left"><input name="AR'+ansNum+'" type="button" value="&nbsp;&nbsp;+&nbsp;&nbsp;" onClick="add_sub_question(\''+tableName+'\','+table_type+','+null+','+null+','+x+','+y+');" /></td>'; // add row
																txtStr += '<td>&nbsp;</td>';
																for (n=0; n<txtArr[x][y][2]; n++)
																{
																	txtStr += '<td>&nbsp;</td>';
																}
																txtStr += '<td>&nbsp;</td>';
																txtStr += '</tr>';
														}
														
														txtStr += '</table>';
													}
											
													break;	
													
											case "8":
													break;		
													
									}
									tArr=txtArr[x][y];
									if (this.displaymode==0)
									{
										if (queArr[0]!=lastIndexVal)
										{
											txtStr += (queArr[0]=="2" || queArr[0]=="3") ? '<br></td>' : '<br><br></td>';
										}
									} else if (this.displaymode==1)
									{
										txtStr += '</td>';
									}
									 ansNum++;
							}
							if (this.displaymode==1 && queArr[0]!=lastIndexVal)
							{
								txtStr += '</tr></table></td></tr>';
							}
					}
				}
                //txtStr+='</td></tr></table></td></tr></table>\n';
				txtStr+='</table></td></tr></table>\n';
                this.counter = ansNum-1;
                //txtStr+='<p align=center><input type="button" onClick="finish();" value="'+button_submit+'"> ';
                txtStr+='</Form>\n';
                
               //alert(txtStr);

                return txtStr;
        }



		/* internal function for conversion and operations */
        function pushUp(arr){
                for (x=0; x<arr.length-1; x++){
                        arr[x]=arr[x+1];
                }
                arr.length=arr.length-1;
                return arr;
        }


        //initialization
        function sheetArr(){
                var txtStr = this.qString;
                var txtArr = txtStr.split("#QUE#");
                var tmpArr = null;
                var resultArr = new Array();
                var valueTemp = new Array();
				
                txtArr = pushUp(txtArr);
                
                //alert(txtArr);
                
                this.counter = txtArr.length;
				
				if(this.displaymode==2)
				{
					resultArr[0] = this.qString;
				}
				else
				{
					for (var x=0; x<txtArr.length; x++)
					{
							tmpArr = txtArr[x].split("||");
							type_no = tmpArr[0].split(",");
							question = tmpArr[1];
							opts = tmpArr[2];
						
							var j = x;

							// question
							resultArr[j] = new Array();
							resultArr[j][0] = recurReplace("<br>", "\n", question);
							resultArr[j][1] = new Array();
							resultArr[j][1][0] = type_no[0];
							if (type_no.length>1)
							{
								resultArr[j][1][1] = type_no[1];
								resultArr[j][1][2] = type_no[2];
								
							}

							// option values
							valueTemp[j] = new Array();
							tmpArr = opts.split("#OPT#");
							if (tmpArr.length>1) {
									for (var m=1; m<tmpArr.length; m++){
											valueTemp[j][m-1] = tmpArr[m];
									}
							}
					}

					if (this.mode==1 && this.aString!=null) {
						this.stuffAns();
					}
				}

                this.valueTmp = valueTemp;
                return resultArr;
          }


        function stuffAns(){
                var ansStr=this.aString;
                ansArr=ansStr.split("#ANS#");
                this.selects = pushUp(ansArr);
          }


        function move(i, dir){
	        	
                var arr = this.answer;
                var temArr = new Array();

                retainValues();
                var valueArr = this.valueTmp;
                var tvArr = new Array();

                switch (dir)
                {
                        case "up":
                                if (i!=0 && i<arr.length){
                                        //swap question
                                        temArr = arr[i];
                                        arr[i] = arr[i-1];
                                        arr[i-1] = temArr;

                                        //swap current fill-in-value
                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i-1];
                                        valueArr[i-1] = temArr;
                                };
                                break;
                        case "down":
                                if (i!=(arr.length-1)&&i>=0){
                                        temArr = arr[i];
                                        arr[i] = arr[i+1];
                                        arr[i+1] = temArr;

                                        temArr = valueArr[i];
                                        valueArr[i] = valueArr[i+1];
                                        valueArr[i+1] = temArr;
                                };
                                break;
                        case "out":
                                for (x=i; x<arr.length; x++) {
                                        arr[x]=arr[x+1];
                                        valueArr[x]=valueArr[x+1];
                                }
                                arr.length = arr.length-1;
                                valueArr.length = valueArr.length-1;
                                break;
                }
                this.answer = arr;
                this.valueTmp = valueArr;
        }


        function secDes(i){
                var arr=this.answer;
                var form_pop_up = window.open("", "form_pop_up", "toolbar=no,location=no,status=no,menubar=no,resizable,width=400,height=200,top=100,left=100");

                var JSfunction = "<script language='javascript'>";
                JSfunction += "function updateDesc(ind, fobj) {";
                JSfunction += "var arrD = window.opener.sheet.answer;";
                JSfunction += "arrD[ind][0] = fobj.new_desc.value;";
                JSfunction += "window.opener.sheet.answer = arrD;";
                JSfunction += "window.opener.retainValues();";
                JSfunction += "window.opener.writetolayer('blockInput',window.opener.sheet.writeSheet());";
                JSfunction += "self.close();";
                JSfunction += "} </script>";

                var desc_form = "<HTML><head><title>"+chg_title+"</title></head><body bgcolor='#EFECE7'><form name='form1'>";
                desc_form += "<br>"+JSfunction+"<table border='0' align='center'>";
                desc_form += "<tr><td><textarea name='new_desc' rows='5' cols='40'>"+arr[i][0]+"</textarea></td></tr>";
                desc_form += "<tr><td align='right'><input type='button' onClick='updateDesc("+i+", this.form);' value='"+button_update+"'> <input type='button' onClick='self.close()' value='"+button_cancel+"'></td></tr>";
                desc_form += "</table></form></body></HTML>";
                form_pop_up.document.write(desc_form);

                return;
        }


        function chgNum(i)
        {
                arr=this.answer;
                num=arr[i].length-1;
                var changeNum = prompt ("Change the number of questions from "+num+" to:","");
                if (changeNum)
                {
                    if (isInteger(changeNum) && changeNum>0)
                    {
                            if (changeNum>num)
                            {
                                    for (n=num; n<changeNum; n++)
                                             arr[i][arr[i].length]=arr[i][arr[i].length-1];
                            }
                            if (changeNum<num)
                            {
                                    diff=num-changeNum;
                                    for (n=0; n<diff; n++)
                                        arr[i].length--;
                            }
                     }
                     else
                             alert("your input is not a valid number");
        		}
        }
        

} // end object AnswerSheet
/****************************************************************************************/
/****************************************************************************************/
/****************************************************************************************/

function editPanel(){
        answer = '<body bgcolor=transparent>';
        if (sheet.mode==0) {
                answer += '<table width="95%" align="center"><tr><td><DIV ID="blockDiv"'
                +'STYLE="background-color:#EFECE7; background-image: url(\''+background_image+'\');'
                +'width:480; visibility:visible; border-top:2px #CCBBBB dashed; border-left:2px #CCBBBB dashed; border-bottom:2px #CCBBBB dashed; border-right:2px #CCBBBB dashed;">\n'
                +'<table align=center><form name="addForm">\n'
                +'<tr><td class="smhd" align=center>~ '+answer_sheet+' ~</td></tr>\n'
                +'<tr><td class="gehd">'+getTemplate()+'</td></tr>\n'
                +'<tr><td class="gehd">\n'
                +'        '+answersheet_header+':<br>\n'
                +'        <textarea name="secDesc"  rows=3 cols=50></textarea><br>\n'
                +'        <select name="qType" onchange="if (this.selectedIndex==2||this.selectedIndex==3) {this.form.oNum.selectedIndex=3;this.form.qNum.selectedIndex=0;} else if(this.selectedIndex==6||this.selectedIndex==7){this.form.oNum.selectedIndex=3;this.form.qNum.selectedIndex=3;}else{this.form.oNum.selectedIndex=0;this.form.qNum.selectedIndex=0;}">\n'
                +'                <option> &nbsp; - '+answersheet_type+' - &nbsp; </option>\n'
                +'                <option>'+answersheet_tf+'\n'
                +'                <option>'+answersheet_mc+'\n'
                +'                <option>'+answersheet_mo+'\n'
                +'                <option>'+answersheet_sq1+'\n'
                +'                <option>'+answersheet_sq2+'\n'
                +'                <option>'+answersheet_likert+'\n'
                +'                <option>'+answersheet_table+'\n'
                +'                <option>'+answersheet_not_applicable+'\n'
                +'                </select>\n'
                +'        <select name="qNum" onchange="if (this.form.qType.selectedIndex!=6&&this.form.qType.selectedIndex!=7){alert(\''+no_questions_for+'\'); this.selectedIndex=0;}">'
                +'                <option> &nbsp; - '+answersheet_question+' - &nbsp; </option>\n'
                +'                <option value=1>1</option>\n'
                +'                <option value=2>2\n'
                +'                <option value=3>3\n'
                +'                <option value=4>4\n'
                +'                <option value=5>5\n'
                +'                <option value=6>6\n'
                +'                <option value=7>7\n' 
                +'                <option value=8>8\n'
                +'                <option value=9>9\n'
                +'                <option value=10>10\n'
                +'  	</select>\n'
                +'        <select name="oNum" onchange="if (this.form.qType.selectedIndex!=6&&this.form.qType.selectedIndex!=7&&this.form.qType.selectedIndex!=2&&this.form.qType.selectedIndex!=3){alert(\''+no_options_for+'\'); this.selectedIndex=0;}">'
                +'                <option> &nbsp; - '+answersheet_option+' - &nbsp; </option>\n'
                +'                <option value=3>3</option>\n'
                +'                <option value=4>4\n'
                +'                <option value=5>5\n'
                +'                <option value=6>6\n'
                +'                <option value=7>7\n'
                +'                <option value=8>8\n'
                +'                <option value=9>9\n'
                +'                <option value=10>10\n'
                +'                <option value=11>11\n'
                +'                <option value=12>12\n'
                +'                <option value=13>13\n'
                +'                <option value=14>14\n'
                +'                <option value=15>15\n'
                +'                <option value=16 class="extraONum" style="display:none;">16\n'
                +'                <option value=17 class="extraONum" style="display:none;">17\n'
                +'                <option value=18 class="extraONum" style="display:none;">18\n'
                +'                <option value=19 class="extraONum" style="display:none;">19\n'
                +'                <option value=20 class="extraONum" style="display:none;">20\n'
                +'                <option value=21 class="extraONum" style="display:none;">21\n'
                +'                <option value=22 class="extraONum" style="display:none;">22\n'
                +'                <option value=23 class="extraONum" style="display:none;">23\n'
                +'                <option value=24 class="extraONum" style="display:none;">24\n'
                +'                <option value=25 class="extraONum" style="display:none;">25\n'
                +'  </select><br>\n'
				+'        <input type="button" onClick="retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.secDesc.value=\'\'; this.form.secDesc.focus();}" value="'+button_add+'">\n'
                +'        </td></tr>\n'
                +'        </form></table>\n'
                +'</DIV></td></tr></table>\n';
        }

        answer += '<DIV ID="blockInput"'
        +'        STYLE="background-color: transparent; '
        +'                width:100%; visibility:visible;">\n'
        +'<script'
        +' language="javascript">\n'
        +' document.write(sheet.writeSheet());\n'
        +'</script'
        +'>\n'
        +'</DIV>\n';

        return answer;
}



function getTemplate() {
	var tmpArr = sheet.templates;
	var xStr = '';
	if (tmpArr.length>0)
	{
		xStr += '<select name="fTemplate" onchange="if (confirm(chg_template)) {changeTemplate(this.form.fTemplate.selectedIndex); this.form.secDesc.focus();} else {this.form.fTemplate.selectedIndex=sheet.templateNo;}">';

		xStr += '<option value=""> &nbsp; - '+answersheet_template+' - &nbsp; </option>\n';
		for (var i=0; i<tmpArr.length; i++) {
			xStr += '<option value="'+i+'">'+tmpArr[i][0]+'</option>\n';
		}
		xStr += '</select>';
	}

	return xStr;
}


function changeTemplate(index) {
	if (index!=0)
	{
		sheet.qString = sheet.templates[index-1][1];
		sheet.answer = sheet.sheetArr();
	} else
	{
		sheet.qString = "";
		sheet.answer = "";
	}
	sheet.templateNo = index;
	writetolayer("blockInput",sheet.writeSheet());
}


function appendTxt(formName){
        var formObj=eval("document."+formName);
        var arr=new Array();
        arr[0]=formObj.secDesc.value;

        qtype=String(formObj.qType.selectedIndex);
        if (qtype==0){
                alert(pls_specify_type);
                formObj.qType.focus();
                return false;
        }

        for (x=0; x<1; x++){
                y=x+1;
                arr[y]=new Array();
                arr[y][0]=qtype;
                if (qtype=="2" ||qtype=="3")
                        arr[y][1]=String(formObj.oNum.options[formObj.oNum.selectedIndex].value);
                
                if (qtype=="6" || qtype=="7")
                {
                        arr[y][1]=String(formObj.qNum.options[formObj.qNum.selectedIndex].value);
                        arr[y][2]=String(formObj.oNum.options[formObj.oNum.selectedIndex].value);
                }
            
        }
        sheet.answer[sheet.answer.length]=arr;

        return true;
}

if (need2checkform==undefined)
{
    var need2checkform = false;
}

function getAns(i, table_type, no_row, no_col){
	
	var count = 1;
	
	if(table_type == 6 || table_type == 7)
	{
		var ansObj = "document.answersheet.RS"+i+"_";
		count = parseInt(no_row);
	}
	else
	{
        var eleObj = eval("document.answersheet.F"+i);
    }
        
    var strAns = null;
    for(var t = 0; t < count; t++)
    {
	    if(table_type == 6)
	    var eleObj = eval(ansObj+t);
	    
	    if(table_type == 7)
	    var eleObj = eval(ansObj+t+'_0');
	    
        var tempAns = null;

        if (eleObj==undefined)
        {
            return false;
        }
        

        if (eleObj.length || table_type == 7)
        {
	        if(table_type == 7)
	        {
		        var switchVal = eleObj.type;
	        }
	        else
	        {
		         var switchVal = eleObj[0].type;
	        }
	        
                switch(switchVal)
                {
                        case "radio":
                                for (p=0; p<eleObj.length; p++)
                                {
                                        if (eleObj[p].checked)
                                        {
                                                tempAns += p;
                                        }
                                }
                                if (need2checkform && tempAns == null )
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                if (tempAns != null)
                                {
	                                if(table_type == 6)
	                                {
		                                if(strAns != null)
	                                	strAns += "," + tempAns;
	                                	else
	                                	strAns += tempAns;
                                	}
	                                else
	                                {
                                    	strAns += tempAns;
                                	}
                                }
                                break;

                        case "checkbox":
                                for (p=0; p<eleObj.length; p++)
                                {
                                        if (eleObj[p].checked)
                                        {
                                            if (tempAns != null)
                                            {
                                                tempAns += ","+p;
                                            }
                                            else
                                            {
                                                tempAns = p;
                                            }
                                        }
                                }
                                if (need2checkform && tempAns == null)
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                strAns = tempAns;
                                break;

                        case "text":
                        	if(table_type == 7)
                        	{
	                        	//alert(t);
	                        	for(cc = 0; cc < no_col; cc++)
	                        	{
	                        		var tmpAnsObjName = "document.answersheet.RS"+i+"_"+t+"_"+cc;
	                        		var tmpAnsObj = eval(tmpAnsObjName);
	                        		if(tmpAnsObj != undefined)
	                        		{
		                        		tempAns = tmpAnsObj.value;
		                        		if(strAns == null)
		                        		strAns = "#MQA#" + tempAns;
		                        		else
		                        		strAns += "#MQA#" + tempAns;
	                        		} 
                        		}
                        	}
                        	else
                       		{
                                for (p=0; p<eleObj.length; p++)
                                {		
                                    tempAns += recurReplace('"', '&quot;', eleObj.value);
                                }
                                if (need2checkform && (tempAns == null || tempAns==''))
                                {
                                    formAllFilled = false;
                                    return false;
                                }
                                strAns += tempAns;
                            }
                                break;
                } // end switch
        } 
        else
        {
                tempAns = recurReplace('"', '&quot;', eleObj.value);
                if (need2checkform && (tempAns == null || tempAns==''))
                {
                    formAllFilled = false;
                    return false;
                }
                strAns = tempAns;
        }
    } // end for
        return strAns;
}


function finish(){
        var txtStr="";
        var ansStr="";
        var arr=sheet.answer;
        var ansNum=1;
        var temp = null;
        formAllFilled = true;
		
        if (sheet.mode==0) {
                // get questions
                for (var x=0; x<arr.length; x++){
                        //txtStr+="#QUE#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //append options
                                queArr = arr[x][y][0].split(",");
                                switch (queArr[0]){
                                        case "1":        myLen=2; break;
                                        case "4":
                                        case "5":
                                        case "8":        myLen=0; break;
                                        default:        myLen=arr[x][y][1]; break;
                                }
                                
                                txtStr+="#QUE#";
                                for (var z=0; z<arr[x][y].length; z++)
                                {
                                	txtStr += (z>0) ? ","+arr[x][y][z] : arr[x][y][z];
                                }
                                
                                //alert(txtStr);
                                
                                txtStr+="||"+arr[x][0]+"||";
                                
                               	if(queArr[0] == 6 || queArr[0] == 7)
	                            {
		                            
		                            var tbObj = document.getElementById("TB_"+ansNum);
									var no_row = tbObj.rows.length;
									var no_col = tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	                               
	                               	for (var m=0; m<no_row-2; m++)
	                               	{
	                               		tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
	                               		txtStr+="#OPT#"+tmpVal;
                                   	}
                                   
                                   	for (var n=0; n<no_col-3; n++)
                                   	{
	                                   tmpVal = eval("document.answersheet.SC"+ansNum+"_"+n+".value");
	                                   txtStr+="#OPT#"+tmpVal;
                                   	}
	                             }
	                             else
	                             {


                                	for (var m=0; m<myLen; m++)
                                	{
                                		optDescription = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                		txtStr+="#OPT#"+optDescription;
                                        
                                        /* check form for null input => return
                                        if (optDescription=="") {
                                                alert(pls_fill_in);
                                                eval("document.answersheet.FD"+ansNum+"_"+m+".focus()");
                                                return false;
                                        }
                                        */
                                	}
                            	}

                                ansNum++;
                        }
                }
                document.ansForm.qStr.value=txtStr;
        } 
        else if (sheet.mode==1) 
        {
			if(sheet.displaymode!=2)
			{
                // get answers
	          for (var x=0; x<arr.length; x++)
	          {
                        queArr = arr[x][1][0].split(",");
                    	//alert(arr[x][1][1] + " | " + arr[x][1][2]);
                        temp = getAns(x+1, queArr[0], arr[x][1][1], arr[x][1][2]);
                        
                        if (formAllFilled==false)
                        {
                            return false;
                        }
                        txtStr+=(queArr[0]=="8") ? "#ANS#" : "#ANS#"+temp;
                }
                document.ansForm.aStr.value=txtStr;
			}
        }
/*
        if (sheet.mode==1) {
                document.write(document.ansForm.aStr.value);
        } else {
            document.write(document.ansForm.qStr.value);
        }
*/
}


function retainValues(){
	
                var txtStr="";
                var ansStr="";
                var arr=sheet.answer;
                var ansNum=1;
                var valueTemp = new Array();

                for (var x=0; x<arr.length; x++){
                        valueTemp[x] = new Array();
                        //txtStr+="#SEC#"+arr[x][0];
                        for (var y=1; y<arr[x].length; y++){
                                //txtStr+="#QUE#";
                                /*
                                for (var z=0; z<arr[x][y].length; z++){
                                        txtStr+=arr[x][y][z]+"||";
                                }
                                */
                                //append options
                                queArr = arr[x][y][0].split(",");
                                
                                switch (queArr[0]){
                                        case "1":        myLen=2; break;
                                        case "4":
                                        case "5":
                                        case "8":        myLen=0; break;
                                        default:        myLen=arr[x][y][1]; break;
                               }
                  				
                               if(queArr[0] == 6 || queArr[0] == 7)
                               {
	                               	var tbObj = document.getElementById("TB_"+ansNum);
									var no_row = tbObj.rows.length;
									var no_col = tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	                               
	                               for (var m=0; m<no_row-2; m++)
	                               {
	                               		tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
	                               		valueTemp[x][m] = recurReplace('"', '&quot;', tmpVal);
                                   }
                                   
                                   for (var n=0; n<no_col-3; n++)
                                   {
	                                   tmpVal = eval("document.answersheet.SC"+ansNum+"_"+n+".value");
	                                   valueTemp[x][m+n] = recurReplace('"', '&quot;', tmpVal);
                                   }
                               }
                               else
                               {
                            
                                	for (var m=0; m<myLen; m++)
                                	{
                                        tmpVal = eval("document.answersheet.FD"+ansNum+"_"+m+".value");
                                        valueTemp[x][m] = recurReplace('"', '&quot;', tmpVal);
                                	}
                            	}
                                ansNum++;
                        }
                }
                sheet.valueTmp = valueTemp;
}


function recurReplace(exp, reby, txt) {
	while (txt.search(exp)!=-1)
	{
		txt = txt.replace(exp, reby);
	}
	return txt;
}




// the following three functions are added for integer validation



function isInteger(s){
	var i;
    if (isEmpty(s))
                if (isInteger.arguments.length == 1) return false;
                else return (isInteger.arguments[1] == true);
    // Search through string's characters one by one
    // until we find a non-numeric character.
    // When we do, return false; if we don't, return true.
    for (i = 0; i < s.length; i++)
    {
        // Check that current character is number.
        var c = s.charAt(i);
        if (!isDigit(c)) return false;
    }

    // All characters are numbers.
    return true;
}



function isEmpty(s)
{
	return ((s == null) || (s.length == 0))
}


function isDigit (c)
{   return ((c >= "0") && (c <= "9"))
}

function getStrLen(tmpArr) {
        var tmpV=0;
        if (tmpArr.length<7) {
                for (var i=0; i<tmpArr.length; i++) {
                        for (var j=0; j<tmpArr[i].length; j++) {
                                tmpV += (tmpArr[i].charCodeAt(j)>1000) ? 2 : 1;
                        }
                }
        } else {
                tmpV = 60;
        }

        return tmpV;
}

function cutStrLen(xStr, xLen) {
        var tmpArr = new Array(xStr);
        var tmpStr = "";
        var xCount = 0;

    if (getStrLen(tmpArr)>xLen) {
                for (var j=0; j<xStr.length; j++) {
                        xCount += (xStr.charCodeAt(j)>1000) ? 2 : 1;
                        tmpStr += xStr.charAt(j);
                        if (xCount>xLen-3) {
                            break;
                        }
                }
        xStr = tmpStr+"...";
    }
        return xStr;
}

//////////////////////////////////////////////////////////////////////////////////////////
///////////////////			Likert Scale			/////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
function get_table_data(tbObj, no_row, no_col, table_type)
{
	//alert(no_row + " | " + no_col);
	dataArr = new Array();

	//alert(tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td')[1].innerHTML);
	
	for(var y = 0; y < no_row; y++)
	{
		dataArr[y] = new Array();
		for(var x = 0; x < no_col; x++)
		{
			//alert(y + " | " + x);
			//dataArr[y][x] = tbObj.getElementsByTagName('tr')[y].getElementsByTagName('td')[x].innerHTML;
			if(y >= 1 && y < no_row - 1 && x >=2 && x < no_col - 1 && table_type == 6){
				dataArr[y][x] = tbObj.getElementsByTagName('tr')[y].getElementsByTagName('td')[x].childNodes[0].checked;
			}else if(y < no_row - 1 && x < no_col - 1){
				if(typeof tbObj.getElementsByTagName('tr')[y].getElementsByTagName('td')[x].childNodes[0] != "undefined"){
					dataArr[y][x] = tbObj.getElementsByTagName('tr')[y].getElementsByTagName('td')[x].childNodes[0].value;
				}else{
				dataArr[y][x] = "";
				}
			}else{
				dataArr[y][x] = "";
			}
		}
	}
	return dataArr;
}

function remove_sub_question(objName, r_index, table_type, answer_x, answer_y)
 {
	var tbObj = document.getElementById(objName);
	//alert("remove row: " + tbObj.name + " | " + r_index);
	r_index = r_index + 1;
	tbObj.deleteRow(r_index);
	
	var no_row = tbObj.rows.length;
	var no_col = tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	var dataArr = get_table_data(tbObj, no_row, no_col, table_type);
	
	update_question_table(tbObj, no_row, no_col, dataArr, table_type, answer_x, answer_y); // table_type: 6 - likert scale 
}

function add_scale_column(objName, table_type, answer_x, answer_y)
 {
	var tbObj = document.getElementById(objName);
	
	var no_row = tbObj.rows.length;
	var no_col = tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	var dataArr = get_table_data(tbObj, no_row, no_col, table_type);
	
	update_question_table(tbObj, no_row, no_col + 1, dataArr, table_type, answer_x, answer_y); // table_type: 6 - likert scale 
	//alert("add col: " + tbObj.name);
}

function remove_scale_column(objName, table_type, answer_x, answer_y) 
{
	var tbObj = document.getElementById(objName);
	
	var no_row = tbObj.rows.length;
	var no_col = tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	var dataArr = get_table_data(tbObj, no_row, no_col, table_type);
	
	update_question_table(tbObj, no_row, no_col - 1, dataArr, table_type, answer_x, answer_y); // table_type: 6 - likert scale 
	//alert("remove col: " + tbObj.name);
}

function add_sub_question(objName, table_type, r_number, colDataArr, answer_x, answer_y)
{
	
	//alert(colDataArr);
	var tmpArr = objName.split("_");
	var ansNum = tmpArr[1];
	var tbObj = document.getElementById(objName);
	var no_col = tbObj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	
	//alert("add: " + tbObj.name);
	
	var m = 0;
	if(r_number == null || r_number == "" || r_number == undefined)
	m = tbObj.rows.length - 2;
	else
	m = r_number - 2;
	
	
	var next_row = parseInt(m) + 1;
	//alert(next_row);
	
	var newRow = tbObj.insertRow(next_row);
	//newRow.id = newRow.uniqueID;
	
		// declare cell variable
    	var newCell;
        
        newCell = newRow.insertCell(0);
        //newCell.id = newCell.uniqueID;
        newCell.innerText = next_row +".";
        //newCell.bgColor = "white";
        
        newCell = newRow.insertCell(1);
        //newCell.id = newCell.uniqueID;
        if(colDataArr != undefined && colDataArr.length > 0)
        newCell.innerHTML = '<input type="text" name="FD'+ansNum+'_'+m+'" size="10" value="'+ colDataArr[0] +'"><br>';
        else
        newCell.innerHTML = '<input type="text" value="" name="FD'+ansNum+'_'+m+'" size="10"><br>';
        
        // -3 is that first 2 column  & last column
    for(i = 0; i < no_col - 3; i++)
    {
        newCell = newRow.insertCell(2+i);
        
        if(table_type == 6)
        {
        	newCell.align = "center";
        	newCell.innerHTML = '<input type="radio" name="RS'+ ansNum + '_' + m + '" />';
    	}
    	else if(table_type == 7)
    	{
	    	//newCell.align = "center";
        	newCell.innerHTML = '<input type="text" name="RS'+ ansNum + '_' + m + '_' + i + '" size="10" />';
    	}
    }
    
    newCell = newRow.insertCell(2+i);
    newCell.innerHTML = '<input type="button" value="&nbsp;&nbsp;X&nbsp;&nbsp;" name="RM'+ ansNum + '_' + m + '" onClick="remove_sub_question(\''+objName+'\','+m+','+table_type+','+answer_x+','+answer_y+');" />';
    
    var no_row = tbObj.rows.length;
    //alert(answer_x + " " + answer_y);
	sheet.answer[answer_x][answer_y][1] = no_row - 2;
}

function add_header_row(obj, colDataArr, table_type, answer_x, answer_y)
{
	var tmpArr = obj.getAttribute("name").split("_");
	var ansNum = tmpArr[1];
	
	var tableName = obj.getAttribute("name");
	//alert(colDataArr);
	var newRow = obj.insertRow(0);
	var newCell;
	newCell = newRow.insertCell(0);
	newCell.innerText = "#";
	
	newCell = newRow.insertCell(1);
	
	if(table_type == "6")
	newCell.innerText = question_scale;
	else if(table_type == "7")
	newCell.innerText = question_question;
	
	for(var i = 0; i < colDataArr.length; i++)
	{
		newCell = newRow.insertCell(2+i);
		newCell.innerHTML = '<input type="text" value="'+colDataArr[i]+'" name="SC'+ansNum+'_'+i+'" size="10"><br>';
	}
	
	newCell = newRow.insertCell(2+i);
	
	txtStr = '<input type="button" name="AC'+ansNum+'" value="&nbsp;+&nbsp;" onClick="add_scale_column(\''+tableName+'\','+table_type+','+answer_x+','+answer_y+');" />'; // add column
	txtStr += '<input type="button" name="RC'+ansNum+'" value="&nbsp;-&nbsp;" onClick="remove_scale_column(\''+tableName+'\','+table_type+','+answer_x+','+answer_y+');" />'; // remove column
	
	newCell.innerHTML = txtStr;
}

function add_tail_row(obj, colDataArr, table_type, answer_x, answer_y)
{
	var tmpArr = obj.getAttribute("name").split("_");
	var ansNum = tmpArr[1];
	var tableName = obj.getAttribute("name");
	var r_number = null;
	var newRow = obj.insertRow(obj.rows.length);
	var newCell;

	newCell = newRow.insertCell(0);
	txtStr = '<td align="left"><input name="AR'+ansNum+'" type="button" value="&nbsp;&nbsp;+&nbsp;&nbsp;" onClick="add_sub_question(\''+tableName+'\','+table_type+','+r_number+','+null+','+answer_x+','+answer_y+');" /></td>'; // add row
	newCell.innerHTML = txtStr;
	
	for(var i = 0; i < colDataArr.length + 2; i++)
	{
		newCell = newRow.insertCell(i+1);
		newCell.innerText = "";
	}
}

// update the table after add/remove row/col
function update_question_table(obj, row, col, dataArr, table_type, answer_x, answer_y)
{
	var str = "";
	var scaleArr = new Array();
	var questionArr = new Array();
	var answerArr = new Array();
	var s_count = 0;
	var q_count = 0;
	var a_count_x = 0;
	var a_count_y = 0;
	
	// set the # of row & col to answer array
	//alert(sheet.answer);
	//alert(answer_x + " | " + answer_y);
	// getting the value
	for(var y = 0; y < row; y++)
	{
		answerArr[a_count_y] = new Array();
		
		for(var x = 0; x < col; x++)
		{
			if(y == 0 && x >= 2 && x < col - 1)
			{
				scaleArr[s_count] = dataArr[y][x];
				s_count++;
			}
			
			if(y >= 1 && y < row - 1 && x == 1)
			{
				questionArr[q_count] = dataArr[y][x];
				q_count++;
			}
			
			if(y >= 1 && y < row - 1 && x >=2 && x < col - 1)
			{
				answerArr[a_count_y][a_count_x] = dataArr[y][x];
				a_count_x++;
			}
			//str += dataArr[y][x];
			//str += " | ";
		} // end for col
		a_count_y++;
	} // end for row
	
	
	// clear the table
  	while(obj.rows.length>0) 
    obj.deleteRow(obj.rows.length-1);
    
    // reconstruct the table
    colDataArr = new Array();
    for(var x = 2; x < col - 1; x++)
    {
	    colDataArr[x-2] = scaleArr[x-2];
    }

    add_header_row(obj, colDataArr, table_type, answer_x, answer_y);
    
   
    // -2 is to reduce the header row & the last row
    for(var y = 0; y < row - 2; y++)
    {
	    colDataArr = new Array();
	    colDataArr[0] = questionArr[y];
	    
	    for(var x = 2; x < col - 1; x++)
	    {
			colDataArr[x-1] = answerArr[y][x];
	    }
	    add_sub_question(obj.getAttribute("name"), table_type, y+2, colDataArr, answer_x, answer_y);
    }
    
    colDataArr = new Array();
    for(var x = 2; x < col - 1; x++)
    {
	    colDataArr[x-2] = "";
    }
    add_tail_row(obj, colDataArr, table_type, answer_x, answer_y);
	
	//alert(scaleArr);
	//alert(questionArr);
	//alert(answerArr);
	var row = obj.rows.length;
	var col = obj.getElementsByTagName('tr')[0].getElementsByTagName('td').length;
	
	sheet.answer[answer_x][answer_y][1] = row - 2;
	sheet.answer[answer_x][answer_y][2] = col - 3;
} // end function