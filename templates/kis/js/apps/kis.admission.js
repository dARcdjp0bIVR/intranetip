// Editing by Tommy
kis.admission = {
    application_period_edit_init: function(lang){
    	// #### Editor START #### //
    	if($('#firstPageContent').length == 0){
    		var first_page_editor = [];
    		$('textarea[id^="firstPageContent"]').each(function(){
    			var id = $(this).attr('id');
    			first_page_editor.push(kis.editor(id));
    		});
    	}else{
        	var first_page_editor = kis.editor('firstPageContent');
    	}
    	
    	if($('#lastPageContent').length == 0){
    		var last_page_editor = [];
    		$('textarea[id^="lastPageContent"]').each(function(){
    			var id = $(this).attr('id');
    			last_page_editor.push(kis.editor(id));
    		});
    	}else{
    		var last_page_editor = kis.editor('lastPageContent');
    	}
    	
    	if($('#emailContent').length == 0){
    		var email_editor = [];
    		$('textarea[id^="emailContent"]').each(function(){
    			var id = $(this).attr('id');
    			email_editor.push(kis.editor(id));
    		});
    	}else{
    		var email_editor = kis.editor('emailContent');
    	}
    	// #### Editor END #### //
    	
    	var schoolYearId = $('#schoolYearId').val();
    	
		kis.datepicker('#start_date');	
		kis.datepicker('#end_date');
		
		kis.datepicker('#preview_start_date');	
		kis.datepicker('#preview_end_date');
		
		kis.datepicker('#update_start_date');
		kis.datepicker('#update_end_date');

		kis.datepicker('#update_extra_attachment_start_date');
		kis.datepicker('#update_extra_attachment_end_date');
		
		kis.datepicker('#bday_start_date');	
		kis.datepicker('#bday_end_date');
		    	
		$('#start_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_start_date').html(warning);
		});
		$('#start_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_start_date').html(warning);
		});
		
		$('#end_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_end_date').html(warning);
		});
		$('#end_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_end_date').html(warning);
		});
		
		//checking for perview period
		$('#preview_start_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('preview_start_date','preview_start_time_hour :selected','preview_start_time_min :selected','preview_start_time_sec :selected','preview_end_date','preview_end_time_hour :selected', 'preview_end_time_min :selected','preview_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_preview_start_date').html(warning);
		});
		$('#preview_start_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('preview_start_date','preview_start_time_hour :selected','preview_start_time_min :selected','preview_start_time_sec :selected','preview_end_date','preview_end_time_hour :selected', 'preview_end_time_min :selected','preview_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_preview_start_date').html(warning);
		});
		
		$('#preview_end_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('preview_start_date','preview_start_time_hour :selected','preview_start_time_min :selected','preview_start_time_sec :selected','preview_end_date','preview_end_time_hour :selected', 'preview_end_time_min :selected','preview_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_preview_end_date').html(warning);
		});
		$('#preview_end_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('preview_start_date','preview_start_time_hour :selected','preview_start_time_min :selected','preview_start_time_sec :selected','preview_end_date','preview_end_time_hour :selected', 'preview_end_time_min :selected','preview_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_preview_end_date').html(warning);
		});
		
		//checking for update period
		$('#update_start_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('update_start_date','update_start_time_hour :selected','update_start_time_min :selected','update_start_time_sec :selected','update_end_date','update_end_time_hour :selected', 'update_end_time_min :selected','update_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_update_start_date').html(warning);
		});
		$('#update_start_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('update_start_date','update_start_time_hour :selected','update_start_time_min :selected','update_start_time_sec :selected','update_end_date','update_end_time_hour :selected', 'update_end_time_min :selected','update_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_update_start_date').html(warning);
		});
		
		$('#update_end_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('update_start_date','update_start_time_hour :selected','update_start_time_min :selected','update_start_time_sec :selected','update_end_date','update_end_time_hour :selected', 'update_end_time_min :selected','update_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_update_end_date').html(warning);
		});
		$('#update_end_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('update_start_date','update_start_time_hour :selected','update_start_time_min :selected','update_start_time_sec :selected','update_end_date','update_end_time_hour :selected', 'update_end_time_min :selected','update_end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_update_end_date').html(warning);
		});
		
		$('#bday_start_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else {
				if(this.value!=''&&$('#bday_end_date').val()!=''&&compareDate($('#bday_end_date').val(),this.value)!=1){
					warning = lang.enddateearlier;
				}
				else if(check_date_without_return_msg(document.getElementById('bday_end_date')))
					$('span#warning_bday_end_date').html("");
			}
			
			$('span#warning_bday_start_date').html(warning);
		});
		$('#bday_start_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else {
				if(this.value!=''&&$('#bday_end_date').val()!=''&&compareDate($('#bday_end_date').val(),this.value)!=1){
					warning = lang.enddateearlier;
				}
				else if(check_date_without_return_msg(document.getElementById('bday_end_date')))
					$('span#warning_bday_end_date').html("");
			}
			
			$('span#warning_bday_start_date').html(warning);
		});
		$('#bday_end_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else {
				if(this.value!=''&&$('#bday_start_date').val()!=''&&compareDate(this.value,$('#bday_start_date').val())!=1){
					warning = lang.enddateearlier;
				}
				else if(check_date_without_return_msg(document.getElementById('bday_start_date')))
					$('span#warning_bday_start_date').html("");
			}
			$('span#warning_bday_end_date').html(warning);
		});
		$('#bday_end_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else {
				if(this.value!=''&&$('#bday_start_date').val()!=''&&compareDate(this.value,$('#bday_start_date').val())!=1){
					warning = lang.enddateearlier;
				}
				else if(check_date_without_return_msg(document.getElementById('bday_start_date')))
					$('span#warning_bday_start_date').html("");
			}
			$('span#warning_bday_end_date').html(warning);
		});		
		$('#application_period_form').submit(function(){
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();
			
			var preview_start_date = $('#preview_start_date').val();
			var preview_end_date = $('#preview_end_date').val();
			
			var update_start_date = $('#update_start_date').val();
			var update_end_date = $('#update_end_date').val();
			
			var bday_start_date = $('#bday_start_date').val();
			var bday_end_date = $('#bday_end_date').val();
			
			var hasInput = false;
			if(start_date!=''&&end_date!=''){
				if(!check_date_without_return_msg(document.getElementById('start_date'))){
					$('span#warning_start_date').html(lang.invaliddateformat);
					$('#start_date').focus();
					return false;
				}else if(!check_date_without_return_msg(document.getElementById('end_date'))){
					$('span#warning_end_date').html(lang.invaliddateformat);
					$('#end_date').focus();
					return false;
				}else if(compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
					$('span#warning_start_date').html(lang.enddateearlier);
					$('#start_date').focus();
					return false;
				}
			}else{
				// miss either one of them
				if(!confirm(lang.emptyapplicationdatewarning)){
						return false;
				}
			}
			
			//checking for perview period
			if($('#preview_start_date').length > 0){
				if(preview_start_date!=''&&preview_end_date!=''){
					if(!check_date_without_return_msg(document.getElementById('preview_start_date'))){
						$('span#warning_preview_start_date').html(lang.invaliddateformat);
						$('#preview_start_date').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('preview_end_date'))){
						$('span#warning_preview_end_date').html(lang.invaliddateformat);
						$('#preview_end_date').focus();
						return false;
					}else if(compareTimeByObjId('preview_start_date','preview_start_time_hour :selected','preview_start_time_min :selected','preview_start_time_sec :selected','preview_end_date','preview_end_time_hour :selected', 'preview_end_time_min :selected','preview_end_time_sec :selected')==false){
						$('span#warning_preview_start_date').html(lang.enddateearlier);
						$('#preview_start_date').focus();
						return false;
					}
				}
			}
			
			//checking for update period
			if($('#update_start_date').length > 0){
				if(update_start_date!=''&&update_end_date!=''){
					if(!check_date_without_return_msg(document.getElementById('update_start_date'))){
						$('span#warning_update_start_date').html(lang.invaliddateformat);
						$('#update_start_date').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('update_end_date'))){
						$('span#warning_update_end_date').html(lang.invaliddateformat);
						$('#update_end_date').focus();
						return false;
					}else if(compareTimeByObjId('update_start_date','update_start_time_hour :selected','update_start_time_min :selected','update_start_time_sec :selected','update_end_date','update_end_time_hour :selected', 'update_end_time_min :selected','update_end_time_sec :selected')==false){
						$('span#warning_update_start_date').html(lang.enddateearlier);
						$('#update_start_date').focus();
						return false;
					}
				}
			}
			
			//checking for the cust (icms)
			if(document.getElementById('bday_start_date')){
				if(bday_start_date!='' || bday_end_date!=''){
					if(!check_date_without_return_msg(document.getElementById('bday_start_date'))){
						$('span#warning_bday_start_date').html(lang.invaliddateformat);
						$('#bday_start_date').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('bday_end_date'))){
						$('span#warning_bday_end_date').html(lang.invaliddateformat);
						$('#bday_end_date').focus();
						return false;
					}else if(compareDate(bday_end_date,bday_start_date)!=1){
						//$('span#warning_bday_start_date').html(lang.enddateearlier);
						$('#bday_start_date').focus();
						return false;
					}
				}
			}
			//checking for the cust (icms)
			if($('input.timeSlotSettings').length > 0){
				$('input.timeSlotSettings').each(function(){
					if(this.checked){
						hasInput = true;
					}
				});	
				if(!hasInput){
					alert(lang.selectatleastonetimeslot);
					return false;
				}
			}
			
			$('span#warning_start_date').html();
			$('span#warning_end_date').html();
			
			if($.isArray(first_page_editor)){
				$.each(first_page_editor, function(index, editor){
					editor.finish();
				});
			}else{
				first_page_editor.finish();
			}
			
			if($.isArray(last_page_editor)){
				$.each(last_page_editor, function(index, editor){
					editor.finish();
				});
			}else{
				last_page_editor.finish();
			}
			
			if($.isArray(email_editor)){
				$.each(email_editor, function(index, editor){
					editor.finish();
				});
			}else{
				email_editor.finish();
			}
			
			$.post('apps/admission/ajax.php?action=updateApplicationPeriod', $(this).serialize(), function(success){
				$.address.value('/apps/admission/settings/?sysMsg='+success);
			});
			
			
		    return false;
		})
		$('select#selectClassLevelID').change(function(){
			$.address.value('/apps/admission/settings/edit/'+this.value+'/');
		});	
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/settings/');
		});					
	},			

	application_form_init: function(lang){
		var schoolYearId = $('#selectSchoolYearID').val();
		var open_fancy_box = function(type){
		    var page = '#edit_box';
			if(type == 'edit_class'){
				page = '#edit_class';
			}
			$.fancybox( {
				'fitToView'    	 : false,	
				'autoDimensions'    : false,
				'autoScale'         : false,
				'autoSize'          : false,
				'width'             : 600,
				'height'            : 450,
				'padding'			 : 20,

				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic',
				'href' : page				 
			});	 
		}
		//Henry modified[20150120]
		$('div.common_table_tool a.tool_set').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
				open_fancy_box();
			}else{
				alert(lang.selectatleastonerecord);
			}
			return false;
		});
		
		//Henry added[20150120]
		$('div.common_table_tool a.tool_set_class').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
				open_fancy_box('edit_class');
			}else{
				alert(lang.selectatleastonerecord);
			}
			return false;
		});	 
		$('div.common_table_tool a.tool_print_form').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			//$('#application_form').prop("target", "_blank").prop("action", KIS_ROOT+"apps/admission/ajax.php?action=printApplications&schoolYearID="+schoolYearId).submit();
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('div.common_table_tool a.tool_print_reply_note').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			//$('#application_form').prop("target", "_blank").prop("action", KIS_ROOT+"apps/admission/ajax.php?action=printApplications&schoolYearID="+schoolYearId).submit();
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=reply_note").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=reply_note").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('div.common_table_tool a.tool_print_interview_form').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			
			if(hasChecked){
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
			}else{
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('div.common_table_tool a.tool_export_label').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			
			if(hasChecked){
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_address_label.php").submit();
			}else{
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_address_label.php").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});

			if(currentSchool.match(/^creative.*$/)){
				$('#application_form #advancedSearch').empty();
				$('#advancedSearchForm select').clone().appendTo('#application_form #advancedSearch');
			}

			if(hasChecked){
				$('#application_form').prop("action", "/kis/admission_form/export_form.php").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_form.php?selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}

			if(currentSchool.match(/^creative.*$/)){
				$('#application_form #advancedSearch').empty();
			}
			return false;
		});
		$('a.tool_export_status').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_form.php?type=status").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_form.php?type=status&selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export_interview_time').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_form.php?type=interviewTime").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_form.php?type=interviewTime&selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export_EDB').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_form.php?type=edb").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_form.php?type=edb&selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export_cust').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectStatus = $('#selectStatus').val();

			var url = '/apps/admission/applicantslist/cust/export/?';
			
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			if(hasChecked){
				url += 'applicationIds='+applicationIds;
				if(typeof(selectClassLevelID) != 'undefined'){
					url += '&classLevelID='+selectClassLevelID;
				}
				$.address.value(url);
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					//$('#application_form').prop("target", "_blank").prop("action", "#/apps/admission/emaillist/compose/").submit();
					url += 'selectStatus='+selectStatus;
					if(typeof(selectClassLevelID) != 'undefined'){
						url += '&classLevelID='+selectClassLevelID;
					}
					$.address.value(url);
				}
			}
			return false;
		});
		$('a.tool_export_stu_acc').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_for_import_account.php?TabID=2").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_for_import_account.php?TabID=2&selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export_prt_acc').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_for_import_account.php?TabID=3").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_for_import_account.php?TabID=3&selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export_payment_record').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_payment_record.php?TabID=4").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_payment_record.php?TabID=4&selectStatus="+$('#selectStatus').val()).submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_download_application_attachment').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			
			$.address.value('/apps/admission/applicantslist/attachment_download/?schoolYearID='+selectSchoolYearID+'&classLevelID='+selectClassLevelID);
			return false;
		});
		$('a.tool_import_interview').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			var selectStatus = $('#selectStatus').val();
			
			$.address.value('/apps/admission/applicantslist/importinterview/?selectStatus='+selectStatus+'&schoolYearID='+selectSchoolYearID+'&classLevelID='+selectClassLevelID);
			return false;
		});
		$('a.tool_import_briefing').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			var selectStatus = $('#selectStatus').val();
			
			$.address.value('/apps/admission/applicantslist/importbriefing/?selectStatus='+selectStatus+'&schoolYearID='+selectSchoolYearID+'&classLevelID='+selectClassLevelID);
			return false;
		});
		$('a.tool_import_admission_form').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			var selectStatus = $('#selectStatus').val();
			
			$.address.value('/apps/admission/applicantslist/import/?selectStatus='+selectStatus+'&schoolYearID='+selectSchoolYearID+'&classLevelID='+selectClassLevelID);
			return false;
		});
		$('a.tool_import_admission_status').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			var selectStatus = $('#selectStatus').val();
			
			$.address.value('/apps/admission/applicantslist/import/?selectStatus='+selectStatus+'&schoolYearID='+selectSchoolYearID+'&classLevelID='+selectClassLevelID+'&type=status');
			return false;
		});
		$('div.common_table_tool a.tool_send_email').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectStatus = $('#selectStatus').val();
			
			var url = '/apps/admission/applicantslist/compose/?';
			
			$("input[name='applicationAry\[\]']").each(function(){
				if(this.checked){
					hasChecked = true;
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			if(currentSchool === 'hkugac' && !hasChecked){
				alert(lang.selectatleastonerecord);
				return false;
			}else if(hasChecked){
			//$('#application_form').prop("target", "_blank").prop("action", "#/apps/admission/emaillist/compose/").submit();
				url += 'applicationIds='+applicationIds;
				if(typeof(selectClassLevelID) != 'undefined'){
					url += '&classLevelID='+selectClassLevelID;
				}
				$.address.value(url);
			}else{
				$("input[name='applicationAry[]']").each(function(){
						//this.checked = true;
						if(applicationIds!='') applicationIds+= ',';
						applicationIds += this.value;
					});
				if(applicationIds==""){
					alert(lang.selectatleastonerecord);
					return false;
				}
				if(confirm(lang.sendemailallrecordsornot)){
					//$('#application_form').prop("target", "_blank").prop("action", "#/apps/admission/emaillist/compose/").submit();
					url += 'selectStatus='+selectStatus;
					if(typeof(selectClassLevelID) != 'undefined'){
						url += '&classLevelID='+selectClassLevelID;
					}
					$.address.value(url);
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});*/
				}
			}
			return false;
		});
		$('div.common_table_tool a.tool_resend_email').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var selectClassLevelID = $('#selectClassLevelID').val();
			var selectStatus = $('#selectStatus').val();
			
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			if(hasChecked){
				if(confirm(lang.confirmresendnotificationemail)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=resendNotificationEmailByIds', {recordIds:applicationIds}, function(data){	
						kis.hideLoading().loadBoard();
						if(data == 'ResendSuccess'){
							alert(lang.successresendnotificationemail);
						}
						else{
							alert(lang.unsuccessresendnotificationemail);
						}
					});
					return false;
				}
			}else{
				alert(lang.selectatleastonerecord);
			}
			return false;
		});
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var recordIds = '';
			$("input[name='emailAry[]']").each(function(i){
				if(this.checked){
					hasChecked = true;
					if(recordIds!='') recordIds+= ',';
					recordIds += this.value;
				}
			});
			if(hasChecked){
				if(confirm(lang.msg_confirm_delete_email_list)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeEmailListByIds', {recordIds:recordIds}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
					});
					return false;
				}
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});				
		$('form#application_status_form').submit(function(){
			var applicationIds = '';
			var status = $('#status').val();
			$("input[name='applicationAry[]']").each(function(i){
				if(this.checked){
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			kis.showLoading();
			$.post('apps/admission/ajax.php?action=updateApplicationStatusByIds', {applicationIds:applicationIds,status:status}, function(data){	
				parent.$.fancybox.close();
				kis.hideLoading().loadBoard();
			});
			return false;
		});
		//Henry added [20150120]
		$('form#application_class_form').submit(function(){
			var applicationIds = '';
			var classlevel = $('#selectChangeClassLevelID').val();
			$("input[name='applicationAry[]']").each(function(i){
				if(this.checked){
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			kis.showLoading();
			$.post('apps/admission/ajax.php?action=updateApplicationClassLevelByIds', {applicationIds:applicationIds,classlevel:classlevel}, function(data){	
				parent.$.fancybox.close();
				kis.hideLoading().loadBoard();
			});
			return false;
		});			
		$('select#selectClassLevelID').change(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+this.value+'/');
		});
		
		$("a[name='emaillist_ViewStudentBtn']").click(function(){
			var recordID = this.id.replace("emaillist_ViewStudentBtn_","");
			var applicantionIds ="";
			$.post(
				'apps/admission/ajax.php?action=getEmailReceiversLayer',
				{
					'recordID' : recordID,
					'applicantionIds' : applicantionIds
				},
				function(data){
					$.fancybox(data, {
						'fitToView' : true,
						'autoDimensions' : true,
						'autoScale' : false,
						'autoSize' : true,
						'padding' : 20,
						'transitionIn' : 'elastic',
						'transitionOut' : 'elastic'
					});
				}
			);						
		});	
	},	

	application_details_init: function(lang,msg){
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		kis.datepicker('#interviewdate');
		$('#interviewdate').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}
			$('span#warning_interviewdate').html(warning);
		});
		$('div.Content_tool a.print_interview_form').click(function(){
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
			return false;
		});
		$('div.Content_tool a.print').click(function(){
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
			return false;
		});						
	},	

	application_details_edit_init: function(lang){
		var schoolYearId = $('#schoolYearId').val();
		var recordID = $('#recordID').val();
		var display = $('#display').val();
		var timeSlot = lang.timeslot.split(',');
		var checkValidForm = function(display){
			if(display=='icmssectionb'){ 
				if($('#student_surname').val()==''){
					alert(lang.enterchinesename);
					$('#student_surname').focus();
					return false;
				}else if($('#student_firstname').val()==''){
					alert(lang.enterenglishname);
					$('#student_firstname').focus();
					return false;
				}else if(!$("input[name=gender]:checked").val()){
					alert(lang.selectgender);
					$('#gender_M').focus();
					return false;
				}else if($('#dateofbirth').val()==''){
					alert(lang.enterdateofbirth);
					$('#dateofbirth').focus();
					return false;
				}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
					alert(lang.invaliddateformat);
					$('#dateofbirth').focus();
					return false;
				}else if($('#placeofbirth').val()==''){
					alert(lang.enterplaceofbirth);
					$('#placeofbirth').focus();
					return false;
				}else if($('#province').val()==''){
					alert(lang.enternativelanguage_icms);
					$('#province').focus();
					return false;
				}else if($('#homephoneno').val()==''){
					alert(lang.enterstudenthomephoneno);
					$('#homephoneno').focus();
					return false;
				}else if($('#homeaddress').val()==''){
					alert(lang.enterhomeaddress);
					$('#homeaddress').focus();
					return false;
				}else if($('#email').val()==''){
					alert(lang.enterstudentemail);
					$('#email').focus();
					return false;
				}else if($('#OthersCurBSName').val()==''){
					alert(lang.CurBSName_icms);
					$('#OthersCurBSName').focus();
					return false;
				}else if($('#LangSpokenAtHome').val()==''){
					alert(lang.enterlangspokenathome_icms);
					$('#LangSpokenAtHome').focus();
					return false;
				}
			}else if(display=='icmssectionc'){
				
				for(var i=0;i<2;i++){
						if($("input[id=parent_surname]")[i].value==''){
							alert(lang.enterchinesename);
							$("input[id=parent_surname]")[i].focus();
							return false;
						}else if($("input[id=parent_firstname]")[i].value==''){
							alert(lang.enterenglishname);
							$("input[id=parent_firstname]")[i].focus();
							return false;
						}else if($("input[id=mobile]")[i].value==''){
							alert(lang.entermobileno_icms);
							$("input[id=mobile]")[i].focus();
							return false;
						}else if($("input[id=NativeLanguage]")[i].value==''){
							alert(lang.enternativelanguage_icms);
							$("input[id=NativeLanguage]")[i].focus();
							return false;
						}else if($("input[id=occupation]")[i].value==''){
							alert(lang.enteroccupation);
							$("input[id=occupation]")[i].focus();
							return false;
						}else if($("input[id=office]")[i].value==''){
							alert(lang.entercompanyno_icms);
							$("input[id=office_no]")[i].focus();
							return false;
						}else if($("input[id=companyaddress]")[i].value==''){
							alert(lang.entercompanyaddress_icms);
							$("input[id=companyaddress]")[i].focus();
							return false;
						}
				}
			}else if(display=='icmssectiondef'){
				if($('#ChildHealth').val()==''){
					alert(lang.childhealth_icms);
					$('#ChildHealth').focus();
					return false;
				}
			}else if(display=='studentinfo'){
				if(lang.TSUENWANBCKG_Settings == 1){
					if($('#student_surname_b5').val()==''){
						alert(lang.enterchinesename);
						$('#student_surname_b5').focus();
						return false;
					}else if($('#student_firstname_b5').val()==''){
						alert(lang.enterchinesename);
						$('#student_firstname_b5').focus();
						return false;
					}else if($('#student_surname_en').val()==''){
						alert(lang.enterenglishname);
						$('#student_surname_en').focus();
						return false;
					}else if($('#student_firstname_en').val()==''){
						alert(lang.enterenglishname);
						$('#student_firstname_en').focus();
						return false;
					}else if($('#dateofbirth').val()==''){
						alert(lang.enterdateofbirth);
						$('#dateofbirth').focus();
						return false;
					}else if(!$("input[name=gender]:checked").val()){
						alert(lang.selectgender);
						$('#gender_M').focus();
						return false;
					}else if($('#nationality').val()==''){
						alert(lang.enternationality_icms);
						$('#nationality').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
						alert(lang.invaliddateformat);
						$('#dateofbirth').focus();
						return false;
					}else if($('#homephoneno').val()==''){
						alert(lang.enterstudenthomephoneno);
						$('#homephoneno').focus();
						return false;
					}else if($('#contactperson').val()==''){
						alert(lang.enterstudentcontactperson);
						$('#contactperson').focus();
						return false;
					}else if($('#contactpersonrelationship').val()==''){
						alert(lang.enterstudentrelationship);
						$('#contactpersonrelationship').focus();
						return false;
					}else if($('#homeaddress').val()==''){
						alert(lang.enterhomeaddress);
						$('#homeaddress').focus();
						return false;
					}else if($('#email').val()==''){
						alert(lang.enterstudentemail);
						$('#email').focus();
						return false;
					}else if($('#birthcertno').val()==''){
						alert(lang.enterbirthcertificatenumber);
						$('#birthcertno').focus();
						return false;
					}else if($('#placeofbirth').val()==''){
						alert(lang.enterplaceofbirth);
						$('#placeofbirth').focus();
						return false;
					}
				}else if(lang.RMKG_Settings == 1){
				
					if($('#student_surname_b5').val()==''){
						alert(lang.studentsfirstname_b5_csm);
						$('#student_surname_b5').focus();
						return false;
					}else if($('#student_firstname_b5').val()==''){
						alert(lang.studentsfirstname_b5_csm);
						$('#student_firstname_b5').focus();
						return false;
					}else if($('#student_surname_en').val()==''){
						alert(lang.studentssurname_en_csm);
						$('#student_surname_en').focus();
						return false;
					}else if($('#student_firstname_en').val()==''){
						alert(lang.studentsfirstname_en_csm);
						$('#student_firstname_en').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
						alert(lang.invaliddateformat);
						$('#dateofbirth').focus();
						return false;
					}else if(!$("input[name=gender]:checked").val()){
						alert(lang.selectgender);
						$('#gender_M').focus();
						return false;
					}else if($('#placeofbirth').val()==''){
						alert(lang.enterplaceofbirth);
						$('#placeofbirth').focus();
						return false;
					}else if($('#birthcertno').val()==''){
						alert(lang.enterbirthcertificatenumber_MUNSANG);
						$('#birthcertno').focus();
						return false;
					}else if($('#homephoneno').val()==''){
						alert(lang.enterstudenthomephoneno_MUNSANG);
						$('#homephoneno').focus();
						return false;
					} else if($('#HomeAddrRoom').value==''){
						alert(lang.enterhomeaddress_MUNSANG);	
						$('#HomeAddrRoom').focus();
						return false;
					} else if($('#HomeAddrFloor').value==''){
						alert(lang.enterhomeaddress_MUNSANG);	
						$('#HomeAddrFloor').focus();
						return false;
					} else if($('#HomeAddrBlock').value==''){
						alert(lang.enterhomeaddress_MUNSANG);	
						$('#HomeAddrBlock').focus();
						return false;
					} else if($('#HomeAddrBlding').value==''){
						alert(lang.enterhomeaddress_MUNSANG);	
						$('#HomeAddrBlding').focus();
						return false;
					} else if($('#HomeAddrStreet').value==''){
						alert(lang.enterhomeaddress_MUNSANG);	
						$('#HomeAddrStreet').focus();
						return false;
					} else if($('#HomeAddrDistrict').value==''){
						alert(lang.enterhomeaddress_MUNSANG);	
						$('#HomeAddrDistrict').focus();
						return false;
					}else if($('#Email').val()==''){
						alert(lang.enterstudentemail);
						$('#Email').focus();
						return false;
					}
				}else if(lang.CHIUCHUNKG_Settings == 1){
					if($('#student_surname_b5').val()==''){
						alert(lang.studentsfirstname_b5_csm);
						$('#student_surname_b5').focus();
						return false;
					}else if($('#student_firstname_b5').val()==''){
						alert(lang.studentsfirstname_b5_csm);
						$('#student_firstname_b5').focus();
						return false;
					}else if($('#student_surname_en').val()==''){
						alert(lang.studentssurname_en_csm);
						$('#student_surname_en').focus();
						return false;
					}else if($('#student_firstname_en').val()==''){
						alert(lang.studentsfirstname_en_csm);
						$('#student_firstname_en').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
						alert(lang.invaliddateformat);
						$('#dateofbirth').focus();
						return false;
					}else if(!$("input[name=gender]:checked").val()){
						alert(lang.selectgender);
						$('#gender_M').focus();
						return false;
					}else if($('#placeofbirth').val()==''){
						alert(lang.enterplaceofbirth);
						$('#placeofbirth').focus();
						return false;
					}else if($('#province').val()==''){
						alert(lang.enternativeplace);
						$('#province').focus();
						return false;
					}else if($('#county').val()==''){
						alert(lang.enternativeplace);
						$('#county').focus();
						return false;
					}else if($('#birthcertno').val()==''){
						alert(lang.enterbirthcertificatenumber_MUNSANG);
						$('#birthcertno').focus();
						return false;
					}else if($('#homephoneno').val()==''){
						alert(lang.enterstudenthomephoneno_MUNSANG);
						$('#homephoneno').focus();
						return false;
					}else if(!$("#HomeAddressDistrict").val()){
						alert(lang.enterhomeaddress_MUNSANG);
						$('#HomeAddressDistrict').focus();
						return false;
					}else if(!$("#homeaddress").val()){
						alert(lang.enterhomeaddress_MUNSANG);
						$('#homeaddress').focus();
						return false;
					}else if(!$("#homeaddresschi").val()){
						alert(lang.enterhomeaddress_MUNSANG);
						$('#homeaddresschi').focus();
						return false;
					}
				}
				else if(lang.MUNSANG_Settings == 1){
					if($('#student_surname_b5').val()==''){
						alert(lang.student_firstname_b5_CSM);
						$('#student_surname_b5').focus();
						return false;
					}else if($('#student_firstname_b5').val()==''){
						alert(lang.student_firstname_b5_CSM);
						$('#student_firstname_b5').focus();
						return false;
					}else if($('#student_surname_en').val()==''){
						alert(lang.student_surname_en_CSM);
						$('#student_surname_en').focus();
						return false;
					}else if($('#student_firstname_en').val()==''){
						alert(lang.student_firstname_en_CSM);
						$('#student_firstname_en').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
						alert(lang.invaliddateformat);
						$('#dateofbirth').focus();
						return false;
					}else if(!$("input[name=gender]:checked").val()){
						alert(lang.selectgender);
						$('#gender_M').focus();
						return false;
					}else if($('#placeofbirth').val()==''){
						alert(lang.enterplaceofbirth);
						$('#placeofbirth').focus();
						return false;
					}else if($('#birthcertno').val()==''){
						alert(lang.enterbirthcertificatenumber_MUNSANG);
						$('#birthcertno').focus();
						return false;
					}else if($('#homephoneno').val()==''){
						alert(lang.enterstudenthomephoneno_MUNSANG);
						$('#homephoneno').focus();
						return false;
					}else if(!$("#homeaddress").val()){
						alert(lang.enterhomeaddress_MUNSANG);
						$('#homeaddress').focus();
						return false;
					}
				}
				else if(lang.CSM_Settings == 1){
					if($('#student_surname_en').val()==''){
						alert(lang.studentssurname_en_csm);
						$('#student_surname_en').focus();
						return false;
					}else if($('#student_firstname_en').val()==''){
						alert(lang.studentsfirstname_en_csm);
						$('#student_firstname_en').focus();
						return false;
					}else if($('#student_surname_b5').val()==''){
						alert(lang.studentssurname_b5_csm);
						$('#student_surname_b5').focus();
						return false;
					}else if($('#student_firstname_b5').val()==''){
						alert(lang.studentsfirstname_b5_csm);
						$('#student_firstname_b5').focus();
						return false;
					}else if(!$("input[name=gender]:checked").val()){
						alert(lang.selectgender);
						$('#gender_M').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
						alert(lang.invaliddateformat);
						$('#dateofbirth').focus();
						return false;
					/*}else if($('#placeofbirth').val()==''){
						alert(lang.enterplaceofbirth);
						$('#placeofbirth').focus();
						return false;*/
					}else if($('#birthcertno').val()==''){
						alert(lang.enterbirthcertificatenumber_csm);
						$('#birthcertno').focus();
						return false;
					/*}else if($('#homephoneno').val()==''){
						alert(lang.enterstudenthomephoneno_csm);
						$('#homephoneno').focus();
						return false;*/
					}else if(!$("input[name=homeaddress]:checked").val()){
						alert(lang.enterhomeaddress_csm);
						$('#homeaddress1').focus();
						return false;
					/*}else if(!$("input[name=CurBSName]:checked").val()){
						alert(lang.entercurbsname_csm);
						$('#CurBSName1').focus();
						return false;*/		
					}
				}
				else{
					if($('#student_name_b5').val()==''){
						alert(lang.enterchinesename);
						$('#student_name_b5').focus();
						return false;
					}else if($('#student_name_en').val()==''){
						alert(lang.enterenglishname);
						$('#student_name_en').focus();
						return false;
					}else if($('#dateofbirth').val()==''){
						alert(lang.enterdateofbirth);
						$('#dateofbirth').focus();
						return false;
					}else if(!$("input[name=gender]:checked").val()){
						alert(lang.selectgender);
						$('#gender_M').focus();
						return false;
					}else if(!check_date_without_return_msg(document.getElementById('dateofbirth'))){
						alert(lang.invaliddateformat);
						$('#dateofbirth').focus();
						return false;
					/*}else if($('#homephoneno').val()!=''){
						var homephoneno = Trim($("#homephoneno").val());
						if(!is_int(homephoneno)){
							alert(lang.invalidhomephoneformat);
							$('#homephoneno').focus();
							return false;
						}*/
					}else if($('#homephoneno').val()==''){
						alert(lang.enterstudenthomephoneno);
						$('#homephoneno').focus();
						return false;
					}else if($('#contactperson').val()==''){
						alert(lang.enterstudentcontactperson);
						$('#contactperson').focus();
						return false;
					}else if($('#contactpersonrelationship').val()==''){
						alert(lang.enterstudentrelationship);
						$('#contactpersonrelationship').focus();
						return false;
					}else if($('#homeaddress').val()==''){
						alert(lang.enterhomeaddress);
						$('#homeaddress').focus();
						return false;
					}else if($('#email').val()==''){
						alert(lang.enterstudentemail);
						$('#email').focus();
						return false;
					}else if($('#birthcertno').val()==''){
						alert(lang.enterbirthcertificatenumber);
						$('#birthcertno').focus();
						return false;
					}else if($('#placeofbirth').val()==''){
						alert(lang.enterplaceofbirth);
						$('#placeofbirth').focus();
						return false;
					}
				}
			}else if(display=='parentinfo'){
				/*var hasInputParent = false;
				for(var i=0;i<3;i++){
					var hasInput = false;
					$('.parent_'+lang.parenttype.split(',')[i]).each(function(){
					   if(this.value!=''){
					   		hasInput = true;
					   		hasInputParent = true;
					   		return false;
					   }
					});
					if(hasInput){
						if($("input[id=parent_name_b5]")[i].value==''){
							alert(lang.enterchinesename);
							$("input[id=parent_name_b5]")[i].focus();
							return false;
						}else if($("input[id=parent_name_en]")[i].value==''){
							alert(lang.enterenglishname);
							$("input[id=parent_name_en]")[i].focus();
							return false;
						}else if($("input[id=occupation]")[i].value==''){
							alert(lang.enteroccupation);
							$("input[id=occupation]")[i].focus();
							return false;
						}else{
							var office_no = Trim($("input[id=office]")[i].value);
							var mobile_no = Trim($("input[id=mobile]")[i].value);
							if(office_no=='' && mobile_no==''){
								alert(lang.enteratleastonephoneno);
								$("input[id=office]")[i].focus();
								return false;
							}*//*else if(office_no!='' && !is_int(office_no)){
								alert(lang.invalidofficephoneformat);
								$("input[id=office]")[i].focus();
								return false;
							}else if(mobile_no!='' && !is_int(mobile_no)){
								alert(lang.invalidmobilephoneformat);
								$("input[id=mobile]")[i].focus();
								return false;
							}*//*
						}
					}
				}
				if(!hasInputParent){
					alert(lang.enteratleastoneparent);
					$("input[id=parent_name_en]")[0].focus();
					return false;
				}*/
			}else if(display=='otherinfo'){
				if(lang.YLSYK_Settings == 1){
				}
				else if(lang.TSUENWANBCKG_Settings == 1){
				}
				else if(lang.RMKG_Settings == 1){
				}
				else if(lang.CHIUCHUNKG_Settings == 1){
				}
				else if(lang.MUNSANG_Settings == 1){
				}
				else if(lang.MGF_Settings == 1){
				}
				else if(lang.MINGWAIPE_Settings == 1){
				}
				else if(lang.MINGWAI_Settings == 1){
				}
				else{
					if(!$("input[name=OthersApplyTerm]:checked").val()){
						alert(lang.enterapplyTerm);
						$('input[name=OthersApplyTerm]')[0].focus();
						return false;
					}else if(!$("input[name=OthersNeedSchoolBus]:checked").val()){
						alert(lang.enterneedschoolbus);
						$('#needschoolbus_y').focus();
						return false;
					}else if($("input[name=OthersNeedSchoolBus]:checked").val()==1&&$('#OthersSchoolBusPlace').val()==''){
						alert(lang.enterplacefortakingschoolbus);
						$('#OthersSchoolBusPlace').focus();
						return false;
					}else if(!$("input[name=OthersKnowUsBy]:checked").val()){
						alert(lang.selectknowusby);
						$('input[name=OthersKnowUsBy]')[0].focus();
						return false;
					}else{
						var hasfilledin = true;
						$("input.hasknowusbyother").each(function(){
							if($(this).prop('checked') && $('span#span_'+this.id+' input').val()==''){
								alert(lang.enterknowusby);
								$('span#span_'+this.id+' input').focus();
								hasfilledin = false;
							}
						});
						if(!hasfilledin){
							return false;
						}
					}
				}
			}else if(display=='remarks'){
				if($("input#receiptID").val()!=''){
					if($('#receiptdate').val()==''){
						alert(lang.enterreceiptdate);
						$('#receiptdate').focus();
						return false;
					}else if(document.getElementById('receiptdate') && !check_date_without_return_msg(document.getElementById('receiptdate'))){
						alert(lang.invaliddateformat);
						$('#receiptdate').focus();
						return false;
					}else if($('#handler').val()==''){
						alert(lang.enterreceipthandler);
						$('#handler').focus();
						return false;
					}
				}else if($('#interviewdate').val()!=''&& document.getElementById('interviewdate') && !check_date_without_return_msg(document.getElementById('interviewdate'))){
					alert(lang.invaliddateformat);
					$('#interviewdate').focus();
					return false;
				}
			}

			return true;
		};
		if(display=='studentinfo' || display=='icmssectionb'){ // for icms cust
			if(lang.CSM_Settings == 1){
				$('select.timeslotselection').change(function(){
					var id = this.id;
					var value = $(this).val();
					var hasChosen = false;
					if(value!=''){
						$('select.timeslotselection').each(function(){
							if(this.id != id && $(this).val() == value){
								hasChosen = true;
								return false;
							}	
						});	
						if(hasChosen){
							alert(lang.timeslotselected);
							$(this).val('');
						}
					}
				});
			}
			if ($('#studentphoto').length>0){
			    kis.uploader({
					browse_button:  'studentphoto',
					url: './apps/admission/ajax.php?action=updatephoto&id='+recordID,
					multi_selection: false,
					auto_start: true,
					onFilesAdded: function(up, files) {
					    $('#studentphoto img').css({opacity: 0.5});
					},
					onUploadProgress: function(up, file) {
					    
					},
					onFileUploaded: function(up, file, info) {
					    
					   var res = $.parseJSON(info.response);
					    if (res.error){
							alert(res.error);
					    }else if (res.personal_photo){
							$('#studentphoto img').attr('src',res.personal_photo).show();
							$('#studentphoto .mail_icon_form').show();
					    }
					    $('#studentphoto img').css({opacity: 1});
					   
					}
			    });
			}
			$('#studentphoto .btn_remove').click(function(){
			    if(confirm(lang.deletecurrentpersonalphoto)){
			    	kis.showLoading();
			    	$(this).parent().hide();
				    $.post('apps/admission/ajax.php?action=removeAttachment', {id:recordID,type:'personal_photo'}, function(data){	
				    	$('#studentphoto img').attr('src',KIS_BLANK_PHOTO).show();
				    	kis.hideLoading();
				    });
			    }
			    return false;
			})	
		}else if(display=='otherinfo' || display=='icmssectiona'){ // for icms cust
			$('select.timeslotselection').change(function(){
				var id = this.id;
				var value = $(this).val();
				var hasChosen = false;
				if(value!=''){
					$('select.timeslotselection').each(function(){
						if(this.id != id && $(this).val() == value){
							hasChosen = true;
							return false;
						}	
					});	
					if(hasChosen){
						alert(lang.timeslotselected);
						$(this).val('');
					}
				}
			});	
			$("input[name=OthersNeedSchoolBus]").change(function(){
				if($(this).val()==1){
					$("#td_needschoolbus").show();
				}else{
					$("#td_needschoolbus").hide();			
				}
			});		
			$("input[name=OthersKnowUsBy]").change(function(){
				$("span.knowusbyother").each(function(){
					$(this).hide();
				});
				if($(this).hasClass('hasknowusbyother')){
					$('#span_'+this.id).show();
				}	
			});	
		}else if(display=='remarks'){
			kis.datepicker('#receiptdate');	
			$("input#receiptID").keyup(function(){
				if(Trim($(this).val())!=''){
					$('.receipt_info').show();
				}else{
					$('.receipt_info').hide();
				}
			});
			$('#receiptdate').change(function(){
				var warning = '';
				if(this.value!=''&&!check_date_without_return_msg(this)){
					warning = lang.invaliddateformat;
				}
				$('span#warning_receiptdate').html(warning);
			});
			kis.datepicker('#interviewdate');
			kis.datepicker('#briefingdate');
			$('#interviewdate').keyup(function(){
				var warning = '';
				if(this.value!=''&&!check_date_without_return_msg(this)){
					warning = lang.invaliddateformat;
				}
				$('span#warning_interviewdate').html(warning);
			});	
			
			$('#briefingdate').keyup(function(){
				var warning = '';
				if(this.value!=''&&!check_date_without_return_msg(this)){
					warning = lang.invaliddateformat;
				}
				$('span#warning_briefingdate').html(warning);
			});	
			
			$('.mail_icon_form .btn_select_ppl').click(function(){
			    $('.mail_select_user').css({'margin-left': -380});
			    $('.mail_select_user .search_results').empty().hide();
			    return false; 
			    
			});		
			$('.mail_select_user').submit(function(){
			    
			    var form = this;
			    kis.showLoading();
			    $.post('apps/admission/ajax.php?action=searchusers', $(this).serialize(), function(res){
				kis.hideLoading();
				
				$('.mail_select_user .search_results').html(res.ui).show();
		
						
			    }, 'json');
			    return false;
			});	
			$('.mail_select_user .search_results').on('click','.btn_add', function(){
			    
			    $('#span_handler').html($(this).closest('.mail_user'));
			    $('.mail_select_user').css({'margin-left': 0});
			    return false;
			});	
			$('.mail_select_user .formsubbutton').click(function(){
			    
			    $('.mail_select_user').css({'margin-left': 0});
			    return false;
			});	
			$('.mail_to_list').on('click', '.mail_user .btn_remove', function(){
			    
			    $('.mail_user').html('--<input type="hidden" name="handler" id="handler">');
			    return false;
			    
			});						
		}
		$('#applicant_form').submit(function(){
			if(checkValidForm(display)){
				$.post('apps/admission/ajax.php?action=updateApplicationInfo', $(this).serialize(), function(success){ 
					$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display+'&sysMsg='+success);
				});
			}
			return false;
		});
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/details/'+schoolYearId+'/'+recordID+'/'+display);
		});			
		$('div.table_row_tool a.delete_dim').click(function(){
			var attachment_type = $(this).closest( "td" ).attr("id");
			var warning_msg = lang.are_you_sure_to_remove_attachment;
			if(attachment_type=='immunisation_record'){
				warning_msg = lang.are_you_sure_to_remove_immunisation_record;
			}else if(attachment_type=='birth_cert'){
				warning_msg = lang.are_you_sure_to_remove_birth_cert;
			}
			 if(confirm(warning_msg)){
		    	kis.showLoading();
		    	$(this).parent().hide();
			    $.post('apps/admission/ajax.php?action=removeAttachment', {id:recordID,type:attachment_type}, function(data){	
			    	$('td[id="'+attachment_type+'"] span.view_attachment').hide();
					$('td[id="'+attachment_type+'"] span.view_attachment a.file_attachment').attr('href','');
					$('td[id="'+attachment_type+'"] input.attachment_upload_btn').show();
			    	kis.hideLoading();
			    });
		    }
		    return false;
	
		});	
		$('.attachment_upload_btn').each(function(i){
			var attachment_type = this.id.split('-')[1];
			kis.uploader({
				browse_button:  this.id,
				url: './apps/admission/ajax.php?action=saveAttachment&type='+attachment_type+'&id='+recordID,
				auto_start: true,
				multi_selection: false,
				onFilesAdded: function(up, files) {
					var uploader = this;
					$.each(files, function(i, file){
						$('td[id="'+attachment_type+'"]').append('<a class="temp_file_attachment">'+file.name+'</a>').append($('<em class="progress">'));
						$('td[id="'+attachment_type+'"] input.attachment_upload_btn').hide();
					});
				},
				onUploadProgress: function(up, file) {
					$('td[id="'+attachment_type+'"] .progress').html(''+file.percent+'%');
				},
				onFileUploaded: function(up, file, info) {
				 	var res = $.parseJSON(info.response);
					$('td[id="'+attachment_type+'"] a.temp_file_attachment').remove();
					$('td[id="'+attachment_type+'"] em.progress').remove();
					$('td[id="'+attachment_type+'"] span.view_attachment').show();
					$('td[id="'+attachment_type+'"] span.view_attachment div.table_row_tool').show();
					$('td[id="'+attachment_type+'"] span.view_attachment a.file_attachment').attr('href','./apps/admission/ajax.php?action=getAttachmentFile&schoolYearId='+schoolYearId+'&id='+recordID+'&type='+attachment_type);
					$('td[id="'+attachment_type+'"] span.view_attachment .attachment_crop_btn').data('imgUrl','./apps/admission/ajax.php?action=getAttachmentFile&viewImage=1&schoolYearId='+schoolYearId+'&id='+recordID+'&type='+attachment_type);
				} 
			});	
		});										
	},
		
	application_instruction_edit_init: function(lang){
		var instruction_editor = kis.editor('generalInstruction');
		var schoolYearId = $('#schoolYearId').val();
		$('#application_settings').submit(function(){
			    instruction_editor.finish();
				$.post('apps/admission/ajax.php?action=updateBasicSettings', $(this).serialize(), function(success){
					$.address.value('/apps/admission/settings/instructiontoapplicants/?sysMsg='+success);
				});
		    return false;
		});   
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/settings/instructiontoapplicants/');
		});	 
	},	

	application_init: function(lang){
		 $("input[name=keyword]").keypress(function(event){
		    if(event.keyCode == 13){
		    	var schoolYearId = $('#selectSchoolYearID').val();
		    	$.address.value('/apps/admission/applicantslist/listbyform/search/?keyword='+this.value);
		    }
		  });
		  return false;
	},	
	application_settings_init: function(msg){			
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
	},
	
	print_form_header_init: function(msg){			
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
	},
	
	online_payment_init: function(msg){			
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
	},
	
	module_settings_init: function(msg){			
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
	},
	
	attachment_settings_init: function(msg){
		var remove_specialchars = function(text){
			var chars_to_remove = '~!@#$%^&*()_+|{}:"<>?`-=\\[];\',./';
			for(var i=0;i<chars_to_remove.length;i++){
				var c = chars_to_remove.charAt(i);
				while(text.indexOf(c) != -1) {
					text = text.replace(c,'');
				}
			}
			return text;
		};
		var msg_request_input_attachment_name = msg.request_input_attachment_name;
		var msg_attachment_name_cannot_use = msg.attachment_name_cannot_use;
		var msg_confirm_delete_attachment_setting = msg.confirm_delete_attachment_setting;
		var reserved_attachment_names = msg.reserved_attachment_names;
		
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		
		$('input#submitBtn').click(function(){
			var $submitBtn = $(this);
			$submitBtn.prop('disabled',true);
			
			var data = {};
			data.ExcludeRecordID = $('input#RecordID').val();

			$('.warning_attachment_name').html('');
			
			var isValid = true;
			$('input[id^="AttachmentName"]').each(function(){
				var $warning = $(this).parent().find('.warning_attachment_name');
				var id = $(this).attr('id');

				var attachment_name = $.trim(remove_specialchars($(this).val()));
				$(this).val(attachment_name);
				
				if(attachment_name == ''){
					$warning.html(msg_request_input_attachment_name);
					$submitBtn.prop('disabled',false);
					isValid = false;
				    return false;
				}else if(jQuery.inArray(attachment_name, reserved_attachment_names)>-1){
					$warning.html(msg_attachment_name_cannot_use);
					$submitBtn.prop('disabled',false);
					isValid = false;
				    return false;
				}
				data[id] = encodeURIComponent(attachment_name);
			});
			
			if(!isValid){
				return false;
			}
			
			$.post('apps/admission/ajax.php?action=checkAttachmentName', 
				data,
				function(recordCount){
					if(recordCount > 0)
					{
						$('#warning_attachment_name').html(msg_attachment_name_cannot_use);
						$(this).prop('disabled',false);
					}else{
						$.post('apps/admission/ajax.php?action=updateAttachmentSetting', $('#attachment_settings_form').serialize(), function(success){
							$.address.value('/apps/admission/settings/attachmentsettings/?sysMsg='+success);
						});
					}
				}
			);
		    return false;
		});  
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/settings/attachmentsettings/');
		});
		
		$('a.delete_dim').click(function(){
			var thisObj = $(this);
			var deleteRecordId = thisObj.find('input[name="RecordID[]"]').val();
			
			if(confirm(msg_confirm_delete_attachment_setting)){
				$.post('apps/admission/ajax.php?action=removeAttachmentSetting', 
				{
					'RecordID[]': [deleteRecordId]
				}, 
				function(success){
					$.address.value('/apps/admission/settings/attachmentsettings/?sysMsg='+success + '&r=' + (new Date()).getTime());
				});
			}
		});
		
		var tableObj = $(".common_table_list");
		if(tableObj.length > 0 && tableObj.tableDnD){
			tableObj.tableDnD({
				onDrop: function(table, DroppedRow) {
					var recordIdObj = document.getElementsByName('RecordID[]');
					var recordIdAry = [];
					for(var i=0;i<recordIdObj.length;i++){
						recordIdAry.push(recordIdObj[i].value);
					}
					var id_list = recordIdAry.join();
					if(this.IdList == id_list) return;
					
					$.post(
						'apps/admission/ajax.php?action=reorderAttachmentSettings',
						{
							'RecordID[]': recordIdAry
						},
						function(success){
							$.address.value('/apps/admission/settings/attachmentsettings/?sysMsg='+success + '&r=' + (new Date()).getTime());
						}
					);
				},
				onDragStart: function(table, DraggedCell) {
					var recordIdObj = document.getElementsByName('RecordID[]');
					var recordIdAry = [];
					for(var i=0;i<recordIdObj.length;i++){
						recordIdAry.push(recordIdObj[i].value);
					}
					this.IdList = recordIdAry.join();
				},
				dragHandle: "Dragable", 
				onDragClass: "move_selected"
			});
		}	
	},
	
	email_list_init: function(lang){
		var schoolYearId = $('#selectSchoolYearID').val();
		var classLevelId = $('#selectClassLevelID').val();
		var open_fancy_box = function(){
			$.fancybox( {
				'fitToView'    	 : false,	
				'autoDimensions'    : false,
				'autoScale'         : false,
				'autoSize'          : false,
				'width'             : 600,
				'height'            : 450,
				'padding'			 : 20,

				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic',
				'href' : '#edit_box'				 
			});	 
		}
		
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var recordIds = '';
			$("input[name='emailAry[]']").each(function(i){
				if(this.checked){
					hasChecked = true;
					if(recordIds!='') recordIds+= ',';
					recordIds += this.value;
				}
			});
			if(hasChecked){
				if(confirm(lang.msg_confirm_delete_email_list)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeEmailListByIds', {recordIds:recordIds}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
					});
					return false;
				}
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});				
		
		$('select#selectSchoolYearID').change(function(){
			var schoolYearId = $('#selectSchoolYearID').val();
			var classLevelId = $('#selectClassLevelID').val();
			var keyword = $.trim($('input#keyword').val());
			$.address.value('/apps/admission/emaillist/?selectSchoolYearID='+schoolYearId+'&selectClassLevelID='+classLevelId+'&keyword='+keyword);
			//$('form#email_list').submit();
		});
		
		$('select#selectClassLevelID').change(function(){
			var schoolYearId = $('#selectSchoolYearID').val();
			var classLevelId = $('#selectClassLevelID').val();
			var keyword = $.trim($('input#keyword').val());
			$.address.value('/apps/admission/emaillist/?selectSchoolYearID='+schoolYearId+'&selectClassLevelID='+classLevelId+'&keyword='+keyword);
			//$('form#email_list').submit();
		});
		
		$("a[name='emaillist_ViewStudentBtn']").click(function(){
			var recordID = this.id.replace("emaillist_ViewStudentBtn_","");
			var applicantionIds ="";
			$.post(
				'apps/admission/ajax.php?action=getEmailReceiversLayer',
				{
					'recordID' : recordID,
					'applicantionIds' : applicantionIds
				},
				function(data){
					$.fancybox(data, {
						'fitToView' : true,
						'autoDimensions' : true,
						'autoScale' : false,
						'autoSize' : true,
						'padding' : 20,
						'transitionIn' : 'elastic',
						'transitionOut' : 'elastic'
					});
				}
			);						
		});
		
		$('a.tool_export').click(function(){
			var hasChecked = false;
			$("input[name='emailAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#email_list').prop("action", "/kis/admission_form/export_email_list.php").submit();
			}else{
				alert(lang.msg_selectatleastonerecord);
				//if(confirm(lang.exportallrecordsornot)){
				//	$("input[name='emailAry[]']").each(function(){
				//		this.checked = true;
				//	});
				//	$('#interview_list').prop("action", "/kis/admission_form/export_interview_list.php").submit();
				//	$("input[name='emailAry[]']").each(function(){
				//		this.checked = false;
				//	});
				//}
			}
			return false;
		});	
	},
	
	briefing_init: function(lang){
		var sysmsg = lang.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+lang.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		var schoolYearId = $('#selectSchoolYearID').val();
		
		$('div.Content_tool a.tool_new').click(function(){
			$.address.value('/apps/admission/briefing/edit');
			return false;
		});
		
		$('div.Content_tool a.tool_export').click(function(){
			$('#exportForm input').remove();
			$("input[name='briefingId[]']:checked").clone().appendTo('#exportForm');
			$("<input>").attr('name', 'selectSchoolYearID').val($('#selectSchoolYearID').val()).appendTo('#exportForm');
		
			if($("input[name='briefingId[]']:checked").length > 0){
				$('#exportForm').submit();
			}else{
				if(confirm(lang.exportallrecordsornot)){
					$('#exportForm').submit();
				}
			}
			return false;
		});
		
		
		$('div.common_table_tool a.tool_edit').click(function(){
			var hasChecked = 0;
			var recordIds = '';
			$checked = $("input[name='briefingId[]']:checked");
			
			if($checked.length == 1){
				$.address.value('/apps/admission/briefing/edit?briefingId='+$checked.val());
				return false;
			}
			else{
				alert(lang.selectonlyonerecord);
				return false;
			}
		});
		
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var hasApplied = false;
			var recordIds = '';
			$checked = $("input[name='briefingId[]']:checked");
			
			values = $.map($checked, function(n, i){
				return n.value;
			}).join(',');
			
			if($checked.length > 0){
				if(confirm(lang.msg_confirm_delete)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeBriefingEditByIds', {recordIds: values}, function(data){
						kis.hideLoading().loadBoard();
					});
				}
			}else{
				alert(lang.msg_selectatleastonerecord);
			}
			return false;
		});
		
		$("input[name=keyword]").keypress(function(event){
		    if(event.keyCode == 13){
		    	var schoolYearId = $('#selectSchoolYearID').val();
		    	$.address.value('/apps/admission/briefing/applicantList/search/?keyword='+this.value);
		    }
		  });
	},
	briefing_edit_init: function(lang){
		kis.datepicker('#briefing_date');
		kis.datepicker('#enrolment_start_period_date');
		kis.datepicker('#enrolment_end_period_date');
		var instruction_editor = kis.editor('instruction');
		$('#briefing_end_time_sec').val(59);
		$('#enrolment_end_time_sec').val(59);
		
		$('#briefing_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}
			$('span#warning_briefing_date').html(warning);
		});
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/briefing/');
			return false;
		});
		$('#briefing_edit_form').submit(function(){
			var briefing_date = $('#briefing_date').val();
			var quota = $('#quota').val();
			var hasInput = false;
			
			//checking the date

			if(!check_date_without_return_msg(document.getElementById('briefing_date'))){
				$('span#warning_briefing_date').html(lang.invaliddateformat);
				$('#briefing_date').focus();
				return false;
			}
			if(compareTimeByObjId('','briefing_start_time_hour :selected','briefing_start_time_min :selected','briefing_start_time_sec :selected',
					'','briefing_end_time_hour :selected', 'briefing_end_time_min :selected','briefing_end_time_sec :selected')==false){
				$('span#warning_briefing_time').html(lang.enddateearlier);
				$('#briefing_start_time_hour').focus();
				return false;
			}
			if($('#briefing_title').val() == ''){
				$('span#warning_briefing_title').html(lang.entertitle);
				$('#briefing_title').focus();
				return false;
			}
			
			if($('#quota').val() == ''){
				$('span#warning_quota').html(lang.enterQuota);
				$('#quota').focus();
				return false;
			}
			if(!is_positive_int(quota)){
				$('span#warning_quota').html(lang.enterpositivenumber);
				$('#quota').focus();
				return false;
			}
			
			
			if($('#enrolment_start_period_date').val() != '' && !check_date_without_return_msg(document.getElementById('enrolment_start_period_date'))){
				$('span#warning_enrolment_start_period').html(lang.invaliddateformat);
				$('#enrolment_start_period_date').focus();
				return false;
			}
			if($('#enrolment_end_period_date').val() != '' && !check_date_without_return_msg(document.getElementById('enrolment_end_period_date'))){
				$('span#warning_enrolment_end_period').html(lang.invaliddateformat);
				$('#enrolment_end_period_date').focus();
				return false;
			}
			if( $('#enrolment_start_period_date').val() != '' && $('#enrolment_end_period_date').val() != ''){
				if(compareTimeByObjId(
					'enrolment_start_period_date','enrolment_start_time_hour :selected','enrolment_start_time_min :selected','enrolment_start_time_sec :selected',
					'enrolment_end_period_date','enrolment_end_time_hour :selected', 'enrolment_end_time_min :selected','enrolment_end_time_sec :selected')==false
				){
					$('span#warning_enrolment_end_period').html(lang.enddateearlier);
					$('#enrolment_end_period_date').focus();
					return false;
				}
				
				if(compareTimeByObjId(
					'briefing_date','briefing_start_time_hour :selected','briefing_start_time_min :selected','briefing_start_time_sec :selected',
					'enrolment_end_period_date','enrolment_end_time_hour :selected', 'enrolment_end_time_min :selected','enrolment_end_time_sec :selected')
				){
					$('span#warning_enrolment_end_period').html(lang.briefingenddateearlier);
					$('#enrolment_end_period_date').focus();
					return false;
				}
				
			}else if(!confirm(lang.emptyapplicationdatewarning)){
				return false;
			}
			
			instruction_editor.finish();
			
			
			var BriefingStartDate = $('#briefing_date').val() + ' ' + $('#briefing_start_time_hour').val() + ':' + $('#briefing_start_time_min').val() + ':' + $('#briefing_start_time_sec').val();
			var BriefingEndDate = $('#briefing_date').val() + ' ' + $('#briefing_end_time_hour').val() + ':' + $('#briefing_end_time_min').val() + ':' + $('#briefing_end_time_sec').val();
			var EnrolmentStartDate = $('#enrolment_start_period_date').val() + ' ' + $('#enrolment_start_time_hour').val() + ':' + $('#enrolment_start_time_min').val() + ':' + $('#enrolment_start_time_sec').val();
			var EnrolmentEndDate = $('#enrolment_end_period_date').val() + ' ' + $('#enrolment_end_time_hour').val() + ':' + $('#enrolment_end_time_min').val() + ':' + $('#enrolment_end_time_sec').val();
			$.post('apps/admission/ajax.php?action=updateBriefingEdit', {
				'Data': {
					'schoolYearID': $('#schoolYearID').val(),
					'BriefingStartDate': BriefingStartDate,
					'BriefingEndDate': BriefingEndDate,
					'Title': $('#briefing_title').val(),
					'TotalQuota': $('#quota').val(),
					'QuotaPerApplicant': $('#applicantsSeatMax').val(),
					'EnrolmentStartDate': EnrolmentStartDate,
					'EnrolmentEndDate': EnrolmentEndDate,
					'Instruction': $('#instruction').val()
				},
				'BriefingID': $('#BriefingID').val()
			}, function(success){
				$.address.value('/apps/admission/briefing/?sysMsg='+success);
			});
			
			
		    return false;
		});
	},
	
	interview_init: function(lang){
		var sysmsg = lang.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+lang.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		var schoolYearId = $('#selectSchoolYearID').val();
		var classLevelId = $('#selectClassLevelID').val();
		
		$('div.common_table_tool a.tool_print_interview_form').click(function(){
			var hasChecked = false;
			$("input[name='interviewAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#interview_list').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
			}else{
				if(confirm(lang.printallrecordsornot)){
					$("input[name='interviewAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
					$("input[name='interviewAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		
		$('div.Content_tool a.tool_new').click(function(){
			$.address.value('/apps/admission/interview/edit/'+$('select#selectInterviewRound').val()+'/');
			return false;
		});
		$('div.common_table_tool a.tool_edit').click(function(){
			var hasChecked = 0;
			var recordIds = '';
			$("input[name='interviewAry[]']").each(function(i){
				if(this.checked){
					hasChecked ++;
					if(recordIds!='') recordIds+= ',';
					recordIds += this.value;
				}
			});
			
			if(hasChecked == 1){
				$.address.value('/apps/admission/interview/edit?recordID='+recordIds);
				return false;
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var hasApplied = false;
			var recordIds = '';
			$("input[name='interviewAry[]']").each(function(i){
				if(this.checked){
					if($('#applied_'+this.value).val() > 0){
						alert('cannot delete the timeslot that user applied!');
						hasApplied = true;
						return false;
					}
					else{
						hasChecked = true;
						if(recordIds!='') recordIds+= ',';
						recordIds += this.value;
					}
				}
			});
			if(hasApplied){
				return false;
			}
			if(hasChecked){
				
				if(confirm(lang.msg_confirm_delete_email_list)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeInterviewEditByIds', {recordIds:recordIds}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
					});
					return false;
				}
				return false;
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	

		$('div.common_table_tool a.tool_send_email').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			var url = '/apps/admission/applicantslist/compose/?fromModule=interview';
			
			$("input[name='interviewAry\[\]']").each(function(){
				if(this.checked){
					hasChecked = true;
					var id = $(this).attr('id');
					id = id.replace('interview_', 'applicant_ids_');
					if($('#' + id).length){
						applicationIds += $('#' + id).val() + ',';
					}
				}
			});
			applicationIds = applicationIds.slice(0, -1); // trim last ','
			if(hasChecked && applicationIds){
				url += '&applicationIds='+applicationIds;
				$.address.value(url);
			}else{
				alert(lang.selectatleastonerecord2);
			}
			return false;
		});
					
		$('a.tool_export').click(function(){
			var hasChecked = false;
			$("input[name='interviewAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#interview_list').prop("action", "/kis/admission_form/export_interview_list.php").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					$("input[name='interviewAry[]']").each(function(){
						this.checked = true;
					});
					$('#interview_list').prop("action", "/kis/admission_form/export_interview_list.php").submit();
					$("input[name='interviewAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export_interview').click(function(){
			$("input[name='interviewAry[]']").each(function(){
				this.checked = true;
			});
			$('#interview_list').prop("action", "/kis/admission_form/export_interview_information.php").submit();
			$("input[name='interviewAry[]']").each(function(){
				this.checked = false;
			});		
		});
		
		$('a.tool_import').click(function(){
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			$.address.value('/apps/admission/interview/import/'+$('select#selectInterviewRound').val()+'/?schoolYearID='+selectSchoolYearID);
			return false;
		});
		
		$('a.tool_import_by_arragement').click(function(){
			/*if(confirm(lang.importinterviewinfobyarrangementornot)){
				kis.showLoading(); 
				$.post('apps/admission/ajax.php?action=importInterviewInfoByArrangement', {selectSchoolYearID:$('#selectSchoolYearID').val()}, function(data){	
					//parent.$.fancybox.close();
					kis.hideLoading().loadBoard();
					if(data < 1)
						alert("Cannot import correctly. Please try again!");
				});
				return false;
			}
			return false;*/
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			$.address.value('/apps/admission/interview/interviewarrangement/'+$('#selectInterviewRound').val()+'/?schoolYearID='+selectSchoolYearID);
			return false;
		});
		
		$('a.tool_import_applicant').click(function(){
			var selectSchoolYearID = $('#selectSchoolYearID').val();
			$.address.value('/apps/admission/interview/importapplicant/'+$('select#selectInterviewRound').val()+'/?schoolYearID='+selectSchoolYearID);
			return false;
		});
		
		$('select#selectInterviewRound').change(function(){
			$.address.value('/apps/admission/interview/'+(this.value?this.value:0)+'/');
		});
	},
	
	interview_edit_init: function(lang){
		kis.datepicker('#interview_date');
		
		$('#interview_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}
			$('span#warning_interview_date').html(warning);
		});
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/');
			return false;
		});
		$('#interview_edit_form').submit(function(){
			var interview_date = $('#interview_date').val();
			var quota = $('#quota').val();
			var hasInput = false;
			
			//checking the date

			if(!check_date_without_return_msg(document.getElementById('interview_date'))){
				$('span#warning_interview_date').html(lang.invaliddateformat);
				$('#interview_date').focus();
				return false;
			}else if(compareTimeByObjId('interview_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','interview_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				$('span#warning_start_time').html(lang.enddateearlier);
				$('#start_date').focus();
				return false;
			}else if(!is_positive_int(quota)){
				alert(lang.enterpositivenumber);
				$('#quota').focus();
				return false;
			}else if(parseInt(lang.applied) > 0 && parseInt(lang.applied) > parseInt(quota)){
				alert(lang.quotashouldnotlessthanapplied);
				$('#quota').focus();
				return false;
			}
			
			//alert(lang.applied);
			$.post('apps/admission/ajax.php?action=updateInterviewEdit', $(this).serialize(), function(success){
				$.address.value('/apps/admission/interview/'+$('#round').val()+'/?sysMsg='+success);
			});
			
			
		    return false;
		});
	},
	
	interview_details_init: function(lang){
		$('div.Content_tool a.tool_new').click(function(){
			$.address.value('/apps/admission/interview/details_add/'+$('#round').val()+'/'+$('#recordID').val()+'/');
			return false;
		});


		$('div.common_table_tool a.tool_send_email').click(function(){
			var hasChecked = false;
			var applicationIds = '';
			
			var url = '/apps/admission/applicantslist/compose/?fromModule=interview';
			
			$("input[name='applicationAry\[\]']").each(function(){
				if(this.checked){
					hasChecked = true;
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			if(hasChecked){
				url += '&applicationIds='+applicationIds;
				$.address.value(url);
			}else{
				alert(lang.selectatleastonerecord);
			}
			return false;
		});
		
		$('div.common_table_tool a.tool_print_form').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			//$('#application_form').prop("target", "_blank").prop("action", KIS_ROOT+"apps/admission/ajax.php?action=printApplications&schoolYearID="+schoolYearId).submit();
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		
		$('div.common_table_tool a.tool_print_interview_form').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
			}else{
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php?type=interview_form").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var hasApplied = false;
			var recordIds = '';
			$("input[name='applicationAry[]']").each(function(i){
				if(this.checked){
					if($('#applied_'+this.value).val() > 0){
						alert('cannot delete the timeslot that user applied!');
						hasApplied = true;
						return false;
					}
					else{
						hasChecked = true;
						if(recordIds!='') recordIds+= ',';
						recordIds += this.value;
					}
				}
			});
			if(hasApplied){
				return false;
			}
			if(hasChecked){
				
				if(confirm(lang.msg_confirm_delete_email_list)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeApplicationInterviewRecord', {recordIds:recordIds, round:$('#round').val()}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
					});
					return false;
				}
				return false;
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	
	},
	
	interview_details_add_init: function(lang){
	
		$('input#submitBtn').click(function(){
			var hasChecked = false;
			var recordIds = '';
			$("input[name='applicationAry[]']").each(function(i){
				if(this.checked){
					hasChecked = true;
					if(recordIds!='') recordIds+= ',';
					recordIds += this.value;
				}
			});
			if(hasChecked){
				if(confirm(lang.msg_confirm_submit)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=assignApplicantToInterviewSetting', {recordIds:recordIds, round:$('#round').val(), interviewSettingID: $('#recordID').val()}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
						//$.address.value('/apps/admission/interview/details/'+$('#round').val()+'/'+$('#recordID').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
					});
					$.address.value('/apps/admission/interview/details/'+$('#round').val()+'/'+$('#recordID').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
					return false;
				}
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/'+$('#round').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
		});
	},
	
	interview_form_input_init: function(lang){
		var sysmsg = lang.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+lang.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		
		$('select#selectInterviewDate').change(function(){
			$.address.value('/apps/admission/interview/interviewforminput/'+($('select#selectInterviewRound').val()?$('select#selectInterviewRound').val():0)+'/'+this.value+'/');
		});
		
		$('select#selectInterviewRound').change(function(){
			$.address.value('/apps/admission/interview/interviewforminput/'+(this.value?this.value:0)+'/'+$('select#selectInterviewDate').val()+'/');
		});
	},
	
	interview_form_input_details_init: function(lang){
	},
	
	interview_form_input_details_view_init: function(lang){
		$('#submitBtn').click(function(){
			
			var value = $("#interview_form").serializeArray();
			$.post('apps/admission/ajax.php?action=updateInterviewFormResultByIds', value, function(success){
				$.address.value('/apps/admission/interview/interviewforminput_details/'+$('#selectInterviewRound').val()+'/'+$('#selectInterviewDate').val()+'/'+$('#recordID').val()+'/?sysMsg='+success);
			});
			
		    return false;
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/interviewforminput_details/'+$('#selectInterviewRound').val()+'/'+$('#selectInterviewDate').val()+'/'+$('#recordID').val()+'/');
		});
	},
	
	interview_form_input_details_add_init: function(lang){
		$('#submitBtn').click(function(){
			
			var value = $("#interview_form").serializeArray();
			$.post('apps/admission/ajax.php?action=updateInterviewFormResultByIds', value, function(success){
				$.address.value('/apps/admission/interview/interviewforminput_details/'+$('#selectInterviewRound').val()+'/'+$('#selectInterviewDate').val()+'/'+$('#recordID').val()+'/?sysMsg='+success);
			});
			
		    return false;
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/interviewforminput_details/'+$('#selectInterviewRound').val()+'/'+$('#selectInterviewDate').val()+'/'+$('#recordID').val()+'/');
		});
	},
	
	interview_form_builter_init: function(lang){
		var sysmsg = lang.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+lang.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		var schoolYearId = $('#selectSchoolYearID').val();
		var classLevelId = $('#selectClassLevelID').val();
		
		$('div.Content_tool a.tool_new').click(function(){
			$.address.value('/apps/admission/interview/interviewformbuilter_edit/');
			return false;
		});
		$('div.common_table_tool a.tool_edit').click(function(){
			var hasChecked = 0;
			var recordIds = '';
			$("input[name='interviewAry[]']").each(function(i){
				if(this.checked){
					hasChecked ++;
					if(recordIds!='') recordIds+= ',';
					recordIds += this.value;
				}
			});
			
			if(hasChecked == 1){
				$.address.value('/apps/admission/interview/interviewformbuilter_edit/'+recordIds+'/');
				return false;
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var hasApplied = false;
			var recordIds = '';
			$("input[name='interviewAry[]']").each(function(i){
				if(this.checked){
					if($('#applied_'+this.value).val() > 0){
						//alert(lang.cannoteditordeleteinterviewform);
						if(confirm(lang.cannoteditordeleteinterviewform)){
							hasChecked = true;
							if(recordIds!='') recordIds+= ',';
							recordIds += this.value;
						}
						else{
							hasApplied = true;
							return false;
						}
					}
					else{
						hasChecked = true;
						if(recordIds!='') recordIds+= ',';
						recordIds += this.value;
					}
				}
			});
			if(hasApplied){
				return false;
			}
			if(hasChecked){
				
				if(confirm(lang.msg_confirm_delete_email_list)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeInterviewFormBuilterEditByIds', {recordIds:recordIds}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
					});
					return false;
				}
				return false;
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	
	},
	
	interview_form_builter_edit_init: function(lang){
		kis.datepicker('#start_date');	
		kis.datepicker('#end_date');
		
		// -- for timeslot checking [start]
		$('#start_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_start_date').html(warning);
		});
		$('#start_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_start_date').html(warning);
		});
		
		$('#end_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_end_date').html(warning);
		});
		$('#end_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_end_date').html(warning);
		});
		// -- for timeslot checking [end]
		
		$('#submitBtn').click(function(){
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();
			
			if(!$('[name="selectClassLevelID[]"] option:selected').val()){
				alert(lang.selectatleastoneclass);
				return false;
			}
			
			if($('#title').val().trim()==''){
				alert(lang.entertitle);
				$('#title').focus();
				return false;
			}
			
			if($('#sortable').find('li').length <= 0){
				alert(lang.enterinputinterviewquestion);
				return false;
			}
			
			if(start_date!=''&&end_date!=''){
				if(!check_date_without_return_msg(document.getElementById('start_date'))){
					$('span#warning_start_date').html(lang.invaliddateformat);
					$('#start_date').focus();
					return false;
				}else if(!check_date_without_return_msg(document.getElementById('end_date'))){
					$('span#warning_end_date').html(lang.invaliddateformat);
					$('#end_date').focus();
					return false;
				}else if(compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
					$('span#warning_start_date').html(lang.enddateearlier);
					$('#start_date').focus();
					return false;
				}
			}else{
				// miss either one of them
				if(!confirm(lang.emptyapplicationdatewarning)){
						return false;
				}
			}
			
			$('span#warning_start_date').html();
			$('span#warning_end_date').html();
			
			$('#form_tools_div').find('input').prop('disabled', true);
			$('#form_tools_div').find('textarea').prop('disabled', true);
			var value = $("#interview_form").serializeArray();
			$.post('apps/admission/ajax.php?action=updateInterviewFormBuilterEditByIds', value, function(success){
				$.address.value('/apps/admission/interview/interviewformbuilter/?sysMsg='+success);
			});
			
		    return false;
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/interviewformbuilter/');
		});
	},
	
	interview_form_result_init: function(lang){
		var schoolYearId = $('#selectSchoolYearID').val();
		
		$('select#selectFormQuestionID').change(function(){
			$('select#selectFormAnswerID').val('');
		});
		
		$('div.common_table_tool a.tool_print').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			//$('#application_form').prop("target", "_blank").prop("action", KIS_ROOT+"apps/admission/ajax.php?action=printApplications&schoolYearID="+schoolYearId).submit();
			$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.printallrecordsornot)){
					$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});
					$('#application_form').prop("target", "_blank").prop("action", "/kis/admission_form/print_form.php").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		$('a.tool_export').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
			$('#application_form').prop("action", "/kis/admission_form/export_interview_result.php").submit();
			}else{
				//alert(lang.selectatleastonerecord);
				if(confirm(lang.exportallrecordsornot)){
					/*$("input[name='applicationAry[]']").each(function(){
						this.checked = true;
					});*/
					$('#application_form').prop("action", "/kis/admission_form/export_interview_result.php").submit();
					$("input[name='applicationAry[]']").each(function(){
						this.checked = false;
					});
				}
			}
			return false;
		});
		
		$('select#selectClassLevelID').change(function(){
			$.address.value('/apps/admission/interview/interviewformresult/'+this.value+'/'+$('select#selectInterviewRound').val()+'/');
		});
		
		$('select#selectInterviewRound').change(function(){
			$.address.value('/apps/admission/interview/interviewformresult/'+$('select#selectClassLevelID').val()+'/'+(this.value?this.value:0)+'/');
		});
		
		var open_fancy_box = function(type){
		    var page = '#edit_box';
			if(type == 'edit_class'){
				page = '#edit_class';
			}
			$.fancybox( {
				'fitToView'    	 : false,	
				'autoDimensions'    : false,
				'autoScale'         : false,
				'autoSize'          : false,
				'width'             : 600,
				'height'            : 450,
				'padding'			 : 20,

				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic',
				'href' : page				 
			});	 
		}
		$('div.common_table_tool a.tool_set').click(function(){
			var hasChecked = false;
			$("input[name='applicationAry[]']").each(function(){
				if(this.checked){
					hasChecked = true;
					return;
				}
			});
			if(hasChecked){
				open_fancy_box();
			}else{
				alert(lang.selectatleastonerecord);
			}
			return false;
		});
		
		$('form#application_status_form').submit(function(){
			var applicationIds = '';
			var status = $('#status').val();
			$("input[name='applicationAry[]']").each(function(i){
				if(this.checked){
					if(applicationIds!='') applicationIds+= ',';
					applicationIds += this.value;
				}
			});
			kis.showLoading();
			$.post('apps/admission/ajax.php?action=updateApplicationStatusByIds', {applicationIds:applicationIds,status:status}, function(data){	
				parent.$.fancybox.close();
				kis.hideLoading().loadBoard();
			});
			return false;
		});
	},
	
	interview_user_settings_init: function(lang){
		var sysmsg = lang.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+lang.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		var schoolYearId = $('#selectSchoolYearID').val();
		var classLevelId = $('#selectClassLevelID').val();
		
		$('div.Content_tool a.tool_new').click(function(){
			$.address.value('/apps/admission/interview/interviewusersettings_new/');
			return false;
		});
		$('div.common_table_tool a.tool_delete').click(function(){
			var hasChecked = false;
			var userIds = '';
			$("input[name='userAry[]']").each(function(i){
				if(this.checked){
					hasChecked = true;
					if(userIds!='') userIds+= ',';
					userIds += this.value;
				}
			});
			if(hasChecked){
				
				if(confirm(lang.msg_confirm_delete_email_list)){
					kis.showLoading(); 
					$.post('apps/admission/ajax.php?action=removeInterviewUserSettingsByIds', {userIds:userIds}, function(data){	
						//parent.$.fancybox.close();
						kis.hideLoading().loadBoard();
					});
					return false;
				}
				return false;
			}
			else{
				alert(lang.msg_selectatleastonerecord);
				return false;
			}
		});	
	},
	
	interview_user_settings_new_init: function(lang){
		
		$('#submitBtn').click(function(){
			var userIds = '';
			$(".mail_to_list input[name='recipients[]']").each(function(i){
				if(userIds!='') userIds+= ',';
				userIds += this.value;
			});
			//alert(userIds);
			if(!userIds){
				alert(lang.selectatleastoneuser);
				return false;
			}
			$.post('apps/admission/ajax.php?action=updateInterviewUserSettingsByIds', {userIds:userIds}, function(success){
				$.address.value('/apps/admission/interview/interviewusersettings/?sysMsg='+success);
			});
			
		    return false;
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/interviewusersettings/');
		});
		
		$('.mail_select_user').submit(function(){
	    
		    var form = this;
		    var exclude_list = $('.mail_to_list .mail_user input').map(function(){
			return $(this).val()
		    }).get().join(',');
		    $('.mail_select_user input[name="exclude_list"]').val(exclude_list);
		    
		    kis.showLoading();
		    $.post('apps/admission/ajax.php?action=searchinterviewusers', $(this).serialize(), function(res){
			kis.hideLoading();
			if (res.count<=0 || res.count>500){
			    $('.mail_select_user .mail_select_all').hide();
	
			}else{
			    $('.mail_select_user .mail_select_all').show();
			    
			}
			$('.mail_select_user .search_results').html(res.ui).show();
	
					
		    }, 'json');
		    return false;
		});
		
		$('.mail_select_user .search_results').on('click','.btn_add', function(){
		    
		    $('.mail_to_list').append($(this).closest('.mail_user'));
		    //resetAutoSave();
		    return false;
		});
		
		$('.mail_select_user .mail_select_all').click(function(){
		    
		    $('.mail_select_user .search_results .btn_add').click();
		    //resetAutoSave();
		    return false;
		});
		
		$('.mail_select_user .formsubbutton').click(function(){
		    
		    $('.mail_select_user').css({'margin-left': 0});
		    $('.mail_to_list').css({'border-color': 'black'});
		    return false;
		});
		
		$('.mail_to_list').on('click', '.mail_user .btn_remove', function(){
		    
		    $('.mail_select_user .search_results').append($(this).closest('.mail_user'));
		    //resetAutoSave();
		    return false;
		    
		});
	
		$('.mail_to_btn .btn_select_ppl').click(function(){
	    
		    var target = $(this).find('.target').html();
		    
		    $('.mail_select_user').css({'margin-left': -380});
		    $('.mail_select_user input[name="target"]').val(target);
		    $('.mail_select_user .search_results').empty().hide();
		    $('.mail_select_user .mail_select_all').hide();
		    $('.mail_to_list').css({'border-color': 'orange'});
		    
		    return false; 
		    
		});
	},
	
	update_status_init: function(msg){
		$("input[name=applicationno]").focus();
		$("input[name=applicationno]").keypress(function(event){
		    if(event.keyCode == 13){
		    	if($('#selectStatus').val() == ""){
		    		alert(msg.msg_selectstatus);
		    		return false;
		    	}
		    	else if(this.value.trim() == ""){
		    		alert(msg.msg_enterapplicationno);
		    		return false;
		    	} 
		    	var status = $('#selectStatus').val();
		    	var applicationIds = this.value.trim();
		    	//alert(status+applicationIds);
		    	$.post('apps/admission/ajax.php?action=updateApplicationStatusByApplicationNos', {applicationIds:applicationIds,status:status}, function(data){	
					if(data == "WrongApplicationNo"){
						alert(msg.msg_worngapplicationno);
						return false;
					}
					else if(data == "UpdateUnsuccess"){
						aleart(msg.msg_updateunsuccess);
						return false;
					}
					$.address.value('/apps/admission/updatestatus/?selectStatus='+status);
					kis.loadBoard();
				});
				return false;
		    }
		  });
		  

	},
	
	attachment_download_init: function(msg){
	
		var submitCheckForm = function(){
			var FromID = $("#FromID").val().trim();
			var ToID = $("#ToID").val().trim();
			
			if(FromID=="")
			{	
				alert(msg.pleaseinputApplicationNo);
				$("#FromID").focus();
				valid = false;
			}
			else if(ToID=="")
			{	
				alert(msg.pleaseinputApplicationNo);
				$("#ToID").focus();
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			if($('input:radio[name=mode]:checked').val() == 'zip'){
				$('#downloadForm').attr("target","_blank");
				$('#downloadForm').attr("action","./apps/admission/templates/applicantslist/download.php");
			}
			else{
				$('#downloadForm').attr("target","_blank");
				$('#downloadForm').attr("action","./apps/admission/templates/applicantslist/print_attachment.php");
			}
			
			$('#downloadForm').submit();
		};
		
		$('input#submitBtn').click(submitCheckForm);
	
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
	},
	
	import_admission_init: function(msg){
	
		//for upload import file
		jQuery.extend({
		    handleError: function( s, xhr, status, e ) {
		        // If a local callback was specified, fire it
		        if ( s.error )
		            s.error( xhr, status, e );
		        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
		        else if(xhr.responseText)
		            console.log(xhr.responseText);
		    }
		});
		var options = {
			complete: function(response) 
			{	
				var data = response.responseText.substring(19);
				data = data.substring(0,data.length-7);
				var strArr = data.split(",");
				
				$('#totalRecordResult').show();
				if(strArr[1] == null){
					$('#totalRecordResult').hide();
					$("#message").html(strArr[0]);
				}
				else{	
					$("#validTotal").html(strArr[0]);
					if(strArr[1] > 0){
						strArr[1] = '<font color="red">'+strArr[1]+'</font>';
						strArr[2] = $('<div />').html(strArr[2]).text();
						$("#message").html(strArr[2]?strArr[2]:'');
					}
					else{
						$("#message").html('');
						$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=importAdmissionInfo');
						$("#importForm").ajaxForm(options2);
					}
					$("#invalidTotal").html(strArr[1]);
				}
				$('.step1').hide();
				$('.step2').show();
				if(strArr[1] != 0)
					$('#submitBtn').hide();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
		var options2 = {
			complete: function(response) 
			{	
				$('.step2').hide();
					$('.step3').show();
					$("#importResult").html(response.responseText);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
     	$("#importForm").ajaxForm(options);

		var submitCheckForm = function(){
			var filename = $("#userfile").val()
			var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
			if(fileext!=".csv" && fileext!=".txt")
			{	
				alert(msg.pleaseselectcsvortxt);
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$(this).submit();
		};
		
		$('input#nextBtn').click(submitCheckForm);

		$('input#backBtn').click(function(){
			$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=checkImportAdmissionInfo');
			$("#importForm").ajaxForm(options);
			$('.step1').show();
			$('.step2').hide();
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
		$('input#doneBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
		$('input#submitBtn').click(function(){
			$(this).submit();
		});
	},
	
	import_interview_settings_init: function(msg){
	
		//for upload import file
		jQuery.extend({
		    handleError: function( s, xhr, status, e ) {
		        // If a local callback was specified, fire it
		        if ( s.error )
		            s.error( xhr, status, e );
		        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
		        else if(xhr.responseText)
		            console.log(xhr.responseText);
		    }
		});
		var options = {
			complete: function(response) 
			{	
				var data = response.responseText.substring(19);
				data = data.substring(0,data.length-7);
				var strArr = data.split(",");
				
				$('#totalRecordResult').show();
				if(strArr[1] == null){
					$('#totalRecordResult').hide();
					$("#message").html(strArr[0]);
				}
				else{	
					$("#validTotal").html(strArr[0]);
					if(strArr[1] > 0){
						strArr[1] = '<font color="red">'+strArr[1]+'</font>';
						strArr[2] = $('<div />').html(strArr[2]).text();
						$("#message").html(strArr[2]?strArr[2]:'');
					}
					else{
						$("#message").html('');
						$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=importInterviewSettingsInfo');
						$("#importForm").ajaxForm(options2);
					}
					$("#invalidTotal").html(strArr[1]);
				}
				$('.step1').hide();
				$('.step2').show();
				if(strArr[1] != 0)
					$('#submitBtn').hide();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
		var options2 = {
			complete: function(response) 
			{	
				$('.step2').hide();
					$('.step3').show();
					$("#importResult").html(response.responseText);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
     	$("#importForm").ajaxForm(options);

		var submitCheckForm = function(){
			var filename = $("#userfile").val()
			var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
			if(fileext!=".csv" && fileext!=".txt")
			{	
				alert(msg.pleaseselectcsvortxt);
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$(this).submit();
		};
		
		$('input#nextBtn').click(submitCheckForm);

		$('input#backBtn').click(function(){
			$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=checkImportInterviewSettingsInfo');
			$("#importForm").ajaxForm(options);
			$('.step1').show();
			$('.step2').hide();
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/'+$('#round').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
		});
		
		$('input#doneBtn').click(function(){
			$.address.value('/apps/admission/interview/'+$('#round').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
		});
		
		$('input#submitBtn').click(function(){
			$(this).submit();
		});
	},
	
	
	import_interview_applicant_init: function(msg){
		
		//for upload import file
		jQuery.extend({
			handleError: function( s, xhr, status, e ) {
				// If a local callback was specified, fire it
				if ( s.error )
					s.error( xhr, status, e );
				// If we have some XML response text (e.g. from an AJAX call) then log it in the console
				else if(xhr.responseText)
					console.log(xhr.responseText);
			}
		});
		var options = {
				complete: function(response) 
				{	
					var data = response.responseText.substring(19);
					data = data.substring(0,data.length-7);
					var strArr = data.split(",");
					
					$('#totalRecordResult').show();
					if(strArr[1] == null){
						$('#totalRecordResult').hide();
						$("#message").html(strArr[0]);
					}
					else{	
						$("#validTotal").html(strArr[0]);
						if(strArr[1] > 0){
							strArr[1] = '<font color="red">'+strArr[1]+'</font>';
							strArr[2] = $('<div />').html(strArr[2]).text();
							$("#message").html(strArr[2]?strArr[2]:'');
						}
						else{
							$("#message").html('');
							$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=importInterviewApplicant');
							$("#importForm").ajaxForm(options2);
						}
						$("#invalidTotal").html(strArr[1]);
					}
					$('.step1').hide();
					$('.step2').show();
					if(strArr[1] != 0)
						$('#submitBtn').hide();
					$('#module_page .board_main_content > .board_loading_overlay').remove();
				},
				error: function()
				{
					$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
					//$('#PreviewSubject').html('');
					//$('#PreviewMessage').html('');
					$('.step1').hide();
					$('.step2').show();
					$('#module_page .board_main_content > .board_loading_overlay').remove();
				}
		};
		var options2 = {
				beforeSubmit: function()
				{
					return confirm(msg.confirmdeletedata);
				},
				complete: function(response) 
				{	
					$('.step2').hide();
					$('.step3').show();
					$("#importResult").html(response.responseText);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
				},
				error: function()
				{
					$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
					//$('#PreviewSubject').html('');
					//$('#PreviewMessage').html('');
					$('.step1').hide();
					$('.step2').show();
					$('#module_page .board_main_content > .board_loading_overlay').remove();
				}
		};
		$("#importForm").ajaxForm(options);
		
		var submitCheckForm = function(){
			var filename = $("#userfile").val()
			var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
			if(fileext!=".csv" && fileext!=".txt")
			{	
				alert(msg.pleaseselectcsvortxt);
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$(this).submit();
		};
		
		$('input#nextBtn').click(submitCheckForm);
		
		$('input#backBtn').click(function(){
			$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=checkImportInterviewApplicant');
			$("#importForm").ajaxForm(options);
			$('.step1').show();
			$('.step2').hide();
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/'+$('#round').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
		});
		
		$('input#doneBtn').click(function(){
			$.address.value('/apps/admission/interview/'+$('#round').val()+'/?selectSchoolYearID='+$('input#schoolYearID').val());
		});
		
		$('input#submitBtn').click(function(){
			$(this).submit();
		});
	},
	
	import_interview_arrangement_init: function(msg){
	
		var submitCheckForm = function(){
			var filename = $("#userfile").val()
			var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
			if(fileext!=".csv" && fileext!=".txt")
			{	
				alert(msg.pleaseselectcsvortxt);
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$(this).submit();
		};
		
		$('input#nextBtn').click(submitCheckForm);

		$('input#backBtn').click(function(){
			$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=checkImportInterviewSettingsInfo');
			$("#importForm").ajaxForm(options);
			$('.step1').show();
			$('.step2').hide();
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/interview/'+$('#round').val()+'/?selectSchoolYearID='+$('input#selectSchoolYearID').val());
		});
		
		$('input#submitBtn').click(function(){
			if(!$('[name="selectStatus[]"] option:selected').val()){
				alert(msg.pleaseselectstatus);
				return false;
			}
			if($('[name="classLevelIds\[\]"]').length > 0 && $('[name="classLevelIds\[\]"]:checked').length == 0){
				alert(msg.pleaseselectclasslevel);
				return false;
			}
			if((currentSchool.match(/^creative.*$/)) || confirm(msg.importinterviewinfobyarrangementornot)){
				kis.showLoading(); 
				var value = $("#importForm").serializeArray();
				$.post('apps/admission/ajax.php?action=importInterviewInfoByArrangement', value, function(data){	
					//parent.$.fancybox.close();
					$.address.value('/apps/admission/interview/'+$("#round").val()+'/?sysMsg='+data);
					setTimeout(function(){
						kis.hideLoading();
					}, 1000);
					if(data < 1)
						alert("Cannot import correctly. Please try again!");
				});
				return false;
			}
			return false;
		});
	},
	
	import_interview_init: function(msg){
	
		//for upload import file
		jQuery.extend({
		    handleError: function( s, xhr, status, e ) {
		        // If a local callback was specified, fire it
		        if ( s.error )
		            s.error( xhr, status, e );
		        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
		        else if(xhr.responseText)
		            console.log(xhr.responseText);
		    }
		});
		var options = {
			complete: function(response) 
			{	
				var data = response.responseText.substring(19);
				data = data.substring(0,data.length-7);
				var strArr = data.split(",");
				
				$('#totalRecordResult').show();
				if(strArr[1] == null){
					$('#totalRecordResult').hide();
					$("#message").html(strArr[0]);
				}
				else{	
					$("#validTotal").html(strArr[0]);
					if(strArr[1] > 0){
						strArr[1] = '<font color="red">'+strArr[1]+'</font>';
						strArr[2] = $('<div />').html(strArr[2]).text();
						$("#message").html(strArr[2]?strArr[2]:'');
					}
					else{
						$("#message").html('');
						$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=importInterviewInfo');
						$("#importForm").ajaxForm(options2);
					}
					$("#invalidTotal").html(strArr[1]);
				}
				$('.step1').hide();
				$('.step2').show();
				if(strArr[1] != 0)
					$('#submitBtn').hide();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
		var options2 = {
			complete: function(response) 
			{	
				$('.step2').hide();
					$('.step3').show();
					$("#importResult").html(response.responseText);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
     	$("#importForm").ajaxForm(options);

		var submitCheckForm = function(){
			var filename = $("#userfile").val()
			var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
			if(fileext!=".csv" && fileext!=".txt")
			{	
				alert(msg.pleaseselectcsvortxt);
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$(this).submit();
		};
		
		$('input#nextBtn').click(submitCheckForm);

		$('input#backBtn').click(function(){
			$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=checkImportInterviewInfo');
			$("#importForm").ajaxForm(options);
			$('.step1').show();
			$('.step2').hide();
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
		$('input#doneBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
		$('input#submitBtn').click(function(){
			$(this).submit();
		});
	},
	import_briefing_init: function(msg){
		
		//for upload import file
		jQuery.extend({
		    handleError: function( s, xhr, status, e ) {
		        // If a local callback was specified, fire it
		        if ( s.error )
		            s.error( xhr, status, e );
		        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
		        else if(xhr.responseText)
		            console.log(xhr.responseText);
		    }
		});
		var options = {
			complete: function(response) 
			{	
				var data = response.responseText.substring(19);
				data = data.substring(0,data.length-7);
				var strArr = data.split(",");
				
				$('#totalRecordResult').show();
				if(strArr[1] == null){
					$('#totalRecordResult').hide();
					$("#message").html(strArr[0]);
				}
				else{	
					$("#validTotal").html(strArr[0]);
					if(strArr[1] > 0){
						strArr[1] = '<font color="red">'+strArr[1]+'</font>';
						strArr[2] = $('<div />').html(strArr[2]).text();
						$("#message").html(strArr[2]?strArr[2]:'');
					}
					else{
						$("#message").html('');
						$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=importBriefingInfo');
						$("#importForm").ajaxForm(options2);
					}
					$("#invalidTotal").html(strArr[1]);
				}
				$('.step1').hide();
				$('.step2').show();
				if(strArr[1] != 0)
					$('#submitBtn').hide();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
		var options2 = {
			complete: function(response) 
			{	
				$('.step2').hide();
					$('.step3').show();
					$("#importResult").html(response.responseText);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
			},
			error: function()
			{
				$("#message").html("<font color='red'> ERROR: unable to upload files</font>");
				//$('#PreviewSubject').html('');
				//$('#PreviewMessage').html('');
				$('.step1').hide();
				$('.step2').show();
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		};
     	$("#importForm").ajaxForm(options);

		var submitCheckForm = function(){
			var filename = $("#userfile").val()
			var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
			if(fileext!=".csv" && fileext!=".txt")
			{	
				alert(msg.pleaseselectcsvortxt);
				valid = false;
			}
			else
				valid = true;
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$(this).submit();
		};
		
		$('input#nextBtn').click(submitCheckForm);

		$('input#backBtn').click(function(){
			$('#importForm').get(0).setAttribute('action', './apps/admission/ajax.php?action=checkImportBriefingInfo');
			$("#importForm").ajaxForm(options);
			$('.step1').show();
			$('.step2').hide();
		});
		
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
		$('input#doneBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
		$('input#submitBtn').click(function(){
			$(this).submit();
		});
	},
	sendmail_compose_init: function(msg){		
		var sysmsg = msg.returnmsg;
		if(sysmsg!=''){
			var returnmsg = sysmsg.split('|=|')[1];
			returnmsg += '<a href="javascript:void(0);" onclick="$(\'div#system_message\').hide();" >['+msg.close+']</a>';
			var msgclass = sysmsg.split('|=|')[0]==1?'system_success':'system_alert';
			$('div#system_message').removeClass();
			$('div#system_message').addClass(msgclass).show();
			$('div#system_message span').html(returnmsg);
			setTimeout(function(){
				$('#system_message').hide();
			}, 3000);	
		}
		
		var editor = kis.editor('Message');
		
		$('input#IsAcknowledgeYes').click(function(){
			$('tr#AcknowledgeRow').show();
		});
		
		$('input#IsAcknowledgeNo').click(function(){
			$('tr#AcknowledgeRow').hide();
		});
		
		var deleteUploadFile = function(obj){
			$(obj).parent().remove();
		};
		
		var deleteAttachment = function(obj){
			if(confirm(msg.confirm_delete_email_attachment)){
				$(obj).parent().remove();
			}
		};
		
		$('a[name="AttachmentRemoveBtn\\[\\]"]').click(function(){
			deleteAttachment(this);
		});
		
		$('a[name="DeleteFileUpload\\[\\]"]:last').click(function(){
			deleteUploadFile(this);
		});
		
		$('#add_more_attachment').click(function(){
			var file_upload_count = parseInt($('input#FileUploadCount').val());
			file_upload_count += 1;
			var input_file = '<div><input type="file" name="FileUpload_'+file_upload_count+'" /><a href="javascript:void(0);" class="btn_remove" name="DeleteFileUpload[]" ></a></div>';
			$('div#FileUploadDiv').append(input_file);
			$('a[name="DeleteFileUpload\\[\\]"]:last').click(function(){
				deleteUploadFile(this);
			});
			$('input#FileUploadCount').val(file_upload_count);
		});
		
		$('#TemplateID').change(function(){
			var template_id = $(this).val();
			var subject_val = $.trim($('#Subject').val());
			var message_val = '';
			if(editor){
				message_val = FCKeditorAPI.GetInstance('Message').GetXHTML( false );
				message_val = $.trim(message_val.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
			}else{
				message_val = $('#Message').val();
			}
			if(subject_val != '' || message_val != ''){
				if(!confirm(msg.confirm_change_email_template))
				{
					$('#TemplateID').val('');
					return;
				}
			}
			if(template_id == ''){
				$('#Subject').val('');
				if(editor){
					FCKeditorAPI.GetInstance('Message').SetHTML('');
				}else{
					$('#Message').val('');
				}
				$('#TemplateAttachmentDiv').html('');
			}else{
				$.get(
					'apps/admission/ajax.php?action=getEmailTemplateJsonRecord&TemplateID='+template_id,
					{},
					function(data){
						var obj;
						if(JSON){
							obj = JSON.parse(data);
						}else{
							eval('var obj='+data);
						}
						var title = decodeURIComponent(obj['Title']);
						var content = decodeURIComponent(obj['Content']);
						var attachment_html = decodeURIComponent(obj['AttachmentHtml']);
						$('#Subject').val(title);
						if(editor){
							FCKeditorAPI.GetInstance('Message').SetHTML(content);
						}else{
							$('#Message').val(content);
						}
						$('#TemplateAttachmentDiv').html(attachment_html);
						
						$('div#TemplateAttachmentDiv a[name="AttachmentRemoveBtn\\[\\]"]').click(function(){
							deleteAttachment(this);
						});
					}
				);
			}
		});
		
		var submitCheckForm = function(){
			var valid = true;
			var warn_subject = $('#WarnSubject');
			var warn_message = $('#WarnMessage');
			var warn_acknowledgemessage = $('#WarnAcknowledgeMessage');
			var mail_subject = $.trim($('input#Subject').val());
			var mail_message = '';
			var is_acknowledge = $('input#IsAcknowledgeYes').is(':checked');
			var acknowledge_message = $.trim($('#AcknowledgeMessage').val());
			var trim_mail_message = '';
			if(editor){
				mail_message = FCKeditorAPI.GetInstance('Message').GetXHTML( false );
				trim_mail_message = $.trim(mail_message.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
			}else{
				mail_message = $('#Message').val();
				trim_mail_message = $.trim(mail_message);
			}
			if(mail_subject == ''){
				valid = false;
				warn_subject.show();
			}else{
				warn_subject.hide();
			}
			if(trim_mail_message == ''){
				valid = false;
				warn_message.show();
			}else{
				warn_message.hide();
			}
			if(is_acknowledge && acknowledge_message == '')
			{
				valid = false;
				warn_acknowledgemessage.show();
			}else{
				warn_acknowledgemessage.hide();
			}
			
			if(!valid) return;
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			$.post(
				'apps/admission/ajax.php?action=getReplacedMailText',
				{
					'text': encodeURIComponent(mail_subject),
					'recordID' : $('#recordID').val(),
					'applicationIds[]' : $('#applicationIds').val().split(',')
				},
				function(replacedSubject){
					$('#PreviewSubject').html(replacedSubject);
					//$('#PreviewSubject').html(mail_subject);
					
					$.post(
						'apps/admission/ajax.php?action=getReplacedMailText',
						{
							'text': encodeURIComponent(mail_message),
							'recordID' : $('#recordID').val(),
							'applicationIds[]' : $('#applicationIds').val().split(',')
						},
						function(replacedMessage){
							//$('#PreviewMessage').html(replacedMessage);
							$('#PreviewMessage').html(mail_message);
							$('#PreviewOptions').change();
							if($('#PreviewAcknowledgeRow').length > 0){
								if(is_acknowledge){
									$('#PreviewAcknowledgeYes').show();
									$('#PreviewAcknowledgeNo').hide();
								}else{
									$('#PreviewAcknowledgeYes').hide();
									$('#PreviewAcknowledgeNo').show();
								}
							}
							$('.step1').hide();
							$('.step2').show();
							$('#module_page .board_main_content > .board_loading_overlay').remove();
						}
					);
				}
			);
			
		};
		
		$('input#nextBtn').click(submitCheckForm);
		
		$('input#backBtn').click(function(){
			$('.step1').show();
			$('.step2').hide();
			if($('input#IsAcknowledgeYes').is(':checked')){
				$('#AcknowledgeRow').show();
			}else{
				$('#AcknowledgeRow').hide();
			}
		});
		
		$('input#submitBtn').click(function(){
		
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			if(editor){
				editor.finish();
			}
			
			var Obj = document.getElementById("MailForm");
			Obj.target = 'FileUploadIframe'; 
			Obj.encoding = "multipart/form-data";
			Obj.method = 'post'; 
			Obj.action = "apps/admission/ajax.php?action=uploadEmailAttachments";
			Obj.submit();
			/*
			$.post(
				'apps/admission/ajax.php?action=sendMails&SendTarget=1',
				$('form#MailForm').serialize(),
				function(data){
					$('.step2').hide();
					$('.step3').show();
					//$.address.value('/apps/admission/emaillist/main/');
					$('#ComposeMailTable').hide();
					var result_table = $('#ComposeResultTable');
					if(result_table.length > 0){
						result_table.remove();
					}
					$('#ComposeMailTable').after(data);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
				}
			);
			*/
		});
		
		$('input#resendBtn').click(function(){
			$.fancybox( {
				'fitToView'    	 : true,	
				'autoDimensions'    : true,
				'autoScale'         : false,
				'autoSize'          : true,
				//'width'             : 600,
				//'height'            : 450,
				'padding'			 : 20,
				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic',
				'href': '#ResendOptionLayer'
			});
		});
		
		$('input#printBtn').click(function(){
			$.fancybox( {
				'fitToView'    	 : true,	
				'autoDimensions'    : true,
				'autoScale'         : false,
				'autoSize'          : true,
				//'width'             : 600,
				//'height'            : 450,
				'padding'			 : 20,
				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic',
				'href': '#PrintOptionLayer'
			});
		});
		
		$('#SendTarget').change(function(){
			if($('#SendTarget').val() == 5){
				$('#DivSelectApplicants').show();
			}
			else{
				$('#DivSelectApplicants').hide();
			}
		});
		
		$('#SendTargetForPrint').change(function(){
			if($('#SendTargetForPrint').val() == 5){
				$('#DivSelectApplicantsForPrint').show();
			}
			else{
				$('#DivSelectApplicantsForPrint').hide();
			}
		});
		
		$('input#confirmResendBtn').click(function(){
			
			if($('#SendTarget').val() == 5){
				var checkboxes = document.getElementsByName('applicant_id[]');
				var vals = "";
				for (var i=0, n=checkboxes.length;i<n;i++) {
				  if (checkboxes[i].checked) 
				  {
				  	vals = 1;
				  	break;
				  }
				}
				if(vals == ""){
					alert(msg.selectatleastoneapplicant);
					return false;
				}
			}
			
			$.fancybox.close();
			
			$('#module_page .board_main_content').append($('.board_loading_overlay').clone().show());
			if(editor){
				editor.finish();
			}
			
			var Obj = document.getElementById("MailForm");
			Obj.target = 'FileUploadIframe'; 
			Obj.encoding = "multipart/form-data";
			Obj.method = 'post'; 
			Obj.action = "apps/admission/ajax.php?action=resendUploadEmailAttachments";
			Obj.submit();
			/*
			$.post(
				'apps/admission/ajax.php?action=sendMails&SendTarget='+$('#SendTarget').val(),
				$('form#MailForm').serialize(),
				function(data){
					$('.step2').hide();
					$('.step3').show();
					//$.address.value('/apps/admission/emaillist/main/');
					$('#ComposeMailTable').hide();
					var result_table = $('#ComposeResultTable');
					if(result_table.length > 0){
						result_table.remove();
					}
					$('#ComposeMailTable').after(data);
					$('#module_page .board_main_content > .board_loading_overlay').remove();
				}
			);
			*/
		});
		
		$('input#confirmPrintBtn').click(function(){
		
			var vals = "";
			
			if($('#SendTargetForPrint').val() == 5){
				var checkboxes = document.getElementsByName('applicant_id[]');
				
				for (var i=0, n=checkboxes.length;i<n;i++) {
				  if (checkboxes[i].checked) 
				  {
				  	vals += ","+checkboxes[i].value;
				  }
				}
				if(vals == ""){
					alert(msg.selectatleastoneapplicant);
					return false;
				}
				vals = '?applicant_id='+vals.substring(1);
			}
			
			unselectAllApplicants();
			
			$.fancybox.close();
			
			$('#MailForm').prop("target", "_blank").prop("action", "/kis/admission_form/print_email.php"+vals).submit();
		});
		
		$('input#cancelBtn').click(function(){
			if($('#fromModule').val() == 'interview'){
				$.address.value('/apps/admission/interview/');
			}else{
				$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
			}
		});
		
		$('input#editCancelBtn').click(function(){
			//var schoolYearId = $('#schoolYearID').val();
			//var classLevelId = $('#classLevelID').val();
			//$.address.value('/apps/admission/emaillist/?selectSchoolYearID='+schoolYearId+'&selectClassLevelID='+classLevelId);
			$.address.value('/apps/admission/emaillist/'+$('input#schoolYearID').val()+'/');
		});
		
		$('input#editDoneBtn').click(function(){
			//var schoolYearId = $('#schoolYearID').val();
			//var classLevelId = $('#classLevelID').val();
			//$.address.value('/apps/admission/emaillist/?selectSchoolYearID='+schoolYearId+'&selectClassLevelID='+classLevelId);
			$.address.value('/apps/admission/emaillist/'+$('input#schoolYearID').val()+'/');
		});
		
		$('input#doneBtn').click(function(){
			if($('#fromModule').val() == 'interview'){
				$.address.value('/apps/admission/interview/');
			}else{
				$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
			}
		});
		
		$('input#editBackBtn').click(function(){
			$('.step1').hide();
			$('.step2').show();
		});
		
		$('input#editSubmitBtn').click(submitCheckForm);
		
		$('#ViewStudentBtn').click(function(){
			/*
			$.post(
				'apps/admission/ajax.php?action=getMailRecipient',
				{
				},
				function(data){
					$.fancybox(data, {
						'fitToView'    	 : false,	
						'autoDimensions'    : false,
						'autoScale'         : false,
						'autoSize'          : false,
						'width'             : 600,
						'height'            : 450,
						'padding'			 : 20,
						'transitionIn'      : 'elastic',
						'transitionOut'     : 'elastic' 
					});	
				}
			);
			*/
			$.fancybox( {
				'fitToView'    	 : true,	
				'autoDimensions'    : true,
				'autoScale'         : false,
				'autoSize'          : true,
				//'width'             : 600,
				//'height'            : 450,
				'padding'			 : 20,
				'transitionIn'      : 'elastic',
				'transitionOut'     : 'elastic',
				'href': '#ViewReceiverLayer'
			});
		});
		
		$('#PreviewOptions').change(function(){
			var applicationID = $("#PreviewOptions option:selected").val();
			var mail_message = $("#PreviewMessage").html();
			var mail_subject = $("#PreviewSubject").html();
		
			$.post(
				'apps/admission/ajax.php?action=getReplacedMailText',
				{
					'text': encodeURIComponent(mail_message),
					'recordID' : $('#recordID').val(),
					'applicationIds' : applicationID
				},
				function(data){	
				$('#PreviewMerged').html(data);
				//alert(data);
			});
			
			$.post(
				'apps/admission/ajax.php?action=getReplacedMailText',
				{
					'text': encodeURIComponent(mail_subject),
					'recordID' : '',
					'applicationIds' : applicationID
				},
				function(data){	
				$('#PreviewSubject').html(data);
				//alert(data);
			});
		}).change();
		
		$('#ViewPrevious').click(function(){
			//var OptionTotal = $('#PreviewOptions option').length;
			//var OptionNow = parseInt($('#PreviewOptions').val());
			$("#PreviewOptions option:selected").attr('selected', '').prev('option').attr('selected', 'selected');	
			$('#PreviewOptions').change();
		});
		
		$('#ViewNext').click(function(){
			//var OptionTotal = $('#PreviewOptions option').length;
			//var OptionNow = parseInt($('#PreviewOptions').val());
			$("#PreviewOptions option:selected").attr('selected', '').next('option').attr('selected', 'selected');
			$('#PreviewOptions').change();
		});
		
		$('#ProgressCancelBtn').click(function(){
			if(confirm(msg.confirm_to_terminate)){
				window.parent.kis.admission.send_email_data_object['Cancelled'] = true;
				$('#ProgressCancel').hide();
			}
		});
		
		$('#ProgressCloseBtn').click(function(){
			$('#ProgressContainer').hide();
			$('#module_page .board_main_content > .board_loading_overlay').remove();
		});
		
		window.parent.kis.admission.send_email_data_object['sending_msg1'] = msg['sending_msg1'];
		window.parent.kis.admission.send_email_data_object['sending_msg2'] = msg['sending_msg2'];
	},
	sendmail_compose_send_email : function(FileUploadTmpFolder){
		
		$('#FileUploadTmpFolder').val(FileUploadTmpFolder);
		/*
		$.post(
			'apps/admission/ajax.php?action=sendMails&SendTarget=1',
			$('form#MailForm').serialize(),
			function(data){
				$('.step2').hide();
				$('.step3').show();
				$('#ComposeMailTable').hide();
				var result_table = $('#ComposeResultTable');
				if(result_table.length > 0){
					result_table.remove();
				}
				$('#ComposeMailTable').after(data);
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		);
		*/
		
		$('#ProgressContainer').show();
		$('#ProgressCancel').hide();
		$('#ProgressClose').hide();
		$('#progressbarA').progressBar(0);
		$.post(
			'apps/admission/ajax.php?action=prepareSendMails&SendTarget=1',
			$('form#MailForm').serialize(),
			function(data){
				var dataObj = {};
				if(JSON){
					dataObj = JSON.parse(data);
				}else{
					eval('var dataObj = ' + data);
				}
				var receiver_id_ary = dataObj['ReceiverID'].split(',');
				window.parent.kis.admission.send_email_data_object['RecordID'] = dataObj['RecordID'];
				window.parent.kis.admission.send_email_data_object['AttachmentFolder'] = dataObj['AttachmentFolder'];
				window.parent.kis.admission.send_email_data_object['ReceiverID'] = receiver_id_ary;
				window.parent.kis.admission.send_email_data_object['AllReceiverID'] = receiver_id_ary.slice(0);
				window.parent.kis.admission.send_email_data_object['Cancelled'] = false;

				window.parent.kis.admission.send_email_to_receivers();
			}
		);
	},
	sendmail_compose_resend_email : function(FileUploadTmpFolder){
		$('#FileUploadTmpFolder').val(FileUploadTmpFolder);
		/*
		$.post(
			'apps/admission/ajax.php?action=sendMails&SendTarget='+$('#SendTarget').val(),
			$('form#MailForm').serialize(),
			function(data){
				$('.step2').hide();
				$('.step3').show();
				//$.address.value('/apps/admission/emaillist/main/');
				$('#ComposeMailTable').hide();
				var result_table = $('#ComposeResultTable');
				if(result_table.length > 0){
					result_table.remove();
				}
				$('#ComposeMailTable').after(data);
				$('#module_page .board_main_content > .board_loading_overlay').remove();
			}
		);
		*/
		var vals = "";
		if($('#SendTarget').val() == 5){
			var checkboxes = document.getElementsByName('applicant_id[]');
			for (var i=0, n=checkboxes.length;i<n;i++) {
			  if (checkboxes[i].checked) 
			  {
			  vals += ","+checkboxes[i].value;
			  }
			}
			vals = '&applicant_id='+vals.substring(1);
		}
		
		$('#ProgressContainer').show();
		$('#ProgressCancel').hide();
		$('#ProgressClose').hide();
		$('#progressbarA').progressBar(0);
		
		$.post(
			'apps/admission/ajax.php?action=prepareSendMails&SendTarget='+$('#SendTarget').val()+vals,
			$('form#MailForm').serialize(),
			function(data){
				var dataObj = {};
				if(JSON){
					dataObj = JSON.parse(data);
				}else{
					eval('var dataObj = ' + data);
				}
				var receiver_id_ary = dataObj['ReceiverID'].split(',');
				window.parent.kis.admission.send_email_data_object['RecordID'] = dataObj['RecordID'];
				window.parent.kis.admission.send_email_data_object['AttachmentFolder'] = dataObj['AttachmentFolder'];
				window.parent.kis.admission.send_email_data_object['ReceiverID'] = receiver_id_ary;
				window.parent.kis.admission.send_email_data_object['AllReceiverID'] = receiver_id_ary.slice(0);
				window.parent.kis.admission.send_email_data_object['Cancelled'] = false;
				
				window.parent.kis.admission.send_email_to_receivers();
			}
		);
	},
	send_email_data_object  : {},
	send_email_to_receivers : function(){
		$('#ProgressCancel').show();
		if(window.parent.kis.admission.send_email_data_object['Cancelled']){
			$.post(
				'apps/admission/ajax.php?action=getSendMailResultTable',
				{
					"recordId": window.parent.kis.admission.send_email_data_object['RecordID'],
					"AttachmentFolder" : window.parent.kis.admission.send_email_data_object['AttachmentFolder'] 
				},
				function(returnData){
					$('.step2').hide();
					$('.step3').show();
					$('#ComposeMailTable').hide();
					var result_table = $('#ComposeResultTable');
					if(result_table.length > 0){
						result_table.remove();
					}
					$('#ComposeMailTable').after(returnData);
					$('#ProgressCancel').hide();
					$('#ProgressClose').show();
				}
			);
			return;
		}
		
		var data = {};
		var num_per_batch = 50;
		data['RecordID'] = window.parent.kis.admission.send_email_data_object['RecordID'];
		data['AttachmentFolder'] = window.parent.kis.admission.send_email_data_object['AttachmentFolder'];
		data['SendTarget'] = $('#SendTarget').length>0? $('#SendTarget').val():1;
		data['ReceiverID[]'] = [];
		for(var i=0;i<num_per_batch;i++){
			if(window.parent.kis.admission.send_email_data_object['ReceiverID'].length>0){
				data['ReceiverID[]'].push(window.parent.kis.admission.send_email_data_object['ReceiverID'].shift());
			}
		}
		data['changeStatus'] = $('[name="changeStatus"]:checked').val();
		data['IsLastBatch'] = window.parent.kis.admission.send_email_data_object['ReceiverID'].length == 0? '1': '0';
		$.post(
			'apps/admission/ajax.php?action=sendEmailToReceivers',
			data,
			function(returnData){
				var total = window.parent.kis.admission.send_email_data_object['AllReceiverID'].length;
				var progress_percent = Math.round( ((total - window.parent.kis.admission.send_email_data_object['ReceiverID'].length)/total) * 100);
				$('#progressbarA').progressBar(progress_percent);
				$('#ProgressMessage').html("<strong>"+window.parent.kis.admission.send_email_data_object['sending_msg1']+" "+total+" "+window.parent.kis.admission.send_email_data_object['sending_msg2']+" </strong>");
				if(data['IsLastBatch']=='1'){
					$('.step2').hide();
					$('.step3').show();
					$('#ComposeMailTable').hide();
					var result_table = $('#ComposeResultTable');
					if(result_table.length > 0){
						result_table.remove();
					}
					$('#ComposeMailTable').after(returnData);
					$('#ProgressCancel').hide();
					$('#ProgressClose').show();
				}else{
					setTimeout(function(){
						window.parent.kis.admission.send_email_to_receivers();
					},3000);
				}
			}
		);
	},
	
	reports_payment_init: function(lang){
		
		kis.datepicker('#start_date');
		kis.datepicker('#end_date');
		$('#start_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_start_date').html(warning);
		});
		$('#start_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			
			$('span#warning_start_date').html(warning);
		});
		
		$('#end_date').keyup(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_end_date').html(warning);
		});
		$('#end_date').change(function(){
			var warning = '';
			if(this.value!=''&&!check_date_without_return_msg(this)){
				warning = lang.invaliddateformat;
			}else if(this.value!=''&&compareTimeByObjId('start_date','start_time_hour :selected','start_time_min :selected','start_time_sec :selected','end_date','end_time_hour :selected', 'end_time_min :selected','end_time_sec :selected')==false){
				warning = lang.enddateearlier;
			}
			$('span#warning_end_date').html(warning);
		});
		
		var submitCheckForm = function(){
			var start_date = $('#start_date').val();
			var end_date = $('#end_date').val();

			if(!check_date_without_return_msg(document.getElementById('start_date'))){
				$('span#warning_start_date').html(lang.invaliddateformat);
				$('#start_date').focus();
				return false;
			}else if(!check_date_without_return_msg(document.getElementById('end_date'))){
				$('span#warning_end_date').html(lang.invaliddateformat);
				$('#end_date').focus();
				return false;
			}else if(compareDate(end_date,start_date)!=1){
				//$('span#warning_start_date').html(lang.enddateearlier);
				$('#start_date').focus();
				return false;
			}
			
			$.address.value('/apps/admission/reports/paymentreport/?start_date='+$('#start_date').val()+'&end_date='+$('#end_date').val());
		};
		
		$('input#submitBtn').click(submitCheckForm);
	
		$('input#cancelBtn').click(function(){
			$.address.value('/apps/admission/applicantslist/listbyform/'+$('input#classLevelID').val()+'/');
		});
		
	},
	
	reports_payment_report_init: function(lang){
	
		$('input#backBtn').click(function(){
			$.address.value('/apps/admission/reports/?start_date='+$('#start_date').val()+'&end_date='+$('#end_date').val());
		});
		
		$('a.tool_export').click(function(){
			$('#export_form').prop("action", "/kis/admission_form/export_payment_report.php").submit();
			return false;
		});
	}
}