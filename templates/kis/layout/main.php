<!DOCTYPE html>
<html>
    
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<title>:: eClass KIS ::</title>
<link type="text/css" rel="stylesheet" href="/templates/kis/css/common.css">
<link type="text/css" rel="stylesheet" href="/templates/jquery/jquery.fancybox.css">
<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Bubblegum+Sans">
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.address-1.5.min.js"></script>
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/templates/kis/js/kis.js"></script>
</head>

<body class="<?=$kis_config['background'][$kis_user['type']]?>">
    <div id="container">
    <script>
	kis.init({
	    user_type: '<?=$kis_user['type']?>',
	    children: <?=$kis_data['children_json']?>
	});
	
	$(function(){
	    
	    $(".fancybox-thumb").fancybox({
		prevEffect : 'fade',
		nextEffect : 'fade',

	    });
	    
	    $('.btn_logout').click(function(){
		return confirm('Are you sure to log out?');
	    });
	});
    </script>
    <div class="top_header">
	<a title="eClass KIS" class="logo_kis" href="./"></a>
	<div class="school_name"><?=$kis_data['school']['en']?><br><?=$kis_data['school']['b5']?></div>
	<div class="user_btn">
	    <a title="Logout" class="btn_logout" href="/logout.php"></a>
	    <? if ($intranet_session_language=='b5'):?>
	    <a title="Change to Chinese Language" class="btn_lang_eng" href="/lang.php?lang=en"></a>
	    <? else: ?>
	    <a title="Change to Chinese Language" class="btn_lang_chn" href="/lang.php?lang=b5"></a>
	    <? endif; ?>
	    <span class="sep">|</span>
	<span><em><?=$kis_lang[$kis_user['type']]?>,</em> <?=$kis_user['name']?></span>            
	</div>
	<? if ($kis_user['type']=='parent') : ?>

	<div class="parent_btn">
	    <a class="btn_menu btn_menu_on" href="#"><span>Menu</span><em></em></a>
	    <div class="student_name_btn"><div>
		<span class="student_name"><em class="<?=$kis_user['sex']=='M'?'boy':'girl'?>"></em>
		    <span><?=$kis_user['name']?></span>
		</span>
		<span class="student_text"><?=$kis_lang['with']?></span>
		<a class="btn_change_student" href="#"></a>
		<a class="btn_logs" href="#/logs"><span><?=$kis_lang['logs']?></span></a>
	    </div></div>
	   
	</div>

	<? endif; ?>
    </div>
    <div class="container_slider" style="margin-left:-990px">
	<div id="update_page" class="board" style="opacity:0">
	    <div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		<div class="update_photo">
		    <img src="<?=$kis_user['image']?>"/>
		</div>
		
		<div class="update_info">
			<h1><?=$kis_user['name']?></h1>
			Class <span class="student_class"><?=$kis_user['class']?></span><br/>
			Class Teacher <a class="student_class_teacher" href="#"><?=$kis_user['teacher']?></a>
		</div>
		    
	    </div></div></div></div>
	    <div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
		    
    
	    </div></div></div></div>
	    
	    <div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
	</div>
	
	
	<div id="main_portal" class="board">
		<div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		
		
		</div></div></div></div>
		<div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">
		    <!---->
		    <ul class="module_list">
			
			<? foreach ($kis_data['apps'] as $app): ?>
			   
				<? if ($app['href']): ?>
				    <li><a class="<?=$app['image']?>" target="_blank" href="<?=$app['href']?>">
				<? else: ?>
				    <li class="app_<?=$app['title']?>"><a class="<?=$app['image']?>" href="#/apps/<?=$app['title']?>/">
				<? endif; ?>
				
				<span style="display:none" class="bg_theme"><?=$app['bg_theme']?></span>
				<span class="title"><?=$kis_lang[$app['title']]?></span>
				<em style="display:none"></em>
			    </a></li>
			<? endforeach; ?>
			    <p class="spacer"></p>
		    </ul>    	
		    <!---->
			     <p class="spacer"></p>
			     
		</div></div></div></div>
		<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		
	</div>
        
	<div id="module_page" class="board">
    
	    <div class="board_top"><div class="board_top_right"><div class="board_top_bg"><div class="board_top_content">
		<div class="navigation_bar">
			<a class="btn_home" href="#"><?=$kis_lang['home']?></a><span class="module_title"></span>
			
		</div>
		    
	    </div></div></div></div>
	    
	    <div class="board_main"><div class="board_main_right"><div class="board_main_bg"><div class="board_main_content">


		  
	    
	    </div></div></div></div>
	    
	<div class="board_bottom"><div class="board_bottom_right"><div class="board_bottom_bg"></div></div></div>
		
    </div>
	
    </div>
      <p class="spacer"></p>
    <div class="footer"><a title="eClass" href="eclass.com.hk"></a><span>Powered by</span></div>
    </div>


</body></html>