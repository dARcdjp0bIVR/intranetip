<?php
/*
 * 2019-01-22 Isaac
 *      Changed the file from static to relative file calling the common template file
 *
 * 	2017-12-20 Cameron
 * 		set overflow:visible for eInventory so that it won't hide partial title name
 */

$containerNo = 3;

$this->loadFooter($source_path, 'custom');

if(file_exists("../..".$powerClass_common_template_source_path."/reports.php")){
    include_once ("../..".$powerClass_common_template_source_path."/reports.php");
}
?>
